USE [DebtPlus]
GO
/****** Object:  StoredProcedure [dbo].[rpt_BalanceVerify]    Script Date: 09/15/2014 13:13:32 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[rpt_BalanceVerify] AS
-- =============================================================================================
-- ==             Retrieve a list of the debts that need to be verified for the balance       ==
-- =============================================================================================

-- ChangeLog
--   2/1/2002
--     Change to rpps_biller_ids table.
--   9/31/2002
--     Added support for client_creditor_balances table
--   11/20/2002
--     Changed to use RPPS' biller_aba number rather than rpps_biller_id
--   2/18/2003
--     Moved the "reprint" date test from rpt_BalanceVerify_CreateMissing to this routine to avoid problems with RPPS
--   3/14/2003
--     Allow for more than 10,000 creditors in a class
--   06/09/2009
--      Remove references to debt_number and use client_creditor
--   1/5/2009
--      Changed from using biller_aba to using rpps_biller_id

-- Suppress intermediate results
SET NOCOUNT ON

-- Set balance verification flags as needed for debts
Execute	rpt_BalanceVerify_CreateMissing

-- Find the range of times that represent "today"
declare		@date_first	datetime
declare		@date_last	datetime
select		@date_first = convert(datetime, convert(varchar(10), getdate(), 101) + ' 00:00:00')
select		@date_last  = dateadd(d, 1, @date_first)

-- Set the flag for paper balance verifications when they are for the same day
-- We do this to accomodate a "reprint" of the report.
update		client_creditor
set			send_bal_verify = 1
FROM		client_creditor cc	WITH (NOLOCK)
INNER JOIN	clients c		WITH (NOLOCK) ON cc.client = c.client
LEFT OUTER JOIN	creditors cr		WITH (NOLOCK) ON cc.creditor = cr.creditor
inner join	creditor_methods cm	WITH (NOLOCK) ON cr.creditor_id = cm.creditor and 'BAL' = cm.type and cm.region in (0, c.region)
left outer join	banks b			WITH (NOLOCK) ON cm.bank = b.bank
WHERE		(cc.verify_request_date >= @date_first) and (cc.verify_request_date < @date_last)
AND			cc.reassigned_debt = 0
AND			isnull(b.type,'C') = 'C';

create table #t_Verifications (
	client_creditor			int null,
	client					int null,
	creditor				varchar(10) null,
	cr_type					varchar(10) null,
	cr_creditor_id			int null,
	person					int null,
	name					varchar(256) null,
	ssn						varchar(20) null,
	account_number			varchar(80) null,
	verify_request_date		datetime null,
	zipcode					varchar(20) null,
	[type]					varchar(10) null,
	total_payments			money null,
	payments_this_creditor	money null,
	balance					money null
);

-- Generate the list of items to be processed
insert into #t_verifications
SELECT		cc.client_creditor					as 'client_creditor',
			cc.client							as 'client',
			cc.creditor							as 'creditor',
			cr.type								as 'cr_type',
			cr.creditor_id						as 'cr_creditor_id',
			cc.person							as 'person',
			convert(varchar(80),null)			as 'name',
			convert(varchar(20),null)			as 'ssn',
			cc.account_number					as 'account_number',
			cc.verify_request_date				as 'verify_request_date',
			convert(varchar(10),null)			as 'zipcode',
			convert(varchar(10), 'X')			as 'type',	-- initially record an invalid type
			bal.total_payments					as 'total_payments',
			cc.payments_this_creditor			as 'payments_this_creditor',
			isnull(bal.orig_balance,0) + isnull(bal.orig_balance_adjustment,0) + isnull(bal.total_interest,0) - isnull(bal.total_payments,0) as 'balance'
FROM		client_creditor cc	WITH (NOLOCK)
INNER JOIN	client_creditor_balances bal WITH (NOLOCK) ON cc.client_creditor_balance = bal.client_creditor_balance
LEFT OUTER JOIN	creditors cr		WITH (NOLOCK) ON cc.creditor = cr.creditor
WHERE		cc.send_bal_verify > 0
AND			cc.reassigned_debt = 0;

-- Set the default owner to the applicant value
update		#t_Verifications
set			person			= p.person
from		#t_Verifications v
inner join	people p WITH (NOLOCK) ON v.client = p.client
where		p.relation = 1
and			v.person is null;

-- Set the type to indicated printed for those that are printed.
update		#t_Verifications
set			type		= 'C'
from		#t_Verifications x
inner join	creditors cr on cr.creditor = x.creditor
inner join	clients c on c.client = x.client
inner join	creditor_methods cm on cr.creditor_id = cm.creditor and 'BAL' = cm.type and cm.region in (0, c.region)
left outer join	banks b on cm.bank = b.bank
where		isnull(b.type,'C') = 'C';

-- Discard all but the printed items.
delete
from		#t_Verifications
where		type = 'X';

-- Correct the information from the person information
update		#t_Verifications
set			name		= dbo.format_normal_name(pn.prefix, pn.first, pn.middle, pn.last, pn.suffix),
			ssn			= dbo.format_ssn(p.ssn)
from		#t_Verifications v
inner join	people p on v.client = p.client and 1 = p.relation
left outer join names pn on p.NameID = pn.Name;

update		#t_Verifications
set			name		= dbo.format_normal_name(pn.prefix, pn.first, pn.middle, pn.last, pn.suffix),
			ssn			= dbo.format_ssn(p.ssn)
from		#t_Verifications v
inner join	people p on v.client = p.client and 1 <> p.relation
left outer join names pn on p.NameID = pn.Name
where		v.person	<> 1
and			p.NameID is not null;

-- Update the zipcode values with the payment information if the proposal address is not present
update		#t_verifications
set			zipcode = ca.postalcode
from		#t_verifications a
inner join	view_creditor_addresses ca on a.creditor = ca.creditor and 'P' = ca.type

update		#t_verifications
set			zipcode = ca.postalcode
from		#t_verifications a
inner join	view_creditor_addresses ca on a.creditor = ca.creditor and 'P' = ca.type
where		a.zipcode is null

-- =============================================================================================
-- ==                Record the fact that we are sending the verifcation now                  ==
-- =============================================================================================

UPDATE		client_creditor
SET			verify_request_date	= getdate(),
			send_bal_verify		= 0
FROM		client_creditor cc
INNER JOIN	#t_Verifications t ON cc.client_creditor = t.client_creditor

-- Reset the result sets
SET NOCOUNT OFF

-- =============================================================================================
-- ==               Return the list of debts to be processed in this report                   ==
-- =============================================================================================

SELECT * FROM #t_Verifications ORDER BY zipcode, cr_type, cr_creditor_id

-- =============================================================================================
-- ==               Discard the working table                                                 ==
-- =============================================================================================
DROP TABLE #t_Verifications

RETURN ( @@rowcount )
GO
