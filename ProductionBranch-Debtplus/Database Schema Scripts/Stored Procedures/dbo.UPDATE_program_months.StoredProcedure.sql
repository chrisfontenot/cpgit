USE [DebtPlus]
GO
/****** Object:  StoredProcedure [dbo].[UPDATE_program_months]    Script Date: 09/15/2014 13:13:34 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[UPDATE_program_months] as

-- ===============================================================================
-- ==            Initialize the program months for the clients                  ==
-- ===============================================================================

update clients
set program_months = -1;

update clients
set program_months = datediff(m, start_date, drop_date)
where program_months < 0 and start_date is not null and drop_date is not null;

update clients
set program_months = datediff(m, start_date, getdate())
where program_months < 0 and start_date is not null and drop_date is null;

update clients
set program_months = 0
where active_status in ('CRE', 'APT', 'WKS', 'RDY', 'PRO');

update clients
set program_months = 0
where program_months < 0;
GO
