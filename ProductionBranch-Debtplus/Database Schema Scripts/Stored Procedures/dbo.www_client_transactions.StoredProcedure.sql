SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER procedure [www_client_transactions] ( @client as int, @date_range as varchar(30) = null ) AS
-- ===========================================================================================
-- ==            Generate the list of transactions for the date range                       ==
-- ===========================================================================================

-- ChangeLog
--   11/18/2002
--     Added "LD" transaction types to the returned result set with special processing for the date.

-- Suppress intermediate results
set nocount on

-- Date ranges
declare	@from_date	datetime
declare	@to_date	datetime

select	@date_range	= lower(@date_range)

-- Determine the current month range
if @date_range = 'current_month'
begin
	select	@to_date = getdate()
	select	@from_date = convert(datetime, convert(varchar, month(@to_date)) + '/01/' + convert(varchar, year(@to_date))) + ' 00:00:00'
end

-- Current year
else if @date_range = 'current_year'
begin
	select	@from_date = getdate()
	select	@to_date = convert(datetime, '12/31/' + convert(varchar, year(@from_date))) + ' 23:59:59'
	select	@from_date = convert(datetime, '01/01/' + convert(varchar, year(@from_date))) + ' 00:00:00'
end

-- Find the last 60 days information
else if @date_range = 'last_60'
begin
	select	@to_date = getdate()
	select	@to_date = convert(datetime, convert(varchar, month(@to_date)) + '/01/' + convert(varchar, year(@to_date))) + ' 00:00:00'
	select	@to_date = dateadd (s, -1, @to_date)
	select	@from_date = dateadd (m, -1, @to_date)
	select	@from_date = convert(datetime, convert(varchar, month(@from_date)) + '/01/' + convert(varchar, year(@from_date))) + ' 00:00:00'
end

-- Find the last 90 days information
else if @date_range = 'last_90'
begin
	select	@to_date = getdate()
	select	@to_date = convert(datetime, convert(varchar, month(@to_date)) + '/01/' + convert(varchar, year(@to_date))) + ' 00:00:00'
	select	@to_date = dateadd (s, -1, @to_date)
	select	@from_date = dateadd (m, -2, @to_date)
	select	@from_date = convert(datetime, convert(varchar, month(@from_date)) + '/01/' + convert(varchar, year(@from_date))) + ' 00:00:00'
end

-- Find the last 120 days information
else if @date_range = 'last_120'
begin
	select	@to_date = getdate()
	select	@to_date = convert(datetime, convert(varchar, month(@to_date)) + '/01/' + convert(varchar, year(@to_date))) + ' 00:00:00'
	select	@to_date = dateadd (s, -1, @to_date)
	select	@from_date = dateadd (m, -3, @to_date)
	select	@from_date = convert(datetime, convert(varchar, month(@from_date)) + '/01/' + convert(varchar, year(@from_date))) + ' 00:00:00'
end

-- Find the last year information
else if @date_range = 'last_year'
begin
	select	@from_date = dateadd (yy, -1, getdate())
	select	@to_date = convert(datetime, '12/31/' + convert(varchar, year(@from_date))) + ' 23:59:59'
	select	@from_date = convert(datetime, '01/01/' + convert(varchar, year(@from_date))) + ' 00:00:00'
end

-- Assume that the default is "last_month"
else
begin
	select	@to_date = getdate()
	select	@to_date = convert(datetime, convert(varchar, month(@to_date)) + '/01/' + convert(varchar, year(@to_date))) + ' 00:00:00'
	select	@to_date = dateadd (s, -1, @to_date)
	select	@from_date = @to_date
	select	@from_date = convert(datetime, convert(varchar, month(@from_date)) + '/01/' + convert(varchar, year(@from_date))) + ' 00:00:00'
end

-- Remove the times from the date fields
select	@from_date = convert(datetime, convert(varchar(10), @from_date, 101) + ' 00:00:00')
select	@to_date   = convert(datetime, convert(varchar(10), @to_date, 101)   + ' 23:59:59')

-- Find the transactions
select
	rc.client_register				as 'client_register',
	convert(varchar(10), rc.date_created, 1)	as 'date_created',
	rc.tran_type					as 'tran_type',
	isnull(rc.credit_amt,0)				as 'credit_amt',
	isnull(rc.debit_amt,0)				as 'debit_amt',
	isnull(convert(varchar,tr.checknum),'')			as 'check_number',
	rc.date_created					as 'date_sequence'

from	registers_client rc with (nolock)
left outer join registers_trust tr with (nolock) on rc.trust_register = tr.trust_register
where	rc.client = @client
and	rc.date_created between @from_date and @to_date
and	rc.tran_type not in ('EB', 'LD')

UNION ALL


select
	rc.client_register				as 'client_register',
	convert(varchar(10), rc.item_date, 1)		as 'date_created',
	'LD'						as 'tran_type',
	0						as 'credit_amt',
	0						as 'debit_amt',
	''						as 'check_number',
	rc.item_date					as 'date_sequence'

from	registers_client rc with (nolock)
where	rc.client = @client
and	rc.item_date between @from_date and @to_date
and	rc.tran_type = 'LD'

order by 7

return ( @@rowcount )
GO
