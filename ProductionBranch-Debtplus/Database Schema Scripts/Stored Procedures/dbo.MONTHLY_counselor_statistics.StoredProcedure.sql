USE [DebtPlus]
GO
/****** Object:  StoredProcedure [dbo].[MONTHLY_counselor_statistics]    Script Date: 09/15/2014 13:13:32 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[MONTHLY_counselor_statistics] AS

-- ChangeLog
--   1/29/2004
--     Initial Creation

-- Create transaction to update the counselor statistics
BEGIN TRANSACTION
SET NOCOUNT ON

-- Find the date range for the report
declare	@FromDate	datetime
declare	@ToDate		datetime

select	@ToDate		= getdate()
select	@FromDate	= convert(datetime, convert(varchar,month(@ToDate)) + '/01/' + convert(varchar,year(@ToDate)) + ' 00:00:00')

-- If this is the first of the month then use the last month figures
if day( @ToDate ) = 1
	select	@FromDate	= dateadd(month, -1, @FromDate)

select	@ToDate		= dateadd(m, 1, @FromDate)
select	@ToDate		= dateadd(s, -1, @ToDate)

-- Create a working table for the current period
create table #new_records (counselor int null, office int null, active_clients int null, dropped_clients int null, clients_disbursed int null, expected_disbursement money null, actual_disbursement money null, paf_fees money null,new_dmp_clients int, new_dmp_debt money);

-- Build the default list of counselors
insert into #new_records (counselor, office, active_clients, dropped_clients, expected_disbursement, actual_disbursement, new_dmp_clients, new_dmp_debt)
select	distinct isnull(counselor,0) as counselor, isnull(office,0), 0, 0, 0, 0, 0, 0
from	clients;

-- Find the active clients
select	isnull(counselor,0) as counselor, isnull(office,0) as office, count(*) as cnt
into	#active_clients
from	clients
where	active_status in ('A', 'AR')
group by isnull(counselor,0), isnull(office,0);

update	#new_records
set	active_clients = x.cnt
from	#new_records r
inner join #active_clients x on r.counselor = x.counselor and r.office = x.office;

-- Determine the number of dropped clients
select	isnull(counselor,0) as counselor, isnull(office,0) as office, count(*) as cnt
into	#dropped_clients
from	clients
where	active_status = 'I'
and	drop_date between @FromDate and @ToDate
group by isnull(counselor,0), isnull(office,0);

update	#new_records
set	dropped_clients = i.cnt
from	#new_records r
inner join #dropped_clients i on r.counselor = i.counselor and r.office = i.office;

drop table #dropped_clients

-- New dmp clients
select	isnull(counselor,0) as counselor,
	isnull(office,0) as office,
	client as client
into	#new_dmp_clients
from	clients with (nolock)
where	active_status_date between @FromDate and @ToDate
and	active_status in ('A','AR');

-- Debt for these clients
select	isnull(a.counselor,0) as counselor,
	isnull(a.office,0) as office,
	a.client as client,
	sum(b.orig_balance + b.orig_balance_adjustment + b.total_interest - b.total_payments) as new_dmp_debt
into	#new_dmp_debt_2
from	#new_dmp_clients a
inner join client_creditor cc on a.client = cc.client
left outer join creditors cr on cc.creditor = cr.creditor
left outer join creditor_classes ccl on cr.creditor_class = ccl.creditor_class
inner join client_creditor_balances b on cc.client_creditor_balance = b.client_creditor_balance and b.client_creditor = cc.client_creditor
where	isnull(ccl.zero_balance,0) = 0
group by a.counselor, a.office, a.client;

-- Find the debt by counselor
select	counselor, office, sum(new_dmp_debt) as new_dmp_debt
into	#new_dmp_debt
from	#new_dmp_debt_2
group by counselor, office;

-- Find the number of clients by counselor
select	counselor, office, count(*) as new_dmp_clients
into	#new_dmp_clients_2
from	#new_dmp_clients
group by counselor, office;

-- Update the new records
update	#new_records
set	new_dmp_clients = isnull(x.new_dmp_clients,0)
from	#new_records a
inner join #new_dmp_clients_2 x on a.counselor = x.counselor and a.office = x.office;

update	#new_records
set	new_dmp_debt = isnull(x.new_dmp_debt,0)
from	#new_records a
inner join #new_dmp_debt x on a.counselor = x.counselor and a.office = x.office;

drop table #new_dmp_debt
drop table #new_dmp_debt_2
drop table #new_dmp_clients_2
drop table #new_dmp_clients

-- Update the expected disbursements for the clients
select		isnull(c.counselor,0) as counselor, isnull(c.office,0) as office, sum(isnull(bal.current_sched_payment,0)) as sched_payment
into		#sched_payment
from		client_creditor_balances bal	with (nolock)
inner join client_creditor cc			with (nolock) on bal.client_creditor = cc.client_creditor
inner join clients c				with (nolock) on cc.client = c.client
where c.active_status in ('A', 'AR')
group by isnull(c.counselor,0), isnull(c.office,0);

update	#new_records
set	expected_disbursement = x.sched_payment
from	#sched_payment x
inner join #new_records y on x.counselor = y.counselor and x.office = y.office;

-- Retrieve the creditor for the P.A.F. amounts
declare	@paf_creditor		varchar(10)
declare	@deduct_creditor	varchar(10)
select	@paf_creditor		= paf_creditor,
	@deduct_creditor	= deduct_creditor
from	config;

if @deduct_creditor is null
	select	@deduct_creditor	= ''

if @paf_creditor is null
	select	@paf_creditor		= @deduct_creditor
else begin
	-- Calculate the PAF fees collected
	select	isnull(c.counselor,0) as counselor, isnull(c.office,0) as office, sum(isnull(rcc.debit_amt,0) - isnull(rcc.credit_amt,0)) as paf_fees
	into	#paf_fees
	from	registers_client_creditor rcc with (nolock)
	inner join clients c with (nolock) on rcc.client = c.client
	where	rcc.date_created between @FromDate and @ToDate
	and	rcc.creditor = @paf_creditor
	and	rcc.tran_type in ('AD','BW','MD','CM','RF','RR','VD')
	group by isnull(c.counselor,0), isnull(c.office,0);

	update	#new_records
	set	paf_fees = x.paf_fees
	from	#paf_fees x
	inner join #new_records y on x.counselor = y.counselor and x.office = y.office;
end

-- Find the payments to the non-PAF creditors
select	isnull(c.counselor,0) as counselor, isnull(c.office,0) as office, sum(isnull(rcc.debit_amt,0) - isnull(rcc.credit_amt,0)) as disbursed
into	#dollars_disbursed
from	registers_client_creditor rcc	with (nolock)
inner join clients c			with (nolock) on rcc.client = c.client
where	rcc.date_created between @FromDate and @ToDate
and	rcc.creditor not in (@paf_creditor, @deduct_creditor)
and	rcc.tran_type in ('AD','BW','MD','CM','RF','RR','VD')
and	c.client > 0
group by isnull(c.counselor,0), isnull(c.office,0);

-- Find the count of clients disbursed
select	distinct isnull(c.counselor,0) as counselor, isnull(c.office,0) as office, c.client
into	#clients_disbursed_raw
from	registers_client_creditor rcc	with (nolock)
inner join clients c			with (nolock) on rcc.client = c.client
where	rcc.date_created between @FromDate and @ToDate
and	rcc.creditor not in (@paf_creditor, @deduct_creditor)
and	rcc.tran_type in ('AD','BW','MD','CM')
and	c.client > 0
and	rcc.debit_amt > 0
group by isnull(c.counselor,0), c.client, isnull(c.office,0);

select	counselor, office, count(*) as cnt
into	#clients_disbursed
from	#clients_disbursed_raw
group by counselor, office;

-- Update the working table with the results
update	#new_records
set	actual_disbursement = x.disbursed
from	#dollars_disbursed x
inner join #new_records y on x.counselor = y.counselor and x.office = y.office;

update	#new_records
set	clients_disbursed = x.cnt
from	#clients_disbursed x
inner join #new_records y on x.counselor = y.counselor and x.office = y.office;

-- Remove any extra items from testing
delete
from	statistics_counselors
where	period_start = @FromDate;

-- Insert the new statistics into the table
insert into statistics_counselors (period_start, counselor, office, dropped_clients, active_clients, clients_disbursed, expected_disbursement, actual_disbursement, paf_fees, new_dmp_clients, new_dmp_debt)
select	@FromDate, counselor, office, isnull(dropped_clients,0), isnull(active_clients,0), isnull(clients_disbursed,0), isnull(expected_disbursement,0), isnull(actual_disbursement,0), isnull(paf_fees,0), isnull(new_dmp_clients,0), isnull(new_dmp_debt,0)
from	#new_records;

-- Discard the working tables
drop table #clients_disbursed;
drop table #clients_disbursed_raw;
drop table #dollars_disbursed;
drop table #new_records;
drop table #paf_fees;
drop table #sched_payment;
drop table #active_clients;

COMMIT TRANSACTION
return ( 1 )
GO
