USE [DebtPlus]
GO
/****** Object:  StoredProcedure [dbo].[custom_report_ach_changes]    Script Date: 09/15/2014 13:13:32 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[custom_report_ach_changes] ( @from_date as datetime, @to_date as datetime ) as

-- ====================================================================================================
-- ==            Report changes in the ACH account information                                       ==
-- ====================================================================================================

-- ChangeLog
--   12/19/2008
--      Revisions for SQL2008

set nocount on

if @to_date is null
	select	@to_date = getdate()

if @from_date is null
	select	@from_date = @to_date

select	@from_date = convert(datetime, convert(varchar(10), @from_date, 101) + ' 00:00:00'),
	@to_date = convert(datetime, convert(varchar(10),   @to_date,   101) + ' 23:59:59')

select	n.client as client,
        dbo.format_normal_name (default, pn.first, default, pn.last, default) as client_name,
        isnull(dbo.format_normal_name(default,cox.first,default,cox.last,default),n.created_by) as counselor,
        n.date_created as event_date,
        convert(varchar(8000),n.note) as note
from	client_notes n
left outer join people p on n.client = p.client and 1 = p.relation
left outer join names pn with (nolock) on p.nameid = pn.name
left outer join counselors co on n.created_by = co.person
left outer join names cox with (nolock) on co.NameID = cox.name
where	n.type = 3
and	n.subject in ('Deposit Information Changed','Deposit Information Changed')
and	n.date_created between @from_date and @to_date
and	convert(varchar(80),n.note) not like 'PAF Fee changed%'
order by 1, 4  -- client, date_created

return ( @@rowcount )
GO
