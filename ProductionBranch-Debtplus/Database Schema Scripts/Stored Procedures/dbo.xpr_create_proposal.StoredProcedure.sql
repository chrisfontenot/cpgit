USE [DebtPlus]
GO
/****** Object:  StoredProcedure [dbo].[xpr_create_proposal]    Script Date: 09/15/2014 13:13:34 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE PROCEDURE [dbo].[xpr_create_proposal] ( @client_creditor AS INT, @Proposal_Status AS INT = 0 ) AS

-- ===================================================================================================
-- ==                   Create a proposal record as needed                                          ==
-- ===================================================================================================

-- ChangeLog
--    4/29/2008
--       Allowed a proposal status of "accepted by default" to be used. This should be the value rather
--       than status 2 (accepted) since accepted needs to have a person accepting the proposal supplied.

-- Ensure that the status is proper
IF @proposal_status IS NULL
	SELECT @proposal_status = 0

IF @proposal_status NOT IN (0, 1, 2, 3)
BEGIN
	RaisError (50052, 16, 1)
	Return ( 0 )
END

SET NOCOUNT ON

-- Ensure that there is a debt record for the proposal
IF NOT EXISTS ( SELECT * FROM client_creditor WHERE client_creditor = @client_creditor )
BEGIN
	RaisError (50053, 16, 1)
	Return ( 0 )
END

-- Create the proposal information
INSERT INTO client_creditor_proposals	( client_creditor,	proposal_status,	proposal_batch_id )
VALUES					( @client_creditor,	@proposal_status,	null )

-- Return the proposal record
DECLARE	@client_creditor_proposal	int
SELECT	@client_creditor_proposal = SCOPE_IDENTITY()

-- Ensure that the debt record is updated with the proposal status
UPDATE	client_creditor
SET	client_creditor_proposal	= @client_creditor_proposal
WHERE	client_creditor			= @client_creditor

-- Return the record number
RETURN ( @client_creditor_proposal )
GO
