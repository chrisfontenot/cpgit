USE [DebtPlus]
GO
/****** Object:  StoredProcedure [dbo].[DAILY_Statistics]    Script Date: 09/15/2014 13:13:32 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE PROCEDURE [dbo].[DAILY_Statistics] ( @Key AS SmallDateTime = NULL ) AS

-- =========================================================================================
-- ==            Do the steps needed for the statistic processing                         ==
-- =========================================================================================

-- Disable intermediate results
SET NOCOUNT ON

-- Local storage
DECLARE	@FromDate	SmallDateTime
DECLARE	@ToDate		SmallDateTime

-- If there is no key then use the proper date
IF @Key IS NULL
BEGIN
--	-- Determine the period for the operation. These are for monthly operations.
--	SELECT	@Key		= convert(SmallDateTime, convert(varchar,DatePart (m,getdate())) + '/01/' + convert(varchar,DatePart(y,getdate())) + ' 00:00:00')

	-- Determine the period for the operation. These are for daily operations.
	SELECT	@Key		= convert(SmallDateTime, convert(varchar(10), getdate(), 101) + ' 00:00:00')
END

-- These are for the monthly operation
--	SELECT	@FromDate	= dateadd( m, -1, @Key)
--	SELECT	@ToDate		= @key

SELECT	@FromDate	= dateadd( d, -1, @Key)
SELECT	@ToDate		= @key

-- Insert the record into the statistics table
IF NOT EXISTS (SELECT * FROM [statistics] WHERE period_start = @FromDate AND period_stop = @ToDate)
	INSERT INTO [statistics] (period_start, period_stop) VALUES (@FromDate, @ToDate)

-- Run the steps of the job
Execute	DAILY_Statistics_Clients	@FromDate,	@ToDate;
Execute	DAILY_Statistics_Trust		@FromDate,	@ToDate;

-- Return success to the caller
RETURN ( 1 )
GO
