USE [DebtPlus]
GO
/****** Object:  StoredProcedure [dbo].[xpr_insert_Housing_InterviewAddress]    Script Date: 09/15/2014 13:13:35 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[xpr_insert_Housing_InterviewAddress] ( @HousingID as int, @PrePostKeyID as varchar(1), @AddressID as int = null, @MortgageProgramCD as int = null, @FinanceProgramCD as int = null) as
-- ======================================================================================================================
-- ==            Insert a row for .NET into the Housing_InterviewAddress table                                         ==
-- ======================================================================================================================
	insert into Housing_InterviewAddress ( [HousingID], [PrePostKeyID], [AddressID], [MortgageProgramCD], [FinanceProgramCD] ) values ( @HousingID, @PrePostKeyID, @AddressID, @MortgageProgramCD, @FinanceProgramCD )
	return ( scope_identity() )
GO
