USE [DebtPlus]
GO
/****** Object:  StoredProcedure [dbo].[lst_rpps_assets]    Script Date: 09/15/2014 13:13:32 ******/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE PROCEDURE [dbo].[lst_rpps_assets] AS

-- ==================================================================================================
-- ==            Retrieve the list of RPPS asset types used in the EDI proposals                   ==
-- ==================================================================================================

select	oID	as item_key,
		description	as description,
		[default],
		ActiveFlag
FROM	RPPSAssetTypes
order by 1

return ( @@rowcount )
GO
