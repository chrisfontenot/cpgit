USE [DebtPlus]
GO
/****** Object:  StoredProcedure [dbo].[rpt_demographics_appt-nfcc_quarterly_rpt]    Script Date: 09/15/2014 13:13:33 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[rpt_demographics_appt-nfcc_quarterly_rpt] ( @FromDate as DateTime = NULL, @ToDate as DateTime = null, @appt_type as int = null, @region as int = null, @appt_class as varchar(10) = null ) as
-- =============================================================================================
-- ==            Demographics report                                                          ==
-- ==                                                                                         ==
-- ==   Warning: This procedure may take a significant amount (1 to 2 minutes)                ==
-- ==            of time to complete.                                                         ==
-- ==                                                                                         ==
-- =============================================================================================

-- ChangeLog
--   2/1/2002
--     Consider either from/to date as null to mean "all clients". Previously, it was only "from" date.
--   9/31/2002
--     Added support for client_creditor_balances table
--   6/7/2005
--     Added "region" to select clients
--   6/28/2006
--     Change dates to be effective only for the item. If a from date is given, then it limits clients
--     from that date. If a todate is given then it limits clients to that date. To get all clients, both
--     dates need to be missing. However, it is now easier to "understand" that these are limits rather
--     than before meaning that one or other will give you all clients if either is missing.
--	10/28/2009
--		Modified age ranges to match requirement for NFCC quarterly report
--	01/20/2011
--		Added appointment class parameter for NFCC quarterly report demographics

set nocount on
set ansi_warnings off

-- Used in calculating the current age
declare	@today	datetime
select	@today = getdate()

-- Create the result table
create table #results ( [active_status]	varchar(10) null,
			[group]		varchar(50) null,
			[description]	varchar(50) null,
			[value]		float );

-- =============================================================================================
-- ==               Create the list of clients in the desired target list                     ==
-- =============================================================================================

-- Create the table to hold the preliminary list of clients selected in the report
create table #demographics_client_list_pre ( client int, appt_class_name varchar(10));
create index temp_ix_demographics_client_list_pre_1 on #demographics_client_list_pre ( client );

-- Create the table to hold the list of clients selected in the report
create table #demographics_client_list ( client int, region int, active_status varchar(20), active_status_date datetime , appt_type int);
create index temp_ix_demographics_client_list_1 on #demographics_client_list ( client );
create index temp_ix_demographics_client_list_2 on #demographics_client_list ( region );
create index temp_ix_demographics_client_list_3 on #demographics_client_list ( active_status_date );

 if @appt_type is null
	select	@appt_type	= -1

-- Get the preliminary list of clients with the correct appointment types
if (@appt_class = 'fc')
begin
insert into #demographics_client_list_pre (client, appt_class_name)
select distinct client, @appt_class as appt_class_name
from client_appointments with (nolock)
where appt_type in ('1', '62', '2', '63', '56', '55', '4', '6', '64', '5', '65', '7', '78', '10', '77')
end
else
if (@appt_class = 'hs')
begin
insert into #demographics_client_list_pre (client, appt_class_name)
select distinct client, @appt_class as appt_class_name
from client_appointments with (nolock)
where appt_type in ('50', '74', '51', '59', '76', '58', '75', '85', '86')
end
else
if (@appt_class = 'rm')
begin
insert into #demographics_client_list_pre (client, appt_class_name)
select distinct client, @appt_class as appt_class_name
from client_appointments with (nolock)
where appt_type in ('66', '67', '61', '68', '87', '88')
end
else
if (@appt_class = 'pre-bk')
begin
insert into #demographics_client_list_pre (client, appt_class_name)
select distinct client, @appt_class as appt_class_name
from client_appointments with (nolock)
where appt_type in ('27', '71', '28', '72', '102')
end
else
if (@appt_class = 'sl')
begin
insert into #demographics_client_list_pre (client, appt_class_name)
select distinct client, @appt_class as appt_class_name
from client_appointments with (nolock)
where appt_type in ('107','108')
end
else
insert into #demographics_client_list_pre (client)
select distinct client
from client_appointments with (nolock)

insert into #demographics_client_list ( client, region, active_status, active_status_date )
select	pre.client as client, isnull(c.region,0) as region,
	case isnull(active_status,'CRE')
		when 'A' then 'A/APT/WKS/PND/RDY'
		when 'AR' then 'A/APT/WKS/PND/RDY'
		when 'APT' then 'A/APT/WKS/PND/RDY'
		when 'WKS' then 'A/APT/WKS/PND/RDY'
		when 'PND' then 'A/APT/WKS/PND/RDY'
		when 'RDY' then 'A/APT/WKS/PND/RDY'
		when 'CRE' then 'A/APT/WKS/PND/RDY'
		else isnull(active_status,'CRE')
	end,
	coalesce(c.active_status_date, c.date_created, '1/1/1900')

from	clients c with (nolock)
inner join #demographics_client_list_pre pre on pre.client = c.client
where c.active_status not in ('i','ex')


-- Discard clients outside the date range
if @FromDate is not null
begin
	select	@FromDate	= convert(datetime, convert(varchar(10), @FromDate, 101) + ' 00:00:00')

	delete
	from	#demographics_client_list
	where	active_status_date < @FromDate;
end

if @ToDate is not null
begin
	select	@ToDate		= convert(datetime, convert(varchar(10), @ToDate, 101) + ' 23:59:59')

	delete
	from	#demographics_client_list
	where	active_status_date > @ToDate;
end



-- Remove the clients for regions that we do not wish to examine
if @region is not null
	delete
	from	#demographics_client_list_pre
	where	region	<> @region

-- =============================================================================================
-- ==               Calculate the secured and unsecured debt by client                        ==
-- =============================================================================================
select	p.client, convert(float,sum(isnull(balance,0))) as 'secured', convert(float,0) as 'unsecured', convert(float,0) as total_creditors, convert(float,0) as dmp_creditors, convert(float,0) as orig_dmp_payment, convert(float,0) as other_debt
into	#demographics_debt_1
from	secured_loans l
inner join secured_properties p on l.secured_property = p.secured_property
inner join #demographics_client_list xx on p.client = xx.client
group by p.client

union all

select	xx.client, convert(float,sum(isnull(l.CurrentLoanBalanceAmt,0))) as 'secured', convert(float,0) as 'unsecured', convert(float,0) as total_creditors, convert(float,0) as dmp_creditors, convert(float,0) as orig_dmp_payment, convert(float,0) as other_debt
from	housing_loans l with (nolock)
inner join housing_properties p with (nolock) on l.PropertyID = p.oID
inner join #demographics_client_list xx on p.HousingID = xx.client
group by xx.client

union all

select	c.client, convert(float,0) as 'secured', convert(float,0) as 'unsecured', convert(float,count(*)) as 'total_creditors', convert(float,0) as 'dmp_creditors', sum(convert(float,orig_dmp_payment)) as orig_dmp_payment, convert(float,0) as other_debt
from	client_creditor c with (nolock)
inner join client_creditor_balances bal with (nolock) on c.client_creditor_balance = bal.client_creditor_balance
inner join #demographics_client_list xx on c.client = xx.client
where	isnull(bal.orig_balance,0)+isnull(bal.orig_balance_adjustment,0)+isnull(bal.total_interest,0) <= isnull(bal.total_payments,0)
and	c.reassigned_debt = 0
group by c.client

union all

select	l.client, convert(float,0) as 'secured', convert(float, sum(isnull(bal.orig_balance,0)+isnull(bal.orig_balance_adjustment,0)+isnull(bal.total_interest,0)-isnull(bal.total_payments,0))) as 'unsecured', convert(float,0) as total_creditors, convert(float,count(*)) as dmp_creditors, sum(convert(float,orig_dmp_payment)) as orig_dmp_payment, convert(float,0) as other_debt
from	client_creditor l with (nolock)
inner join client_creditor_balances bal with (nolock) on l.client_creditor_balance = bal.client_creditor_balance
inner join #demographics_client_list xx on l.client = xx.client
where	isnull(bal.orig_balance,0)+isnull(bal.orig_balance_adjustment,0)+isnull(bal.total_interest,0) > isnull(bal.total_payments,0)
and	l.reassigned_debt = 0
group by l.client

union all

select	l.client, convert(float,0) as 'secured', convert(float,0) as 'unsecured', convert(float,0) as total_creditors, convert(float,0) as dmp_creditors, convert(float,0) as orig_dmp_payment, sum(convert(float,balance)) as other_debt
from	client_other_debts l with (nolock)
left outer join #demographics_client_list xx on l.client = xx.client
group by l.client;




--select	l.client,  convert(float, sum(isnull(bal.orig_balance,0)+isnull(bal.orig_balance_adjustment,0)+isnull(bal.total_interest,0)-isnull(bal.total_payments,0))) as 'unsecured'
--from	client_creditor l with (nolock)
--inner join client_creditor_balances bal with (nolock) on l.client_creditor_balance = bal.client_creditor_balance
--inner join #demographics_client_list xx on l.client = xx.client
--where	isnull(bal.orig_balance,0)+isnull(bal.orig_balance_adjustment,0)+isnull(bal.total_interest,0) > isnull(bal.total_payments,0)
--and	l.reassigned_debt = 0
--group by l.client;


-- Merge the other income figure into the total debt and managed debt
select	client, sum(secured)+sum(unsecured)+sum(other_debt) as total_debt, sum(unsecured) as managed_debt, sum(total_creditors)+sum(dmp_creditors) as total_creditors, sum(dmp_creditors) as dmp_creditors, sum(orig_dmp_payment) as orig_dmp_payment
into	#demographics_debt_2
from	#demographics_debt_1
group by client;

-- =============================================================================================
-- ==               Generate a temporary table with the age breakdown by client               ==
-- =============================================================================================
select	xx.active_status					as 'active_status',
	case
		when p.birthdate is null			then 'Unspecified'
		when datediff(yy, p.birthdate, @today) < 18	then '00 to 18 years old'
		when datediff(yy, p.birthdate, @today) < 25	then '18 to 24 years old'
		when datediff(yy, p.birthdate, @today) < 35	then '25 to 34 years old'
		when datediff(yy, p.birthdate, @today) < 45	then '35 to 44 years old'
		when datediff(yy, p.birthdate, @today) < 55	then '45 to 54 years old'
		when datediff(yy, p.birthdate, @today) < 65	then '55 to 64 years old'
								else '65 years or older'
	end					as 'age_group'
	
into	#demographics_age
from	people p with (nolock)
inner join #demographics_client_list xx on p.client = xx.client
where	p.birthdate is not null;

-- =============================================================================================
-- ==               Generate a temporary table with the education breakdown by client         ==
-- =============================================================================================
--select	xx.active_status					as 'active_status',
--	case
--		when p.education < 9				then 'Grade School'
--		when p.education < 13				then 'High School'
--		when p.education < 14				then 'Some College'
--		when p.education < 16				then 'Associate Degree'
--		when p.education < 17				then 'Bachlor Degree'
--		when p.education < 18				then 'Master Degree'
--		when p.education < 20				then 'Doctorate'
--								else 'Post Doctorate'
--	end							as 'education_group'
	
--into	#demographics_education
--from	people p with (nolock)
--inner join #demographics_client_list xx on p.client = xx.client
--where	isnull(p.education,0) > 0

-- =============================================================================================
-- ==               Calculate the gross and net incomes by client                             ==
-- =============================================================================================
select	a.client, convert(float,sum(isnull(asset_amount,0))) as 'gross', convert(float,sum(isnull(asset_amount,0))) as 'net'
into	#demographics_income_1
from	assets a
inner join #demographics_client_list xx on a.client = xx.client
group by a.client

union all

select	p.client, convert(float, sum(isnull(gross_income,0))) as 'gross', convert(float,sum(isnull(net_income,0))) as 'net'
from	people p with (nolock)
inner join #demographics_client_list xx on p.client = xx.client
group by p.client;

-- Merge the other income figure into the gross/net values
select	client, sum(gross) as gross, sum(net) as net
into	#demographics_income_2
from	#demographics_income_1
group by client;

-- =============================================================================================
-- ==               Calculate the expected and actual deposit amounts                         ==
-- =============================================================================================

--select	d.client, sum(d.deposit_amount) as 'amount'
--into	#demographics_deposits_1
--from	client_deposits d
--inner join #demographics_client_list xx on d.client = xx.client
--where	isnull(d.one_time,0) = 0
--group by d.client;

--select	d.client, month(d.date_created) as 'month', year(d.date_created) as 'year', sum(d.credit_amt) as 'amount'
--into	#demographics_deposits_2
--from	registers_client d
--inner join #demographics_client_list xx on d.client = xx.client
--where	d.tran_type = 'DP'
--group by d.client, month(d.date_created), year(d.date_created);

-- =============================================================================================
-- ==               Calculate the total living expenses from the budgets                      ==
-- =============================================================================================

--select	b.client, max(b.date_created) as date_created, convert(int,0) as budget
--into #demographics_budgets_1
--from budgets b with (nolock)
--inner join #demographics_client_list xx on b.client = xx.client
--group by b.client;

--update #demographics_budgets_1
--set budget = budgets.budget
--from #demographics_budgets_1 a
--inner join budgets on a.client = budgets.client and a.date_created = budgets.date_created;

--select	a.client,
--	sum(case when cat.living_expense  = 1 then isnull(x.suggested_amount,0) else 0 end) as living_expense,
--	sum(case when cat.housing_expense = 1 then isnull(x.suggested_amount,0) else 0 end) as housing_expense
--into #demographics_budgets_2
--from budget_detail x with (nolock)
--inner join #demographics_budgets_1 a on a.budget = x.budget
--inner join budget_categories cat with (nolock) on x.budget_category = cat.budget_category
--group by a.client;

-- =============================================================================================
-- ==               Calculate the marital status by active class                              ==
-- =============================================================================================

insert into #results ([active_status], [group], [description], [value])
select	xx.active_status					as 'active_status',
	'01# POPULATION'						as 'group',
	'TOTAL # IN SURVEY'					as 'description',
	convert(float,count(c.active_status))			as 'value'
from	clients c with (nolock)
inner join #demographics_client_list xx on c.client = xx.client
group by xx.active_status;

-- =============================================================================================
-- ==               Calculate the average age for the clients                                 ==
-- =============================================================================================

insert into #results ([active_status], [group], [description], [value])
select	xx.active_status					as 'active_status',
	'02. AGE'						as 'group',
	'AVERAGE AGE'						as 'description',
	avg(convert(float, datediff(yy, p.birthdate, @today)))	as 'value'
from	people p with (nolock)
inner join #demographics_client_list xx on p.client = xx.client
where	p.birthdate is not null
group by xx.active_status;

-- =============================================================================================
-- ==               Calculate the age breakdown by active class                               ==
-- =============================================================================================

insert into #results ([active_status], [group], [description], [value])
select	active_status						as 'active_status',
	'03# AGE'						as 'group',
	age_group						as 'description',
	convert(float,count(*))					as 'value'
from	#demographics_age
group by active_status, age_group;

-- =============================================================================================
-- ==               Calculate the gender by active class                                      ==
-- =============================================================================================

insert into #results ([active_status], [group], [description], [value])
select	xx.active_status					as 'active_status',
	'04# GENDER'						as 'group',
	isnull(m.description,'Unspecified')			as 'description',
	convert(float,count(p.gender))				as 'value'
from	people p with (nolock)
inner join #demographics_client_list xx on p.client = xx.client
left outer join messages m with (nolock) on p.gender = m.item_value and 'GENDER' = m.item_type
where	p.gender is not null
group by xx.active_status, m.description;

-- =============================================================================================
-- ==               Calculate the marital status by active class                              ==
-- =============================================================================================

insert into #results ([active_status], [group], [description], [value])
select	xx.active_status					as 'active_status',
	'05# MARITAL'						as 'group',
	isnull(m.description,'Unspecified')			as 'description',
	convert(float,count(c.marital_status))			as 'value'
from	clients c with (nolock)
inner join #demographics_client_list xx on c.client = xx.client
left outer join messages m with (nolock) on c.marital_status = m.item_value and 'MARITAL' = m.item_type
where	c.marital_status is not null
group by xx.active_status, m.description;

-- =============================================================================================
-- ==               Calculate the ethnic backbround by active class                           ==
-- =============================================================================================

insert into #results ([active_status], [group], [description], [value])
select	xx.active_status					as 'active_status',
	'06# RACE'						as 'group',
	case isnull(m.description,'11. Unspecified')
	when 'caucasian' then '01. Caucasian'
	when 'hispanic' then '01. Caucasian'
	when 'middle eastern' then '01. Caucasian'
	when 'african-american' then '02. African-American'
	when 'asian' then '03. Asian'
	when 'indian' then '03. Asian'
	when 'American Indian' then '04. American Indian'
	when 'Hawiian/Pac Isle' then '05. Hawaiian/Pac Isle'
	when 'Amer Ind & Cauc' then '06. Amer Ind & Cauc'
	when 'Asian & Cauc' then '07. Asian & Cauc'
	when 'afr amer & cauc' then '08. Afr Amer & Cauc'
	when 'Amer Ind & Afr Amer' then '09. Amer Ind & Afr Amer'
	when 'Other Multi-Racial' then '10. Other Multi-Racial'
	else isnull(m.description,'11. Unspecified')
	end			as 'description',
	convert(float,count(p.race))				as 'value'
from	people p with (nolock)
inner join #demographics_client_list xx on p.client = xx.client
left outer join messages m with (nolock) on p.race = m.item_value and 'RACE' = m.item_type
where	p.race is not null
group by xx.active_status, m.description;

-- =============================================================================================
-- ==               Calculate the average # dependants by active class                        ==
-- =============================================================================================

--insert into #results ([active_status], [group], [description], [value])
--select	xx.active_status					as 'active_status',
--	'07a. DEPENDANTS'					as 'group',
--	'AVG. # OF DEPENDANTS'					as 'description',
--	avg(convert(float, case
--		when c.people < 1				then 0
--		when c.people < 2 and c.marital_status = 2	then 0
--		when c.marital_status = 2			then c.people - 2
--								else c.people - 1
--	end))							as 'value'
--from	clients c with (nolock)
--inner join #demographics_client_list xx on c.client = xx.client
--where	c.people is not null
--group by xx.active_status;

-- =============================================================================================
-- ==               Calculate the average # in household by active class                        ==
-- =============================================================================================

insert into #results ([active_status], [group], [description], [value])
select	xx.active_status					as 'active_status',
	'07b. HOUSEHOLD'					as 'group',
	'AVG. # OF PEOPLE IN HOUSEHOLD'					as 'description',
	avg(c.household)							as 'value'
from	clients c with (nolock)
inner join #demographics_client_list xx on c.client = xx.client
where	c.household is not null
group by xx.active_status;

-- =============================================================================================
-- ==               Calculate education level                                                 ==
-- =============================================================================================

--insert into #results ([active_status], [group], [description], [value])
--select	active_status						as 'active_status',
--	'08# EDUCATION'						as 'group',
--	education_group						as 'description',
--	convert(float,count(*))					as 'value'
--from	#demographics_education
--group by active_status, education_group;

-- =============================================================================================
-- ==               Job Category                                                              ==
-- =============================================================================================

insert into #results ([active_status], [group], [description], [value])
select	xx.active_status					as 'active_status',
	'09# JOB CATEGORY'					as 'group',
	case isnull(j.description, '04. Not Recorded')
	when 'College student (non-working)' then '01. Unemployed'
	when 'Unemployed' then '01. Unemployed'
	when 'homemaker' then '01. Unemployed'
	when 'clerical/office worker' then '02. Employed'
	when 'College student (full-time employment)' then '02. Employed'
	when 'college student (partial employment)' then '02. Employed'
	when 'Craftsman/Operator (mechanic, truck driver)' then '02. Employed'
	when 'Government/military' then '02. Employed'
	when 'Manager/Administrator/Owner' then '02. Employed'
	when 'Professional/Technical (doctor, lawyer, CPA)' then '02. Employed'
	when 'Sales (retail, real estate, automobile)' then '02. Employed'
	when 'Service worker (waiter, police, fire, etc.)' then '02. Employed'
	when 'Retired' then '03. Retired'
	when 'other' then '04. Not Recorded'
	when '' then '04. Not Recorded'
	else isnull(j.description, '04. Not Recorded')
	end
	as 'description',
	convert(float,count(*))					as 'value'
from	clients c with (nolock)
inner join #demographics_client_list xx on c.client = xx.client
inner join people p with (nolock) on xx.client = p.client
inner join job_descriptions j with (nolock) on p.job = j.job_description
group by xx.active_status, j.description;

-- =============================================================================================
-- ==               Return the average gross and net income                                   ==
-- =============================================================================================

insert into #results ([active_status], [group], [description], [value])
select	xx.active_status					as 'active_status',
	'10$ GROSS'						as 'group',
	'AVG ANNUAL GROSS INCOME'					as 'description',
	avg(x.gross * 12)						as 'value'
from	#demographics_income_2 x
inner join #demographics_client_list xx on x.client = xx.client
group by xx.active_status;

--insert into #results ([active_status], [group], [description], [value])
--select	xx.active_status					as 'active_status',
--	'11$ NET'						as 'group',
--	'AVG MO. NET INCOME'					as 'description',
--	avg(x.net)						as 'value'
--from	#demographics_income_2 x
--inner join #demographics_client_list xx on x.client = xx.client
--group by xx.active_status;

-- =============================================================================================
-- ==               Return the average total debt and managed debt                            ==
-- =============================================================================================

--insert into #results ([active_status], [group], [description], [value])
--select	xx.active_status					as 'active_status',
--	'12$ TOTAL DEBT'					as 'group',
--	'AVG. TOTAL OUTSTND. DEBT'				as 'description',
--	avg(x.total_debt)					as 'value'
--from	#demographics_debt_2 x
--inner join #demographics_client_list xx on x.client = xx.client
--group by xx.active_status;

--insert into #results ([active_status], [group], [description], [value])
--select	xx.active_status					as 'active_status',
--	'13. CREDITORS'						as 'group',
--	'AVG. TOTAL # CREDITORS'				as 'description',
--	avg(x.total_creditors)					as 'value'
--from	#demographics_debt_2 x
--inner join #demographics_client_list xx on x.client = xx.client
--group by xx.active_status;

insert into #results ([active_status], [group], [description], [value])

select	xx.active_status					as 'active_status',
	'14$ MANAGED DEBT'					as 'group',
	'AVG. TOTAL DEBT ON DMP'				as 'description',
	avg(x.managed_debt)					as 'value'
from	#demographics_debt_2 x
inner join #demographics_client_list xx on x.client = xx.client
group by xx.active_status;

insert into #results ([active_status], [group], [description], [value])
select	xx.active_status					as 'active_status',
	'15. CREDITORS'						as 'group',
	'AVG. # CREDITORS ON DMP'				as 'description',
	avg(x.dmp_creditors)					as 'value'
from	#demographics_debt_2 x
inner join #demographics_client_list xx on x.client = xx.client
group by xx.active_status;

-- =============================================================================================
-- ==               Find the average expected deposited amount                                ==
-- =============================================================================================

--insert into #results ([active_status], [group], [description], [value])
--select	xx.active_status					as 'active_status',
--	'16$ DEPOSITS'						as 'group',
--	'AVG MO. $ SCHLD FOR DEPOS'				as 'description',
--	avg(x.amount)						as 'value'
--from	#demographics_deposits_1 x
--inner join #demographics_client_list xx on x.client = xx.client
--group by xx.active_status;

-- =============================================================================================
-- ==               Include the average deposited amount                                      ==
-- =============================================================================================

--insert into #results ([active_status], [group], [description], [value])
--select	xx.active_status					as 'active_status',
--	'17$ DEPOSITS'						as 'group',
--	'AVG MO. $ DEPOSITED'					as 'description',
--	avg(x.amount)						as 'value'
--from	#demographics_deposits_2 x
--inner join #demographics_client_list xx on x.client = xx.client
--group by xx.active_status;

-- =============================================================================================
-- ==               Original monthly payment to gross income                                  ==
-- =============================================================================================

--insert into #results ([active_status], [group], [description], [value])
--select	xx.active_status							as 'active_status',
--	'18% SPECIAL'								as 'group',
--	'% ORIG MO PMT TO GROSS IN'						as 'description',
--	sum(isnull(a.orig_dmp_payment,0)) / sum(isnull(b.gross,0)) * 100.0	as 'value'
--from	#demographics_debt_2 a
--inner join #demographics_income_2 b on a.client = b.client
--inner join #demographics_client_list xx on a.client = xx.client
--where isnull(b.gross,0) > 0
--group by xx.active_status;

-- =============================================================================================
-- ==               monthly deposited amount to gross income                                  ==
-- =============================================================================================

--insert into #results ([active_status], [group], [description], [value])
--select	xx.active_status							as 'active_status',
--	'19% SPECIAL'								as 'group',
--	'% MO. $ DEP / GROSS INC.'						as 'description',
--	sum(isnull(a.amount,0)) / sum(isnull(b.gross,0)) * 100.0		as 'value'
--from	#demographics_deposits_2 a
--inner join #demographics_income_2 b on a.client = b.client
--inner join #demographics_client_list xx on a.client = xx.client
--where isnull(b.gross,0) > 0
--group by xx.active_status;

-- =============================================================================================
-- ==               average number of months on the DMP program                               ==
-- =============================================================================================

--insert into #results ([active_status], [group], [description], [value])
--select	xx.active_status					as 'active_status',
--	'20. MONTHS'						as 'group',
--	'AVG. # MOS. ON DMP'					as 'description',
--	avg (convert(float,isnull(c.program_months,0)))		as 'value'
--from	clients c
--inner join #demographics_client_list xx on c.client = xx.client
--group by xx.active_status;

-- =============================================================================================
-- ==               average living expenses                                                   ==
-- =============================================================================================

--insert into #results ([active_status], [group], [description], [value])
--select	xx.active_status					as 'active_status',
--	'21$ EXPENSES'						as 'group',
--	'AVG. MO. LIVING EXPENSES'				as 'description',
--	avg (isnull(x.living_expense,0))			as 'value'
--from	#demographics_budgets_2 x
--inner join #demographics_client_list xx on x.client = xx.client
--group by xx.active_status;

-- =============================================================================================
-- ==               average mortgage loan                                                     ==
-- =============================================================================================

--select	xx.client, xx.active_status, sum(convert(float,l.CurrentLoanBalanceAmt)) as amount_owed
--into	#demographics_mtg_loan
--from housing_properties p with (nolock)
--inner join housing_loans l with (nolock) on p.oID = l.PropertyID
--inner join #demographics_client_list xx on p.HousingID = xx.client
--where	l.CurrentLoanBalanceAmt > 0
--group by xx.client, xx.active_status;

--insert into #results ([active_status], [group], [description], [value])
--select	active_status					as 'active_status',
--	'22$ AVG MTG LOAN'				as 'group',
--	'AVG. MTG LOAN'					as 'description',
--	avg (amount_owed)				as 'value'
--from	#demographics_mtg_loan
--group by active_status;

--drop table #demographics_mtg_loan

-- =============================================================================================
-- ==               average auto loan                                                         ==
-- =============================================================================================

--select	p.client, xx.active_status, sum(convert(float,balance)) as amount_owed
--into	#demographics_auto_loan
--from	secured_loans l
--inner join secured_properties p on l.secured_property = p.secured_property
--inner join #demographics_client_list xx on p.client = xx.client
--inner join secured_types t on p.secured_type = t.secured_type
--where   l.balance is not null
--and     t.auto_home_other <> 'H'
--group by p.client, xx.active_status;

--insert into #results ([active_status], [group], [description], [value])
--select	active_status					as 'active_status',
--	'23$ AVG AUTO LOAN'				as 'group',
--	'AVG. AUTO LOAN'				as 'description',
--	avg (amount_owed)				as 'value'
--from	#demographics_auto_loan
--group by active_status;

-- =============================================================================================
-- ==               average monthly housing cost                                              ==
-- =============================================================================================

--insert into #results ([active_status], [group], [description], [value])
--select	xx.active_status				as 'active_status',
--	'24$ HOUSING'					as 'group',
--	'AVG. MO. HOUSING COST'				as 'description',
--	avg (isnull(x.housing_expense,0))		as 'value'
--from	#demographics_budgets_2 x
--inner join #demographics_client_list xx on x.client = xx.client
--group by xx.active_status;

-- =============================================================================================-- ==               Calculate the housing status by active class                              ==
-- =============================================================================================

--insert into #results ([active_status], [group], [description], [value])
--select	xx.active_status				as 'active_status',
--	'24# HOUSING'					as 'group',
--	isnull(m.description,'Unspecified')		as 'description',
--	convert(float,count(c.housing_status))		as 'value'
--from	clients c with (nolock)
--inner join #demographics_client_list xx on c.client = xx.client
--left outer join messages m with (nolock) on c.housing_status = m.item_value and 'HOUSING STATUS' = m.item_type
--where	c.housing_status is not null
--group by xx.active_status, m.description;

-- =============================================================================================
-- ==               Original Monthly payment to net income                                    ==
-- =============================================================================================
--insert into #results ([active_status], [group], [description], [value])
--select	xx.active_status							as 'active_status',
--	'24% SPECIAL'								as 'group',
--	'% ORIG MO PMT TO NET INC.'						as 'description',
--	avg(isnull(a.orig_dmp_payment,0) / isnull(b.net,0) * 100.0)		as 'value'
--from	#demographics_debt_2 a
--inner join #demographics_income_2 b on a.client = b.client
--inner join #demographics_client_list xx on a.client = xx.client
--where isnull(b.net,0) > 0
--group by xx.active_status;

-- =============================================================================================
-- ==               Original Monthly payment to net income                                    ==
-- =============================================================================================
--insert into #results ([active_status], [group], [description], [value])
--select	xx.active_status							as 'active_status',
--	'25% SPECIAL'								as 'group',
--	'% MO DMP PMT TO NET INC.'						as 'description',
--	avg(isnull(a.amount,0) / isnull(b.net,0) * 100.0)			as 'value'
--from	#demographics_deposits_1 a
--inner join #demographics_income_2 b on a.client = b.client
--inner join #demographics_client_list xx on a.client = xx.client
--where isnull(b.net,0) > 0
--group by xx.active_status;

-- =============================================================================================
-- ==               Original Monthly payment to gross income                                  ==
-- =============================================================================================
--insert into #results ([active_status], [group], [description], [value])
--select	xx.active_status							as 'active_status',
--	'26% SPECIAL'								as 'group',
--	'% MO DMP PMT TO GROSS INC'						as 'description',
--	avg(isnull(a.amount,0) / isnull(b.gross,0) * 100.0)			as 'value'
--from	#demographics_deposits_1 a
--inner join #demographics_income_2 b on a.client = b.client
--inner join #demographics_client_list xx on a.client = xx.client
--where isnull(b.gross,0) > 0
--group by xx.active_status;

-- =============================================================================================
-- ==               Housing cost to net income                                                ==
-- =============================================================================================
--insert into #results ([active_status], [group], [description], [value])
--select	xx.active_status							as 'active_status',
--	'27% SPECIAL'								as 'group',
--	'% HOUSING COST TO NET INC'						as 'description',
--	avg(isnull(a.housing_expense,0) / isnull(b.net,0) * 100.0)		as 'value'
--from	#demographics_budgets_2 a
--inner join #demographics_income_2 b on a.client = b.client
--inner join #demographics_client_list xx on a.client = xx.client
--where isnull(b.net,0) > 0
--group by xx.active_status;

-- =============================================================================================
-- ==               Housing cost to gross income                                              ==
-- =============================================================================================
--insert into #results ([active_status], [group], [description], [value])
--select	xx.active_status							as 'active_status',
--	'28% SPECIAL'								as 'group',
--	'% HOUSING COST TO GR INC.'						as 'description',
--	avg(isnull(a.housing_expense,0) / isnull(b.gross,0) * 100.0)		as 'value'
--from	#demographics_budgets_2 a
--inner join #demographics_income_2 b on a.client = b.client
--inner join #demographics_client_list xx on a.client = xx.client
--where isnull(b.gross,0) > 0
--group by xx.active_status;

-- =============================================================================================
-- ==               Calculate the referral by active class                                    ==
-- =============================================================================================

--insert into #results ([active_status], [group], [description], [value])
--select	xx.active_status				as 'active_status',
--	'29# REFERRAL'					as 'group',
--	isnull(m.description,'Unspecified')		as 'description',
--	convert(float,count(c.referred_by))		as 'value'
--from	clients c with (nolock)
--inner join #demographics_client_list xx on c.client = xx.client
--left outer join referred_by m with (nolock) on c.referred_by = m.referred_by
--where	c.referred_by is not null
--group by xx.active_status, m.description;

-- =============================================================================================
-- ==               Calculate the financial problem by active class                           ==
-- =============================================================================================

--insert into #results ([active_status], [group], [description], [value])
--select	xx.active_status				as 'active_status',
--	'30# FIN PROBLEM'				as 'group',
--	isnull(m.description,'Unspecified')		as 'description',
--	convert(float,count(c.cause_fin_problem1))	as 'value'
--from	clients c with (nolock)
--inner join #demographics_client_list xx on c.client = xx.client
--left outer join financial_problems m with (nolock) on c.cause_fin_problem1 = m.financial_problem
--where	c.cause_fin_problem1 is not null
--group by xx.active_status, m.description;

-- =============================================================================================
-- ==               Calculate the drop status by active class                                 ==
-- =============================================================================================

--insert into #results ([active_status], [group], [description], [value])
--select	xx.active_status				as 'active_status',
--	'31# DROP'					as 'group',
--	isnull(m.description,'Unspecified')		as 'description',
--	convert(float,count(*))				as 'value'
--from	clients c with (nolock)
--inner join #demographics_client_list xx on c.client = xx.client
--left outer join drop_reasons m with (nolock) on c.drop_reason = m.drop_reason
--where	(c.drop_reason is not null) or (c.drop_reason_other is not null)
--group by xx.active_status, m.description;

-- =============================================================================================
-- ==               Ensure that the age category is fully represented                         ==
-- =============================================================================================

insert into #results ([active_status], [group], [description], [value])
select	distinct x.active_status				as 'active_status',
	'02. AGE'						as 'group',
	'AVERAGE AGE'						as 'description',
	convert(float,0.0)					as 'value'
from	#results x;

insert into #results ([active_status], [group], [description], [value])
select	distinct x.active_status				as 'active_status',
	'03# AGE'						as 'group',
	'00 to 18 years old'					as 'description',
	convert(float,0.0)					as 'value'
from	#results x;
insert into #results ([active_status], [group], [description], [value])
select	distinct x.active_status				as 'active_status',
	'03# AGE'						as 'group',
	'18 to 24 years old'					as 'description',
	convert(float,0.0)					as 'value'
from	#results x;

insert into #results ([active_status], [group], [description], [value])
select	distinct x.active_status				as 'active_status',
	'03# AGE'						as 'group',
	'25 to 34 years old'					as 'description',
	convert(float,0.0)					as 'value'
from	#results x;

insert into #results ([active_status], [group], [description], [value])
select	distinct x.active_status				as 'active_status',
	'03# AGE'						as 'group',
	'35 to 44 years old'					as 'description',
	convert(float,0.0)					as 'value'
from	#results x;

insert into #results ([active_status], [group], [description], [value])
select	distinct x.active_status				as 'active_status',
	'03# AGE'						as 'group',
	'45 to 54 years old'					as 'description',
	convert(float,0.0)					as 'value'
from	#results x;

insert into #results ([active_status], [group], [description], [value])
select	distinct x.active_status				as 'active_status',
	'03# AGE'						as 'group',
	'55 to 64 years old'					as 'description',
	convert(float,0.0)					as 'value'
from	#results x;

insert into #results ([active_status], [group], [description], [value])
select	distinct x.active_status				as 'active_status',
	'03# AGE'						as 'group',
	'65 years or older'					as 'description',
	convert(float,0.0)					as 'value'
from	#results x;

insert into #results ([active_status], [group], [description], [value])
select	distinct x.active_status				as 'active_status',
	'04# GENDER'						as 'group',
	isnull(m.description,'Unspecified')			as 'description',
	convert(float,0.0)					as 'value'
from	#results x, messages m
where	m.item_type = 'GENDER';

insert into #results ([active_status], [group], [description], [value])
select	distinct x.active_status				as 'active_status',
	'05# MARITAL'						as 'group',
	isnull(m.description,'Unspecified')			as 'description',
	convert(float,0.0)					as 'value'
from	#results x, messages m
where	m.item_type = 'MARITAL';

insert into #results ([active_status], [group], [description], [value])
select	distinct x.active_status				as 'active_status',
	'06# RACE'						as 'group',
	case isnull(m.description,'11. Unspecified')
	when 'caucasian' then '01. Caucasian'
	when 'hispanic' then '01. Caucasian'
	when 'middle eastern' then '01. Caucasian'
	when 'african-american' then '02. African-American'
	when 'asian' then '03. Asian'
	when 'indian' then '03. Asian'
	when 'American Indian' then '04. American Indian'
	when 'Hawiian/Pac Isle' then '05. Hawaiian/Pac Isle'
	when 'Amer Ind & Cauc' then '06. Amer Ind & Cauc'
	when 'Asian & Cauc' then '07. Asian & Cauc'
	when 'afr amer & cauc' then '08. Afr Amer & Cauc'
	when 'Amer Ind & Afr Amer' then '09. Amer Ind & Afr Amer'
	when 'Other Multi-Racial' then '10. Other Multi-Racial'
	else isnull(m.description,'11. Unspecified')
	end			as 'description',
	convert(float,0.0)					as 'value'
from	#results x, messages m
where	m.item_type = 'RACE';

-- =============================================================================================
-- ==               Finally, return the results to the report                                 ==
-- =============================================================================================

select	[active_status]						as 'active_status',
	[group]							as 'group',
	[description]						as 'description',
	convert(varchar(30), sum(isnull([value],0.0)))		as 'mtd_value',
	convert(varchar(30), sum(isnull([value],0.0)))		as 'ytd_value'
from	#results
group by [active_status], [group], [description]
order by 1, 2, 3;

-- Discard the working tables
drop table #demographics_age;
drop table #demographics_income_1;
drop table #demographics_income_2;
drop table #demographics_debt_1;
drop table #demographics_debt_2;
drop table #demographics_deposits_1;
drop table #demographics_deposits_2;
drop table #demographics_budgets_1;
drop table #demographics_budgets_2;
drop table #demographics_auto_loan;
drop table #results;
drop table #demographics_client_list;
drop table #demographics_education
GO
