USE [DebtPlus]
GO
/****** Object:  StoredProcedure [dbo].[xpr_appt_workshop_book]    Script Date: 09/15/2014 13:13:34 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[xpr_appt_workshop_book] ( @client as typ_client, @workshop as typ_key, @attending as int, @referred_by as int = -1, @HousingFeeAmount as money = 0 ) AS

-- =======================================================================================================================
-- ==            book a client for attending a workshop                                                                 ==
-- =======================================================================================================================

-- ChangeLog
--   2/4/2011
--      Changed for new client_appointments table layout

-- Suppress the intermediate results
set nocount on

declare	@count_attending	int
declare	@max_attendance		int
declare @client_appointment	int

-- Attempt to book the workshop
begin transaction
set xact_abort on

-- Find the existing client appointment
select	@client_appointment	= client_appointment
from	client_appointments with (nolock)
where	client				= @client
and		workshop			= @workshop;

-- Fetch the workshop information
declare	@workshop_start_time	datetime
declare	@workshop_end_time		datetime
declare	@workshop_duration		int
select	@workshop_start_time	= w.start_time,
		@workshop_duration		= t.duration
from	workshops w with (nolock)
left outer join workshop_types t with (nolock) on w.workshop_type = t.workshop_type
where	w.workshop		= @workshop

if @@rowcount < 1
begin
	rollback transaction
	RaisError (50092, 16, 1, @workshop)
	return ( 0 )
end

-- Validate the number of people attending the workshop
if @attending < 0
begin
	rollback transaction
	RaisError (50091, 16, 1)
	return ( 0 )
end

-- Cancel the workshop booking if the count attending is now zero
if @attending = 0
begin
	if @client_appointment is not null
		update		client_appointments
		set			workshop_people		= 0,
					date_updated		= GETDATE(),
					status				= 'C'
		where		client_appointment	= @client_appointment;

	commit transaction
	return ( @client_appointment )
end

-- Ensure that the record exists in the appointment database
if @client_appointment is null
begin
	insert into client_appointments (client, workshop, [status], workshop_people, start_time, end_time)
	select		@client, @workshop, 'P', @count_attending, @workshop_start_time, DATEADD(minute, @workshop_duration, @workshop_start_time)
	select		@client_appointment	= SCOPE_IDENTITY()
end

-- Update the client appointment with the revised values
update	client_appointments
set		[status]				= 'P',
		workshop_people			= @attending,
		referred_by				= @referred_by,
		HousingFeeAmount		= @HousingFeeAmount,
		date_updated			= GETDATE()
where	client_appointment		= @client_appointment

-- Fetch the total number of peole attending the workshop
select	@count_attending		= sum(workshop_people)
from	client_appointments
where	workshop				= @workshop
and		status					= 'P'

-- If the count is more than the seating allowed then reverse the transaction and return failure
select	@max_attendance		= seats_available
from	workshops
where	workshop			= @workshop

-- If the count is too many then rollback the transaction and fail
if @count_attending > @max_attendance
begin
	rollback transaction
	raiserror (50047, 16, 1, @max_attendance)
	return ( 0 )
end

-- Take the client off "CRE" status when possible
update	clients
set		active_status		= 'WKS'
where	client				= @client
and		active_status		= 'CRE'

-- The operation was successful
commit transaction
return ( @client_appointment )
GO
