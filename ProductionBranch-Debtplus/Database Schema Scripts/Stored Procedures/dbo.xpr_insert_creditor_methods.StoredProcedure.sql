USE [DebtPlus]
GO
/****** Object:  StoredProcedure [dbo].[xpr_insert_creditor_methods]    Script Date: 09/15/2014 13:13:35 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[xpr_insert_creditor_methods] ( @creditor as int, @type as varchar(3) = 'EFT', @bank as int = 1, @region as int = 1, @rpps_biller_id as varchar(10) = null, @epay_biller_id as varchar(30) = null ) as
    -- ==========================================================================================================
    -- ==         Create a creditor method item for paying/sending creditor data                               ==
    -- ==========================================================================================================
    
    -- Allow empty strings to represent NULL for the validation rules
    if @rpps_biller_id = ''
        select @rpps_biller_id = null

    if @epay_biller_id = ''
        select @epay_biller_id = null
   
    insert into update_creditor_methods(creditor, [type], bank, region, rpps_biller_id, epay_biller_id) values (@creditor, @type, @bank, @region, @rpps_biller_id, @epay_biller_id)
    return (IDENT_CURRENT('creditor_methods'))
GO
