SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER procedure [cmd_Remove_Void] ( @checknum as bigint ) as

-- ===========================================================================================
-- ==            Reverse the void of a check                                                ==
-- ===========================================================================================

-- ChangeLog
--   9/31/2002
--     Added support for client_creditor_balances table
--   10/15/2002
--     Added support for client_creditor's "*_this_creditor" information
--   2/19/2004
--     Misc corrections.
--   5/22/06
--     Added support for client refund checks
--     Added support for deduct checks
--   06/09/2009
--      Remove references to debt_number and use client_creditor

set nocount on

-- Obtain the trust register from the voided check
declare @trust_register		int
declare	@invoice_register	int
declare	@client_register	int
declare	@check_amount		money
declare	@creditor		varchar(10)
declare	@client			int
declare	@void_date		datetime
declare	@void_creator		varchar(80)
declare	@tran_type		varchar(10)
declare	@deducted_amount	money
declare	@void_gross		money
declare	@void_deduct		money

print	'Check Number = ' + convert(varchar,@checknum)

select top 1
	@trust_register		= trust_register,
	@invoice_register	= invoice_register,
	@check_amount		= amount,
	@creditor		= creditor,
	@client			= client,
	@tran_type		= tran_type
from	registers_trust
where	checknum = @checknum
and	tran_type in ('AD', 'MD', 'CM','CR')
and	cleared = 'V'
order by date_created desc

if @trust_register is null
begin
	raiserror ('The check %s does not seem to be a voided check', 16, 1, @checknum)
	return ( 0 )
end

print	'Trust register = ' + convert(varchar, @trust_register)
print	'Check amount = $' + convert(varchar, @check_amount, 1)

-- Process a deduct check
if @tran_type = 'CM' and @creditor = 'Z9999'
begin
	-- Find the 'VR' transaction for the client
	begin transaction
	select	@client_register	= client_register,
		@void_gross		= credit_amt
	from	registers_client
	where	tran_type		= 'VR'
	and	client			= 0
	and	trust_register		= @trust_register

	-- Validate the amounts
	if @void_gross <> @check_amount
	begin
		rollback transaction
		RaisError ('The check amount does not match the refund value', 16, 1)
		return ( 0 )
	end

	-- Remove the void item from the client
	delete
	from	registers_client
	where	client_register		= @client_register

	-- Remove the money from the client
	update	clients
	set	held_in_trust		= held_in_trust - @check_amount
	where	client			= 0

	-- Correct the check's trust reigster figure
	update	registers_trust
	set	cleared		= ' ',
		reconciled_date	= null
	where	trust_register	= @trust_register

	commit transaction
	print	'Deduct check void removed.'

	return ( 1 )
end

-- Process a client refund check
if @tran_type	= 'CR'
begin
	-- Find the 'VR' transaction for the client
	begin transaction
	select	@client_register	= client_register,
		@void_gross		= credit_amt
	from	registers_client
	where	tran_type		= 'VR'
	and	client			= @client
	and	trust_register		= @trust_register

	-- Validate the amounts
	if @void_gross <> @check_amount
	begin
		rollback transaction
		RaisError ('The check amount does not match the client''s refund value', 16, 1)
		return ( 0 )
	end

	-- Remove the void item from the client
	delete
	from	registers_client
	where	client_register		= @client_register

	-- Remove the money from the client
	update	clients
	set	held_in_trust		= held_in_trust - @check_amount
	where	client			= @client

	-- Correct the check's trust reigster figure
	update	registers_trust
	set	cleared		= ' ',
		reconciled_date	= null
	where	trust_register	= @trust_register

	commit transaction
	print	'Client ID = ' + dbo.format_client_id ( @client )

	return ( 1 )
end

-- Find the detail transactions and re-pay them
-- Determine when the void was performed and who did it
select	@void_date = date_created,
	@void_creator = created_by
from	registers_client_creditor
where	trust_register = @trust_register
and	tran_type = 'VD';

-- Obtain the list of debts that were voided from the transaction
select	client,
	creditor,
	client_creditor,
	sum(credit_amt) as 'credit_amt',
	sum(case creditor_type when 'D' then fairshare_amt else 0 end) as 'deducted',
	sum(case creditor_type when 'B' then fairshare_amt else 0 end) as 'billed'
into	#unvoid_transactions
from	registers_client_creditor
where	trust_register = @trust_register
and	tran_type = 'VD'
group by client, creditor, client_creditor;

-- Print the information for the void operation
print	''
print	'Clients for the transactions:'
print	''

select	client, creditor, client_creditor as DebtID, credit_amt as 'gross', deducted, credit_amt - deducted as 'net', billed
from	#unvoid_transactions;

-- The creditor must be only the value indicated
if exists (select * from #unvoid_transactions where creditor != @creditor)
begin
	drop table #unvoid_transactions
	RaisError ('The voided transactions are for more than one creditor and may not be undone.', 16, 1)
	return ( 0 )
end

-- Verify that the gross is the proper amount
select	@void_gross = sum(credit_amt) - sum(deducted)
from	#unvoid_transactions;

if @void_gross != @check_amount
begin
	drop table #unvoid_transactions
	RaisError ('The check net amount does not match the trust register and may not be reversed.', 16, 1)
	return ( 0 )
end

begin transaction
set xact_abort on

-- Update the debt records to reflect the amount as being "re-paid"
update	client_creditor_balances
set	payments_month_0		= payments_month_0 + rcc.credit_amt,
	total_payments			= total_payments + rcc.credit_amt
from	client_creditor_balances bal
inner join client_creditor cc on bal.client_creditor_balance = cc.client_creditor_balance
inner join #unvoid_transactions rcc on cc.client_creditor = rcc.client_creditor;

-- Update the creditor statistics
update	client_creditor
set	returns_this_creditor		= isnull(cc.returns_this_creditor,0) - rcc.credit_amt
from	client_creditor cc
inner join #unvoid_transactions rcc on cc.client_creditor = rcc.client_creditor;

-- Update the creditors to indicate that the money was paid
update	creditors
set	distrib_mtd = distrib_mtd + rcc.credit_amt,
	contrib_mtd_received = contrib_mtd_received + rcc.deducted
from	creditors cr
inner join #unvoid_transactions rcc on cr.creditor = rcc.creditor

-- Determine the amount for each client
select	client, sum(credit_amt) as 'amount'
into	#unvoid_clients
from	#unvoid_transactions
group by client;

update	clients
set	held_in_trust = held_in_trust - x.amount
from	clients c
inner join #unvoid_clients x on c.client = x.client;

-- Remove the item from the client transactions
-- (This will also remove the client 0 transaction since it shares the trust register pointer.)
delete
from	registers_client
where	tran_type = 'VD'
and	trust_register = @trust_register;

-- Remove the item from the client/creditor transactions
delete
from	registers_client_creditor
where	tran_type = 'VD'
and	trust_register = @trust_register;

-- Remove the item from the creditor transactions
delete
from	registers_creditor
where	tran_type = 'VD'
and	creditor = @creditor
and	trust_register = @trust_register;

-- Refund the deduction amount
select	@deducted_amount = sum(deducted)
from	#unvoid_transactions;

if @deducted_amount > 0
begin
	update	clients
	set	held_in_trust = held_in_trust + @deducted_amount
	where	client = 0
end

-- Remove the void status from the detail items on the disbursement
update	registers_client_creditor
set	void = 0
where	tran_type in ('AD', 'BW', 'MD', 'CM')
and	trust_register = @trust_register;

-- Update the invoice to remove the adjustment if there is an invoice register
if @invoice_register is not null
begin
	update	registers_invoices
	set	adj_amount = adj_amount - (select sum(billed) from #unvoid_transactions)
	where	invoice_register = @invoice_register;

	update	registers_invoices
	set	adj_date = null
	where	invoice_register = @invoice_register
	and	adj_amount <= 0;

	delete
	from	registers_creditor
	where	tran_type = 'RA'
	and	invoice_register = @invoice_register
	and	date_created = @void_date
	and	created_by = @void_creator;
end

-- Finally, leave the check in a "pending" status
update	registers_trust
set	cleared = ' ',
	reconciled_date = null
where	trust_register = @trust_register;

-- Discard working table
drop table #unvoid_transactions
drop table #unvoid_clients

-- Commit the transactions
commit transaction
return ( 1 )
GO
