USE [DebtPlus]
GO
/****** Object:  StoredProcedure [dbo].[rpt_rpps_response_cdn]    Script Date: 09/15/2014 13:13:33 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE PROCEDURE [dbo].[rpt_rpps_response_cdn]  ( @rpps_response_file as int = null ) AS
-- =======================================================================================================
-- ==           Obtain the information for creditor messages                                            ==
-- =======================================================================================================

-- Suppress the count of records
set nocount on

-- Try to find the response file if one is not specified
if @rpps_response_file is null
begin
	select	@rpps_response_file = max(rpps_response_file)
	from	rpps_response_files
	where	date_created = (
		select	max(date_created)
		from	rpps_response_files
	)
end

-- Retrieve the information for the records
select	cc.client,
		dbo.format_reverse_name ( default, pn.first, default, pn.last, default ) as client_name,
		isnull(cc.creditor, d.creditor) as creditor,
		cr.creditor_name as creditor_name,
		d.account_number as account_number,
		d.message as message,
		d.trace_number
from	rpps_response_details_cdm d
left outer join client_creditor cc on d.client_creditor = cc.client_creditor
left outer join creditors cr on d.creditor = cr.creditor
left outer join people p on cc.client = p.client and 1 = p.relation
left outer join names pn on p.nameid = pn.name
where	d.rpps_response_file = @rpps_response_file

order by d.biller_id, d.trace_number, d.line_number

return ( @@rowcount )
GO
