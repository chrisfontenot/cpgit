USE [DebtPlus]
GO
/****** Object:  StoredProcedure [dbo].[xpr_insert_client_addkey]    Script Date: 09/15/2014 13:13:35 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[xpr_insert_client_addkey] (@client as int, @additional as varchar(50)) as

-- ===================================================================================
-- ==            Procedure for .NET interface to create a new row in client_addkeys ==
-- ===================================================================================

insert into client_addkeys(client,additional) values (@client,@additional)
return (scope_identity())
GO
