USE [DebtPlus]
GO
/****** Object:  StoredProcedure [dbo].[UPDATE_Add_System_Messages]    Script Date: 09/15/2014 13:13:34 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[UPDATE_Add_System_Messages] AS

-- ====================================================================================================================
-- ==            Add the system messages for the DebtPlus database                                                   ==
-- ====================================================================================================================

-- ChangeLog
--   09/04/2001
--      Corrected message 50018
--   02/14/2002
--      Added message 50093
--   11/20/2002
--     Changed to use RPPS' biller_aba number rather than rpps_biller_id
--   1/5/2009
--      Changed from using biller_aba to using rpps_biller_id

-- Do not abort the transaction on an error.
set xact_abort off

Execute sp_addmessage @replace='replace',@msgnum=50001,@severity=16,@msgtext=N'The check %s could not be located in the check register.',@lang='us_english';
Execute sp_addmessage @replace='replace',@msgnum=50002,@severity=16,@msgtext=N'The check %s is currently reconciled and may not be voided.',@lang='us_english';
Execute sp_addmessage @replace='replace',@msgnum=50003,@severity=16,@msgtext=N'The check %s has currently cleared the bank and may not be voided.',@lang='us_english';
Execute sp_addmessage @replace='replace',@msgnum=50004,@severity=16,@msgtext=N'The check %s is already void.',@lang='us_english';
Execute sp_addmessage @replace='replace',@msgnum=50005,@severity=11,@msgtext=N'The budget %d is not a valid budget ID in the budgets table',@lang='us_english';
Execute sp_addmessage @replace='replace',@msgnum=50006,@severity=16,@msgtext=N'The batch could not be located from the list of un-posted items. Please supply the proper batch ID.',@lang='us_english';
Execute sp_addmessage @replace='replace',@msgnum=50007,@severity=16,@msgtext=N'The starting sequence (%d) is not valid',@lang='us_english';
Execute sp_addmessage @replace='replace',@msgnum=50008,@severity=16,@msgtext=N'The ending sequence (%d) is not valid',@lang='us_english';
Execute sp_addmessage @replace='replace',@msgnum=50009,@severity=16,@msgtext=N'The ending sequence (%d) must be greater than or equal to the first sequence (@d)',@lang='us_english';
Execute sp_addmessage @replace='replace',@msgnum=50010,@severity=16,@msgtext=N'The confirmation status (%d) is not valid for appointment %d.',@lang='us_english';
Execute sp_addmessage @replace='replace',@msgnum=50011,@severity=16,@msgtext=N'The client could not be determined from the appointment (%d).',@lang='us_english';
Execute sp_addmessage @replace='replace',@msgnum=50012,@severity=14,@msgtext=N'You do not have permission to delete the secured loans table for item %d.',@lang='us_english';
Execute sp_addmessage @replace='replace',@msgnum=50013,@severity=14,@msgtext=N'You do not have permission to update the secured loans table for item %d.',@lang='us_english';
Execute sp_addmessage @replace='replace',@msgnum=50014,@severity=11,@msgtext=N'The client number (%d) does not exist in the database',@lang='us_english';
Execute sp_addmessage @replace='replace',@msgnum=50015,@severity=16,@msgtext=N'The indicated person (%d) is ouside the allowable range 1..100',@lang='us_english';
Execute sp_addmessage @replace='replace',@msgnum=50016,@severity=16,@msgtext=N'The batch does not contain any postable items.',@lang='us_english';
Execute sp_addmessage @replace='replace',@msgnum=50017,@severity=11,@msgtext=N'The batch %d does not exist. It may not be deleted.',@lang='us_english';
Execute sp_addmessage @replace='replace',@msgnum=50018,@severity=16,@msgtext=N'The batch %d is currently open and may not be deleted until it is closed.',@lang='us_english';
Execute sp_addmessage @replace='replace',@msgnum=50019,@severity=16,@msgtext=N'The transaction amount is not valid.',@lang='us_english';
Execute sp_addmessage @replace='replace',@msgnum=50020,@severity=16,@msgtext=N'An empty description may not be inserted into the database.',@lang='us_english';
Execute sp_addmessage @replace='replace',@msgnum=50021,@severity=16,@msgtext=N'The transaction type (%s) is not valid.',@lang='us_english';
Execute sp_addmessage @replace='replace',@msgnum=50022,@severity=16,@msgtext=N'The reconcilation operation is not valid.',@lang='us_english';
Execute sp_addmessage @replace='replace',@msgnum=50023,@severity=11,@msgtext=N'The RPPS trace number %s could not be located in the transaction table.',@lang='us_english';
Execute sp_addmessage @replace='replace',@msgnum=50024,@severity=16,@msgtext=N'The cleared status (%s) for the check is not valid.',@lang='us_english';
Execute sp_addmessage @replace='replace',@msgnum=50025,@severity=8,@msgtext=N'Invoices should not be discarded less than 6 months old.',@lang='us_english';
Execute sp_addmessage @replace='replace',@msgnum=50026,@severity=10,@msgtext=N'There are no creditor billing cycles defined to generate invoices',@lang='us_english';
Execute sp_addmessage @replace='replace',@msgnum=50027,@severity=10,@msgtext=N'There are no creditors in the selection criteria that you have chosen.',@lang='us_english';
Execute sp_addmessage @replace='replace',@msgnum=50028,@severity=11,@msgtext=N'The action plan %d does not exist in the system',@lang='us_english';
Execute sp_addmessage @replace='replace',@msgnum=50029,@severity=11,@msgtext=N'The creditor ''%s'' does not exist in the system',@lang='us_english';
Execute sp_addmessage @replace='replace',@msgnum=50030,@severity=17,@msgtext=N'There are already too many debts for %d*%s*%d so a new debt can not be determined',@lang='us_english';
Execute sp_addmessage @replace='replace',@msgnum=50031,@severity=11,@msgtext=N'The file ID %d is not defined in the rpps_files table',@lang='us_english';
Execute sp_addmessage @replace='replace',@msgnum=50032,@severity=11,@msgtext=N'The ACH file could not be determined from the ach_files table.',@lang='us_english';
Execute sp_addmessage @replace='replace',@msgnum=50033,@severity=11,@msgtext=N'The disbursement batch could not be determined.',@lang='us_english';
Execute sp_addmessage @replace='replace',@msgnum=50034,@severity=11,@msgtext=N'There are no disbursment batches. Please run the disbursement process first.',@lang='us_english';
Execute sp_addmessage @replace='replace',@msgnum=50035,@severity=8,@msgtext=N'The disbursement date could not be determined',@lang='us_english';
Execute sp_addmessage @replace='replace',@msgnum=50036,@severity=8,@msgtext=N'The marker string %s is not valid.',@lang='us_english';
Execute sp_addmessage @replace='replace',@msgnum=50037,@severity=11,@msgtext=N'Client %d does not have a budget to calculate expenses.',@lang='us_english';
Execute sp_addmessage @replace='replace',@msgnum=50038,@severity=11,@msgtext=N'The debt can not be determined from the proposal record %d',@lang='us_english';
Execute sp_addmessage @replace='replace',@msgnum=50039,@severity=11,@msgtext=N'The debt information could not be determined from the proposal record (%d).',@lang='us_english';
Execute sp_addmessage @replace='replace',@msgnum=50040,@severity=11,@msgtext=N'The client could not be determined from the debt record (%d).',@lang='us_english';
Execute sp_addmessage @replace='replace',@msgnum=50041,@severity=8,@msgtext=N'The RPPS File number could not be determined from the outstanding items. Please specify one.',@lang='us_english';
Execute sp_addmessage @replace='replace',@msgnum=50042,@severity=11,@msgtext=N'The creditor could not be determined from the invoice number %d',@lang='us_english';
Execute sp_addmessage @replace='replace',@msgnum=50043,@severity=16,@msgtext=N'The appointment which you have requested was removed from the system. Please schedule a new time.',@lang='us_english';
Execute sp_addmessage @replace='replace',@msgnum=50044,@severity=16,@msgtext=N'The counselor is presently booked for the time period. Please choose a new time period.',@lang='us_english';
Execute sp_addmessage @replace='replace',@msgnum=50045,@severity=10,@msgtext=N'The client could not be determined from the appointment to be cancelled (%d).',@lang='us_english';
Execute sp_addmessage @replace='replace',@msgnum=50046,@severity=8,@msgtext=N'The appointment (%d) is no longer pending and may not be cancelled.',@lang='us_english';
Execute sp_addmessage @replace='replace',@msgnum=50047,@severity=8,@msgtext=N'The workshop booking could not be confirmed. The maximum attendance is %d.',@lang='us_english';
Execute sp_addmessage @replace='replace',@msgnum=50048,@severity=8,@msgtext=N'There are no empty seats available for this workshop at this time.',@lang='us_english';
Execute sp_addmessage @replace='replace',@msgnum=50049,@severity=14,@msgtext=N'%s is not authorized to delete client note #%d at the present time.',@lang='us_english',@with_log='True';
Execute sp_addmessage @replace='replace',@msgnum=50050,@severity=16,@msgtext=N'The note (%d) is no longer present on the system to be deleted.',@lang='us_english';
Execute sp_addmessage @replace='replace',@msgnum=50051,@severity=16,@msgtext=N'The client currently has a fee creditor (%s). Please do not generate multiple fee accounts for this client.',@lang='us_english';
Execute sp_addmessage @replace='replace',@msgnum=50052,@severity=16,@msgtext=N'The proposal status is not valid.',@lang='us_english';
Execute sp_addmessage @replace='replace',@msgnum=50053,@severity=16,@msgtext=N'The debt record does not exist for the requested proposal record.',@lang='us_english';
Execute sp_addmessage @replace='replace',@msgnum=50054,@severity=16,@msgtext=N'The debt record (%d) could not be located.',@lang='us_english';
Execute sp_addmessage @replace='replace',@msgnum=50055,@severity=16,@msgtext=N'You may not change account numbers on reassigned debts.',@lang='us_english';
Execute sp_addmessage @replace='replace',@msgnum=50056,@severity=16,@msgtext=N'The %s status value is not zero nor one for record %d.',@lang='us_english';
Execute sp_addmessage @replace='replace',@msgnum=50057,@severity=16,@msgtext=N'The flag %s is not valid for the xpr_debt_flags procedure.',@lang='us_english';
Execute sp_addmessage @replace='replace',@msgnum=50058,@severity=11,@msgtext=N'The disbursement date (%d) is not valid. There are no clients with a disbursement date.',@lang='us_english';
Execute sp_addmessage @replace='replace',@msgnum=50059,@severity=16,@msgtext=N'The disbursement date (%d) is not valid. There are clients with disbursement date(s) of %s.',@lang='us_english';
Execute sp_addmessage @replace='replace',@msgnum=50060,@severity=8,@msgtext=N'The client %d is not active.',@lang='us_english';
Execute sp_addmessage @replace='replace',@msgnum=50061,@severity=11,@msgtext=N'The account number is missing from %d*%s*%d.',@lang='us_english';
Execute sp_addmessage @replace='replace',@msgnum=50062,@severity=16,@msgtext=N'The fairshare associated with debt %d*%s*%d does not match the fairshare amount entered. The disbursement is not performed.',@lang='us_english';
Execute sp_addmessage @replace='replace',@msgnum=50063,@severity=16,@msgtext=N'The client %d currently has a negative trust balance and may not be disbursed.',@lang='us_english';
Execute sp_addmessage @replace='replace',@msgnum=50064,@severity=17,@msgtext=N'The client %d no longer has the necessary funds in the trust.',@lang='us_english';
Execute sp_addmessage @replace='replace',@msgnum=50065,@severity=11,@msgtext=N'The proposal batch does not exist in the system at this time.',@lang='us_english';
Execute sp_addmessage @replace='replace',@msgnum=50066,@severity=16,@msgtext=N'The batch is not open and may not be closed again.',@lang='us_english';
Execute sp_addmessage @replace='replace',@msgnum=50067,@severity=10,@msgtext=N'The reversal biller ID for creditor %s could not be determined from the rpps_biller_ids table.',@lang='us_english',@with_log='True';
Execute sp_addmessage @replace='replace',@msgnum=50068,@severity=11,@msgtext=N'The proposal associated with the transaction %s could not be located',@lang='us_english';
Execute sp_addmessage @replace='replace',@msgnum=50069,@severity=16,@msgtext=N'The creditor %s is marked as ''prohibit-use'' and may not be used.',@lang='us_english';
Execute sp_addmessage @replace='replace',@msgnum=50070,@severity=16,@msgtext=N'The debt has already been reassigned and may not be reassigned again.',@lang='us_english';
Execute sp_addmessage @replace='replace',@msgnum=50071,@severity=16,@msgtext=N'The creditor does not have a balance. The debt can not be reassigned without a balance.',@lang='us_english';
Execute sp_addmessage @replace='replace',@msgnum=50072,@severity=10,@msgtext=N'The maximum number of debts for this creditor has been reached. You can not reassign this debt for this creditor',@lang='us_english';
Execute sp_addmessage @replace='replace',@msgnum=50073,@severity=16,@msgtext=N'The RPPS transaction %s is not voided and can not be reversed at this time.',@lang='us_english';
Execute sp_addmessage @replace='replace',@msgnum=50074,@severity=10,@msgtext=N'The detail item for the reversal could not be located. The operation is aborted',@lang='us_english';
Execute sp_addmessage @replace='replace',@msgnum=50075,@severity=16,@msgtext=N'The debt number must be between 1 and 9',@lang='us_english';
Execute sp_addmessage @replace='replace',@msgnum=50076,@severity=11,@msgtext=N'The debt record %d*%s*%d could not be located in the system.',@lang='us_english';
Execute sp_addmessage @replace='replace',@msgnum=50077,@severity=11,@msgtext=N'There are no pending disbursement batches.',@lang='us_english';
Execute sp_addmessage @replace='replace',@msgnum=50078,@severity=10,@msgtext=N'The check is drawn for $%s but the disbursements show it to be for $%s. The check amounts don''t balance and may not be voided.',@lang='us_english';
Execute sp_addmessage @replace='replace',@msgnum=50079,@severity=16,@msgtext=N'The transaction may not be voided at this time.',@lang='us_english';
Execute sp_addmessage @replace='replace',@msgnum=50080,@severity=20,@msgtext=N'The transaction type is not valid for this operation.',@lang='us_english',@with_log='True';
Execute sp_addmessage @replace='replace',@msgnum=50081,@severity=20,@msgtext=N'The client update of the trust balance for client %d resulted in the wrong number of clients being updated.',@lang='us_english',@with_log='True';
Execute sp_addmessage @replace='replace',@msgnum=50082,@severity=10,@msgtext=N'The batch is not closed. Please close the batch.',@lang='us_english';
Execute sp_addmessage @replace='replace',@msgnum=50083,@severity=16,@msgtext=N'The batch was posted on %s and may not be posted again.',@lang='us_english';
Execute sp_addmessage @replace='replace',@msgnum=50084,@severity=10,@msgtext=N'There is no money associated with the deposit batch %d.',@lang='us_english';
Execute sp_addmessage @replace='replace',@msgnum=50085,@severity=16,@msgtext=N'The transaction code associated with the transaction %s is not valid for the xpr_rps_reject_payment procedure.',@lang='us_english';
Execute sp_addmessage @replace='replace',@msgnum=50086,@severity=11,@msgtext=N'The disbursement information for transaction %s is not in the disbursement detail table',@lang='us_english';
Execute sp_addmessage @replace='replace',@msgnum=50087,@severity=14,@msgtext=N'You do not have permission to delete a creditor to which disbursements have been made.',@lang='us_english';
Execute sp_addmessage @replace='replace',@msgnum=50088,@severity=11,@msgtext=N'The creditor refund batch %d could not be found or has been closed',@lang='us_english';
Execute sp_addmessage @replace='replace',@msgnum=50089,@severity=11,@msgtext=N'The registers_client transaction %d does not exist.',@lang='us_english';
Execute sp_addmessage @replace='replace',@msgnum=50090,@severity=11,@msgtext=N'The registers_creditor transaction %d does not exist.',@lang='us_english';
Execute sp_addmessage @replace='replace',@msgnum=50091,@severity=11,@msgtext=N'The number of people attending the workshop is not valid.',@lang='us_english';
Execute sp_addmessage @replace='replace',@msgnum=50092,@severity=11,@msgtext=N'The workshop (%d) is not defined in the system.',@lang='us_english';
Execute sp_addmessage @replace='replace',@msgnum=50093,@severity=11,@msgtext=N'The transaction %s could not be found in the pending ACH transactions.',@lang='us_english';
GO
