USE [DebtPlus]
GO
/****** Object:  StoredProcedure [dbo].[lst_menus]    Script Date: 09/15/2014 13:13:32 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[lst_menus] ( @current_user as varchar(80) = null ) AS

-- ChangeLog
--   2/18/2008
--      Support for .NET

-- suppress intermediate result sets
set nocount on

-- Fetch the level for the current user
declare	@menu_level	int

-- Fetch the level for the user
if @current_user is null
	select	@current_user	= suser_sname()

select	@menu_level	= menu_level
from	counselors with (nolock)
where	[person]	= @current_user

-- Assume that the user is a "general" low-life user if it is not in the tables
if @menu_level is null
	set @menu_level = 9

-- Retrieve the menu items from the database for this user
SELECT	seq								as 'seq',
		parentseq						as 'parentseq',
		isnull(attributes,0)			as 'attributes',
		menu_level						as 'menu_level',
		isnull([text],'')				as 'text',
		type							as 'type',
		argument						as 'argument',
		created_by						as 'created_by',
		date_created					as 'date_created',
		isnull(dotnet_program_key,'')	as 'program_key'
FROM	menus WITH (NOLOCK)
where	menu_level >= @menu_level
ORDER BY seq

RETURN ( @@rowcount )
GO
