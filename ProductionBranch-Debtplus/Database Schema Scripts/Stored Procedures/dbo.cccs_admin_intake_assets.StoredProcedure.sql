USE [DebtPlus]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER PROCEDURE [dbo].[cccs_admin_intake_assets] ( @application_id int, @client int = null ) as

set nocount on

-- Find the client if one was not given to us
if @client is null
	select	client	= ForeignDatabaseID
	from	cccs_admin..applications
	where	application_id = @application_id
	
if @client is null
begin
	RaisError('The client is null and can not be determined', 16, 1)
	return ( -1 )
end

-- Find the other monthly income figure from the budgets table
declare	@monthly_income_other	money
select	@monthly_income_other = monthly_income_other
from	cccs_admin..budgets
where	application_id		  = @application_id

-- Ensure that the value is not empty. (There may not be a budget record.)
if @monthly_income_other is null
	select	@monthly_income_other = 0
	
-- Update the "other" income category figure
declare	@asset_id		int
select	@asset_id	= asset_id
from	asset_ids
where	[description] like '%other%';

if @asset_id is null
	select	@asset_id	= 8  -- This is the "other" category

if not exists ( select * from assets where client = @client AND asset_id = @asset_id)
	insert into assets ( client, asset_id, amount, frequency ) values ( @client, @asset_id, 0, 4)
	
update  assets
set		amount			= isnull(@monthly_income_other,0),
		frequency		= 4
where	client			= @client
and		asset_id		= @asset_id;

return ( 1 )
GO
execute CMD_Set_Permissions_cccs_admin
go
