USE [DebtPlus]
GO
/****** Object:  StoredProcedure [dbo].[xpr_ACH_Generate_Prenote]    Script Date: 09/15/2014 13:13:34 ******/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE PROCEDURE [dbo].[xpr_ACH_Generate_Prenote] ( @ach_file as int, @bank as int = 0 ) AS
-- ========================================================================================
-- ==           Generate the transactions to sweep the funds into the ACH table          ==
-- ========================================================================================

-- ChangeLog
--   12/8/2001
--     Added ach_file in preparation for electronic check transactions
--   3/11/2002
--     Prenote "RDY", and "PRO" active status clients as well as "A" and "AR".
--   3/12/2002
--     Create a temporary table to hold the prenote information to ensure that the clients are marked properly.
--   11/24/2002
--     Added bank reference in preparation for identifying clients by ACH banking reference information

-- Do not generate intermediate result sets
set nocount on
declare @result	int

-- Select the clients to be prenoted. Only those in this list will be prenoted.
select	case ltrim(rtrim(isnull(ach.CheckingSavings,'C')))
		when 'S' then 38
			 else 28
	end								as transaction_code,
	left(ltrim(rtrim(replace(ach.ABA,' ',''))),9)	as routing_number,
	left(ltrim(rtrim(replace(ach.AccountNumber,' ',''))), 17)		as account_number,
	0								as amount,
	c.client								as client,
	@ach_file							as deposit_batch_id
INTO	#prenotes
FROM	clients c
inner join client_ach ach with (nolock) on c.client = ach.client
WHERE	ach.isActive = 1
AND		ach.ErrorDate		IS NULL
AND		ach.PrenoteDate		IS NULL
AND		ach.ABA				IS NOT NULL
AND		ach.AccountNumber	IS NOT NULL
AND		c.active_status in ('A', 'AR', 'PRO', 'RDY');

-- Insert the transactions
insert into deposit_batch_details (ach_transaction_code, ach_routing_number, ach_account_number, amount, client, deposit_batch_id, tran_subtype)
select	transaction_code, routing_number, account_number, amount, client, deposit_batch_id, 'AC'
from	#prenotes;

-- Result of the function is the number of items added to the table.
select	@result = @@rowcount

-- Mark the clients extracted as having been prenoted
update	client_ach
set		PrenoteDate = getdate()
from	client_ach ach
inner join #prenotes x on ach.client = x.client
where	PrenoteDate is null;

-- Drop the working table
drop table #prenotes;

-- Return the number of items inserted
return ( @result )
GO
