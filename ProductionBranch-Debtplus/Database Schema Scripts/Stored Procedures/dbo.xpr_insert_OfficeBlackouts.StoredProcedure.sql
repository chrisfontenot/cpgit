USE [DebtPlus]
GO
/****** Object:  StoredProcedure [dbo].[xpr_insert_OfficeBlackouts]    Script Date: 09/15/2014 13:13:35 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[xpr_insert_OfficeBlackouts] ( @Office as typ_key = 0, @DOW as int = null, @Date as datetime = null, @StartTime as int = null, @Duration as int = 0, @Description as varchar(50) = null ) as
-- ========================================================================================
-- ==   Procedure for .NET to insert items into the OfficeBlackouts table                ==
-- ========================================================================================
	insert into OfficeBlackouts ( [Office], [DOW], [Date], [StartTime], [Duration] ) select @Office, @DOW, convert(varchar(10),@Date,101), @StartTime, @Duration
	return ( scope_identity() )
GO
