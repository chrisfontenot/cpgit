USE [DebtPlus]
GO
/****** Object:  StoredProcedure [dbo].[www_intake_update_budget]    Script Date: 09/15/2014 13:13:34 ******/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE PROCEDURE [dbo].[www_intake_update_budget] ( @intake_client as int, @intake_id as varchar(20), @budget_category as int, @client_amount as money, @other_description as varchar(80)) as

-- ==============================================================================================================
-- ==            Start the processing on the new expense information                                           ==
-- ==============================================================================================================

-- Set the proper indicator flags
set nocount	on
set xact_abort	on

if not exists (select	*
		from	intake_clients
		where	intake_client	= @intake_client
		and	intake_id	= @intake_id)
begin
	RaisError ('The client is not in the system', 16, 1, @intake_client)
	return ( 0 )
end

-- Remove leading/trailing blanks from the description.
select	@other_description	= ltrim(rtrim(@other_description))
if @other_description = ''
	select @other_description = null

-- Update the client with the information
update	intake_budgets

set	client_amount		= @client_amount,
	other_budget_category	= @other_description
where	intake_client		= @intake_client
and	budget_category		= @budget_category

-- Verify that the client existed with the appropriate ID
if @@rowcount < 1
begin
	insert into intake_budgets (intake_client,  budget_category,  client_amount,  other_budget_category)
	values			(  @intake_client, @budget_category, @client_amount, @other_description)
end

if @@rowcount < 1
begin
	RaisError ('The client ID is not valid', 16, 1)
	return ( 0 )
end

return ( 1 )
GO
