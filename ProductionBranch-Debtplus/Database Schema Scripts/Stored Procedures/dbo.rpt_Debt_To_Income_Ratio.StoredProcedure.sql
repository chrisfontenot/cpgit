USE [DebtPlus]
GO
/****** Object:  StoredProcedure [dbo].[rpt_Debt_To_Income_Ratio]    Script Date: 09/15/2014 13:13:33 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE PROCEDURE [dbo].[rpt_Debt_To_Income_Ratio] (@Client# as int) As
/*--------------------------------------------------------------------------------------------------------
--	Client's 20% and 40% Debt to Income Ratio
--	Created by:  Ron Kinney, Talisen Technologies, 314-317-7683
--	Creation Date:  6/24/03
--	Modifications
--		By:
--		Reason:
--		Date:
--
--------------------------------------------------------------------------------------------------------*/

/* This whole procedure needs to be rewritten if the report us used again
Set nocount on

If 	@client# is not null

Begin	

SELECT pp.client,
               pp.name             AS 'Name',
               pni.net_income   AS 'Net Income',
               coi.oi                   AS 'Other Income',
               pmr.mmp             AS 'Mortgage/Rent',
               m2m.m2              AS 'Second Morgage',
               clp.cclp              AS 'Monthly Credit Card & Loan Payment',
               cdp.odp	  AS 'Other Debt Payment'
FROM (SELECT p.client, (p.first +' '+ p.Last) AS name
             FROM people p
             WHERE p.relation = 1
             GROUP BY p.client, (p.first +' '+ p.Last)) pp

    LEFT OUTER JOIN (SELECT cc.client, SUM(sd.plan_payment) AS cclp
                                     FROM client_creditor cc, sales_debts sd
                                     WHERE sd.client_creditor = cc.client_creditor
                                     GROUP BY cc.client) clp
     ON (pp.client = clp.client)

     LEFT OUTER JOIN (SELECT cd.client, SUM(cd.payment) AS odp
                                       FROM client_other_debts cd
                                       GROUP BY cd.client) cdp
     ON (pp.client = cdp.client)

     LEFT OUTER JOIN (SELECT ppi.client, SUM(ppi.net_income) AS net_income
                                      FROM people ppi
                                      GROUP BY ppi.client) pni
     ON (pp.client = pni.client)

     LEFT OUTER JOIN (SELECT a.client, SUM(a.asset_amount) AS oi
                                      FROM assets a
                                      GROUP BY a.client) coi
     ON (pp.client = coi.client)

     LEFT OUTER JOIN (SELECT SUM(bd.client_amount) AS m2, lb.budget, lb.client 
                                      FROM budget_detail bd RIGHT OUTER JOIN (SELECT client, MAX(budget) AS budget
                                                                                                                 FROM budgets
                                                                                                                 GROUP BY client) lb
                                                                            ON  (bd.budget = lb.budget)
                                      WHERE bd.budget_category = 1030
                                      GROUP BY lb.budget, lb.client) m2m
     ON (pp.client = m2m.client)

     LEFT OUTER JOIN (SELECT SUM(bd.client_amount) AS mmp, lb.client
                                      FROM budget_detail bd RIGHT OUTER JOIN (SELECT client, MAX(budget) AS budget
                                                                                                                 FROM budgets
                                                                                                                 GROUP BY client) lb
                                                                             ON  (bd.budget = lb.budget)
                                      WHERE bd.budget_category in (1010,1020,1040)
                                      GROUP BY lb.client) pmr
     ON (pp.client = pmr.client)

     WHERE pp.client = @Client# 

End  Else

Return (@@Rowcount)
*/
GO
