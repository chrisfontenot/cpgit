USE [DebtPlus]
GO
/****** Object:  StoredProcedure [dbo].[rpt_ProductionReport_apt_byStatus]    Script Date: 09/15/2014 13:13:33 ******/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE PROCEDURE [dbo].[rpt_ProductionReport_apt_byStatus] ( @FromDate as datetime, @ToDate as datetime ) AS

-- ==========================================================================================================
-- ==            Generate the statistics for the appointment types                                         ==
-- ==========================================================================================================

-- Default the dates to the proper values
if @ToDate is null
	set @ToDate = getdate()

if @FromDate is null
	set @FromDate = @ToDate

-- Remove the time values
select	@FromDate	= convert(datetime, convert(varchar(10), @FromDate, 101) + ' 00:00:00')
select	@ToDate		= convert(datetime, convert(varchar(10), @ToDate, 101)   + ' 23:59:59')

-- Extract the list of appointments and the relative status for them
select	appt_type,

	convert(int, case when isnull(status,'P') = 'P' then 1 else 0 end) as 'pending',
	convert(int, case when isnull(status,'P') = 'M' then 1 else 0 end) as 'missed',
	convert(int, case when isnull(status,'P') = 'R' then 1 else 0 end) as 'rescheduled',
	convert(int, case when isnull(status,'P') = 'C' then 1 else 0 end) as 'cancelled',
	convert(int, case when isnull(status,'P') = 'K' then 1 else 0 end) as 'kept',
	convert(int, case when isnull(status,'P') = 'W' then 1 else 0 end) as 'walkin'

into	#t_appt_counts
from	client_appointments ca
where	ca.start_time between @FromDate and @ToDate;

-- Combine the appointments with the descriptions
select	isnull(td.appt_name, 'Any Type')				as 'description',
	sum(pending)							as 'pending',
	sum(missed)							as 'missed',
	sum(rescheduled)						as 'rescheduled',
	sum(cancelled)							as 'cancelled',
	sum(kept)							as 'kept',
	sum(walkin)							as 'walkin'

from	#t_appt_counts a
left outer join appt_types td on a.appt_type = td.appt_type
group by td.appt_name
order by td.appt_name;

-- Discard the working table
drop table #t_appt_counts;
GO
