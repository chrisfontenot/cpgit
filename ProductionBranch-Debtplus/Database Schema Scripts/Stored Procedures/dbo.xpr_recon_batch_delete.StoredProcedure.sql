USE [DebtPlus]
GO
/****** Object:  StoredProcedure [dbo].[xpr_recon_batch_delete]    Script Date: 09/15/2014 13:13:36 ******/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE PROCEDURE [dbo].[xpr_recon_batch_delete] ( @batch_id as int ) AS

-- ===========================================================================================================
-- ==            Delete the indicated reconcilation batch                                                   ==
-- ===========================================================================================================

if exists (
	select	*
	from	recon_batches
	where	recon_batch	= @batch_id
	and	date_posted	is not null )
begin
	RaisError ('The batch (%d) has been posted and may not be deleted.', 16, 1, @batch_id)
	return ( 0 )
end

-- Ensure that the batch exists
if not exists (
	select	*
	from	recon_batches
	where	recon_batch	= @batch_id )
begin
	RaisError ('The batch (%d) does not exist.', 16, 1, @batch_id)
	return ( 0 )
end

delete
from	recon_details
where	recon_batch	= @batch_id

delete
from	recon_batches
where	recon_batch	= @batch_id

return ( 1 )
GO
