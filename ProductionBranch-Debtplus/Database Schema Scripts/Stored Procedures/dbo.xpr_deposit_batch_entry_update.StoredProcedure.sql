USE [DebtPlus]
GO
/****** Object:  StoredProcedure [dbo].[xpr_deposit_batch_entry_update]    Script Date: 09/15/2014 13:13:35 ******/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE PROCEDURE [dbo].[xpr_deposit_batch_entry_update] ( @record as INT, @client as INT, @amount as Money, @type as VarChar(2) = 'OT', @item_date AS SmallDatetime = NULL, @reference AS VarChar(50) = NULL, @ok_to_post AS INT = 1 ) AS

-- =====================================================================================================
-- ==                  Update the entry in the system for a deposit                                   ==
-- =====================================================================================================

-- ChangeLog
--   5/13/2011
--     Added call to function 'DepositsInTrust' to recalculate the pending trust amount

-- suppress intermediate results
set nocount on

declare	@personal_checks	bit
declare	@test_client		typ_client
declare	@message		varchar(50)

-- Clean up the reference information
if @reference is not null
begin
	set @reference = ltrim(rtrim(@reference))
	if @reference = ''
		set @reference = null
end

-- If the item is valid to be posted then examine to ensure that the client is defined
if @ok_to_post > 0
begin
	if @client <= 0
	begin
		select	@ok_to_post	= 0,
			@message	= 'Invalid Client ID'
	end

	select	@personal_checks	= isnull(personal_checks,0),
		@test_client		= client
	from	clients
	where	client			= @client

	if @test_client is null
	begin
		select	@ok_to_post	= 0,
			@message	= 'Invalid Client ID'
	end
end

if @ok_to_post > 0
begin
	if (@personal_checks = 0) and (@type = 'PC')
	begin
		select	@message	= 'Personal Chks Invalid',
			@ok_to_post	= 0
	end
end

declare	@answer int

UPDATE	deposit_batch_details

SET	client			= @client,
	tran_subtype		= @type,
	amount			= @amount,
	item_date		= @item_date,
	ok_to_post		= @ok_to_post,
	reference		= @reference,
	message			= @message

WHERE	deposit_batch_detail	= @record
select @answer = @@rowcount

-- Correct the deposit amount in the pending tables
update clients set deposit_in_trust = dbo.DepositsInTrust(client) where client = @client

RETURN ( @answer )
GO
