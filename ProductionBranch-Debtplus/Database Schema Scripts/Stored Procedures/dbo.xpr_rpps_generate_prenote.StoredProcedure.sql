USE [DebtPlus]
GO
/****** Object:  StoredProcedure [dbo].[xpr_rpps_generate_prenote]    Script Date: 09/15/2014 13:13:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[xpr_rpps_generate_prenote] ( @rpps_file as int = null ) AS

-- =====================================================================================
-- ==          Generate prenote requests for all creditors that require prenotes      ==
-- =====================================================================================

-- ChangeLog
--   2/20/2002
--     Added test for the biller_type to ensure that EDI biller ids are not confused with payment values.
--   9/31/2002
--     Added support for client_creditor_balances table
--   11/20/2002
--     Changed to use RPPS' biller_aba number rather than rpps_biller_id
--   12/4/2002
--     Do not include prenotes unless payment prenote is required.
--   6/2/2003
--     Change to support "dual" (both proposals and payments) billers
--   11/24/2003
--     Added "@bank" as a parameter. It is optional for now.
--   06/09/2009
--      Remove references to debt_number and use client_creditor
--   1/5/2009
--      Changed from using biller_aba to using rpps_biller_id

-- Disable result sets for these operations
SET NOCOUNT ON

/*
-- Start a transaction for this operation
BEGIN TRANSACTION

declare	@bank	int

if isnull(@rpps_file,-1) > 0
	select	@bank		= bank
	from	rpps_files with (nolock)
	where	rpps_file	= @rpps_file

if @bank is null
	select	@bank		= min(bank)
	from	banks with (nolock)
	where	type		= 'R'
	and	[default]	= 1

if @bank is null
	select	@bank		= min(bank)
	from	banks with (nolock)
	where	type		= 'R'

if @bank is null
	select	@bank		= 2

-- Extract the list of debts and creditors which should be prenoted
SELECT		cc.client_creditor, cc.client, cc.creditor, cc.client_creditor, cm.rpps_biller_id as rpps_biller_id, cc.payment_rpps_mask, isnull(cm.bank,@bank) as bank, cc.account_number
INTO		#t_rpps_prenotes
FROM		client_creditor cc	WITH ( NOLOCK )
LEFT OUTER JOIN	creditor_methods cm	WITH ( NOLOCK ) ON cc.payment_rpps_mask = cm.creditor_method
INNER JOIN	client_creditor_balances bal WITH (NOLOCK) ON cc.client_creditor_balance = bal.client_creditor_balance
INNER JOIN	clients c		WITH ( NOLOCK ) ON cc.client = c.client
WHERE		cc.prenote_date		IS NULL
AND		cc.creditor		IS NOT NULL
AND		cc.account_number	IS NOT NULL
AND		cm.rpps_biller_id	IS NOT NULL
AND		cc.reassigned_debt = 0
AND		isnull(bal.orig_balance,0) + isnull(bal.orig_balance_adjustment,0) + isnull(bal.total_interest,0) > isnull(bal.total_payments,0)
AND		c.active_status NOT IN ('CRE', 'EX', 'I')

-- Update the debt information records to indicate that the prenote was sent
UPDATE		client_creditor
SET		prenote_date = getdate()
FROM		client_creditor cc
INNER JOIN	#t_rpps_prenotes t ON t.client_creditor = cc.client_creditor

-- Insert the items into the pending RPPS transaction table so that it is sent on the next batch
INSERT INTO rpps_transactions (client, creditor, client_creditor, biller_id, transaction_code, service_class_or_purpose, death_date, bank, rpps_file)
SELECT		client				as 'client',
		creditor			as 'creditor',
		client_creditor			as 'client_creditor',
		rpps_biller_id			as 'biller_id',
		'23'				as 'transaction_code',		-- '23' is "PRENOTE"
		'CIE'				as 'service_class_or_purpose',	-- 'CIE' is a payment/prenote request
		dateadd(d, 180, getdate())	as 'death_date',
		bank				as 'bank',
		@rpps_file			as 'rpps_file'

FROM		#t_rpps_prenotes

-- Include a system note that the prenote was performed
INSERT INTO	client_notes (client,client_creditor,type,is_text,subject,note)
SELECT		client,
		client_creditor,
		3 as 'type',
		1 as 'is_text',

		'Prenote Request Generated' as 'subject',
		'The payment prenote was generated to be sent to RPPS' + char(13) + char(10) +
		char(13) + char(10) +
		'account number: ' + account_number + char(13) + char(10) +
		'RPPS biller ID: ' + rpps_biller_id + char(13) + char(10)

FROM		#t_rpps_prenotes

-- Drop the working table
DROP TABLE	#t_rpps_prenotes

-- Commit the transaction
COMMIT TRANSACTION
*/
-- Return success to the caller
RETURN ( 1 )
GO
