USE [DebtPlus]
GO
/****** Object:  StoredProcedure [dbo].[rpt_client_trust_activity]    Script Date: 09/15/2014 13:13:32 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[rpt_client_trust_activity] ( @client as typ_client, @from_date as datetime = null, @to_date as datetime = null) as

-- =====================================================================================================
-- ==            List the client trust activity                                                       ==
-- =====================================================================================================

-- ChangeLog
--   06/09/2009
--      Remove references to debt_number and use client_creditor

-- suppress intermediate results
set nocount on

-- correct the date fields for the selection
if @to_date is null
	set @to_date = getdate()

if @from_date is null
	set @from_date = @to_date

select @from_date = convert(datetime, convert(varchar(10), @from_date, 101) + ' 00:00:00')
select @to_date   = convert(datetime, convert(varchar(10), @to_date,   101) + ' 23:59:59')

-- retrieve the information for the operation
select
	case when rcc.tran_type = 'BW' then 'AD' else rcc.tran_type end	as 'tran_type',
	rcc.creditor							as 'creditor',
	coalesce(cc.creditor_name, cr.creditor_name,'')			as 'creditor_name',
	rcc.client_creditor							as 'client_creditor',
	convert(datetime, convert(varchar(10), rcc.date_created, 101))	as 'date_created',
	case when rcc.tran_type = 'IN' then null else rcc.credit_amt end as 'credit_amt',
	rcc.debit_amt							as 'debit_amt',
	case when rcc.tran_type = 'IN' then rcc.credit_amt else null end as 'interest_amt',
	isnull(rcc.account_number, cc.account_number)			as 'account_number'

from	registers_client_creditor rcc with (nolock)
left outer join creditors cr with (nolock) on rcc.creditor = cr.creditor
left outer join client_creditor cc with (nolock) on rcc.client_creditor = cc.client_creditor
where	rcc.client = @client
and	rcc.date_created between @from_date and @to_date

union all

select
	tran_type							as 'tran_type',
	null								as 'creditor',
	null								as 'creditor_name',
	convert(int,null)					as 'client_creditor',
	convert(datetime, convert(varchar(10), date_created, 101))	as 'date_created',
	credit_amt							as 'credit_amt',
	debit_amt							as 'debit_amt',
	null								as 'interest_amt',
	null								as 'account_number'
from	registers_client rc with (nolock)
where	rc.client = @client
and	rc.date_created between @from_date and @to_date
and	rc.tran_type = 'DP'

order by 5, 1 desc -- date, tran_type desc

return ( @@rowcount )
GO
