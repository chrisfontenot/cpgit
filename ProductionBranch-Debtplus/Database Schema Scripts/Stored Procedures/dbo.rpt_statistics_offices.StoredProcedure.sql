USE [DebtPlus]
GO
/****** Object:  StoredProcedure [dbo].[rpt_statistics_offices]    Script Date: 09/15/2014 13:13:33 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE PROCEDURE [dbo].[rpt_statistics_offices] as

-- ======================================================================================================
-- ==            Retrieve the last 12 dates for the office statistics                                  ==
-- ======================================================================================================

set nocount on

-- Retrieve the distinctive dates from the items
select distinct period_start
into #raw_dates
from statistics_offices;

-- Take the most recient 12 dates
select top 12 period_start
into #date_selections
from #raw_dates
order by 1 desc;

-- Return the information from the statistics
select s.period_start, isnull(o.name,'UNSPECIFIED') as 'name', s.active_clients, s.clients_disbursed, s.expected_disbursement, s.actual_disbursement,
	case when s.expected_disbursement <= 0 then 0
	else convert(float,s.actual_disbursement) / convert(float, s.expected_disbursement) * 100.0
	end as disbursed_percentage, paf_fees
from	statistics_offices s with (nolock)
left outer join offices o with (nolock) on s.office = o.office
where period_start in (select period_start from #date_selections)
order by 1, 2;

-- Discard the working tables
drop table #date_selections
drop table #raw_dates

return ( @@rowcount );
GO
