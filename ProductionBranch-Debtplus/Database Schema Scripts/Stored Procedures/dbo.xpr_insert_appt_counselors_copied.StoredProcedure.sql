USE [DebtPlus]
GO
/****** Object:  StoredProcedure [dbo].[xpr_insert_appt_counselors_copied]    Script Date: 09/15/2014 13:13:35 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create procedure [dbo].[xpr_insert_appt_counselors_copied](@appt_time as int, @appt_time_template as int) as
-- ====================================================================================================
-- ==            Copy the appt_counselor_templates references to the appt_counselors table           ==
-- ====================================================================================================

set nocount on

-- Insert the table references now
INSERT INTO appt_counselors (appt_time, counselor)
SELECT DISTINCT @appt_time, co.counselor
FROM appt_counselor_templates t WITH (NOLOCK)
INNER JOIN counselors co WITH (NOLOCK) ON t.counselor = co.counselor AND co.ActiveFlag <> 0
INNER JOIN counselor_attributes coa WITH (NOLOCK) ON co.counselor = coa.counselor
INNER JOIN attributetypes a WITH (NOLOCK) ON coa.Attribute = a.oID AND (a.Grouping = 'ROLE' AND (a.Attribute = 'COUNSELOR' OR a.Attribute = 'CSR'))
WHERE t.appt_time_template=@appt_time_template

return ( @@rowcount )
GO
