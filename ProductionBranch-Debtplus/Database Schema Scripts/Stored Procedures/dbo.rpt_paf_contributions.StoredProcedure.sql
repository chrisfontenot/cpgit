USE [DebtPlus]
GO
/****** Object:  StoredProcedure [dbo].[rpt_paf_contributions]    Script Date: 09/15/2014 13:13:33 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE PROCEDURE [dbo].[rpt_paf_contributions] AS

-- ======================================================================================================
-- ==                   Calculate the PAF contributions for the previous months                        ==
-- ======================================================================================================

SET NOCOUNT ON
SET ANSI_WARNINGS OFF

DECLARE @current_date	DateTime
DECLARE	@one_date	DateTime
DECLARE @two_date	DateTime
DECLARE @three_date	DateTime
DECLARE @four_date	DateTime

DECLARE	@paf_creditor	typ_creditor
SELECT	@paf_creditor = paf_creditor
FROM	config

-- Fetch the current date
SELECT	@current_date	= convert(DateTime, convert(varchar(10), getdate(), 101) + ' 00:00:00')

-- Compute the start of the current month
SELECT	@one_date	= convert(DateTime, right('00' + convert(varchar, month(@current_date)),2) + '/01/' + right('0000' + convert(varchar(4), year(@current_date)), 4) + ' 00:00:00')

-- Compute the start of last month
SELECT	@two_date	= dateadd(m, -1, @one_date)

-- Compute the start of the third month
SELECT	@three_date	= dateadd(m, -2, @one_date)

-- Compute the start of the fourth month
SELECT	@four_date	= dateadd(m, -3, @one_date)
CREATE TABLE #t_disb_results (
	client		int,
	[deposits]	money NULL,
	[current]	money NULL,
	[one]		money NULL,
	[two]		money NULL,
	[three]		money NULL,
	[four]		money NULL
)

-- Compute the amount disbursed this month

INSERT INTO #t_disb_results (client, [current])
SELECT		client, sum(debit_amt) as amount
FROM		registers_client_creditor
WHERE		tran_type in ('AD', 'BW', 'CM', 'DM')
AND		client >= 0
AND		date_created BETWEEN @one_date AND @current_date
AND		creditor = @paf_creditor
GROUP BY	client

-- Compute the amount disbursed last month
INSERT INTO #t_disb_results (client, [one])
SELECT		client, sum(debit_amt) as amount
FROM		registers_client_creditor
WHERE		tran_type in ('AD', 'BW', 'CM', 'DM')
AND		client >= 0
AND		date_created BETWEEN @two_date AND @one_date
AND		creditor = @paf_creditor
GROUP BY	client

-- Compute the amount disbursed the prior month
INSERT INTO #t_disb_results (client, [two])
SELECT		client, sum(debit_amt) as amount
FROM		registers_client_creditor
WHERE		tran_type in ('AD', 'BW', 'CM', 'DM')
AND		client >= 0
AND		date_created BETWEEN @three_date AND @two_date
AND		creditor = @paf_creditor
GROUP BY	client

-- Compute the amount disbursed the prior month
INSERT INTO #t_disb_results (client, [three])
SELECT		client, sum(debit_amt) as amount
FROM		registers_client_creditor
WHERE		tran_type in ('AD', 'BW', 'CM', 'DM')
AND		client >= 0
AND		date_created BETWEEN @four_date AND @three_date
AND		creditor = @paf_creditor
GROUP BY	client

-- Compute the deposits this month
INSERT INTO #t_disb_results (client, [deposits])
SELECT		client, sum(credit_amt) as amount
FROM		registers_client WITH (NOLOCK)
WHERE		tran_type = 'DP'
AND		client > 0
AND		date_created BETWEEN @one_date AND @current_date
GROUP BY	client

-- Merge the results together
SELECT		a.client,
		dbo.format_normal_name(pn.prefix,pn.first,pn.middle,pn.last,pn.suffix) as name,
		sum(deposits) as deposits,
		sum([current]) as [current],
		sum([one]) as [one],
		sum([two]) as [two],
		sum([three]) as [three]
FROM		#t_disb_results a
LEFT OUTER JOIN	people p WITH (NOLOCK) ON a.client = p.client AND 1 = p.relation
left outer join names pn with (nolock) on p.nameid = pn.name
GROUP BY	a.client, pn.prefix, pn.first, pn.middle, pn.last, pn.suffix
ORDER BY	a.client

-- Drop the intermediate tables
DROP TABLE #t_disb_results

-- Return the resulting count of rows
RETURN ( @@rowcount )
GO
