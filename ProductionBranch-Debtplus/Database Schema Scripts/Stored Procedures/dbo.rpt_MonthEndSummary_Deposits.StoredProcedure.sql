USE [DebtPlus]
GO
/****** Object:  StoredProcedure [dbo].[rpt_MonthEndSummary_Deposits]    Script Date: 09/15/2014 13:13:33 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[rpt_MonthEndSummary_Deposits] ( @FromDate as DateTime = NULL, @ToDate as DateTime = NULL ) AS
-- =============================================================================================
-- ==                   Find the deposits by type                                             ==
-- =============================================================================================

-- Suppress intermediate result sets
SET NOCOUNT ON

IF @ToDate IS NULL
	SELECT @ToDate = getdate()

IF @FromDate IS NULL
	SELECT @FromDate = @ToDate

-- Adjust the dates for the proper settings
SELECT  @FromDate   = CONVERT(varchar(10), @FromDate, 101),
        @ToDate     = CONVERT(varchar(10), @ToDate, 101) + ' 23:59:59';

-- Fetch the deposits
SELECT		t.description					as 'type',
		d.client					as 'client',
		CONVERT(DATETIME,CONVERT(VARCHAR(10),d.date_created,101) + ' 00:00:00')					as 'date',
		d.credit_amt					as 'amount',
		d.message					as 'reference',
	        d.tran_subtype                                  as 'tran_type'
FROM		registers_client d WITH (NOLOCK)
LEFT OUTER JOIN	tran_types t WITH (NOLOCK) ON d.tran_subtype = t.tran_type
WHERE		d.date_created BETWEEN @FromDate AND @ToDate
AND		d.tran_type = 'DP'
ORDER BY	1, 3, 2

RETURN ( @@rowcount )
GO
