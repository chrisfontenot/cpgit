USE [DebtPlus]
GO
/****** Object:  StoredProcedure [dbo].[rpt_client_indicators]    Script Date: 09/15/2014 13:13:32 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE PROCEDURE [dbo].[rpt_client_indicators] ( @indicator as int = null, @From_Date as datetime = null, @To_Date as datetime = null ) as

-- ==========================================================================================
-- ==            List the clients with the various indicators                              ==
-- ==========================================================================================

-- Find the list of indicators for clients
select	i.client,
		convert(varchar(80), null) as name,
		convert(varchar(80), null) as indicator,
		i.date_created as date_created,
		i.created_by as created_by,
		i.indicator as indicator_id
into	#results
from	client_indicators i with (nolock)

-- Select the information from the list
if ISNULL(@indicator, -1) > 0
	delete
	from	#results
	where	indicator_id <> @indicator
	
if @From_Date is not null
	delete
	from	#results
	where	date_created < CONVERT(varchar(10), @from_date, 101)
	
if @To_Date is not null
	delete
	from	#results
	where	date_created >= CONVERT(varchar(10), dateadd(d, 1, @to_date), 101)
	
-- Update the results
update	#results
set		name		= dbo.format_normal_name(pn.prefix, pn.first, pn.middle, pn.last, pn.suffix)
from	#results r
inner join people p on r.client = p.client and 1 = p.relation
inner join Names pn on p.NameID = pn.name

update	#results
set		created_by	= dbo.format_counselor_name ( created_by );

update	#results
set		indicator	= ind.description
from	#results r
inner join indicators ind on r.indicator_id = ind.indicator;

-- Return the results
select	client,
		name,
		indicator,
		date_created,
		created_by
from	#results

drop table #results;

return ( @@rowcount )
GO
