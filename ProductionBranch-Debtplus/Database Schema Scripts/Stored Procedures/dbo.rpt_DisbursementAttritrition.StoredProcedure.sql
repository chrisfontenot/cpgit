USE [DebtPlus]
GO
/****** Object:  StoredProcedure [dbo].[rpt_DisbursementAttritrition]    Script Date: 09/15/2014 13:13:33 ******/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE PROCEDURE [dbo].[rpt_DisbursementAttritrition] AS
-- =============================================================================================
-- ==      Return information as to what was disbursed this month v.s. what was expected      ==
-- =============================================================================================

-- ChangeLog
--   9/31/2002
--     Added support for client_creditor_balances table

-- Ignore warnings about "null values ignored"
SET ANSI_WARNINGS OFF

SELECT		cc.client,
		sum(bal.current_sched_payment)		as 'expected',
		sum(bal.payments_month_0)		as 'current_month',
		sum(bal.payments_month_1)		as 'prior_month'
INTO		#Disbursement_Attrition
FROM		client_creditor cc WITH (NOLOCK)
inner join	client_creditor_balances bal WITH (NOLOCK) ON cc.client_creditor_balance = bal.client_creditor_balance
WHERE		cc.client > 0
AND		cc.reassigned_debt = 0
GROUP BY	cc.client

SELECT		coalesce(o.name,convert(varchar,c.office),'Unspecified') as 'office',
		c.client					as 'client',
		isnull(dbo.format_normal_name (pn.prefix,pn.first,pn.middle,pn.last,pn.suffix),'') as 'name',
		a.expected					as 'expected',
		a.current_month					as 'current',
		a.prior_month					as 'prior'

FROM		clients c WITH (NOLOCK)
INNER JOIN	#Disbursement_Attrition a ON c.client = a.client
LEFT OUTER JOIN	people p WITH (NOLOCK) ON c.client = p.client AND 1 = p.relation
left outer join names pn with (nolock) on p.nameid = pn.name
LEFT OUTER JOIN	offices o WITH (NOLOCK) ON c.office = o.office

WHERE		c.active_status in ('A', 'AR')
OR		a.current_month > 0
OR		a.prior_month > 0

ORDER BY	o.name, c.client

-- Discard the working table
drop table #Disbursement_Attrition

RETURN ( @@rowcount )
GO
