USE [DebtPlus]
GO
/****** Object:  StoredProcedure [dbo].[cccs_admin_intake_import_single]    Script Date: 09/15/2014 13:13:31 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[cccs_admin_intake_import_single]  ( @application_id as int ) as

-- Import the client from the cccs_admin database
begin transaction

-- Suppress the intermediate results
set nocount on

-- Find the method of first contact. This does not change from client to client.
declare	@method_first_contact			int

select	@method_first_contact	= min(oID)
from	FirstContactTypes
where	NFCC			= 'I'
and		[default]		= 1

if @method_first_contact is null
	select	@method_first_contact	= min(oID)
	from	FirstContactTypes
	where	NFCC			= 'I'

if @method_first_contact is null
	select	@method_first_contact	= min(oID)
	from	FirstContactTypes

if @method_first_contact is null
	select	@method_first_contact	= 1

-- Read the applicant id
declare	@address_id					int
declare	@salutation					varchar(80)
declare	@first_name					varchar(80)
declare	@last_name					varchar(80)
declare	@middle_initial				varchar(80)
declare	@suffix						varchar(80)
declare	@email_address				varchar(800)
declare	@ssn						varchar(80)
declare	@birth_date					datetime

-- Information for the home address
declare	@address1					varchar(80)
declare	@address2					varchar(80)
declare	@city						varchar(80)
declare	@state						int
declare	@postalcode					varchar(50)

declare	@marital_status_code		int
declare	@num_dependents				int
declare	@problem_type_id			int
declare	@referral_type_id			int
declare	@housing_payments_current	int
declare	@telephone_home				varchar(14)
declare	@fax_number					varchar(14)
declare	@employer					varchar(80)
declare	@job_title					varchar(80)
declare	@monthly_income_gross		money
declare	@monthly_income_net			money
declare	@monthly_income_other		money
declare	@unpaid_judgments			int
declare	@wages_garnished			int
declare	@accounts_with_attorney		int
declare	@filed_bankruptcy			int
declare @housing_status				int
declare	@comments					varchar(800)
declare	@manager_notes				varchar(800)
declare @work_ph					varchar(14)
declare	@work_ext					varchar(14)
declare	@credit_score				int
declare	@ForeignDatabaseID			varchar(50)
declare	@preferred_contact			int
declare @race						int
declare @ethnicity					int
declare @education					int
declare @other_reason				varchar(800)

-- Find the home address
select
	@ForeignDatabaseID			= ForeignDatabaseID,
	@credit_score				= credit_score,
	@address_id					= address_id,
	@salutation					= dbo.cccs_admin_address ( salutation ),
	@first_name					= UPPER(dbo.cccs_admin_address ( first_name )),
	@middle_initial				= UPPER(dbo.cccs_admin_address ( middle_initial )),
	@last_name					= UPPER(dbo.cccs_admin_address ( last_name )),
	@suffix						= UPPER(dbo.cccs_admin_address ( suffix )),
	@email_address				= email_address,
	@birth_date					= birth_date,
	@num_dependents				= num_dependents,
	@ssn						= dbo.cccs_admin_ssn ( ssn ),
	@marital_status_code		= dbo.cccs_admin_marital ( marital_status_code ),
	@problem_type_id			= dbo.cccs_admin_problem ( problem_type_id ),
	@referral_type_id			= dbo.cccs_admin_referral ( referral_type_id ),
	@telephone_home				= dbo.cccs_admin_telephone ( telephone_home ),
	@fax_number					= dbo.cccs_admin_telephone ( fax_number ),
	@housing_status				= dbo.cccs_admin_housing ( housing_status_code ),
	@housing_payments_current	= housing_payments_current,
	@employer					= employer,
	@job_title					= job_title,
	@monthly_income_gross		= monthly_income_gross,
	@monthly_income_net			= monthly_income_net,
	@monthly_income_other		= monthly_income_other,
	@unpaid_judgments			= unpaid_judgements,
	@wages_garnished			= wages_garnished,
	@accounts_with_attorney		= accounts_with_attorney,
	@filed_bankruptcy			= filed_bankruptcy,
	@comments					= comments,
	@manager_notes				= manager_notes,
	@work_ph					= dbo.cccs_admin_telephone (telephone_work),
	@preferred_contact			= dbo.cccs_admin_map_perferred_contact ( preferred_method_contact ),
	@race						= dbo.cccs_admin_map_race(ethnicity_id),
	@ethnicity					= dbo.cccs_admin_map_ethnicity(ethnicity_id),
	@education					= dbo.cccs_admin_map_education(education_id),
	@other_reason				= other_reason
from	cccs_admin..applications
where	application_id				= @application_id

-- Read the home address information
select	@address1			= UPPER(isnull(dbo.cccs_admin_address ( line_1 ),'')),
		@address2			= UPPER(isnull(dbo.cccs_admin_address ( line_2 ),'')),
		@city				= UPPER(isnull(dbo.cccs_admin_address ( city ),'')),
		@state				= isnull(dbo.cccs_admin_state   ( state_code ),0),
		@postalcode			= UPPER(isnull(dbo.cccs_admin_postalcode ( zip_code ),''))
from	cccs_admin..addresses
where	address_id			= @address_id

-- Remove the extra stuff from the home telephone number
select	@housing_payments_current	= case when @housing_payments_current = 0 then 1 else 0 end

-- Allocate a client id for the new client
declare	@client					int
declare	@created_client			bit
declare	@active_status			varchar(10)

select	@created_client = 0,
		@active_status  = ''

if @ForeignDatabaseID is not null
	select	@client	= client,
			@active_status = active_status
	from	clients with (nolock)
	where	client  = CONVERT(int, @ForeignDatabaseID)

-- If the client exists then the client must not be active. Active clients are not changed.
if @active_status in ('A','AR')
	return ( 0 )

declare	@HomeTelephoneID		int
declare	@AddressID				int
declare	@MsgTelephoneID			int
declare	@WorkPhoneID			int
declare	@Acode					varchar(10)

-- If there is a client then retrieve some of the items from the current client
if @client is not null
begin

	-- Read the data from the clients table
	select	@HomeTelephoneID	= HomeTelephoneID,
			@AddressID			= AddressID,
			@MsgTelephoneID		= MsgTelephoneID
	from	clients
	where	client				= @client
end

-- If there is a number, split the area code from the number		
select	@acode	= null
if @telephone_home is not null
begin
	if len(@telephone_home) > 7
		select	@acode = left(@telephone_home,3),
				@telephone_home = substring(@telephone_home,4,80)
				
	if len(@telephone_home) > 4
		select	@telephone_home = left(@telephone_home, 3) + '-' + substring(@telephone_home, 4, 80)
end
		
-- Record the home telephone number into the system	
if @HomeTelephoneID is not null or @telephone_home is not null
begin
	if @HomeTelephoneID is null
		execute @HomeTelephoneID = xpr_insert_TelephoneNumbers default, @acode, @telephone_home, default
		
	update TelephoneNumbers
	set		acode	= @Acode,
			number	= @telephone_home
	where	TelephoneNumber = @HomeTelephoneID
end

-- If there is a number, split the area code from the number		
select	@acode	= null
if @fax_number is not null
begin
	if len(@fax_number) > 7
		select	@acode = left(@fax_number,3),
				@fax_number = substring(@fax_number,4,80)
				
	if len(@fax_number) > 4
		select	@fax_number = left(@fax_number, 3) + '-' + substring(@fax_number, 4, 80)
end
		
-- Record the Telephoe number into the system	
if @msgTelephoneID is not null or @fax_number is not null
begin
	if @msgTelephoneID is null
		execute @msgTelephoneID = xpr_insert_TelephoneNumbers default, @acode, @fax_number, default
		
	update TelephoneNumbers
	set		acode	= @Acode,
			number	= @fax_number
	where	TelephoneNumber = @msgTelephoneID
end

-- Create the address information
declare	@House					varchar(80)
declare	@address_suffix			varchar(80)
declare	@address_direction		varchar(80)
declare	@postal_abbreviation	int

select	@house				= '',
		@address_suffix		= '',
		@address_direction	= ''

if @address1 is not null
begin
	if @address1 like '[0-9]% %'
		select	@House	= ltrim(rtrim(left(@address1, charindex(' ', @address1)))),
				@address1 = ltrim(rtrim(substring(@address1, charindex(' ', @address1), 80)))
				
	-- Determine if the last word is a match to a postal address_suffix
	if @address1 like '% %'
	begin
		select	@address_suffix = ltrim(rtrim(substring(@address1, len(@address1) + 1 - charindex(' ', reverse(@address1)), 80))),
				@address1 = ltrim(rtrim(substring(@address1, 1, len(@address1) - charindex(' ', reverse(@address1)))))
		
		-- If the address_suffix is defined, see if there is a match
		if @address_suffix <> ''
			select	@postal_abbreviation = postal_abbreviation
			from	postal_abbreviations with (nolock)
			where	(abbreviation = @address_suffix or description = @address_suffix)
			and		type = 2

		-- If there is no match then put the work back			
		if @postal_abbreviation is null
			select	@address1 = @address1 + ' ' + @address_suffix,
					@address_suffix	  = ''
		else
			select	@address_suffix		= abbreviation
			from	postal_abbreviations with (nolock)
			where	postal_abbreviation = @postal_abbreviation
	end
	
	-- Find the leading direction if indicated
	if @address1 like '% %'
	begin
		select	@address_direction = ltrim(rtrim(substring(@address1, 1, charindex(' ', @address1)))),
				@address1  = ltrim(rtrim(substring(@address1, charindex(' ', @address1), 80))),
				@postal_abbreviation = null

		-- Look for conditions like 'south west' or 'north east'
		if @address_direction = 'south' or @address_direction = 'north'
		begin
			declare	@word2		varchar(80)
			if @address1 like '% %'
			begin
				select	@word2 = ltrim(rtrim(substring(@address1, 1, charindex(' ', @address1)))),
						@address1  = ltrim(rtrim(substring(@address1, charindex(' ', @address1), 80)))
						
				if @word2 = 'east' or @word2 = 'west'
					select	@address_direction = @address_direction + ' ' + @word2,
							@address1  = ltrim(rtrim(substring(@address1, charindex(' ', @address1), 80)))
			end
		end
						
		if @address_direction <> ''
			select	@postal_abbreviation = postal_abbreviation
			from	postal_abbreviations with (nolock)
			where	(abbreviation = replace(@address_direction,' ','-') or description = replace(@address_direction,' ','-'))
			and		type = 1

		-- If there is no match then put the work back			
		if @postal_abbreviation is null
			select	@address1 = @address_direction + ' ' + @address1,
					@address_direction	  = ''
		else
			select	@address_direction	= abbreviation
			from	postal_abbreviations with (nolock)
			where	postal_abbreviation = @postal_abbreviation
	end
end

if @AddressID is not null or @address1 is not null
begin
	if @AddressID is null
	begin
		insert into addresses ([house], [direction], [street], [address_line_2], [city], [state], [postalcode])
		select	isnull(@house,''), isnull(@address_direction, ''), isnull(@address1,''), isnull(@address2,''), isnull(@city,''), isnull(@state,0), isnull(@postalcode,'')
		select @AddressID = scope_identity()
	end

	update Addresses
	set		creditor_prefix_1	= '',
			creditor_prefix_2	= '',
			house				= UPPER(isnull(@house,'')),
			direction			= UPPER(isnull(@address_direction,'')),
			street				= UPPER(isnull(@address1,'')),
			suffix				= UPPER(isnull(@address_suffix,'')),
			modifier			= 'APT',
			modifier_value		= '',
			address_line_2		= UPPER(isnull(@address2,'')),
			address_line_3		= '',
			city				= UPPER(isnull(@city,'')),
			state				= isnull(@state,0),
			postalcode			= UPPER(isnull(@postalcode,''))
	where	Address				= @AddressID
end
	
-- Create the client if there is not one
if @client is null
begin
	select	@created_client = 1
	execute @client = xpr_create_client @HomeTelephoneID, @housing_payments_current, @referral_type_id, @method_first_contact

	-- Create the appointment entry
	execute cccs_admin_intake_appointment @application_id, @client

	-- Add an alias to the intake client identifier
	insert into client_addkeys (client, additional) values ( @client, 'INTAKE APP ' + convert(varchar, @application_id) )
end

-- Ensure that the values which come from the import logic are reset to their corresponding values
update	clients
set		HomeTelephoneID		= @HomeTelephoneID,
		MsgTelephoneID		= @MsgTelephoneID,
		AddressID			= @AddressID,
		
		mortgage_problems	= @housing_payments_current,
		dependents			= @num_dependents,
		cause_fin_problem1	= @problem_type_id,
		marital_status		= @marital_status_code,
		preferred_contact	= @preferred_contact
where	client				= @client

-- Correct the client housing status
update	client_housing
set		housing_status		= @housing_status
where	client				= @client

-- Find the default information
declare	@person				int
declare	@WorkTelephoneID	int
declare	@NameID				int
declare	@EmailID			int

select	@person				= person,
		@WorkTelephoneID	= WorkTelephoneID,
		@NameID				= NameID,
		@EmailID			= EmailID
from	people
where	client				= @client
and		relation			= 1

-- If there is a number, split the area code from the number
select	@acode	= null
if @work_ph is not null
begin
	if len(@work_ph) > 7
		select	@acode = left(@work_ph,3),
				@work_ph = substring(@work_ph,4,80)
				
	if len(@work_ph) > 4
		select	@work_ph = left(@work_ph, 3) + '-' + substring(@work_ph, 4, 80)
end
		
-- Record the home telephone number into the system	
if @WorkTelephoneID is not null or @work_ph is not null
begin
	if @WorkTelephoneID is null
		execute @WorkTelephoneID = xpr_insert_TelephoneNumbers default, @acode, @work_ph, default
		
	update TelephoneNumbers
	set		acode	= @Acode,
			number	= @work_ph
	where	TelephoneNumber = @WorkTelephoneID
end

-- Determine the name pointer
if @NameID is not null or @last_name <> ''
begin
	if @NameID is null
		execute @NameID = xpr_insert_Names @salutation, @first_name, @middle_initial, @last_name, @suffix
		
	update Names
	set		prefix	= UPPER(@salutation),
			first	= UPPER(@first_name),
			middle	= UPPER(@middle_initial),
			last	= UPPER(@last_name),
			suffix	= UPPER(@suffix)
	where	Name = @NameID
end

-- Find the Email address
if @EmailID is not null or @email_address <> ''
begin
	if @EmailID is null
		execute @EmailID = xpr_insert_EmailAddress @email_address
		
	update	EmailAddresses
	set		Address	= @email_address
	where	Email = @EmailID
end

-- Update the applicant information. The original person was added during the client create operation.
update	people
set		NameID			= @NameID,
		WorkTelephoneID	= @WorkTelephoneID,
		EmailID			= @EmailID,
		ssn				= @ssn,
		birthdate		= @birth_date,
		gross_income	= @monthly_income_gross,
		net_income		= @monthly_income_net,
		other_job		= @job_title,
		fico_score		= isnull(@credit_score,0),
		gender			= dbo.cccs_admin_gender(@salutation),
		race			= @race,
		ethnicity		= @ethnicity,
		education		= @education
where	person			= @person;

-- Insert the messages to the effect of any problems
declare	@message_text	varchar(800)
declare	@client_note	int
select	@message_text = '',
		@client_note = null

if isnull(@unpaid_judgments,0) <> 0
	select	@message_text = @message_text + char(13) + char(10) + 'The applicant indicated that there were judgements outstanding.'

if isnull(@wages_garnished,0) <> 0
	select	@message_text = @message_text + char(13) + char(10) + 'The applicant has garnished wages.'

if isnull(@accounts_with_attorney,0) <> 0
	select	@message_text = @message_text + char(13) + char(10) + 'The applicant has accounts with an attorney.'

if isnull(@filed_bankruptcy,0) <> 0
	select	@message_text = @message_text + char(13) + char(10) + 'The applicant has filed bankruptcy.'

-- Look for the problem value of "other" and include the note if one was given.
if ltrim(rtrim(isnull(@other_reason,''))) <> ''
	select	@message_text = @message_text + char(13) + char(10) + 'The applicant stated the other problem was ''' + replace(ltrim(rtrim(isnull(@other_reason,''))), '''', '''''') + '''.'

if @message_text <> ''
	select	@message_text = substring(@message_text, 3, 8000)

if @message_text <> ''
begin
	
	-- Try to find the client note that was previously loaded if we did not create the client
	if @created_client = 0
		select	@client_note = client_note
		from	client_notes
		where	client		= @client
		and		type		= 1
		and		subject		= 'Problems noted on the website'
		and		client_creditor is null;
	
	-- If the note does not exist, create it
	if @client_note is null
	begin
		insert into client_notes (client, type, subject, note) values (@client, 1, 'Problems noted on the website', @message_text)
		select	@client_note = SCOPE_IDENTITY();
	end

	-- Ensure that the note has the proper text		
	update	client_notes
	set		note		= @message_text
	where	client_note	= @client_note;
end
	
-- Include the client comment notes
select	@message_text = ltrim(rtrim(isnull(@comments,''))),
		@client_note = null

if @message_text <> ''
begin
	
	-- Try to find the client note that was previously loaded if we did not create the client
	if @created_client = 0
		select	@client_note = client_note
		from	client_notes
		where	client		= @client
		and		type		= 1
		and		subject		= 'Website client comments'
		and		client_creditor is null;
	
	-- If the note does not exist, create it
	if @client_note is null
	begin
		insert into client_notes (client, type, subject, note) values (@client, 1, 'Website client comments', @message_text)
		select	@client_note = SCOPE_IDENTITY();
	end

	-- Ensure that the note has the proper text		
	update	client_notes
	set		note		= @message_text
	where	client_note	= @client_note;
end

-- Include the client comment notes
select	@message_text = ltrim(rtrim(isnull(@manager_notes,''))),
		@client_note = null

if @message_text <> ''
begin
	
	-- Try to find the client note that was previously loaded if we did not create the client
	if @created_client = 0
		select	@client_note = client_note
		from	client_notes
		where	client		= @client
		and		type		= 1
		and		subject		= 'Website manager notes'
		and		client_creditor is null;
	
	-- If the note does not exist, create it
	if @client_note is null
	begin
		insert into client_notes (client, type, subject, note) values (@client, 1, 'Website manager notes', @message_text)
		select	@client_note = SCOPE_IDENTITY();
	end

	-- Ensure that the note has the proper text		
	update	client_notes
	set		note		= @message_text
	where	client_note	= @client_note;
end

-- Include the coapplicant information
execute cccs_admin_intake_coapplicant		@application_id, @client

-- Load the budget expenses. These may come from the budget_expenses table as well.
execute cccs_admin_intake_budget_expenses	@application_id, @client

-- Load the debts. These are taken only from the application_creditors table
execute cccs_admin_intake_debts				@application_id, @client

-- Load the monthly fee debt. This comes from the budgets table.
execute cccs_admin_intake_X0001				@application_id, @client

-- Load the deposit information. This may come from the budgets table. (Must be done after the debts are loaded!!)
execute cccs_admin_intake_budget_deposits	@application_id, @client

-- Update the other income figure. This comes from the budgets table.
execute cccs_admin_intake_assets			@application_id, @client

-- Override the income fields for the applicant and co-applicant from the budgets table.
execute cccs_admin_intake_income			@application_id, @client

commit transaction
return ( @client )
GO
