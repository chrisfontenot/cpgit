IF NOT EXISTS(SELECT * FROM SYSOBJECTS WHERE NAME=N'xpr_disbursement_deduct' AND TYPE='P')
	EXEC('CREATE procedure xpr_disbursement_deduct AS')
GO
ALTER procedure [dbo].[xpr_disbursement_deduct] ( @disbursement_register AS INT ) AS

-- ===================================================================================================================
-- ==            Generate the deduction check(s) for the disbursement batch and mark closed                         ==
-- ===================================================================================================================

-- ChangeLog
--   1/13/2001
--     Changed because the "first_payment_date", "first_payment_amount", and "last_payment_date" was moved
--     to the registers_client_creditor based upon first_payment and last_payment pointers.
--   2/18/2002
--     Added client 0 transaction to record the change in the deduct balance.
--   5/13/2002
--     Obtain the mail_priority from the deduct creditor to use for the check register priority column.
--   9/31/2002
--     Added support for client_creditor_balances table
--   06/09/2009
--      Remove references to debt_number and use client_creditor

-- Disable intermediate result sets
SET NOCOUNT ON

-- Start a transaction
SET XACT_ABORT ON

-- Local storage
DECLARE	@current_amount			Money
DECLARE	@MaximumAmt			Money
DECLARE	@check_amount			Money
DECLARE	@CurrentTrustBalance		Money
DECLARE	@DeductionClient		INT
DECLARE	@DeductionDebt			INT
DECLARE	@DeductionRegister		INT
DECLARE	@trust_register			INT
DECLARE	@client_creditor_register	INT
DECLARE	@creditor_register		INT
DECLARE	@deduct_creditor		varchar(10)
DECLARE	@disbursement_date		datetime
DECLARE	@priority			int
DECLARE	@client_creditor		int

DECLARE	@client					int
select	@client				= 0;

-- -------------------------------------------------------------------------------------------
-- --            Update the disbursement information                                        --
-- -------------------------------------------------------------------------------------------

-- Update the batch information as needed
SELECT	@CurrentTrustBalance = sum(held_in_trust)
FROM	clients WITH (NOLOCK)

-- Fetch the amount of the deduction account
SELECT	@current_amount	= held_in_trust
FROM	clients
WHERE	client		= @client

-- Finish the current disbursement information
UPDATE	registers_disbursement
SET		post_balance	= isnull(@CurrentTrustBalance,0),
		deduct_balance	= isnull(@current_amount,0),
		date_posted		= getdate()
WHERE	disbursement_register = @disbursement_register

-- -------------------------------------------------------------------------------------------
-- --            Do a deduction disbursement if there is any money in the account           --
-- -------------------------------------------------------------------------------------------

-- Determine the creditor used for deductions
SELECT	@deduct_creditor	= deduct_creditor
FROM	config with (nolock)

if @deduct_creditor is null
	select	@deduct_creditor = 'Z9999'
	
-- Find the debt transaction for the deduction account
select	@client_creditor	= client_creditor
from	client_creditor
where	client				= 0
and		creditor			= @deduct_creditor

-- Do not continue until the system is properly configured
if @client_creditor is null
begin
	RaisError('The debt 000000*Z9999 can not be found in the client_creditor table', 16, 1)
	return ( 0 )
end

BEGIN TRANSACTION
BEGIN TRY
	declare	@creditor_id		int
	declare	@bank			int

	select	@creditor_id		= creditor_id,
		@bank			= check_bank
	from	creditors with (nolock)
	where	creditor		= @deduct_creditor

	-- From the creditor, determine the bank account to be used
	if not exists (select * from banks where bank = isnull(@bank, -1) and type = 'C')
		select	@bank	= null

	if @bank is null
		select		@bank		= min(bank)
		from		banks with (nolock)
		where		type		= 'C'
		and		[default]	= 1

	if @bank is null
		select		@bank		= min(bank)
		from		banks with (nolock)
		where		type		= 'C'

	if isnull(@bank,0) <= 0
		select	@bank	= 1

	-- Find the parameters for the deduct check
	select	@MaximumAmt	= coalesce(cr.max_amt_per_check, b.max_amt_per_check, 0),
			@priority	= cr.mail_priority
	from	creditors cr with (nolock), banks b
	where	cr.creditor	= @deduct_creditor
	and	b.bank		= @bank

	if @priority is null
		select	@priority = 9
	else
		if @priority < 0 or @priority > 9
			select	@priority = 9

	IF @MaximumAmt IS NULL
		SET @MaximumAmt = 0

	-- Fetch the disbursement date
	select	@disbursement_date	= date_created
	from	registers_disbursement
	where	disbursement_register	= @disbursement_register

	if @disbursement_date is null
		set @disbursement_date = getdate()

	-- Determine the amount of the deduct contribution.
	select	@current_amount = sum(fairshare_amt)
	from	registers_client_creditor
	where	disbursement_register = @disbursement_register
	and	creditor_type = 'D'

	if isnull(@current_amount,0) <= 0
		select @current_amount = 0

	if @current_amount > 0
	begin
		-- See "xpr_disbursement_cr_trans" for the actual update of the trust balance on client 0.
		-- We just make the one entry for the disbursement at this point.
		insert into registers_client (tran_type, credit_amt, disbursement_register, client)
		values ('AD', @current_amount, @disbursement_register, 0)
	end

	-- Obtain the corrected balance once all of the updates are performed
	SELECT	@current_amount	= held_in_trust
	FROM	clients
	WHERE	client		= @client

	-- Generate the needed checks to encompass the deduction check
	WHILE	@current_amount > 0
	BEGIN
		SET @check_amount = @current_amount
		IF (@MaximumAmt > 0) AND (@check_amount > @MaximumAmt)
			SET @check_amount = @MaximumAmt

		SET	@current_amount = @current_amount - @check_amount

		-- Generate the check for the disbursement
		insert into registers_trust	(tran_type,	creditor,		cleared,	amount,		check_order,	bank)
		values				('CM',		@deduct_creditor,	'P',		@check_amount,	@priority,	@bank)

		SELECT @trust_register = SCOPE_IDENTITY()

		-- Insert the disbursement information for the check
		insert into registers_client_creditor	(tran_type,	client,	creditor,		client_creditor,	trust_register,		disbursement_register,	debit_amt,	account_number)
		select	'CM',		@client,	@deduct_creditor,	@client_creditor,		@trust_register,	@disbursement_register,	@check_amount,	dbo.format_client_id( @client )

		SELECT @client_creditor_register = SCOPE_IDENTITY()

		insert into registers_client		(tran_type,	client,	debit_amt,	message,		trust_register,		disbursement_register)
		values					('CM',		@client,	@check_amount,	'Deduction Check',	@trust_register,	@disbursement_register)

		insert into registers_creditor		(tran_type,	creditor,		debit_amt,	message,		trust_register,		disbursement_register)
		values					('CM',		@deduct_creditor,	@check_amount,	'Deduction Check',	@trust_register,	@disbursement_register)

		select	@creditor_register	= SCOPE_IDENTITY()

		-- Update the client/creditor statistics
		update	client_creditor
		set	last_payment		= @client_creditor_register,
			first_payment		= isnull(first_payment, @client_creditor_register)
		where	client_creditor	= @client_creditor
	
		update	client_creditor_balances
		set	payments_month_0	= payments_month_0 + @check_amount
		from	client_creditor_balances bal
		inner join client_creditor cc on bal.client_creditor_balance = cc.client_creditor_balance
		where	cc.client_creditor	= @client_creditor

		-- Update the creditor statistics
		update	creditors
		set		distrib_mtd		= isnull(distrib_mtd,0) + @check_amount,
				last_payment	= @creditor_register,
				first_payment	= isnull(first_payment, @creditor_register)
		where	creditor		= @deduct_creditor;

		-- Take the money from the deduct creditor
		update	clients
		set	held_in_trust	= held_in_trust - @check_amount
		where	client		= @client;
	END

	-- ======================================================================================================================
	-- ==               Do the last bit of cleanup. Remove the "in progress disbursement" note from the clients            ==
	-- ======================================================================================================================
	update	registers_client
	set		message					= null
	where	tran_type				= 'AD'
	and		disbursement_register	= @disbursement_register

	-- Find the list of debts that were paid in this disbursement but are close to payoff. Merge it with the status.
	select b.zero_bal_status, b.client_creditor_balance, cc.client_creditor, cc.creditor, cr.creditor_name, dbo.statement_account_number(cc.account_number) as account_number, cc.disbursement_factor, b.orig_balance, b.orig_balance + b.orig_balance_adjustment + b.total_interest - b.total_payments as balance, e.address as email_address, dbo.format_normal_name(n.prefix, n.first, n.middle, n.last, n.suffix) as email_name, 3 as email_status, e.validation
	into	#email_notices
	from	client_creditor cc
	inner join client_creditor_balances b on cc.client_creditor_balance = b.client_creditor_balance
	inner join creditors cr on cc.creditor = cr.creditor
	inner join disbursement_creditors x on x.client_creditor = cc.client_creditor
	inner join disbursement_clients c on c.client = x.client and c.disbursement_register = @disbursement_register
	inner join creditor_classes ccl on cr.creditor_class = ccl.creditor_class
	left outer join people p on c.client = p.client and 1 = p.relation
	left outer join names n on p.nameid = n.name
	left outer join emailaddresses e on p.emailid = e.email
	where	b.zero_bal_status = 1
	and		x.disbursement_register = @disbursement_register
	and		c.active_status in ('A','AR')
	and		ccl.agency_account = 0

	-- Change the status to reflect using email for the message if there is an email address.
	update	#email_notices
	set		email_status = 2
	where	email_address is not null
	and		validation = 1;

	-- Update the balance indicator so that they are not sent again
	update	client_creditor_balances
	set		zero_bal_status	= n.email_status
	from	client_creditor_balances b
	inner join #email_notices n on b.client_creditor_balance = n.client_creditor_balance
	where	b.zero_bal_status = 1

	-- Determine the template for the email notice
	if exists (select * from #email_notices where email_status = 2)
	begin

		-- Find the email template ID
		declare	@template	int
		select	@template = oID
		from	email_templates
		where	description = 'Zero Balance Notification'

		if @template is not null
		begin

			insert into email_queue (template, email_address, email_name, substitutions, status, attempts)
			select	@template as template, email_address, email_name, dbo.zero_bal_email_substitutions(client_creditor_balance, client_creditor, creditor, creditor_name, account_number, disbursement_factor, orig_balance, balance, email_address, email_name), 'PENDING', 0
			from	#email_notices
			where	email_status = 2

			-- Set the date to now if we are sending email
			update	client_creditor_balances
			set		zero_bal_letter_date = getdate()
			from	client_creditor_balances b
			inner join #email_notices n on b.client_creditor_balance = n.client_creditor_balance
			where	email_status = 2
		end
	end

	-- Cleanup the table
	drop table #email_notices

	-- Commit the transaction
	COMMIT TRANSACTION
	RETURN ( 1 )
END TRY

BEGIN CATCH
	DECLARE @ErrorMessage NVARCHAR(4000);
	DECLARE @ErrorSeverity INT;
	DECLARE @ErrorState INT;

	SELECT	@ErrorMessage	= ERROR_MESSAGE(),
			@ErrorSeverity	= ERROR_SEVERITY(),
			@ErrorState		= ERROR_STATE();

	if xact_state() = -1
		ROLLBACK TRANSACTION

	RAISERROR (@ErrorMessage, @ErrorSeverity, @ErrorState);
	return ( 0 )
END CATCH
GO
GRANT EXECUTE ON xpr_disbursement_deduct TO public AS dbo;
GO
DENY EXECUTE ON xpr_disbursement_deduct TO www_role;
GO
