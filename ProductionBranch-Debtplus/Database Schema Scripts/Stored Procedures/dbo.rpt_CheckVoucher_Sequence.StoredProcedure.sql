SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER procedure [rpt_CheckVoucher_Sequence] ( @TrustRegister AS INT, @creditor as varchar(10) = null ) AS
-- =============================================================================================
-- ==            Obtain the heading information for the check by sequence ID                  ==
-- =============================================================================================

-- Return the information about the trust register for a check
SELECT	trust_register						as 'trust_register',
	case tran_type when 'BW' then @creditor else creditor end	as 'creditor',
	client								as 'client',
	amount								as 'amount',
	date_created						as 'item_date',
	reconciled_date						as 'reconciled_date',
	left(isnull(cleared,' ')+' ',1)		as 'cleared',
	checknum							as 'checknum',

	case isnull(cleared,' ')
		when 'R' then 'Reconciled' + isnull(' on '+convert(varchar(10), reconciled_date, 1),'')
		when 'V' then 'Voided' + isnull(' on '+convert(varchar(10), reconciled_date, 1),'')
		when 'D' then 'Destroyed in printing'
		when 'E' then 'Cashed' + isnull(' on '+convert(varchar(10), reconciled_date, 1),'')
		when 'P' then 'Pending to be printed'
		else 'Outstanding'
	end							as 'status',

	tran_type						as 'type'

FROM	registers_trust WITH (NOLOCK)
WHERE	trust_register	= @TrustRegister

RETURN ( @@rowcount )
GO
