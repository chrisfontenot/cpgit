USE [DebtPlus]
GO
/****** Object:  StoredProcedure [dbo].[xpr_DisbursementLockCount]    Script Date: 09/15/2014 13:13:35 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
create procedure [dbo].[xpr_DisbursementLockCount](@disbursement_register int = 0) as
-- ================================================================================
-- ==        Return the number of locks on the disbursement tables               ==
-- ================================================================================
begin
	declare	@Answer		int

	-- Count the number of locks on the table
	select  @Answer = count(*)
	from	sys.dm_tran_locks
	where	resource_type = 'OBJECT'
	and		OBJECT_NAME(resource_associated_entity_id) = 'DISBURSEMENT_LOCK'

	-- Return the count of locks to the caller
	return ( isnull(@Answer, 0) )
end
GO
