USE [DebtPlus]
GO
/****** Object:  StoredProcedure [dbo].[DAILY_client_creditor_proposals]    Script Date: 09/15/2014 13:13:32 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE PROCEDURE [dbo].[DAILY_client_creditor_proposals] AS

-- Don't bother about result sets with just the count of records
SET NOCOUNT ON

-- Fetch the system value for acceptance days
DECLARE @system_days INT

SELECT	@system_days = isnull(acceptance_days,30)
FROM	config

-- Create a table to hold the proposal information along with the creditor's values
create table #proposal_default (
	client_creditor_proposal int,
	date_closed datetime,
	acceptance_days int            
)

-- Scan through the proposals to find proposals which are still pending. Update #proposal_default.
INSERT INTO #proposal_default
SELECT	p.client_creditor_proposal		as 'client_creditor_proposal',
	b.date_closed				as 'date_closed',
	isnull(c.acceptance_days,@system_days)	as 'acceptance_days'
FROM	client_creditor_proposals p
inner join proposal_batch_ids b	ON p.proposal_batch_id	= b.proposal_batch_id
INNER JOIN client_creditor l	ON l.client_creditor	= p.client_creditor
INNER JOIN creditors c		ON c.creditor		= l.creditor
WHERE	p.proposal_status = 1
AND	b.date_closed IS NOT NULL

-- Fetch the current date
DECLARE @current_date DATETIME
SET @current_date = getdate()

-- Update the proposals with the DEFAULT acceptance status
UPDATE	client_creditor_proposals
SET	proposal_status = 2,
	proposal_status_date = @current_date
WHERE	client_creditor_proposal IN (
	SELECT	client_creditor_proposal
	FROM	#proposal_default
	WHERE	datediff(day, date_closed, @current_date) > acceptance_days
)

-- Include a system note that the proposal was accepted by default
insert into client_notes (client, client_creditor, is_text, type, dont_edit, dont_delete, dont_print, subject, note )
select		cc.client,
		cc.client_creditor,
		1 as 'is_text',
		3 as 'type',
		1 as 'dont_edit',
		1 as 'dont_delete',
		0 as 'dont_print',
		'Proposal accepted by default',
		'The proposal was accepted by default because it was outstanding for ' + convert(varchar, datediff(day, d.date_closed, @current_date)) + ' days.'

from		client_creditor_proposals p
inner join	#proposal_default d on p.client_creditor_proposal = d.client_creditor_proposal
inner join	client_creditor cc on cc.client_creditor = p.client_creditor

WHERE		datediff(day, d.date_closed, @current_date) > d.acceptance_days

-- Discard the working table
drop table #proposal_default

-- Restore the setting to show the number of rows updated
SET NOCOUNT OFF
return ( @@rowcount )
GO
