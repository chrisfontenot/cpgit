USE [DebtPlus]
GO
/****** Object:  StoredProcedure [dbo].[rpt_housing_no_result]    Script Date: 09/15/2014 13:13:33 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE PROCEDURE [dbo].[rpt_housing_no_result]  ( @From_Date as datetime = null, @To_Date as datetime = null ) as

-- ======================================================================================================
-- ==            List the clients who do not have an open interview for housing                        ==
-- ======================================================================================================

-- Suppress intermediate result sets
set nocount on

if @To_Date is null
	select	@To_Date = getdate()

if @From_Date is null
	select	@From_Date = @To_Date

select	@From_Date = convert(datetime, convert(varchar(10), @From_Date, 101) + ' 00:00:00'),
	@To_Date   = convert(datetime, convert(varchar(10), @To_Date, 101) + ' 23:59:59')

select	i.client as client,
	dbo.format_normal_name(pn.prefix,pn.first,pn.middle,pn.last,pn.suffix) as client_name,
	c.client as hud_id,
	iv.interview_date as interview_created,
	m.description as interview_description,
	c.active_status as active_status,
	c.counselor as counselor,
	coalesce(dbo.format_normal_name(default,cox.first,default,cox.last,default), 'counselor ' + convert(varchar(4), c.counselor), '(no counselor)') as counselor_name,
	c.office as office,
	o.name as office_name
from hud_interviews iv
left outer join client_housing i on iv.client = i.client
left outer join clients c with (nolock) on i.client = c.client
left outer join people p with (nolock) on c.client = p.client and 1 = p.relation
left outer join Names pn with (nolock) on p.NameID = pn.Name
left outer join counselors co with (nolock) on c.counselor = co.counselor
left outer join names cox with (nolock) on co.nameid = cox.name
left outer join offices o with (nolock) on c.office = o.office
left outer join Housing_PurposeOfVisitTypes m with (nolock) on iv.interview_type = m.oID

where	c.active_status in ('A','AR','APT','CRE','PND','PRO','RDY','I','EX','WKS')
and	iv.interview_date between @From_Date and @To_Date
and	iv.result_date is null

order by c.counselor, iv.interview_date

return ( @@rowcount )
GO
