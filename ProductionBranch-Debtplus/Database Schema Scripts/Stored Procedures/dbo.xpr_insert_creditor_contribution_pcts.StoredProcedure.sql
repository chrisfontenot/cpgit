USE [DebtPlus]
GO
/****** Object:  StoredProcedure [dbo].[xpr_insert_creditor_contribution_pcts]    Script Date: 09/15/2014 13:13:35 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[xpr_insert_creditor_contribution_pcts] ( @creditor as varchar(10), @effective_date as datetime = null, @creditor_type_eft as varchar(1) = 'N', @creditor_type_check as varchar(1) = 'N', @fairshare_pct_eft as float = 0.0, @fairshare_pct_check as float = 0.0 ) as
    -- ==========================================================================================================
    -- ==         Create a creditor contribution percentage record                                             ==
    -- ==========================================================================================================
    
    -- Make the effective date "tomrrow" if it is defaulted
    if @effective_date is null
        select @effective_date = CONVERT(varchar(10), dateadd(d, 1, getdate()), 101)
        
    insert into update_creditor_contribution_pcts(creditor, effective_date, creditor_type_eft, creditor_type_check, fairshare_pct_eft, fairshare_pct_check) values (@creditor, @effective_date, @creditor_type_eft, @creditor_type_check, @fairshare_pct_eft, @fairshare_pct_check)
    return (IDENT_CURRENT('creditor_contribution_pcts'))
GO
