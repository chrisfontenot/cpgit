USE [DebtPlus]
GO
/****** Object:  StoredProcedure [dbo].[rpt_referrals_referred_by]    Script Date: 09/15/2014 13:13:33 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[rpt_referrals_referred_by] ( @FromDate as DateTime = NULL, @ToDate as DateTime = NULL, @office as int = null ) AS
-- ==========================================================================================
-- ==           Return the information to generate the referral report                     ==
-- ==========================================================================================

IF @ToDate IS NULL
	SELECT @ToDate = getdate()

IF @FromDate IS NULL
	SELECT @FromDate = @ToDate

-- Adjust the dates for the proper settings

declare	@stmt	varchar(8000)
select	@stmt = '
SELECT		coalesce(typ.description,convert(varchar,ref.referred_by_type),''Unspecified'') as ''referred_by_class'',
		coalesce(ref.description,convert(varchar,c.referred_by),''Unspecified'') as ''referred_by'',
		COUNT(*) AS ''items''
FROM		clients c WITH (NOLOCK)
LEFT OUTER JOIN	referred_by ref WITH (NOLOCK) ON c.referred_by = ref.referred_by
LEFT OUTER JOIN	referred_by_types typ WITH (NOLOCK) ON ref.referred_by_type = typ.referred_by_type'

-- Generate the selection critera for the recordset
declare	@criteria	varchar(800)
select	@criteria =             ' where c.date_created >= ''' + convert(varchar(10), @FromDate, 101) + ' 00:00:00'''
select	@criteria = @criteria + ' and c.date_created <= ''' + convert(varchar(10), @ToDate, 101)     + ' 23:59:59'''

if isnull(@Office,0) > 0
	select	@criteria = @criteria + ' and c.office = ' + convert(varchar, @office)

-- Run the report statement
select @stmt = @stmt + isnull(@criteria,'') + '
	group by ref.description, ref.referred_by_type, c.referred_by, typ.description
	order by typ.description, ref.description;'

exec ( @stmt )
return ( 0 )
GO
