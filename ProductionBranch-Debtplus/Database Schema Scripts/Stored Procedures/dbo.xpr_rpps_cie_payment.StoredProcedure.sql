USE [DebtPlus]
GO
/****** Object:  StoredProcedure [dbo].[xpr_rpps_cie_payment]    Script Date: 09/15/2014 13:13:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[xpr_rpps_cie_payment] ( @client_creditor as int, @client_creditor_register AS INT, @BillerID AS VarChar(20), @bank as int = null, @rpps_file as int = null) AS

-- ========================================================================================================================
-- ==            Create an RPPS transaction for a payment record                                                         ==
-- ========================================================================================================================

-- No intermediate result sets
SET NOCOUNT ON

-- Find the information about the debt
declare	@client		int
declare	@creditor	varchar(10)
select	@client			= client,
		@creditor		= creditor
from	client_creditor
where	client_creditor	= @client_creditor;

-- The bank is not allowed to be null. The file may be (and will be until .NET), but the bank must be defined.
if (@rpps_file is not null) and (@bank is null)
	select	@bank		= bank
	from	rpps_files with (nolock)
	where	rpps_file	= @rpps_file

-- Insert a payment record into the system
INSERT INTO rpps_transactions	(client,	creditor,	client_creditor,	client_creditor_register,	biller_id,	service_class_or_purpose,	transaction_code,	death_date,			rpps_file,	bank)
VALUES				(@client,	@creditor,	@client_creditor,	@client_creditor_register,	@billerID,	'CIE',				22,			dateadd(d, 180, getdate()),	@rpps_file,	@bank);

RETURN ( SCOPE_IDENTITY() )
GO
