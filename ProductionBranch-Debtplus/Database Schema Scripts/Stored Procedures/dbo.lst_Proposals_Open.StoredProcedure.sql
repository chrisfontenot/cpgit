USE [DebtPlus]
GO
/****** Object:  StoredProcedure [dbo].[lst_Proposals_Open]    Script Date: 09/15/2014 13:13:32 ******/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[lst_Proposals_Open] AS

-- ===================================================================================================================
-- ==            Return the list of open proposal batches                                                           ==
-- ===================================================================================================================

select	pr.proposal_batch_id			as 'item_key',
	pr.proposal_type			as 'proposal_type',
	pr.created_by				as 'created_by',
	pr.date_created				as 'date_created',
	pr.note					as 'note',
	convert(varchar(50),case isnull(bk.type,'X')
		when 'C' then 'Printed'
		else coalesce(bk.description,convert(varchar, pr.bank),'')
	end)					as 'bank'

from	proposal_batch_ids pr with (nolock)
left outer join banks bk with (nolock) on pr.bank = bk.bank
where	pr.date_transmitted is null
and	pr.date_closed is null

order by pr.proposal_batch_id
GO
