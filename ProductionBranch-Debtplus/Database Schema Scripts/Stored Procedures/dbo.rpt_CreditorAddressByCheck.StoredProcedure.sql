SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER procedure [rpt_CreditorAddressByCheck] ( @CheckNumber AS BigInt ) AS
-- ============================================================================================
-- ==          Fetch the Creditor ID and Name from the system for a check number             ==
-- ============================================================================================
DECLARE @Creditor         typ_creditor

-- Fetch the detail information for this check
SELECT	@Creditor        = creditor
FROM	registers_trust
WHERE	checknum	 = @CheckNumber
AND	creditor IS NOT NULL
AND	cleared NOT IN ( 'V', 'D' )

-- If the void selection failed then remove it because it could be a legimate voided item.
IF @creditor IS NULL
	SELECT	@Creditor        = creditor
	FROM	registers_trust
	WHERE	checknum	 = @CheckNumber
	AND	creditor IS NOT NULL

-- There must be a trust register (i.e. check) for this to work.
IF @@rowcount < 1
BEGIN
	RaisError (50001, 16, 1, @CheckNumber)
	RETURN ( 0 )
END

-- Go to the routine to actually fetch the data once we have a sequence number.
EXEC xpr_creditor_address_merged @Creditor, 'P'
GO
