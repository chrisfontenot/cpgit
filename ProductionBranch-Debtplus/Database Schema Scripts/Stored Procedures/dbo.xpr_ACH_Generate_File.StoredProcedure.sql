USE [DebtPlus]
GO
/****** Object:  StoredProcedure [dbo].[xpr_ACH_Generate_File]    Script Date: 09/15/2014 13:13:34 ******/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE PROCEDURE [dbo].[xpr_ACH_Generate_File] AS
-- =============================================================================
-- ==        Fetch the list of clients to be generated in the file            ==
-- =============================================================================

SELECT	t.deposit_batch_detail			as 'ach_transaction',
	t.deposit_batch_id			as 'ach_file',
	t.ach_transaction_code			as 'transaction_code',

	isnull(t.ach_routing_number,'MISSING')	as 'routing_number',
	isnull(t.ach_account_number,'MISSING')	as 'account_number',

	case	t.ach_transaction_code
		when '23' then 0
		when '33' then 0
		when '28' then 0
		when '38' then 0
			  else t.amount
	end					as 'amount',

	c.client				as 'client',
	null					as 'discretionary_data',
	t.reference				as 'trace_number',
	LEFT(upper(ltrim(rtrim(isnull(pn.last,'')+isnull(' '+pn.suffix,'')+','+isnull(' '+pn.first,'')+isnull(' '+pn.middle,'')))),22) as 'client_name'

FROM	deposit_batch_details t
INNER JOIN clients c ON t.client=c.client
LEFT OUTER JOIN people p ON c.client = p.client AND 1=p.relation
left outer join names pn with (nolock) on p.nameid = pn.name
WHERE	c.active_status IN ('A', 'AR')
AND	t.reference IS NULL
AND	t.tran_subtype = 'AC'
ORDER BY t.ach_routing_number, t.ach_account_number

return ( @@rowcount )
GO
