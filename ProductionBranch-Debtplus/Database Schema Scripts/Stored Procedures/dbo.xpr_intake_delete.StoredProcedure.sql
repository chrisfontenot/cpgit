USE [DebtPlus]
GO
/****** Object:  StoredProcedure [dbo].[xpr_intake_delete]    Script Date: 09/15/2014 13:13:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[xpr_intake_delete] ( @intake_client as int ) as

-- ==========================================================================================
-- ==            Remove the intake information for the specified client                    ==
-- ==========================================================================================

-- ChangeLog
--    5/9/2006
--       Added support for intake_secured_properties / intake_secured_loans
--       Removed intake_hud_loans and intake_auto_loans

begin transaction
set xact_abort on
set nocount on

delete
from	intake_assets
where	intake_client	= @intake_client

delete
from	intake_budgets
where	intake_client	= @intake_client

delete
from	intake_debts
where	intake_client	= @intake_client

delete	intake_secured_loans
from	intake_secured_loans l
inner join intake_secured_properties p on l.secured_property = p.secured_property
where	p.intake_client	= @intake_client

delete
from	intake_secured_properties
where	intake_client	= @intake_client

delete
from	intake_notes
where	intake_client	= @intake_client

delete
from	intake_people
where	intake_client	= @intake_client

delete
from	intake_clients
where	intake_client	= @intake_client

commit transaction
return ( 1 )
GO
