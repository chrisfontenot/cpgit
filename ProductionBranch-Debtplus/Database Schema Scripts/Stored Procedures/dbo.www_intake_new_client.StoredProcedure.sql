USE [DebtPlus]
GO
/****** Object:  StoredProcedure [dbo].[www_intake_new_client]    Script Date: 09/15/2014 13:13:34 ******/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[www_intake_new_client] ( @housing_status as int, @housing_type as int, @referred_by as int, @cause_fin_problem1 as int, @marital_status as int, @people as int, @bankruptcy as bit = 0 ) AS

-- ============================================================================================================
-- ==            Create the intake session for the client                                                    ==
-- ============================================================================================================

-- Initialize to process the request
set nocount on

-- Default the values as needed
if @housing_status < 0
	select @housing_status = null

if @housing_type < 0
	select @housing_type = null

if @referred_by < 0
	select @referred_by = null

if @cause_fin_problem1 < 0
	select @cause_fin_problem1 = null

if @marital_status < 0
	select @marital_status = null

if @people < 0
	select @people = null

-- Do not permit strange values in the database
if @people is null
	select @people = 1

if @marital_status is null
	select @marital_status = 1

if @housing_status is null
	select @housing_status = 4

if @housing_type is null
	select @housing_type = 10

if isnull(@bankruptcy,0) <> 0
	select	@bankruptcy = 1

declare	@intake_id	varchar(20)
declare	@intake_client	int
declare	@date_start	datetime
declare	@date_stop	datetime
declare	@id_value	int

select	@date_start = convert(datetime, convert(varchar(10), getdate(), 101) + ' 00:00:00'),
	@date_stop  = convert(datetime, convert(varchar(10), getdate(), 101) + ' 23:59:59')

-- Create the client information
insert into intake_clients	( housing_status,  housing_type,  referred_by,  cause_fin_problem1,  marital_status,  people, bankruptcy)
values				(@housing_status, @housing_type, @referred_by, @cause_fin_problem1, @marital_status, @people, isnull(@bankruptcy,0))

select @intake_client = SCOPE_IDENTITY()

-- Update the id for the client
select	@intake_id = max(intake_id)
from	intake_clients
where	date_created between @date_start and @date_stop;

-- If there is no number then use 0000
if @intake_id is null
	select	@id_value = 0
else
	select	@id_value = convert(int, right(@intake_id, 4)) + 1

-- Create the session ID value from the current date and ID
select	@intake_id = right('0000' + convert(varchar, year(getdate())), 4) +
		     right('00'   + convert(varchar, month(getdate())), 2) +
		     right('00'   + convert(varchar, day(getdate())), 2) +
		     right('0000' + convert(varchar, @id_value), 4)

-- Set the session ID string as appropriate
update	intake_clients
set	intake_id	= @intake_id
where	intake_client	= @intake_client;

-- Return the result to the caller
select	@intake_client		as 'intake_client',
	@intake_id		as 'intake_id'

return ( 1 )
GO
