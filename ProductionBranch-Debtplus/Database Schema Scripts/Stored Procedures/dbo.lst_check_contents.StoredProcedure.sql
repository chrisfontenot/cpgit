USE [DebtPlus]
GO
/****** Object:  StoredProcedure [dbo].[lst_check_contents]    Script Date: 09/15/2014 13:13:32 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[lst_check_contents] ( @Trust_Register AS INT, @creditor as varchar(10) = null ) AS

-- ============================================================================================================
-- ==            Return the information for the specific check contents                                      ==
-- ============================================================================================================

-- ChangeLog
--   9/8/2002
--     Added "@creditor" to be used with a bank wire check voucher since the voucher is for all of the creditors
--     that are disbursed on that specific disbursement which receive bank wire transactions.
--   10/23/2002
--     Added cc.client_name as needed
--   12/19/2008
--      Revisions for SQL2008
--   06/09/2009
--      Remove references to debt_number and use client_creditor

-- Suppress intermediate result sets
SET NOCOUNT ON

DECLARE	@tran_type	VarChar(2)
DECLARE	@amount		money
DECLARE	@date_created	datetime
DECLARE	@client		int
DECLARE	@input_creditor	VarChar(10)

SELECT	@tran_type		= tran_type,
		@amount			= amount,
		@client			= client,
		@input_creditor	= creditor,
		@date_created	= date_created
FROM	registers_trust WITH (NOLOCK)
WHERE	trust_register 	= @Trust_Register

-- If the item is a bank wire then use the passed creditor only.
if @tran_type = 'BW'

	SELECT	d.client				as 'client',
			d.creditor				as 'creditor',
			d.client_creditor		as 'client_creditor',
			d.disbursement_register	as 'batch',
			d.debit_amt				as 'amount',
			convert(varchar(50), coalesce (d.account_number, cc.account_number, 'MISSING')) as 'account_number',

			convert(money, case d.creditor_type when 'D' then isnull(d.fairshare_amt,0) else 0 end) as 'deducted',
			convert(money, case when d.creditor_type in ('D','N') then 0 else isnull(d.fairshare_amt,0) end) as 'billed',

			d.invoice_register		as 'invoice_register',

			convert(varchar(80), case
			    when ltrim(rtrim(isnull(cc.client_name,''))) <> '' then ltrim(rtrim(cc.client_name))
			    when p2.person is not null then dbo.format_normal_name(default,p2n.first,p2n.middle,p2n.last,p2n.suffix)
			    else dbo.format_normal_name(default,p1n.first,p1n.middle,p1n.last,p1n.suffix)
			end) as 'name'

		FROM	registers_client_creditor d WITH (NOLOCK)
		LEFT OUTER JOIN client_creditor cc WITH (NOLOCK) ON d.client_creditor = cc.client_creditor
		
		LEFT OUTER JOIN people p2 WITH (NOLOCK) ON cc.person = p2.person
		left outer join names p2n with (nolock) on p2.nameid = p2n.name

		LEFT OUTER JOIN people p1 WITH (NOLOCK) ON cc.client = p1.client and 1 = p1.relation
		left outer join names p1n with (nolock) on p1.nameid = p1n.name

		WHERE	trust_register	= @Trust_Register
		AND	d.tran_type	= @tran_type
		AND	d.creditor	= @creditor
		ORDER BY 1, 2, 3

ELSE IF @input_creditor IS NOT NULL

	SELECT	d.client		as 'client',
		d.creditor		as 'creditor',
		d.client_creditor		as 'client_creditor',
		d.disbursement_register	as 'batch',
		d.debit_amt		as 'amount',
		convert(varchar(50), coalesce (d.account_number, cc.account_number, 'MISSING')) as 'account_number',

		convert(money, case d.creditor_type when 'D' then isnull(d.fairshare_amt,0) else 0 end) as 'deducted',
		convert(money, case when d.creditor_type in ('D','N') then 0 else isnull(d.fairshare_amt,0) end) as 'billed',

		d.invoice_register	as 'invoice_register',

		convert(varchar(80), case
		    when ltrim(rtrim(isnull(cc.client_name,''))) <> '' then ltrim(rtrim(cc.client_name))
		    when p2.person is not null then dbo.format_normal_name(default,p2n.first,p2n.middle,p2n.last,p2n.suffix)
		    else dbo.format_normal_name(default,p1n.first,p1n.middle,p1n.last,p1n.suffix)
		end) as 'name'

	FROM	registers_client_creditor d WITH (NOLOCK)
	LEFT OUTER JOIN client_creditor cc WITH (NOLOCK) ON d.client_creditor = cc.client_creditor
		
	LEFT OUTER JOIN people p2 WITH (NOLOCK) ON cc.person = p2.person
	left outer join names p2n with (nolock) on p2.nameid = p2n.name

	LEFT OUTER JOIN people p1 WITH (NOLOCK) ON cc.client = p1.client and 1 = p1.relation
	left outer join names p1n with (nolock) on p1.nameid = p1n.name
		
	WHERE	trust_register	= @Trust_Register
	AND	d.tran_type	= @tran_type
	ORDER BY 1, 2, 3

ELSE IF @client IS NOT NULL

	SELECT	convert(int,@client)			as 'client',
		convert(varchar(10),null)		as 'creditor',
		convert(int,null)				as 'client_creditor',
		convert(int,0)				as 'batch',
		convert(money,@amount)			as 'amount',
		convert(varchar(50), null)		as 'account_number',
		convert(money,0)			as 'deducted',
		convert(money,0)			as 'billed',
		convert(int,null)			as 'invoice_register',
		convert(varchar(80), isnull(dbo.format_normal_name (pn.prefix, pn.first, pn.middle, pn.last, pn.suffix),'')) as 'name'
	FROM	people p WITH (NOLOCK)
	left outer join names pn with (nolock) on p.nameid = pn.name
	WHERE	p.client = @client
	AND	1 = p.relation

ELSE IF @tran_type = 'DP'

	SELECT	convert(int,null)			as 'client',
		convert(int,null)			as 'creditor',
		convert(int,null)			as 'client_creditor',
		convert(int,0)				as 'batch',
		convert(money,@amount)			as 'amount',
		convert(varchar(50),null)		as 'account_number',
		convert(money,0)			as 'deducted',
		convert(money,0)			as 'billed',
		convert(int,null)			as 'invoice_register',
		convert(varchar(80),'DEPOSIT')		as 'name'

ELSE IF @Tran_Type = 'BI'

	SELECT	convert(int,null)			as 'client',
		convert(int,null)			as 'creditor',
		convert(int,null)			as 'client_creditor',
		convert(int,0)				as 'batch',
		convert(money,@amount)			as 'amount',
		convert(varchar(50),null)		as 'account_number',
		convert(money,0)			as 'deducted',
		convert(money,0)			as 'billed',
		convert(varchar(80),'INTEREST')		as 'name'

ELSE IF @Tran_Type = 'SC'

	SELECT	convert(int,null)			as 'client',
		convert(int,null)			as 'creditor',
		convert(int,null)			as 'client_creditor',
		convert(int,0)				as 'batch',
		convert(money,@amount)			as 'amount',
		convert(varchar(50),null)		as 'account_number',
		convert(money,0)			as 'deducted',
		convert(money,0)			as 'billed',
		convert(varchar(80),'SVC CHARGE')	as 'name'

RETURN ( @@rowcount )
GO
