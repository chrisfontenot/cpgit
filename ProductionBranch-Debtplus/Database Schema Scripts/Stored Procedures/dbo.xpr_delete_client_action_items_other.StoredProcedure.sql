USE [DebtPlus]
GO
/****** Object:  StoredProcedure [dbo].[xpr_delete_client_action_items_other]    Script Date: 09/15/2014 13:13:35 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[xpr_delete_client_action_items_other] ( @action_item int ) as
        
-- Remove references to the description
delete
from    action_items_other
where   action_item_other = @action_item
    
return ( @@rowcount )
GO
