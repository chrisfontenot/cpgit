ALTER PROCEDURE [dbo].[xpr_hecm_create_ocsclient_uploadrecord] (
	@clientid INT
	,@program INT
	,@servicerName VARCHAR(80)
	,@loanNumber VARCHAR(20)
	,@language INT
	,@applicantName INT
	,@coApplicantName INT
	,@address INT
	,@homePhone INT
	,@messagePhone INT
	,@applicantCell INT
	,@applicantWorkPhone INT
	,@coapplicantCell INT
	,@coapplicantWorkPhone INT
	,@InvestorAccountNumber VARCHAR(50)
	)
AS
BEGIN
	DECLARE @is_duplicate BIT
	DECLARE @app_first_name VARCHAR(50)
	DECLARE @app_last_name VARCHAR(50)
	DECLARE @coapp_first_name VARCHAR(50)
	DECLARE @coapp_last_name VARCHAR(50)
	DECLARE @client_street VARCHAR(50)
	DECLARE @client_city VARCHAR(50)
	DECLARE @client_state INT
	DECLARE @client_state_code VARCHAR(50)
	DECLARE @client_zipcode VARCHAR(50)
	DECLARE @home_phone VARCHAR(50)
	DECLARE @coapp_cell VARCHAR(50)
	DECLARE @app_cell VARCHAR(50)
	DECLARE @message_phone VARCHAR(50)
	DECLARE @app_work_phone VARCHAR(50)
	DECLARE @coapp_work_phone VARCHAR(50)
	DECLARE @upload_record_id INT
	DECLARE @timezone VARCHAR(50)
	DECLARE @placeholderloannumber VARCHAR(20)
	DECLARE @ht_queue_code VARCHAR(20)

	SET @ht_queue_code = 'HighTouch'

	-- retrieve the placehodlerloannumber from previous records for the same client
	-- This procedure is called during create and update nation star client
	-- During create, if the loanNumber is empty, the create procedure creates a unique placeholder loan number
	-- During update, if the user enters the loan number, update the loan number in the Loan Number column of 
	--				  otherwise insert the placeholderloannumber 
	-- if there is a client with the loannumber or placeholderloannumber, set the duplicate flag
	SELECT @is_duplicate = 0

	SELECT TOP (1) @placeholderloannumber = LoanNumberPlaceHolder
	FROM ocs_client
	WHERE clientid = @clientid;

	-- get the applicant first and last name to be inserted in the uploadrecord table
	IF @applicantName IS NOT NULL
	BEGIN
		SELECT @app_first_name = First
			,@app_last_name = Last
		FROM Names
		WHERE NAME = @applicantName
	END

	-- get the co-applicant first and last name to be inserted in the uploadrecord table
	IF @coApplicantName IS NOT NULL
	BEGIN
		SELECT @coapp_first_name = First
			,@coapp_last_name = Last
		FROM Names
		WHERE NAME = @coApplicantName
	END

	-- get the address to be inserted in the uploadrecord table
	IF @address IS NOT NULL
	BEGIN
		SELECT @client_street = house + ' ' + direction + ' ' + street + ' ' + suffix
			,@client_city = city
			,@client_state = STATE
			,@client_zipcode = PostalCode
		FROM addresses
		WHERE address = @address

		SELECT @client_state_code = MailingCode
		FROM states
		WHERE STATE = @client_state

		--select @zipcode = PostalCode
		-- from addresses where address = @address
		IF @client_zipcode IS NOT NULL
		BEGIN
			SELECT @timezone = time_zone
			FROM zip_to_timezone
			WHERE zip = @client_zipcode
		END
	END

	IF @homePhone IS NOT NULL
	BEGIN
		SELECT @home_phone = SearchValue
		FROM TelephoneNumbers
		WHERE TelephoneNumber = @homePhone
	END

	IF @messagePhone IS NOT NULL
	BEGIN
		SELECT @message_phone = SearchValue
		FROM TelephoneNumbers
		WHERE TelephoneNumber = @messagePhone
	END

	IF @applicantCell IS NOT NULL
	BEGIN
		SELECT @app_cell = SearchValue
		FROM TelephoneNumbers
		WHERE TelephoneNumber = @applicantCell
	END

	IF @applicantWorkPhone IS NOT NULL
	BEGIN
		SELECT @app_work_phone = SearchValue
		FROM TelephoneNumbers
		WHERE TelephoneNumber = @applicantWorkPhone
	END

	IF @coapplicantCell IS NOT NULL
	BEGIN
		SELECT @coapp_cell = SearchValue
		FROM TelephoneNumbers
		WHERE TelephoneNumber = @coapplicantCell
	END

	IF @coapplicantWorkPhone IS NOT NULL
	BEGIN
		SELECT @coapp_work_phone = SearchValue
		FROM TelephoneNumbers
		WHERE TelephoneNumber = @coapplicantWorkPhone
	END

	DECLARE @newUploadAttempt UNIQUEIDENTIFIER

	SELECT @newUploadAttempt = NewID()

	INSERT INTO [dbo].[OCS_UploadRecord] (
		[Uploaded]
		,[IsDuplicate]
		,[UploadAttempt]
		,[UploadDate]
		,[Servicer]
		,[LoanNumber]
		,[LastName1]
		,[FirstName1]
		,[LastName2]
		,[FirstName2]
		,[ClientStreet]
		,[ClientCity]
		,[ClientState]
		,[ClientZipcode]
		,[Phone1]
		,[Phone2]
		,[Phone3]
		,[Phone4]
		,[Phone5]
		,[Phone6]
		,[InvestorNumber]
		,[Program]
		)
	VALUES (
		1
		,@is_duplicate
		,@newUploadAttempt
		,getdate()
		,@servicerName
		,@loanNumber
		,@app_last_name
		,@app_first_name
		,@coapp_last_name
		,@coapp_first_name
		,@client_street
		,@client_city
		,@client_state_code
		,@client_zipcode
		,@home_phone
		,@message_phone
		,@app_cell
		,@app_work_phone
		,@coapp_cell
		,@coapp_work_phone
		,@InvestorAccountNumber
		,@program
		)

	SELECT @upload_record_id = SCOPE_IDENTITY()

	DECLARE @ocs_client_id INT

	INSERT INTO OCS_Client (
		ClientId
		,UploadRecord
		,StatusCode
		,ContactAttempts
		,Archive
		,InvestorNumber
		,Program
		,ActiveFlag
		,SearchTimezone
		,QueueCode
		,IsDuplicate
		,TrialModification
		,UploadAttempt
		,LoanNumberPlaceHolder
		)
	VALUES (
		@clientid
		,@upload_record_id
		,0
		,0
		,0
		,@InvestorAccountNumber
		,@program
		,1
		,@timezone
		,@ht_queue_code
		,0
		,NULL
		,@newUploadAttempt
		,@placeholderloannumber
		);

	SELECT @ocs_client_id = SCOPE_IDENTITY()

	IF (@loanNumber IS NULL OR @loanNumber = '') AND @placeholderloannumber IS NULL
	BEGIN
		--select @placeholderloannumber=@ocs_client_id;
		UPDATE ocs_client
		SET LoanNumberPlaceHolder = 'DP' + convert(VARCHAR, @ocs_client_id)
		WHERE id = @ocs_client_id
	END

	-- @placeholderloannumber will be null for the first time if the loan
	-- number is not entered
	-- however if the  @placeholderloannumber is not null and loan number is not null
	-- , then replace the loan number with new loan number
	-- 
	IF @placeholderloannumber IS NOT NULL AND (@loanNumber IS NOT NULL OR @loanNumber <> '')
	BEGIN
		UPDATE Housing_lenders
		SET AcctNum = @loanNumber
		WHERE AcctNum = @placeholderloannumber
	END
END
