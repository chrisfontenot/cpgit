USE [DebtPlus]
GO
/****** Object:  StoredProcedure [dbo].[xpr_insert_address]    Script Date: 09/15/2014 13:13:35 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[xpr_insert_address] (@creditor_prefix_1 varchar(256) = '', @creditor_prefix_2 varchar(256) = '', @house varchar(256) = '', @direction varchar(256) = '', @street varchar(256) = '', @suffix varchar(256) = '', @modifier varchar(256) = '', @modifier_value varchar(256) = '', @address_line_2 varchar(256) = '', @address_line_3 varchar(256) = '', @city varchar(256) = '', @state int = 0, @postalcode varchar(256) = '') as
BEGIN
	-- The state must not be null.
	if @state is null
		select @state = MIN(state)
		from   states
		where  [Default] <> 0;
		
	if @state is null
		select	@state = MIN(state)
		from	states;

	-- Add the record
	insert into addresses ([creditor_prefix_1], [creditor_prefix_2], [house], [direction], [street], [suffix], [modifier], [modifier_value], [address_line_2], [address_line_3], [city], [state], [postalcode])
	values (isnull(@creditor_prefix_1,''), isnull(@creditor_prefix_2,''), isnull(@house,''), isnull(@direction,''), isnull(@street,''), isnull(@suffix,''), isnull(@modifier,''), isnull(@modifier_value,''), isnull(@address_line_2,''), isnull(@address_line_3,''), isnull(@city,''), isnull(@state,0), isnull(@postalcode,''))
	return ( scope_identity() )
END
GO
