USE [DebtPlus]
GO
/****** Object:  StoredProcedure [dbo].[rpt_ProductionReport_ClientCount]    Script Date: 09/15/2014 13:13:33 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE PROCEDURE [dbo].[rpt_ProductionReport_ClientCount] ( @FromDate as datetime = null, @ToDate as datetime = null ) as
-- ===================================================================================================================
-- ==               Return the information for the client count                                                     ==
-- ===================================================================================================================

set nocount on

if @ToDate is null
	select @ToDate = getdate()

if @FromDate is null
	select @FromDate = @ToDate

select @FromDate = convert(datetime, convert(varchar(10), @FromDate, 101) + ' 00:00:00'),
	@ToDate = convert(datetime, convert(varchar(10), @ToDate, 101) + ' 23:59:59')

declare	@clients_cre	int
declare	@clients_wks	int
declare	@clients_apt	int
declare	@clients_pnd	int
declare	@clients_rdy	int
declare	@clients_pro	int
declare	@clients_a	int
declare	@clients_ex	int
declare	@clients_i	int

-- Determine the information from the statistics table if there is an item present
select	@clients_cre	= clients_cre,
	@clients_wks	= clients_wks,
	@clients_apt	= clients_apt,
	@clients_pnd	= clients_pnd,
	@clients_rdy	= clients_rdy,
	@clients_pro	= clients_pro,
	@clients_a	= clients_a,
	@clients_ex	= clients_ex,
	@clients_i	= clients_i

FROM	[statistics] with (nolock)
where	period_start	= @ToDate

-- If the information was located then we are able to use these numbers
if @@rowcount = 1
	select	active_status				as 'active_status',
		case upper(active_status)
			when 'CRE' then isnull(@clients_cre,0)
			when 'WKS' then isnull(@clients_wks,0)
			when 'APT' then isnull(@clients_apt,0)
			when 'PND' then isnull(@clients_pnd,0)
			when 'RDY' then isnull(@clients_rdy,0)
			when 'PRO' then isnull(@clients_pro,0)
			when 'A'   then isnull(@clients_a,0)
			when 'EX'  then isnull(@clients_ex,0)
			when 'I'   then isnull(@clients_i,0)
			else 0
		end					as 'count',
		description				as 'description'
	from	active_status with (nolock)
	where	active_status in ('CRE', 'WKS', 'APT', 'PND', 'RDY', 'PRO', 'A', 'EX', 'I')

-- Otherwise, we need to use the current counts
else
	select * from view_client_active_status with (nolock)

return ( @@rowcount )
GO
