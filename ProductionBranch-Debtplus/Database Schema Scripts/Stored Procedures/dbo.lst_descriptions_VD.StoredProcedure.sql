USE [DebtPlus]
GO
/****** Object:  StoredProcedure [dbo].[lst_descriptions_VD]    Script Date: 09/15/2014 13:13:32 ******/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE PROCEDURE [dbo].[lst_descriptions_VD] AS

-- ===================================================================================================
-- ==                Return the list of deposit reasons for the void operations                     ==
-- ===================================================================================================

-- ChangeLog
--   2/01/2002
--     Switch to the messages table

SELECT	item_value		as 'item_key',
	[description]		as 'description'
FROM	messages WITH ( NOLOCK )
WHERE	item_type = 'VD'

return ( @@rowcount )
GO
