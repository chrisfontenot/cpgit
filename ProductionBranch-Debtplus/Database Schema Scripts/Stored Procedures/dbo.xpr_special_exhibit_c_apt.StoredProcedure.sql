USE [DebtPlus]
GO
/****** Object:  StoredProcedure [dbo].[xpr_special_exhibit_c_apt]    Script Date: 09/15/2014 13:13:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[xpr_special_exhibit_c_apt] ( @from_date datetime = null, @to_date datetime = null, @special as int = 1 ) as

-- ChangeLog
--    3/10/2008
--       Corrected issues with Initial/Followup notation.

set nocount on

if @to_date is null
	select @to_date = getdate()

if @from_date is null
	select @from_date = @to_date

select @from_date = convert(varchar(10), @from_date, 101),
	   @to_date = convert(varchar(10), @to_date, 101) + ' 23:59:59'

-- Find the appointment list
create table #client_appointments (ID int not null identity(1,1), client int, start_time varchar(10), counselor varchar(50) null, hud_9902_section varchar(10) null, counseling_hours varchar(50) null, administrative_hours varchar(20) null, initial_followup varchar(10) null, hud_result int null, interview_type int null);

insert into #client_appointments (client, start_time, counselor, hud_9902_section, counseling_hours, administrative_hours, initial_followup, hud_result, interview_type)
select	hi.client,
	convert(varchar(10), hi.interview_date, 101) as start_time,
	isnull(dbo.format_normal_name(default,con.first,default,con.last,default), hi.interview_counselor) as counselor,
	convert(varchar(10), null) as hud_9902_section,
	convert(varchar, round(convert(float, ht.minutes) / 60, 3)) as counseling_hours,
	convert(varchar(20), '0') as administrative_hours,
	convert(varchar(10), 'F') as initial_followup,
	hi.hud_result, hi.interview_type
from hud_interviews hi
inner join hud_transactions ht on hi.hud_interview = ht.hud_interview
inner join counselors co on hi.interview_counselor = co.person
left outer join names con on co.NameID = con.name
-- inner join hud_cars_9902_summary hs on hi.interview_type = hs.hud_interview AND hi.hud_result = hs.hud_result
where hi.interview_date between @from_date and @to_date
and   hi.interview_type is not null
order by 1, 2;

-- Process the interview types based upon the results
update	#client_appointments
set	hud_9902_section	= m.section_label
from	#client_appointments c
inner join hud_cars_9902_summary m with (nolock) on c.interview_type = m.hud_interview and c.hud_result = m.hud_result
where   c.hud_result is not null;

update	#client_appointments
set	hud_9902_section	= m.section_label
from	#client_appointments c
inner join hud_cars_9902_summary m with (nolock) on c.interview_type = m.hud_interview
where   c.hud_9902_section is null
and     m.name like '%_other';

-- Delete the extraneous items
if @special = 0
	delete
	from	#client_appointments
	where	isnull(hud_9902_section,'') not in ('7.c.01', '7.c.02', '7.c.03')

-- Mark the items as initial or followup
update #client_appointments set initial_followup = 'I' where id in (select min(id) from #client_appointments group by client)

-- Create a results table with the item results for the workshop
create table #results ( page varchar(80), location varchar(20), value varchar(20));

-- Break the information down by line
declare @current_lnno int
declare @current_page varchar(80)
select  @current_lnno = 10, @current_page = '2' -- '2. 1-On-1 Activity Log'

declare @current_hud_9902_section varchar(20)
declare @current_client varchar(20)
declare @current_start_time varchar(10)
declare @current_counselor varchar(80)
declare @current_counseling_hours varchar(20)
declare @current_administrative_hours varchar(20)
declare @current_initial_followup varchar(10)

declare item_cursor cursor for
	select client, start_time, counselor, hud_9902_section, counseling_hours, administrative_hours, initial_followup
	from	#client_appointments
	order by client, id

open item_cursor
fetch item_cursor into @current_client, @current_start_time, @current_counselor, @current_hud_9902_section, @current_counseling_hours, @current_administrative_hours, @current_initial_followup

while @@fetch_status = 0
begin

	-- Include information common to all items
	insert into #results (page, location, value)
	select @current_page, 'A' + convert(varchar, @current_lnno), @current_hud_9902_section
	union all
	select @current_page, 'B' + convert(varchar, @current_lnno), @current_client
	union all
	select @current_page, 'C' + convert(varchar, @current_lnno), @current_start_time
	union all
	select @current_page, 'D' + convert(varchar, @current_lnno), @current_counselor
	union all
	select @current_page, 'E' + convert(varchar, @current_lnno), @current_counseling_hours
	union all
	select @current_page, 'F' + convert(varchar, @current_lnno), @current_administrative_hours
	union all
	select @current_page, 'G' + convert(varchar, @current_lnno), @current_initial_followup

	select	@current_lnno = @current_lnno + 1

	fetch item_cursor into @current_client, @current_start_time, @current_counselor, @current_hud_9902_section, @current_counseling_hours, @current_administrative_hours, @current_initial_followup
end

close item_cursor
deallocate item_cursor

-- Return the results from the selections
select * from #results order by page, location
drop table #results
drop table #client_appointments

return ( 0 )
GO
