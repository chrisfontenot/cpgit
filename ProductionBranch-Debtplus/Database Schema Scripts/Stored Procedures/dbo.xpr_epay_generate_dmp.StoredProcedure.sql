USE [DebtPlus]
GO
/****** Object:  StoredProcedure [dbo].[xpr_epay_generate_dmp]    Script Date: 09/15/2014 13:13:35 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[xpr_epay_generate_dmp] ( @epay_file as int ) as

-- ==================================================================================================
-- ==            Generate the proposal messages                                                    ==
-- ==================================================================================================

-- Suppress intermediate results
set nocount on

declare		@bank	int
select		@bank		= bank
from		epay_files with (nolock)
where		epay_file	= @epay_file

select		p.client_creditor_proposal, cc.client, cc.account_number, ids.epay_biller_id, pb.proposal_batch_id
into		#epay_proposals
from		proposal_batch_ids pb
inner join	client_creditor_proposals p	with (NOLOCK) ON pb.proposal_batch_id = p.proposal_batch_id
inner join	client_creditor cc		with (NOLOCK) ON p.client_creditor = cc.client_creditor
INNER JOIN	clients c			WITH (NOLOCK) ON cc.client = c.client
INNER JOIN	creditors cr			WITH (NOLOCK) ON cc.creditor = cr.creditor
INNER JOIN	epay_biller_ids ids		WITH (NOLOCK) ON p.epay_biller_id = ids.epay_biller_id

WHERE		pb.date_closed IS NOT NULL		-- Closed but
AND		pb.date_transmitted IS NULL		-- Not transmitted
AND		p.bank = @bank				-- Want the specific bank DI
AND		p.epay_biller_id IS NOT NULL		-- Must have a biller ID for the table
AND		c.active_status not in ('CRE', 'EX', 'I')

-- Load the transactions
insert into epay_transactions (request, client, account_number, client_creditor_proposal, epay_biller_id, epay_file)
select		'1', client, account_number, client_creditor_proposal, epay_biller_id, @epay_file
from		#epay_proposals

-- Allocate a cursor to assign the proposals for the same client a sequence value
declare	@last_client		int
declare	@client			int
declare	@last_debt		int
declare	@epay_transaction	int

declare	item_cursor cursor for
	select	epay_transaction, client
	from	epay_transactions
	where	epay_file	= @epay_file
	order by client, epay_biller_id;

open	item_cursor
fetch	item_cursor into @epay_transaction, @client

select	@last_client	= @client,
	@last_debt	= 0;

while @@fetch_status = 0
begin
	if @last_client <> @client
		select	@last_client	= @client,
			@last_debt	= 0;

	select	@last_debt		= @last_debt + 1;

	update	epay_transactions
	set	creditor_number		= @last_debt
	where	epay_transaction	= @epay_transaction;

	fetch	item_cursor into @epay_transaction, @client
end

close item_cursor
deallocate item_cursor

-- Mark the files as having been transmitted
update	proposal_batch_ids
set	date_transmitted	= getdate()
from	proposal_batch_ids pb
inner join #epay_proposals p on pb.proposal_batch_id = p.proposal_batch_id

-- Discard the working table
drop table #epay_proposals

return ( 1 )
GO
