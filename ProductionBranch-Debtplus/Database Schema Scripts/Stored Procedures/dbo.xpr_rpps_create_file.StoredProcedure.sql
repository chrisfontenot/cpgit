USE [DebtPlus]
GO
/****** Object:  StoredProcedure [dbo].[xpr_rpps_create_file]    Script Date: 09/15/2014 13:13:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[xpr_rpps_create_file] ( @bank as int, @filename as varchar(80) = null, @file_type as varchar(3) = 'EFT' ) AS
-- ============================================================================================================
-- ==            Create the file record for the RPPS file                                                    ==
-- ============================================================================================================

-- Do not generate intermediate result sets
SET NOCOUNT ON

-- Create the row in the tables
INSERT INTO rpps_files	(created_by,	date_created,	file_type,	bank,	filename)
VALUES			(suser_sname(),	getdate(),	@file_type,	@bank,	@filename)

RETURN ( SCOPE_IDENTITY() )
GO
