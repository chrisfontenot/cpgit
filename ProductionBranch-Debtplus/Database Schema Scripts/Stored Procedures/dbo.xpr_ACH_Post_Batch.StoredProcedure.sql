IF EXISTS(SELECT * FROM SYSOBJECTS WHERE TYPE='P' AND NAME=N'xpr_ACH_Post_Batch')
    EXEC('DROP PROCEDURE xpr_ACH_Post_Batch')
GO
CREATE PROCEDURE [dbo].[xpr_ACH_Post_Batch] ( @ACHFile AS INT ) AS
-- =================================================================================
-- ==          Post the ACH batch                                                 ==
-- =================================================================================

-- Start a transaction
BEGIN TRANSACTION

SET XACT_ABORT ON
SET NOCOUNT ON

BEGIN TRY
	DECLARE	@trust_register		INT
	DECLARE @effective_date		DateTime
	DECLARE	@total_amount		MONEY
	DECLARE	@note				varchar(50)
	DECLARE	@bank				int
	DECLARE	@current_date		datetime

	-- Magic marker for "there is none" in the case of a product operation.
	SET @trust_register = -1

	-- Fetch the information from the file header
	SELECT	@effective_date = ach_effective_date,
			@current_date	= getdate(),
			@bank			= bank,
			@note			= note
	FROM	deposit_batch_ids
	WHERE	deposit_batch_id = @ACHFile
	AND		date_posted is null

	IF @@rowcount < 1
		RaisError(50032, 16, 1, @ACHFile)

	-- Find the main trust account
	if @bank is null
		select	@bank		= min(bank)
		from	banks with (nolock)
		where	type		= 'C'
		and		[default]	= 1

	if @bank is null
		select	@bank		= min(bank)
		from	banks with (nolock)
		where	type		= 'C'

	-- If there is not one, punt.
	if @bank is null
		select	@bank = 1

	-- Correct the label for the subsequent update of the registers_trust table
	if @note is null
		select	@note = ''

	select	@note = ltrim(rtrim(@note))
	if @note <> ''
	begin
		select	@note = '#' + convert(varchar, @ACHFile) + ' ''' + @note
		if len(@note) > 49
			select	@note = left(@note, 49)
		select	@note = @note + ''''
	end else
		select	@note = '#' + convert(varchar, @ACHFile)

	-- Process a product transaction group first
	SELECT		deposit_batch_detail, client_product, convert(datetime, convert(varchar(10), date_created, 101) + ' 00:00:00') as date_created, amount
	INTO		#t_product_by_date
	FROM		deposit_batch_details d
	WHERE		deposit_batch_id = @ACHFile
	AND			ok_to_post = 1
	AND			client_product IS NOT NULL
	AND			date_posted IS NULL

	-- ------------------------------------------------------------------------------
	-- --       PRODUCTS                                                          ---
	-- ------------------------------------------------------------------------------

	IF EXISTS(SELECT * FROM #t_product_by_date)
	BEGIN
		-- Update the tendered amount for the various items
		update		client_products
		set			tendered = tendered + amount,
					invoice_status = 4
		from		client_products p
		inner join	#t_product_by_date x on p.oID = x.client_product

		-- Generate the detail transaction for the items
		insert into product_transactions (transaction_type, client_product, vendor, product, payment)
		select		'RC', x.client_product, p.vendor, p.product, x.amount
		from		#t_product_by_date x
		inner join	client_products p on x.client_product = p.oID

		-- Mark the items as having been posted
		update		deposit_batch_details
		set			date_posted			= getdate()
		from		deposit_batch_details d
		inner join #t_product_by_date x on d.deposit_batch_detail = x.deposit_batch_detail

		-- Indicate that the batch has been posted
		update		deposit_batch_ids
		SET			date_posted			= getdate(),
					posted_by			= suser_sname()
		WHERE		deposit_batch_id	= @ACHFile
	END
	drop table #t_product_by_date

	-- Find the ACH transactions that are for a DMP client
	SELECT		[deposit_batch_detail],[deposit_batch_id],[tran_subtype],[ok_to_post],[scanned_client],[client],[creditor],[client_creditor],[client_product],[amount],[fairshare_amt],[fairshare_pct],[creditor_type],[item_date],[reference],[message],[ach_transaction_code],[ach_routing_number],[ach_account_number],[ach_authentication_code],[ach_response_batch_id],[date_posted]
	INTO		#t_client_by_date
	FROM		deposit_batch_details d
	WHERE		deposit_batch_id = @ACHFile
	AND			ok_to_post = 1
	AND			amount > 0
	AND			client > 0
	AND			ach_transaction_code IN ('27', '37')
	AND			client_product IS NULL
	AND			date_posted IS NULL

	-- ------------------------------------------------------------------------------
	-- --       DMP                                                               ---
	-- ------------------------------------------------------------------------------

	IF EXISTS(SELECT * FROM #t_client_by_date)
	BEGIN
		-- Insert the item into the trust register
		insert into registers_trust	(tran_type, amount, cleared, bank, sequence_number, date_created)
		values				('DP',	0, ' ', @bank, @note, @current_date)
		select	@trust_register = SCOPE_IDENTITY()

		-- Insert the transactions into the system
		insert into registers_client (tran_type, tran_subtype, client, credit_amt, message, trust_register, item_date, date_created)
		select		'DP', 'AC', d.client, d.amount, d.reference, @trust_register, isnull(@effective_date,@current_date), @current_date
		FROM		#t_client_by_date d
		LEFT OUTER JOIN	ach_reject_codes e ON d.ach_authentication_code = e.ach_reject_code
		WHERE		( d.ach_authentication_code IS NULL OR isnull(e.serious_error,1) = 0 )

		-- Fetch the client and the deposit amount
		select		d.client, sum(d.amount) as amount
		into		#t_ach_deposits
		from		#t_client_by_date d
		LEFT OUTER JOIN	ach_reject_codes e ON d.ach_authentication_code = e.ach_reject_code
		where		( d.ach_authentication_code IS NULL OR isnull(e.serious_error,1) = 0 )
		GROUP BY	d.client

		-- Update the trust balance and last deposit date/amount
		update		clients
		set			held_in_trust		= isnull(c.held_in_trust,0)    + x.amount,
					deposit_in_trust	= isnull(c.deposit_in_trust,0) - x.amount,
					last_deposit_date	= @current_date,
					last_deposit_amount	= x.amount
		from		clients c
		inner join	#t_ach_deposits x on c.client = x.client

		-- Mark the items as having been posted
		update		deposit_batch_details
		set			date_posted			= getdate()
		from		deposit_batch_details d
		inner join #t_ach_deposits x on d.deposit_batch_detail = x.deposit_batch_detail

		-- Set the first deposit date
		update		clients
		set			first_deposit_date	= @current_date
		from		clients c
		inner join	#t_ach_deposits x on c.client = x.client
		where		c.first_deposit_date is null

		-- Determine the total deposit amount
		SELECT		@total_amount = sum(amount)
		FROM		#t_ach_deposits

		-- Update the trust register with the deposit amount and cleared status
		UPDATE		registers_trust
		SET			amount = isnull(@total_amount,0),
					cleared = ' '
		WHERE		trust_register = @trust_register

		-- Update the retention events to expire items that are based upon deposits
		update		client_retention_events
		set			date_expired = @current_date
		from		client_retention_events e
		left outer join	#t_ach_deposits x on e.client = x.client and 2 = e.expire_type
		where		e.date_expired is null
		and			e.amount <= x.amount;

		drop table	#t_ach_deposits

		-- Find the list of clients which will have the error status changed
		select		c.client
		into		#ach_clients
		from		clients c with (nolock)
		inner join	#t_client_by_date d with (nolock) on c.client = d.client
		INNER JOIN	ach_reject_codes e WITH (NOLOCK) ON d.ach_authentication_code = e.ach_reject_code
		where		d.ach_authentication_code is not null
		AND			isnull(e.serious_error,1) > 0

		-- Set the error date to today for the clients
		UPDATE		client_ach
		SET			ErrorDate	= getdate()
		from		client_ach c
		inner join	#ach_clients ac on c.client = ac.client

		-- Generate a system note that the ACH failed
		insert into client_notes (client, type, dont_edit, dont_delete, subject, note, date_created)
		select		client, 3, 1, 1,
					'Deposit Information Changed',
					'Error date changed to ' + convert(varchar(10), @current_date, 101),
					@current_date
		from		#ach_clients

		-- Discard the working table
		drop table	#ach_clients

		-- Mark the item as "closed"
		UPDATE		deposit_batch_ids
		SET			date_posted = getdate(),
					trust_register = @trust_register
		WHERE		deposit_batch_id = @ACHFile
	END

	COMMIT TRANSACTION

	RETURN ( @trust_register )
END TRY

BEGIN CATCH
	DECLARE @ErrorMessage NVARCHAR(4000);
	DECLARE @ErrorSeverity INT;
	DECLARE @ErrorState INT;

	SELECT	@ErrorMessage	= ERROR_MESSAGE(),
			@ErrorSeverity	= ERROR_SEVERITY(),
			@ErrorState		= ERROR_STATE();

	if @@trancount > 0
	BEGIN
		BEGIN TRY
			ROLLBACK TRANSACTION
		END TRY
		BEGIN CATCH
		END CATCH
	END

	RAISERROR (@ErrorMessage, @ErrorSeverity, @ErrorState);
	return ( 0 )
END CATCH
GO
grant execute on xpr_ACH_Post_Batch to public as dbo
GO
