USE [DebtPlus]
GO
/****** Object:  StoredProcedure [dbo].[xpr_insert_config_fee]    Script Date: 09/15/2014 13:13:35 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[xpr_insert_config_fee] (@Default as bit,@ActiveFlag as bit,@fee_percent as float,@fee_per_creditor as money,@fee_maximum as money,@description as varchar(50),@fee_type as int,@fee_base as money,@fee_disb_max as money,@fee_monthly_max as money,@fee_balance_max as bit,@fee_balance_inc as money,@fee_sched_payment as money,@state as int) as
INSERT INTO config_fees ([Default],[ActiveFlag],[fee_percent],[fee_per_creditor],[fee_maximum],[description],[fee_type],[fee_base],[fee_disb_max],[fee_monthly_max],[fee_balance_max],[fee_balance_inc],[fee_sched_payment],[state]) VALUES (@Default,@ActiveFlag,@fee_percent,@fee_per_creditor,@fee_maximum,@description,@fee_type,@fee_base,@fee_disb_max,@fee_monthly_max,@fee_balance_max,@fee_balance_inc,@fee_sched_payment,@state);
return ( scope_identity() )
GO
