USE [DebtPlus]
GO
/****** Object:  StoredProcedure [dbo].[xpr_deposit_batch_create]    Script Date: 09/15/2014 13:13:35 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE PROCEDURE [dbo].[xpr_deposit_batch_create] ( @Note as VarChar(256) = NULL, @batch_type as varchar(2) = 'CL', @bank as int = null ) AS

-- ==================================================================================================
-- ==               Create the deposit batch                                                       ==
-- ==================================================================================================

-- ChangeLog
--   11/18/2002
--     Added bank information.

SET NOCOUNT ON
DECLARE	@deposit_batch_id	INT

if @Note is not null
begin
	set @Note = ltrim(rtrim(@Note))
	if @Note = ''
		set @Note = NULL
end

-- Ensure that the bank account is valid
if @bank is not null
	if not exists (select	*
			from	banks
			where	bank	= @bank
			and	type in ('C', 'D', 'A'))
		select	@bank = null

if @bank is null
	select	@bank		= bank
	from	banks
	where	type		= 'C'
	and	[default]	= 1

if @bank is null
	select	@bank		= bank
	from	banks
	where	type		= 'C'

if @bank is null
	select	@bank = 1

-- Start a transaction to ensure that everything is consistent
BEGIN TRANSACTION

-- Create a transaction
INSERT INTO deposit_batch_ids	(date_created,	created_by,	trust_register, date_closed,	date_posted,	note,	batch_type,	bank)
VALUES				(getdate(),	suser_sname(),	null,		null,		null,		@Note,	@batch_type,	@bank)

SELECT @deposit_batch_id = SCOPE_IDENTITY()

-- Ensure that the transaction is complete now
COMMIT TRANSACTION

-- Return the resulting batch ID to the caller
RETURN ( @deposit_batch_id )
GO
