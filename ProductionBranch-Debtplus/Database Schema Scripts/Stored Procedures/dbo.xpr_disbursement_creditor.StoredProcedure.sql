SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER procedure [xpr_disbursement_creditor] ( @Creditor AS VarChar(10) ) AS

-- ===========================================================================================
-- ==            Retrieve the creditor information                                          ==
-- ===========================================================================================

-- ChangeLog
--   08/20/2004
--     Removed fairshare_pct_* and creditor_type_* from the creditor. Added creditor_contribution_pct link.

-- Turn off intermediate result sets
SET NOCOUNT ON

-- Information for the creditor
DECLARE	@Prohibit_Use			int
DECLARE	@voucher_spacing		int
DECLARE	@max_clients_per_check	int
DECLARE	@max_amt_per_check		Money
DECLARE	@fairshare_pct_check	Float
DECLARE @fairshare_pct_eft		Float
DECLARE	@creditor_type_check	typ_creditor_type
DECLARE	@creditor_type_eft		typ_creditor_type
DECLARE	@Test_Creditor			typ_creditor
DECLARE	@System_Max_Dollars		Money
DECLARE	@System_Max_Clients		Money
DECLARE	@bank					int

-- Retrieve the information from the creditor record
SELECT	@Test_Creditor		= cr.creditor,
	@Prohibit_use			= cr.prohibit_use,
	@voucher_spacing		= cr.voucher_spacing,
	@max_clients_per_check	= cr.max_clients_per_check,
	@max_amt_per_check		= cr.max_amt_per_check,
	@bank					= cr.bank,

	@fairshare_pct_check	= isnull(pct.fairshare_pct_check,0),
	@fairshare_pct_eft		= isnull(pct.fairshare_pct_eft,0),
	@creditor_type_check	= isnull(pct.creditor_type_check,'N'),
	@creditor_type_eft		= isnull(pct.creditor_type_eft,'N')

FROM	creditors cr WITH (NOLOCK)
left outer join creditor_conribution_pcts with (nolock) on cr.creditor_contribution_pct = pct.creditor_contribution_pct
WHERE	cr.creditor			= @Creditor

-- Limit the values to the configuration data
if isnull(@bank,0) <= 0
	select	@bank		= min(bank)
	from	banks with (nolock)
	where	type		= 'C'
	and	[default]	= 1

if isnull(@bank,0) <= 0
	select	@bank		= min(bank)
	from	banks with (nolock)
	where	type		= 'C'

if isnull(@bank,0) <= 0
	select	@bank		= 1

SELECT	@System_Max_Dollars	= isnull(max_amt_per_check,0),
		@System_Max_Clients	= isnull(max_clients_per_check,33)
FROM	banks with (nolock)
WHERE	bank			= @bank

-- If the creditor does not exist, do not permit disbursements.
IF @Test_Creditor IS NULL
	SET @Prohibit_Use		= 2

IF isnull(@max_clients_per_check,0) < 1 or @max_clients_per_check > @System_Max_Clients
	SET @max_clients_per_check = @System_Max_Clients

IF @System_Max_Dollars > 0
BEGIN
	IF isnull(@max_amt_per_check,0) <= 0 or @max_amt_per_check > @System_Max_Dollars
		SET @max_amt_per_check = @System_Max_Dollars
END

-- If the voucher is double spaced then use only 1/2 the number permitted on the voucher
IF isnull(@voucher_spacing,0) = 1
	SET @max_clients_per_check = (isnull(@max_clients_per_check,0) + 1) / 2

-- If the check has no voucher then make the limit verrrrry large. Do not use 0. It would mean "none".
ELSE IF isnull(@voucher_spacing,0) = 2
	SET @max_clients_per_check = 2147483647	-- 0x7FFFFFFF or System.Int32.MaxValue

-- Return the values to the caller
SELECT	isnull(@Prohibit_use,0)			as prohibit_use,
	isnull(@voucher_spacing,0)			as voucher_spacing,
	isnull(@max_clients_per_check,0)	as max_clients_per_chk,
	isnull(@max_amt_per_check,0)		as max_amt_per_chk,
	isnull(@fairshare_pct_check,0)		as fairshare_pct,
	isnull(@fairshare_pct_eft,0)		as eft_pct,
	isnull(@creditor_type_eft,'B')		as creditor_type_eft,
	isnull(@creditor_type_check,'B')	as creditor_type_check

RETURN ( 1 )
GO
