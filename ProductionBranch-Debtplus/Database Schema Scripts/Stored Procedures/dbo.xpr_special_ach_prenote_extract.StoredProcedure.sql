USE [DebtPlus]
GO
/****** Object:  StoredProcedure [dbo].[xpr_special_ach_prenote_extract]    Script Date: 09/15/2014 13:13:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[xpr_special_ach_prenote_extract] as

-- Special for Richmond -- Generate the ACH prenote information that their bank wants rather than the standard NACHA formatted file

declare	@last_error	int
declare	@row_count	int

begin transaction T1
set xact_abort on

-- Find the clients who have not been prenoted and are acceptable for a prenote operation
select	convert(varchar(50), '11111111') as company_id,
	convert(varchar(50), isnull(dbo.format_ssn ( p.ssn ),'000-00-0000')) as ssn,
	c.client as client,
	convert(varchar(50), isnull(dbo.format_normal_name ( default, default, default, pn.last, pn.suffix ),''))  as last_name,
	convert(varchar(50), isnull(pn.first,'')) as first_name,
	convert(varchar(2), case isnull(ach.CheckingSavings,'C') when 'S' then '37' else '27' end) as account_type,
	convert(money,0) as amount,
	convert(int,0) as pull_day,
	convert(varchar(10), ach.StartDate, 101) as start_date,
	convert(varchar(50), ach.AccountNumber) as account_number,
	convert(varchar(9), ach.ABA) as routing_number
into	#ach_pull_items
from	clients c with (nolock)
inner join client_ach ach with (nolock) on c.client = ach.client
inner join people p with (nolock) on c.client = p.client and 1 = p.relation	-- Must have a primary name record. Will not allow empty items.
left outer join names pn with (nolock) on p.nameid = pn.name
where	ach.isActive = 1		-- Must be active on ACH
and	ach.ABA like '[0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9]'	-- ABA number is exactly 9 digits, no more, no less
and	ach.ABA <> '000000000'						-- ZEROS are illegal
and	ach.AccountNumber like '[0-9]%'	-- Must have an account number that starts with a numeric at the very least -- no blanks, etc.
and	ach.PrenoteDate is null	-- Must not have a prenote date

-- Look for the exceptional information
select	@last_error	= @@error,
		@row_count	= @@rowcount

if @last_error <> 0
	goto ErrorQuit

if @row_count = 0
	goto EmptyDataSet

-- Find the deposit information
update	#ach_pull_items
set	pull_day	= datepart(d, deposit_date)
from	#ach_pull_items i
inner join client_deposits d on i.client = d.client
where	d.one_time	= 0		-- Must not be the first time as this is a prenote for the other items
-- and		d.ach_pull	= 1		-- Must be configured for ACH

-- Look for the exceptional information
select	@last_error	= @@error,
	@row_count	= @@rowcount

if @last_error <> 0
	goto ErrorQuit

select	d.client, sum(d.deposit_amount) as amount
into	#amounts
from	#ach_pull_items i
inner join client_deposits d on i.client = d.client and i.pull_day = datepart(d, d.deposit_date)
where	d.one_time	= 0
-- and		d.ach_pull	= 1
group by d.client;

-- Look for the exceptional information
select	@last_error	= @@error,
	@row_count	= @@rowcount

if @last_error <> 0
	goto ErrorQuit

update	#ach_pull_items
set	amount	= x.amount
from	#ach_pull_items i
inner join #amounts x on i.client = x.client;

-- Look for the exceptional information
select	@last_error	= @@error,
	@row_count	= @@rowcount

if @last_error <> 0
	goto ErrorQuit

drop table #amounts;

-- Set the prenote date for the clients that we will be returning
update  client_ach
set		PrenoteDate	= getdate()
from	client_ach ach
inner join #ach_pull_items i on ach.client = i.client
where	ach.PrenoteDate is null

-- Look for the exceptional information
select	@last_error	= @@error,
	@row_count	= @@rowcount

if @last_error <> 0
	goto ErrorQuit

-- Return the information from the request
EmptyDataSet:

select	company_id, ssn as ssan, dbo.format_client_id ( client ) as 'client id', last_name as 'name last', first_name as 'name first', account_type as 'account type', amount, pull_day as 'pull day', start_date as 'start date', account_number as 'account number', routing_number as 'rtg number'
from	#ach_pull_items
order by routing_number, account_number;

-- Clean up the tables
drop table #ach_pull_items;

-- Return success
commit transaction T1
return ( 0 )

-- Error exception
ErrorQuit:
	Rollback transaction T1
	RaisError ( 16, 1, @last_error )
	return ( 1 )
GO
