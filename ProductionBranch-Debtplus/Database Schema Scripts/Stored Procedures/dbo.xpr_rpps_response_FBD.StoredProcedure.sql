USE [DebtPlus]
GO
/****** Object:  StoredProcedure [dbo].[xpr_rpps_response_FBD]    Script Date: 09/15/2014 13:13:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[xpr_rpps_response_FBD] ( @response_file as int, @trace_number as varchar(15), @biller_id as varchar(10), @return_code as varchar(3), @name as varchar(5) = null, @account_number as varchar(22) = null, @net as money = 0 ) as

-- =======================================================================================================================
-- ==            Generate the response for a FBD request                                                                ==
-- =======================================================================================================================

declare	@rpps_response_detail	int
execute @rpps_response_detail = xpr_rpps_response @response_file, @trace_number, @biller_id, @return_code, 'FBD', @name, @account_number, @net

return ( @rpps_response_detail )
GO
