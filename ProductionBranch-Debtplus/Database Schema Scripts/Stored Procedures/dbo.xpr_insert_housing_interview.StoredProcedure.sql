USE [DebtPlus]
GO
/****** Object:  StoredProcedure [dbo].[xpr_insert_housing_interview]    Script Date: 09/15/2014 13:13:35 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[xpr_insert_housing_interview](@client int, @interview_type int, @HUD_grant as int = null, @hud_result int = null, @termination_reason int = null, @HousingFeeAmount as money = 0) as
-- ========================================================================================
-- ==             Create a housing interview record                                      ==
-- ========================================================================================

-- Suppress intermediate results
set nocount on

-- Ensure that there is a grant
if @hud_Grant is null
	select	@hud_grant = hud_grant
	from	[client_housing] with (nolock)
	where	[client] = @hud_grant;

if @hud_grant is null
	select	@hud_grant = 0

-- Ensure that there is an amount
if @HousingFeeAmount is null
	select	@HousingFeeAmount = 0

-- Insert the row and return the pointer to the new row
-- (A trigger will fire here and update the timestamps if needed)
declare @hud_interview	int
insert into hud_interviews([client],[HUD_Grant],[interview_type],[interview_date],[interview_counselor],[hud_result],[termination_reason],[HousingFeeAmount]) values (@client, @HUD_Grant, @interview_type, getdate(), suser_sname(), @hud_result, @termination_reason, @HousingFeeAmount)

-- Retrieve the ID of the new row
select	@hud_interview = scope_identity()

-- Pass along the interview to the custom event handler
execute xpr_housing_custom_events @hud_interview

-- Return the ID of the interview to the caller
return ( @hud_interview )
GO
