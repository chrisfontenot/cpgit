USE [DebtPlus]
GO
/****** Object:  StoredProcedure [dbo].[UPDATE_creditor_first_payment]    Script Date: 09/15/2014 13:13:34 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[UPDATE_creditor_first_payment] as

-- Create the first_payment and last_payment fields in the creditor table
update	creditors
set	first_payment = null,
	last_payment = null;

select creditor, min(date_created) as date_created, convert(int, null) as creditor_register
into #first_payment
from registers_creditor
where tran_type in ('AD', 'BW', 'MD', 'CM')
group by creditor;

update #first_payment
set creditor_register = rc.creditor_register
from #first_payment x
inner join registers_creditor rc on rc.creditor = x.creditor and rc.tran_type in ('AD', 'BW', 'MD', 'CM') and rc.date_created = x.date_created

update creditors
set first_payment = x.creditor_register
from creditors cr
inner join #first_payment x on x.creditor = cr.creditor

drop table #first_payment

select creditor, max(date_created) as date_created, convert(int, null) as creditor_register
into #last_payment
from registers_creditor
where tran_type in ('AD', 'BW', 'MD', 'CM')
group by creditor;

update #last_payment
set creditor_register = rc.creditor_register
from #last_payment x
inner join registers_creditor rc on rc.creditor = x.creditor and rc.tran_type in ('AD', 'BW', 'MD', 'CM') and rc.date_created = x.date_created

update creditors
set last_payment = x.creditor_register
from creditors cr
inner join #last_payment x on x.creditor = cr.creditor

drop table #last_payment
GO
