USE [DebtPlus]
GO
/****** Object:  StoredProcedure [dbo].[rpt_Proposal_Proposal]    Script Date: 09/15/2014 13:13:33 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[rpt_Proposal_Proposal] ( @client_creditor_proposal AS INT = 0, @proposal_mode AS INT = 0, @proposal_batch_label as typ_message = null ) AS

-- =============================================================================================================
-- ==            Print the full disclosure proposal                                                           ==
-- =============================================================================================================

-- ChangeLog
--   11/2/2001
--     -  Corrected system note to print when "reprint" is used.
--     -  Allowed the printed batch to not be closed but still printed.
--   9/31/2002
--     Added support for client_creditor_balances table
--   10/1/2003
--     Expanded the proposal_message field to "text" format.
--   3/14/2003
--     Allow for more than 10,000 creditors in a class
--   12/4/2006
--     Added min_start_date to ensure that the debts start date is at least 5 days in the future.

-- Suppress intermediate results
set nocount on

-- Earliest value for the proposed start date
declare	@min_start_date		datetime
select	@min_start_date		= dbo.date_only ( dateadd ( d, 5, getdate() ));

-- ====================================================================================
-- ==                If choosing a batch by name, fetch the numeric batch id         ==
-- ====================================================================================
if @proposal_mode = 1
begin
	select	@client_creditor_proposal	= proposal_batch_id,
		@proposal_mode			= 2
	from	proposal_batch_ids
	where	note				= @proposal_batch_label
	and	proposal_type			= 2
end

-- ====================================================================================
-- ==                Process the batch ID                                            ==
-- ====================================================================================
if @proposal_mode = 2
begin
	declare	@print_date datetime

	select	@print_date			= date_closed
	from	proposal_batch_ids
	where	proposal_batch_id		= @client_creditor_proposal

	-- If the batch is not closed then update the balance information
	if @print_date is null
	begin
		update	client_creditor_proposals
		set	proposed_amount		= cc.disbursement_factor,
			proposed_start_date	= isnull(cc.start_date, c.start_date),
			proposed_balance	= isnull(bal.orig_balance,0) + isnull(bal.orig_balance_adjustment,0) + isnull(bal.total_interest,0) - isnull(bal.total_payments,0)
		from	client_creditor_proposals p
		inner join client_creditor cc on p.client_creditor = cc.client_creditor
		inner join client_creditor_balances bal on cc.client_creditor_balance = bal.client_creditor_balance
		inner join clients c on cc.client = c.client
		where	proposal_batch_id	= @client_creditor_proposal

		-- Correct the start date if needed
		update	client_creditor_proposals
		set	proposed_start_date	= @min_start_date
		where	proposed_start_date	< @min_start_date
		and	proposal_batch_id	= @client_creditor_proposal;
	end

	-- Return the proposal information
	select		cc.client					as 'client',
			cc.creditor					as 'creditor',
			cc.client_creditor				as 'client_creditor',
			coalesce(p.proposed_start_date,cc.start_date,c.start_date,getdate())	as 'print_date',
			convert(varchar(256),dn.note_text)		as 'message',
			isnull(cr.proposal_budget_info,0)		as 'proposal_budget_info',
			p.client_creditor_proposal			as 'client_creditor_proposal',
			isnull(cr.proposal_income_info,0)		as 'proposal_income_info'
	from		client_creditor_proposals p
	inner join	client_creditor cc		WITH (NOLOCK) ON p.client_creditor = cc.client_creditor
	inner join	creditors cr			WITH (NOLOCK) ON cc.creditor = cr.creditor
	inner join	clients c			WITH (NOLOCK) ON cc.client = c.client
	left outer join	debt_notes dn			with (nolock) on dn.client_creditor_proposal = p.client_creditor_proposal and 'PR' = dn.type
	where		p.proposal_batch_id	= @client_creditor_proposal
	order by	cr.type, cr.creditor_id, cc.creditor, cc.client

	return ( @@rowcount )
end

-- ====================================================================================
-- ==                Print a specific proposal ID                                    ==
-- ====================================================================================
if @proposal_mode = 0
begin

	insert into client_notes (client, client_creditor, type, is_text, dont_edit, dont_delete, dont_print, subject, note)
	select		cc.client, cc.client_creditor, 3, 1, 1, 1, 0,
			'Full Disclosure Proposal Reprinted upon demand',
			'A Full Disclosure Proposal was reprinted upon request with the following information:' + char(13) + char(10) +
			char(13) + char(10) +
			'Balance: $'   + convert(varchar,     coalesce(pr.proposed_balance,isnull(bal.orig_balance,0)+isnull(bal.orig_balance_adjustment,0)+isnull(bal.total_interest,0)-isnull(bal.total_payments,0), 0), 1) + char(13) + char(10) +
			'Payment: $'   + convert(varchar,     coalesce(pr.proposed_amount,cc.disbursement_factor,0), 1) + char(13) + char(10) +
			'Start Date: ' + convert(varchar(10), coalesce(pr.proposed_start_date,cc.start_date,c.start_date,getdate()), 101) + char(13) + char(10)

	from		client_creditor_proposals pr
	inner join	client_creditor cc on pr.client_creditor = cc.client_creditor
	inner join client_creditor_balances bal on cc.client_creditor_balance = bal.client_creditor_balance
	inner join	clients c on cc.client = c.client
	LEFT OUTER JOIN	proposal_batch_ids ids WITH (NOLOCK) ON ids.proposal_batch_id = pr.proposal_batch_id
	where		pr.client_creditor_proposal = @client_creditor_proposal

	select		cc.client					as 'client',
			cc.creditor					as 'creditor',
			cc.client_creditor				as 'client_creditor',
			coalesce(p.proposed_start_date,cc.start_date,c.start_date,getdate())	as 'print_date',
			convert(varchar(256),dn.note_text)		as 'message',
			isnull(cr.proposal_budget_info,0)		as 'proposal_budget_info',
			p.client_creditor_proposal			as 'client_creditor_proposal',
			isnull(cr.proposal_income_info,0)		as 'proposal_income_info'
	from		client_creditor_proposals p
	inner join	client_creditor cc		WITH (NOLOCK) ON p.client_creditor = cc.client_creditor
	inner join	creditors cr			WITH (NOLOCK) ON cc.creditor = cr.creditor
	left outer join	proposal_batch_ids ids		with (nolock) on p.proposal_batch_id = ids.proposal_batch_id
	left outer join	debt_notes dn			with (nolock) on dn.client_creditor_proposal = p.client_creditor_proposal and 'PR' = dn.type
	inner join	clients c			WITH (NOLOCK) ON cc.client = c.client
	where		p.client_creditor_proposal = @client_creditor_proposal
        order by	cr.type, cr.creditor_id, cc.creditor	
end

return ( @@rowcount )
GO
