SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER procedure [xpr_PrintChecks_Status] ( @bank as int = -1 ) AS

-- =====================================================================================================
-- ==            Return the number of checks, dollar amount, and next check number                    ==
-- =====================================================================================================

-- Suppress intermediate results
set nocount on
set ansi_warnings off

-- Find the suitable bank id
if isnull(@bank,-1) <= 0
begin
	select	@bank	= min(bank)
	from	banks
	where	type	= 'C'
end

-- Determine the number of checks to be printed and the dollar amount
declare	@check_count		int
declare	@check_amount		money

select	@check_count	= count(*),
	@check_amount	= sum(amount)
from	registers_trust with (nolock)
where	tran_type in ('AD', 'MD', 'AR', 'CR', 'CM')
and	cleared		= 'P'
and	bank		= @bank

-- Retrieve the next check number
declare	@next_checknum	BigInt

select	@next_checknum	= checknum
from	banks with (nolock)
where	bank			= @bank

-- Return the information
select	isnull(@check_count,0)		as 'item_count',
		isnull(@check_amount,0)		as 'item_amount',
		isnull(@next_checknum,0)	as 'item_checknum'

return ( 1 )
GO
