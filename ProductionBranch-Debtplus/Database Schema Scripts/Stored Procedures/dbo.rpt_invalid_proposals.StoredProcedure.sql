USE [DebtPlus]
GO
/****** Object:  StoredProcedure [dbo].[rpt_invalid_proposals]    Script Date: 09/15/2014 13:13:33 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[rpt_invalid_proposals] as

-- ChangeLog
--   1/28/08
--     ignore reassigned debts.

set nocount on

select	c.client								as client,
	dbo.format_normal_name (pn.prefix, pn.first, pn.middle, pn.last, pn.suffix)	as client_name,
	cr.creditor								as creditor,
	cr.creditor_name							as creditor_name,
	cc.account_number							as account_number,
	prop.date_created							as proposal_date

from	client_creditor_proposals prop with (nolock)
inner join client_creditor cc with (nolock) on prop.client_creditor = cc.client_creditor and cc.reassigned_debt = 0
inner join client_creditor_balances b with (nolock) on cc.client_creditor_balance = b.client_creditor_balance and cc.client_creditor = b.client_creditor
inner join creditors cr with (nolock) on cr.creditor = cc.creditor
inner join clients c with (nolock) on cc.client = c.client
left outer join people p with (nolock) on cc.client = p.client and 1 = p.relation
left outer join names pn with (nolock) on p.nameid = pn.name

-- the "-1" is a magic value and is set by the xpr_proposal_create_batch procedure when it can not match
-- the proposal to a valid creditor method.
where	prop.proposal_batch_id = -1

-- The proposal should still be in the pending status. ignore other items that someone just marked otherwise.
and	prop.proposal_status in (0, 1)

order by cr.creditor, c.client

return ( @@rowcount )
GO
