USE [DebtPlus]
GO
/****** Object:  StoredProcedure [dbo].[xpr_retention_client_action_delete]    Script Date: 09/15/2014 13:13:36 ******/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[xpr_retention_client_action_delete] ( @client_retention_action AS typ_key ) AS

-- ========================================================================================================
-- ==            Remove the action event for the client retention event                                  ==
-- ========================================================================================================

-- Do the deletion operation
DELETE
FROM	client_retention_actions
WHERE	client_retention_action	= @client_retention_action

-- Return the number of rows deleted. 1 = OK. 0 = failure.
RETURN ( @@rowcount )
GO
