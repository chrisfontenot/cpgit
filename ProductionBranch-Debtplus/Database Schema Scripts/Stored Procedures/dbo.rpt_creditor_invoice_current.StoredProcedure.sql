SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER procedure [rpt_creditor_invoice_current] (@creditor as typ_creditor) AS

-- ============================================================================
-- ==     Determine the items associated with the indicated invoice          ==
-- ============================================================================

-- ChangeLog
--   4/6/2005
--      Removed test of old "surpress_invoice_detail". The flag is being deleted from the database.
--   06/09/2009
--      Remove references to debt_number and use client_creditor

SELECT	i.creditor								as 'creditor',
		i.invoice_register						as 'invoice_number',
		tr.date_created							as 'date_created',
		tr.checknum							as 'checknum',
		d.client								as 'client',

		dbo.format_reverse_name(default,pn.first,pn.middle,pn.last,pn.suffix) as 'client_name',
		isnull(d.account_number,cc.account_number)			as 'account_number',
 
		d.debit_amt							as 'amount',
		case when d.creditor_type in ('N','D') then 0 else d.fairshare_amt end 	as 'billed',

		isnull(i.pmt_amount,0)						as 'pmt_amount',
		isnull(i.adj_amount,0)						as 'adj_amount',

		tr.date_created							as 'check_date',
		
		i.inv_date								as 'inv_date',		
		d.tran_type								as 'tran_type',
		isnull(b.type,'C')						as 'bank_type'

FROM		registers_client_creditor d	WITH (NOLOCK)
LEFT OUTER JOIN	registers_invoices i		WITH (NOLOCK) ON d.invoice_register = i.invoice_register
LEFT OUTER JOIN	creditors cr			WITH (NOLOCK) ON d.creditor = cr.creditor
LEFT OUTER JOIN	people p			WITH (NOLOCK) ON d.client = p.client AND 1 = p.relation
left outer join names pn with (nolock) on p.NameID = pn.Name
LEFT OUTER JOIN	registers_trust tr		WITH (NOLOCK) ON d.trust_register = tr.trust_register
LEFT OUTER JOIN client_creditor cc		WITH (NOLOCK) ON d.client_creditor = cc.client_creditor
LEFT OUTER JOIN banks b					WITH (NOLOCK) ON tr.bank = b.bank

WHERE	d.creditor = @creditor
AND		d.tran_type in ('AD', 'MD', 'BW', 'CM')
AND		(i.inv_amount > (isnull(i.pmt_amount,0) + isnull(i.adj_amount,0)))

ORDER BY	inv_date, invoice_number, checknum, client

RETURN ( @@rowcount )
GO
