USE [DebtPlus]
GO
/****** Object:  StoredProcedure [dbo].[xpr_disbursement_sysnote_batch]    Script Date: 09/15/2014 13:13:35 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE PROCEDURE [dbo].[xpr_disbursement_sysnote_batch] ( @disbursement_register AS INT ) AS
-- ===================================================================================================================
-- ==            Generate all missing system notes for a disbursement batch                                         ==
-- ===================================================================================================================

-- Disable intermediate result sets
SET NOCOUNT ON

-- Date for the disbursement
DECLARE	@CreateDate	DateTime

SELECT	@CreateDate		= date_created
FROM	registers_disbursement WITH (NOLOCK)
WHERE	disbursement_register	= @disbursement_register

-- Find the clients in the disbursement batch
CREATE TABLE #t_disbursement_clients (
	client	int
)

INSERT INTO #t_disbursement_clients (client)
SELECT	distinct client
FROM	registers_client_creditor
WHERE	disbursement_register = @disbursement_register
AND	tran_type in ('AD', 'BW')

-- Allocate a cursor to find the clients which do not have a disbursement note for the batch
DECLARE	client_cursor CURSOR FORWARD_ONLY FOR
	SELECT	client
	FROM	#t_disbursement_clients a
	WHERE	NOT EXISTS (select * from disbursement_notes x WHERE x.client = a.client AND x.disbursement_register = @disbursement_register)

OPEN client_cursor
DECLARE	@Client	INT

FETCH client_cursor INTO @client
WHILE @@fetch_status = 0
BEGIN
	-- Insert the disbursement note
	Execute xpr_disbursement_sysnote @client, @disbursement_register, NULL, @CreateDate

	-- Fetch the next client
	FETCH client_cursor INTO @client
END

CLOSE client_cursor
DEALLOCATE client_cursor

-- Drop the working table
DROP TABLE #t_disbursement_clients

-- Return success
RETURN ( 1 )
GO
