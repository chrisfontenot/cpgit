USE [DebtPlus]
GO
/****** Object:  StoredProcedure [dbo].[DAILY_client_retention_generate]    Script Date: 09/15/2014 13:13:32 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[DAILY_client_retention_generate] ( @today as datetime = null ) as

-- ================================================================================================
-- ==        Generate the retention events for Richmond's clients                                ==
-- ================================================================================================

-- ChangeLog
--   10/27/2004
--     For all events throw out any client that had a deposit in the last 20 days.
--   7/2/2008
--     Removed 6-month followup event

if @today is null
	select	@today	= getdate()

-- Ignore execution of this routine on the 29, 30, and 31st of the month
declare	@day		int
select	@day		= datepart(d, @today)

if @day > 28
	return ( 0 )

-- Retention event IDS
declare	@event_30	int
declare	@event_60	int
declare	@event_90	int

select	@event_30	= 80,
	@event_60	= 84,
	@event_90	= 81

declare	@first_date	datetime
declare	@last_date	datetime

-- Create the table to hold the temporary results
create table #clients ( client int, start_date datetime, event_code int null )

-- Find the starting and ending periods for the start date
declare	@date_start	datetime
declare	@date_end	datetime

-- Include the clients for the one month followup event
select	@date_start	= dbo.date_only (dateadd(m, -1, @today));
select	@date_end	= @date_start
if @day = 28
	select	@date_end	= dateadd(d, -1, dateadd(m, 1, convert(datetime, convert(varchar, month(@date_start)) + '/1/' + convert(varchar, year(@date_start)) + ' 00:00:00')))
select	@date_end	= dateadd(s, 86399, @date_end);

insert into #clients ( client, start_date, event_code )
select	client, start_date, @event_30
from	clients with (nolock)
where	active_status in ('A','AR')
and	start_date between @date_start and @date_end;

-- Include the clients for the two months followup event
select	@date_start	= dbo.date_only (dateadd(m, -2, @today));
select	@date_end	= @date_start
if @day = 28
	select	@date_end	= dateadd(d, -1, dateadd(m, 1, convert(datetime, convert(varchar, month(@date_start)) + '/1/' + convert(varchar, year(@date_start)) + ' 00:00:00')))
select	@date_end	= dateadd(s, 86399, @date_end);

insert into #clients ( client, start_date, event_code )
select	client, start_date, @event_60
from	clients with (nolock)
where	active_status in ('A','AR')
and	start_date between @date_start and @date_end;

-- Include the clients for the three months followup event
select	@date_start	= dbo.date_only (dateadd(m, -3, @today));
select	@date_end	= @date_start
if @day = 28
	select	@date_end	= dateadd(d, -1, dateadd(m, 1, convert(datetime, convert(varchar, month(@date_start)) + '/1/' + convert(varchar, year(@date_start)) + ' 00:00:00')))
select	@date_end	= dateadd(s, 86399, @date_end);

insert into #clients ( client, start_date, event_code )
select	client, start_date, @event_90
from	clients with (nolock)
where	active_status in ('A','AR')
and	start_date between @date_start and @date_end;

-- -----------------------------------------------------------------------------------------------
-- --            Insert the events into the system                                              --
-- -----------------------------------------------------------------------------------------------
insert into client_retention_events (client, retention_event, priority, expire_type, expire_date, date_created)
select	a.client, retention_event, e.priority, e.expire_type, dateadd(d, 30, getdate()) as expire_date, @today
from	#clients a
inner join retention_events e on e.retention_event = a.event_code
where	isnull(a.client,0) > 0
order by 1, 2

-- Discard the table
drop table #clients

return ( 1 )
GO
