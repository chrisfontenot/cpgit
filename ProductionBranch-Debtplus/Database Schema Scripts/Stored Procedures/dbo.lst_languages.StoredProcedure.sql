USE [DebtPlus]
GO
/****** Object:  StoredProcedure [dbo].[lst_languages]    Script Date: 09/15/2014 13:13:32 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[lst_languages] AS

-- ===========================================================================
-- ==   Return the list of possible spoken languages for the agency         ==
-- ===========================================================================

select	oID					as 'item_key',
		[Attribute]			as 'description',
		[default]			as 'default',
		[ActiveFlag]		as 'ActiveFlag'
from	AttributeTypes
where	[Grouping] = 'LANGUAGE'

return (@@rowcount)
GO
