USE [DebtPlus]
GO
/****** Object:  StoredProcedure [dbo].[rpt_PriorityAppointments]    Script Date: 09/15/2014 13:13:33 ******/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE PROCEDURE [dbo].[rpt_PriorityAppointments] ( @FromDate as datetime = null, @ToDate as datetime = null ) AS

-- ========================================================================================================
-- ==            Retrieve the list of priority appointments                                              ==
-- ========================================================================================================

if @ToDate is null
	select	@ToDate = getdate()

if @FromDate is null
	select	@FromDate = @ToDate

select	@FromDate = convert(datetime, convert(varchar(10), @FromDate, 101) + ' 00:00:00'),
		@ToDate = convert(datetime, convert(varchar(10), @ToDate, 101) + ' 23:59:59')

select	ap.client				as client,
		dbo.format_normal_name(pn.prefix,pn.first,pn.middle,pn.last,pn.suffix) as name,
		o.name					as office,
		ap.start_time				as start_time,
		dbo.format_TelephoneNumber ( c.HomeTelephoneID )	as home_ph,
		dbo.format_TelephoneNumber ( p.WorkTelephoneID )	as work_ph,
		dbo.format_TelephoneNumber ( c.MsgTelephoneID ) as message_ph
from	client_appointments ap	with (nolock)
inner join clients c		with (nolock) on ap.client = c.client
inner join offices o		with (nolock) on ap.office = o.office
left outer join people p	with (nolock) on ap.client = p.client and 1 = p.relation
left outer join names pn	with (nolock) on p.NameID = pn.Name

where	isnull(ap.priority,0) > 0
and	ap.start_time between @FromDate and @ToDate
and	ap.workshop is null
and	ap.status = 'P'

order by 4, 1, 3

return ( @@rowcount )
GO
