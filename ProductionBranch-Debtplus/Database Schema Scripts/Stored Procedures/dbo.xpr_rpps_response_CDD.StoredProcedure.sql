USE [DebtPlus]
GO
/****** Object:  StoredProcedure [dbo].[xpr_rpps_response_CDD]    Script Date: 09/15/2014 13:13:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[xpr_rpps_response_CDD] ( @response_file as int, @trace_number as varchar(15), @biller_id as varchar(10), @return_code as varchar(3), @client as int, @ssn as varchar(9) = null, @company_identification as varchar(10) = null ) as

-- =======================================================================================================================
-- ==            Generate the response for a FBD request                                                                ==
-- =======================================================================================================================

-- Generate the base response row
declare	@rpps_response_detail	int
execute @rpps_response_detail = xpr_rpps_response @response_file, @trace_number, @biller_id, @return_code, 'CDD'

-- Insert the addendum information for proposal acceptances
if @rpps_response_detail > 0
begin
	if @ssn is not null
		select @ssn = ltrim(rtrim(@ssn))

	if @ssn not like '[0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9]'
		select @ssn = null

	if @company_identification is not null
		select @company_identification = ltrim(rtrim(@company_identification))
	if @company_identification = ''
		select @company_identification = null

	-- Update the addendum information for a rejected proposal
	insert into rpps_response_details_cdd (rpps_response_detail, company_identification, ssn, client)
	values (@rpps_response_detail, @company_identification, @ssn, @client)
end

return ( @rpps_response_detail )
GO
