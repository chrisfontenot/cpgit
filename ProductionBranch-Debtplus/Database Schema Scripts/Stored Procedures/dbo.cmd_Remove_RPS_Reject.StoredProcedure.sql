USE [DebtPlus]
GO
/****** Object:  StoredProcedure [dbo].[cmd_Remove_RPS_Reject]    Script Date: 09/15/2014 13:13:31 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[cmd_Remove_RPS_Reject] ( @trace_number as Varchar(15) = null, @rpps_transaction as int = null ) AS

-- ====================================================================================
-- ==                 Fetch the information for this transaction                     ==
-- ====================================================================================

-- ChangeLog
--   9/31/2002
--     Added support for client_creditor_balances table
--   10/15/2002
--     Added support for client_creditor's "*_this_creditor" information
--   1/20/2003
--     Corrected to allow for multiple debts rejected on a single client.
--   4/6/2007
--     Named the transaction to provide better isolation in a rollback
--   06/09/2009
--      Remove references to debt_number and use client_creditor

DECLARE	@Return_Code		varchar(10)

-- Find the transaction from the trace number if given
if @trace_number is not null
begin
	print	'Removing reject for trace number ' + @trace_number

	SELECT TOP 1
		@rpps_transaction	= rpps_transaction
	FROM	rpps_transactions t
	inner join rpps_files f on t.rpps_file = f.rpps_file
	where	t.rpps_transaction	= @rpps_transaction
	order by f.date_created desc;
end

IF @rpps_transaction IS NULL
BEGIN
	RaisError(50023, 16, 1, @trace_number)
	RETURN ( 0 )
END

print 'rpps_transaction = ' + convert(varchar, @rpps_transaction)

-- Find the return code for the transaction
SELECT	@return_code		= return_code
FROM	rpps_transactions t
WHERE	rpps_transaction	= @rpps_transaction;

IF @Return_Code IS NULL
BEGIN
	RaisError(50073, 16, 1, @trace_number)
	RETURN ( 0 )
END

DECLARE @client_creditor_register	int
DECLARE	@disbursement_register		int
DECLARE	@client				int
DECLARE	@creditor			varchar(10)
DECLARE	@client_creditor			int
DECLARE	@rr_trust_register		int
DECLARE	@bw_trust_register		int

-- Start a transaction before we do any "real work"
begin transaction TRAN_cmd_Remove_RPS_Reject

SELECT	@client_creditor_register = client_creditor_register
FROM	rpps_transactions
WHERE	rpps_transaction = @Rpps_Transaction

print	'client_creditor_register = ' + convert(varchar, @client_creditor_register)
print	'return code = ' + @Return_Code

-- Fetch the amounts from the disbursement detail register
DECLARE	@Gross			money
DECLARE	@Net			money
DECLARE	@fairshare_amt		money
DECLARE	@creditor_type		char(1)
DECLARE	@Invoice_Register	int

SELECT	@gross				= debit_amt,
	@fairshare_amt			= fairshare_amt,
	@creditor_type			= creditor_type,
	@bw_trust_register		= trust_register,
	@invoice_register		= invoice_register,
	@client				= client,
	@creditor			= creditor,
	@client_creditor			= client_creditor,
	@disbursement_register		= disbursement_register
FROM	registers_client_creditor
WHERE	client_creditor_register	= @client_creditor_register

print 'Client = ' + convert(varchar, @client)
print 'Creditor = ' + isnull(@creditor,'')
print 'Debt = ' + convert(varchar, @client_creditor)
print 'Disbursement Register = ' + convert(varchar, @disbursement_register)
print 'Trust Register = ' + convert(varchar, @bw_trust_register)
print 'Gross = ' + convert(varchar, @Gross, 1)
print 'Fairshare = ' + convert(varchar, @fairshare_amt, 1)
print 'Creditor_Type = ' + @creditor_type
print 'Invoice Register = ' + convert(varchar, @invoice_register)

-- Indicate that the transaction is no longer voided
UPDATE	rpps_transactions
SET	return_code		= NULL
WHERE	rpps_transaction	= @Rpps_Transaction

-- Find the RR transaction for this item
select	@rr_trust_register	= trust_register
from	registers_client_creditor
where	tran_type		= 'RR'
and	client_creditor		= @client_creditor
and	disbursement_register	= @disbursement_register

print 'RR trust register = ' + isnull(convert(varchar, @rr_trust_register),'NULL')

-- There should be a response at this point
IF @rr_trust_register is null
BEGIN
	RollBack Transaction TRAN_cmd_Remove_RPS_Reject
	RaisError(50074, 16, 1)
	RETURN ( 0 )
END

-- First, delete the registers_client_creditor item that is reversing the transaction
delete
from	registers_client_creditor
where	tran_type		= 'RR'
and	client_creditor		= @client_creditor
and	disbursement_register	= @disbursement_register

-- There should be only one item
IF @@rowcount != 1
BEGIN
	RollBack Transaction TRAN_cmd_Remove_RPS_Reject
	RaisError(50074, 16, 1)
	RETURN ( 0 )
END

-- Locate the refund amount for the client transaction. We may have rejected several for this client so the amounts
-- would have been added together. Allow for that and just take out what we need.
declare	@client_register	int
select	@client_register	= client_register
from	registers_client
where	tran_type		= 'RR'
and	client			= @client
and	trust_register		= @rr_trust_register

IF @client_register is null
BEGIN
	RollBack Transaction TRAN_cmd_Remove_RPS_Reject
	RaisError(50074, 16, 1)
	RETURN ( 0 )
END

print	'Client register for RR transaction = ' + convert(varchar, @client_register)

-- Remove the amount for the credit
update	registers_client
set	credit_amt		= credit_amt - @gross
where	client_register		= @client_register

-- There should be only one item by this point.
IF @@rowcount != 1
BEGIN
	RollBack Transaction TRAN_cmd_Remove_RPS_Reject
	RaisError(50074, 16, 1)
	RETURN ( 0 )
END

-- If the amount is now zero then delete the item completely
if exists (select * from registers_client where client_register = @client_register and credit_amt = 0)
	delete
	from	registers_client
	where	client_register		= @client_register;

-- Update the debt record to indicate that the transaction is reversed
UPDATE	client_creditor_balances
SET	total_payments		= bal.total_payments + @gross
FROM	client_creditor_balances bal
inner join client_creditor cc on cc.client_creditor_balance = bal.client_creditor_balance
WHERE	cc.client_creditor		= @client_creditor

-- Update the payments for this debt
update	client_creditor
set	payments_this_creditor	= isnull(payments_this_creditor,0) + @gross
WHERE	client_creditor		= @client_creditor

select	@Net			= @gross

-- Update the deduction creditor to 'credit' back the amount taken as a deduction
IF @creditor_type = 'D' AND @fairshare_amt > 0
BEGIN
	UPDATE	clients
	SET	held_in_trust	= held_in_trust + @fairshare_amt
	WHERE	client		= 0

	-- Recredit the contribution received that was removed when the item was voided
	UPDATE	creditors
	SET	distrib_mtd		= distrib_mtd + @gross,
		contrib_mtd_received	= contrib_mtd_received + @fairshare_amt
	WHERE	creditor		= @creditor

	SELECT	@net			= @gross - @fairshare_amt
END

-- Update the invoice table to remove the adjustment
IF @creditor_type not in ('D', 'N') and @fairshare_amt > 0 and @invoice_register is not null
BEGIN
	UPDATE	registers_invoices
	SET	adj_amount		= adj_amount - @fairshare_amt,
		adj_date		= NULL
	WHERE	invoice_register	= @invoice_register

	-- Recredit the fairshare billed that was removed when the item was voided
	UPDATE	creditors
	SET	distrib_mtd		= distrib_mtd + @gross,
		contrib_mtd_billed	= contrib_mtd_billed + @fairshare_amt
	WHERE	creditor		= @creditor
END

-- Remove the void status from the detail record
UPDATE	registers_client_creditor
SET	void     = 0
WHERE	client_creditor_register = @client_creditor_register

-- Remove the money from the client's trust account that was credited by the reversal
DECLARE	@TrustBalance	money
SELECT	@TrustBalance = held_in_trust
FROM	clients
WHERE	client = @client

	UPDATE	clients
	SET	held_in_trust	= held_in_trust - @gross
	WHERE	client		= @client

	SELECT	@TrustBalance	= held_in_trust
	FROM	clients
	WHERE	client		= @client

-- Update the RR transaction for the refund
update	registers_trust
set	amount		= amount - @net
where	trust_register	= @rr_trust_register
and	tran_type	= 'RR'

-- Commit the transaction at this point
Commit Transaction TRAN_cmd_Remove_RPS_Reject

-- Return success
RETURN ( 1 )
GO
