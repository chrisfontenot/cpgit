USE [DebtPlus]
GO
/****** Object:  StoredProcedure [dbo].[lst_faq_types]    Script Date: 09/15/2014 13:13:32 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[lst_faq_types] AS

-- ===================================================================================================
-- ==                Return the list of faq question types                                          ==
-- ===================================================================================================

SELECT	faq_type		as 'item_key',
	[description]		as 'description'
FROM	faq_types WITH ( NOLOCK )

return ( @@rowcount )
GO
