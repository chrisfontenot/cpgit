USE [DebtPlus]
GO
/****** Object:  StoredProcedure [dbo].[lst_education]    Script Date: 09/15/2014 13:13:32 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[lst_education] ( @language as int = null ) as

-- =========================================================================================================
-- ==             Return a list of the Workshop Presenters who may or may not match the language          ==
-- =========================================================================================================

-- ChangeLog
--   1/19/2001
--     Added langauge as an optional paramter to select only those counselor who match the language.
--     this is used by the appointment system.
--   9/12/2008
--     Extended for .NET interface
--   12/31/2008
--     Used Counselor_Attributes table

set nocount on

if ISNULL(@language,0) > 0

	select	v.counselor					as 'item_key',
			v.Name						as 'description',
			ISNULL(v.[default],0)			as 'default',
			ISNULL(v.[ActiveFlag],1)		as 'ActiveFlag',
			v.person						as 'person',
			v.NameID						as 'name'
	from	view_educators v with (nolock)
	inner join counselor_attributes a with (nolock) on v.counselor = a.counselor
	inner join attributetypes t with (nolock) on a.attribute = t.oid
	where	t.oid = @language
	and		t.[grouping] = 'LANGUAGE'
	order by 2;
	
else	-- Language was not specified. Use any language

	select	counselor					as 'item_key',
			Name						as 'description',
			ISNULL([default],0)			as 'default',
			ISNULL([ActiveFlag],1)		as 'ActiveFlag',
			person						as 'person',
			NameID						as 'name'
	from	view_educators with (nolock)
	order by 2;
	
return ( @@rowcount )
GO
