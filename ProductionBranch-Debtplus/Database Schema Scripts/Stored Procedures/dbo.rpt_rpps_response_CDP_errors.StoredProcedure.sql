USE [DebtPlus]
GO
/****** Object:  StoredProcedure [dbo].[rpt_rpps_response_CDP_errors]    Script Date: 09/15/2014 13:13:33 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE PROCEDURE [dbo].[rpt_rpps_response_CDP_errors] ( @response_batch_id as varchar(50) ) AS

-- =======================================================================================================
-- ==           Obtain the information for the response conditions of proposals which never made it     ==
-- ==           to the creditor because they failed to pass Mastercard's validation routines.           ==
-- =======================================================================================================

-- ChangeLog
--   2/18/2004
--      Added client's counselor name

-- Suppress the count of records
set nocount on

-- Retrieve the information for the records
select		coalesce(x.description, rt.return_code, '')			as 'error_code', -- 1

		b.date_transmitted						as 'proposal_print_date', -- 2
		pr.proposal_status_date						as 'reject_date', -- 3

		cc.client							as 'client', -- 4
		cc.creditor							as 'creditor', -- 5
		cc.client_creditor							as 'client_creditor', -- 6
		cc.disbursement_factor						as 'disbursement_factor', -- 7

		isnull(cr.creditor_name,'')					as 'creditor_name', -- 8
		convert(varchar(80), dbo.format_normal_name(default,cox.first,default,cox.last,default)) as 'counselor_name', -- 9

                -- Fields for sorting		
		cr.type, -- 10
		cr.creditor_id, -- 11
                rt.creditor, -- 12
                rt.client as response_client, -- 13
                rt.client_creditor as response_client_creditor -- 14
                
from		rpps_transactions rt		with (nolock)
inner join	client_creditor_proposals pr	with (nolock) on rt.client_creditor_proposal = pr.client_creditor_proposal
inner join	client_creditor cc		with (nolock) on pr.client_creditor = cc.client_creditor
inner join	creditors cr			with (nolock) on rt.creditor = cr.creditor
inner join	people p			with (nolock) on rt.client=p.client AND 1=p.relation
inner join	proposal_batch_ids b		with (nolock) on pr.proposal_batch_id = b.proposal_batch_id
left outer join	rpps_reject_codes x		with (nolock) on rt.return_code = x.rpps_reject_code
left outer join	clients c			with (nolock) on cc.client = c.client
left outer join	counselors co			with (nolock) on c.counselor = co.counselor
left outer join names cox with (nolock) on co.NameID = cox.name

where		rt.response_batch_id = @response_batch_id
and		rt.service_class_or_purpose = 'CDP'
and		rt.return_code is not null
and		rt.return_code not like 'CD_%'
and		pr.proposal_reject_reason = 0

order by	10, 11, 12, 2, 13, 14  -- cr.type, cr.creditor_id, rt.creditor, b.date_transmitted, rt.client, rt.client_creditor

return ( @@rowcount )
GO
