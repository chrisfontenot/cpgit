USE [DebtPlus]
GO
/****** Object:  StoredProcedure [dbo].[DAILY_late_deposits]    Script Date: 09/15/2014 13:13:32 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE PROCEDURE [dbo].[DAILY_late_deposits] AS

-- =========================================================================================
-- ==            Determine which client deposits are late                                 ==
-- =========================================================================================

begin transaction
set xact_abort on

-- Generate the list of expected deposits which have been missed
select	client_deposit, client, deposit_date, deposit_amount
into	#deposits_expected
from	client_deposits
where	deposit_date < getdate()
and	client > 0
and	deposit_amount > 0

-- Discard any clients which are not active
delete
from	#deposits_expected
where	client not in (
	select	client
	from	clients
	where	active_status in ('A', 'AR')
)

-- Generate the client transaction that the deposit is missing
insert into registers_client (client, tran_type, message)
select client, 'LD', 'Missed ' + convert(varchar, deposit_date, 1) + ' deposit of $' + convert(varchar, deposit_amount, 1)
from	#deposits_expected

-- Roll the date forward by a month to detect a late deposit next month
update	client_deposits
set	deposit_date = dateadd(m, 1, deposit_date)
where	client_deposit in (
	select	client_deposit
	from	#deposits_expected
)

-- Discard the working table
drop table #deposits_expected
commit transaction
GO
