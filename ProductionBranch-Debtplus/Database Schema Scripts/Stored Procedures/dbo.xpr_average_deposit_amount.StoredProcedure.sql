USE [DebtPlus]
GO
/****** Object:  StoredProcedure [dbo].[xpr_average_deposit_amount]    Script Date: 09/15/2014 13:13:34 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE PROCEDURE [dbo].[xpr_average_deposit_amount] AS
-- ================================================================================
-- ==          Calculate the average scheduled deposit amountst                  ==
-- ================================================================================
SET NOCOUNT ON
SELECT	client_deposits.client AS 'client',
	sum(client_deposits.deposit_amount) AS 'amount'
INTO	#average_deposit_amounts
FROM	client_deposits
GROUP BY client_deposits.client

SELECT AVG(convert(float,amount)) AS 'average' FROM #average_deposit_amounts

DROP TABLE #average_deposit_amounts
SET NOCOUNT OFF
GO
