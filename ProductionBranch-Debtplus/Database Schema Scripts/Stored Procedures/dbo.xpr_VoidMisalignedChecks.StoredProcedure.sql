SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER procedure [xpr_VoidMisalignedChecks] ( @FirstSequence AS INT, @LastSequence AS INT, @Extra_Unused_Parameter AS INT = NULL ) AS
-- =========================================================================================
-- ==           Procedure to void misaligned checks                                       ==
-- =========================================================================================

-- Ensure that the sequence numbers are valid
IF @FirstSequence < 0
BEGIN
	RaisError ( 50007, 16, 1, @FirstSequence )
	RETURN ( 0 )
END

IF @LastSequence < 0
BEGIN
	RaisError ( 50008, 16, 1, @LastSequence )
	RETURN ( 0 )
END

IF @LastSequence < @FirstSequence
BEGIN
	RaisError ( 50009, 16, 1, @LastSequence, @FirstSequence )
	RETURN ( 0 )
END

BEGIN TRANSACTION
SET NOCOUNT ON

-- Duplicate the check with the DESTROYED status
insert into registers_trust (tran_type,	client,	creditor,	cleared,	checknum,	amount,	date_created,	check_order,	reconciled_date,	created_by)
select			     tran_type,	client,	creditor,	'D',		checknum,	amount,	date_created,	check_order,	getdate(),		created_by
from	registers_trust
WHERE	trust_register BETWEEN @FirstSequence AND @LastSequence
AND	cleared = ' '
AND	tran_type NOT IN ('BI', 'SC', 'DP', 'BW')

-- Mark the items as no longer printed
UPDATE	registers_trust
SET	cleared		= 'P',
	checknum	= null,
	sequence_number	= null
FROM	registers_trust
WHERE	trust_register BETWEEN @FirstSequence AND @LastSequence
AND	cleared		= ' '
AND	tran_type NOT IN ('BI', 'SC', 'DP', 'BW')

COMMIT TRANSACTION

-- Return success
RETURN ( 1 )
GO
