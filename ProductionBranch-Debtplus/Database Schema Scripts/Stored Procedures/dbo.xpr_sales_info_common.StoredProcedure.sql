USE [DebtPlus]
GO
/****** Object:  StoredProcedure [dbo].[xpr_sales_info_common]    Script Date: 09/15/2014 13:13:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[xpr_sales_info_common] (@sales_file as int, @client as int = null) as

-- ========================================================================================
-- ==            Retrieve the information which is in common for the debt list           ==
-- ========================================================================================

SELECT	client			as 'client',
	note			as 'note',
	extra_amount		as 'extra_amount',
	client_total_interest	as 'client_total_interest',
	plan_total_interest	as 'plan_total_interest',
	client_months		as 'client_months',
	plan_months		as 'plan_months',

	-- Force READONLY status on an exported file.
	case
		when	date_exported is not null then 1
		else	0
	end			as 'read_only'

FROM	sales_files with (nolock)
WHERE	sales_file=@sales_file
return ( @@rowcount )
GO
