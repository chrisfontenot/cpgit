USE [DebtPlus]
GO
/****** Object:  StoredProcedure [dbo].[rpt_fairshare_by_category]    Script Date: 09/15/2014 13:13:33 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[rpt_fairshare_by_category] as

-- ================================================================================================
-- ==            Return the information for the fairshare by creditor type                       ==
-- ================================================================================================

-- ChangeLog
--   3/14/2003
--     Allow for more than 10,000 creditors in a class

declare	@deduct_creditor	varchar(10)
select	@deduct_creditor	= deduct_creditor
from	config with (nolock)

declare	@month_start	datetime
declare	@month_end	datetime
declare	@year_start	datetime

select	@year_start	= convert(datetime, '01/01/' + right('0000'+convert(varchar,year(getdate())),4) + ' 00:00:00');
select	@month_start	= convert(datetime, right('00' + convert(varchar, month(getdate())), 2) + '/01/' + right('0000'+convert(varchar,year(getdate())),4) + ' 00:00:00');
select	@month_end	= dateadd(m, 1, @month_start);
select	@month_end	= dateadd(s, -1, @month_end);

-- Find the disbursement information by looking at the disbursements by type for the date range
select	cr.type								as 'creditor_type',

	sum(case
		when rcc.creditor_type = 'D' and rcc.date_created >= @month_start then rcc.fairshare_amt
		else 0
	    end
	)								as 'mtd_deduct_dollars',

	sum(case
		when rcc.creditor_type = 'D' then rcc.fairshare_amt
		else 0
	    end
	)								as 'ytd_deduct_dollars',

	sum(case
		when rcc.creditor_type = 'B' and rcc.date_created >= @month_start then rcc.fairshare_amt
		else 0
	    end
	)								as 'mtd_bill_dollars',

	sum(case
		when rcc.creditor_type = 'B' then rcc.fairshare_amt
		else 0
	    end
	)								as 'ytd_bill_dollars'

into	#disbursements
from	registers_client_creditor rcc with (nolock)
left outer join creditors cr on rcc.creditor = cr.creditor
where	rcc.creditor != @deduct_creditor
and	rcc.tran_type in ('AD', 'BW', 'MD', 'CM')
and	rcc.void = 0
and	rcc.date_created between @year_start and @month_end
group by cr.type;

-- Generate the total to compute the percentage of the various row/column pairs
declare	@total_mtd_deduct_dollars	money
declare	@total_ytd_deduct_dollars	money
declare	@total_mtd_bill_dollars		money
declare	@total_ytd_bill_dollars		money

select	@total_mtd_deduct_dollars = sum(mtd_deduct_dollars),
	@total_ytd_deduct_dollars = sum(ytd_deduct_dollars),
	@total_mtd_bill_dollars   = sum(mtd_bill_dollars),
	@total_ytd_bill_dollars   = sum(ytd_bill_dollars)
from	#disbursements;

-- Return the information to the report writer
select	x.creditor_type				as 'creditor_type',
	t.description				as 'description',

	mtd_deduct_dollars			as 'mtd_deduct_dollars',
	case
		when @total_mtd_deduct_dollars <= 0 then 0.0
		else convert(float,mtd_deduct_dollars) / convert(float,@total_mtd_deduct_dollars) * 100.0
	end					as 'mtd_deduct_pct',

	ytd_deduct_dollars			as 'ytd_deduct_dollars',
	case
		when @total_ytd_deduct_dollars <= 0 then 0.0
		else convert(float,ytd_deduct_dollars) / convert(float,@total_ytd_deduct_dollars) * 100.0
	end					as 'ytd_deduct_pct',

	mtd_bill_dollars			as 'mtd_bill_dollars',
	case
		when @total_mtd_bill_dollars <= 0 then 0.0
		else convert(float,mtd_bill_dollars) / convert(float,@total_mtd_bill_dollars) * 100.0
	end					as 'mtd_bill_pct',

	ytd_bill_dollars			as 'ytd_bill_dollars',
	case
		when @total_ytd_bill_dollars <= 0 then 0.0
		else convert(float,ytd_bill_dollars) / convert(float,@total_ytd_bill_dollars) * 100.0
	end					as 'ytd_bill_pct'

from	#disbursements x
left outer join creditor_types t on x.creditor_type = t.type
order by 1;

-- Discard the working table and terminate
drop table #disbursements
return ( @@rowcount )
GO
