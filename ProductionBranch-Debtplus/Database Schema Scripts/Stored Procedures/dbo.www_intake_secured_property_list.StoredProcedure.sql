USE [DebtPlus]
GO
/****** Object:  StoredProcedure [dbo].[www_intake_secured_property_list]    Script Date: 09/15/2014 13:13:34 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[www_intake_secured_property_list] ( @intake_client as int ) as

create table #results (
	secured_property int null,
	secured_type int null,
	description varchar(256) null,
	value money null,
	year_mfg int null,
	first_mortgage money null,
	first_lender varchar(256) null,
	first_account varchar(256) null,
	second_mortgage money null,
	second_lender varchar(256) null,
	second_account varchar(256) null, 
	third_mortgage money null,
	third_lender varchar(256) null,
	third_account varchar(256) null
);

declare	@auto_type	int
select	@auto_type	= secured_type
from	secured_types
where	ltrim(rtrim(isnull(grouping+' ','') + isnull(description,''))) like '%auto #'

if @auto_type is null
	select	@auto_type	= secured_type
	from	secured_types
	where	ltrim(rtrim(isnull(grouping+' ','') + isnull(description,''))) like '%auto%'
	and	auto_home_other	= 'A'

if @auto_type is null
	select	@auto_type	= secured_type
	from	secured_types
	where	ltrim(rtrim(isnull(grouping+' ','') + isnull(description,''))) like '%auto%'

insert into #results ( secured_type, description )
select secured_type, ltrim(rtrim(isnull(grouping+' ','') + isnull(description,'')))
from secured_types
where	secured_type	<> @auto_type
and	ltrim(rtrim(isnull(grouping+' ','') + isnull(description,''))) not like '%auto%'
order by secured_type;

if isnull(@auto_type,0) > 0
	insert into #results ( secured_type, description )
	select @auto_type, 'Personal Property Auto #1'
	union all
	select @auto_type, 'Personal Property Auto #2'

update	#results
set	secured_property	= i.secured_property
from	#results r
inner join intake_secured_properties i on r.secured_type = i.secured_type
where	i.intake_client = @intake_client;

if isnull(@auto_type,0) > 0
begin
	declare	@min_auto	int
	declare	@max_auto	int
	select	@min_auto	= min(secured_property),
		@max_auto	= max(secured_property)
	from	intake_secured_properties
	where	intake_client	= @intake_client
	and	secured_type	= @auto_type;

	if @min_auto = @max_auto
		select	@max_auto	= null;

	update	#results
	set	secured_property	= @min_auto
	where	secured_type		= @auto_type
	and	description		= 'Personal Property Auto #1';

	update	#results
	set	secured_property	= @max_auto
	where	secured_type		= @auto_type
	and	description		= 'Personal Property Auto #2';
end

update	#results
set	value		= p.current_value,
	year_mfg	= p.year_mfg
from	#results r
inner join intake_secured_properties p on r.secured_property = p.secured_property
where	r.secured_property	is not null;

update	#results
set	first_mortgage	= l.balance,
	first_lender	= l.lender,
	first_account	= l.account_number
from	#results r
inner join intake_secured_loans l on r.secured_property = l.secured_property
where	l.priority	= 1;

update	#results
set	second_mortgage	= l.balance,
	second_lender	= l.lender,
	second_account	= l.account_number
from	#results r
inner join intake_secured_loans l on r.secured_property = l.secured_property
where	l.priority	= 2;

update	#results
set	third_mortgage	= l.balance,
	third_lender	= l.lender,
	third_account	= l.account_number
from	#results r
inner join intake_secured_loans l on r.secured_property = l.secured_property
where	l.priority	= 3;

select	secured_type				as secured_type,
		isnull(secured_property,0)	as secured_property,
		isnull(description,'')		as description,
		isnull(value,0)				as value,
		isnull(year_mfg,0)			as year_mfg,
		isnull(first_mortgage,0)	as first_mortgage,
		isnull(first_lender,'') 	as first_lender,
		isnull(first_account,'')	as first_account,
		isnull(second_mortgage,0)	as second_mortgage,
		isnull(second_lender,'')	as second_lender,
		isnull(second_account,'')	as second_account,
		isnull(third_mortgage,0)	as third_mortgage,
		isnull(third_lender,'')		as third_lender,
		isnull(third_account,'')	as third_account
from	#results
order by description

drop table #results;
GO
