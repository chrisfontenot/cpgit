USE [DebtPlus]
GO
/****** Object:  StoredProcedure [dbo].[www_intake_Budget_List]    Script Date: 09/15/2014 13:13:34 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[www_intake_Budget_List] ( @intake_client as INT ) AS
-- =================================================================================================
-- ==            Return the information about an intake client's budget                           ==
-- =================================================================================================

-- ChangeLog
--   12/19/2008
--      Revisions for SQL2008

-- Suppress intermediate result sets
set nocount on

create table #t1 ( budget_category int, description varchar(80) null, heading int null, detail int null);
insert into #t1 (budget_category, description, heading, detail)
select  budget_category, description, heading, detail
from    budget_categories;

insert into #t1 (budget_category, description, heading, detail)
select  ic.budget_category, ic.other_budget_category, 0, 0
from    intake_budgets ic
where   ic.intake_client = @intake_client
and     ic.budget_category >= 10000;

select  c.budget_category, c.budget_category as item_key, c.description, c.heading, c.detail, d.client_amount as 'client_amount', d.client_amount as 'suggested_amount'
from    #t1 c
left outer join intake_budgets d on c.budget_category = d.budget_category AND @intake_client = d.intake_client
order by    1;

drop table #t1;

-- Return the number of resulting rows in the result set
return ( @@rowcount )
GO
