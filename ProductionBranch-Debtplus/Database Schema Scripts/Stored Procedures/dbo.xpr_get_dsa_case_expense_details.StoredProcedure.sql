USE [DebtPlus]
GO
/****** Object:  StoredProcedure [dbo].[xpr_get_dsa_case_expense_details]    Script Date: 12/11/2014 13:27:27 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


CREATE procedure [dbo].[xpr_get_dsa_case_expense_details]
(
	@clientid	int
)
as 
begin

	declare @budget						int
	declare @firstmortgagepayment		money
	declare @secondmortgagepayment		money
	declare @monthlyhomeownerpayment	money
	declare @propertytax				money
	declare @autoloan					money
	declare @creditcardinstallmentloan	money
	declare @childsupport				money
	declare @totalmonthlymedicalexpenses money
	declare @prescriptioncopayments		 money
	declare @medicallifeinsurance		 money
	declare @totalmonthlygroceryexpense	 money
	declare @insurance					 money
	declare @utilityelectricity			 money
	declare @utilitywater				 money
	declare @phoneexpense				 money
	declare @entertainmentexpense	     money
	declare @gasexpense					 money
	declare @eatingoutexpense			 money
	declare @clothingexpense			 money
	declare @drycleaningexpense			 money
	declare @barberexpense				 money
	declare @alcoholexpense				 money
	declare @charityexpense				 money
	declare @clubexpense				 money
	declare @giftexpense				 money
	declare @othersubexpense				 money
	declare @othersub1expense				 money
	declare @propertymaintenance		 money
	declare @otherdayexpense			 money
	declare @studentloan				 money
	declare @otherinstallmentloans		 money
	declare @federaltaxstatetax			 money
	declare @payhoafee					 varchar(10)

	--select @budget = budget
	--  from budgets
	-- where client = @clientid

	select top(1) @budget = budget 
	  from budgets 
	 where client = @clientid
  order by starting_date desc

	select @firstmortgagepayment = sum(client_amount)
	  from budget_detail
	 where budget = @budget 
	   and budget_category in (110, 105, 120)
	   --110 (first mortgage), 105(rent) & 120(lot rent)

	select @secondmortgagepayment = client_amount
	  from budget_detail
	 where budget = @budget 
	   and budget_category = 115

	select @monthlyhomeownerpayment = client_amount
	  from budget_detail
	 where budget = @budget 
	   and budget_category = 510

	select @propertytax = client_amount
	  from budget_detail
	 where budget = @budget 
	   and budget_category = 410

	select @autoloan = client_amount
	  from budget_detail
	 where budget = @budget 
	   and budget_category = 710

	-- get the other debts of the client other than creditor debts
	declare @totalotherdebt  money
	select @totalotherdebt = ISNULL(sum(payment), 0)
	  from client_other_debts
     where client = @clientid

	select @creditcardinstallmentloan = isnull(sum(disbursement_factor), 0) + isnull(@totalotherdebt, 0)
	  from [dbo].[client_creditor] cc
inner join [dbo].[client_creditor_balances] ccb on cc.[client_creditor_balance] = ccb.[client_creditor_balance] 
	 where reassigned_debt = 0
	   and client = @clientid 
	   and ((ccb.[orig_balance] + ccb.[orig_balance_adjustment] + ccb.[total_interest] - ccb.[total_payments]) + @totalotherdebt) > 0 

	select @childsupport = client_amount
	  from budget_detail
	 where budget = @budget 
	   and budget_category = 910

	select @totalmonthlymedicalexpenses = client_amount
	  from budget_detail
	 where budget = @budget 
	   and budget_category = 540

	select @prescriptioncopayments = client_amount
	  from budget_detail
	 where budget = @budget 
	   and budget_category = 960

	select @medicallifeinsurance = sum(client_amount)
	  from budget_detail
	 where budget = @budget 
	   and budget_category IN ( 520, 540)

	select @totalmonthlygroceryexpense = client_amount
	  from budget_detail
	 where budget = @budget 
	   and budget_category = 920

	select @insurance = sum(client_amount)
	  from budget_detail
	 where budget = @budget 
	   and budget_category in (530, 740)

	select @utilityelectricity = client_amount
	  from budget_detail
	 where budget = @budget 
	   and budget_category = 130

	select @utilitywater = client_amount
	  from budget_detail
	 where budget = @budget 
	   and budget_category = 135

	select @phoneexpense = client_amount
	  from budget_detail
	 where budget = @budget 
	   and budget_category = 310

	select @entertainmentexpense = sum(client_amount)
	  from budget_detail
	 where budget = @budget 
	   and budget_category in (320, 330)

	select @gasexpense = sum(client_amount)
	  from budget_detail
	 where budget = @budget 
	   and budget_category in (715, 720, 730)

	select @eatingoutexpense = client_amount
	  from budget_detail
	 where budget = @budget 
	   and budget_category = 925

	select @clothingexpense = client_amount
	  from budget_detail
	 where budget = @budget 
	   and budget_category = 940

	select @drycleaningexpense = client_amount
	  from budget_detail
	 where budget = @budget 
	   and budget_category = 945

	select @barberexpense = client_amount
	  from budget_detail
	 where budget = @budget 
	   and budget_category = 950

	select @alcoholexpense = client_amount
	  from budget_detail
	 where budget = @budget 
	   and budget_category = 955

	select @charityexpense = client_amount
	  from budget_detail
	 where budget = @budget 
	   and budget_category = 980

	select @clubexpense = sum(client_amount)
	  from budget_detail
	 where budget = @budget 
	   and budget_category in (957, 965, 985, 995)

	select @giftexpense = sum(client_amount)
	  from budget_detail
	 where budget = @budget 
	   and budget_category in (930, 975)

	select @othersubexpense = client_amount
	  from budget_detail
	 where budget = @budget 
	   and budget_category = 220

	select @othersub1expense = sum(client_amount)
	  from budget_detail
	 where budget = @budget 
	   and (budget_category IN (970, 990)
	   or budget_category > 10000)
	-- pets (970) and lottery (990)
	
	select @propertymaintenance = sum(client_amount)
	  from budget_detail
	 where budget = @budget 
	   and budget_category in (125, 145)

	select @otherdayexpense = client_amount
	  from budget_detail
	 where budget = @budget 
	   and budget_category = 905

	select @studentloan = client_amount
	  from budget_detail
	 where budget = @budget 
	   and budget_category = 915

	select @otherinstallmentloans = client_amount
	  from budget_detail
	 where budget = @budget 
	   and budget_category = 1000
	   
	select @federaltaxstatetax = sum(client_amount)
	  from budget_detail
	 where budget = @budget 
	   and budget_category in (420, 430)

	select @payhoafee = case when client_amount is not null and client_amount > 0 then 'Yes' else 'No' end
	  from budget_detail
	 where budget = @budget 
	   and budget_category = 1000

	select @firstmortgagepayment as FirstMortgagePayment
	      ,@secondmortgagepayment as SecondMortgagePayment
		  ,@monthlyhomeownerpayment as MonthlyHomeOwnerPayment
		  ,@propertytax as PropertyTax
		  ,@autoloan as AutoLoan
		  ,@creditcardinstallmentloan as CreditCardInstallment
		  ,@childsupport as ChildSupport
		  ,@totalmonthlymedicalexpenses as TotalMonthlyMedicalExpenses
		  ,@prescriptioncopayments as PrescriptionCoPayments
		  ,@medicallifeinsurance as MedicalLifeInsurance
		  ,@totalmonthlygroceryexpense as TotalMonthlyGroceryExpense
		  ,@insurance as Insurance
		  ,@utilityelectricity as UtilityElectricity
		  ,@utilitywater as UtilityWater
		  ,@phoneexpense as PhoneExpense
		  ,@entertainmentexpense as EntertainmentExpense
		  ,@gasexpense as GasExpense
		  ,@eatingoutexpense as EatingOutExpense
		  ,@clothingexpense as ClothingExpense
		  ,@drycleaningexpense as DryCleaningExpense
		  ,@barberexpense as BarberExpense
		  ,@alcoholexpense as AlcoholExpense
		  ,@charityexpense as CharityExpense
		  ,@clubexpense as ClubExpense
		  ,@giftexpense as GiftExpense
		  ,@othersubexpense as OtherSubExpense
		  ,@othersub1expense as OtherSub1Expense
		  ,@propertymaintenance as PropertyMaintenance
		  ,@otherdayexpense as OtherDayExpense
		  ,@studentloan as StudentLoan
		  ,@otherinstallmentloans as OtherInstallmentLoans
		  ,@federaltaxstatetax as FederalTaxStateTax
		  ,@payhoafee as HoaFee
end