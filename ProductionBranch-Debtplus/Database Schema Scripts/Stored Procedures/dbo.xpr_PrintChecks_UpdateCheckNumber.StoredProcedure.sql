SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER procedure [xpr_PrintChecks_UpdateCheckNumber] ( @checknum AS BigInt, @bank as int = -1 ) AS
-- =====================================================================================
-- ==          Update the starting check number for the next batch of checks          ==
-- =====================================================================================

-- Disable intermediate result sets
SET NOCOUNT ON

if isnull(@bank,-1) <= 0
	select	@bank	= min(bank)
	from	banks with (nolock)
	where	type	= 'C'
	and	[default] = 1

if @bank is null
	select	@bank	= min(bank)
	from	banks with (nolock)
	where	type	= 'C'

if @bank is null
	select	@bank	= 1

BEGIN TRANSACTION

-- Update the information for the statistics
update	banks
set		checknum	= @checknum
where	bank		= @bank

if @@rowcount <> 1
begin
	ROLLBACK TRANSACTION
	raiserror ('Unable to update banks table with new check number. bank = %d', 16, 1, @bank)
	return ( 0 )
end

-- Return the number of items in the list
COMMIT TRANSACTION
RETURN ( @@rowcount )
GO
