USE [DebtPlus]
GO
/****** Object:  StoredProcedure [dbo].[xpr_appt_create_appt_counselor]    Script Date: 09/15/2014 13:13:34 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[xpr_appt_create_appt_counselor] ( @appt_time as int, @appt_time_template as int ) AS

-- ==========================================================================================================
-- ==            Create a list of the counselors from the template list for the specific appt time         ==
-- ==========================================================================================================

-- ChangeLog
--   12/18/2002
--     Added support for "inactive" flag in appt_counselors
--   12/31/2008
--     Added counselor_attributes table for roles

insert into appt_counselors (appt_time, counselor, inactive)

-- Find the appointment types
select	@appt_time as 'appt_time', ac.counselor, 0
from	appt_counselor_templates ac	WITH (NOLOCK)
inner join counselors co		WITH (NOLOCK) on ac.counselor = co.counselor
inner join counselor_attributes coa     WITH (NOLOCK) ON coa.counselor = co.counselor and coa.attribute = 'ROLE:COUNSELOR'
where	ac.appt_time_template = @appt_time_template

-- Do not attempt to duplicate the items
and not exists (
	select	*
	from	appt_counselors b WITH (NOLOCK)
	where	b.appt_time = @appt_time
	and	ac.counselor = b.counselor
)

return ( @@rowcount )
GO
