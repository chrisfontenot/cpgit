USE [DebtPlus]
GO
/****** Object:  StoredProcedure [dbo].[rpt_WorkshopList]    Script Date: 09/15/2014 13:13:34 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[rpt_WorkshopList] AS

-- ======================================================================================================
-- ==               Retrieve a list of the workshops                                                   ==
-- ======================================================================================================

-- ChangeLog
--    5/30/12
--        Added location city/state and # people attending to the result set.

SELECT			w.workshop,
				w.start_time,
				t.description										as 'workshop_type',
				l.name												as 'location',
				dbo.format_city_state_zip(a.city,a.[state],default) as 'city_state',
				sum(isnull(ca.workshop_people,0))					as 'attending'

FROM			workshops w
INNER JOIN		workshop_types t ON w.workshop_type = t.workshop_type
INNER JOIN		workshop_locations l ON w.workshop_location=l.workshop_location
LEFT OUTER JOIN	addresses a WITH (NOLOCK) ON l.AddressID = a.Address

-- Count only pending appointments; not cancelled, rescheduled, kept, etc.
LEFT OUTER JOIN	client_appointments ca WITH (NOLOCK) ON w.workshop = ca.workshop AND 'P' = ca.status

-- Look for one week in the pas and all future workshops
WHERE			w.start_time > dateadd (d, -7, getdate())

GROUP BY		w.workshop, w.start_time, t.description, l.name, a.city, a.state
ORDER BY		w.start_time

RETURN ( @@rowcount )
GO
