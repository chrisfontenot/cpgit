USE [DebtPlus]
GO
/****** Object:  StoredProcedure [dbo].[lst_hud_interview_types]    Script Date: 09/15/2014 13:13:32 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[lst_hud_interview_types] AS
-- =====================================================================================================================
-- ==            Retrieve the list of hud interview types from the messages table                                     ==
-- =====================================================================================================================

-- ChangeLog
--   2/01/2002
--     Switch to the messages table
--   1/03/2010
--     Use Housing_PurposeOfVisitTypes table

select	oID	as item_key,
		description	as description,
		[default],
		ActiveFlag
		HourlyGrantAmountUsed,
		HousingFeeAmount
FROM	Housing_PurposeOfVisitTypes WITH (NOLOCK)
ORDER BY 1

return ( @@rowcount )
GO
