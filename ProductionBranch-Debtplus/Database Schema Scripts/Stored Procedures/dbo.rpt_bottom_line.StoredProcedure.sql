USE [DebtPlus]
GO
/****** Object:  StoredProcedure [dbo].[rpt_bottom_line]    Script Date: 09/15/2014 13:13:32 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[rpt_bottom_line] ( @client as typ_client ) as

-- ==============================================================================================================
-- ==            retrieve the information for the "bottom line"                                                ==
-- ==============================================================================================================

-- Suppress intermediate result sets
set nocount on

-- Obtain the net income from the employer
declare @person_1_net money
declare @person_2_net money

select	@person_1_net = net_income
from	people
where	client = @client
and	relation = 1

select	@person_2_net = sum(net_income)
from	people
where	client = @client
and	Relation  <> 1

-- Obtain the additional income amounts
declare @asset money
select	@asset = sum(asset_amount)
from	assets
where	client = @client

-- Find the budget id from the budgets table for this client
declare @budget_id int
select top 1 @budget_id = budget
from	budgets
where	client = @client
order by date_created desc

-- Calculate the budgetary expenses
declare @expense money
select	@expense = sum(suggested_amount)
from	budget_detail
where	budget = @budget_id

-- Calculate the DMP program information
declare	@dmp	money
select	@dmp = sum(cc.disbursement_factor)
from	client_creditor cc
inner join client_creditor_balances ccb on cc.client_creditor_balance = ccb.client_creditor_balance
where	cc.client = @client
and	cc.reassigned_debt = 0
and (cc.creditor like 'x%' or (ccb.orig_balance + ccb.orig_balance_adjustment - ccb.total_payments + ccb.total_interest) > 0)

-- Obtain the other debt amounts

declare @other_debts money
select	@other_debts = sum(payment)
from	client_other_debts
where	client = @client

-- Return the information
SELECT	isnull(@person_1_net,0)	as 'person_1_net',
	isnull(@person_2_net,0)	as 'person_2_net',
	isnull(@asset,0)	as 'asset',
	isnull(@expense,0)	as 'expense',
	isnull(@dmp,0)		as 'dmp',
	isnull(@other_debts,0)	as 'other_debts'

return ( 1 )
GO
