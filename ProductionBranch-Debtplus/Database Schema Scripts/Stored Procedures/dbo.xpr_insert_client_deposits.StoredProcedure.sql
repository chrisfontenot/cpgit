USE [DebtPlus]
GO
/****** Object:  StoredProcedure [dbo].[xpr_insert_client_deposits]    Script Date: 09/15/2014 13:13:35 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[xpr_insert_client_deposits] ( @client int, @deposit_date datetime, @deposit_amount money = 0, @one_time bit = 0, @ach_pull bit = 0 ) as
	insert into update_client_deposits ( client, deposit_date, deposit_amount, one_time ) values ( @client, @deposit_date, @deposit_amount, @one_time )
	return ( scope_identity() )
GO
