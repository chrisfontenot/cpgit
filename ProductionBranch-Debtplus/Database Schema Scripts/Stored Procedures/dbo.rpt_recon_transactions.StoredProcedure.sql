SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER procedure [rpt_recon_transactions] ( @batch as int ) as
-- ================================================================================================
-- ==               Return a list of the transactions and their status for the                   ==
-- ==               check reconcilation operation.                                               ==
-- ================================================================================================

-- ChangeLog
--   7/1/2002
--     Use the trust register or the reconciled item check number.

select	d.tran_type			as 'tran_type',
	isnull(tr.checknum,d.checknum)	as 'checknum',
	d.checknum			as 'checknum',
	d.cleared			as 'cleared',
	d.amount			as 'cleared_amount',
	d.recon_date			as 'cleared_date',
	d.message			as 'message',

	case
		when tr.trust_register is null then null
		when tr.creditor is not null then isnull(cr.creditor_name,'Unknown Creditor')
		when tr.client is not null then dbo.format_normal_name(pn.prefix,pn.first,pn.middle,pn.last,pn.suffix)
		else msg.description
	end				as 'payee',

	tr.amount			as 'check_amount',
	tr.date_created			as 'check_date'

from	recon_details d			with (nolock)

left outer join registers_trust tr	with (nolock) on d.trust_register = tr.trust_register
left outer join creditors cr		with (nolock) on tr.creditor = cr.creditor
left outer join people p		with (nolock) on tr.client = p.client and 1 = p.relation
left outer join names pn with (nolock) on p.nameid = pn.name
left outer join tran_types msg		with (nolock) on d.tran_type = msg.tran_type

where	recon_batch = @batch
order by 1, 2

return ( @@rowcount )
GO
