USE [DebtPlus]
GO
/****** Object:  StoredProcedure [dbo].[rpt_Sched_Pay_vs_Amt_in_Trust]    Script Date: 09/15/2014 13:13:33 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[rpt_Sched_Pay_vs_Amt_in_Trust] ( @counselor as int = null ) AS
-- ===================================================================================
-- ==             Fetch the creditor disbursement information                       ==
-- ===================================================================================

-- ChangeLog
--   5/27/2002
--     Corrected disbursement factor to limit to the debt balance as needed. We did not change the scheduled pay which is also limited but that is a different issue.
--   9/31/2002
--     Added support for client_creditor_balances table
--   01/15/2002
--     Added creditor_classes table references and dropped "creditors.fee_creditor"
--   2/18/2003
--     Added "counselor" to the select set
--   10/25/2006
--     Rewrote to adjust scheduled pay to the balance and "make it faster"

-- Suppress intermediate result sets
set nocount on

if isnull(@counselor,0) <= 0
	select	@counselor = null

-- Validate the counselor ID
if @counselor is null
begin
	if not exists (select * from counselors where counselor = @counselor)
		select	@counselor = null
end

create table	#results_2 (
	client			int,
	disbursement_date	int,
	held_in_trust		money
);

create index ix2_temp_sched_pay on #results_2 ( client );

if @counselor is null
	insert into #results_2 ( client, disbursement_date, held_in_trust )
	select	client, disbursement_date, held_in_trust
	from	clients with (nolock)
	where	client > 0
	and	held_in_trust > 0
else
	insert into #results_2 ( client, disbursement_date, held_in_trust )
	select	client, disbursement_date, held_in_trust
	from	clients with (nolock)
	where	client > 0
	and	held_in_trust > 0
	and	counselor = @counselor;

-- Select all of the debts for the clients where needed
select			c.client,
			isnull(ccl.zero_balance,0) as zero_balance,
			cc.disbursement_factor,
			bal.orig_balance + bal.orig_balance_adjustment + bal.total_interest - bal.total_payments as balance,
			cc.sched_payment

into			#results_1

from			#results_2 c			WITH (NOLOCK)
LEFT OUTER  JOIN	client_creditor cc		WITH (NOLOCK) ON c.client = cc.client
inner join		client_creditor_balances bal	WITH (NOLOCK) ON cc.client_creditor_balance = bal.client_creditor_balance AND cc.client_creditor = bal.client_creditor
left outer join		creditors cr			WITH (NOLOCK) ON cc.creditor = cr.creditor
LEFT OUTER JOIN		creditor_classes ccl		WITH (NOLOCK) ON cr.creditor_class = ccl.creditor_class
WHERE			cc.reassigned_debt = 0

-- Correct erroneous items
update			#results_1
set			disbursement_factor	= 0
where			disbursement_factor	<= 0;

update			#results_1
set			sched_payment		= 0
where			sched_payment		<= 0;	

update			#results_1
set			disbursement_factor	= 0,
			sched_payment		= 0
where			balance			<= 0
and			zero_balance		= 0;

-- Limit the sched payment to the balance of the debt
update			#results_1
set			sched_payment		= balance
where			sched_payment		>= balance
and			balance			>= 0
and			zero_balance		= 0;

-- Combine the results and return the values
SELECT			c.client						as 'client',
			dbo.format_normal_name (default, pn.first, default, pn.last, default)		as 'name',
			c.disbursement_date    					as 'disb_cycle',
			sum(x.disbursement_factor)				as 'disb_factor',
			sum(x.sched_payment)					as 'schedule_pay',
			c.held_in_trust						as 'trust'

from			#results_2 c
inner join		#results_1 x on c.client = x.client
left outer join		people p with (nolock) on c.client = p.client and 1 = p.relation
left outer join names pn with (nolock) on p.nameid = pn.name
group by		c.client, pn.first, pn.last, c.disbursement_date, c.held_in_trust
having			sum(x.sched_payment) <> c.held_in_trust
order by		c.client;

drop table	#results_1;
drop table	#results_2;

RETURN ( @@rowcount )
GO
