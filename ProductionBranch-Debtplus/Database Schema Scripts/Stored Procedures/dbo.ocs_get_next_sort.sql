USE [DebtPlus];
GO
SET ANSI_NULLS ON;
GO
SET QUOTED_IDENTIFIER ON;
GO

-- =============================================
-- Author:		Brandon Wilhite
-- Create date: 
-- Description:	
-- 12/8/2015 KWilkie - Updated to speed up overall attempts
-- =============================================
ALTER PROCEDURE [dbo].[ocs_get_next_sort] 
	@program int = 0,
	@isActive bit = null,
	@preferredLanguage int = null,
	@statusCode int = null,
	@minContacts int = null,
	@maxContacts int = null,
	@stateAbbrev varchar(5) = null,
	@queueCode varchar(10) = null,
  @lastChanceFlag int = 0,
  @counseledState int = 0,
  @counseledFromDate datetime = null,
  @counseledToDate   datetime = null,
	--TIMEZONES
	@atlantic bit = 0,
	@eastern bit = 0,
	@central bit = 0,
	@mountain bit = 0,
	@pacific bit = 0,
	@alaska bit = 0,
	@hawaii bit = 0,
	@chamorro bit = 0,
	@indeterminate bit = 0
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	
	WITH BaseLineData AS (
			select
				  ocs.ClientId [Id],
				  ocs.Id [OCSClientId],
				  ocs.ClaimedDate [ClaimedDate],
				  upload.UploadDate [UploadDate],
				  ISNULL(ocs.[StatusCode], 0) as [StatusCode],
				  --ISNULL(contact.[ContactAttempts], 0) as [AttemptCount],
          ISNULL(ocs.ContactAttempts, 0) as [AttemptCount],
				  client.language [PreferredLanguage],
				  ocs.Archive [Archive],
				  ocs.ActiveFlag [ActiveFlag],
				  ocs.QueueCode [QueueCode],
				  upload.ClientState [StateAbbreviation],
				  ocs.SearchTimezone [TzDescriptor],
				  ocs.Program [Program],
          ocs.PostModLastChanceDate, ocs.FirstCounselDate, ocs.SecondCounselDate, ocs.LastRpcDate,
          case when (ocs.FirstCounselDate is null or ocs.FirstCounselDate = '') and (ocs.SecondCounselDate is null or ocs.SecondCounselDate = '') then 1
               when ocs.FirstCounselDate is not null and (ocs.SecondCounselDate is null or ocs.SecondCounselDate = '') then 2
               when ocs.FirstCounselDate is not null and ocs.SecondCounselDate is not null then 3
          end [CounselDateOrder],
          case when contact.[end] is null then 0 else datediff(day, contact.[end], getdate()) end TimeSinceLastModification,
          case when contact.[end] is null then 0 else case when year(contact.[end]) = year(getdate()) and month(contact.[end]) = month(getdate()) then 1 else 0 end end IsProcessedInThisMonth
			from OCS_Client [ocs]
			inner join clients [client] on client.client = ocs.ClientId
			left join OCS_UploadRecord [upload] on upload.Id = ocs.UploadRecord
			LEFT JOIN OCS_ContactAttempt [contact] on 
				  ocs.ClientId = contact.ClientId AND 
				  contact.Id = (select top 1 Id from OCS_ContactAttempt where ClientId = ocs.ClientId order by [End] desc)
			where ocs.Archive = 0
)  
	--select top 1 OCSClientId from BaseLineData 
    select * from BaseLineData 
		where 
		(Program = @program)
		--Program specific time-based rules
		AND
		
		(
		--if program is Fannie and Quecode is Hightouch then the following applies
		--MAX 12 attempts, MAX 90 days
			@program <> 6 
			OR
			(
				( @queueCode is null  or @queueCode <> 'HighTouch' )
				or
				(
					--attempt count condition
					(AttemptCount < 13)
					AND
					--time-based condition			
					(DATEDIFF(day,UploadDate,getdate()) < 90)
				)
			)
		)
    AND
    (
      -- Program Freddie Mac EI, 180, 720, HAMP, PostMod
      Program not in (0, 1, 2, 3, 4)
      or
      (
        (@queueCode is null or @queueCode <> 'Counselor' )
        and 
        StatusCode not in (
          1, -- Bad Number
          6, -- Counseled
          10, -- Opt Out
          7 -- Contact Made
        )
      )
    )
		AND
		--unclaimed
		(
			ClaimedDate is null 
			OR
			CAST(FLOOR(CAST( getdate() AS float)) AS datetime) <> CAST(FLOOR(CAST( ClaimedDate AS float)) AS datetime)
		)
		AND
		--active filter
		(@isActive is null or ActiveFlag = @isActive)
		AND
		--preferred language
		(@preferredLanguage is null or PreferredLanguage = @preferredLanguage)
		AND
		--status code
		(@statusCode is null or StatusCode = @statusCode)
		--min/max contacts
		AND	(
			(@minContacts is null and @maxContacts is null)
			OR
			(@minContacts is not null and @maxContacts is not null and AttemptCount >= @minContacts AND AttemptCount <= @maxContacts)
			OR
			(@minContacts is not null and @maxContacts is null and AttemptCount >= @minContacts)
			OR
			(@minContacts is null and @maxContacts is not null and AttemptCount <= @maxContacts)
		)
		AND
		--state
		(@stateAbbrev is null or StateAbbreviation = @stateAbbrev)
		--queue
		AND
		(@queueCode is null or QueueCode = @queueCode)
		AND
		--timezone filter
		(
			(@atlantic = 1 and TzDescriptor = 'Atlantic')
			OR
			(@eastern = 1 and TzDescriptor = 'Eastern')
			OR
			(@central = 1 and TzDescriptor = 'Central')
			OR
			(@mountain = 1 and TzDescriptor = 'Mountain')
			OR
			(@pacific = 1 and TzDescriptor = 'Pacific')
			OR
			(@alaska = 1 and TzDescriptor = 'Alaska')
			OR
			(@hawaii = 1 and TzDescriptor = 'Hawaii')
			OR
			(@chamorro = 1 and TzDescriptor = 'Chamorro')
			OR
			(@indeterminate = 1 and TzDescriptor = 'Indeterminate')
		)
		AND StatusCode <> 14
		-- as per request OCSNS-22, do not show record with status exclusion report
    and 
    (
      (@lastChanceFlag is null or @lastChanceFlag = 0 ) 
      or 
      (@lastChanceFlag = 1 and PostModLastChanceDate > dateadd(year, -1, getdate()))
      or 
      (@lastChanceFlag = 2 
       and 
       (@counseledFromDate is not null or @counseledFromDate <> '')
       and
       (@counseledToDate is not null or @counseledToDate <> '')       
       and 
       PostModLastChanceDate >= @counseledFromDate
       and 
       PostModLastChanceDate <= dateadd(day, 1, @counseledToDate)
      )
    )
    and 
    (
      (@lastChanceFlag is null or @lastChanceFlag = 0 ) 
      or 
      ( (@lastChanceFlag = 1 or @lastChanceFlag = 2 )
        and 
        -- skip records processed this month
        IsProcessedInThisMonth = 0
      )
    )
    and 
    (
      (@lastChanceFlag is null or @lastChanceFlag = 0 ) 
      or 
      ( (@lastChanceFlag = 1 or @lastChanceFlag = 2 )
        and 
        -- include ONLY records that have a Right Party Contact (RPC) Date, and with a RPC date that is less than 
        --or equal to 160 days in the past (i.e Last RPC date <= today-160)
        (LastRpcDate is not null and LastRpcDate > dateadd(day, -160, getdate()))
      )
    )
    and 
    (
      (@counseledState is null or @counseledState = 0)
      or       
      @counseledState = [CounselDateOrder]
    )
    and 
    (
      -- Handle Case where ActiveFlag=Active, Queue=HighTouch/no queue, State=NewRecord then return records only with AttemptCount=0 
      (@statusCode is null or @statusCode <> 0)
      or 
      (
          @isActive <> 1
          or
          (
            --@queueCode <> 'HighTouch'
		   ( @queueCode is not  null  or @queueCode <> 'HighTouch' )
			or
            (
              ( @queueCode = 'HighTouch' or @queueCode is  null)
	             and
              AttemptCount = 0
            )
          )          
       )
    )
    
		ORDER BY
      --[UploadDate] DESC,
	  --case  when @statusCode=0 and AttemptCount = 0 then [UploadDate] end desc,
    case when @lastChanceFlag is not null and @lastChanceFlag > 0 then LastRpcDate end asc, 
	  case when @statusCode is not null and @statusCode <> '' then [UploadDate] end desc,
      AttemptCount ASC,    
    case when @statusCode is null then [UploadDate] end desc  
      --case @lastChanceFlag when 1 then TimeSinceLastModification end desc,
      --case @lastChanceFlag when 1 then CounselDateOrder          end asc
--TimeSinceLastModification desc, CounselDateOrder,  [UploadDate] DESC, AttemptCount ASC
END

GO