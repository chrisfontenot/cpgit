USE [DebtPlus]
GO
/****** Object:  StoredProcedure [dbo].[DAILY_creditor_notes]    Script Date: 09/15/2014 13:13:32 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE PROCEDURE [dbo].[DAILY_creditor_notes] AS
-- ======================================================================================
-- ==             Deal with expiration times on the creditor notes                     ==
-- ======================================================================================

-- Start a transaction for the request
BEGIN TRANSACTION

-- Do not generate intermediate results
SET NOCOUNT ON

-- Change alert notes to permanent at the expiration time
UPDATE	creditor_notes
SET	type	= 1,
	expires	= NULL
WHERE	type	= 4
AND	(NOT expires IS NULL)
AND	(expires <= getdate())

-- Delete temporary notes from the system at the expiration time
DELETE	creditor_notes
WHERE	type	= 2
AND	(NOT expires IS NULL)
AND	(expires <= getdate())

-- Make the changes to the database
COMMIT TRANSACTION
RETURN ( 1 )
GO
