USE [DebtPlus]
GO
/****** Object:  StoredProcedure [dbo].[lst_asset_ids]    Script Date: 09/15/2014 13:13:32 ******/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE PROCEDURE [dbo].[lst_asset_ids] AS

-- ===================================================================================================
-- ==            Return the list of asset IDs and the associated RPPS types                         ==
-- ===================================================================================================

select	ids.asset_id		as 'item_key',
		ids.description		as 'description',
		ids.maximum			as 'maximum',
		ids.rpps_code		as 'rpps_code',
		m.description		as 'rpps_description'
from	asset_ids ids with (nolock)
left outer join RPPSAssetTypes m with (nolock) on ids.rpps_code = m.oID
order by 1

return ( @@rowcount )
GO
