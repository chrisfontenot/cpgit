USE [DebtPlus]
GO
/****** Object:  StoredProcedure [dbo].[www_client_client_creditor_trans_menu]    Script Date: 09/15/2014 13:13:34 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[www_client_client_creditor_trans_menu] ( @client as int ) as
-- ================================================================================
-- ==            Return the list of debts for the transaction menu               ==
-- ================================================================================

-- ChangeLog
--   3/14/2003
--     Allow for more than 10,000 creditors in a class
--   06/09/2009
--      Remove references to debt_number and use client_creditor

SELECT	cc.client_creditor								as client_creditor,
	isnull(cr.creditor,'') + ' ' + coalesce(cr.creditor_name,cc.creditor_name,'')	as creditor_name,
	cc.account_number								as account_number,
	convert(int,isnull(cc.reassigned_debt,0))					as reassigned
FROM	client_creditor cc
left outer join creditors cr on cc.creditor=cr.creditor
WHERE	client=@client
ORDER BY cr.type, cr.creditor_id, cc.creditor
return ( @@rowcount )
GO
