USE [DebtPlus]
GO
/****** Object:  StoredProcedure [dbo].[www_client_budget]    Script Date: 09/15/2014 13:13:34 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[www_client_budget] ( @client as int ) AS
-- ==========================================================================
-- ==            Return the budget categories                              ==
-- ==========================================================================

-- ChangeLog
--   12/19/2008
--      Revisions for SQL2008

-- Suppress the intermediate results
set nocount on

-- Fetch the most recient budget
declare @budget_id int
select  @budget_id = dbo.map_client_to_budget ( @client )
if @budget_id is null
    select  @budget_id = -1

-- Include the budget detail
select	bc.budget_category		as 'budget_category',
	isnull(bc.description,'')	as 'description',
	isnull(bd.client_amount,0)	as 'client_amount',
	isnull(bd.suggested_amount,0)	as 'suggested_amount',
	isnull(bc.heading,0)		as 'heading_account',
	isnull(bc.detail,1)		as 'detail_account'
from    budget_categories bc with (nolock)
left outer join budget_detail bd with (nolock) on bc.budget_category = bd.budget_category AND @budget_id = bd.budget

union all

select	bc.budget_category		as 'budget_category',
	isnull(bc.description,'')	as 'description',
	isnull(bd.client_amount,0)	as 'client_amount',
	isnull(bd.suggested_amount,0)	as 'suggested_amount',
	0				as 'heading_account',
	1				as 'detail_account'
from    budget_categories_other bc with (nolock)
left outer join budget_detail bd with (nolock) on bc.budget_category = bd.budget_category AND @budget_id = bd.budget
WHERE   bc.client = @client
order by 1

return ( @@rowcount )
GO
