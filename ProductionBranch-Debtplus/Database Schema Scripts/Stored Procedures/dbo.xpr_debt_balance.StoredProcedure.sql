USE [DebtPlus]
GO
/****** Object:  StoredProcedure [dbo].[xpr_debt_balance]    Script Date: 09/15/2014 13:13:34 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[xpr_debt_balance] ( @client_creditor AS INT, @Value AS MONEY ) AS

-- ==================================================================================================
-- ==            Update the disbursement factor for the indicated debt                             ==
-- ==================================================================================================

-- ChangeLog
--   9/31/2002
--     Added support for client_creditor_balances table
--   01/15/2003
--     Added creditor_classes table references and dropped "creditors.fee_creditor"
--   1/30/2003
--     Do not record a balance change if the amount is the same.
--   3/24/2003
--     Reset scheduled payment to zero when the balance is set to zero.
--   2/1/2012
--     Return the balance information to the caller so that it will be properly shown

-- Disable intermediate result sets
SET NOCOUNT ON

-- Fetch the current values from the debt record
DECLARE	@old_balance			money
DECLARE	@old_orig_balance		money
DECLARE	@old_orig_balance_adjustment	money
DECLARE	@old_total_interest		money
DECLARE	@old_total_payments		money
DECLARE	@setting_orig_balance		bit
DECLARE	@Local				bit
DECLARE	@Client				int

SELECT	@Client				= cc.client,
	@Local				= convert(int, isnull(ccl.zero_balance,0)),
	@old_orig_balance		= isnull(bal.orig_balance,0),
	@old_orig_balance_adjustment	= isnull(bal.orig_balance_adjustment,0),
	@old_total_interest		= isnull(bal.total_interest,0),
	@old_total_payments		= isnull(bal.total_payments,0)

FROM	client_creditor cc			WITH (NOLOCK)
INNER JOIN client_creditor_balances bal		WITH (NOLOCK) ON cc.client_creditor_balance = bal.client_creditor_balance
LEFT OUTER JOIN creditors cr			WITH (NOLOCK) ON cc.creditor = cr.creditor
LEFT OUTER JOIN creditor_classes ccl		WITH (NOLOCK) ON cr.creditor_class = ccl.creditor_class

WHERE	cc.client_creditor = @client_creditor

IF @@rowcount < 1
BEGIN
	RaisError (50054, 16, 1, @client_creditor)
	Return ( 0 )
END

-- If the debt has no balance then set the balance to zero
select	@old_balance		= @old_orig_balance + @old_orig_balance_adjustment + @old_total_interest - @old_total_payments,
	@setting_orig_balance	= 0

if (@old_orig_balance = 0) and (@old_orig_balance_adjustment = 0) and (@old_total_payments = 0) and (@old_total_interest = 0)
	select	@setting_orig_balance = 1

IF (@Local <> 0)
	select @Value = 0.0

-- The value must not be negative
IF @value < 0.0
BEGIN
	RaisError (50019, 16, 1)
	Return ( 0 )
END

-- If updating accounts which have "no balance" then set the balance zero and adjust the total interest figure.
IF @Local <> 0
BEGIN

	SELECT	@old_balance = 0.0

	UPDATE	client_creditor_balances
	SET	orig_balance		= 0,
		orig_balance_adjustment = 0,
		total_interest		= isnull(bal.total_payments,0)
	FROM	client_creditor_balances bal
	INNER JOIN client_creditor cc on bal.client_creditor_balance = cc.client_creditor_balance
	WHERE	cc.client_creditor	= @client_creditor

	IF @@rowcount < 1
	BEGIN
		RaisError (50054, 16, 1, @client_creditor)
		Return ( 0 )
	END

-- Update the value with the new setting
END ELSE BEGIN

	if @setting_orig_balance = 0
	begin
		UPDATE	client_creditor_balances
		SET	orig_balance_adjustment = @value - @old_orig_balance - @old_total_interest + @old_total_payments
		FROM	client_creditor_balances bal
		INNER JOIN client_creditor cc ON bal.client_creditor_balance = cc.client_creditor_balance
		WHERE	cc.client_creditor	= @client_creditor

		IF @@rowcount < 1
		BEGIN
			RaisError (50054, 16, 1, @client_creditor)
			Return ( 0 )
		END

		-- If setting the balance to zero then also reset the scheduled payment to zero.
		IF @value = 0
		begin
			UPDATE	client_creditor
			set	sched_payment	= 0
			WHERE	client_creditor	= @client_creditor

			if @old_balance <> @value
				INSERT INTO client_notes(	client,		client_creditor,	type,	is_text,	subject,	note
							)
				VALUES			(	@client,	@client_creditor,	3,	1,		'Scheduled Payment Changed',
								'The scheduled payment was reset to $0.00 because the balance was changed to $0.00'
							)
		end

	END ELSE BEGIN

		UPDATE	client_creditor_balances
		SET	orig_balance		= @value
		FROM	client_creditor_balances bal
		INNER JOIN client_creditor cc ON bal.client_creditor_balance = cc.client_creditor_balance
		WHERE	cc.client_creditor	= @client_creditor

		IF @@rowcount < 1
		BEGIN
			RaisError (50054, 16, 1, @client_creditor)
			Return ( 0 )
		END
	END
END

-- Generate a system note that the value was changed
if @old_balance <> @value
	INSERT INTO client_notes(	client,		client_creditor,	type,	is_text,	subject,	note
				)
	VALUES			(	@client,	@client_creditor,	3,	1,		'Balance Changed',
					'The balance was changed from $' + convert(varchar,@old_balance,1) + ' to $' + convert(varchar,@value,1) + '.'
				)

-- Return the current balance information so that the values are synchronized properly
SELECT	bal.orig_balance, bal.orig_balance_adjustment, bal.total_interest, bal.total_payments
FROM	client_creditor cc WITH (NOLOCK)
INNER JOIN client_creditor_balances bal WITH (NOLOCK) ON cc.client_creditor_balance = bal.client_creditor_balance
WHERE	cc.client_creditor = @client_creditor

-- Return success to the caller
RETURN ( 1 )
GO
