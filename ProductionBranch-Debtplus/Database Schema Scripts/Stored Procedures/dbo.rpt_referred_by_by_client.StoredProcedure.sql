USE [DebtPlus]
GO
/****** Object:  StoredProcedure [dbo].[rpt_referred_by_by_client]    Script Date: 09/15/2014 13:13:33 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[rpt_referred_by_by_client] ( @From_Date as datetime = null, @To_Date as datetime = null, @Referred_by as int = null ) as

-- Correct the time values
if @To_Date is null
	select	@To_date = getdate()

if @From_date is null
	select	@From_date = @To_Date

select	@From_date = convert(datetime, convert(varchar(10), @From_Date, 101) + ' 00:00:00'),
	@To_date   = convert(datetime, convert(varchar(10), @To_Date, 101) + ' 23:59:59')

-- If there is no referral source then use all
if @Referred_by is null
	select	c.client,
		dbo.format_normal_name ( pn.prefix, pn.first, pn.middle, pn.last, pn.suffix ) as client_name,
		ca.start_time as appt_date,
		ca.result as appt_result,
		rs.description as referral_source
	from	clients c with (nolock)
	inner join client_appointments ca with (nolock) on c.client = ca.client
	left outer join referred_by rs with (nolock) on c.referred_by = rs.referred_by
	left outer join people p with (nolock) on c.client = p.client and 1 = p.relation
	left outer join names pn with (nolock) on p.nameid = pn.name
	where	ca.start_time between @From_Date and @To_Date
	and	ca.office is not null
	and	ca.status in ('K','W')
	order by c.client, ca.start_time
else
	select	c.client,
		dbo.format_normal_name ( pn.prefix, pn.first, pn.middle, pn.last, pn.suffix ) as client_name,
		ca.start_time as appt_date,
		ca.result as appt_result,
		rs.description as referral_source
	from	clients c with (nolock)
	inner join client_appointments ca with (nolock) on c.client = ca.client
	left outer join referred_by rs with (nolock) on c.referred_by = rs.referred_by
	left outer join people p with (nolock) on c.client = p.client and 1 = p.relation
	left outer join names pn with (nolock) on p.nameid = pn.name
	where	ca.start_time between @From_Date and @To_Date
	and	ca.office is not null
	and	ca.status in ('K','W')
	and	c.referred_by = @Referred_by
	order by c.client, ca.start_time

return ( @@rowcount )
GO
