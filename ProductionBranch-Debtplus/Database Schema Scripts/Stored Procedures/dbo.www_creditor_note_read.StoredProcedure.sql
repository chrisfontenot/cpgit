USE [DebtPlus]
GO
/****** Object:  StoredProcedure [dbo].[www_creditor_note_read]    Script Date: 09/15/2014 13:13:34 ******/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE PROCEDURE [dbo].[www_creditor_note_read] ( @creditor as typ_creditor ) as
-- =========================================================================================
-- ==            Return the pending notes                                                 ==
-- =========================================================================================

set nocount on

-- Record the date that the note was last shown to the user
update	creditor_www_notes
set	date_shown	= getdate()
where	creditor	= @creditor
and	date_viewed is null;

-- Return the records from the note table to the user
select	creditor_www_note			as 'item_key',
	convert(varchar(10), date_created, 1)	as 'item_date',
	message					as 'message'
from	creditor_www_notes
where	creditor		= @creditor
and	date_viewed is null;

return ( @@rowcount )
GO
