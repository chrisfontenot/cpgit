USE [DebtPlus]
GO
/****** Object:  StoredProcedure [dbo].[www_client_note_read]    Script Date: 09/15/2014 13:13:34 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE PROCEDURE [dbo].[www_client_note_read] ( @client as typ_client ) as
-- =========================================================================================
-- ==            Return the pending notes                                                 ==
-- =========================================================================================

set nocount on

declare	@pkid			uniqueidentifier
select	@pkid			= pkid
from	client_www
where	username		= convert(varchar,@client)
and		applicationname	= '/';

-- Record the date that the note was last shown to the user
update	client_www_notes
set		date_shown	= getdate()
where	pkid			= pkid
and		date_viewed is null;

-- Return the records from the note table to the user
select	client_www_note				as 'item_key',
		convert(varchar(10), date_created, 1)	as 'item_date',
		message					as 'message'
from	client_www_notes
where	pkid = @pkid
and		date_viewed is null;

return ( @@rowcount )
GO
