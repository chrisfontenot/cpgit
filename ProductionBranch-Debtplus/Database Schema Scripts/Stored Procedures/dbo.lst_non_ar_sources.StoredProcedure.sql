USE [DebtPlus]
GO
/****** Object:  StoredProcedure [dbo].[lst_non_ar_sources]    Script Date: 09/15/2014 13:13:32 ******/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE PROCEDURE [dbo].[lst_non_ar_sources] AS

-- ===================================================================================================
-- ==                Return the list of deposit reasons for the creditor contributions              ==
-- ===================================================================================================

SELECT	non_ar_source		as 'item_key',
	[description]		as 'description'
FROM	non_ar_sources WITH ( NOLOCK )

return ( @@rowcount )
GO
