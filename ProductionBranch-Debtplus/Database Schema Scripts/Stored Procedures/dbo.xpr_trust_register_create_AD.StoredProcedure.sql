SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER procedure [xpr_trust_register_create_AD] ( @creditor as typ_creditor, @amount AS Money = 0, @item_date AS DateTime = NULL, @cleared AS VarChar(1) = NULL, @checknum as BigInt = NULL, @bank as int = 1 ) AS

-- ====================================================================================================
-- ==   Create a check in the trust register for paying this creditor on an automatic disbursement   ==
-- ====================================================================================================

-- ChangeLog
--   5/10/2002
--     Retrieve the "mail_priority" value from the creditor as the check print priority.

SET NOCOUNT ON

-- Determine the priority for the checks from the creditor information
declare	@mail_priority	int
select	@mail_priority = mail_priority
from	creditors with (nolock)
where	creditor = @creditor

if @mail_priority is null
	select	@mail_priority = 9
else
	if @mail_priority < 0 or @mail_priority > 9
		select	@mail_priority = 9

-- Insert the item into the trust register
INSERT INTO	registers_trust (tran_type,	creditor,	date_created,	amount,		cleared,	checknum,	check_order,	bank)
select				'AD',		@creditor,	getdate(),	0,		'P',		null,		@mail_priority,	@bank

-- Return the trust register ID to the caller
RETURN ( SCOPE_IDENTITY() )
GO
