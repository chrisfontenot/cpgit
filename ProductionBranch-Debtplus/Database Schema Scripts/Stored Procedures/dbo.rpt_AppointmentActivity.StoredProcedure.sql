USE [DebtPlus]
GO
/****** Object:  StoredProcedure [dbo].[rpt_AppointmentActivity]    Script Date: 09/15/2014 13:13:32 ******/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE PROCEDURE [dbo].[rpt_AppointmentActivity] AS

-- ==============================================================================================================
-- ==            Return the information needed for the appointment activity report                             ==
-- ==============================================================================================================

SELECT	o.office						as office,
	case
		when o.name is null then 'unknown office'
		when o.name like '% office' then 'the ' + o.name
		else 'the ' + o.name + ' office'
	end							as office_name
FROM	offices o WITH (NOLOCK)
WHERE	type in (1, 2, 3)

union all

select	-1							as office,
	'all offices'						as office_name

return ( @@rowcount )
GO
