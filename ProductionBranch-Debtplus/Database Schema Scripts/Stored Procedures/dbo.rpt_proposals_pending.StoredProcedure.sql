USE [DebtPlus]
GO
/****** Object:  StoredProcedure [dbo].[rpt_proposals_pending]    Script Date: 09/15/2014 13:13:33 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE PROCEDURE [dbo].[rpt_proposals_pending] ( @From_Date as datetime = null, @To_Date as datetime = null, @counselor as int = null )  AS
--  ScZ / 03.18.2004
--   06/09/2009
--      Remove references to debt_number and use client_creditor

if @to_date is null
	select	@to_date = getdate()

if @from_date is null
	select	@from_date = @to_date

select	@from_date = convert(datetime, convert(varchar(10), @from_date, 101) + ' 00:00:00'),
	@to_date   = convert(datetime, convert(varchar(10), @to_date,   101) + ' 23:59:59')

if isnull(@counselor,0) <= 0
	select @counselor = null

if @counselor is null
begin
	if not exists (select * from counselors where counselor = @counselor)
		select @counselor = null
end

if @counselor is null
begin
	select	cc.client,
		dbo.format_normal_name (default, ppn.first, default, ppn.last, default) as 'name',
		cc.creditor,
		cr.creditor_name,
		cc.client_creditor,
		cc.account_number,
		coalesce(p.proposal_print_date, ids.date_transmitted, ids.date_closed, p.date_created) as date_created,
		isnull(dbo.format_normal_name(default,cox.first,default,cox.last,default),'') as counselor

	from		client_creditor_proposals p
	inner join	client_creditor cc with (nolock) on p.client_creditor = cc.client_creditor
	left outer join	creditors cr with (nolock) on cc.creditor = cr.creditor
	inner join	clients c with (nolock) on cc.client = c.client
	left outer join	people pp with (nolock) on c.client = pp.client and 1 = pp.relation
	left outer join names ppn with (nolock) on pp.nameid = ppn.name
	left outer join counselors co with (nolock) on c.counselor = co.counselor
	left outer join names cox with (nolock) on co.NameID = cox.name
	inner join client_creditor_balances bal with (nolock) on cc.client_creditor_balance = bal.client_creditor_balance
	inner join proposal_batch_ids ids with (nolock) on p.proposal_batch_id = ids.proposal_batch_id

	where	coalesce(p.proposal_print_date, ids.date_transmitted, ids.date_closed, p.date_created) between @From_Date and @To_Date
	and	c.active_status in ('A', 'AR')
	and	p.proposal_status in ('1', '0')
	and	bal.orig_balance+bal.orig_balance_adjustment+bal.total_interest > bal.total_payments
	and	(ids.date_closed is not null or ids.date_transmitted is not null)
	and	cc.reassigned_debt = 0

	-- Find only creditors who have requests for proposals. This is a hack. FIX ME!!!!
	and	cr.creditor_id in (select creditor from creditor_methods where type = 'EDI')

	order by 8, 1, 7, 3	-- counselor, client, date, creditor id

end else begin

	select	cc.client,
		dbo.format_normal_name (default, ppn.first, default, ppn.last, default) as 'name',
		cc.creditor,
		cr.creditor_name,
		cc.client_creditor,
		cc.account_number,
		coalesce(p.proposal_print_date, ids.date_transmitted, ids.date_closed, p.date_created) as date_created,
		isnull(dbo.format_normal_name(default,cox.first,default,cox.last,default),'') as counselor

	from		client_creditor_proposals p
	inner join	client_creditor cc with (nolock) on p.client_creditor = cc.client_creditor
	left outer join	creditors cr with (nolock) on cc.creditor = cr.creditor
	inner join	clients c with (nolock) on cc.client = c.client
	left outer join	people pp with (nolock) on c.client = pp.client and 1 = pp.relation
	left outer join	names ppn with (nolock) on pp.nameid = ppn.name
	left outer join counselors co with (nolock) on c.counselor = co.counselor
	left outer join names cox with (nolock) on co.NameID = cox.name
	inner join client_creditor_balances bal with (nolock) on cc.client_creditor_balance = bal.client_creditor_balance
	inner join proposal_batch_ids ids with (nolock) on p.proposal_batch_id = ids.proposal_batch_id

	where	coalesce(p.proposal_print_date, ids.date_transmitted, ids.date_closed, p.date_created) between @From_Date and @To_Date
	and	c.active_status in ('A', 'AR')
	and	p.proposal_status < 2
	and	bal.orig_balance+bal.orig_balance_adjustment+bal.total_interest > bal.total_payments
	and	(ids.date_closed is not null or ids.date_transmitted is not null)
	and	cc.reassigned_debt = 0

	-- Finally, include only the selected counselor
	and	c.counselor = @counselor

	order by 8, 1, 7, 3	-- counselor, client, date, creditor id
end

return ( @@rowcount )
GO
