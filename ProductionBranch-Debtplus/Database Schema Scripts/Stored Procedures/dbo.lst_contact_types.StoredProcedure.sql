USE [DebtPlus]
GO
/****** Object:  StoredProcedure [dbo].[lst_contact_types]    Script Date: 09/15/2014 13:13:32 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[lst_contact_types] AS
-- ===================================================================================================
-- ==                Return the of contact types for creditors                                      ==
-- ===================================================================================================

select	creditor_contact_type as item_key,
	coalesce(description,name,'') as description
from	creditor_contact_types WITH ( NOLOCK )
return (@@rowcount)
GO
