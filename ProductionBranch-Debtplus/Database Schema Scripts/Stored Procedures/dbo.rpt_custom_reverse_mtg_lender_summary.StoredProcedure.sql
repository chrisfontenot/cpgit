USE [DebtPlus]
GO
/****** Object:  StoredProcedure [dbo].[rpt_custom_reverse_mtg_lender_summary]    Script Date: 09/15/2014 13:13:33 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[rpt_custom_reverse_mtg_lender_summary] ( @From_Date as datetime = null, @To_Date as datetime = null ) as

if @To_Date is null
	select	@To_Date = getdate()

if @From_Date is null
	select	@From_Date = @To_Date

select	@From_Date = convert(varchar(10), @From_Date, 101),
	@To_Date = convert(varchar(10), @To_Date, 101) + ' 23:59:59'

select	cc.creditor,
	c.client,
	v.name,
	cr.creditor_name,
	c.active_status,
	cc.date_created,
	cc.date_disp_changed,
	cc.disbursement_factor,
	cc.reassigned_debt,
	b.total_payments,
	b.orig_balance + b.orig_balance_adjustment + b.total_interest - b.total_payments as balance,
	dbo.format_normal_name(cox.prefix,cox.first,cox.middle,cox.last,cox.suffix) as counselor_name
from		view_client_address v		with (nolock)
inner join	clients c with (nolock) on v.client = c.client
left outer join counselors co with (nolock) on c.counselor = co.counselor
left outer join names cox with (nolock) on co.nameid = cox.name
inner join	client_creditor cc		with (nolock) on v.client = cc.client
inner join	client_creditor_balances b	with (nolock) on cc.client_creditor = b.client_creditor and b.client_creditor_balance = cc.client_creditor_balance
inner join	creditors cr			with (nolock) on cc.creditor = cr.creditor

where	cc.creditor in ('X62243','R62239','R57904','R57842','X62240','X62590', 'V68301', 'V68302')

and	cc.date_created between @From_Date and @To_Date
--  and disbursement_factor <> 0
and reassigned_debt = 'false'

order by 1, 2

return ( @@rowcount )
GO
