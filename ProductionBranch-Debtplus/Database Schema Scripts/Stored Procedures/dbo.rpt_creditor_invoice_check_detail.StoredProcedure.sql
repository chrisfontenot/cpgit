SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER procedure [rpt_creditor_invoice_check_detail] ( @Invoice AS INT ) AS
-- ================================================================
-- ==         Retrieve the Invoice Detail Information            ==
-- ================================================================

-- Fetch the creditor from the invoice number
DECLARE @Creditor	typ_creditor
DECLARE @date_printed	DATETIME
select	@date_printed	= getdate()

SELECT	@creditor	= creditor
from	registers_invoices
where	invoice_register = @invoice

IF @creditor IS NULL
BEGIN
	RAISERROR (50042, 16, 1, @invoice)
	RETURN ( 0 )
END

SELECT DISTINCT	r.invoice_register							as 'invoice',

		convert(datetime, convert(varchar(10), r.inv_date,101) + ' 00:00:00')	as 'inv_date',
		r.inv_amount								as 'inv_amt',
		convert(datetime, convert(varchar(10), r.pmt_date,101) + ' 00:00:00')	as 'pmt_date',
		r.pmt_amount								as 'pmt_amt',
		convert(datetime, convert(varchar(10), r.adj_date,101) + ' 00:00:00')	as 'adj_date',
		r.adj_amount								as 'adj_amt',

		case	
			when t.creditor is not null then t.checknum
			else 'E' + right('00000000' + convert(varchar, d.trust_register),8)
		end									as 'checknum'

FROM		registers_invoices r
INNER JOIN	registers_client_creditor d ON d.invoice_register = r.invoice_register
LEFT OUTER JOIN	registers_trust t ON t.trust_register = d.trust_register
WHERE		r.creditor = @Creditor
AND		isnull(r.inv_amount,0) > (isnull(r.pmt_amount,0) + isnull(r.adj_amount,0))
AND		d.tran_type in ('AD', 'MD', 'BW', 'CM')

RETURN ( @@rowcount )
GO
