USE [DebtPlus]
GO
/****** Object:  StoredProcedure [dbo].[xpr_debt_event_list_item]    Script Date: 09/15/2014 13:13:35 ******/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE PROCEDURE [dbo].[xpr_debt_event_list_item] ( @client_creditor_event as int ) AS

-- ==================================================================================================
-- ==            Return the list of events for the debt                                            ==
-- ==================================================================================================

set nocount on

-- Complain if the record has been "used"
if exists (select * from client_creditor_events where date_updated is not null and client_creditor_event = @client_creditor_event)
begin
	RaisError ('The record has been updated in the database and may not be edited.', 16, 1)
	rollback transaction
	return ( 0 )
end

declare	@paf_creditor		varchar(10)
select	@paf_creditor = paf_creditor
from	config

-- Retrieve the changes to the PAF account. These are really in the client as well.
if @paf_creditor is not null
begin
	select	client_creditor_event					as 'item_key',
		effective_date						as 'item_date',
		[value]							as 'item_value',
		'Change PAF' + isnull(' to ' + fee.description,'')	as 'item_description'
	from	client_creditor_events e
	left outer join client_creditor cc on e.client_creditor = cc.client_creditor
	left outer join config_fees fee on e.config_fee = fee.config_fee
	where	e.client_creditor_event	= @client_creditor_event
	and	cc.creditor = @paf_creditor

	union all

	select	client_creditor_event				as 'item_key',
		effective_date					as 'item_date',
		[value]						as 'item_value',
		'Change disbursement factor'			as 'item_description'
	from	client_creditor_events e
	left outer join client_creditor cc on e.client_creditor = cc.client_creditor
	where	e.client_creditor_event	= @client_creditor_event
	and	cc.creditor != @paf_creditor

	order by 2;

end else begin

	-- Retrieve the items to update the disbursements
	select	client_creditor_event				as 'item_key',
		effective_date					as 'item_date',
		[value]						as 'item_value',
		'Change disbursement factor'			as 'item_description'
	from	client_creditor_events e
	left outer join client_creditor cc on e.client_creditor = cc.client_creditor
	where	e.client_creditor_event	= @client_creditor_event

	order by 2;
end

return ( @@rowcount )
GO
