USE [DebtPlus]
GO
/****** Object:  StoredProcedure [dbo].[lst_retention_events]    Script Date: 09/15/2014 13:13:32 ******/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE PROCEDURE [dbo].[lst_retention_events] AS

-- ===============================================================================================================
-- ==            This is used to populate the combobox for the retention events                                 ==
-- ===============================================================================================================

select	retention_event			as 'item_key',
	description			as 'description'
from	retention_events with (nolock)
order by 2

return ( @@rowcount )
GO
