USE [DebtPlus]
GO
/****** Object:  StoredProcedure [dbo].[xpr_insert_housing_arm_hcs_id_AppointmentTypes]    Script Date: 09/15/2014 13:13:35 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[xpr_insert_housing_arm_hcs_id_AppointmentTypes] (@hcs_id int, @AppointmentType varchar(10)) as
insert into housing_arm_hcs_id_AppointmentTypes([hcs_id],[AppointmentType]) values (@hcs_id, @AppointmentType)
return (scope_identity())
GO
