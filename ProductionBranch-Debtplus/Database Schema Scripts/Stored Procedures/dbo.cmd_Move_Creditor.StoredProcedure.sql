USE [DebtPlus]
GO
/****** Object:  StoredProcedure [dbo].[cmd_Move_Creditor]    Script Date: 09/15/2014 13:13:31 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[cmd_Move_Creditor] ( @old_creditor as varchar(10), @new_creditor as varchar (10)) as

-- ===================================================================================================
-- ==            Move all of the transactions, references, etc. from one creditor to another        ==
-- ===================================================================================================

-- ChangeLog
--   06/09/2009
--      Remove references to debt_number and use client_creditor

-- Suppress intermediate results
set nocount on

-- Ensure that the old creditor is present
declare	@err_msg	varchar(80)
if not exists (select * from creditors where creditor = @old_creditor)
begin
	select	@err_msg = 'The old creditor ' + @old_creditor + ' is not defined in the system.'
	RaisError (@err_msg, 16, 1)
	return ( 0 )
end

-- Ensure that the new creditor is valid
if @new_creditor not like '[A-Z][0-9][0-9][0-9][0-9]%'
begin
	select	@err_msg = 'The new creditor ' + @new_creditor + ' is not a valid creditor id.'
	RaisError (@err_msg, 16, 1)
	return ( 0 )
end

if @old_creditor = @new_creditor
begin
	select	@err_msg = 'The new creditor and old creditor ' + @new_creditor + ' cannot be the same.'
	RaisError (@err_msg, 16, 1)
	return ( 0 )
end

-- Start a transation to hold everything
begin transaction

declare	@created_creditor	int
select	@created_creditor	= 1

-- Determine if the new creditor is already in the database
if exists ( select * from creditors where creditor = @new_creditor )
	select	@created_creditor	= 0

-- Change the creditor if the creditor does not exist
if @created_creditor = 1
begin
	-- Move the creditor
	update	creditors
	set	creditor	= @new_creditor
	where	creditor	= @old_creditor

	-- Move the creditor addresses
	delete
	from	creditor_addresses
	where	creditor	= @new_creditor

	update	creditor_addresses
	set	creditor	= @new_creditor
	where	creditor	= @old_creditor

	-- Move the creditor policies
	delete
	from	policy_matrix_policies
	where	creditor	= @new_creditor

	update	policy_matrix_policies
	set	creditor	= @new_creditor
	where	creditor	= @old_creditor

	-- move the creditor contribution percentages
	delete
	from	creditor_contribution_pcts
	where	creditor	= @new_creditor

	update	creditor_contribution_pcts
	set	creditor	= @new_creditor
	where	creditor	= @old_creditor

	-- move the sales_creditors
	delete
	from	sales_creditors
	where	creditor	= @new_creditor

	update	sales_creditors
	set	creditor	= @new_creditor
	where	creditor	= @old_creditor

	-- move the creditor_statistics
	delete
	from	creditor_statistics
	where	creditor	= @new_creditor

	update	creditor_statistics
	set	creditor	= @new_creditor
	where	creditor	= @old_creditor

	-- move the creditor_contacts
	delete
	from	creditor_contacts
	where	creditor	= @new_creditor

	update	creditor_contacts
	set	creditor	= @new_creditor
	where	creditor	= @old_creditor
end

-- move the creditor notes
update	creditor_notes
set	creditor	= @new_creditor
where	creditor	= @old_creditor

-- move the registers_non_ar
update	registers_non_ar
set	creditor	= @new_creditor
where	creditor	= @old_creditor

-- move the registers_invoices
update	registers_invoices
set	creditor	= @new_creditor
where	creditor	= @old_creditor

-- move the www_notes
update	creditor_www_notes
set	creditor	= @new_creditor
where	creditor	= @old_creditor

-- Move the creditor addkeys
update	creditor_addkeys
set	creditor	= @new_creditor
where	creditor	= @old_creditor

-- Move the prefixes
update	creditor_prefixes
set	creditor	= @new_creditor
where	creditor	= @old_creditor

-- Move the registers_trust
update	registers_trust
set	creditor	= @new_creditor
where	creditor	= @old_creditor

-- Move the registers_creditor
update	registers_creditor
set	creditor	= @new_creditor
where	creditor	= @old_creditor

-- Move the disbursement_creditor_notes
update	debt_notes
set	creditor	= @new_creditor
where	creditor	= @old_creditor

-- Update the rpps_response_details_cdm
update	rpps_response_details_cdm
set	creditor	= @new_creditor
where	creditor	= @old_creditor

-- Update the debts with the new creditor / debt
update	client_creditor
set		creditor		= @new_creditor
where	creditor		= @old_creditor;

-- Update the registers_client_creditor table
update	registers_client_creditor
set		creditor		= @new_creditor
where	creditor		= @old_creditor;

-- Update the refund transactions
update	deposit_batch_details
set	creditor		= @new_creditor
where	creditor		= @old_creditor;

-- Update the disbursement_creditors
update	disbursement_creditors
set	creditor		= @new_creditor
where	creditor		= @old_creditor;

-- Update the rpps_transactions
update	rpps_transactions
set	creditor		= @new_creditor
where	creditor		= @old_creditor;

-- Update the epay_transactions
update	epay_transactions
set	creditor		= @new_creditor
where	creditor		= @old_creditor;

update	rpps_messages
set	creditor		= @new_creditor
where	creditor		= @old_creditor;

update	deposit_batch_details
set		creditor		= @new_creditor
where	creditor		= @old_creditor

-- remove the old creditor addresses
delete
from	creditor_addresses
where	creditor	= @old_creditor

-- remove the creditor policies
delete
from	policy_matrix_policies
where	creditor	= @old_creditor

-- remove the creditor contribution percentages
delete
from	creditor_contribution_pcts
where	creditor	= @old_creditor

-- remove the sales_creditors
delete
from	sales_creditors
where	creditor	= @old_creditor

-- remove the creditor_statistics
delete
from	creditor_statistics
where	creditor	= @old_creditor

-- remove the creditor_contacts
delete
from	creditor_contacts
where	creditor	= @old_creditor

-- remove the old creditor
delete
from	creditors
where	creditor	= @old_creditor

-- Commit the changes if we have reached this far
commit transaction
return ( 1 )
GO
