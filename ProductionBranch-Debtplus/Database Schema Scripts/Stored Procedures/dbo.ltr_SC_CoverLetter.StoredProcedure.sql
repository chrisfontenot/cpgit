USE [DebtPlus]
GO
/****** Object:  StoredProcedure [dbo].[ltr_SC_CoverLetter]    Script Date: 09/15/2014 13:13:32 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[ltr_SC_CoverLetter] ( @client as int, @creditor as int = null, @creditor_label as varchar(10) = null, @debt as int = null, @client_appointment as int = null, @letter_type as int = null ) AS
-- ===================================================================================
-- ==             Fetch the client header information                               ==
-- ===================================================================================

-- ChangeLog
--   7/18/2004
--     Extracted from rpt_printclient_sc_header

SET NOCOUNT ON

DECLARE	@Start_Date			DateTime
DECLARE	@Drop_Date			DateTime
DECLARE	@paf_creditor		typ_creditor
DECLARE	@Original_Debt		MONEY
DECLARE	@Creditor_Count		INT
DECLARE @Deposit_day		int
DECLARE	@Current_Deposit	MONEY

select		@paf_creditor = paf_creditor
from		config with (nolock)

-- Fetch the starting and ending dates
SELECT		@Start_Date = dbo.date_only( start_date ),
			@Drop_Date  = dbo.date_only( drop_date )
FROM		clients with (nolock)
WHERE		client = @Client

-- Fetch the original debt and creditor count
SELECT		@Original_Debt = SUM(bal.orig_balance)
FROM		client_creditor cc with (nolock)
INNER JOIN	client_creditor_balances bal with (nolock) on cc.client_creditor_balance = bal.client_creditor_balance
INNER JOIN	creditors cr with (nolock) ON cc.creditor = cr.creditor
LEFT OUTER JOIN creditor_classes ccl with (nolock) ON cr.creditor_class = ccl.creditor_class
WHERE		client = @Client
AND			isnull(ccl.zero_balance,0) = 0
AND			cc.reassigned_debt = 0

SELECT		@Creditor_Count = COUNT(*)
FROM		client_creditor cc
INNER JOIN	creditors cr with (nolock) ON cc.creditor = cr.creditor
LEFT OUTER JOIN creditor_classes ccl with (nolock) ON cr.creditor_class = ccl.creditor_class
WHERE		client = @Client
AND			isnull(ccl.zero_balance,0) = 0
AND			cc.reassigned_debt = 0
AND			cc.creditor != @paf_creditor

-- Fetch the deposit amount
SELECT		@Current_Deposit = SUM(deposit_amount)
FROM		client_deposits with (nolock)
WHERE		client = @Client

/*
-- Determine the deposit date
select		@start_date = datepart(d, min(deposit_date))
from		client_deposits
where		client = @client
*/

select		@deposit_day = datepart(d, @start_date)

-- Find the counselor for the client
declare	@counselor	varchar(80)
select	@counselor =	dbo.format_normal_name(default, cox.first, default, cox.last, default )
from	counselors co with (nolock)
inner join clients c with (nolock) on co.counselor = c.counselor
left outer join names cox with (nolock) on co.nameid = cox.name
where	c.client = @client

-- Return the result to the caller
SELECT	@counselor				as 'counselor',
	isnull(@deposit_day,1)			as 'deposit_day',
	isnull(@Start_Date,getdate())		AS 'start_date',
	@Original_Debt				AS 'original_debt',
	@Creditor_Count				AS 'creditor_count',
	@Current_Deposit			AS 'current_deposit',
	@Drop_Date				AS 'completion_date'

RETURN ( 1 )
GO
