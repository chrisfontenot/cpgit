USE [DebtPlus]
GO
/****** Object:  StoredProcedure [dbo].[rpt_appt_stats_same_month]    Script Date: 09/15/2014 13:13:32 ******/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[rpt_appt_stats_same_month] ( @FromDate AS DateTime = NULL, @ToDate AS DateTime = NULL ) AS

-- ====================================================================================================
-- ==            Return the number of same month return appointments by counselor/office             ==
-- ====================================================================================================

IF @ToDate IS NULL
	SET @ToDate = getdate()

IF @FromDate IS NULL
	SET @FromDate = @ToDate

-- Adjust the dates for the proper settings
SET @FromDate	= convert(datetime, convert(varchar(10), @FromDate, 101) + ' 00:00:00')
SET @ToDate	= convert(datetime, convert(varchar(10), @ToDate, 101)   + ' 23:59:59')

-- Retrieve the number of kept appointments within the time period
select	ca.client,count(*) as cnt
into	#t_rpt_appt_stats_same_month_1
from	client_appointments ca
where	ca.start_time between @FromDate and @ToDate
AND	ca.status	IN ('K', 'W')
group by ca.client

-- Fetch the results of the appointment
select	isnull(co.name,'Not specified')	as 'counselor',
	isnull(o.name,'Not Specified')	as 'office',
	count(*)			as 'count'

from	#t_rpt_appt_stats_same_month_1 x
inner join clients c		with (nolock) on x.client = c.client
left outer join counselors co	with (nolock) on c.counselor = co.counselor
left outer join offices o	with (nolock) on c.office = o.office

where	x.cnt > 1
group by co.name, o.name
ORDER BY 1, 2

-- Discard the intermediate table
drop table #t_rpt_appt_stats_same_month_1

RETURN ( @@rowcount )
GO
