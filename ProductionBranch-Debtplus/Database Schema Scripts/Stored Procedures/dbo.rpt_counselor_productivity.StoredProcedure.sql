USE [DebtPlus]
GO
/****** Object:  StoredProcedure [dbo].[rpt_counselor_productivity]    Script Date: 09/15/2014 13:13:33 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[rpt_counselor_productivity] ( @FromDate AS DateTime = NULL, @ToDate AS DateTime = NULL ) AS
-- ====================================================================================================
-- ==            Return the kept appointments by counselor/office                                    ==
-- ====================================================================================================

-- ChangeLog
--   2/20/2002
--     Looked only at client appointments, not workshop appointments.

-- Suppress intermediate result sets
set nocount on

IF @ToDate IS NULL
	SELECT @ToDate = getdate()

IF @FromDate IS NULL
	SELECT @FromDate = @FromDate

SELECT	@FromDate	= convert(datetime, convert(varchar(10), @FromDate, 101) + ' 00:00:00'),
		@ToDate		= dateadd(d, 1, convert(datetime, convert(varchar(10), @ToDate,  101) + ' 00:00:00'))

-- Create a table for the results
create table #results ( start_time datetime null, client int null, firstname varchar(80) null, lastname varchar(80) null, city varchar(80) null, result varchar(10) null, counselor varchar(80) null, office varchar(80) null, type varchar(80) null, hud_type int null, item_value int null, item_type varchar(80) null, hud_grant varchar(80) null, max_hud_int_date datetime null, language varchar(80) null, state varchar(10) null, hud_interview int null, housing_type int null, client_appointment int null);

-- Retrieve the appointment information
insert into #results ( start_time, client, client_appointment )
select	ca.start_time, ca.client, ca.client_appointment
from	client_appointments ca	with (nolock)
where	ca.status IN ('K','W')
AND		ca.workshop is null
AND		start_time >= @fromdate
and		start_time <  @todate

-- Find the basic information for the client
select	max(iv.interview_date) as interview_date, r.client
into	#hud_interviews
from	hud_interviews iv
inner join #results r on iv.client = r.client
group by r.client;

-- Update the interview information
update	#results
set		hud_interview			= iv.hud_interview,
		item_value				= iv.hud_grant,
		max_hud_int_date		= iv.interview_date
from	#results r
inner join #hud_interviews x on r.client = x.client
inner join hud_interviews iv on r.client = iv.client and x.interview_date = iv.interview_date;

drop table #hud_interviews

-- Find the housing type information
update	#results
set		housing_type	= p.PropertyType
from	#results r
inner join housing_properties p on r.client = p.HousingID
where	p.Residency		= 1;

-- From the items update the client data
update	#results
set		firstname				= pn.[first],
		lastname				= pn.[last],
		city					= a.[city],
		result					= isnull(ca.result, 'Not specified'),
		counselor				= isnull(dbo.format_normal_name(default,cox.first,default,cox.last,default),'Not specified'),
		office					= isnull(o.name, 'Not specified'),
		type					= isnull(ap.appt_name,'Not specified'),
		language				= lan.attribute,
		state					= st.mailingcode
from	#results r
inner join client_appointments ca on r.client_appointment = ca.client_appointment
left outer join people p on r.client = p.client and 1 = p.relation
left outer join clients c on r.client = c.client
left outer join addresses a on c.addressid = a.address
left outer join names pn on p.nameid = pn.name
left outer join offices o on c.office = o.office
left outer join counselors co on ca.counselor = co.counselor
left outer join names cox on co.nameid = cox.name
left outer join appt_types ap	with (nolock) on ca.appt_type = ap.appt_type
left outer join attributetypes lan on c.language = lan.oID
left outer join states st on a.state = st.state

-- Include the housing information
update	#results
set		hud_grant					= t.description,
		item_type					= 'HUD INTERVIEW'
from	#results r
inner join housing_GrantTypes t on r.item_value = t.oID

select	start_time, client, firstname, lastname, city, result, counselor, office, type, hud_type, item_value, item_type, hud_grant, max_hud_int_date, language, state
from	#results
order by  office, start_time, counselor
		
drop table #results

return ( @@rowcount )
GO
