USE [DebtPlus]
GO
/****** Object:  StoredProcedure [dbo].[rpt_intake_clients]    Script Date: 09/15/2014 13:13:33 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[rpt_intake_clients] as

-- ================================================================================================
-- ==            Retrieve the list of client which were created by the web intake function       ==
-- ================================================================================================
select v.*, substring(ca.additional,8,80) as intake_id
from	view_client_address v
inner join client_addkeys ca on v.client = ca.client
where	ca.additional like 'intake [0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9]'
and	v.active_status in ('A','AR')
order by v.state, v.date_created

return ( @@rowcount )
GO
