USE [DEBTPLUS]
GO
/****** Object:  StoredProcedure [dbo].[rpt_get_aarp_call_data]    Script Date: 2/9/2015 8:54:17 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


create procedure [dbo].[rpt_get_aarp_call_data](
	@fromdate	datetime,
	@todate		datetime
) as 
begin

	if @fromdate is null
		select	@fromdate	= convert(datetime, '1/1/1900')

	if @todate is null
		select	@todate	= convert(datetime, '12/31/2199')

	--select c.client
	--     , ca.date_created as CreatedOn
	--	 , ca.created_by as CreatedBy
	--	 , c.client_guid as ClientNumber
	--     , n.First as FirstName
	--	 , n.Last as LastName
	--	 , isnull(a.house, '') + ' ' + isnull(a.direction, '') + ' ' + isnull(a.street, '') + ' ' + isnull(a.suffix, '') + ' ' + isnull(a.address_line_2, '') as StreetAddress
	--	 , a.city as City
	--	 , s.MailingCode as State
	--	 , a.PostalCode as ZipCode
	--	 , e.Address as ClientEmail
	--	 , isnull(ht.Acode, '') + isnull(ht.Number, '') as HomePhone
	--	 , isnull(ct.Acode, '') + isnull(ct.Number, '') as CellPhone
	--	 , isnull(wt.Acode, '') + isnull(wt.Number, '') as WorkPhone
	--	 , ca.referred_by
	--	 , rb.description as ReferralCode
	--	 , convert(varchar, p.Birthdate, 101) as DataOfBirth
	--	 , (select case when ci.client_indicator is null then 'No' else 'Yes' end from client_indicators as ci where ci.indicator = 248 and ci.client = c.client) as InitialProgramRequested
	--	 , (select case when count(1) = 2 then 'BTW 50 and SNAP' end from client_indicators as ci where ci.indicator in (237, 238) and ci.client=c.client) as AllProgramsRequested
	--	 , hst.description as CurrentHousingType
	--	 , jd.aarp as EmploymentStatus
	--	 , (select top(1) i.description from client_indicators ci join indicators as i on ci.indicator = i.indicator where ci.indicator in (245, 246, 247) and ci.client = c.client) as CallReason
	--	 , ca.result as CallResolution
	--	 , 'Foreclosure Prevention' as LineOfService
	--	 , convert(varchar, ca.start_time, 101) as StartDate
	--	 , convert(varchar, ca.start_time, 108) as StartTime
	--	 , case when ca.status = 'K' then 'Yes' when ca.status = 'W' then 'Yes' else 'No' end as AttendedSession
	--	 , isnull((select case when client_indicator is null then 'No' else 'Yes' end from client_indicators where indicator = 240 and client = c.client), 'No') as CanWeEmail
	--	 , isnull((select case when client_indicator is null then 'No' else 'Yes' end from client_indicators where indicator = 241 and client = c.client), 'No') as CanWeText
	--	 , (select case when indicator=242 then 'Email' when indicator=243 then 'Text' when indicator=244 then 'Email and Text' else '' end from client_indicators as ci where ci.indicator IN (242, 243, 244) and ci.client = c.client) as PreferredCommunication
	--	 , 'Yes' as MailingList
	--	 , isnull((select case when client_indicator is null then 'No' else 'Yes' end from client_indicators where indicator = 239 and client = c.client), 'No') as MediaFollowUp
	--	 , isnull((select case when client_indicator is null then 'No' else 'Yes' end from client_indicators where indicator = 238 and client = c.client), 'No') as SNAPFollowUp
	--	 , isnull((select case when client_indicator is null then 'No' else 'Yes' end from client_indicators where indicator = 237 and client = c.client), 'No') as BTWFollowUp
	--	 , (select case when client_indicator is null then 'No' else 'Yes' end from client_indicators where indicator = 236 and client = c.client) as CurrentAARPClient
	--	 , case when p.MilitaryStatusID = 2 then 'Yes' else 'No' end as IsVeteran
	--  from clients as c
	--  join client_appointments as ca on c.client = ca.client
	--  --join client_indicators as ci on c.client = ci.client
	--  join people as p on c.client = p.Client
	--  join Names as n on p.NameID = n.Name
	--  join addresses as a on c.AddressID = a.address
	--  join states as s on a.state = s.state
	--  left outer join EmailAddresses as e on p.EmailID = e.Email
	--  left outer join TelephoneNumbers as ht on c.HomeTelephoneID = ht.TelephoneNumber
	--  left outer join TelephoneNumbers as ct on p.CellTelephoneID = ct.TelephoneNumber
	--  left outer join TelephoneNumbers as wt on p.WorkTelephoneID = wt.TelephoneNumber
	--  left outer join referred_by as rb on ca.referred_by = rb.referred_by
	--  left outer join client_housing as ch on c.client = ch.client
	--  left outer join Housing_StatusTypes as hst on ch.housing_status = hst.oID
	--  left outer join job_descriptions as jd on p.job = jd.job_description
	-- --where ci.indicator = 236
	-- where p.Relation = 1
	--   and ca.referred_by IN (708, 967, 968, 969, 970, 971, 972, 973, 974, 975, 976, 977, 978, 1250)
	--   and ca.date_created between @fromdate and @todate

	select c.client
	     , cl.DateCreated as CreatedOn
		 , cl.CreatedBy as CreatedBy
		 , c.client_guid as ClientNumber
	     , n.First as FirstName
		 , n.Last as LastName
		 , isnull(a.house, '') + ' ' + isnull(a.direction, '') + ' ' + isnull(a.street, '') + ' ' + isnull(a.suffix, '') + ' ' + isnull(a.address_line_2, '') as StreetAddress
		 , a.city as City
		 , s.MailingCode as State
		 , a.PostalCode as ZipCode
		 , e.Address as ClientEmail
		 , isnull(ht.Acode, '') + isnull(ht.Number, '') as HomePhone
		 , isnull(ct.Acode, '') + isnull(ct.Number, '') as CellPhone
		 , isnull(wt.Acode, '') + isnull(wt.Number, '') as WorkPhone
		 , ca.referred_by
		 , rb.description as ReferralCode
		 , convert(varchar, p.Birthdate, 101) as DataOfBirth
		 --, (select case when ci.client_indicator is null then 'No' else 'Yes' end from client_indicators as ci where ci.indicator = 248 and ci.client = c.client) as InitialProgramRequested
		 , case cl.InitialProgramRequested when 1 then 'Yes' else 'No' end as InitialProgramRequested
		 --, (select case when count(1) = 2 then 'BTW 50 and SNAP' end from client_indicators as ci where ci.indicator in (237, 238) and ci.client=c.client) as AllProgramsRequested
		 , case when cl.Btw50ProgramRequested = 1 and cl.SnapProgramRequested = 1 then 'BTW 50 and SNAP' else '' end as AllProgramsRequested
		 , hst.description as CurrentHousingType
		 , jd.aarp as EmploymentStatus
		 --, (select top(1) i.description from client_indicators ci join indicators as i on ci.indicator = i.indicator where ci.indicator in (245, 246, 247) and ci.client = c.client) as CallReason
		 , (select cr.description from call_reason as cr where oID = cl.CallReason) as CallReason
		 , (select cr.description from call_resolution as cr where oID = cl.CallResolution) as CallResolution
		 , 'Foreclosure Prevention' as LineOfService
		 , convert(varchar, ca.start_time, 101) as StartDate
		 , convert(varchar, ca.start_time, 108) as StartTime
		 , case when ca.status = 'K' then 'Yes' when ca.status = 'W' then 'Yes' else 'No' end as AttendedSession
		 --, isnull((select case when client_indicator is null then 'No' else 'Yes' end from client_indicators where indicator = 240 and client = c.client), 'No') as CanWeEmail
		 , case when cl.EmailOptIn = 1 then 'Yes' else 'No' end as CanWeEmail
		 --, isnull((select case when client_indicator is null then 'No' else 'Yes' end from client_indicators where indicator = 241 and client = c.client), 'No') as CanWeText
		 , case when cl.TextOptIn = 1 then 'Yes' else 'No' end as CanWeText
		 --, (select case when indicator=242 then 'Email' when indicator=243 then 'Text' when indicator=244 then 'Email and Text' else '' end from client_indicators as ci where ci.indicator IN (242, 243, 244) and ci.client = c.client) as PreferredCommunication
		 , (select description from contact_preferred_method where oID = cl.PreferredMethod) as PreferredCommunication
		 , 'Yes' as MailingList
		 --, isnull((select case when client_indicator is null then 'No' else 'Yes' end from client_indicators where indicator = 239 and client = c.client), 'No') as MediaFollowUp
		 , case when cl.MediaFollowup = 1 then 'Yes' else 'No' end as MediaFollowUp
		 --, isnull((select case when client_indicator is null then 'No' else 'Yes' end from client_indicators where indicator = 238 and client = c.client), 'No') as SNAPFollowUp
		 , case when cl.SnapProgramRequested = 1 then 'Yes' else 'No' end as SNAPFollowUp
		 --, isnull((select case when client_indicator is null then 'No' else 'Yes' end from client_indicators where indicator = 237 and client = c.client), 'No') as BTWFollowUp
		 , case when cl.Btw50ProgramRequested = 1 then 'Yes' else 'No' end as BTWFollowUp
		 --, (select case when client_indicator is null then 'No' else 'Yes' end from client_indicators where indicator = 236 and client = c.client) as CurrentAARPClient
		 , case when cl.IsAarpClient = 1 then 'Yes' else 'No' end as CurrentAARPClient
		 , case when p.MilitaryStatusID = 2 then 'Yes' else 'No' end as IsVeteran
	  from clients as c
	  join people as p on c.client = p.Client
	  join Names as n on p.NameID = n.Name
	  join addresses as a on c.AddressID = a.address
	  join states as s on a.state = s.state
	  join aarp_call_log as cl on c.client = cl.clientID
	  left outer join client_appointments as ca on cl.AppointmentID = ca.client_appointment
	  left outer join EmailAddresses as e on p.EmailID = e.Email
	  left outer join TelephoneNumbers as ht on c.HomeTelephoneID = ht.TelephoneNumber
	  left outer join TelephoneNumbers as ct on p.CellTelephoneID = ct.TelephoneNumber
	  left outer join TelephoneNumbers as wt on p.WorkTelephoneID = wt.TelephoneNumber
	  left outer join referred_by as rb on ca.referred_by = rb.referred_by
	  left outer join client_housing as ch on c.client = ch.client
	  left outer join Housing_StatusTypes as hst on ch.housing_status = hst.oID
	  left outer join job_descriptions as jd on p.job = jd.job_description
	 where p.Relation = 1
	   and (ca.referred_by is null or ca.referred_by IN (708, 967, 968, 969, 970, 971, 972, 973, 974, 975, 976, 977, 978, 1250))
	   and cl.DateCreated between @fromdate and @todate


end
