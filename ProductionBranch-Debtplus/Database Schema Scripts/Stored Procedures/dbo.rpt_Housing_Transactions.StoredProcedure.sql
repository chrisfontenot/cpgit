USE [DebtPlus]
GO
/****** Object:  StoredProcedure [dbo].[rpt_Housing_Transactions]    Script Date: 09/15/2014 13:13:33 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[rpt_Housing_Transactions]  ( @FromDate AS DateTime = NULL, @ToDate AS DateTime = NULL ) AS
-- ============================================================================================
-- ==              Process the transactions for Housing                                      ==
-- ============================================================================================

-- ChangeLog
--   12/24/2002
--     Changed to "hud_transactions" from "hud_details"
--   1/5/2003
--     Suppressed descriptions if the date for the interview or description is out of the range
--	01/20/2011
--	Added client's region

-- Generate default values for the dates.
IF @ToDate IS NULL
	SELECT @ToDate = getdate()

IF @FromDate IS NULL
	SELECT @FromDate = @ToDate

-- Reduce the dates to valid ranges
SELECT @FromDate = convert(datetime, convert(varchar(10), @FromDate, 101))
SELECT @ToDate   = convert(datetime, convert(varchar(10), @ToDate,   101) + ' 23:59:59')

-- Retrieve the information for the report
SELECT	isnull(g.description,'Unspecified')	as 'hud_type',
		c.client				as 'hud_id',
		hi.client				as 'client',
		dbo.format_normal_name(pn.prefix, pn.first, pn.middle, pn.last, pn.suffix) as client_name,
		case isnull(h.client_appointment,0) when 0 then 'F' else 'AP' end as 'source',
		h.date_created				as 'date_created',
		null					as 'units',
		h.minutes				as 'minutes',

		case
			when hi.interview_date is null then null
			when hi.interview_date < @FromDate then null
			when iv.description is null then 'UNSPECIFIED'
			else iv.description
		end					as 'type',

		case
			when hi.result_date is null then null
			when hi.result_date < @FromDate then null
			when r.description is null then 'UNSPECIFIED'
			else r.description
		end					as 'results',

		dbo.format_counselor_name ( h.created_by )   as 'counselor',

		isnull(hs.description,'Unknown')	as 'housing_status',

		e.description				as 'ethnic',

		case
			when hi.termination_reason is null then 'Open'
			else 'Closed'
		end					as 'status',

		re.description				as 'region'

FROM		hud_transactions h	WITH (NOLOCK)
INNER JOIN hud_interviews hi	WITH (NOLOCK) ON h.hud_interview = hi.hud_interview
INNER JOIN client_housing ids	WITH (NOLOCK) ON hi.client = ids.client
LEFT OUTER JOIN	clients c		WITH (NOLOCK) ON hi.client = c.client
LEFT OUTER JOIN people p		WITH (NOLOCK) ON hi.client = p.client AND 1 = p.relation
LEFT OUTER JOIN Names pn		WITH (NOLOCK) ON p.NameID = pn.Name
LEFT OUTER JOIN Housing_PurposeOfVisitTypes iv	WITH (NOLOCK) ON hi.interview_type = iv.oID
LEFT OUTER JOIN Housing_ResultTypes r			WITH (NOLOCK) ON hi.hud_result = r.oID
LEFT OUTER JOIN client_housing ch				WITH (NOLOCK) ON c.client = ch.client
LEFT OUTER JOIN	Housing_StatusTypes hs			WITH (NOLOCK) ON isnull(ch.housing_status,4) = hs.oID
LEFT OUTER JOIN Housing_GrantTypes g			WITH (NOLOCK) ON ids.hud_grant = g.oID
LEFT OUTER JOIN RaceTypes e		WITH (NOLOCK) ON isnull(p.race,10) = e.oID
inner join regions re			with (nolock) on c.region = re.region

WHERE		h.date_created BETWEEN @FromDate AND @ToDate
ORDER BY isnull(ids.hud_grant,0), h.date_created, c.client

RETURN ( @@rowcount )
GO
