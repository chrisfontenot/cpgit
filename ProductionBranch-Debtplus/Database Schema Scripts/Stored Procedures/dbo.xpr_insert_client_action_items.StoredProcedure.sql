USE [DebtPlus]
GO
/****** Object:  StoredProcedure [dbo].[xpr_insert_client_action_items]    Script Date: 09/15/2014 13:13:35 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[xpr_insert_client_action_items](@action_plan as int,@action_item as int) as
    -- =========================================================================================
    -- ==        Create an action_items item                                                  ==
    -- =========================================================================================
	insert into client_action_items ([action_plan], [action_item], [checked]) values (@action_plan, @action_item, 1)
    return ( scope_identity() )
GO
