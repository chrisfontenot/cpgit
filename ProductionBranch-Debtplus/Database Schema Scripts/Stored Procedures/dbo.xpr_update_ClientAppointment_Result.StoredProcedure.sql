USE [DebtPlus]
GO
/****** Object:  StoredProcedure [dbo].[xpr_update_ClientAppointment_Result]    Script Date: 09/15/2014 13:13:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[xpr_update_ClientAppointment_Result] ( @oID as uniqueidentifier, @StartDate as DateTime, @EndDate as datetime, @AppointmentType as int, @ReferredTo as int, @Office as int, @Counselor as int, @Result as varchar(3), @isWorkshop as bit = 0, @BankruptcyClass as int = null ) as
-- ================================================================================================
-- ==            Update the appointment with the result                                          ==
-- ================================================================================================

declare	@ClientAppointmentHistory	int
set nocount on

-- Put an entry into the history log
insert into ClientAppointmentsHistory([ClientAppointment], [AppointmentType], [Client], [ConfirmStatus], [Counselor], [Duration], [isLocked], [isPriority], [isWorkshop], [Office], [ReferredBy], [ReferredTo], [Results], [StartDate], [TranType], [BankruptcyClass])
select	[oID], @AppointmentType, [Client], [ConfirmStatus], [Counselor], datediff(minute, @EndDate, @StartDate), [isLocked], [isPriority], @isWorkshop, @Office, [ReferredBy], @ReferredTo, @Result, @StartDate, 'CO', @BankruptcyClass
from	ClientAppointments
where	[oID] = @oID;

select	@ClientAppointmentHistory = SCOPE_IDENTITY()

-- Remove the pending appointment since it is no longer pending
delete
from	[ClientAppointments]
where	[oID]	= @oID;

return ( @ClientAppointmentHistory )
GO
