IF NOT EXISTS(SELECT * FROM SYSOBJECTS WHERE TYPE='P' AND NAME=N'xpr_appt_result')
	EXEC('CREATE PROCEDURE [dbo].[xpr_appt_result]() AS')
GO
ALTER procedure [dbo].[xpr_appt_result] ( @client_appointment as typ_key, @client_status as varchar(4) = NULL, @appt_type as typ_key = NULL, @referred_to as typ_key = NULL, @duration as int = NULL, @office as typ_key = null, @credit as bit = 0, @counselor as int = -1, @referred_by as int = null, @end_time as datetime = null, @partner as varchar(50) = null ) AS

-- ==================================================================================================
-- ==            Update the result of an appointment for a client                                  ==
-- ==================================================================================================

-- Hold off intermediate results
set nocount on

-- Default the parameters to null as appropriate
if isnull(@referred_by,-1) <= 0
	set	@referred_by = null

if isnull(@referred_to,-1) <= 0
	set	@referred_to = null

if isnull(@counselor,-1) <= 0
	set	@counselor = null
	
if isnull(@duration,0) <= 0
	set	@duration = 0

-- Current client appointment data
declare	@cur_client					int
declare	@cur_appt_time				int
declare	@cur_counselor				int
declare	@cur_start_time				datetime
declare	@cur_end_time				datetime
declare	@cur_office					int
declare	@cur_workshop				int
declare	@cur_workshop_people		int
declare	@cur_appt_type				int
declare	@cur_status					char(1)
declare	@cur_result					varchar(10)
declare	@cur_previous_appointment	int
declare	@cur_referred_to			int
declare	@cur_referred_by			int
declare	@cur_partner				varchar(50)
declare	@cur_bankruptcy_class		int
declare	@cur_priority				int
declare	@cur_housing				bit
declare	@cur_post_purchase			bit
declare	@cur_credit					bit
declare	@cur_callback_ph			varchar(50)
declare	@cur_HousingFeeAmount		money
declare	@cur_confirmation_status	int
declare	@cur_date_confirmed			datetime
declare @cur_date_uploaded			datetime
declare	@cur_date_updated			datetime

-- Information from the client record
declare	@clnt_client				int
declare	@clnt_active_status			varchar(10)
declare	@clnt_active_status_date	datetime
declare @clnt_client_status			varchar(10)
declare @clnt_client_status_date	datetime
declare	@clnt_counselor				int
declare	@clnt_office				int
declare	@clnt_first_appt			int
declare	@clnt_first_kept_appt		int

-- We need others to hold for a moment on our data.
set transaction isolation level repeatable read
set xact_abort off
begin transaction
begin try

	-- Retrieve the current information from the client appointment table
	select	@cur_client					= [client],
			@cur_appt_time				= [appt_time],
			@cur_counselor				= [counselor],
			@cur_start_time				= [start_time],
			@cur_end_time				= [end_time],
			@cur_office					= [office],
			@cur_workshop				= [workshop],
			@cur_workshop_people		= [workshop_people],
			@cur_appt_type				= [appt_type],
			@cur_status					= [status],
			@cur_result					= [result],
			@cur_previous_appointment	= [previous_appointment],
			@cur_referred_to			= [referred_to],
			@cur_referred_by			= [referred_by],
			@cur_partner				= [partner],
			@cur_bankruptcy_class		= [bankruptcy_class],
			@cur_priority				= [priority],
			@cur_housing				= [housing],
			@cur_post_purchase			= [post_purchase],
			@cur_credit					= [credit],
			@cur_callback_ph			= [callback_ph],
			@cur_HousingFeeAmount		= [HousingFeeAmount],
			@cur_confirmation_status	= [confirmation_status],
			@cur_date_uploaded			= [date_uploaded],
			@cur_date_confirmed			= [date_confirmed],
			@cur_date_updated			= [date_updated]
	from	[client_appointments] with (rowlock)
	where	[client_appointment]		= @client_appointment

	-- There should be a client appointment or we are not able to continue
	if @@rowcount < 1
	begin
		rollback
		RaisError('client appointment record does not exist', 16, 1);
		return ( 0 )
	end

	-- Retrieve the client information
	select	@clnt_client				= client,
			@clnt_active_status			= active_status,
			@clnt_active_status_date	= active_status_date,
			@clnt_client_status			= client_status,
			@clnt_client_status_date	= client_status_date,
			@clnt_counselor				= counselor,
			@clnt_office				= office,
			@clnt_first_appt			= first_appt,
			@clnt_first_kept_appt		= first_kept_appt
	from	clients with (rowlock)
	where	client						= @cur_client

	if @@rowcount < 1
	begin
		rollback
		RaisError('Unable to find client (%d) from client_appointment', 16, 1, @cur_client)
		return ( 0 )
	end

	-- If there is no counselor in the record then try to obtain it from the system
	-- using the suser_sname function and the counselors table
	if @cur_counselor is null
	begin
		select	@cur_counselor	= counselor
		from	counselors
		where	[person]		= suser_sname()
	end

	-- Correct the client status based upon the appointment result
	if @client_status is not null
	begin
		set		@clnt_client_status			= @client_status
		set		@clnt_client_status_date	= getdate()

		if @clnt_client_status = 'DMP' and @clnt_active_status  = 'CRE'
		begin
			set	@clnt_active_status			= 'APT'
			set @clnt_active_status_date	= getdate()
		end
	end
	
	-- Correct the counselor and office fields if needed
	if @clnt_counselor is null			set @clnt_counselor       = @counselor
	if @clnt_office is null				set @clnt_office          = @office
	if @clnt_first_appt is null			set @clnt_first_appt      = @client_appointment
	if @clnt_first_kept_appt is null	set	@clnt_first_kept_appt = @client_appointment

	-- If the appointment is resulted then we can't do anything
	if @cur_status <> 'K'
	begin

		-- The start time must not be in the future. You can't result an appointment
		-- that has yet to occur unless it is a mistake. The problem is that the time will
		-- be returned to the available pool and may be booked again by a different client.
		if @cur_status = 'P' AND @cur_start_time > GETDATE()
		begin
			declare	@future_date	varchar(30)
			select	@future_date = CONVERT(varchar(30), @cur_start_time, 0)
			rollback
			RaisError ('The appointment is for ''%s'' and may not be completed until that time.', 16, 1, @future_date)
			return ( 0 )
		end
	end

	-- Correct the client status information
	update		clients
	set			active_status			= @clnt_active_status,
				client_status			= @clnt_client_status,
				client_status_date		= @clnt_client_status_date,
				first_appt				= @clnt_first_appt,
				first_kept_appt			= @clnt_first_kept_appt,
				counselor				= @clnt_counselor,
				office					= @clnt_office
	where		client					= @clnt_client
	
	-- Update the appointment information based upon our status
	set		@cur_appt_time		= null
	set		@cur_date_updated	= getdate()
	set		@cur_end_time		= dateadd(minute, @duration, @cur_start_time)

	if @counselor is not null		set @cur_counselor		= @counselor
	if @office is not null			set	@cur_office			= @office
	if @appt_type is not null		set @cur_appt_type		= @appt_type
	if @credit is not null			set @cur_credit			= @credit
	if @referred_to is not null		set @cur_referred_to	= @referred_to
	if @referred_by is not null		set @cur_referred_by	= @referred_by
	if @client_status is not null	set @cur_result			= @client_status
	if @end_time is not null		set @cur_end_time		= @end_time
	if @partner is not null         set @cur_partner		= @partner

	-- If the status is pending or missed then it is now "kept"
	if @cur_status in ('P','M')		set	@cur_status = 'K'
	
	-- Correct the client appointment record with all of the new data
	update	[client_appointments]
	set		[appt_time]				= @cur_appt_time,
			[counselor]				= @cur_counselor,
			[start_time]			= @cur_start_time,
			[end_time]				= @cur_end_time,
			[office]				= @cur_office,
			[workshop]				= @cur_workshop,
			[workshop_people]		= @cur_workshop_people,
			[appt_type]				= @cur_appt_type,
			[status]				= @cur_status,
			[result]				= @cur_result,
			[previous_appointment]	= @cur_previous_appointment,
			[referred_to]			= @cur_referred_to,
			[referred_by]			= @cur_referred_by,
			[partner]				= @cur_partner,
			[bankruptcy_class]		= @cur_bankruptcy_class,
			[priority]				= @cur_priority,
			[housing]				= @cur_housing,
			[post_purchase]			= @cur_post_purchase,
			[credit]				= @cur_credit,
			[callback_ph]			= @cur_callback_ph,
			[HousingFeeAmount]		= @cur_HousingFeeAmount,
			[confirmation_status]	= @cur_confirmation_status,
			[date_uploaded]			= @cur_date_uploaded,
			[date_confirmed]		= @cur_date_confirmed,
			[date_updated]			= @cur_date_updated
	where	[client_appointment]	= @client_appointment

	-- Do any additional work when the status is changed
	execute xpr_appt_custom_events @client_appointment

	-- Do special processing for FHLB appointments. We send email on these appointments.
	if @cur_appt_type = 155 and @cur_referred_by in (821, 996, 997)
		execute xpr_appt_result_fhlb @client_appointment = @client_appointment

	-- Commit the transactions and return the pointer to the client appointment record that was changed.
	commit transaction
	return ( @client_appointment )
end try

begin catch
	declare @ErrorMessage nvarchar(MAX)
	declare @ErrorSeverity int
	declare @ErrorState int

	select	@ErrorMessage  = ERROR_MESSAGE(),
			@ErrorSeverity = ERROR_SEVERITY(),
			@ErrorState    = ERROR_STATE()

	-- Abort the transaction if we started one
	if xact_state() = -1
		rollback transaction

	-- Generate the appropriate error condition and return failure to the caller.
	RaisError (@ErrorMessage, @ErrorSeverity, @ErrorState)
	return ( 0 )
end catch
GO
GRANT EXECUTE ON xpr_appt_result TO public AS dbo;
GO
DENY EXECUTE ON xpr_appt_result TO www_role;
GO
