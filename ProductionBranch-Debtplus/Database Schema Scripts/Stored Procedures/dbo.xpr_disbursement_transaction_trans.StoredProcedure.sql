USE [DebtPlus]
GO
/****** Object:  StoredProcedure [dbo].[xpr_disbursement_transaction_trans]    Script Date: 09/15/2014 13:13:35 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[xpr_disbursement_transaction_trans] ( @disbursement_register as int, @client_creditor as int, @debit_amt as money ) AS

-- ====================================================================================================
-- ==            Record the transaction amount on the disbursement table                             ==
-- ====================================================================================================

-- ChangeLog
--   9/10/2002
--     Accomodate the special case where creditors don't want to contribute on some debts but are paid by deduct status at mastercard.
--     For these, we use a special value of $0.01 fairshare amount.
--   12/10/2008
--     Remove the above kludge as it is no longer needed.
--   06/09/2009
--      Remove references to debt_number and use client_creditor

-- Suppress intermediate result sets
set nocount on

-- Fetch the information for the debt
declare	@fairshare_rate	float
declare	@fairshare_amt	money
declare	@creditor_type	varchar(1)
declare	@tran_type		varchar(2)

select	@fairshare_rate	= case
			when creditor_type = 'N' then 0
			when tran_type = 'BW' then fairshare_pct_eft
			else fairshare_pct_check
		end,
		@tran_type		= tran_type,
		@creditor_type	= creditor_type
from	disbursement_creditors
where	disbursement_register	= @disbursement_register
and	client_creditor		= @client_creditor

-- Calculate the fairshare amount.
if @fairshare_rate = 0.0
	select	@fairshare_amt	= 0
else
	select	@fairshare_amt	= convert(money, convert(decimal(8,2), @debit_amt * @fairshare_rate))

-- Update the payment information for the current debt
update	disbursement_creditors
set		fairshare_amt	= @fairshare_amt,
		debit_amt		= @debit_amt
where	disbursement_register	= @disbursement_register
and	client_creditor		= @client_creditor

return ( @@rowcount )
GO
