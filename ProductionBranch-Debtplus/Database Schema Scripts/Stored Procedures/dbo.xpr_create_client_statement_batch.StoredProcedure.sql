IF EXISTS(SELECT * FROM SYSOBJECTS WHERE TYPE='P' AND NAME=N'xpr_create_client_statement_batch')
	EXEC('DROP PROCEDURE xpr_create_client_statement_batch')
GO
CREATE PROCEDURE [dbo].[xpr_create_client_statement_batch] ( @disbursement_date as int = 0, @note as varchar(200) = null, @period_start as datetime = null, @period_end as datetime = null ) as
-- ===================================================================================================
-- ==     Create a batch for the client statements. This is normally done monthly during the        ==
-- ==     administrative job run.                                                                   ==
-- ===================================================================================================

-- Surpress intermediate results
set nocount on
BEGIN TRY

	-- If there is no period specified then make it for last month
	if @period_start is null and @period_end is null
	begin
		select	@period_start	= getdate()
		select	@period_end		= convert(varchar, month( @period_start )) + '/01/' + convert(varchar, year ( @period_start )) + ' 00:00:00'
		select	@period_start	= dateadd(m, -1, @period_end),
				@period_end		= dateadd(d, -1, @period_end)
	end

	-- If there is no ending date then use the end of the month
	if @period_end is null
	begin
		select	@period_end	= convert(varchar, month ( @period_start )) + '/01/' + convert(varchar, year ( @period_start ))
		select	@period_end	= dateadd(m, 1, @period_end )
		select	@period_end	= dateadd(d, -1, @period_end )
	end

	-- If there is no starting date then use the first of the month
	if @period_start is null
		select	@period_start	= convert(varchar, month ( @period_end )) + '/01/' + convert(varchar, year ( @period_end ))

	-- Correct the period dates
	select	@period_start	= convert(varchar(10), @period_start, 101),
			@period_end		= convert(varchar(10), @period_end, 101)

	-- Date Ranges for the selection
	declare	@select_from	datetime
	declare	@select_to		datetime
	declare	@today			datetime
	declare	@tomorrow		datetime

	set		@select_from	= @period_start
	set		@select_to		= dateadd(d, 1, @period_end)
	set		@today			= convert(varchar(10), getdate(), 101)
	set		@tomorrow		= dateadd(d, 1, @today)

	-- Create the disbursement batch
	declare	@client_statement_batch	int
	declare	@last_error		int
	declare	@row_count		int
	declare	@error_text		varchar(256)

	-- Summary (client) information
	create table #statement_clients (
		client_statement uniqueidentifier NOT NULL ROWGUIDCOL CONSTRAINT DF_temp_statement_clients_id DEFAULT (newid()),
		client int NULL,
		active_status varchar(10) NULL,
		held_in_trust money NULL,
		reserved_in_trust money NULL,
		counselor int NULL,
		office int NULL,
		disbursement_date int null,
		reserved_in_trust_cutoff datetime null,
		isMultiPage int NULL,
		isACH int NULL,
		isFinal int NULL,
		note_count int null,
		active_debts int null,
		transaction_count int null,
		deposit_amt money NULL,
		refund_amt money NULL,
		disbursement_amt money NULL,
		last_deposit_date datetime NULL,
		last_deposit_amt money NULL,
		expected_deposit_date varchar(50) NULL,
		expected_deposit_amt varchar(50) NULL,
	);

	create index temp_ix_statement_clients on #statement_clients ( client );

	-- Detail (debt) information
	create table #statement_details (
		client_statement_detail uniqueidentifier NOT NULL ROWGUIDCOL CONSTRAINT DF_temp_statement_details_id DEFAULT (newid()),
		client_statement uniqueidentifier NULL,
		client_creditor int NULL,
		client_creditor_balance int NULL,
		client int NULL,
		current_balance money NULL,
		debits money NULL,
		credits money NULL,
		interest money NULL,
		payments_to_date money NULL,
		original_balance money NULL,
		interest_rate float(53) NULL,
		last_payment int NULL,
		sched_payment money NULL,
		agency_account int null,
		reassigned_debt int null,
		zero_bal int null
	);

	-- Build a list of the deposits for the clients
	DECLARE @statement_disbursements TABLE (
		client				int NOT NULL PRIMARY KEY,
		disbursement_amt	money NULL
	);

	-- Build a list of the disbursements for the client
	insert into @statement_disbursements (client, disbursement_amt)
	select	client, sum(debit_amt) as disbursement_amt
	from	registers_client with (nolock)
	where	tran_type in ('AD', 'MD', 'CM')
	and		(date_created >= @select_from and date_created < @select_to)
	group by client;

	-- Build a list of the deposits for the clients
	DECLARE @statement_deposits TABLE (
		client				int NOT NULL PRIMARY KEY,
		deposit_amt			money NULL,
		last_deposit_date	datetime NULL,
		ending_date			datetime NULL,
		last_deposit_amt	money NULL
	);

	insert into @statement_deposits (client, deposit_amt, last_deposit_date, ending_date, last_deposit_amt)
	select	client, sum(credit_amt) as deposit_amt, max(date_created) as last_deposit_date, convert(datetime,null) as ending_date, convert(money,0) as last_deposit_amt
	from	registers_client with (nolock)
	where	tran_type in ('CD', 'DP', 'RF')
	and		(date_created >= @select_from and date_created < @select_to)
	group by client;

	update	@statement_deposits
	set	last_deposit_date	= convert(varchar(10), last_deposit_date, 101),
		ending_date			= convert(varchar(10), last_deposit_date, 101) + ' 23:59:59';

	DECLARE @last_deposit_amt TABLE (
		client				int NOT NULL PRIMARY KEY,
		last_deposit_amt	money NULL
	);

	insert into @last_deposit_amt (client, last_deposit_amt)
	select	d.client, sum(credit_amt) as last_deposit_amt
	from	registers_client rc with (nolock)
	inner join @statement_deposits d on rc.client = d.client
	where	rc.tran_type in ('CD','DP', 'RF')
	and		(rc.date_created >= @select_from and rc.date_created < @select_to)
	group by d.client;

	update	@statement_deposits
	set		last_deposit_amt	= a.last_deposit_amt
	from	@statement_deposits d
	inner join @last_deposit_amt a on a.client = d.client;

	declare @statement_refunds TABLE (
		client		int NOT NULL PRIMARY KEY,
		refund_amt	money NULL
	);

	-- Include the other credits to the client
	insert into @statement_refunds (client, refund_amt)
	select	client, sum(isnull(credit_amt,0) - isnull(debit_amt,0)) as refund_amt
	from	registers_client with (nolock)
	where	tran_type not in ('CD', 'DP', 'RF', 'LD', 'BB', 'CR', 'AD', 'MD', 'CM')
	and		(date_created >= @select_from and date_created < @select_to)
	group by client;

	-- Generate the list of clients for the statement
	insert into #statement_clients (client, active_status, held_in_trust, reserved_in_trust, counselor, office, disbursement_date, reserved_in_trust_cutoff,
									isMultiPage, isACH, note_count, active_debts, transaction_count, deposit_amt, refund_amt, disbursement_amt, isFinal)
	select client, active_status, held_in_trust, reserved_in_trust, counselor, office, disbursement_date, reserved_in_trust_cutoff,
		   0 as isMultiPage, 0 as isACH, 0 as note_count, 0 as active_debts, 0 as transaction_count, 0 as deposit_amt, 0 as refund_amt, 0 as disbursement_amt, 0 as isFinal
	from	clients c
	where	(c.active_status in ('A','AR')
	or		(c.active_status = 'I' AND c.drop_date >= @period_start and c.drop_date < dateadd(d, 1, @period_end))
	or		c.client in (
			select	client
			from	@statement_refunds
			union	all
			select	client
			from	@statement_deposits
			union	all
			select	client
			from	@statement_disbursements
			union	all
			select	client
			from	client_statement_notes
			where	client_statement is null
	)) AND c.client <> 0;

	-- Identify the final statement items
	update	#statement_clients
	set		isFinal = 1
	from	#statement_clients x
	inner join clients c on x.client = c.client
	where	c.active_status = 'I';	-- We don't need to test data ranges at this point because they would not in the table if outside date range.

	-- Identify the ACH clients
	update	#statement_clients
	set		isACH	= ach.isActive
	from	#statement_clients s
	inner join client_ach ach on s.client = ach.client
	where	ach.EndDate is null
	or		ach.EndDate > @today

	-- Update the client list with the temporary table values
	update	#statement_clients
	set		last_deposit_date	= b.last_deposit_date,
			last_deposit_amt	= b.last_deposit_amt,
			deposit_amt			= b.deposit_amt
	from	#statement_clients a
	inner join @statement_deposits b on a.client = b.client;

	update	#statement_clients
	set		refund_amt		= b.refund_amt
	from	#statement_clients a
	inner join @statement_refunds b on a.client = b.client;

	update	#statement_clients
	set		disbursement_amt		= b.disbursement_amt
	from	#statement_clients a
	inner join @statement_disbursements b on a.client = b.client;

	-- Remove clients from the list which are not suitable for the disbursement_cycle
	if isnull(@disbursement_date,0) > 0
	begin
		delete
		from	#statement_clients
		where	disbursement_date	<> @disbursement_date

		-- If there are no clients then we have finished. There are no clients due to the disbursement date deleting them.
		if not exists ( select top 1 * from #statement_clients )
			return ( 0 )
	end

	-- Find the client expected deposit dates
	declare @expected_amts TABLE (
		client		int NOT NULL PRIMARY KEY,
		deposit_amt	money NULL
	);

	insert into @expected_amts (client, deposit_amt)
	select	client, sum(deposit_amount) as deposit_amt
	from	client_deposits with (nolock)
	where	one_time	 = 0
	group by client;

	update	#statement_clients
	set		expected_deposit_date	= dbo.client_deposit_dates ( c.client, @period_start, getdate() ),
			expected_deposit_amt	= convert(varchar, b.deposit_amt, 0)
	from	#statement_clients c
	inner join @expected_amts  b on c.client = b.client;

	declare @registers_client TABLE (
		client		int NOT NULL PRIMARY KEY,
		[count]		int
	);

	-- Count the number of client transactions for each client
	insert into @registers_client (client, [count])
	select	rc.client, count(*) as [count]
	from	registers_client rc
	inner join #statement_clients x on x.client = rc.client
	where	(rc.date_created > @select_from and rc.date_created < @select_to)
	group by rc.client

	update	#statement_clients
	set		transaction_count = b.[count]
	from	#statement_clients a
	inner join @registers_client b on a.client = b.client

	-- Reset the deposit information for those clients who are inactive
	update	#statement_clients
	set		expected_deposit_amt	= '0.00',
			expected_deposit_date	= ''
	where	active_status			= 'I';

	-- Build list list of debts
	insert into #statement_details (client_statement, client_creditor, client_creditor_balance, client, current_balance, debits, credits, interest, payments_to_date, original_balance, interest_rate, last_payment, sched_payment, agency_account, reassigned_debt, zero_bal)
	select	c.client_statement, cc.client_creditor, cc.client_creditor_balance, cc.client,
			0 as current_balance,
			sum(isnull(rcc.debit_amt,0)) as debits, sum(isnull(rcc.credit_amt,0)) as credits, sum(case when rcc.tran_type = 'IN' then isnull(rcc.credit_amt,0) else 0 end) as interest, 
			0 as payments_to_date,
			0 as original_balance,
			cc.dmp_interest as interest_rate,
			cc.last_payment,
			0 as sched_payment,
			isnull(ccl.agency_account,1) as agency_account,
			cc.reassigned_debt as reassigned_debt,
			isnull(ccl.zero_balance,0) as zero_bal

	from	client_creditor cc with (nolock)
	inner join #statement_clients c on cc.client = c.client
	left outer join creditors cr with (nolock) on cc.creditor = cr.creditor
	left outer join creditor_classes ccl with (nolock) on cr.creditor_class = ccl.creditor_class
	left outer join registers_client_creditor rcc with (nolock) on cc.client_creditor = rcc.client_creditor and (rcc.date_created >= @period_start and rcc.date_created < @select_to)
	group by cc.client, cc.client_creditor, cc.client_creditor_balance, cc.reassigned_debt, cc.last_payment, ccl.agency_account, c.client_statement, cc.dmp_interest, ccl.zero_balance;

	-- Do not count interest in the returns even though the amount is in the credits column.
	update	#statement_details
	set		credits = credits - interest

	-- Set the debts that have been paid but are no longer "current"
	update	#statement_details
	set		reassigned_debt	= 1
	from	#statement_details d
	inner join client_creditor_balances b on b.client_creditor_balance = d.client_creditor_balance
	where	d.client_creditor <> b.client_creditor

	-- Update the balances accordingly
	update	#statement_details
	set		current_balance	 = isnull(b.orig_balance,0) + isnull(b.orig_balance_adjustment,0) + isnull(b.total_interest,0) - isnull(b.total_payments,0),
			sched_payment	 = b.current_sched_payment,
			original_balance = b.orig_balance,
			payments_to_date = b.total_payments
	from	#statement_details d
	inner join client_creditor_balances b on d.client_creditor_balance = b.client_creditor_balance

	-- Set the interest rates
	update	#statement_details
	set		interest_rate	= cr.lowest_apr_pct
	from	#statement_details d
	inner join client_creditor cc on d.client_creditor = cc.client_creditor
	inner join client_creditor_balances b on cc.client_creditor_balance = b.client_creditor_balance
	inner join creditors cr on cc.creditor = cr.creditor

	update	#statement_details
	set		interest_rate	 = cc.dmp_interest
	from	#statement_details d
	inner join client_creditor cc on d.client_creditor = cc.client_creditor
	where	cc.dmp_interest is not null;

	-- Mark the zero balances
	update	#statement_details
	set		current_balance	 = 0,
			interest		 = 0,
			interest_rate	 = 0,
			original_balance = 0
	from	#statement_details d
	where	zero_bal		 = 1

	-- Reassigned debts have no balance
	update	#statement_details
	set		current_balance	 = 0,
			original_balance = 0,
			payments_to_date = 0
	where	reassigned_debt	 = 1

	-- Discard all of the "junk" items
	delete
	from	#statement_details
	where	debits			= 0
	and		credits			= 0
	and		interest		= 0
	and		reassigned_debt	= 1

	-- Mark the clients who have debts
	update	#statement_clients
	set		active_debts	= (select count(*) from #statement_details d where (c.client = d.client) and (d.debits > 0 or d.credits > 0 or d.interest > 0 or d.current_balance > 0))
	from	#statement_clients c

	-- Mark the clients who have non-agency debts
	update	#statement_clients
	set		active_debts = (select count(*) from #statement_details d where c.client = d.client and d.agency_account = 0)
	from	#statement_clients c

	-- Mark the clients who have notes
	update	#statement_clients
	set		note_count	= (select count(*) from client_statement_notes n where n.client = n.client and n.client_statement is null)
	from	#statement_clients c

	-- Summarize the disbursement amounts
	update	#statement_clients
	set		disbursement_amt = (select sum(debits) from #statement_details d where c.client = d.client)
	from	#statement_clients c

	-- Discard clients who have no debts. We don't need blank statements.
	delete
	from	#statement_clients
	where	note_count	= 0
	and		active_debts	= 0;

	-- Discard the debts that we deleted as well when we tossed the clients
	delete	#statement_details
	from	#statement_details d
	left outer join #statement_clients c on d.client = c.client
	where	c.client is null;

	-- Finally determine if the client may possibly take two pages for the statement
	-- This is just an emperical number. It works out this way based upon the current statement layout.
	declare	@detailCount TABLE (
		client_statement uniqueidentifier NOT NULL PRIMARY KEY,
		detail_count int
	);

	insert into @detailCount (client_statement, detail_count)
	select	client_statement, count(*) as detail_count
	from	#statement_details
	group by client_statement;

	update	#statement_clients
	set		active_debts = b.detail_count
	from	#statement_clients a
	inner join @detailCount b on a.client_statement = b.client_statement;

	update	#statement_clients
	set		isMultiPage = 1
	where	transaction_count + active_debts > 23

	-- If there are no clients then we don't need to do anything. The batch would be empty.
	if exists (select top 1 * from #statement_clients)
	begin
		SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED
		BEGIN TRANSACTION

		-- Create the statement batch ID
		insert into client_statement_batches ( disbursement_date, note, statement_date, period_start, period_end )
		select	isnull(@disbursement_date,0), @note, convert(datetime, convert(varchar(10), getdate(), 101)), @period_start, convert(datetime, convert(varchar(10), @period_end, 101))
		select	@client_statement_batch	= scope_identity()

		-- Load the client items into the tables
		insert into client_statement_clients (client_statement, client_statement_batch, client, isMultiPage, isACH, isFinal, active_status, active_debts, last_deposit_date, last_deposit_amt, deposit_amt, refund_amt, disbursement_amt, held_in_trust, reserved_in_trust, counselor, office, expected_deposit_date, expected_deposit_amt)
		select client_statement, @client_statement_batch as client_statement_batch, client, isnull(isMultiPage,0), isnull(isACH,0), isnull(isFinal,0), isnull(active_status,''), isnull(active_debts,0), isnull(last_deposit_date,''), isnull(last_deposit_amt,0), isnull(deposit_amt,0), isnull(refund_amt,0), isnull(disbursement_amt,0), isnull(held_in_trust,0), isnull(reserved_in_trust,0), counselor, office, expected_deposit_date, expected_deposit_amt
		from #statement_clients

		-- Load the debt items into the tables
		insert into client_statement_details (client_statement_detail, client_statement, client_creditor, current_balance, debits, credits, interest, payments_to_date, original_balance, interest_rate, last_payment, sched_payment)
		select client_statement_detail, client_statement, isnull(client_creditor,0), isnull(current_balance,0), isnull(debits,0), isnull(credits,0), isnull(interest,0), isnull(payments_to_date,0), isnull(original_balance,0), isnull(interest_rate,0), isnull(last_payment,0), isnull(sched_payment,0)
		from #statement_details

		-- Bind the notes to the current batch
		update		client_statement_notes
		set			client_statement = c.client_statement
		from		client_statement_notes n
		inner join #statement_clients c on n.client = c.client

		COMMIT TRANSACTION
	end

	-- Discard the working tables
	drop table #statement_details
	drop table #statement_clients
	return ( 0 )
END TRY

BEGIN CATCH
	DECLARE @ErrorMessage NVARCHAR(4000);
	DECLARE @ErrorSeverity INT;
	DECLARE @ErrorState INT;

	SELECT	@ErrorMessage	= ERROR_MESSAGE(),
			@ErrorSeverity	= ERROR_SEVERITY(),
			@ErrorState		= ERROR_STATE();

	if @@trancount > 0
	BEGIN
		BEGIN TRY
			ROLLBACK TRANSACTION
		END TRY
		BEGIN CATCH
		END CATCH
	END

	RAISERROR (@ErrorMessage, @ErrorSeverity, @ErrorState);
	return ( 1 )
END CATCH
GO
GRANT EXECUTE ON xpr_create_client_statement_batch TO public AS dbo;
DENY EXECUTE ON xpr_create_client_statement_batch TO www_role;
GO
