USE [DebtPlus]
GO
/****** Object:  StoredProcedure [dbo].[www_client_zipcode]    Script Date: 09/15/2014 13:13:34 ******/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE PROCEDURE [dbo].[www_client_zipcode] ( @client as typ_client ) AS
select	zipcode
from	view_client_address with (nolock)
where	client = @client
return ( @@rowcount )
GO
