USE [DebtPlus]
GO
/****** Object:  StoredProcedure [dbo].[rpt_rpps_transactions]    Script Date: 09/15/2014 13:13:33 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[rpt_rpps_transactions] ( @disbursement_register as int = NULL ) as
-- ========================================================================================
-- ==             Report for the transactions of a bank-wire transaction                 ==
-- ========================================================================================

-- ChangeLog
--   3/14/2003
--     Allow for more than 10,000 creditors in a class
--   06/09/2009
--      Remove references to debt_number and use client_creditor

set nocount on

-- Determine the disbursement register if one is not specified
if @disbursement_register is null
	select	@disbursement_register = max(disbursement_register)
	from	registers_disbursement
	where	date_created = (
		select	max(date_created)
		from	registers_disbursement
	)

-- Fetch the transactions
select	@disbursement_register		as 'disbursement_register',
	rcc.creditor			as 'creditor',
	cr.creditor_name		as 'creditor_name',
	rcc.client_creditor		as 'client_creditor',
	isnull(rcc.account_number,cc.account_number) as 'account_number',
	rcc.client			as 'client',
	dbo.format_normal_name(pn.prefix,pn.first,pn.middle,pn.last,pn.suffix) as 'client_name',
	rcc.debit_amt			as 'gross_amt',
	case when rcc.creditor_type = 'D' then isnull(rcc.fairshare_amt,0.0) else 0.0 end as 'deducted',
	case when rcc.creditor_type = 'B' then isnull(rcc.fairshare_amt,0.0) else 0.0 end as 'billed',
	case when rcc.invoice_register is not null and rcc.creditor_type = 'B' and rcc.void = 0 then rcc.invoice_register else null end as 'invoice_register'

from	registers_client_creditor rcc with (nolock)
left outer join creditors cr with (nolock)		on rcc.creditor = cr.creditor
left outer join people p with (nolock)			on rcc.client = p.client
left outer join names pn with (nolock) on p.NameID = pn.Name
left outer join client_creditor cc with (nolock)	on rcc.client_creditor = cc.client_creditor

where	rcc.disbursement_register = @disbursement_register
and	rcc.tran_type = 'BW'

order by cr.type, cr.creditor_id, rcc.creditor, rcc.client

return ( @@rowcount )
GO
