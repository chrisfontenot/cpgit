USE [DebtPlus]
GO
/****** Object:  StoredProcedure [dbo].[rpt_appt_weekly_schedule]    Script Date: 09/15/2014 13:13:32 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[rpt_appt_weekly_schedule] ( @fromdate as datetime = null, @todate as datetime = null, @office as int = null ) as

-- ===============================================================================================
-- ==            List the weekly schedule of appointments                                       ==
-- ===============================================================================================

-- ChangeLog
--   12/18/2002
--     Added support for "inactive" flag in appt_counselors
--   3/27/2003
--     Added inactive_reason to the result set

set nocount on

if @ToDate is null
	select @ToDate = getdate()

if @FromDate is null
	select @FromDate = @ToDate

select	@FromDate = convert(datetime, convert(varchar(10), @fromdate, 101) + ' 00:00:00'),
	@ToDate = convert(datetime, convert(varchar(10), @todate, 101) + ' 23:59:59')

if @Office is not null
	select		apt.appt_time			as 'appt_time',
			convert(varchar(10), apt.start_time, 101) as 'appt_date',
			apt.start_time			as 'start_time',
			apt.office			as 'office',
			isnull(typ.appt_name,'ANY')	as 'appt_name',
			o.name				as 'office_name',
			dbo.format_normal_name(default,cox.first,default,cox.last,default) as 'counselor_name',
			case
				when apco.inactive = 0 then null
				else isnull(apco.inactive_reason,'NOT SPECIFIED')
			end				as 'inactive_reason',
			isnull(apco.inactive,0)		as 'inactive'

	from		appt_times apt	with (nolock)
	left outer join	offices o	with (nolock) on apt.office = o.office
	inner join appt_counselors apco	with (nolock) on apt.appt_time = apco.appt_time
	left outer join	counselors co	with (nolock) on apco.counselor = co.counselor
        left outer join names cox with (nolock) on co.NameID = cox.name
	left outer join appt_types typ	with (nolock) on apt.appt_type = typ.appt_type

	where	apt.start_time between @FromDate and @ToDate
	and	apt.office = @Office
	order by 4, 3, 7

else

	select		apt.appt_time			as 'appt_time',
			convert(varchar(10), apt.start_time, 101) as 'appt_date',
			apt.start_time			as 'start_time',
			apt.office			as 'office',
			isnull(typ.appt_name,'ANY')	as 'appt_name',
			o.name				as 'office_name',
			dbo.format_normal_name(default,cox.first,default,cox.last,default) as 'counselor_name',
			apco.inactive_reason		as 'inactive_reason',
			isnull(apco.inactive,0)		as 'inactive'

	from		appt_times apt	with (nolock)
	left outer join	offices o	with (nolock) on apt.office = o.office
	inner join appt_counselors apco	with (nolock) on apt.appt_time = apco.appt_time
	left outer join	counselors co	with (nolock) on apco.counselor = co.counselor
        left outer join names cox with (nolock) on co.NameID = cox.name
	left outer join appt_types typ	with (nolock) on apt.appt_type = typ.appt_type

	where	apt.start_time between @FromDate and @ToDate
	order by 4, 3, 7

return ( @@rowcount )
GO
