USE [DebtPlus]
GO
/****** Object:  StoredProcedure [dbo].[xpr_insert_rpps_aka]    Script Date: 09/15/2014 13:13:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[xpr_insert_rpps_aka] (@rpps_biller_id varchar(20), @name varchar(50)) as
INSERT INTO rpps_akas (rpps_biller_id, name) VALUES (@rpps_biller_id, @name)
return (scope_identity())
GO
