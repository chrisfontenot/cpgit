USE [DebtPlus]
GO
/****** Object:  StoredProcedure [dbo].[rpt_client_active_status_byType]    Script Date: 09/15/2014 13:13:32 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE PROCEDURE [dbo].[rpt_client_active_status_byType] ( @Type AS VARCHAR(3) = 'AAR' ) AS

-- ======================================================================================================
-- ==           Return the client list for the indicated type                                          ==
-- ======================================================================================================

IF upper(@Type) = 'AAR'

	SELECT		c.client								as 'client',
			dbo.format_reverse_name(default,pn.first,pn.middle,pn.last,default)	as 'name',
			c.active_status								as 'active_status',
 			c.start_date								as 'start_date'
	FROM		clients c
	LEFT OUTER JOIN	people p ON c.client=p.client AND 1=p.relation
	left outer join names pn with (nolock) on p.nameid = pn.name
	WHERE		c.active_status IN ('A', 'AR')
	ORDER BY	2, 1

ELSE

	SELECT		c.client								as 'client',
			dbo.format_reverse_name(default,pn.first,pn.middle,pn.last,default)	as 'name',
			c.active_status								as 'active_status',
 			c.start_date								as 'start_date'
	FROM		clients c
	LEFT OUTER JOIN	people p ON c.client=p.client AND 1=p.relation
	left outer join names pn with (nolock) on p.nameid = pn.name
	WHERE		c.active_status = @Type
	ORDER BY	2, 1

RETURN ( @@rowcount )
GO
