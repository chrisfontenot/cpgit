USE [DebtPlus]
GO
/****** Object:  StoredProcedure [dbo].[xpr_insert_workshop_guest]    Script Date: 09/15/2014 13:13:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[xpr_insert_workshop_guest] ( @Name as varchar(50) = null, @Telephone as varchar(50) = null, @Email as varchar(50) = null ) as
-- ========================================================================================
-- ==   Procedure for .NET to insert items into the workshop_guests table                ==
-- ========================================================================================
insert into workshop_guests ( [Name], [Telephone], [Email] )
values ( @Name, @Telephone, @Email )
return ( scope_identity() )
GO
