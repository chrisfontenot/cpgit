USE [DebtPlus]
GO
/****** Object:  StoredProcedure [dbo].[rpt_PolicyMatrix_Creditor]    Script Date: 09/15/2014 13:13:33 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[rpt_PolicyMatrix_Creditor]( @creditor as varchar(10) ) as

-- ==============================================================================================================
-- ==            Retrieve the creditor information for the creditor                                            ==
-- ==============================================================================================================

set nocount on

-- Retrieve the indicated policies
select	creditor,
	creditor_name,
	division,
	sic,
	comment
from	creditors with (nolock)
where	creditor = @creditor

return ( @@rowcount )
GO
