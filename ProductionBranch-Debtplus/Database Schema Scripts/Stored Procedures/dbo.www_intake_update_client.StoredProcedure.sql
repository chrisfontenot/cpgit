USE [DebtPlus]
GO
/****** Object:  StoredProcedure [dbo].[www_intake_update_client]    Script Date: 09/15/2014 13:13:34 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[www_intake_update_client] ( @intake_client int, @housing_status as int, @housing_type as int, @referred_by as int, @cause_fin_problem1 as int, @marital_status as int, @people as int) AS

-- ============================================================================================================
-- ==            Create the intake session for the client                                                    ==
-- ============================================================================================================

-- Initialize to process the request
set nocount on

-- Default the values as needed
if @housing_status < 0
	select @housing_status = null

if @housing_type < 0
	select @housing_type = null

if @referred_by < 0
	select @referred_by = null

if @cause_fin_problem1 < 0
	select @cause_fin_problem1 = null

if @marital_status < 0
	select @marital_status = null

if @people < 0
	select @people = null

-- Do not permit strange values in the database
if @people is null
	select @people = 1

if @marital_status is null
	select @marital_status = 1

if @housing_status is null
	select @housing_status = 4

if @housing_type is null
	select @housing_type = 10

-- Correct the information on the previously existing client
update	intake_clients
set		housing_status		= isnull(@housing_status,4),
		housing_type		= isnull(@housing_type,10),
		referred_by			= @referred_by,
		cause_fin_problem1	= @cause_fin_problem1,
		marital_status		= isnull(@marital_status,1),
		people				= isnull(@people,1)
where	intake_client		= @intake_client

return ( @@rowcount )
GO
