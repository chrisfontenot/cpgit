USE [DebtPlus]
GO
/****** Object:  StoredProcedure [dbo].[www_client_cc_trans_header]    Script Date: 09/15/2014 13:13:34 ******/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE PROCEDURE [dbo].[www_client_cc_trans_header] ( @client as int, @client_register as int ) AS
-- =======================================================================================
-- ==            Determine the client/creditor transaction header                       ==
-- =======================================================================================

-- Disable intermediate results
set nocount on

-- Return the date for the transaction. Format it for the proper style
select	isnull(convert(varchar(10), date_created, 1),'')	as 'date_created'
from	registers_client
where	client_register = @client_register
and	client = @client
and	tran_type in ('AD', 'MD', 'CM')

return ( @@rowcount )
GO
