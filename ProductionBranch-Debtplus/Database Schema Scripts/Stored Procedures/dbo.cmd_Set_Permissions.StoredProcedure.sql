USE [DebtPlus]
GO
if exists(select * from sysobjects where type='P' and name=N'cmd_Set_Permissions')
    exec ('DROP PROCEDURE cmd_Set_Permissions')
GO
CREATE PROCEDURE [dbo].[cmd_Set_Permissions] AS

-- ==============================================================================
-- ==            set the default permissions to permit all access types        ==
-- ==============================================================================

declare	@name		varchar(128)
declare	@stmt		varchar(256)

-- Should we deny access to the www-role user
declare @enable_www_role int
set @enable_www_role = 0
-- if exists (select * from sysusers where name = 'www_role' and issqlrole = 1)
--	set @enable_www_role = 1

-- Grant permissions on views to the group "public"
declare	view_cursor cursor for
	select name from sysobjects
	where	type = 'V'
	and	category = 0

open view_cursor
fetch view_cursor into @name

-- Grant the permissions
while @@fetch_status = 0
begin
	select @stmt = 'grant select on [' + @name + '] to public as dbo;'
	print @stmt
	exec ( @stmt )

	-- If the view name is "update_" then permit updates
	if @name like 'update_%'
	begin
		select @stmt = 'grant insert,update,delete on [' + @name + '] to public as dbo;'
		print @stmt
		exec ( @stmt )
	end

	-- Revoke selection critiera to the www user
	if @enable_www_role = 1
	begin
		select @stmt = 'grant select on [' + @name + '] to www_role;'
		print @stmt
		exec ( @stmt )
		select @stmt = 'deny update,insert,delete on [' + @name + '] to www_role;'
		print @stmt
		exec ( @stmt )
	end

	-- Revoke selection critiera to the www user
	if @enable_www_role = 1
	begin
		select @stmt = 'deny select on [' + @name + '] to www_role;'
		print @stmt
		exec ( @stmt )
	end

	fetch view_cursor into @name
end

close view_cursor
deallocate view_cursor

-- Ensure that some of the views have the proper permission
exec ('grant update on view_rpps_cdp_info to public as dbo;')

-- Revoke selection critiera to the www user
if @enable_www_role = 1
begin
	select @stmt = 'deny update on [view_rpps_cdp_info] to www_role;'
	print @stmt
	exec ( @stmt )
end

exec ('grant update on view_rpps_cie_info to public;')

-- Revoke selection critiera to the www user
if @enable_www_role = 1
begin
	select @stmt = 'deny update on [view_rpps_cie_info] to www_role;'
	print @stmt
	exec ( @stmt )
end

exec ('grant update on view_ACH_Pending_Transactions to public;')

-- Revoke selection critiera to the www user
if @enable_www_role = 1
begin
	select @stmt = 'deny update on [view_ACH_Pending_Transactions] to www_role;'
	print @stmt
	exec ( @stmt )
end

-- Grant permissions on tables to the group "public"
declare	table_cursor cursor for
	select name from sysobjects
	where	type = 'U'

open table_cursor
fetch table_cursor into @name

-- Grant the permissions
while @@fetch_status = 0
begin
	if upper(left(@name + '      ', 6)) != 'INPUT_'
	begin
		select @stmt = 'grant select,update,delete,insert on [' + @name + '] to public as dbo;'
		print @stmt
		exec ( @stmt )

		-- Revoke selection critiera to the www user
		if @enable_www_role = 1
		begin
			select @stmt = 'grant select on [' + @name + '] to www_role;'
			print @stmt
			exec ( @stmt )

			select @stmt = 'deny update,delete,insert on [' + @name + '] to www_role;'
			print @stmt
			exec ( @stmt )

			if @name not in ('client_statement_batches', 'client_statement_clients', 'client_statement_details', 'client_statement_notes')
			begin
				select @stmt = 'deny select on [' + @name + '] to www_role;'
				print @stmt
				exec ( @stmt )
			end
		end
	end

	fetch table_cursor into @name
end

close table_cursor
deallocate table_cursor

-- Grant permissions on stored procedures to the group "public"
declare	procedure_cursor cursor for
	select name from sysobjects
	where xtype = 'P'
	and category = 0

open procedure_cursor
fetch procedure_cursor into @name

-- Grant the permissions
while @@fetch_status = 0
begin
	if (left(@name + '    ', 4) in ('lst_', 'nccr', 'nfcc', 'nfmc', 'rpt_', 'www_', 'xpr_', 'ocs_')) or
		(@name = 'creditor_name_and_addresses') or
		(@name like 'www_%_admin') or
		(@name like 'www_%_message%')
	begin
		select @stmt = 'grant execute on [' + @name + '] to public as dbo;'
		print @stmt
		exec ( @stmt )
	end

	-- Revoke selection critiera to the www user
	if @enable_www_role = 1
	begin
		select @stmt = 'deny execute on [' + @name + '] to www_role;'
		print @stmt
		exec ( @stmt )
	end

	fetch procedure_cursor into @name
end

close procedure_cursor
deallocate procedure_cursor

-- Ensure that the special procedure used in a report is granted access
exec ('grant execute on cmd_random_list_of_clients to public;')

-- Grant permissions on stored procedures to the group "www_role"
if @enable_www_role = 1
begin
	declare	www_cursor cursor for
		select name from sysobjects
		where xtype = 'P'
		and category = 0
		and ((name in ('www_client_message_list', 'www_creditor_message_list', 'xpr_OrganizationAddress'))
		or  (	(name like 'www_%')
			and (name not like 'www_client_message%')
			and (name not like 'www_creditor_message%')
			and (name not like '%_admin')
		))

	open www_cursor
	fetch www_cursor into @name

	-- Grant the permissions
	while @@fetch_status = 0
	begin
		select @stmt = 'grant execute on [' + @name + '] to www_role;'
		print @stmt
		exec ( @stmt )

		fetch www_cursor into @name
	end

	close www_cursor
	deallocate www_cursor
end

-- Grant permissions on functions to the group "public"
declare	procedure_cursor cursor for
	select name from sysobjects
	where xtype = 'FN'
	and category = 0

open procedure_cursor
fetch procedure_cursor into @name

-- Grant the permissions
while @@fetch_status = 0
begin
	select @stmt = 'grant execute on [' + @name + '] to public;'
	print @stmt
	exec ( @stmt )

	fetch procedure_cursor into @name
end

close procedure_cursor
deallocate procedure_cursor

-- Grant permissions to view the server state to the users
-- It is a simple permission and does not really mean much but it is needed for the disbursement posting procedure to work properly
declare @cmd varchar(8000)
select @cmd = isnull(@cmd + char(13) + char(10), '') + 'grant VIEW SERVER STATE TO [' + name + '];' from master.dbo.syslogins where name not like '#%' and name <> 'sa' and name not like 'RIC-DP-%'
select @cmd = 'use master;' + char(13) + char(10) + @cmd 
print @cmd
exec ( @cmd )

-- Ensure that the www_role has access to the client statement tables that it needs
-- These are needed for the client statements to be generated on the web server
if @enable_www_role = 1
begin
	exec ('deny insert,update,delete on client_statement_batches to www_role')
	exec ('deny insert,update,delete on client_statement_clients to www_role')
	exec ('deny insert,update,delete on client_statement_details to www_role')
	exec ('deny insert,update,delete on client_statement_notes to www_role')
--
	exec ('grant select on client_statement_batches to www_role')
	exec ('grant select on client_statement_clients to www_role')
	exec ('grant select on client_statement_details to www_role')
	exec ('grant select on client_statement_notes to www_role')
end
GO
grant execute on cmd_Set_Permissions to public as dbo;
GO
