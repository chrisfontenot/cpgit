USE [DebtPlus]
GO
/****** Object:  StoredProcedure [dbo].[rpt_PrintClient]    Script Date: 09/15/2014 13:13:33 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE PROCEDURE [dbo].[rpt_PrintClient] ( @Client AS INT ) AS

-- ======================================================================================================
-- ==             Retrieve the list of clients to be printed in the client packet                      ==
-- ======================================================================================================

-- Fetch the organization name from the config table
SET NOCOUNT ON

DECLARE @OrganizationName	VarChar(255)
SELECT	@OrganizationName = name
FROM	config with(nolock)

-- Fetch the information for the report
SELECT	cl.client as 'client',
	dbo.format_normal_name(pn.prefix,pn.first,pn.middle,pn.last,pn.suffix) as 'client_name',
	@OrganizationName as 'org_name',
	convert(money,null)     as 'expected_deposit_amt',
	convert(datetime, null) as 'expected_deposit_date',

	convert(money,null) as 'additional_money_1',
	convert(money,null) as 'additional_money_2',
	convert(money,null) as 'additional_money_3',
	convert(money,null) as 'additional_money_4',
	convert(money,null) as 'additional_money_5',
	convert(money,null) as 'additional_money_6',

	convert(money,null) as 'additional_date_1',
	convert(money,null) as 'additional_date_2',
	convert(money,null) as 'additional_date_3',
	convert(money,null) as 'additional_date_4',
	convert(money,null) as 'additional_date_5',
	convert(money,null) as 'additional_date_6',
	a.state as state_num
	
FROM	people p with(nolock)
left outer join names pn with (nolock) on p.nameid = pn.name
left outer join clients cl with (nolock) on p.client = cl.client
left outer join addresses a with (nolock) on cl.addressid = a.address
WHERE	p.client = @client AND 1 = relation

RETURN ( @@rowcount )
GO
