USE [DebtPlus]
GO
/****** Object:  StoredProcedure [dbo].[xpr_insert_housing_lender_servicer]    Script Date: 09/15/2014 13:13:35 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[xpr_insert_housing_lender_servicer] ( @description as varchar(50), @ActiveFlag as bit = 1, @Default as bit = 0, @ServiceID as int = null, @Comment as varchar(256) = null ) as
-- ==========================================================================================
-- ==            Add a new lending servicer to the list of valid lenders                   ==
-- ==========================================================================================
	insert into housing_lender_servicers ( [description], [ActiveFlag], [Default], [ServiceID], [Comment] ) values ( @description, @ActiveFlag, @Default, @ServiceID, @Comment )
	return ( scope_identity() )
GO
