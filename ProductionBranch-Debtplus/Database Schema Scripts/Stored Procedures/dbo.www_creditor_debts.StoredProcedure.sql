USE [DebtPlus]
GO
/****** Object:  StoredProcedure [dbo].[www_creditor_debts]    Script Date: 09/15/2014 13:13:34 ******/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE PROCEDURE [dbo].[www_creditor_debts] ( @creditor as typ_creditor, @client_creditor AS INT ) AS
-- ===========================================================================================
-- ==                    Fetch the client_creditor from the proposal table                  ==
-- ===========================================================================================

-- ChangeLog
--   9/31/2002
--     Added support for client_creditor_balances table
--   01/15/2002
--     Added creditor_classes table references and dropped "creditors.fee_creditor"

-- Suppress intermediate result sets
set nocount on
declare	@client			int

-- Fetch the client information
select	@client		= cc.client
from	client_creditor cc with (nolock)
where	client_creditor	= @client_creditor
and	creditor	= @creditor

-- The client should not be null. It is null if the creditor does not own the debt record.
if @client is null
begin
	RaisError ('The client information is not valid for this record.', 16, 1)
	return ( 0 )
end

-- Fetch the list of debts for the client.
select		case
			when cc.creditor = @creditor then isnull(cr.creditor_name, cc.creditor_name)
			ELSE ISNULL(t.description, 'OTHER CREDITOR (TYPE ' + ISNULL(SUBSTRING(cc.creditor, 1, 1),'?') + ')')
		end						as 'creditor_name',

		case isnull(ccl.zero_balance,0)
			WHEN 0 then isnull(bal.orig_balance,0) + isnull(bal.orig_balance_adjustment,0) + isnull(bal.total_interest,0) - isnull(bal.total_payments,0)
			ELSE 0
		end						as 'balance',

		isnull(cc.disbursement_factor,0)		as 'disbursement_factor'
FROM		client_creditor cc with (nolock)
inner join	client_creditor_balances bal with (nolock) on cc.client_creditor_balance = bal.client_creditor_balance
LEFT OUTER JOIN	creditors cr with (nolock) on cc.creditor = cr.creditor
LEFT OUTER JOIN	creditor_types t with (nolock) on SUBSTRING(cc.creditor, 1, 1) = t.type
LEFT OUTER JOIN creditor_classes ccl WITH (NOLOCK) ON cr.creditor_class = ccl.creditor_class
WHERE		client = @client
AND		cc.reassigned_debt = 0
AND		((bal.orig_balance + bal.orig_balance_adjustment + bal.total_interest > bal.total_payments) OR (ccl.zero_balance <> 0) OR (cc.client_creditor = @Client_Creditor))
ORDER BY	2 DESC, 3 DESC

-- Return the number of debt records selected
RETURN ( @@rowcount )
GO
