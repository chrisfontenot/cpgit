USE [DebtPlus]
GO
/****** Object:  StoredProcedure [dbo].[rpt_balance]    Script Date: 09/15/2014 13:13:32 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[rpt_balance] ( @fromDate as datetime = null, @todate as datetime = null ) as

-- =========================================================================================
-- ==            Provide information to help balance the trust account                    ==
-- =========================================================================================

-- ChangeLog
--   6/28/2002
--      Changed reference to view_disbursement_clients from disbursement_clients

-- Local variables
declare	@total_cm	money
declare	@total_rr	money
declare	@total_rf	money
declare	@total_bw	money
declare	@total_ad	money
declare	@total_vd	money
declare	@total_cd	money
declare	@total_dp	money
declare	@total_dm	money
declare	@total_mf	money
declare	@total_vr	money
declare	@total_cr	money
declare	@total_md	money
declare @total_rs	money

-- Suppress intermediate results
set nocount on

-- Supply the proper defaults
IF @ToDate IS NULL
	SELECT @ToDate = getdate()

IF @FromDate IS NULL
	SELECT @FromDate = @ToDate

-- Remove the time information from the dates
select	@FromDate = convert(datetime, convert(varchar(10), @FromDate, 101) + ' 00:00:00')
select	@ToDate   = convert(datetime, convert(varchar(10), @ToDate,   101) + ' 23:59:59')

-- Create the table to hold the results
create table #t_balance_routine (tran_type	varchar(2),
				fairshare_amt	money null,
				credit_amt	money null,
				debit_amt	money null);

-- Retrieve the information from the registers_client_creditor table for the various amounts
select	tran_type							as 'tran_type',
		creditor_type						as 'creditor_type',
		SUM(fairshare_amt)					as 'fairshare_amt',
		SUM(credit_amt)						as 'credit_amt',
		SUM(debit_amt)						as 'debit_amt',
		convert(int,0)						as 'count_fairshare'
into	#a
from	registers_client_creditor with (nolock)
where	date_created between @fromDate and @todate
and		tran_type in ('RF','VF','AD','BW','CM','MD','VD','RR','ER')
group by tran_type, creditor_type;

-- Correct the fairshare as a voided creditor refund
update	#a
set		fairshare_amt		= 0 - fairshare_amt,
		count_fairshare = 1
where	tran_type = 'VF'

update	#a
set		count_fairshare	= 1
where	tran_type = 'RF'

update	#a
set		count_fairshare	= 1
where	tran_type in ('AD', 'BW', 'CM', 'MD', 'VD', 'RR')
and		creditor_type = 'D'

-- Remove the fairshare from transactions that we are unintrested
update	#a
set		fairshare_amt		= 0
where	count_fairshare = 0

insert into #t_balance_routine (tran_type, fairshare_amt, credit_amt, debit_amt)
select	tran_type			as tran_type,
		sum(fairshare_amt)	as fairshare_amt,
		sum(credit_amt)		as credit_amt,
		sum(debit_amt)		as debit_amt
from	#a
group by tran_type;

drop table #a

-- Some of the transactions are only in the client register since the apply to the client only.
insert into #t_balance_routine (tran_type, fairshare_amt, credit_amt, debit_amt)
select	tran_type							as 'tran_type',
	0.0										as 'fairshare_amt',
	sum(isnull(credit_amt,0))				as 'credit_amt',
	sum(isnull(debit_amt,0))				as 'debit_amt'

from	registers_client with (nolock)
where	date_created between @fromDate and @toDate
and	tran_type IN ('DP', 'CD', 'MF', 'DM', 'VR', 'CR')
group by tran_type;

-- Find the total amount reserved for pending disbursements
select		@total_rs = sum(rc.debit_amt)
from		view_disbursement_clients c	with (nolock)
inner join	registers_client rc		with (nolock) on rc.client_register = c.client_register
inner join	registers_disbursement d	with (nolock) on c.disbursement_register = d.disbursement_register
where		c.date_disbursed between @fromDate and @toDate
and		d.date_posted is null;

-- Retrieve the information from the results
declare	item_cursor cursor for
	select	upper(tran_type) as tran_type,
			isnull(credit_amt,0) + isnull(debit_amt,0) - isnull(fairshare_amt,0) as tran_value
	from	#t_balance_routine

declare	@tran_type	varchar(2)
declare	@tran_value	money

open item_cursor
fetch item_cursor into @tran_type, @tran_value
while @@fetch_status = 0
begin
	if @tran_type = 'CM'
		select	@total_cm = isnull(@total_cm,0) + isnull(@tran_value,0)
	else if @tran_type = 'MD'
		select	@total_md = isnull(@total_md,0) + isnull(@tran_value,0)
	else if @tran_type in ('RF', 'VF')
		select	@total_rf = isnull(@total_rf,0) + isnull(@tran_value,0)
	else if @tran_type = 'RR'
		select	@total_rr = isnull(@total_rr,0) + isnull(@tran_value,0)
	else if @tran_type = 'BW'
		select	@total_ad = isnull(@total_ad,0) + isnull(@tran_value,0)
	else if @tran_type = 'AD'
		select	@total_ad = isnull(@total_ad,0) + isnull(@tran_value,0)
	else if @tran_type = 'VD'
		select	@total_vd = isnull(@total_vd,0) + isnull(@tran_value,0)
	else if @tran_type = 'CD'
		select	@total_cd = isnull(@total_cd,0) + isnull(@tran_value,0)
	else if @tran_type = 'DP'
		select	@total_dp = isnull(@total_dp,0) + isnull(@tran_value,0)
	else if @tran_type = 'DM'
		select	@total_dm = isnull(@total_dm,0) + isnull(@tran_value,0)
	else if @tran_type = 'MF'
		select	@total_mf = isnull(@total_mf,0) + isnull(@tran_value,0)
	else if @tran_type = 'VR'
		select	@total_vr = isnull(@total_vr,0) + isnull(@tran_value,0)
	else if @tran_type = 'CR'
		select	@total_cr = isnull(@total_cr,0) + isnull(@tran_value,0)

	fetch item_cursor into @tran_type, @tran_value
end

close item_cursor
deallocate item_cursor

-- Return the resulting recordset with the values
select	isnull(@total_cm,0.0)		as 'cm',		-- computer generated manual check (deduct check, etc.)
		isnull(@total_ad,0.0)		as 'ad',		-- auto disbursement
		isnull(@total_md,0.0)		as 'md',		-- manual disbursement
		isnull(@total_rf,0.0)		as 'rf',		-- creditor refund
		isnull(@total_rr,0.0)		as 'rr',		-- rps reject
		isnull(@total_vd,0.0)		as 'vd',		-- creditor voids
		isnull(@total_cd,0.0)		as 'cd',		-- client to client transfers
		isnull(@total_dp,0.0)		as 'dp',		-- deposits
		isnull(@total_dm,0.0)		as 'dm',		-- operating to client transfers
		isnull(@total_mf,0.0)		as 'mf',		-- client to operating transfers
		isnull(@total_vr,0.0)		as 'vr',		-- void client refund checks
		isnull(@total_cr,0.0)		as 'cr',		-- client refunds
		isnull(@total_rs,0.0)		as 'rs'			-- reserved for pending disbursements

-- Discard the working table
drop table #t_balance_routine

-- Terminate normally
return ( 1 )
GO
