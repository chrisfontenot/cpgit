USE [DebtPlus]
GO
/****** Object:  StoredProcedure [dbo].[xpr_disbursement_manual_close]    Script Date: 09/15/2014 13:13:35 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[xpr_disbursement_manual_close] ( @disbursement_register AS INT, @trust_register AS INT, @message as typ_message = NULL ) AS

-- ==================================================================================================================
-- ==            Everything went well in the disbursement. Close the check item                                    ==
-- ==================================================================================================================

-- ChangeLog
--   07/23/2008
--      - Added disbursement_register to the creditor transaction
--      - pulled transaction information from the trust register where possible for the creditor transaction
--      - setup cleanup

-- Disable intermediate results
SET NOCOUNT ON

DECLARE		@creditor	typ_creditor
DECLARE		@date_created	datetime
DECLARE		@total_amount	money

-- Ensure that the message does not contain leading or trailing blanks
IF @message is not null
BEGIN
	SELECT @message = ltrim(rtrim(@message))
	IF @message = ''
		SELECT @message = null
END

-- Fetch the creditor and date from the transaction
SELECT		@creditor	= creditor,
		@date_created	= date_created
FROM		registers_trust
WHERE		trust_register	= @trust_register

-- Generate the client withdrawl items
INSERT INTO registers_client (tran_type, client, debit_amt, trust_register, message, date_created, disbursement_register)
SELECT		'MD', client, sum(debit_amt), @trust_register, @message, @date_created, @disbursement_register
FROM		registers_client_creditor rcc with (nolock)
WHERE		trust_register	= @trust_register
GROUP BY	client

-- Generate the creditor check item
DECLARE	@creditor_register	int

insert into registers_creditor (tran_type, creditor, trust_register, debit_amt, message, date_created, disbursement_register)
select		'MD' as tran_type,
		creditor,
		trust_register,
		amount,
		@message,
		date_created,
		@disbursement_register
from	registers_trust
where	trust_register = @trust_register

SELECT	@creditor_register = SCOPE_IDENTITY()

-- Record the last disbursement information for the creditor
update		creditors
set		last_payment	= @creditor_register
where		creditor	= @creditor

update		creditors
set		first_payment 	= @creditor_register
where		creditor	= @creditor
and		first_payment	is null

-- Return the creditor register with the result
RETURN ( @creditor_register )
GO
