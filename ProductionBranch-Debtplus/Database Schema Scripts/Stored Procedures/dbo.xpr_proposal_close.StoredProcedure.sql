USE [DebtPlus]
GO
/****** Object:  StoredProcedure [dbo].[xpr_proposal_close]    Script Date: 09/15/2014 13:13:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[xpr_proposal_close] ( @proposal_batch_id as varchar(256) ) as

-- ====================================================================================
-- ==       Close the proposal batch                                                 ==
-- ====================================================================================

-- ChangeLog
--   9/31/2002
--     Added support for client_creditor_balances table
--   12/4/2006
--     Change the proposed_start_date to be 5 days in the future if it is too short.

-- Suppress intermediate results
set nocount on

-- ------------------------------------------------------------------------------------
-- --       The batch must be open                                                   --
-- ------------------------------------------------------------------------------------
begin transaction

declare	@stmt		varchar(800)
select	@stmt = 'select	proposal_batch_id, client_creditor, client_creditor_proposal into ##proposals_closed from client_creditor_proposals pr with (nolock) where pr.proposal_batch_id in (' + replace(@proposal_batch_id,';',',') + ')'
exec ( @stmt )
if @@error <> 0
begin
	rollback	transaction
	drop table	##proposals_closed
	return 0
end

-- ------------------------------------------------------------------------------------
-- --       Mark the batch as "closed"                                               --
-- ------------------------------------------------------------------------------------

select	@stmt = '
update		proposal_batch_ids
set		date_closed	= getdate()
from		proposal_batch_ids
where		date_closed is null
and		proposal_batch_id in (' + replace(@proposal_batch_id,';',',') + ')'

exec ( @stmt )
if @@error <> 0
begin
	rollback	transaction
	drop table	##proposals_closed
	return 0
end

-- ------------------------------------------------------------------------------------
-- --       Update the proposal information for the batch                            --
-- ------------------------------------------------------------------------------------
update		client_creditor_proposals
set		proposed_balance	= isnull(bal.orig_balance,0) + isnull(bal.orig_balance_adjustment,0) + isnull(bal.total_interest,0) - isnull(bal.total_payments,0),
		proposed_start_date	= isnull(cc.start_date, c.start_date),
		proposed_amount		= isnull(cc.disbursement_factor,0),
		percent_balance		= coalesce(cc.percent_balance, cr.percent_balance, 1.0),
		terms			= isnull(cc.terms, 0)
from		client_creditor_proposals p
inner join	##proposals_closed clo on clo.client_creditor_proposal = p.client_creditor_proposal
inner join	client_creditor cc on p.client_creditor = cc.client_creditor
inner join	client_creditor_balances bal on cc.client_creditor_balance = bal.client_creditor_balance
inner join	clients c on cc.client = c.client
inner join	creditors cr on cc.creditor = cr.creditor

-- Make the debt start date 5 days from now if it is too short.
declare		@min_start_date		datetime
select		@min_start_date		= dbo.date_only ( dateadd( d, 5, getdate() ));

update		client_creditor_proposals
set		proposed_start_date	= @min_start_date
from		client_creditor_proposals pr
inner join	##proposals_closed clo on clo.client_creditor_proposal = pr.client_creditor_proposal
where		pr.proposed_start_date is null
or		pr.proposed_start_date	< @min_start_date;

-- ------------------------------------------------------------------------------------
-- --       Update the debt information with the amount                              --
-- ------------------------------------------------------------------------------------
update		client_creditor
set		orig_dmp_payment	= isnull(disbursement_factor,0)
from		client_creditor cc
inner join	##proposals_closed clo on clo.client_creditor = cc.client_creditor

-- ------------------------------------------------------------------------------------
-- --       Generate system messages for the event that the proposal was printed     --
-- ------------------------------------------------------------------------------------
insert into client_notes (client, client_creditor, type, is_text, dont_edit, dont_delete, dont_print, subject, note)
select		cc.client, cc.client_creditor, 3, 1, 1, 1, 0,
		case ids.proposal_type
			when 1 then 'Standard Proposal Generated'
			when 2 then 'Full Disclosure Proposal Generated'
			when 3 then 'RPPS EDI Proposal Generated'
			when 4 then 'EPAY EDI Proposal Generated'
		end,

		'A Proposal was submitted to the creditor with the following information:' + char(13) + char(10) +
		char(13) + char(10) +
		'Balance: $' + convert(varchar, p.proposed_balance, 1) + char(13) + char(10) +
		'Payment: $' + convert(varchar, p.proposed_amount, 1) + char(13) + char(10) +
		'Start Date: ' + convert(varchar(10), p.proposed_start_date, 101) + char(13) + char(10)

from		client_creditor_proposals p
inner join	##proposals_closed clo on clo.client_creditor_proposal = p.client_creditor_proposal
inner join	proposal_batch_ids ids on ids.proposal_batch_id = p.proposal_batch_id
inner join	client_creditor cc on p.client_creditor = cc.client_creditor
inner join	clients c on cc.client = c.client

drop table	##proposals_closed

-- Return success
commit transaction
return ( 1 )
GO
