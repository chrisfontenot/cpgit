USE [DebtPlus]
GO
/****** Object:  StoredProcedure [dbo].[rpt_PrintClient_Assets]    Script Date: 09/15/2014 13:13:33 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE PROCEDURE [dbo].[rpt_PrintClient_Assets] ( @Client AS INT ) AS

-- ====================================================================================================
-- ==               Fetch the additional income sources for the client                               ==
-- ====================================================================================================

SELECT		b.description					as 'description',
		a.asset_amount					as 'asset_amount'
FROM		assets a WITH (NOLOCK)
INNER JOIN	asset_ids b WITH (NOLOCK) ON a.asset_id = b.asset_id
WHERE		a.client = @Client
ORDER BY	1

return ( @@rowcount )
GO
