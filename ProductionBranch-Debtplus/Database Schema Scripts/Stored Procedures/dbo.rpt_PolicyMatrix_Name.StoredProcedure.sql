USE [DebtPlus]
GO
/****** Object:  StoredProcedure [dbo].[rpt_PolicyMatrix_Name]    Script Date: 09/15/2014 13:13:33 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[rpt_PolicyMatrix_Name]( @creditor as varchar(10) ) as

-- ==============================================================================================================
-- ==            Retrieve the name header for the indicated policy matrix item of the creditor                 ==
-- ==============================================================================================================

select top 1	p.sic,
		p.name,
		p.division,
		p.installed,
		p.received,
		p.updated,

		case p.eft_creditor
			when 0	then 'N'
				else 'Y'
		end as eft_creditor,

		case p.edi_creditor
			when 0	then 'N'
				else 'Y'
		end as edi_creditor,

		case p.full_disclosure
			when 0	then 'N'
				else 'Y'
		end as full_disclosure

from	policy_matrix_name p with (nolock)
inner join creditors cr with (nolock) on p.sic = cr.sic
where	cr.creditor = @creditor

return ( @@rowcount )
GO
