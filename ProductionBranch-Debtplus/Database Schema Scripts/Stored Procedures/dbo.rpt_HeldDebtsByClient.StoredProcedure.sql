USE [DebtPlus]
GO
/****** Object:  StoredProcedure [dbo].[rpt_HeldDebtsByClient]    Script Date: 09/15/2014 13:13:33 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[rpt_HeldDebtsByClient] ( @counselor as int = null ) AS

-- =============================================================================================
-- ==             List of client debts on hold for the disbursements                          ==
-- =============================================================================================

-- ChangeLog
--   9/31/2002
--     Added support for client_creditor_balances table
--   9/21/2006
--     Fixup conversion of name to use format_name
--   06/09/2009
--      Remove references to debt_number and use client_creditor

set nocount on

if @counselor is null
	select	@counselor = 0

if @counselor <= 0
begin

	-- Include all debts for any client which is on hold
	SELECT	c.disbursement_date,
			cc.creditor,
			cr.creditor_name,
			c.client,
			cc.client_creditor,
			cc.disbursement_factor,
			cc.sched_payment,
			isnull(bal.orig_balance,0) + isnull(bal.orig_balance_adjustment,0) + isnull(bal.total_interest,0) - isnull(bal.total_payments,0) as 'balance',
			dbo.format_normal_name(pn.prefix,pn.first,pn.middle,pn.last,pn.suffix) as 'name',
			cc.account_number,
			c.counselor,
			c.office
	FROM		clients c WITH (NOLOCK)
	INNER JOIN	people p WITH (NOLOCK) ON p.client = c.client AND 1 = p.relation
	left outer join names pn with (nolock) on p.nameid = pn.name
	INNER JOIN	client_creditor cc WITH (NOLOCK) ON cc.client=c.client
	INNER JOIN	client_creditor_balances bal WITH (NOLOCK) ON cc.client_creditor_balance = bal.client_creditor_balance AND cc.client_creditor = bal.client_creditor
	INNER JOIN	creditors cr WITH (NOLOCK) ON cc.creditor=cr.creditor
	WHERE		c.hold_disbursements > 0
	AND		cc.reassigned_debt = 0
	AND		c.active_status IN ('A', 'AR')

	UNION -- DO NOT USE "ALL" HERE

	-- Include specific debts for any client_creditor which is on hold
	SELECT	c.disbursement_date,
			cc.creditor,
			cr.creditor_name,
			c.client,
			cc.client_creditor,
			cc.disbursement_factor,
			cc.sched_payment,
			isnull(bal.orig_balance,0) + isnull(bal.orig_balance_adjustment,0) + isnull(bal.total_interest,0) - isnull(bal.total_payments,0) as 'balance',
			dbo.format_normal_name(pn.prefix,pn.first,pn.middle,pn.last,pn.suffix) as 'name',
			cc.account_number,
			c.counselor,
			c.office
	FROM		clients c WITH (NOLOCK)
	LEFT OUTER JOIN	people p WITH (NOLOCK) ON p.client = c.client AND 1 = p.relation
	left outer join names pn with (nolock) on p.nameid = pn.name
	INNER JOIN	client_creditor cc WITH (NOLOCK) ON cc.client=c.client
	INNER JOIN	client_creditor_balances bal WITH (NOLOCK) ON cc.client_creditor_balance = bal.client_creditor_balance AND cc.client_creditor = bal.client_creditor
	INNER JOIN	creditors cr WITH (NOLOCK) ON cc.creditor=cr.creditor
	WHERE		cc.hold_disbursements > 0
	AND		cc.reassigned_debt = 0
	AND		c.active_status IN ('A', 'AR')

	ORDER BY	4 -- client

END ELSE BEGIN

	-- Include all debts for any client which is on hold
	SELECT	c.disbursement_date,
			cc.creditor,
			cr.creditor_name,
			c.client,
			cc.client_creditor,
			cc.disbursement_factor,
			cc.sched_payment,
			isnull(bal.orig_balance,0) + isnull(bal.orig_balance_adjustment,0) + isnull(bal.total_interest,0) - isnull(bal.total_payments,0) as 'balance',
			dbo.format_normal_name(pn.prefix,pn.first,pn.middle,pn.last,pn.suffix) as 'name',
			cc.account_number,
			c.counselor,
			c.office
	FROM		clients c WITH (NOLOCK)
	INNER JOIN	people p WITH (NOLOCK) ON p.client = c.client AND 1 = p.relation
	left outer join names pn with (nolock) on p.nameid = pn.name
	INNER JOIN	client_creditor cc WITH (NOLOCK) ON cc.client=c.client
	INNER JOIN	client_creditor_balances bal WITH (NOLOCK) ON cc.client_creditor_balance = bal.client_creditor_balance AND cc.client_creditor = bal.client_creditor
	INNER JOIN	creditors cr WITH (NOLOCK) ON cc.creditor=cr.creditor
	WHERE		c.hold_disbursements > 0
	AND		cc.reassigned_debt = 0
	AND		c.active_status IN ('A', 'AR')
	AND		c.counselor = @counselor

	UNION -- DO NOT USE "ALL" HERE

	-- Include specific debts for any client_creditor which is on hold
	SELECT	c.disbursement_date,
			cc.creditor,
			cr.creditor_name,
			c.client,
			cc.client_creditor,
			cc.disbursement_factor,
			cc.sched_payment,
			isnull(bal.orig_balance,0) + isnull(bal.orig_balance_adjustment,0) + isnull(bal.total_interest,0) - isnull(bal.total_payments,0) as 'balance',
			dbo.format_normal_name(pn.prefix,pn.first,pn.middle,pn.last,pn.suffix) as 'name',
			cc.account_number,
			c.counselor,
			c.office
	FROM		clients c WITH (NOLOCK)
	LEFT OUTER JOIN	people p WITH (NOLOCK) ON p.client = c.client AND 1 = p.relation
	left outer join names pn with (nolock) on p.nameid = pn.name
	INNER JOIN	client_creditor cc WITH (NOLOCK) ON cc.client=c.client
	INNER JOIN	client_creditor_balances bal WITH (NOLOCK) ON cc.client_creditor_balance = bal.client_creditor_balance AND cc.client_creditor = bal.client_creditor
	INNER JOIN	creditors cr WITH (NOLOCK) ON cc.creditor=cr.creditor
	WHERE		cc.hold_disbursements > 0
	AND		cc.reassigned_debt = 0
	AND		c.active_status IN ('A', 'AR')
	AND		c.counselor = @counselor

	ORDER BY	4 -- client
END

RETURN ( @@rowcount )
GO
