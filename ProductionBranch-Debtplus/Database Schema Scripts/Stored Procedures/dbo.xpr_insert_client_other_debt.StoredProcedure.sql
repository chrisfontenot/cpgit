USE [DebtPlus]
GO
/****** Object:  StoredProcedure [dbo].[xpr_insert_client_other_debt]    Script Date: 09/15/2014 13:13:35 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[xpr_insert_client_other_debt] ( @client int, @creditor_name varchar(50) = null, @account_number varchar(50) = null, @balance money = 0, @payment money = 0, @interest_rate float = 0) as

	-- ===============================================================================================================
	-- ==           Insert an entry into the other_debts table                                                      ==
	-- ===============================================================================================================
	
	set nocount on

	-- Add the row
	insert into client_other_debts ([client], [creditor_name], [account_number], [balance], [payment], [interest_rate] )
	values (@client, @creditor_name, @account_number, @balance, @payment, @interest_rate)

	return ( scope_identity() )
GO
