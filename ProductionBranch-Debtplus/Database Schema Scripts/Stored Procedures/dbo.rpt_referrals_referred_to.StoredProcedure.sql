USE [DebtPlus]
GO
/****** Object:  StoredProcedure [dbo].[rpt_referrals_referred_to]    Script Date: 09/15/2014 13:13:33 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[rpt_referrals_referred_to] ( @FromDate as DateTime = NULL, @ToDate as DateTime = NULL, @office as int = null ) AS
-- ==========================================================================================
-- ==           Return the information to generate the referral report                     ==
-- ==========================================================================================

-- ChangeLog
--   2/11/2002
--      Changed to use the "start_time" rather than "date_updated"
--      Changed to use only the appointment status rather than the client creation time
--	Changed to consider "walkin" appointments as being "kept"

IF @ToDate IS NULL
	SELECT @ToDate = getdate()

IF @FromDate IS NULL
	SELECT @FromDate = @ToDate

-- Build the selection criteria for the database search
declare	@stmt	varchar(8000)
select	@stmt = '
SELECT	coalesce(ref.description,convert(varchar,apt.referred_to),''Unspecified'') as ''referred_to'',
	COUNT(*) AS ''items''
FROM	client_appointments apt WITH (NOLOCK)
LEFT OUTER JOIN referred_to ref WITH (NOLOCK) ON apt.referred_to = ref.referred_to'

-- If the office is included then include the clients table as well
if isnull(@Office,0) > 0
	select	@stmt = @stmt + ' left outer join clients c with (nolock) on apt.client = c.client'

-- Generate the selection critera for the recordset
declare	@criteria	varchar(800)
select	@criteria = ' where apt.status in (''K'',''W'') and apt.workshop is null'
select	@criteria = @criteria + ' and apt.start_time >= ''' + convert(varchar(10), @FromDate, 101) + ' 00:00:00'''
select	@criteria = @criteria + ' and apt.start_time <= ''' + convert(varchar(10), @ToDate, 101) + ' 23:59:59'''

if isnull(@Office,0) > 0
	select	@criteria = @criteria + ' and c.office = ' + convert(varchar, @office)

-- Run the report statement
select @stmt = @stmt + isnull(@criteria,'') + ' group by ref.description, apt.referred_to order by 1;'
exec ( @stmt )
return ( 0 )
GO
