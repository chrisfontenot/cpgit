USE [DebtPlus]
GO
/****** Object:  StoredProcedure [dbo].[rpt_outstanding_proposal]    Script Date: 09/15/2014 13:13:33 ******/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE PROCEDURE [dbo].[rpt_outstanding_proposal] (@From_Date as datetime = null, @To_Date as datetime = null, @Office as int = null ) AS

-- ChangeLog
--   7/22/2003
--     Discard inactive clients from the result set
--     Discard proposals which have subsequent followup item that is not rejected
--   5/18/2005
--     Select based upon office if desired
--     Replaced day counts with from_date and to_date
--   7/25/2008
--     Do not include proposals for debts that have no balance

set nocount on
if @To_Date is null
	select	@To_Date = getdate()

if @From_Date is null
	select	@From_date = @To_date

select @From_date = convert(datetime, convert(varchar(10), @From_date, 101) + ' 00:00:00'),
	@To_date = convert(datetime, convert(varchar(10), @To_date, 101) + ' 23:59:59')

-- =========================================================================================================
-- ==           Proposals which have not been addressed in more than certain days                         ==
-- =========================================================================================================

SELECT		cc.client								as 'client',
		dbo.format_normal_name (pn.prefix, pn.first, pn.middle, pn.last, pn.suffix)	as 'name',
		crp.proposal_status_date						as 'date', 
		cc.creditor								as 'creditor',
		cr.creditor_name							as 'creditor_name',
		cc.account_number							as 'account_number',
		crp.date_created							as 'date_created',
		crp.client_creditor_proposal						as 'client_creditor_proposal',
		crp.client_creditor							as 'client_creditor',
		c.office								as 'office'
INTO		#t_Verifications
FROM		client_creditor cc	      WITH (NOLOCK)
LEFT OUTER JOIN	people p		      WITH (NOLOCK) ON cc.client = p.client AND 1 = p.relation
left outer join names pn with (nolock) on p.nameid = pn.name
LEFT OUTER JOIN	creditors cr		      WITH (NOLOCK) ON cc.creditor = cr.creditor
LEFT OUTER JOIN	client_creditor_proposals crp WITH (NOLOCK) ON cc.client_creditor = crp.client_creditor
INNER JOIN	clients c		      WITH (NOLOCK) ON cc.client = c.client
WHERE		crp.proposal_status = 4
AND		c.active_status in ('A','AR')
and		cc.reassigned_debt = 0
and		crp.proposal_status_date between @From_date and @To_date
order by	cc.client

-- Toss the items that have no balance
delete	#t_Verifications
from	#t_Verifications x
inner join client_creditor cc on x.client_creditor = cc.client_creditor
inner join client_creditor_balances b on cc.client_creditor_balance = b.client_creditor_balance
where	b.orig_balance + b.orig_balance_adjustment + b.total_interest <= b.total_payments;

-- Discard the proposals which have a followup item pending for them
delete		#t_Verifications
from		#t_Verifications x
inner join	client_creditor_proposals p with (nolock) on x.client_creditor = p.client_creditor
where		p.client_creditor_proposal <> x.client_creditor_proposal
and		p.proposal_status in (1, 2, 3)
and		p.date_created > x.date_created

-- If the office was supplied, discard all but the desired office
if @office is not null
	delete
	from	#t_Verifications
	where	office is null
	or	office <> @Office

-- Return the information to the report
select		tp.client				as 'client',
		tp.name					as 'cname',
		min(tp.date)				as 'date',
		dbo.format_telephonenumber( c.[HomeTelephoneID] )	as 'phone',
		tp.creditor				as 'creditor',
		tp.creditor_name			as 'crname',
		tp.account_number			as 'account',
		dbo.format_counselor_name ( co.person )	as 'counselor'
from		#t_Verifications tp
INNER JOIN clients c with (nolock) on tp.client = c.client
LEFT OUTER JOIN counselors co with (NOLOCK) ON c.counselor = co.counselor
group by	tp.client, tp.name, c.HomeTelephoneID, tp.creditor, tp.creditor_name, tp.account_number, co.person
order by	1

DROP TABLE #t_Verifications

return (@@rowcount)
GO
