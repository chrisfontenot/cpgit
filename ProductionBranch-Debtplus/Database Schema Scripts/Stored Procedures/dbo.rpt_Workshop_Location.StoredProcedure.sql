USE [DebtPlus]
GO
/****** Object:  StoredProcedure [dbo].[rpt_Workshop_Location]    Script Date: 09/15/2014 13:13:34 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE PROCEDURE [dbo].[rpt_Workshop_Location] ( @Location AS INT ) AS
-- ======================================================================================================
-- ==                   Retrieve a list of pending workshops                                           ==
-- ======================================================================================================
SELECT	l.name				as 'location_name',
		dbo.format_address_line_1(a.house, a.direction, a.street, a.suffix, a.modifier, a.modifier_value) as 'location_address1',
		a.address_line_2			as 'location_address2',
		dbo.format_city_state_zip ( a.city, a.state, a.postalcode ) as 'location_address3',
		dbo.format_TelephoneNumber(l.TelephoneID)			as 'location_phone',
		l.directions			as 'location_directions',
		t.name				as 'organization_type'
FROM		workshop_locations l
LEFT OUTER JOIN	workshop_organization_types t ON l.type = t.organization_type
left outer join addresses a with (nolock) on l.AddressID = a.Address
WHERE		l.workshop_location = @Location

RETURN ( @@rowcount )
GO
