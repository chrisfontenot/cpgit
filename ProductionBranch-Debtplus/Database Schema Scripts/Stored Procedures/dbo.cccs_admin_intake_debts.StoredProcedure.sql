USE [DebtPlus]
GO
/****** Object:  StoredProcedure [dbo].[cccs_admin_intake_debts]    Script Date: 09/15/2014 13:13:31 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[cccs_admin_intake_debts] ( @application_id as int, @client as int ) as

declare	@creditor_id		int
declare	@priority		int
declare	@defined_creditor_id	varchar(80)
declare	@creditor_name		varchar(80)
declare	@account_number		varchar(80)
declare	@current_balance	money
declare	@monthly_payment	money
declare	@num_months_past_due	int
declare	@interest_rate		float
declare	@state			varchar(10)
declare	@creditor		varchar(10)
declare	@config_fee		int
declare	@fee_sched_payment	money
declare	@fee_type		int
declare @ForeignDatabaseID varchar(50)

-- Create the items as needed
declare intake_debt_cursor cursor for
	select	creditor_id											as creditor_id,
			defined_creditor_id									as creditor,
			creditor_name										as creditor_name,
			dbo.cccs_admin_account_number ( account_number )	as account_number,
			current_balance										as current_balance,
			monthly_payment										as monthly_payment,
			num_months_past_due									as num_months_past_due,
			dbo.cccs_admin_rates ( interest_rate )				as interest_rate,
			ForeignDatabaseID									as ForeignDatabaseID
	from	cccs_admin..applicant_creditors with (nolock)
	where	application_id	= @application_id
	and		defined_creditor_id IS NULL

	union

	select	applicant_creditors.creditor_id						as creditor_id,
			defined_creditor_id									as creditor,
			creditors.creditor_name								as creditor_name,
			dbo.cccs_admin_account_number ( account_number )	as account_number,
			current_balance										as current_balance,
			monthly_payment										as monthly_payment,
			num_months_past_due									as num_months_past_due,
			dbo.cccs_admin_rates ( interest_rate )				as interest_rate,
			ForeignDatabaseID									as ForeignDatabaseID
	from	cccs_admin..applicant_creditors  AS applicant_creditors with (nolock),
			cccs_admin..creditors AS creditors with (nolock)
	where	application_id	= @application_id
	and		defined_creditor_id IS NOT NULL
	and		creditors.creditor_id = applicant_creditors.defined_creditor_id

open intake_debt_cursor
fetch intake_debt_cursor into @creditor_id, @defined_creditor_id, @creditor_name, @account_number, @current_balance, @monthly_payment, @num_months_past_due, @interest_rate, @ForeignDatabaseID

while @@fetch_status = 0
begin
	declare	@client_creditor			int
	declare	@client_creditor_balance	int
	
	select	@client_creditor			= null,
			@client_creditor_balance	= null

	if @ForeignDatabaseID is not null
		select	@client_creditor_balance	= cc.client_creditor_balance,
				@client_creditor			= cc.client_creditor
		from	client_creditor cc
		where	cc.client_creditor			= CONVERT(int, @ForeignDatabaseID);

	-- If there is no existing record then create a new debt
	if @client_creditor is null
	begin

		-- Create the debt record
		execute @client_creditor = xpr_create_debt @client
		
		-- Find the balance record from the debt
		select  @client_creditor_balance = client_creditor_balance
		from	client_creditor
		where	client_creditor = @client_creditor;
		
		-- Update the foregin database with the new debt information
		update	cccs_admin..applicant_creditors
		set		ForeignDatabaseID = @client_creditor
		where	creditor_id		  = @creditor_id
		
		update  client_creditor_balances
		set		orig_balance      = @current_balance
		where	client_creditor_balance = @client_creditor_balance

		-- Correct the debt information
		update	client_creditor
		set		creditor_name       = left(ltrim(rtrim(@creditor_name)), 50),
				account_number      = left(ltrim(rtrim(replace(replace(@account_number, '-', ''), ' ', ''))), 22),
				non_dmp_payment     = ISNULL(@monthly_payment,0),
				disbursement_factor = ISNULL(@monthly_payment,0),
				sched_payment       = ISNULL(@monthly_payment,0),
				months_delinquent	= ISNULL(@num_months_past_due,0),
				non_dmp_interest    = @interest_rate
		where	client_creditor	    = @client_creditor;
	end
	
	-- Otherwise, the debt currently exists. Adjust the balance normally.
	else begin
	
		-- Correct the debt information
		update	client_creditor
		set		creditor_name       = left(ltrim(rtrim(@creditor_name)), 50),
				account_number      = left(ltrim(rtrim(replace(replace(@account_number, '-', ''), ' ', ''))), 22),
				non_dmp_payment     = ISNULL(@monthly_payment,0),
				disbursement_factor = ISNULL(@monthly_payment,0),
				sched_payment       = ISNULL(@monthly_payment,0),
				months_delinquent	= ISNULL(@num_months_past_due,0),
				non_dmp_interest    = @interest_rate
		where	client_creditor	    = @client_creditor;

		-- Correct the balance information
		execute xpr_debt_balance @client_creditor, @current_balance
	end

	-- Go to the next record in the input database
	fetch intake_debt_cursor into @creditor_id, @defined_creditor_id, @creditor_name, @account_number, @current_balance, @monthly_payment, @num_months_past_due, @interest_rate, @ForeignDatabaseID
end

-- After all debts are loaded, close out the cursor.
close		intake_debt_cursor
deallocate	intake_debt_cursor

return ( 1 )
GO
