USE [DebtPlus]
GO
/****** Object:  StoredProcedure [dbo].[rpt_Client_Statements_Quarterly_Notes]    Script Date: 09/15/2014 13:13:32 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE PROCEDURE [dbo].[rpt_Client_Statements_Quarterly_Notes] ( @client_statement_batch_1 as int = null, @client_statement_batch_2 as int = null, @client_statement_batch_3 as int = null, @client as int = null ) as

-- ===========================================================================================
-- ==        Client statement Notes information                                             ==
-- ===========================================================================================
declare	@note		varchar(8000)

-- Include the statement note
select	@note		= isnull(@note + char(13) + char(10) + char(13) + char(10),'') + isnull(note,'')
from	client_statement_batches with (nolock)
where	client_statement_batch	= @client_statement_batch_3
and	note is not null;

-- Include the client statement note
select	@note			= isnull(@note + char(13) + char(10) + char(13) + char(10),'') + isnull(note,'')
from	client_statement_notes with (nolock)
where	client_statement_batch	= @client_statement_batch_3
and	client			= @client
and	note is not null;

-- Make the note empty if there are no notes
while left(@note,1) = char(13)
	select	@note = substring(@note, 3, 8000)

if ltrim(rtrim(isnull(@note,''))) = ''
	select	@note	= null

if @note is null
begin
	-- We need to create an empty result set. Do it with a temp table.
	create table #results ( note varchar(8000) );
	select * from #results;
	drop table #results
end else
	select	@note	as note

return	( 1 )
GO
