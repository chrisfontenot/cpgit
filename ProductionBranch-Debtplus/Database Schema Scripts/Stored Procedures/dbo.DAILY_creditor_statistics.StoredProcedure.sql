USE [DebtPlus]
GO
/****** Object:  StoredProcedure [dbo].[DAILY_creditor_statistics]    Script Date: 09/15/2014 13:13:32 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[DAILY_creditor_statistics] as
-- =============================================================================================
-- ==            Calculate the statitics for the creditors on a dail basis                    ==
-- =============================================================================================

-- ChangeLog
--   01/15/2002
--     Added creditor_classes table references and dropped "creditors.fee_creditor"

-- Discard the creditor statistics
delete
from	creditor_statistics

-- Insert the basic creditor statistics for all creditors
insert into creditor_statistics (creditor, avg_disbursement, total_disbursement, total_balance, clients_balance, zero_bal_clients, active_clients, clients)
select	creditor, 0, 0, 0, 0, 0, 0, 0
from	creditors;

-- Select the debt information for the current disbursements
create table #creditor_statistics (creditor varchar(10), client int, active_status varchar(10) null, balance money, disbursement_factor money, zero_balance int, client_creditor int, client_creditor_balance int)

-- Retrieve the debt list for every active client on the system
insert into #creditor_statistics (creditor, client, active_status, balance, disbursement_factor, zero_balance, client_creditor, client_creditor_balance)
SELECT		cc.creditor as creditor,
			cc.client as client,
			c.active_status as active_status,
			isnull(bal.orig_balance,0) + isnull(bal.orig_balance_adjustment,0) + isnull(bal.total_interest,0) - isnull(bal.total_payments,0) as balance,
			isnull(cc.disbursement_factor,0) as disbursement_factor,
			isnull(ccl.zero_balance,0) as zero_balance,
			cc.client_creditor,
			cc.client_creditor_balance
FROM		client_creditor cc	WITH (NOLOCK)
INNER JOIN	client_creditor_balances bal WITH (NOLOCK) ON cc.client_creditor_balance = bal.client_creditor_balance
INNER JOIN	clients c		WITH (NOLOCK) ON cc.client = c.client
INNER JOIN	creditors cr		WITH (NOLOCK) ON cc.creditor = cr.creditor
LEFT OUTER JOIN	creditor_classes ccl	WITH (NOLOCK) ON cr.creditor_class = ccl.creditor_class
WHERE		c.active_status in ('A', 'AR')
AND			cc.reassigned_debt = 0

-- Correct the balances
update	#creditor_statistics set balance = 0 where zero_balance <> 0;

-- Find the statistics by the creditor/client pair. (Merge duplicate debts together.)
select creditor, client, sum(balance) as balance, 0 as has_balance, count(*) as debt_count, sum(disbursement_factor) as disbursement_factor
into	#creditor_statistics_client_count
from	#creditor_statistics
group by creditor, client;

-- Determine the number of clients who have a balance
update	#creditor_statistics_client_count set has_balance = 1 where balance <> 0;

-- Find the number of clients for each creditor
select	creditor, count(*) as cnt, sum(disbursement_factor) as disbursement_factor, sum(balance) as balance, sum(has_balance) as has_balance, count(*) as debt_count -- , sum(debt_count) as debt_count
into	#creditor_statistics_count
from	#creditor_statistics_client_count
group by creditor;

update	creditor_statistics
set	clients				= x.cnt,
	active_clients		= x.cnt,
	total_disbursement	= x.disbursement_factor,
	total_balance		= x.balance,
	clients_balance		= x.has_balance,
	zero_bal_clients	= x.debt_count - x.has_balance
from	creditor_statistics s
inner join #creditor_statistics_count x on s.creditor = x.creditor;

-- Find the average disbursement information
select	creditor, avg(convert(float,disbursement_factor)) as avg_disbursement
into	#creditor_statistics_avg
from	#creditor_statistics
group by creditor;

update	creditor_statistics
set	avg_disbursement	= convert(money,convert(decimal(10,2),x.avg_disbursement))
from	creditor_statistics s
inner join #creditor_statistics_avg x on s.creditor = x.creditor;

drop table #creditor_statistics_client_count;
drop table #creditor_statistics_count;
drop table #creditor_statistics_avg;
drop table #creditor_statistics;

return ( 1 )
GO
