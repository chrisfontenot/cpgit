USE [DebtPlus]
GO
/****** Object:  StoredProcedure [dbo].[cmd_Reset_ACH_on_completed_clients]    Script Date: 09/15/2014 13:13:32 ******/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE PROCEDURE [dbo].[cmd_Reset_ACH_on_completed_clients] AS
-- =============================================================================================================
-- ==            Find the list of clients and their debt balance for those on ACH                             ==
-- =============================================================================================================

-- ChangeLog
--   9/31/2002
--     Added support for client_creditor_balances table
--   01/15/2002
--     Added creditor_classes table references and dropped "creditors.fee_creditor"

-- Reset the intermediate result sets
SET NOCOUNT ON

-- Turn off the ACH status for non-active clients
update		client_ach
set			isActive	= 0
from		client_ach ach with (nolock)
inner join  clients c with (nolock) on ach.client = c.client
where		c.active_status not in ('A','AR')
and			ach.isActive = 1

-- Fetch the list of clients and their non-fee balances
select		cc.client, sum(bal.orig_balance + bal.orig_balance_adjustment + bal.total_interest - bal.total_payments) as 'balance'
into		#t_active_ach
from		client_creditor cc with (nolock)
inner join	client_creditor_balances bal with (nolock) on cc.client_creditor_balance = bal.client_creditor_balance
inner join	clients c with (nolock) on cc.client = c.client
inner join  client_ach ach with (nolock) on c.client = ach.client
inner join	creditors cr with (nolock) on cc.creditor = cr.creditor
left outer join	creditor_classes ccl with (nolock) on cr.creditor_class = ccl.creditor_class
where		cc.reassigned_debt = 0
and			isnull(ccl.zero_balance,0) = 0
and			ach.isActive = 1
group by	cc.client

-- Turn off the ACH pull for those clients who have no debt
update		client_ach
set			isActive	= 0
from		client_ach ach with (nolock)
inner join  clients c with (nolock) on ach.client = c.client
inner join	#t_active_ach a with (nolock) on c.client = a.client
where		c.active_status not in ('A','AR')
and			ach.isActive = 1
and			a.balance < 1

-- Drop the table
drop table	#t_active_ach

-- Terminate normally
RETURN ( @@rowcount )
GO
