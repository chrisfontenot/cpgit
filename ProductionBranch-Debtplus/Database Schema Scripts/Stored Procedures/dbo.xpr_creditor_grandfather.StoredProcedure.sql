USE [DebtPlus]
GO
/****** Object:  StoredProcedure [dbo].[xpr_creditor_grandfather]    Script Date: 09/15/2014 13:13:34 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[xpr_creditor_grandfather](@creditor as varchar(10), @old_rate as float) as

update	client_creditor
set		dmp_interest	= @old_rate
where	creditor		= @creditor
and		dmp_interest	is null

return ( @@rowcount )
GO
