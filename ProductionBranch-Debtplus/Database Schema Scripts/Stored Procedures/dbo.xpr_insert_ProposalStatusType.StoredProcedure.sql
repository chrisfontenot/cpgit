USE [DebtPlus]
GO
/****** Object:  StoredProcedure [dbo].[xpr_insert_ProposalStatusType]    Script Date: 09/15/2014 13:13:35 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[xpr_insert_ProposalStatusType] ( @description as varchar(50), @nfcc as varchar(4) = null, @rpps as varchar(4) = null, @epay as varchar(4) = null, @note as varchar(256) = null, @hud_9902_section as varchar(256) = null, @default as bit = 0, @ActiveFlag as bit = 1 ) as
-- ========================================================================================
-- ==   Procedure for .NET to insert items into the ProposalStatusTypes table                   ==
-- ========================================================================================
	insert into ProposalStatusTypes ( [description],[nfcc],[rpps],[epay],[note],[hud_9902_section],[default],[ActiveFlag] ) values ( @description,@nfcc,@rpps,@epay,@note,@hud_9902_section,@default,@ActiveFlag )
	return ( scope_identity() )
GO
