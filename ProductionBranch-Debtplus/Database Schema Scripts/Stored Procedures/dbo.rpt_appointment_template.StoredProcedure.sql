USE [DebtPlus]
GO
/****** Object:  StoredProcedure [dbo].[rpt_appointment_template]    Script Date: 09/15/2014 13:13:32 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[rpt_appointment_template]( @Office AS int = NULL, @Day AS VARCHAR(10) = NULL ) AS
-- ===========================================================================================
-- ==           Information for the appointment activity report                             ==
-- ===========================================================================================

-- Generate the list of days
SELECT
		t.appt_time_template,
		o.office as 'office',
		t.dow as 'day',

		CASE t.dow
			WHEN 1 THEN 'Sunday'
			WHEN 2 THEN 'Monday'
			WHEN 3 THEN 'Tuesday'
			WHEN 4 THEN 'Wednesday'
			WHEN 5 THEN 'Thursday'
			WHEN 6 THEN 'Friday'
			WHEN 7 THEN 'Saturday'
		END AS 'DOW',

		RIGHT('00' + CONVERT(varchar(2), t.start_time / 60), 2) + ':' + RIGHT('00' + CONVERT(varchar(2), t.start_time % 60), 2) AS start_hour,
		UPPER(ISNULL(a.appt_name, 'ANY TYPE')) AS 'type'
INTO		#t_app1
FROM		appt_time_templates t with (nolock)
LEFT OUTER JOIN offices o with (nolock) ON t.office = o.office
LEFT OUTER JOIN	appt_types a with (nolock) ON t.appt_type = a.appt_type

-- Generate the counselor / appointment schedule
SELECT		t.appt_time_template, 
		o.office, o.name as office_name,
		t.day,
		t.DOW, 
		t.start_hour, 
		t.type,
		dbo.format_normal_name(default, cox.first, default, cox.last, default) as counselor

INTO		#t_app2

FROM		counselors co with (nolock)
left outer join names cox with (nolock) ON co.nameid = cox.name
INNER JOIN	appt_counselor_templates ct  with (nolock) ON co.counselor = ct.counselor
LEFT OUTER JOIN	#t_app1 t ON ct.appt_time_template = t.appt_time_template
LEFT OUTER JOIN offices o WITH (nolock) on t.office = o.office
WHERE		t.appt_time_template IS NOT NULL  
order by    	t.office, t.day, t.start_hour

-- Toss items that we wish to filter
if isnull(@office,0) > 0
	delete
	from	#t_app2
	where	office <> @office
	
if isnull(@day,'') <> ''
	delete
	from	#t_app2
	where	dow <> @Day

SELECT appt_time_template, office_name as office, day, dow, start_hour, type, counselor FROM #t_app2

drop table #t_app1
drop table #t_app2
GO
