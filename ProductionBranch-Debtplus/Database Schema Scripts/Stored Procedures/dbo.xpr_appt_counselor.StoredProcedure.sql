USE [DebtPlus]
GO
/****** Object:  StoredProcedure [dbo].[xpr_appt_counselor]    Script Date: 09/15/2014 13:13:34 ******/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[xpr_appt_counselor] ( @Appt AS INT, @Counselor AS INT = NULL ) AS

-- ================================================================================================
-- ==            Update the counselor for the indicated appointment                              ==
-- ================================================================================================

-- ChangeLog
--   9/8/2002
--     Do not attempt to set the counselor to -1. This is the "magic" value for NULL that that the appointment
--     class uses.

-- Do not generate intermediate result sets
SET NOCOUNT ON

-- Ensure that the counselor exists
if @counselor is not null
begin
	if not exists (SELECT	counselor
			from	counselors
			where	counselor	= @counselor)
	begin
		select	@counselor = null
	end
end

-- Update the apppointment with the counselor ID. It may be NULL.
UPDATE	client_appointments
SET	counselor		= @Counselor
WHERE	client_appointment	= @Appt

RETURN ( @@rowcount )
GO
