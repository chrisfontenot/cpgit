USE [DebtPlus]
GO
/****** Object:  StoredProcedure [dbo].[rpt_custom_CWCID]    Script Date: 09/15/2014 13:13:33 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[rpt_custom_CWCID] ( @from_date as datetime = null, @to_date as datetime = null ) as

if @to_date is null
	select @to_date = getdate()

if @from_date is null
	select @from_date = @to_date

select	@from_date = convert(varchar(10), @from_date, 101),
	@to_date = convert(varchar(10), @to_date, 101) + ' 23:59:59';

select	e.address as email,cc.creditor, c.client, dbo.format_normal_name(pn.prefix, pn.first, pn.middle, pn.last, pn.suffix) as client_name, b.orig_balance + b.orig_balance_adjustment + b.total_interest - b.total_payments as balance, c.start_date
from	clients c
inner join registers_client rc on c.client = rc.client
inner join client_creditor cc on cc.client = rc.client and cc.creditor in ('Z11973', 'Z0003') and cc.reassigned_debt = 0
inner join client_creditor_balances b on cc.client_creditor_balance = b.client_creditor_balance and cc.client_creditor = b.client_creditor
inner join people p on c.client = p.client and 1 = p.relation
left outer join emailaddresses e on p.emailid = e.email
left outer join names pn with (nolock) on p.nameid = pn.name
where	rc.tran_type in ('DP', 'CD')
and		rc.date_created between @from_date and @to_date
and		c.active_status in ('A','AR')
group by e.address,cc.creditor, c.client, c.date_created, pn.prefix, pn.first, pn.middle, pn.last, pn.suffix, b.orig_balance, b.orig_balance_adjustment, b.total_interest, b.total_payments, c.start_date
having count(rc.client_register) > 1
order by c.client;

return ( @@rowcount )
GO
