SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER procedure [www_client_cc_trans_detail] ( @client as int, @client_register as int ) AS
-- =======================================================================================
-- ==            Determine the client/creditor transactions for the disbursement        ==
-- =======================================================================================

-- ChangeLog
--   9/31/2002
--     Added support for client_creditor_balances table
--   1/28/2004
--     Force balance to be zero on reassigned debts
--   3/14/2003
--     Allow for more than 10,000 creditors in a class
--   06/09/2009
--      Remove references to debt_number and use client_creditor

-- Disable intermediate results
set nocount on

-- Determine the transaction type of the item
declare	@tran_type		varchar(2)
declare	@disbursement_register	int

select	@disbursement_register = disbursement_register
from	registers_client
where	client = @client
and	client_register = @client_register
and	tran_type in ('AD', 'MD', 'CM')

if @disbursement_register is null
	select	@disbursement_register = -1

-- Retrieve the transactions for the indicated disbursement
select		isnull(cc.creditor+' ','') + coalesce (cr.creditor_name, cc.creditor_name, '')	as 'creditor_name',
		coalesce(rcc.account_number, cc.message, cc.account_number, '')			as 'account_number',
		rcc.debit_amt									as 'debit_amt',

		case
			when cc.reassigned_debt = 0 then isnull(bal.orig_balance,0) + isnull(bal.orig_balance_adjustment,0) + isnull(bal.total_interest,0) - isnull(bal.total_payments, 0)
			else convert(money,0)
		end										as 'current_balance',

		convert(varchar,tr.checknum)			as 'check_number',
		cc.client_creditor								as 'client_creditor'

from		registers_client_creditor rcc with (nolock)
inner join	client_creditor cc with (nolock) on rcc.client_creditor = cc.client_creditor
inner join	client_creditor_balances bal with (nolock) on cc.client_creditor_balance = bal.client_creditor_balance
inner join	clients c with (nolock) on rcc.client = c.client
inner join	registers_trust tr with (nolock) on rcc.trust_register = tr.trust_register
left outer join	creditors cr with (nolock) on rcc.creditor = cr.creditor
where		c.active_status in ('A', 'AR')
and		rcc.client = @client
and		rcc.disbursement_register = @disbursement_register
and		rcc.tran_type in ('AD', 'BW', 'MD', 'CM')
order by	cr.type, cr.creditor_id, 1, 2

return ( @@rowcount )
GO
