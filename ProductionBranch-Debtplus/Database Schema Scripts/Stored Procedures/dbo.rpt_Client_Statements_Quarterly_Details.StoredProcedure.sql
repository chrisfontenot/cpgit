USE [DebtPlus]
GO
/****** Object:  StoredProcedure [dbo].[rpt_Client_Statements_Quarterly_Details]    Script Date: 09/15/2014 13:13:32 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE PROCEDURE [dbo].[rpt_Client_Statements_Quarterly_Details] ( @client_statement_batch_1 as int = null, @client_statement_batch_2 as int = null, @client_statement_batch_3 as int = null, @client as int = null ) as

set nocount on

-- ===========================================================================================
-- ==        Client statement information                                                   ==
-- ===========================================================================================

-- Find the period starting dates for the column headings
declare	@period_start_1	datetime
select	@period_start_1 = period_start from client_statement_batches with (nolock) where client_statement_batch = @client_statement_batch_1

declare	@period_start_2	datetime
select	@period_start_2 = period_start from client_statement_batches with (nolock) where client_statement_batch = @client_statement_batch_2

declare	@period_start_3	datetime
select	@period_start_3 = period_start from client_statement_batches with (nolock) where client_statement_batch = @client_statement_batch_3

-- Find the list of the debts (irrespective of the periods)
select	distinct convert(varchar(10), '')				as creditor,
		 convert(int,0)						as client_creditor,
		 b.client_creditor_balance				as client_creditor_balance,
		 convert(money,0)					as current_balance,

		 convert(money,0)					as v1_debits,
		 convert(money,0)					as v2_debits,
		 convert(money,0)					as v3_debits,

		 convert(money,0)					as v1_credits,
		 convert(money,0)					as v2_credits,
		 convert(money,0)					as v3_credits,

		 convert(money,0)					as sched_payment,
		 convert(float,0)					as interest_rate,
		 convert(money,0)					as payments_to_date,
		 b.orig_balance						as original_balance,
		 convert(varchar(50),'')				as account_number,
		 convert(varchar(50),'')				as creditor_name,

		 @period_start_1					as v1_period_date,
		 @period_start_2					as v2_period_date,
		 @period_start_3					as v3_period_date

into	#t
from	client_statement_details d	with (nolock)
inner join client_creditor cc		with (nolock) on d.client_creditor = cc.client_creditor
inner join client_creditor_balances b	with (nolock) on cc.client_creditor_balance = b.client_creditor_balance and cc.client_creditor = b.client_creditor
where	d.client			= @client
and	d.client_statement_batch	in (@client_statement_batch_1, @client_statement_batch_2, @client_statement_batch_3)
and	cc.reassigned_debt		= 0

create unique index temp_ix_t on #t ( client_creditor, client_creditor_balance );

-- Add the payments for the various debt groups together
select	b.client_creditor_balance, b.client_creditor, d.client_statement_batch, sum(credits) as credits, sum(debits) as debits
into	#t_s
from	client_statement_details d	with (nolock)
inner join client_creditor cc		with (nolock) on d.client_creditor = cc.client_creditor
inner join client_creditor_balances b	with (nolock) on cc.client_creditor_balance = b.client_creditor_balance
where	d.client			= @client
and	d.client_statement_batch	in (@client_statement_batch_1, @client_statement_batch_2, @client_statement_batch_3)
group by b.client_creditor_balance, b.client_creditor, d.client_statement_batch;

-- Update the specific period information for the debts
update	#t
set	v3_debits		= s.debits,
	v3_credits		= s.credits,
	client_creditor		= s.client_creditor
from	#t t
inner join #t_s s on t.client_creditor_balance = s.client_creditor_balance
where	s.client_statement_batch	= @client_statement_batch_3;

-- Update the specific period information for the debts
update	#t
set	v2_debits		= s.debits,
	v2_credits		= s.credits,
	client_creditor		= s.client_creditor
from	#t t
inner join #t_s s on t.client_creditor_balance = s.client_creditor_balance
where	s.client_statement_batch	= @client_statement_batch_2;

-- Update the specific period information for the debts
update	#t
set	v1_debits		= s.debits,
	v1_credits		= s.credits,
	client_creditor		= s.client_creditor
from	#t t
inner join #t_s s on t.client_creditor_balance = s.client_creditor_balance
where	s.client_statement_batch	= @client_statement_batch_1;

drop table #t_s;

-- Supply the other information from the batch tables
update	#t
set	creditor		= cr.creditor,
	account_number		= left(cc.account_number, 50),
	current_balance		= d.current_balance,
	sched_payment		= d.sched_payment,
	payments_to_date	= d.payments_to_date,
	creditor_name		= left(cr.creditor_name,50),
	interest_rate		= d.interest_rate
from	client_statement_details d	with (nolock)
inner join client_creditor cc		with (nolock) on d.client_creditor = cc.client_creditor
inner join #t t					      on cc.client_creditor_balance = t.client_creditor_balance
inner join creditors cr			with (nolock) on cc.creditor = cr.creditor
where	d.client			= @client
and	d.client_statement_batch	= @client_statement_batch_1;

-- Overwrite the information with the second batch
update	#t
set	creditor		= cr.creditor,
	account_number		= left(cc.account_number, 50),
	current_balance		= d.current_balance,
	sched_payment		= d.sched_payment,
	payments_to_date	= d.payments_to_date,
	creditor_name		= left(cr.creditor_name,50),
	interest_rate		= d.interest_rate
from	client_statement_details d	with (nolock)
inner join client_creditor cc		with (nolock) on d.client_creditor = cc.client_creditor
inner join #t t					      on cc.client_creditor_balance = t.client_creditor_balance
inner join creditors cr			with (nolock) on cc.creditor = cr.creditor
where	d.client			= @client
and	d.client_statement_batch	= @client_statement_batch_2;

-- Overwrite the information with the third batch for the latest information
update	#t
set	creditor		= cr.creditor,
	account_number		= left(cc.account_number, 50),
	current_balance		= d.current_balance,
	sched_payment		= d.sched_payment,
	payments_to_date	= d.payments_to_date,
	creditor_name		= left(cr.creditor_name,50),
	interest_rate		= d.interest_rate
from	client_statement_details d	with (nolock)
inner join client_creditor cc		with (nolock) on d.client_creditor = cc.client_creditor
inner join #t t					      on cc.client_creditor_balance = t.client_creditor_balance
inner join creditors cr			with (nolock) on cc.creditor = cr.creditor
where	d.client			= @client
and	d.client_statement_batch	= @client_statement_batch_3;

-- Return the results
select	creditor,
	client_creditor,
	current_balance,
	v1_debits - v1_credits as v1_current_payment,
	v2_debits - v2_credits as v2_current_payment,
	v3_debits - v3_credits as v3_current_payment,
	v1_debits, v2_debits, v3_debits,
	v1_credits, v2_credits, v3_credits,
	sched_payment,
	interest_rate * 100.0 as interest_rate,
	payments_to_date,
	original_balance,
	dbo.statement_account_number ( account_number ) as account_number,
	creditor_name,
	v1_period_date,
	v2_period_date,
	v3_period_date
from	#t
order by creditor;

drop table #t

return ( @@rowcount )
GO
