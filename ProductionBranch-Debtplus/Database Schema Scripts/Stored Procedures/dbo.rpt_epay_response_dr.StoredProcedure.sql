USE [DebtPlus]
GO
/****** Object:  StoredProcedure [dbo].[rpt_epay_response_dr]    Script Date: 09/15/2014 13:13:33 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[rpt_epay_response_dr] ( @epay_response_file int = null ) as
-- ============================================================================================
-- ==            List the proposals and their status for the epay response details           ==
-- ============================================================================================

-- Surpress intermediate results
set nocount on

-- Find the file if one was not supplied
if @epay_response_file is null
begin
	select	top 1
		@epay_response_file = epay_response_file
	from	epay_response_files with (nolock)
	order by date_created desc
end

-- There should be a file or we are in trouble.
if @epay_response_file is null
begin
	RaisError ('There are no pending response files', 16, 1)
	return ( 0 )
end

-- Return the information from the database tables for the proposal statuses
select	dr.epay_response_dr				as response_detail,
	dr.transaction_id				as response_trace_number,
	
	case
		when dr.error is not null then dr.error
		when dr.response_code = 'A' then null
		when rej.description is null then dr.decline_reason_code
		else rej.description
	end						as response_error,
	
	dr.response_date				as response_date,
	dr.creditor_id					as creditor_id,
	dr.creditor_name				as creditor_name,
	dr.client_number				as client,
	cc.creditor					as creditor,
	dbo.format_normal_name(default,cox.first,default,cox.last,default) as counselor_name,
	dr.client_name					as client_name,
	dr.response_code				as response_code,
	dr.proposed_payment_amount			as proposed_payment_amount,
	dr.accepted_payment_amount			as accepted_payment_amount,
	dr.customer_biller_account_number_original	as account_number_original,
	dr.customer_biller_account_number_corrected	as account_number_corrected,
	dr.last_payment_date				as last_payment_date,
	case when dr.corrected_balance_included = 'Y' then dr.current_client_balance else null end	as current_client_balance,
	case when dr.corrected_balance_included = 'Y' then dr.date_of_balance else null end		as date_of_balance,
	dr.number_of_days_delinquent			as number_of_days_delinquent,
	dr.creditor_contact_name			as creditor_contact_name,
	dr.date_of_authorization			as date_of_authorization

from		epay_responses_dr dr with (nolock)
left outer join epay_transactions tr with (nolock)		on dr.epay_transaction = tr.epay_transaction
left outer join client_creditor_proposals pr with (nolock)	on tr.client_creditor_proposal = pr.client_creditor_proposal
left outer join client_creditor cc with (nolock)		on pr.client_creditor = cc.client_creditor
left outer join clients c with (nolock)				on cc.client = c.client
left outer join counselors co with (nolock)			on c.counselor = co.counselor
left outer join proposal_result_reasons rej with (nolock)	on pr.proposal_reject_reason = rej.proposal_result_reason
left outer join names cox with (nolock) on co.NameID = cox.name

where	epay_response_file = @epay_response_file

order by	response_code, creditor_id, client_number, len(customer_biller_account_number_original), customer_biller_account_number_original

return ( @@rowcount )
GO
