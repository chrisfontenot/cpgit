IF NOT EXISTS(SELECT * FROM SYSOBJECTS WHERE NAME=N'xpr_disbursement_cc_trans' AND TYPE='P')
	EXEC ('CREATE PROCEDURE xpr_disbursement_cc_trans AS')
GO
ALTER PROCEDURE [dbo].[xpr_disbursement_cc_trans] ( @tran_type as typ_transaction, @client_creditor as int, @creditor_type as varchar(1), @debit_amt as money, @fairshare_pct as float, @fairshare_amt as money, @disbursement_register as typ_key, @trust_register as typ_key, @account_number as varchar(22), @creditor_register as int = null ) AS

-- =======================================================================================================================
-- ==            Record the item for the payment of an automatic disbursement during post processing                    ==
-- =======================================================================================================================

-- ChangeLog
--   1/13/2001
--     Changed because the "first_payment_date", "first_payment_amount", and "last_payment_date" was moved
--     to the registers_client_creditor based upon first_payment and last_payment pointers.
--   9/31/2002
--     Added support for client_creditor_balances table
--   10/15/2002
--     Added support for client_creditor's "*_this_creditor" information
--   12/8/2002
--     Added support for check_payments
--   01/15/2002
--     Added creditor_classes table references and dropped "creditors.fee_creditor"
--   8/22/2004
--     Added reference to creditor_register

-- Do not generate intermediate result sets
set nocount on

declare @client_creditor_register	int
declare	@client_creditor_balance	int
declare	@zero_balance				bit
declare	@disbursement_factor		money
declare	@client						int
declare	@creditor					varchar(10)

select	@client						= client,
		@creditor					= creditor,
		@disbursement_factor		= disbursement_factor,
		@client_creditor_balance	= client_creditor_balance
from	client_creditor
where	client_creditor	= @client_creditor;

-- Determine if the creditor is a fee account
select	@zero_balance	= zero_balance
from	creditors cr
inner join creditor_classes ccl on cr.creditor_class = ccl.creditor_class
where	creditor	= @creditor

if @zero_balance is null
	select @zero_balance = 0

-- Mark the transaction as "processed" so that it is not processed in the future
UPDATE	disbursement_creditors
SET		trust_register			= @trust_register
where	client_creditor			= @client_creditor
and		disbursement_register	= @disbursement_register

-- If there is a deduct amount then update the deduct account with the amount.
-- We do this here rather than in "xpr_disbursement_deduct" to guarantee that it is done once and once only, no matter how many
-- times that the procedure "xpr_disbursement_deduct" is executed.
if (@fairshare_amt > 0) and (@creditor_type = 'D')
	update	clients
	set	held_in_trust	= held_in_trust + @fairshare_amt
	where	client		= 0

-- insert the transaction into the client_creditor table
insert into registers_client_creditor (tran_type, client, creditor, client_creditor, debit_amt, fairshare_amt, fairshare_pct, creditor_type, trust_register, disbursement_register, account_number, creditor_register)
values (@tran_type, @client, @creditor, @client_creditor, @debit_amt, @fairshare_amt, @fairshare_pct, @creditor_type, @trust_register, @disbursement_register, @account_number, @creditor_register)

select @client_creditor_register = SCOPE_IDENTITY()

-- If this is a fee creditor, increase the total interest to keep the balance at the same level
-- otherwise, just record the payment and let the debt balance go down.

if @zero_balance <> 0
begin
	if @tran_type = 'AD'
		update	client_creditor
		set	last_payment			= @client_creditor_register,
			first_payment			= isnull(first_payment, @client_creditor_register),
			sched_payment			= sched_payment - @debit_amt,
			interest_this_creditor	= isnull(interest_this_creditor,0) + @debit_amt,
			payments_this_creditor	= isnull(payments_this_creditor,0) + @debit_amt,
			check_payments			= isnull(check_payments,0) + 1
		where	client_creditor		= @client_creditor
	else
		update	client_creditor
		set	last_payment			= @client_creditor_register,
			first_payment			= isnull(first_payment, @client_creditor_register),
			sched_payment			= sched_payment - @debit_amt,
			interest_this_creditor	= isnull(interest_this_creditor,0) + @debit_amt,
			payments_this_creditor	= isnull(payments_this_creditor,0) + @debit_amt
		where	client_creditor		= @client_creditor

	update	client_creditor_balances
	set		payments_month_0		= payments_month_0 + @debit_amt,
			total_payments			= isnull(total_payments,0) + @debit_amt,
			total_interest			= isnull(total_interest,0) + @debit_amt
	from	client_creditor_balances
	where	client_creditor_balance	= @client_creditor_balance

end else begin

	if @tran_type = 'AD'
		update	client_creditor
		set	last_payment		= @client_creditor_register,
			first_payment		= isnull(first_payment, @client_creditor_register),
			sched_payment		= sched_payment - @debit_amt,
			payments_this_creditor	= isnull(payments_this_creditor,0) + @debit_amt,
			check_payments		= isnull(check_payments,0) + 1
		where	client_creditor		= @client_creditor
	else
		update	client_creditor
		set	last_payment		= @client_creditor_register,
			first_payment		= isnull(first_payment, @client_creditor_register),
			payments_this_creditor	= isnull(payments_this_creditor,0) + @debit_amt,
			sched_payment		= sched_payment - @debit_amt
		where	client_creditor		= @client_creditor

	update	client_creditor_balances
	set		payments_month_0		= payments_month_0 + @debit_amt,
			total_payments			= isnull(total_payments,0) + @debit_amt
	from	client_creditor_balances
	where	client_creditor_balance	= @client_creditor_balance

	-- If the balance is less than 2 months then flag the balance letter to be sent
	update	client_creditor_balances
	set		zero_bal_status	= 1
	from	client_creditor_balances bal
	where	client_creditor_balance = @client_creditor_balance
	and		zero_bal_status	= 0
	and		(@disbursement_factor * 2.0) >= (bal.orig_balance + bal.orig_balance_adjustment + bal.total_interest - bal.total_payments)
end

-- The result is the client/creditor record in the detail table
return ( @client_creditor_register )
GO
GRANT EXECUTE ON xpr_disbursement_cc_trans TO public AS dbo;
DENY EXECUTE ON xpr_disbursement_cc_trans TO www_role;
GO
