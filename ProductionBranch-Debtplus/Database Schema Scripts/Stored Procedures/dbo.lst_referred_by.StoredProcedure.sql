USE [DebtPlus]
GO
/****** Object:  StoredProcedure [dbo].[lst_referred_by]    Script Date: 09/15/2014 13:13:32 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[lst_referred_by] AS

-- ===================================================================================================
-- ==                Return the list of referral codes                                              ==
-- ===================================================================================================

SELECT	referred_by		as 'item_key',
	[description]		as 'description',
	[default]		as 'default',
	ActiveFlag
FROM	referred_by WITH ( NOLOCK )
ORDER BY 2

return ( @@rowcount )
GO
