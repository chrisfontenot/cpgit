SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER procedure [rpt_Transactions_CR] (@Creditor AS typ_creditor, @FromDate AS DateTime = NULL, @ToDate AS DateTime = NULL ) AS
-- ==============================================================================================================
-- ==               Retrieve the information for the given creditor                                            ==
-- ==============================================================================================================

-- Suppress intermediate result sets
SET NOCOUNT ON

IF @ToDate IS NULL
	SELECT @ToDate = getdate()

IF @FromDate IS NULL
	SELECT @FromDate = @ToDate

-- Adjust the dates for the proper settings
SELECT	@FromDate	= convert (datetime, convert(varchar(10), @FromDate, 101) + ' 00:00:00'),
	@ToDate		= convert (datetime, convert(varchar(10), @ToDate,   101) + ' 23:59:59')

-- Return the transactions for the request
SELECT		d.creditor_register								as 'creditor_register',
		d.tran_type									as 'tran_type',

		convert (datetime, convert(varchar(10), d.date_created, 101) + ' 00:00:00')	as 'item_date',
		tr.checknum									as 'checknum',
		convert (datetime, convert(varchar(10), tr.reconciled_date, 101) + ' 00:00:00')	as 'item_reconciled',

		d.credit_amt									as 'credit_amt',
		d.debit_amt									as 'debit_amt',
		right('00000000' + convert(varchar,d.invoice_register), 8)			as 'invoice',
		d.message									as 'message'

FROM		registers_creditor d WITH (NOLOCK)
LEFT OUTER JOIN	registers_trust tr WITH (NOLOCK) ON d.trust_register = tr.trust_register
WHERE		d.creditor = @creditor
AND		d.tran_type IS NOT NULL
AND		d.date_created BETWEEN @FromDate AND @ToDate

ORDER BY	d.date_created, d.tran_type

RETURN ( @@rowcount )
GO
