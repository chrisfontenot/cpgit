USE [DebtPlus]
GO
/****** Object:  StoredProcedure [dbo].[rpt_PendingAppointments_ByOffice]    Script Date: 09/15/2014 13:13:33 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[rpt_PendingAppointments_ByOffice]  ( @FromDate AS DateTime = NULL, @ToDate AS DateTime = NULL, @office as int = null ) AS
-- =============================================================================================
-- ==                      List of Pending Appointments by office                             ==
-- =============================================================================================

-- ChangeLog
--   1/6/2002
--     Added office_id field to allow for printing the counselor list at the end of the daily report
--   1/25/2003
--     Replaced home phone with callback phone if provided in the appointment
--   4/2/2003
--     Returned the "right" portion of the scheduler if this is a domain nanme
--     Use the message table for the confirmation status
--   6/20/2011
--     Added optional office parameter to filter by office

set nocount on

IF @ToDate IS NULL
	SELECT @ToDate = getdate()

IF @FromDate IS NULL
	SELECT @FromDate = @ToDate

-- Adjust the dates for the proper settings
select	@FromDate = convert (datetime, convert(varchar(10), @FromDate, 101) + ' 00:00:00'),
		@ToDate	  = convert (datetime, convert(varchar(10), @ToDate, 101)   + ' 23:59:59')

if @office is not null
	SELECT
			ap.office					as 'office_id', -- 1
			ap.client					as 'client_id', -- 2
			dbo.format_normal_name(pn.prefix,pn.first,pn.middle,pn.last,pn.suffix) as 'client_name', -- 3
			isnull(dbo.format_normal_name(default,cox.first,default,cox.last,default),convert(varchar,ap.counselor)) as 'counselor', -- 4
			isnull(ap.start_time, tm.start_time)		as 'time', -- 5
			o.name						as 'office', -- 6
			isnull(t.appt_name,'')				as 'appointment_type', -- 7

			isnull(dbo.Format_Phone_Number(ap.callback_ph), dbo.format_TelephoneNumber(c.HomeTelephoneID) ) as 'phone', -- 8

			case
				when ap.confirmation_status <= 0 then ''
				when m.description is not null then m.description
				when ap.confirmation_status = 1 then 'Agency'
				when ap.confirmation_status = 2 then 'Client'
				else 'Unknown'
			end					as 'confirmation_status', -- 9

			dbo.format_counselor_name(ap.created_by) as 'created_by' -- 10

	FROM		client_appointments ap	WITH (NOLOCK)
	INNER JOIN clients c			WITH (NOLOCK) ON ap.client	= c.client

	LEFT OUTER JOIN	people p		WITH (NOLOCK) ON ap.client	= p.client AND 1 = p.relation
	left outer join names pn with (nolock) on p.NameID = pn.Name
	LEFT OUTER JOIN	appt_times tm		WITH (NOLOCK) ON ap.appt_time	= tm.appt_time
	LEFT OUTER JOIN	appt_types t		WITH (NOLOCK) ON ap.appt_type	= t.appt_type
	LEFT OUTER JOIN	offices o		WITH (NOLOCK) ON ap.office	= o.office
	LEFT OUTER JOIN	counselors co		WITH (NOLOCK) ON ap.counselor	= co.counselor
	LEFT OUTER JOIN names cox               WITH (NOLOCK) ON co.NameID      = cox.name
	LEFT OUTER JOIN AppointmentConfirmationTypes m		WITH (NOLOCK) ON ap.confirmation_status = m.oID

	WHERE		ap.status in ( 'P', 'K', 'M', 'W' )
	AND			ap.workshop IS NULL
	AND			ap.start_time BETWEEN @FromDate AND @ToDate
	and			ap.office = @office

	ORDER BY	1, 5 -- ap.office, ap.start_time

else

	SELECT
			ap.office					as 'office_id', -- 1
			ap.client					as 'client_id', -- 2
			dbo.format_normal_name(pn.prefix,pn.first,pn.middle,pn.last,pn.suffix) as 'client_name', -- 3
			isnull(dbo.format_normal_name(default,cox.first,default,cox.last,default),convert(varchar,ap.counselor)) as 'counselor', -- 4
			isnull(ap.start_time, tm.start_time)		as 'time', -- 5
			o.name						as 'office', -- 6
			isnull(t.appt_name,'')				as 'appointment_type', -- 7

			isnull(dbo.Format_Phone_Number(ap.callback_ph), dbo.format_TelephoneNumber(c.HomeTelephoneID) ) as 'phone', -- 8

			case
				when ap.confirmation_status <= 0 then ''
				when m.description is not null then m.description
				when ap.confirmation_status = 1 then 'Agency'
				when ap.confirmation_status = 2 then 'Client'
				else 'Unknown'
			end					as 'confirmation_status', -- 9

			dbo.format_counselor_name(ap.created_by) as 'created_by' -- 10

	FROM		client_appointments ap	WITH (NOLOCK)
	INNER JOIN clients c			WITH (NOLOCK) ON ap.client	= c.client

	LEFT OUTER JOIN	people p		WITH (NOLOCK) ON ap.client	= p.client AND 1 = p.relation
	left outer join names pn with (nolock) on p.NameID = pn.Name
	LEFT OUTER JOIN	appt_times tm		WITH (NOLOCK) ON ap.appt_time	= tm.appt_time
	LEFT OUTER JOIN	appt_types t		WITH (NOLOCK) ON ap.appt_type	= t.appt_type
	LEFT OUTER JOIN	offices o		WITH (NOLOCK) ON ap.office	= o.office
	LEFT OUTER JOIN	counselors co		WITH (NOLOCK) ON ap.counselor	= co.counselor
	LEFT OUTER JOIN names cox               WITH (NOLOCK) ON co.NameID      = cox.name
	LEFT OUTER JOIN AppointmentConfirmationTypes m		WITH (NOLOCK) ON ap.confirmation_status = m.oID

	WHERE		ap.status in ( 'P', 'K', 'M', 'W' )
	AND			ap.workshop IS NULL
	AND			ap.start_time BETWEEN @FromDate AND @ToDate

	ORDER BY	1, 5 -- ap.office, ap.start_time

RETURN ( @@rowcount )
GO
