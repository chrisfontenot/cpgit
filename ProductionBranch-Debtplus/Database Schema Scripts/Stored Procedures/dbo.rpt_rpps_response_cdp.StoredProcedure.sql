USE [DebtPlus]
GO
/****** Object:  StoredProcedure [dbo].[rpt_rpps_response_cdp]    Script Date: 09/15/2014 13:13:33 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE PROCEDURE [dbo].[rpt_rpps_response_cdp] ( @rpps_response_file as int = null ) AS

-- =======================================================================================================
-- ==           Obtain the information for the response conditions of proposals which never made it     ==
-- ==           to the creditor because they failed to pass Mastercard's validation routines.           ==
-- =======================================================================================================

-- ChangeLog
--   2/18/2004
--      Added client's counselor name
--   3/14/2003
--     Allow for more than 10,000 creditors in a class
--   1/5/2009
--      Changed from using biller_aba to using rpps_biller_id

-- Suppress the count of records
set nocount on

-- Try to find the response file if one is not specified
if @rpps_response_file is null
begin
	select	@rpps_response_file = max(rpps_response_file)
	from	rpps_response_files
	where	date_created = (
		select	max(date_created)
		from	rpps_response_files
	)
end

select		isnull(tr.client,cdp.client)							as 'client', -- 1
		coalesce('['+cr.creditor+'] ',d.rpps_biller_id+' ','')				as 'creditor', -- 2
		coalesce(cr.creditor_name, ix1.biller_name, 'UNKNOWN CREDITOR') as 'creditor_name', -- 3
		dbo.format_normal_name (pn.prefix,pn.first,pn.middle,pn.last,pn.suffix)		as 'client_name', -- 4
		d.rpps_biller_id								as 'biller_id', -- 5
		d.account_number								as 'account_number', -- 6

		pr.proposed_amount								as 'proposed_amount', -- 7
		convert(datetime, convert(varchar, pr.proposed_start_date,101) + ' 00:00:00')	as 'proposed_start_date', -- 8

		coalesce(d.processing_error,rpse.description,pre.description,'UNKNOWN REASON')	as 'response_reason', -- 9

		d.trace_number									as 'trace_number', -- 10
		convert(varchar(80), dbo.format_normal_name(default,cox.first,default,cox.last,default)) as 'counselor_name', -- 11
		
		cr.type, -- 12
		cr.creditor_id -- 13

from		rpps_response_details d		with (nolock)
left outer join rpps_response_details_cdp cdp	with (nolock) on d.rpps_response_detail		= cdp.rpps_response_detail
left outer join	rpps_biller_ids ix1		with (nolock) on d.rpps_biller_id		= ix1.rpps_biller_id
left outer join rpps_transactions tr		with (nolock) on d.rpps_transaction		= tr.rpps_transaction
left outer join client_creditor_proposals pr	with (nolock) on tr.client_creditor_proposal	= pr.client_creditor_proposal
left outer join client_creditor cc		with (nolock) on pr.client_creditor		= cc.client_creditor
left outer join creditors cr			with (nolock) on cc.creditor			= cr.creditor
left outer join rpps_reject_codes rpse		with (nolock) on d.return_code			= rpse.rpps_reject_code
left outer join proposal_result_reasons pre	with (nolock) on pr.proposal_reject_reason	= pre.proposal_result_reason
left outer join people p			with (nolock) on isnull(tr.client,cdp.client)	= p.client and 1 = p.relation
left outer join names pn with (nolock) on p.nameid = pn.name
left outer join clients c			with (nolock) on cc.client = c.client
left outer join counselors co			with (nolock) on c.counselor = co.counselor
left outer join names cox with (nolock) on co.NameID = cox.name

where		d.rpps_response_file = @rpps_response_file
and		d.service_class_or_purpose = 'CDP'
order by	12, 13, 2, 6

return ( @@rowcount )
GO
