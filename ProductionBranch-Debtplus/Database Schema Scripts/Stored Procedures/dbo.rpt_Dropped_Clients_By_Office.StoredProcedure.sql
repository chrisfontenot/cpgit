USE [DebtPlus]
GO
/****** Object:  StoredProcedure [dbo].[rpt_Dropped_Clients_By_Office]    Script Date: 09/15/2014 13:13:33 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[rpt_Dropped_Clients_By_Office] ( @FromDate as datetime, @ToDate as datetime ) as

-- ===============================================================================================
-- ==            Return the information for the drop reasons based upon the result              ==
-- ===============================================================================================

set nocount on

if @ToDate is null
	select	@ToDate = getdate()

if @FromDate is null
	select	@FromDate = @ToDate

select	@FromDate = convert(datetime, convert(varchar, @FromDate, 101) + ' 00:00:00'),
	@ToDate	  = convert(datetime, convert(varchar, @ToDate, 101) + ' 23:59:59')

-- Return the results
select	c.drop_date,
	o.name as 'office',
	c.client as client,
	dbo.format_normal_name (pn.prefix, pn.first, pn.middle, pn.last, pn.suffix) as 'client_name',

	case
		when d.nfcc is null then 'OTHER'
		when d.nfcc = 'SC1' then 'SC'
		when d.nfcc = 'SC2' then 'SA'
		when d.nfcc = 'DP1' then 'NP'
		when d.nfcc = 'DP2' then 'NP'
		when d.nfcc = 'DP3' then 'NP'
		when d.nfcc = 'DP4' then 'NP'
		when d.nfcc like 'DP5%' then 'BK'
		else 'OTHER'
	end	as drop_reason

from	clients c		with (nolock)
left outer join people p	with (nolock) on c.client = p.client and 1 = p.relation
left outer join names pn with (nolock) on p.nameid = pn.name
left outer join offices o	with (nolock) on c.office = o.office
left outer join drop_reasons d	with (nolock) on c.drop_reason = d.drop_reason

where	c.drop_date between @FromDate and @ToDate
and	c.active_status = 'I'

order by 5, c.office, c.drop_date, c.client -- "5" = drop reason

return ( @@rowcount )
GO
