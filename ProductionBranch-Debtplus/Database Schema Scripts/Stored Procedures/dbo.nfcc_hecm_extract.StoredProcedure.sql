USE [DebtPlus]
GO
/****** Object:  StoredProcedure [dbo].[nfcc_hecm_extract]    Script Date: 09/15/2014 13:13:32 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[nfcc_hecm_extract]  ( @From_Date as datetime, @To_Date as datetime ) WITH RECOMPILE as

declare	@NFCC_ID	varchar(50)
select	@NFCC_ID	= nfcc_unit_no
from	config

select	convert(varchar(50),'') as CsnlrLogin,
	convert(varchar(50),'') as AgencyName,
	convert(varchar(50),'') as DateCertIssued,
	convert(varchar(50),'') as Clientgndr,
	convert(varchar(50),'') as ClientFirst,
	convert(varchar(50),'') as ClientLast,

	convert(varchar(50),'') as Address1,
	convert(varchar(50),'') as City,
	convert(varchar(50),'') as State,
	convert(varchar(50),'') as Zipcode,

	convert(varchar(50),'') as MailAddress1,
	convert(varchar(50),'') as MailCity,
	convert(varchar(50),'') as MailState,
	convert(varchar(50),'') as MailZipcode,

	convert(varchar(50),'') as CoOwner_gender,
	convert(varchar(50),'') as CoOwnerFirst,
	convert(varchar(50),'') as CoOwnerLast,

	convert(varchar(50),'') as Third_gender,
	convert(varchar(50),'') as ThirdFirst,
	convert(varchar(50),'') as ThirdLast,
	convert(varchar(50),'') as ThirdAddress1,
	convert(varchar(50),'') as ThirdCity,
	convert(varchar(50),'') as ThirdState,
	convert(varchar(50),'') as ThirdZipcode,

	convert(varchar(50), '') as ethnicity,
	convert(varchar(50), '') as race,
	convert(varchar(50), '') as family_size,
	convert(varchar(50), '') as monthly_income,
	convert(varchar(50), '') as foreclosure,
	convert(varchar(50), 'counseled on HECM') as seeking_help,
	convert(varchar(50), '') as Other,
	convert(varchar(50), '') as SessionTime,

	client,
	max(client_appointment) as client_appointment

into	#clients
from	client_appointments
where	appt_type in (13,14)
and	start_time between @From_Date and @To_Date
and	status in ('K','W')
group by client;

update	#clients
set	DateCertIssued	= right('00' + convert(varchar,datepart(d,ca.start_time)),2) + '/' + right('00' + convert(varchar,datepart(month,ca.start_time)),2)+ '/' + right('0000' + convert(varchar,datepart(year,ca.start_time)),4),
	SessionTime     = convert(varchar, datediff(minute, ca.start_time, ca.end_time))
from	#clients c
inner join client_appointments ca on c.client_appointment = ca.client_appointment;

-- Counselor id
update	#clients
set	CsnlrLogin	= dbo.format_counselor_name ( co.person )
from	#clients c
inner join clients cl on c.client = cl.client
inner join counselors co on cl.counselor = co.counselor
where	co.person is not null;

declare	@AgencyName	varchar(50)
select	@AgencyName	= left(replace(name,' ',''),50)
from	config

update	#clients
set	AgencyName	= @AgencyName
where	AgencyName	= ''

-- Applicant information
update	#clients
set	ClientFirst	= dbo.remove_pattern ( pn.first, '%[^A-Z]%', 1 ),
	ClientLast	= dbo.remove_pattern ( ltrim(rtrim(isnull(pn.last,'') + isnull(' ' + pn.suffix,''))), '%[^A-Z ]%', 1 ),
	Clientgndr	= case p.gender when 1 then 'Male' else 'Female' end,
	ethnicity	= case p.ethnicity when 0 then 'Non-Hispanic' else 'Hispanic' end,
	race		= case p.race
				when 1 then 'Black or African American'
				when 2 then 'Asian'
				when 3 then 'White'
				when 4 then 'Hispanic'
				when 5 then 'Other Multiple'
				when 6 then 'Other Multiple'
				when 7 then 'American_Indian/African American'
				when 8 then 'Other Multiple'
				when 9 then 'American Indian'
				when 11 then 'Hawaiian or Other Pacific Islander'
				when 12 then 'American_Indian'
				when 13 then 'Asian'
				when 14 then 'African American'
				else 'Unspecified'
			end
from	#clients c
inner join people p on c.client = p.client and 1 = p.relation
left outer join names pn with (nolock) on p.nameid = pn.name

-- CoApplicant information
update	#clients
set	CoOwnerFirst	= dbo.remove_pattern ( pn.first, '%[^A-Z]%', 1 ),
	CoOwnerLast	= dbo.remove_pattern ( ltrim(rtrim(isnull(pn.last,'') + isnull(' ' + pn.suffix,''))), '%[^A-Z ]%', 1 ),
	CoOwner_gender	= case gender when 1 then 'Male' else 'Female' end
from	#clients c
inner join people p on c.client = p.client and 1 <> p.relation
left outer join names pn with (nolock) on p.nameid = pn.name

-- Client information
update	#clients
set	address1	= left(isnull(dbo.format_address_line_1(ca.house,ca.direction,ca.street,ca.suffix,ca.modifier,ca.modifier_value),'') + isnull(' ' + ca.address_line_2, ''),50),
	city		= isnull(ca.city,''),
	state		= isnull(left(st.mailingcode,2),''),
	Zipcode		= case when left(isnull(ca.postalcode,'000000000'),5) = '00000' then '' else left(c.postalcode,5) end,
	family_size	= convert(varchar(50), c.dependents + (case c.marital_status when 2 then 2 else 1 end))
from	#clients a
inner join clients c on a.client = c.client
left outer join addresses ca on c.addressid = ca.address
left outer join states st on c.state = st.state;

/* -- Do not duplicate the address as the mailing address
update	#clients
set	MailAddress1	= Address1,
	MailCity	= City,
	MailState	= State,
	MailZipcode	= Zipcode;
*/

-- Income levels
create table #incomes (client int, income money);

insert into #incomes (client, income)
select	c.client, sum(gross_income)
from	people p
inner join #clients c on p.client = c.client
group by c.client

insert into #incomes (client, income)
select	c.client, asset_amount
from	assets a
inner join #clients c on a.client = c.client;

select	client, sum(income) as income
into	#income2
from	#incomes
group by client;

update	#clients
set	monthly_income	= convert(varchar, convert(int,round(income,0)))
from	#income2 i
inner join #clients c on i.client = c.client;

-- Find the HUD results for these clients
select	h.client, max(h.hud_interview) as hud_interview, convert(int,0) as hud_result
into	#hud_interviews
from	hud_interviews h
inner join #clients c on h.client = c.client
where	isnull(h.hud_result,0) > 0
group by h.client;

update	#hud_interviews
set	hud_result	= h.hud_result
from	#hud_interviews i
inner join hud_interviews h on i.hud_interview = h.hud_interview;

update	#clients
set		seeking_help	= m.description
from	#clients c
inner join #hud_interviews i on c.client = i.client
inner join Housing_ResultTypes m on i.hud_result = m.oID

-- Generate the output table
truncate table Temp_nfcc_HECM_extract

insert into Temp_nfcc_HECM_extract (NFCC_ID,CsnlrLogin,AgencyName,DateCertIssued,Clientgndr,ClientFirst,ClientLast,Address1,City,State,Zipcode,MailAddress1,MailCity,MailState,MailZipcode,CoOwner_gender,CoOwnerFirst,CoOwnerLast,Third_gender,ThirdFirst,ThirdLast,ThirdAddress1,ThirdCity,ThirdState,ThirdZipcode, ethnicity,race,family_size,monthly_income,foreclosure,seeking_help,Other,SessionTime)
select @nfcc_id as NFCC_ID,CsnlrLogin,AgencyName,DateCertIssued,Clientgndr,ClientFirst,ClientLast,Address1,City,State,Zipcode,MailAddress1,MailCity,MailState,MailZipcode,CoOwner_gender,CoOwnerFirst,CoOwnerLast,Third_gender,ThirdFirst,ThirdLast,ThirdAddress1,ThirdCity,ThirdState,ThirdZipcode, ethnicity,race,family_size,monthly_income,foreclosure,seeking_help,Other,SessionTime
-- into Temp_nfcc_HECM_extract
from #clients
order by client;

drop table #hud_interviews
drop table #clients
drop table #incomes
drop table #income2

return ( 0 )
GO
