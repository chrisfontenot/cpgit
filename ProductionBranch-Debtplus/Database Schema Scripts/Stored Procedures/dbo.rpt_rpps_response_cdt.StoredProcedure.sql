USE [DebtPlus]
GO
/****** Object:  StoredProcedure [dbo].[rpt_rpps_response_cdt]    Script Date: 09/15/2014 13:13:33 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[rpt_rpps_response_cdt] ( @rpps_response_file as int = null ) AS

-- =======================================================================================================
-- ==           Obtain the information for the response conditions of error payments                    ==
-- =======================================================================================================

-- ChangeLog
--   2/18/2004
--     Added client's counselor name.
--   1/5/2009
--      Changed from using biller_aba to using rpps_biller_id

-- Suppress the count of records
set nocount on

-- Try to find the response file if one is not specified
if @rpps_response_file is null
begin
	select	@rpps_response_file = max(rpps_response_file)
	from	rpps_response_files
	where	date_created = (
		select	max(date_created)
		from	rpps_response_files
	)
end

select		r.rpps_biller_id							as 'biller_id',
		isnull(x1.biller_name,'UNKNOWN CREDITOR')		as 'creditor_name',
		isnull(rt.client,cdt.client)						as 'client',
		cdt.ssn									as 'ssn',
		dbo.format_normal_name (default, pn.first, pn.middle, pn.last, default)	as 'client_name',
		r.account_number							as 'account_number',
		coalesce(r.processing_error, rej.description, rt.return_code, '')	as 'error_code',
		r.trace_number								as 'trace_number',
		convert(varchar(80), dbo.format_normal_name(default,cox.first,default,cox.last,default))						as 'counselor_name'

from		rpps_response_details r		with (nolock)
left outer join	rpps_transactions rt		with (nolock) on r.rpps_transaction = rt.rpps_transaction
left outer join rpps_response_details_cdt cdt	with (nolock) on r.rpps_response_detail = cdt.rpps_response_detail
left outer join rpps_biller_ids x1		with (nolock) on r.rpps_biller_id = x1.rpps_biller_id
left outer join	people p			with (nolock) ON isnull(rt.client,cdt.client)=p.client AND 1=p.relation
left outer join names pn with (nolock) on p.NameID = pn.Name
left outer join	rpps_reject_codes rej		with (nolock) ON rt.return_code = rej.rpps_reject_code
left outer join clients c			with (nolock) on p.client = c.client
left outer join counselors co			with (nolock) on c.counselor = co.counselor
left outer join names cox with (nolock) on co.NameID = cox.name

where		r.rpps_response_file		= @rpps_response_file
and		r.service_class_or_purpose	in ('CDD','CDT')
and		r.transaction_code		= 22

order by	2, 6		-- creditor name, account number

return ( @@rowcount )
GO
