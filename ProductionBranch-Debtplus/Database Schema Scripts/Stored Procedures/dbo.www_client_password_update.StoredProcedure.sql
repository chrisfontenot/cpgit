USE [DebtPlus]
GO
/****** Object:  StoredProcedure [dbo].[www_client_password_update]    Script Date: 09/15/2014 13:13:34 ******/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE PROCEDURE [dbo].[www_client_password_update] ( @client as int, @old_password as varchar(80) = NULL, @new_password as varchar(80) = NULL ) AS
-- =========================================================================================================
-- ==            Update the password for the client                                                       ==
-- =========================================================================================================

-- Suppress intermediate results
set nocount on

-- Result for the function
-- 0 : failure
-- <> 0 : success
declare	@result	int
select	@result = -1

-- The client must exist in the system
if not exists (select client from clients where client = @client)
	select	@result = 0

-- Lock the tables for a moment while we update the database
begin transaction

-- Find the existing row with the password information
declare	@pkid				uniqueidentifier
declare	@current_password	varchar(80)

select	@pkid				= pkid,
		@current_password	= password
from	client_www with (nolock)
where	username			= convert(varchar,@client);

-- Detect differences beween NULL items
if @old_password is null and @current_password is not null
	select	@result = 0
else if @current_password is null and @old_password is not null
	select	@result = 0

-- Compare the password settings
if @current_password <> @old_password
	select	@result = 0

-- If the client does not exist in the database then add it
if @pkid is null and @result < 0
begin
	select	@pkid		= newid()
	insert into client_www (pkid, username, password) select @pkid, convert(varchar,@client), @new_password
	select	@result = 1
end

-- Finally update the database with the new password
if @result < 0
begin
	update	client_www
	set		[password]  = @new_password,
			LastPasswordChangeDate = getutcdate()
	where	pkid		= @pkid
	select	@result = @@rowcount
end

commit transaction
return ( @result )
GO
