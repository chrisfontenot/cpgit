USE [DebtPlus]
GO
/****** Object:  StoredProcedure [dbo].[xpr_drop_debt]    Script Date: 09/15/2014 13:13:35 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[xpr_drop_debt] ( @client_creditor as int) as
-- =====================================================================================================
-- ==    Called when the debt is dropped. It puts the drop notice into the pending file               ==
-- ==    so that it will go out with the next proposal batch.                                         ==
-- =====================================================================================================

-- ChangeLog
--   1/5/2011
--      initial coding

set nocount on

-- Create the drop notices
insert into debt_notes (type, client, creditor, client_creditor, note_text, drop_reason)
select 'DR'					as  'type',
		cc.client			as  'client',
		cc.creditor			as  'creditor',
		cc.client_creditor	as  'client_creditor',
		convert(varchar(1024), case when cc.drop_reason is not null then 'We have discontinued the client' + isnull(' because ' + dr.description, '') else isnull(c.drop_reason_other,'We have discontinued the client') end) as  'note_text',
		cc.drop_reason		as  'drop_reason'
from	client_creditor cc with (nolock)
inner join creditors cr with (nolock)					on cc.creditor = cr.creditor
inner join client_creditor_balances bal with (nolock)	on cc.client_creditor_balance = bal.client_creditor_balance and bal.client_creditor = cc.client_creditor
inner join clients c with (nolock)						on cc.client = c.client
left outer join drop_reasons dr with (nolock)			on cc.drop_reason = dr.drop_reason
where	cc.client_creditor = @client_creditor
and	    cc.reassigned_debt = 0

-- Update the record
update	client_creditor
set		drop_date			= getdate()
where	client_creditor		= @client_creditor;

return ( @@rowcount )
GO
