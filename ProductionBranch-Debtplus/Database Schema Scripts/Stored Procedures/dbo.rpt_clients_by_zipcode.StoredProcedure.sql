USE [DebtPlus]
GO
/****** Object:  StoredProcedure [dbo].[rpt_clients_by_zipcode]    Script Date: 09/15/2014 13:13:32 ******/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE PROCEDURE [dbo].[rpt_clients_by_zipcode] AS
-- =====================================================================================================
-- ==            Return the list of clients by zipcode                                                ==
-- =====================================================================================================

-- Suppress intermediate results
set nocount on

-- Build a list of the client count by zipcode and active status
create table #zipcodes (office int null, office_name varchar(80) null, active_status varchar(10) null, zipcode varchar(20) null, cnt int null);

-- Build a list by active status for each office
insert into #zipcodes (active_status, zipcode, cnt)
select	c.active_status, left(a.postalcode,5), count(*)
from	clients c
left outer join addresses a on c.addressid = a.address
left outer join states st on a.state = st.state
Where	a.state is null OR st.USAFormat = 1
group by c.active_status, left(a.postalcode,5);

-- Update the office information from the system
update #zipcodes
set office = (select top 1 office from zipcodes where #zipcodes.zipcode+'0000' between zip_lower and zip_upper order by convert(int,zip_upper)-convert(int,zip_lower))

update	#zipcodes
set		office_name	= o.name
from	#zipcodes z
inner join offices o on z.office = o.office;

-- Form a result from the information with the active status to the right
select distinct office, office_name, zipcode, convert(int,0) as cre, convert(int,0) as apt, convert(int,0) as wks, convert(int,0) as pnd, convert(int,0) as rdy, convert(int,0) as pro, convert(int,0) as a, convert(int,0) as ar, convert(int,0) as ex, convert(int,0) as i
into	#results
from	#zipcodes
group by office, office_name, zipcode;

-- Update the figures for the counts
update	#results set cre = b.cnt from #results a inner join #zipcodes b on a.office = b.office and a.zipcode = b.zipcode where b.active_status = 'CRE';
update	#results set apt = b.cnt from #results a inner join #zipcodes b on a.office = b.office and a.zipcode = b.zipcode where b.active_status = 'APT';
update	#results set wks = b.cnt from #results a inner join #zipcodes b on a.office = b.office and a.zipcode = b.zipcode where b.active_status = 'WKS';
update	#results set rdy = b.cnt from #results a inner join #zipcodes b on a.office = b.office and a.zipcode = b.zipcode where b.active_status = 'RDY';
update	#results set pro = b.cnt from #results a inner join #zipcodes b on a.office = b.office and a.zipcode = b.zipcode where b.active_status = 'PRO';
update	#results set pnd = b.cnt from #results a inner join #zipcodes b on a.office = b.office and a.zipcode = b.zipcode where b.active_status = 'PND';
update	#results set a   = b.cnt from #results a inner join #zipcodes b on a.office = b.office and a.zipcode = b.zipcode where b.active_status = 'A';
update	#results set ar  = b.cnt from #results a inner join #zipcodes b on a.office = b.office and a.zipcode = b.zipcode where b.active_status = 'AR';
update	#results set ex  = b.cnt from #results a inner join #zipcodes b on a.office = b.office and a.zipcode = b.zipcode where b.active_status = 'EX';
update	#results set i   = b.cnt from #results a inner join #zipcodes b on a.office = b.office and a.zipcode = b.zipcode where b.active_status = 'I';

-- Return the results
select * from #results order by office, zipcode;

-- Discard working tables
drop table #results
drop table #zipcodes

return ( @@rowcount )
GO
