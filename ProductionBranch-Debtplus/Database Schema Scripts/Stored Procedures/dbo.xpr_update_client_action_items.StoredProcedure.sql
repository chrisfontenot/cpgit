USE [DebtPlus]
GO
/****** Object:  StoredProcedure [dbo].[xpr_update_client_action_items]    Script Date: 09/15/2014 13:13:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[xpr_update_client_action_items](@action_plan as int, @action_item as int, @checked as bit = 0) as
    -- =========================================================================================
    -- ==        Update an action_items item                                                  ==
    -- =========================================================================================

    declare @client_action_item int
    select  @client_action_item = client_action_item
    from    client_action_items
    where   action_plan = @action_plan
    and     action_item = @action_item
    
    if @client_action_item is null
    begin
        insert into client_action_items (action_plan, action_item, checked) values (@action_plan, @action_item, @checked)
        select  @client_action_item = scope_identity()
    end else
        update client_action_items set checked = @checked WHERE client_action_item = @client_action_item
    
    return ( @client_action_item )
GO
