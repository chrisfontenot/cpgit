USE [DebtPlus]
GO
/****** Object:  StoredProcedure [dbo].[rpt_ClientPhoneList_By_DMP_Status_Date]    Script Date: 09/15/2014 13:13:32 ******/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE PROCEDURE [dbo].[rpt_ClientPhoneList_By_DMP_Status_Date] ( @FromDate as datetime = null, @ToDate as datetime = null ) AS

-- ====================================================================================================================
-- ==            Return the list of clients for contacting them based on the DMP status date.                        ==
-- ==            Created a stored procedure because then they didn't have to enter a time value.                     ==
-- ====================================================================================================================

-- Setup
set nocount on
if @ToDate is null
	select	@ToDate = getdate()

if @FromDate is null
	select	@FromDate = @ToDate

select	@ToDate = convert(datetime, convert(varchar(10), @ToDate, 101) + ' 00:00:00'),
	@FromDate = convert(datetime, convert(varchar(10), @FromDate, 101) + ' 23:59:59')

select	c.client					as client,
	dbo.format_reverse_name(default,p1n.first,p1n.middle,p1n.last,p1n.suffix)	as name_1,
	dbo.format_reverse_name(default,p2n.first,p2n.middle,p2n.last,p2n.suffix)	as name_2,

	dbo.format_TelephoneNumber ( c.HomeTelephoneID )		as home_ph,

	isnull(p1e.Address,'')				as email,

	a.city						as city

from	clients c with (nolock)
left outer join addresses a with (nolock) on c.AddressID = a.address
left outer join people p1 on c.client = p1.client and 1 = p1.relation
left outer join names p1n with (nolock) on p1.NameID = p1n.Name
left outer join emailaddresses p1e with (nolock) on p1.emailid = p1e.Email
left outer join people p2 on c.client = p2.client and 1 <> p2.relation
left outer join names p2n with (nolock) on p2.NameID = p2n.Name

where	c.client_status = 'DMP'
and	c.dmp_status_date between @FromDate and @ToDate

order by 2, 3, 1 -- name1, name2, client

return ( @@rowcount )
GO
