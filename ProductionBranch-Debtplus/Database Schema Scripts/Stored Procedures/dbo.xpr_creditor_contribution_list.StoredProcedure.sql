SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER procedure [xpr_creditor_contribution_list] ( @creditor as varchar(10), @show_all as int = 0 ) as

-- =========================================================================================================
-- ==            Retrieve the list of creditor invoices for the creditor                                  ==
-- =========================================================================================================

if @show_all = 0
	SELECT	r.invoice_register as 'invoice', 
		t.checknum as 'checknum', 
		r.inv_date as 'inv_date', 
		isnull(r.inv_amount,0) as 'inv_amount', 
		r.pmt_date as 'pmt_date', 
		isnull(r.pmt_amount,0) as 'pmt_amount', 
		r.adj_date as 'adj_date', 
		isnull(r.adj_amount,0) as 'adj_amount' 

	FROM		registers_invoices r WITH (NOLOCK) 
	RIGHT OUTER JOIN registers_creditor rc WITH (NOLOCK) ON rc.invoice_register = r.invoice_register AND rc.tran_type in ('AD','MD','CM','BW')
	LEFT OUTER JOIN	registers_trust t WITH (NOLOCK) ON rc.trust_register=t.trust_register

	WHERE r.creditor = @creditor
	AND   r.inv_amount > (r.pmt_amount + r.adj_amount)
	order by 3, 1, 2

else

	SELECT	r.invoice_register as 'invoice', 
		t.checknum as 'checknum', 
		r.inv_date as 'inv_date', 
		isnull(r.inv_amount,0) as 'inv_amount', 
		r.pmt_date as 'pmt_date', 
		isnull(r.pmt_amount,0) as 'pmt_amount', 
		r.adj_date as 'adj_date', 
		isnull(r.adj_amount,0) as 'adj_amount' 

	FROM		registers_invoices r WITH (NOLOCK) 
	RIGHT OUTER JOIN registers_creditor rc WITH (NOLOCK) ON rc.invoice_register = r.invoice_register AND rc.tran_type in ('AD','MD','CM','BW')
	LEFT OUTER JOIN	registers_trust t WITH (NOLOCK) ON rc.trust_register=t.trust_register
	WHERE r.creditor = @creditor
	order by 3, 1, 2

return ( @@rowcount )
GO
