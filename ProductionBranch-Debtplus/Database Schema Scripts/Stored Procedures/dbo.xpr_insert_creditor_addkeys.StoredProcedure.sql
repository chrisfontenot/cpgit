USE [DebtPlus]
GO
/****** Object:  StoredProcedure [dbo].[xpr_insert_creditor_addkeys]    Script Date: 09/15/2014 13:13:35 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[xpr_insert_creditor_addkeys] ( @creditor typ_creditor, @type varchar(1) = 'A', @additional varchar(50) = null) as
	insert into creditor_addkeys (creditor, type, additional) values (@creditor, @type, @additional)
	return ( scope_identity() )
GO
