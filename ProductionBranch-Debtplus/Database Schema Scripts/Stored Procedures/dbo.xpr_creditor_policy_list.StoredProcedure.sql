USE [DebtPlus]
GO
/****** Object:  StoredProcedure [dbo].[xpr_creditor_policy_list]    Script Date: 09/15/2014 13:13:34 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[xpr_creditor_policy_list] ( @creditor as varchar(10) ) as

SELECT	v.policy_matrix_policy,
	t.policy_matrix_policy_type,
	t.description,
	v.changed_date,
	v.item_value
FROM	policy_matrix_policy_types t
LEFT OUTER JOIN policy_matrix_policies v ON t.policy_matrix_policy_type = v.policy_matrix_policy_type
where	v.creditor = @creditor
ORDER BY t.description

return ( @@rowcount )
GO
