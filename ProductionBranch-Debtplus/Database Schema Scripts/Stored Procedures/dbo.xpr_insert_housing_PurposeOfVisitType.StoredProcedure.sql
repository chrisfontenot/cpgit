USE [DebtPlus]
GO
/****** Object:  StoredProcedure [dbo].[xpr_insert_housing_PurposeOfVisitType]    Script Date: 09/15/2014 13:13:35 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[xpr_insert_housing_PurposeOfVisitType] ( @description varchar(50), @hud_9902_section varchar(10) = null, @HourlyGrantAmountUsed money = 0, @ActiveFlag bit = 1, @Default bit = 0, @HousingFeeAmount money = 0 ) as
insert into housing_PurposeOfVisitTypes ( [description], [hud_9902_section], [HourlyGrantAmountUsed], [ActiveFlag], [Default], [HousingFeeAmount]) values (@description, @hud_9902_section, @HourlyGrantAmountUsed, @ActiveFlag, @Default, @HousingFeeAmount)
return ( scope_identity() )
GO
