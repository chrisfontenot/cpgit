USE [DebtPlus]
GO
/****** Object:  StoredProcedure [dbo].[www_creditor_note_remove]    Script Date: 09/15/2014 13:13:34 ******/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE PROCEDURE [dbo].[www_creditor_note_remove] ( @creditor as typ_creditor, @item_key as int ) as
-- =========================================================================================
-- ==            Mark the note as viewed by the creditor                                  ==
-- =========================================================================================
set nocount on

-- Mark the record as being viewed
update	creditor_www_notes
set	date_viewed		= getdate()
where	creditor		= @creditor
and	creditor_www_note	= @item_key

return ( @@rowcount )
GO
