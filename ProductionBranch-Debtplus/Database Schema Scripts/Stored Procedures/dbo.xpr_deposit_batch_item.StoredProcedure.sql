USE [DebtPlus]
GO
/****** Object:  StoredProcedure [dbo].[xpr_deposit_batch_item]    Script Date: 09/15/2014 13:13:35 ******/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE PROCEDURE [dbo].[xpr_deposit_batch_item] ( @Transaction AS INT ) AS
-- =====================================================================================================
-- ==                  Fetch the current row from the database                                        ==
-- =====================================================================================================

SELECT	client				as 'deposit_client',
	amount				as 'amount',
	tran_subtype			as 'subtype',
	date_created			as 'date_created',
	item_date			as 'item_date',
	reference			as 'reference',
	scanned_client			as 'scanned_client',
	ok_to_post			as 'ok_to_post',
	message				as 'message'
FROM	deposit_batch_details WITH (NOLOCK)
WHERE	deposit_batch_detail	= @Transaction

-- Return the number of rows retrieved
RETURN ( @@rowcount )
GO
