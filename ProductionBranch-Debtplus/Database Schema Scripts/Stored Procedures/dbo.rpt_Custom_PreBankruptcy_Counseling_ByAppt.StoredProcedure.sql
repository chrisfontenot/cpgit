USE [DebtPlus]
GO
/****** Object:  StoredProcedure [dbo].[rpt_Custom_PreBankruptcy_Counseling_ByAppt]    Script Date: 09/15/2014 13:13:33 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[rpt_Custom_PreBankruptcy_Counseling_ByAppt]  ( @From_Date as datetime = null, @To_Date as datetime = null )  AS 

-- ===========================================================================================
-- ==            List clients, appointments, etc. for bankruptcy information                ==
-- ===========================================================================================

-- ChangeLog
--   Special 
--     - Removed name from report
--     - Added support for indicators in listing
--   10/18/06
--     - Changed for dynamic "method of first contact" values

if @To_Date is null
	select	@To_date	= getdate()

IF @From_Date is null
	select	@From_Date	= @To_Date

select	@From_Date	= convert(varchar(10), @From_Date, 101) + ' 00:00:00',
		@To_Date	= convert(varchar(10), @To_Date,   101) + ' 23:59:59';

-- Indicators for the report
declare	@ind_1		int
declare	@ind_2		int
declare	@ind_3		int
declare @ind_4		int
declare @ind_5		int
declare @ind_6		int

select	@ind_1	=	92, -- Bk Certificate Received
		@ind_2	=	90 -- BK Counseling Fee Received

select	c.client									 as 'client',
		convert(varchar(80), '')					 as 'name',
		ca.start_time								 as 'appt_time',
		t.appt_name									 as 'appt_type',
		dbo.format_city_state_zip (a.city, a.state, a.postalcode)	 as 'city_state_zip',
		isnull(co.bankruptcy_district,0)			 as 'bankruptcy_district_number',
		d.description								 as 'bankruptcy_district',
		ca.result									 as 'appt_result',

		left(isnull(m.nfcc,'X'),1)					 as 'method_first_contact',
		rb.description								 as 'referred_by',
		isnull(count(p.person),1)					 as 'count_people',
		ca.office									 as 'office',
		o.name										 as 'office_name',

		-- Indicators for the client
		convert(varchar(1), case when i1.client_indicator is null then 'N' else 'Y' end) as 'indicator_1',
		convert(varchar(1), case when i2.client_indicator is null then 'N' else 'Y' end) as 'indicator_2',
		convert(varchar(1), case when i3.client_indicator is null then 'N' else 'Y' end) as 'indicator_3',
		convert(varchar(1), case when i4.client_indicator is null then 'N' else 'Y' end) as 'indicator_4',
		convert(varchar(1), case when i5.client_indicator is null then 'N' else 'Y' end) as 'indicator_5',
		convert(varchar(1), case when i6.client_indicator is null then 'N' else 'Y' end) as 'indicator_6'
	
from			client_appointments ca	with (nolock)
inner join		appt_types t			with (nolock) on ca.appt_type = t.appt_type and 1 = t.bankruptcy
inner join		clients c				with (nolock) on ca.client = c.client
left outer join	offices o				with (nolock) on ca.office = o.office
left outer join	people p				with (nolock) on c.client = p.client
left outer join	counties co				with (nolock) on c.county = co.county
left outer join	bankruptcy_districts d	with (nolock) on co.bankruptcy_district = d.bankruptcy_district
left outer join	client_indicators i1	with (nolock) on c.client = i1.client and @ind_1 = i1.indicator
left outer join	client_indicators i2	with (nolock) on c.client = i2.client and @ind_2 = i2.indicator
left outer join	client_indicators i3	with (nolock) on c.client = i3.client and @ind_3 = i3.indicator
left outer join	client_indicators i4	with (nolock) on c.client = i4.client and @ind_4 = i4.indicator
left outer join	client_indicators i5	with (nolock) on c.client = i5.client and @ind_5 = i5.indicator
left outer join	client_indicators i6	with (nolock) on c.client = i6.client and @ind_6 = i6.indicator
left outer join	referred_by rb			with (nolock) on c.referred_by = rb.referred_by
left outer join	FirstContactTypes m		with (nolock) on c.method_first_contact = m.oID
left outer join	addresses a				with (nolock) on c.addressid = a.address

where		ca.start_time	between @From_Date and @To_Date
and 		ca.status in ('k','w')

GROUP BY	c.client, ca.start_time, t.appt_name, a.PostalCode, a.state, a.city,
			co.bankruptcy_district, d.description,
			ca.result, c.method_first_contact, m.nfcc, rb.description,
			ca.office, o.name,
			i1.client_indicator, i2.client_indicator, i3.client_indicator, i4.client_indicator, i5.client_indicator, i6.client_indicator

order by 4, 3, 1 --  appt_type, appt_time, client

return ( @@rowcount )
GO
