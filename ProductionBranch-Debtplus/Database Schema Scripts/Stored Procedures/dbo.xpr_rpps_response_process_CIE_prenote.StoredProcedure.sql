USE [DebtPlus]
GO
/****** Object:  StoredProcedure [dbo].[xpr_rpps_response_process_CIE_prenote]    Script Date: 09/15/2014 13:13:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[xpr_rpps_response_process_CIE_prenote] ( @rpps_response_file as int ) as

-- =========================================================================================
-- ==           Process a prenote response                                                ==
-- =========================================================================================

-- ChangeLog
--   06/09/2009
--      Remove references to debt_number and use client_creditor

-- Start a transaction to block the entire operation as a unit
BEGIN TRANSACTION
SET XACT_ABORT ON
SET NOCOUNT ON

-- Process prenote operations.
select	d.rpps_response_detail,
	d.rpps_transaction,
	t.client,
	t.creditor,
	t.client_creditor,
	d.return_code

into	#rpps_cie_prenotes

from	rpps_response_details d
inner join rpps_transactions t on d.rpps_transaction = t.rpps_transaction
inner join creditors cr on t.creditor = cr.creditor
inner join clients c on t.client = c.client

where	d.rpps_response_file		= @rpps_response_file
and	d.service_class_or_purpose	= 'CIE'
and	d.processing_error is null
and	t.return_code is null
and	t.service_class_or_purpose	= 'CIE'
and	t.transaction_code		= '23';

-- Update the error tables with invalid pointers if the clients do not exist
update	rpps_response_details
set	processing_error		= 'UNKONWN DEBT RECORD'

from	rpps_response_details d
inner join rpps_transactions t on d.rpps_transaction = t.rpps_transaction
inner join creditors cr on t.creditor = cr.creditor
inner join clients c on t.client = c.client

where	d.rpps_response_file		= @rpps_response_file
and	d.service_class_or_purpose	= 'CIE'
and	d.processing_error is null
and	t.return_code is null
and	t.service_class_or_purpose	= 'CIE'
and	t.transaction_code		= '23'

and	d.rpps_response_detail not in (
	select	rpps_response_detail
	from	#rpps_cie_prenotes
)

-- Mark the items as having an error and clear the prenote status
update	client_creditor
set	prenote_date = null
from	client_creditor cc
inner join #rpps_cie_prenotes x on cc.client_creditor = x.client_creditor;

insert into client_notes (client, client_creditor, is_text, type, dont_edit, dont_delete, dont_print, subject, note)
select	x.client,
	cc.client_creditor,
	1 as is_text,
	3 as type,
	1 as dont_edit,
	1 as dont_delete,
	0 as dont_print,
	'RPPS Prenote Failed' as subject,
	'The RPPS prenote failed' + isnull(' with error '+x.return_code,'')
from	#rpps_cie_prenotes x
inner join client_creditor cc on x.client_creditor = cc.client_creditor;

-- Update the RPPS transaction table
update	rpps_transactions
set	return_code = x.return_code
from	rpps_transactions t
inner join #rpps_cie_prenotes x on x.rpps_transaction = t.rpps_transaction;

-- Complete the transaction
drop table #rpps_cie_prenotes
commit transaction
return ( 1 )
GO
