SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER procedure [rpt_disbursement_deduct] ( @disbursement_register as int = null ) as

-- ==================================================================================================
-- ==            Report the information about the deduction check detail                           ==
-- ==================================================================================================

-- Suppress intermediate result sets
set nocount on

-- If there is no disbursement register, find the last disbursement that is of use to us.
if @disbursement_register is null
	select top 1 @disbursement_register = disbursement_register
	from	registers_disbursement
	where	tran_type = 'AD'
	and	date_posted is not null
	order by date_created desc;

-- Determine the deduct creditor
declare	@deduct_creditor	varchar(10)
select	@deduct_creditor = deduct_creditor
from	config with (nolock);

-- Create a table to hold the results of the transactions
create table #result (xact_type varchar(2), description varchar(50), deduct_amount money, bill_amount money);

select	cr.type				as 'creditor_type',

	sum(
		case when rcc.creditor_type = 'D' then rcc.fairshare_amt
		     else 0
		end
	)				as 'deduct_amount',

	sum(
		case when rcc.creditor_type = 'B' then rcc.fairshare_amt
		     else 0
		end
	)				as 'bill_amount'

into	#disbursement_figures
from	registers_client_creditor rcc with (nolock)
left outer join creditors cr with (nolock) on rcc.creditor = cr.creditor
where	rcc.tran_type in ('AD', 'BW', 'MD', 'CM')
and	rcc.creditor <> @deduct_creditor
and	rcc.disbursement_register = @disbursement_register
group by cr.type;

-- Calculate the total amount of the deduction contributions
declare	@total_deduct	money
select	@total_deduct = sum(deduct_amount)
from	#disbursement_figures;

-- Determine the original balance for the deduction account
declare	@deduct_balance	money
select	@deduct_balance = deduct_balance
from	registers_disbursement with (nolock)
where	disbursement_register = @disbursement_register

-- The original amount is the difference between the values
if @deduct_balance is not null
begin
	insert into #result (xact_type, description, deduct_amount, bill_amount)
	values ('1 ', 'Starting balance', @deduct_balance - @total_deduct, 0)
end

-- Include the other transactions
insert into #result (xact_type, description, deduct_amount, bill_amount)
select	'2' + i.creditor_type, isnull(t.description,'Unknown creditor'), i.deduct_amount, i.bill_amount
from	#disbursement_figures i
left outer join creditor_types t on i.creditor_type = t.type;

-- Include any checks issued for the deduct creditor
insert into #result (xact_type, description, deduct_amount, bill_amount)
select	'3 ', 'Issue check' + isnull(' #' + convert(varchar, tr.checknum),''),
	tr.amount,
	0
from	registers_creditor rc
inner join registers_trust tr on rc.trust_register = tr.trust_register
where	tr.tran_type in ('AD', 'MD', 'CM')
and	tr.creditor = @deduct_creditor
and	disbursement_register = @disbursement_register
order by tr.checknum;

-- Include the ending balance figure
if @deduct_balance is not null
begin
	select	@deduct_balance = @deduct_balance - (select sum(deduct_amount)
	from	#result
	where	xact_type like '3%');

	insert into #result (xact_type, description, deduct_amount, bill_amount)
	values ('4 ', 'Ending Balance', @deduct_balance, 0)
end

-- Return the results to the report
select	@disbursement_register		as disbursement_register,
	x.xact_type			as xact_type,
	x.description			as description,
	x.deduct_amount			as deduct_amount,
	x.bill_amount			as bill_amount
from	#result x
order by 1;

-- Discard the working tables
drop table #result
drop table #disbursement_figures
GO
