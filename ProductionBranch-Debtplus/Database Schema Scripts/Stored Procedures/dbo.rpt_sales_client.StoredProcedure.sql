USE [DebtPlus]
GO
/****** Object:  StoredProcedure [dbo].[rpt_sales_client]    Script Date: 09/15/2014 13:13:33 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[rpt_sales_client] ( @sales_file as int ) as

-- ===============================================================================================
-- ==            return the information for the non-dmp plan                                    ==
-- ===============================================================================================

select	creditor_type			as creditor_type,
	creditor_name			as creditor_name,
	isnull(balance,0)		as balance,
	isnull(interest_rate,0) * 100.0	as interest_rate,
	isnull(minimum_payment,0)	as minimum_payment,
	isnull(late_fees,0)		as late_fees,
	isnull(overlimit_fees,0)	as overlimit_fees,
	isnull(cosigner_fees,0)		as cosigner_fees,
	isnull(finance_charge,0)	as finance_charge,
	isnull(total_interest_fees,0)	as total_interest_fees,
	isnull(principal,0)		as principle

from	sales_debts with (nolock)
where	sales_file		= @sales_file

return ( @@rowcount )
GO
