USE [DebtPlus]
GO
/****** Object:  StoredProcedure [dbo].[xpr_rpps_response_process_CDV]    Script Date: 09/15/2014 13:13:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[xpr_rpps_response_process_CDV] ( @rpps_response_file as int ) AS
-- ====================================================================================================================
-- ==            This is the response to a balance verification request                                              ==
-- ====================================================================================================================

-- ChangeLog
--   9/31/2002
--     Added support for client_creditor_balances table
--   3/7/2012
--     Supported prenote balance verification request

-- UPDATE ACCOUNT BALANCES FROM RPPS
-- Set to "0" to disable all balance verification updates
-- Set to "1" to update ALL balance verifications
-- Set to "2" to update ONLY ZERO balance verifications
declare	@update_balance		int
select	@update_balance		= 1

-- SET BALANCE VERIFICATION STATUS TO "V"
-- Set to "0" to set balance verification on all updated balances
-- Set to "1" to set balance verification ONLY IF THE NEW BALANCE IS ZERO
-- (not used if balances are not updated)
declare	@balance_verification	int
select	@balance_verification	= 1

-- Find the proposal from the trace table
begin transaction
set xact_abort on
set nocount off

-- Ensure that the client id is valid
update		rpps_response_details
set			processing_error = 'INVALID CLIENT #'
from		rpps_response_details d
inner join	rpps_response_details_cdv v on d.rpps_response_detail = v.rpps_response_detail
where		d.service_class_or_purpose = 'CDV'
and			d.rpps_response_file = @rpps_response_file
and			isnumeric(v.client) = 0
and			d.processing_error is null;

-- Find the subset of the transactions that we are to process.
select		d.rpps_response_detail			as rpps_response_detail,
			d.rpps_transaction				as rpps_transaction,
			d.trace_number					as trace_number,
			v.account_balance				as account_balance,
			v.client						as balance_client,
			bal.orig_balance + bal.orig_balance_adjustment + bal.total_interest - bal.total_payments	as old_balance,

			d.account_number				as balance_account_number,
			d.processing_error				as processing_error,

			isnull(cc.client_creditor,-1)	as client_creditor,
			isnull(cc.client,-1)			as client,
			isnull(cc.account_number,'')	as account_number,
			t.prenote_status				as prenote_status,
			t.return_code					as return_code,
			@update_balance					as update_balance,
			@balance_verification			as balance_verification

into		#rpps_response_detail_cdv
from		rpps_response_details d
inner join	rpps_response_details_cdv v on d.rpps_response_detail = v.rpps_response_detail
inner join	rpps_transactions t on d.rpps_transaction = t.rpps_transaction
left outer join	client_creditor cc with (nolock) on t.client_creditor = cc.client_creditor
left outer join client_creditor_balances bal with (nolock) on cc.client_creditor_balance = bal.client_creditor_balance
where		d.rpps_response_file = @rpps_response_file
and			d.processing_error is null

-- Create an index to help find the transactions
create index ix1_temp_rpps_response_detail on #rpps_response_detail_cdv ( client_creditor );

-- Look for a mismatch to the client number
update		#rpps_response_detail_cdv
set			processing_error = 'INVALID CLIENT #'
where		client <> balance_client;

-- Look for a mismatch to the account number
update		#rpps_response_detail_cdv
set			processing_error = 'INVALID ACCT #'
where		account_number <> balance_account_number
and			processing_error is null;

-- Update the transactions accordingly
update		rpps_response_details
set			processing_error = x.processing_error
from		rpps_response_details d
inner join	#rpps_response_detail_cdv x on d.rpps_response_detail = x.rpps_response_detail
where		x.processing_error is not null;

-- Remove the invalid lines from being processed further
delete
from		#rpps_response_detail_cdv
where		processing_error is not null;

-- For RPPS, set the last communication information. (Required for later.)
update		client_creditor
set			last_communication		= x.trace_number
from		client_creditor cc
inner join	#rpps_response_detail_cdv x on cc.client_creditor = x.client_creditor

-- Set the succcess condition (#5) if indicated
update		client_creditor
set			send_bal_verify = 5
from		client_creditor cc
inner join	#rpps_response_detail_cdv x on cc.client_creditor = x.client_creditor
where		x.return_code is null
and			x.prenote_status in (2, 3);

-- Set the failure condition (#4) if indicated
update		client_creditor
set			send_bal_verify = 4
from		client_creditor cc
inner join	#rpps_response_detail_cdv x on cc.client_creditor = x.client_creditor
where		x.return_code is not null
and			x.prenote_status in (2, 3);

-- Force the balance update operations
update		#rpps_response_detail_cdv
set			update_balance = 1
where		update_balance = 2
and			account_balance = 0

update		#rpps_response_detail_cdv
set			update_balance = 1
where		prenote_status in (2, 3)

-- Disable the verification status
update		#rpps_response_detail_cdv
set			balance_verification = 0
where		update_balance <> 1;

-- Finally, update the balances for the debts
update		client_creditor_balances
set			orig_balance_adjustment		= isnull(bal.total_payments,0) + isnull(x.account_balance,0) - isnull(bal.orig_balance,0) - isnull(bal.total_interest,0)
from		client_creditor_balances bal
inner join	client_creditor cc on bal.client_creditor_balance = cc.client_creditor_balance
inner join	#rpps_response_detail_cdv x on cc.client_creditor = x.client_creditor
where		x.update_balance = 1

-- Generate the system note that the balance was verified
insert into client_notes (client, client_creditor, type, dont_edit, dont_delete, dont_print, subject, note)
select		cc.client, cc.client_creditor, 3, 1, 1, 0, 'Balance Verification',
			'The balance was verified to be $' + convert(varchar, x.account_balance, 1) + ' by the RPPS system.'
			+ char(13) + char(10) + 'The current at this time was $' + convert(varchar, x.old_balance,1) + '.'
from		client_creditor cc with (nolock)
inner join #rpps_response_detail_cdv x on cc.client_creditor = x.client_creditor
where		x.update_balance = 1
and			x.account_balance <> x.old_balance;

-- Set the verified balance indicator
update		client_creditor
set			balance_verify_date		= getdate(),		-- Set balance verification date
			balance_verify_by		= 'RPPS SYSTEM'		-- Set balance verified "by"
from		client_creditor cc
inner join	#rpps_response_detail_cdv x on cc.client_creditor = x.client_creditor
where		x.balance_verification = 1

-- Set the drop date when the balance is finally paid off
update		client_creditor
set			drop_date				= getdate()			-- For the scorecard people, remember when the debt is paid.
from		client_creditor cc
inner join	#rpps_response_detail_cdv x on cc.client_creditor = x.client_creditor
where		x.balance_verification = 1
and			x.account_balance = 0

drop table #rpps_response_detail_cdv
commit transaction
return ( 1 )
GO
