USE [DebtPlus]
GO
/****** Object:  StoredProcedure [dbo].[rpt_Disbursement_Info]    Script Date: 09/15/2014 13:13:33 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[rpt_Disbursement_Info] ( @disbursement_register AS INT ) AS

-- =====================================================================================================
-- ==                    Fetch the disbursement information                                           ==
-- =====================================================================================================

SELECT
	case tran_type
		when 'AD' then case when date_posted is null then 'O' else 'C' end
		else 'M'
	end as status,
	created_by,
	date_created,
	disbursement_date
FROM	registers_disbursement
WHERE	disbursement_register = @disbursement_register

return ( @@rowcount )
GO
