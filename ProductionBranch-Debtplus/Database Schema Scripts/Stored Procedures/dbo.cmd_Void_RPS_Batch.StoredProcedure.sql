USE [DebtPlus]
GO
/****** Object:  StoredProcedure [dbo].[cmd_Void_RPS_Batch]    Script Date: 09/15/2014 13:13:32 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE PROCEDURE [dbo].[cmd_Void_RPS_Batch] ( @FileID AS INT = NULL ) AS
-- ===========================================================================================
-- ==             Remove (or back-out) an RPS transfer file                                 ==
-- ===========================================================================================

IF @FileID IS NULL
BEGIN
	print 'Please specify the RPS file ID as a parameter to the stored procedure'
	print 'Possible File IDs are located in the table ''rpps_files'''
	print ''
	set NOCOUNT ON
	SELECT TOP 10	right('     ' + convert(varchar(5),rpps_file), 5) as 'File ID',
			convert(varchar(10),date_created,1) as 'created on',
			substring(created_by,1,20) as 'created by'
	FROM rpps_files
	ORDER BY date_created DESC
	SET NOCOUNT OFF
	return ( 0 )
END

-- Ensure that the file id is valid for the system
IF NOT EXISTS ( select rpps_file FROM rpps_files WHERE rpps_file = @FileID )
BEGIN
	RaisError (50031, 11, 1, @FileID)
	return ( 0 )
END

-- Remove the modified gross reversal transactions from the system
DELETE	FROM rpps_transactions
WHERE	trace_number_first IS NULL
AND	transaction_code = 27
AND	service_class_or_purpose = 'CIE'
AND	rpps_batch IN ( select rpps_batch FROM rpps_batches WHERE rpps_file = @FileID )

-- Remove the trace information from the items
UPDATE	rpps_transactions
SET	trace_number_first = NULL,
	trace_number_last = NULL,
	rpps_batch = NULL
WHERE	rpps_batch IN ( select rpps_batch FROM rpps_batches WHERE rpps_file = @FileID )

-- Remove the batches from the system
DELETE FROM	rpps_batches
WHERE		rpps_file = @FileID

-- Remove the file from the system
DELETE FROM	rpps_files
WHERE		rpps_file = @FileID

-- Return success to the caller
RETURN ( 1 )
GO
