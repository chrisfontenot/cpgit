USE [DebtPlus]
GO
/****** Object:  StoredProcedure [dbo].[rpt_Proposal_Employer]    Script Date: 09/15/2014 13:13:33 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE PROCEDURE [dbo].[rpt_Proposal_Employer] ( @Client AS INT ) AS
-- ============================================================================================
-- ==            Fetch the employer and primary reason for the plan                          ==
-- ============================================================================================

SELECT		isnull(e.name,'None was specified') as 'employer',
		isnull(f.description,'No reason was specified') as 'financial_problem'
FROM		clients c
LEFT OUTER JOIN	people p ON p.client = c.client AND 1 = p.relation
LEFT OUTER JOIN	employers e ON e.employer = p.employer
LEFT OUTER JOIN	financial_problems f ON c.cause_fin_problem1 = f.financial_problem
WHERE		c.client = @client

return ( @@rowcount )
GO
