USE [DebtPlus]
GO
/****** Object:  StoredProcedure [dbo].[xpr_insert_sales_file]    Script Date: 09/15/2014 13:13:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE PROCEDURE [dbo].[xpr_insert_sales_file] ( @client as int ) as
	insert into sales_files (client) values ( @client )
	return ( scope_identity() )
GO
