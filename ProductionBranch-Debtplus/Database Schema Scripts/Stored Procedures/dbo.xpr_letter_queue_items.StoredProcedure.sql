USE [DebtPlus]
GO
/****** Object:  StoredProcedure [dbo].[xpr_letter_queue_items]    Script Date: 09/15/2014 13:13:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[xpr_letter_queue_items] ( @letter_queue as varchar(50) ) AS

-- ======================================================================================================
-- ==            Return the list of items in the pending queue for the letters                         ==
-- ======================================================================================================

-- ChangeLog
--   5/15/2003
--     Added language to the result set to support multiple language definitions

select		letter_queue
into		#items
from		letter_queue
where		date_printed	is null
and		queue_name	= @letter_queue;

-- Ensure that the items are removed if they have no letter information
delete		#items
from		#items i
inner join	letter_queue q with (nolock) on i.letter_queue = q.letter_queue
where		q.formatted_letter is null;

delete		#items
from		#items i
inner join	letter_queue q with (nolock) on i.letter_queue = q.letter_queue
where		convert(varchar(80), q.formatted_letter) = '';

-- Return the results to the printer program
select		q.letter_queue			as letter_queue,

		q.top_margin			as top_margin,
		q.bottom_margin			as bottom_margin,
		q.left_margin			as left_margin,
		q.right_margin			as right_margin,
		q.formatted_letter		as formatted_letter

from		letter_queue q with (nolock)
inner join	#items i on q.letter_queue = i.letter_queue
order by	q.priority, q.sort_order

drop table	#items
return ( @@rowcount )
GO
