USE [DebtPlus]
GO
/****** Object:  StoredProcedure [dbo].[xpr_insert_office]    Script Date: 09/15/2014 13:13:35 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[xpr_insert_office] ( @Name as varchar(80), @Type as int = 4, @AddressID as int = null,
    @Directions as text = null,
    @TelephoneID as int = null, @AltTelephoneID as int = null, @FAXID as int = null, @EmailID as int = null,
    @Default_Till_Missed as int = null,
    @Counselor as int = null, @Region as int = null, @District as int = null,
    @Default as bit = False, @ActiveFlag as bit = True,
    @NFCC as varchar(80) = null, @HUD_hcs_id as int = 0,
    
    -- Obsolete parameters. These are just not used but exist for allowing them to be specified.
    
    @TimeZoneID as int = null
) as

-- ===========================================================================================================================
-- ==             Create an entry in the offices table for .NET                                                             ==
-- ===========================================================================================================================

-- ChangeLog
--   10/15/2009
--      Removed "counseling_budget_amt" from the offices table

-- Add the row to the offices table
insert into offices ([Name],[Type],[AddressID],[TelephoneID],[AltTelephoneID],[FAXID],[EmailID],[Directions],[HUD_hcs_id],[NFCC],[Default_till_missed],[Counselor],[Region],[District],[Default],[ActiveFlag])
             values (@Name, @Type, @AddressID, @TelephoneID, @AltTelephoneID, @FAXID, @EmailID, @Directions, @HUD_hcs_id, @NFCC, @Default_till_missed, @Counselor, @Region, @District, @Default, @ActiveFlag)

-- Return the ID of the new office row
return ( scope_identity() )
GO
