IF EXISTS(SELECT * FROM SYSOBJECTS WHERE TYPE='P' AND NAME=N'xpr_deposit_batch_entry_FHLB')
	EXEC ('DROP PROCEDURE xpr_deposit_batch_entry_FHLB')
GO
CREATE PROCEDURE [dbo].[xpr_deposit_batch_entry_FHLB] ( @batch as INT, @scanned_client as varchar(20) = null, @scanned_date as datetime = null, @amount as Money = null, @reference as varchar(50) = NULL, @ErrorMessage as varchar(50) = NULL, @tran_type as varchar(10) = 'LB' ) AS
-- =====================================================================================================
-- ==                  Create a entry into the system for the deposit                                 ==
-- =====================================================================================================

-- Prevent intermediate result sets
SET NOCOUNT ON

-- Try to find the client information
DECLARE	@Client			int
DECLARE	@ActiveStatus	varchar(10)

-- Creditor for the client's debt
DECLARE @Creditor		varchar(10)
SET @Creditor = 'V71585'

-- Remove extra spaces from the input
select	@scanned_client	= ltrim(rtrim(isnull(@scanned_client,''))),
		@reference		= ltrim(rtrim(isnull(@reference,''))),
		@ErrorMessage	= ltrim(rtrim(ISNULL(@ErrorMessage,'')))

-- Do not attempt to load the hash total client even if the config file does not contain
-- the proper reference to the hash total.
if @scanned_client = '9999999'
	return 0

if @ErrorMessage = ''
	select	@ErrorMessage = null

IF @scanned_client = ''
	SELECT @scanned_client = null

IF @reference = ''
	SELECT @reference = null

-- If there is an account reference then use it for the debt search to find the client
IF @ErrorMessage IS NULL AND @scanned_client IS NOT NULL AND @Client IS NULL
BEGIN
	SELECT  @Client = cc.client
	FROM	client_creditor cc WITH (NOLOCK)
	WHERE	creditor = @Creditor AND account_number = @scanned_client
END

if isnull(@Client,0) <= 0
	SET @ErrorMessage = 'Invalid ID'

-- Is the transaction valid to be posted?
declare	@ok		int
SELECT	@OK	= 0

IF @ErrorMessage IS NULL
	SELECT	@OK = 1

-- Insert the item into the batch
INSERT INTO deposit_batch_details	(deposit_batch_id,	ok_to_post,	tran_subtype,	scanned_client,		client,		amount,		item_date,	reference,	message,	created_by,	date_created,	date_posted)
VALUES					(@batch,		@ok,		@tran_type,	@scanned_client,	@client,	@amount,	@scanned_date,	@reference,	@ErrorMessage,	suser_sname(),	getdate(),	null)

if @ok = 1
	SELECT @ok = SCOPE_IDENTITY()

-- Correct the deposit amount in the pending tables
update	clients
set		deposit_in_trust = dbo.DepositsInTrust(client)
where	client = @client

-- Return the record number or 0 to indicate a failure
RETURN ( @ok )
GO
GRANT EXECUTE ON xpr_deposit_batch_entry_FHLB TO public AS dbo;
GO
