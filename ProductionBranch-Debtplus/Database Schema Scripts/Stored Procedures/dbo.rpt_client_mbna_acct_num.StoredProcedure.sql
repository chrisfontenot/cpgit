USE [DebtPlus]
GO
/****** Object:  StoredProcedure [dbo].[rpt_client_mbna_acct_num]    Script Date: 09/15/2014 13:13:32 ******/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE PROCEDURE [dbo].[rpt_client_mbna_acct_num] AS

-- ======================================================================================================
-- ==            Return the client account numbers for clients' MBNA accounts                      ==
-- ======================================================================================================

select	client_creditor.client as 'client',
	client_creditor.account_number as 'account_number',
	creditors.creditor_name as 'creditor_name'
from	client_creditor, creditors with (nolock)
where	creditors.creditor = client_creditor.creditor
	and creditors.creditor_name like '%MBNA%'
return ( @@rowcount )
GO
