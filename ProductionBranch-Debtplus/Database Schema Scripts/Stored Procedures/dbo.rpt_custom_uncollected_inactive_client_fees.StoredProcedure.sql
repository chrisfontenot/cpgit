USE [DebtPlus]
GO
/****** Object:  StoredProcedure [dbo].[rpt_custom_uncollected_inactive_client_fees]    Script Date: 09/15/2014 13:13:33 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE PROCEDURE [dbo].[rpt_custom_uncollected_inactive_client_fees] as

-- Return the basic information for the report.
select	cc.client,
	dbo.format_normal_name (pn.prefix, pn.first, pn.middle, pn.last, pn.suffix)		as client_name,
	dbo.format_TelephoneNumber (c.HomeTelephoneID)						as home_ph,
	cc.creditor,
	isnull(cc.creditor_name, cr.creditor_name)					as creditor_name,
	b.orig_balance,
	b.orig_balance + b.orig_balance_adjustment + b.total_interest - b.total_payments as balance,
	cc.disbursement_factor,
	cc.date_created									as created_date,
	rcc.date_created								as last_disbursement_date,
	rcc.debit_amt									as last_disbursement_amount,
	ca.start_time									as appt_time

from	client_creditor cc			with (nolock)
inner join clients c				with (nolock) on cc.client = c.client
inner join client_creditor_balances b		with (nolock) on cc.client_creditor_balance = b.client_creditor_balance and b.client_creditor = cc.client_creditor
inner join creditors cr				with (nolock) on cc.creditor = cr.creditor
inner join creditor_classes ccl			with (nolock) on cr.creditor_class = ccl.creditor_class
left outer join registers_client_creditor rcc	with (nolock) on cc.last_payment = rcc.client_creditor_register
left outer join people p			with (nolock) on c.client = p.client and 1 = p.relation
left outer join names pn with (nolock) on p.nameid = pn.name
left outer join client_appointments ca		with (nolock) on c.first_kept_appt = ca.client_appointment

where	cc.reassigned_debt	= 0
and	b.orig_balance + b.orig_balance_adjustment + b.total_interest - b.total_payments > 0
and	ccl.client_statement	= 1
and	c.active_status not in ('A','AR')
and	c.client > 0

-- And more than 3 months
and	datediff (m, isnull(rcc.date_created, cc.date_created), getdate()) > 3

order by cc.creditor, cc.date_created, c.client

return ( @@rowcount )
GO
