SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER procedure [xpr_creditor_contribution_invoice_list] ( @invoice_register as int ) as

-- =====================================================================================================
-- ==           Return the information for a specific invoice used on the creditor contributions      ==
-- =====================================================================================================

SELECT		ri.invoice_register		as 'invoice',
		tr.checknum			as 'checknum',
		ri.inv_date			as 'inv_date',
		isnull(ri.inv_amount,0)		as 'inv_amount',
		ri.pmt_date			as 'pmt_date',
		isnull(ri.pmt_amount,0)		as 'pmt_amount',
		ri.adj_date			as 'adj_date',
		isnull(ri.adj_amount,0)		as 'adj_amount'

FROM		registers_invoices ri WITH (NOLOCK)
INNER JOIN	registers_creditor rc WITH (NOLOCK) ON ri.invoice_register = rc.invoice_register AND rc.tran_type IN ('AD','MD','CM','BW')
INNER JOIN	registers_trust tr WITH (NOLOCK) ON rc.trust_register = tr.trust_register

WHERE		ri.invoice_register = @invoice_register

return ( @@rowcount )
GO
