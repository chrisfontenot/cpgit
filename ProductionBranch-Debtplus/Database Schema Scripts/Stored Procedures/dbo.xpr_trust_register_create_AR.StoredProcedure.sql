SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER procedure [xpr_trust_register_create_AR] ( @Client as Int, @Amount AS Money = 0, @ItemDate AS DateTime = NULL, @Cleared AS VarChar(1) = NULL, @checknum as BigInt = NULL ) AS

-- ====================================================================================================
-- ==            Create a check in the trust register for paying this client a refund check          ==
-- ====================================================================================================

SET NOCOUNT ON

-- Ensure that the amount is valid
IF @Amount < 0
BEGIN
	RaisError (50019, 16, 1)
	Return ( 0 )
END

-- Default the item date
IF @ItemDate IS NULL
	SET @ItemDate = getdate()

-- The check can not be blank and still not be pending to be printed
if @Cleared IS NULL
	SET @Cleared = 'P'

-- Validate the cleared status
IF @Cleared NOT IN (' ','R','C','E','P')
BEGIN
	RaisError (50024, 16, 1, @Cleared)
	Return ( 0 )
END

IF @checknum IS NULL
	SET @Cleared = 'P'

DECLARE	@trust_register		INT

-- Insert the item into the trust register
INSERT INTO	registers_trust (tran_type,	client,		date_created,	amount,		cleared,	checknum)
VALUES				('AR',		@client,	@ItemDate,	@Amount,	@Cleared,	@checknum)

SELECT @trust_register = SCOPE_IDENTITY()

-- Return the trust register ID to the caller
RETURN ( @trust_register )
GO
