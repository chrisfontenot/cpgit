USE [DebtPlus]
GO
/****** Object:  StoredProcedure [dbo].[rpt_appt_available_analysis]    Script Date: 09/15/2014 13:13:32 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[rpt_appt_available_analysis] ( @From_date as datetime = null, @To_date as datetime = null, @office as int = null ) as

-- Make the dates valid
set nocount on

if @To_Date is null
	select	@To_Date	= getdate()

if @From_Date is null
	select	@From_Date	= @To_Date

select	@From_Date	= convert(datetime, convert(varchar(10), @From_Date, 101) + ' 00:00:00'),
		@To_Date	= convert(datetime, convert(varchar(10), dateadd(d, 1, @To_Date), 101) + ' 00:00:00')

create table #x ( id int identity(1,1), appt_time int, counselor int null, client_appointment int null, start_time datetime, office int, appt_type int null, available int, booked int);

-- Do the initial selection for the data
insert into #x (appt_time, counselor, start_time, office, appt_type, available, booked)
select t.appt_time, ac.counselor, t.start_time, t.office, t.appt_type, 1, 0
from appt_times t
inner join offices o on t.office = o.office
inner join appt_counselors ac on t.appt_time = ac.appt_time and 0 = ac.inactive
where t.start_time >= @from_date
and   t.start_time <  @to_date

-- Update the client appointment for matching booked appointments
update #x set client_appointment = ca.client_appointment
from #x x
inner join client_appointments ca on x.appt_time = ca.appt_time and x.counselor = ca.counselor
where ca.office is not null
and ca.status = 'P'

-- Add the appointments that are not assigned to a counselor
insert into #x (appt_time, counselor, client_appointment, start_time, office, appt_type, available, booked)
select ca.appt_time, ca.counselor, ca.client_appointment, ca.start_time, ca.office, ca.appt_type, 1, 1
from client_appointments ca
left outer join #x x on ca.appt_time = x.appt_time
where ca.office is not null
and   ca.status = 'P'
and   x.id is null
and	  ca.start_time >= @from_date
and   ca.start_time <  @to_date

-- Correct the appointment type if needed
update #x set appt_type = ca.appt_type
from #x x
inner join client_appointments ca on x.client_appointment = ca.client_appointment
where x.appt_type is null

-- Discard all of the items that are for not the indicated office
if @office is not null
	delete
	from	#x
	where	office <> @office;
	
-- Set the booked status
update #x set booked = 1 where client_appointment is not null;

-- Return the results for the report
select x.id, x.appt_time, x.counselor, x.client_appointment, x.start_time, x.office, x.appt_type, x.available, x.booked,
	   convert(varchar(80), t.appt_name) as appt_name,
	   convert(varchar(80), o.name) as office_name,
	   convert(varchar(80), dbo.format_normal_name(default,cox.first,default,cox.last,default)) as counselor_name
from #x x
left outer join appt_types t on x.appt_type = t.appt_type
left outer join offices o on x.office = o.office
left outer join counselors co on x.counselor = co.counselor
left outer join names cox with (nolock) on co.nameid = cox.name
order by x.office, x.start_time, x.counselor

drop table #x

return ( @@rowcount )
GO
