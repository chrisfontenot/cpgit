USE [DebtPlus]
GO
/****** Object:  StoredProcedure [dbo].[xpr_creditor_methods_list]    Script Date: 09/15/2014 13:13:34 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[xpr_creditor_methods_list] ( @creditor as int, @type as varchar(10) = 'EFT' ) as

-- ================================================================================================
-- ==           Return the information for the payment list for the creditor                     ==
-- ================================================================================================

SELECT	creditor_method as item_key,
	case 
		when @type = 'EFT' then isnull(b.description,'bank #' + convert(varchar, m.bank))
		when b.type = 'C' then 'Printed Information Only'
		else isnull(b.description,'bank #' + convert(varchar, m.bank))
	end as bank,
	case b.type
		when 'R' then m.rpps_biller_id
		when 'E' then m.epay_biller_id
		else coalesce(m.rpps_biller_id,m.epay_biller_id,'')
	end as biller_id
FROM	creditor_methods m with (nolock)
left outer join banks b with (nolock) on m.bank = b.bank
where	m.creditor	= @creditor
AND	m.type		= @type
order by 2, 3, 1

return ( @@rowcount )
GO
