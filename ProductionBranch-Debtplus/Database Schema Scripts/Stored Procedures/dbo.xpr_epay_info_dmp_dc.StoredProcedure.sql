USE [DebtPlus]
GO
/****** Object:  StoredProcedure [dbo].[xpr_epay_info_dmp_dc]    Script Date: 09/15/2014 13:13:35 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE PROCEDURE [dbo].[xpr_epay_info_dmp_dc] ( @epay_transaction as int ) as

-- ===============================================================================================
-- ==          Return the creditor information for the proposal                                 ==
-- ===============================================================================================

-- Suppress intermediate results
set nocount on

declare	@agency_name				varchar(80)
declare	@agency_id				varchar(12)
declare	@request_date				datetime
declare	@epay_biller_id				varchar(12)
declare	@creditor				varchar(10)
declare	@creditor_name				varchar(80)
declare	@creditor_record_number			int
declare	@creditor_industry			varchar(80)
declare	@first_payment_date_under_dmp		datetime
declare	@proposed_creditor_payment		money
declare	@customer_biller_account_number		varchar(80)
declare	@client					int
declare	@client_name				varchar(80)
declare	@client_account_balance			money
declare	@creditor_outstanding_balance		money
declare	@creditor_outstanding_balance_sign	varchar(1)
declare	@client_account_balance_sign		varchar(1)
declare	@date_of_balance			datetime
declare	@dmp_type_indicator			varchar(2)
declare	@transaction_type			varchar(2)
declare	@agency_product_id			varchar(2)
declare	@local_biller_id			varchar(80)

-- Find the proposal
declare	@client_creditor_proposal		int
declare	@epay_file				int
declare	@bank					int

select	@client_creditor_proposal	= client_creditor_proposal,
	@epay_file			= epay_file,
	@epay_biller_id			= epay_biller_id,
	@creditor_record_number		= creditor_number
from	epay_transactions
where	epay_transaction		= @epay_transaction

-- Find the request date
select	@request_date			= date_created,
	@bank				= bank
from	epay_files
where	epay_file			= @epay_file;

-- Find the agency information
select	@agency_name			= isnull(b.immediate_origin_name, cnf.[name]),
	@agency_id			= isnull(b.ach_origin_dfi, b.immediate_origin)
from	banks b, config cnf
where	b.bank				= @bank

-- Find the information from the epay creditors table
select	@creditor_name			= biller_name,
	@creditor_industry		= type
from	epay_biller_ids
where	epay_biller_id			= @epay_biller_id;

-- Find the information from the client table
select	@client				= cc.client,
	@creditor			= cc.creditor,
	@first_payment_date_under_dmp	= coalesce(pr.proposed_start_date, cc.start_date, c.start_date),
	@proposed_creditor_payment	= coalesce(pr.proposed_amount, cc.disbursement_factor),
	@customer_biller_account_number	= cc.account_number,
	@client_name			= isnull(cc.client_name, dbo.format_normal_name ( default, pn.first, pn.middle, pn.last, pn.suffix )),
	@client_account_balance		= coalesce(pr.proposed_balance, isnull(bal.orig_balance,0) + isnull(bal.orig_balance_adjustment,0) + isnull(bal.total_interest,0) - isnull(bal.total_payments,0)),
	@date_of_balance		= isnull(b.date_transmitted, getdate())
from	client_creditor_proposals pr
inner join client_creditor cc on pr.client_creditor = cc.client_creditor
inner join clients c on cc.client = c.client
left outer join people p on cc.client = p.client and 1 = p.relation
left outer join names pn with (nolock) on p.nameid = pn.name
left outer join proposal_batch_ids b on pr.proposal_batch_id = b.proposal_batch_id
left outer join client_creditor_balances bal on cc.client_creditor_balance = bal.client_creditor_balance
where	pr.client_creditor_proposal	= @client_creditor_proposal;

-- Determine if the creditor is given full or summary information
select	@dmp_type_indicator			= case when isnull(full_disclosure,0) = 1 then 'F'
						       when isnull(full_disclosure,0) = 0 then 'S'
						       else 'N'
						  end
from	creditors with (nolock)
where	creditor				= @creditor;

-- Determine the sign of the values
if @client_account_balance >= 0
	select	@client_account_balance_sign	= 'C'
else
	select	@client_account_balance_sign	= 'D',
		@client_account_balance		= 0 - @client_account_balance

-- Find the "static" information
select	@creditor_outstanding_balance		= @client_account_balance,
	@creditor_outstanding_balance_sign	= @client_account_balance_sign,
	@transaction_type			= 'DC',
	@agency_product_id			= ' ',
	@local_biller_id			= ''

-- Return the results
select	@agency_name				as 'agency_name',
	@agency_id				as 'agency_id',
	@request_date				as 'request_date',
	@epay_biller_id				as 'creditor_id',
	@creditor_name				as 'creditor_name',
	@creditor_record_number			as 'creditor_record_number',
	@creditor_industry			as 'creditor_industry',
	@first_payment_date_under_dmp		as 'first_payment_date_under_dmp',
	@proposed_creditor_payment		as 'proposed_creditor_payment',
	@customer_biller_account_number		as 'customer_biller_account_number',
	@client					as 'client',
	@client_name				as 'client_name',
	@client_account_balance			as 'client_account_balance',
	@creditor_outstanding_balance		as 'creditor_outstanding_balance',
	@creditor_outstanding_balance_sign	as 'creditor_outstanding_balance_sign',
	@client_account_balance_sign		as 'client_account_balance_sign',
	@date_of_balance			as 'date_of_balance',
	@dmp_type_indicator			as 'dmp_type_indicator',
	@transaction_type			as 'transaction_type',
	@agency_product_id			as 'agency_product_id',
	@local_biller_id			as 'local_biller_id'

return ( 1 )
GO
