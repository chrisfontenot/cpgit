USE [DebtPlus]
GO
/****** Object:  StoredProcedure [dbo].[xpr_postdisbursement_debt]    Script Date: 09/15/2014 13:13:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[xpr_postdisbursement_debt] ( @disbursement_register AS INT, @tran_type AS VarChar(2), @client_creditor AS int, @gross AS Money, @creditor_type AS Varchar(1) = 'N', @fairshare_pct AS Float = 0.0, @trust_register AS INT ) AS

-- ======================================================================================================================
-- ==            Update the debt information for the disbursement                                                      ==
-- ======================================================================================================================

-- ChangeLog
--   1/5/2002
--      Updated for new column names of "last_payment" rather than "last_payment_date" and "last_payment_amount"
--   9/31/2002
--     Added support for client_creditor_balances table
--   10/15/2002
--     Added support for client_creditor's "*_this_creditor" information

-- Do not generate result sets for now
SET NOCOUNT ON

-- Start the transaction block for this item
BEGIN TRANSACTION

-- Account number at the current time
DECLARE	@account_number		varchar(22)
DECLARE	@creditor			varchar(10)
DECLARE	@client				int
SELECT	@account_number	= account_number,
		@client			= client,
		@creditor		= creditor
FROM	client_creditor cc WITH (NOLOCK)
WHERE	client_creditor	= @client_creditor

-- Ensure that the fairshare rate is proper
WHILE @fairshare_pct >= 1.0
	SET @fairshare_pct = @fairshare_pct / 100.0

-- Calculate the fairshare amount
-- by casting the value to a 12,2 decimal, the fractional amounts of pennies are removed.
DECLARE	@fairshare_amt money
SELECT @fairshare_amt = convert (money, convert ( decimal(12, 2), ( @gross * @fairshare_pct )))

-- The fairshare must never be negative
IF @fairshare_amt < 0
	SET @fairshare_amt = 0

-- Limit the fairshare to the gross amount
IF @fairshare_amt > @gross
	SET @fairshare_amt = @gross

-- If the creditor type is "N" (none) then reset the fairshare to zero.
-- Also make the creditor type "N" if the fairshare rate is zero. (It just looks nicer to have both match.)
IF @creditor_type = 'N'
BEGIN
	SET @fairshare_amt = 0
	SET @fairshare_pct = 0.0
END ELSE
	IF @fairshare_pct = 0.0
		SET @creditor_type = 'N'

-- Calculate the deducted and billed figures for the creditor update
DECLARE	@Deducted money
IF @creditor_type = 'D'
	SET @deducted = @fairshare_amt
ELSE
	SET @deducted = 0

DECLARE @billed				money
DECLARE	@client_creditor_register	int

IF @creditor_type = 'B'
	SET @billed = @fairshare_amt
ELSE
	SET @billed = 0

-- Indicate that the debt was paid
INSERT INTO registers_client_creditor	(tran_type,	client,		creditor,	client_creditor,	debit_amt,	fairshare_amt,	creditor_type,	fairshare_pct,		trust_register,		account_number,		void)
VALUES					(@tran_type,	@client,	@creditor,	@client_creditor,	@gross,		@fairshare_amt,	@creditor_type,	@fairshare_pct,	@trust_register,	@account_number,	0)

SELECT	@client_creditor_register = SCOPE_IDENTITY()

IF @Gross > 0
BEGIN
	-- Update the disbursement information for the current debt
	UPDATE	client_creditor
	SET	last_payment		= @client_creditor_register,
		first_payment		= isnull(first_payment, @client_creditor_register),
		payments_this_creditor	= isnull(payments_this_creditor,0) + @gross
	WHERE	client_creditor		= @client_creditor

	UPDATE	client_creditor_balances
	SET	payments_month_0	= payments_month_0 + @gross,
		total_payments		= total_payments + @gross
	FROM	client_creditor_balances bal
	INNER JOIN client_creditor cc ON bal.client_creditor_balance = cc.client_creditor_balance
	WHERE	cc.client_creditor		= @client_creditor

	-- Update the creditor information with the disbursement detail
	UPDATE	creditors
	SET	distrib_mtd		= distrib_mtd + @gross,
		contrib_mtd_billed	= contrib_mtd_billed + @billed,
		contrib_mtd_received	= contrib_mtd_received + @deducted
	WHERE	creditor		= @creditor

	-- Add the item to the disbursement "check"
	UPDATE	registers_trust
	SET	amount			= amount + @gross - @deducted
	WHERE	trust_register		= @trust_register
END

-- Commit this block as a transaction
COMMIT TRANSACTION

-- Terminate normally
RETURN ( 1 )
GO
