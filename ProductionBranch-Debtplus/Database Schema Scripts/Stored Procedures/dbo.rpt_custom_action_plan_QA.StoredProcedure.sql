USE [DebtPlus]
GO
/****** Object:  StoredProcedure [dbo].[rpt_custom_action_plan_QA]    Script Date: 09/15/2014 13:13:33 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[rpt_custom_action_plan_QA](@From_date as datetime=null,@To_Date as datetime=null) as
if @To_date is null
    select @To_Date = getdate();
if @From_Date is null
    select @From_Date = @To_Date;
select @From_Date = convert(varchar(10), @From_date, 101),
 @To_date = convert(varchar(10), @To_date, 101) + ' 23:59:59';
 
select client, max(date_created) as date_created
into #action_plans
from action_plans
group by client;

select client, max(start_time) as appointment_date
into #appointments
from client_appointments
where office is not null
and status in ('K','W')
group by client;

select isnull(p.client, a.client) as client,
p.date_created as action_plan_date, a.appointment_date, v.name
from #action_plans p
full outer join #appointments a on a.client = p.client
inner join view_client_address v on isnull(p.client,a.client) = v.client
where a.appointment_date between @From_date and @To_Date
order by 1;

drop table #appointments
drop table #action_plans
GO
