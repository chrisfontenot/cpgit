USE [DebtPlus]
GO
/****** Object:  StoredProcedure [dbo].[rpt_PrintClient_Secured]    Script Date: 09/15/2014 13:13:33 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE PROCEDURE [dbo].[rpt_PrintClient_Secured] ( @Client AS INT ) AS

-- ===============================================================================================
-- ==             Fetch the Secured Loans for this client                                       ==
-- ===============================================================================================

SELECT	t.description		as auto_make,
	p.description		as auto_model,
	p.year_mfg		as auto_year,
	l.balance		as auto_balance,
	l.lender		as auto_lender,
	l.account_number	as auto_loan_number,
	l.past_due_amount	as auto_arrears_amount,
	l.past_due_periods	as auto_arrears_months

FROM	secured_loans l		with (nolock)
inner join secured_properties p	with (nolock) on l.secured_property = p.secured_property
inner join secured_types t	with (nolock) on p.secured_type = t.secured_type

where	p.client	= 0
order by 1, 2, 3, 4

return ( @@rowcount )
GO
