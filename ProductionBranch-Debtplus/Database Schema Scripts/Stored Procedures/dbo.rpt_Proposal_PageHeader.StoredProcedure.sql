USE [DebtPlus]
GO
/****** Object:  StoredProcedure [dbo].[rpt_Proposal_PageHeader]    Script Date: 09/15/2014 13:13:33 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE PROCEDURE [dbo].[rpt_Proposal_PageHeader] AS
-- ===========================================================================================
-- ==              Fetch the information for the proposal page header                       ==
-- ===========================================================================================

SELECT c.[name],
	dbo.format_address_line_1(a.house, a.direction, a.street, a.suffix, a.modifier, a.modifier_value) as address1,
	a.address_line_2 as address2,
	dbo.format_city_state_zip (a.city, a.state, a.postalcode ) as address3,
	nfcc_unit_no,
	fed_tax_id
FROM	config c
left outer join addresses a on c.addressid = a.address
where	c.company_id = 1

RETURN ( @@rowcount )
GO
