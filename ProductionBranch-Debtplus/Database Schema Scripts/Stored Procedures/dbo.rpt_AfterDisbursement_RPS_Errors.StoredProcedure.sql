USE [DebtPlus]
GO
/****** Object:  StoredProcedure [dbo].[rpt_AfterDisbursement_RPS_Errors]    Script Date: 09/15/2014 13:13:32 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[rpt_AfterDisbursement_RPS_Errors] AS

-- ==========================================================================================
-- ==               Return a list of the items which are not disbursed due to bad RPPS     ==
-- ==========================================================================================

-- ChangeLog
--   06/09/2009
--      Remove references to debt_number and use client_creditor

SELECT		d.client				as 'client',
		d.creditor				as 'creditor',
		d.client_creditor				as 'client_creditor',
		d.debit_amt				as 'amount',
		d.account_number		as 'account_number',

		case d.creditor_type
			when 'D' then d.fairshare_amt
			else 0
		end					as 'deducted',

		t.biller_id				as 'biller_id'
FROM		rpps_transactions t
LEFT OUTER JOIN	registers_client_creditor d ON t.client_creditor_register = d.client_creditor_register
WHERE		t.trace_number_first IS NULL
AND		t.service_class_or_purpose = 'CIE'
AND		t.transaction_code IN (22, 23)

RETURN ( @@rowcount )
GO
