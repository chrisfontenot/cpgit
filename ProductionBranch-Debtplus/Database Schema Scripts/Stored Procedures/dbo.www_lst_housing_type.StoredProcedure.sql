USE [DebtPlus]
GO
/****** Object:  StoredProcedure [dbo].[www_lst_housing_type]    Script Date: 09/15/2014 13:13:34 ******/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE PROCEDURE [dbo].[www_lst_housing_type] AS

-- ============================================================================================================
-- ==            Provide access to the resource types for the www_user that does not have access to lst_*    ==
-- ============================================================================================================

declare	@result_code int
execute @result_code = lst_housing_type
return ( @result_code )
GO
