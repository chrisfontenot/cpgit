USE [DebtPlus]
GO
/****** Object:  StoredProcedure [dbo].[rpt_custom_money_in_trust_more_than_14_days]    Script Date: 09/15/2014 13:13:33 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[rpt_custom_money_in_trust_more_than_14_days] as

-- Do not generate intermediate results for the period
set nocount on;

select	c.client, v.name, c.active_status, c.start_date, c.last_deposit_amount, c.held_in_trust, datediff(d, c.last_deposit_date, getdate()) as held_days, c.last_deposit_date as date_deposited
from	clients c
left outer join view_client_address v on c.client = v.client
where	c.last_deposit_date < dateadd(d, -14, getdate())
and		c.held_in_trust > 0

order by 1, 5;

return ( 0 );
GO
