USE [DebtPlus]
GO
/****** Object:  StoredProcedure [dbo].[cmd_Special_Correct_LB_deposits]    Script Date: 09/15/2014 13:13:32 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[cmd_Special_Correct_LB_deposits] ( @deposit_batch_id as int ) as

-- Special routine for Richmond/Seattle

-- Resolve deposits for lockbox items by the social security number between the two systems.

-- Mark the items as "not ok to post"
update	deposit_batch_details
set	ok_to_post		= 0
where	deposit_batch_id	= @deposit_batch_id

-- Change the message to the client is not on the file
update	deposit_batch_details
set	message			= 'Client Not on file'
where	message			= 'Not on file'
and	deposit_batch_id	= @deposit_batch_id

-- Add a default message for all non-marked items
update	deposit_batch_details
set	message			= 'SSN not on file'
where	deposit_batch_id	= @deposit_batch_id
and	message is null

-- Lookup the client by the SSN field and fill in the client, permit the posting, and clear the error
update	deposit_batch_details
set	ok_to_post		= 1,
	client			= p.client,
	message			= null
from	deposit_batch_details d
inner join people p on d.reference = p.ssn
where	d.deposit_batch_id	= @deposit_batch_id
GO
