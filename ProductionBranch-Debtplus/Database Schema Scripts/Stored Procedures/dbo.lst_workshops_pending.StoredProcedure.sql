USE [DebtPlus]
GO
/****** Object:  StoredProcedure [dbo].[lst_workshops_pending]    Script Date: 09/15/2014 13:13:32 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[lst_workshops_pending] as

-- ===============================================================================================
-- ==            List of the workshops which have pending clients                               ==
-- ===============================================================================================

select	w.workshop					as 'item_key',
	w.start_time					as 'start_time',
	wl.name						as 'location',
	wt.description					as 'description'
from	workshops w with (nolock)
left outer join workshop_locations wl with (nolock) on w.workshop_location = wl.workshop_location
left outer join workshop_types wt with (nolock) on w.workshop_type = wt.workshop_type
where	workshop in (
	select workshop
	from	client_appointments with (nolock)
	where	workshop is not null
	and	status = 'P'
)
and	w.start_time < getdate()
order by 2 desc

return ( @@rowcount )
GO
