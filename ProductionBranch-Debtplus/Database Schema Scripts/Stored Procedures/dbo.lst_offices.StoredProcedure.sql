USE [DebtPlus]
GO
/****** Object:  StoredProcedure [dbo].[lst_offices]    Script Date: 09/15/2014 13:13:32 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[lst_offices] as

-- ChangeLog
--   09/12/2008
--      Changes for .NET office list control

select office as item_key, [name] as description, [Default], [ActiveFlag], case [type] when 1 then 'Main' when 2 then 'Satellite' when 3 then 'Desk' else 'Other' end as TypeName
from offices with (nolock)
order by 2

return ( @@rowcount )
GO
