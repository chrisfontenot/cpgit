USE [DebtPlus]
GO
/****** Object:  StoredProcedure [dbo].[xpr_insert_document]    Script Date: 09/15/2014 13:13:35 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create procedure [dbo].[xpr_insert_document](@description as varchar(80) = null) as

-- ================================================================================
-- ==          Create the entry in the Documents table                           ==
-- ================================================================================

	-- Insert the document
	insert into documents (description) values (@description)
	return scope_identity()
GO
