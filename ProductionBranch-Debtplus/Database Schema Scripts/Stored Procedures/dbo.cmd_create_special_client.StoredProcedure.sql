USE [DebtPlus]
GO
/****** Object:  StoredProcedure [dbo].[cmd_create_special_client]    Script Date: 09/15/2014 13:13:31 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE PROCEDURE [dbo].[cmd_create_special_client] ( @client as int ) as

-- ==========================================================================
-- ==            Create a client given the client ID                       ==
-- ==========================================================================

if exists (select * from clients where client = @client)
begin
	RaisError ('The client is currently defined in the system and may not be duplicated', 16, 1)
	return ( 0 )
end

-- Ensure that the client is before the first "real" client
declare	@highest_client	int
select	@highest_client = max(client)
from	clients

if @highest_client < @client
begin
	RaisError ('The client is greater than the last client (%d) in the system and can not be created by this routine', 16, 1)
	return ( 0 )
end

-- Create the client
set identity_insert clients on
insert into clients (client, config_fee) values (@client, 1)
set identity_insert clients off

-- Validate the database identity
dbcc checkident (clients);
return ( 1 )
GO
