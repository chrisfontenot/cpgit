USE [DebtPlus]
GO
/****** Object:  StoredProcedure [dbo].[lst_deposit_batches_open]    Script Date: 09/15/2014 13:13:32 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE PROCEDURE [dbo].[lst_deposit_batches_open] AS

-- ===================================================================================================
-- ==                Return the list of open deposit batches                                        ==
-- ===================================================================================================

-- ChangeLog
--   6/16/2004
--      Allow for batches with no deposits. oops.

SELECT		ids.deposit_batch_id				as 'item_key',
		ids.date_created				as 'date_created',
		dbo.format_counselor_name ( ids.created_by )	as 'created_by',
		convert(varchar(50),isnull(ids.note,''))	as 'note',
		convert(money,isnull(sum(d.amount),0))		as 'amount'
FROM		deposit_batch_ids ids WITH ( NOLOCK )
LEFT OUTER JOIN	deposit_batch_details d WITH (NOLOCK) ON d.deposit_batch_id = ids.deposit_batch_id
WHERE		ids.date_closed IS NULL
AND		ids.date_posted IS NULL
AND		ids.batch_type = 'CL'

-- Do not count the offsetting transaction record
AND		(ids.batch_type <> 'AC' or d.ach_transaction_code in (27, 37))

GROUP BY	ids.deposit_batch_id, ids.date_created, ids.created_by, ids.note
 
return ( @@rowcount )
GO
