USE [DebtPlus]
GO
/****** Object:  StoredProcedure [dbo].[xpr_insert_budget_categories]    Script Date: 09/15/2014 13:13:35 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[xpr_insert_budget_categories] ( @description as varchar(50), @auto_factor as money = null, @person_factor as money = null, @maximum_factor as money = null, @heading as bit = 0, @detail as bit = 1, @living_expense as bit = 1, @housing_expense as bit = 0, @rpps_code as smallint = 6 ) as
-- ========================================================================================
-- ==   Procedure for .NET to insert items into the budget_categories table              ==
-- ========================================================================================
	insert into budget_categories ( [description], [auto_factor], [person_factor], [maximum_factor], [heading], [detail], [living_expense], [housing_expense], [rpps_code] ) values ( @description, @auto_factor, @person_factor, @maximum_factor, @heading, @detail, @living_expense, @housing_expense, @rpps_code )
	return ( scope_identity() )
GO
