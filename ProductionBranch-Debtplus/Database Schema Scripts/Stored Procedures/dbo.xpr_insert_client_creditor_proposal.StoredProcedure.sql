USE [DebtPlus]
GO
/****** Object:  StoredProcedure [dbo].[xpr_insert_client_creditor_proposal]    Script Date: 09/15/2014 13:13:35 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[xpr_insert_client_creditor_proposal] (@client_creditor as int, @proposal_status as int = 1, @proposal_print_date as datetime = null, @proposal_accepted_by as varchar(50) = null, @proposal_status_date as datetime = null, @proposal_reject_reason as int = null, @proposal_reject_disp as int = null, @counter_amount as money = null, @missing_item as varchar(50) = null, @percent_balance as float = null, @terms as int = null, @proposed_amount as money = null, @proposed_start_date as datetime = null, @proposal_batch_id as int = null, @proposed_balance as money = null, @proposed_date as datetime = null, @full_disclosure as bit = 0, @bank as int = null, @rpps_biller_id as varchar(10) = null, @epay_biller_id as varchar(50) = null, @proposal_note as text = null) as

	-- =======================================================================================
	-- ==          .NET interface to insert a proposal into the system                      ==
	-- =======================================================================================
	
	IF @proposal_status NOT IN (0, 1, 2, 3)
	BEGIN
		RaisError (50052, 16, 1)
		Return ( 0 )
	END

	DECLARE	@client_creditor_proposal	int
	
	INSERT INTO client_creditor_proposals	( client_creditor,	proposal_status,	proposal_batch_id, proposed_date )
	VALUES									( @client_creditor,	@proposal_status,	null, GETDATE() )
	
	SELECT	@client_creditor_proposal = SCOPE_IDENTITY()

	UPDATE	client_creditor
	SET		client_creditor_proposal	= @client_creditor_proposal
	WHERE	client_creditor				= @client_creditor
	
	-- If there is a note then update the debt note for the proposal
	if @proposal_note is not null
		insert into debt_notes (type, client, creditor, client_creditor, client_creditor_proposal,  note_text,      note_amount, note_date)
						select	'PR', client, creditor, client_creditor, @client_creditor_proposal, @proposal_note, 0,           GETDATE()
						from	client_creditor with (nolock)
						where	client_creditor = @client_creditor
						
	-- Update the proposal with the corresponding new values
	update	client_creditor_proposals
	set		proposal_status				= @proposal_status,
			proposal_print_date			= @proposal_print_date,
			proposal_accepted_by		= @proposal_accepted_by,
			proposal_status_date		= @proposal_status_date,
			proposal_reject_reason		= @proposal_reject_reason,
			proposal_reject_disp		= @proposal_reject_disp,
			counter_amount				= @counter_amount,
			missing_item				= @missing_item,
			percent_balance				= @percent_balance,
			terms						= @terms,
			proposed_amount				= @proposed_amount,
			proposed_start_date			= @proposed_start_date,
			proposal_batch_id			= @proposal_batch_id,
			proposed_balance			= @proposed_balance,
			proposed_date				= isnull(@proposed_date,getdate()),
			full_disclosure				= @full_disclosure,
			bank						= @bank,
			rpps_biller_id				= @rpps_biller_id,
			epay_biller_id				= @epay_biller_id
	where	client_creditor_proposal	= @client_creditor_proposal
		
	return ( @client_creditor_proposal )
GO
