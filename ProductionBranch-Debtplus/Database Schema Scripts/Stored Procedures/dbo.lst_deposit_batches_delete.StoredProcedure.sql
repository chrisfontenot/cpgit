USE [DebtPlus]
GO
/****** Object:  StoredProcedure [dbo].[lst_deposit_batches_delete]    Script Date: 09/15/2014 13:13:32 ******/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE PROCEDURE [dbo].[lst_deposit_batches_delete] AS

-- ===================================================================================================
-- ==                Return the list of deposit batches that may be deleted                         ==
-- ===================================================================================================

-- ChangeLog
--   7/13/2004
--      Do not count the ACH offsetting amount in the deposit batch totals

SELECT		ids.deposit_batch_id				as 'item_key',
		ids.date_created				as 'date_created',
		dbo.format_counselor_name ( ids.created_by )	as 'created_by',
		case
			when ids.batch_type = 'CR' then 'CR' + isnull(' '+ids.note,'')
			else isnull(ids.note,'')
		end						as 'note',
		convert(money,isnull(sum(d.amount),0))		as 'amount'
FROM		deposit_batch_ids ids WITH ( NOLOCK )
LEFT OUTER JOIN	deposit_batch_details d WITH (NOLOCK) ON d.deposit_batch_id = ids.deposit_batch_id

WHERE		(ids.date_posted is null)
AND		(ids.batch_type = 'CR' or ids.date_closed is not null)

-- Do not count the offsetting transaction record
AND		(ids.batch_type <> 'AC' or d.ach_transaction_code in (27, 37))

GROUP BY	ids.deposit_batch_id, ids.date_created, ids.created_by, ids.note, ids.batch_type

return ( @@rowcount )
GO
