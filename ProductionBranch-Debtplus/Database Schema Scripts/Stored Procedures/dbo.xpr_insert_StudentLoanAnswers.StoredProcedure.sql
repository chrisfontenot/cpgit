USE [DebtPlus]
GO
/****** Object:  StoredProcedure [dbo].[xpr_insert_StudentLoanAnswers]    Script Date: 09/15/2014 13:13:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create procedure [dbo].[xpr_insert_StudentLoanAnswers]( @StudentLoan int, @QuestionID int, @AnswerID int ) as
-- =====================================================================================================
-- ==            Insert a row into the StudentLoanAnswers table                                       ==
-- =====================================================================================================
insert into StudentLoanAnswers ( StudentLoan, QuestionID, AnswerID ) values ( @StudentLoan, @QuestionID, @AnswerID )
return ( scope_identity() )
GO
