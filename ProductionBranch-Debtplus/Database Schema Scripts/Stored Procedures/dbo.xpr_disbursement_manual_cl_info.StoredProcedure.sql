USE [DebtPlus]
GO
/****** Object:  StoredProcedure [dbo].[xpr_disbursement_manual_cl_info]    Script Date: 09/15/2014 13:13:35 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[xpr_disbursement_manual_cl_info] ( @client_creditor as int ) AS

-- =====================================================================================================================
-- ==            Obtain information about the indicated debt                                                          ==
-- =====================================================================================================================

-- ChangeLog
--   06/09/2009
--      Remove references to debt_number and use client_creditor

-- Retrieve the client information as needed
	SELECT 		cc.client_creditor,
			v.client_name as 'name',

			isnull(cc.disbursement_factor, 0) as 'disbursement_factor',
			coalesce(cc.fairshare_pct_check, pct.fairshare_pct_check,0) as 'fairshare'

	FROM		client_creditor cc	WITH (NOLOCK)
	LEFT OUTER JOIN view_last_payment v	WITH (NOLOCK) ON cc.client_creditor = v.client_creditor
	LEFT OUTER JOIN	creditors cr		WITH (NOLOCK) ON cc.creditor = cr.creditor
	LEFT OUTER JOIN creditor_contribution_pcts pct with (nolock) on cr.creditor_contribution_pct = pct.creditor_contribution_pct

	WHERE		cc.client_creditor	= @client_creditor
	AND		isnull(cc.reassigned_debt,0) = 0

-- Return success or failure to the caller
RETURN ( @@rowcount )
GO
