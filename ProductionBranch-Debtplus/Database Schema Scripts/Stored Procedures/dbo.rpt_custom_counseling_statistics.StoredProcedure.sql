USE [DebtPlus]
GO
/****** Object:  StoredProcedure [dbo].[rpt_custom_counseling_statistics]    Script Date: 09/15/2014 13:13:33 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[rpt_custom_counseling_statistics] AS

-- Find the period dates
declare	@last_year	datetime
declare	@this_year	datetime
declare	@this_month	datetime
declare	@last_month	datetime
declare	@current_time	datetime
select	@current_time	= convert(varchar(10), getdate(), 101)

select	@this_year	= '01/01/' + convert(varchar(4), year(@current_time))
select	@last_year	= dateadd(yy, -1, @this_year)
select	@this_month	= convert(varchar, month(@current_time)) + '/01/' + convert(varchar, year(@current_time))
select	@last_month	= dateadd(m, -1, @this_month)

select	@current_time	= convert(varchar(10), @current_time, 101) + ' 23:59:59'

-- Find the appointment information
select	convert(int,0) as grp, ca.client, ca.start_time, ca.appt_type, w.workshop_type, ca.status, ca.result, isnull(aptm.nfcc,'F') as contact_type, apt.initial_appt, datediff(minute,ca.[start_time],ca.[end_time]) as appt_duration, wt.duration as w_duration, convert(int,0) as pre_bankruptcy, convert(int,0) as post_bankruptcy, convert(int,0) as housing
into	#appts
from	client_appointments ca
left outer join appt_types apt on ca.appt_type = apt.appt_type
left outer join FirstContactTypes aptm on apt.contact_type = aptm.oID
left outer join workshops w on ca.workshop = w.workshop
left outer join workshop_types wt on w.workshop_type = wt.workshop_type
where	ca.start_time between @last_year and @current_time

-- Items for this year
update	#appts
set	grp = 1
where	start_time >= @this_year;

-- Items for last month
update	#appts
set	grp = 2
where	start_time >= @last_month;

-- Items for this month
update	#appts
set	grp = 3
where	start_time >= @this_month;

-- Mark the counseling appointments as housing and bankruptcy
update	#appts
set	housing		= 1
where	appt_type in (12, 11, 18, 13)

update	#appts
set	pre_bankruptcy	= 1
where	appt_type in (14, 15)

update	#appts
set	post_bankruptcy	= 1
where	appt_type in (16, 19)

-- Do the same to the workshops
update	#appts
set	post_bankruptcy	= 1
where	workshop_type in (51)

-- Mark the workshops as bankruptcy if needed
update	#appts
set	housing		= 1
where	workshop_type	in (42, 43);

-- Discard all workshop appointments that are not bankruptcy related
delete
from	#appts
where	workshop_type is not null
and	pre_bankruptcy	= 0
and	post_bankruptcy	= 0
and	housing		= 0

-- Create the table for the results
create table #results ( grp varchar(256) null, description varchar(256) null, monthly int null, last_month int null, ytd int null, last_year int null);

-- Load the Financial counsling appointments
insert into #results
select	'Financial Counseling' as grp,
	'Appointments Scheduled' as description,
	(select	count(*) from #appts where grp = 3  and appt_type in (1, 2, 9, 10) and status <> 'R') as monthly,
	(select	count(*) from #appts where grp = 2  and appt_type in (1, 2, 9, 10) and status <> 'R') as last_month,
	(select	count(*) from #appts where grp <> 0 and appt_type in (1, 2, 9, 10) and status <> 'R') as ytd,
	(select	count(*) from #appts where grp = 0  and appt_type in (1, 2, 9, 10) and status <> 'R') as last_ytd

-- Load the rescheduled appointments
insert into #results
select	'Financial Counseling' as grp,
	'Client Rescheduled Appointment' as description,
	(select	count(*) from #appts where grp = 3  and appt_type in (1, 2, 9, 10) and status = 'R') as monthly,
	(select	count(*) from #appts where grp = 2  and appt_type in (1, 2, 9, 10) and status = 'R') as last_month,
	(select	count(*) from #appts where grp <> 0 and appt_type in (1, 2, 9, 10) and status = 'R') as ytd,
	(select	count(*) from #appts where grp = 0  and appt_type in (1, 2, 9, 10) and status = 'R') as last_ytd

-- Load the cancelled appointments
insert into #results
select	'Financial Counseling' as grp,
	'Client Cancelled Appointment' as description,
	(select	count(*) from #appts where grp = 3  and appt_type in (1, 2, 9, 10) and status = 'C') as monthly,
	(select	count(*) from #appts where grp = 2  and appt_type in (1, 2, 9, 10) and status = 'C') as last_month,
	(select	count(*) from #appts where grp <> 0 and appt_type in (1, 2, 9, 10) and status = 'C') as ytd,
	(select	count(*) from #appts where grp = 0  and appt_type in (1, 2, 9, 10) and status = 'C') as last_ytd

-- Load the no-show appointments
insert into #results
select	'Financial Counseling' as grp,
	'Client Was a No-Show' as description,
	(select	count(*) from #appts where grp = 3  and appt_type in (1, 2, 9, 10) and status = 'M') as monthly,
	(select	count(*) from #appts where grp = 2  and appt_type in (1, 2, 9, 10) and status = 'M') as last_month,
	(select	count(*) from #appts where grp <> 0 and appt_type in (1, 2, 9, 10) and status = 'M') as ytd,
	(select	count(*) from #appts where grp = 0  and appt_type in (1, 2, 9, 10) and status = 'M') as last_ytd

-- Load the kept appointments
insert into #results
select	'Financial Counseling' as grp,
	'Number of Appointments Kept' as description,
	(select	count(*) from #appts where grp = 3  and appt_type in (1, 2, 9, 10) and status in ('K','W')) as monthly,
	(select	count(*) from #appts where grp = 2  and appt_type in (1, 2, 9, 10) and status in ('K','W')) as last_month,
	(select	count(*) from #appts where grp <> 0 and appt_type in (1, 2, 9, 10) and status in ('K','W')) as ytd,
	(select	count(*) from #appts where grp = 0  and appt_type in (1, 2, 9, 10) and status in ('K','W')) as last_ytd

-- Load the budgeting appointment types scheduled
insert into #results
select	'Financial Counseling' as grp,
	'One-On-One Budgeting Sessions Scheduled' as description,
	(select	count(*) from #appts where grp = 3  and appt_type in (3,4) and status <> 'R') as monthly,
	(select	count(*) from #appts where grp = 2  and appt_type in (3,4) and status <> 'R') as last_month,
	(select	count(*) from #appts where grp <> 0 and appt_type in (3,4) and status <> 'R') as ytd,
	(select	count(*) from #appts where grp = 0  and appt_type in (3,4) and status <> 'R') as last_ytd

-- Load the budgeting appointment types kept
insert into #results
select	'Financial Counseling' as grp,
	'One-On-One Budgeting Sessions Completed' as description,
	(select	count(*) from #appts where grp = 3  and appt_type in (3,4) and status in ('K','W')) as monthly,
	(select	count(*) from #appts where grp = 2  and appt_type in (3,4) and status in ('K','W')) as last_month,
	(select	count(*) from #appts where grp <> 0 and appt_type in (3,4) and status in ('K','W')) as ytd,
	(select	count(*) from #appts where grp = 0  and appt_type in (3,4) and status in ('K','W')) as last_ytd

-- Load the budgeting appointment types scheduled
insert into #results
select	'Financial Counseling' as grp,
	'Credit Report Reviews Scheduled' as description,
	(select	count(*) from #appts where grp = 3  and appt_type in (5) and status <> 'R') as monthly,
	(select	count(*) from #appts where grp = 2  and appt_type in (5) and status <> 'R') as last_month,
	(select	count(*) from #appts where grp <> 0 and appt_type in (5) and status <> 'R') as ytd,
	(select	count(*) from #appts where grp = 0  and appt_type in (5) and status <> 'R') as last_ytd

-- Load the budgeting appointment types kept
insert into #results
select	'Financial Counseling' as grp,
	'Credit Report Reviews Completed' as description,
	(select	count(*) from #appts where grp = 3  and appt_type in (5) and status in ('K','W')) as monthly,
	(select	count(*) from #appts where grp = 2  and appt_type in (5) and status in ('K','W')) as last_month,
	(select	count(*) from #appts where grp <> 0 and appt_type in (5) and status in ('K','W')) as ytd,
	(select	count(*) from #appts where grp = 0  and appt_type in (5) and status in ('K','W')) as last_ytd

-- Pre-File Bankruptcy Counseling
insert into #results
select	'Pre-File Bankruptcy Counseling' as grp,
	'Appointments Scheduled' as description,
	(select	count(*) from #appts where grp = 3  and housing = 0 and pre_bankruptcy = 1 and post_bankruptcy = 0 and status <> 'R') as monthly,
	(select	count(*) from #appts where grp = 2  and housing = 0 and pre_bankruptcy = 1 and post_bankruptcy = 0 and status <> 'R') as last_month,
	(select	count(*) from #appts where grp <> 0 and housing = 0 and pre_bankruptcy = 1 and post_bankruptcy = 0 and status <> 'R') as ytd,
	(select	count(*) from #appts where grp = 0  and housing = 0 and pre_bankruptcy = 1 and post_bankruptcy = 0 and status <> 'R') as last_ytd

-- Load the rescheduled appointments
insert into #results
select	'Pre-File Bankruptcy Counseling' as grp,
	'Client Rescheduled Appointment' as description,
	(select	count(*) from #appts where grp = 3  and housing = 0 and pre_bankruptcy = 1 and post_bankruptcy = 0 and status = 'R') as monthly,
	(select	count(*) from #appts where grp = 2  and housing = 0 and pre_bankruptcy = 1 and post_bankruptcy = 0 and status = 'R') as last_month,
	(select	count(*) from #appts where grp <> 0 and housing = 0 and pre_bankruptcy = 1 and post_bankruptcy = 0 and status = 'R') as ytd,
	(select	count(*) from #appts where grp = 0  and housing = 0 and pre_bankruptcy = 1 and post_bankruptcy = 0 and status = 'R') as last_ytd

-- Load the cancelled appointments
insert into #results
select	'Pre-File Bankruptcy Counseling' as grp,
	'Client Cancelled Appointment' as description,
	(select	count(*) from #appts where grp = 3  and housing = 0 and pre_bankruptcy = 1 and post_bankruptcy = 0 and status = 'C') as monthly,
	(select	count(*) from #appts where grp = 2  and housing = 0 and pre_bankruptcy = 1 and post_bankruptcy = 0 and status = 'C') as last_month,
	(select	count(*) from #appts where grp <> 0 and housing = 0 and pre_bankruptcy = 1 and post_bankruptcy = 0 and status = 'C') as ytd,
	(select	count(*) from #appts where grp = 0  and housing = 0 and pre_bankruptcy = 1 and post_bankruptcy = 0 and status = 'C') as last_ytd

-- Load the no-show appointments
insert into #results
select	'Pre-File Bankruptcy Counseling' as grp,
	'Client Was a No-Show' as description,
	(select	count(*) from #appts where grp = 3  and housing = 0 and pre_bankruptcy = 1 and post_bankruptcy = 0 and status = 'M') as monthly,
	(select	count(*) from #appts where grp = 2  and housing = 0 and pre_bankruptcy = 1 and post_bankruptcy = 0 and status = 'M') as last_month,
	(select	count(*) from #appts where grp <> 0 and housing = 0 and pre_bankruptcy = 1 and post_bankruptcy = 0 and status = 'M') as ytd,
	(select	count(*) from #appts where grp = 0  and housing = 0 and pre_bankruptcy = 1 and post_bankruptcy = 0 and status = 'M') as last_ytd

-- Load the kept appointments
insert into #results
select	'Pre-File Bankruptcy Counseling' as grp,
	'Number of Appointments Kept' as description,
	(select	count(*) from #appts where grp = 3  and housing = 0 and pre_bankruptcy = 1 and post_bankruptcy = 0 and status in ('K','W')) as monthly,
	(select	count(*) from #appts where grp = 2  and housing = 0 and pre_bankruptcy = 1 and post_bankruptcy = 0 and status in ('K','W')) as last_month,
	(select	count(*) from #appts where grp <> 0 and housing = 0 and pre_bankruptcy = 1 and post_bankruptcy = 0 and status in ('K','W')) as ytd,
	(select	count(*) from #appts where grp = 0  and housing = 0 and pre_bankruptcy = 1 and post_bankruptcy = 0 and status in ('K','W')) as last_ytd

-- Pre-File Bankruptcy Counseling
insert into #results
select	'Pre-Discharge Bankruptcy Counseling' as grp,
	'Appointments Scheduled' as description,
	(select	count(*) from #appts where grp = 3  and housing = 0 and pre_bankruptcy = 0 and post_bankruptcy = 1 and status <> 'R') as monthly,
	(select	count(*) from #appts where grp = 2  and housing = 0 and pre_bankruptcy = 0 and post_bankruptcy = 1 and status <> 'R') as last_month,
	(select	count(*) from #appts where grp <> 0 and housing = 0 and pre_bankruptcy = 0 and post_bankruptcy = 1 and status <> 'R') as ytd,
	(select	count(*) from #appts where grp = 0  and housing = 0 and pre_bankruptcy = 0 and post_bankruptcy = 1 and status <> 'R') as last_ytd

-- Load the rescheduled appointments
insert into #results
select	'Pre-Discharge Bankruptcy Counseling' as grp,
	'Client Rescheduled Appointment' as description,
	(select	count(*) from #appts where grp = 3  and housing = 0 and pre_bankruptcy = 0 and post_bankruptcy = 1 and status = 'R') as monthly,
	(select	count(*) from #appts where grp = 2  and housing = 0 and pre_bankruptcy = 0 and post_bankruptcy = 1 and status = 'R') as last_month,
	(select	count(*) from #appts where grp <> 0 and housing = 0 and pre_bankruptcy = 0 and post_bankruptcy = 1 and status = 'R') as ytd,
	(select	count(*) from #appts where grp = 0  and housing = 0 and pre_bankruptcy = 0 and post_bankruptcy = 1 and status = 'R') as last_ytd

-- Load the cancelled appointments
insert into #results
select	'Pre-Discharge Bankruptcy Counseling' as grp,
	'Client Cancelled Appointment' as description,
	(select	count(*) from #appts where grp = 3  and housing = 0 and pre_bankruptcy = 0 and post_bankruptcy = 1 and status = 'C') as monthly,
	(select	count(*) from #appts where grp = 2  and housing = 0 and pre_bankruptcy = 0 and post_bankruptcy = 1 and status = 'C') as last_month,
	(select	count(*) from #appts where grp <> 0 and housing = 0 and pre_bankruptcy = 0 and post_bankruptcy = 1 and status = 'C') as ytd,
	(select	count(*) from #appts where grp = 0  and housing = 0 and pre_bankruptcy = 0 and post_bankruptcy = 1 and status = 'C') as last_ytd

-- Load the no-show appointments
insert into #results
select	'Pre-Discharge Bankruptcy Counseling' as grp,
	'Client Was a No-Show' as description,
	(select	count(*) from #appts where grp = 3  and housing = 0 and pre_bankruptcy = 0 and post_bankruptcy = 1 and status = 'M') as monthly,
	(select	count(*) from #appts where grp = 2  and housing = 0 and pre_bankruptcy = 0 and post_bankruptcy = 1 and status = 'M') as last_month,
	(select	count(*) from #appts where grp <> 0 and housing = 0 and pre_bankruptcy = 0 and post_bankruptcy = 1 and status = 'M') as ytd,
	(select	count(*) from #appts where grp = 0  and housing = 0 and pre_bankruptcy = 0 and post_bankruptcy = 1 and status = 'M') as last_ytd

-- Load the kept appointments
insert into #results
select	'Pre-Discharge Bankruptcy Counseling' as grp,
	'Number of Appointments Kept' as description,
	(select	count(*) from #appts where grp = 3  and housing = 0 and pre_bankruptcy = 0 and post_bankruptcy = 1 and status in ('K','W')) as monthly,
	(select	count(*) from #appts where grp = 2  and housing = 0 and pre_bankruptcy = 0 and post_bankruptcy = 1 and status in ('K','W')) as last_month,
	(select	count(*) from #appts where grp <> 0 and housing = 0 and pre_bankruptcy = 0 and post_bankruptcy = 1 and status in ('K','W')) as ytd,
	(select	count(*) from #appts where grp = 0  and housing = 0 and pre_bankruptcy = 0 and post_bankruptcy = 1 and status in ('K','W')) as last_ytd

-- Housing information
insert into #results
select	'Housing Programs' as grp,
	'Pre-Purchase Class Participants - CCCS' as description,
	(select	count(*) from #appts where grp = 3  and workshop_type in (42,43) and status in ('K','W')) as monthly,
	(select	count(*) from #appts where grp = 2  and workshop_type in (42,43) and status in ('K','W')) as last_month,
	(select	count(*) from #appts where grp <> 0 and workshop_type in (42,43) and status in ('K','W')) as ytd,
	(select	count(*) from #appts where grp = 0  and workshop_type in (42,43) and status in ('K','W')) as last_ytd

insert into #results
select	'Housing Programs' as grp,
	'Pre-Purchase Counseling - One on One' as description,
	(select	count(*) from #appts where grp = 3  and workshop_type is null and appt_type = 12 and status in ('K','W')) as monthly,
	(select	count(*) from #appts where grp = 2  and workshop_type is null and appt_type = 12 and status in ('K','W')) as last_month,
	(select	count(*) from #appts where grp <> 0 and workshop_type is null and appt_type = 12 and status in ('K','W')) as ytd,
	(select	count(*) from #appts where grp = 0  and workshop_type is null and appt_type = 12 and status in ('K','W')) as last_ytd

insert into #results
select	'Housing Programs' as grp,
	'Reverse Mortgages' as description,
	(select	count(*) from #appts where grp = 3  and workshop_type is null and appt_type in (11,18) and status in ('K','W')) as monthly,
	(select	count(*) from #appts where grp = 2  and workshop_type is null and appt_type in (11,18) and status in ('K','W')) as last_month,
	(select	count(*) from #appts where grp <> 0 and workshop_type is null and appt_type in (11,18) and status in ('K','W')) as ytd,
	(select	count(*) from #appts where grp = 0  and workshop_type is null and appt_type in (11,18) and status in ('K','W')) as last_ytd

insert into #results
select	'Housing Programs' as grp,
	'Delinquency Counseling' as description,
	(select	count(*) from #appts where grp = 3  and workshop_type is null and appt_type in (13) and status in ('K','W')) as monthly,
	(select	count(*) from #appts where grp = 2  and workshop_type is null and appt_type in (13) and status in ('K','W')) as last_month,
	(select	count(*) from #appts where grp <> 0 and workshop_type is null and appt_type in (13) and status in ('K','W')) as ytd,
	(select	count(*) from #appts where grp = 0  and workshop_type is null and appt_type in (13) and status in ('K','W')) as last_ytd

-- Client information
declare	@active_clients		int
select	@active_clients		= count(*)
from	clients with (nolock)
where	active_status in ('A','AR');

insert into #results
select	'Client Statistics' as grp,
	'Active Clients' as description,
	isnull(@active_clients,0) as monthly,
	null as last_month,
	null as ytd,
	null as last_ytd

-- Terminated for other reasons
declare	@this_month_terminated_clients	int
select	@this_month_terminated_clients	= count(*)
from	clients c
left outer join drop_reasons r on c.drop_reason = r.drop_reason
where	c.drop_date between @this_month and @current_time
and	c.active_status = 'I'
and	left(isnull(r.nfcc,'XX'),2) <> 'SC'

declare	@last_month_terminated_clients	int
select	@last_month_terminated_clients	= count(*)
from	clients c
left outer join drop_reasons r on c.drop_reason = r.drop_reason
where	c.drop_date between @last_month and @this_month
and	c.active_status = 'I'
and	left(isnull(r.nfcc,'XX'),2) <> 'SC'

insert into #results
select	'Client Statistics' as grp,
	'Terminated Clients' as description,
	@this_month_terminated_clients as monthly,
	@last_month_terminated_clients as last_month,
	null as ytd,
	null as last_ytd

-- Terminated for successful completion
select	@this_month_terminated_clients	= count(*)
from	clients c
left outer join drop_reasons r on c.drop_reason = r.drop_reason
where	c.drop_date between @this_month and @current_time
and	c.active_status = 'I'
and	left(isnull(r.nfcc,'XX'),2) = 'SC'

select	@last_month_terminated_clients	= count(*)
from	clients c
left outer join drop_reasons r on c.drop_reason = r.drop_reason
where	c.drop_date between @last_month and @this_month
and	c.active_status = 'I'
and	left(isnull(r.nfcc,'XX'),2) = 'SC'

insert into #results
select	'Client Statistics' as grp,
	'Successful Completions' as description,
	@this_month_terminated_clients as monthly,
	@last_month_terminated_clients as monthly,
	null as ytd,
	null as last_ytd

-- Expected deposit amount
declare	@this_month_deposits		money
select	@this_month_deposits		= sum(deposit_amount)
from	client_deposits cd with (nolock)
inner join clients c with (nolock) on cd.client = c.client
where	c.active_status in ('A','AR')

insert into #results
select	'Client Statistics' as grp,
	'Average Payment Per Client' as description,
	convert(int, @this_month_deposits) / @active_clients as monthly,
	null as last_month,
	null as ytd,
	null as last_ytd

-- Total Disbursements
declare	@disbursements_last_year	money
select	@disbursements_last_year	= sum(debit_amt)
from	registers_client_creditor with (nolock)
where	tran_type		in ('AD','MD','CM','BW')
and	client			> 0
and	creditor		<> 'Z9999'
and	date_created between @last_year and @this_year

declare	@disbursements_this_year	money
select	@disbursements_this_year	= sum(debit_amt)
from	registers_client_creditor with (nolock)
where	tran_type		in ('AD','MD','CM','BW')
and	client			> 0
and	creditor		<> 'Z9999'
and	date_created between @this_year and @this_month

declare	@disbursements_this_month	money
select	@disbursements_this_month	= sum(debit_amt)
from	registers_client_creditor with (nolock)
where	tran_type		in ('AD','MD','CM','BW')
and	client			> 0
and	creditor		<> 'Z9999'
and	date_created between @this_month and @current_time

declare	@disbursements_last_month	money
select	@disbursements_last_month	= sum(debit_amt)
from	registers_client_creditor with (nolock)
where	tran_type		in ('AD','MD','CM','BW')
and	client			> 0
and	creditor		<> 'Z9999'
and	date_created between @last_month and @this_month

insert into #results
select	'Client Statistics' as grp,
	'Total Disbursements' as description,
	convert(int, isnull(@disbursements_this_month,0)) as monthly,
	convert(int, isnull(@disbursements_last_month,0)) as last_month,
	convert(int, isnull(@disbursements_last_year,0) + isnull(@disbursements_this_month,0)) as ytd,
	convert(int, isnull(@disbursements_this_year,0)) as last_ytd

-- New CRPs written
declare	@this_month_crps_written	int
select	@this_month_crps_written = count(*)
from	clients with (nolock)
where	dmp_status_date between @this_month and @current_time
and	active_status in ('A','AR');

declare	@last_month_crps_written	int
select	@last_month_crps_written = count(*)
from	clients with (nolock)
where	dmp_status_date between @last_month and @this_month
and	active_status in ('A','AR');

insert into #results
select	'Client Statistics' as grp,
	'New CRPs Written' as description,
	isnull(@this_month_crps_written,0) as monthly,
	isnull(@last_month_crps_written,0) as last_month,
	null as ytd,
	null as last_ytd

-- New CRPs started
declare	@this_month_crps_started	int
select	@this_month_crps_started = count(*)
from	clients with (nolock)
where	start_date between @this_month and @current_time
and	active_status in ('A','AR');

declare	@last_month_crps_started	int
select	@last_month_crps_started = count(*)
from	clients with (nolock)
where	start_date between @last_month and @this_month
and	active_status in ('A','AR');

insert into #results
select	'Client Statistics' as grp,
	'New CRPs Started' as description,
	@this_month_crps_started,
	@last_month_crps_started,
	null,
	null

-- Return the results to the report
select grp, [description], last_month as last_month, monthly, ytd, last_year from #results

drop table #results
drop table #appts
return ( @@rowcount )
GO
