USE [DebtPlus]
GO
/****** Object:  StoredProcedure [dbo].[UPDATE_rebuild_invoice_payments]    Script Date: 09/15/2014 13:13:34 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE PROCEDURE [dbo].[UPDATE_rebuild_invoice_payments] as

-- ==========================================================================================================
-- ==            Rebuild the invoice adjustments and payments for all invoices and all dates               ==
-- ==========================================================================================================

set nocount on
begin transaction
set xact_abort on

-- Find the payment amounts
select	invoice_register, sum(isnull(credit_amt,0)) as pmt_amt
into	#pmt_amt
from	registers_creditor
where	tran_type = 'RC'
group by invoice_register;

-- Find the adjustment amounts
select	invoice_register, sum(isnull(credit_amt,0)) as adj_amt
into	#adj_amt
from	registers_creditor
where	tran_type = 'RA'
group by invoice_register;

-- Find the payment date
select	invoice_register, max(date_created) as pmt_date
into	#pmt_date
from	registers_creditor
where	tran_type = 'RC'
group by invoice_register;

-- Find the adjustment date
select	invoice_register, max(date_created) as adj_date
into	#adj_date
from	registers_creditor
where	tran_type = 'RA'
group by invoice_register;

-- Clear the invoice information
update	registers_invoices
set	adj_date = null, adj_amount = 0,
	pmt_date = null, pmt_amount = 0;

-- Update the adjustment date
update	registers_invoices
set	adj_date	= x.adj_date
from	registers_invoices i
inner join #adj_date x on i.invoice_register = x.invoice_register;

-- Update the adjustment amount
update	registers_invoices
set	adj_amount	= x.adj_amt
from	registers_invoices i
inner join #adj_amt x on i.invoice_register = x.invoice_register;

-- Update the payment date
update	registers_invoices
set	pmt_date	= x.pmt_date
from	registers_invoices i
inner join #pmt_date x on i.invoice_register = x.invoice_register;

-- Update the payment amount
update	registers_invoices
set	pmt_amount	= x.pmt_amt
from	registers_invoices i
inner join #pmt_amt x on i.invoice_register = x.invoice_register;

-- Discard working tables
drop table #pmt_date
drop table #pmt_amt
drop table #adj_date
drop table #adj_amt

-- Commit the changes
commit transaction
return ( 1 )
GO
