USE [DebtPlus]
GO
/****** Object:  StoredProcedure [dbo].[xpr_creditor_contribution_default]    Script Date: 09/15/2014 13:13:34 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[xpr_creditor_contribution_default] (@creditor as typ_creditor) AS

-- ==================================================================================================================
-- ==            Return the default information for the creditor contributions                                     ==
-- ==================================================================================================================

--   2/20/2002
--     Changed to xpr_creditor_contribution_default to make similar to the other items in the database.
--   08/20/2004
--     Removed fairshare_pct_* and creditor_type_* from the creditor. Added creditor_contribution_pct link.

-- Suppress intermediate results
set nocount on

-- Fetch the creditor defaults from the system
declare	@fairshare_pct_check	typ_fairshare_rate
declare	@fairshare_pct_eft	typ_fairshare_rate
declare	@creditor_type_check	typ_creditor_type
declare	@creditor_type_eft	typ_creditor_type
declare	@type			varchar(4)

-- Replace the information with the current contribution status from the creditor
select	@fairshare_pct_check	= coalesce(pct.fairshare_pct_check, t.contribution_pct, 0),
	@fairshare_pct_eft	= coalesce(pct.fairshare_pct_eft, t.contribution_pct, 0),
	@creditor_type_check	= coalesce(pct.creditor_type_check, t.status, 'N'),
	@creditor_type_eft	= coalesce(pct.creditor_type_eft, t.status, 'N'),
	@type			= cr.type
from	creditors cr with (nolock)
left outer join creditor_contribution_pcts pct with (nolock) on cr.creditor_contribution_pct = pct.creditor_contribution_pct
left outer join creditor_types t with (nolock) on cr.type = t.type
where	cr.creditor		= @creditor

if @fairshare_pct_eft is null
	SELECT @fairshare_pct_eft	= @fairshare_pct_check

-- Return the information
select	@fairshare_pct_check		as 'fairshare_pct_check',
	@fairshare_pct_eft		as 'fairshare_pct_eft',
	@creditor_type_eft		as 'creditor_type',
	@creditor_type_check		as 'creditor_type_check',
	@creditor_type_eft		as 'creditor_type_eft',
	convert(datetime, convert(varchar(10), dateadd(d, 1, getdate()),101) + ' 00:00:00') as 'effective_date'

return ( 1 )
GO
