SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER procedure [xpr_creditor_invoices] ( @Creditor AS VarChar(10), @AllTransactions AS INT = 0 ) AS

IF @AllTransactions = 1

	SELECT	i.invoice_register			as 'invoice_register',
		i.inv_date				as 'inv_date',
		i.inv_amount				as 'inv_amount',
		i.pmt_date				as 'pmt_date',
		i.pmt_amount				as 'pmt_amount',
		i.adj_date				as 'adj_date',
		i.adj_amount				as 'adj_amount',
		isnull(tr.checknum,'')		as 'checknum'

	FROM	registers_invoices i
	LEFT OUTER JOIN registers_trust tr ON i.invoice_register=tr.invoice_register
	WHERE	i.creditor = @creditor

	ORDER BY	1, 2 DESC

ELSE

	SELECT	i.invoice_register			as 'invoice_register',
		i.inv_date				as 'inv_date',
		i.inv_amount				as 'inv_amount',
		i.pmt_date				as 'pmt_date',
		i.pmt_amount				as 'pmt_amount',
		i.adj_date				as 'adj_date',
		i.adj_amount				as 'adj_amount',
		isnull(tr.checknum,'')		as 'checknum'

	FROM	registers_invoices i
	LEFT OUTER JOIN registers_trust tr ON i.invoice_register=tr.invoice_register
	WHERE i.creditor=@creditor
	AND	i.inv_amount > i.pmt_amount+i.adj_amount

	ORDER BY	1, 2 DESC

RETURN ( @@rowcount )
GO
