USE [DebtPlus]
GO
/****** Object:  StoredProcedure [dbo].[xpr_insert_ticklers]    Script Date: 09/15/2014 13:13:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[xpr_insert_ticklers] ( @client int, @counselor int, @date_effective datetime, @tickler_type int, @priority int, @note varchar(512) = null ) as

	insert into ticklers (client, counselor, date_effective, tickler_type, priority, note, original_counselor )
	select	@client, @counselor, @date_effective, @tickler_type, @priority, @note, person
	from	counselors
	where	counselor = @counselor;
	
	return ( scope_identity() )
GO
