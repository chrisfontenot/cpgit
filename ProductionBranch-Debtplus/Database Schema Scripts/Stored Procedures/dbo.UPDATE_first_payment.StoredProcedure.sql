USE [DebtPlus]
GO
/****** Object:  StoredProcedure [dbo].[UPDATE_first_payment]    Script Date: 09/15/2014 13:13:34 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE PROCEDURE [dbo].[UPDATE_first_payment] AS

-- ChangeLog
--   06/09/2009
--      Remove references to debt_number and use client_creditor

-- Empty the client/creditor payment information
update	client_creditor
set	first_payment = null,
	last_payment = null;

-- Build a list of the disbursements for the first payment
select	client_creditor, min(date_created) as date_created, convert(int, 0) as client_creditor_register
into	#first_payment
from	registers_client_creditor
where	tran_type in ('AD', 'BW', 'MD', 'CM')
group by client_creditor;

update	#first_payment
set	client_creditor_register = rcc.client_creditor_register
from	#first_payment p
inner join registers_client_creditor rcc on p.client_creditor = rcc.client_creditor

update	client_creditor
set	first_payment = x.client_creditor_register
from	client_creditor cc
inner join #first_payment x on cc.client_creditor = x.client_creditor

drop table #first_payment;

-- Build a list of the disbursements for the last payment
select	client_creditor, max(date_created) as date_created, convert(int, 0) as client_creditor_register
into	#last_payment
from	registers_client_creditor
where	tran_type in ('AD', 'BW', 'MD', 'CM')
group by client_creditor;

update	#last_payment
set	client_creditor_register = rcc.client_creditor_register
from	#last_payment p
inner join registers_client_creditor rcc on p.client_creditor = rcc.client_creditor and p.date_created = rcc.date_created and rcc.tran_type in ('AD', 'BW', 'MD', 'CM');

update	client_creditor
set	last_payment = x.client_creditor_register
from	client_creditor cc
inner join #last_payment x on cc.client_creditor = x.client_creditor;

-- Empty the client/creditor payment information
update	creditors
set	first_payment = null,
	last_payment = null;

-- Build a list of the disbursements for the first payment
select	creditor, min(date_created) as date_created, convert(int, 0) as creditor_register
into	#first_creditor
from	registers_creditor
where	tran_type in ('AD', 'BW', 'MD', 'CM')
group by creditor;

update	#first_creditor
set	creditor_register = rcc.creditor_register
from	#first_creditor p
inner join registers_creditor rcc on p.creditor = rcc.creditor and p.date_created = rcc.date_created and rcc.tran_type in ('AD', 'BW', 'MD', 'CM');

update	creditors
set	first_payment = x.creditor_register
from	creditors cc
inner join #first_creditor x on cc.creditor = x.creditor;

drop table #first_creditor;

-- Build a list of the disbursements for the first payment
select	creditor, max(date_created) as date_created, convert(int, 0) as creditor_register
into	#last_creditor
from	registers_creditor
where	tran_type in ('AD', 'BW', 'MD', 'CM')
group by creditor;

update	#last_creditor
set	creditor_register = rcc.creditor_register
from	#last_creditor p
inner join registers_creditor rcc on p.creditor = rcc.creditor and p.date_created = rcc.date_created and rcc.tran_type in ('AD', 'BW', 'MD', 'CM');

update	creditors
set	last_payment = x.creditor_register
from	creditors cc
inner join #last_creditor x on cc.creditor = x.creditor;

drop table #last_creditor;

return ( 1 )
GO
