USE [DebtPlus]
GO
/****** Object:  StoredProcedure [dbo].[www_creditor_client_list]    Script Date: 09/15/2014 13:13:34 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[www_creditor_client_list] ( @creditor as typ_creditor, @item_count as integer = NULL, @direction as varchar(10) = NULL, @first_client as int = NULL, @last_client as int = NULL ) as
-- =================================================================================================
-- ==            List the clients who are active for this creditor                                ==
-- =================================================================================================

-- Suppress intermediate result sets
set nocount on

-- This is the basic statement
declare	@stmt_1		NVARCHAR(2000)
declare	@stmt_2		NVARCHAR(2000)
declare	@stmt_3		NVARCHAR(2000)

select	@stmt_1 = N' select client,client_creditor,client_name,account_number,start_date,current_balance,disbursement_factor,active_status'
select	@stmt_2 = N' from view_www_creditor_client_list where creditor=''' + replace(@creditor, '''', '''''') + ''''

-- Determine the starting and ending client for the request
declare	@where_string		NVARCHAR(80)

-- Limit the result set to the appropriate number
declare	@stmt_rowcount		NVARCHAR(80)
select	@stmt_rowcount = ''

if isnull(@item_count,0) > 0
	select	@stmt_rowcount = N'set rowcount ' + convert(nvarchar, @item_count) + '; '

-- If there is no limit then just return the entire list without any paging
if @stmt_rowcount = ''
	select @direction = '|<'

-- Process a backward selection clause
if @direction = '<'
begin
	select @stmt_3 = @stmt_rowcount + @stmt_1 + N' into ##a ' + @stmt_2 + N' and client < ' + convert(varchar, @first_client) + N' order by client desc; set rowcount 0'
	execute sp_executesql @stmt_3

	if exists (select * from ##a)
	begin
		select * from ##a order by 1
		drop table ##a
		return ( 1 )
	end

	drop table ##a
	select @direction = '|<'
end

-- Process a forward selection
if @direction = '>'
begin
	select @stmt_3 = @stmt_rowcount + @stmt_1 + N' into ##a ' + @stmt_2 + N' and client > ' + convert(varchar, @last_client) + N' order by client; set rowcount 0'
	execute sp_executesql @stmt_3

	if exists (select * from ##a)
	begin
		select * from ##a order by 1
		drop table ##a
		return ( 1 )
	end

	drop table ##a
	select @direction = '>|'
end

-- Process the end of the file
if @direction = '>|'
begin
	select @stmt_3 = @stmt_rowcount + @stmt_1 + N' into ##a ' + @stmt_2 + N' order by client desc; set rowcount 0'
	execute sp_executesql @stmt_3

	select * from ##a order by 1
	drop table ##a
	return ( 1 )
end

-- Process the beginning of the file
select @stmt_3 = @stmt_rowcount + @stmt_1 + @stmt_2 + N' order by client; set rowcount 0'
execute sp_executesql @stmt_3

return ( 1 )
GO
