USE [DebtPlus]
GO
/****** Object:  StoredProcedure [dbo].[rpt_client_by_zipcode]    Script Date: 09/15/2014 13:13:32 ******/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[rpt_client_by_zipcode] ( @FromDate as DateTime = Null,@ToDate as DateTime = Null) as

-- =============================================================================================================
-- ==            Retrieve the fairshare contributions for the clients by a zipcode order                      ==
-- =============================================================================================================

-- ChangeLog
--   12/19/2002
--     Changed the logic to deal with date ranges. Now the dates are independant and you can specify just the selection
--     information.
--   01/15/2002
--     Added creditor_classes table references and dropped "creditors.fee_creditor"

set nocount on

declare	@stmt	varchar(8000)

select	@stmt	= '
SELECT	rd.client											as ''client'',
	dbo.format_normal_name(default, pn.first, pn.middle, pn.last, default)				as ''name'',
	dbo.format_city_state_zip ( ca.city, ca.state, ca.postalcode )					as ''address'',
	ca.city												as ''city'',
	ca.postalcode											as ''postalcode'',
	convert(money,sum(rd.fairshare_amt + case isnull(ccl.zero_balance,0) when 0 then 0 else rd.debit_amt end))	as ''fair_share''

FROM	clients c
LEFT OUTER JOIN addresses ca WITH (NOLOCK) ON c.AddressID = ca.Address
LEFT OUTER JOIN registers_client_creditor rd	with (nolock) on c.client = rd.client
LEFT OUTER JOIN people p			with (nolock) on rd.client = p.client AND 1 = p.relation 
left outer join names pn with (nolock) on p.nameid = pn.name
INNER JOIN creditors cr				with (nolock) on cr.creditor = rd.creditor
LEFT OUTER JOIN creditor_classes ccl		with (nolock) on cr.creditor_class = ccl.creditor_class

WHERE	c.active_status IN (''A'', ''AR'', ''EX'')
and	rd.tran_type in (''AD'', ''BW'', ''MD'', ''CM'')
'

-- If both dates are missing, do all of the transactions in the system.
if @ToDate is not null
	select	@stmt = @stmt + ' AND rd.date_created < ''' + convert(varchar(10), dateadd(d, 1, @ToDate), 101) + ' 00:00:00'''

if @FromDate is not null
	select	@stmt = @stmt + ' AND rd.date_created >= ''' + convert(varchar(10), @FromDate, 101) + ' 00:00:00'''

-- Include the trailing portion of the select statement
select	@stmt = @stmt + ' GROUP BY rd.client, pn.first, pn.middle, pn.last, ca.city, ca.state, ca.postalcode ORDER BY c.postalcode'

-- Run it
exec ( @stmt )
return ( 1 )
GO
