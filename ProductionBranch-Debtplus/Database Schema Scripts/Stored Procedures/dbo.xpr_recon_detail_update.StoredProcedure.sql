SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER procedure [xpr_recon_detail_update]
	(@recon_detail	 	[typ_key],
	 @tran_type	 	[typ_transaction],
	 @checknum	 	BigInt,
	 @amount	 	[money],
	 @cleared	 	[char](1),
	 @recon_date	 	[datetime],
	 @message 		[varchar](80) ) AS

-- =====================================================================================================================
-- ==                Update the detail item with the indicated parameters                                             ==
-- =====================================================================================================================

UPDATE	[recon_details] 

SET	[tran_type]		= @tran_type,
	[checknum]		= @checknum,
	[amount]		= @amount,
	[cleared]		= @cleared,
	[recon_date]		= @recon_date,
	[message]		= @message 
WHERE 
	[recon_detail]		= @recon_detail

RETURN ( @@rowcount )
GO
