USE [DebtPlus]
GO
/****** Object:  StoredProcedure [dbo].[rpt_rpps_response_coa]    Script Date: 09/15/2014 13:13:33 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[rpt_rpps_response_coa] ( @rpps_response_file as int = null ) AS

-- =======================================================================================================
-- ==           Obtain the information for the response conditions of account number changes            ==
-- =======================================================================================================

-- ChangeLog

-- Suppress the count of records
set nocount on

-- Try to find the response file if one is not specified
if @rpps_response_file is null
begin
	select	@rpps_response_file = max(rpps_response_file)
	from	rpps_response_files
	where	date_created = (
		select	max(date_created)
		from	rpps_response_files
	)
end

select	r.rpps_biller_id					as 'biller_id',
		convert(varchar(80),x1.biller_name)	as 'creditor_name',
		r.account_number					as 'old_account_number',
		r.rpps_transaction					as 'rpps_transaction',
		
		coa.ChangeAccountNumber				as 'ChangeAccountNumber',
		coa.NewAccountNumber				as 'new_account_number',
		
		coa.ChangeBillerID					as 'ChangeBillerID',
		coa.NewBillerID						as 'NewBillerID',
		
		r.trace_number						as 'trace_number',
		
		convert(varchar(10), null)			as 'creditor',
		convert(int,null)					as 'client',
		convert(varchar(80), null)			as 'client_name',
		convert(int,null)					as 'counselor',
		convert(varchar(80), null)			as 'counselor_name'
		
into		#rpps_responses_coa
from		rpps_response_details r		with (nolock)
left outer join rpps_response_details_coa coa	with (nolock) on r.rpps_response_detail = coa.rpps_response_detail
left outer join rpps_biller_ids x1		with (nolock) on r.rpps_biller_id = x1.rpps_biller_id
where	r.service_class_or_purpose	= 'coa'
and		r.transaction_code		= 22
and		r.rpps_response_file		= @rpps_response_file

-- Find the client and creditor from the transaction information
update	#rpps_responses_coa
set		creditor			= t.creditor,
		client				= t.client
from	#rpps_responses_coa x
inner join rpps_transactions t on x.rpps_transaction = t.rpps_transaction
where	t.creditor is not null;

-- Try to find the creditor for the biller ids and from there, the debt by the account number.
update		#rpps_responses_coa
set			creditor	= isnull(cc.creditor,cr.creditor),
			client		= cc.client
from		#rpps_responses_coa d
left outer join	creditor_methods m on d.biller_id = m.rpps_biller_id
inner join	banks b on m.bank = b.bank
inner join	creditors cr on m.creditor = cr.creditor_id
left outer join	client_creditor	cc on cr.creditor = cc.creditor
where		d.creditor is null
and			b.type			= 'R'
and			cc.account_number	= d.old_account_number

-- Retrieve the client information
update		#rpps_responses_coa
set			client_name		= dbo.format_normal_name (pn.prefix, pn.first, pn.middle, pn.last, pn.suffix),
			counselor		= c.counselor
from		#rpps_responses_coa x
inner join	clients c on x.client = c.client
inner join	people p on x.client = p.client and 1 = p.relation
left outer join names pn with (nolock) on p.nameid = pn.name;

-- Set the counselor name
update		#rpps_responses_coa
set			counselor_name		= dbo.format_normal_name(default,cox.first,cox.middle,cox.last,cox.suffix)
from		#rpps_responses_coa x
inner join	counselors co on x.counselor = co.counselor
left outer join names cox with (nolock) on co.nameid = cox.name;

-- Set the creditor name
update		#rpps_responses_coa
set			creditor_name	= left(cr.creditor_name, 80)
from		#rpps_responses_coa x
inner join creditors cr on x.creditor = cr.creditor;

-- Return the results
select 		biller_id,
			creditor_name,
			old_account_number,
			new_account_number,
			trace_number,
			creditor,
			client,
			client_name,
			counselor,
			counselor_name,
			rpps_transaction,
			ChangeAccountNumber,
			new_account_number as NewAccountNumber,
			ChangeBillerID,
			NewBillerID
from		#rpps_responses_coa
order by	counselor, biller_id, old_account_number

drop table    #rpps_responses_coa

return ( @@rowcount )
GO
