USE [DebtPlus]
GO
/****** Object:  StoredProcedure [dbo].[rpt_housing_interviews_by_client]    Script Date: 09/15/2014 13:13:33 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[rpt_housing_interviews_by_client]  (@client as int) as
-- =============================================================================================================
-- ==           Select the interviews for the client                                                          ==
-- =============================================================================================================

-- ChangeLog
--   6/25/2008
--     Initial Creation

set nocount on

select	i.hud_interview										as 'ID',
		i.interview_type									as 'interview_type',
		i.interview_date									as 'interview_date',
		dbo.format_counselor_name(i.interview_counselor)					as 'interview_counselor',

		i.hud_result										as 'result_type',
		i.result_date										as 'result_date',
		dbo.format_counselor_name(i.result_counselor)						as 'result_counselor',

		i.termination_reason								as 'termination_reason',
		i.termination_date									as 'termination_date',
		dbo.format_counselor_name(i.termination_counselor)					as 'termination_counselor',

		mi.description										as 'interview_description',
		mr.description										as 'result_description',
		mt.description										as 'termination_description',

		convert(varchar(10), case when t.client_appointment is null then 'CL' else 'AP' end)	as 'minutes_source',
		t.minutes											as 'minutes',
		dbo.format_counselor_name(t.created_by)				as 'minutes_counselor',
		t.date_created										as 'minutes_date',

		i.hud_grant											as 'hud_grant',
		g.description										as 'hud_grant_description'

from	hud_interviews i with (nolock)
left outer join hud_transactions t with (nolock) on i.hud_interview = t.hud_interview
left outer join Housing_PurposeOfVisitTypes mi with (nolock) on i.interview_type = mi.oID
left outer join Housing_ResultTypes mr with (nolock) on i.hud_result = mr.oID
left outer join Housing_TerminationReasonTypes mt with (nolock) on i.termination_reason	= mt.oID
left outer join Housing_GrantTypes g with (nolock) ON i.hud_grant = g.oID
where	i.client	= @client
order by i.interview_date

return ( @@rowcount )
GO
