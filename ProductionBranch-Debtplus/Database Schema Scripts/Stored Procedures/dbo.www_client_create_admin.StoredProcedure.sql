USE [DebtPlus]
GO
/****** Object:  StoredProcedure [dbo].[www_client_create_admin]    Script Date: 09/15/2014 13:13:34 ******/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE PROCEDURE [dbo].[www_client_create_admin] (@client as int, @password as varchar(80) = null, @question as varchar(80) = null, @answer as varchar(80) = null, @email as varchar(255) = null, @isLockedOut as bit = 0, @isApproved as bit = 1, @Comment as varchar(255) = null, @ApplicationName as varchar(255) = '/', @PKID as varchar(255) = null) as 
-- =============================================================================
-- ==            Create access to the client from the client interface        ==
-- =============================================================================

-- Suppress intermediate results
set nocount on

-- Ensure that there is an application name
select @ApplicationName = ltrim(rtrim(isnull(@ApplicationName,'/')))
if @ApplicationName = ''
	select @ApplicationName = '/'

-- If there is no primary key id then create one.
IF @PKID is null
	select	@PKID = newid();

-- Check to determine if the client currently has access
if exists (select * from client_www where username = convert(varchar,@client) and applicationname = @ApplicationName)
begin
	RaisError ('The client currently has WWW access. Use the password change function to alter the password if you must.', 16, 1)
	return ( 0 )
end

-- Generate the client account
insert into client_www	(PKID, ApplicationName,  username,  [password], PasswordQuestion, PasswordAnswer, Email,  isLockedOut,            isApproved,            Comment)
select					@PKID, @ApplicationName, convert(varchar,@client), @password,  @question,        @answer,        @Email, isnull(@isLockedOut,0), isnull(@isApproved,1), @Comment

-- Return success if the client was created
return ( @@rowcount )
GO
