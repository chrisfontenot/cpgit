USE [DEBTPLUS]
GO
/****** Object:  StoredProcedure [dbo].[xpr_update_dsa_case_details]    Script Date: 12/2/2014 1:18:47 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[xpr_update_dsa_case_details] 
(
	@housingloanid	int,
	@caseid			int
)
as
begin

	-- set the case id for the housing loan
	update Housing_Loan_DSADetails
	   set CaseID = @caseid
	 where oID = (select DsaDetailID 
	                from Housing_loans 
				   where oID = @housingloanid)

end