SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER procedure [xpr_ManualCheck_Update] ( @trust_register AS INT, @checknum AS BigInt, @effective_date AS DateTime = NULL ) AS

-- =========================================================================================================================
-- ==                Update the trust register to indicate that the check has been printed with the values                ==
-- =========================================================================================================================

-- Disable intermediate results
SET NOCOUNT ON

IF @effective_date IS NULL
	SET @effective_date = getdate()

-- Update the trust register to reflect the status of the check
UPDATE	registers_trust
SET	date_created	= @effective_date,
	checknum		= @checknum,
	cleared			= ' '
where	trust_register		= @trust_register
and	cleared			= 'P'
and	tran_type		IN ('CM', 'MD', 'MR', 'CR', 'AR')
and	checknum		IS NULL

RETURN ( @@rowcount )
GO
