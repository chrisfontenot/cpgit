IF EXISTS(SELECT * FROM SYSOBJECTS WHERE TYPE='P' AND Name=N'xpr_hecm_get_client')
	EXEC('DROP PROCEDURE xpr_hecm_get_client')
GO
CREATE PROCEDURE [dbo].[xpr_hecm_get_client] (@clientID INT) AS
BEGIN
	DECLARE @referralCode INT
	DECLARE @loanNumber VARCHAR(20)
	DECLARE @TrialModification INT
	DECLARE @applicantName INT
	DECLARE @coapplicantName INT
	DECLARE @homePhone INT
	DECLARE @messagePhone INT
	DECLARE @applicantCell INT
	DECLARE @coapplicantCell INT
	DECLARE @applicantWorkPhone INT
	DECLARE @coapplicantWorkPhone INT
	DECLARE @address INT
	DECLARE @applicantID INT
	DECLARE @coapplicantID INT
	DECLARE @language INT
	DECLARE @ServicerID INT
	DECLARE @applicantEmail INT
	DECLARE @coapplicantEmail INT
	DECLARE @InvestorAccountNumber VARCHAR(50)
	DECLARE @Corporate_Advance MONEY
	DECLARE @tax_delinq_state INT
	DECLARE @ins_delinq_state INT
	DECLARE @SPOC_ID INT
	DECLARE @Program INT

	IF @clientID > 0
	BEGIN
		-- get ocs details
		SELECT @loanNumber = CASE WHEN ur.loannumber IS NULL OR ur.loannumber = '' THEN ocs.LoanNumberPlaceHolder ELSE ur.loannumber END
			,@TrialModification = ocs.TrialModification
			,@Program = ocs.program
		FROM OCS_Client AS ocs
		JOIN OCS_UploadRecord ur ON ocs.UploadRecord = ur.id
		WHERE ocs.ClientId = @clientID

		-- get client details
		SELECT @referralCode = referred_by
			,@homePhone = HomeTelephoneID
			,@messagePhone = MsgTelephoneID
			,@address = AddressID
			,@language = [language]
		FROM clients AS c
		WHERE c.client = @clientID

		-- get applicant details
		SELECT @applicantName = p.NameID
			,@applicantCell = p.CellTelephoneID
			,@applicantWorkPhone = p.WorkTelephoneID
			,@applicantEmail = p.EmailID
			,@SPOC_ID = p.SPOC_ID
		FROM people AS p
		WHERE p.Relation = 1 AND p.Client = @clientID

		-- get coapplicant details
		SELECT @coapplicantName = p.NameID
			,@coapplicantCell = p.CellTelephoneID
			,@coapplicantWorkPhone = p.WorkTelephoneID
			,@coapplicantEmail = p.EmailID
		FROM people AS p
		WHERE p.Relation = 2 AND p.Client = @clientID

		--get Servicer ID
		SELECT @ServicerID = ServicerID
			,@InvestorAccountNumber = InvestorAccountNumber
			,@Corporate_Advance = Corporate_Advance
		FROM [Housing_lenders] HLR
		INNER JOIN [Housing_loans] HLN ON HLR.oID = HLN.CurrentLenderID
		INNER JOIN [Housing_properties] HP ON HP.oID = HLN.PropertyID
		WHERE HousingID = @clientID

		--Get Delinqency Type
		SELECT @tax_delinq_state = tax_delinq_state
			,@ins_delinq_state = ins_delinq_state
		FROM Housing_properties
		WHERE HousingID = @clientID
	END

	SELECT @referralCode AS ReferralCode
		,@loanNumber AS LoanNumber
		,@TrialModification AS TrialModification
		,@applicantName AS ApplicantName
		,@coapplicantName AS CoapplicantName
		,@homePhone AS HomePhone
		,@messagePhone AS MessagePhone
		,@applicantCell AS ApplicantCell
		,@coapplicantCell AS CoapplicantCell
		,@applicantWorkPhone AS ApplicantWorkPhone
		,@coapplicantWorkPhone AS CoapplicantWorkPhone
		,@address AS Address
		,@language AS Language
		,@ServicerID AS ServicerID
		,@applicantEmail AS ApplicantEmail
		,@coapplicantEmail AS CoapplicantEmail
		,@InvestorAccountNumber AS InvestorAccountNumber
		,@Corporate_Advance AS Corporate_Advance
		,@tax_delinq_state AS Tax_Dinq_State
		,@ins_delinq_state AS Ins_Delinq_State
		,@SPOC_ID AS SPOC_ID
		-- ,@Program AS Program
		,convert(int,null) AS Program
END
GO
GRANT EXECUTE ON xpr_hecm_get_client TO public AS dbo;
GO
