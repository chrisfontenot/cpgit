USE [DebtPlus]
GO
/****** Object:  StoredProcedure [dbo].[xpr_rpps_response_process_CDP]    Script Date: 09/15/2014 13:13:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[xpr_rpps_response_process_CDP] ( @rpps_response_file as int ) AS

-- ====================================================================================================================
-- ==            This is a proposal response. The response is basically ignored because the item never reached       ==
-- ==            the creditor. It is basically the same as a reject condition.                                       ==
-- ====================================================================================================================

-- Find the proposal from the trace table
begin transaction
set nocount on
set xact_abort on

-- Find the proposals that are being accepted
select	d.rpps_response_detail,
	x.company_identification,
	x.ssn,
	x.client,
	t.rpps_transaction,
	d.processing_error,
	d.trace_number,
	d.return_code

into	#rpps_response_cdp

from	rpps_response_details d
inner join rpps_response_details_cdd x on d.rpps_response_detail = x.rpps_response_detail
inner join rpps_transactions t on d.rpps_transaction = t.rpps_transaction

where	d.rpps_response_file		= @rpps_response_file
and	d.processing_error		is null
and	d.service_class_or_purpose	= 'CDP'
and	t.service_class_or_purpose	in ('CDP','FBC','FBD')

-- Update the response table with the result codes. We don't do much beyond that because the operation is complete.
update	rpps_transactions
set	return_code = x.return_code
from	rpps_transactions t
inner join #rpps_response_cdp x on t.rpps_transaction = x.rpps_transaction;

-- Cleanup
drop table #rpps_response_cdp
commit transaction
return ( 1 )
GO
