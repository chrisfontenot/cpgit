USE [DebtPlus]
GO
/****** Object:  StoredProcedure [dbo].[rpt_PrintClient_Agreement]    Script Date: 09/15/2014 13:13:33 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE PROCEDURE [dbo].[rpt_PrintClient_Agreement] ( @Client AS INT ) AS

-- ======================================================================================
-- ==                Return the deposit information for the client                     ==
-- ======================================================================================

SET NOCOUNT ON

-- Fetch the deposit amount
DECLARE	@Deposit	MONEY

SELECT	@Deposit = sum(deposit_amount)
FROM	client_deposits
WHERE	client = @Client

IF @Deposit IS NULL
	SELECT @Deposit = 0

-- Determine the deposit date for the client
DECLARE	@FullDepositDate	datetime
SELECT	@FullDepositDate = min(deposit_date)
FROM	client_deposits with (nolock)
WHERE	client = @client

IF @FullDepositDate IS NULL
begin
	SELECT @FullDepositDate = convert(datetime, convert(varchar(2), month(getdate())) + '/01/' + convert(varchar(4), year(getdate())) + ' 00:00:00')
	SELECT @FullDepositDate = dateadd(m, 1, @FullDepositDate)
end

-- Determine the client fee account
DECLARE	@FeeAccount	INT
SELECT	@FeeAccount = config_fee
FROM	clients
WHERE	client = @Client

IF @FeeAccount IS NULL
BEGIN
	SELECT	@FeeAccount = min(config_fee)
	FROM	config_fees
	where	[default] = 1

	IF @FeeAccount IS NULL
	BEGIN
		SELECT	@FeeAccount = min(config_fee)
		FROM	config_fees
	END

	IF @FeeAccount IS NULL
		SELECT @FeeAccount = 1
END

-- Determine the disbursement amounts for the fixed PAF and setup charges
declare	@paf_creditor		varchar(10)
declare	@setup_creditor		varchar(10)
declare	@counseling_creditor	varchar(10)
declare	@paf_amount		money
declare	@setup_amount		money
declare	@counseling_amount	money

-- Retrieve the creditor IDs from the config table
select	@paf_creditor		= paf_creditor,
		@setup_creditor		= setup_creditor,
		@counseling_creditor	= counseling_creditor
from	config

-- Find the first item in the client's debt list that matches our creditor ID
if @paf_creditor is not null
	select	@paf_amount	= disbursement_factor
	from	client_creditor with (nolock)
	where	creditor	= @paf_creditor
	and		client		= @client

-- Do the same for the setup creditor if there is one
if @setup_creditor is not null
	select	@setup_amount	= disbursement_factor
	from	client_creditor with (nolock)
	where	creditor	= @setup_creditor
	and		client		= @client

-- Find the first item in the client's debt list that matches our creditor ID
if @counseling_creditor is not null
	select	@counseling_amount	= disbursement_factor
	from	client_creditor with (nolock)
	where	creditor	= @counseling_creditor
	and		client		= @client

-- Fetch the starting date for the client
DECLARE	@StartDate		DateTime
declare	@Counselor		int
declare	@addr1			varchar(80)
declare	@addr2			varchar(80)
declare	@addr3			varchar(80)
declare	@postalcode		varchar(80)

SELECT	@StartDate	= isnull(start_date, dateadd(d, 45, getdate())),
		@counselor	= counselor,
		@addr1		= ltrim(rtrim(dbo.format_address_line_1(a.house, a.direction, a.street, a.suffix, a.modifier, a.modifier_value))),
		@addr2		= ltrim(rtrim(a.address_line_2)),
		@addr3		= dbo.format_city_state_zip ( a.city, a.state, a.postalcode ),
		@postalcode	= a.postalcode

FROM	clients c with (nolock)
left outer join addresses a with (nolock) on c.addressid = a.address
WHERE	c.client = @client

-- Find the counselor name
declare	@counselor_id	varchar(80)
declare	@counselor_name	varchar(80)

select	@counselor_id	= dbo.format_counselor_name ( co.person ),
		@counselor_name = dbo.format_normal_name(default,n.first,default,n.last,default)
from	counselors co with (nolock)
LEFT OUTER JOIN names n with (nolock) on co.NameID = n.name
where	co.counselor = @counselor

-- Find the client and spouse name
declare	@client_name		varchar(80)
declare	@spouse_name		varchar(80)

select	@client_name	= dbo.format_normal_name(default,pn.first,pn.middle,pn.last,pn.suffix)
from    people p
left outer join names pn with (nolock) on p.nameid = pn.name
where   client          = @client
and     relation        = 1

select	@spouse_name	= dbo.format_normal_name(default,pn.first,pn.middle,pn.last,pn.suffix)
from    people p
left outer join names pn with (nolock) on p.nameid = pn.name
where   client          = @client
and     relation        <> 1

-- Return the information for the configuration fee and deposit information
SELECT	isnull(fee_percent,0)			as fee_percent,
		isnull(fee_per_creditor,0)		as fee_per_creditor,
		isnull(fee_monthly_max,0)		as fee_maximum,
		isnull(description,'')		as fee_description,
		isnull(@Deposit,0)				as deposit_amt,
		day(@FullDepositDate)			as deposit_date,
		@StartDate						as start_date,

		convert(money,isnull(@counseling_amount,0)) as counseling_amount,
		convert(money,isnull(@paf_amount,0))        as paf_amount,
		convert(money,isnull(@setup_amount,0))      as setup_amount,
	
		isnull(@counselor_id, '')		as counselor_id,
		isnull(@counselor_name, '')	as counselor_name,
		@FullDepositDate				as full_deposit_date,

		isnull(@client_name,'')		as client_name,
		isnull(@spouse_name,'')		as spouse_name,
		isnull(@addr1,'')				as addr1,
		isnull(@addr2,'')				as addr2,
		isnull(@addr3,'')				as addr3,
		isnull(@postalcode,'')		as zipcode,

		convert(varchar(80),null)	as field_1,	
		convert(varchar(80),null)	as field_2,
		convert(varchar(80),null)	as field_3,
		convert(varchar(80),null)	as field_4,
		convert(varchar(80),null)	as field_5,
		convert(varchar(80),null)	as field_6,
		convert(varchar(80),null)	as field_7
FROM	config_fees
WHERE	config_fee = @FeeAccount

RETURN ( @@rowcount )
GO
