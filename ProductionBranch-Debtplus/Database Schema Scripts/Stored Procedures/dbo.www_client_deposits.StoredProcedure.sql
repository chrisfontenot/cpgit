USE [DebtPlus]
GO
/****** Object:  StoredProcedure [dbo].[www_client_deposits]    Script Date: 09/15/2014 13:13:34 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE PROCEDURE [dbo].[www_client_deposits] ( @client as int, @list_count as int = NULL ) AS
-- ======================================================================================
-- ==            List of the most recient deposits                                     ==
-- ======================================================================================

-- Suppress intermediate result sets
set nocount on

if @list_count is null
	set @list_count = 30

if @list_count < 0
	set @list_count = 1

-- Generate the statement which limits the rows and then retrieve the items
begin transaction
CREATE table #results (
	pk		int identity(1,1),
	date_created	varchar(10),
	description	varchar(50),
	credit_amt	money,
	item_date	varchar(10),
	message		varchar(50)
);

insert into #results (date_created, description, credit_amt, item_date, message)
SELECT	convert(varchar(10), d.date_created, 1) as 'date_created',
	x.description as 'description',
	d.credit_amt as 'credit_amt',
	convert(varchar(10), d.item_date, 1) as 'item_date',
	d.message as 'message'
FROM	registers_client d WITH (NOLOCK)
LEFT OUTER JOIN tran_types x WITH (NOLOCK) ON d.tran_subtype = x.tran_type
WHERE	d.tran_type = 'DP'
AND	client=@client
ORDER BY convert(datetime,d.date_created) DESC;

select	date_created, description, credit_amt, item_date, message
from	#results
where	pk <= @list_count
order by pk;

drop table #results;

commit transaction
GO
