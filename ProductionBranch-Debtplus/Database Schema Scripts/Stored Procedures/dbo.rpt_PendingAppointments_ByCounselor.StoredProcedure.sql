USE [DebtPlus]
GO
/****** Object:  StoredProcedure [dbo].[rpt_PendingAppointments_ByCounselor]    Script Date: 09/15/2014 13:13:33 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[rpt_PendingAppointments_ByCounselor]  ( @FromDate AS DateTime = null, @ToDate AS DateTime = null ) AS
-- =============================================================================================
-- ==                      List of Pending Appointments by counselor                          ==
-- =============================================================================================

-- ChangeLog
--   7/16/2002
--     Returned the "right" portion of the scheduler if this is a domain name
--   4/2/2003
--     Use the message table for the confirmation status

IF @ToDate IS NULL
	SELECT @ToDate = getdate()

IF @FromDate IS NULL
	SELECT @FromDate = @ToDate

-- Adjust the dates for the proper settings
SELECT @FromDate	= convert (datetime, convert(varchar(10), @FromDate, 101) + ' 00:00:00')
SELECT @ToDate	= convert (datetime, convert(varchar(10), @ToDate, 101)   + ' 23:59:59')

SELECT
		ap.client				as 'client_id',     -- 1

		dbo.format_normal_name(pn.prefix,pn.first,pn.middle,pn.last,pn.suffix) as 'client_name', -- 2

		isnull(dbo.format_normal_name(default,cox.first,default,cox.last,default),ap.counselor)		as 'counselor', -- 3
		isnull(ap.start_time, tm.start_time)	as 'time', -- 4
		o.name					as 'office', -- 5
		isnull(t.appt_name,'')			as 'appointment_type', -- 6
		ap.date_created					as 'date_created', -- 7

		case
			when ap.confirmation_status <= 0 then ''
			when m.description is not null then m.description
			when ap.confirmation_status = 1 then 'Agency'
			when ap.confirmation_status = 2 then 'Client'
			else 'Unknown'
		end					as 'confirmation_status', -- 8

		dbo.format_counselor_name(ap.created_by) as 'created_by' -- 9

FROM		client_appointments ap	WITH (NOLOCK)
INNER JOIN clients c			WITH (NOLOCK) ON ap.client	= c.client

LEFT OUTER JOIN	people p		WITH (NOLOCK) ON ap.client	= p.client AND 1 = p.relation
left outer join names pn with (nolock) on p.nameid = pn.name
LEFT OUTER JOIN	appt_times tm		WITH (NOLOCK) ON ap.appt_time	= tm.appt_time
LEFT OUTER JOIN	appt_types t		WITH (NOLOCK) ON ap.appt_type	= t.appt_type
LEFT OUTER JOIN	offices o		WITH (NOLOCK) ON ap.office	= o.office
LEFT OUTER JOIN	counselors co		WITH (NOLOCK) ON ap.counselor	= co.counselor
LEFT OUTER JOIN names cox               WITH (NOLOCK) ON co.NameID      = cox.name
LEFT OUTER JOIN AppointmentConfirmationTypes m		WITH (NOLOCK) ON ap.confirmation_status = m.oID

WHERE		ap.status in ( 'P', 'K', 'M', 'W' )
AND			ap.workshop IS NULL
AND			ap.start_time BETWEEN @FromDate AND @ToDate

ORDER BY	3, 4 -- ap.counselor, ap.start_time

RETURN ( @@rowcount )
GO
