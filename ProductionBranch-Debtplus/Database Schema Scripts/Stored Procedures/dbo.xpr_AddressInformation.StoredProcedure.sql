USE [DebtPlus]
GO
/****** Object:  StoredProcedure [dbo].[xpr_AddressInformation]    Script Date: 09/15/2014 13:13:34 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE PROCEDURE [dbo].[xpr_AddressInformation](@client as int) AS
-- =========================================================================================
-- ==                             Fetch the Client Address information                    ==
-- =========================================================================================
DECLARE @Addr1   varchar(256)
DECLARE @Addr2   varchar(256)
DECLARE @Addr3   varchar(256)
DECLARE @Addr4   varchar(256)
DECLARE @postalcode varchar(256)
DECLARE @spouse  varchar(256)

SET NOCOUNT ON

SELECT		@Addr1 = isnull(upper(name),''),
			@Addr2 = isnull(upper(addr1),''),
			@Addr3 = isnull(upper(addr2),''),
			@addr4 = isnull(upper(addr3),''),
			@PostalCode	= isnull(upper(zipcode),''),
			@Spouse	= isnull(upper(coapplicant),'')
FROM 		view_client_address WITH (NOLOCK)
WHERE		client = @client

IF @spouse = ''
	SELECT @spouse = 'NONE'

IF @Addr3 = ''
	SELECT @Addr3 = @Addr4, @Addr4 = ''

IF @Addr2 = ''
	SELECT @Addr2 = @Addr3, @Addr3 = @Addr4, @Addr4 = ''

IF @Addr1 = ''
	SELECT @Addr1 = @Addr2, @Addr2 = @Addr3, @Addr3 = @Addr4, @Addr4 = ''

SET NOCOUNT OFF

SELECT	convert(varchar(256),@spouse)		AS 'spouse',
	convert(varchar(256),@Addr1)		AS 'name',
	convert(varchar(256),@Addr2)		AS 'address1',
	convert(varchar(256),@Addr3)		AS 'address2',
	convert(varchar(256),@Addr4)		AS 'address3',
	convert(varchar(256),@postalcode)	AS 'zipcode'
RETURN ( 1 )
GO
