SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER procedure [rpt_creditor_invoice_single]( @invoice as INT = NULL ) AS
-- ============================================================================
-- ==     Determine the items associated with the indicated invoice          ==
-- ============================================================================

-- ChangeLog
--   06/09/2009
--      Remove references to debt_number and use client_creditor

-- Disable intermediate result sets
SET NOCOUNT ON

-- Fetch the creditor from the invoice number
DECLARE @date_printed	DATETIME
select	@date_printed	= getdate()

-- Accept invoice 0 as null
if @invoice is null
	SELECT @invoice = 0

IF @invoice = 0
BEGIN
	-- Fetch the possible invoices to be printed
	SELECT		invoice_register
	INTO		#t_invoice_registers_2
	FROM		registers_invoices WITH ( NOLOCK )
	WHERE		isnull(inv_amount,0) > isnull(pmt_amount,0) + isnull(adj_amount,0)

	-- Process the creditors in the list
	SELECT	i.creditor			as 'creditor',
			i.invoice_register	as 'invoice_register',
			t.date_created		as 'date_created',
			t.checknum		as 'checknum',

			d.client		as 'client',

			v.proposal_client_name as 'client_name',
			coalesce(d.account_number,v.account_number) as 'account_number',
 
			d.debit_amt		as 'amount',
			d.fairshare_amt		as 'billed',
			isnull(i.pmt_amount,0)  as 'pmt_amount',
			isnull(i.adj_amount,0)  as 'adj_amount',
			cr.creditor_name	as 'creditor_name',
			cr.po_number		as 'po_number',
			isnull(b.type,'C')	as 'bank_type'

	FROM		#t_invoice_registers_2 x	WITH ( NOLOCK )
	INNER JOIN	registers_invoices i		WITH ( NOLOCK ) ON i.invoice_register = x.invoice_register
	INNER JOIN	registers_client_creditor d	WITH ( NOLOCK ) ON i.invoice_register = d.invoice_register
	LEFT OUTER JOIN	view_last_payment v		WITH ( NOLOCK ) ON d.client_creditor = v.client_creditor
	LEFT OUTER JOIN	creditors cr			WITH ( NOLOCK ) ON cc.creditor = cr.creditor
	LEFT OUTER JOIN	people p			WITH ( NOLOCK ) ON d.client = p.client AND 1 = p.relation
	LEFT OUTER JOIN	registers_trust t		WITH ( NOLOCK ) ON d.trust_register = t.trust_register
	LEFT OUTER JOIN banks b					WITH ( NOLOCK ) ON t.bank = b.bank

	WHERE		d.tran_type in ('AD', 'BW', 'MD', 'CM')

	ORDER BY	cr.type, cr.creditor_id, 1, 2, 3, 6

	drop table #t_invoice_registers_2

END ELSE

	-- Process the creditors in the list
	SELECT	i.creditor			as 'creditor',
			i.invoice_register	as 'invoice_register',
			t.date_created		as 'date_created',
			t.checknum		as 'checknum',
			d.client		as 'client',

			cc.proposal_client_name as 'client_name',
			coalesce(d.account_number,cc.account_number) as 'account_number',
 
			d.debit_amt		as 'amount',
			d.fairshare_amt		as 'billed',
			isnull(i.pmt_amount,0)  as 'pmt_amount',
			isnull(i.adj_amount,0)  as 'adj_amount',
			cr.creditor_name	as 'creditor_name',
			cr.po_number		as 'po_number',
			isnull(b.type,'C')	as 'bank_type'

	FROM		registers_invoices i
	INNER JOIN	registers_client_creditor d	WITH ( NOLOCK ) ON d.invoice_register = i.invoice_register
	LEFT OUTER JOIN	view_last_payment cc		WITH ( NOLOCK ) ON d.client_creditor = cc.client_creditor
	LEFT OUTER JOIN	creditors cr			WITH ( NOLOCK ) ON cc.creditor = cr.creditor
	LEFT OUTER JOIN	people p			WITH ( NOLOCK ) ON d.client = p.client AND 1 = p.relation
	LEFT OUTER JOIN	registers_trust t		WITH ( NOLOCK ) ON d.trust_register = t.trust_register
	LEFT OUTER JOIN banks b					WITH ( NOLOCK ) ON t.bank = b.bank
	WHERE		i.invoice_register = @invoice
	AND		d.tran_type in ('AD', 'BW', 'MD', 'CM')

	ORDER BY	cr.type, cr.creditor_id, 1, 2, 3, 6

-- Return the number of rows in the result set
RETURN ( @@rowcount )
GO
