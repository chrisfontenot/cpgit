USE [DebtPlus]
GO
/****** Object:  StoredProcedure [dbo].[rpt_PolicyMatrix_Comments]    Script Date: 09/15/2014 13:13:33 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[rpt_PolicyMatrix_Comments]( @creditor as varchar(10) ) as

-- ==============================================================================================================
-- ==            Retrieve the comment fields for the indicated policy matrix item of the creditor              ==
-- ==============================================================================================================

select 		c.sic,

		case c.type
			when 'P'	then 'Payment'
					else 'Creditor'
		end	as type,

		c.comment

from		policy_matrix_comments c with (nolock)
inner join	creditors cr with (nolock) on c.sic = cr.sic
where		cr.creditor = @creditor
order by	c.type

return ( @@rowcount )
GO
