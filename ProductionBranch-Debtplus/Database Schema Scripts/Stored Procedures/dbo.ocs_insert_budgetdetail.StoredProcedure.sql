SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Binh Tran
-- Create date: 08/05/2015
-- Description:	Insert into Budget Detail
-- =============================================
ALTER PROCEDURE [ocs_insert_budgetdetail] (
	@budget INT
	,@category INT
	,@client_amount VARCHAR(50)
	)
AS
BEGIN
	SET NOCOUNT ON;

	IF (@client_amount <> '' AND @client_amount IS NOT NULL AND cast(@client_amount AS MONEY) > 0)
	BEGIN
		INSERT INTO [dbo].[budget_detail] (
			[budget]
			,[budget_category]
			,[client_amount]
			,[suggested_amount]
			)
		VALUES (
			@budget
			,@category
			,@client_amount
			,0
			)
	END
END
GO
