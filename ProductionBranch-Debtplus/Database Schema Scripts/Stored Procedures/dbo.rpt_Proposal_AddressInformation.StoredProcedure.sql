USE [DebtPlus]
GO
/****** Object:  StoredProcedure [dbo].[rpt_Proposal_AddressInformation]    Script Date: 09/15/2014 13:13:33 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[rpt_Proposal_AddressInformation](@client_creditor_proposal as int) AS
-- =========================================================================================
-- ==                             Fetch the Client Address information                    ==
-- =========================================================================================

-- ChangeLog
--   11/27/2002
--      Fold the result set to upper case

DECLARE @Addr1			varchar(256)
DECLARE @Addr2			varchar(256)
DECLARE @Addr3			varchar(256)
DECLARE @Addr4			varchar(256)
DECLARE @postalcode		varchar(256)
DECLARE @spouse			varchar(256)
DECLARE	@applicant		varchar(256)
DECLARE	@ApplicantID	int
DECLARE	@SpouseID		int

SET NOCOUNT ON

declare	@client_creditor	int
declare	@client				int
select	@client_creditor	= client_creditor
from	client_creditor_proposals
where	client_creditor_proposal = @client_creditor_proposal

select	@client			= client,
		@addr1			= client_name
from	client_creditor WITH (NOLOCK)
where	client_creditor		= @client_creditor

SELECT	@Addr2 = upper(v.addr1),
		@Addr3 = upper(v.addr2),
		@Addr4 = upper(v.addr3),
	   	@postalcode = isnull(v.zipcode,'')
FROM 	view_client_address v WITH (NOLOCK)
WHERE	client = @client

-- Find the applicant and spouse from the reversed tables if needed
select	@applicantID = person
from	people with (nolock)
where	client		= @client
and		relation = 1;

select	@spouseID	= person
from	people with (nolock)
where	client		= @client
and		relation <> 1;

-- Swap the names if they are needed
if exists(select * from people p with (nolock) inner join client_creditor cc with (nolock) on cc.client_creditor = @client_creditor and cc.person = p.person WHERE p.relation <> 1) AND (@SpouseID is not null)
begin
	declare	@OldApplicantID	int
	select	@OldApplicantID	= @ApplicantID
	select	@ApplicantID	= @SpouseID,
			@SpouseID		= @OldApplicantID
end
			
-- Translate the names to a string
select	@Applicant		= dbo.format_normal_name(default, first, default, last, default)
from	names n
inner join people p on n.Name = p.NameID
where	p.person		= @ApplicantID;

select	@Spouse			= dbo.format_normal_name(default, first, default, last, default)
from	names n
inner join people p on n.Name = p.NameID
where	p.person		= @SpouseID;

-- Replace the applicant name with the override name if one is given
if exists(select * From client_creditor with (nolock) WHERE client_creditor = @client_creditor AND ltrim(rtrim(isnull(client_name,''))) <> '')
	select	@Applicant		= ltrim(rtrim(client_name))
	from	client_creditor with (nolock)
	where	client_creditor = @client_creditor;
	
-- Reduce the addresses to an adress block
select	@addr1 = ltrim(rtrim(@addr1))
if @addr1 = ''
	select @addr1 = null

if @Addr1 is null
	select @addr1 = @applicant

IF @spouse = ''
	SELECT @spouse = 'NONE'

IF @Addr3 = ''
	SELECT @Addr3 = @Addr4, @Addr4 = ''

IF @Addr2 = ''
	SELECT @Addr2 = @Addr3, @Addr3 = @Addr4, @Addr4 = ''

IF @Addr1 = ''
	SELECT @Addr1 = @Addr2, @Addr2 = @Addr3, @Addr3 = @Addr4, @Addr4 = ''

SET NOCOUNT OFF

SELECT
	upper(convert(varchar(256),@spouse))		AS 'spouse',
	upper(convert(varchar(256),@Addr1))		AS 'name',
	upper(convert(varchar(256),@Addr2))		AS 'address1',
	upper(convert(varchar(256),@Addr3))		AS 'address2',
	upper(convert(varchar(256),@Addr4))		AS 'address3',
	upper(convert(varchar(256),@postalcode))	AS 'zipcode',
	upper(convert(varchar(256),@applicant)) 	as 'applicant',
	isnull(@client,0)				as 'client',
	@client_creditor_proposal		as 'client_creditor_proposal'

RETURN ( 1 )
GO
