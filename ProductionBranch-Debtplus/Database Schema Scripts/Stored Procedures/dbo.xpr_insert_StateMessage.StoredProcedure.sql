USE [DebtPlus]
GO
/****** Object:  StoredProcedure [dbo].[xpr_insert_StateMessage]    Script Date: 09/15/2014 13:13:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[xpr_insert_StateMessage] ( @State int, @StateMessageType int, @Effective datetime, @Details text, @Reason varchar(512) ) as
-- ===============================================================================
-- ==          Insert an entry into the StateMessages table                     ==
-- ===============================================================================
insert into StateMessages ( [State], [StateMessageType], [Effective], [Details], [Reason], [date_updated], [updated_by] ) values ( @State, @StateMessageType, @Effective, @Details, @Reason, getdate(), suser_sname() )
return ( scope_identity() )
GO
