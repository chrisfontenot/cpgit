USE [DebtPlus]
GO
/****** Object:  StoredProcedure [dbo].[nfcc_quarterly_revenues_clients]    Script Date: 09/15/2014 13:13:32 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[nfcc_quarterly_revenues_clients] ( @from_date as datetime = null, @to_date as datetime = null )  AS 

-- This is the name of the resulting sheet in the spreadsheet output
declare	@SheetName	varchar(80)
select	@SheetName	= 'Revenues - Clients and Other '

-- Find last quarter if required
if (@to_date is null) and (@from_date is null)
begin
	select	@from_date	= min(dt) from calendar where y = datepart(year, getdate()) and q = (select q from calendar where dt = convert(varchar(10), getdate(), 101))
	select	@from_date	= dateadd(m, -3, @from_date)
	select	@to_date	= max(dt) from calendar where y = datepart(year, @from_date) and q = (select q from calendar where dt = convert(varchar(10), @from_date, 101))
end

select	@from_date	= convert(varchar(10), @from_date, 101) + ' 00:00:00',
	@to_date	= convert(varchar(10), @to_date, 101)   + ' 23:59:59';

-- Creditors for specific fees
declare	@financial_counseling_creditor		varchar(10)
declare	@housing_counseling_creditor		varchar(10)
declare	@pre_bankruptcy_counseling_creditor	varchar(10)
declare	@pre_discharge_counseling_creditor	varchar(10)
declare	@dmp_setup_creditor					varchar(10)
declare	@dmp_monthly_fees_creditor			varchar(10)
declare	@education_seminars_creditor		varchar(10)

select	@housing_counseling_creditor		= ''
select	@pre_bankruptcy_counseling_creditor	= ''
select	@pre_discharge_counseling_creditor	= ''
select	@education_seminars_creditor		= ''

-- These are in the config file
select	@dmp_setup_creditor					= isnull(setup_creditor,''),
		@dmp_monthly_fees_creditor			= isnull(paf_creditor,''),
		@financial_counseling_creditor		= isnull(counseling_creditor,'')
from	config;

-- Build a map of the creditors to determine where to place the answers
select	creditor, creditor_id, 8 as line, convert(money,0) as disbursements
into	#map_creditors
from	creditors with (nolock)
where	creditor in (@housing_counseling_creditor, @pre_bankruptcy_counseling_creditor, @pre_discharge_counseling_creditor, @education_seminars_creditor, @dmp_setup_creditor, @dmp_monthly_fees_creditor, @financial_counseling_creditor);

update	#map_creditors
set		line		= 14
where	creditor	in (@education_seminars_creditor);

update	#map_creditors
set		line		= 11
where	creditor	in (@pre_discharge_counseling_creditor);

update	#map_creditors
set		line		= 10
where	creditor	in (@pre_bankruptcy_counseling_creditor);

update	#map_creditors
set		line		= 9
where	creditor	in (@housing_counseling_creditor);

update	#map_creditors
set		line		= 8
where	creditor	in (@financial_counseling_creditor);

update	#map_creditors
set		line		= 12
where	creditor	in (@dmp_setup_creditor);

update	#map_creditors
set		line		= 13
where	creditor	in (@dmp_monthly_fees_creditor);

create index temp_ix_map_creditors_1 on #map_creditors ( creditor_id );
create index temp_ix_map_creditors_2 on #map_creditors ( creditor );

-- Find the disbursements in the period
select	x.creditor,
		x.line,
		sum(isnull(rcc.debit_amt,0) - isnull(rcc.credit_amt,0)) as disbursements
into	#disbursements
from	registers_creditor rcc with (nolock)
inner join #map_creditors x on rcc.creditor = x.creditor
where	rcc.tran_type in ('AD','BW','MD','CM','RR','VD','RF')
and	rcc.date_created between @from_date and @to_date
group by x.creditor, x.line;

update	#map_creditors
set		disbursements	= d.disbursements
from	#map_creditors m
inner join #disbursements d on m.creditor = d.creditor

drop table #disbursements

-- Merge the information together to form the columns and lines
select	line, sum(disbursements) as disbursements
into	#map_creditors_2
from	#map_creditors
group by line;

-- Build a list of the disbursement information
create table #results (sheet varchar(80), location varchar(80), value varchar(80))

insert into #results (sheet, location, value)
select	@SheetName, 'B' + convert(varchar, line) as location, convert(varchar, disbursements) as value

from	#map_creditors_2;

drop table #map_creditors;

-- Return the results
select * from #results where value is not null order by location;
drop table #results;
GO
