USE [DebtPlus]
GO
/****** Object:  StoredProcedure [dbo].[rpt_rpps_invalid_accounts_2]    Script Date: 09/15/2014 13:13:33 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[rpt_rpps_invalid_accounts_2] as

-- Version 2 of the report
--    This is a rewrite of the logic to update the payment information for debts
--    that are paid by RPPS. It will update the four fields in the client_creditor table
--    as follows:
--
--    rpps_mask                     : record number of the rpps_masks table that corresponds to the biller ID for RPPS
--    rpps_mask_timestamp           : timestamp of that record to determine if the value was changed
--    rpps_payment_mask             : record pointer to the creditor_methods table corresponding to the payment information.
--    rpps_payment_mask_timestamp   : timestamp of that record to determine if the value was changed
--
--    A client may not be paid if the rpps_payment_mask item is missing and the creditor has a RPPS biller creditor_method record
--    associated with it. We hold payments because the prorate would confuse the payment amounts and money for invalid debts would
--    go to good creditors.
--
--  ChangeLog
--     2/28/2008
--        Initial rewrite.
--     2/29/2008
--        Allow for the possibility of a non-null pointer but having it point to a non-existant biller mask record.
--   06/09/2009
--      Remove references to debt_number and use client_creditor
--   1/5/2009
--      Changed from using biller_aba to using rpps_biller_id

-- Suppress intermediate results
set nocount on

-- Erase the BILLER ID pointers if the biller ID changed information
update	client_creditor
set		rpps_mask					= null,
		rpps_mask_timestamp			= null,
		payment_rpps_mask			= null,
		payment_rpps_mask_timestamp	= null
from	client_creditor cc
LEFT OUTER join rpps_masks mk on cc.rpps_mask = mk.rpps_mask
where	cc.rpps_mask is not null
and		(mk.rpps_mask IS NULL
or		convert(binary(8), mk.[timestamp]) <> cc.rpps_mask_timestamp);

-- Erase the PAYMENT pointers if the timestamps differ
update	client_creditor
set		rpps_mask					= null,
		rpps_mask_timestamp			= null,
		payment_rpps_mask			= null,
		payment_rpps_mask_timestamp	= null
from	client_creditor cc
left outer join creditor_methods mt on cc.payment_rpps_mask = mt.creditor_method
where	cc.payment_rpps_mask is not null
and		(mt.creditor_method is null
or		convert(binary(8), mt.[timestamp]) <> cc.payment_rpps_mask_timestamp);

-- Create a table to hold the suspect information
create table #suspect_accounts (region int, creditor int, client_creditor int, account_number varchar(80), rpps_mask int null, rpps_mask_timestamp binary(8) null, payment_rpps_mask int null, payment_rpps_mask_timestamp binary(8) null, error_condition varchar(80) null);

-- Build a temp index to help things along
CREATE NONCLUSTERED INDEX ix1_suspect_accounts on #suspect_accounts ([payment_rpps_mask]) INCLUDE ([region],[creditor],[account_number]);

-- Build a list of the accounts which have been changed since the last time
insert into #suspect_accounts (region, creditor, client_creditor, account_number, rpps_mask, rpps_mask_timestamp, payment_rpps_mask, payment_rpps_mask_timestamp, error_condition)
select		c.region,
			cr.creditor_id,
			cc.client_creditor,
			cc.account_number,
			convert(int, null)			as rpps_mask,
			convert(binary(8), null)	as rpps_mask_timestamp,
			convert(int, null)			as payment_rpps_mask,
			convert(binary(8), null)	as payment_rpps_mask_timestamp,
			'MASK'						as error_condition
from		client_creditor cc
inner join	client_creditor_balances b on cc.client_creditor_balance = b.client_creditor_balance
inner join	clients c on cc.client = c.client
inner join	creditors cr on cc.creditor = cr.creditor
inner join	creditor_classes cl on cr.creditor_class = cl.creditor_class
where		(cc.rpps_mask is null or cc.payment_rpps_mask is null)
and			c.active_status in ('A','AR')
and			cc.reassigned_debt = 0
and			((b.orig_balance + b.orig_balance_adjustment + b.total_interest > b.total_payments) or (cl.zero_balance > 0))

-- Remove the accounts that do not have creditor method payments associated with them
delete		#suspect_accounts
from		#suspect_accounts x
left outer join creditor_methods m on x.creditor = m.creditor AND 'EFT' = m.type
where		m.creditor_method is null;

-- Set the ones that are specified as checks only
update		#suspect_accounts
set			payment_rpps_mask			= m.creditor_method,
			payment_rpps_mask_timestamp = convert(binary(8), m.[timestamp])
from		#suspect_accounts x
inner join	creditor_methods m on x.creditor = m.creditor and 'EFT' = m.type
inner join	banks b on m.bank = b.bank
where		b.type = 'C';

-- But remove those that have RPPS information associated with them as well.
update		#suspect_accounts
set			payment_rpps_mask			= null,
			payment_rpps_mask_timestamp	= null
from		#suspect_accounts x
inner join	creditor_methods m on x.creditor = m.creditor and 'EFT' = m.type
inner join	banks b on m.bank = b.bank
where		b.type = 'R';

-- Update the check accounts if there are any
update		client_creditor
set			payment_rpps_mask			= x.payment_rpps_mask,
			payment_rpps_mask_timestamp	= x.payment_rpps_mask_timestamp,
			rpps_mask					= null,
			rpps_mask_timestamp			= null
from		client_creditor cc
inner join	#suspect_accounts x on cc.client_creditor = x.client_creditor
where		x.payment_rpps_mask is not null;

-- Discard the check accounts from further consideration
delete
from		#suspect_accounts
where		payment_rpps_mask is not null;

-- Change the error conditions to BILLER if we can not find a biller
update		#suspect_accounts
set			error_condition				= 'BILLER'
from		#suspect_accounts x
left outer join	creditor_methods m on x.creditor = m.creditor AND 'EFT' = m.type
left outer join	banks b on m.bank = b.bank and 'R' = b.type
left outer join	rpps_biller_ids ids on m.rpps_biller_id = ids.rpps_biller_id
where		ids.rpps_biller_id is null;

-- Now, attempt to find the non-checked payment methods
-- If we find a match to the mask for the biller then clear the error and save the pointers.
update		#suspect_accounts
set			rpps_mask					= mk.rpps_mask,
			rpps_mask_timestamp			= convert(binary(8), mk.[timestamp]),
			payment_rpps_mask			= mt.creditor_method,
			payment_rpps_mask_timestamp	= convert(binary(8), mt.[timestamp]),
			error_condition				= null
from		#suspect_accounts a
inner join	creditor_methods mt on a.creditor = mt.creditor and 'EFT' = mt.type and mt.region in (0, a.region)
inner join	banks b on mt.bank = b.bank and 'R' = b.type
inner join	rpps_biller_ids ids on mt.rpps_biller_id = ids.rpps_biller_id
inner join	rpps_masks mk on ids.rpps_biller_id = mk.rpps_biller_id
where		a.payment_rpps_mask is null
and			a.account_number like dbo.map_rpps_masks ( mk.mask );

-- Reset the items which have invalid checksums
-- Those that have a LUHN checksum, validate the account numbers.
update		#suspect_accounts
set			rpps_mask					= null,
			rpps_mask_timestamp			= null,
			payment_rpps_mask			= null,
			payment_rpps_mask_timestamp	= null,
			error_condition				= 'CHECKSUM'
from		#suspect_accounts a
inner join  creditor_methods m on a.payment_rpps_mask = m.creditor_method
inner join  rpps_masks ids with (nolock) on a.rpps_mask = ids.rpps_mask
where		ids.checkdigit <> 0
and			dbo.valid_checksum_luhn ( a.account_number ) = 0;

-- Update the debt record with the new information
update		client_creditor
set			rpps_mask					= a.rpps_mask,
			rpps_mask_timestamp			= a.rpps_mask_timestamp,
			payment_rpps_mask			= a.payment_rpps_mask,
			payment_rpps_mask_timestamp	= a.payment_rpps_mask_timestamp
from		client_creditor cc
inner join	#suspect_accounts a on cc.client_creditor = a.client_creditor;

-- Toss all of the good records
delete
from		#suspect_accounts
where		error_condition is null;

-- Return the information for the report
select		cc.client,
			cc.creditor,
			cc.client_creditor,
			isnull (cc.client_name, dbo.format_normal_name ( pn.prefix, pn.first, pn.middle, pn.last, pn.suffix )) as client_name,
			cr.creditor_name,
			cc.account_number,
			x.error_condition
from		#suspect_accounts x
inner join	client_creditor cc on x.client_creditor = cc.client_creditor
inner join	client_creditor_balances b on cc.client_creditor_balance = b.client_creditor_balance
inner join	creditors cr on cc.creditor = cr.creditor
left outer join people p on cc.client = p.client and 1 = p.relation
left outer join names pn with (nolock) on p.nameid = pn.name
inner join	clients c on cc.client = c.client
inner join	creditor_classes cl on cr.creditor_class = cl.creditor_class
order by	cc.creditor, cc.client, cc.client_creditor;

drop table	#suspect_accounts
return ( 0 )
GO
