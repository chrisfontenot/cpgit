USE [DebtPlus]
GO
/****** Object:  StoredProcedure [dbo].[cmd_delete_posted_RF_item]    Script Date: 09/15/2014 13:13:31 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[cmd_delete_posted_RF_item] ( @deposit_batch_id as int, @client_creditor as int, @deposit_detail_key as int = null ) as

-- ==============================================================================================================
-- ==            Remove the indicated refund item from the deposit batch                                       ==
-- ==============================================================================================================

-- ChangeLog
--   8/23/2003
--     Removed creditor ID from trust register. Creditor refunds are not listed with a creditor on RF transactions.
--   8/9/2004
--     Corrected current month disbursement figures.
--   8/25/2004
--     Corrected current month disbursed amount. Added rather than subtracted the amount.
--   06/09/2009
--      Remove references to debt_number and use client_creditor

-- Start a transaction for the operation
begin transaction

declare @deposit_batch_detail		int
declare	@batch_type			varchar(10)
declare	@trust_register			int
declare	@date_posted			datetime

declare	@amount				money
declare	@fairshare_amt			money
declare	@fairshare_pct			float
declare	@creditor_type			varchar(10)
declare	@client_creditor_balance	int
declare	@client					int
declare	@creditor				varchar(10)

-- Find the information from the batch header
select	@batch_type	= batch_type,
		@trust_register	= trust_register,
		@date_posted	= date_posted
from	deposit_batch_ids
where	deposit_batch_id = @deposit_batch_id

if @batch_type is null
begin
	rollback transaction
	raiserror ('The deposit batch does not exist. Please correct it.', 16, 1)
	return ( 0 )
end

if @batch_type <> 'CR'
begin
	rollback transaction
	raiserror ('The deposit batch is not a creditor refund batch.', 16, 1)
	return ( 0 )
end

if (@trust_register is null) or (@date_posted is null)
begin
	rollback transaction
	raiserror ('The creditor refund batch has not been posted. Please use the normal tools for removing / editing this item.', 16, 1)
	return ( 0 )
end

-- Find the debt information
select	@client						= client,
		@creditor					= creditor,
		@client_creditor_balance	= client_creditor_balance
from	client_creditor
where	client_creditor				= @client_creditor

if @@rowcount < 1
begin
	rollback transaction
	RaisError ('The debt could not be located in the system', 16, 1)
	return ( 0 )
end

-- Find the deposit record. This will determine the amounts.
declare	@record			int

if @deposit_detail_key is null

	-- If a key was not passed then find the last deposit record that matches this debt
	select	@record			= max(deposit_batch_detail)
	from	deposit_batch_details
	where	deposit_batch_id	= @deposit_batch_id
	and		client_creditor		= @client_creditor

else

	-- A key was specified. Use the specific record
	select	@record			= deposit_batch_detail
	from	deposit_batch_details
	where	deposit_batch_id	= @deposit_batch_id
	and		client_creditor		= @client_creditor
	and	deposit_batch_detail	= @deposit_detail_key

if @record is null
begin
	rollback transaction
	RaisError ('The transacton could not be loaded in the list of deposits for this deposit batch', 16, 1)
	return ( 0 )
end

-- Find the deposit item
select	@amount			= amount,
		@fairshare_amt		= fairshare_amt,
		@fairshare_pct		= fairshare_pct,
		@creditor_type		= creditor_type,
	@deposit_batch_detail	= deposit_batch_detail
from	deposit_batch_details
where	deposit_batch_detail	= @record

-- Reduce the net amount from the trust register
update	registers_trust
set		amount		= amount - (@amount - @fairshare_amt)
where	trust_register	= @trust_register
and	tran_type	= 'RF'

if @@rowcount < 1
begin
	rollback transaction
	RaisError ('The trust register item could not be properly loaded as a creditor refund.', 16, 1)
	return ( 0 )
end

-- Make sure that we find only one match. It is easier this way to remove one than two where they may have duplicated items.
set rowcount 1

-- Remove the payment refund from the client's balance
update	client_creditor_balances
set	total_payments		= total_payments + @amount,
	payments_month_0	= isnull(payments_month_0,0) + @amount
where	client_creditor_balance	= @client_creditor_balance

-- Update the debt refund amounts
update	client_creditor
set	returns_this_creditor	= isnull(returns_this_creditor,0) - @amount
where	client_creditor		= @client_creditor

-- Update the creditor statistics
update	creditors
set	distrib_mtd		= distrib_mtd + @amount
where	creditor		= @creditor

-- Remove the money from the client's account
if @amount > 0
begin
	update	clients
	set	held_in_trust	= isnull(held_in_trust,0) - @amount
	where	client		= @client

	insert into registers_client (tran_type, client, debit_amt, message)
	values ('RF', @client, @amount, 'Removal of refund posted in error')
end

-- Credit the deduction account
if @fairshare_amt > 0
begin
	update	clients
	set	held_in_trust	= isnull(held_in_trust,0) + @fairshare_amt
	where	client		= 0

	insert into registers_client (tran_type, client, credit_amt, message)
	values ('RF', 0, @fairshare_amt, 'Removal of refund posted in error')
end

-- Remove the creditor refund transaction
delete
from	registers_creditor
where	trust_register	= @trust_register
and	tran_type	= 'RF'
and	creditor	= @creditor
and	credit_amt	= @amount

-- Remove the debt transaction
delete
from	registers_client_creditor
where	trust_register	= @trust_register
and	client_creditor	= @client_creditor
and	tran_type	= 'RF'
and	credit_amt	= @amount
and	fairshare_amt	= @fairshare_amt

-- Discard the detail information for the refund transaction
delete
from	deposit_batch_details
where	deposit_batch_detail	= @deposit_batch_detail

set rowcount 0

-- Commit the transaction and leave
commit transaction
return ( 1 )
GO
