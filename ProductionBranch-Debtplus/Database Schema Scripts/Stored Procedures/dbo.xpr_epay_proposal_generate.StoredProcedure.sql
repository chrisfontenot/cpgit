USE [DebtPlus]
GO
/****** Object:  StoredProcedure [dbo].[xpr_epay_proposal_generate]    Script Date: 09/15/2014 13:13:35 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE PROCEDURE [dbo].[xpr_epay_proposal_generate] (@epay_file as int = null) AS
-- ===================================================================================================
-- ==            Generate transactions for the EPAY proposal records                               ==
-- ===================================================================================================
-- Disable intermediate result sets
SET NOCOUNT ON

-- Start a transaction for the operation
BEGIN TRANSACTION
SET XACT_ABORT ON

-- Start a transaction for the operation
BEGIN TRANSACTION

-- Disable intermediate result sets
SET NOCOUNT ON

declare	@bank		int

select	@bank		= min(bank)
from	banks with (nolock)
where	type		= 'E'
and	[default]	= 1

if @bank is null
	select	@bank	= min(bank)
	from	banks with (nolock)
	where	type	= 'E'

if @bank is null
	select	@bank	= 5

create table #pending_proposals (client int,
				creditor varchar(10) null,
				client_creditor int null,
				proposal_batch_id int null,
				client_creditor_proposal int null,
				epay_biller_id varchar(50) null )

-- Find the proposals that we wish to process
insert into #pending_proposals (client, creditor, client_creditor, client_creditor_proposal, proposal_batch_id, epay_biller_id)
SELECT		cc.client					as 'client',
		cc.creditor					as 'creditor',
		cc.client_creditor				as 'client_creditor',
		p.client_creditor_proposal			as 'client_creditor_proposal',
		p.proposal_batch_id				as 'proposal_batch_id',
		p.epay_biller_id				as 'epay_biller_id'

FROM		client_creditor_proposals p
INNER JOIN	proposal_batch_ids pb		WITH (NOLOCK) ON p.proposal_batch_id	= pb.proposal_batch_id
INNER JOIN	client_creditor cc		WITH (NOLOCK) ON p.client_creditor	= cc.client_creditor
INNER JOIN	clients c			WITH (NOLOCK) ON cc.client		= c.client

WHERE		c.active_status not in ('CRE', 'EX', 'I')	-- Active clients
AND		pb.date_closed IS NOT NULL			-- Closed but
AND		pb.date_transmitted IS NULL			-- Not transmitted
AND		pb.bank	= @bank					-- For the desired bank

-- Include the proposals in the batch
insert into	epay_transactions (request, epay_biller_id, client_creditor_proposal)
select		'1'				as request,
		cr.epay_proposal_id		as epay_biller_id,
		p.client_creditor_proposal	as client_creditor_proposal
from		#pending_proposals

-- Update the proposal transmitted date
update		proposal_batch_ids
set		date_transmitted	= getdate()
from		proposal_batch_ids b
inner join	#pending_proposals p on b.proposal_batch_id = p.proposal_batch_id;

-- Discard the working table
drop table #pending_proposals

-- Make the changes permanent
COMMIT TRANSACTION
RETURN ( 1 )
GO
