USE [DebtPlus]
GO
/****** Object:  StoredProcedure [dbo].[xpr_ACH_trace_update]    Script Date: 09/15/2014 13:13:34 ******/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE PROCEDURE [dbo].[xpr_ACH_trace_update] ( @ach_file as int, @batch_number as int, @trace_number as int ) AS

-- =========================================================================================================
-- ==            Update the information to generate the next ACH file                                     ==
-- =========================================================================================================

declare	@bank		int
select	@bank		= bank
from	banks
where	type		= 'A'

-- Update the id numbers for the next batch
if @bank is not null
begin
	update	banks
	set	transaction_number	= @trace_number,
		batch_number		= @batch_number
	where	bank			= @bank

	return ( @@rowcount )
end

-- Return the status that the bank could not be located
return ( 0 )
GO
