SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[xpr_housing_arm_v4_summary] as

-- Suppress intermediate results
set nocount on

-- Create the table to hold the results
create table #results (hcs_id int not null, tag varchar(128) not null, hud_count int null, all_count int null );

insert into #results (hcs_id, tag, hud_count, all_count) select hcs_id, 'Ethnicity_Clients_Counseling_Hispanic', sum(isnull(is_hud_client,0)) as hud_count, isnull(count(*),0) as all_count from ##hud_9902_clients where [Ethnicity_Clients_Counseling_Hispanic] = 1 group by hcs_id;
insert into #results (hcs_id, tag, hud_count, all_count) select hcs_id, 'Ethnicity_Clients_Counseling_No_Response', sum(isnull(is_hud_client,0)) as hud_count, isnull(count(*),0) as all_count from ##hud_9902_clients where [Ethnicity_Clients_Counseling_No_Response] = 1 group by hcs_id;
insert into #results (hcs_id, tag, hud_count, all_count) select hcs_id, 'Ethnicity_Clients_Counseling_Non_Hispanic', sum(isnull(is_hud_client,0)) as hud_count, isnull(count(*),0) as all_count from ##hud_9902_clients where [Ethnicity_Clients_Counseling_Non_Hispanic] = 1 group by hcs_id;

insert into #results (hcs_id, tag, hud_count, all_count) select hcs_id, 'Household_Lives_In_Rural_Area', sum(isnull(is_hud_client,0)) as hud_count, isnull(count(*),0) as all_count from ##hud_9902_clients where [Household_Lives_In_Rural_Area] = 1 group by hcs_id;
insert into #results (hcs_id, tag, hud_count, all_count) select hcs_id, 'Household_Does_Not_Live_In_Rural_Area', sum(isnull(is_hud_client,0)) as hud_count, isnull(count(*),0) as all_count from ##hud_9902_clients where [Household_Does_Not_Live_In_Rural_Area] = 1 group by hcs_id;
insert into #results (hcs_id, tag, hud_count, all_count) select hcs_id, 'Rural_Area_No_Response', sum(isnull(is_hud_client,0)) as hud_count, isnull(count(*),0) as all_count from ##hud_9902_clients where [Rural_Area_No_Response] = 1 group by hcs_id;

insert into #results (hcs_id, tag, hud_count, all_count) select hcs_id, 'Household_Is_Limited_English_Proficient', sum(isnull(is_hud_client,0)) as hud_count, isnull(count(*),0) as all_count from ##hud_9902_clients where [Household_Is_Limited_English_Proficient] = 1 group by hcs_id;
insert into #results (hcs_id, tag, hud_count, all_count) select hcs_id, 'Limited_English_Proficient_No_Response', sum(isnull(is_hud_client,0)) as hud_count, isnull(count(*),0) as all_count from ##hud_9902_clients where [Limited_English_Proficient_No_Response] = 1 group by hcs_id;
insert into #results (hcs_id, tag, hud_count, all_count) select hcs_id, 'Household_Is_Not_Limited_English_Proficient', sum(isnull(is_hud_client,0)) as hud_count, isnull(count(*),0) as all_count from ##hud_9902_clients where [Household_Is_Not_Limited_English_Proficient] = 1 group by hcs_id;

insert into #results (hcs_id, tag, hud_count, all_count) select hcs_id, 'Less30_AMI_Level', sum(isnull(is_hud_client,0)) as hud_count, isnull(count(*),0) as all_count from ##hud_9902_clients where [Less30_AMI_Level] = 1 group by hcs_id;
insert into #results (hcs_id, tag, hud_count, all_count) select hcs_id, 'a30_49_AMI_Level', sum(isnull(is_hud_client,0)) as hud_count, isnull(count(*),0) as all_count from ##hud_9902_clients where [a30_49_AMI_Level] = 1 group by hcs_id;
insert into #results (hcs_id, tag, hud_count, all_count) select hcs_id, 'a50_79_AMI_Level', sum(isnull(is_hud_client,0)) as hud_count, isnull(count(*),0) as all_count from ##hud_9902_clients where [a50_79_AMI_Level] = 1 group by hcs_id;
insert into #results (hcs_id, tag, hud_count, all_count) select hcs_id, 'a80_100_AMI_Level', sum(isnull(is_hud_client,0)) as hud_count, isnull(count(*),0) as all_count from ##hud_9902_clients where [a80_100_AMI_Level] = 1 group by hcs_id;
insert into #results (hcs_id, tag, hud_count, all_count) select hcs_id, 'Greater100_AMI_Level', sum(isnull(is_hud_client,0)) as hud_count, isnull(count(*),0) as all_count from ##hud_9902_clients where [Greater100_AMI_Level] = 1 group by hcs_id;
insert into #results (hcs_id, tag, hud_count, all_count) select hcs_id, 'AMI_No_Response', sum(isnull(is_hud_client,0)) as hud_count, isnull(count(*),0) as all_count from ##hud_9902_clients where [AMI_No_Response] = 1 group by hcs_id;

insert into #results (hcs_id, tag, hud_count, all_count) select hcs_id, 'MultiRace_Clients_Counseling_AMINDWHT', sum(isnull(is_hud_client,0)) as hud_count, isnull(count(*),0) as all_count from ##hud_9902_clients where [MultiRace_Clients_Counseling_AMINDWHT] = 1 group by hcs_id;
insert into #results (hcs_id, tag, hud_count, all_count) select hcs_id, 'MultiRace_Clients_Counseling_AMRCINDBLK', sum(isnull(is_hud_client,0)) as hud_count, isnull(count(*),0) as all_count from ##hud_9902_clients where [MultiRace_Clients_Counseling_AMRCINDBLK] = 1 group by hcs_id;
insert into #results (hcs_id, tag, hud_count, all_count) select hcs_id, 'MultiRace_Clients_Counseling_ASIANWHT', sum(isnull(is_hud_client,0)) as hud_count, isnull(count(*),0) as all_count from ##hud_9902_clients where [MultiRace_Clients_Counseling_ASIANWHT] = 1 group by hcs_id;
insert into #results (hcs_id, tag, hud_count, all_count) select hcs_id, 'MultiRace_Clients_Counseling_BLKWHT', sum(isnull(is_hud_client,0)) as hud_count, isnull(count(*),0) as all_count from ##hud_9902_clients where [MultiRace_Clients_Counseling_BLKWHT] = 1 group by hcs_id;
insert into #results (hcs_id, tag, hud_count, all_count) select hcs_id, 'MultiRace_Clients_Counseling_NoResponse', sum(isnull(is_hud_client,0)) as hud_count, isnull(count(*),0) as all_count from ##hud_9902_clients where [MultiRace_Clients_Counseling_NoResponse] = 1 group by hcs_id;
insert into #results (hcs_id, tag, hud_count, all_count) select hcs_id, 'MultiRace_Clients_Counseling_OtherMLTRC', sum(isnull(is_hud_client,0)) as hud_count, isnull(count(*),0) as all_count from ##hud_9902_clients where [MultiRace_Clients_Counseling_OtherMLTRC] = 1 group by hcs_id;
insert into #results (hcs_id, tag, hud_count, all_count) select hcs_id, 'Race_Clients_Counseling_American_Indian_Alaskan_Native', sum(isnull(is_hud_client,0)) as hud_count, isnull(count(*),0) as all_count from ##hud_9902_clients where [Race_Clients_Counseling_American_Indian_Alaskan_Native] = 1 group by hcs_id;
insert into #results (hcs_id, tag, hud_count, all_count) select hcs_id, 'Race_Clients_Counseling_Asian', sum(isnull(is_hud_client,0)) as hud_count, isnull(count(*),0) as all_count from ##hud_9902_clients where [Race_Clients_Counseling_Asian] = 1 group by hcs_id;
insert into #results (hcs_id, tag, hud_count, all_count) select hcs_id, 'Race_Clients_Counseling_Black_AfricanAmerican', sum(isnull(is_hud_client,0)) as hud_count, isnull(count(*),0) as all_count from ##hud_9902_clients where [Race_Clients_Counseling_Black_AfricanAmerican] = 1 group by hcs_id;
insert into #results (hcs_id, tag, hud_count, all_count) select hcs_id, 'Race_Clients_Counseling_Pacific_Islanders', sum(isnull(is_hud_client,0)) as hud_count, isnull(count(*),0) as all_count from ##hud_9902_clients where [Race_Clients_Counseling_Pacific_Islanders] = 1 group by hcs_id;
insert into #results (hcs_id, tag, hud_count, all_count) select hcs_id, 'Race_Clients_Counseling_White', sum(isnull(is_hud_client,0)) as hud_count, isnull(count(*),0) as all_count from ##hud_9902_clients where [Race_Clients_Counseling_White] = 1 group by hcs_id;

insert into #results (hcs_id, tag, hud_count, all_count) select hcs_id, 'Compl_HomeMaint_FinMngt', sum(isnull(is_hud_client,0)) as hud_count, isnull(count(*),0) as all_count from ##hud_9902_clients where [Compl_HomeMaint_FinMngt] = 1 group by hcs_id;
insert into #results (hcs_id, tag, hud_count, all_count) select hcs_id, 'Compl_Workshop_Predatory_Lend', sum(isnull(is_hud_client,0)) as hud_count, isnull(count(*),0) as all_count from ##hud_9902_clients where [Compl_Workshop_Predatory_Lend] = 1 group by hcs_id;
insert into #results (hcs_id, tag, hud_count, all_count) select hcs_id, 'Compl_Help_FairHousing_Workshop', sum(isnull(is_hud_client,0)) as hud_count, isnull(count(*),0) as all_count from ##hud_9902_clients where [Compl_Help_FairHousing_Workshop] = 1 group by hcs_id;
insert into #results (hcs_id, tag, hud_count, all_count) select hcs_id, 'Compl_Resolv_Prevent_Mortg_Deliq', sum(isnull(is_hud_client,0)) as hud_count, isnull(count(*),0) as all_count from ##hud_9902_clients where [Compl_Resolv_Prevent_Mortg_Deliq] = 1 group by hcs_id;
insert into #results (hcs_id, tag, hud_count, all_count) select hcs_id, 'Counseling_Rental_Workshop', sum(isnull(is_hud_client,0)) as hud_count, isnull(count(*),0) as all_count from ##hud_9902_clients where [Counseling_Rental_Workshop] = 1 group by hcs_id;
insert into #results (hcs_id, tag, hud_count, all_count) select hcs_id, 'Compl_HomeBuyer_Educ_Workshop', sum(isnull(is_hud_client,0)) as hud_count, isnull(count(*),0) as all_count from ##hud_9902_clients where [Compl_HomeBuyer_Educ_Workshop] = 1 group by hcs_id;
insert into #results (hcs_id, tag, hud_count, all_count) select hcs_id, 'Compl_NonDelinqency_PostPurchase_Workshop', sum(isnull(is_hud_client,0)) as hud_count, isnull(count(*),0) as all_count from ##hud_9902_clients where [Compl_NonDelinqency_PostPurchase_Workshop] = 1 group by hcs_id;
insert into #results (hcs_id, tag, hud_count, all_count) select hcs_id, 'Compl_Other_Workshop', sum(isnull(is_hud_client,0)) as hud_count, isnull(count(*),0) as all_count from ##hud_9902_clients where [Compl_Other_Workshop] = 1 group by hcs_id;
insert into #results (hcs_id, tag, hud_count, all_count) select hcs_id, 'One_Homeless_Assistance_Counseling', sum(isnull(is_hud_client,0)) as hud_count, isnull(count(*),0) as all_count from ##hud_9902_clients where [One_Homeless_Assistance_Counseling] = 1 group by hcs_id;
insert into #results (hcs_id, tag, hud_count, all_count) select hcs_id, 'One_Rental_Topics_Counseling', sum(isnull(is_hud_client,0)) as hud_count, isnull(count(*),0) as all_count from ##hud_9902_clients where [One_Rental_Topics_Counseling] = 1 group by hcs_id;
insert into #results (hcs_id, tag, hud_count, all_count) select hcs_id, 'One_PrePurchase_HomeBuying_Counseling', sum(isnull(is_hud_client,0)) as hud_count, isnull(count(*),0) as all_count from ##hud_9902_clients where [One_PrePurchase_HomeBuying_Counseling] = 1 group by hcs_id;
insert into #results (hcs_id, tag, hud_count, all_count) select hcs_id, 'One_Home_Maintenance_Fin_Management_Counseling', sum(isnull(is_hud_client,0)) as hud_count, isnull(count(*),0) as all_count from ##hud_9902_clients where [One_Home_Maintenance_Fin_Management_Counseling] = 1 group by hcs_id;
insert into #results (hcs_id, tag, hud_count, all_count) select hcs_id, 'One_Reverse_Mortgage_Counseling', sum(isnull(is_hud_client,0)) as hud_count, isnull(count(*),0) as all_count from ##hud_9902_clients where [One_Reverse_Mortgage_Counseling] = 1 group by hcs_id;
insert into #results (hcs_id, tag, hud_count, all_count) select hcs_id, 'One_Resolv_Prevent_Mortg_Delinq_Counseling', sum(isnull(is_hud_client,0)) as hud_count, isnull(count(*),0) as all_count from ##hud_9902_clients where [One_Resolv_Prevent_Mortg_Delinq_Counseling] = 1 group by hcs_id;

insert into #results (hcs_id, tag, hud_count, all_count) select hcs_id, 'Impact_One_On_One_And_Group', sum(isnull(is_hud_client,0)) as hud_count, isnull(count(*),0) as all_count from ##hud_9902_clients where [Impact_One_On_One_And_Group] = 1 group by hcs_id;
insert into #results (hcs_id, tag, hud_count, all_count) select hcs_id, 'Impact_Received_Info_Fair_Housing', sum(isnull(is_hud_client,0)) as hud_count, isnull(count(*),0) as all_count from ##hud_9902_clients where [Impact_Received_Info_Fair_Housing] = 1 group by hcs_id;
insert into #results (hcs_id, tag, hud_count, all_count) select hcs_id, 'Impact_Developed_Sustainable_Budget', sum(isnull(is_hud_client,0)) as hud_count, isnull(count(*),0) as all_count from ##hud_9902_clients where [Impact_Developed_Sustainable_Budget] = 1 group by hcs_id;
insert into #results (hcs_id, tag, hud_count, all_count) select hcs_id, 'Impact_Improved_Financial_Capacity', sum(isnull(is_hud_client,0)) as hud_count, isnull(count(*),0) as all_count from ##hud_9902_clients where [Impact_Improved_Financial_Capacity] = 1 group by hcs_id;
insert into #results (hcs_id, tag, hud_count, all_count) select hcs_id, 'Impact_Gained_Access_Resources_Improve_Housing', sum(isnull(is_hud_client,0)) as hud_count, isnull(count(*),0) as all_count from ##hud_9902_clients where [Impact_Gained_Access_Resources_Improve_Housing] = 1 group by hcs_id;
insert into #results (hcs_id, tag, hud_count, all_count) select hcs_id, 'Impact_Gained_Access_NonHousing_Resources', sum(isnull(is_hud_client,0)) as hud_count, isnull(count(*),0) as all_count from ##hud_9902_clients where [Impact_Gained_Access_NonHousing_Resources] = 1 group by hcs_id;
insert into #results (hcs_id, tag, hud_count, all_count) select hcs_id, 'Impact_Homeless_Obtained_Housing', sum(isnull(is_hud_client,0)) as hud_count, isnull(count(*),0) as all_count from ##hud_9902_clients where [Impact_Homeless_Obtained_Housing] = 1 group by hcs_id;
insert into #results (hcs_id, tag, hud_count, all_count) select hcs_id, 'Impact_Received_Rental_Counseling_Avoided_Eviction', sum(isnull(is_hud_client,0)) as hud_count, isnull(count(*),0) as all_count from ##hud_9902_clients where [Impact_Received_Rental_Counseling_Avoided_Eviction] = 1 group by hcs_id;
insert into #results (hcs_id, tag, hud_count, all_count) select hcs_id, 'Impact_Received_Rental_Counseling_Improved_Living_Conditions', sum(isnull(is_hud_client,0)) as hud_count, isnull(count(*),0) as all_count from ##hud_9902_clients where [Impact_Received_Rental_Counseling_Improved_Living_Conditions] = 1 group by hcs_id;
insert into #results (hcs_id, tag, hud_count, all_count) select hcs_id, 'Impact_Received_PrePurchase_Counseling_Purchased_Housing', sum(isnull(is_hud_client,0)) as hud_count, isnull(count(*),0) as all_count from ##hud_9902_clients where [Impact_Received_PrePurchase_Counseling_Purchased_Housing] = 1 group by hcs_id;
insert into #results (hcs_id, tag, hud_count, all_count) select hcs_id, 'Impact_Received_Reverse_Mortgage_Counseling_Obtained_HECM', sum(isnull(is_hud_client,0)) as hud_count, isnull(count(*),0) as all_count from ##hud_9902_clients where [Impact_Received_Reverse_Mortgage_Counseling_Obtained_HECM] = 1 group by hcs_id;
insert into #results (hcs_id, tag, hud_count, all_count) select hcs_id, 'Impact_Received_NonDelinquency_PostPurchase_Counseling_Improve_Conditions_Affordability', sum(isnull(is_hud_client,0)) as hud_count, isnull(count(*),0) as all_count from ##hud_9902_clients where [Impact_Received_NonDelinquency_PostPurchase_Counseling_Improve_Conditions_Affordability] = 1 group by hcs_id;
insert into #results (hcs_id, tag, hud_count, all_count) select hcs_id, 'Impact_Prevented_Resolved_Mortgage_Default', sum(isnull(is_hud_client,0)) as hud_count, isnull(count(*),0) as all_count from ##hud_9902_clients where [Impact_Prevented_Resolved_Mortgage_Default] = 1 group by hcs_id;

-- Insert a summary of the totals for the summary report
insert into #results (hcs_id, tag, hud_count, all_count) select 0, tag, sum(hud_count) as hud_count, sum(all_count) as all_count from #results group by tag;

-- Return the results
select hcs_id, tag, hud_count, all_count as all_count from #results order by hcs_id, tag
drop table #results;

return ( 1 )

GO
