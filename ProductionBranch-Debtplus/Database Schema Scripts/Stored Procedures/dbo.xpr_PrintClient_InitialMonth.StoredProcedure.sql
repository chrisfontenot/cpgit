USE [DebtPlus]
GO
/****** Object:  StoredProcedure [dbo].[xpr_PrintClient_InitialMonth]    Script Date: 09/15/2014 13:13:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[xpr_PrintClient_InitialMonth] ( @Client AS INT = NULL ) AS
-- ======================================================================================
-- ==        Fetch the initial monthly DMP interest amounts                            ==
-- ======================================================================================

-- ChangeLog
--   9/31/2002
--     Added support for client_creditor_balances table

-- Do not generate any extra result sets
SET ANSI_WARNINGS OFF
SET NOCOUNT ON

SELECT
	bal.orig_balance + bal.orig_balance_adjustment + bal.total_interest - bal.total_payments as 'balance',
	non_dmp_interest as 'non_dmp_interest',

	isnull(cc.dmp_interest,
		case
			when isnull(bal.orig_balance,0)+isnull(bal.orig_balance_adjustment,0)+isnull(bal.total_interest,0)-isnull(bal.total_payments,0) < cr.medium_apr_amt then cr.lowest_apr_pct
			when isnull(bal.orig_balance,0)+isnull(bal.orig_balance_adjustment,0)+isnull(bal.total_interest,0)-isnull(bal.total_payments,0) < cr.highest_apr_amt then cr.medium_apr_pct
			else cr.highest_apr_pct
		end
	) as 'dmp_interest'
INTO
	#t_PrintClient_Savings_1
FROM
	client_creditor cc
INNER JOIN
	client_creditor_balances bal on cc.client_creditor_balance = bal.client_creditor_balance
LEFT OUTER JOIN
	creditors cr ON cc.creditor = cr.creditor
WHERE
	cc.client = @client
AND
	cc.reassigned_debt = 0
AND
	(bal.orig_balance + bal.orig_balance_adjustment + bal.total_interest) > bal.total_payments

-- Retrieve the sum of the first month's interest amount
SELECT
	SUM(balance * non_dmp_interest / 12.0) as 'SELF',
	SUM(balance * dmp_interest / 12.0) as 'PLAN'
FROM
	#t_PrintClient_Savings_1

-- Restore the parameters
SET ANSI_WARNINGS ON
SET NOCOUNT OFF

RETURN ( 1 )
GO
