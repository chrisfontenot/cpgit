USE [DebtPlus]
GO
/****** Object:  StoredProcedure [dbo].[rpt_intake_assets]    Script Date: 09/15/2014 13:13:33 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[rpt_intake_assets] ( @intake_client as int ) as
select	i.description, d.asset_amount, isnull(i.maximum, convert(money,2147895.0)) as 'maximum'
from	intake_assets d
full outer join asset_ids i on d.asset_id = i.asset_id
where	d.intake_client = @intake_client
or	d.intake_client is null
order by 1

return ( @@rowcount )
GO
