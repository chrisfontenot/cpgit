USE [DebtPlus]
GO
/****** Object:  StoredProcedure [dbo].[rpt_Early_Pay]    Script Date: 09/15/2014 13:13:33 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE PROCEDURE [dbo].[rpt_Early_Pay] (@FromDate as datetime, @counselor as int = null) AS
-- ScZ / 01.15.2004

-- 
select  @FromDate = convert (datetime, convert(varchar(10), @FromDate, 101) + ' 00:00:00')

if @counselor is not null
begin
        if not exists (select * from counselors where counselor = @counselor)
	        select @counselor = null
end

-- Find the latest disbursed date for each client
-- (This is fairly fast. The table is small.)
select  client, max(date_created) as disbursed_date
into    #disb
from    registers_client with (nolock)
where   tran_type = 'AD'
and     date_created >= @FromDate
group by client;

if @counselor is null
begin
	select  v.client, v.name, disbursement_date, c.held_in_trust, isnull(co.name,'') as counselor, x.disbursed_date
	from    view_client_address v
	inner join clients c on c.client = v.client
	left outer join view_counselors co on c.counselor = co.counselor
	inner join #disb x on c.client = x.client
	where   c.held_in_trust > 50
	order by c.held_in_trust, v.client

end else begin

	select  v.client, v.name, disbursement_date, c.held_in_trust, isnull(co.name,'') as counselor, x.disbursed_date
	from    view_client_address v
	inner join clients c on c.client = v.client
	left outer join view_counselors co on c.counselor = co.counselor
	inner join #disb x on c.client = x.client
	where   c.held_in_trust > 50
	and     co.counselor = @counselor
	order by c.held_in_trust, v.client
end

drop table #disb
GO
