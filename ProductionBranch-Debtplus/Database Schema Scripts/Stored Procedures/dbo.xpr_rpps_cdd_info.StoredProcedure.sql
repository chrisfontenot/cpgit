USE [DebtPlus]
GO
/****** Object:  StoredProcedure [dbo].[xpr_rpps_cdd_info]    Script Date: 09/15/2014 13:13:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[xpr_rpps_cdd_info] ( @rpps_transaction AS INT ) AS
-- ===========================================================================================
-- ==            Generate an RPPS CDD information for a specific proposal                   ==
-- ===========================================================================================

-- ChangeLog
--    8/13/2003
--      Made the ssn field default to zeros rather than spaces. It is now "required".
--   06/09/2009
--      Remove references to debt_number and use client_creditor

-- Suppress intermediate result sets
SET NOCOUNT ON

-- Fetch the information from the current proposal information
SELECT	right( '000000000' + coalesce (p.ssn, ''), 9)	AS 'ssn',
	cc.client					AS 'client',
	case when ltrim(rtrim(isnull(dr.rpps_code,''))) = '' then 'ZB' else dr.rpps_code end as 'rpps_code'

FROM		rpps_transactions t	WITH (NOLOCK)
INNER JOIN	client_creditor cc	WITH (NOLOCK) ON t.client_creditor = cc.client_creditor
INNER JOIN	clients c		WITH (NOLOCK) ON t.client = c.client
LEFT OUTER JOIN people p		WITH (NOLOCK) ON cc.client = p.client AND dbo.DebtOwner(cc.client_creditor) = p.person
LEFT OUTER JOIN drop_reasons dr		WITH (NOLOCK) ON cc.drop_reason = dr.drop_reason
LEFT OUTER JOIN	debt_notes dn		WITH (NOLOCK) ON t.debt_note = dn.debt_note

WHERE					t.rpps_transaction = @rpps_transaction

-- Return success to the caller
RETURN ( 1 )
GO
