USE [DebtPlus]
GO
/****** Object:  StoredProcedure [dbo].[xpr_disbursement_schedpay_creditor]    Script Date: 09/15/2014 13:13:35 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[xpr_disbursement_schedpay_creditor] ( @client as int ) as

-- ====================================================================================
-- ==            Return the information for the schedpay utility's debt table        ==
-- ====================================================================================

-- ChangeLog
--   9/31/2002
--     Added support for client_creditor_balances table
--   01/15/2002
--     Added creditor_classes table references and dropped "creditors.fee_creditor"
--   06/09/2009
--      Remove references to debt_number and use client_creditor

set nocount on

select
	coalesce(cc.message,cc.account_number,'MISSING')	as 'account_number',
	isnull(cc.creditor,'')					as 'creditor',
	coalesce(cc.creditor_name, cr.creditor_name, '')	as 'creditor_name',
	isnull(bal.payments_month_0,0)				as 'current_month_disbursement',
	isnull(bal.orig_balance,0)				as 'orig_balance',
	isnull(cc.disbursement_factor,0)			as 'disbursement_factor',
	isnull(bal.orig_balance,0) + isnull(bal.orig_balance_adjustment,0) + isnull(bal.total_interest,0) - isnull(bal.total_payments,0) as 'current_balance',

	case
		when cc.dmp_interest is not null then cc.dmp_interest
		when isnull(bal.orig_balance,0) + isnull(bal.orig_balance_adjustment,0) + isnull(bal.total_interest,0) - isnull(bal.total_payments,0) < cr.medium_apr_amt then isnull(cr.lowest_apr_pct,0)
		when isnull(bal.orig_balance,0) + isnull(bal.orig_balance_adjustment,0) + isnull(bal.total_interest,0) - isnull(bal.total_payments,0) < cr.highest_apr_amt then isnull(cr.medium_apr_pct,0)
		else isnull(cr.highest_apr_pct,0)
	end							as 'apr',

	cc.client_creditor						as 'client_creditor',
	isnull(cc.line_number,0)				as 'line_number',
	isnull(cc.priority,9)					as 'priority',
	isnull(cc.sched_payment,0)				as 'sched_payment',
	isnull(cc.sched_payment,0)				as 'debit_amt',
	isnull(pct.creditor_type_eft,'N')			as 'creditor_type',
	
	case
		when cc.hold_disbursements = 1	then 1
		when c.start_date > getdate()	then 1
		when cc.start_date is null	then 0
		when cc.start_date > getdate()	then 1
		else 0
	end							as 'held',

	isnull(cc.orig_dmp_payment,0)				as 'orig_dmp_payment',

	isnull(ccl.zero_balance,0)				as 'zero_balance',
	isnull(ccl.prorate,1)					as 'prorate'

from		client_creditor cc with (nolock)
inner join	client_creditor_balances bal with (nolock) on cc.client_creditor_balance = bal.client_creditor_balance
inner join	clients c with (nolock) on cc.client = c.client
left outer join	creditors cr with (nolock) on cc.creditor = cr.creditor
LEFT OUTER JOIN creditor_classes ccl with (nolock) on cr.creditor_class = ccl.creditor_class
LEFT OUTER JOIN creditor_contribution_pcts pct with (nolock) on cr.creditor_contribution_pct = pct.creditor_contribution_pct
where		cc.client = @client
and		isnull(cc.reassigned_debt,0) = 0

return ( @@rowcount )
GO
