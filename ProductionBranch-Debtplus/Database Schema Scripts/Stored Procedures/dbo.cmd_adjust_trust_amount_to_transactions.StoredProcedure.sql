SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER procedure [cmd_adjust_trust_amount_to_transactions] ( @checknum as BigInt) as

-- Adjust the check dollar amount in the trust register to match the transaction data so that an old check may be voided.

declare @trust_register  int
declare @corrected_amount money
declare @original_amount  money
declare @tran_type        varchar(10)
declare @creditor         varchar(10)
declare @client           int

begin transaction
select @trust_register   = trust_register, @tran_type = tran_type, @creditor = creditor, @client = client From registers_trust where ltrim(rtrim(cleared)) = '' and checknum = @checknum and tran_type in ('AD','MD','CM','CR')

if @trust_register is not null
begin
	if @tran_type = 'CR'
		select @corrected_amount = debit_amt from registers_client where trust_register = @trust_register and tran_type = 'CR' and client = @client
	else
		select @corrected_amount = sum(case creditor_type when 'D' then debit_amt - fairshare_amt else debit_amt end) from registers_client_creditor where trust_register = @trust_register and tran_type = @tran_type and creditor = @creditor

	select @original_amount  = amount from registers_trust where trust_register = @trust_register
	if not (@corrected_amount is null)
		update registers_trust set amount = @corrected_amount where trust_register = @trust_register
end
commit

print 'check number = ' + convert(varchar,@checknum)
print 'trust register = ' + isnull(convert(varchar, @trust_register), 'NULL')
print 'original amount = $' + convert(varchar, @original_amount, 1)
print 'corrected amount = ' + isnull('$' + convert(varchar, @corrected_amount, 1), '**** UNDEFINED ****')

return ( 0 )
GO
