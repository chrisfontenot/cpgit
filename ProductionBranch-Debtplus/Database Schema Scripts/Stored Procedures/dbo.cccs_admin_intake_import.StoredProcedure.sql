USE [DebtPlus]
GO
/****** Object:  StoredProcedure [dbo].[cccs_admin_intake_import]    Script Date: 09/15/2014 13:13:31 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[cccs_admin_intake_import] as

-- ChangeLog
--    10/30/2007
--       Created a walkin appointment for the client
--    7/29/2008
--       Imported "method_preferred_contact"
--    3/26/2009
--       Removed main logic and created 'cccs_admin_intake_import_single' to allow re-importing "one-off" clients
--    4/26/2009
--       Return the count of items to the job step so that it may be logged there.

-- Queue ids for the clients
declare	@input_queue		int
declare	@output_queue		int

-- Input list of applicants to import and the queue for the result
select	@input_queue	= 3,
		@output_queue	= 14

-- Make things a bit quieter for now.
set nocount off

-- Reset the number of clients to load
declare	@applicant_count	int
select	@applicant_count	= 0
	
declare	@application_id		int

-- Process the list of clients
declare applicant_cursor cursor for
	select	application_id
	from	cccs_admin..applications_statuses
	where	queue_id = @input_queue
	order by application_id

open  applicant_cursor
fetch applicant_cursor into @application_id

while @@fetch_status = 0
begin

	-- Start a transaction to ensure that we don't load a partial client
	begin transaction
	set xact_abort on

	-- Import/Update the application
	declare	@client		int
	execute	@client = cccs_admin_intake_import_single @application_id
	if @client < 0
	begin
		rollback transaction
		break
	end

	-- Update the ForeignDatabaseID field with the client number
	if @client > 0
	begin
		update	cccs_admin..applications
		set		ForeignDatabaseID	= CONVERT(varchar, @client)
		where	application_id		= @application_id;

		-- Count this client
		select	@applicant_count	= @applicant_count + 1
	end

	-- Update the information for the new client
	update	cccs_admin..applications_statuses
	set		queue_id			= @output_queue,
			client_number		= @client,
			last_modified_date	= getdate()
	where	application_id		= @application_id
	and		queue_id			= @input_queue;

	-- Commit the transaciton to the database
	commit transaction

	-- Go on to the next item in the list	
	fetch applicant_cursor into @application_id
end
close      applicant_cursor
deallocate applicant_cursor

-- Restore the previous setting
set nocount on

return ( @applicant_count )
GO
