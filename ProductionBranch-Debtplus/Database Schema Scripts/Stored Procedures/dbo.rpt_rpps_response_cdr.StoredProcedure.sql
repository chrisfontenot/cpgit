USE [DebtPlus]
GO
/****** Object:  StoredProcedure [dbo].[rpt_rpps_response_cdr]    Script Date: 09/15/2014 13:13:33 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[rpt_rpps_response_cdr] ( @rpps_response_file as int = null ) AS
-- =======================================================================================================
-- ==           Obtain the information for the response conditions of rejected proposals                ==
-- =======================================================================================================

-- ChangeLog
--   2/18/2004
--     Added client's counselor name.
--   1/5/2009
--      Changed from using biller_aba to using rpps_biller_id

-- Suppress the count of records
set nocount on

-- Try to find the response file if one is not specified
if @rpps_response_file is null
begin
	select	@rpps_response_file = max(rpps_response_file)
	from	rpps_response_files
	where	date_created = (
		select	max(date_created)
		from	rpps_response_files
	)
end

-- Retrieve the information for the records
SELECT	cr.type											as 'type',
		cr.creditor_id									as 'creditor_id',
		d.account_number								as 'account_number',
		isnull(tr.client,cdr.client)					as 'client',
		coalesce('['+cr.creditor+'] ',d.rpps_biller_id+' ','')				as 'creditor',
		coalesce(cr.creditor_name, ix1.biller_name, 'UNKNOWN CREDITOR') as 'creditor_name',
		dbo.format_normal_name (pn.prefix, pn.first, pn.middle, pn.last, pn.suffix)		as 'client_name',
		d.rpps_biller_id								as 'biller_id',

		pr.proposed_amount								as 'proposed_amount',
		pr.missing_item									as 'missing_item',
		convert(datetime, convert(varchar, pr.proposed_start_date,101))	as 'proposed_start_date',
		pr.counter_amount								as 'counter_amount',

		coalesce(d.processing_error,rpse.description,pre.description,'UNKNOWN REASON')	as 'response_reason',

		d.trace_number									as 'trace_number',
		convert(varchar(80), dbo.format_normal_name(default,cox.first,default,cox.last,default)) as 'counselor_name',

		cdr.reject_reason								as 'reject_reason',
		cdr.resubmit_date								as 'resubmit_date',
		cdr.cycle_months_remaining						as 'cycle_months_remaining',
		cdr.forgiven_pct								as 'forgiven_pct',
		cdr.first_payment_date							as 'first_payment_date',
		cdr.good_thru_date								as 'good_thru_date',
		cdr.ineligible_reason							as 'ineligible_reason',
		cdr.internal_program_ends_date					as 'internal_program_ends_date',
		cdr.interest_rate								as 'interest_rate',
		cdr.third_party_detail							as 'third_party_detail',
		cdr.third_party_contact							as 'third_party_contact',

		-- This is small number of items. Just do them here so that we don't have to modifiy the report if the list changes or becomes large
		-- We force the type to be a typ_description just in case it must become a table.
		convert(varchar(50), case cdr.ineligible_reason
								when 'AD' then 'Account in dispute'
								when 'BR' then 'Business Card Account'
								when 'CD' then 'Cease & Desist'
								when 'CP' then 'Creditor Policy'
								when 'EM' then 'Exceeds max proposals'
								when 'FI' then 'Negative disposable income'
								when 'FR' then 'Fraud'
								when 'GA' then 'Account is government'
								when 'IL' then 'Account is international'
								when 'SS' then 'Soldiers/Sailors Relief Act'
								else NULL
							end)						as 'ineligible_reason_description',

		-- This is small number of items. Just do them here so that we don't have to modifiy the report if the list changes or becomes large
		-- We force the type to be a typ_description just in case it must become a table.
		convert(varchar(50), case cdr.third_party_detail
								when 'AL' then 'Acct In Legal'
								when 'AS' then 'Acct Sold'
								when 'CD' then 'Cease & Desist'
								else NULL
							end)						as 'third_party_detail_description'

-- These are the primary data for which we generate the report. They are required to exist.
from		rpps_response_details d				with (nolock)
inner join rpps_response_details_cdr cdr		with (nolock) on d.rpps_response_detail = cdr.rpps_response_detail

-- These are optional linkages. They may or may not exist.
left outer join	rpps_biller_ids ix1				with (nolock) on d.rpps_biller_id = ix1.rpps_biller_id
left outer join rpps_transactions tr			with (nolock) on d.rpps_transaction = tr.rpps_transaction
left outer join client_creditor_proposals pr	with (nolock) on tr.client_creditor_proposal = pr.client_creditor_proposal
left outer join client_creditor cc				with (nolock) on pr.client_creditor = cc.client_creditor
left outer join creditors cr					with (nolock) on cc.creditor = cr.creditor
left outer join rpps_reject_codes rpse			with (nolock) on cdr.reject_reason = rpse.rpps_reject_code
left outer join proposal_result_reasons pre		with (nolock) on pr.proposal_reject_reason = pre.proposal_result_reason
left outer join people p						with (nolock) on isnull(tr.client,cdr.client) = p.client and 1 = p.relation
left outer join names pn						with (nolock) on p.nameid = pn.name
left outer join clients c						with (nolock) on cc.client = c.client
left outer join counselors co					with (nolock) on c.counselor = co.counselor
left outer join names cox						with (nolock) on co.NameID = cox.name

where		d.rpps_response_file = @rpps_response_file
and			d.service_class_or_purpose in ('CDR','FBC','FBD')
order by	1, 2, 3 -- cr.type, cr.creditor_id, cc.account number

return ( @@rowcount )
GO
