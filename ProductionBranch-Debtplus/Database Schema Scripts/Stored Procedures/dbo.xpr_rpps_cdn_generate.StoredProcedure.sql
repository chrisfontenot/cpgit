USE [DebtPlus]
GO
/****** Object:  StoredProcedure [dbo].[xpr_rpps_cdn_generate]    Script Date: 09/15/2014 13:13:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[xpr_rpps_cdn_generate] ( @rpps_file as int ) as

-- ChangeLog
--   10/3/2008
--      Corrected bank and rpps biller id for CDN notes since they are not put into the table when the note is created.
--   06/09/2009
--      Remove references to debt_number and use client_creditor
--   6/1/2012
--      Set the death_date when creating the transaction

-- Suppress intermediate results
set nocount on

-- Find the bank from the rpps file
declare		@bank		int
select		@bank		= bank
from		rpps_files with (nolock)
where		rpps_file	= @rpps_file

-- Insert the messages
create table #debt_notes_2 (rpps_biller_id varchar(10) null, client int null, creditor varchar(10) null, client_creditor int null, debt_note int null, service_class_or_purpose varchar(3), transaction_code int, rpps_file int null, bank int null);

insert into #debt_notes_2 (rpps_biller_id, client, creditor, client_creditor, debt_note, service_class_or_purpose, transaction_code, rpps_file, bank)
select		m.rpps_biller_id, cc.client, cc.creditor, cc.client_creditor, dn.debt_note, 'CDN' as service_class_or_purpose, 23 as transaction_code, @rpps_file as rpps_file, @bank as bank
from		debt_notes dn with (nolock)
inner join	client_creditor cc with (nolock) on dn.client_creditor = cc.client_creditor
inner join	creditors cr with (nolock) on cc.creditor = cr.creditor
inner join	creditor_methods m with (nolock) on m.creditor = cr.creditor_id and m.type = 'MSG' and m.bank = @bank
inner join	banks b with (nolock) on m.bank = b.bank
where		dn.type	 = 'AN'
and		dn.output_batch	is null
and		b.type		= 'R';

insert into #debt_notes_2 (rpps_biller_id, client, creditor, client_creditor, debt_note, service_class_or_purpose, transaction_code, rpps_file, bank)
select		m.rpps_biller_id, cc.client, cc.creditor, cc.client_creditor, dn.debt_note, 'CDN' as service_class_or_purpose, 23 as transaction_code, @rpps_file as rpps_file, @bank as bank
from		debt_notes dn with (nolock)
inner join	client_creditor cc with (nolock) on dn.client_creditor = cc.client_creditor
inner join	creditors cr with (nolock) on cc.creditor = cr.creditor
inner join	creditor_methods m with (nolock) on m.creditor = cr.creditor_id and m.type = 'CLO' and m.bank = @bank
inner join	banks b with (nolock) on m.bank = b.bank
where		dn.type	 = 'MS'
and		dn.output_batch	is null
and		b.type		= 'R';

update		debt_notes
set		output_batch	= 'RPPS ' + convert(varchar, @rpps_file),
		rpps_biller_id	= x.rpps_biller_id,
		bank		= x.bank
from		debt_notes dn
inner join	#debt_notes_2 x on dn.debt_note = x.debt_note;

insert into	rpps_transactions (biller_id, client, creditor, client_creditor, debt_note, service_class_or_purpose, transaction_code, rpps_file, bank)
select		rpps_biller_id, client, creditor, client_creditor, debt_note, service_class_or_purpose, transaction_code, rpps_file, bank
from		#debt_notes_2;

drop table	#debt_notes_2;

-- Date when the transactions are to expire
declare		@death_date		datetime
select		@death_date	= dateadd(d, 180, getdate())

-- Insert the proposal messages
insert into	rpps_transactions (biller_id, client, creditor, client_creditor, debt_note, service_class_or_purpose, transaction_code, rpps_file, bank, client_creditor_proposal, death_date)
select		tr.biller_id, cc.client, cc.creditor, cc.client_creditor, dn.debt_note, 'CDN' as service_class_or_purpose, 23 as transaction_code, @rpps_file as rpps_file, @bank as bank, p.client_creditor_proposal, @death_date
from		rpps_transactions tr with (nolock)
inner join	client_creditor_proposals p with (nolock) on tr.client_creditor_proposal = p.client_creditor_proposal
inner join	debt_notes dn with (nolock) on p.client_creditor_proposal = dn.client_creditor_proposal and 'PR' = dn.type
inner join	client_creditor cc on p.client_creditor = cc.client_creditor
where		tr.rpps_file = @rpps_file
and		tr.service_class_or_purpose = 'CDP'
and		len(convert(varchar(1000),dn.note_text)) > 96;

return ( @@rowcount )
GO
