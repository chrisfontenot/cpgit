USE [DebtPlus]
GO
/****** Object:  StoredProcedure [dbo].[rpt_PrintClient_Deposits]    Script Date: 09/15/2014 13:13:33 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE PROCEDURE [dbo].[rpt_PrintClient_Deposits] ( @Client AS INT ) AS
-- ===================================================================================
-- ==               Fetch the client deposit information                            ==
-- ===================================================================================

SELECT	datepart(d, deposit_date)	as deposit_date,
	deposit_amount			as deposit_amount
FROM	client_deposits
WHERE	client = @Client
ORDER BY datepart(d, deposit_date)

RETURN ( @@rowcount )
GO
