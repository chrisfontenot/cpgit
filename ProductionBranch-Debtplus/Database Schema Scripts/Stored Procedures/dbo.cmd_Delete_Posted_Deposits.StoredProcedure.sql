USE [DebtPlus]
GO
/****** Object:  StoredProcedure [dbo].[cmd_Delete_Posted_Deposits]    Script Date: 09/15/2014 13:13:31 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE PROCEDURE [dbo].[cmd_Delete_Posted_Deposits] ( @deposit_batch_id as int = null ) as

-- Note Use this procedure if only one batch was posted and they just need to edit something in the batch. 

-- ==================================================================================================
-- ==            Delete a deposit batch once the item has been posted                              ==
-- ==================================================================================================

-- ChangeLog
--   6/1/2003
--     Changed test for open batches to only look at auto-disbursement batches. Others are not "posted".
	

if exists (select * from registers_disbursement where date_posted is null and tran_type = 'AD')
begin
	raiserror ('This procedure may not be executed with an open disbursement batch. Please post or delete any open disbursement batches.', 16, 1)
	return ( 0 )
end

-- Ensure that the batch ID is valid
if @deposit_batch_id is null
	set @deposit_batch_id = 0

if @deposit_batch_id <= 0
begin
	print 'You must enter the batch ID (not the name) for the batch that you wish to remove.'
	print 'The last 10 deposit batches are as follows:'

	select	deposit_batch_id				as 'batch',
		convert(varchar(10), date_created, 101) 	as 'date',
		created_by					as 'created by',
		note						as 'label name'
	from	deposit_batch_ids
	where	batch_type IN ('CL', 'AC')
	order by 2 desc, 1 desc;

end else begin

	declare	@batch_type		varchar(2)
	declare	@trust_register		int

	select	@batch_type		= batch_type,
		@trust_register		= trust_register
	from	deposit_batch_ids
	where	deposit_batch_id	= @deposit_batch_id
	and	batch_type		in ('CL','AC');

	if @trust_register is null
	begin
		print 'The deposit batch (a) does not exist, (b) is not posted, or (c) is not a deposit batch'
		print 'Sorry, but this batch is not valid.'

	end else begin

		-- Keep things consistent
		begin transaction

		-- Log the event information
		DECLARE	@message		varchar(800)
		SELECT	@message	= 'Deleting posted deposit batch #' + convert(varchar, @deposit_batch_id)

		-- Determine the total amount of the deposit for each client. Allow for multiple entries.
		select	client, sum(credit_amt) as credit_amt
		into	#t_deposits
		from	registers_client
		where	tran_type = 'DP'
		and	trust_register = @trust_register
		group by client;

		-- Adjust the client balances to remove the extra money
		update	clients
		set	held_in_trust = held_in_trust - t.credit_amt
		from	clients c
		inner join #t_deposits t on c.client = t.client

		-- Discard the working table
		drop table #t_deposits;

		-- Remove the client deposit records from the system
		delete
		from	registers_client
		where	tran_type = 'DP'
		and	trust_register = @trust_register

		-- Remove the trust register entry for the deposit from the system
		delete
		from	registers_trust
		where	trust_register = @trust_register

		if @batch_type = 'AC'
		begin
			select	@message = @message + ' ACH batch marked as un-posted'

			-- "un-post" ACH batches. Leave the transactions in place for later.
			update	deposit_batch_ids
			set	date_posted	= null,
				posted_by	= null,
				trust_register	= null
			where	deposit_batch_id = @deposit_batch_id;

		end else begin

			declare	@client_count	int
			declare	@client_amount	money

			select	@client_count	= count(*),
				@client_amount	= sum(credit_amt)
			from	registers_client with (nolock)
			where	trust_register	= @trust_register

			select	@message = @message +
					isnull(' with ' + convert(varchar, @client_count) + ' clients','') +
					isnull(' for $' + convert(varchar, @client_amount, 1), '')

			-- Remove the deposit detail records
			delete
			from	deposit_batch_details
			where	deposit_batch_id = @deposit_batch_id

			-- Remove the deposit batch ID from the system
			delete
			from	deposit_batch_ids
			where	deposit_batch_id = @deposit_batch_id
		end

		-- Log the message
		execute master..xp_logevent 60000, @message, informational

		-- Commit the transaction to the database once this is complete
		commit transaction
	end
end
GO
