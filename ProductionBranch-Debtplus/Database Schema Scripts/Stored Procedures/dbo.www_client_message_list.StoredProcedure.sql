USE [DebtPlus]
GO
/****** Object:  StoredProcedure [dbo].[www_client_message_list]    Script Date: 09/15/2014 13:13:34 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE PROCEDURE [dbo].[www_client_message_list] ( @client as int ) as
-- ===================================================================================
-- ==            Return the list of messages that are pending for the client        ==
-- ===================================================================================
declare	@pkid			uniqueidentifier
select	@pkid			= pkid
from	client_www
where	username		= convert(varchar,@client)
and		applicationname	= '/';

select	client_www_note		as 'item_key',
		message			as 'message',
		date_created		as 'date_created',
		created_by		as 'created_by',
		date_shown		as 'date_shown'
from	client_www_notes
where	pkid = @pkid
order by 3
return ( @@rowcount )
GO
