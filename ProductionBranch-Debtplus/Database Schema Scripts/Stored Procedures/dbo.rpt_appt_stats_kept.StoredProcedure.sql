USE [DebtPlus]
GO
/****** Object:  StoredProcedure [dbo].[rpt_appt_stats_kept]    Script Date: 09/15/2014 13:13:32 ******/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[rpt_appt_stats_kept] ( @FromDate AS DateTime = NULL, @ToDate AS DateTime = NULL ) AS

-- ====================================================================================================
-- ==            Return the number of kept appointments by counselor/office                          ==
-- ====================================================================================================

IF @ToDate IS NULL
	select @ToDate = getdate()

IF @FromDate IS NULL
	select @FromDate = @ToDate

-- Adjust the dates for the proper settings
select @FromDate	= convert(datetime, convert(varchar(10), @FromDate, 101) + ' 00:00:00')
select @ToDate	= convert(datetime, convert(varchar(10), @ToDate, 101)   + ' 23:59:59')

select	isnull(dbo.format_normal_name(default,cox.first,default,cox.last,default),'Not specified')		as 'counselor',
	isnull(o.name,'Not Specified')		as 'office',
	isnull(ap.appt_name,'Not Specified')	as 'type',
	count(*)				as 'count'

from	client_appointments ca	with (nolock)
left outer join counselors co	with (nolock) on ca.counselor = co.counselor
left outer join offices o	with (nolock) on ca.office = o.office
left outer join appt_types ap	with (nolock) on ca.appt_type = ap.appt_type
left outer join names cox with (nolock) on co.NameID = cox.name

where	ca.start_time between @FromDate and @ToDate
AND	ca.status IN ('K', 'W')
group by cox.first, cox.last, o.name, ap.appt_name
ORDER BY 1, 2, 3

RETURN ( @@rowcount )
GO
