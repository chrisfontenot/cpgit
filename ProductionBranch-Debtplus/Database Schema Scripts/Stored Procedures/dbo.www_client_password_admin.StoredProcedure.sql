USE [DebtPlus]
GO
/****** Object:  StoredProcedure [dbo].[www_client_password_admin]    Script Date: 09/15/2014 13:13:34 ******/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE PROCEDURE [dbo].[www_client_password_admin] ( @client as int, @new_password as varchar(80) = NULL ) AS
-- =========================================================================================================
-- ==            Update the password for the client                                                       ==
-- =========================================================================================================

-- Update the database value
update	client_www
set	[password]	= @new_password,
    LastPasswordChangeDate = getutcdate(),
    IsLockedOut = 0,
    IsApproved = 1
where	username		= convert(varchar,@client)

return ( @@rowcount )
GO
