USE [DebtPlus]
GO
/****** Object:  StoredProcedure [dbo].[lst_descriptions_CM]    Script Date: 09/15/2014 13:13:32 ******/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[lst_descriptions_CM] AS

-- ===================================================================================================
-- ==                Return the list of reasons for a manual disbursement                           ==
-- ===================================================================================================

-- ChangeLog
--   2/01/2002
--     Switch to the messages table

SELECT	item_value		as 'item_key',
	[description]		as 'description'
FROM	messages WITH ( NOLOCK )
WHERE	item_type = 'MD'

return ( @@rowcount )
GO
