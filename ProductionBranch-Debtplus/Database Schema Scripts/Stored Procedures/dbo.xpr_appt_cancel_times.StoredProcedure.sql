USE [DebtPlus]
GO
/****** Object:  StoredProcedure [dbo].[xpr_appt_cancel_times]    Script Date: 09/15/2014 13:13:34 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[xpr_appt_cancel_times] ( @Appt_time AS typ_key ) AS

-- ====================================================================================================
-- ==            Cancel the indicated appointment(s) for the indicated time slot                     ==
-- ====================================================================================================

-- Start a transaction to ensure that no one cancels this appointment twice
BEGIN TRANSACTION

-- Generate the cancel message that is written to the system note
insert into client_notes (client, type, is_text, dont_edit, dont_delete, subject, note)
select	ca.client, 3, 1, 1, 1, 'Client appointment was cancelled',
	'The appointment (#' + convert(varchar, ca.appt_time) + ') for ' +
	convert(varchar, isnull(ca.start_time, a.start_time), 100) + ' was cancelled.' + char(13) + char(10) +
	'It was a type ''' + ltrim(rtrim(isnull(t.appt_name, 'unknown'))) + '''.' + char(13) + char(10) +
	'The appointment was with ' + isnull(dbo.format_normal_name(default,con.first,default,con.last,default), 'an unspecified counselor') +
	isnull(' at the ' + ltrim(rtrim(o.name)) + ' office','') +
	'.'

from		client_appointments ca	with (nolock)
inner join	appt_times a		with (nolock) on ca.appt_time	= a.appt_time
left outer join	counselors c		with (nolock) on ca.counselor	= c.counselor
left outer join names con               with (nolock) on c.NameID       = con.name
left outer join offices o		with (nolock) on ca.office	= o.office
left outer join appt_types t		with (nolock) on ca.appt_type	= t.appt_type

where		ca.appt_time	= @appt_time
and		ca.status	= 'P'
and		ca.workshop is null

-- Mark the client appointment as cancelled
update		client_appointments
set		status		= 'C',
		appt_time	= null
where		appt_time	= @appt_time
and		status		= 'P'
and		workshop is null

-- Return success to the caller
commit transaction
RETURN ( @@rowcount )
GO
