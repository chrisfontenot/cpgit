IF EXISTS(SELECT * FROM SYSOBJECTS WHERE TYPE='P' AND NAME=N'rpt_Financial_Net_Worth')
	EXEC ('DROP PROCEDURE rpt_Financial_Net_Worth')
GO
CREATE PROCEDURE [dbo].[rpt_Financial_Net_Worth] ( @client as int, @language as int = 1 ) as

-- ===================================================================================================
-- ==       Return the information for the financial net worth sub-report                           ==
-- ===================================================================================================

-- ChangeLog
--   12/19/2008
--      Revisions for SQL2008

-- Create the left hand table for assets
create table #left_input ( secured_type_group int null, source int null, secured_type int null, asset_group int, line int identity(10,10), heading varchar(80), description varchar(80), value money, isHeading int, auto_home_other varchar(1) );

-- Load the secured properties of OTHER into that group
-- Include all types and those properties that match
insert into #left_input ( secured_type_group, source, secured_type, asset_group, heading, description, value, isHeading, auto_home_other)
select	t.secured_type_group, 3 as 'source', t.secured_type, 3, t.[grouping], t.[description], isnull(p.[current_value],0), 0, 'O'
from	secured_types t
left outer join secured_properties p on t.secured_type = p.secured_type and @client = p.client and 0 < p.current_value
where	t.auto_home_other = 'O'

-- Load the secured properties of AUTO into that group
insert into #left_input ( secured_type_group, source, secured_type, asset_group, heading, description, value, isHeading, auto_home_other )
select	t.secured_type_group, 2 as 'source', t.secured_type, 2, t.[grouping], t.[description], p.[current_value], 0, 'A'
from	secured_properties p
inner join secured_types t on p.secured_type = t.secured_type
where	t.auto_home_other = 'A'
and		p.client = @client
and		p.current_value > 0;

-- Move things to the asset group if possible
update  #left_input
set		asset_group = 2
where	asset_group = 3
and		heading in ('Cash', 'Investments', 'Personal Property', 'Retirement', 'Household Goods');

-- Move things to the real proprerty group if possible
update  #left_input
set		asset_group = 1
where	asset_group = 3
and		heading in ('Real Property');

-- Include the real properties from the housing section
insert into #left_input ( secured_type_group, source, secured_type, asset_group, heading, description, value, isHeading, auto_home_other )
select	null as secured_type_group, 1 as 'source', null as secured_type, 1, 'Real Property', m.description, sum(ISNULL(p.LandValue,0) + ISNULL(p.ImprovementsValue,0)), 0, 'H'
from	housing_properties p
inner join messages m on p.Residency = m.item_value and m.item_type = 'HOUSING RESIDENCY'
where	p.HousingID = @client
group by m.description
having	sum(ISNULL(p.LandValue,0) + ISNULL(p.ImprovementsValue,0)) > 0;

-- Translate the descriptions to spanish if needed
-- Change the descriptions to Spanish if needed
IF @language = 2
BEGIN
	-- Correct the heading descriptions
	update	#left_input
	set		heading = t.spanish_description
	from	#left_input i
	inner join secured_type_groups t on i.secured_type_group = t.oID

	-- Automobiles
	update	#left_input
	set		heading = 'Coches'
	where	source = 2

	-- Realestate
	update	#left_input
	set		heading = 'Bienes Raíces'
	where	source = 1

	-- Correct the descriptive values
	update	#left_input
	set		description = t.spanish_description
	from	#left_input i
	inner join secured_types t on i.secured_type = t.secured_type
	where	i.source in (2, 3)
END

-- Insert the heading lines into the list
insert into #left_input ( secured_type_group, source, secured_type, asset_group, heading, description, value, isHeading, auto_home_other )
select  null as secured_type_group, null as source, null as secured_type, asset_group, heading, heading, null, 1, auto_home_other
from	#left_input
where	isheading = 0
group by asset_group, heading, auto_home_other
order by 1

-- Generate the final table to emit the data in the line order properly
create table #left (seq_lineno int identity(1,1), l_auto_home_other varchar(1), l_description varchar(80), l_grouping varchar(80), l_value money, l_heading int);

insert into #left (l_auto_home_other, l_description, l_grouping, l_value, l_heading)
select	auto_home_other, description, heading, value, isHeading
from	#left_input
order by asset_group, heading, isHeading desc, description

-- --------------------- DEBTS -------------------------

-- Create the right hand table for assets
create table #right_input ( secured_type_group int null, source int null, secured_type int null, asset_group int, line int identity(10,10), heading varchar(80), description varchar(80), value money, isHeading int, auto_home_other varchar(1) );

-- Load the secured properties of AUTO into that group
insert into #right_input ( secured_type_group, source, secured_type, asset_group, heading, description, value, isHeading, auto_home_other )
select	t.secured_type_group, 1, t.secured_type, 2, t.[grouping], t.[description], sum(l.[balance]), 0, 'A'
from	secured_properties p
inner join secured_types t on p.secured_type = t.secured_type
inner join secured_loans l on p.secured_property = l.secured_property
where	t.auto_home_other = 'A'
and		p.client = @client
group by t.[secured_type_group], t.[secured_type], t.[grouping], t.[description], p.secured_property
having SUM(l.balance) > 0;

-- Load the secured properties of OTHER into that group
insert into #right_input ( secured_type_group, source, secured_type, asset_group, heading, description, value, isHeading, auto_home_other )
select	t.secured_type_group, 2, t.secured_type, 3, t.[grouping], t.[description], sum(l.[balance]), 0, 'O'
from	secured_properties p
inner join secured_types t on p.secured_type = t.secured_type
inner join secured_loans l on p.secured_property = l.secured_property
where	t.auto_home_other = 'O'
and		p.client = @client
group by t.[secured_type_group], t.[secured_type], t.[grouping], t.[description], p.secured_property
having SUM(l.balance) > 0;

-- Move things to the asset group if possible
update  #right_input
set		asset_group = 2
where	asset_group = 3
and		heading in ('Cash', 'Investments', 'Personal Property', 'Retirement', 'Household Goods');

-- Move things to the real proprerty group if possible
update  #right_input
set		asset_group = 1
where	asset_group = 3
and		heading in ('Real Property');

-- Include the real properties from the housing section
insert into #right_input ( secured_type_group, source, secured_type, asset_group, heading, description, value, isHeading, auto_home_other )
select	null as secured_type_group, 3 as source, null as secured_type, 1, 'Real Property', m.description, sum(l.CurrentLoanBalanceAmt), 0, 'H'
from	housing_properties p
inner join housing_loans l on p.oID = l.PropertyID
inner join messages m on p.Residency = m.item_value and m.item_type = 'HOUSING RESIDENCY'
where	p.HousingID = @client
group by m.description
having	sum(ISNULL(p.LandValue,0) + ISNULL(p.ImprovementsValue,0)) > 0
and		SUM(l.CurrentLoanBalanceAmt) > 0;

-- --------------- credit card debts ------------------------
-- Do this for the first 10 credit cards. Other fold into just one.

-- Generate a table for the current debts
create table #debts (creditor_name varchar(80), balance money, idseq int identity(1,1), corrected_idseq int);

insert into #debts (creditor_name, balance, corrected_idseq)
select	coalesce(cc.creditor_name, cr.creditor_name, 'Credit Card Debt') as creditor_name,
		b.orig_balance + b.orig_balance_adjustment + b.total_interest - b.total_payments as balance,
		0
from	client_creditor cc
inner join client_creditor_balances b on cc.client_creditor_balance = b.client_creditor_balance and b.client_creditor = cc.client_creditor
left outer join creditors cr on cc.creditor = cr.creditor
left outer join creditor_classes ccl on cr.creditor_class = ccl.creditor_class
where 	cc.reassigned_debt = 0
and		isnull(ccl.agency_account,0) = 0
and		b.orig_balance + b.orig_balance_adjustment + b.total_interest - b.total_payments > 0
and		cc.client	= @client;

-- Fold the other credit cards to a single value
update	#debts set corrected_idseq = idseq;
update	#debts set corrected_idseq = 11, creditor_name = 'OTHER CREDIT CARDS' where idseq > 10;

-- Add it to the right side table
insert into #right_input ( secured_type_group, source, secured_type, asset_group, heading, description, value, isHeading, auto_home_other )
select	null as secured_type_group, 4 as source, null as secured_type, 4, 'Credit Cards & Other Unsecured Debts', min(creditor_name), SUM(balance), 0, 'O'
from	#debts
group by corrected_idseq
having SUM(balance) > 0

-- -------------------- other debts ----------------------------

-- Load the other debt information
declare	@other_debt		money
select	@other_debt	= sum(balance)
from	client_other_debts with (nolock)
where	client		= @client;

if @other_debt > 0
	insert into #right_input (secured_type_group, source, secured_type, [asset_group], [description], [heading], [isHeading], [value])
	values (null, 5, null, 4, 'Miscellaneous Debts', 'Miscellaneous Debts', 0, @other_debt)

-- ------------------------------- taxes --------------------------------
declare	@fed_taxes		money
declare	@state_taxes	money
declare	@local_taxes	money

select	@fed_taxes		= fed_tax_owed,
		@state_taxes	= state_tax_owed,
		@local_taxes	= local_tax_owed
from	clients with (nolock)
where	client			= @client

if isnull(@fed_taxes,0) > 0
	insert into #right_input (secured_type_group, source, secured_type, [asset_group], [description], [heading], [isHeading], [value])
	values (null, 6, null, 4, 'Federal Taxes', 'Miscellaneous Debts', 0, @fed_taxes)

if isnull(@state_taxes,0) > 0
	insert into #right_input (secured_type_group, source, secured_type, [asset_group], [description], [heading], [isHeading], [value])
	values (null, 7, null, 4, 'State Taxes', 'Miscellaneous Debts', 0, @state_taxes)

if isnull(@local_taxes,0) > 0
	insert into #right_input (secured_type_group, source, secured_type, [asset_group], [description], [heading], [isHeading], [value])
	values (null, 8, null, 4, 'Local Taxes', 'Miscellaneous Debts', 0, @local_taxes)

-- Translate the descriptions to spanish if needed
if @language = 2
begin
	update		#right_input
	set			heading	= t.spanish_description
	from		#right_input i
	inner join	secured_type_groups t on i.secured_type_group = t.oid

	update		#right_input
	set			description = t.spanish_description
	from		#right_input i
	inner join	secured_types t on i.secured_type = t.secured_type

	update		#right_input
	set			heading = 'Bienes Raíces'
	where		source = 3

	-- These are not in a table
	update		#right_input set heading = 'SPANISH Credit Cards & Other Unsecured Debts'								where source = 4
	update		#right_input set heading = 'SPANISH Miscellaneous Debts', description = 'SPANISH Miscellaneous Debts'	where source = 5
	update		#right_input set heading = 'SPANISH Miscellaneous Debts', description = 'SPANISH Federal Taxes'			where source = 6
	update		#right_input set heading = 'SPANISH Miscellaneous Debts', description = 'SPANISH State Taxes'			where source = 7
	update		#right_input set heading = 'SPANISH Miscellaneous Debts', description = 'SPANISH Loca Taxes'			where source = 8
END
	
-- Insert the heading lines into the list
insert into #right_input ( secured_type_group, source, secured_type, asset_group, heading, description, value, isHeading, auto_home_other )
select null, 9, null, asset_group, heading, heading, null, 1, auto_home_other
from	#right_input
where	isheading = 0
group by asset_group, heading, auto_home_other
order by 1

-- Generate the final table to emit the data in the line order properly
create table #right (seq_lineno int identity(1,1), r_auto_home_other varchar(1), r_description varchar(80), r_grouping varchar(80), r_value money, r_heading int);

insert into #right (r_auto_home_other, r_description, r_grouping, r_value, r_heading)
select	auto_home_other, description, heading, value, isHeading
from	#right_input
order by asset_group, heading, isHeading desc, description

-- Return the combined (merged) tables, order by the line number
select	isnull(l.seq_lineno,r.seq_lineno) as seq_lineno,
		l.l_auto_home_other	as l_auto_home_other,
		l.l_description		as l_description,
		l.l_grouping		as l_grouping,
		l.l_value			as l_value,
		l.l_heading			as l_heading,

		r.r_auto_home_other	as r_auto_home_other,
		r.r_description		as r_description,
		r.r_grouping		as r_grouping,
		r.r_value			as r_value,
		r.r_heading			as r_heading,
	
		ISNULL(l.l_value,0) - ISNULL(r.r_value,0) as 'difference'

from	#left l
full outer join #right r on l.seq_lineno = r.seq_lineno
order by 1

drop table #debts
drop table #left
drop table #right
drop table #left_input
drop table #right_input

return ( 0 )
GO
GRANT EXECUTE ON rpt_Financial_Net_Worth TO public AS dbo;
GO
