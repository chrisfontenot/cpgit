IF NOT EXISTS(SELECT * FROM SYSOBJECTS WHERE TYPE='P' AND NAME=N'DAILY_client_appointments')
	EXEC('CREATE PROCEDURE [dbo].[DAILY_client_appointments] AS')
GO
ALTER PROCEDURE [dbo].[DAILY_client_appointments] AS
-- ======================================================================================
-- ==         If the client missed an appointment then flag it as missed               ==
-- ======================================================================================

-- ChageLog
--   11/15/2002
--      Added items to the retention event table when the client missed the appointment.

DECLARE @client			typ_client
DECLARE @appointment_time	DateTime
DECLARE @msg_text		varchar(256)
DECLARE @subject		typ_subject
DECLARE	@appt_type		int
DECLARE @client_appointment	typ_key
DECLARE	@retention_event	INT

BEGIN TRANSACTION
SET NOCOUNT ON

-- Fetch the default number of days until an appointment is considered "missed"
DECLARE @default_till_missed	INT
SELECT	@default_till_missed = isnull(default_till_missed,0)
FROM	config

-- Allocate a cursor for the client counselor appointment operation
DECLARE client_cursor CURSOR FOR
	SELECT		c.client_appointment, c.client, c.start_time, c.appt_type, isnull(apt.missed_retention_event,-1) as retention_event
	FROM		client_appointments c
	LEFT OUTER JOIN	appt_types apt on c.appt_type = apt.appt_type
	LEFT OUTER JOIN appt_times tm on c.appt_time = tm.appt_time
	LEFT OUTER JOIN	offices o ON c.office = o.office
	WHERE	c.status = 'P'
	and     ((tm.appt_time is null) or (c.workshop IS NULL AND dateadd(d, isnull(o.default_till_missed, @default_till_missed), tm.start_time) < getdate()))

OPEN client_cursor
FETCH FROM client_cursor INTO @client_appointment, @client, @appointment_time, @appt_type, @retention_event

SET @subject = 'Client missed appointment'

WHILE @@fetch_status = 0
BEGIN
	-- Format a system note that the client missed the appointment
	SET @msg_text = 'The client missed the appointment scheduled for ' + convert(varchar, @appointment_time, 100) 

	-- Insert the item into the client permanent notes
	INSERT INTO client_notes (client,note,subject,type,is_text,dont_edit) VALUES (@client,@msg_text,@subject,1,1,1)

	-- Mark the appointment as missed
	UPDATE	client_appointments
	SET	status			= 'M',
		appt_time		= NULL
	WHERE	client_appointment	= @client_appointment

	-- Add the followup event to the system
	if @retention_event > 0
		insert into client_retention_events (client,expire_type,priority,message,retention_event)
		select	@client, isnull(expire_type,1), isnull(priority,9), @msg_text, retention_event
		from	retention_events
		where	retention_event = @retention_event

	-- Go on to the next item
	FETCH FROM client_cursor INTO @client_appointment,@client,@appointment_time,@appt_type, @retention_event
END

CLOSE client_cursor
DEALLOCATE client_cursor
COMMIT TRANSACTION

RETURN ( @@rowcount )
GO
