USE [DebtPlus]
GO
/****** Object:  StoredProcedure [dbo].[rpt_PrintChecks_Address]    Script Date: 09/15/2014 13:13:33 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE PROCEDURE [dbo].[rpt_PrintChecks_Address] ( @trust_register AS INT ) AS
-- ====================================================================================
-- ==               Retrieve the address information for the check                   ==
-- ====================================================================================

-- ChageLog
--   2/15/2003
--     Rewrote to use the "new" format for creditor addresses

SET NOCOUNT ON

DECLARE	@client		typ_client
DECLARE	@creditor	typ_creditor

SELECT	@client		= client,
		@creditor	= creditor
FROM	registers_trust WITH (NOLOCK)
WHERE	trust_register	= @trust_register

declare	@attn		varchar(80)
DECLARE @addr1		varchar(256)
DECLARE @addr2		varchar(256)
DECLARE @addr3		varchar(256)
DECLARE @addr4		varchar(256)
DECLARE	@addr5		varchar(256)
DECLARE @addr6		varchar(256)
declare	@postalcode	varchar(80)

-- Use the creditor address if this is a creditor check
IF @creditor IS NOT NULL
	select	@attn		= 'ATTN: ' + attn,
			@addr1		= addr1,
			@addr2		= addr2,
			@addr3		= addr3,
			@addr4		= addr4,
			@addr5		= addr5,
			@addr6		= addr6,
			@postalcode	= postalcode
	from	view_creditor_addresses ca
	where	creditor	= @creditor
	and		type		= 'P'
	
else if @client is not null

	select	@attn		= null,
			@addr1		= name,
			@addr3		= addr1,
			@addr4		= addr2,
			@addr5		= null,
			@addr6		= addr3,
			@postalcode	= zipcode
	FROM		view_client_address
	WHERE		client = @client

-- Return the address information
select	upper(@attn)		as 'attn',
		upper(@addr1)		as 'addr_1',
		upper(@addr2)		as 'addr_2',
		upper(@addr3)		as 'addr_3',
		upper(@addr4)		as 'addr_4',
		upper(@addr6)		as 'addr_5',
		@postalcode			as 'zipcode',
		@trust_register		as 'trust_register'

-- Return the number of rows in the result set
RETURN ( @@rowcount )
GO
