USE [DebtPlus]
GO
/****** Object:  StoredProcedure [dbo].[cmd_Remove_Posted_RF_Batch]    Script Date: 09/15/2014 13:13:31 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE PROCEDURE [dbo].[cmd_Remove_Posted_RF_Batch] ( @deposit_batch_id as int = null ) as

-- ==========================================================================================================
-- ==            Remove the effects of posting a Creditor Refund batch by mistake (or one with errors)     ==
-- ==========================================================================================================

-- ChangeLog
--   9/31/2002
--     Added support for client_creditor_balances table

-- Disable the count of items
set nocount on

if @deposit_batch_id is null
begin
	Print 'The deposit batch must be specified. Here is a list of the last 10 batches'

	select top 10 deposit_batch_id			as 'batch_id',
		convert(varchar(10), date_created, 101)	as 'Created',
		left(created_by,30)			as 'Creator',
		left(isnull(note,''),30)		as 'Label'
	from	deposit_batch_ids
	where	batch_type		= 'CR'
	and	date_posted		is not null
	order by date_created desc;

	return ( 0 )
end

-- Start a transaction to ensure that all works or all fails correctly
begin transaction

declare	@trust_register		int
declare	@posted_date		datetime
declare	@batch_type		varchar(2)
select	@trust_register 	= trust_register,
	@batch_type		= batch_type,
	@posted_date		= date_posted
from	deposit_batch_ids
where	deposit_batch_id	= @deposit_batch_id

-- This must be a valid batch ID
if @@rowcount != 1
begin
	rollback transaction
	RaisError ('The trust register could not be determined from the deposit batch # %d', 16, 1, @deposit_batch_id)
	return ( 0 )
end

-- This must be a creditor refund batch
if @batch_type != 'CR'
begin
	rollback transaction
	RaisError ('The batch is not a creditor refund batch. Deposit batches are not removed with this command', 16, 1)
	return ( 0 )
end

-- The batch must be posted
if @posted_date is null
begin
	rollback transaction
	RaisError ('The batch was not posted and can be edited normally', 16, 1)
	return ( 0 )
end

print 'Date batch was posted = ' + convert(varchar(10), @posted_date, 101)
print 'Trust register = ' + convert(varchar, @trust_register)

-- Accumulate the amount of the fairshare
declare	@fairshare_amt		money
select	@fairshare_amt = sum(fairshare_amt)
from	registers_client_creditor
where	tran_type = 'RF'
and	trust_register = @trust_register

-- Calculate the refunds in the client register by client
select	client, sum(credit_amt) as credit_amt
into	#client_trust_amounts
from	registers_client
where	tran_type 	= 'RF'
and	trust_register	= @trust_register
group by client;

-- Calculate the refunds in the client_creditor register by client
select	client, sum(credit_amt) as credit_amt
into	#debt_trust_amounts
from	registers_client_creditor
where	tran_type	= 'RF'
and	trust_register	= @trust_register
group by client;

-- Determine the amount from the client refunds
declare	@total_refund_amount	money
select	@total_refund_amount = sum(credit_amt)
from	#client_trust_amounts;

-- Determine the amount from the trust register
declare	@deposit_total		money
select	@deposit_total = amount
from	registers_trust
where	trust_register	= @trust_register

if exists (
	select *
	from	#client_trust_amounts a
	full outer join #debt_trust_amounts b on a.client = b.client
	where	isnull(a.credit_amt,0) != isnull(b.credit_amt,0)
)
begin
	rollback transaction
	RaisError ('The debt refund amounts do not agree with the client refund amounts. This batch may not be recalled.', 16, 1)
	return ( 0 )
end

-- Ensure that the deposit amount is correct
if isnull(@deposit_total,0) != (@total_refund_amount - @fairshare_amt)
begin
	rollback transaction
	RaisError ('The trust register deposit amount differs from the calculated value', 16, 1)
	return ( 0 )
end

print 'Total deposit amount    = ' + right('                    $' + convert(varchar, convert(decimal(10,2), @deposit_total), 1), 20)
print 'Total fairshare amount  = ' + right('                    $' + convert(varchar, convert(decimal(10,2), @fairshare_amt), 1), 20)
print 'Total amount of refunds = ' + right('                    $' + convert(varchar, convert(decimal(10,2), @total_refund_amount), 1), 20)

-- Remove the money from the clients
update	clients
set	held_in_trust = isnull(c.held_in_trust,0) - a.credit_amt
from	clients c
inner join #client_trust_amounts a on c.client = a.client;

-- Correct the debt transactions
update	client_creditor_balances
set	total_payments		= isnull(bal.total_payments,0) + isnull(rcc.credit_amt,0),
	payments_month_0	= isnull(bal.payments_month_0,0) + isnull(rcc.credit_amt,0)
from	client_creditor_balances bal
inner join client_creditor cc on bal.client_creditor = cc.client_creditor
inner join registers_client_creditor rcc on cc.client_creditor = rcc.client_creditor
where	rcc.tran_type		= 'RF'
and	rcc.trust_register	= @trust_register

-- Credit the fairshare account balance
update	clients
set	held_in_trust	= isnull(held_in_trust,0) + isnull(@fairshare_amt,0)
where	client		= 0;

-- Remove the client transactions
delete
from	registers_client
where	tran_type	= 'RF'
and	trust_register	= @trust_register;

-- Remove the client_creditor transactions
delete
from	registers_client_creditor
where	tran_type	= 'RF'
and	trust_register	= @trust_register;

-- Remove the creditor transactions
delete
from	registers_creditor
where	tran_type	= 'RF'
and	trust_register	= @trust_register;

-- Remove the trust reigster item for the transaction
delete
from	registers_trust
where	trust_register		= @trust_register;

-- Remove the deposit batch details
delete
from	deposit_batch_details
where	deposit_batch_id	= @deposit_batch_id;

-- Remove the deposit batch information
delete
from	deposit_batch_ids
where	deposit_batch_id	= @deposit_batch_id;

drop table #client_trust_amounts;
drop table #debt_trust_amounts;

-- Commit the change and complete the processing successfully
commit transaction
return ( 1 )
GO
