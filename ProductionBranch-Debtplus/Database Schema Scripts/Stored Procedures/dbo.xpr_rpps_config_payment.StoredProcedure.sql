USE [DebtPlus]
GO
/****** Object:  StoredProcedure [dbo].[xpr_rpps_config_payment]    Script Date: 09/15/2014 13:13:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[xpr_rpps_config_payment] ( @bank as int ) as
-- ====================================================================================================
-- ==            Retrieve the configuration information for RPPS payments                            ==
-- ====================================================================================================

-- ChangeLog
--   1/31/2003
--     Changed to use the banks table rather than 'config'.

-- Suppress intermediate result sets
SET NOCOUNT ON

-- There must be a bank account number at this point
if isnull(@bank,0) <= 0
begin
	RaisError ('Bank account # is not configured for RPPS', 16, 1)
	return ( 0 )
end

-- The bank must be a valid item
if not exists (select * from banks where type = 'R' and bank = @bank)
begin
	RaisError ('Bank account # %d is not configured for RPPS', 16, 1, @bank)
	return ( 0 )
end

-- Fetch the information for RPS from the configuration table

SELECT	immediate_origin	as 'rps_id',
	immediate_origin_name	as 'rps_origin_name',
	prefix_line		as 'rps_prefix',
	suffix_line		as 'rps_suffix',
	batch_number		as 'batch_id',
	transaction_number	as 'transaction_id',
	bank			as 'bank'

from	banks
where	bank			= @bank

RETURN ( @@rowcount )
GO
