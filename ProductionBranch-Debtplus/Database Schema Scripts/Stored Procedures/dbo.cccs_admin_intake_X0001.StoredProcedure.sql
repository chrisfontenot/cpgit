USE [DebtPlus]
GO
/****** Object:  StoredProcedure [dbo].[cccs_admin_intake_X0001]    Script Date: 09/15/2014 13:13:31 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[cccs_admin_intake_X0001] ( @application_id int, @client int ) as

set nocount on

declare	@config_fee					int
declare	@fee_sched_payment			money
declare	@fee_type					int
declare	@priority					int
declare	@account_number				varchar(50)
declare	@client_creditor			int
declare	@creditor					varchar(10)
declare	@monthly_contribution		money

-- Find the monthly fee creditor
select	@creditor	= paf_creditor
from	config with (nolock)

select	@config_fee	= min(config_fee)
from	config_fees with (nolock)
where	[default] = 1 and [ActiveFlag] = 1

if @config_fee is null
	select	@config_fee = min(config_fee)
	from	config_fees with (nolock)
	where	[default] = 1
	
if @config_fee is null
	select	@config_fee = min(config_fee)
	from	config_fees with (nolock)
	where	[ActiveFlag] = 1
	
if @config_fee is null
	select	@config_fee = min(config_fee)
	from	config_fees with (nolock)
	
-- Correct the client information
if @config_fee is not null
begin

	-- Correct the fee scheduled payment amount
	select	@fee_sched_payment	= fee_sched_payment
	from	config_fees with (nolock)
	where	config_fee		= @config_fee

	-- Update the client with the fee information
	update	clients
	set		config_fee		= isnull(@config_fee,0)
	where	[client]		= @client
end

-- Replace the information for the disbursement amount from the budgets table if possible
select	@monthly_contribution	= monthly_contribution
from	cccs_admin..budgets with (nolock)
where	application_id  = @application_id

-- Find the debt to be updated
select @client_creditor	= client_creditor
from	client_creditor with (nolock)
where	client			= @client
and		creditor		= @creditor
	
-- If there is no debt then add one.
if @client_creditor is null
begin
	select  @account_number = dbo.format_client_id(@client)
	execute @client_creditor = xpr_create_debt @client, @creditor, null, @account_number
end

-- This should NEVER happen. Catch a catastrophic failure here.
if @client_creditor is null
begin
	Raiserror ('X0001 debt can not be located', 16, 1)
	return ( 0 )
end

-- Correct the debt record
update	client_creditor
set		disbursement_factor	= coalesce(@monthly_contribution, @fee_sched_payment, 0),
		sched_payment       = coalesce(@monthly_contribution, @fee_sched_payment, 0)
where	client_creditor		= @client_creditor;

return ( @client_creditor )
GO
