USE [DebtPlus]
GO
/****** Object:  StoredProcedure [dbo].[xpr_insert_ClientAppointments]    Script Date: 09/15/2014 13:13:35 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[xpr_insert_ClientAppointments](@oID as uniqueidentifier, @Client as int = null,@StartDate as datetime = null,@EndDate as datetime = null,@Office as int = null,@Counselor as int = null,@ReminderInfo as text = null,@RecurrenceInfo as text = null,@Description as text = null,@AppointmentType as int = 0,@CallbackTelephone as varchar(80) = null,@isAllDay as bit = 0,@isLocked as bit = 0,@isPriority as bit = 0,@RecurringType as int = 0,@ConfirmStatus as int = 0,@ConfirmedBy as varchar(80) = null,@ConfirmedDate as datetime = null,@ReferredBy as int = 0,@BusyStatus as int = 0,@Subject as varchar(80) = null,@Location as varchar(80) = null) as
-- ========================================================================================================
-- ==           Create an entry in the ClientAppointments table                                          ==
-- ========================================================================================================

-- Create the ClientAppointment entry
INSERT INTO ClientAppointments([oID],[Client],[StartDate],[EndDate],[Office],[Counselor],[ReminderInfo],[RecurrenceInfo],[Description],[AppointmentType],[CallbackTelephone],[isAllDay],[isLocked],[isPriority],[RecurringType],[ConfirmStatus],[ConfirmedBy],[ConfirmedDate],[ReferredBy],[BusyStatus],[Subject],[Location]) values (@oID,@Client,@StartDate,@EndDate,@Office,@Counselor,@ReminderInfo,@RecurrenceInfo,@Description,@AppointmentType,isnull(@CallbackTelephone,''),isnull(@isAllDay,0),isnull(@isLocked,0),isnull(@isPriority,0),@RecurringType,@ConfirmStatus,@ConfirmedBy,@ConfirmedDate,@ReferredBy,@BusyStatus,@Subject,@Location)

-- If there is a client then create a history record
IF @Client is not null
	INSERT INTO ClientAppointmentsHistory([ClientAppointment], [AppointmentType], [Client], [ConfirmStatus], [Counselor], [Duration], [isLocked], [isPriority], [isWalkin], [isWorkshop], [Office], [ReferredBy], [StartDate], [TranType])
	SELECT	[oID], [AppointmentType], [Client], [ConfirmStatus], [Counselor], datediff(minute, [StartDate], [EndDate]), [isLocked], [isPriority], 0, 0, [Office], [ReferredBy], [StartDate], 'PE'
	FROM	ClientAppointments WITH (NOLOCK)
	WHERE	[oID] = @oID;

RETURN ( @@ROWCOUNT )
GO
