USE [DebtPlus]
GO
/****** Object:  StoredProcedure [dbo].[rpt_unpaid_clients_2]    Script Date: 09/15/2014 13:13:34 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[rpt_unpaid_clients_2] ( @counselor as int = null ) AS

-- ====================================================================================================================
-- ==            2nd version of the unpaid client listing                                                            ==
-- ====================================================================================================================

-- ChangeLog
--   1/19/2001
--      Changed to reflect the new values for the last payment date and amount fields in the client_creditor table.
--   12/3/2003
--      Added counselor to the selection information

-- Suppress intermediate results
set nocount on

declare	@first_date	Datetime
declare	@month_1	Datetime
declare	@month_2	Datetime
declare	@month_3	Datetime
declare	@month_4	Datetime

-- Deterine the cutoff dates
set	@first_date	= convert(datetime, convert(varchar,datepart(year,getdate())) + '-' + convert(varchar,datepart(month,getdate())) + '-01 00:00:00')
set	@month_1	= dateadd(month, -1, @first_date)
set	@month_2	= dateadd(month, -1, @month_1)
set	@month_3	= dateadd(month, -1, @month_2)
set	@month_4	= dateadd(month, -1, @month_3)

-- Generate a list of the active clients
if @counselor is null
	select		c.client			as 'client',
			c.active_status			as 'active_status',

			case	when c.last_deposit_date is null	then 0
				when c.last_deposit_date >= @first_date	then 1
				when c.last_deposit_date >= @month_1	then 2
				when c.last_deposit_date >= @month_2	then 3
				when c.last_deposit_date >= @month_3	then 4
									else 5
			end				as 'group',

			c.last_deposit_date		as 'last_deposit_date',
			c.last_deposit_amount		as 'last_deposit_amount',
			(select max(rcc.date_created) from client_creditor cc with (nolock) inner join registers_client_creditor rcc on cc.last_payment = rcc.client_creditor_register WHERE c.client = cc.client AND cc.reassigned_debt = 0) as 'last_disb_date',
			dbo.format_normal_name(pn.prefix,pn.first,pn.middle,pn.last,pn.suffix) as 'name',
			dbo.format_normal_name(default,con.first,default,con.last,default) as 'counselor_name'
	from		clients c with (nolock)
	left outer join	people p with (nolock) on c.client = p.client AND 1 = p.relation
	left outer join names pn with (nolock) on p.nameid = pn.name
	left outer join counselors co with (nolock) on c.counselor = co.counselor
	left outer join names con with (nolock) on co.NameID = con.name
	where		c.active_status in ('A', 'AR')
	order by 3, 1

else

	select		c.client			as 'client',
			c.active_status			as 'active_status',

			case	when c.last_deposit_date is null	then 0
				when c.last_deposit_date >= @first_date	then 1
				when c.last_deposit_date >= @month_1	then 2
				when c.last_deposit_date >= @month_2	then 3
				when c.last_deposit_date >= @month_3	then 4
									else 5
			end				as 'group',

			c.last_deposit_date		as 'last_deposit_date',
			c.last_deposit_amount		as 'last_deposit_amount',
			(select max(rcc.date_created) from client_creditor cc with (nolock) inner join registers_client_creditor rcc on cc.last_payment = rcc.client_creditor_register WHERE c.client = cc.client AND cc.reassigned_debt = 0) as 'last_disb_date',
			dbo.format_normal_name(pn.prefix,pn.first,pn.middle,pn.last,pn.suffix) as 'name',
			dbo.format_normal_name(default,con.first,default,con.last,default) as 'counselor_name'
	from		clients c with (nolock)
	left outer join	people p with (nolock) on c.client = p.client AND 1 = p.relation
	left outer join counselors co with (nolock) on c.counselor = co.counselor
	left outer join names con with (nolock) on co.NameID = con.name
	left outer join names pn with (nolock) on p.nameid = pn.name
	where		c.active_status in ('A', 'AR')
	and		c.counselor = @counselor
	order by 3, 1

return ( @@rowcount )
GO
