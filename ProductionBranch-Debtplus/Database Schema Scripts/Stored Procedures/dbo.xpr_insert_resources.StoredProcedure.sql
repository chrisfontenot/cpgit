USE [DebtPlus]
GO
/****** Object:  StoredProcedure [dbo].[xpr_insert_resources]    Script Date: 09/15/2014 13:13:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[xpr_insert_resources] ( @resource_type as int, @Name as varchar(80), @AddressID as int = null, @TelephoneID as int = null, @FAXID as int = null, @EmailID as int = null, @WWW as varchar(80) = null ) as
-- ========================================================================================
-- ==         Create a resource reference                                                ==
-- ========================================================================================
insert into resources ( [resource_type], [name], [AddressID], [TelephoneID], [FAXID], [EmailID], [www]) values ( @resource_type, @name, @AddressID, @TelephoneID, @FAXID, @EmailID, @www)
return ( scope_identity() )
GO
