SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER PROCEDURE [dbo].[rpt_UnReconciled_Bank_Items] ( @ToDate as DateTime = null ) as

-- ============================================================================================
-- ==            List the unreconciled checks prior to the cutoff date.                      ==
-- ============================================================================================

-- ChangeLog
--   5/12/2003
--     Added 'R' to the 'V' status as being pending before the cutoff date
--   7/11/2003
--     Display unreconciled deposits 
--   8/5/2003
--     Display all items

set nocount on

if @Todate is null
	select	@ToDate = getdate()

select	@ToDate = convert(datetime, convert(varchar(10), @ToDate, 101) + ' 23:59:59')

-- Return the items in the trust register.
-- We want all unreconciled items prior to the cutoff date, *OR*
-- We want all voided items which are voided after the cutoff date

select	checknum			as 'checknum',
	date_created			as 'date',
	credit_amt			as 'credit',
	debit_amt			as 'debit',

	case
		when cleared = 'V' then isnull(payee+' ','')+ '(VOID)'
		else payee
	end				as 'payee',

	case
		when cleared in (' ','P','D') then cleared
		when reconciled_date is null then cleared
		when reconciled_date >= @ToDate then ' '
		else cleared
	end				as 'cleared'

FROM	view_trust_register with (nolock)
where	((cleared = ' ') or (cleared in ('R','V') and reconciled_date >= @ToDate))
and	tran_type in ('AD', 'CM', 'CR', 'MD', 'BW')
and	date_created < @ToDate
order by date_created, checknum

return ( @@rowcount )
GO
