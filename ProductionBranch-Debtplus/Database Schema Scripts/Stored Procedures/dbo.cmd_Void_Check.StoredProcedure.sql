SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER procedure [cmd_Void_Check] ( @checknum as bigint, @message AS typ_message = NULL ) AS

-- ================================================================================================================
-- ==                   Void a check by the check number                                                         ==
-- ================================================================================================================

-- Start a transaction for the requests
SET NOCOUNT ON

-- Find the trust register for the check to be voided
DECLARE	@trust_register	INT
DECLARE	@creditor	typ_creditor
DECLARE	@amount		Money
DECLARE	@check_date	DateTime
DECLARE	@cleared	char(1)
DECLARE	@client		typ_client

SELECT	@trust_register		= trust_register,
	@Creditor		= creditor,
	@client			= client,
	@amount			= amount,
	@check_date		= date_created,
	@cleared		= isnull(cleared,' ')
FROM	registers_trust WITH (NOLOCK)
WHERE	checknum		= @checknum
AND	((creditor IS NOT NULL) OR (client IS NOT NULL))
AND	isnull(cleared,' ')	!= 'D'

-- There should be a trust register
IF @trust_register IS NULL
BEGIN
	RaisError (50001, 16, 1, @checknum)
	RETURN ( 0 )
END

-- The check should be in the proper state
IF @cleared = 'R'
	RaisError (50002, 16, 1, @checknum)
ELSE IF @cleared = 'C'
	RaisError (50003, 16, 1, @checknum)
ELSE IF @cleared = 'V'
	RaisError (50004, 16, 1, @checknum)

IF @cleared not in (' ', 'P')
	Return ( 0 )

-- Do the general void processing
DECLARE	@local_error	INT
EXECUTE @local_error = xpr_Void_Check @trust_register, @message

-- Tell the user that we did the void operation
if @local_error > 0
BEGIN
	declare	@fmt_client	varchar(80)
	select	@fmt_client = dbo.format_client_id ( @client )

	IF @creditor is not null
		print 'Voided check ' + convert(varchar,@checknum) + ' to creditor ' + @creditor + ' in the amount of $' + convert(varchar,@amount,1) + ' on ' + convert(varchar(10), @check_date, 101)
	else
		print 'Voided check ' + convert(varchar,@checknum) + ' to client ' + @fmt_client + ' in the amount of $' + convert(varchar,@amount,1) + ' on ' + convert(varchar(10), @check_date, 101)
END

RETURN ( @local_error )
GO
