USE [DebtPlus]
GO
/****** Object:  StoredProcedure [dbo].[rpt_Client_Statistics_By_Workshop]    Script Date: 09/15/2014 13:13:32 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[rpt_Client_Statistics_By_Workshop] (@FromDate as datetime = null, @ToDate as datetime = null) as

-- =====================================================================================================
-- ==            Report showing the statistics for the workshop attendance                            ==
-- =====================================================================================================

-- Default the parameters
set nocount on

if @ToDate is null
	select	@ToDate = getdate()

if @FromDate is null
	select	@FromDate = @ToDate

-- Remove the times from the input date parameters
select	@FromDate	= convert(datetime, convert(varchar(10), @FromDate, 101) + ' 00:00:00'),
		@ToDate		= dateadd(d, 1, convert(datetime, convert(varchar(10), @ToDate,   101) + ' 00:00:00'))

-- Find the monthly income for the clients
select	p.client, sum(p.gross_income) as income
into	#incomes
from	client_appointments ca
inner join workshops w on w.workshop = ca.workshop
left outer join people p on ca.client = p.client
where	ca.status in ('K', 'W')
and	w.start_time >= @FromDate
and w.start_time < @ToDate
group by p.client;

-- Find the monthly assets for the clients in the list
select	client, sum(asset_amount) as asset_amount
into	#assets
from	assets a
where	client in (select client from #incomes)
group by client;

-- Update the income figure with the monthly asset amounts
update	#incomes
set	income	= income + asset_amount
from	#incomes i
inner join #assets a on i.client = a.client;

-- Find the list of workshops in the date range
select	t.description			as 'workshop',
	w.start_time			as 'start_time',
	c.client			as 'client',
	dbo.format_normal_name(pn.prefix,pn.first,pn.middle,pn.last,pn.suffix) as name,
	isnull(gender.description,'Unspecified')	as gender,
	isnull(race.description,'Unspecified')		as race,
	datediff(yy, p.birthdate, getdate())		as age,

	case isnull(p.relation,0)
		when 1 then i.income
		else 0
	end				as income,

	case isnull(p.relation,0)
		when 1 then c.dependents + (case c.marital_status when 2 then 2 else 1 end)
		else 0
	end				as household

from	clients c
inner join #incomes i on c.client = i.client
inner join client_appointments ca on c.client = ca.client and ca.status in ('K', 'W')
inner join workshops w on ca.workshop = w.workshop and w.start_time between @FromDate and @ToDate
left outer join workshop_types t on w.workshop_type = t.workshop_type
left outer join people p on i.client = p.client
left outer join names pn with (nolock) on p.nameid = pn.name
left outer join GenderTypes gender on p.gender = gender.oID
left outer join RaceTypes race on p.race = race.oID

order by 1, 2, 3 -- workshop, start time, client

-- Discard the working tables
drop table #incomes
drop table #assets

return ( @@rowcount )
GO
