USE [DebtPlus]
GO
/****** Object:  StoredProcedure [dbo].[rpt_dropped_clients]    Script Date: 09/15/2014 13:13:33 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[rpt_dropped_clients] ( @from_date as datetime = null, @to_date as datetime = null, @office as int = null ) as

-- Do some configuration first
set nocount on

if @to_date is null
	select	@to_date = getdate()

if @from_date is null
	select	@from_date = @to_date

select	@from_date = convert(datetime, convert(varchar(10), @from_date, 101) + ' 00:00:00'),
	@to_date = convert(datetime, convert(varchar(10), @to_date, 101) + ' 23:59:59')

declare	@paf_creditor	varchar(10)
select	@paf_creditor	= paf_creditor
from	config with (nolock)

-- If there is no office then don't select by the office
if @office is null
	select	c.client,
		dbo.format_reverse_name ( pn.prefix, pn.first, pn.middle, pn.last, pn.suffix ) as client_name,
		c.drop_date,
		isnull(c.drop_reason_other, dr.description) as drop_reason,
		c.program_months,
		dbo.format_counselor_name ( co.person ) as counselor,
		sum(b.orig_balance) as original_balance,
		sum(b.total_payments) as paid_to_date

	from	clients c with (nolock)
	left outer join client_creditor cc on c.client = cc.client
	left outer join client_creditor_balances b on cc.client_creditor_balance = b.client_creditor_balance and b.client_creditor = cc.client_creditor
	left outer join people p with (nolock) on c.client = p.client and 1 = p.relation
	left outer join names pn with (nolock) on p.nameid = pn.name
	left outer join drop_reasons dr with (nolock) on c.drop_reason = dr.drop_reason
	left outer join counselors co with (nolock) on c.counselor = co.counselor

	where	c.active_status = 'I'
	and	c.drop_date is not null
	and	c.drop_date between @from_date and @to_date
	and	cc.creditor <> @paf_creditor
	group by c.client, c.drop_date, c.drop_reason_other, dr.description, c.program_months, co.person, pn.prefix, pn.first, pn.middle, pn.last, pn.suffix;

else

	select	c.client,
		dbo.format_reverse_name ( pn.prefix, pn.first, pn.middle, pn.last, pn.suffix ) as client_name,
		c.drop_date,
		isnull(c.drop_reason_other, dr.description) as drop_reason,
		c.program_months,
		dbo.format_counselor_name ( co.person ) as counselor,
		sum(b.orig_balance) as original_balance,
		sum(b.total_payments) as paid_to_date

	from	clients c with (nolock)
	left outer join client_creditor cc on c.client = cc.client
	left outer join client_creditor_balances b on cc.client_creditor_balance = b.client_creditor_balance and b.client_creditor = cc.client_creditor
	left outer join people p with (nolock) on c.client = p.client and 1 = p.relation
	left outer join names pn with (nolock) on p.nameid = pn.name
	left outer join drop_reasons dr with (nolock) on c.drop_reason = dr.drop_reason
	left outer join counselors co with (nolock) on c.counselor = co.counselor

	where	c.active_status = 'I'
	and	c.drop_date is not null
	and	c.drop_date between @from_date and @to_date
	and	cc.creditor <> @paf_creditor
	and	c.office = @office
	group by c.client, c.drop_date, c.drop_reason_other, dr.description, c.program_months, co.person, pn.prefix, pn.first, pn.middle, pn.last, pn.suffix;

return ( @@rowcount )
GO
