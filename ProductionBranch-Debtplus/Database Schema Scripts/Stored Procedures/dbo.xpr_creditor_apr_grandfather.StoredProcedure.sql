USE [DebtPlus]
GO
/****** Object:  StoredProcedure [dbo].[xpr_creditor_apr_grandfather]    Script Date: 09/15/2014 13:13:34 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[xpr_creditor_apr_grandfather] ( @creditor as varchar(10) ) as

set nocount on

declare		@old_medium_apr_pct	float
declare		@old_highest_apr_pct	float
declare		@old_lowest_apr_pct	float
declare		@old_medium_apr_amt	money
declare		@old_highest_apr_amt	money

select		@old_medium_apr_pct	= medium_apr_pct,
		@old_highest_apr_pct	= highest_apr_pct,
		@old_lowest_apr_pct	= lowest_apr_pct,
		@old_medium_apr_amt	= medium_apr_amt,
		@old_highest_apr_amt	= highest_apr_amt
from		creditors with (nolock)
where		creditor		= @creditor

select		cc.client, cc.client_creditor, isnull(bal.orig_balance,0) + isnull(bal.orig_balance_adjustment, 0) + isnull(bal.total_interest,0) - isnull(bal.total_payments,0) as balance, @old_lowest_apr_pct as dmp_interest
into		#creditor_rates
from		client_creditor cc
inner join	client_creditor_balances bal	with (nolock) on cc.client_creditor_balance = bal.client_creditor_balance
inner join	clients c			with (nolock) on cc.client = c.client
where		c.active_status in ('A', 'AR')
and		cc.creditor		= @creditor
and		cc.reassigned_debt	= 0
and		isnull(bal.orig_balance,0) + isnull(bal.orig_balance_adjustment, 0) + isnull(bal.total_interest,0) - isnull(bal.total_payments,0) > 0
and		cc.dmp_interest is null
and		cc.first_payment is not null

-- Update the figures for the medium interest rate
update		#creditor_rates
set		dmp_interest = @old_medium_apr_pct
where		balance	>= @old_medium_apr_amt

-- Update the figures for the highest interest rate
update		#creditor_rates
set		dmp_interest	= @old_highest_apr_pct
where		balance	>= @old_highest_apr_amt

-- Apply the changes to the list of debts
update		client_creditor
set		dmp_interest	= x.dmp_interest
from		client_creditor cc
inner join	#creditor_rates x on cc.client_creditor = x.client_creditor

-- Insert a client note that we changed the rate
insert into client_notes (client, client_creditor, is_text, subject, dont_edit, dont_delete, dont_print, type, note)
select		client, client_creditor, 1, 'APR Rate Changed', 1, 1, 0, 3, 'The APR rate for the client debt was changed to ' + convert(varchar, dmp_interest * 100.0) + '% because the creditor rates were changed.'
from		#creditor_rates

drop table	#creditor_rates

return ( 1 )
GO
