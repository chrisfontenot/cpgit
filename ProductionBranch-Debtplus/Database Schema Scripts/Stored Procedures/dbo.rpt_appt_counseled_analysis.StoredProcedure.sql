USE [DebtPlus]
GO
/****** Object:  StoredProcedure [dbo].[rpt_appt_counseled_analysis]    Script Date: 09/15/2014 13:13:32 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[rpt_appt_counseled_analysis](@From_Date as datetime = null, @To_Date as datetime = null) as

-- ====================================================================================================
-- ==            Appointment slots available v.s. appointments kept                                  ==
-- ====================================================================================================

-- Supress intermediate results
set nocount on

if @To_Date is null
	select	@to_date	= getdate()
	
if @From_Date is null
	select	@from_date	= @to_date

-- Standardize the date ranges
select	@From_Date	=	convert(varchar(10), @From_Date, 101),
		@To_Date	=   convert(varchar(10), dateadd(d, 1, @To_date), 101)

-- Build a list of the office slots for that time period
select	c.appt_counselor, convert(int,null) as client_appointment, ca.appt_time, ca.appt_type, ca.start_time, ca.office, c.counselor, 1 as slots, 0 as kept
into	#available
from	appt_times ca
inner join appt_counselors c on ca.appt_time = c.appt_time and 0 = c.inactive
where	ca.start_time >= @From_Date
and		ca.start_time <  @To_date
--and		dbo.DateToMinutes (ca.start_time) in (675,735,795)

-- Update the items from the kept information
update	#available
set		appt_type	= ca.appt_type,
		kept		= 1,
		client_appointment = ca.client_appointment
from	#available a
inner join client_appointments ca on ca.start_time = a.start_time and a.office = ca.office and a.counselor = ca.counselor
where	ca.status = 'K'

select	a.appt_time, a.start_time, a.office, isnull(sum(slots),0) as slots, isnull(sum(kept),0) as kept, a.appt_type, o.name as office_name, apt.appt_name
from	#available a
left outer join offices o on a.office = o.office
left outer join appt_types apt on a.appt_type = apt.appt_type
group by a.appt_time, a.start_time, a.office, a.appt_type, o.name, apt.appt_name
order by a.office, a.start_time

drop table #available
return ( @@rowcount )
GO
