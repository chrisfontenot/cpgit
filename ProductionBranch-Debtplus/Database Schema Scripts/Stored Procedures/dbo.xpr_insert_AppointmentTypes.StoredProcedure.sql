USE [DebtPlus]
GO
/****** Object:  StoredProcedure [dbo].[xpr_insert_AppointmentTypes]    Script Date: 09/15/2014 13:13:35 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[xpr_insert_AppointmentTypes] ( @Description as varchar(50),@Duration as int = 60,@Default as bit = 0,@ActiveFlag as bit = 0,@BookLetter as varchar(10) = null,@RescheduleLetter as varchar(10) = null,@CancelLetter as varchar(10) = null,@MissedAppointmentRetentionEvent as int = null,@ContactType as varchar(4) = null,@BankruptcyClass as int = null ) as
	insert into AppointmentTypes ([Description],[Duration],[Default],[ActiveFlag],[BookLetter],[RescheduleLetter],[CancelLetter],[MissedAppointmentRetentionEvent],[ContactType],[BankruptcyClass]) values (@Description,@Duration,@Default,@ActiveFlag,@BookLetter,@RescheduleLetter,@CancelLetter,@MissedAppointmentRetentionEvent,@ContactType,@BankruptcyClass)
	return ( scope_identity() )
GO
