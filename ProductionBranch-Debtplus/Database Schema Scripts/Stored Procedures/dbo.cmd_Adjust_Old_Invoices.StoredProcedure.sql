USE [DebtPlus]
GO
/****** Object:  StoredProcedure [dbo].[cmd_Adjust_Old_Invoices]    Script Date: 09/15/2014 13:13:31 ******/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[cmd_Adjust_Old_Invoices] ( @CutoffDate as datetime, @Forced as BIT = 0, @creditor as varchar(10) = null ) AS
-- =============================================================================================
-- ==            This procedure will adjust old invoices to take them "off the books"         ==
-- =============================================================================================

-- This is the date that we will use in timestamping all of our transactions
DECLARE @CurrentDate   DateTime
SELECT @CurrentDate = getdate()

-- Complain if the user wants to adjust invoices less than 180 days old.
IF (datediff ( d, @CutoffDate, @CurrentDate) < 180)
BEGIN
	IF (@Forced = 0)
	BEGIN
		RaisError (50025, 8, 1)
		RETURN ( 0 )
	END
END

-- Suspend the ANSI counting
SET NOCOUNT ON

-- Start a transaction
BEGIN TRANSACTION

-- Do the adjustment
create table #adjusted (
	creditor		varchar(10),
	invoice_register	int,
	extra			money
);

-- Find the list of invoices to be adjusted
if @creditor is null
	insert into #adjusted (creditor, invoice_register, extra)
	select	creditor, isnull(invoice_register,0), isnull(inv_amount,0) - isnull(pmt_amount,0) - isnull(adj_amount,0)
	from	registers_invoices
	where	inv_date	< @cutoffDate
	AND	isnull(inv_amount,0) > (isnull(adj_amount,0) + isnull(pmt_amount,0))
else
	insert into #adjusted (creditor, invoice_register, extra)
	select	creditor, isnull(invoice_register,0), isnull(inv_amount,0) - isnull(pmt_amount,0) - isnull(adj_amount,0)
	from	registers_invoices
	where	inv_date	< @cutoffDate
	AND	creditor	= @creditor
	AND	isnull(inv_amount,0) > (isnull(adj_amount,0) + isnull(pmt_amount,0))

-- Generate the adjustment transactions
insert into registers_creditor (creditor, invoice_register, credit_amt, tran_type, item_date, message, date_created)
select	creditor, invoice_register, extra, 'RA', @CurrentDate, 'Adjusted off invoice', @CurrentDate
from	#adjusted

-- Credit the invoices
update	registers_invoices
set	adj_amount	= isnull(ri.adj_amount,0) + i.extra,
	adj_date	= @CurrentDate
from	registers_invoices ri
inner join #adjusted i on ri.invoice_register = i.invoice_register;

-- Determine the statistics for the display
declare	@TotalForgiven	money
declare	@InvoiceCount	int

select	@InvoiceCount	= count(*),
	@TotalForgiven	= sum(extra)
from	#adjusted;

drop table #adjusted

-- Commit the changes to the database. Once this is done there is no going back.
COMMIT TRANSACTION

-- Restore the ANSI counting
SET NOCOUNT OFF

-- Give the user the bad news
PRINT 'Total Amount of ' + convert(varchar, isnull(@InvoiceCount,0)) + ' invoices forgiven is: $ ' + convert (varchar, isnull(@TotalForgiven,0), 1)
RETURN ( @InvoiceCount )
GO
