USE [DebtPlus]
GO
/****** Object:  StoredProcedure [dbo].[xpr_Find_Office_ByZipcode]    Script Date: 09/15/2014 13:13:35 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[xpr_Find_Office_ByZipcode] ( @postalcode AS varchar(50) = null ) AS
-- =============================================================================================
-- ==                Find the office which maps to the indicated zipcode                      ==
-- =============================================================================================

SET NOCOUNT ON

-- If there is no zipcode then use the value of zeros
if @postalcode is null
	select @postalcode = '000000000'

-- Make the zipcode a nine digit number
select	@postalcode = left(replace(replace(@postalcode,'-',''),' ','') + '000000000', 9)

-- Find the indicated range for the zipcode
declare	@office	int

SELECT	top 1
	@office = office
FROM	zipcodes with (nolock)
WHERE	@postalcode BETWEEN zip_lower AND zip_upper

ORDER BY (convert ( int, zip_upper ) - convert ( int,  zip_lower ))

if @office is null
	select @office = 0

SELECT	@office as office
return ( @office )
GO
