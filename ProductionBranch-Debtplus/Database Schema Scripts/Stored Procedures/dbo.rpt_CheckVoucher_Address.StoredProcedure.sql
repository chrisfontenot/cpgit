USE [DebtPlus]
GO
/****** Object:  StoredProcedure [dbo].[rpt_CheckVoucher_Address]    Script Date: 09/15/2014 13:13:32 ******/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE PROCEDURE [dbo].[rpt_CheckVoucher_Address] ( @trust_register AS INT, @creditor as varchar(10) = null ) AS
-- ====================================================================================
-- ==               Retrieve the address information for the check                   ==
-- ====================================================================================

-- ChageLog
--   2/15/2003
--     Rewrote to use the "new" format for creditor addresses

SET NOCOUNT ON

DECLARE	@client		typ_client
DECLARE	@trust_creditor	typ_creditor
DECLARE	@tran_type	varchar(2)

SELECT	@client		= client,
		@trust_creditor	= creditor,
		@tran_type	= tran_type
FROM	registers_trust WITH (NOLOCK)
WHERE	trust_register	= @trust_register

if @tran_type = 'BW' and @creditor is not null
	SELECT	@trust_creditor = @creditor

declare	@attn		varchar(80)
DECLARE @addr1		varchar(256)
DECLARE @addr2		varchar(256)
DECLARE @addr3		varchar(256)
DECLARE @addr4		varchar(256)
DECLARE	@addr5		varchar(256)
declare	@postalcode	varchar(80)

-- Use the creditor address if this is a creditor check
IF @trust_creditor IS NOT NULL
	select	@attn		= 'ATTN: ' + attn,
			@addr1		= addr1,
			@addr2		= addr2,
			@addr3		= addr3,
			@addr4		= addr4,
			@addr5		= addr6,
			@postalcode	= postalcode
	from	view_creditor_addresses with (nolock)
	where	creditor	= @trust_creditor
	and		type		= 'P'

else if @client is not null

	select	@attn		= null,
			@addr1		= name,
			@addr2		= addr1,
			@addr3		= addr2,
			@addr5		= addr3,
			@postalcode	= zipcode
	from	view_client_address with (nolock)
	where	client		= @client

-- Return the address information
select	upper(@attn)		as 'attn',
		upper(@addr1)		as 'addr_1',
		upper(@addr2)		as 'addr_2',
		upper(@addr3)		as 'addr_3',
		upper(@addr4)		as 'addr_4',
		upper(@addr5)		as 'addr_5',
		@postalcode			as 'zipcode'

-- Return the number of rows in the result set
RETURN ( 1 )
GO
