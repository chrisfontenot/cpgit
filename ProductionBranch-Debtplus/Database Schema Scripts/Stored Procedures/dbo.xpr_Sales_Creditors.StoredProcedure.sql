USE [DebtPlus]
GO
CREATE PROCEDURE [dbo].[xpr_Sales_Creditors] as
-- =============================================================================
-- ==            List of the creditors for the sales tool                     ==
-- =============================================================================

select	s.sales_creditor		as sales_creditor,
	isnull(cr.min_accept_amt,0)	as MinPayment,
	isnull(cr.min_accept_pct,0)	as Prorate,
	isnull(s.Type,'A')		as Type,
	s.Name				as Name,
	s.creditor			as creditor,
 	isnull(cr.lowest_apr_pct,1)	as ReducedRate

from	sales_creditors s with (nolock)
left outer join creditors cr with (nolock) on s.creditor = cr.creditor
order by s.name

return ( @@rowcount 

GO
