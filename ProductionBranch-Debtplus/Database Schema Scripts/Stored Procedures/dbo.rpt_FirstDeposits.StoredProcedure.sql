USE [DebtPlus]
GO
/****** Object:  StoredProcedure [dbo].[rpt_FirstDeposits]    Script Date: 09/15/2014 13:13:33 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE PROCEDURE [dbo].[rpt_FirstDeposits] ( @FromDate as DateTime = NULL, @ToDate as DateTime = NULL ) AS
-- =========================================================================================
-- ==             List the information for the client first deposits                      ==
-- =========================================================================================

-- Initialization
set nocount on

-- Ensure that the dates are valid
if @ToDate is null
	select @ToDate = getdate()

if @FromDate is null
	select @FromDate = @ToDate

select	@FromDate = convert(datetime, convert(varchar(10), @FromDate, 101) + ' 00:00:00'),
	@ToDate	  = convert(datetime, convert(varchar(10), @ToDate,   101) + ' 23:59:59')

-- Find the client information with the deposit dates
select
	c.client,				-- client id
	x.name,					-- client name
	c.first_deposit_date,			-- first deposit date
	convert(money,0.0) as first_amount,	-- first deposit amount
	c.last_deposit_date,			-- last deposit date
	c.last_deposit_amount,			-- last deposit amount
	c.active_status				-- active status

from	clients c
inner join view_client_address x on c.client = x.client
where	c.first_deposit_date between @FromDate and @ToDate
order by c.client

return ( @@rowcount )
GO
