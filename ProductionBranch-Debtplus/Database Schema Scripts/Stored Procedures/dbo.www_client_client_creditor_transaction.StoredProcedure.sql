SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER procedure [www_client_client_creditor_transaction] ( @client as int, @client_creditor as int, @date_range as varchar(30) = null ) AS
-- ===========================================================================================
-- ==            Generate the list of transactions for the date range                       ==
-- ===========================================================================================

-- ChangeLog
--   06/09/2009
--      Remove references to debt_number and use client_creditor

-- Suppress intermediate results
set nocount on

-- Date ranges
declare	@from_date	datetime
declare	@to_date	datetime
select	@date_range	= lower(@date_range)

-- Determine the current month range
if @date_range = 'current_month'
begin
	select	@to_date = getdate()
	select	@from_date = convert(datetime, convert(varchar, month(@to_date)) + '/01/' + convert(varchar, year(@to_date))) + ' 00:00:00'
end

-- Current year
else if @date_range = 'current_year'
begin
	select	@from_date = getdate()
	select	@to_date = convert(datetime, '12/31/' + convert(varchar, year(@from_date))) + ' 23:59:59'
	select	@from_date = convert(datetime, '01/01/' + convert(varchar, year(@from_date))) + ' 00:00:00'
end

-- Find the last 60 days information
else if @date_range = 'last_60'
begin
	select	@to_date = getdate()
	select	@to_date = convert(datetime, convert(varchar, month(@to_date)) + '/01/' + convert(varchar, year(@to_date))) + ' 00:00:00'
	select	@to_date = dateadd (s, -1, @to_date)
	select	@from_date = dateadd (m, -1, @to_date)
	select	@from_date = convert(datetime, convert(varchar, month(@from_date)) + '/01/' + convert(varchar, year(@from_date))) + ' 00:00:00'
end

-- Find the last 90 days information
else if @date_range = 'last_90'
begin
	select	@to_date = getdate()
	select	@to_date = convert(datetime, convert(varchar, month(@to_date)) + '/01/' + convert(varchar, year(@to_date))) + ' 00:00:00'
	select	@to_date = dateadd (s, -1, @to_date)
	select	@from_date = dateadd (m, -2, @to_date)
	select	@from_date = convert(datetime, convert(varchar, month(@from_date)) + '/01/' + convert(varchar, year(@from_date))) + ' 00:00:00'
end

-- Find the last 120 days information
else if @date_range = 'last_120'
begin
	select	@to_date = getdate()
	select	@to_date = convert(datetime, convert(varchar, month(@to_date)) + '/01/' + convert(varchar, year(@to_date))) + ' 00:00:00'
	select	@to_date = dateadd (s, -1, @to_date)
	select	@from_date = dateadd (m, -3, @to_date)
	select	@from_date = convert(datetime, convert(varchar, month(@from_date)) + '/01/' + convert(varchar, year(@from_date))) + ' 00:00:00'
end

-- Find the last year information
else if @date_range = 'last_year'
begin
	select	@from_date = dateadd (yy, -1, getdate())
	select	@to_date = convert(datetime, '12/31/' + convert(varchar, year(@from_date))) + ' 23:59:59'
	select	@from_date = convert(datetime, '01/01/' + convert(varchar, year(@from_date))) + ' 00:00:00'
end

-- Find all transactions
else if @date_range = 'all'
begin
	select	@from_date = '1/1/1900'
	select	@to_date = getdate()
end

-- Assume that the default is "last_month"
else
begin
	select	@to_date = getdate()
	select	@to_date = convert(datetime, convert(varchar, month(@to_date)) + '/01/' + convert(varchar, year(@to_date))) + ' 00:00:00'
	select	@to_date = dateadd (s, -1, @to_date)
	select	@from_date = @to_date
	select	@from_date = convert(datetime, convert(varchar, month(@from_date)) + '/01/' + convert(varchar, year(@from_date))) + ' 00:00:00'
end

-- Remove the times from the date fields
select	@from_date = convert(datetime, convert(varchar(10), @from_date, 101) + ' 00:00:00')
select	@to_date   = convert(datetime, convert(varchar(10), @to_date, 101)   + ' 23:59:59')

-- Read the creditor for the transactions
declare	@creditor	varchar(10)
select	@creditor	= creditor
from	client_creditor with (nolock)
where	client		= @client
and	client_creditor	= @client_creditor

-- Once the creditor has been determined, find the transactions
select
	rcc.client_creditor_register			as 'client_creditor_register',
	convert(varchar(10), rcc.date_created, 1)	as 'date_created',
	rcc.tran_type					as 'tran_type',
	isnull(rcc.credit_amt,0)			as 'credit_amt',
	isnull(rcc.debit_amt,0)				as 'debit_amt',
	isnull(convert(varchar,tr.checknum),'')			as 'check_number'
from	registers_client_creditor rcc with (nolock)
left outer join registers_trust tr with (nolock) on rcc.trust_register = tr.trust_register
where	rcc.client = @client
and		rcc.client_creditor = @client_creditor
and		rcc.date_created between @from_date and @to_date
order by rcc.date_created
return ( @@rowcount )
GO
