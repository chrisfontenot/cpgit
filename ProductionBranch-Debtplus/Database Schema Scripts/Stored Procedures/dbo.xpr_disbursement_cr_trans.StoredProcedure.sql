USE [DebtPlus]
GO
/****** Object:  StoredProcedure [dbo].[xpr_disbursement_cr_trans]    Script Date: 09/15/2014 13:13:35 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[xpr_disbursement_cr_trans] ( @tran_type as typ_transaction = 'AD', @trust_register as typ_key = null, @disbursement_register as typ_key = null, @creditor as typ_creditor = null, @gross as money = 0, @deducted as money = 0, @billed as money = 0, @creditor_register as int = null ) AS

-- ======================================================================================================================
-- ==            Update the creditor statistics with the current check data                                            ==
-- ======================================================================================================================

-- NOTE : The transaciton is started by the outer application program that encompasses this call. This call is protected by a
--        transaction. We just don't start it.

-- Disable result sets
set nocount on

-- Local storage
declare	@invoice_register	int

-- Update the net amount for the trust register
update	registers_trust
set		amount		= isnull(amount,0) + @gross - @deducted
where	trust_register	= @trust_register

-- If the creditor is billed and has no billing period then generate an invoice "NOW"
if @billed > 0
begin
	declare	@contrib_cycle			varchar(1)
	declare	@creditor_type_eft		varchar(1)
	declare	@creditor_type_check	varchar(1)

	select	@contrib_cycle			= cr.contrib_cycle,
			@creditor_type_eft		= isnull(pct.creditor_type_eft,'N'),
			@creditor_type_check	= isnull(pct.creditor_type_check,'N')
	from	creditors cr with (nolock)
	LEFT OUTER JOIN creditor_contribution_pcts pct with (nolock) on cr.creditor_contribution_pct = pct.creditor_contribution_pct
	where	cr.creditor		= @creditor

	-- Determine the appropriate contribution type based upon the transaction
	if @tran_type <> 'BW'
		select	@creditor_type_eft = @creditor_type_check

	-- If the billing cycle is "NONE" then do the extra work to create the invoice immediately
	if (isnull(@contrib_cycle,'M') = 'N') and (@creditor_type_eft = 'B')
	begin

		-- Generate an invoice to hold the details
		insert into registers_invoices (creditor, inv_date, inv_amount, pmt_amount, adj_amount)
		values (@creditor, getdate(), @billed, 0, 0)

		select  @invoice_register = SCOPE_IDENTITY()
/* -- maybe needed
		-- Stamp the debt transactions with the invoice
		update	registers_client_creditor
		set		creditor_register		= @creditor_register,
				invoice_register		= @invoice_register
		where	disbursement_register	= @disbursment_register
		and		trust_register			= @trust_register
		and		creditor				- @creditor
*/
		-- Indicate that the check is part of the invoice
		update	registers_trust
		set		invoice_register	= @invoice_register
		where	trust_register		= @trust_register
	end
end

-- Insert the item into the creditor register
if @creditor_register is null
begin
	insert into registers_creditor (tran_type, creditor, debit_amt, trust_register, disbursement_register, invoice_register)
	values (@tran_type, @creditor, @gross - @deducted, @trust_register, @disbursement_register, @invoice_register)

	select	@creditor_register	= SCOPE_IDENTITY()

	-- Post the update back to the debt transactions
	-- (This will not be performed shortly so it is here just to handle the "old" conditions.)
	update	registers_client_creditor
	set		creditor_register	= @creditor_register,
			invoice_register	= @invoice_register
	where	tran_type in ('AD', 'BW', 'CM', 'MD')
	and		trust_register		= @trust_register
	and		creditor			= @creditor
	
	
end else begin

	-- One was created normally in the disbursement. Complete it.
	update	registers_creditor
	set		debit_amt			= @gross - @deducted,
			invoice_register	= @invoice_register
	where	creditor_register	= @creditor_register		
end

-- Update the creditor with the statistics for the disbursement data
update	creditors
set		first_payment			= isnull(first_payment, @creditor_register),
		last_payment			= @creditor_register,
		distrib_mtd				= distrib_mtd + @gross,
		contrib_mtd_billed		= contrib_mtd_billed + @billed,
		contrib_mtd_received	= contrib_mtd_received + @deducted
where	creditor		= @creditor

-- Return the pointer to the new record
return ( @creditor_register )
GO
