USE [DebtPlus]
GO
/****** Object:  StoredProcedure [dbo].[xpr_insert_action_plans]    Script Date: 09/15/2014 13:13:35 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[xpr_insert_action_plans] ( @client as int ) as
-- ========================================================================================
-- ==   Procedure for .NET to insert items into the action_plans table                   ==
-- ========================================================================================
	insert into action_plans ( [client] ) values ( @client )
	return ( scope_identity() )
GO
