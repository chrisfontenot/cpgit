USE [DebtPlus]
GO
-- Discard the obsolete stored procedure
IF EXISTS(SELECT * FROM SYSOBJECTS WHERE TYPE='P' AND NAME=N'xpr_insert_client_payout_details')
	EXEC ('DROP PROCEDURE [dbo].[xpr_insert_client_payout_details]')
GO
