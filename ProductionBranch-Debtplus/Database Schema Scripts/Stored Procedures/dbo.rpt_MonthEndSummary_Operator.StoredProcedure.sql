USE [DebtPlus]
GO
/****** Object:  StoredProcedure [dbo].[rpt_MonthEndSummary_Operator]    Script Date: 09/15/2014 13:13:33 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[rpt_MonthEndSummary_Operator] ( @FromDate as DateTime = NULL, @ToDate as DateTime = NULL ) AS
-- =============================================================================================
-- ==                   Find the receipts by operator                                         ==
-- =============================================================================================

-- Suppress intermediate result sets
SET NOCOUNT ON

IF @ToDate IS NULL
	SELECT @ToDate = getdate()

IF @FromDate IS NULL
	SELECT @FromDate = @ToDate

-- Adjust the dates for the proper settings
SELECT          @FromDate   = CONVERT(varchar(10), @FromDate, 101),
                @ToDate     = CONVERT(varchar(10), @ToDate, 101) + ' 23:59:59';

-- Fetch the receipts
SELECT		d.created_by					as 'counselor',
		convert(datetime, convert(varchar(10), d.date_created, 101) + ' 00:00:00') as 'item_date',
		d.client					as 'client',
		isnull(d.credit_amt,0) - isnull(d.debit_amt,0)	as 'gross',
		0						as 'deduct',
		d.tran_subtype                                  as 'tran_type'
FROM		registers_client d WITH (NOLOCK)
WHERE		d.date_created BETWEEN @FromDate AND @ToDate
and		d.tran_type = 'DP'
ORDER BY	1, 2, 3

RETURN ( @@rowcount )
GO
