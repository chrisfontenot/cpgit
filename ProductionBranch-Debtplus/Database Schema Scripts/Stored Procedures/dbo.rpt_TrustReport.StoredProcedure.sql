USE [DebtPlus]
GO
/****** Object:  StoredProcedure [dbo].[rpt_TrustReport]    Script Date: 09/15/2014 13:13:34 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE PROCEDURE [dbo].[rpt_TrustReport] AS
-- ============================================================================================
-- ==                   Retrieve the information for the trust report                        ==
-- ============================================================================================

-- Fetch the information from the view
SELECT		*
FROM		view_held_in_trust
WHERE		held_in_trust <> 0
order by	client

RETURN ( @@rowcount )
GO
