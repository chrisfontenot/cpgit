USE [DebtPlus]
GO
/****** Object:  StoredProcedure [dbo].[xpr_void_creditor_refund]    Script Date: 09/15/2014 13:13:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[xpr_void_creditor_refund] ( @creditor_register as int, @message as varchar(50) = null ) as
-- ====================================================================================
-- ==            Reverse a creditor refund because the check bounced                 ==
-- ====================================================================================

-- ChangeLog
--   9/31/2002
--     Added support for client_creditor_balances table
--   10/15/2002
--     Added support for client_creditor's "*_this_creditor" information
--   7/22/2003
--     Corrected returns_this_creditor column name
--   06/09/2009
--      Remove references to debt_number and use client_creditor

set nocount on
begin transaction
set xact_abort on

-- Ensure that the transaction is a creditor refund
declare	@creditor		varchar(10)
declare	@tran_type		varchar(2)
declare	@credit_amt		money
declare	@refund_trust		int
declare	@gross_amt		money
declare	@fairshare_amt		money

declare	@void_trust_register	int
declare	@void_creditor_register	int

select	@creditor	= creditor,
	@credit_amt	= credit_amt,
	@refund_trust	= trust_register,
	@tran_type	= tran_type
from	registers_creditor
where	creditor_register = @creditor_register;

-- The "tran_type" is required. If it is null then it is because the row could not be found.
if @tran_type is null
begin
	commit transaction;
	RaisError ('The creditor_register reference is not valid', 16, 1);
	return ( 0 );
end

-- The transaction type should be a creditor refund.
if @tran_type != 'RF'
begin
	commit transaction;
	RaisError ('The creditor_register reference is not a creditor refund', 16, 1);
	return ( 0 );
end

-- Double check the amounts of the details against the check
select	client, creditor, client_creditor, credit_amt, fairshare_amt, fairshare_pct, creditor_type, client_creditor_register
into	#void_creditor_details
from	registers_client_creditor
where	tran_type		= 'RF'
and	trust_register		= @refund_trust
and	creditor_register	= @creditor_register
and	void			= 0;

select	@gross_amt	= sum(credit_amt),
	@fairshare_amt	= sum(fairshare_amt)
from	#void_creditor_details;

if (isnull(@gross_amt,0) - isnull(@fairshare_amt,0)) <> @credit_amt
begin
	drop table #void_creditor_details;
	commit transaction;
	RaisError ('The detail dollar amount does not match the creditor check for the refund', 16, 1);
	return ( 0 );
end

-- Ensure that the client exists to remove the money
if exists (select * from #void_creditor_details d left outer join clients c on d.client = c.client where c.client is null)
begin
	drop table #void_creditor_details;
	commit transaction;
	RaisError ('The check contains client references to clients which no longer exist in the system.', 16, 1);
	return ( 0 );
end

-- Create a trust register to hold the refund "service charge"
insert into registers_trust (tran_type, creditor, amount, cleared) values ('VF', @creditor, @credit_amt, ' ');
select	@void_trust_register = SCOPE_IDENTITY();

-- Create a creditor register refernece to hold the refund
insert into registers_creditor (tran_type, creditor, debit_amt, trust_register, message) values ('VF', @creditor, @credit_amt, @void_trust_register, @message);
select	@void_creditor_register = SCOPE_IDENTITY();

-- Subtract the dollar amounts from the client trust. The amount was credited when the creditor refund was accepted. Now it must be removed.
select	client, sum(credit_amt) as debit_amt
into	#void_client_details
from	#void_creditor_details
group by client;

insert into registers_client (tran_type, client, message, trust_register, debit_amt)
select	'VF', client, @message, @void_trust_register, isnull(debit_amt,0)
from	#void_client_details;

update	clients
set	held_in_trust = isnull(held_in_trust,0) - isnull(debit_amt,0)
from	clients c
inner join #void_client_details d on c.client = d.client;

drop table #void_client_details;

-- Insert the debt transactions
insert into registers_client_creditor (tran_type, client, creditor, client_creditor, debit_amt, fairshare_amt, creditor_type, fairshare_pct, trust_register, creditor_register)
select 'VF', client, creditor, client_creditor, credit_amt, fairshare_amt, creditor_type, fairshare_pct, @void_trust_register, @void_creditor_register
from	#void_creditor_details;

-- Adjust the debt payment information. The payments were reduced. Now they are paid.
update	client_creditor_balances
set	total_payments = total_payments + credit_amt,
	payments_month_0 = payments_month_0 + credit_amt
from	client_creditor_balances bal
inner join client_creditor cc on bal.client_creditor_balance = cc.client_creditor_balance
inner join #void_creditor_details d on cc.client_creditor = d.client_creditor;

-- Update the payments for this debt
update	client_creditor
set	returns_this_creditor = isnull(returns_this_creditor,0) - credit_amt
from	client_creditor cc
inner join #void_creditor_details d on cc.client_creditor = d.client_creditor;

-- Mark the creditor refund items as being "voided". This prevents the same check from being voided more than once.
update	registers_client_creditor
set	void = 1
from	registers_client_creditor rcc
inner join #void_creditor_details d on rcc.client_creditor_register = d.client_creditor_register;

-- Finally, credit the deduction acount with the fairshare amount
declare	@total_fairshare	money
select	@total_fairshare = sum(fairshare_amt)
from	#void_creditor_details;

if @total_fairshare is null
	select @total_fairshare = 0;

if @total_fairshare > 0
begin
	insert into registers_client (tran_type, client, credit_amt, trust_register, message)
	values ('VF', 0, @total_fairshare, @void_trust_register, @message);

	update	clients
	set	held_in_trust = isnull(held_in_trust,0) + @total_fairshare
	where	client = 0;
end

-- This is it.
drop table #void_creditor_details;
commit transaction;
return ( 1 );
GO
