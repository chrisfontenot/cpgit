USE [DebtPlus]
GO
/****** Object:  StoredProcedure [dbo].[www_creditor_message_read]    Script Date: 09/15/2014 13:13:34 ******/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE PROCEDURE [dbo].[www_creditor_message_read] ( @msgid as int ) as
-- ===================================================================================
-- ==            Return the message with the id from the list of messages           ==
-- ===================================================================================
select
	creditor_www_note	as 'item_key',
	message			as 'message',
	date_created		as 'date_created',
	created_by		as 'created_by',
	date_shown		as 'date_shown'
from	creditor_www_notes
where	creditor_www_note = @msgid
return ( @@rowcount )
GO
