USE [DebtPlus]
GO
/****** Object:  StoredProcedure [dbo].[rpt_PrintClient_other_debt]    Script Date: 09/15/2014 13:13:33 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[rpt_PrintClient_other_debt] ( @client as int ) as

-- ================================================================================
-- ==            Return the information for the other debts of the client        ==
-- ================================================================================

select	creditor_name, account_number, balance, payment, interest_rate
from	client_other_debts
where	client = @client
order by 1, 2

return ( @@rowcount )
GO
