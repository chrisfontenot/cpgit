IF NOT EXISTS(SELECT * FROM SYSOBJECTS WHERE TYPE='P' AND NAME=N'ocs_upload_NEW_client_FMAE')
	EXEC ('CREATE PROCEDURE [dbo].[ocs_upload_NEW_client_FMAE] AS')
GO
-- =============================================
-- Author:    Binh Tran
-- Create date:
-- Description: Insert new client FMAE upload 
-- database entries for new client
-- =============================================
/*
1. Insert address
2. Insert phone number rows
3. Insert person names, as appropriate
4. Insert client (FK address)
5. Insert person records, as appropriate (FK phone number, name, client)
6. Insert servicer/lender chain (FK person)
7. Insert OCS Client (FK client)
*/
ALTER PROCEDURE [dbo].[ocs_upload_NEW_client_FMAE] (
	@Program INT
	,@Archive BIT
	,@Activeflag BIT
	,@Isduplicate BIT
	,@UploadedrecordID INT
	,@UploadAttempt VARCHAR(50) = NULL
	,@Preferredlanguage INT = 1
	,@InvestorNumber VARCHAR(20)
	,@ServicerName VARCHAR(50) = NULL
	,@ServicerLoanNumber VARCHAR(50) = NULL
	,@PropertyStreet VARCHAR(50)
  ,@PropertyAddressLine2 VARCHAR(50) = NULL
	,@PropertyCity VARCHAR(50)
	,@PropertyState INT
	,@PropertyZipcode VARCHAR(15)
	,@MailingStreet VARCHAR(50) = NULL
  ,@MailingAddressLine2 VARCHAR(50) = NULL
	,@MailingCity VARCHAR(50) = NULL
	,@MailingState INT = NULL
	,@MailingZipCode VARCHAR(15) = NULL
	,@Hasperson1 BIT
	,@Firstname1 VARCHAR(50) = NULL
  ,@Middlename1 VARCHAR(50) = NULL
	,@Lastname1 VARCHAR(50) = NULL
	,@SSN1 VARCHAR(20) = NULL
	,@Email1 VARCHAR(20) = NULL
	,@Hasperson2 BIT
	,@Firstname2 VARCHAR(50) = NULL
  ,@Middlename2 VARCHAR(50) = NULL
	,@Lastname2 VARCHAR(50) = NULL
	,@Email2 VARCHAR(20) = NULL
	,@Borrower_PhoneAreaCode1 VARCHAR(5) = NULL
	,@Borrower_PhoneNum1 VARCHAR(50) = NULL
	,@Borrower_PhoneAreaCode2 VARCHAR(5) = NULL
	,@Borrower_PhoneNum2 VARCHAR(50) = NULL
	,@Borrower_PhoneAreaCode3 VARCHAR(5) = NULL
	,@Borrower_PhoneNum3 VARCHAR(50) = NULL
	,@Borrower_PhoneAreaCode4 VARCHAR(5) = NULL
	,@Borrower_PhoneNum4 VARCHAR(50) = NULL
	,@Borrower_PhoneAreaCode5 VARCHAR(5) = NULL
	,@Borrower_PhoneNum5 VARCHAR(50) = NULL
	,@Borrower_PhoneAreaCode6 VARCHAR(5) = NULL
	,@Borrower_PhoneNum6 VARCHAR(50) = NULL
	,@CoBorrower_PhoneAreaCode1 VARCHAR(5) = NULL
	,@CoBorrower_PhoneNum1 VARCHAR(50) = NULL
	,@CoBorrower_PhoneAreaCode2 VARCHAR(5) = NULL
	,@CoBorrower_PhoneNum2 VARCHAR(50) = NULL
	,@CoBorrower_PhoneAreaCode3 VARCHAR(5) = NULL
	,@CoBorrower_PhoneNum3 VARCHAR(50) = NULL
	,@CoBorrower_PhoneAreaCode4 VARCHAR(5) = NULL
	,@CoBorrower_PhoneNum4 VARCHAR(50) = NULL
	,@CoBorrower_PhoneAreaCode5 VARCHAR(5) = NULL
	,@CoBorrower_PhoneNum5 VARCHAR(50) = NULL
	,@CoBorrower_PhoneAreaCode6 VARCHAR(5) = NULL
	,@CoBorrower_PhoneNum6 VARCHAR(50) = NULL
	--Store only
	,@ACH_Flag VARCHAR(50) = NULL
	,@Agency_Name VARCHAR(50) = NULL
	,@Backlog_Mod_Flag VARCHAR(50) = NULL
	,@Mod_Conversion_Date VARCHAR(50) = NULL
	,@UPB VARCHAR(50) = NULL
	,@Reason_For_Default VARCHAR(50) = NULL
	,@Comment_Description VARCHAR(500) = NULL
	,@Trial_Mod VARCHAR(50) = NULL
	,@Trial_Mod_Description VARCHAR(500) = NULL
	,@Trial_Mod_Payment_Amount VARCHAR(50) = NULL
	,@TrialPaymentsReceivedAmount VARCHAR(50) = NULL
	,@TrialPaymentsReceivedCount VARCHAR(50) = NULL
	,@Last_Payment_Applied_Amount VARCHAR(50) = NULL
	,@Workout_Type VARCHAR(50) = NULL
	,@Inserted INT OUTPUT
	)
AS
BEGIN
	SET NOCOUNT ON;

	BEGIN TRANSACTION

	BEGIN TRY
		--INSERT CLIENT
		DECLARE @ClientID INT
			,@Calcservicerid INT
			,@Calcservicername VARCHAR(50)
			,@PropertyAddressID INT
			,@MailingAddressID INT
			,@UsehomeAddress BIT
			,@PhoneID1 INT
			,@PhoneID2 INT
			,@PhoneID3 INT
			,@PhoneID4 INT
			,@PhoneID5 INT
			,@PhoneID6 INT
			,@CoPhoneID1 INT
			,@CoPhoneID2 INT
			,@CoPhoneID3 INT
			,@CoPhoneID4 INT
			,@CoPhoneID5 INT
			,@CoPhoneID6 INT
      -- These flags will be used to decide if the phone numbers will be inserted in client notes
      ,@isBorrowerHomeUsed bit
			,@isBorrowerMsgUsed bit
			,@isAppCellphone1Used bit
			,@isAppWorkphone1Used bit
			,@isAppCellphone2Used bit
			,@isAppWorkphone2Used bit
			,@isCoborrowerHomeUsed bit
			,@isCoborrowerMsgUsed bit
			,@isCoAppCellphone1Used bit
			,@isCoAppWorkphone1Used bit
			,@isCoAppCellphone2Used bit
			,@isCoAppWorkphone2Used bit
      
			,@Person1 INT
			,@NameID INT
			,@Person2 INT
			,@CoNameID INT
			,@EmailID INT
			,@CoEmailID INT
			,@Cause_Fin_Problem INT
			,@Return_Value INT
			,@Ocs INT
			,@Ownerid INT
			,@Coownerid INT
			,@Property INT
			,@Loan INT
			,@InvestorID INT
			,@Lender INT
			,@NoteText VARCHAR(800)
			,@Subject VARCHAR(80)
      ,@hasCoApplicantWorkphone bit
      ,@coApplicantWorkphoneID int

		SELECT @ClientID = NULL
			,@Calcservicerid = 442
			,@CalcServicerName = 'Other or Unknown'
			,@PropertyAddressID = NULL
			,@MailingAddressID = NULL
			,@UsehomeAddress = 1
			,@PhoneID1 = NULL
			,@PhoneID2 = NULL
			,@PhoneID3 = NULL
			,@PhoneID4 = NULL
			,@PhoneID5 = NULL
			,@PhoneID6 = NULL
			,@CoPhoneID1 = NULL
			,@CoPhoneID2 = NULL
			,@CoPhoneID3 = NULL
			,@CoPhoneID4 = NULL
			,@CoPhoneID5 = NULL
			,@CoPhoneID6 = NULL
      ,@isBorrowerHomeUsed = 0
			,@isBorrowerMsgUsed = 0
			,@isAppCellphone1Used = 0
			,@isAppWorkphone1Used = 0
			,@isAppCellphone2Used = 0
			,@isAppWorkphone2Used = 0
			,@isCoborrowerHomeUsed = 0
			,@isCoborrowerMsgUsed = 0
			,@isCoAppCellphone1Used = 0
			,@isCoAppWorkphone1Used = 0
			,@isCoAppCellphone2Used = 0
			,@isCoAppWorkphone2Used = 0
			,@Person1 = NULL
			,@NameID = NULL
			,@Person2 = NULL
			,@CoNameID = NULL
			,@EmailID = NULL
			,@CoEmailID = NULL
			,@Cause_Fin_Problem = NULL
			,@Ocs = NULL
			,@Ownerid = NULL
			,@Coownerid = NULL
      , @hasCoApplicantWorkphone = 0
      ,@coApplicantWorkphoneID = null

		--GET Servicer Id
		SET @Calcservicerid = COALESCE(( SELECT TOP 1 Oid FROM Housing_Lender_Servicers [Servicers] WHERE Servicers.[Description] = @ServicerName), 442)
    
		SET @CalcServicerName = CASE WHEN @Calcservicerid = 442 THEN 'Other or Unknown' ELSE @ServicerName END

    print 'inserting property address'
		--INSERT ADDRESS
		IF (@PropertyStreet != '' AND @PropertyStreet IS NOT NULL)
		BEGIN
			EXEC @PropertyAddressID = [ocs_insert_address] @PropertyStreet, @PropertyCity, @PropertyState, @PropertyZipCode
      
      if @PropertyAddressID is not null
      begin
        update addresses
           set address_line_2 = @PropertyAddressLine2
         where address = @PropertyAddressID
      end
		END

    print 'inserting mailing address'
		IF (@MailingStreet != '' AND @PropertyStreet != @MailingStreet)
		BEGIN
			IF (@MailingStreet != '' AND @MailingStreet IS NOT NULL)
			BEGIN
				EXEC @MailingAddressID = [ocs_insert_address] @MailingStreet, @MailingCity, @MailingState, @MailingZipcode
        
        if @MailingAddressID is not null
        begin
          update addresses
             set address_line_2 = @MailingAddressLine2
           where address = @MailingAddressID
        end
			END

			SET @UsehomeAddress = 0
		END
    
    -- insert phone numbers
    -- borrower phone 1 or coborrower phone 1 to be inserted as phone 1
    -- borrower phone 2 or coborrower phone 2 to be inserted as phone 2
    -- borrower phone 3 or borrower phone 5 to be inserted as phone 3
    -- borrower phone 4 or borrower phone 6 or coborrower phone 4 or coborrower phone 6 to be inserted as phone 4
    -- coborrower phone 3 or coborrower phone 5 to be inserted as phone 5
    
    print 'inserting home phone'
    --Phone 1
		IF (@Borrower_PhoneAreaCode1 IS NOT NULL)
		BEGIN
			EXEC @PhoneID1 = xpr_insert_TelephoneNumbers @country = 1, @acode = @Borrower_PhoneAreaCode1, @number = @Borrower_PhoneNum1
      set @isBorrowerHomeUsed = 1
		END
    else
    begin    
      if (@CoBorrower_PhoneAreaCode1 IS NOT NULL)
		  begin
        EXEC @PhoneID1 = xpr_insert_TelephoneNumbers @country = 1, @acode = @CoBorrower_PhoneAreaCode1, @number = @CoBorrower_PhoneNum1
        set @isCoborrowerHomeUsed = 1
       end
    end
    
    print 'inserting msg phone'
    --Phone 2
		IF (@Borrower_PhoneAreaCode2 IS NOT NULL)
		BEGIN
			EXEC @PhoneID2 = xpr_insert_TelephoneNumbers @country = 1, @acode = @Borrower_PhoneAreaCode2, @number = @Borrower_PhoneNum2
      set @isBorrowerMsgUsed = 1
		END
    else
    begin
      if(@CoBorrower_PhoneAreaCode2 IS NOT NULL)
		  begin
			  EXEC @PhoneID2 = xpr_insert_TelephoneNumbers @country = 1, @acode = @CoBorrower_PhoneAreaCode2, @number = @CoBorrower_PhoneNum2
        set @isCoborrowerMsgUsed = 1
      end
		end
    
    print 'inserting applicant cell phone'
    --Phone 3
		IF (@Borrower_PhoneAreaCode3 IS NOT NULL)
		BEGIN
			EXEC @PhoneID3 = xpr_insert_TelephoneNumbers @country = 1, @acode = @Borrower_PhoneAreaCode3, @number = @Borrower_PhoneNum3
      set @isAppCellphone1Used = 1
		END
    else
    begin
      IF (@Borrower_PhoneAreaCode5 IS NOT NULL)
		  BEGIN
			  EXEC @PhoneID3 = xpr_insert_TelephoneNumbers @country = 1, @acode = @Borrower_PhoneAreaCode5, @number = @Borrower_PhoneNum5
        set @isAppCellphone2Used = 1
      end
		end
    
    print 'inserting applicant work phone'
    -- Phone 4
    IF (@Borrower_PhoneAreaCode4 IS NOT NULL)
		BEGIN
			EXEC @PhoneID4 = xpr_insert_TelephoneNumbers @country = 1, @acode = @Borrower_PhoneAreaCode4, @number = @Borrower_PhoneNum4
      set @isAppWorkphone1Used = 1
		END
    else
    begin
      -- check if applicant work phone 2 has value
      if (@Borrower_PhoneAreaCode6 IS NOT NULL)
		  begin
			  EXEC @PhoneID4 = xpr_insert_TelephoneNumbers @country = 1, @acode = @Borrower_PhoneAreaCode6, @number = @Borrower_PhoneNum6
        set @isAppWorkphone2Used = 1
		  end      
    
    end
    
    print 'inserting coapplicant workphone'
    -- check if coapplicant work phone 2 has value
    IF (@CoBorrower_PhoneAreaCode4 IS NOT NULL)
		begin
			EXEC @coApplicantWorkphoneID = xpr_insert_TelephoneNumbers @country = 1, @acode = @CoBorrower_PhoneAreaCode4, @number = @CoBorrower_PhoneNum4
      set @isCoAppWorkphone1Used = 1
      set @hasCoApplicantWorkphone = 1
		end
    else
    begin
      -- check if applicant work phone 2 has value
      if (@CoBorrower_PhoneAreaCode6 IS NOT NULL)
		  begin
			  EXEC @coApplicantWorkphoneID = xpr_insert_TelephoneNumbers @country = 1, @acode = @CoBorrower_PhoneAreaCode6, @number = @CoBorrower_PhoneNum6
      
        set @isCoAppWorkphone2Used = 1
        set @hasCoApplicantWorkphone = 1
		  end    
    end
    
    -- if borrower workphone is empty set coborrower workphone as borrower workphone
    if @PhoneID4 is null
    begin
      set @PhoneID4 = @coApplicantWorkphoneID
    end
    
    print 'inserting coapplicant cell phone'
    --Phone 5
		IF (@CoBorrower_PhoneAreaCode3 IS NOT NULL)
		BEGIN
			EXEC @PhoneID5 = xpr_insert_TelephoneNumbers @country = 1, @acode = @CoBorrower_PhoneAreaCode3, @number = @CoBorrower_PhoneNum3
      set @isCoAppCellphone1Used = 1
		END
    else
    begin
      IF (@CoBorrower_PhoneAreaCode5 IS NOT NULL)
		  BEGIN
			  EXEC @PhoneID5 = xpr_insert_TelephoneNumbers @country = 1, @acode = @CoBorrower_PhoneAreaCode5, @number = @CoBorrower_PhoneNum5
        set @isCoAppCellphone2Used = 1
      end
		end
    
    print 'inserting person 1 name'
		--INSERT NAMES
		IF (@Hasperson1 = 1)
		BEGIN
			EXEC @NameID = xpr_insert_names @first = @Firstname1, @middle = @Middlename1, @last = @Lastname1
		END

    print 'inserting person 2 name'
		IF (@Hasperson2 = 1)
		BEGIN
			EXEC @CoNameID = xpr_insert_names @first = @Firstname2, @middle = @Middlename2, @last = @Lastname2
		END

    print 'inserting email 1'
		--INSERT EMAIL
		IF (@Email1 != '' AND @Email1 IS NOT NULL)
			EXEC @EmailID = xpr_insert_emailaddress @Address = @Email1, @Validation = 1

    print 'inserting email 2'
		IF (@Email2 != '' AND @Email2 IS NOT NULL)
			EXEC @CoEmailID = xpr_insert_emailaddress @Address = @Email2, @Validation = 1


    print 'inserting client'
		--Create Client and People record
		EXEC @Return_Value = [DBO].[Xpr_Create_Client ] @HomeTelephoneID = @PhoneID1
			,@Mortgage_Problems = 1
			,@Referral_Code = 818
			,@Method_First_Contact = NULL
			,@Bankruptcy = 0

		SET @ClientID = @Return_Value

    print 'update client'
		UPDATE Clients
		SET AddressID = CASE WHEN @MailingAddressID IS NOT NULL THEN @MailingAddressID ELSE @PropertyAddressID END
			,MsgTelePhoneID = @PhoneID2
			,cause_fin_problem1 = ( SELECT [dbo].[GetFinancial_Problem](@Reason_For_Default))
			,[Language] = @Preferredlanguage
		WHERE Client = @ClientID
    
    -- if the phone number are all blank, set ocs_client active staus to inactive
    if ((@Borrower_PhoneAreaCode1 is null or @Borrower_PhoneAreaCode1 = '') and 
        (@Borrower_PhoneNum1 is null or @Borrower_PhoneNum1 = '')) and 
       ((@Borrower_PhoneAreaCode2 is null or @Borrower_PhoneAreaCode2 = '') and 
        (@Borrower_PhoneNum2 is null or @Borrower_PhoneNum2 = '')) and 
       ((@Borrower_PhoneAreaCode3 is null or @Borrower_PhoneAreaCode3 = '') and 
        (@Borrower_PhoneNum3 is null or @Borrower_PhoneNum3 = '')) and 
       ((@Borrower_PhoneAreaCode4 is null or @Borrower_PhoneAreaCode4 = '') and 
        (@Borrower_PhoneNum4 is null or @Borrower_PhoneNum4 = '')) and 
       ((@Borrower_PhoneAreaCode5 is null or @Borrower_PhoneAreaCode5 = '') and 
        (@Borrower_PhoneNum5 is null or @Borrower_PhoneNum5 = '')) and 
       ((@Borrower_PhoneAreaCode6 is null or @Borrower_PhoneAreaCode6 = '') and 
        (@Borrower_PhoneNum6 is null or @Borrower_PhoneNum6 = '')) and
       ((@CoBorrower_PhoneAreaCode1 is null or @CoBorrower_PhoneAreaCode1 = '') and 
        (@CoBorrower_PhoneNum1 is null or @CoBorrower_PhoneNum1 = '')) and
       ((@CoBorrower_PhoneAreaCode2 is null or @CoBorrower_PhoneAreaCode2 = '') and 
        (@CoBorrower_PhoneNum2 is null or @CoBorrower_PhoneNum2 = '')) and
       ((@CoBorrower_PhoneAreaCode3 is null or @CoBorrower_PhoneAreaCode3 = '') and 
        (@CoBorrower_PhoneNum3 is null or @CoBorrower_PhoneNum3 = '')) and
       ((@CoBorrower_PhoneAreaCode4 is null or @CoBorrower_PhoneAreaCode4 = '') and 
        (@CoBorrower_PhoneNum4 is null or @CoBorrower_PhoneNum4 = '')) and
       ((@CoBorrower_PhoneAreaCode5 is null or @CoBorrower_PhoneAreaCode5 = '') and 
        (@CoBorrower_PhoneNum5 is null or @CoBorrower_PhoneNum5 = '')) and
       ((@CoBorrower_PhoneAreaCode6 is null or @CoBorrower_PhoneAreaCode6 = '') and 
        (@CoBorrower_PhoneNum6 is null or @CoBorrower_PhoneNum6 = ''))
    begin
    
      set @Activeflag = 0
    
    end
       

    print 'inserting ocs client'
		--INSERT OCS CLIENT
		EXEC @Ocs = ocs_insert_ocs_client @ClientID = @ClientID
			,@Program = @Program
			,@Archive = @Archive
			,@Activeflag = @Activeflag
			,@Isduplicate = @Isduplicate
			,@UploadrecordID = @Uploadedrecordid
			,@Uploadattempt = @Uploadattempt
			,@InvestorNumber = @InvestorNumber
			,@ZipCode = @PropertyZipcode
			,@Searchservicerid = @Calcservicerid

		--INSERT PEOPLE
		IF (@Hasperson1 = 1 AND @NameID IS NOT NULL)
		BEGIN
			IF ( SELECT COUNT(*) FROM People WHERE Client = @ClientID AND Relation = 1 ) = 1
			BEGIN
				SET @Person1 = ( SELECT Person FROM People WHERE Client = @ClientID AND Relation = 1)
			END
			ELSE
			BEGIN
        print 'inserting person 1'
				--Insert into PEOPLE
				EXEC @Person1 = xpr_insert_people @Client = @ClientID
					,@Relation = 1
					,@NameID = @NameID
					,@no_fico_score_reason = 4
					,@race = 10
					,@ethnicity = 2
					,@gender = 1
					,@education = 1
					,@MilitaryStatusID = 0
					,@CreditAgency = 'EQUIFAX'
          ,@CellTelephoneID = @PhoneID3
          ,@WorkTelephoneID = @PhoneID4
			END

			SET @Ownerid = (
					SELECT oID
					FROM Housing_Borrowers
					WHERE NameID = @NameID
					)

			IF (@Ownerid IS NULL)
			BEGIN
        print 'inserting housing borrower'
				-- Insert Housing_Borrower
				EXEC @Ownerid = xpr_insert_housing_borrower @PersonID = 1
					,@NameID = @NameID
					,@Gender = 1
					,@Race = 10
					,@ethnicity = 2
					,@Education = 1
					,@Language = @Preferredlanguage
					,@CreditAgency = 'EQUIFAX'
					,@MilitaryServiceID = 0
					,@MilitaryStatusID = 0
					,@MilitaryGradeID = 0
					,@MilitaryDependentID = 0
			END
		END

		IF (@Hasperson2 = 1 AND @CoNameID IS NOT NULL)
		BEGIN
      print 'inserting person 2'
			--Insert into PEOPLE
			EXECUTE @Person2 = xpr_insert_people @Client = @ClientID
				,@Relation = 2
				,@NameID = @CoNameID
				,@no_fico_score_reason = 4
				,@race = 10
				,@ethnicity = 2
				,@gender = 2
				,@education = 1
				,@MilitaryStatusID = 0
				,@CreditAgency = 'EQUIFAX'
        ,@CellTelephoneID = @PhoneID5
        ,@WorkTelephoneID = @coApplicantWorkphoneID

			SET @Coownerid = (
					SELECT oID
					FROM Housing_Borrowers
					WHERE [Nameid] = @CoNameID
					)

			IF (@Coownerid IS NULL)
			BEGIN
        print 'inserting housing borrower 2'
				-- Insert Housing_Borrower
				EXEC @Coownerid = xpr_insert_housing_borrower @PersonID = 2 --1 = applicant, 2 = coapplicant
					,@NameID = @CoNameID
					,@Gender = 2
					,@Race = 10
					,@ethnicity = 2
					,@Education = 1
					,@Language = @Preferredlanguage
					,@CreditAgency = 'EQUIFAX'
					,@MilitaryServiceID = 0
					,@MilitaryStatusID = 0
					,@MilitaryGradeID = 0
					,@MilitaryDependentID = 0
			END
		END

    print 'update person 1'
    print 'person 1 values NameID: ' + convert(varchar,isnull(@NameID, 0)) + 
          ' Email: ' + convert(varchar,isnull(@EmailID, 0)) + ' WorkTelehpone: ' + convert(varchar,isnull(@PhoneID3, 0)) + 
          ' Cell: ' + convert(varchar,isnull(@PhoneID4, 0)) + ' SSN: ' + isnull(@SSN1, 0) + ' Person: ' + convert(varchar,isnull(@Person1, 0))
		--Update People 
		IF (@Person1 IS NOT NULL)
		BEGIN
			UPDATE [PEOPLE]
			SET NameID = @NameID
				,EmailID = @EmailID
				,CellTelePhoneID = @PhoneID3
				,WorkTelePhoneID = @PhoneID4
				,SSN = @SSN1
			WHERE [PERSON] = @Person1
		END

    print 'update person 2'
		IF (@Person2 IS NOT NULL)
		BEGIN
			UPDATE [PEOPLE]
			SET NameID = @CoNameID
				,EmailID = @CoEmailID
				,CellTelePhoneID = @PhoneID5
				,WorkTelePhoneID = @coApplicantWorkphoneID
			WHERE [PERSON] = @Person2
		END

		--INSERT SERVICER/LENDER CHAIN
		--NOTE: YES, the HousingID should be the ClientId!!
		SET @Property = (
				SELECT TOP 1 Oid
				FROM Housing_Properties
				WHERE Housingid = @ClientID
				)

		IF (@Property IS NULL)
		BEGIN
      print 'inserting housing property'
			--Insert Housing Property
			EXEC @Property = xpr_insert_housing_property @HousingID = @ClientID
				,@OwnerID = @Ownerid
				,@CoOwnerID = @Coownerid
				,@UseHomeAddress = @UsehomeAddress
				,@PropertyAddress = @PropertyAddressID
				,@Residency = 1
				,@PropertyType = 8
		END

		SET @Loan = (
				SELECT TOP 1 Oid
				FROM Housing_Loans
				WHERE Propertyid IS NOT NULL AND Propertyid = @Property
				)

		IF (@Loan IS NULL)
		BEGIN
			SET @InvestorID = 1

			IF (@InvestorNumber != '' AND @InvestorNumber IS NOT NULL)
				SET @InvestorID = 2
			SET @Lender = NULL

      print 'inserting housing lender'
			--Insert Housing Lender
			EXEC @Lender = xpr_insert_housing_lender @ServicerID = @Calcservicerid
				,@ServicerName = @Calcservicername
				,@AcctNum = @ServicerLoanNumber
				,@InvestorID = @InvestorID
				,@InvestorAccountNumber = @InvestorNumber
				,@ContactLenderDate = NULL
				,@AttemptContactDate = NULL
				,@ContactLenderSuccess = 0
				,@WorkoutPlanDate = NULL

      print 'set upb'
			--Insert Housing Loan
			SET @UPB = CASE WHEN isnumeric(@UPB) = 1 THEN CAST(@UPB AS MONEY) ELSE NULL END

      print 'insert housing loan'
			EXEC @Loan = xpr_insert_housing_loan @PropertyID = @Property
				,@Loan1st2nd = 1
				,@LoanDelinquencyMonths = 0
				,@UseInReports = 1
				,@CurrentLenderID = @Lender
				,@CurrentLoanBalanceAmt = @UPB
				,@LoanOriginationDate = NULL
		END

    print 'inserting upload record'
		--Insert into osc_uploadrecord_FMAE
		EXEC ocs_create_upload_record_FMAE @UploadedrecordID
			,@Agency_Name
			,@Backlog_Mod_Flag
			,@Trial_Mod
			,@Mod_Conversion_Date
			,@ACH_Flag
			,@Trial_Mod_Payment_Amount
			,@UPB
			,@Reason_For_Default
			,@Trial_Mod_Description
			,@Comment_Description
			,@TrialPaymentsReceivedAmount
			,@TrialPaymentsReceivedCount
			,@Last_Payment_Applied_Amount
			,@Workout_Type

		--Insert Note
		
      print 'insert borrower phone 1'
      -- INSERT UNUSED PHONES TO CLIENT NOTES
      IF (@Borrower_PhoneAreaCode1 IS NOT NULL AND @isBorrowerHomeUsed = 0)
  		BEGIN
			  SET @NoteText = @Borrower_PhoneAreaCode1 + '-' + @Borrower_PhoneNum1
			  EXEC [xpr_insert_client_notes] @ClientID, 'Fannie Mae Upload - Applicant extra phone number', @NoteText
	  	END

      print 'insert coborrower phone 1'
      IF (@CoBorrower_PhoneAreaCode1 IS NOT NULL AND @isCoborrowerHomeUsed = 0)
  		BEGIN
			  SET @NoteText = @CoBorrower_PhoneAreaCode1 + '-' + @CoBorrower_PhoneNum1
			  EXEC [xpr_insert_client_notes] @ClientID, 'Fannie Mae Upload - Coapplicant extra phone number', @NoteText
	  	END
      
      print 'insert borrower phone 2'
      IF (@Borrower_PhoneAreaCode2 IS NOT NULL AND @isBorrowerMsgUsed = 0)
  		BEGIN
			  SET @NoteText = @Borrower_PhoneAreaCode2 + '-' + @Borrower_PhoneNum2
			  EXEC [xpr_insert_client_notes] @ClientID, 'Fannie Mae Upload - Applicant extra phone number', @NoteText
	  	END
      
      print 'insert coborrower phone 2'
      IF (@CoBorrower_PhoneAreaCode2 IS NOT NULL AND @isCoborrowerMsgUsed = 0)
  		BEGIN
			  SET @NoteText = @CoBorrower_PhoneAreaCode2 + '-' + @CoBorrower_PhoneNum2
			  EXEC [xpr_insert_client_notes] @ClientID, 'Fannie Mae Upload - Coapplicant extra phone number', @NoteText
	  	END
      
      print 'insert borrower phone 3'
      IF (@Borrower_PhoneAreaCode3 IS NOT NULL AND @isAppCellphone1Used = 0)
  		BEGIN
			  SET @NoteText = @Borrower_PhoneAreaCode3 + '-' + @Borrower_PhoneNum3
			  EXEC [xpr_insert_client_notes] @ClientID, 'Fannie Mae Upload - Applicant extra phone number', @NoteText
	  	END
      
      print 'insert borrower phone 4'
      IF (@Borrower_PhoneAreaCode4 IS NOT NULL AND @isAppWorkphone1Used = 0)
  		BEGIN
			  SET @NoteText = @Borrower_PhoneAreaCode4 + '-' + @Borrower_PhoneNum4
			  EXEC [xpr_insert_client_notes] @ClientID, 'Fannie Mae Upload - Applicant extra phone number', @NoteText
	  	END
      
      print 'insert borrower phone 5'
      IF (@Borrower_PhoneAreaCode5 IS NOT NULL AND @isAppCellphone2Used = 0)
  		BEGIN
			  SET @NoteText = @Borrower_PhoneAreaCode5 + '-' + @Borrower_PhoneNum5
			  EXEC [xpr_insert_client_notes] @ClientID, 'Fannie Mae Upload - Applicant extra phone number', @NoteText
	  	END
      
      print 'insert borrower phone 6'
      IF (@Borrower_PhoneAreaCode6 IS NOT NULL AND @isAppWorkphone2Used = 0)
  		BEGIN
			  SET @NoteText = @Borrower_PhoneAreaCode6 + '-' + @Borrower_PhoneNum6
			  EXEC [xpr_insert_client_notes] @ClientID, 'Fannie Mae Upload - Applicant extra phone number', @NoteText
	  	END
      
      print 'insert coborrower phone 3'
      IF (@CoBorrower_PhoneAreaCode3 IS NOT NULL AND @isCoAppCellphone1Used = 0)
  		BEGIN
			  SET @NoteText = @CoBorrower_PhoneAreaCode3 + '-' + @CoBorrower_PhoneNum3
			  EXEC [xpr_insert_client_notes] @ClientID, 'Fannie Mae Upload - Coapplicant extra phone number', @NoteText
	  	END
      
      print 'insert coborrower phone 4'
      IF (@CoBorrower_PhoneAreaCode4 IS NOT NULL AND @isCoAppWorkphone1Used = 0)
  		BEGIN
			  SET @NoteText = @CoBorrower_PhoneAreaCode4 + '-' + @CoBorrower_PhoneNum4
			  EXEC [xpr_insert_client_notes] @ClientID, 'Fannie Mae Upload - Coapplicant extra phone number', @NoteText
	  	END
      
      print 'insert coborrower phone 5'
      IF (@CoBorrower_PhoneAreaCode5 IS NOT NULL AND @isCoAppCellphone2Used = 0)
  		BEGIN
			  SET @NoteText = @CoBorrower_PhoneAreaCode5 + '-' + @CoBorrower_PhoneNum5
			  EXEC [xpr_insert_client_notes] @ClientID, 'Fannie Mae Upload - Coapplicant extra phone number', @NoteText
	  	END
      
      print 'insert coborrower phone 6'
      IF (@CoBorrower_PhoneAreaCode6 IS NOT NULL AND @isCoAppWorkphone2Used = 0)
  		BEGIN
			  SET @NoteText = @CoBorrower_PhoneAreaCode6 + '-' + @CoBorrower_PhoneNum6
			  EXEC [xpr_insert_client_notes] @ClientID, 'Fannie Mae Upload - Coapplicant extra phone number', @NoteText
	  	END

		COMMIT TRANSACTION
	END TRY

	BEGIN CATCH
		DECLARE @ErrorMessage NVARCHAR(4000);
		DECLARE @ErrorSeverity INT;
		DECLARE @ErrorState INT;

		SELECT @ErrorMessage = ERROR_MESSAGE()
			,@ErrorSeverity = ERROR_SEVERITY()
			,@ErrorState = ERROR_STATE();
    print @ErrorMessage

		RAISERROR (
				@ErrorMessage -- Message text.
				,@ErrorSeverity -- Severity.
				,@ErrorState -- State.
				);

		ROLLBACK TRANSACTION
	END CATCH

	SET @Inserted = @ClientID
END

GO