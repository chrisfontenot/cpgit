USE [DebtPlus]
GO
/****** Object:  StoredProcedure [dbo].[rpt_custom_HUD_results_byHousingApptType]    Script Date: 09/15/2014 13:13:33 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[rpt_custom_HUD_results_byHousingApptType]  ( @from_date as datetime = null, @to_date as datetime = null ) as

-- =============================================================================================================================
-- ==            Custom report for initial housing appointments                                                               ==
-- =============================================================================================================================

set nocount on

if @to_date is null
	select	@to_date	= getdate();
	
if @from_date is null
	select	@from_date	= @to_date;
	
select	@from_date	= convert(varchar(10), @from_date, 101),
		@to_date	= convert(varchar(10), dateadd(d, 1, @to_date), 101);

select	client, client_appointment, convert(int,0) as minutes, convert(int,null) as hud_interview
into	#appts
from	client_appointments ca
inner join appt_types a on ca.appt_type = a.appt_type
where	ca.status in ('K','W')
and		ca.office is not null
and		a.housing		= 1
and		a.initial_appt	= 1
and		ca.start_time >= @from_date
and		ca.start_time <  @to_date

select	t.client_appointment, t.hud_interview, sum(t.minutes) as minutes
into	#times
from	hud_transactions t
inner join #appts ca on t.client_appointment = ca.client_appointment
group by t.client_appointment, t.hud_interview

update	#appts
set		minutes	 = x.minutes,
		hud_interview = x.hud_interview
from	#appts a
inner join #times x on a.client_appointment = x.client_appointment;

select	v.client				as 'ClientID',
		v.name					as 'ClientName',
		
		x.client_appointment	as 'AppointmentID',
		ca.start_time			as 'StartTimeDT',
		ca.office				as 'AppointmentOfficeID',
		ca.counselor			as 'AppointmentCounselorID',
		ca.result				as 'AppointmentResultCD',
		datediff(minute, ca.start_time, ca.end_time) as 'AppointmentDuration',
		
		o.name					as 'OfficeName',
		cox.name				as 'CounselorName',

		apt.appt_name			as 'AppointmentType',
		
		x.hud_interview			as 'HousingInterviewID',
		x.minutes				as 'HousingDuration',
		
		i.interview_type		as 'InterviewTypeID',
		i.interview_date		as 'InterviewDT',
		i.hud_result			as 'ResultCD',
		i.result_date			as 'ResultDT',
		i.termination_reason	as 'TerminationCD',
		i.termination_date		as 'TerminationDT',
		
		mi.description			as 'InterviewType',
		mr.description			as 'ResultType',
		mt.description			as 'TerminationType'
		
from	#appts x
inner join view_client_address v on x.client = v.client
inner join client_appointments ca on x.client_appointment = ca.client_appointment
inner join appt_types apt on ca.appt_type = apt.appt_type
inner join offices o on ca.office = o.office
inner join view_counselors cox on ca.counselor = cox.counselor
inner join hud_interviews i on x.hud_interview = i.hud_interview
left outer join Housing_PurposeOfVisitTypes mi on i.interview_type = oID
left outer join Housing_ResultTypes mr on i.hud_result = mr.oID
left outer join Housing_TerminationReasonTypes mt on i.termination_reason = mt.oID

where	x.hud_interview is not null

drop table #appts
drop table #times

return ( @@rowcount )
GO
