IF NOT EXISTS(SELECT * FROM SYSOBJECTS WHERE TYPE='P' AND NAME=N'UPDATE_drop_constraints')
	EXEC('CREATE PROCEDURE [dbo].[UPDATE_drop_constraints] AS')
GO
ALTER PROCEDURE [dbo].[UPDATE_drop_constraints] ( @table varchar(256) ) as

-- Abort early if the table does not exist
if not exists(select * from sysobjects where type = 'U' and name = @table)
	return

-- Drop all pending constraints on a table. This does not care what they are. They are all dropped.
declare	constraint_cursor cursor for
	select	[name]
	from	sysobjects
	where	type in ('D','C')
	and	parent_obj = (
		select	id
		from	sysobjects
		where	type = 'U'
		and	[name] = @table
	)

declare	@name	varchar(256)
declare	@stmt	varchar(512)

open constraint_cursor
fetch constraint_cursor into @name

while @@fetch_status = 0
begin
	select @stmt = 'alter table [' + @table + '] drop constraint [' + @name + '];'
	print	@stmt
	exec ( @stmt )
	fetch constraint_cursor into @name
end

close constraint_cursor
deallocate constraint_cursor

-- Find the foreign key relationships to the tables so that we may drop the table
DECLARE @SQL NVARCHAR(max);
WITH fkeys AS (
    SELECT quotename(s.name) + '.' + quotename(o.name) tablename, quotename(fk.name) constraintname 
    FROM sys.foreign_keys fk
    JOIN sys.objects o ON fk.parent_object_id = o.object_id AND o.name = @TABLE
    JOIN sys.schemas s ON o.schema_id = s.schema_id
	UNION
    SELECT quotename(s.name) + '.' + quotename(ro.name) tablename, quotename(fk.name) constraintname 
    FROM sys.foreign_keys fk
    JOIN sys.objects o ON fk.referenced_object_id = o.object_id AND o.name = @TABLE
    JOIN sys.objects ro ON fk.parent_object_id = ro.object_id
    JOIN sys.schemas s ON o.schema_id = s.schema_id
)
-- Generate the statements
SELECT @SQL = STUFF((SELECT '; ALTER TABLE ' + tablename + ' DROP CONSTRAINT ' + constraintname
FROM fkeys
FOR XML PATH('')),1,2,'')

-- Execute the results
if isnull(@SQL,'') <> ''
BEGIN
	PRINT @SQL
	EXEC (@SQL)
END
GO
