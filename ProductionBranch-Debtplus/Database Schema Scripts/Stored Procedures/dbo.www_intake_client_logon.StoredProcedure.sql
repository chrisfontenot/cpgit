USE [DebtPlus]
GO
/****** Object:  StoredProcedure [dbo].[www_intake_client_logon]    Script Date: 09/15/2014 13:13:34 ******/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE PROCEDURE [dbo].[www_intake_client_logon] ( @account_name as varchar(80) = null, @password as varchar(80) = null ) as

	-- The client logon is not valid since we don't do this here now.
	select	-1			as 'intake_client',
	convert(varchar(20), '')	as 'intake_id'
	return ( -1 )
GO
