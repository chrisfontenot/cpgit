USE [DebtPlus]
GO
/****** Object:  StoredProcedure [dbo].[www_intake_secured_property_reset]    Script Date: 09/15/2014 13:13:34 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[www_intake_secured_property_reset] ( @intake_client as int ) as

-- Remove the loans for the debts
delete	intake_secured_loans
from	intake_secured_loans l
inner join intake_secured_properties p on l.secured_property = p.secured_property
where	intake_client	= @intake_client

-- Remove the property
delete
from	intake_secured_properties
where	intake_client	= @intake_client
GO
