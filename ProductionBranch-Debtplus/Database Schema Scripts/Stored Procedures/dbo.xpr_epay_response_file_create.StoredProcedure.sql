USE [DebtPlus]
GO
/****** Object:  StoredProcedure [dbo].[xpr_epay_response_file_create]    Script Date: 09/15/2014 13:13:35 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[xpr_epay_response_file_create] (@label varchar(80) = null) as
insert into epay_response_files (label) values (@label)
return (scope_identity())
GO
