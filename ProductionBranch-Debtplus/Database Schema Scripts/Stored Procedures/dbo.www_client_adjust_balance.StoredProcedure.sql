USE [DebtPlus]
GO
/****** Object:  StoredProcedure [dbo].[www_client_adjust_balance]    Script Date: 09/15/2014 13:13:34 ******/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE PROCEDURE [dbo].[www_client_adjust_balance] ( @client as int, @client_creditor as int, @new_balance as money = NULL ) AS
-- =============================================================================================================
-- ==            Adjust the balance record for the debt                                                       ==
-- =============================================================================================================

-- ChangeLog
--   9/31/2002
--     Added support for client_creditor_balances table
--   06/09/2009
--      Remove references to debt_number and use client_creditor

-- Suppress intermediate results
set nocount on

-- Fetch the balance information for the debt
declare	@old_balance	money
declare	@active_status	varchar(3)
declare	@creditor	varchar(10)

begin transaction

select	@active_status	= active_status
from	clients
where	client = @client

if @@rowcount < 1
begin
	rollback transaction
	RaisError ('The client %d is not valid in the system', 16, 1, @client)
	return ( 0 )
end

if @active_status not in ('PND', 'RDY', 'PRO', 'A', 'AR', 'EX')
begin
	rollback transaction
	RaisError ('The client %d is not valid in the system at this time', 16, 1, @client)
end

-- Fetch the debt balance
select		@old_balance		= isnull(bal.orig_balance,0) + isnull(bal.orig_balance_adjustment,0) + isnull(bal.total_interest, 0) - isnull(bal.total_payments,0),
		@creditor		= creditor
from		client_creditor cc
inner join	client_creditor_balances bal on cc.client_creditor_balance = bal.client_creditor_balance
where		cc.client_creditor	= @client_creditor
and		cc.client		= @client

if @@rowcount < 0
begin
	rollback transaction
	RaisError ('The client %d is not valid in the system', 16, 1, @client)
	return ( 0 )
end

-- Update the balance if the figures differ
if @old_balance != @new_balance
begin
	-- Update the balance information
	update	client_creditor_balances
	set		orig_balance_adjustment = @new_balance - bal.orig_balance - bal.total_interest + bal.total_payments
	from	client_creditor_balances bal
	inner join client_creditor cc on bal.client_creditor_balance = cc.client_creditor_balance
	where	cc.client_creditor	= @client_creditor

	/* -- Removed at Orange County's request
	update	client_creditor
	set		balance_verify_date	= getdate(),
			balance_verify_by	= 'www client'
	where	client_creditor		= @client_creditor
	*/
	
	-- Generate the system note that the balance was updated
	declare	@note_message		varchar(4000)
	select	@note_message		= 'The balance for the debt ' +
					  dbo.format_client_id ( @client ) + '*' + @creditor +
					  ' was changed from $' + convert(varchar, @old_balance, 1) +
					  ' to a new balance of $' + convert(varchar, @new_balance, 1) +
					  ' by the client using the web site interface.'

	insert into client_notes (client, client_creditor, is_text, dont_edit, dont_print, dont_delete, subject, note, created_by)
	values (@client, @client_creditor, 1, 0, 0, 0, 'Balance adjusted from web site', @note_message, 'www user')
end

-- Complete the transaction and return success
commit transaction
return ( 1 )
GO
