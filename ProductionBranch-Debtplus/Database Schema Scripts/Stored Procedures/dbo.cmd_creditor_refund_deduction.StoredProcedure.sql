USE [DebtPlus]
GO
/****** Object:  StoredProcedure [dbo].[cmd_creditor_refund_deduction]    Script Date: 09/15/2014 13:13:31 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[cmd_creditor_refund_deduction] ( @client_creditor AS Int, @Amount AS Money = 0 ) AS

-- ===================================================================================================================
-- ==            Correct for a creditor refund which should have included the deduction amount                      ==
-- ===================================================================================================================

-- ChangeLog
--   9/31/2002
--     Added support for client_creditor_balances table
--   10/15/2002
--     Added support for client_creditor's "*_this_creditor" information
--   06/09/2009
--      Remove references to debt_number and use client_creditor

-- Disable intermediate result sets
SET NOCOUNT ON

-- Local storage
DECLARE	@p1		VarChar(80)
declare	@Client		int
declare	@Creditor	varchar(10)

-- Start a transaction
BEGIN TRANSACTION

select	@client		= client,
		@creditor	= creditor
from	client_creditor
where	client_creditor	= @client_creditor;

-- Validate the amount
IF @Amount <= 0
BEGIN
	RaisError(50019, 16, 1 )
	RollBack Transaction
	Return ( 0 )
END

-- Ensure that the creditor and the client are valid
IF @client is null or @creditor is null
BEGIN
	RaisError(50060, 16, 1, @client )
	RollBack Transaction
	Return ( 0 )
END

-- Ensure that the creditor is a legal creditor
IF NOT EXISTS ( SELECT creditor FROM creditors WHERE creditor = @Creditor AND prohibit_use = 0 )	
BEGIN
	RaisError(50069, 16, 1, @creditor )
	RollBack Transaction
	Return ( 0 )
END

-- Correct the debt record to indicate that the balance was returned
insert into registers_client_creditor	(tran_type,	client,		creditor,	client_creditor,	credit_amt)
values					('RF',		@client,	@creditor,	@client_creditor,	@amount)

-- Adjust the client's trust balance with the transferred amount
update	clients
set	held_in_trust = held_in_trust + @amount
where	client = @client

-- Indicate that this is a refund transaction from the client 0 trust account
insert into registers_client	(tran_type,	client,		debit_amt,	message)
values				('RF',		0,		@amount,	'Correction for Creditor Refund')

-- Remove the amount from the client 0 trust balance
update	clients
set	held_in_trust = held_in_trust - @amount
where	client = 0

-- This is a "refund" to the client's trust account log
insert into registers_client	(tran_type,	client,		credit_amt,	message)
values				('RF',		@client,	@amount,	'Correction for Creditor Refund')

update	client_creditor_balances
set	total_payments			= bal.total_payments - @amount,
	payments_month_0		= bal.payments_month_0 - @amount
from	client_creditor_balances bal
inner join client_creditor cc on cc.client_creditor_balance = bal.client_creditor_balance
where	cc.client_creditor			= @client_creditor

-- Count the refund amount for the current instance of the debt
update	client_creditor
set	returns_this_creditor		= isnull(returns_this_creditor,0) + @amount
where	client_creditor			= @client_creditor

-- Return success
COMMIT TRANSACTION
RETURN ( 1 )
GO
