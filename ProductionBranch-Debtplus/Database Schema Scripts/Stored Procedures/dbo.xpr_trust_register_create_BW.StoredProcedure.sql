SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER procedure [xpr_trust_register_create_BW] ( @creditor as typ_creditor, @amount AS Money = 0, @item_date AS DateTime = NULL, @cleared AS VarChar(1) = NULL, @checknum as BigInt = NULL, @bank as int = 2 ) AS

-- ====================================================================================================
-- ==   Create a check in the trust register for paying this creditor on an automatic disbursement   ==
-- ====================================================================================================

SET NOCOUNT ON
declare	@trust_register	int

-- Insert the item into the trust register
-- The "BW" transaction does not have a creditor.
INSERT INTO registers_trust	(tran_type,	amount,	cleared,	check_order,	bank)
VALUES				('BW',		0,	' ',		9,		@bank)

SELECT	@trust_register = SCOPE_IDENTITY()

-- Set the check number as a fake value to indicate the presence of a check number.
update	registers_trust
set		checknum		= @trust_register
where	trust_register	= @trust_register

-- Return the trust register ID to the caller
return ( @trust_register )
GO
