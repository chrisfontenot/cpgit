USE [DebtPlus]
GO
/****** Object:  StoredProcedure [dbo].[xpr_creditor_methods_list_masks]    Script Date: 09/15/2014 13:13:34 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[xpr_creditor_methods_list_masks] ( @creditor as int, @method as varchar(10) = 'EFT' ) AS

-- ====================================================================================================
-- ==            Return the list of biller masks for the ids associated with the methods             ==
-- ====================================================================================================

-- ChangeLog
--   1/5/2009
--      Changed from using biller_aba to using rpps_biller_id

set nocount on

select	distinct 'RPPS'					as type,
	m.rpps_biller_id			as biller_id,
	rpps.mask				as mask,
	convert(varchar(10),rpps.description)	as notes

from	creditor_methods m
inner join rpps_biller_ids ids on m.rpps_biller_id = ids.rpps_biller_id
inner join rpps_masks rpps on ids.rpps_biller_id = rpps.rpps_biller_id
inner join banks b on m.bank = b.bank

where	m.creditor		= @creditor
and	m.type			= @method
and	m.rpps_biller_id is not null
and	b.type			= 'R'

order by 1, 2, 3

return ( @@rowcount )
GO
