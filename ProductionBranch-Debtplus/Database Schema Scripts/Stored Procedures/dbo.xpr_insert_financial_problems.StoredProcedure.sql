USE [DebtPlus]
GO
/****** Object:  StoredProcedure [dbo].[xpr_insert_financial_problems]    Script Date: 09/15/2014 13:13:35 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[xpr_insert_financial_problems] ( @description as varchar(50), @rpps_code as varchar(4) = null, @nfcc as varchar(4) = null, @note as varchar(256) = null ) as
-- ========================================================================================
-- ==   Procedure for .NET to insert items into the job descriptions table               ==
-- ========================================================================================
	insert into financial_problems ( [description], [nfcc], [rpps_code], [note] ) values ( @description, @nfcc, @rpps_code, @note )
	return ( scope_identity() )
GO
