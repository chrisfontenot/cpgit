USE [DebtPlus]
GO
/****** Object:  StoredProcedure [dbo].[xpr_disbursement_general_info]    Script Date: 09/15/2014 13:13:35 ******/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE PROCEDURE [dbo].[xpr_disbursement_general_info] AS

-- =================================================================================================================
-- ==            Retrieve the information for starting the disbursement process                                   ==
-- =================================================================================================================

SELECT	isnull(payments_pad_less,0)	as 'payments_pad_less',
	isnull(payments_pad_greater,0)	as 'payments_pad_greater',
	isnull(paf_creditor,'')		as 'paf_creditor',
	0				as 'fee_cap'
FROM	config

RETURN ( @@rowcount )
GO
