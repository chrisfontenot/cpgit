USE [DebtPlus]
GO
/****** Object:  StoredProcedure [dbo].[lst_clients_newclient]    Script Date: 09/15/2014 13:13:32 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE PROCEDURE [dbo].[lst_clients_newclient] ( @HomeTelephone AS VarChar(20) ) AS
-- =============================================================================================================
-- ==            return the list of clients who match the home telephone number                               ==
-- =============================================================================================================

-- Suppress intermediate results
set nocount on;

-- Remove the non-digits from the number
select	@HomeTelephone = dbo.numbers_only( @HomeTelephone )

-- Find the area code from the number
declare	@Acode			varchar(4)
declare	@Number			varchar(20)

if LEN(@HomeTelephone) = 10
	select	@Acode		= LEFT(@HomeTelephone, 3)

if isnull(@Acode,'000') = '000'
	select	@Acode		= isnull(AreaCode,'000')
	from	config

-- Find the number
select	@Number			= RIGHT(@HomeTelephone, 7);

-- If there is an area code then use it in the searc
if @Acode <> '000'
	SELECT	c.client			as 'client',
		c.active_status			as 'active_status',
		p.ssn					as 'ssn',
		dbo.format_address_line_1(a.house, a.direction, a.street, a.suffix, a.modifier, a.modifier_value) as 'address1',
		a.address_line_2		as 'address2',
		dbo.format_city_state_zip ( a.city, a.state, a.postalcode ) as 'address3',
		dbo.format_TelephoneNumber(c.HomeTelephoneID) as home_ph,
		dbo.format_normal_name (pn.prefix, pn.first, pn.middle, pn.last, pn.suffix) as 'name'
	
	FROM		clients c	WITH (NOLOCK)
	left outer join	people p	WITH (NOLOCK) on c.client = p.client and 1 = p.relation
	left outer join addresses a with (nolock) on c.addressid = a.address
	left outer join names pn with (nolock) on p.nameid = pn.name
	left outer join TelephoneNumbers t with (nolock) on c.HomeTelephoneID = t.TelephoneNumber
	where	t.acode = @Acode and replace(t.number,'-','') = @Number
else
	SELECT	c.client			as 'client',
		c.active_status			as 'active_status',
		p.ssn					as 'ssn',
		dbo.format_address_line_1(a.house, a.direction, a.street, a.suffix, a.modifier, a.modifier_value) as 'address1',
		a.address_line_2		as 'address2',
		dbo.format_city_state_zip ( a.city, a.state, a.postalcode ) as 'address3',
		dbo.format_TelephoneNumber(c.HomeTelephoneID) as home_ph,
		dbo.format_normal_name (pn.prefix, pn.first, pn.middle, pn.last, pn.suffix) as 'name'
	
	FROM		clients c	WITH (NOLOCK)
	left outer join	people p	WITH (NOLOCK) on c.client = p.client and 1 = p.relation
	left outer join addresses a with (nolock) on c.addressid = a.address
	left outer join names pn with (nolock) on p.nameid = pn.name
	left outer join TelephoneNumbers t with (nolock) on c.HomeTelephoneID = t.TelephoneNumber
	where	replace(t.number,'-','') = @Number

return ( @@rowcount )
GO
