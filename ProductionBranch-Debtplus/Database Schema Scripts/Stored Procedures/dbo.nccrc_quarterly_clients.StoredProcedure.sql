USE [DebtPlus]
GO
/****** Object:  StoredProcedure [dbo].[nccrc_quarterly_clients]    Script Date: 09/15/2014 13:13:32 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE PROCEDURE [dbo].[nccrc_quarterly_clients]  ( @period_start as datetime = null, @period_end as datetime = null ) as

create table #results (sheet varchar(80), location varchar(80), value varchar(80));
create unique index ix1_results on #results ( location );

-- Build a list of the counseling sessions for the clients in the date range indicated
select	ca.client_appointment			as client_appointment,
	isnull(ca.result,'XXX')			as result,
	ca.client				as client,
	ca.start_time				as start_time,
	isnull(ca.appt_type,0)			as appt_type,
	isnull(a.housing,0)			as housing,
	isnull(a.bankruptcy,0)			as bankruptcy,
	isnull(a.initial_appt,0)		as initial_appt,
	isnull(ca.office,0)			as office,
	isnull(ca.counselor,0)			as counselor,
	convert(varchar(4), null)		as appt_cell,
	isnull(ca.bankruptcy_class, -1)		as result_bankruptcy_class,
	isnull(c.bankruptcy_class, -1)		as starting_bankruptcy_class

into	#appts
from	client_appointments ca with (nolock)
inner join clients c on ca.client = c.client
left outer join appt_types a on ca.appt_type = a.appt_type
where	ca.status in ('K','W')
and	ca.start_time between @period_start and @period_end
and 	ca.office is not null

-- Build a list of the clients in these appointments
select	c.client,
	isnull(convert(float, datediff(yy, p.birthdate,
	getdate())),0.0) as years,
	p.gender,
	p.race,
	isnull(p.hispanic,0) as hispanic,
	c.marital_status,
	c.dependents + (case c.marital_status when 2 then 2 else 1 end),
	c.cause_fin_problem1,
	c.active_status,
	c.program_months,
	isnull(p.education,0) as education,
	left(isnull(st.mailingcode,'CA'),2) as state,
	isnull(prob.nfcc,'XXX') as cause,
	dr.nfcc as drop_reason,
	dr.rpps_code as rpps_drop_reason,
	
	convert(int,4) as ami_group,
	isnull(county.median_income,0) as median_income,
	convert(money,0) as ami,

	-- Values to be supplied later
	convert(varchar(4), '0') as job,
	convert(int,0) as age_grp,
	convert(varchar(4), null) as appt_cell,
	convert(money,0) as gross,
	convert(money,0) as assets, convert(money,0) as other_debt,
	convert(money,0) as secured_debt, convert(money,0) as unsecured_debt,
	convert(float,0) as ratio,
	convert(int,case c.bankruptcy_class when 0 then 0 when -1 then 0 else 1 end) as bankruptcy_group

into	#clients
from	clients c with (nolock)
left outer join addresses ca with (nolock) on c.AddressID = ca.address
left outer join states st with (nolock) on ca.state = st.state
left outer join financial_problems prob with (nolock) on c.cause_fin_problem1 = prob.financial_problem
left outer join people p with (nolock) on c.client = p.client and 1 = p.relation
-- left outer join job_descriptions j with (nolock) on p.job = j.job_description
left outer join drop_reasons dr with (nolock) on c.drop_reason = dr.drop_reason
left outer join counties county with (nolock) on c.county = county.county
where	c.client in (select client from #appts);

-- Fill in the grouping for the bankruptcy information
update	#clients
set		bankruptcy_group = bankruptcy_group + 2
from	#clients c
inner join #appts a on c.client = a.client
where	a.result_bankruptcy_class > 0
and		c.bankruptcy_group in (0, 1);

-- Update the job information based upon one of the client's jobs
update	#clients
set		job	= j.nfcc
from	#clients c
inner join people p on c.client = p.client and 1 = p.person
-- inner join people_jobs pj on p.client = pj.client and p.person = pj.person
-- inner join job_descriptions j on pj.job = j.job_description
inner join job_descriptions j on p.job = j.job_description
where	j.nfcc is not null

-- Breakdown of clients by appointment types
update	#clients
set	appt_cell = case state
			when 'AK' then '75'
			when 'AL' then '76'
			when 'AR' then '77'
			when 'AZ' then '78'
			when 'CA' then '79'
			when 'CO' then '80'
			when 'CT' then '81'
			when 'DC' then '82'
			when 'DE' then '83'
			when 'FL' then '84'
			when 'GA' then '85'
			when 'HI' then '86'
			when 'IA' then '87'
			when 'ID' then '88'
			when 'IL' then '89'
			when 'IN' then '90'
			when 'KS' then '91'
			when 'KY' then '92'
			when 'LA' then '93'
			when 'MA' then '94'
			when 'MD' then '95'
			when 'ME' then '96'
			when 'MI' then '97'
			when 'MN' then '98'
			when 'MO' then '99'
			when 'MS' then '100'
			when 'MT' then '101'
			when 'NC' then '102'
			when 'ND' then '103'
			when 'NE' then '104'
			when 'NH' then '105'
			when 'NJ' then '106'
			when 'NM' then '107'
			when 'NV' then '108'
			when 'NY' then '109'
			when 'OH' then '110'
			when 'OK' then '111'
			when 'OR' then '112'
			when 'PA' then '113'
			when 'PR' then '114'
			when 'RI' then '115'
			when 'SC' then '116'
			when 'SD' then '117'
			when 'TN' then '118'
			when 'TX' then '119'
			when 'UT' then '120'
			when 'VA' then '121'
			when 'VT' then '122'
			when 'WA' then '123'
			when 'WI' then '124'
			when 'WV' then '125'
			when 'WY' then '126'
		end

-- Find the default state
declare	@default_state	varchar(10)

select	@default_state	= mailingcode
from	states st
where	[default] = 1

if @default_state is null
	select	@default_state = mailingcode
	from	states st
	where	st.state = (
			select	MIN(state)
			from	states
	)
	
update	#clients
set	state = @default_state
where	appt_cell is null;

-- Fill in the cell locations for the missing items
update	#clients
set	appt_cell = case state
			when 'AK' then '75'
			when 'AL' then '76'
			when 'AR' then '77'
			when 'AZ' then '78'
			when 'CA' then '79'
			when 'CO' then '80'
			when 'CT' then '81'
			when 'DC' then '82'
			when 'DE' then '83'
			when 'FL' then '84'
			when 'GA' then '85'
			when 'HI' then '86'
			when 'IA' then '87'
			when 'ID' then '88'
			when 'IL' then '89'
			when 'IN' then '90'
			when 'KS' then '91'
			when 'KY' then '92'
			when 'LA' then '93'
			when 'MA' then '94'
			when 'MD' then '95'
			when 'ME' then '96'
			when 'MI' then '97'
			when 'MN' then '98'
			when 'MO' then '99'
			when 'MS' then '100'
			when 'MT' then '101'
			when 'NC' then '102'
			when 'ND' then '103'
			when 'NE' then '104'
			when 'NH' then '105'
			when 'NJ' then '106'
			when 'NM' then '107'
			when 'NV' then '108'
			when 'NY' then '109'
			when 'OH' then '110'
			when 'OK' then '111'
			when 'OR' then '112'
			when 'PA' then '113'
			when 'PR' then '114'
			when 'RI' then '115'
			when 'SC' then '116'
			when 'SD' then '117'
			when 'TN' then '118'
			when 'TX' then '119'
			when 'UT' then '120'
			when 'VA' then '121'
			when 'VT' then '122'
			when 'WA' then '123'
			when 'WI' then '124'
			when 'WV' then '125'
			when 'WY' then '126'
		end
where	appt_cell is null;

-- Fill in the appointment locations
update	#appts
set	appt_cell	= case
		when	bankruptcy = 0 and initial_appt	= 0 and housing = 0 then 'C'
		when	bankruptcy = 0 and initial_appt	= 0 and housing = 1 then 'E'
		when	bankruptcy = 0 and initial_appt	= 1 and housing = 0 then 'B'
		when	bankruptcy = 0 and initial_appt	= 1 and housing = 1 then 'D'
		when	bankruptcy = 1 and initial_appt	= 0 and housing = 0 then 'F'
		when	bankruptcy = 1 and initial_appt	= 0 and housing = 1 then 'E'
		when	bankruptcy = 1 and initial_appt	= 1 and housing = 0 then 'F'
		when	bankruptcy = 1 and initial_appt	= 1 and housing = 1 then 'D'
	end + c.appt_cell
from	#appts a
inner join #clients c on a.client = c.client;

-- Discard the clients where the state is not valid
delete
from	#clients
where	appt_cell is null;

delete
from	#appts
where	appt_cell is null;

-- Appointment types by state
insert into #results (sheet, location, value)
select	'Sheet1', appt_cell, count(*)
from	#appts
group by appt_cell
order by appt_cell;

-- ------------------- FROM THIS POINT FORWARD, WE COUNT ONLY INITIAL APPOINTMENTS IN THE PERIOD -----------
-- -- If fields are to be based upon the clients in the database, they must be put before here !! ----------

delete
from	#appts
where	initial_appt = 0;

delete
from	#clients
where	client not in (select client from #appts)

/*
-- Supply the bankruptcy information
insert into #results (sheet, location, value)
select 'Sheet1' as sheet, 'F' + appt_cell, count(*)
from	#clients
where	bankruptcy_group in (1, 3)
group by appt_cell;

insert into #results (sheet, location, value)
select 'Sheet1' as sheet, 'G' + appt_cell, count(*)
from	#clients
where	bankruptcy_group in (2, 3)
group by appt_cell;
*/

-- Update the gross income
select	c.client, sum(gross_income) as gross
into	#gross
from	people p with (nolock)
-- from people_jobs p with (nolock)
inner join #clients c on p.client = c.client
group by c.client;

update	#clients
set	gross = x.gross
from	#clients c
inner join #gross x on c.client = x.client;

-- Find the other income sources
select  a.client, sum(a.asset_amount) as asset_amount
into	#assets
from	assets a with (nolock)
inner join #clients c on a.client = c.client
group by a.client;

update	#clients
set	assets = a.asset_amount
from	#clients c
inner join #assets a on c.client = a.client

-- Find the unsecured debt
select  cc.client, sum(b.orig_balance + b.orig_balance_adjustment) as unsecured_debt
into	#unsecured_debt
from	client_creditor cc
inner join client_creditor_balances b on cc.client_creditor_balance = b.client_creditor_balance and cc.client_creditor = b.client_creditor
inner join creditors cr on cc.creditor = cr.creditor
inner join creditor_classes ccl on cr.creditor_class = ccl.creditor_class
inner join #clients c on cc.client = c.client
where	isnull(ccl.agency_account,0) = 0
group by cc.client;

update	#clients
set	unsecured_debt = a.unsecured_debt
from	#clients c
inner join #unsecured_debt a on c.client = a.client

-- Find the secured debt
select  p.client, sum(l.balance) as secured_debt
into	#secured_debt
from	secured_properties p
inner join secured_loans l on p.secured_property = l.secured_property
group by p.client;

update	#clients
set	secured_debt = a.secured_debt
from	#clients c
inner join #secured_debt a on c.client = a.client

-- Find the other debt
select  o.client, sum(o.balance) as other_debt
into	#other_debt
from	client_other_debts o
inner join #clients c on o.client = c.client
group by o.client;

update	#clients
set	other_debt = a.other_debt
from	#clients c
inner join #other_debt a on c.client = a.client

-- Set the group code for the various locations
update	#clients
set	age_grp = 1
where	years between 1 and 17;

update	#clients
set	age_grp = 2
where	years between 18 and 24;

update	#clients
set	age_grp = 3
where	years between 25 and 34;

update	#clients
set	age_grp = 4
where	years between 35 and 44;

update	#clients
set	age_grp = 5
where	years between 45 and 54;

update	#clients
set	age_grp = 6
where	years between 55 and 64;

update	#clients
set	age_grp = 7
where	years >= 65;

insert into #results (sheet, location, value)
select	'Sheet1',
	case
		when age_grp = 1 then 'B152'
		when age_grp = 2 then 'B153'
		when age_grp = 3 then 'B154'
		when age_grp = 4 then 'B155'
		when age_grp = 5 then 'B156'
		when age_grp = 6 then 'B157'
		when age_grp = 7 then 'B158'
				 else 'B159'
	end as location,

	count(*) as value
from	#clients
group by age_grp;

-- Find the number of genders
insert into #results (sheet, location, value)
select	'Sheet1',
	'B162',
	count(*) from #clients where gender = 1;

insert into #results (sheet, location, value)
select	'Sheet1',
	'B163',
	count(*) from #clients where gender = 2;

-- Find the marital status
insert into #results (sheet, location, value)
select	'Sheet1',
	'B167',
	count(*) from #clients where marital_status = 1;

insert into #results (sheet, location, value)
select	'Sheet1',
	'B168',
	count(*) from #clients where marital_status = 2;

insert into #results (sheet, location, value)
select	'Sheet1',
	'B169',
	count(*) from #clients where marital_status = 5;

insert into #results (sheet, location, value)
select	'Sheet1',
	'B171',
	count(*) from #clients where marital_status = 3;

insert into #results (sheet, location, value)
select	'Sheet1',
	'B172',
	count(*) from #clients where marital_status = 4;


-- Average number in household
insert into #results (sheet, location, value)
select	'Sheet1', 'B175', round(avg(convert(float,people)),0)
from	#clients;

-- Determine the session type
insert into #results (sheet, location, value)
select	'Sheet1', 'B130', count(apt.appt_type)
from	#appts a
inner join appt_types apt on a.appt_type = apt.appt_type
left outer join FirstContactTypes m on apt.contact_type = m.oID
where	m.nfcc = 'F'
and		apt.housing = 0
and		apt.bankruptcy = 0
and		apt.initial_appt = 1

insert into #results (sheet, location, value)
select	'Sheet1', 'B131', count(apt.appt_type)
from	#appts a
inner join appt_types apt on a.appt_type = apt.appt_type
left outer join FirstContactTypes m on apt.contact_type = m.oID
where	m.nfcc = 'P'
and	apt.housing = 0
and	apt.bankruptcy = 0
and	apt.initial_appt = 1;

insert into #results (sheet, location, value)
select	'Sheet1', 'B132', count(apt.appt_type)
from	#appts a
inner join appt_types apt on a.appt_type = apt.appt_type
left outer join FirstContactTypes m on apt.contact_type = m.oID
where	m.nfcc in ('I','E')
and	apt.housing = 0
and	apt.bankruptcy = 0
and	apt.initial_appt = 1;

insert into #results (sheet, location, value)
select	'Sheet1', 'B133', count(apt.appt_type)
from	#appts a
inner join appt_types apt on a.appt_type = apt.appt_type
left outer join FirstContactTypes m on apt.contact_type = m.oID
where	m.nfcc = 'M'
and	apt.housing = 0
and	apt.bankruptcy = 0
and	apt.initial_appt = 1;

-- Clients enrolled in DMP
insert into #results (sheet, location, value)
select	'Sheet1', 'B137', count(*)
from	#appts a
where	result = 'DMP';

-- Clients referred to legal assistance (bankruptcy)
insert into #results (sheet, location, value)
select	'Sheet1', 'B138', count(*)
from	#appts a
where	result = 'RLA';

-- Clients referred to other agencies
insert into #results (sheet, location, value)
select	'Sheet1', 'B139', count(*)
from	#appts a
where	result = 'ROA';

-- Clients self administering
insert into #results (sheet, location, value)
select	'Sheet1', 'B140', count(*)
from	#appts a
where	result = 'CCH';

-- Clients self administering
insert into #results (sheet, location, value)
select	'Sheet1', 'B141', count(*)
from	#appts a
where	result not in ('CCH','ROA','RTL','DMP')

-- Ethnic background
insert into #results (sheet, location, value)
select	'Sheet1', 'B191', count(*)
from	#clients
where	race = 4		-- hispanic
or	hispanic = 1

insert into #results (sheet, location, value)
select	'Sheet1', 'B192', count(*)
from	#clients
where	hispanic = 0

insert into #results (sheet, location, value)
select	'Sheet1', 'B196', count(*)
from	#clients
where	race = 3		-- caucasian
and	hispanic = 0

insert into #results (sheet, location, value)
select	'Sheet1', 'B197', count(*)
from	#clients
where	race = 1		-- african american
and	hispanic = 0

insert into #results (sheet, location, value)
select	'Sheet1', 'B198', count(*)
from	#clients
where	race = 2		-- asian
and	hispanic = 0

insert into #results (sheet, location, value)
select	'Sheet1', 'B199', count(*)
from	#clients
where	race = 9		-- native american
and	hispanic = 0

insert into #results (sheet, location, value)
select	'Sheet1', 'B200', count(*)
from	#clients
where	race = 11		-- hawaiian
and	hispanic = 0

insert into #results (sheet, location, value)
select	'Sheet1', 'B201', count(*)
from	#clients
where	race = 911		-- American Indian and Alaskan native ????
and	hispanic = 0

insert into #results (sheet, location, value)
select	'Sheet1', 'B202', count(*)
from	#clients
where	race = 13		-- Asian and white ????
and	hispanic = 0

insert into #results (sheet, location, value)
select	'Sheet1', 'B203', count(*)
from	#clients
where	race = 14		-- Black/African American & white ????
and	hispanic = 0

insert into #results (sheet, location, value)
select	'Sheet1', 'B204', count(*)
from	#clients
where	race = 911		-- American Indian and Alskan Native and Black ????
and	hispanic = 0

insert into #results (sheet, location, value)
select	'Sheet1', 'B205', count(*)
from	#clients
where	race not in (14, 13, 11, 9, 4, 3, 2, 1)	-- Everyone else
and	hispanic = 0

-- Education level
insert into #results (sheet, location, value)
select	'Sheet1', 'B216', count(*)
from	#clients
where	education > 0 and education <= 8;

insert into #results (sheet, location, value)
select	'Sheet1', 'B217', count(*)
from	#clients
where	education >= 9 and education <= 12;

insert into #results (sheet, location, value)
select	'Sheet1', 'B218', count(*)
from	#clients
where	education >= 13 and education <= 16;

insert into #results (sheet, location, value)
select	'Sheet1', 'B219', count(*)
from	#clients
where	education >= 17;

-- Cause of finanical problems
insert into #results (sheet, location, value)
select 'Sheet1' as sheet, 'B224' as location,
count(*) from #clients where cause = 'OVS';

insert into #results (sheet, location, value)
select 'Sheet1' as sheet, 'B225' as location,
count(*) from #clients where cause in ('RIC', 'UNE');

insert into #results (sheet, location, value)
select 'Sheet1' as sheet, 'B226' as location,
count(*) from #clients where cause = 'MED';

insert into #results (sheet, location, value)
select 'Sheet1' as sheet, 'B227' as location,
count(*) from #clients where cause = 'DEA';

insert into #results (sheet, location, value)
select 'Sheet1' as sheet, 'B228' as location,
count(*) from #clients where cause = 'DIV';

insert into #results (sheet, location, value)
select 'Sheet1' as sheet, 'B229' as location,
count(*) from #clients where cause = 'SUB';

insert into #results (sheet, location, value)
select 'Sheet1' as sheet, 'B230' as location,
count(*) from #clients where cause = 'GAM';

insert into #results (sheet, location, value)
select 'Sheet1' as sheet, 'B231' as location,
count(*) from #clients where isnull(cause,'XXX') not in ('GAM','SUB','DIV','DEA','MED','RIC','OVS','UNE');

/*
-- Find the values for the income regions
insert into #results (sheet, location, value)
select 'Sheet1' as sheet, 'B191' as location,
count(*) from #clients where ((gross + assets) * 12.0) < 20000.00;

insert into #results (sheet, location, value)
select 'Sheet1' as sheet, 'B192' as location,
count(*) from #clients where ((gross + assets) * 12.0) between 20000.01 and 35000.00;

insert into #results (sheet, location, value)
select 'Sheet1' as sheet, 'B193' as location,
count(*) from #clients where ((gross + assets) * 12.0) between 35000.01 and 50000.00;

insert into #results (sheet, location, value)
select 'Sheet1' as sheet, 'B194' as location,
count(*) from #clients where ((gross + assets) * 12.0) between 50000.01 and 65000.00;

insert into #results (sheet, location, value)
select 'Sheet1' as sheet, 'B195' as location,
count(*) from #clients where ((gross + assets) * 12.0) > 65000.00;
*/

-- Average household income
insert into #results (sheet, location, value)
select 'Sheet1' as sheet, 'B179' as location,
round(avg(gross + assets) * 12.0, 2) as value
from #clients;

-- Find the income to debt ratios
update #clients
set ratio = (unsecured_debt + secured_debt + other_debt) / ((gross + assets) * 12.0) where (gross+assets) > 0.0;

insert into #results (sheet, location, value)
select 'Sheet1' as sheet, 'B235' as location,
count(*) from #clients where ratio <= 0.50;

insert into #results (sheet, location, value)
select 'Sheet1' as sheet, 'B236' as location,
count(*) from #clients where ratio > 0.50 and ratio <= 1.00;

insert into #results (sheet, location, value)
select 'Sheet1' as sheet, 'B237' as location,
count(*) from #clients where ratio > 1.00 and ratio <= 1.50;

insert into #results (sheet, location, value)
select 'Sheet1' as sheet, 'B238' as location,
count(*) from #clients where ratio > 1.50;

-- Find the employment information
insert into #results (sheet, location, value)
select 'Sheet1' as sheet, 'B209' as location, count(*) as value
from	#clients
where	job = '1';	-- UnEmployed

insert into #results (sheet, location, value)
select 'Sheet1' as sheet, 'B212' as location, count(*) as value
from	#clients
where	job = '9';	-- Retired

/*
insert into #results (sheet, location, value)
select 'Sheet1' as sheet, 'B211' as location, count(*) as value
from	#clients
where	job = '10';  -- Homemaker

*/

insert into #results (sheet, location, value)
select 'Sheet1' as sheet, 'B211' as location, count(*) as value
from	#clients
where	job = '1A';	-- Working Part Time

insert into #results (sheet, location, value)
select 'Sheet1' as sheet, 'B210' as location, count(*) as value
from	#clients
where	job not in ('0', '1', '9', '1A');  -- Working Full Time

-- Find the successful completion clients
/*
select	drop_reason, case
		when program_months between 0 and 3 then 0
		when program_months between 4 and 6 then 1
		when program_months between 7 and 12 then 2
		when program_months between 13 and 18 then 3
		when program_months between 19 and 24 then 4
		when program_months between 25 and 36 then 5
		when program_months between 37 and 48 then 6
		when program_months between 49 and 60 then 7
		when program_months > 60 then 8
	end as program_months
into	#lengths
from	#clients
where	drop_reason in ('SC1', 'SC2')

insert into #results (sheet, location, value)
select 'Sheet1' as sheet, 'B' + convert(varchar, 243 + program_months) as location, count(*) as value
from	#lengths
where	drop_reason = 'SC1'
group by program_months;

insert into #results (sheet, location, value)
select 'Sheet1' as sheet, 'B' + convert(varchar, 254 + program_months) as location, count(*) as value
from	#lengths
where	drop_reason = 'SC2'
group by program_months;

drop table #lengths
*/

-- Update the AMI for the person
update	#clients
set	ami = median_income * 0.7 where people <= 1;

update	#clients
set	ami = median_income * 0.8 where people = 2;

update	#clients
set	ami = median_income * 0.9 where people = 3;

update	#clients
set	ami = median_income where people = 4;

update	#clients
set	ami = median_income + convert(decimal(20,2), (0.08 * (convert(float,people) - 4.0)) * convert(float,median_income)) where people > 4;

-- Find the appropriate group for the client's income level
update	#clients
set	ami_group = 3 where gross <= ami;

update	#clients
set	ami_group = 2 where gross <= 0.80 * ami;

update	#clients
set	ami_group = 1 where gross < 0.50 * ami;

insert into #results (sheet, location, value)
select	'Sheet1' as sheet, 'B' + convert(varchar, 182 + ami_group) as location, count(*) as value
from	#clients
group by ami_group;

-- Return the results
select * from #results;

drop table #gross;
drop table #assets
drop table #unsecured_debt
drop table #secured_debt
drop table #other_debt
drop table #results;
drop table #clients;
drop table #appts
GO
