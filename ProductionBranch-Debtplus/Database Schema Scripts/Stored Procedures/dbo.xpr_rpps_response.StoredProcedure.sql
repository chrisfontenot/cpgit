USE [DebtPlus]
GO
/****** Object:  StoredProcedure [dbo].[xpr_rpps_response]    Script Date: 09/15/2014 13:13:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[xpr_rpps_response] ( @response_file as int, @trace_number as varchar(15), @biller_id as varchar(10), @return_code as varchar(3), @service_class_or_purpose as varchar(3), @name as varchar(22) = null, @account_number as varchar(22) = null, @net as money = 0, @addendum_date as varchar(10) = null, @addendum_routing_number as varchar(20) = null, @addendum_information as varchar(44) = null ) as

-- =======================================================================================================================
-- ==            Generate the basic response information record                                                         ==
-- =======================================================================================================================

-- ChangeLog
--   11/20/2002
--     Changed to use RPPS' biller_aba number rather than rpps_biller_id
--   4/14/2003
--     Looked up the transaciton, irregardless of any error condition.
--   2/16/2004
--     - If duplicated item detected, try to find the "proper" pointer
--     - Added "already processed" from the close logic
--   3/24/2004
--     - Looked for duplicates in the current response file.
--   11/8/2006
--     - Do not look for duplicate trace numbers involving CDC and CDN transactions
--   1/5/2009
--      Changed from using biller_aba to using rpps_biller_id

-- Remove the spaces from the trace number
if @trace_number is not null
	select @trace_number = ltrim(rtrim(@trace_number))
if @trace_number = ''
	select @trace_number = null

-- Remove the spaces from the biller id
if @biller_id is not null
	select @biller_id = ltrim(rtrim(@biller_id))
if @biller_id = ''
	select @biller_id = null

-- Remove the spaces from the response code
if @return_code is not null
	select @return_code = ltrim(rtrim(@return_code))
if @return_code = ''
	select @return_code = null

-- Remove the spaces from the name
if @name is not null
	select @name = ltrim(rtrim(@name))
if @name = ''
	select @name = null

-- Remove the spaces from the account number
if @account_number is not null
	select @account_number = ltrim(rtrim(@account_number))
if @account_number = ''
	select @account_number = null

-- The trace number must occur one and one time only
declare	@cnt			int
declare	@msg			varchar(50)
declare	@rpps_transaction	int
declare	@orig_biller		varchar(10)
declare	@old_return_code	varchar(10)

select	@msg			= null;

-- Discover has this bogus return code. Make it special.
if @return_code = 'R95'
	select	@service_class_or_purpose	= 'DFS',
		@msg				= null,
		@return_code			= null
		
-- Map the ABA biller to the biller ID if needed. This is rare and is only done for sites that
-- use the ABA format for biller IDs.
if @biller_id is not null
begin
	if not exists(select rpps_biller_id from rpps_biller_ids WHERE rpps_biller_id = @biller_id)
		select	@biller_id	= rpps_biller_id
		from	rpps_biller_ids with (nolock)
		where	biller_aba	= @biller_id
end

-- Do additional checking if this is not a CDT operation.
if @service_class_or_purpose in ('DFS', 'CDT', 'CDC')
	select	@rpps_transaction		= null

else begin

	-- Try to find the matching transaction in the outbound transaction list
	select	@rpps_transaction	= rpps_transaction
	from	rpps_transactions
	where	@trace_number between trace_number_first and trace_number_last
	and	trace_number_first like '[0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9]';

	if @rpps_transaction is null
		select @msg = 'UNKNOWN TRACE NUMBER'

	if @msg is null
	begin
		select	@cnt = count(*)
		from	rpps_transactions
		where	@trace_number between trace_number_first and trace_number_last
		and	trace_number_first like '[0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9]';

		if @cnt > 1
		begin
			select @msg = 'DUPLICATE TRACE NUMBER'

			-- Try to find the appropriate item if this is a duplicate. Use the account number and biller for the item
			declare @corrected_transaction	int

			select			@corrected_transaction	= rpps_transaction
			from			rpps_transactions t
			left outer join	registers_client_creditor rcc on t.client_creditor_register = rcc.client_creditor_register
			where			@trace_number between trace_number_first and trace_number_last
			and			trace_number_first		like '[0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9]'
			and			t.service_class_or_purpose	= 'CIE'
			and			rcc.account_number		= @account_number;
	
			if @corrected_transaction is not null
				select	@rpps_transaction = @corrected_transaction;
		end
	end

	-- Validate the biller id
	if @msg is null
	begin
		select	@orig_biller		= biller_id,
			@old_return_code	= return_code
		from	rpps_transactions tr with (nolock)
		where	rpps_transaction	= @rpps_transaction

		-- Complain if the biller IDs are not the same.
		if @orig_biller <> @biller_id
			select	@msg = 'WRONG BILLER ID'
	end

	-- If the operation has already been processed then indicate that the item is duplicated
	if @msg is null
	begin
		if @old_return_code is not null
			select	@msg = 'DUPLICATE RESPONSE'
	end

	-- Handle the stupid case where the trace number is duplicated in the same response file
	-- that we are currently processing.
	if @msg is null
	begin
		if exists (select * from rpps_response_details where trace_number = @trace_number and service_class_or_purpose not in ('DFS', 'CDT', 'CDC'))
			select	@msg = 'DUPLICATE RESPONSE'

		-- Look at the transaction number to handle the case where we get duplicates but different trace
		-- numbers just because one is for a "7" record while this is for a "6" record.
		if @msg is null
		begin
			if exists (select * from rpps_response_details where rpps_transaction = @rpps_transaction and service_class_or_purpose not in ('DFS', 'CDT', 'CDC'))
				select	@msg = 'DUPLICATE RESPONSE'
		end
	end
end

-- Insert the transaction for the response
insert into rpps_response_details (rpps_response_file, rpps_biller_id, service_class_or_purpose, return_code, trace_number, rpps_transaction, consumer_name, account_number, net, processing_error)
values (@response_file, @biller_id, @service_class_or_purpose, @return_code, @trace_number, @rpps_transaction, @name, @account_number, @net, @msg)

return ( SCOPE_IDENTITY() )
GO
