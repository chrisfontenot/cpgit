USE [DebtPlus]
GO
/****** Object:  StoredProcedure [dbo].[IntListToTable]    Script Date: 09/15/2014 13:13:32 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[IntListToTable] 
    @cslist VARCHAR(8000), 
    @tablename SYSNAME AS 
BEGIN 
    DECLARE @spot SMALLINT, @str VARCHAR(8000), @sql VARCHAR(8000) 
 
    WHILE @cslist <> '' 
    BEGIN 
        SET @spot = CHARINDEX(',', @cslist) 
        IF @spot>0 
            BEGIN 
                SET @str = CAST(LEFT(@cslist, @spot-1) AS INT) 
                SET @cslist = RIGHT(@cslist, LEN(@cslist)-@spot) 
            END 
        ELSE 
            BEGIN 
                SET @str = CAST(@cslist AS INT) 
                SET @cslist = '' 
            END 
        SET @sql = 'INSERT INTO '+@tablename+' VALUES('+CONVERT(VARCHAR(100),@str)+')' 
        EXEC(@sql) 
    END 
END
GO
