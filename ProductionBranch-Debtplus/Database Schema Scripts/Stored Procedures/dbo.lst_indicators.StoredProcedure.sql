USE [DebtPlus]
GO
/****** Object:  StoredProcedure [dbo].[lst_indicators]    Script Date: 09/15/2014 13:13:32 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE PROCEDURE [dbo].[lst_indicators] AS

-- =========================================================================================================
-- ==            Return the indicator values for the client selection                                     ==
-- =========================================================================================================

select	indicator		as 'item_key',
	description		as 'description'
from	indicators with (nolock)
order by 2

return ( @@rowcount )
GO
