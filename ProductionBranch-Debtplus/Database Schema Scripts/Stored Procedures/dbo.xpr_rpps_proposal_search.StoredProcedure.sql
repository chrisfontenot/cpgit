USE [DebtPlus]
GO
/****** Object:  StoredProcedure [dbo].[xpr_rpps_proposal_search]    Script Date: 09/15/2014 13:13:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE PROCEDURE [dbo].[xpr_rpps_proposal_search] ( @biller_name as varchar(80) ) AS

-- ===============================================================================================
-- ==        Return a list of the matching biller IDs for this name in the dmp_ids table        ==
-- ===============================================================================================

-- ChangeLog
--   2/1/2002
--     Change to rpps_biller_ids table.
--   11/20/2002
--     Changed to use RPPS' biller_aba number rather than rpps_biller_id
--   6/2/2003
--     Change to support "dual" (both proposals and payments) billers
--   1/5/2009
--      Changed from using biller_aba to using rpps_biller_id

SET NOCOUNT ON

SELECT		ids.rpps_biller_id		as 'rpps_biller_id',
		aka.[name]		as 'name'
FROM		rpps_biller_ids ids with (nolock)
INNER JOIN	rpps_akas aka with (nolock) on ids.rpps_biller_id = aka.rpps_biller_id
WHERE		aka.[name] like '%' + ltrim(rtrim(@biller_name)) + '%'
AND		ids.biller_type IN (4, 11, 12, 13, 14)

ORDER BY 1, 2

RETURN ( @@rowcount )
GO
