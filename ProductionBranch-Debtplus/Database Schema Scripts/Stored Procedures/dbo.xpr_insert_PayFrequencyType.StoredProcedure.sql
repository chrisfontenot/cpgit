USE [DebtPlus]
GO
/****** Object:  StoredProcedure [dbo].[xpr_insert_PayFrequencyType]    Script Date: 09/15/2014 13:13:35 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[xpr_insert_PayFrequencyType] ( @description as varchar(50), @PeriodsPerYear as int = 0, @default as bit = 0, @ActiveFlag as bit = 1 ) as
-- ========================================================================================
-- ==   Procedure for .NET to insert items into the PayFrequencyTypes table              ==
-- ========================================================================================
	insert into PayFrequencyTypes ( [description],[PeriodsPerYear],[default],[ActiveFlag] ) values ( @description,@PeriodsPerYear,@default,@ActiveFlag )
	return ( scope_identity() )
GO
