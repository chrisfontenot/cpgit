USE [DebtPlus]
GO
/****** Object:  StoredProcedure [dbo].[rpt_Deposit_Coupon]    Script Date: 09/15/2014 13:13:33 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE PROCEDURE [dbo].[rpt_Deposit_Coupon] ( @client as int = null ) as

-- ==============================================================================================
-- ==            Retrieve the information needed for the deposit coupon report                 ==
-- ==============================================================================================

-- Suppress intermediate results
set nocount on

-- If the client is null then make it zero
if @client is null
	select @client = 0

-- Create the working table to hold the results
create table #deposit_coupons (
	client		int null,
	deposit_day	int null,
	deposit_amount	money null,
	name		varchar(80) null,
	address1	varchar(80) null,
	address2	varchar(80) null,
	address3	varchar(80) null
);

if @client <= 0

	-- Select the total list of clients
	insert into #deposit_coupons (client, deposit_day, deposit_amount, name, address1, address2, address3)
	select	d.client			as client,
		datepart(d, deposit_date)	as deposit_day,
		deposit_amount			as deposit_amount,

		v.name				as name,
		v.addr1				as address1,
		v.addr2				as address2,
		v.addr3				as address3

	from	client_deposits d with (nolock)
	inner join view_client_address v with (nolock) on d.client = v.client
	where	d.client in (
		select	c.client
		from	clients c
		left outer join client_ach ach with (nolock) on c.client = ach.client
		where	c.active_status in ('A', 'AR')

		-- ignore the clients on ACH
		and	isnull(ach.isActive,0) = 0
	)
	and	d.one_time = 0
	and	d.client > 0
	and	d.deposit_amount > 0

else

	-- Select a specific client
	insert into #deposit_coupons (client, deposit_day, deposit_amount, name, address1, address2, address3)
	select	d.client			as client,
		datepart(d, deposit_date)	as deposit_day,
		deposit_amount			as deposit_amount,

		v.name				as name,
		v.addr1				as address1,
		v.addr2				as address2,
		v.addr3				as address3

	from	client_deposits d with (nolock)
	inner join view_client_address v with (nolock) on d.client = v.client
	where	d.one_time = 0
	and	d.client = @client

-- Return four copies of the coupons for each client
select	*
from	#deposit_coupons

union all

select	*
from	#deposit_coupons

union all

select	*
from	#deposit_coupons

union all

select	*
from	#deposit_coupons

order by 1

-- Discard the working table
drop table #deposit_coupons

return ( @@rowcount )
GO
