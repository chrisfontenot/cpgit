USE [DebtPlus]
GO
/****** Object:  StoredProcedure [dbo].[xpr_client_tickler_item]    Script Date: 09/15/2014 13:13:34 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[xpr_client_tickler_item]  ( @tickler as int ) AS
-- ==========================================================================================================
-- ==            List the tickler item                                                                     ==
-- ==========================================================================================================

select		x.tickler		as 'tickler',		-- Primary key
			dbo.format_normal_name(default,cox.first,default,cox.last,default) as 'counselor',		-- Counselor ID
			t.description		as 'description',	-- Description of the event
			x.date_effective	as 'date_effective',	-- Effective date
			x.date_created		as 'date_created',	-- Date item was created
			x.created_by		as 'created_by',	-- Person who created the item
			x.priority		as 'priority',		-- Priority of the item
			x.date_deleted		as 'date_deleted'	-- deleted status

from		ticklers x with (nolock)
left outer join TicklerTypes t on x.tickler_type = t.oID
left outer join	counselors co with (nolock) on x.counselor = co.counselor
left outer join names cox with (nolock) on co.NameID = cox.name
where		tickler = @tickler

return ( @@rowcount )
GO
