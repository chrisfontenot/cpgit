USE [DebtPlus]
GO
/****** Object:  StoredProcedure [dbo].[rpt_PrintClient_ComparisonByClient]    Script Date: 09/15/2014 13:13:33 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[rpt_PrintClient_ComparisonByClient] ( @client as int ) as
-- ===================================================================================
-- ==          Retrieve the comparison information for the indicated client         ==
-- ===================================================================================

set nocount on

declare	@sales_file		int
declare	@sales_file_date	datetime
select	@sales_file_date	= max(date_created)
from	sales_files with (nolock)
where	client				= @client

if @sales_file_date is not null
	select	@sales_file		= max(sales_file)
	from	sales_files with (nolock)
	where	client			= @client
	and		date_created	= @sales_file_date

-- Retrieve the information about this file and the list of debts
select	sales_file, client, convert(decimal(18,0),extra_amount) as extra_amount, note, convert(decimal(18,0),client_monthly_interest) as client_monthly_interest, convert(decimal(18,0),plan_monthly_interest) as plan_monthly_interest, convert(decimal(18,0),client_total_interest) as client_total_interest, convert(decimal(18,0),plan_total_interest) as plan_total_interest, client_months, plan_months, date_exported, exported_by, date_created, created_by
from	sales_files
where	sales_file		= @sales_file

select	sales_debt, sales_file, creditor_type, creditor_name, account_number, client_creditor, convert(decimal(18,0),balance) as balance, interest_rate, convert(decimal(18,0),minimum_payment) as minimum_payment, convert(decimal(18,0),late_fees) as late_fees, convert(decimal(18,0),overlimit_fees) as overlimit_fees, convert(decimal(18,0),cosigner_fees) as cosigner_fees, convert(decimal(18,0),finance_charge) as finance_charge, convert(decimal(18,0),total_interest_fees) as total_interest_fees, convert(decimal(18,0),principal) as principal, convert(decimal(18,0),plan_min_prorate) as plan_min_prorate, convert(decimal(18,0),plan_min_payment) as plan_min_payment, convert(decimal(18,0),plan_payment) as plan_payment, plan_interest_rate, convert(decimal(18,0),plan_total_fees) as plan_total_fees, convert(decimal(18,0),plan_finance_charge) as plan_finance_charge, convert(decimal(18,0),plan_total_interest_fees) as plan_total_interest_fees, convert(decimal(18,0),plan_principal) as plan_principal, payout_percent, convert(decimal(18,0),payout_interest) as payout_interest
from	sales_debts
where	sales_file		= @sales_file

return ( @@rowcount )
GO
