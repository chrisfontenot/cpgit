USE [DebtPlus]
GO
/****** Object:  StoredProcedure [dbo].[xpr_insert_budgets]    Script Date: 09/15/2014 13:13:35 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[xpr_insert_budgets] ( @client int, @type varchar(50) = null, @note varchar(50) = null ) as

-- Suppress intermediate results
set nocount on

declare @budget int

-- Insert the budget into the system
begin transaction

insert into budgets ( client, budget_type ) values ( @client, 'F' )
select @budget = SCOPE_IDENTITY()

-- Find the number of budgets for this client. If there is only one then make it the initial budget
if (select count(*) from budgets where client = @client ) = 1
begin
	update	budgets
	set		budget_type = 'I'
	where	budget = @budget
end

-- Commit the change to the database
commit

-- Return the budget information to the caller
return ( @budget )
GO
