USE [DebtPlus]
GO
/****** Object:  StoredProcedure [dbo].[lst_config_fees]    Script Date: 09/15/2014 13:13:32 ******/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE PROCEDURE [dbo].[lst_config_fees] AS

-- =================================================================================================
-- ==            Rules for computing the P.A.F. amounts                                           ==
-- =================================================================================================

-- Disable intermediate result sets
SET NOCOUNT ON

-- Fetch the list of config fee amounts
SELECT	config_fee	as 'item_key',
	[description]	as 'description'
FROM	config_fees WITH (NOLOCK)
ORDER BY 2

RETURN ( @@rowcount )
GO
