USE [DebtPlus]
GO
/****** Object:  StoredProcedure [dbo].[xpr_insert_client_retention_event]    Script Date: 09/15/2014 13:13:35 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[xpr_insert_client_retention_event] (@client int, @retention_event int, @amount money = 0, @message varchar(1024) = '', @expire_type int = 1, @expire_date datetime = null, @effective_date datetime = null, @priority int = 9) as
	if @effective_date is null
		select @effective_date = convert(varchar(10), getdate(), 101)
	insert into client_retention_events (client, retention_event, amount, message, expire_type, expire_date, effective_date, priority) values (@client, @retention_event, @amount, @message, @expire_type, @expire_date, @effective_date, @priority)
	return ( scope_identity() )
GO
