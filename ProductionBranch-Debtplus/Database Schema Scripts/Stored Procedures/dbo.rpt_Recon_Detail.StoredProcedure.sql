SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER procedure [rpt_Recon_Detail] ( @CutoffDate AS DateTime ) AS

-- ======================================================================================================
-- ==               Generate the detail information for the reconcilation report                       ==
-- ======================================================================================================

-- ChangeLog
--   3/22/2002
--     Added RR to the list of deposit transaction types

-- Convert the date to the start of the day
SELECT	@CutoffDate = convert(datetime, convert(varchar(10), @CutoffDate, 101) + ' 00:00:00')

-- Retrieve the records from the trust register
SELECT
	-- The first field is the group in the report.
		-- Group 1 = reconciled checks prior to the cutoff date
		-- Group 2 = reconciled checks after the cutoff date
		-- Group 3 = un-reconciled checks prior to the cutoff date
		-- Group 4 = un-reconciled checks after the cutoff date
		-- Group 5 = reconciled deposits prior to the cutoff date
		-- Group 6 = reconciled deposits after the cutoff date
		-- Group 7 = un-reconciled deposits prior to the cutoff date
		-- Group 8 = un-reconciled deposits afte the cutoff date
	CASE
		WHEN tran_type IN ('DP', 'RR', 'BI', 'RF') THEN
			CASE ISNULL(tr.cleared,' ')
				WHEN 'V' THEN CASE WHEN tr.date_created > @CutoffDate THEN 6 ELSE 5 END
				WHEN 'R' THEN CASE WHEN tr.date_created > @CutoffDate THEN 6 ELSE 5 END
				ELSE CASE WHEN tr.date_created > @CutoffDate THEN 8 ELSE 7 END
			END
		ELSE CASE ISNULL(tr.cleared,' ')
				WHEN 'V' THEN CASE WHEN tr.date_created > @CutoffDate THEN 2 ELSE 1 END
				WHEN 'R' THEN CASE WHEN tr.date_created > @CutoffDate THEN 2 ELSE 1 END
				ELSE CASE WHEN tr.date_created > @CutoffDate THEN 4 ELSE 3 END
			END
	END AS [group],

	-- The check/deposit date
	tr.date_created,

	-- The date the item was reconciled
	tr.reconciled_date,

	-- The check number. Deposits are NULL here.
	tr.checknum,

	-- The payee for the check. Deposits are simply "Deposit"
	CASE tr.tran_type
		WHEN 'DP' THEN 'Deposit'
		WHEN 'BW' THEN 'Bank Wire'
		WHEN 'SC' THEN 'Service Charge'
		WHEN 'BI' THEN 'Bank Interest'
		WHEN 'RR' THEN 'RPS Reject'
		WHEN 'RF' THEN 'Creditor Refund'
		WHEN 'CR' THEN '[' + dbo.format_client_id ( tr.client ) + '] ' + dbo.format_normal_name(pn.prefix, pn.first, pn.middle, pn.last, pn.suffix)
		ELSE isnull('[' + cr.creditor + ']' + isnull(' '+cr.creditor_name,''),'')
	END as payee,

	-- The cleared status
	tr.cleared	as 'cleared',

	-- Debit amounts are for withdrawls
	-- void items are always zero here.
	case
		when isnull(tr.cleared,' ') = 'V' then 0.0
		when tr.tran_type in ('DP', 'BI', 'RF', 'RR') then 0.0
		else tr.amount
	end as debit,

	-- Credit amounts are for deposits
	case
		when tr.tran_type in ('DP', 'BI', 'RF', 'RR') then tr.amount
		else 0.0
	end as credit

FROM	registers_trust tr
LEFT OUTER JOIN people p ON tr.client = p.client AND 1 = p.relation
left outer join names pn with (nolock) on p.nameid = pn.name
LEFT OUTER JOIN creditors cr ON tr.creditor = cr.creditor
WHERE	(tr.date_created > @CutoffDate)
OR	isnull(tr.cleared,' ') NOT IN ('V', 'R')

ORDER BY 1, 2

RETURN ( @@rowcount )
GO
