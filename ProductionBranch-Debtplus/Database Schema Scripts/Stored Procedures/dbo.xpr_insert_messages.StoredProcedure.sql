USE [DebtPlus]
GO
/****** Object:  StoredProcedure [dbo].[xpr_insert_messages]    Script Date: 09/15/2014 13:13:35 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[xpr_insert_messages] ( @item_type as varchar(20), @item_value as int = 0, @description as varchar(50), @nfcc as varchar(4) = null, @rpps as varchar(10) = null, @epay as varchar(4) = null, @hud_9902_section as varchar(50) = null, @default as bit = 0, @additional as varchar(256) = null, @note as varchar(256) = null ) as
-- ========================================================================================
-- ==   Procedure for .NET to insert items into the messages table                       ==
-- ========================================================================================
	insert into messages ( [item_type], [item_value], [description], [nfcc], [rpps], [epay], [hud_9902_section], [default], [additional], [note] ) values ( @item_type, @item_value, @description, @nfcc, @rpps, @epay, @hud_9902_section, @default, @additional, @note )
	return ( scope_identity() )
GO
