USE [DebtPlus]
GO
/****** Object:  StoredProcedure [dbo].[rpt_Disbursement_Detail_List]    Script Date: 09/15/2014 13:13:33 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE PROCEDURE [dbo].[rpt_Disbursement_Detail_List] ( @disbursement_register AS INT = NULL, @show_all AS bit = 0 ) AS

-- =======================================================================================================
-- ==           Fetch the information to list the clients and the disbursed amounts                     ==
-- =======================================================================================================

-- Disable intermediate result sets
SET NOCOUNT ON

-- Attempt to determine the disbursement register for the current disbursement batch
if @disbursement_register is null
begin
	select top 1	@disbursement_register = disbursement_register
	from		registers_disbursement
	where		tran_type = 'AD'
	and		date_posted IS NULL
	order by	date_created desc

	if @disbursement_register is null
	begin
		RaisError(50077, 16, 1)
		return ( 0 )
	end
end

-- Fetch the client, amount scheduled for disbursement, and the amount disbursed
if @show_all = 0
	select		dc.disbursement_register				as 'disbursement_register',
			dc.client						as 'client',
			isnull(dc.disbursement_factor,0)			as 'disbursement_factor',
			isnull(c.held_in_trust,0) + isnull(rc.debit_amt,0)	as 'held_in_trust',
			isnull(rc.debit_amt,0)					as 'disbursed',
			dbo.format_normal_name(pn.prefix,pn.first,pn.middle,pn.last,pn.suffix) as 'name'

	from		view_disbursement_clients dc with (nolock)
	left outer join	people p with (nolock) on dc.client = p.client and 1 = p.relation
	left outer join names pn with (nolock) on p.nameid = pn.name
	left outer join	clients c with (nolock) on dc.client = c.client
	left outer join registers_client rc with (nolock) on dc.client_register = rc.client_register

	where		dc.disbursement_register = @disbursement_register
	and		isnull(c.held_in_trust,0) > 0
	order by	dc.client

else

	select		dc.disbursement_register				as 'disbursement_register',
			dc.client						as 'client',
			isnull(dc.disbursement_factor,0)			as 'disbursement_factor',
			isnull(c.held_in_trust,0) + isnull(rc.debit_amt,0)	as 'held_in_trust',
			isnull(rc.debit_amt,0)					as 'disbursed',
			dbo.format_normal_name(pn.prefix,pn.first,pn.middle,pn.last,pn.suffix) as 'name'

	from		view_disbursement_clients dc with (nolock)
	left outer join	people p with (nolock) on dc.client = p.client and 1 = p.relation
	left outer join names pn with (nolock) on p.nameid = pn.name
	left outer join	clients c with (nolock) on dc.client = c.client
	left outer join registers_client rc with (nolock) on dc.client_register = rc.client_register

	where		dc.disbursement_register = @disbursement_register
	order by	dc.client

return ( @@rowcount )
GO
