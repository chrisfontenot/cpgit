USE [DebtPlus]
GO
/****** Object:  StoredProcedure [dbo].[xpr_PrintClient_SelfPayout]    Script Date: 09/15/2014 13:13:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE PROCEDURE [dbo].[xpr_PrintClient_SelfPayout] ( @Client AS INT = NULL ) AS
-- ========================================================================================
-- ==               Calculate the debt repayment for the "SELF" plan                     ==
-- ========================================================================================

-- ChangeLog
--   9/31/2002
--     Added support for client_creditor_balances table

-- Prevent temporary recordsets from being returned
SET NOCOUNT ON
SET ANSI_WARNINGS OFF

-- First, scan through the debts and build a payout schedule for each debt
DECLARE debt_cursor CURSOR FORWARD_ONLY FOR
	SELECT	cc.non_dmp_interest, cc.non_dmp_payment, bal.orig_balance+bal.orig_balance_adjustment+bal.total_interest-bal.total_payments AS 'balance'
	FROM	client_creditor cc
	inner join client_creditor_balances bal on cc.client_creditor_balance = bal.client_creditor_balance
	WHERE	cc.client = @client
	AND	cc.reassigned_debt = 0
	AND	bal.orig_balance + bal.orig_balance_adjustment + bal.total_interest > bal.total_payments

OPEN debt_cursor

-- Create the working table. It is sorted by date (year and fractional year)
CREATE TABLE #t_PrintClient_Self_1 (
	year_item	float,
	balance		money,
	interest	money
)

-- Calculate the current starting point for the year representation
declare	@year_base		float
SET @year_base = convert (float, month ( getdate() ) - 1 ) / 12.0
SET @year_base = @year_base + convert ( float, year ( getdate() ))

-- Process all of the debts. Compute the payout schedule
declare @non_dmp_rate		float
declare	@non_dmp_payment	money
declare	@balance		money
declare @month_offset		float
declare @Interest_Amt		money

FETCH debt_cursor INTO @non_dmp_rate, @non_dmp_payment, @balance
WHILE @@fetch_status = 0
BEGIN

	-- The rate must be less than 1.0
	IF @non_dmp_rate >= 1.0
		SET @non_dmp_rate = @non_dmp_rate / 100.0

	-- Calculate the payout schedule to a limit of 10 years
	SET @month_offset = 0.0
	WHILE (@month_offset < 10) AND (@balance > 0)
	BEGIN
		IF @balance <= @non_dmp_payment
		BEGIN
			SET @non_dmp_payment = @balance
			SET @Interest_Amt = 0.0
		END
		ELSE
			SET @Interest_Amt = @Balance * ( @non_dmp_rate / 12.0 )

		-- "Pay" this month payment for this debt
		INSERT INTO #t_PrintClient_Self_1 (year_item, balance, interest)
		VALUES (@month_offset + @year_base, @balance, @Interest_Amt)

		-- Increase the debt by the interest and reduce it by the payment
		SET @balance = @balance + @interest_amt
		SET @balance = @balance - @non_dmp_payment

		-- Increment to the next month cycle
		SET @month_offset = @month_offset + (1.0 / 12.0)
	END

	-- Go on to the next debt
	FETCH debt_cursor INTO @non_dmp_rate, @non_dmp_payment, @balance
END

CLOSE debt_cursor
DEALLOCATE debt_cursor

-- Return the summary of the items, grouped by the date values
SELECT
	year_item as 'year', sum(balance) as 'balance', sum(interest) as 'interest'
FROM
	#t_PrintClient_Self_1
GROUP BY
	year_item
ORDER BY
	year_item

-- Destroy the working table
DROP TABLE #t_PrintClient_Self_1

-- Restore the flags
SET ANSI_WARNINGS ON
SET NOCOUNT OFF
RETURN ( 1 )
GO
