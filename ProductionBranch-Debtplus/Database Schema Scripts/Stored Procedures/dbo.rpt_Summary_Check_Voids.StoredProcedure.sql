SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER procedure [rpt_Summary_Check_Voids] ( @FromDate AS DateTime = NULL, @ToDate AS DateTime = NULL ) AS
-- ======================================================================================================
-- ==                   Fetch the information for voided creditor checks                               ==
-- ======================================================================================================

IF @ToDate IS NULL
	SELECT @ToDate = getdate()

IF @FromDate IS NULL
	SELECT @FromDate = @ToDate

-- Adjust the dates for the proper settings
SELECT @FromDate	= cast (convert(varchar(10), @FromDate, 101) + ' 00:00:00' as datetime)
SELECT @ToDate	= cast (convert(varchar(10), @ToDate, 101)   + ' 23:59:59' as datetime)

-- Fetch the transactions
SELECT		d.creditor		as 'creditor',
		d.credit_amt		as 'amount',
		cr.creditor_name	as 'creditor_name',
		d.date_created		as 'item_date',
		tr.checknum		as 'checknum',
		d.created_by		as 'counselor',
		d.trust_register	as 'trust_register'

FROM		registers_creditor d	WITH (NOLOCK)
LEFT OUTER JOIN	creditors cr		WITH (NOLOCK) ON d.creditor = cr.creditor
LEFT OUTER JOIN	registers_trust tr	WITH (NOLOCK) ON d.trust_register = tr.trust_register

WHERE		d.tran_type = 'VD'
AND		d.date_created BETWEEN @FromDate AND @ToDate
ORDER BY	d.date_created, d.trust_register

RETURN ( @@rowcount )
GO
