USE [DebtPlus]
GO
/****** Object:  StoredProcedure [dbo].[xpr_rpps_cdp_generate]    Script Date: 09/15/2014 13:13:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[xpr_rpps_cdp_generate] ( @rpps_file as int ) as
-- ===================================================================================================
-- ==            Generate transactions for the RPPS CDP proposal record                             ==
-- ===================================================================================================

-- ChangeLog
--   11/20/2002
--     Changed to use RPPS' biller_aba number rather than rpps_biller_id
--   11/24/2003
--     Added "@bank" as a parameter for the time being. It is optional.
--   10/27/2003
--     Moved the proposal message column information to the debt_notes table.
--   06/09/2009
--      Remove references to debt_number and use client_creditor
--   1/5/2009
--      Changed from using biller_aba to using rpps_biller_id
--   6/1/2012
--      Set the death_date when creating the transaction

-- Start a transaction for the operation
BEGIN TRANSACTION

-- Find the bank from the rpps file
declare	@bank		int
select	@bank		= bank
from	rpps_files with (nolock)
where	rpps_file	= @rpps_file

declare	@default	int
select	@default	= [default]
from	banks with (nolock)
where	bank		= @bank

-- Disable intermediate result sets
SET NOCOUNT ON

create table #pending_proposals (client int,
				creditor varchar(10) null,
				client_creditor int null,
				proposal_batch_id int null,
				client_creditor_proposal int null,
				proposal_note_length int null,
				send_cdp int null,
				send_fbc int null,
				send_fbd int null,
				send_msg int null,
				rpps_biller_id varchar(15) null,
				send_bal_verify int null )

-- Find the proposals that we wish to process
insert into #pending_proposals (client, creditor, client_creditor, client_creditor_proposal, proposal_batch_id, proposal_note_length, send_cdp, send_fbc, send_fbd, send_msg, rpps_biller_id, send_bal_verify)
SELECT		cc.client													as 'client',
			cc.creditor													as 'creditor',
			cc.client_creditor											as 'client_creditor',
			p.client_creditor_proposal									as 'client_creditor_proposal',
			p.proposal_batch_id											as 'proposal_batch_id',
			len(isnull(convert(varchar(2000),dn.note_text),''))			as 'proposal_note_length',
			convert(int,1)												as 'send_cdp',
			case when p.full_disclosure = 1 then 1 else 0 end			as 'send_fbc',
			case when p.full_disclosure = 1 then 1 else 0 end			as 'send_fbd',
			convert(int,1)												as 'send_msg',
			p.rpps_biller_id											as 'rpps_biller_id',
			cc.send_bal_verify											as 'send_bal_verify'
FROM		client_creditor_proposals p	WITH (NOLOCK)
INNER JOIN	proposal_batch_ids pb		WITH (NOLOCK) ON p.proposal_batch_id	= pb.proposal_batch_id
INNER JOIN	client_creditor cc			WITH (NOLOCK) ON p.client_creditor	= cc.client_creditor
INNER JOIN	clients c					WITH (NOLOCK) ON cc.client		= c.client
LEFT OUTER JOIN	debt_notes dn			WITH (NOLOCK) ON p.client_creditor_proposal = dn.client_creditor_proposal and 'PR' = dn.type

WHERE		c.active_status not in ('CRE', 'EX', 'I')	-- Active clients
AND			pb.date_closed IS NOT NULL			-- Closed but
AND			pb.date_transmitted IS NULL			-- Not transmitted
AND			pb.bank	= @bank						-- For the desired bank
AND			cc.send_bal_verify in (0, 1, 5)		-- Suppress proposals if prenote is not validated by this creditor
				
-- Reset the success status back to normal if indicated
update		client_creditor
set			send_bal_verify	= 0
where		send_bal_verify = 5;

-- Date when the transactions are to expire
declare		@death_date		datetime
select		@death_date	= dateadd(d, 180, getdate())

-- Include any drops into the RPPS transmission file where there is a need
INSERT INTO	rpps_transactions (client, creditor, client_creditor, biller_id, transaction_code, service_class_or_purpose, client_creditor_proposal, rpps_file, bank, death_date)
SELECT		client,
			creditor,
			client_creditor,
			rpps_biller_id,
			23,
			'CDP',
			client_creditor_proposal,
			@rpps_file, @bank, @death_date
from		#pending_proposals
where		send_cdp > 0;

-- Include the FBC proposal records
INSERT INTO	rpps_transactions (client, creditor, client_creditor, biller_id, transaction_code, service_class_or_purpose, client_creditor_proposal, rpps_file, bank, death_date)
SELECT		client,
			creditor,
			client_creditor,
			rpps_biller_id,
			23,
			'FBC',
			client_creditor_proposal,
			@rpps_file, @bank, @death_date
from		#pending_proposals
where		send_fbc > 0;

-- Include the FBD proposal records
INSERT INTO	rpps_transactions (client, creditor, client_creditor, biller_id, transaction_code, service_class_or_purpose, client_creditor_proposal, rpps_file, bank, death_date)
SELECT		client,
			creditor,
			client_creditor,
			rpps_biller_id,
			23,
			'FBD',
			client_creditor_proposal,
			@rpps_file, @bank, @death_date
from		#pending_proposals
where		send_fbd > 0;

-- Update the proposal transmitted date
update		proposal_batch_ids
set			date_transmitted	= getdate()
from		proposal_batch_ids b
inner join	#pending_proposals p on b.proposal_batch_id = p.proposal_batch_id;

-- Discard the working table
drop table #pending_proposals

-- Make the changes permanent
COMMIT TRANSACTION
RETURN ( 1 )
GO
