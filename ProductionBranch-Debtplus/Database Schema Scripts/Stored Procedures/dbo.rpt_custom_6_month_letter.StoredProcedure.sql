USE [DebtPlus]
GO
/****** Object:  StoredProcedure [dbo].[rpt_custom_6_month_letter]    Script Date: 09/15/2014 13:13:33 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[rpt_custom_6_month_letter] as

-- =================================================================================
-- ==         Generate the 6 month followup letter for clients based on           ==
-- ==         their start date for the program. Only active clients are used.     ==
-- =================================================================================

-- Suppress intermediate results
set nocount on

-- Find the clients who are missing in the table
select	c.client, convert(datetime, convert(varchar(10), dateadd(m, 6, isnull(c.start_date, c.date_created)), 101)) as next_date_to_send
into	#new_clients
from	clients c
left outer join ClearPoint_6Month_Client_Letters x on c.client = x.client
where	x.client is null
and		c.active_status in ('A','AR')
and		c.client > 0;

-- Adjust the starting date where needed
while 1 = 1
begin
	if not exists (select * from #new_clients where next_date_to_send < getdate())
		break;

	update	#new_clients
	set		next_date_to_send = dateadd(m, 6, next_date_to_send)
	where	next_date_to_send < getdate();
end

-- Load the new items into the table
insert into ClearPoint_6Month_Client_Letters(client, next_date_to_send)
select	client, next_date_to_send
from	#new_clients;

drop table #new_clients;

-- Drop any inactive clients
delete	ClearPoint_6Month_Client_Letters
from	ClearPoint_6Month_Client_Letters x
left outer join clients c on x.client = c.client
where	c.client is null
or		c.active_status not in ('A','AR')
or		x.client <= 0;

-- Find the dates that we wish to use
declare	@from_date	datetime
declare	@to_date	datetime
declare	@today		datetime
select	@today		= convert(varchar(10), getdate(), 101)
select	@from_date = convert(varchar, month(@today)) + '/01/' + convert(varchar, year(@today))
select	@to_date   = dateadd(m, 1, @from_date)

-- Mark the letters for this batch
declare	@batch_id	varchar(50)
select	@batch_id	= newid();

update	ClearPoint_6Month_Client_Letters
set		batch		= @batch_id
where	next_date_to_send < @to_date
and		(date_sent is null or datediff(d, date_sent, @to_date) > 60)

-- Letters sent today also qualify in the batch
update	ClearPoint_6Month_Client_Letters
set		batch		= @batch_id
where	date_sent	= @today;

-- Mark the date generated for the letters. There is no need to generate a second note.
update	ClearPoint_6Month_Client_Letters
set		date_sent	= @today
where	batch		= @batch_id;

-- Move the date for the next letter ahead by 6 months
update	ClearPoint_6Month_Client_Letters
set		next_date_to_send	= dateadd(m, 6, next_date_to_send)
where	batch				= @batch_id
and		next_date_to_send   < @to_date;

-- Determine the client information for the result
select c.client, '00000' as postalcode, c.office as office, convert(varchar(80),null) as office_name, convert(varchar(800), null) as office_address, convert(varchar(80), null) as office_phone, convert(varchar(80),null) as client_name, convert(varchar(800),null) as client_address, c.salutation as salutation, c.counselor as counselor, convert(varchar(80),null) as counselor_name, convert(varchar(50),'en-US') as language, convert(varchar(512),'') as client_email
into	#clients
from	ClearPoint_6Month_Client_Letters x
inner join clients c on x.client = c.client
where	batch	 = @batch_id;

-- Set the email address for the clients
update	#clients
set		client_email	= ltrim(rtrim(pe.Address))
from	#clients x
inner join people p on x.client = p.client
inner join EmailAddresses pe on p.EmailID = pe.Email
where	pe.Address is not null
and		pe.Validation = 1;

-- Insert the office information
update	#clients
set		office_name		= o.name,
		office_address	= dbo.address_block_8(dbo.format_address_line_1(a.house, a.direction, a.street, a.suffix, a.modifier, a.modifier_value), a.address_line_2, dbo.format_city_state_zip(a.city, a.state, a.postalcode), default, default, default, default, default),
		office_phone	= dbo.format_TelephoneNumber (o.TelephoneID)
from	#clients c
inner join offices o on c.office = o.office
left outer join addresses a on o.addressid = a.address;

-- Insert the counselor information
update	#clients
set		counselor_name = dbo.format_normal_name(default,cox.first,default,cox.last,default),
		language = isnull(l.LanguageID,'en-US')
from	#clients c
inner join clients cx on c.client = cx.client
inner join counselors co on cx.counselor = co.counselor
left outer join attributetypes l on cx.language = l.oID
left outer join names cox on co.nameid = cox.name;

-- Insert the people information
update	#clients
set		client_name	= dbo.format_normal_name(pn.prefix, pn.first, pn.middle, pn.last, pn.suffix)
from	#clients c
inner join people p on c.client = p.client and 1 = p.relation
left outer join names pn with (nolock) on p.nameid = pn.name;

-- Insert the client infomration
update	#clients
set		client_address	= dbo.address_block_8(dbo.format_address_line_1(a.house, a.direction, a.street, a.suffix, a.modifier, a.modifier_value), a.address_line_2, dbo.format_city_state_zip(a.city, a.state, a.postalcode), default, default, default, default, default),
		salutation		= c.salutation,
		postalcode		= isnull(left(a.postalcode,5),'00000')		
from	#clients x
inner join clients c on x.client = c.client
left outer join addresses a on c.addressid = a.address;

-- Define the salutation with the name as needed
update	#clients
set		salutation = client_name
where	salutation is null;

-- Generate the client note that we are sending the letter
insert into client_notes (client, client_creditor, type, is_text, subject, note, dont_edit, dont_print, dont_delete)
select	client			as client,
		null			as client_creditor,
		3				as type,
		1				as is_text,
		'Printed letter ''6 month followup''' as subject,
		'Printed letter ''6 month followup'' for client' as note,
		1				as dont_edit,
		0				as dont_print,
		1				as dont_delete
from	#clients;

-- Return the information to the report
select *, getdate() as today, dbo.toHTML(office_address) as html_office_address from #clients order by postalcode, client;
drop table #clients;

return ( @@rowcount )
GO
