USE [DebtPlus]
GO
/****** Object:  StoredProcedure [dbo].[xpr_Sales_CreditorTypes]    Script Date: 09/15/2014 13:13:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[xpr_Sales_CreditorTypes] as
-- =============================================================================
-- ==            List of the types associated with the sales tool creditors   ==
-- =============================================================================

select	sales_creditor_type	as TypeLabel,
	description		as Description,
	DefaultPercentPayment	as DefaultPercentPayment,
	DefaultAmount		as DefaultAmount,
	DefaultPercentDisb	as DefaultPercentDisb
from	sales_creditor_types with (nolock)

return ( @@rowcount )
GO
