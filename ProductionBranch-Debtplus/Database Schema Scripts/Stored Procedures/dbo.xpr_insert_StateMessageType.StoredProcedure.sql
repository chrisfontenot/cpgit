USE [DebtPlus]
GO
/****** Object:  StoredProcedure [dbo].[xpr_insert_StateMessageType]    Script Date: 09/15/2014 13:13:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[xpr_insert_StateMessageType]( @description varchar(50), @Default as bit = 0, @ActiveFlag as bit = 0 ) as
-- =====================================================================================
-- ==           Add a State Message type field                                        ==
-- =====================================================================================
insert into StateMessageTypes ( [description], [Default], [ActiveFlag] ) values ( @description, @Default, @ActiveFlag )
return ( scope_identity() )
GO
