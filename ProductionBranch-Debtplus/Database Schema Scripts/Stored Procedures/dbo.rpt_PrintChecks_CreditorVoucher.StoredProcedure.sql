USE [DebtPlus]
GO
/****** Object:  StoredProcedure [dbo].[rpt_PrintChecks_CreditorVoucher]    Script Date: 09/15/2014 13:13:33 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[rpt_PrintChecks_CreditorVoucher] ( @Check AS INT ) AS
-- ====================================================================================
-- ==               Retrieve the information for the creditor voucher                ==
-- ====================================================================================

-- ChangeLog
--   5/8/2002
--      Use cc.client_name if one is supplied.
--   06/09/2009
--      Remove references to debt_number and use client_creditor
--   06/20/2011
--      Added transaction type to the criteria so that when it is used for other voucher
--        related functions, we don't pick up voids.
--      Corrected the billed creditor_type logic
--   12/7/2012
--      Printed the signor name rather than the applicant name on the check voucher

SELECT		t.client as 'client',
			isnull(t.account_number, cc.account_number) as 'account_number',

		case
			when ltrim(rtrim(cc.client_name)) <> '' then cc.client_name
			when cc.person is not null  			then dbo.format_reverse_name(default,pn1.first,pn1.middle,pn1.last,pn1.suffix)
													else dbo.format_reverse_name(default,pn2.first,pn2.middle,pn2.last,pn2.suffix)
		end as 'client_name',

		isnull(t.debit_amt,0)							as 'gross',
		case t.creditor_type when 'D' then isnull(t.fairshare_amt,0) else 0 end	as 'deducted',
		case when t.creditor_type in ('D', 'N') then 0 else isnull(t.fairshare_amt,0) end as 'billed',
		case t.creditor_type when 'D' then isnull(t.debit_amt,0) - isnull(t.fairshare_amt,0) else isnull(t.debit_amt,0) end as 'net'

FROM		registers_client_creditor t
LEFT OUTER JOIN	client_creditor cc WITH (NOLOCK) ON t.client_creditor = cc.client_creditor
LEFT OUTER JOIN people p1 WITH (NOLOCK) ON cc.person = p1.person
LEFT OUTER JOIN people p2 WITH (NOLOCK) ON t.client  = p2.client AND 1 = p2.relation
LEFT OUTER JOIN names pn1 with (nolock) on p1.nameid = pn1.name
LEFT OUTER JOIN names pn2 with (nolock) on p2.nameid = pn2.name
WHERE		t.trust_register = @Check
and			t.tran_type in ('AD','MD','CM')
ORDER BY	t.client

-- Return success to the caller
RETURN ( @@rowcount )
GO
