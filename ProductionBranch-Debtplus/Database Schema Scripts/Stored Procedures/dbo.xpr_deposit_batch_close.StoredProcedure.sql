USE [DebtPlus]
GO
/****** Object:  StoredProcedure [dbo].[xpr_deposit_batch_close]    Script Date: 09/15/2014 13:13:35 ******/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE PROCEDURE [dbo].[xpr_deposit_batch_close] ( @deposit_batch_id AS INT ) AS

-- ==================================================================================================
-- ==               Close the deposit batch                                                        ==
-- ==================================================================================================

-- ChangeLog
--   3/11/2002
--      Preserve the type of the deposit batch when the batch is split due to invalid items.
--   11/18/2002
--     Added bank information.
--   3/25/2003
--     Added deposits_in_trust update for the clients
--   7/16/2004
--     Limit the note field to 50 characters

-- Suppress intermediate result sets
set nocount on

-- Determine the type of the batch to be closed
declare	@batch_type	varchar(2)
select	@batch_type = batch_type
from	deposit_batch_ids
where	deposit_batch_id = @deposit_batch_id

-- If this is a creditor refund batch then close the batch using a different procedure.
if isnull(@batch_type,'CL') = 'CR'
begin
	Execute xpr_creditor_refund_batch_close @deposit_batch_id
	return ( 1 )
end

-- Find a subset of the transactions
select	deposit_batch_detail, client, amount, convert(int,ok_to_post) as ok_to_post
into	#deposit_batch_details
from	deposit_batch_details
where	client > 0
and	deposit_batch_id = @deposit_batch_id;

-- Determine the number of transactions in the batch
if isnull(@batch_type,'CL') <> 'AC'
begin
	if not exists (select * from #deposit_batch_details)
	begin
		-- Discard the working table
		drop table #deposit_batch_details

		-- Simply discard the batch ID if there are no transactions for this batch.
		delete
		from	deposit_batch_ids
		where	deposit_batch_id = @deposit_batch_id

		-- And pretend that everything was OK.
		return ( 0 )
	end

	-- Ensure that there is a valid client for the money
	update		#deposit_batch_details
	set		ok_to_post = 0
	from		#deposit_batch_details d
	left outer join	clients c with (nolock) ON d.client = c.client
	where		c.client is null

	-- Determine if there are any items "ok_to_post" in the batch
	if not exists (select * from #deposit_batch_details where ok_to_post <> 0)
	begin
		drop table #deposit_batch_details

		raiserror (50016, 16, 1)
		return	  ( 0 )
	end
end

-- Close the deposit batch to prevent further input into the batch
begin transaction

UPDATE	deposit_batch_ids
SET	date_closed		= getdate()
WHERE	deposit_batch_id	= @deposit_batch_id

-- Update the client deposit figures
select	d.client, sum(d.amount) as deposit_in_trust
into	#deposit_in_trust
from	deposit_batch_details d
inner join deposit_batch_ids i on d.deposit_batch_id = i.deposit_batch_id
where	d.ok_to_post		<> 0
and     i.trust_register is null
and     i.batch_type in ('AC','CL')
group by d.client;

update	clients
set		deposit_in_trust	= x.deposit_in_trust
from	clients c
inner join #deposit_in_trust x on c.client = x.client;

drop table #deposit_in_trust

-- Move the itens which are not valid for posting to a new batch
if exists (select * from #deposit_batch_details where ok_to_post = 0)
begin
	declare	@new_batch_id	int
	declare	@note		varchar(256)
	select	@note = isnull(note,'')
	from	deposit_batch_ids with (nolock)
	where	deposit_batch_id = @deposit_batch_id

	-- Add a bit of text for the reason that the batch is being created
	if @note not like 'Un-posted from %'
		select	@note	= 'Un-posted from ' + @note + ' deposit batch ' + convert(varchar, @deposit_batch_id)

	-- Create a new batch to hold the un-posted items
	insert into deposit_batch_ids (note,created_by,date_created,batch_type, bank)
	select	left(@note,50), created_by,date_created, @batch_type, bank
	from	deposit_batch_ids with (nolock)
	where	deposit_batch_id = @deposit_batch_id

	select	@new_batch_id	 = SCOPE_IDENTITY()

	-- Move the transactions to the new batch
	update	deposit_batch_details
	set	deposit_batch_id = @new_batch_id,
		ok_to_post	 = 0
	from	deposit_batch_details d
	inner join #deposit_batch_details x on d.deposit_batch_detail = x.deposit_batch_detail
	where	x.ok_to_post = 0
end

drop table #deposit_batch_details

-- Make the transactions perm.
commit

-- Return success to the caller
RETURN ( 1 )
GO
