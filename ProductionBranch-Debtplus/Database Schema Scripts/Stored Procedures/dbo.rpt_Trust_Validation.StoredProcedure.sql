USE [DebtPlus]
GO
/****** Object:  StoredProcedure [dbo].[rpt_Trust_Validation]    Script Date: 09/15/2014 13:13:34 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[rpt_Trust_Validation] ( @FromDate as datetime = null, @ToDate as datetime = null ) as

-- ==============================================================================================================
-- ==            Generate the trust account validation report                                                  ==
-- ==============================================================================================================

set nocount on

if @ToDate is null
	select	@ToDate = getdate()

if @Fromdate is null
	select	@FromDate = @ToDate

select	@FromDate = convert(datetime, convert(varchar(10), @FromDate, 101) + ' 00:00:00'),
	@ToDate   = convert(datetime, convert(varchar(10), @ToDate,   101) + ' 23:59:59')

-- Generate a starting balance of $0.00 for the clients
select	client, convert(money,0) as 'balance', @FromDate as from_date, @ToDate as to_date, convert(money, 0) as debit_amt, convert(money, 0) as credit_amt
into	#beginning_balance
from	clients;

-- Find the first date in the range desired
select	client, min(date_created) as date_created
into	#first_date
from	registers_client
where	date_created between @FromDate and @ToDate
group by client;

update	#beginning_balance
set	from_date = x.date_created
from	#beginning_balance a
inner join #first_date x on a.client = x.client;

-- Find the last date in the range desired
select	client, max(date_created) as date_created
into	#last_date
from	registers_client
where	date_created between @FromDate and @ToDate
group by client;

update	#beginning_balance
set	to_date = x.date_created
from	#beginning_balance a
inner join #last_date x on a.client = x.client;

-- Extract the date of last balance record for each client
select	client,max(date_created) as 'date_created'
into	#beginning_balance_1
from	registers_client
where	tran_type = 'BB'
and	date_created < @ToDate
group by client;

update	#beginning_balance
set	balance = isnull(c.credit_amt,0),
	from_date = convert(datetime, convert(varchar(10), c.date_created, 101))
from	#beginning_balance a
inner join #beginning_balance_1 b on a.client = b.client
inner join registers_client c on b.client = c.client and b.date_created = c.date_created
where	c.tran_type = 'BB';

-- Calculate the changes to the trust balance
select	rc.client, sum(isnull(rc.debit_amt,0)) as 'debit_amt', sum(isnull(rc.credit_amt,0)) as 'credit_amt'
into	#beginning_balance_2
from	registers_client rc
inner join #beginning_balance a on rc.client = a.client
where	rc.tran_type not in ('BB', 'EB')
and	rc.client > 0
and	rc.date_created between a.from_date and a.to_date
group by rc.client;

update	#beginning_balance
set	credit_amt	= x.credit_amt,
	debit_amt	= x.debit_amt
from	#beginning_balance a
inner join #beginning_balance_2 x on a.client = x.client;

-- Return the results for the report
select x.client,x.balance,x.from_date,x.to_date,c.held_in_trust,x.debit_amt,x.credit_amt,dbo.format_reverse_name(default,pn.first,pn.middle,pn.last,default) as 'name', c.active_status
from #beginning_balance x
inner join clients c on x.client = c.client
left outer join people p on x.client = p.client and 1 = p.relation
left outer join names pn with (nolock) on p.nameid = pn.name
where x.client > 0
and (x.balance > 0
or  c.held_in_trust > 0
or  x.debit_amt > 0
or  x.credit_amt > 0)
order by 1;

drop table #first_date
drop table #last_date
drop table #beginning_balance
drop table #beginning_balance_1;
drop table #beginning_balance_2;

return ( @@rowcount )
GO
