USE [DebtPlus]
GO
/****** Object:  StoredProcedure [dbo].[xpr_insert_Housing_CounselorAttributeType]    Script Date: 09/15/2014 13:13:35 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[xpr_insert_Housing_CounselorAttributeType] ( @description as varchar(50), @hud_9902_section as varchar(256) = null, @ActiveFlag as bit = 1 ) as
-- ==========================================================================================
-- ==   Procedure for .NET to insert items into the Housing_CounselorAttributeTypes table  ==
-- ==========================================================================================
	insert into HousingCounselorAttributeTypes ( [description],[hud_9902_section],[ActiveFlag] ) values ( @description,@hud_9902_section,@ActiveFlag )
	return ( scope_identity() )
GO
