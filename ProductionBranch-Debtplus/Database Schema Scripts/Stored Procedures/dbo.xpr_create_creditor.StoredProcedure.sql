USE [DebtPlus]
GO
/****** Object:  StoredProcedure [dbo].[xpr_create_creditor]    Script Date: 09/15/2014 13:13:34 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE PROCEDURE [dbo].[xpr_create_creditor] ( @Type as VARCHAR(1) ) AS
-- ====================================================================================================
-- ==                   Create a creditor of the indicated type                                      ==
-- ====================================================================================================

-- ChangeLog
--   12/12/2003
--      Allow for the possibility of a creditor having "0000" as the ID.
--   3/14/2004
--      Support for more than 10,000 creditors in a type
--   08/20/2004
--     Removed fairshare_pct_* and creditor_type_* from the creditor. Added creditor_contribution_pct link.
--   8/2/2013
--     Set the default for all drop reasons to be "true"

-- Do not generate intermediate result sets
SET NOCOUNT ON
SET xact_abort ON

-- Find the creditor ID from the system
DECLARE	@creditor_id		INT
DECLARE @Creditor			VARCHAR(10)

-- Find the default values for the creditor from the creditor types table
DECLARE	@DefaultBill		VarChar(1)
DECLARE	@DefaultPct			FLOAT
DECLARE	@Cycle				VarChar(1)
DECLARE	@proposals			int
DECLARE	@chks_per_invoice	int

SELECT	@DefaultBill	= status,
		@DefaultPct		= contribution_pct,
		@Cycle			= cycle,
		@proposals		= proposals
FROM	creditor_types
WHERE	type = @Type

if isnull(@proposals,3) not in (1,2)
	select	@proposals	= 1

select	@proposals	= @proposals - 1

IF @DefaultBill IS NULL
	SELECT @DefaultBill = 'D'

-- Default the percentage to 0.0
IF @DefaultPct IS NULL
	SELECT @DefaultPct = 0.0

-- Make the percentage rate legal
select @DefaultPct = dbo.valid_fairshare_pct(@defaultpct)

-- Make the percentage rate and deduction value consistent
IF @DefaultPct <= 0.0
	SELECT @DefaultBill = 'N'

IF @DefaultBill = 'N'
	SELECT @DefaultPct = 0.0

-- Default the billing cycle to monthly if one is not supplied
IF @Cycle IS NULL
	SELECT @Cycle = 'M'

-- Take the first creditor class in the creditor_classes table
declare	@creditor_class	int
select	@creditor_class = min(creditor_class)
from	creditor_classes

if @creditor_class is null
	select @creditor_class = 1	-- Should not happen. Punt if it does.

-- Find the number of checks per invoice
SELECT	@chks_per_invoice	= chks_per_invoice
from	config with (nolock)

if isnull(@chks_per_invoice,-1) < 0
	select	@chks_per_invoice = 0

begin transaction

-- Generate a temporary creditor ID
select	@creditor	= @type + right('00000' + convert(varchar, @@SPID), 5) + 'TMP'

-- Find the bank account for checks if needed
declare	@check_bank	int
select	@check_bank	= min(bank)
from	banks
where	type		= 'C'
and	[default]	= 1

if @check_bank is null
	select	@check_bank	= min(bank)
	from	banks
	where	type		= 'C'

if @check_bank is null
	select	@check_bank	= 1

-- Attempt to add the creditor to the system
INSERT INTO creditors	( chks_per_invoice,	full_disclosure,	type,	creditor,	proposal_income_info,	date_created,	distrib_mtd,	distrib_ytd,	contrib_mtd_billed,	contrib_ytd_billed,	contrib_mtd_received,	contrib_ytd_received,	pledge_cycle,	pledge_bill_month,	pledge_amt,	mail_priority,				contrib_cycle,	contrib_bill_month,	usual_priority,	creditor_class,	creditor_name, check_bank, min_accept_per_bill )
VALUES			( @chks_per_invoice,	@proposals,		@type,	@Creditor,	0,			getdate(),	0,		0,		0,			0,			0,			0,			'M',		1,			0,		9,	@Cycle,		1,			9,		@creditor_class,'This creditor was just created and has no name', @check_bank, 0 )

select	@creditor_id	= SCOPE_IDENTITY()

-- If the creditor number is less than 10,000 then use the value with four leading zeros.
if @creditor_id < 10000
	select	@creditor	= @type + right('0000' + convert(varchar, @creditor_id), 4)
else
	select	@creditor	= @type + convert(varchar, @creditor_id)

-- Correct the creditor ID to match the generated value.
update	creditors
set		creditor		= @creditor
where	creditor_id		= @creditor_id

-- Add the record to the client_creditor_contribution table to make it "legal"
declare @effective_date		datetime
select	@effective_date = CONVERT(varchar(10), getdate(), 101)

-- Generate the contribution record for the creditor
declare	@creditor_contribution_pct	int
execute @creditor_contribution_pct = xpr_insert_creditor_contribution_pcts @creditor, @effective_date, @DefaultBill, @DefaultBill,@DefaultPct, @DefaultPct

-- Set the linkage pointer to the creditor information
update	creditors
set		creditor_contribution_pct	= @creditor_contribution_pct
where	creditor					= @creditor

-- Set the RPPS drop notice types all to be "true".
insert into [creditor_drop_reasons] ([creditor],[rpps_code])
select distinct @creditor, rpps_code
from drop_reasons WITH (NOLOCK)
where len(rpps_code) = 2
and   rpps_code is not null
and   ActiveFlag <> 0

commit transaction

-- Return the creditor to the caller
select	@creditor as 'creditor'
RETURN ( @creditor_id )
GO
