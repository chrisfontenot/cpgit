USE [DebtPlus]
GO
/****** Object:  StoredProcedure [dbo].[xpr_rpps_response_process_COA]    Script Date: 09/15/2014 13:13:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[xpr_rpps_response_process_COA] ( @rpps_response_file as int ) AS
-- ==========================================================================================
-- ==           Process the sequence needed for account number notifications if needed     ==
-- ==========================================================================================

-- ChangeLog

-- Return success
RETURN ( 1 )
GO
