USE [DebtPlus]
GO
/****** Object:  StoredProcedure [dbo].[MONTHLY_clients]    Script Date: 09/15/2014 13:13:32 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[MONTHLY_clients] AS

-- ===========================================================================================================
-- ==            Update the information for the client at the end of the month                              ==
-- ===========================================================================================================

-- ChangeLog
--   12/26/2002
--     Added "statement_dp", "statement_adj", and "statement_trust" to the client update

-- Initialize for processing
BEGIN TRANSACTION
SET XACT_ABORT ON
SET NOCOUNT ON

-- Date for the period being processed. This is "last" month
declare	@month_start	datetime
declare	@month_end		datetime

select	@month_end		= convert(datetime, convert(varchar,month(getdate())) + '/01/' + convert(varchar,year(getdate())) + ' 00:00:00' )
select	@month_start	= dateadd(month, -1, @month_end)

-- Some agencies try to run this manually before the start of the month. Try to accomodate them.
-- This is a kludge. It is normally run by the schedule on the 1st of the month. So, assume that days 1 ... 9 of the month are the scheduled dates. Others are manual executions.
if datepart(d, getdate()) > 9
	select	@month_end		= dateadd(month, 1, @month_end),
			@month_start	= dateadd(month, 1, @month_start)

-- Insert the starting balance for the current month. The "BB" is a credit transaction.
insert into registers_client (tran_type, client, credit_amt, date_created, item_date, created_by)
select	'BB', client, held_in_trust, @month_end, @month_end, 'sa'
from	clients WITH (NOLOCK)
where	active_status in ('A', 'AR')
or		held_in_trust != 0

-- Count the number of months that the client is on the program
update	clients
set	program_months = isnull(program_months,0) + 1
where	active_status in ('A', 'AR')

COMMIT TRANSACTION
RETURN ( 1 )
GO
