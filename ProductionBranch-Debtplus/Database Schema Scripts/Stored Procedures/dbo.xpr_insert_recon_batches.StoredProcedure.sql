USE [DebtPlus]
GO
/****** Object:  StoredProcedure [dbo].[xpr_insert_recon_batches]    Script Date: 09/15/2014 13:13:35 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[xpr_insert_recon_batches] ( @note as varchar(50) = NULL ) as
	insert into recon_batches (note) values (@note)
	return (scope_identity())
GO
