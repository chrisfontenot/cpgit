USE [DebtPlus]
GO
/****** Object:  StoredProcedure [dbo].[xpr_delete_creditor_addresses]    Script Date: 09/15/2014 13:13:35 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[xpr_delete_creditor_addresses] ( @oID as int ) as
-- ==========================================================================================================
-- ==         Delete a creditor address item for a creditor                                                ==
-- ==========================================================================================================

delete
from	creditor_addresses
where	oID = @oID

return ( @@rowcount )
GO
