USE [DebtPlus]
GO
/****** Object:  StoredProcedure [dbo].[xpr_creditor_invoice_detail]    Script Date: 09/15/2014 13:13:34 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[xpr_creditor_invoice_detail] (@Creditor AS typ_creditor) AS
-- ================================================================
-- ==         Retrieve the Invoice Detail Information            ==
-- ================================================================
SELECT		r.invoice_register	as 'invoice',
		r.inv_date		as 'inv_date',
		r.inv_amount		as 'inv_amt',
		r.pmt_date		as 'pmt_date',
		r.pmt_amount		as 'pmt_amt',
		r.adj_date		as 'adj_date',
		r.adj_amount		as 'adj_amt'

FROM		registers_invoices r

WHERE		r.creditor = @Creditor
AND		(isnull(r.inv_amount,0) > (isnull(r.pmt_amount,0) + isnull(r.adj_amount,0)))

RETURN ( @@rowcount )
GO
