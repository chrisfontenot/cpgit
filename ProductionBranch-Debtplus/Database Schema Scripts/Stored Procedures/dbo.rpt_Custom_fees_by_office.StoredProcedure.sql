USE [DebtPlus]
GO
/****** Object:  StoredProcedure [dbo].[rpt_Custom_fees_by_office]    Script Date: 09/15/2014 13:13:33 ******/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE PROCEDURE [dbo].[rpt_Custom_fees_by_office] ( @From_Date as datetime = null, @To_date as datetime = null ) as
-- =====================================================================================================
-- ==            List the fees by each office                                                         ==
-- =====================================================================================================


-- Suppress intermediate results
set nocount on

-- These are the creditors that we wish to process

declare	@X0001_creditor		varchar(10)
declare	@X0002_creditor		varchar(10)
declare @X0003_creditor		varchar(10)

select	@X0001_creditor		= 'X0001',
	@X0002_creditor		= 'X0002',
	@X0003_creditor		= 'X0003'
	
if @to_date is null
	select	@to_date = getdate()

if @from_date is null
	select	@from_date = @to_date

select	@from_date = convert(datetime, convert(varchar(10), @from_date, 101) + ' 00:00:00'),
	@to_date   = convert(datetime, convert(varchar(10), @to_date,   101) + ' 23:59:59')

-- Monthly fee
select	client, sum(debit_amt) as 'X0001'
into	#X0001_1
from	registers_client_creditor with (nolock)
where	creditor = @X0001_creditor
and	tran_type in ('AD','MD','CM','BW')
and	date_created between @from_date and @to_date
group by client;

select	o.office, o.name, sum(a.X0001) as 'X0001'
into	#X0001_2
from	#X0001_1 a
left outer join clients c on a.client = c.client
left outer join offices o on c.office = o.office
group by o.office, o.name;

-- Second creditor
select	client, sum(debit_amt) as 'X0002'
into	#X0002_1
from	registers_client_creditor with (nolock)
where	creditor = @X0002_creditor
and	tran_type in ('AD','MD','CM','BW')
and	date_created between @from_date and @to_date
group by client;

select	o.office, o.name, sum(a.X0002) as 'X0002'
into	#X0002_2
from	#X0002_1 a
left outer join clients c on a.client = c.client
left outer join offices o on c.office = o.office
group by o.office, o.name;

-- Third creditor
select	client, sum(debit_amt) as 'X0003'
into	#X0003_1
from	registers_client_creditor with (nolock)
where	creditor = @X0003_creditor
and	tran_type in ('AD','MD','CM','BW')
and	date_created between @from_date and @to_date
group by client;

select	o.office, o.name, sum(a.X0003) as 'X0003'
into	#X0003_2
from	#X0003_1 a
left outer join clients c on a.client = c.client
left outer join offices o on c.office = o.office
group by o.office, o.name;

-- Find the offices in the list
create table #offices (office int null);

insert into #offices (office)
select isnull(office,-1)
from #X0001_2
union
select isnull(office,-1)
from #X0002_2
union
select isnull(office,-1)
from #X0003_2
union
select office
from offices with (nolock);

-- Return the information for the office statistcs
select		x.office as 'no',
		isnull(o.name,'None') as 'name',

		-- creditors that we want to know
		isnull(a.X0001,0) as 'COL_1',
		isnull(b.X0002,0) as 'COL_2',
		isnull(c.X0003,0) as 'COL_3',

		-- reserved for future expansion
		convert(money,null) as 'COL_4',
		convert(money,null) as 'COL_5',
		convert(money,null) as 'COL_6',
		convert(money,null) as 'COL_7',
		convert(money,null) as 'COL_8',
		convert(money,null) as 'COL_9',
		convert(money,null) as 'COL_10',
		convert(money,null) as 'COL_11'
from #offices x
left outer join offices  o with (nolock) on x.office = o.office
left outer join #X0001_2 a on x.office = a.office
left outer join #X0002_2 b on x.office = b.office
left outer join #X0003_2 c on x.office = c.office

order by 2, 1

drop table #offices
drop table #X0001_1
drop table #X0001_2
drop table #X0002_1
drop table #X0002_2
drop table #X0003_1
drop table #X0003_2
GO
