USE [DebtPlus]
GO
/****** Object:  StoredProcedure [dbo].[ocs_claim_client]    Script Date: 06/18/2015 12:25:40 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO



-- =============================================
-- Author:		Brandon Wilhite
-- Create date: 
-- Description:	Allows a user to 'claim' an OCS Client only if not already claimed that day.  Atomic
-- =============================================
ALTER PROCEDURE [dbo].[ocs_claim_client] 
	-- Add the parameters for the stored procedure here
	@OCSClientID int = 0,
	@UserName varchar(50) = '',
	@Success bit OUTPUT
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	
	begin transaction

	begin try
		declare @affected int
		SET @affected = 0

		UPDATE OCS_Client
		set ClaimedBy = @UserName,
		ClaimedDate = GETDATE()
		where Id = @OCSClientID
		and (
			ClaimedDate is null 
			or 
			CAST(FLOOR(CAST( getdate() AS float)) AS datetime) <> CAST(FLOOR(CAST( ClaimedDate AS float)) AS datetime)
			)
		and Archive = 0

		set @affected = @@ROWCOUNT

	  IF( @affected <> 1)
	    ROLLBACK
	  else
        commit transaction

	end try

	begin catch
	  rollback transaction
	end catch

	if(@affected = 1)
		set @Success = 1
	else
		set @Success = 0
END


