USE [DebtPlus]
GO
/****** Object:  StoredProcedure [dbo].[lst_disbursement_creditor_note_types]    Script Date: 09/15/2014 13:13:32 ******/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE PROCEDURE [dbo].[lst_disbursement_creditor_note_types] AS

SELECT		disbursement_creditor_note_type		as 'item_key',
		description				as 'description'
FROM		disbursement_creditor_note_types
ORDER BY	disbursement_creditor_note_type

return ( @@rowcount )
GO
