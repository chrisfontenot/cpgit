SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[xpr_housing_arm_v4_client_fetch] as
BEGIN
	-- =============================================================================
	-- ==     Retrieve the client information                                     ==
	-- =============================================================================
	select * from ##hud_9902_clients order by hcs_id, Client_ID_Num
	return @@ROWCOUNT
END

GO
