USE [DebtPlus]
GO
/****** Object:  StoredProcedure [dbo].[lst_creditor_contributions]    Script Date: 09/15/2014 13:13:32 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE PROCEDURE [dbo].[lst_creditor_contributions] ( @Creditor AS VarChar(10) ) AS

-- =================================================================================================================
-- ==            Retrieve the creditor contribution list                                                          ==
-- =================================================================================================================

SELECT	creditor_contribution_pct		as 'item_key',
	effective_date				as 'effective_date',
	fairshare_pct_check			as 'fairshare_pct_check',
	fairshare_pct_eft			as 'fairshare_pct_eft',

	-- This is obsolete. It will be removed in the next version
	case isnull(creditor_type_eft,'D')
		when 'B' then 'Bill'
		when 'D' then 'Deduct'
		else 'None'
	end					as 'creditor_type',


	case isnull(creditor_type_check,'N')
		when 'B' then 'Bill'
		when 'D' then 'Deduct'
		else 'None'
	end					as 'creditor_type_check',

	case isnull(creditor_type_eft,'N')
		when 'B' then 'Bill'
		when 'D' then 'Deduct'
		else 'None'
	end					as 'creditor_type_eft'

FROM	creditor_contribution_pcts WITH (NOLOCK)
WHERE	creditor = @Creditor
ORDER BY effective_date DESC

RETURN ( @@rowcount )
GO
