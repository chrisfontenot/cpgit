USE [DebtPlus]
GO
/****** Object:  StoredProcedure [dbo].[rpt_unpaid_clients]    Script Date: 09/15/2014 13:13:34 ******/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE PROCEDURE [dbo].[rpt_unpaid_clients] AS

-- ====================================================================================================================
-- ==            2nd version of the unpaid client listing                                                            ==
-- ====================================================================================================================

-- ChangeLog
--   1/19/2001
--      Changed to reflect the new values for the last payment date and amount fields in the client_creditor table.

-- Suppress intermediate results
set nocount on

-- Construct the list of active clients
create table #t_active_clients (
	client			int,
	active_status		varchar(3)	null,
	last_deposit_date	datetime	null,
	last_deposit_amount	money		null,
	last_disb_date		datetime	null,
	[name]			varchar(80)	null	
);
CREATE unique clustered index ix1_temp_unpaid_clients_2_1 on #t_active_clients (
	client
);

declare	@first_date	Datetime
declare	@month_1	Datetime
declare	@month_2	Datetime
declare	@month_3	Datetime
declare	@month_4	Datetime

-- Deterine the cutoff dates
set	@first_date	= convert(datetime, convert(varchar,datepart(year,getdate())) + '-' + convert(varchar,datepart(month,getdate())) + '-01 00:00:00')
set	@month_1	= dateadd(month, -1, @first_date)
set	@month_2	= dateadd(month, -1, @month_1)
set	@month_3	= dateadd(month, -1, @month_2)
set	@month_4	= dateadd(month, -1, @month_3)

-- Generate a list of the active clients
insert into #t_active_clients (client, active_status, last_deposit_date, last_Deposit_amount, [name], last_disb_date)
select	c.client,
	c.active_status,
	null,
	null,
	dbo.format_normal_name(pn.prefix,pn.first,pn.middle,pn.last,pn.suffix),
	max(rcc.date_created)
from	clients c			WITH (NOLOCK)
left outer join people p		WITH (NOLOCK) on c.client = p.client AND 1 = p.relation
left outer join names pn with (nolock) on p.nameid = pn.name
left outer join client_creditor	cc	WITH (NOLOCK) on c.client = cc.client and cc.reassigned_debt = 0
left outer join registers_client_creditor rcc with (nolock) on cc.last_payment = rcc.client_creditor_register
where	c.active_status in ('A', 'AR')
group by c.client, c.active_status, pn.prefix, pn.first, pn.middle, pn.last, pn.suffix
order by 1

-- Generate the list of deposits
select	client	as 'client',
	date_created	as 'last_deposit_date',
	sum(credit_amt)	as 'amount'
into	#t_deposit_dates
from	registers_client
where	tran_type = 'DP'
and	client in (select client from #t_active_clients)
group by client, date_created
order by date_created
CREATE index ix1_unpaid_clients_2_2 on #t_deposit_dates (
	client,
	last_deposit_date
);

select	client, max(last_deposit_date) as last_deposit_date
into	#t_last_deposit_date
from	#t_deposit_dates
group by client

-- Update the last deposit date
update	#t_active_clients
set	last_deposit_date = x.last_deposit_date
from	#t_active_clients i
inner join #t_last_deposit_date x on i.client = x.client

-- Update the deposit amount for the indicated date
update	#t_active_clients
set	last_deposit_amount = d.amount
from	#t_active_clients t
inner join #t_deposit_dates d on d.client = t.client and d.last_deposit_date = t.last_deposit_date

-- Return the information for the operation
select	client,
	active_status,
	case	when last_deposit_date is null		then 0
		when last_deposit_date >= @first_date	then 1
		when last_deposit_date >= @month_1	then 2
		when last_deposit_date >= @month_2	then 3
		when last_deposit_date >= @month_3	then 4
							else 5
	end as [group],

	last_deposit_date,
	last_deposit_amount,
	last_disb_date,
	[name]
from	#t_active_clients

order by 3, 1

drop table #t_active_clients
drop table #t_deposit_dates
drop table #t_last_deposit_date

return ( @@rowcount )
GO
