USE [DebtPlus]
GO
/****** Object:  StoredProcedure [dbo].[xpr_PrintChecks_CheckList]    Script Date: 09/15/2014 13:13:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE PROCEDURE [dbo].[xpr_PrintChecks_CheckList] ( @bank as int = -1 ) AS

-- ===========================================================================================
-- ==               Return the list of pending checks for the selection process             ==
-- ===========================================================================================

-- Find the suitable bank id
if isnull(@bank,-1) <= 0
	select	@bank		= min(bank)
	from	banks with (nolock)
	where	type		= 'C'
	and	[default]	= 1

if @bank is null
	select	@bank		= min(bank)
	from	banks with (nolock)
	where	type		= 'C'

if @bank is null
	select	@bank		= 1

SELECT		trust_register	as 'item_key',
			date_created	as 'item_date',
			debit_amt		as 'item_amount',
			payee			as 'item_payee'
FROM		view_trust_register WITH (NOLOCK)
WHERE		cleared		= 'P'
AND			debit_amt	> 0
AND			bank		= @bank
ORDER BY	trust_register

RETURN ( @@rowcount )
GO
