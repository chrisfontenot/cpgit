USE [DebtPlus]
GO
/****** Object:  StoredProcedure [dbo].[xpr_appt_confirm]    Script Date: 09/15/2014 13:13:34 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[xpr_appt_confirm] ( @client_appointment AS INT, @confirmation_status AS INT = 0, @confirmation_counselor as int = -1, @confirmation_telephone as varchar(50) = null, @confirmation_note as varchar(256) = null ) AS

-- ================================================================================================
-- ==            Update the confirmation status for the indicated appointment                    ==
-- ================================================================================================

-- Disable intermediate result sets
SET NOCOUNT ON

-- Change the appointment confirmation status
UPDATE	client_appointments
SET	confirmation_status	= @confirmation_status,
	date_confirmed		= getdate(),
	counselor		= case when isnull(@confirmation_counselor,-1) < 0 then counselor else @confirmation_counselor end,
	callback_ph		= case when ltrim(rtrim(isnull(@confirmation_telephone,''))) = '' then callback_ph else @confirmation_telephone end
WHERE	client_appointment	= @client_appointment

RETURN ( @@rowcount )
GO
