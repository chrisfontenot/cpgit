USE [DebtPlus]
GO
/****** Object:  StoredProcedure [dbo].[xpr_special_counseling_activity_log]    Script Date: 09/15/2014 13:13:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE PROCEDURE [dbo].[xpr_special_counseling_activity_log] ( @from_date datetime = null, @to_date datetime = null, @special int = 0 ) as

set nocount on

if @to_date is null
	select @to_date = getdate()

if @from_date is null
	select @from_date = @to_date

select @from_date = convert(varchar(10), @from_date, 101),
	   @to_date = convert(varchar(10), @to_date, 101) + ' 23:59:59'

-- Find the appointment list
select	hi.client,
	convert(varchar(10), hi.interview_date, 101) as start_time,
	isnull(dbo.format_normal_name(default,con.first,default,con.last,default), hi.interview_counselor) as counselor,
	isnull(hs.section_label,'7.a.1') as hud_9902_section,
	convert(varchar, round(convert(float, (select sum(minutes) from hud_transactions ht where ht.hud_interview = hi.hud_interview))/60, 3)) as counseling_hours,
	convert(varchar(20), '0') as administrative_hours,
	convert(varchar(10), 'I') as initial_followup
into #client_appointments
from hud_interviews hi
inner join counselors co on hi.interview_counselor = co.person
left outer join names con on co.NameID = con.name
inner join hud_cars_9902_summary hs on hi.interview_type = hs.hud_interview AND hi.hud_result = hs.hud_result
where hi.interview_date between @from_date and @to_date
and   hi.interview_type is not null
and   hi.hud_result is not null

-- Delete the extraneous items
if @special = 0
	delete
	from	#client_appointments
	where	hud_9902_section not in ('7.c.01', '7.c.02', '7.c.03')

-- Create a results table with the item results for the workshop
create table #results ( page varchar(80), location varchar(20), value varchar(20));

-- Break the information down by line
declare @current_lnno int
declare @current_page varchar(80)
select  @current_lnno = 12, @current_page = '2' --'2. Counseling Activity Log'

declare @current_hud_9902_section varchar(20)
declare @current_client varchar(20)
declare @current_start_time varchar(10)
declare @current_counselor varchar(80)
declare @current_counseling_hours varchar(20)
declare @current_administrative_hours varchar(20)
declare @current_initial_followup varchar(10)

declare item_cursor cursor for
	select client, start_time, counselor, hud_9902_section, counseling_hours, administrative_hours, initial_followup
	from	#client_appointments
	order by client, start_time

open item_cursor
fetch item_cursor into @current_client, @current_start_time, @current_counselor, @current_hud_9902_section, @current_counseling_hours, @current_administrative_hours, @current_initial_followup

while @@fetch_status = 0
begin

	-- Include information common to all items
	insert into #results (page, location, value)
	select @current_page, 'A' + convert(varchar, @current_lnno), @current_hud_9902_section
	union all
	select @current_page, 'B' + convert(varchar, @current_lnno), @current_client
	union all
	select @current_page, 'C' + convert(varchar, @current_lnno), @current_start_time
	union all
	select @current_page, 'D' + convert(varchar, @current_lnno), @current_counselor
	union all
	select @current_page, 'E' + convert(varchar, @current_lnno), @current_counseling_hours
	union all
	select @current_page, 'F' + convert(varchar, @current_lnno), @current_administrative_hours

	select	@current_lnno = @current_lnno + 1

	fetch item_cursor into @current_client, @current_start_time, @current_counselor, @current_hud_9902_section, @current_counseling_hours, @current_administrative_hours, @current_initial_followup
end

close item_cursor
deallocate item_cursor

-- Return the results from the selections
select * from #results order by page, location
drop table #results
drop table #client_appointments

return ( 0 )
GO
