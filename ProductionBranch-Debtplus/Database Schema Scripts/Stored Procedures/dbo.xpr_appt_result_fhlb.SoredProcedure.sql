IF NOT EXISTS(SELECT * FROM SYSOBJECTS WHERE NAME=N'xpr_appt_result_fhlb' AND TYPE='P')
    EXEC ('CREATE PROCEDURE xpr_appt_result_fhlb AS')
GO
ALTER procedure [dbo].[xpr_appt_result_fhlb] ( @client_appointment as typ_key ) as

-- Suppress intermediate results
set nocount on

-- Information from the current appointment record
declare	@cur_client			typ_client
declare	@cur_partner		varchar(50)
declare	@cur_start_time		datetime
declare	@cur_end_time		datetime
declare	@cur_duration		int

-- Information about the applicant
declare	@ap_emailID			int
declare	@ap_nameID			int

-- Information about the email address
declare	@email_address		varchar(80)
declare	@email_name			varchar(80)

-- Information from the name record
declare	@name_string		varchar(80)

-- Information for the email template record
declare	@template_id		int

-- Find the email template
select	@template_id		= [oID]
from	[email_templates]
where	[description]		= 'FHLB Counseled'

-- There needs to be a row
if @@rowcount < 1
	return

-- Retrieve the current appointment
select	@cur_client				= [client],
		@cur_partner			= [partner],
		@cur_start_time			= [start_time],
		@cur_end_time			= [end_time]
from	[client_appointments]
where	[client_appointment]	= @client_appointment

-- There needs to be a row
if @@rowcount < 1
	return

-- Retrieve the information from the applicant record
select	@ap_emailID				= [emailID],
		@ap_nameID				= [nameID]
from	people
where	[client]				= @cur_client
and		[relation]				= 1

-- There needs to be a row
if @@rowcount < 1
	return

-- Find the email address
select	@email_address			= 'fhlbasap@fhlbatl.com',
		@email_name				= 'FHLB ASAP'

-- Testing only.
select	@email_address			= 'CAROLINE@DEBTPLUS.COM'

-- Find the applicant name information
select	@name_string			= dbo.format_normal_name(n.prefix, n.first, n.middle, n.last, n.suffix)
from	[names] n
where	n.name					= @ap_nameID

if @@rowcount < 1
	return
	
-- Generate the XML for the substituion block of the message
declare	@xml					varchar(max)
select	@xml =        '<substitution><field>client_id</field><value>' + dbo.format_client_id(@cur_client) + '</value></substitution>'
select	@xml = @xml + '<substitution><field>partner_code</field><value>' + isnull(@cur_partner,'') + '</value></substitution>'
select	@xml = @xml + '<substitution><field>start_date</field><value>' + convert(varchar(10), @cur_start_time, 101) + '</value></substitution>'
select	@xml = @xml + '<substitution><field>client_name</field><value>' + dbo.encode_xml_text (@name_string) + '</value></substitution>'
select	@xml = '<substitutions>' + @xml + '</substitutions>'

-- Insert the email message into the queue. The server will take it from this point.
insert into email_queue (template, email_address, email_name, substitutions, attempts)
values (@template_id, @email_address, @email_name, @xml, 0)
GO
