USE [DebtPlus]
GO
/****** Object:  StoredProcedure [dbo].[rpt_appt_counselor_templates]    Script Date: 09/15/2014 13:13:32 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE PROCEDURE [dbo].[rpt_appt_counselor_templates] AS
-- ======================================================================================================
-- ==            View the prototype week information for the appointments                              ==
-- ======================================================================================================

set nocount on

-- Fetch the information from the view
select		office					as 'office',
		DOW					as 'DOW',
		start_hour				as 'start_hour',
		type					as 'type',
		1					as 'available_slots',
		1					as 'full_schedule',
		counselor				as 'counselor'
from		view_appt_counselor_templates
order by	office, DOW, start_hour

return ( @@rowcount )
GO
