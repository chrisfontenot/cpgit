USE [DebtPlus]
GO
/****** Object:  StoredProcedure [dbo].[xpr_insert_asset_ids]    Script Date: 09/15/2014 13:13:35 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[xpr_insert_asset_ids] ( @description as varchar(50), @maximum as money = 0, @rpps_code as int = 0 ) as
-- ========================================================================================
-- ==   Procedure for .NET to insert items into the asset_ids table                      ==
-- ========================================================================================
	insert into asset_ids ( [description], maximum, rpps_code ) values ( @description, @maximum, @rpps_code )
	return ( scope_identity() )
GO
