USE [DebtPlus]
GO
/****** Object:  StoredProcedure [dbo].[xpr_debt_default]    Script Date: 09/15/2014 13:13:35 ******/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE PROCEDURE [dbo].[xpr_debt_default] ( @Client as int ) AS

select	stack_proration,
	null as threshold
from	clients
where	client = @client

RETURN ( @@rowcount )
GO
