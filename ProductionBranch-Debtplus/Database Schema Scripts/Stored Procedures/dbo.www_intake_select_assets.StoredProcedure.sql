USE [DebtPlus]
GO
/****** Object:  StoredProcedure [dbo].[www_intake_select_assets]    Script Date: 09/15/2014 13:13:34 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[www_intake_select_assets] (@intake_client as int, @intake_id as varchar(20), @asset_id as int = null) as

	if @asset_id is null
		select		v.*, a.description
		from		intake_assets v with (nolock)
		inner join	intake_clients c with (nolock) on v.intake_client = c.client
		inner join	asset_ids a with (nolock) on v.asset_id = a.asset_id
		where		c.intake_client	= @intake_client
		and			c.intake_id		= @intake_id
		and			v.asset_id		= @asset_id
	else
		select		v.*, a.description
		from		intake_assets v with (nolock)
		inner join	intake_clients c with (nolock) on v.intake_client = c.client
		inner join	asset_ids a with (nolock) on v.asset_id = a.asset_id
		where		c.intake_client	= @intake_client
		and			c.intake_id		= @intake_id

	return ( @@rowcount )
GO
