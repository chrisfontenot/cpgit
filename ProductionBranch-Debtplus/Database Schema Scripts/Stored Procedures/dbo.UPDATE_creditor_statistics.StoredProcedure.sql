USE [DebtPlus]
GO
/****** Object:  StoredProcedure [dbo].[UPDATE_creditor_statistics]    Script Date: 09/15/2014 13:13:34 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[UPDATE_creditor_statistics] AS

-- ===============================================================================================
-- ==            Correct the statistics for the creditor should it be required                  ==
-- ===============================================================================================

-- ChangeLog
--  10/10/2002
--    Corrected table for RC contributions. It was registers_client_creditor.
--  8/5/2008
--    Added "NA" transactions to the creditor contributions amount.
--  8/8/2012
--    RICHMOND: Per Jim Craig, removed the "NA" transactions. Only count "RC" transactions and deduction amounts.
--  10/15/2012
--    RICHMOND: Per Jim Craig, Added back the "NA" transactions.

update	creditors
set	distrib_ytd = 0,
	distrib_mtd = 0,
	contrib_mtd_billed = 0,
	contrib_ytd_billed = 0,
	contrib_mtd_received = 0,
	contrib_ytd_received = 0;

declare	@year_start		datetime
declare	@month_start	datetime
declare	@month_end		datetime

select	@year_start  = convert(datetime, '01/01/' + right('0000' + convert(varchar,year(getdate())), 4) + ' 00:00:00'),
		@month_start = convert(datetime, right('00' + convert(varchar,month(getdate())), 2) + '/01/' + right('0000' + convert(varchar,year(getdate())), 4) + ' 00:00:00')
select	@month_end   = dateadd(m, 1, @month_start)

select	creditor, sum(isnull(debit_amt,0)) as 'debit_amt', sum(isnull(credit_amt,0)) as 'credit_amt'
into	#distrib_ytd
from	registers_client_creditor
where	tran_type in ('AD', 'BW', 'MD', 'CM', 'RR', 'VD', 'RF')
and		date_created >= @year_start
and		date_created < @month_start
group by creditor;

select	creditor, sum(isnull(debit_amt,0)) as 'debit_amt', sum(isnull(credit_amt,0)) as 'credit_amt'
into	#distrib_mtd
from	registers_client_creditor
where	tran_type in ('AD', 'BW', 'MD', 'CM', 'RR', 'VD', 'RF')
and		date_created >= @month_start
and		date_created < @month_end
group by creditor;

select	creditor, sum(isnull(inv_amount,0)) as 'contrib_ytd_billed'
into	#contrib_ytd_billed
from	registers_invoices
where	inv_date >= @year_start
and		inv_date < @month_start
group by creditor;

select	creditor, sum(isnull(inv_amount,0)) as 'contrib_mtd_billed'
into	#contrib_mtd_billed
from	registers_invoices
where	inv_date >= @month_start
and		inv_date < @month_end
group by creditor;

select	creditor, sum(case when tran_type in ('RR', 'VD', 'RF') then 0 - isnull(fairshare_amt,0) else isnull(fairshare_amt,0) end) as 'contrib_mtd_received'
into	#contrib_mtd_received_a
from	registers_client_creditor
where	tran_type in ('AD', 'BW', 'MD', 'CM', 'RR', 'VD', 'RF')
and		creditor_type = 'D'
and		date_created >= @month_start
and		date_created < @month_end
group by creditor;

select	creditor, sum(case when tran_type in ('RR', 'VD', 'RF') then 0 - isnull(fairshare_amt,0) else isnull(fairshare_amt,0) end) as 'contrib_ytd_received'
into	#contrib_ytd_received_b
from	registers_client_creditor
where	tran_type in ('AD', 'BW', 'MD', 'CM', 'RR', 'VD', 'RF')
and		creditor_type = 'D'
and		date_created >= @year_start
and		date_created < @month_start
group by creditor;

select	creditor, sum(isnull(credit_amt,0)) as 'contrib_mtd_received'
into	#contrib_mtd_received_c
from	registers_creditor
where	date_created >= @month_start
and		date_created < @month_end
and		tran_type in ('RC','NA')
group by creditor;

select	creditor, sum(isnull(credit_amt,0)) as 'contrib_ytd_received'
into	#contrib_ytd_received_d
from	registers_creditor
where	date_created >= @year_start
and		date_created < @month_start
and		tran_type in ('RC','NA')
group by creditor;

-- disbursements year to date
update	creditors
set		distrib_ytd = x.debit_amt - x.credit_amt
from	creditors cr
inner join #distrib_ytd x on x.creditor = cr.creditor;

-- disbursements month to date
update	creditors
set		distrib_mtd = x.debit_amt - x.credit_amt
from	creditors cr
inner join #distrib_mtd x on x.creditor = cr.creditor;

-- Contributions billed year to date
update	creditors
set		contrib_ytd_billed = x.contrib_ytd_billed
from	creditors cr
inner join #contrib_ytd_billed x on x.creditor = cr.creditor;

-- Contributions billed month to date
update	creditors
set		contrib_mtd_billed = x.contrib_mtd_billed
from	creditors cr
inner join #contrib_mtd_billed x on x.creditor = cr.creditor;

-- Contributions received year to date
update	creditors
set		contrib_ytd_received = cr.contrib_ytd_received + x.contrib_ytd_received
from	creditors cr
inner join #contrib_ytd_received_b x on x.creditor = cr.creditor;

-- Contributions received month to date
update	creditors
set		contrib_mtd_received = cr.contrib_mtd_received + x.contrib_mtd_received
from	creditors cr
inner join #contrib_mtd_received_a x on x.creditor = cr.creditor;

-- Contributions received month to date
update	creditors
set		contrib_mtd_received = cr.contrib_mtd_received + x.contrib_mtd_received
from	creditors cr
inner join #contrib_mtd_received_c x on x.creditor = cr.creditor;

-- Contributions received year to date
update	creditors
set		contrib_ytd_received = cr.contrib_ytd_received + x.contrib_ytd_received
from	creditors cr
inner join #contrib_ytd_received_d x on x.creditor = cr.creditor;

drop table #distrib_ytd;
drop table #distrib_mtd;
drop table #contrib_ytd_billed;
drop table #contrib_mtd_billed;
drop table #contrib_mtd_received_a;
drop table #contrib_ytd_received_b;
drop table #contrib_mtd_received_c;
drop table #contrib_ytd_received_d;
GO
