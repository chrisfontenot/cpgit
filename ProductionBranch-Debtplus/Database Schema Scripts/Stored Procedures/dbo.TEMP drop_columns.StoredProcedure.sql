USE [DebtPlus]
GO
/****** Object:  StoredProcedure [dbo].[TEMP drop_columns]    Script Date: 09/15/2014 13:13:34 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create procedure [dbo].[TEMP drop_columns] as

-- drop merged ID columns
declare stmt_cursor cursor for
	select	'alter table [' + t.name + '] drop column [' + c.name + '];'
	from	sysobjects t
	inner join syscolumns c on t.id = c.id
	where	t.name not in ('numbers', 'dtproperties', 'sysdiagrams', 'CitiCreditors', 'AMI_hh_adjustment', 'obsolete hud_cars_9902_summary', 'round8_low_income_zips', 'round8_minority_zips', 'ZipCodeSearch-new', 'zip_msa', 'zip_to_timezone', 'rpps_transactions')
	and		t.type = 'U'
	and		c.name like 'old%'
	order by 1

declare	@stmt		varchar(800)
open stmt_cursor
fetch stmt_cursor into @stmt

while @@fetch_status = 0
begin
	print @stmt
 	exec ( @stmt )

	fetch stmt_cursor into @stmt
end

close stmt_cursor
deallocate stmt_cursor

-- Refresh the views
exec ('execute UPDATE_Refresh_Views')
GO
