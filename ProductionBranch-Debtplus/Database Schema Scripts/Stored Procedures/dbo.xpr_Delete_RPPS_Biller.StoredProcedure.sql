USE [DebtPlus]
GO
/****** Object:  StoredProcedure [dbo].[xpr_Delete_RPPS_Biller]    Script Date: 09/15/2014 13:13:35 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[xpr_Delete_RPPS_Biller] ( @BillerID AS typ_rpps_biller_id ) AS
-- =================================================================================================
-- ==               Remove the BILLER ID from the system                                          ==
-- =================================================================================================

-- ChangeLog
--   11/20/2002
--     Changed to use RPPS' biller_aba number rather than rpps_biller_id

DELETE
FROM	rpps_biller_ids
WHERE	rpps_biller_id = @BillerID

-- Return success to the caller
RETURN ( @@rowcount )
GO
