USE [DebtPlus]
GO
/****** Object:  StoredProcedure [dbo].[rpt_client_contributions]    Script Date: 09/15/2014 13:13:32 ******/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE PROCEDURE [dbo].[rpt_client_contributions] ( @From_Date as datetime = null, @To_Date as datetime = null ) as

-- =================================================================================================
-- ==            Client contributions by zipcode                                                  ==
-- =================================================================================================

-- ChangeLog
--   01/15/2002
--     Added creditor_classes table references and dropped "creditors.fee_creditor"

-- Initialize
set nocount on

if @To_Date is null
	select	@to_date = getdate()

if @from_date is null
	select	@from_date = @to_date

select	@from_date = convert(datetime, convert(varchar(10), @from_date, 101) + ' 00:00:00'),
	@to_date = convert(datetime, convert(varchar(10), @to_date, 101) + ' 23:59:59')

declare	@paf_creditor		varchar(10)
declare	@setup_creditor		varchar(10)
declare	@deduct_creditor	varchar(10)

select	@paf_creditor		= paf_creditor,
	@deduct_creditor	= deduct_creditor,
	@setup_creditor		= setup_creditor
from	config

-- Generate a list of the amounts by the setup creditor
create table #contrib_setup (client int, amount money null);
insert into #contrib_setup (client, amount)
select	rc.client, sum(rc.debit_amt) as amount
from	registers_client_creditor rc
left outer join creditors cr with (nolock) on rc.creditor = cr.creditor
LEFT OUTER JOIN creditor_classes ccl with (nolock) on cr.creditor_class = ccl.creditor_class
where	rc.tran_type in ('AD', 'BW', 'MD', 'CM')
and	rc.void = 0
and	rc.date_created between @from_date and @to_date
and	rc.creditor != @paf_creditor
and	(isnull(ccl.zero_balance,0) > 0 or rc.creditor = @setup_creditor)
and	rc.creditor != @deduct_creditor
group by rc.client;

-- Generate a list of the amounts by the fee creditor
create table #contrib_paf (client int, amount money null);
insert into #contrib_paf (client, amount)
select	rc.client, sum(rc.debit_amt) as amount
from	registers_client_creditor rc
where	rc.tran_type in ('AD', 'BW', 'MD', 'CM')
and	rc.void = 0
and	rc.date_created between @from_date and @to_date
and	rc.creditor = @paf_creditor
group by rc.client;

-- Generate a list of the fairshare by client
create table #contrib_fairshare (client int, amount money null);
insert into #contrib_fairshare (client, amount)
select	rc.client, sum(rc.fairshare_amt) as amount
from	registers_client_creditor rc
where	rc.tran_type in ('AD', 'BW', 'MD', 'CM')
and	rc.void = 0
and	rc.date_created between @from_date and @to_date
group by rc.client;

-- Build a list of the clients and their corresponding offices
select	client, left(isnull(a.postalcode,'00000'),5) as zipcode, convert(int, null) as office
into	#contrib_offices
from	clients c
left outer join addresses a on c.addressid = a.address;

-- Update the office information from the system
update #contrib_offices
set office = (
	select top 1	office
	from		zipcodes
	where		#contrib_offices.zipcode+'0000' between zip_lower and zip_upper
	order by	convert(int,zip_upper)-convert(int,zip_lower)
);

-- Replace the missing offices with a default value.
declare	@default_office int
select	@default_office = office
from	offices
where	type = 1

if @default_office is null
	select @default_office = office
	from	offices

if @default_office is null
	select @default_office = 1

-- Ensure that there is an office
update	#contrib_offices
set	office = @default_office
where	office is null
or	office not in (
	select	office
	from	offices
);

-- Return the information to the report
select	a.client			as 'client',
	c.active_status			as 'active_status',
	c.postalcode			as 'zipcode',
	a.office			as 'office',
	o.name				as 'office_name',
	dbo.format_reverse_name (default, pn.first, default, pn.last, default ) as 'client_name',
	isnull(a1.amount,0)		as 'fee_amount',
	isnull(a2.amount,0)		as 'paf_amount',
	isnull(a3.amount,0)		as 'fairshare_amount'

from	#contrib_offices a
inner join clients c with (nolock) on a.client = c.client
left outer join offices o with (nolock) on a.office = o.office
left outer join people p with (nolock) on a.client = p.client and 1 = p.relation
left outer join names pn with (nolock) on p.nameid = pn.name
left outer join #contrib_setup a1 on a.client = a1.client
left outer join #contrib_paf a2 on a.client = a2.client
left outer join #contrib_fairshare a3 on a.client = a3.client

where	isnull(a1.amount,0) > 0
or	isnull(a2.amount,0) > 0
or	isnull(a3.amount,0) > 0;

drop table #contrib_setup
drop table #contrib_paf
drop table #contrib_fairshare

return ( @@rowcount )
GO
