USE [DebtPlus]
GO
/****** Object:  StoredProcedure [dbo].[rpt_sales_plan]    Script Date: 09/15/2014 13:13:33 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE PROCEDURE [dbo].[rpt_sales_plan] ( @sales_file as int ) as

-- ===============================================================================================
-- ==            return the information for the dmp plan                                        ==
-- ===============================================================================================

select	creditor_type				as creditor_type,
	creditor_name				as creditor_name,
	isnull(balance,0)			as balance,
	isnull(plan_min_prorate,0) * 100.0	as min_prorate,
	isnull(plan_min_payment,0)		as minimum_payment,
	isnull(plan_payment,0)			as payment,
	isnull(plan_interest_rate,0) * 100.0	as interest_rate,
	isnull(plan_total_fees,0)		as total_fees,
	isnull(plan_finance_charge,0)		as finance_charge,
	isnull(plan_total_interest_fees,0)	as total_interest_fees,
	isnull(plan_principal,0)		as principal

from	sales_debts with (nolock)
where	sales_file		= @sales_file

return ( @@rowcount )
GO
