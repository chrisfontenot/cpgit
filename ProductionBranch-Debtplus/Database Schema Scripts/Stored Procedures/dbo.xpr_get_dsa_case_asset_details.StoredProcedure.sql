﻿USE [DEBTPLUS]
GO
/****** Object:  StoredProcedure [dbo].[xpr_get_dsa_case_asset_details]    Script Date: 12/4/2014 11:18:21 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE procedure [dbo].[xpr_get_dsa_case_asset_details] (
	@clientid	int,
	@propertyid	int
)
as
begin

	declare @home					money
	declare @primarycheckingaccount	money
	declare @savingsmoneymarket		money
	declare @certificateofdeposit	money
	declare @stockbonds				money
	declare @iramarket				money
	declare @esopaccount			money
	declare @lifeinsurance			money
	declare @otherassetbalance		money
	declare @anyotherassetbalance	money
	declare @otherrealestate		money
	declare @cars					money
	declare @other1					money
	declare @other2					money
	declare @other3					money
	declare @other4					money

	--------------------------------
	-- asset details
	--------------------------------
	select @home = ISNULL(LandValue, 0) + ISNULL(ImprovementsValue, 0)
	  from Housing_properties
	 where Residency = 1
	   and oID = @propertyid
	   and HousingID = @clientid;

	select @primarycheckingaccount = sum(current_value)
	  from secured_properties 
	 where client = @clientid 
	   and secured_type = 100

	select @savingsmoneymarket = sum(current_value)
	  from secured_properties 
	 where client = @clientid 
	   and secured_type = 110

	select @certificateofdeposit = sum(current_value)
	  from secured_properties 
	 where client = @clientid 
	   and secured_type = 120

	select @stockbonds = sum(current_value)
	  from secured_properties 
	 where client = @clientid 
	   and secured_type = 200

	select @iramarket = sum(current_value)
	  from secured_properties 
	 where client = @clientid 
	   and secured_type = 300

	select @esopaccount = sum(current_value)
	  from secured_properties 
	 where client = @clientid 
	   and secured_type = 310

	select @lifeinsurance = sum(current_value)
	  from secured_properties 
	 where client = @clientid 
	   and secured_type = 1002

	select @otherassetbalance = sum(current_value)
	  from secured_properties 
	 where client = @clientid 
	   and secured_type IN (720, 740, 750)

	select @anyotherassetbalance = sum(current_value)
	  from secured_properties 
	 where client = @clientid 
	   and secured_type = 130

	select @otherrealestate = case when ImprovementsValue is null then LandValue else ImprovementsValue end
	  from Housing_properties
	 where Residency <> 1
	   and oID = @propertyid
	   and HousingID = @clientid;

	select @cars = sum(current_value)
	  from secured_properties 
	 where client = @clientid 
	   and secured_type = 400

	select @other1 = sum(current_value)
	  from secured_properties 
	 where client = @clientid 
	   and secured_type = 410

	select @other2 = sum(current_value)
	  from secured_properties 
	 where client = @clientid 
	   and secured_type = 420

	select @other3 = sum(current_value)
	  from secured_properties 
	 where client = @clientid 
	   and secured_type = 600

	select @other4 = sum(current_value)
	  from secured_properties 
	 where client = @clientid 
	   and secured_type = 1003

	select @home as Home
		 , @primarycheckingaccount as PrimaryCheckingAccount
		 , @savingsmoneymarket as SavingsMoneyMarket
		 , @certificateofdeposit as CertificateOfDeposit
		 , @stockbonds as StockBonds
		 , @iramarket as IRAMarket
		 , @esopaccount as ESOPAccount
		 , @lifeinsurance as LifeInsurance
		 , @otherassetbalance as OtherAssetBalance
		 , @anyotherassetbalance as AnyOtherAssetBalance
		 , @otherrealestate as OtherRealEstate
		 , @cars as Cars
		 , @other1 as Other1
		 , @other2 as Other2
		 , @other3 as Other3
		 , @other4 as Other4

end