USE [DebtPlus]
GO
/****** Object:  StoredProcedure [dbo].[rpt_NonAR_ByLedger]    Script Date: 09/15/2014 13:13:33 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE PROCEDURE [dbo].[rpt_NonAR_ByLedger] ( @FromDate AS DateTime = NULL, @ToDate AS DateTime = NULL ) AS
-- ============================================================================================
-- ==             Report information to list non-AR by creditor                              ==
-- ============================================================================================

-- ChangeLog
--   4/3/2003
--     Subtract the debit from the credit column to show the debits as negative.

-- Suppress intermediate results
set nocount on

IF @ToDate IS NULL
	SELECT @ToDate = getdate()

IF @FromDate IS NULL
	SELECT @FromDate = @ToDate

-- Adjust the dates for the proper settings
SELECT	@FromDate	= convert (datetime, convert(varchar(10), @FromDate, 101) + ' 00:00:00'),
	@ToDate		= convert (datetime, convert(varchar(10), @ToDate, 101)   + ' 23:59:59')

-- Retrieve the contributions
SELECT		isnull(d.dst_ledger_account,d.src_ledger_account)	as 'ledger_account',
		info.description					as 'description',
		d.date_created						as 'date',

		case
			when d.creditor is not null then type.description
			when d.client is not null then 'Client Transfers'
			else 'Non AR Source'
		end							as 'creditor_class',

		case
			when d.creditor is not null then d.creditor
			when d.client is not null then dbo.format_client_id ( d.client )
			else null
		end							as 'creditor',

		case
			when d.creditor is not null then cr.creditor_name
			when d.client is not null then dbo.format_normal_name(pn.prefix,pn.first,pn.middle,pn.last,pn.suffix)
			else null
		end							as 'name',

		isnull(d.credit_amt,0) - isnull(d.debit_amt,0)		as 'amount',
		coalesce(d.message,d.reference, '')			as 'reference'

FROM		registers_non_ar d
LEFT OUTER JOIN	ledger_codes info ON coalesce(d.dst_ledger_account,d.src_ledger_account) = info.ledger_code
LEFT OUTER JOIN	people p ON d.client = p.client AND 1 = p.relation
left outer join names pn with (nolock) on p.nameid = pn.name
LEFT OUTER JOIN	creditors cr ON d.creditor = cr.creditor
LEFT OUTER JOIN creditor_types type ON substring(d.creditor,1,1) = type.type
WHERE		d.date_created BETWEEN @FromDate AND @ToDate

ORDER BY	3, 1	-- date_created, ledger

RETURN ( @@rowcount )
GO
