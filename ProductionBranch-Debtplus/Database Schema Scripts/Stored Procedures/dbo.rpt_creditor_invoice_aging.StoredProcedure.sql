USE [DebtPlus]
GO
/****** Object:  StoredProcedure [dbo].[rpt_creditor_invoice_aging]    Script Date: 09/15/2014 13:13:33 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[rpt_creditor_invoice_aging] (@creditor as typ_creditor) AS
-- ==========================================================================================
-- ==     Fetch the amount billed from the system as aged                                  ==
-- ==========================================================================================

-- Configure to suppress intermediate results
SET NOCOUNT ON
SET ANSI_WARNINGS OFF

-- Information for the current aging of the invoices
DECLARE @amt_Current	MONEY
DECLARE @amt_30			MONEY
DECLARE @amt_60			MONEY
DECLARE @amt_90			MONEY
DECLARE @amt_120		MONEY
DECLARE @inv_date		DateTime
DECLARE @amount			MONEY
DECLARE	@aging_months	int

-- Find the current date without the time so that the information can be computed properly
declare @todays_date	datetime
select	@todays_date = convert(varchar(10), getdate(), 101)

-- Obtain a cursor to select the outstanding invoice data
DECLARE invoice_cursor CURSOR FORWARD_ONLY FOR
	SELECT  inv_date,
			isnull(inv_amount,0) - isnull(adj_amount,0) - isnull(pmt_amount,0) as 'amount',
			aging_months
	FROM	registers_invoices with (nolock)
	WHERE	creditor = @creditor
	AND	(isnull(pmt_amount,0)+isnull(adj_amount,0)) < isnull(inv_amount,0)

OPEN	invoice_cursor
FETCH	invoice_cursor INTO @inv_date, @amount, @aging_months

-- Initialize the local storage
SELECT	@amt_Current	= 0,
		@amt_30			= 0,
		@amt_60			= 0,
		@amt_90			= 0,
		@amt_120		= 0

-- Process all outstanding invoices for the creditor
WHILE @@fetch_status = 0
BEGIN
	if @aging_months < 1
	    set @amt_current = @amt_current + @amount
	else
	    if @aging_months < 2
			set @amt_30  = @amt_30  + @amount
	    else
			if @aging_months < 3
			    set @amt_60  = @amt_60  + @amount
			else
				if @aging_months < 4
					set @amt_90  = @amt_90  + @amount
				else
					set @amt_120 = @amt_120 + @amount
	
	FETCH	invoice_cursor INTO @inv_date, @amount, @aging_months
END

CLOSE invoice_cursor
DEALLOCATE invoice_cursor

-- Return the aging information
SELECT	@creditor							as 'creditor',
		@todays_date						as 'billing_date',
		@amt_current + @amt_30 + @amt_60 + @amt_90 + @amt_120	as 'total_due',
		@amt_current						as 'current',
		@amt_30								as 'net_30',
		@amt_60								as 'net_60',
		@amt_90								as 'net_90',
		@amt_120							as 'net_120'

RETURN ( 1 )
GO
