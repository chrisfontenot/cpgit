USE [DebtPlus]
GO
/****** Object:  StoredProcedure [dbo].[xpr_letter_system_note]    Script Date: 09/15/2014 13:13:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE PROCEDURE [dbo].[xpr_letter_system_note] (@letter_type as int, @client as int = -1, @creditor as varchar(10) = null, @debt as int = -1) as
-- ==========================================================================================================
-- ==            Insert a note that we generate the letter for the client or creditor                      ==
-- ==========================================================================================================

-- Determine the note text
declare	@note_text	varchar(800)

select	@note_text	= 'Printed letter ''' + replace(description,'''', '''''') + ''''
from	letter_types with (nolock)
where	letter_type	= @letter_type

-- If there is a debt then find the client associated with the debt
if @debt > 0
	select	@client		= client
	from	client_creditor with (nolock)
	where	client_creditor	= @debt

if @client <> -1

	-- insert the client or debt note.
	insert into client_notes (client, client_creditor, type, is_text, subject, note, dont_edit, dont_print, dont_delete)
	select		@client				as client,
				case
					when	@debt > 0 then @debt
					else	null
				end				as client_creditor,
				3				as type,
				1				as is_text,
				@note_text			as subject,
				@note_text + ' for client'	as note,
				1				as dont_edit,
				0				as dont_print,
				1				as dont_delete

else if @creditor is not null

	-- insert the creditor note
	insert into creditor_notes (creditor, type, is_text, subject, note, dont_edit, dont_print, dont_delete)
	select		@creditor			as creditor,
				3				as type,
				1				as is_text,
				@note_text			as subject,
				@note_text + ' for creditor'	as note,
				1				as dont_edit,
				0				as dont_print,
				1				as dont_delete

return ( @@rowcount )
GO
