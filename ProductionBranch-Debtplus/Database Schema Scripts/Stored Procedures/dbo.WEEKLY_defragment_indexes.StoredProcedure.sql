USE [DebtPlus]
GO
/****** Object:  StoredProcedure [dbo].[WEEKLY_defragment_indexes]    Script Date: 09/15/2014 13:13:34 ******/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE PROCEDURE [dbo].[WEEKLY_defragment_indexes] AS
-- ==========================================================================================
-- ==            Defragment indexes on a weekly basis                                      ==
-- ==========================================================================================

-- Cut down on the "noise".
SET NOCOUNT ON

declare	@tablename	varchar(128)
declare	@execstr	varchar(255)
declare	@objectid	int
declare	@indexid	int
declare	@frag		decimal
declare	@maxfrag	decimal
DECLARE	@StatusMsg	varchar(800)

-- Decide on the maximum fragmentation to allow
select	@maxfrag	= 10.0	-- per BOL recomendation

-- Declare cursor
declare	tables cursor FOR
	SELECT	table_name
	FROM	information_schema.tables
	where	table_type = 'BASE TABLE'

-- Create the working table
create table #fraglist (
	ObjectName	char(255),
	ObjectId	int,
	IndexName	char(255),
	IndexId		int,
	Lvl		int,
	CountPages	int,
	CountRows	int,
	MinRecSize	int,
	MaxRecSize	int,
	AvgRecSize	int,
	ForwaredRecords	int,
	Extents		int,
	ExtentSwitches	int,
	AvgFreeBytes	int,
	AvgPageDensity	int,
	ScanDensity	decimal,
	BestCount	int,
	ActualCount	int,
	LogicalFrag	decimal,
	ExtentFrag	decimal
);

-- Open the cursor
OPEN	tables

-- Loop through all the tables in the database
FETCH NEXT FROM tables INTO @tablename
WHILE @@FETCH_STATUS = 0
BEGIN
	-- Run the DBCC showconfig command to view the fragmentation
	INSERT INTO #fraglist -- (ObjectName, ObjectId, IndexName, IndexId, Lvl, CountPages, CountRows, MinRecSize, MaxRecSize, AvgRecSize, ForwaredRecords, Extents, ExtentSwitches, AvgFreeBytes, AvgPageDensity, ScanDensity, BestCount, ActualCount, LogicalFrag, ExtentFrag)
	EXEC ('DBCC SHOWCONTIG (''' + @tablename + ''') WITH FAST, TABLERESULTS, ALL_INDEXES, NO_INFOMSGS')

	FETCH NEXT FROM tables INTO @tablename
END

-- Close and deallocate the cursor
CLOSE		tables
DEALLOCATE	tables

-- Create a cursor for the index names
DECLARE indexes CURSOR FOR
	SELECT	ObjectName, ObjectId, IndexId, LogicalFrag
	FROM	#fraglist
	WHERE	LogicalFrag >= @MaxFrag
	AND	INDEXPROPERTY( ObjectId, IndexName, 'IndexDepth') > 0

-- Open the cursor
OPEN	indexes

-- Loop through the indexes
FETCH	NEXT
FROM	indexes
INTO	@TableName, @ObjectId, @IndexId, @Frag

WHILE	@@FETCH_STATUS = 0
BEGIN

	-- Generate the statement to rebuild the index
	SELECT	@StatusMsg = 'Executing DBCC INDEXDEFRAG (0, ' + RTRIM(@TableName) + ', ' + RTRIM(@IndexId) + ') - Fragmentation currently at ' + convert(varchar(15), @frag) + '%'
	EXEC	master..xp_logevent 60000, @StatusMsg, INFORMATIONAL

	-- Rebuild the index
	SELECT	@ExecStr = 'DBCC INDEXDEFRAG (0, ' + RTRIM(@ObjectId) + ', ' + RTRIM(@IndexId) + ') WITH NO_INFOMSGS'
	EXEC	( @ExecStr )

	FETCH	NEXT
	FROM	indexes
	INTO	@TableName, @ObjectId, @IndexId, @Frag
END

-- Close and deallocate the cursor
close		indexes
deallocate	indexes

-- Discard working table
drop table #fraglist
GO
