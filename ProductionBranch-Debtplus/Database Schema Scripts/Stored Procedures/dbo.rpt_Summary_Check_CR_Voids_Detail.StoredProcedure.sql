SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER procedure [rpt_Summary_Check_CR_Voids_Detail] ( @FromDate AS DateTime = NULL, @ToDate AS DateTime = NULL ) AS
-- ======================================================================================================
-- ==                   Fetch the information for voided creditor checks                               ==
-- ======================================================================================================

set nocount on

IF @ToDate IS NULL
	SELECT @ToDate = getdate()

IF @FromDate IS NULL
	SELECT @FromDate = @ToDate

-- Adjust the dates for the proper settings
SELECT	@FromDate	= convert(datetime, convert(varchar(10), @FromDate, 101) + ' 00:00:00'),
	@ToDate		= convert(datetime, convert(varchar(10), @ToDate,   101) + ' 23:59:59')

-- Find the checks that have been voided in this period
select	distinct
	tr.trust_register			as trust_register,
	tr.creditor				as creditor,
	tr.checknum				as checknum,
	tr.bank					as bank,
	tr.date_created				as item_date,
	case isnull(tr.cleared,' ')
		when ' ' then 'Pending'
		when 'V' then 'Void'
		when 'R' then 'Reconciled'
		when 'P' then 'Written'
		else 'Unknown'
	end					as status

from	registers_trust tr with (nolock)
inner join registers_creditor rc on rc.trust_register = tr.trust_register
where	rc.tran_type = 'VD'
and	rc.date_created between @FromDate and @ToDate
order by tr.bank, tr.checknum;

return ( @@rowcount )
GO
