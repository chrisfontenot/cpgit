USE [DebtPlus]
GO
/****** Object:  StoredProcedure [dbo].[rpt_BudgetSummary]    Script Date: 09/15/2014 13:13:32 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE PROCEDURE [dbo].[rpt_BudgetSummary] ( @Client AS INT ) AS
-- ==========================================================================================
-- ==             Fetch the budget ID for the current client                               ==
-- ==========================================================================================

-- Prevent the temporary recordsets from being generated with the item count
SET NOCOUNT ON

DECLARE @BudgetID    INT
select	@BudgetID= dbo.map_client_to_budget ( @client );

IF @BudgetID IS NULL
	SELECT @BudgetID = 0

-- Summarize details by the groups
select	g.[description],
		SUM(d.[suggested_amount]) as amount
from	budget_detail d
inner join budgets b on d.budget = b.budget
left outer join budget_categories c on d.budget_category = c.budget_category
left outer join budget_categories_other o on d.budget_category = o.budget_category
left outer join budget_category_groups g on ISNULL(o.budget_category_group, c.budget_category_group) = g.budget_category_group
where	d.budget = @BudgetID
group by g.description
order by 1;

-- The result of the procedure is the grand total amount
RETURN ( @@rowcount )
GO
