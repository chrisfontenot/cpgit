USE [DebtPlus]
GO
/****** Object:  StoredProcedure [dbo].[xpr_insert_workshop_type]    Script Date: 09/15/2014 13:13:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[xpr_insert_workshop_type] ( @description varchar(50), @duration int = 30, @ActiveFlag as bit = 1, @HousingFeeAmount as money = 0, @HUD_Grant as int = null ) as
insert into workshop_types ([description], [duration], [ActiveFlag], [HousingFeeAmount], [HUD_Grant]) values (@description, @duration, ISNULL(@ActiveFlag,1), @HousingFeeAmount, @Hud_Grant)
return scope_identity()
GO
