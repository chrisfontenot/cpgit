USE [DebtPlus]
GO
/****** Object:  StoredProcedure [dbo].[xpr_ACH_response_PAYMENT]    Script Date: 09/15/2014 13:13:34 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[xpr_ACH_response_PAYMENT] ( @deposit_batch_detail as int, @reason as varchar(4), @ach_response_batch_id as varchar(50) = null ) AS
-- ==========================================================================================
-- ==           Process the sequence needed to void an ACH transaction                     ==
-- ==========================================================================================

-- ChangeLog
--   4/23/2003
--     Reset the "ok_to_post" flag on errors
--   3/24/2011
--     Change parameters for .NET to use deposit_batch_detail
--   5/13/2011
--     Added call to function 'DepositsInTrust' to recalculate the pending trust amount

-- Allocate a transaction for the operation
BEGIN TRANSACTION

-- Find the client associated with this transaction
declare	@client			int
declare	@amount			money
declare	@trace_number	varchar(15)
declare	@posted			int

select	@client			= dp.client,
		@amount			= dp.amount,
		@trace_number	= dp.reference,
		@posted			= bi.trust_register
from	deposit_batch_details dp
inner join deposit_batch_ids bi on dp.deposit_batch_id = bi.deposit_batch_id
WHERE	dp.deposit_batch_detail = @deposit_batch_detail

-- If there is an error then mark the transaction invalid
if @client is not null
begin
	if @reason is not null
		UPDATE	deposit_batch_details
		SET		ach_authentication_code	= @reason,
				ach_response_batch_id	= @ach_response_batch_id,
				ok_to_post				= 0
		WHERE	deposit_batch_detail	= @deposit_batch_detail
	else
		UPDATE	deposit_batch_details
		SET		ach_authentication_code	= null,
				ach_response_batch_id	= null,
				ok_to_post				= 1
		WHERE	deposit_batch_detail	= @deposit_batch_detail

	-- We must be able to find the one transaction in the ACH table
	IF @@rowcount != 1
	BEGIN
		Rollback Transaction
		RaisError(50093, 16, 1, @trace_number)
		Return ( 0 )
	END
end

-- Commit the transaction
COMMIT TRANSACTION

-- Return success
RETURN ( 1 )
GO
