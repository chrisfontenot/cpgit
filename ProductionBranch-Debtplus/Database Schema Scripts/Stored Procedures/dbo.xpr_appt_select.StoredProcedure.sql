USE [DebtPlus]
GO
/****** Object:  StoredProcedure [dbo].[xpr_appt_select]    Script Date: 09/15/2014 13:13:34 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[xpr_appt_select] (@client as int = null, @appt_type as int = null, @date_part as VARCHAR(50) = NULL, @am_pm AS VARCHAR(50) = NULL, @zipcode as VarChar(50) = null, @counselor as int = null, @FromDate as DateTime = NULL, @ToDate as DateTime = NULL) AS
-- ================================================================================================
-- ==                Find a list of the available appointments for this client                   ==
-- ================================================================================================

-- ChangeLog
--   1/25/2002
--      Added support for workshop appointments in the client_appointments table.
--   9//6/2002
--      Ensure that the counselor exists in the counselors table to be considered "available".
--      Count only "Pending" appointments as "booked".
--   11/7/2002
--      Removed invalid condition for client appointment in that it only counted appointments where the language matched.
--   12/18/2002
--     Added support for "inactive" flag in appt_counselors
--   6/24/2005
--     Used description for appointment type rather than id. Was mistakenly left in as an error.
--   11/15/2005
--     Added bankruptcy qualification of counselors
--   12/6/2005
--     Added window to the appointments to allow finding Pacific Time appointments with Eastern Time counselors
--   6/22/2007
--     Rewrote some logic to clean up the counselor selections.
--   8/12/2010
--     Rewrite to support new AttributeTypes tables

-- Disable intermediate result sets
SET NOCOUNT ON

-- Determine if the counselor is to be "forced" when it is not specified. It should be the same as xpr_appt_book.
declare	@use_counselor	int
select	@use_counselor = 1

-- Determine the cutoff period for the appointments
declare	@appt_cutoff_date	datetime
select	@appt_cutoff_date	= getdate()

-- If you wish to adjust the time back then do it here. Some agencies want it back 3 hours to account for Eastern time to Pacific Time
-- To do so, enable the following line and possibly change the "-3" to the number of hours that you wish to use.
select @appt_cutoff_date	= dateadd(hour, -3, @appt_cutoff_date)

-- If there is no starting date then use sunday for this week
IF @FromDate IS NULL
	SELECT @FromDate = getdate()

-- Make the week start on a "Sunday".
SELECT @FromDate = dateadd ( d, 1 - datepart( w, @FromDate ), @FromDate )

IF @ToDate IS NULL
	SELECT @ToDate = @FromDate

-- Make the week end on a "Saturday".
SELECT @ToDate = dateadd ( d, 7 - datepart ( w, @ToDate ), @ToDate )

-- The starting date is the beginning of the week
IF  @FromDate > @ToDate
	SELECT @ToDate = @FromDate

-- Finally, limit the starting date to today
IF @FromDate <= getdate()
	SELECT @FromDate = getdate()

-- Remove the time periods from the date ranges
SELECT	@FromDate = convert(datetime, convert(varchar(10), @FromDate, 101) + ' 00:00:00'),
		@ToDate   = convert(datetime, convert(varchar(10), @ToDate,   101) + ' 23:59:59')

-- Find a list of the possible counselors in the system that can handle this appointment type if needed
create table #requirements ( AttributeType int )
if @appt_type is not null
	insert into #requirements ( AttributeType )
	select	Attribute
	from	AppointmentTypeAttributes WITH (NOLOCK)
	where	AppointmentType = @appt_type;
	
-- Add the counselor attribute to the list
insert into #requirements ( AttributeType )
select	oID
from	AttributeTypes
where	[GROUPING] = 'ROLE'
and		[ATTRIBUTE] = 'COUNSELOR';
	
-- Add the language from the client
if @client is not null
	insert into #requirements ( AttributeType )
	select		t.oID
	from		AttributeTypes t
	inner join	clients c on t.oID = c.language
	where		t.[Grouping] = 'LANGUAGE'
	and			c.client = @client;

-- Build a list of the counselors who match the attributes required
create table #counselors ( counselor int, Attribute int, ActiveFlag int )
insert into #counselors (counselor, Attribute, ActiveFlag)
select	co.counselor, x.AttributeType, isnull(y.oID,0)
from	counselors co with (nolock)
cross join #requirements x
LEFT OUTER JOIN counselor_attributes y on co.counselor = y.counselor and x.AttributeType = y.attribute

-- Delete any counselor who has a zero in any field.
delete	#counselors
from	#counselors
where	counselor in (
		select	counselor
		from	#counselors
		where	ActiveFlag = 0
);

-- What is left are the counselors who are a total match to the requirements.
-- Reduce the list to a single item
select distinct counselor into #distinct_counselors from #counselors;
drop table #counselors
drop table #requirements

-- Find a first cut at the data of available appointments
create table #available_times (appt_time int null, appt_type_id int null, office_id int null, start_time datetime null, office varchar(50) null, appt_type varchar(50) null, counselor int null, is_booked int null, zip_match int null, language_match int null, attribute_match int null);

insert into #available_times (appt_time, appt_type_id, office_id, start_time, office, appt_type, counselor, is_booked, zip_match, language_match, attribute_match)
select  a.appt_time, a.appt_type as appt_type_id, o.office, a.start_time, o.name as 'office', apt.appt_name as appt_type, ac.counselor, CONVERT(int,0) as is_booked, convert(int,0) as zip_match, convert(int,0) as language_match, convert(int,1) as attribute_match
from	appt_times a
left outer join appt_types apt on a.appt_type = apt.appt_type -- yes, use the type from the appt_times table!!
inner join appt_counselors ac on a.appt_time = ac.appt_time
inner join #distinct_counselors co on ac.counselor = co.counselor
inner join offices o on a.office = o.office and 1 = o.ActiveFlag
where a.start_time >= @FromDate
and   a.start_time <  @ToDate
and	  ac.inactive = 0
and   a.start_time >  @appt_cutoff_date

-- Discard the counselor list
drop table #distinct_counselors

-- If we are looking for a specific counselor, discard all others
if @counselor is not null
	delete
	from	#available_times
	where	counselor <> @counselor;
	
-- If we are looking for a specific type then discard all others
if @appt_type is not null
	delete
	from	#available_times
	where	appt_type_id <> @appt_type
	and		appt_type_id is not null;

-- Toss the items that are not for the specified days of the week
if @date_part is not null
begin
	declare	@date_part_stmt	varchar(80)
	select	@date_part_stmt = 'delete from #available_times where datepart(dw,start_time) not in (' + @date_part + ')'
	exec	(@date_part_stmt)
end

-- Toss the items that are outside of the time range desired
IF @am_pm IS NOT NULL
BEGIN
	if @am_pm = 'AM'
		delete
		from	#available_times
		where	DATEPART(hour, start_time) >= 12
	else
		delete
		from	#available_times
		where	DATEPART(hour, start_time) < 12
END
	
-- Toss the offices based upon the zipcodes	
IF @zipcode IS NOT NULL
BEGIN
	select @zipcode = replace(@zipcode,'''','')

	if len(@zipcode) < 5
		select @zipcode = right('00000' + @zipcode, 5)

	update	#available_times
	set		zip_match	= 1
	from	#available_times a
	inner join zipcodes z on a.office_id = z.office
	where	left(z.zip_lower,5) = left(@zipcode,5)
	
	delete
	from	#available_times
	where	zip_match	= 0
END

-- Mark the appointments that are booked
update	#available_times
set		is_booked	= 1
from	#available_times a
inner join client_appointments ca on a.appt_time = ca.appt_time
where	a.counselor = ca.counselor
and		ca.status = 'P'
and		ca.counselor is not null;

-- Return the results for the operation
select	 appt_time, start_time, office, appt_type, sum(is_booked) as booked, count(*) as available_slots
from	 #available_times
group    by appt_time, start_time, office, appt_type
having   sum(is_booked) < count(*);

drop table #available_times

-- Return the row count
RETURN ( @@rowcount )
GO
