USE [DebtPlus]
GO
/****** Object:  StoredProcedure [dbo].[xpr_rpps_response_Manual]    Script Date: 09/15/2014 13:13:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[xpr_rpps_response_Manual] ( @response_file as int, @rpps_transaction as int, @return_code as varchar(4), @disbursement_message as int = 0) as
-- ============================================================================================
-- ==            Process the manual reject of payments                                       ==
-- ============================================================================================

-- This routine does not use the CIE responses because doing so would lookup the item again in the
-- transaction table against the trace number. The purpose of the trace number is to find the transaction.
-- Since we are passed the transaction, we do not need the trace number. Also, if the automatic (file)
-- processing does not work then we need a "manual" method of doing refunds against the table and this is
-- the preferred approach.

-- Find the information from the transaction table
set nocount on
begin transaction

declare	@trace_number			varchar(15)
declare	@biller_id			varchar(15)
declare	@name				varchar(5)
declare	@account_number			varchar(22)
declare	@net				money
declare	@gross				money
declare	@service_class_or_purpose	varchar(4)
declare	@transaction_code		varchar(4)
declare	@original_reject_code		varchar(4)

-- Find the information from the rpps transaction table
select	@service_class_or_purpose	= t.service_class_or_purpose,
	@transaction_code		= t.transaction_code,
	@trace_number			= t.trace_number_first,
	@biller_id			= t.biller_id,
	@name				= left(isnull(pn.last,'')+'     ', 5),
	@account_number			= rcc.account_number,
	@original_reject_code		= t.return_code,
	@gross				= rcc.debit_amt,
	@net				= case isnull(rcc.creditor_type,'N')
						when 'D' then rcc.debit_amt - rcc.fairshare_amt
						else rcc.debit_amt
					  end

from	rpps_transactions t
left outer join	registers_client_creditor rcc	on t.client_creditor_register = rcc.client_creditor_register
left outer join people p			on rcc.client = p.client and 1 = p.relation
left outer join names pn with (nolock) on p.NameID = pn.Name
where	t.rpps_transaction		= @rpps_transaction

-- Ensure that there is an item
if @trace_number is null
begin
	rollback transaction
	RaisError ('The RPPS transaction (%d) is not valid. It could not be located.', 16, 1, @rpps_transaction)
	return ( 0 )
end

-- The transaction must be a payment. Do not accept prenotes. Do not accept reversals. Accept only payments here.
-- All other types do not need to be processed here. Proposals are simply marked as accepted or rejected. Balance
-- verifications are simply updated. Prenotes do not matter. Only money withdrawn from clients needs to be refunded.
if @service_class_or_purpose <> 'CIE' or @transaction_code <> '22'
begin
	rollback transaction
	RaisError ('The manual processing can only reject payments. This is transaction (%d) is not a payment.', 16, 1, @rpps_transaction)
	return ( 0 )
end

-- Determine if there is a pending transaction for this response
declare	@rpps_response_detail		int

select	@rpps_response_detail		= rpps_response_detail
from	rpps_response_details
where	rpps_transaction		= @rpps_transaction
and	rpps_response_file		= @response_file

-- If there is a response in the system and the transaction has been rejected previously then do not allow duplicates.
-- Ignore the duplicate response. We have processed it earlier and do not need to do it again for the manual processing.
if @rpps_response_detail is not null
begin
	if @original_reject_code is not null
	begin
		rollback transaction
		return ( 1 )
	end	
end

-- Insert the transaction for the response if it is not presently in the system
if @rpps_response_detail is null
begin
	insert into rpps_response_details (rpps_response_file, rpps_biller_id, service_class_or_purpose, return_code, trace_number, rpps_transaction, consumer_name, account_number, net, gross, processing_error)
	values (@response_file, @biller_id, 'CIE', @return_code, @trace_number, @rpps_transaction, @name, @account_number, @net, @gross, null)
	select	@rpps_response_detail	= SCOPE_IDENTITY()
end

update	rpps_response_details
set	return_code			= @return_code
where	rpps_response_detail		= @rpps_response_detail
		
-- Return the new record in the response detail table
commit transaction
return ( @rpps_response_detail )
GO
