USE [DebtPlus]
GO
/****** Object:  StoredProcedure [dbo].[cccs_admin_intake_budget_debts]    Script Date: 09/15/2014 13:13:31 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[cccs_admin_intake_budget_debts] ( @application_id as int, @client as int = NULL ) as

-- Suppress intermediate results
set nocount on

-- If there is no client then find it from the re-import tables
if @client is null
	select	@client = ForeignDatabaseID
	from	cccs_admin..applications
	where	application_id = @application_id

-- If there still is no client then do not continue	
if @client is null
begin
	RaisError('Client is NULL and can not be determined', 16, 1)
	return ( -1 )
end

declare	@creditor_id		int
declare	@priority		int
declare	@defined_creditor_id	varchar(80)
declare	@creditor_name		varchar(80)
declare	@account_number		varchar(80)
declare	@current_balance	money
declare	@monthly_payment	money
declare @adjusted_payment   money
declare	@num_months_past_due	int
declare	@interest_rate		float
declare @adjusted_interest_rate float
declare	@state			varchar(10)
declare	@creditor		varchar(10)
declare	@config_fee		int
declare	@fee_sched_payment	money
declare	@fee_type		int
declare @ForeignDatabaseID varchar(50)

-- Build a list of the debts that the client currently has which may be deleted
select	client_creditor, creditor
into	#debts
from	client_creditor
where	client	= @client;

-- Allocate an index to find things quicker
create unique index ix1_temp_debts on #debts ( client_creditor );

-- Find the PAF creditor
declare	@paf_creditor	varchar(10)
select	@paf_creditor = paf_creditor
from	config;

-- If there is a PAF creditor then do delete this creditor from the deletion list. We must keep it.
if @paf_creditor is not null
	delete
	from   #debts
	where  creditor = @paf_creditor;

-- Create the items as needed
declare intake_debt_cursor cursor for
	select	c.creditor_id											as creditor_id,
			c.defined_creditor_id									as creditor,
			c.creditor_name											as creditor_name,
			dbo.cccs_admin_account_number ( c.account_number )		as account_number,
			c.current_balance										as current_balance,
			c.monthly_payment										as monthly_payment,
			c.adjusted_payment										as adjusted_payment,
			c.num_months_past_due									as num_months_past_due,
			dbo.cccs_admin_rates ( c.interest_rate )				as interest_rate,
			dbo.cccs_admin_rates ( c.adjusted_interest_rate )		as adjusted_interest_rate,
			c.ForeignDatabaseID										as ForeignDatabaseID
	from	cccs_admin..budget_creditors AS c with (nolock)
	INNER JOIN cccs_admin..budgets b on c.budget_id = b.budget_id
	where	b.application_id	= @application_id
	and		c.defined_creditor_id IS NULL

	union

	select	c.creditor_id											as creditor_id,
			c.defined_creditor_id									as creditor,
			creditors.creditor_name									as creditor_name,
			dbo.cccs_admin_account_number ( c.account_number )		as account_number,
			c.current_balance										as current_balance,
			c.monthly_payment										as monthly_payment,
			c.adjusted_payment										as adjusted_payment,
			c.num_months_past_due									as num_months_past_due,
			dbo.cccs_admin_rates ( c.interest_rate )				as interest_rate,
			dbo.cccs_admin_rates ( c.adjusted_interest_rate )		as adjusted_interest_rate,
			c.ForeignDatabaseID										as ForeignDatabaseID
	from	cccs_admin..budget_creditors AS c with (nolock)
	INNER JOIN cccs_admin..budgets b on c.budget_id = b.budget_id
	LEFT OUTER JOIN cccs_admin..creditors AS creditors with (nolock) ON c.creditor_id = creditors.creditor_id
	where	b.application_id	= @application_id
	and		c.defined_creditor_id IS NOT NULL

open intake_debt_cursor
fetch intake_debt_cursor into @creditor_id, @defined_creditor_id, @creditor_name, @account_number, @current_balance, @monthly_payment, @adjusted_payment, @num_months_past_due, @interest_rate, @adjusted_interest_rate, @ForeignDatabaseID

while @@fetch_status = 0
begin
	declare	@client_creditor			int
	declare	@client_creditor_balance	int
	
	select	@client_creditor			= null,
			@client_creditor_balance	= null

	if @ForeignDatabaseID is not null
		select	@client_creditor_balance	= cc.client_creditor_balance,
				@client_creditor			= cc.client_creditor
		from	client_creditor cc
		where	cc.client_creditor			= CONVERT(int, @ForeignDatabaseID);

	-- If there is no existing record then create a new debt
	if @client_creditor is null
	begin

		-- Create the debt record
		execute @client_creditor = xpr_create_debt @client
		
		-- Find the balance record from the debt
		select  @client_creditor_balance = client_creditor_balance
		from	client_creditor
		where	client_creditor = @client_creditor;
		
		-- Update the foregin database with the new debt information
		update	cccs_admin..applicant_creditors
		set		ForeignDatabaseID = @client_creditor
		where	creditor_id		  = @creditor_id
		
		update  client_creditor_balances
		set		orig_balance      = @current_balance
		where	client_creditor_balance = @client_creditor_balance

		-- Correct the debt information
		update	client_creditor
		set		creditor_name       = left(ltrim(rtrim(@creditor_name)), 50),
				account_number      = left(ltrim(rtrim(replace(replace(@account_number, '-', ''), ' ', ''))), 22),
				non_dmp_payment     = ISNULL(@monthly_payment,0),
				non_dmp_interest    = @interest_rate,
				disbursement_factor = COALESCE(@adjusted_payment, @monthly_payment,0),
				sched_payment       = COALESCE(@adjusted_payment, @monthly_payment,0),
				months_delinquent	= ISNULL(@num_months_past_due,0),
				dmp_interest		= @adjusted_interest_rate,
				dmp_payout_interest = @adjusted_interest_rate
		where	client_creditor	    = @client_creditor;
	end
	
	-- Otherwise, the debt currently exists. Adjust the balance normally.
	else begin
	
		-- Correct the debt information
		update	client_creditor
		set		creditor_name       = left(ltrim(rtrim(@creditor_name)), 50),
				account_number      = left(ltrim(rtrim(replace(replace(@account_number, '-', ''), ' ', ''))), 22),
				non_dmp_payment     = ISNULL(@monthly_payment,0),
				non_dmp_interest    = @interest_rate,
				disbursement_factor = COALESCE(@adjusted_payment, @monthly_payment,0),
				sched_payment       = COALESCE(@adjusted_payment, @monthly_payment,0),
				months_delinquent	= ISNULL(@num_months_past_due,0),
				dmp_interest		= @adjusted_interest_rate,
				dmp_payout_interest = @adjusted_interest_rate
		where	client_creditor	    = @client_creditor;

		-- Correct the balance information
		execute xpr_debt_balance @client_creditor, @current_balance
		
		-- Remove the debt from the deletion list
		delete
		from	#debts
		where	client_creditor		= @client_creditor;
	end

	-- Go to the next record in the input database
	fetch intake_debt_cursor into @creditor_id, @defined_creditor_id, @creditor_name, @account_number, @current_balance, @monthly_payment, @adjusted_payment, @num_months_past_due, @interest_rate, @adjusted_interest_rate, @ForeignDatabaseID
end

-- After all debts are loaded, close out the cursor.
close		intake_debt_cursor
deallocate	intake_debt_cursor

-- Discard the debt records
delete	client_creditor_balances
from	client_creditor_balances b
inner join client_creditor cc on b.client_creditor_balance = cc.client_creditor_balance and b.client_creditor = cc.client_creditor
inner join #debts x on cc.client_creditor = x.client_creditor

-- Discard the proposal records
delete	client_creditor_proposals
from	client_creditor_proposals pr
inner join client_creditor cc on pr.client_creditor_proposal = cc.client_creditor_proposal
inner join #debts x on cc.client_creditor = x.client_creditor

-- Discard the payee records
delete	client_creditor
from	client_creditor cc
inner join #debts x on cc.client_creditor = x.client_creditor

drop table #debts

return ( 1 )
GO
