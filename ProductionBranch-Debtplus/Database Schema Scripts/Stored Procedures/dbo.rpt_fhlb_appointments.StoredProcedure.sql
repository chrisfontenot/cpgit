USE [DebtPlus]
GO
/****** Object:  StoredProcedure [dbo].[rpt_fhlb_appointments]    Script Date: 09/15/2014 13:13:33 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--INSERT INTO [dbo].[menus]([seq], [parentSeq],[attributes],[menu_level],[text],[type],[argument],[date_created],[created_by])
--values(10040095, 10040000, 0, 7, 'FHLB Appointments', 'DebtPlus.UI.Desktop.Reports.Print.Mainline', 'DebtPlus.Reports.Appointments.Fhlb.FhlbAppointmentsReport', getdate(), 'sa')

create procedure [dbo].[rpt_fhlb_appointments] (
	@fromdate as datetime = null, 
	@todate as datetime = null
)
as 
begin

SELECT ca.[partner] as FHLB#
	  , rb.description as referral
	  , case ci.indicator when 66 then 'Y' else 'N' end as share_auth
	  ,ca.[client] 
	  ,ca.[start_time]
	  --,ca.[appt_type] as appt_type_id
	  ,at.Description as appt_type
	  --,ca.[status]
	  , dbo.format_appointment_status ( ca.status ) as appt_status
      , n.First + ' ' + n.middle + ' ' + n.last as clientname
	  , a.city as clientcity
	  , s.Name as clientstate
	  , a.PostalCode as clientzip
	  , a.city + ', ' + s.Name + ', ' + a.PostalCode as city_state_zip
	  , e.address as clientemail
	  --,ca.[office]
	  ,o.name as officename
      --,ca.[counselor]      
	  , n1.first + ' ' + n1.middle + ' ' + n.last as counselorname
  FROM [dbo].[client_appointments] as ca
  join [dbo].[clients] as c on ca.client = c.client
  join [dbo].[referred_by] as rb on rb.referred_by = ca.referred_by
  join [dbo].[people] as p on c.client = p.client and p.relation = 1
  left outer join [dbo].[emailaddresses] as e on p.emailid = e.email
  left outer join [dbo].[client_indicators] as ci on ca.client = ci.client
  left outer join [dbo].[names] as n on p.nameid = n.name
  left outer join [dbo].[addresses] as a on c.addressid = a.address
  left outer join [dbo].[states] as s on a.state = s.state
  left outer join [dbo].[offices] as o on ca.office = o.office
  left outer join [dbo].[counselors] as co on ca.counselor = co.counselor
  left outer join [dbo].[names] as n1 on co.nameid = n1.name
  left outer join [dbo].[appointmenttypes] as at on ca.[appt_type] = at.oID
  Where rb.description like 'fhlb%' and ca.start_time between @fromdate and @todate

end
GO
