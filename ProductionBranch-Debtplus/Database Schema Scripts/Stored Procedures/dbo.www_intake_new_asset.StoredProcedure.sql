USE [DebtPlus]
GO
/****** Object:  StoredProcedure [dbo].[www_intake_new_asset]    Script Date: 09/15/2014 13:13:34 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE PROCEDURE [dbo].[www_intake_new_asset] (@intake_client as int, @intake_id as varchar(20), @asset_id as int, @asset_amount as money) as

-- ==============================================================================================================
-- ==            Create/Update the person associated with the ID                                               ==
-- ==============================================================================================================

-- Suppress extra result sets
set nocount on

-- Validate the ID against the client
if not exists (	select	*
		from	intake_clients
		where	intake_client	= @intake_client
		and	intake_id	= @intake_id )
begin
	RaisError ('The client ID is invalid.', 16, 1)
	return ( 0 )
end

-- Validate the person information
if not exists (	select	*
		from	asset_ids
		where	asset_id	= @asset_id )
begin
	RaisError ('The asset id %d is invalid.', 16, 1, @asset_id)
	return ( 0 )
end

-- Ensure that the row exists for the asset of this client
if not exists (
	select	*
	from	intake_assets with (nolock)
	where	intake_client	= @intake_client
	and	asset_id	= @asset_id
)
	insert into intake_assets ( intake_client,  asset_id,  asset_amount)
	values			  (@intake_client, @asset_id,  0)

-- Correct the value for the row
update	intake_assets
set	asset_amount	= @asset_amount
where	intake_client	= @intake_client
and	asset_id	= @asset_id

return ( @@rowcount )
GO
