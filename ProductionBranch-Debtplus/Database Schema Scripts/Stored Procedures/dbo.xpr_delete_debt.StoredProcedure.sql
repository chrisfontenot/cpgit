USE [DebtPlus]
GO
/****** Object:  StoredProcedure [dbo].[xpr_delete_debt]    Script Date: 09/15/2014 13:13:35 ******/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE PROCEDURE [dbo].[xpr_delete_debt] ( @client_creditor AS typ_key ) AS

-- ==============================================================================================================
-- ==            Delete the debt information                                                                   ==
-- ==============================================================================================================

-- ChangeLog
--   9/31/2002
--     Added support for client_creditor_balances table
--   3/14/2003
--     Corrected rpps_transaction table update to delete bad transactions since we can't change the account info to null.
--   06/09/2009
--      Remove references to debt_number and use client_creditor
--   04/30/2012
--      Moved the processing to the delete trigger on the client_creidtor table

-- Remove the debt from the system here.
delete
from	client_creditor
where	client_creditor		= @client_creditor

return ( @@rowcount )
GO
