USE [DebtPlus]
GO
/****** Object:  StoredProcedure [dbo].[rpt_Custom_appts_available_analysis]    Script Date: 09/15/2014 13:13:33 ******/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE PROCEDURE [dbo].[rpt_Custom_appts_available_analysis] ( @From_date as datetime = null, @To_date as datetime = null ) as

-- Make the dates valid
set nocount on

if @To_Date is null
	select	@To_Date	= getdate()

if @From_Date is null
	select	@From_Date	= @To_Date

select	@From_Date	= convert(datetime, convert(varchar(10), @From_Date, 101) + ' 00:00:00'),
	@To_Date	= convert(datetime, convert(varchar(10), @To_Date, 101) + ' 23:59:59')

create table #avail_appts ( appt_time int, appt_type int null, office int, start_time datetime, avail int)

-- Find the available slots for appointments. Look at the list of counselors and count those as a free slot
-- for the indicated time period.
insert into #avail_appts ( appt_time, appt_type, office, start_time, avail )
select	t.appt_time			as appt_time,
	t.appt_type			as appt_type,
	t.office			as office,
	t.start_time			as start_time,
	count (ao.appt_counselor)	as avail

from	appt_times t with (nolock)
inner join offices o with (nolock) on t.office = o.office
inner join appt_counselors ao with (nolock) on t.appt_time = ao.appt_time
where	t.start_time between @from_date and @to_date
and	ao.inactive = 0
group by t.appt_time, t.office, t.start_time, t.appt_type;

-- Return the results to the user
select	dbo.date_only ( t.start_time )						as start_date,
	dbo.time_only ( t.start_time )						as start_time,
	o.name									as office_name,
	coalesce(apt.appt_name, '#' + convert(varchar, t.appt_type), 'ANY')	as appt_name,
	convert(int,isnull(avail,0))						as appt_count

from	#avail_appts t
left outer join offices o with (nolock) on t.office = o.office
left outer join appt_types apt with (nolock) on t.appt_type = apt.appt_type
order by 3, 4, 1, 2

drop table #avail_appts

return ( @@rowcount )
GO
