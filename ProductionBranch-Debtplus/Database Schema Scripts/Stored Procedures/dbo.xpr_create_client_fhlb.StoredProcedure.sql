IF EXISTS(SELECT * FROM SYSOBJECTS WHERE TYPE='P' AND NAME=N'xpr_create_client_fhlb')
	EXEC('DROP procedure xpr_create_client_fhlb')
GO
CREATE procedure [dbo].[xpr_create_client_fhlb] ( @client as int, @account_number as varchar(22) = null, @creditor as varchar(10) = null, @amount as money = null ) as

-- turn off intermediate results
set nocount on

-- Default the parameters as needed
if isnull(@creditor,'') = ''
	SET	@creditor	= 'V71585'
	
if @amount is null
	SET @amount = 175.00

if isnull(@account_number,'') = ''
	SET	@account_number = dbo.format_client_id ( @client )

-- Ensure that the creditor exists
if not exists ( select * from creditors where creditor = @creditor )
	return 0

BEGIN TRANSACTION
BEGIN TRY

	-- Create the debt for the client
	declare	@client_creditor	int

	execute @client_creditor = xpr_create_debt @client = @client, @creditor = @creditor, @creditor_name = null, @account_number = @account_number
	if isnull(@client_creditor,0) < 1
		set @client_creditor = 0
	else
	begin
		-- Update the balance for the debt
		update	client_creditor_balances
		set		orig_balance			= @amount,
				current_sched_payment	= @amount
		from	client_creditor cc
		inner join client_creditor_balances b on cc.client_creditor_balance = b.client_creditor_balance
		where	cc.client_creditor	= @client_creditor

		-- Set the disbursement factor and sched_payment accordingly
		update	client_creditor
		set		disbursement_factor		= @amount,
				sched_payment			= @amount
		where	client_creditor			= @client_creditor
	end

	COMMIT TRANSACTION

	-- return the pointer to the new debt
	return @client_creditor
END TRY

BEGIN CATCH
	DECLARE	@ErrorMessage	NVARCHAR(4000);
	DECLARE	@ErrorSeverity	INT;
	DECLARE	@ErrorState		INT;

	SELECT	@ErrorMessage	= ERROR_MESSAGE(),
			@ErrorSeverity	= ERROR_SEVERITY(),
			@ErrorState		= ERROR_STATE();

	ROLLBACK TRANSACTION

	-- Raise the error condition that was trapped
	RAISERROR (@ErrorMessage, @ErrorSeverity, @ErrorState);

	return ( 0 )
END CATCH
GO
GRANT EXECUTE ON xpr_create_client_fhlb TO public AS dbo;
GO
