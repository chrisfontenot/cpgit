USE [DebtPlus]
GO
/****** Object:  StoredProcedure [dbo].[xpr_disbursement_manual_create]    Script Date: 09/15/2014 13:13:35 ******/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE PROCEDURE [dbo].[xpr_disbursement_manual_create] ( @type as varchar(2)) AS

-- Generate the disbursement batch
INSERT INTO registers_disbursement	(tran_type,	created_by,	date_created,	date_posted,	note)
VALUES					(@type,		suser_sname(),	getdate(),	null,		'Manual Disbursement')

-- Remember the disbursement register
return ( SCOPE_IDENTITY() )
GO
