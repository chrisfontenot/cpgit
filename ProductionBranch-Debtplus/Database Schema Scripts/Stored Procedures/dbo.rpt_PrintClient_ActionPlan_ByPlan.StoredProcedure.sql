USE [DebtPlus]
GO
/****** Object:  StoredProcedure [dbo].[rpt_PrintClient_ActionPlan_ByPlan]    Script Date: 09/15/2014 13:13:33 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[rpt_PrintClient_ActionPlan_ByPlan] ( @ActionPlan AS INT ) AS
-- ====================================================================================
-- ==              Find the action plan information for this specific item           ==
-- ====================================================================================

-- ChangeLog
--   --/--/----
--      Only generate the items that are checked for the list. Do not print the "un-checked" items.

-- Do not generate intermediate result sets
SET NOCOUNT		ON

-- Fetch the action plan from the list of action plans for this client
DECLARE @ClientID	INT
SELECT	@ClientID = client
FROM	action_plans
WHERE	client = @ClientID
ORDER BY date_created DESC

-- Restore the counter
SET NOCOUNT OFF

-- Fetch the information for the client
SELECT
	items.item_group,
	items.description,
	convert(int, isnull(clients.checked, 0)) as 'checked'
from	action_items items
FULL OUTER JOIN client_action_items clients on items.action_item = clients.action_item AND clients.action_plan = @ActionPlan
WHERE	item_group IS NOT NULL
AND	clients.checked <> 0

UNION ALL

SELECT
	items.item_group,
	items.description,
	convert(int, isnull(clients.checked,0)) as 'checked'
FROM	action_items_other items
INNER JOIN client_action_items clients on items.action_item_other = clients.action_item AND clients.action_plan = @ActionPlan
WHERE	item_group IS NOT NULL
AND	items.action_plan = @ActionPlan
AND	clients.checked <> 0

ORDER BY 1, 2

RETURN ( @@rowcount )
GO
