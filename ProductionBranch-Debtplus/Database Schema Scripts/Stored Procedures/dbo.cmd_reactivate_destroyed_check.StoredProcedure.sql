SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER procedure [cmd_reactivate_destroyed_check] ( @destroyed_check as bigint, @good_check as bigint ) as
-- ===========================================================================================================================
-- ==            The agency sent out a destroyed check. Make it a "real" check.                                             ==
-- ===========================================================================================================================

-- ChangeLog
--   06/09/2009
--      Remove references to debt_number and use client_creditor

-- Find the trust register pointers for the items
declare	@disbursement_register		int

declare	@destroyed_trust_register	int
declare	@destroyed_tran_type		varchar(10)
declare	@destroyed_creditor		varchar(10)
declare	@destroyed_amount		money
declare	@destroyed_date			datetime

declare	@good_date			datetime
declare	@good_trust_register		int
declare	@good_creditor			varchar(10)
declare	@good_amount			money
declare	@good_tran_type			varchar(10)

declare	@error_msg			varchar(80)
declare	@created_by			varchar(80)

declare	@deducted_fairshare		money
declare	@billed_fairshare		money
declare	@deduct_creditor		varchar(10)
declare	@total_gross			money
declare	@message			varchar(80)

declare	@chks_per_invoice		int
declare	@invoice_register		int
declare	@total_billed			money

-- Find the information about the destroyed check
select	@destroyed_trust_register	= trust_register,
	@destroyed_creditor		= creditor,
	@destroyed_tran_type		= tran_type,
	@destroyed_creditor		= creditor,
	@destroyed_date			= date_created,
	@destroyed_amount		= amount
from	registers_trust
where	cleared				= 'D'
and	checknum			= @destroyed_check

-- Find the information about the destroyed check
select	@good_trust_register		= trust_register,
	@good_creditor			= creditor,
	@good_tran_type			= tran_type,
	@good_creditor			= creditor,
	@good_date			= date_created,
	@good_amount			= amount,
	@created_by			= created_by
from	registers_trust
where	cleared				= ' '
and	checknum			= @good_check

select @message = 'Issued destroyed check'

-- Complain if the check numbers are not defined in the system
if @destroyed_trust_register is null
begin
	select @error_msg = 'The check number ''' + @destroyed_check + ''' is not in the trust register (or is not destroyed)'
	RaisError (@error_msg, 16, 1)
	return ( 0 )
end

if @good_trust_register is null
begin
	select @error_msg = 'The check number ''' + @good_check + ''' is not in the trust register (or is not outstanding)'
	RaisError (@error_msg, 16, 1)
	return ( 0 )
end

-- Validate the transaction type
if @destroyed_tran_type not in ('AD','MD','CM')
begin
	select @error_msg = 'The check number ''' + @destroyed_check + ''' is not a valid transaction type'
	RaisError (@error_msg, 16, 1)
	return ( 0 )
end

if @destroyed_tran_type <> @good_tran_type
begin
	select @error_msg = 'The check numbers ''' + @destroyed_check + ''' and ''' + @good_check + ''' are not the same transaction type'
	RaisError (@error_msg, 16, 1)
	return ( 0 )
end

-- Validate the amount
if isnull(@destroyed_amount,0) <= 0
begin
	select @error_msg = 'The check number ''' + convert(varchar,@destroyed_check) + ' has a zero or negative dollar amount'
	RaisError (@error_msg, 16, 1)
	return ( 0 )
end

if @destroyed_amount <> @good_amount
begin
	select @error_msg = 'The check numbers ''' + convert(varchar,@destroyed_check) + ''' and ''' + convert(varchar,@good_check) + ''' are not the same amount'
	RaisError (@error_msg, 16, 1)
	return ( 0 )
end

-- Validate the creditor
if @destroyed_creditor <> @good_creditor
begin
	select @error_msg = 'The check numbers ''' + convert(varchar,@destroyed_check) + ''' and ''' + convert(varchar,@good_check) + ''' are not the same creditor'
	RaisError (@error_msg, 16, 1)
	return ( 0 )
end

-- Ensure that the same item is not being used twice
if @destroyed_trust_register = @good_trust_register
begin
	select @error_msg = 'The check numbers ''' + convert(varchar,@destroyed_check) + ''' and ''' + convert(varchar,@good_check) + ''' are the same check'
	RaisError (@error_msg, 16, 1)
	return ( 0 )
end

-- Find the disbursement batch
select	@disbursement_register	= disbursement_register
from	registers_creditor
where	trust_register		= @good_trust_register
and	tran_type		= @good_tran_type

if @disbursement_register is null
begin
	select @error_msg = 'The disbursement batch ID could not be found for check number ''' + @destroyed_check + ''''
	RaisError (@error_msg, 16, 1)
	return ( 0 )
end

-- Handle the special case for a Z9999 check.
select	@deduct_creditor = deduct_creditor
from	config;

if @deduct_creditor is null
	select	@deduct_creditor = 'Z9999'
	
-- Start a transaction to keep things honest
begin transaction

-- Include the creditor register entry
insert into registers_creditor	( tran_type, creditor, debit_amt, disbursement_register, trust_register, date_created, item_date, message, created_by )
values				( @good_tran_type, @good_creditor, @good_amount, @disbursement_register, @destroyed_trust_register, @destroyed_date, @destroyed_date, @message, @created_by )

-- If this is the deduction check then we need to do something special here.
if @destroyed_creditor = @deduct_creditor
begin
	declare	@z9999_client_creditor	int
	select	@z9999_client_creditor	= client_creditor
	from	client_creditor
	where	client			= 0
	and		creditor		= @deduct_creditor;
	
	-- Generate the total gross amount for later processing
	select	@total_gross		= @good_amount,
		@billed_fairshare	= 0,
		@deducted_fairshare	= 0,
		@invoice_register	= null

	-- Take the money from the deduct account
	update	clients
	set	held_in_trust		= held_in_trust - @total_gross
	where	client			= 0;

	-- Insert the client transaction
	insert into registers_client ( tran_type, client, disbursement_register, trust_register, debit_amt, created_by, date_created, message, item_date )
	values ( @good_tran_type, 0, @disbursement_register, @destroyed_trust_register, @total_gross, @created_by, @destroyed_date, @message, @destroyed_date )

	if @z9999_client_creditor is not null
	begin
	-- Insert the creditor transaction
	insert into registers_client_creditor ( tran_type, client, creditor, client_creditor, disbursement_register, trust_register, creditor_type, fairshare_amt, debit_amt, fairshare_pct, created_by, date_created )
		values ( @good_tran_type, 0, @good_creditor, @z9999_client_creditor, @disbursement_register, @destroyed_trust_register, 'N', 0, @total_gross, 0.0, @created_by, @destroyed_date )

	-- Insert the debt transaction
	insert into registers_client_creditor ( tran_type, client, creditor, client_creditor, disbursement_register, trust_register, debit_amt, created_by, date_created )
		values ( @good_tran_type, 0, @deduct_creditor, @z9999_client_creditor, @disbursement_register, @destroyed_trust_register, @total_gross, @created_by, @destroyed_date )
	end

end else begin

	-- Find the contents of the check
	select	client, creditor, client_creditor, debit_amt, fairshare_amt, fairshare_pct, creditor_type, invoice_register, account_number
	into	#contents
	from	registers_client_creditor
	where	disbursement_register	= @disbursement_register
	and	tran_type		= @good_tran_type
	and	trust_register		= @good_trust_register
	and	creditor		= @good_creditor
	and	client			<> 0; -- Just a safety condition to avoid real problems later

	-- There should be some contents on the check
	if not exists (select * from #contents)
	begin
		rollback transaction
		RaisError ('There are no paid debts on this check', 16, 1)
		return ( 0 )
	end

	-- Find the total gross amount
	select	@total_gross		= sum(debit_amt)
	from	#contents;

	-- Find the deduct amount and billed amounts
	select	@deducted_fairshare	= sum(fairshare_amt)
	from	#contents
	where	creditor_type		= 'D';

	select	@billed_fairshare	= sum(fairshare_amt)
	from	#contents
	where	creditor_type not in ('D','N')
	and	invoice_register is not null;

	if @deducted_fairshare is null
		select	@deducted_fairshare = 0;

	if @billed_fairshare is null
		select	@billed_fairshare = 0;

	-- Generate an invoice if the creditor requires a new invoice
	if exists (select * from #contents where creditor_type = 'B' and invoice_register is not null)
	begin
		select	@invoice_register	= invoice_register
		from	#contents
		where	invoice_register is not null;

		select	@chks_per_invoice	= coalesce(creditors.chks_per_invoice, config.chks_per_invoice, 0)
		from	creditors, config
		where	creditors.creditor	= @good_creditor

		-- If there is nothing billed then ignore the invoice.
		if @billed_fairshare <= 0
			select	@chks_per_invoice = 0

		-- Generate a new invoice for the check if the creditor requires a new invoice for this check.
		if isnull(@chks_per_invoice,0) <> 0
		begin
			insert into registers_invoices (creditor, inv_amount, inv_date, created_by, date_created)
			values	( @good_creditor, 0, @good_date, @created_by, @good_date )
			select @invoice_register	= @@identity

			-- Update the transactions with the new invoice number
			update	#contents
			set	invoice_register	= @invoice_register
			where	creditor_type not in ('N','D')
			and	invoice_register is not null;
		end

		if @invoice_register is not null
		begin
			-- Count this as a billed amount for the creditor
			update	creditors
			set	contrib_mtd_billed	= contrib_mtd_billed + @billed_fairshare
			where	creditor		= @good_creditor;

			update	registers_invoices
			set	inv_amount		= inv_amount + @billed_fairshare
			where	invoice_register	= @invoice_register;
		end
	end

	-- Insert the transactions for the destroyed check
	insert into registers_client_creditor (tran_type, client, creditor, client_creditor, debit_amt, fairshare_amt, fairshare_pct, creditor_type, invoice_register, trust_register, disbursement_register, created_by, date_created, account_number)
	select	@good_tran_type, client, creditor, client_creditor, debit_amt, fairshare_amt, fairshare_pct, creditor_type, invoice_register, @destroyed_trust_register, @disbursement_register, @created_by, @destroyed_date, account_number
	from	#contents;

	-- Find the amount disbursed for each of the clients
	select	client, sum(debit_amt) as debit_amt
	into	#client
	from	#contents
	group by client;

	-- Update the amount field for the client operations
	update	registers_client
	set	debit_amt			= rc.debit_amt + x.debit_amt
	from	registers_client rc
	inner join #client x on rc.client = x.client
	where	rc.tran_type			= @good_tran_type
	and	rc.disbursement_register	= @disbursement_register;

	-- Take the money from the client's account
	update	clients
	set	held_in_trust			= held_in_trust - x.debit_amt
	from	clients c
	inner join #client x on c.client = x.client;

	-- Discard the working table
	drop table #client

	if isnull(@deducted_fairshare,0) > 0
	begin
		-- Insert the transaction for the credit operation
		insert into registers_client (tran_type, client, credit_amt, message, disbursement_register, created_by, item_date, date_created)
		values (@good_tran_type, 0, @deducted_fairshare, @message, @disbursement_register, @created_by, @destroyed_date, @destroyed_date)

		-- Credit the deduct account for the amount deducted
		update	clients
		set	held_in_trust		= held_in_trust + @deducted_fairshare
		where	client			= 0;

		-- Update the creditor statistics
		update	creditors
		set	contrib_mtd_received	= contrib_mtd_received + @deducted_fairshare
		where	creditor		= @good_creditor;
	end

	-- Calculate the amount paid by debt
	select	c.client_creditor, cc.client_creditor_balance, sum(debit_amt) as debit_amt
	into	#debt
	from	#contents c
	inner join client_creditor cc on c.client_creditor = cc.client_creditor
	group by c.client_creditor, cc.client_creditor_balance;

	-- Update the statistics for this creditor
	update	client_creditor
	set	payments_this_creditor		= payments_this_creditor + x.debit_amt
	from	client_creditor cc
	inner join #debt x on cc.client_creditor = x.client_creditor;

	-- Find the amount paid by debt
	select	client_creditor_balance, sum(debit_amt) as debit_amt
	into	#balance
	from	#debt
	group by client_creditor_balance;

	drop table #debt

	-- Update the balances for the debts
	update	client_creditor_balances
	set	payments_month_0		= payments_month_0 + x.debit_amt,
		total_payments			= total_payments + x.debit_amt
	from	client_creditor_balances b
	inner join #balance x on b.client_creditor_balance = x.client_creditor_balance;

	-- Discard working table
	drop table #balance
	drop table #contents
end

-- Update the creditor with the amount disbursed
update	creditors
set	distrib_mtd		= isnull(distrib_mtd,0) + @total_gross
where	creditor		= @good_creditor;

-- Mark the check as now pending
update	registers_trust
set	cleared			= ' ',
	invoice_register	= @invoice_register
where	trust_register		= @destroyed_trust_register;

-- Commit the transaction at this point and terminate
commit transaction
return ( 1 )
GO
