USE [DebtPlus]
GO
/****** Object:  StoredProcedure [dbo].[rpt_missing_creditors]    Script Date: 09/15/2014 13:13:33 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[rpt_missing_creditors] as

select cc.client,
		dbo.format_normal_name(pn.prefix, pn.first, pn.middle, pn.last, pn.suffix) as name,
		cc.client_creditor,
		cc.creditor_name,
		cc.account_number,
		cc.date_created,
		cc.created_by
from	client_creditor cc with (nolock)
inner join clients c on cc.client = c.client
left outer join people p on cc.client = p.client and 1 = p.Relation
left outer join Names pn with (nolock) on p.NameID = pn.Name
where	cc.creditor is null
and     cc.creditor_name is not null
and		c.active_status in ('PND', 'APT', 'RDY', 'PRO', 'A', 'AR')
order by 3, 1, 2
GO
