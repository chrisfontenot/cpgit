USE [DebtPlus]
GO
/****** Object:  StoredProcedure [dbo].[cccs_admin_intake_debts]    Script Date: 09/15/2014 13:13:31 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


create procedure xpr_add_aarp_call_log(
	@client_id	int,
	@is_aarp_client	bit,
	@btw50_program_requested bit,
	@snap_program_requested bit,
	@media_followup bit,
	@housing_situation int,
	@email_optin bit,
	@text_optin bit,
	@preferred_method int,
	@call_reason int,
	@call_resolution int,
	@initial_program_requested int,
	@aarp_code varchar
)
as 
begin

	declare @errorstr varchar(100)

	declare @errorsvr int 
	set @errorsvr = 11

	declare @errorstate int 
	set @errorstate= 1

	if @client_id is not null and @call_reason is not null and @call_resolution is not null
	begin

		insert into aarp_call_log([ClientID], [IsAarpClient], [Btw50ProgramRequested], [SnapProgramRequested], [MediaFollowup], [HousingSituation], [EmailOptIn], [TextOptIn], [PreferredMethod], [CallReason], [CallResolution], [InitialProgramRequested], [AarpCode])
		values(@client_id, @is_aarp_client, @btw50_program_requested, @snap_program_requested, @media_followup, @housing_situation, @email_optin, @text_optin, @preferred_method, @call_reason, @call_resolution, @initial_program_requested, @aarp_code)
		
	end
	else
	begin
		-- throw error
		raiserror('Client or CallReason or CallResolution cannot be blank', @errorsvr, @errorstate)
	end

end