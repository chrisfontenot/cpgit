USE [DebtPlus]
GO
/****** Object:  StoredProcedure [dbo].[rpt_rpps_response_CDP_reject]    Script Date: 09/15/2014 13:13:33 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[rpt_rpps_response_CDP_reject] ( @response_batch_id as varchar(50) ) AS
-- =======================================================================================================
-- ==           Obtain the information for the response conditions of rejected proposals                ==
-- =======================================================================================================

-- ChangeLog
--   2/18/2004
--     Added client's counselor name.

-- Suppress the count of records
set nocount on

-- Retrieve the information for the records
select		b.date_transmitted						as 'proposal_print_date', -- 1
		pr.proposal_status_date						as 'reject_date', -- 2
		isnull(pr.proposed_amount,cc.disbursement_factor)		as 'proposed_amount', -- 3
		coalesce(pr.proposed_start_date,cc.start_date,c.start_date)	as 'start_date', -- 4
		coalesce(r.description,rt.return_code,'')			as 'reject_reason', -- 5
		isnull(pr.counter_amount,0)					as 'counter_offer', -- 6
		isnull(pr.missing_item,'')					as 'missing_item', -- 7

		cc.client							as 'client', -- 8
		cc.creditor							as 'creditor', -- 9 
		cc.client_creditor							as 'client_creditor', -- 10
		cc.disbursement_factor						as 'disbursement_factor', -- 11

		isnull(cr.creditor_name,'')					as 'creditor_name', -- 12
		convert(varchar(80), dbo.format_normal_name(default,cox.first,default,cox.last,default)) as 'counselor_name', -- 13
		
		cr.type, -- 14
		cr.creditor_id -- 15

from		rpps_transactions rt
inner join	client_creditor_proposals pr ON rt.client_creditor_proposal = pr.client_creditor_proposal
inner join	client_creditor cc ON pr.client_creditor = cc.client_creditor
inner join	creditors cr on cc.creditor = cr.creditor
inner join	clients c on cc.client = c.client
left outer join	proposal_batch_ids b on pr.proposal_batch_id = b.proposal_batch_id
left outer join	proposal_result_reasons r on pr.proposal_reject_reason = r.proposal_result_reason
left outer join	counselors co with (nolock) on c.counselor = co.counselor
left outer join names cox with (nolock) on co.NameID = cox.name

where		response_batch_id = @response_batch_id
and		service_class_or_purpose in ('CDP', 'FBC', 'FBD')
and		pr.proposal_status = 4
and		isnull(pr.proposal_reject_reason,1) != 0

order by	14, 15, 9, 8, 10 -- cr.type, cr.creditor_id, cc.creditor, cc.client, cc.client_creditor

return ( @@rowcount )
GO
