USE [DebtPlus]
GO
/****** Object:  StoredProcedure [dbo].[rpt_client_status_summary_creditors]    Script Date: 09/15/2014 13:13:32 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[rpt_client_status_summary_creditors] ( @Client AS INT ) AS
-- ==============================================================================================
-- ==              Generate the client status summary information for CMS reports              ==
-- ==============================================================================================

-- ChangeLog
--   9/31/2002
--     Added support for client_creditor_balances table
--   01/15/2002
--     Added creditor_classes table references and dropped "creditors.fee_creditor"
--   3/14/2003
--     Allow for more than 10,000 creditors in a class
--   06/09/2009
--      Remove references to debt_number and use client_creditor

SELECT
	isnull(bal.payments_month_1,0)				as 'last_payment',
	cc.creditor	as 'creditor',
	cr.creditor_name					as 'creditor_name',
	isnull(cc.message,cc.account_number)			as 'acct',

	-- Fee accounts never have a starting balance
	case isnull(ccl.zero_balance,0)
		when 0 then bal.orig_balance
		else 0
	end as 'starting_balance',

	case
		when isnull(ccl.zero_balance,0) > 0 then cc.disbursement_factor
		when (isnull(bal.orig_balance,0) + isnull(bal.orig_balance_adjustment,0) + isnull(bal.total_interest,0) - isnull(bal.total_payments,0)) <= 0 then 0
		else cc.disbursement_factor
	end as 'dm_payment',

	-- The interest figure is a bit more complicated. Take the specified rate if one is given.
	-- Otherwise, attempt to find the rate from the creditor's data for the balance
	case
		when isnull(ccl.zero_balance,0) > 0 then 0
		when cc.dmp_interest is not null then cc.dmp_interest * (isnull(bal.orig_balance,0) + isnull(bal.orig_balance_adjustment,0) + isnull(bal.total_interest,0) - isnull(bal.total_payments,0)) / 12
		when cr.highest_apr_amt > 0 and cr.highest_apr_amt <= (isnull(bal.orig_balance,0) + isnull(bal.orig_balance_adjustment,0) + isnull(bal.total_interest,0) - isnull(bal.total_payments,0)) then (isnull(bal.orig_balance,0) + isnull(bal.orig_balance_adjustment,0) + isnull(bal.total_interest,0) - isnull(bal.total_payments,0)) * cr.highest_apr_pct / 12
		when cr.medium_apr_amt > 0 and cr.medium_apr_amt <= (isnull(bal.orig_balance,0) + isnull(bal.orig_balance_adjustment,0) + isnull(bal.total_interest,0) - isnull(bal.total_payments,0)) then (isnull(bal.orig_balance,0) + isnull(bal.orig_balance_adjustment,0) + isnull(bal.total_interest,0) - isnull(bal.total_payments,0)) * cr.medium_apr_pct / 12
		else isnull(cr.lowest_apr_pct,0) * (isnull(bal.orig_balance,0) + isnull(bal.orig_balance_adjustment,0) + isnull(bal.total_interest,0) - isnull(bal.total_payments,0)) / 12
	end as 'est_interest',

	-- Fee accounts never have a current balance
	case
		when isnull(ccl.zero_balance,0) > 0 then 0
		else isnull(bal.orig_balance,0) + isnull(bal.orig_balance_adjustment,0) + isnull(bal.total_interest,0) - isnull(bal.total_payments,0)
	end as 'current_balance'

from		client_creditor cc		with (nolock)
inner join	client_creditor_balances bal	WITH (NOLOCK) ON cc.client_creditor_balance = bal.client_creditor_balance
inner join	creditors cr			with (nolock) on cc.creditor = cr.creditor
LEFT OUTER JOIN	creditor_classes ccl		with (nolock) on cr.creditor_class = ccl.creditor_class
where		cc.client = @client
and		cc.reassigned_debt = 0
order by	cr.type, cr.creditor_id, cr.creditor

RETURN ( @@rowcount )
GO
