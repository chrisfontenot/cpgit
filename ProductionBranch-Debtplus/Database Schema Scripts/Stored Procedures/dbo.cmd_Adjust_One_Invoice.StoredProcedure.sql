USE [DebtPlus]
GO
/****** Object:  StoredProcedure [dbo].[cmd_Adjust_One_Invoice]    Script Date: 09/15/2014 13:13:31 ******/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE PROCEDURE [dbo].[cmd_Adjust_One_Invoice] ( @invoice as int ) AS
-- =============================================================================================
-- ==            This procedure will adjust am invoice to take them "off the books"           ==
-- =============================================================================================

-- Suspend the ANSI counting
SET NOCOUNT ON

-- Start a transaction
BEGIN TRANSACTION

declare	@extra		money
declare	@creditor	varchar(10)
select	@extra		= inv_amount - adj_amount - pmt_amount,
	@creditor	= creditor
from	registers_invoices
where	invoice_register	= @invoice

if (@@rowcount <> 1) or (@extra <= 0)
begin
	rollback transaction
	RaisError ('Invoice is not pending', 16, 1)
	return ( 1 )
end

-- Generate the adjustment transactions
insert into registers_creditor (creditor, invoice_register, credit_amt, tran_type, item_date, message)
select	@creditor, @invoice, @extra, 'RA', getdate(), 'Adjusted off invoice'

-- Credit the invoices
update	registers_invoices
set	adj_amount		= isnull(adj_amount,0) + @extra,
	adj_date		= getdate()
where	invoice_register	= @invoice

-- Commit the changes to the database. Once this is done there is no going back.
COMMIT TRANSACTION

-- Restore the ANSI counting
SET NOCOUNT OFF

-- Give the user the bad news
PRINT 'Total Amount of $' + convert(varchar, isnull(@extra,0), 1)
GO
