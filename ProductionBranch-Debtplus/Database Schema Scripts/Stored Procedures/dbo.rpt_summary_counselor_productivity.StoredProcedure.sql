USE [DebtPlus]
GO
/****** Object:  StoredProcedure [dbo].[rpt_summary_counselor_productivity]    Script Date: 09/15/2014 13:13:33 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[rpt_summary_counselor_productivity] ( @From_Date as datetime = null, @To_date as datetime = null ) as
-- ========================================================================================================
-- ==            Summary information for counselor productivity                                          ==
-- ========================================================================================================

set nocount on

if @To_Date is null
	select	@To_Date	= getdate()

if @From_Date is null
	select	@From_Date	= @To_Date

select	@From_Date	= convert(varchar(10), @From_Date, 101),
	@To_Date	= convert(varchar(10), @To_Date, 101) + ' 23:59:59'

-- Find the detail information
select	counselor, office, convert(datetime, convert(varchar(10), start_time, 101)) as start_time, appt_type
into	#summary_detail
from	client_appointments with (nolock)
where	status in ('K','W')
and	office is not null
and	start_time between @From_Date and @To_Date;

-- Generate the result table as the count of appointments
select	counselor, office, appt_type, count(*) as appts, convert(int, 0) as days, convert(int,0) as total_appts
into	#summary_results
from	#summary_detail
group by counselor, office, appt_type;

-- Determine the distict days for each counselor/office
select	distinct counselor, office, start_time
into	#summary_periods
from	#summary_detail;

-- Generate the proper count of items
select	counselor, office, count(*) as days
into	#summary_days
from	#summary_periods
group by counselor, office;

-- Find the total number of appointments for the counsleor/office
select	counselor, office, sum(appts) as total_appts
into	#summary_total
from	#summary_results
group by counselor, office;

update	#summary_results
set	total_appts = b.total_appts
from	#summary_total b
inner join #summary_results a on a.counselor = b.counselor and a.office = b.office;

-- Ensure that the results are properly reflected
update	#summary_results
set	days = b.days
from	#summary_results a
inner join #summary_days b on a.counselor = b.counselor and a.office = b.office;

-- Return the resulting counts for each counselor/office pair
select	isnull(o.name,'office #' + convert(varchar, a.office))			as 'office',
	isnull(dbo.format_normal_name(default, cox.first, default, cox.last, default),'counselor #' + convert(varchar, a.counselor))		as 'counselor',
	coalesce(apt.appt_name, 'type #' + convert(varchar, a.appt_type), '')	as 'appt_type',
	a.appts									as 'appts',
	a.total_appts								as 'total_appts',
	a.days									as 'days'
from	#summary_results a
left outer join counselors co on a.counselor = co.counselor
left outer join names cox on co.nameid = cox.name
left outer join offices o on a.office = o.office
left outer join appt_types apt on a.appt_type = apt.appt_type
order by 1, 2, 3

drop table #summary_results
drop table #summary_days
drop table #summary_periods
drop table #summary_detail
drop table #summary_total

return ( @@rowcount )
GO
