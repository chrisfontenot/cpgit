USE [DebtPlus]
GO
/****** Object:  StoredProcedure [dbo].[xpr_average_administered_debt]    Script Date: 09/15/2014 13:13:34 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE PROCEDURE [dbo].[xpr_average_administered_debt] AS
-- ===============================================================================
-- ==          Calculate the average amount of adminstered debt                 ==
-- ===============================================================================

-- ChangeLog
--   9/31/2002
--     Added support for client_creditor_balances table
--   01/15/2002
--     Added creditor_classes table references and dropped "creditors.fee_creditor"

SET NOCOUNT ON

SELECT	cc.client AS 'client',
	sum(bal.orig_balance+bal.orig_balance_adjustment+bal.total_interest-bal.total_payments) AS 'debt'
INTO	#average_administered_debt
FROM	client_creditor cc		with (nolock)
inner join client_creditor_balances bal	WITH (NOLOCK) ON cc.client_creditor_balance = bal.client_creditor_balance
INNER JOIN creditors cr			with (nolock) ON cc.creditor = cr.creditor
LEFT OUTER JOIN creditor_classes ccl	with (nolock) on cr.creditor_class = ccl.creditor_class
WHERE	isnull(ccl.zero_balance,0) = 0
AND	cc.reassigned_debt = 0
GROUP BY cc.client

SELECT AVG(debt) AS 'average' FROM #average_administered_debt

DROP TABLE #average_administered_debt
SET NOCOUNT OFF
GO
