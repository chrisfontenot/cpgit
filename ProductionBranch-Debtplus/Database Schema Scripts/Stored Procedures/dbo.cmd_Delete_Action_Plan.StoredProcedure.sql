USE [DebtPlus]
GO
/****** Object:  StoredProcedure [dbo].[cmd_Delete_Action_Plan]    Script Date: 09/15/2014 13:13:31 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE PROCEDURE [dbo].[cmd_Delete_Action_Plan] ( @Plan AS INT ) AS
-- ==========================================================================
-- ==               Delete an action plan                                  ==
-- ==========================================================================
set nocount on

-- Fetch the client ID from the action plan
declare @client		INT
declare @created	datetime

SELECT	@client		= client,
	@created	= date_created
FROM	action_plans
where	action_plan	= @Plan

IF @Client is null
begin
	RaisError (50028, 11, 1, @Plan)
	return ( 0 )
end

-- Generate a system note to the effect that the plan is being deleted
insert into client_notes (client, type, is_text, subject, note)
values (@client, 3, 1,
	'Deleted action plan dated ' + convert(varchar, @created, 1) + ' from the system',
	'Deleted action plan dated ' + convert(varchar, @created, 1) + ' from the system')

-- Remove the client action items
delete from client_action_items where action_plan = @Plan

-- Remove the goals text
delete from action_items_goals where action_plan = @Plan

-- Remove the action plan item
delete from action_plans where action_plan = @Plan

return ( 1 )
GO
