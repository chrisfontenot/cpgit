USE [DebtPlus]
GO
/****** Object:  StoredProcedure [dbo].[rpt_sales_summary]    Script Date: 09/15/2014 13:13:33 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[rpt_sales_summary] ( @sales_file as int ) as

-- ===============================================================================================
-- ==            return the information for the summary plan                                    ==
-- ===============================================================================================

declare	@client_monthly_interest	money
declare	@plan_monthly_interest		money
declare	@client_total_interest		money
declare	@plan_total_interest		money
declare	@client_months			float
declare	@plan_months			float

-- Find the common information that is "hard" to calcualte **EXACTLY** the same as the client tool.
select	@client_monthly_interest	= client_monthly_interest,
	@plan_monthly_interest		= plan_monthly_interest,
	@client_total_interest		= client_total_interest,
	@plan_total_interest		= plan_total_interest,
	@client_months			= client_months,
	@plan_months			= plan_months
from	sales_files with (nolock)
where	sales_file			= @sales_file

select
	sum(isnull(balance,0))			as balance,
	sum(isnull(minimum_payment,0))		as minimum_payment,
	sum(isnull(late_fees,0))		as late_fees,
	sum(isnull(overlimit_fees,0))		as overlimit_fees,
	sum(isnull(cosigner_fees,0))		as cosigner_fees,
	sum(isnull(finance_charge,0))		as finance_charge,
	sum(isnull(total_interest_fees,0))	as total_interest_fees,
	sum(isnull(principal,0))		as principal,
	sum(isnull(plan_min_prorate,0))		as plan_min_prorate,
	sum(isnull(plan_min_payment,0))		as plan_min_payment,
	sum(isnull(plan_payment,0))		as plan_payment,
	sum(isnull(plan_finance_charge,0))	as plan_finance_charge,
	sum(isnull(plan_total_fees,0)+isnull(plan_finance_charge,0))		as plan_total_interest_fees,
	sum(isnull(plan_principal,0))		as plan_principal,

	-- Common information
	isnull(@client_monthly_interest,0)	as client_monthly_interest,
	isnull(@plan_monthly_interest,0)	as plan_monthly_interest,
	isnull(@client_total_interest,0)	as client_total_interest,
	isnull(@plan_total_interest,0)		as plan_total_interest,
	isnull(@client_months,0.0)		as client_months,
	isnull(@plan_months,0.0)		as plan_months

from	sales_debts with (nolock)
where	sales_file		= @sales_file

return ( @@rowcount )
GO
