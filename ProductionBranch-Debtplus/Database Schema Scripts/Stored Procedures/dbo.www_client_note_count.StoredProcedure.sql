USE [DebtPlus]
GO
/****** Object:  StoredProcedure [dbo].[www_client_note_count]    Script Date: 09/15/2014 13:13:34 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE PROCEDURE [dbo].[www_client_note_count] ( @client as typ_client ) as
-- =========================================================================================
-- ==            Return the number of notes for the client                                ==
-- =========================================================================================
set nocount on
declare	@item_count	int

declare	@pkid			uniqueidentifier
select	@pkid			= pkid
from	client_www
where	username		= convert(varchar,@client)
and		applicationname	= '/';

select	@item_count	= count(*)
from	client_www_notes with (nolock)
where	pkid			= @pkid
and		date_viewed is null

if @item_count is null
	select	@item_count = 0

return ( @item_count )
GO
