USE [DebtPlus]
GO
/****** Object:  StoredProcedure [dbo].[rpt_deposit_detail]    Script Date: 09/15/2014 13:13:33 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE PROCEDURE [dbo].[rpt_deposit_detail] (@deposit_batch_id as INT) AS
-- ========================================================================
-- ==          Fetch the values for the Client Batch Detail Report       ==
-- ========================================================================

SELECT		d.client				as 'client',
		dbo.format_normal_name(pn.prefix,pn.first,pn.middle,pn.last,pn.suffix) as 'name',
		d.amount				as 'amount',
		d.item_date				as 'item_date',
		d.tran_subtype				as 'type',
		detail.description			as 'description',
		d.reference				as 'reference',
		d.deposit_batch_id			as 'register',
		d.created_by				as 'created_by',
		d.date_created				as 'date_created'

FROM		deposit_batch_details d	WITH ( NOLOCK )
INNER JOIN	people p		WITH ( NOLOCK ) ON d.client = p.client AND 1 = p.relation
left outer join names pn with (nolock) on p.nameid = pn.name
LEFT OUTER JOIN tran_types detail	WITH ( NOLOCK ) ON d.tran_subtype = detail.tran_type
WHERE		isnull(d.client,-1) >= 0
AND		d.deposit_batch_id = @deposit_batch_id
ORDER BY	d.tran_subtype, d.date_created

RETURN ( @@rowcount )
GO
