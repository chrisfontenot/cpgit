USE [DebtPlus]
GO
/****** Object:  StoredProcedure [dbo].[rpt_sales_info]    Script Date: 09/15/2014 13:13:33 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[rpt_sales_info] ( @sales_file as int ) as
-- ======================================================================================
-- ==            Return the standard information about the indicated file              ==
-- ======================================================================================

set nocount on

-- Find the default threshold value from the config table
declare	@threshold	float
declare	@agency		varchar(80)
select	@threshold	= threshold,
	@agency		= name
from	config

-- Return the information about the file
select  isnull(@threshold,0)				as threshold,
	isnull(s.extra_amount,0)			as extra_amount,
	convert(varchar(512),isnull(s.note, ''))	as note,
	isnull(s.client,0)				as client,
	s.date_created					as 'date_created',
	isnull(@agency,'')				as agency_name,
	dbo.format_normal_name(pn.prefix,pn.first,pn.middle,pn.last,pn.suffix) as 'name'
from	sales_files s with (nolock)
left outer join people p with (nolock) on s.client = p.client and 1 = p.relation
left outer join names pn with (nolock) on p.NameID = pn.Name
where	sales_file		= @sales_file

return ( @@rowcount )
GO
