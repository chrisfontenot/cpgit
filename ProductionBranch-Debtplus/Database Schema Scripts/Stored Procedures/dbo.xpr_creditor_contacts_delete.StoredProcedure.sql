USE [DebtPlus]
GO
/****** Object:  StoredProcedure [dbo].[xpr_creditor_contacts_delete]    Script Date: 09/15/2014 13:13:34 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[xpr_creditor_contacts_delete] ( @creditor_contact AS INT ) AS
DELETE	creditor_contacts
WHERE	creditor_contact	= @creditor_contact
RETURN ( @@rowcount )
GO
