USE [DebtPlus]
GO
/****** Object:  StoredProcedure [dbo].[rpt_MonthEndSummary_Returns]    Script Date: 09/15/2014 13:13:33 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[rpt_MonthEndSummary_Returns] ( @FromDate as DateTime = NULL, @ToDate as DateTime = NULL ) AS
-- =============================================================================================
-- ==                   Find the returns by type                                              ==
-- =============================================================================================

IF @ToDate IS NULL
	SELECT @ToDate = getdate()

IF @FromDate IS NULL
	SELECT @FromDate = @ToDate

-- Adjust the dates for the proper settings
SELECT          @FromDate   = CONVERT(varchar(10), @FromDate, 101),
                @ToDate     = CONVERT(varchar(10), @ToDate, 101) + ' 23:59:59';

SELECT		td.description									as 'type',
		convert(datetime, convert(varchar(10), d.date_created, 101) + ' 00:00:00')	as 'item_date',
		d.credit_amt									as 'amount',
		dbo.format_client_id ( d.client )						as 'creditor',
		d.message									as 'reference',
		d.tran_type                                                                     as 'tran_type'

FROM		registers_client d WITH (NOLOCK)
LEFT OUTER JOIN	tran_types td WITH (NOLOCK) ON d.tran_type = td.tran_type
WHERE		d.tran_type IN ('VR', 'RR', 'RF', 'VD')
AND		d.date_created BETWEEN @FromDate AND @ToDate
ORDER BY	1, 2

RETURN ( @@rowcount )
GO
