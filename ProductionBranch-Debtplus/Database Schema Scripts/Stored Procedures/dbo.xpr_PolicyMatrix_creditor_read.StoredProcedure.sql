USE [DebtPlus]
GO
/****** Object:  StoredProcedure [dbo].[xpr_PolicyMatrix_creditor_read]    Script Date: 09/15/2014 13:13:36 ******/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE PROCEDURE [dbo].[xpr_PolicyMatrix_creditor_read] (@creditor as typ_creditor, @policy_type as varchar(20) = NULL) AS

-- ================================================================================================================
-- ==            Retrieve the information for the policy matrix on this creditor                                 ==
-- ================================================================================================================

set nocount on

if @policy_type is null
BEGIN

	SELECT	v.policy_matrix_policy		as policy_matrix_policy,
		t.policy_matrix_policy_type	as policy_matrix_policy_type,
		t.description			as description,
		v.changed_date			as changed_date,
		v.item_value			as item_value,
		v.message			as message
	FROM	policy_matrix_policy_types t
	LEFT OUTER JOIN policy_matrix_policies v ON t.policy_matrix_policy_type=v.policy_matrix_policy_type AND @creditor = v.creditor
	ORDER BY 3

END ELSE BEGIN

	declare	@description			varchar(80)
	declare	@policy_matrix_policy		int
	declare	@changed_date			datetime
	declare	@item_value			varchar(80)
	declare	@message			varchar(1024)

	select	@description			= description
	from	policy_matrix_policy_types with (nolock)
	where	policy_matrix_policy_type	= @policy_type

	select	@policy_matrix_policy		= policy_matrix_policy,
		@changed_date			= changed_date,
		@item_value			= item_value,
		@message			= message
	from	policy_matrix_policies with (nolock)
	where	creditor			= @creditor
	and	policy_matrix_policy_type	= @policy_type

	SELECT	@policy_matrix_policy		as policy_matrix_policy,
		@policy_type			as policy_matrix_policy_type,
		@description			as description,
		@changed_date			as changed_date,
		@item_value			as item_value,
		@message			as message

END
return ( 1 )
GO
