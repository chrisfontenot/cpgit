SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER procedure [cmd_PrintChecks_Restart] ( @disbursement_register as int = null, @checknum as BigInt = null ) as

-- ===============================================================================================================
-- ==            Reset the checks that have been erroneously "accepted" as good back to print status            ==
-- ===============================================================================================================

-- ChangeLog
--   4/17/2003
--     Use isnumeric() to validate the check numbers. CPR has some strange check numbers that are not valid numbers.
--   11/14/2003 
--     Record the destroyed checks in the trust register.
	
if @disbursement_register is null
begin

	print	'Last 10 disbursements'
	select top 10 disbursement_register, date_created, created_by, note
	from	registers_disbursement
	where	tran_type = 'AD'
	order by date_created desc

	return ( 0 )
end

-- Validate that the disbursement is the proper type
if not exists (select * from registers_disbursement where disbursement_register = @disbursement_register and tran_type = 'AD')
begin
	raiserror ('Disbursement %d is not of a valid type', 16, 1, @disbursement_register)
	return ( 0 )
end

begin transaction
set xact_abort on
CREATE table #reset_items (trust_register int, checknum bigint null, tran_type varchar(8) null, creditor varchar(10) null, client int null, amount money, date_created datetime, created_by varchar(80));

-- Clear the checks in the disbursement
insert into #reset_items (trust_register, checknum, tran_type, creditor, client, amount, date_created, created_by)
select	trust_register, checknum, tran_type, creditor, client, amount, date_created, created_by
from	registers_trust
where	trust_register in (
	select	trust_register
	from	registers_creditor
	where	tran_type in ('AD', 'CM')
	and	disbursement_register = @disbursement_register
)
and	cleared not in ('D','V','R')

if @checknum is not null
begin
	delete
	from	#reset_items
	where	checknum < @checknum

	-- Complain if there are no items to reset
	if not exists ( select * from #reset_items where checknum = @checknum )
		RaisError ('The check number %d is not in the list of checks printed in this disbursement batch. The request was cancelled.', 16, 1, @checknum )
end

-- If there is an item in the list then do the work
if exists (select * from #reset_items)
begin

	-- Reset the check status for the checks
	update	registers_trust
	set	cleared		= 'P',
		checknum	= null,
		sequence_number	= null
	where	trust_register in (
		select	trust_register
		from	#reset_items
	)

       	-- Add the deleted items to cover the checks that we are now marking destroyed
	insert into registers_trust (tran_type, client, creditor, checknum, date_created, created_by, amount, cleared, reconciled_date)
	select	tran_type, client, creditor, checknum, date_created, created_by, amount, 'D', getdate()
	from	#reset_items

end

-- Discard the working table
drop table #reset_items

-- Return success
commit transaction
return ( @@rowcount )
GO
