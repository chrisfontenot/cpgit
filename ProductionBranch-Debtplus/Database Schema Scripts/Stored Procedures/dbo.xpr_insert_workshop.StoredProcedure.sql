USE [DebtPlus]
GO
/****** Object:  StoredProcedure [dbo].[xpr_insert_workshop]    Script Date: 09/15/2014 13:13:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[xpr_insert_workshop] ( @workshop_type as int, @start_time as datetime, @workshop_location as int, @seats_available as int, @counselorID as int, @HUD_Grant as int = null, @HousingFeeAmount as money = 0, @Guest as int = null ) as
-- ========================================================================================
-- ==   Procedure for .NET to insert items into the workshop_types table                 ==
-- ========================================================================================
declare @counselor_signon  varchar(80)
select  @counselor_signon = person from counselors where counselor = @counselorID

insert into workshops ( [workshop_type], [start_time], [workshop_location], [seats_available], [counselorID], [counselor], [HUD_Grant], [HousingFeeAmount], [Guest])
values (@workshop_type, @start_time, @workshop_location, @seats_available, @counselorID, @counselor_signon, @HUD_Grant, @HousingFeeAmount, @Guest)
return ( scope_identity() )
GO
