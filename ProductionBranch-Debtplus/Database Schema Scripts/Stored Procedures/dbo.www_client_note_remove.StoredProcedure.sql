USE [DebtPlus]
GO
/****** Object:  StoredProcedure [dbo].[www_client_note_remove]    Script Date: 09/15/2014 13:13:34 ******/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE PROCEDURE [dbo].[www_client_note_remove] ( @client as typ_client, @item_key as int ) as
-- =========================================================================================
-- ==            Mark the note as viewed by the client                                    ==
-- =========================================================================================
set nocount on

declare	@pkid			uniqueidentifier
select	@pkid			= pkid
from	client_www
where	username		= convert(varchar,@client)
and		applicationname	= '/';

-- Mark the record as being viewed
update	client_www_notes
set		date_viewed		= getdate()
where	pkid			= @pkid
and		client_www_note	= @item_key

return ( @@rowcount )
GO
