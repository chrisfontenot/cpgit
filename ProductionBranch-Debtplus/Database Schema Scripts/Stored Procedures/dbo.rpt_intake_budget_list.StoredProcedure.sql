USE [DebtPlus]
GO
/****** Object:  StoredProcedure [dbo].[rpt_intake_budget_list]    Script Date: 09/15/2014 13:13:33 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[rpt_intake_budget_list] ( @intake_client as INT ) AS

-- =================================================================================================
-- ==            Print the contents of the budget for a specific intake client id                 ==
-- =================================================================================================

-- ChangeLog
--   12/19/2008
--      Revisions for SQL2008

-- Suppress intermediate result sets
set nocount on

-- Create a temporary table to hold the combined tables for system and user labels
create table #t_budget_categories (
	budget_category		int,
	[description]		varchar(80) null,
	heading			int,
	detail			int,
	[group]			int null
);

-- Include the system categories into the table
insert into #t_budget_categories (budget_category, [description], heading, detail, [group])
select	budget_category,
	[description],
	heading,
	detail,
	budget_category_group
from budget_categories;

-- Include any cilent categories
insert into #t_budget_categories (budget_category, [description], heading, detail, [group])
select	budget_category,
	[other_budget_category],
	0 as heading,
	1 as detail,
	10000
from	intake_budgets uc
where	intake_client = @intake_client
and	other_budget_category is not null
and	budget_category >= 10000;

update #t_budget_categories
set		[group] = (select max(budget_category)
					from #t_budget_categories b
					where a.budget_category >= b.budget_category
					and b.heading = 1)
from	#t_budget_categories a

-- Return the result set to the caller
select	d.budget_category,	-- for testing purposes only

	case d.heading
	    when 0 then isnull(d.[description],'')
	    else isnull(upper(d.[description]),'')
	end			as 'description',

	d.heading		as 'heading',
	d.detail		as 'detail',

	case d.detail
	    when 0 then null
	    else isnull(b.client_amount,0)
	end			as 'client_amount',
	
	d.[group]

from	#t_budget_categories d
left outer join intake_budgets b on d.budget_category = b.budget_category AND @intake_client = b.intake_client
order by 1;

-- Discard the working table
drop table #t_budget_categories;

return ( @@rowcount )
GO
