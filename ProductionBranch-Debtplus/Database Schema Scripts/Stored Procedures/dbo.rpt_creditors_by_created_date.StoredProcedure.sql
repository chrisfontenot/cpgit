USE [DebtPlus]
GO
/****** Object:  StoredProcedure [dbo].[rpt_creditors_by_created_date]    Script Date: 09/15/2014 13:13:33 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE PROCEDURE [dbo].[rpt_creditors_by_created_date] ( @from_date as datetime = null, @to_date as datetime = null ) as

-- ==================================================================================================
-- ==        List the newly created creditors and the outstanding invoiced amounts                 ==
-- ==================================================================================================

-- Surpress intermediate results
set nocount on

-- "clean" the dates entered.
if @from_date is null
	select	@from_date	= '1/1/1900'

if @to_date is null
	select	@to_date	= '12/31/2199';

select	@from_date = convert(datetime, convert(varchar(10), @from_date, 101) + ' 00:00:00'),
	@to_date   = convert(datetime, convert(varchar(10), @to_date, 101)   + ' 23:59:59')

-- Find the fairshare contact information
declare	@original_creditor_contact_type	int

select	@original_creditor_contact_type	= creditor_contact_type
from	creditor_contact_types with (nolock)
where	contact_type	like '%P%';

if @original_creditor_contact_type is null
	select	@original_creditor_contact_type	= creditor_contact_type
	from	creditor_contact_types with (nolock)
	where	name		= 'ORIGINAL'

if @original_creditor_contact_type is null
	select	@original_creditor_contact_type	= min(creditor_contact_type)
	from	creditor_contact_types with (nolock)

if @original_creditor_contact_type is null
	select	@original_creditor_contact_type	= 1

-- Find the amount outstanding for each creditor
select	creditor, sum(inv_amount - pmt_amount - adj_amount) as outstanding
into	#outstanding
from	registers_invoices with (nolock)
where	inv_amount - pmt_amount - adj_amount > 0
group by creditor;

-- Find the creditor contact that we want
select	creditor, min(creditor_contact) as creditor_contact
into	#contacts
from	creditor_contacts with (nolock)
where	creditor_contact_type = @original_creditor_contact_type
and		(TelephoneID is not null OR NameID is not null)
group by creditor;

-- Generate the report information
select	cr.creditor, cr.creditor_name,
		dbo.format_normal_name (default, ccn.first, ccn.middle, ccn.last, default) as contact_name,
		dbo.format_TelephoneNumber(cc.TelephoneID) as contact_phone,
		ar.PostalCode as zipcode,

	convert(float, case
		when cp.creditor_type_eft is null then 0.0
		when cp.fairshare_pct_eft is null then 0.0
		when cp.creditor_type_eft = 'N' then 0.0
		when cp.fairshare_pct_eft < 0 then 0.0
		else cp.fairshare_pct_eft
		end
	) as contribution_pct,

	convert(money, isnull(o.outstanding,0))	as outstanding_invoice_amt,

	cr.date_created as date_created

from	creditors cr with (nolock)
left outer join creditor_addresses a with (nolock) on cr.creditor = a.creditor and 'P' = a.type
left outer join addresses ar with (nolock) on a.addressid = ar.address
left outer join creditor_contribution_pcts cp with (nolock) on cr.creditor_contribution_pct = cp.creditor_contribution_pct
left outer join #contacts xc on cr.creditor = xc.creditor
left outer join creditor_contacts cc with (nolock) on xc.creditor_contact = cc.creditor_contact
left outer join names ccn with (nolock) on cc.nameid = ccn.name
left outer join #outstanding o on cr.creditor = o.creditor

where	cr.date_created between @from_date and @to_date
order by 5, cr.type, cr.creditor_id

-- Clean up the tables
drop table #outstanding
drop table #contacts

return ( @@rowcount )
GO
