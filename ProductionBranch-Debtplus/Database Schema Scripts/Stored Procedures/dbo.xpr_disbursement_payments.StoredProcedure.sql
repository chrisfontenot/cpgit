USE [DebtPlus]
GO
/****** Object:  StoredProcedure [dbo].[xpr_disbursement_payments]    Script Date: 09/15/2014 13:13:35 ******/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE PROCEDURE [dbo].[xpr_disbursement_payments] ( @client AS INT, @disbursement_register AS INT ) AS

-- ==================================================================================================================
-- ==            Retrieve the information for a specific disbursement                                              ==
-- ==================================================================================================================

-- Retrieve the information about the specific payments in a disbursement for the client and the batch
SELECT	creditor, client_creditor, debit_amt as amount
FROM	registers_client_creditor
WHERE	tran_type in ('AD', 'BW', 'CM', 'MD')
AND	client			= @client
AND	disbursement_register	= @disbursement_register

RETURN ( @@rowcount )
GO
