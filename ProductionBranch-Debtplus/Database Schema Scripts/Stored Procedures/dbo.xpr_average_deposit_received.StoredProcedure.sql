USE [DebtPlus]
GO
/****** Object:  StoredProcedure [dbo].[xpr_average_deposit_received]    Script Date: 09/15/2014 13:13:34 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE PROCEDURE [dbo].[xpr_average_deposit_received] AS
-- ====================================================================================
-- ==          Calculate the average deposit amounts that are actually received      ==
-- ====================================================================================
SET NOCOUNT ON

SELECT		client as 'client',
		sum(credit_amt)	as 'amount'
INTO		#average_deposit_amounts_received

FROM		registers_client

WHERE		tran_type = 'DP'
AND		client >= 0

GROUP BY	client

SELECT AVG(convert(float,amount)) AS 'average' FROM #average_deposit_amounts_received

DROP TABLE #average_deposit_amounts_received
SET NOCOUNT OFF
GO
