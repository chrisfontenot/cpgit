USE [DebtPlus]
GO
/****** Object:  StoredProcedure [dbo].[www_creditor_message_update]    Script Date: 09/15/2014 13:13:34 ******/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE PROCEDURE [dbo].[www_creditor_message_update] ( @msgid as int, @msgtext as varchar(512) ) as

-- ===================================================================================
-- ==            Update the message with the text from the user                     ==
-- ===================================================================================

update	creditor_www_notes
set	message			= @msgtext
where	creditor_www_note	= @msgid

return ( @@rowcount )
GO
