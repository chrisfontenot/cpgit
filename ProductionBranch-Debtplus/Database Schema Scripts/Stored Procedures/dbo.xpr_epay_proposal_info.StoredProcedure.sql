USE [DebtPlus]
GO
/****** Object:  StoredProcedure [dbo].[xpr_epay_proposal_info]    Script Date: 09/15/2014 13:13:35 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE PROCEDURE [dbo].[xpr_epay_proposal_info] ( @bank as int = null ) AS
-- ====================================================================================================
-- ==            Retrieve the configuration information for EPAY proposals                           ==
-- ====================================================================================================

-- ChangeLog
--   1/31/2003
--     Changed to use the banks table rather than 'config'.
--   12/21/2005
--     Added ach_origin_dfi to the result set.

-- Suppress intermediate result sets
SET NOCOUNT ON

-- Find the corresponding bank information
if isnull(@bank,0) <= 0
	select	@bank	= null

if @bank is null
	select	@bank		= min(bank)
	from	banks with (nolock)
	where	type		= 'E'
	and	[default]	= 1

if @bank is null
	select	@bank		= min(bank)
	from	banks with (nolock)
	where	type		= 'E'

if @bank is null
	select	@bank	= 1

-- The bank must be a valid item
declare	@originator_id		varchar(20)
select	@originator_id	= immediate_origin
from	banks with (nolock)
where	type		= 'E'
and	bank		= @bank

if @originator_id is null
begin
	RaisError ('Bank account # %d is not configured for EPAY', 16, 1, @bank)
	return ( 0 )
end

-- Fetch the information for EPAY from the configuration table
select	'DPSHEADER'						as 'record_type',
	@originator_id						as 'originator_id',
	immediate_origin_name					as 'originator_name',
	'EPAY'							as 'product_identifier',
	'CCCS'							as 'file_type',
	' '							as 'function',
	prefix_line						as 'prefix_line',
	suffix_line						as 'suffix_line',
	batch_number						as 'batch_id',
	transaction_number					as 'transaction_id',
	ach_origin_dfi						as 'origin_dfi',

	-- The agency sub ID is assigned by the concentrator.
	left(@originator_id, 2)					as 'agency_sub_id'	-- For Dallas

from	banks with (nolock)
where	bank		= @bank

RETURN ( @@rowcount )
GO
