USE [DebtPlus]
GO
/****** Object:  StoredProcedure [dbo].[custom_counseling_statistics]    Script Date: 09/15/2014 13:13:32 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[custom_counseling_statistics]  (@from_date datetime = null, @to_date datetime = null) as

-- Do a custom monthly statistics extract to excel for Orange County

declare @sheet			varchar(80)
select	@sheet			= '1'

if @to_date is null
	select	@to_date = GETDATE()
	
if @from_date is null
	select	@from_date = @to_date

select	@from_date	= convert(varchar(10), @from_date, 101)
select	@to_date	= dateadd(day, 1, convert(datetime, convert(varchar(10), @to_date, 101)))

declare	@year_start	datetime
declare	@year_end	datetime
select	@year_start = dbo.mkdate(1, 1, DATEPART(year, @from_date)),
		@year_end	= @to_date

-- Create the result table
create table #results ( sheet varchar(80), location varchar(80), value decimal(10,2))

-- Find the YTD figures
select	ca.start_time, ca.appt_type, ca.office, ca.result, dbo.custom_counseling_statistics_appt_type(ca.appt_type) as type_row, dbo.custom_counseling_statistics_office(ca.office) as office_row, CONVERT(int,0) as in_person, CONVERT(int,0) as telephone, convert(int,0) as internet, CONVERT(int,0) as dmp_result, CONVERT(int,0) as housing, CONVERT(int,0) as bankruptcy
into	#items_mtd
from	client_appointments ca
where	ca.start_time >= @year_start
and		ca.start_time <  @year_end
and		ca.status in ('K','W')
and		ca.office is not null;

-- Mark the items that are DMP status
update	#items_mtd set dmp_result = 1 where result = 'DMP'

-- Mark the type of the contact
update	#items_mtd
set		in_person = 1
from	#items_mtd x
inner join appt_types t on x.appt_type = t.appt_type
inner join FirstContactTypes m on t.contact_type = m.oID
where	m.nfcc = 'F'

-- Mark the telephone appointments
update	#items_mtd
set		telephone = 1
from	#items_mtd x
inner join appt_types t on x.appt_type = t.appt_type
inner join FirstContactTypes m on t.contact_type = m.oID
where	m.nfcc = 'P'

-- Mark the internet appointments
update	#items_mtd
set		internet = 1
from	#items_mtd x
inner join appt_types t on x.appt_type = t.appt_type
inner join FirstContactTypes m on t.contact_type = m.oID
where	m.nfcc = 'I'

-- Mark the bankruptcy appointments
update	#items_mtd
set		bankruptcy = 1
from	#items_mtd x
inner join appt_types t on x.appt_type = t.appt_type
where	t.bankruptcy = 1

-- Mark the housing appointments
update	#items_mtd
set		housing = 1
from	#items_mtd x
inner join appt_types t on x.appt_type = t.appt_type
where	t.housing = 1
or		t.appt_type in (28, 34, 35)

-- B40 = YTD Available appts
declare	@ytd_available	int
select	@ytd_available = count(*)
from	appt_counselors co
inner join appt_times t on co.appt_time = t.appt_time
where co.inactive = 0
and   t.start_time >= @year_start
and   t.start_time < @year_end;

insert into #results (sheet, location, value)
select	@sheet, 'B40', convert(decimal(10,2), ISNULL(@ytd_available, 0))

-- C40 = YTD Kept appointments
declare	@ytd_kept	int
select	@ytd_kept = count(*) from #items_mtd

insert into #results (sheet, location, value)
select	@sheet, 'C40', convert(decimal(10,2), ISNULL(@ytd_kept, 0))

-- It is also in B25
insert into #results (sheet, location, value)
select	@sheet, 'B25', convert(decimal(10,2), ISNULL(@ytd_kept, 0))

-- F25 = YTD telephone appointments
insert into #results (sheet, location, value)
select	@sheet, 'F25', CONVERT(decimal(10,2),sum(telephone))
from	#items_mtd

-- D25 = YTD in_person appointments
insert into #results (sheet, location, value)
select	@sheet, 'D25', CONVERT(decimal(10,2),sum(in_person))
from	#items_mtd

-- H25 = YTD internet appointments
insert into #results (sheet, location, value)
select	@sheet, 'H25', CONVERT(decimal(10,2),sum(internet))
from	#items_mtd

-- F40 - YTD Housing
insert into #results (sheet, location, value)
select	@sheet, 'F40', CONVERT(decimal(10,2),count(*))
from	#items_mtd
where	housing = 1

-- H40 - YTD Bankruptcy
insert into #results (sheet, location, value)
select	@sheet, 'H40', CONVERT(decimal(10,2),sum(bankruptcy))
from	#items_mtd

-- J40 - YTD DMP results
insert into #results (sheet, location, value)
select	@sheet, 'J40', CONVERT(decimal(10,2),sum(dmp_result))
from	#items_mtd

-- It is also in J25
insert into #results (sheet, location, value)
select	@sheet, 'J25', CONVERT(decimal(10,2),sum(dmp_result))
from	#items_mtd

-- I3 - I11 - YTD appointment types
insert into #results (sheet, location, value)
select	@sheet, 'I' + CONVERT(varchar,type_row + 2), CONVERT(decimal(10,2),count(*))
from	#items_mtd
group by type_row
order by 2, 1, 3

-- Toss all but the current month. We no longer want to examine YTD information
delete
from	#items_mtd
where	start_time < @from_date

-- Find the number of available appointments for the month
select	dbo.custom_counseling_statistics_office(t.office) as office_row, CONVERT(int,1) as available_appts
into	#available_mtd_1
from	appt_times t
inner join appt_counselors co on t.appt_time = co.appt_time
where	co.inactive = 0
and		t.start_time >= @from_date
and		t.start_time < @to_date

select	office_row, count(*) as available_appts
into	#available_mtd
from	#available_mtd_1
group by office_row;

drop table #available_mtd_1

-- B28 - B38 - Available Appointments
insert into #results (sheet, location, value)
select	@sheet, 'B' + CONVERT(varchar,office_row + 27), CONVERT(decimal(10,2),available_appts)
from	#available_mtd
order by 2, 1, 3

-- C28 - C38 - Appointments Kept
insert into #results (sheet, location, value)
select	@sheet, 'C' + CONVERT(varchar,office_row + 27), CONVERT(decimal(10,2),count(*))
from	#items_mtd
group by office_row
order by 2, 1, 3

-- F28 - F38 - Housing
insert into #results (sheet, location, value)
select	@sheet, 'F' + CONVERT(varchar,office_row + 27), CONVERT(decimal(10,2),count(*))
from	#items_mtd
where	housing = 1
group by office_row
order by 2, 1, 3

-- H28 - H38 - Bankruptcy
insert into #results (sheet, location, value)
select	@sheet, 'H' + CONVERT(varchar,office_row + 27), CONVERT(decimal(10,2),sum(bankruptcy))
from	#items_mtd
group by office_row
order by 2, 1, 3

-- J28 - J38 - DMP Results
insert into #results (sheet, location, value)
select	@sheet, 'J' + CONVERT(varchar,office_row + 27), CONVERT(decimal(10,2),sum(dmp_result))
from	#items_mtd
group by office_row
order by 2, 1, 3

-- D15 - D23 - in person
insert into #results (sheet, location, value)
select	@sheet, 'D' + CONVERT(varchar,type_row + 14), CONVERT(decimal(10,2),sum(in_person))
from	#items_mtd
group by type_row
order by 2, 1, 3

-- F15 - F23 - DMP Results
insert into #results (sheet, location, value)
select	@sheet, 'F' + CONVERT(varchar,type_row + 14), CONVERT(decimal(10,2),sum(telephone))
from	#items_mtd
group by type_row
order by 2, 1, 3

-- H15 - H23 - Internet
insert into #results (sheet, location, value)
select	@sheet, 'H' + CONVERT(varchar,type_row + 14), CONVERT(decimal(10,2),sum(internet))
from	#items_mtd
group by type_row
order by 2, 1, 3

-- J15 - J23 - DMP Results
insert into #results (sheet, location, value)
select	@sheet, 'J' + CONVERT(varchar,type_row + 14), CONVERT(decimal(10,2),sum(dmp_result))
from	#items_mtd
group by type_row
order by 2, 1, 3

-- Find the contribution information for MTD
declare	@paf_creditor		varchar(10)
declare	@setup_creditor		varchar(10)
declare	@deduct_creditor	varchar(10)
select	@paf_creditor		= paf_creditor,
		@deduct_creditor	= deduct_creditor,
		@setup_creditor		= setup_creditor
from	config

-- Generate a list of the amounts by the setup creditor
create table #contrib_setup (office int null, mapped_office int null, amount decimal(10,2) null);

insert into #contrib_setup (office, amount)
select		c.office, sum(rc.debit_amt) as amount
from		registers_client_creditor rc
inner join clients c with (nolock) on rc.client = c.client
left outer join creditors cr with (nolock) on rc.creditor = cr.creditor
LEFT OUTER JOIN creditor_classes ccl with (nolock) on cr.creditor_class = ccl.creditor_class

where	rc.tran_type in ('AD', 'BW', 'MD', 'CM')
and		rc.void = 0
and		rc.date_created >= @from_date
and		rc.date_created < @to_date
and		rc.creditor != @paf_creditor
and		rc.creditor != @deduct_creditor
and		(isnull(ccl.zero_balance,0) > 0 or rc.creditor = @setup_creditor)
group by c.office;

update	#contrib_setup set mapped_office = dbo.custom_counseling_statistics_office(office)

insert into #results ( sheet, location, value )
select	@sheet, 'N' + CONVERT(varchar, mapped_office + 27), CONVERT(decimal(10,2), SUM(amount))
from	#contrib_setup
group by mapped_office;

-- Find the YTD value
declare		@ytd_setup		decimal(10,2)
select		@ytd_setup = sum(rc.debit_amt)
from		registers_client_creditor rc
left outer join creditors cr with (nolock) on rc.creditor = cr.creditor
LEFT OUTER JOIN creditor_classes ccl with (nolock) on cr.creditor_class = ccl.creditor_class

where	rc.tran_type in ('AD', 'BW', 'MD', 'CM')
and		rc.void = 0
and		rc.date_created >= @year_start
and		rc.date_created < @year_end
and		rc.creditor != @paf_creditor
and		rc.creditor != @deduct_creditor
and		(isnull(ccl.zero_balance,0) > 0 or rc.creditor = @setup_creditor)

insert into #results (sheet, location, value)
select	@sheet, 'N40', CONVERT(decimal(10,2),isnull(@ytd_setup,0))

-- Generate a list of the amounts by the fee creditor
create table #contrib_paf (office int null, mapped_office int null, amount decimal(10,2) null);

insert into #contrib_paf (office, amount)
select	c.office, sum(rc.debit_amt) as amount
from	registers_client_creditor rc
inner join clients c with (nolock) on rc.client = c.client
where	rc.tran_type in ('AD', 'BW', 'MD', 'CM')
and		rc.void = 0
and		rc.date_created >= @from_date
and		rc.date_created < @to_date
and		rc.creditor = @paf_creditor
group by c.office;

update	#contrib_paf set mapped_office = dbo.custom_counseling_statistics_office(office)

insert into #results ( sheet, location, value )
select	@sheet, 'M' + CONVERT(varchar, mapped_office + 27), CONVERT(decimal(10,2), SUM(amount))
from	#contrib_paf
group by mapped_office;

-- Find the YTD value
declare	@ytd_paf	decimal(10,2)
select	@ytd_paf = sum(rc.debit_amt)
from	registers_client_creditor rc
where	rc.tran_type in ('AD', 'BW', 'MD', 'CM')
and		rc.void = 0
and		rc.date_created >= @year_start
and		rc.date_created < @year_end
and		rc.creditor = @paf_creditor

insert into #results (sheet, location, value)
select	@sheet, 'M40', CONVERT(decimal(10,2),isnull(@ytd_paf,0))

-- Return the results
select sheet,location,sum(isnull(value,0)) as value from #results group by sheet,location order by 1, 2

-- Toss the temporary tables
drop table	#available_mtd
drop table	#items_mtd
drop table	#results
GO
