USE [DebtPlus]
GO
/****** Object:  StoredProcedure [dbo].[www_lst_financial_problems]    Script Date: 09/15/2014 13:13:34 ******/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE PROCEDURE [dbo].[www_lst_financial_problems] AS

-- ============================================================================================================
-- ==            Provide access to the resource types for the www_user that does not have access to lst_*    ==
-- ============================================================================================================

SELECT	financial_problem	as 'item_key',
		[description]		as 'description'
FROM	financial_problems with (nolock)
WHERE	[ActiveFlag] <> 0
ORDER BY [description]

return ( @@rowcount )
GO
