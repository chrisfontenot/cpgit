USE [DebtPlus]
GO
/****** Object:  StoredProcedure [dbo].[xpr_Compare_Disbursements_Deposits]    Script Date: 09/15/2014 13:13:34 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE PROCEDURE [dbo].[xpr_Compare_Disbursements_Deposits] ( @Client AS INT ) AS

-- ====================================================================================================
-- ==               Determine if the disbursements equals the deposit amounts                        ==
-- ====================================================================================================

-- ChangeLog
--   9/31/2002
--     Added support for client_creditor_balances table
--   01/15/2002
--     Added creditor_classes table references and dropped "creditors.fee_creditor"

-- Prevent temporary recordsets from being generated
SET NOCOUNT ON

DECLARE @Disbursements	MONEY
DECLARE @Deposits	MONEY
DECLARE @Deposit_Count	INT

-- Change the following from "0" to "1" to enable the processing of this routine
DECLARE	@Enabled	int
SELECT	@Enabled	= 0

if @Enabled > 0
BEGIN
	-- Fetch the sum of the disbursement amounts
	SELECT		@Disbursements = SUM(cc.disbursement_factor)
	FROM		client_creditor cc
	inner join	client_creditor_balances bal on cc.client_creditor_balance = bal.client_creditor_balance
	INNER JOIN	creditors cr ON cc.creditor = cr.creditor
	LEFT OUTER JOIN	creditor_classes ccl with (nolock) on cr.creditor_class = ccl.creditor_class
	WHERE		client = @Client
	AND		reassigned_debt = 0
	AND		(((isnull(ccl.zero_balance,0) > 0)) OR (isnull(bal.orig_balance,0)+isnull(bal.orig_balance_adjustment,0)+isnull(bal.total_interest,0) > isnull(bal.total_payments,0)))

	-- Fetch the sum of the deposits amounts
	SELECT		@Deposits = SUM(deposit_amount),
			@Deposit_Count = COUNT(*)
	FROM		client_deposits
	WHERE		client = @Client
	AND		isnull(one_time,0) = 0
END

-- Return the results to the user. The two numbers are compared in the application later.
SELECT	isnull(@Disbursements,0)	as 'disbursements',
	isnull(@Deposits,0)		as 'deposits',
	isnull(@Deposit_Count,1)	as 'deposit_count'

RETURN ( 1 )
GO
