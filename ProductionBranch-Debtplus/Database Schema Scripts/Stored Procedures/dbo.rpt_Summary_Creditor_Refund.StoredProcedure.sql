USE [DebtPlus]
GO
/****** Object:  StoredProcedure [dbo].[rpt_Summary_Creditor_Refund]    Script Date: 09/15/2014 13:13:33 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE PROCEDURE [dbo].[rpt_Summary_Creditor_Refund] ( @FromDate AS DateTime = NULL, @ToDate AS DateTime = NULL ) AS
-- ======================================================================================================
-- ==                   Fetch the information for creditor refunds to clients                          ==
-- ======================================================================================================

-- ChangeLog
--   3/14/2003
--     Allow for more than 10,000 creditors in a class

IF @ToDate IS NULL
	SELECT @ToDate = getdate()

IF @FromDate IS NULL
	SELECT @FromDate = @ToDate

-- Adjust the dates for the proper settings
SELECT @FromDate	= convert(datetime, convert(varchar(10), @FromDate, 101) + ' 00:00:00')
SELECT @ToDate	= convert(datetime, convert(varchar(10), @ToDate, 101)   + ' 23:59:59')

-- Fetch the transactions
SELECT		d.client						as 'client',
		d.creditor						as 'creditor',
		d.client_creditor					as 'client_creditor',
		dbo.format_normal_name(pn.prefix,pn.first,pn.middle,pn.last,pn.suffix) as 'client_name',
		cr.creditor_name					as 'creditor_name',
		case
			when d.tran_type = 'RF' then d.credit_amt
			when d.tran_type = 'VF' then 0 - d.debit_amt
			else 0
		end							as 'gross',

		case
			when d.tran_type = 'RF' then d.fairshare_amt
			when d.tran_type = 'VF' then 0 - d.fairshare_amt
			else 0
		end							as 'deducted',

		d.date_created						as 'item_date',
		d.created_by						as 'counselor'
FROM		registers_client_creditor d
LEFT OUTER JOIN	people p on d.client = p.client AND 1 = p.relation
left outer join names pn with (nolock) on p.nameid = pn.name
left outer join	creditors cr on d.creditor = cr.creditor
WHERE		d.tran_type in ('RF','VF')
AND		d.date_created BETWEEN @FromDate AND @ToDate
ORDER BY	d.date_created, cr.type, cr.creditor_id, d.creditor, d.client, d.client_creditor

RETURN ( @@rowcount )
GO
