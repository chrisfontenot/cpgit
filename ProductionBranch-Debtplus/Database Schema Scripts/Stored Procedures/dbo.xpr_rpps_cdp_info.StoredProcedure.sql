USE [DebtPlus]
GO
/****** Object:  StoredProcedure [dbo].[xpr_rpps_cdp_info]    Script Date: 09/15/2014 13:13:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[xpr_rpps_cdp_info] ( @rpps_transaction AS INT, @Last_Communication AS VarChar(15) = NULL ) AS

-- ===========================================================================================
-- ==            Generate an RPPS CDP information for a specific proposal                   ==
-- ===========================================================================================

-- ChangeLog
--   9/31/2002
--     Added support for client_creditor_balances table
--   01/15/2002
--     Added creditor_classes table references and dropped "creditors.fee_creditor"
--   8/11/2003
--     Added proposal_count for new specification
--     Added second social security number. Zero filled.
--     Made social security numbers required. Zero filled.
--   6/22/2005
--     - Ignore disbursement factor when the balance is negative. (Caused negative disbursement factors.)
--     - Changed to use map_client_to_budget rather than hard-coded logic (old method).
--   12/4/2006
--     Added min_start_date to ensure that the debts start date is at least 5 days in the future.

-- Suppress intermediate result sets
SET NOCOUNT ON

declare	@min_start_date		datetime
select	@min_start_date		= dbo.date_only ( dateadd( d, 5, getdate() ));

-- Determine the proposal from the transaction record
DECLARE	@client_creditor_proposal		INT
SELECT	@client_creditor_proposal = client_creditor_proposal
FROM	rpps_transactions WITH (NOLOCK)
WHERE	rpps_transaction = @rpps_transaction

-- Fetch the information from the current proposal information
DECLARE	@Client			int
DECLARE	@client_creditor	int
DECLARE	@FirstCounsel		datetime
DECLARE	@Proposed_Start		DateTime
DECLARE	@Proposed_Payment	Money
DECLARE	@Account_Balance	Money
DECLARE	@Account_Number		VarChar(50)
DECLARE	@signer			int

DECLARE	@rpps_client_type_indicator	varchar(1)
DECLARE	@terms	int
DECLARE	@percent_balance	float

-- Fetch the information about the current debt
SELECT	@Client			= cc.client,
	@client_creditor	= cc.client_creditor,
	@Proposed_Start		= cc.start_date,
	@Proposed_Payment	= cc.disbursement_factor,
	@signer				= cc.person,
	@Account_Balance	= isnull(bal.orig_balance,0) + isnull(bal.orig_balance_adjustment,0) + isnull(bal.total_interest,0) - isnull(bal.total_payments,0),
	@Account_Number		= left(cc.account_number,22),
	@percent_balance	= coalesce(cc.percent_balance, 1.0),
	@terms				= coalesce(cc.terms, 0),
	@rpps_client_type_indicator    = isnull(cc.rpps_client_type_indicator, ' ')

FROM		client_creditor_proposals prop WITH (NOLOCK)
INNER JOIN	client_creditor cc WITH (NOLOCK) ON prop.client_creditor = cc.client_creditor
inner join	client_creditor_balances bal with (nolock) on cc.client_creditor_balance = bal.client_creditor_balance
WHERE		prop.client_creditor_proposal = @client_creditor_proposal

-- Ensure that the start date is valid.
if @proposed_start is null
	select	@proposed_start	= @min_start_date;

if @proposed_start < @min_start_date
	select	@proposed_start	= @min_start_date;

-- Neither of these should be negative
if @Account_Balance <= 0
	select	@Account_Balance	= 0

if @Proposed_Payment <= 0
	select	@Proposed_Payment	= 0

-- Ensure the debt signer is valid
if @signer is null
	select	@signer = person
	from	people
	where	client = @client
	and		relation = 1
	
declare	@relation	int
select	@relation = relation
from	people
where	Person = @signer

-- Default the start date to the client value if there is not one for the debt. Fetch the date of the first counsel (client create date)
declare		@bankruptcy_class	int
SELECT		@Proposed_Start		= isnull(@proposed_start, start_date),
			@FirstCounsel		= date_created
FROM		clients with (nolock)
WHERE		client = @client

-- Fetch the social security number
DECLARE		@SSN1		VarChar(9)
SELECT		@SSN1		= ssn
from		people with (nolock)
where		client = @client
and			relation = 1

IF @SSN1 IS NULL
	SELECT @SSN1 = '000000000'

-- Find the spouse's social security number
DECLARE		@SSN2		varchar(9)
SELECT		@SSN2		= ssn
from		people with (nolock)
where		client = @client
and			relation <> 1

IF @SSN2 IS NULL
	SELECT @SSN2 = '000000000'

-- Switch the ssn numbers if the signer is different
if @relation <> 1
	select	@ssn1 = @ssn2, @ssn2 = @ssn1

-- Compute the total number of creditors for this client
DECLARE	@Creditor_Count		int

SELECT		@Creditor_Count			= COUNT(*)
FROM		client_creditor cc with (nolock)
inner join	client_creditor_balances bal with (nolock)	on cc.client_creditor_balance = bal.client_creditor_balance
INNER JOIN	creditors cr with (nolock)			ON cc.creditor	= cr.creditor
LEFT OUTER JOIN	creditor_classes ccl with (nolock)		on cr.creditor_class = ccl.creditor_class

WHERE		isnull(ccl.proposal_balance,1) <> 0
AND		isnull(cc.reassigned_debt,0)	= 0
AND		cc.client			= @Client
AND		isnull(bal.orig_balance,0) + isnull(bal.orig_balance_adjustment,0) + isnull(bal.total_interest,0) - isnull(bal.total_payments,0) > 0
AND		cc.disbursement_factor > 0

IF @Creditor_Count IS NULL
	SELECT @Creditor_Count = 0

-- Find the total consumer debt
DECLARE	@Total_Orig_Balance	Money
DECLARE	@Total_Orig_Balance_Adj	Money
DECLARE	@Total_Interest		Money
DECLARE	@Total_Payments		Money

SELECT		@Total_Orig_Balance		= SUM(isnull(bal.orig_balance,0)),
		@Total_Orig_Balance_Adj		= SUM(isnull(bal.orig_balance_adjustment,0)),
		@Total_Interest			= SUM(isnull(bal.total_interest,0)),
		@Total_Payments			= SUM(isnull(bal.total_payments,0))
FROM		client_creditor cc with (nolock)
inner join	client_creditor_balances bal with (nolock) on cc.client_creditor_balance = bal.client_creditor_balance
INNER JOIN	creditors cr with (nolock) ON cc.creditor = cr.creditor
LEFT OUTER JOIN	creditor_classes ccl with (nolock) on cr.creditor_class = ccl.creditor_class
WHERE		cc.client			= @client
AND		isnull(ccl.proposal_balance,1) <> 0
AND		isnull(cc.reassigned_debt,0)	= 0

-- The debt must have a positive balance to be included in the total amounts
AND		bal.orig_balance + bal.orig_balance_adjustment + bal.total_interest > bal.total_payments

DECLARE	@Total_Debt		Money
SELECT	@Total_Debt = isnull(@Total_Orig_balance,0) + isnull(@Total_Orig_balance_Adj,0) + isnull(@Total_Interest,0) - isnull(@Total_Payments,0)

-- Find the total monthly income
DECLARE	@Total_Income		Money
SELECT	@Total_Income	= sum(coalesce(net_income,gross_income,0))
FROM	people
WHERE	client		= @client

DECLARE	@Total_Assets		money
SELECT	@Total_Assets	= sum(asset_amount)
FROM	assets
WHERE	client		= @client

SELECT	@Total_Income = isnull(@Total_Income,0) + isnull(@Total_Assets,0)

-- Find the total consumer debt payments
DECLARE	@Total_Debt_Payments	Money
SELECT	@Total_Debt_Payments = sum(
	case
		when isnull(ccl.zero_balance,0) = 1 then cc.disbursement_factor
		when isnull(bal.orig_balance,0)+isnull(bal.orig_balance_adjustment,0)+isnull(bal.total_interest,0)-isnull(bal.total_payments,0) <= 0 then 0
		when isnull(bal.orig_balance,0)+isnull(bal.orig_balance_adjustment,0)+isnull(bal.total_interest,0)-isnull(bal.total_payments,0) > isnull(cc.disbursement_factor,0) then isnull(cc.disbursement_factor,0)
		else isnull(bal.orig_balance,0)+isnull(bal.orig_balance_adjustment,0)+isnull(bal.total_interest,0)-isnull(bal.total_payments,0)
	end)

FROM		client_creditor cc		with (nolock)
inner join	client_creditor_balances bal	with (nolock) on cc.client_creditor_balance = bal.client_creditor_balance
INNER JOIN	creditors cr			with (nolock) on cc.creditor = cr.creditor
LEFT OUTER JOIN	creditor_classes ccl		with (nolock) on cr.creditor_class = ccl.creditor_class

WHERE		cc.reassigned_debt = 0
AND		cr.creditor is not null
AND		cc.client = @client
AND		isnull(ccl.proposal_balance,1) <> 0

-- Find the consumer living expenses
DECLARE	@Living_Expenses	Money
DECLARE	@Budget_ID		INT

select	@budget_id = dbo.map_client_to_budget ( @client )
IF @Budget_ID IS NOT NULL
BEGIN
	SELECT		@Living_Expenses = sum(suggested_amount)
	FROM		budget_detail d
	WHERE		d.budget = @Budget_ID
END

-- Fetch the consumer home phone
DECLARE		@HomePhone	VarChar(10)
SELECT		@HomePhone = dbo.format_TelephoneNumber_BaseOnly(HomeTelephoneID)
FROM		clients
WHERE		client = @client

-- Fetch the consumer work phone
DECLARE		@WorkPhone	VarChar(10)
SELECT		@WorkPhone = dbo.format_TelephoneNumber_BaseOnly(WorkTelephoneID)
FROM		people
WHERE		client = @client
and			WorkTelephoneID is not null
ORDER BY	relation desc

IF @HomePhone IS NULL
	SELECT @HomePhone = isnull(@WorkPhone,'0000000000')

IF @WorkPhone IS NULL
	SELECT @WorkPhone = @HomePhone

-- Update the proposal record with the information
update	client_creditor_proposals
SET	proposed_amount		 = @proposed_payment,
	proposed_start_date	 = @proposed_start,
	proposed_balance	 = @account_balance,
	[percent_balance]	 = @percent_balance,
	[rpps_client_type_indicator]	= @rpps_client_type_indicator,
	[terms]				= @terms
where	client_creditor_proposal = @client_creditor_proposal

-- Update the last communication value for this debt record
IF @Last_Communication IS NOT NULL
BEGIN
	UPDATE	client_creditor
	SET	last_communication	= @Last_Communication
	WHERE	client_creditor		= @client_creditor
END

-- Find the number of proposals for this debt
declare	@proposal_count		int

select	@proposal_count	= count(*)
from	client_creditor_proposals with (nolock)
where	proposal_status in (2, 3, 4)
and	client_creditor		 =  @client_creditor
and	client_creditor_proposal <> @client_creditor_proposal

-- Return the information from the request
SELECT	isnull(@client,0)		as client,
	isnull(@ssn1,'000000000')	as ssn,
	isnull(@ssn2,'000000000')	as ssn2,
	isnull(@FirstCounsel,0)		as 'first_counsel',
	isnull(@proposed_payment,0)	as 'proposed_payment',
	isnull(@proposed_start,0)	as 'proposed_start',
	isnull(@account_balance,0)	as 'account_balance',
	isnull(@account_number,0)	as 'account_number',
	isnull(@creditor_count,0)	as 'creditor_count',
	isnull(@Total_Debt,0)		as 'total_debt',
	isnull(@total_income,0)		as 'total_income',
	isnull(@total_debt_payments,0)	as 'total_debt_payments',
	isnull(@living_expenses,0)	as 'living_expenses',
	isnull(@HomePhone,'0000000000')	as 'home_phone',
	isnull(@WorkPhone,'0000000000')	as 'work_phone',
	''				as 'special_identifier',

	convert(int, case
		when @proposal_count is null then 1
		when @proposal_count > 8 then 9
		else @proposal_count + 1
	end)				as 'proposal_count',

	-- Additional fields for bankruptcy information
	@rpps_client_type_indicator		as 'bankruptcy_flag',
	@percent_balance		as 'percent_balance',
	@terms		as 'bankruptcy_term'

-- Return success to the caller
RETURN ( 1 )
GO
