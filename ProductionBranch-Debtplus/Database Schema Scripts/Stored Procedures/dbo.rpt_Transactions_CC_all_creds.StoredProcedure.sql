SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER procedure [rpt_Transactions_CC_all_creds] ( @client as int, @FromDate as datetime, @ToDate as datetime ) AS

-- =======================================================================================================================
-- ==            List the client/creditor transactions in the date range indicated                                      ==
-- =======================================================================================================================

-- Suppress intermediate results
set nocount on

-- Remove the time values from the date ranges
if @ToDate is null
	SELECT @ToDate = getdate()

if @FromDate IS NULL
	SELECT @FromDate = @ToDate

SELECT	@FromDate	= convert(datetime, convert(varchar(10), @FromDate, 101) + ' 00:00:00'),
	@ToDate		= convert(datetime, convert(varchar(10), @ToDate, 101)   + ' 23:59:59')

-- Retrieve the information from the view
select	item_date,
	tran_type,
	creditor_name,
	client_creditor,
	credit_amt,
	debit_amt,
	checknum,
	item_reconciled

FROM	view_transactions_cc

where	client	= @client
and	item_date BETWEEN @FromDate AND @ToDate

order by item_date

return ( @@rowcount )
GO
