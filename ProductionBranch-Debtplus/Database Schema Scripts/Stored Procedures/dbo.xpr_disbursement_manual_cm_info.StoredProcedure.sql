USE [DebtPlus]
GO
/****** Object:  StoredProcedure [dbo].[xpr_disbursement_manual_cm_info]    Script Date: 09/15/2014 13:13:35 ******/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[xpr_disbursement_manual_cm_info] ( @client as typ_client ) AS

-- ======================================================================================================================
-- ==            Retrieve the information for the debts of a client                                                    ==
-- ======================================================================================================================

-- ChangeLog
--   9/31/2002
--     Added support for client_creditor_balances table
--   01/15/2002
--     Added creditor_classes table references and dropped "creditors.fee_creditor"
--   3/14/2003
--     Allow for more than 10,000 creditors in a class
--   06/09/2009
--      Remove references to debt_number and use client_creditor

-- Fetch the information from the system
SELECT		cc.client_creditor							as 'client_creditor',
		cc.creditor								as 'creditor',
		cr.creditor_name							as 'creditor_name',

		case isnull(ccl.zero_balance,0)
			when 0 then isnull(bal.orig_balance,0)+isnull(bal.orig_balance_adjustment,0)+isnull(bal.total_interest,0)-isnull(bal.total_payments,0)
			else cc.disbursement_factor
		end									as 'balance',

		cc.disbursement_factor							as 'disbursement_factor'

FROM		client_creditor cc
inner join	client_creditor_balances bal with (nolock) on cc.client_creditor_balance = bal.client_creditor_balance
INNER JOIN	creditors cr ON cc.creditor=cr.creditor
LEFT OUTER JOIN	creditor_classes ccl WITH (NOLOCK) ON cr.creditor_class = ccl.creditor_class

WHERE		cc.client=@client
AND		cc.reassigned_debt = 0

ORDER BY	cr.type, cr.creditor_id, cc.creditor,cc.client_creditor

RETURN ( @@rowcount )
GO
