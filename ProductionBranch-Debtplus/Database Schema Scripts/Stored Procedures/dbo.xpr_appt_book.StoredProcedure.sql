USE [DebtPlus]
GO
/****** Object:  StoredProcedure [dbo].[xpr_appt_book]    Script Date: 09/15/2014 13:13:34 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[xpr_appt_book] ( @client AS INT, @appt_time AS INT, @priority AS INT = 0, @appt_type AS INT = NULL, @old_appointment AS INT = NULL, @counselor AS INT = NULL, @bankruptcy_class as int = -1, @partner as varchar(50) = null ) AS
-- ==========================================================================================
-- ==                  Book a client appointment into the system                           ==
-- ==========================================================================================

-- ChangeLog
--   9/6/2002
--     Ensure that the counselors exist in the counselors table to be counted in the "available slots" of an appointment.
--   10/9/2002
--     Check the counts properly when a counselor is specified for a book request
--   10/10/2002
--     Always ensure that an appointment is booked against a counselor. Find a counselor if one was not specified.
--   12/18/2002
--     Added support for "inactive" flag in appt_counselors
--    4/3/2003
--     Use RAND() to choose a random counselor from the list of possible ginnie pigs.
--   11/15/2005
--     Added bankruptcy qualification of counselors
--   12/31/2008
--     changed counselor_languages to counselor_attributes table
--     added support for AppointmentAttributes table

-- Do not generate intermediate result sets
SET NOCOUNT ON

-- Determine if the counselor is to be "forced" when it is not specified
declare	@use_counselor	int
select	@use_counselor = 1

-- Items from the appointment record
DECLARE	@office		int
DECLARE	@start_time	datetime
DECLARE @end_time	datetime
DECLARE	@duration	int
DECLARE	@Appt		int
DECLARE	@housing	int
DECLARE	@bankruptcy	int

-- Start a transaction to ensure the booking is performed properly
BEGIN TRANSACTION

-- Fetch the appointment time, office, and counselor information
SELECT	@office		= office,
	@start_time	= start_time
FROM	appt_times
WHERE	appt_time	= @appt_time

IF @@rowcount < 1
BEGIN
	ROLLBACK TRANSACTION
	RaisError (50043, 16, 1)
	Return ( 0 )
END

-- Find the language requirement for the client
declare	@language	int
select	@language	= isnull(language,1)
from	clients
where	client		= @client

-- If the client is rescheduling the appointment then release the old appointment slot.
-- This will be "rolled back" if we fail. However, we want to release the slot so that we can test
-- for an empty condition next.

if @old_appointment is not null
	UPDATE	client_appointments
	SET	status			= 'R',
		appt_time		= NULL,
		date_updated		= getdate()
	WHERE	client_appointment	= @old_appointment

-- If there is no counselor then find one
if @use_counselor = 1
begin
	if @counselor IS NULL
	begin
	
        -- Build a list of the requirements for a counselor
        create table #req ( seq_no int IDENTITY(0,1), Attribute int)

        -- A counselor role is always required
        insert into #req(Attribute)
        select	oID
        from	AttributeTypes
        where	[Grouping] = 'ROLE'
        and		[Attribute] = 'COUNSELOR';
            
        -- Load the language as a requirement for the counselor
        insert into #req ( Attribute )
        select	oID
        from	AttributeTypes
        where	[grouping]  = 'LANGUAGE'
        and		[oID]		= @language;

        -- Load the standard list of appointment requirements
        if @appt_type is not null
        begin
            insert into #req ( Attribute )
			select	Attribute
			From	AppointmentTypeAttributes WITH (NOLOCK)
			Where	AppointmentType	= @appt_type
		end

        -- Build a list of possible counselors who have attributes for this type list
        CREATE TABLE #counselors ( seq_no int IDENTITY(0,1), counselor int, EnabledFlag int );

        -- Load the list of standard counselors which are Active
        insert into #counselors (counselor, EnabledFlag)
        select  co.counselor, 1
        from    counselors co
        inner join appt_counselors x on co.counselor = x.counselor and @appt_time = x.appt_time
        where   co.ActiveFlag = 1
        and     x.Inactive = 0;

        update  #counselors
        set     EnabledFlag = 0
        from    #counselors m
        cross join #req r
        left outer join counselor_attributes a on m.counselor = a.counselor and r.Attribute = a.attribute
        where   a.[oID] is null;
        
        -- Disable counselors who are booked for this time
        update  #counselors
        set     EnabledFlag = 0
        from    #counselors x
	    inner join client_appointments ca with (nolock) on x.counselor = ca.counselor
	    where	@appt_time = ca.appt_time
	    AND		'P' = ca.status;

	    -- Find the counselor from the list
        declare	@item_count	int
        declare @seq_no     int
	    select	@item_count = count(*)
	    from	#counselors  with (nolock)
	    where	EnabledFlag = 1;

	    -- Choose a "good" seed for the random number generator
	    declare	@desired_ginnie_pig	int
	    select	@desired_ginnie_pig =	convert(int, convert(float, @item_count) * RAND())

	    -- Find the counselor from the list of possible counselors.
	    select	@seq_no	= min(seq_no)
	    from	#counselors with (nolock)
	    where	seq_no >= @desired_ginnie_pig
	    AND     EnabledFlag = 1
    
	    if @seq_no is null
	        select  @seq_no = MIN(seq_no)
	        from    #counselors
	        where   EnabledFlag = 1;
	        
	    if @seq_no is not null
	        select  @counselor = counselor
	        from    #counselors
	        where   seq_no  = @seq_no;
	        
        drop table #counselors;
        drop table #req

        -- There should be a counselor for this time period or we can't book the appointment
	    if @counselor is null
	    begin
	        rollback transaction
	        RaisError ('There are no free counselors who meet the requirements', 16, 1)
	        return ( 0 )
	    end
		
	end else begin

		-- The counselor must be valid
		if not exists (	select	*
				from	counselor_attributes ca
				inner join AttributeTypes t on ca.Attribute = t.oID
				where   ca.counselor    = @counselor
				and     t.[attribute]   = 'COUNSELOR'
				and		t.[grouping]    = 'ROLE' )
		begin
			ROLLBACK TRANSACTION
			RaisError (50044, 16, 1)
			RETURN ( 0 )
		end

		-- The counselor listed in the request must exist in the list of counselors for the appointment times
		IF NOT exists (select	*
				from	appt_counselors
				where	appt_time	= @appt_time
				AND	inactive	= 0
				and	counselor	= @counselor )
		begin
			ROLLBACK TRANSACTION
			RaisError (50044, 16, 1)
			RETURN ( 0 )
		end

		-- The counselor must not have been previously booked to another client
		IF exists (	SELECT	*
				FROM	client_appointments
				WHERE	appt_time	= @appt_time
				AND	counselor	= @counselor
				AND	status IN ('P', 'K'))
		BEGIN
			ROLLBACK TRANSACTION
			RaisError (50044, 16, 1)
			RETURN ( 0 )
		END
	END
END

-- Find the normal appointment duration from the type of the appointment
select	@duration	= appt_duration
from	appt_types with (nolock)
where	appt_type	= @appt_type

-- The ending time is the duration plus the starting time	
select	@end_time	= DATEADD(minute, isnull(@duration,0), @start_time)

-- Insert (book) the appointment time for this client
INSERT INTO client_appointments ([client], [appt_time], [counselor], [status], [priority], [previous_appointment], [appt_type], [office], [start_time], [end_time], [created_by], [confirmation_status], [date_confirmed], [date_updated], [workshop], [bankruptcy_class], [partner])
VALUES (@client, @appt_time, @counselor, 'P', @priority, @old_appointment, @appt_type, @office, @start_time, @end_time, suser_sname(), 0, null, getdate(), null, @bankruptcy_class, @partner)

-- Fetch the appointment identifier
SELECT @Appt = SCOPE_IDENTITY()

-- Ensure that the time slot is not overbooked
DECLARE	@booked_count		int
DECLARE	@available_count	int

-- This needs to be done only when the counselor is not specified because if the counselor is specified,
-- the one and only one time slot for this counselor is checked earlier in this procedure.
if (@counselor is null) or (@use_counselor = 0)
begin

	-- Fetch the count of booked client appointments (including this one.)
	SELECT	@booked_count	= count(*)
	FROM	client_appointments WITH (NOLOCK)
	WHERE	appt_time	= @appt_time
	AND	status		= 'P'

	-- Determine the number of available slots (without regard to counselors) for this time period/office
	SELECT	@available_count	= count(*)
	FROM	appt_counselors
	WHERE	appt_time		= @appt_time
	AND	inactive		= 0
	AND	counselor in (
	    SELECT a.counselor
	    FROM   counselor_attributes a
	    inner join counselors co on a.counselor = co.counselor
		inner join attributetypes t on a.attribute = t.oid
	    WHERE t.attribute = 'COUNSELOR' and t.[grouping] = 'ROLE'
	)

	-- Ensure that the booked count is less thant the number of counselors
	IF isnull(@booked_count,0) > isnull(@available_count,0)

	BEGIN
		RollBack Transaction
		RaisError (50044, 16, 1)
		Return ( 0 )
	END
END

-- Update the client with the first appointment
-- At the same time move "CRE" clients to "APT" status
UPDATE	clients
SET	first_appt		= @Appt,
	active_status		= case when active_status = 'CRE' then 'APT' else active_status end
WHERE	client			= @client
AND	first_appt IS NULL

-- Update the client bankruptcy status if required
if @bankruptcy_class > 0
	update	clients
	set	bankruptcy_class	= @bankruptcy_class
	where	client			= @client

IF @old_appointment IS NOT NULL
BEGIN
	-- Update the client with the rescheduled appointment
	UPDATE	clients
	SET	first_resched_appt	= @Appt
	WHERE	client			= @client
	AND	first_appt		= @old_appointment
	AND	first_resched_appt IS NULL
END

-- Commit the changes to the database at this point.
COMMIT TRANSACTION

-- Tell the user that the operation was successful by returning the next key
RETURN ( @Appt )
GO
