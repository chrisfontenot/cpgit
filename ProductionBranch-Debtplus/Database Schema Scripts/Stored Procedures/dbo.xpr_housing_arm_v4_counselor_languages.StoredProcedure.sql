SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[xpr_housing_arm_v4_counselor_languages] as
set nocount on

-- Return the list of languages for the counselors
select	a.cms_counselor_id as cms_counselor_id, c.oID as InputLanguage, dbo.map_hud_9902_language(c.oID) as language
from	##hud_9902_counselors a
inner join counselor_attributes b on a.cms_counselor_id = b.counselor
inner join attributetypes c on b.attribute = c.oID
where	c.grouping = 'LANGUAGE'

return @@rowcount

GO
