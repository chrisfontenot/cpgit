SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER procedure [rpt_CheckVoucher_Check] ( @CheckNum AS BigInt, @creditor as varchar(10) = null ) AS
-- =============================================================================================
-- ==            Obtain the transaction detail for the indicated check                        ==
-- =============================================================================================

-- ChangeLog
--   9/8/2002
--     Added the "@creditor" parameter to permit check vouchers for bank-wire transactions.

-- Turn off intermediate result sets
SET NOCOUNT ON

-- Translate the check number to a valid trust regster
DECLARE	@TrustRegister	INT

-- If the creditor is '' then make it NULL.
if @creditor is not null
begin
	select	@creditor = ltrim(rtrim(@creditor))
	if @creditor = ''
		select	@creditor = null
end

-- If there is a passed creditor then accept bank wire transactions
if @creditor is not null
	SELECT TOP 1	@TrustRegister	= trust_register
	from		registers_trust with (nolock)
	where		checknum	= @checknum
	AND		tran_type in ('BW', 'AD', 'CM', 'MD', 'CR', 'AR')
	and		isnull(cleared, ' ') != 'D'
	order by	date_created desc

else

	SELECT TOP 1	@TrustRegister	= trust_register
	FROM		registers_trust WITH (NOLOCK)
	WHERE		checknum	= @checknum
	AND		tran_type in ('AD', 'CM', 'MD', 'CR', 'AR')
	AND		isnull(cleared,' ') != 'D'
	ORDER BY	date_created desc	-- Accomodate duplicate check numbers by taking the most recient

DECLARE	@Rows		INT
EXECUTE @Rows = rpt_CheckVoucher_Sequence @TrustRegister, @creditor
RETURN ( @rows )
GO
