USE [DebtPlus]
GO
/****** Object:  StoredProcedure [dbo].[xpr_deposit_batch_entry_add]    Script Date: 09/15/2014 13:13:35 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[xpr_deposit_batch_entry_add] ( @batch as INT, @client as INT, @amount as Money, @type as VarChar(2) = 'OT', @item_date AS SmallDatetime = NULL, @Reference AS VarChar(50) = NULL, @ok_to_post as int = 1 ) AS
-- =====================================================================================================
-- ==                  Create a entry into the system for the deposit                                 ==
-- =====================================================================================================

-- ChangeLog
--   5/13/2011
--     Added call to function 'DepositsInTrust' to recalculate the pending trust amount

-- Prevent intermediate result sets
SET NOCOUNT ON

declare	@personal_checks	bit
declare	@test_client		typ_client
declare	@message		varchar(50)

-- Default the item date to today if one was not specified
IF @item_date IS NULL
	SET @item_date = getdate()

-- Clean up the reference information
if @reference is not null
begin
	set @reference = ltrim(rtrim(@reference))
	if @reference = ''
		set @reference = null
end

-- If the item is valid to be posted then examine to ensure that the client is defined
if @ok_to_post > 0
begin
	select	@personal_checks	= isnull(personal_checks,0),
		@test_client		= client
	from	clients
	where	client			= @client

	if @test_client is null
	begin
		select	@ok_to_post	= 0,
			@message	= 'Invalid Client ID'
	end
end

if @ok_to_post > 0
begin
	if (@personal_checks = 0) and (@type = 'PC')
	begin
		select	@message	= 'Personal Chks Invalid',
			@ok_to_post	= 0
	end
end

if @ok_to_post > 0
begin
	if @client <= 0
	begin
		select	@ok_to_post	= 0,
			@message	= 'Invalid Client ID'
	end
end

-- Add the item to the pending queue
INSERT INTO deposit_batch_details	(deposit_batch_id,	client,		tran_subtype,	amount,		item_date,	reference,	created_by,	date_created,	date_posted,	ok_to_post,	message)
VALUES					(@batch,		@client,	@type,		@amount,	@item_date,	@reference,	suser_sname(),	getdate(),	null,		@ok_to_post,	@message)

-- Correct the deposit amount in the pending tables
update clients set deposit_in_trust = dbo.DepositsInTrust(client) where client = @client

-- Return the new record location
RETURN ( SCOPE_IDENTITY() )
GO
