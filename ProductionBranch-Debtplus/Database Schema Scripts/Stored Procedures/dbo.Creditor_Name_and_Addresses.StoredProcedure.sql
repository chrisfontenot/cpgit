USE [DebtPlus]
GO
/****** Object:  StoredProcedure [dbo].[Creditor_Name_and_Addresses]    Script Date: 09/15/2014 13:13:32 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[Creditor_Name_and_Addresses] AS
-- ======================================================================================================
-- ==                    Fetch the list of creditor and their addresses for mail merge                 ==
-- ======================================================================================================

-- ChangeLog
--   2/04/2002
--     Removed test for having "active" debts on the creditor list
--   4/1/2002
--     Changed "creditor_type" to "creditor_type_eft"
--   7/11/2003
--     Display Fairshare percentage by EFT or Check
--   3/14/2003
--     Allow for more than 10,000 creditors in a class
--   08/20/2004
--     Removed fairshare_pct_* and creditor_type_* from the creditor. Added creditor_contribution_pct link.

-- Suppress the intermediate result sets
SET NOCOUNT ON

-- Create a temporary table to hold the creditor ID and type
CREATE TABLE #t_creditor_list_1 (
	creditor	varchar(10),
	type		varchar(1),
	CONSTRAINT [pk_Creditor_Name_and_Addresses2] PRIMARY KEY CLUSTERED 
	(
		[creditor]
	)
)

-- Default to the payment addresses for all creditors
INSERT INTO #t_creditor_list_1 (creditor,type)
SELECT	creditor, 'P' as 'type'
FROM	creditors;

/*
-- Remove this statement to leave the payment address
UPDATE	#t_creditor_list_1
SET	type = 'L'
WHERE	creditor IN (
	select	creditor
	FROM	creditor_addresses WITH (NOLOCK)
	WHERE	type = 'L'
);
*/

-- Retrieve the creditor addresses
SELECT		convert(varchar(10), a1.creditor)				as 'creditor',

		'ATTN: ' + a1.attn						as 'addr_1',
		a1.address1							as 'addr_2',
		a1.address2							as 'addr_3',
		a1.address3							as 'addr_4',
		a1.address4							as 'addr_5',
		dbo.format_city_state_zip (a1.city, a1.state, a1.postalcode)	as 'addr_6',

		convert(varchar(1), isnull(pct.creditor_type_check,'N'))	as 'creditor_type_check',
		convert(varchar(1), isnull(pct.creditor_type_eft,'N'))		as 'creditor_type_eft',
		convert(float, isnull(pct.fairshare_pct_check,0))		as 'fairshare_pct_check',
		convert(float, isnull(pct.fairshare_pct_eft,0))			as 'fairshare_pct_eft',

		convert(float, cr.lowest_apr_pct)				as 'apr_1',
		convert(float, cr.medium_apr_pct)				as 'apr_2',
		convert(float, cr.highest_apr_pct)				as 'apr_3',
		cr.sic								as 'sic'

FROM		#t_creditor_list_1 l	WITH (NOLOCK)
inner join	creditors cr		WITH (NOLOCK) ON l.creditor = cr.creditor
INNER JOIN	creditor_addresses a1	WITH (NOLOCK) ON l.creditor = a1.creditor AND l.type = a1.type
LEFT OUTER JOIN	creditor_contribution_pcts pct WITH (NOLOCK) ON cr.creditor_contribution_pct = pct.creditor_contribution_pct
ORDER BY a1.address1,cr.type,cr.creditor_id

-- Discard the working tables
DROP TABLE #t_creditor_list_1

RETURN ( @@rowcount )
GO
