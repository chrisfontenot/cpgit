SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER procedure [rpt_Disbursement_Check_Register] ( @DisbursementBatch AS INT = NULL ) AS
-- ============================================================================================
-- ==           If a disbursement batch was not specified, use the latest one                ==
-- ============================================================================================

-- Disable intermediate result sets
SET NOCOUNT ON

DECLARE	@Deduct_Creditor	varchar(10)
SELECT	@Deduct_Creditor = deduct_creditor
FROM	config

IF @DisbursementBatch IS NULL
BEGIN
	SELECT		@DisbursementBatch = max(disbursement_register)
	FROM		registers_disbursement with (nolock)
	WHERE		tran_type = 'AD'
	and		date_posted IS NOT NULL

	IF @DisbursementBatch IS NULL
	BEGIN
		RaisError (50033, 11, 1)
		RETURN ( 0 )
	END
END

-- ============================================================================================
-- ==           Fetch the check information for this batch                                   ==
-- ============================================================================================

SELECT	d.client								as 'client_id',
		d.creditor								as 'creditor',
		cr.creditor_name							as 'creditor_name',
		d.debit_amt								as 'gross',
		case when d.creditor_type = 'D' then d.fairshare_amt else 0 end		as 'deducted',
		case when d.creditor_type in ('N','D') then 0 else d.fairshare_amt end	as 'billed',
		d.account_number							as 'account_number',
		dbo.format_normal_name (pn.prefix, pn.first, pn.middle, pn.last, pn.suffix)	as 'client_name',
		t.checknum									as 'checknum',
		t.date_created								as 'check_date'

FROM		registers_client_creditor d
INNER JOIN	people p		WITH (NOLOCK) ON d.client = p.client AND 1 = p.relation
left outer join names pn with (nolock) on p.nameid = pn.name
INNER JOIN	registers_trust t	WITH (NOLOCK) ON d.trust_register = t.trust_register
INNER JOIN	creditors cr		WITH (NOLOCK) ON d.creditor = cr.creditor

WHERE		d.tran_type IN ('AD', 'MD', 'CM')
AND		d.disbursement_register = @DisbursementBatch
AND		d.creditor != @Deduct_Creditor
ORDER BY	t.trust_register, d.client

RETURN ( @@rowcount )
GO
