USE [DebtPlus]
GO
/****** Object:  StoredProcedure [dbo].[xpr_rpps_response_process_CIE]    Script Date: 09/15/2014 13:13:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[xpr_rpps_response_process_CIE] ( @rpps_response_file as int ) AS
-- ==========================================================================================
-- ==           Process the sequence needed to void a RPPS payment transaction             ==
-- ==========================================================================================

-- ChangeLog
--   12/3/2001
--     Added a deposit register for the RPS rejects to make a deposit transaction.
--   1/2/2002
--     Added update of denorm_current_month_disbursement to reflect the reject of the payment.
--   2/18/2002
--     Added client 0 transaction to record the change in the deduct balance.
--   7/10/2002
--     Added additional test for duplication of trace numbers
--   7/30/2002
--     Rewrote for version 1.5 of DebtPlus.
--   9/31/2002
--     Added support for client_creditor_balances table
--   4/16/2003
--     Updated creditors.returns_this_creditor
--   5/6/2003
--     Corected registes_trust to show the proper net amount of the deposit.
--   06/09/2009
--      Remove references to debt_number and use client_creditor

-- Start a transaction to block the entire operation as a unit
BEGIN TRANSACTION
SET XACT_ABORT ON
SET NOCOUNT ON

-- Validate the fact that the transaction is really a CIE item.
update	rpps_response_details
set	processing_error		= 'NOT PAYMENT'
from	rpps_response_details d
inner join rpps_transactions t on d.rpps_transaction = t.rpps_transaction
where	d.processing_error is null
and	d.rpps_response_file		= @rpps_response_file
and	d.service_class_or_purpose	= 'CIE'
and	t.service_class_or_purpose	<> 'CIE'
and	d.processing_error		is null;

-- Validate the account number
update	rpps_response_details
set	processing_error		= 'ACCT NO DIFFER'
from	rpps_response_details d
inner join rpps_transactions t on d.rpps_transaction = t.rpps_transaction
inner join registers_client_creditor rcc on t.client_creditor_register = rcc.client_creditor_register
where	d.processing_error is null
and	d.rpps_response_file		= @rpps_response_file
and	d.service_class_or_purpose	= 'CIE'
and	t.service_class_or_purpose	= 'CIE'
and	t.transaction_code		= 22
and	d.account_number		!= rcc.account_number
and	d.processing_error		is null;

-- Complain if the transaction was previously voided
update	rpps_response_details
set	processing_error		= 'PREVIOUSLY VOIDED'
from	rpps_response_details d
inner join rpps_transactions t on d.rpps_transaction = t.rpps_transaction
inner join registers_client_creditor rcc on t.client_creditor_register = rcc.client_creditor_register
where	d.processing_error is null
and	d.rpps_response_file		= @rpps_response_file
and	d.service_class_or_purpose	= 'CIE'
and	t.service_class_or_purpose	= 'CIE'
and	t.transaction_code		= 22
and	rcc.void			<> 0
and	d.processing_error		is null;

-- Ensure that the gross amounts match
update	rpps_response_details
set	processing_error		= 'GROSS AMT DIFFER'
from	rpps_response_details d
inner join rpps_transactions t on d.rpps_transaction = t.rpps_transaction
inner join registers_client_creditor rcc on t.client_creditor_register = rcc.client_creditor_register
where	d.processing_error is null
and	d.rpps_response_file		= @rpps_response_file
and	d.service_class_or_purpose	= 'CIE'
and	t.service_class_or_purpose	= 'CIE'
and	t.transaction_code		= 22
and	rcc.debit_amt			!= d.gross
and	d.processing_error		is null
and	rcc.void			= 0;

-- Validate the billed and no contribution creditors
update	rpps_response_details
set	processing_error		= 'NET AMT DIFFER'
from	rpps_response_details d
inner join rpps_transactions t on d.rpps_transaction = t.rpps_transaction
inner join registers_client_creditor rcc on t.client_creditor_register = rcc.client_creditor_register
where	d.processing_error is null
and	d.rpps_response_file		= @rpps_response_file
and	d.service_class_or_purpose	= 'CIE'
and	t.service_class_or_purpose	= 'CIE'
and	t.transaction_code		= 22
and	rcc.debit_amt			!= d.net
and	rcc.creditor_type		!= 'D'
and	d.processing_error		is null
and	rcc.void			= 0;

-- Validate the deduction creditors
update	rpps_response_details
set	processing_error		= 'NET AMT DIFFER'
from	rpps_response_details d
inner join rpps_transactions t on d.rpps_transaction = t.rpps_transaction
inner join registers_client_creditor rcc on t.client_creditor_register = rcc.client_creditor_register
where	d.processing_error is null
and	d.rpps_response_file		= @rpps_response_file
and	d.service_class_or_purpose	= 'CIE'
and	t.service_class_or_purpose	= 'CIE'
and	t.transaction_code		= 22
and	d.net				!= rcc.debit_amt - rcc.fairshare_amt
and	rcc.creditor_type		= 'D'
and	d.processing_error		is null
and	rcc.void			= 0;

-- Do not accept transactions which do not have clients, creditors, or debts
update	rpps_response_details
set	processing_error		= 'INVALID CLIENT'
from	rpps_response_details d
inner join rpps_transactions t on d.rpps_transaction = t.rpps_transaction
inner join registers_client_creditor rcc on t.client_creditor_register = rcc.client_creditor_register
left outer join clients c on rcc.client = c.client
where	d.processing_error is null
and	d.rpps_response_file		= @rpps_response_file
and	d.service_class_or_purpose	= 'CIE'
and	t.service_class_or_purpose	= 'CIE'
and	t.transaction_code		= 22
and	c.client			is null
and	d.processing_error		is null;

update	rpps_response_details
set	processing_error		= 'INVALID CREDITOR'
from	rpps_response_details d
inner join rpps_transactions t on d.rpps_transaction = t.rpps_transaction
inner join registers_client_creditor rcc on t.client_creditor_register = rcc.client_creditor_register
left outer join creditors cr on rcc.creditor = cr.creditor
where	d.processing_error is null
and	d.rpps_response_file		= @rpps_response_file
and	d.service_class_or_purpose	= 'CIE'
and	t.service_class_or_purpose	= 'CIE'
and	t.transaction_code		= 22
and	cr.creditor			is null
and	d.processing_error		is null;

update	rpps_response_details
set	processing_error		= 'INVALID DEBT'
from	rpps_response_details d
inner join rpps_transactions t on d.rpps_transaction = t.rpps_transaction
inner join registers_client_creditor rcc on t.client_creditor_register = rcc.client_creditor_register
left outer join client_creditor cc on rcc.client_creditor = cc.client_creditor
where	d.processing_error is null
and	d.rpps_response_file		= @rpps_response_file
and	d.service_class_or_purpose	= 'CIE'
and	t.service_class_or_purpose	= 'CIE'
and	t.transaction_code		= 22
and	cc.client_creditor		is null
and	d.processing_error		is null;

-- Build the list of the transactions for the refund
select	d.rpps_response_detail		as rpps_response_detail,
	t.rpps_transaction		as rpps_transaction,
	d.return_code			as return_code,
	t.client_creditor_register	as client_creditor_register,
	rcc.client			as client,
	rcc.creditor			as creditor,
	rcc.client_creditor			as client_creditor,
	rcc.debit_amt			as debit_amt,
	rcc.fairshare_amt		as fairshare_amt,
	rcc.fairshare_pct		as fairshare_pct,
	rcc.invoice_register		as invoice_register,
	rcc.creditor_type		as creditor_type,
	rcc.trust_register		as trust_register,
	rcc.disbursement_register	as disbursement_register

into	#rpps_response_cie

from	rpps_response_details d
inner join rpps_transactions t on d.rpps_transaction = t.rpps_transaction
inner join registers_client_creditor rcc on t.client_creditor_register = rcc.client_creditor_register
where	d.processing_error is null
and	d.rpps_response_file		= @rpps_response_file
and	d.service_class_or_purpose	= 'CIE'
and	t.service_class_or_purpose	= 'CIE'
and	t.transaction_code		= 22
and	d.processing_error		is null
and	rcc.void			= 0;

-- If there are no transactions then there is nothing else to be done. Quit with success.
if not exists (select * from #rpps_response_cie)
begin
	drop table #rpps_response_cie
	commit transaction
	return ( 1 )
end

-- Allocate a trust register for the deposits

declare	@deposit_trust_register	int
insert into registers_trust (tran_type, amount, cleared) values ('RR', 0, ' ')
select	@deposit_trust_register = SCOPE_IDENTITY()

-- Mark the transaction as failed
UPDATE	rpps_transactions
SET	return_code		= isnull(x.return_code,'000')
FROM	rpps_transactions t
INNER JOIN #rpps_response_cie x on t.rpps_transaction = x.rpps_transaction

-- Mark the transaction as voided
UPDATE	registers_client_creditor
SET	void			 = 1
FROM	registers_client_creditor rcc
inner join #rpps_response_cie x on rcc.client_creditor_register = x.client_creditor_register

-- Calculate the various component pieces for the creditors
select	creditor, sum(debit_amt) as gross, sum(case creditor_type when 'D' then fairshare_amt else 0 end) as deducted, convert(money,0) as net, sum(case when creditor_type in ('N','D') then 0 else fairshare_amt end) as billed
into	#rpps_response_cie_creditor_1
from	#rpps_response_cie
group by creditor;

update	#rpps_response_cie_creditor_1
set	net = gross - deducted;

-- Calcluate the invoiced amounts by invoice that must be adjusted
select	creditor, invoice_register, sum(fairshare_amt) as fairshare_amt
into	#rpps_response_cie_creditor_3
from	#rpps_response_cie
where	creditor_type not in ('N', 'D')
and	invoice_register is not null
group by creditor, invoice_register;

-- If there are invoiced amounts then adjust the invoices
if exists (select * from #rpps_response_cie_creditor_3)
begin
	-- Calcluate the invoiced amounts by creditor that must be adjusted
	select	creditor, sum(fairshare_amt) as fairshare_amt
	into	#rpps_response_cie_creditor_2
	from	#rpps_response_cie_creditor_3
	group by creditor;

	-- Adjust the invoices
	UPDATE	registers_invoices
	SET	adj_amount = isnull(i.adj_amount,0) + x.fairshare_amt
	FROM	registers_invoices i
	inner join #rpps_response_cie_creditor_3 x on i.invoice_register = x.invoice_register;

	-- Record the event that we are adjusting the invoice
	INSERT INTO registers_creditor	(tran_type, creditor, invoice_register, credit_amt, trust_register)
	SELECT	'RA'			as tran_type,
		creditor		as creditor,
		invoice_register	as invoice_register,
		fairshare_amt		as credit_amt,
		@deposit_trust_register as trust_register
	FROM	#rpps_response_cie_creditor_3;

	-- Update the MTD billed to indicate that the value is no longer billed
	update	creditors
	SET	contrib_mtd_billed = cr.contrib_mtd_billed - x.fairshare_amt
	from	creditors cr
	inner join #rpps_response_cie_creditor_2 x on cr.creditor = x.creditor;

	-- Discard the working table that we just created
	drop table #rpps_response_cie_creditor_2;
end

-- Remove the amount of the fairshare from the fairshare check for a refund
declare	@fairshare_total	money
select	@fairshare_total = sum(deducted)
from	#rpps_response_cie_creditor_1;

if @fairshare_total is null
	select @fairshare_total = 0

if @fairshare_total > 0
begin
	update	clients
	set	held_in_trust = isnull(held_in_trust,0) - @fairshare_total
	where	client = 0

	insert into registers_client (tran_type, client, debit_amt, trust_register, message)
	values ('RR', 0, @fairshare_total, @deposit_trust_register, 'RPPS Deduction')

	update	creditors
	set	contrib_mtd_received = isnull(cr.contrib_mtd_received,0) - x.deducted
	from	creditors cr
	inner join #rpps_response_cie_creditor_1 x on cr.creditor = x.creditor
end

-- Include the net amount of the refund into the creditor transactions
insert into registers_creditor	(tran_type,	creditor,	credit_amt,	invoice_register,	trust_register,	disbursement_register)
select	'RR', creditor, sum(case creditor_type when 'D' then debit_amt - fairshare_amt else debit_amt end), invoice_register, @deposit_trust_register, disbursement_register
from	#rpps_response_cie
group by creditor, invoice_register, disbursement_register;

-- Include the deposit transactions
insert into registers_client (tran_type, client, credit_amt, trust_register, disbursement_register)
select	'RR', client, sum(debit_amt), @deposit_trust_register, disbursement_register
from	#rpps_response_cie
group by client, disbursement_register;

select	client, sum(debit_amt) as debit_amt
into	#rpps_response_cie_client_1
from	#rpps_response_cie
group by client;

update	clients
set	held_in_trust = isnull(held_in_trust,0) + x.debit_amt
from	clients c
inner join #rpps_response_cie_client_1 x on c.client = x.client;

-- Adjust the creditor disbursement totals
update	creditors
set	distrib_mtd = isnull(distrib_mtd,0) - x.gross
from	creditors cr
inner join #rpps_response_cie_creditor_1 x on cr.creditor = x.creditor;

-- Include the transactions for the debt information
insert into registers_client_creditor (tran_type, client, creditor, client_creditor, credit_amt, creditor_type, fairshare_amt, fairshare_pct, disbursement_register, trust_register, invoice_register)
select 'RR', client, creditor, client_creditor, debit_amt, creditor_type, fairshare_amt, fairshare_pct, disbursement_register, @deposit_trust_register, invoice_register
from #rpps_response_cie;

-- Summarize the transactions by debt
select	client_creditor, sum(debit_amt) as debit_amt
into	#rpps_response_cie_client_2
from	#rpps_response_cie
group by client_creditor;

-- Update the statistics for the debt record
update	client_creditor
set	sched_payment  = isnull(sched_payment,0)  + debit_amt,
	last_payment = null,
	returns_this_creditor = isnull(returns_this_creditor,0) + debit_amt
from	client_creditor cc
inner join #rpps_response_cie_client_2 x on cc.client_creditor = x.client_creditor;

update	client_creditor_balances
set	total_payments = isnull(total_payments,0) - debit_amt,
	payments_month_0 = isnull(payments_month_0,0) - debit_amt
from	client_creditor_balances bal
inner join client_creditor cc on bal.client_creditor_balance = cc.client_creditor_balance
inner join #rpps_response_cie_client_2 x on cc.client_creditor = x.client_creditor;

-- Update the amount of the "deposit" in the trust register
declare	@trust_amount	money
select	@trust_amount	= sum(case creditor_type when 'D' then debit_amt - fairshare_amt else debit_amt end)
from	#rpps_response_cie

update	registers_trust
set	amount		= @trust_amount
where	trust_register	= @deposit_trust_register

-- Determine if a disbursement note is desired. If so, create it
declare	@rpps_resend_msg	bit
select	@rpps_resend_msg	= rpps_resend_msg
from	config with (nolock)

if isnull(@rpps_resend_msg,1) <> 0
	insert into disbursement_notes (client, type, is_text, subject, note)
	select	client, 5, 1, 'RPPS Reject Processed',
		'The scheduled payment for creditor ''' + cc.creditor + ''' account # ''' + cc.account_number + ''' was increased by $' + convert(varchar, debit_amt, 1) + ' due to an RPPS reject.' + char(13) + char(10) + char(13) + char(10) +
		'NOTE: This is an automated message entered in response to the system processing the rejection. Please do not pay the amount until the reason for the rejection has been addressed or it will be similarily rejected in the future.' + char(13) + char(10) +
		'(for example, if the account number was invalid due to a checksum error then the account number must be corrected before it may be used.)'
	from	#rpps_response_cie_client_2 x
    inner join client_creditor cc on x.client_creditor = cc.client_creditor;

-- Discard the working tables
drop table #rpps_response_cie;
drop table #rpps_response_cie_creditor_1;
drop table #rpps_response_cie_creditor_3;
drop table #rpps_response_cie_client_1;
drop table #rpps_response_cie_client_2;

-- Commit the transaction
COMMIT TRANSACTION

-- Return success
RETURN ( 1 )
GO
