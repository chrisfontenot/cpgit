SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER procedure [rpt_close_to_payoff_debts] ( @counselor int = null ) as
-- =============================================================================================
-- ==           Report information for the "close to disbursement" report                     ==
-- =============================================================================================

-- ChangeLog
--   5/14/2003
--     Copied from rpt_close_to_payoff
--   7/28/2003
--     Added held_in_trust to the result set
--   2/03/2004
--     Modified to ask for counselor
--   3/14/2003
--     Allow for more than 10,000 creditors in a class
--   5/5/2010
--     Revised to use temporary tables for speed

-- Suppress intermediate results
set nocount on

-- Period of months before the payoff is triggered
declare	@payoff_period	float
select	@payoff_period	= 3.0

-- Build an inital list of debts in the system that might match
select	cc.client_creditor, cc.client_creditor_balance, cc.account_number, cc.client, convert(varchar(10),null) as creditor_type, convert(int,null) as creditor_id, cc.creditor, cc.creditor_name, convert(int,1) as debt_number, cc.disbursement_factor, cc.client_name, cc.balance_verify_date, cc.last_payment,
		bal.orig_balance, bal.orig_balance_adjustment, bal.total_payments, bal.total_interest,
		convert(money,0) as last_payment_amt, convert(datetime,null) as last_payment_date, convert(varchar(50),'RPPS') as checknum,
		c.held_in_trust as held_in_trust, c.counselor,
		convert(int,0) as creditor_class,
		convert(int,0) as zero_balance, convert(int,0) as agency_account,
		convert(int,0) as trust_register
into	#payoff_details
from	clients c with (nolock)
inner join client_creditor cc with (nolock) on c.client = cc.client
inner join client_creditor_balances bal with (nolock) on cc.client_creditor_balance = bal.client_creditor_balance and bal.client_creditor = cc.client_creditor
where	c.active_status in ('A','AR')
and		c.client > 0
and		bal.total_payments > 0;

-- A few indexes help speed things along
create index ix1_temp_payoff_details on #payoff_details ( client )
create index ix2_temp_payoff_details on #payoff_details ( creditor )
create index ix3_temp_payoff_details on #payoff_details ( last_payment )

-- Toss the items for a different counselor if required
if @counselor is not null
	delete
	from	#payoff_details
	where	counselor <> @counselor;

-- Look at the creditors table for additional information
update	#payoff_details
set		creditor_class		= cr.creditor_class,
		creditor_name		= cr.creditor_name,
		creditor_type		= cr.[type],
		creditor_id			= cr.creditor_id
from	#payoff_details d
inner join creditors cr with (nolock) on d.creditor = cr.creditor;

-- Look at the creditor_classes table for additional information
update	#payoff_details
set		zero_balance		= ccl.zero_balance,
		agency_account		= ccl.agency_account
from	#payoff_details d
inner join creditor_classes ccl with (nolock) on d.creditor_class = ccl.creditor_class

-- Discard the items that have no balance but are designed to have a balance
delete
from	#payoff_details
where	(orig_balance + orig_balance_adjustment + total_interest <= total_payments and zero_balance = 0)
or		(agency_account <> 0);

-- Toss the items that have more than the number of months for payoff
delete
from	#payoff_details
where	(orig_balance + orig_balance_adjustment + total_interest - total_payments) > (disbursement_factor * @payoff_period)

-- Look at the registers_client_creditor table for additional information
update	#payoff_details
set		last_payment_amt	= rcc.debit_amt,
		last_payment_date	= rcc.date_created,
		trust_register		= rcc.trust_register
from	#payoff_details d
inner join registers_client_creditor rcc with (nolock) on d.last_payment = rcc.client_creditor_register

-- Discard items that have not been paid recently
declare	@earliest_date		datetime
select	@earliest_date		= convert(varchar(10), dateadd(d, -45, getdate()), 101)

delete
from	#payoff_details
where	last_payment_date	is null
or		last_payment_date	< @earliest_date;

-- Correct the check number where applicable
update	#payoff_details
set		checknum		= tr.checknum
from	#payoff_details d
inner join registers_trust tr with (nolock) on d.trust_register = tr.trust_register
where	tr.tran_type <> 'BW';

-- Correct the client name
update	#payoff_details
set		client_name		= dbo.format_normal_name(pn.prefix,pn.first,pn.middle,pn.last,pn.suffix)
from	#payoff_details d
inner join people p with (nolock) on d.client = p.client and 1 = p.relation
inner join names pn with (nolock) on p.nameid = pn.name
where	d.client_name is null;

-- Return the results
select	client, creditor, creditor_name, debt_number, disbursement_factor, account_number, client_name,
		orig_balance, orig_balance + orig_balance_adjustment + total_interest - total_payments as current_balance,
		last_payment_date, last_payment_amt,
		checknum,
		held_in_trust, balance_verify_date
from	#payoff_details
order by creditor_type, creditor_id, creditor, account_number, client

drop table #payoff_details
return ( @@rowcount )
GO
