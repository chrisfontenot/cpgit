USE [DebtPlus]
GO
/****** Object:  StoredProcedure [dbo].[WEEKLY_client_deposits]    Script Date: 09/15/2014 13:13:34 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[WEEKLY_client_deposits] AS

-- ===========================================================================================================
-- ==            Do the processing to adjust the client_deposits table                                      ==
-- ===========================================================================================================

-- ChangeLog
--   5/6/2003
--     Use the expiration type from the retention tables. The dates and amounts default if the type is not "forever".
--   7/8/2003
--     Extracted from MONTHLY_client_deposits. This procedure replaced the monthly item.
--   7/22/2003
--     Defined the period a bit more closely to catch late deposits. We allow a window forward and back based upon the
--     expected deposit date rather than the rolling period.
--   2/2/2004
--     Special for Richmond. Ignore the retention event processing.

-- This is the window for client deposits. It is represented in days.
declare		@days_back	float
declare		@days_forward	float
select		@days_back = 7,
		@days_forward = 7		-- should be 14 day period.

-- These are somewhat "hard-coded" events from the retention_events table
declare		@missed_initial_payment		int
declare		@missed_subsequent_payment	int
select		@missed_initial_payment		= 1,
		@missed_subsequent_payment	= 2

-- Suppress intermediate results
set nocount on

-- Find the period range for the items
declare		@roll_start	datetime
declare		@roll_end	datetime
declare		@now		datetime
select		@now        = convert(datetime, convert(varchar(10), getdate(), 101) + ' 00:00:00')

-- From the 20th, start on the 6th and end on the 12th
select		@roll_end   = dateadd(d, -8,    @now)
select		@roll_start = dateadd(d, -6,    @roll_end)
select		@roll_end   = dateadd(s, 86399, @roll_end)	-- 23:59:59

-- A missing deposit date is shown as this figure. Leave it alone please.
declare		@missing_date	datetime
select		@missing_date	= convert(datetime, '1/1/1960 00:00:00')

-- Log the event information
DECLARE @roll_end_date		varchar(10)
DECLARE	@roll_start_date	varchar(10)
DECLARE	@roll_message		varchar(800)

SELECT	@roll_end_date		= convert(varchar(10), @roll_end, 101),
	@roll_start_date	= convert(varchar(10), @roll_start, 101)

SELECT	@roll_message	= 'Rolling deposit dates between ' + @roll_start_date + ' and ' + @roll_end_date + ' to the next month.'

execute master..xp_logevent 60000, @roll_message, informational

-- Start a transaction to keep things "honest"
begin transaction
set xact_abort on

-- Correct any problems with the last deposit date for clients since we will be using that here.
select	client, convert(datetime, convert(varchar(10), date_created, 101) + ' 00:00:00') as date_created, credit_amt
into	#raw_deposits_1
from	registers_client with (nolock)

-- Normally, it would only be "DP". However, since bad deposits are client-client transfers, then include it as well.
where	tran_type in ('DP','CD');

-- Merge the items together to form the values by date
select	client, date_created, sum(credit_amt) as credit_amt
into	#raw_deposits_2
from	#raw_deposits_1
group by client, date_created;

-- Find the last deposit date for the clients
select	client, max(date_created) as date_created
into	#raw_deposits_3
from	#raw_deposits_2
group by client;

-- Reset the information for the clients
update	clients
set	last_deposit_date	= null,
	last_deposit_amount	= 0

-- Update the last deposit date for the clients
update	clients
set	last_deposit_date	= i.date_created
from	clients c
inner join #raw_deposits_3 i on c.client = i.client;

-- Update the last deposit amount for the clients
update	clients
set	last_deposit_amount	= i.credit_amt
from	clients c
inner join #raw_deposits_2 i on c.client = i.client and c.last_deposit_date = i.date_created;

-- Discard the working tables
drop table	#raw_deposits_1
drop table	#raw_deposits_2
drop table	#raw_deposits_3
commit transaction

-- Start a transaction to make things repeatable
begin transaction
-- Delete items which are for $0.00. These should not have been recorded but it is possible to do so.
delete
from	client_deposits
where	deposit_amount = 0.00;

-- Create a list of the deposit records to be processed
select	d.client_deposit				as client_deposit,
	d.client					as client,
	d.deposit_amount				as deposit_amt,
	d.deposit_date					as deposit_date,

	case
		when dateadd(m, 1, d.deposit_date) < getdate() then dateadd(m, 2, d.deposit_date)
		else dateadd(m, 1, d.deposit_date)
	end						as new_deposit_date,

	convert(datetime, convert(varchar(10), dateadd(d, 0 - @days_back, d.deposit_date), 101) + ' 00:00:00')	as period_start,
	convert(datetime, convert(varchar(10), dateadd(d, @days_forward, d.deposit_date), 101) + ' 23:59:59')	as period_end,

	isnull(c.last_deposit_date,@missing_date)	as last_deposit_date,
	c.active_status					as active_status,

	case
		when c.last_deposit_date is null then @missed_initial_payment
		else @missed_subsequent_payment
	end						as retention_event

into	#deposits
from	client_deposits d
inner join clients c on d.client = c.client
where	d.one_time = 0
and	d.deposit_date between @roll_start and @roll_end
order by d.client, d.deposit_date;

-- Advance the deposit date to the next month
update	client_deposits
set	deposit_date = d.new_deposit_date
from	client_deposits cd
inner join #deposits d on cd.client_deposit = d.client_deposit;

/*
-- Remove items for deposits that are not "in error"
delete
from	#deposits
where	(last_deposit_date between period_start and period_end)
or	active_status not in ('A','AR');

-- Build a list of the "missed" deposits by client
select	client, sum(deposit_amt) as deposit_amt, deposit_date
into	#missed_dates
from	#deposits
group by client, deposit_date

-- Generate the transactions showing the missed deposits
insert into registers_client (tran_type, client, item_date, credit_amt, debit_amt, message)
select	'LD'		as tran_type,
	client		as client,
	deposit_date	as item_date,
	deposit_amt	as credit_amt,
	deposit_amt	as debit_amt,
	'failed to make deposit of $' + convert(varchar, deposit_amt, 1) as message
from	#missed_dates

-- Generate the retention event item
insert into client_retention_events (client, retention_event, amount, message, expire_type, expire_date, priority)
select	d.client, d.retention_event, d.deposit_amt, 'Client failed to make the expected deposit of $' + convert(varchar, d.deposit_amt, 1) + ' on ' + convert(varchar(10), d.deposit_date, 101) as message, isnull(e.expire_type,1), null, e.priority
from	#deposits d
left outer join retention_events e on d.retention_event = e.retention_event

-- Include system notes to the same effect. We don't use the notes. It is for the loely humans.
insert into client_notes (client, client_creditor, type, subject, is_text, dont_delete, dont_edit, dont_print, note)
select	client				as client,
	null				as client_creditor,
	3				as type,
	'Missed expected deposit'	as subject,
	1				as is_text,
	1				as dont_delete,
	1				as dont_edit,
	0				as dont_print,
	'Client failed to make the expected deposit of $' + convert(varchar, deposit_amt, 1) + ' on ' + convert(varchar(10), deposit_date, 101) as note
from	#missed_dates

drop table #missed_dates
*/

-- Commit the changes to the database
commit transaction

-- Discard the working table
drop table #deposits

-- Return success
return ( 1 )
GO
