USE [DebtPlus]
GO
/****** Object:  StoredProcedure [dbo].[xpr_disbursement_schedpay_deposit]    Script Date: 09/15/2014 13:13:35 ******/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE PROCEDURE [dbo].[xpr_disbursement_schedpay_deposit] ( @client as int ) AS

-- ===============================================================================================
-- ==            Return the deposit information for the schedpay update routine                 ==
-- ===============================================================================================

set nocount on

SELECT TOP 10	tran_type,
		date_created,
		credit_amt
FROM		registers_client with (nolock)
WHERE		client = @client
AND		tran_type NOT IN ('BB', 'EB')
AND		credit_amt > 0
ORDER BY date_created DESC

return ( @@rowcount )
GO
