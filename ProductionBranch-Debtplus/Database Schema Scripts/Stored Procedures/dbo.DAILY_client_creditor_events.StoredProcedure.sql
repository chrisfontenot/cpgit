USE [DebtPlus]
GO
/****** Object:  StoredProcedure [dbo].[DAILY_client_creditor_events]    Script Date: 09/15/2014 13:13:32 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE PROCEDURE [dbo].[DAILY_client_creditor_events] AS
-- =======================================================================================================
-- ==                 Adjust the disbursement factors on the debt(s) from a schedule                    ==
-- =======================================================================================================

-- ChangeLog
--   9/31/2002
--     Added support for client_creditor_balances table

-- Determine the end of the current effective date
declare	@end_of_day		datetime
select	@end_of_day = convert(datetime, convert(varchar(10), getdate(), 101) + ' 23:59:59')

-- Start a transaction for the operations
begin transaction
set nocount on

-- Fetch the P.A.F. creditor from the system configuration table
declare	@paf_creditor		varchar(10)
select	@paf_creditor	= paf_creditor
from	config;

-- Allocate a cursor for the items
declare	item_cursor cursor for
	select		e.date_created, e.created_by, cc.creditor, e.client_creditor, e.config_fee, e.[value]
	from		client_creditor_events e
	left outer join	client_creditor cc on e.client_creditor = cc.client_creditor
	where		date_updated is null
	and		effective_date <= @end_of_day
for update of	date_updated;

declare	@created_by		varchar(80)
declare	@date_created		datetime
declare	@creditor		varchar(10)
declare	@client_creditor	int
declare	@config_fee		int
declare	@value			money
declare	@original_disbursement	money

open item_cursor
fetch item_cursor into @date_created, @created_by, @creditor, @client_creditor, @config_fee, @value

while @@fetch_status = 0
begin
	-- Generate a system note that the disbursement factor was changed
	insert into client_notes (created_by, client, client_creditor, type, is_text, subject, note)
	select	@created_by, client, @client_creditor, 3, 1, 'Automatic update of disbursement factor',
		'The disbursement factor was changed from $' + convert(varchar, disbursement_factor, 1) + ' to $' + convert(varchar, @value, 1) + ' due to an event entered into the system on ' + convert(varchar(10), @date_created, 101) + '.' + char(13) + char(10) +
		'The balance at this time was $' + convert(varchar, isnull(bal.orig_balance,0) + isnull(bal.orig_balance_adjustment,0) + isnull(bal.total_interest,0) - isnull(bal.total_payments,0)) + '.'
	from	client_creditor cc inner join client_creditor_balances bal on cc.client_creditor_balance = bal.client_creditor_balance
	where	cc.client_creditor = @client_creditor;

	-- Adjust the disbursement factor for the debt
	update	client_creditor
	set	disbursement_factor	= @value	-- Set the new disbursement factor
--		,sched_payment		= @value	-- Adjust the scheduled payment
--		,current_sched_payment	= @value	-- as well as the value for the NFCC statistics
	where	client_creditor		= @client_creditor

	-- If the creditor is the PAF creditor then adjust the config_fee item for the client
	if @creditor = @paf_creditor
	begin
		if @config_fee >= 0
		begin
			update	clients
			set	config_fee = @config_fee
			where	client = (select client from client_creditor where client_creditor = @client_creditor)

			-- Generate a system note that the P.A.F. computation was changed
			insert into client_notes (created_by, client, client_creditor, type, is_text, subject, note)
			select		@created_by, c.client, null, 3, 1, 'Changed P.A.F. calculation',
					'The P.A.F. calculation was changed to ' + isnull(fee.description,'') + ' due to an event entered into the system on ' + convert(varchar(10), @date_created, 101) + '.'
			from		client_creditor cc, clients c, config_fees fee
			where		cc.client_creditor = @client_creditor
			and		cc.client = c.client
			and		fee.config_fee = @config_fee
		end
	end

	-- Indicate that the event was processed
	update	client_creditor_events
	set	date_updated = getdate()
	where	current of item_cursor;

	fetch item_cursor into @date_created, @created_by, @creditor, @client_creditor, @config_fee, @value
end

close item_cursor
deallocate item_cursor

-- Commit the changes
commit transaction

-- Indicate success
return ( 1 )
GO
