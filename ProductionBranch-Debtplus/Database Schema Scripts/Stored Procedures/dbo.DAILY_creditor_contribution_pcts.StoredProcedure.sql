USE [DebtPlus]
GO
/****** Object:  StoredProcedure [dbo].[DAILY_creditor_contribution_pcts]    Script Date: 09/15/2014 13:13:32 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[DAILY_creditor_contribution_pcts] AS
-- ===========================================================================================
-- ==          Adjust the contribution percentages at the appropriate time                  ==
-- ===========================================================================================

BEGIN TRANSACTION
SET NOCOUNT ON

DECLARE contrib_pct CURSOR FOR
	SELECT   creditor_contribution_pct,creditor,fairshare_pct_check,fairshare_pct_eft,creditor_type_check,creditor_type_eft
	FROM     creditor_contribution_pcts
	WHERE    (date_updated is null) AND effective_date <= getdate()
	ORDER BY effective_date

OPEN contrib_pct

DECLARE @creditor_contribution_pct	INT
DECLARE @creditor			typ_creditor
DECLARE @fairshare_pct_check		FLOAT
DECLARE @fairshare_pct_eft		FLOAT
DECLARE @creditor_type_check		typ_creditor_type
DECLARE @creditor_type_eft		typ_creditor_type
DECLARE @check_msg			VARCHAR(50)
DECLARE @eft_msg			VARCHAR(50)

-- Fetch the pending items from the system
FETCH contrib_pct INTO @creditor_contribution_pct,@creditor,@fairshare_pct_check,@fairshare_pct_eft,@creditor_type_check,@creditor_type_eft
WHILE @@fetch_status = 0
BEGIN
	-- Indicate that we have processed this specific record for the next time
	UPDATE	creditor_contribution_pcts
	SET	date_updated				= getdate()
	WHERE	creditor_contribution_pct	= @creditor_contribution_pct

	-- Update the creditor record with the new values
	UPDATE	creditors
	SET	creditor_contribution_pct	= @creditor_contribution_pct
	WHERE	creditor			= @creditor

	-- Include a system note that the percentage was changed
	IF @@rowcount > 0
	BEGIN

		-- Generate the message text for check percentages
		IF @creditor_type_check = 'B'
			SET @check_msg = 'BILL at ' + convert(varchar,isnull(@fairshare_pct_check,0) * 100.0) + '%'
		ELSE BEGIN
			IF @creditor_type_check = 'D'
				SET @check_msg = 'DEDUCT at ' + convert(varchar,isnull(@fairshare_pct_check,0) * 100.0) + '%'
			ELSE
				SET @check_msg = 'NO CONTRIBUTIONS'
		END

		-- Generate the message text for EFT percentages
		IF @creditor_type_eft = 'B'
			SET @eft_msg = 'BILL at ' + convert(varchar,isnull(@fairshare_pct_eft,0) * 100.0) + '%'
		ELSE BEGIN
			IF @creditor_type_eft = 'D'
				SET @eft_msg = 'DEDUCT at ' + convert(varchar,isnull(@fairshare_pct_eft,0) * 100.0) + '%'
			ELSE
				SET @eft_msg = 'NO CONTRIBUTIONS'
		END

		-- Merge the two messages if they differ
		if @eft_msg <> @check_msg
			select @check_msg = @check_msg + ' for checks and ' + @eft_msg + ' for EFT transfers'
		else
			select @check_msg = @check_msg + ' for both checks and EFT'

		-- Add a creditor system note that the contribution status was changed
		INSERT INTO creditor_notes (creditor,is_text,type,dont_delete,dont_edit,subject,note)
		VALUES (@creditor,
			1,3,1,1,
			'Contribution information was changed',
			'Contribution information was changed due to the scheduled request to the following infomation:' + char(13) + char(10) + @check_msg)
	END

	-- Obtain the next row from the recordset
	FETCH contrib_pct INTO @creditor_contribution_pct,@creditor,@fairshare_pct_check,@fairshare_pct_eft,@creditor_type_check,@creditor_type_eft
END

-- Cleanup
CLOSE contrib_pct
DEALLOCATE contrib_pct
SET NOCOUNT OFF
COMMIT TRANSACTION

-- Terminate normally
RETURN ( 1 )
GO
