USE [DebtPlus]
GO
/****** Object:  StoredProcedure [dbo].[rpt_counselor_productivity-original]    Script Date: 09/15/2014 13:13:33 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[rpt_counselor_productivity-original] ( @FromDate AS DateTime = NULL, @ToDate AS DateTime = NULL ) AS
-- ====================================================================================================
-- ==            Return the kept appointments by counselor/office                                    ==
-- ====================================================================================================

-- ChangeLog
--   2/20/2002
--     Looked only at client appointments, not workshop appointments.

-- Suppress intermediate result sets
set nocount on

IF @ToDate IS NULL
	SET @ToDate = getdate()

IF @FromDate IS NULL
	SET @FromDate = @FromDate

SET @FromDate	= convert(datetime, convert(varchar(10), @FromDate, 101) + ' 00:00:00')
SET @ToDate	= convert(datetime, convert(varchar(10), @ToDate,  101) + ' 23:59:59')

-- Retrieve the appointment information
select	ca.start_time				as 'start_time',
	ca.client				as 'client',
	pn.[first]				as 'firstname',
	pn.[last]				as 'lastname',
	cad.city					as 'city',
 	isnull(ca.result,'Not specified')	as 'result',
	isnull(dbo.format_normal_name(cox.prefix,cox.first,cox.middle,cox.last,cox.suffix),'Not specified')		as 'counselor',
	isnull(o.name,'Not Specified')		as 'office',
	isnull(ap.appt_name,'Not Specified')	as 'type'

from	client_appointments ca	with (nolock)

inner join clients c		with (nolock) on ca.client = c.client 
-- and ca.client_appointment = c.first_kept_appt    
left outer join counselors co	with (nolock) on ca.counselor = co.counselor
left outer join offices o	with (nolock) on ca.office = o.office
left outer join appt_types ap	with (nolock) on ca.appt_type = ap.appt_type
left outer join people p	with (nolock) on ca.client = p.client and 1 = p.relation
left outer join names cox with (nolock) on co.nameid = cox.name
left outer join names pn with (nolock) on p.nameid = pn.name
left outer join addresses cad with (nolock) on c.addressid = cad.address

where	ca.status IN ('K','W')
AND	ca.workshop is null
AND	ca.start_time BETWEEN @FromDate AND @ToDate

-- Order by office, start_time,counselor
order by  office, start_time,counselor

return ( @@rowcount )
GO
