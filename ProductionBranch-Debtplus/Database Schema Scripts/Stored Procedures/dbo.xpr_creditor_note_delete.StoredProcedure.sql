USE [DebtPlus]
GO
/****** Object:  StoredProcedure [dbo].[xpr_creditor_note_delete]    Script Date: 09/15/2014 13:13:34 ******/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE PROCEDURE [dbo].[xpr_creditor_note_delete] ( @Creditor_Note AS INT ) AS

-- ==================================================================================================
-- ==            Delete the indicated creditor note                                                ==
-- ==================================================================================================

-- Disable intermediate results
SET NOCOUNT ON

-- Ensure that the note is owned by the proper person
DECLARE	@CurrentUser	VarChar(80)
DECLARE	@NoteOwner	VarChar(80)
DECLARE	@MyName		VarChar(80)
DECLARE	@DontDelete	INT
DECLARE	@Type		INT

SELECT	@CurrentUser	= suser_sname(),
	@MyName		= user_name(),
	@NoteOwner	= created_by,
	@DontDelete	= dont_delete,
	@Type		= type
FROM	creditor_notes
WHERE	creditor_note	= @creditor_note

-- If this is the database owner then accept the delete of any and all notes
IF @MyName = 'dbo'
	SET @DontDelete	= 0

ELSE BEGIN

	-- System notes are not delete canidates unless earlier exempted
	IF @Type = 3
		SET @DontDelete = 1
	ELSE
		-- If the person deleting the note is the creator then don't bother with dont delete flag
		IF @NoteOwner = @CurrentUser
			SET @DontDelete	= 0
END

-- Ensure that the note may be deleted
IF @DontDelete > 0
BEGIN
	RaisError (50049, 16, 1, @CurrentUser, @creditor_note)
	Return ( 0 )
END

-- Remove the note from the system
DELETE
FROM	creditor_notes
WHERE	creditor_note	= @creditor_note

DECLARE	@Rows		INT
SET	@Rows = @@rowcount

IF @Rows < 1
	RaisError (50050, 10, 1, @creditor_note)

-- Return success to the caller
RETURN ( @Rows )
GO
