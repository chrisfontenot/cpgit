USE [DebtPlus]
GO
/****** Object:  StoredProcedure [dbo].[xpr_disbursement_schedpay_client]    Script Date: 09/15/2014 13:13:35 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[xpr_disbursement_schedpay_client] ( @client as int ) as
-- ======================================================================================================
-- ==            Return the information needed for the schedpay utility                                ==
-- ======================================================================================================
set nocount on

-- ======================================================================================================
-- ==            Find the number of disbursement notes for the client                                  ==
-- ======================================================================================================
declare		@note_count int

select		@note_count = count(*)
from		disbursement_notes
where		client = @client
and		disbursement_register is null;

-- ======================================================================================================
-- ==            Find the number of disbursement notes for the client                                  ==
-- ======================================================================================================
declare		@paf_creditor	varchar(10)
declare		@payment_minimum money
declare		@fee_record int

select		@paf_creditor = paf_creditor,
			@payment_minimum = payment_minimum
from		config

select		@fee_record = min(config_fee)
from		config_fees
where		[default] = 1

if @fee_record is null
	select		@fee_record = min(config_fee)
	from		config_fees

-- ======================================================================================================
-- ==            Obtain the information to process this client.                                        ==
-- ======================================================================================================
select		isnull(c.held_in_trust,0)			AS 'trust_balance',
		isnull(@note_count,0)				as 'notes',

		-- Information from the config fee table
		isnull(f.fee_type,0)				as 'fee_type',
		isnull(f.fee_base,0)				as 'fee_base',
		isnull(f.fee_percent,0)				as 'fee_percent',
		isnull(f.fee_per_creditor,0)			as 'fee_per_creditor',
		isnull(f.fee_disb_max,0)			as 'fee_disb_max',
		isnull(f.fee_monthly_max,0)			as 'fee_monthly_max',
		isnull(f.fee_balance_max,0)			as 'fee_balance_max',
		isnull(c.stack_proration,0)			as 'stack_proration',

		-- Information from the config table
		isnull(@paf_creditor,'')			as 'paf_creditor',
		isnull(@payment_minimum,0)			as 'payment_minimum',
		isnull(f.config_fee,1)				as 'config_fee'

from		clients c		WITH (NOLOCK)
left outer join	config_fees f		with (nolock) on f.config_fee = coalesce(c.config_fee,@fee_record,1)
where		c.client		= @client

return ( @@rowcount )
GO
