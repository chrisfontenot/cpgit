USE [DebtPlus]
GO
/****** Object:  StoredProcedure [dbo].[rpt_workshop_posted_attendance_summary]    Script Date: 09/15/2014 13:13:34 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE PROCEDURE [dbo].[rpt_workshop_posted_attendance_summary] ( @from_date as datetime = null, @to_date as datetime = null ) as

-- List workshop attendance counts by client active status

if @to_date is null
	select	@to_date	= getdate()

if @from_date is null
	select	@from_date	= @to_date

select	@from_date	= convert(datetime, convert(varchar(10), @from_date, 101) + ' 00:00:00'),
	@to_date	= convert(datetime, convert(varchar(10), @to_date,   101) + ' 23:59:59')

select	convert(varchar(10), w.start_time, 101) + ' #' + convert(varchar, w.workshop) + isnull(' ' + wt.description,convert(varchar, w.workshop_type))	as workshop,

	case	when c.active_status in ('A','AR') then 1 else 0 end as count_active,
	case	when c.active_status = 'WKS' then 1 else 0 end as count_wks,
	case	when c.active_status = 'I'   then 1 else 0 end as count_inactive,
	case	when c.active_status in ('A','AR','WKS','I') then 0 else 1 end as count_other,

	case
		when ca.status in ('K','W') and ca.workshop_people > 0 then ca.workshop_people
		else 0
	end		as attended,

	case
		when ca.status = 'M' and ca.workshop_people > 0 then ca.workshop_people
		else 0
	end		as no_show
into	#workshops
from	workshops w
inner join client_appointments ca on ca.workshop = w.workshop
inner join workshop_types wt on w.workshop_type = wt.workshop_type
inner join clients c on ca.client = c.client
where	ca.office is null
and	w.start_time between @from_date and @to_date

select	workshop,sum(count_active) as count_active, sum(count_wks) as count_wks, sum(count_inactive) as count_inactive, sum(count_other) as count_other, sum(attended) as attended, sum(no_show) as no_show
from	#workshops
group by workshop
order by 1;

drop table #workshops
	
return ( @@rowcount )
GO
