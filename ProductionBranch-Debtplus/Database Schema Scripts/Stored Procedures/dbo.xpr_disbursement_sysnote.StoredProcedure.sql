USE [DebtPlus]
GO
/****** Object:  StoredProcedure [dbo].[xpr_disbursement_sysnote]    Script Date: 09/15/2014 13:13:35 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE PROCEDURE [dbo].[xpr_disbursement_sysnote] ( @Client AS INT, @disbursement_register AS INT, @EndingTrust AS Money = NULL, @CreateDate AS DateTime = NULL ) AS

-- =================================================================================================
-- ==            Insert a disbursement note showing the payments for the client and batch         ==
-- =================================================================================================

-- Disable intermediate result sets
SET NOCOUNT ON

-- This is a cursor for accessing the disbursement detail information

-- Remove the comments to generate the disbursement notes from items which have already been disbursed.
-- This procedure is normally called from the disbursement "programming" stage so use the temporary
-- tables to retrieve the results. If you call this after doing the disbursement, look in the permanent
-- table, registers_client_creditor, for the data.
declare batch_cursor CURSOR FORWARD_ONLY FOR
	select	d.creditor,
		isnull(cc.account_number,'') as account_number,
		d.debit_amt,
		d.current_balance as starting_balance
--	from	registers_client_creditor d
	from	disbursement_creditors d
	left outer join client_creditor cc on d.client_creditor = cc.client_creditor
	where	d.disbursement_register = @disbursement_register
	and	d.client = @Client
--	and	d.tran_type in ('AD', 'BW')

open batch_cursor
declare	@creditor	VarChar(10)
declare	@account_number varchar(256)
declare	@Amount		money
declare	@StartingBal	money

declare	@msg		varchar(8000)
declare	@item		varchar(256)

-- Generate the standard header text message
SELECT @msg = 'Autodisbursement Performed\par \par CREDITOR  ACCOUNT NUMBER              PAYMENT   PRIOR BALANCE'

fetch batch_cursor INTO @creditor, @account_number, @Amount, @StartingBal
while @@fetch_status = 0
begin
	-- The account number must be exactly 22 characters in length for the proper formatting.
	if len(@account_number) > 22
		select @account_number = right(@account_number, 22)
	select @account_number = substring(@account_number + '                       ', 1, 22)
	
	-- If a create date is passed then this is being executed AFTER postdisbursement.
	select	@item = left(ltrim(rtrim(@creditor)) + '       ', 8) +
			'  ' +
			@account_number +
			right('             $' + convert(varchar, @Amount, 1), 13) +
			right('                $' + convert(varchar, @StartingBal, 1), 16)

	-- Include the line into the message text
	select @msg = @msg + '\par ' + @item

	-- Go on to the next debt
	fetch batch_cursor INTO @creditor, @account_number, @Amount, @StartingBal
end

-- Release the cursor
close batch_cursor
deallocate batch_cursor

-- Fetch the ending trust balance
IF @EndingTrust IS NULL
	SELECT	@EndingTrust	= isnull(held_in_trust,0)
	FROM	clients WITH (NOLOCK)
	WHERE	client		= @client

-- Include the ending trust balance into the message
select @msg = @msg + '\par \par Trust Balance After Disbursement = $' + convert(varchar, @EndingTrust, 1)

-- Finally, wrap the text with the appropriate header and trailer for RTF text
SELECT @msg = '{\rtf1\ansi\ansicpg1252\deff0\deflang1033{\fonttbl{\f0\fmodern\fprq1\fcharset0 Courier New;}}\viewkind4\uc1\pard\f0\fs20 ' + @msg + '\par}'

-- Insert the disbursement message
insert into disbursement_notes	(client,	disbursement_register,	type,	subject,			note,	date_created)
select				@client,	@disbursement_register,	3,		'Autodisbursement Performed',	@msg,	isnull(@CreateDate,getdate())

-- Complete processing
RETURN ( SCOPE_IDENTITY() )
GO
