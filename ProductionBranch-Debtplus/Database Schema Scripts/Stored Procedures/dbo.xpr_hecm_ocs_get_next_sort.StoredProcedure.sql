-- =============================================
-- Author:		BinhTran
-- Create date: 
-- Description:	
-- =============================================
ALTER PROCEDURE [dbo].[xpr_hecm_ocs_get_next_sort] @program INT = 0
	,@isActive BIT = NULL
	,@preferredLanguage INT = NULL
	,@statusCode INT = NULL
	,@minContacts INT = NULL
	,@maxContacts INT = NULL
	,@stateAbbrev VARCHAR(5) = NULL
	,@queueCode VARCHAR(10) = NULL
	,
	--TIMEZONES
	@atlantic BIT = 0
	,@eastern BIT = 0
	,@central BIT = 0
	,@mountain BIT = 0
	,@pacific BIT = 0
	,@alaska BIT = 0
	,@hawaii BIT = 0
	,@chamorro BIT = 0
	,@indeterminate BIT = 0
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	SELECT sort.OCSClientId
		,sort.ClaimedDate
		,sort.AttemptCount
		,sort.TzDescriptor
	FROM view_OCS_Sort [sort]
	WHERE (sort.Program = @program)
		--Program specific time-based rules
		AND
		--10 attempts, 180 days
		(
			@program NOT IN (0, 1, 2, 3, 4, 5) OR
			--attempt count condition
			(sort.AttemptCount < 11) OR
			--time-based condition			
			(DATEDIFF(day, sort.UploadDate, getdate()) < 181)
			) AND
		--unclaimed
		(ClaimedDate IS NULL OR CAST(FLOOR(CAST(getdate() AS FLOAT)) AS DATETIME) <> CAST(FLOOR(CAST(ClaimedDate AS FLOAT)) AS DATETIME)) AND
		--active filter
		(@isActive IS NULL OR sort.ActiveFlag = @isActive) AND
		--preferred language
		(@preferredLanguage IS NULL OR sort.PreferredLanguage = @preferredLanguage) AND
		--status code
		(@statusCode IS NULL OR sort.StatusCode = @statusCode)
		--min/max contacts
		AND ((@minContacts IS NULL AND @maxContacts IS NULL) OR (@minContacts IS NOT NULL AND @maxContacts IS NOT NULL AND sort.AttemptCount >= @minContacts AND sort.AttemptCount <= @maxContacts) OR (@minContacts IS NOT NULL AND @maxContacts IS NULL AND sort.AttemptCount >= @minContacts) OR (@minContacts IS NULL AND @maxContacts IS NOT NULL AND sort.AttemptCount <= @maxContacts)) AND
		--state
		(@stateAbbrev IS NULL OR sort.StateAbbreviation = @stateAbbrev)
		--queue
		AND (@queueCode IS NULL OR sort.QueueCode = @queueCode) AND
		--timezone filter
		(
			((@atlantic = 1 AND sort.TzDescriptor = 'Atlantic') AND dbo.nationstar_is_in_time_range('Atlantic', sort.ClaimedDate) = 1) OR ((@eastern = 1 AND sort.TzDescriptor = 'Eastern') AND dbo.nationstar_is_in_time_range('Eastern', sort.ClaimedDate) = 1) OR ((@central = 1 AND sort.TzDescriptor = 'Central') AND dbo.nationstar_is_in_time_range('Central', sort.ClaimedDate) = 1) OR ((@mountain = 1 AND sort.TzDescriptor = 'Mountain') AND dbo.nationstar_is_in_time_range('Mountain', sort.ClaimedDate) = 1) OR ((@pacific = 1 AND sort.TzDescriptor = 'Pacific') AND dbo.nationstar_is_in_time_range('Pacific', sort.ClaimedDate) = 1) OR ((@alaska = 1 AND sort.TzDescriptor = 'Alaska') AND dbo.nationstar_is_in_time_range('Alaska', sort.ClaimedDate) = 1) OR ((@hawaii = 1 AND sort.TzDescriptor = 'Hawaii') AND dbo.nationstar_is_in_time_range('Hawaii', sort.ClaimedDate) = 1
				) OR ((@chamorro = 1 AND sort.TzDescriptor = 'Chamorro') AND dbo.nationstar_is_in_time_range('Chamorro', sort.ClaimedDate) = 1) OR ((@indeterminate = 1 AND sort.TzDescriptor = 'Indeterminate') AND dbo.nationstar_is_in_time_range('Indeterminate', sort.ClaimedDate) = 1)
			)
	ORDER BY sort.AttemptCount ASC
		,sort.ClaimedDate DESC
END
