USE [DebtPlus]
GO
/****** Object:  StoredProcedure [dbo].[xpr_insert_creditor_addresses]    Script Date: 09/15/2014 13:13:35 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[xpr_insert_creditor_addresses] ( @creditor as varchar(10), @type as varchar(1) = 'P', @attn as varchar(50) = null, @AddressID as int ) as
    -- ==========================================================================================================
    -- ==         Create a creditor address item for a creditor                                                ==
    -- ==========================================================================================================
    insert into creditor_addresses([creditor], [type], [attn], [AddressID]) VALUES (@creditor, @type, @attn, @AddressID);
    return ( scope_identity() )
GO
