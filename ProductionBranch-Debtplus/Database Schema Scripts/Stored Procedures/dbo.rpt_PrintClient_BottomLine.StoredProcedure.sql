IF NOT EXISTS(SELECT * FROM SYSOBJECTS WHERE TYPE='P' AND NAME=N'rpt_PrintClient_BottomLine')
	EXEC ('CREATE PROCEDURE rpt_PrintClient_BottomLine AS')
GO

ALTER PROCEDURE [dbo].[rpt_PrintClient_BottomLine] ( @client as int ) as
-- =============================================================================================
-- ==            Print the bottom line information for the client                             ==
-- =============================================================================================

-- ChangeLog
--   12/9/2002
--      Added client_other_debts table summary information
--   5/6/2003
--      Do not include disbursement_factors for zero balance debts into the "program_fees"

-- suppress intermediate results
set nocount on

declare	@budget_id int
declare	@payroll_1					money
declare	@payroll_2					money
declare	@plan_program_fees			money
declare @client_program_fees		money
declare	@assets						money
declare	@plan_living_expenses		money
declare	@plan_housing_expenses		money
declare	@plan_other_expenses		money
declare @client_living_expenses		money
declare @client_housing_expenses	money
declare @client_other_expenses		money
declare	@other_debt					money
declare	@setup_creditor				varchar(10)

-- Find the setup creditor. We don't want this creditor in the list of expenses.
select	@setup_creditor = setup_creditor
from	config with (nolock)

-- The creditor ID must be defined and not null. Make it invalid if the item is null.
if @setup_creditor is null
	set	@setup_creditor	= '*****'

select	@payroll_2 = sum(isnull(net_income,0))
from	people
where	client = @client

select	@payroll_1 = isnull(net_income,0)
from	people
where	client = @client
and		relation = 1

select	@assets	= sum(isnull(asset_amount,0))
from	assets
where	client = @client

select	@budget_id = dbo.map_client_to_budget ( @client )
		
if isnull(@budget_id,0) > 0
begin
	select	@plan_living_expenses		= sum(suggested_amount),
			@client_living_expenses		= sum(client_amount)
	from	budget_detail d
	inner join budget_categories cat on d.budget_category = cat.budget_category
	where	cat.living_expense <> 0
	and		budget = @budget_id

	select	@plan_housing_expenses		= sum(suggested_amount),
			@client_housing_expenses	= sum(client_amount)
	from	budget_detail d
	inner join budget_categories cat on d.budget_category = cat.budget_category
	where	cat.housing_expense <> 0
	and		budget = @budget_id

	select	@plan_other_expenses	= sum(suggested_amount),
			@client_other_expenses	= sum(client_amount)
	from	budget_detail d
	where	budget = @budget_id
end
	
select	@plan_program_fees		= sum(isnull(disbursement_factor,0)),
		@client_program_fees	= sum(isnull(non_dmp_payment,0))
from	client_creditor cc
left outer join creditors cr on isnull(cc.creditor,'*****') = cr.creditor
left outer join creditor_classes ccl on isnull(cr.creditor_class,-1) = ccl.creditor_class
left outer join client_creditor_balances bal on cc.client_creditor_balance = bal.client_creditor_balance
where	(client	= @client)
and		(isnull(cr.creditor,'') <> @setup_creditor)
and		(cc.reassigned_debt = 0)
and		((cc.creditor is null) or ((isnull(ccl.zero_balance,0) > 0) or (bal.orig_balance + bal.orig_balance_adjustment + bal.total_interest > bal.total_payments)))

-- Find the total of the "other debt" payments
select	@other_debt	= sum(payment)
from	client_other_debts with (nolock)
where	client		= @client

-- Return the fields to the original v1 report
if @assets is null					set @assets = 0
if @plan_living_expenses is null	set @plan_living_expenses = 0
if @client_living_expenses is null	set @client_living_expenses = 0
if @plan_housing_expenses is null	set @plan_housing_expenses = 0
if @client_housing_expenses is null	set @client_housing_expenses = 0
if @plan_other_expenses is null		set @plan_other_expenses = 0
if @client_other_expenses is null	set @client_other_expenses = 0
if @client_program_fees is null		set @client_program_fees = 0
if @plan_program_fees is null		set @plan_program_fees = 0
if @other_debt is null				set @other_debt = 0
if @payroll_1 is null				set @payroll_1 = 0
if @payroll_2 is null				set @payroll_2 = 0

set	@payroll_2				= @payroll_2 - @payroll_1
set	@plan_other_expenses	= @plan_other_expenses - @plan_housing_expenses - @plan_living_expenses
set @client_other_expenses	= @client_other_expenses - @client_housing_expenses - @client_living_expenses

select	@payroll_1					as 'payroll_1',
		@Payroll_2					as 'payroll_2',
		@assets						as 'assets',
		@plan_living_expenses		as 'living_expenses',
		@plan_housing_expenses		as 'housing_expenses',
		@plan_other_expenses		as 'other_expenses',
		@plan_program_fees			as 'program_fees',
		@other_debt					as 'other_debt',
		
		-- Return the fields to the v2 report

		@payroll_1	as 'v2_self_person_1_net',
		@payroll_1	as 'v2_plan_person_1_net',

		@payroll_2	as 'v2_self_person_2_net',
		@payroll_2	as 'v2_plan_person_2_net',

		@assets		as 'v2_self_asset',
		@assets		as 'v2_plan_asset',

		@payroll_1 + @payroll_2 + @assets as 'v2_self_total_assets',
		@payroll_1 + @payroll_2 + @assets as 'v2_plan_total_assets',
		
		@client_living_expenses + @client_housing_expenses + @client_other_expenses	as 'v2_self_expense',
		@plan_living_expenses + @plan_housing_expenses + @plan_other_expenses		as 'v2_plan_expense',

		@other_debt	as 'v2_self_other_debt',
		@other_debt	as 'v2_plan_other_debt',

		(@client_living_expenses + @client_housing_expenses + @client_other_expenses) + @other_debt	as 'v2_self_expense_total',
		(@plan_living_expenses + @plan_housing_expenses + @plan_other_expenses) + @other_debt			as 'v2_plan_expense_total',

		@client_program_fees	as 'v2_self_debt_payment',
		@plan_program_fees		as 'v2_plan_debt_payment',

		(@payroll_1 + @payroll_2 + @assets) - (@client_living_expenses + @client_housing_expenses + @client_other_expenses + @other_debt) - @client_program_fees	as 'v2_self_total',
		(@payroll_1 + @payroll_2 + @assets) - (@plan_living_expenses + @plan_housing_expenses + @plan_other_expenses + @other_debt) - @plan_program_fees			as 'v2_plan_total'

return ( 1 )
GO
GRANT EXECUTE ON rpt_PrintClient_BottomLine TO public AS dbo;
GO
DENY EXECUTE ON rpt_PrintClient_BottomLine TO www_role;
GO
