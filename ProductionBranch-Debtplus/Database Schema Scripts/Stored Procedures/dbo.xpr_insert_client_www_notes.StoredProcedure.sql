USE [DebtPlus]
GO
/****** Object:  StoredProcedure [dbo].[xpr_insert_client_www_notes]    Script Date: 09/15/2014 13:13:35 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[xpr_insert_client_www_notes] (@PKID as uniqueidentifier, @Message as varchar(256), @Date_Shown as datetime = null, @Date_Viewed as datetime = null) as
	-- ==================================================================================================
	-- ==         Insert a client www note and return the new id for the note                          ==
	-- ==================================================================================================
	insert into client_www_notes (PKID, message) values (@PKID, @message)
	return ( scope_identity() )
GO
