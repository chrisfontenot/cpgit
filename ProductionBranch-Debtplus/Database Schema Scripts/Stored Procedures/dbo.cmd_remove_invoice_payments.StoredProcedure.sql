USE [DebtPlus]
GO
/****** Object:  StoredProcedure [dbo].[cmd_remove_invoice_payments]    Script Date: 09/15/2014 13:13:31 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE PROCEDURE [dbo].[cmd_remove_invoice_payments] ( @creditor as typ_creditor, @date_created as datetime ) as

-- ===================================================================================================
-- ==            Remove the payments for creditor/date invoices                                     ==
-- ===================================================================================================

set nocount on

declare	@start_date	datetime
declare	@end_date	datetime
select	@start_date = convert(datetime, convert(varchar(10), @date_created, 101) + ' 00:00:00')
select	@end_date   = dateadd(d, 1, @start_date)

begin transaction
set xact_abort on

declare item_cursor cursor for
	select	creditor_register,tran_type,invoice_register,credit_amt
	from	registers_creditor
	where	tran_type in ('RC', 'RA')
	and	date_created between @start_date and @end_date
	and	creditor = @creditor

declare	@creditor_register	int
declare	@tran_type		varchar(2)
declare	@invoice_register	int
declare	@credit_amt		money

open item_cursor
fetch item_cursor into @creditor_register,@tran_type, @invoice_register, @credit_amt
while @@fetch_status = 0
begin

	print convert(varchar, @invoice_register) + ' ' + @tran_type + ' ' + convert(varchar, @credit_amt, 1)

	if @tran_type = 'RC'
	begin
		update	registers_invoices
		set	pmt_amount = pmt_amount - @credit_amt,
			pmt_date = null
		where	invoice_register = @invoice_register;

	end else begin

		update	registers_invoices
		set	adj_amount = adj_amount - @credit_amt,
			adj_date = null
		where	invoice_register = @invoice_register;
	end

	delete
	from	registers_creditor
	where	creditor_register = @creditor_register;

	fetch item_cursor into @creditor_register, @tran_type, @invoice_register, @credit_amt
end
close item_cursor
deallocate item_cursor

commit transaction
GO
