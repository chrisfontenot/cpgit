USE [DebtPlus]
GO
/****** Object:  StoredProcedure [dbo].[xpr_delete_client_creditor_proposal]    Script Date: 09/15/2014 13:13:35 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[xpr_delete_client_creditor_proposal] (@client_creditor_proposal as int) as

delete
from	client_creditor_proposals
where	client_creditor_proposal	= @client_creditor_proposal

return ( @@rowcount )
GO
