USE [DebtPlus]
GO
/****** Object:  StoredProcedure [dbo].[www_intake_new_person]    Script Date: 09/15/2014 13:13:34 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE PROCEDURE [dbo].[www_intake_new_person] (@intake_client as int, @intake_id as varchar(20), @person as int, @dob as varchar(80), @gender as int, @race as int, @gross_income as money, @net_income as money, @education as int) as

-- ==============================================================================================================
-- ==            Create/Update the person associated with the ID                                               ==
-- ==============================================================================================================

-- Validate the ID against the client
if not exists (	select	*
		from	intake_clients
		where	intake_client	= @intake_client
		and	intake_id	= @intake_id )
begin
	RaisError ('The client ID is invalid.', 16, 1)
	return ( 0 )
end

-- Validate the person information
if @person not in (1, 2)
begin
	RaisError ('The person value %d is invalid.', 16, 1, @person)
	return ( 0 )
end

-- Convert the date to the appropriate value
declare	@birthdate	datetime
select	@dob = ltrim(rtrim(@dob))
if @dob is null
	select @dob = ''

if @dob = ''
	select	@birthdate = null
else
	select	@birthdate = convert(datetime, @dob)

-- Look for the various types that indicate that the column is "not specified".
if @gender < 0
	select	@gender = null

if @race < 0
	select	@race = 10

if @gross_income < 0
	select	@gross_income = 0

if @net_income < 0
	select	@net_income = 0

if @education < 0
	select	@education = 0

-- Update the current information
update	intake_people
set	birthdate	= @birthdate,
	gender		= @gender,
	race		= @race,
	gross_income	= @gross_income,
	net_income	= @net_income,
	education	= @education
where	intake_client	= @intake_client
and	person		= @person

if @@rowcount < 1
begin
	insert into intake_people	( intake_client,  person,  birthdate,  gender,  race,  gross_income,  net_income,  education)
	values				(@intake_client, @person, @birthdate, @gender, @race, @gross_income, @net_income, @education)
end

return ( @@rowcount )
GO
