USE [DebtPlus]
GO
/****** Object:  StoredProcedure [dbo].[xpr_rpps_response_CDR]    Script Date: 09/15/2014 13:13:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[xpr_rpps_response_CDR] ( @response_file as int, @trace_number as varchar(15), @biller_id as varchar(10), @return_code as varchar(3), @name as varchar(5) = null, @account_number as varchar(22) = null, @net as money = 0, @client as int, @ssn as varchar(9) = null, @company_identification as varchar(10) = null, @reason as varchar(3) = null, @counter_offer as money = 0, @information_required as varchar(22) = null, @resubmit_date as datetime = null, @cycle_months_remaining as int = null, @forgiven_pct as float = null, @first_payment_date as datetime = null, @good_thru_date as datetime = null, @ineligible_reason as varchar(10) = null, @internal_program_ends_date as datetime = null, @interest_rate as float = null, @third_party_detail as varchar(10) = null, @third_party_contact as varchar(80) = null ) as

-- =======================================================================================================================
-- ==            Generate the response for a CDR request                                                                ==
-- =======================================================================================================================

-- ChangeLog
--   4/15/2003
--     Wrote the counter_offer to the tables. It was errorenously ignored.
--   5/11/2012
--     Added fields for mastercard's "cleanup"

-- Generate the base response row
declare	@rpps_response_detail	int
execute @rpps_response_detail = xpr_rpps_response @response_file, @trace_number, @biller_id, @return_code, 'CDR', @name, @account_number, @net

-- Insert the addendum information for proposal acceptances
if @rpps_response_detail > 0
begin
	-- Remove the spaces from the SSN field
	if @ssn is not null
		select @ssn = ltrim(rtrim(@ssn))

	if @ssn not like '[0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9]'
		select @ssn = null

	-- Remove the spaces from the company identification
	if @company_identification is not null
		select @company_identification = ltrim(rtrim(@company_identification))

	if @company_identification = ''
		select @company_identification = null

	-- Remove the spaces from the information required
	if @information_required is not null
		select @information_required = ltrim(rtrim(@information_required))

	if @information_required = ''
		select @information_required = null

	-- Remove the spaces from the reason
	if @reason is not null
		select @reason = ltrim(rtrim(@reason))

	if @reason = ''
		select @reason = null

	-- Update the addendum information for a rejected proposal
	insert into rpps_response_details_cdr ([rpps_response_detail],[company_identification],[ssn],[client],[reject_reason],[counter_offer],[additional_info_required],[resubmit_date],[cycle_months_remaining],[forgiven_pct],[first_payment_date],[good_thru_date],[ineligible_reason],[internal_program_ends_date],[interest_rate],[third_party_detail],[third_party_contact])
	values (@rpps_response_detail,@company_identification,@ssn,@client,@reason,isnull(@counter_offer,0),@information_required,@resubmit_date,@cycle_months_remaining,@forgiven_pct,@first_payment_date,@good_thru_date,@ineligible_reason,@internal_program_ends_date,@interest_rate,@third_party_detail,@third_party_contact)
end

return ( @rpps_response_detail )
GO
