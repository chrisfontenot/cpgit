USE [DebtPlus]
GO
/****** Object:  StoredProcedure [dbo].[cccs_admin_intake_budget_expenses]    Script Date: 09/15/2014 13:13:31 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[cccs_admin_intake_budget_expenses] ( @application_id as int, @client as int = NULL ) as

-- Suppress intermediate results
set nocount on

-- If we are not given a client then find the client from the applications.
-- This is normally only done on a re-import operation so the client should be there
if @client is null
	select	@client = ForeignDatabaseID
	from	cccs_admin..applications
	where	application_id = @application_id

-- There must be a client at this point or we can not continue.	
if @client is null
begin
	RaisError('Client ID is not passed and can not be determined', 16, 1)
	return ( 0 )
end

-- Translate the budget categories to the values used by DebtPlus
select	dbo.cccs_admin_map_budgets ( c.expense_type_id ) as budget_category, convert(money, ISNULL(c.amount,0)) as client_amount, convert(money,coalesce(s.amount,c.amount,0)) as suggested_amount, c.ForeignDatabaseID
into	#budgets
from	cccs_admin..applications x
left outer join cccs_admin..applicant_expenses c ON x.application_id = c.application_id
left outer join cccs_admin..budgets b on x.application_id = b.application_id
left outer join cccs_admin..budget_expenses s on b.budget_id = s.budget_id AND c.expense_type_id = s.expense_type_id
where	x.application_id = @application_id

if exists ( select * from #budgets )
begin

	-- Do special processing for the "OTHER" category
	if exists ( select * from #budgets where budget_category = 10000 )
	begin
	
		-- First, allocate a budget category for it.
		declare @budget_category_other	int
		select  @budget_category_other = budget_category
		from	budget_categories_other
		where	[client]      = @client
		and     [description] = 'OTHER';

		if @budget_category_other is null
		begin
			insert into budget_categories_other (client, description ) values ( @client, 'OTHER' )
			select  @budget_category_other = scope_identity()
		end

		-- Then update the budget detail with the category ID
		update	#budgets
		set	    budget_category = @budget_category_other
		where	budget_category = 10000
	end

	declare	@budget int

	-- Find the existing budget ID
	select	@budget = b.budget
	from	#budgets x
	inner join budgets b on convert(int,x.ForeignDatabaseID) = b.budget
	where	x.ForeignDatabaseID is not null
	and     b.client = @client;
	
	-- Allocate the budget infomation
	if @budget is null
	begin
		insert into budgets ( client ) values ( @client )
		select @budget = scope_identity()
	end else
	
		-- Toss all existing budget detail since it is being reloaded
		delete
		from	budget_detail
		where	budget = @budget;

	-- Include the budget details
	insert into budget_detail (budget, budget_category, client_amount, suggested_amount)
	select	@budget, budget_category, sum(client_amount), sum(suggested_amount)
	from	#budgets
	group by budget_category
	order by budget_category;
	
	-- Update the key information in the other database for the next time
	update cccs_admin..applicant_expenses
	set    ForeignDatabaseID = @budget
	where  application_id = @application_id;
	
end

drop table #budgets
return ( @budget )
GO
