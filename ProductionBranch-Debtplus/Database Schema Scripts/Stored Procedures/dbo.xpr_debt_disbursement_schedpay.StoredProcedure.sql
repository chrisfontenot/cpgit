USE [DebtPlus]
GO
/****** Object:  StoredProcedure [dbo].[xpr_debt_disbursement_schedpay]    Script Date: 09/15/2014 13:13:35 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE PROCEDURE [dbo].[xpr_debt_disbursement_schedpay] ( @client as int ) as

-- ========================================================================================
-- ==               Correct the scheduled pay to reflect changes in the disbursement     ==
-- ========================================================================================

-- ChangeLog
--   9/31/2002
--     Added support for client_creditor_balances table
--   01/15/2002
--     Added creditor_classes table references and dropped "creditors.fee_creditor"

-- Suppress intermediate results
set nocount on

begin transaction

-- The scheduled pay for standard debts is the disbursement factor less the amount this month
update	client_creditor
set	sched_payment = disbursement_factor - bal.payments_month_0
from	client_creditor cc
inner join client_creditor_balances bal on cc.client_creditor_balance = bal.client_creditor_balance
where	client = @client;

-- Limit the scheduled pay to the balance
update	client_creditor
set	sched_payment = isnull(bal.orig_balance,0) + isnull(bal.orig_balance_adjustment,0) + isnull(bal.total_interest,0) - isnull(bal.total_payments,0)
from	client_creditor cc
inner join client_creditor_balances bal with (nolock) on cc.client_creditor_balance = bal.client_creditor_balance
inner join creditors cr on cc.creditor = cr.creditor
LEFT OUTER JOIN creditor_classes ccl WITH (NOLOCK) ON cr.creditor_class = ccl.creditor_class
where	cc.sched_payment > isnull(bal.orig_balance,0) + isnull(bal.orig_balance_adjustment,0) + isnull(bal.total_interest,0) - isnull(bal.total_payments,0)
and	isnull(ccl.zero_balance,0) = 0
and	cc.client = @client;

-- Ensure that the amount is never negative
update	client_creditor
set	sched_payment = 0
where	sched_payment < 0
and	client = @client;

-- Ensure that the "invalid" debts have no disbursement
update	client_creditor
set	sched_payment = 0
where	client = @client
and	isnull(reassigned_debt,0) <> 0;

update	client_creditor
set	sched_payment = 0
where	client = @client
and	creditor is null;

commit transaction
return ( 0 )
GO
