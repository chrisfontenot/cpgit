USE [DebtPlus]
GO
/****** Object:  StoredProcedure [dbo].[xpr_insert_registers_invoice]    Script Date: 09/15/2014 13:13:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[xpr_insert_registers_invoice] ( @creditor varchar(10), @inv_amount money = 0, @inv_date datetime = null, @adj_amount money = 0, @adj_date datetime = null, @pmt_amount money = 0, @pmt_date datetime = null, @date_printed datetime = null) as
-- =========================================================================================================
-- ==           Create an invoice record                                                                  ==
-- =========================================================================================================
insert into registers_invoices ( [creditor], [inv_amount], [inv_date],                   [adj_amount], [adj_date], [pmt_amount], [pmt_date], [date_printed])
values                         ( @creditor,  @inv_amount,  isnull(@inv_date,getdate()),  @adj_amount,  @adj_date,  @pmt_amount,  @pmt_date,  @date_printed )
return ( scope_identity() )
GO
