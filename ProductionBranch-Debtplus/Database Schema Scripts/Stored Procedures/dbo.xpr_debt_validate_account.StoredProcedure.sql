USE [DebtPlus]
GO
/****** Object:  StoredProcedure [dbo].[xpr_debt_validate_account]    Script Date: 09/15/2014 13:13:35 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE PROCEDURE [dbo].[xpr_debt_validate_account] ( @creditor as typ_creditor, @account_number as varchar(80), @client as int = null ) as

-- ==========================================================================================================
-- ==            Validate the account number against the list of accounts for this creditor method         ==
-- ==========================================================================================================

-- ChangeLog
--   1/5/2009
--      Changed from using biller_aba to using rpps_biller_id

-- Suppress intermediate result sets
set nocount on

declare	@creditor_id		int
declare	@creditor_method	int
declare	@checkdigit			int
declare	@rpps_mask		varchar(80)

-- Find the creditor ID from the creditors table
select	@creditor_id	= creditor_id
from	creditors with (nolock)
where	creditor	= @creditor

-- If there is no id then the creditor does not exist
if @creditor_id is null
begin
	raiserror ('The creditor is not valid in the system', 16, 1)
	return ( 0 )
end

-- Find the method for the RPPS payments
select	@creditor_method	= mt.creditor_method,
	@checkdigit		= ids.[checkdigit]
from		creditor_methods mt with (nolock)
inner join	banks b with (nolock) on mt.bank = b.bank and 'R' = b.type
inner join	rpps_biller_ids ids with (nolock) on mt.rpps_biller_id = ids.rpps_biller_id
inner join	rpps_masks m with (nolock) on ids.rpps_biller_id = m.rpps_biller_id
where		mt.type		= 'EFT'
and		mt.creditor	= @creditor_id
and		@account_number like dbo.map_rpps_masks ( m.mask )
and		m.length	= len(@account_number)

-- IF there is no match then there must not be an RPPS biller payment for this creditor
if @creditor_method is null
begin
	select top 1
		@creditor_method	= mt.creditor_method,
		@rpps_mask		= m.mask
	from		creditor_methods mt with (nolock)
	inner join	banks b with (nolock) on mt.bank = b.bank and 'R' = b.type
	inner join	rpps_biller_ids ids with (nolock) on mt.rpps_biller_id = ids.rpps_biller_id
	inner join	rpps_masks m with (nolock) on ids.rpps_biller_id = m.rpps_biller_id
	where		mt.type		= 'EFT'
	and		mt.creditor	= @creditor_id
	order by m.length desc

	if @creditor_method is not null
	begin
		declare	@error_message	varchar(128)
		select	@error_message = 'The account number does not match the creditor mask of ' + isnull(@rpps_mask,'')
		RaisError ( @error_message, 16, 1 )
		return ( 0 )
	end
end

if @creditor_method is not null and @checkdigit <> 0
begin
	-- Validate the checksum at this point
	if dbo.valid_checksum_luhn ( @account_number ) = 0
	begin
		RaisError ('The checksum does not match the expected value', 16, 1)
		return ( 0 )
	end
end

return ( 1 )
GO
