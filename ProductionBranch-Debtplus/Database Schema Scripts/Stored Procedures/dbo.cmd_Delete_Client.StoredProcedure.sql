USE [DebtPlus]
GO
/****** Object:  StoredProcedure [dbo].[cmd_Delete_Client]    Script Date: 09/15/2014 13:13:31 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[cmd_Delete_Client] ( @Client AS INT ) AS
-- ==========================================================================
-- ==               Delete a Client                                        ==
-- ==========================================================================

-- ChangeLog
--   06/09/2009
--      Remove references to debt_number and use client_creditor

if not exists (select * from clients where client = @client)
begin
	RaisError (50014, 11, 1, @client)
	return ( 0 )
end

-- Stop intermediate result sets
set nocount on
begin transaction
set xact_abort on

-- Transfer any money to the encheit account
declare	@held_in_trust	money
select	@held_in_trust = held_in_trust
from	clients
where	client = @client

if @held_in_trust is null
	select @held_in_trust = 0

if @held_in_trust != 0
begin
	-- Determine the primary account name
	declare	@name		varchar(80)
	select	@name = left(dbo.format_normal_name (pn.prefix, pn.first, pn.middle, pn.last, pn.suffix),80)
	from	people p
	left outer join names pn on p.NameID = pn.Name
	where	client	= @client
	and		relation  = 1;

	declare	@ledger_code	varchar(80)
	select	@ledger_code = ledger_code
	from	ledger_codes
	where	description like '%encheit%';

	if @ledger_code is null
	begin
		select @ledger_code = '0100-0000';
		insert into ledger_codes (ledger_code, description)
		values ('0100-0000', 'Client encheit account')
	end

	declare	@non_ar_source	int
	select	@non_ar_source = non_ar_source
	from	non_ar_sources
	where	description like '%deleted client funds%'

	if @non_ar_source is null
	begin
		insert into non_ar_sources (description) values ('DELETED CLIENT FUNDS')
		select @non_ar_source = SCOPE_IDENTITY();
	end

	insert into registers_non_ar (tran_type, non_ar_source, credit_amt, reference, dst_ledger_account, message)
	select 'NA', @non_ar_source, @held_in_trust, dbo.format_client_id ( @client), @ledger_code, 'Transferred funds from client trust balance to encheit account because the client was deleted. The client was ' + dbo.format_client_id ( @client ) + isnull(' '+@name,'') + '.'

	update	clients
	set	held_in_trust = 0
	where	client = @client
end

-- Remove the disbursement notes
delete from disbursement_notes where client = @Client

-- Remove the budget information
delete
from	budget_categories_other
where	client = @client

delete	budget_detail
from	budget_detail d
inner join budgets b on d.budget = b.budget
where	b.client = @client

delete
from	budgets
where	client = @client

-- Remove ticklers
delete
from	ticklers
where	client = @client

-- "Un-import" the WWW client
update	intake_clients
set	client		= null,
	date_imported	= null,
	imported_by	= null
where	client		= @client

-- Remove the www access information
delete client_www_notes
from   client_www_notes n
inner join client_www w on n.pkid = w.pkid
where w.UserName = convert(varchar, @client)
and   w.ApplicationName = '/'

delete	client_www
from	client_www w
where w.UserName = convert(varchar, @client)
and   w.ApplicationName = '/'

-- Remove the deposit items
delete
from	client_deposits
where	client = @Client

-- Remove the appointments
delete
from	client_appointments
where	client = @client

-- Remove the action plans
delete from client_action_items	where action_plan in (select action_plan from action_plans where client = @client)
delete from action_items_other	where action_plan IN (select action_plan from action_plans where client = @client)
delete from action_items_goals	where action_plan IN (select action_plan from action_plans where client = @client)
delete from action_plans	where client = @client

-- Remove the assets
delete from assets where client = @client

-- remove the secured account information
delete	secured_loans
from	secured_loans l
inner join secured_properties p on l.secured_property = p.secured_property
where	p.client	= @client

delete
from	secured_properties
where	client		= @client

-- remove the housing information
delete
from	hud_loans
where	client	= @client

delete
from	hud_transactions
where	hud_interview in (
	select	hud_interview
	from	hud_interviews
	where	client	= @client
)

delete
from	hud_interviews
where	client	= @client

delete
from	client_housing
where	client = @client

-- remove the additional keys
delete from client_addkeys where client = @client

-- remove the proposal information
delete from client_creditor_proposals where client_creditor in (select client_creditor from client_creditor where client = @client)

-- Remove the client/creditor notes
delete from client_notes where client_creditor in (select client_creditor from client_creditor where client = @client)

-- remove the retention information
delete from client_retention_actions where client_retention_event in (
	select	client_retention_event
	from	client_retention_events
	where	client = @client
)
delete from client_retention_events where client = @client

-- remove the debt events
delete from client_creditor_events where client_creditor in (
	select	client_creditor
	from	client_creditor
	where	client = @client
)

-- Remove the balance information
delete
from	client_creditor_balances
where	client_creditor in (
	select	client_creditor
	from	client_creditor
	where	client = @client
)

-- remove the debt information
delete from client_creditor where client = @client

-- remove the personal information
delete from people where client = @client

-- Remove the check register linkage
update registers_trust set client = Null where client = @client

-- Remove the client/creditor transactions
delete from	registers_client_creditor where	client = @client

-- Remove the client transactions
delete from registers_client where client = @client

-- Remove the non-ar transactions
update registers_non_ar set client = null where client = @client

-- delete the disbursement notes
delete from disbursement_notes where client = @client

-- remove the rpps transactions
delete from rpps_transactions where client = @client

-- remove the client notes
delete from client_notes where client = @client

-- Remove the sales debts
delete
from	sales_debts
where	sales_file in (
	select	sales_file
	from	sales_files
	where	client	= @client
)

delete
from	sales_files
where	client	= @client

-- remove the client
delete from clients where client = @client

-- Make the transaction valid
commit transaction
return ( 1 )
GO
