USE [DebtPlus]
GO
/****** Object:  StoredProcedure [dbo].[www_client_contact_iframe]    Script Date: 09/15/2014 13:13:34 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE PROCEDURE [dbo].[www_client_contact_iframe] as
-- ====================================================================================
-- ==            Return the list of offices for the agency                           ==
-- ====================================================================================
-- Suppress intermediate result sets
set nocount on

-- Return the first recordset as HTML code to be included prior to the list of offices
SELECT	'Our general office hours are from 7:30am to 6pm, Monday through Thursday and 7:30am to 5pm on Friday.'
UNION ALL
SELECT	'Some of our offices are open on Saturday, and typically our main office is open on Saturday.'
UNION ALL
SELECT	'We observe the holidays of New Year''s day, Vetran''s Day, Memorial Day, Thanksgiving, and Christmas. Our offices will be closed on those days, or on the following business day.'

-- Follow it by the recordset with the office names and address information
SELECT	name			as 'name',
	addr_1			as 'addr_1',
	addr_2			as 'addr_2',
	addr_3			as 'addr_3',
	phone			as 'phone',
	fax			as 'fax',
	directions		as 'directions'
FROM	view_offices with (nolock)
ORDER BY office
return ( @@rowcount )
GO
