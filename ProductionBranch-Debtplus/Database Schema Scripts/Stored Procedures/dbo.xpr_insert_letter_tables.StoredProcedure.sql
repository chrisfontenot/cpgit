USE [DebtPlus]
GO
/****** Object:  StoredProcedure [dbo].[xpr_insert_letter_tables]    Script Date: 09/15/2014 13:13:35 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[xpr_insert_letter_tables] ( @query_type as int = 1, @query as varchar(256) = '' ) as
-- ========================================================================================
-- ==   Procedure for .NET to insert items into the letter_tables table                  ==
-- ========================================================================================
	insert into letter_tables ( [query_type], [query] ) values ( @query_type, @query )
	return ( scope_identity() )
GO
