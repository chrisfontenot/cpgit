USE [DebtPlus]
GO
/****** Object:  StoredProcedure [dbo].[MONTHLY_office_statistics]    Script Date: 09/15/2014 13:13:32 ******/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE PROCEDURE [dbo].[MONTHLY_office_statistics] AS

-- ChangeLog
--    8/1/2002
--       Extracted from MONTHLY_client_creditor
--   9/31/2002
--     Added support for client_creditor_balances table
--   4/2/2003
--     Rewrite to use registers_client_creditor information to avoid problems with reassigned debts.

-- Create transaction to update the office statistics
BEGIN TRANSACTION
SET NOCOUNT ON

-- Find the date range for the report
declare	@FromDate	datetime
declare	@ToDate		datetime

select	@ToDate		= getdate()
select	@FromDate	= convert(datetime, convert(varchar,month(@ToDate)) + '/01/' + convert(varchar,year(@ToDate)) + ' 00:00:00')

-- If this is the first of the month then use the last month figures
if day( @ToDate ) = 1
	select	@FromDate	= dateadd(month, -1, @FromDate)

select	@ToDate		= dateadd(m, 1, @FromDate)
select	@ToDate		= dateadd(s, -1, @ToDate)

-- Create a working table for the current period
create table #new_records (office int null, active_clients int null, clients_disbursed int null, expected_disbursement money null, actual_disbursement money null, paf_fees money null);

-- Determine the number of active clients in each office
insert into #new_records (office, active_clients, expected_disbursement, actual_disbursement)
select	isnull(office,0) as office, count(*), 0, 0
from	clients
where	active_status in ('A', 'AR')
group by isnull(office,0);

-- Update the expected disbursements for the clients
select		isnull(c.office,0) as office, sum(isnull(bal.current_sched_payment,0)) as sched_payment
into		#sched_payment
from		client_creditor_balances bal	with (nolock)
inner join client_creditor cc			with (nolock) on bal.client_creditor = cc.client_creditor
inner join clients c				with (nolock) on cc.client = c.client
where c.active_status in ('A', 'AR')
group by isnull(c.office,0);

update	#new_records
set	expected_disbursement = x.sched_payment
from	#sched_payment x
inner join #new_records y on x.office = y.office;

-- Retrieve the creditor for the P.A.F. amounts
declare	@paf_creditor		varchar(10)
declare	@deduct_creditor	varchar(10)
select	@paf_creditor		= paf_creditor,
	@deduct_creditor	= deduct_creditor
from	config;

if @deduct_creditor is null
	select	@deduct_creditor	= ''

if @paf_creditor is null
	select	@paf_creditor		= @deduct_creditor
else begin
	-- Calculate the PAF fees collected
	select	isnull(c.office,0) as office, sum(isnull(rcc.debit_amt,0) - isnull(rcc.credit_amt,0)) as paf_fees
	into	#paf_fees
	from	registers_client_creditor rcc with (nolock)
	inner join clients c with (nolock) on rcc.client = c.client
	where	rcc.date_created between @FromDate and @ToDate
	and	rcc.creditor = @paf_creditor
	and	rcc.tran_type in ('AD','BW','MD','CM','RF','RR','VD')
	group by isnull(c.office,0);

	update	#new_records
	set	paf_fees = x.paf_fees
	from	#paf_fees x
	inner join #new_records y on x.office = y.office;
end

-- Find the payments to the non-PAF creditors
select	isnull(c.office,0) as office, sum(isnull(rcc.debit_amt,0) - isnull(rcc.credit_amt,0)) as disbursed
into	#dollars_disbursed
from	registers_client_creditor rcc	with (nolock)
inner join clients c			with (nolock) on rcc.client = c.client
where	rcc.date_created between @FromDate and @ToDate
and	rcc.creditor not in (@paf_creditor, @deduct_creditor)
and	rcc.tran_type in ('AD','BW','MD','CM','RF','RR','VD')
and	c.client > 0
group by isnull(c.office,0);

-- Find the count of clients disbursed
select	distinct isnull(c.office,0) as office, c.client
into	#clients_disbursed_raw
from	registers_client_creditor rcc	with (nolock)
inner join clients c			with (nolock) on rcc.client = c.client
where	rcc.date_created between @FromDate and @ToDate
and	rcc.creditor not in (@paf_creditor, @deduct_creditor)
and	rcc.tran_type in ('AD','BW','MD','CM')
and	c.client > 0
and	rcc.debit_amt > 0
group by isnull(c.office,0), c.client;

select	office, count(*) as cnt
into	#clients_disbursed
from	#clients_disbursed_raw
group by office;

drop table #clients_disbursed_raw

-- Update the working table with the results
update	#new_records
set	actual_disbursement = x.disbursed
from	#dollars_disbursed x
inner join #new_records y on x.office = y.office;

update	#new_records
set	clients_disbursed = x.cnt
from	#clients_disbursed x
inner join #new_records y on x.office = y.office;

-- Remove any extra items from testing
delete
from	statistics_offices
where	period_start = @FromDate;

-- Insert the new statistics into the table
insert into statistics_offices (period_start, office, active_clients, clients_disbursed, expected_disbursement, actual_disbursement, paf_fees)
select	@FromDate, office, isnull(active_clients,0), isnull(clients_disbursed,0), isnull(expected_disbursement,0), isnull(actual_disbursement,0), isnull(paf_fees,0)
from	#new_records;

-- Discard the working tables
drop table #clients_disbursed;
drop table #dollars_disbursed;
drop table #new_records;
drop table #paf_fees;
drop table #sched_payment;

COMMIT TRANSACTION
return ( 1 )
GO
