USE [DebtPlus]
GO
/****** Object:  StoredProcedure [dbo].[xpr_rpps_response_process_CDA]    Script Date: 09/15/2014 13:13:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[xpr_rpps_response_process_CDA] ( @rpps_response_file as int ) AS
-- ====================================================================================================================
-- ==            Mark the proposal information as being accepted                                                     ==
-- ====================================================================================================================

-- ChangeLog
--   5/11/2012
--     Added support for additional fields from Mastercard in their "error code cleanup"

-- Find the proposal from the trace table
begin transaction
set nocount on
set xact_abort on

-- Find the proposals that are being accepted
select	pr.client_creditor_proposal,
		d.rpps_response_detail,
		t.rpps_transaction,
		d.processing_error,
		d.trace_number,
		x.payment_amount,
		x.start_date
into	#rpps_response_cda
from	rpps_response_details d
inner join rpps_response_details_cda x on d.rpps_response_detail = x.rpps_response_detail
inner join rpps_transactions t on d.rpps_transaction = t.rpps_transaction
inner join client_creditor_proposals pr on t.client_creditor_proposal = pr.client_creditor_proposal
where	d.rpps_response_file		= @rpps_response_file
and		d.processing_error			is null
and		d.service_class_or_purpose	= 'CDA'
and		t.service_class_or_purpose	= 'CDP'

-- Mark all other proposals as "unknown"
update	rpps_response_details
set		processing_error		= 'UNKNOWN PROPOSAL'
where	rpps_response_file		= @rpps_response_file
and		service_class_or_purpose	= 'CDA'
and		processing_error is null
and		rpps_response_detail not in (
		select	rpps_response_detail
		from	#rpps_response_cda
);

-- Do nothing more if there are no proposals to be processed
if not exists (select * from #rpps_response_cda)
begin
	drop table #rpps_response_cda
	commit transaction
	return ( 1 )
end

-- Mark the transactions as being accepted
update	rpps_transactions
set	return_code = 'CDA'
where	rpps_transaction in (
	select	rpps_transaction
	from	#rpps_response_cda
);

-- Mark the proposals as accepted
update	client_creditor_proposals
set		proposal_status		= 3,
		proposal_status_date	= getdate(),
		proposal_accepted_by	= 'RPPS SYSTEM'
where	client_creditor_proposal in (
		select	client_creditor_proposal
		from	#rpps_response_cda
);

-- Insert a system note for the transactions
insert into client_notes (client, client_creditor, is_text, type, dont_edit, dont_delete, dont_print, subject, note)
select		cc.client,
			cc.client_creditor,
			1 as 'is_text',
			3 as 'type',
			1 as 'dont_edit',
			1 as 'dont_delete',
			0 as 'dont_print',
			'Proposal Accepted by RPPS System' as 'subject',

			'The proposal dated ' + isnull(convert(varchar(10), b.date_closed, 101), '??/??/????') + ' was accepted by the creditor.' + char(13) + char(10) +
			'The proposed amount was $' + convert(varchar, p.proposed_amount, 1) + char(13) + char(10) +
			isnull('The accepted amount was $' + convert(varchar, x.payment_amount, 1) + char(13) + char(10),'') +
			'The proposed balance was $' + convert(varchar, p.proposed_balance, 1) + char(13) + char(10) +
			'The proposed start date was ' + convert(varchar(10), p.proposed_start_date, 101) + char(13) + char(10) +
			isnull('The accepted start date was ' + convert(varchar(10), x.start_date, 101) + char(13) + char(10),'') +
			char(13) + char(10) +
			'The proposal was sent out in batch number ' + convert(varchar, p.proposal_batch_id) + ' dated ' + convert(varchar(10), b.date_created, 101) + char(13) + char(10) +
			'It was accepted in the batch ' + convert(varchar, @rpps_response_file) + char(13) + char(10) +
			'The batch was processed by ' + suser_sname()

from		client_creditor_proposals p
inner join	#rpps_response_cda x on p.client_creditor_proposal = x.client_creditor_proposal
inner join	client_creditor cc on p.client_creditor = cc.client_creditor
left outer join	proposal_batch_ids b on p.proposal_batch_id = b.proposal_batch_id
	
-- Cleanup
drop table #rpps_response_cda
commit transaction
return ( 1 )
GO
