SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER procedure [rpt_TrustRegister] ( @FromDate AS DateTime = NULL, @ToDate AS DateTime = NULL ) AS
-- ===================================================================================================
-- ==                Generate a listing showing the detail information for the request              ==
-- ===================================================================================================

IF @ToDate IS NULL
	SET @ToDate = getdate()

IF @FromDate IS NULL
	SET @FromDate = @ToDate

-- Adjust the dates for the proper settings
SET @FromDate	= convert(datetime, convert(varchar(10), @FromDate, 101) + ' 00:00:00')
SET @ToDate	= convert(datetime, convert(varchar(10), @ToDate, 101)   + ' 23:59:59')

-- Return the items from the trust register
SELECT		*
FROM		view_trust_register
WHERE		date_created BETWEEN @FromDate AND @ToDate
ORDER BY	checknum, date_created, trust_register

RETURN ( @@rowcount )
GO
