USE [DebtPlus]
GO
/****** Object:  StoredProcedure [dbo].[rpt_Recon_Summary]    Script Date: 09/15/2014 13:13:33 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE PROCEDURE [dbo].[rpt_Recon_Summary] ( @cutoff_date as datetime ) AS

-- ==================================================================================================
-- ==            Retrieve the summary information for the reconcilation report                     ==
-- ==================================================================================================

-- ChangeLog
--   3/22/2002
--     Added RR to the list of deposit transaction types

-- Remove the time from the date/time item
set @cutoff_date = convert(datetime, convert(varchar(10), @cutoff_date, 101) + ' 00:00:00')

select
	-- Reconciled deposits after the cutoff date
	sum (case
		when tran_type in ('DP', 'BI', 'RF', 'RR') and cleared IN ('R', 'V', 'D')	and date_created > @cutoff_date		then amount
		else 0
	end) as dp_c_post_amt,

	count (case
		when tran_type in ('DP', 'BI', 'RF', 'RR') and cleared IN ('R', 'V', 'D')	and date_created > @cutoff_date		then amount
		else null
	end) as dp_c_post_cnt,

	-- Reconciled deposits before the cutoff date
	sum (case
		when tran_type in ('DP', 'BI', 'RF', 'RR') and cleared IN ('R', 'V', 'D')	and date_created <= @cutoff_date	then amount
		else 0
	end) as dp_c_pre_amt,

	count (case
		when tran_type in ('DP', 'BI', 'RF', 'RR') and cleared IN ('R', 'V', 'D')	and date_created <= @cutoff_date	then amount
		else null
	end) as dp_c_pre_cnt,

	-- UnReconciled deposits after the cutoff date
	sum (case
		when tran_type in ('DP', 'BI', 'RF', 'RR') and cleared NOT IN ('R', 'V', 'D')	and date_created > @cutoff_date		then amount
		else 0
	end) as dp_nc_post_amt,

	count (case
		when tran_type in ('DP', 'BI', 'RF', 'RR') and cleared NOT IN ('R', 'V', 'D')	and date_created > @cutoff_date		then amount
		else null
	end) as dp_nc_post_cnt,

	-- UnReconciled deposits before the cutoff date
	sum(case
		when tran_type in ('DP', 'BI', 'RF', 'RR') and cleared NOT IN ('R', 'V', 'D')	and date_created <= @cutoff_date	then amount
		else 0
	end) as dp_nc_pre_amt,

	count(case
		when tran_type in ('DP', 'BI', 'RF', 'RR') and cleared NOT IN ('R', 'V', 'D')	and date_created <= @cutoff_date	then amount
		else null
	end) as dp_nc_pre_cnt,

	-- Reconciled checks after the cutoff date
	sum(case
		when not tran_type in ('DP', 'BI', 'RF', 'RR') and cleared IN ('R', 'V', 'D')	and date_created > @cutoff_date		then amount
		else 0
	end) as ck_c_post_amt,

	count(case
		when not tran_type in ('DP', 'BI', 'RF', 'RR') and cleared IN ('R', 'V', 'D')	and date_created > @cutoff_date		then amount
		else null
	end) as ck_c_post_cnt,

	-- Reconciled checks before the cutoff date
	sum(case
		when not tran_type in ('DP', 'BI', 'RF', 'RR') and cleared IN ('R', 'V', 'D')	and date_created <= @cutoff_date	then amount
		else 0
	end) as ck_c_pre_amt,

	count(case
		when not tran_type in ('DP', 'BI', 'RF', 'RR') and cleared IN ('R', 'V', 'D')	and date_created <= @cutoff_date	then amount
		else null
	end) as ck_c_pre_cnt,

	-- UnReconciled checks after the cutoff date
	sum(case
		when not tran_type in ('DP', 'BI', 'RF', 'RR') and cleared NOT IN ('R', 'V', 'D')	and date_created > @cutoff_date		then amount
		else 0
	end) as ck_nc_post_amt,

	count(case
		when not tran_type in ('DP', 'BI', 'RF', 'RR') and cleared NOT IN ('R', 'V', 'D')	and date_created > @cutoff_date		then amount
		else null
	end) as ck_nc_post_cnt,

	-- UnReconciled checks before the cutoff date
	sum(case
		when not tran_type in ('DP', 'BI', 'RF', 'RR') and cleared NOT IN ('R', 'V', 'D')	and date_created <= @cutoff_date	then amount
		else 0
	end) as ck_nc_pre_amt,

	count(case
		when not tran_type in ('DP', 'BI', 'RF', 'RR') and cleared NOT IN ('R', 'V', 'D')	and date_created <= @cutoff_date	then amount
		else null
	end) as ck_nc_pre_cnt

from	registers_trust with (nolock)
where	cleared in (' ', 'P')
or	date_created >= @cutoff_date

return ( @@rowcount )
GO
