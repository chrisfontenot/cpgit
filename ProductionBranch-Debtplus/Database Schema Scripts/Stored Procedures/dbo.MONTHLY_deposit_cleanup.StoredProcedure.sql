USE [DebtPlus]
GO
/****** Object:  StoredProcedure [dbo].[MONTHLY_deposit_cleanup]    Script Date: 09/15/2014 13:13:32 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[MONTHLY_deposit_cleanup] AS

-- ==================================================================================================
-- ==            Remove obsolete deposit batches from the system                                   ==
-- ==================================================================================================

begin transaction
set xact_abort on
CREATE table #cleanup_batch_ids ( deposit_batch_id int );

-- Discard the deposit transactions from the files which have been posted over 90 days ago
-- The "registers_client" has all of this information. The deposit_* tables are only "working" values that hold the
-- current in-process deposit batch data. Once the batch has been posted, there is no need for the duplicate items other
-- than a possibility of history. The "real" information is in the other tables, including everything that is in these
-- items.

insert into #cleanup_batch_ids ( deposit_batch_id )
select	deposit_batch_id
from	deposit_batch_ids
where	date_posted < dateadd(d, -90, getdate())
AND	batch_type != 'AC';

-- Discard the ACH transactions over 45 days old. This is required for the transaction trace numbers.
insert into #cleanup_batch_ids ( deposit_batch_id )
select	deposit_batch_id
from	deposit_batch_ids
where	date_posted < dateadd(d, -45, getdate())
AND	batch_type = 'AC';

-- Drop the ACH transactions which have been created over 95 days ago. These should have been posted but never made it.
insert into #cleanup_batch_ids ( deposit_batch_id )
select	deposit_batch_id
from	deposit_batch_ids
where	date_created < dateadd(d, -95, getdate())
AND	batch_type = 'AC';

-- Discard the transactions
delete
from	deposit_batch_details
where	deposit_batch_id in (
	select	deposit_batch_id
	from	#cleanup_batch_ids
);

-- Discard the deposit batch IDs
delete
from	deposit_batch_ids
where	deposit_batch_id in (
	select	deposit_batch_id
	from	#cleanup_batch_ids
);

drop table #cleanup_batch_ids
commit transaction

-- Return success
return ( 1 )
GO
