USE [DebtPlus]
GO
/****** Object:  StoredProcedure [dbo].[www_client_resources]    Script Date: 09/15/2014 13:13:34 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[www_client_resources] (@zipcode as varchar(50) = NULL, @types as varchar(256) = NULL, @client as int = NULL) AS
-- =================================================================================
-- ==    Select the information needed to generate the Resource Guide Report      ==
-- =================================================================================
declare	@office		int
declare	@return_value	int

-- If an office is supplied then limit the resources to the office
select	@zipcode = isnull(@zipcode,'')

-- Find the indicated range for the zipcode
if @zipcode = ''
	select @office = null
else begin
	SET ROWCOUNT 1
	SELECT	@office = office
	FROM	zipcodes
	WHERE	@zipcode BETWEEN zip_lower AND zip_upper
	ORDER BY (convert ( int, zip_upper ) - convert ( int,  zip_lower ))
	SET ROWCOUNT 0

	-- If there is no office then reset the office to null
	if @office <= 0
		select	@office = null
end

-- Remove extra spaces from the type list.
if @types is not null
begin
	select	@types = ltrim(rtrim(@types))
	if @types = ''
		select @types = null
end

execute @return_value = rpt_ResourceGuide @office, @types
return ( @return_value )
GO
