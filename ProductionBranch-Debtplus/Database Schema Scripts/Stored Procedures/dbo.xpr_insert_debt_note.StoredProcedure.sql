USE [DebtPlus]
GO
/****** Object:  StoredProcedure [dbo].[xpr_insert_debt_note]    Script Date: 09/15/2014 13:13:35 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[xpr_insert_debt_note] ( @client_creditor int, @type varchar(2) = 'AN', @note_text text = '', @note_amount money = 0, @note_date datetime = null ) as

set nocount on

declare	@client				int
declare @creditor			varchar(10)
declare @creditor_method	int
declare	@bank				int
declare	@rpps_biller_id		varchar(20)
declare	@epay_biller_id		varchar(80)

if @note_date is null
	select @note_date = GETDATE()
	
if @note_amount is null
	select @note_amount = 0
	
if isnull(@client_creditor,0) > 0
begin
	declare	@creditor_id			int
	select	@client					= client,
			@creditor				= creditor
	from	client_creditor with (nolock)
	where	client_creditor = @client_creditor;
	
	if @creditor is not null
	begin
		select	@creditor_id		= creditor_id
		from	creditors with (nolock)
		where	creditor			= @creditor;
		
		if isnull(@creditor_id,0) > 0
		begin
			select	@creditor_method	= cm.creditor_method,
					@bank				= cm.bank,
					@rpps_biller_id		= cm.rpps_biller_id,
					@epay_biller_id		= cm.epay_biller_id
			from	creditor_methods cm with (nolock)
			inner join banks b with (nolock) on cm.bank = b.bank
			where	creditor			= @creditor_id
			AND		cm.[TYPE]			= 'MSG'
			AND		b.type				in ('R', 'E')
			
			if @creditor_method is null
				select	@creditor_method	= cm.creditor_method,
						@bank				= cm.bank
				from	creditor_methods cm with (nolock)
				inner join banks b with (nolock) on cm.bank = b.bank
				where	creditor			= @creditor_id
				AND		cm.[TYPE]			= 'MSG'
		end
	end
end
	
insert into debt_notes([client_creditor], [client], [creditor], [bank], [creditor_method], [rpps_biller_id], [epay_biller_id], [type], [note_text], [note_date], [note_amount]) values (@client_creditor, @client, @creditor, @bank, @creditor_method, @rpps_biller_id, @epay_biller_id, @type, @note_text, @note_date, @note_amount)
return ( scope_identity() )
GO
