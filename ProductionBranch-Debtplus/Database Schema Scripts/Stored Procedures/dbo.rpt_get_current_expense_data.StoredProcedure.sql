USE [DebtPlus]
GO
/****** Object:  StoredProcedure [dbo].[rpt_get_current_expense_data]    Script Date: 09/15/2014 13:13:33 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[rpt_get_current_expense_data](
	@client int
) as
begin

	declare @budget int 

	select @budget = budget FROM budgets WITH (NOLOCK) WHERE client=@client

	SELECT bc.budget_category, bc.description, bc.auto_factor, bc.person_factor, bc.maximum_factor, bc.heading, bc.detail, bc.living_expense, bc.housing_expense, bc.rpps_code, convert(money,case when isnull(bc.detail,1) = 0 then null else isnull(bd.client_amount,0) end) as client_amount, convert(money,case when isnull(bc.detail,1) = 0 then null else isnull(bd.suggested_amount,0) end) as suggested_amount, bd.date_created, bd.created_by 
	 from budget_categories bc 
	left outer join budget_detail bd on bc.budget_category = bd.budget_category and bd.budget = @budget 
	union all 
	SELECT bco.budget_category, bco.description, null as auto_factor, null as person_factor, null as maximum_factor, 0 as heading, 1 as detail, 1 as living_expense, 0 as housing_expense, 6 as rpps_code, isnull(bd.client_amount,0) as client_amount, isnull(bd.suggested_amount,0) as suggested_amount, bd.date_created, bd.created_by 
	  from budget_categories_other bco 
	left outer join budget_detail bd on bco.budget_category = bd.budget_category and bd.budget = @budget 
	 where bco.client = @client order by 1
end
GO
