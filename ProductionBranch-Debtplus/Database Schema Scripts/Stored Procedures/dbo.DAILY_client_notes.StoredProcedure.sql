USE [DebtPlus]
GO
/****** Object:  StoredProcedure [dbo].[DAILY_client_notes]    Script Date: 09/15/2014 13:13:32 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE PROCEDURE [dbo].[DAILY_client_notes] AS
-- ======================================================================================
-- ==             Remove expired temporary client notes from the system                ==
-- ======================================================================================
BEGIN TRANSACTION
SET NOCOUNT ON

-- Turn client alert notes into permanent notes when they expire
UPDATE	client_notes
SET	type		= 1,
	expires		= null
WHERE	type		= 4
AND	expires IS NOT NULL
AND	expires <= getdate()

-- Delete temporary client notes when they expire
DELETE
FROM	client_notes
WHERE	type		= 2
AND	expires IS NOT NULL
AND	(expires <= getdate())

COMMIT TRANSACTION
GO
