IF EXISTS(SELECT * FROM SYSOBJECTS WHERE TYPE='P' AND NAME=N'xpr_deposit_batch_post')
    EXEC('DROP PROCEDURE xpr_deposit_batch_post')
GO
CREATE PROCEDURE [dbo].[xpr_deposit_batch_post] ( @deposit_batch_id AS INT ) AS

-- ==================================================================================================
-- ==               Post the deposit batch                                                        ==
-- ==================================================================================================

-- Start a transaction
BEGIN TRANSACTION
SET XACT_ABORT ON
SET NOCOUNT ON

BEGIN TRY
	DECLARE	@bank				int
	DECLARE	@Client_Count		int
	DECLARE	@trust_register		int
	DECLARE	@product_count		int
	DECLARE	@date_closed		datetime
	DECLARE	@date_posted		datetime
	DECLARE	@date_created		datetime
	DECLARE	@Deposit_Amount		money
	DECLARE	@note				varchar(50)
	DECLARE	@deposit_batch_type	varchar(80)

	SET		@client_count   = 0
	SET		@Deposit_Amount = 0
	SET		@product_count  = 0
	SET		@trust_register	= -1

	SELECT	@date_created		= date_created,
			@date_closed		= date_closed,
			@date_posted		= date_posted,
			@bank				= bank,
			@note				= note,
			@deposit_batch_type	= batch_type
	FROM	deposit_batch_ids with (nolock)
	WHERE	deposit_batch_id	= @deposit_batch_id
	AND		batch_type		in ('AC', 'CL')

	IF @date_created IS NULL
		RaisError(50006, 16, 1, @deposit_batch_id)

	IF @date_posted IS NOT NULL
	BEGIN
		declare	@p1	varchar(10)
		SELECT	@p1 = convert(varchar(10), @date_posted, 1)
		RaisError(50083, 16, 1, @p1)
	END

	IF @date_closed IS NULL
		RaisError(50082, 16, 1, @deposit_batch_id)

	-- Correct the label for the subsequent update of the registers_trust table
	if @note is null
		select	@note = ''

	select	@note = ltrim(rtrim(@note))
	if @note <> ''
	begin
		select	@note = '#' + convert(varchar, @deposit_batch_id) + ' ''' + @note
		if len(@note) > 49
			select	@note = left(@note, 49)
		select	@note = @note + ''''
	end else
		select	@note = '#' + convert(varchar, @deposit_batch_id)

	-- Reset all deposit valid items if the client does not exist. It is never, never valid to deposit items to non-existent clients.
	UPDATE		deposit_batch_details
	SET			ok_to_post = 0
	FROM		deposit_batch_details d
	LEFT OUTER JOIN clients c WITH (NOLOCK) on d.client = c.client
	WHERE		d.deposit_batch_id = @deposit_batch_id
	AND			(c.client IS NULL OR d.date_posted IS NOT NULL)

	-- Force the transactions as "not ok to post" if there is an error on the ACH transaction
	if @deposit_batch_type = 'AC'
	begin
		update		deposit_batch_details
		set			ok_to_post = 0
		from		deposit_batch_details d
		where		d.deposit_batch_id = @deposit_batch_id
		and			ach_authentication_code is not null
	end

	-- Process a product transaction group first
	SELECT		d.deposit_batch_detail, d.client_product, convert(datetime, convert(varchar(10), d.date_created, 101) + ' 00:00:00') as date_created, d.amount
	INTO		#t_product_by_date
	FROM		deposit_batch_details d
	INNER JOIN	client_products p on d.client_product = p.oID
	WHERE		d.deposit_batch_id = @deposit_batch_id
	AND			d.ok_to_post = 1
	AND			p.invoice_status = 3
	AND			d.date_posted IS NULL

	-- Find the statistic information
	select		@product_count = count(*),
				@Deposit_Amount = sum(amount)
	from		#t_product_by_date

	IF @product_count > 0
	BEGIN
		-- Update the tendered amount for the various items
		update		client_products
		set			tendered = tendered + amount,
					invoice_status = 4
		from		client_products p
		inner join	#t_product_by_date x on p.oID = x.client_product

		-- Generate the detail transaction for the items
		insert into product_transactions (transaction_type, client_product, vendor, product, payment)
		select		'RC', x.client_product, p.vendor, p.product, x.amount
		from		#t_product_by_date x
		inner join	client_products p on x.client_product = p.oID

		-- Mark the items as having been posted
		update		deposit_batch_details
		set			date_posted			= getdate()
		from		deposit_batch_details d
		inner join #t_product_by_date x on d.deposit_batch_detail = x.deposit_batch_detail

		-- Indicate that the batch has been posted
		update		deposit_batch_ids
		SET			date_posted			= getdate(),
					posted_by			= suser_sname()
		WHERE		deposit_batch_id	= @deposit_batch_id
	END
	drop table #t_product_by_date

	-- Break the deposit items down by date for each client
	SELECT		client, convert(datetime, convert(varchar(10), date_created, 101) + ' 00:00:00') as date_created, sum(amount) as amount
	INTO		#t_deposit_by_date
	FROM		deposit_batch_details d
	WHERE		deposit_batch_id = @deposit_batch_id
	AND			ok_to_post = 1
	AND			client_product IS NULL
	AND			date_posted IS NULL

	-- Must do a "group by convert(datetime,...)" or we will update client's trust with only one of the deposits per batch.
	GROUP BY	client, convert(datetime, convert(varchar(10), date_created, 101) + ' 00:00:00')

	-- Create an index to make things work faster
	create index ix1_t_deposit_by_date ON #t_deposit_by_date ( client, date_created );

	-- Calculate the total amount of the deposit by client
	SELECT		client, sum(amount) as amount
	INTO		#t_deposit_by_client
	FROM		#t_deposit_by_date
	GROUP BY	client

	-- Determine the amount and the number of clients
	SELECT	@Client_Count	= count(*),
			@Deposit_Amount	= @Deposit_Amount + isnull(sum(amount),0)
	FROM	#t_deposit_by_client

	-- Look only for DMP clients. There may be product transactions so don't just die here.
	IF @Client_Count > 0
	BEGIN

		-- Create working indicies to make things work faster since this is the majority of the time here.
		create index ix1_t_deposit_by_client ON #t_deposit_by_client ( client, amount );
		create index ix2_t_deposit_by_client ON #t_deposit_by_client ( client );

		-- Update the retention events to expire items that are based upon deposits
		update		client_retention_events
		set			date_expired = getdate()
		from		#t_deposit_by_client x
		left outer join client_retention_events e on x.client = e.client and 2 = e.expire_type
		where		e.date_expired is null
		and			e.amount <= x.amount;

		-- Update the trust balances for the clients
		UPDATE		clients
		SET			held_in_trust		= isnull(c.held_in_trust,0)    + x.amount,
					deposit_in_trust	= isnull(c.deposit_in_trust,0) - x.amount
		FROM		clients c
		INNER JOIN	#t_deposit_by_client x on c.client = x.client;

		-- Update the last deposit date and amount for the client.
		UPDATE		clients
		SET			last_deposit_date	= x.date_created,
					last_deposit_amount	= x.amount
		FROM		clients c
		INNER JOIN	#t_deposit_by_date x ON c.client = x.client
		WHERE		c.client > 0

		-- Set the first deposit date
		update		clients
		set			first_deposit_date	= x.date_created
		from		clients c
		inner join	#t_deposit_by_date x on c.client = x.client
		where		c.client > 0
		and			c.first_deposit_date is null

		-- The $100.00 figure is just a shot in the dark.
		and			x.amount >= 100

		-- Insert the item into the deposit register
		insert into registers_trust (tran_type, amount, cleared, bank, sequence_number) values ('DP', @deposit_amount, ' ', isnull(@bank,1), @note)
		SELECT @trust_register = SCOPE_IDENTITY()

		IF @deposit_batch_type = 'AC'
			insert into registers_client	(tran_type,	client,		tran_subtype,	message,	item_date,	trust_register,		credit_amt)
			select			'DP',		client,		'AC',	reference,	item_date,	@trust_register,	amount
			from			deposit_batch_details
			where			deposit_batch_id = @deposit_batch_id
			and				date_posted is null
			and				ok_to_post = 1
			and				client > 0
		else
			insert into registers_client	(tran_type,	client,		tran_subtype,	message,	item_date,	trust_register,		credit_amt)
			select			'DP',		client,		tran_subtype,	reference,	item_date,	@trust_register,	amount
			from			deposit_batch_details
			where			deposit_batch_id = @deposit_batch_id
			and				date_posted is null
			and				ok_to_post = 1
			and				client > 0

		-- Mark the items as being deposited
		update	deposit_batch_details
		set		date_posted			= getdate()
		where	deposit_batch_id	= @deposit_batch_id
		AND		ok_to_post		> 0
		and		client			> 0

		-- Indicate that the batch has been posted
		update	deposit_batch_ids
		SET		date_posted			= getdate(),
				posted_by			= suser_sname(),
				trust_register		= @trust_register
		WHERE	deposit_batch_id	= @deposit_batch_id

		-- Find the list of clients which will have the error status changed
		if @deposit_batch_type = 'AC'
		BEGIN
			select		c.client
			into		#ach_clients
			from		clients c with (nolock)
			inner join	deposit_batch_details d with (nolock) on c.client = d.client
			INNER JOIN	ach_reject_codes e WITH (NOLOCK) ON d.ach_authentication_code = e.ach_reject_code
			where		d.deposit_batch_id = @deposit_batch_id
			AND			d.ach_authentication_code is not null
			AND			d.ach_transaction_code in ('27', '37')
			AND			isnull(e.serious_error,1) > 0
			AND			c.client > 0

			-- Set the error date to today for the clients
			UPDATE		client_ach
			SET			ErrorDate	= getdate()
			from		clients c
			inner join	#ach_clients ac on c.client = ac.client

			-- Generate a system note that the ACH failed
			insert into client_notes (client, type, dont_edit, dont_delete, subject, note, date_created)
			select		client, 3, 1, 1,
						'Deposit Information Changed',
						'Error date changed to ' + convert(varchar(10), GETDATE(), 101),
						GETDATE()
			from		#ach_clients

			-- Discard the working table
			drop table	#ach_clients
		END

		-- Calculate the pending deposits for the clients in this batch
		select distinct client
		into #distinct_clients
		from deposit_batch_details
		where deposit_batch_id = @deposit_batch_id

		update clients
		set    deposit_in_trust = dbo.DepositsInTrust(c.client)
		from   clients c
		inner join #distinct_clients x on c.client = x.client;

		drop table #distinct_clients;
	END

	-- Finally make the operations permanent
	COMMIT TRANSACTION

	-- Tell the user the dollar amount posted
	declare	@p2					varchar(80)

	if @client_count > 0
		set	@p2 = isnull(@p2,'') + ' and ' + convert(varchar, @Client_Count) + ' Client(s)'

	if @product_count > 0
		set	@p2 = isnull(@p2,'') + ' and ' + convert(varchar, @product_count) + ' Product(s)'

	set @p2 = substring(@p2, 5, 80) + ' were posted'

	if @deposit_amount > 0
		set	@p2 = isnull(@p2,'') + ' with a total of $' + convert(varchar, @Deposit_Amount, 1)

	PRINT	@p2

	RETURN ( @trust_register )
END TRY

BEGIN CATCH
	DECLARE @ErrorMessage NVARCHAR(4000);
	DECLARE @ErrorSeverity INT;
	DECLARE @ErrorState INT;

	SELECT	@ErrorMessage	= ERROR_MESSAGE(),
			@ErrorSeverity	= ERROR_SEVERITY(),
			@ErrorState		= ERROR_STATE();

	if @@trancount > 0
	BEGIN
		BEGIN TRY
			ROLLBACK TRANSACTION
		END TRY
		BEGIN CATCH
		END CATCH
	END

	RAISERROR (@ErrorMessage, @ErrorSeverity, @ErrorState);
	return ( 0 )
END CATCH
go
grant execute on xpr_deposit_batch_post to public as dbo
GO
