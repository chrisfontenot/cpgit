USE [DebtPlus]
GO
/****** Object:  StoredProcedure [dbo].[xpr_creditor_refund_batch_close]    Script Date: 09/15/2014 13:13:34 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[xpr_creditor_refund_batch_close] ( @deposit_batch_id as typ_key ) as

-- ===========================================================================================
-- ==            Close (and post) the creditor refund batch                                 ==
-- ===========================================================================================

-- ChangeLog
--    11/02/2001
--       Removed test for creditor type. All fairshare amounts in RF transactions are always "Deduct", irregardless of the creditor type.
--   9/31/2002
--     Added support for client_creditor_balances table
--   10/15/2002
--     Added support for client_creditor's "*_this_creditor" information
--   11/18/2002
--     Added bank information.
--   06/09/2009
--      Remove references to debt_number and use client_creditor

begin transaction
set xact_abort on

-- Disable intermediate result sets
set nocount on

-- Ensure that the batch is a creditor refund batch
if not exists (select * from deposit_batch_ids where batch_type = 'CR' and date_posted is null and deposit_batch_id = @deposit_batch_id)
begin
	raiserror (50088, 14, 1, @deposit_batch_id)
	rollback transaction
	return ( 0 )
end

-- Obtain the bank account ID for the deposit batch
declare	@bank		int
select	@bank	= bank
from	deposit_batch_ids
where	deposit_batch_id = @deposit_batch_id

declare	@trust_register		typ_key
select	@trust_register = 0

declare	item_cursor cursor for
	select	client, creditor, client_creditor, amount, fairshare_amt, creditor_type, fairshare_pct, message, reference, date_posted
	from	deposit_batch_details
	where	deposit_batch_id = @deposit_batch_id
	and	date_posted is null
for update of date_posted

declare	@creditor_register	int
declare	@input_date_posted	datetime
declare	@input_client		typ_client
declare	@input_creditor		typ_creditor
declare	@input_client_creditor	int
declare	@input_gross		money
declare	@input_deducted		money
declare	@input_net		money
declare	@input_fairshare_amt	money
declare	@input_creditor_type	typ_creditor_type
declare	@input_fairshare_pct	float
declare	@input_message		typ_message
declare	@input_description	typ_description

-- Total amount for the deposit
declare	@total_net		money
select	@total_net		= 0

open item_cursor
fetch item_cursor into @input_client, @input_creditor, @input_client_creditor, @input_gross, @input_fairshare_amt, @input_creditor_type, @input_fairshare_pct, @input_message, @input_description, @input_date_posted

while @@fetch_status = 0
begin
	-- Allocate a trust register if we don't have one
	if @trust_register < 1
	begin
		insert into registers_trust (tran_type, amount, cleared, bank)
		values ('RF', 0, ' ', @bank)
		select	@trust_register = SCOPE_IDENTITY()
	end

	-- Adjust the disbursed amounts
	update	client_creditor_balances
	set	total_payments		= total_payments - @input_gross,
		payments_month_0	= payments_month_0 - @input_gross
	from	client_creditor_balances bal
	inner join client_creditor cc on bal.client_creditor_balance = cc.client_creditor_balance
	where	cc.client_creditor		= @input_client_creditor

	-- Adjust the statistics for the current instance of the debt
	update	client_creditor
	set	returns_this_creditor	= isnull(returns_this_creditor,0) + @input_gross
	from	client_creditor cc
	where	cc.client_creditor		= @input_client_creditor

	-- Adjust the client deposit balance
	update	clients
	set	held_in_trust		= held_in_trust + @input_gross
	where	client			= @input_client

	-- Remove the deducted amount from the trust balance
	select @input_deducted = @input_fairshare_amt
	if @input_deducted is null
		set @input_deducted = 0

	select	@input_net = @input_gross - @input_deducted
	select	@total_net = @total_net + @input_net

	if @input_deducted > 0
		update	clients
		set	held_in_trust	= held_in_trust - @input_deducted
		where	client		= 0

	-- Insert the transaction into the creditor table for the refund
	insert into registers_creditor		(tran_type,	creditor,		trust_register,		credit_amt,	message)
	values					('RF',		@input_creditor,	@trust_register,	@input_net,	@input_message)
	select	@creditor_register = SCOPE_IDENTITY()

	-- Insert the transaction into the client table for the refund
	insert into registers_client		(tran_type,	client,		credit_amt,	message,		trust_register)
	values					('RF',		@input_client,	@input_gross,	@input_message,		@trust_register)

	-- Insert the transaction into the client/creditor table for the refund
	insert into registers_client_creditor	(tran_type,	client,		creditor,		client_creditor,		credit_amt,	fairshare_amt,		creditor_type,		fairshare_pct,		trust_register,		void,	creditor_register)
	values 					('RF',		@input_client,	@input_creditor,	@input_client_creditor,	@input_gross,	@input_fairshare_amt,	'D',			@input_fairshare_pct,	@trust_register,	0,	@creditor_register)

	-- Indicate that the item was now posted
	update	deposit_batch_details set date_posted = getdate() where current of item_cursor

	-- Fetch the next record from the items
	fetch item_cursor into @input_client, @input_creditor, @input_client_creditor, @input_gross, @input_fairshare_amt, @input_creditor_type, @input_fairshare_pct, @input_message, @input_description, @input_date_posted
end

close item_cursor
deallocate item_cursor

-- Update the trust register with the net "deposit"
if @trust_register > 0
	update	registers_trust
	set	amount		= @total_net
	where	trust_register	= @trust_register

-- Indicate that the batch was posted
update	deposit_batch_ids
set	date_posted		= getdate(),
	date_closed		= getdate(),
	trust_register		= @trust_register
where	deposit_batch_id	= @deposit_batch_id

-- Commit the transaction
commit transaction
return ( 1 )
GO
