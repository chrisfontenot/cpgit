USE [DEBTPLUS_UAT]
GO
DROP PROCEDURE [dbo].[www_client_debt_info]
GO
CREATE PROCEDURE [dbo].[www_client_debt_info] ( @client as int, @last_month as int = null, @ignore_reassigned as int = null ) as

-- ==========================================================================================
-- ==            Generate the previous debt information                                    ==
-- ==========================================================================================

-- ChangeLog
--   9/31/2002
--     Added support for client_creditor_balances table
--   1/20/2004
--     Ensure that reassigned debts have a zero balance/disbursement factor
--   3/14/2003
--     Allow for more than 10,000 creditors in a class
--   11/29/2012
--     Return an indicator if the balance may be changed by the web client.

-- Suppress intermediate results
-- set nocount on

-- Retrieve the information for the previous month information
if @last_month is null
	select @last_month = 1

if @ignore_reassigned is null
	select @ignore_reassigned = 1

create table #results (
	[type]							varchar(10)  null,
	[creditor_id]					int          null,
	[creditor]						varchar(10)  null,
	[creditor_name]					varchar(256) null,
	[creditor_division_name]		varchar(256) null,
	[account_number]				varchar(256) null,
	[orig_balance]					money,
	[orig_balance_adjustment]		money,
	[current_balance]				money,
	[total_interest]				money,
	[total_payments]				money,	
	[disbursed]						money,
	[disbursement_factor]			money,
	[last_month_disbursement]		money,
	[current_month_disbursement]	money,
	[client_creditor]				int,
	[client_creditor_balance]		int,
	[reassigned_debt]				int,
	[balance_record]				int,
	[balance_verify_date]			datetime null,
	[allow_balance_update]			int,
	[first_payment_date]			datetime null,
	[last_payment_date]				datetime null,
	[payout_date]					datetime null,
	[interest_rate]					float null,
	[first_payment_amount]			money,
	[last_payment_amount]			money
);

-- Process last month's values
if @last_month <> 0
	insert into #results ([type],[creditor_id],[creditor],[creditor_name],[creditor_division_name],[account_number],[orig_balance],[orig_balance_adjustment],[current_balance],[total_interest],[total_payments],[disbursed],[disbursement_factor],[last_month_disbursement],[current_month_disbursement],[client_creditor],[client_creditor_balance],[reassigned_debt],[balance_record],[balance_verify_date],[allow_balance_update],[first_payment_date],[last_payment_date],[payout_date],[interest_rate],[first_payment_amount],[last_payment_amount])
	select cr.[type] as [type], isnull(cr.[creditor_id],0) as [creditor_id], cc.[creditor] as [creditor], cr.[creditor_name] as [creditor_name], cr.[division] as [division_name], cc.[account_number] as [account_number], bal.[orig_balance] as [orig_balance], bal.[orig_balance_adjustment] as [orig_balance_adjustment], bal.[orig_balance]+bal.[orig_balance_adjustment]+bal.[total_interest]-bal.[total_payments] as [current_balance], bal.[total_interest] as [total_interest], bal.[total_payments] as [total_payments], bal.[payments_month_1] as [disbursed], cc.[disbursement_factor] as [disbursement_factor], bal.[payments_month_1] as [last_month_disbursement], bal.[payments_month_0] as [current_month_disbursement], cc.[client_creditor] as [client_creidtor], bal.[client_creditor_balance] as [client_creditor_balance], cc.[reassigned_debt] as [reassigned_debt], bal.[client_creditor] as [balance_record], cc.[balance_verify_date] as [balance_verify_date], 1 - isnull(convert(int, ccl.agency_account), 1)	as [allow_balance_update], rccf.[date_created] as [first_payment_date], rccl.[date_created] as [last_payment_date], cc.[expected_payout_date] as [payout_date], coalesce(cc.[dmp_interest], cr.lowest_apr_pct, 0) as [interest_rate], rccf.[debit_amt] as [first_payment_amount], rccl.[debit_amt] as [last_payment_amount]
	from client_creditor cc left outer join creditors cr on cc.creditor = cr.creditor inner join client_creditor_balances bal on cc.client_creditor_balance = bal.client_creditor_balance left outer join creditor_classes ccl on cr.creditor_class = ccl.creditor_class left outer join registers_client_creditor rccl on cc.last_payment = rccl.client_creditor_register left outer join registers_client_creditor rccf on cc.first_payment = rccf.client_creditor_register
	where cc.client = @client

else -- Use this month's figures

	insert into #results ([type],[creditor_id],[creditor],[creditor_name],[creditor_division_name],[account_number],[orig_balance],[orig_balance_adjustment],[current_balance],[total_interest],[total_payments],[disbursed],[disbursement_factor],[last_month_disbursement],[current_month_disbursement],[client_creditor],[client_creditor_balance],[reassigned_debt],[balance_record],[balance_verify_date],[allow_balance_update],[first_payment_date],[last_payment_date],[payout_date],[interest_rate],[first_payment_amount],[last_payment_amount])
	select cr.[type] as [type], isnull(cr.[creditor_id],0) as [creditor_id], cc.[creditor] as [creditor], cr.[creditor_name] as [creditor_name], cr.[division] as [division_name], cc.[account_number] as [account_number], bal.[orig_balance] as [orig_balance], bal.[orig_balance_adjustment] as [orig_balance_adjustment], bal.[orig_balance]+bal.[orig_balance_adjustment]+bal.[total_interest]-bal.[total_payments] as [current_balance], bal.[total_interest] as [total_interest], bal.[total_payments] as [total_payments], bal.[payments_month_0] as [disbursed], cc.[disbursement_factor] as [disbursement_factor], bal.[payments_month_1] as [last_month_disbursement], bal.[payments_month_0] as [current_month_disbursement], cc.[client_creditor] as [client_creidtor], bal.[client_creditor_balance] as [client_creditor_balance], cc.[reassigned_debt] as [reassigned_debt], bal.[client_creditor] as [balance_record], cc.[balance_verify_date] as [balance_verify_date], 1 - isnull(convert(int, ccl.agency_account), 1)	as [allow_balance_update], rccf.[date_created] as [first_payment_date], rccl.[date_created] as [last_payment_date], cc.[expected_payout_date] as [payout_date], coalesce(cc.[dmp_interest], cr.lowest_apr_pct, 0) as [interest_rate], rccf.[debit_amt] as [first_payment_amount], rccl.[debit_amt] as [last_payment_amount]
	from client_creditor cc left outer join creditors cr on cc.creditor = cr.creditor inner join client_creditor_balances bal on cc.client_creditor_balance = bal.client_creditor_balance left outer join creditor_classes ccl on cr.creditor_class = ccl.creditor_class left outer join registers_client_creditor rccl on cc.last_payment = rccl.client_creditor_register left outer join registers_client_creditor rccf on cc.first_payment = rccf.client_creditor_register
	where cc.client = @client

-- Set the reassignment flag if needed
update	#results
set		reassigned_debt	= 1
where	balance_record <> client_creditor;

-- Remove reassigned debts if they are not desired
if isnull(@ignore_reassigned,0) <> 0
	delete
	from	#results
	where	reassigned_debt = 1

-- For reassigned debts, clear the figures
update	#results
set		orig_balance			= 0,
		total_payments			= 0,
		current_balance			= 0,
		disbursed				= 0,
		disbursement_factor		= 0,
		allow_balance_update	= 0
where	reassigned_debt			= 1;

-- Mark the debts as disallowing the balance update if the balance was verified in the past 15 days
declare	@today				datetime
select	@today				= convert(varchar(10), getdate(), 101);

update	#results
set		allow_balance_update	= 0
where	balance_verify_date is not null
and		datediff(d, balance_verify_date, @today) < 16;

-- Return the results
select		[type], [creditor_id], coalesce (creditor + ' ' + creditor_name, creditor_name, '') as 'creditor_name', [creditor_division_name], [account_number], [orig_balance], [orig_balance_adjustment], [current_balance], [total_interest], [total_payments], [disbursed],
			[disbursement_factor], [last_month_disbursement], [current_month_disbursement], [client_creditor] as 'item_key', [client_creditor_balance], [reassigned_debt], [balance_record], [balance_verify_date], [allow_balance_update], [first_payment_date], [last_payment_date],
			[payout_date], [interest_rate], [first_payment_amount], [last_payment_amount]
from		#results
order by	type, creditor_id

drop table #results
return ( @@rowcount )
GO
GRANT EXECUTE ON www_client_debt_info TO public AS dbo;
DENY EXECUTE ON www_client_debt_info TO www_role
GO
