USE [DebtPlus]
GO
/****** Object:  StoredProcedure [dbo].[xpr_transfer_cl_op]    Script Date: 09/15/2014 13:13:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE PROCEDURE [dbo].[xpr_transfer_cl_op] ( @SrcClient AS INT, @DestAccount AS VarChar(80), @Amount AS Money, @Description AS VarChar(80) = NULL ) AS

-- ===============================================================================================
-- ==            Generate a transfer from one client to an operating account                    ==
-- ===============================================================================================

DECLARE	@ErrorMsg	VarChar(128)
DECLARE	@OldBalance	Money

-- Do not generate intermediate result sets
SET NOCOUNT ON

-- Do nothing if the amount is zero
IF @Amount = 0
	Return ( 0 )

-- Ensure that the amount is valid
IF @Amount < 0
BEGIN
	RaisError(50019, 16, 1 )
	Return ( 0 )
END

-- Generate a transaction for the operation
BEGIN TRANSACTION

-- Fetch the starting balance for the operation
SELECT	@OldBalance = isnull(held_in_trust,0)
FROM	clients
WHERE	client = @SrcClient

IF @@error > 0
BEGIN
	Rollback Transaction
	RETURN ( 0 )
END

IF @OldBalance IS NULL
BEGIN
	RaisError(50014, 16, 1, @SrcClient)
	RollBack Transaction
	Return ( 0 )
END

-- Remove the money from the client trust
UPDATE	clients
SET	held_in_trust = held_in_trust - @Amount
WHERE	client = @SrcClient

-- Insert the item into the client register for the withdrawl
INSERT INTO registers_client	(tran_type,	client,		debit_amt,	message)
VALUES				('MF',		@SrcClient,	@Amount,	isnull(@description, 'Misc. Agency Fee'))

-- Insert the item into the non-ar table
INSERT INTO registers_non_ar	(tran_type,	client,		credit_amt,	dst_ledger_account,	message)
VALUES				('DM',		@SrcClient,	@Amount,	@DestAccount,		isnull(@description, 'Client to Oper Transfer'))

-- Return the client trust balance
SELECT	held_in_trust as 'balance'
FROM	clients
WHERE	client = @SrcClient

-- Terminate normally
COMMIT TRANSACTION
RETURN ( 1 )
GO
