USE [DebtPlus]
GO
/****** Object:  StoredProcedure [dbo].[UPDATE_timestamp]    Script Date: 09/15/2014 13:13:34 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[UPDATE_timestamp] ( @Position as varchar(80) ) as
declare	@msg		varchar(800)
declare	@now		varchar(80)
select	@now = CONVERT(varchar, getdate())
print ' '
select @msg = '***** At ' + @Position + ' ' + @now + ' *******'
print  @msg
print ' '
GO
