USE [DebtPlus]
GO
/****** Object:  StoredProcedure [dbo].[xpr_creditor_contact_select]    Script Date: 09/15/2014 13:13:34 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[xpr_creditor_contact_select] ( @CreditorContact AS INT ) AS
-- ==================================================================================================================
-- ==            Return the specific creditor contact information                                                  ==
-- ==================================================================================================================

-- ChangeLog
--    2/18/2002
--       Added space after middle and before last name

SELECT
	coalesce(t.[name],'Unknown type ' + convert(varchar,c.creditor_contact_type), '') as 'creditor_contact_type',

	dbo.format_normal_name(c.prefix,c.first,c.middle,c.last,c.suffix)				as 'name',
	c.phone											as 'phone',
	c.fax													as 'fax',
	isnull(c.title,'') as title,
	c.address1		as 'input_address1',
	c.address2											as 'input_address2',
	c.city + ' ' + c.state + ' ' + c.zipcode	as 'address3',

	isnull(c.notes,'') as 'note',
	c.email														as 'input_email'
FROM	view_creditor_contact_info c
LEFT OUTER JOIN	creditor_contact_types t	WITH (NOLOCK) ON c.creditor_contact_type = t.creditor_contact_type
WHERE	c.creditor_contact = @CreditorContact

RETURN ( @@rowcount )
GO
