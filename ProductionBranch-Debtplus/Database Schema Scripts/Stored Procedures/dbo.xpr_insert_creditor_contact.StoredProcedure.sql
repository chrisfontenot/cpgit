USE [DebtPlus]
GO
/****** Object:  StoredProcedure [dbo].[xpr_insert_creditor_contact]    Script Date: 09/15/2014 13:13:35 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[xpr_insert_creditor_contact] ( @creditor as varchar(10), @creditor_contact_type as int, @NameID as int = null, @Title as varchar(255) = null, @TelephoneID as int = null, @AddressID as int = null, @FAXID as int = null, @EmailID as int = null, @Notes as typ_message = null) as
-- =================================================================================
-- ==      Add a creditor contact into the system                                 ==
-- =================================================================================
insert into creditor_contacts ( [creditor], [creditor_contact_type], [NameID], [Title], [TelephoneID], [AddressID], [FAXID], [EmailID], [Notes] ) values ( @creditor, @creditor_contact_type, @NameID, @Title, @TelephoneID, @AddressID, @FAXID, @EmailID, @Notes );
return ( scope_identity() )
GO
