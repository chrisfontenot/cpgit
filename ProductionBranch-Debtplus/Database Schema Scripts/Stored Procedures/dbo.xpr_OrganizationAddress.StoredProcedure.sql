USE [DebtPlus]
GO
/****** Object:  StoredProcedure [dbo].[xpr_OrganizationAddress]    Script Date: 09/15/2014 13:13:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[xpr_OrganizationAddress] AS
-- =================================================================================================
-- ==                             Fetch the Organization Address information                      ==
-- =================================================================================================

SELECT TOP 1
		convert(varchar(256),upper(c.name))	AS 'name',
		convert(varchar(256),upper(dbo.format_Address_Line_1(a.house, a.direction, a.street, a.suffix, a.modifier, a.modifier_value))) as 'addr1',
		convert(varchar(256),upper(a.address_line_2)) as 'addr2',
		convert(varchar(256),upper(dbo.format_city_state_zip (a.city, a.state, a.postalcode))) as 'addr3',
		convert(varchar(256),dbo.format_TelephoneNumber( c.TelephoneID )) as 'telephone',
		convert(varchar(10), isnull(a.PostalCode,'')) as 'zipcode'
FROM	config c WITH (NOLOCK)
LEFT OUTER JOIN addresses a WITH (NOLOCK) ON c.AddressID = a.Address
RETURN ( @@rowcount )
GO
