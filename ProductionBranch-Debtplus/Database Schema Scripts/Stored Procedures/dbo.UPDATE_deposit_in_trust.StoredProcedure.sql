USE [DebtPlus]
GO
/****** Object:  StoredProcedure [dbo].[UPDATE_deposit_in_trust]    Script Date: 09/15/2014 13:13:34 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[UPDATE_deposit_in_trust] as

-- Mark the ACH transactions with the proper flag
update	deposit_batch_details
set	ok_to_post	= 0
where	deposit_batch_id in (
	select	deposit_batch_id
	from	deposit_batch_ids
	where	batch_type = 'AC'
)

update	deposit_batch_details
set	ok_to_post	= 1
where	client		> 0
and	ach_authentication_code is null
and	deposit_batch_id in (
	select	deposit_batch_id
	from	deposit_batch_ids
	where	batch_type = 'AC'
)

-- Mark the ACH batches as closed
update	deposit_batch_ids
set	date_closed	= date_created
where	batch_type	= 'AC';

-- Find the total amount for the batches
select	d.client, sum(d.amount) as amount
into	#deposit_in_trust
from	deposit_batch_details d
inner join deposit_batch_ids ids on d.deposit_batch_id = ids.deposit_batch_id
where	ids.batch_type	<> 'CR'
and	d.ok_to_post	= 1
and	d.client	> 0
and	ids.date_closed is not null
and	ids.date_posted is null
group by d.client;

update	clients
set	deposit_in_trust	= 0;

update	clients
set	deposit_in_trust	= x.amount
from	clients c
inner join #deposit_in_trust x on c.client = x.client;

-- Update the client's first and last payment information
select rc.client, tr.trust_register, sum(rc.credit_amt) as credit_amt, tr.date_created as date_created, convert(int,0) as is_last, convert(int,0) as is_first
into #deposits
from registers_client rc
inner join registers_trust tr on rc.trust_register = tr.trust_register
where rc.tran_type = 'DP'
group by rc.client, tr.trust_register, tr.date_created

select client, max(date_created) as date_created
into #deposits_2
from #deposits
group by client;

update #deposits set is_last = 1 from #deposits a inner join #deposits_2 b on a.client = b.client and a.date_created = b.date_created

select client, min(date_created) as date_created
into #deposits_3
from #deposits
group by client;

update #deposits set is_first = 1 from #deposits a inner join #deposits_3 b on a.client = b.client and a.date_created = b.date_created

update	clients
set		last_deposit_date = null,
		last_deposit_amount = 0,
		first_deposit_date = null

update clients
set first_deposit_date = d.date_created
from clients c
inner join #deposits d on c.client = d.client
where is_first = 1

update clients
set last_deposit_date = d.date_created,
	last_deposit_amount = d.credit_amt
from clients c
inner join #deposits d on c.client = d.client
where is_last = 1

drop table #deposits
drop table #deposits_2
drop table #deposits_3
GO
