USE [DebtPlus]
GO
/****** Object:  StoredProcedure [dbo].[xpr_disbursement_create]    Script Date: 09/15/2014 13:13:35 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[xpr_disbursement_create] ( @disbursement_date AS varchar(512) = null, @region as varchar(512) = null, @Note as VarChar(256) = NULL ) AS

-- ===================================================================================================
-- ==            Create a disbursement batch for the indicated disbursement date                    ==
-- ===================================================================================================

-- ChangeLog
--   2/6/2002
--     Created a variable "debt_start_date". Set this to "tomorrow" to accomodate possible time values in the debt and
--     client start date. The times should not be used.
--   3/29/2002
--     add "held" flag for creditor disbursements being held
--   6/16/2002
--     Removed columns disbursed and last_name from disbursement_clients table
--   9/10/2002
--     Made the fairshare percentage 0.00001 if it was zero and the creditor was bank-wire and had a deduct status with a non-zero percentage.
--     This handles the case where the fairshare must be 0.01 to pass mastercard's validation.
--   9/31/2002
--     Added support for client_creditor_balances table
--   11/20/2002
--     Changed to use RPPS' biller_aba number rather than rpps_biller_id
--   12/08/2002
--     Added check_payments
--   01/15/2002
--     Added creditor_classes table references and dropped "creditors.fee_creditor"
--   02/13/2004
--     Do not permit RPPS "gross" billers to be deduct. Force them to be "billed".
--   08/20/2004
--     Removed fairshare_pct_* and creditor_type_* from the creditor. Added creditor_contribution_pct link.
--   06/09/2009
--      Remove references to debt_number and use client_creditor
--   1/5/2009
--      Changed from using biller_aba to using rpps_biller_id

-- Disable intermediate result sets
SET NOCOUNT ON

-- Start a transaction to create the disbursement batch
begin transaction

DECLARE @trust_balance		money
DECLARE	@disbursement_register	int
DECLARE	@use_sched_pay		int
DECLARE	@debt_start_date	datetime

-- This is the debt start date. It should be for "tomorrow" to accomodate the time values that may be present.
select	@debt_start_date = convert(datetime, convert(varchar(10), getdate(), 101) + ' 00:00:00')
select	@debt_start_date = dateadd(d, 1, @debt_start_date)

-- Should the scheduled pay be used or the disbursement factor?
select	@use_sched_pay = convert(int, isnull(use_sched_pay,0))
from	config

-- Calculate the trust balance
SELECT	@trust_balance	= sum(held_in_trust)
FROM	clients WITH (NOLOCK)

-- Generate the disbursement batch
INSERT INTO registers_disbursement	(tran_type,	disbursement_date,	region,		prior_balance,	created_by,	date_created,	date_posted,	note)
VALUES					('AD',		@disbursement_date,	@region,	@trust_balance,	suser_sname(),	getdate(),	null,		@note)

-- Remember the disbursement register
select	@disbursement_register = SCOPE_IDENTITY()

-- Build the statement to construct the list of clients for the disbursement
declare	@stmt		varchar(8000)
select	@stmt		= '
	SELECT		isnull(c.region,0),
			' + convert(varchar,@disbursement_register) + ',
			c.client,
			c.held_in_trust,
			0,
			0,
			null,
			isnull(active_status,''CRE''),
			isnull(start_date, convert(datetime, ''1/1/1980 00:00:00'')),

			case
				when c.reserved_in_trust is null			then 0
				when c.reserved_in_trust_cutoff is null			then 0
				when c.reserved_in_trust <= 0				then 0
				when c.reserved_in_trust_cutoff < convert(datetime,''' + convert(varchar(10), @debt_start_date,101) + ' 00:00:00'')	then 0
				else c.reserved_in_trust
			end,
			c.counselor,
			c.csr,
			c.office,
			upper(left(pn.last,1))
				
	FROM		clients c
	LEFT OUTER JOIN people p on c.client = p.client and 1 = p.relation
	LEFT OUTER JOIN Names pn ON p.NameID = pn.Name
	WHERE		c.held_in_trust > 0
	AND		c.hold_disbursements = 0
	AND		c.client > 0
'

-- If a disbursement date is specified then select those clients
if ltrim(rtrim(isnull(@disbursement_date,''))) <> ''
	select		@stmt = @stmt + ' AND c.disbursement_date IN (' + replace(@disbursement_date,'''','''''') + ')'

-- If a region is specified then select those regions as well
if ltrim(rtrim(isnull(@region,''))) <> ''
	select		@stmt = @stmt + ' AND c.region IN (' + replace(@region,'''','''''') + ')'

-- Include the "insert" statement to complete it
select	@stmt = 'INSERT INTO disbursement_clients (region,disbursement_register, client, held_in_trust, note_count, disbursement_factor, client_register, active_status, start_date, reserved_in_trust, counselor, csr, office, last_name) ' + @stmt

-- Build the client list
-- print @stmt
exec ( @stmt )

-- Discard clients who have all of their money on hold
delete
from	disbursement_clients
where	reserved_in_trust	>= held_in_trust
and	disbursement_register	= @disbursement_register;

-- Create the list of debts which are to be disbursed
declare	@debts_stmt	varchar(8000)
select	@debts_stmt	= '
	SELECT		' + convert(varchar, @disbursement_register) + ' as ''disbursement_register'',
			cc.client, cc.creditor, cc.client_creditor,
			isnull(mt.bank,-1)						as bank,
			isnull(mt.rpps_biller_id,''0000000000'')			as ''rpps_biller_id'',
			''BW''								as ''tran_type'',
			''N''								as ''creditor_type'',
			isnull(cc.disbursement_factor,0)				as ''disbursement_factor'',
'

-- If scheduled pay is not used, use the disbursement factor. Otherwise, prefer scheduled pay.
if @use_sched_pay = 0
	select	@debts_stmt = @debts_stmt + '			coalesce(cc.disbursement_factor,0)				as ''sched_payment'','
else
	select	@debts_stmt = @debts_stmt + '			coalesce(cc.sched_payment,cc.disbursement_factor,0)		as ''sched_payment'','

select	@debts_stmt = @debts_stmt + '
			isnull(bal.payments_month_0,0)					as ''current_month_disbursement'',

			case isnull(ccl.zero_balance,0)
				when 0 then isnull(bal.orig_balance,0)
				else 0
			end								as ''orig_balance'',

			case
				when isnull(ccl.zero_balance,0) = 0 then isnull(bal.orig_balance,0) + isnull(bal.orig_balance_adjustment,0) + isnull(bal.total_interest,0) - isnull(bal.total_payments,0)
				when coalesce(cc.disbursement_factor,0) < coalesce(bal.payments_month_0,0) then 0
				else coalesce(cc.disbursement_factor,0) - coalesce(bal.payments_month_0,0)
			end								as ''current_balance'',


			coalesce (cc.dmp_interest, cr.lowest_apr_pct, cr.medium_apr_pct, cr.highest_apr_pct, 0) as ''apr'',
			0								as ''debit_amt'',
			0								as ''fairshare_amt'',
			cc.fairshare_pct_check						as ''fairshare_pct_check'',
			cc.fairshare_pct_eft						as ''fairshare_pct_eft'',
			cc.priority							as ''priority'',

			case
				when	cc.hold_disbursements <> 0		then 1'

-- Include the testing for the debt start date when appropriate. (Uncomment the following 3 lines to enable its processing.)
-- select	@stmt = @stmt + '
--				when	cc.start_date is null			then 0
--				when	cc.start_date >= convert(datetime, ''' + convert(varchar(10), @debt_start_date, 101) + ' 00:00:00'')	then 1

select	@debts_stmt = @debts_stmt + '
										else 0
			end								as ''held''

	FROM		disbursement_clients c		with (nolock)
	inner join	client_creditor cc		with (nolock) ON c.client = cc.client
	inner join	client_creditor_balances bal	with (nolock) on cc.client_creditor_balance = bal.client_creditor_balance
	inner join	creditors cr			with (nolock) ON cc.creditor = cr.creditor
	left outer join creditor_classes ccl		with (nolock) on cr.creditor_class = ccl.creditor_class
	left outer join creditor_methods mt		with (nolock) ON cc.payment_rpps_mask = mt.creditor_method
'

select	@stmt	= @debts_stmt + '
	where		c.disbursement_register = ' + convert(varchar, @disbursement_register) + '
	and		cc.reassigned_debt = 0
	and		cc.disbursement_factor > 0
	and		((isnull(ccl.zero_balance,0) > 0) or (isnull(bal.orig_balance,0) + isnull(bal.orig_balance_adjustment,0) + isnull(bal.total_interest,0) - isnull(bal.total_payments,0) > 0))
	and		isnull(ccl.always_disburse,1) <> 0
'

-- Include the statement for the insertion into the debt list
select @stmt = 'insert into disbursement_creditors (disbursement_register, client, creditor, client_creditor, bank, rpps_biller_id, tran_type, creditor_type, disbursement_factor, sched_payment, current_month_disbursement, orig_balance, current_balance, apr, debit_amt, fairshare_amt, fairshare_pct_check, fairshare_pct_eft, priority, held) ' + @stmt
-- print @stmt
exec ( @stmt )

-- Create the list of debts which are to be disbursed because they are for general creditors
select	@stmt = @debts_stmt + '
	where		c.disbursement_register = ' + convert(varchar, @disbursement_register) + '
	and		cc.reassigned_debt = 0
	and		cc.disbursement_factor > 0
	and		((isnull(ccl.zero_balance,0) > 0) or (isnull(bal.orig_balance,0) + isnull(bal.orig_balance_adjustment,0) + isnull(bal.total_interest,0) - isnull(bal.total_payments,0) > 0))

	-- Normal creditors must be active and have a valid disbursement date
	and		isnull(ccl.always_disburse,1) = 0
	and		c.active_status in (''A'', ''AR'')
	and		c.start_date < convert(datetime, ''' + convert(varchar(10), @debt_start_date, 101) + ' 00:00:00'')'

select @stmt = 'insert into disbursement_creditors (disbursement_register, client, creditor, client_creditor, bank, rpps_biller_id, tran_type, creditor_type, disbursement_factor, sched_payment, current_month_disbursement, orig_balance, current_balance, apr, debit_amt, fairshare_amt, fairshare_pct_check, fairshare_pct_eft, priority, held) ' + @stmt
-- print @stmt
exec ( @stmt )

-- If the scheduled payment is negative, make it zero.
-- It can go to negative if the debt has been overpaid. Just make it zero for later processing where
-- negatives mess things up.
update		disbursement_creditors
set		sched_payment			= 0
where		sched_payment			< 0
and		disbursement_register		= @disbursement_register;

-- Correct the transaction to AD if this does not have a RPPS payment id
update		disbursement_creditors
set		tran_type = 'AD'
where		tran_type = 'BW'
and		disbursement_register = @disbursement_register
and		isnull(rpps_biller_id,'0000000000') = '0000000000';

-- Correct the bank to the creditor's check bank if paying by check
update		disbursement_creditors
set		bank				= isnull(cr.check_bank,-1)
from		disbursement_creditors d
inner join	creditors cr with (nolock) on cr.creditor	= d.creditor
where		disbursement_register		= @disbursement_register
and		tran_type			= 'AD';

-- Fix the banks so that they are valid for a check bank if needed
declare		@check_bank		int
select		@check_bank	= min(bank)
from		banks with (nolock)
where		type = 'C'
and		[default] = 1

if @check_bank is null
	select		@check_bank	= min(bank)
	from		banks with (nolock)
	where		type = 'C'

if @check_bank is null
	select		@check_bank	= 1

-- Update the bank/tran type to indicate checks if there is no EFT override
update		disbursement_creditors
set		bank		= @check_bank
where		bank		< 0
and		disbursement_register = @disbursement_register

-- Correct the creditor types
update		disbursement_creditors
set		creditor_type		= case
						when d.tran_type = 'BW' then isnull(pct.creditor_type_eft,'N')
						else isnull(pct.creditor_type_check,'N')
					  end,

		fairshare_pct_check	= case
						when isnull(d.fairshare_pct_check,-1) < 0 then isnull(pct.fairshare_pct_check,0)
						else d.fairshare_pct_check
					  end,

		fairshare_pct_eft	= case
						when isnull(d.fairshare_pct_eft,-1) < 0 then isnull(pct.fairshare_pct_eft,0)
						else d.fairshare_pct_eft
					  end

from		disbursement_creditors d	with (nolock)
left outer join creditors cr			with (nolock) on d.creditor = cr.creditor
left outer join creditor_contribution_pcts pct  with (nolock) on cr.creditor_contribution_pct = pct.creditor_contribution_pct
where		disbursement_register = @disbursement_register

/*
-- Set the fairshare percentage to a special value if the creditor is normally deducted but the override is
-- zero and the creditor is on a bank-wire status.
update	disbursement_creditors
set	fairshare_pct_eft		= 0.0001			-- Will be recognized in "post" processing as having a non-zero value but having a zero fairshare.
from	disbursement_creditors d with (nolock)
inner join creditors cr with (nolock) on d.creditor = cr.creditor
left outer join creditor_contribution_pcts pct with (nolock) on cr.creditor_contribution_pct = pct.creditor_contribution_pct
where	d.fairshare_pct_eft		= 0				-- stored fairshare is zero
and	pct.creditor_type_eft		= 'D'				-- creditor type is deduct
and	pct.fairshare_pct_eft		> 0.0				-- creditor has a fairshare
and	d.tran_type			= 'BW'				-- bank wire creditor
and	d.disbursement_register		= @disbursement_register	-- and this disbursement
*/

-- Ensure that the held disbursements have no disbursement information
update	disbursement_creditors

set	tran_type			= 'AD',
	creditor_type			= 'N',
	debit_amt			= 0,
	fairshare_amt			= 0,
	fairshare_pct_check		= 0,
	fairshare_pct_eft		= 0

where	held				= 1
and	disbursement_register		= @disbursement_register

-- Calculate the total amount of the disbursement factors that are normally to be paid
select	client, sum(sched_payment) as disbursement_factor
into	#t_disbursement_payments
from	disbursement_creditors with (nolock)
where	disbursement_register = @disbursement_register
group by client

-- Compute the number of disbursement notes
select	client, count(*) as note_count
into	#t_disbursement_note_count
from	disbursement_notes with (nolock)
where	disbursement_register is null
and	type = 5
group by client

-- Update the client information with the totals to enable the proper selection
update		disbursement_clients
set		disbursement_factor	= isnull(b.disbursement_factor,0),
		note_count		= isnull(c.note_count,0)
from		disbursement_clients a with (nolock)
left outer join #t_disbursement_payments b on a.client = b.client
left outer join #t_disbursement_note_count c on a.client = c.client
where		disbursement_register = @disbursement_register

-- discard the working tables
drop table #t_disbursement_payments;
drop table #t_disbursement_note_count;

-- Discard the clients which have a zero for the disbursement factor sum. These are clients which existed in the database but have no valid debts.
delete
from		disbursement_clients
where		disbursement_register = @disbursement_register
and 		disbursement_factor <= 0

-- Find the creditors who have EFT payments and are being paid by check
select distinct client
into		#disbursement_held_clients

from		disbursement_creditors dc with (nolock)
inner join	creditors cr with (nolock) on dc.creditor = cr.creditor
left outer join	creditor_methods mt with (nolock) on cr.creditor_id = mt.creditor and 'EFT' = mt.type
inner join	banks b with (nolock) on mt.bank = b.bank and 'R' = b.type

where		dc.disbursement_register	= @disbursement_register
and		dc.tran_type			= 'AD'
and		dc.held				= 0		-- do not test held debts. set to checks earlier. this is not an error.

-- Generate a system note that we are dropping this client
insert into client_notes (client, type, dont_edit, dont_delete, dont_print, subject, note)
select		client, 3 as type, 1 as dont_edit, 1 as dont_delete, 0 as dont_print, 'Client removed from disbursement',
		'The client was removed from the disbursement batch # ' + convert(varchar, @disbursement_register) + ' because one or more debts that were to be paid to creditors had invalid account information and could not be paid. The creditor biller could not be found in the RPPS tables due to an invalid account.'
from		#disbursement_held_clients

-- Set the hold-disbursements flag for the client.
update		clients
set		hold_disbursements = 1
where		client in (
			select	client
			from	#disbursement_held_clients
		)

-- Delete the client from the disbursement batch
delete		disbursement_creditors
from		disbursement_creditors cr with (nolock)
inner join	#disbursement_held_clients c on cr.client = c.client and cr.disbursement_register = @disbursement_register

-- Delete the client from the disbursement batch
delete		disbursement_clients
from		disbursement_clients cl with (nolock)
inner join	#disbursement_held_clients c on cl.client = c.client and cl.disbursement_register = @disbursement_register

-- Discard the invalid client list
drop table	#disbursement_held_clients

-- Commit the transaction here
commit transaction

-- Return the disbursement batch ID
RETURN ( @disbursement_register )
GO
