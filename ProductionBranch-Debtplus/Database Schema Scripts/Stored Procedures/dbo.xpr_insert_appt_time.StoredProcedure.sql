USE [DebtPlus]
GO
/****** Object:  StoredProcedure [dbo].[xpr_insert_appt_time]    Script Date: 09/15/2014 13:13:35 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[xpr_insert_appt_time] (@start_time as datetime, @office as int, @appt_type as int = null) as
	-- ====================================================================================
	-- ==       create a row in the appt_times table                                     ==
	-- ====================================================================================
	insert into appt_times (start_time, office, appt_type)
	values (@start_time, @office, @appt_type)
	return ( scope_identity() )
GO
