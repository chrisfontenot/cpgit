USE [DebtPlus]
GO
/****** Object:  StoredProcedure [dbo].[rpt_Client_Statements]    Script Date: 09/15/2014 13:13:32 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[rpt_Client_Statements] ( @client_statement_batch as int = null ) as

-- ===========================================================================================
-- ==        Client statement information                                                   ==
-- ===========================================================================================

set nocount on

if @client_statement_batch is null
	select top 1	@client_statement_batch	= client_statement_batch
	from	client_statement_batches with (nolock)
	order by date_created desc

select	v.client,
	@client_statement_batch as client_statement_batch,
	v.deposit_amt,
	v.refund_amt,
	v.disbursement_amt,
	v.held_in_trust,
	v.reserved_in_trust,
	v.expected_deposit_date,
	v.expected_deposit_amt,
	v.counselor_name,
	v.company_id,

	b.type,
	b.disbursement_date,
	b.statement_date,
	b.period_start,
	b.period_end

from	view_client_statement_clients_extract v	with (nolock)
inner join client_statement_batches b	with (nolock) on v.client_statement_batch = b.client_statement_batch
inner join clients c			with (nolock) on v.client = c.client
left outer join addresses a with (nolock) on c.addressid = a.address
where	v.client_statement_batch = @client_statement_batch
and	c.mail_error_date is null

order by a.postalcode, c.client;

return ( @@rowcount )
GO
