SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER procedure [rpt_disbursement_detail]( @FromDate AS DateTime = NULL, @ToDate AS DateTime = NULL ) AS

-- =============================================================================================
-- ==               Fetch the information on payments to creditors                            ==
-- =============================================================================================

-- ChangeLog
--   3/14/2003
--     Allow for more than 10,000 creditors in a class
--   06/09/2009
--      Remove references to debt_number and use client_creditor

-- Suppress intermediate result sets
set nocount on

/*
-- Fetch the deduction creditor account
declare	@deduct_creditor	typ_creditor
select	@deduct_creditor = deduct_creditor
from	config
*/

IF @ToDate IS NULL
	SET @ToDate = getdate()

IF @FromDate IS NULL
	SET @FromDate = @ToDate

-- Adjust the dates for the proper settings
SELECT	@FromDate	= convert (datetime, convert(varchar(10), @FromDate, 101) + ' 00:00:00'),
	@ToDate		= convert (datetime, convert(varchar(10), @ToDate, 101)   + ' 23:59:59')

-- Retrieve the information from the database
SELECT	d.creditor				as 'creditor',
		cr.creditor_name		as 'creditor_name',
		cr.division				as 'division',
		d.date_created			as 'check_date',
		t.checknum				as 'checknum',
		d.client				as 'client',

		dbo.format_normal_name(pn.prefix,pn.first,pn.middle,pn.last,pn.suffix) AS 'name',
		coalesce (d.account_number, cc.account_number, 'MISSING') AS 'account_number',

		isnull(d.debit_amt,0)		as 'gross',
		case d.creditor_type when 'D' then d.fairshare_amt else 0 end as 'deducted',
		case d.creditor_type when 'B' then d.fairshare_amt else 0 end as 'billed',

		-- Should the amount be included in the gross figure.
--		case when d.creditor = @deduct_creditor then 0 else 1 end as 'ok_to_gross'
		1				as 'ok_to_gross'

FROM		registers_client_creditor d	WITH (NOLOCK)
LEFT OUTER JOIN	people p		WITH (NOLOCK) ON d.client = p.client AND 1 = p.relation
left outer join names pn with (nolock) on p.nameid = pn.name
INNER JOIN	client_creditor cc	WITH (NOLOCK) ON d.client_creditor = cc.client_creditor
INNER JOIN	registers_trust t	WITH (NOLOCK) ON d.trust_register = t.trust_register
INNER JOIN	creditors cr		WITH (NOLOCK) ON d.creditor = cr.creditor

WHERE		d.date_created BETWEEN @FromDate AND @ToDate
AND		d.tran_type in ('AD', 'MD', 'BW', 'CM')
ORDER BY	cr.type, cr.creditor_id, 1, 4, 6

RETURN ( @@rowcount )
GO
