USE [DebtPlus]
GO
/****** Object:  StoredProcedure [dbo].[xpr_sell_debt]    Script Date: 09/15/2014 13:13:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[xpr_sell_debt] (@old_record AS typ_key, @new_creditor AS typ_creditor, @generate_proposals as int = 0) AS
-- ===============================================================
-- ==      Sell the debt from one creditor to another           ==
-- ===============================================================

-- ChangeLog
--   1/13/2001
--   - Changed because the "first_payment_date", "first_payment_amount", and "last_payment_date" was moved
--     to the registers_client_creditor based upon first_payment and last_payment pointers.
--   - Changed date_completed to drop_date in client_creditor table
--   2/11/2002
--     Accessed the schema and made the column definitions totally "soft".
--   9/31/2002
--     Added support for client_creditor_balances table
--   12/10/2002
--     Removed the *_this_creditor fields from being copied to the new debt.
--   12/20/2004
--     Added creditor names to the system note
--   06/09/2009
--      Remove references to debt_number and use client_creditor
--   1/5/2009
--      Changed from using biller_aba to using rpps_biller_id

SET NOCOUNT ON
BEGIN TRANSACTION

-- Obtain the information from the new creditor
declare	@prohibit_use		bit
DECLARE @priority		int

declare	@old_creditor_name	varchar(50)
declare	@new_creditor_name	varchar(50)

SELECT	@priority		= isnull(usual_priority,9),
	@prohibit_use		= prohibit_use,
	@new_creditor_name	= creditor_name
FROM	creditors
WHERE	creditor		= @new_creditor

-- Ensure that the creditor exists
if @@rowcount < 1
begin
	Rollback Transaction
	RaisError(50029, 16, 1, @new_creditor)
	return ( 0 )
end

-- Complain if the creditor is marked "prohibit use"
if isnull(@prohibit_use,0) > 0
begin
	Rollback Transaction
	RaisError(50069, 16, 1, @new_creditor)
	return ( 0 )
end

-- Fetch the existing information from the record
declare @outstanding_debt	money
declare @old_creditor		typ_creditor
declare @orig_balance		money
declare	@client_creditor_balance int
declare @client			typ_client
declare	@reassigned_debt	bit
declare @disbursement_factor	money
declare @account_number		typ_client_account

SELECT
	@client_creditor_balance= cc.client_creditor_balance,
	@reassigned_debt	= reassigned_debt,
	@outstanding_debt	= isnull(bal.orig_balance,0) + isnull(bal.orig_balance_adjustment,0) + isnull(bal.total_interest,0) - isnull(bal.total_payments,0),
       	@old_creditor		= creditor,
       	@orig_balance		= bal.orig_balance,
       	@client			= client,
       	@disbursement_factor	= disbursement_factor,
       	@account_number		= account_number

FROM	client_creditor cc
INNER JOIN client_creditor_balances bal on cc.client_creditor_balance = bal.client_creditor_balance
WHERE	cc.client_creditor	= @old_record

-- The debt must not be currently "reassigned"
IF @reassigned_debt > 0
begin
	RaisError(50070, 16, 1)
	Rollback Transaction
	return ( 0 )
end

-- There is no need to do a reassignment on a closed account.
IF @outstanding_debt <= 0
BEGIN
	RaisError(50071, 10, 1)
	ROLLBACK TRANSACTION
	RETURN ( 0 )
END

declare @new_record int
/*
-- Determine the list of column names from the schema definition. It is more flexiable than hard-coding the column names here.
declare	@column_names		varchar(6000)
declare	@current_column_name	varchar(800)

declare	column_name_cursor cursor for
	select	name
	from	syscolumns
	where	id = (
		select	id
		from	sysobjects
		where	name = 'client_creditor'
		and	xtype = 'U'
	)
				-- Do not include the primary key nor the timestamp column.
	and	name not in (	'client_creditor',
				'timestamp',

				-- Do not include the payment columns
				'first_payment',
				'last_payment',

				-- Do not include the payment information. These need to be rebuilt.
				'rpps_mask',
				'rpps_mask_timestamp',
				'payment_rpps_mask',
				'payment_rpps_mask_timestamp',

				-- These columns are to be defaulted to their "default" values and not copied from the existing debt information
				'created_by',
				'date_created',
				'prenote_date')

		-- Do not include the statistics for this instance of the debt.
	and	name not like '%_this_creditor'

open column_name_cursor
fetch column_name_cursor into @current_column_name

select	@column_names = ''
while @@fetch_status = 0
begin
	select @column_names = ',[' + @current_column_name + ']' + @column_names 
	fetch column_name_cursor into @current_column_name
end
close column_name_cursor
deallocate column_name_cursor

-- Remove the leading comma from the column name string
select	@column_names = substring(@column_names, 2, 8000)

-- Move the creditor to the new ID value
declare	@stmt	varchar(8000)

select	@stmt = 'INSERT INTO [client_creditor] (payments_this_creditor,returns_this_creditor,interest_this_creditor,' + isnull(@column_names,'') + ') SELECT 0,0,0,' + isnull(@column_names,'') + ' FROM [client_creditor] WHERE [client_creditor]=' + convert(varchar, @old_record)
exec ( @stmt )
if @@error <> 0
begin
	rollback transaction
	return ( 0 )
end

select	@new_record = ident_current ( 'client_creditor' )

-- Correct the new item so that the defaulted values are now properly defined
update	client_creditor
set	creditor			= @new_creditor,
	payment_rpps_mask_timestamp	= null,
	rpps_mask_timestamp		= null
where	client_creditor			= @new_record
*/

-- Do things the "brute force" method to allow for multiple simultaenous updates of the debts table since ident_current is not valid at that time.
insert into client_creditor ( [payments_this_creditor],[returns_this_creditor],[interest_this_creditor],[client_creditor_balance],[client],[creditor],[creditor_name],[line_number],[priority],[drop_date],[drop_reason],[drop_reason_sent],[person],[client_name],[non_dmp_payment],[non_dmp_interest],[disbursement_factor],[date_disp_changed],[orig_dmp_payment],[months_delinquent],[start_date],[dmp_interest],[dmp_payout_interest],[last_stmt_date],[irs_form_on_file],[student_loan_release],[last_payment_date_b4_dmp],[expected_payout_date],[rpps_client_type_indicator],[terms],[percent_balance],[client_creditor_proposal],[contact_name],[sched_payment],[verify_request_date],[balance_verify_date],[balance_verify_by],[account_number],[message],[hold_disbursements],[send_bal_verify],[send_drop_notice],[last_communication],[reassigned_debt],[fairshare_pct_check],[fairshare_pct_eft],[check_payments],[payment_rpps_mask_timestamp],[rpps_mask_timestamp] )
select		0,0,0,[client_creditor_balance],[client],@new_creditor,[creditor_name],[line_number],[priority],[drop_date],[drop_reason],[drop_reason_sent],[person],[client_name],[non_dmp_payment],[non_dmp_interest],[disbursement_factor],[date_disp_changed],[orig_dmp_payment],[months_delinquent],[start_date],[dmp_interest],[dmp_payout_interest],[last_stmt_date],isnull([irs_form_on_file],0),isnull([student_loan_release],0),[last_payment_date_b4_dmp],[expected_payout_date],[rpps_client_type_indicator],[terms],[percent_balance],[client_creditor_proposal],[contact_name],[sched_payment],[verify_request_date],[balance_verify_date],[balance_verify_by],[account_number],[message],isnull([hold_disbursements],0),isnull([send_bal_verify],0),isnull([send_drop_notice],0),[last_communication],0 as [reassigned_debt],[fairshare_pct_check],[fairshare_pct_eft],isnull([check_payments],0),null,null
from		client_creditor
where		client_creditor = @old_record

if @@error <> 0
begin
	rollback transaction
	return ( 0 )
end

-- We should use scope_identity as this gives us the proper value for simultaneous updates.
select	@new_record = SCOPE_IDENTITY()

-- Ensure that the balance record points to the new debt
update	client_creditor_balances
SET		client_creditor		    = @new_record
where	client_creditor_balance	= @client_creditor_balance

-- Try to correct the EFT payment information
update		client_creditor
set			payment_rpps_mask			= mt.creditor_method,
			payment_rpps_mask_timestamp	= mt.[timestamp],
			rpps_mask					= mk.rpps_mask,
			rpps_mask_timestamp			= mk.[timestamp]
from		client_creditor cc with (nolock)
inner join	creditors cr with (nolock) on cc.creditor = cr.creditor
inner join	clients c with (nolock) on cc.client = c.client
inner join	creditor_methods mt with (nolock) on cr.creditor_id = mt.creditor AND 'EFT' = mt.type
inner join	rpps_biller_ids ids with (nolock) on mt.rpps_biller_id = ids.rpps_biller_id
inner join	rpps_masks mk with (nolock) on ids.rpps_biller_id = mk.rpps_biller_id
inner join	banks b with (nolock) on mt.bank = b.bank
where		cc.client_creditor		= @new_record
and		cc.account_number like dbo.map_rpps_masks (mk.mask)
and		mt.region in (0, c.region)
and		b.type = 'R'
and		ids.biller_type in (1, 2, 3, 11, 12, 13)

select	@old_creditor_name	= creditor_name
from	creditors
where	creditor		= @old_creditor

-- Add a system note that the debt was reassigned
INSERT INTO client_notes (type,dont_edit,dont_delete,client,is_text,subject,note) values (
	3, 1, 1, @client, 1,
	'Reassigned Debt to New Creditor',
	'Reassigned this debt to a new creditor'+
	char(13) + char(10)			+
	char(13) + char(10)			+
        'Old Debt: '				+
        @old_creditor				+
	isnull(' (' + @old_creditor_name + ')','') +
	char(13) + char(10)			+
        'New Debt: '				+
        @new_creditor				+
	isnull(' (' + @new_creditor_name + ')','') +
	char(13) + char(10)			+
	'Account Number: '			+
	@account_number				+
	char(13) + char(10)			+
        'Balance: $'				+
        convert(varchar, @outstanding_debt, 1)  +
	char(13) + char(10)			+
	'Disbursement Factor: $'		+
        convert(varchar, @disbursement_factor, 1)
)

-- Adjust the balance on the old creditor to indicate that it is paid off
UPDATE	client_creditor
SET		message					= @new_creditor + ' ' + substring(convert(varchar,getdate(),101), 1, 10),
        disbursement_factor		= 0,
		sched_payment			= 0,
		hold_disbursements		= 1,
        reassigned_debt			= 1,
		balance_verify_date		= getdate(),
		balance_verify_by		= 'REASSIGNED DEBT',
		drop_date				= getdate(),
		expected_payout_date	= getdate()
WHERE	client_creditor			= @old_record

-- Determine the new proposal status
if isnull(@generate_proposals,0) = 0
	select  @generate_proposals = 2	-- accepted by default
else
	select	@generate_proposals	= 1	-- pending

-- Generate a proposal record for the new creditor. It is now a new pending proposal.
declare	@client_creditor_proposal	int
EXECUTE	@client_creditor_proposal	= xpr_insert_client_creditor_proposal @new_record, @generate_proposals

COMMIT TRANSACTION
RETURN ( @new_record )
GO
