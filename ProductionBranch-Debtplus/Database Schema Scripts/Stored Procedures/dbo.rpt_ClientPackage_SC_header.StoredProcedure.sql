USE [DebtPlus]
GO
/****** Object:  StoredProcedure [dbo].[rpt_ClientPackage_SC_header]    Script Date: 09/15/2014 13:13:32 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[rpt_ClientPackage_SC_header] ( @Client AS INT ) AS
-- ===================================================================================
-- ==             Fetch the client header information                               ==
-- ===================================================================================

-- ChangeLog
--   9/31/2002
--     Added support for client_creditor_balances table
--   01/15/2002
--     Added creditor_classes table references and dropped "creditors.fee_creditor"

SET NOCOUNT ON

DECLARE	@Start_Date		DateTime
DECLARE	@Drop_Date		DateTime
DECLARE	@paf_creditor		typ_creditor
DECLARE @client_address		varchar(80)

select	@paf_creditor = paf_creditor
from	config

-- Fetch the starting and ending dates
SELECT	@Start_Date = start_date,
		@Drop_Date  = drop_date
FROM	clients
WHERE	client = @Client

DECLARE	@Original_Debt		MONEY
DECLARE	@Creditor_Count		INT
DECLARE @Deposit_day		int

-- Fetch the original debt and creditor count
SELECT		@Original_Debt = SUM(bal.orig_balance)
FROM		client_creditor cc
INNER JOIN	client_creditor_balances bal on cc.client_creditor_balance = bal.client_creditor_balance
INNER JOIN	creditors cr ON cc.creditor = cr.creditor
LEFT OUTER JOIN creditor_classes ccl WITH (NOLOCK) ON cr.creditor_class = ccl.creditor_class
WHERE		client = @Client
AND		isnull(ccl.zero_balance,0) = 0
AND		cc.reassigned_debt = 0

SELECT		@Creditor_Count = COUNT(*)
FROM		client_creditor cc
INNER JOIN	creditors cr ON cc.creditor = cr.creditor
LEFT OUTER JOIN creditor_classes ccl WITH (NOLOCK) ON cr.creditor_class = ccl.creditor_class
WHERE		client = @Client
AND		isnull(ccl.zero_balance,0) = 0
AND		cc.reassigned_debt = 0
AND		cc.creditor != @paf_creditor

DECLARE	@Current_Deposit	MONEY

-- Fetch the deposit amount
SELECT		@Current_Deposit = SUM(deposit_amount)
FROM		client_deposits
WHERE		client = @Client

/*
-- Determine the deposit date
select		@start_date = datepart(d, min(deposit_date))
from		client_deposits
where		client = @client
*/

select		@deposit_day = datepart(d, @start_date)

-- Fetch the person name
DECLARE	@Client_Name		VARCHAR(50)
DECLARE @Salutation			varchar(50)
SELECT	@Client_Name = name,
		@client_address = ltrim(rtrim(isnull(addr1,'') + isnull(' ' + addr2,'') + isnull(' ' + addr3,''))),
		@salutation = salutation
from	view_client_address
where	client	= @client

-- Find the counselor for the client
declare	@counselor	varchar(80)
select	@counselor = dbo.format_normal_name(default,cox.first,default,cox.last,default)
from	counselors co
left outer join names cox with (nolock) on co.NameID = cox.name
inner join clients c on co.counselor = c.counselor
where	c.client = @client

-- Return the result to the caller
SELECT	@counselor							as 'counselor',
		isnull(@deposit_day,1)				as 'deposit_day',
		isnull(@Start_Date,getdate())		AS 'start_date',
		@Original_Debt						AS 'original_debt',
		@Creditor_Count						AS 'creditor_count',
		@Current_Deposit					AS 'current_deposit',
		@Drop_Date							AS 'completion_date',
		@Client_Name						AS 'client_name',
		name								AS 'company_name',
		@client_address						as 'client_address',
		@salutation							as 'salutation'
FROM	config

RETURN ( @@rowcount )
GO
