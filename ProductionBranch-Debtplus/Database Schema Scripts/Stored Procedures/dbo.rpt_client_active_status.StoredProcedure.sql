USE [DebtPlus]
GO
/****** Object:  StoredProcedure [dbo].[rpt_client_active_status]    Script Date: 09/15/2014 13:13:32 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE PROCEDURE [dbo].[rpt_client_active_status] AS
-- ====================================================================================================
-- ==             Return the counts for the various types of the database                            ==
-- ====================================================================================================
-- This is really a stupid stored procedure. All it does is return the view which has all of the
-- information that we need. However, in order to hook the report into the system, we need to have
-- a stored procedure as the record source. So, here it is.
SELECT * FROM view_client_active_status;
RETURN ( @@rowcount )
GO
