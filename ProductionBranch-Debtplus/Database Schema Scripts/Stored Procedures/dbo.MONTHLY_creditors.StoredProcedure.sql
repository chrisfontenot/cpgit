USE [DebtPlus]
GO
/****** Object:  StoredProcedure [dbo].[MONTHLY_creditors]    Script Date: 09/15/2014 13:13:32 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE PROCEDURE [dbo].[MONTHLY_creditors] AS
-- =====================================================================================
-- ==          Update the monthly statistics for the creditors                        ==
-- =====================================================================================

-- 3/1/2011
--   Fold the annual processing into this job step to avoid problems in Richmond who likes to run the annual job on a monthly basis.

BEGIN TRANSACTION

-- If this is Janurary 1st, then clear the YTD information. Otherwise, accumulate it.
if DATEPART(m, getdate()) = 1
	UPDATE	creditors
	SET		distrib_ytd				= 0,
			contrib_ytd_billed		= 0,
			contrib_ytd_received	= 0;
else
	UPDATE	creditors
	SET		distrib_ytd				= distrib_ytd + distrib_mtd,
			contrib_ytd_billed		= contrib_ytd_billed + contrib_mtd_billed,
			contrib_ytd_received	= contrib_ytd_received + contrib_mtd_received;
			
-- Clear the montly counters
UPDATE	creditors
SET		distrib_mtd				= 0,
		contrib_mtd_billed		= 0,
		contrib_mtd_received	= 0

COMMIT TRANSACTION
GO
