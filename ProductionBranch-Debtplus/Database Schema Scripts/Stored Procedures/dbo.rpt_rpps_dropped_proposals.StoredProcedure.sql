USE [DebtPlus]
GO
/****** Object:  StoredProcedure [dbo].[rpt_rpps_dropped_proposals]    Script Date: 09/15/2014 13:13:33 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[rpt_rpps_dropped_proposals] as

-- ========================================================================================================================
-- ==           Look at the proposals that would go into the FBD transactions. Find the proposals for clients which      ==
-- ==           do not have budget detail and drop them from the batch.                                                  ==
-- ========================================================================================================================

-- ChangeLog
--   6/2/2003
--     Change to support "dual" (both proposals and payments) billers
--   06/09/2009
--      Remove references to debt_number and use client_creditor
--   1/5/2009
--      Changed from using biller_aba to using rpps_biller_id

-- Suppress intermediate results
set nocount on

-- Find the list of pending proposals
select		cc.client,
		c.region			as region,
		cc.account_number		as account_number,
		cc.creditor			as creditor,
		cr.creditor_id			as creditor_id,
		cc.client_creditor			as client_creditor,
		convert(varchar(10),null)	as biller_id,
		p.client_creditor_proposal	as client_creditor_proposal,
		convert(int,0)			as budget,
		convert(money,0)		as budget_amount
into		#budgets_1
FROM		proposal_batch_ids pb
INNER JOIN	client_creditor_proposals p	WITH (NOLOCK) ON pb.proposal_batch_id = p.proposal_batch_id
INNER JOIN	client_creditor cc		WITH (NOLOCK) ON p.client_creditor = cc.client_creditor
INNER JOIN	clients c			WITH (NOLOCK) ON cc.client = c.client
INNER JOIN	creditors cr			WITH (NOLOCK) ON cc.creditor = cr.creditor

WHERE		cr.full_disclosure > 0
AND		pb.date_closed IS NOT NULL		-- Closed but
AND		pb.date_transmitted IS NULL		-- Not transmitted
AND		c.active_status not in ('CRE', 'EX', 'I');

-- Find the budget ID
update		#budgets_1
set		budget = dbo.map_client_to_budget ( client );

-- Update the budget information
select		sum(b.suggested_amount) as budget_amount,
		a.budget
into		#budgets_2
from		budgets b
inner join	#budgets_1 a on a.budget = b.budget
where		a.budget > 0
group by	a.budget;

update		#budgets_1
set		budget_amount = c.budget_amount
from		#budgets_1 a
inner join	#budgets_2 c on a.budget = c.budget;

-- Discard all of the items which have a budget
delete
from		#budgets_1
where		budget_amount > 0;

-- Find the biller information for the missing items
update		#bugets_1
set		biller_id = cm.rpps_biller_id
from		#budgets_1 a
inner join	creditors cr on a.creditor = cr.creditor
inner join	creditor_methods cm on cr.creditor_id = cm.creditor and 'EDI' = cm.type and cm.region in (0, a.region)
inner join	banks b on cm.bank = b.bank and 'R' = b.type
inner join	rpps_biller_ids on ids.rpps_biller_id = cm.rpps_biller_id
inner join	rpps_masks mk on ids.rpps_biller_id = mk.rpps_biller_id
where		account_number like dbo.map_rpps_masks ( ids.mask );

-- Insert the transactions for the proposals which are not extracted
update	client_creditor_proposals
set	proposal_batch_id = null
where	client_creditor_proposal in (
	select		client_creditor_proposal
	from		#budgets_1 a
	inner join	#budgets_2 b on a.client = b.client
	where		isnull(b.budget_amount,0) <= 0
);

-- Return the list of deleted proposals from the batch
select		a.client,
		a.creditor,
		a.client_creditor,
		a.biller_id,
		a.client_creditor_proposal,
		cc.account_number,
		p.proposed_start_date
from		#budgets_1 a
inner join	#budgets_2 b on a.client = b.client
inner join client_creditor_proposals p on a.client_creditor_proposal = p.client_creditor_proposal
inner join client_creditor cc on p.client_creditor = cc.client_creditor
where		isnull(b.budget_amount,0) <= 0

return ( @@rowcount )
GO
