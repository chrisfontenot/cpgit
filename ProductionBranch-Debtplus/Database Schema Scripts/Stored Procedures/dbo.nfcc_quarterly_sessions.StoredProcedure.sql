USE [DebtPlus]
GO
/****** Object:  StoredProcedure [dbo].[nfcc_quarterly_sessions]    Script Date: 09/15/2014 13:13:32 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[nfcc_quarterly_sessions] ( @from_date as datetime = null, @to_date as datetime = null )  AS 

-- This is the name of the resulting sheet in the spreadsheet output
declare	@SheetName	varchar(80)
select	@SheetName	= 'Sessions'

-- Find last quarter if required
if (@to_date is null) and (@from_date is null)
begin
	select	@from_date	= min(dt) from calendar where y = datepart(year, getdate()) and q = (select q from calendar where dt = convert(varchar(10), getdate(), 101))
	select	@from_date	= dateadd(m, -3, @from_date)
	select	@to_date	= max(dt) from calendar where y = datepart(year, @from_date) and q = (select q from calendar where dt = convert(varchar(10), @from_date, 101))
end

select	@from_date	= convert(varchar(10), @from_date, 101) + ' 00:00:00',
	@to_date	= convert(varchar(10), @to_date, 101)   + ' 23:59:59';

-- Build a list of the clients who are counseled and their corresponding states
create table #sessions (client int, [state] int, mailing_code varchar(10) null, line int, [column] varchar(1));

-- One on One financial counseling sessions are easy
insert into #sessions (client, [state], mailing_code, line, [column])
select	c.client, a.state, convert(varchar(10), null) as mailing_code, convert(int,0) as line, convert(varchar(1), 'B') as [column]
from	client_appointments ca
inner join clients c on ca.client = c.client
left outer join Addresses a with (nolock) on c.AddressID = a.Address
inner join appt_types apt on ca.appt_type = apt.appt_type
where	ca.status in ('K','W')
and	ca.start_time between @from_date and @to_date
and	ca.office is not null
and	apt.housing = 0
and	apt.bankruptcy = 0;

-- Include housing one-on-one counseling sessions 
insert into #sessions (client, [state], mailing_code, line, [column])
select	c.client, a.state, convert(varchar(10), null) as mailing_code, convert(int,0) as line, convert(varchar(1), 'D') as [column]
from	client_appointments ca
inner join clients c on ca.client = c.client
left outer join Addresses a with (nolock) on c.AddressID = a.Address
inner join appt_types apt on ca.appt_type = apt.appt_type
where	ca.status in ('K','W')
and	ca.start_time between @from_date and @to_date
and	ca.office is not null
and	apt.housing <> 0
and	apt.appt_type not in (8, 28, 32, 33); -- exclude reverse mtg

-- Include housing reverse mtg counseling sessions
insert into #sessions (client, [state], mailing_code, line, [column])
select	c.client, a.state, convert(varchar(10), null) as mailing_code, convert(int,0) as line, convert(varchar(1), 'E') as [column]
from	client_appointments ca
inner join clients c on ca.client = c.client
left outer join Addresses a with (nolock) on c.AddressID = a.Address
inner join appt_types apt on ca.appt_type = apt.appt_type
where	ca.status in ('K','W')
and	ca.start_time between @from_date and @to_date
and	ca.office is not null
and	apt.housing <> 0
and	apt.appt_type in (8, 28, 32, 33);

-- Look for bankruptcy pre-filing counseling
insert into #sessions (client, [state], mailing_code, line, [column])
select	c.client, a.state, convert(varchar(10), null) as mailing_code, convert(int,0) as line, convert(varchar(1), 'G') as [column]
from	client_appointments ca
inner join clients c on ca.client = c.client
left outer join Addresses a with (nolock) on c.AddressID = a.Address
inner join appt_types apt on ca.appt_type = apt.appt_type
where	ca.status in ('K','W')
and	ca.start_time between @from_date and @to_date
and	ca.office is not null
and	apt.bankruptcy <> 0;

-- Workshop Sessions
select	c.client, a.state, convert(varchar(10), null) as mailing_code, convert(int,0) as line, convert(varchar(1), 'C') as [column], w.workshop_type
into	#workshops
from	client_appointments ca
inner join clients c on ca.client = c.client
left outer join Addresses a with (nolock) on c.AddressID = a.Address
inner join workshops w on ca.workshop = w.workshop
where	ca.status in ('K','W')
and	ca.start_time between @from_date and @to_date;

-- Change the column to housing if needed
update	#workshops
set	[column] = 'F'
from	#workshops w
inner join workshop_types wt on w.workshop_type = wt.workshop_type
inner join workshop_contents wc on wt.workshop_type = wc.workshop_type
inner join workshop_content_types wct on wc.content_type = wct.content_type
where	isnull(wct.hud_9902_section,'') in ('PPHE', 'FINL', 'RPMD', 'HMFM', 'FAHO', 'PDLD', 'RENT', 'OTHE')

-- Change the column to pre-discharge bankruptcy if needed
update	#workshops
set	[column] = 'H'
from	#workshops w
inner join workshop_types wt on w.workshop_type = wt.workshop_type
where	isnull(wt.description,'') like '%bankruptcy%'

-- Load the workshops
insert into #sessions (client, [state], mailing_code, line, [column])
select	client, [state], mailing_code, line, [column]
from	#workshops;

drop table #workshops;

-- Map the states to the mailing codes
update	#sessions
set	mailing_code = left(st.MailingCode,2)
from	#sessions s
inner join states st on s.state = st.state

-- Map the states to the corresponding line
update	#sessions set line =  8 where mailing_code = 'AK'
update	#sessions set line =  9 where mailing_code = 'AL'
update	#sessions set line = 10 where mailing_code = 'AR'
update	#sessions set line = 11 where mailing_code = 'AZ'
update	#sessions set line = 12 where mailing_code = 'CA'
update	#sessions set line = 13 where mailing_code = 'CO'
update	#sessions set line = 14 where mailing_code = 'CT'
update	#sessions set line = 15 where mailing_code = 'DC'
update	#sessions set line = 16 where mailing_code = 'DE'
update	#sessions set line = 17 where mailing_code = 'FL'
update	#sessions set line = 18 where mailing_code = 'GA'
update	#sessions set line = 19 where mailing_code = 'HI'
update	#sessions set line = 20 where mailing_code = 'IA'
update	#sessions set line = 21 where mailing_code = 'ID'
update	#sessions set line = 22 where mailing_code = 'IL'
update	#sessions set line = 23 where mailing_code = 'IN'
update	#sessions set line = 24 where mailing_code = 'KS'
update	#sessions set line = 25 where mailing_code = 'KY'
update	#sessions set line = 26 where mailing_code = 'LA'
update	#sessions set line = 27 where mailing_code = 'MA'
update	#sessions set line = 28 where mailing_code = 'MD'
update	#sessions set line = 29 where mailing_code = 'ME'
update	#sessions set line = 30 where mailing_code = 'MI'
update	#sessions set line = 31 where mailing_code = 'MN'
update	#sessions set line = 32 where mailing_code = 'MO'
update	#sessions set line = 33 where mailing_code = 'MS'
update	#sessions set line = 34 where mailing_code = 'MT'
update	#sessions set line = 35 where mailing_code = 'NC'
update	#sessions set line = 36 where mailing_code = 'ND'
update	#sessions set line = 37 where mailing_code = 'NE'
update	#sessions set line = 38 where mailing_code = 'NH'
update	#sessions set line = 39 where mailing_code = 'NJ'
update	#sessions set line = 40 where mailing_code = 'NM'
update	#sessions set line = 41 where mailing_code = 'NV'
update	#sessions set line = 42 where mailing_code = 'NY'
update	#sessions set line = 43 where mailing_code = 'OH'
update	#sessions set line = 44 where mailing_code = 'OK'
update	#sessions set line = 45 where mailing_code = 'OR'
update	#sessions set line = 46 where mailing_code = 'PA'
update	#sessions set line = 47 where mailing_code = 'PR'
update	#sessions set line = 48 where mailing_code = 'RI'
update	#sessions set line = 49 where mailing_code = 'SC'
update	#sessions set line = 50 where mailing_code = 'SD'
update	#sessions set line = 51 where mailing_code = 'TN'
update	#sessions set line = 52 where mailing_code = 'TX'
update	#sessions set line = 53 where mailing_code = 'UT'
update	#sessions set line = 54 where mailing_code = 'VA'
update	#sessions set line = 55 where mailing_code = 'VT'
update	#sessions set line = 56 where mailing_code = 'WA'
update	#sessions set line = 57 where mailing_code = 'WI'
update	#sessions set line = 58 where mailing_code = 'WV'
update	#sessions set line = 59 where mailing_code = 'WY'

-- Build a list of the disbursement information
create table #results (sheet varchar(80), location varchar(80), value varchar(80))

insert into #results (sheet, location, value)
select	@SheetName, [column] + convert(varchar, line) as location, convert(varchar, count(*)) as value
from	#sessions
where	[line] >= 8
group by [line], [column];

drop table #sessions;

-- Return the results
select * from #results where value is not null order by location;
drop table #results;
GO
