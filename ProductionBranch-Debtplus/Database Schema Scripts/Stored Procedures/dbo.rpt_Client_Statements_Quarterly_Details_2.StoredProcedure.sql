USE [DebtPlus]
GO
/****** Object:  StoredProcedure [dbo].[rpt_Client_Statements_Quarterly_Details_2]    Script Date: 09/15/2014 13:13:32 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE PROCEDURE [dbo].[rpt_Client_Statements_Quarterly_Details_2] ( @client_statement_batch_1 as int, @client_statement_batch_2 as int, @client_statement_batch_3 as int, @client as int ) as

set nocount on

-- ===========================================================================================
-- ==        Client statement information                                                   ==
-- ===========================================================================================

		-- Information from the batch
select	b.client_statement_batch							as 'client_statement_batch',
		b.period_start										as 'period_start',
		b.period_end										as 'period_end',
		
		-- Information from the payment detail
		d.client											as 'client',
		d.client_creditor									as 'client_creditor',
		d.current_balance									as 'current_balance',
		d.debits											as 'debits',
		d.credits											as 'credits',
		d.interest											as 'interest',
		d.payments_to_date									as 'payments_to_date',
		d.original_balance									as 'original_balance',
		d.interest_rate										as 'interest_rate',
		d.sched_payment										as 'sched_payment',

		-- Information from the last payment data
		rcc.date_created									as 'last_payment_date',
		rcc.debit_amt										as 'last_payment_amount',

		-- Information from the creditor record
		cc.creditor											as 'creditor',
		cr.creditor_name									as 'creditor_name',

		-- Information from the debt
		dbo.statement_account_number ( cc.account_number )	as 'account_number',

		-- Information from the client for the batch.
		c.deposit_amt										as 'deposit_amt'

from		client_statement_batches b with (nolock)
inner join	client_statement_details d with (nolock) on b.client_statement_batch = d.client_statement_batch
inner join	client_creditor cc with (nolock) on d.client_creditor = cc.client_creditor
inner join	creditors cr with (nolock) on cc.creditor = cr.creditor
inner join  client_statement_clients c with (nolock) on b.client_statement_batch = c.client_statement_batch and d.client = c.client
left outer join registers_client_creditor rcc with (nolock) on d.last_payment = rcc.client_creditor_register
where	b.client_statement_batch in (   @client_statement_batch_1,
										@client_statement_batch_2,
										@client_statement_batch_3 )
and		d.client = @client
order by client_statement_batch, client, client_creditor;

return ( @@rowcount )
GO
