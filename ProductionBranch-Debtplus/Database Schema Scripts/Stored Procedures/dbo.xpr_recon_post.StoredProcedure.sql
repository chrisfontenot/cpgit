USE [DebtPlus]
GO
/****** Object:  StoredProcedure [dbo].[xpr_recon_post]    Script Date: 09/15/2014 13:13:36 ******/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE PROCEDURE [dbo].[xpr_recon_post] ( @Batch AS INT ) AS

-- =====================================================================================================================
-- ==            Post the indicated batch to the trust register                                                       ==
-- =====================================================================================================================

-- Disable intermediate result sets
SET NOCOUNT ON
BEGIN TRANSACTION

-- Mark the items as reconciled for the indicated date
UPDATE		registers_trust
SET		cleared		= b.cleared,
		reconciled_date	= b.recon_date
FROM		registers_trust tr
INNER JOIN	recon_details	b ON tr.trust_register = b.trust_register
WHERE		b.recon_batch	= @Batch
AND		b.message	IS NULL
AND		b.cleared	= 'R'

/*
-- Update the reconciled balance figure
DECLARE	@Credits	Money

SELECT		@Credits	= sum(b.amount)
FROM		registers_trust tr
INNER JOIN	recon_details	 b ON tr.trust_register = b.trust_register
WHERE		tr.tran_type IN ('DP', 'BI', 'RR', 'RF')
AND		b.recon_batch	= @Batch
AND		b.message	IS NULL
AND		b.cleared	= 'R'

-- Determine the debits to the trust account which has cleared
DECLARE	@Debits		Money
SELECT		@Debits		= sum(b.amount)
FROM		registers_trust tr
INNER JOIN	recon_details	 b ON tr.trust_register = b.trust_register
WHERE		tr.tran_type NOT IN ('DP', 'BI', 'RR', 'RF')
AND		b.recon_batch	= @Batch
AND		b.message	IS NULL
AND		b.cleared	= 'R'

-- Update the reconciled trust balance
UPDATE	config
SET	cleared_trust_account_balance = isnull(cleared_trust_account_balance,0) - isnull(@Debits,0) + isnull(@Credits,0)
*/

-- Indicate that this batch is posted
UPDATE	recon_batches
SET	date_posted	= getdate()
WHERE	recon_batch	= @Batch

-- Return success to the caller
COMMIT TRANSACTION
RETURN ( 1 )
GO
