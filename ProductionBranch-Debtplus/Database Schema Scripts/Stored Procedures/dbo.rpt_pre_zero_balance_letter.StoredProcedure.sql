IF NOT EXISTS(SELECT * FROM SYSOBJECTS WHERE NAME='rpt_pre_zero_balance_letter')
	EXEC('CREATE PROCEDURE rpt_pre_zero_balance_letter AS')
GO
ALTER PROCEDURE [dbo].[rpt_pre_zero_balance_letter] as
-- ============================================================================================================
-- ==            List the debts that have less than 2 months to payout for the last AD disbursement          ==
-- ============================================================================================================

-- ChangeLog
--   06/09/2009
--      Remove references to debt_number and use client_creditor

-- Suppress intermediate results
set nocount on

-- Find the last disbursement register
declare @disbursement_register int
select TOP 1 @disbursement_register = disbursement_register
from registers_disbursement
order by 1 desc

-- There is nothing to print if there is no disbursement
if @disbursement_register is null
	return

-- Find the list of debts that were paid in the disbursement
select		cc.client, cc.creditor, cc.client_creditor, rcc.date_created, rcc.debit_amt, rcc.account_number, convert(money,1) as balance, convert(money,0) as disbursement_factor, b.client_creditor_balance
into		#paid_debts
from		registers_client_creditor rcc with (nolock)
inner join	client_creditor cc on rcc.client_creditor = cc.client_creditor
inner join	client_creditor_balances b on cc.client_creditor_balance = b.client_creditor_balance
where		rcc.tran_type in ('AD','BW','MD','CM')
and			rcc.disbursement_register = @disbursement_register
and			rcc.void = 0;

-- Find the balances for the debts
update		#paid_debts
set			balance				= b.orig_balance + b.orig_balance_adjustment + b.total_interest - b.total_payments,
			disbursement_factor	= cc.disbursement_factor
from		#paid_debts d
inner join	client_creditor cc with (nolock) on d.client_creditor = cc.client_creditor
inner join	client_creditor_balances b with (nolock) on cc.client_creditor_balance = b.client_creditor_balance
inner join	creditors cr with (nolock) on cc.creditor = cr.creditor
inner join	creditor_classes cl with (nolock) on cr.creditor_class = cl.creditor_class
where		cl.agency_account = 0;

/*
-- Generate a system note that we are going to print the letter
insert into client_notes (client, client_creditor, type, is_text, subject, dont_edit, dont_delete, dont_print, note)
select		client, client_creditor, 3, 1, 'Printing pre-zero-balance letter', 1, 1, 0, 'Printed the pre-zero balance letter from after the disbursement batch # ' + convert(varchar, @disbursement_register) + char(13) + char(10) + char(13) + char(10) + 'The balance was $' + convert(varchar, balance, 1)
from		#paid_debts
where		(balance <= (2.0 * disbursement_factor))
or		(balance = 0);
*/

-- Correct the items that were printed today
declare		@today		datetime
set			@today		= getdate()

update		client_creditor_balances
set			zero_bal_status = 3
from		client_creditor_balances b
inner join	#paid_debts x on b.client_creditor_balance = x.client_creditor_balance
where		zero_bal_status = 4

-- return the list of clients for the fields
select		a.postalcode, x.client, x.creditor, cr.creditor_name, dbo.statement_account_number(x.account_number) as account_number, x.debit_amt, x.date_created, x.balance
from		client_creditor cc
inner join	clients c on cc.client = c.client
left outer join addresses a on c.addressid = a.address
left outer join creditors cr on cc.creditor = cr.creditor
inner join	#paid_debts x on cc.client_creditor = x.client_creditor
inner join	client_creditor_balances b on cc.client_creditor_balance = b.client_creditor_balance
where		b.zero_bal_status = 3
order by	1, 2;

-- Mark the items that we are going to print now
update	client_creditor_balances
set		zero_bal_status	= 4,
		zero_bal_letter_date = getdate()
from	client_creditor_balances b
inner join #paid_debts x on b.client_creditor_balance = x.client_creditor_balance
where	b.zero_bal_status = 3

-- Drop the working table and exit
drop table	#paid_debts
return ( @@rowcount )
GO
GRANT EXECUTE ON rpt_pre_zero_balance_letter TO public AS dbo;
DENY EXECUTE ON rpt_pre_zero_balance_letter TO www_role;
GO
