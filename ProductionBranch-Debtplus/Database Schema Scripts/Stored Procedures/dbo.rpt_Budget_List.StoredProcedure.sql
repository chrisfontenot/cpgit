USE [DebtPlus]
GO
/****** Object:  StoredProcedure [dbo].[rpt_Budget_List]    Script Date: 09/15/2014 13:13:32 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[rpt_Budget_List] ( @budget as INT ) AS

-- =================================================================================================
-- ==            Print the contents of the budget for a specific budget ID                        ==
-- =================================================================================================

-- ChangeLog
--   12/19/2008
--      Revisions for SQL2008

-- Suppress intermediate result sets
set nocount on

-- Find the client from the budget. This is needed for the client specific budget descritpions.
declare	@client		int

select	@client = client
from	budgets
where	budget = @budget

-- Create a temporary table to hold the combined tables for system and user labels
create table #t_budget_categories (
	budget_category		int,
	[description]		varchar(80) null,
	heading			int,
	detail			int
);

-- Include the system categories into the table
insert into #t_budget_categories (budget_category, description, heading, detail)
select	sc.budget_category,
	sc.[description],
	sc.heading,
	sc.detail
from budget_categories sc;

-- Include any cilent categories
insert into #t_budget_categories (budget_category, description, heading, detail)
select	uc.budget_category,
	uc.[description],
	0 as heading,
	1 as detail
from	budget_categories_other uc
where	uc.client = @client;

-- Return the result set to the caller
select	b.budget_category,	-- for testing purposes only
	b.[description]		as 'description',
	b.heading		as 'heading',
	b.detail		as 'detail',
	d.client_amount		as 'client_amount',
	d.suggested_amount	as 'suggested_amount'

FROM    #t_budget_categories b
LEFT OUTER JOIN budget_detail d ON b.budget_category = d.budget_category AND @budget = d.budget 
order by 1;

-- Discard the working table
drop table #t_budget_categories;

-- Return the number of resulting rows in the result set
return ( @@rowcount )
GO
