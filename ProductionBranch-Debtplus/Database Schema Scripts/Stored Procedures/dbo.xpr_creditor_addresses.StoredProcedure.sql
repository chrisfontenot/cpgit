USE [DebtPlus]
GO
/****** Object:  StoredProcedure [dbo].[xpr_creditor_addresses]    Script Date: 09/15/2014 13:13:34 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE PROCEDURE [dbo].[xpr_creditor_addresses] (@creditor typ_creditor, @type as char(1)) AS
-- =================================================================================
-- ==        Retrieve the creditor name and address information                   ==
-- =================================================================================

-- Populate the table with the creditor's information
SET NOCOUNT ON

-- There are no addresses in the table. Punt. Just use the creditor name.
SET NOCOUNT OFF

declare	@attn		varchar(80)
declare	@address1	varchar(80)
declare	@address2	varchar(80)
declare	@address3	varchar(80)
declare	@address4	varchar(80)
declare	@address5	varchar(80)
declare	@address6	varchar(80)

-- Try to obtain the desired type
select	@attn		= attn,
		@address1	= addr1,
		@address2	= addr2,
		@address3	= addr3,
		@address4	= addr4,
		@address5	= addr5,
		@address6	= addr6
from	view_creditor_addresses with (nolock)
where	creditor = @creditor
and		type	= @type

-- If the record is not available, try other types
if @@rowcount = 0
begin
	-- Try the payment address first
	select	@attn		= attn,
			@address1	= addr1,
			@address2	= addr2,
			@address3	= addr3,
			@address4	= addr4,
			@address5	= addr5,
			@address6	= addr6
	from	view_creditor_addresses with (nolock)
	where	creditor	= @creditor
	and		type		= 'P'

	if @@rowcount = 0
		-- Try the proposal address next
		select	@attn		= attn,
				@address1	= addr1,
				@address2	= addr2,
				@address3	= addr3,
				@address4	= addr4,
				@address5	= addr5,
				@address6	= addr6
		from	view_creditor_addresses with (nolock)
		where	creditor	= @creditor
		and		type		= 'L'
end
	
create table #temp (address varchar(80) null, type varchar(1), line int)
insert into #temp (address,type,line) values ('ATTN: ' + @attn, @type, 1)
insert into #temp (address,type,line) values (@address1, @type, 2)
insert into #temp (address,type,line) values (@address2, @type, 3)
insert into #temp (address,type,line) values (@address3, @type, 4)
insert into #temp (address,type,line) values (@address4, @type, 5)
insert into #temp (address,type,line) values (@address5, @type, 6)
insert into #temp (address,type,line) values (@address6, @type, 7)

select address,type from #temp where address is not null order by line

drop table #temp

-- The temporary table is automatically destroyed when the recordset is closed
RETURN ( @@rowcount )
GO
