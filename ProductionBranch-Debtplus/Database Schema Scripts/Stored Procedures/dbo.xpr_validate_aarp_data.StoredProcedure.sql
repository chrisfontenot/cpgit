USE [DebtPlus]
GO
/****** Object:  StoredProcedure [dbo].[cccs_admin_intake_appointment]    Script Date: 09/15/2014 13:13:31 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


create procedure xpr_validate_aarp_data
	@call_resolution int
as
begin

	declare @no_appt	bit
	declare @immediate_appt	bit
	declare @schedule_appt	bit
	declare @reschedule_appt	bit
	declare @cancel_appt	bit

	-- set the default value of the flags to false
	set @no_appt         = 0
	set @immediate_appt  = 0
	set @schedule_appt   = 0
	set @reschedule_appt = 0
	set @cancel_appt     = 0

	--1	Answered General Question 
	--2	Call Drop
	-- 3	Cancel Availability
	-- 4	Colonial Penn
	-- 5	Referred to HUD (Rent, Repair)
	--6	Remove from list
	--7	Transferred to AARP FDN
	--8	Transferred to Counselor
	-- 9	Scheduled a New Appointment
	-- 10	Rescheduled Appointment

	declare @CancelAvailability int
	set @CancelAvailability = 3

	declare @ColonialPenn int
	set @ColonialPenn = 4

	declare @ReferredToHUD int
	set @ReferredToHUD = 5

	declare @ScheduledNewAppointment int
	set @ScheduledNewAppointment = 9

	declare @RescheduledAppointment int
	set @RescheduledAppointment = 10

	if @call_resolution is not null
	begin

		if @call_resolution = @ScheduledNewAppointment or @call_resolution = @ColonialPenn or @call_resolution = @ReferredToHUD
		begin
			set @no_appt         = 0
			set @immediate_appt  = 1
			set @schedule_appt   = 1
			set @reschedule_appt = 0
			set @cancel_appt     = 0
		end
		else
		begin
			if @call_resolution = @RescheduledAppointment
			begin
				set @no_appt         = 0
				set @immediate_appt  = 0
				set @schedule_appt   = 0
				set @reschedule_appt = 1
				set @cancel_appt     = 0
			end
			else 
			begin
				if @call_resolution = @CancelAvailability
				begin
					set @no_appt         = 0
					set @immediate_appt  = 0
					set @schedule_appt   = 0
					set @reschedule_appt = 0
					set @cancel_appt     = 1
				end
				else
				begin
					set @no_appt         = 1
					set @immediate_appt  = 0
					set @schedule_appt   = 0
					set @reschedule_appt = 0
					set @cancel_appt     = 0
				end
			end
		end
	end

	select @no_appt as NoAppointment, 
		   @immediate_appt as ImmediateAppointment, 
		   @schedule_appt as ScheduledAppointment, 
		   @reschedule_appt as RescheduleAppointment, 
		   @cancel_appt as CancelAppointment

end