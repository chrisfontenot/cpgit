USE [DebtPlus]
GO
/****** Object:  StoredProcedure [dbo].[xpr_deposit_batch_detail]    Script Date: 09/15/2014 13:13:35 ******/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE PROCEDURE [dbo].[xpr_deposit_batch_detail] (@deposit_batch_id as INT) AS
-- ========================================================================
-- ==          Fetch the values for the Client Deposits                  ==
-- ========================================================================

-- Return the client and the deposit amount for the various items in the deposit batch
SELECT	deposit_batch_detail		as 'item_key',
	amount				as 'amount',
	client				as 'client'
FROM	deposit_batch_details
WHERE	deposit_batch_id	= @deposit_batch_id

RETURN ( @@rowcount )
GO
