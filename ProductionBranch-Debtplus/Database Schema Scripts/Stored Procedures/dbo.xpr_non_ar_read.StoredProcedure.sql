USE [DebtPlus]
GO
/****** Object:  StoredProcedure [dbo].[xpr_non_ar_read]    Script Date: 09/15/2014 13:13:36 ******/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE PROCEDURE [dbo].[xpr_non_ar_read] ( @non_ar_register AS INT ) AS

-- ===============================================================================================
-- ==            Generate a transfer from one operating account to another                      ==
-- ===============================================================================================

select		r.tran_type		as 'tran_type',
		r.client		as 'client',
		r.creditor		as 'creditor',
		r.non_ar_source		as 'non_ar_source',
		d3.description		as 'non_ar_description',
		r.credit_amt		as 'credit_amt',
		r.debit_amt		as 'debit_amt',
		r.date_created		as 'date_created',
		r.src_ledger_account	as 'src_ledger_account',
		d1.description		as 'src_ledger_description',
		r.dst_ledger_account	as 'dst_ledger_account',
		d2.description		as 'dst_ledger_description',
		r.message		as 'message'

from		registers_non_ar r
left outer join	ledger_codes d1 on r.src_ledger_account = d1.ledger_code
left outer join	ledger_codes d2 on r.dst_ledger_account = d1.ledger_code
left outer join	non_ar_sources d3 on r.non_ar_source = d3.non_ar_source

where		r.non_ar_source = @non_ar_register

-- Terminate the transaction
RETURN ( @@rowcount )
GO
