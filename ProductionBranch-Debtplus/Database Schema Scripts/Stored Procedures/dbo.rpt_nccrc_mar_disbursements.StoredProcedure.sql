USE [DebtPlus]
GO
/****** Object:  StoredProcedure [dbo].[rpt_nccrc_mar_disbursements]    Script Date: 09/15/2014 13:13:33 ******/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE PROCEDURE [dbo].[rpt_nccrc_mar_disbursements] ( @FromDate as datetime = null, @ToDate as datetime = null ) AS

-- ==========================================================================================
-- ==            Return the information for sheet #12 for the NCCRC Annual Report sheet 12 ==
-- ==                                        SPECIFIC COMPANY DISBURSED 1                  ==
-- ==========================================================================================

set nocount on

if @ToDate is null
begin
	select	@ToDate   = dateadd(yy, 1, @ToDate);
	select	@ToDate   = dateadd(s, -1, @ToDate);
end

if @FromDate is null
	select	@FromDate = convert(datetime, '01/01/' + convert(varchar(10), year(@ToDate) - 1) + ' 00:00:00');

select	@FromDate = convert(datetime, convert(varchar(10), @FromDate, 101) + ' 00:00:00'),
	@ToDate   = convert(datetime, convert(varchar(10), @ToDate,   101) + ' 23:59:59')

-- This is the table with the SIC codes for the selection criteria
create table #nccrc_sic (page_no int, line_no int, sic varchar(20));

-- Construct the list of sic codes and the corresponding line for the result
insert into #nccrc_sic (page_no, line_no, sic) values (1,  4, 'N0159601125');
insert into #nccrc_sic (page_no, line_no, sic) values (1,  4, 'N0159600014');
insert into #nccrc_sic (page_no, line_no, sic) values (1,  4, 'N0159600011');
insert into #nccrc_sic (page_no, line_no, sic) values (1,  4, 'N0159600001');
insert into #nccrc_sic (page_no, line_no, sic) values (1,  5, 'N0004600737');
insert into #nccrc_sic (page_no, line_no, sic) values (1,  6, 'N0297600697');
insert into #nccrc_sic (page_no, line_no, sic) values (1,  7, 'N0257600295');
insert into #nccrc_sic (page_no, line_no, sic) values (1,  7, 'N0257600090');
insert into #nccrc_sic (page_no, line_no, sic) values (1,  8, 'N0135600001');
insert into #nccrc_sic (page_no, line_no, sic) values (1,  8, 'N0135600657');
insert into #nccrc_sic (page_no, line_no, sic) values (1,  8, 'N0135600659');
insert into #nccrc_sic (page_no, line_no, sic) values (1,  8, 'N0135600660');
insert into #nccrc_sic (page_no, line_no, sic) values (1,  8, 'N0135600675');
insert into #nccrc_sic (page_no, line_no, sic) values (1,  8, 'N0135600658');
insert into #nccrc_sic (page_no, line_no, sic) values (1,  8, 'N0135600661');
insert into #nccrc_sic (page_no, line_no, sic) values (1,  8, 'N0135600674');
insert into #nccrc_sic (page_no, line_no, sic) values (1,  8, 'N0135600544');
insert into #nccrc_sic (page_no, line_no, sic) values (1,  8, 'N0135600541');
insert into #nccrc_sic (page_no, line_no, sic) values (1,  8, 'N0135600024');
insert into #nccrc_sic (page_no, line_no, sic) values (1,  8, 'L0205600008');
insert into #nccrc_sic (page_no, line_no, sic) values (1,  9, 'N0135600002');
insert into #nccrc_sic (page_no, line_no, sic) values (1, 10, 'N0135600527');
insert into #nccrc_sic (page_no, line_no, sic) values (1, 10, 'N0135600541');
insert into #nccrc_sic (page_no, line_no, sic) values (1, 10, 'N0135600024');
insert into #nccrc_sic (page_no, line_no, sic) values (1, 10, 'N0135600197');
insert into #nccrc_sic (page_no, line_no, sic) values (1, 11, 'N0318600135');
insert into #nccrc_sic (page_no, line_no, sic) values (1, 12, 'N0323600714');
insert into #nccrc_sic (page_no, line_no, sic) values (1, 13, 'N0136600004');
insert into #nccrc_sic (page_no, line_no, sic) values (1, 14, 'N0153600051');
insert into #nccrc_sic (page_no, line_no, sic) values (1, 15, 'N0025600524');
insert into #nccrc_sic (page_no, line_no, sic) values (1, 15, 'N0025600004');
insert into #nccrc_sic (page_no, line_no, sic) values (1, 15, 'N0025600382');
insert into #nccrc_sic (page_no, line_no, sic) values (1, 15, 'N0025600663');
insert into #nccrc_sic (page_no, line_no, sic) values (1, 16, 'R0104600669');
insert into #nccrc_sic (page_no, line_no, sic) values (1, 16, 'R0127600034');
insert into #nccrc_sic (page_no, line_no, sic) values (1, 17, 'N0189600490');
insert into #nccrc_sic (page_no, line_no, sic) values (1, 18, 'N0097600688');
insert into #nccrc_sic (page_no, line_no, sic) values (1, 19, 'N0161600014');
insert into #nccrc_sic (page_no, line_no, sic) values (1, 20, 'N0025600663');
insert into #nccrc_sic (page_no, line_no, sic) values (1, 21, 'N0078600031');
insert into #nccrc_sic (page_no, line_no, sic) values (1, 21, 'N0078600023');
insert into #nccrc_sic (page_no, line_no, sic) values (1, 21, 'N0078600026');
insert into #nccrc_sic (page_no, line_no, sic) values (1, 21, 'N0078600021');
insert into #nccrc_sic (page_no, line_no, sic) values (1, 21, 'N0078600028');
insert into #nccrc_sic (page_no, line_no, sic) values (1, 21, 'N0078600030');
insert into #nccrc_sic (page_no, line_no, sic) values (1, 21, 'N0078600029');
insert into #nccrc_sic (page_no, line_no, sic) values (1, 21, 'N0078600027');
insert into #nccrc_sic (page_no, line_no, sic) values (1, 21, 'N0078600680');
insert into #nccrc_sic (page_no, line_no, sic) values (1, 22, 'N0078600475');
insert into #nccrc_sic (page_no, line_no, sic) values (1, 23, 'N0228600009');
insert into #nccrc_sic (page_no, line_no, sic) values (1, 24, 'R0164600197');
insert into #nccrc_sic (page_no, line_no, sic) values (1, 25, 'N0031600024');
insert into #nccrc_sic (page_no, line_no, sic) values (1, 25, 'N0031600041');
insert into #nccrc_sic (page_no, line_no, sic) values (1, 25, 'N0031600552');
insert into #nccrc_sic (page_no, line_no, sic) values (1, 25, 'N0031600051');
insert into #nccrc_sic (page_no, line_no, sic) values (1, 26, 'N0011600698');
insert into #nccrc_sic (page_no, line_no, sic) values (1, 26, 'N0011600006');
insert into #nccrc_sic (page_no, line_no, sic) values (1, 27, 'N0152600121');
insert into #nccrc_sic (page_no, line_no, sic) values (1, 28, 'N0322600713');
insert into #nccrc_sic (page_no, line_no, sic) values (1, 29, 'N0242600104');
insert into #nccrc_sic (page_no, line_no, sic) values (1, 30, 'N0071600520');
insert into #nccrc_sic (page_no, line_no, sic) values (1, 31, 'R0231600040');
insert into #nccrc_sic (page_no, line_no, sic) values (1, 31, 'R0231600552');
insert into #nccrc_sic (page_no, line_no, sic) values (1, 32, 'N0209600572');
insert into #nccrc_sic (page_no, line_no, sic) values (1, 32, 'N0209600604');
insert into #nccrc_sic (page_no, line_no, sic) values (1, 32, 'N0209600669');
insert into #nccrc_sic (page_no, line_no, sic) values (1, 33, 'N0051600001');
insert into #nccrc_sic (page_no, line_no, sic) values (1, 34, 'N0064600075');
insert into #nccrc_sic (page_no, line_no, sic) values (1, 34, 'N0025600713');
insert into #nccrc_sic (page_no, line_no, sic) values (1, 34, 'N0025600084');
insert into #nccrc_sic (page_no, line_no, sic) values (1, 35, 'N0065600008');
insert into #nccrc_sic (page_no, line_no, sic) values (1, 35, 'N0065600030');
insert into #nccrc_sic (page_no, line_no, sic) values (1, 35, 'N0065600095');
insert into #nccrc_sic (page_no, line_no, sic) values (1, 35, 'N0065600167');
insert into #nccrc_sic (page_no, line_no, sic) values (1, 35, 'N0065600523');

-- Finance Creditors
insert into #nccrc_sic (page_no, line_no,sic) values (2,  4,'N0145640357');
insert into #nccrc_sic (page_no, line_no,sic) values (2,  5,'N0003640161');
insert into #nccrc_sic (page_no, line_no,sic) values (2,  6,'N0003640003');
insert into #nccrc_sic (page_no, line_no,sic) values (2,  7,'N0003640020');
insert into #nccrc_sic (page_no, line_no,sic) values (2,  7,'N0003640018');
insert into #nccrc_sic (page_no, line_no,sic) values (2,  7,'N0003640004');
insert into #nccrc_sic (page_no, line_no,sic) values (2,  7,'N0003640016');
insert into #nccrc_sic (page_no, line_no,sic) values (2,  7,'N0003640161');
insert into #nccrc_sic (page_no, line_no,sic) values (2,  7,'N0003640003');
insert into #nccrc_sic (page_no, line_no,sic) values (2,  8,'N0260640188');
insert into #nccrc_sic (page_no, line_no,sic) values (2,  9,'N0204570013');
insert into #nccrc_sic (page_no, line_no,sic) values (2, 10,'N0135640009');
insert into #nccrc_sic (page_no, line_no,sic) values (2, 10,'N0135570030');
insert into #nccrc_sic (page_no, line_no,sic) values (2, 10,'N0135640020');
insert into #nccrc_sic (page_no, line_no,sic) values (2, 11,'N0261640312');
insert into #nccrc_sic (page_no, line_no,sic) values (2, 12,'N0189640019');
insert into #nccrc_sic (page_no, line_no,sic) values (2, 14,'N0170570002');
insert into #nccrc_sic (page_no, line_no,sic) values (2, 14,'N0170570003');
insert into #nccrc_sic (page_no, line_no,sic) values (2, 14,'N0162570001');
insert into #nccrc_sic (page_no, line_no,sic) values (2, 15,'N0078640001');
insert into #nccrc_sic (page_no, line_no,sic) values (2, 15,'N0078640407');
insert into #nccrc_sic (page_no, line_no,sic) values (2, 15,'N0078640032');
insert into #nccrc_sic (page_no, line_no,sic) values (2, 15,'N0078640414');
insert into #nccrc_sic (page_no, line_no,sic) values (2, 15,'N0078640419');
insert into #nccrc_sic (page_no, line_no,sic) values (2, 16,'N0037640421');

-- Retail Creditors
insert into #nccrc_sic (page_no, line_no,sic) values (2, 20,'N0244530943');
insert into #nccrc_sic (page_no, line_no,sic) values (2, 20,'N0244530023');
insert into #nccrc_sic (page_no, line_no,sic) values (2, 21,'N0073530533');
insert into #nccrc_sic (page_no, line_no,sic) values (2, 21,'N0073530821');
insert into #nccrc_sic (page_no, line_no,sic) values (2, 21,'N0073530823');
insert into #nccrc_sic (page_no, line_no,sic) values (2, 21,'N0073530831');
insert into #nccrc_sic (page_no, line_no,sic) values (2, 21,'N0073530725');
insert into #nccrc_sic (page_no, line_no,sic) values (2, 21,'N0073530835');
insert into #nccrc_sic (page_no, line_no,sic) values (2, 21,'N0073530824');
insert into #nccrc_sic (page_no, line_no,sic) values (2, 21,'N0073530848');
insert into #nccrc_sic (page_no, line_no,sic) values (2, 21,'N0073530833');
insert into #nccrc_sic (page_no, line_no,sic) values (2, 21,'N0073530826');
insert into #nccrc_sic (page_no, line_no,sic) values (2, 21,'N0073530528');
insert into #nccrc_sic (page_no, line_no,sic) values (2, 21,'N0073530722');
insert into #nccrc_sic (page_no, line_no,sic) values (2, 21,'N0073530827');
insert into #nccrc_sic (page_no, line_no,sic) values (2, 21,'N0073530531');
insert into #nccrc_sic (page_no, line_no,sic) values (2, 21,'N0073530825');
insert into #nccrc_sic (page_no, line_no,sic) values (2, 21,'N0073530840');
insert into #nccrc_sic (page_no, line_no,sic) values (2, 21,'N0073530534');
insert into #nccrc_sic (page_no, line_no,sic) values (2, 21,'N0073530845');
insert into #nccrc_sic (page_no, line_no,sic) values (2, 21,'N0073530029');
insert into #nccrc_sic (page_no, line_no,sic) values (2, 21,'N0073530030');
insert into #nccrc_sic (page_no, line_no,sic) values (2, 21,'N0073530843');
insert into #nccrc_sic (page_no, line_no,sic) values (2, 21,'N0073530838');
insert into #nccrc_sic (page_no, line_no,sic) values (2, 21,'N0073530284');
insert into #nccrc_sic (page_no, line_no,sic) values (2, 21,'N0073530830');
insert into #nccrc_sic (page_no, line_no,sic) values (2, 21,'N0073530822');
insert into #nccrc_sic (page_no, line_no,sic) values (2, 21,'N0073530532');
insert into #nccrc_sic (page_no, line_no,sic) values (2, 21,'N0073530828');
insert into #nccrc_sic (page_no, line_no,sic) values (2, 21,'N0073530724');
insert into #nccrc_sic (page_no, line_no,sic) values (2, 21,'N0073530841');
insert into #nccrc_sic (page_no, line_no,sic) values (2, 21,'N0073530844');
insert into #nccrc_sic (page_no, line_no,sic) values (2, 21,'N0073530529');
insert into #nccrc_sic (page_no, line_no,sic) values (2, 21,'N0073530536');
insert into #nccrc_sic (page_no, line_no,sic) values (2, 21,'N0073530842');
insert into #nccrc_sic (page_no, line_no,sic) values (2, 21,'N0073530832');
insert into #nccrc_sic (page_no, line_no,sic) values (2, 21,'N0073530839');
insert into #nccrc_sic (page_no, line_no,sic) values (2, 21,'N0073530535');
insert into #nccrc_sic (page_no, line_no,sic) values (2, 21,'N0073530834');
insert into #nccrc_sic (page_no, line_no,sic) values (2, 21,'N0073530847');
insert into #nccrc_sic (page_no, line_no,sic) values (2, 21,'N0073530836');
insert into #nccrc_sic (page_no, line_no,sic) values (2, 21,'N0073530954');
insert into #nccrc_sic (page_no, line_no,sic) values (2, 21,'N0073530955');
insert into #nccrc_sic (page_no, line_no,sic) values (2, 21,'N0073530956');
insert into #nccrc_sic (page_no, line_no,sic) values (2, 21,'N0073530957');
insert into #nccrc_sic (page_no, line_no,sic) values (2, 21,'N0073530958');
insert into #nccrc_sic (page_no, line_no,sic) values (2, 21,'N0073530959');
insert into #nccrc_sic (page_no, line_no,sic) values (2, 21,'N0073530960');
insert into #nccrc_sic (page_no, line_no,sic) values (2, 21,'N0073530961');
insert into #nccrc_sic (page_no, line_no,sic) values (2, 21,'N0073530962');
insert into #nccrc_sic (page_no, line_no,sic) values (2, 21,'N0073530963');
insert into #nccrc_sic (page_no, line_no,sic) values (2, 21,'N0073530964');
insert into #nccrc_sic (page_no, line_no,sic) values (2, 21,'N0073530965');
insert into #nccrc_sic (page_no, line_no,sic) values (2, 21,'N0073530966');
insert into #nccrc_sic (page_no, line_no,sic) values (2, 21,'N0073530967');
insert into #nccrc_sic (page_no, line_no,sic) values (2, 21,'N0073530968');
insert into #nccrc_sic (page_no, line_no,sic) values (2, 21,'N0073530969');
insert into #nccrc_sic (page_no, line_no,sic) values (2, 21,'N0073530970');
insert into #nccrc_sic (page_no, line_no,sic) values (2, 21,'N0073530971');
insert into #nccrc_sic (page_no, line_no,sic) values (2, 21,'N0073530972');
insert into #nccrc_sic (page_no, line_no,sic) values (2, 21,'N0073530973');
insert into #nccrc_sic (page_no, line_no,sic) values (2, 21,'N0073530974');
insert into #nccrc_sic (page_no, line_no,sic) values (2, 21,'N0073530975');
insert into #nccrc_sic (page_no, line_no,sic) values (2, 21,'N0073530976');
insert into #nccrc_sic (page_no, line_no,sic) values (2, 21,'N0073530977');
insert into #nccrc_sic (page_no, line_no,sic) values (2, 21,'N0073530978');
insert into #nccrc_sic (page_no, line_no,sic) values (2, 21,'N0073530979');
insert into #nccrc_sic (page_no, line_no,sic) values (2, 23,'N0003530900');
insert into #nccrc_sic (page_no, line_no,sic) values (2, 23,'N0003530901');
insert into #nccrc_sic (page_no, line_no,sic) values (2, 22,'N0003530900');
insert into #nccrc_sic (page_no, line_no,sic) values (2, 22,'N0003530901');
insert into #nccrc_sic (page_no, line_no,sic) values (2, 22,'N0072530928');
insert into #nccrc_sic (page_no, line_no,sic) values (2, 22,'N0072530753');
insert into #nccrc_sic (page_no, line_no,sic) values (2, 22,'N0072530914');
insert into #nccrc_sic (page_no, line_no,sic) values (2, 22,'N0072530875');
insert into #nccrc_sic (page_no, line_no,sic) values (2, 22,'N0072530923');
insert into #nccrc_sic (page_no, line_no,sic) values (2, 22,'N0072530754');
insert into #nccrc_sic (page_no, line_no,sic) values (2, 22,'N0072630009');
insert into #nccrc_sic (page_no, line_no,sic) values (2, 22,'N0072530934');
insert into #nccrc_sic (page_no, line_no,sic) values (2, 22,'N0072530884');
insert into #nccrc_sic (page_no, line_no,sic) values (2, 22,'N0072530876');
insert into #nccrc_sic (page_no, line_no,sic) values (2, 22,'N0072530755');
insert into #nccrc_sic (page_no, line_no,sic) values (2, 22,'N0072530756');
insert into #nccrc_sic (page_no, line_no,sic) values (2, 22,'N0072530808');
insert into #nccrc_sic (page_no, line_no,sic) values (2, 22,'N0072530926');
insert into #nccrc_sic (page_no, line_no,sic) values (2, 22,'N0072530878');
insert into #nccrc_sic (page_no, line_no,sic) values (2, 22,'N0072630013');
insert into #nccrc_sic (page_no, line_no,sic) values (2, 22,'N0072530916');
insert into #nccrc_sic (page_no, line_no,sic) values (2, 22,'N0072530937');
insert into #nccrc_sic (page_no, line_no,sic) values (2, 22,'N0072530879');
insert into #nccrc_sic (page_no, line_no,sic) values (2, 22,'N0072530929');
insert into #nccrc_sic (page_no, line_no,sic) values (2, 22,'N0072530809');
insert into #nccrc_sic (page_no, line_no,sic) values (2, 22,'N0072530919');
insert into #nccrc_sic (page_no, line_no,sic) values (2, 22,'N0072530802');
insert into #nccrc_sic (page_no, line_no,sic) values (2, 22,'N0072530917');
insert into #nccrc_sic (page_no, line_no,sic) values (2, 22,'N0072530155');
insert into #nccrc_sic (page_no, line_no,sic) values (2, 22,'N0072530930');
insert into #nccrc_sic (page_no, line_no,sic) values (2, 22,'N0072530935');
insert into #nccrc_sic (page_no, line_no,sic) values (2, 22,'N0072530811');
insert into #nccrc_sic (page_no, line_no,sic) values (2, 22,'N0072530918');
insert into #nccrc_sic (page_no, line_no,sic) values (2, 22,'N0072530758');
insert into #nccrc_sic (page_no, line_no,sic) values (2, 22,'N0072530933');
insert into #nccrc_sic (page_no, line_no,sic) values (2, 22,'N0072530791');
insert into #nccrc_sic (page_no, line_no,sic) values (2, 22,'N0072530896');
insert into #nccrc_sic (page_no, line_no,sic) values (2, 22,'N0072530447');
insert into #nccrc_sic (page_no, line_no,sic) values (2, 22,'N0072530762');
insert into #nccrc_sic (page_no, line_no,sic) values (2, 22,'N0072530759');
insert into #nccrc_sic (page_no, line_no,sic) values (2, 22,'N0072530915');
insert into #nccrc_sic (page_no, line_no,sic) values (2, 22,'N0072530806');
insert into #nccrc_sic (page_no, line_no,sic) values (2, 22,'N0072530250');
insert into #nccrc_sic (page_no, line_no,sic) values (2, 22,'N0072530925');
insert into #nccrc_sic (page_no, line_no,sic) values (2, 22,'N0072530230');
insert into #nccrc_sic (page_no, line_no,sic) values (2, 22,'N0072530760');
insert into #nccrc_sic (page_no, line_no,sic) values (2, 22,'N0072530761');
insert into #nccrc_sic (page_no, line_no,sic) values (2, 22,'N0072530921');
insert into #nccrc_sic (page_no, line_no,sic) values (2, 22,'N0072530763');
insert into #nccrc_sic (page_no, line_no,sic) values (2, 22,'N0072530764');
insert into #nccrc_sic (page_no, line_no,sic) values (2, 22,'N0072530765');
insert into #nccrc_sic (page_no, line_no,sic) values (2, 22,'N0072530766');
insert into #nccrc_sic (page_no, line_no,sic) values (2, 22,'N0072530058');
insert into #nccrc_sic (page_no, line_no,sic) values (2, 22,'N0072530922');
insert into #nccrc_sic (page_no, line_no,sic) values (2, 22,'N0072530920');
insert into #nccrc_sic (page_no, line_no,sic) values (2, 22,'N0072530886');
insert into #nccrc_sic (page_no, line_no,sic) values (2, 22,'N0072530767');
insert into #nccrc_sic (page_no, line_no,sic) values (2, 22,'N0072530897');
insert into #nccrc_sic (page_no, line_no,sic) values (2, 22,'N0072530924');
insert into #nccrc_sic (page_no, line_no,sic) values (2, 22,'N0072530931');
insert into #nccrc_sic (page_no, line_no,sic) values (2, 22,'N0072590020');
insert into #nccrc_sic (page_no, line_no,sic) values (2, 22,'N0072530938');
insert into #nccrc_sic (page_no, line_no,sic) values (2, 22,'N0072530803');
insert into #nccrc_sic (page_no, line_no,sic) values (2, 22,'N0072530770');
insert into #nccrc_sic (page_no, line_no,sic) values (2, 22,'N0072530810');
insert into #nccrc_sic (page_no, line_no,sic) values (2, 22,'N0072530771');
insert into #nccrc_sic (page_no, line_no,sic) values (2, 22,'N0072530932');
insert into #nccrc_sic (page_no, line_no,sic) values (2, 22,'N0072530807');
insert into #nccrc_sic (page_no, line_no,sic) values (2, 22,'N0072530913');
insert into #nccrc_sic (page_no, line_no,sic) values (2, 22,'N0072530890');
insert into #nccrc_sic (page_no, line_no,sic) values (2, 22,'N0072530936');
insert into #nccrc_sic (page_no, line_no,sic) values (2, 22,'N0072530804');
insert into #nccrc_sic (page_no, line_no,sic) values (2, 22,'N0072530939');
insert into #nccrc_sic (page_no, line_no,sic) values (2, 22,'N0072530772');
insert into #nccrc_sic (page_no, line_no,sic) values (2, 22,'N0072530757');
insert into #nccrc_sic (page_no, line_no,sic) values (2, 22,'N0072530435');
insert into #nccrc_sic (page_no, line_no,sic) values (2, 22,'N0072530289');
insert into #nccrc_sic (page_no, line_no,sic) values (2, 22,'N0072530892');
insert into #nccrc_sic (page_no, line_no,sic) values (2, 22,'N0072530893');
insert into #nccrc_sic (page_no, line_no,sic) values (2, 22,'N0072530894');
insert into #nccrc_sic (page_no, line_no,sic) values (2, 22,'N0072530773');
insert into #nccrc_sic (page_no, line_no,sic) values (2, 22,'N0072530774');
insert into #nccrc_sic (page_no, line_no,sic) values (2, 22,'N0072530895');
insert into #nccrc_sic (page_no, line_no,sic) values (2, 22,'N0072630008');
insert into #nccrc_sic (page_no, line_no,sic) values (2, 22,'N0072530775');
insert into #nccrc_sic (page_no, line_no,sic) values (2, 25,'N0060530513');
insert into #nccrc_sic (page_no, line_no,sic) values (2, 25,'N0060530485');
insert into #nccrc_sic (page_no, line_no,sic) values (2, 25,'N0060530201');
insert into #nccrc_sic (page_no, line_no,sic) values (2, 26,'N0135530721');
insert into #nccrc_sic (page_no, line_no,sic) values (2, 26,'N0135530074');
insert into #nccrc_sic (page_no, line_no,sic) values (2, 26,'N0135530723');
insert into #nccrc_sic (page_no, line_no,sic) values (2, 26,'N0135530801');
insert into #nccrc_sic (page_no, line_no,sic) values (2, 26,'N0135530022');
insert into #nccrc_sic (page_no, line_no,sic) values (2, 26,'N0135530344');
insert into #nccrc_sic (page_no, line_no,sic) values (2, 26,'N0135530793');
insert into #nccrc_sic (page_no, line_no,sic) values (2, 27,'N0019530011');
insert into #nccrc_sic (page_no, line_no,sic) values (2, 28,'N0068530016');
insert into #nccrc_sic (page_no, line_no,sic) values (2, 28,'N0068530533');
insert into #nccrc_sic (page_no, line_no,sic) values (2, 28,'N0068530025');
insert into #nccrc_sic (page_no, line_no,sic) values (2, 28,'N0068530079');
insert into #nccrc_sic (page_no, line_no,sic) values (2, 28,'N0068530019');
insert into #nccrc_sic (page_no, line_no,sic) values (2, 28,'N0068530055');
insert into #nccrc_sic (page_no, line_no,sic) values (2, 28,'N0068530012');
insert into #nccrc_sic (page_no, line_no,sic) values (2, 28,'N0068530438');
insert into #nccrc_sic (page_no, line_no,sic) values (2, 28,'N0068530032');
insert into #nccrc_sic (page_no, line_no,sic) values (2, 28,'N0068530033');
insert into #nccrc_sic (page_no, line_no,sic) values (2, 29,'N0066530453');
insert into #nccrc_sic (page_no, line_no,sic) values (2, 29,'N0066530727');
insert into #nccrc_sic (page_no, line_no,sic) values (2, 29,'N0066530783');
insert into #nccrc_sic (page_no, line_no,sic) values (2, 29,'N0066530728');
insert into #nccrc_sic (page_no, line_no,sic) values (2, 29,'N0066530193');
insert into #nccrc_sic (page_no, line_no,sic) values (2, 29,'N0066530940');
insert into #nccrc_sic (page_no, line_no,sic) values (2, 30,'N0097530527');
insert into #nccrc_sic (page_no, line_no,sic) values (2, 31,'N0189530117');
insert into #nccrc_sic (page_no, line_no,sic) values (2, 32,'N0134530597');
insert into #nccrc_sic (page_no, line_no,sic) values (2, 32,'N0134530617');
insert into #nccrc_sic (page_no, line_no,sic) values (2, 32,'N0134530010');
insert into #nccrc_sic (page_no, line_no,sic) values (2, 32,'N0134530685');
insert into #nccrc_sic (page_no, line_no,sic) values (2, 32,'N0134530626');
insert into #nccrc_sic (page_no, line_no,sic) values (2, 32,'N0134530574');
insert into #nccrc_sic (page_no, line_no,sic) values (2, 32,'N0134530661');
insert into #nccrc_sic (page_no, line_no,sic) values (2, 32,'N0134530736');
insert into #nccrc_sic (page_no, line_no,sic) values (2, 32,'N0134530035');
insert into #nccrc_sic (page_no, line_no,sic) values (2, 32,'N0134530599');
insert into #nccrc_sic (page_no, line_no,sic) values (2, 32,'N0134530657');
insert into #nccrc_sic (page_no, line_no,sic) values (2, 32,'N0134530581');
insert into #nccrc_sic (page_no, line_no,sic) values (2, 32,'N0134530696');
insert into #nccrc_sic (page_no, line_no,sic) values (2, 32,'N0134530598');
insert into #nccrc_sic (page_no, line_no,sic) values (2, 32,'N0134530024');
insert into #nccrc_sic (page_no, line_no,sic) values (2, 32,'N0134530687');
insert into #nccrc_sic (page_no, line_no,sic) values (2, 32,'N0134530637');
insert into #nccrc_sic (page_no, line_no,sic) values (2, 32,'N0134530600');
insert into #nccrc_sic (page_no, line_no,sic) values (2, 32,'N0134530577');
insert into #nccrc_sic (page_no, line_no,sic) values (2, 32,'N0134530662');
insert into #nccrc_sic (page_no, line_no,sic) values (2, 32,'N0134530737');
insert into #nccrc_sic (page_no, line_no,sic) values (2, 32,'N0134530621');
insert into #nccrc_sic (page_no, line_no,sic) values (2, 32,'N0134530645');
insert into #nccrc_sic (page_no, line_no,sic) values (2, 32,'N0134530636');
insert into #nccrc_sic (page_no, line_no,sic) values (2, 32,'N0134530639');
insert into #nccrc_sic (page_no, line_no,sic) values (2, 32,'N0134530527');
insert into #nccrc_sic (page_no, line_no,sic) values (2, 32,'N0134530643');
insert into #nccrc_sic (page_no, line_no,sic) values (2, 32,'N0134530610');
insert into #nccrc_sic (page_no, line_no,sic) values (2, 32,'N0134530576');
insert into #nccrc_sic (page_no, line_no,sic) values (2, 32,'N0134530572');
insert into #nccrc_sic (page_no, line_no,sic) values (2, 32,'N0134530503');
insert into #nccrc_sic (page_no, line_no,sic) values (2, 32,'N0134530738');
insert into #nccrc_sic (page_no, line_no,sic) values (2, 32,'N0134530601');
insert into #nccrc_sic (page_no, line_no,sic) values (2, 32,'N0134530602');
insert into #nccrc_sic (page_no, line_no,sic) values (2, 32,'N0134530664');
insert into #nccrc_sic (page_no, line_no,sic) values (2, 32,'N0134530653');
insert into #nccrc_sic (page_no, line_no,sic) values (2, 32,'N0134530603');
insert into #nccrc_sic (page_no, line_no,sic) values (2, 32,'N0134530650');
insert into #nccrc_sic (page_no, line_no,sic) values (2, 32,'N0134530630');
insert into #nccrc_sic (page_no, line_no,sic) values (2, 32,'N0134530584');
insert into #nccrc_sic (page_no, line_no,sic) values (2, 32,'N0134530624');
insert into #nccrc_sic (page_no, line_no,sic) values (2, 32,'N0134530686');
insert into #nccrc_sic (page_no, line_no,sic) values (2, 32,'N0134530739');
insert into #nccrc_sic (page_no, line_no,sic) values (2, 32,'N0134530012');
insert into #nccrc_sic (page_no, line_no,sic) values (2, 32,'N0134530605');
insert into #nccrc_sic (page_no, line_no,sic) values (2, 32,'N0134530295');
insert into #nccrc_sic (page_no, line_no,sic) values (2, 32,'N0134530554');
insert into #nccrc_sic (page_no, line_no,sic) values (2, 32,'N0134530676');
insert into #nccrc_sic (page_no, line_no,sic) values (2, 32,'N0134530647');
insert into #nccrc_sic (page_no, line_no,sic) values (2, 32,'N0134530606');
insert into #nccrc_sic (page_no, line_no,sic) values (2, 32,'N0134530405');
insert into #nccrc_sic (page_no, line_no,sic) values (2, 32,'N0134530628');
insert into #nccrc_sic (page_no, line_no,sic) values (2, 32,'N0134530620');
insert into #nccrc_sic (page_no, line_no,sic) values (2, 32,'N0134530652');
insert into #nccrc_sic (page_no, line_no,sic) values (2, 32,'N0134530580');
insert into #nccrc_sic (page_no, line_no,sic) values (2, 32,'N0134530726');
insert into #nccrc_sic (page_no, line_no,sic) values (2, 32,'N0134530659');
insert into #nccrc_sic (page_no, line_no,sic) values (2, 32,'N0134530569');
insert into #nccrc_sic (page_no, line_no,sic) values (2, 32,'N0134530673');
insert into #nccrc_sic (page_no, line_no,sic) values (2, 32,'N0134530632');
insert into #nccrc_sic (page_no, line_no,sic) values (2, 32,'N0134530678');
insert into #nccrc_sic (page_no, line_no,sic) values (2, 32,'N0134530570');
insert into #nccrc_sic (page_no, line_no,sic) values (2, 32,'N0134530208');
insert into #nccrc_sic (page_no, line_no,sic) values (2, 32,'N0134530607');
insert into #nccrc_sic (page_no, line_no,sic) values (2, 32,'N0134530715');
insert into #nccrc_sic (page_no, line_no,sic) values (2, 32,'N0134530052');
insert into #nccrc_sic (page_no, line_no,sic) values (2, 32,'N0134530688');
insert into #nccrc_sic (page_no, line_no,sic) values (2, 32,'N0134530680');
insert into #nccrc_sic (page_no, line_no,sic) values (2, 32,'N0134530710');
insert into #nccrc_sic (page_no, line_no,sic) values (2, 32,'N0134530583');
insert into #nccrc_sic (page_no, line_no,sic) values (2, 32,'N0134530741');
insert into #nccrc_sic (page_no, line_no,sic) values (2, 32,'N0134530345');
insert into #nccrc_sic (page_no, line_no,sic) values (2, 32,'N0134530307');
insert into #nccrc_sic (page_no, line_no,sic) values (2, 32,'N0134530618');
insert into #nccrc_sic (page_no, line_no,sic) values (2, 32,'N0134530588');
insert into #nccrc_sic (page_no, line_no,sic) values (2, 32,'N0134530794');
insert into #nccrc_sic (page_no, line_no,sic) values (2, 32,'N0134530615');
insert into #nccrc_sic (page_no, line_no,sic) values (2, 32,'N0134530635');
insert into #nccrc_sic (page_no, line_no,sic) values (2, 32,'N0134530953');
insert into #nccrc_sic (page_no, line_no,sic) values (2, 32,'N0134530005');
insert into #nccrc_sic (page_no, line_no,sic) values (2, 32,'N0134530749');
insert into #nccrc_sic (page_no, line_no,sic) values (2, 32,'N0134530579');
insert into #nccrc_sic (page_no, line_no,sic) values (2, 32,'N0134530616');
insert into #nccrc_sic (page_no, line_no,sic) values (2, 32,'N0134530614');
insert into #nccrc_sic (page_no, line_no,sic) values (2, 32,'N0134530589');
insert into #nccrc_sic (page_no, line_no,sic) values (2, 32,'N0134530124');
insert into #nccrc_sic (page_no, line_no,sic) values (2, 32,'N0134530619');
insert into #nccrc_sic (page_no, line_no,sic) values (2, 32,'N0134530003');
insert into #nccrc_sic (page_no, line_no,sic) values (2, 32,'N0134530660');
insert into #nccrc_sic (page_no, line_no,sic) values (2, 32,'N0134530681');
insert into #nccrc_sic (page_no, line_no,sic) values (2, 32,'N0134530629');
insert into #nccrc_sic (page_no, line_no,sic) values (2, 32,'N0134530640');
insert into #nccrc_sic (page_no, line_no,sic) values (2, 32,'N0134530712');
insert into #nccrc_sic (page_no, line_no,sic) values (2, 32,'N0134530679');
insert into #nccrc_sic (page_no, line_no,sic) values (2, 32,'N0134530566');
insert into #nccrc_sic (page_no, line_no,sic) values (2, 32,'N0134530437');
insert into #nccrc_sic (page_no, line_no,sic) values (2, 32,'N0134530663');
insert into #nccrc_sic (page_no, line_no,sic) values (2, 32,'N0134530651');
insert into #nccrc_sic (page_no, line_no,sic) values (2, 32,'N0134530743');
insert into #nccrc_sic (page_no, line_no,sic) values (2, 32,'N0134530146');
insert into #nccrc_sic (page_no, line_no,sic) values (2, 32,'N0134530587');
insert into #nccrc_sic (page_no, line_no,sic) values (2, 32,'N0134530563');
insert into #nccrc_sic (page_no, line_no,sic) values (2, 32,'N0134530582');
insert into #nccrc_sic (page_no, line_no,sic) values (2, 32,'N0134530591');
insert into #nccrc_sic (page_no, line_no,sic) values (2, 32,'N0134530646');
insert into #nccrc_sic (page_no, line_no,sic) values (2, 32,'N0134530674');
insert into #nccrc_sic (page_no, line_no,sic) values (2, 32,'N0134530683');
insert into #nccrc_sic (page_no, line_no,sic) values (2, 32,'N0134530714');
insert into #nccrc_sic (page_no, line_no,sic) values (2, 32,'N0134530684');
insert into #nccrc_sic (page_no, line_no,sic) values (2, 32,'N0134530104');
insert into #nccrc_sic (page_no, line_no,sic) values (2, 32,'N0134530744');
insert into #nccrc_sic (page_no, line_no,sic) values (2, 32,'N0134530631');
insert into #nccrc_sic (page_no, line_no,sic) values (2, 32,'N0134530593');
insert into #nccrc_sic (page_no, line_no,sic) values (2, 32,'N0134530565');
insert into #nccrc_sic (page_no, line_no,sic) values (2, 32,'N0134530746');
insert into #nccrc_sic (page_no, line_no,sic) values (2, 32,'N0134530114');
insert into #nccrc_sic (page_no, line_no,sic) values (2, 32,'N0134530713');
insert into #nccrc_sic (page_no, line_no,sic) values (2, 32,'N0134530622');
insert into #nccrc_sic (page_no, line_no,sic) values (2, 32,'N0134530656');
insert into #nccrc_sic (page_no, line_no,sic) values (2, 32,'N0134530609');
insert into #nccrc_sic (page_no, line_no,sic) values (2, 32,'N0134530592');
insert into #nccrc_sic (page_no, line_no,sic) values (2, 32,'N0134530571');
insert into #nccrc_sic (page_no, line_no,sic) values (2, 32,'N0134530350');
insert into #nccrc_sic (page_no, line_no,sic) values (2, 32,'N0134530594');
insert into #nccrc_sic (page_no, line_no,sic) values (2, 32,'N0134530568');
insert into #nccrc_sic (page_no, line_no,sic) values (2, 32,'N0134530550');
insert into #nccrc_sic (page_no, line_no,sic) values (2, 32,'N0134530625');
insert into #nccrc_sic (page_no, line_no,sic) values (2, 32,'N0134530718');
insert into #nccrc_sic (page_no, line_no,sic) values (2, 32,'N0134530642');
insert into #nccrc_sic (page_no, line_no,sic) values (2, 32,'N0134530612');
insert into #nccrc_sic (page_no, line_no,sic) values (2, 32,'N0134530246');
insert into #nccrc_sic (page_no, line_no,sic) values (2, 32,'N0134530747');
insert into #nccrc_sic (page_no, line_no,sic) values (2, 32,'N0134530567');
insert into #nccrc_sic (page_no, line_no,sic) values (2, 32,'N0134530748');
insert into #nccrc_sic (page_no, line_no,sic) values (2, 32,'N0134530595');
insert into #nccrc_sic (page_no, line_no,sic) values (2, 32,'N0134530750');
insert into #nccrc_sic (page_no, line_no,sic) values (2, 32,'N0134530273');
insert into #nccrc_sic (page_no, line_no,sic) values (2, 32,'N0134530751');
insert into #nccrc_sic (page_no, line_no,sic) values (2, 32,'N0134530902');
insert into #nccrc_sic (page_no, line_no,sic) values (2, 32,'N0134530699');
insert into #nccrc_sic (page_no, line_no,sic) values (2, 32,'N0134530903');
insert into #nccrc_sic (page_no, line_no,sic) values (2, 32,'N0134530259');
insert into #nccrc_sic (page_no, line_no,sic) values (2, 32,'N0134530717');
insert into #nccrc_sic (page_no, line_no,sic) values (2, 32,'N0134530694');
insert into #nccrc_sic (page_no, line_no,sic) values (2, 32,'N0134530190');
insert into #nccrc_sic (page_no, line_no,sic) values (2, 32,'N0134530701');
insert into #nccrc_sic (page_no, line_no,sic) values (2, 32,'N0134530904');
insert into #nccrc_sic (page_no, line_no,sic) values (2, 32,'N0134530638');
insert into #nccrc_sic (page_no, line_no,sic) values (2, 32,'N0134530905');
insert into #nccrc_sic (page_no, line_no,sic) values (2, 32,'N0134530693');
insert into #nccrc_sic (page_no, line_no,sic) values (2, 32,'N0134530740');
insert into #nccrc_sic (page_no, line_no,sic) values (2, 32,'N0134530658');
insert into #nccrc_sic (page_no, line_no,sic) values (2, 32,'N0134530980');
insert into #nccrc_sic (page_no, line_no,sic) values (2, 32,'N0134530906');
insert into #nccrc_sic (page_no, line_no,sic) values (2, 32,'N0134530088');
insert into #nccrc_sic (page_no, line_no,sic) values (2, 32,'N0134530690');
insert into #nccrc_sic (page_no, line_no,sic) values (2, 32,'N0134530692');
insert into #nccrc_sic (page_no, line_no,sic) values (2, 32,'N0134530907');
insert into #nccrc_sic (page_no, line_no,sic) values (2, 32,'N0134530691');
insert into #nccrc_sic (page_no, line_no,sic) values (2, 32,'N0134530031');
insert into #nccrc_sic (page_no, line_no,sic) values (2, 32,'N0134530711');
insert into #nccrc_sic (page_no, line_no,sic) values (2, 32,'N0134530321');
insert into #nccrc_sic (page_no, line_no,sic) values (2, 32,'N0134530668');
insert into #nccrc_sic (page_no, line_no,sic) values (2, 32,'N0134530742');
insert into #nccrc_sic (page_no, line_no,sic) values (2, 32,'N0134530908');
insert into #nccrc_sic (page_no, line_no,sic) values (2, 32,'N0134530556');
insert into #nccrc_sic (page_no, line_no,sic) values (2, 32,'N0134530517');
insert into #nccrc_sic (page_no, line_no,sic) values (2, 32,'N0134530009');
insert into #nccrc_sic (page_no, line_no,sic) values (2, 32,'N0134530745');
insert into #nccrc_sic (page_no, line_no,sic) values (2, 32,'N0134530697');
insert into #nccrc_sic (page_no, line_no,sic) values (2, 32,'N0134530148');
insert into #nccrc_sic (page_no, line_no,sic) values (2, 32,'N0134530627');
insert into #nccrc_sic (page_no, line_no,sic) values (2, 32,'N0134530682');
insert into #nccrc_sic (page_no, line_no,sic) values (2, 32,'N0134530695');
insert into #nccrc_sic (page_no, line_no,sic) values (2, 32,'N0134530716');
insert into #nccrc_sic (page_no, line_no,sic) values (2, 32,'N0134530752');
insert into #nccrc_sic (page_no, line_no,sic) values (2, 32,'N0134530649');
insert into #nccrc_sic (page_no, line_no,sic) values (2, 34,'N0078530735');
insert into #nccrc_sic (page_no, line_no,sic) values (2, 34,'N0078530686');
insert into #nccrc_sic (page_no, line_no,sic) values (2, 34,'N0078530658');
insert into #nccrc_sic (page_no, line_no,sic) values (2, 34,'N0078600475');
insert into #nccrc_sic (page_no, line_no,sic) values (2, 35,'N0134530002');
insert into #nccrc_sic (page_no, line_no,sic) values (2, 36,'N0089530253');
insert into #nccrc_sic (page_no, line_no,sic) values (2, 36,'N0089530911');
insert into #nccrc_sic (page_no, line_no,sic) values (2, 36,'N0089530535');
insert into #nccrc_sic (page_no, line_no,sic) values (2, 36,'N0089530080');
insert into #nccrc_sic (page_no, line_no,sic) values (2, 36,'N0089530536');
insert into #nccrc_sic (page_no, line_no,sic) values (2, 36,'N0089530385');
insert into #nccrc_sic (page_no, line_no,sic) values (2, 36,'N0089530910');
insert into #nccrc_sic (page_no, line_no,sic) values (2, 36,'N0089530516');
insert into #nccrc_sic (page_no, line_no,sic) values (2, 36,'N0089530236');
insert into #nccrc_sic (page_no, line_no,sic) values (2, 36,'N0089530376');
insert into #nccrc_sic (page_no, line_no,sic) values (2, 36,'N0089530548');
insert into #nccrc_sic (page_no, line_no,sic) values (2, 36,'N0089530387');
insert into #nccrc_sic (page_no, line_no,sic) values (2, 36,'N0089530245');
insert into #nccrc_sic (page_no, line_no,sic) values (2, 36,'N0089530041');
insert into #nccrc_sic (page_no, line_no,sic) values (2, 36,'N0089530375');
insert into #nccrc_sic (page_no, line_no,sic) values (2, 37,'R0303530119');
insert into #nccrc_sic (page_no, line_no,sic) values (2, 38,'N0088530049');
insert into #nccrc_sic (page_no, line_no,sic) values (2, 38,'N0088530039');
insert into #nccrc_sic (page_no, line_no,sic) values (2, 38,'N0088530050');
insert into #nccrc_sic (page_no, line_no,sic) values (2, 38,'N0088530028');
insert into #nccrc_sic (page_no, line_no,sic) values (2, 38,'N0088530812');
insert into #nccrc_sic (page_no, line_no,sic) values (2, 38,'N0088530047');
insert into #nccrc_sic (page_no, line_no,sic) values (2, 38,'N0088530043');
insert into #nccrc_sic (page_no, line_no,sic) values (2, 38,'N0088530020');
insert into #nccrc_sic (page_no, line_no,sic) values (2, 38,'N0088530071');
insert into #nccrc_sic (page_no, line_no,sic) values (2, 38,'N0088530082');
insert into #nccrc_sic (page_no, line_no,sic) values (2, 38,'N0088530040');
insert into #nccrc_sic (page_no, line_no,sic) values (2, 38,'N0088530799');
insert into #nccrc_sic (page_no, line_no,sic) values (2, 39,'R0197530157');
insert into #nccrc_sic (page_no, line_no,sic) values (2, 39,'R0197530092');
insert into #nccrc_sic (page_no, line_no,sic) values (2, 39,'R0197530180');
insert into #nccrc_sic (page_no, line_no,sic) values (2, 39,'R0197530500');
insert into #nccrc_sic (page_no, line_no,sic) values (2, 39,'R0197530172');
insert into #nccrc_sic (page_no, line_no,sic) values (2, 39,'R0197530173');
insert into #nccrc_sic (page_no, line_no,sic) values (2, 39,'R0197530143');
insert into #nccrc_sic (page_no, line_no,sic) values (2, 39,'R0197530095');
insert into #nccrc_sic (page_no, line_no,sic) values (2, 39,'R0197530140');
insert into #nccrc_sic (page_no, line_no,sic) values (2, 39,'R0197530147');
insert into #nccrc_sic (page_no, line_no,sic) values (2, 39,'R0197530734');
insert into #nccrc_sic (page_no, line_no,sic) values (2, 39,'R0197530107');
insert into #nccrc_sic (page_no, line_no,sic) values (2, 39,'R0197530179');
insert into #nccrc_sic (page_no, line_no,sic) values (2, 40,'R0199530014');
insert into #nccrc_sic (page_no, line_no,sic) values (2, 41,'N0036530009');
insert into #nccrc_sic (page_no, line_no,sic) values (2, 42,'N0059530044');
insert into #nccrc_sic (page_no, line_no,sic) values (2, 42,'N0059530023');
insert into #nccrc_sic (page_no, line_no,sic) values (2, 42,'N0059530015');
insert into #nccrc_sic (page_no, line_no,sic) values (2, 42,'N0059530004');
insert into #nccrc_sic (page_no, line_no,sic) values (2, 42,'N0059530776');
insert into #nccrc_sic (page_no, line_no,sic) values (2, 43,'N0111530818');
insert into #nccrc_sic (page_no, line_no,sic) values (2, 43,'N0111530815');
insert into #nccrc_sic (page_no, line_no,sic) values (2, 43,'N0111530816');
insert into #nccrc_sic (page_no, line_no,sic) values (2, 43,'N0111530814');
insert into #nccrc_sic (page_no, line_no,sic) values (2, 43,'N0111530813');
insert into #nccrc_sic (page_no, line_no,sic) values (2, 43,'N0111530817');
insert into #nccrc_sic (page_no, line_no,sic) values (2, 43,'N0111530094');
insert into #nccrc_sic (page_no, line_no,sic) values (2, 43,'N0111530021');
insert into #nccrc_sic (page_no, line_no,sic) values (2, 43,'N0111530054');
insert into #nccrc_sic (page_no, line_no,sic) values (2, 44,'N0043530518');

-- Oil Company Creditors
insert into #nccrc_sic (page_no, line_no,sic) values (3,  4,'N0003550002');
insert into #nccrc_sic (page_no, line_no,sic) values (3,  4,'N0135550042');
insert into #nccrc_sic (page_no, line_no,sic) values (3,  5,'N0160550003');
insert into #nccrc_sic (page_no, line_no,sic) values (3,  6,'N0135550011');
insert into #nccrc_sic (page_no, line_no,sic) values (3,  7,'N0013550014');
insert into #nccrc_sic (page_no, line_no,sic) values (3,  8,'R0114550015');
insert into #nccrc_sic (page_no, line_no,sic) values (3,  9,'N0034550004');
insert into #nccrc_sic (page_no, line_no,sic) values (3, 10,'N0039550009');
insert into #nccrc_sic (page_no, line_no,sic) values (3, 11,'N0135550004');
insert into #nccrc_sic (page_no, line_no,sic) values (3, 12,'N0156550010');
insert into #nccrc_sic (page_no, line_no,sic) values (3, 13,'N0135550005');
insert into #nccrc_sic (page_no, line_no,sic) values (3, 14,'N0050550007');
insert into #nccrc_sic (page_no, line_no,sic) values (3, 15,'R0018550013');

-- Build the list of distinct sic codes
select distinct convert(int, 0) as page_no, convert(int, 0) as line_no, sic
into #nccrc_sic_1
from #nccrc_sic;

update #nccrc_sic_1
set	page_no = x.page_no,
	line_no = x.line_no
from	#nccrc_sic_1 a
inner join #nccrc_sic x on a.sic = x.sic;

-- Build a list of the creditors which match the SIC codes
create table #nccrc_creditors (page_no int, line_no int, creditor varchar(10), creditor_name varchar(50), sic varchar(20));

insert into #nccrc_creditors (page_no, line_no, creditor, creditor_name, sic)
select distinct a.page_no, a.line_no, cr.creditor, isnull(cr.creditor_name, ''), isnull(cr.sic,'')
from #nccrc_sic_1 a
left outer join creditors cr on a.sic = cr.sic;

-- Generate the disbursement information for the creditors in the list
select		cr.page_no, cr.line_no, rcr.client, rcr.creditor, rcr.client_creditor, sum(isnull(debit_amt,0)) as 'debit_amt', sum(isnull(credit_amt,0)) as 'credit_amt'
into		#nccrc_disbursements
from		#nccrc_creditors cr
inner join	registers_client_creditor rcr on rcr.creditor = cr.creditor

where		cr.page_no is not null
and		cr.line_no is not null
and		rcr.tran_type in ('AD', 'BW', 'MD', 'CM', 'RF', 'VF')
and		rcr.date_created between @FromDate and @ToDate
and		isnull(rcr.void,0) = 0

group by	cr.page_no, cr.line_no, rcr.client, rcr.creditor, rcr.client_creditor;

-- Summarize the information by the line number
select		x.page_no, x.line_no, cr.creditor, cr.creditor_name, cr.sic, sum(isnull(x.debit_amt,0)) as 'debit_amt', sum(isnull(x.credit_amt,0)) as credit_amt
from		#nccrc_disbursements x
inner join	#nccrc_creditors cr on x.creditor = cr.creditor
group by	x.page_no, x.line_no, cr.creditor, cr.creditor_name, cr.sic
order by	1, 2, 3, 4;

-- Discard the working tables
drop table #nccrc_disbursements;
drop table #nccrc_creditors;
drop table #nccrc_sic;
drop table #nccrc_sic_1;

return ( @@rowcount )
GO
