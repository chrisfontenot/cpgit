USE [DebtPlus]
GO
/****** Object:  StoredProcedure [dbo].[UPDATE_client_appointments]    Script Date: 09/15/2014 13:13:34 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[UPDATE_client_appointments] AS

-- ==========================================================================================
-- ==            Set the appointment information in the clients table                      ==
-- ==========================================================================================

-- Create the client appointment information in the client table for the ScoreCard
select client,min(start_time) as start_time,0 as 'client_appointment',0 as 'resched_appt'
into #t_first_appt
from client_appointments
group by client

update #t_first_appt
set client_appointment = a.client_appointment
from #t_first_appt x
inner join client_appointments a on a.client = x.client and a.start_time = x.start_time

select client_appointment, previous_appointment
into #t_resched
from client_appointments
where isnull(previous_appointment,0) > 0

update #t_first_appt
set resched_appt = y.client_appointment
from #t_first_appt x
inner join #t_resched y on y.previous_appointment = x.client_appointment

update clients
set first_appt = case when x.client_appointment = 0 then null else  x.client_appointment end,
    first_resched_appt = case when x.resched_appt = 0 then null else x.resched_appt end
from clients c
inner join #t_first_appt x on c.client = x.client

update clients
set method_first_contact = 1

update clients
set method_first_contact = isnull(ap.contact_type,1)
from clients c
inner join client_appointments a ON a.client_appointment = c.first_appt
inner join appt_types ap on ap.appt_type = a.appt_type

select client,min(start_time) as start_time,0 as 'client_appointment'
into #t_first_kept_appt
from client_appointments
where status = 'K'
group by client

update #t_first_kept_appt
set client_appointment = a.client_appointment
from #t_first_kept_appt x
inner join client_appointments a on a.client = x.client and a.start_time = x.start_time

update clients
set first_kept_appt = case when x.client_appointment = 0 then null else  x.client_appointment end
from clients c
inner join #t_first_kept_appt x on c.client = x.client

-- Discard the temporary tables
drop table #t_first_kept_appt;
drop table #t_first_appt;
drop table #t_resched;

return ( 1 )
GO
