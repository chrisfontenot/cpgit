USE [DebtPlus]
GO
/****** Object:  StoredProcedure [dbo].[UPDATE_creditor_statistics_payments]    Script Date: 09/15/2014 13:13:34 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE PROCEDURE [dbo].[UPDATE_creditor_statistics_payments] AS

-- ===============================================================================================
-- ==            Correct the statistics for the last creditor payment                           ==
-- ===============================================================================================

declare	@year_start		datetime
declare	@month_start		datetime
declare	@month_end		datetime

select	@year_start = convert(datetime, '01/01/' + right('0000' + convert(varchar,year(getdate())), 4) + ' 00:00:00'),
	@month_start = convert(datetime, right('00' + convert(varchar,month(getdate())), 2) + '/01/' + right('0000' + convert(varchar,year(getdate())), 4) + ' 00:00:00')
select	@month_end = dateadd(m, 1, @month_start)

update	creditors
set	last_payment	= null,
	first_payment	= null;

-- Last payment
select	creditor, max(date_created) as 'date_created', convert(int, 0) as 'creditor_register'
into	#last_payments
from	registers_creditor
where	tran_type in ('AD', 'MD', 'BW', 'CM')
group by creditor;

update	#last_payments
set	creditor_register = rc.creditor_register
from	#last_payments x
inner join registers_creditor rc on x.creditor = rc.creditor and x.date_created = rc.date_created
where	rc.tran_type in ('AD', 'MD', 'BW', 'CM');

update	creditors
set	last_payment = x.creditor_register
from	creditors cr
inner join #last_payments x on cr.creditor = x.creditor;

drop table #last_payments

-- First payment
select	creditor, min(date_created) as 'date_created', convert(int, 0) as 'creditor_register'
into	#first_payments
from	registers_creditor
where	tran_type in ('AD', 'MD', 'BW', 'CM')
group by creditor;

update	#first_payments
set	creditor_register = rc.creditor_register
from	#first_payments x
inner join registers_creditor rc on x.creditor = rc.creditor and x.date_created = rc.date_created
where	rc.tran_type in ('AD', 'MD', 'BW', 'CM');

update	creditors
set	first_payment = x.creditor_register
from	creditors cr
inner join #first_payments x on cr.creditor = x.creditor;

drop table #first_payments
GO
