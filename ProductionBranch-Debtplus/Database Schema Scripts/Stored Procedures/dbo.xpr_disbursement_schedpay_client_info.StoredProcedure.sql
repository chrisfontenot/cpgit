USE [DebtPlus]
GO
/****** Object:  StoredProcedure [dbo].[xpr_disbursement_schedpay_client_info]    Script Date: 09/15/2014 13:13:35 ******/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE PROCEDURE [dbo].[xpr_disbursement_schedpay_client_info] ( @client as int ) AS

-- ============================================================================================================
-- ==            Return the information for the info form in the schedpay utility                            ==
-- ============================================================================================================

set nocount on

SELECT	dbo.format_TelephoneNUmber ( c.HomeTelephoneID )	as 'home_ph',
	dbo.format_TelephoneNumber ( c.MsgTelephoneID )	as 'message_ph',
	isnull(dbo.format_address_line_1(ca.house,ca.direction,ca.street,ca.suffix,ca.modifier,ca.modifier_value),'')			as 'addr1',
	isnull(ca.address_line_2,'')			as 'addr2',
	dbo.format_city_state_zip ( ca.city, ca.state, ca.PostalCode ) as 'addr3',
	isnull(l.Attribute,'US-English')	as 'language',
	dbo.format_TelephoneNumber ( p.WorkTelephoneID )	as 'work_ph',
	''			as 'work_ext',
	dbo.format_normal_name(pn.prefix,pn.first,pn.middle,pn.last,pn.suffix) as 'name'

from	clients c with (nolock)
left outer join AttributeTypes l with (nolock) on c.language=l.oID
left outer join people p with (nolock) on c.client = p.client and 1 = p.relation
left outer join names pn with (nolock) on p.nameid = pn.name
left outer join addresses ca with (nolock) on c.addressid = ca.address

where c.client = @client

return ( @@rowcount )
GO
