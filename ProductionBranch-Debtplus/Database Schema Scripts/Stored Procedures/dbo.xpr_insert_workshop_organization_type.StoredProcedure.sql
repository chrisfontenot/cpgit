USE [DebtPlus]
GO
/****** Object:  StoredProcedure [dbo].[xpr_insert_workshop_organization_type]    Script Date: 09/15/2014 13:13:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[xpr_insert_workshop_organization_type] ( @name as varchar(50), @ActiveFlag as bit = 1 ) as
	insert into workshop_organization_types ([name],[ActiveFlag]) values (@name, isnull(@ActiveFlag,1))
	return (scope_identity())
GO
