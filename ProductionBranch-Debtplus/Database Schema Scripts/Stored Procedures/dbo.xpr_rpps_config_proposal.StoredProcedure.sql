USE [DebtPlus]
GO
/****** Object:  StoredProcedure [dbo].[xpr_rpps_config_proposal]    Script Date: 09/15/2014 13:13:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[xpr_rpps_config_proposal] ( @rpps_file as int ) as
-- ====================================================================================================
-- ==            Retrieve the configuration information for RPS proposals                            ==
-- ====================================================================================================

-- ChangeLog
--   1/31/2003
--     Changed to use the banks table rather than 'config'.

-- Suppress intermediate result sets
SET NOCOUNT ON

declare	@bank		int
select	@bank		= bank
from	rpps_files with (nolock)
where	rpps_file	= @rpps_file

-- There must be a bank account number at this point
if @bank is null
begin
	RaisError ('Bank account # is not configured for RPPS', 16, 1)
	return ( 0 )
end

-- The bank must be a valid item
if not exists (select * from banks where type = 'R' and bank = @bank)
begin
	RaisError ('Bank account # %d is not configured for RPPS', 16, 1, @bank)
	return ( 0 )
end

-- Fetch the information for RPS from the configuration table
SELECT	b.immediate_origin			as 'rps_id',
		b.immediate_origin_name		as 'rps_origin_name',
		b.prefix_line				as 'rps_prefix',
		b.suffix_line				as 'rps_suffix',
		b.batch_number				as 'batch_id',
		b.transaction_number		as 'transaction_id',

		upper(left(ltrim(isnull(cnf.name,'')) + '                  ',18))	as 'agency_name',
		upper(left(ltrim(coalesce(dbo.format_address_line_1(a.house,a.direction,a.street,a.suffix,a.modifier,a.modifier_value),a.address_line_2,'')) + '                            ',28)) as 'agency_address',
		upper(left(ltrim(isnull(a.city,'')) + '                    ',20))	as 'agency_city',
		upper(left(ltrim(isnull(st.MailingCode,'')) + '  ',2))			as 'agency_state',
		upper(left(isnull(a.PostalCode,'') + '     ',5))			as 'agency_zipcode',
		upper(left(isnull(isnull(t.acode,'000'),3) + left(replace(isnull(t.number,'0000000'),'-',''),7) + '          ',10))			as 'agency_phone',
		b.bank									as 'bank'
from	banks b with (nolock)
left outer join config cnf on 1 = cnf.company_id
left outer join addresses a on cnf.addressid = a.address
left outer join states st with (nolock) on a.state = st.state
left outer join TelephoneNumbers t with (nolock) on cnf.TelephoneID = t.TelephoneNumber
where	b.bank			= @bank

RETURN ( @@rowcount )
GO
