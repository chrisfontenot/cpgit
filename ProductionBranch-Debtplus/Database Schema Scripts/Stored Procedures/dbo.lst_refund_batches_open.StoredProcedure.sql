USE [DebtPlus]
GO
/****** Object:  StoredProcedure [dbo].[lst_refund_batches_open]    Script Date: 09/15/2014 13:13:32 ******/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[lst_refund_batches_open] AS

-- ===================================================================================================
-- ==                Return the list of open deposit batches                                        ==
-- ===================================================================================================

SELECT	deposit_batch_id	as 'item_key',
	date_created		as 'date_created',
	created_by		as 'created_by',
	note			as 'note'
FROM	deposit_batch_ids WITH ( NOLOCK )
where	date_closed IS NULL
AND	date_posted IS NULL
AND	batch_type = 'CR'

return ( @@rowcount )
GO
