USE [DebtPlus]
GO
/****** Object:  StoredProcedure [dbo].[rpt_last_disbursed_date]    Script Date: 09/15/2014 13:13:33 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE PROCEDURE [dbo].[rpt_last_disbursed_date] ( @from_date as datetime ) as

-- Remove the time from the information
select	@from_date = convert(datetime, convert(varchar(10), @from_date, 101) + ' 00:00:00')

-- From the registers_client table, find the last auto-disbursed date
select	client, max(date_created) as disbursed_date
into	#disb
from	registers_client
where	tran_type = 'AD'
and	date_created > @from_date
group by client;

-- Merge with the client information
select	v.client, v.name, c.held_in_trust, isnull(co.name,'') as counselor, x.disbursed_date
from	view_client_address v
inner join clients c on c.client = v.client
left outer join counselors co on c.counselor = co.counselor
inner join #disb x on c.client = x.client
where	c.held_in_trust > 50
order by 4, 1

drop table #disb
return ( @@rowcount )
GO
