USE [DebtPlus]
GO
/****** Object:  StoredProcedure [dbo].[rpt_Creditor_Contributions]    Script Date: 09/15/2014 13:13:33 ******/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE PROCEDURE [dbo].[rpt_Creditor_Contributions] ( @FromDate as datetime = null, @ToDate as datetime = null ) as
-- =====================================================================================
-- ==            List the creditor contributions by creditor                          ==
-- =====================================================================================

-- ChangeLog
--   1/3/2002
--     Changed report to use SIC code rather than creditor ID to group creditors by "name"

-- Initialize
set nocount on

if @ToDate is null
	set @toDate = getdate()

if @FromDate is null
	set @FromDate = @ToDate

select	@FromDate = convert(datetime, convert(varchar(10), @FromDate, 101) + ' 00:00:00'),
	@ToDate   = convert(datetime, convert(varchar(10), @ToDate,   101) + ' 23:59:59')

-- ====================================================================================
-- ==           Build a list of the deductions by creditor                           ==
-- ====================================================================================

select	rc.creditor, sum(rc.fairshare_amt) as 'fairshare'
into	#deducted
from	registers_client_creditor rc with (nolock)
where	rc.date_created between @FromDate and @ToDate
and	rc.tran_type in ('AD', 'BW', 'MD', 'CM')
and	rc.creditor_type = 'D'
and	rc.void = 0
group by rc.creditor;

-- ====================================================================================
-- ==           Build a list of the invoiced amounts by creditor                     ==
-- ====================================================================================

select	rc.creditor, sum(fairshare_amt) as 'fairshare'
into	#billed
from	registers_client_creditor rc with (nolock)
where	rc.date_created between @FromDate and @ToDate
and	rc.tran_type in ('AD', 'BW', 'MD', 'CM')
and	rc.creditor_type not in ('N', 'D')
and	rc.void = 0
group by rc.creditor;

-- ====================================================================================
-- ==           Build a list of the invoice payments by creditor                     ==
-- ====================================================================================

select	rc.creditor, sum(rc.credit_amt) as 'fairshare'
into	#receipts
from	registers_creditor rc with (nolock)
where	rc.date_created between @FromDate and @ToDate
and	rc.tran_type = 'RC'
group by rc.creditor;

-- ====================================================================================
-- ==           Return the results to the caller                                     ==
-- ====================================================================================

select	cr.sic							as 'sic',
	isnull(a.fairshare,0)					as 'deducted',
	isnull(b.fairshare,0)					as 'billed',
	isnull(c.fairshare,0)					as 'receipts',
	cr.creditor_name					as 'creditor_name'
from	creditors cr with (nolock)
left outer join #deducted a on cr.creditor = a.creditor
left outer join #billed b   on cr.creditor = b.creditor
left outer join #receipts c on cr.creditor = c.creditor

where	isnull(a.fairshare,0) > 0
or	isnull(b.fairshare,0) > 0
or	isnull(c.fairshare,0) > 0

-- Clean up
drop table #deducted;
drop table #billed;
drop table #receipts;

return ( @@rowcount )
GO
