IF NOT EXISTS(SELECT * FROM SYSOBJECTS WHERE TYPE='P' AND NAME=N'rpt_PrintClient_CreditorDebt')
	EXEC ('CREATE PROCEDURE [dbo].[rpt_PrintClient_CreditorDebt] () AS')
GO
ALTER PROCEDURE [dbo].[rpt_PrintClient_CreditorDebt] ( @Client AS INT ) AS
-- ======================================================================================================
-- ==            Retrieve the list of debts and the corresponding creditor information                 ==
-- ======================================================================================================

SET NOCOUNT ON

CREATE TABLE #t_PrintClient_CC (
	creditor					varchar(10),
	cr_type						varchar(4) null,
	cr_creditor_id				int null,
	client_creditor				int,
	creditor_name				varchar(256) NULL,
	account_number				varchar(256) NULL,
	non_dmp_interest			float NULL,
	dmp_interest				float NULL,
	balance						money NULL,
	non_dmp_payment				money NULL,
	dmp_payment					money NULL,
	dmp_payout					datetime NULL,
	empty_creditor				bit,
	setup_creditor				bit,
	cushion_creditor			bit,
	fee_creditor				bit,
	adjusted_orig_balance		money
)

INSERT INTO #t_PrintClient_CC (cr_type,cr_creditor_id,creditor,client_creditor,creditor_name,account_number,non_dmp_interest,dmp_interest,balance,non_dmp_payment,dmp_payment,dmp_payout,empty_creditor,setup_creditor,cushion_creditor,fee_creditor,adjusted_orig_balance)
SELECT		cr.type as cr_type,
		cr.creditor_id as cr_creditor_id,
		cc.creditor,
		cc.client_creditor,
		isnull(cr.creditor_name,cc.creditor_name) as 'creditor_name',

		cc.account_number,
		cc.non_dmp_interest,

		case
			when cc.dmp_interest is not null then cc.dmp_interest
			when cr.creditor is null then isnull(cc.dmp_payout_interest,0)
			when isnull(bal.orig_balance,0)+isnull(bal.orig_balance_adjustment,0)+isnull(bal.total_interest,0)-isnull(bal.total_payments,0) < cr.medium_apr_amt then cr.lowest_apr_pct
			when isnull(bal.orig_balance,0)+isnull(bal.orig_balance_adjustment,0)+isnull(bal.total_interest,0)-isnull(bal.total_payments,0) < cr.highest_apr_amt then cr.medium_apr_pct
			else cr.highest_apr_pct
		end as 'dmp_interest',

		case
			when isnull(ccl.zero_balance,0) <> 0 then 0
			else isnull(bal.orig_balance,0)+isnull(bal.orig_balance_adjustment,0)+isnull(bal.total_interest,0)-isnull(bal.total_payments,0)
		end as 'balance',
	
		cc.non_dmp_payment as 'non_dmp_payment',

		case
			when isnull(ccl.zero_balance,0) <> 0 then isnull(cc.disbursement_factor,0)
			when isnull(bal.orig_balance,0)+isnull(bal.orig_balance_adjustment,0)+isnull(bal.total_interest,0)-isnull(bal.total_payments,0) <= 0 then 0
			else isnull(cc.disbursement_factor,0)
		end as 'dmp_payment',
	
		case
			when (isnull(ccl.zero_balance,0) > 0) then null
			else cc.expected_payout_date
		end as 'dmp_payout',

		0,0,0,0,
		bal.orig_balance + bal.orig_balance_adjustment

FROM		client_creditor cc with (nolock)
inner join	client_creditor_balances bal with (nolock) on cc.client_creditor_balance = bal.client_creditor_balance -- and bal.client_creditor = cc.client_creditor
LEFT OUTER JOIN	creditors cr with (nolock) ON cc.creditor = cr.creditor
LEFT OUTER JOIN	creditor_classes ccl WITH (NOLOCK) ON cr.creditor_class = ccl.creditor_class
WHERE		cc.client = @Client
AND		cc.reassigned_debt	= 0

-- Find the setup creditor from the config file
declare	@setup_creditor		varchar(10)
select	@setup_creditor = setup_creditor from config

-- Set the appropriate mode flags
update	#t_PrintClient_CC set empty_creditor   = 1 where creditor is null
update  #t_PrintClient_CC set cushion_creditor = 1 where empty_creditor = 1 and (creditor_name = 'cushion' or creditor_name = 'payment cushion')
update  #t_PrintClient_CC set setup_creditor   = 1 where empty_creditor = 0 and creditor = @setup_creditor
update	#t_PrintClient_CC set fee_creditor     = 1 where empty_creditor = 0 and cushion_creditor = 0 and setup_creditor = 0 and (creditor like 'X%' or creditor like 'Z%')

-- Construct the empty table of creditor addresses for each creditor in the client's list
CREATE TABLE #t_PrintClient_CR (
	creditor	varchar(10)  NULL,
	addr1		varchar(256) NULL,
	addr2		varchar(256) NULL,
	addr3		varchar(256) NULL,
	addr4		varchar(256) NULL,
	addr5		varchar(256) NULL
)

INSERT INTO #t_PrintClient_CR (creditor, addr1, addr2, addr3, addr4, addr5)
SELECT DISTINCT
	creditor,
	null as addr1,
	null as addr2,
	null as addr3,
	null as addr4,
	null as addr5
FROM	#t_PrintClient_CC

-- Select the address records and update the various fields in the resulting table of addresses
update	#t_PrintClient_CR
set	addr1		= b.addr1,
	addr2		= b.addr2,
	addr3		= b.addr3,
	addr4		= b.addr4,
	addr5		= b.addr5
from	#t_PrintClient_CR a
inner join view_creditor_addresses b on a.creditor = b.creditor and 'P' = b.type

-- Merge the client/creditor information with the creditor address data
SELECT		a.creditor,
			a.client_creditor,
			a.creditor_name,
			right(dbo.statement_account_number ( a.account_number ), 4) as account_number,
			a.non_dmp_interest,
			a.dmp_interest,
			a.balance,
			a.non_dmp_payment,
			a.dmp_payment,
			a.dmp_payout,

			a.empty_creditor,
			a.cushion_creditor,
			a.setup_creditor,
			a.fee_creditor,
			a.adjusted_orig_balance,

			b.addr1,
			b.addr2,
			b.addr3,
			b.addr4,
			b.addr5

FROM		#t_PrintClient_CC a
LEFT OUTER JOIN	#t_PrintClient_CR b ON a.creditor = b.creditor
ORDER BY	a.cr_type, a.cr_creditor_id, a.creditor, a.client_creditor

-- Drop the working tables
DROP TABLE #t_PrintClient_CC
DROP TABLE #t_PrintClient_CR

RETURN ( @@rowcount )
GO
GRANT EXECUTE ON [rpt_PrintClient_CreditorDebt] to public as dbo;
GO
