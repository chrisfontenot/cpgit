USE [DebtPlus]
GO
/****** Object:  StoredProcedure [dbo].[xpr_recon_batch_insert]    Script Date: 09/15/2014 13:13:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE PROCEDURE [dbo].[xpr_recon_batch_insert] AS

-- =====================================================================================================================
-- ==            Create the reconcilation batch                                                                       ==
-- =====================================================================================================================

-- Disable intermediate results
SET NOCOUNT ON

-- Batch ID
DECLARE	@Batch	INT

INSERT INTO recon_batches	(created_by,	date_created)
VALUES				(suser_sname(),	getdate())

SELECT @Batch = SCOPE_IDENTITY()

RETURN ( @Batch )
GO
