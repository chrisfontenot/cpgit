USE [DEBTPLUS]
GO
/****** Object:  StoredProcedure [dbo].[xpr_get_dsa_case_income_details]    Script Date: 12/5/2014 7:49:59 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE procedure [dbo].[xpr_get_dsa_case_income_details] (
	@clientid	int
)
as
begin

	declare @unemploymentincome			money
	declare @childsupportalimony		money
	declare @socialsecurity				money
	declare @rentsreceived				money
	declare @bonus						money
	declare @otherincome				money
	declare @othermonthlyincome			money
	declare @foodwelfare				money

	--------------------------------
	-- income details
	--------------------------------
	-- get unemployment income
	select @unemploymentincome = asset_amount
	  from assets
	 where client = @clientid
	   and asset_id = 5

	-- get child support, alimony, seperation
	select @childsupportalimony = sum(asset_amount)
	  from assets
	 where client = @clientid
	   and asset_id in (2, 7)

	-- get social security, ssdi
	select @socialsecurity = sum(asset_amount)
	  from assets
	 where client = @clientid
	   and asset_id in (3, 10)

	-- get bonus
	select @bonus = sum(asset_amount)
	  from assets
	 where client = @clientid
	   and asset_id IN (6, 8)
	-- parttime (6) and other income(8)

	-- get other income
	select @otherincome = sum(asset_amount)
	  from assets
	 where client = @clientid
	   and asset_id in (9, 11)
	
	-- get other monthly income
	select @othermonthlyincome = asset_amount
	  from assets
	 where client = @clientid
	   and asset_id = 1

	-- get food welfare
	select @foodwelfare = asset_amount
	  from assets
	 where client = @clientid
	   and asset_id = 4
	
	-- get rent received
	select @rentsreceived = asset_amount
	  from assets
	 where client = @clientid
	   and asset_id = 12

	select @unemploymentincome as UnemploymentIncome
		 , @childsupportalimony as ChildSupportAlimonyIncome
		 , @socialsecurity as SocialSecurityIncome
		 , @bonus as BonusIncome
		 , @otherincome as OtherIncome
		 , @othermonthlyincome as OtherMonthlyIncome
		 , @foodwelfare as FoodWelfareIncome
		 , @rentsreceived as RentReceivedIncome

end