USE [DebtPlus]
GO
/****** Object:  StoredProcedure [dbo].[rpt_PrintChecks_Mark_First]    Script Date: 09/15/2014 13:13:33 ******/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE PROCEDURE [dbo].[rpt_PrintChecks_Mark_First] ( @bank as int = 1 ) AS

-- Find the Trust Register ID for the first check that would be printed
SET NOCOUNT ON
DECLARE @TrustRegister	INT

if isnull(@bank,-1) <= 0
	select	@bank	= bank
	from	banks with (nolock)
	where	type	= 'C'

SET ROWCOUNT 1

SELECT		@TrustRegister = trust_register
FROM		registers_trust
WHERE		isnull(cleared,' ') = 'P'
AND		((creditor is not null) OR (client is not null))
AND		amount > 0.0
--and		bank	= @bank
ORDER BY	check_order, trust_register

SET ROWCOUNT 0

-- If there are some items then there should be a trust register.
-- So, mark that item as being printed by the system
if @TrustRegister IS NULL
	return ( 0 )

-- Generate a distinct ID for the pending checks
DECLARE	@ID_String	Varchar(50)
SELECT	@ID_String	= newid()

-- Mark the pending checks to be printed
UPDATE	registers_trust
SET	sequence_number	= @ID_String
FROM	registers_trust
WHERE	trust_register	= @TrustRegister

-- Return the ID string for the marked checks
SELECT	@ID_String	as id,
	1		as items

-- Return the number of rows in the result
RETURN ( 1 )
GO
