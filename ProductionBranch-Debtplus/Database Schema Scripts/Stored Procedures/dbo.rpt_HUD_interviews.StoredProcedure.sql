USE [DebtPlus]
GO
/****** Object:  StoredProcedure [dbo].[rpt_HUD_interviews]    Script Date: 09/15/2014 13:13:33 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[rpt_HUD_interviews]  (@From_Date as datetime = null, @To_Date as datetime = null) as
-- =============================================================================================================
-- ==           Select the interviews for the client                                                          ==
-- =============================================================================================================

-- ChangeLog
--   6/25/2008
--     Initial Creation

set nocount on

if @to_date is null
	select	@to_date = getdate()

if @from_date is null
	select	@from_date = @to_date

select	@from_date	= convert(varchar(10), @from_date, 101),
		@to_date = convert(varchar(10), @to_date, 101) + ' 23:59:59';

select	i.client											as 'client',

		i.interview_type									as 'interview_type',
		i.interview_date									as 'interview_date',
		dbo.format_counselor_name(i.interview_counselor)	as 'interview_counselor',

		i.hud_result										as 'result_type',
		i.result_date										as 'result_date',
		dbo.format_counselor_name(i.result_counselor)		as 'result_counselor',

		i.termination_reason								as 'termination_reason',
		i.termination_date									as 'termination_date',
		dbo.format_counselor_name(i.termination_counselor)	as 'termination_counselor',

		left(v.name, 256)						as 'client_name',
		convert(int,isnull(sum(t.minutes),0))	as 'minutes',
		mi.description							as 'interview_description',
		mr.description							as 'result_description',
		mt.description							as 'termination_description'

from	hud_interviews i
inner join view_client_address v on i.client = v.client
left outer join hud_transactions t on i.hud_interview = t.hud_interview
left outer join housing_PurposeOfVisitTypes mi with (nolock) on i.interview_type = mi.oID
left outer join Housing_ResultTypes mr with (nolock) on i.hud_result = mr.oID
left outer join Housing_TerminationReasonTypes mt with (nolock) on i.termination_reason	= mt.oID

where	(i.interview_date between @from_date and @to_date
or		i.result_date	 between @from_date and @to_date
or		i.termination_date between @from_date and @to_date)

group by i.client, v.name,
		 i.interview_type, i.interview_date, i.interview_counselor, mi.description,
		 i.hud_result, i.result_date, i.result_counselor, mr.description,
		 i.termination_reason, i.termination_date, i.termination_counselor, mt.description

order by i.interview_date, i.client

return ( @@rowcount )
GO
