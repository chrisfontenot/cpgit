USE [DebtPlus]
GO
/****** Object:  StoredProcedure [dbo].[xpr_insert_regions]    Script Date: 09/15/2014 13:13:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[xpr_insert_regions] ( @description as varchar(50), @default as bit = 0 ) as
-- ========================================================================================
-- ==   Procedure for .NET to insert items into the regions table                        ==
-- ========================================================================================
	insert into regions ( [description], [default] ) values ( @description, @default )
	return ( scope_identity() )
GO
