USE [DebtPlus]
GO
/****** Object:  StoredProcedure [dbo].[rpt_activated]    Script Date: 09/15/2014 13:13:32 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[rpt_activated] as

-- Suppress intermediate results
set nocount on

-- Create a list of pending clients
select	c.client, c.start_date, c.held_in_trust, c.counselor
into	#clients
from	clients c
where	active_status = 'PND'
and	c.client > 0

-- Delete the clients which have un-coded debts
delete	#clients
from	#clients x
where	client in (
	select	client
	from	client_creditor
	where	creditor is null
)

-- Return the list of client to the system
select	v.client as client,
	v.name   as name,
	c.start_date as start_date,
	c.held_in_trust as held_in_trust,
	c.counselor as counselor,
	dbo.format_normal_name (default, cox.first, default, cox.last, default) as counselor_name
from	#clients c
inner join view_client_address v on c.client = v.client
left outer join counselors co on c.counselor = co.counselor
left outer join names cox with (nolock) on co.nameid = cox.name
order by c.counselor, c.client

drop table #clients

return ( @@rowcount )
GO
