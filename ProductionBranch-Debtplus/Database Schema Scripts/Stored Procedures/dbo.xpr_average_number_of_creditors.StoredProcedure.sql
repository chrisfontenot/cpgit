USE [DebtPlus]
GO
/****** Object:  StoredProcedure [dbo].[xpr_average_number_of_creditors]    Script Date: 09/15/2014 13:13:34 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE PROCEDURE [dbo].[xpr_average_number_of_creditors] AS
-- =====================================================================================
-- ==          Calculate the average number of creditors for the summary report       ==
--- ====================================================================================

-- ChangeLog
--   01/15/2002
--     Added creditor_classes table references and dropped "creditors.fee_creditor"

SET NOCOUNT ON

SELECT	cc.client AS 'client',
	count(cc.creditor) AS 'creditors'
INTO	#average_number_of_creditors
FROM	client_creditor cc with (nolock)
INNER JOIN creditors cr with (nolock) ON cc.creditor = cr.creditor
left outer join creditor_classes ccl with (nolock) on cr.creditor_class = ccl.creditor_class
WHERE isnull(ccl.zero_balance,0) = 0
and	cc.reassigned_debt = 0
GROUP BY cc.client

SELECT AVG(convert(float,creditors)) AS 'average' FROM #average_number_of_creditors

DROP TABLE #average_number_of_creditors
SET NOCOUNT OFF
GO
