USE [DebtPlus]
GO
/****** Object:  StoredProcedure [dbo].[xpr_insert_creditor_drop_reason]    Script Date: 09/15/2014 13:13:35 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[xpr_insert_creditor_drop_reason] ( @creditor varchar(10), @rpps_code varchar(10) ) as
-- ========================================================================================
-- ==   Procedure for .NET to insert items into the insert_creditor_drop_reasons table   ==
-- ========================================================================================
	insert into creditor_drop_reasons ( [creditor], [rpps_code] ) values ( @creditor, @rpps_code )
	return ( scope_identity() )
GO
