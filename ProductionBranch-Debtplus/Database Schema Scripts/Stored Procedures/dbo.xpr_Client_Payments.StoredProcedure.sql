SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
ALTER procedure [xpr_Client_Payments] ( @Client AS INT, @Deposits AS INT = 1, @Disbursements AS INT = 1, @FromDate AS DateTime = NULL, @ToDate AS DateTime = NULL ) AS

-- ==================================================================================================================
-- ==            Generate the information for the "Payments" screen of the client                                  ==
-- ==================================================================================================================

-- ChangeLog
--   06/09/2009
--      Remove references to debt_number and use client_creditor

IF @FromDate IS NULL
	SET @FromDate = @ToDate

IF @ToDate IS NULL
	SET @ToDate = @FromDate

SELECT @FromDate = convert(DateTime, convert(varchar(10), @FromDate, 101) + ' 00:00:00')
SELECT @ToDate   = convert(DateTime, convert(varchar(10), @ToDate,   101) + ' 23:59:59')

-- ---------------------------------------------------------------------------------------------
-- --          Include both deposits and disbursements                                        --
-- ---------------------------------------------------------------------------------------------

IF (@deposits > 0) AND (@disbursements > 0)

	SELECT	tran_type				as 'type',
		client					as 'client',
		null					as 'creditor',
		0					as 'client_creditor',
		credit_amt				as 'amount',
		date_created				as 'item_date',
		isnull(tran_subtype,'')			as 'deposit_type',
		message					as 'reference',
		null					as 'trust_register'

	FROM	registers_client rc WITH (NOLOCK)
	WHERE	tran_type IN ( 'DP', 'VD', 'RF', 'RR', 'RE' )
	AND	client	= @client
	AND	rc.date_created BETWEEN @FromDate AND @ToDate

	UNION ALL

	SELECT	rcc.tran_type				as 'type',
		rcc.client				as 'client',
		rcc.creditor				as 'creditor',
		rcc.client_creditor				as 'client_creditor',
		rcc.debit_amt				as 'amount',
		rcc.date_created			as 'item_date',
		null					as 'item_date',
		null					as 'reference',
		tr.checknum				as 'trust_register'

	FROM	registers_client_creditor rcc WITH (NOLOCK)
	LEFT OUTER JOIN registers_trust tr WITH (NOLOCK) ON rcc.trust_register = tr.trust_register
	WHERE	rcc.tran_type IN ('AD', 'BW', 'CM', 'MD', 'CR', 'AR', 'VF')
	AND	rcc.client = @client
	AND	rcc.date_created BETWEEN @FromDate AND @ToDate

	ORDER BY 6, 1

-- ---------------------------------------------------------------------------------------------
-- --          Select items for a deposit batch only                                          --
-- ---------------------------------------------------------------------------------------------

ELSE IF @deposits > 0

	SELECT	tran_type				as 'type',
		client					as 'client',
		null					as 'creditor',
		0					as 'client_creditor',
		credit_amt				as 'amount',
		date_created				as 'item_date',
		isnull(tran_subtype,'')			as 'deposit_type',
		message					as 'reference',
		null					as 'trust_register'

	FROM	registers_client rc WITH (NOLOCK)
	WHERE	tran_type IN ( 'DP', 'VD', 'RF', 'RR', 'RE' )
	AND	client	= @client
	AND	rc.date_created BETWEEN @FromDate AND @ToDate

	ORDER BY 6, 1

-- ---------------------------------------------------------------------------------------------
-- --          Select items for a disbursement batch only                                     --
-- ---------------------------------------------------------------------------------------------

ELSE IF @disbursements > 0

	SELECT	rcc.tran_type				as 'type',
		rcc.client				as 'client',
		rcc.creditor				as 'creditor',
		rcc.client_creditor				as 'client_creditor',
		rcc.debit_amt				as 'amount',
		rcc.date_created			as 'item_date',
		null					as 'item_date',
		null					as 'reference',
		tr.checknum				as 'trust_register'

	FROM	registers_client_creditor rcc WITH (NOLOCK)
	LEFT OUTER JOIN registers_trust tr WITH (NOLOCK) ON rcc.trust_register = tr.trust_register
	WHERE	rcc.tran_type IN ('AD', 'BW', 'CM', 'MD', 'CR', 'AR', 'VF')
	AND	rcc.client = @client
	AND	rcc.date_created BETWEEN @FromDate AND @ToDate

	ORDER BY 6, 1

RETURN ( @@rowcount )
GO
