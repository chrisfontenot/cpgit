EXECUTE UPDATE_DROP_CONSTRAINTS 'vendors'
GO
/* To prevent any potential data loss issues, you should review this script in detail before running it outside the context of the database designer.*/
BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION
GO
CREATE TABLE dbo.Tmp_vendors
	(
	oID dbo.typ_key NOT NULL IDENTITY (1, 1),
	Type varchar(2) NOT NULL,
	Label varchar(10) NULL,
	Name varchar(50) NULL,
	ActiveFlag bit NOT NULL,
	BillingMode varchar(10) NOT NULL,
	ACH_CheckingSavings char(1) NOT NULL,
	ACH_RoutingNumber char(9) NULL,
	ACH_AccountNumber varchar(22) NULL,
	CC_ExpirationDate datetime NULL,
	CC_CID2 varchar(10) NULL,
	CVV int NULL,
	SupressPayments bit NOT NULL,
	SupressInvoices bit NOT NULL,
	NameOnCard varchar(50) NULL,
	SupressPrintingPayments bit NOT NULL,
	MonthlyBillingDay int NOT NULL,
	Website varchar(512) NULL,
	Additional xml NULL,
	date_created dbo.typ_date NOT NULL,
	created_by dbo.typ_counselor NOT NULL,
	ts timestamp NOT NULL
	)  ON [PRIMARY]
	 TEXTIMAGE_ON [PRIMARY]
GO
ALTER TABLE dbo.Tmp_vendors SET (LOCK_ESCALATION = TABLE)
GO
DECLARE @v sql_variant 
SET @v = N'If there is a website for this vendor, here is the URL to it.'
EXECUTE sp_addextendedproperty N'MS_Description', @v, N'SCHEMA', N'dbo', N'TABLE', N'Tmp_vendors', N'COLUMN', N'Website'
GO
EXECUTE sp_bindefault N'dbo.default_true', N'dbo.Tmp_vendors.ActiveFlag'
GO
ALTER TABLE dbo.Tmp_vendors ADD CONSTRAINT
	DF_vendors_BillingMode DEFAULT ('NONE') FOR BillingMode
GO
ALTER TABLE dbo.Tmp_vendors ADD CONSTRAINT
	DF_vendors_ACH_CheckingSavings DEFAULT ('C') FOR ACH_CheckingSavings
GO
EXECUTE sp_bindefault N'dbo.default_false', N'dbo.Tmp_vendors.SupressPayments'
GO
EXECUTE sp_bindefault N'dbo.default_false', N'dbo.Tmp_vendors.SupressInvoices'
GO
EXECUTE sp_bindefault N'dbo.default_false', N'dbo.Tmp_vendors.SupressPrintingPayments'
GO
EXECUTE sp_bindefault N'dbo.default_one', N'dbo.Tmp_vendors.MonthlyBillingDay'
GO
IF EXISTS(SELECT * FROM SYSOBJECTS WHERE TYPE='U' AND NAME=N'vendors')
BEGIN
	EXEC('SET IDENTITY_INSERT dbo.Tmp_vendors ON;
		  INSERT INTO dbo.Tmp_vendors (oID, Type, Label, Name, ActiveFlag, BillingMode, ACH_CheckingSavings, ACH_RoutingNumber, ACH_AccountNumber, CC_ExpirationDate, CC_CID2, SupressPayments, SupressInvoices, date_created, created_by, NameOnCard, SupressPrintingPayments, MonthlyBillingDay, CVV, Website, Additional)
		  SELECT oID, Type, Label, Name, ActiveFlag, BillingMode, ACH_CheckingSavings, ACH_RoutingNumber, ACH_AccountNumber, CC_ExpirationDate, CC_CID2, SupressPayments, SupressInvoices, date_created, created_by, NameOnCard, SupressPrintingPayments, MonthlyBillingDay, CVV, Website, Additional FROM dbo.vendors WITH (HOLDLOCK TABLOCKX);
		  SET IDENTITY_INSERT dbo.Tmp_vendors OFF;')
	EXEC('DROP TABLE dbo.vendors')
END
GO
EXECUTE sp_rename N'dbo.Tmp_vendors', N'vendors', 'OBJECT' 
GO
ALTER TABLE dbo.vendors ADD CONSTRAINT
	PK_vendors PRIMARY KEY CLUSTERED 
	(
	oID
	) WITH( STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
CREATE UNIQUE NONCLUSTERED INDEX IX_vendors_1 ON dbo.vendors
	(
	Label
	) WITH( STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
DECLARE @v sql_variant 
SET @v = N'Index of vendor labels'
EXECUTE sp_addextendedproperty N'MS_Description', @v, N'SCHEMA', N'dbo', N'TABLE', N'vendors', N'INDEX', N'IX_vendors_1'
GO
ALTER TABLE dbo.vendors ADD CONSTRAINT
	FK_vendors_vendor_types FOREIGN KEY
	(
	Type
	) REFERENCES dbo.vendor_types
	(
	oID
	) ON UPDATE  NO ACTION 
	 ON DELETE  NO ACTION 
GO
DECLARE @v sql_variant 
SET @v = N'Link the type of the vendor to the vendor_types table entries'
EXECUTE sp_addextendedproperty N'MS_Description', @v, N'SCHEMA', N'dbo', N'TABLE', N'vendors', N'CONSTRAINT', N'FK_vendors_vendor_types'
GO
EXECUTE sp_bindrule N'dbo.valid_vendor_BillingMode', N'dbo.vendors.BillingMode'
GO
EXECUTE sp_bindrule N'dbo.valid_vendor_CheckingSavings', N'dbo.vendors.ACH_CheckingSavings'
GO
EXECUTE sp_bindrule N'dbo.valid_vendor_ACH_RoutingNumber', N'dbo.vendors.ACH_RoutingNumber'
GO
EXECUTE sp_bindrule N'dbo.valid_MonthlyBillingDay', N'dbo.vendors.MonthlyBillingDay'
GO
CREATE TRIGGER trig_vendors_I_Label ON dbo.vendors AFTER INSERT AS
BEGIN
	SET NOCOUNT ON

	-- On insert operation, set the label from the ID and the type.
	UPDATE		vendors
	SET			Label = v.[Type] + '-' + right('0000' + convert(varchar, v.[oID]), 4)
	FROM		vendors v
	INNER JOIN  inserted i on v.oID = i.oID
	WHERE		v.[Label] IS NULL
END
GO
COMMIT
BEGIN TRANSACTION
GO
ALTER TABLE dbo.vendor_products ADD CONSTRAINT
	FK_vendor_products_vendors FOREIGN KEY
	(
	vendor
	) REFERENCES dbo.vendors
	(
	oID
	) ON UPDATE  CASCADE 
	 ON DELETE  CASCADE 
GO
ALTER TABLE dbo.vendor_products SET (LOCK_ESCALATION = TABLE)
GO
COMMIT
BEGIN TRANSACTION
GO
ALTER TABLE dbo.vendor_notes ADD CONSTRAINT
	FK_vendor_notes_vendors FOREIGN KEY
	(
	vendor
	) REFERENCES dbo.vendors
	(
	oID
	) ON UPDATE  CASCADE 
	 ON DELETE  CASCADE 
GO
ALTER TABLE dbo.vendor_notes SET (LOCK_ESCALATION = TABLE)
GO
COMMIT
BEGIN TRANSACTION
GO
ALTER TABLE dbo.vendor_contacts ADD CONSTRAINT
	FK_vendor_contacts_vendors FOREIGN KEY
	(
	vendor
	) REFERENCES dbo.vendors
	(
	oID
	) ON UPDATE  CASCADE 
	 ON DELETE  CASCADE 
GO
ALTER TABLE dbo.vendor_contacts SET (LOCK_ESCALATION = TABLE)
GO
COMMIT
BEGIN TRANSACTION
GO
ALTER TABLE dbo.vendor_addresses ADD CONSTRAINT
	FK_vendor_addresses_vendors FOREIGN KEY
	(
	vendor
	) REFERENCES dbo.vendors
	(
	oID
	) ON UPDATE  CASCADE 
	 ON DELETE  CASCADE 
GO
ALTER TABLE dbo.vendor_addresses SET (LOCK_ESCALATION = TABLE)
GO
COMMIT
BEGIN TRANSACTION
GO
ALTER TABLE dbo.vendor_addkeys ADD CONSTRAINT
	FK_vendor_addkeys_vendors FOREIGN KEY
	(
	vendor
	) REFERENCES dbo.vendors
	(
	oID
	) ON UPDATE  CASCADE 
	 ON DELETE  CASCADE 
GO
ALTER TABLE dbo.vendor_addkeys SET (LOCK_ESCALATION = TABLE)
GO
COMMIT
BEGIN TRANSACTION
GO
ALTER TABLE dbo.product_transactions ADD CONSTRAINT
	FK_product_transactions_vendors FOREIGN KEY
	(
	vendor
	) REFERENCES dbo.vendors
	(
	oID
	) ON UPDATE  NO ACTION 
	 ON DELETE  NO ACTION 
GO
ALTER TABLE dbo.product_transactions SET (LOCK_ESCALATION = TABLE)
GO
COMMIT
BEGIN TRANSACTION
GO
ALTER TABLE dbo.client_products ADD CONSTRAINT
	FK_client_products_vendors FOREIGN KEY
	(
	vendor
	) REFERENCES dbo.vendors
	(
	oID
	) ON UPDATE  CASCADE 
	 ON DELETE  CASCADE 
GO
ALTER TABLE dbo.client_products SET (LOCK_ESCALATION = TABLE)
GO
COMMIT
