EXECUTE UPDATE_DROP_CONSTRAINTS 'products'
GO
/* To prevent any potential data loss issues, you should review this script in detail before running it outside the context of the database designer.*/
BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION
GO
CREATE TABLE dbo.Tmp_products
	(
	oID dbo.typ_key NOT NULL IDENTITY (1, 1),
	description varchar(50) NULL,
	Amount money NOT NULL,
	ActiveFlag bit NOT NULL,
	[Default] bit NOT NULL,
	InvoiceEmailTemplateID dbo.typ_key NOT NULL,
	date_created dbo.typ_date NOT NULL,
	created_by dbo.typ_counselor NOT NULL,
	ts timestamp NOT NULL
	)  ON [PRIMARY]
GO
ALTER TABLE dbo.Tmp_products SET (LOCK_ESCALATION = TABLE)
GO
EXECUTE sp_bindefault N'dbo.default_money', N'dbo.Tmp_products.Amount'
GO
EXECUTE sp_bindefault N'dbo.default_true', N'dbo.Tmp_products.ActiveFlag'
GO
EXECUTE sp_bindefault N'dbo.default_false', N'dbo.Tmp_products.[Default]'
GO
IF EXISTS(SELECT * FROM SYSOBJECTS WHERE TYPE='U' AND NAME=N'products')
BEGIN
	EXEC('SET IDENTITY_INSERT dbo.Tmp_products ON;
		  INSERT INTO dbo.Tmp_products (oID, description, Amount, ActiveFlag, [Default], InvoiceEmailTemplateID, date_created, created_by)
		  SELECT oID, description, Amount, ActiveFlag, [Default], InvoiceEmailTemplateID, date_created, created_by FROM dbo.products WITH (HOLDLOCK TABLOCKX);
		  SET IDENTITY_INSERT dbo.Tmp_products OFF;')
	EXEC('DROP TABLE dbo.products')
END
GO
EXECUTE sp_rename N'dbo.Tmp_products', N'products', 'OBJECT' 
GO
ALTER TABLE dbo.products ADD CONSTRAINT
	PK_products PRIMARY KEY CLUSTERED 
	(
	oID
	) WITH( STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
ALTER TABLE dbo.products ADD CONSTRAINT
	FK_products_email_templates FOREIGN KEY
	(
	InvoiceEmailTemplateID
	) REFERENCES dbo.email_templates
	(
	oID
	) ON UPDATE  NO ACTION 
	 ON DELETE  NO ACTION 
GO
COMMIT
BEGIN TRANSACTION
GO
ALTER TABLE dbo.vendor_products ADD CONSTRAINT
	FK_vendor_products_products FOREIGN KEY
	(
	product
	) REFERENCES dbo.products
	(
	oID
	) ON UPDATE  CASCADE 
	 ON DELETE  CASCADE 
GO
ALTER TABLE dbo.vendor_products SET (LOCK_ESCALATION = TABLE)
GO
COMMIT
BEGIN TRANSACTION
GO
ALTER TABLE dbo.product_states ADD CONSTRAINT
	FK_product_states_products FOREIGN KEY
	(
	productID
	) REFERENCES dbo.products
	(
	oID
	) ON UPDATE  CASCADE 
	 ON DELETE  CASCADE 
GO
ALTER TABLE dbo.product_states SET (LOCK_ESCALATION = TABLE)
GO
COMMIT
BEGIN TRANSACTION
GO
ALTER TABLE dbo.product_transactions ADD CONSTRAINT
	FK_product_transactions_products FOREIGN KEY
	(
	product
	) REFERENCES dbo.products
	(
	oID
	) ON UPDATE  NO ACTION 
	 ON DELETE  NO ACTION 
GO
ALTER TABLE dbo.product_transactions SET (LOCK_ESCALATION = TABLE)
GO
COMMIT
BEGIN TRANSACTION
GO
ALTER TABLE dbo.client_products ADD CONSTRAINT
	FK_client_products_products FOREIGN KEY
	(
	product
	) REFERENCES dbo.products
	(
	oID
	) ON UPDATE  NO ACTION 
	 ON DELETE  NO ACTION 
GO
ALTER TABLE dbo.client_products SET (LOCK_ESCALATION = TABLE)
GO
COMMIT
