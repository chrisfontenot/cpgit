/*
   Wednesday, November 04, 20156:10:06 AM
   User: sa
   Server: 10.0.199.12
   Database: DebtPlus_HPF
   Application: 
*/

/* To prevent any potential data loss issues, you should review this script in detail before running it outside the context of the database designer.*/
BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION
GO
EXECUTE UPDATE_ADD_COLUMN 'militaryStatusTypes', 'hpf'
GO
CREATE TABLE dbo.Tmp_militaryStatusTypes
	(
	oID dbo.typ_key NOT NULL IDENTITY (1, 1),
	description dbo.typ_description NOT NULL,
	hpf varchar(50) NULL,
	ActiveFlag bit NOT NULL,
	[Default] bit NOT NULL,
	date_created dbo.typ_date NOT NULL,
	created_by dbo.typ_counselor NOT NULL,
	ts timestamp NOT NULL
	)  ON [PRIMARY]
GO
GRANT DELETE ON dbo.Tmp_militaryStatusTypes TO public  AS dbo
GO
GRANT INSERT ON dbo.Tmp_militaryStatusTypes TO public  AS dbo
GO
GRANT SELECT ON dbo.Tmp_militaryStatusTypes TO public  AS dbo
GO
GRANT UPDATE ON dbo.Tmp_militaryStatusTypes TO public  AS dbo
GO
EXECUTE sp_bindefault N'dbo.default_true', N'dbo.Tmp_militaryStatusTypes.ActiveFlag'
GO
EXECUTE sp_bindefault N'dbo.default_false', N'dbo.Tmp_militaryStatusTypes.[Default]'
GO
SET IDENTITY_INSERT dbo.Tmp_militaryStatusTypes ON
GO
IF EXISTS(SELECT * FROM dbo.militaryStatusTypes)
	 EXEC('INSERT INTO dbo.Tmp_militaryStatusTypes (oID, description, ActiveFlag, [Default], created_by, date_created, hpf)
		SELECT oID, description, ActiveFlag, [Default], created_by, date_created, hpf FROM dbo.militaryStatusTypes WITH (HOLDLOCK TABLOCKX)')
GO
SET IDENTITY_INSERT dbo.Tmp_militaryStatusTypes OFF
GO
DROP TABLE dbo.militaryStatusTypes
GO
EXECUTE sp_rename N'dbo.Tmp_militaryStatusTypes', N'militaryStatusTypes', 'OBJECT' 
GO
ALTER TABLE dbo.militaryStatusTypes ADD CONSTRAINT
	PK_military_StatusTypes PRIMARY KEY CLUSTERED 
	(
	oID
	) WITH( STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]

GO
DENY DELETE ON dbo.militaryStatusTypes TO www_role  AS dbo 
GO
DENY INSERT ON dbo.militaryStatusTypes TO www_role  AS dbo 
GO
DENY SELECT ON dbo.militaryStatusTypes TO www_role  AS dbo 
GO
DENY UPDATE ON dbo.militaryStatusTypes TO www_role  AS dbo 
GO
COMMIT
