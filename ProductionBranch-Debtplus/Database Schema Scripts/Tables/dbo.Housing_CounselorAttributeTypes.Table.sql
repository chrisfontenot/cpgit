USE [DebtPlus]
GO
/****** Object:  Table [dbo].[Housing_CounselorAttributeTypes]    Script Date: 09/15/2014 13:13:26 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Housing_CounselorAttributeTypes](
	[oID] [dbo].[typ_key] IDENTITY(1,1) NOT NULL,
	[description] [dbo].[typ_description] NOT NULL,
	[hud_9902_section] [varchar](256) NULL,
	[ActiveFlag] [bit] NOT NULL,
 CONSTRAINT [PK_Housing_CounselorAttributeTypes] PRIMARY KEY CLUSTERED 
(
	[oID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Primary key to the table' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Housing_CounselorAttributeTypes', @level2type=N'COLUMN',@level2name=N'oID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Descriptive text for the attribute' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Housing_CounselorAttributeTypes', @level2type=N'COLUMN',@level2name=N'description'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Value of the "name" section in the referenceinfo' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Housing_CounselorAttributeTypes', @level2type=N'COLUMN',@level2name=N'hud_9902_section'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Allow the attribute to be checked "True" for a counselor' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Housing_CounselorAttributeTypes', @level2type=N'COLUMN',@level2name=N'ActiveFlag'
GO
