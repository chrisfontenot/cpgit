EXEC ('update clients set counselor = null where counselor not in (select counselor from counselors)')
GO
EXEC ('update clients set csr = null where csr not in (select csr from counselors)')
GO
EXEC ('update clients set config_fee = dbo.default_config_fee() where config_fee not in (select config_fee from config_fees)')
GO
EXEC ('update clients set county = (select min(county) from counties where [default] = 1) where county not in (select county from counties)')
GO
EXEC ('update clients set office = null where office not in (select office from offices)')
GO
EXEC ('update clients set region = (select min(region) from regions where [default] = 1) where region not in (select region from regions)')
GO
EXEC ('update clients set drop_reason = null where drop_reason is not null and drop_reason not in (select drop_reason from drop_reasons)')
GO
EXEC ('update clients set referred_by = null where referred_by is not null and referred_by not in (select referred_by from referred_by)')
GO
EXEC ('update clients set cause_fin_problem1 = null where cause_fin_problem1 is not null and cause_fin_problem1 not in (select financial_problem from financial_problems)')
GO
EXEC ('update clients set cause_fin_problem2 = null where cause_fin_problem2 is not null and cause_fin_problem2 not in (select financial_problem from financial_problems)')
GO
EXEC ('update clients set cause_fin_problem3 = null where cause_fin_problem3 is not null and cause_fin_problem3 not in (select financial_problem from financial_problems)')
GO
EXEC ('update clients set cause_fin_problem4 = null where cause_fin_problem4 is not null and cause_fin_problem4 not in (select financial_problem from financial_problems)')
GO
/* To prevent any potential data loss issues, you should review this script in detail before running it outside the context of the database designer.*/
BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION
GO
EXEC UPDATE_DROP_CONSTRAINTS 'clients'
GO
EXEC UPDATE_ADD_COLUMN 'clients', 'DoNotDisturb', 'bit'
GO
CREATE TABLE dbo.Tmp_clients
	(
	client dbo.typ_client NOT NULL IDENTITY (1001, 1),
	client_guid uniqueidentifier NOT NULL,
	salutation varchar(50) NULL,
	AddressID dbo.typ_key NULL,
	HomeTelephoneID dbo.typ_key NULL,
	MsgTelephoneID dbo.typ_key NULL,
	county dbo.typ_key NOT NULL,
	region dbo.typ_key NOT NULL,
	active_status varchar(3) NOT NULL,
	active_status_date datetime NULL,
	dmp_status_date datetime NULL,
	client_status varchar(3) NULL,
	client_status_date datetime NULL,
	disbursement_date int NOT NULL,
	mail_error_date datetime NULL,
	start_date datetime NULL,
	restart_date datetime NULL,
	drop_date datetime NULL,
	drop_reason dbo.typ_key NULL,
	drop_reason_other dbo.typ_description NULL,
	referred_by dbo.typ_key NULL,
	cause_fin_problem1 dbo.typ_key NULL,
	cause_fin_problem2 dbo.typ_key NULL,
	cause_fin_problem3 dbo.typ_key NULL,
	cause_fin_problem4 dbo.typ_key NULL,
	office dbo.typ_key NULL,
	counselor dbo.typ_key NULL,
	csr dbo.typ_key NULL,
	hold_disbursements bit NOT NULL,
	personal_checks bit NOT NULL,
	ach_active bit NOT NULL,
	stack_proration bit NOT NULL,
	mortgage_problems bit NOT NULL,
	intake_agreement bit NOT NULL,
	DoNotDisturb bit NOT NULL,
	ElectronicCorrespondence int NOT NULL,
	ElectronicStatements int NOT NULL,
	bankruptcy_class dbo.typ_key NOT NULL,
	fed_tax_owed money NOT NULL,
	state_tax_owed money NOT NULL,
	local_tax_owed money NOT NULL,
	fed_tax_months int NOT NULL,
	state_tax_months int NOT NULL,
	local_tax_months int NOT NULL,
	held_in_trust money NOT NULL,
	deposit_in_trust money NOT NULL,
	reserved_in_trust money NOT NULL,
	reserved_in_trust_cutoff datetime NULL,
	marital_status int NULL,
	language dbo.typ_key NULL,
	dependents int NOT NULL,
	household dbo.typ_key NOT NULL,
	method_first_contact dbo.typ_key NOT NULL,
	preferred_contact dbo.typ_key NOT NULL,
	satisfaction_score int NOT NULL,
	config_fee dbo.typ_key NOT NULL,
	first_appt dbo.typ_key NULL,
	first_kept_appt dbo.typ_key NULL,
	first_resched_appt dbo.typ_key NULL,
	program_months int NOT NULL,
	first_deposit_date datetime NULL,
	last_deposit_date datetime NULL,
	last_deposit_amount money NULL,
	InboundTelephoneGroupID int NULL,
	InboundTelephoneNumberID int NULL,
	payout_total_debt money NOT NULL,
	payout_months_to_payout int NOT NULL,
	payout_total_fees money NOT NULL,
	payout_total_interest money NOT NULL,
	payout_total_payments money NOT NULL,
	payout_deposit_amount money NOT NULL,
	payout_monthly_fee money NOT NULL,
	payout_cushion_amount money NOT NULL,
	payout_termination_date datetime NULL,
	initial_program dbo.typ_key NOT NULL,
	current_program dbo.typ_key NOT NULL,
	created_by dbo.typ_counselor NOT NULL,
	date_created dbo.typ_date NOT NULL,
	ts timestamp NOT NULL,
	housing_status dbo.typ_key NULL,
	housing_type dbo.typ_key NULL,
	people int NULL
	)  ON [PRIMARY]
GO
GRANT SELECT,INSERT,UPDATE,DELETE ON dbo.Tmp_clients TO public  AS dbo
GO
DECLARE @v sql_variant 
SET @v = N'Client ID. This is the primary key.'
EXECUTE sp_addextendedproperty N'MS_Description', @v, N'SCHEMA', N'dbo', N'TABLE', N'Tmp_clients', N'COLUMN', N'client'
GO
DECLARE @v sql_variant 
SET @v = N'Salutation used in writing letters to the client'
EXECUTE sp_addextendedproperty N'MS_Description', @v, N'SCHEMA', N'dbo', N'TABLE', N'Tmp_clients', N'COLUMN', N'salutation'
GO
DECLARE @v sql_variant 
SET @v = N'County or district in the state'
EXECUTE sp_addextendedproperty N'MS_Description', @v, N'SCHEMA', N'dbo', N'TABLE', N'Tmp_clients', N'COLUMN', N'county'
GO
DECLARE @v sql_variant 
SET @v = N'Region identifier for this client. Initially loaded from the office.'
EXECUTE sp_addextendedproperty N'MS_Description', @v, N'SCHEMA', N'dbo', N'TABLE', N'Tmp_clients', N'COLUMN', N'region'
GO
DECLARE @v sql_variant 
SET @v = N'Active status for the client'
EXECUTE sp_addextendedproperty N'MS_Description', @v, N'SCHEMA', N'dbo', N'TABLE', N'Tmp_clients', N'COLUMN', N'active_status'
GO
DECLARE @v sql_variant 
SET @v = N'Date / Time when the active status was last changed'
EXECUTE sp_addextendedproperty N'MS_Description', @v, N'SCHEMA', N'dbo', N'TABLE', N'Tmp_clients', N'COLUMN', N'active_status_date'
GO
DECLARE @v sql_variant 
SET @v = N'Date / Time when the client_status was set to "DMP"'
EXECUTE sp_addextendedproperty N'MS_Description', @v, N'SCHEMA', N'dbo', N'TABLE', N'Tmp_clients', N'COLUMN', N'dmp_status_date'
GO
DECLARE @v sql_variant 
SET @v = N'Client status. This is normally the result of the last appointment.'
EXECUTE sp_addextendedproperty N'MS_Description', @v, N'SCHEMA', N'dbo', N'TABLE', N'Tmp_clients', N'COLUMN', N'client_status'
GO
DECLARE @v sql_variant 
SET @v = N'Date / Time when client_status was last changed.'
EXECUTE sp_addextendedproperty N'MS_Description', @v, N'SCHEMA', N'dbo', N'TABLE', N'Tmp_clients', N'COLUMN', N'client_status_date'
GO
DECLARE @v sql_variant 
SET @v = N'Value used to select clients for disbursement batches.'
EXECUTE sp_addextendedproperty N'MS_Description', @v, N'SCHEMA', N'dbo', N'TABLE', N'Tmp_clients', N'COLUMN', N'disbursement_date'
GO
DECLARE @v sql_variant 
SET @v = N'Date when mail was returned as "undelivered"'
EXECUTE sp_addextendedproperty N'MS_Description', @v, N'SCHEMA', N'dbo', N'TABLE', N'Tmp_clients', N'COLUMN', N'mail_error_date'
GO
DECLARE @v sql_variant 
SET @v = N'Starting date for disbursements'
EXECUTE sp_addextendedproperty N'MS_Description', @v, N'SCHEMA', N'dbo', N'TABLE', N'Tmp_clients', N'COLUMN', N'start_date'
GO
DECLARE @v sql_variant 
SET @v = N'Date / Time when the client was changed from "inactive" to "active" as a restart operation.'
EXECUTE sp_addextendedproperty N'MS_Description', @v, N'SCHEMA', N'dbo', N'TABLE', N'Tmp_clients', N'COLUMN', N'restart_date'
GO
DECLARE @v sql_variant 
SET @v = N'Date when the client was changed to "inactive" and dropped'
EXECUTE sp_addextendedproperty N'MS_Description', @v, N'SCHEMA', N'dbo', N'TABLE', N'Tmp_clients', N'COLUMN', N'drop_date'
GO
DECLARE @v sql_variant 
SET @v = N'Reason for dropping the client'
EXECUTE sp_addextendedproperty N'MS_Description', @v, N'SCHEMA', N'dbo', N'TABLE', N'Tmp_clients', N'COLUMN', N'drop_reason'
GO
DECLARE @v sql_variant 
SET @v = N'If the reason is not listed in the table, this is the text reason.'
EXECUTE sp_addextendedproperty N'MS_Description', @v, N'SCHEMA', N'dbo', N'TABLE', N'Tmp_clients', N'COLUMN', N'drop_reason_other'
GO
DECLARE @v sql_variant 
SET @v = N'Who or what agency sent us the client.'
EXECUTE sp_addextendedproperty N'MS_Description', @v, N'SCHEMA', N'dbo', N'TABLE', N'Tmp_clients', N'COLUMN', N'referred_by'
GO
DECLARE @v sql_variant 
SET @v = N'Primary cause for financial problem'
EXECUTE sp_addextendedproperty N'MS_Description', @v, N'SCHEMA', N'dbo', N'TABLE', N'Tmp_clients', N'COLUMN', N'cause_fin_problem1'
GO
DECLARE @v sql_variant 
SET @v = N'Secondary cause for financial problem'
EXECUTE sp_addextendedproperty N'MS_Description', @v, N'SCHEMA', N'dbo', N'TABLE', N'Tmp_clients', N'COLUMN', N'cause_fin_problem2'
GO
DECLARE @v sql_variant 
SET @v = N'Third cause for financial problem'
EXECUTE sp_addextendedproperty N'MS_Description', @v, N'SCHEMA', N'dbo', N'TABLE', N'Tmp_clients', N'COLUMN', N'cause_fin_problem3'
GO
DECLARE @v sql_variant 
SET @v = N'Fourth cause for financial problem'
EXECUTE sp_addextendedproperty N'MS_Description', @v, N'SCHEMA', N'dbo', N'TABLE', N'Tmp_clients', N'COLUMN', N'cause_fin_problem4'
GO
DECLARE @v sql_variant 
SET @v = N'This is a pointer to the current office for the client.'
EXECUTE sp_addextendedproperty N'MS_Description', @v, N'SCHEMA', N'dbo', N'TABLE', N'Tmp_clients', N'COLUMN', N'office'
GO
DECLARE @v sql_variant 
SET @v = N'This is a pointer to the current counselor for the client.'
EXECUTE sp_addextendedproperty N'MS_Description', @v, N'SCHEMA', N'dbo', N'TABLE', N'Tmp_clients', N'COLUMN', N'counselor'
GO
DECLARE @v sql_variant 
SET @v = N'Do we wish to hold disbursements on this client?'
EXECUTE sp_addextendedproperty N'MS_Description', @v, N'SCHEMA', N'dbo', N'TABLE', N'Tmp_clients', N'COLUMN', N'hold_disbursements'
GO
DECLARE @v sql_variant 
SET @v = N'Are personal checks acceptable for this client?'
EXECUTE sp_addextendedproperty N'MS_Description', @v, N'SCHEMA', N'dbo', N'TABLE', N'Tmp_clients', N'COLUMN', N'personal_checks'
GO
DECLARE @v sql_variant 
SET @v = N'Is the client on ACH?'
EXECUTE sp_addextendedproperty N'MS_Description', @v, N'SCHEMA', N'dbo', N'TABLE', N'Tmp_clients', N'COLUMN', N'ach_active'
GO
DECLARE @v sql_variant 
SET @v = N'Should the prorate function "stack" creditors when there is excess money?'
EXECUTE sp_addextendedproperty N'MS_Description', @v, N'SCHEMA', N'dbo', N'TABLE', N'Tmp_clients', N'COLUMN', N'stack_proration'
GO
DECLARE @v sql_variant 
SET @v = N'Did the client start with mortgage problems when it was created?'
EXECUTE sp_addextendedproperty N'MS_Description', @v, N'SCHEMA', N'dbo', N'TABLE', N'Tmp_clients', N'COLUMN', N'mortgage_problems'
GO
DECLARE @v sql_variant 
SET @v = N'If this is an intake (web site) client, did the client agree to the "Statement of Counseling Services?"'
EXECUTE sp_addextendedproperty N'MS_Description', @v, N'SCHEMA', N'dbo', N'TABLE', N'Tmp_clients', N'COLUMN', N'intake_agreement'
GO
DECLARE @v sql_variant 
SET @v = N'Did the client arrive for bankrupcy issues?'
EXECUTE sp_addextendedproperty N'MS_Description', @v, N'SCHEMA', N'dbo', N'TABLE', N'Tmp_clients', N'COLUMN', N'bankruptcy_class'
GO
DECLARE @v sql_variant 
SET @v = N'Dollar amount of federal income taxes owed by the client'
EXECUTE sp_addextendedproperty N'MS_Description', @v, N'SCHEMA', N'dbo', N'TABLE', N'Tmp_clients', N'COLUMN', N'fed_tax_owed'
GO
DECLARE @v sql_variant 
SET @v = N'Dollar amount of local taxes owed by the client'
EXECUTE sp_addextendedproperty N'MS_Description', @v, N'SCHEMA', N'dbo', N'TABLE', N'Tmp_clients', N'COLUMN', N'local_tax_owed'
GO
DECLARE @v sql_variant 
SET @v = N'Number of months that the federal taxes are past-due'
EXECUTE sp_addextendedproperty N'MS_Description', @v, N'SCHEMA', N'dbo', N'TABLE', N'Tmp_clients', N'COLUMN', N'fed_tax_months'
GO
DECLARE @v sql_variant 
SET @v = N'Number of months that the statel taxes are past-due'
EXECUTE sp_addextendedproperty N'MS_Description', @v, N'SCHEMA', N'dbo', N'TABLE', N'Tmp_clients', N'COLUMN', N'state_tax_months'
GO
DECLARE @v sql_variant 
SET @v = N'Number of months that the local taxes are past-due'
EXECUTE sp_addextendedproperty N'MS_Description', @v, N'SCHEMA', N'dbo', N'TABLE', N'Tmp_clients', N'COLUMN', N'local_tax_months'
GO
DECLARE @v sql_variant 
SET @v = N'Dollar amount of money held in reserve for disbursements'
EXECUTE sp_addextendedproperty N'MS_Description', @v, N'SCHEMA', N'dbo', N'TABLE', N'Tmp_clients', N'COLUMN', N'held_in_trust'
GO
DECLARE @v sql_variant 
SET @v = N'Amount of money in unposted deposits'
EXECUTE sp_addextendedproperty N'MS_Description', @v, N'SCHEMA', N'dbo', N'TABLE', N'Tmp_clients', N'COLUMN', N'deposit_in_trust'
GO
DECLARE @v sql_variant 
SET @v = N'Amount of money not to be disbursed'
EXECUTE sp_addextendedproperty N'MS_Description', @v, N'SCHEMA', N'dbo', N'TABLE', N'Tmp_clients', N'COLUMN', N'reserved_in_trust'
GO
DECLARE @v sql_variant 
SET @v = N'Date when the reserved_in_trust is no longer valid'
EXECUTE sp_addextendedproperty N'MS_Description', @v, N'SCHEMA', N'dbo', N'TABLE', N'Tmp_clients', N'COLUMN', N'reserved_in_trust_cutoff'
GO
DECLARE @v sql_variant 
SET @v = N'Marital status for the client (single, married, divorced, etc.)'
EXECUTE sp_addextendedproperty N'MS_Description', @v, N'SCHEMA', N'dbo', N'TABLE', N'Tmp_clients', N'COLUMN', N'marital_status'
GO
DECLARE @v sql_variant 
SET @v = N'Pointer to the language requirement of the client'
EXECUTE sp_addextendedproperty N'MS_Description', @v, N'SCHEMA', N'dbo', N'TABLE', N'Tmp_clients', N'COLUMN', N'language'
GO
DECLARE @v sql_variant 
SET @v = N'Number of dependents for this client'
EXECUTE sp_addextendedproperty N'MS_Description', @v, N'SCHEMA', N'dbo', N'TABLE', N'Tmp_clients', N'COLUMN', N'dependents'
GO
DECLARE @v sql_variant 
SET @v = N'Relationship between household head and dependents'
EXECUTE sp_addextendedproperty N'MS_Description', @v, N'SCHEMA', N'dbo', N'TABLE', N'Tmp_clients', N'COLUMN', N'household'
GO
DECLARE @v sql_variant 
SET @v = N'Method of first contact for the client.'
EXECUTE sp_addextendedproperty N'MS_Description', @v, N'SCHEMA', N'dbo', N'TABLE', N'Tmp_clients', N'COLUMN', N'method_first_contact'
GO
DECLARE @v sql_variant 
SET @v = N'Preferred method of contact'
EXECUTE sp_addextendedproperty N'MS_Description', @v, N'SCHEMA', N'dbo', N'TABLE', N'Tmp_clients', N'COLUMN', N'preferred_contact'
GO
DECLARE @v sql_variant 
SET @v = N'Client statisfaction level with the agency.'
EXECUTE sp_addextendedproperty N'MS_Description', @v, N'SCHEMA', N'dbo', N'TABLE', N'Tmp_clients', N'COLUMN', N'satisfaction_score'
GO
DECLARE @v sql_variant 
SET @v = N'Pointer to the client''s PAF fee calculation routine. Initially taken from the config table.'
EXECUTE sp_addextendedproperty N'MS_Description', @v, N'SCHEMA', N'dbo', N'TABLE', N'Tmp_clients', N'COLUMN', N'config_fee'
GO
DECLARE @v sql_variant 
SET @v = N'Pointer the client''s first appointment in client_appointments table'
EXECUTE sp_addextendedproperty N'MS_Description', @v, N'SCHEMA', N'dbo', N'TABLE', N'Tmp_clients', N'COLUMN', N'first_appt'
GO
DECLARE @v sql_variant 
SET @v = N'Pointer to the first appointment that the client really kept.'
EXECUTE sp_addextendedproperty N'MS_Description', @v, N'SCHEMA', N'dbo', N'TABLE', N'Tmp_clients', N'COLUMN', N'first_kept_appt'
GO
DECLARE @v sql_variant 
SET @v = N'If the first appointment is rescheduled, this is a pointer to the new appointment.'
EXECUTE sp_addextendedproperty N'MS_Description', @v, N'SCHEMA', N'dbo', N'TABLE', N'Tmp_clients', N'COLUMN', N'first_resched_appt'
GO
DECLARE @v sql_variant 
SET @v = N'Number of months that the client is "A"ctive on the DMP.'
EXECUTE sp_addextendedproperty N'MS_Description', @v, N'SCHEMA', N'dbo', N'TABLE', N'Tmp_clients', N'COLUMN', N'program_months'
GO
DECLARE @v sql_variant 
SET @v = N'Date of the first deposit for the client'
EXECUTE sp_addextendedproperty N'MS_Description', @v, N'SCHEMA', N'dbo', N'TABLE', N'Tmp_clients', N'COLUMN', N'first_deposit_date'
GO
DECLARE @v sql_variant 
SET @v = N'Date of the last client deposit'
EXECUTE sp_addextendedproperty N'MS_Description', @v, N'SCHEMA', N'dbo', N'TABLE', N'Tmp_clients', N'COLUMN', N'last_deposit_date'
GO
DECLARE @v sql_variant 
SET @v = N'Dollar amount of the last cleint''s deposit'
EXECUTE sp_addextendedproperty N'MS_Description', @v, N'SCHEMA', N'dbo', N'TABLE', N'Tmp_clients', N'COLUMN', N'last_deposit_amount'
GO
DECLARE @v sql_variant 
SET @v = N'Pointer to the associated Inbound Call Number Group for this client'
EXECUTE sp_addextendedproperty N'MS_Description', @v, N'SCHEMA', N'dbo', N'TABLE', N'Tmp_clients', N'COLUMN', N'InboundTelephoneGroupID'
GO
DECLARE @v sql_variant 
SET @v = N'Pointer to the associated Inbound Call Number for this client'
EXECUTE sp_addextendedproperty N'MS_Description', @v, N'SCHEMA', N'dbo', N'TABLE', N'Tmp_clients', N'COLUMN', N'InboundTelephoneNumberID'
GO
DECLARE @v sql_variant 
SET @v = N'Name of the person who created the client'
EXECUTE sp_addextendedproperty N'MS_Description', @v, N'SCHEMA', N'dbo', N'TABLE', N'Tmp_clients', N'COLUMN', N'created_by'
GO
DECLARE @v sql_variant 
SET @v = N'Date / Time when the client was created'
EXECUTE sp_addextendedproperty N'MS_Description', @v, N'SCHEMA', N'dbo', N'TABLE', N'Tmp_clients', N'COLUMN', N'date_created'
GO
DECLARE @v sql_variant 
SET @v = N'Housing status for the client (rent, own, etc.)'
EXECUTE sp_addextendedproperty N'MS_Description', @v, N'SCHEMA', N'dbo', N'TABLE', N'Tmp_clients', N'COLUMN', N'housing_status'
GO
DECLARE @v sql_variant 
SET @v = N'Type of housing for the client (apartment, single family home, duplex, etc.)'
EXECUTE sp_addextendedproperty N'MS_Description', @v, N'SCHEMA', N'dbo', N'TABLE', N'Tmp_clients', N'COLUMN', N'housing_type'
GO
DECLARE @v sql_variant 
SET @v = N'Number of people in the household for the client'
EXECUTE sp_addextendedproperty N'MS_Description', @v, N'SCHEMA', N'dbo', N'TABLE', N'Tmp_clients', N'COLUMN', N'people'
GO
ALTER TABLE dbo.Tmp_clients ADD CONSTRAINT
	DF_client_client_guid DEFAULT (newid()) FOR client_guid
GO
EXECUTE sp_bindefault N'dbo.default_one', N'dbo.Tmp_clients.county'
GO
EXECUTE sp_bindefault N'dbo.default_one', N'dbo.Tmp_clients.region'
GO
ALTER TABLE dbo.Tmp_clients ADD CONSTRAINT
	DF_clients_active_status DEFAULT ('CRE') FOR active_status
GO
EXECUTE sp_bindefault N'dbo.default_one', N'dbo.Tmp_clients.disbursement_date'
GO
EXECUTE sp_bindefault N'dbo.default_false', N'dbo.Tmp_clients.hold_disbursements'
GO
EXECUTE sp_bindefault N'dbo.default_false', N'dbo.Tmp_clients.personal_checks'
GO
EXECUTE sp_bindefault N'dbo.default_false', N'dbo.Tmp_clients.ach_active'
GO
EXECUTE sp_bindefault N'dbo.default_false', N'dbo.Tmp_clients.stack_proration'
GO
EXECUTE sp_bindefault N'dbo.default_false', N'dbo.Tmp_clients.mortgage_problems'
GO
EXECUTE sp_bindefault N'dbo.default_false', N'dbo.Tmp_clients.intake_agreement'
GO
EXECUTE sp_bindefault N'dbo.default_false', N'dbo.Tmp_clients.DoNotDisturb'
GO
ALTER TABLE dbo.Tmp_clients ADD CONSTRAINT
	DF_clients_ElectronicCorrespondence DEFAULT ((2)) FOR ElectronicCorrespondence
GO
ALTER TABLE dbo.Tmp_clients ADD CONSTRAINT
	DF_clients_ElectronicStatements DEFAULT ((2)) FOR ElectronicStatements
GO
EXECUTE sp_bindefault N'dbo.default_zero', N'dbo.Tmp_clients.bankruptcy_class'
GO
EXECUTE sp_bindefault N'dbo.default_money', N'dbo.Tmp_clients.fed_tax_owed'
GO
EXECUTE sp_bindefault N'dbo.default_money', N'dbo.Tmp_clients.state_tax_owed'
GO
EXECUTE sp_bindefault N'dbo.default_money', N'dbo.Tmp_clients.local_tax_owed'
GO
EXECUTE sp_bindefault N'dbo.default_zero', N'dbo.Tmp_clients.fed_tax_months'
GO
EXECUTE sp_bindefault N'dbo.default_zero', N'dbo.Tmp_clients.state_tax_months'
GO
EXECUTE sp_bindefault N'dbo.default_zero', N'dbo.Tmp_clients.local_tax_months'
GO
EXECUTE sp_bindefault N'dbo.default_money', N'dbo.Tmp_clients.held_in_trust'
GO
EXECUTE sp_bindefault N'dbo.default_money', N'dbo.Tmp_clients.deposit_in_trust'
GO
EXECUTE sp_bindefault N'dbo.default_money', N'dbo.Tmp_clients.reserved_in_trust'
GO
EXECUTE sp_bindefault N'dbo.default_zero', N'dbo.Tmp_clients.dependents'
GO
EXECUTE sp_bindefault N'dbo.default_one', N'dbo.Tmp_clients.household'
GO
EXECUTE sp_bindefault N'dbo.default_one', N'dbo.Tmp_clients.method_first_contact'
GO
EXECUTE sp_bindefault N'dbo.default_one', N'dbo.Tmp_clients.preferred_contact'
GO
EXECUTE sp_bindefault N'dbo.default_zero', N'dbo.Tmp_clients.satisfaction_score'
GO
EXECUTE sp_bindefault N'dbo.default_zero', N'dbo.Tmp_clients.program_months'
GO
EXECUTE sp_bindefault N'dbo.default_money', N'dbo.Tmp_clients.payout_total_debt'
GO
EXECUTE sp_bindefault N'dbo.default_zero', N'dbo.Tmp_clients.payout_months_to_payout'
GO
EXECUTE sp_bindefault N'dbo.default_money', N'dbo.Tmp_clients.payout_total_fees'
GO
EXECUTE sp_bindefault N'dbo.default_money', N'dbo.Tmp_clients.payout_total_interest'
GO
EXECUTE sp_bindefault N'dbo.default_money', N'dbo.Tmp_clients.payout_total_payments'
GO
EXECUTE sp_bindefault N'dbo.default_money', N'dbo.Tmp_clients.payout_deposit_amount'
GO
EXECUTE sp_bindefault N'dbo.default_money', N'dbo.Tmp_clients.payout_monthly_fee'
GO
EXECUTE sp_bindefault N'dbo.default_money', N'dbo.Tmp_clients.payout_cushion_amount'
GO
EXECUTE sp_bindefault N'dbo.default_one', N'dbo.Tmp_clients.initial_program'
GO
EXECUTE sp_bindefault N'dbo.default_one', N'dbo.Tmp_clients.current_program'
GO
EXECUTE sp_bindefault N'dbo.default_one', N'dbo.Tmp_clients.people'
GO
SET IDENTITY_INSERT dbo.Tmp_clients ON
GO
IF EXISTS(SELECT * FROM dbo.clients)
	 EXEC('INSERT INTO dbo.Tmp_clients (client, client_guid, salutation, AddressID, HomeTelephoneID, MsgTelephoneID, county, region, active_status, active_status_date, dmp_status_date, client_status, client_status_date, disbursement_date, mail_error_date, start_date, restart_date, drop_date, drop_reason, drop_reason_other, referred_by, cause_fin_problem1, cause_fin_problem2, cause_fin_problem3, cause_fin_problem4, office, counselor, csr, hold_disbursements, personal_checks, ach_active, stack_proration, mortgage_problems, intake_agreement, DoNotDisturb, ElectronicCorrespondence, ElectronicStatements, bankruptcy_class, fed_tax_owed, state_tax_owed, local_tax_owed, fed_tax_months, state_tax_months, local_tax_months, held_in_trust, deposit_in_trust, reserved_in_trust, reserved_in_trust_cutoff, marital_status, language, dependents, household, method_first_contact, preferred_contact, satisfaction_score, config_fee, first_appt, first_kept_appt, first_resched_appt, program_months, first_deposit_date, last_deposit_date, last_deposit_amount, InboundTelephoneGroupID, InboundTelephoneNumberID, payout_total_debt, payout_months_to_payout, payout_total_fees, payout_total_interest, payout_total_payments, payout_deposit_amount, payout_monthly_fee, payout_cushion_amount, payout_termination_date, initial_program, current_program, created_by, date_created, housing_status, housing_type, people)
		SELECT client, client_guid, salutation, AddressID, HomeTelephoneID, MsgTelephoneID, county, region, active_status, active_status_date, dmp_status_date, client_status, client_status_date, disbursement_date, mail_error_date, start_date, restart_date, drop_date, drop_reason, drop_reason_other, referred_by, cause_fin_problem1, cause_fin_problem2, cause_fin_problem3, cause_fin_problem4, office, counselor, csr, hold_disbursements, personal_checks, ach_active, stack_proration, mortgage_problems, intake_agreement, isnull(DoNotDisturb,0), ElectronicCorrespondence, ElectronicStatements, bankruptcy_class, fed_tax_owed, state_tax_owed, local_tax_owed, fed_tax_months, state_tax_months, local_tax_months, held_in_trust, deposit_in_trust, reserved_in_trust, reserved_in_trust_cutoff, marital_status, language, dependents, household, method_first_contact, preferred_contact, satisfaction_score, config_fee, first_appt, first_kept_appt, first_resched_appt, program_months, first_deposit_date, last_deposit_date, last_deposit_amount, InboundTelephoneGroupID, InboundTelephoneNumberID, payout_total_debt, payout_months_to_payout, payout_total_fees, payout_total_interest, payout_total_payments, payout_deposit_amount, payout_monthly_fee, payout_cushion_amount, payout_termination_date, initial_program, current_program, created_by, date_created, housing_status, housing_type, people FROM dbo.clients WITH (HOLDLOCK TABLOCKX)')
GO
SET IDENTITY_INSERT dbo.Tmp_clients OFF
GO
DROP TABLE dbo.clients
GO
EXECUTE sp_rename N'dbo.Tmp_clients', N'clients', 'OBJECT' 
GO
ALTER TABLE dbo.clients ADD CONSTRAINT
	PK_clients PRIMARY KEY CLUSTERED 
	(
	client
	) WITH( STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX IX_clients_1 ON dbo.clients
	(
	active_status
	) WITH( STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX IX_clients_2 ON dbo.clients
	(
	HomeTelephoneID
	) INCLUDE (client) 
 WITH( STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX IX_clients_3 ON dbo.clients
	(
	MsgTelephoneID
	) INCLUDE (client) 
 WITH( STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
ALTER TABLE dbo.clients ADD CONSTRAINT
	FK_clients_counselors FOREIGN KEY
	(
	counselor
	) REFERENCES dbo.counselors
	(
	Counselor
	) ON UPDATE  NO ACTION 
	 ON DELETE  NO ACTION 
GO
ALTER TABLE dbo.clients ADD CONSTRAINT
	FK_clients_csrs FOREIGN KEY
	(
	csr
	) REFERENCES dbo.counselors
	(
	Counselor
	) ON UPDATE  NO ACTION 
	 ON DELETE  NO ACTION 
GO
ALTER TABLE dbo.clients ADD CONSTRAINT
	FK_clients_config_fees FOREIGN KEY
	(
	config_fee
	) REFERENCES dbo.config_fees
	(
	config_fee
	) ON UPDATE  NO ACTION 
	 ON DELETE  NO ACTION 
GO
DECLARE @v sql_variant 
SET @v = N'Do not allow the config_fee entry to be deleted if it is still referenced'
EXECUTE sp_addextendedproperty N'MS_Description', @v, N'SCHEMA', N'dbo', N'TABLE', N'clients', N'CONSTRAINT', N'FK_clients_config_fees'
GO
ALTER TABLE dbo.clients ADD CONSTRAINT
	FK_clients_Counties FOREIGN KEY
	(
	county
	) REFERENCES dbo.counties
	(
	county
	) ON UPDATE  CASCADE 
	 ON DELETE  NO ACTION 
GO
ALTER TABLE dbo.clients ADD CONSTRAINT
	FK_clients_offices FOREIGN KEY
	(
	office
	) REFERENCES dbo.offices
	(
	office
	) ON UPDATE  NO ACTION 
	 ON DELETE  NO ACTION 
GO
ALTER TABLE dbo.clients ADD CONSTRAINT
	FK_clients_Regions FOREIGN KEY
	(
	region
	) REFERENCES dbo.regions
	(
	region
	) ON UPDATE  CASCADE 
	 ON DELETE  NO ACTION 
GO
ALTER TABLE dbo.clients ADD CONSTRAINT
	FK_clients_drop_reasons FOREIGN KEY
	(
	drop_reason
	) REFERENCES dbo.drop_reasons
	(
	drop_reason
	) ON UPDATE  CASCADE 
	 ON DELETE  SET NULL 
GO
ALTER TABLE dbo.clients ADD CONSTRAINT
	FK_clients_referred_by FOREIGN KEY
	(
	referred_by
	) REFERENCES dbo.referred_by
	(
	referred_by
	) ON UPDATE  NO ACTION 
	 ON DELETE  NO ACTION 
GO
ALTER TABLE dbo.clients ADD CONSTRAINT
	FK_clients_financial_problems1 FOREIGN KEY
	(
	cause_fin_problem1
	) REFERENCES dbo.financial_problems
	(
	financial_problem
	) ON UPDATE  NO ACTION 
	 ON DELETE  NO ACTION 
GO
ALTER TABLE dbo.clients ADD CONSTRAINT
	FK_clients_financial_problems2 FOREIGN KEY
	(
	cause_fin_problem2
	) REFERENCES dbo.financial_problems
	(
	financial_problem
	) ON UPDATE  NO ACTION 
	 ON DELETE  NO ACTION 
GO
ALTER TABLE dbo.clients ADD CONSTRAINT
	FK_clients_financial_problems3 FOREIGN KEY
	(
	cause_fin_problem3
	) REFERENCES dbo.financial_problems
	(
	financial_problem
	) ON UPDATE  NO ACTION 
	 ON DELETE  NO ACTION 
GO
ALTER TABLE dbo.clients ADD CONSTRAINT
	FK_clients_financial_problems4 FOREIGN KEY
	(
	cause_fin_problem4
	) REFERENCES dbo.financial_problems
	(
	financial_problem
	) ON UPDATE  NO ACTION 
	 ON DELETE  NO ACTION 
GO
ALTER TABLE dbo.clients ADD CONSTRAINT
	FK_clients_InBoundTelephoneNumberGroups FOREIGN KEY
	(
	InboundTelephoneGroupID
	) REFERENCES dbo.InBoundTelephoneNumberGroups
	(
	oID
	) ON UPDATE  CASCADE 
	 ON DELETE  SET NULL 
GO
ALTER TABLE dbo.clients ADD CONSTRAINT
	FK_clients_InboundTelephoneNumbers FOREIGN KEY
	(
	InboundTelephoneNumberID
	) REFERENCES dbo.InboundTelephoneNumbers
	(
	oID
	) ON UPDATE  NO ACTION 
	 ON DELETE  NO ACTION 
GO
EXECUTE sp_bindrule N'dbo.valid_day', N'dbo.clients.disbursement_date'
GO
CREATE TRIGGER [dbo].[trig_clients_d] on dbo.clients for delete as
-- ================================================================
-- ==           Propigate the delete for the clients             ==
-- ================================================================

set nocount on

-- ach onetime pull
delete		ach_onetimes
from		ach_onetimes i
inner join	deleted d on i.client = d.client

-- action plans
delete		action_plans
from		action_plans i
inner join	deleted d on i.client = d.client

-- budgets
delete		budgets
from		budgets i
inner join	deleted d on i.client = d.client

-- Other budget categories
delete		budget_categories_other
from		budget_categories_other i
inner join	deleted d on i.client = d.client

-- ach
delete		client_ach
from		client_ach i
inner join	deleted d on i.client = d.client

-- additional keys
delete		client_addkeys
from		client_addkeys i
inner join	deleted d on i.client = d.client

-- appointments
delete		client_appointments
from		client_appointments i
inner join	deleted d on i.client = d.client

-- notes
delete		client_notes
from		client_notes i
inner join	deleted d on i.client = d.client

delete		client_notes
from		client_notes b
inner join	client_creditor i on b.client_creditor = i.client_creditor
inner join	deleted d on i.client = d.client

-- client_statement_clients
delete		client_statement_clients
from		client_statement_clients i
inner join	deleted d on i.client = d.client

-- deposits
delete		client_deposits
from		client_deposits i
inner join	deleted d on i.client = d.client

-- housing
delete		client_housing
from		client_housing i
inner join	deleted d on i.client = d.client

-- indicators
delete		client_indicators
from		client_indicators i
inner join	deleted d on i.client = d.client

-- other debts
delete		client_other_debts
from		client_other_debts i
inner join	deleted d on i.client = d.client

-- retention_events
delete		client_retention_events
from		client_retention_events i
inner join	deleted d on i.client = d.client

-- debt_notes
delete		debt_notes
from		debt_notes i
inner join	deleted d on i.client = d.client

-- deposit_batch_details
delete		deposit_batch_details
from		deposit_batch_details i
inner join	deleted d on i.client = d.client

-- disbursement_clients
delete		disbursement_clients
from		disbursement_clients i
inner join	deleted d on i.client = d.client

-- disbursement_creditor_notes
delete		disbursement_creditor_notes
from		disbursement_creditor_notes i
inner join	deleted d on i.client = d.client

-- disbursement_notes
delete		disbursement_notes
from		disbursement_notes i
inner join	deleted d on i.client = d.client

-- faq_details
delete		faq_details
from		faq_details i
inner join	deleted d on i.client = d.client

-- hud_interviews
delete		hud_interviews
from		hud_interviews i
inner join	deleted d on i.client = d.client

-- intake_clients
update		intake_clients
set			client = null, date_imported = null
from		intake_clients i
inner join	deleted d on i.client = d.client

-- letter_queue
delete		letter_queue
from		letter_queue i
inner join	deleted d on i.client = d.client

-- people
delete		people
from		people i
inner join	deleted d on i.client = d.client

-- registers_client
delete		registers_client
from		registers_client i
inner join	deleted d on i.client = d.client

-- registers_non_ar
delete		registers_non_ar
from		registers_non_ar i
inner join	deleted d on i.client = d.client

-- registers_trust
delete		registers_trust
from		registers_trust i
inner join	deleted d on i.client = d.client
where		i.tran_type in ('CR','AR')

-- sales_files
delete		sales_files
from		sales_files i
inner join	deleted d on i.client = d.client

-- ticklers
delete		ticklers
from		ticklers i
inner join	deleted d on i.client = d.client

-- balance information
delete		client_creditor_balances
from		client_creditor_balances b
inner join	client_creditor i on b.client_creditor_balance = i.client_creditor_balance
inner join	deleted d on i.client = d.client
GO
COMMIT
GO
BEGIN TRANSACTION
EXEC ('DENY INSERT,SELECT,UPDATE,DELETE ON dbo.clients TO www_role  AS dbo')
COMMIT
GO
BEGIN TRANSACTION
GO
ALTER TABLE dbo.client_statement_notes ADD CONSTRAINT
	FK_client_statement_notes_clients FOREIGN KEY
	(
	client
	) REFERENCES dbo.clients
	(
	client
	) ON UPDATE  CASCADE 
	 ON DELETE  CASCADE 
GO
COMMIT
BEGIN TRANSACTION
GO
ALTER TABLE dbo.budget_categories_other ADD CONSTRAINT
	FK_budget_categories_other_clients FOREIGN KEY
	(
	client
	) REFERENCES dbo.clients
	(
	client
	) ON UPDATE  NO ACTION 
	 ON DELETE  CASCADE 
GO
COMMIT
BEGIN TRANSACTION
GO
ALTER TABLE dbo.client_statement_clients ADD CONSTRAINT
	FK_client_statement_clients_clients FOREIGN KEY
	(
	client
	) REFERENCES dbo.clients
	(
	client
	) ON UPDATE  CASCADE 
	 ON DELETE  CASCADE 
GO
COMMIT
BEGIN TRANSACTION
GO
ALTER TABLE dbo.budgets ADD CONSTRAINT
	FK_budgets_clients FOREIGN KEY
	(
	client
	) REFERENCES dbo.clients
	(
	client
	) ON UPDATE  NO ACTION 
	 ON DELETE  CASCADE 
GO
COMMIT
BEGIN TRANSACTION
GO
ALTER TABLE dbo.assets ADD CONSTRAINT
	FK_assets_clients FOREIGN KEY
	(
	client
	) REFERENCES dbo.clients
	(
	client
	) ON UPDATE  CASCADE 
	 ON DELETE  CASCADE 
GO
COMMIT
BEGIN TRANSACTION
GO
ALTER TABLE dbo.OCS_Client ADD CONSTRAINT
	FK_OCS_Client_clients FOREIGN KEY
	(
	ClientId
	) REFERENCES dbo.clients
	(
	client
	) ON UPDATE  NO ACTION 
	 ON DELETE  CASCADE 
GO
COMMIT
BEGIN TRANSACTION
GO
ALTER TABLE dbo.client_disclosures ADD CONSTRAINT
	FK_client_disclosures_clients FOREIGN KEY
	(
	client
	) REFERENCES dbo.clients
	(
	client
	) ON UPDATE  NO ACTION 
	 ON DELETE  CASCADE 
GO
COMMIT
BEGIN TRANSACTION
GO
ALTER TABLE dbo.OCS_ContactAttempt ADD CONSTRAINT
	FK_OCS_ContactAttempt_clients FOREIGN KEY
	(
	ClientId
	) REFERENCES dbo.clients
	(
	client
	) ON UPDATE  NO ACTION 
	 ON DELETE  CASCADE 
GO
COMMIT
BEGIN TRANSACTION
GO
ALTER TABLE dbo.ach_onetimes ADD CONSTRAINT
	FK_ach_onetimes_clients FOREIGN KEY
	(
	client
	) REFERENCES dbo.clients
	(
	client
	) ON UPDATE  NO ACTION 
	 ON DELETE  CASCADE 
GO
COMMIT
BEGIN TRANSACTION
GO
ALTER TABLE dbo.client_products ADD CONSTRAINT
	FK_client_products_clients FOREIGN KEY
	(
	client
	) REFERENCES dbo.clients
	(
	client
	) ON UPDATE  CASCADE 
	 ON DELETE  CASCADE 
GO
COMMIT
GO
