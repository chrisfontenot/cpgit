SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER TRIGGER [trig_HPFEvents_U] ON [HPFEvents] AFTER UPDATE AS
BEGIN
	set nocount on

	-- set the updated status appropriately
	if not update(date_changed) and not update(changed_by)
	begin
		update	HPFEvents
		set		date_changed = getdate(), changed_by = suser_sname()
		FROM	HPFEvents e
		inner join inserted i on e.oID = i.oID

		return
	end

	-- set the updated status appropriately
	if not update(date_changed)
	begin
		update	HPFEvents
		set		date_changed = getdate()
		FROM	HPFEvents e
		inner join inserted i on e.oID = i.oID

		return
	end

	-- set the updated status appropriately
	if not update(changed_by)
	begin
		update	HPFEvents
		set		changed_by = suser_sname()
		FROM	HPFEvents e
		inner join inserted i on e.oID = i.oID

		return
	end
end
GO
