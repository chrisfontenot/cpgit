USE [DebtPlus]
GO

------------------------------------
-- Job Description Table
------------------------------------

update job_descriptions
   set aarp = 'Employed FT'
 where description = 'FULL-TIME'

update job_descriptions
   set aarp = 'Employed PT'
 where description = 'PART-TIME'

update job_descriptions
   set aarp = 'Retired'
 where description = 'RETIRED'

update job_descriptions
   set aarp = 'Unemployed'
 where description = 'UNEMPLOYED'

INSERT INTO [dbo].[job_descriptions]([description],[nfcc],[date_created],[created_by],[aarp])
     VALUES ('Retired - Seeking Work',11,getdate(),'sa','Retired - Seeking Work')

INSERT INTO [dbo].[job_descriptions]([description],[nfcc],[date_created],[created_by],[aarp])
     VALUES ('Social Security',11,getdate(),'sa','Social Security')

INSERT INTO [dbo].[job_descriptions]([description],[nfcc],[date_created],[created_by],[aarp])
     VALUES ('Social Security - Disability',11,getdate(),'sa','Social Security - Disability')
GO

