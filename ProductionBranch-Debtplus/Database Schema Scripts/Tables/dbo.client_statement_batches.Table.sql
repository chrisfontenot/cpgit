USE [DebtPlus]
GO
/****** Object:  Table [dbo].[client_statement_batches]    Script Date: 09/15/2014 13:13:26 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[client_statement_batches](
	[client_statement_batch] [dbo].[typ_key] IDENTITY(1,1) NOT NULL,
	[type] [varchar](4) NOT NULL,
	[disbursement_date] [int] NOT NULL,
	[note] [varchar](4000) NULL,
	[statement_date] [datetime] NOT NULL,
	[period_start] [datetime] NOT NULL,
	[period_end] [datetime] NOT NULL,
	[quarter_1_batch] [dbo].[typ_key] NULL,
	[quarter_2_batch] [dbo].[typ_key] NULL,
	[quarter_3_batch] [dbo].[typ_key] NULL,
	[date_printed] [datetime] NULL,
	[printed_by] [varchar](80) NULL,
	[date_created] [dbo].[typ_date] NOT NULL,
	[created_by] [dbo].[typ_counselor] NOT NULL,
	[old_client_statement_batch] [int] NULL,
 CONSTRAINT [PK_client_statement_batches] PRIMARY KEY CLUSTERED 
(
	[client_statement_batch] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 90) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
ALTER TABLE [dbo].[client_statement_batches] ADD  CONSTRAINT [DF_client_statement_batches_type]  DEFAULT ('M') FOR [type]
GO
