USE [DebtPlus]
GO
/****** Object:  Table [dbo].[DocumentComponents]    Script Date: 09/15/2014 13:13:26 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[DocumentComponents](
	[oID] [dbo].[typ_key] IDENTITY(1,1) NOT NULL,
	[DocumentID] [dbo].[typ_key] NOT NULL,
	[SequenceID] [int] NOT NULL,
	[EffectiveStartDate] [datetime] NULL,
	[EffectiveExpireDate] [datetime] NULL,
	[PageEjectBeforeFLG] [bit] NOT NULL,
	[SectionID] [varchar](50) NOT NULL,
	[ComponentType] [varchar](50) NULL,
	[ComponentLocationURI] [varchar](512) NULL,
	[RevisionDate] [datetime] NOT NULL,
 CONSTRAINT [PK_DocumentComponents] PRIMARY KEY CLUSTERED 
(
	[oID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
ALTER TABLE [dbo].[DocumentComponents] ADD  CONSTRAINT [DF_DocumentComponents_SequenceID]  DEFAULT ((0)) FOR [SequenceID]
GO
ALTER TABLE [dbo].[DocumentComponents] ADD  CONSTRAINT [DF_DocumentComponents_SectionID]  DEFAULT ('detail') FOR [SectionID]
GO
ALTER TABLE [dbo].[DocumentComponents] ADD  CONSTRAINT [DF_DocumentComponents_RevisionDate]  DEFAULT (getdate()) FOR [RevisionDate]
GO
