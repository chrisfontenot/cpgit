/* To prevent any potential data loss issues, you should review this script in detail before running it outside the context of the database designer.*/
BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
execute UPDATE_ADD_COLUMN 'Housing_MortgageTypes', 'hpf'
GO
execute UPDATE_DROP_CONSTRAINTS 'Housing_MortgageTypes'
GO
BEGIN TRANSACTION
GO
CREATE TABLE dbo.Tmp_Housing_MortgageTypes
	(
	oID dbo.typ_key NOT NULL IDENTITY (1, 1),
	description dbo.typ_description NOT NULL,
	hud_9902_section varchar(256) NULL,
	mortgageCD dbo.typ_description NOT NULL,
	termLengthCD dbo.typ_description NOT NULL,
	[default] bit NOT NULL,
	ActiveFlag bit NOT NULL,
	nfcc varchar(4) NULL,
	rpps varchar(4) NULL,
	hpf varchar(50) NULL,
	note dbo.typ_message NULL,
	epay varchar(4) NULL,
	date_created dbo.typ_date NOT NULL,
	created_by dbo.typ_counselor NOT NULL,
	ts timestamp NOT NULL
	)  ON [PRIMARY]
GO
GRANT DELETE ON dbo.Tmp_Housing_MortgageTypes TO public  AS dbo
GO
GRANT INSERT ON dbo.Tmp_Housing_MortgageTypes TO public  AS dbo
GO
GRANT SELECT ON dbo.Tmp_Housing_MortgageTypes TO public  AS dbo
GO
GRANT UPDATE ON dbo.Tmp_Housing_MortgageTypes TO public  AS dbo
GO
ALTER TABLE dbo.Tmp_Housing_MortgageTypes ADD CONSTRAINT
	DF_Housing_MortgageTypes_mortgageType DEFAULT ('UNK') FOR mortgageCD
GO
ALTER TABLE dbo.Tmp_Housing_MortgageTypes ADD CONSTRAINT
	DF_Housing_MortgageTypes_termLengthType DEFAULT ('UNK') FOR termLengthCD
GO
EXECUTE sp_bindefault N'dbo.default_false', N'dbo.Tmp_Housing_MortgageTypes.[default]'
GO
EXECUTE sp_bindefault N'dbo.default_true', N'dbo.Tmp_Housing_MortgageTypes.ActiveFlag'
GO
SET IDENTITY_INSERT dbo.Tmp_Housing_MortgageTypes ON
GO
IF EXISTS(SELECT * FROM dbo.Housing_MortgageTypes)
	 EXEC('INSERT INTO dbo.Tmp_Housing_MortgageTypes (oID, description, hud_9902_section, mortgageCD, termLengthCD, [default], ActiveFlag, date_created, created_by, nfcc, rpps, epay, note, hpf)
		SELECT oID, description, hud_9902_section, mortgageCD, termLengthCD, [default], ActiveFlag, date_created, created_by, nfcc, rpps, epay, note, hpf FROM dbo.Housing_MortgageTypes WITH (HOLDLOCK TABLOCKX)')
GO
SET IDENTITY_INSERT dbo.Tmp_Housing_MortgageTypes OFF
GO
DROP TABLE dbo.Housing_MortgageTypes
GO
EXECUTE sp_rename N'dbo.Tmp_Housing_MortgageTypes', N'Housing_MortgageTypes', 'OBJECT' 
GO
ALTER TABLE dbo.Housing_MortgageTypes ADD CONSTRAINT
	PK_HousingMortgageTypes PRIMARY KEY CLUSTERED 
	(
	oID
	) WITH( STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
DENY DELETE ON dbo.Housing_MortgageTypes TO www_role  AS dbo 
GO
DENY INSERT ON dbo.Housing_MortgageTypes TO www_role  AS dbo 
GO
DENY SELECT ON dbo.Housing_MortgageTypes TO www_role  AS dbo 
GO
DENY UPDATE ON dbo.Housing_MortgageTypes TO www_role  AS dbo 
GO
COMMIT
