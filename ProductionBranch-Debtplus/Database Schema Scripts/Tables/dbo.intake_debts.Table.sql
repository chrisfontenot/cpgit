USE [DebtPlus]
GO
/****** Object:  Table [dbo].[intake_debts]    Script Date: 09/15/2014 13:13:26 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[intake_debts](
	[intake_debt] [dbo].[typ_key] IDENTITY(1,1) NOT NULL,
	[intake_client] [dbo].[typ_key] NOT NULL,
	[creditor_name] [varchar](80) NOT NULL,
	[account_number] [dbo].[typ_client_account] NULL,
	[non_dmp_payment] [money] NULL,
	[non_dmp_interest] [dbo].[typ_fairshare_rate] NULL,
	[balance] [money] NOT NULL,
	[months_delinquent] [smallint] NOT NULL,
	[date_created] [dbo].[typ_date] NOT NULL,
	[created_by] [dbo].[typ_counselor] NOT NULL,
 CONSTRAINT [PK_intake_debts] PRIMARY KEY CLUSTERED 
(
	[intake_debt] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 90) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
