EXECUTE UPDATE_DROP_CONSTRAINTS 'deposit_batch_ids'
GO
/* To prevent any potential data loss issues, you should review this script in detail before running it outside the context of the database designer.*/
BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION
GO
CREATE TABLE dbo.Tmp_deposit_batch_ids
	(
	deposit_batch_id dbo.typ_key NOT NULL IDENTITY (1, 1) NOT FOR REPLICATION,
	batch_type varchar(2) NOT NULL,
	bank dbo.typ_key NOT NULL,
	trust_register dbo.typ_key NULL,
	ach_pull_date datetime NULL,
	ach_settlement_date varchar(3) NULL,
	ach_effective_date datetime NULL,
	ach_file varchar(4096) NULL,
	note dbo.typ_description NULL,
	date_closed datetime NULL,
	date_posted datetime NULL,
	posted_by varchar(80) NULL,
	date_created datetime NOT NULL,
	created_by dbo.typ_counselor NOT NULL,
	ts timestamp NOT NULL
	)  ON [PRIMARY]
GO
ALTER TABLE dbo.Tmp_deposit_batch_ids SET (LOCK_ESCALATION = TABLE)
GO
GRANT DELETE ON dbo.Tmp_deposit_batch_ids TO public  AS dbo
GO
GRANT INSERT ON dbo.Tmp_deposit_batch_ids TO public  AS dbo
GO
GRANT SELECT ON dbo.Tmp_deposit_batch_ids TO public  AS dbo
GO
GRANT UPDATE ON dbo.Tmp_deposit_batch_ids TO public  AS dbo
GO
DECLARE @v sql_variant 
SET @v = N'For each deposit batch, an id is generated from this table. This table is referenced by the deposit_batch_details table that holds the details of the deposit batch.'
EXECUTE sp_addextendedproperty N'MS_Description', @v, N'SCHEMA', N'dbo', N'TABLE', N'Tmp_deposit_batch_ids', NULL, NULL
GO
DECLARE @v sql_variant 
SET @v = N'Deposit batch ID. This is the primary key.'
EXECUTE sp_addextendedproperty N'MS_Description', @v, N'SCHEMA', N'dbo', N'TABLE', N'Tmp_deposit_batch_ids', N'COLUMN', N'deposit_batch_id'
GO
DECLARE @v sql_variant 
SET @v = N'Type of the deposit batch. ''CL'' is a client deposit batch. ''CR'' is a creditor refund batch. ''AC'' is ACH (a flavor of DP).'
EXECUTE sp_addextendedproperty N'MS_Description', @v, N'SCHEMA', N'dbo', N'TABLE', N'Tmp_deposit_batch_ids', N'COLUMN', N'batch_type'
GO
DECLARE @v sql_variant 
SET @v = N'Pointer to the bank table to receive the deposit.'
EXECUTE sp_addextendedproperty N'MS_Description', @v, N'SCHEMA', N'dbo', N'TABLE', N'Tmp_deposit_batch_ids', N'COLUMN', N'bank'
GO
DECLARE @v sql_variant 
SET @v = N'When the batch is posted to the trust, this is the trust register ID.'
EXECUTE sp_addextendedproperty N'MS_Description', @v, N'SCHEMA', N'dbo', N'TABLE', N'Tmp_deposit_batch_ids', N'COLUMN', N'trust_register'
GO
DECLARE @v sql_variant 
SET @v = N'For ACH, this is the pull date for the clients. It must match the deposit dates in the client_deposits table.'
EXECUTE sp_addextendedproperty N'MS_Description', @v, N'SCHEMA', N'dbo', N'TABLE', N'Tmp_deposit_batch_ids', N'COLUMN', N'ach_pull_date'
GO
DECLARE @v sql_variant 
SET @v = N'For ACH, this is the bank''s settlement date.'
EXECUTE sp_addextendedproperty N'MS_Description', @v, N'SCHEMA', N'dbo', N'TABLE', N'Tmp_deposit_batch_ids', N'COLUMN', N'ach_settlement_date'
GO
DECLARE @v sql_variant 
SET @v = N'For ACH, this is the effective date of the money transfer. This is a required field for ACH transactions.'
EXECUTE sp_addextendedproperty N'MS_Description', @v, N'SCHEMA', N'dbo', N'TABLE', N'Tmp_deposit_batch_ids', N'COLUMN', N'ach_effective_date'
GO
DECLARE @v sql_variant 
SET @v = N'For ACH, we record the filename on the local disk system for the ACH file.'
EXECUTE sp_addextendedproperty N'MS_Description', @v, N'SCHEMA', N'dbo', N'TABLE', N'Tmp_deposit_batch_ids', N'COLUMN', N'ach_file'
GO
DECLARE @v sql_variant 
SET @v = N'User defined note text'
EXECUTE sp_addextendedproperty N'MS_Description', @v, N'SCHEMA', N'dbo', N'TABLE', N'Tmp_deposit_batch_ids', N'COLUMN', N'note'
GO
DECLARE @v sql_variant 
SET @v = N'Date and time when the batch was closed. A closed batch posts the item to the trust register.'
EXECUTE sp_addextendedproperty N'MS_Description', @v, N'SCHEMA', N'dbo', N'TABLE', N'Tmp_deposit_batch_ids', N'COLUMN', N'date_closed'
GO
DECLARE @v sql_variant 
SET @v = N'Date and time when the deposits are actually posted to the client trust account.'
EXECUTE sp_addextendedproperty N'MS_Description', @v, N'SCHEMA', N'dbo', N'TABLE', N'Tmp_deposit_batch_ids', N'COLUMN', N'date_posted'
GO
DECLARE @v sql_variant 
SET @v = N'Name of the person who posted file batch'
EXECUTE sp_addextendedproperty N'MS_Description', @v, N'SCHEMA', N'dbo', N'TABLE', N'Tmp_deposit_batch_ids', N'COLUMN', N'posted_by'
GO
DECLARE @v sql_variant 
SET @v = N'Date/Time when the row was created.'
EXECUTE sp_addextendedproperty N'MS_Description', @v, N'SCHEMA', N'dbo', N'TABLE', N'Tmp_deposit_batch_ids', N'COLUMN', N'date_created'
GO
DECLARE @v sql_variant 
SET @v = N'Person who created the row.'
EXECUTE sp_addextendedproperty N'MS_Description', @v, N'SCHEMA', N'dbo', N'TABLE', N'Tmp_deposit_batch_ids', N'COLUMN', N'created_by'
GO
ALTER TABLE dbo.Tmp_deposit_batch_ids ADD CONSTRAINT
	DF_deposit_batch_ids_batch_type DEFAULT ('CL') FOR batch_type
GO
ALTER TABLE dbo.Tmp_deposit_batch_ids ADD CONSTRAINT
	DF_deposit_batch_ids_bank DEFAULT (1) FOR bank
GO
EXECUTE sp_bindefault N'dbo.default_date', N'dbo.Tmp_deposit_batch_ids.date_created'
GO
SET IDENTITY_INSERT dbo.Tmp_deposit_batch_ids ON
GO
IF EXISTS(SELECT * FROM dbo.deposit_batch_ids)
	 EXEC('INSERT INTO dbo.Tmp_deposit_batch_ids (deposit_batch_id, batch_type, bank, trust_register, ach_pull_date, ach_settlement_date, ach_effective_date, ach_file, note, date_closed, date_posted, posted_by, date_created, created_by)
		SELECT deposit_batch_id, batch_type, bank, trust_register, ach_pull_date, ach_settlement_date, ach_effective_date, ach_file, note, date_closed, date_posted, posted_by, date_created, created_by FROM dbo.deposit_batch_ids WITH (HOLDLOCK TABLOCKX)')
GO
SET IDENTITY_INSERT dbo.Tmp_deposit_batch_ids OFF
GO
DROP TABLE dbo.deposit_batch_ids
GO
EXECUTE sp_rename N'dbo.Tmp_deposit_batch_ids', N'deposit_batch_ids', 'OBJECT' 
GO
ALTER TABLE dbo.deposit_batch_ids ADD CONSTRAINT
	PK_deposit_batch_ids PRIMARY KEY CLUSTERED 
	(
	deposit_batch_id
	) WITH( PAD_INDEX = OFF, FILLFACTOR = 90, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX IX_deposit_batch_ids_1 ON dbo.deposit_batch_ids
	(
	note
	) WITH( PAD_INDEX = OFF, FILLFACTOR = 90, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
DENY DELETE ON dbo.deposit_batch_ids TO www_role  AS dbo 
GO
DENY INSERT ON dbo.deposit_batch_ids TO www_role  AS dbo 
GO
DENY SELECT ON dbo.deposit_batch_ids TO www_role  AS dbo 
GO
DENY UPDATE ON dbo.deposit_batch_ids TO www_role  AS dbo 
GO
COMMIT
BEGIN TRANSACTION
GO
ALTER TABLE dbo.deposit_batch_details ADD CONSTRAINT
	FK_deposit_batch_details_deposit_batch_ids FOREIGN KEY
	(
	deposit_batch_id
	) REFERENCES dbo.deposit_batch_ids
	(
	deposit_batch_id
	) ON UPDATE  CASCADE 
	 ON DELETE  CASCADE 
GO
ALTER TABLE dbo.deposit_batch_details SET (LOCK_ESCALATION = TABLE)
GO
COMMIT
BEGIN TRANSACTION
GO
ALTER TABLE dbo.ach_onetimes ADD CONSTRAINT
	FK_ach_onetimes_deposit_batch_ids FOREIGN KEY
	(
	deposit_batch_id
	) REFERENCES dbo.deposit_batch_ids
	(
	deposit_batch_id
	) ON UPDATE  CASCADE 
	 ON DELETE  SET NULL 
GO
ALTER TABLE dbo.ach_onetimes SET (LOCK_ESCALATION = TABLE)
GO
COMMIT
