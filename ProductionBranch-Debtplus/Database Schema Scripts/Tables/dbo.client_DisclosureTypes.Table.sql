IF EXISTS(SELECT * FROM SYSOBJECTS WHERE TYPE='U' AND NAME=N'client_DisclosureLanguageTypes')
	EXEC('ALTER TABLE dbo.client_DisclosureLanguageTypes DROP CONSTRAINT FK_client_DisclosureLanguageTypes_client_DisclosureTypes')
GO
/* To prevent any potential data loss issues, you should review this script in detail before running it outside the context of the database designer.*/
BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION
GO
CREATE TABLE dbo.Tmp_client_DisclosureTypes
	(
	oID dbo.typ_key NOT NULL IDENTITY (1, 1),
	description dbo.typ_description NOT NULL,
	[Assembly] varchar(512) NOT NULL,
	ActiveFlag bit NOT NULL,
	[Default] bit NOT NULL,
	applies_applicant bit NOT NULL,
	applies_coapplicant bit NOT NULL,
	required_propertyID bit NOT NULL,
	created_by dbo.typ_counselor NOT NULL,
	date_created dbo.typ_date NOT NULL,
	ts timestamp NOT NULL
	) ON [PRIMARY]
GO
EXECUTE sp_bindefault N'dbo.default_true', N'dbo.Tmp_client_DisclosureTypes.ActiveFlag'
GO
EXECUTE sp_bindefault N'dbo.default_false', N'dbo.Tmp_client_DisclosureTypes.[Default]'
GO
EXECUTE sp_bindefault N'dbo.default_false', N'dbo.Tmp_client_DisclosureTypes.applies_applicant'
GO
EXECUTE sp_bindefault N'dbo.default_false', N'dbo.Tmp_client_DisclosureTypes.applies_coapplicant'
GO
EXECUTE sp_bindefault N'dbo.default_false', N'dbo.Tmp_client_DisclosureTypes.required_propertyID'
GO
SET IDENTITY_INSERT dbo.Tmp_client_DisclosureTypes ON
GO
IF EXISTS(SELECT * FROM sysobjects where type = 'U' and name = N'client_DisclosureTypes')
BEGIN
	EXEC('INSERT INTO dbo.Tmp_client_DisclosureTypes (oID, description, [Assembly], ActiveFlag, [Default], applies_applicant, applies_coapplicant, required_propertyID, created_by, date_created) SELECT oID, description, [Assembly], ActiveFlag, [Default], applies_applicant, applies_coapplicant, required_propertyID, created_by, date_created FROM dbo.client_DisclosureTypes WITH (HOLDLOCK TABLOCKX)')
	EXEC ('DROP TABLE dbo.client_DisclosureTypes')
END
GO
SET IDENTITY_INSERT dbo.Tmp_client_DisclosureTypes OFF
GO
EXECUTE sp_rename N'dbo.Tmp_client_DisclosureTypes', N'client_DisclosureTypes', 'OBJECT' 
GO
ALTER TABLE dbo.client_DisclosureTypes ADD CONSTRAINT
	PK_client_DisclosureTypes PRIMARY KEY CLUSTERED 
	(
	oID
	) ON [PRIMARY]
GO
COMMIT
GO
BEGIN TRANSACTION
GO
ALTER TABLE dbo.client_DisclosureLanguageTypes ADD CONSTRAINT
	FK_client_DisclosureLanguageTypes_client_DisclosureTypes FOREIGN KEY
	(
	clientDisclosureTypeID
	) REFERENCES dbo.client_DisclosureTypes
	(
	oID
	) ON UPDATE  CASCADE 
	 ON DELETE  CASCADE 
GO
COMMIT
