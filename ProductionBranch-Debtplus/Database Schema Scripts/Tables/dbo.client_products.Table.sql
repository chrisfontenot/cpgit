EXECUTE UPDATE_DROP_CONSTRAINTS 'client_products'
GO
/* To prevent any potential data loss issues, you should review this script in detail before running it outside the context of the database designer.*/
BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION
GO
CREATE TABLE dbo.Tmp_client_products
	(
	oID dbo.typ_key NOT NULL IDENTITY (1, 1),
	client dbo.typ_key NOT NULL,
	vendor dbo.typ_key NOT NULL,
	product dbo.typ_key NOT NULL,
	ActiveFlag bit NOT NULL,
	message varchar(50) NULL,
	reference varchar(50) NULL,
	cost money NOT NULL,
	discount money NOT NULL,
	tendered money NOT NULL,
	disputed_amount money NOT NULL,
	disputed_date datetime NULL,
	disputed_status dbo.typ_key NULL,
	disputed_note varchar(2048) NULL,
	resolution_date datetime NULL,
	resolution_status dbo.typ_key NULL,
	resolution_note varchar(2048) NULL,
	invoice_status int NOT NULL,
	invoice_date datetime NULL,
	date_created dbo.typ_date NOT NULL,
	created_by dbo.typ_counselor NOT NULL,
	ts timestamp NOT NULL
	)  ON [PRIMARY]
GO
ALTER TABLE dbo.Tmp_client_products SET (LOCK_ESCALATION = TABLE)
GO
DECLARE @v sql_variant 
SET @v = N'Pointer to the vendor for this item.'
EXECUTE sp_addextendedproperty N'MS_Description', @v, N'SCHEMA', N'dbo', N'TABLE', N'Tmp_client_products', N'COLUMN', N'vendor'
GO
DECLARE @v sql_variant 
SET @v = N'Pointer to the product item.'
EXECUTE sp_addextendedproperty N'MS_Description', @v, N'SCHEMA', N'dbo', N'TABLE', N'Tmp_client_products', N'COLUMN', N'product'
GO
DECLARE @v sql_variant 
SET @v = N'TRUE if the record is currently active.'
EXECUTE sp_addextendedproperty N'MS_Description', @v, N'SCHEMA', N'dbo', N'TABLE', N'Tmp_client_products', N'COLUMN', N'ActiveFlag'
GO
DECLARE @v sql_variant 
SET @v = N'Original cost of the item. This is equivalent to the "original balance".'
EXECUTE sp_addextendedproperty N'MS_Description', @v, N'SCHEMA', N'dbo', N'TABLE', N'Tmp_client_products', N'COLUMN', N'cost'
GO
DECLARE @v sql_variant 
SET @v = N'Dollar amount associated with a discount/adjustment.'
EXECUTE sp_addextendedproperty N'MS_Description', @v, N'SCHEMA', N'dbo', N'TABLE', N'Tmp_client_products', N'COLUMN', N'discount'
GO
DECLARE @v sql_variant 
SET @v = N'Total dollar amount of money paid on this product.'
EXECUTE sp_addextendedproperty N'MS_Description', @v, N'SCHEMA', N'dbo', N'TABLE', N'Tmp_client_products', N'COLUMN', N'tendered'
GO
DECLARE @v sql_variant 
SET @v = N'Dollar amount of the dispute.'
EXECUTE sp_addextendedproperty N'MS_Description', @v, N'SCHEMA', N'dbo', N'TABLE', N'Tmp_client_products', N'COLUMN', N'disputed_amount'
GO
DECLARE @v sql_variant 
SET @v = N'Status for the disputed item. If non-null, the debt is disputed.'
EXECUTE sp_addextendedproperty N'MS_Description', @v, N'SCHEMA', N'dbo', N'TABLE', N'Tmp_client_products', N'COLUMN', N'disputed_status'
GO
DECLARE @v sql_variant 
SET @v = N'Note associated with the dispute.'
EXECUTE sp_addextendedproperty N'MS_Description', @v, N'SCHEMA', N'dbo', N'TABLE', N'Tmp_client_products', N'COLUMN', N'disputed_note'
GO
DECLARE @v sql_variant 
SET @v = N'Note associated with the resolution.'
EXECUTE sp_addextendedproperty N'MS_Description', @v, N'SCHEMA', N'dbo', N'TABLE', N'Tmp_client_products', N'COLUMN', N'resolution_note'
GO
DECLARE @v sql_variant 
SET @v = N'Date when the item was invoiced to the vendor.'
EXECUTE sp_addextendedproperty N'MS_Description', @v, N'SCHEMA', N'dbo', N'TABLE', N'Tmp_client_products', N'COLUMN', N'invoice_date'
GO
DECLARE @v sql_variant 
SET @v = N'Date/Time when the record was created'
EXECUTE sp_addextendedproperty N'MS_Description', @v, N'SCHEMA', N'dbo', N'TABLE', N'Tmp_client_products', N'COLUMN', N'date_created'
GO
DECLARE @v sql_variant 
SET @v = N'ID of the account where the record was created'
EXECUTE sp_addextendedproperty N'MS_Description', @v, N'SCHEMA', N'dbo', N'TABLE', N'Tmp_client_products', N'COLUMN', N'created_by'
GO
EXECUTE sp_bindefault N'dbo.default_true', N'dbo.Tmp_client_products.ActiveFlag'
GO
EXECUTE sp_bindefault N'dbo.default_money', N'dbo.Tmp_client_products.cost'
GO
EXECUTE sp_bindefault N'dbo.default_money', N'dbo.Tmp_client_products.discount'
GO
EXECUTE sp_bindefault N'dbo.default_money', N'dbo.Tmp_client_products.tendered'
GO
EXECUTE sp_bindefault N'dbo.default_money', N'dbo.Tmp_client_products.disputed_amount'
GO
EXECUTE sp_bindefault N'dbo.default_zero', N'dbo.Tmp_client_products.invoice_status'
GO
IF EXISTS(SELECT * FROM SYSOBJECTS WHERE TYPE='U' AND NAME=N'client_products')
BEGIN
	EXEC('SET IDENTITY_INSERT dbo.Tmp_client_products ON;
		  INSERT INTO dbo.Tmp_client_products (oID, client, vendor, product, ActiveFlag, message, reference, cost, discount, tendered, disputed_amount, disputed_date, disputed_status, disputed_note, resolution_date, resolution_status, resolution_note, invoice_status, invoice_date, date_created, created_by)
		  SELECT oID, client, vendor, product, ActiveFlag, message, reference, cost, discount, tendered, disputed_amount, disputed_date, disputed_status, disputed_note, resolution_date, resolution_status, resolution_note, invoice_status, invoice_date, date_created, created_by FROM dbo.client_products WITH (HOLDLOCK TABLOCKX);
		  SET IDENTITY_INSERT dbo.Tmp_client_products OFF;')
	EXEC('DROP TABLE dbo.client_products')
END
GO
EXECUTE sp_rename N'dbo.Tmp_client_products', N'client_products', 'OBJECT' 
GO
ALTER TABLE dbo.client_products ADD CONSTRAINT
	PK_client_products PRIMARY KEY CLUSTERED 
	(
	oID
	) WITH( STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
ALTER TABLE dbo.client_products ADD CONSTRAINT
	FK_client_products_disputed_status_types FOREIGN KEY
	(
	disputed_status
	) REFERENCES dbo.disputed_status_types
	(
	oID
	) ON UPDATE  NO ACTION 
	 ON DELETE  NO ACTION 
GO
ALTER TABLE dbo.client_products ADD CONSTRAINT
	FK_client_products_product_resolution_types FOREIGN KEY
	(
	resolution_status
	) REFERENCES dbo.product_resolution_types
	(
	oID
	) ON UPDATE  NO ACTION 
	 ON DELETE  NO ACTION 
GO
ALTER TABLE dbo.client_products ADD CONSTRAINT
	FK_client_products_clients FOREIGN KEY
	(
	client
	) REFERENCES dbo.clients
	(
	client
	) ON UPDATE  CASCADE 
	 ON DELETE  CASCADE 
GO
ALTER TABLE dbo.client_products ADD CONSTRAINT
	FK_client_products_vendors FOREIGN KEY
	(
	vendor
	) REFERENCES dbo.vendors
	(
	oID
	) ON UPDATE  CASCADE 
	 ON DELETE  CASCADE 
GO
ALTER TABLE dbo.client_products ADD CONSTRAINT
	FK_client_products_products FOREIGN KEY
	(
	product
	) REFERENCES dbo.products
	(
	oID
	) ON UPDATE  NO ACTION 
	 ON DELETE  NO ACTION 
GO
COMMIT
BEGIN TRANSACTION
GO
ALTER TABLE dbo.client_product_notes ADD CONSTRAINT
	FK_client_product_notes_client_products FOREIGN KEY
	(
	client_product
	) REFERENCES dbo.client_products
	(
	oID
	) ON UPDATE  CASCADE 
	 ON DELETE  CASCADE 
GO
ALTER TABLE dbo.client_product_notes SET (LOCK_ESCALATION = TABLE)
GO
COMMIT
BEGIN TRANSACTION
GO
ALTER TABLE dbo.client_products_history ADD CONSTRAINT
	FK_client_products_history_client_products FOREIGN KEY
	(
	client_product
	) REFERENCES dbo.client_products
	(
	oID
	) ON UPDATE  CASCADE 
	 ON DELETE  CASCADE 
GO
ALTER TABLE dbo.client_products_history SET (LOCK_ESCALATION = TABLE)
GO
COMMIT
BEGIN TRANSACTION
GO
ALTER TABLE dbo.product_transactions ADD CONSTRAINT
	FK_product_transactions_client_products FOREIGN KEY
	(
	client_product
	) REFERENCES dbo.client_products
	(
	oID
	) ON UPDATE  NO ACTION 
	 ON DELETE  NO ACTION 
GO
ALTER TABLE dbo.product_transactions SET (LOCK_ESCALATION = TABLE)
GO
COMMIT
BEGIN TRANSACTION
GO
ALTER TABLE dbo.ach_onetimes ADD CONSTRAINT
	FK_ach_onetimes_client_products FOREIGN KEY
	(
	client_product
	) REFERENCES dbo.client_products
	(
	oID
	) ON UPDATE  NO ACTION 
	 ON DELETE  NO ACTION 
GO
ALTER TABLE dbo.ach_onetimes SET (LOCK_ESCALATION = TABLE)
GO
COMMIT
BEGIN TRANSACTION
GO
ALTER TABLE dbo.deposit_batch_details ADD CONSTRAINT
	FK_deposit_batch_details_client_products FOREIGN KEY
	(
	client_product
	) REFERENCES dbo.client_products
	(
	oID
	) ON UPDATE  CASCADE 
	 ON DELETE  CASCADE 
GO
ALTER TABLE dbo.deposit_batch_details SET (LOCK_ESCALATION = TABLE)
GO
COMMIT
