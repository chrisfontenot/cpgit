USE [DebtPlus]
GO
/****** Object:  Table [dbo].[ticklers]    Script Date: 09/15/2014 13:13:26 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[ticklers](
	[tickler] [dbo].[typ_key] IDENTITY(1,1) NOT NULL,
	[client] [dbo].[typ_client] NOT NULL,
	[original_counselor] [dbo].[typ_counselor] NULL,
	[counselor] [dbo].[typ_key] NULL,
	[date_effective] [datetime] NOT NULL,
	[date_deleted] [datetime] NULL,
	[tickler_type] [dbo].[typ_key] NOT NULL,
	[priority] [int] NOT NULL,
	[note] [dbo].[typ_message] NULL,
	[created_by] [dbo].[typ_counselor] NOT NULL,
	[date_created] [dbo].[typ_date] NOT NULL,
	[old_tickler] [int] NULL,
 CONSTRAINT [PK_ticklers] PRIMARY KEY CLUSTERED 
(
	[tickler] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
