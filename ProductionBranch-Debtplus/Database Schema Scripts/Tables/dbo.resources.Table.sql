USE [DebtPlus]
GO
/****** Object:  Table [dbo].[resources]    Script Date: 09/15/2014 13:13:26 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[resources](
	[Resource] [dbo].[typ_key] IDENTITY(1,1) NOT NULL,
	[Resource_type] [dbo].[typ_key] NOT NULL,
	[Name] [varchar](50) NOT NULL,
	[AddressID] [dbo].[typ_key] NULL,
	[TelephoneID] [dbo].[typ_key] NULL,
	[FAXID] [dbo].[typ_key] NULL,
	[EmailID] [dbo].[typ_key] NULL,
	[WWW] [varchar](80) NULL,
	[date_created] [dbo].[typ_date] NOT NULL,
	[created_by] [dbo].[typ_counselor] NOT NULL,
	[old_resource] [int] NULL,
 CONSTRAINT [PK_resources] PRIMARY KEY CLUSTERED 
(
	[Resource] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 90) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Resource ID number. This is the primary key.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'resources', @level2type=N'COLUMN',@level2name=N'Resource'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Pointer to the type of the resource.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'resources', @level2type=N'COLUMN',@level2name=N'Resource_type'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Name of this resource' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'resources', @level2type=N'COLUMN',@level2name=N'Name'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Web page address' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'resources', @level2type=N'COLUMN',@level2name=N'WWW'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Date/Time when the row was created' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'resources', @level2type=N'COLUMN',@level2name=N'date_created'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Name of the person who created this row' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'resources', @level2type=N'COLUMN',@level2name=N'created_by'
GO
