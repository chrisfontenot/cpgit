USE [DebtPlus]
GO
/****** Object:  Table [dbo].[letter_tables]    Script Date: 09/15/2014 13:13:26 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[letter_tables](
	[letter_table] [dbo].[typ_key] IDENTITY(1,1) NOT NULL,
	[query_type] [int] NOT NULL,
	[query] [varchar](256) NOT NULL,
	[date_created] [dbo].[typ_date] NOT NULL,
	[created_by] [dbo].[typ_counselor] NOT NULL,
 CONSTRAINT [PK_letter_tables] PRIMARY KEY CLUSTERED 
(
	[letter_table] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
