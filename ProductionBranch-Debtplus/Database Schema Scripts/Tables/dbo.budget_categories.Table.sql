-- Rule applied to the budget_category column to ensure that we don't cross over into the "other" category group.
if not exists(select * from sysobjects where type = 'R' and name = N'valid_budget_category')
	exec ('CREATE RULE [dbo].[valid_budget_category] AS @value between 1 and 9999')
GO
EXEC UPDATE_ADD_COLUMN 'budget_categories', 'spanish_description', 'nvarchar(50)'
GO
EXEC('UPDATE [budget_categories] SET [spanish_description] = ''categoría no especificada'' WHERE [spanish_description] is null')
GO
EXEC UPDATE_DROP_CONSTRAINTS 'budget_categories'
GO
/* To prevent any potential data loss issues, you should review this script in detail before running it outside the context of the database designer.*/
BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION
GO
CREATE TABLE dbo.Tmp_budget_categories
	(
	budget_category dbo.typ_key NOT NULL IDENTITY (1, 1),
	budget_category_group dbo.typ_key NULL,
	description dbo.typ_description NOT NULL,
	spanish_description nvarchar(50) NOT NULL,
	ExpenseCode varchar(1) NOT NULL,
	auto_factor money NULL,
	person_factor money NULL,
	maximum_factor money NULL,
	rpps_code int NOT NULL,
	hpf varchar(50) NULL,
	date_created dbo.typ_date NOT NULL,
	created_by dbo.typ_counselor NOT NULL,
	heading bit NOT NULL,
	detail bit NOT NULL,
	living_expense bit NOT NULL,
	housing_expense bit NOT NULL,
	ts timestamp NOT NULL
	)  ON [PRIMARY]
GO
GRANT DELETE ON dbo.Tmp_budget_categories TO public  AS dbo
GO
GRANT INSERT ON dbo.Tmp_budget_categories TO public  AS dbo
GO
GRANT SELECT ON dbo.Tmp_budget_categories TO public  AS dbo
GO
GRANT UPDATE ON dbo.Tmp_budget_categories TO public  AS dbo
GO
DECLARE @v sql_variant 
SET @v = N'This table lists the system defined budget categories. Each budget has these categories.'
EXECUTE sp_addextendedproperty N'MS_Description', @v, N'SCHEMA', N'dbo', N'TABLE', N'Tmp_budget_categories', NULL, NULL
GO
DECLARE @v sql_variant 
SET @v = N'Primary key to the table. This is the budget category used in the budget_details table.'
EXECUTE sp_addextendedproperty N'MS_Description', @v, N'SCHEMA', N'dbo', N'TABLE', N'Tmp_budget_categories', N'COLUMN', N'budget_category'
GO
DECLARE @v sql_variant 
SET @v = N'English Language description associated with this category.'
EXECUTE sp_addextendedproperty N'MS_Description', @v, N'SCHEMA', N'dbo', N'TABLE', N'Tmp_budget_categories', N'COLUMN', N'description'
GO
DECLARE @v sql_variant 
SET @v = N'Spanish Language description associated with this category.'
EXECUTE sp_addextendedproperty N'MS_Description', @v, N'SCHEMA', N'dbo', N'TABLE', N'Tmp_budget_categories', N'COLUMN', N'spanish_description'
GO
DECLARE @v sql_variant 
SET @v = N'Per automobile, this is the suggested value.'
EXECUTE sp_addextendedproperty N'MS_Description', @v, N'SCHEMA', N'dbo', N'TABLE', N'Tmp_budget_categories', N'COLUMN', N'auto_factor'
GO
DECLARE @v sql_variant 
SET @v = N'Per person in the household, this is the suggested value.'
EXECUTE sp_addextendedproperty N'MS_Description', @v, N'SCHEMA', N'dbo', N'TABLE', N'Tmp_budget_categories', N'COLUMN', N'person_factor'
GO
DECLARE @v sql_variant 
SET @v = N'This is the suggested monthly maximum value.'
EXECUTE sp_addextendedproperty N'MS_Description', @v, N'SCHEMA', N'dbo', N'TABLE', N'Tmp_budget_categories', N'COLUMN', N'maximum_factor'
GO
DECLARE @v sql_variant 
SET @v = N'When reporting expenses to RPPS, use this value for the expenses.'
EXECUTE sp_addextendedproperty N'MS_Description', @v, N'SCHEMA', N'dbo', N'TABLE', N'Tmp_budget_categories', N'COLUMN', N'rpps_code'
GO
DECLARE @v sql_variant 
SET @v = N'System assigned timestamp when the row was created.'
EXECUTE sp_addextendedproperty N'MS_Description', @v, N'SCHEMA', N'dbo', N'TABLE', N'Tmp_budget_categories', N'COLUMN', N'date_created'
GO
DECLARE @v sql_variant 
SET @v = N'Name of the person who created the row.'
EXECUTE sp_addextendedproperty N'MS_Description', @v, N'SCHEMA', N'dbo', N'TABLE', N'Tmp_budget_categories', N'COLUMN', N'created_by'
GO
DECLARE @v sql_variant 
SET @v = N'If set, this category is a heading item.'
EXECUTE sp_addextendedproperty N'MS_Description', @v, N'SCHEMA', N'dbo', N'TABLE', N'Tmp_budget_categories', N'COLUMN', N'heading'
GO
DECLARE @v sql_variant 
SET @v = N'If set, this category may have expenses associated with it.'
EXECUTE sp_addextendedproperty N'MS_Description', @v, N'SCHEMA', N'dbo', N'TABLE', N'Tmp_budget_categories', N'COLUMN', N'detail'
GO
DECLARE @v sql_variant 
SET @v = N'Is this category a living expense?'
EXECUTE sp_addextendedproperty N'MS_Description', @v, N'SCHEMA', N'dbo', N'TABLE', N'Tmp_budget_categories', N'COLUMN', N'living_expense'
GO
DECLARE @v sql_variant 
SET @v = N'Is this category a housing expense?'
EXECUTE sp_addextendedproperty N'MS_Description', @v, N'SCHEMA', N'dbo', N'TABLE', N'Tmp_budget_categories', N'COLUMN', N'housing_expense'
GO
ALTER TABLE dbo.Tmp_budget_categories ADD CONSTRAINT DF_budget_categories_spanish_description DEFAULT 'categoría no especificada' FOR spanish_description
GO
ALTER TABLE dbo.Tmp_budget_categories ADD CONSTRAINT DF_budget_categories_ExpenseCode DEFAULT ('L') FOR ExpenseCode
GO
ALTER TABLE dbo.Tmp_budget_categories ADD CONSTRAINT DF_budget_categories_rpps_code DEFAULT ((11)) FOR rpps_code
GO
EXECUTE sp_bindefault N'dbo.default_false', N'dbo.Tmp_budget_categories.heading'
GO
EXECUTE sp_bindefault N'dbo.default_false', N'dbo.Tmp_budget_categories.detail'
GO
EXECUTE sp_bindefault N'dbo.default_false', N'dbo.Tmp_budget_categories.living_expense'
GO
EXECUTE sp_bindefault N'dbo.default_false', N'dbo.Tmp_budget_categories.housing_expense'
GO
SET IDENTITY_INSERT dbo.Tmp_budget_categories ON
GO
IF EXISTS(SELECT * FROM dbo.budget_categories)
	EXEC('INSERT INTO dbo.Tmp_budget_categories (budget_category, budget_category_group, description, ExpenseCode, auto_factor, person_factor, maximum_factor, rpps_code, hpf, date_created, created_by, heading, detail, living_expense, housing_expense)
		SELECT budget_category, budget_category_group, description, ExpenseCode, auto_factor, person_factor, maximum_factor, rpps_code, hpf, date_created, created_by, heading, detail, living_expense, housing_expense FROM dbo.budget_categories WITH (HOLDLOCK TABLOCKX)')
GO
SET IDENTITY_INSERT dbo.Tmp_budget_categories OFF
GO
DROP TABLE dbo.budget_categories
GO
EXECUTE sp_rename N'dbo.Tmp_budget_categories', N'budget_categories', 'OBJECT' 
GO
ALTER TABLE dbo.budget_categories ADD CONSTRAINT PK_budget_categories PRIMARY KEY CLUSTERED ( budget_category ) WITH( STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
EXECUTE sp_bindrule N'dbo.valid_budget_category', N'dbo.budget_categories.budget_category'
GO
CREATE TRIGGER [dbo].[trig_budget_categories_D] on dbo.budget_categories AFTER DELETE as
begin
	-- ================================================================
	-- ==     Process the deletion of a budget category              ==
	-- ================================================================

	set nocount on

	-- Remove the client budget referneces
	delete	budget_detail
	from	budget_detail i
	inner join deleted d on i.budget_category = d.budget_category
	
	-- Remove the intake budget references
	delete	intake_budgets
	from	intake_budgets i
	inner join deleted d on i.budget_category = d.budget_category
end
GO
DENY DELETE ON dbo.budget_categories TO www_role  AS dbo 
GO
DENY INSERT ON dbo.budget_categories TO www_role  AS dbo 
GO
DENY SELECT ON dbo.budget_categories TO www_role  AS dbo 
GO
DENY UPDATE ON dbo.budget_categories TO www_role  AS dbo 
GO
COMMIT
