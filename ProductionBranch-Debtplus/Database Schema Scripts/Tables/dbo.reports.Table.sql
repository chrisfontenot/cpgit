USE [DebtPlus]
GO
/****** Object:  Table [dbo].[reports]    Script Date: 09/15/2014 13:13:26 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[reports](
	[report] [dbo].[typ_key] IDENTITY(1,1) NOT NULL,
	[label] [dbo].[typ_description] NULL,
	[description] [dbo].[typ_description] NULL,
	[Type] [varchar](2) NULL,
	[menu_name] [varchar](255) NULL,
	[ClassName] [varchar](512) NULL,
	[Argument] [varchar](512) NULL,
	[date_created] [dbo].[typ_date] NOT NULL,
	[created_by] [dbo].[typ_counselor] NOT NULL,
	[assembly] [varchar](512) NULL,
	[filename] [varchar](255) NULL,
 CONSTRAINT [PK_reports] PRIMARY KEY CLUSTERED 
(
	[report] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 90) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
