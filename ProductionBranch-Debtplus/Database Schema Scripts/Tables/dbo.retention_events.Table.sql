USE [DebtPlus]
GO
/****** Object:  Table [dbo].[retention_events]    Script Date: 09/15/2014 13:13:26 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[retention_events](
	[retention_event] [dbo].[typ_key] IDENTITY(101,1) NOT NULL,
	[description] [dbo].[typ_description] NOT NULL,
	[priority] [int] NOT NULL,
	[initial_retention_action] [dbo].[typ_key] NULL,
	[expire_type] [int] NULL,
	[date_created] [dbo].[typ_date] NOT NULL,
	[created_by] [dbo].[typ_counselor] NOT NULL,
 CONSTRAINT [PK_retention_events] PRIMARY KEY CLUSTERED 
(
	[retention_event] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
