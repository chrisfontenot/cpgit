USE [DebtPlus]
GO
/****** Object:  Table [dbo].[workshop_contents]    Script Date: 09/15/2014 13:13:26 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[workshop_contents](
	[oID] [dbo].[typ_key] IDENTITY(1,1) NOT NULL,
	[workshop_type] [dbo].[typ_key] NOT NULL,
	[content_type] [dbo].[typ_key] NOT NULL,
	[date_created] [dbo].[typ_date] NOT NULL,
	[created_by] [dbo].[typ_counselor] NOT NULL,
 CONSTRAINT [PK_workshop_contents_1] PRIMARY KEY CLUSTERED 
(
	[oID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY],
 CONSTRAINT [IX_workshop_contents_1] UNIQUE NONCLUSTERED 
(
	[workshop_type] ASC,
	[content_type] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 90) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Pointer to the workshop type associated with the workshops. All instances of this workshop are assumed to be of the same content.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'workshop_contents', @level2type=N'COLUMN',@level2name=N'workshop_type'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Pointer to the content type for the workshop.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'workshop_contents', @level2type=N'COLUMN',@level2name=N'content_type'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Date/Time when the row was created.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'workshop_contents', @level2type=N'COLUMN',@level2name=N'date_created'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Person who created the row.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'workshop_contents', @level2type=N'COLUMN',@level2name=N'created_by'
GO
