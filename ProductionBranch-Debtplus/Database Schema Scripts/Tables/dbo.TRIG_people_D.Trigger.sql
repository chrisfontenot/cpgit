SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER TRIGGER   [TRIG_people_D] on [people] AFTER DELETE as
-- ================================================================================
-- ==         Remove the table references when a person is removed               ==
-- ================================================================================

-- ChangeLog
--    1/1/2011
--       Initial version

set nocount on

-- Remove the contact name
delete		Names
from		Names n
inner join	deleted d on n.Name = d.NameID

-- Remove the cell telephone reference
delete		TelephoneNumbers
from		TelephoneNumbers t
inner join	deleted d on t.TelephoneNumber = d.CellTelephoneID

-- Remove the work telephone reference
delete		TelephoneNumbers
from		TelephoneNumbers t
inner join	deleted d on t.TelephoneNumber = d.WorkTelephoneID

-- Remove the email reference
delete		EmailAddresses
from		EmailAddresses e
inner join	deleted d on e.Email = d.EmailID

-- Remove references to the housing borrower id information
-- We just want to set the PersonID = 0 for any references to the deleted people.
update		housing_borrowers set PersonID = 0
from		deleted d
inner join	housing_properties prop on d.client = prop.HousingID
inner join	housing_borrowers b on prop.ownerID = b.oID
where		(case d.relation when 1 then 1 else 2 end) = b.PersonID

update		housing_borrowers set PersonID = 0
from		deleted d
inner join	housing_properties prop on d.client = prop.HousingID
inner join	housing_borrowers b on prop.coownerID = b.oID
where		(case d.relation when 1 then 1 else 2 end) = b.PersonID

-- Remove references in the debts table
update		client_creditor
set			Person = null
from		client_creditor cc
inner join deleted d on cc.client = d.client  -- use an indexed field to make things faster.
where		cc.Person is not null
and			cc.Person = d.Person;

-- Remove the references to the people_updates table
delete		people_updates
from		people_updates u
inner join	deleted d on d.Person = u.Person
GO
