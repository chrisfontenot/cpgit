/* To prevent any potential data loss issues, you should review this script in detail before running it outside the context of the database designer.*/
BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION
GO
CREATE TABLE dbo.Tmp_HPFOutcomes
	(
	oID dbo.typ_key NOT NULL IDENTITY (1, 1),
	propertyID dbo.typ_key NOT NULL,
	outcomeTypeID dbo.typ_key NULL,
	nonProfitReferralName varchar(50) NULL,
	extRefOtherName varchar(50) NULL,
	date_created dbo.typ_date NOT NULL,
	created_by dbo.typ_counselor NOT NULL,
	ts timestamp NOT NULL
	)  ON [PRIMARY]
GO
GRANT DELETE ON dbo.Tmp_HPFOutcomes TO public  AS dbo
GO
GRANT INSERT ON dbo.Tmp_HPFOutcomes TO public  AS dbo
GO
GRANT SELECT ON dbo.Tmp_HPFOutcomes TO public  AS dbo
GO
GRANT UPDATE ON dbo.Tmp_HPFOutcomes TO public  AS dbo
GO
SET IDENTITY_INSERT dbo.Tmp_HPFOutcomes ON
GO
IF EXISTS(SELECT * FROM sysobjects WHERE name=N'HPFOutcomes' AND type='U')
BEGIN
	EXEC('INSERT INTO dbo.Tmp_HPFOutcomes (oID, propertyID, outcomeTypeID, nonProfitReferralName, extRefOtherName, date_created, created_by)
		SELECT oID, propertyID, outcomeTypeID, nonProfitReferralName, extRefOtherName, date_created, created_by FROM dbo.HPFOutcomes WITH (HOLDLOCK TABLOCKX)')
	EXEC('DROP TABLE dbo.HPFOutcomes')
END
GO
SET IDENTITY_INSERT dbo.Tmp_HPFOutcomes OFF
GO
EXECUTE sp_rename N'dbo.Tmp_HPFOutcomes', N'HPFOutcomes', 'OBJECT' 
GO
ALTER TABLE dbo.HPFOutcomes ADD CONSTRAINT
	PK_HPFOutcomes PRIMARY KEY CLUSTERED 
	(
	oID
	) WITH( STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
DENY DELETE ON dbo.HPFOutcomes TO www_role  AS dbo 
GO
DENY INSERT ON dbo.HPFOutcomes TO www_role  AS dbo 
GO
DENY SELECT ON dbo.HPFOutcomes TO www_role  AS dbo 
GO
DENY UPDATE ON dbo.HPFOutcomes TO www_role  AS dbo 
GO
COMMIT
