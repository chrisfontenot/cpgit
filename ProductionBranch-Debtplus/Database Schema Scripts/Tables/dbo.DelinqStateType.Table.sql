use debtplus
GO

/* To prevent any potential data loss issues, you should review this script in detail before running it outside the context of the database designer.*/
BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION
GO
CREATE TABLE dbo.Tmp_DelinqStateType
	(
	oID dbo.typ_key NOT NULL IDENTITY (1, 1),
	Name dbo.typ_description NOT NULL,
	SortOrder int NULL,
	[Default] bit NOT NULL,
	ActiveFlag bit NOT NULL,
	date_created dbo.typ_date NOT NULL,
	created_by dbo.typ_counselor NOT NULL,
	ts rowversion NOT NULL
	)  ON [PRIMARY]
GO
EXECUTE sp_bindefault N'dbo.default_false', N'dbo.Tmp_DelinqStateType.[Default]'
GO
EXECUTE sp_bindefault N'dbo.default_true', N'dbo.Tmp_DelinqStateType.ActiveFlag'
GO
SET IDENTITY_INSERT dbo.Tmp_DelinqStateType ON
GO
IF EXISTS(SELECT * FROM sysobjects where name = N'DelinqStateType' AND type = 'U')
BEGIN
	EXEC('INSERT INTO dbo.Tmp_DelinqStateType (oID, Name, SortOrder)
		SELECT oID, Name, SortOrder FROM dbo.DelinqStateType WITH (HOLDLOCK TABLOCKX)')

	declare @actualrowcount int 
	select @actualrowcount = count(*) from DelinqStateType
	print 'DelinqStateType row count : ' + convert(varchar, @actualrowcount)

	declare @tmprowcount int 
	select @tmprowcount = count(*) from Tmp_DelinqStateType
	print 'Tmp_DelinqStateType row count : ' + convert(varchar, @tmprowcount)

	if @actualrowcount = @tmprowcount 
	begin
		print 'actual row count and temp table row count match, dropping table...'
		drop table dbo.DelinqStateType
	end
END
GO
SET IDENTITY_INSERT dbo.Tmp_DelinqStateType OFF
GO
EXECUTE sp_rename N'dbo.Tmp_DelinqStateType', N'DelinqStateType', 'OBJECT' 
GO
ALTER TABLE dbo.DelinqStateType ADD CONSTRAINT
	PK_DelinqStateType PRIMARY KEY CLUSTERED 
	(
	oID
	) ON [PRIMARY]
GO
COMMIT
GO
GRANT SELECT,INSERT,DELETE,UPDATE ON DelinqStateType TO public AS dbo
GO
DENY SELECT,INSERT,DELETE,UPDATE ON DelinqStateType TO www_role
GO
IF NOT EXISTS(SELECT * FROM DelinqStateType)
BEGIN
	INSERT INTO [dbo].[DelinqStateType] ([Name],[Sortorder]) VALUES('Current',1)
	INSERT INTO [dbo].[DelinqStateType] ([Name],[Sortorder]) VALUES('Delinquent',2)
END
GO
