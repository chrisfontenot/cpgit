USE [DebtPlus]
GO
/****** Object:  Table [dbo].[zipcodes]    Script Date: 09/15/2014 13:13:26 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[zipcodes](
	[zipcode] [dbo].[typ_key] IDENTITY(1,1) NOT NULL,
	[office] [dbo].[typ_key] NOT NULL,
	[zip_lower] [dbo].[typ_zipcode] NOT NULL,
	[zip_upper] [dbo].[typ_zipcode] NOT NULL,
	[date_created] [dbo].[typ_date] NOT NULL,
	[created_by] [dbo].[typ_counselor] NOT NULL,
	[old_zipcode] [int] NULL,
 CONSTRAINT [PK_zipcodes] PRIMARY KEY CLUSTERED 
(
	[zipcode] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 90) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Record number for the zipcode entry' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'zipcodes', @level2type=N'COLUMN',@level2name=N'zipcode'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Office number for the zipcode range' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'zipcodes', @level2type=N'COLUMN',@level2name=N'office'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Lower range for the zipcodes' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'zipcodes', @level2type=N'COLUMN',@level2name=N'zip_lower'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Upper range for the zipcodes' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'zipcodes', @level2type=N'COLUMN',@level2name=N'zip_upper'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Date/Time when the row was created.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'zipcodes', @level2type=N'COLUMN',@level2name=N'date_created'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Person who created the row.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'zipcodes', @level2type=N'COLUMN',@level2name=N'created_by'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'This table maps zip codes to office locations.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'zipcodes'
GO
