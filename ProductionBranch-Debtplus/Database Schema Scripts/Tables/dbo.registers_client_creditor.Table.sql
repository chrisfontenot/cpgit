USE [DebtPlus]
GO
/****** Object:  Table [dbo].[registers_client_creditor]    Script Date: 09/15/2014 13:13:26 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[registers_client_creditor](
	[client_creditor_register] [dbo].[typ_key] IDENTITY(1,1) NOT NULL,
	[client_creditor] [dbo].[typ_key] NOT NULL,
	[tran_type] [dbo].[typ_transaction] NOT NULL,
	[client] [dbo].[typ_client] NULL,
	[creditor] [dbo].[typ_creditor] NULL,
	[creditor_type] [dbo].[typ_creditor_type] NULL,
	[disbursement_register] [dbo].[typ_key] NULL,
	[trust_register] [dbo].[typ_key] NULL,
	[invoice_register] [dbo].[typ_key] NULL,
	[creditor_register] [dbo].[typ_key] NULL,
	[credit_amt] [money] NULL,
	[debit_amt] [money] NULL,
	[fairshare_amt] [money] NULL,
	[fairshare_pct] [dbo].[typ_fairshare_rate] NULL,
	[account_number] [dbo].[typ_client_account] NULL,
	[void] [int] NOT NULL,
	[date_created] [dbo].[typ_date] NOT NULL,
	[created_by] [dbo].[typ_counselor] NOT NULL,
	[ts] [timestamp] NOT NULL,
 CONSTRAINT [PK_registers_client_creditor] PRIMARY KEY CLUSTERED 
(
	[client_creditor_register] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Primary key to the table. This is a sequential number.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'registers_client_creditor', @level2type=N'COLUMN',@level2name=N'client_creditor_register'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Pointer to the specific debt for the transaction.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'registers_client_creditor', @level2type=N'COLUMN',@level2name=N'client_creditor'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Transaction type for the record' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'registers_client_creditor', @level2type=N'COLUMN',@level2name=N'tran_type'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Client ID of the operation' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'registers_client_creditor', @level2type=N'COLUMN',@level2name=N'client'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Creditor ID of the operation' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'registers_client_creditor', @level2type=N'COLUMN',@level2name=N'creditor'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Type of the creditor''s fairshare computation. N = no contribution, B = billed creditor, D = deduct creditor' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'registers_client_creditor', @level2type=N'COLUMN',@level2name=N'creditor_type'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Pointer to the disbursement register for the batch' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'registers_client_creditor', @level2type=N'COLUMN',@level2name=N'disbursement_register'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Pointer to the trust register for the operation.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'registers_client_creditor', @level2type=N'COLUMN',@level2name=N'trust_register'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Pointer to the invoice register for the operation' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'registers_client_creditor', @level2type=N'COLUMN',@level2name=N'invoice_register'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'For refunds, this is the pointer to the registers_creditor table that is the "check".' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'registers_client_creditor', @level2type=N'COLUMN',@level2name=N'creditor_register'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'IF this is a withdrawal record, this is the gross amount of the withdrawal' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'registers_client_creditor', @level2type=N'COLUMN',@level2name=N'credit_amt'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'If this is a deposit, this is the gross amount of the deposit' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'registers_client_creditor', @level2type=N'COLUMN',@level2name=N'debit_amt'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Amount of the fairshare with this transaction' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'registers_client_creditor', @level2type=N'COLUMN',@level2name=N'fairshare_amt'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Rate associated with the operation. Used in the disbursement/check to calculate the proper fairshare.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'registers_client_creditor', @level2type=N'COLUMN',@level2name=N'fairshare_pct'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Set only in disbursement. This is the account number used in the payment at the time of the payment.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'registers_client_creditor', @level2type=N'COLUMN',@level2name=N'account_number'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'If the invoice item is to be voided then this is set to 1.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'registers_client_creditor', @level2type=N'COLUMN',@level2name=N'void'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Date/Time when the row was created.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'registers_client_creditor', @level2type=N'COLUMN',@level2name=N'date_created'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Person who created the row.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'registers_client_creditor', @level2type=N'COLUMN',@level2name=N'created_by'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'This table holds the information associated with the debts in the system. It is a very, very, large table and should be consulted only when you need the detail information. The registers_client and registers_creditor are much smaller tables.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'registers_client_creditor'
GO
