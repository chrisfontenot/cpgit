EXECUTE UPDATE_DROP_CONSTRAINTS 'StateMessages'
GO
/* To prevent any potential data loss issues, you should review this script in detail before running it outside the context of the database designer.*/
BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION
GO
CREATE TABLE dbo.Tmp_StateMessages
	(
	oID dbo.typ_key NOT NULL IDENTITY (1, 1) NOT FOR REPLICATION,
	State dbo.typ_key NOT NULL,
	StateMessageType dbo.typ_key NOT NULL,
	Effective datetime NOT NULL,
	Details text NOT NULL,
	Reason varchar(512) NULL,
	date_updated dbo.typ_date NOT NULL,
	updated_by dbo.typ_counselor NOT NULL,
	date_created dbo.typ_date NOT NULL,
	created_by dbo.typ_counselor NOT NULL,
	ts timestamp NOT NULL
	)  ON [PRIMARY]
	 TEXTIMAGE_ON [PRIMARY]
GO
ALTER TABLE dbo.Tmp_StateMessages SET (LOCK_ESCALATION = TABLE)
GO
GRANT DELETE ON dbo.Tmp_StateMessages TO public  AS dbo
GO
GRANT INSERT ON dbo.Tmp_StateMessages TO public  AS dbo
GO
GRANT SELECT ON dbo.Tmp_StateMessages TO public  AS dbo
GO
GRANT UPDATE ON dbo.Tmp_StateMessages TO public  AS dbo
GO
EXECUTE sp_bindefault N'dbo.default_date', N'dbo.Tmp_StateMessages.Effective'
GO
SET IDENTITY_INSERT dbo.Tmp_StateMessages ON
GO
IF EXISTS(SELECT * FROM dbo.StateMessages)
	 EXEC('INSERT INTO dbo.Tmp_StateMessages (oID, State, StateMessageType, Effective, Details, Reason, date_updated, updated_by, date_created, created_by)
		SELECT oID, State, StateMessageType, Effective, Details, Reason, date_updated, updated_by, date_created, created_by FROM dbo.StateMessages WITH (HOLDLOCK TABLOCKX)')
GO
SET IDENTITY_INSERT dbo.Tmp_StateMessages OFF
GO
DROP TABLE dbo.StateMessages
GO
EXECUTE sp_rename N'dbo.Tmp_StateMessages', N'StateMessages', 'OBJECT' 
GO
ALTER TABLE dbo.StateMessages ADD CONSTRAINT
	PK_StateMessages PRIMARY KEY CLUSTERED 
	(
	oID
	) WITH( PAD_INDEX = OFF, FILLFACTOR = 90, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
ALTER TABLE dbo.StateMessages ADD CONSTRAINT
	FK_StateMessages_states FOREIGN KEY
	(
	State
	) REFERENCES dbo.states
	(
	state
	) ON UPDATE  CASCADE 
	 ON DELETE  CASCADE 
GO
ALTER TABLE dbo.StateMessages ADD CONSTRAINT
	FK_StateMessages_StateMessageTypes FOREIGN KEY
	(
	StateMessageType
	) REFERENCES dbo.StateMessageTypes
	(
	oID
	) ON UPDATE  CASCADE 
	 ON DELETE  CASCADE 
GO
CREATE TRIGGER dbo.TRIG_StateMessages_U ON dbo.StateMessages AFTER UPDATE AS 
BEGIN
	SET NOCOUNT ON;

	-- Set the update information where needed
	if (not update(date_updated)) and (not update(updated_by))
	begin
		update	StateMessages
		set		date_updated = getdate(),
				updated_by	 = suser_sname()
		from	StateMessages m
		inner join inserted i on m.oID = i.OID
	end
END
GO
DENY DELETE ON dbo.StateMessages TO www_role  AS dbo 
GO
DENY INSERT ON dbo.StateMessages TO www_role  AS dbo 
GO
DENY SELECT ON dbo.StateMessages TO www_role  AS dbo 
GO
DENY UPDATE ON dbo.StateMessages TO www_role  AS dbo 
GO
COMMIT
