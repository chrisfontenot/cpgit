USE [DebtPlus]
GO
/****** Object:  Table [dbo].[rpps_contacts]    Script Date: 09/15/2014 13:13:26 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[rpps_contacts](
	[rpps_contact] [dbo].[typ_key] IDENTITY(1,1) NOT NULL,
	[rpps_biller_id] [dbo].[typ_rpps_biller_id] NOT NULL,
	[Mastercard_key] [varchar](50) NULL,
	[EffDate] [datetime] NOT NULL,
	[Type] [varchar](50) NULL,
	[FirstName] [varchar](255) NULL,
	[LastName] [varchar](255) NULL,
	[Title] [varchar](255) NULL,
	[address1] [varchar](255) NULL,
	[address2] [varchar](255) NULL,
	[city] [varchar](255) NULL,
	[state] [varchar](50) NULL,
	[country] [varchar](50) NULL,
	[postalcode] [varchar](50) NULL,
	[email] [varchar](255) NULL,
 CONSTRAINT [PK_rpps_contacts] PRIMARY KEY CLUSTERED 
(
	[rpps_contact] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
ALTER TABLE [dbo].[rpps_contacts] ADD  CONSTRAINT [DF_rpps_contacts_EffDate]  DEFAULT (getdate()) FOR [EffDate]
GO
