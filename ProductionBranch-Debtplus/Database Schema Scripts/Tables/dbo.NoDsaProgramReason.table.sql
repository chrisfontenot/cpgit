/*
   Wednesday, November 04, 20156:37:15 AM
   User: sa
   Server: 10.0.199.12
   Database: DebtPlus_HPF
   Application: 
*/

/* To prevent any potential data loss issues, you should review this script in detail before running it outside the context of the database designer.*/
BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION
GO
EXECUTE UPDATE_ADD_COLUMN 'NoDsaProgramReason', 'hpf'
GO
CREATE TABLE dbo.Tmp_NoDsaProgramReason
	(
	oID dbo.typ_key NOT NULL IDENTITY (1, 1),
	description dbo.typ_description NULL,
	nfcc varchar(4) NULL,
	rpps varchar(4) NULL,
	epay varchar(4) NULL,
	note dbo.typ_message NULL,
	hud_9902_section varchar(256) NULL,
	hpf varchar(50) NULL,
	[default] bit NOT NULL,
	ActiveFlag bit NOT NULL,
	date_created dbo.typ_date NOT NULL,
	created_by dbo.typ_counselor NOT NULL,
	ts timestamp NOT NULL
	)  ON [PRIMARY]
GO
GRANT DELETE ON dbo.Tmp_NoDsaProgramReason TO public  AS dbo
GO
GRANT INSERT ON dbo.Tmp_NoDsaProgramReason TO public  AS dbo
GO
GRANT SELECT ON dbo.Tmp_NoDsaProgramReason TO public  AS dbo
GO
GRANT UPDATE ON dbo.Tmp_NoDsaProgramReason TO public  AS dbo
GO
SET IDENTITY_INSERT dbo.Tmp_NoDsaProgramReason ON
GO
IF EXISTS(SELECT * FROM dbo.NoDsaProgramReason)
	 EXEC('INSERT INTO dbo.Tmp_NoDsaProgramReason (oID, description, nfcc, rpps, epay, note, hud_9902_section, [default], ActiveFlag, date_created, created_by, hpf)
		SELECT oID, description, nfcc, rpps, epay, note, hud_9902_section, [default], ActiveFlag, date_created, created_by, hpf FROM dbo.NoDsaProgramReason WITH (HOLDLOCK TABLOCKX)')
GO
SET IDENTITY_INSERT dbo.Tmp_NoDsaProgramReason OFF
GO
DROP TABLE dbo.NoDsaProgramReason
GO
EXECUTE sp_rename N'dbo.Tmp_NoDsaProgramReason', N'NoDsaProgramReason', 'OBJECT' 
GO
ALTER TABLE dbo.NoDsaProgramReason ADD CONSTRAINT
	PK_NoDsaProgramReason PRIMARY KEY CLUSTERED 
	(
	oID
	) WITH( STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
DENY DELETE ON dbo.NoDsaProgramReason TO www_role  AS dbo 
GO
DENY INSERT ON dbo.NoDsaProgramReason TO www_role  AS dbo 
GO
DENY SELECT ON dbo.NoDsaProgramReason TO www_role  AS dbo 
GO
DENY UPDATE ON dbo.NoDsaProgramReason TO www_role  AS dbo 
GO
COMMIT
