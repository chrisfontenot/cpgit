USE [DebtPlus]
GO
/* To prevent any potential data loss issues, you should review this script in detail before running it outside the context of the database designer.*/
BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
GO
EXECUTE UPDATE_drop_constraints 'Housing_ImpactResultTypes'
EXECUTE UPDATE_add_column 'Housing_ImpactResultTypes', 'ImpactType', 'int'
EXECUTE UPDATE_add_column 'Housing_ImpactResultTypes', 'POVType', 'int'
EXECUTE UPDATE_add_column 'Housing_ImpactResultTypes', 'ResultType', 'int'
GO
BEGIN TRANSACTION
GO
CREATE TABLE dbo.Tmp_Housing_ImpactResultTypes
	(
	oID dbo.typ_key NOT NULL IDENTITY (1, 1),
	ImpactType dbo.typ_key NOT NULL,
	POVType dbo.typ_key NOT NULL,
	ResultType dbo.typ_key NOT NULL,
	ts timestamp NOT NULL
	)  ON [PRIMARY]
GO
DECLARE @v sql_variant 
SET @v = N'Primary key to the table'
EXECUTE sp_addextendedproperty N'MS_Description', @v, N'SCHEMA', N'dbo', N'TABLE', N'Tmp_Housing_ImpactResultTypes', N'COLUMN', N'oID'
GO
DECLARE @v sql_variant 
SET @v = N'Pointer to the Housing Impact and Scope item'
EXECUTE sp_addextendedproperty N'MS_Description', @v, N'SCHEMA', N'dbo', N'TABLE', N'Tmp_Housing_ImpactResultTypes', N'COLUMN', N'ImpactType'
GO
DECLARE @v sql_variant 
SET @v = N'Pointer to the associated Purpose Of Visit item'
EXECUTE sp_addextendedproperty N'MS_Description', @v, N'SCHEMA', N'dbo', N'TABLE', N'Tmp_Housing_ImpactResultTypes', N'COLUMN', N'POVType'
GO
DECLARE @v sql_variant 
SET @v = N'Pointer to the associated POV Result'
EXECUTE sp_addextendedproperty N'MS_Description', @v, N'SCHEMA', N'dbo', N'TABLE', N'Tmp_Housing_ImpactResultTypes', N'COLUMN', N'ResultType'
GO
DECLARE @v sql_variant 
SET @v = N'System assigned timestamp'
EXECUTE sp_addextendedproperty N'MS_Description', @v, N'SCHEMA', N'dbo', N'TABLE', N'Tmp_Housing_ImpactResultTypes', N'COLUMN', N'ts'
GO
SET IDENTITY_INSERT dbo.Tmp_Housing_ImpactResultTypes ON
GO
IF EXISTS(SELECT * FROM sysobjects WHERE type='U' AND name=N'Housing_ImpactResultTypes')
BEGIN
	EXEC('INSERT INTO dbo.Tmp_Housing_ImpactResultTypes (oID, ImpactType, POVType, ResultType) SELECT oID, ImpactType, POVType, ResultType FROM dbo.Housing_ImpactResultTypes WITH (HOLDLOCK TABLOCKX)')
	EXEC('DROP TABLE dbo.Housing_ImpactResultTypes')
END
GO
SET IDENTITY_INSERT dbo.Tmp_Housing_ImpactResultTypes OFF
GO
EXECUTE sp_rename N'dbo.Tmp_Housing_ImpactResultTypes', N'Housing_ImpactResultTypes', 'OBJECT' 
GO
ALTER TABLE dbo.Housing_ImpactResultTypes ADD CONSTRAINT
	PK_Housing_ImpactResultTypes PRIMARY KEY CLUSTERED 
	(
	oID
	) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX IX_Housing_ImpactResultTypes_1 ON dbo.Housing_ImpactResultTypes
	(
	POVType,
	ResultType
	) WITH( STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
CREATE UNIQUE NONCLUSTERED INDEX IX_Housing_ImpactResultTypes_2 ON dbo.Housing_ImpactResultTypes
	(
	ImpactType,
	POVType,
	ResultType
	) WITH( STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
ALTER TABLE dbo.Housing_ImpactResultTypes ADD CONSTRAINT
	FK_Housing_ImpactResultTypes_Housing_ResultTypes FOREIGN KEY
	(
	ResultType
	) REFERENCES dbo.Housing_ResultTypes
	(
	oID
	) ON UPDATE  NO ACTION 
	 ON DELETE  CASCADE 
GO
ALTER TABLE dbo.Housing_ImpactResultTypes ADD CONSTRAINT
	FK_Housing_ImpactResultTypes_Housing_HUD_ImpactTypes FOREIGN KEY
	(
	ImpactType
	) REFERENCES dbo.Housing_HUD_ImpactTypes
	(
	oID
	) ON UPDATE  NO ACTION 
	 ON DELETE  CASCADE 
GO
ALTER TABLE dbo.Housing_ImpactResultTypes ADD CONSTRAINT
	FK_Housing_ImpactResultTypes_Housing_PurposeOfVisitTypes FOREIGN KEY
	(
	POVType
	) REFERENCES dbo.Housing_PurposeOfVisitTypes
	(
	oID
	) ON UPDATE  NO ACTION 
	 ON DELETE  CASCADE 
GO
COMMIT
GO
DENY INSERT,SELECT,UPDATE,DELETE ON dbo.Housing_ImpactResultTypes TO www_role  AS dbo 
GO
