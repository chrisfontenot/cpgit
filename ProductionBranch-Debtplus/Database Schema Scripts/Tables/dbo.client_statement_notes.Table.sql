USE [DebtPlus]
GO
/****** Object:  Table [dbo].[client_statement_notes]    Script Date: 09/15/2014 13:13:26 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[client_statement_notes](
	[client_statement_note] [dbo].[typ_key] IDENTITY(1,1) NOT NULL,
	[client_statement_batch] [dbo].[typ_key] NULL,
	[client] [dbo].[typ_client] NOT NULL,
	[note] [dbo].[typ_message] NOT NULL,
	[date_created] [dbo].[typ_date] NOT NULL,
	[created_by] [dbo].[typ_counselor] NOT NULL,
	[old_client_statement_note] [int] NULL,
 CONSTRAINT [PK_client_statement_notes] PRIMARY KEY CLUSTERED 
(
	[client_statement_note] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 90) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
