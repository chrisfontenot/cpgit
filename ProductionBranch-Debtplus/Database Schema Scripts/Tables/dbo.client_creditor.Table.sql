USE [DebtPlus]
GO
/****** Object:  Table [dbo].[client_creditor]    Script Date: 09/15/2014 13:13:25 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[client_creditor](
	[client_creditor] [dbo].[typ_key] IDENTITY(1,1) NOT NULL,
	[client_creditor_balance] [dbo].[typ_key] NOT NULL,
	[client] [dbo].[typ_client] NOT NULL,
	[creditor] [dbo].[typ_creditor] NULL,
	[creditor_name] [varchar](80) NULL,
	[line_number] [int] NOT NULL,
	[priority] [int] NOT NULL,
	[drop_date] [datetime] NULL,
	[drop_reason] [dbo].[typ_key] NULL,
	[drop_reason_sent] [datetime] NULL,
	[person] [int] NULL,
	[client_name] [varchar](255) NULL,
	[non_dmp_payment] [money] NOT NULL,
	[non_dmp_interest] [dbo].[typ_fairshare_rate] NOT NULL,
	[disbursement_factor] [money] NOT NULL,
	[date_disp_changed] [datetime] NULL,
	[orig_dmp_payment] [money] NULL,
	[months_delinquent] [int] NOT NULL,
	[start_date] [datetime] NULL,
	[dmp_interest] [float] NULL,
	[dmp_payout_interest] [dbo].[typ_fairshare_rate] NULL,
	[last_stmt_date] [datetime] NULL,
	[irs_form_on_file] [bit] NOT NULL,
	[student_loan_release] [bit] NOT NULL,
	[balance_verification_release] [bit] NOT NULL,
	[last_payment_date_b4_dmp] [datetime] NULL,
	[expected_payout_date] [datetime] NULL,
	[rpps_client_type_indicator] [varchar](1) NULL,
	[terms] [int] NULL,
	[percent_balance] [float] NULL,
	[client_creditor_proposal] [dbo].[typ_key] NULL,
	[contact_name] [varchar](255) NULL,
	[payments_this_creditor] [money] NOT NULL,
	[returns_this_creditor] [money] NOT NULL,
	[interest_this_creditor] [money] NOT NULL,
	[sched_payment] [money] NOT NULL,
	[proposal_prenote_date] [datetime] NULL,
	[send_bal_verify] [int] NOT NULL,
	[verify_request_date] [datetime] NULL,
	[balance_verify_date] [datetime] NULL,
	[balance_verify_by] [varchar](50) NULL,
	[first_payment] [dbo].[typ_key] NULL,
	[last_payment] [dbo].[typ_key] NULL,
	[account_number] [dbo].[typ_client_account] NULL,
	[message] [dbo].[typ_message] NULL,
	[hold_disbursements] [bit] NOT NULL,
	[send_drop_notice] [bit] NOT NULL,
	[last_communication] [dbo].[typ_trace_number] NULL,
	[reassigned_debt] [bit] NOT NULL,
	[fairshare_pct_check] [float] NULL,
	[fairshare_pct_eft] [float] NULL,
	[prenote_date] [datetime] NULL,
	[check_payments] [int] NOT NULL,
	[payment_rpps_mask] [dbo].[typ_key] NULL,
	[payment_rpps_mask_timestamp] [binary](8) NULL,
	[rpps_mask] [dbo].[typ_key] NULL,
	[rpps_mask_timestamp] [binary](8) NULL,
	[created_by] [dbo].[typ_counselor] NOT NULL,
	[date_created] [dbo].[typ_date] NOT NULL,
	[old_client_creditor] [int] NULL,
 CONSTRAINT [PK_client_creditor] PRIMARY KEY CLUSTERED 
(
	[client_creditor] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Primary key to the table' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'client_creditor', @level2type=N'COLUMN',@level2name=N'client_creditor'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Pointer to the client_creditor_balances table. That table holds the balance information for this debt.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'client_creditor', @level2type=N'COLUMN',@level2name=N'client_creditor_balance'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Client number' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'client_creditor', @level2type=N'COLUMN',@level2name=N'client'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Creditor ID when it is known' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'client_creditor', @level2type=N'COLUMN',@level2name=N'creditor'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Creditor name when the creditor is not known' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'client_creditor', @level2type=N'COLUMN',@level2name=N'creditor_name'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'When stacking is used, this is the "priority" of the debt' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'client_creditor', @level2type=N'COLUMN',@level2name=N'line_number'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Normal priority for the debt. Taken from the creditor record when a creditor ID is known.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'client_creditor', @level2type=N'COLUMN',@level2name=N'priority'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Date when the debt is paid off.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'client_creditor', @level2type=N'COLUMN',@level2name=N'drop_date'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Person ID from the people table for the owner of the debt.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'client_creditor', @level2type=N'COLUMN',@level2name=N'person'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Client name when it is different from the account name' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'client_creditor', @level2type=N'COLUMN',@level2name=N'client_name'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Payment amount prior to starting DMP program.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'client_creditor', @level2type=N'COLUMN',@level2name=N'non_dmp_payment'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Interest prior to starting the DMP program' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'client_creditor', @level2type=N'COLUMN',@level2name=N'non_dmp_interest'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Monthly disbursement factor. Seeds the sched_payment figure used by disbursements.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'client_creditor', @level2type=N'COLUMN',@level2name=N'disbursement_factor'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Date that the disbursement factor was changed' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'client_creditor', @level2type=N'COLUMN',@level2name=N'date_disp_changed'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Initial proposed payment to the creditor' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'client_creditor', @level2type=N'COLUMN',@level2name=N'orig_dmp_payment'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Number of months delinquent when the debt is created.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'client_creditor', @level2type=N'COLUMN',@level2name=N'months_delinquent'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Date that the creditor should start to expect payments on the debt.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'client_creditor', @level2type=N'COLUMN',@level2name=N'start_date'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Override Interest calculation for DMP creditor' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'client_creditor', @level2type=N'COLUMN',@level2name=N'dmp_interest'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Interest rate to be used in the payout function when no other interest rate is available' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'client_creditor', @level2type=N'COLUMN',@level2name=N'dmp_payout_interest'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Date of the last statement received by the agency for this debt.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'client_creditor', @level2type=N'COLUMN',@level2name=N'last_stmt_date'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Is the IRS release form on file?' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'client_creditor', @level2type=N'COLUMN',@level2name=N'irs_form_on_file'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Is the student loan release on file?' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'client_creditor', @level2type=N'COLUMN',@level2name=N'student_loan_release'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Last payment date before the client started DMP' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'client_creditor', @level2type=N'COLUMN',@level2name=N'last_payment_date_b4_dmp'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Expected payout date' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'client_creditor', @level2type=N'COLUMN',@level2name=N'expected_payout_date'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Pointer to the current proposal record' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'client_creditor', @level2type=N'COLUMN',@level2name=N'client_creditor_proposal'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Contact name at the creditor for this debt account' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'client_creditor', @level2type=N'COLUMN',@level2name=N'contact_name'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Total gross disbursements to this creditor for this debt.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'client_creditor', @level2type=N'COLUMN',@level2name=N'payments_this_creditor'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Total gross refunds from this creditor for this debt. This is either creditor or RPPS refunds. Net payments = gross payments - gross refunds.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'client_creditor', @level2type=N'COLUMN',@level2name=N'returns_this_creditor'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Total Interest charged to this debt while the debt was assigned to this creditor.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'client_creditor', @level2type=N'COLUMN',@level2name=N'interest_this_creditor'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Scheduled payment used in the disbursement processing.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'client_creditor', @level2type=N'COLUMN',@level2name=N'sched_payment'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Time when the prenote balance verification was sent' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'client_creditor', @level2type=N'COLUMN',@level2name=N'proposal_prenote_date'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Should a balance verification be sent as soon as possible?' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'client_creditor', @level2type=N'COLUMN',@level2name=N'send_bal_verify'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Date that the balance verification was sent. Used to suppress multiple balance verifications.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'client_creditor', @level2type=N'COLUMN',@level2name=N'verify_request_date'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Date that the balance was verified to a value.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'client_creditor', @level2type=N'COLUMN',@level2name=N'balance_verify_date'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Name of the person at the creditor who verified the balance. ("SYSTEM" is RPPS'' identifier for a balance verification response.)' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'client_creditor', @level2type=N'COLUMN',@level2name=N'balance_verify_by'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Pointer to the registers_client_creditor for the first payment on this debt' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'client_creditor', @level2type=N'COLUMN',@level2name=N'first_payment'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Pointer to the registers_client_creditor for the last payment on this debt' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'client_creditor', @level2type=N'COLUMN',@level2name=N'last_payment'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Account number for the debt' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'client_creditor', @level2type=N'COLUMN',@level2name=N'account_number'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'A general-purpose message. Replaces the account number in some reports.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'client_creditor', @level2type=N'COLUMN',@level2name=N'message'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'If set, do not disburse money on this debt.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'client_creditor', @level2type=N'COLUMN',@level2name=N'hold_disbursements'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Should the drop notification be sent on this debt?' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'client_creditor', @level2type=N'COLUMN',@level2name=N'send_drop_notice'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Last proposal ID string. Used when balance verifications are sent by RPPS.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'client_creditor', @level2type=N'COLUMN',@level2name=N'last_communication'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Set when the debt is reassigned to a new creditor' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'client_creditor', @level2type=N'COLUMN',@level2name=N'reassigned_debt'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Fairshare percentage if a payment is made by check. Overrides the creditor defaults.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'client_creditor', @level2type=N'COLUMN',@level2name=N'fairshare_pct_check'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Fairshare percentage if the debt is paid via EFT. Overrides the creditor record.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'client_creditor', @level2type=N'COLUMN',@level2name=N'fairshare_pct_eft'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Date that the account was prenoted for RPPS or EPAY payments. NULL = not prenoted.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'client_creditor', @level2type=N'COLUMN',@level2name=N'prenote_date'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Number of payments made by check' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'client_creditor', @level2type=N'COLUMN',@level2name=N'check_payments'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Pointer to the creditor_method for payments on this debt' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'client_creditor', @level2type=N'COLUMN',@level2name=N'payment_rpps_mask'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Timestamp of the creditor_method table entry' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'client_creditor', @level2type=N'COLUMN',@level2name=N'payment_rpps_mask_timestamp'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Pointer to the rpps_mask table entry for this debt payments' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'client_creditor', @level2type=N'COLUMN',@level2name=N'rpps_mask'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Timestamp of the rpps_masks table pointer for this debt' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'client_creditor', @level2type=N'COLUMN',@level2name=N'rpps_mask_timestamp'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Person who created the row.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'client_creditor', @level2type=N'COLUMN',@level2name=N'created_by'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Date/Time when the row was created.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'client_creditor', @level2type=N'COLUMN',@level2name=N'date_created'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'This table reflects the current debts associated with the client. All ''top-level'' debt information is kept here.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'client_creditor'
GO
