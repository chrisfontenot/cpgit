USE [DebtPlus]
GO
/****** Object:  Table [dbo].[intake_secured_properties]    Script Date: 09/15/2014 13:13:26 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[intake_secured_properties](
	[secured_property] [dbo].[typ_key] IDENTITY(1,1) NOT NULL,
	[intake_client] [dbo].[typ_client] NOT NULL,
	[secured_type] [dbo].[typ_key] NOT NULL,
	[description] [dbo].[typ_description] NOT NULL,
	[sub_description] [dbo].[typ_description] NOT NULL,
	[original_price] [money] NOT NULL,
	[current_value] [money] NOT NULL,
	[year_acquired] [int] NOT NULL,
	[year_mfg] [int] NOT NULL,
	[housing_type] [dbo].[typ_key] NOT NULL,
	[primary_residence] [bit] NOT NULL,
	[created_by] [dbo].[typ_counselor] NOT NULL,
	[date_created] [dbo].[typ_date] NOT NULL,
 CONSTRAINT [PK_intake_secured_properties] PRIMARY KEY CLUSTERED 
(
	[secured_property] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 90) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
