/* To prevent any potential data loss issues, you should review this script in detail before running it outside the context of the database designer.*/
BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION
GO
execute UPDATE_add_column 'ZipCodeSearch', 'ZIPCode'
execute UPDATE_add_column 'ZipCodeSearch', 'ZIPType'
execute UPDATE_add_column 'ZipCodeSearch', 'CityName'
execute UPDATE_add_column 'ZipCodeSearch', 'CityType'
execute UPDATE_add_column 'ZipCodeSearch', 'State'
execute UPDATE_add_column 'ZipCodeSearch', 'ActiveFlag'
execute UPDATE_add_column 'ZipCodeSearch', 'CountyName'
execute UPDATE_add_column 'ZipCodeSearch', 'CountyFIPS'
execute UPDATE_add_column 'ZipCodeSearch', 'StateName'
execute UPDATE_add_column 'ZipCodeSearch', 'StateAbbr'
execute UPDATE_add_column 'ZipCodeSearch', 'StateFIPS'
execute UPDATE_add_column 'ZipCodeSearch', 'MSACode'
execute UPDATE_add_column 'ZipCodeSearch', 'AreaCode'
execute UPDATE_add_column 'ZipCodeSearch', 'TimeZone'
execute UPDATE_add_column 'ZipCodeSearch', 'UTC'
execute UPDATE_add_column 'ZipCodeSearch', 'DST'
execute UPDATE_add_column 'ZipCodeSearch', 'Latitude'
execute UPDATE_add_column 'ZipCodeSearch', 'Longitude'
GO
CREATE TABLE dbo.Tmp_ZipCodeSearch
	(
	oID int NOT NULL IDENTITY (1, 1),
	ZIPCode varchar(10) NOT NULL,
	ZIPType varchar(1) NOT NULL,
	CityName varchar(50) NOT NULL,
	CityType varchar(1) NOT NULL,
	State int NOT NULL,
	ActiveFlag bit NOT NULL,
	CountyName varchar(50) NULL,
	CountyFIPS varchar(50) NULL,
	StateName varchar(256) NULL,
	StateAbbr varchar(50) NULL,
	StateFIPS varchar(50) NULL,
	MSACode varchar(50) NULL,
	AreaCode varchar(10) NULL,
	TimeZone varchar(50) NULL,
	UTC varchar(50) NULL,
	DST varchar(50) NULL,
	Latitude float(53) NULL,
	Longitude float(53) NULL,
	ts timestamp NOT NULL
	)  ON [PRIMARY]
GO
GRANT SELECT,UPDATE,INSERT,DELETE on dbo.Tmp_ZipCodeSearch TO public AS dbo
GO
SET IDENTITY_INSERT dbo.Tmp_ZipCodeSearch ON
GO
IF EXISTS(SELECT * FROM dbo.ZipCodeSearch)
	 EXEC('INSERT INTO dbo.Tmp_ZipCodeSearch (oID, ZIPCode, ZIPType, CityName, CityType, CountyName, CountyFIPS, StateName, StateAbbr, State, ActiveFlag, StateFIPS, MSACode, AreaCode, TimeZone, UTC, DST, Latitude, Longitude)
		SELECT oID, ZIPCode, ZIPType, CityName, CityType, CountyName, CountyFIPS, StateName, StateAbbr, State, ActiveFlag, StateFIPS, MSACode, AreaCode, TimeZone, UTC, DST, Latitude, Longitude FROM dbo.ZipCodeSearch WITH (HOLDLOCK TABLOCKX)')
GO
SET IDENTITY_INSERT dbo.Tmp_ZipCodeSearch OFF
GO
DROP TABLE dbo.ZipCodeSearch
GO
EXECUTE sp_rename N'dbo.Tmp_ZipCodeSearch', N'ZipCodeSearch', 'OBJECT' 
GO
ALTER TABLE dbo.ZipCodeSearch ADD CONSTRAINT
	PK_ZipCodeSearch PRIMARY KEY CLUSTERED 
	(
	oID
	) WITH( STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX IX_ZipCodeSearch_1 ON dbo.ZipCodeSearch
	(
	ZIPCode
	)
	INCLUDE ( [ZIPType], [CityName], [CityType], [State], [ActiveFlag] )
	WITH( STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX IX_ZipCodeSearch_2 ON dbo.ZipCodeSearch
	(
	State
	) WITH( STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
COMMIT
GO
DENY SELECT,UPDATE,INSERT,DELETE on dbo.ZipCodeSearch TO www_role
GO