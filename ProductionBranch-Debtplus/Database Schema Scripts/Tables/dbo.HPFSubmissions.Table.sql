/* To prevent any potential data loss issues, you should review this script in detail before running it outside the context of the database designer.*/
BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
GO
EXECUTE UPDATE_DROP_CONSTRAINTS 'HPFSubmissions'
GO
BEGIN TRANSACTION
GO
CREATE TABLE dbo.Tmp_HPFSubmissions
	(
	oID dbo.typ_key NOT NULL IDENTITY (1, 1),
	requestType varchar(50) NOT NULL,
	propertyID dbo.typ_key NULL,
	FcId int NULL,
	requestXML xml NULL,
	responseXML xml NULL,
	responseStatus varchar(50) NULL,
	date_created dbo.typ_date NOT NULL,
	created_by dbo.typ_counselor NOT NULL,
	ts timestamp NOT NULL
	)  ON [PRIMARY]
	 TEXTIMAGE_ON [PRIMARY]
GO
GRANT DELETE ON dbo.Tmp_HPFSubmissions TO public  AS dbo
GO
GRANT INSERT ON dbo.Tmp_HPFSubmissions TO public  AS dbo
GO
GRANT SELECT ON dbo.Tmp_HPFSubmissions TO public  AS dbo
GO
GRANT UPDATE ON dbo.Tmp_HPFSubmissions TO public  AS dbo
GO
ALTER TABLE dbo.Tmp_HPFSubmissions ADD CONSTRAINT
	DF_HPFSubmissions_requestType DEFAULT ('SUBMIT') FOR requestType
GO
SET IDENTITY_INSERT dbo.Tmp_HPFSubmissions ON
GO
IF EXISTS(SELECT * FROM sysobjects WHERE name=N'HPFSubmissions' AND type='U')
BEGIN
	 EXEC('INSERT INTO dbo.Tmp_HPFSubmissions (oID, requestType, propertyID, FcId, requestXML, responseXML, date_created, created_by, responseStatus)
		SELECT oID, requestType, propertyID, FcId, requestXML, responseXML, date_created, created_by, responseStatus FROM dbo.HPFSubmissions WITH (HOLDLOCK TABLOCKX)')
	 EXEC('DROP TABLE dbo.HPFSubmissions')
END
GO
SET IDENTITY_INSERT dbo.Tmp_HPFSubmissions OFF
GO
EXECUTE sp_rename N'dbo.Tmp_HPFSubmissions', N'HPFSubmissions', 'OBJECT' 
GO
ALTER TABLE dbo.HPFSubmissions ADD CONSTRAINT
	PK_HPFSubmissions PRIMARY KEY CLUSTERED 
	(
	oID
	) WITH( STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
DENY DELETE ON dbo.HPFSubmissions TO www_role  AS dbo 
GO
DENY INSERT ON dbo.HPFSubmissions TO www_role  AS dbo 
GO
DENY SELECT ON dbo.HPFSubmissions TO www_role  AS dbo 
GO
DENY UPDATE ON dbo.HPFSubmissions TO www_role  AS dbo 
GO
COMMIT
