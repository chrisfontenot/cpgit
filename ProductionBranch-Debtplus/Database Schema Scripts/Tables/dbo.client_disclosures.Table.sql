ALTER TABLE dbo.client_disclosures DROP CONSTRAINT FK_client_disclosures_client_DisclosureLanguageTypes
ALTER TABLE dbo.client_disclosures DROP CONSTRAINT FK_client_disclosures_Housing_properties
ALTER TABLE dbo.client_disclosures DROP CONSTRAINT FK_client_disclosures_clients
GO
/* To prevent any potential data loss issues, you should review this script in detail before running it outside the context of the database designer.*/
BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION
GO
CREATE TABLE dbo.Tmp_client_disclosures
	(
	oID dbo.typ_key NOT NULL IDENTITY (1, 1),
	client dbo.typ_key NOT NULL,
	disclosureID dbo.typ_key NOT NULL,
	propertyID dbo.typ_key NULL,
	applies_applicant bit NOT NULL,
	applies_coapplicant bit NOT NULL,
	date_created dbo.typ_date NOT NULL,
	created_by dbo.typ_counselor NOT NULL,
	ts timestamp NOT NULL
	)  ON [PRIMARY]
GO
ALTER TABLE dbo.Tmp_client_disclosures SET (LOCK_ESCALATION = TABLE)
GO
DECLARE @v sql_variant 
SET @v = N'Primary key to the table'
EXECUTE sp_addextendedproperty N'MS_Description', @v, N'SCHEMA', N'dbo', N'TABLE', N'Tmp_client_disclosures', N'COLUMN', N'oID'
GO
DECLARE @v sql_variant 
SET @v = N'Pointer to the clients row'
EXECUTE sp_addextendedproperty N'MS_Description', @v, N'SCHEMA', N'dbo', N'TABLE', N'Tmp_client_disclosures', N'COLUMN', N'client'
GO
DECLARE @v sql_variant 
SET @v = N'Pointer to the DisclosureLanguageType row'
EXECUTE sp_addextendedproperty N'MS_Description', @v, N'SCHEMA', N'dbo', N'TABLE', N'Tmp_client_disclosures', N'COLUMN', N'disclosureID'
GO
DECLARE @v sql_variant 
SET @v = N'Pointer to the housing_properties row'
EXECUTE sp_addextendedproperty N'MS_Description', @v, N'SCHEMA', N'dbo', N'TABLE', N'Tmp_client_disclosures', N'COLUMN', N'propertyID'
GO
DECLARE @v sql_variant 
SET @v = N'Status of the check-box "applies to applicant"'
EXECUTE sp_addextendedproperty N'MS_Description', @v, N'SCHEMA', N'dbo', N'TABLE', N'Tmp_client_disclosures', N'COLUMN', N'applies_applicant'
GO
DECLARE @v sql_variant 
SET @v = N'Status of the check-box "applies to co-applicant"'
EXECUTE sp_addextendedproperty N'MS_Description', @v, N'SCHEMA', N'dbo', N'TABLE', N'Tmp_client_disclosures', N'COLUMN', N'applies_coapplicant'
GO
DECLARE @v sql_variant 
SET @v = N'Date the row was created'
EXECUTE sp_addextendedproperty N'MS_Description', @v, N'SCHEMA', N'dbo', N'TABLE', N'Tmp_client_disclosures', N'COLUMN', N'date_created'
GO
DECLARE @v sql_variant 
SET @v = N'User ID of the row''s creator'
EXECUTE sp_addextendedproperty N'MS_Description', @v, N'SCHEMA', N'dbo', N'TABLE', N'Tmp_client_disclosures', N'COLUMN', N'created_by'
GO
DECLARE @v sql_variant 
SET @v = N'Timestamp assigned when the row is updated'
EXECUTE sp_addextendedproperty N'MS_Description', @v, N'SCHEMA', N'dbo', N'TABLE', N'Tmp_client_disclosures', N'COLUMN', N'ts'
GO
EXECUTE sp_bindefault N'dbo.default_false', N'dbo.Tmp_client_disclosures.applies_applicant'
GO
EXECUTE sp_bindefault N'dbo.default_false', N'dbo.Tmp_client_disclosures.applies_coapplicant'
GO
SET IDENTITY_INSERT dbo.Tmp_client_disclosures ON
GO
IF EXISTS(SELECT * FROM SYSOBJECTS WHERE TYPE='U' AND NAME=N'client_disclosures')
BEGIN
	EXEC('INSERT INTO dbo.Tmp_client_disclosures (oID, client, disclosureID, propertyID, applies_applicant, applies_coapplicant, date_created, created_by) SELECT oID, client, disclosureID, propertyID, applies_applicant, applies_coapplicant, date_created, created_by FROM dbo.client_disclosures WITH (HOLDLOCK TABLOCKX)')
	EXEC('DROP TABLE dbo.client_disclosures')
END
GO
SET IDENTITY_INSERT dbo.Tmp_client_disclosures OFF
GO
EXEC ('ALTER TABLE dbo.client_disclosures_AFS DROP CONSTRAINT FK_client_disclosures_AFS_client_disclosures')
EXEC ('ALTER TABLE dbo.client_disclosures_CBR DROP CONSTRAINT FK_client_disclosures_CBR_client_disclosures')
EXEC ('ALTER TABLE dbo.client_disclosures_MHA DROP CONSTRAINT FK_client_disclosures_MHA_client_disclosures')
GO
EXECUTE sp_rename N'dbo.Tmp_client_disclosures', N'client_disclosures', 'OBJECT' 
GO
ALTER TABLE dbo.client_disclosures ADD CONSTRAINT
	PK_client_disclosures PRIMARY KEY CLUSTERED 
	(
	oID
	) ON [PRIMARY]
GO
ALTER TABLE dbo.client_disclosures ADD CONSTRAINT
	FK_client_disclosures_clients FOREIGN KEY
	(
	client
	) REFERENCES dbo.clients
	(
	client
	) ON UPDATE  CASCADE 
	 ON DELETE  CASCADE 
GO
ALTER TABLE dbo.client_disclosures ADD CONSTRAINT
	FK_client_disclosures_Housing_properties FOREIGN KEY
	(
	propertyID
	) REFERENCES dbo.Housing_properties
	(
	oID
	) ON UPDATE  NO ACTION 
	 ON DELETE  SET NULL 
GO
ALTER TABLE dbo.client_disclosures ADD CONSTRAINT
	FK_client_disclosures_client_DisclosureLanguageTypes FOREIGN KEY
	(
	disclosureID
	) REFERENCES dbo.client_DisclosureLanguageTypes
	(
	oID
	) ON UPDATE  CASCADE 
	 ON DELETE  CASCADE 
GO
COMMIT
BEGIN TRANSACTION
GO
ALTER TABLE dbo.client_disclosures_MHA ADD CONSTRAINT
	FK_client_disclosures_MHA_client_disclosures FOREIGN KEY
	(
	client_disclosure_ID
	) REFERENCES dbo.client_disclosures
	(
	oID
	) ON UPDATE  CASCADE 
	 ON DELETE  CASCADE 
GO
DECLARE @v sql_variant 
SET @v = N'One-to-One relationship'
EXECUTE sp_addextendedproperty N'MS_Description', @v, N'SCHEMA', N'dbo', N'TABLE', N'client_disclosures_MHA', N'CONSTRAINT', N'FK_client_disclosures_MHA_client_disclosures'
GO
ALTER TABLE dbo.client_disclosures_MHA SET (LOCK_ESCALATION = TABLE)
GO
COMMIT
BEGIN TRANSACTION
GO
ALTER TABLE dbo.client_disclosures_CBR ADD CONSTRAINT
	FK_client_disclosures_CBR_client_disclosures FOREIGN KEY
	(
	client_disclosure_ID
	) REFERENCES dbo.client_disclosures
	(
	oID
	) ON UPDATE  CASCADE 
	 ON DELETE  CASCADE 
GO
DECLARE @v sql_variant 
SET @v = N'One-to-One relationship'
EXECUTE sp_addextendedproperty N'MS_Description', @v, N'SCHEMA', N'dbo', N'TABLE', N'client_disclosures_CBR', N'CONSTRAINT', N'FK_client_disclosures_CBR_client_disclosures'
GO
COMMIT
BEGIN TRANSACTION
GO
ALTER TABLE dbo.client_disclosures_AFS ADD CONSTRAINT
	FK_client_disclosures_AFS_client_disclosures FOREIGN KEY
	(
	client_disclosure_ID
	) REFERENCES dbo.client_disclosures
	(
	oID
	) ON UPDATE  CASCADE 
	 ON DELETE  CASCADE 
GO
DECLARE @v sql_variant 
SET @v = N'One-to-One relationship'
EXECUTE sp_addextendedproperty N'MS_Description', @v, N'SCHEMA', N'dbo', N'TABLE', N'client_disclosures_AFS', N'CONSTRAINT', N'FK_client_disclosures_AFS_client_disclosures'
GO
ALTER TABLE dbo.client_disclosures_AFS SET (LOCK_ESCALATION = TABLE)
GO
COMMIT
