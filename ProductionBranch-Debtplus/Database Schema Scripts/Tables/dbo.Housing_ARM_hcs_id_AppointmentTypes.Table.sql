EXECUTE UPDATE_DROP_CONSTRAINTS 'Housing_ARM_hcs_id_AppointmentTypes'
GO
/* To prevent any potential data loss issues, you should review this script in detail before running it outside the context of the database designer.*/
BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION
GO
CREATE TABLE dbo.Tmp_Housing_ARM_hcs_id_AppointmentTypes
	(
	oID dbo.typ_key NOT NULL IDENTITY (1, 1) NOT FOR REPLICATION,
	hcs_id dbo.typ_key NOT NULL,
	AppointmentType varchar(10) NOT NULL,
	ts timestamp NOT NULL
	)  ON [PRIMARY]
GO
GRANT DELETE ON dbo.Tmp_Housing_ARM_hcs_id_AppointmentTypes TO public  AS dbo
GO
GRANT INSERT ON dbo.Tmp_Housing_ARM_hcs_id_AppointmentTypes TO public  AS dbo
GO
GRANT SELECT ON dbo.Tmp_Housing_ARM_hcs_id_AppointmentTypes TO public  AS dbo
GO
GRANT UPDATE ON dbo.Tmp_Housing_ARM_hcs_id_AppointmentTypes TO public  AS dbo
GO
SET IDENTITY_INSERT dbo.Tmp_Housing_ARM_hcs_id_AppointmentTypes ON
GO
IF EXISTS(SELECT * FROM dbo.Housing_ARM_hcs_id_AppointmentTypes)
	 EXEC('INSERT INTO dbo.Tmp_Housing_ARM_hcs_id_AppointmentTypes (oID, hcs_id, AppointmentType)
		SELECT oID, hcs_id, AppointmentType FROM dbo.Housing_ARM_hcs_id_AppointmentTypes WITH (HOLDLOCK TABLOCKX)')
GO
SET IDENTITY_INSERT dbo.Tmp_Housing_ARM_hcs_id_AppointmentTypes OFF
GO
DROP TABLE dbo.Housing_ARM_hcs_id_AppointmentTypes
GO
EXECUTE sp_rename N'dbo.Tmp_Housing_ARM_hcs_id_AppointmentTypes', N'Housing_ARM_hcs_id_AppointmentTypes', 'OBJECT' 
GO
ALTER TABLE dbo.Housing_ARM_hcs_id_AppointmentTypes ADD CONSTRAINT
	PK_housing_arm_hcs_id_appoinment_types PRIMARY KEY CLUSTERED 
	(
	oID
	) WITH( STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
ALTER TABLE dbo.Housing_ARM_hcs_id_AppointmentTypes ADD CONSTRAINT
	FK_Housing_ARM_hcs_id_AppointmentTypes_Housing_ARM_hcs_ids FOREIGN KEY
	(
	hcs_id
	) REFERENCES dbo.Housing_ARM_hcs_ids
	(
	hcs_id
	) ON UPDATE  NO ACTION 
	 ON DELETE  CASCADE 
GO
DENY DELETE ON dbo.Housing_ARM_hcs_id_AppointmentTypes TO www_role  AS dbo 
GO
DENY INSERT ON dbo.Housing_ARM_hcs_id_AppointmentTypes TO www_role  AS dbo 
GO
DENY SELECT ON dbo.Housing_ARM_hcs_id_AppointmentTypes TO www_role  AS dbo 
GO
DENY UPDATE ON dbo.Housing_ARM_hcs_id_AppointmentTypes TO www_role  AS dbo 
GO
COMMIT
