EXECUTE UPDATE_DROP_CONSTRAINTS 'vendor_types'
GO
/* To prevent any potential data loss issues, you should review this script in detail before running it outside the context of the database designer.*/
BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION
GO
CREATE TABLE dbo.Tmp_vendor_types
	(
	oID varchar(2) NOT NULL,
	description dbo.typ_description NOT NULL,
	ActiveFlag bit NOT NULL,
	[Default] bit NOT NULL,
	created_by dbo.typ_counselor NOT NULL,
	date_created dbo.typ_date NOT NULL,
	ts timestamp NOT NULL
	)  ON [PRIMARY]
GO
ALTER TABLE dbo.Tmp_vendor_types SET (LOCK_ESCALATION = TABLE)
GO
GRANT DELETE ON dbo.Tmp_vendor_types TO public  AS dbo
GO
GRANT INSERT ON dbo.Tmp_vendor_types TO public  AS dbo
GO
GRANT SELECT ON dbo.Tmp_vendor_types TO public  AS dbo
GO
GRANT UPDATE ON dbo.Tmp_vendor_types TO public  AS dbo
GO
EXECUTE sp_bindefault N'dbo.default_true', N'dbo.Tmp_vendor_types.ActiveFlag'
GO
EXECUTE sp_bindefault N'dbo.default_false', N'dbo.Tmp_vendor_types.[Default]'
GO
IF EXISTS(SELECT * FROM SYSOBJECTS WHERE TYPE='U' AND NAME=N'vendor_types')
BEGIN
	EXEC('INSERT INTO dbo.Tmp_vendor_types (oID, description, ActiveFlag, [Default], created_by, date_created)
		  SELECT oID, description, ActiveFlag, [Default], created_by, date_created FROM dbo.vendor_types WITH (HOLDLOCK TABLOCKX)')
	EXEC('DROP TABLE dbo.vendor_types')
END
GO
EXECUTE sp_rename N'dbo.Tmp_vendor_types', N'vendor_types', 'OBJECT' 
GO
ALTER TABLE dbo.vendor_types ADD CONSTRAINT
	PK_vendor_types PRIMARY KEY CLUSTERED 
	(
	oID
	) WITH( STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
EXECUTE sp_bindrule N'dbo.valid_vendor_type', N'dbo.vendor_types.oID'
GO
COMMIT
BEGIN TRANSACTION
GO
ALTER TABLE dbo.vendors ADD CONSTRAINT
	FK_vendors_vendor_types FOREIGN KEY
	(
	Type
	) REFERENCES dbo.vendor_types
	(
	oID
	) ON UPDATE  NO ACTION 
	 ON DELETE  NO ACTION 
GO
DECLARE @v sql_variant 
SET @v = N'Link the type of the vendor to the vendor_types table entries'
EXECUTE sp_addextendedproperty N'MS_Description', @v, N'SCHEMA', N'dbo', N'TABLE', N'vendors', N'CONSTRAINT', N'FK_vendors_vendor_types'
GO
ALTER TABLE dbo.vendors SET (LOCK_ESCALATION = TABLE)
GO
COMMIT
