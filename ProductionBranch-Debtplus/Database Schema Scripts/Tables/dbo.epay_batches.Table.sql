USE [DebtPlus]
GO
/****** Object:  Table [dbo].[epay_batches]    Script Date: 09/15/2014 13:13:26 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[epay_batches](
	[epay_batch] [dbo].[typ_key] IDENTITY(1,1) NOT NULL,
	[epay_file] [dbo].[typ_key] NOT NULL,
	[epay_biller_id] [dbo].[typ_epay_biller] NOT NULL,
	[batch_contents] [varchar](10) NOT NULL,
	[control_id] [varchar](12) NOT NULL,
	[created_by] [dbo].[typ_counselor] NOT NULL,
	[date_created] [dbo].[typ_date] NOT NULL,
 CONSTRAINT [PK_epay_batches] PRIMARY KEY CLUSTERED 
(
	[epay_batch] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 90) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
