USE [DebtPlus]
GO
/****** Object:  Table [dbo].[creditor_contacts]    Script Date: 09/15/2014 13:13:26 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[creditor_contacts](
	[creditor_contact] [dbo].[typ_key] IDENTITY(1,1) NOT NULL,
	[creditor] [dbo].[typ_creditor] NOT NULL,
	[creditor_contact_type] [dbo].[typ_key] NOT NULL,
	[NameID] [int] NULL,
	[Title] [varchar](255) NULL,
	[TelephoneID] [int] NULL,
	[AltTelephoneID] [int] NULL,
	[AddressID] [int] NULL,
	[FAXID] [int] NULL,
	[EmailID] [int] NULL,
	[Notes] [dbo].[typ_message] NULL,
	[date_created] [dbo].[typ_date] NOT NULL,
	[created_by] [dbo].[typ_counselor] NOT NULL,
	[old_creditor_contact] [int] NULL,
 CONSTRAINT [PK_creditor_contacts] PRIMARY KEY CLUSTERED 
(
	[creditor_contact] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 90) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Primary key to the table. It is not specifically referenced in the database but is used by the programs to update this specific record.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'creditor_contacts', @level2type=N'COLUMN',@level2name=N'creditor_contact'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Pointer to the creditors table. This is the creditor ID.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'creditor_contacts', @level2type=N'COLUMN',@level2name=N'creditor'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Pointer to the creditor_contact_types table. This is the type of the contact' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'creditor_contacts', @level2type=N'COLUMN',@level2name=N'creditor_contact_type'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'If the contact has a title, it is kept here.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'creditor_contacts', @level2type=N'COLUMN',@level2name=N'Title'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'A general purpose note for the contact information. Comments are stored here.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'creditor_contacts', @level2type=N'COLUMN',@level2name=N'Notes'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Date/Time when the row was created.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'creditor_contacts', @level2type=N'COLUMN',@level2name=N'date_created'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Person who created the row.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'creditor_contacts', @level2type=N'COLUMN',@level2name=N'created_by'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'This table lists the creditor contact information associated with a specific creditor.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'creditor_contacts'
GO
