USE [DebtPlus]
GO
/****** Object:  Table [dbo].[Housing_AllowedVisitOutcomeTypes]    Script Date: 09/15/2014 13:13:26 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Housing_AllowedVisitOutcomeTypes](
	[oID] [dbo].[typ_key] IDENTITY(1,1) NOT NULL,
	[PurposeOfVisit] [dbo].[typ_key] NOT NULL,
	[Outcome] [dbo].[typ_key] NOT NULL,
	[HUD_9902_Section] [varchar](256) NOT NULL,
	[NFMCP_Section] [varchar](10) NOT NULL,
	[Default] [bit] NOT NULL,
	[old_hud_9902_section] [varchar](100) NULL,
 CONSTRAINT [PK_housing_allowedvisitoutcometypes] PRIMARY KEY CLUSTERED 
(
	[oID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
