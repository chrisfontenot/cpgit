USE [DebtPlus]
GO
/****** Object:  Table [dbo].[ClientAppointments]    Script Date: 09/15/2014 13:13:26 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[ClientAppointments](
	[oID] [uniqueidentifier] NOT NULL,
	[Client] [dbo].[typ_client] NULL,
	[StartDate] [datetime] NULL,
	[EndDate] [datetime] NULL,
	[Office] [dbo].[typ_key] NULL,
	[Status] [int] NULL,
	[Counselor] [dbo].[typ_key] NULL,
	[ReminderInfo] [text] NULL,
	[RecurrenceInfo] [text] NULL,
	[Description] [text] NULL,
	[AppointmentType] [dbo].[typ_key] NULL,
	[CallbackTelephone] [varchar](50) NULL,
	[isAllDay] [bit] NOT NULL,
	[isLocked] [bit] NOT NULL,
	[isPriority] [bit] NOT NULL,
	[Label] [int] NULL,
	[ConfirmStatus] [dbo].[typ_key] NULL,
	[ConfirmedBy] [varchar](80) NULL,
	[ConfirmedDate] [datetime] NULL,
	[ReferredBy] [dbo].[typ_key] NULL,
	[RecurringType] [int] NOT NULL,
	[BusyStatus] [varchar](50) NULL,
	[Subject] [varchar](50) NULL,
	[Location] [varchar](50) NULL,
 CONSTRAINT [PK_ClientAppointments] PRIMARY KEY CLUSTERED 
(
	[oID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
ALTER TABLE [dbo].[ClientAppointments] ADD  CONSTRAINT [DF_ClientAppointments_oID]  DEFAULT (newid()) FOR [oID]
GO
