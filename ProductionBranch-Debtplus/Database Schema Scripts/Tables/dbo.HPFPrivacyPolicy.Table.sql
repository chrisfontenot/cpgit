/* To prevent any potential data loss issues, you should review this script in detail before running it outside the context of the database designer.*/
BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION
GO
CREATE TABLE dbo.Tmp_HPFPrivacyPolicy
	(
	oID dbo.typ_key NOT NULL IDENTITY (1, 1),
	client dbo.typ_client NOT NULL,
	address varchar(1024) NULL,
	print_queue uniqueidentifier NULL,
	print_date datetime NULL,
	death_date datetime NULL,
	email_queueID dbo.typ_key NULL,
	date_created dbo.typ_date NOT NULL,
	ts timestamp NOT NULL
	)  ON [PRIMARY]
GO
GRANT DELETE ON dbo.Tmp_HPFPrivacyPolicy TO public  AS dbo
GO
GRANT INSERT ON dbo.Tmp_HPFPrivacyPolicy TO public  AS dbo
GO
GRANT SELECT ON dbo.Tmp_HPFPrivacyPolicy TO public  AS dbo
GO
GRANT UPDATE ON dbo.Tmp_HPFPrivacyPolicy TO public  AS dbo
GO
SET IDENTITY_INSERT dbo.Tmp_HPFPrivacyPolicy ON
GO
IF EXISTS(SELECT * FROM sysobjects WHERE name=N'HPFPrivacyPolicy' AND type='U')
BEGIN
	EXEC('INSERT INTO dbo.Tmp_HPFPrivacyPolicy (oID, client, address, print_queue, print_date, death_date, email_queueID, date_created)
		SELECT oID, client, address, print_queue, print_date, death_date, email_queueID, date_created FROM dbo.HPFPrivacyPolicy WITH (HOLDLOCK TABLOCKX)')
	EXEC('DROP TABLE dbo.HPFPrivacyPolicy')
END
GO
SET IDENTITY_INSERT dbo.Tmp_HPFPrivacyPolicy OFF
GO
EXECUTE sp_rename N'dbo.Tmp_HPFPrivacyPolicy', N'HPFPrivacyPolicy', 'OBJECT' 
GO
DENY DELETE ON dbo.HPFPrivacyPolicy TO www_role  AS dbo 
GO
DENY INSERT ON dbo.HPFPrivacyPolicy TO www_role  AS dbo 
GO
DENY SELECT ON dbo.HPFPrivacyPolicy TO www_role  AS dbo 
GO
DENY UPDATE ON dbo.HPFPrivacyPolicy TO www_role  AS dbo 
GO
COMMIT
