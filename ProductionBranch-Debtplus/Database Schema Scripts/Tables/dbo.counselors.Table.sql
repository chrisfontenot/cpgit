/* To prevent any potential data loss issues, you should review this script in detail before running it outside the context of the database designer.*/
BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION
GO
EXECUTE UPDATE_DROP_CONSTRAINTS 'counselors'
EXECUTE UPDATE_ADD_COLUMN 'counselors', 'Manager', 'int'
GO
CREATE TABLE dbo.Tmp_counselors
	(
	Counselor dbo.typ_key NOT NULL IDENTITY (1, 1),
	NameID int NULL,
	EmailID int NULL,
	TelephoneID int NULL,
	Person dbo.typ_counselor NOT NULL,
	Manager dbo.typ_key NULL,
	Note varchar(256) NULL,
	[Default] bit NOT NULL,
	ActiveFlag bit NOT NULL,
	Office dbo.typ_key NOT NULL,
	Menu_Level int NOT NULL,
	Color int NULL,
	Image image NULL,
	SSN dbo.typ_ssn NULL,
	billing_mode varchar(10) NULL,
	rate money NULL,
	emp_start_date datetime NOT NULL,
	emp_end_date datetime NULL,
	HUD_id int NULL,
	CaseCounter int NOT NULL,
	RxOfficeCounselorID varchar(50) NULL,
	date_created dbo.typ_date NOT NULL,
	created_by dbo.typ_counselor NOT NULL,
	ts timestamp NOT NULL
	)  ON [PRIMARY]
	 TEXTIMAGE_ON [PRIMARY]
GO
GRANT DELETE ON dbo.Tmp_counselors TO public  AS dbo
GO
GRANT INSERT ON dbo.Tmp_counselors TO public  AS dbo
GO
GRANT SELECT ON dbo.Tmp_counselors TO public  AS dbo
GO
GRANT UPDATE ON dbo.Tmp_counselors TO public  AS dbo
GO
DECLARE @v sql_variant 
SET @v = N'Counselor number. This is the primary key to the table.'
EXECUTE sp_addextendedproperty N'MS_Description', @v, N'SCHEMA', N'dbo', N'TABLE', N'Tmp_counselors', N'COLUMN', N'Counselor'
GO
DECLARE @v sql_variant 
SET @v = N'Account identifier for this person'
EXECUTE sp_addextendedproperty N'MS_Description', @v, N'SCHEMA', N'dbo', N'TABLE', N'Tmp_counselors', N'COLUMN', N'Person'
GO
DECLARE @v sql_variant 
SET @v = N'Is this the default counselor for the list of people?'
EXECUTE sp_addextendedproperty N'MS_Description', @v, N'SCHEMA', N'dbo', N'TABLE', N'Tmp_counselors', N'COLUMN', N'Default'
GO
DECLARE @v sql_variant 
SET @v = N'Pointer to the default office for this person'
EXECUTE sp_addextendedproperty N'MS_Description', @v, N'SCHEMA', N'dbo', N'TABLE', N'Tmp_counselors', N'COLUMN', N'Office'
GO
DECLARE @v sql_variant 
SET @v = N'A level associated with the person''s menu tree'
EXECUTE sp_addextendedproperty N'MS_Description', @v, N'SCHEMA', N'dbo', N'TABLE', N'Tmp_counselors', N'COLUMN', N'Menu_Level'
GO
DECLARE @v sql_variant 
SET @v = N'Date / Time created'
EXECUTE sp_addextendedproperty N'MS_Description', @v, N'SCHEMA', N'dbo', N'TABLE', N'Tmp_counselors', N'COLUMN', N'date_created'
GO
DECLARE @v sql_variant 
SET @v = N'Name of the person who created this row'
EXECUTE sp_addextendedproperty N'MS_Description', @v, N'SCHEMA', N'dbo', N'TABLE', N'Tmp_counselors', N'COLUMN', N'created_by'
GO
EXECUTE sp_bindefault N'dbo.default_false', N'dbo.Tmp_counselors.[Default]'
GO
EXECUTE sp_bindefault N'dbo.default_true', N'dbo.Tmp_counselors.ActiveFlag'
GO
EXECUTE sp_bindefault N'dbo.default_zero', N'dbo.Tmp_counselors.Office'
GO
EXECUTE sp_bindefault N'dbo.default_zero', N'dbo.Tmp_counselors.CaseCounter'
GO
EXECUTE sp_bindefault N'dbo.default_priority', N'dbo.Tmp_counselors.Menu_Level'
GO
EXECUTE sp_bindefault N'dbo.default_money', N'dbo.Tmp_counselors.rate'
GO
EXECUTE sp_bindefault N'dbo.default_date', N'dbo.Tmp_counselors.emp_start_date'
GO
SET IDENTITY_INSERT dbo.Tmp_counselors ON
GO
EXECUTE sp_unbindrule N'dbo.Tmp_counselors.SSN'
GO
IF EXISTS(SELECT * FROM dbo.counselors)
	 EXEC('INSERT INTO dbo.Tmp_counselors (Counselor, NameID, EmailID, TelephoneID, Person, Manager, Note, [Default], ActiveFlag, Office, Menu_Level, Color, Image, SSN, billing_mode, rate, emp_start_date, emp_end_date, HUD_id, date_created, created_by, CaseCounter, RxOfficeCounselorID)
		SELECT Counselor, NameID, EmailID, TelephoneID, Person, Manager, Note, [Default], ActiveFlag, Office, Menu_Level, Color, Image, SSN, billing_mode, rate, emp_start_date, emp_end_date, HUD_id, date_created, created_by, CaseCounter, RxOfficeCounselorID FROM dbo.counselors WITH (HOLDLOCK TABLOCKX)')
GO
SET IDENTITY_INSERT dbo.Tmp_counselors OFF
GO
DROP TABLE dbo.counselors
GO
EXECUTE sp_rename N'dbo.Tmp_counselors', N'counselors', 'OBJECT' 
GO
ALTER TABLE dbo.counselors ADD CONSTRAINT
	PK_counselors PRIMARY KEY NONCLUSTERED 
	(
	Counselor
	) WITH( STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
CREATE UNIQUE NONCLUSTERED INDEX IX_counselors_1 ON dbo.counselors
	(
	Person
	) WITH( STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX IX_counselors_2 ON dbo.counselors
	(
	Manager
	) WITH( STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
EXECUTE sp_bindrule N'dbo.valid_ssn', N'dbo.counselors.SSN'
GO
CREATE TRIGGER [dbo].[trig_counselors_D] on dbo.counselors AFTER DELETE as
begin
	-- ================================================================
	-- ==     Process the deletion of a counselor                    ==
	-- ================================================================

	set nocount on

	-- Discard the graphical scheduler appointment information
	delete	CounselorSchedules
	from	CounselorSchedules s
	inner join deleted d on s.Counselor = d.counselor

	-- Discard the client appointment items	
	delete	ClientAppointments
	from	ClientAppointments a
	inner join deleted d on a.counselor = d.counselor
	
	-- Discard the email references
	delete	EmailAddresses
	from	EmailAddresses i
	inner join deleted d on i.Email = d.EmailID
	
	-- Discard the Telephone references
	delete	TelephoneNumbers
	from	TelephoneNumbers i
	inner join deleted d on i.TelephoneNumber = d.TelephoneID
	
	-- Discard the name references
	delete	Names
	from	Names i
	inner join deleted d on i.Name = d.NameID

	-- Toss the appointment information
	delete	appt_counselor_templates
	from	appt_counselor_templates i
	inner join deleted d on i.counselor = d.Counselor
	
	delete	appt_counselors
	from	appt_counselors i
	inner join deleted d on i.counselor = d.Counselor
	
	-- Clear the counselors from the client appointments
	update	client_appointments
	set		counselor = null
	from	client_appointments i
	inner join deleted d on i.counselor = d.Counselor

	-- Correct the client statements
	update	client_statement_clients
	set		counselor = null
	from	client_statement_clients i
	inner join deleted d on i.counselor = d.Counselor
	
	-- Clear the counselors from the clients
	update	clients
	set		counselor		= null
	from	clients i
	inner join deleted d on i.counselor = d.Counselor

	-- Remove the reference to the csr as well
	update	clients
	set		csr		= null
	from	clients i
	inner join deleted d on i.csr = d.Counselor
		
	-- Remove the attributes
	delete	counselor_attributes
	from	counselor_attributes i
	inner join deleted d on i.counselor = d.Counselor

	-- Remove the training
	delete	counselor_training
	from	counselor_training i
	inner join deleted d on i.counselor = d.Counselor
	
	-- Remove the disbursement information
	update	disbursement_clients
	set		counselor		= null
	from	disbursement_clients i
	inner join deleted d on i.counselor = d.Counselor
	
	-- Remove the office information
	update	offices
	set		counselor		= null
	from	offices i
	inner join deleted d on i.counselor = d.Counselor
	
	-- Discard the statistics information
	delete	statistics_counselors
	from	statistics_counselors i
	inner join deleted d on i.counselor = d.Counselor
	
	-- Discard the tickler events
	delete	ticklers
	from	ticklers i
	inner join deleted d on i.counselor = d.Counselor

	-- Discard the workshops
	delete	workshops
	from	workshops i
	inner join deleted d on i.counselor = d.Person
	
	-- Discard the manager information.
	update	counselors
	set		Manager = null
	from	counselors i
	inner join deleted d on i.Manager = d.Counselor
end
GO
DENY DELETE ON dbo.counselors TO www_role  AS dbo 
GO
DENY INSERT ON dbo.counselors TO www_role  AS dbo 
GO
DENY SELECT ON dbo.counselors TO www_role  AS dbo 
GO
DENY UPDATE ON dbo.counselors TO www_role  AS dbo 
GO
COMMIT
