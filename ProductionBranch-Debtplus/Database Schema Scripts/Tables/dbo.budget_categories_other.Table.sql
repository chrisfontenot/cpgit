EXECUTE UPDATE_DROP_CONSTRAINTS 'budget_categories_other'
GO
/* To prevent any potential data loss issues, you should review this script in detail before running it outside the context of the database designer.*/
BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION
GO
CREATE TABLE dbo.Tmp_budget_categories_other
	(
	budget_category dbo.typ_key NOT NULL IDENTITY (10000, 1),
	budget_category_group dbo.typ_key NULL,
	client dbo.typ_client NOT NULL,
	description dbo.typ_description NOT NULL,
	date_created dbo.typ_date NOT NULL,
	created_by dbo.typ_counselor NOT NULL,
	ts timestamp NOT NULL
	)  ON [PRIMARY]
GO
GRANT DELETE ON dbo.Tmp_budget_categories_other TO public  AS dbo
GO
GRANT INSERT ON dbo.Tmp_budget_categories_other TO public  AS dbo
GO
GRANT SELECT ON dbo.Tmp_budget_categories_other TO public  AS dbo
GO
GRANT UPDATE ON dbo.Tmp_budget_categories_other TO public  AS dbo
GO
DECLARE @v sql_variant 
SET @v = N'If a client wants a specific budget category, the information concerning the additional budget category is stored in this table.'
EXECUTE sp_addextendedproperty N'MS_Description', @v, N'SCHEMA', N'dbo', N'TABLE', N'Tmp_budget_categories_other', NULL, NULL
GO
DECLARE @v sql_variant 
SET @v = N'Budget category for the client'
EXECUTE sp_addextendedproperty N'MS_Description', @v, N'SCHEMA', N'dbo', N'TABLE', N'Tmp_budget_categories_other', N'COLUMN', N'budget_category'
GO
DECLARE @v sql_variant 
SET @v = N'Client ID associated with this budget category'
EXECUTE sp_addextendedproperty N'MS_Description', @v, N'SCHEMA', N'dbo', N'TABLE', N'Tmp_budget_categories_other', N'COLUMN', N'client'
GO
DECLARE @v sql_variant 
SET @v = N'Description of the budget category for the client'
EXECUTE sp_addextendedproperty N'MS_Description', @v, N'SCHEMA', N'dbo', N'TABLE', N'Tmp_budget_categories_other', N'COLUMN', N'description'
GO
DECLARE @v sql_variant 
SET @v = N'Date/Time when the row was created.'
EXECUTE sp_addextendedproperty N'MS_Description', @v, N'SCHEMA', N'dbo', N'TABLE', N'Tmp_budget_categories_other', N'COLUMN', N'date_created'
GO
DECLARE @v sql_variant 
SET @v = N'Person who created the row.'
EXECUTE sp_addextendedproperty N'MS_Description', @v, N'SCHEMA', N'dbo', N'TABLE', N'Tmp_budget_categories_other', N'COLUMN', N'created_by'
GO
SET IDENTITY_INSERT dbo.Tmp_budget_categories_other ON
GO
IF EXISTS(SELECT * FROM dbo.budget_categories_other)
	 EXEC('INSERT INTO dbo.Tmp_budget_categories_other (budget_category, budget_category_group, client, description, date_created, created_by)
		SELECT budget_category, budget_category_group, client, description, date_created, created_by FROM dbo.budget_categories_other WITH (HOLDLOCK TABLOCKX)')
GO
SET IDENTITY_INSERT dbo.Tmp_budget_categories_other OFF
GO
DROP TABLE dbo.budget_categories_other
GO
EXECUTE sp_rename N'dbo.Tmp_budget_categories_other', N'budget_categories_other', 'OBJECT' 
GO
ALTER TABLE dbo.budget_categories_other ADD CONSTRAINT
	PK_budget_categories_other PRIMARY KEY CLUSTERED 
	(
	budget_category
	) WITH( PAD_INDEX = OFF, FILLFACTOR = 90, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX IX_budget_categories_other_1 ON dbo.budget_categories_other
	(
	client
	) WITH( PAD_INDEX = OFF, FILLFACTOR = 90, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
CREATE TRIGGER [dbo].[trig_budget_categories_other_D] ON dbo.budget_categories_other AFTER DELETE AS

-- ====================================================================================
-- ==           Handle the cascade delete from the "other" budget categories to the  ==
-- ==           "other" budget detail.                                               ==
-- ====================================================================================

set nocount on

-- Remove the references to the budget category in the other tables
delete		budget_detail
from		budget_detail det
inner join	deleted d on det.budget_category = d.budget_category
where		d.budget_category >= 10000
and			det.budget is not null
GO
DENY DELETE ON dbo.budget_categories_other TO www_role  AS dbo 
GO
DENY INSERT ON dbo.budget_categories_other TO www_role  AS dbo 
GO
DENY SELECT ON dbo.budget_categories_other TO www_role  AS dbo 
GO
DENY UPDATE ON dbo.budget_categories_other TO www_role  AS dbo 
GO
COMMIT
GO
EXEC('DELETE FROM budget_categories_other WHERE client NOT IN (SELECT client FROM clients)')
EXEC('ALTER TABLE dbo.budget_categories_other ADD CONSTRAINT FK_budget_categories_other_clients FOREIGN KEY ( client ) REFERENCES dbo.clients ( client ) ON UPDATE NO ACTION ON DELETE CASCADE')
GO
