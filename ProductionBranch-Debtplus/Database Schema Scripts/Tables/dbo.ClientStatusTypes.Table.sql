USE [DebtPlus]
GO
/****** Object:  Table [dbo].[ClientStatusTypes]    Script Date: 09/15/2014 13:13:26 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[ClientStatusTypes](
	[client_status] [varchar](10) NOT NULL,
	[description] [varchar](50) NOT NULL,
	[ActiveFlag] [bit] NOT NULL,
	[Default] [bit] NOT NULL,
 CONSTRAINT [PK_ClientStatusTypes] PRIMARY KEY CLUSTERED 
(
	[client_status] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
