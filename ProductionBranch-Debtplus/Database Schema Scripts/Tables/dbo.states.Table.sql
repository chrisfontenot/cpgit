EXECUTE UPDATE_DROP_CONSTRAINTS 'states'
GO
EXECUTE UPDATE_ADD_COLUMN 'states', 'UseInWeb', 'bit'
GO
/* To prevent any potential data loss issues, you should review this script in detail before running it outside the context of the database designer.*/
BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION
GO
CREATE TABLE dbo.Tmp_states
	(
	state dbo.typ_key NOT NULL IDENTITY (1001, 1) NOT FOR REPLICATION,
	MailingCode varchar(4) NULL,
	Name dbo.typ_description NOT NULL,
	Country dbo.typ_key NOT NULL,
	TimeZoneID dbo.typ_key NOT NULL,
	USAFormat int NOT NULL,
	[Default] bit NOT NULL,
	ActiveFlag bit NOT NULL,
	UseInWeb bit NOT NULL,
	DoingBusiness bit NOT NULL,
	NegativeDebtWarning bit NOT NULL,
	AddressFormat varchar(256) NOT NULL,
	FIPS varchar(2) NULL,
	hud_9902 int NOT NULL,
	created_by varchar(80) NOT NULL,
	date_created datetime NOT NULL,
	ts timestamp NOT NULL
	)  ON [PRIMARY]
GO
ALTER TABLE dbo.Tmp_states SET (LOCK_ESCALATION = TABLE)
GO
GRANT DELETE ON dbo.Tmp_states TO public  AS dbo
GO
GRANT INSERT ON dbo.Tmp_states TO public  AS dbo
GO
GRANT SELECT ON dbo.Tmp_states TO public  AS dbo
GO
GRANT UPDATE ON dbo.Tmp_states TO public  AS dbo
GO
DECLARE @v sql_variant 
SET @v = N'Mailing code for state names. US format only. Normally two letters.'
EXECUTE sp_addextendedproperty N'MS_Description', @v, N'SCHEMA', N'dbo', N'TABLE', N'Tmp_states', N'COLUMN', N'MailingCode'
GO
DECLARE @v sql_variant 
SET @v = N'Descriptive name of the state'
EXECUTE sp_addextendedproperty N'MS_Description', @v, N'SCHEMA', N'dbo', N'TABLE', N'Tmp_states', N'COLUMN', N'Name'
GO
DECLARE @v sql_variant 
SET @v = N'Pointer to the country table for this state entry.'
EXECUTE sp_addextendedproperty N'MS_Description', @v, N'SCHEMA', N'dbo', N'TABLE', N'Tmp_states', N'COLUMN', N'Country'
GO
DECLARE @v sql_variant 
SET @v = N'IF this is a US state, this flag is set. It means that the postal codes are zipcode format.'
EXECUTE sp_addextendedproperty N'MS_Description', @v, N'SCHEMA', N'dbo', N'TABLE', N'Tmp_states', N'COLUMN', N'USAFormat'
GO
DECLARE @v sql_variant 
SET @v = N'Is this the default item?'
EXECUTE sp_addextendedproperty N'MS_Description', @v, N'SCHEMA', N'dbo', N'TABLE', N'Tmp_states', N'COLUMN', N'Default'
GO
DECLARE @v sql_variant 
SET @v = N'Can the value be selected and changed to this item?'
EXECUTE sp_addextendedproperty N'MS_Description', @v, N'SCHEMA', N'dbo', N'TABLE', N'Tmp_states', N'COLUMN', N'ActiveFlag'
GO
DECLARE @v sql_variant 
SET @v = N'List this state on the web page for a choice by a user'
EXECUTE sp_addextendedproperty N'MS_Description', @v, N'SCHEMA', N'dbo', N'TABLE', N'Tmp_states', N'COLUMN', N'UseInWeb'
GO
DECLARE @v sql_variant 
SET @v = N'How to format the last line of the address. {0} is the city, {1} is the Mailing code, {2} is the postal code, and {3} is the Country'
EXECUTE sp_addextendedproperty N'MS_Description', @v, N'SCHEMA', N'dbo', N'TABLE', N'Tmp_states', N'COLUMN', N'AddressFormat'
GO
DECLARE @v sql_variant 
SET @v = N'Used in reporting the locality to H.U.D.'
EXECUTE sp_addextendedproperty N'MS_Description', @v, N'SCHEMA', N'dbo', N'TABLE', N'Tmp_states', N'COLUMN', N'hud_9902'
GO
DECLARE @v sql_variant 
SET @v = N'Name of the person who created the record'
EXECUTE sp_addextendedproperty N'MS_Description', @v, N'SCHEMA', N'dbo', N'TABLE', N'Tmp_states', N'COLUMN', N'created_by'
GO
DECLARE @v sql_variant 
SET @v = N'Date/Time when the record was created'
EXECUTE sp_addextendedproperty N'MS_Description', @v, N'SCHEMA', N'dbo', N'TABLE', N'Tmp_states', N'COLUMN', N'date_created'
GO
EXECUTE sp_bindefault N'dbo.default_true', N'dbo.Tmp_states.Country'
GO
EXECUTE sp_bindefault N'dbo.default_zero', N'dbo.Tmp_states.TimeZoneID'
GO
EXECUTE sp_bindefault N'dbo.default_false', N'dbo.Tmp_states.USAFormat'
GO
EXECUTE sp_bindefault N'dbo.default_false', N'dbo.Tmp_states.[Default]'
GO
EXECUTE sp_bindefault N'dbo.default_true', N'dbo.Tmp_states.ActiveFlag'
GO
EXECUTE sp_bindefault N'dbo.default_false', N'dbo.Tmp_states.UseInWeb'
GO
ALTER TABLE dbo.Tmp_states ADD CONSTRAINT
	DF_states_hud_9902 DEFAULT ((61)) FOR hud_9902
GO
EXECUTE sp_bindefault N'dbo.default_false', N'dbo.Tmp_states.DoingBusiness'
GO
EXECUTE sp_bindefault N'dbo.default_false', N'dbo.Tmp_states.NegativeDebtWarning'
GO
EXECUTE sp_bindefault N'dbo.default_counselor', N'dbo.Tmp_states.created_by'
GO
EXECUTE sp_bindefault N'dbo.default_date', N'dbo.Tmp_states.date_created'
GO
SET IDENTITY_INSERT dbo.Tmp_states ON
GO
IF EXISTS(SELECT * FROM dbo.states)
	 EXEC('INSERT INTO dbo.Tmp_states (state, MailingCode, Name, Country, TimeZoneID, USAFormat, [Default], ActiveFlag, UseInWeb, AddressFormat, FIPS, hud_9902, DoingBusiness, NegativeDebtWarning, created_by, date_created)
		SELECT state, MailingCode, Name, ISNULL(Country,1), ISNULL(TimeZoneID,1), ISNULL(USAFormat,1), ISNULL([Default],0), ISNULL(ActiveFlag,1), ISNULL(UseInWeb,1), ISNULL(AddressFormat,''''), FIPS, ISNULL(hud_9902,0), ISNULL(DoingBusiness,1), ISNULL(NegativeDebtWarning,0), created_by, date_created FROM dbo.states WITH (HOLDLOCK TABLOCKX)')
GO
SET IDENTITY_INSERT dbo.Tmp_states OFF
GO
DROP TABLE dbo.states
GO
EXECUTE sp_rename N'dbo.Tmp_states', N'states', 'OBJECT' 
GO
ALTER TABLE dbo.states ADD CONSTRAINT
	PK_states PRIMARY KEY CLUSTERED 
	(
	state
	) WITH( STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]

GO
CREATE NONCLUSTERED INDEX IX_states_1 ON dbo.states
	(
	MailingCode
	) WITH( STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
ALTER TABLE dbo.states ADD CONSTRAINT
	FK_states_countries FOREIGN KEY
	(
	Country
	) REFERENCES dbo.countries
	(
	country
	) ON UPDATE  NO ACTION 
	 ON DELETE  NO ACTION 
GO
ALTER TABLE dbo.states ADD CONSTRAINT
	FK_states_TimeZones FOREIGN KEY
	(
	TimeZoneID
	) REFERENCES dbo.TimeZones
	(
	oID
	) ON UPDATE  NO ACTION 
	 ON DELETE  NO ACTION 
GO
DENY DELETE ON dbo.states TO www_role  AS dbo 
GO
DENY INSERT ON dbo.states TO www_role  AS dbo 
GO
DENY SELECT ON dbo.states TO www_role  AS dbo 
GO
DENY UPDATE ON dbo.states TO www_role  AS dbo 
GO
COMMIT
BEGIN TRANSACTION
GO
ALTER TABLE dbo.StateMessages ADD CONSTRAINT
	FK_StateMessages_states FOREIGN KEY
	(
	State
	) REFERENCES dbo.states
	(
	state
	) ON UPDATE  CASCADE 
	 ON DELETE  CASCADE 
GO
ALTER TABLE dbo.StateMessages SET (LOCK_ESCALATION = TABLE)
GO
COMMIT
BEGIN TRANSACTION
GO
ALTER TABLE dbo.ZipCodeSearch ADD CONSTRAINT
	FK_ZipCodeSearch_states FOREIGN KEY
	(
	State
	) REFERENCES dbo.states
	(
	state
	) ON UPDATE  CASCADE 
	 ON DELETE  NO ACTION 
GO
ALTER TABLE dbo.ZipCodeSearch SET (LOCK_ESCALATION = TABLE)
GO
COMMIT
BEGIN TRANSACTION
GO
ALTER TABLE dbo.StateSetupFees ADD CONSTRAINT
	FK_StateSetupFees_states FOREIGN KEY
	(
	state
	) REFERENCES dbo.states
	(
	state
	) ON UPDATE  NO ACTION 
	 ON DELETE  CASCADE 
GO
DECLARE @v sql_variant 
SET @v = N'Link StateSetupFees.state to States.state'
EXECUTE sp_addextendedproperty N'MS_Description', @v, N'SCHEMA', N'dbo', N'TABLE', N'StateSetupFees', N'CONSTRAINT', N'FK_StateSetupFees_states'
GO
ALTER TABLE dbo.StateSetupFees SET (LOCK_ESCALATION = TABLE)
GO
COMMIT
BEGIN TRANSACTION
GO
ALTER TABLE dbo.DocumentAttributes ADD CONSTRAINT
	FK_DocumentAttributes_states FOREIGN KEY
	(
	State
	) REFERENCES dbo.states
	(
	state
	) ON UPDATE  NO ACTION 
	 ON DELETE  CASCADE 
GO
ALTER TABLE dbo.DocumentAttributes SET (LOCK_ESCALATION = TABLE)
GO
COMMIT
BEGIN TRANSACTION
GO
ALTER TABLE dbo.addresses ADD CONSTRAINT
	FK_Addresses_States FOREIGN KEY
	(
	state
	) REFERENCES dbo.states
	(
	state
	) ON UPDATE  CASCADE 
	 ON DELETE  NO ACTION 
GO
ALTER TABLE dbo.addresses SET (LOCK_ESCALATION = TABLE)
GO
COMMIT
BEGIN TRANSACTION
GO
ALTER TABLE dbo.config_fees ADD CONSTRAINT
	FK_config_fees_states FOREIGN KEY
	(
	state
	) REFERENCES dbo.states
	(
	state
	) ON UPDATE  CASCADE 
	 ON DELETE  CASCADE 
GO
ALTER TABLE dbo.config_fees SET (LOCK_ESCALATION = TABLE)
GO
COMMIT
BEGIN TRANSACTION
GO
ALTER TABLE dbo.product_states ADD CONSTRAINT
	FK_product_states_states FOREIGN KEY
	(
	stateID
	) REFERENCES dbo.states
	(
	state
	) ON UPDATE  CASCADE 
	 ON DELETE  CASCADE 
GO
ALTER TABLE dbo.product_states SET (LOCK_ESCALATION = TABLE)
GO
COMMIT
GO
-- Set the flag for the web list. These are the 50 US states plus District of Columbia.
update states set useinweb = 0
update states set useinweb = 1 where mailingcode in ('AL','AK','AZ','AR','CA','CO','CT','DE','DC','FL','GA','HI','ID','IL','IN','IA','KS','KY','LA','ME','MD','MA','MI','MN','MS','MO','MT','NE','NV','NH','NJ','NM','NY','NC','ND','OH','OK','OR','PA','RI','SC','SD','TN','TX','UT','VT','VA','WA','WV','WI','WY')
GO
