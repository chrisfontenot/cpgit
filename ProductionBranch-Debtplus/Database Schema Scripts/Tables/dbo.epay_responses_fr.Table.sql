USE [DebtPlus]
GO
/****** Object:  Table [dbo].[epay_responses_fr]    Script Date: 09/15/2014 13:13:26 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[epay_responses_fr](
	[epay_response_fr] [dbo].[typ_key] IDENTITY(1,1) NOT NULL,
	[epay_response_file] [int] NOT NULL,
	[transaction_id] [varchar](27) NULL,
	[epay_transaction] [dbo].[typ_key] NULL,
	[error] [varchar](50) NULL,
	[sending_id] [varchar](12) NULL,
	[receiving_id] [varchar](12) NULL,
	[message] [varchar](300) NOT NULL,
	[local_biller_id] [varchar](12) NULL,
	[created_by] [dbo].[typ_counselor] NOT NULL,
	[date_created] [dbo].[typ_date] NOT NULL,
 CONSTRAINT [PK_epay_responses_fr] PRIMARY KEY CLUSTERED 
(
	[epay_response_fr] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 90) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
