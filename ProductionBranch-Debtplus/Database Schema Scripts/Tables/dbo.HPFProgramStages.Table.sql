/* To prevent any potential data loss issues, you should review this script in detail before running it outside the context of the database designer.*/
BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION
GO
CREATE TABLE dbo.Tmp_HPFProgramStages
	(
	oID dbo.typ_key NOT NULL IDENTITY (1, 1),
	name varchar(50) NOT NULL,
	comment dbo.typ_message NULL,
	description dbo.typ_description NOT NULL,
	ActiveFlag bit NOT NULL,
	ts timestamp NOT NULL,
	hpf varchar(50) NULL
	)  ON [PRIMARY]
GO
GRANT DELETE ON dbo.Tmp_HPFProgramStages TO public  AS dbo
GO
GRANT INSERT ON dbo.Tmp_HPFProgramStages TO public  AS dbo
GO
GRANT SELECT ON dbo.Tmp_HPFProgramStages TO public  AS dbo
GO
GRANT UPDATE ON dbo.Tmp_HPFProgramStages TO public  AS dbo
GO
EXECUTE sp_bindefault N'dbo.default_true', N'dbo.Tmp_HPFProgramStages.ActiveFlag'
GO
SET IDENTITY_INSERT dbo.Tmp_HPFProgramStages ON
GO
IF EXISTS(SELECT * FROM sysobjects WHERE name=N'HPFProgramStages' AND type='U')
BEGIN
	EXEC('INSERT INTO dbo.Tmp_HPFProgramStages (oID, name, comment, description, ActiveFlag, hpf)
		SELECT oID, name, comment, description, ActiveFlag, hpf FROM dbo.HPFProgramStages WITH (HOLDLOCK TABLOCKX)')
	EXEC('DROP TABLE dbo.HPFProgramStages')
END
GO
SET IDENTITY_INSERT dbo.Tmp_HPFProgramStages OFF
GO
EXECUTE sp_rename N'dbo.Tmp_HPFProgramStages', N'HPFProgramStages', 'OBJECT' 
GO
ALTER TABLE dbo.HPFProgramStages ADD CONSTRAINT
	PK_HPFProgramStages PRIMARY KEY CLUSTERED 
	(
	oID
	) WITH( STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
DENY DELETE ON dbo.HPFProgramStages TO www_role  AS dbo 
GO
DENY INSERT ON dbo.HPFProgramStages TO www_role  AS dbo 
GO
DENY SELECT ON dbo.HPFProgramStages TO www_role  AS dbo 
GO
DENY UPDATE ON dbo.HPFProgramStages TO www_role  AS dbo 
GO
COMMIT
GO
