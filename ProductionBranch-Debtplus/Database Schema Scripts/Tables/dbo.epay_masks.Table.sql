USE [DebtPlus]
GO
/****** Object:  Table [dbo].[epay_masks]    Script Date: 09/15/2014 13:13:26 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[epay_masks](
	[epay_mask] [int] IDENTITY(1,1) NOT NULL,
	[biller_id] [varchar](255) NOT NULL,
	[mask] [varchar](255) NOT NULL,
 CONSTRAINT [PK_epay_masks] PRIMARY KEY CLUSTERED 
(
	[epay_mask] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 90) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
