USE [DebtPlus]
GO
/****** Object:  Table [dbo].[hud_interviews]    Script Date: 09/15/2014 13:13:26 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[hud_interviews](
	[hud_interview] [dbo].[typ_key] IDENTITY(1,1) NOT NULL,
	[client] [dbo].[typ_client] NOT NULL,
	[HUD_Grant] [dbo].[typ_key] NOT NULL,
	[HousingFeeAmount] [money] NOT NULL,
	[interview_type] [dbo].[typ_key] NOT NULL,
	[interview_date] [dbo].[typ_date] NOT NULL,
	[interview_counselor] [dbo].[typ_counselor] NOT NULL,
	[hud_result] [dbo].[typ_key] NULL,
	[result_date] [datetime] NULL,
	[result_counselor] [varchar](80) NULL,
	[termination_reason] [dbo].[typ_key] NULL,
	[termination_date] [datetime] NULL,
	[termination_counselor] [varchar](80) NULL,
	[old_hud_interview] [int] NULL,
 CONSTRAINT [PK_hud_interviews] PRIMARY KEY CLUSTERED 
(
	[hud_interview] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Primary key to the table. This is referenced in the details table for the transactions against this item.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'hud_interviews', @level2type=N'COLUMN',@level2name=N'hud_interview'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Client ID associated with the item.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'hud_interviews', @level2type=N'COLUMN',@level2name=N'client'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Type of the interview' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'hud_interviews', @level2type=N'COLUMN',@level2name=N'interview_type'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Date and time when the interview was created.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'hud_interviews', @level2type=N'COLUMN',@level2name=N'interview_date'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Name of the counselor who created the interview' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'hud_interviews', @level2type=N'COLUMN',@level2name=N'interview_counselor'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Result associated with this interview' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'hud_interviews', @level2type=N'COLUMN',@level2name=N'hud_result'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Date and time when the result was updated.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'hud_interviews', @level2type=N'COLUMN',@level2name=N'result_date'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Name of the counselor who updated the result' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'hud_interviews', @level2type=N'COLUMN',@level2name=N'result_counselor'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Information associated with the HUD interviews attached to clients. It lists the type of the interview and its associated result.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'hud_interviews'
GO
