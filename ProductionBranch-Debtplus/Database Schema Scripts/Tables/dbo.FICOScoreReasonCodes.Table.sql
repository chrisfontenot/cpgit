execute UPDATE_DROP_CONSTRAINTS 'FICOScoreReasonCodes'
GO
/* To prevent any potential data loss issues, you should review this script in detail before running it outside the context of the database designer.*/
BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION
GO
CREATE TABLE dbo.Tmp_FICOScoreReasonCodes
	(
	oID dbo.typ_key NOT NULL IDENTITY(1,1),
	description varchar(50) NOT NULL,
	ActiveFlag bit NOT NULL,
	[Default] bit NOT NULL,
	long_description varchar(2048) NULL,
	tip varchar(2048) NULL,
	Equifax int NULL,
	date_created dbo.typ_date NOT NULL,
	created_by dbo.typ_counselor NOT NULL,
	ts timestamp NOT NULL
	)  ON [PRIMARY]
GO
GRANT DELETE ON dbo.Tmp_FICOScoreReasonCodes TO public  AS dbo
GO
GRANT INSERT ON dbo.Tmp_FICOScoreReasonCodes TO public  AS dbo
GO
GRANT SELECT ON dbo.Tmp_FICOScoreReasonCodes TO public  AS dbo
GO
GRANT UPDATE ON dbo.Tmp_FICOScoreReasonCodes TO public  AS dbo
GO
EXECUTE sp_bindefault N'dbo.default_true', N'dbo.Tmp_FICOScoreReasonCodes.ActiveFlag'
GO
EXECUTE sp_bindefault N'dbo.default_false', N'dbo.Tmp_FICOScoreReasonCodes.[Default]'
GO
SET IDENTITY_INSERT Tmp_FICOScoreReasonCodes ON;
GO
IF EXISTS(SELECT * FROM SYSOBJECTS WHERE TYPE='U' AND NAME=N'FICOScoreReasonCodes')
BEGIN
	EXEC('INSERT INTO dbo.Tmp_FICOScoreReasonCodes (oID, description, ActiveFlag, [Default], long_description, tip, Equifax, date_created, created_by)
		SELECT oID, description, ActiveFlag, [Default], long_description, tip, EquiFax, date_created, created_by FROM dbo.FICOScoreReasonCodes WITH (HOLDLOCK TABLOCKX)')
	EXEC('DROP TABLE dbo.FICOScoreReasonCodes')
END
GO
SET IDENTITY_INSERT Tmp_FICOScoreReasonCodes OFF;
GO
EXECUTE sp_rename N'dbo.Tmp_FICOScoreReasonCodes', N'FICOScoreReasonCodes', 'OBJECT' 
GO
ALTER TABLE dbo.FICOScoreReasonCodes ADD CONSTRAINT PK_FICOScoreReasonCodes PRIMARY KEY CLUSTERED 
	(
	oID
	) WITH( STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX IX_FICOScoreReasonCodes_1 ON dbo.FICOScoreReasonCodes
	(
	EquiFax
	) WITH( STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
DENY INSERT,SELECT,UPDATE,DELETE ON dbo.FICOScoreReasonCodes TO www_role  AS dbo 
GO
COMMIT
BEGIN TRANSACTION
GO
ALTER TABLE dbo.PeopleFICOScoreReasons ADD CONSTRAINT
	FK_PeopleFICOScoreReasons_FICOScoreReasonCodes FOREIGN KEY
	(
	ScoreReasonID
	) REFERENCES dbo.FICOScoreReasonCodes
	(
	oID
	) ON UPDATE  CASCADE 
	 ON DELETE  CASCADE 
GO
COMMIT
