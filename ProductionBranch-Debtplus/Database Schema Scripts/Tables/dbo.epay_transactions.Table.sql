USE [DebtPlus]
GO
/****** Object:  Table [dbo].[epay_transactions]    Script Date: 09/15/2014 13:13:26 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[epay_transactions](
	[epay_transaction] [dbo].[typ_key] IDENTITY(1,1) NOT NULL,
	[epay_file] [int] NULL,
	[client] [dbo].[typ_client] NULL,
	[creditor] [dbo].[typ_creditor] NULL,
	[client_creditor] [dbo].[typ_key] NULL,
	[creditor_number] [int] NULL,
	[request] [char](1) NOT NULL,
	[trace_number] [varchar](21) NULL,
	[epay_biller_id] [dbo].[typ_epay_biller] NULL,
	[account_number] [varchar](50) NULL,
	[client_creditor_proposal] [dbo].[typ_key] NULL,
	[debt_note] [dbo].[typ_key] NULL,
	[created_by] [dbo].[typ_counselor] NOT NULL,
	[date_created] [dbo].[typ_date] NOT NULL,
 CONSTRAINT [PK_epay_transactions] PRIMARY KEY CLUSTERED 
(
	[epay_transaction] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 90) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Pointer to the associated debt' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'epay_transactions', @level2type=N'COLUMN',@level2name=N'client_creditor'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Type of the transaciton.  1 = proposals, 2 = balance verification, 3 = drop. (4 is not valid here.)' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'epay_transactions', @level2type=N'COLUMN',@level2name=N'request'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'If drop or proposal, this is a pointer to the message text' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'epay_transactions', @level2type=N'COLUMN',@level2name=N'debt_note'
GO
ALTER TABLE [dbo].[epay_transactions] ADD  CONSTRAINT [DF_epay_transactions_request]  DEFAULT ('1') FOR [request]
GO
