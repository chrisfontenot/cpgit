ALTER TABLE dbo.client_disclosures_CBR DROP CONSTRAINT FK_client_disclosures_CBR_client_disclosures
GO
/* To prevent any potential data loss issues, you should review this script in detail before running it outside the context of the database designer.*/
BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION
GO
CREATE TABLE dbo.Tmp_client_disclosures_CBR
	(
	oID dbo.typ_key NOT NULL IDENTITY (1, 1),
	client_disclosure_ID dbo.typ_key NOT NULL,
	applicant int NOT NULL,
	coapplicant int NOT NULL,
	birthplace varchar(50) NOT NULL,
	ts timestamp NOT NULL
	)  ON [PRIMARY]
GO
EXECUTE sp_bindefault N'dbo.default_zero', N'dbo.Tmp_client_disclosures_CBR.applicant'
GO
EXECUTE sp_bindefault N'dbo.default_zero', N'dbo.Tmp_client_disclosures_CBR.coapplicant'
GO
EXECUTE sp_bindefault N'dbo.default_string', N'dbo.Tmp_client_disclosures_CBR.birthplace'
GO
SET IDENTITY_INSERT dbo.Tmp_client_disclosures_CBR ON
GO
IF EXISTS(SELECT * FROM SYSOBJECTS WHERE TYPE='U' AND NAME=N'client_disclosures_CBR')
BEGIN
	EXEC('INSERT INTO dbo.Tmp_client_disclosures_CBR (oID, client_disclosure_ID, applicant, coapplicant, birthplace) SELECT oID, client_disclosure_ID, applicant, coapplicant, birthplace FROM dbo.client_disclosures_CBR WITH (HOLDLOCK TABLOCKX)')
	EXEC('DROP TABLE dbo.client_disclosures_CBR')
END
GO
SET IDENTITY_INSERT dbo.Tmp_client_disclosures_CBR OFF
GO
EXECUTE sp_rename N'dbo.Tmp_client_disclosures_CBR', N'client_disclosures_CBR', 'OBJECT' 
GO
ALTER TABLE dbo.client_disclosures_CBR ADD CONSTRAINT
	PK_client_disclosures_CBR PRIMARY KEY CLUSTERED 
	(
	oID
	) ON [PRIMARY]
GO
COMMIT
GO
ALTER TABLE dbo.client_disclosures_CBR ADD CONSTRAINT
	FK_client_disclosures_CBR_client_disclosures FOREIGN KEY
	(
	client_disclosure_ID
	) REFERENCES dbo.client_disclosures
	(
	oID
	) ON UPDATE  CASCADE 
	 ON DELETE  CASCADE 
GO
DECLARE @v sql_variant 
SET @v = N'One-to-One relationship'
EXECUTE sp_addextendedproperty N'MS_Description', @v, N'SCHEMA', N'dbo', N'TABLE', N'client_disclosures_CBR', N'CONSTRAINT', N'FK_client_disclosures_CBR_client_disclosures'
GO
