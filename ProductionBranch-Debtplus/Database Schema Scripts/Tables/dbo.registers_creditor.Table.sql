/* To prevent any potential data loss issues, you should review this script in detail before running it outside the context of the database designer.*/
BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION
GO
CREATE TABLE dbo.Tmp_registers_creditor
	(
	creditor_register dbo.typ_key NOT NULL IDENTITY (1, 1),
	tran_type dbo.typ_transaction NOT NULL,
	creditor dbo.typ_creditor NOT NULL,
	item_date datetime NULL,
	fairshare_pct float(53) NULL,
	disbursement_register dbo.typ_key NULL,
	trust_register dbo.typ_key NULL,
	invoice_register dbo.typ_key NULL,
	debit_amt money NULL,
	credit_amt money NULL,
	message dbo.typ_message NULL,
	date_created dbo.typ_date NOT NULL,
	created_by dbo.typ_counselor NOT NULL,
	ts timestamp NOT NULL
	)  ON [PRIMARY]
GO
GRANT DELETE ON dbo.Tmp_registers_creditor TO public  AS dbo
GO
GRANT INSERT ON dbo.Tmp_registers_creditor TO public  AS dbo
GO
GRANT SELECT ON dbo.Tmp_registers_creditor TO public  AS dbo
GO
GRANT UPDATE ON dbo.Tmp_registers_creditor TO public  AS dbo
GO
DECLARE @v sql_variant 
SET @v = N'This table holds all of the transactions that relate to creditor payments'
EXECUTE sp_addextendedproperty N'MS_Description', @v, N'SCHEMA', N'dbo', N'TABLE', N'Tmp_registers_creditor', NULL, NULL
GO
DECLARE @v sql_variant 
SET @v = N'Primary key to the table'
EXECUTE sp_addextendedproperty N'MS_Description', @v, N'SCHEMA', N'dbo', N'TABLE', N'Tmp_registers_creditor', N'COLUMN', N'creditor_register'
GO
DECLARE @v sql_variant 
SET @v = N'Type of the transaction'
EXECUTE sp_addextendedproperty N'MS_Description', @v, N'SCHEMA', N'dbo', N'TABLE', N'Tmp_registers_creditor', N'COLUMN', N'tran_type'
GO
DECLARE @v sql_variant 
SET @v = N'Creditor ID associated with the transaction'
EXECUTE sp_addextendedproperty N'MS_Description', @v, N'SCHEMA', N'dbo', N'TABLE', N'Tmp_registers_creditor', N'COLUMN', N'creditor'
GO
DECLARE @v sql_variant 
SET @v = N'Date for a refund check, etc.'
EXECUTE sp_addextendedproperty N'MS_Description', @v, N'SCHEMA', N'dbo', N'TABLE', N'Tmp_registers_creditor', N'COLUMN', N'item_date'
GO
DECLARE @v sql_variant 
SET @v = N'Pointer to the registers_disbursement table for a disbursement transaction'
EXECUTE sp_addextendedproperty N'MS_Description', @v, N'SCHEMA', N'dbo', N'TABLE', N'Tmp_registers_creditor', N'COLUMN', N'disbursement_register'
GO
DECLARE @v sql_variant 
SET @v = N'Pointer to the registers_trust for a check related transaction'
EXECUTE sp_addextendedproperty N'MS_Description', @v, N'SCHEMA', N'dbo', N'TABLE', N'Tmp_registers_creditor', N'COLUMN', N'trust_register'
GO
DECLARE @v sql_variant 
SET @v = N'Pointer to the registers_invoice table for fairshare invoice related transactions'
EXECUTE sp_addextendedproperty N'MS_Description', @v, N'SCHEMA', N'dbo', N'TABLE', N'Tmp_registers_creditor', N'COLUMN', N'invoice_register'
GO
DECLARE @v sql_variant 
SET @v = N'Amount for debit transactions on the creditor debts'
EXECUTE sp_addextendedproperty N'MS_Description', @v, N'SCHEMA', N'dbo', N'TABLE', N'Tmp_registers_creditor', N'COLUMN', N'debit_amt'
GO
DECLARE @v sql_variant 
SET @v = N'Amount for credit amounts from the creditor to pay invoices'
EXECUTE sp_addextendedproperty N'MS_Description', @v, N'SCHEMA', N'dbo', N'TABLE', N'Tmp_registers_creditor', N'COLUMN', N'credit_amt'
GO
DECLARE @v sql_variant 
SET @v = N'General purpose message field for the transaction. It may contain items such as the creditor''s check number.'
EXECUTE sp_addextendedproperty N'MS_Description', @v, N'SCHEMA', N'dbo', N'TABLE', N'Tmp_registers_creditor', N'COLUMN', N'message'
GO
DECLARE @v sql_variant 
SET @v = N'Date/Time when the row was created.'
EXECUTE sp_addextendedproperty N'MS_Description', @v, N'SCHEMA', N'dbo', N'TABLE', N'Tmp_registers_creditor', N'COLUMN', N'date_created'
GO
DECLARE @v sql_variant 
SET @v = N'Person who created the row.'
EXECUTE sp_addextendedproperty N'MS_Description', @v, N'SCHEMA', N'dbo', N'TABLE', N'Tmp_registers_creditor', N'COLUMN', N'created_by'
GO
EXECUTE sp_bindefault N'dbo.default_money', N'dbo.Tmp_registers_creditor.debit_amt'
GO
EXECUTE sp_bindefault N'dbo.default_money', N'dbo.Tmp_registers_creditor.credit_amt'
GO
SET IDENTITY_INSERT dbo.Tmp_registers_creditor ON
GO
EXECUTE sp_unbindrule N'dbo.Tmp_registers_creditor.creditor'
GO
IF EXISTS(SELECT * FROM sysobjects WHERE name=N'registers_creditor' AND type='U')
BEGIN
	EXEC('INSERT INTO dbo.Tmp_registers_creditor (creditor_register, tran_type, creditor, item_date, fairshare_pct, disbursement_register, trust_register, invoice_register, debit_amt, credit_amt, message, date_created, created_by) SELECT creditor_register, tran_type, creditor, item_date, fairshare_pct, disbursement_register, trust_register, invoice_register, debit_amt, credit_amt, message, date_created, created_by FROM dbo.registers_creditor WITH (HOLDLOCK TABLOCKX)')
	EXEC('DROP TABLE dbo.registers_creditor')
END	
GO
SET IDENTITY_INSERT dbo.Tmp_registers_creditor OFF
GO
EXECUTE sp_rename N'dbo.Tmp_registers_creditor', N'registers_creditor', 'OBJECT' 
GO
ALTER TABLE dbo.registers_creditor ADD CONSTRAINT
	PK_registers_creditor PRIMARY KEY CLUSTERED 
	(
	creditor_register
	) WITH( PAD_INDEX = OFF, FILLFACTOR = 90, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX IX_registers_creditor_1 ON dbo.registers_creditor
	(
	tran_type,
	creditor,
	date_created
	) WITH( PAD_INDEX = OFF, FILLFACTOR = 90, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX IX_registers_creditor_2 ON dbo.registers_creditor
	(
	tran_type,
	invoice_register
	) WITH( PAD_INDEX = OFF, FILLFACTOR = 90, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX IX_registers_creditor_3 ON dbo.registers_creditor
	(
	tran_type,
	trust_register
	) WITH( PAD_INDEX = OFF, FILLFACTOR = 90, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX IX_registers_creditor_4 ON dbo.registers_creditor
	(
	invoice_register
	) WITH( STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX IX_registers_creditor_5 ON dbo.registers_creditor
	(
	creditor,
	tran_type
	) INCLUDE (trust_register, invoice_register) 
 WITH( STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX IX_registers_creditor_6 ON dbo.registers_creditor
	(
	creditor,
	date_created
	) INCLUDE (creditor_register, tran_type, trust_register, invoice_register, debit_amt, credit_amt, message) 
 WITH( STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
EXECUTE sp_bindrule N'dbo.valid_creditor', N'dbo.registers_creditor.creditor'
GO
DENY DELETE ON dbo.registers_creditor TO www_role  AS dbo 
GO
DENY INSERT ON dbo.registers_creditor TO www_role  AS dbo 
GO
DENY SELECT ON dbo.registers_creditor TO www_role  AS dbo 
GO
DENY UPDATE ON dbo.registers_creditor TO www_role  AS dbo 
GO
COMMIT
