USE [DebtPlus]
GO
/****** Object:  Table [dbo].[client_retention_events]    Script Date: 09/15/2014 13:13:26 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[client_retention_events](
	[client_retention_event] [dbo].[typ_key] IDENTITY(1,1) NOT NULL,
	[client] [dbo].[typ_client] NOT NULL,
	[retention_event] [dbo].[typ_key] NOT NULL,
	[amount] [money] NULL,
	[message] [dbo].[typ_message] NULL,
	[expire_type] [int] NOT NULL,
	[expire_date] [datetime] NULL,
	[priority] [int] NOT NULL,
	[effective_date] [datetime] NOT NULL,
	[date_expired] [datetime] NULL,
	[date_created] [dbo].[typ_date] NOT NULL,
	[created_by] [dbo].[typ_counselor] NOT NULL,
	[hud_interview] [int] NULL,
	[old_client_retention_event] [int] NULL,
 CONSTRAINT [PK_client_retention_events] PRIMARY KEY CLUSTERED 
(
	[client_retention_event] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Primary key to the table' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'client_retention_events', @level2type=N'COLUMN',@level2name=N'client_retention_event'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Client ID associated with the entry' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'client_retention_events', @level2type=N'COLUMN',@level2name=N'client'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Event to be generated for the follow-up' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'client_retention_events', @level2type=N'COLUMN',@level2name=N'retention_event'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Dollar amount associated with the followup event' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'client_retention_events', @level2type=N'COLUMN',@level2name=N'amount'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'A general purpose message associated with this event' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'client_retention_events', @level2type=N'COLUMN',@level2name=N'message'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Type of expiration. 1 = never, 2 = deposit of ''amount'', 3 = date from expire_date' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'client_retention_events', @level2type=N'COLUMN',@level2name=N'expire_type'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'If expire_type = 3, this is the date when the event expires' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'client_retention_events', @level2type=N'COLUMN',@level2name=N'expire_date'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Priority for this event in all system events' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'client_retention_events', @level2type=N'COLUMN',@level2name=N'priority'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'When the event is to be effective. It will show up in the processing on this date.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'client_retention_events', @level2type=N'COLUMN',@level2name=N'effective_date'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'If the item is expired, this is the date when it was expired' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'client_retention_events', @level2type=N'COLUMN',@level2name=N'date_expired'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Date/Time when the row was created.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'client_retention_events', @level2type=N'COLUMN',@level2name=N'date_created'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'This table holds the retention events generated for a client. Actions are posted against these events.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'client_retention_events'
GO
