USE [DebtPlus]
GO
/****** Object:  Table [dbo].[people_updates]    Script Date: 09/15/2014 13:13:26 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[people_updates](
	[ID] [dbo].[typ_key] IDENTITY(1,1) NOT NULL,
	[person] [dbo].[typ_key] NOT NULL,
	[field] [varchar](50) NOT NULL,
	[old_value] [varchar](50) NULL,
	[new_value] [varchar](50) NULL,
	[date_created] [dbo].[typ_date] NOT NULL,
	[created_by] [dbo].[typ_counselor] NOT NULL,
	[old_ID] [int] NULL,
 CONSTRAINT [PK_people_updates] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
