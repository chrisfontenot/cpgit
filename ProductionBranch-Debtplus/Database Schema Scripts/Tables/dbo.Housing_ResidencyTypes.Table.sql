/* To prevent any potential data loss issues, you should review this script in detail before running it outside the context of the database designer.*/
BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION
GO
EXECUTE UPDATE_ADD_COLUMN 'Housing_ResidencyTypes', 'hpf'
GO
CREATE TABLE dbo.Tmp_Housing_ResidencyTypes
	(
	oID dbo.typ_key NOT NULL IDENTITY (1, 1),
	description dbo.typ_description NOT NULL,
	[Default] bit NOT NULL,
	ActiveFlag bit NOT NULL,
	primaryResidence bit NOT NULL,
	ownerOccupied bit NOT NULL,
	vacantOrCondemned bit NOT NULL,
	hpf varchar(50) NULL,
	date_created dbo.typ_date NOT NULL,
	created_by dbo.typ_counselor NOT NULL,
	ts timestamp NOT NULL
	)  ON [PRIMARY]
GO
GRANT DELETE ON dbo.Tmp_Housing_ResidencyTypes TO public  AS dbo
GO
GRANT INSERT ON dbo.Tmp_Housing_ResidencyTypes TO public  AS dbo
GO
GRANT SELECT ON dbo.Tmp_Housing_ResidencyTypes TO public  AS dbo
GO
GRANT UPDATE ON dbo.Tmp_Housing_ResidencyTypes TO public  AS dbo
GO
EXECUTE sp_bindefault N'dbo.default_false', N'dbo.Tmp_Housing_ResidencyTypes.[Default]'
GO
EXECUTE sp_bindefault N'dbo.default_true', N'dbo.Tmp_Housing_ResidencyTypes.ActiveFlag'
GO
EXECUTE sp_bindefault N'dbo.default_false', N'dbo.Tmp_Housing_ResidencyTypes.primaryResidence'
GO
EXECUTE sp_bindefault N'dbo.default_false', N'dbo.Tmp_Housing_ResidencyTypes.ownerOccupied'
GO
EXECUTE sp_bindefault N'dbo.default_false', N'dbo.Tmp_Housing_ResidencyTypes.vacantOrCondemned'
GO
SET IDENTITY_INSERT dbo.Tmp_Housing_ResidencyTypes ON
GO
IF EXISTS(SELECT * FROM dbo.Housing_ResidencyTypes)
	 EXEC('INSERT INTO dbo.Tmp_Housing_ResidencyTypes (oID, description, [Default], ActiveFlag, primaryResidence, ownerOccupied, vacantOrCondemned, date_created, created_by, hpf)
		SELECT oID, description, [Default], ActiveFlag, primaryResidence, ownerOccupied, vacantOrCondemned, date_created, created_by, hpf FROM dbo.Housing_ResidencyTypes WITH (HOLDLOCK TABLOCKX)')
GO
SET IDENTITY_INSERT dbo.Tmp_Housing_ResidencyTypes OFF
GO
DROP TABLE dbo.Housing_ResidencyTypes
GO
EXECUTE sp_rename N'dbo.Tmp_Housing_ResidencyTypes', N'Housing_ResidencyTypes', 'OBJECT' 
GO
ALTER TABLE dbo.Housing_ResidencyTypes ADD CONSTRAINT
	PK_Housing_ResidencyTypes PRIMARY KEY CLUSTERED 
	(
	oID
	) WITH( STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
DENY DELETE ON dbo.Housing_ResidencyTypes TO www_role  AS dbo 
GO
DENY INSERT ON dbo.Housing_ResidencyTypes TO www_role  AS dbo 
GO
DENY SELECT ON dbo.Housing_ResidencyTypes TO www_role  AS dbo 
GO
DENY UPDATE ON dbo.Housing_ResidencyTypes TO www_role  AS dbo 
GO
COMMIT
