/* To prevent any potential data loss issues, you should review this script in detail before running it outside the context of the database designer.*/
BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION
GO
CREATE TABLE dbo.Tmp_OCS_UploadReport
	(
	ID dbo.typ_key NOT NULL IDENTITY (1, 1),
	[User] varchar(80) NULL,
	BatchName varchar(200) NULL,
	AttemptId uniqueidentifier NULL,
	Servicer varchar(100) NULL,
	UploadDate datetime NULL,
	StartTime datetime NULL,
	EndTime datetime NULL,
	RunTimeSeconds int NULL,
	RecordsGiven int NULL,
	RecordsDuplicates int NULL,
	RecordsAttemptInsert int NULL,
	RecordsInserted int NULL,
	DupeTableEntriesCreated int NULL,
	PurgeEntriesCreated int NULL,
	RecordsMissingPhoneNumbers int NULL,
	PhoneNumbersAdded int NULL,
	RecordsSentToMTI int NULL,
	InsertDuplicates bit NULL,
	Success bit NULL,
	ReportSaved bit NULL,
	Errors text NULL,
	Exceptions text NULL,
	DuplicateRecordsList text NULL,
	Program int NULL
	)  ON [PRIMARY]
	 TEXTIMAGE_ON [PRIMARY]
GO
GRANT DELETE ON dbo.Tmp_OCS_UploadReport TO public  AS dbo
GO
GRANT INSERT ON dbo.Tmp_OCS_UploadReport TO public  AS dbo
GO
GRANT SELECT ON dbo.Tmp_OCS_UploadReport TO public  AS dbo
GO
GRANT UPDATE ON dbo.Tmp_OCS_UploadReport TO public  AS dbo
GO
SET IDENTITY_INSERT dbo.Tmp_OCS_UploadReport ON
GO
IF EXISTS(SELECT * FROM sysobjects where name=N'OCS_UploadReport' and type='U')
BEGIN
	EXEC('INSERT INTO dbo.Tmp_OCS_UploadReport (ID, [User], BatchName, AttemptId, Servicer, UploadDate, StartTime, EndTime, RunTimeSeconds, RecordsGiven, RecordsDuplicates, RecordsAttemptInsert, RecordsInserted, DupeTableEntriesCreated, PurgeEntriesCreated, RecordsMissingPhoneNumbers, PhoneNumbersAdded, RecordsSentToMTI, InsertDuplicates, Success, ReportSaved, Errors, Exceptions, DuplicateRecordsList, Program)
		SELECT ID, [User], BatchName, AttemptId, Servicer, UploadDate, StartTime, EndTime, RunTimeSeconds, RecordsGiven, RecordsDuplicates, RecordsAttemptInsert, RecordsInserted, DupeTableEntriesCreated, PurgeEntriesCreated, RecordsMissingPhoneNumbers, PhoneNumbersAdded, RecordsSentToMTI, InsertDuplicates, Success, ReportSaved, Errors, Exceptions, DuplicateRecordsList, Program FROM dbo.OCS_UploadReport WITH (HOLDLOCK TABLOCKX)')
	EXEC('DROP TABLE dbo.OCS_UploadReport')
END
GO
SET IDENTITY_INSERT dbo.Tmp_OCS_UploadReport OFF
GO
EXECUTE sp_rename N'dbo.Tmp_OCS_UploadReport', N'OCS_UploadReport', 'OBJECT' 
GO
ALTER TABLE dbo.OCS_UploadReport ADD CONSTRAINT
	PK_OCS_FMACClientUploadReport PRIMARY KEY CLUSTERED 
	(
	ID
	) WITH( STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
DENY DELETE ON dbo.OCS_UploadReport TO www_role  AS dbo 
GO
DENY INSERT ON dbo.OCS_UploadReport TO www_role  AS dbo 
GO
DENY SELECT ON dbo.OCS_UploadReport TO www_role  AS dbo 
GO
DENY UPDATE ON dbo.OCS_UploadReport TO www_role  AS dbo 
GO
COMMIT
