USE [DebtPlus]
GO
/****** Object:  Table [dbo].[postal_abbreviations]    Script Date: 09/15/2014 13:13:26 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[postal_abbreviations](
	[postal_abbreviation] [dbo].[typ_key] IDENTITY(1,1) NOT NULL,
	[type] [int] NOT NULL,
	[description] [dbo].[typ_description] NOT NULL,
	[abbreviation] [varchar](10) NOT NULL,
	[require_modifier] [bit] NOT NULL,
	[Default] [bit] NOT NULL,
 CONSTRAINT [PK_postal_abbreviations] PRIMARY KEY CLUSTERED 
(
	[postal_abbreviation] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
