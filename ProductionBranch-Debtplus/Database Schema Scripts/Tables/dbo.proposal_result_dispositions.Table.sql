USE [DebtPlus]
GO
/****** Object:  Table [dbo].[proposal_result_dispositions]    Script Date: 09/15/2014 13:13:26 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[proposal_result_dispositions](
	[proposal_result_disposition] [dbo].[typ_key] IDENTITY(1,1) NOT NULL,
	[description] [varchar](255) NULL,
	[rpps_code] [varchar](10) NULL,
	[date_created] [dbo].[typ_date] NOT NULL,
	[created_by] [dbo].[typ_counselor] NOT NULL,
	[old_proposal_result_disposition] [int] NULL,
 CONSTRAINT [PK_proposal_result_dispositions] PRIMARY KEY CLUSTERED 
(
	[proposal_result_disposition] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 90) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Primary key to the table.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'proposal_result_dispositions', @level2type=N'COLUMN',@level2name=N'proposal_result_disposition'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Description for the action performed on the rejected proposal.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'proposal_result_dispositions', @level2type=N'COLUMN',@level2name=N'description'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Date/Time when the row was created.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'proposal_result_dispositions', @level2type=N'COLUMN',@level2name=N'date_created'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Person who created the row.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'proposal_result_dispositions', @level2type=N'COLUMN',@level2name=N'created_by'
GO
