/* To prevent any potential data loss issues, you should review this script in detail before running it outside the context of the database designer.*/
BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION
GO
CREATE TABLE dbo.Tmp_HECM_Default
	(
	ReferralID dbo.typ_key NOT NULL IDENTITY (1, 1),
	Client dbo.typ_client NOT NULL,
	Referral_Option int NULL,
	Tier int NULL,
	Checkup int NULL,
	BDT_BEC int NULL,
	Hardest_Hit_Ref_Date datetime NULL,
	Ref_Food int NULL,
	Save_Food money NULL,
	Ref_Medical int NULL,
	Save_Medical money NULL,
	Ref_Prescription int NULL,
	Save_Prescription money NULL,
	Ref_Utility int NULL,
	Save_Utility money NULL,
	Ref_Car_Ins int NULL,
	Save_Car_Ins money NULL,
	Ref_Property_Taxes int NULL,
	Save_Property_Taxes money NULL,
	Ref_Home_Repair int NULL,
	Save_Home_Repair money NULL,
	Ref_Income int NULL,
	Save_Income money NULL,
	Ref_Social_Services int NULL,
	Save_Social_Services money NULL,
	Ref_SSI int NULL,
	Save_SSI money NULL,
	File_Status int NULL,
	File_Status_Date datetime NULL,
	Created_Date dbo.typ_date NOT NULL,
	Created_By dbo.typ_counselor NOT NULL,
	ts timestamp NOT NULL,
	[OBSOLETE Save_Income] datetime NULL
	)  ON [PRIMARY]
GO
SET IDENTITY_INSERT dbo.Tmp_HECM_Default ON
GO
IF EXISTS(SELECT * FROM dbo.HECM_Default)
	 EXEC('INSERT INTO dbo.Tmp_HECM_Default (ReferralID, Created_Date, Created_By, Client, Referral_Option, Tier, Checkup, BDT_BEC, Hardest_Hit_Ref_Date, Ref_Food, Save_Food, Ref_Medical, Save_Medical, Ref_Prescription, Save_Prescription, Ref_Utility, Save_Utility, Ref_Car_Ins, Save_Car_Ins, Ref_Property_Taxes, Save_Property_Taxes, Ref_Home_Repair, Save_Home_Repair, Ref_Income, [OBSOLETE Save_Income], Save_Income, Ref_Social_Services, Save_Social_Services, Ref_SSI, Save_SSI, File_Status, File_Status_Date)
		SELECT ReferralID, Created_Date, Created_By, Client, Referral_Option, Tier, Checkup, BDT_BEC, Hardest_Hit_Ref_Date, Ref_Food, Save_Food, Ref_Medical, Save_Medical, Ref_Prescription, Save_Prescription, Ref_Utility, Save_Utility, Ref_Car_Ins, Save_Car_Ins, Ref_Property_Taxes, Save_Property_Taxes, Ref_Home_Repair, Save_Home_Repair, Ref_Income, Save_Income, convert(money,Save_Income) as Save_Income, Ref_Social_Services, Save_Social_Services, Ref_SSI, Save_SSI, File_Status, File_Status_Date FROM dbo.HECM_Default WITH (HOLDLOCK TABLOCKX)')
GO
SET IDENTITY_INSERT dbo.Tmp_HECM_Default OFF
GO
DROP TABLE dbo.HECM_Default
GO
EXECUTE sp_rename N'dbo.Tmp_HECM_Default', N'HECM_Default', 'OBJECT' 
GO
ALTER TABLE dbo.HECM_Default ADD CONSTRAINT
	PK_HECM_Default PRIMARY KEY CLUSTERED 
	(
	ReferralID
	) WITH( STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
COMMIT
GO
