USE [DebtPlus]
GO
/****** Object:  Table [dbo].[CounselorSchedules]    Script Date: 09/15/2014 13:13:26 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[CounselorSchedules](
	[oID] [int] IDENTITY(1,1) NOT NULL,
	[Counselor] [dbo].[typ_key] NOT NULL,
	[Office] [dbo].[typ_key] NULL,
	[Dow] [int] NULL,
	[Date] [datetime] NULL,
 CONSTRAINT [PK_CounselorSchedules] PRIMARY KEY CLUSTERED 
(
	[oID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
