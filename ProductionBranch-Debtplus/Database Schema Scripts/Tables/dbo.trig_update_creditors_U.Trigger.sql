SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
execute sp_refreshview 'update_creditors'
GO
ALTER TRIGGER [trig_update_creditors_U] ON [update_creditors] INSTEAD OF UPDATE AS
BEGIN

	set nocount on

	-- Ensure that we can do what we need for the single client.
	-- This view is for the program to update a single client and generate a system note on the fields being changed.
	if (select count(*) FROM inserted) > 1
	begin
		RaisError ('Only one creditor may be updated with the update_creditors view at a time.', 16, 1)
		Rollback
		return
	end

	-- Fields that are used in this trigger
	declare	@creditor			varchar(10)
	declare	@system_note		varchar(8000)
	declare	@update_stmt		varchar(8000)
	declare	@fields				varchar(512)
	declare	@updated_apr		int

	-- Old and new values for the types (base) that we use in this table
	declare	@new_str			varchar(512)
	declare	@old_str			varchar(512)
	declare	@new_int			int
	declare	@old_int			int
	declare	@new_float			float
	declare	@old_float			float
	declare	@new_date			datetime
	declare	@old_date			datetime
	declare	@old_money			decimal(10,2)
	declare	@new_money			decimal(10,2)

	-- Retrieve the client information being updated
	select	@creditor			= creditor
	from	inserted

	-- type
	if update(type)
	begin
		select		@new_str	= type
		from		inserted

		select		@old_str	= type
		from		deleted

		if @new_str <> @old_str
		begin
			select	@system_note	= isnull(@system_note + char(13) + char(10),'') + 'changed creditor type' + coalesce(' from ' + @old_str, '') + ' to ' + isnull(@new_str, 'NOTHING')
			select	@fields		= isnull(@fields+',','') + 'type'
		end
		select	@update_stmt	= isnull(@update_stmt + ',','') + 'proposed_date=' + isnull('''' + replace(@new_str,'''', '''''') + '''', 'NULL')
	end

	-- creditor_name
	if update(creditor_name)
	begin
		select		@new_str	= creditor_name
		from		inserted

		select		@old_str	= creditor_name
		from		deleted

		if isnull(@new_str,'') <> isnull(@old_str,'')
		begin
			select	@system_note	= isnull(@system_note + char(13) + char(10),'') + 'changed creditor name' + coalesce(' from ' + @old_str, '') + ' to ' + isnull(@new_str, 'NOTHING')
			select	@fields		= isnull(@fields+',','') + 'name'
		end
		select	@update_stmt	= isnull(@update_stmt + ',','') + 'creditor_name=' + isnull('''' + replace(@new_str,'''', '''''') + '''', 'NULL')
	end

	-- division
	if update(division)
	begin
		select		@new_str	= division
		from		inserted

		select		@old_str	= division
		from		deleted

		if isnull(@new_str,'') <> isnull(@old_str,'')
		begin
			select	@system_note	= isnull(@system_note + char(13) + char(10),'') + 'changed division' + coalesce(' from ' + @old_str, '') + ' to ' + isnull(@new_str, 'NOTHING')
			select	@fields		= isnull(@fields+',','') + 'division'
		end
		select	@update_stmt	= isnull(@update_stmt + ',','') + 'division=' + isnull('''' + replace(@new_str,'''', '''''') + '''', 'NULL')
	end

	-- sic
	if update(sic)
	begin
		select		@new_str	= sic
		from		inserted

		select		@old_str	= sic
		from		deleted

		if isnull(@new_str,'') <> isnull(@old_str,'')
		begin
			select	@system_note	= isnull(@system_note + char(13) + char(10),'') + 'changed sic' + coalesce(' from ' + @old_str, '') + ' to ' + isnull(@new_str, 'NOTHING')
			select	@fields		= isnull(@fields+',','') + 'sic'
		end
		select	@update_stmt	= isnull(@update_stmt + ',','') + 'sic=' + isnull('''' + replace(@new_str,'''', '''''') + '''', 'NULL')
	end

	-- comment
	if update(comment)
	begin
		select		@new_str	= comment
		from		inserted

		select		@old_str	= comment
		from		deleted

		if isnull(@new_str,'') <> isnull(@old_str,'')
		begin
			select	@system_note	= isnull(@system_note + char(13) + char(10),'') + 'changed comment' + coalesce(' from ' + @old_str, '') + ' to ' + isnull(@new_str, 'NOTHING')
			select	@fields		= isnull(@fields+',','') + 'comment'
		end
		select	@update_stmt	= isnull(@update_stmt + ',','') + 'comment=' + isnull('''' + replace(@new_str,'''', '''''') + '''', 'NULL')
	end

	-- po_number
	if update(po_number)
	begin
		select		@new_str	= po_number
		from		inserted

		select		@old_str	= po_number
		from		deleted

		if isnull(@new_str,'') <> isnull(@old_str,'')
		begin
			select	@system_note	= isnull(@system_note + char(13) + char(10),'') + 'changed P. O. Number' + coalesce(' from ' + @old_str, '') + ' to ' + isnull(@new_str, 'NOTHING')
			select	@fields		= isnull(@fields+',','') + 'po#'
		end
		select	@update_stmt	= isnull(@update_stmt + ',','') + 'po_number=' + isnull('''' + replace(@new_str,'''', '''''') + '''', 'NULL')
	end

	-- payment_balance
	if update(payment_balance)
	begin
		select		@new_str	= payment_balance
		from		inserted

		select		@old_str	= payment_balance
		from		deleted

		if @new_str <> @old_str
		begin
			select	@system_note	= isnull(@system_note + char(13) + char(10),'') + 'changed payment/balance from ' + isnull(@old_str,'B') + ' to ' + isnull(@new_str, 'B')
			select	@fields		= isnull(@fields+',','') + 'P/B'
		end
		select	@update_stmt	= isnull(@update_stmt + ',','') + 'payment_balance=''' + isnull(@new_str, 'B') + ''''
	end

	-- prohibit_use
	if update(prohibit_use)
	begin
		select		@new_int	= prohibit_use
		from		inserted

		select		@old_int	= prohibit_use
		from		deleted

		if @new_int <> @old_int
		begin
			select	@system_note	= isnull(@system_note + char(13) + char(10),'') + 'changed prohibit use from ' + dbo.map_yes_no (@old_int) + ' to ' + dbo.map_yes_no ( @new_int )
			select	@fields		= isnull(@fields+',','') + 'prohibit'
		end
		select	@update_stmt	= isnull(@update_stmt + ',','') + 'prohibit_use=' + convert(varchar,isnull(@new_int,0))
	end

	-- suppress_invoice
	if update(suppress_invoice)
	begin
		select		@new_int	= suppress_invoice
		from		inserted

		select		@old_int	= suppress_invoice
		from		deleted

		if @new_int <> @old_int
		begin
			select	@system_note	= isnull(@system_note + char(13) + char(10),'') + 'changed suppress invoice from ' + dbo.map_yes_no (@old_int) + ' to ' + dbo.map_yes_no ( @new_int )
			select	@fields		= isnull(@fields+',','') + 'supr_inv'
		end
		select	@update_stmt	= isnull(@update_stmt + ',','') + 'suppress_invoice=' + convert(varchar,isnull(@new_int,0))
	end

	-- full_disclosure
	if update(full_disclosure)
	begin
		select		@new_int	= full_disclosure
		from		inserted

		select		@old_int	= full_disclosure
		from		deleted

		if @new_int <> @old_int
		begin
			select	@system_note	= isnull(@system_note + char(13) + char(10),'') + 'changed full disclosure from ' + dbo.map_yes_no (@old_int) + ' to ' + dbo.map_yes_no ( @new_int )
			select	@fields		= isnull(@fields+',','') + 'full discl'
		end
		select	@update_stmt	= isnull(@update_stmt + ',','') + 'full_disclosure=' + convert(varchar,isnull(@new_int,0))
	end

	-- proposal_budget_info
	if update(proposal_budget_info)
	begin
		select		@new_int	= proposal_budget_info
		from		inserted

		select		@old_int	= proposal_budget_info
		from		deleted

		if @new_int <> @old_int
		begin
			select	@system_note	= isnull(@system_note + char(13) + char(10),'') + 'changed proposal_budget_info from ' + dbo.map_yes_no (@old_int) + ' to ' + dbo.map_yes_no ( @new_int )
			select	@fields		= isnull(@fields+',','') + 'proposal_budget_info'
		end
		select	@update_stmt	= isnull(@update_stmt + ',','') + 'proposal_budget_info=' + convert(varchar,isnull(@new_int,0))
	end

	-- proposal_income_info
	if update(proposal_income_info)
	begin
		select		@new_int	= proposal_income_info
		from		inserted

		select		@old_int	= proposal_income_info
		from		deleted

		if @new_int <> @old_int
		begin
			select	@system_note	= isnull(@system_note + char(13) + char(10),'') + 'changed proposal_income_info from ' + dbo.map_yes_no (@old_int) + ' to ' + dbo.map_yes_no ( @new_int )
			select	@fields		= isnull(@fields+',','') + 'proposal_income_info'
		end
		select	@update_stmt	= isnull(@update_stmt + ',','') + 'proposal_income_info=' + convert(varchar,isnull(@new_int,0))
	end

	-- max_amount_per_check
	if update(max_amt_per_check)
	begin
		select		@new_money	= max_amt_per_check
		from		inserted

		select		@old_money	= max_amt_per_check
		from		deleted

		if isnull(@new_money,-1) <> isnull(@old_money,-1)
		begin
			select	@system_note	= isnull(@system_note + char(13) + char(10),'') + 'changed max_amount_per_check' + isnull(' from $' + convert(varchar, @old_money, 1), '') + ' to ' + isnull('$' + convert(varchar, @new_money, 1),'default')
			select	@fields		= isnull(@fields+',','') + 'chk$'
		end
		select	@update_stmt	= isnull(@update_stmt + ',','') + 'max_amount_per_check=' + isnull(convert(varchar, @new_money), 'NULL')
	end

	-- min_accept_per_bill
	if update(min_accept_per_bill)
	begin
		select		@new_money	= min_accept_per_bill
		from		inserted

		select		@old_money	= min_accept_per_bill
		from		deleted

		if isnull(@new_money,-1) <> isnull(@old_money,-1)
		begin
			select	@system_note	= isnull(@system_note + char(13) + char(10),'') + 'changed minimum accepted per bill (if more than one check per bill)' + isnull(' from $' + convert(varchar, @old_money, 1), '') + ' to ' + isnull('$' + convert(varchar, @new_money, 1),'default')
			select	@fields		= isnull(@fields+',','') + 'min bill$'
		end
		select	@update_stmt	= isnull(@update_stmt + ',','') + 'min_accept_per_bill=' + isnull(convert(varchar, @new_money), 'NULL')
	end

	-- max_fairshare_per_debt
	if update(max_fairshare_per_debt)
	begin
		select		@new_money	= max_fairshare_per_debt
		from		inserted

		select		@old_money	= max_fairshare_per_debt
		from		deleted

		if isnull(@new_money,-1) <> isnull(@old_money,-1)
		begin
			select	@system_note	= isnull(@system_note + char(13) + char(10),'') + 'changed max_fairshare_per_debt' + isnull(' from $' + convert(varchar, @old_money, 1), '') + ' to ' + isnull('$' + convert(varchar, @new_money, 1),'default')
			select	@fields		= isnull(@fields+',','') + 'f/s$' 	end
		end
		select	@update_stmt	= isnull(@update_stmt + ',','') + 'max_fairshare_per_debt=' + isnull(convert(varchar, @new_money), 'NULL')
	end

	-- contrib_cycle
	if update(contrib_cycle)
	begin
		select		@new_str	= contrib_cycle
		from		inserted

		select		@old_str	= contrib_cycle
		from		deleted

		if @new_str <> @old_str
		begin
			select	@system_note	= isnull(@system_note + char(13) + char(10),'') + 'changed billing cycle from ' + dbo.map_bill_cycle(@old_str) + ' to ' + dbo.map_bill_cycle(@new_str)
			select	@fields		= isnull(@fields+',','') + 'bill'
		end
		select	@update_stmt	= isnull(@update_stmt + ',','') + 'contrib_cycle=' + isnull('''' + replace(@new_str, '''', '''''') + '''', 'NULL')
	end

	-- contrib_bill_month
	if update(contrib_bill_month)
	begin
		select		@new_int	= contrib_bill_month
		from		inserted

		select		@old_int	= contrib_bill_month
		from		deleted

		if @new_int <> @old_int
		begin
			select	@system_note	= isnull(@system_note + char(13) + char(10),'') + 'changed billing period from ' + convert(varchar, isnull(@old_int,1)) + ' to ' + convert(varchar, isnull(@new_int,1))
			select	@fields		= isnull(@fields+',','') + 'bill period'
		end
		select	@update_stmt	= isnull(@update_stmt + ',','') + 'contrib_bill_month=' + convert(varchar, isnull(@new_int,1))
	end

	-- percent_balance
	if update(percent_balance)
	begin
		select		@new_float	= percent_balance
		from		inserted

		select		@old_float	= percent_balance
		from		deleted

		if @new_float <> @old_float
		begin
			select	@system_note	= isnull(@system_note + char(13) + char(10),'') + 'changed percent of balance from ' + convert(varchar, isnull(@old_float * 100.0,100.0)) + '% to ' + convert(varchar, isnull(@new_float * 100.0,100.0)) + '%'
			select	@fields		= isnull(@fields+',','') + 'balance%'
		end
		select	@update_stmt	= isnull(@update_stmt + ',','') + 'percent_balance=' + convert(varchar,isnull(@new_float,100))
	end

	-- pledge_amount
	if update(pledge_amt)
	begin
		select		@new_money	= pledge_amt
		from		inserted

		select		@old_money	= pledge_amt
		from		deleted

		if @new_money <> @old_money
		begin
			select	@system_note	= isnull(@system_note + char(13) + char(10),'') + 'changed pledge amount from $' + convert(varchar, isnull(@old_money,0), 1) + ' to $' + convert(varchar, isnull(@new_money,1), 1)
			select	@fields		= isnull(@fields+',','') + 'pledge$'
		end
		select	@update_stmt	= isnull(@update_stmt + ',','') + 'pledge_amt=' + convert(varchar, isnull(@new_money,0))
	end

	-- pledge_cycle
	if update(pledge_cycle)
	begin
		select		@new_str	= pledge_cycle
		from		inserted

		select		@old_str	= pledge_cycle
		from		deleted

		if @new_str <> @old_str
		begin
			select	@system_note	= isnull(@system_note + char(13) + char(10),'') + 'changed pledge cycle from ' + dbo.map_bill_cycle(@old_str) + ' to ' + dbo.map_bill_cycle(@new_str)
			select	@fields		= isnull(@fields+',','') + 'pledge period'
		end
		select	@update_stmt	= isnull(@update_stmt + ',','') + 'pledge_cycle=' + isnull('''' + replace(@new_str, '''', '''''') + '''', 'NULL')
	end

	-- pledge_bill_month
	if update(pledge_bill_month)
	begin
		select		@new_int	= pledge_bill_month
		from		inserted
		select		@old_int	= pledge_bill_month
		from		deleted

		if @new_int <> @old_int
		begin
			select	@system_note	= isnull(@system_note + char(13) + char(10),'') + 'changed pledge bill month from ' + convert(varchar, isnull(@old_int,1)) + ' to ' + convert(varchar, isnull(@new_int,1))
			select	@fields		= isnull(@fields+',','') + 'pledge month'
		end
		select	@update_stmt	= isnull(@update_stmt + ',','') + 'pledge_bill_month=' + convert(varchar, isnull(@new_int,1))
	end

	-- creditor_class
	if update(creditor_class)
	begin
		select		@new_int	= creditor_class
		from		inserted

		select		@new_str	= isnull(description,'#' + convert(varchar,@new_int))
		from		creditor_classes with (nolock)
		where		creditor_class = @new_int

		select		@old_str	= isnull(x.description,'#' + convert(varchar,d.creditor_class))
		from		deleted d
		left outer join	creditor_classes x with (nolock) on d.creditor_class = x.creditor_class

		if @new_str <> @old_str
		begin
			select	@system_note	= isnull(@system_note + char(13) + char(10),'') + 'changed creditor class' + coalesce(' from ' + @old_str, '') + ' to ' + isnull(@new_str, 'NOTHING')
			select	@fields		= isnull(@fields+',','') + 'class'
		end
		select	@update_stmt	= isnull(@update_stmt + ',','') + 'creditor_class=' + isnull(convert(varchar, @new_int),'NULL')
	end

	-- voucher_spacing
	if update(voucher_spacing)
	begin
		select		@new_int	= voucher_spacing
		from		inserted

		select		@old_int	= voucher_spacing
		from		deleted

		select		@new_str	= case isnull(@new_int,0) when 0 then 'single' when 1 then 'double' when 2 then 'none' end,
				    @old_str	= case isnull(@old_int,0) when 0 then 'single' when 1 then 'double' when 2 then 'none' end

		if @new_str <> @old_str
		begin
			select	@system_note	= isnull(@system_note + char(13) + char(10),'') + 'changed voucher spacing from ' + @old_str + ' to ' + @new_str
			select	@fields		= isnull(@fields+',','') + 'spacing'
		end
		select	@update_stmt	= isnull(@update_stmt + ',','') + 'voucher_spacing=' + convert(varchar, isnull(@new_int,0))
	end

	-- mail_priority
	if update(mail_priority)
	begin
		select		@new_int	= mail_priority
		from		inserted

		select		@old_int	= mail_priority
		from		deleted

		if @new_int <> @old_int
		begin
			select	@system_note	= isnull(@system_note + char(13) + char(10),'') + 'changed mail priority from ' + convert(varchar, isnull(@old_int,0)) + ' to ' + convert(varchar, isnull(@new_int,0))
			select	@fields		= isnull(@fields+',','') + 'mail'
		end
		select	@update_stmt	= isnull(@update_stmt + ',','') + 'mail_priority=' + convert(varchar, isnull(@new_int,0))
	end

	-- usual_priority
	if update(usual_priority)
	begin
		select		@new_int	= usual_priority
		from		inserted

		select		@old_int	= usual_priority
		from		deleted

		if @new_int <> @old_int
		begin
			select	@system_note	= isnull(@system_note + char(13) + char(10),'') + 'changed usual_priority from ' + convert(varchar, isnull(@old_int,0)) + ' to ' + convert(varchar, isnull(@new_int,0))
			select	@fields		= isnull(@fields+',','') + 'priority'
		end
		select	@update_stmt	= isnull(@update_stmt + ',','') + 'usual_priority=' + convert(varchar, isnull(@new_int,0))
	end

	-- max_clients_per_check
	if update(max_clients_per_check)
	begin
		select		@new_int	= max_clients_per_check
		from		inserted

		select		@old_int	= max_clients_per_check
		from		deleted

		if @new_int <> @old_int
		begin
			select	@system_note	= isnull(@system_note + char(13) + char(10),'') + 'changed max_clients_per_check' + isnull(' from ' + convert(varchar, @old_int), '') + ' to ' + isnull(convert(varchar, @new_int),'default')
			select	@fields		= isnull(@fields+',','') + 'chk#'
		end
		select	@update_stmt	= isnull(@update_stmt + ',','') + 'max_clients_per_check=' + isnull(convert(varchar, @new_int), 'NULL')
	end

	-- acceptance_days
	if update(acceptance_days)
	begin
		select		@new_int	= acceptance_days
		from		inserted

		select		@old_int	= acceptance_days
		from		deleted

		if @new_int <> @old_int
		begin
			select	@system_note	= isnull(@system_note + char(13) + char(10),'') + 'changed proposal default acceptance days' + isnull(' from ' + convert(varchar, @old_int), '') + ' to ' + isnull(convert(varchar, @new_int),'default')
			select	@fields		= isnull(@fields+',','') + 'prop days'
		end
		select	@update_stmt	= isnull(@update_stmt + ',','') + 'acceptance_days=' + isnull(convert(varchar, @new_int), 'NULL')
	end

	-- chks_per_invoice
	if update(chks_per_invoice)
	begin
		select		@new_int	= chks_per_invoice
		from		inserted

		select		@old_int	= chks_per_invoice
		from		deleted

		if @new_int <> @old_int
		begin
			select	@system_note	= isnull(@system_note + char(13) + char(10),'') + 'changed chks_per_invoice' + isnull(' from ' + convert(varchar, @old_int), '') + ' to ' + isnull(convert(varchar, @new_int),'default')
			select	@fields		= isnull(@fields+',','') + 'inv#'
		end
		select	@update_stmt	= isnull(@update_stmt + ',','') + 'chks_per_invoice=' + isnull(convert(varchar, @new_int), 'NULL')
	end

	-- check_payments
	if update(check_payments)
	begin
		select		@new_int	= check_payments
		from		inserted

		select		@old_int	= check_payments
		from		deleted

		if @new_int <> @old_int
		begin
			select	@system_note	= isnull(@system_note + char(13) + char(10),'') + 'changed check_payments' + isnull(' from ' + convert(varchar, @old_int), '') + ' to ' + isnull(convert(varchar, @new_int),'0')
			select	@fields		= isnull(@fields+',','') + 'pmts#'
		end
		select	@update_stmt	= isnull(@update_stmt + ',','') + 'check_payments=' + isnull(convert(varchar, @new_int), '0')
	end

	-- check_bank
	if update(check_bank)
	begin
		select		@new_int	= check_bank
		from		inserted

		select		@new_str	= description
		from		banks with (nolock)
		where		bank		= @new_int

		select		@old_int	= check_bank
		from		deleted

		select		@old_str	= description
		from		banks with (nolock)
		where		bank		= @old_int

		if @new_int <> @old_int
		begin
			select	@system_note	= isnull(@system_note + char(13) + char(10),'') + 'changed check_bank' + isnull(' from ' + @old_str, '') + ' to ' + isnull(@new_str,'NOTHING')
			select	@fields		= isnull(@fields+',','') + 'chk bank'
		end
		select	@update_stmt	= isnull(@update_stmt + ',','') + 'check_bank=' + isnull(convert(varchar, @new_int), 'NULL')
	end

	-- min_accept_amt
	if update(min_accept_amt)
	begin
		select		@new_money	= min_accept_amt
		from		inserted

		select		@old_money	= min_accept_amt
		from		deleted

		if @new_money <> @old_money
		begin
			select	@system_note	= isnull(@system_note + char(13) + char(10),'') + 'changed mininum acceptable from $' + convert(varchar, isnull(@old_money,0), 1) + ' to $' + convert(varchar, isnull(@new_money,1), 1)
			select	@fields		= isnull(@fields+',','') + 'accept$'
		end
		select	@update_stmt	= isnull(@update_stmt + ',','') + 'min_accept_amt=' + convert(varchar, isnull(@new_money,0))
	end

	-- min_accept_pct
	if update(min_accept_pct)
	begin
		select		@new_float	= min_accept_pct
		from		inserted

		select		@old_float	= min_accept_pct
		from		deleted

		if @new_float <> @old_float
		begin
			select	@system_note	= isnull(@system_note + char(13) + char(10),'') + 'changed minimum acceptable pct from ' + convert(varchar, isnull(@old_float,0) * 100.0) + '% to ' + convert(varchar, isnull(@old_float,0) * 100.0) + '%'
			select	@fields		= isnull(@fields+',','') + 'accept%'
		end
		select	@update_stmt	= isnull(@update_stmt + ',','') + 'min_accept_pct=' + convert(varchar,isnull(@new_float,0))
	end

	-- returned_mail
	if update(returned_mail)
	begin
		select		@new_date	= returned_mail
		from		inserted

		select		@old_date	= returned_mail
		from		deleted

		if @new_date <> @old_date
		begin
			select	@system_note	= isnull(@system_note + char(13) + char(10),'') + 'changed error date' + isnull(' from ' + convert(varchar(10), @old_date, 101), '') + ' to ' + isnull(convert(varchar(10), @new_date, 101), 'nothing')
			select	@fields		= isnull(@fields+',','') + 'error date'
		end
		select	@update_stmt	= isnull(@update_stmt + ',','') + 'returned_mail=' + isnull('''' + convert(varchar(10),@new_date,101) + ' 00:00:00''', 'NULL')
	end

	-- lowest_apr_pct
	select	@updated_apr	= 0
	if update(lowest_apr_pct)
	begin
		select		@new_float	= lowest_apr_pct
		from		inserted

		select		@old_float	= lowest_apr_pct
		from		deleted

		if @new_float <> @old_float
		begin
			select	@system_note	= isnull(@system_note + char(13) + char(10),'') + 'changed lowest A.P.R. pct from ' + convert(varchar, isnull(@old_float,0) * 100.0) + '% to ' + convert(varchar, isnull(@new_float,0) * 100.0) + '%'
			select	@updated_apr	= 1
		end
		select	@update_stmt	= isnull(@update_stmt + ',','') + 'lowest_apr_pct=' + convert(varchar,isnull(@new_float,0))
	end

	-- medium_apr_pct
	if update(medium_apr_pct)
	begin
		select		@new_float	= medium_apr_pct
		from		inserted

		select		@old_float	= medium_apr_pct
		from		deleted

		if @new_float <> @old_float
		begin
			select	@system_note	= isnull(@system_note + char(13) + char(10),'') + 'changed middle A.P.R. pct from ' + convert(varchar, isnull(@old_float,0) * 100.0) + '% to ' + convert(varchar, isnull(@new_float,0) * 100.0) + '%'
			select	@updated_apr	= 1
		end
		select	@update_stmt	= isnull(@update_stmt + ',','') + 'medium_apr_pct=' + convert(varchar,isnull(@new_float,0))
	end

	-- medium_apr_amt
	if update(medium_apr_amt)
	begin
		select		@new_money	= medium_apr_amt
		from		inserted

		select		@old_money	= medium_apr_amt
		from		deleted

		if @new_money <> @old_money
		begin
			select	@system_note	= isnull(@system_note + char(13) + char(10),'') + 'changed APR cutoff from $' + convert(varchar, isnull(@old_money,0), 1) + ' to $' + convert(varchar, isnull(@new_money,1), 1)
			select	@updated_apr	= 1
		end
		select	@update_stmt	= isnull(@update_stmt + ',','') + 'medium_apr_amt=' + convert(varchar, isnull(@new_money,0))
	end

	-- highest_apr_pct
	if update(highest_apr_pct)
	begin
		select		@new_float	= highest_apr_pct
		from		inserted

		select		@old_float	= highest_apr_pct
		from		deleted

		if @new_float <> @old_float
		begin
			select	@system_note	= isnull(@system_note + char(13) + char(10),'') + 'changed upper A.P.R. pct from ' + convert(varchar, isnull(@old_float,0) * 100.0) + '% to ' + convert(varchar, isnull(@new_float,0) * 100.0) + '%'
			select	@updated_apr	= 1
		end
		select	@update_stmt	= isnull(@update_stmt + ',','') + 'highest_apr_pct=' + convert(varchar,isnull(@new_float,0))
	end

	-- highest_apr_amt
	if update(highest_apr_amt)
	begin
		select		@new_money	= highest_apr_amt
		from		inserted

		select		@old_money	= highest_apr_amt
		from		deleted

		if @new_money <> @old_money
		begin
			select	@system_note	= isnull(@system_note + char(13) + char(10),'') + 'changed APR cutoff from $' + convert(varchar, isnull(@old_money,0), 1) + ' to $' + convert(varchar, isnull(@new_money,1), 1)
			select	@updated_apr	= 1
		end
		select	@update_stmt	= isnull(@update_stmt + ',','') + 'highest_apr_amt=' + convert(varchar, isnull(@new_money,0))
	end

	if @updated_apr <> 0
		select	@fields		= isnull(@fields+',','') + 'A.P.R.'

	-- Update the clients table with the changes
	if @update_stmt is not null
	begin
		select	@update_stmt =	'UPDATE creditors SET ' + @update_stmt +
					' WHERE creditor=''' + replace(@creditor, '''', '''''') + ''''

		-- Include a bit of debugging information into the system note should we want it later.
		-- select	@system_note = @system_note + char(13) + char(10) + char(13) + char(10) + 'Debugging info:' + char(13) + char(10) + @update_stmt
	end

	-- Generate the system note showing the changes
	if @fields is not null
	begin
		insert into creditor_notes (is_text, creditor, type, subject, dont_edit, dont_delete, note)
		values	(1, @creditor, 3, left('Changed ' + @fields, 64), 1, 1, @system_note)
	end

	-- End with the update statement being performed
	if @update_stmt is not null
		exec ( @update_stmt );
GO
