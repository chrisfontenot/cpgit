/* To prevent any potential data loss issues, you should review this script in detail before running it outside the context of the database designer.*/
BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION
GO
CREATE TABLE dbo.Tmp_OCS_PartnerProgram
	(
	Id dbo.typ_key NOT NULL IDENTITY (1, 1),
	ShortCode dbo.typ_description NULL,
	Description varchar(100) NULL,
	ts timestamp NOT NULL
	)  ON [PRIMARY]
GO
ALTER TABLE dbo.Tmp_OCS_PartnerProgram SET (LOCK_ESCALATION = TABLE)
GO
GRANT DELETE ON dbo.Tmp_OCS_PartnerProgram TO public  AS dbo
GO
GRANT INSERT ON dbo.Tmp_OCS_PartnerProgram TO public  AS dbo
GO
GRANT SELECT ON dbo.Tmp_OCS_PartnerProgram TO public  AS dbo
GO
GRANT UPDATE ON dbo.Tmp_OCS_PartnerProgram TO public  AS dbo
GO
SET IDENTITY_INSERT dbo.Tmp_OCS_PartnerProgram ON
GO
IF EXISTS(SELECT * FROM dbo.OCS_PartnerProgram)
	 EXEC('INSERT INTO dbo.Tmp_OCS_PartnerProgram (Id, ShortCode, Description)
		SELECT Id, ShortCode, Description FROM dbo.OCS_PartnerProgram WITH (HOLDLOCK TABLOCKX)')
GO
SET IDENTITY_INSERT dbo.Tmp_OCS_PartnerProgram OFF
GO
DROP TABLE dbo.OCS_PartnerProgram
GO
EXECUTE sp_rename N'dbo.Tmp_OCS_PartnerProgram', N'OCS_PartnerProgram', 'OBJECT' 
GO
ALTER TABLE dbo.OCS_PartnerProgram ADD CONSTRAINT
	PK_OCS_PartnerProgram PRIMARY KEY CLUSTERED 
	(
	Id
	) WITH( STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
DENY DELETE ON dbo.OCS_PartnerProgram TO www_role  AS dbo 
GO
DENY INSERT ON dbo.OCS_PartnerProgram TO www_role  AS dbo 
GO
DENY SELECT ON dbo.OCS_PartnerProgram TO www_role  AS dbo 
GO
DENY UPDATE ON dbo.OCS_PartnerProgram TO www_role  AS dbo 
GO
COMMIT
