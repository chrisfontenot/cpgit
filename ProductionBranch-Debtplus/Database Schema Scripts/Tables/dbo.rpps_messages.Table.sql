USE [DebtPlus]
GO
/****** Object:  Table [dbo].[rpps_messages]    Script Date: 09/15/2014 13:13:26 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[rpps_messages](
	[rpps_message] [dbo].[typ_key] IDENTITY(1,1) NOT NULL,
	[source] [varchar](2) NOT NULL,
	[biller_id] [varchar](10) NULL,
	[client] [dbo].[typ_client] NULL,
	[creditor] [dbo].[typ_creditor] NOT NULL,
	[client_creditor] [dbo].[typ_key] NULL,
	[account_number] [varchar](50) NULL,
	[rpps_transaction] [dbo].[typ_key] NULL,
	[note] [text] NULL,
	[date_created] [dbo].[typ_date] NOT NULL,
	[created_by] [dbo].[typ_counselor] NOT NULL,
	[amount] [money] NULL,
 CONSTRAINT [PK_rpps_messages] PRIMARY KEY CLUSTERED 
(
	[rpps_message] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 90) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Primary key to the table' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'rpps_messages', @level2type=N'COLUMN',@level2name=N'rpps_message'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Message source. "PR" = proposal. Others are pending.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'rpps_messages', @level2type=N'COLUMN',@level2name=N'source'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'RPPS biller ID associated with the creditor.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'rpps_messages', @level2type=N'COLUMN',@level2name=N'biller_id'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Client associated with the debt.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'rpps_messages', @level2type=N'COLUMN',@level2name=N'client'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Creditor ID associated with the message.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'rpps_messages', @level2type=N'COLUMN',@level2name=N'creditor'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Account number with the creditor for this message.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'rpps_messages', @level2type=N'COLUMN',@level2name=N'account_number'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'When put into the output queue, this is the pointer to the transaction item.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'rpps_messages', @level2type=N'COLUMN',@level2name=N'rpps_transaction'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The text of the message. When proposals, it is taken from the proposal message.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'rpps_messages', @level2type=N'COLUMN',@level2name=N'note'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Date / Time when the row was created' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'rpps_messages', @level2type=N'COLUMN',@level2name=N'date_created'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Name of the counselor who created the row' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'rpps_messages', @level2type=N'COLUMN',@level2name=N'created_by'
GO
