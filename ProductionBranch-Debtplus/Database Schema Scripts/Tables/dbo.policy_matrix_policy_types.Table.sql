USE [DebtPlus]
GO
/****** Object:  Table [dbo].[policy_matrix_policy_types]    Script Date: 09/15/2014 13:13:26 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[policy_matrix_policy_types](
	[policy_matrix_policy_type] [varchar](50) NOT NULL,
	[description] [dbo].[typ_description] NOT NULL,
	[Default] [bit] NOT NULL,
	[ActiveFlag] [bit] NOT NULL,
	[date_created] [dbo].[typ_date] NOT NULL,
	[created_by] [dbo].[typ_counselor] NOT NULL,
 CONSTRAINT [PK_policy_matrix_policy_types] PRIMARY KEY CLUSTERED 
(
	[policy_matrix_policy_type] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
