/* To prevent any potential data loss issues, you should review this script in detail before running it outside the context of the database designer.*/
BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
GO
BEGIN TRANSACTION
GO
EXECUTE UPDATE_DROP_CONSTRAINTS 'email_queue'
GO
CREATE TABLE dbo.Tmp_email_queue
	(
	oID dbo.typ_key NOT NULL IDENTITY (1, 1),
	template dbo.typ_key NOT NULL,
	email_address varchar(512) NOT NULL,
	email_name varchar(512) NULL,
	substitutions xml NULL,
	date_created dbo.typ_date NOT NULL,
	created_by dbo.typ_counselor NOT NULL,
	death_date datetime NULL,
	status varchar(20) NOT NULL,
	last_attempt datetime NULL,
	next_attempt datetime NULL,
	attempts int NOT NULL,
	last_error varchar(512) NULL,
	date_sent datetime NULL,
	ts timestamp NOT NULL
	)  ON [PRIMARY]
	 TEXTIMAGE_ON [PRIMARY]
GO
GRANT DELETE ON dbo.Tmp_email_queue TO public  AS dbo
GO
GRANT INSERT ON dbo.Tmp_email_queue TO public  AS dbo
GO
GRANT SELECT ON dbo.Tmp_email_queue TO public  AS dbo
GO
GRANT UPDATE ON dbo.Tmp_email_queue TO public  AS dbo
GO
ALTER TABLE dbo.Tmp_email_queue ADD CONSTRAINT
	DF_email_queue_status DEFAULT ('PENDING') FOR status
GO
EXECUTE sp_bindefault N'dbo.default_zero', N'dbo.Tmp_email_queue.attempts'
GO
SET IDENTITY_INSERT dbo.Tmp_email_queue ON
GO
IF EXISTS(SELECT * FROM SYSOBJECTS WHERE NAME = N'email_queue' AND TYPE = 'U')
BEGIN
	 EXEC('INSERT INTO dbo.Tmp_email_queue (oID, template, email_address, email_name, substitutions, date_created, created_by, death_date, status, last_attempt, next_attempt, attempts, last_error, date_sent)
		SELECT oID, template, email_address, email_name, substitutions, date_created, created_by, death_date, status, last_attempt, next_attempt, attempts, last_error, date_sent FROM dbo.email_queue WITH (HOLDLOCK TABLOCKX)')
	 EXEC('DROP TABLE dbo.email_queue')
END
GO
SET IDENTITY_INSERT dbo.Tmp_email_queue OFF
GO
EXECUTE sp_rename N'dbo.Tmp_email_queue', N'email_queue', 'OBJECT' 
GO
ALTER TABLE dbo.email_queue ADD CONSTRAINT
	PK_email_queue PRIMARY KEY CLUSTERED 
	(
	oID
	) WITH( STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
DENY DELETE ON dbo.email_queue TO www_role  AS dbo 
GO
DENY INSERT ON dbo.email_queue TO www_role  AS dbo 
GO
DENY SELECT ON dbo.email_queue TO www_role  AS dbo 
GO
DENY UPDATE ON dbo.email_queue TO www_role  AS dbo 
GO
COMMIT
GO
