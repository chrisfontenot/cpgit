USE [DebtPlus]
GO
/****** Object:  Table [dbo].[letter_types]    Script Date: 09/15/2014 13:13:26 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[letter_types](
	[letter_code] [dbo].[typ_letter_code] NOT NULL,
	[queue_name] [dbo].[typ_description] NULL,
	[description] [dbo].[typ_description] NOT NULL,
	[menu_name] [dbo].[typ_description] NULL,
	[letter_group] [dbo].[typ_letter_group] NOT NULL,
	[display_mode] [int] NOT NULL,
	[date_created] [dbo].[typ_date] NOT NULL,
	[created_by] [dbo].[typ_counselor] NOT NULL,
	[letter_type] [dbo].[typ_key] IDENTITY(1,1) NOT NULL,
	[region] [dbo].[typ_key] NOT NULL,
	[top_margin] [float] NOT NULL,
	[left_margin] [float] NOT NULL,
	[bottom_margin] [float] NOT NULL,
	[right_margin] [float] NOT NULL,
	[filename] [varchar](255) NOT NULL,
	[language] [dbo].[typ_key] NOT NULL,
	[Default] [bit] NOT NULL,
	[old_letter_type] [int] NULL,
 CONSTRAINT [PK_letter_types] PRIMARY KEY CLUSTERED 
(
	[letter_type] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 90) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Type of the letter. Each letter must has its own type code which is just a string.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'letter_types', @level2type=N'COLUMN',@level2name=N'letter_code'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Name of the spool queue for the letters. NULL means to print immediately.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'letter_types', @level2type=N'COLUMN',@level2name=N'queue_name'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Description for the letter This is also the menu location.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'letter_types', @level2type=N'COLUMN',@level2name=N'description'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'''OTHER'' = other, ''CL'' = client, ''CR'' = creditor, ''DEBT'' = debt letter' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'letter_types', @level2type=N'COLUMN',@level2name=N'letter_group'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Display mode for the letter. "-1" means "default".' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'letter_types', @level2type=N'COLUMN',@level2name=N'display_mode'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Date and time when the row was created' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'letter_types', @level2type=N'COLUMN',@level2name=N'date_created'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Name of the person who created the row' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'letter_types', @level2type=N'COLUMN',@level2name=N'created_by'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Type of the letter. Each letter type has a distinct label' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'letter_types', @level2type=N'COLUMN',@level2name=N'letter_type'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Region for the specific letter' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'letter_types', @level2type=N'COLUMN',@level2name=N'region'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Top margin in inches' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'letter_types', @level2type=N'COLUMN',@level2name=N'top_margin'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Let margin in inches' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'letter_types', @level2type=N'COLUMN',@level2name=N'left_margin'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Bottom margin in inches' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'letter_types', @level2type=N'COLUMN',@level2name=N'bottom_margin'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Right margin in inches' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'letter_types', @level2type=N'COLUMN',@level2name=N'right_margin'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Filename to the RTF letter template' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'letter_types', @level2type=N'COLUMN',@level2name=N'filename'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Language ID for the letter. This allows different languages for letter templates.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'letter_types', @level2type=N'COLUMN',@level2name=N'language'
GO
ALTER TABLE [dbo].[letter_types] ADD  CONSTRAINT [DF_letter_types_display_mode]  DEFAULT ((-1)) FOR [display_mode]
GO
ALTER TABLE [dbo].[letter_types] ADD  CONSTRAINT [DF_letter_types_top_margin]  DEFAULT ((1.5)) FOR [top_margin]
GO
ALTER TABLE [dbo].[letter_types] ADD  CONSTRAINT [DF_letter_types_left_margin]  DEFAULT ((1.25)) FOR [left_margin]
GO
ALTER TABLE [dbo].[letter_types] ADD  CONSTRAINT [DF_letter_types_bottom_margin]  DEFAULT ((1.0)) FOR [bottom_margin]
GO
ALTER TABLE [dbo].[letter_types] ADD  CONSTRAINT [DF_letter_types_right_margin]  DEFAULT ((1.0)) FOR [right_margin]
GO
