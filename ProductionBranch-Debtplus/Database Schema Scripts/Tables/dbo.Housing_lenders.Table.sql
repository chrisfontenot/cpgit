EXEC('ALTER TABLE dbo.Housing_lenders DROP CONSTRAINT FK_Housing_lenders_Housing_lender_servicers')
EXEC('ALTER TABLE dbo.Housing_lenders DROP CONSTRAINT FK_Housing_lenders_Investor')
EXEC('ALTER TABLE dbo.Housing_lenders DROP CONSTRAINT FK_Housing_lenders_Housing_CounselorContactOutcomeTypes')
EXEC('ALTER TABLE dbo.Housing_lenders DROP CONSTRAINT FK_Housing_lenders_Housing_CounselorContactReasonTypes')
EXEC('ALTER TABLE dbo.Housing_lenders DROP CONSTRAINT FK_Housing_lenders_Housing_ClientContactOutcomeTypes')
GO
execute UPDATE_ADD_COLUMN 'Housing_lenders', 'ClientConcatLenderOutcome', 'int'
execute UPDATE_ADD_COLUMN 'Housing_lenders', 'CnslrContactLenderOutcome', 'int'
execute UPDATE_ADD_COLUMN 'Housing_lenders', 'CnslrContactLenderReason', 'int'
GO
execute UPDATE_DROP_CONSTRAINTS 'Housing_lenders'
GO
/* To prevent any potential data loss issues, you should review this script in detail before running it outside the context of the database designer.*/
BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
GO
CREATE TABLE dbo.Tmp_Housing_lenders
	(
	oID dbo.typ_key NOT NULL IDENTITY (1, 1),
	ServicerID dbo.typ_key NULL,
	ServicerName varchar(80) NULL,
	AcctNum varchar(50) NULL,
	FdicNcusNum varchar(20) NULL,
	CaseNumber varchar(20) NULL,
	ClientConcatLenderOutcome dbo.typ_key NULL,
	CnslrContactLenderOutcome dbo.typ_key NULL,
	CnslrContactLenderReason dbo.typ_key NULL,
	InvestorID dbo.typ_key NULL,
	InvestorAccountNumber varchar(50) NULL,
	ContactLenderDate datetime NULL,
	AttemptContactDate datetime NULL,
	ContactLenderSuccess bit NOT NULL,
	WorkoutPlanDate datetime NULL,
	Corporate_Advance money NULL,
	Monthly_Desired_Repay_Amt money NULL,
	Last_Updated datetime NULL,
	Repay_Month int NULL,
	ts timestamp NOT NULL
	)  ON [PRIMARY]
GO
GRANT DELETE ON dbo.Tmp_Housing_lenders TO public  AS dbo
GO
GRANT INSERT ON dbo.Tmp_Housing_lenders TO public  AS dbo
GO
GRANT SELECT ON dbo.Tmp_Housing_lenders TO public  AS dbo
GO
GRANT UPDATE ON dbo.Tmp_Housing_lenders TO public  AS dbo
GO
DECLARE @v sql_variant 
SET @v = N'If the servicer is not defined then the name is kept here'
EXECUTE sp_addextendedproperty N'MS_Description', @v, N'SCHEMA', N'dbo', N'TABLE', N'Tmp_Housing_lenders', N'COLUMN', N'ServicerName'
GO
EXECUTE sp_bindefault N'dbo.default_string', N'dbo.Tmp_Housing_lenders.ServicerName'
GO
EXECUTE sp_bindefault N'dbo.default_false', N'dbo.Tmp_Housing_lenders.ContactLenderSuccess'
GO
EXECUTE sp_bindefault N'dbo.default_money', N'dbo.Tmp_Housing_lenders.Corporate_Advance'
GO
EXECUTE sp_bindefault N'dbo.default_money', N'dbo.Tmp_Housing_lenders.Monthly_Desired_Repay_Amt'
GO
SET IDENTITY_INSERT dbo.Tmp_Housing_lenders ON
GO
IF EXISTS(SELECT * FROM dbo.Housing_lenders)
	 EXEC('INSERT INTO dbo.Tmp_Housing_lenders (oID, ServicerID, ServicerName, AcctNum, FdicNcusNum, CaseNumber, ClientConcatLenderOutcome, CnslrContactLenderOutcome, CnslrContactLenderReason, InvestorID, InvestorAccountNumber, ContactLenderDate, AttemptContactDate, ContactLenderSuccess, WorkoutPlanDate, Corporate_Advance, Monthly_Desired_Repay_Amt, Last_Updated, Repay_Month)
		SELECT oID, ServicerID, ServicerName, AcctNum, FdicNcusNum, CaseNumber, ClientConcatLenderOutcome, CnslrContactLenderOutcome, CnslrContactLenderReason, InvestorID, InvestorAccountNumber, ContactLenderDate, AttemptContactDate, isnull(ContactLenderSuccess,0), WorkoutPlanDate, isnull(Corporate_Advance,0), isnull(Monthly_Desired_Repay_Amt,0), Last_Updated, Repay_Month FROM dbo.Housing_lenders WITH (HOLDLOCK TABLOCKX)')
GO
SET IDENTITY_INSERT dbo.Tmp_Housing_lenders OFF
GO
DROP TABLE dbo.Housing_lenders
GO
EXECUTE sp_rename N'dbo.Tmp_Housing_lenders', N'Housing_lenders', 'OBJECT' 
GO
ALTER TABLE dbo.Housing_lenders ADD CONSTRAINT
	PK_hpf_CaseLenderDTO PRIMARY KEY CLUSTERED 
	(
	oID
	) WITH( STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
ALTER TABLE dbo.Housing_lenders ADD CONSTRAINT
	FK_Housing_lenders_Housing_ClientContactOutcomeTypes FOREIGN KEY
	(
	ClientConcatLenderOutcome
	) REFERENCES dbo.Housing_ClientContactOutcomeTypes
	(
	oID
	) ON UPDATE  NO ACTION 
	 ON DELETE  NO ACTION 
GO
ALTER TABLE dbo.Housing_lenders ADD CONSTRAINT
	FK_Housing_lenders_Housing_CounselorContactReasonTypes FOREIGN KEY
	(
	CnslrContactLenderReason
	) REFERENCES dbo.Housing_CounselorContactReasonTypes
	(
	oID
	) ON UPDATE  NO ACTION 
	 ON DELETE  NO ACTION 
GO
ALTER TABLE dbo.Housing_lenders ADD CONSTRAINT
	FK_Housing_lenders_Housing_CounselorContactOutcomeTypes FOREIGN KEY
	(
	CnslrContactLenderOutcome
	) REFERENCES dbo.Housing_CounselorContactOutcomeTypes
	(
	oID
	) ON UPDATE  NO ACTION 
	 ON DELETE  NO ACTION 
GO
ALTER TABLE dbo.Housing_lenders ADD CONSTRAINT
	FK_Housing_lenders_Investor FOREIGN KEY
	(
	InvestorID
	) REFERENCES dbo.Investor
	(
	ID
	) ON UPDATE  NO ACTION 
	 ON DELETE  NO ACTION 
GO
ALTER TABLE dbo.Housing_lenders ADD CONSTRAINT
	FK_Housing_lenders_Housing_lender_servicers FOREIGN KEY
	(
	ServicerID
	) REFERENCES dbo.Housing_lender_servicers
	(
	oID
	) ON UPDATE  NO ACTION 
	 ON DELETE  NO ACTION 
GO
DENY DELETE ON dbo.Housing_lenders TO www_role  AS dbo 
GO
DENY INSERT ON dbo.Housing_lenders TO www_role  AS dbo 
GO
DENY SELECT ON dbo.Housing_lenders TO www_role  AS dbo 
GO
DENY UPDATE ON dbo.Housing_lenders TO www_role  AS dbo 
GO
COMMIT
GO
