USE [DebtPlus]
GO
/****** Object:  Table [dbo].[disbursement_clients]    Script Date: 09/15/2014 13:13:26 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[disbursement_clients](
	[oID] [dbo].[typ_key] IDENTITY(1,1) NOT NULL,
	[disbursement_register] [dbo].[typ_key] NOT NULL,
	[client] [dbo].[typ_client] NOT NULL,
	[held_in_trust] [money] NOT NULL,
	[reserved_in_trust] [money] NOT NULL,
	[disbursement_factor] [money] NOT NULL,
	[note_count] [int] NOT NULL,
	[client_register] [dbo].[typ_key] NULL,
	[active_status] [varchar](4) NULL,
	[start_date] [datetime] NULL,
	[region] [dbo].[typ_key] NOT NULL,
	[counselor] [dbo].[typ_key] NULL,
	[csr] [dbo].[typ_key] NULL,
	[office] [dbo].[typ_key] NULL,
	[last_name] [varchar](1) NULL,
	[date_created] [dbo].[typ_date] NOT NULL,
	[created_by] [dbo].[typ_counselor] NOT NULL,
 CONSTRAINT [PK_disbursement_clients] PRIMARY KEY CLUSTERED 
(
	[oID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Pointer to the disbursement register describing this disbursement.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'disbursement_clients', @level2type=N'COLUMN',@level2name=N'disbursement_register'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Pointer to the client for this disbursement.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'disbursement_clients', @level2type=N'COLUMN',@level2name=N'client'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Original trust balance. Used in selecting clients. Forms the maximum amount disbursed from the client trust.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'disbursement_clients', @level2type=N'COLUMN',@level2name=N'held_in_trust'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Minimum amount to be left in the trust after the disbursement' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'disbursement_clients', @level2type=N'COLUMN',@level2name=N'reserved_in_trust'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Sum of the disbursement factors for the debts. Used with the trust balance to determine the relative order.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'disbursement_clients', @level2type=N'COLUMN',@level2name=N'disbursement_factor'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Number of notes. Used to determine the relative order.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'disbursement_clients', @level2type=N'COLUMN',@level2name=N'note_count'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'When paid, this is the pointer to the registers_client table entry.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'disbursement_clients', @level2type=N'COLUMN',@level2name=N'client_register'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Client active status when selected' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'disbursement_clients', @level2type=N'COLUMN',@level2name=N'active_status'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Client''s start date' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'disbursement_clients', @level2type=N'COLUMN',@level2name=N'start_date'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Region number for this client' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'disbursement_clients', @level2type=N'COLUMN',@level2name=N'region'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Name of the counselor who created the row.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'disbursement_clients', @level2type=N'COLUMN',@level2name=N'date_created'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Date/Time when the row was created.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'disbursement_clients', @level2type=N'COLUMN',@level2name=N'created_by'
GO
