USE [DebtPlus]
GO
/****** Object:  Table [dbo].[intake_budgets]    Script Date: 09/15/2014 13:13:26 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[intake_budgets](
	[intake_budget] [dbo].[typ_key] IDENTITY(1,1) NOT NULL,
	[intake_client] [dbo].[typ_key] NOT NULL,
	[budget_category] [dbo].[typ_key] NULL,
	[other_budget_category] [dbo].[typ_description] NULL,
	[client_amount] [money] NOT NULL,
	[date_created] [dbo].[typ_date] NOT NULL,
	[created_by] [dbo].[typ_counselor] NOT NULL,
 CONSTRAINT [PK_intake_budgets] PRIMARY KEY CLUSTERED 
(
	[intake_budget] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 90) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
