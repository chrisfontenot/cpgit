USE [DebtPlus]
GO
/****** Object:  Table [dbo].[letter_queue]    Script Date: 09/15/2014 13:13:26 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[letter_queue](
	[letter_queue] [dbo].[typ_key] IDENTITY(1,1) NOT NULL,
	[queue_name] [dbo].[typ_description] NOT NULL,
	[letter_type] [dbo].[typ_key] NOT NULL,
	[client] [dbo].[typ_client] NULL,
	[creditor] [dbo].[typ_key] NULL,
	[sort_order] [varchar](15) NOT NULL,
	[priority] [int] NOT NULL,
	[date_printed] [datetime] NULL,
	[death_date] [datetime] NULL,
	[top_margin] [float] NOT NULL,
	[bottom_margin] [float] NOT NULL,
	[left_margin] [float] NOT NULL,
	[right_margin] [float] NOT NULL,
	[letter_fields] [text] NOT NULL,
	[formatted_letter] [text] NULL,
	[date_created] [dbo].[typ_date] NOT NULL,
	[created_by] [dbo].[typ_counselor] NOT NULL,
 CONSTRAINT [PK_letter_queue] PRIMARY KEY CLUSTERED 
(
	[letter_queue] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 90) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Primary key to the table.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'letter_queue', @level2type=N'COLUMN',@level2name=N'letter_queue'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Name of the queue for this entry.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'letter_queue', @level2type=N'COLUMN',@level2name=N'queue_name'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Pointer to the letter that was actually printed' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'letter_queue', @level2type=N'COLUMN',@level2name=N'letter_type'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'If this is a client, then the client ID is stored here' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'letter_queue', @level2type=N'COLUMN',@level2name=N'client'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'If this is a creditor / debt, the creditor ID is stored here' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'letter_queue', @level2type=N'COLUMN',@level2name=N'creditor'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Order for sorting purposes when the queue is printed' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'letter_queue', @level2type=N'COLUMN',@level2name=N'sort_order'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Printing priority value' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'letter_queue', @level2type=N'COLUMN',@level2name=N'priority'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'When the letter is printed, this is the date/time. A null value indicates that the letter has not been printed.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'letter_queue', @level2type=N'COLUMN',@level2name=N'date_printed'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Date when the entry is to be removed from the queue' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'letter_queue', @level2type=N'COLUMN',@level2name=N'death_date'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Formatted letter code. This is RTF text of the formatted letter image, exactly as it is to be printed.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'letter_queue', @level2type=N'COLUMN',@level2name=N'formatted_letter'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'This is the date/time when the row was created' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'letter_queue', @level2type=N'COLUMN',@level2name=N'date_created'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'This is the person who created the row.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'letter_queue', @level2type=N'COLUMN',@level2name=N'created_by'
GO
ALTER TABLE [dbo].[letter_queue] ADD  CONSTRAINT [DF_letter_queue_sort_order]  DEFAULT ('') FOR [sort_order]
GO
ALTER TABLE [dbo].[letter_queue] ADD  CONSTRAINT [DF_letter_queue_letter_fields]  DEFAULT ('<letter_fields />') FOR [letter_fields]
GO
