USE [DebtPlus]
GO
/****** Object:  Table [dbo].[hud_transactions]    Script Date: 09/15/2014 13:13:26 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[hud_transactions](
	[hud_transaction] [dbo].[typ_key] IDENTITY(1,1) NOT NULL,
	[hud_interview] [dbo].[typ_key] NOT NULL,
	[minutes] [int] NOT NULL,
	[GrantAmountUsed] [money] NOT NULL,
	[client_appointment] [dbo].[typ_key] NULL,
	[date_created] [dbo].[typ_date] NOT NULL,
	[created_by] [dbo].[typ_counselor] NOT NULL,
	[old_hud_transaction] [int] NULL,
 CONSTRAINT [PK_hud_transactions] PRIMARY KEY CLUSTERED 
(
	[hud_transaction] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Primary key to the table.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'hud_transactions', @level2type=N'COLUMN',@level2name=N'hud_transaction'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Pointer to the associated interview record' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'hud_transactions', @level2type=N'COLUMN',@level2name=N'hud_interview'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Number of minutes for this transaction' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'hud_transactions', @level2type=N'COLUMN',@level2name=N'minutes'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Amount of grant money used for this transaction' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'hud_transactions', @level2type=N'COLUMN',@level2name=N'GrantAmountUsed'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'If the transaction occured during an appointment, this is the appointment ID.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'hud_transactions', @level2type=N'COLUMN',@level2name=N'client_appointment'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Date and time when the row was created' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'hud_transactions', @level2type=N'COLUMN',@level2name=N'date_created'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Name of the counselor who created the row' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'hud_transactions', @level2type=N'COLUMN',@level2name=N'created_by'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'List of times associated with the interviews for HUD' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'hud_transactions'
GO
