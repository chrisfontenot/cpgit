EXECUTE UPDATE_DROP_CONSTRAINTS 'vendor_contacts'
GO
/* To prevent any potential data loss issues, you should review this script in detail before running it outside the context of the database designer.*/
BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION
GO
CREATE TABLE dbo.Tmp_vendor_contacts
	(
	oID dbo.typ_key NOT NULL IDENTITY (1, 1),
	vendor dbo.typ_key NOT NULL,
	contact_type dbo.typ_key NOT NULL,
	nameID dbo.typ_key NULL,
	addressID dbo.typ_key NULL,
	telephoneID dbo.typ_key NULL,
	faxID dbo.typ_key NULL,
	emailID dbo.typ_key NULL,
	note varchar(256) NULL,
	date_created dbo.typ_date NOT NULL,
	created_by dbo.typ_counselor NOT NULL,
	ts timestamp NOT NULL,
	Title varchar(150) NULL
	)  ON [PRIMARY]
GO
ALTER TABLE dbo.Tmp_vendor_contacts SET (LOCK_ESCALATION = TABLE)
GO
IF EXISTS(SELECT * FROM SYSOBJECTS WHERE TYPE='U' AND NAME=N'vendor_contacts')
BEGIN
	EXEC('SET IDENTITY_INSERT dbo.Tmp_vendor_contacts ON;
		 INSERT INTO dbo.Tmp_vendor_contacts (oID, vendor, contact_type, nameID, addressID, telephoneID, faxID, emailID, note, date_created, created_by, Title)
		 SELECT oID, vendor, contact_type, nameID, addressID, telephoneID, faxID, emailID, note, date_created, created_by, Title FROM dbo.vendor_contacts WITH (HOLDLOCK TABLOCKX);
	     SET IDENTITY_INSERT dbo.Tmp_vendor_contacts OFF;')
	EXEC ('DROP TABLE dbo.vendor_contacts')
END
GO
EXECUTE sp_rename N'dbo.Tmp_vendor_contacts', N'vendor_contacts', 'OBJECT' 
GO
ALTER TABLE dbo.vendor_contacts ADD CONSTRAINT
	PK_vendor_contacts PRIMARY KEY CLUSTERED 
	(
	oID
	) WITH( STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
ALTER TABLE dbo.vendor_contacts ADD CONSTRAINT
	FK_vendor_contacts_vendors FOREIGN KEY
	(
	vendor
	) REFERENCES dbo.vendors
	(
	oID
	) ON UPDATE  CASCADE 
	 ON DELETE  CASCADE 
GO
ALTER TABLE dbo.vendor_contacts ADD CONSTRAINT
	FK_vendor_contacts_vendor_contact_types FOREIGN KEY
	(
	contact_type
	) REFERENCES dbo.vendor_contact_types
	(
	oID
	) ON UPDATE  CASCADE 
	 ON DELETE  CASCADE 
GO
CREATE TRIGGER [dbo].[trig_vendor_contacts_D_1] ON dbo.vendor_contacts AFTER DELETE AS
BEGIN
	set nocount on

	-- Discard the names, addresses, etc. from the corresponding object tables
	delete		names
	from		names n
	inner join	deleted d on d.nameID = n.name
	where		d.nameID is not null;

	delete		addresses
	from		addresses a
	inner join	deleted d on d.addressID = a.address
	where		d.addressID is not null;

	delete		telephonenumbers
	from		telephonenumbers t
	inner join	deleted d on d.TelephoneID = t.telephonenumber
	where		d.TelephoneID is not null;

	delete		telephonenumbers
	from		telephonenumbers t
	inner join	deleted d on d.faxID = t.telephonenumber
	where		d.faxID is not null;

	delete		emailaddresses
	from		emailaddresses e
	inner join	deleted d on d.emailID = e.email
	where		d.emailID is not null;
END
GO
COMMIT
