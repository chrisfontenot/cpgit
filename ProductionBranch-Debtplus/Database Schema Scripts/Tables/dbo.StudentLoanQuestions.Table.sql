USE [DebtPlus]
GO
/****** Object:  Table [dbo].[StudentLoanQuestions]    Script Date: 09/15/2014 13:13:26 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[StudentLoanQuestions](
	[oID] [nvarchar](80) NOT NULL,
	[question] [nvarchar](800) NULL,
	[answers] [nvarchar](800) NULL,
 CONSTRAINT [PK_StudentLoanQuestions_1] PRIMARY KEY CLUSTERED 
(
	[oID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
