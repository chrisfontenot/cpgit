USE [DebtPlus]
GO
/****** Object:  Table [dbo].[rpps_biller_ids]    Script Date: 09/15/2014 13:13:26 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[rpps_biller_ids](
	[rpps_biller_id] [dbo].[typ_rpps_biller_id] NOT NULL,
	[Mastercard_Key] [int] NULL,
	[reversal_biller_id] [dbo].[typ_rpps_biller_id] NULL,
	[effdate] [datetime] NULL,
	[livedate] [datetime] NULL,
	[biller_aba] [varchar](10) NULL,
	[biller_name] [dbo].[typ_rpps_biller_name] NULL,
	[biller_class] [dbo].[typ_description] NULL,
	[biller_type] [int] NOT NULL,
	[currency] [int] NULL,
	[payment_prenote] [bit] NOT NULL,
	[proposal_prenote] [bit] NOT NULL,
	[dmppayonly] [bit] NOT NULL,
	[guaranteed_funds] [bit] NOT NULL,
	[pvt_biller_id] [bit] NOT NULL,
	[note] [varchar](1024) NULL,
	[send_cdp] [bit] NOT NULL,
	[send_fbc] [bit] NOT NULL,
	[send_fbd] [bit] NOT NULL,
	[send_cdv] [bit] NOT NULL,
	[send_cdd] [bit] NOT NULL,
	[send_cdn] [bit] NOT NULL,
	[send_cdf] [bit] NOT NULL,
	[send_exception_pay] [bit] NOT NULL,
	[returns_cdr] [bit] NOT NULL,
	[returns_cdt] [bit] NOT NULL,
	[returns_cda] [bit] NOT NULL,
	[returns_cdv] [bit] NOT NULL,
	[returns_cdc] [bit] NOT NULL,
	[returns_cdm] [bit] NOT NULL,
	[reqAdndaRev] [bit] NOT NULL,
	[checkdigit] [bit] NOT NULL,
	[blroldname] [varchar](1024) NULL,
	[date_created] [dbo].[typ_date] NOT NULL,
	[created_by] [dbo].[typ_counselor] NOT NULL,
 CONSTRAINT [PK_rpps_biller_ids] PRIMARY KEY CLUSTERED 
(
	[rpps_biller_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 90) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'RPPS Biller ID from MasterCard. This is the primary key.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'rpps_biller_ids', @level2type=N'COLUMN',@level2name=N'rpps_biller_id'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Reversal biller ID associated with modified gross billers' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'rpps_biller_ids', @level2type=N'COLUMN',@level2name=N'reversal_biller_id'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Effective date when read from Mastercard. Not used, but stored for display purposes only.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'rpps_biller_ids', @level2type=N'COLUMN',@level2name=N'effdate'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Name of the biller when creating transactions.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'rpps_biller_ids', @level2type=N'COLUMN',@level2name=N'biller_name'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Class for the biller, i.e. "Bank" or "Retail"' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'rpps_biller_ids', @level2type=N'COLUMN',@level2name=N'biller_class'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Type of the biller (1 = Net, 2 = Gross, 3 = Modified Gross)' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'rpps_biller_ids', @level2type=N'COLUMN',@level2name=N'biller_type'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Does the biller accept prenotes?' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'rpps_biller_ids', @level2type=N'COLUMN',@level2name=N'payment_prenote'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Does the biller accept proposals on prenotes?' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'rpps_biller_ids', @level2type=N'COLUMN',@level2name=N'proposal_prenote'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Does the biller require guaranteed funds?' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'rpps_biller_ids', @level2type=N'COLUMN',@level2name=N'guaranteed_funds'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'For EDI, send the creditor a proposal record' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'rpps_biller_ids', @level2type=N'COLUMN',@level2name=N'send_cdp'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'If sending a proposal, include the FBC (full budget creditor) records' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'rpps_biller_ids', @level2type=N'COLUMN',@level2name=N'send_fbc'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'If sending proposals, send FBD (full budget disclosure) records' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'rpps_biller_ids', @level2type=N'COLUMN',@level2name=N'send_fbd'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'If sending proposals, should a balance verification be sent' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'rpps_biller_ids', @level2type=N'COLUMN',@level2name=N'send_cdv'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'If sending proposals, should a drop notification be sent' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'rpps_biller_ids', @level2type=N'COLUMN',@level2name=N'send_cdd'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Date/Time when the row was created.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'rpps_biller_ids', @level2type=N'COLUMN',@level2name=N'date_created'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Person who created the row.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'rpps_biller_ids', @level2type=N'COLUMN',@level2name=N'created_by'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'This table holds the biller IDs that are known to the RPPS system. For a creditor to be considered valid for RPPS, the biller ID must be listed in this table.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'rpps_biller_ids'
GO
ALTER TABLE [dbo].[rpps_biller_ids] ADD  CONSTRAINT [DF_rpps_biller_ids_biller_type]  DEFAULT ((1)) FOR [biller_type]
GO
