USE [DebtPlus]
GO
/****** Object:  Table [dbo].[appt_time_templates]    Script Date: 09/15/2014 13:13:25 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[appt_time_templates](
	[appt_time_template] [dbo].[typ_key] IDENTITY(1,1) NOT NULL,
	[office] [dbo].[typ_key] NOT NULL,
	[dow] [int] NOT NULL,
	[start_time] [int] NOT NULL,
	[appt_type] [dbo].[typ_key] NULL,
	[date_created] [dbo].[typ_date] NOT NULL,
	[created_by] [dbo].[typ_counselor] NOT NULL,
 CONSTRAINT [PK_appt_time_templates] PRIMARY KEY CLUSTERED 
(
	[appt_time_template] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Appointment time template record ID' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'appt_time_templates', @level2type=N'COLUMN',@level2name=N'appt_time_template'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Office ID for the appointment' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'appt_time_templates', @level2type=N'COLUMN',@level2name=N'office'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Day of the week for the appointment (1 = Sun, ..., 6 = Sat)' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'appt_time_templates', @level2type=N'COLUMN',@level2name=N'dow'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Starting time (minutes since midnight) for the appointment' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'appt_time_templates', @level2type=N'COLUMN',@level2name=N'start_time'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Type of the appointment in appt_types table' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'appt_time_templates', @level2type=N'COLUMN',@level2name=N'appt_type'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Date/Time when the row was created.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'appt_time_templates', @level2type=N'COLUMN',@level2name=N'date_created'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Person who created the row.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'appt_time_templates', @level2type=N'COLUMN',@level2name=N'created_by'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'This table lists the appointment times that are defined in the prototype week.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'appt_time_templates'
GO
