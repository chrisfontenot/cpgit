EXECUTE UPDATE_DROP_CONSTRAINTS 'product_transaction_types'
GO
/* To prevent any potential data loss issues, you should review this script in detail before running it outside the context of the database designer.*/
BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION
GO
CREATE TABLE dbo.Tmp_product_transaction_types
	(
	oID varchar(2) NOT NULL,
	Description dbo.typ_description NOT NULL,
	date_created dbo.typ_date NOT NULL,
	created_by dbo.typ_counselor NOT NULL,
	ts timestamp NOT NULL
	)  ON [PRIMARY]
GO
ALTER TABLE dbo.Tmp_product_transaction_types SET (LOCK_ESCALATION = TABLE)
GO
IF EXISTS(SELECT * FROM SYSOBJECTS WHERE TYPE='U' AND NAME=N'product_transaction_types')
BEGIN
	EXEC('INSERT INTO dbo.Tmp_product_transaction_types (oID, Description, date_created, created_by)
		SELECT oID, Description, date_created, created_by FROM dbo.product_transaction_types WITH (HOLDLOCK TABLOCKX)')
	EXEC('DROP TABLE dbo.product_transaction_types')
END
GO
EXECUTE sp_rename N'dbo.Tmp_product_transaction_types', N'product_transaction_types', 'OBJECT' 
GO
ALTER TABLE dbo.product_transaction_types ADD CONSTRAINT
	PK_product_transaction_types PRIMARY KEY CLUSTERED 
	(
	oID
	) WITH( STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
COMMIT
BEGIN TRANSACTION
GO
ALTER TABLE dbo.product_transactions ADD CONSTRAINT
	FK_product_transactions_vendor_transaction_types FOREIGN KEY
	(
	transaction_type
	) REFERENCES dbo.product_transaction_types
	(
	oID
	) ON UPDATE  NO ACTION 
	 ON DELETE  NO ACTION 
GO
ALTER TABLE dbo.product_transactions SET (LOCK_ESCALATION = TABLE)
GO
COMMIT
