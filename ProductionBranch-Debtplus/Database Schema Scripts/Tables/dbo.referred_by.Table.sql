/*
   Wednesday, November 04, 20156:30:56 AM
   User: sa
   Server: 10.0.199.12
   Database: DebtPlus_HPF
   Application: 
*/

/* To prevent any potential data loss issues, you should review this script in detail before running it outside the context of the database designer.*/
BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION
GO
EXECUTE UPDATE_ADD_COLUMN 'referred_by', 'hpf'
GO
CREATE TABLE dbo.Tmp_referred_by
	(
	referred_by dbo.typ_key NOT NULL IDENTITY (1, 1),
	description dbo.typ_description NOT NULL,
	referred_by_type dbo.typ_key NULL,
	[Default] bit NOT NULL,
	ActiveFlag bit NOT NULL,
	note dbo.typ_message NULL,
	partnerValidation varchar(50) NULL,
	hud_9902_section varchar(256) NULL,
	date_created dbo.typ_date NOT NULL,
	created_by dbo.typ_counselor NOT NULL,
	ts timestamp NOT NULL,
	RxOffice varchar(20) NULL,
	hpf varchar(50) NULL
	)  ON [PRIMARY]
GO
GRANT DELETE ON dbo.Tmp_referred_by TO public  AS dbo
GO
GRANT INSERT ON dbo.Tmp_referred_by TO public  AS dbo
GO
GRANT SELECT ON dbo.Tmp_referred_by TO public  AS dbo
GO
GRANT UPDATE ON dbo.Tmp_referred_by TO public  AS dbo
GO
DECLARE @v sql_variant 
SET @v = N'Section for reporting referral sources on the HUD 9902 report'
EXECUTE sp_addextendedproperty N'MS_Description', @v, N'SCHEMA', N'dbo', N'TABLE', N'Tmp_referred_by', N'COLUMN', N'hud_9902_section'
GO
EXECUTE sp_bindefault N'dbo.default_false', N'dbo.Tmp_referred_by.[Default]'
GO
EXECUTE sp_bindefault N'dbo.default_true', N'dbo.Tmp_referred_by.ActiveFlag'
GO
SET IDENTITY_INSERT dbo.Tmp_referred_by ON
GO
IF EXISTS(SELECT * FROM dbo.referred_by)
	 EXEC('INSERT INTO dbo.Tmp_referred_by (referred_by, description, referred_by_type, [Default], ActiveFlag, note, partnerValidation, hud_9902_section, date_created, created_by, RxOffice, hpf)
		SELECT referred_by, description, referred_by_type, [Default], ActiveFlag, note, partnerValidation, hud_9902_section, date_created, created_by, RxOffice, hpf FROM dbo.referred_by WITH (HOLDLOCK TABLOCKX)')
GO
SET IDENTITY_INSERT dbo.Tmp_referred_by OFF
GO
DROP TABLE dbo.referred_by
GO
EXECUTE sp_rename N'dbo.Tmp_referred_by', N'referred_by', 'OBJECT' 
GO
ALTER TABLE dbo.referred_by ADD CONSTRAINT
	PK_referred_by PRIMARY KEY CLUSTERED 
	(
	referred_by
	) WITH( PAD_INDEX = OFF, FILLFACTOR = 90, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]

GO
DENY DELETE ON dbo.referred_by TO www_role  AS dbo 
GO
DENY INSERT ON dbo.referred_by TO www_role  AS dbo 
GO
DENY SELECT ON dbo.referred_by TO www_role  AS dbo 
GO
DENY UPDATE ON dbo.referred_by TO www_role  AS dbo 
GO
COMMIT
