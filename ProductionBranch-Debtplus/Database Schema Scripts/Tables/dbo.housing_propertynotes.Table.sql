/* To prevent any potential data loss issues, you should review this script in detail before running it outside the context of the database designer.*/
BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION
GO
CREATE TABLE dbo.Tmp_Housing_PropertyNotes
	(
	oID dbo.typ_key NOT NULL IDENTITY (1, 1),
	PropertyID dbo.typ_key NOT NULL,
	NoteBuffer varchar(8000) NULL,
	date_created dbo.typ_date NOT NULL,
	created_by dbo.typ_counselor NOT NULL,
	ts timestamp NOT NULL
	)  ON [PRIMARY]
GO
GRANT DELETE ON dbo.Tmp_Housing_PropertyNotes TO public  AS dbo
GO
GRANT INSERT ON dbo.Tmp_Housing_PropertyNotes TO public  AS dbo
GO
GRANT SELECT ON dbo.Tmp_Housing_PropertyNotes TO public  AS dbo
GO
GRANT UPDATE ON dbo.Tmp_Housing_PropertyNotes TO public  AS dbo
GO
SET IDENTITY_INSERT dbo.Tmp_Housing_PropertyNotes ON
GO
IF EXISTS(SELECT * FROM SYSOBJECTS WHERE NAME=N'Housing_PropertyNotes' AND TYPE='U')
BEGIN
	EXEC('INSERT INTO dbo.Tmp_Housing_PropertyNotes (oID, PropertyID, NoteBuffer, date_created, created_by)
		SELECT oID, PropertyID, NoteBuffer, date_created, created_by FROM dbo.Housing_PropertyNotes WITH (HOLDLOCK TABLOCKX)')
	EXEC('DROP TABLE dbo.Housing_PropertyNotes')
END
GO
SET IDENTITY_INSERT dbo.Tmp_Housing_PropertyNotes OFF
GO
EXECUTE sp_rename N'dbo.Tmp_Housing_PropertyNotes', N'Housing_PropertyNotes', 'OBJECT' 
GO
ALTER TABLE dbo.Housing_PropertyNotes ADD CONSTRAINT
	PK_Housing_PropertyNotes PRIMARY KEY CLUSTERED 
	(
	oID
	) WITH( STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX IX_Housing_PropertyNotes_1 ON dbo.Housing_PropertyNotes
	(
	PropertyID,
	date_created DESC
	) WITH( STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
DENY DELETE ON dbo.Housing_PropertyNotes TO www_role  AS dbo 
GO
DENY INSERT ON dbo.Housing_PropertyNotes TO www_role  AS dbo 
GO
DENY SELECT ON dbo.Housing_PropertyNotes TO www_role  AS dbo 
GO
DENY UPDATE ON dbo.Housing_PropertyNotes TO www_role  AS dbo 
GO
COMMIT
