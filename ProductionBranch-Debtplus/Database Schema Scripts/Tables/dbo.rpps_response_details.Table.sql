USE [DebtPlus]
GO
/****** Object:  Table [dbo].[rpps_response_details]    Script Date: 09/15/2014 13:13:26 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[rpps_response_details](
	[rpps_response_detail] [dbo].[typ_key] IDENTITY(1,1) NOT NULL,
	[rpps_response_file] [dbo].[typ_key] NOT NULL,
	[rpps_biller_id] [dbo].[typ_rpps_biller_id] NULL,
	[rpps_transaction] [dbo].[typ_key] NULL,
	[service_class_or_purpose] [varchar](3) NOT NULL,
	[transaction_code] [int] NOT NULL,
	[return_code] [dbo].[typ_rps_result] NULL,
	[addendum_count] [int] NULL,
	[trace_number] [dbo].[typ_trace_number] NULL,
	[net] [money] NULL,
	[gross] [money] NULL,
	[consumer_name] [varchar](15) NULL,
	[account_number] [dbo].[typ_client_account] NULL,
	[processing_error] [dbo].[typ_description] NULL,
	[addendum_code] [varchar](2) NULL,
	[addendum_date] [varchar](6) NULL,
	[addendum_routing] [varchar](8) NULL,
	[addendum_information] [varchar](50) NULL,
 CONSTRAINT [PK_rpps_response_details] PRIMARY KEY CLUSTERED 
(
	[rpps_response_detail] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 90) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
ALTER TABLE [dbo].[rpps_response_details] ADD  CONSTRAINT [DF_rpps_response_details_transaction_code]  DEFAULT ((22)) FOR [transaction_code]
GO
