USE [DebtPlus]
GO
/****** Object:  Table [dbo].[rpps_response_details_cdr]    Script Date: 09/15/2014 13:13:26 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[rpps_response_details_cdr](
	[rpps_response_details_cdr] [dbo].[typ_key] IDENTITY(1,1) NOT NULL,
	[rpps_response_detail] [dbo].[typ_key] NOT NULL,
	[last_communication] [dbo].[typ_trace_number] NULL,
	[company_identification] [varchar](6) NULL,
	[ssn] [dbo].[typ_ssn] NULL,
	[client] [dbo].[typ_client] NULL,
	[reject_reason] [dbo].[typ_rps_result] NULL,
	[counter_offer] [money] NULL,
	[additional_info_required] [varchar](18) NULL,
	[resubmit_date] [datetime] NULL,
	[cycle_months_remaining] [int] NULL,
	[forgiven_pct] [float] NULL,
	[first_payment_date] [datetime] NULL,
	[good_thru_date] [datetime] NULL,
	[ineligible_reason] [varchar](10) NULL,
	[internal_program_ends_date] [datetime] NULL,
	[interest_rate] [float] NULL,
	[third_party_detail] [varchar](10) NULL,
	[third_party_contact] [varchar](80) NULL,
 CONSTRAINT [PK_rpps_response_details_cdr] PRIMARY KEY CLUSTERED 
(
	[rpps_response_details_cdr] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 90) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
