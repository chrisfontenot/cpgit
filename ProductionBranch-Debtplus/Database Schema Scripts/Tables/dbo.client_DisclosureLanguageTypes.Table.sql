EXECUTE UPDATE_DROP_CONSTRAINTS 'client_DisclosureLanguageTypes'
EXECUTE UPDATE_ADD_COLUMN 'client_DisclosureLanguageTypes', 'effective_date', 'datetime'
EXECUTE UPDATE_ADD_COLUMN 'client_DisclosureLanguageTypes', 'expiration_date', 'datetime'
GO
/* To prevent any potential data loss issues, you should review this script in detail before running it outside the context of the database designer.*/
BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION
GO
CREATE TABLE dbo.Tmp_client_DisclosureLanguageTypes
	(
	oID dbo.typ_key NOT NULL IDENTITY (1, 1),
	clientDisclosureTypeID dbo.typ_key NOT NULL,
	languageID dbo.typ_key NOT NULL,
	template_url varchar(512) NOT NULL,
	revision_date datetime NOT NULL,
	effective_date datetime NOT NULL,
	expiration_date datetime NULL,
	created_by dbo.typ_counselor NOT NULL,
	date_created dbo.typ_date NOT NULL,
	ts timestamp NOT NULL
	)  ON [PRIMARY]
GO
EXECUTE sp_bindefault N'dbo.default_date', N'dbo.Tmp_client_DisclosureLanguageTypes.revision_date'
GO
EXECUTE sp_bindefault N'dbo.default_date', N'dbo.Tmp_client_DisclosureLanguageTypes.effective_date'
GO
SET IDENTITY_INSERT dbo.Tmp_client_DisclosureLanguageTypes ON
GO
IF EXISTS(SELECT * FROM SYSOBJECTS WHERE NAME=N'client_DisclosureLanguageTypes' AND TYPE='U')
BEGIN
	EXEC('INSERT INTO dbo.Tmp_client_DisclosureLanguageTypes (oID, clientDisclosureTypeID, languageID, template_url, revision_date, created_by, date_created, effective_date, expiration_date) SELECT oID, clientDisclosureTypeID, languageID, template_url, revision_date, created_by, date_created, coalesce(effective_date, date_created, getdate()), expiration_date FROM dbo.client_DisclosureLanguageTypes WITH (HOLDLOCK TABLOCKX)')
	EXEC('DROP TABLE dbo.client_DisclosureLanguageTypes')
END
GO
SET IDENTITY_INSERT dbo.Tmp_client_DisclosureLanguageTypes OFF
GO
EXECUTE sp_rename N'dbo.Tmp_client_DisclosureLanguageTypes', N'client_DisclosureLanguageTypes', 'OBJECT' 
GO
ALTER TABLE dbo.client_DisclosureLanguageTypes ADD CONSTRAINT
	PK_client_DisclosureLanguageTypes PRIMARY KEY CLUSTERED 
	(
	oID
	) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX IX_client_DisclosureLanguageTypes_1 ON dbo.client_DisclosureLanguageTypes
	(
	clientDisclosureTypeID,
	languageID
	) ON [PRIMARY]
GO
DECLARE @v sql_variant 
SET @v = N'Disclosure Type & Language ID index'
EXECUTE sp_addextendedproperty N'MS_Description', @v, N'SCHEMA', N'dbo', N'TABLE', N'client_DisclosureLanguageTypes', N'INDEX', N'IX_client_DisclosureLanguageTypes_1'
GO
ALTER TABLE dbo.client_DisclosureLanguageTypes ADD CONSTRAINT
	FK_client_DisclosureLanguageTypes_AttributeTypes FOREIGN KEY
	(
	languageID
	) REFERENCES dbo.AttributeTypes
	(
	oID
	) ON UPDATE  NO ACTION 
	 ON DELETE  NO ACTION 
GO
ALTER TABLE dbo.client_DisclosureLanguageTypes ADD CONSTRAINT
	FK_client_DisclosureLanguageTypes_client_DisclosureTypes FOREIGN KEY
	(
	clientDisclosureTypeID
	) REFERENCES dbo.client_DisclosureTypes
	(
	oID
	) ON UPDATE  CASCADE 
	 ON DELETE  CASCADE 
GO
COMMIT
BEGIN TRANSACTION
GO
ALTER TABLE dbo.client_disclosures ADD CONSTRAINT
	FK_client_disclosures_client_DisclosureLanguageTypes FOREIGN KEY
	(
	disclosureID
	) REFERENCES dbo.client_DisclosureLanguageTypes
	(
	oID
	) ON UPDATE  CASCADE 
	 ON DELETE  CASCADE 
GO
COMMIT
GO
EXEC ('GRANT SELECT,UPDATE,INSERT,DELETE ON client_DisclosureLanguageTypes TO public AS dbo')
GO
EXEC ('DENY SELECT,UPDATE,INSERT,DELETE ON client_DisclosureLanguageTypes TO www_role')
GO
