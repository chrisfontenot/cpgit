/* To prevent any potential data loss issues, you should review this script in detail before running it outside the context of the database designer.*/
BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION
GO
EXECUTE UPDATE_ADD_COLUMN 'MaritalTypes', 'hpf'
GO
CREATE TABLE dbo.Tmp_MaritalTypes
	(
	oID dbo.typ_key NOT NULL IDENTITY (1, 1),
	description dbo.typ_description NOT NULL,
	[default] bit NOT NULL,
	ActiveFlag bit NOT NULL,
	note dbo.typ_message NULL,
	nfcc varchar(4) NULL,
	rpps varchar(4) NULL,
	epay varchar(4) NULL,
	hud_9902_section varchar(256) NULL,
	RxOffice varchar(20) NULL,
	hpf varchar(50) NULL,
	date_created dbo.typ_date NOT NULL,
	created_by dbo.typ_counselor NOT NULL,
	ts timestamp NOT NULL
	)  ON [PRIMARY]
GO
GRANT DELETE ON dbo.Tmp_MaritalTypes TO public  AS dbo
GO
GRANT INSERT ON dbo.Tmp_MaritalTypes TO public  AS dbo
GO
GRANT SELECT ON dbo.Tmp_MaritalTypes TO public  AS dbo
GO
GRANT UPDATE ON dbo.Tmp_MaritalTypes TO public  AS dbo
GO
EXECUTE sp_bindefault N'dbo.default_false', N'dbo.Tmp_MaritalTypes.[default]'
GO
EXECUTE sp_bindefault N'dbo.default_true', N'dbo.Tmp_MaritalTypes.ActiveFlag'
GO
SET IDENTITY_INSERT dbo.Tmp_MaritalTypes ON
GO
IF EXISTS(SELECT * FROM dbo.MaritalTypes)
	 EXEC('INSERT INTO dbo.Tmp_MaritalTypes (oID, description, nfcc, rpps, epay, note, hud_9902_section, [default], ActiveFlag, date_created, created_by, RxOffice, hpf)
		SELECT oID, description, nfcc, rpps, epay, note, hud_9902_section, [default], ActiveFlag, date_created, created_by, RxOffice, hpf FROM dbo.MaritalTypes WITH (HOLDLOCK TABLOCKX)')
GO
SET IDENTITY_INSERT dbo.Tmp_MaritalTypes OFF
GO
DROP TABLE dbo.MaritalTypes
GO
EXECUTE sp_rename N'dbo.Tmp_MaritalTypes', N'MaritalTypes', 'OBJECT' 
GO
ALTER TABLE dbo.MaritalTypes ADD CONSTRAINT
	PK_MaritalTypes PRIMARY KEY CLUSTERED 
	(
	oID
	) WITH( STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
DENY DELETE ON dbo.MaritalTypes TO www_role  AS dbo 
GO
DENY INSERT ON dbo.MaritalTypes TO www_role  AS dbo 
GO
DENY SELECT ON dbo.MaritalTypes TO www_role  AS dbo 
GO
DENY UPDATE ON dbo.MaritalTypes TO www_role  AS dbo 
GO
COMMIT
