USE [DebtPlus]
GO
/****** Object:  Table [dbo].[statistics_offices]    Script Date: 09/15/2014 13:13:26 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[statistics_offices](
	[statistic_office] [dbo].[typ_key] IDENTITY(1,1) NOT NULL,
	[period_start] [datetime] NOT NULL,
	[office] [int] NOT NULL,
	[active_clients] [int] NOT NULL,
	[clients_disbursed] [int] NOT NULL,
	[expected_disbursement] [money] NOT NULL,
	[actual_disbursement] [money] NOT NULL,
	[paf_fees] [money] NOT NULL,
	[date_created] [dbo].[typ_date] NOT NULL,
	[created_by] [dbo].[typ_counselor] NOT NULL,
 CONSTRAINT [PK_statistics_offices] PRIMARY KEY CLUSTERED 
(
	[statistic_office] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 90) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'This is the primary key to the table' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'statistics_offices', @level2type=N'COLUMN',@level2name=N'statistic_office'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Starting date for the period. This is the 1st of the month.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'statistics_offices', @level2type=N'COLUMN',@level2name=N'period_start'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Office number for the item.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'statistics_offices', @level2type=N'COLUMN',@level2name=N'office'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Number of active clients who list the office as their "office".' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'statistics_offices', @level2type=N'COLUMN',@level2name=N'active_clients'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Of the clients, how many were actually disbursed in this period.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'statistics_offices', @level2type=N'COLUMN',@level2name=N'clients_disbursed'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The expected disbursement amount based upon the scheduled_pay set at the start of the month.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'statistics_offices', @level2type=N'COLUMN',@level2name=N'expected_disbursement'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Actual disbursement from the "denorm_current_month_disbursement" in the client_creditor table.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'statistics_offices', @level2type=N'COLUMN',@level2name=N'actual_disbursement'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Amount disbursed to the P.A.F. creditor this month.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'statistics_offices', @level2type=N'COLUMN',@level2name=N'paf_fees'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Date/Time when the row was created.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'statistics_offices', @level2type=N'COLUMN',@level2name=N'date_created'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Person who created the row.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'statistics_offices', @level2type=N'COLUMN',@level2name=N'created_by'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Statistics for the offices based upon the disbursement information.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'statistics_offices'
GO
