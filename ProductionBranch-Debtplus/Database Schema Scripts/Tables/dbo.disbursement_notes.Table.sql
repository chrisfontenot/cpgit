USE [DebtPlus]
GO
/****** Object:  Table [dbo].[disbursement_notes]    Script Date: 09/15/2014 13:13:26 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[disbursement_notes](
	[disbursement_note] [dbo].[typ_key] IDENTITY(1,1) NOT NULL,
	[client] [dbo].[typ_client] NOT NULL,
	[deposit_batch_id] [dbo].[typ_key] NULL,
	[disbursement_register] [dbo].[typ_key] NULL,
	[prior_note] [dbo].[typ_key] NULL,
	[type] [int] NOT NULL,
	[dont_edit] [bit] NOT NULL,
	[dont_delete] [bit] NOT NULL,
	[dont_print] [bit] NOT NULL,
	[note] [text] NULL,
	[subject] [dbo].[typ_subject] NULL,
	[date_created] [datetime] NOT NULL,
	[created_by] [dbo].[typ_counselor] NOT NULL,
	[is_text] [bit] NOT NULL,
	[old_disbursement_note] [int] NULL,
	[ts] [timestamp] NOT NULL,
 CONSTRAINT [PK_disbursment_notes] PRIMARY KEY CLUSTERED 
(
	[disbursement_note] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 90) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'System assigned primary key to the table.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'disbursement_notes', @level2type=N'COLUMN',@level2name=N'disbursement_note'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'client number for the note' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'disbursement_notes', @level2type=N'COLUMN',@level2name=N'client'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'if the note was created by a deposit batch, this is the batch ID for the deposit. It is null for other creators' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'disbursement_notes', @level2type=N'COLUMN',@level2name=N'deposit_batch_id'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'When the note is accepted for a disbursement batch, this is the batch ID. Until it is accepted, it is displayed for all disbursement batches.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'disbursement_notes', @level2type=N'COLUMN',@level2name=N'disbursement_register'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'If the note was altered, and editing is not permitted, this is the pointer to the previous copy of this note' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'disbursement_notes', @level2type=N'COLUMN',@level2name=N'prior_note'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Type of the note. Note types are either "5" for a disbursement note or "3" for a system disbursement note.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'disbursement_notes', @level2type=N'COLUMN',@level2name=N'type'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Should others be allowed to edit the note?' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'disbursement_notes', @level2type=N'COLUMN',@level2name=N'dont_edit'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Should others be allowed to delete the note?' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'disbursement_notes', @level2type=N'COLUMN',@level2name=N'dont_delete'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Should others be allowed to print the note?' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'disbursement_notes', @level2type=N'COLUMN',@level2name=N'dont_print'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The note contents' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'disbursement_notes', @level2type=N'COLUMN',@level2name=N'note'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The subject for the note.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'disbursement_notes', @level2type=N'COLUMN',@level2name=N'subject'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Date/Time when the row was created.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'disbursement_notes', @level2type=N'COLUMN',@level2name=N'date_created'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Person who created the row.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'disbursement_notes', @level2type=N'COLUMN',@level2name=N'created_by'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Is the note text or RTF?' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'disbursement_notes', @level2type=N'COLUMN',@level2name=N'is_text'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'This holds the client disbursement notes.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'disbursement_notes'
GO
ALTER TABLE [dbo].[disbursement_notes] ADD  CONSTRAINT [DF_disbursment_notes_type]  DEFAULT ((5)) FOR [type]
GO
