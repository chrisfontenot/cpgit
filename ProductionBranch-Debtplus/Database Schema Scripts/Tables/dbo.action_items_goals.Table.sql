USE [DebtPlus]
GO
/****** Object:  Table [dbo].[action_items_goals]    Script Date: 09/15/2014 13:13:25 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[action_items_goals](
	[action_plan] [dbo].[typ_key] NOT NULL,
	[text] [text] NULL,
	[date_created] [dbo].[typ_date] NOT NULL,
	[created_by] [dbo].[typ_counselor] NOT NULL,
	[old_action_plan] [int] NULL,
 CONSTRAINT [PK_action_items_goals] PRIMARY KEY CLUSTERED 
(
	[action_plan] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 90) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Action plan ID' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'action_items_goals', @level2type=N'COLUMN',@level2name=N'action_plan'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Text associated with the action plan' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'action_items_goals', @level2type=N'COLUMN',@level2name=N'text'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Date/Time when the row was created.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'action_items_goals', @level2type=N'COLUMN',@level2name=N'date_created'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Person who created the row.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'action_items_goals', @level2type=N'COLUMN',@level2name=N'created_by'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'For each action plan, the goals and objectives information is recorded in this table.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'action_items_goals'
GO
