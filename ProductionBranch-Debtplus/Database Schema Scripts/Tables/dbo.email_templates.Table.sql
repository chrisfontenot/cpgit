-- Drop the relationships and ensure that any new columns are present
EXECUTE UPDATE_DROP_CONSTRAINTS 'email_templates'
EXECUTE UPDATE_ADD_COLUMN 'email_templates', 'attachments', 'xml'
GO

-- Correct the references so that the foreign key relationships will not fail
EXEC ('UPDATE email_templates SET language=dbo.default_language() FROM email_templates t LEFT OUTER JOIN attributetypes a on t.language = a.oID where a.grouping = ''LANGUAGE'' and a.oID IS NULL')
EXEC ('DELETE email_queue FROM email_queue q LEFT OUTER JOIN email_templates t ON q.template = t.oid WHERE t.oid IS NULL')

/* To prevent any potential data loss issues, you should review this script in detail before running it outside the context of the database designer.*/
BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION
GO
CREATE TABLE dbo.Tmp_email_templates
	(
	oID dbo.typ_key NOT NULL IDENTITY (1, 1),
	description dbo.typ_description NOT NULL,
	language dbo.typ_key NOT NULL,
	html_URI varchar(512) NULL,
	text_URI varchar(512) NULL,
	sender_email varchar(512) NULL,
	sender_name varchar(512) NULL,
	subject varchar(512) NULL,
	attachments xml NULL,
	date_created dbo.typ_date NOT NULL,
	created_by dbo.typ_counselor NOT NULL,
	ts timestamp NOT NULL
	)  ON [PRIMARY]
	 TEXTIMAGE_ON [PRIMARY]
GO
GRANT SELECT,INSERT,UPDATE,DELETE ON dbo.Tmp_email_templates TO public  AS dbo
GO
DECLARE @v sql_variant 
SET @v = N'List of attachments for this template'
EXECUTE sp_addextendedproperty N'MS_Description', @v, N'SCHEMA', N'dbo', N'TABLE', N'Tmp_email_templates', N'COLUMN', N'attachments'
GO
SET IDENTITY_INSERT dbo.Tmp_email_templates ON
GO
IF EXISTS(SELECT * FROM SYSOBJECTS WHERE TYPE='U' AND NAME=N'email_templates')
BEGIN
	EXEC('INSERT INTO dbo.Tmp_email_templates (oID, description, language, html_URI, text_URI, sender_email, sender_name, subject, attachments, date_created, created_by)
		SELECT oID, description, language, html_URI, text_URI, sender_email, sender_name, subject, attachments, date_created, created_by FROM dbo.email_templates WITH (HOLDLOCK TABLOCKX)')
	EXEC('DROP TABLE dbo.email_templates')
END
GO
SET IDENTITY_INSERT dbo.Tmp_email_templates OFF
GO
EXECUTE sp_rename N'dbo.Tmp_email_templates', N'email_templates', 'OBJECT' 
GO
ALTER TABLE dbo.email_templates ADD CONSTRAINT
	PK_email_templates PRIMARY KEY CLUSTERED 
	(
	oID
	) WITH( STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
ALTER TABLE dbo.email_templates ADD CONSTRAINT
	FK_email_templates_AttributeTypes FOREIGN KEY
	(
	language
	) REFERENCES dbo.AttributeTypes
	(
	oID
	) ON UPDATE  NO ACTION 
	 ON DELETE  NO ACTION 
GO
DENY INSERT,SELECT,UPDATE,DELETE ON dbo.email_templates TO www_role  AS dbo 
GO
COMMIT
BEGIN TRANSACTION
GO
ALTER TABLE dbo.email_queue ADD CONSTRAINT
	FK_email_queue_email_templates FOREIGN KEY
	(
	template
	) REFERENCES dbo.email_templates
	(
	oID
	) ON UPDATE  CASCADE 
	 ON DELETE  CASCADE 
GO
COMMIT
GO
