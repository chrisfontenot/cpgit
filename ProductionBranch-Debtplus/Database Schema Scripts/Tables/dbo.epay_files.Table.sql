USE [DebtPlus]
GO
/****** Object:  Table [dbo].[epay_files]    Script Date: 09/15/2014 13:13:26 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[epay_files](
	[epay_file] [dbo].[typ_key] IDENTITY(1,1) NOT NULL,
	[bank] [dbo].[typ_key] NOT NULL,
	[file_type] [varchar](4) NOT NULL,
	[filename] [varchar](50) NULL,
	[date_created] [dbo].[typ_date] NOT NULL,
	[created_by] [dbo].[typ_counselor] NOT NULL,
 CONSTRAINT [PK_epay_files] PRIMARY KEY CLUSTERED 
(
	[epay_file] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 90) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Output type of file' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'epay_files', @level2type=N'COLUMN',@level2name=N'file_type'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Filename, or last part of the filename' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'epay_files', @level2type=N'COLUMN',@level2name=N'filename'
GO
ALTER TABLE [dbo].[epay_files] ADD  CONSTRAINT [DF_epay_files_file_type]  DEFAULT ('EDI') FOR [file_type]
GO
