EXEC UPDATE_DROP_CONSTRAINTS 'OCS_Client'
EXEC UPDATE_ADD_COLUMN 'OCS_Client', 'closedate', 'datetime'
EXEC UPDATE_ADD_COLUMN 'OCS_Client', 'TrialModification-old', 'bit'
EXEC UPDATE_ADD_COLUMN 'OCS_Client', 'TrialModification', 'int'
GO
/* To prevent any potential data loss issues, you should review this script in detail before running it outside the context of the database designer.*/
BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION
GO
CREATE TABLE dbo.Tmp_OCS_Client
	(
	Id int NOT NULL IDENTITY (1, 1),
	UploadRecord int NULL,
	ClientId int NOT NULL,
	StatusCode int NOT NULL,
	ContactAttempts int NOT NULL,
	ActiveFlag bit NOT NULL,
	Archive bit NOT NULL,
	IsDuplicate bit NOT NULL,
	UploadAttempt uniqueidentifier NULL,
	InvestorNumber varchar(20) NULL,
	InvestorDueDate datetime NULL,
	InvestorLastChanceList datetime NULL,
	InvestorActualSaleDate datetime NULL,
	Program int NULL,
	ClaimedDate datetime NULL,
	ClaimedBy varchar(50) NULL,
	SearchTimezone varchar(50) NULL,
	SearchServicerId varchar(20) NULL,
	ArchiveAttempt int NULL,
	QueueCode varchar(10) NULL,
	PostModLastChanceFlag int NULL,
	PostModLastChanceDate datetime NULL,
	LoanNumberPlaceHolder varchar(20) NULL,
	TrialModification int NULL,
	closedate datetime NULL,
	ts timestamp NULL,
	[TrialModification-old] bit NULL
	)  ON [PRIMARY]
GO
GRANT DELETE ON dbo.Tmp_OCS_Client TO public  AS dbo
GO
GRANT INSERT ON dbo.Tmp_OCS_Client TO public  AS dbo
GO
GRANT SELECT ON dbo.Tmp_OCS_Client TO public  AS dbo
GO
GRANT UPDATE ON dbo.Tmp_OCS_Client TO public  AS dbo
GO
ALTER TABLE dbo.Tmp_OCS_Client ADD CONSTRAINT DF_OCS_Client_ContactAttempts DEFAULT ((0)) FOR ContactAttempts
GO
SET IDENTITY_INSERT dbo.Tmp_OCS_Client ON
GO
IF EXISTS(SELECT * FROM sysobjects WHERE name=N'OCS_Client' AND type='U')
BEGIN
	EXEC('INSERT INTO dbo.Tmp_OCS_Client (Id, UploadRecord, ClientId, StatusCode, ContactAttempts, ActiveFlag, Archive, IsDuplicate, UploadAttempt, InvestorNumber, InvestorDueDate, InvestorLastChanceList, InvestorActualSaleDate, Program, ClaimedDate, ClaimedBy, SearchTimezone, SearchServicerId, ArchiveAttempt, QueueCode, PostModLastChanceFlag, PostModLastChanceDate, LoanNumberPlaceHolder, [TrialModification-old], TrialModification, closedate)
		SELECT Id, UploadRecord, ClientId, StatusCode, ContactAttempts, ActiveFlag, Archive, IsDuplicate, UploadAttempt, InvestorNumber, InvestorDueDate, InvestorLastChanceList, InvestorActualSaleDate, Program, ClaimedDate, ClaimedBy, SearchTimezone, SearchServicerId, ArchiveAttempt, QueueCode, PostModLastChanceFlag, PostModLastChanceDate, LoanNumberPlaceHolder, [TrialModification-old], TrialModification, closedate FROM dbo.OCS_Client WITH (HOLDLOCK TABLOCKX)')
	EXEC('DROP TABLE dbo.OCS_Client')
END
GO
SET IDENTITY_INSERT dbo.Tmp_OCS_Client OFF
GO
EXECUTE sp_rename N'dbo.Tmp_OCS_Client', N'OCS_Client', 'OBJECT' 
GO
ALTER TABLE dbo.OCS_Client ADD CONSTRAINT
	PK_OCS_Client PRIMARY KEY CLUSTERED 
	(
	Id
	) WITH( STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX IX_OCSClient_ClientID ON dbo.OCS_Client
	(
	ClientId
	) WITH( STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX IX_OCSClient_ArchiveProg ON dbo.OCS_Client
	(
	Archive,
	Program
	) INCLUDE (Id, UploadRecord, ClientId, ActiveFlag, ClaimedDate, SearchTimezone, QueueCode) 
WITH( STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX ix_OCS_Client_UploadAttempt ON dbo.OCS_Client
	(
	UploadAttempt
	) WITH( PAD_INDEX = OFF, FILLFACTOR = 100, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
DENY DELETE ON dbo.OCS_Client TO www_role  AS dbo 
GO
DENY INSERT ON dbo.OCS_Client TO www_role  AS dbo 
GO
DENY SELECT ON dbo.OCS_Client TO www_role  AS dbo 
GO
DENY UPDATE ON dbo.OCS_Client TO www_role  AS dbo 
GO
COMMIT
GO
EXEC('DELETE dbo.OCS_Client FROM dbo.OCS_Client o LEFT OUTER JOIN clients c on o.ClientId = c.client WHERE c.client IS NULL')
EXEC('ALTER TABLE dbo.OCS_Client ADD CONSTRAINT FK_OCS_Client_clients FOREIGN KEY ( ClientId ) REFERENCES dbo.clients ( client ) ON UPDATE NO ACTION ON DELETE CASCADE')
EXEC('ALTER TABLE dbo.OCS_Client ADD CONSTRAINT FK_OCS_Client_OCS_StatusCode FOREIGN KEY ( StatusCode ) REFERENCES dbo.OCS_StatusCode ( Id ) ON UPDATE NO ACTION ON DELETE NO ACTION')
GO
