-- Remove stranded records
DELETE FROM sales_debts WHERE sales_file NOT IN (SELECT sales_file FROM sales_files)
GO
/* To prevent any potential data loss issues, you should review this script in detail before running it outside the context of the database designer.*/
BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
GO
EXECUTE UPDATE_DROP_CONSTRAINTS 'sales_debts'
GO
BEGIN TRANSACTION
GO
ALTER TABLE dbo.sales_debts DROP CONSTRAINT FK_sales_debts_sales_files
GO
COMMIT
GO
BEGIN TRANSACTION
GO
CREATE TABLE dbo.Tmp_sales_debts
	(
	sales_debt dbo.typ_key NOT NULL IDENTITY (1, 1),
	sales_file dbo.typ_key NOT NULL,
	creditor_type varchar(10) NOT NULL,
	creditor_name dbo.typ_description NOT NULL,
	account_number dbo.typ_client_account NULL,
	client_creditor dbo.typ_key NULL,
	balance money NOT NULL,
	interest_rate float(53) NOT NULL,
	minimum_payment money NOT NULL,
	late_fees money NOT NULL,
	overlimit_fees money NOT NULL,
	cosigner_fees money NOT NULL,
	finance_charge money NOT NULL,
	total_interest_fees money NOT NULL,
	principal money NOT NULL,
	plan_min_prorate float(53) NOT NULL,
	plan_min_payment money NOT NULL,
	plan_payment money NOT NULL,
	plan_interest_rate float(53) NOT NULL,
	plan_total_fees money NOT NULL,
	plan_finance_charge money NOT NULL,
	plan_total_interest_fees money NOT NULL,
	plan_principal money NOT NULL,
	payout_percent float(53) NOT NULL,
	payout_interest money NOT NULL,
	ts timestamp NOT NULL
	)  ON [PRIMARY]
GO
GRANT DELETE ON dbo.Tmp_sales_debts TO public  AS dbo
GO
GRANT INSERT ON dbo.Tmp_sales_debts TO public  AS dbo
GO
GRANT SELECT ON dbo.Tmp_sales_debts TO public  AS dbo
GO
GRANT UPDATE ON dbo.Tmp_sales_debts TO public  AS dbo
GO
EXECUTE sp_bindefault N'dbo.default_zero', N'dbo.Tmp_sales_debts.interest_rate'
GO
EXECUTE sp_bindefault N'dbo.default_money', N'dbo.Tmp_sales_debts.minimum_payment'
GO
EXECUTE sp_bindefault N'dbo.default_money', N'dbo.Tmp_sales_debts.late_fees'
GO
EXECUTE sp_bindefault N'dbo.default_money', N'dbo.Tmp_sales_debts.overlimit_fees'
GO
EXECUTE sp_bindefault N'dbo.default_money', N'dbo.Tmp_sales_debts.cosigner_fees'
GO
EXECUTE sp_bindefault N'dbo.default_money', N'dbo.Tmp_sales_debts.finance_charge'
GO
EXECUTE sp_bindefault N'dbo.default_money', N'dbo.Tmp_sales_debts.total_interest_fees'
GO
EXECUTE sp_bindefault N'dbo.default_money', N'dbo.Tmp_sales_debts.principal'
GO
EXECUTE sp_bindefault N'dbo.default_zero', N'dbo.Tmp_sales_debts.plan_min_prorate'
GO
EXECUTE sp_bindefault N'dbo.default_money', N'dbo.Tmp_sales_debts.plan_min_payment'
GO
EXECUTE sp_bindefault N'dbo.default_money', N'dbo.Tmp_sales_debts.plan_payment'
GO
EXECUTE sp_bindefault N'dbo.default_zero', N'dbo.Tmp_sales_debts.plan_interest_rate'
GO
EXECUTE sp_bindefault N'dbo.default_money', N'dbo.Tmp_sales_debts.plan_total_fees'
GO
EXECUTE sp_bindefault N'dbo.default_money', N'dbo.Tmp_sales_debts.plan_finance_charge'
GO
EXECUTE sp_bindefault N'dbo.default_money', N'dbo.Tmp_sales_debts.plan_total_interest_fees'
GO
EXECUTE sp_bindefault N'dbo.default_money', N'dbo.Tmp_sales_debts.plan_principal'
GO
ALTER TABLE dbo.Tmp_sales_debts ADD CONSTRAINT DF_sales_debts_payout_percent DEFAULT (0.02) FOR payout_percent
GO
EXECUTE sp_bindefault N'dbo.default_money', N'dbo.Tmp_sales_debts.payout_interest'
GO
SET IDENTITY_INSERT dbo.Tmp_sales_debts ON
GO
IF EXISTS(SELECT * FROM dbo.sales_debts)
	 EXEC('INSERT INTO dbo.Tmp_sales_debts (sales_debt, sales_file, creditor_type, creditor_name, account_number, client_creditor, balance, interest_rate, minimum_payment, late_fees, overlimit_fees, cosigner_fees, finance_charge, total_interest_fees, principal, plan_min_prorate, plan_min_payment, plan_payment, plan_interest_rate, plan_total_fees, plan_finance_charge, plan_total_interest_fees, plan_principal, payout_percent, payout_interest)
		SELECT sales_debt, sales_file, creditor_type, creditor_name, account_number, client_creditor, balance, interest_rate, minimum_payment, late_fees, overlimit_fees, cosigner_fees, finance_charge, total_interest_fees, principal, plan_min_prorate, plan_min_payment, plan_payment, plan_interest_rate, plan_total_fees, plan_finance_charge, plan_total_interest_fees, plan_principal, payout_percent, payout_interest FROM dbo.sales_debts WITH (HOLDLOCK TABLOCKX)')
GO
SET IDENTITY_INSERT dbo.Tmp_sales_debts OFF
GO
DROP TABLE dbo.sales_debts
GO
EXECUTE sp_rename N'dbo.Tmp_sales_debts', N'sales_debts', 'OBJECT' 
GO
ALTER TABLE dbo.sales_debts ADD CONSTRAINT
	PK_sales_debts PRIMARY KEY CLUSTERED 
	(
	sales_debt
	) WITH( PAD_INDEX = OFF, FILLFACTOR = 90, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX IX_sales_debts_1 ON dbo.sales_debts
	(
	sales_file
	) WITH( PAD_INDEX = OFF, FILLFACTOR = 90, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX IX_sales_debts_2 ON dbo.sales_debts
	(
	client_creditor
	) WITH( PAD_INDEX = OFF, FILLFACTOR = 90, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
ALTER TABLE dbo.sales_debts ADD CONSTRAINT FK_sales_debts_sales_files FOREIGN KEY
	(
	sales_file
	) REFERENCES dbo.sales_files
	(
	sales_file
	) ON UPDATE CASCADE 
	 ON DELETE CASCADE 
GO
DECLARE @v sql_variant 
SET @v = N'Link the sales_debts table to the sales_file table for delete operations on the sales_file'
EXECUTE sp_addextendedproperty N'MS_Description', @v, N'SCHEMA', N'dbo', N'TABLE', N'sales_debts', N'CONSTRAINT', N'FK_sales_debts_sales_files'
GO
create trigger TRIG_sales_debts_D on dbo.sales_debts after delete as
begin
-- =========================================================================
-- ==           Prevent the deleting of debts once the items are exported ==
-- =========================================================================
	if exists (	select * from deleted d inner join sales_files f on d.sales_file = f.sales_file where f.date_exported is not null )
	begin
		RaisError ('You can not delete debts once they have been exported', 16, 1)
		Rollback Transaction
		return
	end
end
GO
DENY DELETE ON dbo.sales_debts TO www_role  AS dbo 
GO
DENY INSERT ON dbo.sales_debts TO www_role  AS dbo 
GO
DENY SELECT ON dbo.sales_debts TO www_role  AS dbo 
GO
DENY UPDATE ON dbo.sales_debts TO www_role  AS dbo 
GO
COMMIT
