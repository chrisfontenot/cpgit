EXEC UPDATE_DROP_CONSTRAINTS 'vendor_addresses'
GO
/* To prevent any potential data loss issues, you should review this script in detail before running it outside the context of the database designer.*/
BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION
GO
CREATE TABLE dbo.Tmp_vendor_addresses
	(
	oID dbo.typ_key NOT NULL IDENTITY (1, 1),
	vendor dbo.typ_key NOT NULL,
	address_type char(1) NOT NULL,
	attn varchar(50) NULL,
	line_1 varchar(50) NULL,
	line_2 varchar(50) NULL,
	addressID dbo.typ_key NULL,
	date_created dbo.typ_date NOT NULL,
	created_by dbo.typ_counselor NOT NULL,
	ts timestamp NOT NULL
	)  ON [PRIMARY]
GO
ALTER TABLE dbo.Tmp_vendor_addresses SET (LOCK_ESCALATION = TABLE)
GO
ALTER TABLE dbo.Tmp_vendor_addresses ADD CONSTRAINT
	DF_vendor_addresses_address_type DEFAULT ('G') FOR address_type
GO
IF EXISTS(SELECT * FROM SYSOBJECTS WHERE TYPE='U' AND NAME=N'vendor_addresses')
BEGIN
	EXEC('SET IDENTITY_INSERT dbo.Tmp_vendor_addresses ON;
		  INSERT INTO dbo.Tmp_vendor_addresses (oID, vendor, address_type, attn, line_1, line_2, addressID, date_created, created_by)
		  SELECT oID, vendor, address_type, attn, line_1, line_2, addressID, date_created, created_by FROM dbo.vendor_addresses WITH (HOLDLOCK TABLOCKX);
		  SET IDENTITY_INSERT dbo.Tmp_vendor_addresses OFF;')
	EXEC('DROP TABLE dbo.vendor_addresses')
END
GO
EXECUTE sp_rename N'dbo.Tmp_vendor_addresses', N'vendor_addresses', 'OBJECT' 
GO
ALTER TABLE dbo.vendor_addresses ADD CONSTRAINT
	PK_vendor_addresses PRIMARY KEY CLUSTERED 
	(
	oID
	) WITH( STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
CREATE UNIQUE NONCLUSTERED INDEX IX_vendor_addresses_1 ON dbo.vendor_addresses
	(
	vendor,
	address_type
	) WITH( STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
ALTER TABLE dbo.vendor_addresses ADD CONSTRAINT
	FK_vendor_addresses_vendors FOREIGN KEY
	(
	vendor
	) REFERENCES dbo.vendors
	(
	oID
	) ON UPDATE  CASCADE 
	 ON DELETE  CASCADE 
GO
EXECUTE sp_bindrule N'dbo.valid_vendor_address_type', N'dbo.vendor_addresses.address_type'
GO
-- =============================================
-- Author:		Al Longyear
-- Description:	Drop reference fields on delete
-- =============================================
CREATE TRIGGER dbo.TRIG_vendor_addresses_D ON dbo.vendor_addresses AFTER DELETE AS 
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	SET NOCOUNT ON;

	-- Discard the address blocks since they are not re-used for anything
	-- but the one reference here.
	DELETE		addresses
	FROM		addresses a
	INNER JOIN	deleted d on a.Address = d.AddressID
END
GO
COMMIT
