USE [DebtPlus]
GO

update Housing_StatusTypes
   set aarp = 'Homeless Shelter'
 where description = 'Homeless'

update Housing_StatusTypes
   set aarp = 'Living w/ Others'
 where description = 'Lives with friend/relative'

update Housing_StatusTypes
   set aarp = 'Owning w/o Mortgage'
 where description = 'Own Free & Clear'

update Housing_StatusTypes
   set aarp = 'Owning w/ Mortgage'
 where description = 'Own w/Mortgage'

update Housing_StatusTypes
   set aarp = 'Renting'
 where description = 'Rent'