USE [DEBTPLUS]
GO

/****** Object:  Table [dbo].[Housing_Loan_DSADetails]    Script Date: 11/21/2014 11:51:06 AM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[Housing_Loan_DSADetails](
	[oID] [int] IDENTITY(1,1) NOT NULL,
	[ProgramBenefit] [int] NULL,
	[Program] [int] NULL,
	[NoDsaReason] [int] NULL,
	[CaseID] [int] NULL,
	[Specialist] [int] NULL,
	[ClientSituation] [int] NULL,
	[ts] [timestamp] NOT NULL,
 CONSTRAINT [PK_Housing_Loan_DSADetails] PRIMARY KEY CLUSTERED 
(
	[oID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

