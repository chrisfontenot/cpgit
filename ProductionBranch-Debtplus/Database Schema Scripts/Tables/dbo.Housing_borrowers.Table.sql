EXEC('ALTER TABLE dbo.Housing_borrowers DROP CONSTRAINT DF_Housing_borrowers_Gender')
EXEC('ALTER TABLE dbo.Housing_borrowers DROP CONSTRAINT DF_housing_borrowers_Race')
EXEC('ALTER TABLE dbo.Housing_borrowers DROP CONSTRAINT DF_Housing_borrowers_ethnicity')
EXEC('ALTER TABLE dbo.Housing_borrowers DROP CONSTRAINT DF_Housing_borrowers_Language')
EXEC('ALTER TABLE dbo.Housing_borrowers DROP CONSTRAINT DF_Housing_borrowers_marital_status')
EXEC('ALTER TABLE dbo.Housing_borrowers DROP CONSTRAINT DF_Housing_borrowers_Education')
EXEC('ALTER TABLE dbo.Housing_borrowers DROP CONSTRAINT DF_Housing_borrowers_CreditAgency')
GO
EXECUTE UPDATE_ADD_COLUMN 'Housing_borrowers', 'bkfileddate'
EXECUTE UPDATE_ADD_COLUMN 'Housing_borrowers', 'bkdischarge'
EXECUTE UPDATE_ADD_COLUMN 'Housing_borrowers', 'bkchapter'
EXECUTE UPDATE_ADD_COLUMN 'Housing_borrowers', 'bkPaymentCurrent'
EXECUTE UPDATE_ADD_COLUMN 'Housing_borrowers', 'bkAuthLetterDate'
GO
/* To prevent any potential data loss issues, you should review this script in detail before running it outside the context of the database designer.*/
BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
GO
BEGIN TRANSACTION
GO
CREATE TABLE dbo.Tmp_Housing_borrowers
	(
	oID dbo.typ_key NOT NULL IDENTITY (1, 1),
	PersonID int NOT NULL,
	NameID dbo.typ_key NULL,
	EmailID dbo.typ_key NULL,
	Former dbo.typ_lname NULL,
	SSN dbo.typ_ssn NULL,
	Gender dbo.typ_key NOT NULL,
	Race dbo.typ_key NOT NULL,
	ethnicity dbo.typ_key NOT NULL,
	Language dbo.typ_key NULL,
	Disabled bit NOT NULL,
	marital_status dbo.typ_key NULL,
	Education dbo.typ_key NOT NULL,
	Birthdate datetime NULL,
	FICO_Score int NULL,
	CreditAgency varchar(10) NULL,
	emp_start_date datetime NULL,
	emp_end_date datetime NULL,
	other_job dbo.typ_description NULL,
	MilitaryServiceID dbo.typ_key NOT NULL,
	MilitaryDependentID dbo.typ_key NOT NULL,
	MilitaryGradeID dbo.typ_key NOT NULL,
	MilitaryStatusID dbo.typ_key NOT NULL,
	bkfileddate datetime NULL,
	bkdischarge datetime NULL,
	bkchapter dbo.typ_key NULL,
	bkPaymentCurrent dbo.typ_key NULL,
	bkAuthLetterDate datetime NULL,
	ts timestamp NOT NULL
	)  ON [PRIMARY]
GO
GRANT DELETE ON dbo.Tmp_Housing_borrowers TO public  AS dbo
GO
GRANT INSERT ON dbo.Tmp_Housing_borrowers TO public  AS dbo
GO
GRANT SELECT ON dbo.Tmp_Housing_borrowers TO public  AS dbo
GO
GRANT UPDATE ON dbo.Tmp_Housing_borrowers TO public  AS dbo
GO
EXECUTE sp_bindefault N'dbo.default_zero', N'dbo.Tmp_Housing_borrowers.PersonID'
GO
ALTER TABLE dbo.Tmp_Housing_borrowers ADD CONSTRAINT DF_Housing_borrowers_Gender DEFAULT ([dbo].[default_gender]()) FOR Gender
GO
ALTER TABLE dbo.Tmp_Housing_borrowers ADD CONSTRAINT DF_housing_borrowers_Race DEFAULT ([dbo].[default_race]()) FOR Race
GO
ALTER TABLE dbo.Tmp_Housing_borrowers ADD CONSTRAINT DF_Housing_borrowers_ethnicity DEFAULT ([dbo].[default_Ethnicity]()) FOR ethnicity
GO
ALTER TABLE dbo.Tmp_Housing_borrowers ADD CONSTRAINT DF_Housing_borrowers_Language DEFAULT ([dbo].[Default_Language]()) FOR Language
GO
ALTER TABLE dbo.Tmp_Housing_borrowers ADD CONSTRAINT DF_Housing_borrowers_marital_status DEFAULT ([dbo].[Default_marital_status]()) FOR marital_status
GO
ALTER TABLE dbo.Tmp_Housing_borrowers ADD CONSTRAINT DF_Housing_borrowers_Education DEFAULT ([dbo].[Default_Education]()) FOR Education
GO
ALTER TABLE dbo.Tmp_Housing_borrowers ADD CONSTRAINT DF_Housing_borrowers_CreditAgency DEFAULT ([dbo].[default_CreditAgency]()) FOR CreditAgency
GO
EXECUTE sp_bindefault N'dbo.default_false', N'dbo.Tmp_Housing_borrowers.Disabled'
GO
EXECUTE sp_bindefault N'dbo.default_zero', N'dbo.Tmp_Housing_borrowers.MilitaryServiceID'
GO
EXECUTE sp_bindefault N'dbo.default_zero', N'dbo.Tmp_Housing_borrowers.MilitaryDependentID'
GO
EXECUTE sp_bindefault N'dbo.default_zero', N'dbo.Tmp_Housing_borrowers.MilitaryGradeID'
GO
EXECUTE sp_bindefault N'dbo.default_zero', N'dbo.Tmp_Housing_borrowers.MilitaryStatusID'
GO
SET IDENTITY_INSERT dbo.Tmp_Housing_borrowers ON
GO
EXECUTE sp_unbindrule N'dbo.Tmp_Housing_borrowers.SSN'
GO
IF EXISTS(SELECT * FROM dbo.Housing_borrowers)
	 EXEC('INSERT INTO dbo.Tmp_Housing_borrowers (oID, PersonID, NameID, EmailID, Former, SSN, Gender, Race, ethnicity, Language, Disabled, marital_status, Education, Birthdate, FICO_Score, CreditAgency, emp_start_date, emp_end_date, other_job, MilitaryServiceID, MilitaryDependentID, MilitaryGradeID, MilitaryStatusID, bkfileddate, bkdischarge, bkchapter, bkPaymentCurrent, bkAuthLetterDate)
		SELECT oID, PersonID, NameID, EmailID, Former, SSN, isnull(Gender,1), isnull(Race,10), isnull(ethnicity,0), isnull(Language,1), isnull(Disabled,0), isnull(marital_status,1), isnull(Education,1), Birthdate, FICO_Score, isnull(CreditAgency,''EXPERIAN''), emp_start_date, emp_end_date, other_job, isnull(MilitaryServiceID,0), isnull(MilitaryDependentID,0), isnull(MilitaryGradeID,0), isnull(MilitaryStatusID,0), bkfileddate, bkdischarge, bkchapter, bkPaymentCurrent, bkAuthLetterDate FROM dbo.Housing_borrowers WITH (HOLDLOCK TABLOCKX)')
GO
SET IDENTITY_INSERT dbo.Tmp_Housing_borrowers OFF
GO
DROP TABLE dbo.Housing_borrowers
GO
EXECUTE sp_rename N'dbo.Tmp_Housing_borrowers', N'Housing_borrowers', 'OBJECT' 
GO
ALTER TABLE dbo.Housing_borrowers ADD CONSTRAINT
	PK_housing_borrowers_1 PRIMARY KEY CLUSTERED 
	(
	oID
	) WITH( STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
EXECUTE sp_bindrule N'dbo.valid_ssn', N'dbo.Housing_borrowers.SSN'
GO
CREATE TRIGGER  [trig_housing_borrowers_D] on dbo.Housing_borrowers AFTER DELETE as
-- =======================================================================================
-- ==        Remove referenced items when the borrower is removed                       ==
-- =======================================================================================

-- ChangeLog
--   1/1/2011
--      Initial version

set nocount on

-- Name
delete	Names
from	Names n
inner join deleted d on n.Name = d.NameID
where d.NameID is not null

-- Email address
delete	EmailAddresses
from	EmailAddresses e
inner join deleted d on e.Email = d.EmailID
where	d.EmailID is not null
GO
COMMIT
GO
DENY DELETE ON dbo.Housing_borrowers TO www_role  AS dbo 
GO
DENY INSERT ON dbo.Housing_borrowers TO www_role  AS dbo 
GO
DENY SELECT ON dbo.Housing_borrowers TO www_role  AS dbo 
GO
DENY UPDATE ON dbo.Housing_borrowers TO www_role  AS dbo 
GO
