USE [DEBTPLUS]
GO
/* To prevent any potential data loss issues, you should review this script in detail before running it outside the context of the database designer.*/
BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
GO
EXECUTE UPDATE_add_column 'Housing_RetentionTypes', 'PlanToKeepHomeIND', 'bit'
GO
BEGIN TRANSACTION
GO
CREATE TABLE dbo.Tmp_Housing_RetentionTypes
	(
	[oID] dbo.typ_key NOT NULL IDENTITY (1, 1),
	[Description] dbo.typ_description NOT NULL,
	[Default] bit NOT NULL,
	[ActiveFlag] bit NOT NULL,
	[PlanToKeepHomeIND] bit NOT NULL,
	[date_created] dbo.typ_date NOT NULL,
	[created_by] dbo.typ_counselor NOT NULL,
	[ts] timestamp NOT NULL
	) ON [PRIMARY]
GO
GRANT SELECT,INSERT,UPDATE,DELETE to public as dbo
GO
EXECUTE sp_bindefault N'dbo.default_false', N'dbo.Tmp_Housing_RetentionTypes.[Default]'
GO
EXECUTE sp_bindefault N'dbo.default_true', N'dbo.Tmp_Housing_RetentionTypes.ActiveFlag'
GO
EXECUTE sp_bindefault N'dbo.default_false', N'dbo.Tmp_Housing_RetentionTypes.PlanToKeepHomeIND'
GO
SET IDENTITY_INSERT dbo.Tmp_Housing_RetentionTypes ON
GO
IF EXISTS(SELECT * FROM [sysobjects] WHERE name = N'Housing_RetentionTypes' AND type = 'U')
	EXEC('INSERT INTO dbo.Tmp_Housing_RetentionTypes (oID, Description, [Default], ActiveFlag, PlanToKeepHomeIND, date_created, created_by)
		SELECT oID, Description, isnull([Default],0), isnull(ActiveFlag,1), isnull(PlanToKeepHomeIND,0), date_created, created_by FROM dbo.Housing_RetentionTypes WITH (HOLDLOCK TABLOCKX)')

	declare @actualrowcount int 
	select @actualrowcount = count(*) from Housing_RetentionTypes
	print 'Housing_RetentionTypes row count : ' + convert(varchar, @actualrowcount)

	declare @tmprowcount int 
	select @tmprowcount = count(*) from Tmp_Housing_RetentionTypes
	print 'Tmp_Housing_RetentionTypes row count : ' + convert(varchar, @tmprowcount)

	if @actualrowcount = @tmprowcount 
	begin
		print 'actual row count and temp table row count match, dropping table...'
		drop table dbo.Housing_RetentionTypes
	end

SET IDENTITY_INSERT dbo.Tmp_Housing_RetentionTypes OFF
GO
EXECUTE sp_rename N'dbo.Tmp_Housing_RetentionTypes', N'Housing_RetentionTypes', 'OBJECT' 
GO
ALTER TABLE dbo.Housing_RetentionTypes ADD CONSTRAINT PK_Housing_RetentionTypes PRIMARY KEY CLUSTERED 
	(
	oID
	) ON [PRIMARY]
GO
COMMIT
GO
IF NOT EXISTS(SELECT * FROM [dbo].[Housing_RetentionTypes])
	EXEC ('SET IDENTITY_INSERT [dbo].[Housing_RetentionTypes] ON
		   INSERT [dbo].[Housing_RetentionTypes] ([oID], [Description], [Default], [ActiveFlag], [PlanToKeepHomeIND], [date_created], [created_by]) VALUES (1, N''Sell Home'', 0, 1, 0, CAST(N''2014-09-03 18:46:02.773'' AS DateTime), N''sa'');
		   INSERT [dbo].[Housing_RetentionTypes] ([oID], [Description], [Default], [ActiveFlag], [PlanToKeepHomeIND], [date_created], [created_by]) VALUES (2, N''Keep Home'', 0, 1, 1, CAST(N''2014-09-03 18:46:07.363'' AS DateTime), N''sa'');
		   INSERT [dbo].[Housing_RetentionTypes] ([oID], [Description], [Default], [ActiveFlag], [PlanToKeepHomeIND], [date_created], [created_by]) VALUES (3, N''Vacate'', 0, 1, 0, CAST(N''2014-09-03 18:46:14.970'' AS DateTime), N''sa'');
		   SET IDENTITY_INSERT [dbo].[Housing_RetentionTypes] OFF')
GO
IF NOT EXISTS (SELECT * FROM sysobjects WHERE xtype = 'TR' and name = N'TRIG_Housing_RetentionTypes_D')
EXEC ('CREATE TRIGGER TRIG_Housing_RetentionTypes_D ON Housing_RetentionTypes AFTER DELETE AS
BEGIN
	-- Suppress intermediate results
	SET NOCOUNT ON

	-- Cascade the delete to the housing_properties table
	UPDATE	[housing_properties]
	SET		[PlanToKeepHomeCD] = null
	FROM	[Housing_properties] p
	INNER JOIN deleted d ON p.[PlanToKeepHomeCD] = d.[oID]
END
')
GO
DENY SELECT,INSERT,UPDATE,DELETE to www_role as dbo
GO
