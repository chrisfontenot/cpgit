EXECUTE UPDATE_DROP_CONSTRAINTS 'InBoundTelephoneNumberGroups'
GO
/* To prevent any potential data loss issues, you should review this script in detail before running it outside the context of the database designer.*/
BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
GO
COMMIT
BEGIN TRANSACTION
GO
CREATE TABLE dbo.Tmp_InBoundTelephoneNumberGroups
	(
	oID dbo.typ_key NOT NULL IDENTITY (1, 1),
	Description dbo.typ_description NOT NULL,
	referred_by dbo.typ_key NULL,
	[Default] bit NOT NULL,
	ActiveFlag bit NOT NULL,
	date_created dbo.typ_date NOT NULL,
	created_by dbo.typ_counselor NOT NULL,
	ts timestamp NOT NULL
	)  ON [PRIMARY]
GO
GRANT SELECT,INSERT,UPDATE,DELETE Tmp_InBoundTelephoneNumberGroups TO public AS dbo;
GO
EXECUTE sp_bindefault N'dbo.default_string', N'dbo.Tmp_InBoundTelephoneNumberGroups.Description'
GO
EXECUTE sp_bindefault N'dbo.default_false', N'dbo.Tmp_InBoundTelephoneNumberGroups.[Default]'
GO
EXECUTE sp_bindefault N'dbo.default_true', N'dbo.Tmp_InBoundTelephoneNumberGroups.ActiveFlag'
GO
SET IDENTITY_INSERT dbo.Tmp_InBoundTelephoneNumberGroups ON
GO
IF EXISTS(SELECT * FROM SYSOBJECTS WHERE NAME=N'InBoundTelephoneNumberGroups' AND TYPE='U')
BEGIN
	EXEC('INSERT INTO dbo.Tmp_InBoundTelephoneNumberGroups (oID, Description, referred_by, [Default], ActiveFlag, date_created, created_by)
		SELECT oID, Description, referred_by, [Default], ActiveFlag, date_created, created_by FROM dbo.InBoundTelephoneNumberGroups WITH (HOLDLOCK TABLOCKX)')
	EXEC('DROP TABLE dbo.InBoundTelephoneNumberGroups')
END
GO
SET IDENTITY_INSERT dbo.Tmp_InBoundTelephoneNumberGroups OFF
GO
EXECUTE sp_rename N'dbo.Tmp_InBoundTelephoneNumberGroups', N'InBoundTelephoneNumberGroups', 'OBJECT' 
GO
ALTER TABLE dbo.InBoundTelephoneNumberGroups ADD CONSTRAINT
	PK_InBoundTelephoneNumberGroups PRIMARY KEY CLUSTERED 
	(
	oID
	) WITH( STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
ALTER TABLE dbo.InBoundTelephoneNumberGroups ADD CONSTRAINT
	FK_InBoundTelephoneNumberGroups_referred_by FOREIGN KEY
	(
	referred_by
	) REFERENCES dbo.referred_by
	(
	referred_by
	) ON UPDATE  CASCADE 
	 ON DELETE  SET NULL 
GO
COMMIT
BEGIN TRANSACTION
GO
ALTER TABLE dbo.InboundTelephoneNumbers ADD CONSTRAINT
	FK_InboundTelephoneNumbers_InBoundTelephoneNumberGroups FOREIGN KEY
	(
	InboundTelephoneNumberGroup
	) REFERENCES dbo.InBoundTelephoneNumberGroups
	(
	oID
	) ON UPDATE  CASCADE 
	 ON DELETE  CASCADE 
GO
COMMIT
BEGIN TRANSACTION
GO
ALTER TABLE dbo.clients ADD CONSTRAINT
	FK_clients_InBoundTelephoneNumberGroups FOREIGN KEY
	(
	InboundTelephoneGroupID
	) REFERENCES dbo.InBoundTelephoneNumberGroups
	(
	oID
	) ON UPDATE  CASCADE 
	 ON DELETE  SET NULL 
GO
COMMIT
