USE [DebtPlus]
GO
/****** Object:  Table [dbo].[Housing_counselor_training_courses]    Script Date: 09/15/2014 13:13:26 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Housing_counselor_training_courses](
	[oID] [dbo].[typ_key] IDENTITY(1,1) NOT NULL,
	[Counselor] [dbo].[typ_key] NOT NULL,
	[TrainingCourse] [dbo].[typ_key] NOT NULL,
	[TrainingDate] [datetime] NULL,
	[date_created] [dbo].[typ_date] NOT NULL,
	[created_by] [dbo].[typ_counselor] NOT NULL,
	[old_oid] [int] NULL,
 CONSTRAINT [PK_housing_counselor_training_courses] PRIMARY KEY CLUSTERED 
(
	[oID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
