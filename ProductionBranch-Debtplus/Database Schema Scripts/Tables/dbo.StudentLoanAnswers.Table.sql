USE [DebtPlus]
GO
/****** Object:  Table [dbo].[StudentLoanAnswers]    Script Date: 09/15/2014 13:13:26 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[StudentLoanAnswers](
	[oID] [dbo].[typ_key] IDENTITY(1,1) NOT NULL,
	[StudentLoan] [dbo].[typ_key] NOT NULL,
	[QuestionID] [varchar](50) NOT NULL,
	[AnswerID] [varchar](50) NOT NULL,
 CONSTRAINT [PK_StudentLoanQuestions] PRIMARY KEY CLUSTERED 
(
	[oID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
