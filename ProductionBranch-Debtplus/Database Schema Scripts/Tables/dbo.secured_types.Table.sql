EXEC UPDATE_drop_constraints 'secured_types'
GO
/* To prevent any potential data loss issues, you should review this script in detail before running it outside the context of the database designer.*/
BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION
GO
CREATE TABLE dbo.Tmp_secured_types
	(
	secured_type dbo.typ_key NOT NULL IDENTITY (1, 1),
	secured_type_group dbo.typ_key NOT NULL,
	description dbo.typ_description NOT NULL,
	spanish_description nvarchar(50) NOT NULL,
	hpf varchar(50) NULL,
	created_by dbo.typ_counselor NOT NULL,
	date_created dbo.typ_date NOT NULL,
	ts timestamp NOT NULL,
	grouping dbo.typ_description NOT NULL,
	auto_home_other varchar(1) NOT NULL
	)  ON [PRIMARY]
GO
GRANT DELETE ON dbo.Tmp_secured_types TO public  AS dbo
GO
GRANT INSERT ON dbo.Tmp_secured_types TO public  AS dbo
GO
GRANT SELECT ON dbo.Tmp_secured_types TO public  AS dbo
GO
GRANT UPDATE ON dbo.Tmp_secured_types TO public  AS dbo
GO
EXECUTE sp_bindefault N'dbo.default_string', N'dbo.Tmp_secured_types.spanish_description'
GO
SET IDENTITY_INSERT dbo.Tmp_secured_types ON
GO
IF EXISTS(SELECT * FROM dbo.secured_types)
	EXEC('INSERT INTO dbo.Tmp_secured_types (secured_type, secured_type_group, description, spanish_description, hpf, created_by, date_created, grouping, auto_home_other)
		SELECT secured_type, secured_type_group, description, spanish_description, hpf, created_by, date_created, grouping, auto_home_other FROM dbo.secured_types WITH (HOLDLOCK TABLOCKX)')
GO
SET IDENTITY_INSERT dbo.Tmp_secured_types OFF
GO
DROP TABLE dbo.secured_types
GO
EXECUTE sp_rename N'dbo.Tmp_secured_types', N'secured_types', 'OBJECT' 
GO
ALTER TABLE dbo.secured_types ADD CONSTRAINT
	PK_secured_types PRIMARY KEY CLUSTERED 
	(
	secured_type
	) WITH( PAD_INDEX = OFF, FILLFACTOR = 90, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
DENY DELETE ON dbo.secured_types TO www_role  AS dbo 
GO
DENY INSERT ON dbo.secured_types TO www_role  AS dbo 
GO
DENY SELECT ON dbo.secured_types TO www_role  AS dbo 
GO
DENY UPDATE ON dbo.secured_types TO www_role  AS dbo 
GO
COMMIT
BEGIN TRANSACTION
EXEC('DELETE FROM secured_properties WHERE secured_type NOT IN (SELECT secured_type FROM secured_types)')
GO
EXEC('ALTER TABLE dbo.secured_properties ADD CONSTRAINT FK_secured_properties_secured_types FOREIGN KEY ( secured_type ) REFERENCES dbo.secured_types ( secured_type ) ON UPDATE NO ACTION ON DELETE CASCADE')
GO
EXEC('DELETE FROM secured_types WHERE secured_type_group NOT IN (SELECT oID FROM secured_type_groups)')
GO
EXEC('ALTER TABLE dbo.secured_types ADD CONSTRAINT FK_secured_types_secured_type_groups FOREIGN KEY ( secured_type_group ) REFERENCES dbo.secured_type_groups ( oID ) ON UPDATE NO ACTION ON DELETE CASCADE')
GO
COMMIT
GO

