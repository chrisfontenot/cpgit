USE [DebtPlus]
GO
/****** Object:  Table [dbo].[client_notes]    Script Date: 09/15/2014 13:13:26 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[client_notes](
	[client_note] [dbo].[typ_key] IDENTITY(1,1) NOT NULL,
	[client] [dbo].[typ_client] NOT NULL,
	[client_creditor] [dbo].[typ_key] NULL,
	[prior_note] [dbo].[typ_key] NULL,
	[type] [int] NOT NULL,
	[expires] [datetime] NULL,
	[dont_edit] [bit] NOT NULL,
	[dont_delete] [bit] NOT NULL,
	[dont_print] [bit] NOT NULL,
	[subject] [dbo].[typ_subject] NULL,
	[note] [text] NULL,
	[date_created] [datetime] NOT NULL,
	[created_by] [dbo].[typ_counselor] NOT NULL,
	[is_text] [bit] NULL,
	[old_client_note] [int] NULL,
	[ts] [timestamp] NOT NULL,
 CONSTRAINT [PK_client_notes] PRIMARY KEY CLUSTERED 
(
	[client_note] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Primary key. Sequential number for the note.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'client_notes', @level2type=N'COLUMN',@level2name=N'client_note'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Client for whom the message applies' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'client_notes', @level2type=N'COLUMN',@level2name=N'client'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'If this is a client/creditor note then this is the pointer to the client/creditor record. NULL = client note.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'client_notes', @level2type=N'COLUMN',@level2name=N'client_creditor'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'If this is an edited note, this is the pointer to the previous note.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'client_notes', @level2type=N'COLUMN',@level2name=N'prior_note'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Type of the note.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'client_notes', @level2type=N'COLUMN',@level2name=N'type'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Date the note expires.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'client_notes', @level2type=N'COLUMN',@level2name=N'expires'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'If set, do not allow others to change the note.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'client_notes', @level2type=N'COLUMN',@level2name=N'dont_edit'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'If set, do not allow others to delete this note.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'client_notes', @level2type=N'COLUMN',@level2name=N'dont_delete'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'If set, do not allow others to print the note.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'client_notes', @level2type=N'COLUMN',@level2name=N'dont_print'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Subject for the note.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'client_notes', @level2type=N'COLUMN',@level2name=N'subject'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Text for the note.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'client_notes', @level2type=N'COLUMN',@level2name=N'note'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Date/Time when the row was created.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'client_notes', @level2type=N'COLUMN',@level2name=N'date_created'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Person who created the row.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'client_notes', @level2type=N'COLUMN',@level2name=N'created_by'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'If set, the note is not RTF. It is simple ASCII text.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'client_notes', @level2type=N'COLUMN',@level2name=N'is_text'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'This table holds all of the client and client_creditor notes.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'client_notes'
GO
