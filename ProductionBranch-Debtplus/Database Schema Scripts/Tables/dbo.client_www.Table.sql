DELETE FROM client_www_note WHERE PKID not in (SELECT PKID from client_www)
GO
EXECUTE UPDATE_DROP_CONSTRAINTS 'client_www'
GO
EXECUTE UPDATE_ADD_COLUMN 'client_www', 'DatabaseKeyID', 'int'
GO
/* To prevent any potential data loss issues, you should review this script in detail before running it outside the context of the database designer.*/
BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION
GO
CREATE TABLE dbo.Tmp_client_www
	(
	PKID uniqueidentifier NOT NULL,
	ApplicationName varchar(50) NOT NULL,
	UserName varchar(50) NOT NULL,
	Password varchar(50) NULL,
	Email dbo.typ_email NULL,
	PasswordQuestion varchar(50) NULL,
	PasswordAnswer varchar(80) NULL,
	Comment varchar(255) NULL,
	IsLockedOut bit NOT NULL,
	IsApproved bit NOT NULL,
	LastPasswordChangeDate datetime NOT NULL,
	LastLoginDate datetime NOT NULL,
	LastActivityDate datetime NOT NULL,
	LastLockoutDate datetime NULL,
	FailedPasswordAnswerAttemptCount int NOT NULL,
	FailedPasswordAnswerAttemptWindowStart datetime NOT NULL,
	FailedPasswordAttemptCount int NOT NULL,
	FailedPasswordAttemptWindowStart datetime NOT NULL,
	DatabaseKeyID dbo.typ_key NULL,
	CreationDate dbo.typ_date NOT NULL,
	created_by dbo.typ_counselor NOT NULL,
	ts timestamp NOT NULL
	)  ON [PRIMARY]
GO
ALTER TABLE dbo.Tmp_client_www SET (LOCK_ESCALATION = TABLE)
GO
GRANT DELETE ON dbo.Tmp_client_www TO public  AS dbo
GO
GRANT INSERT ON dbo.Tmp_client_www TO public  AS dbo
GO
GRANT SELECT ON dbo.Tmp_client_www TO public  AS dbo
GO
GRANT UPDATE ON dbo.Tmp_client_www TO public  AS dbo
GO
DECLARE @v sql_variant 
SET @v = N'ID associated with the WWW access'
EXECUTE sp_addextendedproperty N'MS_Description', @v, N'SCHEMA', N'dbo', N'TABLE', N'Tmp_client_www', N'COLUMN', N'UserName'
GO
DECLARE @v sql_variant 
SET @v = N'Password for the client. This is the MD5 hash of the password.'
EXECUTE sp_addextendedproperty N'MS_Description', @v, N'SCHEMA', N'dbo', N'TABLE', N'Tmp_client_www', N'COLUMN', N'Password'
GO
DECLARE @v sql_variant 
SET @v = N'E-mail address of the client associated with the WWW access'
EXECUTE sp_addextendedproperty N'MS_Description', @v, N'SCHEMA', N'dbo', N'TABLE', N'Tmp_client_www', N'COLUMN', N'Email'
GO
DECLARE @v sql_variant 
SET @v = N'Question associated with the client when the account was created'
EXECUTE sp_addextendedproperty N'MS_Description', @v, N'SCHEMA', N'dbo', N'TABLE', N'Tmp_client_www', N'COLUMN', N'PasswordQuestion'
GO
DECLARE @v sql_variant 
SET @v = N'Client''s answer to the question'
EXECUTE sp_addextendedproperty N'MS_Description', @v, N'SCHEMA', N'dbo', N'TABLE', N'Tmp_client_www', N'COLUMN', N'PasswordAnswer'
GO
DECLARE @v sql_variant 
SET @v = N'Date / Time when the password was last changed by the client'
EXECUTE sp_addextendedproperty N'MS_Description', @v, N'SCHEMA', N'dbo', N'TABLE', N'Tmp_client_www', N'COLUMN', N'LastPasswordChangeDate'
GO
DECLARE @v sql_variant 
SET @v = N'Date / Time when the client last "logged on" to the system'
EXECUTE sp_addextendedproperty N'MS_Description', @v, N'SCHEMA', N'dbo', N'TABLE', N'Tmp_client_www', N'COLUMN', N'LastLoginDate'
GO
DECLARE @v sql_variant 
SET @v = N'Date / Time when the row was created'
EXECUTE sp_addextendedproperty N'MS_Description', @v, N'SCHEMA', N'dbo', N'TABLE', N'Tmp_client_www', N'COLUMN', N'CreationDate'
GO
DECLARE @v sql_variant 
SET @v = N'Name of the person who created the row.'
EXECUTE sp_addextendedproperty N'MS_Description', @v, N'SCHEMA', N'dbo', N'TABLE', N'Tmp_client_www', N'COLUMN', N'created_by'
GO
ALTER TABLE dbo.Tmp_client_www ADD CONSTRAINT
	DF_client_www_PKID DEFAULT (newid()) FOR PKID
GO
ALTER TABLE dbo.Tmp_client_www ADD CONSTRAINT
	DF_client_www_ApplicationName DEFAULT ('/') FOR ApplicationName
GO
EXECUTE sp_bindefault N'dbo.default_false', N'dbo.Tmp_client_www.IsLockedOut'
GO
EXECUTE sp_bindefault N'dbo.default_true', N'dbo.Tmp_client_www.IsApproved'
GO
ALTER TABLE dbo.Tmp_client_www ADD CONSTRAINT
	DF_client_www_LastPasswordChangeDate DEFAULT (getutcdate()) FOR LastPasswordChangeDate
GO
ALTER TABLE dbo.Tmp_client_www ADD CONSTRAINT
	DF_client_www_LastLoginDate DEFAULT (getutcdate()) FOR LastLoginDate
GO
ALTER TABLE dbo.Tmp_client_www ADD CONSTRAINT
	DF_client_www_LastActivityDate DEFAULT (getutcdate()) FOR LastActivityDate
GO
EXECUTE sp_bindefault N'dbo.default_zero', N'dbo.Tmp_client_www.FailedPasswordAnswerAttemptCount'
GO
ALTER TABLE dbo.Tmp_client_www ADD CONSTRAINT
	DF_client_www_FailedPasswordAnswerAttemptWindowStart DEFAULT (getutcdate()) FOR FailedPasswordAnswerAttemptWindowStart
GO
EXECUTE sp_bindefault N'dbo.default_zero', N'dbo.Tmp_client_www.FailedPasswordAttemptCount'
GO
ALTER TABLE dbo.Tmp_client_www ADD CONSTRAINT
	DF_client_www_FailedPasswordAttemptWindowStart DEFAULT (getutcdate()) FOR FailedPasswordAttemptWindowStart
GO
IF EXISTS(SELECT * FROM dbo.client_www)
	 EXEC('INSERT INTO dbo.Tmp_client_www (PKID, ApplicationName, UserName, Password, Email, PasswordQuestion, PasswordAnswer, Comment, IsLockedOut, IsApproved, LastPasswordChangeDate, LastLoginDate, LastActivityDate, LastLockoutDate, FailedPasswordAnswerAttemptCount, FailedPasswordAnswerAttemptWindowStart, FailedPasswordAttemptCount, FailedPasswordAttemptWindowStart, DatabaseKeyID, CreationDate, created_by)
		SELECT PKID, ApplicationName, UserName, Password, Email, PasswordQuestion, PasswordAnswer, Comment, IsLockedOut, IsApproved, LastPasswordChangeDate, LastLoginDate, LastActivityDate, LastLockoutDate, FailedPasswordAnswerAttemptCount, FailedPasswordAnswerAttemptWindowStart, FailedPasswordAttemptCount, FailedPasswordAttemptWindowStart, DatabaseKeyID, CreationDate, created_by FROM dbo.client_www WITH (HOLDLOCK TABLOCKX)')
GO
DROP TABLE dbo.client_www
GO
EXECUTE sp_rename N'dbo.Tmp_client_www', N'client_www', 'OBJECT' 
GO
ALTER TABLE dbo.client_www ADD CONSTRAINT
	PK_client_www PRIMARY KEY CLUSTERED 
	(
	PKID
	) WITH( STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX IX_client_www_1 ON dbo.client_www
	(
	UserName,
	ApplicationName
	) WITH( STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX IX_client_www_2 ON dbo.client_www
	(
	Email,
	ApplicationName
	) WITH( STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
DENY DELETE ON dbo.client_www TO www_role  AS dbo 
GO
DENY INSERT ON dbo.client_www TO www_role  AS dbo 
GO
DENY SELECT ON dbo.client_www TO www_role  AS dbo 
GO
DENY UPDATE ON dbo.client_www TO www_role  AS dbo 
GO
COMMIT
BEGIN TRANSACTION
GO
ALTER TABLE dbo.client_www_notes ADD CONSTRAINT
	FK_client_www_notes_client_www FOREIGN KEY
	(
	PKID
	) REFERENCES dbo.client_www
	(
	PKID
	) ON UPDATE  CASCADE 
	 ON DELETE  CASCADE 
GO
ALTER TABLE dbo.client_www_notes SET (LOCK_ESCALATION = TABLE)
GO
COMMIT
GO
