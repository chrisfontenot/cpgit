USE [DebtPlus]
GO
/****** Object:  Table [dbo].[StudentLoanAccounts]    Script Date: 09/15/2014 13:13:26 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[StudentLoanAccounts](
	[oID] [dbo].[typ_key] NOT NULL,
	[client] [dbo].[typ_key] NULL,
	[client_note] [dbo].[typ_key] NULL,
	[homeAddressID] [dbo].[typ_key] NULL,
	[homePhoneID] [dbo].[typ_key] NULL,
	[dependents] [int] NULL,
	[household_people] [int] NULL,
	[marital_status] [dbo].[typ_key] NULL,
	[EmailID] [dbo].[typ_key] NULL,
	[NameID] [dbo].[typ_key] NULL,
	[race] [int] NULL,
	[gender] [int] NULL,
	[ethnicity] [int] NULL,
	[dob] [datetime] NULL,
	[gross_income] [money] NULL,
	[contact_me] [bit] NOT NULL,
	[receive_tips] [bit] NOT NULL,
	[why_taking_modules] [varchar](512) NULL,
	[main_goal] [varchar](512) NULL,
	[company_id] [varchar](512) NULL,
	[financial_challenge] [varchar](512) NULL,
 CONSTRAINT [PK_StudentLoanAccounts] PRIMARY KEY CLUSTERED 
(
	[oID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
