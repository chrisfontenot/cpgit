USE [DebtPlus]
GO
/****** Object:  Table [dbo].[Calendar]    Script Date: 09/15/2014 13:13:25 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Calendar](
	[dt] [datetime] NOT NULL,
	[isWeekday] [bit] NOT NULL,
	[isHoliday] [bit] NOT NULL,
	[isBankHoliday] [bit] NOT NULL,
	[Y] [int] NOT NULL,
	[FY] [int] NOT NULL,
	[Q] [int] NOT NULL,
	[M] [int] NOT NULL,
	[FM] [int] NOT NULL,
	[D] [int] NOT NULL,
	[DW] [int] NOT NULL,
	[DOY] [int] NOT NULL,
	[DayNumber] [int] NOT NULL,
	[monthname] [varchar](9) NOT NULL,
	[dayname] [varchar](9) NOT NULL,
	[W] [int] NOT NULL,
	[UTCOffset] [int] NOT NULL,
	[HolidayDescription] [varchar](32) NOT NULL,
 CONSTRAINT [PK_Calendar] PRIMARY KEY CLUSTERED 
(
	[dt] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 90) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
