/* To prevent any potential data loss issues, you should review this script in detail before running it outside the context of the database designer.*/
BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
execute UPDATE_ADD_COLUMN 'HouseholdHeadTypes', 'hpf'
BEGIN TRANSACTION
GO
CREATE TABLE dbo.Tmp_HouseholdHeadTypes
	(
	oID dbo.typ_key NOT NULL IDENTITY (1, 1),
	description dbo.typ_description NOT NULL,
	nfcc varchar(4) NULL,
	rpps varchar(4) NULL,
	nfmcp_section varchar(4) NULL,
	note dbo.typ_message NULL,
	hud_9902_section varchar(256) NULL,
	[default] bit NOT NULL,
	ActiveFlag bit NOT NULL,
	hpf varchar(50) NULL,
	ts timestamp NOT NULL,
	epay varchar(4) NULL
	)  ON [PRIMARY]
GO
GRANT DELETE ON dbo.Tmp_HouseholdHeadTypes TO public  AS dbo
GO
GRANT INSERT ON dbo.Tmp_HouseholdHeadTypes TO public  AS dbo
GO
GRANT SELECT ON dbo.Tmp_HouseholdHeadTypes TO public  AS dbo
GO
GRANT UPDATE ON dbo.Tmp_HouseholdHeadTypes TO public  AS dbo
GO
EXECUTE sp_bindefault N'dbo.default_false', N'dbo.Tmp_HouseholdHeadTypes.[default]'
GO
EXECUTE sp_bindefault N'dbo.default_true', N'dbo.Tmp_HouseholdHeadTypes.ActiveFlag'
GO
SET IDENTITY_INSERT dbo.Tmp_HouseholdHeadTypes ON
GO
IF EXISTS(SELECT * FROM dbo.HouseholdHeadTypes)
	 EXEC('INSERT INTO dbo.Tmp_HouseholdHeadTypes (oID, description, nfcc, rpps, epay, nfmcp_section, note, hud_9902_section, [default], ActiveFlag, hpf)
		SELECT oID, description, nfcc, rpps, epay, nfmcp_section, note, hud_9902_section, [default], ActiveFlag, hpf FROM dbo.HouseholdHeadTypes WITH (HOLDLOCK TABLOCKX)')
GO
SET IDENTITY_INSERT dbo.Tmp_HouseholdHeadTypes OFF
GO
DROP TABLE dbo.HouseholdHeadTypes
GO
EXECUTE sp_rename N'dbo.Tmp_HouseholdHeadTypes', N'HouseholdHeadTypes', 'OBJECT' 
GO
ALTER TABLE dbo.HouseholdHeadTypes ADD CONSTRAINT
	PK_HouseholdHeadTypes PRIMARY KEY CLUSTERED 
	(
	oID
	) WITH( STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
DENY DELETE ON dbo.HouseholdHeadTypes TO www_role  AS dbo 
GO
DENY INSERT ON dbo.HouseholdHeadTypes TO www_role  AS dbo 
GO
DENY SELECT ON dbo.HouseholdHeadTypes TO www_role  AS dbo 
GO
DENY UPDATE ON dbo.HouseholdHeadTypes TO www_role  AS dbo 
GO
COMMIT
