USE [DebtPlus]
GO
/****** Object:  Table [dbo].[rpps_response_details_cdt]    Script Date: 09/15/2014 13:13:26 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[rpps_response_details_cdt](
	[rpps_response_details_cdt] [dbo].[typ_key] IDENTITY(1,1) NOT NULL,
	[rpps_response_detail] [dbo].[typ_key] NOT NULL,
	[last_communication] [dbo].[typ_trace_number] NULL,
	[company_identification] [varchar](6) NULL,
	[ssn] [dbo].[typ_ssn] NULL,
	[client] [dbo].[typ_client] NULL,
 CONSTRAINT [PK_rpps_response_details_cdt] PRIMARY KEY CLUSTERED 
(
	[rpps_response_details_cdt] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 90) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
