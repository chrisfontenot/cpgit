USE [DebtPlus]
GO
/****** Object:  Table [dbo].[OCSClientSearch]    Script Date: 01/20/2016 08:56:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[OCSClientSearch](
	[Id] [int] NOT NULL,
	[ClientID] [int] NOT NULL,
	[ActiveFlag] [bit] NOT NULL,
	[Archive] [bit] NOT NULL,
	[BatchName] [varchar](200) NULL,
	[InvestorNo] [varchar](20) NULL,
	[StreetAddress] [varchar](50) NULL,
	[City] [varchar](50) NULL,
	[StateAbbreviation] [varchar](50) NULL,
	[ZipCode] [varchar](10) NULL,
	[AppSSN] [varchar](11) NULL,
	[Program] [varchar](100) NULL,
	[AppFirstName] [varchar](50) NULL,
	[AppLastName] [varchar](50) NULL,
	[Phone1] [varchar](21) NULL,
	[Phone2] [varchar](21) NULL,
	[Phone3] [varchar](21) NULL,
	[Phone4] [varchar](21) NULL,
	[Phone5] [varchar](21) NULL,
	[Servicer] [varchar](50) NULL,
	[ServicerLoanNum] [varchar](50) NULL,
	[ServicerID] [varchar](20) NULL,
	[UserName] [varchar](50) NOT NULL,
 CONSTRAINT [PK_OCSClientSearch] PRIMARY KEY CLUSTERED 
(
	[Id] ASC,
	[ClientID] ASC,
	[UserName] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF