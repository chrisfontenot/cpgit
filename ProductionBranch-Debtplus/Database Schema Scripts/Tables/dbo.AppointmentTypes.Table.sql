USE [DebtPlus]
GO
/****** Object:  Table [dbo].[AppointmentTypes]    Script Date: 09/15/2014 13:13:25 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[AppointmentTypes](
	[oID] [dbo].[typ_key] IDENTITY(1,1) NOT NULL,
	[Description] [dbo].[typ_description] NOT NULL,
	[Duration] [int] NOT NULL,
	[BookLetter] [varchar](10) NULL,
	[RescheduleLetter] [varchar](10) NULL,
	[CancelLetter] [varchar](10) NULL,
	[ContactType] [dbo].[typ_key] NULL,
	[BankruptcyClass] [dbo].[typ_key] NULL,
	[MissedAppointmentRetentionEvent] [dbo].[typ_key] NULL,
	[Default] [bit] NOT NULL,
	[ActiveFlag] [bit] NOT NULL,
	[isInitialAppointment] [bit] NOT NULL,
 CONSTRAINT [PK_AppointmentTypes] PRIMARY KEY CLUSTERED 
(
	[oID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Duration for the appointment, in minutes' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'AppointmentTypes', @level2type=N'COLUMN',@level2name=N'Duration'
GO
ALTER TABLE [dbo].[AppointmentTypes] ADD  CONSTRAINT [DF_AppointmentTypes_Duration]  DEFAULT ((60)) FOR [Duration]
GO
