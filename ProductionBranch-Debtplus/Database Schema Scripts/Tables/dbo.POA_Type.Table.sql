/*
   Wednesday, November 04, 20156:28:06 AM
   User: sa
   Server: 10.0.199.12
   Database: DebtPlus_HPF
   Application: 
*/

/* To prevent any potential data loss issues, you should review this script in detail before running it outside the context of the database designer.*/
BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION
GO
CREATE TABLE dbo.Tmp_POA_Type
	(
	ID dbo.typ_key NOT NULL IDENTITY (1, 1),
	Description dbo.typ_description NOT NULL,
	CreatedDate dbo.typ_date NOT NULL,
	ts timestamp NOT NULL
	)  ON [PRIMARY]
GO
SET IDENTITY_INSERT dbo.Tmp_POA_Type ON
GO
IF EXISTS(SELECT * FROM dbo.POA_Type)
	 EXEC('INSERT INTO dbo.Tmp_POA_Type (ID, Description, CreatedDate)
		SELECT ID, Description, CreatedDate FROM dbo.POA_Type WITH (HOLDLOCK TABLOCKX)')
GO
SET IDENTITY_INSERT dbo.Tmp_POA_Type OFF
GO
DROP TABLE dbo.POA_Type
GO
EXECUTE sp_rename N'dbo.Tmp_POA_Type', N'POA_Type', 'OBJECT' 
GO
ALTER TABLE dbo.POA_Type ADD CONSTRAINT
	PK_POA_Type PRIMARY KEY CLUSTERED 
	(
	ID
	) WITH( STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]

GO
COMMIT
