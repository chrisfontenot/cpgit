USE [DebtPlus]
GO
/****** Object:  Table [dbo].[TimeZones]    Script Date: 09/15/2014 13:13:26 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[TimeZones](
	[oID] [dbo].[typ_key] IDENTITY(11,1) NOT NULL,
	[Description] [dbo].[typ_description] NOT NULL,
	[TimeZoneShiftRule] [dbo].[typ_key] NOT NULL,
	[GMTOffset] [float] NULL,
	[Default] [bit] NOT NULL,
	[StandardTime] [varchar](10) NULL,
	[DSTTime] [varchar](10) NULL,
 CONSTRAINT [PK_TimeZones] PRIMARY KEY CLUSTERED 
(
	[oID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
