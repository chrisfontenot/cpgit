EXECUTE UPDATE_DROP_CONSTRAINTS 'vendor_addkeys'
GO
/* To prevent any potential data loss issues, you should review this script in detail before running it outside the context of the database designer.*/
BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION
GO
CREATE TABLE dbo.Tmp_vendor_addkeys
	(
	oID dbo.typ_key NOT NULL IDENTITY (1, 1),
	vendor dbo.typ_key NOT NULL,
	additional dbo.typ_description NOT NULL,
	date_created dbo.typ_date NOT NULL,
	created_by dbo.typ_counselor NOT NULL,
	ts timestamp NOT NULL
	)  ON [PRIMARY]
GO
ALTER TABLE dbo.Tmp_vendor_addkeys SET (LOCK_ESCALATION = TABLE)
GO
IF EXISTS(SELECT * FROM SYSOBJECTS WHERE TYPE='U' AND NAME=N'vendor_addkeys')
BEGIN
	EXEC('SET IDENTITY_INSERT dbo.Tmp_vendor_addkeys ON;
		  INSERT INTO dbo.Tmp_vendor_addkeys (oID, vendor, additional, date_created, created_by)
		  SELECT oID, vendor, additional, date_created, created_by FROM dbo.vendor_addkeys WITH (HOLDLOCK TABLOCKX);
		  SET IDENTITY_INSERT dbo.Tmp_vendor_addkeys OFF;')
	EXEC('DROP TABLE dbo.vendor_addkeys')
END
GO
EXECUTE sp_rename N'dbo.Tmp_vendor_addkeys', N'vendor_addkeys', 'OBJECT' 
GO
ALTER TABLE dbo.vendor_addkeys ADD CONSTRAINT
	PK_vendor_addkeys PRIMARY KEY CLUSTERED 
	(
	oID
	) WITH( STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX IX_vendor_addkeys_1 ON dbo.vendor_addkeys
	(
	vendor
	) WITH( STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
ALTER TABLE dbo.vendor_addkeys ADD CONSTRAINT
	FK_vendor_addkeys_vendors FOREIGN KEY
	(
	vendor
	) REFERENCES dbo.vendors
	(
	oID
	) ON UPDATE  CASCADE 
	 ON DELETE  CASCADE 
GO
COMMIT
