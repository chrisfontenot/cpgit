USE [DebtPlus]
GO
/****** Object:  Table [dbo].[debt_notes]    Script Date: 09/15/2014 13:13:26 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[debt_notes](
	[debt_note] [dbo].[typ_key] IDENTITY(1,1) NOT NULL,
	[type] [varchar](2) NOT NULL,
	[client] [dbo].[typ_client] NULL,
	[creditor] [dbo].[typ_creditor] NULL,
	[client_creditor] [dbo].[typ_key] NULL,
	[client_creditor_proposal] [dbo].[typ_key] NULL,
	[output_batch] [varchar](50) NULL,
	[note_text] [text] NULL,
	[note_amount] [money] NOT NULL,
	[note_date] [datetime] NOT NULL,
	[creditor_method] [dbo].[typ_key] NULL,
	[bank] [dbo].[typ_key] NULL,
	[rpps_transaction] [dbo].[typ_key] NULL,
	[rpps_biller_id] [dbo].[typ_rpps_biller_id] NULL,
	[epay_transaction] [dbo].[typ_key] NULL,
	[epay_biller_id] [dbo].[typ_epay_biller] NULL,
	[drop_reason] [dbo].[typ_key] NULL,
	[date_created] [dbo].[typ_date] NOT NULL,
	[created_by] [dbo].[typ_counselor] NOT NULL,
	[old_debt_note] [int] NULL,
 CONSTRAINT [PK_debt_notes] PRIMARY KEY CLUSTERED 
(
	[debt_note] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 90) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Primary key to the table' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'debt_notes', @level2type=N'COLUMN',@level2name=N'debt_note'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Type of note. "PR" = proposal, "MS" = message, "AN" = account note, "SN" = system note, "DR" = drop notice' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'debt_notes', @level2type=N'COLUMN',@level2name=N'type'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Client Identifier' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'debt_notes', @level2type=N'COLUMN',@level2name=N'client'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Creditor Identifier' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'debt_notes', @level2type=N'COLUMN',@level2name=N'creditor'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Pointer to the debt being referenced if required' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'debt_notes', @level2type=N'COLUMN',@level2name=N'client_creditor'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Pointer to the proposal being referenced if required' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'debt_notes', @level2type=N'COLUMN',@level2name=N'client_creditor_proposal'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Set to the output batch ID when the note is sent' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'debt_notes', @level2type=N'COLUMN',@level2name=N'output_batch'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Actual text of the note' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'debt_notes', @level2type=N'COLUMN',@level2name=N'note_text'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'If an amount is referenced, the value is here' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'debt_notes', @level2type=N'COLUMN',@level2name=N'note_amount'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'If a date is referenced, the value is here' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'debt_notes', @level2type=N'COLUMN',@level2name=N'note_date'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Poitner to the method for this creditor note' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'debt_notes', @level2type=N'COLUMN',@level2name=N'creditor_method'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Pointer to the corresponding bank number' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'debt_notes', @level2type=N'COLUMN',@level2name=N'bank'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'If RPPS, this is a pointer to the RPPS transaction table entry when generated' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'debt_notes', @level2type=N'COLUMN',@level2name=N'rpps_transaction'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'If RPPS, this is the biller ID' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'debt_notes', @level2type=N'COLUMN',@level2name=N'rpps_biller_id'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'If ePay, this is a pointer to the ePay transaction table item' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'debt_notes', @level2type=N'COLUMN',@level2name=N'epay_transaction'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'If ePay, this is the epay biller ID' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'debt_notes', @level2type=N'COLUMN',@level2name=N'epay_biller_id'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'If this is a drop note, this is the drop reason when it is known.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'debt_notes', @level2type=N'COLUMN',@level2name=N'drop_reason'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Date the item was created' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'debt_notes', @level2type=N'COLUMN',@level2name=N'date_created'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Person who created the item' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'debt_notes', @level2type=N'COLUMN',@level2name=N'created_by'
GO
ALTER TABLE [dbo].[debt_notes] ADD  CONSTRAINT [DF_debt_notes_type]  DEFAULT ('PR') FOR [type]
GO
