USE [DebtPlus]
GO
/****** Object:  Table [dbo].[Housing_training_courses]    Script Date: 09/15/2014 13:13:26 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Housing_training_courses](
	[oID] [dbo].[typ_key] IDENTITY(1,1) NOT NULL,
	[TrainingTitle] [dbo].[typ_description] NOT NULL,
	[TrainingDuration] [int] NOT NULL,
	[TrainingOrganization] [varchar](10) NULL,
	[TrainingOrganizationOther] [dbo].[typ_description] NULL,
	[TrainingSponsor] [varchar](10) NULL,
	[TrainingSponsorOther] [dbo].[typ_description] NULL,
	[Certificate] [bit] NOT NULL,
	[ActiveFlag] [bit] NOT NULL,
	[old_oid] [int] NULL,
 CONSTRAINT [PK_housing_training_courses] PRIMARY KEY CLUSTERED 
(
	[oID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
