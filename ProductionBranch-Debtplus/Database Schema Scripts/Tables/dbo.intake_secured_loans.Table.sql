USE [DebtPlus]
GO
/****** Object:  Table [dbo].[intake_secured_loans]    Script Date: 09/15/2014 13:13:26 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[intake_secured_loans](
	[secured_loan] [dbo].[typ_key] IDENTITY(1,1) NOT NULL,
	[secured_property] [dbo].[typ_key] NOT NULL,
	[priority] [int] NOT NULL,
	[lender] [dbo].[typ_description] NOT NULL,
	[account_number] [dbo].[typ_description] NOT NULL,
	[case_number] [dbo].[typ_description] NOT NULL,
	[interest_rate] [float] NOT NULL,
	[payment] [money] NOT NULL,
	[original_amount] [money] NOT NULL,
	[balance] [money] NOT NULL,
	[periods] [int] NOT NULL,
	[past_due_amount] [money] NOT NULL,
	[past_due_periods] [int] NOT NULL,
	[loan_type] [dbo].[typ_key] NOT NULL,
	[created_by] [dbo].[typ_counselor] NOT NULL,
	[date_created] [dbo].[typ_date] NOT NULL,
 CONSTRAINT [PK_intake_secured_loans] PRIMARY KEY CLUSTERED 
(
	[secured_loan] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 90) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
