USE [DebtPlus]
GO
/****** Object:  Table [dbo].[client_creditor_proposals]    Script Date: 09/15/2014 13:13:26 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[client_creditor_proposals](
	[client_creditor_proposal] [dbo].[typ_key] IDENTITY(1,1) NOT NULL,
	[client_creditor] [dbo].[typ_client] NOT NULL,
	[proposal_status] [int] NOT NULL,
	[proposal_print_date] [datetime] NULL,
	[proposal_accepted_by] [varchar](255) NULL,
	[proposal_status_date] [datetime] NULL,
	[proposal_reject_reason] [dbo].[typ_key] NULL,
	[proposal_reject_disp] [dbo].[typ_key] NULL,
	[counter_amount] [money] NULL,
	[missing_item] [varchar](50) NULL,
	[rpps_client_type_indicator] [varchar](1) NOT NULL,
	[percent_balance] [float] NULL,
	[terms] [int] NULL,
	[proposed_amount] [money] NULL,
	[proposed_start_date] [datetime] NULL,
	[proposal_batch_id] [dbo].[typ_key] NULL,
	[proposed_balance] [money] NULL,
	[proposed_date] [datetime] NOT NULL,
	[full_disclosure] [bit] NOT NULL,
	[bank] [dbo].[typ_key] NULL,
	[rpps_biller_id] [dbo].[typ_rpps_biller_id] NULL,
	[epay_biller_id] [dbo].[typ_epay_biller] NULL,
	[date_created] [dbo].[typ_date] NOT NULL,
	[created_by] [dbo].[typ_counselor] NOT NULL,
	[rpps_resubmit_date] [datetime] NULL,
	[rpps_cycle_months_remaining] [int] NULL,
	[rpps_forgiven_pct] [float] NULL,
	[rpps_first_payment_date] [datetime] NULL,
	[rpps_good_thru_date] [datetime] NULL,
	[rpps_ineligible_reason] [varchar](10) NULL,
	[rpps_internal_program_ends_date] [datetime] NULL,
	[rpps_interest_rate] [float] NULL,
	[rpps_third_party_detail] [varchar](10) NULL,
	[rpps_third_party_contact] [varchar](80) NULL,
	[old_client_creditor_proposal] [int] NULL,
 CONSTRAINT [PK_client_creditor_proposals] PRIMARY KEY CLUSTERED 
(
	[client_creditor_proposal] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The primary key to the table.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'client_creditor_proposals', @level2type=N'COLUMN',@level2name=N'client_creditor_proposal'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Pointer to the specific debt record for this proposal.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'client_creditor_proposals', @level2type=N'COLUMN',@level2name=N'client_creditor'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Status of the proposal (0=created,1=pending,2=accepted,3=accepted by default,4=rejected)' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'client_creditor_proposals', @level2type=N'COLUMN',@level2name=N'proposal_status'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Name of person at creditor who accepted the proposal when status = 2' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'client_creditor_proposals', @level2type=N'COLUMN',@level2name=N'proposal_accepted_by'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Date the status was last changed' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'client_creditor_proposals', @level2type=N'COLUMN',@level2name=N'proposal_status_date'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Item from the proposal_reject_reasons why the proposal was rejected' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'client_creditor_proposals', @level2type=N'COLUMN',@level2name=N'proposal_reject_reason'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Item from the proposal_dispositions table what was done to correct this proposal' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'client_creditor_proposals', @level2type=N'COLUMN',@level2name=N'proposal_reject_disp'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'If the proposal is rejected this is the counter offer amount given by the creditor' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'client_creditor_proposals', @level2type=N'COLUMN',@level2name=N'counter_amount'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'If the proposal is rejected this is the missing item from the proposal' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'client_creditor_proposals', @level2type=N'COLUMN',@level2name=N'missing_item'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Copy of the disbursement_factor at the time that the proposal batch was sent (closed) to the creditor' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'client_creditor_proposals', @level2type=N'COLUMN',@level2name=N'proposed_amount'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Copy of the debt start date at the time the proposal batch was sent (closed) to the creditor' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'client_creditor_proposals', @level2type=N'COLUMN',@level2name=N'proposed_start_date'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Batch ID for sending proposals' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'client_creditor_proposals', @level2type=N'COLUMN',@level2name=N'proposal_batch_id'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Account balance at the time of the proposal' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'client_creditor_proposals', @level2type=N'COLUMN',@level2name=N'proposed_balance'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Effective date for the proposal.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'client_creditor_proposals', @level2type=N'COLUMN',@level2name=N'proposed_date'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Was the proposal full disclosure?' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'client_creditor_proposals', @level2type=N'COLUMN',@level2name=N'full_disclosure'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Which bank was used to send the proposal' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'client_creditor_proposals', @level2type=N'COLUMN',@level2name=N'bank'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'RPPS biller ID if bank type = rpps' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'client_creditor_proposals', @level2type=N'COLUMN',@level2name=N'rpps_biller_id'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'ePay biller ID if bank type = epay' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'client_creditor_proposals', @level2type=N'COLUMN',@level2name=N'epay_biller_id'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Date/Time when the row was created.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'client_creditor_proposals', @level2type=N'COLUMN',@level2name=N'date_created'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Person who created the row.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'client_creditor_proposals', @level2type=N'COLUMN',@level2name=N'created_by'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'This table shows the proposals that have been created in the system. Each proposal is attached to the debt record in the client_creditor table.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'client_creditor_proposals'
GO
