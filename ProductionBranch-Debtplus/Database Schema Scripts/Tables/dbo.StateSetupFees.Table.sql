EXECUTE UPDATE_DROP_CONSTRAINTS 'StateSetupFees'
GO
BEGIN TRANSACTION
GO
CREATE TABLE dbo.Tmp_StateSetupFees
	(
	oID dbo.typ_key NOT NULL IDENTITY(1,1),
	state dbo.typ_key NOT NULL,
	amount money NOT NULL,
	effective_date datetime NOT NULL,
	cutoff_date datetime NULL,
	date_created dbo.typ_date NOT NULL,
	created_by dbo.typ_counselor NOT NULL,
	ts timestamp NOT NULL
	)  ON [PRIMARY]
GO
EXECUTE sp_bindefault N'dbo.default_money', N'dbo.Tmp_StateSetupFees.amount'
GO
EXECUTE sp_bindefault N'dbo.default_date', N'dbo.Tmp_StateSetupFees.effective_date'
GO
SET IDENTITY_INSERT dbo.Tmp_StateSetupFees ON
GO
IF EXISTS(SELECT * FROM SYSOBJECTS WHERE TYPE='U' AND NAME=N'StateSetupFees')
BEGIN
	EXEC('INSERT INTO dbo.Tmp_StateSetupFees (oID, state, amount, effective_date, cutoff_date, date_created, created_by)
		SELECT oID, state, amount, effective_date, cutoff_date, date_created, created_by FROM dbo.StateSetupFees WITH (HOLDLOCK TABLOCKX)')
	EXEC('DROP TABLE dbo.StateSetupFees')
END
GO
SET IDENTITY_INSERT dbo.Tmp_StateSetupFees OFF
GO
EXECUTE sp_rename N'dbo.Tmp_StateSetupFees', N'StateSetupFees', 'OBJECT' 
GO
ALTER TABLE dbo.StateSetupFees ADD CONSTRAINT
	PK_StateSetupFees PRIMARY KEY CLUSTERED 
	(
	oID
	) WITH( STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
ALTER TABLE dbo.StateSetupFees ADD CONSTRAINT
	FK_StateSetupFees_states FOREIGN KEY
	(
	state
	) REFERENCES dbo.states
	(
	state
	) ON UPDATE  NO ACTION 
	 ON DELETE  CASCADE 
GO
DECLARE @v sql_variant 
SET @v = N'Link StateSetupFees.state to States.state'
EXECUTE sp_addextendedproperty N'MS_Description', @v, N'SCHEMA', N'dbo', N'TABLE', N'StateSetupFees', N'CONSTRAINT', N'FK_StateSetupFees_states'
GO
COMMIT
