EXECUTE UPDATE_DROP_CONSTRAINTS 'vendor_notes'
GO
/* To prevent any potential data loss issues, you should review this script in detail before running it outside the context of the database designer.*/
BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION
GO
CREATE TABLE dbo.Tmp_vendor_notes
	(
	oID dbo.typ_key NOT NULL IDENTITY (1, 1),
	vendor dbo.typ_key NOT NULL,
	type int NOT NULL,
	expires datetime NULL,
	dont_edit bit NOT NULL,
	dont_delete bit NOT NULL,
	dont_print bit NOT NULL,
	subject dbo.typ_subject NULL,
	note text NULL,
	date_created dbo.typ_date NOT NULL,
	created_by dbo.typ_counselor NOT NULL,
	ts timestamp NOT NULL
	)  ON [PRIMARY]
	 TEXTIMAGE_ON [PRIMARY]
GO
ALTER TABLE dbo.Tmp_vendor_notes SET (LOCK_ESCALATION = TABLE)
GO
EXECUTE sp_bindefault N'dbo.default_one', N'dbo.Tmp_vendor_notes.type'
GO
EXECUTE sp_bindefault N'dbo.default_false', N'dbo.Tmp_vendor_notes.dont_edit'
GO
EXECUTE sp_bindefault N'dbo.default_false', N'dbo.Tmp_vendor_notes.dont_delete'
GO
EXECUTE sp_bindefault N'dbo.default_false', N'dbo.Tmp_vendor_notes.dont_print'
GO
IF EXISTS(SELECT * FROM SYSOBJECTS WHERE TYPE='U' AND NAME=N'vendor_notes')
BEGIN
	 EXEC('SET IDENTITY_INSERT dbo.Tmp_vendor_notes ON;
		   INSERT INTO dbo.Tmp_vendor_notes (oID, vendor, type, expires, dont_edit, dont_delete, dont_print, subject, note, date_created, created_by)
		   SELECT oID, vendor, type, expires, dont_edit, dont_delete, dont_print, subject, note, date_created, created_by FROM dbo.vendor_notes WITH (HOLDLOCK TABLOCKX);
		   SET IDENTITY_INSERT dbo.Tmp_vendor_notes OFF;')
	 EXEC('DROP TABLE dbo.vendor_notes')
END
GO
EXECUTE sp_rename N'dbo.Tmp_vendor_notes', N'vendor_notes', 'OBJECT' 
GO
ALTER TABLE dbo.vendor_notes ADD CONSTRAINT
	PK_vendor_notes PRIMARY KEY CLUSTERED 
	(
	oID
	) WITH( STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
ALTER TABLE dbo.vendor_notes ADD CONSTRAINT
	FK_vendor_notes_vendors FOREIGN KEY
	(
	vendor
	) REFERENCES dbo.vendors
	(
	oID
	) ON UPDATE  CASCADE 
	 ON DELETE  CASCADE 
GO
EXECUTE sp_bindrule N'dbo.valid_vendor_note_type', N'dbo.vendor_notes.type'
GO
COMMIT
