/* To prevent any potential data loss issues, you should review this script in detail before running it outside the context of the database designer.*/
BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION
GO
CREATE TABLE dbo.Tmp_HPFErrorCodes
	(
	oID dbo.typ_key NOT NULL IDENTITY (1, 1),
	WebService varchar(256) NOT NULL,
	Prefix varchar(16) NOT NULL,
	ErrorID varchar(16) NOT NULL,
	Description varchar(1024) NOT NULL,
	ts timestamp NOT NULL
	)  ON [PRIMARY]
GO
GRANT DELETE ON dbo.Tmp_HPFErrorCodes TO public  AS dbo
GO
GRANT INSERT ON dbo.Tmp_HPFErrorCodes TO public  AS dbo
GO
GRANT SELECT ON dbo.Tmp_HPFErrorCodes TO public  AS dbo
GO
GRANT UPDATE ON dbo.Tmp_HPFErrorCodes TO public  AS dbo
GO
SET IDENTITY_INSERT dbo.Tmp_HPFErrorCodes ON
GO
IF EXISTS(SELECT * FROM sysobjects WHERE name=N'HPFErrorCodes' AND type='U')
BEGIN
	EXEC('INSERT INTO dbo.Tmp_HPFErrorCodes (oID, WebService, Prefix, ErrorID, Description)
		SELECT oID, WebService, Prefix, ErrorID, Description FROM dbo.HPFErrorCodes WITH (HOLDLOCK TABLOCKX)')
	EXEC('DROP TABLE dbo.HPFErrorCodes')
END
GO
SET IDENTITY_INSERT dbo.Tmp_HPFErrorCodes OFF
GO
EXECUTE sp_rename N'dbo.Tmp_HPFErrorCodes', N'HPFErrorCodes', 'OBJECT' 
GO
ALTER TABLE dbo.HPFErrorCodes ADD CONSTRAINT
	PK_HPFErrorCodes PRIMARY KEY CLUSTERED 
	(
	oID
	) WITH( STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
DENY DELETE ON dbo.HPFErrorCodes TO www_role  AS dbo 
GO
DENY INSERT ON dbo.HPFErrorCodes TO www_role  AS dbo 
GO
DENY SELECT ON dbo.HPFErrorCodes TO www_role  AS dbo 
GO
DENY UPDATE ON dbo.HPFErrorCodes TO www_role  AS dbo 
GO
COMMIT
