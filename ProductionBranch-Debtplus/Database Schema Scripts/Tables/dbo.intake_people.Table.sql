USE [DebtPlus]
GO
/****** Object:  Table [dbo].[intake_people]    Script Date: 09/15/2014 13:13:26 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[intake_people](
	[intake_client] [dbo].[typ_key] NOT NULL,
	[person] [int] NOT NULL,
	[prefix] [dbo].[typ_pname] NULL,
	[first] [dbo].[typ_fname] NULL,
	[middle] [dbo].[typ_mname] NULL,
	[last] [dbo].[typ_lname] NULL,
	[suffix] [dbo].[typ_sname] NULL,
	[former] [dbo].[typ_lname] NULL,
	[email] [dbo].[typ_email] NULL,
	[ssn] [dbo].[typ_ssn] NULL,
	[gross_income] [money] NOT NULL,
	[net_income] [money] NOT NULL,
	[job] [dbo].[typ_key] NULL,
	[other_job] [dbo].[typ_description] NULL,
	[birthdate] [datetime] NULL,
	[gender] [int] NOT NULL,
	[race] [int] NOT NULL,
	[education] [varchar](4) NOT NULL,
	[employer] [dbo].[typ_key] NULL,
	[employer_name] [dbo].[typ_description] NULL,
	[employer_address1] [dbo].[typ_address] NULL,
	[employer_address2] [dbo].[typ_address] NULL,
	[employer_city] [dbo].[typ_city] NULL,
	[employer_state] [dbo].[typ_key] NULL,
	[employer_PostalCode] [varchar](50) NULL,
	[work_ph] [dbo].[typ_phone] NULL,
	[work_ext] [int] NULL,
	[work_fax] [dbo].[typ_phone] NULL,
	[date_created] [dbo].[typ_date] NOT NULL,
	[created_by] [dbo].[typ_counselor] NOT NULL,
 CONSTRAINT [PK_intake_people] PRIMARY KEY CLUSTERED 
(
	[intake_client] ASC,
	[person] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 90) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
ALTER TABLE [dbo].[intake_people] ADD  CONSTRAINT [DF_intake_people_gender]  DEFAULT (2) FOR [gender]
GO
ALTER TABLE [dbo].[intake_people] ADD  CONSTRAINT [DF_intake_people_race]  DEFAULT (10) FOR [race]
GO
