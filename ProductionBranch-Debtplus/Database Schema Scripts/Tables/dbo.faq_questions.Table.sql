USE [DebtPlus]
GO
/****** Object:  Table [dbo].[faq_questions]    Script Date: 09/15/2014 13:13:26 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[faq_questions](
	[faq_question] [dbo].[typ_key] IDENTITY(1,1) NOT NULL,
	[description] [varchar](256) NOT NULL,
	[faq_type] [dbo].[typ_key] NOT NULL,
	[count_mtd] [int] NOT NULL,
	[count_ytd] [int] NOT NULL,
	[count_prior_year] [int] NOT NULL,
	[date_created] [dbo].[typ_date] NOT NULL,
	[created_by] [dbo].[typ_counselor] NOT NULL,
 CONSTRAINT [PK_faq_questions] PRIMARY KEY CLUSTERED 
(
	[faq_question] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 90) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Primary key to the table' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'faq_questions', @level2type=N'COLUMN',@level2name=N'faq_question'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Question description value.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'faq_questions', @level2type=N'COLUMN',@level2name=N'description'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Pointer to the faq_types table. Used for the NFCC report.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'faq_questions', @level2type=N'COLUMN',@level2name=N'faq_type'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Count of questions this month' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'faq_questions', @level2type=N'COLUMN',@level2name=N'count_mtd'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Count of questions this year (not counting the value this month)' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'faq_questions', @level2type=N'COLUMN',@level2name=N'count_ytd'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'When the year rolls, this is the total number of questions asked last year.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'faq_questions', @level2type=N'COLUMN',@level2name=N'count_prior_year'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Date/Time when the row was created.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'faq_questions', @level2type=N'COLUMN',@level2name=N'date_created'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Person who created the row.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'faq_questions', @level2type=N'COLUMN',@level2name=N'created_by'
GO
