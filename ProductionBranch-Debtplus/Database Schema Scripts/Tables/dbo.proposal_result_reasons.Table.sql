USE [DebtPlus]
GO
/****** Object:  Table [dbo].[proposal_result_reasons]    Script Date: 09/15/2014 13:13:26 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[proposal_result_reasons](
	[proposal_result_reason] [dbo].[typ_key] IDENTITY(1,1) NOT NULL,
	[description] [dbo].[typ_description] NULL,
	[rpps_code] [varchar](10) NULL,
	[nfcc_code] [varchar](4) NULL,
	[epay_code] [varchar](4) NULL,
	[date_created] [dbo].[typ_date] NOT NULL,
	[created_by] [dbo].[typ_counselor] NOT NULL,
	[old_proposal_result_reason] [int] NULL,
 CONSTRAINT [PK_proposal_result_reasons] PRIMARY KEY NONCLUSTERED 
(
	[proposal_result_reason] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Primary key to the table' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'proposal_result_reasons', @level2type=N'COLUMN',@level2name=N'proposal_result_reason'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Description for the reject reason' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'proposal_result_reasons', @level2type=N'COLUMN',@level2name=N'description'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Code used by EDI proposals to represent the rejection reason' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'proposal_result_reasons', @level2type=N'COLUMN',@level2name=N'rpps_code'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Line number used for the NFCC annual report.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'proposal_result_reasons', @level2type=N'COLUMN',@level2name=N'nfcc_code'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Date/Time when the row was created.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'proposal_result_reasons', @level2type=N'COLUMN',@level2name=N'date_created'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Person who created the row.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'proposal_result_reasons', @level2type=N'COLUMN',@level2name=N'created_by'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'This table lists the results associated with a proposal. The proposal points to this table.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'proposal_result_reasons'
GO
