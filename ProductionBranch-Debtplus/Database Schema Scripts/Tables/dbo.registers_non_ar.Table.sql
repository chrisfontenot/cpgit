USE [DebtPlus]
GO
/****** Object:  Table [dbo].[registers_non_ar]    Script Date: 09/15/2014 13:13:26 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[registers_non_ar](
	[non_ar_register] [dbo].[typ_key] IDENTITY(1,1) NOT NULL,
	[tran_type] [dbo].[typ_transaction] NOT NULL,
	[non_ar_source] [dbo].[typ_key] NULL,
	[client] [dbo].[typ_client] NULL,
	[creditor] [dbo].[typ_creditor] NULL,
	[credit_amt] [money] NULL,
	[debit_amt] [money] NULL,
	[date_created] [dbo].[typ_date] NOT NULL,
	[item_date] [dbo].[typ_date] NOT NULL,
	[reference] [dbo].[typ_description] NULL,
	[created_by] [dbo].[typ_counselor] NOT NULL,
	[src_ledger_account] [dbo].[typ_glaccount] NULL,
	[dst_ledger_account] [dbo].[typ_glaccount] NULL,
	[message] [dbo].[typ_message] NULL,
	[old_non_ar_register] [int] NULL,
 CONSTRAINT [PK_registers_non_ar] PRIMARY KEY CLUSTERED 
(
	[non_ar_register] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 90) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Primary key to the table. It is a sequential number.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'registers_non_ar', @level2type=N'COLUMN',@level2name=N'non_ar_register'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Transaction code for the operation' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'registers_non_ar', @level2type=N'COLUMN',@level2name=N'tran_type'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Pointer to the source for the transaction if there is no client involved with the operation. It is mutually exclusive with a client and creditor.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'registers_non_ar', @level2type=N'COLUMN',@level2name=N'non_ar_source'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Client reflecting the operation. Used if the non_ar_source is NULL. It is mutually exclusive with a creditor and non_ar_source.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'registers_non_ar', @level2type=N'COLUMN',@level2name=N'client'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'If the contribution is from a creditor, this is the creditor ID. It is mutually exclusive with a client and non_ar_source.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'registers_non_ar', @level2type=N'COLUMN',@level2name=N'creditor'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'For deposits into Non-AR, this is the amount' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'registers_non_ar', @level2type=N'COLUMN',@level2name=N'credit_amt'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'For transfers out of Non-AR, this is the amount of the transfer' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'registers_non_ar', @level2type=N'COLUMN',@level2name=N'debit_amt'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Date/Time when the row was created.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'registers_non_ar', @level2type=N'COLUMN',@level2name=N'date_created'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Misc. date associated with the item being referenced' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'registers_non_ar', @level2type=N'COLUMN',@level2name=N'item_date'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Misc comment field associated with the item being recorded. A check number may go here.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'registers_non_ar', @level2type=N'COLUMN',@level2name=N'reference'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Person who created the row.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'registers_non_ar', @level2type=N'COLUMN',@level2name=N'created_by'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Ledger code for the source of the event if needed.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'registers_non_ar', @level2type=N'COLUMN',@level2name=N'src_ledger_account'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Ledger code for the destination of the operation if needed' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'registers_non_ar', @level2type=N'COLUMN',@level2name=N'dst_ledger_account'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Message associated with the event if used.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'registers_non_ar', @level2type=N'COLUMN',@level2name=N'message'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'This table holds the Non-AR transactions that have occurred. It shows transfers to/from the operating accounts as well as non-ar receipts.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'registers_non_ar'
GO
