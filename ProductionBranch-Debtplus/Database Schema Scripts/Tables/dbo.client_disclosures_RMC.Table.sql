EXECUTE UPDATE_DROP_CONSTRAINTS 'client_disclosures_RMC'
GO
/* To prevent any potential data loss issues, you should review this script in detail before running it outside the context of the database designer.*/
BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION
GO
CREATE TABLE dbo.Tmp_client_disclosures_RMC
	(
	oID dbo.typ_key NOT NULL IDENTITY (1, 1),
	client_disclosure_ID dbo.typ_key NOT NULL,
	q1_answer int NOT NULL,
	q2_answer int NOT NULL,
	q3_answer int NOT NULL,
	q4_answer int NOT NULL,
	q5_answer int NOT NULL,
	q6_answer int NOT NULL,
	q7_answer int NOT NULL,
	q8_answer int NOT NULL,
	q9_answer int NOT NULL,
	q10_answer int NOT NULL,
	q11_answer int NOT NULL,
	q12_answer int NOT NULL,
	q13_answer int NOT NULL,
	q14_answer int NOT NULL,
	q15_answer int NOT NULL,
	q16_answer int NOT NULL,
	q17_answer int NOT NULL,
	q18_answer int NOT NULL,
	q19_answer int NOT NULL,
	q1_name varchar(50) NOT NULL,
	q1_relation varchar(50) NOT NULL,
	q10_contact varchar(50) NOT NULL,
	q10_fax varchar(50) NOT NULL,
	q10_name varchar(50) NOT NULL,
	q10_telephone varchar(50) NOT NULL,
	q20_missing_info varchar(4096) NOT NULL,
	q15_comments varchar(512) NOT NULL,
	ts timestamp NOT NULL
	)  ON [PRIMARY]
GO
EXECUTE sp_bindefault N'dbo.default_zero', N'dbo.Tmp_client_disclosures_RMC.q1_answer'
GO
EXECUTE sp_bindefault N'dbo.default_zero', N'dbo.Tmp_client_disclosures_RMC.q2_answer'
GO
EXECUTE sp_bindefault N'dbo.default_zero', N'dbo.Tmp_client_disclosures_RMC.q3_answer'
GO
EXECUTE sp_bindefault N'dbo.default_zero', N'dbo.Tmp_client_disclosures_RMC.q4_answer'
GO
EXECUTE sp_bindefault N'dbo.default_zero', N'dbo.Tmp_client_disclosures_RMC.q5_answer'
GO
EXECUTE sp_bindefault N'dbo.default_zero', N'dbo.Tmp_client_disclosures_RMC.q6_answer'
GO
EXECUTE sp_bindefault N'dbo.default_zero', N'dbo.Tmp_client_disclosures_RMC.q7_answer'
GO
EXECUTE sp_bindefault N'dbo.default_zero', N'dbo.Tmp_client_disclosures_RMC.q8_answer'
GO
EXECUTE sp_bindefault N'dbo.default_zero', N'dbo.Tmp_client_disclosures_RMC.q9_answer'
GO
EXECUTE sp_bindefault N'dbo.default_zero', N'dbo.Tmp_client_disclosures_RMC.q10_answer'
GO
EXECUTE sp_bindefault N'dbo.default_zero', N'dbo.Tmp_client_disclosures_RMC.q11_answer'
GO
EXECUTE sp_bindefault N'dbo.default_zero', N'dbo.Tmp_client_disclosures_RMC.q12_answer'
GO
EXECUTE sp_bindefault N'dbo.default_zero', N'dbo.Tmp_client_disclosures_RMC.q13_answer'
GO
EXECUTE sp_bindefault N'dbo.default_zero', N'dbo.Tmp_client_disclosures_RMC.q14_answer'
GO
EXECUTE sp_bindefault N'dbo.default_zero', N'dbo.Tmp_client_disclosures_RMC.q15_answer'
GO
EXECUTE sp_bindefault N'dbo.default_zero', N'dbo.Tmp_client_disclosures_RMC.q16_answer'
GO
EXECUTE sp_bindefault N'dbo.default_zero', N'dbo.Tmp_client_disclosures_RMC.q17_answer'
GO
EXECUTE sp_bindefault N'dbo.default_zero', N'dbo.Tmp_client_disclosures_RMC.q18_answer'
GO
EXECUTE sp_bindefault N'dbo.default_zero', N'dbo.Tmp_client_disclosures_RMC.q19_answer'
GO
EXECUTE sp_bindefault N'dbo.default_string', N'dbo.Tmp_client_disclosures_RMC.q1_name'
GO
EXECUTE sp_bindefault N'dbo.default_string', N'dbo.Tmp_client_disclosures_RMC.q1_relation'
GO
EXECUTE sp_bindefault N'dbo.default_string', N'dbo.Tmp_client_disclosures_RMC.q10_contact'
GO
EXECUTE sp_bindefault N'dbo.default_string', N'dbo.Tmp_client_disclosures_RMC.q10_fax'
GO
EXECUTE sp_bindefault N'dbo.default_string', N'dbo.Tmp_client_disclosures_RMC.q10_name'
GO
EXECUTE sp_bindefault N'dbo.default_string', N'dbo.Tmp_client_disclosures_RMC.q10_telephone'
GO
EXECUTE sp_bindefault N'dbo.default_string', N'dbo.Tmp_client_disclosures_RMC.q20_missing_info'
GO
EXECUTE sp_bindefault N'dbo.default_string', N'dbo.Tmp_client_disclosures_RMC.q15_comments'
GO
SET IDENTITY_INSERT dbo.Tmp_client_disclosures_RMC ON
GO
IF EXISTS(SELECT * FROM sysobjects where type='U' and name='client_disclosures_RMC')
BEGIN
	EXEC('INSERT INTO dbo.Tmp_client_disclosures_RMC (oID, client_disclosure_ID, q1_answer, q2_answer, q3_answer, q4_answer, q5_answer, q6_answer, q7_answer, q8_answer, q9_answer, q10_answer, q11_answer, q12_answer, q13_answer, q14_answer, q15_answer, q16_answer, q17_answer, q18_answer, q19_answer, q1_name, q1_relation, q10_contact, q10_fax, q10_name, q10_telephone, q20_missing_info, q15_comments)
		SELECT oID, client_disclosure_ID, q1_answer, q2_answer, q3_answer, q4_answer, q5_answer, q6_answer, q7_answer, q8_answer, q9_answer, q10_answer, q11_answer, q12_answer, q13_answer, q14_answer, q15_answer, q16_answer, q17_answer, q18_answer, q19_answer, q1_name, q1_relation, q10_contact, q10_fax, q10_name, q10_telephone, q20_missing_info, q15_comments FROM dbo.client_disclosures_RMC WITH (HOLDLOCK TABLOCKX)')
	EXEC('DROP TABLE dbo.client_disclosures_RMC')
END
GO
SET IDENTITY_INSERT dbo.Tmp_client_disclosures_RMC OFF
GO
EXECUTE sp_rename N'dbo.Tmp_client_disclosures_RMC', N'client_disclosures_RMC', 'OBJECT' 
GO
ALTER TABLE dbo.client_disclosures_RMC ADD CONSTRAINT
	PK_client_disclosures_RMC PRIMARY KEY CLUSTERED 
	(
	oID
	) WITH( STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
ALTER TABLE dbo.client_disclosures_RMC ADD CONSTRAINT
	FK_client_disclosures_RMC_client_disclosures FOREIGN KEY
	(
	client_disclosure_ID
	) REFERENCES dbo.client_disclosures
	(
	oID
	) ON UPDATE  CASCADE 
	 ON DELETE  CASCADE 
GO
COMMIT
GO
GRANT SELECT,UPDATE,INSERT,DELETE ON client_disclosures_RMC TO public AS dbo;
GO
