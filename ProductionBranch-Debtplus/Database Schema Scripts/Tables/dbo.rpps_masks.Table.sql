USE [DebtPlus]
GO
/****** Object:  Table [dbo].[rpps_masks]    Script Date: 09/15/2014 13:13:26 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[rpps_masks](
	[rpps_mask] [dbo].[typ_key] IDENTITY(1,1) NOT NULL,
	[Mastercard_Key] [varchar](50) NULL,
	[timestamp] [timestamp] NOT NULL,
	[EffDate] [datetime] NOT NULL,
	[rpps_biller_id] [dbo].[typ_rpps_biller_id] NULL,
	[Length] [int] NULL,
	[Mask] [varchar](50) NULL,
	[Description] [dbo].[typ_description] NULL,
	[ExceptionMask] [bit] NOT NULL,
	[CheckDigit] [bit] NOT NULL,
	[date_created] [dbo].[typ_date] NOT NULL,
	[created_by] [dbo].[typ_counselor] NOT NULL,
 CONSTRAINT [PK_rpps_masks] PRIMARY KEY CLUSTERED 
(
	[rpps_mask] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 90) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
