USE [DebtPlus]
GO
/****** Object:  Table [dbo].[appt_counselors]    Script Date: 09/15/2014 13:13:25 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[appt_counselors](
	[appt_counselor] [dbo].[typ_key] IDENTITY(1,1) NOT NULL,
	[appt_time] [dbo].[typ_key] NOT NULL,
	[counselor] [dbo].[typ_key] NOT NULL,
	[inactive] [int] NOT NULL,
	[inactive_reason] [dbo].[typ_description] NULL,
	[date_created] [dbo].[typ_date] NOT NULL,
	[created_by] [dbo].[typ_counselor] NOT NULL,
 CONSTRAINT [PK_appt_counselors] PRIMARY KEY CLUSTERED 
(
	[appt_counselor] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 90) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'When non-zero, the counselor is not "here" at this time. This item is not to be counted.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'appt_counselors', @level2type=N'COLUMN',@level2name=N'inactive'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Reason that the counselor is not present. It is optional.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'appt_counselors', @level2type=N'COLUMN',@level2name=N'inactive_reason'
GO
