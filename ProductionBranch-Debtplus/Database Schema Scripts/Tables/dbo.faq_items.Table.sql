USE [DebtPlus]
GO
/****** Object:  Table [dbo].[faq_items]    Script Date: 09/15/2014 13:13:26 ******/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[faq_items](
	[faq_item] [dbo].[typ_key] IDENTITY(1,1) NOT NULL,
	[grouping] [varchar](50) NOT NULL,
	[description] [varchar](128) NOT NULL,
	[attributes] [varchar](50) NULL,
	[date_created] [dbo].[typ_date] NOT NULL,
	[created_by] [dbo].[typ_counselor] NOT NULL,
 CONSTRAINT [PK_faq_items] PRIMARY KEY CLUSTERED 
(
	[faq_item] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 90) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Primary key to the table' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'faq_items', @level2type=N'COLUMN',@level2name=N'faq_item'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Item grouping' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'faq_items', @level2type=N'COLUMN',@level2name=N'grouping'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Description value in the control list' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'faq_items', @level2type=N'COLUMN',@level2name=N'description'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Selection attributes' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'faq_items', @level2type=N'COLUMN',@level2name=N'attributes'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Date / Time when the row was created' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'faq_items', @level2type=N'COLUMN',@level2name=N'date_created'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Name of the person who created the row' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'faq_items', @level2type=N'COLUMN',@level2name=N'created_by'
GO
