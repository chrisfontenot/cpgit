EXECUTE UPDATE_DROP_CONSTRAINTS 'people'
GO
EXECUTE UPDATE_ADD_COLUMN 'people', 'bkfileddate', 'datetime'
EXECUTE UPDATE_ADD_COLUMN 'people', 'bkdischarge', 'datetime'
EXECUTE UPDATE_ADD_COLUMN 'people', 'bkchapter', 'int'
EXECUTE UPDATE_ADD_COLUMN 'people', 'bkPaymentCurrent', 'int'
EXECUTE UPDATE_ADD_COLUMN 'people', 'bkAuthLetterDate', 'datetime'
EXECUTE UPDATE_ADD_COLUMN 'people', 'bkCertificateNo', 'varchar(50)'
EXECUTE UPDATE_ADD_COLUMN 'people', 'bkCertificateIssued', 'datetime'
EXECUTE UPDATE_ADD_COLUMN 'people', 'bkCertificateExpires', 'datetime'
EXECUTE UPDATE_ADD_COLUMN 'people', 'bkDistrict', 'int'
GO

/* To prevent any potential data loss issues, you should review this script in detail before running it outside the context of the database designer.*/
BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION
GO
CREATE TABLE dbo.Tmp_people
	(
	Person dbo.typ_key NOT NULL IDENTITY (1, 1),
	Client dbo.typ_client NOT NULL,
	Relation dbo.typ_key NOT NULL,
	NameID dbo.typ_key NULL,
	WorkTelephoneID dbo.typ_key NULL,
	CellTelephoneID dbo.typ_key NULL,
	EmailID dbo.typ_key NULL,
	Former dbo.typ_lname NULL,
	SSN dbo.typ_ssn NULL,
	Gender dbo.typ_key NOT NULL,
	Race dbo.typ_key NOT NULL,
	Ethnicity dbo.typ_key NOT NULL,
	Disabled bit NOT NULL,
	MilitaryStatusID dbo.typ_key NOT NULL,
	MilitaryServiceID dbo.typ_key NOT NULL,
	MilitaryGradeID dbo.typ_key NOT NULL,
	MilitaryDependentID dbo.typ_key NOT NULL,
	Education dbo.typ_key NOT NULL,
	Birthdate datetime NULL,
	FICO_Score int NULL,
	CreditAgency varchar(10) NULL,
	no_fico_score_reason dbo.typ_key NULL,
	FICO_pull_date datetime NULL,
	gross_income money NOT NULL,
	net_income money NOT NULL,
	Frequency dbo.typ_key NOT NULL,
	emp_start_date datetime NULL,
	emp_end_date datetime NULL,
	employer dbo.typ_key NULL,
	job dbo.typ_key NULL,
	other_job dbo.typ_description NULL,
	bkfileddate datetime NULL,
	bkdischarge datetime NULL,
	bkchapter dbo.typ_key NULL,
	bkPaymentCurrent int NULL,
	bkAuthLetterDate datetime NULL,
	bkCertificateNo varchar(50) NULL,
	bkCertificateIssued datetime NULL,
	bkCertificateExpires datetime NULL,
	bkDistrict dbo.typ_key NULL,
	SPOC_ID int NULL,
	created_by dbo.typ_counselor NOT NULL,
	date_created dbo.typ_date NOT NULL,
	ts timestamp NOT NULL,
	POA_Name varchar(50) NULL,
	poa datetime NULL
	)  ON [PRIMARY]
GO
ALTER TABLE dbo.Tmp_people SET (LOCK_ESCALATION = TABLE)
GO
GRANT DELETE ON dbo.Tmp_people TO public  AS dbo
GO
GRANT INSERT ON dbo.Tmp_people TO public  AS dbo
GO
GRANT SELECT ON dbo.Tmp_people TO public  AS dbo
GO
GRANT UPDATE ON dbo.Tmp_people TO public  AS dbo
GO
DECLARE @v sql_variant 
SET @v = N'Client ID associated with the person'
EXECUTE sp_addextendedproperty N'MS_Description', @v, N'SCHEMA', N'dbo', N'TABLE', N'Tmp_people', N'COLUMN', N'Client'
GO
DECLARE @v sql_variant 
SET @v = N'Relationship to the applicant (self, spouse, child, etc.)'
EXECUTE sp_addextendedproperty N'MS_Description', @v, N'SCHEMA', N'dbo', N'TABLE', N'Tmp_people', N'COLUMN', N'Relation'
GO
DECLARE @v sql_variant 
SET @v = N'Former name of the person if indicated'
EXECUTE sp_addextendedproperty N'MS_Description', @v, N'SCHEMA', N'dbo', N'TABLE', N'Tmp_people', N'COLUMN', N'Former'
GO
DECLARE @v sql_variant 
SET @v = N'Social security number of the person'
EXECUTE sp_addextendedproperty N'MS_Description', @v, N'SCHEMA', N'dbo', N'TABLE', N'Tmp_people', N'COLUMN', N'SSN'
GO
DECLARE @v sql_variant 
SET @v = N'Gender for the person'
EXECUTE sp_addextendedproperty N'MS_Description', @v, N'SCHEMA', N'dbo', N'TABLE', N'Tmp_people', N'COLUMN', N'Gender'
GO
DECLARE @v sql_variant 
SET @v = N'Racial background for the person'
EXECUTE sp_addextendedproperty N'MS_Description', @v, N'SCHEMA', N'dbo', N'TABLE', N'Tmp_people', N'COLUMN', N'Race'
GO
DECLARE @v sql_variant 
SET @v = N'Ethnic background for the person'
EXECUTE sp_addextendedproperty N'MS_Description', @v, N'SCHEMA', N'dbo', N'TABLE', N'Tmp_people', N'COLUMN', N'Ethnicity'
GO
DECLARE @v sql_variant 
SET @v = N'Number of years of formal education for the person'
EXECUTE sp_addextendedproperty N'MS_Description', @v, N'SCHEMA', N'dbo', N'TABLE', N'Tmp_people', N'COLUMN', N'Education'
GO
DECLARE @v sql_variant 
SET @v = N'Birthdate of the person'
EXECUTE sp_addextendedproperty N'MS_Description', @v, N'SCHEMA', N'dbo', N'TABLE', N'Tmp_people', N'COLUMN', N'Birthdate'
GO
DECLARE @v sql_variant 
SET @v = N'Fair Issac''s Company score for this person'
EXECUTE sp_addextendedproperty N'MS_Description', @v, N'SCHEMA', N'dbo', N'TABLE', N'Tmp_people', N'COLUMN', N'FICO_Score'
GO
DECLARE @v sql_variant 
SET @v = N'Date when the FICO score was last updated'
EXECUTE sp_addextendedproperty N'MS_Description', @v, N'SCHEMA', N'dbo', N'TABLE', N'Tmp_people', N'COLUMN', N'FICO_pull_date'
GO
DECLARE @v sql_variant 
SET @v = N'Occupation gross income amount'
EXECUTE sp_addextendedproperty N'MS_Description', @v, N'SCHEMA', N'dbo', N'TABLE', N'Tmp_people', N'COLUMN', N'gross_income'
GO
DECLARE @v sql_variant 
SET @v = N'Occupation net income amount'
EXECUTE sp_addextendedproperty N'MS_Description', @v, N'SCHEMA', N'dbo', N'TABLE', N'Tmp_people', N'COLUMN', N'net_income'
GO
DECLARE @v sql_variant 
SET @v = N'Starting date of employment'
EXECUTE sp_addextendedproperty N'MS_Description', @v, N'SCHEMA', N'dbo', N'TABLE', N'Tmp_people', N'COLUMN', N'emp_start_date'
GO
DECLARE @v sql_variant 
SET @v = N'Ending date of employment'
EXECUTE sp_addextendedproperty N'MS_Description', @v, N'SCHEMA', N'dbo', N'TABLE', N'Tmp_people', N'COLUMN', N'emp_end_date'
GO
DECLARE @v sql_variant 
SET @v = N'Pointer to the employer for the person'
EXECUTE sp_addextendedproperty N'MS_Description', @v, N'SCHEMA', N'dbo', N'TABLE', N'Tmp_people', N'COLUMN', N'employer'
GO
DECLARE @v sql_variant 
SET @v = N'Pointer to the system defined job'
EXECUTE sp_addextendedproperty N'MS_Description', @v, N'SCHEMA', N'dbo', N'TABLE', N'Tmp_people', N'COLUMN', N'job'
GO
DECLARE @v sql_variant 
SET @v = N'If the job is not listed then this is the text description'
EXECUTE sp_addextendedproperty N'MS_Description', @v, N'SCHEMA', N'dbo', N'TABLE', N'Tmp_people', N'COLUMN', N'other_job'
GO
DECLARE @v sql_variant 
SET @v = N'Bankruptcy Certificate Number'
EXECUTE sp_addextendedproperty N'MS_Description', @v, N'SCHEMA', N'dbo', N'TABLE', N'Tmp_people', N'COLUMN', N'bkCertificateNo'
GO
DECLARE @v sql_variant 
SET @v = N'Date bankruptcy certificate issued'
EXECUTE sp_addextendedproperty N'MS_Description', @v, N'SCHEMA', N'dbo', N'TABLE', N'Tmp_people', N'COLUMN', N'bkCertificateIssued'
GO
DECLARE @v sql_variant 
SET @v = N'Date bankruptcy certificate expires'
EXECUTE sp_addextendedproperty N'MS_Description', @v, N'SCHEMA', N'dbo', N'TABLE', N'Tmp_people', N'COLUMN', N'bkCertificateExpires'
GO
DECLARE @v sql_variant 
SET @v = N'Pointer to the bankruptcy district where the action was filed'
EXECUTE sp_addextendedproperty N'MS_Description', @v, N'SCHEMA', N'dbo', N'TABLE', N'Tmp_people', N'COLUMN', N'bkDistrict'
GO
DECLARE @v sql_variant 
SET @v = N'Name of the person who created the row'
EXECUTE sp_addextendedproperty N'MS_Description', @v, N'SCHEMA', N'dbo', N'TABLE', N'Tmp_people', N'COLUMN', N'created_by'
GO
DECLARE @v sql_variant 
SET @v = N'Date and time when the row was created'
EXECUTE sp_addextendedproperty N'MS_Description', @v, N'SCHEMA', N'dbo', N'TABLE', N'Tmp_people', N'COLUMN', N'date_created'
GO
DECLARE @v sql_variant 
SET @v = N'Date of the power-of-attorney document. NULL if none is known.'
EXECUTE sp_addextendedproperty N'MS_Description', @v, N'SCHEMA', N'dbo', N'TABLE', N'Tmp_people', N'COLUMN', N'poa'
GO
ALTER TABLE dbo.Tmp_people ADD CONSTRAINT
	DF_people_relation DEFAULT ([dbo].[Default_Relation]()) FOR Relation
GO
ALTER TABLE dbo.Tmp_people ADD CONSTRAINT
	DF_people_Gender DEFAULT ([dbo].[Default_Gender]()) FOR Gender
GO
ALTER TABLE dbo.Tmp_people ADD CONSTRAINT
	DF_people_Race DEFAULT ([dbo].[Default_Race]()) FOR Race
GO
ALTER TABLE dbo.Tmp_people ADD CONSTRAINT
	DF_people_Ethnicity DEFAULT ([dbo].[default_Ethnicity]()) FOR Ethnicity
GO
EXECUTE sp_bindefault N'dbo.default_false', N'dbo.Tmp_people.Disabled'
GO
EXECUTE sp_bindefault N'dbo.default_zero', N'dbo.Tmp_people.MilitaryStatusID'
GO
EXECUTE sp_bindefault N'dbo.default_zero', N'dbo.Tmp_people.MilitaryServiceID'
GO
EXECUTE sp_bindefault N'dbo.default_zero', N'dbo.Tmp_people.MilitaryGradeID'
GO
EXECUTE sp_bindefault N'dbo.default_zero', N'dbo.Tmp_people.MilitaryDependentID'
GO
ALTER TABLE dbo.Tmp_people ADD CONSTRAINT
	DF_people_Education DEFAULT ([dbo].[Default_Education]()) FOR Education
GO
ALTER TABLE dbo.Tmp_people ADD CONSTRAINT
	DF_people_CreditAgency DEFAULT ('EQUIFAX') FOR CreditAgency
GO
ALTER TABLE dbo.Tmp_people ADD CONSTRAINT
	DF_people_no_fico_score_reason DEFAULT ([dbo].[Default_Housing_FICONotIncludedReason]()) FOR no_fico_score_reason
GO
EXECUTE sp_bindefault N'dbo.default_money', N'dbo.Tmp_people.gross_income'
GO
EXECUTE sp_bindefault N'dbo.default_money', N'dbo.Tmp_people.net_income'
GO
ALTER TABLE dbo.Tmp_people ADD CONSTRAINT
	DF_people_Frequency DEFAULT ([dbo].[default_PayFrequency]()) FOR Frequency
GO
EXECUTE sp_bindefault N'dbo.default_zero', N'dbo.Tmp_people.bkPaymentCurrent'
GO
SET IDENTITY_INSERT dbo.Tmp_people ON
GO
EXECUTE sp_unbindrule N'dbo.Tmp_people.SSN'
GO
IF EXISTS(SELECT * FROM dbo.people)
	 EXEC('INSERT INTO dbo.Tmp_people (Person, Client, Relation, NameID, WorkTelephoneID, CellTelephoneID, EmailID, Former, SSN, Gender, Race, Ethnicity, Disabled, MilitaryStatusID, MilitaryServiceID, MilitaryGradeID, MilitaryDependentID, Education, Birthdate, FICO_Score, CreditAgency, no_fico_score_reason, FICO_pull_date, gross_income, net_income, Frequency, emp_start_date, emp_end_date, employer, job, other_job, bkfileddate, bkdischarge, bkchapter, bkPaymentCurrent, bkAuthLetterDate, bkCertificateNo, bkCertificateIssued, bkCertificateExpires, bkDistrict, SPOC_ID, created_by, date_created, POA_Name, poa)
		SELECT Person, Client, Relation, NameID, WorkTelephoneID, CellTelephoneID, EmailID, Former, SSN, Gender, Race, Ethnicity, Disabled, MilitaryStatusID, MilitaryServiceID, MilitaryGradeID, MilitaryDependentID, Education, Birthdate, FICO_Score, CreditAgency, no_fico_score_reason, FICO_pull_date, gross_income, net_income, Frequency, emp_start_date, emp_end_date, employer, job, other_job, bkfileddate, bkdischarge, bkchapter, bkPaymentCurrent, bkAuthLetterDate, bkCertificateNo, bkCertificateIssued, bkCertificateExpires, bkDistrict, SPOC_ID, created_by, date_created, POA_Name, poa FROM dbo.people WITH (HOLDLOCK TABLOCKX)')
GO
SET IDENTITY_INSERT dbo.Tmp_people OFF
GO
DROP TABLE dbo.people
GO
EXECUTE sp_rename N'dbo.Tmp_people', N'people', 'OBJECT' 
GO
ALTER TABLE dbo.people ADD CONSTRAINT
	PK_people PRIMARY KEY CLUSTERED 
	(
	Person
	) WITH( STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]

GO
CREATE NONCLUSTERED INDEX IX_people_1 ON dbo.people
	(
	Client,
	Relation
	) INCLUDE (NameID, Former, SSN) 
 WITH( STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX IX_people_2 ON dbo.people
	(
	CellTelephoneID
	) INCLUDE (Client) 
 WITH( STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX IX_people_3 ON dbo.people
	(
	WorkTelephoneID
	) INCLUDE (Client) 
 WITH( STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX IX_people_4 ON dbo.people
	(
	Former
	) WITH( STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX IX_people_5 ON dbo.people
	(
	SSN
	) WITH( STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX IX_People_Email ON dbo.people
	(
	EmailID
	) INCLUDE (Client) 
 WITH( STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX ix_people_Relation_includes ON dbo.people
	(
	Relation
	) INCLUDE (Client, NameID, CellTelephoneID, SSN) 
 WITH( PAD_INDEX = OFF, FILLFACTOR = 100, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX ix_people_Relation_NameID_includes ON dbo.people
	(
	Relation,
	NameID
	) INCLUDE (Client, SSN) 
 WITH( PAD_INDEX = OFF, FILLFACTOR = 100, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
ALTER TABLE dbo.people ADD CONSTRAINT
	FK_people_Housing_FICONotIncludedReasons FOREIGN KEY
	(
	no_fico_score_reason
	) REFERENCES dbo.Housing_FICONotIncludedReasons
	(
	oID
	) ON UPDATE  NO ACTION 
	 ON DELETE  NO ACTION 
GO
ALTER TABLE dbo.people ADD CONSTRAINT
	FK_people_bankruptcy_districts FOREIGN KEY
	(
	bkDistrict
	) REFERENCES dbo.bankruptcy_districts
	(
	bankruptcy_district
	) ON UPDATE  NO ACTION 
	 ON DELETE  SET NULL 
GO
ALTER TABLE dbo.people ADD CONSTRAINT
	FK_people_payfrequencytypes FOREIGN KEY
	(
	Frequency
	) REFERENCES dbo.PayFrequencyTypes
	(
	oID
	) ON UPDATE  NO ACTION 
	 ON DELETE  NO ACTION 
GO
ALTER TABLE dbo.people ADD CONSTRAINT
	FK_people_RaceTypes FOREIGN KEY
	(
	Race
	) REFERENCES dbo.RaceTypes
	(
	oID
	) ON UPDATE  NO ACTION 
	 ON DELETE  NO ACTION 
GO
ALTER TABLE dbo.people ADD CONSTRAINT
	FK_people_GenderTypes FOREIGN KEY
	(
	Gender
	) REFERENCES dbo.GenderTypes
	(
	oID
	) ON UPDATE  NO ACTION 
	 ON DELETE  NO ACTION 
GO
ALTER TABLE dbo.people ADD CONSTRAINT
	FK_people_EducationTypes FOREIGN KEY
	(
	Education
	) REFERENCES dbo.EducationTypes
	(
	oID
	) ON UPDATE  NO ACTION 
	 ON DELETE  NO ACTION 
GO
EXECUTE sp_bindrule N'dbo.valid_ssn', N'dbo.people.SSN'
GO
CREATE TRIGGER   [TRIG_people_D] on dbo.people AFTER DELETE as
-- ================================================================================
-- ==         Remove the table references when a person is removed               ==
-- ================================================================================

-- ChangeLog
--    1/1/2011
--       Initial version

set nocount on

-- Remove the contact name
delete		Names
from		Names n
inner join	deleted d on n.Name = d.NameID

-- Remove the cell telephone reference
delete		TelephoneNumbers
from		TelephoneNumbers t
inner join	deleted d on t.TelephoneNumber = d.CellTelephoneID

-- Remove the work telephone reference
delete		TelephoneNumbers
from		TelephoneNumbers t
inner join	deleted d on t.TelephoneNumber = d.WorkTelephoneID

-- Remove the email reference
delete		EmailAddresses
from		EmailAddresses e
inner join	deleted d on e.Email = d.EmailID

-- Remove references to the housing borrower id information
-- We just want to set the PersonID = 0 for any references to the deleted people.
update		housing_borrowers set PersonID = 0
from		deleted d
inner join	housing_properties prop on d.client = prop.HousingID
inner join	housing_borrowers b on prop.ownerID = b.oID
where		(case d.relation when 1 then 1 else 2 end) = b.PersonID

update		housing_borrowers set PersonID = 0
from		deleted d
inner join	housing_properties prop on d.client = prop.HousingID
inner join	housing_borrowers b on prop.coownerID = b.oID
where		(case d.relation when 1 then 1 else 2 end) = b.PersonID

-- Remove references in the debts table
update		client_creditor
set			Person = null
from		client_creditor cc
inner join deleted d on cc.client = d.client  -- use an indexed field to make things faster.
where		cc.Person is not null
and			cc.Person = d.Person;

-- Remove the references to the people_updates table
delete		people_updates
from		people_updates u
inner join	deleted d on d.Person = u.Person
GO
CREATE TRIGGER [dbo].[TRIG_people_FICO_U] ON dbo.people AFTER UPDATE AS 
begin
	-- =================================================================================
	-- ==          Track changes to the FICO score                                    ==
	-- =================================================================================

	-- Supress intermediate results
	set	nocount on

	-- Look for changes to the FICO score
	if update(FICO_Score)
		insert into people_updates ( person, field, old_value, new_value )
		select	d.person, 'FICO_SCORE', convert(varchar, isnull(d.FICO_Score,0)), convert(varchar, isnull(i.FICO_Score,0))
		from	deleted d
		inner join inserted i on d.Person = i.Person
		where	isnull(i.FICO_Score,0) <> 0
		and		isnull(d.FICO_Score,0) <> 0
		and		isnull(d.FICO_Score,0) <> isnull(i.FICO_Score,0)
end
GO
DENY DELETE ON dbo.people TO www_role  AS dbo 
GO
DENY INSERT ON dbo.people TO www_role  AS dbo 
GO
DENY SELECT ON dbo.people TO www_role  AS dbo 
GO
DENY UPDATE ON dbo.people TO www_role  AS dbo 
GO
COMMIT
BEGIN TRANSACTION
GO
ALTER TABLE dbo.PeopleFICOScoreReasons ADD CONSTRAINT
	FK_PeopleFICOScoreReasons_people FOREIGN KEY
	(
	PersonID
	) REFERENCES dbo.people
	(
	Person
	) ON UPDATE  CASCADE 
	 ON DELETE  CASCADE 
GO
ALTER TABLE dbo.PeopleFICOScoreReasons SET (LOCK_ESCALATION = TABLE)
GO
COMMIT
