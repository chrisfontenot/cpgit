
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING OFF
GO
CREATE TABLE [dbo].[OCS_Program_RPCResultCodes](
	[Id] int NOT NULL identity,
  [ProgramID] int NOT NULL,
  [ResultCode] int NOT NULL,
 CONSTRAINT [PK_OCS_Program_RPCResultCodes] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF

-- FannieMae - PostMod
insert into OCS_Program_RPCResultCodes(ProgramID, ResultCode) 
values (6, 9) -- NI - CL Not Interested
insert into OCS_Program_RPCResultCodes(ProgramID, ResultCode) 
values (6, 4) -- CO - Counsel
insert into OCS_Program_RPCResultCodes(ProgramID, ResultCode) 
values (6, 14) -- IM - Immediate Counseling
insert into OCS_Program_RPCResultCodes(ProgramID, ResultCode) 
values (6, 15) -- RP - Spoke to client
insert into OCS_Program_RPCResultCodes(ProgramID, ResultCode) 
values (6, 10) -- SA - Scheduled Appointment
insert into OCS_Program_RPCResultCodes(ProgramID, ResultCode) 
values (6, 17) -- ST - Second Counsel
insert into OCS_Program_RPCResultCodes(ProgramID, ResultCode) 
values (6, 12) -- TR-Transferred call to Counselor

-- FreddieMac - PostMod
insert into OCS_Program_RPCResultCodes(ProgramID, ResultCode) 
values (4, 9) -- NI - CL Not Interested
insert into OCS_Program_RPCResultCodes(ProgramID, ResultCode) 
values (4, 4) -- CO - Counsel
insert into OCS_Program_RPCResultCodes(ProgramID, ResultCode) 
values (4, 14) -- IM - Immediate Counseling
insert into OCS_Program_RPCResultCodes(ProgramID, ResultCode) 
values (4, 15) -- RP - Spoke to client
insert into OCS_Program_RPCResultCodes(ProgramID, ResultCode) 
values (4, 10) -- SA - Scheduled Appointment
insert into OCS_Program_RPCResultCodes(ProgramID, ResultCode) 
values (4, 17) -- ST - Second Counsel
insert into OCS_Program_RPCResultCodes(ProgramID, ResultCode) 
values (4, 12) -- TR-Transferred call to Counselor

-- Nationstar-SD1308
insert into OCS_Program_RPCResultCodes(ProgramID, ResultCode) 
values (5, 9) -- NI - CL Not Interested
insert into OCS_Program_RPCResultCodes(ProgramID, ResultCode) 
values (5, 4) -- CO - Counsel
insert into OCS_Program_RPCResultCodes(ProgramID, ResultCode) 
values (5, 14) -- IM - Immediate Counseling
insert into OCS_Program_RPCResultCodes(ProgramID, ResultCode) 
values (5, 15) -- RP - Spoke to client
insert into OCS_Program_RPCResultCodes(ProgramID, ResultCode) 
values (5, 10) -- SA - Scheduled Appointment
insert into OCS_Program_RPCResultCodes(ProgramID, ResultCode) 
values (5, 17) -- ST - Second Counsel
insert into OCS_Program_RPCResultCodes(ProgramID, ResultCode) 
values (5, 18) -- EX - Exclusion Report
insert into OCS_Program_RPCResultCodes(ProgramID, ResultCode) 
values (5, 12) -- TR-Transferred call to Counselor


-- OneWest-SD1308
insert into OCS_Program_RPCResultCodes(ProgramID, ResultCode) 
values (15, 9) -- NI - CL Not Interested
insert into OCS_Program_RPCResultCodes(ProgramID, ResultCode) 
values (15, 4) -- CO - Counsel
insert into OCS_Program_RPCResultCodes(ProgramID, ResultCode) 
values (15, 14) -- IM - Immediate Counseling
insert into OCS_Program_RPCResultCodes(ProgramID, ResultCode) 
values (15, 15) -- RP - Spoke to client
insert into OCS_Program_RPCResultCodes(ProgramID, ResultCode) 
values (15, 10) -- SA - Scheduled Appointment
insert into OCS_Program_RPCResultCodes(ProgramID, ResultCode) 
values (15, 17) -- ST - Second Counsel
insert into OCS_Program_RPCResultCodes(ProgramID, ResultCode) 
values (15, 18) -- EX - Exclusion Report
insert into OCS_Program_RPCResultCodes(ProgramID, ResultCode) 
values (15, 12) -- TR-Transferred call to Counselor


-- USBank-SD1308
insert into OCS_Program_RPCResultCodes(ProgramID, ResultCode) 
values (14, 9) -- NI - CL Not Interested
insert into OCS_Program_RPCResultCodes(ProgramID, ResultCode) 
values (14, 4) -- CO - Counsel
insert into OCS_Program_RPCResultCodes(ProgramID, ResultCode) 
values (14, 14) -- IM - Immediate Counseling
insert into OCS_Program_RPCResultCodes(ProgramID, ResultCode) 
values (14, 15) -- RP - Spoke to client
insert into OCS_Program_RPCResultCodes(ProgramID, ResultCode) 
values (14, 10) -- SA - Scheduled Appointment
insert into OCS_Program_RPCResultCodes(ProgramID, ResultCode) 
values (14, 17) -- ST - Second Counsel
insert into OCS_Program_RPCResultCodes(ProgramID, ResultCode) 
values (14, 18) -- EX - Exclusion Report
insert into OCS_Program_RPCResultCodes(ProgramID, ResultCode) 
values (14, 12) -- TR-Transferred call to Counselor
