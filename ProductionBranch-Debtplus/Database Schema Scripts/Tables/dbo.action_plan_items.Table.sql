USE [DebtPlus]
GO
/****** Object:  Table [dbo].[action_plan_items]    Script Date: 09/15/2014 13:13:25 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[action_plan_items](
	[action_plan_item] [dbo].[typ_key] IDENTITY(1,1) NOT NULL,
	[item_group_name] [dbo].[typ_description] NOT NULL,
	[item_group] [int] NOT NULL,
	[item_sub_group] [int] NULL,
	[description] [dbo].[typ_subject] NOT NULL,
	[date_created] [dbo].[typ_date] NOT NULL,
	[created_by] [dbo].[typ_counselor] NOT NULL,
 CONSTRAINT [PK_action_plan_item] PRIMARY KEY CLUSTERED 
(
	[action_plan_item] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 90) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
ALTER TABLE [dbo].[action_plan_items] ADD  CONSTRAINT [DF_action_plan_item_group]  DEFAULT ((1)) FOR [item_group]
GO
