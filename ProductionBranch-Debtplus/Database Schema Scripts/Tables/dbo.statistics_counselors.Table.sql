USE [DebtPlus]
GO
/****** Object:  Table [dbo].[statistics_counselors]    Script Date: 09/15/2014 13:13:26 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[statistics_counselors](
	[statistic_counselor] [int] IDENTITY(1,1) NOT NULL,
	[period_start] [datetime] NOT NULL,
	[office] [int] NOT NULL,
	[counselor] [int] NOT NULL,
	[active_clients] [int] NOT NULL,
	[dropped_clients] [int] NOT NULL,
	[clients_disbursed] [int] NOT NULL,
	[expected_disbursement] [money] NOT NULL,
	[actual_disbursement] [money] NOT NULL,
	[paf_fees] [money] NOT NULL,
	[new_dmp_clients] [int] NOT NULL,
	[new_dmp_debt] [money] NOT NULL,
	[date_created] [datetime] NOT NULL,
	[created_by] [varchar](80) NOT NULL,
 CONSTRAINT [PK_statistics_counselors] PRIMARY KEY CLUSTERED 
(
	[statistic_counselor] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 90) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
