USE [DebtPlus]
GO
/****** Object:  Table [dbo].[rpps_files]    Script Date: 09/15/2014 13:13:26 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[rpps_files](
	[rpps_file] [dbo].[typ_key] IDENTITY(1,1) NOT NULL,
	[file_type] [dbo].[typ_rpps_file_type] NOT NULL,
	[bank] [dbo].[typ_key] NULL,
	[filename] [varchar](50) NULL,
	[date_created] [datetime] NOT NULL,
	[created_by] [dbo].[typ_counselor] NOT NULL,
 CONSTRAINT [PK_rps_files] PRIMARY KEY CLUSTERED 
(
	[rpps_file] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 90) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The file identifier. This is the primary key to this table and is used in the rpps_batches table.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'rpps_files', @level2type=N'COLUMN',@level2name=N'rpps_file'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Type of transactions. EFT or EDI.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'rpps_files', @level2type=N'COLUMN',@level2name=N'file_type'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Bank number for this file' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'rpps_files', @level2type=N'COLUMN',@level2name=N'bank'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'File name for the output file' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'rpps_files', @level2type=N'COLUMN',@level2name=N'filename'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Date/Time when the row was created.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'rpps_files', @level2type=N'COLUMN',@level2name=N'date_created'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Person who created the row.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'rpps_files', @level2type=N'COLUMN',@level2name=N'created_by'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'This table holds the file information when a RPPS file is generated to be sent to MasterCard' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'rpps_files'
GO
