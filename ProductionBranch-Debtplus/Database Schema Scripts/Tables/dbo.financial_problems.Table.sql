/* To prevent any potential data loss issues, you should review this script in detail before running it outside the context of the database designer.*/
BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
execute UPDATE_ADD_COLUMN 'financial_problems', 'hpf'
BEGIN TRANSACTION
GO
CREATE TABLE dbo.Tmp_financial_problems
	(
	financial_problem dbo.typ_key NOT NULL IDENTITY (1, 1),
	description dbo.typ_description NOT NULL,
	[Default] bit NOT NULL,
	ActiveFlag bit NOT NULL,
	note varchar(255) NULL,
	nfcc varchar(10) NULL,
	rpps_code varchar(10) NULL,
	nfmcp_section varchar(4) NULL,
	RxOffice varchar(20) NULL,
	hpf varchar(50) NULL,
	date_created dbo.typ_date NOT NULL,
	created_by dbo.typ_counselor NOT NULL,
	ts timestamp NOT NULL
	)  ON [PRIMARY]
GO
GRANT DELETE ON dbo.Tmp_financial_problems TO public  AS dbo
GO
GRANT INSERT ON dbo.Tmp_financial_problems TO public  AS dbo
GO
GRANT SELECT ON dbo.Tmp_financial_problems TO public  AS dbo
GO
GRANT UPDATE ON dbo.Tmp_financial_problems TO public  AS dbo
GO
DECLARE @v sql_variant 
SET @v = N'This table lists the causes of financial problems in the system.'
EXECUTE sp_addextendedproperty N'MS_Description', @v, N'SCHEMA', N'dbo', N'TABLE', N'Tmp_financial_problems', NULL, NULL
GO
DECLARE @v sql_variant 
SET @v = N'Primary key to the table.'
EXECUTE sp_addextendedproperty N'MS_Description', @v, N'SCHEMA', N'dbo', N'TABLE', N'Tmp_financial_problems', N'COLUMN', N'financial_problem'
GO
DECLARE @v sql_variant 
SET @v = N'Descriptive cause of the financial problem'
EXECUTE sp_addextendedproperty N'MS_Description', @v, N'SCHEMA', N'dbo', N'TABLE', N'Tmp_financial_problems', N'COLUMN', N'description'
GO
DECLARE @v sql_variant 
SET @v = N'Alert note generated when the financial problem is selected'
EXECUTE sp_addextendedproperty N'MS_Description', @v, N'SCHEMA', N'dbo', N'TABLE', N'Tmp_financial_problems', N'COLUMN', N'note'
GO
DECLARE @v sql_variant 
SET @v = N'Value reported to the NFCC for the financial problem'
EXECUTE sp_addextendedproperty N'MS_Description', @v, N'SCHEMA', N'dbo', N'TABLE', N'Tmp_financial_problems', N'COLUMN', N'nfcc'
GO
DECLARE @v sql_variant 
SET @v = N'Value reported to the RPPS proposals for the financial problem cause'
EXECUTE sp_addextendedproperty N'MS_Description', @v, N'SCHEMA', N'dbo', N'TABLE', N'Tmp_financial_problems', N'COLUMN', N'rpps_code'
GO
DECLARE @v sql_variant 
SET @v = N'Date/Time when the row was created.'
EXECUTE sp_addextendedproperty N'MS_Description', @v, N'SCHEMA', N'dbo', N'TABLE', N'Tmp_financial_problems', N'COLUMN', N'date_created'
GO
DECLARE @v sql_variant 
SET @v = N'Person who created the row.'
EXECUTE sp_addextendedproperty N'MS_Description', @v, N'SCHEMA', N'dbo', N'TABLE', N'Tmp_financial_problems', N'COLUMN', N'created_by'
GO
EXECUTE sp_bindefault N'dbo.default_false', N'dbo.Tmp_financial_problems.[Default]'
GO
EXECUTE sp_bindefault N'dbo.default_true', N'dbo.Tmp_financial_problems.ActiveFlag'
GO
EXECUTE sp_bindefault N'dbo.default_zero', N'dbo.Tmp_financial_problems.nfcc'
GO
SET IDENTITY_INSERT dbo.Tmp_financial_problems ON
GO
IF EXISTS(SELECT * FROM dbo.financial_problems)
	 EXEC('INSERT INTO dbo.Tmp_financial_problems (financial_problem, description, [Default], ActiveFlag, note, nfcc, rpps_code, nfmcp_section, date_created, created_by, RxOffice, hpf)
		SELECT financial_problem, description, [Default], ActiveFlag, note, nfcc, rpps_code, nfmcp_section, date_created, created_by, RxOffice, hpf FROM dbo.financial_problems WITH (HOLDLOCK TABLOCKX)')
GO
SET IDENTITY_INSERT dbo.Tmp_financial_problems OFF
GO
DROP TABLE dbo.financial_problems
GO
EXECUTE sp_rename N'dbo.Tmp_financial_problems', N'financial_problems', 'OBJECT' 
GO
ALTER TABLE dbo.financial_problems ADD CONSTRAINT
	PK_financial_problems PRIMARY KEY CLUSTERED 
	(
	financial_problem
	) WITH( PAD_INDEX = OFF, FILLFACTOR = 90, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]

GO
CREATE TRIGGER trig_financial_problems_D on dbo.financial_problems AFTER DELETE AS
-- ==============================================================================
-- ==        Remove invalid references to the financial problems               ==
-- ==============================================================================
set nocount on

create table #financial_problems ( client int );

insert into #financial_problems ( client )
select	client
from	clients c
inner join deleted d on c.cause_fin_problem1 = d.financial_problem

union

select	client
from	clients c
inner join deleted d on c.cause_fin_problem2 = d.financial_problem

union

select	client
from	clients c
inner join deleted d on c.cause_fin_problem3 = d.financial_problem

union

select	client
from	clients c
inner join deleted d on c.cause_fin_problem4 = d.financial_problem;

-- Remove the references in the clients
update	clients
set		cause_fin_problem1 = null
from	clients c
inner join deleted d on c.cause_fin_problem1 = d.financial_problem;

update	clients
set		cause_fin_problem2 = null
from	clients c
inner join deleted d on c.cause_fin_problem2 = d.financial_problem;

update	clients
set		cause_fin_problem3 = null
from	clients c
inner join deleted d on c.cause_fin_problem3 = d.financial_problem;

update	clients
set		cause_fin_problem4 = null
from	clients c
inner join deleted d on c.cause_fin_problem4 = d.financial_problem;

-- Ensure that the primary is defined. The other can exist or not as needed.
update	clients set cause_fin_problem1 = coalesce(cause_fin_problem2, cause_fin_problem3, cause_fin_problem4)
from	clients c
inner join #cause_fin_problem x on c.client = x.client
where	cause_fin_problem1 is null

-- Remove the duplicate causes if indicated
update	clients set cause_fin_problem2 = null from clients c inner join #financial_problems x on c.client = x.client where c.cause_fin_problem1 = c.cause_fin_problem2;
update	clients set cause_fin_problem3 = null from clients c inner join #financial_problems x on c.client = x.client where c.cause_fin_problem1 = c.cause_fin_problem3;
update	clients set cause_fin_problem4 = null from clients c inner join #financial_problems x on c.client = x.client where c.cause_fin_problem1 = c.cause_fin_problem4;

-- Toss the working table
drop table #cause_fin_problem;
GO
DENY DELETE ON dbo.financial_problems TO www_role  AS dbo 
GO
DENY INSERT ON dbo.financial_problems TO www_role  AS dbo 
GO
DENY SELECT ON dbo.financial_problems TO www_role  AS dbo 
GO
DENY UPDATE ON dbo.financial_problems TO www_role  AS dbo 
GO
COMMIT
GO
