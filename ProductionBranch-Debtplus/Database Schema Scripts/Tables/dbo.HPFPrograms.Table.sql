/* To prevent any potential data loss issues, you should review this script in detail before running it outside the context of the database designer.*/
BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION
GO
CREATE TABLE dbo.Tmp_HPFPrograms
	(
	oID dbo.typ_key NOT NULL IDENTITY (1, 1),
	description dbo.typ_description NOT NULL,
	programID dbo.typ_key NOT NULL,
	sponsorID dbo.typ_key NULL,
	campaignID dbo.typ_key NULL,
	ActiveFlag bit NOT NULL,
	[default] bit NOT NULL,
	ts timestamp NOT NULL,
	hpf varchar(50) NULL
	)  ON [PRIMARY]
GO
GRANT DELETE ON dbo.Tmp_HPFPrograms TO public  AS dbo
GO
GRANT INSERT ON dbo.Tmp_HPFPrograms TO public  AS dbo
GO
GRANT SELECT ON dbo.Tmp_HPFPrograms TO public  AS dbo
GO
GRANT UPDATE ON dbo.Tmp_HPFPrograms TO public  AS dbo
GO
EXECUTE sp_bindefault N'dbo.default_true', N'dbo.Tmp_HPFPrograms.ActiveFlag'
GO
EXECUTE sp_bindefault N'dbo.default_false', N'dbo.Tmp_HPFPrograms.[default]'
GO
SET IDENTITY_INSERT dbo.Tmp_HPFPrograms ON
GO
IF EXISTS(SELECT * FROM sysobjects WHERE name=N'HPFPrograms' AND type='U')
BEGIN
	EXEC('INSERT INTO dbo.Tmp_HPFPrograms (oID, description, programID, sponsorID, campaignID, ActiveFlag, [default], hpf)
		SELECT oID, description, programID, sponsorID, campaignID, ActiveFlag, [default], hpf FROM dbo.HPFPrograms WITH (HOLDLOCK TABLOCKX)')
	EXEC('DROP TABLE dbo.HPFPrograms')
END
GO
SET IDENTITY_INSERT dbo.Tmp_HPFPrograms OFF
GO
EXECUTE sp_rename N'dbo.Tmp_HPFPrograms', N'HPFPrograms', 'OBJECT' 
GO
ALTER TABLE dbo.HPFPrograms ADD CONSTRAINT
	PK_HPFPrograms PRIMARY KEY CLUSTERED 
	(
	oID
	) WITH( STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
DENY DELETE ON dbo.HPFPrograms TO www_role  AS dbo 
GO
DENY INSERT ON dbo.HPFPrograms TO www_role  AS dbo 
GO
DENY SELECT ON dbo.HPFPrograms TO www_role  AS dbo 
GO
DENY UPDATE ON dbo.HPFPrograms TO www_role  AS dbo 
GO
COMMIT
GO
