USE [DebtPlus]
GO
/****** Object:  Table [dbo].[rpps_response_details_other]    Script Date: 09/15/2014 13:13:26 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[rpps_response_details_other](
	[rpps_response_details_other] [dbo].[typ_key] IDENTITY(1,1) NOT NULL,
	[rpps_response_detail] [dbo].[typ_key] NOT NULL,
	[transaction_type] [dbo].[typ_rps_transaction] NOT NULL,
	[detail] [varchar](80) NULL,
	[trace_number] [dbo].[typ_trace_number] NOT NULL,
 CONSTRAINT [PK_rpps_response_details_other] PRIMARY KEY CLUSTERED 
(
	[rpps_response_details_other] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 90) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
