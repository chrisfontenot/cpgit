USE [DebtPlus]
GO
/****** Object:  Table [dbo].[epay_responses_cr]    Script Date: 09/15/2014 13:13:26 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[epay_responses_cr](
	[epay_response_cr] [dbo].[typ_key] IDENTITY(1,1) NOT NULL,
	[epay_response_file] [dbo].[typ_key] NOT NULL,
	[transaction_id] [varchar](27) NOT NULL,
	[epay_transaction] [dbo].[typ_key] NULL,
	[error] [varchar](50) NULL,
	[response_date] [datetime] NULL,
	[agency_name] [varchar](35) NULL,
	[agency_id] [varchar](12) NULL,
	[creditor_id] [varchar](12) NULL,
	[creditor_name] [varchar](35) NULL,
	[client_number] [dbo].[typ_client] NOT NULL,
	[client_name] [varchar](35) NULL,
	[response_originator] [char](1) NOT NULL,
	[customer_biller_account_number] [varchar](32) NULL,
	[current_client_balance] [money] NULL,
	[date_of_balance] [datetime] NULL,
	[date_of_last_payment] [datetime] NULL,
	[date_created] [dbo].[typ_date] NOT NULL,
	[created_by] [dbo].[typ_counselor] NOT NULL,
 CONSTRAINT [PK_epay_responses_cr] PRIMARY KEY CLUSTERED 
(
	[epay_response_cr] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 90) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
