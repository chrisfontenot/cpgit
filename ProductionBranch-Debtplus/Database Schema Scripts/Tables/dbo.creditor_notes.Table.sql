USE [DebtPlus]
GO
/****** Object:  Table [dbo].[creditor_notes]    Script Date: 09/15/2014 13:13:26 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[creditor_notes](
	[creditor_note] [dbo].[typ_key] IDENTITY(1,1) NOT NULL,
	[creditor] [dbo].[typ_creditor] NOT NULL,
	[prior_note] [dbo].[typ_key] NULL,
	[expires] [datetime] NULL,
	[type] [int] NOT NULL,
	[dont_edit] [bit] NOT NULL,
	[dont_delete] [bit] NOT NULL,
	[dont_print] [bit] NOT NULL,
	[note] [text] NULL,
	[subject] [dbo].[typ_subject] NULL,
	[created_by] [dbo].[typ_counselor] NOT NULL,
	[date_created] [dbo].[typ_date] NOT NULL,
	[is_text] [bit] NULL,
	[old_creditor_note] [int] NULL,
	[ts] [timestamp] NOT NULL,
 CONSTRAINT [PK_creditor_notes] PRIMARY KEY CLUSTERED 
(
	[creditor_note] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 90) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Primary key to the table' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'creditor_notes', @level2type=N'COLUMN',@level2name=N'creditor_note'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Creditor ID associated with the note' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'creditor_notes', @level2type=N'COLUMN',@level2name=N'creditor'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'If notes are not to be edited, this is a pointer to the previous version of the note.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'creditor_notes', @level2type=N'COLUMN',@level2name=N'prior_note'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Date / time when the note expires' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'creditor_notes', @level2type=N'COLUMN',@level2name=N'expires'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Type of the note. (1 = perm, 2 = temp, 3 = sys, 5 = research)' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'creditor_notes', @level2type=N'COLUMN',@level2name=N'type'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'If set, do not permit others to edit the note' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'creditor_notes', @level2type=N'COLUMN',@level2name=N'dont_edit'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'If set, do not permit others to delete the note' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'creditor_notes', @level2type=N'COLUMN',@level2name=N'dont_delete'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'If set, do not permit others to print the note' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'creditor_notes', @level2type=N'COLUMN',@level2name=N'dont_print'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Note Text' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'creditor_notes', @level2type=N'COLUMN',@level2name=N'note'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Subject for the note' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'creditor_notes', @level2type=N'COLUMN',@level2name=N'subject'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Person who created the row.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'creditor_notes', @level2type=N'COLUMN',@level2name=N'created_by'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Date/Time when the row was created.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'creditor_notes', @level2type=N'COLUMN',@level2name=N'date_created'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'If set, the note is ASCII text rather than RTF' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'creditor_notes', @level2type=N'COLUMN',@level2name=N'is_text'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'This table holds all of the creditor notes.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'creditor_notes'
GO
