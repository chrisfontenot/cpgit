/* To prevent any potential data loss issues, you should review this script in detail before running it outside the context of the database designer.*/
BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION
GO
EXECUTE UPDATE_ADD_COLUMN 'Housing_service_types', 'hpf'
GO
CREATE TABLE dbo.Tmp_Housing_service_types
	(
	service_type dbo.typ_key NOT NULL,
	description dbo.typ_description NOT NULL,
	[Default] bit NOT NULL,
	ActiveFlag bit NOT NULL,
	hpf varchar(50) NULL,
	date_created dbo.typ_date NOT NULL,
	created_by dbo.typ_counselor NOT NULL,
	ts timestamp NOT NULL
	)  ON [PRIMARY]
GO
GRANT DELETE ON dbo.Tmp_Housing_service_types TO public  AS dbo
GO
GRANT INSERT ON dbo.Tmp_Housing_service_types TO public  AS dbo
GO
GRANT SELECT ON dbo.Tmp_Housing_service_types TO public  AS dbo
GO
GRANT UPDATE ON dbo.Tmp_Housing_service_types TO public  AS dbo
GO
EXECUTE sp_bindefault N'dbo.default_false', N'dbo.Tmp_Housing_service_types.[Default]'
GO
EXECUTE sp_bindefault N'dbo.default_true', N'dbo.Tmp_Housing_service_types.ActiveFlag'
GO
IF EXISTS(SELECT * FROM dbo.Housing_service_types)
	 EXEC('INSERT INTO dbo.Tmp_Housing_service_types (service_type, description, [Default], ActiveFlag, date_created, created_by, hpf)
		SELECT service_type, description, [Default], ActiveFlag, date_created, created_by, hpf FROM dbo.Housing_service_types WITH (HOLDLOCK TABLOCKX)')
GO
DROP TABLE dbo.Housing_service_types
GO
EXECUTE sp_rename N'dbo.Tmp_Housing_service_types', N'Housing_service_types', 'OBJECT' 
GO
ALTER TABLE dbo.Housing_service_types ADD CONSTRAINT
	PK_housing_service_types PRIMARY KEY CLUSTERED 
	(
	service_type
	) WITH( STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]

GO
DENY DELETE ON dbo.Housing_service_types TO www_role  AS dbo 
GO
DENY INSERT ON dbo.Housing_service_types TO www_role  AS dbo 
GO
DENY SELECT ON dbo.Housing_service_types TO www_role  AS dbo 
GO
DENY UPDATE ON dbo.Housing_service_types TO www_role  AS dbo 
GO
COMMIT
