USE [DebtPlus]
GO
/****** Object:  Table [dbo].[rpps_transactions]    Script Date: 09/15/2014 13:13:26 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[rpps_transactions](
	[rpps_transaction] [dbo].[typ_key] IDENTITY(1,1) NOT NULL,
	[bank] [dbo].[typ_key] NULL,
	[client] [dbo].[typ_client] NULL,
	[creditor] [dbo].[typ_creditor] NOT NULL,
	[client_creditor] [dbo].[typ_key] NULL,
	[biller_id] [dbo].[typ_rpps_biller_id] NOT NULL,
	[trace_number_first] [dbo].[typ_trace_number] NULL,
	[trace_number_last] [dbo].[typ_trace_number] NULL,
	[rpps_file] [dbo].[typ_key] NULL,
	[rpps_batch] [dbo].[typ_key] NULL,
	[transaction_code] [dbo].[typ_rps_transaction] NULL,
	[service_class_or_purpose] [varchar](3) NULL,
	[return_code] [dbo].[typ_rps_result] NULL,
	[client_creditor_register] [dbo].[typ_key] NULL,
	[client_creditor_proposal] [dbo].[typ_key] NULL,
	[death_date] [datetime] NOT NULL,
	[old_account_number] [varchar](22) NULL,
	[new_account_number] [varchar](22) NULL,
	[old_balance] [money] NULL,
	[response_batch_id] [dbo].[typ_description] NULL,
	[prenote_status] [int] NULL,
	[debt_note] [dbo].[typ_key] NULL,
	[date_created] [dbo].[typ_date] NOT NULL,
	[created_by] [dbo].[typ_counselor] NOT NULL,
 CONSTRAINT [PK_rpps_transactions] PRIMARY KEY CLUSTERED 
(
	[rpps_transaction] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Primary key to the table. This is just an identity field.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'rpps_transactions', @level2type=N'COLUMN',@level2name=N'rpps_transaction'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Pointer to the banks table for this RPPS transaction.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'rpps_transactions', @level2type=N'COLUMN',@level2name=N'bank'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Client ID associated with the transaction.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'rpps_transactions', @level2type=N'COLUMN',@level2name=N'client'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Creditor ID associated with the transaction.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'rpps_transactions', @level2type=N'COLUMN',@level2name=N'creditor'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'RPPS Biller ID corresponding to the creditor. Built during disbursement processing and later used to build the rpps_batches table.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'rpps_transactions', @level2type=N'COLUMN',@level2name=N'biller_id'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Starting trace number of the transactions in this request. Non-Null when the transaction is complete.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'rpps_transactions', @level2type=N'COLUMN',@level2name=N'trace_number_first'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Ending trace number of the transactions in this request. Same as trace_number_first if there is only one transaction.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'rpps_transactions', @level2type=N'COLUMN',@level2name=N'trace_number_last'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Used for proposal creation. This is the file number for this transaction.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'rpps_transactions', @level2type=N'COLUMN',@level2name=N'rpps_file'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Batch ID associated with the biller in the file.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'rpps_transactions', @level2type=N'COLUMN',@level2name=N'rpps_batch'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Transaction code for the request. For example 22 is a prenote. 27 is a payment.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'rpps_transactions', @level2type=N'COLUMN',@level2name=N'transaction_code'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Service class or purpose of the row. CIE indicates a payment. CDP is a proposal.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'rpps_transactions', @level2type=N'COLUMN',@level2name=N'service_class_or_purpose'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Response code from Mastercard for any error.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'rpps_transactions', @level2type=N'COLUMN',@level2name=N'return_code'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'For a payment, this is the pointer to the payment record in the registers_client_creditor table.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'rpps_transactions', @level2type=N'COLUMN',@level2name=N'client_creditor_register'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'For a proposal, this is the pointer to the proposal record.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'rpps_transactions', @level2type=N'COLUMN',@level2name=N'client_creditor_proposal'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Date that the transaction is to be deleted from the system.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'rpps_transactions', @level2type=N'COLUMN',@level2name=N'death_date'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'for account number changes, this is the old account number' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'rpps_transactions', @level2type=N'COLUMN',@level2name=N'old_account_number'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'for account number changes, this is the new account number' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'rpps_transactions', @level2type=N'COLUMN',@level2name=N'new_account_number'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'If this was a CDV transaction, what was the balance at the time of the transaction?' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'rpps_transactions', @level2type=N'COLUMN',@level2name=N'old_balance'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'When the response is received and processed, this is the name of the response batch. Used to generate the report.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'rpps_transactions', @level2type=N'COLUMN',@level2name=N'response_batch_id'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Pointer to the rpps_messages table for CDN transactions' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'rpps_transactions', @level2type=N'COLUMN',@level2name=N'debt_note'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Date and time when the transaction was created.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'rpps_transactions', @level2type=N'COLUMN',@level2name=N'date_created'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Name of the person who created the transaction.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'rpps_transactions', @level2type=N'COLUMN',@level2name=N'created_by'
GO
ALTER TABLE [dbo].[rpps_transactions] ADD  CONSTRAINT [DF_rpps_transactions_service_class_or_purpose]  DEFAULT ('CIE') FOR [service_class_or_purpose]
GO
ALTER TABLE [dbo].[rpps_transactions] ADD  CONSTRAINT [DF_rpps_transactions_death_date]  DEFAULT (dateadd(day,(180),getdate())) FOR [death_date]
GO
