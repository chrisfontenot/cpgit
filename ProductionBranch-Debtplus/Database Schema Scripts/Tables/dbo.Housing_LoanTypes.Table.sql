/* To prevent any potential data loss issues, you should review this script in detail before running it outside the context of the database designer.*/
BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
execute UPDATE_ADD_COLUMN 'Housing_LoanTypes', 'hpf'
BEGIN TRANSACTION
GO
CREATE TABLE dbo.Tmp_Housing_LoanTypes
	(
	oID dbo.typ_key NOT NULL IDENTITY (1, 1),
	description dbo.typ_description NOT NULL,
	morgageType dbo.typ_description NOT NULL,
	rateType dbo.typ_description NOT NULL,
	modification bit NOT NULL,
	hud_9902_section varchar(256) NULL,
	nfmcp_section varchar(4) NULL,
	[default] bit NOT NULL,
	ActiveFlag bit NOT NULL,
	ts timestamp NOT NULL,
	nfcc varchar(4) NULL,
	rpps varchar(4) NULL,
	note varchar(256) NULL,
	hpf varchar(50) NULL,
	date_created dbo.typ_date NOT NULL,
	created_by dbo.typ_counselor NOT NULL,
	epay varchar(4) NULL
	)  ON [PRIMARY]
GO
GRANT DELETE ON dbo.Tmp_Housing_LoanTypes TO public  AS dbo
GO
GRANT INSERT ON dbo.Tmp_Housing_LoanTypes TO public  AS dbo
GO
GRANT SELECT ON dbo.Tmp_Housing_LoanTypes TO public  AS dbo
GO
GRANT UPDATE ON dbo.Tmp_Housing_LoanTypes TO public  AS dbo
GO
EXECUTE sp_bindefault N'dbo.default_false', N'dbo.Tmp_Housing_LoanTypes.modification'
GO
EXECUTE sp_bindefault N'dbo.default_false', N'dbo.Tmp_Housing_LoanTypes.[default]'
GO
EXECUTE sp_bindefault N'dbo.default_true', N'dbo.Tmp_Housing_LoanTypes.ActiveFlag'
GO
SET IDENTITY_INSERT dbo.Tmp_Housing_LoanTypes ON
GO
IF EXISTS(SELECT * FROM dbo.Housing_LoanTypes)
	 EXEC('INSERT INTO dbo.Tmp_Housing_LoanTypes (oID, description, morgageType, rateType, modification, hud_9902_section, nfmcp_section, [default], ActiveFlag, date_created, created_by, nfcc, rpps, epay, note, hpf)
		SELECT oID, CONVERT(varchar(50), description), morgageType, rateType, modification, hud_9902_section, nfmcp_section, [default], ActiveFlag, date_created, created_by, nfcc, rpps, epay, note, hpf FROM dbo.Housing_LoanTypes WITH (HOLDLOCK TABLOCKX)')
GO
SET IDENTITY_INSERT dbo.Tmp_Housing_LoanTypes OFF
GO
DROP TABLE dbo.Housing_LoanTypes
GO
EXECUTE sp_rename N'dbo.Tmp_Housing_LoanTypes', N'Housing_LoanTypes', 'OBJECT' 
GO
ALTER TABLE dbo.Housing_LoanTypes ADD CONSTRAINT
	PK_Housing_LoanTypes_1 PRIMARY KEY CLUSTERED 
	(
	oID
	) WITH( STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]

GO
DENY DELETE ON dbo.Housing_LoanTypes TO www_role  AS dbo 
GO
DENY INSERT ON dbo.Housing_LoanTypes TO www_role  AS dbo 
GO
DENY SELECT ON dbo.Housing_LoanTypes TO www_role  AS dbo 
GO
DENY UPDATE ON dbo.Housing_LoanTypes TO www_role  AS dbo 
GO
COMMIT
