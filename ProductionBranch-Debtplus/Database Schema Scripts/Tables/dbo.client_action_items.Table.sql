USE [DebtPlus]
GO
/****** Object:  Table [dbo].[client_action_items]    Script Date: 09/15/2014 13:13:25 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[client_action_items](
	[client_action_item] [dbo].[typ_key] IDENTITY(1,1) NOT NULL,
	[action_plan] [dbo].[typ_key] NOT NULL,
	[action_item] [dbo].[typ_key] NOT NULL,
	[checked] [bit] NOT NULL,
	[date_created] [dbo].[typ_date] NOT NULL,
	[created_by] [dbo].[typ_counselor] NOT NULL,
 CONSTRAINT [PK_client_action_items] PRIMARY KEY CLUSTERED 
(
	[client_action_item] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 90) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'ID for the row' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'client_action_items', @level2type=N'COLUMN',@level2name=N'client_action_item'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Action plan associated with the item' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'client_action_items', @level2type=N'COLUMN',@level2name=N'action_plan'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Action Item associated with the specific action plan' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'client_action_items', @level2type=N'COLUMN',@level2name=N'action_item'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Is the value to be "checked"?' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'client_action_items', @level2type=N'COLUMN',@level2name=N'checked'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Date/Time when the row was created.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'client_action_items', @level2type=N'COLUMN',@level2name=N'date_created'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Person who created the row.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'client_action_items', @level2type=N'COLUMN',@level2name=N'created_by'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'This table associates the client, action plan, and which items are checked for the specific action plan.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'client_action_items'
GO
