EXECUTE UPDATE_DROP_CONSTRAINTS 'client_product_notes'
GO
/* To prevent any potential data loss issues, you should review this script in detail before running it outside the context of the database designer.*/
BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION
GO
ALTER TABLE dbo.client_products SET (LOCK_ESCALATION = TABLE)
GO
CREATE TABLE dbo.Tmp_client_product_notes
	(
	oID dbo.typ_key NOT NULL IDENTITY (1, 1),
	client_product dbo.typ_key NOT NULL,
	type int NOT NULL,
	dont_edit bit NOT NULL,
	dont_delete bit NOT NULL,
	dont_print bit NOT NULL,
	subject dbo.typ_subject NULL,
	note text NULL,
	date_created datetime NOT NULL,
	created_by dbo.typ_counselor NOT NULL,
	ts timestamp NOT NULL
	)  ON [PRIMARY]
	 TEXTIMAGE_ON [PRIMARY]
GO
ALTER TABLE dbo.Tmp_client_product_notes SET (LOCK_ESCALATION = TABLE)
GO
GRANT DELETE ON dbo.Tmp_client_product_notes TO public  AS dbo
GO
GRANT INSERT ON dbo.Tmp_client_product_notes TO public  AS dbo
GO
GRANT SELECT ON dbo.Tmp_client_product_notes TO public  AS dbo
GO
GRANT UPDATE ON dbo.Tmp_client_product_notes TO public  AS dbo
GO
DECLARE @v sql_variant 
SET @v = N'This table holds all of the client_product notes.'
EXECUTE sp_addextendedproperty N'MS_Description', @v, N'SCHEMA', N'dbo', N'TABLE', N'Tmp_client_product_notes', NULL, NULL
GO
DECLARE @v sql_variant 
SET @v = N'Primary key. Sequential number for the note.'
EXECUTE sp_addextendedproperty N'MS_Description', @v, N'SCHEMA', N'dbo', N'TABLE', N'Tmp_client_product_notes', N'COLUMN', N'oID'
GO
DECLARE @v sql_variant 
SET @v = N'client_product for whom the message applies'
EXECUTE sp_addextendedproperty N'MS_Description', @v, N'SCHEMA', N'dbo', N'TABLE', N'Tmp_client_product_notes', N'COLUMN', N'client_product'
GO
DECLARE @v sql_variant 
SET @v = N'Type of the note.'
EXECUTE sp_addextendedproperty N'MS_Description', @v, N'SCHEMA', N'dbo', N'TABLE', N'Tmp_client_product_notes', N'COLUMN', N'type'
GO
DECLARE @v sql_variant 
SET @v = N'If set, do not allow others to change the note.'
EXECUTE sp_addextendedproperty N'MS_Description', @v, N'SCHEMA', N'dbo', N'TABLE', N'Tmp_client_product_notes', N'COLUMN', N'dont_edit'
GO
DECLARE @v sql_variant 
SET @v = N'If set, do not allow others to delete this note.'
EXECUTE sp_addextendedproperty N'MS_Description', @v, N'SCHEMA', N'dbo', N'TABLE', N'Tmp_client_product_notes', N'COLUMN', N'dont_delete'
GO
DECLARE @v sql_variant 
SET @v = N'If set, do not allow others to print the note.'
EXECUTE sp_addextendedproperty N'MS_Description', @v, N'SCHEMA', N'dbo', N'TABLE', N'Tmp_client_product_notes', N'COLUMN', N'dont_print'
GO
DECLARE @v sql_variant 
SET @v = N'Subject for the note.'
EXECUTE sp_addextendedproperty N'MS_Description', @v, N'SCHEMA', N'dbo', N'TABLE', N'Tmp_client_product_notes', N'COLUMN', N'subject'
GO
DECLARE @v sql_variant 
SET @v = N'Text for the note.'
EXECUTE sp_addextendedproperty N'MS_Description', @v, N'SCHEMA', N'dbo', N'TABLE', N'Tmp_client_product_notes', N'COLUMN', N'note'
GO
DECLARE @v sql_variant 
SET @v = N'Date/Time when the row was created.'
EXECUTE sp_addextendedproperty N'MS_Description', @v, N'SCHEMA', N'dbo', N'TABLE', N'Tmp_client_product_notes', N'COLUMN', N'date_created'
GO
DECLARE @v sql_variant 
SET @v = N'Person who created the row.'
EXECUTE sp_addextendedproperty N'MS_Description', @v, N'SCHEMA', N'dbo', N'TABLE', N'Tmp_client_product_notes', N'COLUMN', N'created_by'
GO
EXECUTE sp_bindefault N'dbo.default_note_type', N'dbo.Tmp_client_product_notes.type'
GO
EXECUTE sp_bindefault N'dbo.default_false', N'dbo.Tmp_client_product_notes.dont_edit'
GO
EXECUTE sp_bindefault N'dbo.default_false', N'dbo.Tmp_client_product_notes.dont_delete'
GO
EXECUTE sp_bindefault N'dbo.default_false', N'dbo.Tmp_client_product_notes.dont_print'
GO
EXECUTE sp_bindefault N'dbo.default_date', N'dbo.Tmp_client_product_notes.date_created'
GO
IF EXISTS(SELECT * FROM SYSOBJECTS WHERE TYPE='U' AND NAME=N'client_product_notes')
BEGIN
	EXEC('SET IDENTITY_INSERT dbo.Tmp_client_product_notes ON;
		  INSERT INTO dbo.Tmp_client_product_notes (oID, client_product, type, dont_edit, dont_delete, dont_print, subject, note, date_created, created_by)
		  SELECT oID, client_product, type, dont_edit, dont_delete, dont_print, subject, note, date_created, created_by FROM dbo.client_product_notes WITH (HOLDLOCK TABLOCKX);
		  SET IDENTITY_INSERT dbo.Tmp_client_product_notes OFF;')
	EXEC('DROP TABLE dbo.client_product_notes')
END
GO
EXECUTE sp_rename N'dbo.Tmp_client_product_notes', N'client_product_notes', 'OBJECT' 
GO
ALTER TABLE dbo.client_product_notes ADD CONSTRAINT
	PK_client_product_notes PRIMARY KEY CLUSTERED 
	(
	oID
	) WITH( STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX ix_client_product_notes_client_product_includes ON dbo.client_product_notes
	(
	client_product
	) INCLUDE (oID, type, dont_edit, dont_delete, dont_print, subject, date_created, created_by) 
 WITH( PAD_INDEX = OFF, FILLFACTOR = 100, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX ix_client_product_notes_2 ON dbo.client_product_notes
	(
	oID,
	subject
	) INCLUDE (created_by) 
 WITH( PAD_INDEX = OFF, FILLFACTOR = 100, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
ALTER TABLE dbo.client_product_notes ADD CONSTRAINT
	FK_client_product_notes_client_products FOREIGN KEY
	(
	client_product
	) REFERENCES dbo.client_products
	(
	oID
	) ON UPDATE  CASCADE 
	 ON DELETE  CASCADE 
GO
DENY DELETE ON dbo.client_product_notes TO www_role  AS dbo 
GO
DENY INSERT ON dbo.client_product_notes TO www_role  AS dbo 
GO
DENY SELECT ON dbo.client_product_notes TO www_role  AS dbo 
GO
DENY UPDATE ON dbo.client_product_notes TO www_role  AS dbo 
GO
COMMIT
