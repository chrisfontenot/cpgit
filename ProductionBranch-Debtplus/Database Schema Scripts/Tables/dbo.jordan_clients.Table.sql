USE [DebtPlus]
GO
/****** Object:  Table [dbo].[jordan_clients]    Script Date: 09/15/2014 13:13:26 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[jordan_clients](
	[client] [int] NULL,
	[appt_date] [datetime] NULL,
	[appt_type] [varchar](50) NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
