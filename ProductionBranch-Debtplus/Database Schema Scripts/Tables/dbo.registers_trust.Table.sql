if exists(select * from sysobjects where type = 'U' and name = N'saved_registers_trust')
	exec ('drop table saved_registers_trust')
go
exec('select * into saved_registers_trust from registers_trust')
go

/*
   Wednesday, November 04, 20156:46:55 AM
   User: sa
   Server: 10.0.199.12
   Database: DebtPlus_HPF
   Application: 
*/

/* To prevent any potential data loss issues, you should review this script in detail before running it outside the context of the database designer.*/
BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION
GO
EXECUTE UPDATE_DROP_CONSTRAINTS 'registers_trust'
GO
EXECUTE UPDATE_ADD_COLUMN 'registers_trust', 'checknum', 'bigint'
GO
CREATE TABLE dbo.Tmp_registers_trust
	(
	trust_register dbo.typ_key NOT NULL IDENTITY (1, 1),
	tran_type dbo.typ_transaction NOT NULL,
	client dbo.typ_key NULL,
	creditor dbo.typ_creditor NULL,
	check_order int NOT NULL,
	cleared dbo.typ_cleared NOT NULL,
	bank dbo.typ_key NOT NULL,
	checknum bigint NULL,
	amount money NOT NULL,
	invoice_register dbo.typ_key NULL,
	sequence_number varchar(50) NULL,
	reconciled_date datetime NULL,
	bank_xmit_date datetime NULL,
	created_by dbo.typ_counselor NOT NULL,
	date_created dbo.typ_date NOT NULL,
	ts timestamp NOT NULL,
	check_number varchar(50) NULL
	)  ON [PRIMARY]
GO
GRANT DELETE ON dbo.Tmp_registers_trust TO public  AS dbo
GO
GRANT INSERT ON dbo.Tmp_registers_trust TO public  AS dbo
GO
GRANT SELECT ON dbo.Tmp_registers_trust TO public  AS dbo
GO
GRANT UPDATE ON dbo.Tmp_registers_trust TO public  AS dbo
GO
DECLARE @v sql_variant 
SET @v = N'Pointer to the bank for the transaction'
EXECUTE sp_addextendedproperty N'MS_Description', @v, N'SCHEMA', N'dbo', N'TABLE', N'Tmp_registers_trust', N'COLUMN', N'bank'
GO
DECLARE @v sql_variant 
SET @v = N'Date/Time when the item was included in the extract for the bank'
EXECUTE sp_addextendedproperty N'MS_Description', @v, N'SCHEMA', N'dbo', N'TABLE', N'Tmp_registers_trust', N'COLUMN', N'bank_xmit_date'
GO
EXECUTE sp_bindefault N'dbo.default_priority', N'dbo.Tmp_registers_trust.check_order'
GO
ALTER TABLE dbo.Tmp_registers_trust ADD CONSTRAINT
	DF_registers_trust_bank DEFAULT ((1)) FOR bank
GO
EXECUTE sp_bindefault N'dbo.default_money', N'dbo.Tmp_registers_trust.amount'
GO
SET IDENTITY_INSERT dbo.Tmp_registers_trust ON
GO
EXECUTE sp_unbindrule N'dbo.Tmp_registers_trust.creditor'
GO
IF EXISTS(SELECT * FROM dbo.registers_trust)
	EXEC('INSERT INTO dbo.Tmp_registers_trust (trust_register, tran_type, client, creditor, check_order, cleared, bank, check_number, amount, invoice_register, sequence_number, reconciled_date, bank_xmit_date, created_by, date_created, checknum)
		SELECT trust_register, tran_type, client, creditor, check_order, cleared, bank, check_number, amount, invoice_register, sequence_number, reconciled_date, bank_xmit_date, created_by, date_created, convert(bigint,dbo.numbers_only(check_number)) as checknum FROM dbo.registers_trust WITH (HOLDLOCK TABLOCKX)')
GO
SET IDENTITY_INSERT dbo.Tmp_registers_trust OFF
GO
DROP TABLE dbo.registers_trust
GO
EXECUTE sp_rename N'dbo.Tmp_registers_trust', N'registers_trust', 'OBJECT' 
GO
ALTER TABLE dbo.registers_trust ADD CONSTRAINT
	PK_registers_trust PRIMARY KEY CLUSTERED 
	(
	trust_register
	) WITH( STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX IX_registers_trust_1 ON dbo.registers_trust
	(
	bank,
	checknum
	) WITH( PAD_INDEX = OFF, FILLFACTOR = 90, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX IX_registers_trust_2 ON dbo.registers_trust
	(
	sequence_number
	) WITH( PAD_INDEX = OFF, FILLFACTOR = 90, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX IX_registers_trust_3 ON dbo.registers_trust
	(
	creditor,
	tran_type
	) WITH( PAD_INDEX = OFF, FILLFACTOR = 90, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX IX_registers_trust_4 ON dbo.registers_trust
	(
	client,
	tran_type
	) WITH( PAD_INDEX = OFF, FILLFACTOR = 90, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX IX_registers_trust_5 ON dbo.registers_trust
	(
	invoice_register
	) WITH( PAD_INDEX = OFF, FILLFACTOR = 90, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
EXECUTE sp_bindrule N'dbo.valid_creditor', N'dbo.registers_trust.creditor'
GO
DENY DELETE ON dbo.registers_trust TO www_role  AS dbo 
GO
DENY INSERT ON dbo.registers_trust TO www_role  AS dbo 
GO
DENY SELECT ON dbo.registers_trust TO www_role  AS dbo 
GO
DENY UPDATE ON dbo.registers_trust TO www_role  AS dbo 
GO
COMMIT
