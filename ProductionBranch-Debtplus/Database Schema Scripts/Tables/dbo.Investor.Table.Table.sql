/* To prevent any potential data loss issues, you should review this script in detail before running it outside the context of the database designer.*/
BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION
GO
EXECUTE UPDATE_ADD_COLUMN 'Investor', 'hpf'
GO
CREATE TABLE dbo.Tmp_Investor
	(
	ID dbo.typ_key NOT NULL IDENTITY (1, 1),
	InvestorName dbo.typ_description NOT NULL,
	[Default] bit NOT NULL,
	IsActive bit NOT NULL,
	RxOffice varchar(20) NULL,
	hpf varchar(50) NULL,
	date_created dbo.typ_date NOT NULL,
	created_by dbo.typ_counselor NOT NULL,
	ts timestamp NOT NULL
	)  ON [PRIMARY]
GO
GRANT DELETE ON dbo.Tmp_Investor TO public  AS dbo
GO
GRANT INSERT ON dbo.Tmp_Investor TO public  AS dbo
GO
GRANT SELECT ON dbo.Tmp_Investor TO public  AS dbo
GO
GRANT UPDATE ON dbo.Tmp_Investor TO public  AS dbo
GO
DECLARE @v sql_variant 
SET @v = N'Primary key to the table'
EXECUTE sp_addextendedproperty N'MS_Description', @v, N'SCHEMA', N'dbo', N'TABLE', N'Tmp_Investor', N'COLUMN', N'ID'
GO
DECLARE @v sql_variant 
SET @v = N'Description for the investor'
EXECUTE sp_addextendedproperty N'MS_Description', @v, N'SCHEMA', N'dbo', N'TABLE', N'Tmp_Investor', N'COLUMN', N'InvestorName'
GO
DECLARE @v sql_variant 
SET @v = N'TRUE if this is the default item'
EXECUTE sp_addextendedproperty N'MS_Description', @v, N'SCHEMA', N'dbo', N'TABLE', N'Tmp_Investor', N'COLUMN', N'Default'
GO
DECLARE @v sql_variant 
SET @v = N'TRUE if the value may be used for new references'
EXECUTE sp_addextendedproperty N'MS_Description', @v, N'SCHEMA', N'dbo', N'TABLE', N'Tmp_Investor', N'COLUMN', N'IsActive'
GO
DECLARE @v sql_variant 
SET @v = N'Date/Time when the item was created'
EXECUTE sp_addextendedproperty N'MS_Description', @v, N'SCHEMA', N'dbo', N'TABLE', N'Tmp_Investor', N'COLUMN', N'date_created'
GO
DECLARE @v sql_variant 
SET @v = N'Person who created the item'
EXECUTE sp_addextendedproperty N'MS_Description', @v, N'SCHEMA', N'dbo', N'TABLE', N'Tmp_Investor', N'COLUMN', N'created_by'
GO
EXECUTE sp_bindefault N'dbo.default_false', N'dbo.Tmp_Investor.[Default]'
GO
EXECUTE sp_bindefault N'dbo.default_true', N'dbo.Tmp_Investor.IsActive'
GO
SET IDENTITY_INSERT dbo.Tmp_Investor ON
GO
IF EXISTS(SELECT * FROM dbo.Investor)
	 EXEC('INSERT INTO dbo.Tmp_Investor (ID, InvestorName, [Default], IsActive, date_created, created_by, RxOffice, HPF)
		SELECT ID, InvestorName, [Default], IsActive, date_created, created_by, RxOffice, HPF FROM dbo.Investor WITH (HOLDLOCK TABLOCKX)')
GO
SET IDENTITY_INSERT dbo.Tmp_Investor OFF
GO
DROP TABLE dbo.Investor
GO
EXECUTE sp_rename N'dbo.Tmp_Investor', N'Investor', 'OBJECT' 
GO
ALTER TABLE dbo.Investor ADD CONSTRAINT
	PK_Investor PRIMARY KEY CLUSTERED 
	(
	ID
	) WITH( STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
DENY DELETE ON dbo.Investor TO www_role  AS dbo 
GO
DENY INSERT ON dbo.Investor TO www_role  AS dbo 
GO
DENY SELECT ON dbo.Investor TO www_role  AS dbo 
GO
DENY UPDATE ON dbo.Investor TO www_role  AS dbo 
GO
COMMIT
