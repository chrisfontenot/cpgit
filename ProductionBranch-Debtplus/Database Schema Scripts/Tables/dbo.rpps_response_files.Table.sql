USE [DebtPlus]
GO
/****** Object:  Table [dbo].[rpps_response_files]    Script Date: 09/15/2014 13:13:26 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[rpps_response_files](
	[rpps_response_file] [int] IDENTITY(1,1) NOT NULL,
	[date_posted] [datetime] NULL,
	[filename] [varchar](50) NULL,
	[label] [dbo].[typ_description] NULL,
	[file_date] [datetime] NULL,
	[date_created] [dbo].[typ_date] NOT NULL,
	[created_by] [dbo].[typ_counselor] NOT NULL,
 CONSTRAINT [PK_rpps_response_files] PRIMARY KEY CLUSTERED 
(
	[rpps_response_file] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 90) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
