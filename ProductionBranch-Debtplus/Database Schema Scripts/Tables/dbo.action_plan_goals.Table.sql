USE [DebtPlus]
GO
/****** Object:  Table [dbo].[action_plan_goals]    Script Date: 09/15/2014 13:13:25 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[action_plan_goals](
	[action_plan] [dbo].[typ_key] NOT NULL,
	[short_term_goals] [text] NULL,
	[long_term_goals] [text] NULL,
	[financial_problem_id] [dbo].[typ_key] NULL,
	[financial_problem_desc] [text] NULL,
	[housing_consideration_desc] [text] NULL,
	[recommendation] [text] NULL,
	[options] [text] NULL,
	[date_created] [dbo].[typ_date] NOT NULL,
	[created_by] [dbo].[typ_counselor] NOT NULL,
 CONSTRAINT [PK_action_plan_goals] PRIMARY KEY CLUSTERED 
(
	[action_plan] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 90) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
