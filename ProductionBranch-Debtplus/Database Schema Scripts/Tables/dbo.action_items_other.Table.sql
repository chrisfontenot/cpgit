USE [DebtPlus]
GO
/****** Object:  Table [dbo].[action_items_other]    Script Date: 09/15/2014 13:13:25 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[action_items_other](
	[action_item_other] [dbo].[typ_key] IDENTITY(10000,1) NOT NULL,
	[action_plan] [dbo].[typ_key] NOT NULL,
	[item_group] [int] NOT NULL,
	[description] [dbo].[typ_description] NOT NULL,
	[date_created] [dbo].[typ_date] NOT NULL,
	[created_by] [dbo].[typ_counselor] NOT NULL,
	[old_action_item_other] [int] NULL,
	[old_action_item] [int] NULL,
 CONSTRAINT [PK_action_items_other] PRIMARY KEY CLUSTERED 
(
	[action_item_other] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 90) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Primary key to the table. This is the item referenced by the client_action_items table.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'action_items_other', @level2type=N'COLUMN',@level2name=N'action_item_other'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Action plan associated with the item.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'action_items_other', @level2type=N'COLUMN',@level2name=N'action_plan'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Group for the action plan item. It is the same as the "action_items" table.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'action_items_other', @level2type=N'COLUMN',@level2name=N'item_group'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Description of the item.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'action_items_other', @level2type=N'COLUMN',@level2name=N'description'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Date/Time when the row was created.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'action_items_other', @level2type=N'COLUMN',@level2name=N'date_created'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Person who created the row.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'action_items_other', @level2type=N'COLUMN',@level2name=N'created_by'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'If the action plan requires a non-system item, the additional items are kept in this table. It lists the action plan, client, and the group along with the description. This table is joined with the action_items to form the total list of identified action items.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'action_items_other'
GO
ALTER TABLE [dbo].[action_items_other] ADD  CONSTRAINT [DF_action_items_other_item_group]  DEFAULT ((1)) FOR [item_group]
GO
