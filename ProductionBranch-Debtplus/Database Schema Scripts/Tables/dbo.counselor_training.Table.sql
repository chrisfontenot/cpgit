USE [DebtPlus]
GO
/****** Object:  Table [dbo].[counselor_training]    Script Date: 09/15/2014 13:13:26 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[counselor_training](
	[oID] [dbo].[typ_key] IDENTITY(1,1) NOT NULL,
	[Counselor] [dbo].[typ_key] NOT NULL,
	[TrainingTitle] [varchar](50) NULL,
	[TrainingDate] [datetime] NULL,
	[TrainingDuration] [int] NULL,
	[TrainingCertificateDate] [datetime] NULL,
	[TrainingOrganization] [dbo].[typ_key] NULL,
	[TrainingOrganizationOther] [varchar](50) NULL,
	[TrainingSponsor] [dbo].[typ_key] NULL,
	[TrainingSponsorOther] [varchar](50) NULL,
 CONSTRAINT [PK_counselor_trainings] PRIMARY KEY CLUSTERED 
(
	[oID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
