/* To prevent any potential data loss issues, you should review this script in detail before running it outside the context of the database designer.*/
BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
GO
EXECUTE UPDATE_DROP_CONSTRAINTS 'client_creditor_balances'
GO
EXECUTE UPDATE_ADD_COLUMN 'client_creditor_balances', 'zero_bal_status', 'int'
GO
EXECUTE UPDATE_ADD_COLUMN 'client_creditor_balances', 'zero_bal_letter_date', 'datetime'
GO
BEGIN TRANSACTION
GO
CREATE TABLE dbo.Tmp_client_creditor_balances
	(
	client_creditor_balance dbo.typ_key NOT NULL IDENTITY (1, 1) NOT FOR REPLICATION,
	client_creditor dbo.typ_key NOT NULL,
	orig_balance money NOT NULL,
	orig_balance_adjustment money NOT NULL,
	total_payments money NOT NULL,
	total_interest money NOT NULL,
	current_sched_payment money NOT NULL,
	total_sched_payment money NOT NULL,
	payments_month_0 money NOT NULL,
	payments_month_1 money NOT NULL,
	zero_bal_status int NOT NULL,
	zero_bal_letter_date datetime NULL,
	created_by dbo.typ_counselor NOT NULL,
	date_created dbo.typ_date NOT NULL,
	ts timestamp NOT NULL
	)  ON [PRIMARY]
GO
GRANT DELETE ON dbo.Tmp_client_creditor_balances TO public  AS dbo
GO
GRANT INSERT ON dbo.Tmp_client_creditor_balances TO public  AS dbo
GO
GRANT SELECT ON dbo.Tmp_client_creditor_balances TO public  AS dbo
GO
GRANT UPDATE ON dbo.Tmp_client_creditor_balances TO public  AS dbo
GO
EXECUTE sp_bindefault N'dbo.default_zero', N'dbo.Tmp_client_creditor_balances.client_creditor'
GO
EXECUTE sp_bindefault N'dbo.default_money', N'dbo.Tmp_client_creditor_balances.orig_balance'
GO
EXECUTE sp_bindefault N'dbo.default_money', N'dbo.Tmp_client_creditor_balances.orig_balance_adjustment'
GO
EXECUTE sp_bindefault N'dbo.default_money', N'dbo.Tmp_client_creditor_balances.total_payments'
GO
EXECUTE sp_bindefault N'dbo.default_money', N'dbo.Tmp_client_creditor_balances.total_interest'
GO
EXECUTE sp_bindefault N'dbo.default_money', N'dbo.Tmp_client_creditor_balances.current_sched_payment'
GO
EXECUTE sp_bindefault N'dbo.default_money', N'dbo.Tmp_client_creditor_balances.total_sched_payment'
GO
EXECUTE sp_bindefault N'dbo.default_money', N'dbo.Tmp_client_creditor_balances.payments_month_0'
GO
EXECUTE sp_bindefault N'dbo.default_money', N'dbo.Tmp_client_creditor_balances.payments_month_1'
GO
EXECUTE sp_bindefault N'dbo.default_zero', N'dbo.Tmp_client_creditor_balances.zero_bal_status'
GO
SET IDENTITY_INSERT dbo.Tmp_client_creditor_balances ON
GO
IF EXISTS(SELECT * FROM dbo.client_creditor_balances)
	 EXEC('INSERT INTO dbo.Tmp_client_creditor_balances (client_creditor_balance, client_creditor, orig_balance, orig_balance_adjustment, total_payments, total_interest, current_sched_payment, total_sched_payment, payments_month_0, payments_month_1, zero_bal_status, zero_bal_letter_date, created_by, date_created)
		SELECT client_creditor_balance, client_creditor, orig_balance, orig_balance_adjustment, total_payments, total_interest, current_sched_payment, total_sched_payment, payments_month_0, payments_month_1, ISNULL(zero_bal_status,0), zero_bal_letter_date, created_by, date_created FROM dbo.client_creditor_balances WITH (HOLDLOCK TABLOCKX)')
GO
SET IDENTITY_INSERT dbo.Tmp_client_creditor_balances OFF
GO
DROP TABLE dbo.client_creditor_balances
GO
EXECUTE sp_rename N'dbo.Tmp_client_creditor_balances', N'client_creditor_balances', 'OBJECT' 
GO
ALTER TABLE dbo.client_creditor_balances ADD CONSTRAINT
	PK_client_creditor_balances PRIMARY KEY CLUSTERED 
	(
	client_creditor_balance
	) WITH( STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX IX_client_creditor_balances_1 ON dbo.client_creditor_balances
	(
	client_creditor
	) WITH( STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX IX_client_creditor_balances_2 ON dbo.client_creditor_balances
	(
	total_payments
	) INCLUDE (client_creditor_balance, client_creditor, orig_balance, orig_balance_adjustment, total_interest) 
 WITH( STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
DENY DELETE ON dbo.client_creditor_balances TO www_role  AS dbo 
GO
DENY INSERT ON dbo.client_creditor_balances TO www_role  AS dbo 
GO
DENY SELECT ON dbo.client_creditor_balances TO www_role  AS dbo 
GO
DENY UPDATE ON dbo.client_creditor_balances TO www_role  AS dbo 
GO
COMMIT
BEGIN TRANSACTION
GO
UPDATE client_creditor SET client_creditor_balance = 0 where client_creditor_balance not in (SELECT client_creditor_balance from client_creditor_balances)
GO
ALTER TABLE dbo.client_creditor ADD CONSTRAINT
	FK_client_creditor_client_creditor_balances FOREIGN KEY
	(
	client_creditor_balance
	) REFERENCES dbo.client_creditor_balances
	(
	client_creditor_balance
	) ON UPDATE  CASCADE 
	 ON DELETE  CASCADE 
GO
COMMIT
GO
