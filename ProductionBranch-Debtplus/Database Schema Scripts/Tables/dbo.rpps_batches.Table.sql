USE [DebtPlus]
GO
/****** Object:  Table [dbo].[rpps_batches]    Script Date: 09/15/2014 13:13:26 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[rpps_batches](
	[rpps_batch] [dbo].[typ_key] IDENTITY(1,1) NOT NULL,
	[rpps_file] [dbo].[typ_key] NULL,
	[trace_number] [dbo].[typ_trace_number] NULL,
	[service_class] [char](3) NULL,
	[entry_class] [char](3) NULL,
	[rpps_biller_id] [dbo].[typ_rpps_biller_id] NULL,
	[biller_name] [varchar](50) NULL,
	[death_date] [datetime] NOT NULL,
	[date_created] [dbo].[typ_date] NOT NULL,
	[created_by] [dbo].[typ_counselor] NOT NULL,
 CONSTRAINT [PK_rps_batches] PRIMARY KEY CLUSTERED 
(
	[rpps_batch] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 90) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
ALTER TABLE [dbo].[rpps_batches] ADD  CONSTRAINT [DF_rpps_batches_service_class]  DEFAULT ('220') FOR [service_class]
GO
ALTER TABLE [dbo].[rpps_batches] ADD  CONSTRAINT [DF_rpps_batches_entry_class]  DEFAULT ('CIE') FOR [entry_class]
GO
ALTER TABLE [dbo].[rpps_batches] ADD  CONSTRAINT [DF_rpps_batches_death_date]  DEFAULT (dateadd(day,(180),getdate())) FOR [death_date]
GO
