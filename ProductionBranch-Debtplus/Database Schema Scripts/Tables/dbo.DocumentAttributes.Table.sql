USE [DebtPlus]
GO
/****** Object:  Table [dbo].[DocumentAttributes]    Script Date: 09/15/2014 13:13:26 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[DocumentAttributes](
	[oID] [dbo].[typ_key] IDENTITY(1,1) NOT NULL,
	[DocumentID] [dbo].[typ_key] NOT NULL,
	[LanguageID] [dbo].[typ_key] NULL,
	[State] [dbo].[typ_key] NULL,
	[AllLanguagesFLG] [bit] NOT NULL,
	[AllStatesFLG] [bit] NOT NULL,
 CONSTRAINT [PK_DocumentAttributes] PRIMARY KEY CLUSTERED 
(
	[oID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
