USE [DebtPlus]
GO
/****** Object:  Table [dbo].[creditor_classes]    Script Date: 09/15/2014 13:13:26 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[creditor_classes](
	[creditor_class] [dbo].[typ_key] IDENTITY(1,1) NOT NULL,
	[description] [dbo].[typ_description] NOT NULL,
	[zero_balance] [bit] NOT NULL,
	[prorate] [bit] NOT NULL,
	[always_disburse] [bit] NOT NULL,
	[agency_account] [bit] NOT NULL,
	[proposal_balance] [bit] NOT NULL,
	[client_statement] [bit] NOT NULL,
	[creditor_statement] [bit] NOT NULL,
	[created_by] [dbo].[typ_counselor] NOT NULL,
	[date_created] [dbo].[typ_date] NOT NULL,
 CONSTRAINT [PK_creditor_class] PRIMARY KEY CLUSTERED 
(
	[creditor_class] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 90) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Primary key to the table' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'creditor_classes', @level2type=N'COLUMN',@level2name=N'creditor_class'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'A general description of the class. It is shown to the user.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'creditor_classes', @level2type=N'COLUMN',@level2name=N'description'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Does the creditor have a balance limit?' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'creditor_classes', @level2type=N'COLUMN',@level2name=N'zero_balance'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'If the creditor is in the disbursement, does it change for a prorate function?' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'creditor_classes', @level2type=N'COLUMN',@level2name=N'prorate'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Disburse on these debts irregardless of client''s active status' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'creditor_classes', @level2type=N'COLUMN',@level2name=N'always_disburse'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Is this an agency account? If so, the client ID is assumed for the account number.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'creditor_classes', @level2type=N'COLUMN',@level2name=N'agency_account'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Should the balance be included in total debt on proposals?' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'creditor_classes', @level2type=N'COLUMN',@level2name=N'proposal_balance'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Name of the person who created the row' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'creditor_classes', @level2type=N'COLUMN',@level2name=N'created_by'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Date the row was created' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'creditor_classes', @level2type=N'COLUMN',@level2name=N'date_created'
GO
