DELETE FROM sales_debts WHERE sales_file NOT IN (SELECT sales_file FROM sales_files)
GO
ALTER TABLE dbo.sales_debts DROP CONSTRAINT FK_sales_debts_sales_files
GO
/* To prevent any potential data loss issues, you should review this script in detail before running it outside the context of the database designer.*/
BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION
GO
CREATE TABLE dbo.Tmp_sales_files
	(
	sales_file dbo.typ_key NOT NULL IDENTITY (1, 1),
	client dbo.typ_client NOT NULL,
	extra_amount money NOT NULL,
	note varchar(4096) NULL,
	client_monthly_interest money NOT NULL,
	plan_monthly_interest money NOT NULL,
	client_total_interest money NOT NULL,
	plan_total_interest money NOT NULL,
	client_months float(53) NOT NULL,
	plan_months float(53) NOT NULL,
	date_exported datetime NULL,
	exported_by varchar(80) NULL,
	date_created dbo.typ_date NOT NULL,
	created_by dbo.typ_counselor NOT NULL,
	ts timestamp NOT NULL
	)  ON [PRIMARY]
GO
GRANT DELETE ON dbo.Tmp_sales_files TO public  AS dbo
GO
GRANT INSERT ON dbo.Tmp_sales_files TO public  AS dbo
GO
GRANT SELECT ON dbo.Tmp_sales_files TO public  AS dbo
GO
GRANT UPDATE ON dbo.Tmp_sales_files TO public  AS dbo
GO
DECLARE @v sql_variant 
SET @v = N'Primary key to the table'
EXECUTE sp_addextendedproperty N'MS_Description', @v, N'SCHEMA', N'dbo', N'TABLE', N'Tmp_sales_files', N'COLUMN', N'sales_file'
GO
DECLARE @v sql_variant 
SET @v = N'Pointer to the clients table. This is the client ID.'
EXECUTE sp_addextendedproperty N'MS_Description', @v, N'SCHEMA', N'dbo', N'TABLE', N'Tmp_sales_files', N'COLUMN', N'client'
GO
DECLARE @v sql_variant 
SET @v = N'"Extra" payment amount for the plan'
EXECUTE sp_addextendedproperty N'MS_Description', @v, N'SCHEMA', N'dbo', N'TABLE', N'Tmp_sales_files', N'COLUMN', N'extra_amount'
GO
DECLARE @v sql_variant 
SET @v = N'Note text'
EXECUTE sp_addextendedproperty N'MS_Description', @v, N'SCHEMA', N'dbo', N'TABLE', N'Tmp_sales_files', N'COLUMN', N'note'
GO
DECLARE @v sql_variant 
SET @v = N'Client: monthly interest amount'
EXECUTE sp_addextendedproperty N'MS_Description', @v, N'SCHEMA', N'dbo', N'TABLE', N'Tmp_sales_files', N'COLUMN', N'client_monthly_interest'
GO
DECLARE @v sql_variant 
SET @v = N'Plan: monthly interest amount'
EXECUTE sp_addextendedproperty N'MS_Description', @v, N'SCHEMA', N'dbo', N'TABLE', N'Tmp_sales_files', N'COLUMN', N'plan_monthly_interest'
GO
DECLARE @v sql_variant 
SET @v = N'Client: total interest amount'
EXECUTE sp_addextendedproperty N'MS_Description', @v, N'SCHEMA', N'dbo', N'TABLE', N'Tmp_sales_files', N'COLUMN', N'client_total_interest'
GO
DECLARE @v sql_variant 
SET @v = N'Plan: total interest amount'
EXECUTE sp_addextendedproperty N'MS_Description', @v, N'SCHEMA', N'dbo', N'TABLE', N'Tmp_sales_files', N'COLUMN', N'plan_total_interest'
GO
DECLARE @v sql_variant 
SET @v = N'Client: number of months required to pay the debts'
EXECUTE sp_addextendedproperty N'MS_Description', @v, N'SCHEMA', N'dbo', N'TABLE', N'Tmp_sales_files', N'COLUMN', N'client_months'
GO
DECLARE @v sql_variant 
SET @v = N'Plan: number of months required to pay the debts'
EXECUTE sp_addextendedproperty N'MS_Description', @v, N'SCHEMA', N'dbo', N'TABLE', N'Tmp_sales_files', N'COLUMN', N'plan_months'
GO
DECLARE @v sql_variant 
SET @v = N'Date and time when the file was exported to the DebtPlus client list'
EXECUTE sp_addextendedproperty N'MS_Description', @v, N'SCHEMA', N'dbo', N'TABLE', N'Tmp_sales_files', N'COLUMN', N'date_exported'
GO
DECLARE @v sql_variant 
SET @v = N'Name of the counselor who exported the file.'
EXECUTE sp_addextendedproperty N'MS_Description', @v, N'SCHEMA', N'dbo', N'TABLE', N'Tmp_sales_files', N'COLUMN', N'exported_by'
GO
DECLARE @v sql_variant 
SET @v = N'Date and time the item was created'
EXECUTE sp_addextendedproperty N'MS_Description', @v, N'SCHEMA', N'dbo', N'TABLE', N'Tmp_sales_files', N'COLUMN', N'date_created'
GO
DECLARE @v sql_variant 
SET @v = N'Name of the person who created the plan'
EXECUTE sp_addextendedproperty N'MS_Description', @v, N'SCHEMA', N'dbo', N'TABLE', N'Tmp_sales_files', N'COLUMN', N'created_by'
GO
EXECUTE sp_bindefault N'dbo.default_money', N'dbo.Tmp_sales_files.extra_amount'
GO
EXECUTE sp_bindefault N'dbo.default_money', N'dbo.Tmp_sales_files.client_monthly_interest'
GO
EXECUTE sp_bindefault N'dbo.default_money', N'dbo.Tmp_sales_files.plan_monthly_interest'
GO
EXECUTE sp_bindefault N'dbo.default_money', N'dbo.Tmp_sales_files.client_total_interest'
GO
EXECUTE sp_bindefault N'dbo.default_money', N'dbo.Tmp_sales_files.plan_total_interest'
GO
EXECUTE sp_bindefault N'dbo.default_zero', N'dbo.Tmp_sales_files.client_months'
GO
EXECUTE sp_bindefault N'dbo.default_zero', N'dbo.Tmp_sales_files.plan_months'
GO
SET IDENTITY_INSERT dbo.Tmp_sales_files ON
GO
IF EXISTS(SELECT * FROM dbo.sales_files)
	 EXEC('INSERT INTO dbo.Tmp_sales_files (sales_file, client, extra_amount, note, client_monthly_interest, plan_monthly_interest, client_total_interest, plan_total_interest, client_months, plan_months, date_exported, exported_by, date_created, created_by)
		SELECT sales_file, client, extra_amount, note, client_monthly_interest, plan_monthly_interest, client_total_interest, plan_total_interest, client_months, plan_months, date_exported, exported_by, date_created, created_by FROM dbo.sales_files WITH (HOLDLOCK TABLOCKX)')
GO
SET IDENTITY_INSERT dbo.Tmp_sales_files OFF
GO
DROP TABLE dbo.sales_files
GO
EXECUTE sp_rename N'dbo.Tmp_sales_files', N'sales_files', 'OBJECT' 
GO
ALTER TABLE dbo.sales_files ADD CONSTRAINT PK_sales_files PRIMARY KEY CLUSTERED 
	(
	sales_file
	) WITH( PAD_INDEX = OFF, FILLFACTOR = 90, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
DENY DELETE ON dbo.sales_files TO www_role  AS dbo 
GO
DENY INSERT ON dbo.sales_files TO www_role  AS dbo 
GO
DENY SELECT ON dbo.sales_files TO www_role  AS dbo 
GO
DENY UPDATE ON dbo.sales_files TO www_role  AS dbo 
GO
COMMIT
BEGIN TRANSACTION
GO
ALTER TABLE dbo.sales_debts ADD CONSTRAINT FK_sales_debts_sales_files FOREIGN KEY
	(
	sales_file
	) REFERENCES dbo.sales_files
	(
	sales_file
	) ON UPDATE CASCADE 
	 ON DELETE CASCADE 
GO
DECLARE @v sql_variant 
SET @v = N'Link the sales_debts table to the sales_file table for delete operations on the sales_file'
EXECUTE sp_addextendedproperty N'MS_Description', @v, N'SCHEMA', N'dbo', N'TABLE', N'sales_debts', N'CONSTRAINT', N'FK_sales_debts_sales_files'
GO
COMMIT
