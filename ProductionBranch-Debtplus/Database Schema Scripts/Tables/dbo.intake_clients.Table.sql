USE [DebtPlus]
GO
/****** Object:  Table [dbo].[intake_clients]    Script Date: 09/15/2014 13:13:26 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[intake_clients](
	[intake_client] [dbo].[typ_key] IDENTITY(1,1) NOT NULL,
	[client] [dbo].[typ_key] NULL,
	[intake_id] [varchar](20) NULL,
	[address1] [dbo].[typ_address] NULL,
	[address2] [dbo].[typ_address] NULL,
	[city] [dbo].[typ_city] NULL,
	[state] [dbo].[typ_key] NULL,
	[PostalCode] [varchar](50) NULL,
	[home_ph] [dbo].[typ_phone] NULL,
	[message_ph] [dbo].[typ_phone] NULL,
	[housing_status] [dbo].[typ_key] NULL,
	[housing_type] [dbo].[typ_key] NULL,
	[referred_by] [dbo].[typ_key] NULL,
	[cause_fin_problem1] [dbo].[typ_key] NULL,
	[fed_tax_owed] [money] NOT NULL,
	[state_tax_owed] [money] NOT NULL,
	[local_tax_owed] [money] NOT NULL,
	[fed_tax_months] [int] NOT NULL,
	[state_tax_months] [int] NOT NULL,
	[local_tax_months] [int] NOT NULL,
	[intake_agreement] [bit] NOT NULL,
	[bankruptcy] [bit] NOT NULL,
	[marital_status] [int] NOT NULL,
	[message] [varchar](1024) NULL,
	[people] [int] NOT NULL,
	[date_imported] [datetime] NULL,
	[imported_by] [varchar](80) NULL,
	[completed_date] [datetime] NULL,
	[identity_date] [datetime] NULL,
	[ReEntry_ID] [varchar](80) NULL,
	[ReEntry_Password] [varchar](50) NULL,
	[date_created] [dbo].[typ_date] NOT NULL,
	[created_by] [dbo].[typ_counselor] NOT NULL,
	[car_payment_current] [bit] NOT NULL,
	[auto_arrears_amt] [money] NOT NULL,
	[auto_arrears_months] [int] NOT NULL,
 CONSTRAINT [PK_intake_clients] PRIMARY KEY CLUSTERED 
(
	[intake_client] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 90) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Primary key to the table. This is the intake client used in the other intake_* tables.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'intake_clients', @level2type=N'COLUMN',@level2name=N'intake_client'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'When the client is imported into debtplus'' database, this is the "real" client ID.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'intake_clients', @level2type=N'COLUMN',@level2name=N'client'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'ID string given to the user for reference. It is YYYYMMDDnnnn where ''nnnn'' is the relative client in the date (MM/DD/YYYY)' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'intake_clients', @level2type=N'COLUMN',@level2name=N'intake_id'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'1st line of the client home address' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'intake_clients', @level2type=N'COLUMN',@level2name=N'address1'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'2nd line of the client home address' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'intake_clients', @level2type=N'COLUMN',@level2name=N'address2'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'city of the client home address' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'intake_clients', @level2type=N'COLUMN',@level2name=N'city'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'State of the client home address' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'intake_clients', @level2type=N'COLUMN',@level2name=N'state'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'US Postal zipcode of the client home address' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'intake_clients', @level2type=N'COLUMN',@level2name=N'PostalCode'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Home telephone number' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'intake_clients', @level2type=N'COLUMN',@level2name=N'home_ph'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Message telephone number' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'intake_clients', @level2type=N'COLUMN',@level2name=N'message_ph'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Housing status. See "messages" table for values.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'intake_clients', @level2type=N'COLUMN',@level2name=N'housing_status'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Housing type. See "messages" table for values.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'intake_clients', @level2type=N'COLUMN',@level2name=N'housing_type'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Referral value. See "referred_by" table for values.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'intake_clients', @level2type=N'COLUMN',@level2name=N'referred_by'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Primary cause of financial problem. See "financial_problems" table.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'intake_clients', @level2type=N'COLUMN',@level2name=N'cause_fin_problem1'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Total amount of federal tax owed.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'intake_clients', @level2type=N'COLUMN',@level2name=N'fed_tax_owed'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'State income tax owed' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'intake_clients', @level2type=N'COLUMN',@level2name=N'state_tax_owed'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Locality / city tax owed' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'intake_clients', @level2type=N'COLUMN',@level2name=N'local_tax_owed'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Number of months that the tax is overdue.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'intake_clients', @level2type=N'COLUMN',@level2name=N'fed_tax_months'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Number of months that the tax is overdue.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'intake_clients', @level2type=N'COLUMN',@level2name=N'state_tax_months'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Number of months that the tax is overdue.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'intake_clients', @level2type=N'COLUMN',@level2name=N'local_tax_months'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Set if the client clicked on the "agree" button for the intake agreement.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'intake_clients', @level2type=N'COLUMN',@level2name=N'intake_agreement'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Set if the client wanted bankruptcy.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'intake_clients', @level2type=N'COLUMN',@level2name=N'bankruptcy'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Marital status. See "messages" table for values.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'intake_clients', @level2type=N'COLUMN',@level2name=N'marital_status'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'A general message entered by the processing person for the intake import.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'intake_clients', @level2type=N'COLUMN',@level2name=N'message'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Number of people in the household.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'intake_clients', @level2type=N'COLUMN',@level2name=N'people'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'When the client is imported into the system, this is the date/time. When non-null, the client is considered to be imported and is no longer displayed in the list of clients.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'intake_clients', @level2type=N'COLUMN',@level2name=N'date_imported'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Name of the counselor who imported the client.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'intake_clients', @level2type=N'COLUMN',@level2name=N'imported_by'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'When the client completed the application, this is set to the current date/time.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'intake_clients', @level2type=N'COLUMN',@level2name=N'completed_date'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'When the client completed the identification page, this is set to the date/time. When it is null, the client has not given the identity information.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'intake_clients', @level2type=N'COLUMN',@level2name=N'identity_date'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Date/Time when the row was created.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'intake_clients', @level2type=N'COLUMN',@level2name=N'date_created'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Name of the counselor who created the row. This is normally the "www" (IUSR_*) account.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'intake_clients', @level2type=N'COLUMN',@level2name=N'created_by'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Status of the "car payment current?" radio button' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'intake_clients', @level2type=N'COLUMN',@level2name=N'car_payment_current'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Money owing on the automobile loans.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'intake_clients', @level2type=N'COLUMN',@level2name=N'auto_arrears_amt'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Number of months that it is past-due.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'intake_clients', @level2type=N'COLUMN',@level2name=N'auto_arrears_months'
GO
