
USE [DEBTPLUS]
GO

/****** Object:  Table [dbo].[aarp_call_log]    Script Date: 2/9/2015 2:23:32 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[aarp_call_log](
	[oID] [dbo].[typ_key] IDENTITY(1,1) NOT NULL,
	[ClientID] [dbo].[typ_key] NOT NULL,
	[IsAarpClient] [bit] NULL,
	[Btw50ProgramRequested] [bit] NULL,
	[SnapProgramRequested] [bit] NULL,
	[MediaFollowup] [bit] NULL,
	[HousingSituation] [int] NULL,
	[EmailOptIn] [bit] NULL,
	[TextOptIn] [bit] NULL,
	[PreferredMethod] [int] NULL,
	[CallReason] [int] NOT NULL,
	[CallResolution] [int] NOT NULL,
	[InitialProgramRequested] [int] NOT NULL,
	[AarpCode] [varchar](20) NULL,
	[IsActive] [bit] NOT NULL,
	[DateCreated] [dbo].[typ_date] NOT NULL,
	[CreatedBy] [dbo].[typ_counselor] NOT NULL,
	[AppointmentID] [int] NULL,
	[ReferralCode] [int] NULL,
 CONSTRAINT [PK_aarp_call_log] PRIMARY KEY CLUSTERED 
(
	[oID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO

