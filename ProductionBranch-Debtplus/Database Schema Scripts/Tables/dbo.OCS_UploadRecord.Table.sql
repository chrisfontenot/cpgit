EXECUTE UPDATE_ADD_COLUMN 'OCS_UploadRecord', 'phone7'
EXECUTE UPDATE_ADD_COLUMN 'OCS_UploadRecord', 'phone8'
EXECUTE UPDATE_ADD_COLUMN 'OCS_UploadRecord', 'phone9'
EXECUTE UPDATE_ADD_COLUMN 'OCS_UploadRecord', 'phone10'
EXECUTE UPDATE_ADD_COLUMN 'OCS_UploadRecord', 'phone11'
EXECUTE UPDATE_ADD_COLUMN 'OCS_UploadRecord', 'phone12'
EXECUTE UPDATE_ADD_COLUMN 'OCS_UploadRecord', 'status'
EXECUTE UPDATE_ADD_COLUMN 'OCS_UploadRecord', 'casetype'
EXECUTE UPDATE_DROP_CONSTRAINTS 'OCS_UploadRecord'
GO
/* To prevent any potential data loss issues, you should review this script in detail before running it outside the context of the database designer.*/
BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION
GO
CREATE TABLE dbo.Tmp_OCS_UploadRecord
	(
	Id int NOT NULL IDENTITY (1, 1),
	Uploaded bit NOT NULL,
	IsDuplicate bit NULL,
	UploadAttempt uniqueidentifier NULL,
	UploadDate datetime NULL,
	Servicer varchar(50) NULL,
	LoanNumber varchar(50) NULL,
	LastName1 varchar(50) NULL,
	FirstName1 varchar(50) NULL,
	SSN1 varchar(11) NULL,
	LastName2 varchar(50) NULL,
	FirstName2 varchar(50) NULL,
	SSN2 varchar(11) NULL,
	ClientStreet varchar(50) NULL,
	ClientCity varchar(50) NULL,
	ClientState varchar(50) NULL,
	ClientZipcode varchar(10) NULL,
	Phone1 varchar(21) NULL,
	Phone2 varchar(21) NULL,
	Phone3 varchar(21) NULL,
	Phone4 varchar(21) NULL,
	Phone5 varchar(21) NULL,
	Phone6 varchar(21) NULL,
	phone7 varchar(21) NULL,
	phone8 varchar(21) NULL,
	phone9 varchar(21) NULL,
	phone10 varchar(21) NULL,
	phone11 varchar(21) NULL,
	phone12 varchar(21) NULL,
	casetype varchar(50) NULL,
	STATUS varchar(50) NULL,
	InvestorNumber varchar(50) NULL,
	InvestorLastChanceList datetime NULL,
	InvestorActualSaleDate datetime NULL,
	InvestorDueDate datetime NULL,
	Program int NULL,
	Description varchar(MAX) NULL,
	TrialPeriodStartDate datetime NULL,
	TrialMod_Type varchar(50) NULL,
	Referral_Date datetime NULL,
	Next_Payment_Due_Date datetime NULL,
	Last_Payment_Applied_Date datetime NULL
	)  ON [PRIMARY]
	 TEXTIMAGE_ON [PRIMARY]
GO
GRANT DELETE ON dbo.Tmp_OCS_UploadRecord TO public  AS dbo
GO
GRANT INSERT ON dbo.Tmp_OCS_UploadRecord TO public  AS dbo
GO
GRANT SELECT ON dbo.Tmp_OCS_UploadRecord TO public  AS dbo
GO
GRANT UPDATE ON dbo.Tmp_OCS_UploadRecord TO public  AS dbo
GO
SET IDENTITY_INSERT dbo.Tmp_OCS_UploadRecord ON
GO
IF EXISTS(SELECT * FROM dbo.OCS_UploadRecord)
	 EXEC('INSERT INTO dbo.Tmp_OCS_UploadRecord (Id, Uploaded, IsDuplicate, UploadAttempt, UploadDate, Servicer, LoanNumber, LastName1, FirstName1, SSN1, LastName2, FirstName2, SSN2, ClientStreet, ClientCity, ClientState, ClientZipcode, Phone1, Phone2, Phone3, Phone4, Phone5, Phone6, InvestorNumber, InvestorLastChanceList, InvestorActualSaleDate, InvestorDueDate, Program, Description, TrialPeriodStartDate, TrialMod_Type, Referral_Date, Next_Payment_Due_Date, Last_Payment_Applied_Date, casetype, STATUS, phone7, phone8, phone9, phone10, phone11, phone12)
		SELECT Id, Uploaded, IsDuplicate, UploadAttempt, UploadDate, Servicer, LoanNumber, LastName1, FirstName1, SSN1, LastName2, FirstName2, SSN2, ClientStreet, ClientCity, ClientState, ClientZipcode, Phone1, Phone2, Phone3, Phone4, Phone5, Phone6, InvestorNumber, InvestorLastChanceList, InvestorActualSaleDate, InvestorDueDate, Program, Description, TrialPeriodStartDate, TrialMod_Type, Referral_Date, Next_Payment_Due_Date, Last_Payment_Applied_Date, casetype, STATUS, phone7, phone8, phone9, phone10, phone11, phone12 FROM dbo.OCS_UploadRecord WITH (HOLDLOCK TABLOCKX)')
GO
SET IDENTITY_INSERT dbo.Tmp_OCS_UploadRecord OFF
GO
DROP TABLE dbo.OCS_UploadRecord
GO
EXECUTE sp_rename N'dbo.Tmp_OCS_UploadRecord', N'OCS_UploadRecord', 'OBJECT' 
GO
ALTER TABLE dbo.OCS_UploadRecord ADD CONSTRAINT
	PK_OCS_FMAC_EI_Upload_Record PRIMARY KEY CLUSTERED 
	(
	Id
	) WITH( STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
DENY DELETE ON dbo.OCS_UploadRecord TO www_role  AS dbo 
GO
DENY INSERT ON dbo.OCS_UploadRecord TO www_role  AS dbo 
GO
DENY SELECT ON dbo.OCS_UploadRecord TO www_role  AS dbo 
GO
DENY UPDATE ON dbo.OCS_UploadRecord TO www_role  AS dbo 
GO
COMMIT
GO
