/* To prevent any potential data loss issues, you should review this script in detail before running it outside the context of the database designer.*/
BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
GO
EXECUTE UPDATE_drop_constraints 'Housing_HUD_ImpactTypes'
GO
BEGIN TRANSACTION
GO
CREATE TABLE dbo.Tmp_Housing_HUD_ImpactTypes
	(
	oID dbo.typ_key NOT NULL IDENTITY (1, 1),
	description varchar(4096) NOT NULL,
	ActiveFlag bit NOT NULL,
	date_created dbo.typ_date NOT NULL,
	created_by dbo.typ_counselor NOT NULL,
	ts timestamp NOT NULL
	)  ON [PRIMARY]
GO
EXECUTE sp_bindefault N'dbo.default_true', N'dbo.Tmp_Housing_HUD_ImpactTypes.ActiveFlag'
GO
SET IDENTITY_INSERT dbo.Tmp_Housing_HUD_ImpactTypes ON
GO
IF EXISTS(SELECT * FROM sysobjects WHERE type='U' AND name=N'Housing_HUD_ImpactTypes')
BEGIN
	EXEC('INSERT INTO dbo.Tmp_Housing_HUD_ImpactTypes (oID, description, ActiveFlag, date_created, created_by)
		SELECT oID, description, ActiveFlag, date_created, created_by FROM dbo.Housing_HUD_ImpactTypes WITH (HOLDLOCK TABLOCKX)')
	EXEC('DROP TABLE dbo.Housing_HUD_ImpactTypes')
END
GO
SET IDENTITY_INSERT dbo.Tmp_Housing_HUD_ImpactTypes OFF
GO
EXECUTE sp_rename N'dbo.Tmp_Housing_HUD_ImpactTypes', N'Housing_HUD_ImpactTypes', 'OBJECT' 
GO
ALTER TABLE dbo.Housing_HUD_ImpactTypes ADD CONSTRAINT
	PK_Housing_HUD_ImpactTypes PRIMARY KEY CLUSTERED 
	(
	oID
	) WITH( STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
ALTER TABLE dbo.Housing_ImpactResultTypes ADD CONSTRAINT
	FK_Housing_ImpactResultTypes_Housing_ImpactResultTypes1 FOREIGN KEY
	(
	ImpactType
	) REFERENCES dbo.Housing_HUD_ImpactTypes
	(
	oID
	) ON UPDATE  NO ACTION 
	 ON DELETE  CASCADE 
GO
ALTER TABLE dbo.Housing_Client_Impacts ADD CONSTRAINT
	FK_Housing_Client_Impacts_Housing_HUD_ImpactTypes FOREIGN KEY
	(
	ImpactId
	) REFERENCES dbo.Housing_HUD_ImpactTypes
	(
	oID
	) ON UPDATE  NO ACTION 
	 ON DELETE  CASCADE 
GO
COMMIT
