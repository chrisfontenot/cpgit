/* To prevent any potential data loss issues, you should review this script in detail before running it outside the context of the database designer.*/
BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION
GO
EXECUTE UPDATE_drop_constraints 'creditors'
EXECUTE UPDATE_add_column 'creditors', 'max_amt_per_check', 'money'
EXECUTE UPDATE_add_column 'creditors', 'max_amount_per_check', 'money'
GO
CREATE TABLE dbo.Tmp_creditors
	(
	creditor dbo.typ_creditor NOT NULL,
	creditor_id int NOT NULL IDENTITY (100, 1),
	type varchar(4) NULL,
	sic dbo.typ_sic NULL,
	creditor_name varchar(255) NOT NULL,
	comment varchar(80) NULL,
	division varchar(255) NULL,
	creditor_class dbo.typ_key NOT NULL,
	voucher_spacing int NOT NULL,
	mail_priority int NOT NULL,
	payment_balance dbo.typ_payment_balance NOT NULL,
	prohibit_use bit NOT NULL,
	full_disclosure bit NOT NULL,
	suppress_invoice bit NOT NULL,
	proposal_budget_info bit NOT NULL,
	proposal_income_info bit NOT NULL,
	contrib_cycle dbo.typ_contrib_cycle NOT NULL,
	contrib_bill_month int NOT NULL,
	pledge_amt money NULL,
	pledge_cycle dbo.typ_contrib_cycle NOT NULL,
	pledge_bill_month int NOT NULL,
	min_accept_amt money NULL,
	min_accept_pct dbo.typ_fairshare_rate NULL,
	min_accept_per_bill money NULL,
	lowest_apr_pct dbo.typ_fairshare_rate NOT NULL,
	medium_apr_pct dbo.typ_fairshare_rate NOT NULL,
	highest_apr_pct dbo.typ_fairshare_rate NOT NULL,
	medium_apr_amt money NOT NULL,
	highest_apr_amt money NOT NULL,
	max_clients_per_check int NULL,
	max_amt_per_check money NULL,
	max_fairshare_per_debt money NULL,
	chks_per_invoice int NULL,
	returned_mail datetime NULL,
	po_number varchar(50) NULL,
	usual_priority int NOT NULL,
	percent_balance float(53) NOT NULL,
	distrib_mtd money NOT NULL,
	distrib_ytd money NOT NULL,
	contrib_mtd_billed money NOT NULL,
	contrib_ytd_billed money NOT NULL,
	contrib_mtd_received money NOT NULL,
	contrib_ytd_received money NOT NULL,
	first_payment dbo.typ_key NULL,
	last_payment dbo.typ_key NULL,
	acceptance_days int NULL,
	check_payments int NOT NULL,
	check_bank dbo.typ_key NOT NULL,
	creditor_contribution_pct dbo.typ_key NULL,
	date_created dbo.typ_date NOT NULL,
	created_by dbo.typ_counselor NOT NULL,
	ts timestamp NOT NULL,
	max_amount_per_check money NULL
	)  ON [PRIMARY]
GO
GRANT DELETE ON dbo.Tmp_creditors TO public  AS dbo
GO
GRANT INSERT ON dbo.Tmp_creditors TO public  AS dbo
GO
GRANT SELECT ON dbo.Tmp_creditors TO public  AS dbo
GO
GRANT UPDATE ON dbo.Tmp_creditors TO public  AS dbo
GO
DECLARE @v sql_variant 
SET @v = N'This table holds all of the ''top-level'' creditor information.'
EXECUTE sp_addextendedproperty N'MS_Description', @v, N'SCHEMA', N'dbo', N'TABLE', N'Tmp_creditors', NULL, NULL
GO
DECLARE @v sql_variant 
SET @v = N'Creditor ID in the system'
EXECUTE sp_addextendedproperty N'MS_Description', @v, N'SCHEMA', N'dbo', N'TABLE', N'Tmp_creditors', N'COLUMN', N'creditor'
GO
DECLARE @v sql_variant 
SET @v = N'Standard Industry Code for the creditor'
EXECUTE sp_addextendedproperty N'MS_Description', @v, N'SCHEMA', N'dbo', N'TABLE', N'Tmp_creditors', N'COLUMN', N'sic'
GO
DECLARE @v sql_variant 
SET @v = N'Name of the creditor'
EXECUTE sp_addextendedproperty N'MS_Description', @v, N'SCHEMA', N'dbo', N'TABLE', N'Tmp_creditors', N'COLUMN', N'creditor_name'
GO
DECLARE @v sql_variant 
SET @v = N'A general purpose comment field for the creditor'
EXECUTE sp_addextendedproperty N'MS_Description', @v, N'SCHEMA', N'dbo', N'TABLE', N'Tmp_creditors', N'COLUMN', N'comment'
GO
DECLARE @v sql_variant 
SET @v = N'If the creditor has a division name, it is stored here'
EXECUTE sp_addextendedproperty N'MS_Description', @v, N'SCHEMA', N'dbo', N'TABLE', N'Tmp_creditors', N'COLUMN', N'division'
GO
DECLARE @v sql_variant 
SET @v = N'Pointer to the creditor_classes table'
EXECUTE sp_addextendedproperty N'MS_Description', @v, N'SCHEMA', N'dbo', N'TABLE', N'Tmp_creditors', N'COLUMN', N'creditor_class'
GO
DECLARE @v sql_variant 
SET @v = N'How to space the voucher (0 = single, 1 = double, 2 = suppress voucher)'
EXECUTE sp_addextendedproperty N'MS_Description', @v, N'SCHEMA', N'dbo', N'TABLE', N'Tmp_creditors', N'COLUMN', N'voucher_spacing'
GO
DECLARE @v sql_variant 
SET @v = N'Priority level for generating letters to the creditor'
EXECUTE sp_addextendedproperty N'MS_Description', @v, N'SCHEMA', N'dbo', N'TABLE', N'Tmp_creditors', N'COLUMN', N'mail_priority'
GO
DECLARE @v sql_variant 
SET @v = N'Is the minimum percentage based upon the payment or the balance?'
EXECUTE sp_addextendedproperty N'MS_Description', @v, N'SCHEMA', N'dbo', N'TABLE', N'Tmp_creditors', N'COLUMN', N'payment_balance'
GO
DECLARE @v sql_variant 
SET @v = N'Is the creditor invalid for use in creating new debts?'
EXECUTE sp_addextendedproperty N'MS_Description', @v, N'SCHEMA', N'dbo', N'TABLE', N'Tmp_creditors', N'COLUMN', N'prohibit_use'
GO
DECLARE @v sql_variant 
SET @v = N'Should proposals be full disclosure or not?'
EXECUTE sp_addextendedproperty N'MS_Description', @v, N'SCHEMA', N'dbo', N'TABLE', N'Tmp_creditors', N'COLUMN', N'full_disclosure'
GO
DECLARE @v sql_variant 
SET @v = N'Do you want to suppress the generation of invoices for this billed creditor?'
EXECUTE sp_addextendedproperty N'MS_Description', @v, N'SCHEMA', N'dbo', N'TABLE', N'Tmp_creditors', N'COLUMN', N'suppress_invoice'
GO
DECLARE @v sql_variant 
SET @v = N'Should we include budget information on proposals to this creditor?'
EXECUTE sp_addextendedproperty N'MS_Description', @v, N'SCHEMA', N'dbo', N'TABLE', N'Tmp_creditors', N'COLUMN', N'proposal_budget_info'
GO
DECLARE @v sql_variant 
SET @v = N'Send income information on a paper full disclosure proposal letter?'
EXECUTE sp_addextendedproperty N'MS_Description', @v, N'SCHEMA', N'dbo', N'TABLE', N'Tmp_creditors', N'COLUMN', N'proposal_income_info'
GO
DECLARE @v sql_variant 
SET @v = N'Billing cycle for the creditor'
EXECUTE sp_addextendedproperty N'MS_Description', @v, N'SCHEMA', N'dbo', N'TABLE', N'Tmp_creditors', N'COLUMN', N'contrib_cycle'
GO
DECLARE @v sql_variant 
SET @v = N'If this is not monthly, which month is the bill generated?'
EXECUTE sp_addextendedproperty N'MS_Description', @v, N'SCHEMA', N'dbo', N'TABLE', N'Tmp_creditors', N'COLUMN', N'contrib_bill_month'
GO
DECLARE @v sql_variant 
SET @v = N'If the creditor has a pledge, this is the amount of the pledge.'
EXECUTE sp_addextendedproperty N'MS_Description', @v, N'SCHEMA', N'dbo', N'TABLE', N'Tmp_creditors', N'COLUMN', N'pledge_amt'
GO
DECLARE @v sql_variant 
SET @v = N'How often should the pledge letters be generated?'
EXECUTE sp_addextendedproperty N'MS_Description', @v, N'SCHEMA', N'dbo', N'TABLE', N'Tmp_creditors', N'COLUMN', N'pledge_cycle'
GO
DECLARE @v sql_variant 
SET @v = N'If the pledge cycle is not monthly, this is the month for the pledge letters'
EXECUTE sp_addextendedproperty N'MS_Description', @v, N'SCHEMA', N'dbo', N'TABLE', N'Tmp_creditors', N'COLUMN', N'pledge_bill_month'
GO
DECLARE @v sql_variant 
SET @v = N'Minimum accepted amount of a proposal for this creditor'
EXECUTE sp_addextendedproperty N'MS_Description', @v, N'SCHEMA', N'dbo', N'TABLE', N'Tmp_creditors', N'COLUMN', N'min_accept_amt'
GO
DECLARE @v sql_variant 
SET @v = N'If there is a minimum percentage of the payment/balance'
EXECUTE sp_addextendedproperty N'MS_Description', @v, N'SCHEMA', N'dbo', N'TABLE', N'Tmp_creditors', N'COLUMN', N'min_accept_pct'
GO
DECLARE @v sql_variant 
SET @v = N'Smallest amount to generate on an invoice if the invoice is not one per check'
EXECUTE sp_addextendedproperty N'MS_Description', @v, N'SCHEMA', N'dbo', N'TABLE', N'Tmp_creditors', N'COLUMN', N'min_accept_per_bill'
GO
DECLARE @v sql_variant 
SET @v = N'A.P.R. for the lowest balance'
EXECUTE sp_addextendedproperty N'MS_Description', @v, N'SCHEMA', N'dbo', N'TABLE', N'Tmp_creditors', N'COLUMN', N'lowest_apr_pct'
GO
DECLARE @v sql_variant 
SET @v = N'A.P.R. for the next lowest balance'
EXECUTE sp_addextendedproperty N'MS_Description', @v, N'SCHEMA', N'dbo', N'TABLE', N'Tmp_creditors', N'COLUMN', N'medium_apr_pct'
GO
DECLARE @v sql_variant 
SET @v = N'A.P.R. for the highest balance'
EXECUTE sp_addextendedproperty N'MS_Description', @v, N'SCHEMA', N'dbo', N'TABLE', N'Tmp_creditors', N'COLUMN', N'highest_apr_pct'
GO
DECLARE @v sql_variant 
SET @v = N'The balance that forms the cutoff between the lowest and medium A.P.R. boundaries'
EXECUTE sp_addextendedproperty N'MS_Description', @v, N'SCHEMA', N'dbo', N'TABLE', N'Tmp_creditors', N'COLUMN', N'medium_apr_amt'
GO
DECLARE @v sql_variant 
SET @v = N'The balance that forms the cutoff between the  medium and highest A.P.R. boundaries'
EXECUTE sp_addextendedproperty N'MS_Description', @v, N'SCHEMA', N'dbo', N'TABLE', N'Tmp_creditors', N'COLUMN', N'highest_apr_amt'
GO
DECLARE @v sql_variant 
SET @v = N'Maximum number of clients to print on a check. This overrides the system '
EXECUTE sp_addextendedproperty N'MS_Description', @v, N'SCHEMA', N'dbo', N'TABLE', N'Tmp_creditors', N'COLUMN', N'max_clients_per_check'
GO
DECLARE @v sql_variant 
SET @v = N'Maximum dollar amount to be written on a single check. This overrides the system defaults.'
EXECUTE sp_addextendedproperty N'MS_Description', @v, N'SCHEMA', N'dbo', N'TABLE', N'Tmp_creditors', N'COLUMN', N'max_amt_per_check'
GO
DECLARE @v sql_variant 
SET @v = N'Maximum amount of fairshare per debt per check. "First USA" wants a $200 limit on the fairshare.'
EXECUTE sp_addextendedproperty N'MS_Description', @v, N'SCHEMA', N'dbo', N'TABLE', N'Tmp_creditors', N'COLUMN', N'max_fairshare_per_debt'
GO
DECLARE @v sql_variant 
SET @v = N'Number of checks to be included on an invoice. This overrides the system default. "0" is "unlimited", "1" is a single check per invoice, "2" is at most 2 checks, etc.'
EXECUTE sp_addextendedproperty N'MS_Description', @v, N'SCHEMA', N'dbo', N'TABLE', N'Tmp_creditors', N'COLUMN', N'chks_per_invoice'
GO
DECLARE @v sql_variant 
SET @v = N'If a proposal is returned due to a mailing error, this is the date that the mail was returned. If set, no future proposals/letters are sent to this creditor.'
EXECUTE sp_addextendedproperty N'MS_Description', @v, N'SCHEMA', N'dbo', N'TABLE', N'Tmp_creditors', N'COLUMN', N'returned_mail'
GO
DECLARE @v sql_variant 
SET @v = N'Purchase Order number assigned by the creditor. Printed on all billing data.'
EXECUTE sp_addextendedproperty N'MS_Description', @v, N'SCHEMA', N'dbo', N'TABLE', N'Tmp_creditors', N'COLUMN', N'po_number'
GO
DECLARE @v sql_variant 
SET @v = N'Normal priority for debts created by this creditor. "9" is the lowest. "0" is the highest.'
EXECUTE sp_addextendedproperty N'MS_Description', @v, N'SCHEMA', N'dbo', N'TABLE', N'Tmp_creditors', N'COLUMN', N'usual_priority'
GO
DECLARE @v sql_variant 
SET @v = N'Normal percentage of the balance proposed with this creditor. Used to seed the debt figures.'
EXECUTE sp_addextendedproperty N'MS_Description', @v, N'SCHEMA', N'dbo', N'TABLE', N'Tmp_creditors', N'COLUMN', N'percent_balance'
GO
DECLARE @v sql_variant 
SET @v = N'Total disbursements to this creditor for this month.'
EXECUTE sp_addextendedproperty N'MS_Description', @v, N'SCHEMA', N'dbo', N'TABLE', N'Tmp_creditors', N'COLUMN', N'distrib_mtd'
GO
DECLARE @v sql_variant 
SET @v = N'Total disbursements to this creditor for this year.'
EXECUTE sp_addextendedproperty N'MS_Description', @v, N'SCHEMA', N'dbo', N'TABLE', N'Tmp_creditors', N'COLUMN', N'distrib_ytd'
GO
DECLARE @v sql_variant 
SET @v = N'Total amount billed to this creditor for this month.'
EXECUTE sp_addextendedproperty N'MS_Description', @v, N'SCHEMA', N'dbo', N'TABLE', N'Tmp_creditors', N'COLUMN', N'contrib_mtd_billed'
GO
DECLARE @v sql_variant 
SET @v = N'Total amount billed to this creditor for the current year.'
EXECUTE sp_addextendedproperty N'MS_Description', @v, N'SCHEMA', N'dbo', N'TABLE', N'Tmp_creditors', N'COLUMN', N'contrib_ytd_billed'
GO
DECLARE @v sql_variant 
SET @v = N'Total amount received (both deducted and billed payments) for this creditor this month.'
EXECUTE sp_addextendedproperty N'MS_Description', @v, N'SCHEMA', N'dbo', N'TABLE', N'Tmp_creditors', N'COLUMN', N'contrib_mtd_received'
GO
DECLARE @v sql_variant 
SET @v = N'Total amount received (both deducted and billed payments) for this creditor this year.'
EXECUTE sp_addextendedproperty N'MS_Description', @v, N'SCHEMA', N'dbo', N'TABLE', N'Tmp_creditors', N'COLUMN', N'contrib_ytd_received'
GO
DECLARE @v sql_variant 
SET @v = N'Pointer, in registers_creditor, of the first payment to this creditor.'
EXECUTE sp_addextendedproperty N'MS_Description', @v, N'SCHEMA', N'dbo', N'TABLE', N'Tmp_creditors', N'COLUMN', N'first_payment'
GO
DECLARE @v sql_variant 
SET @v = N'Pointer, in registers_creditor, of the last payment to this creditor.'
EXECUTE sp_addextendedproperty N'MS_Description', @v, N'SCHEMA', N'dbo', N'TABLE', N'Tmp_creditors', N'COLUMN', N'last_payment'
GO
DECLARE @v sql_variant 
SET @v = N'Number of days that a proposal is allowed to be outstanding until it is accepted by default. This overrides the system value.'
EXECUTE sp_addextendedproperty N'MS_Description', @v, N'SCHEMA', N'dbo', N'TABLE', N'Tmp_creditors', N'COLUMN', N'acceptance_days'
GO
DECLARE @v sql_variant 
SET @v = N'Number of payments made by check before starting to use EFT'
EXECUTE sp_addextendedproperty N'MS_Description', @v, N'SCHEMA', N'dbo', N'TABLE', N'Tmp_creditors', N'COLUMN', N'check_payments'
GO
DECLARE @v sql_variant 
SET @v = N'If a check is required for the creditor, which bank account is to be used?'
EXECUTE sp_addextendedproperty N'MS_Description', @v, N'SCHEMA', N'dbo', N'TABLE', N'Tmp_creditors', N'COLUMN', N'check_bank'
GO
DECLARE @v sql_variant 
SET @v = N'Pointer to the current contribution record for the creditor'
EXECUTE sp_addextendedproperty N'MS_Description', @v, N'SCHEMA', N'dbo', N'TABLE', N'Tmp_creditors', N'COLUMN', N'creditor_contribution_pct'
GO
DECLARE @v sql_variant 
SET @v = N'Date/Time when the row was created.'
EXECUTE sp_addextendedproperty N'MS_Description', @v, N'SCHEMA', N'dbo', N'TABLE', N'Tmp_creditors', N'COLUMN', N'date_created'
GO
DECLARE @v sql_variant 
SET @v = N'Person who created the row.'
EXECUTE sp_addextendedproperty N'MS_Description', @v, N'SCHEMA', N'dbo', N'TABLE', N'Tmp_creditors', N'COLUMN', N'created_by'
GO
EXECUTE sp_bindefault N'dbo.default_zero', N'dbo.Tmp_creditors.creditor_class'
GO
EXECUTE sp_bindefault N'dbo.default_zero', N'dbo.Tmp_creditors.voucher_spacing'
GO
EXECUTE sp_bindefault N'dbo.default_priority', N'dbo.Tmp_creditors.mail_priority'
GO
EXECUTE sp_bindefault N'dbo.default_false', N'dbo.Tmp_creditors.prohibit_use'
GO
EXECUTE sp_bindefault N'dbo.default_false', N'dbo.Tmp_creditors.full_disclosure'
GO
EXECUTE sp_bindefault N'dbo.default_false', N'dbo.Tmp_creditors.suppress_invoice'
GO
EXECUTE sp_bindefault N'dbo.default_false', N'dbo.Tmp_creditors.proposal_budget_info'
GO
EXECUTE sp_bindefault N'dbo.default_false', N'dbo.Tmp_creditors.proposal_income_info'
GO
EXECUTE sp_bindefault N'dbo.default_money', N'dbo.Tmp_creditors.medium_apr_amt'
GO
EXECUTE sp_bindefault N'dbo.default_money', N'dbo.Tmp_creditors.highest_apr_amt'
GO
EXECUTE sp_bindefault N'dbo.default_priority', N'dbo.Tmp_creditors.usual_priority'
GO
ALTER TABLE dbo.Tmp_creditors ADD CONSTRAINT
	DF_creditors_percent_balance DEFAULT ((100)) FOR percent_balance
GO
EXECUTE sp_bindefault N'dbo.default_money', N'dbo.Tmp_creditors.distrib_mtd'
GO
EXECUTE sp_bindefault N'dbo.default_money', N'dbo.Tmp_creditors.distrib_ytd'
GO
EXECUTE sp_bindefault N'dbo.default_money', N'dbo.Tmp_creditors.contrib_mtd_billed'
GO
EXECUTE sp_bindefault N'dbo.default_money', N'dbo.Tmp_creditors.contrib_ytd_billed'
GO
EXECUTE sp_bindefault N'dbo.default_money', N'dbo.Tmp_creditors.contrib_mtd_received'
GO
EXECUTE sp_bindefault N'dbo.default_money', N'dbo.Tmp_creditors.contrib_ytd_received'
GO
EXECUTE sp_bindefault N'dbo.default_zero', N'dbo.Tmp_creditors.check_payments'
GO
EXECUTE sp_bindefault N'dbo.default_one', N'dbo.Tmp_creditors.check_bank'
GO
SET IDENTITY_INSERT dbo.Tmp_creditors ON
GO
EXECUTE sp_unbindrule N'dbo.Tmp_creditors.creditor'
GO
EXECUTE sp_unbindrule N'dbo.Tmp_creditors.payment_balance'
GO
EXECUTE sp_unbindrule N'dbo.Tmp_creditors.contrib_cycle'
GO
EXECUTE sp_unbindrule N'dbo.Tmp_creditors.pledge_cycle'
GO
EXECUTE sp_unbindrule N'dbo.Tmp_creditors.min_accept_pct'
GO
EXECUTE sp_unbindrule N'dbo.Tmp_creditors.lowest_apr_pct'
GO
EXECUTE sp_unbindrule N'dbo.Tmp_creditors.medium_apr_pct'
GO
EXECUTE sp_unbindrule N'dbo.Tmp_creditors.highest_apr_pct'
GO
IF EXISTS(SELECT * FROM dbo.creditors)
	 EXEC('INSERT INTO dbo.Tmp_creditors (creditor, creditor_id, type, sic, creditor_name, comment, division, creditor_class, voucher_spacing, mail_priority, payment_balance, prohibit_use, full_disclosure, suppress_invoice, proposal_budget_info, proposal_income_info, contrib_cycle, contrib_bill_month, pledge_amt, pledge_cycle, pledge_bill_month, min_accept_amt, min_accept_pct, min_accept_per_bill, lowest_apr_pct, medium_apr_pct, highest_apr_pct, medium_apr_amt, highest_apr_amt, max_clients_per_check, max_amt_per_check, max_fairshare_per_debt, chks_per_invoice, returned_mail, po_number, usual_priority, percent_balance, distrib_mtd, distrib_ytd, contrib_mtd_billed, contrib_ytd_billed, contrib_mtd_received, contrib_ytd_received, first_payment, last_payment, acceptance_days, check_payments, check_bank, creditor_contribution_pct, date_created, created_by, max_amount_per_check)
		SELECT creditor, creditor_id, type, sic, creditor_name, comment, division, creditor_class, voucher_spacing, mail_priority, payment_balance, prohibit_use, full_disclosure, suppress_invoice, proposal_budget_info, proposal_income_info, contrib_cycle, contrib_bill_month, pledge_amt, pledge_cycle, pledge_bill_month, min_accept_amt, min_accept_pct, min_accept_per_bill, lowest_apr_pct, medium_apr_pct, highest_apr_pct, medium_apr_amt, highest_apr_amt, max_clients_per_check, convert(money,max_amount_per_check) as max_amt_per_check, max_fairshare_per_debt, chks_per_invoice, returned_mail, po_number, usual_priority, percent_balance, distrib_mtd, distrib_ytd, contrib_mtd_billed, contrib_ytd_billed, contrib_mtd_received, contrib_ytd_received, first_payment, last_payment, acceptance_days, check_payments, check_bank, creditor_contribution_pct, date_created, created_by, max_amount_per_check FROM dbo.creditors WITH (HOLDLOCK TABLOCKX)')
GO
SET IDENTITY_INSERT dbo.Tmp_creditors OFF
GO
DROP TABLE dbo.creditors
GO
EXECUTE sp_rename N'dbo.Tmp_creditors', N'creditors', 'OBJECT' 
GO
ALTER TABLE dbo.creditors ADD CONSTRAINT
	PK_creditors PRIMARY KEY CLUSTERED 
	(
	creditor
	) WITH( PAD_INDEX = OFF, FILLFACTOR = 90, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX IX_creditors_1 ON dbo.creditors
	(
	creditor_name
	) WITH( PAD_INDEX = OFF, FILLFACTOR = 90, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX IX_creditors_2 ON dbo.creditors
	(
	sic
	) WITH( PAD_INDEX = OFF, FILLFACTOR = 90, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
CREATE UNIQUE NONCLUSTERED INDEX IX_creditors_4 ON dbo.creditors
	(
	creditor_id
	) WITH( PAD_INDEX = OFF, FILLFACTOR = 90, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX IX_creditors_3 ON dbo.creditors
	(
	type
	) WITH( PAD_INDEX = ON, FILLFACTOR = 90, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
EXECUTE sp_bindrule N'dbo.valid_creditor', N'dbo.creditors.creditor'
GO
EXECUTE sp_bindrule N'dbo.valid_voucher_spacing', N'dbo.creditors.voucher_spacing'
GO
EXECUTE sp_bindrule N'dbo.valid_payment_balance', N'dbo.creditors.payment_balance'
GO
EXECUTE sp_bindrule N'dbo.valid_contrib_cycle', N'dbo.creditors.contrib_cycle'
GO
EXECUTE sp_bindrule N'dbo.valid_contrib_month', N'dbo.creditors.contrib_bill_month'
GO
EXECUTE sp_bindrule N'dbo.valid_contrib_cycle', N'dbo.creditors.pledge_cycle'
GO
EXECUTE sp_bindrule N'dbo.valid_contrib_month', N'dbo.creditors.pledge_bill_month'
GO
EXECUTE sp_bindrule N'dbo.valid_fairshare_rate', N'dbo.creditors.min_accept_pct'
GO
EXECUTE sp_bindrule N'dbo.valid_fairshare_rate', N'dbo.creditors.lowest_apr_pct'
GO
EXECUTE sp_bindrule N'dbo.valid_fairshare_rate', N'dbo.creditors.medium_apr_pct'
GO
EXECUTE sp_bindrule N'dbo.valid_fairshare_rate', N'dbo.creditors.highest_apr_pct'
GO
CREATE trigger [dbo].[trig_creditors_D] on dbo.creditors after delete as
-- ==============================================================================
-- ==          Clean up references from the creditors to other tables          ==
-- ==============================================================================
set nocount on

delete	creditor_addkeys
from	creditor_addkeys i
inner join deleted d on i.creditor = d.creditor;

delete	creditor_addresses
from	creditor_addresses i
inner join deleted d on i.creditor = d.creditor;

delete	creditor_contacts
from	creditor_contacts i
inner join deleted d on i.creditor = d.creditor;

delete  creditor_contribution_pcts
from    creditor_contribution_pcts i
inner join deleted d on i.creditor = d.creditor;

delete  creditor_drop_reasons
from    creditor_drop_reasons i
inner join deleted d on i.creditor = d.creditor;

delete  creditor_methods
from    creditor_methods i
inner join deleted d on i.creditor = d.creditor_id;

delete	creditor_notes
from	creditor_notes i
inner join deleted d on i.creditor = d.creditor;

delete	creditor_statistics
from	creditor_statistics i
inner join deleted d on i.creditor = d.creditor;

delete	creditor_www
from	creditor_www i
inner join deleted d on i.creditor = d.creditor;

delete	creditor_www_notes
from	creditor_www_notes i
inner join deleted d on i.creditor = d.creditor;

delete	registers_creditor
from	registers_creditor i
inner join deleted d on i.creditor = d.creditor;

delete	registers_non_ar
from	registers_non_ar i
inner join deleted d on i.creditor = d.creditor
where	i.creditor is not null;

delete	client_creditor
from	client_creditor i
inner join deleted d on i.creditor = d.creditor
where	i.creditor is not null;
GO
DENY DELETE ON dbo.creditors TO www_role  AS dbo 
GO
DENY INSERT ON dbo.creditors TO www_role  AS dbo 
GO
DENY SELECT ON dbo.creditors TO www_role  AS dbo 
GO
DENY UPDATE ON dbo.creditors TO www_role  AS dbo 
GO
COMMIT
GO
