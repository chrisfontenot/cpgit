USE [DebtPlus]
GO
/****** Object:  Table [dbo].[ClearPoint_6Month_Client_Letters]    Script Date: 09/15/2014 13:13:25 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[ClearPoint_6Month_Client_Letters](
	[client] [int] NULL,
	[batch] [varchar](50) NULL,
	[date_sent] [datetime] NULL,
	[next_date_to_send] [datetime] NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
