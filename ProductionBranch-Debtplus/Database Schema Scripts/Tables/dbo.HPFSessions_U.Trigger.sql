SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER TRIGGER [HPFSessions_U] on [HPFSessions] AFTER UPDATE AS
BEGIN
	-- ======================================================================================
	-- ==             Record the ID and Timestamp of the last update to the rows           ==
	-- ======================================================================================

	-- Suppress intermediate results
	set nocount on

	-- Set the ID and date when the row was last updated unless the update includes that information
	if not update(date_changed) AND not update(changed_by)
		update	HPFSessions
		set		date_changed	= getdate(),
				changed_by		= suser_sname()
		from	HPFSessions p
		inner join inserted i on p.oID = i.oID;
END
GO
