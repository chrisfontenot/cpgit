/* To prevent any potential data loss issues, you should review this script in detail before running it outside the context of the database designer.*/
BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
EXECUTE UPDATE_drop_constraints 'Housing_Client_Impacts'
GO
BEGIN TRANSACTION
GO
CREATE TABLE dbo.Tmp_Housing_Client_Impacts
	(
	oID dbo.typ_key NOT NULL IDENTITY (1, 1),
	POVId dbo.typ_key NOT NULL,
	ImpactId dbo.typ_key NOT NULL,
	ts timestamp NOT NULL
	)  ON [PRIMARY]
GO
GRANT SELECT,UPDATE,INSERT,DELETE ON dbo.Housing_Client_Impacts TO public AS dbo
GO
SET IDENTITY_INSERT dbo.Tmp_Housing_Client_Impacts ON
GO
IF EXISTS(SELECT * FROM sysobjects where type = 'U' and name = N'Housing_Client_Impacts')
BEGIN
	EXEC('INSERT INTO dbo.Tmp_Housing_Client_Impacts (oID, POVId, ImpactId)
		SELECT oID, POVId, ImpactId FROM dbo.Housing_Client_Impacts WITH (HOLDLOCK TABLOCKX)')
	EXEC('DROP TABLE dbo.Housing_Client_Impacts')
END
GO
SET IDENTITY_INSERT dbo.Tmp_Housing_Client_Impacts OFF
GO
EXECUTE sp_rename N'dbo.Tmp_Housing_Client_Impacts', N'Housing_Client_Impacts', 'OBJECT' 
GO
ALTER TABLE dbo.Housing_Client_Impacts ADD CONSTRAINT
	PK_Housing_Client_Impacts PRIMARY KEY CLUSTERED 
	(
	oID
	) WITH( STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX IX_Housing_Client_Impacts ON dbo.Housing_Client_Impacts
	(
	POVId
	) WITH( STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX IX_Table_2 ON dbo.Housing_Client_Impacts
	(
	ImpactId
	) WITH( STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
ALTER TABLE dbo.Housing_Client_Impacts ADD CONSTRAINT
	FK_Housing_Client_Impacts_hud_interviews FOREIGN KEY
	(
	POVId
	) REFERENCES dbo.hud_interviews
	(
	hud_interview
	) ON UPDATE  NO ACTION 
	 ON DELETE  CASCADE 
GO
ALTER TABLE dbo.Housing_Client_Impacts ADD CONSTRAINT
	FK_Housing_Client_Impacts_Housing_HUD_ImpactTypes FOREIGN KEY
	(
	ImpactId
	) REFERENCES dbo.Housing_HUD_ImpactTypes
	(
	oID
	) ON UPDATE  NO ACTION 
	 ON DELETE  CASCADE 
GO
COMMIT
GO
DENY SELECT,UPDATE,INSERT,DELETE ON dbo.Housing_Client_Impacts TO www_role
GO