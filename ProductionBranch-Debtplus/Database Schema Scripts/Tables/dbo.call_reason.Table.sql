USE [DEBTPLUS]
GO

/****** Object:  Table [dbo].[call_reason]    Script Date: 2/9/2015 9:09:17 AM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[call_reason](
	[oID] [dbo].[typ_key] IDENTITY(1,1) NOT NULL,
	[description] [dbo].[typ_description] NOT NULL,
	[nfcc] [varchar](4) NULL,
	[rpps] [varchar](4) NULL,
	[epay] [varchar](4) NULL,
	[note] [dbo].[typ_message] NULL,
	[hud_9902_section] [varchar](256) NULL,
	[default] [bit] NOT NULL CONSTRAINT [DF_call_reason_default]  DEFAULT ((0)),
	[sortorder] [int] NOT NULL,
	[ActiveFlag] [bit] NOT NULL CONSTRAINT [DF_call_reason_ActiveFlag]  DEFAULT ((1)),
	[date_created] [dbo].[typ_date] NOT NULL,
	[created_by] [dbo].[typ_counselor] NOT NULL,
	[ts] [timestamp] NOT NULL,
 CONSTRAINT [PK_call_reason] PRIMARY KEY CLUSTERED 
(
	[oID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO




USE [DEBTPLUS]
GO

INSERT INTO [dbo].[call_reason]([description],[sortorder],[date_created],[created_by]) VALUES ('Schedule a New Appointment', 1, getdate(), 'sa')
INSERT INTO [dbo].[call_reason]([description],[sortorder],[date_created],[created_by]) VALUES ('Cancel an Appointment', 2, getdate(), 'sa')
INSERT INTO [dbo].[call_reason]([description],[sortorder],[date_created],[created_by]) VALUES ('Reschedule an Existing Appointment', 3, getdate(), 'sa')
INSERT INTO [dbo].[call_reason]([description],[sortorder],[date_created],[created_by]) VALUES ('Ask General Question', 4, getdate(), 'sa')
GO