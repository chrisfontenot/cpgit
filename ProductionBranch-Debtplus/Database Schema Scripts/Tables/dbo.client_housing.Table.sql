/* To prevent any potential data loss issues, you should review this script in detail before running it outside the context of the database designer.*/
BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION
GO
execute UPDATE_drop_constraints 'client_housing'
execute UPDATE_add_column 'client_housing', 'FrontEnd_DTI', 'float'
execute UPDATE_add_column 'client_housing', 'BackEnd_DTI', 'float'
execute UPDATE_add_column 'client_housing', 'DSA_ProgramBenefit', 'bit'
execute UPDATE_add_column 'client_housing', 'DSA_Program', 'int'
execute UPDATE_add_column 'client_housing', 'DSA_NoDsaReason', 'int'
execute UPDATE_add_column 'client_housing', 'DSA_Specialist', 'int'
execute UPDATE_add_column 'client_housing', 'NFMCP_DTI', 'float'
execute UPDATE_add_column 'client_housing', 'housing_type', 'int'
GO
CREATE TABLE dbo.Tmp_client_housing
	(
	client dbo.typ_client NOT NULL,
	housing_status dbo.typ_key NOT NULL,
	HUD_fair_housing bit NOT NULL,
	HUD_colonias bit NOT NULL,
	HUD_migrant_farm_worker bit NOT NULL,
	HUD_predatory_lending bit NOT NULL,
	HUD_FirstTimeHomeBuyer bit NOT NULL,
	HUD_DiscriminationVictim bit NOT NULL,
	HUD_LoanScam bit NOT NULL,
	HUD_Assistance dbo.typ_key NULL,
	OtherHUDFunding money NOT NULL,
	HECM_Certificate_Date datetime NULL,
	HECM_Certificate_Expires datetime NULL,
	HECM_Certificate_ID varchar(10) NULL,
	NFMCP_level varchar(10) NOT NULL,
	NFMCP_privacy_policy bit NOT NULL,
	NFMCP_decline_authorization bit NOT NULL,
	HUD_home_ownership_voucher bit NOT NULL,
	HUD_housing_voucher bit NOT NULL,
	FrontEnd_DTI float(53) NOT NULL,
	BackEnd_DTI float(53) NOT NULL,
	HUD_grant dbo.typ_key NOT NULL,
	DSA_ProgramBenefit bit NULL,
	DSA_Program dbo.typ_key NULL,
	DSA_NoDsaReason dbo.typ_key NULL,
	DSA_CaseID int NULL,
	DSA_Specialist dbo.typ_key NULL,
	ts timestamp NOT NULL,
	NFMCP_DTI float(53) NULL,
	housing_type int NULL
	)  ON [PRIMARY]
GO
GRANT DELETE ON dbo.Tmp_client_housing TO public  AS dbo
GO
GRANT INSERT ON dbo.Tmp_client_housing TO public  AS dbo
GO
GRANT SELECT ON dbo.Tmp_client_housing TO public  AS dbo
GO
GRANT UPDATE ON dbo.Tmp_client_housing TO public  AS dbo
GO
DECLARE @v sql_variant 
SET @v = N'Is this a fair housing issue item?'
EXECUTE sp_addextendedproperty N'MS_Description', @v, N'SCHEMA', N'dbo', N'TABLE', N'Tmp_client_housing', N'COLUMN', N'HUD_fair_housing'
GO
DECLARE @v sql_variant 
SET @v = N'Front End DTI for the client. Does not include housing.'
EXECUTE sp_addextendedproperty N'MS_Description', @v, N'SCHEMA', N'dbo', N'TABLE', N'Tmp_client_housing', N'COLUMN', N'FrontEnd_DTI'
GO
DECLARE @v sql_variant 
SET @v = N'Back End DTI for the client. Includes all debt.'
EXECUTE sp_addextendedproperty N'MS_Description', @v, N'SCHEMA', N'dbo', N'TABLE', N'Tmp_client_housing', N'COLUMN', N'BackEnd_DTI'
GO
ALTER TABLE dbo.Tmp_client_housing ADD CONSTRAINT
	DF_client_housing_housing_status DEFAULT ((4)) FOR housing_status
GO
EXECUTE sp_bindefault N'dbo.default_false', N'dbo.Tmp_client_housing.HUD_fair_housing'
GO
EXECUTE sp_bindefault N'dbo.default_false', N'dbo.Tmp_client_housing.HUD_colonias'
GO
EXECUTE sp_bindefault N'dbo.default_false', N'dbo.Tmp_client_housing.HUD_migrant_farm_worker'
GO
EXECUTE sp_bindefault N'dbo.default_false', N'dbo.Tmp_client_housing.HUD_predatory_lending'
GO
EXECUTE sp_bindefault N'dbo.default_false', N'dbo.Tmp_client_housing.HUD_FirstTimeHomeBuyer'
GO
EXECUTE sp_bindefault N'dbo.default_false', N'dbo.Tmp_client_housing.HUD_DiscriminationVictim'
GO
EXECUTE sp_bindefault N'dbo.default_false', N'dbo.Tmp_client_housing.HUD_LoanScam'
GO
EXECUTE sp_bindefault N'dbo.default_money', N'dbo.Tmp_client_housing.OtherHUDFunding'
GO
EXECUTE sp_bindefault N'dbo.default_string', N'dbo.Tmp_client_housing.HECM_Certificate_ID'
GO
EXECUTE sp_bindefault N'dbo.default_string', N'dbo.Tmp_client_housing.NFMCP_level'
GO
EXECUTE sp_bindefault N'dbo.default_false', N'dbo.Tmp_client_housing.NFMCP_privacy_policy'
GO
EXECUTE sp_bindefault N'dbo.default_false', N'dbo.Tmp_client_housing.NFMCP_decline_authorization'
GO
EXECUTE sp_bindefault N'dbo.default_false', N'dbo.Tmp_client_housing.HUD_home_ownership_voucher'
GO
EXECUTE sp_bindefault N'dbo.default_false', N'dbo.Tmp_client_housing.HUD_housing_voucher'
GO
EXECUTE sp_bindefault N'dbo.default_zero', N'dbo.Tmp_client_housing.FrontEnd_DTI'
GO
EXECUTE sp_bindefault N'dbo.default_zero', N'dbo.Tmp_client_housing.BackEnd_DTI'
GO
EXECUTE sp_bindefault N'dbo.default_one', N'dbo.Tmp_client_housing.HUD_grant'
GO
EXECUTE sp_bindefault N'dbo.default_housing_type', N'dbo.Tmp_client_housing.housing_type'
GO
IF EXISTS(SELECT * FROM sysobjects where name = 'client_housing')
BEGIN
	EXEC('INSERT INTO dbo.Tmp_client_housing (client, housing_status, HUD_fair_housing, HUD_colonias, HUD_migrant_farm_worker, HUD_predatory_lending, HUD_FirstTimeHomeBuyer, HUD_DiscriminationVictim, HUD_LoanScam, HUD_Assistance, OtherHUDFunding, HECM_Certificate_Date, HECM_Certificate_Expires, HECM_Certificate_ID, NFMCP_level, NFMCP_privacy_policy, NFMCP_decline_authorization, HUD_home_ownership_voucher, HUD_housing_voucher, FrontEnd_DTI, BackEnd_DTI, HUD_grant, DSA_ProgramBenefit, DSA_Program, DSA_NoDsaReason, DSA_CaseID, DSA_Specialist, NFMCP_DTI, housing_type)
		SELECT client, housing_status, HUD_fair_housing, HUD_colonias, HUD_migrant_farm_worker, HUD_predatory_lending, HUD_FirstTimeHomeBuyer, HUD_DiscriminationVictim, HUD_LoanScam, HUD_Assistance, OtherHUDFunding, HECM_Certificate_Date, HECM_Certificate_Expires, HECM_Certificate_ID, NFMCP_level, NFMCP_privacy_policy, NFMCP_decline_authorization, HUD_home_ownership_voucher, HUD_housing_voucher, coalesce(FrontEnd_DTI,NFMCP_DTI,0), isnull(BackEnd_DTI,0), HUD_grant, isnull(DSA_ProgramBenefit,0), DSA_Program, DSA_NoDsaReason, DSA_CaseID, DSA_Specialist, NFMCP_DTI, housing_type FROM dbo.client_housing WITH (HOLDLOCK TABLOCKX)')
	DROP TABLE dbo.client_housing
END
GO
EXECUTE sp_rename N'dbo.Tmp_client_housing', N'client_housing', 'OBJECT' 
GO
ALTER TABLE dbo.client_housing ADD CONSTRAINT
	PK_client_housing PRIMARY KEY CLUSTERED 
	(
	client
	) WITH( STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
COMMIT
GO
DENY DELETE ON dbo.client_housing TO www_role  AS dbo 
GO
DENY INSERT ON dbo.client_housing TO www_role  AS dbo 
GO
DENY SELECT ON dbo.client_housing TO www_role  AS dbo 
GO
DENY UPDATE ON dbo.client_housing TO www_role  AS dbo 
GO

-- Discard the columns that are no longer needed
alter table dbo.client_housing drop column NFMCP_DTI
GO
