EXECUTE UPDATE_DROP_CONSTRAINTS 'secured_loans'
GO
/* To prevent any potential data loss issues, you should review this script in detail before running it outside the context of the database designer.*/
BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION
GO
CREATE TABLE dbo.Tmp_secured_loans
	(
	secured_loan dbo.typ_key NOT NULL IDENTITY (1, 1),
	secured_property dbo.typ_key NOT NULL,
	priority int NOT NULL,
	lender dbo.typ_description NOT NULL,
	account_number dbo.typ_description NOT NULL,
	fdic_number varchar(20) NULL,
	original_lender varchar(50) NULL,
	original_account_number varchar(50) NULL,
	original_fdic_number varchar(20) NULL,
	case_number dbo.typ_description NOT NULL,
	interest_rate float(53) NOT NULL,
	payment money NOT NULL,
	original_amount money NOT NULL,
	balance money NOT NULL,
	periods int NOT NULL,
	past_due_amount money NOT NULL,
	past_due_periods int NOT NULL,
	loan_type dbo.typ_key NOT NULL,
	loan_type_description dbo.typ_description NOT NULL,
	arm bit NOT NULL,
	arm_reset bit NOT NULL,
	interest_only bit NOT NULL,
	hybrid_arm bit NOT NULL,
	option_arm bit NOT NULL,
	fha_va_insured bit NOT NULL,
	privately_held bit NOT NULL,
	created_by dbo.typ_counselor NOT NULL,
	date_created dbo.typ_date NOT NULL,
	ts timestamp NOT NULL
	)  ON [PRIMARY]
GO
GRANT DELETE ON dbo.Tmp_secured_loans TO public  AS dbo
GO
GRANT INSERT ON dbo.Tmp_secured_loans TO public  AS dbo
GO
GRANT SELECT ON dbo.Tmp_secured_loans TO public  AS dbo
GO
GRANT UPDATE ON dbo.Tmp_secured_loans TO public  AS dbo
GO
DECLARE @v sql_variant 
SET @v = N'Primary key to the table'
EXECUTE sp_addextendedproperty N'MS_Description', @v, N'SCHEMA', N'dbo', N'TABLE', N'Tmp_secured_loans', N'COLUMN', N'secured_loan'
GO
DECLARE @v sql_variant 
SET @v = N'Pointer to the secured_proprerty for which this is a loan'
EXECUTE sp_addextendedproperty N'MS_Description', @v, N'SCHEMA', N'dbo', N'TABLE', N'Tmp_secured_loans', N'COLUMN', N'secured_property'
GO
DECLARE @v sql_variant 
SET @v = N'Loan priority (first, second, third, line-of-credit, etc.)'
EXECUTE sp_addextendedproperty N'MS_Description', @v, N'SCHEMA', N'dbo', N'TABLE', N'Tmp_secured_loans', N'COLUMN', N'priority'
GO
DECLARE @v sql_variant 
SET @v = N'Name of the lending institution'
EXECUTE sp_addextendedproperty N'MS_Description', @v, N'SCHEMA', N'dbo', N'TABLE', N'Tmp_secured_loans', N'COLUMN', N'lender'
GO
DECLARE @v sql_variant 
SET @v = N'Account number with the lender'
EXECUTE sp_addextendedproperty N'MS_Description', @v, N'SCHEMA', N'dbo', N'TABLE', N'Tmp_secured_loans', N'COLUMN', N'account_number'
GO
DECLARE @v sql_variant 
SET @v = N'Optional case number associated with the loan'
EXECUTE sp_addextendedproperty N'MS_Description', @v, N'SCHEMA', N'dbo', N'TABLE', N'Tmp_secured_loans', N'COLUMN', N'case_number'
GO
DECLARE @v sql_variant 
SET @v = N'Interest rate if known'
EXECUTE sp_addextendedproperty N'MS_Description', @v, N'SCHEMA', N'dbo', N'TABLE', N'Tmp_secured_loans', N'COLUMN', N'interest_rate'
GO
DECLARE @v sql_variant 
SET @v = N'Monthly payment if known'
EXECUTE sp_addextendedproperty N'MS_Description', @v, N'SCHEMA', N'dbo', N'TABLE', N'Tmp_secured_loans', N'COLUMN', N'payment'
GO
DECLARE @v sql_variant 
SET @v = N'Original amount of the loan if known'
EXECUTE sp_addextendedproperty N'MS_Description', @v, N'SCHEMA', N'dbo', N'TABLE', N'Tmp_secured_loans', N'COLUMN', N'original_amount'
GO
DECLARE @v sql_variant 
SET @v = N'Current balance if known'
EXECUTE sp_addextendedproperty N'MS_Description', @v, N'SCHEMA', N'dbo', N'TABLE', N'Tmp_secured_loans', N'COLUMN', N'balance'
GO
DECLARE @v sql_variant 
SET @v = N'Number of months (periods) if known'
EXECUTE sp_addextendedproperty N'MS_Description', @v, N'SCHEMA', N'dbo', N'TABLE', N'Tmp_secured_loans', N'COLUMN', N'periods'
GO
DECLARE @v sql_variant 
SET @v = N'Past-due amount if known'
EXECUTE sp_addextendedproperty N'MS_Description', @v, N'SCHEMA', N'dbo', N'TABLE', N'Tmp_secured_loans', N'COLUMN', N'past_due_amount'
GO
DECLARE @v sql_variant 
SET @v = N'Past-due periods if known'
EXECUTE sp_addextendedproperty N'MS_Description', @v, N'SCHEMA', N'dbo', N'TABLE', N'Tmp_secured_loans', N'COLUMN', N'past_due_periods'
GO
DECLARE @v sql_variant 
SET @v = N'Loan type (FHA, VA, CONV, etc.) if known'
EXECUTE sp_addextendedproperty N'MS_Description', @v, N'SCHEMA', N'dbo', N'TABLE', N'Tmp_secured_loans', N'COLUMN', N'loan_type'
GO
DECLARE @v sql_variant 
SET @v = N'If the type is not a valid identifier, this is the description for the loan type'
EXECUTE sp_addextendedproperty N'MS_Description', @v, N'SCHEMA', N'dbo', N'TABLE', N'Tmp_secured_loans', N'COLUMN', N'loan_type_description'
GO
DECLARE @v sql_variant 
SET @v = N'Person who created the row'
EXECUTE sp_addextendedproperty N'MS_Description', @v, N'SCHEMA', N'dbo', N'TABLE', N'Tmp_secured_loans', N'COLUMN', N'created_by'
GO
DECLARE @v sql_variant 
SET @v = N'Date/Time when the row was created'
EXECUTE sp_addextendedproperty N'MS_Description', @v, N'SCHEMA', N'dbo', N'TABLE', N'Tmp_secured_loans', N'COLUMN', N'date_created'
GO
EXECUTE sp_bindefault N'dbo.default_zero', N'dbo.Tmp_secured_loans.priority'
GO
EXECUTE sp_bindefault N'dbo.default_string', N'dbo.Tmp_secured_loans.lender'
GO
EXECUTE sp_bindefault N'dbo.default_string', N'dbo.Tmp_secured_loans.account_number'
GO
EXECUTE sp_bindefault N'dbo.default_string', N'dbo.Tmp_secured_loans.case_number'
GO
EXECUTE sp_bindefault N'dbo.default_string', N'dbo.Tmp_secured_loans.loan_type_description'
GO
EXECUTE sp_bindefault N'dbo.default_money', N'dbo.Tmp_secured_loans.payment'
GO
EXECUTE sp_bindefault N'dbo.default_money', N'dbo.Tmp_secured_loans.original_amount'
GO
EXECUTE sp_bindefault N'dbo.default_money', N'dbo.Tmp_secured_loans.balance'
GO
EXECUTE sp_bindefault N'dbo.default_zero', N'dbo.Tmp_secured_loans.periods'
GO
EXECUTE sp_bindefault N'dbo.default_money', N'dbo.Tmp_secured_loans.past_due_amount'
GO
EXECUTE sp_bindefault N'dbo.default_zero', N'dbo.Tmp_secured_loans.past_due_periods'
GO
EXECUTE sp_bindefault N'dbo.default_zero', N'dbo.Tmp_secured_loans.loan_type'
GO
EXECUTE sp_bindefault N'dbo.default_false', N'dbo.Tmp_secured_loans.arm'
GO
EXECUTE sp_bindefault N'dbo.default_false', N'dbo.Tmp_secured_loans.arm_reset'
GO
EXECUTE sp_bindefault N'dbo.default_false', N'dbo.Tmp_secured_loans.interest_only'
GO
EXECUTE sp_bindefault N'dbo.default_false', N'dbo.Tmp_secured_loans.hybrid_arm'
GO
EXECUTE sp_bindefault N'dbo.default_false', N'dbo.Tmp_secured_loans.option_arm'
GO
EXECUTE sp_bindefault N'dbo.default_false', N'dbo.Tmp_secured_loans.fha_va_insured'
GO
EXECUTE sp_bindefault N'dbo.default_false', N'dbo.Tmp_secured_loans.privately_held'
GO
ALTER TABLE dbo.Tmp_secured_loans ADD CONSTRAINT DF_secured_loans_interest_rate DEFAULT (0.0) FOR interest_rate
GO
SET IDENTITY_INSERT dbo.Tmp_secured_loans ON
GO
IF EXISTS(SELECT * FROM dbo.secured_loans)
	 EXEC('INSERT INTO dbo.Tmp_secured_loans (secured_loan, secured_property, priority, lender, account_number, fdic_number, original_lender, original_account_number, original_fdic_number, case_number, interest_rate, payment, original_amount, balance, periods, past_due_amount, past_due_periods, loan_type, loan_type_description, arm, arm_reset, interest_only, hybrid_arm, option_arm, fha_va_insured, privately_held, created_by, date_created)
		SELECT secured_loan, secured_property, priority, lender, account_number, fdic_number, original_lender, original_account_number, original_fdic_number, case_number, interest_rate, payment, original_amount, balance, periods, past_due_amount, past_due_periods, loan_type, loan_type_description, arm, arm_reset, interest_only, hybrid_arm, option_arm, fha_va_insured, privately_held, created_by, date_created FROM dbo.secured_loans WITH (HOLDLOCK TABLOCKX)')
GO
SET IDENTITY_INSERT dbo.Tmp_secured_loans OFF
GO
DROP TABLE dbo.secured_loans
GO
EXECUTE sp_rename N'dbo.Tmp_secured_loans', N'secured_loans', 'OBJECT' 
GO
ALTER TABLE dbo.secured_loans ADD CONSTRAINT
	PK_secured_loans PRIMARY KEY CLUSTERED 
	(
	secured_loan
	) WITH( PAD_INDEX = OFF, FILLFACTOR = 90, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX IX_secured_loans_1 ON dbo.secured_loans
	(
	secured_property
	) WITH( PAD_INDEX = OFF, FILLFACTOR = 90, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
EXEC ('delete secured_loans from secured_loans l left outer join secured_properties p on l.secured_property = p.secured_property where p.secured_property is null')
GO
ALTER TABLE dbo.secured_loans ADD CONSTRAINT
	FK_secured_loans_secured_properties FOREIGN KEY
	(
	secured_property
	) REFERENCES dbo.secured_properties
	(
	secured_property
	) ON UPDATE  NO ACTION 
	 ON DELETE  CASCADE 
GO
DENY DELETE ON dbo.secured_loans TO www_role  AS dbo 
GO
DENY INSERT ON dbo.secured_loans TO www_role  AS dbo 
GO
DENY SELECT ON dbo.secured_loans TO www_role  AS dbo 
GO
DENY UPDATE ON dbo.secured_loans TO www_role  AS dbo 
GO
COMMIT
GO
