execute UPDATE_DROP_CONSTRAINTS 'client_appointments'
GO
execute UPDATE_add_column 'client_appointments', 'date_uploaded', 'datetime'
GO
/* To prevent any potential data loss issues, you should review this script in detail before running it outside the context of the database designer.*/
BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION
GO
CREATE TABLE dbo.Tmp_client_appointments
	(
	client_appointment dbo.typ_key NOT NULL IDENTITY (1, 1) NOT FOR REPLICATION,
	client dbo.typ_client NOT NULL,
	appt_time dbo.typ_key NULL,
	counselor dbo.typ_key NULL,
	start_time datetime NOT NULL,
	end_time datetime NULL,
	office dbo.typ_key NULL,
	workshop dbo.typ_key NULL,
	workshop_people int NULL,
	appt_type dbo.typ_key NULL,
	status dbo.typ_appt_status NOT NULL,
	result varchar(3) NULL,
	previous_appointment dbo.typ_key NULL,
	referred_to dbo.typ_key NULL,
	referred_by dbo.typ_key NULL,
	partner varchar(50) NULL,
	bankruptcy_class dbo.typ_key NOT NULL,
	priority bit NOT NULL,
	housing bit NOT NULL,
	post_purchase bit NOT NULL,
	credit bit NOT NULL,
	callback_ph varchar(50) NULL,
	HousingFeeAmount money NOT NULL,
	confirmation_status dbo.typ_key NOT NULL,
	date_confirmed datetime NULL,
	date_uploaded datetime NULL,
	date_updated datetime NOT NULL,
	date_created datetime NOT NULL,
	created_by dbo.typ_counselor NOT NULL,
	ts timestamp NOT NULL
	)  ON [PRIMARY]
GO
GRANT DELETE ON dbo.Tmp_client_appointments TO public  AS dbo
GO
GRANT INSERT ON dbo.Tmp_client_appointments TO public  AS dbo
GO
GRANT SELECT ON dbo.Tmp_client_appointments TO public  AS dbo
GO
GRANT UPDATE ON dbo.Tmp_client_appointments TO public  AS dbo
GO
DECLARE @v sql_variant 
SET @v = N'Referral source'
EXECUTE sp_addextendedproperty N'MS_Description', @v, N'SCHEMA', N'dbo', N'TABLE', N'Tmp_client_appointments', N'COLUMN', N'referred_by'
GO
DECLARE @v sql_variant 
SET @v = N'Used for the callback number that a client may give to confirm appointments. It is not stored with the client, but with the appointment.'
EXECUTE sp_addextendedproperty N'MS_Description', @v, N'SCHEMA', N'dbo', N'TABLE', N'Tmp_client_appointments', N'COLUMN', N'callback_ph'
GO
DECLARE @v sql_variant 
SET @v = N'Pointer to the confirmation status from the messages table.'
EXECUTE sp_addextendedproperty N'MS_Description', @v, N'SCHEMA', N'dbo', N'TABLE', N'Tmp_client_appointments', N'COLUMN', N'confirmation_status'
GO
DECLARE @v sql_variant 
SET @v = N'Date/Time when the FHLB appointment was sent to Vanco'
EXECUTE sp_addextendedproperty N'MS_Description', @v, N'SCHEMA', N'dbo', N'TABLE', N'Tmp_client_appointments', N'COLUMN', N'date_uploaded'
GO
EXECUTE sp_bindefault N'dbo.default_zero', N'dbo.Tmp_client_appointments.workshop_people'
GO
EXECUTE sp_bindefault N'dbo.default_zero', N'dbo.Tmp_client_appointments.referred_to'
GO
EXECUTE sp_bindefault N'dbo.default_zero', N'dbo.Tmp_client_appointments.referred_by'
GO
EXECUTE sp_bindefault N'dbo.default_zero', N'dbo.Tmp_client_appointments.bankruptcy_class'
GO
EXECUTE sp_bindefault N'dbo.default_false', N'dbo.Tmp_client_appointments.priority'
GO
EXECUTE sp_bindefault N'dbo.default_false', N'dbo.Tmp_client_appointments.housing'
GO
EXECUTE sp_bindefault N'dbo.default_false', N'dbo.Tmp_client_appointments.post_purchase'
GO
EXECUTE sp_bindefault N'dbo.default_false', N'dbo.Tmp_client_appointments.credit'
GO
EXECUTE sp_bindefault N'dbo.default_money', N'dbo.Tmp_client_appointments.HousingFeeAmount'
GO
EXECUTE sp_bindefault N'dbo.default_zero', N'dbo.Tmp_client_appointments.confirmation_status'
GO
EXECUTE sp_bindefault N'dbo.default_date', N'dbo.Tmp_client_appointments.date_updated'
GO
EXECUTE sp_bindefault N'dbo.default_date', N'dbo.Tmp_client_appointments.date_created'
GO
SET IDENTITY_INSERT dbo.Tmp_client_appointments ON
GO
EXECUTE sp_unbindrule N'dbo.Tmp_client_appointments.status'
GO
IF EXISTS(SELECT * FROM dbo.client_appointments)
	 EXEC('INSERT INTO dbo.Tmp_client_appointments (client_appointment, client, appt_time, counselor, start_time, end_time, office, workshop, workshop_people, appt_type, status, result, previous_appointment, referred_to, referred_by, partner, bankruptcy_class, priority, housing, post_purchase, credit, callback_ph, HousingFeeAmount, confirmation_status, date_confirmed, date_uploaded, date_updated, date_created, created_by)
		SELECT client_appointment, client, appt_time, counselor, start_time, end_time, office, workshop, workshop_people, appt_type, status, result, previous_appointment, referred_to, referred_by, partner, bankruptcy_class, priority, housing, post_purchase, credit, callback_ph, HousingFeeAmount, confirmation_status, date_confirmed, date_uploaded, date_updated, date_created, created_by FROM dbo.client_appointments WITH (HOLDLOCK TABLOCKX)')
GO
SET IDENTITY_INSERT dbo.Tmp_client_appointments OFF
GO
DROP TABLE dbo.client_appointments
GO
EXECUTE sp_rename N'dbo.Tmp_client_appointments', N'client_appointments', 'OBJECT' 
GO
ALTER TABLE dbo.client_appointments ADD CONSTRAINT
	PK_client_appointments PRIMARY KEY CLUSTERED 
	(
	client_appointment
	) WITH( PAD_INDEX = OFF, FILLFACTOR = 90, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX IX_client_appointments_1 ON dbo.client_appointments
	(
	appt_time,
	workshop
	) WITH( PAD_INDEX = OFF, FILLFACTOR = 90, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX IX_client_appointments_2 ON dbo.client_appointments
	(
	client
	) WITH( PAD_INDEX = OFF, FILLFACTOR = 90, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX IX_client_appointments_3 ON dbo.client_appointments
	(
	workshop,
	status
	) INCLUDE (appt_time) WITH( STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
ALTER TABLE dbo.client_appointments WITH NOCHECK ADD CONSTRAINT CK_client_appointments_1 CHECK ((NOT [office] IS NULL OR NOT [workshop] IS NULL))
GO
EXECUTE sp_bindrule N'dbo.valid_appt_status', N'dbo.client_appointments.status'
GO
/*
EXEC ('update client_appointments set status = ''M'' where status = ''P'' and appt_time is null and office is not null')
-- GO
ALTER TABLE dbo.client_appointments ADD CONSTRAINT CK_client_appointments_2 CHECK (NOT (([status]='P') AND ([appt_time] IS NULL) AND ([office] IS NOT NULL)))
-- GO
DECLARE @v sql_variant 
SET @v = N'Ensure pending appointments have a time slot'
EXECUTE sp_addextendedproperty N'MS_Description', @v, N'SCHEMA', N'dbo', N'TABLE', N'client_appointments', N'CONSTRAINT', N'CK_client_appointments_2'
-- GO
*/
CREATE trigger trig_client_appointments_fhlb on dbo.client_appointments after update as
begin
	set nocount on

	--
	-- If we are changing the partner code then clear the date uploaded.
	-- The row needs to be retransmitted to Vanco with the proper FHLB partner number.
	--
	if update(partner)
	begin
		update	client_appointments
		set		date_uploaded = null
		from	client_appointments ca
		inner join inserted i on ca.client_appointment = i.client_appointment
		inner join deleted  d on ca.client_appointment = d.client_appointment
		where	isnull(i.partner,'') <> isnull(d.partner,'')
	end
end
GO
DENY DELETE ON dbo.client_appointments TO www_role  AS dbo 
GO
DENY INSERT ON dbo.client_appointments TO www_role  AS dbo 
GO
DENY SELECT ON dbo.client_appointments TO www_role  AS dbo 
GO
DENY UPDATE ON dbo.client_appointments TO www_role  AS dbo 
GO
COMMIT
GO
