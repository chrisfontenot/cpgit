/* To prevent any potential data loss issues, you should review this script in detail before running it outside the context of the database designer.*/
BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
execute UPDATE_ADD_COLUMN 'EthnicityTypes', 'hpf'
BEGIN TRANSACTION
GO
CREATE TABLE dbo.Tmp_EthnicityTypes
	(
	oID dbo.typ_key NOT NULL IDENTITY (0, 1),
	description dbo.typ_description NOT NULL,
	nfcc varchar(4) NULL,
	rpps varchar(4) NULL,
	epay varchar(4) NULL,
	note dbo.typ_message NULL,
	hud_9902_section varchar(256) NULL,
	nfmcp_section varchar(4) NULL,
	[default] bit NOT NULL,
	ActiveFlag bit NOT NULL,
	RxOffice varchar(20) NULL,
	hpf varchar(50) NULL,
	date_created dbo.typ_date NOT NULL,
	created_by dbo.typ_counselor NOT NULL,
	ts timestamp NOT NULL
	)  ON [PRIMARY]
GO
GRANT DELETE ON dbo.Tmp_EthnicityTypes TO public  AS dbo
GO
GRANT INSERT ON dbo.Tmp_EthnicityTypes TO public  AS dbo
GO
GRANT SELECT ON dbo.Tmp_EthnicityTypes TO public  AS dbo
GO
GRANT UPDATE ON dbo.Tmp_EthnicityTypes TO public  AS dbo
GO
EXECUTE sp_bindefault N'dbo.default_false', N'dbo.Tmp_EthnicityTypes.[default]'
GO
EXECUTE sp_bindefault N'dbo.default_true', N'dbo.Tmp_EthnicityTypes.ActiveFlag'
GO
SET IDENTITY_INSERT dbo.Tmp_EthnicityTypes ON
GO
IF EXISTS(SELECT * FROM dbo.EthnicityTypes)
	 EXEC('INSERT INTO dbo.Tmp_EthnicityTypes (oID, description, nfcc, rpps, epay, note, hud_9902_section, nfmcp_section, [default], ActiveFlag, date_created, created_by, RxOffice, hpf)
		SELECT oID, description, nfcc, rpps, epay, note, hud_9902_section, nfmcp_section, [default], ActiveFlag, date_created, created_by, RxOffice, hpf FROM dbo.EthnicityTypes WITH (HOLDLOCK TABLOCKX)')
GO
SET IDENTITY_INSERT dbo.Tmp_EthnicityTypes OFF
GO
DROP TABLE dbo.EthnicityTypes
GO
EXECUTE sp_rename N'dbo.Tmp_EthnicityTypes', N'EthnicityTypes', 'OBJECT' 
GO
ALTER TABLE dbo.EthnicityTypes ADD CONSTRAINT
	PK_EthnicityTypes PRIMARY KEY CLUSTERED 
	(
	oID
	) WITH( STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
DENY DELETE ON dbo.EthnicityTypes TO www_role  AS dbo 
GO
DENY INSERT ON dbo.EthnicityTypes TO www_role  AS dbo 
GO
DENY SELECT ON dbo.EthnicityTypes TO www_role  AS dbo 
GO
DENY UPDATE ON dbo.EthnicityTypes TO www_role  AS dbo 
GO
COMMIT
GO
