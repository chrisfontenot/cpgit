USE [DebtPlus]
GO
/****** Object:  Table [dbo].[registers_invoices]    Script Date: 09/15/2014 13:13:26 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[registers_invoices](
	[invoice_register] [dbo].[typ_key] IDENTITY(1,1) NOT NULL,
	[creditor] [dbo].[typ_creditor] NOT NULL,
	[inv_date] [datetime] NOT NULL,
	[inv_amount] [money] NOT NULL,
	[adj_date] [datetime] NULL,
	[adj_amount] [money] NOT NULL,
	[pmt_date] [datetime] NULL,
	[pmt_amount] [money] NOT NULL,
	[date_printed] [dbo].[typ_date] NULL,
	[date_created] [dbo].[typ_date] NOT NULL,
	[created_by] [dbo].[typ_counselor] NOT NULL,
	[ts] [timestamp] NOT NULL,
	[aging_months]  AS ([dbo].[invoice_aging_months]([inv_date],getdate())),
 CONSTRAINT [PK_registers_invoices] PRIMARY KEY CLUSTERED 
(
	[invoice_register] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 90) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Invoice Number. This is the primary key to the table.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'registers_invoices', @level2type=N'COLUMN',@level2name=N'invoice_register'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Creditor for which the invoice applies' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'registers_invoices', @level2type=N'COLUMN',@level2name=N'creditor'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Date / Time when the invoice was created' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'registers_invoices', @level2type=N'COLUMN',@level2name=N'inv_date'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Amount of the invoice. This should be the sum of the detail items from the registers_client_creditor disbursement rows.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'registers_invoices', @level2type=N'COLUMN',@level2name=N'inv_amount'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Date that the invoice was last adjusted' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'registers_invoices', @level2type=N'COLUMN',@level2name=N'adj_date'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Total amount of the adjustments to this invoice' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'registers_invoices', @level2type=N'COLUMN',@level2name=N'adj_amount'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Date that a payment was last received for this invoice' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'registers_invoices', @level2type=N'COLUMN',@level2name=N'pmt_date'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Total amount of the payments on this invoice' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'registers_invoices', @level2type=N'COLUMN',@level2name=N'pmt_amount'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Date that the invoice was actually printed' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'registers_invoices', @level2type=N'COLUMN',@level2name=N'date_printed'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Date/Time when the row was created.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'registers_invoices', @level2type=N'COLUMN',@level2name=N'date_created'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Person who created the row.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'registers_invoices', @level2type=N'COLUMN',@level2name=N'created_by'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Number of months that the invoice is outstanding if it is outstanding' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'registers_invoices', @level2type=N'COLUMN',@level2name=N'aging_months'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'This table holds the summary information for an invoice' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'registers_invoices'
GO
