USE [DebtPlus]
GO
/****** Object:  Table [dbo].[rpps_response_details_cdc]    Script Date: 09/15/2014 13:13:26 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[rpps_response_details_cdc](
	[rpps_response_detail_cdc] [int] IDENTITY(1,1) NOT NULL,
	[rpps_response_detail] [int] NULL,
	[monthly_payment_amount] [money] NULL,
	[balance] [money] NULL,
	[new_account_number] [varchar](50) NULL,
	[created_by] [dbo].[typ_counselor] NOT NULL,
	[date_created] [dbo].[typ_date] NOT NULL,
 CONSTRAINT [PK_rpps_response_details_cdc] PRIMARY KEY CLUSTERED 
(
	[rpps_response_detail_cdc] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 90) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
