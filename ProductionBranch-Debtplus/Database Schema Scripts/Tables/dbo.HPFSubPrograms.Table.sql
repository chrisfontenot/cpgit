/* To prevent any potential data loss issues, you should review this script in detail before running it outside the context of the database designer.*/
BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION
GO
CREATE TABLE dbo.Tmp_HPFSubPrograms
	(
	oID dbo.typ_key NOT NULL IDENTITY (1, 1),
	HPFProgramID dbo.typ_key NULL,
	description dbo.typ_description NOT NULL,
	SubProgramCode varchar(50) NULL,
	SubProgramId int NULL,
	hpf varchar(50) NULL,
	ActiveFlag bit NOT NULL,
	ts timestamp NOT NULL
	)  ON [PRIMARY]
GO
GRANT DELETE ON dbo.Tmp_HPFSubPrograms TO public  AS dbo
GO
GRANT INSERT ON dbo.Tmp_HPFSubPrograms TO public  AS dbo
GO
GRANT SELECT ON dbo.Tmp_HPFSubPrograms TO public  AS dbo
GO
GRANT UPDATE ON dbo.Tmp_HPFSubPrograms TO public  AS dbo
GO
EXECUTE sp_bindefault N'dbo.default_true', N'dbo.Tmp_HPFSubPrograms.ActiveFlag'
GO
SET IDENTITY_INSERT dbo.Tmp_HPFSubPrograms ON
GO
IF EXISTS(SELECT * FROM sysobjects WHERE name=N'HPFSubPrograms' AND type='U')
BEGIN
	EXEC('INSERT INTO dbo.Tmp_HPFSubPrograms (oID, HPFProgramID, description, SubProgramCode, SubProgramId, hpf, ActiveFlag)
		SELECT oID, HPFProgramID, description, SubProgramCode, SubProgramId, hpf, ActiveFlag FROM dbo.HPFSubPrograms WITH (HOLDLOCK TABLOCKX)')
	EXEC('DROP TABLE dbo.HPFSubPrograms')
END
GO
SET IDENTITY_INSERT dbo.Tmp_HPFSubPrograms OFF
GO
EXECUTE sp_rename N'dbo.Tmp_HPFSubPrograms', N'HPFSubPrograms', 'OBJECT' 
GO
ALTER TABLE dbo.HPFSubPrograms ADD CONSTRAINT
	PK_HPFSubPrograms PRIMARY KEY CLUSTERED 
	(
	oID
	) WITH( STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
DENY DELETE ON dbo.HPFSubPrograms TO www_role  AS dbo 
GO
DENY INSERT ON dbo.HPFSubPrograms TO www_role  AS dbo 
GO
DENY SELECT ON dbo.HPFSubPrograms TO www_role  AS dbo 
GO
DENY UPDATE ON dbo.HPFSubPrograms TO www_role  AS dbo 
GO
COMMIT
