USE [DebtPlus]
GO
/****** Object:  Table [dbo].[creditor_statistics]    Script Date: 09/15/2014 13:13:26 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[creditor_statistics](
	[creditor] [dbo].[typ_creditor] NOT NULL,
	[clients] [int] NOT NULL,
	[active_clients] [int] NOT NULL,
	[zero_bal_clients] [int] NOT NULL,
	[clients_balance] [int] NOT NULL,
	[total_balance] [money] NOT NULL,
	[total_disbursement] [money] NOT NULL,
	[avg_disbursement] [money] NOT NULL,
	[date_created] [dbo].[typ_date] NOT NULL,
	[created_by] [dbo].[typ_counselor] NOT NULL,
 CONSTRAINT [PK_creditor_statistics] PRIMARY KEY CLUSTERED 
(
	[creditor] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 90) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
