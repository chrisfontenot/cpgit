USE [DebtPlus]
GO
/****** Object:  Table [dbo].[letter_fields]    Script Date: 09/15/2014 13:13:26 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[letter_fields](
	[letter_field] [varchar](255) NOT NULL,
	[type] [int] NOT NULL,
	[letter_table] [dbo].[typ_key] NULL,
	[field_name] [varchar](255) NULL,
	[formatting] [varchar](255) NULL,
	[date_created] [dbo].[typ_date] NOT NULL,
	[created_by] [dbo].[typ_counselor] NOT NULL,
 CONSTRAINT [PK_letter_fields] PRIMARY KEY CLUSTERED 
(
	[letter_field] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
