/* To prevent any potential data loss issues, you should review this script in detail before running it outside the context of the database designer.*/
BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION
GO
EXECUTE UPDATE_ADD_COLUMN 'HousingTypes', 'hpf'
GO
CREATE TABLE dbo.Tmp_HousingTypes
	(
	oID dbo.typ_key NOT NULL IDENTITY (1, 1),
	description dbo.typ_description NOT NULL,
	note dbo.typ_message NULL,
	[default] bit NOT NULL,
	ActiveFlag bit NOT NULL,
	SingleFamilyHomeIND bit NOT NULL,
	hud_9902_section varchar(256) NULL,
	rpps varchar(4) NULL,
	nfcc varchar(4) NULL,
	epay varchar(4) NULL,
	hpf varchar(50) NULL,
	date_created dbo.typ_date NOT NULL,
	created_by dbo.typ_counselor NOT NULL,
	ts timestamp NOT NULL,
	)  ON [PRIMARY]
GO
GRANT DELETE ON dbo.Tmp_HousingTypes TO public  AS dbo
GO
GRANT INSERT ON dbo.Tmp_HousingTypes TO public  AS dbo
GO
GRANT SELECT ON dbo.Tmp_HousingTypes TO public  AS dbo
GO
GRANT UPDATE ON dbo.Tmp_HousingTypes TO public  AS dbo
GO
EXECUTE sp_bindefault N'dbo.default_false', N'dbo.Tmp_HousingTypes.[default]'
GO
EXECUTE sp_bindefault N'dbo.default_true', N'dbo.Tmp_HousingTypes.ActiveFlag'
GO
EXECUTE sp_bindefault N'dbo.default_false', N'dbo.Tmp_HousingTypes.SingleFamilyHomeIND'
GO
SET IDENTITY_INSERT dbo.Tmp_HousingTypes ON
GO
IF EXISTS(SELECT * FROM dbo.HousingTypes)
	 EXEC('INSERT INTO dbo.Tmp_HousingTypes (oID, description, note, hud_9902_section, [default], ActiveFlag, SingleFamilyHomeIND, rpps, date_created, created_by, nfcc, epay, hpf)
		SELECT oID, description, note, hud_9902_section, [default], ActiveFlag, SingleFamilyHomeIND, rpps, date_created, created_by, nfcc, epay, hpf FROM dbo.HousingTypes WITH (HOLDLOCK TABLOCKX)')
GO
SET IDENTITY_INSERT dbo.Tmp_HousingTypes OFF
GO
DROP TABLE dbo.HousingTypes
GO
EXECUTE sp_rename N'dbo.Tmp_HousingTypes', N'HousingTypes', 'OBJECT' 
GO
ALTER TABLE dbo.HousingTypes ADD CONSTRAINT
	PK_HousingTypes PRIMARY KEY CLUSTERED 
	(
	oID
	) WITH( STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]

GO
DENY DELETE ON dbo.HousingTypes TO www_role  AS dbo 
GO
DENY INSERT ON dbo.HousingTypes TO www_role  AS dbo 
GO
DENY SELECT ON dbo.HousingTypes TO www_role  AS dbo 
GO
DENY UPDATE ON dbo.HousingTypes TO www_role  AS dbo 
GO
COMMIT
