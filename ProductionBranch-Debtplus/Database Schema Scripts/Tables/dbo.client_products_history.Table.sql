EXECUTE UPDATE_DROP_CONSTRAINTS 'client_products_history'
GO
/* To prevent any potential data loss issues, you should review this script in detail before running it outside the context of the database designer.*/
BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION
GO
CREATE TABLE dbo.Tmp_client_products_history
	(
	oID dbo.typ_key NOT NULL IDENTITY (1, 1),
	client_product dbo.typ_key NOT NULL,
	vendor dbo.typ_key NOT NULL,
	cost money NOT NULL,
	discount money NOT NULL,
	tendered money NOT NULL,
	disputed_amount money NOT NULL,
	disputed_date datetime NULL,
	disputed_status dbo.typ_key NULL,
	disputed_note varchar(2048) NULL,
	resolution_date datetime NULL,
	resolution_status dbo.typ_key NULL,
	resolution_note varchar(2048) NULL,
	invoice_status int NOT NULL,
	invoice_date datetime NULL,
	vendor_invoice dbo.typ_key NULL,
	date_created dbo.typ_date NOT NULL,
	created_by dbo.typ_counselor NOT NULL,
	ts timestamp NOT NULL
	)  ON [PRIMARY]
GO
ALTER TABLE dbo.Tmp_client_products_history SET (LOCK_ESCALATION = TABLE)
GO
EXECUTE sp_bindefault N'dbo.default_money', N'dbo.Tmp_client_products_history.cost'
GO
EXECUTE sp_bindefault N'dbo.default_money', N'dbo.Tmp_client_products_history.discount'
GO
EXECUTE sp_bindefault N'dbo.default_money', N'dbo.Tmp_client_products_history.tendered'
GO
EXECUTE sp_bindefault N'dbo.default_money', N'dbo.Tmp_client_products_history.disputed_amount'
GO
EXECUTE sp_bindefault N'dbo.default_zero', N'dbo.Tmp_client_products_history.invoice_status'
GO
IF EXISTS(SELECT * FROM SYSOBJECTS WHERE TYPE='U' AND NAME=N'client_products_history')
BEGIN
	 EXEC('SET IDENTITY_INSERT dbo.Tmp_client_products_history ON;
		   INSERT INTO dbo.Tmp_client_products_history (oID, client_product, vendor, cost, discount, tendered, disputed_amount, disputed_date, disputed_status, disputed_note, resolution_date, resolution_status, resolution_note, invoice_status, invoice_date, vendor_invoice, date_created, created_by)
		   SELECT oID, client_product, vendor, cost, discount, tendered, disputed_amount, disputed_date, disputed_status, disputed_note, resolution_date, resolution_status, resolution_note, invoice_status, invoice_date, vendor_invoice, date_created, created_by FROM dbo.client_products_history WITH (HOLDLOCK TABLOCKX);
		   SET IDENTITY_INSERT dbo.Tmp_client_products_history OFF;')
	 EXEC('DROP TABLE dbo.client_products_history')
END
GO
EXECUTE sp_rename N'dbo.Tmp_client_products_history', N'client_products_history', 'OBJECT' 
GO
ALTER TABLE dbo.client_products_history ADD CONSTRAINT
	PK_client_products_history PRIMARY KEY CLUSTERED 
	(
	oID
	) WITH( STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
ALTER TABLE dbo.client_products_history ADD CONSTRAINT
	FK_client_products_history_client_products FOREIGN KEY
	(
	client_product
	) REFERENCES dbo.client_products
	(
	oID
	) ON UPDATE  CASCADE 
	 ON DELETE  CASCADE 
GO
ALTER TABLE dbo.client_products_history ADD CONSTRAINT
	FK_client_products_history_vendors FOREIGN KEY
	(
	vendor
	) REFERENCES dbo.vendors
	(
	oID
	) ON UPDATE  NO ACTION 
	 ON DELETE  NO ACTION 
GO
COMMIT
