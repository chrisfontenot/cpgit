USE [DebtPlus]
GO
/****** Object:  Table [dbo].[client_www_notes]    Script Date: 09/15/2014 13:13:26 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[client_www_notes](
	[client_www_note] [dbo].[typ_key] IDENTITY(1,1) NOT NULL,
	[PKID] [uniqueidentifier] NOT NULL,
	[message] [dbo].[typ_message] NOT NULL,
	[date_viewed] [datetime] NULL,
	[date_shown] [datetime] NULL,
	[date_created] [dbo].[typ_date] NOT NULL,
	[created_by] [dbo].[typ_counselor] NOT NULL,
 CONSTRAINT [PK_client_www_notes] PRIMARY KEY CLUSTERED 
(
	[client_www_note] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 90) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Primary key to the table.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'client_www_notes', @level2type=N'COLUMN',@level2name=N'client_www_note'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Message text for the note' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'client_www_notes', @level2type=N'COLUMN',@level2name=N'message'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Date / Time that the message was actually shown to the client. The client clicked on the "view" button.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'client_www_notes', @level2type=N'COLUMN',@level2name=N'date_viewed'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Date / Time when the client clicked on the "delete" button.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'client_www_notes', @level2type=N'COLUMN',@level2name=N'date_shown'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Date / Time when the message was created' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'client_www_notes', @level2type=N'COLUMN',@level2name=N'date_created'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Name of the person who created the note.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'client_www_notes', @level2type=N'COLUMN',@level2name=N'created_by'
GO
