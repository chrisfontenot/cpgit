USE [DebtPlus]
GO
/****** Object:  Table [dbo].[zip_to_timezone]    Script Date: 09/15/2014 13:13:26 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[zip_to_timezone](
	[zip] [nchar](10) NULL,
	[city] [nchar](100) NULL,
	[state] [nchar](10) NULL,
	[gmt_offset] [int] NULL,
	[dst] [int] NULL,
	[time_zone] [nchar](10) NULL,
	[tz_num] [int] NULL
) ON [PRIMARY]
GO
