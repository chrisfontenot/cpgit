USE [DebtPlus]
GO
SET IDENTITY_INSERT [dbo].[AttributeTypes]  ON
INSERT INTO [dbo].[AttributeTypes](oID, [Grouping],[Attribute],[Default],[ActiveFlag])VALUES (104, 'ROLE', 'DSA', 0, 1)
INSERT INTO [dbo].[AttributeTypes](oID, [Grouping],[Attribute],[Default],[ActiveFlag])VALUES (301, 'DSAPARTNER', 'Freddie Mac', 0, 1)
INSERT INTO [dbo].[AttributeTypes](oID, [Grouping],[Attribute],[Default],[ActiveFlag])VALUES (302, 'DSAPARTNER', 'Fannie Mae MHN', 0, 1)
INSERT INTO [dbo].[AttributeTypes](oID, [Grouping],[Attribute],[Default],[ActiveFlag])VALUES (303, 'DSAPARTNER', 'Fannie Mae HPF', 0, 1)
INSERT INTO [dbo].[AttributeTypes](oID, [Grouping],[Attribute],[Default],[ActiveFlag])VALUES (304, 'DSAPARTNER', 'Wells Fargo', 0, 1)
INSERT INTO [dbo].[AttributeTypes](oID, [Grouping],[Attribute],[Default],[ActiveFlag])VALUES (305, 'DSAPARTNER', 'HPF-FMAC', 0, 1)
SET IDENTITY_INSERT [dbo].[AttributeTypes]  OFF
GO