USE [DebtPlus]
GO
/****** Object:  Table [dbo].[addresses]    Script Date: 09/15/2014 13:13:25 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[addresses](
	[address] [dbo].[typ_key] IDENTITY(11,1) NOT NULL,
	[creditor_prefix_1] [varchar](256) NOT NULL,
	[creditor_prefix_2] [varchar](256) NOT NULL,
	[house] [varchar](256) NOT NULL,
	[direction] [varchar](256) NOT NULL,
	[street] [varchar](256) NOT NULL,
	[suffix] [varchar](256) NOT NULL,
	[modifier] [varchar](256) NOT NULL,
	[modifier_value] [varchar](256) NOT NULL,
	[address_line_2] [varchar](256) NOT NULL,
	[address_line_3] [varchar](256) NOT NULL,
	[city] [varchar](256) NOT NULL,
	[state] [dbo].[typ_key] NOT NULL,
	[PostalCode] [varchar](256) NOT NULL,
 CONSTRAINT [PK_addresses] PRIMARY KEY CLUSTERED 
(
	[address] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
