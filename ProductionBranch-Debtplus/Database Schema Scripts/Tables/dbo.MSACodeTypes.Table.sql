EXECUTE UPDATE_DROP_CONSTRAINTS 'MSACodeTypes'
GO
/* To prevent any potential data loss issues, you should review this script in detail before running it outside the context of the database designer.*/
BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION
GO
CREATE TABLE dbo.Tmp_MSACodeTypes
	(
	Id varchar(10) NOT NULL,
	Name varchar(50) NOT NULL,
	Type varchar(10) NOT NULL,
	date_created dbo.typ_date NOT NULL,
	created_by dbo.typ_counselor NOT NULL,
	ts timestamp NOT NULL
	)  ON [PRIMARY]
GO
IF EXISTS(SELECT * FROM SYSOBJECTS WHERE TYPE='U' AND NAME=N'MSACodeTypes')
BEGIN
	EXEC('INSERT INTO dbo.Tmp_MSACodeTypes (Id, Name, Type, date_created, created_by)
		SELECT Id, Name, Type, date_created, created_by FROM dbo.MSACodeTypes WITH (HOLDLOCK TABLOCKX)')
	EXEC('DROP TABLE dbo.MSACodeTypes')
END
GO
EXECUTE sp_rename N'dbo.Tmp_MSACodeTypes', N'MSACodeTypes', 'OBJECT' 
GO
ALTER TABLE dbo.MSACodeTypes ADD CONSTRAINT
	PK_MSACodeTypes PRIMARY KEY CLUSTERED 
	(
	Id
	) WITH( STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
COMMIT
BEGIN TRANSACTION
GO
ALTER TABLE dbo.ZipCodeSearch ADD CONSTRAINT
	FK_ZipCodeSearch_MSACodeTypes FOREIGN KEY
	(
	MSACode
	) REFERENCES dbo.MSACodeTypes
	(
	Id
	) ON UPDATE  CASCADE 
	 ON DELETE  NO ACTION 
GO
COMMIT
GO
