USE [DebtPlus]
GO
/****** Object:  Table [dbo].[counselor_attributes]    Script Date: 09/15/2014 13:13:26 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[counselor_attributes](
	[oID] [dbo].[typ_key] IDENTITY(1,1) NOT NULL,
	[Counselor] [dbo].[typ_key] NOT NULL,
	[Attribute] [dbo].[typ_key] NOT NULL,
 CONSTRAINT [PK_counselor_attributes] PRIMARY KEY CLUSTERED 
(
	[oID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY],
 CONSTRAINT [IX_counselor_attributes_3] UNIQUE NONCLUSTERED 
(
	[Counselor] ASC,
	[Attribute] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
