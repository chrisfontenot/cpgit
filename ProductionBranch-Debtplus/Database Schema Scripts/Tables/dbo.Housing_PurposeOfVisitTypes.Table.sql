USE [DebtPlus]
GO
/****** Object:  Table [dbo].[Housing_PurposeOfVisitTypes]    Script Date: 09/15/2014 13:13:26 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Housing_PurposeOfVisitTypes](
	[oID] [dbo].[typ_key] IDENTITY(1,1) NOT NULL,
	[description] [dbo].[typ_description] NOT NULL,
	[hud_9902_section] [varchar](256) NULL,
	[HourlyGrantAmountUsed] [money] NOT NULL,
	[HousingFeeAmount] [money] NOT NULL,
	[Default] [bit] NOT NULL,
	[ActiveFlag] [bit] NOT NULL,
	[old_hud_9902_section] [varchar](100) NULL,
 CONSTRAINT [PK_housing_PurposeOfVisitTypes] PRIMARY KEY CLUSTERED 
(
	[oID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
