USE [DebtPlus]
GO
/****** Object:  Table [dbo].[client_creditor_events]    Script Date: 09/15/2014 13:13:25 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[client_creditor_events](
	[client_creditor_event] [dbo].[typ_key] IDENTITY(1,1) NOT NULL,
	[client_creditor] [dbo].[typ_key] NOT NULL,
	[update_item] [int] NOT NULL,
	[config_fee] [int] NULL,
	[value] [money] NOT NULL,
	[effective_date] [datetime] NOT NULL,
	[date_updated] [datetime] NULL,
	[date_created] [dbo].[typ_date] NOT NULL,
	[created_by] [dbo].[typ_counselor] NOT NULL,
	[old_client_creditor_event] [int] NULL,
 CONSTRAINT [PK_client_creditor_events] PRIMARY KEY CLUSTERED 
(
	[client_creditor_event] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 90) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
ALTER TABLE [dbo].[client_creditor_events]  WITH NOCHECK ADD  CONSTRAINT [CK_client_creditor_events] CHECK NOT FOR REPLICATION (([effective_date] > getdate()))
GO
ALTER TABLE [dbo].[client_creditor_events] CHECK CONSTRAINT [CK_client_creditor_events]
GO
