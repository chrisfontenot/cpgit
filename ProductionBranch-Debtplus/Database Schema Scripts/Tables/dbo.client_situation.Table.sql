﻿USE [DEBTPLUS]
GO

/****** Object:  Table [dbo].[client_situation]    Script Date: 11/17/2014 12:36:56 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[client_situation](
	[oID] [dbo].[typ_key] IDENTITY(1,1) NOT NULL,
	[description] [dbo].[typ_description] NOT NULL,
	[nfcc] [varchar](4) NULL,
	[rpps] [varchar](4) NULL,
	[epay] [varchar](4) NULL,
	[note] [dbo].[typ_message] NULL,
	[hud_9902_section] [varchar](256) NULL,
	[default] [bit] NOT NULL CONSTRAINT [DF_client_situation_default]  DEFAULT ((0)),
	[sortorder] [int] NOT NULL,
	[ActiveFlag] [bit] NOT NULL CONSTRAINT [DF_client_situation_ActiveFlag]  DEFAULT ((1)),
	[date_created] [dbo].[typ_date] NOT NULL,
	[created_by] [dbo].[typ_counselor] NOT NULL,
	[ts] [timestamp] NOT NULL
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO


USE [DEBTPLUS]
GO

INSERT INTO [dbo].[client_situation]([description],[default],[sortorder],[ActiveFlag],[date_created],[created_by])VALUES ('Short term', 0, 1, 1, getdate(), 'sa')
INSERT INTO [dbo].[client_situation]([description],[default],[sortorder],[ActiveFlag],[date_created],[created_by])VALUES ('Long term', 0, 2, 1, getdate(), 'sa')
INSERT INTO [dbo].[client_situation]([description],[default],[sortorder],[ActiveFlag],[date_created],[created_by])VALUES ('Permanent', 0, 3, 1, getdate(), 'sa')
GO

