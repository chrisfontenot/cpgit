EXECUTE UPDATE_DROP_CONSTRAINTS 'counties'
GO
/* To prevent any potential data loss issues, you should review this script in detail before running it outside the context of the database designer.*/
BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION
GO
CREATE TABLE dbo.Tmp_counties
	(
	county dbo.typ_key NOT NULL IDENTITY (2, 1) NOT FOR REPLICATION,
	state dbo.typ_key NOT NULL,
	name dbo.typ_description NOT NULL,
	median_income money NOT NULL,
	bankruptcy_district dbo.typ_key NULL,
	[default] bit NOT NULL,
	ActiveFlag bit NOT NULL,
	FIPS varchar(10) NULL,
	created_by dbo.typ_counselor NOT NULL,
	date_created dbo.typ_date NOT NULL,
	ts timestamp NOT NULL
	)  ON [PRIMARY]
GO
GRANT INSERT,SELECT,UPDATE,DELETE ON dbo.Tmp_counties TO public  AS dbo
GO
DECLARE @v sql_variant 
SET @v = N'County ID for the client'
EXECUTE sp_addextendedproperty N'MS_Description', @v, N'SCHEMA', N'dbo', N'TABLE', N'Tmp_counties', N'COLUMN', N'county'
GO
DECLARE @v sql_variant 
SET @v = N'Name of the county'
EXECUTE sp_addextendedproperty N'MS_Description', @v, N'SCHEMA', N'dbo', N'TABLE', N'Tmp_counties', N'COLUMN', N'name'
GO
DECLARE @v sql_variant 
SET @v = N'For H.U.D., this is the median income for the county.'
EXECUTE sp_addextendedproperty N'MS_Description', @v, N'SCHEMA', N'dbo', N'TABLE', N'Tmp_counties', N'COLUMN', N'median_income'
GO
DECLARE @v sql_variant 
SET @v = N'Pointer to the bankruptcy district for this county'
EXECUTE sp_addextendedproperty N'MS_Description', @v, N'SCHEMA', N'dbo', N'TABLE', N'Tmp_counties', N'COLUMN', N'bankruptcy_district'
GO
DECLARE @v sql_variant 
SET @v = N'Is this the default county?'
EXECUTE sp_addextendedproperty N'MS_Description', @v, N'SCHEMA', N'dbo', N'TABLE', N'Tmp_counties', N'COLUMN', N'default'
GO
DECLARE @v sql_variant 
SET @v = N'If false, item may not be selected'
EXECUTE sp_addextendedproperty N'MS_Description', @v, N'SCHEMA', N'dbo', N'TABLE', N'Tmp_counties', N'COLUMN', N'ActiveFlag'
GO
DECLARE @v sql_variant 
SET @v = N'ID from HUD for the median income'
EXECUTE sp_addextendedproperty N'MS_Description', @v, N'SCHEMA', N'dbo', N'TABLE', N'Tmp_counties', N'COLUMN', N'FIPS'
GO
DECLARE @v sql_variant 
SET @v = N'Name of the person who created the row'
EXECUTE sp_addextendedproperty N'MS_Description', @v, N'SCHEMA', N'dbo', N'TABLE', N'Tmp_counties', N'COLUMN', N'created_by'
GO
DECLARE @v sql_variant 
SET @v = N'When the row was created'
EXECUTE sp_addextendedproperty N'MS_Description', @v, N'SCHEMA', N'dbo', N'TABLE', N'Tmp_counties', N'COLUMN', N'date_created'
GO
EXECUTE sp_bindefault N'dbo.default_zero', N'dbo.Tmp_counties.state'
GO
EXECUTE sp_bindefault N'dbo.default_money', N'dbo.Tmp_counties.median_income'
GO
EXECUTE sp_bindefault N'dbo.default_false', N'dbo.Tmp_counties.[default]'
GO
EXECUTE sp_bindefault N'dbo.default_true', N'dbo.Tmp_counties.ActiveFlag'
GO
SET IDENTITY_INSERT dbo.Tmp_counties ON
GO
IF EXISTS(SELECT * FROM SYSOBJECTS WHERE TYPE='U' AND NAME=N'counties')
BEGIN
	EXEC('INSERT INTO dbo.Tmp_counties (county, state, name, median_income, bankruptcy_district, [default], ActiveFlag, FIPS, created_by, date_created)
		SELECT county, state, name, median_income, bankruptcy_district, [default], ActiveFlag, FIPS, created_by, date_created FROM dbo.counties WITH (HOLDLOCK TABLOCKX)')
    EXEC('DROP TABLE dbo.counties')
END
GO
SET IDENTITY_INSERT dbo.Tmp_counties OFF
GO
EXECUTE sp_rename N'dbo.Tmp_counties', N'counties', 'OBJECT' 
GO
ALTER TABLE dbo.counties ADD CONSTRAINT
	PK_counties PRIMARY KEY CLUSTERED 
	(
	county
	) WITH( PAD_INDEX = OFF, FILLFACTOR = 90, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
DENY INSERT,SELECT,UPDATE,DELETE ON dbo.counties TO www_role  AS dbo 
GO
COMMIT
BEGIN TRANSACTION
GO
ALTER TABLE dbo.clients ADD CONSTRAINT
	FK_clients_Counties FOREIGN KEY
	(
	county
	) REFERENCES dbo.counties
	(
	county
	) ON UPDATE  CASCADE 
	 ON DELETE  NO ACTION 
GO
COMMIT
GO
