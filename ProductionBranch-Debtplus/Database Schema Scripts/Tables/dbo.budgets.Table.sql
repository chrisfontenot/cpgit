EXECUTE UPDATE_DROP_CONSTRAINTS 'budgets'
GO
/* To prevent any potential data loss issues, you should review this script in detail before running it outside the context of the database designer.*/
BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION
GO
CREATE TABLE dbo.Tmp_budgets
	(
	budget dbo.typ_key NOT NULL IDENTITY (1, 1),
	client dbo.typ_client NOT NULL,
	starting_date datetime NOT NULL,
	ending_date datetime NULL,
	budget_type char(1) NOT NULL,
	date_created dbo.typ_date NOT NULL,
	created_by dbo.typ_counselor NOT NULL,
	ts timestamp NOT NULL
	)  ON [PRIMARY]
GO
GRANT DELETE ON dbo.Tmp_budgets TO public  AS dbo
GO
GRANT INSERT ON dbo.Tmp_budgets TO public  AS dbo
GO
GRANT SELECT ON dbo.Tmp_budgets TO public  AS dbo
GO
GRANT UPDATE ON dbo.Tmp_budgets TO public  AS dbo
GO
DECLARE @v sql_variant 
SET @v = N'This lists the total budgets in the system. The client is included, along with the date that the budget was created.'
EXECUTE sp_addextendedproperty N'MS_Description', @v, N'SCHEMA', N'dbo', N'TABLE', N'Tmp_budgets', NULL, NULL
GO
DECLARE @v sql_variant 
SET @v = N'Budget ID'
EXECUTE sp_addextendedproperty N'MS_Description', @v, N'SCHEMA', N'dbo', N'TABLE', N'Tmp_budgets', N'COLUMN', N'budget'
GO
DECLARE @v sql_variant 
SET @v = N'Client associated with the budget'
EXECUTE sp_addextendedproperty N'MS_Description', @v, N'SCHEMA', N'dbo', N'TABLE', N'Tmp_budgets', N'COLUMN', N'client'
GO
DECLARE @v sql_variant 
SET @v = N'Starting date for the budget'
EXECUTE sp_addextendedproperty N'MS_Description', @v, N'SCHEMA', N'dbo', N'TABLE', N'Tmp_budgets', N'COLUMN', N'starting_date'
GO
DECLARE @v sql_variant 
SET @v = N'Ending date for the budget'
EXECUTE sp_addextendedproperty N'MS_Description', @v, N'SCHEMA', N'dbo', N'TABLE', N'Tmp_budgets', N'COLUMN', N'ending_date'
GO
DECLARE @v sql_variant 
SET @v = N'Type of the budget ("I" = Initial, "F" = follow-up)'
EXECUTE sp_addextendedproperty N'MS_Description', @v, N'SCHEMA', N'dbo', N'TABLE', N'Tmp_budgets', N'COLUMN', N'budget_type'
GO
DECLARE @v sql_variant 
SET @v = N'Date/Time when the row was created.'
EXECUTE sp_addextendedproperty N'MS_Description', @v, N'SCHEMA', N'dbo', N'TABLE', N'Tmp_budgets', N'COLUMN', N'date_created'
GO
DECLARE @v sql_variant 
SET @v = N'Person who created the row.'
EXECUTE sp_addextendedproperty N'MS_Description', @v, N'SCHEMA', N'dbo', N'TABLE', N'Tmp_budgets', N'COLUMN', N'created_by'
GO
EXECUTE sp_bindefault N'dbo.default_date', N'dbo.Tmp_budgets.starting_date'
GO
ALTER TABLE dbo.Tmp_budgets ADD CONSTRAINT DF_budgets_budget_type DEFAULT ('F') FOR budget_type
GO
SET IDENTITY_INSERT dbo.Tmp_budgets ON
GO
IF EXISTS(SELECT * FROM dbo.budgets)
	 EXEC('INSERT INTO dbo.Tmp_budgets (budget, client, starting_date, ending_date, budget_type, date_created, created_by)
		SELECT budget, client, starting_date, ending_date, budget_type, date_created, created_by FROM dbo.budgets WITH (HOLDLOCK TABLOCKX)')
GO
SET IDENTITY_INSERT dbo.Tmp_budgets OFF
GO
DROP TABLE dbo.budgets
GO
EXECUTE sp_rename N'dbo.Tmp_budgets', N'budgets', 'OBJECT' 
GO
ALTER TABLE dbo.budgets ADD CONSTRAINT
	PK_budgets PRIMARY KEY CLUSTERED 
	(
	budget
	) WITH( PAD_INDEX = OFF, FILLFACTOR = 90, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX IX_budgets_1 ON dbo.budgets
	(
	client
	) WITH( PAD_INDEX = OFF, FILLFACTOR = 90, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
EXEC ('DELETE budgets FROM budgets b LEFT OUTER JOIN clients c on b.client = c.client WHERE c.client IS NULL')
GO
EXEC ('ALTER TABLE dbo.budgets ADD CONSTRAINT FK_budgets_clients FOREIGN KEY (client) REFERENCES dbo.clients (client) ON UPDATE NO ACTION ON DELETE CASCADE')
GO
DENY DELETE ON dbo.budgets TO www_role  AS dbo 
GO
DENY INSERT ON dbo.budgets TO www_role  AS dbo 
GO
DENY SELECT ON dbo.budgets TO www_role  AS dbo 
GO
DENY UPDATE ON dbo.budgets TO www_role  AS dbo 
GO
COMMIT
BEGIN TRANSACTION
GO
EXEC ('DELETE dbo.budget_detail FROM dbo.budget_detail d LEFT OUTER JOIN dbo.budgets b on d.budget = b.budget where b.budget IS NULL')
GO
EXEC ('ALTER TABLE dbo.budget_detail ADD CONSTRAINT FK_budget_detail_budgets FOREIGN KEY (budget) REFERENCES dbo.budgets (budget) ON UPDATE NO ACTION ON DELETE CASCADE')
GO
COMMIT
