USE [DebtPlus]
GO
/****** Object:  Table [dbo].[appt_types]    Script Date: 09/15/2014 13:13:25 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[appt_types](
	[appt_type] [dbo].[typ_key] IDENTITY(1,1) NOT NULL,
	[appt_name] [dbo].[typ_description] NOT NULL,
	[appt_duration] [int] NOT NULL,
	[default] [bit] NOT NULL,
	[housing] [bit] NOT NULL,
	[credit] [bit] NOT NULL,
	[bankruptcy] [bit] NOT NULL,
	[initial_appt] [bit] NOT NULL,
	[contact_type] [dbo].[typ_key] NOT NULL,
	[missed_retention_event] [dbo].[typ_key] NULL,
	[create_letter] [dbo].[typ_letter_code] NULL,
	[reschedule_letter] [dbo].[typ_letter_code] NULL,
	[cancel_letter] [dbo].[typ_letter_code] NULL,
	[bankruptcy_class] [dbo].[typ_key] NOT NULL,
	[date_created] [dbo].[typ_date] NOT NULL,
	[created_by] [dbo].[typ_counselor] NOT NULL,
 CONSTRAINT [PK_appointment_type] PRIMARY KEY CLUSTERED 
(
	[appt_type] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Appointment requires certified counselors' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'appt_types', @level2type=N'COLUMN',@level2name=N'bankruptcy'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Is this an "initial" appointment as defined by the NCCRC reporting?' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'appt_types', @level2type=N'COLUMN',@level2name=N'initial_appt'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Bankruptcy type for default by this appointment type' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'appt_types', @level2type=N'COLUMN',@level2name=N'bankruptcy_class'
GO
ALTER TABLE [dbo].[appt_types] ADD  CONSTRAINT [DF_appt_types_appt_duration]  DEFAULT ((60)) FOR [appt_duration]
GO
ALTER TABLE [dbo].[appt_types] ADD  CONSTRAINT [DF_appt_types_contact_type]  DEFAULT ((1)) FOR [contact_type]
GO
ALTER TABLE [dbo].[appt_types] ADD  CONSTRAINT [DF_appt_types_bankruptcy_type]  DEFAULT ((-1)) FOR [bankruptcy_class]
GO
