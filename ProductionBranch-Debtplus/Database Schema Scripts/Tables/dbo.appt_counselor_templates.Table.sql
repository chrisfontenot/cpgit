USE [DebtPlus]
GO
/****** Object:  Table [dbo].[appt_counselor_templates]    Script Date: 09/15/2014 13:13:25 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[appt_counselor_templates](
	[appt_counselor_template] [dbo].[typ_key] IDENTITY(1,1) NOT NULL,
	[appt_time_template] [dbo].[typ_key] NOT NULL,
	[counselor] [dbo].[typ_key] NOT NULL,
	[date_created] [dbo].[typ_date] NOT NULL,
	[created_by] [dbo].[typ_counselor] NOT NULL,
 CONSTRAINT [PK_appt_counselor_templates] PRIMARY KEY CLUSTERED 
(
	[appt_counselor_template] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 90) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'ID for the row' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'appt_counselor_templates', @level2type=N'COLUMN',@level2name=N'appt_counselor_template'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Appointment time for the row' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'appt_counselor_templates', @level2type=N'COLUMN',@level2name=N'appt_time_template'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Counselor associated with the appointment time' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'appt_counselor_templates', @level2type=N'COLUMN',@level2name=N'counselor'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Date/Time when the row was created.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'appt_counselor_templates', @level2type=N'COLUMN',@level2name=N'date_created'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Person who created the row.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'appt_counselor_templates', @level2type=N'COLUMN',@level2name=N'created_by'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'For each appointment template, there are one or more counselors. This lists the counselors which are associated with a template appointment time. There is a similar table, appt_counselors, which lists appointment counselors that have been generated as appointment ''slots''.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'appt_counselor_templates'
GO
