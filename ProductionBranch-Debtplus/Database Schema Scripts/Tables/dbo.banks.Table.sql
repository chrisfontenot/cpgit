/* To prevent any potential data loss issues, you should review this script in detail before running it outside the context of the database designer.*/
BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION
GO
execute UPDATE_drop_constraints 'banks'
execute UPDATE_add_column 'banks', 'checknum',             'bigint'
execute UPDATE_add_column 'banks', 'max_amt_per_check',    'money'
execute UPDATE_add_column 'banks', 'check_number',         'int'
execute UPDATE_add_column 'banks', 'max_amount_per_check', 'int'
GO
CREATE TABLE dbo.Tmp_banks
	(
	bank dbo.typ_key NOT NULL IDENTITY (1, 1),
	description dbo.typ_description NOT NULL,
	[default] bit NOT NULL,
	ActiveFlag bit NOT NULL,
	type varchar(1) NOT NULL,
	ContactNameID dbo.typ_key NULL,
	bank_name varchar(80) NULL,
	BankAddressID dbo.typ_key NULL,
	aba dbo.typ_ach_routing NULL,
	account_number varchar(40) NULL,
	immediate_origin varchar(12) NULL,
	immediate_origin_name varchar(30) NULL,
	ach_priority varchar(2) NULL,
	ach_company_id varchar(10) NULL,
	ach_origin_dfi varchar(12) NULL,
	ach_enable_offset bit NOT NULL,
	ach_company_identification varchar(10) NULL,
	ach_message_authentication varchar(19) NULL,
	ach_batch_company_id varchar(10) NULL,
	immediate_destination varchar(12) NULL,
	immediate_destination_name varchar(30) NULL,
	checknum bigint NOT NULL,
	max_clients_per_check int NOT NULL,
	max_amt_per_check money NOT NULL,
	batch_number int NOT NULL,
	transaction_number int NOT NULL,
	SignatureImage image NULL,
	prefix_line varchar(256) NULL,
	suffix_line varchar(256) NULL,
	output_directory varchar(256) NULL,
	created_by dbo.typ_counselor NOT NULL,
	date_created dbo.typ_date NOT NULL,
	ts timestamp NOT NULL,
	check_number int NULL,
	max_amount_per_check int NULL
	)  ON [PRIMARY]
	 TEXTIMAGE_ON [PRIMARY]
GO
GRANT DELETE ON dbo.Tmp_banks TO public  AS dbo
GO
GRANT INSERT ON dbo.Tmp_banks TO public  AS dbo
GO
GRANT SELECT ON dbo.Tmp_banks TO public  AS dbo
GO
GRANT UPDATE ON dbo.Tmp_banks TO public  AS dbo
GO
DECLARE @v sql_variant 
SET @v = N'Primary key to the table'
EXECUTE sp_addextendedproperty N'MS_Description', @v, N'SCHEMA', N'dbo', N'TABLE', N'Tmp_banks', N'COLUMN', N'bank'
GO
DECLARE @v sql_variant 
SET @v = N'Description for the bank account'
EXECUTE sp_addextendedproperty N'MS_Description', @v, N'SCHEMA', N'dbo', N'TABLE', N'Tmp_banks', N'COLUMN', N'description'
GO
DECLARE @v sql_variant 
SET @v = N'Is this the default item for this type?'
EXECUTE sp_addextendedproperty N'MS_Description', @v, N'SCHEMA', N'dbo', N'TABLE', N'Tmp_banks', N'COLUMN', N'default'
GO
DECLARE @v sql_variant 
SET @v = N'Can this bank be used again?'
EXECUTE sp_addextendedproperty N'MS_Description', @v, N'SCHEMA', N'dbo', N'TABLE', N'Tmp_banks', N'COLUMN', N'ActiveFlag'
GO
DECLARE @v sql_variant 
SET @v = N'Type of the account (''C'' = checking, ''A'' = ACH, ''R'' = RPPS)'
EXECUTE sp_addextendedproperty N'MS_Description', @v, N'SCHEMA', N'dbo', N'TABLE', N'Tmp_banks', N'COLUMN', N'type'
GO
DECLARE @v sql_variant 
SET @v = N'Name of the bank'
EXECUTE sp_addextendedproperty N'MS_Description', @v, N'SCHEMA', N'dbo', N'TABLE', N'Tmp_banks', N'COLUMN', N'bank_name'
GO
DECLARE @v sql_variant 
SET @v = N'American Banking Association ID number'
EXECUTE sp_addextendedproperty N'MS_Description', @v, N'SCHEMA', N'dbo', N'TABLE', N'Tmp_banks', N'COLUMN', N'aba'
GO
DECLARE @v sql_variant 
SET @v = N'Account number at the bank'
EXECUTE sp_addextendedproperty N'MS_Description', @v, N'SCHEMA', N'dbo', N'TABLE', N'Tmp_banks', N'COLUMN', N'account_number'
GO
DECLARE @v sql_variant 
SET @v = N'For RPPS, this is the immediate origin number'
EXECUTE sp_addextendedproperty N'MS_Description', @v, N'SCHEMA', N'dbo', N'TABLE', N'Tmp_banks', N'COLUMN', N'immediate_origin'
GO
DECLARE @v sql_variant 
SET @v = N'Descriptive name for the immediate originator'
EXECUTE sp_addextendedproperty N'MS_Description', @v, N'SCHEMA', N'dbo', N'TABLE', N'Tmp_banks', N'COLUMN', N'immediate_origin_name'
GO
DECLARE @v sql_variant 
SET @v = N'For ACH, this is the priority code'
EXECUTE sp_addextendedproperty N'MS_Description', @v, N'SCHEMA', N'dbo', N'TABLE', N'Tmp_banks', N'COLUMN', N'ach_priority'
GO
DECLARE @v sql_variant 
SET @v = N'For ACH accounts, this is the company ID number'
EXECUTE sp_addextendedproperty N'MS_Description', @v, N'SCHEMA', N'dbo', N'TABLE', N'Tmp_banks', N'COLUMN', N'ach_company_id'
GO
DECLARE @v sql_variant 
SET @v = N'For ACH, this is the originating DFI identifcation number. It is normally "like" the ach_company_id, but is some cases, it is a different value.'
EXECUTE sp_addextendedproperty N'MS_Description', @v, N'SCHEMA', N'dbo', N'TABLE', N'Tmp_banks', N'COLUMN', N'ach_origin_dfi'
GO
DECLARE @v sql_variant 
SET @v = N'ACH: Do we generate a type "27" transaction for the total deposit amount?'
EXECUTE sp_addextendedproperty N'MS_Description', @v, N'SCHEMA', N'dbo', N'TABLE', N'Tmp_banks', N'COLUMN', N'ach_enable_offset'
GO
DECLARE @v sql_variant 
SET @v = N'For ACH. This is the the company identification that is used on some banks.'
EXECUTE sp_addextendedproperty N'MS_Description', @v, N'SCHEMA', N'dbo', N'TABLE', N'Tmp_banks', N'COLUMN', N'ach_company_identification'
GO
DECLARE @v sql_variant 
SET @v = N'For ACH: This is the message authentication used on some banks.'
EXECUTE sp_addextendedproperty N'MS_Description', @v, N'SCHEMA', N'dbo', N'TABLE', N'Tmp_banks', N'COLUMN', N'ach_message_authentication'
GO
DECLARE @v sql_variant 
SET @v = N'For ACH: This is the ID used on the batch records'
EXECUTE sp_addextendedproperty N'MS_Description', @v, N'SCHEMA', N'dbo', N'TABLE', N'Tmp_banks', N'COLUMN', N'ach_batch_company_id'
GO
DECLARE @v sql_variant 
SET @v = N'For EFT transfers, this is the destination ID for the bank.'
EXECUTE sp_addextendedproperty N'MS_Description', @v, N'SCHEMA', N'dbo', N'TABLE', N'Tmp_banks', N'COLUMN', N'immediate_destination'
GO
DECLARE @v sql_variant 
SET @v = N'For EFT transfers, this is the name associated with the immediate destination'
EXECUTE sp_addextendedproperty N'MS_Description', @v, N'SCHEMA', N'dbo', N'TABLE', N'Tmp_banks', N'COLUMN', N'immediate_destination_name'
GO
DECLARE @v sql_variant 
SET @v = N'Current check number for generating check transactions in the trust register'
EXECUTE sp_addextendedproperty N'MS_Description', @v, N'SCHEMA', N'dbo', N'TABLE', N'Tmp_banks', N'COLUMN', N'checknum'
GO
DECLARE @v sql_variant 
SET @v = N'Maximum number of clients to print on a check voucher if single spaced'
EXECUTE sp_addextendedproperty N'MS_Description', @v, N'SCHEMA', N'dbo', N'TABLE', N'Tmp_banks', N'COLUMN', N'max_clients_per_check'
GO
DECLARE @v sql_variant 
SET @v = N'Maximum dollar amount to be written on a standard (creditor or deduct) check.'
EXECUTE sp_addextendedproperty N'MS_Description', @v, N'SCHEMA', N'dbo', N'TABLE', N'Tmp_banks', N'COLUMN', N'max_amt_per_check'
GO
DECLARE @v sql_variant 
SET @v = N'Used in creating RPPS or ACH files, this is the next available batch number.'
EXECUTE sp_addextendedproperty N'MS_Description', @v, N'SCHEMA', N'dbo', N'TABLE', N'Tmp_banks', N'COLUMN', N'batch_number'
GO
DECLARE @v sql_variant 
SET @v = N'Used in creating RPPS or ACH files, this is the next available transaction number.'
EXECUTE sp_addextendedproperty N'MS_Description', @v, N'SCHEMA', N'dbo', N'TABLE', N'Tmp_banks', N'COLUMN', N'transaction_number'
GO
DECLARE @v sql_variant 
SET @v = N'Signature used in signing the checks for this account'
EXECUTE sp_addextendedproperty N'MS_Description', @v, N'SCHEMA', N'dbo', N'TABLE', N'Tmp_banks', N'COLUMN', N'SignatureImage'
GO
DECLARE @v sql_variant 
SET @v = N'When generating files, these lines are written before the output text file'
EXECUTE sp_addextendedproperty N'MS_Description', @v, N'SCHEMA', N'dbo', N'TABLE', N'Tmp_banks', N'COLUMN', N'prefix_line'
GO
DECLARE @v sql_variant 
SET @v = N'When generating output files, these lines are written after the data'
EXECUTE sp_addextendedproperty N'MS_Description', @v, N'SCHEMA', N'dbo', N'TABLE', N'Tmp_banks', N'COLUMN', N'suffix_line'
GO
DECLARE @v sql_variant 
SET @v = N'For EFT operations, this is the directory for this bank'
EXECUTE sp_addextendedproperty N'MS_Description', @v, N'SCHEMA', N'dbo', N'TABLE', N'Tmp_banks', N'COLUMN', N'output_directory'
GO
DECLARE @v sql_variant 
SET @v = N'Name of the person who created the row'
EXECUTE sp_addextendedproperty N'MS_Description', @v, N'SCHEMA', N'dbo', N'TABLE', N'Tmp_banks', N'COLUMN', N'created_by'
GO
DECLARE @v sql_variant 
SET @v = N'Date / time when the row was created'
EXECUTE sp_addextendedproperty N'MS_Description', @v, N'SCHEMA', N'dbo', N'TABLE', N'Tmp_banks', N'COLUMN', N'date_created'
GO
EXECUTE sp_bindefault N'dbo.default_false', N'dbo.Tmp_banks.[default]'
GO
EXECUTE sp_bindefault N'dbo.default_true', N'dbo.Tmp_banks.ActiveFlag'
GO
ALTER TABLE dbo.Tmp_banks ADD CONSTRAINT DF_banks_type DEFAULT ('C') FOR type
GO
ALTER TABLE dbo.Tmp_banks ADD CONSTRAINT DF_banks_checknum DEFAULT (101) FOR checknum
GO
ALTER TABLE dbo.Tmp_banks ADD CONSTRAINT DF_banks_ach_priority DEFAULT ('01') FOR ach_priority
GO
EXECUTE sp_bindefault N'dbo.default_false', N'dbo.Tmp_banks.ach_enable_offset'
GO
EXECUTE sp_bindefault N'dbo.default_zero', N'dbo.Tmp_banks.max_clients_per_check'
GO
EXECUTE sp_bindefault N'dbo.default_money', N'dbo.Tmp_banks.max_amt_per_check'
GO
EXECUTE sp_bindefault N'dbo.default_zero', N'dbo.Tmp_banks.batch_number'
GO
EXECUTE sp_bindefault N'dbo.default_zero', N'dbo.Tmp_banks.transaction_number'
GO
SET IDENTITY_INSERT dbo.Tmp_banks ON
GO
IF EXISTS(SELECT * FROM dbo.banks)
	 EXEC('INSERT INTO dbo.Tmp_banks (bank, description, [default], ActiveFlag, type, ContactNameID, bank_name, BankAddressID, aba, account_number, immediate_origin, immediate_origin_name, ach_priority, ach_company_id, ach_origin_dfi, ach_enable_offset, ach_company_identification, ach_message_authentication, ach_batch_company_id, immediate_destination, immediate_destination_name, checknum, max_clients_per_check, max_amt_per_check, batch_number, transaction_number, SignatureImage, prefix_line, suffix_line, output_directory, created_by, date_created, check_number, max_amount_per_check)
		SELECT bank, description, [default], ActiveFlag, type, ContactNameID, bank_name, BankAddressID, aba, account_number, immediate_origin, immediate_origin_name, ach_priority, ach_company_id, ach_origin_dfi, ach_enable_offset, ach_company_identification, ach_message_authentication, ach_batch_company_id, immediate_destination, immediate_destination_name, convert(bigint,check_number), max_clients_per_check, convert(money,max_amount_per_check), batch_number, transaction_number, SignatureImage, prefix_line, suffix_line, output_directory, created_by, date_created, check_number, max_amount_per_check FROM dbo.banks WITH (HOLDLOCK TABLOCKX)')
GO
SET IDENTITY_INSERT dbo.Tmp_banks OFF
GO
DROP TABLE dbo.banks
GO
EXECUTE sp_rename N'dbo.Tmp_banks', N'banks', 'OBJECT' 
GO
ALTER TABLE dbo.banks ADD CONSTRAINT
	PK_banks PRIMARY KEY CLUSTERED 
	(
	bank
	) WITH( PAD_INDEX = OFF, FILLFACTOR = 90, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
CREATE TRIGGER [dbo].[trig_banks_D] on dbo.banks AFTER DELETE as
-- ================================================================================
-- ==         Remove the table references when a bank account is removed         ==
-- ================================================================================

-- ChangeLog
--    1/1/2011
--       Initial version

set nocount on

-- Remove the contact name
delete	Names
from	Names n
inner join deleted d on n.Name = d.ContactNameID

-- Remove the contact address
delete	addresses
from	addresses a
inner join deleted d on a.address = d.BankAddressID
GO
COMMIT
GO
