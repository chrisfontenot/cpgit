USE [DebtPlus]
GO
/****** Object:  Table [dbo].[zip_msa]    Script Date: 09/15/2014 13:13:26 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[zip_msa](
	[zipcode] [nchar](10) NULL,
	[msa_name] [nchar](100) NULL
) ON [PRIMARY]
GO
