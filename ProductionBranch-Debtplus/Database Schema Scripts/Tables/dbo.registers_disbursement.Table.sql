USE [DebtPlus]
GO
/****** Object:  Table [dbo].[registers_disbursement]    Script Date: 09/15/2014 13:13:26 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[registers_disbursement](
	[disbursement_register] [dbo].[typ_key] IDENTITY(1,1) NOT NULL,
	[tran_type] [dbo].[typ_transaction] NOT NULL,
	[disbursement_date] [varchar](256) NULL,
	[region] [varchar](256) NULL,
	[prior_balance] [money] NULL,
	[post_balance] [money] NULL,
	[deduct_balance] [money] NULL,
	[date_posted] [datetime] NULL,
	[note] [dbo].[typ_message] NULL,
	[created_by] [dbo].[typ_counselor] NOT NULL,
	[date_created] [datetime] NOT NULL,
	[old_disbursement_register] [int] NULL,
 CONSTRAINT [PK_disbursment_batch_ids] PRIMARY KEY CLUSTERED 
(
	[disbursement_register] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 90) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Primary key to the disbursement table' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'registers_disbursement', @level2type=N'COLUMN',@level2name=N'disbursement_register'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Type of the disbursement. It may be ''AD'', ''MD'', or ''CM''.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'registers_disbursement', @level2type=N'COLUMN',@level2name=N'tran_type'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'For AD batches, this are the disbursement date(s) for the clients. Not used for manual disbursements.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'registers_disbursement', @level2type=N'COLUMN',@level2name=N'disbursement_date'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'For AD batches, this is the region(s) for the clients. Not used for manual disbursements.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'registers_disbursement', @level2type=N'COLUMN',@level2name=N'region'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'For AD batches, this is the trust balance when the batch was created. Not used on other types.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'registers_disbursement', @level2type=N'COLUMN',@level2name=N'prior_balance'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'For AD batches, this is the trust balance when the batch is closed. Not used on other types.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'registers_disbursement', @level2type=N'COLUMN',@level2name=N'post_balance'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'For AD batches, this is the value of the trust in client 0. Set prior to starting the post-disbursement process. Not used on other batch types.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'registers_disbursement', @level2type=N'COLUMN',@level2name=N'deduct_balance'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'When the disbursement is posted, this is the date/time. A closed batch is one which has a value.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'registers_disbursement', @level2type=N'COLUMN',@level2name=N'date_posted'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'General purpose text string to help identify the ''AD'' disbursement batch' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'registers_disbursement', @level2type=N'COLUMN',@level2name=N'note'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Person who created the row.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'registers_disbursement', @level2type=N'COLUMN',@level2name=N'created_by'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Date/Time when the row was created.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'registers_disbursement', @level2type=N'COLUMN',@level2name=N'date_created'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'This table contains the disbursement batch ID referenced in the registers_client, registers_creditor, and registers_client_creditor tables' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'registers_disbursement'
GO
ALTER TABLE [dbo].[registers_disbursement] ADD  CONSTRAINT [DF_registers_disbursement_tran_type]  DEFAULT ('CM') FOR [tran_type]
GO
