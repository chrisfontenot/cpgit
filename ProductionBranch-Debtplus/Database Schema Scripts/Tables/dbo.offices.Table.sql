USE [DebtPlus]
GO
/****** Object:  Table [dbo].[offices]    Script Date: 09/15/2014 13:13:26 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[offices](
	[office] [dbo].[typ_key] IDENTITY(1,1) NOT NULL,
	[name] [dbo].[typ_description] NOT NULL,
	[type] [int] NULL,
	[AddressID] [dbo].[typ_key] NULL,
	[EmailID] [dbo].[typ_key] NULL,
	[TelephoneID] [dbo].[typ_key] NULL,
	[AltTelephoneID] [dbo].[typ_key] NULL,
	[FAXID] [dbo].[typ_key] NULL,
	[Default] [bit] NOT NULL,
	[ActiveFlag] [bit] NOT NULL,
	[hud_hcs_id] [dbo].[typ_key] NULL,
	[TimeZoneID] [dbo].[typ_key] NOT NULL,
	[nfcc] [varchar](50) NULL,
	[default_till_missed] [int] NULL,
	[counselor] [dbo].[typ_key] NULL,
	[region] [dbo].[typ_key] NOT NULL,
	[district] [dbo].[typ_key] NOT NULL,
	[directions] [text] NULL,
	[date_created] [dbo].[typ_date] NOT NULL,
	[created_by] [dbo].[typ_counselor] NOT NULL,
	[old_office] [int] NULL,
 CONSTRAINT [PK_offices] PRIMARY KEY CLUSTERED 
(
	[office] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Office ID. This is the primary key used in clients / workshops / counselors / appointments tables.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'offices', @level2type=N'COLUMN',@level2name=N'office'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Name of the office' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'offices', @level2type=N'COLUMN',@level2name=N'name'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Type of the office. (0 = not an office, 1 = main office, 2 = satellite office, 3 = satellite desk)' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'offices', @level2type=N'COLUMN',@level2name=N'type'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Is this the default office?' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'offices', @level2type=N'COLUMN',@level2name=N'Default'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Is the office allowed to be assigned to new items?' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'offices', @level2type=N'COLUMN',@level2name=N'ActiveFlag'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'HUD ID associated with this office' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'offices', @level2type=N'COLUMN',@level2name=N'hud_hcs_id'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Value used in reporting this office to the NFCC' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'offices', @level2type=N'COLUMN',@level2name=N'nfcc'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'number of days that appointments for this office are allowed to exist before considered missing. This overrides the system default value.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'offices', @level2type=N'COLUMN',@level2name=N'default_till_missed'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Default counselor for this office' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'offices', @level2type=N'COLUMN',@level2name=N'counselor'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Region associated with this office' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'offices', @level2type=N'COLUMN',@level2name=N'region'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Pointer to the bankruptcy district for the office' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'offices', @level2type=N'COLUMN',@level2name=N'district'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'general driving directions to the office' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'offices', @level2type=N'COLUMN',@level2name=N'directions'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Date/Time when the row was created.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'offices', @level2type=N'COLUMN',@level2name=N'date_created'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Person who created the row.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'offices', @level2type=N'COLUMN',@level2name=N'created_by'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'This table lists the offices known to the system' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'offices'
GO
ALTER TABLE [dbo].[offices] ADD  CONSTRAINT [DF_offices_TimeZoneID]  DEFAULT ((12)) FOR [TimeZoneID]
GO
