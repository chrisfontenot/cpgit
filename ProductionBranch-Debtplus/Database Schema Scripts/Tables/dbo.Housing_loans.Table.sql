﻿/* To prevent any potential data loss issues, you should review this script in detail before running it outside the context of the database designer.*/
BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION
GO
CREATE TABLE dbo.Tmp_Housing_loans
	(
	oID dbo.typ_key NOT NULL IDENTITY (1, 1),
	PropertyID dbo.typ_key NOT NULL,
	Loan1st2nd dbo.typ_key NULL,
	UseOrigLenderID bit NOT NULL,
	CurrentLenderID dbo.typ_key NULL,
	OrigLenderID dbo.typ_key NULL,
	OrigLoanAmt money NULL,
	CurrentLoanBalanceAmt money NULL,
	IntakeSameAsCurrent bit NOT NULL,
	IntakeDetailID dbo.typ_key NULL,
	CurrentDetailID dbo.typ_key NULL,
	DsaDetailID dbo.typ_key NULL,
	MortgageClosingCosts money NULL,
	UseInReports bit NULL,
	LoanDelinquencyMonths int NOT NULL,
	LoanDelinquencyAmount money NOT NULL,
	LoanOriginationDate datetime NULL,
	[LoanDelinqStatusCD] dbo.typ_key NULL,
	ts timestamp NOT NULL
	)  ON [PRIMARY]
GO
ALTER TABLE dbo.Tmp_Housing_loans SET (LOCK_ESCALATION = TABLE)
GO
GRANT DELETE ON dbo.Tmp_Housing_loans TO public  AS dbo
GO
GRANT INSERT ON dbo.Tmp_Housing_loans TO public  AS dbo
GO
GRANT SELECT ON dbo.Tmp_Housing_loans TO public  AS dbo
GO
GRANT UPDATE ON dbo.Tmp_Housing_loans TO public  AS dbo
GO
EXECUTE sp_bindefault N'dbo.default_money', N'dbo.Tmp_Housing_loans.OrigLoanAmt'
GO
EXECUTE sp_bindefault N'dbo.default_money', N'dbo.Tmp_Housing_loans.CurrentLoanBalanceAmt'
GO
EXECUTE sp_bindefault N'dbo.default_money', N'dbo.Tmp_Housing_loans.MortgageClosingCosts'
GO
EXECUTE sp_bindefault N'dbo.default_false', N'dbo.Tmp_Housing_loans.UseInReports'
GO
EXECUTE sp_bindefault N'dbo.default_zero', N'dbo.Tmp_Housing_loans.LoanDelinquencyMonths'
GO
EXECUTE sp_bindefault N'dbo.default_money', N'dbo.Tmp_Housing_loans.LoanDelinquencyAmount'
GO
SET IDENTITY_INSERT dbo.Tmp_Housing_loans ON
GO
IF EXISTS(SELECT * FROM dbo.Housing_loans)
	 EXEC('INSERT INTO dbo.Tmp_Housing_loans (oID, PropertyID, Loan1st2nd, UseOrigLenderID, CurrentLenderID, OrigLenderID, OrigLoanAmt, CurrentLoanBalanceAmt, IntakeSameAsCurrent, IntakeDetailID, CurrentDetailID, MortgageClosingCosts, UseInReports, LoanDelinquencyMonths, LoanDelinquencyAmount, LoanOriginationDate, LoanDelinqStatusCD)
		SELECT oID, PropertyID, Loan1st2nd, UseOrigLenderID, CurrentLenderID, OrigLenderID, OrigLoanAmt, CurrentLoanBalanceAmt, IntakeSameAsCurrent, IntakeDetailID, CurrentDetailID, MortgageClosingCosts, UseInReports, LoanDelinquencyMonths, LoanDelinquencyAmount, LoanOriginationDate, LoanDelinqStatusCD FROM dbo.Housing_loans WITH (HOLDLOCK TABLOCKX)')
GO
SET IDENTITY_INSERT dbo.Tmp_Housing_loans OFF
GO
DROP TABLE dbo.Housing_loans
GO
EXECUTE sp_rename N'dbo.Tmp_Housing_loans', N'Housing_loans', 'OBJECT' 
GO
ALTER TABLE dbo.Housing_loans ADD CONSTRAINT
	PK_housing_loans PRIMARY KEY CLUSTERED 
	(
	oID
	) WITH( STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]

GO
CREATE NONCLUSTERED INDEX IX_housing_loans_1 ON dbo.Housing_loans
	(
	PropertyID
	) WITH( STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
CREATE TRIGGER [trig_housing_loans_D] on dbo.Housing_loans AFTER DELETE as
begin
	delete	housing_lenders
	from	housing_lenders l
	inner join deleted d on d.CurrentLenderID = l.oID
	where	d.CurrentLenderID is not null
	
	delete	housing_lenders
	from	housing_lenders l
	inner join deleted d on d.OrigLenderID = l.oID
	where	d.OrigLenderID is not null

	delete	housing_loan_details
	from	housing_loan_details l
	inner join deleted d on d.IntakeDetailID = l.oID
	where	d.IntakeDetailID is not null

	delete	housing_loan_details
	from	housing_loan_details l
	inner join deleted d on d.CurrentDetailID = l.oID
	where	d.CurrentDetailID is not null
end
GO
DENY DELETE ON dbo.Housing_loans TO www_role  AS dbo 
GO
DENY INSERT ON dbo.Housing_loans TO www_role  AS dbo 
GO
DENY SELECT ON dbo.Housing_loans TO www_role  AS dbo 
GO
DENY UPDATE ON dbo.Housing_loans TO www_role  AS dbo 
GO
COMMIT
