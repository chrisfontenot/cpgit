USE [DebtPlus]
GO
/****** Object:  Table [dbo].[client_deposits]    Script Date: 09/15/2014 13:13:26 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[client_deposits](
	[client_deposit] [dbo].[typ_key] IDENTITY(1,1) NOT NULL,
	[client] [dbo].[typ_client] NOT NULL,
	[deposit_date] [datetime] NOT NULL,
	[deposit_amount] [money] NOT NULL,
	[one_time] [bit] NOT NULL,
	[date_created] [dbo].[typ_date] NOT NULL,
	[created_by] [dbo].[typ_counselor] NOT NULL,
	[ach_pull] [bit] NOT NULL,
	[old_client_deposit] [int] NULL,
 CONSTRAINT [PK_client_payments] PRIMARY KEY CLUSTERED 
(
	[client_deposit] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 90) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Primary key to the table.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'client_deposits', @level2type=N'COLUMN',@level2name=N'client_deposit'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Client ID associated with the entry' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'client_deposits', @level2type=N'COLUMN',@level2name=N'client'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Date for the expected deposit' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'client_deposits', @level2type=N'COLUMN',@level2name=N'deposit_date'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Amount of the deposit' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'client_deposits', @level2type=N'COLUMN',@level2name=N'deposit_amount'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Is this a "one time" deposit. These deposits are deleted after being pulled for ACH.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'client_deposits', @level2type=N'COLUMN',@level2name=N'one_time'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Date/Time when the row was created.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'client_deposits', @level2type=N'COLUMN',@level2name=N'date_created'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Person who created the row.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'client_deposits', @level2type=N'COLUMN',@level2name=N'created_by'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Is the deposit scheduled for an ACH operation. Requires ACH on the client.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'client_deposits', @level2type=N'COLUMN',@level2name=N'ach_pull'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'This table shows the possible deposit dates and amounts that a client may have configured.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'client_deposits'
GO
