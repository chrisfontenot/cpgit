EXEC UPDATE_DROP_CONSTRAINTS 'secured_type_groups'
GO
/* To prevent any potential data loss issues, you should review this script in detail before running it outside the context of the database designer.*/
BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION
GO
CREATE TABLE dbo.Tmp_secured_type_groups
	(
	oID dbo.typ_key NOT NULL IDENTITY (1, 1),
	KeyType varchar(10) NULL,
	description dbo.typ_description NOT NULL,
	spanish_description nvarchar(50) NOT NULL,
	[Default] bit NOT NULL,
	ActiveFlag bit NOT NULL,
	date_created dbo.typ_date NOT NULL,
	created_by dbo.typ_counselor NOT NULL,
	ts timestamp NOT NULL
	)  ON [PRIMARY]
GO
GRANT DELETE ON dbo.Tmp_secured_type_groups TO public  AS dbo
GO
GRANT INSERT ON dbo.Tmp_secured_type_groups TO public  AS dbo
GO
GRANT SELECT ON dbo.Tmp_secured_type_groups TO public  AS dbo
GO
GRANT UPDATE ON dbo.Tmp_secured_type_groups TO public  AS dbo
GO
DECLARE @v sql_variant 
SET @v = N'Primary identifier for the table. This is the primary key.'
EXECUTE sp_addextendedproperty N'MS_Description', @v, N'SCHEMA', N'dbo', N'TABLE', N'Tmp_secured_type_groups', N'COLUMN', N'oID'
GO
DECLARE @v sql_variant 
SET @v = N'Date/Time when the row was created.'
EXECUTE sp_addextendedproperty N'MS_Description', @v, N'SCHEMA', N'dbo', N'TABLE', N'Tmp_secured_type_groups', N'COLUMN', N'date_created'
GO
DECLARE @v sql_variant 
SET @v = N'Person who created the row.'
EXECUTE sp_addextendedproperty N'MS_Description', @v, N'SCHEMA', N'dbo', N'TABLE', N'Tmp_secured_type_groups', N'COLUMN', N'created_by'
GO
EXECUTE sp_bindefault N'dbo.default_string', N'dbo.Tmp_secured_type_groups.description'
GO
EXECUTE sp_bindefault N'dbo.default_string', N'dbo.Tmp_secured_type_groups.spanish_description'
GO
EXECUTE sp_bindefault N'dbo.default_false', N'dbo.Tmp_secured_type_groups.[Default]'
GO
EXECUTE sp_bindefault N'dbo.default_true', N'dbo.Tmp_secured_type_groups.ActiveFlag'
GO
SET IDENTITY_INSERT dbo.Tmp_secured_type_groups ON
GO
IF EXISTS(SELECT * FROM dbo.secured_type_groups)
	 EXEC('INSERT INTO dbo.Tmp_secured_type_groups (oID, KeyType, description, spanish_description, [Default], ActiveFlag, date_created, created_by)
		SELECT oID, KeyType, description, spanish_description, [Default], ActiveFlag, date_created, created_by FROM dbo.secured_type_groups WITH (HOLDLOCK TABLOCKX)')
GO
SET IDENTITY_INSERT dbo.Tmp_secured_type_groups OFF
GO
DROP TABLE dbo.secured_type_groups
GO
EXECUTE sp_rename N'dbo.Tmp_secured_type_groups', N'secured_type_groups', 'OBJECT' 
GO
ALTER TABLE dbo.secured_type_groups ADD CONSTRAINT PK_secured_type_groups PRIMARY KEY CLUSTERED ( oID ) WITH( STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
DENY DELETE ON dbo.secured_type_groups TO www_role  AS dbo 
GO
DENY INSERT ON dbo.secured_type_groups TO www_role  AS dbo 
GO
DENY SELECT ON dbo.secured_type_groups TO www_role  AS dbo 
GO
DENY UPDATE ON dbo.secured_type_groups TO www_role  AS dbo 
GO
COMMIT
BEGIN TRANSACTION
EXEC('DELETE FROM secured_types WHERE secured_type_group NOT IN (SELECT oID from secured_type_groups)')
EXEC('ALTER TABLE dbo.secured_types ADD CONSTRAINT FK_secured_types_secured_type_groups FOREIGN KEY ( secured_type_group ) REFERENCES dbo.secured_type_groups ( oID ) ON UPDATE NO ACTION ON DELETE CASCADE')
COMMIT
