EXECUTE UPDATE_DROP_CONSTRAINTS 'product_resolution_types'
GO
/* To prevent any potential data loss issues, you should review this script in detail before running it outside the context of the database designer.*/
BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION
GO
CREATE TABLE dbo.Tmp_product_resolution_types
	(
	oID dbo.typ_key NOT NULL IDENTITY (1, 1),
	description dbo.typ_description NOT NULL,
	ActiveFlag bit NOT NULL,
	[Default] bit NOT NULL,
	date_created dbo.typ_date NOT NULL,
	created_by dbo.typ_counselor NOT NULL,
	ts timestamp NOT NULL
	)  ON [PRIMARY]
GO
ALTER TABLE dbo.Tmp_product_resolution_types SET (LOCK_ESCALATION = TABLE)
GO
EXECUTE sp_bindefault N'dbo.default_true', N'dbo.Tmp_product_resolution_types.ActiveFlag'
GO
EXECUTE sp_bindefault N'dbo.default_false', N'dbo.Tmp_product_resolution_types.[Default]'
GO
IF EXISTS(SELECT * FROM SYSOBJECTS WHERE TYPE='U' AND NAME=N'product_resolution_types')
BEGIN
	EXEC('SET IDENTITY_INSERT dbo.Tmp_product_resolution_types ON;
		  INSERT INTO dbo.Tmp_product_resolution_types (oID, description, ActiveFlag, [Default], date_created, created_by)
		  SELECT oID, description, ActiveFlag, [Default], date_created, created_by FROM dbo.product_resolution_types WITH (HOLDLOCK TABLOCKX);
		  SET IDENTITY_INSERT dbo.Tmp_product_resolution_types OFF;')
	EXEC('DROP TABLE dbo.product_resolution_types')
END
GO
EXECUTE sp_rename N'dbo.Tmp_product_resolution_types', N'product_resolution_types', 'OBJECT' 
GO
ALTER TABLE dbo.product_resolution_types ADD CONSTRAINT
	PK_product_resolution_types PRIMARY KEY CLUSTERED 
	(
	oID
	) WITH( STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
COMMIT
BEGIN TRANSACTION
GO
ALTER TABLE dbo.client_products ADD CONSTRAINT
	FK_client_products_product_resolution_types FOREIGN KEY
	(
	resolution_status
	) REFERENCES dbo.product_resolution_types
	(
	oID
	) ON UPDATE  NO ACTION 
	 ON DELETE  NO ACTION 
GO
ALTER TABLE dbo.client_products SET (LOCK_ESCALATION = TABLE)
GO
COMMIT
