USE [DEBTPLUS]
GO

/****** Object:  Table [dbo].[call_resolution]    Script Date: 2/9/2015 9:05:49 AM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[call_resolution](
	[oID] [dbo].[typ_key] IDENTITY(1,1) NOT NULL,
	[description] [dbo].[typ_description] NOT NULL,
	[nfcc] [varchar](4) NULL,
	[rpps] [varchar](4) NULL,
	[epay] [varchar](4) NULL,
	[note] [dbo].[typ_message] NULL,
	[hud_9902_section] [varchar](256) NULL,
	[default] [bit] NOT NULL CONSTRAINT [DF_call_resolution_default]  DEFAULT ((0)),
	[sortorder] [int] NOT NULL,
	[ActiveFlag] [bit] NOT NULL CONSTRAINT [DF_call_resolution_ActiveFlag]  DEFAULT ((1)),
	[date_created] [dbo].[typ_date] NOT NULL,
	[created_by] [dbo].[typ_counselor] NOT NULL,
	[ts] [timestamp] NOT NULL
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO


USE [DEBTPLUS]
GO

INSERT INTO [dbo].[call_resolution]([description],[sortorder],[date_created],[created_by]) VALUES ('Answered General Question', 1, getdate(), 'sa')
INSERT INTO [dbo].[call_resolution]([description],[sortorder],[date_created],[created_by]) VALUES ('Call Drop', 2, getdate(), 'sa')
INSERT INTO [dbo].[call_resolution]([description],[sortorder],[date_created],[created_by]) VALUES ('Cancel Availability', 3, getdate(), 'sa')
INSERT INTO [dbo].[call_resolution]([description],[sortorder],[date_created],[created_by]) VALUES ('Colonial Penn', 4, getdate(), 'sa')
INSERT INTO [dbo].[call_resolution]([description],[sortorder],[date_created],[created_by]) VALUES ('Referred to HUD (Rent, Repair)', 5, getdate(), 'sa')
INSERT INTO [dbo].[call_resolution]([description],[sortorder],[date_created],[created_by]) VALUES ('Remove from list', 6, getdate(), 'sa')
INSERT INTO [dbo].[call_resolution]([description],[sortorder],[date_created],[created_by]) VALUES ('Transferred to AARP FDN', 7, getdate(), 'sa')
INSERT INTO [dbo].[call_resolution]([description],[sortorder],[date_created],[created_by]) VALUES ('Transferred to Counselor', 8, getdate(), 'sa')
INSERT INTO [dbo].[call_resolution]([description],[sortorder],[date_created],[created_by]) VALUES ('Scheduled Appointment', 9, getdate(), 'sa')
INSERT INTO [dbo].[call_resolution]([description],[sortorder],[date_created],[created_by]) VALUES ('Rescheduled Appointment', 10, getdate(), 'sa')
GO



