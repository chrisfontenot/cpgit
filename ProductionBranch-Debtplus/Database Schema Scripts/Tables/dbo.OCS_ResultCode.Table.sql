USE [DebtPlus]
GO
/****** Object:  Table [dbo].[OCS_ResultCode]    Script Date: 06/18/2015 15:49:52 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[OCS_ResultCode](
	[Id] [dbo].[typ_key] IDENTITY(0,1) NOT NULL,
	[ShortCode] [varchar](10) NOT NULL,
	[Description] [dbo].[typ_description] NOT NULL,
	[ActiveFlag] [bit] NOT NULL,
	[IsRPC] [bit] NOT NULL,
	[IsCA] [bit] NOT NULL,
	[ts] [timestamp] NOT NULL,
 CONSTRAINT [PK_OCS_ResultCode] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF