USE [DebtPlus]
GO
/****** Object:  Table [dbo].[messages]    Script Date: 09/15/2014 13:13:26 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[messages](
	[message] [dbo].[typ_key] IDENTITY(1,1) NOT NULL,
	[item_type] [varchar](20) NOT NULL,
	[item_value] [int] NOT NULL,
	[additional] [varchar](256) NULL,
	[description] [dbo].[typ_description] NOT NULL,
	[nfcc] [varchar](4) NULL,
	[rpps] [varchar](4) NULL,
	[epay] [varchar](4) NULL,
	[note] [dbo].[typ_message] NULL,
	[default] [bit] NOT NULL,
	[ActiveFlag] [bit] NOT NULL,
	[hud_9902_section] [varchar](256) NULL,
	[date_created] [dbo].[typ_date] NOT NULL,
	[created_by] [dbo].[typ_counselor] NOT NULL,
	[old_item_value] [int] NULL,
 CONSTRAINT [PK_messages] PRIMARY KEY CLUSTERED 
(
	[message] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 90) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Primary key to the table. This is used only by the message editing tool. Others refer to the ''item_value'' field.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'messages', @level2type=N'COLUMN',@level2name=N'message'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Type of the message item. The type is well known to the programs and procedures. Change with caution.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'messages', @level2type=N'COLUMN',@level2name=N'item_type'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Value for the description.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'messages', @level2type=N'COLUMN',@level2name=N'item_value'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Additional qualification information. Used by ''HUD RESULTS'' and others.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'messages', @level2type=N'COLUMN',@level2name=N'additional'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Descriptive text of the ''item_value''.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'messages', @level2type=N'COLUMN',@level2name=N'description'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'NFCC response category id when it is needed. Used by some types.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'messages', @level2type=N'COLUMN',@level2name=N'nfcc'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Mastercard RPPS information used by some categories' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'messages', @level2type=N'COLUMN',@level2name=N'rpps'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'This is the message to be displayed with the value is choosen' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'messages', @level2type=N'COLUMN',@level2name=N'note'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Field ID for reporting these items to H.U.D.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'messages', @level2type=N'COLUMN',@level2name=N'hud_9902_section'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Date / Time when the row was created' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'messages', @level2type=N'COLUMN',@level2name=N'date_created'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Person who created the row.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'messages', @level2type=N'COLUMN',@level2name=N'created_by'
GO
