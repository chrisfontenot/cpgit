SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER TRIGGER [HPFSessions_I] on [HPFSessions] AFTER INSERT AS
BEGIN
	-- ======================================================================================
	-- ==             Record the ID and Timestamp of the last update to the rows           ==
	-- ======================================================================================

	-- Suppress intermediate results
	set nocount on

	-- Set the ID and date when the row was last updated
	update	HPFSessions
	set		date_changed	= getdate(),
			changed_by		= suser_sname()
	from	HPFSessions p
	inner join inserted i on p.oID = i.oID;
END
GO
