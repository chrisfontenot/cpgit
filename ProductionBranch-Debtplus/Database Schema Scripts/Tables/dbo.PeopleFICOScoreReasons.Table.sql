EXECUTE UPDATE_DROP_CONSTRAINTS 'PeopleFICOScoreReasons'
GO
/* To prevent any potential data loss issues, you should review this script in detail before running it outside the context of the database designer.*/
BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION
GO
CREATE TABLE dbo.Tmp_PeopleFICOScoreReasons
	(
	oID dbo.typ_key NOT NULL IDENTITY(1,1),
	PersonID dbo.typ_key NOT NULL,
	ScoreReasonID dbo.typ_key NOT NULL,
	ts timestamp NOT NULL
	)  ON [PRIMARY]
GO
GRANT DELETE ON dbo.Tmp_PeopleFICOScoreReasons TO public  AS dbo
GO
GRANT INSERT ON dbo.Tmp_PeopleFICOScoreReasons TO public  AS dbo
GO
GRANT SELECT ON dbo.Tmp_PeopleFICOScoreReasons TO public  AS dbo
GO
GRANT UPDATE ON dbo.Tmp_PeopleFICOScoreReasons TO public  AS dbo
GO
SET IDENTITY_INSERT Tmp_PeopleFICOScoreReasons ON;
GO
IF EXISTS(SELECT * FROM SYSOBJECTS WHERE TYPE='U' AND NAME=N'PeopleFICOScoreReasons')
BEGIN
	EXEC('INSERT INTO dbo.Tmp_PeopleFICOScoreReasons (oID, PersonID, ScoreReasonID)
		SELECT oID, PersonID, ScoreReasonID FROM dbo.PeopleFICOScoreReasons WITH (HOLDLOCK TABLOCKX)')
	EXEC('DROP TABLE dbo.PeopleFICOScoreReasons')
END
GO
SET IDENTITY_INSERT Tmp_PeopleFICOScoreReasons OFF;
GO
EXECUTE sp_rename N'dbo.Tmp_PeopleFICOScoreReasons', N'PeopleFICOScoreReasons', 'OBJECT' 
GO
ALTER TABLE dbo.PeopleFICOScoreReasons ADD CONSTRAINT
	PK_PeopleFICOScoreReasons PRIMARY KEY CLUSTERED 
	(
	oID
	) WITH( STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
ALTER TABLE dbo.PeopleFICOScoreReasons ADD CONSTRAINT
	FK_PeopleFICOScoreReasons_FICOScoreReasonCodes FOREIGN KEY
	(
	ScoreReasonID
	) REFERENCES dbo.FICOScoreReasonCodes
	(
	oID
	) ON UPDATE  CASCADE 
	 ON DELETE  CASCADE 
GO
ALTER TABLE dbo.PeopleFICOScoreReasons ADD CONSTRAINT
	FK_PeopleFICOScoreReasons_people FOREIGN KEY
	(
	PersonID
	) REFERENCES dbo.people
	(
	Person
	) ON UPDATE  CASCADE 
	 ON DELETE  CASCADE 
GO
DENY INSERT,SELECT,UPDATE,DELETE ON dbo.PeopleFICOScoreReasons TO www_role  AS dbo 
GO
COMMIT
