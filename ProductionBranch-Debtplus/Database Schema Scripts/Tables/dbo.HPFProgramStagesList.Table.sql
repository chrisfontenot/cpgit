/* To prevent any potential data loss issues, you should review this script in detail before running it outside the context of the database designer.*/
BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION
GO
CREATE TABLE dbo.Tmp_HPFProgramStagesList
	(
	oID dbo.typ_key NOT NULL IDENTITY (1, 1),
	ProgramID dbo.typ_key NOT NULL,
	StageID dbo.typ_key NOT NULL,
	[default] bit NOT NULL,
	ts timestamp NOT NULL
	)  ON [PRIMARY]
GO
GRANT DELETE ON dbo.Tmp_HPFProgramStagesList TO public  AS dbo
GO
GRANT INSERT ON dbo.Tmp_HPFProgramStagesList TO public  AS dbo
GO
GRANT SELECT ON dbo.Tmp_HPFProgramStagesList TO public  AS dbo
GO
GRANT UPDATE ON dbo.Tmp_HPFProgramStagesList TO public  AS dbo
GO
EXECUTE sp_bindefault N'dbo.default_false', N'dbo.Tmp_HPFProgramStagesList.[default]'
GO
SET IDENTITY_INSERT dbo.Tmp_HPFProgramStagesList ON
GO
IF EXISTS(SELECT * FROM sysobjects WHERE name=N'HPFProgramStagesList' AND type='U')
BEGIN
	EXEC('INSERT INTO dbo.Tmp_HPFProgramStagesList (oID, ProgramID, StageID, [default])
		SELECT oID, ProgramID, StageID, [default] FROM dbo.HPFProgramStagesList WITH (HOLDLOCK TABLOCKX)')
	EXEC('DROP TABLE dbo.HPFProgramStagesList')
END
GO
SET IDENTITY_INSERT dbo.Tmp_HPFProgramStagesList OFF
GO
EXECUTE sp_rename N'dbo.Tmp_HPFProgramStagesList', N'HPFProgramStagesList', 'OBJECT' 
GO
ALTER TABLE dbo.HPFProgramStagesList ADD CONSTRAINT
	PK_HPFProgramStagesList PRIMARY KEY CLUSTERED 
	(
	oID
	) WITH( STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
DENY DELETE ON dbo.HPFProgramStagesList TO www_role  AS dbo 
GO
DENY INSERT ON dbo.HPFProgramStagesList TO www_role  AS dbo 
GO
DENY SELECT ON dbo.HPFProgramStagesList TO www_role  AS dbo 
GO
DENY UPDATE ON dbo.HPFProgramStagesList TO www_role  AS dbo 
GO
COMMIT
