/* To prevent any potential data loss issues, you should review this script in detail before running it outside the context of the database designer.*/
BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION
GO
CREATE TABLE dbo.Tmp_Housing_ReasonForCallTypes
	(
	oID dbo.typ_key NOT NULL IDENTITY (1, 1),
	description dbo.typ_description NOT NULL,
	hpf varchar(50) NULL,
	[default] bit NOT NULL,
	ActiveFlag bit NOT NULL,
	date_created dbo.typ_date NOT NULL,
	created_by dbo.typ_counselor NOT NULL,
	ts timestamp NOT NULL
	)  ON [PRIMARY]
GO
EXECUTE sp_bindefault N'dbo.default_false', N'dbo.Tmp_Housing_ReasonForCallTypes.[default]'
GO
EXECUTE sp_bindefault N'dbo.default_true', N'dbo.Tmp_Housing_ReasonForCallTypes.ActiveFlag'
GO
SET IDENTITY_INSERT dbo.Tmp_Housing_ReasonForCallTypes ON
GO
IF EXISTS(SELECT * FROM SYSOBJECTS WHERE NAME=N'Housing_ReasonForCallTypes' AND TYPE='U')
BEGIN
	EXEC('INSERT INTO dbo.Tmp_Housing_ReasonForCallTypes (oID, description, hpf, [default], ActiveFlag, date_created, created_by)
		SELECT oID, description, hpf, [default], ActiveFlag, date_created, created_by FROM dbo.Housing_ReasonForCallTypes WITH (HOLDLOCK TABLOCKX)')
	EXEC('DROP TABLE dbo.Housing_ReasonForCallTypes')
END
GO
SET IDENTITY_INSERT dbo.Tmp_Housing_ReasonForCallTypes OFF
GO
EXECUTE sp_rename N'dbo.Tmp_Housing_ReasonForCallTypes', N'Housing_ReasonForCallTypes', 'OBJECT' 
GO
ALTER TABLE dbo.Housing_ReasonForCallTypes ADD CONSTRAINT
	PK_Housing_ReasonForCallTypes PRIMARY KEY CLUSTERED 
	(
	oID
	) WITH( STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
COMMIT
GO
