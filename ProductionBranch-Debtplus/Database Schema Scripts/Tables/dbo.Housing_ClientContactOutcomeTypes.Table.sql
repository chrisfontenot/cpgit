/* To prevent any potential data loss issues, you should review this script in detail before running it outside the context of the database designer.*/
BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION
GO
CREATE TABLE dbo.Tmp_Housing_ClientContactOutcomeTypes
	(
	oID dbo.typ_key NOT NULL IDENTITY (1, 1),
	description dbo.typ_description NOT NULL,
	[default] bit NOT NULL,
	ActiveFlag bit NOT NULL,
	hpf varchar(50) NULL,
	date_created dbo.typ_date NOT NULL,
	created_by dbo.typ_counselor NOT NULL,
	ts timestamp NOT NULL
	)  ON [PRIMARY]
GO
GRANT DELETE ON dbo.Tmp_Housing_ClientContactOutcomeTypes TO public  AS dbo
GO
GRANT INSERT ON dbo.Tmp_Housing_ClientContactOutcomeTypes TO public  AS dbo
GO
GRANT SELECT ON dbo.Tmp_Housing_ClientContactOutcomeTypes TO public  AS dbo
GO
GRANT UPDATE ON dbo.Tmp_Housing_ClientContactOutcomeTypes TO public  AS dbo
GO
EXECUTE sp_bindefault N'dbo.default_false', N'dbo.Tmp_Housing_ClientContactOutcomeTypes.[default]'
GO
EXECUTE sp_bindefault N'dbo.default_true', N'dbo.Tmp_Housing_ClientContactOutcomeTypes.ActiveFlag'
GO
SET IDENTITY_INSERT dbo.Tmp_Housing_ClientContactOutcomeTypes ON
GO
IF EXISTS(SELECT * FROM SYSOBJECTS WHERE NAME=N'Housing_ClientContactOutcomeTypes' AND TYPE='U')
BEGIN
	 EXEC('INSERT INTO dbo.Tmp_Housing_ClientContactOutcomeTypes (oID, description, [default], ActiveFlag, hpf, date_created, created_by)
		SELECT oID, description, [default], ActiveFlag, hpf, date_created, created_by FROM dbo.Housing_ClientContactOutcomeTypes WITH (HOLDLOCK TABLOCKX)')
	 EXEC('DROP TABLE dbo.Housing_ClientContactOutcomeTypes')
END
GO
SET IDENTITY_INSERT dbo.Tmp_Housing_ClientContactOutcomeTypes OFF
GO
EXECUTE sp_rename N'dbo.Tmp_Housing_ClientContactOutcomeTypes', N'Housing_ClientContactOutcomeTypes', 'OBJECT' 
GO
ALTER TABLE dbo.Housing_ClientContactOutcomeTypes ADD CONSTRAINT
	PK_Housing_ClientContactOutcomeTypes PRIMARY KEY CLUSTERED 
	(
	oID
	) WITH( STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
DENY DELETE ON dbo.Housing_ClientContactOutcomeTypes TO www_role  AS dbo 
GO
DENY INSERT ON dbo.Housing_ClientContactOutcomeTypes TO www_role  AS dbo 
GO
DENY SELECT ON dbo.Housing_ClientContactOutcomeTypes TO www_role  AS dbo 
GO
DENY UPDATE ON dbo.Housing_ClientContactOutcomeTypes TO www_role  AS dbo 
GO
COMMIT
