EXECUTE UPDATE_DROP_CONSTRAINTS 'secured_properties'
GO
/* To prevent any potential data loss issues, you should review this script in detail before running it outside the context of the database designer.*/
BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION
GO
CREATE TABLE dbo.Tmp_secured_properties
	(
	secured_property dbo.typ_key NOT NULL IDENTITY (1, 1),
	client dbo.typ_client NOT NULL,
	class dbo.typ_key NOT NULL,
	secured_type dbo.typ_key NOT NULL,
	school_name dbo.typ_description NULL,
	studentLoan_type_id dbo.typ_key NULL,
	general_outcome_id dbo.typ_key NULL,
	type_of_workout_id dbo.typ_key NULL,
	borrower_id dbo.typ_key NULL,
	description dbo.typ_description NOT NULL,
	sub_description dbo.typ_description NOT NULL,
	original_price money NOT NULL,
	current_value money NULL,
	year_acquired int NOT NULL,
	year_mfg int NOT NULL,
	housing_type dbo.typ_key NOT NULL,
	primary_residence bit NOT NULL,
	created_by dbo.typ_counselor NOT NULL,
	date_created dbo.typ_date NOT NULL,
	ts timestamp NOT NULL
	)  ON [PRIMARY]
GO
GRANT DELETE ON dbo.Tmp_secured_properties TO public  AS dbo
GO
GRANT INSERT ON dbo.Tmp_secured_properties TO public  AS dbo
GO
GRANT SELECT ON dbo.Tmp_secured_properties TO public  AS dbo
GO
GRANT UPDATE ON dbo.Tmp_secured_properties TO public  AS dbo
GO
DECLARE @v sql_variant 
SET @v = N'Primary key to the table'
EXECUTE sp_addextendedproperty N'MS_Description', @v, N'SCHEMA', N'dbo', N'TABLE', N'Tmp_secured_properties', N'COLUMN', N'secured_property'
GO
DECLARE @v sql_variant 
SET @v = N'Client ID associated with this secured asset/liability'
EXECUTE sp_addextendedproperty N'MS_Description', @v, N'SCHEMA', N'dbo', N'TABLE', N'Tmp_secured_properties', N'COLUMN', N'client'
GO
DECLARE @v sql_variant 
SET @v = N'Pointer to the class of the asset'
EXECUTE sp_addextendedproperty N'MS_Description', @v, N'SCHEMA', N'dbo', N'TABLE', N'Tmp_secured_properties', N'COLUMN', N'class'
GO
DECLARE @v sql_variant 
SET @v = N'Type of the secured asset'
EXECUTE sp_addextendedproperty N'MS_Description', @v, N'SCHEMA', N'dbo', N'TABLE', N'Tmp_secured_properties', N'COLUMN', N'secured_type'
GO
DECLARE @v sql_variant 
SET @v = N'Primary description associated with the asset'
EXECUTE sp_addextendedproperty N'MS_Description', @v, N'SCHEMA', N'dbo', N'TABLE', N'Tmp_secured_properties', N'COLUMN', N'description'
GO
DECLARE @v sql_variant 
SET @v = N'If a sub-description (i.e. auto "model") is used, it is stored here.'
EXECUTE sp_addextendedproperty N'MS_Description', @v, N'SCHEMA', N'dbo', N'TABLE', N'Tmp_secured_properties', N'COLUMN', N'sub_description'
GO
DECLARE @v sql_variant 
SET @v = N'Purchase price'
EXECUTE sp_addextendedproperty N'MS_Description', @v, N'SCHEMA', N'dbo', N'TABLE', N'Tmp_secured_properties', N'COLUMN', N'original_price'
GO
DECLARE @v sql_variant 
SET @v = N'Current value if it is known'
EXECUTE sp_addextendedproperty N'MS_Description', @v, N'SCHEMA', N'dbo', N'TABLE', N'Tmp_secured_properties', N'COLUMN', N'current_value'
GO
DECLARE @v sql_variant 
SET @v = N'When was the asset obtained'
EXECUTE sp_addextendedproperty N'MS_Description', @v, N'SCHEMA', N'dbo', N'TABLE', N'Tmp_secured_properties', N'COLUMN', N'year_acquired'
GO
DECLARE @v sql_variant 
SET @v = N'When was the asset manufactured/built'
EXECUTE sp_addextendedproperty N'MS_Description', @v, N'SCHEMA', N'dbo', N'TABLE', N'Tmp_secured_properties', N'COLUMN', N'year_mfg'
GO
DECLARE @v sql_variant 
SET @v = N'If house, what type of house is it?'
EXECUTE sp_addextendedproperty N'MS_Description', @v, N'SCHEMA', N'dbo', N'TABLE', N'Tmp_secured_properties', N'COLUMN', N'housing_type'
GO
DECLARE @v sql_variant 
SET @v = N'Is this the primary residence?'
EXECUTE sp_addextendedproperty N'MS_Description', @v, N'SCHEMA', N'dbo', N'TABLE', N'Tmp_secured_properties', N'COLUMN', N'primary_residence'
GO
DECLARE @v sql_variant 
SET @v = N'Person who created the row'
EXECUTE sp_addextendedproperty N'MS_Description', @v, N'SCHEMA', N'dbo', N'TABLE', N'Tmp_secured_properties', N'COLUMN', N'created_by'
GO
DECLARE @v sql_variant 
SET @v = N'When was the row created'
EXECUTE sp_addextendedproperty N'MS_Description', @v, N'SCHEMA', N'dbo', N'TABLE', N'Tmp_secured_properties', N'COLUMN', N'date_created'
GO
EXECUTE sp_bindefault N'dbo.default_zero', N'dbo.Tmp_secured_properties.class'
GO
EXECUTE sp_bindefault N'dbo.default_string', N'dbo.Tmp_secured_properties.description'
GO
EXECUTE sp_bindefault N'dbo.default_string', N'dbo.Tmp_secured_properties.sub_description'
GO
EXECUTE sp_bindefault N'dbo.default_money', N'dbo.Tmp_secured_properties.original_price'
GO
EXECUTE sp_bindefault N'dbo.default_money', N'dbo.Tmp_secured_properties.current_value'
GO
EXECUTE sp_bindefault N'dbo.default_zero', N'dbo.Tmp_secured_properties.year_acquired'
GO
EXECUTE sp_bindefault N'dbo.default_zero', N'dbo.Tmp_secured_properties.year_mfg'
GO
EXECUTE sp_bindefault N'dbo.default_zero', N'dbo.Tmp_secured_properties.housing_type'
GO
EXECUTE sp_bindefault N'dbo.default_false', N'dbo.Tmp_secured_properties.primary_residence'
GO
SET IDENTITY_INSERT dbo.Tmp_secured_properties ON
GO
IF EXISTS(SELECT * FROM dbo.secured_properties)
	 EXEC('INSERT INTO dbo.Tmp_secured_properties (secured_property, client, class, secured_type, school_name, studentLoan_type_id, general_outcome_id, type_of_workout_id, borrower_id, description, sub_description, original_price, current_value, year_acquired, year_mfg, housing_type, primary_residence, created_by, date_created)
		SELECT secured_property, client, class, secured_type, school_name, studentLoan_type_id, general_outcome_id, type_of_workout_id, borrower_id, description, sub_description, original_price, current_value, year_acquired, year_mfg, housing_type, primary_residence, created_by, date_created FROM dbo.secured_properties WITH (HOLDLOCK TABLOCKX)')
GO
SET IDENTITY_INSERT dbo.Tmp_secured_properties OFF
GO
DROP TABLE dbo.secured_properties
GO
EXECUTE sp_rename N'dbo.Tmp_secured_properties', N'secured_properties', 'OBJECT' 
GO
ALTER TABLE dbo.secured_properties ADD CONSTRAINT
	PK_secured_properties PRIMARY KEY CLUSTERED 
	(
	secured_property
	) WITH( PAD_INDEX = OFF, FILLFACTOR = 90, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX IX_secured_properties_1 ON dbo.secured_properties
	(
	client
	) WITH( PAD_INDEX = OFF, FILLFACTOR = 90, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
EXEC('delete secured_properties FROM secured_properties p left outer join secured_types t on p.secured_type = t.secured_type where t.secured_type is null')
ALTER TABLE dbo.secured_properties ADD CONSTRAINT
	FK_secured_properties_secured_types FOREIGN KEY
	(
	secured_type
	) REFERENCES dbo.secured_types
	(
	secured_type
	) ON UPDATE  NO ACTION 
	 ON DELETE  CASCADE 
GO
DENY DELETE ON dbo.secured_properties TO www_role  AS dbo 
GO
DENY INSERT ON dbo.secured_properties TO www_role  AS dbo 
GO
DENY SELECT ON dbo.secured_properties TO www_role  AS dbo 
GO
DENY UPDATE ON dbo.secured_properties TO www_role  AS dbo 
GO
COMMIT
EXEC('DELETE FROM secured_loans WHERE secured_property NOT IN (SELECT secured_property FROM secured_properties)')
GO
EXEC('ALTER TABLE dbo.secured_loans ADD CONSTRAINT FK_secured_loans_secured_properties FOREIGN KEY ( secured_property ) REFERENCES dbo.secured_properties ( secured_property ) ON UPDATE NO ACTION ON DELETE CASCADE')
GO
