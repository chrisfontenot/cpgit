EXECUTE UPDATE_DROP_CONSTRAINTS 'product_transactions'
GO
/* To prevent any potential data loss issues, you should review this script in detail before running it outside the context of the database designer.*/
BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION
GO
ALTER TABLE dbo.vendors SET (LOCK_ESCALATION = TABLE)
GO
CREATE TABLE dbo.Tmp_product_transactions
	(
	oID dbo.typ_key NOT NULL IDENTITY (1, 1),
	vendor dbo.typ_key NULL,
	product dbo.typ_key NULL,
	client_product dbo.typ_key NULL,
	transaction_type varchar(2) NOT NULL,
	cost money NOT NULL,
	discount money NOT NULL,
	payment money NOT NULL,
	disputed money NOT NULL,
	note varchar(50) NULL,
	date_created dbo.typ_date NOT NULL,
	created_by dbo.typ_counselor NOT NULL,
	ts timestamp NOT NULL
	)  ON [PRIMARY]
GO
ALTER TABLE dbo.Tmp_product_transactions SET (LOCK_ESCALATION = TABLE)
GO
EXECUTE sp_bindefault N'dbo.default_money', N'dbo.Tmp_product_transactions.cost'
GO
EXECUTE sp_bindefault N'dbo.default_money', N'dbo.Tmp_product_transactions.discount'
GO
EXECUTE sp_bindefault N'dbo.default_money', N'dbo.Tmp_product_transactions.payment'
GO
EXECUTE sp_bindefault N'dbo.default_money', N'dbo.Tmp_product_transactions.disputed'
GO
IF EXISTS(SELECT * FROM SYSOBJECTS WHERE TYPE='U' AND NAME=N'product_transactions')
BEGIN
	EXEC('SET IDENTITY_INSERT dbo.Tmp_product_transactions ON;
		  INSERT INTO dbo.Tmp_product_transactions (oID, vendor, product, client_product, transaction_type, cost, discount, payment, disputed, note, date_created, created_by)
		  SELECT oID, vendor, product, client_product, transaction_type, cost, discount, payment, disputed, note, date_created, created_by FROM dbo.product_transactions WITH (HOLDLOCK TABLOCKX);
		  SET IDENTITY_INSERT dbo.Tmp_product_transactions OFF;')
	EXEC('DROP TABLE dbo.product_transactions')
END
GO
EXECUTE sp_rename N'dbo.Tmp_product_transactions', N'product_transactions', 'OBJECT' 
GO
ALTER TABLE dbo.product_transactions ADD CONSTRAINT
	PK_product_transactions PRIMARY KEY CLUSTERED 
	(
	oID
	) WITH( STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX IX_product_transactions_1 ON dbo.product_transactions
	(
	product,
	transaction_type
	) WITH( STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
ALTER TABLE dbo.product_transactions ADD CONSTRAINT
	CK_product_transactions CHECK ((NOT ([vendor] IS NULL AND [product] IS NULL AND [client_product] IS NULL)))
GO
DECLARE @v sql_variant 
SET @v = N'Ensure that there is at least one primary reference in the transaction log. Empty records are not allowed.'
EXECUTE sp_addextendedproperty N'MS_Description', @v, N'SCHEMA', N'dbo', N'TABLE', N'product_transactions', N'CONSTRAINT', N'CK_product_transactions'
GO
ALTER TABLE dbo.product_transactions ADD CONSTRAINT
	FK_product_transactions_vendors FOREIGN KEY
	(
	vendor
	) REFERENCES dbo.vendors
	(
	oID
	) ON UPDATE  NO ACTION 
	 ON DELETE  NO ACTION 
GO
ALTER TABLE dbo.product_transactions ADD CONSTRAINT
	FK_product_transactions_products FOREIGN KEY
	(
	product
	) REFERENCES dbo.products
	(
	oID
	) ON UPDATE  NO ACTION 
	 ON DELETE  NO ACTION 
GO
ALTER TABLE dbo.product_transactions ADD CONSTRAINT
	FK_product_transactions_client_products FOREIGN KEY
	(
	client_product
	) REFERENCES dbo.client_products
	(
	oID
	) ON UPDATE  NO ACTION 
	 ON DELETE  NO ACTION 
GO
ALTER TABLE dbo.product_transactions ADD CONSTRAINT
	FK_product_transactions_vendor_transaction_types FOREIGN KEY
	(
	transaction_type
	) REFERENCES dbo.product_transaction_types
	(
	oID
	) ON UPDATE  NO ACTION 
	 ON DELETE  NO ACTION 
GO
COMMIT
