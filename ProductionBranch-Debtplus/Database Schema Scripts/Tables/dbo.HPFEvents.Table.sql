/* To prevent any potential data loss issues, you should review this script in detail before running it outside the context of the database designer.*/
BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION
GO
CREATE TABLE dbo.Tmp_HPFEvents
	(
	oID dbo.typ_key NOT NULL IDENTITY (1, 1),
	property dbo.typ_key NOT NULL,
	fcID dbo.typ_key NULL,
	eventTypeCD dbo.typ_key NULL,
	eventOutcomeCD dbo.typ_key NULL,
	comments dbo.typ_message NULL,
	completedIND bit NOT NULL,
	optOutReasonCD dbo.typ_key NULL,
	programRefusalDT datetime NULL,
	programStageID dbo.typ_key NULL,
	rpcIND bit NOT NULL,
	eventID dbo.typ_key NULL,
	created_by dbo.typ_counselor NOT NULL,
	date_created dbo.typ_date NOT NULL,
	changed_by varchar(80) NOT NULL,
	date_changed datetime NOT NULL,
	ts timestamp NOT NULL
	)  ON [PRIMARY]
GO
GRANT DELETE ON dbo.Tmp_HPFEvents TO public  AS dbo
GO
GRANT INSERT ON dbo.Tmp_HPFEvents TO public  AS dbo
GO
GRANT SELECT ON dbo.Tmp_HPFEvents TO public  AS dbo
GO
GRANT UPDATE ON dbo.Tmp_HPFEvents TO public  AS dbo
GO
EXECUTE sp_bindefault N'dbo.default_false', N'dbo.Tmp_HPFEvents.completedIND'
GO
EXECUTE sp_bindefault N'dbo.default_false', N'dbo.Tmp_HPFEvents.rpcIND'
GO
EXECUTE sp_bindefault N'dbo.default_counselor', N'dbo.Tmp_HPFEvents.changed_by'
GO
EXECUTE sp_bindefault N'dbo.default_date', N'dbo.Tmp_HPFEvents.date_changed'
GO
SET IDENTITY_INSERT dbo.Tmp_HPFEvents ON
GO
IF EXISTS(SELECT * FROM sysobjects WHERE name=N'HPFEvents' AND type='U')
BEGIN
	EXEC('INSERT INTO dbo.Tmp_HPFEvents (oID, property, fcID, eventTypeCD, eventOutcomeCD, comments, completedIND, optOutReasonCD, programRefusalDT, programStageID, rpcIND, eventID, created_by, date_created, changed_by, date_changed)
		SELECT oID, property, fcID, eventTypeCD, eventOutcomeCD, comments, completedIND, optOutReasonCD, programRefusalDT, programStageID, rpcIND, eventID, created_by, date_created, changed_by, date_changed FROM dbo.HPFEvents WITH (HOLDLOCK TABLOCKX)')
	EXEC('DROP TABLE dbo.HPFEvents')
END
GO
SET IDENTITY_INSERT dbo.Tmp_HPFEvents OFF
GO
EXECUTE sp_rename N'dbo.Tmp_HPFEvents', N'HPFEvents', 'OBJECT' 
GO
ALTER TABLE dbo.HPFEvents ADD CONSTRAINT
	PK_HPFEvents PRIMARY KEY CLUSTERED 
	(
	oID
	) WITH( STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
CREATE trigger [dbo].[trig_HPFEvents_U] ON dbo.HPFEvents AFTER UPDATE AS
BEGIN
	set nocount on

	-- set the updated status appropriately
	if not update(date_changed) and not update(changed_by)
	begin
		update	HPFEvents
		set		date_changed = getdate(), changed_by = suser_sname()
		FROM	HPFEvents e
		inner join inserted i on e.oID = i.oID

		return
	end

	-- set the updated status appropriately
	if not update(date_changed)
	begin
		update	HPFEvents
		set		date_changed = getdate()
		FROM	HPFEvents e
		inner join inserted i on e.oID = i.oID

		return
	end

	-- set the updated status appropriately
	if not update(changed_by)
	begin
		update	HPFEvents
		set		changed_by = suser_sname()
		FROM	HPFEvents e
		inner join inserted i on e.oID = i.oID

		return
	end
end
GO
DENY DELETE ON dbo.HPFEvents TO www_role  AS dbo 
GO
DENY INSERT ON dbo.HPFEvents TO www_role  AS dbo 
GO
DENY SELECT ON dbo.HPFEvents TO www_role  AS dbo 
GO
DENY UPDATE ON dbo.HPFEvents TO www_role  AS dbo 
GO
COMMIT
GO

