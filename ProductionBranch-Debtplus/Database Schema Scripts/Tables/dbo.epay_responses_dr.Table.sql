USE [DebtPlus]
GO
/****** Object:  Table [dbo].[epay_responses_dr]    Script Date: 09/15/2014 13:13:26 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[epay_responses_dr](
	[epay_response_dr] [dbo].[typ_key] IDENTITY(1,1) NOT NULL,
	[epay_response_file] [dbo].[typ_key] NOT NULL,
	[transaction_id] [varchar](27) NOT NULL,
	[epay_transaction] [dbo].[typ_key] NULL,
	[error] [varchar](50) NULL,
	[response_date] [datetime] NULL,
	[agency_name] [varchar](35) NULL,
	[agency_id] [varchar](12) NOT NULL,
	[creditor_id] [varchar](12) NOT NULL,
	[creditor_name] [varchar](35) NULL,
	[client_number] [dbo].[typ_client] NOT NULL,
	[client_name] [varchar](35) NULL,
	[response_code] [varchar](1) NULL,
	[proposed_payment_amount] [money] NOT NULL,
	[accepted_payment_amount] [money] NOT NULL,
	[decline_reason_code] [varchar](2) NULL,
	[customer_biller_account_number_original] [varchar](32) NULL,
	[customer_biller_account_number_corrected] [varchar](32) NULL,
	[last_payment_date] [datetime] NULL,
	[current_client_balance] [money] NOT NULL,
	[date_of_balance] [datetime] NULL,
	[number_of_days_delinquent] [int] NOT NULL,
	[creditor_contact_name] [varchar](35) NULL,
	[date_of_authorization] [datetime] NULL,
	[corrected_balance_included] [varchar](1) NOT NULL,
	[date_created] [dbo].[typ_date] NOT NULL,
	[created_by] [dbo].[typ_counselor] NOT NULL,
 CONSTRAINT [PK_epay_response_dmp] PRIMARY KEY CLUSTERED 
(
	[epay_response_dr] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 90) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
