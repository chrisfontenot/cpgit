USE [DebtPlus]
GO
/****** Object:  Table [dbo].[workshops]    Script Date: 09/15/2014 13:13:26 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[workshops](
	[workshop] [dbo].[typ_key] IDENTITY(1,1) NOT NULL,
	[workshop_type] [dbo].[typ_key] NOT NULL,
	[start_time] [datetime] NOT NULL,
	[workshop_location] [dbo].[typ_key] NOT NULL,
	[seats_available] [int] NOT NULL,
	[CounselorID] [dbo].[typ_key] NULL,
	[HUD_Grant] [dbo].[typ_key] NULL,
	[HousingFeeAmount] [money] NOT NULL,
	[Guest] [dbo].[typ_key] NULL,
	[date_created] [dbo].[typ_date] NOT NULL,
	[created_by] [dbo].[typ_counselor] NOT NULL,
	[counselor] [dbo].[typ_counselor] NULL,
	[old_workshop] [int] NULL,
 CONSTRAINT [PK_workshops] PRIMARY KEY CLUSTERED 
(
	[workshop] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Workshop ID for the specific workshop. This is the primary key to the table.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'workshops', @level2type=N'COLUMN',@level2name=N'workshop'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Pointer to the current workshop type for the specific type of the workshop' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'workshops', @level2type=N'COLUMN',@level2name=N'workshop_type'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Starting date and time for the workshop' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'workshops', @level2type=N'COLUMN',@level2name=N'start_time'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Pointer to the location for the workshop. This is somewhat equivalnt to the "office" field for a counselor appointment.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'workshops', @level2type=N'COLUMN',@level2name=N'workshop_location'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The number of seats available for the workshop. This prevents over-booking the workshop.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'workshops', @level2type=N'COLUMN',@level2name=N'seats_available'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Date/Time when the row was created.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'workshops', @level2type=N'COLUMN',@level2name=N'date_created'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Person who created the row.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'workshops', @level2type=N'COLUMN',@level2name=N'created_by'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Counselor who is expected to conduct the workshop.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'workshops', @level2type=N'COLUMN',@level2name=N'counselor'
GO
ALTER TABLE [dbo].[workshops] ADD  CONSTRAINT [DF_workshops_seats_available]  DEFAULT ((20)) FOR [seats_available]
GO
