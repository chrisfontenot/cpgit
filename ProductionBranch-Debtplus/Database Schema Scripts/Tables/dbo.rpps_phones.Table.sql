USE [DebtPlus]
GO
/****** Object:  Table [dbo].[rpps_phones]    Script Date: 09/15/2014 13:13:26 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[rpps_phones](
	[rpps_phone] [dbo].[typ_key] NOT NULL,
	[Mastercard_key] [int] NULL,
	[rpps_contact] [dbo].[typ_key] NULL,
	[EffDate] [datetime] NOT NULL,
	[PhoneType] [varchar](50) NULL,
	[PhoneNum] [varchar](50) NULL,
 CONSTRAINT [PK_rpps_phones] PRIMARY KEY CLUSTERED 
(
	[rpps_phone] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
ALTER TABLE [dbo].[rpps_phones] ADD  CONSTRAINT [DF_rpps_phones_EffDate]  DEFAULT (getdate()) FOR [EffDate]
GO
