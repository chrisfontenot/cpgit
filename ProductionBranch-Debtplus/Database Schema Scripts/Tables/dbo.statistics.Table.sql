USE [DebtPlus]
GO
/****** Object:  Table [dbo].[statistics]    Script Date: 09/15/2014 13:13:26 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[statistics](
	[period_start] [smalldatetime] NOT NULL,
	[period_stop] [smalldatetime] NOT NULL,
	[clients_cre] [int] NULL,
	[clients_wks] [int] NULL,
	[clients_apt] [int] NULL,
	[clients_pnd] [int] NULL,
	[clients_rdy] [int] NULL,
	[clients_pro] [int] NULL,
	[clients_a] [int] NULL,
	[clients_ex] [int] NULL,
	[clients_i] [int] NULL,
	[clients_other] [int] NULL,
	[trust_active] [money] NULL,
	[trust_inactive] [money] NULL,
	[client_0] [money] NULL,
	[date_created] [dbo].[typ_date] NOT NULL,
	[created_by] [dbo].[typ_counselor] NOT NULL,
 CONSTRAINT [PK_statistics] PRIMARY KEY CLUSTERED 
(
	[period_start] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 90) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
