USE [DebtPlus]
GO
/****** Object:  Table [dbo].[rpps_response_details_cdp]    Script Date: 09/15/2014 13:13:26 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[rpps_response_details_cdp](
	[rpps_response_details_cdp] [dbo].[typ_key] IDENTITY(1,1) NOT NULL,
	[rpps_response_detail] [dbo].[typ_key] NOT NULL,
	[client] [dbo].[typ_client] NULL,
	[company_identification] [varchar](6) NULL,
	[ssn] [dbo].[typ_ssn] NULL,
	[agency_address] [varchar](15) NULL,
	[agency_zipcode] [varchar](5) NULL,
	[proposal_amount] [money] NULL,
	[start_date] [datetime] NULL,
	[creditor_count] [int] NULL,
	[agency_city] [varchar](20) NULL,
	[first_counsel_date] [datetime] NULL,
	[agency_name] [varchar](18) NULL,
	[agency_phone] [varchar](10) NULL,
	[account_balance] [money] NULL,
	[consumer_debt] [money] NULL,
	[consumer_income] [money] NULL,
	[monthly_debt_payments] [money] NULL,
	[living_expenses] [money] NULL,
	[home_phone] [varchar](10) NULL,
	[work_phone] [varchar](10) NULL,
	[agency_state] [varchar](2) NULL,
 CONSTRAINT [PK_rpps_response_details_cdp] PRIMARY KEY CLUSTERED 
(
	[rpps_response_details_cdp] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 90) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
