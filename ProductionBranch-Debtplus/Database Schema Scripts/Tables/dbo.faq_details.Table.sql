USE [DebtPlus]
GO
/****** Object:  Table [dbo].[faq_details]    Script Date: 09/15/2014 13:13:26 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[faq_details](
	[faq_detail] [dbo].[typ_key] IDENTITY(1,1) NOT NULL,
	[grouping] [dbo].[typ_key] NULL,
	[reason_for_call] [dbo].[typ_key] NULL,
	[referral] [dbo].[typ_key] NULL,
	[disposition] [dbo].[typ_key] NULL,
	[client] [dbo].[typ_client] NULL,
	[name] [varchar](256) NULL,
	[phone] [varchar](256) NULL,
	[note] [varchar](2048) NULL,
	[date_created] [dbo].[typ_date] NOT NULL,
	[created_by] [dbo].[typ_counselor] NOT NULL,
 CONSTRAINT [PK_faq_details] PRIMARY KEY CLUSTERED 
(
	[faq_detail] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 90) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
