USE [DebtPlus]
GO
/****** Object:  Table [dbo].[client_other_debts]    Script Date: 09/15/2014 13:13:26 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[client_other_debts](
	[client_other_debt] [dbo].[typ_key] IDENTITY(1,1) NOT NULL,
	[client] [dbo].[typ_client] NOT NULL,
	[creditor_name] [dbo].[typ_description] NULL,
	[account_number] [dbo].[typ_description] NULL,
	[balance] [money] NOT NULL,
	[payment] [money] NULL,
	[interest_rate] [dbo].[typ_fairshare_rate] NOT NULL,
	[created_by] [dbo].[typ_counselor] NOT NULL,
	[date_created] [dbo].[typ_date] NOT NULL,
	[old_client_other_debt] [int] NULL,
 CONSTRAINT [PK_client_other_debts] PRIMARY KEY CLUSTERED 
(
	[client_other_debt] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 90) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Primary key to the table' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'client_other_debts', @level2type=N'COLUMN',@level2name=N'client_other_debt'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Client associated with the item' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'client_other_debts', @level2type=N'COLUMN',@level2name=N'client'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Name of the creditor with whom the debt exists. It is not managed by the system.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'client_other_debts', @level2type=N'COLUMN',@level2name=N'creditor_name'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Account number with the creditor.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'client_other_debts', @level2type=N'COLUMN',@level2name=N'account_number'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Balance of the debt at the time that the record was created' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'client_other_debts', @level2type=N'COLUMN',@level2name=N'balance'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Normal monthly payment' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'client_other_debts', @level2type=N'COLUMN',@level2name=N'payment'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Interest rate for the debt.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'client_other_debts', @level2type=N'COLUMN',@level2name=N'interest_rate'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Name of the person who created the row' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'client_other_debts', @level2type=N'COLUMN',@level2name=N'created_by'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Date/Time when the row was created' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'client_other_debts', @level2type=N'COLUMN',@level2name=N'date_created'
GO
