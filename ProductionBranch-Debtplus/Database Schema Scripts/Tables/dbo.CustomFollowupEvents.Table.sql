USE [DebtPlus]
GO
/****** Object:  Table [dbo].[CustomFollowupEvents]    Script Date: 09/15/2014 13:13:26 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[CustomFollowupEvents](
	[client] [dbo].[typ_client] NOT NULL,
	[client_appointment] [dbo].[typ_key] NOT NULL,
	[date_printed] [datetime] NULL,
	[letter_date] [datetime] NOT NULL,
	[date_created] [dbo].[typ_date] NOT NULL,
	[created_by] [dbo].[typ_counselor] NOT NULL,
 CONSTRAINT [PK_CustomFollowupEvents] PRIMARY KEY CLUSTERED 
(
	[client_appointment] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 90) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
