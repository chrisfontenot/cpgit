/* To prevent any potential data loss issues, you should review this script in detail before running it outside the context of the database designer.*/
BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
GO
BEGIN TRANSACTION
GO
execute UPDATE_ADD_COLUMN 'BankruptcyClassTypes', 'hpf'
GO
CREATE TABLE dbo.Tmp_BankruptcyClassTypes
	(
	oID dbo.typ_key NOT NULL IDENTITY (1, 1),
	description dbo.typ_description NOT NULL,
	nfcc varchar(256) NULL,
	rpps varchar(256) NULL,
	epay varchar(256) NULL,
	note varchar(256) NULL,
	hud_9902_section varchar(256) NULL,
	[default] bit NOT NULL,
	ActiveFlag bit NOT NULL,
	date_created dbo.typ_date NOT NULL,
	created_by dbo.typ_counselor NOT NULL,
	ts timestamp NOT NULL,
	hpf varchar(50) NULL
	)  ON [PRIMARY]
GO
GRANT DELETE ON dbo.Tmp_BankruptcyClassTypes TO public  AS dbo
GO
GRANT INSERT ON dbo.Tmp_BankruptcyClassTypes TO public  AS dbo
GO
GRANT SELECT ON dbo.Tmp_BankruptcyClassTypes TO public  AS dbo
GO
GRANT UPDATE ON dbo.Tmp_BankruptcyClassTypes TO public  AS dbo
GO
EXECUTE sp_bindefault N'dbo.default_false', N'dbo.Tmp_BankruptcyClassTypes.[default]'
GO
EXECUTE sp_bindefault N'dbo.default_true', N'dbo.Tmp_BankruptcyClassTypes.ActiveFlag'
GO
SET IDENTITY_INSERT dbo.Tmp_BankruptcyClassTypes ON
GO
IF EXISTS(SELECT * FROM dbo.BankruptcyClassTypes)
	 EXEC('INSERT INTO dbo.Tmp_BankruptcyClassTypes (oID, description, nfcc, rpps, epay, note, hud_9902_section, [default], ActiveFlag, date_created, created_by, hpf)
		SELECT oID, description, nfcc, rpps, epay, note, hud_9902_section, [default], ActiveFlag, date_created, created_by, hpf FROM dbo.BankruptcyClassTypes WITH (HOLDLOCK TABLOCKX)')
GO
SET IDENTITY_INSERT dbo.Tmp_BankruptcyClassTypes OFF
GO
DROP TABLE dbo.BankruptcyClassTypes
GO
EXECUTE sp_rename N'dbo.Tmp_BankruptcyClassTypes', N'BankruptcyClassTypes', 'OBJECT' 
GO
ALTER TABLE dbo.BankruptcyClassTypes ADD CONSTRAINT
	PK_BankruptcyClassTypes PRIMARY KEY CLUSTERED 
	(
	oID
	) WITH( STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
DENY DELETE ON dbo.BankruptcyClassTypes TO www_role  AS dbo 
GO
DENY INSERT ON dbo.BankruptcyClassTypes TO www_role  AS dbo 
GO
DENY SELECT ON dbo.BankruptcyClassTypes TO www_role  AS dbo 
GO
DENY UPDATE ON dbo.BankruptcyClassTypes TO www_role  AS dbo 
GO
COMMIT
GO
