ALTER TABLE dbo.client_disclosures_AFS DROP CONSTRAINT FK_client_disclosures_AFS_client_disclosures
GO
/* To prevent any potential data loss issues, you should review this script in detail before running it outside the context of the database designer.*/
BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION
GO
CREATE TABLE dbo.Tmp_client_disclosures_AFS
	(
	oID dbo.typ_key NOT NULL IDENTITY (1, 1),
	client_disclosure_ID dbo.typ_key NOT NULL,
	maiden varchar(50) NULL,
	ts timestamp NOT NULL
	)  ON [PRIMARY]
GO
SET IDENTITY_INSERT dbo.Tmp_client_disclosures_AFS ON
GO
IF EXISTS(SELECT * FROM SYSOBJECTS WHERE TYPE='U' AND NAME=N'client_disclosures_AFS')
BEGIN
	EXEC('INSERT INTO dbo.Tmp_client_disclosures_AFS (oID, client_disclosure_ID, maiden) SELECT oID, client_disclosure_ID, maiden FROM dbo.client_disclosures_AFS WITH (HOLDLOCK TABLOCKX)')
	EXEC('DROP TABLE dbo.client_disclosures_AFS')
END
GO
SET IDENTITY_INSERT dbo.Tmp_client_disclosures_AFS OFF
GO
EXECUTE sp_rename N'dbo.Tmp_client_disclosures_AFS', N'client_disclosures_AFS', 'OBJECT' 
GO
ALTER TABLE dbo.client_disclosures_AFS ADD CONSTRAINT
	PK_client_disclosures_AFS PRIMARY KEY CLUSTERED 
	(
	oID
	) ON [PRIMARY]
GO
COMMIT
GO
ALTER TABLE dbo.client_disclosures_AFS ADD CONSTRAINT
	FK_client_disclosures_AFS_client_disclosures FOREIGN KEY
	(
	client_disclosure_ID
	) REFERENCES dbo.client_disclosures
	(
	oID
	) ON UPDATE  CASCADE 
	 ON DELETE  CASCADE 
GO
DECLARE @v sql_variant 
SET @v = N'One-to-One relationship'
EXECUTE sp_addextendedproperty N'MS_Description', @v, N'SCHEMA', N'dbo', N'TABLE', N'client_disclosures_AFS', N'CONSTRAINT', N'FK_client_disclosures_AFS_client_disclosures'
GO
