-- Correct the data in the tables before continuing with the table update
DELETE FROM ASSETS WHERE client NOT IN (SELECT client FROM clients)
DELETE FROM ASSETS WHERE asset_id NOT IN (SELECT asset_id FROM asset_ids)
UPDATE ASSETS SET frequency = 4 where frequency not in (select oid from payfrequencytypes)
GO
/* To prevent any potential data loss issues, you should review this script in detail before running it outside the context of the database designer.*/
BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION
GO
EXECUTE UPDATE_DROP_CONSTRAINTS 'assets'
GO
CREATE TABLE dbo.Tmp_assets
	(
	asset dbo.typ_key NOT NULL IDENTITY (1, 1),
	client dbo.typ_client NULL,
	asset_id dbo.typ_key NOT NULL,
	asset_amount  AS ([dbo].[MonthlyAmount]([amount],[frequency])),
	amount money NOT NULL,
	frequency dbo.typ_key NOT NULL,
	date_created dbo.typ_date NOT NULL,
	created_by dbo.typ_counselor NOT NULL,
	ts timestamp NOT NULL
	)  ON [PRIMARY]
GO
GRANT DELETE ON dbo.Tmp_assets TO public  AS dbo
GO
GRANT INSERT ON dbo.Tmp_assets TO public  AS dbo
GO
GRANT SELECT ON dbo.Tmp_assets TO public  AS dbo
GO
GRANT UPDATE ON dbo.Tmp_assets TO public  AS dbo
GO
DECLARE @v sql_variant 
SET @v = N'Primary key to the table.'
EXECUTE sp_addextendedproperty N'MS_Description', @v, N'SCHEMA', N'dbo', N'TABLE', N'Tmp_assets', N'COLUMN', N'asset'
GO
DECLARE @v sql_variant 
SET @v = N'Client ID who owns the asset'
EXECUTE sp_addextendedproperty N'MS_Description', @v, N'SCHEMA', N'dbo', N'TABLE', N'Tmp_assets', N'COLUMN', N'client'
GO
DECLARE @v sql_variant 
SET @v = N'ID from the asset_ids table.'
EXECUTE sp_addextendedproperty N'MS_Description', @v, N'SCHEMA', N'dbo', N'TABLE', N'Tmp_assets', N'COLUMN', N'asset_id'
GO
DECLARE @v sql_variant 
SET @v = N'Dollar amount of the asset value'
EXECUTE sp_addextendedproperty N'MS_Description', @v, N'SCHEMA', N'dbo', N'TABLE', N'Tmp_assets', N'COLUMN', N'asset_amount'
GO
DECLARE @v sql_variant 
SET @v = N'Date / Time when the row was created.'
EXECUTE sp_addextendedproperty N'MS_Description', @v, N'SCHEMA', N'dbo', N'TABLE', N'Tmp_assets', N'COLUMN', N'date_created'
GO
DECLARE @v sql_variant 
SET @v = N'Person who created the row.'
EXECUTE sp_addextendedproperty N'MS_Description', @v, N'SCHEMA', N'dbo', N'TABLE', N'Tmp_assets', N'COLUMN', N'created_by'
GO
EXECUTE sp_bindefault N'dbo.default_money', N'dbo.Tmp_assets.amount'
GO
ALTER TABLE dbo.Tmp_assets ADD CONSTRAINT DF_assets_frequency DEFAULT ([dbo].[Default_PayFrequency]()) FOR frequency
GO
SET IDENTITY_INSERT dbo.Tmp_assets ON
GO
IF EXISTS(SELECT * FROM dbo.assets)
	 EXEC('INSERT INTO dbo.Tmp_assets (asset, client, asset_id, amount, frequency, date_created, created_by)
		SELECT asset, client, asset_id, amount, frequency, date_created, created_by FROM dbo.assets WITH (HOLDLOCK TABLOCKX)')
GO
SET IDENTITY_INSERT dbo.Tmp_assets OFF
GO
DROP TABLE dbo.assets
GO
EXECUTE sp_rename N'dbo.Tmp_assets', N'assets', 'OBJECT' 
GO
ALTER TABLE dbo.assets ADD CONSTRAINT
	PK_assets PRIMARY KEY CLUSTERED 
	(
	asset
	) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX IX_assets_1 ON dbo.assets
	(
	client
	) ON [PRIMARY]
GO
ALTER TABLE dbo.assets ADD CONSTRAINT
	FK_assets_PayFrequencyTypes FOREIGN KEY
	(
	frequency
	) REFERENCES dbo.PayFrequencyTypes
	(
	oID
	) ON UPDATE  NO ACTION 
	 ON DELETE  NO ACTION 
GO
ALTER TABLE dbo.assets ADD CONSTRAINT
	FK_assets_asset_ids FOREIGN KEY
	(
	asset_id
	) REFERENCES dbo.asset_ids
	(
	asset_id
	) ON UPDATE  CASCADE 
	 ON DELETE  CASCADE 
GO
ALTER TABLE dbo.assets ADD CONSTRAINT
	FK_assets_clients FOREIGN KEY
	(
	client
	) REFERENCES dbo.clients
	(
	client
	) ON UPDATE  CASCADE 
	 ON DELETE  CASCADE 
GO
DENY DELETE ON dbo.assets TO www_role  AS dbo 
GO
DENY INSERT ON dbo.assets TO www_role  AS dbo 
GO
DENY SELECT ON dbo.assets TO www_role  AS dbo 
GO
DENY UPDATE ON dbo.assets TO www_role  AS dbo 
GO
COMMIT
GO
