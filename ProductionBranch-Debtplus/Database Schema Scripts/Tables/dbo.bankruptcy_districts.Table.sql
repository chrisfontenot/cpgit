USE [DebtPlus]
GO
/****** Object:  Table [dbo].[bankruptcy_districts]    Script Date: 09/15/2014 13:13:25 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[bankruptcy_districts](
	[bankruptcy_district] [dbo].[typ_key] IDENTITY(1,1) NOT NULL,
	[Description] [dbo].[typ_description] NOT NULL,
	[Default] [bit] NOT NULL,
	[ActiveFlag] [bit] NOT NULL,
	[date_created] [dbo].[typ_date] NOT NULL,
	[created_by] [dbo].[typ_counselor] NOT NULL,
 CONSTRAINT [PK_bankruptcy_districts] PRIMARY KEY CLUSTERED 
(
	[bankruptcy_district] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 90) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Bankruptcy district ID' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'bankruptcy_districts', @level2type=N'COLUMN',@level2name=N'bankruptcy_district'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Description for the reports, etc.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'bankruptcy_districts', @level2type=N'COLUMN',@level2name=N'Description'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Default item in a list of items' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'bankruptcy_districts', @level2type=N'COLUMN',@level2name=N'Default'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'If false, item may not be selected' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'bankruptcy_districts', @level2type=N'COLUMN',@level2name=N'ActiveFlag'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Date/Time when the row was created' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'bankruptcy_districts', @level2type=N'COLUMN',@level2name=N'date_created'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Name of the counselor who created the row' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'bankruptcy_districts', @level2type=N'COLUMN',@level2name=N'created_by'
GO
