EXEC('ALTER TABLE dbo.OCS_ContactAttempt DROP CONSTRAINT FK_OCS_ContactAttempt_OCS_ContactType')
EXEC('ALTER TABLE dbo.OCS_ContactAttempt DROP CONSTRAINT FK_OCS_ContactAttempt_OCS_ResultCode')
EXEC('ALTER TABLE dbo.OCS_ContactAttempt DROP CONSTRAINT FK_OCS_ContactAttempt_clients')
EXEC('ALTER TABLE dbo.OCS_CallOutcome DROP CONSTRAINT FK_OCS_CallOutcome_OCS_ContactAttempt')
GO
/* To prevent any potential data loss issues, you should review this script in detail before running it outside the context of the database designer.*/
BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
GO
BEGIN TRANSACTION
GO
CREATE TABLE dbo.Tmp_OCS_ContactAttempt
	(
	[Id] dbo.typ_key NOT NULL IDENTITY (1, 1),
	[ClientId] dbo.typ_client NOT NULL,
	[User] varchar(80) NULL,
	[ContactType] dbo.typ_key NULL,
	[Begin] datetime NULL,
	[End] datetime NULL,
	[ResultCode] dbo.typ_key NULL,
	[SpecificReason] varchar(100) NULL,
	[StatusCode] dbo.typ_key NULL,
	[ContactAttempts] int NULL,
	[ActiveFlag] bit NULL,
	[CounselorId] dbo.typ_key NULL,
	[OcsId] int NULL,
	[Timestamp] timestamp NOT NULL
	)  ON [PRIMARY]
GO
GRANT DELETE ON dbo.Tmp_OCS_ContactAttempt TO public  AS dbo
GO
GRANT INSERT ON dbo.Tmp_OCS_ContactAttempt TO public  AS dbo
GO
GRANT SELECT ON dbo.Tmp_OCS_ContactAttempt TO public  AS dbo
GO
GRANT UPDATE ON dbo.Tmp_OCS_ContactAttempt TO public  AS dbo
GO
SET IDENTITY_INSERT dbo.Tmp_OCS_ContactAttempt ON
GO
IF EXISTS(SELECT * FROM sysobjects where name=N'OCS_ContactAttempt' and type='U')
BEGIN
	EXEC('INSERT INTO dbo.Tmp_OCS_ContactAttempt (Id, ClientId, [User], ContactType, [Begin], [End], ResultCode, SpecificReason, StatusCode, ContactAttempts, ActiveFlag, CounselorId, OcsId)
		SELECT Id, ClientId, [User], ContactType, [Begin], [End], ResultCode, SpecificReason, StatusCode, ContactAttempts, ActiveFlag, CounselorId, OcsId FROM dbo.OCS_ContactAttempt WITH (HOLDLOCK TABLOCKX)')
	EXEC('DROP TABLE dbo.OCS_ContactAttempt')
END
GO
SET IDENTITY_INSERT dbo.Tmp_OCS_ContactAttempt OFF
GO
EXECUTE sp_rename N'dbo.Tmp_OCS_ContactAttempt', N'OCS_ContactAttempt', 'OBJECT' 
GO
ALTER TABLE dbo.OCS_ContactAttempt ADD CONSTRAINT
	PK_OCS_ContactAttempt PRIMARY KEY CLUSTERED 
	(
	Id
	) WITH( STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX IX_OCS_ContactAttempt_1 ON dbo.OCS_ContactAttempt
	(
	ClientId
	) WITH( STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
DENY DELETE ON dbo.OCS_ContactAttempt TO www_role  AS dbo 
GO
DENY INSERT ON dbo.OCS_ContactAttempt TO www_role  AS dbo 
GO
DENY SELECT ON dbo.OCS_ContactAttempt TO www_role  AS dbo 
GO
DENY UPDATE ON dbo.OCS_ContactAttempt TO www_role  AS dbo 
GO
COMMIT
BEGIN TRANSACTION
GO
EXEC('ALTER TABLE dbo.OCS_ContactAttempt ADD CONSTRAINT FK_OCS_ContactAttempt_clients FOREIGN KEY ( ClientId ) REFERENCES dbo.clients ( client ) ON UPDATE NO ACTION ON DELETE CASCADE')
EXEC('ALTER TABLE dbo.OCS_ContactAttempt ADD CONSTRAINT FK_OCS_ContactAttempt_OCS_ResultCode FOREIGN KEY ( ResultCode ) REFERENCES dbo.OCS_ResultCode ( Id ) ON UPDATE NO ACTION ON DELETE SET NULL')
EXEC('ALTER TABLE dbo.OCS_ContactAttempt ADD CONSTRAINT FK_OCS_ContactAttempt_OCS_ContactType FOREIGN KEY ( ContactType ) REFERENCES dbo.OCS_ContactType ( Id ) ON UPDATE NO ACTION ON DELETE SET NULL')
EXEC('ALTER TABLE dbo.OCS_CallOutcome ADD CONSTRAINT FK_OCS_CallOutcome_OCS_ContactAttempt FOREIGN KEY ( ContactAttempt ) REFERENCES dbo.OCS_ContactAttempt ( Id ) ON UPDATE NO ACTION ON DELETE CASCADE')
GO
COMMIT
GO
