USE [DebtPlus]
GO
/****** Object:  Table [dbo].[workshop_locations]    Script Date: 09/15/2014 13:13:26 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[workshop_locations](
	[workshop_location] [dbo].[typ_key] IDENTITY(1,1) NOT NULL,
	[name] [varchar](80) NOT NULL,
	[organization] [varchar](80) NULL,
	[type] [dbo].[typ_key] NULL,
	[AddressID] [dbo].[typ_key] NULL,
	[TelephoneID] [dbo].[typ_key] NULL,
	[directions] [varchar](1024) NULL,
	[milage] [float] NULL,
	[ActiveFlag] [bit] NOT NULL,
	[hcs_id] [dbo].[typ_key] NULL,
	[date_created] [dbo].[typ_date] NOT NULL,
	[created_by] [dbo].[typ_counselor] NOT NULL,
	[old_workshop_location] [int] NULL,
 CONSTRAINT [PK_workshop_locations] PRIMARY KEY CLUSTERED 
(
	[workshop_location] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 90) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
