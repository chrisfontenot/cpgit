USE [DebtPlus]
GO
/****** Object:  Table [dbo].[epay_response_files]    Script Date: 09/15/2014 13:13:26 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[epay_response_files](
	[epay_response_file] [int] IDENTITY(1,1) NOT NULL,
	[label] [dbo].[typ_description] NULL,
	[created_by] [dbo].[typ_counselor] NOT NULL,
	[date_created] [dbo].[typ_date] NOT NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
