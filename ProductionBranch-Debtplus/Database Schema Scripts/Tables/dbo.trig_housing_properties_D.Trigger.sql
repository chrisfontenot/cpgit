SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER TRIGGER [trig_housing_properties_D] on [Housing_properties] AFTER DELETE as
begin

	set nocount on

	delete	housing_borrowers
	from	housing_borrowers b
	inner join deleted d on d.CoOwnerID = b.oID
	where	d.CoOwnerID is not null;
	
	delete	housing_borrowers
	from	housing_borrowers b
	inner join deleted d on d.OwnerID = b.oID
	where	d.OwnerID is not null;
	
	delete	housing_loans
	from	housing_loans l
	inner join deleted d on d.oID = l.PropertyID;
	
	delete	addresses
	from	addresses a
	inner join deleted d on d.PropertyAddress = a.address
	where	d.PropertyAddress is not null;

	delete	addresses
	from	addresses a
	inner join deleted d on d.PreviousAddress = a.address
	where	d.PreviousAddress is not null;
end
GO
