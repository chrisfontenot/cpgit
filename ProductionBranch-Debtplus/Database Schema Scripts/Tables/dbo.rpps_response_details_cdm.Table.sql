USE [DebtPlus]
GO
/****** Object:  Table [dbo].[rpps_response_details_cdm]    Script Date: 09/15/2014 13:13:26 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[rpps_response_details_cdm](
	[rpps_response_detail_cdm] [int] IDENTITY(1,1) NOT NULL,
	[rpps_response_file] [int] NULL,
	[trace_number] [varchar](15) NULL,
	[rpps_transaction] [dbo].[typ_key] NULL,
	[biller_id] [dbo].[typ_rpps_biller_id] NULL,
	[creditor] [dbo].[typ_creditor] NULL,
	[client_creditor] [dbo].[typ_key] NULL,
	[account_number] [dbo].[typ_client_account] NULL,
	[net] [money] NULL,
	[line_number] [int] NULL,
	[message] [varchar](96) NULL,
	[created_by] [dbo].[typ_counselor] NOT NULL,
	[date_created] [dbo].[typ_date] NOT NULL,
 CONSTRAINT [PK_rpps_response_details_cdm] PRIMARY KEY CLUSTERED 
(
	[rpps_response_detail_cdm] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 90) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
