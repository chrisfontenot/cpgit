USE [DebtPlus]
GO
-- Discard the obsolete table
IF EXISTS(SELECT * FROM SYSOBJECTS WHERE TYPE='U' AND NAME=N'client_payout_detail')
	EXEC ('DROP TABLE [client_payout_detail]')
GO
