/* To prevent any potential data loss issues, you should review this script in detail before running it outside the context of the database designer.*/
BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION
GO
EXECUTE UPDATE_ADD_COLUMN 'Housing_VisitOutcomeTypes', 'hpf'
GO
CREATE TABLE dbo.Tmp_Housing_VisitOutcomeTypes
	(
	oID dbo.typ_key NOT NULL IDENTITY (1, 1),
	description dbo.typ_description NOT NULL,
	hpf varchar(50) NULL,
	ActiveFlag bit NOT NULL,
	ts timestamp NOT NULL
	)  ON [PRIMARY]
GO
GRANT DELETE ON dbo.Tmp_Housing_VisitOutcomeTypes TO public  AS dbo
GO
GRANT INSERT ON dbo.Tmp_Housing_VisitOutcomeTypes TO public  AS dbo
GO
GRANT SELECT ON dbo.Tmp_Housing_VisitOutcomeTypes TO public  AS dbo
GO
GRANT UPDATE ON dbo.Tmp_Housing_VisitOutcomeTypes TO public  AS dbo
GO
EXECUTE sp_bindefault N'dbo.default_true', N'dbo.Tmp_Housing_VisitOutcomeTypes.ActiveFlag'
GO
SET IDENTITY_INSERT dbo.Tmp_Housing_VisitOutcomeTypes ON
GO
IF EXISTS(SELECT * FROM dbo.Housing_VisitOutcomeTypes)
	 EXEC('INSERT INTO dbo.Tmp_Housing_VisitOutcomeTypes (oID, description, ActiveFlag, hpf)
		SELECT oID, description, ActiveFlag, hpf FROM dbo.Housing_VisitOutcomeTypes WITH (HOLDLOCK TABLOCKX)')
GO
SET IDENTITY_INSERT dbo.Tmp_Housing_VisitOutcomeTypes OFF
GO
DROP TABLE dbo.Housing_VisitOutcomeTypes
GO
EXECUTE sp_rename N'dbo.Tmp_Housing_VisitOutcomeTypes', N'Housing_VisitOutcomeTypes', 'OBJECT' 
GO
ALTER TABLE dbo.Housing_VisitOutcomeTypes ADD CONSTRAINT
	PK_housing_VisitOutcomeTypes PRIMARY KEY CLUSTERED 
	(
	oID
	) WITH( STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]

GO
DENY DELETE ON dbo.Housing_VisitOutcomeTypes TO www_role  AS dbo 
GO
DENY INSERT ON dbo.Housing_VisitOutcomeTypes TO www_role  AS dbo 
GO
DENY SELECT ON dbo.Housing_VisitOutcomeTypes TO www_role  AS dbo 
GO
DENY UPDATE ON dbo.Housing_VisitOutcomeTypes TO www_role  AS dbo 
GO
COMMIT
