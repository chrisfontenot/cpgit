USE [DebtPlus]
GO
/****** Object:  Table [dbo].[creditor_methods]    Script Date: 09/15/2014 13:13:26 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[creditor_methods](
	[creditor_method] [dbo].[typ_key] IDENTITY(1,1) NOT NULL,
	[timestamp] [timestamp] NOT NULL,
	[creditor] [dbo].[typ_key] NOT NULL,
	[type] [varchar](3) NOT NULL,
	[region] [dbo].[typ_key] NOT NULL,
	[bank] [dbo].[typ_key] NOT NULL,
	[validated] [bit] NOT NULL,
	[rpps_biller_id] [dbo].[typ_rpps_biller_id] NULL,
	[epay_biller_id] [dbo].[typ_epay_biller] NULL,
	[created_by] [dbo].[typ_counselor] NOT NULL,
	[date_created] [dbo].[typ_date] NOT NULL,
	[old_creditor_method] [int] NULL,
 CONSTRAINT [PK_creditor_methods] PRIMARY KEY CLUSTERED 
(
	[creditor_method] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Primary key to the table.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'creditor_methods', @level2type=N'COLUMN',@level2name=N'creditor_method'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'System assigned timestamp when the row was last updated' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'creditor_methods', @level2type=N'COLUMN',@level2name=N'timestamp'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Type of record. EFT = payment, EDI = proposal' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'creditor_methods', @level2type=N'COLUMN',@level2name=N'type'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'If regions specific, this is the pointer to the region information' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'creditor_methods', @level2type=N'COLUMN',@level2name=N'region'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Pointer to the bank for originating the transactions' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'creditor_methods', @level2type=N'COLUMN',@level2name=N'bank'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Set to zero when the epay, rpps, or vanco items are changed in the tables. Set to 1 when the acocunts are validated. when 1, accounts no longer need to be validated.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'creditor_methods', @level2type=N'COLUMN',@level2name=N'validated'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'If RPPS, this is the biller ID' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'creditor_methods', @level2type=N'COLUMN',@level2name=N'rpps_biller_id'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'If ePay, this is the biller ID' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'creditor_methods', @level2type=N'COLUMN',@level2name=N'epay_biller_id'
GO
ALTER TABLE [dbo].[creditor_methods] ADD  CONSTRAINT [DF_creditor_payment_methods_type]  DEFAULT ('EFT') FOR [type]
GO
