EXEC('ALTER TABLE dbo.client_statement_details DROP CONSTRAINT FK_client_statement_details_client_statement_batches')
GO
/* To prevent any potential data loss issues, you should review this script in detail before running it outside the context of the database designer.*/
BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION
GO
CREATE TABLE dbo.Tmp_client_statement_details
	(
	client_statement_detail dbo.typ_key NOT NULL IDENTITY (1, 1),
	client_statement_batch dbo.typ_key NOT NULL,
	client dbo.typ_client NOT NULL,
	client_creditor dbo.typ_key NOT NULL,
	current_balance money NOT NULL,
	debits money NOT NULL,
	credits money NOT NULL,
	interest money NOT NULL,
	payments_to_date money NOT NULL,
	original_balance money NOT NULL,
	interest_rate float(53) NOT NULL,
	last_payment dbo.typ_key NOT NULL,
	sched_payment money NOT NULL,
	ts timestamp NOT NULL
	)  ON [PRIMARY]
GO
GRANT DELETE ON dbo.Tmp_client_statement_details TO public  AS dbo
GO
GRANT INSERT ON dbo.Tmp_client_statement_details TO public  AS dbo
GO
GRANT SELECT ON dbo.Tmp_client_statement_details TO public  AS dbo
GO
GRANT SELECT ON dbo.Tmp_client_statement_details TO www_role  AS dbo
GO
GRANT UPDATE ON dbo.Tmp_client_statement_details TO public  AS dbo
GO
EXECUTE sp_bindefault N'dbo.default_money', N'dbo.Tmp_client_statement_details.current_balance'
GO
EXECUTE sp_bindefault N'dbo.default_money', N'dbo.Tmp_client_statement_details.debits'
GO
EXECUTE sp_bindefault N'dbo.default_money', N'dbo.Tmp_client_statement_details.credits'
GO
EXECUTE sp_bindefault N'dbo.default_money', N'dbo.Tmp_client_statement_details.interest'
GO
EXECUTE sp_bindefault N'dbo.default_money', N'dbo.Tmp_client_statement_details.payments_to_date'
GO
EXECUTE sp_bindefault N'dbo.default_money', N'dbo.Tmp_client_statement_details.original_balance'
GO
EXECUTE sp_bindefault N'dbo.default_zero', N'dbo.Tmp_client_statement_details.interest_rate'
GO
EXECUTE sp_bindefault N'dbo.default_zero', N'dbo.Tmp_client_statement_details.last_payment'
GO
EXECUTE sp_bindefault N'dbo.default_money', N'dbo.Tmp_client_statement_details.sched_payment'
GO
SET IDENTITY_INSERT dbo.Tmp_client_statement_details ON
GO
IF EXISTS(SELECT * FROM dbo.client_statement_details)
	 EXEC('INSERT INTO dbo.Tmp_client_statement_details (client_statement_detail, client_statement_batch, client, client_creditor, current_balance, debits, credits, interest, payments_to_date, original_balance, interest_rate, last_payment, sched_payment)
		SELECT client_statement_detail, client_statement_batch, client, client_creditor, current_balance, debits, credits, interest, payments_to_date, original_balance, interest_rate, last_payment, sched_payment FROM dbo.client_statement_details WITH (HOLDLOCK TABLOCKX)')
GO
SET IDENTITY_INSERT dbo.Tmp_client_statement_details OFF
GO
DROP TABLE dbo.client_statement_details
GO
EXECUTE sp_rename N'dbo.Tmp_client_statement_details', N'client_statement_details', 'OBJECT' 
GO
ALTER TABLE dbo.client_statement_details ADD CONSTRAINT
	PK_client_statement_detail PRIMARY KEY NONCLUSTERED 
	(
	client_statement_detail
	) WITH( STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
CREATE CLUSTERED INDEX IX_client_statement_detail_1 ON dbo.client_statement_details
	(
	client_statement_batch,
	client
	) WITH( STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX IX_client_statement_detail_2 ON dbo.client_statement_details
	(
	client_creditor
	) WITH( STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
DENY DELETE ON dbo.client_statement_details TO www_role  AS dbo 
GO
DENY INSERT ON dbo.client_statement_details TO www_role  AS dbo 
GO
DENY UPDATE ON dbo.client_statement_details TO www_role  AS dbo 
GO
COMMIT
GO
EXEC ('ALTER TABLE dbo.client_statement_details ADD CONSTRAINT FK_client_statement_details_client_statement_batches FOREIGN KEY ( client_statement_batch ) REFERENCES dbo.client_statement_batches ( client_statement_batch ) ON UPDATE NO ACTION ON DELETE CASCADE')
GO
