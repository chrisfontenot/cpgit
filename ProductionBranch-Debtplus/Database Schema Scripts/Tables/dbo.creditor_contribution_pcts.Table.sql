USE [DebtPlus]
GO
/****** Object:  Table [dbo].[creditor_contribution_pcts]    Script Date: 09/15/2014 13:13:26 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[creditor_contribution_pcts](
	[creditor_contribution_pct] [dbo].[typ_key] IDENTITY(1,1) NOT NULL,
	[creditor] [dbo].[typ_creditor] NOT NULL,
	[effective_date] [datetime] NOT NULL,
	[fairshare_pct_check] [float] NOT NULL,
	[fairshare_pct_eft] [float] NULL,
	[creditor_type_eft] [dbo].[typ_creditor_type] NOT NULL,
	[creditor_type_check] [dbo].[typ_creditor_type] NOT NULL,
	[date_updated] [datetime] NULL,
	[created_by] [dbo].[typ_counselor] NOT NULL,
	[date_created] [dbo].[typ_date] NOT NULL,
	[old_creditor_contribution_pct] [int] NULL,
 CONSTRAINT [PK_creditor_contribution_pcts] PRIMARY KEY CLUSTERED 
(
	[creditor_contribution_pct] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 90) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Primary key' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'creditor_contribution_pcts', @level2type=N'COLUMN',@level2name=N'creditor_contribution_pct'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Creditor ID' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'creditor_contribution_pcts', @level2type=N'COLUMN',@level2name=N'creditor'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Effective date when the row is to be changed' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'creditor_contribution_pcts', @level2type=N'COLUMN',@level2name=N'effective_date'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Percentage for fairshare if paid by check' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'creditor_contribution_pcts', @level2type=N'COLUMN',@level2name=N'fairshare_pct_check'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Payment for fairshare if paid by EFT' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'creditor_contribution_pcts', @level2type=N'COLUMN',@level2name=N'fairshare_pct_eft'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Creditor type for EFT transfers (''B'' = Bill, ''D" = Deduct, "N" = no contribution)' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'creditor_contribution_pcts', @level2type=N'COLUMN',@level2name=N'creditor_type_eft'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Creditor type for Checks (''B'' = Bill, ''D" = Deduct, "N" = no contribution)' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'creditor_contribution_pcts', @level2type=N'COLUMN',@level2name=N'creditor_type_check'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Date when the row been moved to the creditor record' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'creditor_contribution_pcts', @level2type=N'COLUMN',@level2name=N'date_updated'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Person who created the row.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'creditor_contribution_pcts', @level2type=N'COLUMN',@level2name=N'created_by'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Date/Time when the row was created.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'creditor_contribution_pcts', @level2type=N'COLUMN',@level2name=N'date_created'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'This table includes the contribution percentages for a creditor. As each effective date is reached, the information is updated in the creditor table.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'creditor_contribution_pcts'
GO
