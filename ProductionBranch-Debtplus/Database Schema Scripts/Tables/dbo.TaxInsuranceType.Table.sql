use debtplus
GO

/* To prevent any potential data loss issues, you should review this script in detail before running it outside the context of the database designer.*/
BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION
GO
CREATE TABLE dbo.Tmp_TaxInsuranceType
	(
	oID dbo.typ_key NOT NULL IDENTITY (1, 1),
	Name dbo.typ_description NOT NULL,
	SortOrder int NULL,
	[Default] bit NOT NULL,
	ActiveFlag bit NOT NULL,
	date_created dbo.typ_date NOT NULL,
	created_by dbo.typ_counselor NOT NULL,
	ts rowversion NOT NULL
	)  ON [PRIMARY]
GO
EXECUTE sp_bindefault N'dbo.default_false', N'dbo.Tmp_TaxInsuranceType.[Default]'
GO
EXECUTE sp_bindefault N'dbo.default_true', N'dbo.Tmp_TaxInsuranceType.ActiveFlag'
GO
SET IDENTITY_INSERT dbo.Tmp_TaxInsuranceType ON
GO
IF EXISTS(SELECT * FROM sysobjects where name = N'TaxInsuranceType' AND type = 'U')
BEGIN
	EXEC('INSERT INTO dbo.Tmp_TaxInsuranceType (oID, Name, SortOrder)
		SELECT oID, Name, SortOrder FROM dbo.TaxInsuranceType WITH (HOLDLOCK TABLOCKX)')

	declare @actualrowcount int 
	select @actualrowcount = count(*) from TaxInsuranceType
	print 'TaxInsuranceType row count : ' + convert(varchar, @actualrowcount)

	declare @tmprowcount int 
	select @tmprowcount = count(*) from Tmp_TaxInsuranceType
	print 'Tmp_TaxInsuranceType row count : ' + convert(varchar, @tmprowcount)

	if @actualrowcount = @tmprowcount 
	begin
		print 'actual row count and temp table row count match, dropping table...'
		drop table dbo.TaxInsuranceType
	end
END
GO
SET IDENTITY_INSERT dbo.Tmp_TaxInsuranceType OFF
GO
EXECUTE sp_rename N'dbo.Tmp_TaxInsuranceType', N'TaxInsuranceType', 'OBJECT' 
GO
ALTER TABLE dbo.TaxInsuranceType ADD CONSTRAINT
	PK_tax_insurance PRIMARY KEY CLUSTERED 
	(
	oID
	) ON [PRIMARY]
GO
COMMIT
GO
GRANT SELECT,INSERT,DELETE,UPDATE ON TaxInsuranceType TO public AS dbo
GO
DENY SELECT,INSERT,DELETE,UPDATE ON TaxInsuranceType TO www_role
GO
IF NOT EXISTS(SELECT * FROM TaxInsuranceType)
BEGIN
	INSERT INTO [dbo].[TaxInsuranceType]([Name],[Sortorder])VALUES('Incl. in Mtg pmt',1)
	INSERT INTO [dbo].[TaxInsuranceType]([Name],[Sortorder])VALUES('Not incl. in Mtg',2)
END
GO