USE [DebtPlus]
GO
/****** Object:  Table [dbo].[registers_client]    Script Date: 09/15/2014 13:13:26 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[registers_client](
	[client_register] [dbo].[typ_key] IDENTITY(1,1) NOT NULL,
	[tran_type] [dbo].[typ_transaction] NOT NULL,
	[tran_subtype] [dbo].[typ_transaction] NULL,
	[client] [dbo].[typ_client] NOT NULL,
	[trust_register] [dbo].[typ_key] NULL,
	[disbursement_register] [dbo].[typ_key] NULL,
	[item_date] [datetime] NULL,
	[credit_amt] [money] NULL,
	[debit_amt] [money] NULL,
	[message] [dbo].[typ_message] NULL,
	[date_created] [dbo].[typ_date] NOT NULL,
	[created_by] [dbo].[typ_counselor] NOT NULL,
	[old_client_register] [int] NULL,
 CONSTRAINT [PK_registers_client] PRIMARY KEY CLUSTERED 
(
	[client_register] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 90) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
