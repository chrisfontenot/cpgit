USE [DebtPlus]
GO
-- This table is no longer needed since the values have been converted into the "number of months" rather than a code value.
if exists (select * from sysobjects where type = 'U' and name = N'Housing_DelinquencyTypes')
	drop table Housing_DelinquencyTypes
GO
