EXEC('ALTER TABLE dbo.Housing_Properties DROP CONSTRAINT FK_Housing_Properties_client_housing')
GO
EXEC('ALTER TABLE dbo.Housing_Properties DROP CONSTRAINT FK_Housing_Properties_Housing_ReasonForCallTypes')
GO
EXEC('ALTER TABLE dbo.client_disclosures DROP CONSTRAINT FK_client_disclosures_Housing_properties')
GO
EXEC('ALTER TABLE dbo.Housing_loans DROP CONSTRAINT FK_Housing_Loans_Housing_properties')
GO
EXEC('ALTER TABLE dbo.Housing_PropertyNotes DROP CONSTRAINT FK_Housing_PropertyNotes_Housing_properties')
GO
EXEC('ALTER TABLE dbo.HPFSubmissions DROP CONSTRAINT FK_HPFSubmissions_Housing_properties')
GO
EXEC('ALTER TABLE dbo.HPFOutcomes DROP CONSTRAINT FK_HPFOutcomes_Housing_properties')
GO
/* To prevent any potential data loss issues, you should review this script in detail before running it outside the context of the database designer.*/
BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION
GO
CREATE TABLE dbo.Tmp_Housing_Properties
	(
	oID dbo.typ_key NOT NULL IDENTITY (1, 1),
	HousingID dbo.typ_key NOT NULL,
	UseHomeAddress bit NOT NULL,
	PropertyAddress dbo.typ_key NULL,
	PreviousAddress dbo.typ_key NULL,
	TrustName varchar(256) NULL,
	OwnerID dbo.typ_key NULL,
	CoOwnerID dbo.typ_key NULL,
	Residency dbo.typ_key NOT NULL,
	Occupancy int NOT NULL,
	PropertyType dbo.typ_key NOT NULL,
	PurchaseYear int NOT NULL,
	PurchasePrice money NOT NULL,
	LandValue money NOT NULL,
	ImprovementsValue money NOT NULL,
	PlanToKeepHomeCD dbo.typ_key NULL,
	ForSaleIND bit NOT NULL,
	SalePrice money NOT NULL,
	RealityCompany varchar(50) NULL,
	sales_contract_date datetime NULL,
	FcNoticeReceivedIND bit NOT NULL,
	FcSaleDT datetime NULL,
	UseInReports bit NOT NULL,
	last_change_date dbo.typ_date NULL,
	last_change_by dbo.typ_counselor NULL,
	property_tax dbo.typ_key NULL,
	annual_property_tax money NOT NULL,
	tax_delinq_state dbo.typ_key NULL,
	tax_due_date datetime NULL,
	tax_due_amount money NOT NULL,
	pmi_insurance dbo.typ_key NULL,
	annual_ins_amount money NOT NULL,
	ins_delinq_state dbo.typ_key NULL,
	ins_due_date datetime NULL,
	ins_due_amount money NOT NULL,
	homeowner_ins int NULL,
	HOA_delinq_state int NULL,
	HOA_delinq_amount money NOT NULL,
	Appraisal_Type int NULL,
	Appraisal_Date datetime NULL,
	HPF_Program dbo.typ_key NULL,
	HPF_SubProgram dbo.typ_key NULL,
	HPF_Counselor dbo.typ_key NULL,
	HPF_interviewID dbo.typ_key NULL,
	HPF_foreclosureCaseID dbo.typ_key NULL,
	HPF_worked_with_other_agency bit NOT NULL,
	HPF_ReasonForCallCD dbo.typ_key NULL,
	ts timestamp NOT NULL,
	date_created dbo.typ_date NOT NULL,
	created_by dbo.typ_counselor NOT NULL
	)  ON [PRIMARY]
GO
GRANT DELETE ON dbo.Tmp_Housing_Properties TO public  AS dbo
GO
GRANT INSERT ON dbo.Tmp_Housing_Properties TO public  AS dbo
GO
GRANT SELECT ON dbo.Tmp_Housing_Properties TO public  AS dbo
GO
GRANT UPDATE ON dbo.Tmp_Housing_Properties TO public  AS dbo
GO
DECLARE @v sql_variant 
SET @v = N'Does the client plan to keep the home?'
EXECUTE sp_addextendedproperty N'MS_Description', @v, N'SCHEMA', N'dbo', N'TABLE', N'Tmp_Housing_Properties', N'COLUMN', N'PlanToKeepHomeCD'
GO
DECLARE @v sql_variant 
SET @v = N'Pointer to the hud_interviews table for the interview that we used with this case. It is for tracking purposes only and is used on a re-submission of a partial case.'
EXECUTE sp_addextendedproperty N'MS_Description', @v, N'SCHEMA', N'dbo', N'TABLE', N'Tmp_Housing_Properties', N'COLUMN', N'HPF_interviewID'
GO
DECLARE @v sql_variant 
SET @v = N'HPF Foreclosure case ID returned by HPF for this case'
EXECUTE sp_addextendedproperty N'MS_Description', @v, N'SCHEMA', N'dbo', N'TABLE', N'Tmp_Housing_Properties', N'COLUMN', N'HPF_foreclosureCaseID'
GO
DECLARE @v sql_variant 
SET @v = N'Date/Time when the property was created'
EXECUTE sp_addextendedproperty N'MS_Description', @v, N'SCHEMA', N'dbo', N'TABLE', N'Tmp_Housing_Properties', N'COLUMN', N'date_created'
GO
DECLARE @v sql_variant 
SET @v = N'Person who created the property record'
EXECUTE sp_addextendedproperty N'MS_Description', @v, N'SCHEMA', N'dbo', N'TABLE', N'Tmp_Housing_Properties', N'COLUMN', N'created_by'
GO
EXECUTE sp_bindefault N'dbo.default_true', N'dbo.Tmp_Housing_Properties.UseHomeAddress'
GO
EXECUTE sp_bindefault N'dbo.default_zero', N'dbo.Tmp_Housing_Properties.Occupancy'
GO
EXECUTE sp_bindefault N'dbo.default_zero', N'dbo.Tmp_Housing_Properties.PurchaseYear'
GO
EXECUTE sp_bindefault N'dbo.default_money', N'dbo.Tmp_Housing_Properties.PurchasePrice'
GO
EXECUTE sp_bindefault N'dbo.default_money', N'dbo.Tmp_Housing_Properties.LandValue'
GO
EXECUTE sp_bindefault N'dbo.default_money', N'dbo.Tmp_Housing_Properties.ImprovementsValue'
GO
EXECUTE sp_bindefault N'dbo.default_false', N'dbo.Tmp_Housing_Properties.ForSaleIND'
GO
EXECUTE sp_bindefault N'dbo.default_zero', N'dbo.Tmp_Housing_Properties.SalePrice'
GO
EXECUTE sp_bindefault N'dbo.default_false', N'dbo.Tmp_Housing_Properties.FcNoticeReceivedIND'
GO
EXECUTE sp_bindefault N'dbo.default_false', N'dbo.Tmp_Housing_Properties.UseInReports'
GO
EXECUTE sp_bindefault N'dbo.default_money', N'dbo.Tmp_Housing_Properties.annual_property_tax'
GO
EXECUTE sp_bindefault N'dbo.default_money', N'dbo.Tmp_Housing_Properties.tax_due_amount'
GO
EXECUTE sp_bindefault N'dbo.default_money', N'dbo.Tmp_Housing_Properties.annual_ins_amount'
GO
EXECUTE sp_bindefault N'dbo.default_money', N'dbo.Tmp_Housing_Properties.ins_due_amount'
GO
EXECUTE sp_bindefault N'dbo.default_money', N'dbo.Tmp_Housing_Properties.HOA_delinq_amount'
GO
EXECUTE sp_bindefault N'dbo.default_false', N'dbo.Tmp_Housing_Properties.HPF_worked_with_other_agency'
GO
SET IDENTITY_INSERT dbo.Tmp_Housing_Properties ON
GO
IF EXISTS(SELECT * FROM dbo.Housing_Properties)
	 EXEC('INSERT INTO dbo.Tmp_Housing_Properties (oID, HousingID, UseHomeAddress, PropertyAddress, PreviousAddress, TrustName, OwnerID, CoOwnerID, Residency, Occupancy, PropertyType, PurchaseYear, PurchasePrice, LandValue, ImprovementsValue, PlanToKeepHomeCD, ForSaleIND, SalePrice, RealityCompany, sales_contract_date, FcNoticeReceivedIND, FcSaleDT, UseInReports, last_change_date, last_change_by, property_tax, annual_property_tax, tax_delinq_state, tax_due_date, tax_due_amount, pmi_insurance, annual_ins_amount, ins_delinq_state, ins_due_date, ins_due_amount, homeowner_ins, HOA_delinq_state, HOA_delinq_amount, Appraisal_Type, Appraisal_Date, HPF_Program, HPF_SubProgram, HPF_Counselor, HPF_interviewID, HPF_foreclosureCaseID, HPF_worked_with_other_agency, HPF_ReasonForCallCD, date_created, created_by)
		SELECT oID, HousingID, UseHomeAddress, PropertyAddress, PreviousAddress, TrustName, OwnerID, CoOwnerID, Residency, Occupancy, PropertyType, PurchaseYear, PurchasePrice, LandValue, ImprovementsValue, PlanToKeepHomeCD, ForSaleIND, SalePrice, RealityCompany, sales_contract_date, FcNoticeReceivedIND, FcSaleDT, UseInReports, last_change_date, last_change_by, property_tax, annual_property_tax, tax_delinq_state, tax_due_date, tax_due_amount, pmi_insurance, annual_ins_amount, ins_delinq_state, ins_due_date, ins_due_amount, homeowner_ins, HOA_delinq_state, HOA_delinq_amount, Appraisal_Type, Appraisal_Date, HPF_Program, HPF_SubProgram, HPF_Counselor, HPF_interviewID, HPF_foreclosureCaseID, HPF_worked_with_other_agency, HPF_ReasonForCallCD, date_created, created_by FROM dbo.Housing_Properties WITH (HOLDLOCK TABLOCKX)')
GO
SET IDENTITY_INSERT dbo.Tmp_Housing_Properties OFF
GO
DROP TABLE dbo.Housing_Properties
GO
EXECUTE sp_rename N'dbo.Tmp_Housing_Properties', N'Housing_Properties', 'OBJECT' 
GO
ALTER TABLE dbo.Housing_Properties ADD CONSTRAINT
	PK_housing_properties PRIMARY KEY CLUSTERED 
	(
	oID
	) WITH( STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX IX_Housing_Properties_1 ON dbo.Housing_Properties
	(
	HousingID
	) WITH( STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX IX_Housing_Properties_2 ON dbo.Housing_Properties
	(
	OwnerID
	) WITH( STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
CREATE TRIGGER [trig_housing_properties_D] on dbo.Housing_Properties AFTER DELETE as
begin

	set nocount on

	delete	housing_borrowers
	from	housing_borrowers b
	inner join deleted d on d.CoOwnerID = b.oID
	where	d.CoOwnerID is not null;
	
	delete	housing_borrowers
	from	housing_borrowers b
	inner join deleted d on d.OwnerID = b.oID
	where	d.OwnerID is not null;
	
	delete	housing_loans
	from	housing_loans l
	inner join deleted d on d.oID = l.PropertyID;
	
	delete	addresses
	from	addresses a
	inner join deleted d on d.PropertyAddress = a.address
	where	d.PropertyAddress is not null;

	delete	addresses
	from	addresses a
	inner join deleted d on d.PreviousAddress = a.address
	where	d.PreviousAddress is not null;
end
GO
create trigger housing_properties_U on dbo.Housing_Properties AFTER UPDATE AS
BEGIN
	-- ======================================================================================
	-- ==             Record the ID and Timestamp of the last update to the rows           ==
	-- ======================================================================================

	-- Suppress intermediate results
	set nocount on

	-- Set the ID and date when the row was last updated unless the update includes that information
	if not update(last_change_date) AND not update(last_change_by)
		update	housing_properties
		set		last_change_date	= getdate(),
				last_change_by		= suser_sname()
		from	housing_properties p
		inner join inserted i on p.oID = i.oID;
END
GO
CREATE trigger housing_properties_I on dbo.Housing_Properties AFTER INSERT AS
BEGIN
	-- ======================================================================================
	-- ==             Record the ID and Timestamp of the last update to the rows           ==
	-- ======================================================================================

	-- Suppress intermediate results
	set nocount on

	-- Set the ID and date when the row was last updated
	update	housing_properties
	set		last_change_date	= getdate(),
			last_change_by		= suser_sname()
	from	housing_properties p
	inner join inserted i on p.oID = i.oID;
END
GO
DENY DELETE ON dbo.Housing_Properties TO www_role  AS dbo 
GO
DENY INSERT ON dbo.Housing_Properties TO www_role  AS dbo 
GO
DENY SELECT ON dbo.Housing_Properties TO www_role  AS dbo 
GO
DENY UPDATE ON dbo.Housing_Properties TO www_role  AS dbo 
GO
COMMIT
BEGIN TRANSACTION
GO
EXEC('ALTER TABLE dbo.HPFOutcomes ADD CONSTRAINT FK_HPFOutcomes_Housing_properties FOREIGN KEY ( propertyID ) REFERENCES dbo.Housing_Properties ( oID ) ON UPDATE CASCADE ON DELETE CASCADE')
GO
EXEC('ALTER TABLE dbo.HPFSubmissions ADD CONSTRAINT FK_HPFSubmissions_Housing_properties FOREIGN KEY ( propertyID ) REFERENCES dbo.Housing_Properties ( oID ) ON UPDATE CASCADE ON DELETE CASCADE')
GO
EXEC('ALTER TABLE dbo.Housing_PropertyNotes ADD CONSTRAINT FK_Housing_PropertyNotes_Housing_properties FOREIGN KEY ( PropertyID ) REFERENCES dbo.Housing_Properties ( oID ) ON UPDATE CASCADE ON DELETE CASCADE')
GO
EXEC('ALTER TABLE dbo.Housing_loans ADD CONSTRAINT FK_Housing_Loans_Housing_properties FOREIGN KEY ( PropertyID ) REFERENCES dbo.Housing_Properties ( oID ) ON UPDATE CASCADE ON DELETE CASCADE')
GO
EXEC('ALTER TABLE dbo.client_disclosures ADD CONSTRAINT FK_client_disclosures_Housing_properties FOREIGN KEY ( propertyID ) REFERENCES dbo.Housing_Properties ( oID ) ON UPDATE NO ACTION ON DELETE SET NULL')
GO
EXEC('ALTER TABLE dbo.Housing_Properties ADD CONSTRAINT FK_Housing_Properties_Housing_ReasonForCallTypes FOREIGN KEY ( HPF_ReasonForCallCD ) REFERENCES dbo.Housing_ReasonForCallTypes ( oID ) ON UPDATE CASCADE ON DELETE SET NULL')
GO
EXEC('ALTER TABLE dbo.Housing_Properties ADD CONSTRAINT FK_Housing_Properties_client_housing FOREIGN KEY ( HousingID ) REFERENCES dbo.client_housing ( client ) ON UPDATE CASCADE ON DELETE CASCADE')
GO
COMMIT
