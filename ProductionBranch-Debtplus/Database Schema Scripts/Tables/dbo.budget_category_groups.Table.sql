USE [DebtPlus]
GO
/****** Object:  Table [dbo].[budget_category_groups]    Script Date: 09/15/2014 13:13:25 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[budget_category_groups](
	[budget_category_group] [dbo].[typ_key] IDENTITY(1,1) NOT NULL,
	[description] [dbo].[typ_description] NOT NULL,
	[default] [bit] NOT NULL,
	[rpps_code] [int] NULL,
 CONSTRAINT [PK_budget_category_groups] PRIMARY KEY CLUSTERED 
(
	[budget_category_group] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
