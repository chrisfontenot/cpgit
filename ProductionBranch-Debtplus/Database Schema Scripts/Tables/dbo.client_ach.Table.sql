USE [DebtPlus]
GO
/****** Object:  Table [dbo].[client_ach]    Script Date: 09/15/2014 13:13:25 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[client_ach](
	[client] [dbo].[typ_client] NOT NULL,
	[CheckingSavings] [dbo].[typ_CheckingSavings] NOT NULL,
	[ABA] [dbo].[typ_ach_routing] NOT NULL,
	[AccountNumber] [varchar](50) NOT NULL,
	[StartDate] [datetime] NOT NULL,
	[EndDate] [datetime] NULL,
	[ErrorDate] [datetime] NULL,
	[PrenoteDate] [datetime] NULL,
	[ContractDate] [datetime] NULL,
	[EnrollDate] [datetime] NOT NULL,
	[isActive] [bit] NOT NULL,
	[date_created] [dbo].[typ_date] NOT NULL,
	[created_by] [dbo].[typ_counselor] NOT NULL,
	[old_client] [int] NULL,
 CONSTRAINT [PK_client_ach] PRIMARY KEY CLUSTERED 
(
	[client] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
