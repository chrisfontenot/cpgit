/* To prevent any potential data loss issues, you should review this script in detail before running it outside the context of the database designer.*/
BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION
GO
execute UPDATE_drop_constraints 'asset_ids'
execute UPDATE_ADD_COLUMN 'asset_ids', 'hpf'
GO
CREATE TABLE dbo.Tmp_asset_ids
	(
	asset_id dbo.typ_key NOT NULL IDENTITY (1, 1),
	description dbo.typ_description NOT NULL,
	maximum money NULL,
	rpps_code int NOT NULL,
	hpf varchar(50) NULL,
	date_created dbo.typ_date NOT NULL,
	created_by dbo.typ_counselor NOT NULL,
	ts timestamp NOT NULL
	) ON [PRIMARY]
GO
GRANT DELETE ON dbo.Tmp_asset_ids TO public  AS dbo
GO
GRANT INSERT ON dbo.Tmp_asset_ids TO public  AS dbo
GO
GRANT SELECT ON dbo.Tmp_asset_ids TO public  AS dbo
GO
GRANT UPDATE ON dbo.Tmp_asset_ids TO public  AS dbo
GO
DECLARE @v sql_variant 
SET @v = N'This table lists the system defined asset ids. An asset id is an additional monthly source of income. Items such as retirement, spousal/child support are listed in this table.'
EXECUTE sp_addextendedproperty N'MS_Description', @v, N'SCHEMA', N'dbo', N'TABLE', N'Tmp_asset_ids', NULL, NULL
GO
DECLARE @v sql_variant 
SET @v = N'Asset ID in the table. This is the primary key to the table. It is referenced by the assets table.'
EXECUTE sp_addextendedproperty N'MS_Description', @v, N'SCHEMA', N'dbo', N'TABLE', N'Tmp_asset_ids', N'COLUMN', N'asset_id'
GO
DECLARE @v sql_variant 
SET @v = N'Description of the asset'
EXECUTE sp_addextendedproperty N'MS_Description', @v, N'SCHEMA', N'dbo', N'TABLE', N'Tmp_asset_ids', N'COLUMN', N'description'
GO
DECLARE @v sql_variant 
SET @v = N'Dollar amount of the maximum permitted by the field. A warning is generated if the entry exceeds this limit.'
EXECUTE sp_addextendedproperty N'MS_Description', @v, N'SCHEMA', N'dbo', N'TABLE', N'Tmp_asset_ids', N'COLUMN', N'maximum'
GO
DECLARE @v sql_variant 
SET @v = N'Value used in the RPPS EDI proposals to indicate items such as "child support" or "retirement".'
EXECUTE sp_addextendedproperty N'MS_Description', @v, N'SCHEMA', N'dbo', N'TABLE', N'Tmp_asset_ids', N'COLUMN', N'rpps_code'
GO
DECLARE @v sql_variant 
SET @v = N'Date/Time when the row was created.'
EXECUTE sp_addextendedproperty N'MS_Description', @v, N'SCHEMA', N'dbo', N'TABLE', N'Tmp_asset_ids', N'COLUMN', N'date_created'
GO
DECLARE @v sql_variant 
SET @v = N'Person who created the row.'
EXECUTE sp_addextendedproperty N'MS_Description', @v, N'SCHEMA', N'dbo', N'TABLE', N'Tmp_asset_ids', N'COLUMN', N'created_by'
GO
ALTER TABLE dbo.Tmp_asset_ids ADD CONSTRAINT
	DF_asset_ids_rpps_code DEFAULT (6) FOR rpps_code
GO
SET IDENTITY_INSERT dbo.Tmp_asset_ids ON
GO
IF EXISTS(SELECT * FROM dbo.asset_ids)
	 EXEC('INSERT INTO dbo.Tmp_asset_ids (asset_id, description, maximum, rpps_code, date_created, created_by, hpf)
		SELECT asset_id, description, maximum, rpps_code, date_created, created_by, hpf FROM dbo.asset_ids WITH (HOLDLOCK TABLOCKX)')
GO
SET IDENTITY_INSERT dbo.Tmp_asset_ids OFF
GO
DROP TABLE dbo.asset_ids
GO
EXECUTE sp_rename N'dbo.Tmp_asset_ids', N'asset_ids', 'OBJECT' 
GO
ALTER TABLE dbo.asset_ids ADD CONSTRAINT
	PK_asset_ids PRIMARY KEY CLUSTERED 
	(
	asset_id
	) WITH( PAD_INDEX = OFF, FILLFACTOR = 90, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
CREATE TRIGGER [dbo].[trig_asset_ids_D] on dbo.asset_ids AFTER DELETE as
begin
	-- ================================================================
	-- ==     Process the deletion of an asset_id                    ==
	-- ================================================================

	set nocount on

	-- Remove the client assets for this type
	delete	assets
	from	assets i
	inner join deleted d on i.asset_id = d.asset_id
	
	-- Remove the intake assets for this type
	delete	intake_assets
	from	intake_assets i
	inner join deleted d on i.asset_id = d.asset_id
end
GO
COMMIT
GO
