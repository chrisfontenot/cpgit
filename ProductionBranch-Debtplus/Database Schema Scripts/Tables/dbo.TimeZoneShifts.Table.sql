USE [DebtPlus]
GO
/****** Object:  Table [dbo].[TimeZoneShifts]    Script Date: 09/15/2014 13:13:26 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[TimeZoneShifts](
	[oID] [dbo].[typ_key] IDENTITY(1,1) NOT NULL,
	[DSTRule] [int] NOT NULL,
	[StartingPeriod] [datetime] NOT NULL,
	[ShiftOffset] [float] NOT NULL,
 CONSTRAINT [PK_TimeZoneShifts_1] PRIMARY KEY CLUSTERED 
(
	[oID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
