USE [DebtPlus]
GO
/****** Object:  Table [dbo].[creditor_types]    Script Date: 09/15/2014 13:13:26 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[creditor_types](
	[type] [char](1) NOT NULL,
	[description] [dbo].[typ_description] NOT NULL,
	[sic_prefix] [dbo].[typ_sic] NOT NULL,
	[send_pledge_letter] [dbo].[typ_key] NULL,
	[send_contribution_letter] [dbo].[typ_key] NULL,
	[send_car_letter] [dbo].[typ_key] NULL,
	[contribution_pct] [dbo].[typ_fairshare_rate] NULL,
	[status] [dbo].[typ_creditor_type] NULL,
	[cycle] [dbo].[typ_contrib_cycle] NULL,
	[nfcc_type] [varchar](4) NULL,
	[proposals] [int] NULL,
	[date_created] [dbo].[typ_date] NOT NULL,
	[created_by] [dbo].[typ_counselor] NOT NULL,
 CONSTRAINT [PK_creditor_types] PRIMARY KEY CLUSTERED 
(
	[type] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 90) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'This is the type of the creditor. It is the first character from the creditor ID.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'creditor_types', @level2type=N'COLUMN',@level2name=N'type'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'A textual description for the class of creditor. Used in reports and full disclosure proposals.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'creditor_types', @level2type=N'COLUMN',@level2name=N'description'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Used in generating a default SIC code. This is the prefix to the SIC value. The number is appended to this to form the actual SIC code.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'creditor_types', @level2type=N'COLUMN',@level2name=N'sic_prefix'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Letter to send for a pledge. When creditor is configured to have a pledge cycle, this is the letter code to be used.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'creditor_types', @level2type=N'COLUMN',@level2name=N'send_pledge_letter'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Letter code used to request contributions. This is the letter code when a creditor is configured for "No contributions" in order to request contributions.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'creditor_types', @level2type=N'COLUMN',@level2name=N'send_contribution_letter'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Letter ID to send for a Contribution Assistance Request' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'creditor_types', @level2type=N'COLUMN',@level2name=N'send_car_letter'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Default contribution percentage for both checks and EFT.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'creditor_types', @level2type=N'COLUMN',@level2name=N'contribution_pct'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Should this creditor be Billed, Deducted, or No contributions' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'creditor_types', @level2type=N'COLUMN',@level2name=N'status'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'If the creditor is Billed, this is the billing cycle.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'creditor_types', @level2type=N'COLUMN',@level2name=N'cycle'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'This is us the creditor type used in the NFCC ScoreCard. Please refer to the NCCRC document for the proper values.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'creditor_types', @level2type=N'COLUMN',@level2name=N'nfcc_type'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Date/Time when the row was created.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'creditor_types', @level2type=N'COLUMN',@level2name=N'date_created'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Person who created the row.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'creditor_types', @level2type=N'COLUMN',@level2name=N'created_by'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'This table lists the creditor types and the default values when a creditor is created.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'creditor_types'
GO
