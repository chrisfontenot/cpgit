USE [DebtPlus]
GO
/****** Object:  Table [dbo].[disbursement_creditors]    Script Date: 09/15/2014 13:13:26 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[disbursement_creditors](
	[oID] [int] IDENTITY(1,1) NOT NULL,
	[disbursement_register] [dbo].[typ_key] NOT NULL,
	[client_creditor] [dbo].[typ_key] NOT NULL,
	[client] [dbo].[typ_client] NOT NULL,
	[creditor] [dbo].[typ_creditor] NOT NULL,
	[priority] [int] NOT NULL,
	[tran_type] [dbo].[typ_transaction] NOT NULL,
	[creditor_type] [dbo].[typ_creditor_type] NOT NULL,
	[disbursement_factor] [money] NOT NULL,
	[sched_payment] [money] NOT NULL,
	[current_month_disbursement] [money] NOT NULL,
	[orig_balance] [money] NOT NULL,
	[current_balance] [money] NOT NULL,
	[apr] [dbo].[typ_fairshare_rate] NOT NULL,
	[debit_amt] [money] NOT NULL,
	[fairshare_amt] [money] NOT NULL,
	[fairshare_pct_check] [dbo].[typ_fairshare_rate] NULL,
	[fairshare_pct_eft] [dbo].[typ_fairshare_rate] NULL,
	[trust_register] [dbo].[typ_key] NULL,
	[held] [bit] NOT NULL,
	[bank] [dbo].[typ_key] NOT NULL,
	[rpps_biller_id] [dbo].[typ_rpps_biller_id] NOT NULL,
	[date_created] [dbo].[typ_date] NOT NULL,
	[created_by] [dbo].[typ_counselor] NOT NULL,
 CONSTRAINT [PK_disbursement_creditors_1] PRIMARY KEY CLUSTERED 
(
	[oID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Disbursement register ID for the disbursement' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'disbursement_creditors', @level2type=N'COLUMN',@level2name=N'disbursement_register'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Client ID for the debt' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'disbursement_creditors', @level2type=N'COLUMN',@level2name=N'client'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Creditor ID for the debt' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'disbursement_creditors', @level2type=N'COLUMN',@level2name=N'creditor'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Priority for the current debt as extracted from the debt record' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'disbursement_creditors', @level2type=N'COLUMN',@level2name=N'priority'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Transaction type (''AD'' or ''BW'') depending upon the type of payments to the creditor' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'disbursement_creditors', @level2type=N'COLUMN',@level2name=N'tran_type'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Type of the creditor (''B''/''D''/''N'')' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'disbursement_creditors', @level2type=N'COLUMN',@level2name=N'creditor_type'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Original disbursement factor for the debt' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'disbursement_creditors', @level2type=N'COLUMN',@level2name=N'disbursement_factor'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Scheduled payment for the debt' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'disbursement_creditors', @level2type=N'COLUMN',@level2name=N'sched_payment'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Current amount disbursed this month' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'disbursement_creditors', @level2type=N'COLUMN',@level2name=N'current_month_disbursement'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Original balance for the debt' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'disbursement_creditors', @level2type=N'COLUMN',@level2name=N'orig_balance'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Current debt balance' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'disbursement_creditors', @level2type=N'COLUMN',@level2name=N'current_balance'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'A.P.R. used by the creditor for the debt' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'disbursement_creditors', @level2type=N'COLUMN',@level2name=N'apr'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Gross amount of the disbursement for this debt' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'disbursement_creditors', @level2type=N'COLUMN',@level2name=N'debit_amt'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Fairshare amount for this transaction' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'disbursement_creditors', @level2type=N'COLUMN',@level2name=N'fairshare_amt'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Fairshare percentage for a check disbursement' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'disbursement_creditors', @level2type=N'COLUMN',@level2name=N'fairshare_pct_check'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Fairshare percentage for EFT disbursement' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'disbursement_creditors', @level2type=N'COLUMN',@level2name=N'fairshare_pct_eft'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Pointer to the trust register for this item' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'disbursement_creditors', @level2type=N'COLUMN',@level2name=N'trust_register'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Is the debt held (do not disburse)? If so, it is listed but no disbursements are permitted' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'disbursement_creditors', @level2type=N'COLUMN',@level2name=N'held'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Date/Time when the row was created.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'disbursement_creditors', @level2type=N'COLUMN',@level2name=N'date_created'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Person who created the row.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'disbursement_creditors', @level2type=N'COLUMN',@level2name=N'created_by'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'During auto-disbursement, this table holds the debt (client_creditor) information needed to do the disbursement operation.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'disbursement_creditors'
GO
