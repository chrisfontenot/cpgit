/* To prevent any potential data loss issues, you should review this script in detail before running it outside the context of the database designer.*/
BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
GO
EXECUTE UPDATE_DROP_CONSTRAINTS 'client_statement_clients'
EXECUTE UPDATE_ADD_COLUMN 'client_statement_clients', 'isFinal', 'int'
GO
BEGIN TRANSACTION
CREATE TABLE dbo.Tmp_client_statement_clients
	(
	client_statement uniqueidentifier NOT NULL ROWGUIDCOL,
	client_statement_batch dbo.typ_key NOT NULL,
	client dbo.typ_client NOT NULL,
	isMultiPage int NOT NULL,
	isACH int NOT NULL,
	isFinal int NOT NULL,
	active_status varchar(10) NOT NULL,
	active_debts int NOT NULL,
	last_deposit_date datetime NULL,
	last_deposit_amt money NOT NULL,
	deposit_amt money NOT NULL,
	refund_amt money NOT NULL,
	disbursement_amt money NOT NULL,
	held_in_trust money NOT NULL,
	reserved_in_trust money NOT NULL,
	starting_trust_balance money NOT NULL,
	counselor dbo.typ_key NULL,
	office dbo.typ_key NULL,
	expected_deposit_date varchar(50) NULL,
	expected_deposit_amt varchar(50) NULL,
	delivery_method varchar(4) NOT NULL,
	delivery_date datetime NULL,
	date_created dbo.typ_date NOT NULL,
	created_by dbo.typ_counselor NOT NULL,
	ts timestamp NOT NULL
	)  ON [PRIMARY]
GO
ALTER TABLE dbo.Tmp_client_statement_clients ADD CONSTRAINT
	DF_client_statement_clients_client_statement DEFAULT (newid()) FOR client_statement
GO
EXECUTE sp_bindefault N'dbo.default_zero', N'dbo.Tmp_client_statement_clients.isMultiPage'
GO
EXECUTE sp_bindefault N'dbo.default_zero', N'dbo.Tmp_client_statement_clients.isACH'
GO
EXECUTE sp_bindefault N'dbo.default_zero', N'dbo.Tmp_client_statement_clients.isFinal'
GO
EXECUTE sp_bindefault N'dbo.default_zero', N'dbo.Tmp_client_statement_clients.active_debts'
GO
EXECUTE sp_bindefault N'dbo.default_money', N'dbo.Tmp_client_statement_clients.last_deposit_amt'
GO
EXECUTE sp_bindefault N'dbo.default_money', N'dbo.Tmp_client_statement_clients.deposit_amt'
GO
EXECUTE sp_bindefault N'dbo.default_money', N'dbo.Tmp_client_statement_clients.refund_amt'
GO
EXECUTE sp_bindefault N'dbo.default_money', N'dbo.Tmp_client_statement_clients.disbursement_amt'
GO
EXECUTE sp_bindefault N'dbo.default_money', N'dbo.Tmp_client_statement_clients.held_in_trust'
GO
EXECUTE sp_bindefault N'dbo.default_money', N'dbo.Tmp_client_statement_clients.reserved_in_trust'
GO
EXECUTE sp_bindefault N'dbo.default_money', N'dbo.Tmp_client_statement_clients.starting_trust_balance'
GO
ALTER TABLE dbo.Tmp_client_statement_clients ADD CONSTRAINT
	DF_client_statement_clients_expected_deposit_amt DEFAULT ('0.00') FOR expected_deposit_amt
GO
ALTER TABLE dbo.Tmp_client_statement_clients ADD CONSTRAINT
	DF_client_statement_clients_delivery_method DEFAULT ('P') FOR delivery_method
GO
IF EXISTS(SELECT * FROM dbo.client_statement_clients)
	 EXEC('INSERT INTO dbo.Tmp_client_statement_clients (client_statement, client_statement_batch, client, isMultiPage, isACH, isFinal, active_status, active_debts, last_deposit_date, last_deposit_amt, deposit_amt, refund_amt, disbursement_amt, held_in_trust, reserved_in_trust, starting_trust_balance, counselor, office, expected_deposit_date, expected_deposit_amt, delivery_method, delivery_date, date_created, created_by)
		SELECT client_statement, client_statement_batch, client, isMultiPage, isACH, isnull(isFinal,0), active_status, active_debts, last_deposit_date, last_deposit_amt, deposit_amt, refund_amt, disbursement_amt, held_in_trust, reserved_in_trust, starting_trust_balance, counselor, office, expected_deposit_date, expected_deposit_amt, delivery_method, delivery_date, date_created, created_by FROM dbo.client_statement_clients WITH (HOLDLOCK TABLOCKX)')
GO
DROP TABLE dbo.client_statement_clients
GO
EXECUTE sp_rename N'dbo.Tmp_client_statement_clients', N'client_statement_clients', 'OBJECT' 
GO
ALTER TABLE dbo.client_statement_clients ADD CONSTRAINT
	PK_client_statement_clients PRIMARY KEY CLUSTERED 
	(
	client_statement
	) WITH( STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]

GO
CREATE UNIQUE NONCLUSTERED INDEX IX_client_statement_clients_1 ON dbo.client_statement_clients
	(
	client_statement_batch,
	client
	) WITH( PAD_INDEX = OFF, FILLFACTOR = 90, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX IX_client_statement_clients_2 ON dbo.client_statement_clients
	(
	client
	) WITH( PAD_INDEX = OFF, FILLFACTOR = 90, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX IX_client_statement_clients_3 ON dbo.client_statement_clients
	(
	client_statement_batch,
	active_status,
	active_debts
	) WITH( PAD_INDEX = OFF, FILLFACTOR = 90, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
ALTER TABLE dbo.client_statement_clients ADD CONSTRAINT
	FK_client_statement_clients_client_statement_batches FOREIGN KEY
	(
	client_statement_batch
	) REFERENCES dbo.client_statement_batches
	(
	client_statement_batch
	) ON UPDATE  CASCADE 
	 ON DELETE  CASCADE 
	
GO
ALTER TABLE dbo.client_statement_clients ADD CONSTRAINT
	FK_client_statement_clients_clients FOREIGN KEY
	(
	client
	) REFERENCES dbo.clients
	(
	client
	) ON UPDATE  CASCADE 
	 ON DELETE  CASCADE 
	
GO
ALTER TABLE dbo.client_statement_clients ADD CONSTRAINT
	FK_client_statement_clients_counselors FOREIGN KEY
	(
	counselor
	) REFERENCES dbo.counselors
	(
	Counselor
	) ON UPDATE  SET NULL 
	 ON DELETE  SET NULL 
	
GO
ALTER TABLE dbo.client_statement_clients ADD CONSTRAINT
	FK_client_statement_clients_offices FOREIGN KEY
	(
	office
	) REFERENCES dbo.offices
	(
	office
	) ON UPDATE  SET NULL 
	 ON DELETE  SET NULL 
GO
COMMIT
BEGIN TRANSACTION
GO
ALTER TABLE dbo.client_statement_details ADD CONSTRAINT
	FK_client_statement_details_client_statement_clients FOREIGN KEY
	(
	client_statement
	) REFERENCES dbo.client_statement_clients
	(
	client_statement
	) ON UPDATE  CASCADE 
	 ON DELETE  CASCADE 
GO
COMMIT
GO
GRANT SELECT,INSERT,UPDATE,DELETE ON dbo.Tmp_client_statement_clients TO public  AS dbo
DENY  SELECT,INSERT,UPDATE,DELETE ON dbo.client_statement_clients TO www_role
GO
