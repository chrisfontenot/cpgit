USE [DebtPlus]
GO
/****** Object:  Table [dbo].[client_retention_actions]    Script Date: 09/15/2014 13:13:26 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[client_retention_actions](
	[client_retention_action] [dbo].[typ_key] IDENTITY(1,1) NOT NULL,
	[client_retention_event] [dbo].[typ_key] NOT NULL,
	[retention_action] [dbo].[typ_key] NOT NULL,
	[message] [dbo].[typ_message] NULL,
	[amount] [money] NULL,
	[created_by] [dbo].[typ_counselor] NOT NULL,
	[date_created] [dbo].[typ_date] NOT NULL,
	[old_client_retention_action] [int] NULL,
 CONSTRAINT [PK_client_retention_actions] PRIMARY KEY CLUSTERED 
(
	[client_retention_action] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 90) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Primary key to the table. This is the specific action event for the client''s event.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'client_retention_actions', @level2type=N'COLUMN',@level2name=N'client_retention_action'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Client event for which this action is appropriate' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'client_retention_actions', @level2type=N'COLUMN',@level2name=N'client_retention_event'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'A pointer to the specific action recorded against the client''s event' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'client_retention_actions', @level2type=N'COLUMN',@level2name=N'retention_action'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'A general purpose message associated with the action.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'client_retention_actions', @level2type=N'COLUMN',@level2name=N'message'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'If a letter is needed, this is the amount set as the <followup.amount> field.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'client_retention_actions', @level2type=N'COLUMN',@level2name=N'amount'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Person who created the row.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'client_retention_actions', @level2type=N'COLUMN',@level2name=N'created_by'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Date/Time when the row was created.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'client_retention_actions', @level2type=N'COLUMN',@level2name=N'date_created'
GO
