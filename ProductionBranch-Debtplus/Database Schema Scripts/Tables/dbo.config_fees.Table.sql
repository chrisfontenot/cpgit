/* To prevent any potential data loss issues, you should review this script in detail before running it outside the context of the database designer.*/
BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
EXECUTE UPDATE_DROP_CONSTRAINTS 'config_fees'
EXECUTE UPDATE_ADD_COLUMN 'config_fees', 'preferred'
GO
BEGIN TRANSACTION
CREATE TABLE dbo.Tmp_config_fees
	(
	config_fee dbo.typ_key NOT NULL IDENTITY (1, 1),
	[Default] bit NOT NULL,
	ActiveFlag bit NOT NULL,
	preferred bit NOT NULL,
	fee_percent dbo.typ_fairshare_rate NULL,
	fee_per_creditor money NULL,
	fee_maximum money NULL,
	description dbo.typ_description NOT NULL,
	fee_type int NOT NULL,
	fee_base money NOT NULL,
	fee_disb_max money NULL,
	fee_monthly_max money NULL,
	fee_balance_max bit NOT NULL,
	fee_balance_inc money NULL,
	fee_sched_payment money NULL,
	state dbo.typ_key NULL,
	date_created dbo.typ_date NOT NULL,
	created_by dbo.typ_counselor NOT NULL,
	ts timestamp NOT NULL
	)  ON [PRIMARY]
GO
GRANT DELETE ON dbo.Tmp_config_fees TO public  AS dbo
GO
GRANT INSERT ON dbo.Tmp_config_fees TO public  AS dbo
GO
GRANT SELECT ON dbo.Tmp_config_fees TO public  AS dbo
GO
GRANT UPDATE ON dbo.Tmp_config_fees TO public  AS dbo
GO
DECLARE @v sql_variant 
SET @v = N'If relative to the state name, this is the state code. List each state only once.'
EXECUTE sp_addextendedproperty N'MS_Description', @v, N'SCHEMA', N'dbo', N'TABLE', N'Tmp_config_fees', N'COLUMN', N'state'
GO
EXECUTE sp_unbindefault N'dbo.Tmp_config_fees.fee_percent'
GO
EXECUTE sp_bindefault N'dbo.default_false', N'dbo.Tmp_config_fees.[Default]'
GO
EXECUTE sp_bindefault N'dbo.default_true', N'dbo.Tmp_config_fees.ActiveFlag'
GO
EXECUTE sp_bindefault N'dbo.default_false', N'dbo.Tmp_config_fees.preferred'
GO
EXECUTE sp_bindefault N'dbo.default_zero', N'dbo.Tmp_config_fees.fee_percent'
GO
EXECUTE sp_bindefault N'dbo.default_zero', N'dbo.Tmp_config_fees.fee_per_creditor'
GO
EXECUTE sp_bindefault N'dbo.default_money', N'dbo.Tmp_config_fees.fee_maximum'
GO
EXECUTE sp_bindefault N'dbo.default_money', N'dbo.Tmp_config_fees.fee_base'
GO
EXECUTE sp_bindefault N'dbo.default_money', N'dbo.Tmp_config_fees.fee_disb_max'
GO
EXECUTE sp_bindefault N'dbo.default_money', N'dbo.Tmp_config_fees.fee_monthly_max'
GO
EXECUTE sp_bindefault N'dbo.default_false', N'dbo.Tmp_config_fees.fee_balance_max'
GO
EXECUTE sp_bindefault N'dbo.default_money', N'dbo.Tmp_config_fees.fee_balance_inc'
GO
EXECUTE sp_bindefault N'dbo.default_money', N'dbo.Tmp_config_fees.fee_sched_payment'
GO
SET IDENTITY_INSERT dbo.Tmp_config_fees ON
GO
EXECUTE sp_unbindrule N'dbo.Tmp_config_fees.fee_percent'
GO
IF EXISTS(SELECT * FROM dbo.config_fees)
	 EXEC('INSERT INTO dbo.Tmp_config_fees (config_fee, [Default], ActiveFlag, preferred, fee_percent, fee_per_creditor, fee_maximum, description, fee_type, fee_base, fee_disb_max, fee_monthly_max, fee_balance_max, fee_balance_inc, fee_sched_payment, state, date_created, created_by)
		SELECT config_fee, [Default], ActiveFlag, ISNULL(preferred,0), fee_percent, fee_per_creditor, fee_maximum, description, fee_type, fee_base, fee_disb_max, fee_monthly_max, fee_balance_max, fee_balance_inc, fee_sched_payment, state, date_created, created_by FROM dbo.config_fees WITH (HOLDLOCK TABLOCKX)')
GO
SET IDENTITY_INSERT dbo.Tmp_config_fees OFF
GO
DROP TABLE dbo.config_fees
GO
EXECUTE sp_rename N'dbo.Tmp_config_fees', N'config_fees', 'OBJECT' 
GO
ALTER TABLE dbo.config_fees ADD CONSTRAINT
	PK_config_fees PRIMARY KEY NONCLUSTERED 
	(
	config_fee
	) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX IX_config_fees_1 ON dbo.config_fees
	(
	state
	) ON [PRIMARY]
GO
update config_fees set [state] = null where [state] not in (select [state] from states)
GO
ALTER TABLE dbo.config_fees ADD CONSTRAINT
	FK_config_fees_states FOREIGN KEY
	(
	state
	) REFERENCES dbo.states
	(
	state
	) ON UPDATE  CASCADE 
	 ON DELETE  CASCADE 
GO
EXECUTE sp_bindrule N'dbo.valid_fairshare_rate', N'dbo.config_fees.fee_percent'
GO
EXECUTE sp_bindrule N'dbo.valid_fee_type', N'dbo.config_fees.fee_type'
GO
DENY DELETE ON dbo.config_fees TO www_role  AS dbo 
GO
DENY INSERT ON dbo.config_fees TO www_role  AS dbo 
GO
DENY SELECT ON dbo.config_fees TO www_role  AS dbo 
GO
DENY UPDATE ON dbo.config_fees TO www_role  AS dbo 
GO
COMMIT
BEGIN TRANSACTION
GO
update clients set config_fee = dbo.default_config_fee() where config_fee not in (select config_fee from config_fees)
GO
ALTER TABLE dbo.clients ADD CONSTRAINT
	FK_clients_config_fees FOREIGN KEY
	(
	config_fee
	) REFERENCES dbo.config_fees
	(
	config_fee
	) ON UPDATE  NO ACTION 
	 ON DELETE  NO ACTION 
GO
DECLARE @v sql_variant 
SET @v = N'Do not allow the config_fee entry to be deleted if it is still referenced'
EXECUTE sp_addextendedproperty N'MS_Description', @v, N'SCHEMA', N'dbo', N'TABLE', N'clients', N'CONSTRAINT', N'FK_clients_config_fees'
GO
COMMIT
GO
