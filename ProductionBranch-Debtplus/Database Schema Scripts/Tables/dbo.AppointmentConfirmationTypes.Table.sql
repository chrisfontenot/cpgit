USE [DebtPlus]
GO
/****** Object:  Table [dbo].[AppointmentConfirmationTypes]    Script Date: 09/15/2014 13:13:25 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[AppointmentConfirmationTypes](
	[oID] [dbo].[typ_key] IDENTITY(1,1) NOT NULL,
	[description] [dbo].[typ_description] NOT NULL,
	[nfcc] [varchar](256) NULL,
	[rpps] [varchar](256) NULL,
	[epay] [varchar](256) NULL,
	[note] [varchar](256) NULL,
	[hud_9902_section] [varchar](256) NULL,
	[default] [bit] NOT NULL,
	[ActiveFlag] [bit] NOT NULL,
	[date_created] [dbo].[typ_date] NOT NULL,
	[created_by] [dbo].[typ_counselor] NOT NULL,
 CONSTRAINT [PK_AppointmentConfirmationTypes] PRIMARY KEY CLUSTERED 
(
	[oID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
