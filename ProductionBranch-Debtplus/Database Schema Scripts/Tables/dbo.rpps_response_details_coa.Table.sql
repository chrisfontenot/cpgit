USE [DebtPlus]
GO
/****** Object:  Table [dbo].[rpps_response_details_coa]    Script Date: 09/15/2014 13:13:26 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[rpps_response_details_coa](
	[rpps_response_detail_coa] [dbo].[typ_key] IDENTITY(1,1) NOT NULL,
	[rpps_response_detail] [dbo].[typ_key] NOT NULL,
	[Name] [varchar](50) NULL,
	[ChangeAccountNumber] [bit] NOT NULL,
	[NewAccountNumber] [varchar](22) NULL,
	[ChangeBillerID] [bit] NOT NULL,
	[NewBillerID] [varchar](10) NULL,
 CONSTRAINT [PK_rpps_response_details_coa] PRIMARY KEY CLUSTERED 
(
	[rpps_response_detail_coa] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
