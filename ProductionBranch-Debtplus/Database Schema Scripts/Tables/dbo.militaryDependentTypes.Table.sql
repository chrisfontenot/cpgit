/* To prevent any potential data loss issues, you should review this script in detail before running it outside the context of the database designer.*/
BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION
GO
EXECUTE UPDATE_ADD_COLUMN 'militaryDependentTypes', 'hpf'
GO
CREATE TABLE dbo.Tmp_militaryDependentTypes
	(
	oID dbo.typ_key NOT NULL IDENTITY (1, 1),
	description dbo.typ_description NOT NULL,
	ActiveFlag bit NOT NULL,
	[Default] bit NOT NULL,
	hpf varchar(50) NULL,
	created_by dbo.typ_counselor NOT NULL,
	date_created dbo.typ_date NOT NULL,
	ts timestamp NOT NULL
	)  ON [PRIMARY]
GO
GRANT DELETE ON dbo.Tmp_militaryDependentTypes TO public  AS dbo
GO
GRANT INSERT ON dbo.Tmp_militaryDependentTypes TO public  AS dbo
GO
GRANT SELECT ON dbo.Tmp_militaryDependentTypes TO public  AS dbo
GO
GRANT UPDATE ON dbo.Tmp_militaryDependentTypes TO public  AS dbo
GO
EXECUTE sp_bindefault N'dbo.default_true', N'dbo.Tmp_militaryDependentTypes.ActiveFlag'
GO
EXECUTE sp_bindefault N'dbo.default_false', N'dbo.Tmp_militaryDependentTypes.[Default]'
GO
SET IDENTITY_INSERT dbo.Tmp_militaryDependentTypes ON
GO
IF EXISTS(SELECT * FROM dbo.militaryDependentTypes)
	 EXEC('INSERT INTO dbo.Tmp_militaryDependentTypes (oID, description, ActiveFlag, [Default], created_by, date_created, hpf)
		SELECT oID, description, ActiveFlag, [Default], created_by, date_created, hpf FROM dbo.militaryDependentTypes WITH (HOLDLOCK TABLOCKX)')
GO
SET IDENTITY_INSERT dbo.Tmp_militaryDependentTypes OFF
GO
DROP TABLE dbo.militaryDependentTypes
GO
EXECUTE sp_rename N'dbo.Tmp_militaryDependentTypes', N'militaryDependentTypes', 'OBJECT' 
GO
ALTER TABLE dbo.militaryDependentTypes ADD CONSTRAINT
	PK_military_DependentTypes PRIMARY KEY CLUSTERED 
	(
	oID
	) WITH( STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]

GO
DENY DELETE ON dbo.militaryDependentTypes TO www_role  AS dbo 
GO
DENY INSERT ON dbo.militaryDependentTypes TO www_role  AS dbo 
GO
DENY SELECT ON dbo.militaryDependentTypes TO www_role  AS dbo 
GO
DENY UPDATE ON dbo.militaryDependentTypes TO www_role  AS dbo 
GO
COMMIT
