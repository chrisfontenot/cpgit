USE [DebtPlus]
GO
/****** Object:  Table [dbo].[ClientAppointmentsHistory]    Script Date: 09/15/2014 13:13:26 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[ClientAppointmentsHistory](
	[oID] [dbo].[typ_key] IDENTITY(1,1) NOT NULL,
	[ClientAppointment] [uniqueidentifier] NULL,
	[RescheduledClientAppointment] [uniqueidentifier] NULL,
	[TranType] [varchar](2) NOT NULL,
	[Client] [dbo].[typ_client] NOT NULL,
	[isWorkshop] [bit] NOT NULL,
	[isWalkIn] [bit] NOT NULL,
	[StartDate] [datetime] NOT NULL,
	[Duration] [int] NOT NULL,
	[AppointmentType] [dbo].[typ_key] NULL,
	[Office] [dbo].[typ_key] NULL,
	[Counselor] [dbo].[typ_key] NULL,
	[Workshop] [dbo].[typ_key] NULL,
	[ConfirmStatus] [dbo].[typ_key] NULL,
	[ReferredBy] [dbo].[typ_key] NULL,
	[BankruptcyClass] [dbo].[typ_key] NULL,
	[Results] [varchar](10) NULL,
	[ReferredTo] [dbo].[typ_key] NULL,
	[isLocked] [bit] NOT NULL,
	[isPriority] [bit] NOT NULL,
	[HousingTransaction] [dbo].[typ_key] NULL,
	[DateCreated] [dbo].[typ_date] NOT NULL,
	[CreatedBy] [dbo].[typ_counselor] NOT NULL,
 CONSTRAINT [PK_ClientAppointmentsHistory] PRIMARY KEY CLUSTERED 
(
	[oID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
