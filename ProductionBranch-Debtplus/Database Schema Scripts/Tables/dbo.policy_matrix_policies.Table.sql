USE [DebtPlus]
GO
/****** Object:  Table [dbo].[policy_matrix_policies]    Script Date: 09/15/2014 13:13:26 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[policy_matrix_policies](
	[policy_matrix_policy] [dbo].[typ_key] IDENTITY(1,1) NOT NULL,
	[creditor] [dbo].[typ_creditor] NOT NULL,
	[policy_matrix_policy_type] [dbo].[typ_policy_matrix_policy_type] NOT NULL,
	[changed_date] [datetime] NOT NULL,
	[item_value] [varchar](1024) NULL,
	[message] [varchar](1024) NULL,
	[date_created] [dbo].[typ_date] NOT NULL,
	[created_by] [dbo].[typ_counselor] NOT NULL,
 CONSTRAINT [PK_policy_matrix_policies] PRIMARY KEY CLUSTERED 
(
	[policy_matrix_policy] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 90) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Primary key to the table' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'policy_matrix_policies', @level2type=N'COLUMN',@level2name=N'policy_matrix_policy'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Creditor ID' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'policy_matrix_policies', @level2type=N'COLUMN',@level2name=N'creditor'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Type of the policy matrix' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'policy_matrix_policies', @level2type=N'COLUMN',@level2name=N'policy_matrix_policy_type'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Date the item was changed' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'policy_matrix_policies', @level2type=N'COLUMN',@level2name=N'changed_date'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Value for the item' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'policy_matrix_policies', @level2type=N'COLUMN',@level2name=N'item_value'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Message associated with the value' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'policy_matrix_policies', @level2type=N'COLUMN',@level2name=N'message'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Date/Time when the row was created.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'policy_matrix_policies', @level2type=N'COLUMN',@level2name=N'date_created'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Person who created the row.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'policy_matrix_policies', @level2type=N'COLUMN',@level2name=N'created_by'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'This table holds the policy information for a policy matrix' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'policy_matrix_policies'
GO
