EXECUTE UPDATE_DROP_CONSTRAINTS 'product_states'
GO
/* To prevent any potential data loss issues, you should review this script in detail before running it outside the context of the database designer.*/
BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION
GO
ALTER TABLE dbo.products SET (LOCK_ESCALATION = TABLE)
GO
CREATE TABLE dbo.Tmp_product_states
	(
	oID dbo.typ_key NOT NULL IDENTITY (1, 1),
	productID dbo.typ_key NOT NULL,
	stateID dbo.typ_key NOT NULL,
	Amount money NOT NULL,
	date_created dbo.typ_date NOT NULL,
	created_by dbo.typ_counselor NOT NULL,
	ts timestamp NOT NULL
	)  ON [PRIMARY]
GO
ALTER TABLE dbo.Tmp_product_states SET (LOCK_ESCALATION = TABLE)
GO
IF EXISTS(SELECT * FROM SYSOBJECTS WHERE TYPE='U' AND NAME=N'product_states')
BEGIN
	EXEC('SET IDENTITY_INSERT dbo.Tmp_product_states ON;
	      INSERT INTO dbo.Tmp_product_states (oID, productID, stateID, Amount, date_created, created_by)
		  SELECT oID, productID, stateID, Amount, date_created, created_by FROM dbo.product_states WITH (HOLDLOCK TABLOCKX);
		  SET IDENTITY_INSERT dbo.Tmp_product_states OFF')
	EXEC('DROP TABLE dbo.product_states')
END
GO
EXECUTE sp_rename N'dbo.Tmp_product_states', N'product_states', 'OBJECT' 
GO
ALTER TABLE dbo.product_states ADD CONSTRAINT
	PK_product_states PRIMARY KEY CLUSTERED 
	(
	oID
	) WITH( STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
ALTER TABLE dbo.product_states ADD CONSTRAINT
	FK_product_states_products FOREIGN KEY
	(
	productID
	) REFERENCES dbo.products
	(
	oID
	) ON UPDATE  CASCADE 
	 ON DELETE  CASCADE 
GO
ALTER TABLE dbo.product_states ADD CONSTRAINT
	FK_product_states_states FOREIGN KEY
	(
	stateID
	) REFERENCES dbo.states
	(
	state
	) ON UPDATE  CASCADE 
	 ON DELETE  CASCADE 
GO
COMMIT
