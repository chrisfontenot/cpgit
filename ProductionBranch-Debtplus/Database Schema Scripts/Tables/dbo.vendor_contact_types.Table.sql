EXECUTE UPDATE_DROP_CONSTRAINTS 'vendor_contact_types'
GO
/* To prevent any potential data loss issues, you should review this script in detail before running it outside the context of the database designer.*/
BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION
GO
CREATE TABLE dbo.Tmp_vendor_contact_types
	(
	oID dbo.typ_key NOT NULL IDENTITY (1, 1),
	description dbo.typ_description NULL,
	ActiveFlag bit NOT NULL,
	[Default] bit NOT NULL,
	date_created dbo.typ_date NOT NULL,
	created_by dbo.typ_counselor NOT NULL,
	ts timestamp NOT NULL
	)  ON [PRIMARY]
GO
ALTER TABLE dbo.Tmp_vendor_contact_types SET (LOCK_ESCALATION = TABLE)
GO
EXECUTE sp_bindefault N'dbo.default_true', N'dbo.Tmp_vendor_contact_types.ActiveFlag'
GO
EXECUTE sp_bindefault N'dbo.default_false', N'dbo.Tmp_vendor_contact_types.[Default]'
GO
IF EXISTS(SELECT * FROM dbo.vendor_contact_types)
BEGIN
	EXEC  ('SET IDENTITY_INSERT dbo.Tmp_vendor_contact_types ON;
			INSERT INTO dbo.Tmp_vendor_contact_types (oID, description, ActiveFlag, [Default], date_created, created_by)
			SELECT oID, description, ActiveFlag, [Default], date_created, created_by FROM dbo.vendor_contact_types WITH (HOLDLOCK TABLOCKX);
			SET IDENTITY_INSERT dbo.Tmp_vendor_contact_types OFF;')
	EXEC ('DROP TABLE dbo.vendor_contact_types')
END
GO
EXECUTE sp_rename N'dbo.Tmp_vendor_contact_types', N'vendor_contact_types', 'OBJECT' 
GO
ALTER TABLE dbo.vendor_contact_types ADD CONSTRAINT
	PK_vendor_contact_types PRIMARY KEY CLUSTERED 
	(
	oID
	) WITH( STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
COMMIT
BEGIN TRANSACTION
GO
ALTER TABLE dbo.vendor_contacts ADD CONSTRAINT
	FK_vendor_contacts_vendor_contact_types FOREIGN KEY
	(
	contact_type
	) REFERENCES dbo.vendor_contact_types
	(
	oID
	) ON UPDATE  CASCADE 
	 ON DELETE  CASCADE 
GO
ALTER TABLE dbo.vendor_contacts SET (LOCK_ESCALATION = TABLE)
GO
COMMIT
