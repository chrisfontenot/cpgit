USE [DebtPlus]
GO
/****** Object:  Table [dbo].[workshop_types]    Script Date: 09/15/2014 13:13:26 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[workshop_types](
	[workshop_type] [dbo].[typ_key] IDENTITY(1,1) NOT NULL,
	[description] [dbo].[typ_description] NOT NULL,
	[duration] [int] NOT NULL,
	[HUD_Grant] [dbo].[typ_key] NULL,
	[HousingFeeAmount] [money] NOT NULL,
	[ActiveFlag] [bit] NOT NULL,
	[date_created] [dbo].[typ_date] NOT NULL,
	[created_by] [dbo].[typ_counselor] NOT NULL,
	[old_workshop_type] [int] NULL,
 CONSTRAINT [PK_workshop_types] PRIMARY KEY CLUSTERED 
(
	[workshop_type] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 90) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'This is the types of workshops that are known to the system.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'workshop_types'
GO
ALTER TABLE [dbo].[workshop_types] ADD  CONSTRAINT [DF_workshop_types_duration]  DEFAULT ((90)) FOR [duration]
GO
