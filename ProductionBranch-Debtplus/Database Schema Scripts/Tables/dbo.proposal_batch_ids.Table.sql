USE [DebtPlus]
GO
/****** Object:  Table [dbo].[proposal_batch_ids]    Script Date: 09/15/2014 13:13:26 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[proposal_batch_ids](
	[proposal_batch_id] [dbo].[typ_key] IDENTITY(1,1) NOT NULL,
	[proposal_type] [int] NOT NULL,
	[date_closed] [datetime] NULL,
	[date_transmitted] [datetime] NULL,
	[note] [dbo].[typ_description] NULL,
	[bank] [dbo].[typ_key] NOT NULL,
	[date_created] [datetime] NOT NULL,
	[created_by] [dbo].[typ_counselor] NOT NULL,
	[old_proposal_batch_id] [int] NULL,
 CONSTRAINT [PK_proposal_batch_ids] PRIMARY KEY CLUSTERED 
(
	[proposal_batch_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
