USE [DebtPlus]
GO
/****** Object:  Table [dbo].[Names]    Script Date: 09/15/2014 13:13:26 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Names](
	[Name] [int] IDENTITY(11,1) NOT FOR REPLICATION NOT NULL,
	[Prefix] [varchar](40) NULL,
	[First] [varchar](80) NULL,
	[Middle] [varchar](80) NULL,
	[Last] [varchar](400) NULL,
	[Suffix] [varchar](40) NULL,
	[old_Name] [int] NULL,
 CONSTRAINT [PK_Names] PRIMARY KEY CLUSTERED 
(
	[Name] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
