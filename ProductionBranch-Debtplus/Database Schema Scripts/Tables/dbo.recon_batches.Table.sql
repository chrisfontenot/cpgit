USE [DebtPlus]
GO
/****** Object:  Table [dbo].[recon_batches]    Script Date: 09/15/2014 13:13:26 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[recon_batches](
	[recon_batch] [dbo].[typ_key] IDENTITY(1,1) NOT NULL,
	[created_by] [dbo].[typ_counselor] NOT NULL,
	[date_created] [dbo].[typ_date] NOT NULL,
	[date_posted] [datetime] NULL,
	[note] [dbo].[typ_description] NULL,
 CONSTRAINT [PK_recon_batches] PRIMARY KEY CLUSTERED 
(
	[recon_batch] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 90) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Reconcilation batch ID. This is the primary key to the table.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'recon_batches', @level2type=N'COLUMN',@level2name=N'recon_batch'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Person who created the row.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'recon_batches', @level2type=N'COLUMN',@level2name=N'created_by'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Date/Time when the row was created.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'recon_batches', @level2type=N'COLUMN',@level2name=N'date_created'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Date the reconcile batch was posted. A posted batch has a date. Open batches do not.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'recon_batches', @level2type=N'COLUMN',@level2name=N'date_posted'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Optional message associated with the batch. This is not the batch ID, but is a suitable message used to help the human find the batch.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'recon_batches', @level2type=N'COLUMN',@level2name=N'note'
GO
