EXECUTE UPDATE_drop_constraints 'appt_times'
go
/* To prevent any potential data loss issues, you should review this script in detail before running it outside the context of the database designer.*/
BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION
GO
CREATE TABLE dbo.Tmp_appt_times
	(
	appt_time dbo.typ_key NOT NULL IDENTITY (1, 1) NOT FOR REPLICATION,
	start_time datetime NOT NULL,
	office dbo.typ_key NOT NULL,
	appt_type dbo.typ_key NULL,
	date_created dbo.typ_date NOT NULL,
	created_by dbo.typ_counselor NOT NULL,
	ts timestamp NOT NULL
	)  ON [PRIMARY]
GO
GRANT DELETE ON dbo.Tmp_appt_times TO public  AS dbo
GO
GRANT INSERT ON dbo.Tmp_appt_times TO public  AS dbo
GO
GRANT SELECT ON dbo.Tmp_appt_times TO public  AS dbo
GO
GRANT UPDATE ON dbo.Tmp_appt_times TO public  AS dbo
GO
DECLARE @v sql_variant 
SET @v = N'This table lists the possible booked appointments in the week. The appointments are booked against this table.'
EXECUTE sp_addextendedproperty N'MS_Description', @v, N'SCHEMA', N'dbo', N'TABLE', N'Tmp_appt_times', NULL, NULL
GO
DECLARE @v sql_variant 
SET @v = N'Appointment time slot record ID. This is the primary key to the table.'
EXECUTE sp_addextendedproperty N'MS_Description', @v, N'SCHEMA', N'dbo', N'TABLE', N'Tmp_appt_times', N'COLUMN', N'appt_time'
GO
DECLARE @v sql_variant 
SET @v = N'Starting time for the appointment. This is the date/time of the appointment time slot.'
EXECUTE sp_addextendedproperty N'MS_Description', @v, N'SCHEMA', N'dbo', N'TABLE', N'Tmp_appt_times', N'COLUMN', N'start_time'
GO
DECLARE @v sql_variant 
SET @v = N'Pointer to the office associated with the appointment time.'
EXECUTE sp_addextendedproperty N'MS_Description', @v, N'SCHEMA', N'dbo', N'TABLE', N'Tmp_appt_times', N'COLUMN', N'office'
GO
DECLARE @v sql_variant 
SET @v = N'Pointer to the type of the appointment. NULL indicates that "any type" is allowed for the appointment slot.'
EXECUTE sp_addextendedproperty N'MS_Description', @v, N'SCHEMA', N'dbo', N'TABLE', N'Tmp_appt_times', N'COLUMN', N'appt_type'
GO
DECLARE @v sql_variant 
SET @v = N'Date/Time when the row was created.'
EXECUTE sp_addextendedproperty N'MS_Description', @v, N'SCHEMA', N'dbo', N'TABLE', N'Tmp_appt_times', N'COLUMN', N'date_created'
GO
DECLARE @v sql_variant 
SET @v = N'Person who created the row.'
EXECUTE sp_addextendedproperty N'MS_Description', @v, N'SCHEMA', N'dbo', N'TABLE', N'Tmp_appt_times', N'COLUMN', N'created_by'
GO
SET IDENTITY_INSERT dbo.Tmp_appt_times ON
GO
IF EXISTS(SELECT * FROM dbo.appt_times)
	 EXEC('INSERT INTO dbo.Tmp_appt_times (appt_time, start_time, office, appt_type, date_created, created_by)
		SELECT appt_time, start_time, office, appt_type, date_created, created_by FROM dbo.appt_times WITH (HOLDLOCK TABLOCKX)')
GO
SET IDENTITY_INSERT dbo.Tmp_appt_times OFF
GO
DROP TABLE dbo.appt_times
GO
EXECUTE sp_rename N'dbo.Tmp_appt_times', N'appt_times', 'OBJECT' 
GO
-- Discard duplicate entries for the appt times table
select office, start_time, max(appt_time) as appt_time into #appt_times from appt_times group by office, start_time having count(*) > 1
delete appt_times from appt_times a inner join #appt_times x on x.office = a.office and x.start_time = a.start_time where x.appt_time <> a.appt_time
drop table #appt_times
GO
-- Discard counselor references where they are not present
delete appt_counselors from appt_counselors co left outer join appt_times t on t.appt_time = co.appt_time where t.appt_time is null
GO
ALTER TABLE dbo.appt_times ADD CONSTRAINT
	PK_appt_times PRIMARY KEY CLUSTERED 
	(
	appt_time
	) WITH( PAD_INDEX = OFF, FILLFACTOR = 90, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX ix_appt_times_1 ON dbo.appt_times
	(
	start_time
	) INCLUDE (appt_time, office, appt_type) 
 WITH( STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
CREATE UNIQUE NONCLUSTERED INDEX IX_appt_times_2 ON dbo.appt_times
	(
	start_time,
	office
	) WITH( STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
CREATE TRIGGER [dbo].[trig_appt_time_D] on dbo.appt_times AFTER DELETE as
begin
	-- ================================================================
	-- ==     Process the deletion of an appointment time            ==
	-- ================================================================

	set nocount on

	-- Remove the counselor references to the deleted items	
	delete	appt_counselors
	from	appt_counselors i
	inner join deleted d on i.appt_time = d.appt_time
	
	-- Cancel the client appointments that have been booked for this time
	select	ca.client, ca.client_appointment, ca.start_time
	into	#trig_appt_time_d
	from	client_appointments ca with (nolock)
	inner join deleted d on ca.appt_time = d.appt_time
	where	ca.status = 'P'
	and		ca.office is not null;
	
	-- Create a system note that we are cancelling the appointment
	insert into client_notes (client, subject, note, [type], dont_print, dont_edit, dont_delete)
	select	client, 'Cancelled appointment', 'The appointment at ' + CONVERT(varchar, start_time) + ' was cancelled since the time slot was deleted.', 3, 0, 0, 0
	from	#trig_appt_time_d
	
	-- Cancel the appointments and remove the references
	update	client_appointments
	set		status = 'C',
			[date_updated] = GETDATE(),
			[appt_time] = null
	from	client_appointments ca
	inner join #trig_appt_time_d d on ca.client_appointment = d.client_appointment
	
	drop table #trig_appt_time_d
end
GO
DENY DELETE ON dbo.appt_times TO www_role  AS dbo 
GO
DENY INSERT ON dbo.appt_times TO www_role  AS dbo 
GO
DENY SELECT ON dbo.appt_times TO www_role  AS dbo 
GO
DENY UPDATE ON dbo.appt_times TO www_role  AS dbo 
GO
COMMIT
