USE [DebtPlus]
GO
/****** Object:  Table [dbo].[Housing_loan_details]    Script Date: 09/15/2014 13:13:26 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Housing_loan_details](
	[oID] [dbo].[typ_key] IDENTITY(1,1) NOT NULL,
	[LoanTypeCD] [dbo].[typ_key] NULL,
	[FinanceTypeCD] [dbo].[typ_key] NULL,
	[MortgageTypeCD] [dbo].[typ_key] NULL,
	[InterestRate] [float] NULL,
	[Hybrid_ARM_Loan] [bit] NOT NULL,
	[Option_ARM_Loan] [bit] NOT NULL,
	[Interest_Only_Loan] [bit] NOT NULL,
	[FHA_VA_Insured_Loan] [bit] NOT NULL,
	[ARM_Reset] [bit] NOT NULL,
	[Privately_Held_Loan] [bit] NOT NULL,
	[Payment] [money] NOT NULL,
	[old_oid] [int] NULL,
 CONSTRAINT [PK_housing_loan_details] PRIMARY KEY CLUSTERED 
(
	[oID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
