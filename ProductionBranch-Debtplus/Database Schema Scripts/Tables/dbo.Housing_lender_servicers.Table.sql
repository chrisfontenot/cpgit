EXECUTE UPDATE_DROP_CONSTRAINTS 'Housing_lender_servicers'
GO
EXECUTE UPDATE_ADD_COLUMN 'Housing_lender_servicers', 'hlp_servicer', 'bit'
GO
/* To prevent any potential data loss issues, you should review this script in detail before running it outside the context of the database designer.*/
BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION
GO
CREATE TABLE dbo.Tmp_Housing_lender_servicers
	(
	oID dbo.typ_key NOT NULL IDENTITY (1, 1),
	ServiceID int NULL,
	RxOffice int NULL,
	description dbo.typ_description NOT NULL,
	ActiveFlag bit NOT NULL,
	[default] bit NOT NULL,
	IsOther bit NOT NULL,
	hlp_servicer bit NOT NULL,
	Comment varchar(80) NULL,
	ts timestamp NOT NULL
	)  ON [PRIMARY]
GO
ALTER TABLE dbo.Tmp_Housing_lender_servicers SET (LOCK_ESCALATION = TABLE)
GO
GRANT DELETE ON dbo.Tmp_Housing_lender_servicers TO public  AS dbo
GO
GRANT INSERT ON dbo.Tmp_Housing_lender_servicers TO public  AS dbo
GO
GRANT SELECT ON dbo.Tmp_Housing_lender_servicers TO public  AS dbo
GO
GRANT UPDATE ON dbo.Tmp_Housing_lender_servicers TO public  AS dbo
GO
EXECUTE sp_bindefault N'dbo.default_true', N'dbo.Tmp_Housing_lender_servicers.ActiveFlag'
GO
EXECUTE sp_bindefault N'dbo.default_false', N'dbo.Tmp_Housing_lender_servicers.[default]'
GO
EXECUTE sp_bindefault N'dbo.default_false', N'dbo.Tmp_Housing_lender_servicers.IsOther'
GO
EXECUTE sp_bindefault N'dbo.default_false', N'dbo.Tmp_Housing_lender_servicers.hlp_servicer'
GO
SET IDENTITY_INSERT dbo.Tmp_Housing_lender_servicers ON
GO
IF EXISTS(SELECT * FROM dbo.Housing_lender_servicers)
	 EXEC('INSERT INTO dbo.Tmp_Housing_lender_servicers (oID, ServiceID, RxOffice, description, ActiveFlag, [default], IsOther, Comment, hlp_servicer)
		SELECT oID, ServiceID, RxOffice, description, ActiveFlag, [default], IsOther, Comment, ISNULL(hlp_servicer,0) FROM dbo.Housing_lender_servicers WITH (HOLDLOCK TABLOCKX)')
GO
SET IDENTITY_INSERT dbo.Tmp_Housing_lender_servicers OFF
GO
DROP TABLE dbo.Housing_lender_servicers
GO
EXECUTE sp_rename N'dbo.Tmp_Housing_lender_servicers', N'Housing_lender_servicers', 'OBJECT' 
GO
ALTER TABLE dbo.Housing_lender_servicers ADD CONSTRAINT
	PK_housing_lender_servicers PRIMARY KEY CLUSTERED 
	(
	oID
	) WITH( PAD_INDEX = OFF, FILLFACTOR = 90, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
DENY DELETE ON dbo.Housing_lender_servicers TO www_role  AS dbo 
GO
DENY INSERT ON dbo.Housing_lender_servicers TO www_role  AS dbo 
GO
DENY SELECT ON dbo.Housing_lender_servicers TO www_role  AS dbo 
GO
DENY UPDATE ON dbo.Housing_lender_servicers TO www_role  AS dbo 
GO
COMMIT
BEGIN TRANSACTION
GO
ALTER TABLE dbo.Housing_lenders ADD CONSTRAINT
	FK_Housing_lenders_Housing_lender_servicers FOREIGN KEY
	(
	ServicerID
	) REFERENCES dbo.Housing_lender_servicers
	(
	oID
	) ON UPDATE  NO ACTION 
	 ON DELETE  NO ACTION 
GO
ALTER TABLE dbo.Housing_lenders SET (LOCK_ESCALATION = TABLE)
GO
COMMIT
GO
