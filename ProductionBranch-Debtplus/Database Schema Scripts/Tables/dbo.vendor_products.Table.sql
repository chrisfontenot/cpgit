EXEC UPDATE_DROP_CONSTRAINTS 'vendor_products'
GO
/* To prevent any potential data loss issues, you should review this script in detail before running it outside the context of the database designer.*/
BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION
GO
ALTER TABLE dbo.vendors SET (LOCK_ESCALATION = TABLE)
GO
CREATE TABLE dbo.Tmp_vendor_products
	(
	oID dbo.typ_key NOT NULL IDENTITY (1, 1),
	vendor dbo.typ_key NOT NULL,
	product dbo.typ_key NOT NULL,
	escrow bit NOT NULL,
	effective_date datetime NOT NULL,
	date_created dbo.typ_date NOT NULL,
	created_by dbo.typ_counselor NOT NULL,
	ts timestamp NOT NULL
	)  ON [PRIMARY]
GO
ALTER TABLE dbo.Tmp_vendor_products SET (LOCK_ESCALATION = TABLE)
GO
EXECUTE sp_bindefault N'dbo.default_false', N'dbo.Tmp_vendor_products.escrow'
GO
EXECUTE sp_bindefault N'dbo.default_date', N'dbo.Tmp_vendor_products.effective_date'
GO
IF EXISTS(SELECT * FROM SYSOBJECTS WHERE TYPE='U' AND NAME=N'vendor_products')
BEGIN
	 EXEC('SET IDENTITY_INSERT dbo.Tmp_vendor_products ON;
		   INSERT INTO dbo.Tmp_vendor_products (oID, vendor, product, escrow, date_created, created_by, effective_date)
		   SELECT oID, vendor, product, ISNULL(escrow,0), date_created, created_by, ISNULL(effective_date,getdate()) FROM dbo.vendor_products WITH (HOLDLOCK TABLOCKX);
		   SET IDENTITY_INSERT dbo.Tmp_vendor_products OFF;')
	EXEC('DROP TABLE dbo.vendor_products')
END
GO
EXECUTE sp_rename N'dbo.Tmp_vendor_products', N'vendor_products', 'OBJECT' 
GO
ALTER TABLE dbo.vendor_products ADD CONSTRAINT
	PK_vendor_products PRIMARY KEY CLUSTERED 
	(
	oID
	) WITH( STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
ALTER TABLE dbo.vendor_products ADD CONSTRAINT
	FK_vendor_products_vendors FOREIGN KEY
	(
	vendor
	) REFERENCES dbo.vendors
	(
	oID
	) ON UPDATE  CASCADE 
	 ON DELETE  CASCADE 
GO
ALTER TABLE dbo.vendor_products ADD CONSTRAINT
	FK_vendor_products_products FOREIGN KEY
	(
	product
	) REFERENCES dbo.products
	(
	oID
	) ON UPDATE  CASCADE 
	 ON DELETE  CASCADE 
GO
COMMIT
