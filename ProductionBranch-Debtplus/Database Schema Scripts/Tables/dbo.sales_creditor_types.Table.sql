USE [DebtPlus]
GO
/****** Object:  Table [dbo].[sales_creditor_types]    Script Date: 09/15/2014 13:13:26 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[sales_creditor_types](
	[sales_creditor_type] [dbo].[typ_NameField] NOT NULL,
	[description] [dbo].[typ_NameField] NOT NULL,
	[DefaultPercentPayment] [float] NOT NULL,
	[DefaultAmount] [money] NULL,
	[DefaultPercentDisb] [float] NULL,
	[created_by] [dbo].[typ_counselor] NOT NULL,
	[date_created] [dbo].[typ_date] NOT NULL,
 CONSTRAINT [PK_sales_creditor_types] PRIMARY KEY CLUSTERED 
(
	[sales_creditor_type] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 90) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
