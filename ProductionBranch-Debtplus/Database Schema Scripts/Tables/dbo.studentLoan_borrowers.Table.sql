USE [DebtPlus]
GO
/****** Object:  Table [dbo].[studentLoan_borrowers]    Script Date: 09/15/2014 13:13:26 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[studentLoan_borrowers](
	[borrower_id] [dbo].[typ_key] IDENTITY(1001,1) NOT NULL,
	[description] [dbo].[typ_description] NOT NULL,
	[date_created] [dbo].[typ_date] NOT NULL,
 CONSTRAINT [PK_studentLoan_borrowers] PRIMARY KEY CLUSTERED 
(
	[borrower_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 90) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
