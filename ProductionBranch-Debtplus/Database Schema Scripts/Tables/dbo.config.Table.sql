USE [DebtPlus]
GO
/****** Object:  Table [dbo].[config]    Script Date: 09/15/2014 13:13:26 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[config](
	[company_id] [dbo].[typ_key] NOT NULL,
	[name] [varchar](255) NULL,
	[TelephoneID] [dbo].[typ_key] NULL,
	[AltTelephoneID] [dbo].[typ_key] NULL,
	[AddressID] [dbo].[typ_key] NULL,
	[AltAddressID] [dbo].[typ_key] NULL,
	[FAXID] [dbo].[typ_key] NULL,
	[areacode] [varchar](10) NULL,
	[debtplus_installed_date] [datetime] NOT NULL,
	[debtplus_database_date] [datetime] NOT NULL,
	[nfcc_unit_no] [int] NULL,
	[use_sched_pay] [bit] NOT NULL,
	[payment_minimum] [money] NULL,
	[acceptance_days] [int] NOT NULL,
	[fed_tax_id] [varchar](50) NULL,
	[dun_nbr] [varchar](30) NOT NULL,
	[default_till_missed] [int] NOT NULL,
	[chks_per_invoice] [int] NULL,
	[bal_verify_months] [float] NOT NULL,
	[rpps_resend_msg] [bit] NOT NULL,
	[payments_pad_less] [money] NOT NULL,
	[payments_pad_greater] [money] NOT NULL,
	[threshold] [float] NOT NULL,
	[paf_creditor] [dbo].[typ_creditor] NULL,
	[setup_creditor] [dbo].[typ_creditor] NULL,
	[deduct_creditor] [dbo].[typ_creditor] NULL,
	[counseling_creditor] [dbo].[typ_creditor] NULL,
	[myriad_client_statement_id] [varchar](4) NULL,
	[client_statement_agency] [varchar](10) NULL,
	[web_site] [varchar](50) NOT NULL,
	[ComparisonRate] [float] NOT NULL,
	[ComparisonPercent] [float] NOT NULL,
	[ComparisonAmount] [money] NOT NULL,
	[date_updated] [dbo].[typ_date] NULL,
	[updated_by] [dbo].[typ_counselor] NULL,
	[date_created] [dbo].[typ_date] NOT NULL,
	[created_by] [dbo].[typ_counselor] NOT NULL,
 CONSTRAINT [PK_config] PRIMARY KEY NONCLUSTERED 
(
	[company_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 90) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Primary key to the table' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'config', @level2type=N'COLUMN',@level2name=N'company_id'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Agency Name' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'config', @level2type=N'COLUMN',@level2name=N'name'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Default area code for the agency' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'config', @level2type=N'COLUMN',@level2name=N'areacode'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'NFCC Unit number' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'config', @level2type=N'COLUMN',@level2name=N'nfcc_unit_no'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'In automatic disbursements, should the sched_pay column be used or should the disbursements be based solely on the disbursement_factor?' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'config', @level2type=N'COLUMN',@level2name=N'use_sched_pay'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Minimum amount of payment to each creditor' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'config', @level2type=N'COLUMN',@level2name=N'payment_minimum'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Number of days that a proposal is expected to be outstanding before it is accepted by default' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'config', @level2type=N'COLUMN',@level2name=N'acceptance_days'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Federal Tax ID number for the agency' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'config', @level2type=N'COLUMN',@level2name=N'fed_tax_id'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Number of days that an appointment for an office is assumed to be pending before it is marked "missed"' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'config', @level2type=N'COLUMN',@level2name=N'default_till_missed'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Default number of checks per invoice for each creditor.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'config', @level2type=N'COLUMN',@level2name=N'chks_per_invoice'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Number of months before sending a balance verification request' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'config', @level2type=N'COLUMN',@level2name=N'bal_verify_months'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Should messages be sent to RPPS' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'config', @level2type=N'COLUMN',@level2name=N'rpps_resend_msg'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'money by which the payment may be less and still assumed to be "equal" in disbursements' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'config', @level2type=N'COLUMN',@level2name=N'payments_pad_less'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'money by which the payment may be greater and still assumed to be "equal" in disbursements' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'config', @level2type=N'COLUMN',@level2name=N'payments_pad_greater'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Threshold value for new debts' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'config', @level2type=N'COLUMN',@level2name=N'threshold'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Program Administration Fee creditor' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'config', @level2type=N'COLUMN',@level2name=N'paf_creditor'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Setup fee creditor' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'config', @level2type=N'COLUMN',@level2name=N'setup_creditor'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Deduct creditor to receive deduct amounts' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'config', @level2type=N'COLUMN',@level2name=N'deduct_creditor'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Counseling fee creditor' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'config', @level2type=N'COLUMN',@level2name=N'counseling_creditor'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'For myriad client statements, this is the ID string for the agency' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'config', @level2type=N'COLUMN',@level2name=N'myriad_client_statement_id'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Date/Time when the row was updated.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'config', @level2type=N'COLUMN',@level2name=N'date_updated'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Person who updated the row.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'config', @level2type=N'COLUMN',@level2name=N'updated_by'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Date/Time when the row was created.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'config', @level2type=N'COLUMN',@level2name=N'date_created'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Person who created the row.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'config', @level2type=N'COLUMN',@level2name=N'created_by'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'This is the system configuration file. It holds system-wide configuration information such as the agency name and address.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'config'
GO
ALTER TABLE [dbo].[config] ADD  CONSTRAINT [DF_config_acceptance_days]  DEFAULT ((90)) FOR [acceptance_days]
GO
ALTER TABLE [dbo].[config] ADD  CONSTRAINT [DF_config_threshold]  DEFAULT ((0.031)) FOR [threshold]
GO
ALTER TABLE [dbo].[config] ADD  CONSTRAINT [DF_config_paf_creditor]  DEFAULT ('X0001') FOR [paf_creditor]
GO
ALTER TABLE [dbo].[config] ADD  CONSTRAINT [DF_config_deduct_creditor]  DEFAULT ('Z9999') FOR [deduct_creditor]
GO
