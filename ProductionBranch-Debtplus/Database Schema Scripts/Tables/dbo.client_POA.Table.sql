/*
   Wednesday, November 04, 20153:37:09 PM
   User: sa
   Server: 10.0.199.12
   Database: DebtPlus_HPF
   Application: 
*/

/* To prevent any potential data loss issues, you should review this script in detail before running it outside the context of the database designer.*/
BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION
GO
CREATE TABLE dbo.Tmp_client_POA
	(
	ID dbo.typ_key NOT NULL IDENTITY (1, 1),
	Client dbo.typ_client NOT NULL,
	POAType int NULL,
	POA_ID varchar(50) NULL,
	POA_ID_Issued datetime NULL,
	POA_ID_Expires datetime NULL,
	ApprovalBy varchar(50) NULL,
	RealEstate int NULL,
	Financial int NULL,
	Other int NULL,
	Stage int NULL,
	CreatedDate dbo.typ_date NOT NULL,
	NameID int NULL,
	TelephoneID int NULL,
	TelephoneType int NULL,
	POA_Expires datetime NULL,
	relation dbo.typ_key NULL,
	ts timestamp NOT NULL
	)  ON [PRIMARY]
GO
GRANT DELETE ON dbo.Tmp_client_POA TO public  AS dbo
GO
GRANT INSERT ON dbo.Tmp_client_POA TO public  AS dbo
GO
GRANT SELECT ON dbo.Tmp_client_POA TO public  AS dbo
GO
GRANT UPDATE ON dbo.Tmp_client_POA TO public  AS dbo
GO
SET IDENTITY_INSERT dbo.Tmp_client_POA ON
GO
IF EXISTS(SELECT * FROM dbo.client_POA)
	 EXEC('INSERT INTO dbo.Tmp_client_POA (ID, Client, POAType, POA_ID, POA_ID_Issued, POA_ID_Expires, ApprovalBy, RealEstate, Financial, Other, Stage, CreatedDate, NameID, TelephoneID, TelephoneType, POA_Expires, relation)
		SELECT ID, Client, POAType, POA_ID, POA_ID_Issued, POA_ID_Expires, ApprovalBy, RealEstate, Financial, Other, Stage, CreatedDate, NameID, TelephoneID, TelephoneType, POA_Expires, relation FROM dbo.client_POA WITH (HOLDLOCK TABLOCKX)')
GO
SET IDENTITY_INSERT dbo.Tmp_client_POA OFF
GO
DROP TABLE dbo.client_POA
GO
EXECUTE sp_rename N'dbo.Tmp_client_POA', N'client_POA', 'OBJECT' 
GO
ALTER TABLE dbo.client_POA ADD CONSTRAINT
	PK_client_POA PRIMARY KEY CLUSTERED 
	(
	ID
	) WITH( STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
DENY DELETE ON dbo.client_POA TO www_role  AS dbo 
GO
DENY INSERT ON dbo.client_POA TO www_role  AS dbo 
GO
DENY SELECT ON dbo.client_POA TO www_role  AS dbo 
GO
DENY UPDATE ON dbo.client_POA TO www_role  AS dbo 
GO
COMMIT
