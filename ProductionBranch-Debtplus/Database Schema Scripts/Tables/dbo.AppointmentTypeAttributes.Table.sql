USE [DebtPlus]
GO
/****** Object:  Table [dbo].[AppointmentTypeAttributes]    Script Date: 09/15/2014 13:13:25 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[AppointmentTypeAttributes](
	[oID] [dbo].[typ_key] IDENTITY(1,1) NOT NULL,
	[AppointmentType] [dbo].[typ_key] NOT NULL,
	[Attribute] [dbo].[typ_key] NOT NULL,
 CONSTRAINT [PK_AppointmentTypeAttributes] PRIMARY KEY CLUSTERED 
(
	[oID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
