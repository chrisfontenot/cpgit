USE [DebtPlus]
GO
/****** Object:  Table [dbo].[AppointmentReservedTimes]    Script Date: 09/15/2014 13:13:25 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[AppointmentReservedTimes](
	[oID] [int] IDENTITY(1,1) NOT NULL,
	[Office] [dbo].[typ_key] NULL,
	[StartDate] [datetime] NULL,
	[EndDate] [datetime] NULL,
	[StartTime] [int] NULL,
	[Duration] [int] NULL,
	[OnSunday] [bit] NOT NULL,
	[OnMonday] [bit] NOT NULL,
	[OnTuesday] [bit] NOT NULL,
	[OnWednesday] [bit] NOT NULL,
	[onThursday] [bit] NOT NULL,
	[OnFriday] [bit] NOT NULL,
	[OnSaturday] [bit] NOT NULL,
	[Label] [varchar](80) NULL,
	[Color] [int] NULL,
 CONSTRAINT [PK_AppointmentReservedTimes] PRIMARY KEY CLUSTERED 
(
	[oID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
