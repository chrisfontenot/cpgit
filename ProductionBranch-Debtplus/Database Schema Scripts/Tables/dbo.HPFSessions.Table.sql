/* To prevent any potential data loss issues, you should review this script in detail before running it outside the context of the database designer.*/
BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION
GO
EXECUTE UPDATE_DROP_CONSTRAINTS 'HPFSessions'
GO
COMMIT
BEGIN TRANSACTION
GO
CREATE TABLE dbo.Tmp_HPFSessions
	(
	oID dbo.typ_key NOT NULL IDENTITY (1, 1),
	event dbo.typ_key NOT NULL,
	sessionID dbo.typ_key NULL,
	budgetSetID dbo.typ_key NULL,
	programStageID dbo.typ_key NULL,
	CompletionEvent dbo.typ_key NULL,
	debtLoadCalculation money NULL,
	householdGrossAnnualIncome money NULL,
	householdNetAnnualIncome money NULL,
	fcId int NULL,
	date_changed dbo.typ_date NOT NULL,
	changed_by dbo.typ_counselor NOT NULL,
	date_created dbo.typ_date NOT NULL,
	created_by dbo.typ_counselor NOT NULL,
	ts timestamp NOT NULL
	)  ON [PRIMARY]
GO
GRANT DELETE ON dbo.Tmp_HPFSessions TO public  AS dbo
GO
GRANT INSERT ON dbo.Tmp_HPFSessions TO public  AS dbo
GO
GRANT SELECT ON dbo.Tmp_HPFSessions TO public  AS dbo
GO
GRANT UPDATE ON dbo.Tmp_HPFSessions TO public  AS dbo
GO
SET IDENTITY_INSERT dbo.Tmp_HPFSessions ON
GO
IF EXISTS(SELECT * FROM sysobjects WHERE name=N'HPFSessions' AND type='U')
BEGIN
	EXEC('INSERT INTO dbo.Tmp_HPFSessions (oID, event, sessionID, budgetSetID, programStageID, CompletionEvent, debtLoadCalculation, householdGrossAnnualIncome, householdNetAnnualIncome, fcId, date_changed, changed_by, date_created, created_by)
		SELECT oID, event, sessionID, budgetSetID, programStageID, CompletionEvent, debtLoadCalculation, householdGrossAnnualIncome, householdNetAnnualIncome, fcId, date_changed, changed_by, date_created, created_by FROM dbo.HPFSessions WITH (HOLDLOCK TABLOCKX)')
	EXEC('DROP TABLE dbo.HPFSessions')
END
GO
SET IDENTITY_INSERT dbo.Tmp_HPFSessions OFF
GO
EXECUTE sp_rename N'dbo.Tmp_HPFSessions', N'HPFSessions', 'OBJECT' 
GO
ALTER TABLE dbo.HPFSessions ADD CONSTRAINT PK_HPFSessions PRIMARY KEY CLUSTERED (oID) WITH( STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
CREATE trigger [dbo].[HPFSessions_I] on dbo.HPFSessions AFTER INSERT AS
BEGIN
	-- ======================================================================================
	-- ==             Record the ID and Timestamp of the last update to the rows           ==
	-- ======================================================================================

	-- Suppress intermediate results
	set nocount on

	-- Set the ID and date when the row was last updated
	update	HPFSessions
	set		date_changed	= getdate(),
			changed_by		= suser_sname()
	from	HPFSessions p
	inner join inserted i on p.oID = i.oID;
END
GO
CREATE trigger [dbo].[HPFSessions_U] on dbo.HPFSessions AFTER UPDATE AS
BEGIN
	-- ======================================================================================
	-- ==             Record the ID and Timestamp of the last update to the rows           ==
	-- ======================================================================================

	-- Suppress intermediate results
	set nocount on

	-- Set the ID and date when the row was last updated unless the update includes that information
	if not update(date_changed) AND not update(changed_by)
		update	HPFSessions
		set		date_changed	= getdate(),
				changed_by		= suser_sname()
		from	HPFSessions p
		inner join inserted i on p.oID = i.oID;
END
GO
DENY DELETE ON dbo.HPFSessions TO www_role  AS dbo 
GO
DENY INSERT ON dbo.HPFSessions TO www_role  AS dbo 
GO
DENY SELECT ON dbo.HPFSessions TO www_role  AS dbo 
GO
DENY UPDATE ON dbo.HPFSessions TO www_role  AS dbo 
GO
COMMIT
