EXECUTE UPDATE_DROP_CONSTRAINTS 'deposit_batch_details'
GO
EXECUTE UPDATE_ADD_COLUMN 'deposit_batch_details', 'client_product', 'int'
GO
DELETE FROM deposit_batch_details where deposit_batch_id NOT IN (select deposit_batch_id from deposit_batch_ids)
GO
/* To prevent any potential data loss issues, you should review this script in detail before running it outside the context of the database designer.*/
BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION
GO
CREATE TABLE dbo.Tmp_deposit_batch_details
	(
	deposit_batch_detail dbo.typ_key NOT NULL IDENTITY (1, 1),
	deposit_batch_id dbo.typ_key NOT NULL,
	tran_subtype dbo.typ_transaction NULL,
	ok_to_post bit NOT NULL,
	scanned_client varchar(50) NULL,
	client dbo.typ_client NULL,
	creditor dbo.typ_creditor NULL,
	client_creditor dbo.typ_key NULL,
	client_product dbo.typ_key NULL,
	amount money NOT NULL,
	fairshare_amt money NULL,
	fairshare_pct dbo.typ_fairshare_rate NULL,
	creditor_type dbo.typ_creditor_type NULL,
	item_date datetime NOT NULL,
	reference dbo.typ_description NULL,
	message dbo.typ_description NULL,
	ach_transaction_code dbo.typ_rps_transaction NULL,
	ach_routing_number dbo.typ_ach_routing NULL,
	ach_account_number dbo.typ_ach_account NULL,
	ach_authentication_code dbo.typ_rps_result NULL,
	ach_response_batch_id dbo.typ_description NULL,
	date_posted datetime NULL,
	created_by dbo.typ_counselor NOT NULL,
	date_created datetime NOT NULL,
	ts timestamp NOT NULL
	)  ON [PRIMARY]
GO
ALTER TABLE dbo.Tmp_deposit_batch_details SET (LOCK_ESCALATION = TABLE)
GO
GRANT DELETE ON dbo.Tmp_deposit_batch_details TO public  AS dbo
GO
GRANT INSERT ON dbo.Tmp_deposit_batch_details TO public  AS dbo
GO
GRANT SELECT ON dbo.Tmp_deposit_batch_details TO public  AS dbo
GO
GRANT UPDATE ON dbo.Tmp_deposit_batch_details TO public  AS dbo
GO
DECLARE @v sql_variant 
SET @v = N'This table holds the deposit information until the deposit batch is posted.'
EXECUTE sp_addextendedproperty N'MS_Description', @v, N'SCHEMA', N'dbo', N'TABLE', N'Tmp_deposit_batch_details', NULL, NULL
GO
DECLARE @v sql_variant 
SET @v = N'Primary key to the table. This is simply an identity counter.'
EXECUTE sp_addextendedproperty N'MS_Description', @v, N'SCHEMA', N'dbo', N'TABLE', N'Tmp_deposit_batch_details', N'COLUMN', N'deposit_batch_detail'
GO
DECLARE @v sql_variant 
SET @v = N'Pointer to the deposit batch ID from the deposit_batch_ids table.'
EXECUTE sp_addextendedproperty N'MS_Description', @v, N'SCHEMA', N'dbo', N'TABLE', N'Tmp_deposit_batch_details', N'COLUMN', N'deposit_batch_id'
GO
DECLARE @v sql_variant 
SET @v = N'Subtype of the deposit. see tran_types table.'
EXECUTE sp_addextendedproperty N'MS_Description', @v, N'SCHEMA', N'dbo', N'TABLE', N'Tmp_deposit_batch_details', N'COLUMN', N'tran_subtype'
GO
DECLARE @v sql_variant 
SET @v = N'If true, the item is acceptable for being posted. There should not be an error.'
EXECUTE sp_addextendedproperty N'MS_Description', @v, N'SCHEMA', N'dbo', N'TABLE', N'Tmp_deposit_batch_details', N'COLUMN', N'ok_to_post'
GO
DECLARE @v sql_variant 
SET @v = N'If the source is a lockbox, this is the character string that represents the client ID as it came from the source. It may not be a valid client id.'
EXECUTE sp_addextendedproperty N'MS_Description', @v, N'SCHEMA', N'dbo', N'TABLE', N'Tmp_deposit_batch_details', N'COLUMN', N'scanned_client'
GO
DECLARE @v sql_variant 
SET @v = N'If scanned_client is a number, this is the client number.'
EXECUTE sp_addextendedproperty N'MS_Description', @v, N'SCHEMA', N'dbo', N'TABLE', N'Tmp_deposit_batch_details', N'COLUMN', N'client'
GO
DECLARE @v sql_variant 
SET @v = N'If the item is a creditor refund, this is the creditor associated with the refund.'
EXECUTE sp_addextendedproperty N'MS_Description', @v, N'SCHEMA', N'dbo', N'TABLE', N'Tmp_deposit_batch_details', N'COLUMN', N'creditor'
GO
DECLARE @v sql_variant 
SET @v = N'Pointer to the client_products table if needed, otherwise NULL'
EXECUTE sp_addextendedproperty N'MS_Description', @v, N'SCHEMA', N'dbo', N'TABLE', N'Tmp_deposit_batch_details', N'COLUMN', N'client_product'
GO
DECLARE @v sql_variant 
SET @v = N'This is the dollar amount of the transaction.'
EXECUTE sp_addextendedproperty N'MS_Description', @v, N'SCHEMA', N'dbo', N'TABLE', N'Tmp_deposit_batch_details', N'COLUMN', N'amount'
GO
DECLARE @v sql_variant 
SET @v = N'For creditor refunds, this is the amount of the deduction taken from the Z9999 creditor.'
EXECUTE sp_addextendedproperty N'MS_Description', @v, N'SCHEMA', N'dbo', N'TABLE', N'Tmp_deposit_batch_details', N'COLUMN', N'fairshare_amt'
GO
DECLARE @v sql_variant 
SET @v = N'For creditor refunds, this is the associated percentage rate of the deduction / billed figure.'
EXECUTE sp_addextendedproperty N'MS_Description', @v, N'SCHEMA', N'dbo', N'TABLE', N'Tmp_deposit_batch_details', N'COLUMN', N'fairshare_pct'
GO
DECLARE @v sql_variant 
SET @v = N'For creditor refunds, this is the deduct/none/billed status of the creditor at the time of the creditor refund.'
EXECUTE sp_addextendedproperty N'MS_Description', @v, N'SCHEMA', N'dbo', N'TABLE', N'Tmp_deposit_batch_details', N'COLUMN', N'creditor_type'
GO
DECLARE @v sql_variant 
SET @v = N'This is the date of the instrument (check, money order, etc). It is for documentation purposes only.'
EXECUTE sp_addextendedproperty N'MS_Description', @v, N'SCHEMA', N'dbo', N'TABLE', N'Tmp_deposit_batch_details', N'COLUMN', N'item_date'
GO
DECLARE @v sql_variant 
SET @v = N'This is the check number, money order number, etc of the deposit item.'
EXECUTE sp_addextendedproperty N'MS_Description', @v, N'SCHEMA', N'dbo', N'TABLE', N'Tmp_deposit_batch_details', N'COLUMN', N'reference'
GO
DECLARE @v sql_variant 
SET @v = N'This is the reason why the item is not acceptable to be posted. It should be null if "ok_to_post" is 1.'
EXECUTE sp_addextendedproperty N'MS_Description', @v, N'SCHEMA', N'dbo', N'TABLE', N'Tmp_deposit_batch_details', N'COLUMN', N'message'
GO
DECLARE @v sql_variant 
SET @v = N'For ACH deposit batches, this is the transaction code associated with the item. "27" is checking, "37" is savings, etc.'
EXECUTE sp_addextendedproperty N'MS_Description', @v, N'SCHEMA', N'dbo', N'TABLE', N'Tmp_deposit_batch_details', N'COLUMN', N'ach_transaction_code'
GO
DECLARE @v sql_variant 
SET @v = N'For ACH deposit batches, this is the ABA routing number with the checkdigit.'
EXECUTE sp_addextendedproperty N'MS_Description', @v, N'SCHEMA', N'dbo', N'TABLE', N'Tmp_deposit_batch_details', N'COLUMN', N'ach_routing_number'
GO
DECLARE @v sql_variant 
SET @v = N'For ACH deposit batches, this is the bank account number'
EXECUTE sp_addextendedproperty N'MS_Description', @v, N'SCHEMA', N'dbo', N'TABLE', N'Tmp_deposit_batch_details', N'COLUMN', N'ach_account_number'
GO
DECLARE @v sql_variant 
SET @v = N'For ACH deposit batches, this is the response (error) code. If not-null, the ok_to_post should be 0.'
EXECUTE sp_addextendedproperty N'MS_Description', @v, N'SCHEMA', N'dbo', N'TABLE', N'Tmp_deposit_batch_details', N'COLUMN', N'ach_authentication_code'
GO
DECLARE @v sql_variant 
SET @v = N'For ACH deposit batches, this is the batch label associated with the response. It is used to print the report.'
EXECUTE sp_addextendedproperty N'MS_Description', @v, N'SCHEMA', N'dbo', N'TABLE', N'Tmp_deposit_batch_details', N'COLUMN', N'ach_response_batch_id'
GO
DECLARE @v sql_variant 
SET @v = N'This is the date / time when the item is actually posted. It is used to prevent duplicate postings.'
EXECUTE sp_addextendedproperty N'MS_Description', @v, N'SCHEMA', N'dbo', N'TABLE', N'Tmp_deposit_batch_details', N'COLUMN', N'date_posted'
GO
DECLARE @v sql_variant 
SET @v = N'This is the name of the person who created the deposit / refund transaction.'
EXECUTE sp_addextendedproperty N'MS_Description', @v, N'SCHEMA', N'dbo', N'TABLE', N'Tmp_deposit_batch_details', N'COLUMN', N'created_by'
GO
DECLARE @v sql_variant 
SET @v = N'This is the date / time when the transaction was created.'
EXECUTE sp_addextendedproperty N'MS_Description', @v, N'SCHEMA', N'dbo', N'TABLE', N'Tmp_deposit_batch_details', N'COLUMN', N'date_created'
GO
EXECUTE sp_bindefault N'dbo.default_true', N'dbo.Tmp_deposit_batch_details.ok_to_post'
GO
EXECUTE sp_bindefault N'dbo.default_money', N'dbo.Tmp_deposit_batch_details.amount'
GO
EXECUTE sp_bindefault N'dbo.default_money', N'dbo.Tmp_deposit_batch_details.fairshare_amt'
GO
EXECUTE sp_bindefault N'dbo.default_date', N'dbo.Tmp_deposit_batch_details.item_date'
GO
EXECUTE sp_bindefault N'dbo.default_date', N'dbo.Tmp_deposit_batch_details.date_created'
GO
SET IDENTITY_INSERT dbo.Tmp_deposit_batch_details ON
GO
EXECUTE sp_unbindrule N'dbo.Tmp_deposit_batch_details.creditor'
GO
EXECUTE sp_unbindrule N'dbo.Tmp_deposit_batch_details.fairshare_pct'
GO
EXECUTE sp_unbindrule N'dbo.Tmp_deposit_batch_details.creditor_type'
GO
IF EXISTS(SELECT * FROM dbo.deposit_batch_details)
	 EXEC('INSERT INTO dbo.Tmp_deposit_batch_details (deposit_batch_detail, deposit_batch_id, tran_subtype, ok_to_post, scanned_client, client, creditor, client_creditor, client_product, amount, fairshare_amt, fairshare_pct, creditor_type, item_date, reference, message, ach_transaction_code, ach_routing_number, ach_account_number, ach_authentication_code, ach_response_batch_id, date_posted, created_by, date_created)
		SELECT deposit_batch_detail, deposit_batch_id, tran_subtype, ok_to_post, scanned_client, client, creditor, client_creditor, client_product, amount, fairshare_amt, fairshare_pct, creditor_type, item_date, reference, message, ach_transaction_code, ach_routing_number, ach_account_number, ach_authentication_code, ach_response_batch_id, date_posted, created_by, date_created FROM dbo.deposit_batch_details WITH (HOLDLOCK TABLOCKX)')
GO
SET IDENTITY_INSERT dbo.Tmp_deposit_batch_details OFF
GO
DROP TABLE dbo.deposit_batch_details
GO
EXECUTE sp_rename N'dbo.Tmp_deposit_batch_details', N'deposit_batch_details', 'OBJECT' 
GO
ALTER TABLE dbo.deposit_batch_details ADD CONSTRAINT
	PK_deposit_batch_details PRIMARY KEY CLUSTERED 
	(
	deposit_batch_detail
	) WITH( PAD_INDEX = OFF, FILLFACTOR = 90, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]

GO
CREATE NONCLUSTERED INDEX IX_deposit_batch_details_1 ON dbo.deposit_batch_details
	(
	deposit_batch_id
	) WITH( PAD_INDEX = OFF, FILLFACTOR = 90, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX IX_deposit_batch_details_2 ON dbo.deposit_batch_details
	(
	ach_response_batch_id
	) WITH( PAD_INDEX = OFF, FILLFACTOR = 90, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX IX_deposit_batch_details_3 ON dbo.deposit_batch_details
	(
	deposit_batch_id,
	reference,
	ach_transaction_code,
	ach_routing_number,
	ach_account_number
	) WITH( PAD_INDEX = OFF, FILLFACTOR = 90, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
ALTER TABLE dbo.deposit_batch_details ADD CONSTRAINT
	FK_deposit_batch_details_client_products FOREIGN KEY
	(
	client_product
	) REFERENCES dbo.client_products
	(
	oID
	) ON UPDATE  CASCADE 
	 ON DELETE  CASCADE 
GO
ALTER TABLE dbo.deposit_batch_details ADD CONSTRAINT
	FK_deposit_batch_details_deposit_batch_ids FOREIGN KEY
	(
	deposit_batch_id
	) REFERENCES dbo.deposit_batch_ids
	(
	deposit_batch_id
	) ON UPDATE  CASCADE 
	 ON DELETE  CASCADE 
GO
EXECUTE sp_bindrule N'dbo.valid_creditor', N'dbo.deposit_batch_details.creditor'
GO
EXECUTE sp_bindrule N'dbo.valid_fairshare_rate', N'dbo.deposit_batch_details.fairshare_pct'
GO
EXECUTE sp_bindrule N'dbo.valid_creditor_type', N'dbo.deposit_batch_details.creditor_type'
GO
create trigger trig_deposit_batch_details_D on dbo.deposit_batch_details after delete as
begin
	-- =====================================================================
	-- ==        Delete the reference to the deposit_batch_details        ==
	-- =====================================================================
	
	set nocount on

	-- Find the clients that are involved with the batches
	-- We want the client list independant of the transactions since the transactions may now be missing.
	select distinct client into #deposit_batch_details_clients from deleted where client is not null

	-- Compute the amount of money for the clients that are effected
	select	c.client, isnull(sum(d.amount),0) as amount
	into	#deposit_batch_details_amount
	from	#deposit_batch_details_clients c
	inner join deposit_batch_details d on c.client = d.client and d.amount is not null
	inner join deposit_batch_ids ids on d.deposit_batch_id = ids.deposit_batch_id
	where	ids.date_posted is null
	group by c.client;

	-- Correct the amount that the client has in the deposit batches
	update	clients
	set		deposit_in_trust = isnull(x.amount,0)
	from	clients c with (nolock)
	inner join #deposit_batch_details_clients a on c.client = a.client
	left outer join #deposit_batch_details_amount x on a.client = x.client;

	-- remove the temp tables
	drop table #deposit_batch_details_clients
	drop table #deposit_batch_details_amount
end
GO
DENY DELETE ON dbo.deposit_batch_details TO www_role  AS dbo 
GO
DENY INSERT ON dbo.deposit_batch_details TO www_role  AS dbo 
GO
DENY SELECT ON dbo.deposit_batch_details TO www_role  AS dbo 
GO
DENY UPDATE ON dbo.deposit_batch_details TO www_role  AS dbo 
GO
COMMIT
GO
