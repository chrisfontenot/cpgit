EXECUTE UPDATE_DROP_CONSTRAINTS 'InboundTelephoneNumbers'
GO
/* To prevent any potential data loss issues, you should review this script in detail before running it outside the context of the database designer.*/
BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION
GO
CREATE TABLE dbo.Tmp_InboundTelephoneNumbers
	(
	oID dbo.typ_key NOT NULL IDENTITY (1, 1),
	InboundTelephoneNumberGroup dbo.typ_key NOT NULL,
	Description dbo.typ_description NOT NULL,
	TelephoneNumber varchar(50) NULL,
	[Default] bit NOT NULL,
	ActiveFlag bit NOT NULL,
	date_created dbo.typ_date NOT NULL,
	created_by dbo.typ_counselor NOT NULL,
	ts timestamp NOT NULL
	)  ON [PRIMARY]
GO
GRANT SELECT,INSERT,UPDATE,DELETE ON Tmp_InboundTelephoneNumbers TO public AS dbo;
GO
EXECUTE sp_bindefault N'dbo.default_false', N'dbo.Tmp_InboundTelephoneNumbers.[Default]'
GO
EXECUTE sp_bindefault N'dbo.default_true', N'dbo.Tmp_InboundTelephoneNumbers.ActiveFlag'
GO
SET IDENTITY_INSERT dbo.Tmp_InboundTelephoneNumbers ON
GO
IF EXISTS(SELECT * FROM SYSOBJECTS WHERE NAME=N'InboundTelephoneNumbers' AND TYPE='U')
BEGIN
	EXEC('INSERT INTO dbo.Tmp_InboundTelephoneNumbers (oID, InboundTelephoneNumberGroup, Description, TelephoneNumber, [Default], ActiveFlag, date_created, created_by)
		SELECT oID, InboundTelephoneNumberGroup, Description, TelephoneNumber, [Default], ActiveFlag, date_created, created_by FROM dbo.InboundTelephoneNumbers WITH (HOLDLOCK TABLOCKX)')
	EXEC('DROP TABLE dbo.InboundTelephoneNumbers')
END
GO
SET IDENTITY_INSERT dbo.Tmp_InboundTelephoneNumbers OFF
GO
EXECUTE sp_rename N'dbo.Tmp_InboundTelephoneNumbers', N'InboundTelephoneNumbers', 'OBJECT' 
GO
ALTER TABLE dbo.InboundTelephoneNumbers ADD CONSTRAINT
	PK_InboundTelephoneNumbers PRIMARY KEY CLUSTERED 
	(
	oID
	) WITH( STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
ALTER TABLE dbo.InboundTelephoneNumbers ADD CONSTRAINT
	FK_InboundTelephoneNumbers_InBoundTelephoneNumberGroups FOREIGN KEY
	(
	InboundTelephoneNumberGroup
	) REFERENCES dbo.InBoundTelephoneNumberGroups
	(
	oID
	) ON UPDATE  CASCADE 
	 ON DELETE  CASCADE 
GO
COMMIT
