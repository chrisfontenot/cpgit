ALTER TABLE dbo.client_disclosures_MHA DROP CONSTRAINT FK_client_disclosures_MHA_client_disclosures
GO

/* To prevent any potential data loss issues, you should review this script in detail before running it outside the context of the database designer.*/
BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION
GO
CREATE TABLE dbo.Tmp_client_disclosures_MHA
	(
	oID dbo.typ_key NOT NULL IDENTITY (1, 1),
	client_disclosure_ID dbo.typ_key NOT NULL,
	eligible_HAMP_tier_1 bit NOT NULL,
	eligible_HAMP_tier_2 bit NOT NULL,
	eligible_HARP bit NOT NULL,
	eligible_FHA_HAMP bit NOT NULL,
	eligible_HAFA bit NOT NULL,
	answer_01_01 bit NOT NULL,
	answer_01_02 bit NOT NULL,
	answer_01_03 bit NOT NULL,
	answer_01_04 bit NOT NULL,
	answer_01_05 bit NOT NULL,
	answer_01_06 bit NOT NULL,
	answer_01_07 bit NOT NULL,
	answer_01_08 bit NOT NULL,
	answer_01_09 bit NOT NULL,
	answer_01_10 bit NOT NULL,
	answer_01_11 bit NOT NULL,
	answer_02_01 bit NOT NULL,
	answer_02_02 bit NOT NULL,
	answer_02_03 bit NOT NULL,
	answer_03_01 bit NOT NULL,
	answer_03_02 bit NOT NULL,
	answer_03_03 bit NOT NULL,
	answer_03_04 bit NOT NULL,
	answer_03_05 bit NOT NULL,
	answer_04_01 bit NOT NULL,
	answer_04_02 bit NOT NULL,
	answer_04_03 bit NOT NULL,
	answer_04_04 bit NOT NULL,
	answer_04_05 bit NOT NULL,
	answer_05_01 bit NOT NULL,
	answer_05_02 bit NOT NULL,
	answer_05_03 bit NOT NULL,
	answer_05_04 bit NOT NULL,
	answer_05_05 bit NOT NULL,
	answer_05_06 bit NOT NULL,
	answer_05_07 bit NOT NULL,
	ts timestamp NOT NULL
	)  ON [PRIMARY]
GO
ALTER TABLE dbo.Tmp_client_disclosures_MHA SET (LOCK_ESCALATION = TABLE)
GO
EXECUTE sp_bindefault N'dbo.default_false', N'dbo.Tmp_client_disclosures_MHA.eligible_HAMP_tier_1'
GO
EXECUTE sp_bindefault N'dbo.default_false', N'dbo.Tmp_client_disclosures_MHA.eligible_HAMP_tier_2'
GO
EXECUTE sp_bindefault N'dbo.default_false', N'dbo.Tmp_client_disclosures_MHA.eligible_HARP'
GO
EXECUTE sp_bindefault N'dbo.default_false', N'dbo.Tmp_client_disclosures_MHA.eligible_FHA_HAMP'
GO
EXECUTE sp_bindefault N'dbo.default_false', N'dbo.Tmp_client_disclosures_MHA.eligible_HAFA'
GO
EXECUTE sp_bindefault N'dbo.default_false', N'dbo.Tmp_client_disclosures_MHA.answer_01_01'
GO
EXECUTE sp_bindefault N'dbo.default_false', N'dbo.Tmp_client_disclosures_MHA.answer_01_02'
GO
EXECUTE sp_bindefault N'dbo.default_false', N'dbo.Tmp_client_disclosures_MHA.answer_01_03'
GO
EXECUTE sp_bindefault N'dbo.default_false', N'dbo.Tmp_client_disclosures_MHA.answer_01_04'
GO
EXECUTE sp_bindefault N'dbo.default_false', N'dbo.Tmp_client_disclosures_MHA.answer_01_05'
GO
EXECUTE sp_bindefault N'dbo.default_false', N'dbo.Tmp_client_disclosures_MHA.answer_01_06'
GO
EXECUTE sp_bindefault N'dbo.default_false', N'dbo.Tmp_client_disclosures_MHA.answer_01_07'
GO
EXECUTE sp_bindefault N'dbo.default_false', N'dbo.Tmp_client_disclosures_MHA.answer_01_08'
GO
EXECUTE sp_bindefault N'dbo.default_false', N'dbo.Tmp_client_disclosures_MHA.answer_01_09'
GO
EXECUTE sp_bindefault N'dbo.default_false', N'dbo.Tmp_client_disclosures_MHA.answer_01_10'
GO
EXECUTE sp_bindefault N'dbo.default_false', N'dbo.Tmp_client_disclosures_MHA.answer_01_11'
GO
EXECUTE sp_bindefault N'dbo.default_false', N'dbo.Tmp_client_disclosures_MHA.answer_02_01'
GO
EXECUTE sp_bindefault N'dbo.default_false', N'dbo.Tmp_client_disclosures_MHA.answer_02_02'
GO
EXECUTE sp_bindefault N'dbo.default_false', N'dbo.Tmp_client_disclosures_MHA.answer_02_03'
GO
EXECUTE sp_bindefault N'dbo.default_false', N'dbo.Tmp_client_disclosures_MHA.answer_03_01'
GO
EXECUTE sp_bindefault N'dbo.default_false', N'dbo.Tmp_client_disclosures_MHA.answer_03_02'
GO
EXECUTE sp_bindefault N'dbo.default_false', N'dbo.Tmp_client_disclosures_MHA.answer_03_03'
GO
EXECUTE sp_bindefault N'dbo.default_false', N'dbo.Tmp_client_disclosures_MHA.answer_03_04'
GO
EXECUTE sp_bindefault N'dbo.default_false', N'dbo.Tmp_client_disclosures_MHA.answer_03_05'
GO
EXECUTE sp_bindefault N'dbo.default_false', N'dbo.Tmp_client_disclosures_MHA.answer_04_01'
GO
EXECUTE sp_bindefault N'dbo.default_false', N'dbo.Tmp_client_disclosures_MHA.answer_04_02'
GO
EXECUTE sp_bindefault N'dbo.default_false', N'dbo.Tmp_client_disclosures_MHA.answer_04_03'
GO
EXECUTE sp_bindefault N'dbo.default_false', N'dbo.Tmp_client_disclosures_MHA.answer_04_04'
GO
EXECUTE sp_bindefault N'dbo.default_false', N'dbo.Tmp_client_disclosures_MHA.answer_04_05'
GO
EXECUTE sp_bindefault N'dbo.default_false', N'dbo.Tmp_client_disclosures_MHA.answer_05_01'
GO
EXECUTE sp_bindefault N'dbo.default_false', N'dbo.Tmp_client_disclosures_MHA.answer_05_02'
GO
EXECUTE sp_bindefault N'dbo.default_false', N'dbo.Tmp_client_disclosures_MHA.answer_05_03'
GO
EXECUTE sp_bindefault N'dbo.default_false', N'dbo.Tmp_client_disclosures_MHA.answer_05_04'
GO
EXECUTE sp_bindefault N'dbo.default_false', N'dbo.Tmp_client_disclosures_MHA.answer_05_05'
GO
EXECUTE sp_bindefault N'dbo.default_false', N'dbo.Tmp_client_disclosures_MHA.answer_05_06'
GO
EXECUTE sp_bindefault N'dbo.default_false', N'dbo.Tmp_client_disclosures_MHA.answer_05_07'
GO
SET IDENTITY_INSERT dbo.Tmp_client_disclosures_MHA ON
GO
IF EXISTS(SELECT * FROM sysobjects where type='U' AND name = N'client_disclosures_MHA')
BEGIN
	EXEC('INSERT INTO dbo.Tmp_client_disclosures_MHA (oID, client_disclosure_ID, eligible_HAMP_tier_1, eligible_HAMP_tier_2, eligible_HARP, eligible_FHA_HAMP, eligible_HAFA, answer_01_01, answer_01_02, answer_01_03, answer_01_04, answer_01_05, answer_01_06, answer_01_07, answer_01_08, answer_01_09, answer_01_10, answer_01_11, answer_02_01, answer_02_02, answer_02_03, answer_03_01, answer_03_02, answer_03_03, answer_03_04, answer_03_05, answer_04_01, answer_04_02, answer_04_03, answer_04_04, answer_04_05, answer_05_01, answer_05_02, answer_05_03, answer_05_04, answer_05_05, answer_05_06, answer_05_07) SELECT oID, client_disclosure_ID, eligible_HAMP_tier_1, eligible_HAMP_tier_2, eligible_HARP, eligible_FHA_HAMP, eligible_HAFA, answer_01_01, answer_01_02, answer_01_03, answer_01_04, answer_01_05, answer_01_06, answer_01_07, answer_01_08, answer_01_09, answer_01_10, answer_01_11, answer_02_01, answer_02_02, answer_02_03, answer_03_01, answer_03_02, answer_03_03, answer_03_04, answer_03_05, answer_04_01, answer_04_02, answer_04_03, answer_04_04, answer_04_05, answer_05_01, answer_05_02, answer_05_03, answer_05_04, answer_05_05, answer_05_06, answer_05_07 FROM dbo.client_disclosures_MHA WITH (HOLDLOCK TABLOCKX)')
	EXEC('DROP TABLE dbo.client_disclosures_MHA')
END
GO
SET IDENTITY_INSERT dbo.Tmp_client_disclosures_MHA OFF
GO
EXECUTE sp_rename N'dbo.Tmp_client_disclosures_MHA', N'client_disclosures_MHA', 'OBJECT' 
GO
ALTER TABLE dbo.client_disclosures_MHA ADD CONSTRAINT
	PK_client_disclosures_MHA PRIMARY KEY CLUSTERED 
	(
	oID
	) ON [PRIMARY]
GO
DECLARE @v sql_variant 
SET @v = N'One-to-One relationship'
EXECUTE sp_addextendedproperty N'MS_Description', @v, N'SCHEMA', N'dbo', N'TABLE', N'client_disclosures_MHA', N'CONSTRAINT', N'FK_client_disclosures_MHA_client_disclosures'
GO
COMMIT
GO
ALTER TABLE dbo.client_disclosures_MHA ADD CONSTRAINT
	FK_client_disclosures_MHA_client_disclosures FOREIGN KEY
	(
	client_disclosure_ID
	) REFERENCES dbo.client_disclosures
	(
	oID
	) ON UPDATE  CASCADE 
	 ON DELETE  CASCADE 
GO
