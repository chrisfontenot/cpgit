USE [DebtPlus]
GO
/****** Object:  Table [dbo].[rpps_addresses]    Script Date: 09/15/2014 13:13:26 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[rpps_addresses](
	[rpps_biller_address] [dbo].[typ_key] IDENTITY(1,1) NOT NULL,
	[Mastercard_Key] [varchar](50) NULL,
	[rpps_biller_id] [dbo].[typ_rpps_biller_id] NOT NULL,
	[biller_addr1] [dbo].[typ_address] NULL,
	[biller_addr2] [dbo].[typ_address] NULL,
	[biller_city] [dbo].[typ_city] NULL,
	[biller_state] [dbo].[typ_state] NULL,
	[biller_zipcode] [dbo].[typ_zipcode] NULL,
	[biller_country] [varchar](3) NULL,
	[created_by] [dbo].[typ_counselor] NOT NULL,
	[date_created] [dbo].[typ_date] NOT NULL,
 CONSTRAINT [PK_rpps_biller_addresses] PRIMARY KEY CLUSTERED 
(
	[rpps_biller_address] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 90) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
