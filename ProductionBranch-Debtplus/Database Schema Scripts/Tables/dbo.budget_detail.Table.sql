-- Add the identity column
IF NOT EXISTS(SELECT * FROM SYSOBJECTS o INNER JOIN SYSCOLUMNS c ON o.ID = c.ID WHERE o.NAME = 'budget_detail' and c.NAME = 'oID' AND o.TYPE = 'U')
	exec ('ALTER TABLE budget_detail ADD oID int NOT NULL IDENTITY(1,1)')
GO
/* To prevent any potential data loss issues, you should review this script in detail before running it outside the context of the database designer.*/
BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION
GO
COMMIT
BEGIN TRANSACTION
GO
CREATE TABLE dbo.Tmp_budget_detail
	(
	oID dbo.typ_key NOT NULL IDENTITY (1, 1),
	budget dbo.typ_key NOT NULL,
	budget_category dbo.typ_key NOT NULL,
	client_amount money NOT NULL,
	suggested_amount money NOT NULL,
	date_created dbo.typ_date NOT NULL,
	created_by dbo.typ_counselor NOT NULL,
	ts timestamp NOT NULL
	)  ON [PRIMARY]
GO
GRANT DELETE ON dbo.Tmp_budget_detail TO public  AS dbo
GO
GRANT INSERT ON dbo.Tmp_budget_detail TO public  AS dbo
GO
GRANT SELECT ON dbo.Tmp_budget_detail TO public  AS dbo
GO
GRANT UPDATE ON dbo.Tmp_budget_detail TO public  AS dbo
GO
DECLARE @v sql_variant 
SET @v = N'Primary key to the table'
EXECUTE sp_addextendedproperty N'MS_Description', @v, N'SCHEMA', N'dbo', N'TABLE', N'Tmp_budget_detail', N'COLUMN', N'oID'
GO
DECLARE @v sql_variant 
SET @v = N'This table binds the budget categories, budgets, and clients together. It lists the information for each budget (and subsequently, each client) and the budget category.'
EXECUTE sp_addextendedproperty N'MS_Description', @v, N'SCHEMA', N'dbo', N'TABLE', N'Tmp_budget_detail', NULL, NULL
GO
DECLARE @v sql_variant 
SET @v = N'Original value from the client'
EXECUTE sp_addextendedproperty N'MS_Description', @v, N'SCHEMA', N'dbo', N'TABLE', N'Tmp_budget_detail', N'COLUMN', N'client_amount'
GO
DECLARE @v sql_variant 
SET @v = N'Suggested amount given by the counselor'
EXECUTE sp_addextendedproperty N'MS_Description', @v, N'SCHEMA', N'dbo', N'TABLE', N'Tmp_budget_detail', N'COLUMN', N'suggested_amount'
GO
DECLARE @v sql_variant 
SET @v = N'Date/Time when the row was created.'
EXECUTE sp_addextendedproperty N'MS_Description', @v, N'SCHEMA', N'dbo', N'TABLE', N'Tmp_budget_detail', N'COLUMN', N'date_created'
GO
DECLARE @v sql_variant 
SET @v = N'Person who created the row.'
EXECUTE sp_addextendedproperty N'MS_Description', @v, N'SCHEMA', N'dbo', N'TABLE', N'Tmp_budget_detail', N'COLUMN', N'created_by'
GO
EXECUTE sp_bindefault N'dbo.default_money', N'dbo.Tmp_budget_detail.client_amount'
GO
EXECUTE sp_bindefault N'dbo.default_money', N'dbo.Tmp_budget_detail.suggested_amount'
GO
SET IDENTITY_INSERT dbo.Tmp_budget_detail ON
GO
IF EXISTS(SELECT * FROM dbo.budget_detail)
	 EXEC('INSERT INTO dbo.Tmp_budget_detail (oID, budget, budget_category, client_amount, suggested_amount, date_created, created_by)
		SELECT oID, budget, budget_category, client_amount, suggested_amount, date_created, created_by FROM dbo.budget_detail WITH (HOLDLOCK TABLOCKX)')
GO
SET IDENTITY_INSERT dbo.Tmp_budget_detail OFF
GO
DROP TABLE dbo.budget_detail
GO
EXECUTE sp_rename N'dbo.Tmp_budget_detail', N'budget_detail', 'OBJECT' 
GO
ALTER TABLE dbo.budget_detail ADD CONSTRAINT PK_budget_detail PRIMARY KEY CLUSTERED 
	(
	oID
	) WITH( PAD_INDEX = OFF, FILLFACTOR = 90, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
CREATE UNIQUE NONCLUSTERED INDEX IX_budget_detail_1 ON dbo.budget_detail
	(
	budget,
	budget_category
	) WITH( STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX IX_budget_detail_2 ON dbo.budget_detail
	(
	budget
	) WITH( PAD_INDEX = OFF, FILLFACTOR = 90, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
EXEC ('DELETE dbo.budget_detail FROM dbo.budget_detail d LEFT OUTER JOIN dbo.budgets b on d.budget = b.budget WHERE b.budget is null')
GO
ALTER TABLE dbo.budget_detail ADD CONSTRAINT FK_budget_detail_budgets FOREIGN KEY
	(
	budget
	) REFERENCES dbo.budgets
	(
	budget
	) ON UPDATE  NO ACTION 
	 ON DELETE  CASCADE 
GO
DENY DELETE ON dbo.budget_detail TO www_role  AS dbo 
GO
DENY INSERT ON dbo.budget_detail TO www_role  AS dbo 
GO
DENY SELECT ON dbo.budget_detail TO www_role  AS dbo 
GO
DENY UPDATE ON dbo.budget_detail TO www_role  AS dbo 
GO
COMMIT
