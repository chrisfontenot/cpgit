/*
   Wednesday, November 04, 20156:30:09 AM
   User: sa
   Server: 10.0.199.12
   Database: DebtPlus_HPF
   Application: 
*/

/* To prevent any potential data loss issues, you should review this script in detail before running it outside the context of the database designer.*/
BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION
GO
EXECUTE UPDATE_ADD_COLUMN 'recon_details', 'checknum'
GO
CREATE TABLE dbo.Tmp_recon_details
	(
	recon_detail dbo.typ_key NOT NULL IDENTITY (1, 1),
	recon_batch dbo.typ_key NOT NULL,
	tran_type dbo.typ_transaction NULL,
	bank dbo.typ_key NULL,
	checknum bigint NULL,
	trust_register dbo.typ_key NULL,
	amount money NULL,
	cleared dbo.typ_cleared NOT NULL,
	recon_date datetime NULL,
	message dbo.typ_message NULL,
	comments varchar(256) NULL,
	date_created dbo.typ_date NOT NULL,
	created_by dbo.typ_counselor NOT NULL,
	ts timestamp NOT NULL,
	check_number varchar(20) NULL
	)  ON [PRIMARY]
GO
GRANT DELETE ON dbo.Tmp_recon_details TO public  AS dbo
GO
GRANT INSERT ON dbo.Tmp_recon_details TO public  AS dbo
GO
GRANT SELECT ON dbo.Tmp_recon_details TO public  AS dbo
GO
GRANT UPDATE ON dbo.Tmp_recon_details TO public  AS dbo
GO
DECLARE @v sql_variant 
SET @v = N'Primary key to the table.'
EXECUTE sp_addextendedproperty N'MS_Description', @v, N'SCHEMA', N'dbo', N'TABLE', N'Tmp_recon_details', N'COLUMN', N'recon_detail'
GO
DECLARE @v sql_variant 
SET @v = N'Reconcile batch number associated with the transaction. >From the recon_batches table.'
EXECUTE sp_addextendedproperty N'MS_Description', @v, N'SCHEMA', N'dbo', N'TABLE', N'Tmp_recon_details', N'COLUMN', N'recon_batch'
GO
DECLARE @v sql_variant 
SET @v = N'Type of the transaction. "AD" is a check, others may be "DP", "BE", "SC", "IN", or "BW".'
EXECUTE sp_addextendedproperty N'MS_Description', @v, N'SCHEMA', N'dbo', N'TABLE', N'Tmp_recon_details', N'COLUMN', N'tran_type'
GO
DECLARE @v sql_variant 
SET @v = N'Pointer to the associated trust register item being reconciled.'
EXECUTE sp_addextendedproperty N'MS_Description', @v, N'SCHEMA', N'dbo', N'TABLE', N'Tmp_recon_details', N'COLUMN', N'trust_register'
GO
DECLARE @v sql_variant 
SET @v = N'Reconcilation amount from the bank statement. It should equal the amount in the trust register.'
EXECUTE sp_addextendedproperty N'MS_Description', @v, N'SCHEMA', N'dbo', N'TABLE', N'Tmp_recon_details', N'COLUMN', N'amount'
GO
DECLARE @v sql_variant 
SET @v = N'Status of the reconcilation item. "R" is reconciled, " " is un-reconciled (deleted)'
EXECUTE sp_addextendedproperty N'MS_Description', @v, N'SCHEMA', N'dbo', N'TABLE', N'Tmp_recon_details', N'COLUMN', N'cleared'
GO
DECLARE @v sql_variant 
SET @v = N'Date the item is cleared'
EXECUTE sp_addextendedproperty N'MS_Description', @v, N'SCHEMA', N'dbo', N'TABLE', N'Tmp_recon_details', N'COLUMN', N'recon_date'
GO
DECLARE @v sql_variant 
SET @v = N'Error message associated with the transaction. Valid transactions have NULL in this field.'
EXECUTE sp_addextendedproperty N'MS_Description', @v, N'SCHEMA', N'dbo', N'TABLE', N'Tmp_recon_details', N'COLUMN', N'message'
GO
DECLARE @v sql_variant 
SET @v = N'Optional message string from the bank reconcilation file.'
EXECUTE sp_addextendedproperty N'MS_Description', @v, N'SCHEMA', N'dbo', N'TABLE', N'Tmp_recon_details', N'COLUMN', N'comments'
GO
DECLARE @v sql_variant 
SET @v = N'Date/Time when the row was created.'
EXECUTE sp_addextendedproperty N'MS_Description', @v, N'SCHEMA', N'dbo', N'TABLE', N'Tmp_recon_details', N'COLUMN', N'date_created'
GO
DECLARE @v sql_variant 
SET @v = N'Person who created the row.'
EXECUTE sp_addextendedproperty N'MS_Description', @v, N'SCHEMA', N'dbo', N'TABLE', N'Tmp_recon_details', N'COLUMN', N'created_by'
GO
DECLARE @v sql_variant 
SET @v = N'Check number from the bank associated with the transaction.'
EXECUTE sp_addextendedproperty N'MS_Description', @v, N'SCHEMA', N'dbo', N'TABLE', N'Tmp_recon_details', N'COLUMN', N'check_number'
GO
EXECUTE sp_bindefault N'dbo.default_money', N'dbo.Tmp_recon_details.amount'
GO
EXECUTE sp_bindefault N'dbo.default_date', N'dbo.Tmp_recon_details.recon_date'
GO
SET IDENTITY_INSERT dbo.Tmp_recon_details ON
GO
IF EXISTS(SELECT * FROM dbo.recon_details)
	 EXEC('INSERT INTO dbo.Tmp_recon_details (recon_detail, recon_batch, tran_type, bank, checknum, trust_register, amount, cleared, recon_date, message, comments, date_created, created_by, check_number)
		SELECT recon_detail, recon_batch, tran_type, bank, convert(bigint,dbo.numbers_only(check_number)) as checknum, trust_register, amount, cleared, recon_date, message, comments, date_created, created_by, check_number FROM dbo.recon_details WITH (HOLDLOCK TABLOCKX)')
GO
SET IDENTITY_INSERT dbo.Tmp_recon_details OFF
GO
DROP TABLE dbo.recon_details
GO
EXECUTE sp_rename N'dbo.Tmp_recon_details', N'recon_details', 'OBJECT' 
GO
ALTER TABLE dbo.recon_details ADD CONSTRAINT
	PK_recon_details PRIMARY KEY CLUSTERED 
	(
	recon_detail
	) WITH( PAD_INDEX = OFF, FILLFACTOR = 90, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]

GO
CREATE NONCLUSTERED INDEX IX_recon_details_1 ON dbo.recon_details
	(
	recon_batch,
	tran_type
	) WITH( PAD_INDEX = OFF, FILLFACTOR = 90, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
EXECUTE sp_bindrule N'dbo.valid_cleared', N'dbo.recon_details.cleared'
GO
DENY DELETE ON dbo.recon_details TO www_role  AS dbo 
GO
DENY INSERT ON dbo.recon_details TO www_role  AS dbo 
GO
DENY SELECT ON dbo.recon_details TO www_role  AS dbo 
GO
DENY UPDATE ON dbo.recon_details TO www_role  AS dbo 
GO
COMMIT
