/* To prevent any potential data loss issues, you should review this script in detail before running it outside the context of the database designer.*/
BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION
GO
CREATE TABLE dbo.Tmp_HECMDefault_Lookup
	(
	Id dbo.typ_key NOT NULL IDENTITY (1, 1),
	Description varchar(50) NOT NULL,
	[Default] bit NOT NULL,
	LookupType dbo.typ_key NOT NULL,
	CreatedDate dbo.typ_date NOT NULL,
	ts timestamp NOT NULL
	)  ON [PRIMARY]
GO
EXECUTE sp_bindefault N'dbo.default_false', N'dbo.Tmp_HECMDefault_Lookup.[Default]'
GO
SET IDENTITY_INSERT dbo.Tmp_HECMDefault_Lookup ON
GO
IF EXISTS(SELECT * FROM dbo.HECMDefault_Lookup)
	 EXEC('INSERT INTO dbo.Tmp_HECMDefault_Lookup (Id, Description, [Default], LookupType, CreatedDate)
		SELECT ID, Description, [Default], LookupType, CreatedDate FROM dbo.HECMDefault_Lookup WITH (HOLDLOCK TABLOCKX)')
GO
SET IDENTITY_INSERT dbo.Tmp_HECMDefault_Lookup OFF
GO
DROP TABLE dbo.HECMDefault_Lookup
GO
EXECUTE sp_rename N'dbo.Tmp_HECMDefault_Lookup', N'HECMDefault_Lookup', 'OBJECT' 
GO
ALTER TABLE dbo.HECMDefault_Lookup ADD CONSTRAINT
	PK_HECMDefault_Lookup PRIMARY KEY CLUSTERED 
	(
	Id
	) WITH( STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
COMMIT
