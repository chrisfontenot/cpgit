USE [DebtPlus]
GO
/****** Object:  Table [dbo].[Housing_ARM_referenceInfo]    Script Date: 09/15/2014 13:13:26 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Housing_ARM_referenceInfo](
	[oID] [dbo].[typ_key] IDENTITY(1,1) NOT NULL,
	[groupId] [int] NOT NULL,
	[id] [int] NOT NULL,
	[longDesc] [varchar](2048) NULL,
	[name] [varchar](256) NULL,
	[shortDesc] [varchar](256) NULL,
	[old_hud_9902_section] [varchar](80) NULL,
 CONSTRAINT [PK_housing_ARM_referenceInfo] PRIMARY KEY CLUSTERED 
(
	[oID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
