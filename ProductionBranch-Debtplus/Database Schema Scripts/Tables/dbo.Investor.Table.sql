use debtplus
GO

/* To prevent any potential data loss issues, you should review this script in detail before running it outside the context of the database designer.*/
BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION
GO
CREATE TABLE dbo.Tmp_Investor
	(
	ID dbo.typ_key NOT NULL IDENTITY (1, 1),
	InvestorName dbo.typ_description NOT NULL,
	[Default] bit NOT NULL,
	IsActive bit NOT NULL,
	date_created dbo.typ_date NOT NULL,
	created_by dbo.typ_counselor NOT NULL,
	ts rowversion NOT NULL
	)  ON [PRIMARY]
GO
DECLARE @v sql_variant 
SET @v = N'Primary key to the table'
EXECUTE sp_addextendedproperty N'MS_Description', @v, N'SCHEMA', N'dbo', N'TABLE', N'Tmp_Investor', N'COLUMN', N'ID'
GO
DECLARE @v sql_variant 
SET @v = N'Description for the investor'
EXECUTE sp_addextendedproperty N'MS_Description', @v, N'SCHEMA', N'dbo', N'TABLE', N'Tmp_Investor', N'COLUMN', N'InvestorName'
GO
DECLARE @v sql_variant 
SET @v = N'TRUE if this is the default item'
EXECUTE sp_addextendedproperty N'MS_Description', @v, N'SCHEMA', N'dbo', N'TABLE', N'Tmp_Investor', N'COLUMN', N'Default'
GO
DECLARE @v sql_variant 
SET @v = N'TRUE if the value may be used for new references'
EXECUTE sp_addextendedproperty N'MS_Description', @v, N'SCHEMA', N'dbo', N'TABLE', N'Tmp_Investor', N'COLUMN', N'IsActive'
GO
DECLARE @v sql_variant 
SET @v = N'Date/Time when the item was created'
EXECUTE sp_addextendedproperty N'MS_Description', @v, N'SCHEMA', N'dbo', N'TABLE', N'Tmp_Investor', N'COLUMN', N'date_created'
GO
DECLARE @v sql_variant 
SET @v = N'Person who created the item'
EXECUTE sp_addextendedproperty N'MS_Description', @v, N'SCHEMA', N'dbo', N'TABLE', N'Tmp_Investor', N'COLUMN', N'created_by'
GO
EXECUTE sp_bindefault N'dbo.default_false', N'dbo.Tmp_Investor.[Default]'
GO
EXECUTE sp_bindefault N'dbo.default_true', N'dbo.Tmp_Investor.IsActive'
GO
SET IDENTITY_INSERT dbo.Tmp_Investor ON
GO
IF EXISTS(SELECT * FROM sysobjects WHERE name = N'Investor' and type = 'U')
BEGIN
	EXEC('INSERT INTO dbo.Tmp_Investor (ID, InvestorName, [Default], IsActive)
		  SELECT ID, CONVERT(varchar(50), InvestorName), [Default], IsActive FROM dbo.Investor WITH (HOLDLOCK TABLOCKX)')

	declare @actualrowcount int 
	select @actualrowcount = count(*) from Investor
	print 'Investor row count : ' + convert(varchar, @actualrowcount)

	declare @tmprowcount int 
	select @tmprowcount = count(*) from Tmp_Investor
	print 'Tmp_Investor row count : ' + convert(varchar, @tmprowcount)

	if @actualrowcount = @tmprowcount 
	begin
		print 'actual row count and temp table row count match, dropping table...'
		drop table dbo.Investor
	end
END
GO
SET IDENTITY_INSERT dbo.Tmp_Investor OFF
GO
EXECUTE sp_rename N'dbo.Tmp_Investor', N'Investor', 'OBJECT' 
GO
ALTER TABLE dbo.Investor ADD CONSTRAINT
	PK_Investor PRIMARY KEY CLUSTERED 
	(
	ID
	) ON [PRIMARY]
GO
COMMIT
GO
GRANT SELECT,INSERT,DELETE,UPDATE on Investor TO public AS dbo
GO
DENY SELECT,INSERT,DELETE,UPDATE on Investor TO www_role
GO
IF NOT EXISTS(SELECT * FROM Investor)
BEGIN
	TRUNCATE TABLE [Investor]
	INSERT INTO [dbo].[Investor]([InvestorName],[Default]) VALUES ('Unknown or N/A',1)
	INSERT INTO [dbo].[Investor]([InvestorName]) VALUES ('Fannie Mae')
	INSERT INTO [dbo].[Investor]([InvestorName]) VALUES ('Freddie Mac')
	INSERT INTO [dbo].[Investor]([InvestorName]) VALUES ('Private Investor')
END
GO

ALTER TABLE [dbo].[Investor] ADD RxOffice varchar(20) NULL;

update [dbo].[Investor]
   set RxOffice = 'fnma'
where InvestorName = 'Fannie Mae';

update [dbo].[Investor]
   set RxOffice = 'frm'
where InvestorName = 'Freddie Mac';