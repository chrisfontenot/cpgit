USE [DebtPlus]
GO
/****** Object:  Table [dbo].[ach_reject_codes]    Script Date: 09/15/2014 13:13:25 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[ach_reject_codes](
	[ach_reject_code] [varchar](3) NOT NULL,
	[description] [dbo].[typ_description] NOT NULL,
	[serious_error] [bit] NOT NULL,
	[letter_code] [dbo].[typ_letter_code] NULL,
	[date_created] [dbo].[typ_date] NOT NULL,
	[created_by] [dbo].[typ_counselor] NOT NULL,
 CONSTRAINT [PK_ach_reject_codes] PRIMARY KEY CLUSTERED 
(
	[ach_reject_code] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 90) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Primary key to the error code table' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ach_reject_codes', @level2type=N'COLUMN',@level2name=N'ach_reject_code'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Description for the error' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ach_reject_codes', @level2type=N'COLUMN',@level2name=N'description'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Should the ACH be suspended if this error occurred?' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ach_reject_codes', @level2type=N'COLUMN',@level2name=N'serious_error'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Letter to be generated when this error occurs' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ach_reject_codes', @level2type=N'COLUMN',@level2name=N'letter_code'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Date/Time when the row was created.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ach_reject_codes', @level2type=N'COLUMN',@level2name=N'date_created'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Person who created the row.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ach_reject_codes', @level2type=N'COLUMN',@level2name=N'created_by'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'This is a list of the error response codes from the ACH system as well as the actions that we are to take when we receive a specific error. The description is used on the reports.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ach_reject_codes'
GO
