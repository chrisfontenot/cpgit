USE [DebtPlus]
GO
/****** Object:  Table [dbo].[TelephoneNumbers]    Script Date: 09/15/2014 13:13:26 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[TelephoneNumbers](
	[TelephoneNumber] [dbo].[typ_key] IDENTITY(11,1) NOT NULL,
	[Country] [dbo].[typ_key] NULL,
	[Acode] [varchar](50) NULL,
	[Number] [varchar](50) NULL,
	[Ext] [varchar](50) NULL,
	[SearchValue] [varchar](50) NULL,
 CONSTRAINT [PK_TelephoneNumbers] PRIMARY KEY CLUSTERED 
(
	[TelephoneNumber] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
