/* To prevent any potential data loss issues, you should review this script in detail before running it outside the context of the database designer.*/
BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
GO
BEGIN TRANSACTION
GO
execute UPDATE_ADD_COLUMN 'AttributeTypes', 'hpf'
GO
CREATE TABLE dbo.Tmp_AttributeTypes
	(
	[oID] dbo.typ_key NOT NULL IDENTITY (1, 1),
	[Grouping] varchar(50) NULL,
	[Attribute] varchar(50) NOT NULL,
	[Default] bit NOT NULL,
	[ActiveFlag] bit NOT NULL,
	[LanguageID] varchar(50) NULL,
	[HUDLanguage] varchar(50) NULL,
	[HUDServiceType] varchar(10) NULL,
	[hpf] varchar(50) NULL,
	[RxOffice] varchar(20) NULL,
	[ts] timestamp NOT NULL
	)  ON [PRIMARY]
GO
GRANT DELETE ON dbo.Tmp_AttributeTypes TO public  AS dbo
GO
GRANT INSERT ON dbo.Tmp_AttributeTypes TO public  AS dbo
GO
GRANT SELECT ON dbo.Tmp_AttributeTypes TO public  AS dbo
GO
GRANT UPDATE ON dbo.Tmp_AttributeTypes TO public  AS dbo
GO
DECLARE @v sql_variant 
SET @v = N'Attribute Name'
EXECUTE sp_addextendedproperty N'MS_Description', @v, N'SCHEMA', N'dbo', N'TABLE', N'Tmp_AttributeTypes', N'COLUMN', N'Attribute'
GO
EXECUTE sp_bindefault N'dbo.default_false', N'dbo.Tmp_AttributeTypes.[Default]'
GO
EXECUTE sp_bindefault N'dbo.default_true', N'dbo.Tmp_AttributeTypes.ActiveFlag'
GO
SET IDENTITY_INSERT dbo.Tmp_AttributeTypes ON
GO
IF EXISTS(SELECT * FROM dbo.AttributeTypes)
	 EXEC('INSERT INTO dbo.Tmp_AttributeTypes (oID, Grouping, Attribute, [Default], ActiveFlag, LanguageID, HUDLanguage, HUDServiceType, RxOffice, hpf)
		SELECT oID, Grouping, Attribute, [Default], ActiveFlag, LanguageID, HUDLanguage, HUDServiceType, RxOffice, hpf FROM dbo.AttributeTypes WITH (HOLDLOCK TABLOCKX)')
GO
SET IDENTITY_INSERT dbo.Tmp_AttributeTypes OFF
GO
DROP TABLE dbo.AttributeTypes
GO
EXECUTE sp_rename N'dbo.Tmp_AttributeTypes', N'AttributeTypes', 'OBJECT' 
GO
ALTER TABLE dbo.AttributeTypes ADD CONSTRAINT
	PK_AttributeTypes PRIMARY KEY CLUSTERED 
	(
	oID
	) WITH( STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
CREATE UNIQUE NONCLUSTERED INDEX IX_AttributeTypes_1 ON dbo.AttributeTypes
	(
	Attribute
	) WITH( STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
DECLARE @v sql_variant 
SET @v = N'Unique Attribute Types'
EXECUTE sp_addextendedproperty N'MS_Description', @v, N'SCHEMA', N'dbo', N'TABLE', N'AttributeTypes', N'INDEX', N'IX_AttributeTypes_1'
GO
CREATE TRIGGER [dbo].[trig_AttributeTypes_D] on dbo.AttributeTypes AFTER DELETE as
begin
	-- ================================================================
	-- ==     Process the deletion of an AttributeType               ==
	-- ================================================================

	set nocount on

	-- Remove the counselor references
	delete	counselor_attributes
	from	counselor_attributes i
	inner join deleted d on d.oID = i.Attribute
	
	-- Remove the appointment references
	delete	AppointmentTypeAttributes
	from	AppointmentTypeAttributes i	
	inner join deleted d on d.oID = i.Attribute

	-- Do special processing on languages
	if exists(select * from deleted where [GROUPING] = 'LANGUAGE')
	begin
	
		-- Remove the client language
		update	clients
		set		[language] = null
		from	clients c
		inner join deleted d on c.language = d.oID and [GROUPING] = 'LANGUAGE'
		
		-- Remove the letter language
		update	letter_types
		set		[language] = null
		from	clients c
		inner join deleted d on c.language = d.oID and [GROUPING] = 'LANGUAGE'
	end
end
GO
DENY DELETE ON dbo.AttributeTypes TO www_role  AS dbo 
GO
DENY INSERT ON dbo.AttributeTypes TO www_role  AS dbo 
GO
DENY SELECT ON dbo.AttributeTypes TO www_role  AS dbo 
GO
DENY UPDATE ON dbo.AttributeTypes TO www_role  AS dbo 
GO
COMMIT
GO
