USE [DebtPlus]
GO
/****** Object:  Table [dbo].[disbursement_creditor_notes]    Script Date: 09/15/2014 13:13:26 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[disbursement_creditor_notes](
	[disbursement_creditor_note] [dbo].[typ_key] IDENTITY(1,1) NOT NULL,
	[note_type] [dbo].[typ_disbursement_note_type] NOT NULL,
	[client] [dbo].[typ_client] NOT NULL,
	[creditor] [dbo].[typ_creditor] NULL,
	[client_creditor] [dbo].[typ_key] NULL,
	[client_creditor_proposal] [dbo].[typ_key] NULL,
	[account_number] [varchar](50) NULL,
	[rpps_biller_id] [dbo].[typ_rpps_biller_id] NULL,
	[epay_biller_id] [varchar](14) NULL,
	[drop_reason] [dbo].[typ_key] NULL,
	[disbursement_register] [dbo].[typ_key] NULL,
	[disbursement_creditor_note_type] [dbo].[typ_key] NOT NULL,
	[note_text] [text] NULL,
	[note_amount] [money] NULL,
	[note_date] [datetime] NULL,
	[date_printed] [datetime] NULL,
	[created_by] [dbo].[typ_counselor] NOT NULL,
	[date_created] [datetime] NOT NULL,
 CONSTRAINT [PK_disbursement_creditor_notes] PRIMARY KEY CLUSTERED 
(
	[disbursement_creditor_note] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 90) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Primary key to the table. It is the note ID associated with this note.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'disbursement_creditor_notes', @level2type=N'COLUMN',@level2name=N'disbursement_creditor_note'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Type of the note. ''MS'' - miscelleanous, ''PR'' - proposal, etc.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'disbursement_creditor_notes', @level2type=N'COLUMN',@level2name=N'note_type'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Client ID associated with the note' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'disbursement_creditor_notes', @level2type=N'COLUMN',@level2name=N'client'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Creditor ID associated with the note' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'disbursement_creditor_notes', @level2type=N'COLUMN',@level2name=N'creditor'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Debt ID associated with the note' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'disbursement_creditor_notes', @level2type=N'COLUMN',@level2name=N'client_creditor'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'If proposal note, this is a pointer to the proposal record' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'disbursement_creditor_notes', @level2type=N'COLUMN',@level2name=N'client_creditor_proposal'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Account number associated with the note. ''MISSING'' means that there is no account and it is just a note to the creditor.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'disbursement_creditor_notes', @level2type=N'COLUMN',@level2name=N'account_number'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'If the creditor is on RPPS, this is the biller ID to be used in generating the notes.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'disbursement_creditor_notes', @level2type=N'COLUMN',@level2name=N'rpps_biller_id'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'If the creditor is on ePay, this is the biller ID associated with the creditor for the notes.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'disbursement_creditor_notes', @level2type=N'COLUMN',@level2name=N'epay_biller_id'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'If this is a drop message, this is a pointer to the drop reason.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'disbursement_creditor_notes', @level2type=N'COLUMN',@level2name=N'drop_reason'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'This is the disbursement register associated with the note. It is set when the paper note is printed.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'disbursement_creditor_notes', @level2type=N'COLUMN',@level2name=N'disbursement_register'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Note type' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'disbursement_creditor_notes', @level2type=N'COLUMN',@level2name=N'disbursement_creditor_note_type'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'This is the text for the note. It may be long for RPPS and ePay.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'disbursement_creditor_notes', @level2type=N'COLUMN',@level2name=N'note_text'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'If there is an amount, this is the amount associated with the note.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'disbursement_creditor_notes', @level2type=N'COLUMN',@level2name=N'note_amount'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'This is the date associated with the note. It is normally the date the note is created but may be changed by the user.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'disbursement_creditor_notes', @level2type=N'COLUMN',@level2name=N'note_date'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'This is the date that the note "is no longer pending". It is set when the note is put into a batch or is printed.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'disbursement_creditor_notes', @level2type=N'COLUMN',@level2name=N'date_printed'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'This is the name of the person who created the note.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'disbursement_creditor_notes', @level2type=N'COLUMN',@level2name=N'created_by'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'This is the date that the note was created.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'disbursement_creditor_notes', @level2type=N'COLUMN',@level2name=N'date_created'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'This table binds the disbursement_creditor_note_types to the disbursement and client information. It indicates that a client is to print a specific disbursement note to all of the creditors on a specific disbursement batch.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'disbursement_creditor_notes'
GO
