EXECUTE UPDATE_DROP_CONSTRAINTS 'Housing_ARM_hcs_ids'
GO
/* To prevent any potential data loss issues, you should review this script in detail before running it outside the context of the database designer.*/
BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION
GO
CREATE TABLE dbo.Tmp_Housing_ARM_hcs_ids
	(
	hcs_id dbo.typ_key NOT NULL,
	Description dbo.typ_description NOT NULL,
	Username varchar(255) NULL,
	Password varchar(255) NULL,
	counseling_amount money NOT NULL,
	faith_based bit NOT NULL,
	colonias bit NOT NULL,
	migrant_farm_worker bit NOT NULL,
	validation_date datetime NULL,
	[Default] bit NOT NULL,
	ActiveFlag bit NOT NULL,
	SendClients bit NOT NULL,
	SendWorkshops bit NOT NULL,
	ts timestamp NOT NULL
	)  ON [PRIMARY]
GO
GRANT DELETE ON dbo.Tmp_Housing_ARM_hcs_ids TO public  AS dbo
GO
GRANT INSERT ON dbo.Tmp_Housing_ARM_hcs_ids TO public  AS dbo
GO
GRANT SELECT ON dbo.Tmp_Housing_ARM_hcs_ids TO public  AS dbo
GO
GRANT UPDATE ON dbo.Tmp_Housing_ARM_hcs_ids TO public  AS dbo
GO
EXECUTE sp_bindefault N'dbo.default_money', N'dbo.Tmp_Housing_ARM_hcs_ids.counseling_amount'
GO
EXECUTE sp_bindefault N'dbo.default_false', N'dbo.Tmp_Housing_ARM_hcs_ids.faith_based'
GO
EXECUTE sp_bindefault N'dbo.default_false', N'dbo.Tmp_Housing_ARM_hcs_ids.colonias'
GO
EXECUTE sp_bindefault N'dbo.default_false', N'dbo.Tmp_Housing_ARM_hcs_ids.migrant_farm_worker'
GO
EXECUTE sp_bindefault N'dbo.default_false', N'dbo.Tmp_Housing_ARM_hcs_ids.[Default]'
GO
EXECUTE sp_bindefault N'dbo.default_true', N'dbo.Tmp_Housing_ARM_hcs_ids.ActiveFlag'
GO
EXECUTE sp_bindefault N'dbo.default_true', N'dbo.Tmp_Housing_ARM_hcs_ids.SendClients'
GO
EXECUTE sp_bindefault N'dbo.default_false', N'dbo.Tmp_Housing_ARM_hcs_ids.SendWorkshops'
GO
IF EXISTS(SELECT * FROM dbo.Housing_ARM_hcs_ids)
	 EXEC('INSERT INTO dbo.Tmp_Housing_ARM_hcs_ids (hcs_id, Description, Username, Password, counseling_amount, faith_based, colonias, migrant_farm_worker, validation_date, [Default], ActiveFlag, SendClients, SendWorkshops)
		SELECT hcs_id, Description, Username, Password, counseling_amount, faith_based, colonias, migrant_farm_worker, validation_date, [Default], ActiveFlag, SendClients, SendWorkshops FROM dbo.Housing_ARM_hcs_ids WITH (HOLDLOCK TABLOCKX)')
GO
DROP TABLE dbo.Housing_ARM_hcs_ids
GO
EXECUTE sp_rename N'dbo.Tmp_Housing_ARM_hcs_ids', N'Housing_ARM_hcs_ids', 'OBJECT' 
GO
ALTER TABLE dbo.Housing_ARM_hcs_ids ADD CONSTRAINT
	PK_housing_ARM_hcs_ids PRIMARY KEY CLUSTERED 
	(
	hcs_id
	) WITH( STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
DENY DELETE ON dbo.Housing_ARM_hcs_ids TO www_role  AS dbo 
GO
DENY INSERT ON dbo.Housing_ARM_hcs_ids TO www_role  AS dbo 
GO
DENY SELECT ON dbo.Housing_ARM_hcs_ids TO www_role  AS dbo 
GO
DENY UPDATE ON dbo.Housing_ARM_hcs_ids TO www_role  AS dbo 
GO
COMMIT
BEGIN TRANSACTION
GO
ALTER TABLE dbo.Housing_ARM_hcs_id_languages ADD CONSTRAINT
	FK_Housing_ARM_hcs_id_languages_Housing_ARM_hcs_ids FOREIGN KEY
	(
	hcs_id
	) REFERENCES dbo.Housing_ARM_hcs_ids
	(
	hcs_id
	) ON UPDATE  NO ACTION 
	 ON DELETE  CASCADE 
GO
COMMIT
BEGIN TRANSACTION
GO
ALTER TABLE dbo.Housing_ARM_hcs_id_AppointmentTypes ADD CONSTRAINT
	FK_Housing_ARM_hcs_id_AppointmentTypes_Housing_ARM_hcs_ids FOREIGN KEY
	(
	hcs_id
	) REFERENCES dbo.Housing_ARM_hcs_ids
	(
	hcs_id
	) ON UPDATE  NO ACTION 
	 ON DELETE  CASCADE 
GO
COMMIT
