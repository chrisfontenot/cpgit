﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DebtPlus.OCS.Domain;
using System.Data.Linq;
using DebtPlus.LINQ;
using DebtPlus.LINQ.BusinessLayer;
using System.Data.SqlTypes;
using System.Text.RegularExpressions;

namespace DebtPlus.Svc.OCS
{
    public class OCSDbMapper : IDisposable
    {
        LINQ.BusinessContext context;

        public OCSDbMapper(LINQ.BusinessContext ctx)
        {
            context = ctx;
        }

        public void refreshContext()
        {
            DebtPlus.LINQ.BusinessContext bc = new LINQ.BusinessContext();
            context = bc;
        }

        public OCS_QUEUE? getQueue(string queueCode)
        {
            OCS_QUEUE? toReturn = null;

            if (!String.IsNullOrWhiteSpace(queueCode))
            {
                toReturn = (OCS_QUEUE)Enum.Parse(typeof(OCS_QUEUE), queueCode, true);
            }

            return toReturn;
        }

        public IQueryable<OCSClient> GetOCSClient()
        {
            return (
                    from cl in context.view_OCS_Full_Clients
                    let tz2 = context.ZipCodeSearches.Where(z => z.ZIPCode.PadLeft(5, '0') == cl.ClientZip.PadLeft(5, '0')).FirstOrDefault()
                    join tzInfo in context.TimeZones on tz2.TimeZone equals tzInfo.Id into tzs2
                    from tz3 in tzs2.DefaultIfEmpty()
                    orderby cl.OCSId descending
                    select new OCSClient()
                    {
                        ID                    = cl.OCSId,
                        ClientID              = cl.ClientId,
                        Program               = (PROGRAM)cl.Program.Value,
                        ClaimedBy             = cl.ClaimedBy,
                        ClaimedDate           = cl.ClaimedDate,
                        PostModLastChanceDate = cl.PostModLastChanceDate,
                        PostModLastChanceFlag = cl.PostModLastChanceFlag,
                        Queue                 = getQueue(cl.QueueCode),
                        PreferredLanguage     = cl.PreferredLanguage.HasValue && cl.PreferredLanguage.Value > 0 ? (Person.LANGUAGE)cl.PreferredLanguage.Value : Person.LANGUAGE.English,
                        Servicer              = cl.Servicer,
                        LoanNo                = cl.ServicerNumber,
                        InvestorLoanNo        = cl.InvestorNumber,
                        StatusCode            = (STATUS_CODE)cl.StatusCode,
                        ContactAttemptCount   = cl.ContactAttempts,
                        FirstCounselDate      = cl.FirstCounselDate,
                        SecondCounselDate     = cl.SecondCounselDate,
                        ActiveFlag            = cl.ActiveFlag,
                        Archive               = cl.Archive,
                        IsDuplicate           = cl.IsDuplicate,
                        Address               = new Address()
                        {
                            street        = cl.ClientStreet,
                            city          = cl.ClientCity,
                            PostalCode    = cl.ClientZip,
                            StateProvince = (US_STATE?)Enum.Parse(typeof(US_STATE), cl.ClientState),
                            TzDescriptor  = cl.SearchTimezone,
                            GMTOffset     = tz3 == null || !tz3.GMTOffset.HasValue ? -5 : Convert.ToInt32(tz3.GMTOffset.Value)
                        },

                        UseHomeAddress  = cl.UseHomeAddress.HasValue == false || cl.UseHomeAddress.Value,
                        PropertyAddress = cl.UseHomeAddress.HasValue && cl.UseHomeAddress.Value == false ?
                            new Address()
                            {
                                street        = cl.PropertyStreet,
                                city          = cl.PropertyCity,
                                PostalCode    = cl.PropertyZip,
                                StateProvince = (US_STATE?)Enum.Parse(typeof(US_STATE), cl.PropertyState)
                            } :
                            new Address()
                            {
                                street        = cl.ClientStreet,
                                city          = cl.ClientCity,
                                PostalCode    = cl.ClientZip,
                                StateProvince = (US_STATE?)Enum.Parse(typeof(US_STATE), cl.ClientState),
                                TzDescriptor  = cl.SearchTimezone,
                                GMTOffset     = tz3 == null || !tz3.GMTOffset.HasValue ? -5 : Convert.ToInt32(tz3.GMTOffset.Value)
                            },
                        Person1 = cl.HasApplicant.HasValue == false || cl.HasApplicant.Value == false ?
                            null :
                            new Person()
                            {
                                FirstName    = cl.AppFName,
                                MiddleName   = cl.AppMName,
                                LastName     = cl.AppLName,
                                Salutation   = cl.AppPrefix.mapSalutation(),
                                Suffix       = cl.AppSuffix.mapSuffix(),
                                SSN          = cl.AppSSN,
                                EmailAddress = cl.AppEmail
                            },

                        Person2 = cl.HasCoApplicant.HasValue == false || cl.HasCoApplicant.Value == false ?
                            null :
                            new Person()
                            {
                                FirstName    = cl.CoAppFName,
                                MiddleName   = cl.CoAppMName,
                                LastName     = cl.CoAppLName,
                                Salutation   = cl.CoAppPrefix.mapSalutation(),
                                Suffix       = cl.CoAppSuffix.mapSuffix(),
                                SSN          = cl.CoAppSSN,
                                EmailAddress = cl.CoAppEmail
                            },
#pragma warning disable 618
                        ClientHomePhone = cl.HomePhone.map(PHONE_TYPE.ClientHome, cl.HomePhoneUserSelectedType),
                        ClientMsgPhone  = cl.MsgPhone.map(PHONE_TYPE.ClientMsg, cl.MsgPhoneUserSelectedType),
                        ApplicantCell   = cl.AppCellPhone.map(PHONE_TYPE.ApplicantCell, cl.AppCellPhoneUserSelectedType),
                        ApplicantWork   = cl.AppWorkPhone.map(PHONE_TYPE.ApplicantWork, cl.AppWorkPhoneUserSelectedType),
                        CoApplicantCell = cl.CoAppCellPhone.map(PHONE_TYPE.CoApplicantCell, cl.CoAppCellPhoneUserSelectedType),
                        CoApplicantWork = cl.CoAppWorkPhone.map(PHONE_TYPE.CoApplicantWork, cl.CoAppWorkPhoneUserSelectedType),
#pragma warning restore 618

                        Notes = new List<client_note>()
                    });
        }

        public List<OCS_ResultCode> GetResultCodeList()
        {
            return context.OCS_ResultCodes.ToList();
        }

        public System.Collections.Generic.List<LINQ.ocs_result_codes_reportResult> GetResultCodesReport(DateTime begin, DateTime end, PROGRAM? program)
        {
            end = end.Date.AddDays(1);

            return (
                from t in context.OCS_ContactAttempts
                where t.End >= begin && t.End < end && t.User != null && t.ResultCode != null
                group t by new { t.User, t.ResultCode }
                    into g
                    select new LINQ.ocs_result_codes_reportResult()
                    {
                        Outcome = g.Key.ResultCode,
                        User = g.Key.User,
                        Count = g.Count()
                    }).ToList();
        }

        public IQueryable<LINQ.OCS_UploadReport> GetUploadReports()
        {
            return context.OCS_UploadReports;
        }

        public IQueryable<LINQ.OCS_UploadRecord> GetUploadedRecords(PROGRAM program)
        {
            var prog = (int)program;

            return (from record in context.OCS_UploadRecords
                    where record.Program.HasValue && record.Program.Value == prog
                    select record).AsQueryable();
        }

        public OCSClient GetActiveClientById(int id)
        {
            return GetActiveClientById(new List<int> { id }).FirstOrDefault();
        }

        public List<OCSClient> GetActiveClientById(List<int> IDs)
        {
            var ocsList = GetOCSClient().Where(c => c.ActiveFlag == true && IDs.Contains(c.ClientID.Value));

            return ocsList.ToList();
        }

        public OCSClient GetByOCSId(int id)
        {
            return GetByOCSId(new List<int> { id }).FirstOrDefault();
        }

        public List<OCSClient> GetByOCSId(List<int> IDs)
        {
            var ocsList = GetOCSClient().Where(c => IDs.Contains(c.ID.Value));

            return ocsList.ToList();
        }

        public OCSClient GetProgramSpecificInfo(OCSClient client)
        {
            if (client.Program == PROGRAM.FreddieMacPostMod || client.Program == PROGRAM.FannieMaePostMod)
            {
                var rps = ProgramConfigService.getRightPartyContacts(client.Program).Select(rp => (int)rp);

                var LastRPContact = (from attempt in context.OCS_ContactAttempts
                                     where attempt.ClientId == client.ClientID.Value && attempt.ResultCode.HasValue && rps.Contains(attempt.ResultCode.Value)
                                     orderby attempt.Begin
                                     select attempt).FirstOrDefault();

                client.LastRightPartyContact = LastRPContact == null ? null : LastRPContact.Begin;

                return client;
            }
            else
            {
                return client;
            }
        }

        public System.Collections.Generic.List<DebtPlus.LINQ.rpt_ocs_status_by_dateResult> GetStatusByDate(DateTime? fromDate, DateTime? toDate, int Program)
        {
            return context.rpt_ocs_status_by_date(fromDate, toDate, Program);
        }

        private static Regex sanitizePhone = new Regex(@"\D");

        private void parsePhone(List<ContactAttempt.CallOutcome> list, PHONE_TYPE autoType, ref string area, ref string number, ref int? result, ref string userType)
        {
            var found = list.Where(p => (p.PhoneNumber.AutoAssignedType == autoType || p.PhoneNumber.AutoAssignedType == PHONE_TYPE.None) && p.IsAssigned == false).FirstOrDefault();
            if (found != null)
            {
                area = found.PhoneNumber.map(null).Acode;
                number = sanitizePhone.Replace(found.PhoneNumber.map(null).Number, "");
                userType = found.PhoneNumber.UserAssignedType.ToString();
                result = (int)found.Result.Value;
                found.IsAssigned = true;
            }
        }

        private String stringOrNull(string str)
        {
            if (String.IsNullOrWhiteSpace(str))
            {
                return null;
            }
            else
            {
                return str;
            }
        }

        /// <summary>
        /// Update an Active Client record
        /// </summary>
        /// <param name="client"></param>
        /// <returns></returns>
        public int UpdateActiveClient(OCSClient client)
        {

            int? toReturn = -1;

            //new notes, since the number of notes we need to insert is indeterminate, go through normal LINQ instead of stored proc
            //the only notes we will ever see here are new notes, therefore no filter is needed
            var newNotes = client.Notes.Try().Select(n => n.map(client.ClientID));
            context.client_notes.InsertAllOnSubmit(newNotes);
            context.SubmitChanges();

            if (client.CurrentAttempt != null && client.CurrentAttempt.IsClosed)
            {

                string p1Area = null; string p1Num = null; int? p1Res = null; string p1Type = null;
                string p2Area = null; string p2Num = null; int? p2Res = null; string p2Type = null;
                string p3Area = null; string p3Num = null; int? p3Res = null; string p3Type = null;
                string p4Area = null; string p4Num = null; int? p4Res = null; string p4Type = null;
                string p5Area = null; string p5Num = null; int? p5Res = null; string p5Type = null;
                string p6Area = null; string p6Num = null; int? p6Res = null; string p6Type = null;

                parsePhone(client.CurrentAttempt.Outcomes, PHONE_TYPE.ClientHome, ref p1Area, ref p1Num, ref p1Res, ref p1Type);
                parsePhone(client.CurrentAttempt.Outcomes, PHONE_TYPE.ClientMsg, ref p2Area, ref p2Num, ref p2Res, ref p2Type);
                parsePhone(client.CurrentAttempt.Outcomes, PHONE_TYPE.ApplicantCell, ref p3Area, ref p3Num, ref p3Res, ref p3Type);
                parsePhone(client.CurrentAttempt.Outcomes, PHONE_TYPE.ApplicantWork, ref p4Area, ref p4Num, ref p4Res, ref p4Type);
                parsePhone(client.CurrentAttempt.Outcomes, PHONE_TYPE.CoApplicantCell, ref p5Area, ref p5Num, ref p5Res, ref p5Type);
                parsePhone(client.CurrentAttempt.Outcomes, PHONE_TYPE.CoApplicantWork, ref p6Area, ref p6Num, ref p6Res, ref p6Type);

                context.ocs_create_contact_attempt(
                    client.ClientID.Value,
                    Environment.UserName,
                    (int)client.CurrentAttempt.Type.Value,
                    client.CurrentAttempt.Begin,
                    client.CurrentAttempt.End,
                    (int)client.CurrentAttempt.ResultCode,
                    client.CurrentAttempt.SpecificReason,
                    (int)client.StatusCode,
                    client.ContactAttemptCount,
                    client.ActiveFlag,
                    p1Area, p1Num, p1Res, p1Type,
                    p2Area, p2Num, p2Res, p2Type,
                    p3Area, p3Num, p3Res, p3Type,
                    p4Area, p4Num, p4Res, p4Type,
                    p5Area, p5Num, p5Res, p5Type,
                    p6Area, p6Num, p6Res, p6Type,
                    ref toReturn);

                string _q = null;
                if (client.Queue.HasValue) _q = client.Queue.Value.ToString();

                int? propState = null;
                if (client.PropertyAddress != null && client.PropertyAddress.StateProvince.HasValue) propState = (int)client.PropertyAddress.StateProvince.Value;

                String errorMsg = String.Empty;

                var p1 = stringOrNull(client.Person1.Salutation.HasValue ? client.Person1.Salutation.Value.ToString() : "");
                var p2 = stringOrNull(client.Person1.Suffix.HasValue ? client.Person1.Suffix.Value.ToString() : "");
                var p3 = stringOrNull(client.Person1.EmailAddress);
                var p4 = stringOrNull(client.Person1.SSN != null && client.Person1.SSN.Length == 9 ? client.Person1.SSN : "");
                var p5 = stringOrNull(client.Person2 != null && client.Person2.Salutation.HasValue ? client.Person2.Salutation.Value.ToString() : "");
                var p6 = stringOrNull(client.Person2 != null && client.Person2.Suffix.HasValue ? client.Person2.Suffix.Value.ToString() : "");
                var p7 = stringOrNull(client.Person2 != null ? client.Person2.EmailAddress : "");
                var p8 = stringOrNull(client.Person2 != null && client.Person2.SSN != null && client.Person2.SSN.Length == 9 ? client.Person2.SSN : "");
                var p9 = client.UseHomeAddress;
                var p10 = (client.PropertyAddress != null ? client.PropertyAddress.AddressLine1 ?? "" : "");
                var p11 = (client.PropertyAddress != null ? client.PropertyAddress.address_line_2 ?? "" : "");
                var p12 = (client.PropertyAddress != null ? client.PropertyAddress.city ?? "" : "");
                var p13 = propState;
                var p14 = stringOrNull(client.PropertyAddress != null ? client.PropertyAddress.PostalCode ?? "" : "");

                System.Threading.Tasks.Task t = System.Threading.Tasks.Task.Factory.StartNew(
                    () =>
                    {
                        bool finished = false;
                        int numTries = 0;
                        while (!finished)
                        {
                            numTries++;

                            using (var ctx = new LINQ.BusinessContext())
                            {
                                ctx.ocs_update_client(
                                    client.ClientID.Value,
                                    _q,
                                    (int)client.PreferredLanguage,
                                    stringOrNull(client.Person1.Salutation.HasValue ? client.Person1.Salutation.Value.ToString() : ""),
                                    stringOrNull(client.Person1.Suffix.HasValue ? client.Person1.Suffix.Value.ToString() : ""),
                                    stringOrNull(client.Person1.EmailAddress),
                                    stringOrNull(client.Person1.SSN != null && client.Person1.SSN.Length == 9 ? client.Person1.SSN : ""),
                                    stringOrNull(client.Person2 != null && client.Person2.Salutation.HasValue ? client.Person2.Salutation.Value.ToString() : ""),
                                    stringOrNull(client.Person2 != null && client.Person2.Suffix.HasValue ? client.Person2.Suffix.Value.ToString() : ""),
                                    stringOrNull(client.Person2 != null ? client.Person2.EmailAddress : ""),
                                    stringOrNull(client.Person2 != null && client.Person2.SSN != null && client.Person2.SSN.Length == 9 ? client.Person2.SSN : ""),
                                    client.UseHomeAddress,
                                    (client.PropertyAddress != null ? client.PropertyAddress.AddressLine1 ?? "" : ""),
                                    (client.PropertyAddress != null ? client.PropertyAddress.address_line_2 ?? "" : ""),
                                    (client.PropertyAddress != null ? client.PropertyAddress.city ?? "" : ""),
                                    propState,
                                    stringOrNull(client.PropertyAddress != null ? client.PropertyAddress.PostalCode ?? "" : ""),
                                    (client.FirstCounselDate == null || client.FirstCounselDate == DateTime.MinValue) ? null : client.FirstCounselDate,
                                    (client.SecondCounselDate == null || client.SecondCounselDate == DateTime.MinValue) ? null : client.SecondCounselDate,
                                    (client.LastRpcDate == null || client.LastRpcDate == DateTime.MinValue) ? null : client.LastRpcDate,
                                    (client.PostModLastChanceDate == null || client.PostModLastChanceDate == DateTime.MinValue) ? null : client.PostModLastChanceDate,
                                    (client.PostModLastChanceFlag == null) ? null : client.PostModLastChanceFlag,
                                    ref errorMsg);

                                if (numTries >= 60 && errorMsg != String.Empty)
                                {
                                    finished = true;
                                    var logNote = new List<client_note>
                                {
                                    new client_note{
                                        client = client.ClientID.Value,
                                        subject = "SYSTEM LOG - SAVE ERROR",
                                        note = String.Format( 
@"Failed to save client after 60 retries, aborting save operation.
Params:
{0},
{1},
{2},
{3},
{4},
{5},
{6},
{7},
{8},
{9},
{10},
{11},
{12},
{13},
{14},
{15},
{16}",
client.ClientID.Value,
_q,
(int)client.PreferredLanguage,
stringOrNull(client.Person1.Salutation.HasValue ? client.Person1.Salutation.Value.ToString() : ""),
stringOrNull(client.Person1.Suffix.HasValue ? client.Person1.Suffix.Value.ToString() : ""),
stringOrNull(client.Person1.EmailAddress),
stringOrNull(client.Person1.SSN != null && client.Person1.SSN.Length == 9 ? client.Person1.SSN : ""),
stringOrNull(client.Person2 != null && client.Person2.Salutation.HasValue ? client.Person2.Salutation.Value.ToString() : ""),
stringOrNull(client.Person2 != null && client.Person2.Suffix.HasValue ? client.Person2.Suffix.Value.ToString() : ""),
stringOrNull(client.Person2 != null ? client.Person2.EmailAddress : ""),
stringOrNull(client.Person2 != null && client.Person2.SSN != null && client.Person2.SSN.Length == 9 ? client.Person2.SSN : ""),
client.UseHomeAddress,
(client.PropertyAddress != null ? client.PropertyAddress.AddressLine1 ?? "" : ""),
(client.PropertyAddress != null ? client.PropertyAddress.address_line_2 ?? "" : ""),
(client.PropertyAddress != null ? client.PropertyAddress.city ?? "" : ""),
propState,
stringOrNull(client.PropertyAddress != null ? client.PropertyAddress.PostalCode ?? "" : "")),
                                        dont_edit = true,
                                        dont_print = true,
                                        dont_delete = true                                        
                                    }
                                };
                                    ctx.client_notes.InsertAllOnSubmit(logNote);
                                    ctx.SubmitChanges();
                                    throw new Exception(String.Format("We're sorry, but we were unable to update the client record #{0}.  You may wish to navigate back in the queue and try to save again, if possible.", client.ClientID.Value));
                                }
                                else
                                {
                                    finished = errorMsg == String.Empty;
                                    errorMsg = String.Empty;
                                    System.Threading.Thread.Sleep(5000);
                                }
                            }
                        }

                    });

                if (errorMsg != String.Empty)
                {
                    throw new Exception(errorMsg);
                }
            }

            return toReturn.Value;
        }

        public bool claimRecord(int ocsClientId, string username)
        {
            bool? toReturn = false;
            context.ocs_claim_client(ocsClientId, username, ref toReturn);

            return toReturn.HasValue ? toReturn.Value : false;
        }

        public int? getNextSort(OCSSortArgs sortArgs)
        {
            int? prefLang = null;
            if (sortArgs.PreferredLanguage.HasValue)
            {
                prefLang = (int)sortArgs.PreferredLanguage.Value;
            }
            string stateAbbrev = null;
            if (sortArgs.State.HasValue)
            {
                stateAbbrev = sortArgs.State.Value.ToString();
            }
            int? status = null;
            if (sortArgs.StatusCode.HasValue)
            {
                status = (int)sortArgs.StatusCode.Value;
            }
            string _queue = null;
            if (sortArgs.Queue.HasValue)
            {
                _queue = sortArgs.Queue.ToString();
            }
            int lastChanceFlag = 0;
            lastChanceFlag = sortArgs.LastChanceFlag;

            int? toReturn = null;

            var found = context.ocs_get_next_sort(
               (int)sortArgs.Program,
               sortArgs.ActiveFlag,
               prefLang,
               status,
               sortArgs.MinContacts,
               sortArgs.MaxContacts,
               stateAbbrev,
               _queue,
               lastChanceFlag,
               sortArgs.CounseledState,
               sortArgs.CounseledFromDate,
               sortArgs.CounseledToDate,
               sortArgs.AllowedTZs.Exists(tz => tz == TIMEZONE.Atlantic),
               sortArgs.AllowedTZs.Exists(tz => tz == TIMEZONE.Eastern),
               sortArgs.AllowedTZs.Exists(tz => tz == TIMEZONE.Central),
               sortArgs.AllowedTZs.Exists(tz => tz == TIMEZONE.Mountain),
               sortArgs.AllowedTZs.Exists(tz => tz == TIMEZONE.Pacific),
               sortArgs.AllowedTZs.Exists(tz => tz == TIMEZONE.Alaska),
               sortArgs.AllowedTZs.Exists(tz => tz == TIMEZONE.Hawaii),
               sortArgs.AllowedTZs.Exists(tz => tz == TIMEZONE.Chamorro),
               sortArgs.AllowedTZs.Exists(tz => tz == TIMEZONE.Indeterminate)
           ).FirstOrDefault();

            if (found != null) toReturn = found.OCSClientId;

            return toReturn;
        }

        public OCSClient claimNextSort(SortProvider provider, OCSSortArgs sortArgs)
        {

            var username = Environment.UserName;

            provider.FillAllowedTimeZones(sortArgs, GetDate());

            var first = getNextSort(sortArgs);

            bool claimed = false;
            while (first.HasValue && !claimed)
            {
                claimed = claimRecord(first.Value, username);
                if (!claimed)
                {
                    first = getNextSort(sortArgs);
                }
            }
            if (claimed)
            {
                var toReturn = GetOCSClient().Where(c => c.ID.Value == first.Value).First();
                return toReturn;
            }

            return null;
        }

        public LINQ.view_OCS_Account_Tab GetClientAccountTab(int clientId, int ocsId, PROGRAM program)
        {
            var toReturn = context.view_OCS_Account_Tabs.Where(cl => cl.OcsId == ocsId).FirstOrDefault();

            var rps = ProgramConfigService.getRightPartyContacts(program).Select(rp => rp.toInt());

            var FirstRPContact = (from attempt in context.OCS_ContactAttempts
                                  where attempt.OcsId.HasValue &&
                                        attempt.OcsId.Value == ocsId &&
                                        attempt.ResultCode.HasValue &&
                                        rps.Contains(attempt.ResultCode.Value)
                                  orderby attempt.Begin
                                  select attempt).FirstOrDefault();

            toReturn.FirstRPContactDate = FirstRPContact == null ? null : FirstRPContact.Begin;

            return toReturn;
        }

        public int archiveOlderThan(PROGRAM program, DateTime dateTime)
        {
            bool? success = false;
            int? numberArchived = null;
            context.ocs_archive_older_than(Environment.UserName, (int)program, dateTime, ref numberArchived, ref success);

            if (!success.Value)
            {
                throw new Exception("Something went wrong during archive operation, please try again.");
            }

            return numberArchived.HasValue ? numberArchived.Value : -1;
        }

        public List<LINQ.client_note> GetClientNotes(int clientId)
        {
            return context.client_notes.Where(n => n.client == clientId).ToList();
        }

        public void AddNote(int clientId, string subject, string body)
        {

            var note = new LINQ.client_note
            {
                client = clientId,
                dont_delete = true,
                dont_edit = true,
                dont_print = true,
                type = 3,
                subject = subject,
                note = body
            };

            context.client_notes.InsertOnSubmit(note);
            context.SubmitChanges();
        }

        public string GetCounselorName(int counselorId)
        {
            var name = (from c in context.counselors
                        where c.Id == counselorId
                        select c.Name).FirstOrDefault();
            return (name == null ? "" : name.First + " " + name.Last);
        }
        public string GetAppointmentType(int apptType)
        {
            return (from a in context.appt_types
                    where a.Id == apptType
                    select a.appt_name).FirstOrDefault();
        }
        public string GetOfficeName(int office)
        {
            return (from o in context.offices
                    where o.Id == office
                    select o.name).FirstOrDefault();
        }

        public IQueryable<LINQ.view_OCS_Sort> GetSortView()
        {
            return context.view_OCS_Sorts;
        }

        public IQueryable<LINQ.view_OCS_Active_Client_Summary> GetActiveClientSummaries()
        {
            return context.view_OCS_Active_Client_Summaries;
        }

        public IQueryable<LINQ.OCS_ArchiveReport> GetArchiveReports()
        {
            return context.OCS_ArchiveReports;
        }

        public IQueryable<LINQ.view_OCS_PostMod_Records_Created> GetPostModsCreated()
        {
            return context.view_OCS_PostMod_Records_Createds;
        }

        private class getdate_result
        {
            public DateTime date { get; set; }
        }

        public DateTime GetDate()
        {
            var r = context.ExecuteQuery<getdate_result>(@"select getdate() as date").FirstOrDefault();
            return r.date;
        }

        public bool unarchiveAndActivate(int p)
        {
            bool? success = null;
            context.ocs_unarchive_record(p, ref success);

            return success ?? false;
        }

        public int insertBrandNew(
            PROGRAM program,
            bool Archive,
            bool ActiveFlag,
            bool IsDuplicate,
            int UploadRecordId,
            Guid UploadAttempt,
            string FMAC,

            string Servicer,
            string ServicerNo,

            DateTime LastChanceList,
            DateTime ActualSaleDate,
            DateTime DueDate,

            string QueueCode,

            string Street,
            string City,
            int State,
            string Zipcode,

            bool HasPerson1,
            string FirstName1,
            string LastName1,
            string SSN1,

            bool HasPerson2,
            string FirstName2,
            string LastName2,
            string SSN2,

            string p1Area, string p1Num,
            string p2Area, string p2Num,
            string p3Area, string p3Num,
            string p4Area, string p4Num,
            string p5Area, string p5Num,
            string p6Area, string p6Num
            )
        {

            int? inserted = null;
            DateTime? _LastChanceList = null;
            DateTime? _ActualSaleDate = null;
            DateTime? _DueDate = null;

            if (LastChanceList > SqlDateTime.MinValue.Value)
            {
                _LastChanceList = LastChanceList;
            }
            if (ActualSaleDate > SqlDateTime.MinValue.Value)
            {
                _ActualSaleDate = ActualSaleDate;
            }
            if (DueDate > SqlDateTime.MinValue.Value)
            {
                _DueDate = DueDate;
            }


            context.ocs_upload_brand_new_client(
                (int)program,
                Archive,
                ActiveFlag,
                IsDuplicate,
                UploadRecordId,
                UploadAttempt.ToString(),
                FMAC,

                Servicer,
                ServicerNo,

                _LastChanceList,
                _ActualSaleDate,
                _DueDate,

                QueueCode,

                Street,
                City,
                State,
                Zipcode,

                HasPerson1,
                FirstName1,
                LastName1,
                SSN1,

                HasPerson2,
                FirstName2,
                LastName2,
                SSN2,

                p1Area, p1Num,
                p2Area, p2Num,
                p3Area, p3Num,
                p4Area, p4Num,
                p5Area, p5Num,
                p6Area, p6Num,

                ref inserted);

            return inserted.HasValue ? inserted.Value : -1;
        }

        public int insertNewDuplicate(
            int program,
            bool archive,
            bool activeflag,
            bool isduplicate,
            int clientid,
            int uploadrecordid,
            Guid attemptId,

            DateTime LastChanceList,
            DateTime ActualSaleDate,
            DateTime DueDate,

            string QueueCode,

            string p1Area, string p1Num,
            string p2Area, string p2Num,
            string p3Area, string p3Num,
            string p4Area, string p4Num,
            string p5Area, string p5Num,
            string p6Area, string p6Num,

            string servicerName,

            string zipcode)
        {
            int? inserted = null;

            DateTime? _LastChanceList = null;
            DateTime? _ActualSaleDate = null;
            DateTime? _DueDate = null;

            if (LastChanceList > SqlDateTime.MinValue.Value)
            {
                _LastChanceList = LastChanceList;
            }
            if (ActualSaleDate > SqlDateTime.MinValue.Value)
            {
                _ActualSaleDate = ActualSaleDate;
            }
            if (DueDate > SqlDateTime.MinValue.Value)
            {
                _DueDate = DueDate;
            }

            context.ocs_upload_duplicate_client(
                program,
                archive,
                activeflag,
                isduplicate,
                clientid,
                uploadrecordid,
                attemptId.ToString(),

                _LastChanceList,
                _ActualSaleDate,
                _DueDate,

                QueueCode,//queueCode here

                p1Area, p1Num,//home
                p2Area, p2Num,//msg
                p3Area, p3Num,//app cell
                p4Area, p4Num,//app work
                p5Area, p5Num,//coapp cell
                p6Area, p6Num,//coapp work

                servicerName,

                zipcode,

                ref inserted);

            return clientid;
        }

        public IQueryable<LINQ.view_onewest_client_creation> GetOneWestClientCreationReport()
        {
            return context.view_onewest_client_creations;
        }

        public IQueryable<LINQ.view_onewest_active_flag_compliance> GetOneWestActiveFlagComplianceReport()
        {
            return context.view_onewest_active_flag_compliances;
        }

        public IQueryable<LINQ.view_usbank_client_creation> GetUSBankClientCreationReport()
        {
            return context.view_usbank_client_creations;
        }

        public IQueryable<LINQ.view_usbank_active_flag_compliance> GetUSBankActiveFlagComplianceReport()
        {
            return context.view_usbank_active_flag_compliances;
        }

        protected virtual void Dispose(bool disposing)
        {
            if (context != null)
            {
                context.Dispose();
            }
            context = null;
        }

        private bool disposed = false;
        public void Dispose()
        {
            try
            {
                if (!disposed)
                {
                    Dispose(true);
                }
            }
            finally
            {
                GC.SuppressFinalize(this);
            }
        }

        public IQueryable<LINQ.view_nationstar_client_creation> GetNationStarClientCreationReport()
        {
            return context.view_nationstar_client_creations;
        }

        public IQueryable<LINQ.view_nationstar_active_flag_compliance> GetNationStarActiveFlagComplianceReport()
        {
            return context.view_nationstar_active_flag_compliances;
        }

        public List<string> GetHECMPartners()
        {
            return context.OCS_UploadRecords.Select(h => h.Servicer).Distinct().ToList();
        }

        public IQueryable<LINQ.view_OCS_HECM_FileListing> GetHECMFileListingReport()
        {
            return context.view_OCS_HECM_FileListings;
        }


        public int insertBrandNewFMAE
            (
             PROGRAM program,
            bool Archive,
            bool ActiveFlag,
            bool IsDuplicate,
            int UploadRecordId,
            Guid UploadAttempt,
            Person.LANGUAGE Preferredlanguage,

            string InvestorNumber,
            string ServicerName,
            string ServicerLoanNumber,

            string Street,
            string AddressLine2,
            string City,
            int State,
            string Zipcode,

            string MailingStreet,
            string MailingAddressLine2,
            string MailingCity,
            int MailingState,
            string MailingZipcode,

            bool HasPerson1,
            string FirstName1,
            string MiddleName1,
            string LastName1,
            string SSN1,
            string Email1,

            bool HasPerson2,
            string FirstName2,
            string MiddleName2,
            string LastName2,
            string Email2,

            string p1Area, string p1Num,
            string p2Area, string p2Num,
            string p3Area, string p3Num,
            string p4Area, string p4Num,
            string p5Area, string p5Num,
            string p6Area, string p6Num,

            string cop1Area, string cop1Num,
            string cop2Area, string cop2Num,
            string cop3Area, string cop3Num,
            string cop4Area, string cop4Num,
            string cop5Area, string cop5Num,
            string cop6Area, string cop6Num,


            string ACH_Flag,
            string Agency_Name,
            string Backlog_Mod_Flag,
            string Mod_Conversion_Date,
            string UPB,
            string Reason_For_Default,
            string Comment_Description,
            string Trial_Mod,
            string Trial_Mod_Description,
            string Trial_Mod_Payment_Amount,
            string TrialPaymentsReceivedAmount,
            string TrialPaymentsReceivedCount,
            string Last_Payment_Applied_Amount,
            string Workout_Type

            )
        {
            int? inserted = null;

            context.ocs_upload_NEW_client_FMAE(
                (int)program,
                Archive,
                ActiveFlag,
                IsDuplicate,
                UploadRecordId,
                UploadAttempt.ToString(),
                (int)Preferredlanguage,

                InvestorNumber,
                ServicerName,
                ServicerLoanNumber,

                Street,
                AddressLine2,
                City,
                State,
                Zipcode,

                MailingStreet,
                MailingAddressLine2,
                MailingCity,
                MailingState,
                MailingZipcode,

                HasPerson1,
                FirstName1,
                MiddleName1,
                LastName1,
                SSN1,
                Email1,

                HasPerson2,
                FirstName2,
                MiddleName2,
                LastName2,
                Email2,

                p1Area, p1Num,
                p2Area, p2Num,
                p3Area, p3Num,
                p4Area, p4Num,
                p5Area, p5Num,
                p6Area, p6Num,

                cop1Area, cop1Num,
                cop2Area, cop2Num,
                cop3Area, cop3Num,
                cop4Area, cop4Num,
                cop5Area, cop5Num,
                cop6Area, cop6Num

                , ACH_Flag
                , Agency_Name
                , Backlog_Mod_Flag
                , Mod_Conversion_Date
                , UPB
                , Reason_For_Default
                , Comment_Description
                , Trial_Mod
                , Trial_Mod_Description
                , Trial_Mod_Payment_Amount
                , TrialPaymentsReceivedAmount
                , TrialPaymentsReceivedCount
                , Last_Payment_Applied_Amount
                , Workout_Type,


                ref inserted);

            return inserted.HasValue ? inserted.Value : -1;
        }
        //  }


        /// <remarks/>
        [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Xml", "4.0.30319.17929")]
        [System.SerializableAttribute()]
        [System.Diagnostics.DebuggerStepThroughAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
        public partial class ArrayOfOCSSearch : object, System.ComponentModel.INotifyPropertyChanged
        {

            private int ID_Field;

            private int ClientID_Field;

            private bool ActiveFlag_Field;

            private bool Archive_Field;

            private string BatchName_Field;

            private string InvestorNo_Field;

            private string StreetAddress_Field;

            private string City_Field;

            private string StateAbbreviation_Field;

            private string ZipCode_Field;

            private string AppSSN_Field;

            private string Program_Field;

            private string AppFirstName_Field;

            private string AppLastName_Field;

            private string Phone1_Field;

            private string Phone2_Field;

            private string Phone3_Field;

            private string Phone4_Field;

            private string Phone5_Field;

            private string Servicer_Field;

            private string ServicerLoanNumber_Field;

            private string ServicerID_Field;

            /// <remarks/>
            [System.Xml.Serialization.XmlElementAttribute(Form = System.Xml.Schema.XmlSchemaForm.Unqualified, Order = 0)]
            public Int32 ID
            {
                get
                {
                    return this.ID_Field;
                }
                set
                {
                    this.ID_Field = value;
                    this.RaisePropertyChanged("ID");
                }
            }

            /// <remarks/>
            [System.Xml.Serialization.XmlElementAttribute(Form = System.Xml.Schema.XmlSchemaForm.Unqualified, Order = 1)]
            public int ClientID
            {
                get
                {
                    return this.ClientID_Field;
                }
                set
                {
                    this.ClientID_Field = value;
                    this.RaisePropertyChanged("ClientID");
                }
            }

            /// <remarks/>
            [System.Xml.Serialization.XmlElementAttribute(Form = System.Xml.Schema.XmlSchemaForm.Unqualified, Order = 2)]
            public bool ActiveFlag
            {
                get
                {
                    return this.ActiveFlag_Field;
                }
                set
                {
                    this.ActiveFlag_Field = value;
                    this.RaisePropertyChanged("ActiveFlag");
                }
            }

            /// <remarks/>
            [System.Xml.Serialization.XmlElementAttribute(Form = System.Xml.Schema.XmlSchemaForm.Unqualified, Order = 3)]
            public bool Archive
            {
                get
                {
                    return this.Archive_Field;
                }
                set
                {
                    this.Archive_Field = value;
                    this.RaisePropertyChanged("Archive");
                }
            }

            /// <remarks/>
            [System.Xml.Serialization.XmlElementAttribute(Form = System.Xml.Schema.XmlSchemaForm.Unqualified, Order = 4)]
            public string BatchName
            {
                get
                {
                    return this.BatchName_Field;
                }
                set
                {
                    this.BatchName_Field = value;
                    this.RaisePropertyChanged("BatchName");
                }
            }

            /// <remarks/>
            [System.Xml.Serialization.XmlElementAttribute(Form = System.Xml.Schema.XmlSchemaForm.Unqualified, Order = 5)]
            public string InvestorNo
            {
                get
                {
                    return this.InvestorNo_Field;
                }
                set
                {
                    this.InvestorNo_Field = value.ToString();
                    this.RaisePropertyChanged("InvestorNo");
                }
            }

            /// <remarks/>
            [System.Xml.Serialization.XmlElementAttribute(Form = System.Xml.Schema.XmlSchemaForm.Unqualified, Order = 6)]
            public string StreetAddress
            {
                get
                {
                    return this.StreetAddress_Field;
                }
                set
                {
                    this.StreetAddress_Field = value;
                    this.RaisePropertyChanged("StreetAddress");
                }
            }

            /// <remarks/>
            [System.Xml.Serialization.XmlElementAttribute(Form = System.Xml.Schema.XmlSchemaForm.Unqualified, Order = 7)]
            public string City
            {
                get
                {
                    return this.City_Field;
                }
                set
                {
                    this.City_Field = value;
                    this.RaisePropertyChanged("City");
                }
            }

            /// <remarks/>
            [System.Xml.Serialization.XmlElementAttribute(Form = System.Xml.Schema.XmlSchemaForm.Unqualified, Order = 8)]
            public string StateAbbreviation
            {
                get
                {
                    return this.StateAbbreviation_Field;
                }
                set
                {
                    this.StateAbbreviation_Field = value;
                    this.RaisePropertyChanged("StateAbbreviation");
                }
            }

            /// <remarks/>
            [System.Xml.Serialization.XmlElementAttribute(Form = System.Xml.Schema.XmlSchemaForm.Unqualified, Order = 9)]
            public string ZipCode
            {
                get
                {
                    return this.ZipCode_Field;
                }
                set
                {
                    this.ZipCode_Field = value;
                    this.RaisePropertyChanged("ZipCode");
                }
            }

            /// <remarks/>
            [System.Xml.Serialization.XmlElementAttribute(Form = System.Xml.Schema.XmlSchemaForm.Unqualified, Order = 10)]
            public string AppSSN
            {
                get
                {
                    return this.AppSSN_Field;
                }
                set
                {
                    this.AppSSN_Field = value;
                    this.RaisePropertyChanged("AppSSN");
                }
            }

            /// <remarks/>
            [System.Xml.Serialization.XmlElementAttribute(Form = System.Xml.Schema.XmlSchemaForm.Unqualified, Order = 11)]
            public string Program
            {
                get
                {
                    return this.Program_Field;
                }
                set
                {
                    this.Program_Field = value;
                    this.RaisePropertyChanged("Program");
                }
            }

            /// <remarks/>
            [System.Xml.Serialization.XmlElementAttribute(Form = System.Xml.Schema.XmlSchemaForm.Unqualified, Order = 12)]
            public string AppFirstName
            {
                get
                {
                    return this.AppFirstName_Field;
                }
                set
                {
                    this.AppFirstName_Field = value;
                    this.RaisePropertyChanged("AppFirstName");
                }
            }

            /// <remarks/>
            [System.Xml.Serialization.XmlElementAttribute(Form = System.Xml.Schema.XmlSchemaForm.Unqualified, Order = 13)]
            public string AppLastName
            {
                get
                {
                    return this.AppLastName_Field;
                }
                set
                {
                    this.AppLastName_Field = value;
                    this.RaisePropertyChanged("AppLastName");
                }
            }

            /// <remarks/>
            [System.Xml.Serialization.XmlElementAttribute(Form = System.Xml.Schema.XmlSchemaForm.Unqualified, Order = 14)]
            public string Phone1
            {
                get
                {
                    return this.Phone1_Field;
                }
                set
                {
                    this.Phone1_Field = value;
                    this.RaisePropertyChanged("Phone1");
                }
            }

            /// <remarks/>
            [System.Xml.Serialization.XmlElementAttribute(Form = System.Xml.Schema.XmlSchemaForm.Unqualified, Order = 15)]
            public string Phone2
            {
                get
                {
                    return this.Phone2_Field;
                }
                set
                {
                    this.Phone2_Field = value;
                    this.RaisePropertyChanged("Phone2");
                }
            }

            /// <remarks/>
            [System.Xml.Serialization.XmlElementAttribute(Form = System.Xml.Schema.XmlSchemaForm.Unqualified, Order = 16)]
            public string Phone3
            {
                get
                {
                    return this.Phone3_Field;
                }
                set
                {
                    this.Phone3_Field = value;
                    this.RaisePropertyChanged("Phone3");
                }
            }

            /// <remarks/>
            [System.Xml.Serialization.XmlElementAttribute(Form = System.Xml.Schema.XmlSchemaForm.Unqualified, Order = 17)]
            public string Phone4
            {
                get
                {
                    return this.Phone4_Field;
                }
                set
                {
                    this.Phone4_Field = value;
                    this.RaisePropertyChanged("Phone4");
                }
            }

            /// <remarks/>
            [System.Xml.Serialization.XmlElementAttribute(Form = System.Xml.Schema.XmlSchemaForm.Unqualified, Order = 18)]
            public string Phone5
            {
                get
                {
                    return this.Phone5_Field;
                }
                set
                {
                    this.Phone5_Field = value;
                    this.RaisePropertyChanged("Phone5");
                }
            }

            /// <remarks/>
            [System.Xml.Serialization.XmlElementAttribute(Form = System.Xml.Schema.XmlSchemaForm.Unqualified, Order = 19)]
            public string Servicer
            {
                get
                {
                    return this.Servicer_Field;
                }
                set
                {
                    this.Servicer_Field = value;
                    this.RaisePropertyChanged("Servicer");
                }
            }

            /// <remarks/>
            [System.Xml.Serialization.XmlElementAttribute(Form = System.Xml.Schema.XmlSchemaForm.Unqualified, Order = 20)]
            public string ServicerLoanNum
            {
                get
                {
                    return this.ServicerLoanNumber_Field;
                }
                set
                {
                    this.ServicerLoanNumber_Field = value;
                    this.RaisePropertyChanged("ServicerLoanNum");
                }
            }

            /// <remarks/>
            [System.Xml.Serialization.XmlElementAttribute(Form = System.Xml.Schema.XmlSchemaForm.Unqualified, Order = 21)]
            public string ServicerID
            {
                get
                {
                    return this.ServicerID_Field;
                }
                set
                {
                    this.ServicerID_Field = value;
                    this.RaisePropertyChanged("ServicerID");
                }
            }

            public int RowCount
            {
                get
                {
                    return this.RowCount;
                }
            }

            public event System.ComponentModel.PropertyChangedEventHandler PropertyChanged;

            protected void RaisePropertyChanged(string propertyName)
            {
                System.ComponentModel.PropertyChangedEventHandler propertyChanged = this.PropertyChanged;
                if ((propertyChanged != null))
                {
                    propertyChanged(this, new System.ComponentModel.PropertyChangedEventArgs(propertyName));
                }
            }
        }

        public List<LINQ.ocs_client_SearchResult> GetOCSSearch(string _lh, string _op, string rh)
        {
            return context.ocs_client_Search(_lh, _op, rh).ToList();

        }        
    }
}
