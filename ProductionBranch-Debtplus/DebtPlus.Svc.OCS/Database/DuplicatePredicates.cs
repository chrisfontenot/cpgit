﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DebtPlus.OCS.Domain;

namespace DebtPlus.OCS
{
    public static class DuplicatePredicates
    {
        /// <summary>
        /// 
        /// </summary>
        /// <see cref=""/>
        /// <param name="program"></param>
        /// <param name="investorNumbers"></param>
        /// <returns></returns>
        public static Func<LINQ.OCS_Client, bool> GetDupPredicate(List<string> investorNumbers) {
                       
            //strip leading zeros for duplicate detection
            //return cl => cl.Program.HasValue && cl.Program == progInt && investorNumbers.Contains(cl.InvestorNumber.Trim().TrimStart('0'));
			return cl => cl.Program.HasValue &&
                  (cl.Program == (int)PROGRAM.FreddieMacEI || 
                    cl.Program == (int)PROGRAM.FreddieMac180 || 
                    cl.Program == (int)PROGRAM.FreddieMac720 || 
                    cl.Program == (int)PROGRAM.FreddieMacPostMod || 
                    cl.Program == (int)PROGRAM.FannieMaePostMod)  && 
                  investorNumbers.Contains(cl.InvestorNumber.Trim().TrimStart('0'));

            /*
            var fmacs = new List<int> { (int)PROGRAM.FreddieMacEI, (int)PROGRAM.FreddieMac180 };
            //for freddie mac, allow the same number to "flow" through different programs
            Func<LINQ.OCS_Client, bool> fmacPred = cl => cl.Program.HasValue && fmacs.Contains(cl.Program.Value) && investorNumbers.Contains(cl.InvestorNumber.Trim());

            switch (program) { 
                case PROGRAM.FreddieMacEI:
                    return fmacPred;
                case PROGRAM.FreddieMac180:
                    return fmacPred;
                default: //if program and loan number match, then these are duplicates
                    return cl => cl.Program.HasValue && cl.Program == progInt && investorNumbers.Contains(cl.InvestorNumber);
            }
            */
        }
    }
}
