﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DebtPlus.OCS.Domain;

namespace DebtPlus.Svc.OCS
{
    public class Reporter : IDisposable
    {
#region Dispose() logic
        protected virtual void Dispose(bool Disposing)
        {
            if (Disposing)
            {
                if (repository != null)
                {
                    repository.Dispose();
                }
            }
            repository = null;
        }

        bool disposed = false;
        public void Dispose()
        {
            try
            {
                if (!disposed)
                {
                    disposed = true;
                    Dispose(true);
                }
            }
            finally
            {
                GC.SuppressFinalize(this);
            }
        }
#endregion

        // This is the base date for Excel. All dates must be greater than this value.
        static DateTime dateMarker = new DateTime(1899, 12, 31);

        // Excel cells with this sequence at the start are always treated as a text string.
        static string stringField = "'";

        private OCSDbMapper repository;

        public Reporter(OCSDbMapper repo)
        {
            repository = repo;
        }

        /// <summary>
        /// Convert the input row from a number to the value acceptable by Excel
        /// </summary>
        protected static string ExcelRow(int rowNumber)
        {
            if (rowNumber < 1)
            {
                return "1";
            }
            return rowNumber.ToString();
        }

        /// <summary>
        /// Convert the input column from a number to a value acceptable by excel
        /// </summary>
        protected static string ExcelColumn(int columnNumber)
        {
            // Invalid rows are in the first position
            columnNumber -= 1;
            if (columnNumber < 0)
            {
                return "A";
            }

            // Values that are from A ... Z are standard
            if (columnNumber < 26)
            {
                return Convert.ToChar(columnNumber + 0x41).ToString();  // 0x41 = "A"
            }
            columnNumber -= 26;

            // Values that are from AA ... ZZ are more complicated
            if (columnNumber < 676)
            {
                int remainder;
                int quotient = System.Math.DivRem(columnNumber, 26, out remainder);
                return ExcelColumn(quotient + 1) + ExcelColumn(remainder + 1);
            }

            // We don't go beyond ZZ for now.
            return "ZZ";
        }

        /// <summary>
        /// Convert the cell position to a valid cell location for Excel
        /// </summary>
        protected static string ExcelCell(int rowNumber, int columnNumber)
        {
            return ExcelColumn(columnNumber) + ExcelRow(rowNumber);
        }

        protected static object ExcelDateValue(DateTime? dateItem)
        {
            if (!dateItem.HasValue)
            {
                return System.Reflection.Missing.Value;
            }

            // Convert the date to the number of days since the marker
            return (dateItem.Value.Date.Subtract(dateMarker)).TotalDays + 1.0D;
        }

        public void GetLoadResultsExcel(DateTime start, DateTime end, PROGRAM? program)
        {
            // Get the data for the report
            var q = repository.GetUploadReports().Where(report => report.UploadDate.HasValue && report.UploadDate.Value.Date >= start.Date && report.UploadDate.Value.Date < end.Date.AddDays(1));

            // If there is a program then limit it to just the specified program.
            if (program.HasValue)
            {
                var prog = (int)program.Value;
                q = q.Where(report => report.Program == prog);
            }

            // Filter the results through a formatting function if needed and create the result list.
            var lst = q.Select(s => new { attemptId = s.AttemptId, BatchName = s.BatchName, DupeTableEntriesCreated = s.DupeTableEntriesCreated, DuplicateRecordsList = s.DuplicateRecordsList, EndTime = s.EndTime, Errors = s.Errors, Exceptions = s.Exceptions, ID = s.ID, InsertDuplicates = s.InsertDuplicates, InsertedIDs = s.InsertedIDs, PhoneNumbersAdded = s.PhoneNumbersAdded, Program = s.Program, PurgeEntriesCreated = s.PurgeEntriesCreated, RecordsAttemptInsert = s.RecordsAttemptInsert, RecordsDuplicates = s.RecordsDuplicates, RecordsGiven = s.RecordsGiven, RecordsInserted = s.RecordsInserted, RecordsMissingPhoneNumbers = s.RecordsMissingPhoneNumbers, RecordsSentToMTI = s.RecordsSentToMTI, ReportSaved = s.ReportSaved, RunTimeSeconds = s.RunTimeSeconds, Servicer = s.Servicer, StartTime = s.StartTime, Success = s.Success, UploadDate = s.UploadDate, User = DebtPlus.Utils.Format.Counselor.FormatCounselor((s.User ?? string.Empty).ToLower()) }).OrderBy(s => s.UploadDate).ThenBy(s => s.BatchName).ToList();

            // Load the excel worksheet document
            System.Type excelType = System.Type.GetTypeFromProgID("Excel.Application");
            if (excelType == null)
            {
                throw new ApplicationException("Can not locate Excel application");
            }
            dynamic excelApp = Activator.CreateInstance(excelType);
            excelApp.Visible = true;

            // Get a new worksheet
            dynamic excelWorkbook = excelApp.Workbooks.Add(System.Reflection.Missing.Value);
            dynamic excelSheet = excelWorkbook.ActiveSheet;

            // Add table headers going cell by cell.
            excelSheet.Cells[1, 1] = "User";
            excelSheet.Cells[1, 2] = "Upload Date";
            excelSheet.Cells[1, 3] = "Servicer Name";
            excelSheet.Cells[1, 4] = "Batch Name/No";
            excelSheet.Cells[1, 5] = "Records Added";
            excelSheet.Cells[1, 6] = "Phone Numbers Added";
            excelSheet.Cells[1, 7] = "Duplicates";
            excelSheet.Cells[1, 8] = "Duplicates Auto Archived/Replaced";
            excelSheet.Cells[1, 9] = "Records w/ Missing Phone Numbers";
            excelSheet.Cells[1, 10] = "Records Sent to MTI";

            // Make the headings bold
            dynamic rngHeading = excelSheet.Range("A1");
            rngHeading.EntireRow.Font.Bold = true;

            // Make the date columns formatted as a date
            dynamic rngDateCol = excelSheet.Range(ExcelCell(1, 2));
            rngDateCol.EntireColumn.NumberFormat = "M/D/YYYY";

            // Format the numbers with commas
            for (var colID = 5; colID < 11; ++colID)
            {
                dynamic rngNumCol = excelSheet.Range(ExcelCell(1, colID));
                rngNumCol.EntireColumn.NumberFormat = "#,##0";
            }

            int line = 1;
            foreach (var report in lst)
            {
                ++line;

                // Format the data into the row
                excelSheet.Cells[line, 1] = report.User;
                excelSheet.Cells[line, 2] = ExcelDateValue(report.UploadDate);
                excelSheet.Cells[line, 3] = stringField + (report.Servicer ?? string.Empty);
                excelSheet.Cells[line, 4] = stringField + (report.BatchName ?? string.Empty);
                excelSheet.Cells[line, 5] = report.RecordsGiven.Value.ToString();
                excelSheet.Cells[line, 6] = report.PhoneNumbersAdded.Value.ToString();
                excelSheet.Cells[line, 7] = report.DupeTableEntriesCreated.Value.ToString();
                excelSheet.Cells[line, 8] = report.PurgeEntriesCreated.Value.ToString();
                excelSheet.Cells[line, 9] = report.RecordsMissingPhoneNumbers.Value.ToString();
                excelSheet.Cells[line, 10] = "0";
            }

            // Turn control over to the user
            excelSheet.Columns.AutoFit();
            excelApp.UserControl = true;

            // Dispose of the record storage.
            lst.Clear();
            lst = null;
        }

        public void GetLoadDuplicatesExcel(PROGRAM program, DateTime start, DateTime end, bool wereUploaded)
        {
            // Get the data for the report
            var lst = repository.GetUploadedRecords(program)
                            .Where(record => record.IsDuplicate.HasValue &&
                                    record.IsDuplicate.Value &&
                                    record.Uploaded == wereUploaded &&
                                    record.UploadDate.HasValue &&
                                    record.UploadDate.Value >= start.Date &&
                                    record.UploadDate.Value < end.Date.AddDays(1)).ToList();

            // Load the excel worksheet document
            System.Type excelType = System.Type.GetTypeFromProgID("Excel.Application");
            if (excelType == null)
            {
                throw new ApplicationException("Can not locate Excel application");
            }
            dynamic excelApp = Activator.CreateInstance(excelType);
            excelApp.Visible = true;

            // Get a new worksheet
            dynamic excelWorkbook = excelApp.Workbooks.Add(System.Reflection.Missing.Value);
            dynamic excelSheet = excelWorkbook.ActiveSheet;

            // Add table headers going cell by cell.
            excelSheet.Cells[1, 1] = "FMAC CNNO";
            excelSheet.Cells[1, 2] = "SERVICER";
            excelSheet.Cells[1, 3] = "LOAN #";
            excelSheet.Cells[1, 4] = "LAST NAME";
            excelSheet.Cells[1, 5] = "FIRST NAME";
            excelSheet.Cells[1, 6] = "SSN";
            excelSheet.Cells[1, 7] = "LAST NAME (2)";
            excelSheet.Cells[1, 8] = "FIRST NAME (2)";
            excelSheet.Cells[1, 9] = "SSN (2)";
            excelSheet.Cells[1, 10] = "STREET";
            excelSheet.Cells[1, 11] = "CITY";
            excelSheet.Cells[1, 12] = "STATE";
            excelSheet.Cells[1, 13] = "ZIP";
            excelSheet.Cells[1, 14] = "Phone 1";
            excelSheet.Cells[1, 15] = "Phone 2";
            excelSheet.Cells[1, 16] = "Phone 3";
            excelSheet.Cells[1, 17] = "Phone 4";
            excelSheet.Cells[1, 18] = "Phone 5";
            excelSheet.Cells[1, 19] = "Last Chance List";
            excelSheet.Cells[1, 20] = "Actual Sale Date";
            excelSheet.Cells[1, 21] = "Due Date";

            // Make the headings bold
            dynamic rngHeading = excelSheet.Range(ExcelCell(1, 1));
            rngHeading.EntireRow.Font.Bold = true;

            // Make the date columns formatted as a date
            dynamic rng = excelSheet.Range(ExcelCell(1, 19));
            rng.EntireColumn.NumberFormat = "M/D/YYYY";

            rng = excelSheet.Range(ExcelCell(1, 20));
            rng.EntireColumn.NumberFormat = "M/D/YYYY";

            rng = excelSheet.Range(ExcelCell(1, 21));
            rng.EntireColumn.NumberFormat = "M/D/YYYY";

            int line = 1;
            foreach (var record in lst)
            {
                ++line;
                excelSheet.Cells[line, 1] = stringField + (record.InvestorNumber ?? string.Empty);
                excelSheet.Cells[line, 2] = stringField + (record.Servicer ?? string.Empty);
                excelSheet.Cells[line, 3] = stringField + (record.LoanNumber ?? string.Empty);
                excelSheet.Cells[line, 4] = stringField + (record.LastName1 ?? string.Empty);
                excelSheet.Cells[line, 5] = stringField + (record.FirstName1 ?? string.Empty);
                excelSheet.Cells[line, 6] = stringField + (record.SSN1 ?? string.Empty);
                excelSheet.Cells[line, 7] = stringField + (record.LastName2 ?? string.Empty);
                excelSheet.Cells[line, 8] = stringField + (record.FirstName2 ?? string.Empty);
                excelSheet.Cells[line, 9] = stringField + (record.SSN2 ?? string.Empty);
                excelSheet.Cells[line, 10] = stringField + (record.ClientStreet ?? string.Empty);
                excelSheet.Cells[line, 11] = stringField + (record.ClientCity ?? string.Empty);
                excelSheet.Cells[line, 12] = stringField + (record.ClientState ?? string.Empty);
                excelSheet.Cells[line, 13] = stringField + (record.ClientZipcode ?? string.Empty);
                excelSheet.Cells[line, 14] = stringField + (record.Phone1 ?? string.Empty);
                excelSheet.Cells[line, 15] = stringField + (record.Phone2 ?? string.Empty);
                excelSheet.Cells[line, 16] = stringField + (record.Phone3 ?? string.Empty);
                excelSheet.Cells[line, 17] = stringField + (record.Phone4 ?? string.Empty);
                excelSheet.Cells[line, 18] = stringField + (record.Phone5 ?? string.Empty);
                excelSheet.Cells[line, 19] = ExcelDateValue(record.InvestorLastChanceList);
                excelSheet.Cells[line, 20] = ExcelDateValue(record.InvestorActualSaleDate);
                excelSheet.Cells[line, 21] = ExcelDateValue(record.InvestorDueDate);
            }

            // Turn control over to the user
            excelSheet.Columns.AutoFit();
            excelApp.UserControl = true;

            // Dispose of the record storage.
            lst.Clear();
            lst = null;
        }

        public void GetPostModRecordsCreatedExcel(PROGRAM program, DateTime start, DateTime end)
        {
            var prog = (int)program;
            var lst = repository.GetPostModsCreated().Where(record => record.Program == prog && record.ReceivedDate.HasValue && record.ReceivedDate.Value >= start.Date && record.ReceivedDate.Value < end.Date.AddDays(1)).ToList();

            // Load the excel worksheet document
            System.Type excelType = System.Type.GetTypeFromProgID("Excel.Application");
            if (excelType == null)
            {
                throw new ApplicationException("Can not locate Excel application");
            }
            dynamic excelApp = Activator.CreateInstance(excelType);
            excelApp.Visible = true;

            // Get a new worksheet
            dynamic excelWorkbook = excelApp.Workbooks.Add(System.Reflection.Missing.Value);
            dynamic excelSheet = excelWorkbook.ActiveSheet;

            // Add table headers going cell by cell.
            excelSheet.Cells[1, 1] = "User";
            excelSheet.Cells[1, 2] = "Received Date";
            excelSheet.Cells[1, 3] = "Client ID";
            excelSheet.Cells[1, 4] = "ClientName";
            excelSheet.Cells[1, 5] = "Servicer Name";
            excelSheet.Cells[1, 6] = "Servicer Loan Number";
            excelSheet.Cells[1, 7] = "First RPC Date";
            excelSheet.Cells[1, 8] = "Current Queue";

            // Make the headings bold
            dynamic rngHeading = excelSheet.Range(ExcelCell(1, 1));
            rngHeading.EntireRow.Font.Bold = true;

            // Make the date columns formatted as a date
            dynamic rng = excelSheet.Range(ExcelCell(1, 2));
            rng.EntireColumn.NumberFormat = "M/D/YYYY";

            rng = excelSheet.Range(ExcelCell(1, 7));
            rng.EntireColumn.NumberFormat = "M/D/YYYY";

            int line = 1;
            foreach (var summary in lst)
            {
                ++line;
                excelSheet.Cells[line, 1] = summary.User;
                excelSheet.Cells[line, 2] = ExcelDateValue(summary.ReceivedDate);
                excelSheet.Cells[line, 3] = stringField + summary.Id.ToString();
                excelSheet.Cells[line, 4] = stringField + (summary.ClientName ?? string.Empty);
                excelSheet.Cells[line, 5] = stringField + (summary.ServicerName ?? string.Empty);
                excelSheet.Cells[line, 6] = stringField + (summary.LoanNumber ?? string.Empty);
                excelSheet.Cells[line, 7] = ExcelDateValue(summary.FRPDate);
                excelSheet.Cells[line, 8] = summary.CurrentQueue ?? string.Empty;
            }

            // Turn control over to the user
            excelSheet.Columns.AutoFit();
            excelApp.UserControl = true;

            // Dispose of the record storage.
            lst.Clear();
            lst = null;
        }

        public void GetStatusByReceivedDateExcel(PROGRAM program, DateTime start, DateTime end)
        {
            var prog = (int)program;
            var lst = repository.GetStatusByDate(start, end, (int)program);
            var colProgramTypes = DebtPlus.OCS.InMemory.OCSProgramTypes.getList();

            dynamic rng;

            // Load the excel worksheet document
            System.Type excelType = System.Type.GetTypeFromProgID("Excel.Application");
            if (excelType == null)
            {
                throw new ApplicationException("Can not locate Excel application");
            }
            dynamic excelApp = Activator.CreateInstance(excelType);
            excelApp.Visible = true;

            // Get a new worksheet
            dynamic workBook = excelApp.Workbooks.Add(System.Reflection.Missing.Value);
            dynamic excelSheet = workBook.ActiveSheet;

            // Add table headers going cell by cell.
            /* A */ excelSheet.Cells[1, 1] = "Program";
            /* B */ excelSheet.Cells[1, 2] = "Referral";
            /* C */ excelSheet.Cells[1, 3] = "Client ID";
            /* D */ excelSheet.Cells[1, 4] = "Client Name";
            /* E */ excelSheet.Cells[1, 5] = "Investor Loan #";
            /* F */ excelSheet.Cells[1, 6] = "Servicer Loan #";
            /* G */ excelSheet.Cells[1, 7] = "Received Date";
            /* H */ excelSheet.Cells[1, 8] = "First RPC Date";
            /* I */ excelSheet.Cells[1, 9] = "First RPC Name";
            /* J */ excelSheet.Cells[1, 10] = "Contact Attempts Made";
            /* K */ excelSheet.Cells[1, 11] = "Appt Type";
            /* L */ excelSheet.Cells[1, 12] = "Appt Created";
            /* M */ excelSheet.Cells[1, 13] = "Appt Created By";
            /* N */ excelSheet.Cells[1, 14] = "Appt Date";
            /* O */ excelSheet.Cells[1, 15] = "TR-Trnsf to Counselor (Y/N)";
            /* P */ excelSheet.Cells[1, 16] = "Appt Status (CO,XC,NS)";

            // Make the orientation "Upward"
            rng = excelSheet.Range("A1", "P1");
            rng.Orientation = -4171;         // Microsoft.Office.Interop.Excel.XlOrientation.xlUpward;
            rng.HorizontalAlignment = -4108; // Microsoft.Office.Interop.Excel.XlHAlign.xlHAlignCenter;

            // Make the headings bold
            rng = excelSheet.Range("A1");
            rng.EntireRow.Font.Bold = true;

            // Make the date columns formatted as a date
            foreach (var col in new Int32[] { 7, 8, 12, 14 })
            {
                rng = excelSheet.Range(ExcelCell(1, col));
                rng.EntireColumn.NumberFormat = "M/D/YYYY";
            }

            // These columns are centered
            foreach (var col in new Int32[] { 15, 16 })
            {
                rng = excelSheet.Range(ExcelCell(1, col));
                rng.EntireColumn.HorizontalAlignment = -4108; // Microsoft.Office.Interop.Excel.XlHAlign.xlHAlignCenter;
            }

            int line = 1;
            foreach (var summary in lst)
            {
                ++line;

                // Translate the program type to a suitable text value
                var pgm = colProgramTypes.Find(s => s.Id == summary.ProgramID);
                if (pgm != null)
                {
                    excelSheet.Cells[line, 1] = pgm.description;
                }

                excelSheet.Cells[line, 2] = stringField + summary.Referral;
                excelSheet.Cells[line, 3] = summary.ClientID;
                excelSheet.Cells[line, 4] = stringField + summary.ClientName;
                excelSheet.Cells[line, 5] = stringField + summary.InvestorLoan;
                excelSheet.Cells[line, 6] = stringField + summary.ServicerLoan;
                excelSheet.Cells[line, 7] = ExcelDateValue(summary.ReceivedDate);
                excelSheet.Cells[line, 8] = ExcelDateValue(summary.FirstRpcDate);
                excelSheet.Cells[line, 9] = stringField + summary.FirstRpcName;
                excelSheet.Cells[line, 10] = summary.ContactAttempts;
                excelSheet.Cells[line, 11] = stringField + summary.ApptType;
                excelSheet.Cells[line, 12] = ExcelDateValue(summary.ApptDateCreated);
                excelSheet.Cells[line, 13] = stringField + summary.ApptCreatedBy;
                excelSheet.Cells[line, 14] = ExcelDateValue(summary.ApptDate);
                excelSheet.Cells[line, 15] = stringField + summary.TransferToCounselor;
                excelSheet.Cells[line, 16] = stringField + summary.ApptStatus;
            }

            // Turn control over to the user
            excelSheet.Columns.AutoFit();
            excelApp.UserControl = true;

            // Dispose of the record storage.
            lst.Clear();
            lst = null;
        }

        public void GetArchivedReportExcel(DateTime start, DateTime end)
        {
            // Get the data for the report
            var lst = repository.GetArchiveReports().Where(report => report.ArchiveDate >= start && report.ArchiveDate <= end).OrderBy(s => s.User).ToList();

            // Load the excel worksheet document
            System.Type excelType = System.Type.GetTypeFromProgID("Excel.Application");
            if (excelType == null)
            {
                throw new ApplicationException("Can not locate Excel application");
            }
            dynamic excelApp = Activator.CreateInstance(excelType);
            excelApp.Visible = true;

            // Get a new worksheet
            dynamic excelWorkbook = excelApp.Workbooks.Add(System.Reflection.Missing.Value);
            dynamic excelSheet = excelWorkbook.ActiveSheet;

            // Add table headers going cell by cell.
            excelSheet.Cells[1, 1] = "User";
            excelSheet.Cells[1, 2] = "ArchiveDate";
            excelSheet.Cells[1, 3] = "Records Prior To";
            excelSheet.Cells[1, 4] = "Records Archived";
            excelSheet.Cells[1, 5] = "Program";

            // Make the headings bold
            dynamic rngHeading = excelSheet.Range("A1");
            rngHeading.EntireRow.Font.Bold = true;

            // Make the date columns formatted as a date
            dynamic rng = excelSheet.Range(ExcelCell(1, 2));
            rng.EntireColumn.NumberFormat = "M/D/YYYY";

            int line = 1;
            foreach (var item in lst)
            {
                ++line;
                excelSheet.Cells[line, 1] = item.User;
                excelSheet.Cells[line, 2] = ExcelDateValue(item.ArchiveDate);
                excelSheet.Cells[line, 3] = item.RecordsPriorTo.ToString();
                excelSheet.Cells[line, 4] = item.RecordsArchived.ToString();
                if (item.Program.HasValue)
                {
                    excelSheet.Cells[line, 5] = item.Program.Value.ToString();
                }
            }

            // Turn control over to the user
            excelSheet.Columns.AutoFit();
            excelApp.UserControl = true;

            // Dispose of the record storage.
            lst.Clear();
            lst = null;
        }

        public void GetResultCodeSummaryExcel(DateTime start, DateTime end)
        {
            GetResultCodeSummaryExcel(start, end, null);
        }

        public void GetResultCodeSummaryExcel(DateTime start, DateTime end, PROGRAM? program)
        {
            // Generate the columns for the sheet
            var columns = repository.GetResultCodeList().Where(s => s.ActiveFlag).ToList();
            columns.Sort((leftItem, rightItem) => string.Compare(leftItem.ShortCode ?? string.Empty, rightItem.ShortCode ?? string.Empty, true));

            // Obtain the "report" results
            var results = repository.GetResultCodesReport(start, end, program).ToList();

            // Find a distinct set of users in the results
            var distinctUsers = results.Select(result => result.User.ToLower()).Distinct().ToList();
            distinctUsers.Sort();

            dynamic rng;

            // Load the excel worksheet document
            System.Type excelType = System.Type.GetTypeFromProgID("Excel.Application");
            if (excelType == null)
            {
                throw new ApplicationException("Can not locate Excel application");
            }
            dynamic excelApp = Activator.CreateInstance(excelType);
            excelApp.Visible = true;

            // Get a new worksheet
            dynamic excelWorkbook = excelApp.Workbooks.Add(System.Reflection.Missing.Value);
            dynamic excelSheet = excelWorkbook.ActiveSheet;

            // The first line holds the column headings
            excelSheet.Cells[1, 1] = "User";
            var colTotals = 1;
            System.Text.StringBuilder sbRPC = new System.Text.StringBuilder();
            System.Text.StringBuilder sbCA = new System.Text.StringBuilder();
            foreach (var col in columns)
            {
                excelSheet.Cells[1, ++colTotals] = col.display_description;

                // Add the column to the CA total if desired
                if (col.IsCA)
                {
                    sbCA.AppendFormat("+{0}{1}", ExcelColumn(colTotals), "{0:f0}");
                }

                // Add the column to the RPC total if desired
                if (col.IsRPC)
                {
                    sbRPC.AppendFormat("+{0}{1}", ExcelColumn(colTotals), "{0:f0}");
                    rng = excelSheet.Range(ExcelCell(1, colTotals));
                    rng.Font.Color = System.Drawing.Color.Blue;
                }
                else
                {
                    rng = excelSheet.Range(ExcelCell(1, colTotals));
                    rng.Font.Color = System.Drawing.Color.Black;
                }
            }

            excelSheet.Cells[1, ++colTotals] = "Total (RPC)";  // RPC
            rng = excelSheet.Range(ExcelCell(1, colTotals));
            rng.Font.Color = System.Drawing.Color.Blue;

            excelSheet.Cells[1, ++colTotals] = "Total (Not PBN, SK)";  // CA
            rng = excelSheet.Range(ExcelCell(1, colTotals));
            rng.Font.Color = System.Drawing.Color.Red;

            // Complete the formula line
            sbRPC.Insert(0, "=0");
            sbCA.Insert(0, "=0");

            // Make the orientation "Upward"
            dynamic oResizeRange = excelSheet.Range("A1").Resize(System.Reflection.Missing.Value, colTotals);
            oResizeRange.Orientation = -4171;         // Microsoft.Office.Interop.Excel.XlOrientation.xlUpward;
            oResizeRange.HorizontalAlignment = -4108; // Microsoft.Office.Interop.Excel.XlHAlign.xlHAlignCenter;

            // Make the headings bold
            dynamic rngHeading = excelSheet.Range("A1");
            rngHeading.EntireRow.Font.Bold = true;

            // Make the user total columns bold as well.
            rngHeading = excelSheet.Range(ExcelCell(1, colTotals));
            rngHeading.EntireColumn.Font.Bold = true;

            rngHeading = excelSheet.Range(ExcelCell(1, colTotals - 1));
            rngHeading.EntireColumn.Font.Bold = true;

            // Add the rows for the user data
            var lineNo = 1;
            var colNo = 0;
            foreach (var user in distinctUsers)
            {
                // The first column is the user name.
                colNo = 1;
                excelSheet.Cells[++lineNo, colNo] = user;

                // Add the total items to the cell
                rng = excelSheet.Range(ExcelCell(lineNo, colTotals - 1));
                rng.NumberFormat = "#,##0";
                rng.Formula = string.Format(sbRPC.ToString(), lineNo);

                rng = excelSheet.Range(ExcelCell(lineNo, colTotals));
                rng.NumberFormat = "#,##0";
                rng.Formula = string.Format(sbCA.ToString(), lineNo);

                // Find the details for this user in the list.
                var userResults = results.Where(s => string.Compare(s.User, user, true) == 0).ToList();
                foreach (var userResult in userResults)
                {
                    var col = columns.Find(s => s.Id == userResult.Outcome.GetValueOrDefault(-300));
                    if (col != null)
                    {
                        var ordinal = columns.IndexOf(col) + 2;
                        if (ordinal > 1)
                        {
                            excelSheet.Cells[lineNo, ordinal] = userResult.Count;
                        }
                    }
                }
            }

            // Generate a total line that is below the data
            if (lineNo > 1 && colTotals > 1)
            {
                excelSheet.Cells[++lineNo, 1] = "TOTALS";
                rng = excelSheet.Range(ExcelCell(lineNo, 1));
                rng.EntireRow.Font.Bold = true;
                rng.EntireRow.Font.Color = System.Drawing.Color.Green;

                for (var totalColNo = 2; totalColNo <= colTotals; ++totalColNo)
                {
                    var formulaString = string.Format("=SUM({0}:{1})", ExcelCell(2, totalColNo), ExcelCell(lineNo - 1, totalColNo));
                    rng = excelSheet.Range(ExcelCell(lineNo, totalColNo));
                    rng.Formula = formulaString;
                    rng.NumberFormat = "#,##0";
                }
            }

            // Turn control of the sheet over to the user
            excelSheet.Columns.AutoFit();
            excelApp.UserControl = true;
        }

        public void GetActiveClientsSummaryExcel(PROGRAM program, DateTime from, DateTime to)
        {
            //          (begin.Date, end.Date.AddDays(1));
            if (program != PROGRAM.FreddieMacPostMod &&  program != PROGRAM.FannieMaePostMod )
            {
                GetActiveClientsSummaryEIFamilyExcel(program, from.Date, to.Date.AddDays(1));
            }
            else
            {
                GetActiveClientsSummaryPostModExcel(program, from.Date, to.Date.AddDays(1));
            }
        }

        private void GetActiveClientsSummaryEIFamilyExcel(PROGRAM program, DateTime from, DateTime to)
        {
            // Get the data for the report
            var prog = (int)program;
            var lst = repository.GetActiveClientSummaries().Where(record => record.Program == prog && record.ReceivedDate >= from && record.ReceivedDate < to).ToList();

            // Load the excel worksheet document
            System.Type excelType = System.Type.GetTypeFromProgID("Excel.Application");
            if (excelType == null)
            {
                throw new ApplicationException("Can not locate Excel application");
            }
            dynamic excelApp = Activator.CreateInstance(excelType);
            excelApp.Visible = true;

            // Get a new worksheet
            dynamic excelWorkbook = excelApp.Workbooks.Add(System.Reflection.Missing.Value);
            dynamic excelSheet = excelWorkbook.ActiveSheet;

            // Add table headers going cell by cell.
            excelSheet.Cells[1, 1] = "Client ID";
            excelSheet.Cells[1, 2] = "Client Name";
            excelSheet.Cells[1, 3] = "Loan Number";
            excelSheet.Cells[1, 4] = "Received Date";
            excelSheet.Cells[1, 5] = "First Right Party Contact Date";
            excelSheet.Cells[1, 6] = "Contact Attempts Made";
            excelSheet.Cells[1, 7] = "Appointment Scheduled (Y/N)";

            // Make the headings bold
            dynamic rngHeading = excelSheet.Range("A1");
            rngHeading.EntireRow.Font.Bold = true;

            // Make the date columns formatted as a date
            dynamic rng = excelSheet.Range("D1");
            rng.EntireColumn.NumberFormat = "M/D/YYYY";

            rng = excelSheet.Range("E1");
            rng.EntireColumn.NumberFormat = "M/D/YYYY";

            int line = 1;
            foreach (var summary in lst)
            {
                ++line;
                excelSheet.Cells[line, 1] = summary.Id.ToString();
                excelSheet.Cells[line, 2] = stringField + (summary.ClientName ?? string.Empty);
                excelSheet.Cells[line, 3] = stringField + (summary.LoanNumber ?? string.Empty);
                excelSheet.Cells[line, 4] = ExcelDateValue(summary.ReceivedDate);
                excelSheet.Cells[line, 5] = ExcelDateValue(summary.FRPDate);
                excelSheet.Cells[line, 6] = summary.ContactAttempts.ToString();
                if (summary.AppointmentScheduled.HasValue)
                {
                    excelSheet.Cells[line, 7] = summary.AppointmentScheduled.Value;
                }
            }

            // Turn control of the sheet over to the user
            excelSheet.Columns.AutoFit();
            excelApp.UserControl = true;
        }

        private void GetActiveClientsSummaryPostModExcel(PROGRAM program, DateTime from, DateTime to)
        {
            // Get the data for the report
            var prog = (int)program;
            var lst = repository.GetActiveClientSummaries()
                .Where(record => record.Program == prog && record.ReceivedDate >= from && record.ReceivedDate < to)
                .ToList();

            // Load the excel worksheet document
            System.Type excelType = System.Type.GetTypeFromProgID("Excel.Application");
            if (excelType == null)
            {
                throw new ApplicationException("Can not locate Excel application");
            }
            dynamic excelApp = Activator.CreateInstance(excelType);
            excelApp.Visible = true;

            // Get a new worksheet
            dynamic excelWorkbook = excelApp.Workbooks.Add(System.Reflection.Missing.Value);
            dynamic excelSheet = excelWorkbook.ActiveSheet;

            // Add table headers going cell by cell.
            excelSheet.Cells[1, 1] = "Client ID";
            excelSheet.Cells[1, 2] = "Client Name";
            excelSheet.Cells[1, 3] = "Servicer Name";
            excelSheet.Cells[1, 4] = "Servicer Loan Number";
            excelSheet.Cells[1, 5] = "Received Date";
            excelSheet.Cells[1, 6] = "First Right Party Contact Date";
            excelSheet.Cells[1, 7] = "Contact Attempts Made";
            excelSheet.Cells[1, 8] = "Appointment Scheduled (Y/N)";

            dynamic rng = excelSheet.Range("A1");
            rng.EntireRow.Font.Bold = true;

            // Make the date columns formatted as a date
            rng = excelSheet.Range(ExcelCell(1, 5));
            rng.EntireColumn.NumberFormat = "M/D/YYYY";

            rng = excelSheet.Range(ExcelCell(1, 6));
            rng.EntireColumn.NumberFormat = "M/D/YYYY";

            int line = 1;
            foreach (var summary in lst)
            {
                ++line;
                excelSheet.Cells[line, 1] = summary.Id.ToString();
                excelSheet.Cells[line, 2] = stringField + (summary.ClientName ?? string.Empty);
                excelSheet.Cells[line, 3] = stringField + (summary.ServicerName ?? string.Empty);
                excelSheet.Cells[line, 4] = stringField + (summary.LoanNumber ?? string.Empty);
                excelSheet.Cells[line, 5] = ExcelDateValue(summary.ReceivedDate);
                excelSheet.Cells[line, 6] = ExcelDateValue(summary.FRPDate);
                excelSheet.Cells[line, 7] = summary.ContactAttempts.ToString();
                if (summary.AppointmentScheduled.HasValue)
                {
                    excelSheet.Cells[line, 8] = summary.AppointmentScheduled.Value;
                }
            }

            // Turn control of the sheet over to the user
            excelSheet.Columns.AutoFit();
            excelApp.UserControl = true;
        }

        public void GetNationStarClientCreationReportExcel(DateTime start, DateTime end)
        {
            // Get the data for the report
            var lst = repository.GetNationStarClientCreationReport().Where(report => report.ReceivedDate >= start && report.ReceivedDate <= end).OrderBy(s => s.ReceivedDate).ToList();

            // Load the excel worksheet document
            System.Type excelType = System.Type.GetTypeFromProgID("Excel.Application");
            if (excelType == null)
            {
                throw new ApplicationException("Can not locate Excel application");
            }
            dynamic excelApp = Activator.CreateInstance(excelType);
            excelApp.Visible = true;

            // Get a new worksheet
            dynamic excelWorkbook = excelApp.Workbooks.Add(System.Reflection.Missing.Value);
            dynamic excelSheet = excelWorkbook.ActiveSheet;

            // Add table headers going cell by cell.
            excelSheet.Cells[1, 1] = "User";
            excelSheet.Cells[1, 2] = "Received Date";
            excelSheet.Cells[1, 3] = "Client ID";
            excelSheet.Cells[1, 4] = "Client Name";
            excelSheet.Cells[1, 5] = "Servicer Name";
            excelSheet.Cells[1, 6] = "Servicer Loan Number";
            excelSheet.Cells[1, 7] = "First RPC Date";
            excelSheet.Cells[1, 8] = "Current Queue";

            // Make the headings bold
            var rngHeading = excelSheet.Range("A1");
            rngHeading.EntireRow.Font.Bold = true;

            // Make the date columns formatted as a date
            rngHeading.EntireColumn.NumberFormat = "M/D/YYYY";

            int line = 1;
            foreach (var item in lst)
            {
                ++line;
                excelSheet.Cells[line, 1] = item.User;
                excelSheet.Cells[line, 2] = ExcelDateValue(item.ReceivedDate);
                excelSheet.Cells[line, 3] = item.ClientID.ToString();
                excelSheet.Cells[line, 4] = item.ClientName;
                excelSheet.Cells[line, 5] = item.ServicerName;
                excelSheet.Cells[line, 6] = item.ServicerLoanNumber;
                excelSheet.Cells[line, 7] = item.FirstRPContactDate;
                excelSheet.Cells[line, 8] = item.CurrentQueue;
            }

            // Turn control of the sheet over to the user
            excelSheet.Columns.AutoFit();
            excelApp.UserControl = true;

            // Dispose of the record storage.
            lst.Clear();
            lst = null;
        }

        public void GetNationStarActiveFlagComplianceReportExcel(DateTime start, DateTime end)
        {
            // Get the data for the report
            var lst = repository.GetNationStarActiveFlagComplianceReport().Where(report => report.ReceivedDate >= start && report.ReceivedDate <= end).OrderBy(s => s.ReceivedDate).ToList();

            // Load the excel worksheet document
            System.Type excelType = System.Type.GetTypeFromProgID("Excel.Application");
            if (excelType == null)
            {
                throw new ApplicationException("Can not locate Excel application");
            }
            dynamic excelApp = Activator.CreateInstance(excelType);
            excelApp.Visible = true;

            // Get a new worksheet
            dynamic excelWorkbook = excelApp.Workbooks.Add(System.Reflection.Missing.Value);
            dynamic excelSheet = excelWorkbook.ActiveSheet;

            // Add table headers going cell by cell.
            excelSheet.Cells[1, 1] = "Client ID";
            excelSheet.Cells[1, 2] = "Client Name";
            excelSheet.Cells[1, 3] = "Servicer Name";
            excelSheet.Cells[1, 4] = "Servicer Loan Number";
            excelSheet.Cells[1, 5] = "Received Date";
            excelSheet.Cells[1, 6] = "First Right Party Contact Date";
            excelSheet.Cells[1, 7] = "Contact Attempts Made";
            excelSheet.Cells[1, 8] = "Appointment Scheduled (Y/N)";
            excelSheet.Cells[1, 9] = "Non-Compliance (>3 days)";
            excelSheet.Cells[1, 10] = "Non-Compliance (>30 days)";

            // Make the headings bold
            var rngHeading = excelSheet.Range("A1");
            rngHeading.EntireRow.Font.Bold = true;

            // Make the date columns formatted as a date
            var receivedDate = excelSheet.Range("E1");
            receivedDate.EntireColumn.NumberFormat = "M/D/YYYY";

            var firstRightParty = excelSheet.Range("F1");
            firstRightParty.EntireColumn.NumberFormat = "M/D/YYYY";

            int line = 1;
            foreach (var item in lst)
            {
                ++line;
                excelSheet.Cells[line, 1] = item.ClientID.ToString();
                excelSheet.Cells[line, 2] = item.ClientName;
                excelSheet.Cells[line, 3] = item.ServiceName;
                excelSheet.Cells[line, 4] = item.ServicerLoanNum;
                excelSheet.Cells[line, 5] = ExcelDateValue(item.ReceivedDate);
                excelSheet.Cells[line, 6] = ExcelDateValue(item.FirstRightPartyContactDate);
                excelSheet.Cells[line, 7] = item.ContactAttemptsMade;
                excelSheet.Cells[line, 8] = item.AppointmentScheduled;
                excelSheet.Cells[line, 9] = item.ThreeDayNonCompliance;
                excelSheet.Cells[line, 10] = item.ThirtyDayNonCompliance;
            }

            // Turn control over to the user
            excelSheet.Columns.AutoFit();
            excelApp.UserControl = true;

            // Dispose of the record storage.
            lst.Clear();
            lst = null;
        }

        public void GetHECMFileListingReportExcel(DateTime start, DateTime end, String servicer)
        {
            // Get the data for the report
            var lst = repository.GetHECMFileListingReport()
                .Where(report => report.ReceivedDate >= start && report.ReceivedDate <= end &&
                    (servicer.ToLower() == "all" || report.ServicerName == servicer))
                .OrderBy(s => s.ReceivedDate).ToList();

            // Load the excel worksheet document
            System.Type excelType = System.Type.GetTypeFromProgID("Excel.Application");
            if (excelType == null)
            {
                throw new ApplicationException("Can not locate Excel application");
            }
            dynamic excelApp = Activator.CreateInstance(excelType);
            excelApp.Visible = true;

            // Get a new worksheet
            dynamic excelWorkbook = excelApp.Workbooks.Add(System.Reflection.Missing.Value);
            dynamic excelSheet = excelWorkbook.ActiveSheet;

            // Add table headers going cell by cell.
            excelSheet.Cells[1, 1] = "Received Date";
            excelSheet.Cells[1, 2] = "Client ID";
            excelSheet.Cells[1, 3] = "Servicer Name";
            excelSheet.Cells[1, 4] = "Servicer Loan Number";
            excelSheet.Cells[1, 5] = "Investor Loan Number";
            excelSheet.Cells[1, 6] = "Appointment Status";
            excelSheet.Cells[1, 7] = "Phone Number";

            // Make the headings bold
            var rngHeading = excelSheet.get_Range("A1");
            rngHeading.EntireRow.Font.Bold = true;

            // Make the date columns formatted as a date
            rngHeading.EntireColumn.NumberFormat = "M/D/YYYY";

            int line = 1;
            foreach (var item in lst)
            {
                ++line;
                excelSheet.Cells[line, 1] = ExcelDateValue(item.ReceivedDate);
                excelSheet.Cells[line, 2] = item.ClientId.ToString();
                excelSheet.Cells[line, 3] = item.ServicerName;
                excelSheet.Cells[line, 4] = item.ServicerLoanNumber;
                excelSheet.Cells[line, 5] = item.InvestorLoanNumber;
                excelSheet.Cells[line, 6] = item.AppointmentStatus.ToString();
                excelSheet.Cells[line, 7] = item.PhoneNumber;
            }

            // Turn control over to the user
            excelSheet.Columns.AutoFit();
            excelApp.UserControl = true;

            excelWorkbook = null;
            excelApp = null;
            excelSheet = null;

            // Dispose of the record storage.
            lst.Clear();
            lst = null;
        }

        public void GetOneWestClientCreationReportExcel(DateTime start, DateTime end)
        {
            // Get the data for the report
            var lst = repository.GetOneWestClientCreationReport().Where(report => report.ReceivedDate >= start && report.ReceivedDate <= end).OrderBy(s => s.ReceivedDate).ToList();

            // Load the excel worksheet document
            System.Type excelType = System.Type.GetTypeFromProgID("Excel.Application");
            if (excelType == null)
            {
                throw new ApplicationException("Can not locate Excel application");
            }
            dynamic excelApp = Activator.CreateInstance(excelType);
            excelApp.Visible = true;

            // Get a new worksheet
            dynamic excelWorkbook = excelApp.Workbooks.Add(System.Reflection.Missing.Value);
            dynamic excelSheet = excelWorkbook.ActiveSheet;

            // Add table headers going cell by cell.
            excelSheet.Cells[1, 1] = "User";
            excelSheet.Cells[1, 2] = "Received Date";
            excelSheet.Cells[1, 3] = "Client ID";
            excelSheet.Cells[1, 4] = "Client Name";
            excelSheet.Cells[1, 5] = "Servicer Name";
            excelSheet.Cells[1, 6] = "Servicer Loan Number";
            excelSheet.Cells[1, 7] = "First RPC Date";
            excelSheet.Cells[1, 8] = "Current Queue";

            // Make the headings bold
            var rngHeading = excelSheet.Range("A1");
            rngHeading.EntireRow.Font.Bold = true;

            // Make the date columns formatted as a date
            rngHeading.EntireColumn.NumberFormat = "M/D/YYYY";

            int line = 1;
            foreach (var item in lst)
            {
                ++line;
                excelSheet.Cells[line, 1] = item.User;
                excelSheet.Cells[line, 2] = ExcelDateValue(item.ReceivedDate);
                excelSheet.Cells[line, 3] = item.ClientID.ToString();
                excelSheet.Cells[line, 4] = item.ClientName;
                excelSheet.Cells[line, 5] = item.ServicerName;
                excelSheet.Cells[line, 6] = item.ServicerLoanNumber;
                excelSheet.Cells[line, 7] = item.FirstRPContactDate;
                excelSheet.Cells[line, 8] = item.CurrentQueue;
            }

            // Turn control of the sheet over to the user
            excelSheet.Columns.AutoFit();
            excelApp.UserControl = true;

            // Dispose of the record storage.
            lst.Clear();
            lst = null;
        }

        public void GetOneWestActiveFlagComplianceReportExcel(DateTime start, DateTime end)
        {
            // Get the data for the report
            var lst = repository.GetOneWestActiveFlagComplianceReport().Where(report => report.ReceivedDate >= start && report.ReceivedDate <= end).OrderBy(s => s.ReceivedDate).ToList();

            // Load the excel worksheet document
            System.Type excelType = System.Type.GetTypeFromProgID("Excel.Application");
            if (excelType == null)
            {
                throw new ApplicationException("Can not locate Excel application");
            }
            dynamic excelApp = Activator.CreateInstance(excelType);
            excelApp.Visible = true;

            // Get a new worksheet
            dynamic excelWorkbook = excelApp.Workbooks.Add(System.Reflection.Missing.Value);
            dynamic excelSheet = excelWorkbook.ActiveSheet;

            // Add table headers going cell by cell.
            excelSheet.Cells[1, 1] = "Client ID";
            excelSheet.Cells[1, 2] = "Client Name";
            excelSheet.Cells[1, 3] = "Servicer Name";
            excelSheet.Cells[1, 4] = "Servicer Loan Number";
            excelSheet.Cells[1, 5] = "Received Date";
            excelSheet.Cells[1, 6] = "First Right Party Contact Date";
            excelSheet.Cells[1, 7] = "Contact Attempts Made";
            excelSheet.Cells[1, 8] = "Appointment Scheduled (Y/N)";
            excelSheet.Cells[1, 9] = "Non-Compliance (>3 days)";
            excelSheet.Cells[1, 10] = "Non-Compliance (>30 days)";

            // Make the headings bold
            var rngHeading = excelSheet.Range("A1");
            rngHeading.EntireRow.Font.Bold = true;

            // Make the date columns formatted as a date
            var receivedDate = excelSheet.Range("E1");
            receivedDate.EntireColumn.NumberFormat = "M/D/YYYY";

            var firstRightParty = excelSheet.Range("F1");
            firstRightParty.EntireColumn.NumberFormat = "M/D/YYYY";

            int line = 1;
            foreach (var item in lst)
            {
                ++line;
                excelSheet.Cells[line, 1] = item.ClientID.ToString();
                excelSheet.Cells[line, 2] = item.ClientName;
                excelSheet.Cells[line, 3] = item.ServiceName;
                excelSheet.Cells[line, 4] = item.ServicerLoanNum;
                excelSheet.Cells[line, 5] = ExcelDateValue(item.ReceivedDate);
                excelSheet.Cells[line, 6] = ExcelDateValue(item.FirstRightPartyContactDate);
                excelSheet.Cells[line, 7] = item.ContactAttemptsMade;
                excelSheet.Cells[line, 8] = item.AppointmentScheduled;
                excelSheet.Cells[line, 9] = item.ThreeDayNonCompliance;
                excelSheet.Cells[line, 10] = item.ThirtyDayNonCompliance;
            }

            // Turn control over to the user
            excelSheet.Columns.AutoFit();
            excelApp.UserControl = true;

            // Dispose of the record storage.
            lst.Clear();
            lst = null;
        }

        public void GetUSBankClientCreationReportExcel(DateTime start, DateTime end)
        {
            // Get the data for the report
            var lst = repository.GetUSBankClientCreationReport().Where(report => report.ReceivedDate >= start && report.ReceivedDate <= end).OrderBy(s => s.ReceivedDate).ToList();

            // Load the excel worksheet document
            System.Type excelType = System.Type.GetTypeFromProgID("Excel.Application");
            if (excelType == null)
            {
                throw new ApplicationException("Can not locate Excel application");
            }
            dynamic excelApp = Activator.CreateInstance(excelType);
            excelApp.Visible = true;

            // Get a new worksheet
            dynamic excelWorkbook = excelApp.Workbooks.Add(System.Reflection.Missing.Value);
            dynamic excelSheet = excelWorkbook.ActiveSheet;

            // Add table headers going cell by cell.
            excelSheet.Cells[1, 1] = "User";
            excelSheet.Cells[1, 2] = "Received Date";
            excelSheet.Cells[1, 3] = "Client ID";
            excelSheet.Cells[1, 4] = "Client Name";
            excelSheet.Cells[1, 5] = "Servicer Name";
            excelSheet.Cells[1, 6] = "Servicer Loan Number";
            excelSheet.Cells[1, 7] = "First RPC Date";
            excelSheet.Cells[1, 8] = "Current Queue";

            // Make the headings bold
            var rngHeading = excelSheet.Range("A1");
            rngHeading.EntireRow.Font.Bold = true;

            // Make the date columns formatted as a date
            rngHeading.EntireColumn.NumberFormat = "M/D/YYYY";

            int line = 1;
            foreach (var item in lst)
            {
                ++line;
                excelSheet.Cells[line, 1] = item.User;
                excelSheet.Cells[line, 2] = ExcelDateValue(item.ReceivedDate);
                excelSheet.Cells[line, 3] = item.ClientID.ToString();
                excelSheet.Cells[line, 4] = item.ClientName;
                excelSheet.Cells[line, 5] = item.ServicerName;
                excelSheet.Cells[line, 6] = item.ServicerLoanNumber;
                excelSheet.Cells[line, 7] = item.FirstRPContactDate;
                excelSheet.Cells[line, 8] = item.CurrentQueue;
            }

            // Turn control of the sheet over to the user
            excelSheet.Columns.AutoFit();
            excelApp.UserControl = true;

            // Dispose of the record storage.
            lst.Clear();
            lst = null;
        }

        public void GetUSBankActiveFlagComplianceReportExcel(DateTime start, DateTime end)
        {
            // Get the data for the report
            var lst = repository.GetUSBankActiveFlagComplianceReport().Where(report => report.ReceivedDate >= start && report.ReceivedDate <= end).OrderBy(s => s.ReceivedDate).ToList();

            // Load the excel worksheet document
            System.Type excelType = System.Type.GetTypeFromProgID("Excel.Application");
            if (excelType == null)
            {
                throw new ApplicationException("Can not locate Excel application");
            }
            dynamic excelApp = Activator.CreateInstance(excelType);
            excelApp.Visible = true;

            // Get a new worksheet
            dynamic excelWorkbook = excelApp.Workbooks.Add(System.Reflection.Missing.Value);
            dynamic excelSheet = excelWorkbook.ActiveSheet;

            // Add table headers going cell by cell.
            excelSheet.Cells[1, 1] = "Client ID";
            excelSheet.Cells[1, 2] = "Client Name";
            excelSheet.Cells[1, 3] = "Servicer Name";
            excelSheet.Cells[1, 4] = "Servicer Loan Number";
            excelSheet.Cells[1, 5] = "Received Date";
            excelSheet.Cells[1, 6] = "First Right Party Contact Date";
            excelSheet.Cells[1, 7] = "Contact Attempts Made";
            excelSheet.Cells[1, 8] = "Appointment Scheduled (Y/N)";
            excelSheet.Cells[1, 9] = "Non-Compliance (>3 days)";
            excelSheet.Cells[1, 10] = "Non-Compliance (>30 days)";

            // Make the headings bold
            var rngHeading = excelSheet.Range("A1");
            rngHeading.EntireRow.Font.Bold = true;

            // Make the date columns formatted as a date
            var receivedDate = excelSheet.Range("E1");
            receivedDate.EntireColumn.NumberFormat = "M/D/YYYY";

            var firstRightParty = excelSheet.Range("F1");
            firstRightParty.EntireColumn.NumberFormat = "M/D/YYYY";

            int line = 1;
            foreach (var item in lst)
            {
                ++line;
                excelSheet.Cells[line, 1] = item.ClientID.ToString();
                excelSheet.Cells[line, 2] = item.ClientName;
                excelSheet.Cells[line, 3] = item.ServiceName;
                excelSheet.Cells[line, 4] = item.ServicerLoanNum;
                excelSheet.Cells[line, 5] = ExcelDateValue(item.ReceivedDate);
                excelSheet.Cells[line, 6] = ExcelDateValue(item.FirstRightPartyContactDate);
                excelSheet.Cells[line, 7] = item.ContactAttemptsMade;
                excelSheet.Cells[line, 8] = item.AppointmentScheduled;
                excelSheet.Cells[line, 9] = item.ThreeDayNonCompliance;
                excelSheet.Cells[line, 10] = item.ThirtyDayNonCompliance;
            }

            // Turn control over to the user
            excelSheet.Columns.AutoFit();
            excelApp.UserControl = true;

            // Dispose of the record storage.
            lst.Clear();
            lst = null;
        }
    }
}
