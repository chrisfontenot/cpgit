﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DebtPlus.OCS.Domain;
using System.Data.Linq;
using DebtPlus.LINQ;

namespace DebtPlus.Svc.OCS
{
    public class OCSSortArgs {
        public bool? ActiveFlag { get; set; }
        public Person.LANGUAGE? PreferredLanguage { get; set; }
        public STATUS_CODE? StatusCode { get; set; }
        public int? MinContacts { get; set; }
        public int? MaxContacts { get; set; }
        public US_STATE? State { get; set; }
        public string TzDescriptor { get; set; }
        public List<TIMEZONE> AllowedTZs { get; set; }
        public PROGRAM Program { get; set; }
        public OCS_QUEUE? Queue { get; set; }
        public int LastChanceFlag { get; set; }
        public int? CounseledState { get; set; }
        public DateTime? CounseledFromDate { get; set; }
        public DateTime? CounseledToDate { get; set; }
    }

    public class NoCallableTimezone : Exception { }

    public class SortProvider
    {
        private DebtPlus.Svc.OCS.OCSDbMapper repository;

        public SortProvider(DebtPlus.Svc.OCS.OCSDbMapper rep) {
            repository = rep;
        }
        
        public void FillAllowedTimeZones(OCSSortArgs sortArgs, DateTime serverTime)
        {
            List<TIMEZONE> tzs = TimezoneHelper.TimeZonesPast8am(serverTime);
            //case the user wants a specific timezone
            if (!String.IsNullOrWhiteSpace(sortArgs.TzDescriptor))
            {
                //check to make sure we can call that timezone right now...
                if (tzs.Contains((TIMEZONE)Enum.Parse(typeof(TIMEZONE), sortArgs.TzDescriptor)))
                {
                    tzs = new List<TIMEZONE> { (TIMEZONE)Enum.Parse(typeof(TIMEZONE), sortArgs.TzDescriptor) };
                }
                else {
                    throw new NoCallableTimezone();
                }
            }

            if (tzs.Count < 1) {
                throw new NoCallableTimezone();            
            }

            sortArgs.AllowedTZs = tzs;
        }
    }
}
