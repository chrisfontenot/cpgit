﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DebtPlus.OCS.Domain;
using DebtPlus.LINQ;

namespace DebtPlus.Svc.OCS
{
    public static class SeqHelpers
    {
        private static Action noOp = () => { };

        public static IEnumerable<T> Try<T>(this IEnumerable<T> self)
        {
            if (self == null)
            {
                return new List<T>();
            }

            return self;
        }

        /// <summary>
        /// Returns the enumerable if and only if it has only one element, otherwise defaults to returning an empty list.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="self"></param>
        /// <param name="onConditionNotMet">Use this to throw an error, or take some other action, if you want.</param>
        /// <returns></returns>
        public static IEnumerable<T> IfHasExactlyOne<T>(this IEnumerable<T> self, Action onConditionNotMet)
        {
            if (self == null || self.Count() != 1)
            {
                onConditionNotMet();
                return new List<T>();
            }

            return self;
        }

        /// <summary>
        /// Returns the enumerable if and only if it has only one element, otherwise defaults to returning an empty list.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="self"></param>
        /// <returns></returns>
        public static IEnumerable<T> IfHasExactlyOne<T>(this IEnumerable<T> self)
        {
            return self.IfHasExactlyOne(noOp);
        }
    }

    public static class RepositoryMapHelpers
    {
        //people <-(OwnerID,CoOwnerID) Housing_properties <-(PropertyID) Housing_loans (OrigLenderID,UseOrigLenderID)-> Housing_lenders (ServicerID)-> Housing_lender_servicers
        //var servicerEntites = ocs.Servicer.map(null);
        public static Tuple<LINQ.Housing_property, LINQ.Housing_loan, LINQ.Housing_lender> map(this String from, String LoanNo, Tuple<LINQ.Housing_property, LINQ.Housing_loan, LINQ.Housing_lender> _to = null)
        {
            if (from == null) return null;

            var property = _to == null ? (LINQ.Housing_property) null : _to.Item1;
            var loan = _to == null ? (LINQ.Housing_loan) null : _to.Item2;
            var lender = _to == null ? (LINQ.Housing_lender) null : _to.Item3;

            if (property == null) property = DebtPlus.LINQ.Factory.Manufacture_Housing_property();
            if (loan == null) loan = DebtPlus.LINQ.Factory.Manufacture_Housing_loan();
            if (lender == null) lender = DebtPlus.LINQ.Factory.Manufacture_housing_lender();

            var to = new Tuple<LINQ.Housing_property, LINQ.Housing_loan, LINQ.Housing_lender>(property, loan, lender);

            property.UseHomeAddress = true;

            lender.ServicerName = from;
            lender.ServicerID = from.ToServicerId();
            lender.AcctNum = LoanNo;

            loan.LoanDelinquencyMonths = 0;

            return to;
        }

        public static int? ToServicerId(this String from)
        {
            if (from == null) {
                return null;
            }

            switch (from.ToLower())
            {
                case "chase mortgage":
                    return 105;
                default:
                    return null;
            }
        }

        public static LINQ.OCS_CallOutcome map(this ContactAttempt.CallOutcome from, LINQ.OCS_CallOutcome _to)
        {
            if (from == null) return null;
            var to = _to == null ? new LINQ.OCS_CallOutcome() : _to;

            to.PhoneNumber = from.PhoneNumber.map(new LINQ.TelephoneNumber()).SearchValue;
            to.ResultCode = from.Result.toInt();

            return to;
        }

        //result codes start at ID of 0
        public static int toInt(this RESULT_CODE? from)
        {
            if (!from.HasValue)
            {
                return 0;
            }

            return Convert.ToInt32(from.Value);
        }
        public static int toInt(this RESULT_CODE from)
        {
            return Convert.ToInt32(from);
        }
        //YourEnum foo = (YourEnum)yourInt;
        public static RESULT_CODE toRESULT_CODE(this int from)
        {
            return (RESULT_CODE)from;
        }

        //states start at ID of 0
        public static int toInt(this US_STATE? from)
        {
            if (!from.HasValue)
            {
                return 0;
            }

            return Convert.ToInt32(from.Value);
        }

        //status codes start at ID of 0
        public static int toInt(this STATUS_CODE from)
        {
            return Convert.ToInt32(from);
        }

        public static US_STATE mapUSState(this String from)
        {
            if (String.IsNullOrWhiteSpace(from))
            {
                return US_STATE.None;
            }

            return (US_STATE)Enum.Parse(typeof(US_STATE), from.ToUpper(), true);
        }

        public static PHONE_TYPE mapPhoneType(this String from)
        {
            if (String.IsNullOrWhiteSpace(from))
            {
                return PHONE_TYPE.None;
            }

            return (PHONE_TYPE)Enum.Parse(typeof(PHONE_TYPE), from.ToUpper(), true);
        }

        public static Person.SALUTATION? mapSalutation(this String from)
        {

            if (String.IsNullOrWhiteSpace(from))
            {
                return null;
            }

            return (Person.SALUTATION)Enum.Parse(typeof(Person.SALUTATION), from.Trim('.'), true);
        }

        public static Person.SUFFIX? mapSuffix(this String from)
        {

            if (String.IsNullOrWhiteSpace(from))
            {
                return null;
            }

            return (Person.SUFFIX)Enum.Parse(typeof(Person.SUFFIX), from.Replace(".", ""), true);
        }

        public static LINQ.OCS_ContactAttempt map(this ContactAttempt from, LINQ.OCS_ContactAttempt _to)
        {
            if (from == null) return null;
            var to = _to == null ? new LINQ.OCS_ContactAttempt() : _to;

            to.Begin = from.Begin;
            to.End = from.End;
            to.ResultCode = from.ResultCode.toInt();
            to.ContactType = (int)from.Type;
            to.SpecificReason = from.SpecificReason;
            to.User = Environment.UserName;

            return to;
        }

        public static TelephoneNumber map(this LINQ.TelephoneNumber from, PHONE_TYPE phoneType, TelephoneNumber _to = null)
        {
            if (from == null) return null;
            var to = _to;
            
            if(to == null)
            {
                to = DebtPlus.LINQ.Factory.Manufacture_TelephoneNumber();
            }

            to.AutoAssignedType = phoneType;
            to.SetNumber = from.SearchValue;
            to.UserAssignedType = from.user_selected_type.mapPhoneType();

            return to;
        }

        public static LINQ.TelephoneNumber map(this TelephoneNumber from, LINQ.TelephoneNumber _to)
        {
            if (from == null) return null;

            var to = _to;
            if (to == null)
            {
                to = DebtPlus.LINQ.Factory.Manufacture_TelephoneNumber();
            }

            /* example: 1-555-555-5555, so valid lengths are: 7, 10, 11 */
            //10 digits appears to be the most common
            if (from.Number.Length == 10)
            {
                to.Acode = from.Number.Substring(0, 3);
                to.Number = from.Number.Substring(3, 3) + "-" + from.Number.Substring(6);
            }
            else if (from.Number.Length == 7)
            {
                to.Number = from.Number.Substring(0, 3) + "-" + from.Number.Substring(3);
            }
            else if (from.Number.Length == 11)
            {
                to.Country = Convert.ToInt32(from.Number.Substring(0, 1));
                to.Acode = from.Number.Substring(1, 3);
                to.Number = from.Number.Substring(2, 3) + "-" + from.Number.Substring(7);
            }
            to.user_selected_type = from.UserAssignedType.ToString();

            return to;
        }

        public static LINQ.TelephoneNumber map(this string from, PHONE_TYPE phoneType, string userSelectedType = null)
        {
            if (String.IsNullOrWhiteSpace(from)) return null;
            var to = new LINQ.TelephoneNumber();

            /* example: 1-555-555-5555, so valid lengths are: 7, 10, 11 */
            //10 digits appears to be the most common
            if (from.Length == 10)
            {
                to.Acode = from.Substring(0, 3);
                to.Number = from.Substring(3, 3) + "-" + from.Substring(6);
            }
            else if (from.Length == 7)
            {
                to.Number = from.Substring(0, 3) + "-" + from.Substring(3);
            }
            else if (from.Length == 11)
            {
                to.Country = Convert.ToInt32(from.Substring(0, 1));
                to.Acode = from.Substring(1, 3);
                to.Number = from.Substring(2, 3) + "-" + from.Substring(7);
            }

            to.AutoAssignedType = phoneType;

            if (!String.IsNullOrWhiteSpace(userSelectedType))
            {
                to.user_selected_type = userSelectedType;
                to.UserAssignedType = (PHONE_TYPE)Enum.Parse(typeof(PHONE_TYPE), userSelectedType);
            }

            return to;
        }

        public static LINQ.client map(this OCSClient from, LINQ.client _to)
        {
            if (from == null) return null;
            var to = _to == null ? new LINQ.client() : _to;

            to.active_status = to.active_status ?? "CRE";
            to.disbursement_date = 1;

            return to;
        }

        public static LINQ.OCS_Client map(this OCSClient from, LINQ.OCS_Client _to)
        {
            if (from == null) return null;
            var to = _to == null ? new LINQ.OCS_Client() : _to;

            to.StatusCode = from.StatusCode.toInt();

            return to;
        }

        public static LINQ.address map(this address from, LINQ.address _to)
        {
            if (from == null) return null;
            var to = _to == null ? new LINQ.address() : _to;

            to.PostalCode = from.PostalCode ?? String.Empty;
            to.street = from.street ?? String.Empty;
            to.address_line_2 = from.address_line_2 ?? String.Empty;
            to.city = from.city ?? String.Empty;
            if (from.StateProvince.HasValue)
            {
                to.state = Convert.ToInt32(from.StateProvince.Value);
            }
            else
            {
                to.state = 0;
            }

            return to;
        }

        private static System.Text.RegularExpressions.Regex sanitizeSSN = new System.Text.RegularExpressions.Regex(@"\D");

        public static Tuple<LINQ.people, LINQ.Name, LINQ.EmailAddress> map(this Person from, LINQ.people _toPeople, LINQ.Name _toName, LINQ.EmailAddress _toEmail, int clientId)
        {

            if (from == null) return null;

            var toPeople = _toPeople;
            var toName = _toName;
            var toEmail = _toEmail;

            if (toPeople == null) toPeople = DebtPlus.LINQ.Factory.Manufacture_people();
            if (toName == null) toName = DebtPlus.LINQ.Factory.Manufacture_Name();
            if (toEmail == null) toEmail = DebtPlus.LINQ.Factory.Manufacture_EmailAddress();

            var toReturn = new Tuple<LINQ.people, LINQ.Name, LINQ.EmailAddress>(toPeople, toName, toEmail);

            toPeople.Client = clientId;
            if (from.SSN != null)
            {
                toPeople.SSN = sanitizeSSN.Replace(from.SSN, String.Empty).PadLeft(9, '0');
            }

            toName.Prefix = from.Salutation.HasValue ? from.Salutation.ToString() : null;
            /*NOTE: we DO NOT want to make name changes from OCS, so do not map them
            toName.First = from.FirstName;
            toName.Middle = from.MiddleName;
            toName.Last = from.LastName;
            */
            toName.Suffix = from.Suffix.HasValue ? from.Suffix.ToString() : null;

            toEmail.Address = !String.IsNullOrWhiteSpace(from.EmailAddress) ? from.EmailAddress : null;

            return toReturn;
        }

        public static List<client_note> map(this IQueryable<LINQ.client_note> from)
        {
            if (from == null || from.Count() < 1) return new List<client_note>();

            return from.Select(note =>
                new client_note
                {
                    Id = note.Id,
                    subject = note.subject,
                    note = note.note
                }).ToList();
        }

        public static LINQ.client_note map(this client_note from, int? clientId = null, LINQ.client_note _to = null)
        {
            if (from == null) return null;
            var to = _to == null ? new LINQ.client_note() : _to;

            to.dont_delete = true;
            to.dont_edit = true;
            to.dont_print = false;
            if (clientId.HasValue) to.client = clientId.Value;
            to.subject = from.subject;
            to.note = from.note;
            to.type = from.type;

            return to;
        }
    }
}
