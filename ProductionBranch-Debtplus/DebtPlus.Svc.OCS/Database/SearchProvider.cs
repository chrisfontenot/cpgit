﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using DebtPlus.LINQ;
using DebtPlus.LINQ.BusinessLayer;

namespace DebtPlus.UI.Housing.OCS
{
    public class SearchProvider
    {
        public enum LH_VALUE {
            DebtPlusID,
            LastName,
            Street,
            Phone,
            InvestorLoanNo,
            ServicerLoanNo,
            ServicerNo,
            BatchName
        }

        public enum OPERATOR
        {
            EQ,
            NE,
            StartsWith,
            EndsWith,
            NotStartsWith,
            Contains,
            NotContains,
            Any
        }

        private DebtPlus.Svc.OCS.OCSDbMapper repository;

        public SearchProvider(DebtPlus.Svc.OCS.OCSDbMapper rep) {
            repository = rep;
        }

        public bool Validate(string lh, string op, string rh) {
            return true;
        }

        public Dictionary<string, object> SearchFields = new Dictionary<string, object> { 
            { "Client ID",                      LH_VALUE.DebtPlusID },
            { "Client Last/Former Name only",   LH_VALUE.LastName },
            { "Home Street Address",            LH_VALUE.Street },
            { "Home/Cell/Msg Phone Number",     LH_VALUE.Phone },
            { "Investor Loan Number",           LH_VALUE.InvestorLoanNo },
            { "Servicer Loan Number",           LH_VALUE.ServicerLoanNo},
            { "Servicer Number",                LH_VALUE.ServicerNo },
            { "Batch Name/No.",                 LH_VALUE.BatchName }
        };

        private static readonly List<OPERATOR> StringOperators = new List<OPERATOR> { 
            OPERATOR.EQ, OPERATOR.NE, OPERATOR.StartsWith, OPERATOR.EndsWith, OPERATOR.NotStartsWith, OPERATOR.Contains, OPERATOR.NotContains
        };

        public Dictionary<LH_VALUE, List<OPERATOR>> FieldOperators = new Dictionary<LH_VALUE, List<OPERATOR>> { 
            {LH_VALUE.DebtPlusID,       new List<OPERATOR> { OPERATOR.EQ } },
            {LH_VALUE.LastName,         StringOperators },
            {LH_VALUE.Street,           StringOperators},
            {LH_VALUE.Phone,            StringOperators},
            {LH_VALUE.InvestorLoanNo,       StringOperators},
            {LH_VALUE.ServicerLoanNo,   StringOperators},
            {LH_VALUE.ServicerNo,       StringOperators},
            {LH_VALUE.BatchName,        StringOperators}
        };

        public Dictionary<LH_VALUE, string> MRUs = new Dictionary<LH_VALUE, string> { 
            {LH_VALUE.DebtPlusID,       "Clients" },
            {LH_VALUE.LastName,         "Client Last Name" },
            {LH_VALUE.Street,           "Client Home Address" },
            {LH_VALUE.Phone,            "Client Personal Phone" },
            {LH_VALUE.InvestorLoanNo,       "Investor Loan Number" },
            {LH_VALUE.ServicerLoanNo,   "Servicer Loan Number" },
            {LH_VALUE.ServicerNo,       "ServicerNumber" },
            {LH_VALUE.BatchName,        "Batch Number" }
        };

        public Dictionary<OPERATOR, string> SearchOperators = new Dictionary<OPERATOR,string> { 
            { OPERATOR.EQ,              "=" },
            { OPERATOR.NE,              "not ="},
            { OPERATOR.StartsWith,      "starts with" },
            { OPERATOR.EndsWith,        "ends with" },
            { OPERATOR.NotStartsWith,   "doesn't start with" },
            { OPERATOR.Contains,        "contains" },
            { OPERATOR.NotContains,     "doesn't contain" }
        };

        private static Regex sanitizePhone = new Regex(@"\D");

        public Dictionary<string, object> GetOperators(object field) { 
            var lh = (LH_VALUE)field;
            var toReturn = new Dictionary<string,object>();

            foreach (var op in FieldOperators[lh]) {
                toReturn.Add(SearchOperators[op], op);
            }

            return toReturn;
        }

        private Func<LINQ.view_OCS_Search, bool> GetPredicate(LH_VALUE lh, OPERATOR op, string value) {
            
            if (op == OPERATOR.Any)
            {
                return _ => { return true; };

            } else if (lh == LH_VALUE.DebtPlusID){

                var ID = Convert.ToInt32(value);

                //int predicate for searching records by id
                return client => {

                    var toReturn = client.ClientId == ID;

                    return toReturn;
                };

            }
            else if (lh == LH_VALUE.Phone) {

                var sanitizedPhoneValue = sanitizePhone.Replace(value, "");

                //string predicate for searching records
                return client =>
                {
                    List<string> fields = (new List<string> { 
                        client.Phone1, client.Phone2, client.Phone3, client.Phone4, client.Phone5
                    }).Where(phone => phone != null).ToList();

                    var toReturn = op == OPERATOR.Contains ? fields.Any(field => field.Contains(sanitizedPhoneValue)) :
                        op == OPERATOR.EndsWith ? fields.Any(field => field.EndsWith(sanitizedPhoneValue)) :
                        op == OPERATOR.EQ ? fields.Any(field => field == sanitizedPhoneValue) :
                        op == OPERATOR.NE ? fields.Any(field => field != sanitizedPhoneValue) :
                        op == OPERATOR.NotContains ? fields.Any(field => !field.Contains(sanitizedPhoneValue)) :
                        op == OPERATOR.NotStartsWith ? fields.Any(field => !field.StartsWith(sanitizedPhoneValue)) :
                        op == OPERATOR.StartsWith ? fields.Any(field => field.StartsWith(sanitizedPhoneValue)) : false;

                    return toReturn;
                };
            
            } else if (FieldOperators[lh] == StringOperators){

                var upperedValue = value.ToUpper().Trim();
                if (lh == LH_VALUE.InvestorLoanNo || lh == LH_VALUE.ServicerLoanNo)
                {
                    upperedValue = upperedValue.TrimStart('0');
                }

                //string predicate for searching records
                return client =>
                {
                    var field = 
                        lh == LH_VALUE.BatchName && client.BatchName != null ? client.BatchName.ToUpper() :
                        lh == LH_VALUE.InvestorLoanNo && client.InvestorNo != null ? client.InvestorNo :
                        lh == LH_VALUE.LastName && client.AppLastName != null ? client.AppLastName.ToUpper() :
                        lh == LH_VALUE.ServicerLoanNo && client.ServicerLoanNum != null ? client.ServicerLoanNum :
                        lh == LH_VALUE.ServicerNo && client.ServicerId != null ? client.ServicerId :
                        lh == LH_VALUE.Street && client.StreetAddress != null ? client.StreetAddress.ToUpper() :
                        "";

                    field = field.ToUpper();

                    if (lh == LH_VALUE.InvestorLoanNo || lh == LH_VALUE.ServicerLoanNo) {
                        field = field.TrimStart('0');
                    }

                    var toReturn = op == OPERATOR.Contains ? field.Contains(upperedValue) :
                        op == OPERATOR.EndsWith ? field.EndsWith(upperedValue) :
                        op == OPERATOR.EQ ? field == upperedValue :
                        op == OPERATOR.NE ? field != upperedValue :
                        op == OPERATOR.NotContains ? !field.Contains(upperedValue) :
                        op == OPERATOR.NotStartsWith ? !field.StartsWith(upperedValue) :
                        op == OPERATOR.StartsWith ? field.StartsWith(upperedValue) : false;

                    return toReturn;
                };
            }

            return _ => { return true; };
        }
        
        public System.Data.DataTable Search(object _lh, object _op, string rh, int skip, int take, bool archivedOnly = false) {
            LH_VALUE lh = (LH_VALUE)_lh;
            OPERATOR op = (OPERATOR)_op;

            //var predicate = GetPredicate(lh, op, rh);

            var all = repository.GetOCSSearch(lh.ToString(), op.ToString(), rh.ToString())
                .Where(s => archivedOnly == false ? s.Archive == false : s.Archive == true)
                //.Where(predicate)
                .Skip(skip).Take(take);
                //.ToList();
            
            var toReturn = new System.Data.DataTable();
            toReturn.Columns.Add("ID");
            toReturn.Columns.Add("Client");
            toReturn.Columns.Add("Name");
            toReturn.Columns.Add("Phone");
            toReturn.Columns.Add("SSN");
            toReturn.Columns.Add("Program");
            toReturn.Columns.Add("Address");

            foreach (var client in all)
            {
                if (client != null)
                {
                    var aRow = toReturn.NewRow();
                    aRow["ID"] = client.Id;
                    aRow["Client"] = client.ClientID;
                    aRow["Name"] = client.AppLastName + ", " + client.AppFirstName;
                    aRow["Phone"] = !String.IsNullOrWhiteSpace(client.Phone1) ? client.Phone1 :
                        !String.IsNullOrWhiteSpace(client.Phone2) ? client.Phone2 :
                        !String.IsNullOrWhiteSpace(client.Phone3) ? client.Phone3 :
                        !String.IsNullOrWhiteSpace(client.Phone4) ? client.Phone4 :
                        !String.IsNullOrWhiteSpace(client.Phone5) ? client.Phone5 :
                        String.Empty;
                    aRow["SSN"] = client.AppSSN;
                    aRow["Program"] = client.Program;
                    aRow["Address"] = String.Format("{0}, {1}, {2} {3}", client.StreetAddress, client.City, client.StateAbbreviation, client.ZipCode);

                    toReturn.Rows.Add(aRow);
                }
            }

            return toReturn;
        }
    }
}

