﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DebtPlus.LINQ;
using DebtPlus.OCS.Domain;
using DebtPlus.Svc.OCS;
using System.Data.SqlTypes;

namespace DebtPlus.OCS
{
    public static class RecordLoader
    {
        private static int count = 0;

        const string NationStarServicer = "Nation Star";
        const string OneWestServicer    = "ONEWEST BANK";
        const string USBankServicer     = "US BANK HOME MORTGAGE";

        public static int singleLoad(BusinessContext context, PROGRAM program, System.Boolean replaceDupes, System.String batchName, List<Tuple<OCSClient, OCS_UploadRecord>> mappedFromExcel, System.String UserName, int ServicerId = 442)
        {
            count = 0;
            var allOCSClientObjects = mappedFromExcel.Select(mapped => mapped.Item1).ToList();
            var allUploadRecords = mappedFromExcel.Select(mapped => mapped.Item2).ToList();

            var startTime = System.DateTime.Now;

            var report = new OCS_UploadReport
            {
                User = UserName,
                BatchName = batchName,
                AttemptId = System.Guid.NewGuid(),
                Servicer = mappedFromExcel.First().Item1.Servicer,
                UploadDate = startTime,
                StartTime = startTime,
                RecordsGiven = allOCSClientObjects.Count,
                RecordsMissingPhoneNumbers = allOCSClientObjects.Count(cl => cl.PhoneNumbers == null || cl.PhoneNumbers.Count < 1),
                PhoneNumbersAdded = allOCSClientObjects.Sum(cl => { return cl.PhoneNumbers != null ? cl.PhoneNumbers.Count : 0; }),
                Program = (int)program,

                //these get filled out as we go
                RecordsDuplicates = 0,
                RecordsAttemptInsert = 0,
                RecordsInserted = 0,
                InsertDuplicates = replaceDupes,
                DupeTableEntriesCreated = 0,
                PurgeEntriesCreated = 0,
                InsertedIDs = new List<int>(),

                //these get filled out at the end
                Success = true,
                ReportSaved = false,
                Errors = "",
                Exceptions = "",
                DuplicateRecordsList = ""
            };

            //map batch name and attempt id to each record
            allOCSClientObjects = allOCSClientObjects.Select(cl =>
            {
                cl.UploadAttempt = report.AttemptId.Value;

                return cl;
            }).ToList();

            allUploadRecords = allUploadRecords.Select(cl =>
            {
                cl.UploadAttempt = report.AttemptId.Value;
                cl.UploadDate = startTime;

                return cl;
            }).ToList();

            insertBrandNewRecords(context, program, allOCSClientObjects, allUploadRecords, report);

            var endTime = System.DateTime.Now;
            var runDiff = endTime.Subtract(startTime);
            var runSeconds = (int)runDiff.TotalSeconds;

            report.EndTime = endTime;
            report.RunTimeSeconds = runSeconds;

            report.ReportSaved = true;
            context.SubmitChanges(System.Data.Linq.ConflictMode.FailOnFirstConflict);

            return report.InsertedIDs.First();
        }

        public static OCS_UploadReport bulkLoad(BusinessContext context, PROGRAM program, System.Boolean replaceDupes, System.String batchName, List<Tuple<OCSClient, OCS_UploadRecord>> mappedFromExcel, System.String UserName, string fileName)
        {
            count                   = 0;
            var allOCSClientObjects = mappedFromExcel.Select(mapped => mapped.Item1).ToList();
            var allUploadRecords    = mappedFromExcel.Select(mapped => mapped.Item2).ToList();
            var startTime           = System.DateTime.Now;

            var report = new OCS_UploadReport()
            {
                User                       = UserName,
                BatchName                  = batchName,
                AttemptId                  = System.Guid.NewGuid(),
                Servicer                   = mappedFromExcel.First().Item1.Servicer,
                UploadDate                 = startTime,
                StartTime                  = startTime,
                RecordsGiven               = allOCSClientObjects.Count,
                RecordsMissingPhoneNumbers = allOCSClientObjects.Count(cl => cl.PhoneNumbers == null || cl.PhoneNumbers.Count < 1),
                PhoneNumbersAdded          = allOCSClientObjects.Sum(cl => { return cl.PhoneNumbers != null ? cl.PhoneNumbers.Count : 0; }),
                Program                    = (int)program,

                //these get filled out as we go
                RecordsDuplicates       = 0,
                RecordsAttemptInsert    = 0,
                RecordsInserted         = 0,
                InsertDuplicates        = replaceDupes,
                DupeTableEntriesCreated = 0,
                PurgeEntriesCreated     = 0,
                RecordsSentToMTI        = 0,
                InsertedIDs             = new List<int>(),

                //these get filled out at the end
                Success              = true,
                ReportSaved          = false,
                Errors               = string.Empty,
                Exceptions           = string.Empty,
                DuplicateRecordsList = string.Empty
            };

            //map batch name and attempt id to each record
            allOCSClientObjects = allOCSClientObjects.Select(cl =>
            {
                cl.UploadAttempt = report.AttemptId.Value;
                return cl;
            }).ToList();

            allUploadRecords = allUploadRecords.Select(cl =>
            {
                cl.UploadAttempt = report.AttemptId.Value;
                cl.UploadDate    = startTime;
                cl.Program       = (int)program;
                return cl;
            }).ToList();

            List<OCSClient> brandNewOCSClients               = null;
            List<OCS_UploadRecord> brandNewUploadRecords     = null;
            List<OCS_Client> oldDuplicateOCSClients          = null;
            List<OCSClient> newDuplicateOCSClients           = null;
            List<OCS_UploadRecord> newDuplicateUploadRecords = null;

            if (program == PROGRAM.NationStarSD1308 || program == PROGRAM.OneWestSD1308 || program == PROGRAM.USBankSD1308)
            {
                List<string> loanNumbers = new List<string>();
                loanNumbers = allOCSClientObjects.Where(cl => cl.LoanNo != null).Select(cl => cl.LoanNo.Trim()).ToList();

                List<OCS_UploadRecord> preExistingLoanRecords = null;
                /* get the last record for the loan number, check for duplicate against the last record uploaded */
                /* look for records with uploaded flag true (uploaded=false are orphan records with no reference in the ocs_client table) */
                var result = context.OCS_UploadRecords
                                                    .Where(ur => ((ur.IsDuplicate == null || ur.IsDuplicate == false) && ur.Uploaded == true) && loanNumbers.Contains(ur.LoanNumber)).ToList();
                                                    //.OrderByDescending(ur => ur.Id)
                                                    //.GroupBy(ur => ur.LoanNumber)
                                                    //.Select(ur => ur.First()).ToList();

                preExistingLoanRecords = result.OrderByDescending(ur => ur.Id)
                                                    .GroupBy(ur => ur.LoanNumber)
                                                    .Select(ur => ur.First()).ToList();

                var preExistingUploadIDs = preExistingLoanRecords.Select(ur => ur.Id).ToList();
                var preExistingLoansNumbers = preExistingLoanRecords.Select(ur => ur.LoanNumber).ToList();

                // Check the duplicates for the loan number and program type
                oldDuplicateOCSClients = context.OCS_Clients.Where(cl => (int)cl.Program == (int)program &&
                                                                              cl.UploadRecord != null &&
                                                                              preExistingUploadIDs.Contains((int)cl.UploadRecord)).Select(cl => cl).ToList();

                var loanNumberWithOtherProgram = context.OCS_UploadRecords.Where(cl => (int)cl.Program != (int)program && preExistingUploadIDs.Contains((int)cl.Id)).Select(cl => cl.LoanNumber).ToList();

                newDuplicateOCSClients = allOCSClientObjects.Where(record => preExistingLoansNumbers.Contains(record.LoanNo) && loanNumberWithOtherProgram != null && !loanNumberWithOtherProgram.Contains(record.LoanNo)).ToList();
                newDuplicateUploadRecords = allUploadRecords.Where(record => preExistingLoansNumbers.Contains(record.LoanNumber) && loanNumberWithOtherProgram != null && !loanNumberWithOtherProgram.Contains(record.LoanNumber)).ToList();
                report.RecordsDuplicates = newDuplicateOCSClients.Count;

                //get shiny new ones
                brandNewOCSClients = allOCSClientObjects.Where(record => !preExistingLoansNumbers.Contains(record.LoanNo)).ToList();
                brandNewUploadRecords = allUploadRecords.Where(record => !preExistingLoansNumbers.Contains(record.LoanNumber)).ToList();

                if (loanNumberWithOtherProgram != null && loanNumberWithOtherProgram.Count > 0)
                {
                    //lists of duplicates for the loan number and program type
                    brandNewOCSClients.AddRange(allOCSClientObjects.Where(record => loanNumberWithOtherProgram.Contains(record.LoanNo)).ToList());
                    brandNewUploadRecords.AddRange(allUploadRecords.Where(record => loanNumberWithOtherProgram.Contains(record.LoanNumber)).ToList());
                }
            }
            else
            {
                List<PROGRAM> fmacPrograms = new List<PROGRAM>()
                {
                    PROGRAM.FreddieMacEI,
                    PROGRAM.FreddieMac180,
                    PROGRAM.FreddieMac720,
                    PROGRAM.FreddieMacHamp,
                    PROGRAM.FreddieMacPostMod
                }; // FannieMaePostMod should be treated like 1308


                List<string> investorNumbers = new List<string>();
                investorNumbers = allOCSClientObjects.Where(cl => cl.InvestorLoanNo != null).Select(cl => cl.InvestorLoanNo.Trim()).ToList();

                List<OCS_Client> result = null;

                if (program == PROGRAM.FannieMaePostMod || program == PROGRAM.PostModPilotCheckin || program == PROGRAM.PostModPilotCounseling)
                {
                    result = context.OCS_Clients.Where(cl => (int)cl.Program == (int)program && investorNumbers.Contains(cl.InvestorNumber)).ToList();
                }
                else
                {
                    result = context.OCS_Clients
                                    .Where(cl => fmacPrograms.Contains((PROGRAM)(cl.Program)) && investorNumbers.Contains(cl.InvestorNumber))
                                    .ToList();
                }

                oldDuplicateOCSClients = result.OrderByDescending(cl => cl.Id).GroupBy(cl => cl.InvestorNumber).Select(cl => cl.First()).ToList();

                var preExistingFMACs = oldDuplicateOCSClients.Select(record => record.InvestorNumber.Trim().TrimStart('0')).ToList();

                //lists of duplicates
                newDuplicateOCSClients    = allOCSClientObjects.Where(record => preExistingFMACs.Contains(record.InvestorLoanNo.Trim().TrimStart('0'))).ToList();
                newDuplicateUploadRecords = allUploadRecords.Where(record => preExistingFMACs.Contains(record.InvestorNumber.Trim().TrimStart('0'))).ToList();
                report.RecordsDuplicates  = newDuplicateOCSClients.Count;

                //get shiny new ones
                brandNewOCSClients = allOCSClientObjects.Where(record => !preExistingFMACs.Contains(record.InvestorLoanNo.Trim().TrimStart('0'))).ToList();
                brandNewUploadRecords = allUploadRecords.Where(record => !preExistingFMACs.Contains(record.InvestorNumber.Trim().TrimStart('0'))).ToList();
            }

            insertBrandNewRecords(context, program, brandNewOCSClients, brandNewUploadRecords, report);

            if (replaceDupes)
            {
                insertWithPurge(context, (int)program, oldDuplicateOCSClients, newDuplicateOCSClients, newDuplicateUploadRecords, report);
            }
            else
            {
                insertWithoutPurge(context, newDuplicateUploadRecords, report);
            }

            var endTime = System.DateTime.Now;
            var runDiff = endTime.Subtract(startTime);
            var runSeconds = (int)runDiff.TotalSeconds;

            report.FileName = fileName;
            report.EndTime = endTime;
            report.RunTimeSeconds = runSeconds;

            report.ReportSaved = true;
            context.OCS_UploadReports.InsertOnSubmit(report);
            context.SubmitChanges(System.Data.Linq.ConflictMode.FailOnFirstConflict);

            return report;
        }

        private static void insertWithoutPurge(BusinessContext context, List<OCS_UploadRecord> newDuplicateUploadRecords, OCS_UploadReport report)
        {
            report.RecordsInserted = report.RecordsInserted ?? 0;
            report.DupeTableEntriesCreated = report.DupeTableEntriesCreated ?? 0;

            foreach (var record in newDuplicateUploadRecords)
            {
                record.Uploaded = false;
                record.IsDuplicate = false; // set the duplicate of latest record to false and all other records to isDuplicate = True
                report.RecordsAttemptInsert++;
                context.OCS_UploadRecords.InsertOnSubmit(record);
                context.SubmitChanges(System.Data.Linq.ConflictMode.FailOnFirstConflict);

                count++;
            }
        }

        private static void insertWithPurge(BusinessContext context, int program, List<OCS_Client> oldDuplicateOCSClients, List<OCSClient> newDuplicateOCSClients, List<OCS_UploadRecord> newDuplicateUploadRecords, OCS_UploadReport report)
        {
            var repository = new OCSDbMapper(context);

            report.RecordsInserted = report.RecordsInserted ?? 0;
            report.DupeTableEntriesCreated = report.DupeTableEntriesCreated ?? 0;

            foreach (var record in newDuplicateUploadRecords)
            {
                //insert upload record
                /* set this flag to false initially and later update it to true when the ocs_client record is created 
                   this will help in isolating orphan records that do not have reference in ocs_cleint table due to failed transactions or errors in upload*/
                record.Uploaded = false; 
                record.IsDuplicate = true;
                context.OCS_UploadRecords.InsertOnSubmit(record);
                context.SubmitChanges(System.Data.Linq.ConflictMode.FailOnFirstConflict);

                OCSClient ocsClient = null;

                //insert new ocs client record
                if (program == (int)PROGRAM.NationStarSD1308 || program == (int)PROGRAM.OneWestSD1308 || program == (int)PROGRAM.USBankSD1308)
                {
                    ocsClient = newDuplicateOCSClients.Where(ocs => ocs.LoanNo == record.LoanNumber).First();
                    var uploadRecord = context.OCS_UploadRecords.Where(ur => ur.LoanNumber == record.LoanNumber).First();
                    /* get the ocs_uploadrecord  for this loan number, to get the client id */
                    var oldUploadRecord = context.OCS_UploadRecords.Where(ur => (ur.IsDuplicate == null || ur.IsDuplicate == false) && 
                                                                                 ur.Uploaded == true && 
                                                                                 ur.LoanNumber == record.LoanNumber)
                                                                    .OrderByDescending(ur => ur.Id)
                                                                    .Select(ur => ur)
                                                                    .FirstOrDefault();

                    if (oldUploadRecord != null)
                    {
                        ocsClient.ClientID = oldDuplicateOCSClients.Where(ocs => ocs.UploadRecord == oldUploadRecord.Id).First().ClientId;
                    }

                    // Since this value is not available in the input excel, enter the value here so that 
                    // the stored procedure can populate the correct servicer
                    switch(program)
                    {
                        case (int)PROGRAM.NationStarSD1308: 
                            ocsClient.Servicer = NationStarServicer;
                            break;
                        case (int)PROGRAM.OneWestSD1308:
                            ocsClient.Servicer = OneWestServicer;
                            break;
                        case (int)PROGRAM.USBankSD1308:
                            ocsClient.Servicer = USBankServicer;
                            break;
                    }
                }
                else
                {
                    ocsClient = newDuplicateOCSClients.Where(ocs => ocs.InvestorLoanNo == record.InvestorNumber).First();
                    ocsClient.ClientID = oldDuplicateOCSClients.Where(ocs => ocs.InvestorNumber == ocsClient.InvestorLoanNo).First().ClientId;
                }

                ocsClient.Archive        = false;
                ocsClient.ActiveFlag     = true;
                ocsClient.IsDuplicate    = false;
                // ocsClient.ClientID    = oldDuplicateOCSClients.Where(ocs => ocs.InvestorNumber == ocsClient.InvestorLoanNo).First().ClientId;
                ocsClient.UploadRecordId = record.Id;

                report.RecordsAttemptInsert++;

                string p1Area = null;
                string p1Num  = null;
                string p2Area = null;
                string p2Num  = null;
                string p3Area = null;
                string p3Num  = null;
                string p4Area = null;
                string p4Num  = null;
                string p5Area = null;
                string p5Num  = null;
                string p6Area = null;
                string p6Num  = null;

                parsePhone(ocsClient.PhoneNumbers, PHONE_TYPE.ClientHome, ref p1Area, ref p1Num);
                parsePhone(ocsClient.PhoneNumbers, PHONE_TYPE.ClientMsg, ref p2Area, ref p2Num);
                parsePhone(ocsClient.PhoneNumbers, PHONE_TYPE.ApplicantCell, ref p3Area, ref p3Num);
                parsePhone(ocsClient.PhoneNumbers, PHONE_TYPE.ApplicantWork, ref p4Area, ref p4Num);
                parsePhone(ocsClient.PhoneNumbers, PHONE_TYPE.CoApplicantCell, ref p5Area, ref p5Num);
                parsePhone(ocsClient.PhoneNumbers, PHONE_TYPE.CoApplicantWork, ref p6Area, ref p6Num);

                String _queueCode = null;
                if (ocsClient.Queue.HasValue)
                {
                    _queueCode = ocsClient.Queue.Value.ToString();
                }
                else
                {
                    _queueCode = OCS_QUEUE.HighTouch.ToString();
                }

                int inserted = repository.insertNewDuplicate(
                    program,

                    ocsClient.Archive,
                    ocsClient.ActiveFlag,
                    ocsClient.IsDuplicate,
                    ocsClient.ClientID.Value,
                    ocsClient.UploadRecordId,
                    ocsClient.UploadAttempt,

                    ocsClient.LastChanceList,
                    ocsClient.ActualSaleDate,
                    ocsClient.DueDate,

                    _queueCode,

                    p1Area, p1Num,
                    p2Area, p2Num,
                    p3Area, p3Num,
                    p4Area, p4Num,
                    p5Area, p5Num,
                    p6Area, p6Num,

                    ocsClient.Servicer,
                    ocsClient.Address.PostalCode
                    );

                report.InsertedIDs.Add(inserted);
                report.DupeTableEntriesCreated++;
                report.RecordsInserted++;
                count++;
            }
        }

        private static void parsePhone(List<TelephoneNumber> numbers, PHONE_TYPE type, ref string area, ref string number)
        {
            var found = numbers.Where(p => p.AutoAssignedType == type).FirstOrDefault();
            if (found != null)
            {
                area = found.map(null).Acode;
                number = found.map(null).Number;
            }
            else
            {
                found = numbers.Where(p => p.UserAssignedType == type).FirstOrDefault();
                if (found != null)
                {
                    area = found.map(null).Acode;
                    number = found.map(null).Number;
                }
            }
        }

        private static void insertBrandNewRecords(
            BusinessContext context,
            PROGRAM program,
            List<OCSClient> brandNewOCSClients,
            List<OCS_UploadRecord> brandNewUploadRecords,
            OCS_UploadReport report)
        {
            var repository = new OCSDbMapper(context);

            report.RecordsInserted = report.RecordsInserted ?? 0;
            report.DupeTableEntriesCreated = report.DupeTableEntriesCreated ?? 0;

            foreach (var record in brandNewUploadRecords)
            {
                record.Uploaded = true;
                record.IsDuplicate = false;
                context.OCS_UploadRecords.InsertOnSubmit(record);
                context.SubmitChanges(System.Data.Linq.ConflictMode.FailOnFirstConflict);

                OCSClient ocsClient = null;
                if (program == PROGRAM.NationStarSD1308)
                {
                    ocsClient = brandNewOCSClients.Where(ocs => ocs.LoanNo == record.LoanNumber).First();
                    // Since this value is not available in the input excel, enter the value here so that 
                    // the stored procedure can populate the correct servicer
                    ocsClient.Servicer = NationStarServicer;
                }
                else if (program == PROGRAM.OneWestSD1308)
                {
                    ocsClient = brandNewOCSClients.Where(ocs => ocs.LoanNo == record.LoanNumber).First();
                    // Since this value is not available in the input excel, enter the value here so that 
                    // the stored procedure can populate the correct servicer
                    ocsClient.Servicer = OneWestServicer;
                }
                else if (program == PROGRAM.USBankSD1308)
                {
                    ocsClient = brandNewOCSClients.Where(ocs => ocs.LoanNo == record.LoanNumber).First();
                    // Since this value is not available in the input excel, enter the value here so that 
                    // the stored procedure can populate the correct servicer
                    ocsClient.Servicer = USBankServicer;
                }
                else
                {
                    ocsClient = brandNewOCSClients.Where(ocs => ocs.InvestorLoanNo == record.InvestorNumber).First();
                }

                ocsClient.Archive = false;
                ocsClient.IsDuplicate = false;
                ocsClient.UploadRecordId = record.Id;

                string p1Area = null;
                string p1Num  = null;
                string p2Area = null;
                string p2Num  = null;
                string p3Area = null;
                string p3Num  = null;
                string p4Area = null;
                string p4Num  = null;
                string p5Area = null;
                string p5Num  = null;
                string p6Area = null;
                string p6Num  = null;

                string CoPhoneArea1 = null;
                string CoPhoneNum1  = null;
                string CoPhoneArea2 = null;
                string CoPhoneNum2  = null;
                string CoPhoneArea3 = null;
                string CoPhoneNum3  = null;
                string CoPhoneArea4 = null;
                string CoPhoneNum4  = null;
                string CoPhoneArea5 = null;
                string CoPhoneNum5  = null;
                string CoPhoneArea6 = null;
                string CoPhoneNum6  = null;

                string queueCode = null;
                if (ocsClient.Queue.HasValue)
                {
                    queueCode = ocsClient.Queue.Value.ToString();
                }
                else
                {
                    queueCode = OCS_QUEUE.HighTouch.ToString();
                }

                report.RecordsAttemptInsert++;

                var inserted = 0;

                if (program == PROGRAM.FannieMaePostMod || program == PROGRAM.PostModPilotCheckin || program == PROGRAM.PostModPilotCounseling)
                {
                    parsePhone(ocsClient.PhoneNumbers, PHONE_TYPE.ClientHome, ref p1Area, ref p1Num);
                    parsePhone(ocsClient.PhoneNumbers, PHONE_TYPE.ClientMsg, ref p2Area, ref p2Num);
                    parsePhone(ocsClient.PhoneNumbers, PHONE_TYPE.ApplicantCell, ref p3Area, ref p3Num);
                    parsePhone(ocsClient.PhoneNumbers, PHONE_TYPE.ApplicantWork, ref p4Area, ref p4Num);
                    parsePhone(ocsClient.PhoneNumbers, PHONE_TYPE.ApplicantCell2, ref p5Area, ref p5Num);
                    parsePhone(ocsClient.PhoneNumbers, PHONE_TYPE.ApplicantWork2, ref p6Area, ref p6Num);

                    parsePhone(ocsClient.PhoneNumbers, PHONE_TYPE.CoApplicantHome, ref CoPhoneArea1, ref CoPhoneNum1);
                    parsePhone(ocsClient.PhoneNumbers, PHONE_TYPE.CoApplicantMsg, ref CoPhoneArea2, ref CoPhoneNum2);
                    parsePhone(ocsClient.PhoneNumbers, PHONE_TYPE.CoApplicantCell, ref CoPhoneArea3, ref CoPhoneNum3);
                    parsePhone(ocsClient.PhoneNumbers, PHONE_TYPE.CoApplicantWork, ref CoPhoneArea4, ref CoPhoneNum4);
                    parsePhone(ocsClient.PhoneNumbers, PHONE_TYPE.CoApplicantCell2, ref CoPhoneArea5, ref CoPhoneNum5);
                    parsePhone(ocsClient.PhoneNumbers, PHONE_TYPE.CoApplicantWork2, ref CoPhoneArea6, ref CoPhoneNum6);

                    inserted = repository.insertBrandNewFMAE(
                    program,

                    ocsClient.Archive,
                    ocsClient.ActiveFlag,
                    ocsClient.IsDuplicate,
                    ocsClient.UploadRecordId,
                    ocsClient.UploadAttempt,
                    ocsClient.PreferredLanguage,

                    ocsClient.InvestorLoanNo,
                    ocsClient.Servicer,
                    ocsClient.LoanNo,

                    ocsClient.PropertyAddress.AddressLine1,
                    ocsClient.PropertyAddress.address_line_2,
                    ocsClient.PropertyAddress.city,
                    (ocsClient.PropertyAddress.StateProvince.HasValue ? System.Convert.ToInt32(ocsClient.PropertyAddress.StateProvince) : 0),
                    ocsClient.PropertyAddress.PostalCode,

                    ocsClient.Address.street,
                    ocsClient.Address.address_line_2,
                    ocsClient.Address.city,
                    (ocsClient.Address.StateProvince.HasValue ? System.Convert.ToInt32(ocsClient.Address.StateProvince) : 0),
                    ocsClient.Address.PostalCode,

                    (ocsClient.Person1 != null),
                    (ocsClient.Person1 != null ? ocsClient.Person1.FirstName : null),
                    (ocsClient.Person1 != null ? ocsClient.Person1.MiddleName : null),
                    (ocsClient.Person1 != null ? ocsClient.Person1.LastName : null),
                    (ocsClient.Person1 != null ? ocsClient.Person1.SSN : null),
                    (ocsClient.Person1 != null ? ocsClient.Person1.EmailAddress : null),

                    (ocsClient.Person2 != null),
                    (ocsClient.Person2 != null ? ocsClient.Person2.FirstName : null),
                    (ocsClient.Person2 != null ? ocsClient.Person2.MiddleName : null),
                    (ocsClient.Person2 != null ? ocsClient.Person2.LastName : null),
                    (ocsClient.Person2 != null ? ocsClient.Person2.EmailAddress : null),

                    p1Area, p1Num,
                    p2Area, p2Num,
                    p3Area, p3Num,
                    p4Area, p4Num,
                    p5Area, p5Num,
                    p6Area, p6Num,

                    CoPhoneArea1, CoPhoneNum1,
                    CoPhoneArea2, CoPhoneNum2,
                    CoPhoneArea3, CoPhoneNum3,
                    CoPhoneArea4, CoPhoneNum4,
                    CoPhoneArea5, CoPhoneNum5,
                    CoPhoneArea6, CoPhoneNum6,

                    ocsClient.ACH_Flag,
                    ocsClient.Agency_Name,
                    ocsClient.Backlog_Mod_Flag,
                    ocsClient.Mod_Conversion_Date,
                    ocsClient.UPB,
                    ocsClient.Reason_For_Default,
                    ocsClient.Comment_Description,
                    ocsClient.Trial_Mod,
                    ocsClient.Trial_Mod_Description,
                    ocsClient.Trial_Mod_Payment_Amount,
                    ocsClient.TrialPaymentsReceivedAmount,
                    ocsClient.TrialPaymentsReceivedCount,
                    ocsClient.Last_Payment_Applied_Amount,
                    ocsClient.Workout_Type
                    );
                }
                else
                {
                    parsePhone(ocsClient.PhoneNumbers, PHONE_TYPE.ClientHome, ref p1Area, ref p1Num);
                    parsePhone(ocsClient.PhoneNumbers, PHONE_TYPE.ClientMsg, ref p2Area, ref p2Num);
                    parsePhone(ocsClient.PhoneNumbers, PHONE_TYPE.ApplicantCell, ref p3Area, ref p3Num);
                    parsePhone(ocsClient.PhoneNumbers, PHONE_TYPE.ApplicantWork, ref p4Area, ref p4Num);
                    parsePhone(ocsClient.PhoneNumbers, PHONE_TYPE.CoApplicantCell, ref p5Area, ref p5Num);
                    parsePhone(ocsClient.PhoneNumbers, PHONE_TYPE.CoApplicantWork, ref p6Area, ref p6Num);


                    inserted = repository.insertBrandNew(
                        program,

                    ocsClient.Archive,
                    ocsClient.ActiveFlag,
                    ocsClient.IsDuplicate,
                    ocsClient.UploadRecordId,
                    ocsClient.UploadAttempt,

                    ocsClient.InvestorLoanNo,

                    ocsClient.Servicer,
                    ocsClient.LoanNo,

                    ocsClient.LastChanceList,
                    ocsClient.ActualSaleDate,
                    ocsClient.DueDate,

                    queueCode,

                    ocsClient.Address.street,
                    ocsClient.Address.city,
                    (ocsClient.Address.StateProvince.HasValue ? Convert.ToInt32(ocsClient.Address.StateProvince) : 0),
                    ocsClient.Address.PostalCode,

                    (ocsClient.Person1 != null),
                    (ocsClient.Person1 != null ? ocsClient.Person1.FirstName : null),
                    (ocsClient.Person1 != null ? ocsClient.Person1.LastName : null),
                    (ocsClient.Person1 != null ? ocsClient.Person1.SSN : null),

                    (ocsClient.Person2 != null),
                    (ocsClient.Person2 != null ? ocsClient.Person2.FirstName : null),
                    (ocsClient.Person2 != null ? ocsClient.Person2.LastName : null),
                    (ocsClient.Person2 != null ? ocsClient.Person2.SSN : null),

                    p1Area, p1Num,
                    p2Area, p2Num,
                    p3Area, p3Num,
                    p4Area, p4Num,
                    p5Area, p5Num,
                    p6Area, p6Num

                        );
                }

                report.InsertedIDs.Add(inserted);
                report.RecordsInserted++;

                count++;
                Console.WriteLine(count);
            }
        }
    }
}
