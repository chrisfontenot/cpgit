﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.IO;
using Excel;
using DebtPlus.OCS.Domain;
using System.Data.SqlTypes;
using DebtPlus.Svc.OCS.Rules;
using System.Data;


namespace DebtPlus.UI.Housing.OCS
{
    public class InvalidSpreadsheetException : Exception
    {
        public List<string> errors { get; set; }
    }

    public class ExcelToRelationalMapper
    {
        private Dictionary<string, Action<string, OCSClient, LINQ.OCS_UploadRecord>> fieldConvertActions = FMACExcelMap.fieldConvertActions;
                
        private Dictionary<string, Func<string, bool>> fieldValidationFuncs = FMACExcelMap.fieldValidationFuncs;
        private string keyColumn = null;

        private Action<string, OCSClient, LINQ.OCS_UploadRecord> getFieldConversion(string columnName)
        {
            return fieldConvertActions[columnName.ToLower()];
        }

        private Func<string,bool> getFieldValidation(string columnName)
        {
            return fieldValidationFuncs[columnName.ToLower()];
        }

        private Func<System.Data.DataRow, Tuple<OCSClient,LINQ.OCS_UploadRecord>> getRecordConversion(System.Data.DataColumnCollection columns)
        {
            List<Action<string, OCSClient, LINQ.OCS_UploadRecord>> fieldConversions = new List<Action<string, OCSClient, LINQ.OCS_UploadRecord>>();
            
            for (int i = 0, l = columns.Count; i < l; i++) {
                fieldConversions.Add(getFieldConversion(columns[i].ColumnName));
            }

            return (row) => {

                var ocs = new OCSClient();
                ocs.PreferredLanguage = Person.LANGUAGE.English;
                var uploadRecord = new LINQ.OCS_UploadRecord();
                var toReturn = new Tuple<OCSClient, LINQ.OCS_UploadRecord>(ocs, uploadRecord);
                
                for (int k = 0, m = row.ItemArray.Length; k < m; k++) {
                    var curr = row.ItemArray[k];
                    var val = curr.stringifyAndSuperTrim();
                    fieldConversions[k](val, ocs, uploadRecord);
                }

                if (fieldConvertActions.ContainsKey("GLOBAL RECORD MAPPER")) {
                    fieldConvertActions["GLOBAL RECORD MAPPER"]("", ocs, uploadRecord);
                }

                return toReturn; 
            };
        }

        private Func<System.Data.DataRow, List<string>> getRowValidator(System.Data.DataColumnCollection columns)
        {
            List<Func<string, bool>> fieldValidations = new List<Func<string, bool>>();

            for (int i = 0, l = columns.Count; i < l; i++)
            {
                fieldValidations.Add(fieldValidationFuncs[columns[i].ColumnName.ToLower()]);
            }

            return (row) =>
            {
                var toReturn = new List<string>();

                for (int k = 0, m = row.ItemArray.Length; k < m; k++)
                {
                    var curr = row.ItemArray[k];
                    if(!fieldValidations[k](curr.ToString())){
                        toReturn.Add(
                            (columns[k].ColumnName.ToLower() == keyColumn.ToLower() ? "Key " : "") + 
                            "Column \"" + columns[k].ColumnName + "\" has errors"); 
                    }
                }

                return toReturn;
            };
        }

        /// <summary>
        /// Returns a list of (Client, Upload Record) and a list of strings, which are the errors
        /// </summary>
        /// <param name="file"></param>
        /// <returns>(List of (OCSClient, OCS_UploadRecord), List of errors)</returns>
        private Tuple<List<Tuple<OCSClient, LINQ.OCS_UploadRecord>>,List<String>> mapFile(System.IO.Stream file) {             
            IExcelDataReader reader = Excel.ExcelReaderFactory.CreateOpenXmlReader(file);
            reader.IsFirstRowAsColumnNames = true;
            var ds = reader.AsDataSet();
            var tbl1 = ds.Tables[0];//for now, we are only handling worksheet 1

            //Ignore all empty rows
            tbl1 = tbl1.Rows.Cast<DataRow>().Where(row => !row.ItemArray.All(field => field is System.DBNull || string.Compare((field as string).Trim(), string.Empty) == 0)).CopyToDataTable();

            var columns = tbl1.Columns;

            var columnCountErrors = validateColumnCounts(columns);

            if (columnCountErrors.Count > 0) {

                //fieldValidationFuncs
                var validColumns = String.Join(",",fieldValidationFuncs.Keys);

                var _errors = new List<string> { 
                    "The columns for this spreadsheet did not validate.",
                    String.Format("The columns must be these, and only these:{0}{1}",Environment.NewLine,validColumns)
                };               

                throw new InvalidSpreadsheetException { errors = _errors.Concat(columnCountErrors).ToList() };
            }
            
            var validator = getRowValidator(columns);
            var validationErrors = new List<string>();

            List<Tuple<OCSClient, LINQ.OCS_UploadRecord>> allTheThings = new List<Tuple<OCSClient, LINQ.OCS_UploadRecord>>();
            var mappy = getRecordConversion(columns);

            for (int i = 0, l = tbl1.Rows.Count; i < l; i++) {
                var hasKeyError = false;
                var r = tbl1.Rows[i];
                var bad = validator(r).Select(e => "Row " + (i + 1).ToString() + ": " + e);
                hasKeyError = bad.ToList().Exists(e => e.StartsWith("Row " + (i + 1).ToString() + ": " + "Key Column"));

                //map "new" errors to our list of already accumulated errors
                validationErrors = validationErrors.Concat(bad.Where(e => !validationErrors.Contains(e)).ToList()).ToList();

                if (!hasKeyError)
                {
                    allTheThings.Add(mappy(r));
                }
                else {
                    validationErrors.Add("SKIPPED Row " + (i + 1).ToString() + ": Primary Key Column Error");
                }
            }
            if (validationErrors.Count > 0) {
                //we will keep going, but report back the errors when we're done
                //throw new InvalidSpreadsheetException { errors = validationErrors };
            }       
            
            return new Tuple<List<Tuple<OCSClient,LINQ.OCS_UploadRecord>>,List<string>>(allTheThings,validationErrors);
        }

        private List<string> validateColumnCounts(System.Data.DataColumnCollection columns)
        {
            var toReturn = new List<string>();

            var keys = fieldValidationFuncs.Keys.ToList();
            var _cols = new List<string>();

            for (int i = 0, l = columns.Count; i < l; i++)
            {
                _cols.Add(columns[i].ColumnName.ToLower());
            }

            foreach (var key in keys) {
                var count = _cols.Count(c => c == key);
                if (count > 1) {
                    toReturn.Add("Column " + key + " appears more than once.");
                }
                else if (count < 1 && key.ToLower() == keyColumn.ToLower()) {
                    toReturn.Add("Primary Key Column \"" + key + "\" is missing.");
                }
            }

            foreach (var col in _cols) {
                if (keys.Count(k => k == col) == 0) { 
                    toReturn.Add("This spreadsheet has an unknown column named '" + col + "'");
                }
            }

            return toReturn;
        }

        private ExcelToRelationalMapper(Dictionary<string, Func<string, bool>> _fieldValidationFuncs, Dictionary<string, Action<string, OCSClient, LINQ.OCS_UploadRecord>> _fieldConvertActions, String _keyColumn) {
            fieldValidationFuncs = _fieldValidationFuncs;
            fieldConvertActions = _fieldConvertActions;
            keyColumn = _keyColumn;
        }

        /// <summary>
        /// Returns a list of (Client, Upload Record) and a list of strings, which are the errors
        /// </summary>
        /// <param name="file"></param>
        /// <returns></returns>
        public static Tuple<List<Tuple<OCSClient, LINQ.OCS_UploadRecord>>, List<String>> mapFile(System.IO.Stream file, Dictionary<string, Func<string, bool>> _fieldValidationFuncs, Dictionary<string, Action<string, OCSClient, LINQ.OCS_UploadRecord>> _fieldConvertActions, String _keyColumn)
        {
            var mapper = new ExcelToRelationalMapper(_fieldValidationFuncs, _fieldConvertActions, _keyColumn);

            return mapper.mapFile(file);
        }
    }
}
