{
    "UniversalTriggers": [
        {
            "Trigger": "LanguageChanged",
            "LastChanceRules": false,
            "SetActive": "DoNothing",
            "SetQueue": "DoNothing",
            "IncrementAttempts": "ResetTo1",
            "SetStatus": null,
            "CalcStatus": null
        },
        {
            "Trigger": "Result_BN",
            "LastChanceRules": false,
            "SetActive": "SetInactive",
            "SetQueue": "DoNothing",
            "IncrementAttempts": "IncrementBy1",
            "SetStatus": "BadNumber",
            "CalcStatus": null
        },
        {
            "Trigger": "Result_CO",
            "LastChanceRules": false,
            "SetActive": "SetInactive",
            "SetQueue": "DoNothing",
            "IncrementAttempts": "IncrementBy1",
            "SetStatus": "Counseled",
            "CalcStatus": null
        },
        {
            "Trigger": "Result_DS",
            "LastChanceRules": false,
            "SetActive": "SetInactive",
            "SetQueue": "DoNothing",
            "IncrementAttempts": "IncrementBy1",
            "SetStatus": "ContactMade",
            "CalcStatus": null
        },
        {
            "Trigger": "Result_NS",
            "LastChanceRules": false,
            "SetActive": "SetInactive",
            "SetQueue": "DoNothing",
            "IncrementAttempts": "IncrementBy1",
            "SetStatus": "NoShow",
            "CalcStatus": null
        },
        {
            "Trigger": "Result_NI",
            "LastChanceRules": false,
            "SetActive": "SetInactive",
            "SetQueue": "DoNothing",
            "IncrementAttempts": "IncrementBy1",
            "SetStatus": "OptOut",
            "CalcStatus": null 
        },
        {
            "Trigger": "Result_SA",
            "LastChanceRules": false,
            "SetActive": "SetInactive",
            "SetQueue": "DoNothing",
            "IncrementAttempts": "IncrementBy1",
            "SetStatus": "PendingAppointment",
            "CalcStatus": null
        },
        {
            "Trigger": "Result_SK",
            "LastChanceRules": false,
            "SetActive": "DoNothing",
            "SetQueue": "DoNothing",
            "IncrementAttempts": "DoNothing",
            "SetStatus": null,
            "CalcStatus": null
        },
                {
            "Trigger": "Result_TR",
            "LastChanceRules": false,
            "SetActive": "SetInactive",
            "SetQueue": "DoNothing",
            "IncrementAttempts": "IncrementBy1",
            "SetStatus": "TransferToCounselor",
            "CalcStatus": null
        },
        {
            "Trigger": "Result_WL",
            "LastChanceRules": false,
            "SetActive": "SetInactive",
            "SetQueue": "DoNothing",
            "IncrementAttempts": "IncrementBy1",
            "SetStatus": "ContactMade",
            "CalcStatus": null
        }
    ],
    "Scenarios": [
        {
            "StatusCode": "NewRecord",
            "Triggers": [
                {
                    "Trigger": "Result_CB",
                    "LastChanceRules": false,
                    "SetActive": "SetInactive",
                    "SetQueue": "DoNothing",
                    "IncrementAttempts": "IncrementBy1",
                    "SetStatus": "CounselorWillCallback1",
                    "CalcStatus": null
                },
                {
                    "Trigger": "Result_CC",
                    "LastChanceRules": false,
                    "SetActive": "SetActive",
                    "SetQueue": "DoNothing",
                    "IncrementAttempts": "IncrementBy1",
                    "SetStatus": "CSRWillCallBack",
                    "CalcStatus": null
                },
                {
                    "Trigger": "Result_LM",
                    "LastChanceRules": false,
                    "SetActive": "SetActive",
                    "SetQueue": "DoNothing",
                    "IncrementAttempts": "IncrementBy1",
                    "SetStatus": "NoContact",
                    "CalcStatus": null
                },
                {
                    "Trigger": "Result_NC",
                    "LastChanceRules": false,
                    "SetActive": "SetActive",
                    "SetQueue": "DoNothing",
                    "IncrementAttempts": "IncrementBy1",
                    "SetStatus": "NoContact",
                    "CalcStatus": null
                }
            ]
        },
        {
            "StatusCode": "CounselorWillCallback1",
            "Triggers": [
                {
                    "Trigger": "Result_CB",
                    "LastChanceRules": false,
                    "SetActive": "SetInactive",
                    "SetQueue": "DoNothing",
                    "IncrementAttempts": "IncrementBy1",
                    "SetStatus": "CounselorWillCallback2",
                    "CalcStatus": null
                },
                {
                    "Trigger": "Result_CC",
                    "LastChanceRules": false,
                    "SetActive": "SetActive",
                    "SetQueue": "DoNothing",
                    "IncrementAttempts": "IncrementBy1",
                    "SetStatus": "CSRWillCallBack",
                    "CalcStatus": null
                },
                {
                    "Trigger": "Result_LM",
                    "LastChanceRules": false,
                    "SetActive": "SetInactive",
                    "SetQueue": "DoNothing",
                    "IncrementAttempts": "IncrementBy1",
                    "SetStatus": "CounselorWillCallback2",
                    "CalcStatus": null
                },
                {
                    "Trigger": "Result_NC",
                    "LastChanceRules": false,
                    "SetActive": "SetInactive",
                    "SetQueue": "DoNothing",
                    "IncrementAttempts": "IncrementBy1",
                    "SetStatus": "CounselorWillCallback2",
                    "CalcStatus": null
                }
            ]
        },
        {
            "StatusCode": "CounselorWillCallback2",
            "Triggers": [
                {
                    "Trigger": "Result_CB",
                    "LastChanceRules": false,
                    "SetActive": "SetInactive",
                    "SetQueue": "DoNothing",
                    "IncrementAttempts": "IncrementBy1",
                    "SetStatus": "CounselorWillCallback3",
                    "CalcStatus": null
                },
                {
                    "Trigger": "Result_CC",
                    "LastChanceRules": false,
                    "SetActive": "SetActive",
                    "SetQueue": "DoNothing",
                    "IncrementAttempts": "IncrementBy1",
                    "SetStatus": "CSRWillCallBack",
                    "CalcStatus": null
                },
                {
                    "Trigger": "Result_LM",
                    "LastChanceRules": false,
                    "SetActive": "SetInactive",
                    "SetQueue": "DoNothing",
                    "IncrementAttempts": "IncrementBy1",
                    "SetStatus": "CounselorWillCallback3",
                    "CalcStatus": null
                },
                {
                    "Trigger": "Result_NC",
                    "LastChanceRules": false,
                    "SetActive": "SetInactive",
                    "SetQueue": "DoNothing",
                    "IncrementAttempts": "IncrementBy1",
                    "SetStatus": "CounselorWillCallback3",
                    "CalcStatus": null
                }
            ]
        },
        {
            "StatusCode": "CounselorWillCallback3",
            "Triggers": [
                {
                    "Trigger": "Result_CB",
                    "LastChanceRules": false,
                    "SetActive": "SetInactive",
                    "SetQueue": "DoNothing",
                    "IncrementAttempts": "IncrementBy1",
                    "SetStatus": "CounselorWillCallback3",
                    "CalcStatus": null
                },
                {
                    "Trigger": "Result_CC",
                    "LastChanceRules": false,
                    "SetActive": "SetActive",
                    "SetQueue": "DoNothing",
                    "IncrementAttempts": "IncrementBy1",
                    "SetStatus": "CSRWillCallBack",
                    "CalcStatus": null
                },
                {
                    "Trigger": "Result_LM",
                    "LastChanceRules": false,
                    "SetActive": "SetInactive",
                    "SetQueue": "DoNothing",
                    "IncrementAttempts": "IncrementBy1",
                    "SetStatus": "CounselorWillCallback3",
                    "CalcStatus": null
                },
                {
                    "Trigger": "Result_NC",
                    "LastChanceRules": false,
                    "SetActive": "SetInactive",
                    "SetQueue": "DoNothing",
                    "IncrementAttempts": "IncrementBy1",
                    "SetStatus": "CounselorWillCallback3",
                    "CalcStatus": null
                }
            ]
        },
        {
            "StatusCode": "Counseled",
            "Triggers": [
                {
                    "Trigger": "Result_CB",
                    "LastChanceRules": false,
                    "SetActive": "SetInactive",
                    "SetQueue": "DoNothing",
                    "IncrementAttempts": "IncrementBy1",
                    "SetStatus": "CounselorWillCallback1",
                    "CalcStatus": null
                },
                {
                    "Trigger": "Result_CC",
                    "LastChanceRules": false,
                    "SetActive": "SetActive",
                    "SetQueue": "DoNothing",
                    "IncrementAttempts": "IncrementBy1",
                    "SetStatus": "CSRWillCallBack",
                    "CalcStatus": null
                },
                {
                    "Trigger": "Result_LM",
                    "LastChanceRules": false,
                    "SetActive": "SetInactive",
                    "SetQueue": "DoNothing",
                    "IncrementAttempts": "IncrementBy1",
                    "SetStatus": "NoContact",
                    "CalcStatus": null
                },
                {
                    "Trigger": "Result_NC",
                    "LastChanceRules": false,
                    "SetActive": "SetInactive",
                    "SetQueue": "DoNothing",
                    "IncrementAttempts": "IncrementBy1",
                    "SetStatus": "NoContact",
                    "CalcStatus": null
                }
            ]
        },
        {
            "StatusCode": "BadNumber",
            "Triggers": [
                {
                    "Trigger": "Result_CB",
                    "LastChanceRules": false,
                    "SetActive": "SetInactive",
                    "SetQueue": "DoNothing",
                    "IncrementAttempts": "IncrementBy1",
                    "SetStatus": "CounselorWillCallback1",
                    "CalcStatus": null
                },
                {
                    "Trigger": "Result_CC",
                    "LastChanceRules": false,
                    "SetActive": "SetActive",
                    "SetQueue": "DoNothing",
                    "IncrementAttempts": "IncrementBy1",
                    "SetStatus": "CSRWillCallBack",
                    "CalcStatus": null
                },
                {
                    "Trigger": "Result_LM",
                    "LastChanceRules": false,
                    "SetActive": "SetInactive",
                    "SetQueue": "DoNothing",
                    "IncrementAttempts": "IncrementBy1",
                    "SetStatus": "NoContact",
                    "CalcStatus": null
                },
                {
                    "Trigger": "Result_NC",
                    "LastChanceRules": false,
                    "SetActive": "SetInactive",
                    "SetQueue": "DoNothing",
                    "IncrementAttempts": "IncrementBy1",
                    "SetStatus": "NoContact",
                    "CalcStatus": null
                }
            ]
        },
        {
            "StatusCode": "ContactMade",
            "Triggers": [
                {
                    "Trigger": "Result_CB",
                    "LastChanceRules": false,
                    "SetActive": "SetInactive",
                    "SetQueue": "DoNothing",
                    "IncrementAttempts": "IncrementBy1",
                    "SetStatus": "CounselorWillCallback1",
                    "CalcStatus": null
                },
                {
                    "Trigger": "Result_CC",
                    "LastChanceRules": false,
                    "SetActive": "SetActive",
                    "SetQueue": "DoNothing",
                    "IncrementAttempts": "IncrementBy1",
                    "SetStatus": "CSRWillCallBack",
                    "CalcStatus": null
                },
                {
                    "Trigger": "Result_LM",
                    "LastChanceRules": false,
                    "SetActive": "SetInactive",
                    "SetQueue": "DoNothing",
                    "IncrementAttempts": "IncrementBy1",
                    "SetStatus": "NoContact",
                    "CalcStatus": null
                },
                {
                    "Trigger": "Result_NC",
                    "LastChanceRules": false,
                    "SetActive": "SetInactive",
                    "SetQueue": "DoNothing",
                    "IncrementAttempts": "IncrementBy1",
                    "SetStatus": "NoContact",
                    "CalcStatus": null
                }
            ]
        },
		{
            "StatusCode": "NoShow",
            "Triggers": [
                {
                    "Trigger": "Result_CB",
                    "LastChanceRules": false,
                    "SetActive": "SetInactive",
                    "SetQueue": "DoNothing",
                    "IncrementAttempts": "IncrementBy1",
                    "SetStatus": "CounselorWillCallback1",
                    "CalcStatus": null
                },
                {
                    "Trigger": "Result_CC",
                    "LastChanceRules": false,
                    "SetActive": "SetActive",
                    "SetQueue": "DoNothing",
                    "IncrementAttempts": "IncrementBy1",
                    "SetStatus": "CSRWillCallBack",
                    "CalcStatus": null
                },
                {
                    "Trigger": "Result_LM",
                    "LastChanceRules": false,
                    "SetActive": "SetInactive",
                    "SetQueue": "DoNothing",
                    "IncrementAttempts": "IncrementBy1",
                    "SetStatus": "NoContact",
                    "CalcStatus": null
                },
                {
                    "Trigger": "Result_NC",
                    "LastChanceRules": false,
                    "SetActive": "SetInactive",
                    "SetQueue": "DoNothing",
                    "IncrementAttempts": "IncrementBy1",
                    "SetStatus": "NoContact",
                    "CalcStatus": null
                }
            ]
        },
        {
            "StatusCode": "PendingAppointment",
            "Triggers": [
                {
                    "Trigger": "Result_CB",
                    "LastChanceRules": false,
                    "SetActive": "SetInactive",
                    "SetQueue": "DoNothing",
                    "IncrementAttempts": "IncrementBy1",
                    "SetStatus": "CounselorWillCallback1",
                    "CalcStatus": null
                },
                {
                    "Trigger": "Result_CC",
                    "LastChanceRules": false,
                    "SetActive": "SetActive",
                    "SetQueue": "DoNothing",
                    "IncrementAttempts": "IncrementBy1",
                    "SetStatus": "CSRWillCallBack",
                    "CalcStatus": null
                },
                {
                    "Trigger": "Result_LM",
                    "LastChanceRules": false,
                    "SetActive": "SetInactive",
                    "SetQueue": "DoNothing",
                    "IncrementAttempts": "IncrementBy1",
                    "SetStatus": "NoContact",
                    "CalcStatus": null
                },
                {
                    "Trigger": "Result_NC",
                    "LastChanceRules": false,
                    "SetActive": "SetInactive",
                    "SetQueue": "DoNothing",
                    "IncrementAttempts": "IncrementBy1",
                    "SetStatus": "NoContact",
                    "CalcStatus": null
                }
            ]
        },
        {
            "StatusCode": "TransferToCounselor",
            "Triggers": [
                {
                    "Trigger": "Result_CB",
                    "LastChanceRules": false,
                    "SetActive": "SetInactive",
                    "SetQueue": "DoNothing",
                    "IncrementAttempts": "IncrementBy1",
                    "SetStatus": "CounselorWillCallback1",
                    "CalcStatus": null
                },
                {
                    "Trigger": "Result_CC",
                    "LastChanceRules": false,
                    "SetActive": "SetActive",
                    "SetQueue": "DoNothing",
                    "IncrementAttempts": "IncrementBy1",
                    "SetStatus": "CSRWillCallBack",
                    "CalcStatus": null
                },
                {
                    "Trigger": "Result_LM",
                    "LastChanceRules": false,
                    "SetActive": "SetInactive",
                    "SetQueue": "DoNothing",
                    "IncrementAttempts": "IncrementBy1",
                    "SetStatus": "NoContact",
                    "CalcStatus": null
                },
                {
                    "Trigger": "Result_NC",
                    "LastChanceRules": false,
                    "SetActive": "SetInactive",
                    "SetQueue": "DoNothing",
                    "IncrementAttempts": "IncrementBy1",
                    "SetStatus": "NoContact",
                    "CalcStatus": null
                }
            ]
        },
        {
            "StatusCode": "OptOut",
            "Triggers": [
                {
                    "Trigger": "Result_CB",
                    "LastChanceRules": false,
                    "SetActive": "SetInactive",
                    "SetQueue": "DoNothing",
                    "IncrementAttempts": "IncrementBy1",
                    "SetStatus": "CounselorWillCallback1",
                    "CalcStatus": null
                },
                {
                    "Trigger": "Result_CC",
                    "LastChanceRules": false,
                    "SetActive": "SetInactive",
                    "SetQueue": "DoNothing",
                    "IncrementAttempts": "IncrementBy1",
                    "SetStatus": "CSRWillCallBack",
                    "CalcStatus": null
                },
                {
                    "Trigger": "Result_LM",
                    "LastChanceRules": false,
                    "SetActive": "SetInactive",
                    "SetQueue": "DoNothing",
                    "IncrementAttempts": "IncrementBy1",
                    "SetStatus": "NoContact",
                    "CalcStatus": null
                },
                {
                    "Trigger": "Result_NC",
                    "LastChanceRules": false,
                    "SetActive": "SetInactive",
                    "SetQueue": "DoNothing",
                    "IncrementAttempts": "IncrementBy1",
                    "SetStatus": "NoContact",
                    "CalcStatus": null
                }
            ]
        },
        {
            "StatusCode": "NoContact",
            "Triggers": [
                {
                    "Trigger": "Result_CB",
                    "LastChanceRules": false,
                    "SetActive": "SetInactive",
                    "SetQueue": "DoNothing",
                    "IncrementAttempts": "IncrementBy1",
                    "SetStatus": "CounselorWillCallback1",
                    "CalcStatus": null
                },
                {
                    "Trigger": "Result_CC",
                    "LastChanceRules": false,
                    "SetActive": "SetInactive",
                    "SetQueue": "DoNothing",
                    "IncrementAttempts": "IncrementBy1",
                    "SetStatus": "CSRWillCallBack",
                    "CalcStatus": null
                },
                {
                    "Trigger": "Result_LM",
                    "LastChanceRules": false,
                    "SetActive": "SetActive",
                    "SetQueue": "DoNothing",
                    "IncrementAttempts": "IncrementBy1",
                    "SetStatus": "NoContact",
                    "CalcStatus": null
                },
                {
                    "Trigger": "Result_NC",
                    "LastChanceRules": false,
                    "SetActive": "SetActive",
                    "SetQueue": "DoNothing",
                    "IncrementAttempts": "IncrementBy1",
                    "SetStatus": "NoContact",
                    "CalcStatus": null
                }
            ]
        },
        {
            "StatusCode": "CSRWillCallBack",
            "Triggers": [
                {
                    "Trigger": "Result_CB",
                    "LastChanceRules": false,
                    "SetActive": "SetInactive",
                    "SetQueue": "DoNothing",
                    "IncrementAttempts": "IncrementBy1",
                    "SetStatus": "CounselorWillCallback1",
                    "CalcStatus": null
                },
                {
                    "Trigger": "Result_CC",
                    "LastChanceRules": false,
                    "SetActive": "SetActive",
                    "SetQueue": "DoNothing",
                    "IncrementAttempts": "IncrementBy1",
                    "SetStatus": "CSRWillCallBack",
                    "CalcStatus": null
                },
                {
                    "Trigger": "Result_LM",
                    "LastChanceRules": false,
                    "SetActive": "SetActive",
                    "SetQueue": "DoNothing",
                    "IncrementAttempts": "IncrementBy1",
                    "SetStatus": "NoContact",
                    "CalcStatus": null
                },
                {
                    "Trigger": "Result_NC",
                    "LastChanceRules": false,
                    "SetActive": "SetActive",
                    "SetQueue": "DoNothing",
                    "IncrementAttempts": "IncrementBy1",
                    "SetStatus": "NoContact",
                    "CalcStatus": null
                }
            ]
        }
    ]
}