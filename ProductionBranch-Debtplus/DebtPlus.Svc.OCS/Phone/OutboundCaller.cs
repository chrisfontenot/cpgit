﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Net.Sockets;

namespace DebtPlus.Svc.OCS.Phone
{
    public class OutboundCaller
    {
        public static int call(string phoneNumber) {
            var toReturn = -1;
            
            UdpClient udpClient = new UdpClient();

            var packet = String.Format(
@"<?xml version=""1.0""?>
<!-- IPC Receive Event Message -->
<IPCActions>
<IPCMakeCallAction>
<ActionName>IPC Make Call Action</ActionName>
<PhoneNumber>91{0}</PhoneNumber>
<DisplayError>true</DisplayError>
</IPCMakeCallAction>
</IPCActions>",phoneNumber);

            Byte[] sendBytes = Encoding.ASCII.GetBytes(packet);

            

            toReturn = udpClient.Send(sendBytes, sendBytes.Length, "localhost", 58000);
            //toReturn = true;

            return toReturn;
        }
    }
}
