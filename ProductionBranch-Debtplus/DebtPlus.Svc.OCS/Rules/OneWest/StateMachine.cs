﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DebtPlus.OCS.Domain;
using DebtPlus.LINQ;
using DebtPlus.Svc.OCS.Rules.StateMachineSupport;

namespace DebtPlus.Svc.OCS.Rules.OneWest
{
    public class StateMachine
    {
        public static OCSClient UpdateOnTrigger(TRIGGER passedTrigger, STATUS_CODE priorStatusCode, OCSClient client)
        {
            if (!client.Queue.HasValue) {
                throw new ArgumentException("One West clients should always have a queue. This client has no assigned queue.");
            }

            List<IScenario> scenarios = new List<IScenario>();
            
            if (client.Queue.Value == OCS_QUEUE.HighTouch) {
                scenarios = ScenarioConfigLoader.getOneWestHighTouchScenarios();
            }
            else if (client.Queue.Value == OCS_QUEUE.Counselor) {
                scenarios = ScenarioConfigLoader.getOneWestCounselorScenarios();
            }

            List<RESULT_CODE> resultCodes = null;

            using (var bc = new BusinessContext())
            {
                resultCodes = bc.OCS_ContactAttempts.Where(ca => ca.ClientId == client.ClientID && ca.ResultCode != null)
                                                    .Select(ca => (RESULT_CODE)ca.ResultCode)
                                                    .Distinct().ToList();
            }

            bool isRpc = false;

            if (resultCodes != null && resultCodes.Count > 0)
            {
                isRpc = RpcHelper.HasPreviousRpcResultCode(client.Program, resultCodes);
            }

            var matchingScenarios = scenarios.Where(st => st.StatusCode == priorStatusCode).ToList();

            IScenario currentScenario = null;
            if (matchingScenarios.Any(s => s.WithRpc == isRpc))
            {
                currentScenario = matchingScenarios.Where(s => s.WithRpc == isRpc).FirstOrDefault();
            }
            else
            {
                currentScenario = matchingScenarios[0];
            }

            if (currentScenario == null) {
                throw new ArgumentException("No valid state is defined for a One West in the " + client.Queue.Value.ToString() + " queue with a status of " + priorStatusCode.ToString());
            }

            TRIGGER triggerToUse = GetTrigger(passedTrigger,client.CurrentAttempt);
            
            var currentTransition = currentScenario.GetTriggerEffect(triggerToUse);

            return StateExecutor.Execute(currentTransition, client);
        }

        #region Get Trigger

        private static TRIGGER GetTrigger(TRIGGER passedTrigger, ContactAttempt contactAttempt)
        {
            TRIGGER toReturn = passedTrigger;

            if (contactAttempt != null)
            {
                //set 'not set' to 'skip'
                contactAttempt.Outcomes.Where(o => !o.Result.HasValue || o.Result.Value == RESULT_CODE.NotSet).ToList()
                    .ForEach(o => { o.Result = RESULT_CODE.Skip; });

                //get right result code from heirarchy
                contactAttempt.ResultCode = GetHierarchyResultCode(contactAttempt);

                //map to trigger
                toReturn = MapResultCodeToTrigger(contactAttempt.ResultCode);
            }

            return toReturn;
        }

        #region One West Trigger Map
        private static Dictionary<RESULT_CODE, TRIGGER> ResultToTrigger = new Dictionary<RESULT_CODE, TRIGGER> { 
            { RESULT_CODE.ExclusionReport, TRIGGER.Result_EX },
            { RESULT_CODE.ClientNotInterested, TRIGGER.Result_NI },
            { RESULT_CODE.Counseled, TRIGGER.Result_CO },
            { RESULT_CODE.ImmediateCounseling, TRIGGER.Result_IM },
            { RESULT_CODE.SecondCounsel, TRIGGER.Result_CO },
            { RESULT_CODE.ScheduledAppointment, TRIGGER.Result_SA },
            { RESULT_CODE.TransferredToCounselor, TRIGGER.Result_TR },
            { RESULT_CODE.RightParty, TRIGGER.Result_RP },
            { RESULT_CODE.CheckIn, TRIGGER.Result_CH },
            //{ RESULT_CODE.GeneralQuestions, TRIGGER.Result_GQ },
            { RESULT_CODE.NoShow, TRIGGER.Result_NS },
            { RESULT_CODE.Skip, TRIGGER.Result_SK },
            { RESULT_CODE.InboundVoicemail, TRIGGER.Result_IV },
            { RESULT_CODE.NoContact, TRIGGER.Result_NC },
            { RESULT_CODE.LeftMessage, TRIGGER.Result_LM },
            { RESULT_CODE.BadNumber, TRIGGER.Result_BN },
            //{ RESULT_CODE.PreviousBadNumber, TRIGGER.Result_BN}
        };
        #endregion One West Trigger Map

        private static TRIGGER MapResultCodeToTrigger(RESULT_CODE resultCode)
        {
            if (!ResultToTrigger.ContainsKey(resultCode)) {
                String errorMsg = String.Format("Result Code for program '{0}' contains no corresponding trigger.  Result Code passed was {1}",
                    PROGRAM.OneWestSD1308.ToString(), resultCode.ToString());

                throw new ArgumentException(errorMsg);
            }

            return ResultToTrigger[resultCode];
        }

        #region One West Hierarchy
        public static List<RESULT_CODE> OneWestHierarchy = new List<RESULT_CODE> { 
            RESULT_CODE.ExclusionReport,
            RESULT_CODE.ClientNotInterested,
            RESULT_CODE.Counseled,
            RESULT_CODE.ImmediateCounseling,
            RESULT_CODE.SecondCounsel,
            RESULT_CODE.CheckIn,
            RESULT_CODE.ScheduledAppointment,
            RESULT_CODE.TransferredToCounselor,
            RESULT_CODE.RightParty,
            //RESULT_CODE.GeneralQuestions,
            RESULT_CODE.NoShow,
            /* SK - Some SK + no Right Party Contact (RPC), All SK + Some BNs or All Skip*/
            RESULT_CODE.Skip,
            RESULT_CODE.InboundVoicemail,
            RESULT_CODE.NoContact, 
            RESULT_CODE.LeftMessage,
            RESULT_CODE.BadNumber,
            RESULT_CODE.PreviousBadNumber
        };
        #endregion One West Hierarchy

        private static RESULT_CODE GetHierarchyResultCode(ContactAttempt contactAttempt)
        {
            if (contactAttempt.Outcomes == null || contactAttempt.Outcomes.Count == 0)
            {
                return RESULT_CODE.Skip;
            }

            var outcomes = contactAttempt.Outcomes.Select(o => o.Result.Value);

            if (outcomes.Except(OneWestHierarchy).Count() > 0)
            {
                String errorMsg = String.Format("Contact attempt for program '{0}' contains result codes that do not belong to this program.  Invalid result codes are: {1}",
                    PROGRAM.OneWestSD1308.ToString(), String.Join(",", outcomes.Except(OneWestHierarchy)));

                throw new ArgumentException(errorMsg);
            }

            foreach (RESULT_CODE code in OneWestHierarchy)
            {
                if (outcomes.Contains(code)) return code;
            }

            String errorMsg2 = String.Format("Contact attempt for program '{0}' contains no result codes fitting this program.  Result codes it contains are: {1}",
                PROGRAM.OneWestSD1308.ToString(),String.Join(",",outcomes.Select(o => o.ToString()).ToList()));

            throw new ArgumentException(errorMsg2);
        }
        
        #endregion Get Trigger
           
    }
}
