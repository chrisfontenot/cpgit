﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DebtPlus.Svc.OCS.Rules.FMAC180
{
    public class FMAC180ProgramConfig
    {
        public static ProgramConfig getConfig() {
            var toReturn = DebtPlus.Svc.OCS.Rules.FMACEI.FMACEIProgramConfig.getConfig();

            toReturn.Program = DebtPlus.OCS.Domain.PROGRAM.FreddieMac180;
            toReturn.ProgramDescription = "Freddie Mac - 180";
            toReturn.ReferralSource = 1366;            

            return toReturn;
        }
    }
}
