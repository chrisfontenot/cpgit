﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DebtPlus.OCS.Domain;

namespace DebtPlus.Svc.OCS.Rules.FMACEIFamily
{
    public class IntermediateConfig
    {
        #region helpers
        private static TriggerEffect t(ACTIVE_RULE setActive, STATUS_CODE? setStatus, INCREMENT_RULE incrementAttempts)
        {
            return new TriggerEffect
            {
                SetActive = setActive,
                SetStatus = setStatus,
                CalcStatus = null,
                SetQueue = QUEUE_RULE.DoNothing,
                LastChanceRules = false,
                IncrementAttempts = incrementAttempts
            };
        }

        private static TriggerEffect t(TRIGGER trigger, ACTIVE_RULE setActive, STATUS_CODE? setStatus, INCREMENT_RULE incrementAttempts)
        {
            var toReturn = t(setActive, setStatus, incrementAttempts);
            toReturn.Trigger = trigger;
            return toReturn;
        }

        private const ACTIVE_RULE Active = ACTIVE_RULE.SetActive;
        private const ACTIVE_RULE Inactive = ACTIVE_RULE.SetInactive;
        #endregion helpers

        private static GeneralScenario resultsInCB3(STATUS_CODE status)
        {
            return new GeneralScenario
            {
                StatusCode = status,
                Triggers = new List<TriggerEffect> {
                    t(TRIGGER.Result_CB, Inactive, STATUS_CODE.CounselorWillCallback3, INCREMENT_RULE.IncrementBy1),
                    t(TRIGGER.Result_CC, Active, STATUS_CODE.CSRWillCallBack, INCREMENT_RULE.IncrementBy1),
                    t(TRIGGER.Result_LM, Inactive, STATUS_CODE.CounselorWillCallback3, INCREMENT_RULE.IncrementBy1),
                    t(TRIGGER.Result_NC, Inactive, STATUS_CODE.CounselorWillCallback3, INCREMENT_RULE.IncrementBy1)
                }                
            };
        }

        private static GeneralScenario resultsInCB1NC(STATUS_CODE status)
        {
            return new GeneralScenario
            {
                StatusCode = status,
                Triggers = new List<TriggerEffect> {
                    t(TRIGGER.Result_CB, Inactive, STATUS_CODE.CounselorWillCallback1, INCREMENT_RULE.IncrementBy1),
                    t(TRIGGER.Result_CC, Active, STATUS_CODE.CSRWillCallBack, INCREMENT_RULE.IncrementBy1),
                    t(TRIGGER.Result_LM, Inactive, STATUS_CODE.NoContact, INCREMENT_RULE.IncrementBy1),
                    t(TRIGGER.Result_NC, Inactive, STATUS_CODE.NoContact, INCREMENT_RULE.IncrementBy1)
                }                
            };
        }

        public static ScenarioConfig get() {

            return new ScenarioConfig
            {
                UniversalTriggers = new List<TriggerEffect>
                {
                    t(TRIGGER.LanguageChanged, ACTIVE_RULE.DoNothing,   null, INCREMENT_RULE.ResetTo1),
                    //Determined by Call Results
                    t(TRIGGER.Result_BN, Inactive,                      STATUS_CODE.BadNumber, INCREMENT_RULE.IncrementBy1),
                    t(TRIGGER.Result_CO, Inactive,                      STATUS_CODE.Counseled, INCREMENT_RULE.IncrementBy1),
                    t(TRIGGER.Result_DS, Inactive,                      STATUS_CODE.ContactMade, INCREMENT_RULE.IncrementBy1),
                    t(TRIGGER.Result_NS, Inactive,                      STATUS_CODE.NoShow, INCREMENT_RULE.IncrementBy1),
                    t(TRIGGER.Result_NI, Inactive,                      STATUS_CODE.OptOut, INCREMENT_RULE.IncrementBy1),
                    t(TRIGGER.Result_SA, Inactive,                      STATUS_CODE.PendingAppointment, INCREMENT_RULE.IncrementBy1),
                    t(TRIGGER.Result_SK, ACTIVE_RULE.DoNothing,         null, INCREMENT_RULE.DoNothing),
                    t(TRIGGER.Result_TR, Inactive,                      STATUS_CODE.TransferToCounselor, INCREMENT_RULE.IncrementBy1),
                    t(TRIGGER.Result_WL, Inactive,                      STATUS_CODE.ContactMade, INCREMENT_RULE.IncrementBy1)
                },
                Scenarios = new List<GeneralScenario> {

new GeneralScenario
{
    StatusCode = STATUS_CODE.NewRecord,
    Triggers = new List<TriggerEffect>{
        t(TRIGGER.Result_CB, Inactive, STATUS_CODE.CounselorWillCallback1,INCREMENT_RULE.IncrementBy1),
        t(TRIGGER.Result_CC, Active, STATUS_CODE.CSRWillCallBack, INCREMENT_RULE.IncrementBy1),
        t(TRIGGER.Result_LM, Active, STATUS_CODE.NoContact, INCREMENT_RULE.IncrementBy1),
        t(TRIGGER.Result_NC, Active, STATUS_CODE.NoContact, INCREMENT_RULE.IncrementBy1)
    }
},        
new GeneralScenario
{
    StatusCode = STATUS_CODE.CounselorWillCallback1,
    Triggers = new List<TriggerEffect> {
        t(TRIGGER.Result_CB, Inactive, STATUS_CODE.CounselorWillCallback2, INCREMENT_RULE.IncrementBy1),
        t(TRIGGER.Result_CC, Active,STATUS_CODE.CSRWillCallBack,INCREMENT_RULE.IncrementBy1),
        t(TRIGGER.Result_LM, Inactive, STATUS_CODE.CounselorWillCallback2, INCREMENT_RULE.IncrementBy1),
        t(TRIGGER.Result_NC, Inactive, STATUS_CODE.CounselorWillCallback2, INCREMENT_RULE.IncrementBy1)
    }                
},
resultsInCB3(STATUS_CODE.CounselorWillCallback2),
resultsInCB3(STATUS_CODE.CounselorWillCallback3),
resultsInCB3(STATUS_CODE.Counseled),
new GeneralScenario
{
    StatusCode = STATUS_CODE.BadNumber,
    Triggers = new List<TriggerEffect>{
        t(TRIGGER.Result_CB, Inactive, STATUS_CODE.CounselorWillCallback3, INCREMENT_RULE.IncrementBy1),
        t(TRIGGER.Result_CC, Active, STATUS_CODE.CSRWillCallBack, INCREMENT_RULE.IncrementBy1),
        t(TRIGGER.Result_LM, Inactive, STATUS_CODE.CounselorWillCallback3, INCREMENT_RULE.IncrementBy1),
        t(TRIGGER.Result_NC, Inactive, STATUS_CODE.CounselorWillCallback3, INCREMENT_RULE.IncrementBy1)
    }
},
resultsInCB1NC(STATUS_CODE.ContactMade),
resultsInCB1NC(STATUS_CODE.NoShow),
resultsInCB1NC(STATUS_CODE.PendingAppointment),
resultsInCB1NC(STATUS_CODE.TransferToCounselor),
new GeneralScenario
{
    StatusCode = STATUS_CODE.OptOut,
    Triggers = new List<TriggerEffect> {
        t(TRIGGER.Result_CB, Inactive, STATUS_CODE.CounselorWillCallback1, INCREMENT_RULE.IncrementBy1),
        t(TRIGGER.Result_CC, Inactive, STATUS_CODE.CSRWillCallBack, INCREMENT_RULE.IncrementBy1),
        t(TRIGGER.Result_LM, Inactive, STATUS_CODE.NoContact, INCREMENT_RULE.IncrementBy1),
        t(TRIGGER.Result_NC, Inactive, STATUS_CODE.NoContact, INCREMENT_RULE.IncrementBy1)
    }        
},
new GeneralScenario
{
    StatusCode = STATUS_CODE.NoContact,
    Triggers = new List<TriggerEffect> {
        t(TRIGGER.Result_CB, Inactive, STATUS_CODE.CounselorWillCallback1, INCREMENT_RULE.IncrementBy1),
        t(TRIGGER.Result_CC, Inactive, STATUS_CODE.CSRWillCallBack, INCREMENT_RULE.IncrementBy1),
        t(TRIGGER.Result_LM, Active, STATUS_CODE.NoContact, INCREMENT_RULE.IncrementBy1),
        t(TRIGGER.Result_NC, Active, STATUS_CODE.NoContact, INCREMENT_RULE.IncrementBy1)
    }        
},
new GeneralScenario
{
    StatusCode = STATUS_CODE.CSRWillCallBack,
    Triggers = new List<TriggerEffect>{
        t(TRIGGER.Result_CB, Inactive, STATUS_CODE.CounselorWillCallback1, INCREMENT_RULE.IncrementBy1),
        t(TRIGGER.Result_CC, Active, STATUS_CODE.CSRWillCallBack, INCREMENT_RULE.IncrementBy1),
        t(TRIGGER.Result_LM, Active, STATUS_CODE.NoContact, INCREMENT_RULE.IncrementBy1),
        t(TRIGGER.Result_NC, Active, STATUS_CODE.NoContact, INCREMENT_RULE.IncrementBy1)
    }        
}



                }
            };        
        }
    }
}
