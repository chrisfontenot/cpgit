﻿using System;
using System.Collections.Generic;
using System.Data.SqlTypes;
using System.Text.RegularExpressions;
using DebtPlus.LINQ;
using DebtPlus.LINQ.BusinessLayer;
using DebtPlus.OCS.Domain;
using DebtPlus.Svc.OCS;
using DebtPlus.Svc.OCS.Rules;

namespace DebtPlus.UI.Housing.OCS
{
    public class FMACExcelMap
    {
        private static Regex sanitizePhone = new Regex(@"\D");

        private static Action<object, OCSClient, LINQ.OCS_UploadRecord> getMapPhoneNumber(string columnName)
        {
            return (val, client, upload) =>
            {
                if (String.IsNullOrWhiteSpace(val.ToString()))
                {
                    return;
                }

                if (!CommonValidation.IsPhone(val.ToString()))
                {
                    return;
                }

                if (client.PhoneNumbers == null)
                {
                    client.PhoneNumbers = new List<TelephoneNumber>();
                }

                String result = sanitizePhone.Replace(val.ToString(), "");

                if (result.Length == 11)
                {
                    result = result.Substring(1);
                }                
                
                var newNumber = new TelephoneNumber
                {
                    SetNumber = result
                };

                if (client.PhoneNumbers.Exists(t => t.SearchValue == newNumber.SearchValue)) {
                    return;
                }

                switch (columnName)
                {
                    case "phone1":
                        upload.Phone1 = result;
                        break;
                    case "phone2":
                        upload.Phone2 = result;
                        break;
                    case "phone3":
                        upload.Phone3 = result;
                        break;
                    case "phone4":
                        upload.Phone4 = result;
                        break;
                    case "phone5":
                        upload.Phone5 = result;
                        break;
                }
                
                client.PhoneNumbers.autoAssignNumber(newNumber);
                client.PhoneNumbers.Add(newNumber);
            };
        }
                
        public static Dictionary<string, Action<string, OCSClient, LINQ.OCS_UploadRecord>> fieldConvertActions = new Dictionary<string, Action<string, OCSClient, LINQ.OCS_UploadRecord>> { 

            //special map function that gets called after all field-level mappings
            {"GLOBAL RECORD MAPPER", (val, client, upload) => {
                client.PreferredLanguage = Person.LANGUAGE.English;
                client.ContactAttemptCount = 0;
                client.ActiveFlag = client.PhoneNumbers != null && client.PhoneNumbers.Count > 0;
            } },
            { "fmac lnno", (val, client, upload) => {
                upload.InvestorNumber = val.ToString();
                client.InvestorLoanNo = val.ToString(); 
            } },
            { "servicer",  (val, client, upload) => {
                upload.Servicer = val.ToString();
                client.Servicer = val.ToString(); 
            } },
            { "loan #", (val, client, upload) => {
                upload.LoanNumber = val.ToString();
                client.LoanNo = val.ToString(); 
            } },
            { "last name", (val, client, upload) => {
                upload.LastName1 = val.ToString();
                if (client.Person1 == null) { client.Person1 = new Person(); }
                client.Person1.LastName = val.ToString(); 
            } },
            { "first name", (val, client, upload) => {
                upload.FirstName1 = val.ToString();
                if (client.Person1 == null) { client.Person1 = new Person(); }
                client.Person1.FirstName = val.ToString(); 
            } },
            { "ssn", (val, client, upload) => {
                upload.SSN1 = val.ToString();
                if (client.Person1 == null) { client.Person1 = new Person(); }
                var str = val.ToString();
                if (!String.IsNullOrWhiteSpace(str))
                    client.Person1.SSN = str;
            } },
            { "last name (2)", (val, client, upload) => {
                upload.LastName2 = val.ToString();

                if (client.Person2 == null && !String.IsNullOrWhiteSpace(val.ToString())) { client.Person2 = new Person(); }

                if(client.Person2 != null) {
                    client.Person2.LastName = val.ToString(); 
                }                
            } },
            { "first name (2)", (val, client, upload) => {
                upload.FirstName2 = val.ToString();

                if (client.Person2 == null && !String.IsNullOrWhiteSpace(val.ToString())) { client.Person2 = new Person(); }

                if(client.Person2 != null){
                    client.Person2.FirstName = val.ToString(); 
                }                
            } },
            { "ssn (2)", (val, client, upload) => {
                upload.SSN2 = val.ToString();
                
                if (client.Person2 == null && !String.IsNullOrWhiteSpace(val.ToString())) { 
                    client.Person2 = new Person(); 
                }

                if(client.Person2 != null) {
                    var str = val.ToString();
                    if (!String.IsNullOrWhiteSpace(str))
                        client.Person2.SSN = str;
                }
                
            } },
            { "street", (val, client, upload) => {
                upload.ClientStreet = val.ToString();
                if (client.Address == null) { client.Address = new Address(); }
                client.Address.street = val.ToString();
            } },
            { "city", (val, client, upload) => {
                upload.ClientCity = val.ToString();
                if (client.Address == null) { client.Address = new Address(); }
                client.Address.city = val.ToString();
            } },
            { "state", (val, client, upload) => {
                upload.ClientState = val.ToString();
                if (client.Address == null) { client.Address = new Address(); }
                if(!String.IsNullOrWhiteSpace(val.ToString())){
                    client.Address.StateProvince = (US_STATE)Enum.Parse(typeof(US_STATE), val.ToString().ToUpper(), true);
                }                
            } },
            { "zp", (val, client, upload) => {
                upload.ClientZipcode = val.ToString();
                if (client.Address == null) { client.Address = new Address(); }
                client.Address.PostalCode = val.ToString();
            } },
            { "phone1", getMapPhoneNumber("phone1") },
            { "phone2", getMapPhoneNumber("phone2") },
            { "phone3", getMapPhoneNumber("phone3") },
            { "phone4", getMapPhoneNumber("phone4") },
            { "phone5", getMapPhoneNumber("phone5") },
            { "last chance list", (val, client, upload) => {

                DateTime _out;

                if(!String.IsNullOrEmpty(val.ToString()) && DateTime.TryParse(val.ToString(),out _out)){
                    client.LastChanceList = _out;
                    upload.InvestorLastChanceList = client.LastChanceList;
                }else{
                    client.LastChanceList = SqlDateTime.MinValue.Value;
                    upload.InvestorLastChanceList = null;
                }
            } },
            { "actual sale date", (val, client, upload) => {

                DateTime _out;

                if (!String.IsNullOrEmpty(val.ToString()) && DateTime.TryParse(val.ToString(), out _out))
                {
                    client.ActualSaleDate = _out;
                    upload.InvestorActualSaleDate = client.ActualSaleDate;
                }else{
                    client.ActualSaleDate = SqlDateTime.MinValue.Value;
                    upload.InvestorActualSaleDate = null;
                }
            } },
            { "due date", (val, client, upload) => {

                DateTime _out;

                if (!String.IsNullOrEmpty(val.ToString()) && DateTime.TryParse(val.ToString(), out _out))
                {
                    client.DueDate = _out;
                    upload.InvestorDueDate = client.DueDate;
                }else{
                    client.DueDate = SqlDateTime.MinValue.Value;
                    upload.InvestorDueDate = null;
                }
            } },
            { "modtrialtypedescription", (val, client, upload) => {
                //do nothing but put in upload table until we get some clarity
                upload.Description = val.ToString();
            } },
            // TRIALPERIODSTARTDATE
            { "trialperiodstartdate", (val, client, upload) => {
                //do nothing but put in upload table until we get some clarity
                DateTime _out;

                if (!String.IsNullOrEmpty(val.ToString()) && DateTime.TryParse(val.ToString(),out _out))
                {
                    upload.TrialPeriodStartDate = _out;
                }
                else
                {
                    upload.TrialPeriodStartDate = null;
                }
            } }
        };

        private static Func<string,bool> dateValidator = (val) => {

            if (!String.IsNullOrEmpty(val.ToString()))
            {
                DateTime _out;
                return DateTime.TryParse(val.ToString(),out _out);
            }

            return true;             
        };

        public static Dictionary<string, Func<string, bool>> fieldValidationFuncs = new Dictionary<string, Func<string, bool>> { 
            { "fmac lnno", (val) => {
                return String.IsNullOrWhiteSpace(val.ToString()) == false;
            } },
            { "servicer",  (val) => { return true; } },
            { "loan #", (val) => { return true; } },
            { "last name", (val) => { return true; } },
            { "first name", (val) => { return true; } },
            { "ssn", (val) => { return true; } },
            { "last name (2)", (val) => { return true; } },
            { "first name (2)", (val) => { return true; } },
            { "ssn (2)", (val) => { return true; } },
            { "street", (val) => { return true; } },
            { "city", (val) => { return true; } },
            { "state", (val) => { return true; } },
            { "zp", (val) => { return true; } },
            { "phone1", (val) => { return val.ToString().Trim() == String.Empty || CommonValidation.IsPhone(val.ToString()); } },
            { "phone2", (val) => { return val.ToString().Trim() == String.Empty || CommonValidation.IsPhone(val.ToString()); } },
            { "phone3", (val) => { return val.ToString().Trim() == String.Empty || CommonValidation.IsPhone(val.ToString()); } },
            { "phone4", (val) => { return val.ToString().Trim() == String.Empty || CommonValidation.IsPhone(val.ToString()); } },
            { "phone5", (val) => { return val.ToString().Trim() == String.Empty || CommonValidation.IsPhone(val.ToString()); } },
            { "last chance list",  dateValidator },
            { "actual sale date", dateValidator },
            { "due date", dateValidator },
            { "modtrialtypedescription",(val) => { return true; } },
            { "trialperiodstartdate",   (val) => { return true; } }
        };
    }
}
