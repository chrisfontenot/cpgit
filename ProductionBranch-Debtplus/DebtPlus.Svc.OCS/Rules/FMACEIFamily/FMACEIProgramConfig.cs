﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DebtPlus.OCS.Domain;

namespace DebtPlus.Svc.OCS.Rules.FMACEI
{
    public class FMACEIProgramConfig
    {
        public static ProgramConfig getConfig() {
            var toReturn = DefaultProgramConfig.getConfig();

            toReturn.Program = DebtPlus.OCS.Domain.PROGRAM.FreddieMacEI;
            toReturn.ProgramDescription = "Freddie Mac - EI";
            toReturn.ReferralSource = 822;
            toReturn.RightPartyContacts = new List<RESULT_CODE> { 
                    RESULT_CODE.Counseled, //4
                    RESULT_CODE.ClientNotInterested,//9
                    RESULT_CODE.ScheduledAppointment,//10
                    RESULT_CODE.TransferredToCounselor,//12
                    RESULT_CODE.WorkingWithLender,//13 
                    RESULT_CODE.CounselorWillCallBack,//2
                    RESULT_CODE.Deceased//5
                };
            toReturn.ResultCodes = new Dictionary<string, RESULT_CODE> { 
                {"Not Set",RESULT_CODE.NotSet},
                {"BN - Bad Number",RESULT_CODE.BadNumber},
                {"PBN - Previous Bad Number",RESULT_CODE.PreviousBadNumber},
                {"CC - CSR Will Call Back",RESULT_CODE.CSRWillCallBack},
                {"CB - Counselor Will Call Back",RESULT_CODE.CounselorWillCallBack},
                {"CO - Counseled",RESULT_CODE.Counseled},
                {"DS - Deceased",RESULT_CODE.Deceased},
                {"LM - Left Message",RESULT_CODE.LeftMessage},
                {"NC - No Contact",RESULT_CODE.NoContact},
                {"NS - No Show",RESULT_CODE.NoShow},
                {"NI - Client Not Interested",RESULT_CODE.ClientNotInterested},
                {"SA - Scheduled Appointment",RESULT_CODE.ScheduledAppointment},
                {"SK - Skip",RESULT_CODE.Skip},
                {"TR - Transferred To Counselor",RESULT_CODE.TransferredToCounselor},
                { "WL - Working With Lender", RESULT_CODE.WorkingWithLender }//,

                /*All of the following come from the legacy CredAbility database*/
                /*
                { "PBN - DEPRECATED", RESULT_CODE.DEP_PBN },
                { "CL - DEPRECATED", RESULT_CODE.DEP_CL },
                { "MC - DEPRECATED", RESULT_CODE.DEP_MC },
                { "VN - DEPRECATED", RESULT_CODE.DEP_VN },
                { "VM - DEPRECATED", RESULT_CODE.DEP_VM },
                { "Blank - DEPRECATED", RESULT_CODE.DEP_BLANK },
                { "MI - DEPRECATED", RESULT_CODE.DEP_MI },
                { "M1 - DEPRECATED", RESULT_CODE.DEP_M1 },
                { "EM - DEPRECATED", RESULT_CODE.DEP_EM },
                { "RI - DEPRECATED", RESULT_CODE.DEP_RI },
                { "BZ - DEPRECATED", RESULT_CODE.DEP_BZ },
                { "CD - DEPRECATED", RESULT_CODE.DEP_CD },
                { "DC - DEPRECATED", RESULT_CODE.DEP_DC }
                */
            };
            toReturn.ExcelPrimaryKeyColumn = "FMAC LNNO";
            toReturn.Servicers = new Dictionary<string, int> { 
                { "Chase Mortgage - 806", 806 },
                { "CitiMortgage - 815", 815 },
                { "Bank of America - 726", 726 },
                { "PNC - 1257", 1257 },
                { "U.S. Bank - 1448", 1448 },
                { "Unknown - 442", 442 }
            };

            toReturn.StatusCodes = new Dictionary<string, STATUS_CODE> { 
                    {"New Record",STATUS_CODE.NewRecord},
                    {"CO - Counseled",STATUS_CODE.Counseled},
                    {"CB1 - Counselor Will Call Back",STATUS_CODE.CounselorWillCallback1},
                    {"CB2 - Counselor Will Call Back",STATUS_CODE.CounselorWillCallback2},
                    {"CB3 - Counselor Will Call Back",STATUS_CODE.CounselorWillCallback3},
                    {"CT - Contact Made",STATUS_CODE.ContactMade},
                    {"OP - Opt Out",STATUS_CODE.OptOut},
                    {"PE - Pending Appointment",STATUS_CODE.PendingAppointment},
                    {"TR - Transfer To Counselor", STATUS_CODE.TransferToCounselor},
                    {"CC - CSR Will Call Back",STATUS_CODE.CSRWillCallBack},
                    {"NS - No Show",STATUS_CODE.NoShow},
                    {"NC - No Contact",STATUS_CODE.NoContact},                                        
                    {"BN - Bad Number",STATUS_CODE.BadNumber},
                    { "Blank - DEPRECATED", STATUS_CODE.DEP_BLANK  },
                    { "MC - Deprecated", STATUS_CODE.DEP_MC },
                    { "VM - Deprecated", STATUS_CODE.DEP_VM },
                    { "VN - Deprecated", STATUS_CODE.DEP_VN },
                    { "WS - Deprecated", STATUS_CODE.DEP_WS },
                    { "MT - Deprecated", STATUS_CODE.DEP_MT },
                    { "SP - Deprecated", STATUS_CODE.DEP_SP },
                    { "MZ - Deprecated", STATUS_CODE.DEP_MZ },
                    { "BZ - Deprecated", STATUS_CODE.DEP_BZ }
                };
            
            return toReturn;
        }
    }
}
