﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DebtPlus.Svc.OCS.Rules.FMACHAMP
{
    public class FMACHAMPProgramConfig
    {
        public static ProgramConfig getConfig() {
            var toReturn = DebtPlus.Svc.OCS.Rules.FMACEI.FMACEIProgramConfig.getConfig();

            toReturn.Program = DebtPlus.OCS.Domain.PROGRAM.FreddieMacHamp;
            toReturn.ProgramDescription = "Freddie Mac  -  HAMP";
            toReturn.ReferralSource = 5;            

            return toReturn;
        }
    }
}
