﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DebtPlus.OCS.Domain;

namespace DebtPlus.Svc.OCS.Rules.FMACEI
{
    public class StateMachine
    {
        public static OCSClient UpdateOnTrigger(TRIGGER passedTrigger, STATUS_CODE priorStatusCode, OCSClient client)
        {
            List<IScenario> scenarios = ScenarioConfigLoader.getFMACFamilyScenarios();
            
            var currentScenario = scenarios.FirstOrDefault(st => st.StatusCode == priorStatusCode);
            if (currentScenario == null) {
                throw new ArgumentException("No valid scenario is defined for a FMAC EI with a status of " + priorStatusCode.ToString());
            }

            TRIGGER triggerToUse = GetTrigger(passedTrigger,client.CurrentAttempt);
            
            var currentTransition = currentScenario.GetTriggerEffect(triggerToUse);

            var toReturn = StateExecutor.Execute(currentTransition, client);

            //Change language is a bit of a special case for FMAC EI.  It alters the attempt count back to 1, but otherwise makes no change.
            if (passedTrigger == TRIGGER.LanguageChanged)
            {
                toReturn = StateExecutor.Execute(currentScenario.GetTriggerEffect(TRIGGER.LanguageChanged), toReturn);            
            }

            return toReturn;
        }

        #region Get Trigger

        private static TRIGGER GetTrigger(TRIGGER passedTrigger, ContactAttempt contactAttempt)
        {
            TRIGGER toReturn = passedTrigger;

            if (contactAttempt != null)
            {
                //set 'not set' to 'skip'
                contactAttempt.Outcomes.Where(o => !o.Result.HasValue || o.Result.Value == RESULT_CODE.NotSet).ToList()
                    .ForEach(o => { o.Result = RESULT_CODE.Skip; });

                //get right result code from heirarchy
                contactAttempt.ResultCode = GetHierarchyResultCode(contactAttempt);

                //map to trigger
                toReturn = MapResultCodeToTrigger(contactAttempt.ResultCode);
            }

            return toReturn;
        }

        #region FMAC PostMod Trigger Map
        private static Dictionary<RESULT_CODE, TRIGGER> ResultToTrigger = new Dictionary<RESULT_CODE, TRIGGER> { 
            { RESULT_CODE.Counseled, TRIGGER.Result_CO },            
            { RESULT_CODE.CounselorWillCallBack, TRIGGER.Result_CB },
            { RESULT_CODE.Deceased, TRIGGER.Result_DS },
            { RESULT_CODE.WorkingWithLender, TRIGGER.Result_WL },
            { RESULT_CODE.ClientNotInterested, TRIGGER.Result_NI },
            { RESULT_CODE.ScheduledAppointment, TRIGGER.Result_SA },
            { RESULT_CODE.TransferredToCounselor, TRIGGER.Result_TR },
            { RESULT_CODE.Skip, TRIGGER.Result_SK },
            { RESULT_CODE.CSRWillCallBack, TRIGGER.Result_CC },
            { RESULT_CODE.NoShow, TRIGGER.Result_NS },
            { RESULT_CODE.NoContact, TRIGGER.Result_NC },
            { RESULT_CODE.LeftMessage, TRIGGER.Result_LM },
            { RESULT_CODE.BadNumber, TRIGGER.Result_BN },
            { RESULT_CODE.PreviousBadNumber, TRIGGER.Result_BN}
        };
        #endregion FMAC PostMod Trigger Map

        private static TRIGGER MapResultCodeToTrigger(RESULT_CODE resultCode)
        {
            if (!ResultToTrigger.ContainsKey(resultCode)) {
                String errorMsg = String.Format("Result Code for program '{0}' contains no corresponding trigger.  Result Code passed was {1}",
                    PROGRAM.FreddieMacPostMod.ToString(), resultCode.ToString());

                throw new ArgumentException(errorMsg);
            }

            return ResultToTrigger[resultCode];
        }

        #region FMAC PostMod Hierarchy
        /*
Level	Status Code	Result Code
1	CO-Counsel	CO
2	CB1	CB
2	CB2	CB
2	CB3	CB
2	CT	DS, WL
2	OP	NI
2	PE	SA
2	P1	TR
2	CC	CC
3	NS	NS
3	SK	SK, or Blank
3	NC	NC, LM
4	BN	All BNs
*/
        public static List<RESULT_CODE> FMACPostModHierarchy = new List<RESULT_CODE> { 
            RESULT_CODE.Counseled,            
            RESULT_CODE.CounselorWillCallBack,
            RESULT_CODE.Deceased,
            RESULT_CODE.WorkingWithLender,
            RESULT_CODE.ClientNotInterested,
            RESULT_CODE.ScheduledAppointment,
            RESULT_CODE.TransferredToCounselor,
            RESULT_CODE.CSRWillCallBack,
            RESULT_CODE.NoShow,
            RESULT_CODE.Skip,
            RESULT_CODE.NoContact,  
            RESULT_CODE.LeftMessage,
            RESULT_CODE.BadNumber,
            RESULT_CODE.PreviousBadNumber
        };
        #endregion FMAC PostMod Hierarchy

        private static RESULT_CODE GetHierarchyResultCode(ContactAttempt contactAttempt)
        {
            if (contactAttempt.Outcomes == null || contactAttempt.Outcomes.Count == 0) {
                return RESULT_CODE.Skip;
            }

            var outcomes = contactAttempt.Outcomes.Select(o => o.Result.Value);

            if (outcomes.Except(FMACPostModHierarchy).Count() > 0) {
                String errorMsg = String.Format("Contact attempt for program contains result codes that do not belong to this program.  Invalid result codes are: {1}",
                    String.Join(",",outcomes.Except(FMACPostModHierarchy)));

                throw new ArgumentException(errorMsg);
            }

            foreach (RESULT_CODE code in FMACPostModHierarchy) {
                if (outcomes.Contains(code)) return code;
            }

            String errorMsg2 = String.Format("Contact attempt for program contains no result codes fitting this program.  Result codes it contains are: {1}",
                String.Join(",",outcomes.Select(o => o.ToString()).ToList()));

            throw new ArgumentException(errorMsg2);
        }
        
        #endregion Get Trigger
           
    }
}
