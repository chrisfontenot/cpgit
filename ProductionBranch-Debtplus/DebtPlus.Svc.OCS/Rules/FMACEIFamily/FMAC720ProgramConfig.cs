﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DebtPlus.Svc.OCS.Rules.FMAC720
{
    public class FMAC720ProgramConfig
    {
        public static ProgramConfig getConfig() {
            var toReturn = DebtPlus.Svc.OCS.Rules.FMACEI.FMACEIProgramConfig.getConfig();

            toReturn.Program = DebtPlus.OCS.Domain.PROGRAM.FreddieMac720;
            toReturn.ProgramDescription = "Freddie Mac - 720";
            toReturn.ReferralSource = 1367;

            return toReturn;
        }
    }
}
