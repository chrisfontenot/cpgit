﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DebtPlus.OCS.Domain;

namespace DebtPlus.Svc.OCS.Rules.FMAEPostMod
{
    public class FMAEPostModConfig
    {
        public static ProgramConfig getConfig()
        {
            var toReturn = DefaultProgramConfig.getConfig();

            toReturn.Program = PROGRAM.FannieMaePostMod;
            toReturn.ProgramDescription = "Fannie Mae - Post Mod";

            toReturn.RightPartyContacts = new List<RESULT_CODE> { 
                RESULT_CODE.Counseled,
                RESULT_CODE.ImmediateCounseling,
                RESULT_CODE.RightParty,
                RESULT_CODE.ScheduledAppointment,
                RESULT_CODE.GeneralQuestions,
                RESULT_CODE.SecondCounsel
            };
            toReturn.ReferralSource = 415;
            toReturn.ResultCodes = new Dictionary<string, RESULT_CODE> { 
                {"Not Set",RESULT_CODE.NotSet},
                {"BN - Bad Number",RESULT_CODE.BadNumber},
                {"PBN - Previous Bad Number",RESULT_CODE.PreviousBadNumber},
                {"SK - Skip",RESULT_CODE.Skip},
                {"LM - Left Message",RESULT_CODE.LeftMessage},
                {"NS - No Show",RESULT_CODE.NoShow},
                {"NC - No Contact",RESULT_CODE.NoContact},
                {"IV - Inbound Voicemail Message",RESULT_CODE.InboundVoicemail},
                {"RP - Right Party",RESULT_CODE.RightParty},
                {"CH - Check In",RESULT_CODE.CheckIn},
                {"TR - Transferred To Counselor",RESULT_CODE.TransferredToCounselor},
                {"SA - Scheduled Appointment",RESULT_CODE.ScheduledAppointment},
                {"SCO - Second Counseling",RESULT_CODE.SecondCounsel},
                {"IM - Immediate Counseling",RESULT_CODE.ImmediateCounseling},
                {"CO - Counseled",RESULT_CODE.Counseled},
                {"NI - Client Not Interested",RESULT_CODE.ClientNotInterested},
                {"EX - ExclusionReport",RESULT_CODE.ExclusionReport}
            };

            toReturn.Servicers = new Dictionary<string, int> { 
                { "Chase Mortgage - 806", 806 },
                { "CitiMortgage - 815", 815 },
                { "Bank of America - 726", 726 },
                { "PNC - 1257", 1257 },
                { "U.S. Bank - 1448", 1448 },
                { "Unknown - 442", 442 }
            };
            toReturn.StatusCodes = new Dictionary<string, STATUS_CODE> { 
                    {"New Record",STATUS_CODE.NewRecord},
                    {"CO - Counseled",STATUS_CODE.Counseled},
                    {"IM - Immediate Counseling",STATUS_CODE.ImmediateCounseling},
                    {"SCO - Second Counseling", STATUS_CODE.SecondCounseling},
                    {"C1 - Initial Contact Attempt 1",STATUS_CODE.InitialContactAttempt1},
                    {"C2 - Initial Contact Attempt 2",STATUS_CODE.InitialContactAttempt2},
                    {"C3 - Initial Contact Attempt 3",STATUS_CODE.InitialContactAttempt3},
                    {"OT - Extra Contact Attempts",STATUS_CODE.InitialContactAttemptExtras},
                    {"OP - Opt Out",STATUS_CODE.OptOut},
                    {"BN - Bad Number",STATUS_CODE.BadNumber},
                    {"PE - Pending Appointment",STATUS_CODE.PendingAppointment},
                    {"NS - No Show",STATUS_CODE.NoShow},
                    {"P1 - Payment Reminder 1",STATUS_CODE.PaymentReminder1},
                    {"P2 - Payment Reminder 2",STATUS_CODE.PaymentReminder2},
                    {"P3 - Payment Reminder 3",STATUS_CODE.PaymentReminder3},
                    {"P4 - Payment Reminder 4",STATUS_CODE.PaymentReminder4},
                    {"P5 - Payment Reminder 5",STATUS_CODE.PaymentReminder5},
                    {"P6 - Payment Reminder 6",STATUS_CODE.PaymentReminder6},
                    {"P7 - Payment Reminder 7",STATUS_CODE.PaymentReminder7},
                    {"P8 - Payment Reminder 8",STATUS_CODE.PaymentReminder8},
                    {"P9 - Payment Reminder 9",STATUS_CODE.PaymentReminder9},
                    {"P10 - Payment Reminder 10",STATUS_CODE.PaymentReminder10},
                    {"P11 - Payment Reminder 11",STATUS_CODE.PaymentReminder11},
                    {"P12 - Payment Reminder 12",STATUS_CODE.PaymentReminder12},
                    {"GQ - General Questions",STATUS_CODE.GeneralQuestions},
                    {"TR - Transfer To Counselor", STATUS_CODE.TransferToCounselor},
                    {"NC - No Contact",STATUS_CODE.NoContact},

                    {"CT - Contact Made",STATUS_CODE.ContactMade},
                    {"EX - Exclusion Report",STATUS_CODE.ExclusionReport}
                };

            return toReturn;
        }
    }
}