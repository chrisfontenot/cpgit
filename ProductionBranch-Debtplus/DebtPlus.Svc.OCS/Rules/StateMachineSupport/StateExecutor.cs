﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DebtPlus.OCS.Domain;
using DebtPlus.LINQ;
using DebtPlus.Svc.OCS.Rules.StateMachineSupport;

namespace DebtPlus.Svc.OCS.Rules
{
    /// <summary>
    /// This and the configuration are the "heart" of the state machine implementation.  
    /// This class's "execute" function is what determines what happens when various configurations are set.
    /// </summary>
    public class StateExecutor
    {
        /// <summary>
        /// Alter the client/take other actions according to the rules defined in the TriggerEffect
        /// </summary>
        /// <param name="transition"></param>
        /// <param name="client"></param>
        /// <returns></returns>
        public static OCSClient Execute(TriggerEffect transition, OCSClient client) {

            if (transition.SetActive.HasValue == false || transition.SetActive.Value == ACTIVE_RULE.DoNothing) { 
                //do nothing
            } else if (transition.SetActive.Value == ACTIVE_RULE.SetActive) {
                client.ActiveFlag = true;
            } else if (transition.SetActive.Value == ACTIVE_RULE.SetInactive) {
                client.ActiveFlag = false;
            }

            

            /* check if the result codes contains any of the following result related to counseling
             * - Immediate Counseling
             * - Counseled
             * - Second Counseling
             * Set First Contact Date if it is not set already
             * if First Contact Date is already set, set the Second Contact Date
             * Do not update First Contact Date and Second Contact Date once set
             * Do Not set First Contact Date And Second Contact Date at the same time
             */

            bool isCounseled = HasCounseledResultStatus(client.CurrentAttempt);

            bool isFirstRpc = false;
            if (isCounseled == true)
            {
                if (client.FirstCounselDate == null || client.FirstCounselDate == DateTime.MinValue)
                {
                    isFirstRpc = true;
                    client.FirstCounselDate = DateTime.Now;
                }
                else
                {
                    if (client.SecondCounselDate == null || client.SecondCounselDate == DateTime.MinValue)
                    {
                        client.SecondCounselDate = DateTime.Now;
                    }
                }
            }

            if (transition.SetStatus.HasValue)
            {
                // Handle the condition 
                // If first RPC, then P1, else Pn(t)
                if (transition.HasRpcCondition == true)
                {
                    if (isFirstRpc == true)
                    {
                        client.StatusCode = STATUS_CODE.PaymentReminder1;
                    }
                    else
                    {
                        client.StatusCode = Calculations.CalculatePnOfT(client);
                    }
                }
                else
                {
                    client.StatusCode = transition.SetStatus.Value;
                }

                //right now we only need to accommodate one calculation...later we'll need to retrieve the right one for the enum
            }
            else if (transition.CalcStatus.HasValue)
            {
                //client.StatusCode = Calculations.CalculatePnOfT(client);
                switch (transition.CalcStatus)
                {
                    case STATUS_FUNC.PnOfT:
                        client.StatusCode = Calculations.CalculatePnOfT(client);
                        break;
                    case STATUS_FUNC.CnOfT:
                        client.StatusCode = Calculations.CalculateCnOfT(client);
                        break;
                    default:
                        client.StatusCode = Calculations.CalculatePnOfT(client);
                        break;
                }
            }
            //do nothing
            else
            {
            }


            bool isRpc = RpcHelper.HasRpcResultCode(client.Program, client.CurrentAttempt);

            if (isRpc)
            {
                client.LastRpcDate = DateTime.Now;
            }

            if (transition.SetQueue.HasValue == false || transition.SetQueue.Value == QUEUE_RULE.DoNothing) { 
                //do nothing
            } else if (transition.SetQueue.Value == QUEUE_RULE.SetCounselor) {
                client.Queue = OCS_QUEUE.Counselor;
            }
            else if (transition.SetQueue.Value == QUEUE_RULE.SetHighTouch) {
                client.Queue = OCS_QUEUE.HighTouch;
            }

            if (transition.LastChanceRules.HasValue == false || transition.LastChanceRules.Value == false) { 
                //do nothing
            } else if (transition.LastChanceRules.Value == true) {
                Calculations.CalculatePostModLastChance(client);
            }

            if (transition.IncrementAttempts.HasValue == false) { 
                //do nothing
            } else if (transition.IncrementAttempts.Value == INCREMENT_RULE.ResetTo1) {
                client.ContactAttemptCount = 1;
            } else if (transition.IncrementAttempts.Value == INCREMENT_RULE.IncrementBy1) {
                client.ContactAttemptCount++;
            }
            
            return client;
        }


        /// <summary>
        /// This method is used to check if the contact attempts has at least one result code related to counselling
        /// </summary>
        /// <param name="client"></param>
        /// <returns>True if the contact attempts has at least one instance of the IM, CO or SCO result code </returns>
        private static bool HasCounseledResultStatus(ContactAttempt attempt)
        {
            bool isCounseled = false;
            if (attempt != null && attempt.Outcomes != null && attempt.Outcomes.Count > 0)
            {
                var outcomes = attempt.Outcomes;
                foreach (var o in outcomes)
                {
                    if (o.Result == RESULT_CODE.ImmediateCounseling || 
                        o.Result == RESULT_CODE.Counseled ||
                        o.Result == RESULT_CODE.SecondCounsel)
                    {
                        isCounseled = true;
                        break;
                    }
                }
            }
            return isCounseled;
        }
    }
}
