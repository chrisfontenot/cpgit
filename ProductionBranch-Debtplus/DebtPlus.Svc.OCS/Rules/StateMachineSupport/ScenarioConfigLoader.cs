﻿using System;
using System.Collections.Generic;
using System.Linq;
using Newtonsoft.Json;

namespace DebtPlus.Svc.OCS.Rules
{
    public class ScenarioConfig
    {
        public List<TriggerEffect> UniversalTriggers { get; set; }
        public List<GeneralScenario> Scenarios { get; set; }
    }

    public static class ScenarioConfigLoader
    {

        private static String FMACFamilyConfigPath = System.IO.Path.Combine(
            System.IO.Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().Location),
            "Rules",
            "FMACEIFamily",
            "FMACFamilyScenarioConfig.json");
        private static readonly List<IScenario> FMACFamilyScenarios = load(FMACFamilyConfigPath);
        public static List<IScenario> getFMACFamilyScenarios()
        {
            return FMACFamilyScenarios;
        }

        private static String PostModHighTouchConfigPath = System.IO.Path.Combine(
            System.IO.Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().Location),
            "Rules",
            "FMACPostMod",
            "HighTouchQScenarioConfig.json");
        private static readonly List<IScenario> PostModHighTouchScenarios = load(PostModHighTouchConfigPath);
        public static List<IScenario> getPostModHighTouchScenarios()
        {
            return PostModHighTouchScenarios;
        }

        private static String PostModCounselorConfigPath = System.IO.Path.Combine(
        System.IO.Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().Location),
            "Rules",
            "FMACPostMod",
            "CounselorQScenarioConfig.json");
        private static readonly List<IScenario> PostModCounselorScenarios = load(PostModCounselorConfigPath);
        public static List<IScenario> getPostModCounselorScenarios()
        {
            return PostModCounselorScenarios;
        }

        private static String FMaePostModHighTouchConfigPath = System.IO.Path.Combine(
            System.IO.Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().Location),
            "Rules",
            "FMAEPostMod",
            "HighTouchQScenarioConfig.json");
        private static readonly List<IScenario> FMaePostModHighTouchScenarios = load(FMaePostModHighTouchConfigPath);
        public static List<IScenario> getFMaePostModHighTouchScenarios()
        {
            return FMaePostModHighTouchScenarios;
        }

        private static String FMaePostModCounselorConfigPath = System.IO.Path.Combine(
        System.IO.Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().Location),
            "Rules",
            "FMAEPostMod",
            "CounselorQScenarioConfig.json");
        private static readonly List<IScenario> FMaePostModCounselorScenarios = load(FMaePostModCounselorConfigPath);
        public static List<IScenario> getFMaePostModCounselorScenarios()
        {
            return FMaePostModCounselorScenarios;
        }

        private static String NationStarHighTouchConfigPath = System.IO.Path.Combine(
            System.IO.Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().Location),
            "Rules",
            "NationStar",
            "HighTouchQScenarioConfig.json");
        private static readonly List<IScenario> NationStarHighTouchScenarios = load(NationStarHighTouchConfigPath);
        public static List<IScenario> getNationStarHighTouchScenarios()
        {
            return NationStarHighTouchScenarios;
        }

        private static String NationStarCounselorConfigPath = System.IO.Path.Combine(
        System.IO.Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().Location),
            "Rules",
            "NationStar",
            "CounselorQScenarioConfig.json");
        private static readonly List<IScenario> NationStarCounselorScenarios = load(NationStarCounselorConfigPath);
        public static List<IScenario> getNationStarCounselorScenarios()
        {
            return NationStarCounselorScenarios;
        }

        private static String OneWestHighTouchConfigPath = System.IO.Path.Combine(
            System.IO.Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().Location),
            "Rules",
            "OneWest",
            "HighTouchQScenarioConfig.json");
        private static readonly List<IScenario> OneWestHighTouchScenarios = load(OneWestHighTouchConfigPath);
        public static List<IScenario> getOneWestHighTouchScenarios()
        {
            return OneWestHighTouchScenarios;
        }

        private static String OneWestCounselorConfigPath = System.IO.Path.Combine(
        System.IO.Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().Location),
            "Rules",
            "OneWest",
            "CounselorQScenarioConfig.json");
        private static readonly List<IScenario> OneWestCounselorScenarios = load(OneWestCounselorConfigPath);
        public static List<IScenario> getOneWestCounselorScenarios()
        {
            return OneWestCounselorScenarios;
        }

        private static String USBankHighTouchConfigPath = System.IO.Path.Combine(
            System.IO.Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().Location),
            "Rules",
            "USBank",
            "HighTouchQScenarioConfig.json");
        private static readonly List<IScenario> USBankHighTouchScenarios = load(USBankHighTouchConfigPath);
        public static List<IScenario> getUSBankHighTouchScenarios()
        {
            return USBankHighTouchScenarios;
        }

        private static String USBankCounselorConfigPath = System.IO.Path.Combine(
        System.IO.Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().Location),
            "Rules",
            "USBank",
            "CounselorQScenarioConfig.json");
        private static readonly List<IScenario> USBankCounselorScenarios = load(USBankCounselorConfigPath);
        public static List<IScenario> getUSBankCounselorScenarios()
        {
            return USBankCounselorScenarios;
        }

        private static List<IScenario> load(string filePath)
        {

            var str = System.IO.File.ReadAllText(filePath);
            var cfg = JsonConvert.DeserializeObject<ScenarioConfig>(str);

            return load(cfg);
        }

        public static List<IScenario> load(ScenarioConfig config)
        {
            var toReturn = new List<IScenario>();

            var universal = config.UniversalTriggers.Try().ToList();
            var scenarios = config.Scenarios.Try().ToList();

            //add universal triggers to each and every scenario.  also cast to proper type
            scenarios.ForEach(s =>
            {
                s.Triggers.AddRange(universal);
                toReturn.Add((IScenario)s);
            });

            return toReturn;
        }
    }
}
