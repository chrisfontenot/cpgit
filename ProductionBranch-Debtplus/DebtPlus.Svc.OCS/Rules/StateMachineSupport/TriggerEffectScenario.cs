﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DebtPlus.OCS.Domain;

namespace DebtPlus.Svc.OCS.Rules
{
    public class serializablescenario
    {
        public String StatusCode { get; set; }
        public List<serializabletrigger> Triggers { get; set; }
    }
    public class serializabletrigger
    {
        public String Trigger { get; set; }
        public bool? LastChanceRules { get; set; }
        public String SetActive { get; set; }
        public String SetQueue { get; set; }
        public String IncrementAttempts { get; set; }
        public String SetStatus { get; set; }
        public String CalcStatus { get; set; }
    }

    public enum INCREMENT_RULE
    {
        DoNothing,
        IncrementBy1,
        ResetTo1
    }

    public enum STATUS_FUNC
    {
        PnOfT,
        CnOfT
    }

    public enum QUEUE_RULE {
        DoNothing,
        SetHighTouch,
        SetCounselor
    }

    public enum ACTIVE_RULE { 
        DoNothing,
        SetActive,
        SetInactive
    }

    public class TriggerEffect
    {
        /// <summary>
        /// The trigger which results in the given effects.
        /// </summary>
        public TRIGGER Trigger { get; set; }
        
        /// <summary>
        /// true means "run the 2 last chance rules to update values"
        /// false means "do nothing"
        /// </summary>
        public bool? LastChanceRules { get; set; }

        public ACTIVE_RULE? SetActive { get; set; }
        public QUEUE_RULE? SetQueue { get; set; }
        public INCREMENT_RULE? IncrementAttempts { get; set; }
        public STATUS_CODE? SetStatus { get; set; }
        public STATUS_FUNC? CalcStatus { get; set; }
        public bool HasRpcCondition { get; set; }
    }

    /// <summary>
    /// Can be thrown if a scenario doesn't contain a given trigger;
    /// </summary>
    public class InvalidTrigger : Exception {

        private static string getMessage(IScenario scenario, TRIGGER trigger) {
            return String.Format(@"Scenario for status code {0} does not have {1} as a valid trigger.",
                scenario.StatusCode.ToString(),
                trigger.ToString());
        }

        public InvalidTrigger(IScenario scenario, TRIGGER trigger) : 
            base(InvalidTrigger.getMessage(scenario,trigger)){ }
    }

    public interface IScenario {
        TriggerEffect GetTriggerEffect(TRIGGER trigger);
        STATUS_CODE StatusCode { get; set; }
        bool WithRpc { get; set; }
    }

    public class GeneralScenario : IScenario {

        public STATUS_CODE StatusCode { get; set; }

        public bool WithRpc { get; set; }

        public List<TriggerEffect> Triggers { get; set; }

        public TriggerEffect GetTriggerEffect(TRIGGER trigger)
        {
            TriggerEffect toReturn = Triggers.FirstOrDefault(t => t.Trigger == trigger);

            if (toReturn == null) {
                throw new InvalidTrigger(this, trigger);
            }

            return toReturn;
        }
    }
}
