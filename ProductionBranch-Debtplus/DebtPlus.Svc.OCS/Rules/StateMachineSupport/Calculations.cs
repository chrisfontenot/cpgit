﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DebtPlus.OCS.Domain;

namespace DebtPlus.Svc.OCS.Rules
{
    public class Calculations
    {
        private static int MonthsDiff(DateTime date1, DateTime date2)
        {
            return ((date1.Year - date2.Year) * 12) + date1.Month - date2.Month;
        }

        /// <summary>
        /// Calculates the correct "PaymentReminder 'n' " code
        /// </summary>
        /// <remarks>
        /// This calculation is based off of how many months it has been since the last right-party contact.
        /// It assumes that all the PaymentReminder status codes are next to each other in the enumeration,
        /// starting with PaymentReminder1 and progressing sequentially to P12               
        /// </remarks>
        public static Func<OCSClient, STATUS_CODE> CalculatePnOfT = (client) =>
        {
            int maxReminders = 0;
            STATUS_CODE lastReminder = STATUS_CODE.PaymentReminder12; 

            if (client.Program == PROGRAM.NationStarSD1308 || client.Program == PROGRAM.OneWestSD1308)
            {
                /* NationStar and OneWest have 1 to 6 Payment Reminders */
                maxReminders = 6;
                lastReminder = STATUS_CODE.PaymentReminder6;
            }
            else
            {
                /* others have 12 reminders */
                maxReminders = 12;
                lastReminder = STATUS_CODE.PaymentReminder12;
            }

            if (!client.LastRightPartyContact.HasValue)
            {
                return STATUS_CODE.PaymentReminder1;
            }
            else
            {
                var diff = MonthsDiff(DateTime.Now, client.LastRightPartyContact.Value);

                if (diff == 0)
                {
                    return STATUS_CODE.PaymentReminder1;
                }
                //max is P12
                else if (diff >= maxReminders)
                {
                    return lastReminder;
                    
                }
                //right now the value of the enumeration starts at 21
                else
                {
                    var offset = ((int)STATUS_CODE.PaymentReminder1);

                    return (STATUS_CODE)(offset + diff);
                }
            }
        };

        public static Func<OCSClient, STATUS_CODE> CalculateCnOfT = (client) =>
        {
            STATUS_CODE newStatus;
            switch (client.StatusCode)
            {
                case STATUS_CODE.InitialContactAttempt1:
                    newStatus = STATUS_CODE.InitialContactAttempt2;
                    break;
                case STATUS_CODE.InitialContactAttempt2:
                    newStatus = STATUS_CODE.InitialContactAttempt3;
                    break;
                case STATUS_CODE.InitialContactAttempt3:
                    if (client.Program == PROGRAM.NationStarSD1308)
                    {
                        newStatus = STATUS_CODE.InitialContactAttempt4;
                    }
                    else
                    {
                        newStatus = STATUS_CODE.InitialContactAttemptExtras;
                    }
                    break;
                case STATUS_CODE.InitialContactAttempt4:
                    newStatus = STATUS_CODE.InitialContactAttemptExtras;
                    break;
                case STATUS_CODE.InitialContactAttemptExtras:
                    newStatus = STATUS_CODE.InitialContactAttemptExtras;
                    break;
                default:
                    newStatus = client.StatusCode;
                    break;
            }
            return newStatus;
        };

        public static Action<OCSClient> CalculatePostModLastChance = (client) =>
        {

            if (!client.PostModLastChanceDate.HasValue)
            {
                client.PostModLastChanceDate = DateTime.Now;
            }

            // Jan 2012 and started with zero. 
            if (!client.PostModLastChanceFlag.HasValue)
            {
                client.PostModLastChanceFlag = MonthsDiff(DateTime.Now, new DateTime(2012, 1, 1));
            }
        };
    }
}
