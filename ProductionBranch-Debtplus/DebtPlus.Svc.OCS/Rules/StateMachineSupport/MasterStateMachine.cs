﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DebtPlus.OCS.Domain;

namespace DebtPlus.OCS.Domain
{
    public class MasterStateMachine
    {
        public static OCSClient UpdateOnTrigger(PROGRAM program, TRIGGER passedTrigger, STATUS_CODE priorStatusCode, OCSClient client)
        {
            if (client.CurrentAttempt != null)
            {
                if (!client.CurrentAttempt.IsClosed)
                {
                    client.CurrentAttempt.EndAttempt();
                }
            }

            if (program == PROGRAM.FreddieMac180 || program == PROGRAM.FreddieMacEI) 
			{
                return DebtPlus.Svc.OCS.Rules.FMACEI.StateMachine.UpdateOnTrigger(passedTrigger, priorStatusCode, client);
            } 
            else if (program == PROGRAM.FreddieMacPostMod) 
            {
                return DebtPlus.Svc.OCS.Rules.FMACPostMod.StateMachine.UpdateOnTrigger(passedTrigger, priorStatusCode, client);
            } 
            else if (program == PROGRAM.FannieMaePostMod) 
            {
                return DebtPlus.Svc.OCS.Rules.FMAEPostMod.StateMachine.UpdateOnTrigger(passedTrigger, priorStatusCode, client);
            }
            else if (program == PROGRAM.NationStarSD1308)
            {
                return DebtPlus.Svc.OCS.Rules.NationStar.StateMachine.UpdateOnTrigger(passedTrigger, priorStatusCode, client);
            }
            else if (program == PROGRAM.USBankSD1308)
            {
                return DebtPlus.Svc.OCS.Rules.USBank.StateMachine.UpdateOnTrigger(passedTrigger, priorStatusCode, client);
            }
            else if (program == PROGRAM.OneWestSD1308)
            {
                return DebtPlus.Svc.OCS.Rules.OneWest.StateMachine.UpdateOnTrigger(passedTrigger, priorStatusCode, client);
            }
            else if (program == PROGRAM.PostModPilotCheckin)
            {
                return DebtPlus.Svc.OCS.Rules.PostModPilotCheckin.StateMachine.UpdateOnTrigger(passedTrigger, priorStatusCode, client);
            }
            else if (program == PROGRAM.PostModPilotCounseling)
            {
                return DebtPlus.Svc.OCS.Rules.PostModPilotCounseling.StateMachine.UpdateOnTrigger(passedTrigger, priorStatusCode, client);
            }
            else
            {
                return DebtPlus.Svc.OCS.Rules.FMACEI.StateMachine.UpdateOnTrigger(passedTrigger, priorStatusCode, client);
            }

        }
    }
}
