﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DebtPlus.OCS.Domain;
using DebtPlus.LINQ;

namespace DebtPlus.Svc.OCS.Rules.StateMachineSupport
{
    public static class RpcHelper
    {
        public static bool HasRpcResultCode(PROGRAM program, ContactAttempt attempt)
        {
            bool isRpc = false;
            if (attempt != null && attempt.Outcomes != null && attempt.Outcomes.Count > 0)
            {
                List<RESULT_CODE> rpcResultCodes = null;
                using (var bc = new BusinessContext())
                {
                    // The mapping for the Program and the Result codes for RPC are part of this table
                    rpcResultCodes = bc.OCS_Program_RPCResultCodes
                                           .Where(p => p.ProgramID == (int)program)
                                           .Select(p => (RESULT_CODE)p.ResultCode).ToList();
                }

                if (rpcResultCodes != null && rpcResultCodes.Count > 0)
                {
                    var outcomes = attempt.Outcomes;
                    foreach (var o in outcomes)
                    {
                        foreach (var rpcCode in rpcResultCodes)
                        {
                            if (o.Result == rpcCode)
                            {
                                isRpc = true;
                                break;
                            }
                        }
                        if (isRpc == true)
                        {
                            break;
                        }
                    }
                }

            }
            return isRpc;
        }

        internal static bool HasPreviousRpcResultCode(PROGRAM program, List<RESULT_CODE> previousResultCodes)
        {
            List<RESULT_CODE> rpcResultCodes = null;
            bool isRpc = false;
            if (previousResultCodes != null && previousResultCodes.Count > 0)
            {
                using (var bc = new BusinessContext())
                {
                    // The mapping for the Program and the Result codes for RPC are part of this table
                    rpcResultCodes = bc.OCS_Program_RPCResultCodes
                                           .Where(p => p.ProgramID == (int)program)
                                           .Select(p => (RESULT_CODE)p.ResultCode).ToList();
                }

                foreach (var rc in previousResultCodes)
                {
                    foreach (var rpcCode in rpcResultCodes)
                    {
                        if (rc == rpcCode)
                        {
                            isRpc = true;
                            break;
                        }
                    }
                    if (isRpc == true)
                    {
                        break;
                    }
                }
            }

            return isRpc;
        }
    }
}
