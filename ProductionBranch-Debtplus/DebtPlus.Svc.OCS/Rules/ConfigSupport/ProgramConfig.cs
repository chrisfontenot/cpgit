﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DebtPlus.OCS.Domain;

namespace DebtPlus.Svc.OCS.Rules
{
    public class ProgramConfig
    {
        public PROGRAM Program { get; set; }
        public String ProgramDescription { get; set; }
        public List<RESULT_CODE> RightPartyContacts { get; set; }
        public int? ReferralSource { get; set; }
        public Dictionary<string, STATUS_CODE> StatusCodes { get; set; }
        public Dictionary<string, RESULT_CODE> ResultCodes { get; set; }
        public Dictionary<string, CONTACT_TYPE> ContactTypes { get; set; }
        public Dictionary<string, MTI_RESULT> MTIResults { get; set; }
        public Dictionary<string, int> Servicers { get; set; }
        public String ExcelPrimaryKeyColumn { get; set; }
    }
}
