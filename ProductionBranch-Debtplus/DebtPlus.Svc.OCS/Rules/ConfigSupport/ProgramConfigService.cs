﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DebtPlus.OCS.Domain;
using DebtPlus.Svc.OCS.Rules;
using DebtPlus.Svc.OCS.Rules.FMACPostMod;

namespace DebtPlus.OCS.Domain
{
    public static class ProgramConfigService
    {
        private static ProgramConfig defaultConfig = DefaultProgramConfig.getConfig();

        private static Dictionary<PROGRAM, ProgramConfig> configs = new Dictionary<PROGRAM, ProgramConfig> { 
            { PROGRAM.FreddieMacPostMod, PostModConfig.getConfig() },
            { PROGRAM.FannieMaePostMod, DebtPlus.Svc.OCS.Rules.FMAEPostMod.FMAEPostModConfig.getConfig() },
            { PROGRAM.FreddieMacEI, DebtPlus.Svc.OCS.Rules.FMACEI.FMACEIProgramConfig.getConfig() },
            { PROGRAM.FreddieMac180, DebtPlus.Svc.OCS.Rules.FMAC180.FMAC180ProgramConfig.getConfig() },
            { PROGRAM.FreddieMac720, DebtPlus.Svc.OCS.Rules.FMAC720.FMAC720ProgramConfig.getConfig() },
            { PROGRAM.FreddieMacHamp, DebtPlus.Svc.OCS.Rules.FMACHAMP.FMACHAMPProgramConfig.getConfig() },
            { PROGRAM.HECMDefault, DebtPlus.Svc.OCS.Rules.HECMDefault.HECMDefaultConfig.getConfig() },
            { PROGRAM.NationStarHECM, DebtPlus.Svc.OCS.Rules.HECMDefault.HECMDefaultConfig.getConfig() },
            { PROGRAM.OneWestHECM, DebtPlus.Svc.OCS.Rules.HECMDefault.HECMDefaultConfig.getConfig() },
            { PROGRAM.RMSHECM, DebtPlus.Svc.OCS.Rules.HECMDefault.HECMDefaultConfig.getConfig() },
            { PROGRAM.WELLSHECM, DebtPlus.Svc.OCS.Rules.HECMDefault.HECMDefaultConfig.getConfig() },
            { PROGRAM.NationStarSD1308, DebtPlus.Svc.OCS.Rules.NationStar.NationStarConfig.getConfig() },
            { PROGRAM.OneWestSD1308, DebtPlus.Svc.OCS.Rules.OneWest.OneWestConfig.getConfig() },
            { PROGRAM.USBankSD1308, DebtPlus.Svc.OCS.Rules.USBank.USBankConfig.getConfig() },
            { PROGRAM.PostModPilotCheckin, DebtPlus.Svc.OCS.Rules.FMAEPostMod.PostModPilotCheckinConfig.getConfig() },
            { PROGRAM.PostModPilotCounseling, DebtPlus.Svc.OCS.Rules.FMAEPostMod.PostModPilotCounselingConfig.getConfig() }};

        private static T getValue<T>(PROGRAM program, Func<ProgramConfig, T> getter) {
            if (configs.ContainsKey(program))
            {
                return getter(configs[program]);
            }
            else {
                return getter(defaultConfig);
            }
        }

        public static int getReferralSource(PROGRAM program) {
            return getValue(program, p => p.ReferralSource.Value);
        }

        public static List<RESULT_CODE> getRightPartyContacts(PROGRAM program){
            return getValue(program, p => p.RightPartyContacts);
        }

        public static String getDescription(PROGRAM program) {
            return getValue(program, p => p.ProgramDescription);
        }

        public static Dictionary<string, STATUS_CODE> getStatusCodes(PROGRAM program) {
            return getValue(program, p => p.StatusCodes);
        }

        public static Dictionary<string, RESULT_CODE> getResultCodes(PROGRAM program) {
            return getValue(program, p => p.ResultCodes);
        }

        public static Dictionary<string, CONTACT_TYPE> getContactTypes(PROGRAM program) {
            return getValue(program, p => p.ContactTypes);
        }

        public static Dictionary<string, MTI_RESULT> getMTIResults(PROGRAM program) {
            return getValue(program, p => p.MTIResults);
        }

        public static String getKeyColumn(PROGRAM program)
        {
            return getValue(program, p => p.ExcelPrimaryKeyColumn);
        }

        public static Dictionary<string, int> getServicers(PROGRAM program)
        {
            return getValue(program, p => p.Servicers);
        }        
    }
}