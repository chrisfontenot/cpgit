﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DebtPlus.OCS.Domain;
 
namespace DebtPlus.Svc.OCS.Rules
{
    public class DefaultProgramConfig
    {
        public static ProgramConfig getConfig() {
            return new ProgramConfig
            {
                ProgramDescription = "???",
                RightPartyContacts = new List<RESULT_CODE>(),
                ReferralSource = 415,
                StatusCodes = new Dictionary<string, STATUS_CODE> { 
                    {"New Record",STATUS_CODE.NewRecord},
                    {"BN - Bad Number",STATUS_CODE.BadNumber},
                    {"CC - CSR Will Call Back",STATUS_CODE.CSRWillCallBack},
                    {"CB1 - Counselor Will Call Back",STATUS_CODE.CounselorWillCallback1},
                    {"CB2 - Counselor Will Call Back",STATUS_CODE.CounselorWillCallback2},
                    {"CB3 - Counselor Will Call Back",STATUS_CODE.CounselorWillCallback3},
                    {"CO - Counseled",STATUS_CODE.Counseled},
                    {"CT - Contact Made",STATUS_CODE.ContactMade},
                    {"NC - No Contact",STATUS_CODE.NoContact},
                    {"NS - No Show",STATUS_CODE.NoShow},
                    {"OP - Opt Out",STATUS_CODE.OptOut},
                    {"PE - Pending Appointment",STATUS_CODE.PendingAppointment},
                    {"TR - Transfer To Counselor", STATUS_CODE.TransferToCounselor},
                    {"EX - Exclusion Report",STATUS_CODE.ExclusionReport},
                    {"GQ - General Questions",STATUS_CODE.GeneralQuestions},
                    {"IM - Immediate Counseling",STATUS_CODE.ImmediateCounseling},
                    {"C1 - Initial Contact Attempt 1",STATUS_CODE.InitialContactAttempt1},
                    {"C2 - Initial Contact Attempt 2",STATUS_CODE.InitialContactAttempt2},
                    {"C3 - Initial Contact Attempt 3",STATUS_CODE.InitialContactAttempt3},
                    {"OT - Extra Contact Attempts",STATUS_CODE.InitialContactAttemptExtras},
                    {"P1 - Payment Reminder 1",STATUS_CODE.PaymentReminder1},
                    {"P2 - Payment Reminder 2",STATUS_CODE.PaymentReminder2},
                    {"P3 - Payment Reminder 3",STATUS_CODE.PaymentReminder3},
                    {"P4 - Payment Reminder 4",STATUS_CODE.PaymentReminder4},
                    {"P5 - Payment Reminder 5",STATUS_CODE.PaymentReminder5},
                    {"P6 - Payment Reminder 6",STATUS_CODE.PaymentReminder6},
                    {"P7 - Payment Reminder 7",STATUS_CODE.PaymentReminder7},
                    {"P8 - Payment Reminder 8",STATUS_CODE.PaymentReminder8},
                    {"P9 - Payment Reminder 9",STATUS_CODE.PaymentReminder9},
                    {"P10 - Payment Reminder 10",STATUS_CODE.PaymentReminder10},
                    {"P11 - Payment Reminder 11",STATUS_CODE.PaymentReminder11},
                    {"P12 - Payment Reminder 12",STATUS_CODE.PaymentReminder12},
                    {"SCO - Second Counseling", STATUS_CODE.SecondCounseling}
                },
                ResultCodes = new Dictionary<string, RESULT_CODE>(),
                ContactTypes = new Dictionary<string, CONTACT_TYPE> { 
                    {"CSR IL - Incoming/ReceivedLetter",CONTACT_TYPE.CSR_Incoming_ReceivedLetter},
                    {"CSR IR - Incoming/Returning Call",CONTACT_TYPE.CSR_Incoming_ReturningCall},
                    {"CSR NT - Internet",CONTACT_TYPE.CSR_Internet},
                    {"CSR OB - Outbound Call",CONTACT_TYPE.CSR_OutboundCall},
                    {"CSR RL - Referred by Lender",CONTACT_TYPE.CSR_ReferredByLender},
                    {"CO IL - Incoming/ReceivedLetter",CONTACT_TYPE.CO_Incoming_ReceivedLetter},
                    {"CO IR - Incoming/Returning Call",CONTACT_TYPE.CO_Incoming_ReturningCall},
                    {"CO NT - Internet",CONTACT_TYPE.CO_Internet},
                    {"CO OB - Outbound Call",CONTACT_TYPE.CO_OutboundCall},
                    {"CO RL - Referred by Lender",CONTACT_TYPE.CO_ReferredByLender}

                },
                MTIResults = new Dictionary<string, MTI_RESULT> { 
                    {"BZ –  MTI Busy",MTI_RESULT.Busy},
                    {"M1 – MTI Attempt",MTI_RESULT.Attempt},
                    {"VM – MTI Voice Mail",MTI_RESULT.VoiceMail},
                    {"VN – MTI Valid Number",MTI_RESULT.ValidNumber}
                },
                ExcelPrimaryKeyColumn = String.Empty,
                Servicers = new Dictionary<string, int> { }
            };
        }
    }
}
