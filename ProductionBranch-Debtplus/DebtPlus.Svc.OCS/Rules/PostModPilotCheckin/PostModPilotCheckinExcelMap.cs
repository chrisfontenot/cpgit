﻿using System;
using System.Collections.Generic;
using System.Data.SqlTypes;
using System.Text.RegularExpressions;
using DebtPlus.LINQ;
using DebtPlus.LINQ.BusinessLayer;
using DebtPlus.OCS.Domain;
using DebtPlus.Svc.OCS;
using DebtPlus.Svc.OCS.Rules;

namespace DebtPlus.UI.Housing.OCS
{
    public class PostModPilotCheckinExcelMap
    {
        private static Regex sanitizePhone = new Regex(@"\D");

        private static Action<object, OCSClient, LINQ.OCS_UploadRecord> getMapPhoneNumber(string columnName)
        {
            return (val, client, upload) =>
            {
                if (String.IsNullOrWhiteSpace(val.ToString()))
                {
                    return;
                }

                if (!CommonValidation.IsPhone(val.ToString()))
                {
                    return;
                }

                if (client.PhoneNumbers == null)
                {
                    client.PhoneNumbers = new List<TelephoneNumber>();
                }

                String result = sanitizePhone.Replace(val.ToString(), "");

                if (result.Length == 11)
                {
                    result = result.Substring(1);
                }

                var newNumber = new TelephoneNumber
                {
                    SetNumber = result
                };

                if (client.PhoneNumbers.Exists(t => t.SearchValue == newNumber.SearchValue))
                {
                    return;
                }

                switch (columnName)
                {
                    case "borrower phone - home":
                        upload.Phone1 = result;
                        client.PhoneNumbers.AssignNumber(newNumber, PHONE_TYPE.ClientHome);
                        client.PhoneNumbers.Add(newNumber);
                        break;
                    case "borrower phone other":
                        upload.Phone2 = result;
                        client.PhoneNumbers.AssignNumber(newNumber, PHONE_TYPE.ClientMsg);
                        client.PhoneNumbers.Add(newNumber);
                        break;
                    case "borrower cell phone 1":
                        upload.Phone3 = result;
                        client.PhoneNumbers.AssignNumber(newNumber, PHONE_TYPE.ApplicantCell);
                        client.PhoneNumbers.Add(newNumber);
                        break;
                    case "borrower cell phone 2":
                        upload.Phone7 = result;
                        client.PhoneNumbers.AssignNumber(newNumber, PHONE_TYPE.ApplicantCell2);
                        client.PhoneNumbers.Add(newNumber);
                        break;
                    case "borrower phone - office 1":
                        upload.Phone4 = result;
                        client.PhoneNumbers.AssignNumber(newNumber, PHONE_TYPE.ApplicantWork);
                        client.PhoneNumbers.Add(newNumber);
                        break;
                    case "borrower phone office 2":
                        upload.Phone8 = result;
                        client.PhoneNumbers.AssignNumber(newNumber, PHONE_TYPE.ApplicantWork2);
                        client.PhoneNumbers.Add(newNumber);
                        break;
                    case "co borrower phone home":
                        upload.Phone9 = result;
                        client.PhoneNumbers.AssignNumber(newNumber, PHONE_TYPE.CoApplicantHome);
                        client.PhoneNumbers.Add(newNumber);
                        break;
                    case "co borrower phone other":
                        upload.Phone10 = result;
                        client.PhoneNumbers.AssignNumber(newNumber, PHONE_TYPE.CoApplicantMsg);
                        client.PhoneNumbers.Add(newNumber);
                        break;
                    case "co borrower cell phone 1":
                        upload.Phone5 = result;
                        client.PhoneNumbers.AssignNumber(newNumber, PHONE_TYPE.CoApplicantCell);
                        client.PhoneNumbers.Add(newNumber);
                        break;
                    case "co borrower cell phone 2":
                        upload.Phone11 = result;
                        client.PhoneNumbers.AssignNumber(newNumber, PHONE_TYPE.CoApplicantCell2);
                        client.PhoneNumbers.Add(newNumber);
                        break;
                    case "co borrower phone office 1":
                        upload.Phone6 = result;
                        client.PhoneNumbers.AssignNumber(newNumber, PHONE_TYPE.CoApplicantWork);
                        client.PhoneNumbers.Add(newNumber);
                        break;
                    case "co borrower phone office 2":
                        upload.Phone12 = result;
                        client.PhoneNumbers.AssignNumber(newNumber, PHONE_TYPE.CoApplicantWork2);
                        client.PhoneNumbers.Add(newNumber);
                        break;
                }
            };
        }

        private static Func<string, bool> dateValidator = (val) =>
        {
            if (!String.IsNullOrEmpty(val.ToString()))
            {
                DateTime _out;
                return DateTime.TryParse(val.ToString(), out _out);
            }

            return true;
        };

        public static Dictionary<string, Func<string, bool>> fieldValidationFuncs = new Dictionary<string, Func<string, bool>> 
        { 
       		{ "fannie_mae_loan_number", (val) => { return string.IsNullOrWhiteSpace(val.ToString()) == false; } },
            { "referral date",  dateValidator },
            { "servicer name", (val) => { return true; } },
            { "agency name", (val) => { return true; } },
            { "backlog mod flag", (val) => { return true; } },
            { "trial/mod y/n", (val) => { return true; } },
            { "trial mod type", (val) => { return true; } },
            { "trial start date", dateValidator },
            { "mod conversion date", dateValidator },
            { "servicer loan number", (val) => { return true; } },
            { "ach flag", (val) => { return true; } },
            { "trial mod payment amount", (val) => { return true; } },
            { "next payment due date", dateValidator },
            { "last payment applied date", dateValidator },
            { "upb", (val) => { return true; } },
            { "reason for default", (val) => { return true; } },
            { "spanish_speaking_flag_(y/n)", (val) => { return true; } },
            { "borrower first name", (val) => { return string.IsNullOrWhiteSpace(val.ToString()) == false; } },
            { "borrower middle name", (val) => { return true; } },
            { "borrower last name", (val) => { return true; } },
            { "co borrower first name", (val) => { return true; } },
            { "co-borrower middle name", (val) => { return true; } },
            { "co borrower last name", (val) => { return true; } },
            { "property address 1", (val) => { return string.IsNullOrWhiteSpace(val.ToString()) == false; } },
            { "property address 2", (val) => { return true; } },
            { "property city", (val) => { return true; } },
            { "property state", (val) => { return true; } },
            { "property zip", (val) => { return true; } },
            { "mailing address 1", (val) => { return true; } },
            { "mailing address 2", (val) => { return true; } },
            { "mailing address 3", (val) => { return true; } },
            { "mailing city", (val) => { return true; } },
            { "mailing state", (val) => { return true; } },
            { "mailing zip", (val) => { return true; } },
            { "borrower phone - home", (val) => { return val.ToString().Trim() == string.Empty || CommonValidation.IsPhone(val.ToString()); } },
            { "borrower phone - office 1", (val) => { return val.ToString().Trim() == string.Empty || CommonValidation.IsPhone(val.ToString()); } },
            { "borrower phone office 2", (val) => { return val.ToString().Trim() == string.Empty || CommonValidation.IsPhone(val.ToString()); } },
            { "borrower phone other", (val) => { return val.ToString().Trim() == string.Empty || CommonValidation.IsPhone(val.ToString()); } },
            { "borrower cell phone 1", (val) => { return val.ToString().Trim() == string.Empty || CommonValidation.IsPhone(val.ToString()); } },
            { "borrower cell phone 2", (val) => { return val.ToString().Trim() == string.Empty || CommonValidation.IsPhone(val.ToString()); } },
            { "borrower email", (val) => { return true; } },
            { "co borrower phone home", (val) => { return val.ToString().Trim() == string.Empty || CommonValidation.IsPhone(val.ToString()); } },
            { "co borrower phone office 1", (val) => { return val.ToString().Trim() == string.Empty || CommonValidation.IsPhone(val.ToString()); } },
            { "co borrower phone office 2", (val) => { return val.ToString().Trim() == string.Empty || CommonValidation.IsPhone(val.ToString()); } },
            { "co borrower phone other", (val) => { return val.ToString().Trim() == string.Empty || CommonValidation.IsPhone(val.ToString()); } },
            { "co borrower cell phone 1", (val) => { return val.ToString().Trim() == string.Empty || CommonValidation.IsPhone(val.ToString()); } },
            { "co borrower cell phone 2", (val) => { return val.ToString().Trim() == string.Empty || CommonValidation.IsPhone(val.ToString()); } },
            { "co borrower email", (val) => { return true; } },
            { "trial mod description", (val) => { return true; } },
            { "comment description", (val) => { return true; } },
            { "trialpaymentsreceivedamount", (val) => { return true; } },
            { "trialpaymentsreceivedcount", (val) => { return true; } },
            { "last payment applied amount", (val) => { return true; } },
            { "workout type", (val) => { return true; } },
            { "borrower ssn", (val) => { return true; } }
        };

        public static Dictionary<string, Action<string, OCSClient, LINQ.OCS_UploadRecord>> fieldConvertActions = new Dictionary<string, Action<string, OCSClient, LINQ.OCS_UploadRecord>> 
        { 
            //special map function that gets called after all field-level mappings
            {"GLOBAL RECORD MAPPER", (val, client, upload) => {
                //client.PreferredLanguage = Person.LANGUAGE.English;
                client.ContactAttemptCount = 0;
                client.ActiveFlag = client.PhoneNumbers != null && client.PhoneNumbers.Count > 0;
            } },
            { "fannie_mae_loan_number", (val, client, upload) => {
                upload.InvestorNumber = val.ToString();
                client.InvestorLoanNo = val.ToString(); 
            } },
            { "servicer name",  (val, client, upload) => {
                upload.Servicer = val.ToString();
                client.Servicer = val.ToString(); 
            } },
            { "servicer loan number", (val, client, upload) => {
                upload.LoanNumber = val.ToString();
                client.LoanNo = val.ToString(); 
            } },
            { "borrower last name", (val, client, upload) => {
                upload.LastName1 = val.ToString();
                if (client.Person1 == null) { client.Person1 = new Person(); }
                client.Person1.LastName = val.ToString(); 
            } },
            { "borrower middle name", (val, client, upload) => {
                if (client.Person1 == null) { client.Person1 = new Person(); }
                    client.Person1.MiddleName = val.ToString(); 
            } },
            { "borrower first name", (val, client, upload) => {
                upload.FirstName1 = val.ToString();
                if (client.Person1 == null) { client.Person1 = new Person(); }
                    client.Person1.FirstName = val.ToString(); 
            } },
            { "borrower ssn", (val, client, upload) => {
                upload.SSN1 = val.ToString();
                if (client.Person1 == null) { client.Person1 = new Person(); }
                var str = val.ToString();
                if (!String.IsNullOrWhiteSpace(str))
                    client.Person1.SSN = str;
            } },
            { "borrower email", (val, client, upload) => {
                if(client.Person1 != null) {
                    client.Person1.EmailAddress = val.ToString();
                }
            } },
            { "co borrower last name", (val, client, upload) => {
                upload.LastName2 = val.ToString();

                if (client.Person2 == null && !String.IsNullOrWhiteSpace(val.ToString())) { client.Person2 = new Person(); }

                if(client.Person2 != null) {
                    client.Person2.LastName = val.ToString(); 
                }                
            } },
            { "co-borrower middle name", (val, client, upload) => {
                if (client.Person2 == null) { client.Person2 = new Person(); }
                client.Person2.MiddleName = val.ToString(); 
            } },
            { "co borrower first name", (val, client, upload) => {
                upload.FirstName2 = val.ToString();

                if (client.Person2 == null && !String.IsNullOrWhiteSpace(val.ToString())) { client.Person2 = new Person(); }

                if(client.Person2 != null){
                    client.Person2.FirstName = val.ToString(); 
                }                
            } },
            { "co borrower email", (val, client, upload) => {
                 if(client.Person2 != null) {
                    client.Person2.EmailAddress = val.ToString();
                 }
            } },
            { "property address 1", (val, client, upload) => {
                upload.ClientStreet = val.ToString();
                if (client.PropertyAddress == null) { client.PropertyAddress = new Address(); }
                    client.PropertyAddress.street = val.ToString();
            } },
            { "property address 2", (val, client, upload) => {
                 if (client.PropertyAddress == null) { client.PropertyAddress = new Address(); }
                    client.PropertyAddress.address_line_2 = val.ToString();
            } },
            { "property city", (val, client, upload) => {
                upload.ClientCity = val.ToString();
                if (client.PropertyAddress == null) { client.PropertyAddress = new Address(); }
                    client.PropertyAddress.city = val.ToString();
            } },
            { "property state", (val, client, upload) => {
                upload.ClientState = val.ToString();
                if (client.PropertyAddress == null) { client.PropertyAddress = new Address(); }
                if(!String.IsNullOrWhiteSpace(val.ToString())){
                    client.PropertyAddress.StateProvince = (US_STATE)Enum.Parse(typeof(US_STATE), val.ToString().ToUpper(), true);
                }                
            } },
            { "property zip", (val, client, upload) => {
                upload.ClientZipcode = val.ToString();
                if (client.PropertyAddress == null) { client.PropertyAddress = new Address(); }
                    client.PropertyAddress.PostalCode = val.ToString();
            } },
            { "mailing address 1", (val, client, upload) => {
                 if (client.Address == null) { client.Address = new Address(); }
                    client.Address.street = val.ToString();
            } },
            { "mailing address 2", (val, client, upload) => {
                 if (client.Address == null) { client.Address = new Address(); }
                    client.Address.address_line_2 = val.ToString();
            } } ,           
            { "mailing address 3", (val, client, upload) => {
                 if (client.Address == null) { client.Address = new Address(); }
                 client.Address.address_line_3 = val.ToString();
            } },
            { "mailing city", (val, client, upload) => {
                 if (client.Address == null) { client.Address = new Address(); }
                    client.Address.city = val.ToString();
            } },
            { "mailing state", (val, client, upload) => {
                if (client.Address == null) { client.Address = new Address(); }
                if(!String.IsNullOrWhiteSpace(val.ToString())){
                    client.Address.StateProvince = (US_STATE)Enum.Parse(typeof(US_STATE), val.ToString().ToUpper(), true);
                }               
            } },
            { "mailing zip", (val, client, upload) => {
                 if (client.Address == null) { client.Address = new Address(); }
                    client.Address.PostalCode = val.ToString();
            } },
            { "borrower phone - home", getMapPhoneNumber("borrower phone - home") },
            { "borrower phone - office 1", getMapPhoneNumber("borrower phone - office 1") },
            { "borrower phone office 2", getMapPhoneNumber("Borrower Phone - Office 2") },
            { "borrower phone other", getMapPhoneNumber("borrower phone other") },
            { "borrower cell phone 1", getMapPhoneNumber("borrower cell phone 1") },
            { "borrower cell phone 2", getMapPhoneNumber("borrower cell phone 2") },

            { "co borrower phone home", getMapPhoneNumber("co borrower phone home") },
            { "co borrower phone office 1", getMapPhoneNumber("co borrower phone office 1") },
            { "co borrower phone office 2", getMapPhoneNumber("co borrower phone office 2") },
            { "co borrower phone other", getMapPhoneNumber("co borrower phone other") },
            { "co borrower cell phone 1", getMapPhoneNumber("co borrower cell phone 1") },
            { "co borrower cell phone 2", getMapPhoneNumber("co borrower cell phone 2") },

            { "trial mod description", (val, client, upload) => {
                client.Trial_Mod_Description = val.ToString();
            } },
            // TRIALPERIODSTARTDATE
            { "trial start date", (val, client, upload) => {
                DateTime _out;

                if (!String.IsNullOrEmpty(val.ToString()) && DateTime.TryParse(val.ToString(),out _out))
                {
                    upload.TrialPeriodStartDate = _out;
                }
                else
                {
                    upload.TrialPeriodStartDate = null;
                }
            } },
           
            { "referral date", (val, client, upload) => {
                DateTime _out;
                if (!String.IsNullOrEmpty(val.ToString()) && DateTime.TryParse(val.ToString(), out _out))
                {
                    upload.Referral_Date = _out;
                }
                else
                {
                    upload.Referral_Date = null;
                }
            } },
            { "agency name", (val, client, upload) => {
                client.Agency_Name = val.ToString(); 
            } },
            { "backlog mod flag", (val, client, upload) => {
                client.Backlog_Mod_Flag = val.ToString(); 
            } },
            { "trial/mod y/n", (val, client, upload) => {
                client.Trial_Mod = val.ToString(); 
            } },
            { "trial mod type", (val, client, upload) => {
                 upload.TrialMod_Type = val.ToString();
            } },
            { "mod conversion date", (val, client, upload) => {
                client.Mod_Conversion_Date = val.ToString(); 
            } },
            { "trial mod payment amount", (val, client, upload) => {
                client.Trial_Mod_Payment_Amount = val.ToString(); 
            } },
            { "trialpaymentsreceivedamount", (val, client, upload) => {
                client.TrialPaymentsReceivedAmount = val.ToString(); 
            } },
            { "trialpaymentsreceivedcount", (val, client, upload) => {
                client.TrialPaymentsReceivedCount = val.ToString(); 
            } },
            { "upb", (val, client, upload) => {
                client.UPB = val.ToString(); 
            } },
            { "workout type", (val, client, upload) => {
                client.Workout_Type = val.ToString(); 
            } },
             { "ach flag", (val, client, upload) => {
                client.ACH_Flag = val.ToString(); 
            } },
            { "reason for default", (val, client, upload) => {
                client.Reason_For_Default = val.ToString(); 
            } },
            { "next payment due date", (val, client, upload) => {
                DateTime _out;
                if (!String.IsNullOrEmpty(val.ToString()) && DateTime.TryParse(val.ToString(), out _out))
                {
                    upload.Next_Payment_Due_Date = _out;
                }
                else
                {
                    upload.Next_Payment_Due_Date = null;
                }
            } },
            { "last payment applied date", (val, client, upload) => {
                DateTime _out;
                if (!String.IsNullOrEmpty(val.ToString()) && DateTime.TryParse(val.ToString(), out _out))
                {
                    upload.Last_Payment_Applied_Date = _out;
                }
                else
                {
                    upload.Last_Payment_Applied_Date = null;
                }
            } },
            { "spanish_speaking_flag_(y/n)", (val, client, upload) => {
                if(val.ToString().Length > 0)
                    client.PreferredLanguage = Person.LANGUAGE.Spanish;
            } },              
            { "comment description", (val, client, upload) => {
                 client.Comment_Description = val.ToString(); 
            } },
            { "last payment applied amount", (val, client, upload) => {
                 client.Last_Payment_Applied_Amount = val.ToString(); 
            } }
        };
    }
}
