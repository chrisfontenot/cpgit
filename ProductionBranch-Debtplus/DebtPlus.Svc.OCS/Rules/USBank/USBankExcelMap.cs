﻿using System;
using System.Collections.Generic;
using System.Data.SqlTypes;
using System.Text.RegularExpressions;
using DebtPlus.LINQ;
using DebtPlus.LINQ.BusinessLayer;
using DebtPlus.OCS.Domain;
using DebtPlus.Svc.OCS;
using DebtPlus.Svc.OCS.Rules;

namespace DebtPlus.UI.Housing.OCS
{
    public class USBankExcelMap
    {
        private static Regex sanitizePhone = new Regex(@"\D");

        private static Action<object, OCSClient, LINQ.OCS_UploadRecord> getMapPhoneNumber(string columnName)
        {
            return (val, client, upload) =>
            {
                if (String.IsNullOrWhiteSpace(val.ToString()))
                {
                    return;
                }

                if (!CommonValidation.IsPhone(val.ToString()))
                {
                    return;
                }

                if (client.PhoneNumbers == null)
                {
                    client.PhoneNumbers = new List<TelephoneNumber>();
                }

                String result = sanitizePhone.Replace(val.ToString(), "");

                if (result.Length == 11)
                {
                    result = result.Substring(1);
                }                
                
                var newNumber = new TelephoneNumber
                {
                    SetNumber = result
                };

                if (client.PhoneNumbers.Exists(t => t.SearchValue == newNumber.SearchValue)) {
                    return;
                }

                switch (columnName)
                {
                    case "phone1":
                        upload.Phone1 = result;
                        break;
                    case "phone2":
                        upload.Phone2 = result;
                        break;
                    case "phone3":
                        upload.Phone3 = result;
                        break;
                    case "phone4":
                        upload.Phone4 = result;
                        break;
                    case "phone5":
                        upload.Phone5 = result;
                        break;
                }
                
                client.PhoneNumbers.autoAssignNumber(newNumber);
                client.PhoneNumbers.Add(newNumber);
            };
        }
                
        public static Dictionary<string, Action<string, OCSClient, LINQ.OCS_UploadRecord>> fieldConvertActions = new Dictionary<string, Action<string, OCSClient, LINQ.OCS_UploadRecord>> { 

            //special map function that gets called after all field-level mappings
            {"GLOBAL RECORD MAPPER", (val, client, upload) => {
                client.PreferredLanguage = Person.LANGUAGE.English;
                client.ContactAttemptCount = 0;
                client.ActiveFlag = client.PhoneNumbers != null && client.PhoneNumbers.Count > 0;
            } },            
            { "loan_id", (val, client, upload) => {
                upload.LoanNumber = val.ToString();
                client.LoanNo = val.ToString(); 
            } },
            { "case_type", (val, client, upload) => {
                if (!string.IsNullOrWhiteSpace(val))
                {
                    upload.CaseType = val;
                }
            } },
            { "status", (val, client, upload) => {
                // referred By
                if(!string.IsNullOrWhiteSpace(val))
                {
                    upload.Status = val;                    
                }
            } },
            { "entry_date", (val, client, upload) => {

                DateTime _out;

                if (!String.IsNullOrEmpty(val.ToString()) && DateTime.TryParse(val.ToString(), out _out))
                {
                    // Last Mod Date
                    upload.TrialPeriodStartDate = _out;
                }else{
                    upload.TrialPeriodStartDate = null;
                }
            } },
            { "months del", (val, client, upload) => {
                // Months Past Due
                int monthsDiff = 0;
                if(int.TryParse(val, out monthsDiff))
                {
                    upload.InvestorDueDate = DateTime.Now.AddMonths(monthsDiff);
                }
            } },
            { "borrower 1 name", (val, client, upload) => {

                var person = GetPersonName(val);
                if(person != null)
                {
                    upload.FirstName1 = person.FirstName;
                    upload.LastName1 = person.LastName;
                    client.Person1 = person;
                }              
            } },
            { "borrower 2 name", (val, client, upload) => {
                var person = GetPersonName(val);
                if(person != null)
                {
                    upload.FirstName2 = person.FirstName;
                    upload.LastName2 = person.LastName;
                    client.Person2 = person;
                }    
            } },
            { "mail address", (val, client, upload) => {
                upload.ClientStreet = val.ToString();
                if (client.Address == null) { client.Address = new Address(); }
                client.Address.street = val.ToString();
            } },
            { "city", (val, client, upload) => {
                upload.ClientCity = val.ToString();
                if (client.Address == null) { client.Address = new Address(); }
                client.Address.city = val.ToString();
            } },
            { "st", (val, client, upload) => {
                upload.ClientState = val.ToString();
                if (client.Address == null) { client.Address = new Address(); }
                if(!String.IsNullOrWhiteSpace(val.ToString())){
                    client.Address.StateProvince = (US_STATE)Enum.Parse(typeof(US_STATE), val.ToString().ToUpper(), true);
                }                
            } },
            { "zip", (val, client, upload) => {
                upload.ClientZipcode = val.ToString();
                if (client.Address == null) { client.Address = new Address(); }
                client.Address.PostalCode = val.ToString();
            } },
            { "borr 1 home", getMapPhoneNumber("phone1") },
            { "borr 1 work", getMapPhoneNumber("phone2") },
            { "borr 1 other", getMapPhoneNumber("phone3") },
            { "borr 2 work", getMapPhoneNumber("phone4") },
            
        };

        /// <summary>
        /// This function is used to split the client first and last name and trim any comma seperator
        /// </summary>
        /// <param name="name"></param>
        /// <returns></returns>
        private static Person GetPersonName(string name)
        {
            Person person = null;

            if (!string.IsNullOrWhiteSpace(name))
            {
                string[] clientName = name.Split(new char[] { ' ' });

                if (clientName.Length > 1)
                {
                    person = new Person();
                    person.LastName = clientName[0].Replace(',', ' ').Trim();
                    person.FirstName = clientName[1].Replace(',', ' ').Trim();
                }
            }

            return person;
        }

        private static Func<string,bool> dateValidator = (val) => {

            if (!String.IsNullOrEmpty(val.ToString()))
            {
                DateTime _out;
                return DateTime.TryParse(val.ToString(),out _out);
            }

            return true;             
        };

        public static Dictionary<string, Func<string, bool>> fieldValidationFuncs = new Dictionary<string, Func<string, bool>> { 
            { "loan_id", (val) => { return String.IsNullOrWhiteSpace(val.ToString()) == false;} },
            { "case_type",  (val) => { return true; } },
            { "status", (val) => { return true; } },
            { "entry_date", (val) => { return true; } },
            { "months del", (val) => { return true; } },
            { "borrower 1 name", (val) => { return String.IsNullOrWhiteSpace(val.ToString()) == false;; } },
            { "borrower 2 name", (val) => { return true; } },
            { "mail address", (val) => { return true; } },
            { "city", (val) => { return true; } },
            { "st", (val) => { return true; } },
            { "zip", (val) => { return true; } },
            { "borr 1 home", (val) => { return val.ToString().Trim() == String.Empty || CommonValidation.IsPhone(val.ToString()); } },
            { "borr 1 work", (val) => { return val.ToString().Trim() == String.Empty || CommonValidation.IsPhone(val.ToString()); } },
            { "borr 1 other", (val) => { return val.ToString().Trim() == String.Empty || CommonValidation.IsPhone(val.ToString()); } },
            { "borr 2 work", (val) => { return val.ToString().Trim() == String.Empty || CommonValidation.IsPhone(val.ToString()); } }
        };
    }
}
