﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Linq.Expressions;
using DebtPlus.LINQ;

namespace DebtPlus.OCS.Domain
{
    public class OCSClientDiff
    {
        private OCSClient original;
        private OCSClient changed;
        private List<string> messages = new List<string>();

        public List<string> Messages {
            get {
                return messages;
            }
        }
        
        private void message(string msg, params object[] objs)
        {
            messages.Add(String.Format("[Change]: " + msg, objs));
        }

        /// <summary>
        /// Convert PropertyNames to "Property Names"
        /// </summary>
        /// <param name="text"></param>
        /// <param name="preserveAcronyms"></param>
        /// <returns></returns>
        /// <see cref="http://stackoverflow.com/questions/272633/add-spaces-before-capital-letters"/>
        private string AddSpacesToSentence(string text, bool preserveAcronyms = true)
        {
            if (string.IsNullOrWhiteSpace(text))
                return string.Empty;
            StringBuilder newText = new StringBuilder(text.Length * 2);
            newText.Append(text[0]);
            for (int i = 1; i < text.Length; i++)
            {
                if (char.IsUpper(text[i]))
                    if ((text[i - 1] != ' ' && !char.IsUpper(text[i - 1])) ||
                        (preserveAcronyms && char.IsUpper(text[i - 1]) &&
                         i < text.Length - 1 && !char.IsUpper(text[i + 1])))
                        newText.Append(' ');
                newText.Append(text[i]);
            }
            return newText.ToString();
        }

        /// <summary>
        /// Compares two boxed primitive values
        /// </summary>
        /// <typeparam name="T1"></typeparam>
        /// <typeparam name="T2"></typeparam>
        /// <param name="val1"></param>
        /// <param name="val2"></param>
        /// <returns></returns>
        /// <see cref="http://stackoverflow.com/questions/6668697/boxed-value-type-comparisons"/>
        private static bool ValueEquality<T1, T2>(T1 val1, T2 val2)
            where T1 : IConvertible
            where T2 : IConvertible
        {
            // convert val2 to type of val1.
            T1 boxed2 = (T1)Convert.ChangeType(val2, typeof(T1));

            // compare now that same type.
            return (val1 == null && boxed2 == null) || val1.Equals(boxed2);
        }

        private void diff<T>(Expression<Func<Person, T>> e, Person lh, Person rh, String prefix = "") where T : IConvertible {
            diff<Person, T>(e, lh, rh, prefix);
        }

        /// <summary>
        /// Given a linq expression, compares two primitive property values and adds a message to list of returned messages.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="e"></param>
        /// <see cref="http://blogs.msdn.com/b/csharpfaq/archive/2010/03/11/how-can-i-get-objects-and-property-values-from-expression-trees.aspx"/>
        private void diff<T>(Expression<Func<OCSClient, T>> e, String prefix = "") where T : IConvertible
        {
            diff<OCSClient,T>(e, original, changed, prefix);
        }
        
        private void diff<T1,T2>(Expression<Func<T1, T2>> e, T1 _original, T1 _changed, String prefix = "") where T2 : IConvertible
        {
            var member = (MemberExpression)e.Body;
            var propName = AddSpacesToSentence(member.Member.Name);

            Func<T1, T2> f = e.Compile();

            var lhVal = f(_original);
            var rhVal = f(_changed);

            //something added
            if (lhVal == null && rhVal != null && (rhVal.GetType() == typeof(String)))
            {
                //going from null to empty string does not count as being added
                if (rhVal.GetType() == typeof(String) && String.IsNullOrWhiteSpace(rhVal.ToString())) return;

                message("Added \"{0}{1}\" as {2}",
                    (prefix == String.Empty ? "" : prefix + " "),
                    propName, rhVal);
            }
            //something deleted
            else if (lhVal != null && rhVal == null)
            {
                message("Deleted \"{0}{1}\"",
                    (prefix == String.Empty ? "" : prefix + " "),
                    propName);
            }
            //no change nulls
            else if (lhVal == null && rhVal == null)
            {
                return;
            }
            else if (!ValueEquality(lhVal, rhVal))
            {
                message("Changed \"{0}{1}\" from \"{2}\" to \"{3}\"",
                    (prefix == String.Empty ? "" : prefix + " "),
                    propName,
                    lhVal,
                    rhVal);
            }
        }
                
        private void added<T>(Expression<Func<OCSClient, T>> e, String prefix = "") where T : IConvertible
        {
            added<OCSClient, T>(e, changed, prefix);
        }

        private void added<T1,T2>(Expression<Func<T1, T2>> e, T1 _changed, String prefix = "") where T2 : IConvertible
        {
            var member = (MemberExpression)e.Body;
            var propName = member.Member.Name;

            Func<T1, T2> f = e.Compile();

            message("Added \"{0}{1}\" as {2}",
                (prefix == String.Empty ? "" : prefix + " "),
                AddSpacesToSentence(propName),
                f(_changed));
        }

        public OCSClientDiff(OCSClient _original, OCSClient _changed) {
            original = _original;
            changed = _changed;

            diff(c => c.ActualSaleDate);
            DiffAddresses(original.Address, changed.Address, "Address");
            if (original.UseHomeAddress == false || original.UseHomeAddress != changed.UseHomeAddress)
            {
                DiffAddresses(original.PropertyAddress, changed.PropertyAddress, "Property Address");
            }
            diff(c => c.ClientID.Value);
            diff(c => c.InvestorLoanNo);
            diff(c => c.ContactAttemptCount);
            diff(c => c.ActiveFlag);

            DiffPersons(original.Person1, changed.Person1,"Applicant");
            DiffPersons(original.Person2, changed.Person2,"CoApplicant");

            diff(c => c.PreferredLanguage);
            diff(c => c.StatusCode);

            if (changed.ContactAttemptCount > original.ContactAttemptCount)
            {
                message("Added Contact Attempt");
            }

            DiffContactAttempt(changed.CurrentAttempt);
        }

        private void DiffContactAttempt(ContactAttempt contactAttempt)
        {
            if (contactAttempt != null) {
                
                foreach (var outcome in contactAttempt.Outcomes.Where(o => o.PhoneNumber != null)) {
                    message("Call Outcome -- Phone Number: {0}, Phone Type: {1}, Result: {2}, MTI Result: {3}", 
                        outcome.PhoneNumber.Number, 
                        outcome.PhoneNumber.AutoAssignedType,
                        (outcome.Result.HasValue ? outcome.Result.Value : RESULT_CODE.NotSet),
                        (outcome.MTIResult.HasValue ? outcome.MTIResult.Value.ToString() : "NotSet"));
                }
            }
        }

        public void DiffAddresses(address first, address second, string msgPrefix) {
            //change
            if (first != null && second != null) {
                diff<address,string>(c => c.city,first,second, msgPrefix);

                if (first.StateProvince.HasValue && second.StateProvince.HasValue)
                {
                    diff<address, US_STATE>(c => c.StateProvince.Value, first, second, msgPrefix);
                }
                else if (!first.StateProvince.HasValue && second.StateProvince.HasValue)
                {
                    message("Added \"State\" as {0}", second.StateProvince.Value);
                }
                else if (first.StateProvince.HasValue && !second.StateProvince.HasValue)
                {
                    message("Deleted \"" + msgPrefix + " State\"");
                }

                diff<address, string>(c => c.street, first, second, msgPrefix);
                diff<address, string>(c => c.address_line_2, first, second, msgPrefix);
                diff<address, string>(c => c.PostalCode, first, second, msgPrefix);
            }
            //added
            else if (first == null && second != null)
            {
                message("Added " + msgPrefix);
                added<address, string>(c => c.city, second, msgPrefix);

                if (second.StateProvince.HasValue)
                {
                    added<address, US_STATE>(c => c.StateProvince.Value, second, msgPrefix);
                }

                added<address, string>(c => c.street, second, msgPrefix);
                added<address, string>(c => c.address_line_2, second, msgPrefix);
                added<address, string>(c => c.PostalCode, second, msgPrefix);
            //deleted
            } else if (first != null && second == null)
            {
                message("Deleted " + msgPrefix);
            }
        }
    
        public void DiffPersons(Person first, Person second, string whom){
            if (first != null && second != null) {

                if (first.Salutation.HasValue && second.Salutation.HasValue){
                    diff(c => c.Salutation.Value, first, second, whom);
                }
                else if (!first.Salutation.HasValue && second.Salutation.HasValue){
                    message("Added \"{0} Salutation\" as {1}", whom, second.Salutation.Value);
                }
                else if (first.Salutation.HasValue && !second.Salutation.HasValue){
                    message("Deleted \"{0} Salutation\"", whom);
                }

                diff(c => c.FirstName,first,second, whom);
                diff(c => c.MiddleName, first, second, whom);
                diff(c => c.LastName, first, second, whom);

                if (first.Suffix.HasValue && second.Suffix.HasValue)
                {
                    diff(c => c.Suffix.Value, first, second, whom);
                } else if (!first.Suffix.HasValue && second.Suffix.HasValue){
                    message("Added \"{0} Suffix\" as {1}", whom, second.Suffix.Value);
                }
                else if (first.Suffix.HasValue && !second.Suffix.HasValue){
                    message("Deleted \"{0} Suffix\"",whom);
                }

                diff(c => c.EmailAddress, first, second, whom);
                diff(c => c.SSN, first, second, whom);

            }else if (first == null && second != null) {
                message("Added {0}",whom);
                if (second.Salutation.HasValue)
                {
                    added(c => c.Salutation.Value,second, whom);
                }

                added(c => c.FirstName, second, whom);
                added(c => c.MiddleName, second, whom);
                added(c => c.LastName, second, whom);
                if (second.Suffix.HasValue)
                {
                    added(c => c.Suffix.Value, second, whom);
                }
                added(c => c.EmailAddress, second, whom);
                added(c => c.SSN, second, whom);
            }
            else if(first != null && second == null)
            {
                if (!string.IsNullOrWhiteSpace(first.EmailAddress) && !string.IsNullOrWhiteSpace(first.FirstName) && 
                    !string.IsNullOrWhiteSpace(first.LastName) && !string.IsNullOrWhiteSpace(first.MiddleName) && 
                    !string.IsNullOrWhiteSpace(first.SSN))
                {
                    message("Deleted {0}", whom);
                }
            }
        }
    }
}
