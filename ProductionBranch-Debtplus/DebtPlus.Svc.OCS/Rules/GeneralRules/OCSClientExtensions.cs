﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DebtPlus.OCS.Domain;
using DebtPlus.LINQ;

namespace DebtPlus.OCS.Domain
{
    public static class OCSClientExtensions
    {
        public static OCSClient.ValidationResult Validate(this OCSClient original, OCSClient changed) {
            
            var toReturn = new OCSClient.ValidationResult();

            toReturn.PassedChecks.Add("Passed ALL OCS Client Validation");
            toReturn.Warnings = toReturn.Warnings.Concat(new OCSClientDiff(original, changed).Messages).ToList();

            return toReturn;
        }
    }

    public static class ContactAttemptExtensions
    {
        public static OCSClient StartContactAttempt(this OCSClient self)
        {
            self.CurrentAttempt = new ContactAttempt();
            self.CurrentAttempt.StartAttempt();

            self.CurrentAttempt.Outcomes = self.PhoneNumbers
                .Select(pn => new ContactAttempt.CallOutcome
                {
                    PhoneNumber = new TelephoneNumber
                    {
                        AutoAssignedType = pn.AutoAssignedType,
                        SetNumber = pn.Number,
                        UserAssignedType = pn.UserAssignedType,
                        Acode = pn.Acode
                    },
                    Result = RESULT_CODE.NotSet
                }).ToList();

            return self;
        }

        public static OCSClient CloseContactAttempt(this OCSClient self)
        {
            if (self.CurrentAttempt == null || self.CurrentAttempt.IsClosed)
            {
                throw new InvalidOperationException("It is not allowed to close a contact attempt unless we are currently making a contact attempt.  The current contact attempt must be set and must not be closed.");
            }

            foreach (var outcome in self.CurrentAttempt.Outcomes.Where(o => !o.Result.HasValue || o.Result == RESULT_CODE.NotSet))
            {
                outcome.Result = RESULT_CODE.Skip;
            }

            self.CurrentAttempt.EndAttempt();
            self.ContactAttempts.Add(self.CurrentAttempt);
            //debatable whether or not we should go ahead and set the the current attempt to null or leave it alone
            //for now we are leaving it alone and it will be replaced if we start another new attempt

            return self;
        }
    }

    public static class TelephoneNumberExtensions {
        public static void autoAssignNumber(this List<TelephoneNumber> phones, TelephoneNumber number)
        {
            PHONE_TYPE setType = PHONE_TYPE.ClientHome;

            if (!phones.Exists(p => p.AutoAssignedType == PHONE_TYPE.ClientHome))
            {
                setType = PHONE_TYPE.ClientHome;
            }
            else if (!phones.Exists(p => p.AutoAssignedType == PHONE_TYPE.ClientMsg))
            {
                setType = PHONE_TYPE.ClientMsg;
            }
            else if (!phones.Exists(p => p.AutoAssignedType == PHONE_TYPE.ApplicantCell))
            {
                setType = PHONE_TYPE.ApplicantCell;
            }
            else if (!phones.Exists(p => p.AutoAssignedType == PHONE_TYPE.ApplicantWork))
            {
                setType = PHONE_TYPE.ApplicantWork;
            }
            else if (!phones.Exists(p => p.AutoAssignedType == PHONE_TYPE.CoApplicantCell))
            {
                setType = PHONE_TYPE.CoApplicantCell;
            }
            number.AutoAssignedType = setType;
        }

        public static void AssignNumber(this List<TelephoneNumber> phones, TelephoneNumber number, PHONE_TYPE phoneType)
        {
            if (!phones.Exists(p => p.UserAssignedType == phoneType))
            {
                number.UserAssignedType = phoneType;
            }
        }
    }
}