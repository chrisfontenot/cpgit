﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DebtPlus.LINQ;

namespace DebtPlus.OCS.Domain
{
    public static class TimezoneHelper
    {
        public static TimeZoneInfo GetTzInfo(this TIMEZONE self) {
            switch (self) { 
                case TIMEZONE.Atlantic:
                    return TimeZoneInfo.FindSystemTimeZoneById("Atlantic Standard Time");
                case TIMEZONE.Eastern:
                    return TimeZoneInfo.FindSystemTimeZoneById("Eastern Standard Time");
                case TIMEZONE.Central:
                    return TimeZoneInfo.FindSystemTimeZoneById("Central Standard Time");
                case TIMEZONE.Mountain:
                    return TimeZoneInfo.FindSystemTimeZoneById("Mountain Standard Time");
                case TIMEZONE.Pacific:
                    return TimeZoneInfo.FindSystemTimeZoneById("Pacific Standard Time");
                case TIMEZONE.Alaska:
                    return TimeZoneInfo.FindSystemTimeZoneById("Alaskan Standard Time");
                case TIMEZONE.Hawaii:
                    return TimeZoneInfo.FindSystemTimeZoneById("Hawaiian Standard Time");
                case TIMEZONE.Chamorro:
                    return TimeZoneInfo.FindSystemTimeZoneById("West Pacific Standard Time");
                default:
                    return TimeZoneInfo.FindSystemTimeZoneById("Hawaiian Standard Time");
            }
        }

        /// <summary>
        /// Gives you the current time for a given timezone.
        /// </summary>
        /// <param name="self"></param>
        /// <see cref="http://stackoverflow.com/questions/7206624/how-to-find-current-time-of-different-culture-i-e-different-timezone-in-c-sharp"/>
        /// <seealso cref="http://msdn.microsoft.com/en-us/library/ms912391%28v=winembedded.11%29.aspx"/>
        /// <seealso cref="http://msdn.microsoft.com/en-us/library/system.timezoneinfo.findsystemtimezonebyid(v=vs.110).aspx"/>
        /// <returns></returns>
        public static DateTime WhatTimeIsItNow(this TIMEZONE self, DateTime serverTime)
        {
            var nowHere = serverTime;//DateTime.Now;
            
            return TimeZoneInfo.ConvertTime(nowHere, TimeZoneInfo.Local, self.GetTzInfo());
        }

        public static bool IsItPast8am(this TIMEZONE self, DateTime serverTime)
        {
            //indeterminate follows special rules
            if (self == TIMEZONE.Indeterminate) {
                return TIMEZONE.Eastern.WhatTimeIsItNow(serverTime).Hour >= 14 && TIMEZONE.Eastern.WhatTimeIsItNow(serverTime).Hour < 21;
            }

            return self.WhatTimeIsItNow(serverTime).Hour >= 8 && self.WhatTimeIsItNow(serverTime).Hour < 21;
        }

        private static List<TIMEZONE> TzList = new List<TIMEZONE> { 
            TIMEZONE.Atlantic, TIMEZONE.Eastern, TIMEZONE.Central, TIMEZONE.Mountain, TIMEZONE.Pacific, TIMEZONE.Alaska, TIMEZONE.Hawaii, TIMEZONE.Chamorro, TIMEZONE.Indeterminate
        };
        
        public static List<TIMEZONE> TimeZonesPast8am(DateTime serverTime)
        {
            return TzList.Where(tz => tz.IsItPast8am(serverTime)).Select(tz => tz).ToList();
        }

        public static int GetOffset(this TIMEZONE self) {
            int toReturn = -5;
            var tz = self;

            switch (tz)
            {
                case TIMEZONE.Atlantic:
                    toReturn = -4;
                    break;
                case TIMEZONE.Eastern:
                    toReturn = -5;
                    break;
                case TIMEZONE.Central:
                    toReturn = -6;
                    break;
                case TIMEZONE.Mountain:
                    toReturn = -7;
                    break;
                case TIMEZONE.Pacific:
                    toReturn = -8;
                    break;
                case TIMEZONE.Alaska:
                    toReturn = -9;
                    break;
                case TIMEZONE.Hawaii:
                    toReturn = -10;
                    break;
                case TIMEZONE.Chamorro:
                    toReturn = +10;
                    break;
                default:
                    toReturn = -5;
                    break;
            }

            return toReturn;
        }
    }
   
}
