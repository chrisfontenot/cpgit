﻿using System;
using System.Linq;
using System.Text.RegularExpressions;

namespace DebtPlus.Svc.OCS.Rules
{
    public static class CommonValidation
    {
        public static Regex sanitizePhone = new Regex(@"\D");
        public static String take(this String self, int howMany) {
            return String.Concat(self.Take(howMany));
        }
        public static String skip(this String self, int howMany)
        {
            return String.Concat(self.Skip(howMany));
        }
        public static bool allAreTheSame (this String str)
        {
            var first = str.First();
            return str.All(c => c == first);
        }

        public static Regex whitespaceRuns = new Regex(@"\s{2,}");
        public static String stringifyAndSuperTrim(this object self) {
            if (self == null) return String.Empty;

            var toReturn = self.ToString().Trim();

            //makes sure there is only a single whitespace
            return whitespaceRuns.Replace(toReturn, " ");
        }

        public static bool IsPhone(String val) {
            String result = sanitizePhone.Replace(val.ToString(), "");
            
            /* example: 1-555-555-5555, so valid lengths are: 7, 10, 11 */
            if (result.Length == 11) { result = result.Substring(1); } //strip country code
            else if (result.Length != 10 && result.Length != 7) { return false; } //invalid length

            //all characters the same digit
            if(result.allAreTheSame()) return false;

            if(result.Length == 10){

                var _area = result.take(3).ToString();

                //any area code less than 201 is bad
                if(Convert.ToInt32(result.take(3)) < 201) return false;
                //area code + exchange
                if(result.take(6).allAreTheSame()) return false;
                //area code + last four
                if((result.take(3) + result.skip(6)).allAreTheSame()) return false;
                //exchange + last four
                if(result.skip(3).allAreTheSame()) return false;
            }

            return true;
        }
    }
}