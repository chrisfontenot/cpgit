﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DebtPlus.OCS.Domain;

namespace DebtPlus.Svc.OCS.ExternalApi
{
    public class SMWrapper
    {
        // private OCSDbMapper repository = null;

        public static void fireEvent(int clientId, TRIGGER eventTrigger) {
            // DebtPlus.LINQ.SQLInfoClass SqlInfo = DebtPlus.LINQ.SQLInfoClass.getDefault();
            // var connectionString = DebtPlus.LINQ.SQLInfoClass.getDefault().ConnectionString;
            DebtPlus.LINQ.BusinessContext bc = new LINQ.BusinessContext(); // This is not needed nor recommended to be used (connectionString);
            var repository = new OCSDbMapper(bc);

            fireEvent(clientId, eventTrigger, repository);
        }
        
        public static void fireEvent(int clientId, TRIGGER eventTrigger, OCSDbMapper repository){
                        
            var oldClient = repository.GetActiveClientById(clientId);
            var newClient = shallowCopy(oldClient);
            MasterStateMachine.UpdateOnTrigger(newClient.Program, eventTrigger, oldClient.StatusCode, newClient);

            var result = oldClient.Validate(newClient);
            StringBuilder sb = new StringBuilder();
            foreach (var change in result.Warnings.Where(w => w.StartsWith("[Change]: ")))
            {
                sb.AppendLine(change);
            }

            newClient.AddNote("SOME USER", "Update", sb.ToString(), (int)DebtPlus.LINQ.InMemory.Notes.NoteTypes.System);

            repository.UpdateActiveClient(newClient);
        }

        private static OCSClient shallowCopy(OCSClient oldClient)
        {
            return new OCSClient { 
                ActiveFlag = oldClient.ActiveFlag,
                ActualSaleDate = oldClient.ActualSaleDate,
                Address = oldClient.Address,
                Archive = oldClient.Archive,
                ClaimedBy = oldClient.ClaimedBy,
                ClaimedDate = oldClient.ClaimedDate,
                ClientID = oldClient.ClientID,
                ContactAttemptCount = oldClient.ContactAttemptCount,
                ContactAttempts = oldClient.ContactAttempts,
                CurrentAttempt = oldClient.CurrentAttempt,
                DueDate = oldClient.DueDate,
                InvestorLoanNo = oldClient.InvestorLoanNo,
                ID = oldClient.ID,
                IsDuplicate = oldClient.IsDuplicate,
                LastChanceList = oldClient.LastChanceList,
                LoanNo = oldClient.LoanNo,
                Notes = oldClient.Notes,
                Person1 = oldClient.Person1,
                Person2 = oldClient.Person2,
                PhoneNumbers = oldClient.PhoneNumbers,
                PreferredLanguage = oldClient.PreferredLanguage,
                PropertyAddress = oldClient.PropertyAddress,
                Servicer = oldClient.Servicer,
                StatusCode = oldClient.StatusCode,
                FirstCounselDate = oldClient.FirstCounselDate,
                SecondCounselDate = oldClient.SecondCounselDate,
                UploadAttempt = oldClient.UploadAttempt,
                UploadRecordId = oldClient.UploadRecordId,
                UseHomeAddress = oldClient.UseHomeAddress,
#pragma warning disable 618
                ApplicantCell = oldClient.ApplicantCell,
                ApplicantWork = oldClient.ApplicantWork,
                ClientHomePhone = oldClient.ClientHomePhone,
                ClientMsgPhone = oldClient.ClientMsgPhone,
                CoApplicantCell = oldClient.CoApplicantCell,
                CoApplicantWork = oldClient.CoApplicantWork,
#pragma warning restore 618
                Program = oldClient.Program
            };
        }
    
    }
}
