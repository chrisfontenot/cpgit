#region Copyright 2000-2012 DebtPlus, L.L.C.
//{*******************************************************************}
//{                                                                   }
//{       DebtPlus Debt Management System                             }
//{                                                                   }
//{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
//{       ALL RIGHTS RESERVED                                         }
//{                                                                   }
//{   The entire contents of this file is protected by U.S. and       }
//{   International Copyright Laws. Unauthorized reproduction,        }
//{   reverse-engineering, and distribution of all or any portion of  }
//{   the code contained in this file is strictly prohibited and may  }
//{   result in severe civil and criminal penalties and will be       }
//{   prosecuted to the maximum extent possible under the law.        }
//{                                                                   }
//{   RESTRICTIONS                                                    }
//{                                                                   }
//{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
//{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
//{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
//{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
//{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
//{                                                                   }
//{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
//{   ADDITIONAL RESTRICTIONS.                                        }
//{                                                                   }
//{*******************************************************************}
#endregion

using System;
using System.Data;
using System.Data.SqlClient;
using System.Collections.Generic;
using System.Windows.Forms;
using System.Linq;
using System.ComponentModel;
using DebtPlus.LINQ;

namespace DebtPlus.UI.Creditor.Widgets.Controls
{
    public partial class CreditorID : DevExpress.XtraEditors.ButtonEdit
    {

        /// <summary>
        /// Handle the creation of the control
        /// </summary>
        public CreditorID()
        {
            InitializeComponent();
            EditValue = string.Empty;
            TabOnSearch = true;

            // Force the character casing to be upper case. It looks better.
            Properties.CharacterCasing = CharacterCasing.Upper;

            try  //This may cause an error for some cases. Don't trap on the error condition.
            {
                AllowDrop = true;
            }
#pragma warning disable 168
            catch (Exception ex) { }
#pragma warning restore 168
            RegisterHandlers();
        }

        /// <summary>
        /// Add any event handler registration
        /// </summary>
        private void RegisterHandlers()
        {
        }

        /// <summary>
        /// Remove all events added by RegisterHandlers
        /// </summary>
        private void UnRegisterHandlers()
        {
        }

        /// <summary>
        /// Validation that we perform on the creditor ID
        /// </summary>
        [System.Xml.Serialization.XmlType(Namespace = "urn:CreditorID")]
        [System.Xml.Serialization.XmlRoot(Namespace = "urn:CreditorID")]
        public enum ValidationLevelEnum
        {
            None = 0,
            Exists = 1,
            NotProhibitUse = 2
        }

        /// <summary>
        /// Should we do a tab operation when search is complete
        /// </summary>
        [Description("Should next control be selected when search is completed"), Category("DebtPlus"), Browsable(true), DefaultValue(typeof(bool), "true")]
        public bool TabOnSearch { get; set; }

        /// <summary>
        /// Raised when we change the validation level
        /// </summary>
        public event EventHandler ValidationLevelChanged;
        protected void RaiseValidationLevelChanged(EventArgs e)
        {
            EventHandler evt = ValidationLevelChanged;
            if (evt != null)
            {
                evt(this, e);
            }
        }

        protected virtual void OnValidationLevelChanged(EventArgs e)
        {
            RaiseValidationLevelChanged(e);
        }

        private ValidationLevelEnum _ValidationLevel = ValidationLevelEnum.None;

        /// <summary>
        /// Validation that we perform on the creditor ID
        /// </summary>
        [Description("Types of validation that we perform"), Category("DebtPlus"), Browsable(true), DefaultValue(typeof(ValidationLevelEnum), "None")]
        public ValidationLevelEnum ValidationLevel
        {
            get
            {
                return _ValidationLevel;
            }
            set
            {
                if (value != _ValidationLevel)
                {
                    _ValidationLevel = value;
                    OnValidationLevelChanged(EventArgs.Empty);
                }
            }
        }

        /// <summary>
        /// return the creditor ID as a string value
        /// </summary>
        [Description("Decoded edit value for the creditor ID"), Category("Data"), Browsable(true), DefaultValue(typeof(string), "")]
        public new string EditValue
        {
            get
            {
                if (base.EditValue == null || base.EditValue == DBNull.Value)
                {
                    return null;
                }
                return Convert.ToString(base.EditValue).Trim();
            }

            set
            {
                if (string.IsNullOrEmpty(value))
                {
                    base.EditValue = null;
                }
                else
                {
                    base.EditValue = value;
                }
            }
        }

        #region  drag/drop
        protected override void OnMouseMove(MouseEventArgs e)
        {
            base.OnMouseMove(e);

            // Do nothing if the operation is not valid
            if ( (e.Button == 0) || (e.X >= 0 && e.X < Width && e.Y >= 0 && e.Y < Height) )
            {
                return;
            }

            string txt;

            // if this is a text box then look for the selected text component of the text field
            if (((DevExpress.XtraEditors.TextEdit)this).SelectionLength > 0)
            {
                txt = SelectedText;
            }
            else
            {
                txt = string.Empty;
            }

            // if the text is missing then try the entire text field
            if (txt == string.Empty)
            {
                txt = Text.Trim();
            }

            // if there is no text then do nothing
            if (txt == string.Empty)
            {
                return;
            }

            // Start a drag-drop operation when the mouse moves outside the control
            DataObject dobj = new DataObject();
            dobj.SetData(DataFormats.Text, true, txt);

            // Do the operation. It will return when the operation is complete.
            DragDropEffects effect = DragDropEffects.Copy;
            effect = DoDragDrop(dobj, effect);
        }

        protected override void OnDragEnter(DragEventArgs e)
        {
            base.OnDragEnter(e);

            // if there is a text field then look to determine the accepable processing
            if (e.Data.GetDataPresent(DataFormats.Text, true))
            {
                e.Effect = e.AllowedEffect & DragDropEffects.Copy;
            }
            else
            {
                e.Effect = DragDropEffects.None;
            }
        }

        protected override void OnDragDrop(DragEventArgs e)
        {
            base.OnDragDrop(e);

            // if there is a text field then look to determine the accepable processing
            if (!e.Data.GetDataPresent(DataFormats.Text, true))
            {
                return;
            }

            // We only support copy. No modifier needs to be tested
            e.Effect = e.AllowedEffect & DragDropEffects.Copy;

            // Paste the text into the control
            EditValue = Convert.ToString(e.Data.GetData(DataFormats.Text, true));
        }

        protected override void OnQueryContinueDrag(QueryContinueDragEventArgs e)
        {
            // if the escape key is pressed then cancel the operation
            if (e.EscapePressed)
            {
                e.Action = DragAction.Cancel;
                return;
            }

            base.OnQueryContinueDrag(e);
        }
        #endregion

        #region  Creditor search
        protected override void OnClickButton(DevExpress.XtraEditors.Drawing.EditorButtonObjectInfoArgs buttonInfo)
        {
            string strTag = buttonInfo.Button.Tag as string;
            if (strTag != null && string.Compare(strTag, "search", true, System.Globalization.CultureInfo.InvariantCulture) == 0)
            {
                PerformCreditorSearch();
            }
            else
            {
                base.OnClickButton(buttonInfo);
            }
        }

        public virtual void PerformCreditorSearch()
        {
            using (var SearchItem = new DebtPlus.UI.Creditor.Widgets.Search.CreditorSearchClass())
            {
                if (SearchItem.ShowDialog() == DialogResult.OK)
                {
                    EditValue = SearchItem.Creditor;
                    if (TabOnSearch)
                    {
                        Parent.SelectNextControl(this, true, true, false, true);
                    }
                }
            }
        }
        #endregion

        /// <summary>
        /// Handle the keypress event. Do not allow leading whitespace
        /// </summary>
        protected override void OnEditorKeyPress(KeyPressEventArgs e)
        {
            base.OnEditorKeyPress(e);

            // Leading spaces are not significant and are removed
            if (!e.Handled)
            {
                if (!Char.IsControl(e.KeyChar))
                {
                    if (Char.IsWhiteSpace(e.KeyChar) && Text == string.Empty)
                    {
                        e.Handled = true;
                    }
                }
            }
        }

        /// <summary>
        /// Process the control creation
        /// </summary>
        protected override void OnCreateControl()
        {
            base.OnCreateControl();

            Properties.NullText = string.Empty;
            Properties.MaxLength = 10;
            Properties.CharacterCasing = CharacterCasing.Upper;

            Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.RegEx;
            Properties.Mask.EditMask = "[A-Z]{1,2}[0-9]{4,}";
            Properties.Mask.UseMaskAsDisplayFormat = true;
            Properties.Mask.BeepOnError = true;
            Properties.Mask.AutoComplete = DevExpress.XtraEditors.Mask.AutoCompleteType.Strong;
        }

        /// <summary>
        /// When the control gets focus, select all of the text
        /// </summary>
        protected override void OnGotFocus(EventArgs e)
        {
            base.OnGotFocus(e);
            SelectAll();
        }

        /// <summary>
        /// When the control gets focus, select all of the text
        /// </summary>
        protected override void OnValidating(CancelEventArgs e)
        {
            // Do the base logic first. This updates the edit value.
            base.OnValidating(e);
            if (e.Cancel)
            {
                return;
            }

            // Do checking on a creditor when a creditor is given
            if (! string.IsNullOrEmpty( EditValue ) && ValidationLevel != ValidationLevelEnum.None)
            {
                // Clear the error text from the previous call
                ErrorText = string.Empty;
                try
                {
                    using (var bc = new BusinessContext())
                    {
                        var q = bc.creditors.Where(s => s.Id == EditValue).FirstOrDefault();
                        if (q == null)
                        {
                            ErrorText = "Creditor does not exist";
                            e.Cancel = true;
                            return;
                        }

                        if (ValidationLevel == ValidationLevelEnum.NotProhibitUse && q.prohibit_use)
                        {
                            ErrorText = "Creditor marked PROHIBIT USE";
                            e.Cancel = true;
                            return;
                        }
                    }
                }

                catch (System.Data.SqlClient.SqlException ex)
                {
                    ErrorText = ex.Message;
                    DebtPlus.UI.Common.ErrorHandling.HandleErrors(ex);
                }
            }
        }
    }
}
