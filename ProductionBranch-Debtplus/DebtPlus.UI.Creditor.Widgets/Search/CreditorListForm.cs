﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using DebtPlus;
using System.Data.SqlClient;

namespace DebtPlus.UI.Creditor.Widgets.Search
{
    internal partial class CreditorListForm : DebtPlus.Data.Forms.DebtPlusForm
    {
        /// <summary>
        /// Handle the disposing event for the class
        /// </summary>
        /// <param name="disposing"></param>
        private void CreditorListForm_Dispose(bool disposing)
        {
            // Remove the event handlers when the class is disposed.
            if (disposing && !DesignMode)
            {
                UnRegisterHandlers();
            }
        }

        /// <summary>
        /// Initialize the form class
        /// </summary>
        /// <param name="DatabaseInfo"></param>
        public CreditorListForm() : base()
        {
            InitializeComponent();
            if (!DesignMode)
            {
                RegisterHandlers();
            }
        }

        /// <summary>
        /// Register the event handlers
        /// </summary>
        private void RegisterHandlers()
        {
            this.Load += CreditorListForm_Load;
            Button_More.Click += Button_More_Click;
            GridView1.DoubleClick += GridView1_DoubleClick;
            GridView1.FocusedRowChanged += GridView1_FocusedRowChanged;
            Button_Print.Click += Button_Print_Click;
            Button_OK.Click += Button_OK_Click;
            GridControl1.Click += GridControl1_Click;
            CheckEdit_Preview.CheckStateChanged += CheckEdit_Preview_CheckStateChanged;
            GridView1.Layout += LayoutChanged;
        }

        /// <summary>
        /// Remove the event handler registrations
        /// </summary>
        private void UnRegisterHandlers()
        {
            this.Load -= CreditorListForm_Load;
            Button_More.Click -= Button_More_Click;
            Button_OK.Click -= Button_OK_Click;
            GridView1.DoubleClick -= GridView1_DoubleClick;
            GridView1.FocusedRowChanged -= GridView1_FocusedRowChanged;
            Button_Print.Click -= Button_Print_Click;
            GridControl1.Click -= GridControl1_Click;
            CheckEdit_Preview.CheckStateChanged -= CheckEdit_Preview_CheckStateChanged;
            GridView1.Layout -= LayoutChanged;
        }

        /// <summary>
        /// Creditor from the search operation
        /// </summary>
        public string Creditor{ get; set; }

        public event EventHandler NextPage;
        /// <summary>
        /// Process a click event on the MORE button
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Button_More_Click(object sender, EventArgs e)
        {
            if( NextPage != null )
            {
                NextPage(this, new EventArgs());
            }
        }

        /// <summary>
        /// Handle the double-click event on the grid view
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void GridView1_DoubleClick(object sender, EventArgs e)
        {
            DataRow row = null;
            DevExpress.XtraGrid.Views.Grid.ViewInfo.GridHitInfo hi = GridView1.CalcHitInfo((GridControl1.PointToClient(System.Windows.Forms.Control.MousePosition)));

            // Find and set the row handle to the grid control
            GridView1.FocusedRowHandle = hi.RowHandle;
            if (hi.RowHandle >= 0)
            {
                row = (DataRow)GridView1.GetDataRow(hi.RowHandle);
            }

            // If there is a row then find the creditor and complete the dialog
            if( row != null )
            {
                Creditor = Convert.ToString(row["creditor"]);
                if (Creditor != null)
                {
                    DialogResult = System.Windows.Forms.DialogResult.OK;
                }
            }
        }

        /// <summary>
        /// Process the OK button being clicked
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Button_OK_Click(object sender, EventArgs e)
        {
            DialogResult = System.Windows.Forms.DialogResult.OK;
        }

        /// <summary>
        /// Process a change in the focused row for the grid
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void GridView1_FocusedRowChanged(object sender, DevExpress.XtraGrid.Views.Base.FocusedRowChangedEventArgs e)
        {
            try
            {
                // If there is a row then process the double-click event. Ignore it if there is no row.
                DataRow row = null;
                if( e.FocusedRowHandle >= 0 )
                {
                    row = (DataRow) GridView1.GetDataRow(e.FocusedRowHandle);
                }

                // From the datasource, we can find the client in the table.
                if( row != null )
                {
                    Creditor = Convert.ToString(row["creditor"]);
                }
            }

            catch( Exception ex )
            {
                DebtPlus.Svc.Common.ErrorHandling.HandleErrors(ex);
                Creditor = string.Empty;
            }

            // Enable the OK button if the creditor is specified
            Button_OK.Enabled = ! string.IsNullOrEmpty ( Creditor );
        }

        /// <summary>
        /// Process a click event on the PRINT button
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Button_Print_Click(object sender, EventArgs e)
        {
            bool Success = false;

            Cursor currentCursor = Cursor.Current;
            try
            {
                Cursor.Current = Cursors.WaitCursor;
                if( GridControl1.IsPrintingAvailable )
                {
                    GridControl1.ShowPrintPreview();
                    Success = true;
                }

            }
            catch( Exception ex )
            {
                DebtPlus.Svc.Common.ErrorHandling.HandleErrors(ex);
            }
            finally
            {
                Cursor.Current = currentCursor;
            }

            if( ! Success )
            {
                DebtPlus.Data.Forms.MessageBox.Show(this, "Sorry, that function is not implemented at this time. It will be shortly.", "Function not available", MessageBoxButtons.OK);
            }
        }

        /// <summary>
        /// Process a click event on the grid
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void GridControl1_Click(object sender, EventArgs e)
        {
            DataRow row = null;
            DevExpress.XtraGrid.Views.Grid.ViewInfo.GridHitInfo hi = GridView1.CalcHitInfo((GridControl1.PointToClient(System.Windows.Forms.Control.MousePosition)));

            // Find and set the row handle to the grid control
            GridView1.FocusedRowHandle = hi.RowHandle;
            if (hi.RowHandle >= 0)
            {
                row = (DataRow)GridView1.GetDataRow(hi.RowHandle);
            }

            // If there is a row then find the creditor and complete the dialog
            if (row != null)
            {
                Creditor = Convert.ToString(row["creditor"]);
                if (Creditor != null)
                {
                    DialogResult = System.Windows.Forms.DialogResult.OK;
                }
            }
        }

        /// <summary>
        /// Load and initalize the form
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void CreditorListForm_Load(object sender, EventArgs e)
        {
            UnRegisterHandlers();
            try
            {
                // Try to restore the layout from the registry settings.
                ReloadGridControlLayout();

                // Enable or disable the addresses in the list
                CheckEdit_Preview.Checked = ShowCreditorAddresses;
                Enable_Disable_Addresses(CheckEdit_Preview.Checked);

                // Select the first creditor in the list
                DevExpress.XtraGrid.Views.Grid.ViewInfo.GridHitInfo hi = GridView1.CalcHitInfo((GridControl1.PointToClient(System.Windows.Forms.Control.MousePosition)));

                // Find and set the row handle to the grid control
                GridView1.FocusedRowHandle = 0;

                // If there is a row then find the creditor and complete the dialog
                DataRow row = (DataRow)GridView1.GetDataRow(0);
                if (row != null)
                {
                    Creditor = Convert.ToString(row["creditor"]);
                }
            }
            finally
            {
                RegisterHandlers();
            }
        }

        /// <summary>
        /// Process a change in the creditor_address checkbox
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void CheckEdit_Preview_CheckStateChanged(object sender, EventArgs e)
        {
            UnRegisterHandlers();
            try
            {
                bool Result = CheckEdit_Preview.Checked;
                ShowCreditorAddresses = Result;
                Enable_Disable_Addresses(Result);
            }

            finally
            {
                RegisterHandlers();
            }
        }

        /// <summary>
        /// if the layout is changed then update the file
        /// </summary>
        /// <param name="Sender"></param>
        /// <param name="e"></param>
        private void LayoutChanged(object Sender, EventArgs e)
	    {
            UnRegisterHandlers();
            try
		    {
                string PathName = XMLBasePath();
                if( ! string.IsNullOrEmpty(PathName) )
			    {
                    if( ! System.IO.Directory.Exists(PathName) )
				    {
                        System.IO.Directory.CreateDirectory(PathName);
                    }
                    string FileName = System.IO.Path.Combine(PathName, string.Format("{0}.Grid.xml", Name));
                    GridView1.SaveLayoutToXml(FileName);
                }
            }
		    finally
		    {
                RegisterHandlers();
            }
        }

        /// <summary>
        /// Enable or disable the display the address field
        /// </summary>
        /// <param name="Value"></param>
        private void Enable_Disable_Addresses(bool Value)
        {
            if( Value )
            {
                // Display the preview lines
                GridView1.OptionsView.RowAutoHeight = true;
                GridView1.OptionsView.ShowPreviewRowLines = DevExpress.Utils.DefaultBoolean.True;
                GridView1.OptionsView.RowAutoHeight = true;
                GridView1.OptionsView.AutoCalcPreviewLineCount = true;
                GridView1.OptionsView.ShowPreview = true;
                GridView1.PreviewFieldName = "address";
            }
            else
            {
                // Hide them
                GridView1.OptionsView.ShowPreviewRowLines = DevExpress.Utils.DefaultBoolean.False;
                GridView1.OptionsView.RowAutoHeight = false;
                GridView1.OptionsView.AutoCalcPreviewLineCount = false;
                GridView1.OptionsView.ShowPreview = false;
            }
        }

        /// <summary>
        /// Should the creditor addresses be shown?
        /// </summary>
        private bool ShowCreditorAddresses
        {
            get
            {
                Microsoft.Win32.RegistryKey RegKey;
                bool Result = false;
                string strResult = Boolean.FalseString;

                // Look in the machine values for the defaults for all users
                RegKey = Microsoft.Win32.Registry.LocalMachine;
                try
                {
                    RegKey = RegKey.OpenSubKey(DebtPlus.Configuration.AssemblyInfo.InstallationRegistryKey + @"\options", false);
                    if( RegKey != null )
                    {
                        strResult = Convert.ToString(RegKey.GetValue("show creditor addresses", strResult));
                    }
                }

                catch( System.IO.DirectoryNotFoundException ex )
                {
                    DebtPlus.Svc.Common.ErrorHandling.HandleErrors(ex);
                }

                catch( System.IO.FileNotFoundException ex )
                {
                    DebtPlus.Svc.Common.ErrorHandling.HandleErrors(ex);
                }

                // Allow the user to override the machine settings
                RegKey = Microsoft.Win32.Registry.CurrentUser;
                try
                {
                    RegKey = RegKey.OpenSubKey(DebtPlus.Configuration.AssemblyInfo.InstallationRegistryKey + @"\options", false);
                    if( RegKey != null )
                    {
                        strResult = Convert.ToString(RegKey.GetValue("show creditor addresses", strResult));
                    }
                }
                
                catch( System.IO.DirectoryNotFoundException ex )
                {
                    DebtPlus.Svc.Common.ErrorHandling.HandleErrors(ex);
                }
                
                catch( System.IO.FileNotFoundException ex )
                {
                    DebtPlus.Svc.Common.ErrorHandling.HandleErrors(ex);
                }

                try
                {
                    Result = Convert.ToBoolean(strResult);
                }
                
                catch( FormatException ex )
                {
                    DebtPlus.Svc.Common.ErrorHandling.HandleErrors(ex);
                }

                return Result;
            }

            set
            {
                Microsoft.Win32.RegistryKey RegKey;

                // Allow the user to override the machine settings
                RegKey = Microsoft.Win32.Registry.CurrentUser;
                try
                {
                    RegKey = RegKey.CreateSubKey(DebtPlus.Configuration.AssemblyInfo.InstallationRegistryKey + @"\options");
                    RegKey.SetValue("show creditor addresses", value ? bool.TrueString : bool.FalseString);
                }
                
                catch( System.IO.DirectoryNotFoundException ex )
                {
                    DebtPlus.Svc.Common.ErrorHandling.HandleErrors(ex);
                }
                
                finally
                {
                    if( RegKey != null )
                    {
                        RegKey.Close();
                    }
                }
            }
        }

        /// <summary>
        /// return the directory to the saved layout
        /// </summary>
        /// <returns></returns>
        protected virtual string XMLBasePath()
        {
            if( DesignMode )
            {
                return string.Empty;
            }
            string BasePath = string.Format("{0}{1}DebtPlus", Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData), System.IO.Path.DirectorySeparatorChar);
            return System.IO.Path.Combine(BasePath, "Creditor.Search");
        }

        /// <summary>
        /// Reload the layout of the grid control if needed
        /// </summary>
        protected void ReloadGridControlLayout()
        {
            // Find the base path to the saved file location
            string PathName = XMLBasePath();
            if( ! string.IsNullOrEmpty(PathName) )
            {
                try
                {
                    string FileName = System.IO.Path.Combine(PathName, string.Format("{0}.Grid.xml", Name));
                    if( System.IO.File.Exists(FileName) )
                    {
                        GridView1.RestoreLayoutFromXml(FileName);
                    }
                }
#pragma warning disable 168
                catch( System.IO.DirectoryNotFoundException ex ) { }
                catch( System.IO.FileNotFoundException ex ) { }
#pragma warning restore 168
            }
        }
    }
}