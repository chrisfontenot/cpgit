namespace DebtPlus.UI.Creditor.Widgets.Search
{
    partial class CreditorListForm
    {
        /// <summary>;
        /// Required designer variable.;
        /// </summary>;
        private System.ComponentModel.IContainer components = null;

        /// <summary>;
        /// Clean up any resources being used.;
        /// </summary>;
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>;
        protected override void Dispose(bool disposing)
        {
            try
            {
                CreditorListForm_Dispose(disposing);
                if (disposing && (components != null))
                {
                    components.Dispose();
                }
            }
            finally
            {
                base.Dispose(disposing);
            }
        }

        /// <summary>;
        /// Required method for Designer support - do not modify;
        /// the contents of this method with the code editor.;
        /// </summary>;
        private void InitializeComponent() {
            this.GridControl1 = new DevExpress.XtraGrid.GridControl();
            this.GridView1 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.col_creditor = new DevExpress.XtraGrid.Columns.GridColumn();
            this.col_name = new DevExpress.XtraGrid.Columns.GridColumn();
            this.col_comment = new DevExpress.XtraGrid.Columns.GridColumn();
            this.col_fairshare = new DevExpress.XtraGrid.Columns.GridColumn();
            this.col_address = new DevExpress.XtraGrid.Columns.GridColumn();
            this.col_creditor_id = new DevExpress.XtraGrid.Columns.GridColumn();
            this.col_sic = new DevExpress.XtraGrid.Columns.GridColumn();
            this.col_fairshare_eft = new DevExpress.XtraGrid.Columns.GridColumn();
            this.col_fairshare_check = new DevExpress.XtraGrid.Columns.GridColumn();
            this.Button_OK = new DevExpress.XtraEditors.SimpleButton();
            this.Button_Cancel = new DevExpress.XtraEditors.SimpleButton();
            this.Button_More = new DevExpress.XtraEditors.SimpleButton();
            this.Button_Print = new DevExpress.XtraEditors.SimpleButton();
            this.CheckEdit_Preview = new DevExpress.XtraEditors.CheckEdit();
            ((System.ComponentModel.ISupportInitialize)(this.DxErrorProvider1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.GridControl1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.GridView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.CheckEdit_Preview.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // GridControl1
            // 
            this.GridControl1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.GridControl1.Location = new System.Drawing.Point(8, 9);
            this.GridControl1.MainView = this.GridView1;
            this.GridControl1.Name = "GridControl1";
            this.GridControl1.Size = new System.Drawing.Size(352, 231);
            this.GridControl1.TabIndex = 0;
            this.GridControl1.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.GridView1});
            // 
            // GridView1
            // 
            this.GridView1.Appearance.ColumnFilterButton.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(104)))), ((int)(((byte)(184)))), ((int)(((byte)(251)))));
            this.GridView1.Appearance.ColumnFilterButton.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(104)))), ((int)(((byte)(184)))), ((int)(((byte)(251)))));
            this.GridView1.Appearance.ColumnFilterButton.ForeColor = System.Drawing.Color.White;
            this.GridView1.Appearance.ColumnFilterButton.Options.UseBackColor = true;
            this.GridView1.Appearance.ColumnFilterButton.Options.UseBorderColor = true;
            this.GridView1.Appearance.ColumnFilterButton.Options.UseForeColor = true;
            this.GridView1.Appearance.ColumnFilterButtonActive.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(170)))), ((int)(((byte)(216)))), ((int)(((byte)(254)))));
            this.GridView1.Appearance.ColumnFilterButtonActive.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(170)))), ((int)(((byte)(216)))), ((int)(((byte)(254)))));
            this.GridView1.Appearance.ColumnFilterButtonActive.ForeColor = System.Drawing.Color.Black;
            this.GridView1.Appearance.ColumnFilterButtonActive.Options.UseBackColor = true;
            this.GridView1.Appearance.ColumnFilterButtonActive.Options.UseBorderColor = true;
            this.GridView1.Appearance.ColumnFilterButtonActive.Options.UseForeColor = true;
            this.GridView1.Appearance.Empty.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(236)))), ((int)(((byte)(246)))), ((int)(((byte)(255)))));
            this.GridView1.Appearance.Empty.BackColor2 = System.Drawing.Color.White;
            this.GridView1.Appearance.Empty.Options.UseBackColor = true;
            this.GridView1.Appearance.EvenRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(247)))), ((int)(((byte)(251)))), ((int)(((byte)(255)))));
            this.GridView1.Appearance.EvenRow.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(247)))), ((int)(((byte)(251)))), ((int)(((byte)(255)))));
            this.GridView1.Appearance.EvenRow.ForeColor = System.Drawing.Color.Black;
            this.GridView1.Appearance.EvenRow.Options.UseBackColor = true;
            this.GridView1.Appearance.EvenRow.Options.UseBorderColor = true;
            this.GridView1.Appearance.EvenRow.Options.UseForeColor = true;
            this.GridView1.Appearance.FilterCloseButton.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(104)))), ((int)(((byte)(184)))), ((int)(((byte)(251)))));
            this.GridView1.Appearance.FilterCloseButton.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(104)))), ((int)(((byte)(184)))), ((int)(((byte)(251)))));
            this.GridView1.Appearance.FilterCloseButton.ForeColor = System.Drawing.Color.White;
            this.GridView1.Appearance.FilterCloseButton.Options.UseBackColor = true;
            this.GridView1.Appearance.FilterCloseButton.Options.UseBorderColor = true;
            this.GridView1.Appearance.FilterCloseButton.Options.UseForeColor = true;
            this.GridView1.Appearance.FilterPanel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(236)))), ((int)(((byte)(246)))), ((int)(((byte)(255)))));
            this.GridView1.Appearance.FilterPanel.BackColor2 = System.Drawing.Color.White;
            this.GridView1.Appearance.FilterPanel.ForeColor = System.Drawing.Color.Black;
            this.GridView1.Appearance.FilterPanel.Options.UseBackColor = true;
            this.GridView1.Appearance.FilterPanel.Options.UseForeColor = true;
            this.GridView1.Appearance.FixedLine.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(59)))), ((int)(((byte)(133)))), ((int)(((byte)(195)))));
            this.GridView1.Appearance.FixedLine.Options.UseBackColor = true;
            this.GridView1.Appearance.FocusedCell.BackColor = System.Drawing.Color.White;
            this.GridView1.Appearance.FocusedCell.ForeColor = System.Drawing.Color.Black;
            this.GridView1.Appearance.FocusedCell.Options.UseBackColor = true;
            this.GridView1.Appearance.FocusedCell.Options.UseForeColor = true;
            this.GridView1.Appearance.FocusedRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(38)))), ((int)(((byte)(109)))), ((int)(((byte)(189)))));
            this.GridView1.Appearance.FocusedRow.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(59)))), ((int)(((byte)(139)))), ((int)(((byte)(206)))));
            this.GridView1.Appearance.FocusedRow.ForeColor = System.Drawing.Color.White;
            this.GridView1.Appearance.FocusedRow.Options.UseBackColor = true;
            this.GridView1.Appearance.FocusedRow.Options.UseBorderColor = true;
            this.GridView1.Appearance.FocusedRow.Options.UseForeColor = true;
            this.GridView1.Appearance.FooterPanel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(104)))), ((int)(((byte)(184)))), ((int)(((byte)(251)))));
            this.GridView1.Appearance.FooterPanel.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(104)))), ((int)(((byte)(184)))), ((int)(((byte)(251)))));
            this.GridView1.Appearance.FooterPanel.ForeColor = System.Drawing.Color.Black;
            this.GridView1.Appearance.FooterPanel.Options.UseBackColor = true;
            this.GridView1.Appearance.FooterPanel.Options.UseBorderColor = true;
            this.GridView1.Appearance.FooterPanel.Options.UseForeColor = true;
            this.GridView1.Appearance.GroupButton.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(104)))), ((int)(((byte)(184)))), ((int)(((byte)(251)))));
            this.GridView1.Appearance.GroupButton.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(104)))), ((int)(((byte)(184)))), ((int)(((byte)(251)))));
            this.GridView1.Appearance.GroupButton.Options.UseBackColor = true;
            this.GridView1.Appearance.GroupButton.Options.UseBorderColor = true;
            this.GridView1.Appearance.GroupFooter.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(170)))), ((int)(((byte)(216)))), ((int)(((byte)(254)))));
            this.GridView1.Appearance.GroupFooter.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(170)))), ((int)(((byte)(216)))), ((int)(((byte)(254)))));
            this.GridView1.Appearance.GroupFooter.ForeColor = System.Drawing.Color.Black;
            this.GridView1.Appearance.GroupFooter.Options.UseBackColor = true;
            this.GridView1.Appearance.GroupFooter.Options.UseBorderColor = true;
            this.GridView1.Appearance.GroupFooter.Options.UseForeColor = true;
            this.GridView1.Appearance.GroupPanel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(236)))), ((int)(((byte)(246)))), ((int)(((byte)(255)))));
            this.GridView1.Appearance.GroupPanel.BackColor2 = System.Drawing.Color.White;
            this.GridView1.Appearance.GroupPanel.ForeColor = System.Drawing.Color.Black;
            this.GridView1.Appearance.GroupPanel.Options.UseBackColor = true;
            this.GridView1.Appearance.GroupPanel.Options.UseForeColor = true;
            this.GridView1.Appearance.GroupRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(170)))), ((int)(((byte)(216)))), ((int)(((byte)(254)))));
            this.GridView1.Appearance.GroupRow.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(170)))), ((int)(((byte)(216)))), ((int)(((byte)(254)))));
            this.GridView1.Appearance.GroupRow.ForeColor = System.Drawing.Color.Black;
            this.GridView1.Appearance.GroupRow.Options.UseBackColor = true;
            this.GridView1.Appearance.GroupRow.Options.UseBorderColor = true;
            this.GridView1.Appearance.GroupRow.Options.UseForeColor = true;
            this.GridView1.Appearance.HeaderPanel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(139)))), ((int)(((byte)(201)))), ((int)(((byte)(254)))));
            this.GridView1.Appearance.HeaderPanel.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(139)))), ((int)(((byte)(201)))), ((int)(((byte)(254)))));
            this.GridView1.Appearance.HeaderPanel.ForeColor = System.Drawing.Color.Black;
            this.GridView1.Appearance.HeaderPanel.Options.UseBackColor = true;
            this.GridView1.Appearance.HeaderPanel.Options.UseBorderColor = true;
            this.GridView1.Appearance.HeaderPanel.Options.UseForeColor = true;
            this.GridView1.Appearance.HideSelectionRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(105)))), ((int)(((byte)(170)))), ((int)(((byte)(225)))));
            this.GridView1.Appearance.HideSelectionRow.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(83)))), ((int)(((byte)(155)))), ((int)(((byte)(215)))));
            this.GridView1.Appearance.HideSelectionRow.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(236)))), ((int)(((byte)(246)))), ((int)(((byte)(255)))));
            this.GridView1.Appearance.HideSelectionRow.Options.UseBackColor = true;
            this.GridView1.Appearance.HideSelectionRow.Options.UseBorderColor = true;
            this.GridView1.Appearance.HideSelectionRow.Options.UseForeColor = true;
            this.GridView1.Appearance.HorzLine.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(104)))), ((int)(((byte)(184)))), ((int)(((byte)(251)))));
            this.GridView1.Appearance.HorzLine.Options.UseBackColor = true;
            this.GridView1.Appearance.OddRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(236)))), ((int)(((byte)(246)))), ((int)(((byte)(255)))));
            this.GridView1.Appearance.OddRow.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(236)))), ((int)(((byte)(246)))), ((int)(((byte)(255)))));
            this.GridView1.Appearance.OddRow.ForeColor = System.Drawing.Color.Black;
            this.GridView1.Appearance.OddRow.Options.UseBackColor = true;
            this.GridView1.Appearance.OddRow.Options.UseBorderColor = true;
            this.GridView1.Appearance.OddRow.Options.UseForeColor = true;
            this.GridView1.Appearance.Preview.Font = new System.Drawing.Font("Verdana", 7.5F);
            this.GridView1.Appearance.Preview.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(83)))), ((int)(((byte)(155)))), ((int)(((byte)(215)))));
            this.GridView1.Appearance.Preview.Options.UseFont = true;
            this.GridView1.Appearance.Preview.Options.UseForeColor = true;
            this.GridView1.Appearance.Row.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(247)))), ((int)(((byte)(251)))), ((int)(((byte)(255)))));
            this.GridView1.Appearance.Row.ForeColor = System.Drawing.Color.Black;
            this.GridView1.Appearance.Row.Options.UseBackColor = true;
            this.GridView1.Appearance.Row.Options.UseForeColor = true;
            this.GridView1.Appearance.RowSeparator.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(236)))), ((int)(((byte)(246)))), ((int)(((byte)(255)))));
            this.GridView1.Appearance.RowSeparator.BackColor2 = System.Drawing.Color.White;
            this.GridView1.Appearance.RowSeparator.Options.UseBackColor = true;
            this.GridView1.Appearance.SelectedRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(83)))), ((int)(((byte)(155)))), ((int)(((byte)(215)))));
            this.GridView1.Appearance.SelectedRow.ForeColor = System.Drawing.Color.White;
            this.GridView1.Appearance.SelectedRow.Options.UseBackColor = true;
            this.GridView1.Appearance.SelectedRow.Options.UseForeColor = true;
            this.GridView1.Appearance.TopNewRow.BackColor = System.Drawing.Color.White;
            this.GridView1.Appearance.TopNewRow.Options.UseBackColor = true;
            this.GridView1.Appearance.VertLine.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(104)))), ((int)(((byte)(184)))), ((int)(((byte)(251)))));
            this.GridView1.Appearance.VertLine.Options.UseBackColor = true;
            this.GridView1.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.col_creditor,
            this.col_name,
            this.col_comment,
            this.col_fairshare,
            this.col_address,
            this.col_creditor_id,
            this.col_sic,
            this.col_fairshare_eft,
            this.col_fairshare_check});
            this.GridView1.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus;
            this.GridView1.GridControl = this.GridControl1;
            this.GridView1.Name = "GridView1";
            this.GridView1.OptionsBehavior.Editable = false;
            this.GridView1.OptionsFilter.UseNewCustomFilterDialog = true;
            this.GridView1.OptionsPrint.ExpandAllGroups = false;
            this.GridView1.OptionsPrint.PrintFooter = false;
            this.GridView1.OptionsPrint.PrintGroupFooter = false;
            this.GridView1.OptionsPrint.PrintPreview = true;
            this.GridView1.OptionsPrint.UsePrintStyles = true;
            this.GridView1.OptionsSelection.UseIndicatorForSelection = false;
            this.GridView1.OptionsView.AutoCalcPreviewLineCount = true;
            this.GridView1.OptionsView.ShowFilterPanelMode = DevExpress.XtraGrid.Views.Base.ShowFilterPanelMode.Never;
            this.GridView1.OptionsView.ShowGroupPanel = false;
            this.GridView1.OptionsView.ShowIndicator = false;
            this.GridView1.ShowButtonMode = DevExpress.XtraGrid.Views.Base.ShowButtonModeEnum.ShowForFocusedRow;
            this.GridView1.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.col_creditor, DevExpress.Data.ColumnSortOrder.Ascending)});
            // 
            // col_creditor
            // 
            this.col_creditor.Caption = "Creditor";
            this.col_creditor.FieldName = "creditor";
            this.col_creditor.Name = "col_creditor";
            this.col_creditor.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.True;
            this.col_creditor.Visible = true;
            this.col_creditor.VisibleIndex = 0;
            this.col_creditor.Width = 68;
            // 
            // col_name
            // 
            this.col_name.Caption = "Name";
            this.col_name.FieldName = "name";
            this.col_name.Name = "col_name";
            this.col_name.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.True;
            this.col_name.Visible = true;
            this.col_name.VisibleIndex = 1;
            this.col_name.Width = 190;
            // 
            // col_comment
            // 
            this.col_comment.Caption = "Comment";
            this.col_comment.FieldName = "comment";
            this.col_comment.Name = "col_comment";
            this.col_comment.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.True;
            this.col_comment.Visible = true;
            this.col_comment.VisibleIndex = 2;
            this.col_comment.Width = 176;
            // 
            // col_fairshare
            // 
            this.col_fairshare.Caption = "Fairshare";
            this.col_fairshare.FieldName = "fairshare";
            this.col_fairshare.Name = "col_fairshare";
            this.col_fairshare.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.True;
            this.col_fairshare.Visible = true;
            this.col_fairshare.VisibleIndex = 3;
            this.col_fairshare.Width = 81;
            // 
            // col_address
            // 
            this.col_address.Caption = "Address";
            this.col_address.FieldName = "address";
            this.col_address.Name = "col_address";
            this.col_address.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.True;
            // 
            // col_creditor_id
            // 
            this.col_creditor_id.Caption = "CreditorID";
            this.col_creditor_id.DisplayFormat.FormatString = "f0";
            this.col_creditor_id.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.col_creditor_id.FieldName = "creditor_id";
            this.col_creditor_id.Name = "col_creditor_id";
            this.col_creditor_id.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.True;
            // 
            // col_sic
            // 
            this.col_sic.Caption = "S.I.C.";
            this.col_sic.FieldName = "sic";
            this.col_sic.Name = "col_sic";
            this.col_sic.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.True;
            // 
            // col_fairshare_eft
            // 
            this.col_fairshare_eft.AppearanceCell.Options.UseTextOptions = true;
            this.col_fairshare_eft.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.col_fairshare_eft.AppearanceHeader.Options.UseTextOptions = true;
            this.col_fairshare_eft.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.col_fairshare_eft.Caption = "EFT Fairshare %";
            this.col_fairshare_eft.DisplayFormat.FormatString = "p2";
            this.col_fairshare_eft.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.col_fairshare_eft.FieldName = "fairshare_pct_eft";
            this.col_fairshare_eft.GroupFormat.FormatString = "p2";
            this.col_fairshare_eft.GroupFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.col_fairshare_eft.Name = "col_fairshare_eft";
            this.col_fairshare_eft.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.True;
            // 
            // col_fairshare_check
            // 
            this.col_fairshare_check.AppearanceCell.Options.UseTextOptions = true;
            this.col_fairshare_check.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.col_fairshare_check.AppearanceHeader.Options.UseTextOptions = true;
            this.col_fairshare_check.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.col_fairshare_check.Caption = "Chk Fairshare %";
            this.col_fairshare_check.DisplayFormat.FormatString = "p2";
            this.col_fairshare_check.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.col_fairshare_check.FieldName = "fairshare_pct_check";
            this.col_fairshare_check.GroupFormat.FormatString = "p2";
            this.col_fairshare_check.GroupFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.col_fairshare_check.Name = "col_fairshare_check";
            this.col_fairshare_check.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.True;
            // 
            // Button_OK
            // 
            this.Button_OK.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.Button_OK.Location = new System.Drawing.Point(376, 17);
            this.Button_OK.Name = "Button_OK";
            this.Button_OK.Size = new System.Drawing.Size(75, 25);
            this.Button_OK.TabIndex = 1;
            this.Button_OK.Text = "&OK";
            this.Button_OK.ToolTip = "Click here to accept the creditor.";
            this.Button_OK.ToolTipController = this.ToolTipController1;
            // 
            // Button_Cancel
            // 
            this.Button_Cancel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.Button_Cancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.Button_Cancel.Location = new System.Drawing.Point(376, 60);
            this.Button_Cancel.Name = "Button_Cancel";
            this.Button_Cancel.Size = new System.Drawing.Size(75, 25);
            this.Button_Cancel.TabIndex = 2;
            this.Button_Cancel.Text = "&Cancel";
            this.Button_Cancel.ToolTip = "Click here to cancel this form and return to the previous imput form.";
            this.Button_Cancel.ToolTipController = this.ToolTipController1;
            // 
            // Button_More
            // 
            this.Button_More.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.Button_More.Location = new System.Drawing.Point(376, 103);
            this.Button_More.Name = "Button_More";
            this.Button_More.Size = new System.Drawing.Size(75, 25);
            this.Button_More.TabIndex = 3;
            this.Button_More.Text = "&More";
            this.Button_More.ToolTip = "There are more creditors. Click here to see the next block.";
            this.Button_More.ToolTipController = this.ToolTipController1;
            // 
            // Button_Print
            // 
            this.Button_Print.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.Button_Print.Location = new System.Drawing.Point(376, 240);
            this.Button_Print.Name = "Button_Print";
            this.Button_Print.Size = new System.Drawing.Size(75, 25);
            this.Button_Print.TabIndex = 4;
            this.Button_Print.Text = "&Print...";
            this.Button_Print.ToolTip = "Print the list on the printer";
            this.Button_Print.ToolTipController = this.ToolTipController1;
            // 
            // CheckEdit_Preview
            // 
            this.CheckEdit_Preview.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.CheckEdit_Preview.Location = new System.Drawing.Point(8, 248);
            this.CheckEdit_Preview.Name = "CheckEdit_Preview";
            this.CheckEdit_Preview.Properties.Caption = "Include creditor addresses";
            this.CheckEdit_Preview.Size = new System.Drawing.Size(352, 19);
            this.CheckEdit_Preview.TabIndex = 5;
            this.CheckEdit_Preview.ToolTip = "Do you wish to include the creditor addresses in the list? if( so, check this box" +
    ".";
            this.CheckEdit_Preview.ToolTipController = this.ToolTipController1;
            // 
            // CreditorListForm
            // 
            this.AcceptButton = this.Button_OK;
            this.CancelButton = this.Button_Cancel;
            this.ClientSize = new System.Drawing.Size(464, 273);
            this.Controls.Add(this.CheckEdit_Preview);
            this.Controls.Add(this.Button_Print);
            this.Controls.Add(this.Button_More);
            this.Controls.Add(this.Button_Cancel);
            this.Controls.Add(this.Button_OK);
            this.Controls.Add(this.GridControl1);
            this.Name = "CreditorListForm";
            this.Text = "Creditors Found";
            ((System.ComponentModel.ISupportInitialize)(this.DxErrorProvider1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.GridControl1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.GridView1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.CheckEdit_Preview.Properties)).EndInit();
            this.ResumeLayout(false);

        }
    internal DevExpress.XtraGrid.GridControl GridControl1;
    internal DevExpress.XtraGrid.Views.Grid.GridView GridView1;
    internal DevExpress.XtraEditors.SimpleButton Button_OK;
    internal DevExpress.XtraEditors.SimpleButton Button_Cancel;
    internal DevExpress.XtraEditors.SimpleButton Button_More;
    internal DevExpress.XtraGrid.Columns.GridColumn col_name;
    internal DevExpress.XtraGrid.Columns.GridColumn col_comment;
    internal DevExpress.XtraGrid.Columns.GridColumn col_fairshare;
    internal DevExpress.XtraGrid.Columns.GridColumn col_address;
    internal DevExpress.XtraGrid.Columns.GridColumn col_sic;
    internal DevExpress.XtraGrid.Columns.GridColumn col_fairshare_eft;
    internal DevExpress.XtraGrid.Columns.GridColumn col_fairshare_check;
    internal DevExpress.XtraGrid.Columns.GridColumn col_creditor;
    internal DevExpress.XtraGrid.Columns.GridColumn col_creditor_id;
    internal DevExpress.XtraEditors.SimpleButton Button_Print;
    internal DevExpress.XtraEditors.CheckEdit CheckEdit_Preview;
    }
}