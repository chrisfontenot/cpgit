using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using DebtPlus;
using System.Data.SqlClient;

namespace DebtPlus.UI.Creditor.Widgets.Search
{
    public partial class CreditorSearchClass : DebtPlus.Data.Forms.DebtPlusForm, DebtPlus.Interfaces.Creditor.ICreditorSearch
    {
        /// <summary>
        /// Dispose of the class when it is desired
        /// </summary>
        /// <param name="disposing"></param>
        private void CreditorSearchClass_Dispose(bool disposing)
        {
            // Remove the event handlers
            if (disposing && !DesignMode)
            {
                UnRegisterHandlers();
            }

            // Remove the pointers to the larger structures
            dsRows = null;
        }

        // Category values
        private const string Category_Creditor = "creditor";
        private const string Category_Alias = "name_or_alias";
        private const string Category_Name = "name";
        private const string Category_AccountNumber = "account_number";
        private const string Category_Prefix = "prefix";
        private const string Category_SIC = "sic";
        private const string Category_CheckNumber = "checknum";
        private const string Category_InvoiceNumber = "invoice_number";
        private const string Category_Telephone = "telephone";
        private const string Category_Address = "creditor_address";
        private const string Category_Payment = "rpps_payment";
        private const string Category_Proposal = "rpps_proposal";

        // Values used for the relation (2nd input field)
        private const string relation_EQ = "=";
        private const string relation_GE = ">=";
        private const string relation_GT = ">";
        private const string relation_LT = "<";
        private const string relation_LE = "<=";
        private const string relation_NE = "not =";
        private const string relation_between = "between";
        private const string relation_not_between = "not between";
        private const string relation_contains = "contains";
        private const string relation_not_contains = "not contain";
        private const string relation_ends = "ends";
        private const string relation_starts = "starts";
        private const string relation_not_starts = "not starts";

        // MRU keys
        private const string MRU_Creditor = "Creditors";
        private const string MRU_Alias = "Creditor Alias";
        private const string MRU_Name = "Creditor Name";
        private const string MRU_AccountNumber = "Creditor Account No";
        private const string MRU_Prefix = "Creditor Prefix";
        private const string MRU_SIC = "Creditor SIC";
        private const string MRU_CheckNumber = "Creditor Check No";
        private const string MRU_InvoiceNumber = "Creditor Invoice No";
        private const string MRU_Telephone = "Creditor Telephone";
        private const string MRU_Address = "Creditor Address";
        private const string MRU_Payment = "Creditor RPPS Payment";
        private const string MRU_Proposal = "Creditor RPPS Proposal";

        /// <summary>
        /// Event to validate the creditor selection
        /// </summary>
        protected void RaiseValidating(System.ComponentModel.CancelEventArgs e)
        {
            OnValidating(e);
        }

        /// <summary>
        /// Create a new instance of our class
        /// </summary>
        /// <param name="DatabaseInfo">Pointer to the database connection object</param>
        public CreditorSearchClass() : base()
        {
            InitializeComponent();
            if (!DesignMode)
            {
                RegisterHandlers();

                // Ensure that our window is not hidden.
                Form frm = Form.ActiveForm;
                if( frm != null && ! frm.InvokeRequired && frm.TopMost )
                {
                    TopMost = true;
                }
            }
        }

        /// <summary>
        /// Register the event handlers
        /// </summary>
        private void RegisterHandlers()
        {
            // Form Load event
            this.Load += CreditorSearchClass_Load;

            // OK button click event
            Button_OK.Click += Button_OK_Click;

            // Handle the changes in the relations
            Combo_Category.SelectedIndexChanged += Combo_Category_SelectedIndexChanged;
            Combo_Relation.SelectedIndexChanged += Combo_Relation_SelectedIndexChanged;

            // Select and trim the text fields
            Combo_Value1.Enter += Combo_Value1_Enter;
            Combo_Value2.Enter += Combo_Value1_Enter;
            Combo_Value1.Leave += Combo_Value1_Leave;
            Combo_Value2.Leave += Combo_Value1_Leave;

            // Form changed events.
            Combo_Category.SelectedValueChanged += Form_Changed;
            Combo_Relation.SelectedValueChanged += Form_Changed;
            Combo_Value1.SelectedIndexChanged += Form_Changed;
            Combo_Value1.TextChanged += Form_Changed;
            Combo_Value2.TextChanged += Form_Changed;
        }

        /// <summary>
        /// Remove the event registration
        /// </summary>
        private void UnRegisterHandlers()
        {
            // Form Load event
            this.Load -= CreditorSearchClass_Load;

            // OK button click event
            Button_OK.Click -= Button_OK_Click;

            // Handle the changes in the relations
            Combo_Category.SelectedIndexChanged -= Combo_Category_SelectedIndexChanged;
            Combo_Relation.SelectedIndexChanged -= Combo_Relation_SelectedIndexChanged;

            // Select and trim the text fields
            Combo_Value1.Enter -= Combo_Value1_Enter;
            Combo_Value2.Enter -= Combo_Value1_Enter;
            Combo_Value1.Leave -= Combo_Value1_Leave;
            Combo_Value2.Leave -= Combo_Value1_Leave;

            // Form changed events.
            Combo_Category.SelectedValueChanged -= Form_Changed;
            Combo_Relation.SelectedValueChanged -= Form_Changed;
            Combo_Value1.SelectedIndexChanged -= Form_Changed;
            Combo_Value1.TextChanged -= Form_Changed;
            Combo_Value2.TextChanged -= Form_Changed;
        }

        // Information about the Creditor search
        private string lastCreditor;            // Last Creditor in the selection list. Used for the "MORE" function as a starting point.
        private string privateCreditor;         // Creditor ID found in the search
        private Int32 rowLimit;                 // Maximum to display in found results screen
        private DataSet dsRows;                 // Dataset for the resulting items

        /// <summary>
        /// The resulting creditor ID from the search operation. Valid only when
        /// the search is successful.
        /// </summary>
        public string Creditor
        {  
            get
            {
                return privateCreditor;
            }
            set
            {
                throw new NotImplementedException("creditor search does not allow setting the creditor ID");
            }
        }

        /// <summary>
        /// Attempt to complete the dialog with the creditor
        /// </summary>
        /// <param name="CreditorID">Creditor ID found</param>
        /// <returns>TRUE if dialog is complete. FALSE if failure.</returns>
        private bool SetCreditor(string CreditorID)
        {
            // Set the creditor found
            privateCreditor = CreditorID;

            // Determine if the creditor is valid. Stop if invalid.
            System.ComponentModel.CancelEventArgs e = new System.ComponentModel.CancelEventArgs(false);
            RaiseValidating(e);
            if (e.Cancel)
            {
                return false;
            }

            // Save the creditor for the next time
            Save_Recent(CreditorID);
            DefaultCategory = CategoryLabel;
            DefaultRelation = RelationLabel;

            // Complete the dialog normally
            DialogResult = System.Windows.Forms.DialogResult.OK;
            return true;
        }

#region Combobox Values
        /// <summary>
        /// Which item is selected for the category
        /// </summary>
        private string CategoryLabel
        {
            get
            {
                string Answer = string.Empty;
                if( Combo_Category.SelectedIndex >= 0 )
                {
                    Answer = Convert.ToString(((DebtPlus.Data.Controls.ComboboxItem) Combo_Category.SelectedItem).value);
                }
                return Answer;
            }
        }

        /// <summary>
        /// Which item is selected for the relation
        /// </summary>
        private string RelationLabel
        {
            get
            {
                string Answer = string.Empty;
                if( Combo_Relation.SelectedIndex >= 0 )
                {
                    Answer = Convert.ToString(((DebtPlus.Data.Controls.ComboboxItem) Combo_Relation.SelectedItem).value);
                }
                return Answer;
            }
        }
#endregion

        /// <summary>
        /// Translate the category selection to an MRU key
        /// </summary>
        private string GetMRUKey
        {
            get
            {
                switch( CategoryLabel )
                {
                    case Category_AccountNumber:
                        return MRU_AccountNumber;

                    case Category_Address:
                        return MRU_Address;
                    
                    case Category_Alias:
                        return MRU_Alias;
                    
                    case Category_CheckNumber:
                        return MRU_CheckNumber;
                    
                    case Category_Creditor:
                        return MRU_Creditor;
                    
                    case Category_InvoiceNumber:
                        return MRU_InvoiceNumber;
                    
                    case Category_Name:
                        return MRU_Name;
                    
                    case Category_Payment:
                        return MRU_Payment;
                    
                    case Category_Prefix:
                        return MRU_Prefix;
                    
                    case Category_Proposal:
                        return MRU_Proposal;
                    
                    case Category_SIC:
                        return MRU_SIC;
                    
                    case Category_Telephone:
                        return MRU_Telephone;
                    
                    default:
                        throw new ArgumentOutOfRangeException("CategoryLabel");
                }
            }
        }

#region Form Navigation
        /// <summary>
        /// Load the category combo box with the proper values
        /// </summary>
        private void Load_Category()
	    {
            Combo_Category.Properties.Items.Clear();

            // Add the item to the list. This is the default value.
            Combo_Category.Properties.Items.Add(new DebtPlus.Data.Controls.ComboboxItem("Creditor ID", Category_Creditor));

            // Add the other values to the list
            Combo_Category.Properties.Items.Add(new DebtPlus.Data.Controls.ComboboxItem("Creditor Name or Alias", Category_Alias));
            Combo_Category.Properties.Items.Add(new DebtPlus.Data.Controls.ComboboxItem("Creditor Name", Category_Name));
            Combo_Category.Properties.Items.Add(new DebtPlus.Data.Controls.ComboboxItem("Account Number", Category_AccountNumber));
            Combo_Category.Properties.Items.Add(new DebtPlus.Data.Controls.ComboboxItem("Account Prefix", Category_Prefix));
            Combo_Category.Properties.Items.Add(new DebtPlus.Data.Controls.ComboboxItem("Creditor SIC Code", Category_SIC));
            Combo_Category.Properties.Items.Add(new DebtPlus.Data.Controls.ComboboxItem("Check Number", Category_CheckNumber));
            Combo_Category.Properties.Items.Add(new DebtPlus.Data.Controls.ComboboxItem("Invoice Number", Category_InvoiceNumber));
            Combo_Category.Properties.Items.Add(new DebtPlus.Data.Controls.ComboboxItem("Creditor Telephone Number", Category_Telephone));
            Combo_Category.Properties.Items.Add(new DebtPlus.Data.Controls.ComboboxItem("Creditor Address", Category_Address));
            Combo_Category.Properties.Items.Add(new DebtPlus.Data.Controls.ComboboxItem("RPPS Payment Biller ID", Category_Payment));
            Combo_Category.Properties.Items.Add(new DebtPlus.Data.Controls.ComboboxItem("RPPS Proposal Biller ID", Category_Proposal));

            // Set the default item to the last displayed category
            string DefaultItem = DefaultCategory;
            foreach( DebtPlus.Data.Controls.ComboboxItem item in Combo_Category.Properties.Items )
            {
                if( string.Compare(Convert.ToString(item.value), DefaultItem, true) == 0 )
                {
                    Combo_Category.SelectedItem = item;
                    break;
                }
            }

            // Choose the first item in the list if there is no default item
            if( Combo_Category.SelectedItem == null )
            {
                Combo_Category.SelectedIndex = 0;
            }
        }

        /// <summary>
        /// Load the relation combo box with the proper values
        /// </summary>
        private void Load_Relation()
	    {
            Combo_Relation.Properties.Items.Clear();
            switch( CategoryLabel )
            {
                case Category_Creditor:
                    Combo_Relation.SelectedIndex = Combo_Relation.Properties.Items.Add(new DebtPlus.Data.Controls.ComboboxItem(relation_EQ, relation_EQ));
                    Combo_Relation.Properties.Items.Add(new DebtPlus.Data.Controls.ComboboxItem(relation_NE, relation_NE));
                    Combo_Relation.Properties.Items.Add(new DebtPlus.Data.Controls.ComboboxItem("less than", relation_LT));
                    Combo_Relation.Properties.Items.Add(new DebtPlus.Data.Controls.ComboboxItem("is atleast", relation_GE));
                    Combo_Relation.Properties.Items.Add(new DebtPlus.Data.Controls.ComboboxItem("is greater than", relation_GT));
                    Combo_Relation.Properties.Items.Add(new DebtPlus.Data.Controls.ComboboxItem("not more than", relation_LE));
                    Combo_Relation.Properties.Items.Add(new DebtPlus.Data.Controls.ComboboxItem(relation_between, relation_between));
                    Combo_Relation.Properties.Items.Add(new DebtPlus.Data.Controls.ComboboxItem(relation_not_between, relation_not_between));
                    break;

                case Category_Alias:
                case Category_Name:
                case Category_InvoiceNumber:
                case Category_CheckNumber:
                    Combo_Relation.SelectedIndex = Combo_Relation.Properties.Items.Add(new DebtPlus.Data.Controls.ComboboxItem(relation_EQ, relation_EQ));
                    Combo_Relation.Properties.Items.Add(new DebtPlus.Data.Controls.ComboboxItem(relation_NE, relation_NE));
                    Combo_Relation.Properties.Items.Add(new DebtPlus.Data.Controls.ComboboxItem("starts with", relation_starts));
                    Combo_Relation.Properties.Items.Add(new DebtPlus.Data.Controls.ComboboxItem("ends with", relation_ends));
                    Combo_Relation.Properties.Items.Add(new DebtPlus.Data.Controls.ComboboxItem("doesn't start with", relation_not_starts));
                    Combo_Relation.Properties.Items.Add(new DebtPlus.Data.Controls.ComboboxItem(relation_contains, relation_contains));
                    Combo_Relation.Properties.Items.Add(new DebtPlus.Data.Controls.ComboboxItem("doesn't contain", relation_not_contains));
                    break;

                case Category_Prefix:
                case Category_AccountNumber:
                case Category_SIC:
                case Category_Telephone:
                case Category_Address:
                case Category_Payment:
                case Category_Proposal:
                    Combo_Relation.SelectedIndex = Combo_Relation.Properties.Items.Add(new DebtPlus.Data.Controls.ComboboxItem(relation_EQ, relation_EQ));
                    Combo_Relation.Properties.Items.Add(new DebtPlus.Data.Controls.ComboboxItem(relation_NE, relation_NE));
                    Combo_Relation.Properties.Items.Add(new DebtPlus.Data.Controls.ComboboxItem("less than", relation_LT));
                    Combo_Relation.Properties.Items.Add(new DebtPlus.Data.Controls.ComboboxItem("is atleast", relation_GE));
                    Combo_Relation.Properties.Items.Add(new DebtPlus.Data.Controls.ComboboxItem("is greater than", relation_GT));
                    Combo_Relation.Properties.Items.Add(new DebtPlus.Data.Controls.ComboboxItem("not more than", relation_LE));
                    Combo_Relation.Properties.Items.Add(new DebtPlus.Data.Controls.ComboboxItem(relation_between, relation_between));
                    Combo_Relation.Properties.Items.Add(new DebtPlus.Data.Controls.ComboboxItem(relation_not_between, relation_not_between));
                    Combo_Relation.Properties.Items.Add(new DebtPlus.Data.Controls.ComboboxItem("starts with", relation_starts));
                    Combo_Relation.Properties.Items.Add(new DebtPlus.Data.Controls.ComboboxItem("ends with", relation_ends));
                    Combo_Relation.Properties.Items.Add(new DebtPlus.Data.Controls.ComboboxItem("doesn't start with", relation_not_starts));
                    Combo_Relation.Properties.Items.Add(new DebtPlus.Data.Controls.ComboboxItem(relation_contains, relation_contains));
                    Combo_Relation.Properties.Items.Add(new DebtPlus.Data.Controls.ComboboxItem("doesn't contain", relation_not_contains));
                    break;

                default:
                    throw new ArgumentOutOfRangeException("CategoryLabel");
            }

            // Set the default item to the last displayed category
            string DefaultItem = DefaultRelation;
            foreach (DebtPlus.Data.Controls.ComboboxItem item in Combo_Relation.Properties.Items)
            {
                if( string.Compare(Convert.ToString(item.value), DefaultItem, true) == 0 )
                {
                    Combo_Relation.SelectedItem = item;
                    break;
                }
            }

            // Choose the first item in the list if there is no default item
            if( Combo_Relation.SelectedItem == null )
            {
                Combo_Relation.SelectedIndex = 0;
            }

            // Load the combo lists
            Load_Value_1();
            Load_Value_2();

            // Hide the second value box until it is selected by the relation
            label_and.Visible = false;
            Combo_Value2.Visible = false;
        }

        /// <summary>
        /// The starting selection criteria is stored in the registry
        /// </summary>
        private string DefaultCategory
	    {
            get
            {
                string Answer = string.Empty;
                string Path = System.IO.Path.Combine(DebtPlus.Configuration.AssemblyInfo.InstallationRegistryKey, "history");
                Microsoft.Win32.RegistryKey reg = Microsoft.Win32.Registry.CurrentUser;
                try
                {
                    reg = reg.OpenSubKey(Path, false);
                    if( reg != null )
                    {
                        Answer = Convert.ToString(reg.GetValue("creditor search method"));
                    }
			    }
                catch { }
                finally
                {
                    if( reg != null )
                    {
                        reg.Close();
                    }
                }

                // Default the answer
                if( string.IsNullOrEmpty(Answer) )
                {
                    Answer = Category_Creditor;
                }
                return Answer;
            }

            set
            {
                string Path = System.IO.Path.Combine(DebtPlus.Configuration.AssemblyInfo.InstallationRegistryKey, "history");
                Microsoft.Win32.RegistryKey reg = Microsoft.Win32.Registry.CurrentUser.CreateSubKey(Path);
                if( reg != null )
                {
                    reg.SetValue("creditor search method", value.ToLower(), Microsoft.Win32.RegistryValueKind.String);
                    reg.Close();
                }
            }
        }

        /// <summary>
        /// The starting relation for the form is stored in the registry
        /// </summary>
        private string DefaultRelation
	    {
            get
            {
                string Answer = string.Empty;
                Microsoft.Win32.RegistryKey reg = Microsoft.Win32.Registry.CurrentUser;
                try
                {
                    string Path = System.IO.Path.Combine(DebtPlus.Configuration.AssemblyInfo.InstallationRegistryKey, "history");
                    reg = reg.OpenSubKey(Path, false);
                    if( reg != null )
                    {
                        Answer = Convert.ToString(reg.GetValue("creditor search relation"));
                    }
                }
                catch { }
                finally
                {
                    if( reg != null )
                    {
                        reg.Close();
                    }
                }

                // Default the answer
                if( string.IsNullOrEmpty(Answer) )
                {
                    Answer = relation_EQ;
                }
                return Answer;
            }

            set
            {
                Microsoft.Win32.RegistryKey reg = Microsoft.Win32.Registry.CurrentUser;
                try
                {
                    string Path = System.IO.Path.Combine(DebtPlus.Configuration.AssemblyInfo.InstallationRegistryKey, "history");
                    reg = reg.OpenSubKey(Path, true);
                    if( reg != null )
                    {
                        reg.SetValue("creditor search relation", value.ToLower(), Microsoft.Win32.RegistryValueKind.String);
                    }
                }
                catch { }
                finally
                {
                    if( reg != null )
                    {
                        reg.Close();
                    }
                }
            }
        }

        /// <summary>
        /// Load the list of valid items into the first input field
        /// </summary>
        private void Load_Value_1()
	    {
            // Load the list of previous Creditors. Select the first one.
            string SavedText = Combo_Value1.Text;
            Combo_Value1.SelectedIndex = -1;
            Combo_Value1.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            Combo_Value1.Properties.ReadOnly = false;
            Combo_Value1.Properties.Items.Clear();

            if( CategoryLabel == Category_Creditor )
            {
                // Do special processing on the tags for the Creditor
                using (var mruList = new DebtPlus.Data.MRU(MRU_Creditor, "Recent Creditors"))
                {
                    string[] lst = mruList.GetList;
                    foreach(string item in lst )
                    {
                        Combo_Value1.Properties.Items.Add(new DevExpress.XtraEditors.Controls.ComboBoxItem(item));
                    }
                }
            }
            else
            {
                // Otherwise, load the standard list of items from the MRU list
                using (var mruList = new DebtPlus.Data.MRU(GetMRUKey))
                {
                    string[] lst = mruList.GetList;
                    foreach(string item in lst )
                    {
                        Combo_Value1.Properties.Items.Add(new DevExpress.XtraEditors.Controls.ComboBoxItem(item));
                    }
                }
            }

            // Restore the text for the item
            Combo_Value1.SelectedIndex = -1;
            Combo_Value1.Text = SavedText;
        }

        /// <summary>
        /// Load the list of valid items into the second input field
        /// </summary>
        private void Load_Value_2()
	    {
            // Don't do anything here. We want to preserve the text field.
        }

        /// <summary>
        /// Process the LOAD event for the FORM
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void CreditorSearchClass_Load(object sender, EventArgs e)
	    {
            UnRegisterHandlers();

            // Save the maximum number for display since it is a static value.
            rowLimit = SelectLimitCount;

            try
            {
                // Load the comboboxes
                Load_Category();                    // Load the category information
                Load_Relation();                    // Load the relation list
                EnableOK();                         // Enable/Disable the OK button
            }
            finally
            {
                RegisterHandlers();
            }
        }

        /// <summary>
        /// Process the change of the category
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        /// <remarks></remarks>
        private void Combo_Category_SelectedIndexChanged(object sender, EventArgs e)
        {
            Load_Relation();
            EnableOK();
        }

        /// <summary>
        /// When the relationship control changes, enable or disable the second input field.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Combo_Relation_SelectedIndexChanged(object sender, EventArgs e)
	    {
            // Enable the second input field if needed
            bool ShouldEnable = IsValue2Enabled();
            label_and.Visible = ShouldEnable;
            Combo_Value2.Visible = ShouldEnable;

            // Load the values based upon the new relation
            Load_Value_1();

            // Check for the OK button being valid now.
            EnableOK();
        }

        /// <summary>
        /// Should the second input field be displayed?
        /// </summary>
        /// <returns></returns>
        private bool IsValue2Enabled()
	    {
            // If the item is relation_between or relation_not_between then enable the second value
            switch( RelationLabel )
		    {
                case relation_between:
                case relation_not_between:
                    return true;

                default:
                    return false;
            }
        }
#endregion

#region Form Validation
        /// <summary>
        /// Enable or disable the OK button as needed
        /// </summary>
        private void EnableOK()
	    {
            // Enable or disable the checkbox based upon the criteria
            if( CategoryLabel == Category_Creditor && CategoryLabel == relation_EQ )
		    {
                Check_InactiveCreditors.Enabled = false;
            }
		    else
		    {
                Check_InactiveCreditors.Enabled = true;
            }

            // Enable the OK button based upon the error condition on the form
            Button_OK.Enabled = ! HasErrors();
        }

        /// <summary>
        /// Determine if there are errors in the input fields.
        /// </summary>
        /// <returns>TRUE if there is an error. FALSE if the form is valid.</returns>
        private bool HasErrors()
        {
            string RelationSelected = RelationLabel;
            switch( CategoryLabel )
            {
                case Category_Creditor:
                    if( ! valid_creditor(Combo_Value1.Text) )
			        {
				        return true;
			        }
                    if( IsValue2Enabled() && ! valid_creditor(Combo_Value2.Text) )
                    {
                        return true;
                    }
                    break;

                case Category_Telephone:
                    if( ! valid_phone(Combo_Value1.Text) )
                    {
                        return true;
                    }
                    if( IsValue2Enabled() && ! valid_phone(Combo_Value2.Text) )
                    {
                        return true;
                    }
                    break;

                case Category_Payment:
                case Category_Proposal:
                    if( ! Valid_BillerID(Combo_Value1.Text) )
                    {
                        return true;
                    }

                    if( IsValue2Enabled() && ! Valid_BillerID(Combo_Value2.Text) )
                    {
                        return true;
                    }
                    break;

                default:
                    if( ! valid_string(Combo_Value1.Text) )
                    {
                        return true;
                    }
                    if( IsValue2Enabled() && ! valid_string(Combo_Value2.Text) )
                    {
                        return true;
                    }
                    break;
                }

            return false;
        }

        /// <summary>
        /// Look for a valid creitor ID or label
        /// </summary>
        /// <param name="Inputstring"></param>
        /// <returns></returns>
        private bool valid_creditor(string Inputstring)
	    {
            // The creditor ID may be a number. Accept valid non-negative numbers
            Int32 Result;
            if( Int32.TryParse(Inputstring, System.Globalization.NumberStyles.Integer, System.Globalization.CultureInfo.InvariantCulture, out Result))
            {
                return ( Result >= 0 );
            }

            // Use the name if one is referenced
            return valid_creditor_label(Inputstring);
        }

        /// <summary>
        /// Look for a valid creditor label.
        /// </summary>
        /// <param name="Inputstring"></param>
        /// <returns></returns>
        private bool valid_creditor_label(string Inputstring)
	    {
            System.Text.RegularExpressions.Regex rx = new System.Text.RegularExpressions.Regex("^[A-Za-z]{1,2}[0-9][0-9][0-9][0-9]+$");
            return rx.IsMatch(Inputstring);
        }

        /// <summary>
        /// Look for a valid telephone number
        /// </summary>
        /// <param name="Inputstring"></param>
        /// <returns></returns>
        private bool valid_phone(string Inputstring)
	    {
            switch( RelationLabel )
		    {
                case relation_contains:
                case relation_not_contains:
                case relation_starts:
                case relation_not_starts:
                case relation_ends:
                    return valid_string(Inputstring);    // Portion only. Do not validate the whole string.

                default:
                    return valid_string(Inputstring);    // Validate the whole telephone number
            }
        }

        /// <summary>
        /// Look for a valid text string
        /// </summary>
        /// <param name="Inputstring"></param>
        /// <returns></returns>
        private bool valid_string(string Inputstring)
	    {
            return Inputstring.Trim().Length > 0;
        }

        /// <summary>
        /// Look for a valid RPPS biller ID
        /// </summary>
        /// <param name="Inputstring"></param>
        /// <returns></returns>
        private bool Valid_BillerID(string Inputstring)
	    {
            string strValidate = "^[0-9]{10}$";
            bool Answer;

            // Validate the biller ID. Find the regular expression to validate it.
            Microsoft.Win32.RegistryKey RegKey = Microsoft.Win32.Registry.LocalMachine;
            try
		    {
                RegKey = RegKey.OpenSubKey(DebtPlus.Configuration.AssemblyInfo.InstallationRegistryKey + @"\Validation");
                if( RegKey != null )
			    {
				    strValidate = Convert.ToString(RegKey.GetValue("rpps_biller_ids.rpps_biller_id", strValidate));
			    }
            }
		    catch( Exception ex )
		    {
                DebtPlus.Svc.Common.ErrorHandling.HandleErrors(ex); // do nothing on errors
		    }
		    finally
		    {
                if( RegKey != null )
			    {
				    RegKey.Close();
			    }
            }

            if( strValidate != string.Empty )
		    {
                System.Text.RegularExpressions.Regex rx = new System.Text.RegularExpressions.Regex( strValidate );
                Answer = rx.IsMatch(Inputstring);
            }
		    else
		    {
                Answer = true;
            }
            return Answer;
        }
#endregion

#region Combo_Value
        /// <summary>
        /// Select the string when the control enters
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Combo_Value1_Enter(object sender, EventArgs e)
	    {
            ((DevExpress.XtraEditors.ComboBoxEdit) sender).SelectAll();
        }

        /// <summary>
        /// Trim the string when the control is left
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Combo_Value1_Leave(object sender, EventArgs e)
        {
            DevExpress.XtraEditors.ComboBoxEdit ctl = (DevExpress.XtraEditors.ComboBoxEdit)sender;
            ctl.Text = ctl.Text.Trim();
        }

        /// <summary>
        /// Handle the change in the form data. Control the OK button enabling.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Form_Changed(object sender, EventArgs e)
	    {
            EnableOK();
        }
#endregion

#region BUILD selection statement
        /// <summary>
        /// Create the valid constant for the search operation
        /// </summary>
        /// <param name="ctl">Control to retrieve the string</param>
        /// <returns></returns>
        private string Item_Value(string ctl)
        {
            return Item_Value(ctl, string.Empty, string.Empty);
        }

        /// <summary>
        /// Create the valid constant for the search operation
        /// </summary>
        /// <param name="ctl">Control to retrieve the string</param>
        /// <param name="strPrefix">Leading characters for the character mask</param>
        /// <param name="strSuffix">Trailing characters for the character mask</param>
        /// <returns></returns>
        private string Item_Value(string ctl, string strPrefix, string strSuffix)
	    {
            switch( CategoryLabel )
		    {
                case Category_Telephone:
                    return string.Format("'{0}{1}{2}'", strPrefix, ctl.Trim().Replace("'", "''"), strSuffix);

                default:
                    return string.Format("'{0}{1}{2}'", strPrefix, ctl.Trim().Replace("'", "''"), strSuffix);
            }
        }

        /// <summary>
        /// Construct the relation for the key to the constant
        /// </summary>
        /// <param name="key"></param>
        /// <returns></returns>
        private string Relation_Value(string key)
	    {
            string Answer = string.Empty;
            switch( RelationLabel )
		    {
                case relation_EQ:
                    Answer = string.Format("{0}={1}", key, Item_Value(Combo_Value1.Text));
                    break;
            
                case relation_LT:
                    Answer = string.Format("{0}<{1}", key, Item_Value(Combo_Value1.Text));
                    break;
            
                case relation_LE:
                    Answer = string.Format("{0}<={1}", key, Item_Value(Combo_Value1.Text));
                    break;
            
                case relation_GT:
                    Answer = string.Format("{0}>{1}", key, Item_Value(Combo_Value1.Text));
                    break;
            
                case relation_GE:
                    Answer = string.Format("{0}>={1}", key, Item_Value(Combo_Value1.Text));
                    break;
            
                case relation_NE:
                    Answer = string.Format("NOT {0}={1}", key, Item_Value(Combo_Value1.Text));
                    break;
            
                case relation_starts:
                    Answer = string.Format("{0} LIKE {1}", key, Item_Value(Combo_Value1.Text, string.Empty, "%"));
                    break;
            
                case relation_ends:
                    Answer = string.Format("{0} LIKE {1}", key, Item_Value(Combo_Value1.Text, "%", string.Empty));
                    break;
            
                case relation_not_starts:
                    Answer = string.Format("{0} NOT LIKE {1}", key, Item_Value(Combo_Value1.Text, string.Empty, "%"));
                    break;
            
                case relation_contains:
                    Answer = string.Format("{0} LIKE {1}", key, Item_Value(Combo_Value1.Text, "%", "%"));
                    break;
            
                case "not contains":
                    Answer = string.Format("{0} NOT LIKE {1}", key, Item_Value(Combo_Value1.Text, "%", "%"));
                    break;

                case relation_between:
                    Answer = string.Format("{0} BETWEEN {1} AND {2}", key, Item_Value(Combo_Value1.Text), Item_Value(Combo_Value2.Text));
                    break;

                default:
                    break;
            }

            return Answer;
        }

        /// <summary>
        /// Selection criteria when the search is for a creditor ID
        /// </summary>
        /// <param name="strName"></param>
        /// <returns></returns>
        private string Fetch_Creditor(string strName)
        {
            return Relation_Value(strName);
        }

        /// <summary>
        /// Selection critera when the search is against the prefix entries
        /// </summary>
        /// <returns></returns>
        private string Fetch_Prefix()
        {
            return string.Format("c.creditor IN (SELECT [creditor] FROM creditor_addkeys WITH (NOLOCK) WHERE [type] = 'P' AND {0})", Relation_Value("[additional]"));
        }

        /// <summary>
        /// Selection criteria when we are looking for an account number in the list of debts.
        /// </summary>
        /// <returns></returns>
        private string Fetch_AccountNumber()
        {
            System.Text.StringBuilder strSQL = new System.Text.StringBuilder("c.creditor in (SELECT cr.creditor FROM creditors cr LEFT OUTER JOIN creditor_contribution_pcts p with (nolock) on cr.creditor_contribution_pct=p.creditor_contribution_pct");
            strSQL.Append(" INNER JOIN creditor_methods m WITH (NOLOCK) ON m.creditor=cr.creditor_id");
            strSQL.Append(" INNER JOIN rpps_biller_ids ids WITH (NOLOCK) on m.rpps_biller_id = ids.rpps_biller_id");
            strSQL.Append(" INNER JOIN banks b WITH (NOLOCK) on m.bank = b.bank");
            strSQL.Append(" INNER JOIN rpps_masks mk WITH (NOLOCK) on ids.rpps_biller_id = mk.rpps_biller_id");
            strSQL.Append(" WHERE m.type = 'EFT' AND b.type = 'R'");
            strSQL.AppendFormat(" AND '{0}' LIKE dbo.map_rpps_masks ( mk.mask )", Combo_Value1.Text.Trim().Replace("'", "''"));
            strSQL.AppendFormat(" AND {0} = mk.length ) ", Combo_Value1.Text.Trim().Length);

            return strSQL.ToString();
        }

        /// <summary>
        /// Selection criteria when the search is against the alias/prefix table
        /// </summary>
        /// <returns></returns>
        private string Fetch_Alias()
        {
            return string.Format("(({0}) OR (c.creditor IN (SELECT creditor FROM creditor_addkeys WITH (NOLOCK) WHERE {1})))", Relation_Value("c.creditor_name"), Relation_Value("additional"));
        }

        /// <summary>
        /// Selection criteria when the search is against the telephone number table
        /// </summary>
        /// <returns></returns>
        private string Fetch_ContactPhone()
        {
            return string.Format("c.creditor in (select creditor from creditor_contacts cc WITH (NOLOCK) inner join telephonenumbers t1 WITH (NOLOCK) on t1.telephonenumber in (isnull(cc.telephoneid,-1), isnull(cc.alttelephoneid,-1)) where {0})", Relation_Value("replace(isnull(t1.acode,'')+isnull(t1.number,''),'-','')"));
        }

        /// <summary>
        /// Selection criteria when the search is against the creditor address list
        /// </summary>
        /// <param name="strType"></param>
        /// <returns></returns>
        private string Fetch_Address(string strType)
        {
            System.Diagnostics.Debug.Assert(strType == "P");
            return Relation_Value("dbo.address_block_6(ca.attn,dbo.format_address_line_1(a.house,a.direction,a.street,a.suffix,a.modifier,a.modifier_value),a.address_line_2,a.address_line_3, default, dbo.format_city_state_zip(a.city,a.state,a.postalcode))");
        }

        /// <summary>
        /// Selection criteria when the search is against the creditors table for the creditor name.
        /// </summary>
        /// <returns></returns>
        private string Fetch_Name()
        {
            return Relation_Value("c.creditor_name");
        }

        /// <summary>
        /// Selection criteria when the search is against the trust register (check number, etc.)
        /// </summary>
        /// <param name="field"></param>
        /// <returns></returns>
        private string Fetch_Trust(string field)
        {
            return string.Format("c.creditor IN (SELECT creditor FROM registers_trust WITH (NOLOCK) WHERE tran_type in ('AD','MD','CM') AND cleared!='D' AND {0})", Relation_Value(field));
        }

        /// <summary>
        /// Selection criteria when the search is for an invoice number
        /// </summary>
        /// <param name="field"></param>
        /// <returns></returns>
        private string Fetch_Invoice(string field)
        {
            return string.Format("c.creditor IN (SELECT creditor FROM registers_invoices i WITH (NOLOCK) WHERE {0})", Relation_Value(field));
        }

        /// <summary>
        /// Selection criteria when the search is for an RPPS biller id
        /// </summary>
        /// <param name="field"></param>
        /// <returns></returns>
        private string Fetch_RPPS(string field)
        {
            return string.Format("c.creditor_id IN (SELECT creditor FROM creditor_methods m inner join banks b on m.bank = b.bank WHERE b.type = 'R' AND m.type='{0}' AND {1})", field, Relation_Value("m.rpps_biller_id"));
        }

        /// <summary>
        /// Is this a creditor number or label?
        /// </summary>
        /// <returns>TRUE if number. FALSE If label.</returns>
        private bool use_creditor_id()
        {
            Int32 TestNumber = Int32.MinValue;
            return Int32.TryParse(Combo_Value1.Text, out TestNumber) && TestNumber > 0;
        }

        /// <summary>
        /// Build the select statement for the list of creditors
        /// </summary>
        /// <param name="lastCreditor"></param>
        /// <returns></returns>
        private string SelectStatement(string LastCreditor)
        {
            string SelectClause = string.Empty;

            switch( CategoryLabel )
            {
                case Category_Name:
                    SelectClause = Fetch_Name();
                    break;

                case Category_Alias:
                    SelectClause = Fetch_Alias();
                    break;

                case Category_Creditor:
                    if( use_creditor_id() )
                    {
                        SelectClause = Fetch_Creditor("c.creditor_id");
                    }
                    else
                    {
                        SelectClause = Fetch_Creditor("c.creditor");
                    }
                    break;

                case Category_Prefix:
                    SelectClause = Fetch_Prefix();
                    break;

                case Category_SIC:
                    SelectClause = Fetch_Creditor("c.sic").Replace("-", "");    // Remove the dash characters.
                    break;

                case Category_CheckNumber:
                    SelectClause = Fetch_Trust("checknum");
                    break;

                case Category_InvoiceNumber:
                    SelectClause = Fetch_Invoice("i.invoice_register");
                    break;

                case "payment_address":
                    SelectClause = Fetch_Address("P");
                    break;

                case Category_Telephone:
                    SelectClause = Fetch_ContactPhone();
                    break;

                case Category_Payment:
                    SelectClause = Fetch_RPPS("EFT");
                    break;

                case Category_Proposal:
                    SelectClause = Fetch_RPPS("EDI");
                    break;

                case Category_AccountNumber:
                    SelectClause = Fetch_AccountNumber();
                    break;

                case Category_Address:
                    SelectClause = Fetch_Address("P");
                    break;

                default:
                    System.Diagnostics.Debug.Assert(false);
                    return string.Empty;
            }

            if( Check_InactiveCreditors.CheckState != CheckState.Unchecked )
            {
                SelectClause += " AND (c.prohibit_use=0)";
            }

            // Start with the basic verb
            System.Text.StringBuilder sb = new System.Text.StringBuilder();
            sb.Append("SELECT ");

            // If there is a limit then limit the row set
            if( rowLimit > 0 )
            {
                sb.AppendFormat("TOP {0:f0} ", (rowLimit + 1));
            }

            // Build the standard component of the statement
            sb.Append("c.creditor_id as creditor_id, c.creditor as creditor, c.sic, pct.fairshare_pct_eft, pct.fairshare_pct_check, coalesce(c.creditor_name+' '+c.division,c.creditor_name,c.division,'') as name, c.comment, dbo.format_contribution_type(pct.creditor_type_eft,pct.creditor_type_check) as fairshare, dbo.address_block_6(ca.attn,dbo.format_address_line_1(a.house,a.direction,a.street,a.suffix,a.modifier,a.modifier_value),a.address_line_2,a.address_line_3, default, dbo.format_city_state_zip(a.city,a.state,a.postalcode)) as address ");
            sb.Append("FROM creditors c WITH (NOLOCK) ");
            sb.Append("LEFT OUTER JOIN creditor_addresses ca WITH (NOLOCK) ON c.creditor=ca.creditor AND 'P'=ca.type ");
            sb.Append("LEFT OUTER JOIN Addresses a WITH (NOLOCK) ON ca.AddressID = a.Address ");
            sb.Append("LEFT OUTER JOIN creditor_contribution_pcts pct WITH (NOLOCK) ON c.creditor_contribution_pct = pct.creditor_contribution_pct ");

            if( LastCreditor != string.Empty && SelectClause != string.Empty )
            {
                sb.AppendFormat(" WHERE ({1}) AND (c.creditor >= '{0}')", LastCreditor.Replace("'", "''"), SelectClause);
            }
            else
            {
                if ( LastCreditor != string.Empty )
                {
                    sb.AppendFormat(" WHERE c.creditor >= '{0}'", LastCreditor.Replace("'", "''"));
                }
                else
                {
                    sb.Append(" WHERE ");
                    sb.Append(SelectClause);
                }
            }

            sb.Append(" ORDER BY c.creditor");
            return sb.ToString();
        }

        /// <summary>
        /// Handle the OK button CLICK event.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Button_OK_Click(object sender, EventArgs e)
        {
            if( CategoryLabel == Category_Creditor && RelationLabel == relation_EQ )
            {
                // Look for a numeric response. if( so, translate the number to a label.
                privateCreditor = Combo_Value1.Text.Trim();
                Int32 IntValue;
                if( Int32.TryParse(privateCreditor, out IntValue) )
                {
                    privateCreditor = FindCreditorByID(IntValue);
                }

                // If there is a creditor then find the creditor in the system to ensure that it is present.
                // We don't care if the creditor is marked "prohibit use" since it is specfically a given creditor ID.
                if (privateCreditor != string.Empty)
                {
                    Repository.GetScalarResult gsr = Repository.Creditors.Exists(privateCreditor);
                    if( gsr.Success && Convert.ToBoolean(gsr.Result) )
                    {
                        SetCreditor(privateCreditor);
                    }
                    else
                    {
                        DebtPlus.Data.Forms.MessageBox.Show("The creditor is not valid. Please choose an existing creditor.", "Information ! Found", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Stop);
                    }
                    return;
                }
            }

            // Allocate the dataset for the information
            using (dsRows = new DataSet("creditor_search"))
            {
                // Start with the initial search criteria
                LoadSearchList(SelectStatement(string.Empty));

                // Determine if there are any values in the search expression
                DataTable tbl = dsRows.Tables[0];
                string Filter = string.Empty;

                if (tbl.Rows.Count == 0)
                {
                    DebtPlus.Data.Forms.MessageBox.Show("There are no creditors which match your search criteria. Please change the selection and try again or press Cancel to return to the previous screen.", "No creditors could be found", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    return;
                }

                // If only one row matched then stop here
                if (tbl.Rows.Count == 1)
                {
                    privateCreditor = Convert.ToString(tbl.Rows[0]["creditor"]);
                    SetCreditor(privateCreditor);
                    return;
                }

                // Display the initial search form
                using (var FormCreditorList = new CreditorListForm())
                {
                    FormCreditorList.NextPage += FormCreditorList_NextPage;

                    // Load the first page of creditors into the form
                    LoadNextPage(FormCreditorList);

                    // Ask the user to choose a creditor at this point.
                    if (FormCreditorList.ShowDialog() == System.Windows.Forms.DialogResult.OK)
                    {
                        privateCreditor = FormCreditorList.Creditor;
                        SetCreditor(privateCreditor);
                    }
                    FormCreditorList.NextPage -= FormCreditorList_NextPage;
                }
            }

            // Remove the pointer to the dataset
            dsRows = null;
        }
#endregion

#region formCreditorList Events
        /// <summary>
        /// Process the form's MORE button when it is pressed
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void FormCreditorList_NextPage(object sender, EventArgs e)
	    {
            // Empty the dataset for the results
            dsRows.Clear();

            // Read the initial block from the database
            LoadSearchList(SelectStatement(lastCreditor));
            LoadNextPage((CreditorListForm) sender);
        }

        /// <summary>
        /// Load the next page of creditors into the search function
        /// </summary>
        /// <param name="formCreditorList"></param>
        private void LoadNextPage(CreditorListForm FormCreditorList)
        {
            string Filter = string.Empty;
            DataTable tbl = dsRows.Tables[0];

            FormCreditorList.Button_More.Visible = false;

            // If there are more than the limit in the search then enable the more button
            if( rowLimit > 0 && tbl.Rows.Count > rowLimit )
            {
                FormCreditorList.Button_More.Visible = true;

                // Remove the last item from the list. We generate the list one item too far to find the first item on the next page.
                using( DataView vueLast = new DataView(tbl, string.Empty, "creditor desc", DataViewRowState.CurrentRows) )
                {
                    lastCreditor = Convert.ToString(vueLast[0]["creditor"]);
                    Filter = string.Format("creditor<>'{0}'", lastCreditor.Replace("'", "''"));
                }
            }

            // Load the datasource for the view.
            FormCreditorList.GridControl1.DataSource = new DataView(tbl, Filter, "creditor", DataViewRowState.CurrentRows);
            FormCreditorList.GridControl1.RefreshDataSource();
        }
#endregion

#region Load the Creditor List Form Items
        /// <summary>
        /// The search limit for finding matches against the creditor criteria
        /// </summary>
        private Int32 SelectLimitCount
	    {
            get
            {
                Microsoft.Win32.RegistryKey RegKey = Microsoft.Win32.Registry.LocalMachine;
                Int32 Result = 1000;

                // Look in the machine values for the defaults for all users
                try
                {
                    RegKey = RegKey.OpenSubKey(DebtPlus.Configuration.AssemblyInfo.InstallationRegistryKey + @"\options", false);
                    if (RegKey != null)
                    {
                        object obj = RegKey.GetValue("creditor search limit", string.Empty);
                        Int32 Answer = -1;
                        if (obj != null && Int32.TryParse(Convert.ToString(obj), out Answer) && Answer > 0)
                        {
                            Result = Answer;
                        }
                    }
                }
                catch (Exception ex)
                {
                    DebtPlus.Svc.Common.ErrorHandling.HandleErrors(ex);
                }
                finally
                {
                    if (RegKey != null)
                    {
                        RegKey.Close();
                    }
                }

                // Allow the user to override the machine settings
                RegKey = Microsoft.Win32.Registry.CurrentUser;
                try
                {
                    if (RegKey != null)
                    {
                        object obj = RegKey.GetValue("creditor search limit", string.Empty);
                        Int32 Answer = -1;
                        if (obj != null && Int32.TryParse(Convert.ToString(obj), out Answer) && Answer > 0)
                        {
                            Result = Answer;
                        }
                    }
                }
                catch (Exception ex)
                {
                    DebtPlus.Svc.Common.ErrorHandling.HandleErrors(ex);
                }
                finally
                {
                    if (RegKey != null)
                    {
                        RegKey.Close();
                    }
                }
                return Result;
            }
        }

        /// <summary>
        /// Rebuild the list of Creditors from the database
        /// </summary>
        private void LoadSearchList(string Statement)
	    {
            using (new Common.CursorManager())
            {
                var gdr = Repository.Common.FillNamedDataTableFromSelectCommand(Statement, dsRows, "CreditorList");
                if (!gdr.Success) return;

                var tbl = dsRows.Tables["CreditorList"];
                if (tbl.PrimaryKey.GetUpperBound(0) <= 0)
                {
                    tbl.PrimaryKey = new[] { tbl.Columns["creditor"] };
                }
            }
        }
#endregion

        /// <summary>
        /// Find a creditor from the indicated ID number
        /// </summary>
        /// <param name="CreditorID"></param>
        /// <returns></returns>
        /// <remarks></remarks>
        public string FindCreditorByID(Int32 CreditorID)
        {
            Repository.GetScalarResult gsr = Repository.Creditors.GetCreditorById(CreditorID);
            if (gsr.Success && !Object.Equals(System.DBNull.Value, gsr.Result))
            {
                return Convert.ToString(gsr.Result);
            }
            return string.Empty;
        }

        /// <summary>
        /// Save the current history for the next invocation
        /// </summary>
        /// <param name=Category_Creditor>The creditor ID which was selected</param>
        /// <remarks></remarks>
        private void Save_Recent(string Creditor)
        {
            // First, save the creditor ID
            using (var mruList = new DebtPlus.Data.MRU("Creditors", "Recent Creditors"))
            {
                mruList.InsertTopMost(Creditor);
                mruList.SaveKey();
            }

            // Now, save the category relative item into the list.
            if( CategoryLabel != Category_Creditor )
            {
                using (var mruList = new DebtPlus.Data.MRU(GetMRUKey))
                {
                    mruList.InsertTopMost(Combo_Value1.Text.Trim());
                    mruList.SaveKey();
                }
            }
        }

        /// <summary>
        /// Perform the creditor search operation
        /// </summary>
        /// <param name="owner">Owner for the windows</param>
        /// <returns>The dialog result status for the serarch operation</returns>
        public DialogResult PerformCreditorSearch(IWin32Window owner)
        {
            return this.ShowDialog(owner);
        }

        /// <summary>
        /// Perform the creditor search operation
        /// </summary>
        /// <returns>The dialog result status for the serarch operation</returns>
        public DialogResult PerformCreditorSearch()
        {
            return this.ShowDialog();
        }
    }
}
