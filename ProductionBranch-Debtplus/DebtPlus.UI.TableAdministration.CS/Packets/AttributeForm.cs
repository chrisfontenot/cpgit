﻿using System;
using DebtPlus.LINQ;

namespace DebtPlus.UI.TableAdministration.CS.Packets
{
    internal partial class AttributeForm : DebtPlus.Data.Forms.DebtPlusForm
    {
        private BusinessContext bc = null;
        private DocumentAttribute record = null;

        /// <summary>
        /// Initialize the new form context
        /// </summary>
        internal AttributeForm()
            : base()
        {
            InitializeComponent();
        }

        /// <summary>
        /// Initialize the new form context
        /// </summary>
        internal AttributeForm(BusinessContext bc, DocumentAttribute record)
            : this()
        {
            this.bc = bc;
            this.record = record;
            RegisterHandlers();
        }

        /// <summary>
        /// Register the event handlers
        /// </summary>
        private void RegisterHandlers()
        {
            LayoutControl1.Resize += LayoutControl1_Resize;
            Load += AttributeForm_Load;
            CheckEdit_all_languages.CheckedChanged += CheckedChanged;
            CheckEdit_all_states.CheckedChanged += CheckedChanged;
            SimpleButton1.Click += SimpleButton1_Click;
        }

        /// <summary>
        /// Remove the event handler registrations
        /// </summary>
        private void UnRegisterHandlers()
        {
            LayoutControl1.Resize -= LayoutControl1_Resize;
            Load -= AttributeForm_Load;
            CheckEdit_all_languages.CheckedChanged -= CheckedChanged;
            CheckEdit_all_states.CheckedChanged -= CheckedChanged;
            SimpleButton1.Click -= SimpleButton1_Click;
        }

        /// <summary>
        /// Process the CLICK event on the OK button
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void SimpleButton1_Click(object sender, System.EventArgs e)
        {
            record.AllLanguagesFLG = CheckEdit_all_languages.Checked;
            record.AllStatesFLG = CheckEdit_all_states.Checked;
            record.LanguageID = DebtPlus.Utils.Nulls.v_Int32(LookUpEdit_language.EditValue);
            record.State = DebtPlus.Utils.Nulls.v_Int32(LookUpEdit_State.EditValue);

            // Clear the language reference if "all languages" is used
            if (record.AllLanguagesFLG)
            {
                record.LanguageID = null;
            }

            // Clear the state reference if "all states" is used
            if (record.AllStatesFLG)
            {
                record.State = null;
            }

            DialogResult = System.Windows.Forms.DialogResult.OK;
        }

        /// <summary>
        /// Handle the change in the checkbox status values
        /// </summary>
        private void CheckedChanged(object sender, EventArgs e)
        {
            EnableControls();
        }

        /// <summary>
        /// Enable or disable controls based upon the checkbox status
        /// </summary>
        private void EnableControls()
        {
            CheckEdit3.Checked = !CheckEdit_all_languages.Checked;
            CheckEdit2.Checked = !CheckEdit_all_states.Checked;
            LookUpEdit_language.Enabled = CheckEdit3.Checked;
            LookUpEdit_State.Enabled = CheckEdit2.Checked;
        }

        /// <summary>
        /// Center the buttons when the form is resized.
        /// </summary>
        private void LayoutControl1_Resize(object sender, System.EventArgs e)
        {
            EmptySpaceItem_left.Width = (EmptySpaceItem_left.Width + EmptySpaceItem_right.Width) / 2;
        }

        /// <summary>
        /// Process the LOAD event for the form
        /// </summary>
        private void AttributeForm_Load(object sender, System.EventArgs e)
        {
            UnRegisterHandlers();
            try
            {
                LookUpEdit_language.Properties.DataSource = DebtPlus.LINQ.Cache.AttributeType.getLanguageList();
                LookUpEdit_State.Properties.DataSource = DebtPlus.LINQ.Cache.state.getList();

                CheckEdit_all_languages.Checked = record.AllLanguagesFLG;
                CheckEdit_all_states.Checked = record.AllStatesFLG;
                LookUpEdit_language.EditValue = record.LanguageID;
                LookUpEdit_State.EditValue = record.State;

                EnableControls();
            }
            finally
            {
                RegisterHandlers();
            }
        }
    }
}