using System;
using System.Drawing;
using System.Windows.Forms;
using DebtPlus.LINQ;

namespace DebtPlus.UI.TableAdministration.CS.Packets
{
    internal partial class AttributeGridControl : DebtPlus.Data.Controls.UserControl
    {
        private BusinessContext bc = null;
        private DocumentComponent documentRecord = null;
        private Int32 ControlRow = -1;

        internal AttributeGridControl()
            : base()
        {
            InitializeComponent();
        }

        /// <summary>
        /// Register the event handlers
        /// </summary>
        /// <remarks></remarks>
        private void RegisterHandlers()
        {
            PopupMenu1.Popup += PopupMenu_Items_Popup;
            BarButtonItem_Add.ItemClick += MenuItemCreate;
            BarButtonItem_Change.ItemClick += MenuItemEdit;
            BarButtonItem_Delete.ItemClick += MenuItemDelete;
            gridView1.MouseUp += gridView1_MouseUp;
            gridView1.MouseDown += gridView1_MouseDown;
            gridView1.FocusedRowChanged += gridView1_FocusedRowChanged;
            gridView1.DoubleClick += gridView1_DoubleClick;
        }

        /// <summary>
        /// Remove the event handler registration
        /// </summary>
        /// <remarks></remarks>
        private void UnRegisterHandlers()
        {
            PopupMenu1.Popup -= PopupMenu_Items_Popup;
            BarButtonItem_Add.ItemClick -= MenuItemCreate;
            BarButtonItem_Change.ItemClick -= MenuItemEdit;
            BarButtonItem_Delete.ItemClick -= MenuItemDelete;
            gridView1.MouseUp -= gridView1_MouseUp;
            gridView1.MouseDown -= gridView1_MouseDown;
            gridView1.FocusedRowChanged -= gridView1_FocusedRowChanged;
            gridView1.DoubleClick -= gridView1_DoubleClick;
        }

        /// <summary>
        /// Read the information for the grid control.
        /// </summary>
        internal void ReadForm(BusinessContext bc, DocumentComponent documentRecord)
        {
            UnRegisterHandlers();
            try
            {
                this.gridColumn_LanguageID.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Custom;
                this.gridColumn_LanguageID.GroupFormat.FormatType = DevExpress.Utils.FormatType.Custom;
                this.gridColumn_State.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Custom;
                this.gridColumn_State.GroupFormat.FormatType = DevExpress.Utils.FormatType.Custom;

                // Load the formatter items into the grid
                gridColumn_LanguageID.DisplayFormat.Format = new LanguageFormatter();
                gridColumn_LanguageID.GroupFormat.Format = new LanguageFormatter();
                gridColumn_State.DisplayFormat.Format = new StateFormatter();
                gridColumn_State.GroupFormat.Format = new StateFormatter();

                this.bc = bc;
                this.documentRecord = documentRecord;

                // Load the grid with the attribute list.
                gridControl1.DataSource = documentRecord.DocumentAttributes.GetNewBindingList();
                gridView1.BestFitColumns();
                gridView1.RefreshData();
            }
            finally
            {
                RegisterHandlers();
            }
        }

        /// <summary>
        /// Edit the current item
        /// </summary>
        private void UpdateRecord(object obj)
        {
            var record = obj as DocumentAttribute;
            if (record != null)
            {
                // Just do the edit. The update will be done when the record is updated.
                using (var frm = new AttributeForm(bc, record))
                {
                    if (frm.ShowDialog() != DialogResult.OK)
                    {
                        return;
                    }
                }
                gridControl1.DataSource = documentRecord.DocumentAttributes.GetNewBindingList();
                gridView1.RefreshData();
            }
        }

        /// <summary>
        /// Create a new record
        /// </summary>
        /// <remarks></remarks>
        private void CreateRecord()
        {
            var record = DebtPlus.LINQ.Factory.Manufacture_DocumentAttribute();
            using (var frm = new AttributeForm(bc, record))
            {
                if (frm.ShowDialog() != DialogResult.OK)
                {
                    return;
                }
            }

            // Add the record to the component list and rebuild the grid
            documentRecord.DocumentAttributes.Add(record);
            gridControl1.DataSource = documentRecord.DocumentAttributes.GetNewBindingList();
            gridView1.RefreshData();
        }

        /// <summary>
        /// Delete the current item
        /// </summary>
        private void DeleteRecord(object obj)
        {
            var record = obj as DocumentAttribute;
            if (record != null)
            {
                // Remove the record and tell LINQ to delete it later
                documentRecord.DocumentAttributes.Remove(record);
                record.DocumentID = default(Int32);
                record.DocumentComponent = null;

                // Refresh the grid control
                gridControl1.DataSource = documentRecord.DocumentAttributes.GetNewBindingList();
                gridView1.RefreshData();
            }
        }

        /// <summary>
        /// Handle the condition where the popup menu was activated
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        /// <remarks></remarks>
        private void gridView1_MouseUp(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Right)
            {
                PopupMenu1.ShowPopup(Control.MousePosition);
            }
        }

        /// <summary>
        /// Handle the condition where the popup menu was activated
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        /// <remarks></remarks>
        protected virtual void PopupMenu_Items_Popup(object sender, EventArgs e)
        {
            BarButtonItem_Add.Enabled = true;
            BarButtonItem_Delete.Enabled = ControlRow >= 0;
            BarButtonItem_Change.Enabled = ControlRow >= 0;
        }

        /// <summary>
        /// Process a CREATE menu item choice
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        /// <remarks></remarks>
        protected void MenuItemCreate(object sender, EventArgs e)
        {
            CreateRecord();
        }

        /// <summary>
        /// Process an EDIT menu item choice
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        /// <remarks></remarks>
        protected void MenuItemEdit(object sender, EventArgs e)
        {
            if (ControlRow >= 0)
            {
                UpdateRecord(gridView1.GetRow(ControlRow));
            }
        }

        /// <summary>
        /// Process a DELETE menu item
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        /// <remarks></remarks>
        protected void MenuItemDelete(object sender, EventArgs e)
        {
            if (ControlRow >= 0)
            {
                DeleteRecord(gridView1.GetRow(ControlRow));
            }
        }

        /// <summary>
        /// Look for the mouse down on the grid control
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        /// <remarks></remarks>
        private void gridView1_MouseDown(object sender, MouseEventArgs e)
        {
            DevExpress.XtraGrid.Views.Grid.ViewInfo.GridHitInfo hi = gridView1.CalcHitInfo(new Point(e.X, e.Y));
            ControlRow = hi.IsValid && hi.InRow ? hi.RowHandle : -1;
        }

        /// <summary>
        /// Process a change in the current row for the control
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        /// <remarks></remarks>
        private void gridView1_FocusedRowChanged(object sender, DevExpress.XtraGrid.Views.Base.FocusedRowChangedEventArgs e)
        {
            gridView1.FocusedRowHandle = e.FocusedRowHandle;
        }

        /// <summary>
        /// Double Click on an item -- edit it
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        /// <remarks></remarks>
        private void gridView1_DoubleClick(object sender, EventArgs e)
        {
            // Find the row targeted as the double-click item
            DevExpress.XtraGrid.Views.Grid.GridView gv = (DevExpress.XtraGrid.Views.Grid.GridView)sender;
            DevExpress.XtraGrid.GridControl ctl = (DevExpress.XtraGrid.GridControl)gv.GridControl;
            DevExpress.XtraGrid.Views.Grid.ViewInfo.GridHitInfo hi = gv.CalcHitInfo((ctl.PointToClient(System.Windows.Forms.Control.MousePosition)));
            ControlRow = hi.RowHandle;

            if (ControlRow >= 0)
            {
                UpdateRecord(gridView1.GetRow(ControlRow));
            }
        }

        private class StateFormatter : ICustomFormatter, IFormatProvider
        {
            internal StateFormatter()
                : base()
            {
            }

            public object GetFormat(Type formatType)
            {
                return this;
            }

            public string Format(string format1, object arg, IFormatProvider formatProvider)
            {
                Int32 stateID = DebtPlus.Utils.Nulls.v_Int32(arg).GetValueOrDefault(-1);
                if (stateID >= 0)
                {
                    var q = DebtPlus.LINQ.Cache.state.getList().Find(s => s.Id == stateID);
                    if (q != null)
                    {
                        return q.Name;
                    }
                }
                return string.Empty;
            }
        }

        private class LanguageFormatter : ICustomFormatter, IFormatProvider
        {
            internal LanguageFormatter()
                : base()
            {
            }

            public object GetFormat(Type formatType)
            {
                return this;
            }

            public string Format(string format1, object arg, IFormatProvider formatProvider)
            {
                Int32 attributeID = DebtPlus.Utils.Nulls.v_Int32(arg).GetValueOrDefault(-1);
                if (attributeID > 0)
                {
                    var q = DebtPlus.LINQ.Cache.AttributeType.getList().Find(s => s.Id == attributeID);
                    if (q != null)
                    {
                        return q.Attribute;
                    }
                }
                return string.Empty;
            }
        }
    }
}