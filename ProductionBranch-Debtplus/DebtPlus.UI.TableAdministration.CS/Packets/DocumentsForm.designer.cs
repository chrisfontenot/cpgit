namespace DebtPlus.UI.TableAdministration.CS.Packets
{
    partial class DocumentsForm
    {
        //Form overrides dispose to clean up the component list.
        [System.Diagnostics.DebuggerNonUserCode()]
        protected override void Dispose(bool disposing)
        {
            try
            {
                if (disposing && components != null)
                {
                    components.Dispose();
                }
            }
            finally
            {
                base.Dispose(disposing);
            }
        }

        //Required by the Windows Form Designer
        private System.ComponentModel.IContainer components = null;

        //NOTE: The following procedure is required by the Windows Form Designer
        //It can be modified using the Windows Form Designer.  
        //Do not modify it using the code editor.
        [System.Diagnostics.DebuggerStepThrough]
        private void InitializeComponent()
        {
            this.LayoutControl1 = new DevExpress.XtraLayout.LayoutControl();
            this.SimpleButton1 = new DevExpress.XtraEditors.SimpleButton();
            this.TextEdit1 = new DevExpress.XtraEditors.TextEdit();
            this.LayoutControlGroup1 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.LayoutControlItem1 = new DevExpress.XtraLayout.LayoutControlItem();
            this.EmptySpaceItem1 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.EmptySpaceItem_right = new DevExpress.XtraLayout.EmptySpaceItem();
            this.LayoutControlItem8 = new DevExpress.XtraLayout.LayoutControlItem();
            this.EmptySpaceItem_left = new DevExpress.XtraLayout.EmptySpaceItem();
            this.ComponentGridControl1 = new DebtPlus.UI.TableAdministration.CS.Packets.ComponentGridControl();
            this.LayoutControlItem2 = new DevExpress.XtraLayout.LayoutControlItem();
            ((System.ComponentModel.ISupportInitialize)(this.DxErrorProvider1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LayoutControl1)).BeginInit();
            this.LayoutControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.TextEdit1.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LayoutControlGroup1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LayoutControlItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.EmptySpaceItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.EmptySpaceItem_right)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LayoutControlItem8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.EmptySpaceItem_left)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LayoutControlItem2)).BeginInit();
            this.SuspendLayout();
            // 
            // DefaultLookAndFeel1
            // 
            this.DefaultLookAndFeel1.LookAndFeel.SkinName = "McSkin";
            // 
            // LayoutControl1
            // 
            this.LayoutControl1.Controls.Add(this.SimpleButton1);
            this.LayoutControl1.Controls.Add(this.TextEdit1);
            this.LayoutControl1.Controls.Add(this.ComponentGridControl1);
            this.LayoutControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.LayoutControl1.Location = new System.Drawing.Point(0, 0);
            this.LayoutControl1.Name = "LayoutControl1";
            this.LayoutControl1.OptionsCustomizationForm.DesignTimeCustomizationFormPositionAndSize = new System.Drawing.Rectangle(633, 81, 250, 350);
            this.LayoutControl1.Root = this.LayoutControlGroup1;
            this.LayoutControl1.Size = new System.Drawing.Size(358, 268);
            this.LayoutControl1.TabIndex = 0;
            this.LayoutControl1.Text = "LayoutControl1";
            // 
            // SimpleButton1
            // 
            this.SimpleButton1.Location = new System.Drawing.Point(146, 234);
            this.SimpleButton1.MaximumSize = new System.Drawing.Size(66, 22);
            this.SimpleButton1.MinimumSize = new System.Drawing.Size(66, 22);
            this.SimpleButton1.Name = "SimpleButton1";
            this.SimpleButton1.Size = new System.Drawing.Size(66, 22);
            this.SimpleButton1.StyleController = this.LayoutControl1;
            this.SimpleButton1.TabIndex = 12;
            this.SimpleButton1.Text = "&OK";
            // 
            // TextEdit1
            // 
            this.TextEdit1.Location = new System.Drawing.Point(42, 12);
            this.TextEdit1.Name = "TextEdit1";
            this.TextEdit1.Size = new System.Drawing.Size(304, 20);
            this.TextEdit1.StyleController = this.LayoutControl1;
            this.TextEdit1.TabIndex = 4;
            // 
            // LayoutControlGroup1
            // 
            this.LayoutControlGroup1.CustomizationFormText = "Root";
            this.LayoutControlGroup1.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.True;
            this.LayoutControlGroup1.GroupBordersVisible = false;
            this.LayoutControlGroup1.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.LayoutControlItem1,
            this.EmptySpaceItem1,
            this.EmptySpaceItem_right,
            this.LayoutControlItem8,
            this.EmptySpaceItem_left,
            this.LayoutControlItem2});
            this.LayoutControlGroup1.Location = new System.Drawing.Point(0, 0);
            this.LayoutControlGroup1.Name = "Root";
            this.LayoutControlGroup1.Size = new System.Drawing.Size(358, 268);
            this.LayoutControlGroup1.Text = "Root";
            this.LayoutControlGroup1.TextVisible = false;
            // 
            // LayoutControlItem1
            // 
            this.LayoutControlItem1.Control = this.TextEdit1;
            this.LayoutControlItem1.CustomizationFormText = "Name";
            this.LayoutControlItem1.Location = new System.Drawing.Point(0, 0);
            this.LayoutControlItem1.Name = "LayoutControlItem1";
            this.LayoutControlItem1.Size = new System.Drawing.Size(338, 24);
            this.LayoutControlItem1.Text = "Name";
            this.LayoutControlItem1.TextSize = new System.Drawing.Size(27, 13);
            // 
            // EmptySpaceItem1
            // 
            this.EmptySpaceItem1.AllowHotTrack = false;
            this.EmptySpaceItem1.CustomizationFormText = "EmptySpaceItem1";
            this.EmptySpaceItem1.Location = new System.Drawing.Point(0, 209);
            this.EmptySpaceItem1.MaxSize = new System.Drawing.Size(0, 13);
            this.EmptySpaceItem1.MinSize = new System.Drawing.Size(10, 13);
            this.EmptySpaceItem1.Name = "EmptySpaceItem1";
            this.EmptySpaceItem1.Size = new System.Drawing.Size(338, 13);
            this.EmptySpaceItem1.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.EmptySpaceItem1.Text = "EmptySpaceItem1";
            this.EmptySpaceItem1.TextSize = new System.Drawing.Size(0, 0);
            // 
            // EmptySpaceItem_right
            // 
            this.EmptySpaceItem_right.AllowHotTrack = false;
            this.EmptySpaceItem_right.CustomizationFormText = "EmptySpaceItem2";
            this.EmptySpaceItem_right.Location = new System.Drawing.Point(204, 222);
            this.EmptySpaceItem_right.Name = "EmptySpaceItem_right";
            this.EmptySpaceItem_right.Size = new System.Drawing.Size(134, 26);
            this.EmptySpaceItem_right.Text = "EmptySpaceItem_right";
            this.EmptySpaceItem_right.TextSize = new System.Drawing.Size(0, 0);
            // 
            // LayoutControlItem8
            // 
            this.LayoutControlItem8.Control = this.SimpleButton1;
            this.LayoutControlItem8.CustomizationFormText = "LayoutControlItem8";
            this.LayoutControlItem8.Location = new System.Drawing.Point(134, 222);
            this.LayoutControlItem8.Name = "LayoutControlItem8";
            this.LayoutControlItem8.Size = new System.Drawing.Size(70, 26);
            this.LayoutControlItem8.Text = "LayoutControlItem8";
            this.LayoutControlItem8.TextSize = new System.Drawing.Size(0, 0);
            this.LayoutControlItem8.TextToControlDistance = 0;
            this.LayoutControlItem8.TextVisible = false;
            // 
            // EmptySpaceItem_left
            // 
            this.EmptySpaceItem_left.AllowHotTrack = false;
            this.EmptySpaceItem_left.CustomizationFormText = "EmptySpaceItem3";
            this.EmptySpaceItem_left.Location = new System.Drawing.Point(0, 222);
            this.EmptySpaceItem_left.Name = "EmptySpaceItem_left";
            this.EmptySpaceItem_left.Size = new System.Drawing.Size(134, 26);
            this.EmptySpaceItem_left.Text = "EmptySpaceItem_left";
            this.EmptySpaceItem_left.TextSize = new System.Drawing.Size(0, 0);
            // 
            // ComponentGridControl1
            // 
            this.ComponentGridControl1.Location = new System.Drawing.Point(12, 36);
            this.ComponentGridControl1.Name = "ComponentGridControl1";
            this.ComponentGridControl1.Size = new System.Drawing.Size(334, 181);
            this.ComponentGridControl1.TabIndex = 14;
            // 
            // LayoutControlItem2
            // 
            this.LayoutControlItem2.Control = this.ComponentGridControl1;
            this.LayoutControlItem2.CustomizationFormText = "Component Grid";
            this.LayoutControlItem2.Location = new System.Drawing.Point(0, 24);
            this.LayoutControlItem2.Name = "LayoutControlItem2";
            this.LayoutControlItem2.Size = new System.Drawing.Size(338, 185);
            this.LayoutControlItem2.Text = "Component Grid";
            this.LayoutControlItem2.TextSize = new System.Drawing.Size(0, 0);
            this.LayoutControlItem2.TextToControlDistance = 0;
            this.LayoutControlItem2.TextVisible = false;
            // 
            // DocumentsForm
            // 
            this.AcceptButton = this.SimpleButton1;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(358, 268);
            this.Controls.Add(this.LayoutControl1);
            this.Name = "DocumentsForm";
            this.Text = "Document";
            ((System.ComponentModel.ISupportInitialize)(this.DxErrorProvider1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LayoutControl1)).EndInit();
            this.LayoutControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.TextEdit1.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LayoutControlGroup1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LayoutControlItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.EmptySpaceItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.EmptySpaceItem_right)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LayoutControlItem8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.EmptySpaceItem_left)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LayoutControlItem2)).EndInit();
            this.ResumeLayout(false);

        }
        private DevExpress.XtraLayout.LayoutControl LayoutControl1;
        private DevExpress.XtraEditors.SimpleButton SimpleButton1;
        private DevExpress.XtraEditors.TextEdit TextEdit1;
        private DevExpress.XtraLayout.LayoutControlGroup LayoutControlGroup1;
        private DevExpress.XtraLayout.LayoutControlItem LayoutControlItem1;
        private DevExpress.XtraLayout.EmptySpaceItem EmptySpaceItem1;
        private DevExpress.XtraLayout.EmptySpaceItem EmptySpaceItem_right;
        private DevExpress.XtraLayout.LayoutControlItem LayoutControlItem8;
        private DevExpress.XtraLayout.EmptySpaceItem EmptySpaceItem_left;
        private DebtPlus.UI.TableAdministration.CS.Packets.ComponentGridControl ComponentGridControl1;
        private DevExpress.XtraLayout.LayoutControlItem LayoutControlItem2;
    }
}