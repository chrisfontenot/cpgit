namespace DebtPlus.UI.TableAdministration.CS.Packets
{
    partial class ComponentsForm
    {
        //Form overrides dispose to clean up the component list.
        [System.Diagnostics.DebuggerNonUserCode]
        protected override void Dispose(bool disposing)
        {
            try
            {
                if (disposing && components != null)
                {
                    components.Dispose();
                }
            }
            finally
            {
                base.Dispose(disposing);
            }
        }

        //Required by the Windows Form Designer
        private System.ComponentModel.IContainer components = null;

        //NOTE: The following procedure is required by the Windows Form Designer
        //It can be modified using the Windows Form Designer.  
        //Do not modify it using the code editor.
        [System.Diagnostics.DebuggerStepThrough]
        private void InitializeComponent()
        {
            this.LayoutControl1 = new DevExpress.XtraLayout.LayoutControl();
            this.ComboBoxEdit_Section = new DevExpress.XtraEditors.ComboBoxEdit();
            this.simpleButton_OK = new DevExpress.XtraEditors.SimpleButton();
            this.CheckEdit1 = new DevExpress.XtraEditors.CheckEdit();
            this.TextEdit2 = new DevExpress.XtraEditors.TextEdit();
            this.ComboBoxEdit_UriType = new DevExpress.XtraEditors.ComboBoxEdit();
            this.SpinEdit1 = new DevExpress.XtraEditors.SpinEdit();
            this.ComboBoxEdit_Type = new DevExpress.XtraEditors.ComboBoxEdit();
            this.LayoutControlGroup1 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.LayoutControlItem6 = new DevExpress.XtraLayout.LayoutControlItem();
            this.EmptySpaceItem1 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.EmptySpaceItem_right = new DevExpress.XtraLayout.EmptySpaceItem();
            this.LayoutControlItem8 = new DevExpress.XtraLayout.LayoutControlItem();
            this.EmptySpaceItem_left = new DevExpress.XtraLayout.EmptySpaceItem();
            this.LayoutControlItem10 = new DevExpress.XtraLayout.LayoutControlItem();
            this.LayoutControlItem7 = new DevExpress.XtraLayout.LayoutControlItem();
            this.LayoutControlItem5 = new DevExpress.XtraLayout.LayoutControlItem();
            this.LayoutControlItem2 = new DevExpress.XtraLayout.LayoutControlItem();
            this.LayoutControlItem3 = new DevExpress.XtraLayout.LayoutControlItem();
            this.LayoutControlItem1 = new DevExpress.XtraLayout.LayoutControlItem();
            this.dateEdit_EffectiveStartDate = new DevExpress.XtraEditors.DateEdit();
            this.layoutControlItem4 = new DevExpress.XtraLayout.LayoutControlItem();
            this.dateEdit_EffectiveExpireDate = new DevExpress.XtraEditors.DateEdit();
            this.layoutControlItem9 = new DevExpress.XtraLayout.LayoutControlItem();
            this.AttributeGridControl1 = new AttributeGridControl();
            ((System.ComponentModel.ISupportInitialize)(this.DxErrorProvider1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LayoutControl1)).BeginInit();
            this.LayoutControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ComboBoxEdit_Section.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.CheckEdit1.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TextEdit2.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ComboBoxEdit_UriType.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.SpinEdit1.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ComboBoxEdit_Type.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LayoutControlGroup1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LayoutControlItem6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.EmptySpaceItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.EmptySpaceItem_right)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LayoutControlItem8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.EmptySpaceItem_left)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LayoutControlItem10)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LayoutControlItem7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LayoutControlItem5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LayoutControlItem2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LayoutControlItem3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LayoutControlItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEdit_EffectiveStartDate.Properties.VistaTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEdit_EffectiveStartDate.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEdit_EffectiveExpireDate.Properties.VistaTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEdit_EffectiveExpireDate.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem9)).BeginInit();
            this.SuspendLayout();
            // 
            // LayoutControl1
            // 
            this.LayoutControl1.Controls.Add(this.dateEdit_EffectiveExpireDate);
            this.LayoutControl1.Controls.Add(this.dateEdit_EffectiveStartDate);
            this.LayoutControl1.Controls.Add(this.ComboBoxEdit_Section);
            this.LayoutControl1.Controls.Add(this.simpleButton_OK);
            this.LayoutControl1.Controls.Add(this.CheckEdit1);
            this.LayoutControl1.Controls.Add(this.AttributeGridControl1);
            this.LayoutControl1.Controls.Add(this.TextEdit2);
            this.LayoutControl1.Controls.Add(this.ComboBoxEdit_UriType);
            this.LayoutControl1.Controls.Add(this.SpinEdit1);
            this.LayoutControl1.Controls.Add(this.ComboBoxEdit_Type);
            this.LayoutControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.LayoutControl1.Location = new System.Drawing.Point(0, 0);
            this.LayoutControl1.Name = "LayoutControl1";
            this.LayoutControl1.OptionsCustomizationForm.DesignTimeCustomizationFormPositionAndSize = new System.Drawing.Rectangle(633, 81, 250, 350);
            this.LayoutControl1.Root = this.LayoutControlGroup1;
            this.LayoutControl1.Size = new System.Drawing.Size(379, 312);
            this.LayoutControl1.TabIndex = 0;
            this.LayoutControl1.Text = "LayoutControl1";
            // 
            // ComboBoxEdit_Section
            // 
            this.ComboBoxEdit_Section.Location = new System.Drawing.Point(64, 60);
            this.ComboBoxEdit_Section.Name = "ComboBoxEdit_Section";
            this.ComboBoxEdit_Section.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.ComboBoxEdit_Section.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
            this.ComboBoxEdit_Section.Size = new System.Drawing.Size(142, 20);
            this.ComboBoxEdit_Section.StyleController = this.LayoutControl1;
            this.ComboBoxEdit_Section.TabIndex = 14;
            // 
            // simpleButton_OK
            // 
            this.simpleButton_OK.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.simpleButton_OK.Enabled = false;
            this.simpleButton_OK.Location = new System.Drawing.Point(157, 278);
            this.simpleButton_OK.MaximumSize = new System.Drawing.Size(66, 22);
            this.simpleButton_OK.MinimumSize = new System.Drawing.Size(66, 22);
            this.simpleButton_OK.Name = "simpleButton_OK";
            this.simpleButton_OK.Size = new System.Drawing.Size(66, 22);
            this.simpleButton_OK.StyleController = this.LayoutControl1;
            this.simpleButton_OK.TabIndex = 12;
            this.simpleButton_OK.Text = "&OK";
            // 
            // CheckEdit1
            // 
            this.CheckEdit1.EditValue = true;
            this.CheckEdit1.Location = new System.Drawing.Point(210, 84);
            this.CheckEdit1.Name = "CheckEdit1";
            this.CheckEdit1.Properties.Caption = "Start New Page";
            this.CheckEdit1.Properties.GlyphAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.CheckEdit1.Properties.ValueChecked = false;
            this.CheckEdit1.Properties.ValueUnchecked = true;
            this.CheckEdit1.Size = new System.Drawing.Size(157, 19);
            this.CheckEdit1.StyleController = this.LayoutControl1;
            this.CheckEdit1.TabIndex = 9;
            // 
            // AttributeGridControl1
            // 
            this.AttributeGridControl1.Location = new System.Drawing.Point(12, 124);
            this.AttributeGridControl1.Name = "AttributeGridControl1";
            this.AttributeGridControl1.Size = new System.Drawing.Size(355, 137);
            this.AttributeGridControl1.TabIndex = 15;
            // 
            // TextEdit2
            // 
            this.TextEdit2.Location = new System.Drawing.Point(136, 12);
            this.TextEdit2.Name = "TextEdit2";
            this.TextEdit2.Size = new System.Drawing.Size(231, 20);
            this.TextEdit2.StyleController = this.LayoutControl1;
            this.TextEdit2.TabIndex = 6;
            // 
            // ComboBoxEdit_UriType
            // 
            this.ComboBoxEdit_UriType.EditValue = "FILE://";
            this.ComboBoxEdit_UriType.Location = new System.Drawing.Point(64, 12);
            this.ComboBoxEdit_UriType.MaximumSize = new System.Drawing.Size(68, 0);
            this.ComboBoxEdit_UriType.MinimumSize = new System.Drawing.Size(68, 0);
            this.ComboBoxEdit_UriType.Name = "ComboBoxEdit_UriType";
            this.ComboBoxEdit_UriType.Properties.AllowMouseWheel = false;
            this.ComboBoxEdit_UriType.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.False;
            this.ComboBoxEdit_UriType.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.ComboBoxEdit_UriType.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
            this.ComboBoxEdit_UriType.Size = new System.Drawing.Size(68, 20);
            this.ComboBoxEdit_UriType.StyleController = this.LayoutControl1;
            this.ComboBoxEdit_UriType.TabIndex = 5;
            // 
            // SpinEdit1
            // 
            this.SpinEdit1.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.SpinEdit1.Location = new System.Drawing.Point(262, 60);
            this.SpinEdit1.Name = "SpinEdit1";
            this.SpinEdit1.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.SpinEdit1.Size = new System.Drawing.Size(105, 20);
            this.SpinEdit1.StyleController = this.LayoutControl1;
            this.SpinEdit1.TabIndex = 8;
            // 
            // ComboBoxEdit_Type
            // 
            this.ComboBoxEdit_Type.Location = new System.Drawing.Point(64, 84);
            this.ComboBoxEdit_Type.Name = "ComboBoxEdit_Type";
            this.ComboBoxEdit_Type.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.False;
            this.ComboBoxEdit_Type.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.ComboBoxEdit_Type.Properties.PopupSizeable = true;
            this.ComboBoxEdit_Type.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
            this.ComboBoxEdit_Type.Size = new System.Drawing.Size(142, 20);
            this.ComboBoxEdit_Type.StyleController = this.LayoutControl1;
            this.ComboBoxEdit_Type.TabIndex = 10;
            // 
            // LayoutControlGroup1
            // 
            this.LayoutControlGroup1.CustomizationFormText = "Root";
            this.LayoutControlGroup1.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.True;
            this.LayoutControlGroup1.GroupBordersVisible = false;
            this.LayoutControlGroup1.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.LayoutControlItem6,
            this.EmptySpaceItem1,
            this.EmptySpaceItem_right,
            this.LayoutControlItem8,
            this.EmptySpaceItem_left,
            this.LayoutControlItem10,
            this.LayoutControlItem7,
            this.LayoutControlItem5,
            this.LayoutControlItem2,
            this.LayoutControlItem3,
            this.LayoutControlItem1,
            this.layoutControlItem4,
            this.layoutControlItem9});
            this.LayoutControlGroup1.Location = new System.Drawing.Point(0, 0);
            this.LayoutControlGroup1.Name = "Root";
            this.LayoutControlGroup1.Size = new System.Drawing.Size(379, 312);
            this.LayoutControlGroup1.Text = "Root";
            this.LayoutControlGroup1.TextVisible = false;
            // 
            // LayoutControlItem6
            // 
            this.LayoutControlItem6.Control = this.CheckEdit1;
            this.LayoutControlItem6.CustomizationFormText = "LayoutControlItem6";
            this.LayoutControlItem6.Location = new System.Drawing.Point(198, 72);
            this.LayoutControlItem6.Name = "LayoutControlItem6";
            this.LayoutControlItem6.Size = new System.Drawing.Size(161, 24);
            this.LayoutControlItem6.Text = "LayoutControlItem6";
            this.LayoutControlItem6.TextSize = new System.Drawing.Size(0, 0);
            this.LayoutControlItem6.TextToControlDistance = 0;
            this.LayoutControlItem6.TextVisible = false;
            // 
            // EmptySpaceItem1
            // 
            this.EmptySpaceItem1.AllowHotTrack = false;
            this.EmptySpaceItem1.CustomizationFormText = "EmptySpaceItem1";
            this.EmptySpaceItem1.Location = new System.Drawing.Point(0, 253);
            this.EmptySpaceItem1.MaxSize = new System.Drawing.Size(0, 13);
            this.EmptySpaceItem1.MinSize = new System.Drawing.Size(10, 13);
            this.EmptySpaceItem1.Name = "EmptySpaceItem1";
            this.EmptySpaceItem1.Size = new System.Drawing.Size(359, 13);
            this.EmptySpaceItem1.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.EmptySpaceItem1.Text = "EmptySpaceItem1";
            this.EmptySpaceItem1.TextSize = new System.Drawing.Size(0, 0);
            // 
            // EmptySpaceItem_right
            // 
            this.EmptySpaceItem_right.AllowHotTrack = false;
            this.EmptySpaceItem_right.CustomizationFormText = "EmptySpaceItem2";
            this.EmptySpaceItem_right.Location = new System.Drawing.Point(215, 266);
            this.EmptySpaceItem_right.Name = "EmptySpaceItem_right";
            this.EmptySpaceItem_right.Size = new System.Drawing.Size(144, 26);
            this.EmptySpaceItem_right.Text = "EmptySpaceItem_right";
            this.EmptySpaceItem_right.TextSize = new System.Drawing.Size(0, 0);
            // 
            // LayoutControlItem8
            // 
            this.LayoutControlItem8.Control = this.simpleButton_OK;
            this.LayoutControlItem8.CustomizationFormText = "LayoutControlItem8";
            this.LayoutControlItem8.Location = new System.Drawing.Point(145, 266);
            this.LayoutControlItem8.Name = "LayoutControlItem8";
            this.LayoutControlItem8.Size = new System.Drawing.Size(70, 26);
            this.LayoutControlItem8.Text = "LayoutControlItem8";
            this.LayoutControlItem8.TextSize = new System.Drawing.Size(0, 0);
            this.LayoutControlItem8.TextToControlDistance = 0;
            this.LayoutControlItem8.TextVisible = false;
            // 
            // EmptySpaceItem_left
            // 
            this.EmptySpaceItem_left.AllowHotTrack = false;
            this.EmptySpaceItem_left.CustomizationFormText = "EmptySpaceItem3";
            this.EmptySpaceItem_left.Location = new System.Drawing.Point(0, 266);
            this.EmptySpaceItem_left.Name = "EmptySpaceItem_left";
            this.EmptySpaceItem_left.Size = new System.Drawing.Size(145, 26);
            this.EmptySpaceItem_left.Text = "EmptySpaceItem_left";
            this.EmptySpaceItem_left.TextSize = new System.Drawing.Size(0, 0);
            // 
            // LayoutControlItem10
            // 
            this.LayoutControlItem10.Control = this.ComboBoxEdit_Section;
            this.LayoutControlItem10.CustomizationFormText = "Section";
            this.LayoutControlItem10.Location = new System.Drawing.Point(0, 48);
            this.LayoutControlItem10.Name = "LayoutControlItem10";
            this.LayoutControlItem10.Size = new System.Drawing.Size(198, 24);
            this.LayoutControlItem10.Text = "Section";
            this.LayoutControlItem10.TextSize = new System.Drawing.Size(48, 13);
            // 
            // LayoutControlItem7
            // 
            this.LayoutControlItem7.Control = this.ComboBoxEdit_Type;
            this.LayoutControlItem7.CustomizationFormText = "type";
            this.LayoutControlItem7.Location = new System.Drawing.Point(0, 72);
            this.LayoutControlItem7.Name = "LayoutControlItem7";
            this.LayoutControlItem7.Size = new System.Drawing.Size(198, 24);
            this.LayoutControlItem7.Text = "type";
            this.LayoutControlItem7.TextSize = new System.Drawing.Size(48, 13);
            // 
            // LayoutControlItem5
            // 
            this.LayoutControlItem5.Control = this.SpinEdit1;
            this.LayoutControlItem5.CustomizationFormText = "Order on the report (0=first)";
            this.LayoutControlItem5.Location = new System.Drawing.Point(198, 48);
            this.LayoutControlItem5.Name = "LayoutControlItem5";
            this.LayoutControlItem5.Size = new System.Drawing.Size(161, 24);
            this.LayoutControlItem5.Text = "Order";
            this.LayoutControlItem5.TextSize = new System.Drawing.Size(48, 13);
            // 
            // LayoutControlItem2
            // 
            this.LayoutControlItem2.Control = this.ComboBoxEdit_UriType;
            this.LayoutControlItem2.CustomizationFormText = "URI";
            this.LayoutControlItem2.Location = new System.Drawing.Point(0, 0);
            this.LayoutControlItem2.Name = "LayoutControlItem2";
            this.LayoutControlItem2.Size = new System.Drawing.Size(124, 24);
            this.LayoutControlItem2.Text = "URI";
            this.LayoutControlItem2.TextSize = new System.Drawing.Size(48, 13);
            // 
            // LayoutControlItem3
            // 
            this.LayoutControlItem3.Control = this.TextEdit2;
            this.LayoutControlItem3.CustomizationFormText = "File Location";
            this.LayoutControlItem3.Location = new System.Drawing.Point(124, 0);
            this.LayoutControlItem3.Name = "LayoutControlItem3";
            this.LayoutControlItem3.Size = new System.Drawing.Size(235, 24);
            this.LayoutControlItem3.Text = "File Location";
            this.LayoutControlItem3.TextSize = new System.Drawing.Size(0, 0);
            this.LayoutControlItem3.TextToControlDistance = 0;
            this.LayoutControlItem3.TextVisible = false;
            // 
            // LayoutControlItem1
            // 
            this.LayoutControlItem1.Control = this.AttributeGridControl1;
            this.LayoutControlItem1.CustomizationFormText = "Attributes";
            this.LayoutControlItem1.Location = new System.Drawing.Point(0, 96);
            this.LayoutControlItem1.Name = "LayoutControlItem1";
            this.LayoutControlItem1.Size = new System.Drawing.Size(359, 157);
            this.LayoutControlItem1.Text = "Attributes";
            this.LayoutControlItem1.TextLocation = DevExpress.Utils.Locations.Top;
            this.LayoutControlItem1.TextSize = new System.Drawing.Size(48, 13);
            // 
            // dateEdit_EffectiveStartDate
            // 
            this.dateEdit_EffectiveStartDate.EditValue = null;
            this.dateEdit_EffectiveStartDate.Location = new System.Drawing.Point(64, 36);
            this.dateEdit_EffectiveStartDate.Name = "dateEdit_EffectiveStartDate";
            this.dateEdit_EffectiveStartDate.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dateEdit_EffectiveStartDate.Properties.VistaTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.dateEdit_EffectiveStartDate.Size = new System.Drawing.Size(142, 20);
            this.dateEdit_EffectiveStartDate.StyleController = this.LayoutControl1;
            this.dateEdit_EffectiveStartDate.TabIndex = 16;
            // 
            // layoutControlItem_simpleButton_Cancel
            // 
            this.layoutControlItem4.Control = this.dateEdit_EffectiveStartDate;
            this.layoutControlItem4.CustomizationFormText = "Effective";
            this.layoutControlItem4.Location = new System.Drawing.Point(0, 24);
            this.layoutControlItem4.Name = "layoutControlItem_simpleButton_Cancel";
            this.layoutControlItem4.Size = new System.Drawing.Size(198, 24);
            this.layoutControlItem4.Text = "Effective";
            this.layoutControlItem4.TextSize = new System.Drawing.Size(48, 13);
            // 
            // dateEdit_EffectiveExpireDate
            // 
            this.dateEdit_EffectiveExpireDate.EditValue = null;
            this.dateEdit_EffectiveExpireDate.Location = new System.Drawing.Point(262, 36);
            this.dateEdit_EffectiveExpireDate.Name = "dateEdit_EffectiveExpireDate";
            this.dateEdit_EffectiveExpireDate.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dateEdit_EffectiveExpireDate.Properties.VistaTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.dateEdit_EffectiveExpireDate.Size = new System.Drawing.Size(105, 20);
            this.dateEdit_EffectiveExpireDate.StyleController = this.LayoutControl1;
            this.dateEdit_EffectiveExpireDate.TabIndex = 17;
            // 
            // layoutControlItem9
            // 
            this.layoutControlItem9.Control = this.dateEdit_EffectiveExpireDate;
            this.layoutControlItem9.CustomizationFormText = "Expires";
            this.layoutControlItem9.Location = new System.Drawing.Point(198, 24);
            this.layoutControlItem9.Name = "layoutControlItem9";
            this.layoutControlItem9.Size = new System.Drawing.Size(161, 24);
            this.layoutControlItem9.Text = "Expires";
            this.layoutControlItem9.TextSize = new System.Drawing.Size(48, 13);
            // 
            // ComponentsForm
            // 
            this.AcceptButton = this.simpleButton_OK;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(379, 312);
            this.Controls.Add(this.LayoutControl1);
            this.Name = "ComponentsForm";
            this.Text = "Component";
            ((System.ComponentModel.ISupportInitialize)(this.DxErrorProvider1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LayoutControl1)).EndInit();
            this.LayoutControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.ComboBoxEdit_Section.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.CheckEdit1.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TextEdit2.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ComboBoxEdit_UriType.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.SpinEdit1.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ComboBoxEdit_Type.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LayoutControlGroup1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LayoutControlItem6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.EmptySpaceItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.EmptySpaceItem_right)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LayoutControlItem8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.EmptySpaceItem_left)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LayoutControlItem10)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LayoutControlItem7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LayoutControlItem5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LayoutControlItem2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LayoutControlItem3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LayoutControlItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEdit_EffectiveStartDate.Properties.VistaTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEdit_EffectiveStartDate.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEdit_EffectiveExpireDate.Properties.VistaTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEdit_EffectiveExpireDate.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem9)).EndInit();
            this.ResumeLayout(false);
        }

        private DevExpress.XtraLayout.LayoutControl LayoutControl1;
        private DevExpress.XtraEditors.ComboBoxEdit ComboBoxEdit_Section;
        private DevExpress.XtraEditors.SimpleButton simpleButton_OK;
        private DevExpress.XtraEditors.CheckEdit CheckEdit1;
        private DevExpress.XtraEditors.TextEdit TextEdit2;
        private DevExpress.XtraEditors.ComboBoxEdit ComboBoxEdit_UriType;
        private DevExpress.XtraEditors.SpinEdit SpinEdit1;
        private DevExpress.XtraLayout.LayoutControlGroup LayoutControlGroup1;
        private DevExpress.XtraLayout.LayoutControlItem LayoutControlItem2;
        private DevExpress.XtraLayout.LayoutControlItem LayoutControlItem3;
        private DevExpress.XtraLayout.LayoutControlItem LayoutControlItem6;
        private DevExpress.XtraLayout.EmptySpaceItem EmptySpaceItem1;
        private DevExpress.XtraLayout.EmptySpaceItem EmptySpaceItem_right;
        private DevExpress.XtraLayout.LayoutControlItem LayoutControlItem8;
        private DevExpress.XtraLayout.EmptySpaceItem EmptySpaceItem_left;
        private DevExpress.XtraLayout.LayoutControlItem LayoutControlItem10;
        private DevExpress.XtraLayout.LayoutControlItem LayoutControlItem7;
        private DevExpress.XtraLayout.LayoutControlItem LayoutControlItem5;
        private AttributeGridControl AttributeGridControl1;
        private DevExpress.XtraLayout.LayoutControlItem LayoutControlItem1;
        private DevExpress.XtraEditors.ComboBoxEdit ComboBoxEdit_Type;
        private DevExpress.XtraEditors.DateEdit dateEdit_EffectiveExpireDate;
        private DevExpress.XtraEditors.DateEdit dateEdit_EffectiveStartDate;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem4;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem9;
    }
}