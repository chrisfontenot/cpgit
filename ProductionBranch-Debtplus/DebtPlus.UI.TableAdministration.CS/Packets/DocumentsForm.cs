using DebtPlus.LINQ;

namespace DebtPlus.UI.TableAdministration.CS.Packets
{
    internal partial class DocumentsForm : DebtPlus.Data.Forms.DebtPlusForm
    {
        private BusinessContext bc = null;
        private Document documentRecord;

        /// <summary>
        /// Initialize the form class
        /// </summary>
        /// <param name="DatabaseInfo"></param>
        /// <param name="drv"></param>
        internal DocumentsForm()
            : base()
        {
            InitializeComponent();
        }

        internal DocumentsForm(BusinessContext bc, Document documentRecord)
            : this()
        {
            this.bc = bc;
            this.documentRecord = documentRecord;
            RegisterHandlers();
        }

        /// <summary>
        /// Register the event handlers
        /// </summary>
        private void RegisterHandlers()
        {
            this.Load += DocumentsForm_Load;
            SimpleButton1.Click += SimpleButton1_Click;
            LayoutControl1.Resize += LayoutControl1_Resize;
        }

        /// <summary>
        /// Remove the event handler registration
        /// </summary>
        private void UnRegisterHandlers()
        {
            this.Load -= DocumentsForm_Load;
            SimpleButton1.Click -= SimpleButton1_Click;
            LayoutControl1.Resize -= LayoutControl1_Resize;
        }

        /// <summary>
        /// Process the LOAD sequence for the form
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void DocumentsForm_Load(object sender, System.EventArgs e)
        {
            UnRegisterHandlers();
            try
            {
                TextEdit1.Text = documentRecord.description;
                ComponentGridControl1.ReadForm(bc, documentRecord);
            }
            finally
            {
                RegisterHandlers();
            }
        }

        /// <summary>
        /// Process the CLICK event on the OK button
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void SimpleButton1_Click(object sender, System.EventArgs e)
        {
            documentRecord.description = TextEdit1.Text.Trim();
            DialogResult = System.Windows.Forms.DialogResult.OK;
        }

        /// <summary>
        /// Handle the resize event to center the button
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void LayoutControl1_Resize(object sender, System.EventArgs e)
        {
            EmptySpaceItem_left.Width = (EmptySpaceItem_right.Width + EmptySpaceItem_left.Width) / 2;
        }
    }
}