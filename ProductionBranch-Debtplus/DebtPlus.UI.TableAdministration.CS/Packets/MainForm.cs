#region "Copyright 2000-2012 DebtPlus, L.L.C."
//{*******************************************************************}
//{                                                                   }
//{       DebtPlus Debt Management System                             }
//{                                                                   }
//{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
//{       ALL RIGHTS RESERVED                                         }
//{                                                                   }
//{   The entire contents of this file is protected by U.S. and       }
//{   International Copyright Laws. Unauthorized reproduction,        }
//{   reverse-engineering, and distribution of all or any portion of  }
//{   the code contained in this file is strictly prohibited and may  }
//{   result in severe civil and criminal penalties and will be       }
//{   prosecuted to the maximum extent possible under the law.        }
//{                                                                   }
//{   RESTRICTIONS                                                    }
//{                                                                   }
//{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
//{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
//{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
//{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
//{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
//{                                                                   }
//{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
//{   ADDITIONAL RESTRICTIONS.                                        }
//{                                                                   }
//{*******************************************************************}

#endregion "Copyright 2000-2012 DebtPlus, L.L.C."

using System;
using System.Linq;
using System.Windows.Forms;
using DebtPlus.LINQ;

namespace DebtPlus.UI.TableAdministration.CS.Packets
{
    public partial class MainForm : Templates.MainForm
    {
        private DebtPlus.LINQ.BusinessContext bc = new BusinessContext();
        private System.Collections.Generic.List<Document> colRecords = null;

        public MainForm()
            : base()
        {
            InitializeComponent();
            RegisterHandlers();
        }

        /// <summary>
        /// Register the event handlers
        /// </summary>
        private void RegisterHandlers()
        {
            this.Load += MainForm_Load;
        }

        /// <summary>
        /// Remove the event registrations
        /// </summary>
        private void UnRegisterHandlers()
        {
            this.Load -= MainForm_Load;
        }

        /// <summary>
        /// Process the load event on the form
        /// </summary>
        private void MainForm_Load(object sender, EventArgs e)
        {
            UnRegisterHandlers();
            try
            {
                // Do an eager loading of the referential structures
                var dlo = new System.Data.Linq.DataLoadOptions();
                dlo.LoadWith<Document>(d => d.DocumentComponents);
                dlo.LoadWith<DocumentComponent>(d => d.DocumentAttributes);
                bc.LoadOptions = dlo;

                // Read the list of retention events
                colRecords = bc.Documents.ToList();

                // Update the grid with the new record collection
                gridControl1.DataSource = colRecords;
                gridView1.BestFitColumns();
                gridView1.RefreshData();
            }
            catch (System.Data.SqlClient.SqlException ex)
            {
                // Handle a database error
                DebtPlus.UI.Common.ErrorHandling.HandleErrors(ex, "Error reading Documents table");
            }
            finally
            {
                // Restore the event servicers
                RegisterHandlers();
            }
        }

        /// <summary>
        /// Change the record in the collection
        /// </summary>
        /// <param name="obj"></param>
        protected override void UpdateRecord(object obj)
        {
            // Find the record to be used
            Document record = obj as Document;
            if (record == null)
            {
                return;
            }

            // Do the edit operation. If successful, update the database accordingly.
            using (var frm = new DocumentsForm(bc, record))
            {
                if (frm.ShowDialog() != System.Windows.Forms.DialogResult.OK)
                {
                    return;
                }

                bc.SubmitChanges();
                gridView1.RefreshData();
            }
        }

        /// <summary>
        /// Create a new record for the collection
        /// </summary>
        protected override void CreateRecord()
        {
            // Create a new (blank) record for the operation
            var record = DebtPlus.LINQ.Factory.Manufacture_Document();

            // Do the edit operation.
            using (var frm = new DocumentsForm(bc, record))
            {
                if (frm.ShowDialog() != DialogResult.OK)
                {
                    return;
                }
            }

            // Submit the changes to the database and update the list of records.
            if (record.Id < 1)
            {
                bc.Documents.InsertOnSubmit(record);
            }

            bc.SubmitChanges();
            colRecords.Add(record);
            gridControl1.RefreshDataSource();
            gridView1.RefreshData();
        }

        /// <summary>
        /// Delete the record from the collection
        /// </summary>
        /// <param name="obj"></param>
        protected override void DeleteRecord(object obj)
        {
            // Find the record to be used
            Document record = obj as Document;
            if (record == null)
            {
                return;
            }

            // Ensure that we want to delete the record
            if (DebtPlus.Data.Prompts.RequestConfirmation_Delete() != System.Windows.Forms.DialogResult.Yes)
            {
                return;
            }

            // Delete the record from the database
            bc.Documents.DeleteOnSubmit(record);
            bc.SubmitChanges();

            // Remove the record from the display collection
            colRecords.Remove(record);
            gridControl1.RefreshDataSource();
        }
    }
}