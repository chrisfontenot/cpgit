using System;
using System.Drawing;
using System.Windows.Forms;
using DebtPlus.LINQ;

namespace DebtPlus.UI.TableAdministration.CS.Packets
{
    internal partial class ComponentGridControl : System.Windows.Forms.UserControl
    {
        private BusinessContext bc = null;
        private Document documentRecord = null;
        private Int32 ControlRow = -1;

        internal ComponentGridControl()
            : base()
        {
            InitializeComponent();
        }

        /// <summary>
        /// Register the event handlers
        /// </summary>
        /// <remarks></remarks>
        private void RegisterHandlers()
        {
            PopupMenu1.Popup += PopupMenu_Items_Popup;
            BarButtonItem_Add.ItemClick += MenuItemCreate;
            BarButtonItem_Change.ItemClick += MenuItemEdit;
            BarButtonItem_Delete.ItemClick += MenuItemDelete;
            gridView1.MouseUp += gridView1_MouseUp;
            gridView1.MouseDown += gridView1_MouseDown;
            gridView1.DoubleClick += gridView1_DoubleClick;
        }

        /// <summary>
        /// Remove the event handler registration
        /// </summary>
        /// <remarks></remarks>
        private void UnRegisterHandlers()
        {
            PopupMenu1.Popup -= PopupMenu_Items_Popup;
            BarButtonItem_Add.ItemClick -= MenuItemCreate;
            BarButtonItem_Change.ItemClick -= MenuItemEdit;
            BarButtonItem_Delete.ItemClick -= MenuItemDelete;
            gridView1.MouseUp -= gridView1_MouseUp;
            gridView1.MouseDown -= gridView1_MouseDown;
            gridView1.DoubleClick -= gridView1_DoubleClick;
        }

        internal void ReadForm(BusinessContext bc, Document documentRecord)
        {
            UnRegisterHandlers();

            // Hook the column to the formatting routines
            gridColumn_ComponentLocationURI.DisplayFormat.Format = new URIFormatter();
            gridColumn_ComponentLocationURI.GroupFormat.Format = new URIFormatter();

            try
            {
                this.bc = bc;
                this.documentRecord = documentRecord;
                gridControl1.DataSource = documentRecord.DocumentComponents.GetNewBindingList();
                gridView1.BestFitColumns();
                gridView1.RefreshData();
            }
            finally
            {
                RegisterHandlers();
            }
        }

        /// <summary>
        /// Edit the current item
        /// </summary>
        private void UpdateRecord(object obj)
        {
            var record = obj as DocumentComponent;
            if (record != null)
            {
                // Just do the edit. The update will be done when the record is updated.
                using (var frm = new ComponentsForm(bc, record))
                {
                    if (frm.ShowDialog() != DialogResult.OK)
                    {
                        return;
                    }
                }
                gridControl1.DataSource = documentRecord.DocumentComponents.GetNewBindingList();
                gridView1.RefreshData();
            }
        }

        /// <summary>
        /// Create a new record
        /// </summary>
        /// <remarks></remarks>
        private void CreateRecord()
        {
            DocumentComponent record = DebtPlus.LINQ.Factory.Manufacture_DocumentComponent();
            using (var frm = new ComponentsForm(bc, record))
            {
                if (frm.ShowDialog() != DialogResult.OK)
                {
                    return;
                }
            }

            // Add the record to the component list and rebuild the grid
            documentRecord.DocumentComponents.Add(record);
            gridControl1.DataSource = documentRecord.DocumentComponents.GetNewBindingList();
            gridView1.RefreshData();
        }

        /// <summary>
        /// Delete the current item
        /// </summary>
        private void DeleteRecord(object obj)
        {
            var record = obj as DocumentComponent;
            if (record != null)
            {
                // Remove the record from the list and the database
                documentRecord.DocumentComponents.Remove(record);
                record.Document = null;
                record.DocumentID = default(Int32);

                // Refresh the display list
                gridControl1.DataSource = documentRecord.DocumentComponents.GetNewBindingList();
                gridView1.RefreshData();
            }
        }

        /// <summary>
        /// Process the EDIT button
        /// </summary>
        /// <remarks></remarks>
        private void EditButton()
        {
            if (ControlRow >= 0)
            {
                UpdateRecord(gridView1.GetRow(gridView1.FocusedRowHandle));
            }
        }

        /// <summary>
        /// Handle the condition where the popup menu was activated
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        /// <remarks></remarks>
        private void gridView1_MouseUp(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Right)
            {
                PopupMenu1.ShowPopup(Control.MousePosition);
            }
        }

        /// <summary>
        /// Handle the condition where the popup menu was activated
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        /// <remarks></remarks>
        protected virtual void PopupMenu_Items_Popup(object sender, EventArgs e)
        {
            BarButtonItem_Add.Enabled = true;
            BarButtonItem_Delete.Enabled = ControlRow >= 0;
            BarButtonItem_Change.Enabled = ControlRow >= 0;
        }

        /// <summary>
        /// Process a CREATE menu item choice
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        /// <remarks></remarks>
        protected void MenuItemCreate(object sender, EventArgs e)
        {
            CreateRecord();
        }

        /// <summary>
        /// Process an EDIT menu item choice
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        /// <remarks></remarks>
        protected void MenuItemEdit(object sender, EventArgs e)
        {
            if (ControlRow >= 0)
            {
                UpdateRecord(gridView1.GetRow(ControlRow));
            }
        }

        /// <summary>
        /// Process a DELETE menu item
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        /// <remarks></remarks>
        protected void MenuItemDelete(object sender, EventArgs e)
        {
            if (ControlRow >= 0)
            {
                DeleteRecord(gridView1.GetRow(ControlRow));
            }
        }

        /// <summary>
        /// Look for the mouse down on the grid control
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        /// <remarks></remarks>
        private void gridView1_MouseDown(object sender, MouseEventArgs e)
        {
            DevExpress.XtraGrid.Views.Grid.ViewInfo.GridHitInfo hi = gridView1.CalcHitInfo(new Point(e.X, e.Y));
            ControlRow = hi.IsValid && hi.InRow ? hi.RowHandle : -1;
        }

        /// <summary>
        /// Double Click on an item -- edit it
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        /// <remarks></remarks>
        private void gridView1_DoubleClick(object sender, EventArgs e)
        {
            // Find the row targeted as the double-click item
            DevExpress.XtraGrid.Views.Grid.GridView gv = (DevExpress.XtraGrid.Views.Grid.GridView)sender;
            DevExpress.XtraGrid.GridControl ctl = (DevExpress.XtraGrid.GridControl)gv.GridControl;
            DevExpress.XtraGrid.Views.Grid.ViewInfo.GridHitInfo hi = gv.CalcHitInfo((ctl.PointToClient(System.Windows.Forms.Control.MousePosition)));
            ControlRow = hi.RowHandle;

            if (ControlRow >= 0)
            {
                UpdateRecord(gridView1.GetRow(ControlRow));
            }
        }

        internal class URIFormatter : System.ICustomFormatter, System.IFormatProvider
        {
            internal URIFormatter()
            {
            }

            public object GetFormat(Type formatType)
            {
                return this;
            }

            public string Format(string format, object arg, IFormatProvider formatProvider)
            {
                string strItem = DebtPlus.Utils.Nulls.v_String(arg);
                if (!string.IsNullOrWhiteSpace(strItem))
                {
                    try
                    {
                        // Wash the input string through the URI parser
                        var uriValue = new System.Uri(strItem);

                        // If the URI is a file then return the file component to the path.
                        if (uriValue.IsFile)
                        {
                            return uriValue.LocalPath;
                        }
                        return uriValue.ToString();
                    }
                    catch
                    {
                    }
                }
                return strItem;
            }
        }
    }
}