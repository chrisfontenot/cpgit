using System;
using System.Linq;
using DebtPlus.LINQ;

namespace DebtPlus.UI.TableAdministration.CS.Packets
{
    internal partial class ComponentsForm : DebtPlus.Data.Forms.DebtPlusForm
    {
        private BusinessContext bc = null;
        private DocumentComponent record = null;

        /// <summary>
        /// Initialize the new instance of the form
        /// </summary>
        internal ComponentsForm()
            : base()
        {
            InitializeComponent();
        }

        /// <summary>
        /// Initialize the new instance of the form
        /// </summary>
        internal ComponentsForm(BusinessContext bc, DocumentComponent record)
            : this()
        {
            this.bc = bc;
            this.record = record;

            // Load the list of sections
            ComboBoxEdit_Section.Properties.Items.Clear();
            ComboBoxEdit_Section.Properties.Items.Add(new DebtPlus.Data.Controls.ComboboxItem("Header", "RPTHEAD"));
            ComboBoxEdit_Section.Properties.Items.Add(new DebtPlus.Data.Controls.ComboboxItem("Detail", "DETAIL"));
            ComboBoxEdit_Section.Properties.Items.Add(new DebtPlus.Data.Controls.ComboboxItem("Footer", "RPTFOOT"));

            // The types are one of two items
            ComboBoxEdit_Type.Properties.Items.Clear();
            ComboBoxEdit_Type.Properties.Items.Add(new DebtPlus.Data.Controls.ComboboxItem("Sub-Report", "RPT"));
            ComboBoxEdit_Type.Properties.Items.Add(new DebtPlus.Data.Controls.ComboboxItem("Text Document", "RTF"));
            ComboBoxEdit_Type.Properties.Items.Add(new DebtPlus.Data.Controls.ComboboxItem("Blank Space", "SPC"));

            ComboBoxEdit_UriType.Properties.Items.Clear();
            ComboBoxEdit_UriType.Properties.Items.Add(new DebtPlus.Data.Controls.ComboboxItem(System.Uri.UriSchemeFile, "FILE"));
            ComboBoxEdit_UriType.Properties.Items.Add(new DebtPlus.Data.Controls.ComboboxItem(System.Uri.UriSchemeHttp, "HTTP"));
            ComboBoxEdit_UriType.Properties.Items.Add(new DebtPlus.Data.Controls.ComboboxItem(System.Uri.UriSchemeHttps, "HTTPS"));

            // Register the event handlers
            RegisterHandlers();
        }

        /// <summary>
        /// Register the event handlers
        /// </summary>
        /// <remarks></remarks>
        private void RegisterHandlers()
        {
            Load += ComponentsForm_Load;
            simpleButton_OK.Click += simpleButton_OK_Click;
            CheckEdit1.EditValueChanged += FormChanged;
            ComboBoxEdit_Section.SelectedIndexChanged += FormChanged;
            ComboBoxEdit_Type.SelectedIndexChanged += FormChanged;
            SpinEdit1.EditValueChanged += FormChanged;
            TextEdit2.TextChanged += FormChanged;
            ComboBoxEdit_UriType.SelectedIndexChanged += FormChanged;
            LayoutControl1.Resize += LayoutControl1_Resize;
        }

        /// <summary>
        /// Remove the event handler registration
        /// </summary>
        /// <remarks></remarks>
        private void UnRegisterHandlers()
        {
            Load -= ComponentsForm_Load;
            simpleButton_OK.Click -= simpleButton_OK_Click;
            CheckEdit1.EditValueChanged -= FormChanged;
            ComboBoxEdit_Section.SelectedIndexChanged -= FormChanged;
            ComboBoxEdit_Type.SelectedIndexChanged -= FormChanged;
            SpinEdit1.EditValueChanged -= FormChanged;
            TextEdit2.TextChanged -= FormChanged;
            ComboBoxEdit_UriType.SelectedIndexChanged -= FormChanged;
            LayoutControl1.Resize -= LayoutControl1_Resize;
        }

        /// <summary>
        /// Return the appropriate index to the selected item in the combobox
        /// </summary>
        private Int32 FindCombo(DevExpress.XtraEditors.ComboBoxEdit ctl, string Current)
        {
            // We need a non-null value for the search to work.
            if (!string.IsNullOrWhiteSpace(Current))
            {
                // Find the selected item in the list of possible items
                var q = ctl.Properties.Items.OfType<DebtPlus.Data.Controls.ComboboxItem>().Where(s => string.Compare((string)s.value, Current, true) == 0).FirstOrDefault();
                if (q != null)
                {
                    return ctl.Properties.Items.IndexOf(q);
                }
            }

            return -1;
        }

        /// <summary>
        /// Process the load sequence for the form
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        /// <remarks></remarks>
        private void ComponentsForm_Load(object sender, System.EventArgs e)
        {
            UnRegisterHandlers();
            try
            {
                ComboBoxEdit_Section.SelectedIndex = FindCombo(ComboBoxEdit_Section, record.SectionID);
                ComboBoxEdit_Type.SelectedIndex = FindCombo(ComboBoxEdit_Type, record.ComponentType);

                // Bind the other fields
                SpinEdit1.EditValue = record.SequenceID;
                CheckEdit1.Checked = record.PageEjectBeforeFLG;
                dateEdit_EffectiveExpireDate.EditValue = record.EffectiveExpireDate;
                dateEdit_EffectiveStartDate.EditValue = record.EffectiveStartDate;

                // Split the URI into the component pieces
                string uriType = string.Empty;
                string uriLocation = string.Empty;

                // If there is a URI then split the fields into the two items
                if (!string.IsNullOrWhiteSpace(record.ComponentLocationURI))
                {
                    try
                    {
                        System.Uri uri = new System.Uri(record.ComponentLocationURI);
                        uriType = uri.Scheme;
                        uriLocation = System.Uri.UnescapeDataString(uri.AbsolutePath);
                    }
                    catch { }
                }

                // Put the two fields into the controls. These are not bound.
                TextEdit2.EditValue = uriLocation;
                ComboBoxEdit_UriType.SelectedIndex = FindCombo(ComboBoxEdit_UriType, uriType);
                if (ComboBoxEdit_UriType.SelectedIndex < 0)
                {
                    ComboBoxEdit_UriType.SelectedIndex = FindCombo(ComboBoxEdit_UriType, "FILE://");
                }

                // Initialize the attribute form
                AttributeGridControl1.ReadForm(bc, record);

                // Look for errors at this time.
                simpleButton_OK.Enabled = !HasErrors();
            }
            finally
            {
                RegisterHandlers();
            }
        }

        /// <summary>
        /// Process the CLICK event on the OK button.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        /// <remarks></remarks>
        private void simpleButton_OK_Click(object sender, System.EventArgs e)
        {
            UnRegisterHandlers();

            try
            {
                // Set the combobox values into the record. They are not bound.
                record.SectionID = (string)((DebtPlus.Data.Controls.ComboboxItem)ComboBoxEdit_Section.SelectedItem).value;
                record.ComponentType = (string)((DebtPlus.Data.Controls.ComboboxItem)ComboBoxEdit_Type.SelectedItem).value;
                record.SequenceID = DebtPlus.Utils.Nulls.v_Int32(SpinEdit1.EditValue).GetValueOrDefault();

                // Merge the two controls into the URI
                if (TextEdit2.Text.Trim() == string.Empty)
                {
                    record.ComponentLocationURI = null;
                }
                else
                {
                    string uriScheme = (string)((DebtPlus.Data.Controls.ComboboxItem)ComboBoxEdit_UriType.SelectedItem).value;
                    string uriPath = TextEdit2.Text.Trim();

                    // The spacing function does not have a URI path. The parameters are in the path itself.
                    if (string.Compare(record.ComponentType, "SPC", true) == 0)
                    {
                        record.ComponentLocationURI = TextEdit2.Text.Trim();
                    }
                    else
                    {
                        var bld = new System.UriBuilder();
                        bld.Host = string.Empty;
                        bld.Scheme = uriScheme;

                        // Split the path into the parameters and the pathname.
                        string strParameters = string.Empty;
                        string strPath = string.Empty;
                        System.Text.RegularExpressions.Match m = System.Text.RegularExpressions.Regex.Match(strPath, @"([^;]+);(.*)");
                        if (m.Success)
                        {
                            // If there are parameters then we need to remove the parameters before we build the URI. Add them back afterwards.
                            bld.Path = m.Captures[1].Value;
                            record.ComponentLocationURI = bld.ToString() + ";" + m.Captures[2].Value;
                        }
                        else
                        {
                            // There are no parameters. Just use the string as it is.
                            bld.Path = uriPath;
                            record.ComponentLocationURI = bld.ToString();
                        }
                    }
                }

                // Set the revision date
                record.RevisionDate = System.DateTime.Now;

                // Set the effective and expiration dates
                record.EffectiveStartDate = DebtPlus.Utils.Nulls.v_DateTime(dateEdit_EffectiveStartDate.EditValue).GetValueOrDefault().Date;
                record.EffectiveExpireDate = DebtPlus.Utils.Nulls.v_DateTime(dateEdit_EffectiveExpireDate.EditValue);
            }
            finally
            {
                RegisterHandlers();
            }
        }

        /// <summary>
        /// When the form is changed, enable or disable the OK button.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        /// <remarks></remarks>
        private void FormChanged(object sender, System.EventArgs e)
        {
            simpleButton_OK.Enabled = !HasErrors();
        }

        /// <summary>
        /// Determine if the input form has an error condition
        /// </summary>
        /// <returns></returns>
        /// <remarks></remarks>
        private bool HasErrors()
        {
            // Look to ensure that there are no error conditions pending on the input form
            if (ComboBoxEdit_Section.SelectedIndex < 0)
            {
                return true;
            }

            if (ComboBoxEdit_Type.SelectedIndex < 0)
            {
                return true;
            }

            if (ComboBoxEdit_UriType.SelectedIndex < 0)
            {
                return true;
            }

            if (SpinEdit1.Text.Trim() == string.Empty)
            {
                return true;
            }

            if (CheckEdit1.EditValue == null)
            {
                return true;
            }

            if (TextEdit2.Text.Trim() == string.Empty)
            {
                return true;
            }

            return false;
        }

        /// <summary>
        /// Center the button when the control is resized
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        /// <remarks></remarks>
        private void LayoutControl1_Resize(object sender, System.EventArgs e)
        {
            EmptySpaceItem_left.Width = (EmptySpaceItem_left.Width + EmptySpaceItem_right.Width) / 2;
        }
    }
}