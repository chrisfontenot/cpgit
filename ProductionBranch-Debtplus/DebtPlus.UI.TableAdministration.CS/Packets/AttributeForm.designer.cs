﻿using Microsoft.VisualBasic;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Diagnostics;

namespace DebtPlus.UI.TableAdministration.CS.Packets
{
    partial class AttributeForm
    {
        //Form overrides dispose to clean up the component list.
        [System.Diagnostics.DebuggerNonUserCode]
        protected override void Dispose(bool disposing)
        {
            if (disposing && components != null)
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        //Required by the Windows Form Designer
        private System.ComponentModel.IContainer components = null;

        //NOTE: The following procedure is required by the Windows Form Designer
        //It can be modified using the Windows Form Designer.  
        //Do not modify it using the code editor.
        [System.Diagnostics.DebuggerStepThrough]
        private void InitializeComponent()
        {
            this.LayoutControl1 = new DevExpress.XtraLayout.LayoutControl();
            this.CheckEdit3 = new DevExpress.XtraEditors.CheckEdit();
            this.CheckEdit_all_languages = new DevExpress.XtraEditors.CheckEdit();
            this.CheckEdit2 = new DevExpress.XtraEditors.CheckEdit();
            this.CheckEdit_all_states = new DevExpress.XtraEditors.CheckEdit();
            this.SimpleButton1 = new DevExpress.XtraEditors.SimpleButton();
            this.LookUpEdit_State = new DevExpress.XtraEditors.LookUpEdit();
            this.LookUpEdit_language = new DevExpress.XtraEditors.LookUpEdit();
            this.LayoutControlGroup1 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.LayoutControlItem3 = new DevExpress.XtraLayout.LayoutControlItem();
            this.LayoutControlGroup2 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.LayoutControlItem2 = new DevExpress.XtraLayout.LayoutControlItem();
            this.LayoutControlItem5 = new DevExpress.XtraLayout.LayoutControlItem();
            this.LayoutControlItem4 = new DevExpress.XtraLayout.LayoutControlItem();
            this.LayoutControlGroup3 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.LayoutControlItem6 = new DevExpress.XtraLayout.LayoutControlItem();
            this.LayoutControlItem7 = new DevExpress.XtraLayout.LayoutControlItem();
            this.LayoutControlItem1 = new DevExpress.XtraLayout.LayoutControlItem();
            this.EmptySpaceItem1 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.EmptySpaceItem_left = new DevExpress.XtraLayout.EmptySpaceItem();
            this.EmptySpaceItem_right = new DevExpress.XtraLayout.EmptySpaceItem();
            ((System.ComponentModel.ISupportInitialize)(this.DxErrorProvider1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LayoutControl1)).BeginInit();
            this.LayoutControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.CheckEdit3.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.CheckEdit_all_languages.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.CheckEdit2.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.CheckEdit_all_states.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LookUpEdit_State.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LookUpEdit_language.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LayoutControlGroup1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LayoutControlItem3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LayoutControlGroup2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LayoutControlItem2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LayoutControlItem5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LayoutControlItem4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LayoutControlGroup3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LayoutControlItem6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LayoutControlItem7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LayoutControlItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.EmptySpaceItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.EmptySpaceItem_left)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.EmptySpaceItem_right)).BeginInit();
            this.SuspendLayout();
            // 
            // LayoutControl1
            // 
            this.LayoutControl1.Controls.Add(this.CheckEdit3);
            this.LayoutControl1.Controls.Add(this.CheckEdit_all_languages);
            this.LayoutControl1.Controls.Add(this.CheckEdit2);
            this.LayoutControl1.Controls.Add(this.CheckEdit_all_states);
            this.LayoutControl1.Controls.Add(this.SimpleButton1);
            this.LayoutControl1.Controls.Add(this.LookUpEdit_State);
            this.LayoutControl1.Controls.Add(this.LookUpEdit_language);
            this.LayoutControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.LayoutControl1.Location = new System.Drawing.Point(0, 0);
            this.LayoutControl1.Name = "LayoutControl1";
            this.LayoutControl1.Root = this.LayoutControlGroup1;
            this.LayoutControl1.Size = new System.Drawing.Size(314, 288);
            this.LayoutControl1.TabIndex = 0;
            this.LayoutControl1.Text = "LayoutControl1";
            // 
            // CheckEdit3
            // 
            this.CheckEdit3.Location = new System.Drawing.Point(25, 182);
            this.CheckEdit3.Name = "CheckEdit3";
            this.CheckEdit3.Properties.Caption = "Or specific Language";
            this.CheckEdit3.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Radio;
            this.CheckEdit3.Properties.RadioGroupIndex = 2;
            this.CheckEdit3.Size = new System.Drawing.Size(264, 19);
            this.CheckEdit3.StyleController = this.LayoutControl1;
            this.CheckEdit3.TabIndex = 10;
            this.CheckEdit3.TabStop = false;
            // 
            // CheckEdit_all_languages
            // 
            this.CheckEdit_all_languages.Location = new System.Drawing.Point(25, 159);
            this.CheckEdit_all_languages.Name = "CheckEdit_all_languages";
            this.CheckEdit_all_languages.Properties.Caption = "All Languages";
            this.CheckEdit_all_languages.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Radio;
            this.CheckEdit_all_languages.Properties.RadioGroupIndex = 2;
            this.CheckEdit_all_languages.Size = new System.Drawing.Size(264, 19);
            this.CheckEdit_all_languages.StyleController = this.LayoutControl1;
            this.CheckEdit_all_languages.TabIndex = 9;
            this.CheckEdit_all_languages.TabStop = false;
            // 
            // CheckEdit2
            // 
            this.CheckEdit2.Location = new System.Drawing.Point(25, 67);
            this.CheckEdit2.Name = "CheckEdit2";
            this.CheckEdit2.Properties.Caption = "Or specific State";
            this.CheckEdit2.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Radio;
            this.CheckEdit2.Properties.RadioGroupIndex = 1;
            this.CheckEdit2.Size = new System.Drawing.Size(264, 19);
            this.CheckEdit2.StyleController = this.LayoutControl1;
            this.CheckEdit2.TabIndex = 8;
            this.CheckEdit2.TabStop = false;
            // 
            // CheckEdit_all_states
            // 
            this.CheckEdit_all_states.Location = new System.Drawing.Point(25, 44);
            this.CheckEdit_all_states.Name = "CheckEdit_all_states";
            this.CheckEdit_all_states.Properties.Caption = "All States";
            this.CheckEdit_all_states.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Radio;
            this.CheckEdit_all_states.Properties.RadioGroupIndex = 1;
            this.CheckEdit_all_states.Size = new System.Drawing.Size(264, 19);
            this.CheckEdit_all_states.StyleController = this.LayoutControl1;
            this.CheckEdit_all_states.TabIndex = 7;
            this.CheckEdit_all_states.TabStop = false;
            // 
            // SimpleButton1
            // 
            this.SimpleButton1.Location = new System.Drawing.Point(124, 254);
            this.SimpleButton1.MaximumSize = new System.Drawing.Size(66, 22);
            this.SimpleButton1.MinimumSize = new System.Drawing.Size(66, 22);
            this.SimpleButton1.Name = "SimpleButton1";
            this.SimpleButton1.Size = new System.Drawing.Size(66, 22);
            this.SimpleButton1.StyleController = this.LayoutControl1;
            this.SimpleButton1.TabIndex = 6;
            this.SimpleButton1.Text = "&OK";
            // 
            // LookUpEdit_State
            // 
            this.LookUpEdit_State.Location = new System.Drawing.Point(25, 90);
            this.LookUpEdit_State.Name = "LookUpEdit_State";
            this.LookUpEdit_State.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.True;
            this.LookUpEdit_State.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LookUpEdit_State.Properties.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("Id", "State", 20, DevExpress.Utils.FormatType.Numeric, "f0", false, DevExpress.Utils.HorzAlignment.Far, DevExpress.Data.ColumnSortOrder.None, DevExpress.Utils.DefaultBoolean.True),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("Name", "Name", 20, DevExpress.Utils.FormatType.None, "", true, DevExpress.Utils.HorzAlignment.Near, DevExpress.Data.ColumnSortOrder.Ascending, DevExpress.Utils.DefaultBoolean.True)});
            this.LookUpEdit_State.Properties.DisplayMember = "Name";
            this.LookUpEdit_State.Properties.NullText = "";
            this.LookUpEdit_State.Properties.ShowFooter = false;
            this.LookUpEdit_State.Properties.ShowHeader = false;
            this.LookUpEdit_State.Properties.ValueMember = "Id";
            this.LookUpEdit_State.Size = new System.Drawing.Size(264, 20);
            this.LookUpEdit_State.StyleController = this.LayoutControl1;
            this.LookUpEdit_State.TabIndex = 5;
            // 
            // LookUpEdit_language
            // 
            this.LookUpEdit_language.Location = new System.Drawing.Point(25, 205);
            this.LookUpEdit_language.Name = "LookUpEdit_language";
            this.LookUpEdit_language.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LookUpEdit_language.Properties.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("Id", "ID", 20, DevExpress.Utils.FormatType.Numeric, "f0", false, DevExpress.Utils.HorzAlignment.Default),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("Attribute", "Attribute", 20, DevExpress.Utils.FormatType.None, "", true, DevExpress.Utils.HorzAlignment.Default, DevExpress.Data.ColumnSortOrder.Ascending)});
            this.LookUpEdit_language.Properties.DisplayMember = "Attribute";
            this.LookUpEdit_language.Properties.NullText = "";
            this.LookUpEdit_language.Properties.ShowFooter = false;
            this.LookUpEdit_language.Properties.ShowHeader = false;
            this.LookUpEdit_language.Properties.ValueMember = "Id";
            this.LookUpEdit_language.Size = new System.Drawing.Size(264, 20);
            this.LookUpEdit_language.StyleController = this.LayoutControl1;
            this.LookUpEdit_language.TabIndex = 4;
            // 
            // LayoutControlGroup1
            // 
            this.LayoutControlGroup1.CustomizationFormText = "LayoutControlGroup1";
            this.LayoutControlGroup1.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.True;
            this.LayoutControlGroup1.GroupBordersVisible = false;
            this.LayoutControlGroup1.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.LayoutControlItem3,
            this.LayoutControlGroup2,
            this.LayoutControlGroup3,
            this.EmptySpaceItem1,
            this.EmptySpaceItem_left,
            this.EmptySpaceItem_right});
            this.LayoutControlGroup1.Location = new System.Drawing.Point(0, 0);
            this.LayoutControlGroup1.Name = "LayoutControlGroup1";
            this.LayoutControlGroup1.Size = new System.Drawing.Size(314, 288);
            this.LayoutControlGroup1.Text = "LayoutControlGroup1";
            this.LayoutControlGroup1.TextVisible = false;
            // 
            // LayoutControlItem3
            // 
            this.LayoutControlItem3.Control = this.SimpleButton1;
            this.LayoutControlItem3.CustomizationFormText = "LayoutControlItem3";
            this.LayoutControlItem3.Location = new System.Drawing.Point(112, 242);
            this.LayoutControlItem3.Name = "LayoutControlItem3";
            this.LayoutControlItem3.Size = new System.Drawing.Size(70, 26);
            this.LayoutControlItem3.Text = "LayoutControlItem3";
            this.LayoutControlItem3.TextSize = new System.Drawing.Size(0, 0);
            this.LayoutControlItem3.TextToControlDistance = 0;
            this.LayoutControlItem3.TextVisible = false;
            // 
            // LayoutControlGroup2
            // 
            this.LayoutControlGroup2.CustomizationFormText = "State Information";
            this.LayoutControlGroup2.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.LayoutControlItem2,
            this.LayoutControlItem5,
            this.LayoutControlItem4});
            this.LayoutControlGroup2.Location = new System.Drawing.Point(0, 0);
            this.LayoutControlGroup2.Name = "LayoutControlGroup2";
            this.LayoutControlGroup2.Size = new System.Drawing.Size(294, 115);
            this.LayoutControlGroup2.Text = "State Information";
            // 
            // LayoutControlItem2
            // 
            this.LayoutControlItem2.Control = this.LookUpEdit_State;
            this.LayoutControlItem2.CustomizationFormText = "LayoutControlItem2";
            this.LayoutControlItem2.Location = new System.Drawing.Point(0, 46);
            this.LayoutControlItem2.Name = "LayoutControlItem2";
            this.LayoutControlItem2.Size = new System.Drawing.Size(268, 24);
            this.LayoutControlItem2.Text = "LayoutControlItem2";
            this.LayoutControlItem2.TextSize = new System.Drawing.Size(0, 0);
            this.LayoutControlItem2.TextToControlDistance = 0;
            this.LayoutControlItem2.TextVisible = false;
            // 
            // LayoutControlItem5
            // 
            this.LayoutControlItem5.Control = this.CheckEdit2;
            this.LayoutControlItem5.CustomizationFormText = "LayoutControlItem5";
            this.LayoutControlItem5.Location = new System.Drawing.Point(0, 23);
            this.LayoutControlItem5.Name = "LayoutControlItem5";
            this.LayoutControlItem5.Size = new System.Drawing.Size(268, 23);
            this.LayoutControlItem5.Text = "LayoutControlItem5";
            this.LayoutControlItem5.TextSize = new System.Drawing.Size(0, 0);
            this.LayoutControlItem5.TextToControlDistance = 0;
            this.LayoutControlItem5.TextVisible = false;
            // 
            // LayoutControlItem4
            // 
            this.LayoutControlItem4.Control = this.CheckEdit_all_states;
            this.LayoutControlItem4.CustomizationFormText = "LayoutControlItem4";
            this.LayoutControlItem4.Location = new System.Drawing.Point(0, 0);
            this.LayoutControlItem4.Name = "LayoutControlItem4";
            this.LayoutControlItem4.Size = new System.Drawing.Size(268, 23);
            this.LayoutControlItem4.Text = "LayoutControlItem4";
            this.LayoutControlItem4.TextSize = new System.Drawing.Size(0, 0);
            this.LayoutControlItem4.TextToControlDistance = 0;
            this.LayoutControlItem4.TextVisible = false;
            // 
            // LayoutControlGroup3
            // 
            this.LayoutControlGroup3.CustomizationFormText = "Language Information";
            this.LayoutControlGroup3.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.LayoutControlItem6,
            this.LayoutControlItem7,
            this.LayoutControlItem1});
            this.LayoutControlGroup3.Location = new System.Drawing.Point(0, 115);
            this.LayoutControlGroup3.Name = "LayoutControlGroup3";
            this.LayoutControlGroup3.Size = new System.Drawing.Size(294, 115);
            this.LayoutControlGroup3.Text = "Language Information";
            // 
            // LayoutControlItem6
            // 
            this.LayoutControlItem6.Control = this.CheckEdit_all_languages;
            this.LayoutControlItem6.CustomizationFormText = "LayoutControlItem6";
            this.LayoutControlItem6.Location = new System.Drawing.Point(0, 0);
            this.LayoutControlItem6.Name = "LayoutControlItem6";
            this.LayoutControlItem6.Size = new System.Drawing.Size(268, 23);
            this.LayoutControlItem6.Text = "LayoutControlItem6";
            this.LayoutControlItem6.TextSize = new System.Drawing.Size(0, 0);
            this.LayoutControlItem6.TextToControlDistance = 0;
            this.LayoutControlItem6.TextVisible = false;
            // 
            // LayoutControlItem7
            // 
            this.LayoutControlItem7.Control = this.CheckEdit3;
            this.LayoutControlItem7.CustomizationFormText = "LayoutControlItem7";
            this.LayoutControlItem7.Location = new System.Drawing.Point(0, 23);
            this.LayoutControlItem7.Name = "LayoutControlItem7";
            this.LayoutControlItem7.Size = new System.Drawing.Size(268, 23);
            this.LayoutControlItem7.Text = "LayoutControlItem7";
            this.LayoutControlItem7.TextSize = new System.Drawing.Size(0, 0);
            this.LayoutControlItem7.TextToControlDistance = 0;
            this.LayoutControlItem7.TextVisible = false;
            // 
            // LayoutControlItem1
            // 
            this.LayoutControlItem1.Control = this.LookUpEdit_language;
            this.LayoutControlItem1.CustomizationFormText = "LayoutControlItem1";
            this.LayoutControlItem1.Location = new System.Drawing.Point(0, 46);
            this.LayoutControlItem1.Name = "LayoutControlItem1";
            this.LayoutControlItem1.Size = new System.Drawing.Size(268, 24);
            this.LayoutControlItem1.Text = "LayoutControlItem1";
            this.LayoutControlItem1.TextSize = new System.Drawing.Size(0, 0);
            this.LayoutControlItem1.TextToControlDistance = 0;
            this.LayoutControlItem1.TextVisible = false;
            // 
            // EmptySpaceItem1
            // 
            this.EmptySpaceItem1.AllowHotTrack = false;
            this.EmptySpaceItem1.CustomizationFormText = "EmptySpaceItem1";
            this.EmptySpaceItem1.Location = new System.Drawing.Point(0, 230);
            this.EmptySpaceItem1.Name = "EmptySpaceItem1";
            this.EmptySpaceItem1.Size = new System.Drawing.Size(294, 12);
            this.EmptySpaceItem1.Text = "EmptySpaceItem1";
            this.EmptySpaceItem1.TextSize = new System.Drawing.Size(0, 0);
            // 
            // EmptySpaceItem_left
            // 
            this.EmptySpaceItem_left.AllowHotTrack = false;
            this.EmptySpaceItem_left.CustomizationFormText = "EmptySpaceItem_left";
            this.EmptySpaceItem_left.Location = new System.Drawing.Point(0, 242);
            this.EmptySpaceItem_left.Name = "EmptySpaceItem_left";
            this.EmptySpaceItem_left.Size = new System.Drawing.Size(112, 26);
            this.EmptySpaceItem_left.Text = "EmptySpaceItem_left";
            this.EmptySpaceItem_left.TextSize = new System.Drawing.Size(0, 0);
            // 
            // EmptySpaceItem_right
            // 
            this.EmptySpaceItem_right.AllowHotTrack = false;
            this.EmptySpaceItem_right.CustomizationFormText = "EmptySpaceItem_right";
            this.EmptySpaceItem_right.Location = new System.Drawing.Point(182, 242);
            this.EmptySpaceItem_right.Name = "EmptySpaceItem_right";
            this.EmptySpaceItem_right.Size = new System.Drawing.Size(112, 26);
            this.EmptySpaceItem_right.Text = "EmptySpaceItem_right";
            this.EmptySpaceItem_right.TextSize = new System.Drawing.Size(0, 0);
            // 
            // AttributeForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(314, 288);
            this.Controls.Add(this.LayoutControl1);
            this.Name = "AttributeForm";
            this.Text = "Attribute";
            ((System.ComponentModel.ISupportInitialize)(this.DxErrorProvider1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LayoutControl1)).EndInit();
            this.LayoutControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.CheckEdit3.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.CheckEdit_all_languages.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.CheckEdit2.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.CheckEdit_all_states.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LookUpEdit_State.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LookUpEdit_language.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LayoutControlGroup1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LayoutControlItem3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LayoutControlGroup2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LayoutControlItem2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LayoutControlItem5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LayoutControlItem4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LayoutControlGroup3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LayoutControlItem6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LayoutControlItem7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LayoutControlItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.EmptySpaceItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.EmptySpaceItem_left)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.EmptySpaceItem_right)).EndInit();
            this.ResumeLayout(false);

        }
        private DevExpress.XtraLayout.LayoutControl LayoutControl1;
        private DevExpress.XtraLayout.LayoutControlGroup LayoutControlGroup1;
        private DevExpress.XtraEditors.LookUpEdit LookUpEdit_language;
        private DevExpress.XtraLayout.LayoutControlItem LayoutControlItem1;
        private DevExpress.XtraEditors.SimpleButton SimpleButton1;
        private DevExpress.XtraEditors.LookUpEdit LookUpEdit_State;
        private DevExpress.XtraLayout.LayoutControlItem LayoutControlItem2;
        private DevExpress.XtraLayout.LayoutControlItem LayoutControlItem3;
        private DevExpress.XtraEditors.CheckEdit CheckEdit2;
        private DevExpress.XtraEditors.CheckEdit CheckEdit_all_states;
        private DevExpress.XtraLayout.LayoutControlGroup LayoutControlGroup2;
        private DevExpress.XtraLayout.LayoutControlItem LayoutControlItem5;
        private DevExpress.XtraLayout.LayoutControlItem LayoutControlItem4;
        private DevExpress.XtraEditors.CheckEdit CheckEdit3;
        private DevExpress.XtraEditors.CheckEdit CheckEdit_all_languages;
        private DevExpress.XtraLayout.LayoutControlItem LayoutControlItem6;
        private DevExpress.XtraLayout.LayoutControlItem LayoutControlItem7;
        private DevExpress.XtraLayout.LayoutControlGroup LayoutControlGroup3;
        private DevExpress.XtraLayout.EmptySpaceItem EmptySpaceItem1;
        private DevExpress.XtraLayout.EmptySpaceItem EmptySpaceItem_left;
        private DevExpress.XtraLayout.EmptySpaceItem EmptySpaceItem_right;
    }
}