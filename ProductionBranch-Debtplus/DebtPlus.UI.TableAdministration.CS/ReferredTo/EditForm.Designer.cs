﻿namespace DebtPlus.UI.TableAdministration.CS.ReferredTo
{
    partial class EditForm
    {
		//Form overrides dispose to clean up the component list.
		[System.Diagnostics.DebuggerNonUserCode]
		protected override void Dispose(bool disposing)
		{
			try 
            {
				if (disposing)
                {
                    if (components != null) components.Dispose();
				}
                components = null;
            }
    	    finally
            {
				base.Dispose(disposing);
			}
		}

		//Required by the Windows Form Designer
		private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.LabelControl_notes = new DevExpress.XtraEditors.LabelControl();
            this.TextEdit_notes = new DevExpress.XtraEditors.TextEdit();
            this.TextEdit_description = new DevExpress.XtraEditors.TextEdit();
            this.LabelControl_description = new DevExpress.XtraEditors.LabelControl();
            this.LabelControl_ID = new DevExpress.XtraEditors.LabelControl();
            this.LabelControl1 = new DevExpress.XtraEditors.LabelControl();
            ((System.ComponentModel.ISupportInitialize)(this.DxErrorProvider1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TextEdit_notes.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TextEdit_description.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // simpleButton_Cancel
            // 
            this.simpleButton_Cancel.Location = new System.Drawing.Point(300, 67);
            // 
            // simpleButton_OK
            // 
            this.simpleButton_OK.Location = new System.Drawing.Point(300, 35);
            // 
            // DefaultLookAndFeel1
            // 
            this.DefaultLookAndFeel1.LookAndFeel.SkinName = "McSkin";
            // 
            // LabelControl_notes
            // 
            this.LabelControl_notes.Location = new System.Drawing.Point(11, 61);
            this.LabelControl_notes.Name = "LabelControl_notes";
            this.LabelControl_notes.Size = new System.Drawing.Size(28, 13);
            this.LabelControl_notes.TabIndex = 26;
            this.LabelControl_notes.Text = "Notes";
            // 
            // TextEdit_notes
            // 
            this.TextEdit_notes.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.TextEdit_notes.Location = new System.Drawing.Point(84, 58);
            this.TextEdit_notes.Name = "TextEdit_notes";
            this.TextEdit_notes.Properties.MaxLength = 50;
            this.TextEdit_notes.Size = new System.Drawing.Size(193, 20);
            this.TextEdit_notes.TabIndex = 27;
            // 
            // TextEdit_description
            // 
            this.TextEdit_description.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.TextEdit_description.Location = new System.Drawing.Point(84, 32);
            this.TextEdit_description.Name = "TextEdit_description";
            this.TextEdit_description.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.False;
            this.TextEdit_description.Properties.MaxLength = 50;
            this.TextEdit_description.Size = new System.Drawing.Size(193, 20);
            this.TextEdit_description.TabIndex = 25;
            // 
            // LabelControl_description
            // 
            this.LabelControl_description.Location = new System.Drawing.Point(11, 35);
            this.LabelControl_description.Name = "LabelControl_description";
            this.LabelControl_description.Size = new System.Drawing.Size(53, 13);
            this.LabelControl_description.TabIndex = 24;
            this.LabelControl_description.Text = "Description";
            // 
            // LabelControl_ID
            // 
            this.LabelControl_ID.Location = new System.Drawing.Point(84, 13);
            this.LabelControl_ID.Name = "LabelControl_ID";
            this.LabelControl_ID.Size = new System.Drawing.Size(0, 13);
            this.LabelControl_ID.TabIndex = 23;
            // 
            // LabelControl1
            // 
            this.LabelControl1.Location = new System.Drawing.Point(11, 13);
            this.LabelControl1.Name = "LabelControl1";
            this.LabelControl1.Size = new System.Drawing.Size(11, 13);
            this.LabelControl1.TabIndex = 22;
            this.LabelControl1.Text = "ID";
            // 
            // EditForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(384, 113);
            this.Controls.Add(this.LabelControl_notes);
            this.Controls.Add(this.TextEdit_notes);
            this.Controls.Add(this.TextEdit_description);
            this.Controls.Add(this.LabelControl_description);
            this.Controls.Add(this.LabelControl_ID);
            this.Controls.Add(this.LabelControl1);
            this.Name = "EditForm";
            this.Text = "Place where we refer a client";
            this.Controls.SetChildIndex(this.simpleButton_OK, 0);
            this.Controls.SetChildIndex(this.simpleButton_Cancel, 0);
            this.Controls.SetChildIndex(this.LabelControl1, 0);
            this.Controls.SetChildIndex(this.LabelControl_ID, 0);
            this.Controls.SetChildIndex(this.LabelControl_description, 0);
            this.Controls.SetChildIndex(this.TextEdit_description, 0);
            this.Controls.SetChildIndex(this.TextEdit_notes, 0);
            this.Controls.SetChildIndex(this.LabelControl_notes, 0);
            ((System.ComponentModel.ISupportInitialize)(this.DxErrorProvider1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TextEdit_notes.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TextEdit_description.Properties)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }


        private DevExpress.XtraEditors.LabelControl LabelControl_notes;
        private DevExpress.XtraEditors.TextEdit TextEdit_notes;
        private DevExpress.XtraEditors.TextEdit TextEdit_description;
        private DevExpress.XtraEditors.LabelControl LabelControl_description;
        private DevExpress.XtraEditors.LabelControl LabelControl_ID;
        private DevExpress.XtraEditors.LabelControl LabelControl1;


    }
}