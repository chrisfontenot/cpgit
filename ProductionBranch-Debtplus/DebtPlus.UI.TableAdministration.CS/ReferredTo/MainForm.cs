﻿using System;
using System.Linq;
using DebtPlus.LINQ;

namespace DebtPlus.UI.TableAdministration.CS.ReferredTo
{
    public partial class MainForm : Templates.MainForm
    {
        /// <summary>
        /// List of the records to be edited in the program
        /// </summary>
        private System.Collections.Generic.List<referred_to> colRecords;

        /// <summary>
        /// Initialize the new class
        /// </summary>
        /// <param name="dc">Pointer to the data context in the mainline</param>
        public MainForm()
            : base()
        {
            InitializeComponent();
            RegisterHandlers();
        }

        private void RegisterHandlers()
        {
            Load += MainForm_Load;
        }

        private void UnRegisterHandlers()
        {
            Load -= MainForm_Load;
        }

        /// <summary>
        /// Process the load event on the form
        /// </summary>
        private void MainForm_Load(object sender, System.EventArgs e)
        {
            using (var cm = new DebtPlus.UI.Common.CursorManager())
            {
                try
                {
                    // Retrieve the list of menu items
                    using (var drm = new DebtPlus.LINQ.BusinessContext())
                    {
                        colRecords = drm.referred_tos.ToList();
                        gridControl1.DataSource = colRecords;
                    }
                }
                catch (Exception ex)
                {
                    DebtPlus.UI.Common.ErrorHandling.HandleErrors(ex, "Error retrieving database information");
                }
            }
        }

        /// <summary>
        /// Create the new record
        /// </summary>
        protected override void CreateRecord()
        {
            // Allocate the new record to be inserted.
            DebtPlus.LINQ.referred_to record = new DebtPlus.LINQ.referred_to();

            // Edit the new record. If OK then attempt to insert the record.
            using (EditForm frm = new EditForm(record))
            {
                if (frm.ShowDialog() == System.Windows.Forms.DialogResult.OK)
                {
                    try
                    {
                        using (var drm = new DebtPlus.LINQ.BusinessContext())
                        {
                            drm.referred_tos.InsertOnSubmit(record);
                            drm.SubmitChanges();

                            // Add the record to the main list if the database is updated
                            colRecords.Add(record);
                            gridControl1.RefreshDataSource();
                        }
                    }
                    catch (System.Data.SqlClient.SqlException ex)
                    {
                        DebtPlus.UI.Common.ErrorHandling.HandleErrors(ex, "Error updating database");
                    }
                }
            }
        }

        /// <summary>
        /// Update the existing record
        /// </summary>
        /// <param name="obj">Pointer to the desired record for the operation</param>
        protected override void UpdateRecord(object obj)
        {
            // Find the record
            DebtPlus.LINQ.referred_to record = obj as referred_to;
            if (obj == null)
            {
                return;
            }

            using (EditForm frm = new EditForm(record))
            {
                if (frm.ShowDialog() == System.Windows.Forms.DialogResult.OK)
                {
                    try
                    {
                        // Update the database with the current values
                        using (var drm = new BusinessContext())
                        {
                            var q = (from r in drm.referred_tos where r.Id == record.Id select r).SingleOrDefault();
                            if (q != null)
                            {
                                q.description = record.description;
                                q.notes = record.notes;

                                drm.SubmitChanges();
                                gridControl1.RefreshDataSource();
                            }
                        }
                    }
                    catch (System.Data.SqlClient.SqlException ex)
                    {
                        DebtPlus.UI.Common.ErrorHandling.HandleErrors(ex, "Error updating database");
                    }
                }
            }
        }

        /// <summary>
        /// Delete the indicated record from the database
        /// </summary>
        /// <param name="obj">Pointer to the desired record for the operation</param>
        protected override void DeleteRecord(object obj)
        {
            // Find the record
            DebtPlus.LINQ.referred_to record = obj as referred_to;
            if (obj == null)
            {
                return;
            }

            if (DebtPlus.Data.Prompts.RequestConfirmation_Delete() == System.Windows.Forms.DialogResult.Yes)
            {
                using (var drm = new BusinessContext())
                {
                    // Find the record and remove it from the database if it is still there
                    var query = (from m in drm.referred_tos where m.Id == record.Id select m).SingleOrDefault();
                    if (query != null)
                    {
                        drm.referred_tos.DeleteOnSubmit(query);
                        drm.SubmitChanges();
                    }

                    // Remove the record from the display list and redisplay it
                    colRecords.Remove(record);
                    gridControl1.RefreshDataSource();
                }
            }
        }
    }
}