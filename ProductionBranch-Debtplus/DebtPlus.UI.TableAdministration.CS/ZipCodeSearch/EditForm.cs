#region "Copyright 2000-2012 DebtPlus, L.L.C."
//{*******************************************************************}
//{                                                                   }
//{       DebtPlus Debt Management System                             }
//{                                                                   }
//{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
//{       ALL RIGHTS RESERVED                                         }
//{                                                                   }
//{   The entire contents of this file is protected by U.S. and       }
//{   International Copyright Laws. Unauthorized reproduction,        }
//{   reverse-engineering, and distribution of all or any portion of  }
//{   the code contained in this file is strictly prohibited and may  }
//{   result in severe civil and criminal penalties and will be       }
//{   prosecuted to the maximum extent possible under the law.        }
//{                                                                   }
//{   RESTRICTIONS                                                    }
//{                                                                   }
//{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
//{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
//{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
//{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
//{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
//{                                                                   }
//{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
//{   ADDITIONAL RESTRICTIONS.                                        }
//{                                                                   }
//{*******************************************************************}

#endregion "Copyright 2000-2012 DebtPlus, L.L.C."

using System;
using DebtPlus.LINQ;
using System.Linq;

namespace DebtPlus.UI.TableAdministration.CS.ZipCodeSearch
{
    internal partial class EditForm : Templates.EditTemplateForm
    {
        private DebtPlus.LINQ.ZipCodeSearch record = null;
        private BusinessContext bc                 = null;

        /// <summary>
        /// Initialize the input form
        /// </summary>
        internal EditForm()
            : base()
        {
            InitializeComponent();
        }

        /// <summary>
        /// Initialize the input form
        /// </summary>
        internal EditForm(BusinessContext bc, DebtPlus.LINQ.ZipCodeSearch record)
            : this()
        {
            this.bc = bc;
            this.record = record;
            RegisterHandlers();
        }

        /// <summary>
        /// Add the event handler routines
        /// </summary>
        private void RegisterHandlers()
        {
            Load                                  += EditForm_Load;
            textEdit_CityName.EditValueChanged    += form_Changed;
            textEdit_Zipcode.EditValueChanged     += form_Changed;
            lookUpEdit_MSA.EditValueChanged       += form_Changed;
            lookUpEdit_Timezone.EditValueChanged  += form_Changed;
            textEdit_AreaCode.EditValueChanged    += form_Changed;
            textEdit_Latitude.EditValueChanged    += form_Changed;
            textEdit_Longitude.EditValueChanged  += form_Changed;
            lookUpEdit_CityType.EditValueChanged  += form_Changed;
            lookUpEdit_StateID.EditValueChanged   += form_Changed;
            lookUpEdit_StateID.EditValueChanged   += State_Changed;
            lookUpEdit_ZipType.EditValueChanged   += form_Changed;
            checkEdit_ActiveFlag.EditValueChanged += form_Changed;
            textEdit_Zipcode.Validating           += textEdit_Zipcode_Validating;
            textEdit_Latitude.Validating          += validate_Latitude_Longtitude;
            textEdit_Longitude.Validating        += validate_Latitude_Longtitude;
        }

        /// <summary>
        /// Remove the event handler registrations
        /// </summary>
        private void UnRegisterHandlers()
        {
            Load                                  -= EditForm_Load;
            textEdit_CityName.EditValueChanged    -= form_Changed;
            textEdit_Zipcode.EditValueChanged     -= form_Changed;
            lookUpEdit_MSA.EditValueChanged       -= form_Changed;
            lookUpEdit_Timezone.EditValueChanged  -= form_Changed;
            textEdit_AreaCode.EditValueChanged    -= form_Changed;
            textEdit_Latitude.EditValueChanged    -= form_Changed;
            textEdit_Longitude.EditValueChanged  -= form_Changed;
            lookUpEdit_CityType.EditValueChanged  -= form_Changed;
            lookUpEdit_StateID.EditValueChanged   -= form_Changed;
            lookUpEdit_StateID.EditValueChanged   -= State_Changed;
            lookUpEdit_ZipType.EditValueChanged   -= form_Changed;
            checkEdit_ActiveFlag.EditValueChanged -= form_Changed;
            textEdit_Zipcode.Validating           -= textEdit_Zipcode_Validating;
            textEdit_Latitude.Validating          -= validate_Latitude_Longtitude;
            textEdit_Longitude.Validating        -= validate_Latitude_Longtitude;
        }

        /// <summary>
        /// Process the LOAD sequence on the form
        /// </summary>
        private void EditForm_Load(object sender, EventArgs e)
        {
            UnRegisterHandlers();
            try
            {
                // Set the DataSource for the lookup controls
                lookUpEdit_CityType.Properties.DataSource = DebtPlus.LINQ.InMemory.ZipcodeSearchCityTypes.getList();
                lookUpEdit_ZipType.Properties.DataSource  = DebtPlus.LINQ.InMemory.ZipcodeSearchZipTypes.getList();
                lookUpEdit_MSA.Properties.DataSource      = DebtPlus.LINQ.Cache.MSACodeType.getList();
                lookUpEdit_Timezone.Properties.DataSource = DebtPlus.LINQ.Cache.TimeZone.getList();

                lookUpEdit_StateID.Properties.DataSource  = bc.states.ToList();

                // Define the values for the current record
                checkEdit_ActiveFlag.Checked  = record.ActiveFlag;
                textEdit_AreaCode.EditValue   = record.AreaCode;
                textEdit_CityName.EditValue   = record.CityName;
                textEdit_Latitude.EditValue   = record.Latitude;
                textEdit_Longitude.EditValue = record.Longitude;
                textEdit_Zipcode.EditValue    = record.ZIPCode;
                lookUpEdit_CityType.EditValue = record.CityType;
                lookUpEdit_ZipType.EditValue  = record.ZIPType;
                lookUpEdit_MSA.EditValue      = record.MSACode;
                lookUpEdit_Timezone.EditValue = record.TimeZone;
                lookUpEdit_MSA.EditValue      = record.MSACode;
                lookUpEdit_StateID.EditValue  = record.State;
                lookUpEdit_Timezone.EditValue = record.TimeZone;
                lookUpEdit_County.EditValue   = record.County;

                UpdateCountyList();

                // Enable or Disable the OK button as appropriate
                simpleButton_OK.Enabled = !HasErrors();
            }
            finally
            {
                RegisterHandlers();
            }
        }

        /// <summary>
        /// Process a change in the state edit value.
        /// </summary>
        private void State_Changed(object sender, EventArgs e)
        {
            UnRegisterHandlers();
            try
            {
                UpdateCountyList();
                simpleButton_OK.Enabled = !HasErrors();
            }
            finally
            {
                RegisterHandlers();
            }
        }

        /// <summary>
        /// Change the list of county fields to match only the counties associated with the state reference.
        /// </summary>
        private void UpdateCountyList()
        {
            Int32? stateID = DebtPlus.Utils.Nulls.v_Int32(lookUpEdit_StateID.EditValue);
            if (stateID.HasValue)
            {
                lookUpEdit_County.Properties.DataSource = DebtPlus.LINQ.Cache.county.getList().FindAll(s => s.state == stateID.Value);
                lookUpEdit_County.Properties.ForceInitialize();
                return;
            }
            lookUpEdit_County.Properties.DataSource = null;
            lookUpEdit_County.Properties.ForceInitialize();
        }

        /// <summary>
        /// Validate the Longitude and LATITUDE input fields
        /// </summary>
        private void validate_Latitude_Longtitude(object sender, System.ComponentModel.CancelEventArgs e)
        {
            Double? value = DebtPlus.Utils.Nulls.v_Double((sender as DevExpress.XtraEditors.TextEdit).EditValue);
            if (value.HasValue)
            {
                if (System.Math.Abs(value.Value) > 180.0)
                {
                    (sender as DevExpress.XtraEditors.TextEdit).ErrorText = "Value is out of range. Must be -180 to +180";
                    e.Cancel = true;
                    return;
                }
            }

            (sender as DevExpress.XtraEditors.TextEdit).ErrorText = null;
        }

        /// <summary>
        /// Validate the ZIPCODE field
        /// </summary>
        private void textEdit_Zipcode_Validating(object sender, System.ComponentModel.CancelEventArgs e)
        {
            if (!isValidZipcode(DebtPlus.Utils.Nulls.v_String(textEdit_Zipcode.EditValue)))
            {
                textEdit_Zipcode.ErrorText = "Value is not valid";
            }
            else
            {
                textEdit_Zipcode.ErrorText = string.Empty;
            }
        }

        /// <summary>
        /// Handle the FORM CHANGE event
        /// </summary>
        private void form_Changed(object sender, EventArgs e)
        {
            simpleButton_OK.Enabled = !HasErrors();
        }

        /// <summary>
        /// Determine if there are missing fields in the input form
        /// </summary>
        /// <returns></returns>
        private bool HasErrors()
        {
            // The other controls can not be null.
            foreach (var ctl in new DevExpress.XtraEditors.TextEdit[] { textEdit_Zipcode, textEdit_CityName, lookUpEdit_CityType, lookUpEdit_StateID, lookUpEdit_ZipType })
            {
                if (ctl.EditValue == null)
                {
                    return true;
                }
            }

            // We need 5 digits for the zipcode
            if (DebtPlus.Utils.Nulls.v_String(textEdit_Zipcode.EditValue).Length != 5)
            {
                return true;
            }

            // Ensure that the city name has a value
            if (DebtPlus.Utils.Nulls.v_String(textEdit_CityName.EditValue).Length < 1)
            {
                return true;
            }

            // The zipcode field is special
            if (!isValidZipcode(DebtPlus.Utils.Nulls.v_String(textEdit_Zipcode.EditValue)))
            {
                return true;
            }

            return false;
        }

        /// <summary>
        /// Determine if the zipcode is valid
        /// </summary>
        private bool isValidZipcode(string key)
        {
            if (string.IsNullOrEmpty(key))
            {
                return false;
            }

            if (key.Length != 5)
            {
                return false;
            }

            // All is valid
            return true;
        }

        /// <summary>
        /// Process the CLICK event on the OK button
        /// </summary>
        protected override void simpleButton_OK_Click(object sender, EventArgs e)
        {
            record.ActiveFlag = checkEdit_ActiveFlag.Checked;
            record.AreaCode   = DebtPlus.Utils.Nulls.v_String(textEdit_AreaCode.EditValue);
            record.CityName   = DebtPlus.Utils.Nulls.v_String(textEdit_CityName.EditValue);
            record.CityType   = Convert.ToChar(lookUpEdit_CityType.EditValue);
            record.ZIPType    = Convert.ToChar(lookUpEdit_ZipType.EditValue);
            record.MSACode    = DebtPlus.Utils.Nulls.v_String(lookUpEdit_MSA.EditValue);
            record.TimeZone   = DebtPlus.Utils.Nulls.v_Int32(lookUpEdit_Timezone.EditValue);
            record.County     = DebtPlus.Utils.Nulls.v_Int32(lookUpEdit_County.EditValue);
            record.Latitude   = DebtPlus.Utils.Nulls.v_Double(textEdit_Latitude.EditValue);
            record.Longitude = DebtPlus.Utils.Nulls.v_Double(textEdit_Longitude.EditValue);
            record.MSACode    = DebtPlus.Utils.Nulls.v_String(lookUpEdit_MSA.EditValue);
            record.state1     = lookUpEdit_StateID.GetSelectedDataRow() as DebtPlus.LINQ.state;
            record.TimeZone   = DebtPlus.Utils.Nulls.v_Int32(lookUpEdit_Timezone.EditValue);
            record.ZIPCode    = DebtPlus.Utils.Nulls.v_String(textEdit_Zipcode.EditValue);

            base.simpleButton_OK_Click(sender, e);
        }
    }
}