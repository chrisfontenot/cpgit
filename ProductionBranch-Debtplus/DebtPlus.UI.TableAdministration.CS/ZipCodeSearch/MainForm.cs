#region "Copyright 2000-2012 DebtPlus, L.L.C."
//{*******************************************************************}
//{                                                                   }
//{       DebtPlus Debt Management System                             }
//{                                                                   }
//{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
//{       ALL RIGHTS RESERVED                                         }
//{                                                                   }
//{   The entire contents of this file is protected by U.S. and       }
//{   International Copyright Laws. Unauthorized reproduction,        }
//{   reverse-engineering, and distribution of all or any portion of  }
//{   the code contained in this file is strictly prohibited and may  }
//{   result in severe civil and criminal penalties and will be       }
//{   prosecuted to the maximum extent possible under the law.        }
//{                                                                   }
//{   RESTRICTIONS                                                    }
//{                                                                   }
//{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
//{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
//{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
//{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
//{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
//{                                                                   }
//{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
//{   ADDITIONAL RESTRICTIONS.                                        }
//{                                                                   }
//{*******************************************************************}

#endregion "Copyright 2000-2012 DebtPlus, L.L.C."

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using DebtPlus.LINQ;
using DebtPlus.UI.Common;

namespace DebtPlus.UI.TableAdministration.CS.ZipCodeSearch
{
    public partial class MainForm : Templates.MainForm
    {
        private System.Collections.Generic.List<DebtPlus.LINQ.ZipCodeSearch> colRecords;
        private BusinessContext bc = new BusinessContext();

        public MainForm()
            : base()
        {
            InitializeComponent();
            RegisterHandlers();
        }

        private void RegisterHandlers()
        {
            this.Load += MainForm_Load;
            barButtonItem_Import.ItemClick += barButtonItem_Import_ItemClick;
        }

        private void UnRegisterHandlers()
        {
            this.Load -= MainForm_Load;
            barButtonItem_Import.ItemClick -= barButtonItem_Import_ItemClick;
        }

        private DevExpress.Utils.WaitDialogForm statusDlg = null;

        /// <summary>
        /// Handle the import of the new list
        /// </summary>
        private void barButtonItem_Import_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            try
            {
                using (var frm = new ImportForm(ref colRecords, bc))
                {
                    frm.Status += frm_Status;
                    if (frm.ShowDialog() == System.Windows.Forms.DialogResult.OK)
                    {
                        try
                        {
                            using (var cm = new DebtPlus.UI.Common.CursorManager())
                            {
                                showStatus(string.Format("Applying changes to database ({0})", bc.GetChangeSet().ToString()));
                                bc.SubmitChanges();
                                gridView1.RefreshData();
                            }
                        }
                        catch (System.Data.SqlClient.SqlException ex)
                        {
                            DebtPlus.UI.Common.ErrorHandling.HandleErrors(ex, "Error updating database");
                        }
                    }
                    frm.Status -= frm_Status;
                }
            }
            finally
            {
                if (statusDlg != null)
                {
                    statusDlg.Close();
                    statusDlg.Dispose();
                    statusDlg = null;
                }
            }
        }

        private void frm_Status(object sender, ImportForm.StatusEventArgs e)
        {
            showStatus(e.Message);
        }

        private void showStatus(string Message)
        {
            if (statusDlg == null)
            {
                statusDlg = new DevExpress.Utils.WaitDialogForm();
                statusDlg.Show();
            }

            statusDlg.SetCaption(Message);
            Application.DoEvents();
        }

        /// <summary>
        /// Process the load event on the form
        /// </summary>
        private void MainForm_Load(object sender, EventArgs e)
        {
            UnRegisterHandlers();
            try
            {
                using (var cm = new CursorManager())
                {
                    showStatus("Loading records from database");

                    try
                    {
                        // Load the state references with this dataset
                        var dl = new System.Data.Linq.DataLoadOptions();
                        dl.LoadWith<DebtPlus.LINQ.ZipCodeSearch>(s => s.state1);
                        bc.LoadOptions = dl;

                        colRecords = bc.ZipCodeSearches.ToList();
                        gridControl1.DataSource = colRecords;
                        gridView1.BestFitColumns();
                    }
                    catch (System.Data.SqlClient.SqlException ex)
                    {
                        DebtPlus.UI.Common.ErrorHandling.HandleErrors(ex);
                    }

                    finally
                    {
                        if (statusDlg != null)
                        {
                            statusDlg.Close();
                            statusDlg.Dispose();
                            statusDlg = null;
                        }
                    }
                }
            }
            finally
            {
                RegisterHandlers();
            }
        }

        protected override void CreateRecord()
        {
            // Allocate the new record to be inserted.
            var record = DebtPlus.LINQ.Factory.Manufacture_ZipCodeSearch();

            // Edit the new record. If OK then attempt to insert the record.
            using (var frm = new EditForm(bc, record))
            {
                if (frm.ShowDialog() != System.Windows.Forms.DialogResult.OK)
                {
                    return;
                }

                try
                {
                    // Add the record to the collection
                    bc.ZipCodeSearches.InsertOnSubmit(record);
                    bc.SubmitChanges();
                    colRecords.Add(record);
                    gridView1.RefreshData();
                }
                catch (System.Data.SqlClient.SqlException ex)
                {
                    DebtPlus.UI.Common.ErrorHandling.HandleErrors(ex);
                }
            }
        }

        /// <summary>
        /// Update the existing record
        /// </summary>
        /// <param name="obj">Pointer to the desired record for the operation</param>
        protected override void UpdateRecord(object obj)
        {
            // Find the record
            var record = obj as DebtPlus.LINQ.ZipCodeSearch;
            if (obj == null)
            {
                return;
            }

            // Update the record now.
            using (var frm = new EditForm(bc, record))
            {
                if (frm.ShowDialog() != System.Windows.Forms.DialogResult.OK)
                {
                    return;
                }
            }

            try
            {
                bc.SubmitChanges();
                gridView1.RefreshData();
            }
            catch (System.Data.SqlClient.SqlException ex)
            {
                DebtPlus.UI.Common.ErrorHandling.HandleErrors(ex);
            }
        }

        /// <summary>
        /// Delete the indicated record from the database
        /// </summary>
        /// <param name="obj">Pointer to the desired record for the operation</param>
        protected override void DeleteRecord(object obj)
        {
            // Find the record
            var record = obj as DebtPlus.LINQ.ZipCodeSearch;
            if (obj == null)
            {
                return;
            }

            if (DebtPlus.Data.Prompts.RequestConfirmation_Delete() != System.Windows.Forms.DialogResult.Yes)
            {
                return;
            }

            try
            {
                bc.ZipCodeSearches.DeleteOnSubmit(record);
                bc.SubmitChanges();

                colRecords.Remove(record);
                gridView1.RefreshData();
            }
            catch (System.Data.SqlClient.SqlException ex)
            {
                DebtPlus.UI.Common.ErrorHandling.HandleErrors(ex);
            }
        }
    }
}