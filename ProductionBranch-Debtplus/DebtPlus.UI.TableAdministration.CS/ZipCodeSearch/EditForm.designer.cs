using System;
using System.Windows.Forms;
using System.Drawing;
using DebtPlus.LINQ;
using System.Linq;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Diagnostics;
using DebtPlus.UI.Common.Templates;

namespace DebtPlus.UI.TableAdministration.CS.ZipCodeSearch
{
	partial class EditForm
	{
		//Form overrides dispose to clean up the component list.
		[System.Diagnostics.DebuggerNonUserCode]
		protected override void Dispose(bool disposing)
		{
			try 
            {
				if (disposing)
                {
                    if (components != null) components.Dispose();
				}
                components = null;
            }
    	    finally
            {
				base.Dispose(disposing);
			}
		}

		//Required by the Windows Form Designer
		private System.ComponentModel.IContainer components = null;

		//NOTE: The following procedure is required by the Windows Form Designer
		//It can be modified using the Windows Form Designer.  
		//Do not modify it using the code editor.
		[System.Diagnostics.DebuggerStepThrough]
		private void InitializeComponent()
		{
            this.labelControl1 = new DevExpress.XtraEditors.LabelControl();
            this.textEdit_Zipcode = new DevExpress.XtraEditors.TextEdit();
            this.textEdit_CityName = new DevExpress.XtraEditors.TextEdit();
            this.labelControl2 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl3 = new DevExpress.XtraEditors.LabelControl();
            this.lookUpEdit_StateID = new DevExpress.XtraEditors.LookUpEdit();
            this.lookUpEdit_ZipType = new DevExpress.XtraEditors.LookUpEdit();
            this.labelControl4 = new DevExpress.XtraEditors.LabelControl();
            this.lookUpEdit_CityType = new DevExpress.XtraEditors.LookUpEdit();
            this.labelControl5 = new DevExpress.XtraEditors.LabelControl();
            this.checkEdit_ActiveFlag = new DevExpress.XtraEditors.CheckEdit();
            this.labelControl7 = new DevExpress.XtraEditors.LabelControl();
            this.lookUpEdit_MSA = new DevExpress.XtraEditors.LookUpEdit();
            this.lookUpEdit_Timezone = new DevExpress.XtraEditors.LookUpEdit();
            this.labelControl6 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl8 = new DevExpress.XtraEditors.LabelControl();
            this.textEdit_Longitude = new DevExpress.XtraEditors.TextEdit();
            this.textEdit_Latitude = new DevExpress.XtraEditors.TextEdit();
            this.labelControl9 = new DevExpress.XtraEditors.LabelControl();
            this.textEdit_AreaCode = new DevExpress.XtraEditors.TextEdit();
            this.labelControl10 = new DevExpress.XtraEditors.LabelControl();
            this.lookUpEdit_County = new DevExpress.XtraEditors.LookUpEdit();
            this.labelControl11 = new DevExpress.XtraEditors.LabelControl();
            ((System.ComponentModel.ISupportInitialize)(this.DxErrorProvider1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit_Zipcode.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit_CityName.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lookUpEdit_StateID.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lookUpEdit_ZipType.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lookUpEdit_CityType.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit_ActiveFlag.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lookUpEdit_MSA.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lookUpEdit_Timezone.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit_Longitude.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit_Latitude.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit_AreaCode.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lookUpEdit_County.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // simpleButton_OK
            // 
            this.simpleButton_OK.Location = new System.Drawing.Point(372, 13);
            this.simpleButton_OK.TabIndex = 23;
            // 
            // simpleButton_Cancel
            // 
            this.simpleButton_Cancel.Location = new System.Drawing.Point(372, 56);
            this.simpleButton_Cancel.TabIndex = 24;
            // 
            // DefaultLookAndFeel1
            // 
            this.DefaultLookAndFeel1.LookAndFeel.SkinName = "McSkin";
            // 
            // labelControl1
            // 
            this.labelControl1.Location = new System.Drawing.Point(13, 13);
            this.labelControl1.Name = "labelControl1";
            this.labelControl1.Size = new System.Drawing.Size(39, 13);
            this.labelControl1.TabIndex = 0;
            this.labelControl1.Text = "ZipCode";
            // 
            // textEdit_Zipcode
            // 
            this.textEdit_Zipcode.Location = new System.Drawing.Point(83, 13);
            this.textEdit_Zipcode.Name = "textEdit_Zipcode";
            this.textEdit_Zipcode.Properties.Appearance.Options.UseTextOptions = true;
            this.textEdit_Zipcode.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.textEdit_Zipcode.Properties.Mask.EditMask = "00000";
            this.textEdit_Zipcode.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Simple;
            this.textEdit_Zipcode.Properties.MaxLength = 5;
            this.textEdit_Zipcode.Size = new System.Drawing.Size(100, 20);
            this.textEdit_Zipcode.TabIndex = 1;
            // 
            // textEdit_CityName
            // 
            this.textEdit_CityName.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.textEdit_CityName.Location = new System.Drawing.Point(83, 65);
            this.textEdit_CityName.Name = "textEdit_CityName";
            this.textEdit_CityName.Properties.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.textEdit_CityName.Properties.Mask.BeepOnError = true;
            this.textEdit_CityName.Properties.Mask.EditMask = "[^ ].*";
            this.textEdit_CityName.Properties.Mask.IgnoreMaskBlank = false;
            this.textEdit_CityName.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.RegEx;
            this.textEdit_CityName.Properties.Mask.ShowPlaceHolders = false;
            this.textEdit_CityName.Properties.MaxLength = 50;
            this.textEdit_CityName.Size = new System.Drawing.Size(265, 20);
            this.textEdit_CityName.TabIndex = 5;
            // 
            // labelControl2
            // 
            this.labelControl2.Location = new System.Drawing.Point(13, 68);
            this.labelControl2.Name = "labelControl2";
            this.labelControl2.Size = new System.Drawing.Size(19, 13);
            this.labelControl2.TabIndex = 4;
            this.labelControl2.Text = "City";
            // 
            // labelControl3
            // 
            this.labelControl3.Location = new System.Drawing.Point(13, 120);
            this.labelControl3.Name = "labelControl3";
            this.labelControl3.Size = new System.Drawing.Size(26, 13);
            this.labelControl3.TabIndex = 8;
            this.labelControl3.Text = "State";
            // 
            // lookUpEdit_StateID
            // 
            this.lookUpEdit_StateID.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.lookUpEdit_StateID.Location = new System.Drawing.Point(83, 117);
            this.lookUpEdit_StateID.Name = "lookUpEdit_StateID";
            this.lookUpEdit_StateID.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.True;
            this.lookUpEdit_StateID.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.lookUpEdit_StateID.Properties.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("Id", "ID", 20, DevExpress.Utils.FormatType.None, "", false, DevExpress.Utils.HorzAlignment.Near, DevExpress.Data.ColumnSortOrder.None, DevExpress.Utils.DefaultBoolean.True),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("Name", "Name", 20, DevExpress.Utils.FormatType.None, "", true, DevExpress.Utils.HorzAlignment.Near, DevExpress.Data.ColumnSortOrder.Ascending, DevExpress.Utils.DefaultBoolean.True)});
            this.lookUpEdit_StateID.Properties.DisplayMember = "Name";
            this.lookUpEdit_StateID.Properties.NullText = "";
            this.lookUpEdit_StateID.Properties.ShowFooter = false;
            this.lookUpEdit_StateID.Properties.ShowHeader = false;
            this.lookUpEdit_StateID.Properties.SortColumnIndex = 2;
            this.lookUpEdit_StateID.Properties.ValueMember = "Id";
            this.lookUpEdit_StateID.Size = new System.Drawing.Size(265, 20);
            this.lookUpEdit_StateID.TabIndex = 9;
            // 
            // lookUpEdit_ZipType
            // 
            this.lookUpEdit_ZipType.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.lookUpEdit_ZipType.Location = new System.Drawing.Point(83, 39);
            this.lookUpEdit_ZipType.Name = "lookUpEdit_ZipType";
            this.lookUpEdit_ZipType.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.False;
            this.lookUpEdit_ZipType.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.lookUpEdit_ZipType.Properties.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("Id", "ID", 20, DevExpress.Utils.FormatType.None, "", false, DevExpress.Utils.HorzAlignment.Near, DevExpress.Data.ColumnSortOrder.None, DevExpress.Utils.DefaultBoolean.True),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("description", "Description", 20, DevExpress.Utils.FormatType.None, "", true, DevExpress.Utils.HorzAlignment.Near, DevExpress.Data.ColumnSortOrder.Ascending, DevExpress.Utils.DefaultBoolean.True)});
            this.lookUpEdit_ZipType.Properties.DisplayMember = "description";
            this.lookUpEdit_ZipType.Properties.NullText = "";
            this.lookUpEdit_ZipType.Properties.ShowFooter = false;
            this.lookUpEdit_ZipType.Properties.ShowHeader = false;
            this.lookUpEdit_ZipType.Properties.SortColumnIndex = 2;
            this.lookUpEdit_ZipType.Properties.ValueMember = "Id";
            this.lookUpEdit_ZipType.Size = new System.Drawing.Size(265, 20);
            this.lookUpEdit_ZipType.TabIndex = 3;
            // 
            // labelControl4
            // 
            this.labelControl4.Location = new System.Drawing.Point(13, 42);
            this.labelControl4.Name = "labelControl4";
            this.labelControl4.Size = new System.Drawing.Size(66, 13);
            this.labelControl4.TabIndex = 2;
            this.labelControl4.Text = "ZipCode Type";
            // 
            // lookUpEdit_CityType
            // 
            this.lookUpEdit_CityType.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.lookUpEdit_CityType.Location = new System.Drawing.Point(83, 91);
            this.lookUpEdit_CityType.Name = "lookUpEdit_CityType";
            this.lookUpEdit_CityType.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.False;
            this.lookUpEdit_CityType.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.lookUpEdit_CityType.Properties.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("Id", "ID", 20, DevExpress.Utils.FormatType.None, "", false, DevExpress.Utils.HorzAlignment.Near, DevExpress.Data.ColumnSortOrder.None, DevExpress.Utils.DefaultBoolean.True),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("description", "Description", 20, DevExpress.Utils.FormatType.None, "", true, DevExpress.Utils.HorzAlignment.Near, DevExpress.Data.ColumnSortOrder.Ascending, DevExpress.Utils.DefaultBoolean.True)});
            this.lookUpEdit_CityType.Properties.DisplayMember = "description";
            this.lookUpEdit_CityType.Properties.NullText = "";
            this.lookUpEdit_CityType.Properties.ShowFooter = false;
            this.lookUpEdit_CityType.Properties.ShowHeader = false;
            this.lookUpEdit_CityType.Properties.SortColumnIndex = 2;
            this.lookUpEdit_CityType.Properties.ValueMember = "Id";
            this.lookUpEdit_CityType.Size = new System.Drawing.Size(265, 20);
            this.lookUpEdit_CityType.TabIndex = 7;
            // 
            // labelControl5
            // 
            this.labelControl5.Location = new System.Drawing.Point(13, 94);
            this.labelControl5.Name = "labelControl5";
            this.labelControl5.Size = new System.Drawing.Size(46, 13);
            this.labelControl5.TabIndex = 6;
            this.labelControl5.Text = "City Type";
            // 
            // checkEdit_ActiveFlag
            // 
            this.checkEdit_ActiveFlag.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.checkEdit_ActiveFlag.Location = new System.Drawing.Point(13, 277);
            this.checkEdit_ActiveFlag.Name = "checkEdit_ActiveFlag";
            this.checkEdit_ActiveFlag.Properties.Caption = "Active";
            this.checkEdit_ActiveFlag.Size = new System.Drawing.Size(66, 20);
            this.checkEdit_ActiveFlag.TabIndex = 22;
            // 
            // labelControl7
            // 
            this.labelControl7.Location = new System.Drawing.Point(13, 172);
            this.labelControl7.Name = "labelControl7";
            this.labelControl7.Size = new System.Drawing.Size(21, 13);
            this.labelControl7.TabIndex = 12;
            this.labelControl7.Text = "MSA";
            // 
            // lookUpEdit_MSA
            // 
            this.lookUpEdit_MSA.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.lookUpEdit_MSA.Location = new System.Drawing.Point(83, 169);
            this.lookUpEdit_MSA.Name = "lookUpEdit_MSA";
            this.lookUpEdit_MSA.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.True;
            this.lookUpEdit_MSA.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.lookUpEdit_MSA.Properties.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("Id", "ID", 20, DevExpress.Utils.FormatType.None, "", false, DevExpress.Utils.HorzAlignment.Near, DevExpress.Data.ColumnSortOrder.None, DevExpress.Utils.DefaultBoolean.True),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("Name", "Name", 20, DevExpress.Utils.FormatType.None, "", true, DevExpress.Utils.HorzAlignment.Near, DevExpress.Data.ColumnSortOrder.Ascending, DevExpress.Utils.DefaultBoolean.True)});
            this.lookUpEdit_MSA.Properties.DisplayMember = "Name";
            this.lookUpEdit_MSA.Properties.NullText = "";
            this.lookUpEdit_MSA.Properties.ShowFooter = false;
            this.lookUpEdit_MSA.Properties.ShowHeader = false;
            this.lookUpEdit_MSA.Properties.SortColumnIndex = 2;
            this.lookUpEdit_MSA.Properties.ValueMember = "Id";
            this.lookUpEdit_MSA.Size = new System.Drawing.Size(265, 20);
            this.lookUpEdit_MSA.TabIndex = 13;
            // 
            // lookUpEdit_Timezone
            // 
            this.lookUpEdit_Timezone.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.lookUpEdit_Timezone.Location = new System.Drawing.Point(83, 195);
            this.lookUpEdit_Timezone.Name = "lookUpEdit_Timezone";
            this.lookUpEdit_Timezone.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.True;
            this.lookUpEdit_Timezone.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.lookUpEdit_Timezone.Properties.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("Id", "ID", 20, DevExpress.Utils.FormatType.None, "", false, DevExpress.Utils.HorzAlignment.Near, DevExpress.Data.ColumnSortOrder.None, DevExpress.Utils.DefaultBoolean.True),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("Description", "Description", 20, DevExpress.Utils.FormatType.None, "", true, DevExpress.Utils.HorzAlignment.Near, DevExpress.Data.ColumnSortOrder.Ascending, DevExpress.Utils.DefaultBoolean.True)});
            this.lookUpEdit_Timezone.Properties.DisplayMember = "Description";
            this.lookUpEdit_Timezone.Properties.NullText = "";
            this.lookUpEdit_Timezone.Properties.ShowFooter = false;
            this.lookUpEdit_Timezone.Properties.ShowHeader = false;
            this.lookUpEdit_Timezone.Properties.SortColumnIndex = 2;
            this.lookUpEdit_Timezone.Properties.ValueMember = "Id";
            this.lookUpEdit_Timezone.Size = new System.Drawing.Size(265, 20);
            this.lookUpEdit_Timezone.TabIndex = 15;
            // 
            // labelControl6
            // 
            this.labelControl6.Location = new System.Drawing.Point(13, 198);
            this.labelControl6.Name = "labelControl6";
            this.labelControl6.Size = new System.Drawing.Size(45, 13);
            this.labelControl6.TabIndex = 14;
            this.labelControl6.Text = "Time zone";
            // 
            // labelControl8
            // 
            this.labelControl8.Location = new System.Drawing.Point(13, 224);
            this.labelControl8.Name = "labelControl8";
            this.labelControl8.Size = new System.Drawing.Size(51, 13);
            this.labelControl8.TabIndex = 16;
            this.labelControl8.Text = "Longitude";
            // 
            // textEdit_Longitude
            // 
            this.textEdit_Longitude.Location = new System.Drawing.Point(83, 221);
            this.textEdit_Longitude.Name = "textEdit_Longitude";
            this.textEdit_Longitude.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.True;
            this.textEdit_Longitude.Properties.Appearance.Options.UseTextOptions = true;
            this.textEdit_Longitude.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.textEdit_Longitude.Properties.DisplayFormat.FormatString = "f";
            this.textEdit_Longitude.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.textEdit_Longitude.Properties.EditFormat.FormatString = "f";
            this.textEdit_Longitude.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.textEdit_Longitude.Properties.Mask.BeepOnError = true;
            this.textEdit_Longitude.Properties.Mask.EditMask = "f";
            this.textEdit_Longitude.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.textEdit_Longitude.Properties.ValidateOnEnterKey = true;
            this.textEdit_Longitude.Size = new System.Drawing.Size(100, 20);
            this.textEdit_Longitude.TabIndex = 17;
            // 
            // textEdit_Latitude
            // 
            this.textEdit_Latitude.Location = new System.Drawing.Point(248, 221);
            this.textEdit_Latitude.Name = "textEdit_Latitude";
            this.textEdit_Latitude.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.True;
            this.textEdit_Latitude.Properties.Appearance.Options.UseTextOptions = true;
            this.textEdit_Latitude.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.textEdit_Latitude.Properties.DisplayFormat.FormatString = "f";
            this.textEdit_Latitude.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.textEdit_Latitude.Properties.EditFormat.FormatString = "f";
            this.textEdit_Latitude.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.textEdit_Latitude.Properties.Mask.BeepOnError = true;
            this.textEdit_Latitude.Properties.Mask.EditMask = "f";
            this.textEdit_Latitude.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.textEdit_Latitude.Properties.ValidateOnEnterKey = true;
            this.textEdit_Latitude.Size = new System.Drawing.Size(100, 20);
            this.textEdit_Latitude.TabIndex = 19;
            // 
            // labelControl9
            // 
            this.labelControl9.Location = new System.Drawing.Point(195, 224);
            this.labelControl9.Name = "labelControl9";
            this.labelControl9.Size = new System.Drawing.Size(39, 13);
            this.labelControl9.TabIndex = 18;
            this.labelControl9.Text = "Latitude";
            // 
            // textEdit_AreaCode
            // 
            this.textEdit_AreaCode.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.textEdit_AreaCode.Location = new System.Drawing.Point(83, 247);
            this.textEdit_AreaCode.Name = "textEdit_AreaCode";
            this.textEdit_AreaCode.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.True;
            this.textEdit_AreaCode.Properties.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.textEdit_AreaCode.Properties.Mask.BeepOnError = true;
            this.textEdit_AreaCode.Properties.Mask.EditMask = "[^ ].*";
            this.textEdit_AreaCode.Properties.Mask.IgnoreMaskBlank = false;
            this.textEdit_AreaCode.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.RegEx;
            this.textEdit_AreaCode.Properties.Mask.ShowPlaceHolders = false;
            this.textEdit_AreaCode.Properties.MaxLength = 50;
            this.textEdit_AreaCode.Size = new System.Drawing.Size(265, 20);
            this.textEdit_AreaCode.TabIndex = 21;
            // 
            // labelControl10
            // 
            this.labelControl10.Location = new System.Drawing.Point(13, 250);
            this.labelControl10.Name = "labelControl10";
            this.labelControl10.Size = new System.Drawing.Size(64, 13);
            this.labelControl10.TabIndex = 20;
            this.labelControl10.Text = "Area Code(s)";
            // 
            // lookUpEdit_County
            // 
            this.lookUpEdit_County.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.lookUpEdit_County.Location = new System.Drawing.Point(83, 143);
            this.lookUpEdit_County.Name = "lookUpEdit_County";
            this.lookUpEdit_County.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.True;
            this.lookUpEdit_County.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.lookUpEdit_County.Properties.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("Id", "ID", 20, DevExpress.Utils.FormatType.None, "", false, DevExpress.Utils.HorzAlignment.Near),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("name", "Name", 20, DevExpress.Utils.FormatType.None, "", true, DevExpress.Utils.HorzAlignment.Near, DevExpress.Data.ColumnSortOrder.Ascending)});
            this.lookUpEdit_County.Properties.DisplayMember = "name";
            this.lookUpEdit_County.Properties.NullText = "";
            this.lookUpEdit_County.Properties.ShowFooter = false;
            this.lookUpEdit_County.Properties.ShowHeader = false;
            this.lookUpEdit_County.Properties.SortColumnIndex = 2;
            this.lookUpEdit_County.Properties.ValueMember = "Id";
            this.lookUpEdit_County.Size = new System.Drawing.Size(265, 20);
            this.lookUpEdit_County.TabIndex = 11;
            // 
            // labelControl11
            // 
            this.labelControl11.Location = new System.Drawing.Point(13, 146);
            this.labelControl11.Name = "labelControl11";
            this.labelControl11.Size = new System.Drawing.Size(35, 13);
            this.labelControl11.TabIndex = 10;
            this.labelControl11.Text = "County";
            // 
            // EditForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(468, 307);
            this.Controls.Add(this.lookUpEdit_County);
            this.Controls.Add(this.labelControl11);
            this.Controls.Add(this.textEdit_AreaCode);
            this.Controls.Add(this.labelControl10);
            this.Controls.Add(this.textEdit_Latitude);
            this.Controls.Add(this.labelControl9);
            this.Controls.Add(this.textEdit_Longitude);
            this.Controls.Add(this.labelControl8);
            this.Controls.Add(this.lookUpEdit_Timezone);
            this.Controls.Add(this.labelControl6);
            this.Controls.Add(this.lookUpEdit_MSA);
            this.Controls.Add(this.labelControl7);
            this.Controls.Add(this.checkEdit_ActiveFlag);
            this.Controls.Add(this.lookUpEdit_CityType);
            this.Controls.Add(this.labelControl5);
            this.Controls.Add(this.lookUpEdit_ZipType);
            this.Controls.Add(this.labelControl4);
            this.Controls.Add(this.lookUpEdit_StateID);
            this.Controls.Add(this.labelControl3);
            this.Controls.Add(this.textEdit_CityName);
            this.Controls.Add(this.labelControl2);
            this.Controls.Add(this.textEdit_Zipcode);
            this.Controls.Add(this.labelControl1);
            this.Name = "EditForm";
            this.Text = "ZipCode Search Item";
            this.Controls.SetChildIndex(this.simpleButton_OK, 0);
            this.Controls.SetChildIndex(this.simpleButton_Cancel, 0);
            this.Controls.SetChildIndex(this.labelControl1, 0);
            this.Controls.SetChildIndex(this.textEdit_Zipcode, 0);
            this.Controls.SetChildIndex(this.labelControl2, 0);
            this.Controls.SetChildIndex(this.textEdit_CityName, 0);
            this.Controls.SetChildIndex(this.labelControl3, 0);
            this.Controls.SetChildIndex(this.lookUpEdit_StateID, 0);
            this.Controls.SetChildIndex(this.labelControl4, 0);
            this.Controls.SetChildIndex(this.lookUpEdit_ZipType, 0);
            this.Controls.SetChildIndex(this.labelControl5, 0);
            this.Controls.SetChildIndex(this.lookUpEdit_CityType, 0);
            this.Controls.SetChildIndex(this.checkEdit_ActiveFlag, 0);
            this.Controls.SetChildIndex(this.labelControl7, 0);
            this.Controls.SetChildIndex(this.lookUpEdit_MSA, 0);
            this.Controls.SetChildIndex(this.labelControl6, 0);
            this.Controls.SetChildIndex(this.lookUpEdit_Timezone, 0);
            this.Controls.SetChildIndex(this.labelControl8, 0);
            this.Controls.SetChildIndex(this.textEdit_Longitude, 0);
            this.Controls.SetChildIndex(this.labelControl9, 0);
            this.Controls.SetChildIndex(this.textEdit_Latitude, 0);
            this.Controls.SetChildIndex(this.labelControl10, 0);
            this.Controls.SetChildIndex(this.textEdit_AreaCode, 0);
            this.Controls.SetChildIndex(this.labelControl11, 0);
            this.Controls.SetChildIndex(this.lookUpEdit_County, 0);
            ((System.ComponentModel.ISupportInitialize)(this.DxErrorProvider1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit_Zipcode.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit_CityName.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lookUpEdit_StateID.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lookUpEdit_ZipType.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lookUpEdit_CityType.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit_ActiveFlag.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lookUpEdit_MSA.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lookUpEdit_Timezone.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit_Longitude.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit_Latitude.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit_AreaCode.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lookUpEdit_County.Properties)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        private DevExpress.XtraEditors.LabelControl labelControl1;
        private DevExpress.XtraEditors.TextEdit textEdit_Zipcode;
        private DevExpress.XtraEditors.TextEdit textEdit_CityName;
        private DevExpress.XtraEditors.LabelControl labelControl2;
        private DevExpress.XtraEditors.LabelControl labelControl3;
        private DevExpress.XtraEditors.LookUpEdit lookUpEdit_StateID;
        private DevExpress.XtraEditors.LookUpEdit lookUpEdit_ZipType;
        private DevExpress.XtraEditors.LabelControl labelControl4;
        private DevExpress.XtraEditors.LookUpEdit lookUpEdit_CityType;
        private DevExpress.XtraEditors.LabelControl labelControl5;
        private DevExpress.XtraEditors.CheckEdit checkEdit_ActiveFlag;
        private DevExpress.XtraEditors.LabelControl labelControl7;
        private DevExpress.XtraEditors.LookUpEdit lookUpEdit_MSA;
        private DevExpress.XtraEditors.LookUpEdit lookUpEdit_Timezone;
        private DevExpress.XtraEditors.LabelControl labelControl6;
        private DevExpress.XtraEditors.LabelControl labelControl8;
        private DevExpress.XtraEditors.TextEdit textEdit_Longitude;
        private DevExpress.XtraEditors.TextEdit textEdit_Latitude;
        private DevExpress.XtraEditors.LabelControl labelControl9;
        private DevExpress.XtraEditors.TextEdit textEdit_AreaCode;
        private DevExpress.XtraEditors.LabelControl labelControl10;
        private DevExpress.XtraEditors.LookUpEdit lookUpEdit_County;
        private DevExpress.XtraEditors.LabelControl labelControl11;
	}
}

