﻿using System;
using System.Windows.Forms;

namespace DebtPlus.UI.TableAdministration.CS.ZipCodeSearch
{
    internal partial class ImportForm : DebtPlus.Data.Forms.DebtPlusForm
    {
        internal class StatusEventArgs : System.EventArgs
        {
            internal StatusEventArgs()
                : base()
            {
            }

            internal StatusEventArgs(string Message)
                : this()
            {
                this.Message = Message;
            }

            internal string Message { get; set; }
        }

        internal delegate void StatusEventHandler(object sender, StatusEventArgs e);

        public event StatusEventHandler Status;

        protected void RaiseStatus(StatusEventArgs e)
        {
            var evt = Status;
            if (evt != null)
            {
                evt(this, e);
            }
        }

        protected virtual void OnRaiseStatus(StatusEventArgs e)
        {
            RaiseStatus(e);
        }

        private System.Collections.Generic.List<DebtPlus.LINQ.ZipCodeSearch> colRecords = null;
        private DebtPlus.LINQ.BusinessContext bc = null;

        internal ImportForm()
        {
            InitializeComponent();
        }

        internal ImportForm(ref System.Collections.Generic.List<DebtPlus.LINQ.ZipCodeSearch> colRecords, DebtPlus.LINQ.BusinessContext bc)
            : this()
        {
            this.colRecords = colRecords;
            this.bc = bc;
            RegisterHandlers();
        }

        private void RegisterHandlers()
        {
            this.Load += ImportForm_Load;
            simpleButton_OK.Click += simpleButton_OK_Click;
            buttonEdit_FileName.ButtonClick += buttonEdit_FileName_ButtonClick;
            buttonEdit_FileName.EditValueChanging += buttonEdit_FileName_EditValueChanging;
        }

        private void UnRegisterHandlers()
        {
            this.Load -= ImportForm_Load;
            simpleButton_OK.Click -= simpleButton_OK_Click;
            buttonEdit_FileName.ButtonClick -= buttonEdit_FileName_ButtonClick;
            buttonEdit_FileName.EditValueChanging -= buttonEdit_FileName_EditValueChanging;
        }

        private void buttonEdit_FileName_EditValueChanging(object sender, DevExpress.XtraEditors.Controls.ChangingEventArgs e)
        {
            simpleButton_OK.Enabled = !HasErrors(DebtPlus.Utils.Nulls.v_String(e.NewValue));
        }

        /// <summary>
        /// Handle the load event for the form.
        /// </summary>
        private void ImportForm_Load(object sender, System.EventArgs e)
        {
            UnRegisterHandlers();
            try
            {
                simpleButton_OK.Enabled = !HasErrors();
            }
            finally
            {
                RegisterHandlers();
            }
        }

        /// <summary>
        /// Look for an error condition on the input form
        /// </summary>
        /// <returns></returns>
        private bool HasErrors()
        {
            return HasErrors(DebtPlus.Utils.Nulls.v_String(buttonEdit_FileName.EditValue));
        }

        private bool HasErrors(string fileName)
        {
            return string.IsNullOrEmpty(fileName);
        }

        /// <summary>
        /// Process a click event on the filename button
        /// </summary>
        private void buttonEdit_FileName_ButtonClick(object sender, DevExpress.XtraEditors.Controls.ButtonPressedEventArgs e)
        {
            UnRegisterHandlers();
            try
            {
                using (var dlg = new System.Windows.Forms.OpenFileDialog()
                {
                    AddExtension = true,
                    AutoUpgradeEnabled = true,
                    CheckFileExists = true,
                    CheckPathExists = true,
                    DefaultExt = ".csv",
                    DereferenceLinks = true,
                    Filter = "Comma-Separated-Values (*.csv)|*.csv|Text Documents (*.txt)|*.txt|All Files|*.*",
                    FilterIndex = 0,
                    InitialDirectory = System.Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments),
                    Multiselect = false,
                    ReadOnlyChecked = false,
                    ShowHelp = false,
                    ShowReadOnly = false,
                    RestoreDirectory = true,
                    SupportMultiDottedExtensions = true,
                    Title = "Input File Name"
                })
                {
                    if (dlg.ShowDialog() != System.Windows.Forms.DialogResult.OK)
                    {
                        return;
                    }
                    buttonEdit_FileName.Text = dlg.FileName;
                    simpleButton_OK.Enabled = !HasErrors();
                }
            }
            finally
            {
                RegisterHandlers();
            }
        }

        /// <summary>
        /// Process the CLICK event on the OK button
        /// </summary>
        private void simpleButton_OK_Click(object sender, System.EventArgs e)
        {
            System.Collections.Generic.List<DebtPlus.LINQ.ZipCodeSearch> newCollection = ReadCollection(bc, DebtPlus.Utils.Nulls.v_String(buttonEdit_FileName.EditValue));
            if (newCollection == null)
            {
                return;
            }

            // Update the collection in the list.
            AddIfMissing(newCollection);
            DeleteIfFound(newCollection);

            // Complete the dialog properly
            DialogResult = System.Windows.Forms.DialogResult.OK;
        }

        /// <summary>
        /// Add or update all items that are found in the list.
        /// </summary>
        /// <param name="newCollection">Pointer to the new list of items</param>
        private void AddIfMissing(System.Collections.Generic.List<DebtPlus.LINQ.ZipCodeSearch> newCollection)
        {
            OnRaiseStatus(new StatusEventArgs("Adding any missing items to the list"));

            // Build the dictionary list to speed search operations
            var dict = buildDictionary(colRecords);
            DebtPlus.LINQ.ZipCodeSearch q = null;

            // Process the new list. Add any record that is not in the current list. Update those that are.
            foreach (var newItem in newCollection)
            {
                Application.DoEvents();
                string key = keyString(newItem);
                if (dict.ContainsKey(key))
                {
                    q = dict[key];

                    // Update the record with the new information
                    q.AreaCode = newItem.AreaCode;
                    q.CityType = newItem.CityType;
                    q.County = newItem.County;
                    q.MSACode = newItem.MSACode;
                    q.TimeZone = newItem.TimeZone;
                    q.ZIPType = newItem.ZIPType;
                    continue;
                }

                // Add the new record to the collection. It was created blank so we don't need to do anything special here.
                newItem.state1 = newItem.UpdateUtilityStateReference;   // Finally bind the record to the state reference.
                bc.ZipCodeSearches.InsertOnSubmit(newItem);
                colRecords.Add(newItem);
            }
        }

        /// <summary>
        /// Remove all items from the collection that are no longer in the new list
        /// </summary>
        /// <param name="newCollection">Pointer to the new list of items</param>
        private void DeleteIfFound(System.Collections.Generic.List<DebtPlus.LINQ.ZipCodeSearch> newCollection)
        {
            // Build the dictionary list to speed search operations
            var dict = buildDictionary(newCollection);

            OnRaiseStatus(new StatusEventArgs("Deleting any items that are no longer valid"));

            // Process the original list. Delete any record that is not in the new list
            for (Int32 itemNumber = colRecords.Count - 1; itemNumber >= 0; --itemNumber)
            {
                Application.DoEvents();
                var search = colRecords[itemNumber];
                string key = keyString(search);
                if (!dict.ContainsKey(key))
                {
                    bc.ZipCodeSearches.DeleteOnSubmit(search);
                    colRecords.RemoveAt(itemNumber);

                    // Remove it from the list
                    dict.Remove(key);
                }
            }
        }

        private System.Collections.Generic.Dictionary<string, DebtPlus.LINQ.ZipCodeSearch> buildDictionary(System.Collections.Generic.List<DebtPlus.LINQ.ZipCodeSearch> collection)
        {
            System.Collections.Generic.Dictionary<string, DebtPlus.LINQ.ZipCodeSearch> dict = new System.Collections.Generic.Dictionary<string, DebtPlus.LINQ.ZipCodeSearch>();
            foreach (var item in collection)
            {
                dict.Add(keyString(item), item);
            }
            return dict;
        }

        private string keyString(DebtPlus.LINQ.ZipCodeSearch record)
        {
            if (record != null)
            {
                if (record.UpdateUtilityStateReference != null)
                {
                    return (record.ZIPCode + "*" + record.UpdateUtilityStateReference.Id.ToString() + "*" + record.CityName).ToUpper();
                }

                return (record.ZIPCode + "*" + (record.state1 == null ? "NULL" : record.state1.Id.ToString()) + "*" + record.CityName).ToUpper();
            }

            return string.Empty;
        }
    }
}