﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace DebtPlus.UI.TableAdministration.CS.ZipCodeSearch
{
    internal partial class ImportForm
    {
        private System.Collections.Generic.List<DebtPlus.LINQ.state> colStates = null;

        private System.Collections.Generic.List<DebtPlus.LINQ.ZipCodeSearch> ReadCollection(DebtPlus.LINQ.BusinessContext bc, string FileName)
        {
            int itemsLoaded = 0;
            int lastItemsLoaded = -100;

            try
            {
                // Read a list of the states. They need to be in our BusinessContext
                colStates = bc.states.ToList();

                // List of the field names for the input data
                string[] fieldNames;

                // Collection of new records
                System.Collections.Generic.List<DebtPlus.LINQ.ZipCodeSearch> newCollection = new System.Collections.Generic.List<DebtPlus.LINQ.ZipCodeSearch>();

                // Process the data in the input file.
                using (var ifs = new System.IO.StreamReader(FileName))
                {
                    // Read the header line
                    string line = ifs.ReadLine();
                    if (line == null)
                    {
                        return null;
                    }
                    fieldNames = line.Trim().Split(',');

                    // Process the input lines until we run off the end
                    while ((line = ifs.ReadLine()) != null)
                    {
                        if ((itemsLoaded - lastItemsLoaded) >= 100)
                        {
                            lastItemsLoaded = itemsLoaded;
                            OnRaiseStatus(new StatusEventArgs(string.Format("Reading source file ({0:n0} items)", itemsLoaded)));
                        }
                        ++itemsLoaded;

                        // Allow the window updates to occur for now.
                        Application.DoEvents();

                        // Construct a dictionary to pair the original tag names with the new values
                        string[] values = line.Trim().Split(',');
                        var dict = fieldNames.Zip(values, (key, val) => new { k = key, v = val }).ToDictionary(s => s.k, s => s.v);

                        // Create the new record
                        var record        = DebtPlus.LINQ.Factory.Manufacture_ZipCodeSearch();

                        record.Latitude                    = convert_double(dict["Latitude"]);
                        record.Longitude                   = convert_double(dict["Longitude"]);
                        record.UpdateUtilityStateReference = convert_state(dict["StateAbbr"], dict["StateFIPS"]);
                        record.County                      = convert_county(dict["CountyFIPS"], record.UpdateUtilityStateReference, dict["CountyName"]);
                        record.MSACode                     = convert_msa(dict["MSACode"]);
                        record.TimeZone                    = convert_timezone(convert_double(dict["UTC"]), convert_bool(dict["DST"]));
                        record.AreaCode                    = convert_areacode(dict["AreaCode"]);
                        record.ZIPCode                     = dict["ZIPCode"];
                        record.CityName                    = dict["CityName"];
                        record.ZIPType                     = Convert.ToChar(dict["ZIPType"]);
                        record.CityType                    = Convert.ToChar(dict["CityType"]);
                        record.ActiveFlag                  = true;

                        newCollection.Add(record);
                    }
                }
                return newCollection;
            }
            catch
            {
                return null;
            }
        }

        /// <summary>
        /// Convert the input string to the suitable double value.
        /// </summary>
        private Double? convert_double(string DoubleValue)
        {
            double v;
            if (!string.IsNullOrWhiteSpace(DoubleValue))
            {
                if (double.TryParse(DoubleValue, out v))
                {
                    if (v != 0.0D)
                    {
                        return v;
                    }
                }
            }

            return null;
        }

        /// <summary>
        /// Convert the value to a suitable AreaCode string
        /// </summary>
        private string convert_areacode(string acode)
        {
            if (!string.IsNullOrWhiteSpace(acode))
            {
                System.Collections.Generic.List<string> list = acode.Split('/').ToList<string>();
                for (int indx = list.Count - 1; indx >= 0; --indx)
                {
                    if (string.IsNullOrWhiteSpace(list[indx]) || list[indx] == "000")
                    {
                        list.RemoveAt(indx);
                    }
                }
                acode = string.Join("/", list.ToArray());
            }
 
            if (!string.IsNullOrWhiteSpace(acode))
            {
                return acode;
            }
            return null;
        }

        /// <summary>
        /// Convert the value to a suitable MSA string
        /// </summary>
        private string convert_msa(string MSAValue)
        {
            if (!string.IsNullOrWhiteSpace(MSAValue))
            {
                var q = DebtPlus.LINQ.Cache.MSACodeType.getList().Find(s => s.Id == MSAValue);
                if (q != null)
                {
                    return q.Id;
                }
            }

            return null;
        }

        /// <summary>
        /// Convert the input to a suitable timezone entry
        /// </summary>
        private Int32? convert_timezone(double? UtcOffset, bool DST)
        {
            Int32 ruleID = DST ? 2 : 1;
            var q = DebtPlus.LINQ.Cache.TimeZone.getList().Find(s => s.GMTOffset.Value == UtcOffset.GetValueOrDefault(0.0D) && s.TimeZoneShiftRule == ruleID);
            if (q != null)
            {
                return q.Id;
            }

            return null;
        }

        /// <summary>
        /// Convert the input to a suitable boolean value
        /// </summary>
        private bool convert_bool(string boolString)
        {
            if (!string.IsNullOrWhiteSpace(boolString))
            {
                char chrCode = (boolString.Trim() + "N")[0];
                if (chrCode == 'Y')
                {
                    return true;
                }
            }
            return false;
        }

        /// <summary>
        /// Convert the item to a valid county record ID
        /// </summary>
        private Int32? convert_county(string CountyFIPS, DebtPlus.LINQ.state stateRecord, string CountyName)
        {
            // Match the FIPS number first. This is the most accurate.
            if (!string.IsNullOrWhiteSpace(CountyFIPS))
            {
                var q = DebtPlus.LINQ.Cache.county.getList().Find(s => string.Compare(s.FIPS, CountyFIPS, true) == 0);
                if (q != null)
                {
                    return q.Id;
                }
            }

            // Try to find the county by name. County names are state specific.
            if (!string.IsNullOrWhiteSpace(CountyName))
            {
                var q = DebtPlus.LINQ.Cache.county.getList().Find(s => string.Compare(s.name, CountyName, true) == 0 && s.state == stateRecord.Id);
                if (q != null)
                {
                    return q.Id;
                }
            }

            // There is no match.
            return null;
        }

        private DebtPlus.LINQ.state convert_state(string Abbreviation, string FIPS)
        {
            if (FIPS != null && FIPS != "00")
            {
                var q = colStates.Find(s => s.FIPS == FIPS);
                if (q != null)
                {
                    return q;
                }
            }

            if (Abbreviation != null)
            {
                var q = colStates.Find(s => s.MailingCode == Abbreviation);
                if (q != null && q.Id != 0)
                {
                    return q;
                }
            }

            return null;
        }
    }
}
