﻿namespace DebtPlus.UI.TableAdministration.CS.ACHRejectCodes
{
    partial class MainForm
    {
        //Form overrides dispose to clean up the component list.
        [System.Diagnostics.DebuggerNonUserCode]
        protected override void Dispose(bool disposing)
        {
            try
            {
                if (disposing)
				{
					if (components != null) components.Dispose();
					if (bc != null) bc.Dispose();
                }
				components = null;
				bc = null;
            }
            finally
            {
                base.Dispose(disposing);
            }
        }

        //Required by the Windows Form Designer
        private System.ComponentModel.IContainer components = null;
		
        //NOTE: The following procedure is required by the Windows Form Designer
        //It can be modified using the Windows Form Designer.  
        //Do not modify it using the code editor.
        [System.Diagnostics.DebuggerStepThrough]
        private void InitializeComponent()
        {
            DevExpress.XtraGrid.StyleFormatCondition styleFormatCondition1 = new DevExpress.XtraGrid.StyleFormatCondition();
            this.gridColumn_ID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn_description = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn_serious = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn_letter_code = new DevExpress.XtraGrid.Columns.GridColumn();
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.popupMenu1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DxErrorProvider1)).BeginInit();
            this.SuspendLayout();
            // 
            // gridView1
            // 
            this.gridView1.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gridColumn_ID,
            this.gridColumn_description,
            this.gridColumn_serious,
            this.gridColumn_letter_code});
            styleFormatCondition1.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            styleFormatCondition1.Appearance.Options.UseFont = true;
            styleFormatCondition1.ApplyToRow = true;
            styleFormatCondition1.Condition = DevExpress.XtraGrid.FormatConditionEnum.NotEqual;
            styleFormatCondition1.Value1 = "false";
            this.gridView1.FormatConditions.AddRange(new DevExpress.XtraGrid.StyleFormatCondition[] {
            styleFormatCondition1});
            // 
            // gridColumn_ID
            // 
            this.gridColumn_ID.Caption = "ID";
            this.gridColumn_ID.FieldName = "Id";
            this.gridColumn_ID.Name = "gridColumn_ID";
            this.gridColumn_ID.Visible = true;
            this.gridColumn_ID.VisibleIndex = 0;
            this.gridColumn_ID.Width = 73;
            // 
            // gridColumn_description
            // 
            this.gridColumn_description.Caption = "Description";
            this.gridColumn_description.FieldName = "description";
            this.gridColumn_description.Name = "gridColumn_description";
            this.gridColumn_description.Visible = true;
            this.gridColumn_description.VisibleIndex = 1;
            this.gridColumn_description.Width = 364;
            // 
            // gridColumn_serious
            // 
            this.gridColumn_serious.Caption = "Serious?";
            this.gridColumn_serious.FieldName = "serious_error";
            this.gridColumn_serious.Name = "gridColumn_serious";
            this.gridColumn_serious.Visible = true;
            this.gridColumn_serious.VisibleIndex = 2;
            this.gridColumn_serious.Width = 80;
            // 
            // gridColumn_letter_code
            // 
            this.gridColumn_letter_code.Caption = "Letter";
            this.gridColumn_letter_code.FieldName = "letter_code";
            this.gridColumn_letter_code.Name = "gridColumn_letter_code";
            this.gridColumn_letter_code.Visible = true;
            this.gridColumn_letter_code.VisibleIndex = 3;
            this.gridColumn_letter_code.Width = 100;
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.ClientSize = new System.Drawing.Size(528, 294);
            this.Name = "MainForm";
            this.Text = "ACH Reject Codes";
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.popupMenu1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DxErrorProvider1)).EndInit();
            this.ResumeLayout(false);
        }
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn_ID;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn_description;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn_serious;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn_letter_code;
    }
}
