﻿namespace DebtPlus.UI.TableAdministration.CS.ACHRejectCodes
{
    partial class EditForm
    {
		//Form overrides dispose to clean up the component list.
		[System.Diagnostics.DebuggerNonUserCode]
		protected override void Dispose(bool disposing)
		{
			try 
            {
				if (disposing)
                {
                    if (components != null) components.Dispose();
				}
                components = null;
            }
    	    finally
            {
				base.Dispose(disposing);
			}
		}

		//Required by the Windows Form Designer
		private System.ComponentModel.IContainer components = null;

        //NOTE: The following procedure is required by the Windows Form Designer
        //It can be modified using the Windows Form Designer.  
        //Do not modify it using the code editor.
        [System.Diagnostics.DebuggerStepThrough()]
        private void InitializeComponent()
        {
            this.TextEdit_ach_error_code = new DevExpress.XtraEditors.TextEdit();
            this.TextEdit_description = new DevExpress.XtraEditors.TextEdit();
            this.LookUpEdit_letter_code = new DevExpress.XtraEditors.LookUpEdit();
            this.LabelControl4 = new DevExpress.XtraEditors.LabelControl();
            this.LabelControl2 = new DevExpress.XtraEditors.LabelControl();
            this.CheckEdit_serious_error = new DevExpress.XtraEditors.CheckEdit();
            this.LabelControl1 = new DevExpress.XtraEditors.LabelControl();
            ((System.ComponentModel.ISupportInitialize)(this.DxErrorProvider1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TextEdit_ach_error_code.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TextEdit_description.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LookUpEdit_letter_code.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.CheckEdit_serious_error.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // simpleButton_OK
            // 
            this.simpleButton_OK.Enabled = true;
            this.simpleButton_OK.Location = new System.Drawing.Point(303, 34);
            this.simpleButton_OK.TabIndex = 7;
            // 
            // simpleButton_Cancel
            // 
            this.simpleButton_Cancel.Location = new System.Drawing.Point(303, 77);
            this.simpleButton_Cancel.TabIndex = 8;
            // 
            // TextEdit_ach_error_code
            // 
            this.TextEdit_ach_error_code.Location = new System.Drawing.Point(93, 13);
            this.TextEdit_ach_error_code.Name = "TextEdit_ach_error_code";
            this.TextEdit_ach_error_code.Properties.Mask.BeepOnError = true;
            this.TextEdit_ach_error_code.Properties.Mask.EditMask = "[A-Z][0-9][0-9]";
            this.TextEdit_ach_error_code.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.RegEx;
            this.TextEdit_ach_error_code.Properties.MaxLength = 3;
            this.TextEdit_ach_error_code.Size = new System.Drawing.Size(192, 20);
            this.TextEdit_ach_error_code.TabIndex = 1;
            // 
            // TextEdit_description
            // 
            this.TextEdit_description.Location = new System.Drawing.Point(93, 40);
            this.TextEdit_description.Name = "TextEdit_description";
            this.TextEdit_description.Properties.MaxLength = 50;
            this.TextEdit_description.Size = new System.Drawing.Size(192, 20);
            this.TextEdit_description.TabIndex = 3;
            // 
            // LookUpEdit_letter_code
            // 
            this.LookUpEdit_letter_code.Location = new System.Drawing.Point(93, 66);
            this.LookUpEdit_letter_code.Name = "LookUpEdit_letter_code";
            this.LookUpEdit_letter_code.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LookUpEdit_letter_code.Properties.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("letter_type", "Type", 5, DevExpress.Utils.FormatType.Numeric, "f0", false, DevExpress.Utils.HorzAlignment.Far),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("letter_code", 5, "ID"),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("description", "Description")});
            this.LookUpEdit_letter_code.Properties.DisplayMember = "description";
            this.LookUpEdit_letter_code.Properties.NullText = "";
            this.LookUpEdit_letter_code.Properties.ShowFooter = false;
            this.LookUpEdit_letter_code.Properties.SortColumnIndex = 1;
            this.LookUpEdit_letter_code.Properties.ValidateOnEnterKey = true;
            this.LookUpEdit_letter_code.Properties.ValueMember = "letter_code";
            this.LookUpEdit_letter_code.Size = new System.Drawing.Size(192, 20);
            this.LookUpEdit_letter_code.TabIndex = 5;
            // 
            // LabelControl4
            // 
            this.LabelControl4.Location = new System.Drawing.Point(17, 69);
            this.LabelControl4.Name = "LabelControl4";
            this.LabelControl4.Size = new System.Drawing.Size(29, 13);
            this.LabelControl4.TabIndex = 4;
            this.LabelControl4.Text = "Letter";
            // 
            // LabelControl2
            // 
            this.LabelControl2.Location = new System.Drawing.Point(18, 43);
            this.LabelControl2.Name = "LabelControl2";
            this.LabelControl2.Size = new System.Drawing.Size(53, 13);
            this.LabelControl2.TabIndex = 2;
            this.LabelControl2.Text = "Description";
            // 
            // CheckEdit_serious_error
            // 
            this.CheckEdit_serious_error.Location = new System.Drawing.Point(15, 93);
            this.CheckEdit_serious_error.Name = "CheckEdit_serious_error";
            this.CheckEdit_serious_error.Properties.Caption = "Suspend further ACH pulls if this occurs?";
            this.CheckEdit_serious_error.Size = new System.Drawing.Size(270, 20);
            this.CheckEdit_serious_error.TabIndex = 6;
            // 
            // LabelControl1
            // 
            this.LabelControl1.Location = new System.Drawing.Point(18, 16);
            this.LabelControl1.Name = "LabelControl1";
            this.LabelControl1.Size = new System.Drawing.Size(52, 13);
            this.LabelControl1.TabIndex = 0;
            this.LabelControl1.Text = "Error Code";
            // 
            // EditForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(399, 126);
            this.Controls.Add(this.TextEdit_ach_error_code);
            this.Controls.Add(this.TextEdit_description);
            this.Controls.Add(this.LookUpEdit_letter_code);
            this.Controls.Add(this.LabelControl4);
            this.Controls.Add(this.LabelControl2);
            this.Controls.Add(this.CheckEdit_serious_error);
            this.Controls.Add(this.LabelControl1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.Sizable;
            this.Name = "EditForm";
            this.Text = "Reject Code Item";
            this.Controls.SetChildIndex(this.simpleButton_OK, 0);
            this.Controls.SetChildIndex(this.simpleButton_Cancel, 0);
            this.Controls.SetChildIndex(this.LabelControl1, 0);
            this.Controls.SetChildIndex(this.CheckEdit_serious_error, 0);
            this.Controls.SetChildIndex(this.LabelControl2, 0);
            this.Controls.SetChildIndex(this.LabelControl4, 0);
            this.Controls.SetChildIndex(this.LookUpEdit_letter_code, 0);
            this.Controls.SetChildIndex(this.TextEdit_description, 0);
            this.Controls.SetChildIndex(this.TextEdit_ach_error_code, 0);
            ((System.ComponentModel.ISupportInitialize)(this.DxErrorProvider1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TextEdit_ach_error_code.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TextEdit_description.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LookUpEdit_letter_code.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.CheckEdit_serious_error.Properties)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();
        }
		
        private DevExpress.XtraEditors.TextEdit TextEdit_ach_error_code;
        private DevExpress.XtraEditors.TextEdit TextEdit_description;
        private DevExpress.XtraEditors.LookUpEdit LookUpEdit_letter_code;
        private DevExpress.XtraEditors.LabelControl LabelControl4;
        private DevExpress.XtraEditors.LabelControl LabelControl2;
        private DevExpress.XtraEditors.CheckEdit CheckEdit_serious_error;
        private DevExpress.XtraEditors.LabelControl LabelControl1;
    }
}
