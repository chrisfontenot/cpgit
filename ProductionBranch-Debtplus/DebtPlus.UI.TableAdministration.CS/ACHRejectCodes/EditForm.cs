#region "Copyright 2000-2012 DebtPlus, L.L.C."

//{*******************************************************************}
//{                                                                   }
//{       DebtPlus Debt Management System                             }
//{                                                                   }
//{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
//{       ALL RIGHTS RESERVED                                         }
//{                                                                   }
//{   The entire contents of this file is protected by U.S. and       }
//{   International Copyright Laws. Unauthorized reproduction,        }
//{   reverse-engineering, and distribution of all or any portion of  }
//{   the code contained in this file is strictly prohibited and may  }
//{   result in severe civil and criminal penalties and will be       }
//{   prosecuted to the maximum extent possible under the law.        }
//{                                                                   }
//{   RESTRICTIONS                                                    }
//{                                                                   }
//{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
//{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
//{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
//{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
//{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
//{                                                                   }
//{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
//{   ADDITIONAL RESTRICTIONS.                                        }
//{                                                                   }
//{*******************************************************************}

#endregion "Copyright 2000-2012 DebtPlus, L.L.C."

using System;
using System.Linq;

namespace DebtPlus.UI.TableAdministration.CS.ACHRejectCodes
{
    internal partial class EditForm : Templates.EditTemplateForm
    {
        private DebtPlus.LINQ.ach_reject_code record;
        private System.Collections.Generic.List<DebtPlus.LINQ.ach_reject_code> colRecords;

        /// <summary>
        /// Initialize the new class
        /// </summary>
        internal EditForm()
            : base()
        {
            InitializeComponent();
        }

        /// <summary>
        /// Initialize the new class
        /// </summary>
        internal EditForm(System.Collections.Generic.List<DebtPlus.LINQ.ach_reject_code> colRecords, DebtPlus.LINQ.ach_reject_code record)
            : this()
        {
            this.colRecords = colRecords;
            this.record = record;

            // Load the list of letter types into the LookUpEdit control
            LookUpEdit_letter_code.Properties.DataSource = DebtPlus.LINQ.Cache.letter_type.getList();

            RegisterHandlers();
        }

        /// <summary>
        /// Register the event handlers
        /// </summary>
        private void RegisterHandlers()
        {
            Load += EditForm_Load;
            LookUpEdit_letter_code.EditValueChanging += DebtPlus.Data.Validation.LookUpEdit_ActiveTest;
            LookUpEdit_letter_code.EditValueChanged += FormChanged;
            TextEdit_description.EditValueChanged += FormChanged;
            TextEdit_ach_error_code.EditValueChanged += FormChanged;
            CheckEdit_serious_error.EditValueChanged += FormChanged;
        }

        /// <summary>
        /// Remove the event handler registrations
        /// </summary>
        private void UnRegisterHandlers()
        {
            Load -= EditForm_Load;
            LookUpEdit_letter_code.EditValueChanging -= DebtPlus.Data.Validation.LookUpEdit_ActiveTest;
            LookUpEdit_letter_code.EditValueChanged -= FormChanged;
            TextEdit_description.EditValueChanged -= FormChanged;
            TextEdit_ach_error_code.EditValueChanged -= FormChanged;
            CheckEdit_serious_error.EditValueChanged -= FormChanged;
        }

        /// <summary>
        /// Process the LOAD sequence for the form
        /// </summary>
        private void EditForm_Load(object sender, System.EventArgs e)
        {
            UnRegisterHandlers();
            try
            {
                TextEdit_ach_error_code.EditValue = record.Id;
                TextEdit_description.EditValue = record.description;
                CheckEdit_serious_error.EditValue = record.serious_error;
                LookUpEdit_letter_code.EditValue = record.letter_code;

                simpleButton_OK.Enabled = !HasErrors();
            }
            finally
            {
                RegisterHandlers();
            }
        }

        /// <summary>
        /// Handle the click event on the OK button
        /// </summary>
        protected override void simpleButton_OK_Click(object sender, EventArgs e)
        {
            record.Id = ((string)TextEdit_ach_error_code.EditValue).Trim();
            record.description = ((string)TextEdit_description.EditValue).Trim();
            record.serious_error = (Boolean)CheckEdit_serious_error.EditValue;
            record.letter_code = (string)LookUpEdit_letter_code.EditValue;

            base.simpleButton_OK_Click(sender, e);
        }

        /// <summary>
        /// When the form is changed, update the OK button status
        /// </summary>
        private void FormChanged(object Sender, System.EventArgs e)
        {
            simpleButton_OK.Enabled = !HasErrors();
        }

        /// <summary>
        /// Determine if there are any errors in the form
        /// </summary>
        private bool HasErrors()
        {
            // Ensure that there is an ID value.
            if (string.IsNullOrWhiteSpace((string)TextEdit_ach_error_code.EditValue))
            {
                return true;
            }
            var newID = ((string)TextEdit_ach_error_code.EditValue).Trim();

            // Ensure that the code is not duplicated
            if (string.Compare(record.Id, newID, true, System.Globalization.CultureInfo.InvariantCulture) != 0)
            {
                var foundID = (from r in colRecords where r.Id == newID select r.Id).SingleOrDefault();
                if (foundID != null)
                {
                    return true;
                }
            }

            if (string.IsNullOrWhiteSpace((string)TextEdit_description.EditValue))
            {
                return true;
            }

            // Finally, everything is valid.
            return false;
        }
    }
}