namespace DebtPlus.UI.TableAdministration.CS.Letter.Tables
{
	partial class EditForm
	{
		//Form overrides dispose to clean up the component list.
		[System.Diagnostics.DebuggerNonUserCode]
		protected override void Dispose(bool disposing)
		{
			try 
            {
				if (disposing)
                {
                    if (components != null) components.Dispose();
				}
                components = null;
            }
    	    finally
            {
				base.Dispose(disposing);
			}
		}

		//Required by the Windows Form Designer
		private System.ComponentModel.IContainer components = null;

		//NOTE: The following procedure is required by the Windows Form Designer
		//It can be modified using the Windows Form Designer.  
		//Do not modify it using the code editor.
		[System.Diagnostics.DebuggerStepThrough]
		private void InitializeComponent()
		{
            this.LabelControl1 = new DevExpress.XtraEditors.LabelControl();
            this.LabelControl_id = new DevExpress.XtraEditors.LabelControl();
            this.LabelControl3 = new DevExpress.XtraEditors.LabelControl();
            this.MemoEdit_query = new DevExpress.XtraEditors.MemoEdit();
            this.LabelControl2 = new DevExpress.XtraEditors.LabelControl();
            this.LookUpEdit_query_type = new DevExpress.XtraEditors.LookUpEdit();
            ((System.ComponentModel.ISupportInitialize)(this.DxErrorProvider1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.MemoEdit_query.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LookUpEdit_query_type.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // simpleButton_OK
            // 
            this.simpleButton_OK.Location = new System.Drawing.Point(316, 43);
            this.simpleButton_OK.TabIndex = 6;
            // 
            // simpleButton_Cancel
            // 
            this.simpleButton_Cancel.Location = new System.Drawing.Point(316, 86);
            this.simpleButton_Cancel.TabIndex = 7;
            // 
            // DefaultLookAndFeel1
            // 
            this.DefaultLookAndFeel1.LookAndFeel.SkinName = "McSkin";
            // 
            // LabelControl1
            // 
            this.LabelControl1.Location = new System.Drawing.Point(13, 13);
            this.LabelControl1.Name = "LabelControl1";
            this.LabelControl1.Size = new System.Drawing.Size(48, 13);
            this.LabelControl1.TabIndex = 0;
            this.LabelControl1.Text = "Record ID";
            // 
            // LabelControl_id
            // 
            this.LabelControl_id.Location = new System.Drawing.Point(86, 13);
            this.LabelControl_id.Name = "LabelControl_id";
            this.LabelControl_id.Size = new System.Drawing.Size(23, 13);
            this.LabelControl_id.TabIndex = 1;
            this.LabelControl_id.Text = "NEW";
            // 
            // LabelControl3
            // 
            this.LabelControl3.Location = new System.Drawing.Point(8, 77);
            this.LabelControl3.Name = "LabelControl3";
            this.LabelControl3.Size = new System.Drawing.Size(30, 13);
            this.LabelControl3.TabIndex = 4;
            this.LabelControl3.Text = "Query";
            // 
            // MemoEdit_query
            // 
            this.MemoEdit_query.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.MemoEdit_query.Location = new System.Drawing.Point(86, 74);
            this.MemoEdit_query.Name = "MemoEdit_query";
            this.MemoEdit_query.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.True;
            this.MemoEdit_query.Properties.MaxLength = 256;
            this.MemoEdit_query.Size = new System.Drawing.Size(212, 61);
            this.MemoEdit_query.TabIndex = 5;
            // 
            // LabelControl2
            // 
            this.LabelControl2.Location = new System.Drawing.Point(8, 43);
            this.LabelControl2.Name = "LabelControl2";
            this.LabelControl2.Size = new System.Drawing.Size(24, 13);
            this.LabelControl2.TabIndex = 2;
            this.LabelControl2.Text = "Type";
            // 
            // LookUpEdit_query_type
            // 
            this.LookUpEdit_query_type.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.LookUpEdit_query_type.Location = new System.Drawing.Point(86, 40);
            this.LookUpEdit_query_type.Name = "LookUpEdit_query_type";
            this.LookUpEdit_query_type.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.False;
            this.LookUpEdit_query_type.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LookUpEdit_query_type.Properties.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("Id", "ID", 20, DevExpress.Utils.FormatType.Numeric, "f0", false, DevExpress.Utils.HorzAlignment.Default),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("description", "Description")});
            this.LookUpEdit_query_type.Properties.DisplayMember = "description";
            this.LookUpEdit_query_type.Properties.ShowFooter = false;
            this.LookUpEdit_query_type.Properties.ShowHeader = false;
            this.LookUpEdit_query_type.Properties.SortColumnIndex = 1;
            this.LookUpEdit_query_type.Properties.ValueMember = "Id";
            this.LookUpEdit_query_type.Size = new System.Drawing.Size(212, 20);
            this.LookUpEdit_query_type.TabIndex = 3;
            // 
            // EditForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(412, 167);
            this.Controls.Add(this.LookUpEdit_query_type);
            this.Controls.Add(this.LabelControl2);
            this.Controls.Add(this.LabelControl_id);
            this.Controls.Add(this.LabelControl1);
            this.Controls.Add(this.MemoEdit_query);
            this.Controls.Add(this.LabelControl3);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.Sizable;
            this.Name = "EditForm";
            this.Text = "Letter Table";
            this.Controls.SetChildIndex(this.LabelControl3, 0);
            this.Controls.SetChildIndex(this.MemoEdit_query, 0);
            this.Controls.SetChildIndex(this.LabelControl1, 0);
            this.Controls.SetChildIndex(this.LabelControl_id, 0);
            this.Controls.SetChildIndex(this.simpleButton_OK, 0);
            this.Controls.SetChildIndex(this.simpleButton_Cancel, 0);
            this.Controls.SetChildIndex(this.LabelControl2, 0);
            this.Controls.SetChildIndex(this.LookUpEdit_query_type, 0);
            ((System.ComponentModel.ISupportInitialize)(this.DxErrorProvider1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.MemoEdit_query.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LookUpEdit_query_type.Properties)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();
		}

		private DevExpress.XtraEditors.LabelControl LabelControl1;
		private DevExpress.XtraEditors.LabelControl LabelControl_id;
		private DevExpress.XtraEditors.LabelControl LabelControl3;
		private DevExpress.XtraEditors.MemoEdit MemoEdit_query;
		private DevExpress.XtraEditors.LabelControl LabelControl2;
		private DevExpress.XtraEditors.LookUpEdit LookUpEdit_query_type;
	}
}
