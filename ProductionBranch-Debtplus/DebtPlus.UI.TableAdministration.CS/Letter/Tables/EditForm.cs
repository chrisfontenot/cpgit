#region "Copyright 2000-2012 DebtPlus, L.L.C."
//{*******************************************************************}
//{                                                                   }
//{       DebtPlus Debt Management System                             }
//{                                                                   }
//{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
//{       ALL RIGHTS RESERVED                                         }
//{                                                                   }
//{   The entire contents of this file is protected by U.S. and       }
//{   International Copyright Laws. Unauthorized reproduction,        }
//{   reverse-engineering, and distribution of all or any portion of  }
//{   the code contained in this file is strictly prohibited and may  }
//{   result in severe civil and criminal penalties and will be       }
//{   prosecuted to the maximum extent possible under the law.        }
//{                                                                   }
//{   RESTRICTIONS                                                    }
//{                                                                   }
//{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
//{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
//{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
//{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
//{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
//{                                                                   }
//{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
//{   ADDITIONAL RESTRICTIONS.                                        }
//{                                                                   }
//{*******************************************************************}

#endregion "Copyright 2000-2012 DebtPlus, L.L.C."

using System;
using DebtPlus.LINQ;

namespace DebtPlus.UI.TableAdministration.CS.Letter.Tables
{
    internal partial class EditForm : Templates.EditTemplateForm
    {
        private letter_table record;

        /// <summary>
        /// Initialize the new form class
        /// </summary>
        internal EditForm()
            : base()
        {
            InitializeComponent();
        }

        /// <summary>
        /// Initialize the new form class
        /// </summary>
        internal EditForm(letter_table record)
            : this()
        {
            this.record = record;
            RegisterHandlers();
        }

        /// <summary>
        /// Register the event handlers
        /// </summary>
        private void RegisterHandlers()
        {
            Load += EditForm_Load;
            LookUpEdit_query_type.EditValueChanged += form_Changed;
            MemoEdit_query.EditValueChanged += form_Changed;
        }

        /// <summary>
        /// Remove the event handler registrations
        /// </summary>
        private void UnRegisterHandlers()
        {
            Load -= EditForm_Load;
            LookUpEdit_query_type.EditValueChanged -= form_Changed;
            MemoEdit_query.EditValueChanged -= form_Changed;
        }

        /// <summary>
        /// Process the LOAD event for the form
        /// </summary>
        private void EditForm_Load(object sender, EventArgs e)
        {
            UnRegisterHandlers();
            try
            {
                LookUpEdit_query_type.Properties.DataSource = DebtPlus.LINQ.InMemory.LetterQueryTypes.getList();

                LabelControl_id.Text = (record.Id < 1) ? "NEW" : record.Id.ToString();
                LookUpEdit_query_type.EditValue = record.query_type;
                MemoEdit_query.EditValue = record.query;

                simpleButton_OK.Enabled = !HasErrors();
            }
            finally
            {
                RegisterHandlers();
            }
        }

        /// <summary>
        /// Handle the change in a control
        /// </summary>
        private void form_Changed(object sender, System.EventArgs e)
        {
            simpleButton_OK.Enabled = !HasErrors();
        }

        /// <summary>
        /// Determine if the controls contain enough for a valid record
        /// </summary>
        private bool HasErrors()
        {
            if (!DebtPlus.Utils.Nulls.v_Int32(LookUpEdit_query_type.EditValue).HasValue)
            {
                return true;
            }

            if (string.IsNullOrWhiteSpace(DebtPlus.Utils.Nulls.v_String(MemoEdit_query.EditValue)))
            {
                return true;
            }

            return false;
        }

        /// <summary>
        /// Process the CLICK event on the OK button
        /// </summary>
        protected override void simpleButton_OK_Click(object sender, EventArgs e)
        {
            record.query_type = DebtPlus.Utils.Nulls.v_Int32(LookUpEdit_query_type.EditValue).GetValueOrDefault();
            record.query = DebtPlus.Utils.Nulls.v_String(MemoEdit_query.EditValue);

            base.simpleButton_OK_Click(sender, e);
        }
    }
}