#region "Copyright 2000-2012 DebtPlus, L.L.C."
//{*******************************************************************}
//{                                                                   }
//{       DebtPlus Debt Management System                             }
//{                                                                   }
//{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
//{       ALL RIGHTS RESERVED                                         }
//{                                                                   }
//{   The entire contents of this file is protected by U.S. and       }
//{   International Copyright Laws. Unauthorized reproduction,        }
//{   reverse-engineering, and distribution of all or any portion of  }
//{   the code contained in this file is strictly prohibited and may  }
//{   result in severe civil and criminal penalties and will be       }
//{   prosecuted to the maximum extent possible under the law.        }
//{                                                                   }
//{   RESTRICTIONS                                                    }
//{                                                                   }
//{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
//{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
//{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
//{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
//{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
//{                                                                   }
//{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
//{   ADDITIONAL RESTRICTIONS.                                        }
//{                                                                   }
//{*******************************************************************}

#endregion "Copyright 2000-2012 DebtPlus, L.L.C."

using System;
using System.Linq;
using DebtPlus.LINQ;

namespace DebtPlus.UI.TableAdministration.CS.Letter.Types
{
    public partial class MainForm : Templates.MainForm
    {
        private BusinessContext bc = new BusinessContext();
        private System.Collections.Generic.List<letter_type> colRecords = null;

        public MainForm()
            : base()
        {
            InitializeComponent();
            RegisterHandlers();
        }

        private void RegisterHandlers()
        {
            Load += MainForm_Load;
        }

        private void UnRegisterHandlers()
        {
            Load -= MainForm_Load;
        }

        /// <summary>
        /// Process the load event on the form
        /// </summary>
        private void MainForm_Load(object sender, EventArgs e)
        {
            UnRegisterHandlers();
            try
            {
                // Define the formatting routines for the columns
                gridColumn_region.DisplayFormat.Format = new RegionFormatter();
                gridColumn_region.GroupFormat.Format = new RegionFormatter();
                gridColumn_class.DisplayFormat.Format = new LetterClassFormatter();
                gridColumn_class.GroupFormat.Format = new LetterClassFormatter();

                // Load the records from the database
                colRecords = bc.letter_types.ToList();
                gridControl1.DataSource = colRecords;

                gridView1.RefreshData();
                gridView1.BestFitColumns();
            }
            finally
            {
                RegisterHandlers();
            }
        }

        /// <summary>
        /// Create the new record
        /// </summary>
        protected override void CreateRecord()
        {
            // Allocate the new record to be inserted.
            var record = DebtPlus.LINQ.Factory.Manufacture_letter_type();

            // Override the default value for the region and set it to "ALL"
            record.region = 0;

            // Edit the new record. If OK then attempt to insert the record.
            using (var frm = new EditForm(record))
            {
                if (frm.ShowDialog() != System.Windows.Forms.DialogResult.OK)
                {
                    return;
                }

                // If this is the new default then clear the previous one.
                if (record.Default)
                {
                    foreach (var defaultRecord in colRecords.Where(s => s.Default))
                    {
                        defaultRecord.Default = false;
                    }
                    record.Default = true;
                }

                try
                {
                    // Add the record to the collection
                    bc.letter_types.InsertOnSubmit(record);
                    bc.SubmitChanges();
                    colRecords.Add(record);
                    gridView1.RefreshData();
                }
                catch (System.Data.SqlClient.SqlException ex)
                {
                    DebtPlus.UI.Common.ErrorHandling.HandleErrors(ex);
                }
            }
        }

        /// <summary>
        /// Update the existing record
        /// </summary>
        /// <param name="obj">Pointer to the desired record for the operation</param>
        protected override void UpdateRecord(object obj)
        {
            // Find the record
            var record = obj as letter_type;
            if (obj == null)
            {
                return;
            }

            // Remember the previous value for the default status. We will need it after the update.
            bool priorDefault = record.Default;

            // Update the record now.
            using (var frm = new EditForm(record))
            {
                if (frm.ShowDialog() != System.Windows.Forms.DialogResult.OK)
                {
                    return;
                }
            }

            try
            {
                // Turn off the default status of all other records in the collection.
                if (record.Default && !priorDefault)
                {
                    foreach (var defaultRecord in colRecords.Where(s => s.Default))
                    {
                        defaultRecord.Default = false;
                    }

                    // Reset the default status back to TRUE for the current record.
                    record.Default = true;
                }

                // Update the database accordingly
                bc.SubmitChanges();
                gridView1.RefreshData();
            }
            catch (System.Data.SqlClient.SqlException ex)
            {
                DebtPlus.UI.Common.ErrorHandling.HandleErrors(ex);
            }
        }

        /// <summary>
        /// Delete the indicated record from the database
        /// </summary>
        /// <param name="obj">Pointer to the desired record for the operation</param>
        protected override void DeleteRecord(object obj)
        {
            // Find the record
            var record = obj as letter_type;
            if (obj == null)
            {
                return;
            }

            if (DebtPlus.Data.Prompts.RequestConfirmation_Delete() != System.Windows.Forms.DialogResult.Yes)
            {
                return;
            }

            try
            {
                bc.letter_types.DeleteOnSubmit(record);
                bc.SubmitChanges();

                colRecords.Remove(record);
                gridView1.RefreshData();
            }
            catch (System.Data.SqlClient.SqlException ex)
            {
                DebtPlus.UI.Common.ErrorHandling.HandleErrors(ex);
            }
        }

        /// <summary>
        /// Class to format the region information
        /// </summary>
        private class RegionFormatter : System.IFormatProvider, System.ICustomFormatter
        {
            /// <summary>
            /// Initialize the new class instance
            /// </summary>
            internal RegionFormatter() { }

            /// <summary>
            /// Return the formatter for the specific type of argument
            /// </summary>
            public object GetFormat(Type formatType)
            {
                // We take all comers.
                return this;
            }

            /// <summary>
            /// Format the value passed to a string.
            /// </summary>
            public string Format(string format, object arg, IFormatProvider formatProvider)
            {
                // Find the key value. There should be one.
                var intKey = DebtPlus.Utils.Nulls.v_Int32(arg);
                if (intKey.HasValue)
                {
                    // If the value is the magic marker for "ALL" then use it.
                    if (intKey.Value == 0)
                    {
                        return "All Regions";
                    }

                    // Otherwise, translate the region through the tables.
                    var q = DebtPlus.LINQ.Cache.region.getList().Find(s => s.Id == intKey.Value);
                    if (q != null)
                    {
                        return q.description;
                    }
                }

                // The region is not defined. It is empty.
                return string.Empty;
            }
        }

        /// <summary>
        /// Class to format the letter class information
        /// </summary>
        private class LetterClassFormatter : System.IFormatProvider, System.ICustomFormatter
        {
            /// <summary>
            /// Initialize the new class instance
            /// </summary>
            internal LetterClassFormatter() { }

            /// <summary>
            /// Return the formatter for the specific type of argument
            /// </summary>
            public object GetFormat(Type formatType)
            {
                // We take all comers.
                return this;
            }

            /// <summary>
            /// Format the value passed to a string.
            /// </summary>
            public string Format(string format, object arg, IFormatProvider formatProvider)
            {
                string key = DebtPlus.Utils.Nulls.v_String(arg);
                if (!string.IsNullOrEmpty(key))
                {
                    var q = DebtPlus.LINQ.InMemory.LetterGroupTypes.getList().Find(s => s.Id == key);
                    if (q != null)
                    {
                        return q.description;
                    }
                }

                return string.Empty;
            }
        }
    }
}