using System;
using System.Windows.Forms;
using System.Drawing;
using DebtPlus.LINQ;
using System.Linq;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Diagnostics;
using DebtPlus.UI.Common.Templates;

namespace DebtPlus.UI.TableAdministration.CS.Letter.Types
{
	partial class EditForm
	{
		//Form overrides dispose to clean up the component list.
		[System.Diagnostics.DebuggerNonUserCode]
		protected override void Dispose(bool disposing)
		{
			try 
            {
				if (disposing)
                {
                    if (components != null) components.Dispose();
				}
                components = null;
            }
    	    finally
            {
				base.Dispose(disposing);
			}
		}

		//Required by the Windows Form Designer
		private System.ComponentModel.IContainer components = null;

		//NOTE: The following procedure is required by the Windows Form Designer
		//It can be modified using the Windows Form Designer.  
		//Do not modify it using the code editor.
		[System.Diagnostics.DebuggerStepThrough]
		private void InitializeComponent()
		{
			this.LayoutControl1 = new DevExpress.XtraLayout.LayoutControl();
			this.CheckEdit_default = new DevExpress.XtraEditors.CheckEdit();
			this.TextEdit_right_margin = new DevExpress.XtraEditors.TextEdit();
			this.TextEdit_bottom_margin = new DevExpress.XtraEditors.TextEdit();
			this.TextEdit_left_margin = new DevExpress.XtraEditors.TextEdit();
			this.TextEdit_top_margin = new DevExpress.XtraEditors.TextEdit();
			this.LookUpEdit_display_mode = new DevExpress.XtraEditors.LookUpEdit();
			this.TextEdit_queue_name = new DevExpress.XtraEditors.TextEdit();
			this.TextEdit_menu_name = new DevExpress.XtraEditors.TextEdit();
			this.LookUpEdit_letter_group = new DevExpress.XtraEditors.LookUpEdit();
			this.LookUpEdit_language = new DevExpress.XtraEditors.LookUpEdit();
			this.ButtonEdit_filename = new DevExpress.XtraEditors.ButtonEdit();
			this.LookUpEdit_region = new DevExpress.XtraEditors.LookUpEdit();
			this.TextEdit_description = new DevExpress.XtraEditors.TextEdit();
			this.TextEdit_letter_code = new DevExpress.XtraEditors.TextEdit();
			this.LayoutControlGroup1 = new DevExpress.XtraLayout.LayoutControlGroup();
			this.LayoutControlItem1 = new DevExpress.XtraLayout.LayoutControlItem();
			this.LayoutControlItem2 = new DevExpress.XtraLayout.LayoutControlItem();
			this.LayoutControlItem3 = new DevExpress.XtraLayout.LayoutControlItem();
			this.LayoutControlItem4 = new DevExpress.XtraLayout.LayoutControlItem();
			this.LayoutControlItem5 = new DevExpress.XtraLayout.LayoutControlItem();
			this.LayoutControlItem6 = new DevExpress.XtraLayout.LayoutControlItem();
			this.LayoutControlItem7 = new DevExpress.XtraLayout.LayoutControlItem();
			this.LayoutControlItem8 = new DevExpress.XtraLayout.LayoutControlItem();
			this.LayoutControlItem9 = new DevExpress.XtraLayout.LayoutControlItem();
			this.LayoutControlGroup2 = new DevExpress.XtraLayout.LayoutControlGroup();
			this.LayoutControlItem10 = new DevExpress.XtraLayout.LayoutControlItem();
			this.LayoutControlItem12 = new DevExpress.XtraLayout.LayoutControlItem();
			this.LayoutControlItem11 = new DevExpress.XtraLayout.LayoutControlItem();
			this.LayoutControlItem13 = new DevExpress.XtraLayout.LayoutControlItem();
			this.LayoutControlItem14 = new DevExpress.XtraLayout.LayoutControlItem();
			((System.ComponentModel.ISupportInitialize)this.DxErrorProvider1).BeginInit();
			((System.ComponentModel.ISupportInitialize)this.LayoutControl1).BeginInit();
			this.LayoutControl1.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)this.CheckEdit_default.Properties).BeginInit();
			((System.ComponentModel.ISupportInitialize)this.TextEdit_right_margin.Properties).BeginInit();
			((System.ComponentModel.ISupportInitialize)this.TextEdit_bottom_margin.Properties).BeginInit();
			((System.ComponentModel.ISupportInitialize)this.TextEdit_left_margin.Properties).BeginInit();
			((System.ComponentModel.ISupportInitialize)this.TextEdit_top_margin.Properties).BeginInit();
			((System.ComponentModel.ISupportInitialize)this.LookUpEdit_display_mode.Properties).BeginInit();
			((System.ComponentModel.ISupportInitialize)this.TextEdit_queue_name.Properties).BeginInit();
			((System.ComponentModel.ISupportInitialize)this.TextEdit_menu_name.Properties).BeginInit();
			((System.ComponentModel.ISupportInitialize)this.LookUpEdit_letter_group.Properties).BeginInit();
			((System.ComponentModel.ISupportInitialize)this.LookUpEdit_language.Properties).BeginInit();
			((System.ComponentModel.ISupportInitialize)this.ButtonEdit_filename.Properties).BeginInit();
			((System.ComponentModel.ISupportInitialize)this.LookUpEdit_region.Properties).BeginInit();
			((System.ComponentModel.ISupportInitialize)this.TextEdit_description.Properties).BeginInit();
			((System.ComponentModel.ISupportInitialize)this.TextEdit_letter_code.Properties).BeginInit();
			((System.ComponentModel.ISupportInitialize)this.LayoutControlGroup1).BeginInit();
			((System.ComponentModel.ISupportInitialize)this.LayoutControlItem1).BeginInit();
			((System.ComponentModel.ISupportInitialize)this.LayoutControlItem2).BeginInit();
			((System.ComponentModel.ISupportInitialize)this.LayoutControlItem3).BeginInit();
			((System.ComponentModel.ISupportInitialize)this.LayoutControlItem4).BeginInit();
			((System.ComponentModel.ISupportInitialize)this.LayoutControlItem5).BeginInit();
			((System.ComponentModel.ISupportInitialize)this.LayoutControlItem6).BeginInit();
			((System.ComponentModel.ISupportInitialize)this.LayoutControlItem7).BeginInit();
			((System.ComponentModel.ISupportInitialize)this.LayoutControlItem8).BeginInit();
			((System.ComponentModel.ISupportInitialize)this.LayoutControlItem9).BeginInit();
			((System.ComponentModel.ISupportInitialize)this.LayoutControlGroup2).BeginInit();
			((System.ComponentModel.ISupportInitialize)this.LayoutControlItem10).BeginInit();
			((System.ComponentModel.ISupportInitialize)this.LayoutControlItem12).BeginInit();
			((System.ComponentModel.ISupportInitialize)this.LayoutControlItem11).BeginInit();
			((System.ComponentModel.ISupportInitialize)this.LayoutControlItem13).BeginInit();
			((System.ComponentModel.ISupportInitialize)this.LayoutControlItem14).BeginInit();
			this.SuspendLayout();
			//
			//simpleButton_OK
			//
			this.simpleButton_OK.Location = new System.Drawing.Point(318, 17);
			this.simpleButton_OK.TabIndex = 10;
			//
			//simpleButton_Cancel
			//
			this.simpleButton_Cancel.Location = new System.Drawing.Point(318, 60);
			this.simpleButton_Cancel.TabIndex = 11;
			//
			//LayoutControl1
			//
			this.LayoutControl1.Anchor = (AnchorStyles)(((AnchorStyles.Top | AnchorStyles.Bottom) | AnchorStyles.Left) | AnchorStyles.Right);
			this.LayoutControl1.Controls.Add(this.CheckEdit_default);
			this.LayoutControl1.Controls.Add(this.TextEdit_right_margin);
			this.LayoutControl1.Controls.Add(this.TextEdit_bottom_margin);
			this.LayoutControl1.Controls.Add(this.TextEdit_left_margin);
			this.LayoutControl1.Controls.Add(this.TextEdit_top_margin);
			this.LayoutControl1.Controls.Add(this.LookUpEdit_display_mode);
			this.LayoutControl1.Controls.Add(this.TextEdit_queue_name);
			this.LayoutControl1.Controls.Add(this.TextEdit_menu_name);
			this.LayoutControl1.Controls.Add(this.LookUpEdit_letter_group);
			this.LayoutControl1.Controls.Add(this.LookUpEdit_language);
			this.LayoutControl1.Controls.Add(this.ButtonEdit_filename);
			this.LayoutControl1.Controls.Add(this.LookUpEdit_region);
			this.LayoutControl1.Controls.Add(this.TextEdit_description);
			this.LayoutControl1.Controls.Add(this.TextEdit_letter_code);
			this.LayoutControl1.Location = new System.Drawing.Point(2, 1);
			this.LayoutControl1.Name = "LayoutControl1";
			this.LayoutControl1.Root = this.LayoutControlGroup1;
			this.LayoutControl1.Size = new System.Drawing.Size(309, 313);
			this.LayoutControl1.TabIndex = 12;
			this.LayoutControl1.Text = "LayoutControl1";
			//
			//CheckEdit_default
			//
			this.CheckEdit_default.Location = new System.Drawing.Point(156, 2);
			this.CheckEdit_default.Name = "CheckEdit_default";
			this.CheckEdit_default.Properties.Caption = "Default";
			this.CheckEdit_default.Size = new System.Drawing.Size(151, 19);
			this.CheckEdit_default.StyleController = this.LayoutControl1;
			this.CheckEdit_default.TabIndex = 17;
			//
			//TextEdit_right_margin
			//
			this.TextEdit_right_margin.Location = new System.Drawing.Point(229, 274);
			this.TextEdit_right_margin.Name = "TextEdit_right_margin";
			this.TextEdit_right_margin.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.False;
			this.TextEdit_right_margin.Properties.Appearance.Options.UseTextOptions = true;
			this.TextEdit_right_margin.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
			this.TextEdit_right_margin.Properties.DisplayFormat.FormatString = "{0:f2}";
			this.TextEdit_right_margin.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
			this.TextEdit_right_margin.Properties.EditFormat.FormatString = "{0:f2}";
			this.TextEdit_right_margin.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
			this.TextEdit_right_margin.Properties.Mask.BeepOnError = true;
			this.TextEdit_right_margin.Properties.Mask.EditMask = "f3";
			this.TextEdit_right_margin.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
			this.TextEdit_right_margin.Size = new System.Drawing.Size(66, 20);
			this.TextEdit_right_margin.StyleController = this.LayoutControl1;
			this.TextEdit_right_margin.TabIndex = 16;
			//
			//TextEdit_bottom_margin
			//
			this.TextEdit_bottom_margin.Location = new System.Drawing.Point(87, 274);
			this.TextEdit_bottom_margin.Name = "TextEdit_bottom_margin";
			this.TextEdit_bottom_margin.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.False;
			this.TextEdit_bottom_margin.Properties.Appearance.Options.UseTextOptions = true;
			this.TextEdit_bottom_margin.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
			this.TextEdit_bottom_margin.Properties.DisplayFormat.FormatString = "{0:f2}";
			this.TextEdit_bottom_margin.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
			this.TextEdit_bottom_margin.Properties.EditFormat.FormatString = "{0:f2}";
			this.TextEdit_bottom_margin.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
			this.TextEdit_bottom_margin.Properties.Mask.BeepOnError = true;
			this.TextEdit_bottom_margin.Properties.Mask.EditMask = "f3";
			this.TextEdit_bottom_margin.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
			this.TextEdit_bottom_margin.Size = new System.Drawing.Size(65, 20);
			this.TextEdit_bottom_margin.StyleController = this.LayoutControl1;
			this.TextEdit_bottom_margin.TabIndex = 15;
			//
			//TextEdit_left_margin
			//
			this.TextEdit_left_margin.Location = new System.Drawing.Point(229, 250);
			this.TextEdit_left_margin.Name = "TextEdit_left_margin";
			this.TextEdit_left_margin.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.False;
			this.TextEdit_left_margin.Properties.Appearance.Options.UseTextOptions = true;
			this.TextEdit_left_margin.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
			this.TextEdit_left_margin.Properties.DisplayFormat.FormatString = "{0:f2}";
			this.TextEdit_left_margin.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
			this.TextEdit_left_margin.Properties.EditFormat.FormatString = "{0:f2}";
			this.TextEdit_left_margin.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
			this.TextEdit_left_margin.Properties.Mask.BeepOnError = true;
			this.TextEdit_left_margin.Properties.Mask.EditMask = "f3";
			this.TextEdit_left_margin.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
			this.TextEdit_left_margin.Size = new System.Drawing.Size(66, 20);
			this.TextEdit_left_margin.StyleController = this.LayoutControl1;
			this.TextEdit_left_margin.TabIndex = 14;
			//
			//TextEdit_top_margin
			//
			this.TextEdit_top_margin.Location = new System.Drawing.Point(87, 250);
			this.TextEdit_top_margin.Name = "TextEdit_top_margin";
			this.TextEdit_top_margin.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.False;
			this.TextEdit_top_margin.Properties.Appearance.Options.UseTextOptions = true;
			this.TextEdit_top_margin.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
			this.TextEdit_top_margin.Properties.DisplayFormat.FormatString = "{0:f2}";
			this.TextEdit_top_margin.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
			this.TextEdit_top_margin.Properties.EditFormat.FormatString = "{0:f2}";
			this.TextEdit_top_margin.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
			this.TextEdit_top_margin.Properties.Mask.BeepOnError = true;
			this.TextEdit_top_margin.Properties.Mask.EditMask = "f3";
			this.TextEdit_top_margin.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
			this.TextEdit_top_margin.Size = new System.Drawing.Size(65, 20);
			this.TextEdit_top_margin.StyleController = this.LayoutControl1;
			this.TextEdit_top_margin.TabIndex = 13;
			//
			//LookUpEdit_display_mode
			//
			this.LookUpEdit_display_mode.Location = new System.Drawing.Point(75, 194);
			this.LookUpEdit_display_mode.Name = "LookUpEdit_display_mode";
			this.LookUpEdit_display_mode.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.False;
			this.LookUpEdit_display_mode.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] { new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo) });
			this.LookUpEdit_display_mode.Properties.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
				new DevExpress.XtraEditors.Controls.LookUpColumnInfo("Id", "ID", 20, DevExpress.Utils.FormatType.None, "", false, DevExpress.Utils.HorzAlignment.Default),
				new DevExpress.XtraEditors.Controls.LookUpColumnInfo("description", "Description", 20, DevExpress.Utils.FormatType.None, "", true, DevExpress.Utils.HorzAlignment.Default, DevExpress.Data.ColumnSortOrder.Ascending)
			});
			this.LookUpEdit_display_mode.Properties.DisplayMember = "description";
			this.LookUpEdit_display_mode.Properties.NullText = "";
			this.LookUpEdit_display_mode.Properties.ShowFooter = false;
			this.LookUpEdit_display_mode.Properties.ShowHeader = false;
			this.LookUpEdit_display_mode.Properties.ValueMember = "Id";
			this.LookUpEdit_display_mode.Size = new System.Drawing.Size(232, 20);
			this.LookUpEdit_display_mode.StyleController = this.LayoutControl1;
			this.LookUpEdit_display_mode.TabIndex = 12;
			this.LookUpEdit_display_mode.Properties.SortColumnIndex = 1;
			//
			//TextEdit_queue_name
			//
			this.TextEdit_queue_name.Location = new System.Drawing.Point(75, 170);
			this.TextEdit_queue_name.Name = "TextEdit_queue_name";
			this.TextEdit_queue_name.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.True;
			this.TextEdit_queue_name.Properties.MaxLength = 50;
			this.TextEdit_queue_name.Size = new System.Drawing.Size(232, 20);
			this.TextEdit_queue_name.StyleController = this.LayoutControl1;
			this.TextEdit_queue_name.TabIndex = 11;
			//
			//TextEdit_menu_name
			//
			this.TextEdit_menu_name.Location = new System.Drawing.Point(75, 146);
			this.TextEdit_menu_name.Name = "TextEdit_menu_name";
			this.TextEdit_menu_name.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.True;
			this.TextEdit_menu_name.Properties.MaxLength = 256;
			this.TextEdit_menu_name.Size = new System.Drawing.Size(232, 20);
			this.TextEdit_menu_name.StyleController = this.LayoutControl1;
			this.TextEdit_menu_name.TabIndex = 10;
			//
			//LookUpEdit_letter_group
			//
			this.LookUpEdit_letter_group.Location = new System.Drawing.Point(75, 122);
			this.LookUpEdit_letter_group.Name = "LookUpEdit_letter_group";
			this.LookUpEdit_letter_group.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.False;
			this.LookUpEdit_letter_group.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] { new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo) });
			this.LookUpEdit_letter_group.Properties.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
				new DevExpress.XtraEditors.Controls.LookUpColumnInfo("Id", "ID", 20, DevExpress.Utils.FormatType.None, "", false, DevExpress.Utils.HorzAlignment.Default),
				new DevExpress.XtraEditors.Controls.LookUpColumnInfo("description", "Description", 20, DevExpress.Utils.FormatType.None, "", true, DevExpress.Utils.HorzAlignment.Default, DevExpress.Data.ColumnSortOrder.Ascending)
			});
			this.LookUpEdit_letter_group.Properties.DisplayMember = "description";
			this.LookUpEdit_letter_group.Properties.NullText = "";
			this.LookUpEdit_letter_group.Properties.ShowFooter = false;
			this.LookUpEdit_letter_group.Properties.ShowHeader = false;
			this.LookUpEdit_letter_group.Properties.ValueMember = "Id";
			this.LookUpEdit_letter_group.Size = new System.Drawing.Size(232, 20);
			this.LookUpEdit_letter_group.StyleController = this.LayoutControl1;
			this.LookUpEdit_letter_group.TabIndex = 9;
			this.LookUpEdit_letter_group.Properties.SortColumnIndex = 1;
			//
			//LookUpEdit_language
			//
			this.LookUpEdit_language.Location = new System.Drawing.Point(75, 74);
			this.LookUpEdit_language.Name = "LookUpEdit_language";
			this.LookUpEdit_language.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.False;
			this.LookUpEdit_language.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] { new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo) });
			this.LookUpEdit_language.Properties.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
				new DevExpress.XtraEditors.Controls.LookUpColumnInfo("Id", "ID", 20, DevExpress.Utils.FormatType.Numeric, "f0", false, DevExpress.Utils.HorzAlignment.Default),
				new DevExpress.XtraEditors.Controls.LookUpColumnInfo("Attribute", "Description", 20, DevExpress.Utils.FormatType.None, "", true, DevExpress.Utils.HorzAlignment.Default, DevExpress.Data.ColumnSortOrder.Ascending)
			});
			this.LookUpEdit_language.Properties.DisplayMember = "Attribute";
			this.LookUpEdit_language.Properties.NullText = "";
			this.LookUpEdit_language.Properties.ShowFooter = false;
			this.LookUpEdit_language.Properties.ShowHeader = false;
			this.LookUpEdit_language.Properties.ValueMember = "Id";
			this.LookUpEdit_language.Size = new System.Drawing.Size(232, 20);
			this.LookUpEdit_language.StyleController = this.LayoutControl1;
			this.LookUpEdit_language.TabIndex = 8;
			this.LookUpEdit_language.Properties.SortColumnIndex = 1;
			//
			//ButtonEdit_filename
			//
			this.ButtonEdit_filename.Location = new System.Drawing.Point(75, 50);
			this.ButtonEdit_filename.Name = "ButtonEdit_filename";
			this.ButtonEdit_filename.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.False;
			this.ButtonEdit_filename.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] { new DevExpress.XtraEditors.Controls.EditorButton() });
			this.ButtonEdit_filename.Properties.MaxLength = 255;
			this.ButtonEdit_filename.Size = new System.Drawing.Size(232, 20);
			this.ButtonEdit_filename.StyleController = this.LayoutControl1;
			this.ButtonEdit_filename.TabIndex = 7;
			//
			//LookUpEdit_region
			//
			this.LookUpEdit_region.Location = new System.Drawing.Point(75, 98);
			this.LookUpEdit_region.Name = "LookUpEdit_region";
			this.LookUpEdit_region.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.True;
			this.LookUpEdit_region.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] { new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo) });
			this.LookUpEdit_region.Properties.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
				new DevExpress.XtraEditors.Controls.LookUpColumnInfo("Id", "ID", 20, DevExpress.Utils.FormatType.Numeric, "f0", false, DevExpress.Utils.HorzAlignment.Default),
				new DevExpress.XtraEditors.Controls.LookUpColumnInfo("description", "Description", 20, DevExpress.Utils.FormatType.None, "", true, DevExpress.Utils.HorzAlignment.Default, DevExpress.Data.ColumnSortOrder.Ascending)
			});
			this.LookUpEdit_region.Properties.DisplayMember = "description";
			this.LookUpEdit_region.Properties.NullText = "";
			this.LookUpEdit_region.Properties.ShowFooter = false;
			this.LookUpEdit_region.Properties.ShowHeader = false;
			this.LookUpEdit_region.Properties.ValueMember = "Id";
			this.LookUpEdit_region.Size = new System.Drawing.Size(232, 20);
			this.LookUpEdit_region.StyleController = this.LayoutControl1;
			this.LookUpEdit_region.TabIndex = 6;
			this.LookUpEdit_region.Properties.SortColumnIndex = 1;
			//
			//TextEdit_description
			//
			this.TextEdit_description.Location = new System.Drawing.Point(75, 26);
			this.TextEdit_description.Name = "TextEdit_description";
			this.TextEdit_description.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.False;
			this.TextEdit_description.Properties.MaxLength = 50;
			this.TextEdit_description.Size = new System.Drawing.Size(232, 20);
			this.TextEdit_description.StyleController = this.LayoutControl1;
			this.TextEdit_description.TabIndex = 5;
			//
			//TextEdit_letter_code
			//
			this.TextEdit_letter_code.Location = new System.Drawing.Point(75, 2);
			this.TextEdit_letter_code.Name = "TextEdit_letter_code";
			this.TextEdit_letter_code.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.False;
			this.TextEdit_letter_code.Properties.CharacterCasing = CharacterCasing.Upper;
			this.TextEdit_letter_code.Properties.MaxLength = 10;
			this.TextEdit_letter_code.Size = new System.Drawing.Size(77, 20);
			this.TextEdit_letter_code.StyleController = this.LayoutControl1;
			this.TextEdit_letter_code.TabIndex = 4;
			//
			//LayoutControlGroup1
			//
			this.LayoutControlGroup1.CustomizationFormText = "LayoutControlGroup1";
			this.LayoutControlGroup1.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.True;
			this.LayoutControlGroup1.GroupBordersVisible = false;
			this.LayoutControlGroup1.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
				this.LayoutControlItem1,
				this.LayoutControlItem2,
				this.LayoutControlItem3,
				this.LayoutControlItem4,
				this.LayoutControlItem5,
				this.LayoutControlItem6,
				this.LayoutControlItem7,
				this.LayoutControlItem8,
				this.LayoutControlItem9,
				this.LayoutControlGroup2,
				this.LayoutControlItem14
			});
			this.LayoutControlGroup1.Location = new System.Drawing.Point(0, 0);
			this.LayoutControlGroup1.Name = "LayoutControlGroup1";
			this.LayoutControlGroup1.Padding = new DevExpress.XtraLayout.Utils.Padding(0, 0, 0, 0);
			this.LayoutControlGroup1.Size = new System.Drawing.Size(309, 313);
			this.LayoutControlGroup1.Spacing = new DevExpress.XtraLayout.Utils.Padding(0, 0, 0, 0);
			this.LayoutControlGroup1.Text = "LayoutControlGroup1";
			this.LayoutControlGroup1.TextVisible = false;
			//
			//LayoutControlItem1
			//
			this.LayoutControlItem1.Control = this.TextEdit_letter_code;
			this.LayoutControlItem1.CustomizationFormText = "ID";
			this.LayoutControlItem1.Location = new System.Drawing.Point(0, 0);
			this.LayoutControlItem1.Name = "LayoutControlItem1";
			this.LayoutControlItem1.Size = new System.Drawing.Size(154, 24);
			this.LayoutControlItem1.Text = "ID";
			this.LayoutControlItem1.TextSize = new System.Drawing.Size(69, 13);
			//
			//LayoutControlItem2
			//
			this.LayoutControlItem2.Control = this.TextEdit_description;
			this.LayoutControlItem2.CustomizationFormText = "Description";
			this.LayoutControlItem2.Location = new System.Drawing.Point(0, 24);
			this.LayoutControlItem2.Name = "LayoutControlItem2";
			this.LayoutControlItem2.Size = new System.Drawing.Size(309, 24);
			this.LayoutControlItem2.Text = "Description";
			this.LayoutControlItem2.TextSize = new System.Drawing.Size(69, 13);
			//
			//LayoutControlItem3
			//
			this.LayoutControlItem3.Control = this.LookUpEdit_region;
			this.LayoutControlItem3.CustomizationFormText = "Region";
			this.LayoutControlItem3.Location = new System.Drawing.Point(0, 96);
			this.LayoutControlItem3.Name = "LayoutControlItem3";
			this.LayoutControlItem3.Size = new System.Drawing.Size(309, 24);
			this.LayoutControlItem3.Text = "Region";
			this.LayoutControlItem3.TextSize = new System.Drawing.Size(69, 13);
			//
			//LayoutControlItem4
			//
			this.LayoutControlItem4.Control = this.ButtonEdit_filename;
			this.LayoutControlItem4.CustomizationFormText = "Filename";
			this.LayoutControlItem4.Location = new System.Drawing.Point(0, 48);
			this.LayoutControlItem4.Name = "LayoutControlItem4";
			this.LayoutControlItem4.Size = new System.Drawing.Size(309, 24);
			this.LayoutControlItem4.Text = "Filename";
			this.LayoutControlItem4.TextSize = new System.Drawing.Size(69, 13);
			//
			//LayoutControlItem5
			//
			this.LayoutControlItem5.Control = this.LookUpEdit_language;
			this.LayoutControlItem5.CustomizationFormText = "Language";
			this.LayoutControlItem5.Location = new System.Drawing.Point(0, 72);
			this.LayoutControlItem5.Name = "LayoutControlItem5";
			this.LayoutControlItem5.Size = new System.Drawing.Size(309, 24);
			this.LayoutControlItem5.Text = "Language";
			this.LayoutControlItem5.TextSize = new System.Drawing.Size(69, 13);
			//
			//LayoutControlItem6
			//
			this.LayoutControlItem6.Control = this.LookUpEdit_letter_group;
			this.LayoutControlItem6.CustomizationFormText = "Menu Location";
			this.LayoutControlItem6.Location = new System.Drawing.Point(0, 120);
			this.LayoutControlItem6.Name = "LayoutControlItem6";
			this.LayoutControlItem6.Size = new System.Drawing.Size(309, 24);
			this.LayoutControlItem6.Text = "Menu Location";
			this.LayoutControlItem6.TextSize = new System.Drawing.Size(69, 13);
			//
			//LayoutControlItem7
			//
			this.LayoutControlItem7.Control = this.TextEdit_menu_name;
			this.LayoutControlItem7.CustomizationFormText = "Menu Name";
			this.LayoutControlItem7.Location = new System.Drawing.Point(0, 144);
			this.LayoutControlItem7.Name = "LayoutControlItem7";
			this.LayoutControlItem7.Size = new System.Drawing.Size(309, 24);
			this.LayoutControlItem7.Text = "Menu Name";
			this.LayoutControlItem7.TextSize = new System.Drawing.Size(69, 13);
			//
			//LayoutControlItem8
			//
			this.LayoutControlItem8.Control = this.TextEdit_queue_name;
			this.LayoutControlItem8.CustomizationFormText = "Queue Name";
			this.LayoutControlItem8.Location = new System.Drawing.Point(0, 168);
			this.LayoutControlItem8.Name = "LayoutControlItem8";
			this.LayoutControlItem8.Size = new System.Drawing.Size(309, 24);
			this.LayoutControlItem8.Text = "Queue Name";
			this.LayoutControlItem8.TextSize = new System.Drawing.Size(69, 13);
			//
			//LayoutControlItem9
			//
			this.LayoutControlItem9.Control = this.LookUpEdit_display_mode;
			this.LayoutControlItem9.CustomizationFormText = "Display Mode";
			this.LayoutControlItem9.Location = new System.Drawing.Point(0, 192);
			this.LayoutControlItem9.Name = "LayoutControlItem9";
			this.LayoutControlItem9.Size = new System.Drawing.Size(309, 24);
			this.LayoutControlItem9.Text = "Display Mode";
			this.LayoutControlItem9.TextSize = new System.Drawing.Size(69, 13);
			//
			//LayoutControlGroup2
			//
			this.LayoutControlGroup2.CustomizationFormText = "Margins";
			this.LayoutControlGroup2.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
				this.LayoutControlItem10,
				this.LayoutControlItem12,
				this.LayoutControlItem11,
				this.LayoutControlItem13
			});
			this.LayoutControlGroup2.Location = new System.Drawing.Point(0, 216);
			this.LayoutControlGroup2.Name = "LayoutControlGroup2";
			this.LayoutControlGroup2.Size = new System.Drawing.Size(309, 97);
			this.LayoutControlGroup2.Text = "Margins";
			//
			//LayoutControlItem10
			//
			this.LayoutControlItem10.Control = this.TextEdit_top_margin;
			this.LayoutControlItem10.CustomizationFormText = "Top Margin";
			this.LayoutControlItem10.Location = new System.Drawing.Point(0, 0);
			this.LayoutControlItem10.Name = "LayoutControlItem10";
			this.LayoutControlItem10.Size = new System.Drawing.Size(142, 24);
			this.LayoutControlItem10.Text = "Top";
			this.LayoutControlItem10.TextSize = new System.Drawing.Size(69, 13);
			//
			//LayoutControlItem12
			//
			this.LayoutControlItem12.Control = this.TextEdit_bottom_margin;
			this.LayoutControlItem12.CustomizationFormText = "Bottom Margin";
			this.LayoutControlItem12.Location = new System.Drawing.Point(0, 24);
			this.LayoutControlItem12.Name = "LayoutControlItem12";
			this.LayoutControlItem12.Size = new System.Drawing.Size(142, 29);
			this.LayoutControlItem12.Text = "Bottom";
			this.LayoutControlItem12.TextSize = new System.Drawing.Size(69, 13);
			//
			//LayoutControlItem11
			//
			this.LayoutControlItem11.Control = this.TextEdit_left_margin;
			this.LayoutControlItem11.CustomizationFormText = "Left Margin";
			this.LayoutControlItem11.Location = new System.Drawing.Point(142, 0);
			this.LayoutControlItem11.Name = "LayoutControlItem11";
			this.LayoutControlItem11.Size = new System.Drawing.Size(143, 24);
			this.LayoutControlItem11.Text = "Left";
			this.LayoutControlItem11.TextSize = new System.Drawing.Size(69, 13);
			//
			//LayoutControlItem13
			//
			this.LayoutControlItem13.Control = this.TextEdit_right_margin;
			this.LayoutControlItem13.CustomizationFormText = "Right Margin";
			this.LayoutControlItem13.Location = new System.Drawing.Point(142, 24);
			this.LayoutControlItem13.Name = "LayoutControlItem13";
			this.LayoutControlItem13.Size = new System.Drawing.Size(143, 29);
			this.LayoutControlItem13.Text = "Right";
			this.LayoutControlItem13.TextSize = new System.Drawing.Size(69, 13);
			//
			//LayoutControlItem14
			//
			this.LayoutControlItem14.Control = this.CheckEdit_default;
			this.LayoutControlItem14.CustomizationFormText = "Default Checkbox";
			this.LayoutControlItem14.Location = new System.Drawing.Point(154, 0);
			this.LayoutControlItem14.Name = "LayoutControlItem14";
			this.LayoutControlItem14.Size = new System.Drawing.Size(155, 24);
			this.LayoutControlItem14.Text = "Default";
			this.LayoutControlItem14.TextSize = new System.Drawing.Size(0, 0);
			this.LayoutControlItem14.TextToControlDistance = 0;
			this.LayoutControlItem14.TextVisible = false;
			//
			//EditForm
			//
			this.AutoScaleDimensions = new System.Drawing.SizeF(6f, 13f);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(412, 315);
			this.Controls.Add(this.LayoutControl1);
			this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.Sizable;
			this.Name = "EditForm";
			this.Text = "Letter Type";
			this.Controls.SetChildIndex(this.LayoutControl1, 0);
			this.Controls.SetChildIndex(this.simpleButton_OK, 0);
			this.Controls.SetChildIndex(this.simpleButton_Cancel, 0);
			((System.ComponentModel.ISupportInitialize)this.DxErrorProvider1).EndInit();
			((System.ComponentModel.ISupportInitialize)this.LayoutControl1).EndInit();
			this.LayoutControl1.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)this.CheckEdit_default.Properties).EndInit();
			((System.ComponentModel.ISupportInitialize)this.TextEdit_right_margin.Properties).EndInit();
			((System.ComponentModel.ISupportInitialize)this.TextEdit_bottom_margin.Properties).EndInit();
			((System.ComponentModel.ISupportInitialize)this.TextEdit_left_margin.Properties).EndInit();
			((System.ComponentModel.ISupportInitialize)this.TextEdit_top_margin.Properties).EndInit();
			((System.ComponentModel.ISupportInitialize)this.LookUpEdit_display_mode.Properties).EndInit();
			((System.ComponentModel.ISupportInitialize)this.TextEdit_queue_name.Properties).EndInit();
			((System.ComponentModel.ISupportInitialize)this.TextEdit_menu_name.Properties).EndInit();
			((System.ComponentModel.ISupportInitialize)this.LookUpEdit_letter_group.Properties).EndInit();
			((System.ComponentModel.ISupportInitialize)this.LookUpEdit_language.Properties).EndInit();
			((System.ComponentModel.ISupportInitialize)this.ButtonEdit_filename.Properties).EndInit();
			((System.ComponentModel.ISupportInitialize)this.LookUpEdit_region.Properties).EndInit();
			((System.ComponentModel.ISupportInitialize)this.TextEdit_description.Properties).EndInit();
			((System.ComponentModel.ISupportInitialize)this.TextEdit_letter_code.Properties).EndInit();
			((System.ComponentModel.ISupportInitialize)this.LayoutControlGroup1).EndInit();
			((System.ComponentModel.ISupportInitialize)this.LayoutControlItem1).EndInit();
			((System.ComponentModel.ISupportInitialize)this.LayoutControlItem2).EndInit();
			((System.ComponentModel.ISupportInitialize)this.LayoutControlItem3).EndInit();
			((System.ComponentModel.ISupportInitialize)this.LayoutControlItem4).EndInit();
			((System.ComponentModel.ISupportInitialize)this.LayoutControlItem5).EndInit();
			((System.ComponentModel.ISupportInitialize)this.LayoutControlItem6).EndInit();
			((System.ComponentModel.ISupportInitialize)this.LayoutControlItem7).EndInit();
			((System.ComponentModel.ISupportInitialize)this.LayoutControlItem8).EndInit();
			((System.ComponentModel.ISupportInitialize)this.LayoutControlItem9).EndInit();
			((System.ComponentModel.ISupportInitialize)this.LayoutControlGroup2).EndInit();
			((System.ComponentModel.ISupportInitialize)this.LayoutControlItem10).EndInit();
			((System.ComponentModel.ISupportInitialize)this.LayoutControlItem12).EndInit();
			((System.ComponentModel.ISupportInitialize)this.LayoutControlItem11).EndInit();
			((System.ComponentModel.ISupportInitialize)this.LayoutControlItem13).EndInit();
			((System.ComponentModel.ISupportInitialize)this.LayoutControlItem14).EndInit();
			this.ResumeLayout(false);

		}
		private DevExpress.XtraLayout.LayoutControl LayoutControl1;
		private DevExpress.XtraEditors.ButtonEdit ButtonEdit_filename;
		private DevExpress.XtraEditors.LookUpEdit LookUpEdit_region;
		private DevExpress.XtraEditors.TextEdit TextEdit_description;
		private DevExpress.XtraEditors.TextEdit TextEdit_letter_code;
		private DevExpress.XtraLayout.LayoutControlGroup LayoutControlGroup1;
		private DevExpress.XtraLayout.LayoutControlItem LayoutControlItem1;
		private DevExpress.XtraLayout.LayoutControlItem LayoutControlItem2;
		private DevExpress.XtraLayout.LayoutControlItem LayoutControlItem3;
		private DevExpress.XtraLayout.LayoutControlItem LayoutControlItem4;
		private DevExpress.XtraEditors.TextEdit TextEdit_bottom_margin;
		private DevExpress.XtraEditors.TextEdit TextEdit_left_margin;
		private DevExpress.XtraEditors.TextEdit TextEdit_top_margin;
		private DevExpress.XtraEditors.LookUpEdit LookUpEdit_display_mode;
		private DevExpress.XtraEditors.TextEdit TextEdit_queue_name;
		private DevExpress.XtraEditors.TextEdit TextEdit_menu_name;
		private DevExpress.XtraEditors.LookUpEdit LookUpEdit_letter_group;
		private DevExpress.XtraEditors.LookUpEdit LookUpEdit_language;
		private DevExpress.XtraLayout.LayoutControlItem LayoutControlItem5;
		private DevExpress.XtraLayout.LayoutControlItem LayoutControlItem6;
		private DevExpress.XtraLayout.LayoutControlItem LayoutControlItem7;
		private DevExpress.XtraLayout.LayoutControlItem LayoutControlItem8;
		private DevExpress.XtraLayout.LayoutControlItem LayoutControlItem9;
		private DevExpress.XtraLayout.LayoutControlItem LayoutControlItem10;
		private DevExpress.XtraLayout.LayoutControlItem LayoutControlItem11;
		private DevExpress.XtraLayout.LayoutControlItem LayoutControlItem12;
		private DevExpress.XtraEditors.CheckEdit CheckEdit_default;
		private DevExpress.XtraEditors.TextEdit TextEdit_right_margin;
		private DevExpress.XtraLayout.LayoutControlGroup LayoutControlGroup2;
		private DevExpress.XtraLayout.LayoutControlItem LayoutControlItem13;
		private DevExpress.XtraLayout.LayoutControlItem LayoutControlItem14;
	}
}

