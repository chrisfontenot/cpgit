#region "Copyright 2000-2012 DebtPlus, L.L.C."
//{*******************************************************************}
//{                                                                   }
//{       DebtPlus Debt Management System                             }
//{                                                                   }
//{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
//{       ALL RIGHTS RESERVED                                         }
//{                                                                   }
//{   The entire contents of this file is protected by U.S. and       }
//{   International Copyright Laws. Unauthorized reproduction,        }
//{   reverse-engineering, and distribution of all or any portion of  }
//{   the code contained in this file is strictly prohibited and may  }
//{   result in severe civil and criminal penalties and will be       }
//{   prosecuted to the maximum extent possible under the law.        }
//{                                                                   }
//{   RESTRICTIONS                                                    }
//{                                                                   }
//{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
//{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
//{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
//{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
//{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
//{                                                                   }
//{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
//{   ADDITIONAL RESTRICTIONS.                                        }
//{                                                                   }
//{*******************************************************************}

#endregion "Copyright 2000-2012 DebtPlus, L.L.C."

using System;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Windows.Forms;
using DebtPlus.LINQ;
using DevExpress.XtraEditors.Controls;

namespace DebtPlus.UI.TableAdministration.CS.Letter.Types
{
    internal partial class EditForm : Templates.EditTemplateForm
    {
        private letter_type record = null;

        internal EditForm()
            : base()
        {
            InitializeComponent();
        }

        internal EditForm(letter_type record)
            : this()
        {
            this.record = record;
            RegisterHandlers();
        }

        private void RegisterHandlers()
        {
            Load                                     += EditForm_Load;
            ButtonEdit_filename.ButtonPressed        += ButtonEdit_filename_ButtonPressed;
            TextEdit_left_margin.EditValueChanging   += Margin_EditValueChanging;
            LookUpEdit_letter_group.EditValueChanged += LookUpEdit_letter_group_EditValueChanged;
            ButtonEdit_filename.Validating           += ButtonEdit_filename_Validating;
            TextEdit_right_margin.EditValueChanging  += Margin_EditValueChanging;
            TextEdit_bottom_margin.EditValueChanging += Margin_EditValueChanging;
            TextEdit_top_margin.EditValueChanging    += Margin_EditValueChanging;
            LookUpEdit_display_mode.EditValueChanged += LookUpEdit_display_mode_EditValueChanged;
            ButtonEdit_filename.EditValueChanged     += FormChanged;
            CheckEdit_default.CheckStateChanged      += FormChanged;
            LookUpEdit_display_mode.EditValueChanged += FormChanged;
            LookUpEdit_language.EditValueChanged     += FormChanged;
            LookUpEdit_letter_group.EditValueChanged += FormChanged;
            TextEdit_left_margin.EditValueChanged    += FormChanged;
            TextEdit_description.EditValueChanged    += FormChanged;
            LookUpEdit_region.EditValueChanged       += FormChanged;
            TextEdit_queue_name.EditValueChanged     += FormChanged;
            TextEdit_bottom_margin.EditValueChanged  += FormChanged;
            TextEdit_menu_name.EditValueChanged      += FormChanged;
            TextEdit_right_margin.EditValueChanged   += FormChanged;
            TextEdit_top_margin.EditValueChanged     += FormChanged;
            TextEdit_letter_code.EditValueChanged    += FormChanged;
        }

        private void UnRegisterHandlers()
        {
            Load                                     -= EditForm_Load;
            ButtonEdit_filename.ButtonPressed        -= ButtonEdit_filename_ButtonPressed;
            TextEdit_left_margin.EditValueChanging   -= Margin_EditValueChanging;
            LookUpEdit_letter_group.EditValueChanged -= LookUpEdit_letter_group_EditValueChanged;
            ButtonEdit_filename.Validating           -= ButtonEdit_filename_Validating;
            TextEdit_right_margin.EditValueChanging  -= Margin_EditValueChanging;
            TextEdit_bottom_margin.EditValueChanging -= Margin_EditValueChanging;
            TextEdit_top_margin.EditValueChanging    -= Margin_EditValueChanging;
            LookUpEdit_display_mode.EditValueChanged -= LookUpEdit_display_mode_EditValueChanged;
            ButtonEdit_filename.EditValueChanged     -= FormChanged;
            CheckEdit_default.CheckStateChanged      -= FormChanged;
            LookUpEdit_display_mode.EditValueChanged -= FormChanged;
            LookUpEdit_language.EditValueChanged     -= FormChanged;
            LookUpEdit_letter_group.EditValueChanged -= FormChanged;
            TextEdit_left_margin.EditValueChanged    -= FormChanged;
            TextEdit_description.EditValueChanged    -= FormChanged;
            LookUpEdit_region.EditValueChanged       -= FormChanged;
            TextEdit_queue_name.EditValueChanged     -= FormChanged;
            TextEdit_bottom_margin.EditValueChanged  -= FormChanged;
            TextEdit_menu_name.EditValueChanged      -= FormChanged;
            TextEdit_right_margin.EditValueChanged   -= FormChanged;
            TextEdit_top_margin.EditValueChanged     -= FormChanged;
            TextEdit_letter_code.EditValueChanged    -= FormChanged;
        }

        /// <summary>
        /// Process the LOAD event
        /// </summary>
        private void EditForm_Load(object sender, EventArgs e)
        {
            UnRegisterHandlers();
            try
            {
                // Build a new list of regions that has the "All Regions" entry as id # 0
                System.Collections.Generic.List<region> colRegions = new System.Collections.Generic.List<region>();
                DebtPlus.LINQ.Cache.region.getList().FindAll(s => s.Id != 0).ForEach(s => colRegions.Add(new region() { ActiveFlag = s.ActiveFlag, Default = false, description = s.description, Id = s.Id, ManagerName = s.ManagerName }));
                colRegions.Add(new region() { description = "All Regions", Id = 0, ActiveFlag = true, Default = true, ManagerName = string.Empty });

                // Load the lookup controls with the proper lists
                LookUpEdit_region.Properties.DataSource       = colRegions;
                LookUpEdit_display_mode.Properties.DataSource = DebtPlus.LINQ.InMemory.letterDisplayModes.getList();
                LookUpEdit_language.Properties.DataSource     = DebtPlus.LINQ.Cache.AttributeType.getLanguageList();
                LookUpEdit_letter_group.Properties.DataSource = DebtPlus.LINQ.InMemory.LetterGroupTypes.getList();

                // Load the record into the editing controls
                ButtonEdit_filename.EditValue     = record.filename;
                CheckEdit_default.Checked         = record.Default;
                LookUpEdit_display_mode.EditValue = record.display_mode;
                LookUpEdit_language.EditValue     = record.language;
                LookUpEdit_letter_group.EditValue = record.letter_group;
                LookUpEdit_region.EditValue       = record.region;
                TextEdit_bottom_margin.EditValue  = record.bottom_margin;
                TextEdit_description.EditValue    = record.description;
                TextEdit_left_margin.EditValue    = record.left_margin;
                TextEdit_letter_code.EditValue    = record.letter_code;
                TextEdit_menu_name.EditValue      = record.menu_name;
                TextEdit_queue_name.EditValue     = record.queue_name;
                TextEdit_right_margin.EditValue   = record.right_margin;
                TextEdit_top_margin.EditValue     = record.top_margin;

                // Enable the controls based upon the record contents.
                EnableMenuName();
                EnableQueueName();

                // Process the OK button status
                simpleButton_OK.Enabled = !HasErrors();
            }
            finally
            {
                RegisterHandlers();
            }
        }

        protected override void simpleButton_OK_Click(object sender, EventArgs e)
        {
            record.filename      = DebtPlus.Utils.Nulls.v_String(ButtonEdit_filename.EditValue);
            record.Default       = CheckEdit_default.Checked;
            record.display_mode  = DebtPlus.Utils.Nulls.v_Int32(LookUpEdit_display_mode.EditValue).GetValueOrDefault();
            record.language      = DebtPlus.Utils.Nulls.v_Int32(LookUpEdit_language.EditValue).GetValueOrDefault();
            record.letter_group  = DebtPlus.Utils.Nulls.v_String(LookUpEdit_letter_group.EditValue);
            record.region        = DebtPlus.Utils.Nulls.v_Int32(LookUpEdit_region.EditValue).GetValueOrDefault();
            record.bottom_margin = DebtPlus.Utils.Nulls.v_Double(TextEdit_bottom_margin.EditValue).GetValueOrDefault();
            record.description   = DebtPlus.Utils.Nulls.v_String(TextEdit_description.EditValue);
            record.left_margin   = DebtPlus.Utils.Nulls.v_Double(TextEdit_left_margin.EditValue).GetValueOrDefault();
            record.letter_code   = DebtPlus.Utils.Nulls.v_String(TextEdit_letter_code.EditValue);
            record.menu_name     = DebtPlus.Utils.Nulls.v_String(TextEdit_menu_name.EditValue);
            record.queue_name    = DebtPlus.Utils.Nulls.v_String(TextEdit_queue_name.EditValue);
            record.right_margin  = DebtPlus.Utils.Nulls.v_Double(TextEdit_right_margin.EditValue).GetValueOrDefault();
            record.top_margin    = DebtPlus.Utils.Nulls.v_Double(TextEdit_top_margin.EditValue).GetValueOrDefault();

            base.simpleButton_OK_Click(sender, e);
        }

        /// <summary>
        /// Enable the OK button if there are no errors
        /// </summary>
        private void FormChanged(object Sender, EventArgs e)
        {
            simpleButton_OK.Enabled = !HasErrors();
        }

        /// <summary>
        /// Ensure that there are no errors on the form
        /// </summary>
        private bool HasErrors()
        {
            var groupID = LookUpEdit_letter_group.GetSelectedDataRow() as DebtPlus.LINQ.InMemory.LetterGroupType;
            var displayMode = LookUpEdit_display_mode.GetSelectedDataRow() as DebtPlus.LINQ.InMemory.letterDisplayMode;

            // These fields are required
            if (string.IsNullOrWhiteSpace(DebtPlus.Utils.Nulls.v_String(TextEdit_description.EditValue)))
            {
                return true;
            }

            if (string.IsNullOrWhiteSpace(DebtPlus.Utils.Nulls.v_String(ButtonEdit_filename.EditValue)))
            {
                return true;
            }

            if (string.IsNullOrWhiteSpace(DebtPlus.Utils.Nulls.v_String(TextEdit_letter_code.EditValue)))
            {
                return true;
            }

            if (!DebtPlus.Utils.Nulls.v_Int32(LookUpEdit_region.EditValue).HasValue)
            {
                return true;
            }

            if (!DebtPlus.Utils.Nulls.v_Int32(LookUpEdit_language.EditValue).HasValue)
            {
                return true;
            }

            if (groupID == null)
            {
                return true;
            }

            if (displayMode == null)
            {
                return true;
            }

            // An entry other than "OTHER" requires a menu name string.
            if (groupID.Id != "OTHER" && string.IsNullOrWhiteSpace(DebtPlus.Utils.Nulls.v_String(TextEdit_menu_name.EditValue)))
            {
                return true;
            }

            // A display mode of "QUEUE" requires a queue name
            if (displayMode.Id == 3 && string.IsNullOrWhiteSpace(DebtPlus.Utils.Nulls.v_String(TextEdit_queue_name.EditValue)))
            {
                return true;
            }

            return false;
        }

        /// <summary>
        /// Process the filename search button
        /// </summary>
        private void ButtonEdit_filename_ButtonPressed(object sender, ButtonPressedEventArgs e)
        {
            // Search button
            if (e.Button.Index == 0)
            {
                SearchFileName();
            }
        }

        /// <summary>
        /// Do the lookup for the letter file
        /// </summary>
        private void SearchFileName()
        {
            string baseDirectory = global::DebtPlus.Configuration.Config.LettersDirectory;
            string currentFname = Path.Combine(baseDirectory, ButtonEdit_filename.Text.Trim());

            try
            {
                // Ensure that the directory exists and there are no other problems found.
                FileAttributes status = File.GetAttributes(currentFname);
            }
#pragma warning disable 168
            catch (FileNotFoundException ex)
            {
            }
            catch (Exception ex)
            {
                baseDirectory = Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments);
            }
#pragma warning restore 168

            // Ask the user for a filename. The options are pretty standard, but set them anyway.
            using (var dlg = new OpenFileDialog()
                {
                    FileName           = currentFname,
                    Title              = "Letter Filename",
                    DefaultExt         = ".rtf",
                    Filter             = "Rich Text (*.rtf)|*.rtf|Open XML (*.docx)|*.docx|Open Document (*.odt)|*.odt|HTML document (*.htm,*.html)|*.htm;*.html|Text Documents (*.txt)|*.txt|All Files (*.*)|*.*",
                    FilterIndex        = 0,
                    InitialDirectory   = baseDirectory,
                    Multiselect        = false,
                    ShowReadOnly       = false,
                    ValidateNames      = true,
                    AddExtension       = true,
                    AutoUpgradeEnabled = true,
                    CheckFileExists    = true,
                    CheckPathExists    = true,
                    DereferenceLinks   = true
                })
            {
                dlg.FileOk += FileOK;

                if (dlg.ShowDialog() == DialogResult.OK)
                {
                    currentFname = dlg.FileName;

                    // Remove the directory it the value is the same as our base directory from the registry.
                    if (currentFname.StartsWith(baseDirectory, StringComparison.InvariantCultureIgnoreCase))
                    {
                        currentFname = currentFname.Substring(baseDirectory.Length);

                        // Sometimes the base name has the trailing slash. Sometimes it does not. It all depends
                        // upon the person doing the installation. Handle both cases.
                        currentFname = currentFname.TrimEnd(new char[] { System.IO.Path.DirectorySeparatorChar, System.IO.Path.AltDirectorySeparatorChar });
                    }

                    // Update the text field with the resulting filename
                    ButtonEdit_filename.Text = currentFname.Trim();
                }
                dlg.FileOk -= FileOK;
            }
        }

        /// <summary>
        /// Ensure that the file name does not have a DOC extension
        /// </summary>
        private void FileOK(object Sender, CancelEventArgs e)
        {
            string[] Names = ((FileDialog)Sender).FileNames;
            foreach (string Name in Names)
            {
                if (string.Compare(System.IO.Path.GetExtension(Name), ".doc", true) == 0)
                {
                    e.Cancel = true;
                    break;
                }
            }
        }

        /// <summary>
        /// Process a change in the margin fields
        /// </summary>
        private void Margin_EditValueChanging(object sender, ChangingEventArgs e)
        {
            if (e.NewValue != null && Convert.ToDouble(e.NewValue) < 0.0)
            {
                e.Cancel = true;
            }
        }

        /// <summary>
        /// Enable the menu option when there is a menu location
        /// </summary>
        private void LookUpEdit_letter_group_EditValueChanged(object sender, EventArgs e)
        {
            EnableMenuName();
        }

        /// <summary>
        /// Enable or disable the menu name field when it is needed.
        /// </summary>
        private void EnableMenuName()
        {
            var groupID = LookUpEdit_letter_group.GetSelectedDataRow() as DebtPlus.LINQ.InMemory.LetterGroupType;
            if (groupID != null && groupID.Id != "OTHER")
            {
                TextEdit_menu_name.Enabled = true;
                return;
            }
            TextEdit_menu_name.Enabled = false;
        }

        /// <summary>
        /// Check the filename as it is entered for a valid type
        /// </summary>
        private void ButtonEdit_filename_Validating(object sender, CancelEventArgs e)
        {
            string Name = ButtonEdit_filename.Text.Trim();
            string extensionName = System.IO.Path.GetExtension(Name).ToLower();
            if ((new string[] { ".odt", ".htm", ".html", ".docx", ".txt", ".rtf" }).Contains(extensionName))
            {
                return;
            }

            if (MessageBox.Show("The file type is not a recognized record and may not be a valid letter." + Environment.NewLine + "Are you sure that you want this name?" + Environment.NewLine + Environment.NewLine + "(Old Microsoft Word DOC files are NOT acceptable)", "Are you sure?", MessageBoxButtons.YesNo, MessageBoxIcon.Hand, MessageBoxDefaultButton.Button2) == DialogResult.No)
            {
                e.Cancel = true;
            }
        }

        private void LookUpEdit_display_mode_EditValueChanged(object sender, EventArgs e)
        {
            EnableQueueName();
        }

        private void EnableQueueName()
        {
            var displayMode = LookUpEdit_display_mode.GetSelectedDataRow() as DebtPlus.LINQ.InMemory.letterDisplayMode;
            if (displayMode != null && (displayMode.Id == 3 || displayMode.Id == 0))
            {
                TextEdit_queue_name.Enabled = true;
                return;
            }
            TextEdit_queue_name.Enabled = false;
        }
    }
}