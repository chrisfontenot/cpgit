#region "Copyright 2000-2012 DebtPlus, L.L.C."
//{*******************************************************************}
//{                                                                   }
//{       DebtPlus Debt Management System                             }
//{                                                                   }
//{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
//{       ALL RIGHTS RESERVED                                         }
//{                                                                   }
//{   The entire contents of this file is protected by U.S. and       }
//{   International Copyright Laws. Unauthorized reproduction,        }
//{   reverse-engineering, and distribution of all or any portion of  }
//{   the code contained in this file is strictly prohibited and may  }
//{   result in severe civil and criminal penalties and will be       }
//{   prosecuted to the maximum extent possible under the law.        }
//{                                                                   }
//{   RESTRICTIONS                                                    }
//{                                                                   }
//{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
//{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
//{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
//{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
//{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
//{                                                                   }
//{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
//{   ADDITIONAL RESTRICTIONS.                                        }
//{                                                                   }
//{*******************************************************************}

#endregion "Copyright 2000-2012 DebtPlus, L.L.C."

using System;
using DebtPlus.LINQ;

namespace DebtPlus.UI.TableAdministration.CS.Letter.Fields
{
    internal partial class EditForm : Templates.EditTemplateForm
    {
        internal EditForm()
            : base()
        {
            InitializeComponent();
        }

        private System.Collections.Generic.List<letter_field> colLetterFields;
        private System.Collections.Generic.List<letter_table> colLetterTables;
        private letter_field record;

        internal EditForm(System.Collections.Generic.List<letter_field> colLetterFields, System.Collections.Generic.List<letter_table> colLetterTables, letter_field record)
            : this()
        {
            this.record = record;
            this.colLetterFields = colLetterFields;
            this.colLetterTables = colLetterTables;

            RegisterHandlers();
        }

        private void RegisterHandlers()
        {
            this.Load += EditForm_Load;
            LookUpEdit_type.EditValueChanged += LookUpEdit_type_EditValueChanged;
            LookUpEdit_type.EditValueChanged += fieldChanged;
            LookUpEdit_letter_table.EditValueChanged += fieldChanged;
            TextEdit_letter_field.EditValueChanged += fieldChanged;
            TextEdit_formatting.EditValueChanged += fieldChanged;
            TextEdit_field_name.EditValueChanged += fieldChanged;
        }

        private void UnRegisterHandlers()
        {
            this.Load -= EditForm_Load;
            LookUpEdit_type.EditValueChanged -= LookUpEdit_type_EditValueChanged;
            LookUpEdit_type.EditValueChanged -= fieldChanged;
            LookUpEdit_letter_table.EditValueChanged -= fieldChanged;
            TextEdit_letter_field.EditValueChanged -= fieldChanged;
            TextEdit_formatting.EditValueChanged -= fieldChanged;
            TextEdit_field_name.EditValueChanged -= fieldChanged;
        }

        private void EditForm_Load(object sender, EventArgs e)
        {
            UnRegisterHandlers();
            try
            {
                LookUpEdit_letter_table.Properties.DataSource = colLetterTables;
                LookUpEdit_type.Properties.DataSource = DebtPlus.LINQ.InMemory.LetterFieldTypes.getList();

                TextEdit_letter_field.EditValue = record.Id;
                LookUpEdit_type.EditValue = record.type;
                TextEdit_formatting.EditValue = record.formatting;
                TextEdit_field_name.EditValue = record.field_name;
                LookUpEdit_letter_table.EditValue = record.letter_table;

                LookUpEdit_letter_table.Properties.PopupWidth = LookUpEdit_letter_table.Width + 200;

                simpleButton_OK.Enabled = !HasErrors();
                EnableTable();
            }
            finally
            {
                RegisterHandlers();
            }
        }

        /// <summary>
        /// Handle a change in the field type list
        /// </summary>
        private void LookUpEdit_type_EditValueChanged(object sender, EventArgs e)
        {
            EnableTable();
        }

        /// <summary>
        /// Enable or disable the table reference for the field.
        /// </summary>
        private void EnableTable()
        {
            // If a table is needed then enable the table editing routine
            if (NeedTable)
            {
                LookUpEdit_letter_table.Enabled = true;
                TextEdit_field_name.Enabled = true;
            }
            else
            {
                TextEdit_field_name.EditValue = null;
                LookUpEdit_letter_table.EditValue = null;
                LookUpEdit_letter_table.Enabled = false;
                TextEdit_field_name.Enabled = false;
            }
        }

        private void fieldChanged(object sender, EventArgs e)
        {
            simpleButton_OK.Enabled = !HasErrors();
        }

        private bool HasErrors()
        {
            // These two fields are required
            if (string.IsNullOrWhiteSpace(DebtPlus.Utils.Nulls.v_String(TextEdit_letter_field.EditValue)))
            {
                return true;
            }

            if (!DebtPlus.Utils.Nulls.v_Int32(LookUpEdit_type.EditValue).HasValue)
            {
                return true;
            }

            // This field is required only if the record is a normal lookup table
            if (NeedTable && !DebtPlus.Utils.Nulls.v_Int32(LookUpEdit_letter_table.EditValue).HasValue)
            {
                return true;
            }

            return false;
        }

        /// <summary>
        /// Does the selected item need a table reference?
        /// </summary>
        private bool NeedTable
        {
            get
            {
                var q = LookUpEdit_type.GetSelectedDataRow() as DebtPlus.LINQ.InMemory.LetterFieldType;
                if (q != null)
                {
                    return q.need_table;
                }
                return false;
            }
        }

        /// <summary>
        /// Process the CLICK event on the OK button
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected override void simpleButton_OK_Click(object sender, EventArgs e)
        {
            base.simpleButton_OK_Click(sender, e);

            // Get the standard items from the controls
            record.type = DebtPlus.Utils.Nulls.v_Int32(LookUpEdit_type.EditValue).GetValueOrDefault();
            record.formatting = DebtPlus.Utils.Nulls.v_String(TextEdit_formatting.EditValue);

            if (NeedTable)
            {
                record.letter_table = DebtPlus.Utils.Nulls.v_Int32(LookUpEdit_letter_table.EditValue);
                record.letter_table1 = LookUpEdit_letter_table.GetSelectedDataRow() as letter_table;
                record.field_name = DebtPlus.Utils.Nulls.v_String(TextEdit_field_name.EditValue);
            }
            else
            {
                record.letter_table = null;
                record.letter_table1 = null;
                record.field_name = null;
            }
        }
    }
}