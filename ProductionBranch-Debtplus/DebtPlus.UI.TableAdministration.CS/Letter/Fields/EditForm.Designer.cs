using System;
using System.Windows.Forms;
using System.Drawing;
using DebtPlus.LINQ;
using System.Linq;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Diagnostics;
namespace DebtPlus.UI.TableAdministration.CS.Letter.Fields
{
	partial class EditForm
	{
		//Form overrides dispose to clean up the component list.
		[System.Diagnostics.DebuggerNonUserCode]
		protected override void Dispose(bool disposing)
		{
			try 
            {
				if (disposing)
                {
                    if (components != null) components.Dispose();
				}
                components = null;
            }
    	    finally
            {
				base.Dispose(disposing);
			}
		}

		//Required by the Windows Form Designer
		private System.ComponentModel.IContainer components = null;

		//NOTE: The following procedure is required by the Windows Form Designer
		//It can be modified using the Windows Form Designer.  
		//Do not modify it using the code editor.
		[System.Diagnostics.DebuggerStepThrough]
		private void InitializeComponent()
		{
			this.LabelControl1 = new DevExpress.XtraEditors.LabelControl();
			this.LabelControl3 = new DevExpress.XtraEditors.LabelControl();
			this.LabelControl2 = new DevExpress.XtraEditors.LabelControl();
			this.LookUpEdit_type = new DevExpress.XtraEditors.LookUpEdit();
			this.TextEdit_letter_field = new DevExpress.XtraEditors.TextEdit();
			this.LookUpEdit_letter_table = new DevExpress.XtraEditors.LookUpEdit();
			this.TextEdit_formatting = new DevExpress.XtraEditors.TextEdit();
			this.LabelControl4 = new DevExpress.XtraEditors.LabelControl();
			this.TextEdit_field_name = new DevExpress.XtraEditors.TextEdit();
			this.LabelControl5 = new DevExpress.XtraEditors.LabelControl();
			((System.ComponentModel.ISupportInitialize)this.DxErrorProvider1).BeginInit();
			((System.ComponentModel.ISupportInitialize)this.LookUpEdit_type.Properties).BeginInit();
			((System.ComponentModel.ISupportInitialize)this.TextEdit_letter_field.Properties).BeginInit();
			((System.ComponentModel.ISupportInitialize)this.LookUpEdit_letter_table.Properties).BeginInit();
			((System.ComponentModel.ISupportInitialize)this.TextEdit_formatting.Properties).BeginInit();
			((System.ComponentModel.ISupportInitialize)this.TextEdit_field_name.Properties).BeginInit();
			this.SuspendLayout();
			//
			//simpleButton_OK
			//
			this.simpleButton_OK.Location = new System.Drawing.Point(318, 17);
			this.simpleButton_OK.TabIndex = 10;
			//
			//simpleButton_Cancel
			//
			this.simpleButton_Cancel.Location = new System.Drawing.Point(318, 60);
			this.simpleButton_Cancel.TabIndex = 11;
			//
			//LabelControl1
			//
			this.LabelControl1.Location = new System.Drawing.Point(8, 17);
			this.LabelControl1.Name = "LabelControl1";
			this.LabelControl1.Size = new System.Drawing.Size(54, 13);
			this.LabelControl1.TabIndex = 0;
			this.LabelControl1.Text = "Letter Field";
			//
			//LabelControl3
			//
			this.LabelControl3.Location = new System.Drawing.Point(8, 69);
			this.LabelControl3.Name = "LabelControl3";
			this.LabelControl3.Size = new System.Drawing.Size(26, 13);
			this.LabelControl3.TabIndex = 4;
			this.LabelControl3.Text = "Table";
			//
			//LabelControl2
			//
			this.LabelControl2.Location = new System.Drawing.Point(8, 43);
			this.LabelControl2.Name = "LabelControl2";
			this.LabelControl2.Size = new System.Drawing.Size(24, 13);
			this.LabelControl2.TabIndex = 2;
			this.LabelControl2.Text = "Type";
			//
			//LookUpEdit_type
			//
			this.LookUpEdit_type.Anchor = (AnchorStyles)((AnchorStyles.Top | AnchorStyles.Left) | AnchorStyles.Right);
			this.LookUpEdit_type.Location = new System.Drawing.Point(86, 40);
			this.LookUpEdit_type.Name = "LookUpEdit_type";
			this.LookUpEdit_type.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.False;
			this.LookUpEdit_type.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] { new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo) });
			this.LookUpEdit_type.Properties.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
				new DevExpress.XtraEditors.Controls.LookUpColumnInfo("Id", "ID", 20, DevExpress.Utils.FormatType.Numeric, "f0", false, DevExpress.Utils.HorzAlignment.Default, DevExpress.Data.ColumnSortOrder.None),
				new DevExpress.XtraEditors.Controls.LookUpColumnInfo("description", "Description", 20, DevExpress.Utils.FormatType.None, "", true, DevExpress.Utils.HorzAlignment.Default, DevExpress.Data.ColumnSortOrder.None)
			});
			this.LookUpEdit_type.Properties.DisplayMember = "description";
			this.LookUpEdit_type.Properties.NullText = "";
			this.LookUpEdit_type.Properties.ShowFooter = false;
			this.LookUpEdit_type.Properties.ShowHeader = false;
			this.LookUpEdit_type.Properties.ValueMember = "Id";
			this.LookUpEdit_type.Size = new System.Drawing.Size(212, 20);
			this.LookUpEdit_type.TabIndex = 3;
			this.LookUpEdit_type.Properties.SortColumnIndex = 1;
			//
			//TextEdit_letter_field
			//
			this.TextEdit_letter_field.Anchor = (AnchorStyles)((AnchorStyles.Top | AnchorStyles.Left) | AnchorStyles.Right);
			this.TextEdit_letter_field.Location = new System.Drawing.Point(86, 14);
			this.TextEdit_letter_field.Name = "TextEdit_letter_field";
			this.TextEdit_letter_field.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.False;
			this.TextEdit_letter_field.Properties.MaxLength = 50;
			this.TextEdit_letter_field.Size = new System.Drawing.Size(212, 20);
			this.TextEdit_letter_field.TabIndex = 1;
			//
			//LookUpEdit_letter_table
			//
			this.LookUpEdit_letter_table.Anchor = (AnchorStyles)((AnchorStyles.Top | AnchorStyles.Left) | AnchorStyles.Right);
			this.LookUpEdit_letter_table.Location = new System.Drawing.Point(86, 66);
			this.LookUpEdit_letter_table.Name = "LookUpEdit_letter_table";
			this.LookUpEdit_letter_table.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.False;
			this.LookUpEdit_letter_table.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] { new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo) });
			this.LookUpEdit_letter_table.Properties.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
				new DevExpress.XtraEditors.Controls.LookUpColumnInfo("Id", "ID", 5, DevExpress.Utils.FormatType.Numeric, "f0", true, DevExpress.Utils.HorzAlignment.Far, DevExpress.Data.ColumnSortOrder.None),
				new DevExpress.XtraEditors.Controls.LookUpColumnInfo("query", "Query", 50, DevExpress.Utils.FormatType.None, "", true, DevExpress.Utils.HorzAlignment.Near)
			});
			this.LookUpEdit_letter_table.Properties.DisplayMember = "query";
            this.LookUpEdit_letter_table.Properties.ValueMember = "Id";
            this.LookUpEdit_letter_table.Properties.NullText = "";
			this.LookUpEdit_letter_table.Properties.ShowFooter = false;
			this.LookUpEdit_letter_table.Size = new System.Drawing.Size(212, 20);
			this.LookUpEdit_letter_table.TabIndex = 5;
			this.LookUpEdit_letter_table.Properties.SortColumnIndex = 1;
            this.LookUpEdit_letter_table.Properties.PopupWidth = 750;
			//
			//TextEdit_formatting
			//
			this.TextEdit_formatting.Anchor = (AnchorStyles)((AnchorStyles.Top | AnchorStyles.Left) | AnchorStyles.Right);
			this.TextEdit_formatting.Location = new System.Drawing.Point(86, 118);
			this.TextEdit_formatting.Name = "TextEdit_formatting";
			this.TextEdit_formatting.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.True;
			this.TextEdit_formatting.Properties.MaxLength = 50;
			this.TextEdit_formatting.Size = new System.Drawing.Size(212, 20);
			this.TextEdit_formatting.TabIndex = 9;
			//
			//LabelControl4
			//
			this.LabelControl4.Location = new System.Drawing.Point(8, 121);
			this.LabelControl4.Name = "LabelControl4";
			this.LabelControl4.Size = new System.Drawing.Size(52, 13);
			this.LabelControl4.TabIndex = 8;
			this.LabelControl4.Text = "Formatting";
			//
			//TextEdit_field_name
			//
			this.TextEdit_field_name.Anchor = (AnchorStyles)((AnchorStyles.Top | AnchorStyles.Left) | AnchorStyles.Right);
			this.TextEdit_field_name.Location = new System.Drawing.Point(86, 92);
			this.TextEdit_field_name.Name = "TextEdit_field_name";
			this.TextEdit_field_name.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.False;
			this.TextEdit_field_name.Properties.MaxLength = 50;
			this.TextEdit_field_name.Size = new System.Drawing.Size(212, 20);
			this.TextEdit_field_name.TabIndex = 7;
			//
			//LabelControl5
			//
			this.LabelControl5.Location = new System.Drawing.Point(8, 95);
			this.LabelControl5.Name = "LabelControl5";
			this.LabelControl5.Size = new System.Drawing.Size(52, 13);
			this.LabelControl5.TabIndex = 6;
			this.LabelControl5.Text = "Field Name";
			//
			//EditForm
			//
			this.AutoScaleDimensions = new System.Drawing.SizeF(6f, 13f);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(412, 153);
			this.Controls.Add(this.TextEdit_field_name);
			this.Controls.Add(this.LabelControl5);
			this.Controls.Add(this.LabelControl4);
			this.Controls.Add(this.TextEdit_formatting);
			this.Controls.Add(this.LookUpEdit_letter_table);
			this.Controls.Add(this.TextEdit_letter_field);
			this.Controls.Add(this.LookUpEdit_type);
			this.Controls.Add(this.LabelControl2);
			this.Controls.Add(this.LabelControl1);
			this.Controls.Add(this.LabelControl3);
			this.Name = "EditForm";
			this.Text = "Letter Field";
			this.Controls.SetChildIndex(this.LabelControl3, 0);
			this.Controls.SetChildIndex(this.LabelControl1, 0);
			this.Controls.SetChildIndex(this.simpleButton_OK, 0);
			this.Controls.SetChildIndex(this.simpleButton_Cancel, 0);
			this.Controls.SetChildIndex(this.LabelControl2, 0);
			this.Controls.SetChildIndex(this.LookUpEdit_type, 0);
			this.Controls.SetChildIndex(this.TextEdit_letter_field, 0);
			this.Controls.SetChildIndex(this.LookUpEdit_letter_table, 0);
			this.Controls.SetChildIndex(this.TextEdit_formatting, 0);
			this.Controls.SetChildIndex(this.LabelControl4, 0);
			this.Controls.SetChildIndex(this.LabelControl5, 0);
			this.Controls.SetChildIndex(this.TextEdit_field_name, 0);
			((System.ComponentModel.ISupportInitialize)this.DxErrorProvider1).EndInit();
			((System.ComponentModel.ISupportInitialize)this.LookUpEdit_type.Properties).EndInit();
			((System.ComponentModel.ISupportInitialize)this.TextEdit_letter_field.Properties).EndInit();
			((System.ComponentModel.ISupportInitialize)this.LookUpEdit_letter_table.Properties).EndInit();
			((System.ComponentModel.ISupportInitialize)this.TextEdit_formatting.Properties).EndInit();
			((System.ComponentModel.ISupportInitialize)this.TextEdit_field_name.Properties).EndInit();
			this.ResumeLayout(false);
			this.PerformLayout();
		}

		private DevExpress.XtraEditors.LabelControl LabelControl1;
		private DevExpress.XtraEditors.LabelControl LabelControl3;
		private DevExpress.XtraEditors.LabelControl LabelControl2;
		private DevExpress.XtraEditors.LookUpEdit LookUpEdit_type;
		private DevExpress.XtraEditors.TextEdit TextEdit_letter_field;
		private DevExpress.XtraEditors.LookUpEdit LookUpEdit_letter_table;
		private DevExpress.XtraEditors.TextEdit TextEdit_formatting;
		private DevExpress.XtraEditors.LabelControl LabelControl4;
		private DevExpress.XtraEditors.TextEdit TextEdit_field_name;
		private DevExpress.XtraEditors.LabelControl LabelControl5;
	}
}

