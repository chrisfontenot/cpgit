#region "Copyright 2000-2012 DebtPlus, L.L.C."
//{*******************************************************************}
//{                                                                   }
//{       DebtPlus Debt Management System                             }
//{                                                                   }
//{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
//{       ALL RIGHTS RESERVED                                         }
//{                                                                   }
//{   The entire contents of this file is protected by U.S. and       }
//{   International Copyright Laws. Unauthorized reproduction,        }
//{   reverse-engineering, and distribution of all or any portion of  }
//{   the code contained in this file is strictly prohibited and may  }
//{   result in severe civil and criminal penalties and will be       }
//{   prosecuted to the maximum extent possible under the law.        }
//{                                                                   }
//{   RESTRICTIONS                                                    }
//{                                                                   }
//{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
//{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
//{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
//{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
//{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
//{                                                                   }
//{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
//{   ADDITIONAL RESTRICTIONS.                                        }
//{                                                                   }
//{*******************************************************************}

#endregion "Copyright 2000-2012 DebtPlus, L.L.C."

using System;
using System.Linq;
using System.Windows.Forms;
using DebtPlus.LINQ;
using DebtPlus.UI.Common;

namespace DebtPlus.UI.TableAdministration.CS.Letter.Fields
{
    public partial class MainForm : Templates.MainForm
    {
        // Allocate a database connection object for the table references
        // We need to do it here because the letter_fields table references the letter_tables table and they both need to be in the same context.
        private DebtPlus.LINQ.BusinessContext bc = new DebtPlus.LINQ.BusinessContext();

        private System.Collections.Generic.List<letter_field> colRecords;
        private System.Collections.Generic.List<letter_table> colLetterTables;

        public MainForm()
            : base()
        {
            InitializeComponent();
            this.Load += MainForm_Load;

            gridcolumn_type.DisplayFormat.Format = new TypeFormatter();
            gridcolumn_type.GroupFormat.Format = new TypeFormatter();
        }

        /// <summary>
        /// Process the load event on the form
        /// </summary>
        private void MainForm_Load(object sender, EventArgs e)
        {
            try
            {
                using (var cm = new CursorManager())
                {
                    // Load the letter_table references
                    System.Data.Linq.DataLoadOptions dl = new System.Data.Linq.DataLoadOptions();
                    dl.LoadWith<letter_field>(s => s.letter_table1);
                    bc.LoadOptions = dl;
                    bc.DeferredLoadingEnabled = false;

                    colRecords = bc.letter_fields.ToList();
                    colLetterTables = bc.letter_tables.ToList();

                    // Bind the dataset to the control
                    gridControl1.DataSource = colRecords;
                    gridControl1.RefreshDataSource();
                    gridView1.BestFitColumns();
                }
            }
            catch (System.Data.SqlClient.SqlException ex)
            {
                DebtPlus.UI.Common.ErrorHandling.HandleErrors(ex);
            }
        }

        /// <summary>
        /// Handle the edit of the information on the form
        /// </summary>
        protected override void UpdateRecord(object obj)
        {
            var record = obj as letter_field;
            if (record == null)
            {
                return;
            }

            using (var frm = new EditForm(colRecords, colLetterTables, record))
            {
                if (frm.ShowDialog() != DialogResult.OK)
                {
                    return;
                }
            }

            try
            {
                bc.SubmitChanges();
                gridView1.RefreshData();
            }
            catch (System.Data.SqlClient.SqlException ex)
            {
                DebtPlus.UI.Common.ErrorHandling.HandleErrors(ex);
            }
        }

        protected override void DeleteRecord(object obj)
        {
            var record = obj as letter_field;
            if (record == null)
            {
                return;
            }

            if (DebtPlus.Data.Prompts.RequestConfirmation_Delete() != DialogResult.Yes)
            {
                return;
            }

            try
            {
                bc.letter_fields.DeleteOnSubmit(record);
                bc.SubmitChanges();
                gridView1.RefreshData();
            }
            catch (System.Data.SqlClient.SqlException ex)
            {
                DebtPlus.UI.Common.ErrorHandling.HandleErrors(ex);
            }
        }

        protected override void CreateRecord()
        {
            var record = DebtPlus.LINQ.Factory.Manufacture_letter_field();
            using (EditForm frm = new EditForm(colRecords, colLetterTables, record))
            {
                if (frm.ShowDialog() != DialogResult.OK)
                {
                    return;
                }
            }

            bc.letter_fields.InsertOnSubmit(record);

            try
            {
                bc.SubmitChanges();

                colRecords.Add(record);
                gridView1.RefreshData();
            }
            catch (System.Data.SqlClient.SqlException ex)
            {
                DebtPlus.UI.Common.ErrorHandling.HandleErrors(ex);
            }
        }

        /// <summary>
        /// Translate the letter_table types to a suitable format
        /// </summary>
        private class TypeFormatter : ICustomFormatter, IFormatProvider
        {
            internal TypeFormatter()
            {
            }

            public string Format(string format1, object arg, IFormatProvider formatProvider)
            {
                if (arg != null && !object.ReferenceEquals(arg, System.DBNull.Value))
                {
                    var q = DebtPlus.LINQ.InMemory.LetterFieldTypes.getList().Find(s => s.Id == Convert.ToInt32(arg));
                    if (q != null)
                    {
                        return q.description;
                    }
                }

                return string.Empty;
            }

            public object GetFormat(Type formatType)
            {
                return this;
            }
        }
    }
}