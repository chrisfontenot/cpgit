﻿namespace DebtPlus.UI.TableAdministration.CS.DropReasons
{
    partial class MainForm
    {
        //Form overrides dispose to clean up the component list.
        [System.Diagnostics.DebuggerNonUserCode]
        protected override void Dispose(bool disposing)
        {
            try
            {
                if (disposing)
                {
                    if (components != null) components.Dispose();
                    if (bc != null) bc.Dispose();
                }
                components = null;
                bc         = null;
            }
            finally
            {
                base.Dispose(disposing);
            }
        }

        //Required by the Windows Form Designer
        private System.ComponentModel.IContainer components = null;

        //NOTE: The following procedure is required by the Windows Form Designer
        //It can be modified using the Windows Form Designer.  
        //Do not modify it using the code editor.
        [System.Diagnostics.DebuggerStepThrough]
        private void InitializeComponent()
        {
            this.gridColumn_ID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn_ID.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.True;
            this.gridColumn_description = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn_description.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.True;
            this.gridColumn_nfcc = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn_nfcc.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.True;
            this.gridColumn_rpps = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn_rpps.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.True;
            this.gridColumn_default = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn_default.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.True;
            ((System.ComponentModel.ISupportInitialize)this.gridControl1).BeginInit();
            ((System.ComponentModel.ISupportInitialize)this.gridView1).BeginInit();
            ((System.ComponentModel.ISupportInitialize)this.DxErrorProvider1).BeginInit();
            this.SuspendLayout();
            //
            //gridView1
            //
            this.gridView1.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
				this.gridColumn_ID,
				this.gridColumn_description,
				this.gridColumn_rpps,
				this.gridColumn_nfcc,
				this.gridColumn_default
			});
            //
            //gridColumn_ID
            //
            this.gridColumn_ID.Caption = "ID";
            this.gridColumn_ID.DisplayFormat.FormatString = "f0";
            this.gridColumn_ID.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.gridColumn_ID.FieldName = "Id";
            this.gridColumn_ID.GroupFormat.FormatString = "f0";
            this.gridColumn_ID.GroupFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.gridColumn_ID.Name = "gridColumn_ID";
            this.gridColumn_ID.Visible = true;
            this.gridColumn_ID.VisibleIndex = 0;
            //
            //gridColumn_Name
            //
            this.gridColumn_description.Caption = "Description";
            this.gridColumn_description.FieldName = "description";
            this.gridColumn_description.Name = "gridColumn_Name";
            this.gridColumn_description.Visible = true;
            this.gridColumn_description.VisibleIndex = 1;
            //
            //gridColumn_nfcc
            //
            this.gridColumn_nfcc.Caption = "NFCC";
            this.gridColumn_nfcc.FieldName = "nfcc";
            this.gridColumn_nfcc.Name = "gridColumn_nfcc";
            this.gridColumn_nfcc.Visible = true;
            this.gridColumn_nfcc.VisibleIndex = 4;
            //
            //gridColumn_rpps
            //
            this.gridColumn_rpps.Caption = "RPPS";
            this.gridColumn_rpps.FieldName = "rpps_code";
            this.gridColumn_rpps.Name = "gridColumn_rpps";
            this.gridColumn_rpps.Visible = true;
            this.gridColumn_rpps.VisibleIndex = 2;
            //
            //gridColumn_default
            //
            this.gridColumn_default.Caption = "Default";
            this.gridColumn_default.FieldName = "Default";
            this.gridColumn_default.Name = "gridColumn_default";
            this.gridColumn_default.Visible = true;
            this.gridColumn_default.VisibleIndex = 5;
            //
            //MainForm
            //
            this.AutoScaleDimensions = new System.Drawing.SizeF(6f, 13f);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(528, 294);
            this.Name = "MainForm";
            this.ToolTipController1.SetSuperTip(this, null);
            this.Text = "Drop Reasons";
            ((System.ComponentModel.ISupportInitialize)this.gridControl1).EndInit();
            ((System.ComponentModel.ISupportInitialize)this.gridView1).EndInit();
            ((System.ComponentModel.ISupportInitialize)this.DxErrorProvider1).EndInit();
            this.ResumeLayout(false);
        }

        private DevExpress.XtraGrid.Columns.GridColumn gridColumn_ID;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn_description;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn_nfcc;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn_rpps;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn_default;
    }
}
