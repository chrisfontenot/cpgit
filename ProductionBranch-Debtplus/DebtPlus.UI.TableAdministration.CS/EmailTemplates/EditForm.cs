#region "Copyright 2000-2012 DebtPlus, L.L.C."
//{*******************************************************************}
//{                                                                   }
//{       DebtPlus Debt Management System                             }
//{                                                                   }
//{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
//{       ALL RIGHTS RESERVED                                         }
//{                                                                   }
//{   The entire contents of this file is protected by U.S. and       }
//{   International Copyright Laws. Unauthorized reproduction,        }
//{   reverse-engineering, and distribution of all or any portion of  }
//{   the code contained in this file is strictly prohibited and may  }
//{   result in severe civil and criminal penalties and will be       }
//{   prosecuted to the maximum extent possible under the law.        }
//{                                                                   }
//{   RESTRICTIONS                                                    }
//{                                                                   }
//{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
//{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
//{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
//{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
//{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
//{                                                                   }
//{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
//{   ADDITIONAL RESTRICTIONS.                                        }
//{                                                                   }
//{*******************************************************************}

#endregion "Copyright 2000-2012 DebtPlus, L.L.C."

using System;
using DebtPlus.LINQ;

namespace DebtPlus.UI.TableAdministration.CS.EmailTemplates
{
    internal partial class EditForm : Templates.EditTemplateForm
    {
        private email_template record = null;
        private System.Collections.Generic.List<Attachment> colRecords = null;

        /// <summary>
        /// Initialize the new form context
        /// </summary>
        internal EditForm()
            : base()
        {
            InitializeComponent();
        }

        /// <summary>
        /// Initialize the new form context
        /// </summary>
        internal EditForm(email_template record)
            : this()
        {
            this.record = record;
            RegisterHandlers();
        }

        /// <summary>
        /// Register the event handlers
        /// </summary>
        private void RegisterHandlers()
        {
            Load                                   += EditForm_Load;
            textEdit_description.EditValueChanged  += form_Changed;
            textEdit_subject.EditValueChanged      += form_Changed;
            textEdit_text_URI.EditValueChanged     += form_Changed;
            textEdit_html_URI.EditValueChanged     += form_Changed;
            textEdit_sender_name.EditValueChanged  += form_Changed;
            textEdit_sender_email.EditValueChanged += form_Changed;
            lookUpEdit_language.EditValueChanged   += form_Changed;

            textEdit_html_URI.Validated            += textEdit_html_URI_Validated;
            textEdit_text_URI.Validated            += textEdit_html_URI_Validated;

            barButtonItem_Create.ItemClick         += barButtonItem_Create_ItemClick;
            barButtonItem_Update.ItemClick         += barButtonItem_Update_ItemClick;
            barButtonItem_Delete.ItemClick         += barButtonItem_Delete_ItemClick;
            gridView1.DoubleClick                  += gridView1_DoubleClick;
            gridView1.MouseDown                    += gridView1_MouseDown;
        }

        /// <summary>
        /// Remove the event handler registrations
        /// </summary>
        private void UnRegisterHandlers()
        {
            Load                                   -= EditForm_Load;
            textEdit_description.EditValueChanged  -= form_Changed;
            textEdit_subject.EditValueChanged      -= form_Changed;
            textEdit_text_URI.EditValueChanged     -= form_Changed;
            textEdit_html_URI.EditValueChanged     -= form_Changed;
            textEdit_sender_name.EditValueChanged  -= form_Changed;
            textEdit_sender_email.EditValueChanged -= form_Changed;
            lookUpEdit_language.EditValueChanged   -= form_Changed;

            textEdit_html_URI.Validated            -= textEdit_html_URI_Validated;
            textEdit_text_URI.Validated            -= textEdit_html_URI_Validated;

            barButtonItem_Create.ItemClick         -= barButtonItem_Create_ItemClick;
            barButtonItem_Update.ItemClick         -= barButtonItem_Update_ItemClick;
            barButtonItem_Delete.ItemClick         -= barButtonItem_Delete_ItemClick;
            gridView1.DoubleClick                  -= gridView1_DoubleClick;
            gridView1.MouseDown                    -= gridView1_MouseDown;
        }

        /// <summary>
        /// Process the validation on the URI fields
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void textEdit_html_URI_Validated(object sender, EventArgs e)
        {
            UnRegisterHandlers();
            try
            {
                string item = (sender as DevExpress.XtraEditors.TextEdit).Text.Trim();
                System.Uri tempURI = new System.Uri(item);
                string newItem = tempURI.ToString();

                if (item != newItem)
                {
                    (sender as DevExpress.XtraEditors.TextEdit).Text = item;
                }
            }
            catch { }
            finally
            {
                RegisterHandlers();
            }
        }

        /// <summary>
        /// Determine if there are errors on the pending form
        /// </summary>
        private void form_Changed(object sender, EventArgs e)
        {
            simpleButton_OK.Enabled = !HasErrors();
        }

        /// <summary>
        /// Process the LOAD sequence for the form
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void EditForm_Load(object sender, EventArgs e)
        {
            UnRegisterHandlers();

            // Load the list of languages
            lookUpEdit_language.Properties.DataSource = DebtPlus.LINQ.Cache.AttributeType.getLanguageList();

            try
            {
                labelControl_Id.Text = (record.Id < 1 ? "NEW" : record.Id.ToString());

                textEdit_description.EditValue  = record.description;
                textEdit_subject.EditValue      = record.subject;
                textEdit_text_URI.EditValue     = record.text_URI;
                textEdit_html_URI.EditValue     = record.html_URI;
                textEdit_sender_name.EditValue  = record.sender_name;
                textEdit_sender_email.EditValue = record.sender_email;
                lookUpEdit_language.EditValue   = record.language;

                // Split the attachments into the list of attachments
                colRecords              = Attachment.DecodeCollection(record.attachments);
                gridControl1.DataSource = colRecords;

                simpleButton_OK.Enabled = !HasErrors();
            }

            finally
            {
                RegisterHandlers();
            }
        }

        /// <summary>
        /// Process the click event on the OK button
        /// </summary>
        /// <param name="Sender"></param>
        /// <param name="e"></param>
        protected override void simpleButton_OK_Click(object Sender, EventArgs e)
        {
            record.description  = DebtPlus.Utils.Nulls.v_String(textEdit_description.EditValue);
            record.subject      = DebtPlus.Utils.Nulls.v_String(textEdit_subject.EditValue);
            record.text_URI     = DebtPlus.Utils.Nulls.v_String(textEdit_text_URI.EditValue);
            record.html_URI     = DebtPlus.Utils.Nulls.v_String(textEdit_html_URI.EditValue);
            record.sender_name  = DebtPlus.Utils.Nulls.v_String(textEdit_sender_name.EditValue);
            record.sender_email = DebtPlus.Utils.Nulls.v_String(textEdit_sender_email.EditValue);
            record.language     = DebtPlus.Utils.Nulls.v_Int32(lookUpEdit_language.EditValue).GetValueOrDefault(1);
            record.attachments  = Attachment.EncodeCollection(colRecords);

            base.simpleButton_OK_Click(Sender, e);
        }

        /// <summary>
        /// Determine if there are any error conditions on the form
        /// </summary>
        /// <returns></returns>
        private bool HasErrors()
        {
            // At least one must be specified to be valid
            if (string.IsNullOrWhiteSpace(DebtPlus.Utils.Nulls.v_String(textEdit_text_URI.EditValue)) && string.IsNullOrWhiteSpace(DebtPlus.Utils.Nulls.v_String(textEdit_html_URI.EditValue)))
            {
                return true;
            }

            // We need a description
            if (string.IsNullOrWhiteSpace(DebtPlus.Utils.Nulls.v_String(textEdit_description.EditValue)))
            {
                return true;
            }

            // We need an email address
            if (string.IsNullOrWhiteSpace(DebtPlus.Utils.Nulls.v_String(textEdit_sender_email.EditValue)))
            {
                return true;
            }

            // We need a subject
            if (string.IsNullOrWhiteSpace(DebtPlus.Utils.Nulls.v_String(textEdit_subject.EditValue)))
            {
                return true;
            }

            return false;
        }

        private void DeleteRecord(object obj)
        {
            var record = obj as Attachment;
            if (record == null)
            {
                return;
            }

            if (DebtPlus.Data.Prompts.RequestConfirmation_Delete() != System.Windows.Forms.DialogResult.Yes)
            {
                return;
            }

            colRecords.Remove(record);
            gridView1.RefreshData();
        }

        private void UpdateRecord(object obj)
        {
            var record = obj as Attachment;
            if (record == null)
            {
                return;
            }

            using (var frm = new AttachmentEdit(record))
            {
                if (frm.ShowDialog() != System.Windows.Forms.DialogResult.OK)
                {
                    return;
                }
            }

            gridView1.RefreshData();
        }

        private void CreateRecord()
        {
            var record = new Attachment();
            using (var frm = new AttachmentEdit(record))
            {
                if (frm.ShowDialog() != System.Windows.Forms.DialogResult.OK)
                {
                    return;
                }
            }

            colRecords.Add(record);
            gridView1.RefreshData();
        }


        /// <summary>
        /// Double click event on the list
        /// </summary>
        private void gridView1_MouseDown(object sender, System.Windows.Forms.MouseEventArgs e)
        {
            // Find the record to be edited from the list
            if (e.Button == System.Windows.Forms.MouseButtons.Right)
            {
                DevExpress.XtraGrid.Views.Grid.ViewInfo.GridHitInfo hi = gridView1.CalcHitInfo((gridControl1.PointToClient(System.Windows.Forms.Control.MousePosition)));
                System.Int32 RowHandle = hi.RowHandle;
                gridView1.FocusedRowHandle = RowHandle;

                // Find the record for this row
                object obj = null;
                if (RowHandle >= 0)
                {
                    obj = gridView1.GetRow(RowHandle);
                }

                // If the row is defined then edit the row.
                if (obj != null)
                {
                    barButtonItem_Update.Enabled = true;
                    barButtonItem_Delete.Enabled = true;
                }
                else
                {
                    barButtonItem_Update.Enabled = false;
                    barButtonItem_Delete.Enabled = false;
                }

                popupMenu1.ShowPopup(System.Windows.Forms.Control.MousePosition);
            }
        }

        /// <summary>
        /// Double click event on the list
        /// </summary>
        private void gridView1_DoubleClick(object sender, System.EventArgs e)
        {
            DevExpress.XtraGrid.Views.Grid.ViewInfo.GridHitInfo hi = gridView1.CalcHitInfo((gridControl1.PointToClient(System.Windows.Forms.Control.MousePosition)));
            System.Int32 RowHandle = hi.RowHandle;

            // Find the record to be edited from the list
            if (RowHandle >= 0)
            {
                gridView1.FocusedRowHandle = RowHandle;
                Object obj = gridView1.GetRow(RowHandle);
                if (obj != null)
                {
                    UpdateRecord(obj);
                }
            }
        }

        /// <summary>
        /// Edit -> Create menu item clicked
        /// </summary>
        private void barButtonItem_Create_ItemClick(System.Object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            CreateRecord();
        }

        /// <summary>
        /// Edit -> Edit menu item clicked
        /// </summary>
        private void barButtonItem_Update_ItemClick(System.Object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            System.Int32 RowHandle = gridView1.FocusedRowHandle;

            if (RowHandle >= 0)
            {
                gridView1.FocusedRowHandle = RowHandle;
                object obj = gridView1.GetRow(RowHandle);
                if (obj != null)
                {
                    UpdateRecord(obj);
                }
            }
        }

        /// <summary>
        /// Edit -> Delete menu item clicked
        /// </summary>
        private void barButtonItem_Delete_ItemClick(System.Object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            System.Int32 RowHandle = gridView1.FocusedRowHandle;

            if (RowHandle >= 0)
            {
                gridView1.FocusedRowHandle = RowHandle;
                object obj = gridView1.GetRow(RowHandle);
                if (obj != null)
                {
                    DeleteRecord(obj);
                }
            }
        }
    }
}