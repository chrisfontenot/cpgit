﻿#region "Copyright 2000-2012 DebtPlus, L.L.C."
//{*******************************************************************}
//{                                                                   }
//{       DebtPlus Debt Management System                             }
//{                                                                   }
//{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
//{       ALL RIGHTS RESERVED                                         }
//{                                                                   }
//{   The entire contents of this file is protected by U.S. and       }
//{   International Copyright Laws. Unauthorized reproduction,        }
//{   reverse-engineering, and distribution of all or any portion of  }
//{   the code contained in this file is strictly prohibited and may  }
//{   result in severe civil and criminal penalties and will be       }
//{   prosecuted to the maximum extent possible under the law.        }
//{                                                                   }
//{   RESTRICTIONS                                                    }
//{                                                                   }
//{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
//{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
//{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
//{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
//{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
//{                                                                   }
//{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
//{   ADDITIONAL RESTRICTIONS.                                        }
//{                                                                   }
//{*******************************************************************}
#endregion "Copyright 2000-2012 DebtPlus, L.L.C."

using System;
using DebtPlus.LINQ;

namespace DebtPlus.UI.TableAdministration.CS.EmailTemplates
{
    internal partial class AttachmentEdit : Templates.EditTemplateForm
    {
        private Attachment record = null;

        /// <summary>
        /// Initialize the blank class form
        /// </summary>
        internal AttachmentEdit() : base()
        {
            InitializeComponent();
        }

        /// <summary>
        /// Initialize the new class instance
        /// </summary>
        /// <param name="record">Pointer to the record to be edited</param>
        internal AttachmentEdit(Attachment record) : this()
        {
            this.record = record;
            RegisterHandlers();
        }

        /// <summary>
        /// Register the event handlers
        /// </summary>
        private void RegisterHandlers()
        {
            this.Load                            += AttachmentEdit_Load;
            this.buttonEdit_FileName.ButtonClick += buttonEdit_FileName_ButtonClick;
            this.textEdit_name.EditValueChanged  += FormChanged;
            this.comboBox_type.EditValueChanged  += FormChanged;
            this.checkEdit_Inline.CheckedChanged += FormChanged;
            this.buttonEdit_FileName.Validating  += buttonEdit_FileName_Validating;
        }

        /// <summary>
        /// Remove the event handler registrations
        /// </summary>
        private void UnRegisterHandlers()
        {
            this.Load                            -= AttachmentEdit_Load;
            this.buttonEdit_FileName.ButtonClick -= buttonEdit_FileName_ButtonClick;
            this.textEdit_name.EditValueChanged  -= FormChanged;
            this.comboBox_type.EditValueChanged  -= FormChanged;
            this.checkEdit_Inline.CheckedChanged -= FormChanged;
            this.buttonEdit_FileName.Validating  -= buttonEdit_FileName_Validating;
        }

        private void AttachmentEdit_Load(object sender, System.EventArgs e)
        {
            UnRegisterHandlers();
            try
            {
                comboBox_type.EditValue       = record.Type;
                textEdit_name.EditValue       = record.Name;
                buttonEdit_FileName.EditValue = record.FileName;
                checkEdit_Inline.Checked      = record.Inline;
                simpleButton_OK.Enabled       = !HasErrors();
            }
            finally
            {
                RegisterHandlers();
            }
        }

        private void buttonEdit_FileName_ButtonClick(object sender, DevExpress.XtraEditors.Controls.ButtonPressedEventArgs e)
        {
            UnRegisterHandlers();
            try
            {
                using (var frm = new System.Windows.Forms.OpenFileDialog()
                {
                    AddExtension       = true,
                    AutoUpgradeEnabled = true,
                    CheckFileExists    = true,
                    CheckPathExists    = true,
                    DefaultExt         = "",
                    FileName           = buttonEdit_FileName.Text,
                    Multiselect        = false,
                    ReadOnlyChecked    = true,
                    RestoreDirectory   = true,
                    ShowReadOnly       = false,
                    ShowHelp           = false,
                    Title              = "Attachment FileName",
                    ValidateNames      = true
                })
                {
                    if (frm.ShowDialog() != System.Windows.Forms.DialogResult.OK)
                    {
                        return;
                    }

                    buttonEdit_FileName.EditValue = frm.FileName;
                    simpleButton_OK.Enabled       = !HasErrors();
                }
            }
            finally
            {
                RegisterHandlers();
            }
        }

        private void FormChanged(object sender, EventArgs e)
        {
            simpleButton_OK.Enabled = !HasErrors();
        }

        private bool HasErrors()
        {
            if (string.IsNullOrWhiteSpace(DebtPlus.Utils.Nulls.v_String(textEdit_name.EditValue)))
            {
                return true;
            }

            if (string.IsNullOrWhiteSpace(DebtPlus.Utils.Nulls.v_String(buttonEdit_FileName.EditValue)))
            {
                return true;
            }

            if (string.IsNullOrWhiteSpace(DebtPlus.Utils.Nulls.v_String(comboBox_type.EditValue)))
            {
                return true;
            }

            return false;
        }

        protected override void simpleButton_OK_Click(object sender, EventArgs e)
        {
            base.simpleButton_OK_Click(sender, e);

            // Retrieve the field values
            record.FileName = DebtPlus.Utils.Nulls.v_String(buttonEdit_FileName.EditValue);
            record.Name     = DebtPlus.Utils.Nulls.v_String(textEdit_name.EditValue);
            record.Type     = DebtPlus.Utils.Nulls.v_String(comboBox_type.EditValue);
            record.Inline   = checkEdit_Inline.Checked;
        }

        /// <summary>
        /// Validate the entry for the filename
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void buttonEdit_FileName_Validating(object sender, System.ComponentModel.CancelEventArgs e)
        {
            var name = DebtPlus.Utils.Nulls.v_String(buttonEdit_FileName.EditValue);
            if (string.IsNullOrWhiteSpace(name))
            {
                return;
            }

            // Validate the name
            try
            {
                if (System.IO.File.Exists(name))
                {
                    return;
                }
            }
            catch { }

            // There is something wrong with the name
            DebtPlus.Data.Forms.MessageBox.Show("There is something possibly wrong with the name that you have chosen.\r\nYou can not access the file. While this is possible for the server\r\nto do that, it may be that you have entered a bad name. Please re-verify\r\nthe file name before submitting the change.", "Possible error", System.Windows.Forms.MessageBoxButtons.OK);
        }
    }
}
