﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DebtPlus.UI.TableAdministration.CS.EmailTemplates
{
    /// <summary>
    /// Attachments are parsed in structures of this type
    /// </summary>
    internal class Attachment
    {
        internal Attachment()
        {
            FileName  = string.Empty;
            Image     = null;
            Name      = string.Empty;
            msImage   = null;
            Inline    = true;
            Type      = "application/pdf";
            ContentID = System.Guid.NewGuid().ToString();
        }

        internal Attachment(System.Xml.XmlReader Reader)
            : this()
        {
            Decode(Reader);
        }

        /// <summary>
        /// Name to be given to the attached file for the receiver.
        /// </summary>
        internal string Name { get; set; }

        /// <summary>
        /// Filename for the attachment buffer.
        /// </summary>
        internal string FileName { get; set; }

        /// <summary>
        /// Mime type for the attachment. Only specific items are allowed here.
        /// </summary>
        internal string Type { get; set; }

        /// <summary>
        /// Buffer for the attachment bytes
        /// </summary>
        internal Byte[] Image { get; set; }

        /// <summary>
        /// True if the attachment is inline and shown as an attachment. False for images that are embedded.
        /// </summary>
        internal bool Inline { get; set; }

        /// <summary>
        /// Disposition type for the attachment
        /// </summary>
        internal string DispositionType
        {
            get
            {
                return Inline ? System.Net.Mime.DispositionTypeNames.Inline : System.Net.Mime.DispositionTypeNames.Attachment;
            }
        }

        /// <summary>
        /// Stream to the image buffer. Used only for the SMTP mailer.
        /// </summary>
        internal System.IO.MemoryStream msImage { get; set; }

        /// <summary>
        /// Attachment content ID. Used only for the SMTP mailer.
        /// </summary>
        internal string ContentID { get; set; }

        /// <summary>
        /// Serialize the string with the current Attachment Data.
        /// </summary>
        /// <param name="Writer">Pointer to the resulting XML writer to be used in writing the data</param>
        internal void Encode(System.Xml.XmlWriter Writer)
        {
            // Write "<Attachment>"
            Writer.WriteStartElement("Attachment");

            // Include the name of the attribute
            if (!string.IsNullOrWhiteSpace(Name))
            {
                Writer.WriteElementString("Name", Name);
            }

            // Include the attribute type
            if (!string.IsNullOrWhiteSpace(Type))
            {
                Writer.WriteElementString("Type", Type);
            }

            // Include the filename field
            if (!string.IsNullOrWhiteSpace(FileName))
            {
                Writer.WriteElementString("FileName", FileName);
            }

            Writer.WriteElementString("Inline", Inline ? "1" : "0");

            // Include the image buffer if needed. We don't normally use it.
            if (Image != null && Image.Length > 0)
            {
                Writer.WriteStartElement("Image");
                Writer.WriteAttributeString("dt", "urn:schemas-microsoft-com:datatypes", "bin.base64");
                Writer.WriteBase64(Image, 0, Image.Length);
                Writer.WriteEndElement();
            }

            // Write "</Attachment>"
            Writer.WriteEndElement();
        }

        /// <summary>
        /// Decode the Attachment node from the XML document.
        /// </summary>
        /// <param name="Reader">Reader for the XML node</param>
        internal void Decode(System.Xml.XmlReader Reader)
        {
            var NodeName = Reader.Name;
            while (true)
            {
                if (Reader.NodeType == System.Xml.XmlNodeType.EndElement && Reader.Name == NodeName)
                {
                    break;
                }

                if (Reader.NodeType != System.Xml.XmlNodeType.Element)
                {
                    Reader.Read();
                    continue;
                }

                switch (Reader.Name)
                {
                    case "Type":
                        Type = Reader.ReadElementContentAsString();
                        break;

                    case "Name":
                        Name = Reader.ReadElementContentAsString();
                        break;

                    case "FileName":
                        FileName = Reader.ReadElementContentAsString();
                        break;

                    case "Inline":
                        Inline = Reader.ReadElementContentAsBoolean();
                        break;

                    case "Image":
                        using (var ms = new System.IO.MemoryStream(Image))
                        {
                            var localBuffer = new byte[4096];
                            int readBytes;
                            while ((readBytes = Reader.ReadElementContentAsBase64(localBuffer, 0, 4096)) > 0)
                            {
                                ms.Write(localBuffer, 0, readBytes);
                            }
                            ms.Flush();
                            ms.Close();
                        }
                        break;

                    default:
                        Reader.Read();
                        break;
                }
            }
        }

        /// <summary>
        /// Serialize the list of attachments for the collection.
        /// </summary>
        /// <returns></returns>
        internal static System.Xml.Linq.XElement EncodeCollection(System.Collections.Generic.List<Attachment> colRecords)
        {
            // If there are no attachments then there are no attachments.
            if (colRecords == null || colRecords.Count == 0)
            {
                return null;
            }

            // Encode the attachment counter to the collection.
            using (var ms = new System.IO.MemoryStream())
            {
                using (var tw = new System.IO.StreamWriter(ms))
                {
                    using (var xmlWriter = new System.Xml.XmlTextWriter(tw))
                    {
                        xmlWriter.WriteStartElement("Attachments");
                        foreach (var att in colRecords)
                        {
                            att.Encode(xmlWriter);
                        }
                        xmlWriter.WriteEndElement();
                        xmlWriter.Flush();

                        // Read the byte buffer with the encoded data
                        ms.Flush();
                        ms.Position = 0L;

                        // Read the buffer as a byte array
                        byte[] ab = new byte[ms.Length];
                        ms.Read(ab, 0, (int)ms.Length);

                        // Turn it into a string and from the string into the XML that we need.
                        var stringBuffer = System.Text.Encoding.UTF8.GetString(ab);
                        return System.Xml.Linq.XElement.Parse(stringBuffer);
                    }
                }
            }
        }

        /// <summary>
        /// Decode the list of documents defined in the attachment XML section.
        /// </summary>
        /// <param name="attachmentXML"></param>
        /// <returns></returns>
        internal static System.Collections.Generic.List<Attachment> DecodeCollection(System.Xml.Linq.XElement attachmentXML)
        {
            var colAttachments = new System.Collections.Generic.List<Attachment>();
            if (attachmentXML != null)
            {
                var Reader = attachmentXML.CreateReader();
                while (Reader.Read())
                {
                    // Look for the element "Attachments". If found, parse it.
                    if (Reader.NodeType == System.Xml.XmlNodeType.Element && Reader.Name == "Attachments")
                    {
                        decodeAttachments(ref colAttachments, Reader);
                        break;
                    }
                    Reader.Skip();
                }
            }

            // Return the empty list.
            return colAttachments;
        }

        /// <summary>
        /// Convert the XML element to a list of attached documents
        /// </summary>
        private static void decodeAttachments(ref System.Collections.Generic.List<Attachment> colAttachments, System.Xml.XmlReader Reader)
        {
            var nodeName = Reader.Name;
            while (Reader.Read())
            {
                if (Reader.NodeType == System.Xml.XmlNodeType.EndElement && Reader.Name == nodeName)
                {
                    break;
                }

                if (Reader.Name == "Attachment")
                {
                    colAttachments.Add(new Attachment(Reader));
                }
            }
        }
    }
}
