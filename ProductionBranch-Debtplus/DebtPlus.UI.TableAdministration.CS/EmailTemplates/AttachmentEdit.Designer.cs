﻿namespace DebtPlus.UI.TableAdministration.CS.EmailTemplates
{
    partial class AttachmentEdit
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.labelControl1 = new DevExpress.XtraEditors.LabelControl();
            this.textEdit_name = new DevExpress.XtraEditors.TextEdit();
            this.buttonEdit_FileName = new DevExpress.XtraEditors.ButtonEdit();
            this.labelControl2 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl3 = new DevExpress.XtraEditors.LabelControl();
            this.comboBox_type = new DevExpress.XtraEditors.ComboBoxEdit();
            this.checkEdit_Inline = new DevExpress.XtraEditors.CheckEdit();
            ((System.ComponentModel.ISupportInitialize)(this.DxErrorProvider1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit_name.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.buttonEdit_FileName.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.comboBox_type.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit_Inline.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // simpleButton_OK
            // 
            this.simpleButton_OK.Location = new System.Drawing.Point(361, 15);
            this.simpleButton_OK.TabIndex = 6;
            // 
            // simpleButton_Cancel
            // 
            this.simpleButton_Cancel.Location = new System.Drawing.Point(361, 58);
            this.simpleButton_Cancel.TabIndex = 7;
            // 
            // DefaultLookAndFeel1
            // 
            this.DefaultLookAndFeel1.LookAndFeel.SkinName = "McSkin";
            // 
            // labelControl1
            // 
            this.labelControl1.Location = new System.Drawing.Point(12, 15);
            this.labelControl1.Name = "labelControl1";
            this.labelControl1.Size = new System.Drawing.Size(27, 13);
            this.labelControl1.TabIndex = 0;
            this.labelControl1.Text = "Name";
            // 
            // textEdit_name
            // 
            this.textEdit_name.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.textEdit_name.Location = new System.Drawing.Point(71, 12);
            this.textEdit_name.Name = "textEdit_name";
            this.textEdit_name.Size = new System.Drawing.Size(266, 20);
            this.textEdit_name.TabIndex = 1;
            // 
            // buttonEdit_FileName
            // 
            this.buttonEdit_FileName.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.buttonEdit_FileName.Location = new System.Drawing.Point(71, 38);
            this.buttonEdit_FileName.Name = "buttonEdit_FileName";
            this.buttonEdit_FileName.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.buttonEdit_FileName.Size = new System.Drawing.Size(266, 20);
            this.buttonEdit_FileName.TabIndex = 3;
            // 
            // labelControl2
            // 
            this.labelControl2.Location = new System.Drawing.Point(12, 40);
            this.labelControl2.Name = "labelControl2";
            this.labelControl2.Size = new System.Drawing.Size(46, 13);
            this.labelControl2.TabIndex = 2;
            this.labelControl2.Text = "File Name";
            // 
            // labelControl3
            // 
            this.labelControl3.Location = new System.Drawing.Point(12, 66);
            this.labelControl3.Name = "labelControl3";
            this.labelControl3.Size = new System.Drawing.Size(53, 13);
            this.labelControl3.TabIndex = 4;
            this.labelControl3.Text = "MIME Type";
            // 
            // comboBox_type
            // 
            this.comboBox_type.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.comboBox_type.Location = new System.Drawing.Point(71, 63);
            this.comboBox_type.Name = "comboBox_type";
            this.comboBox_type.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.comboBox_type.Properties.Items.AddRange(new object[] {
            "application/octet",
            "application/pdf",
            "application/rtf",
            "application/soap",
            "application/zip",
            "image/gif",
            "image/jpeg",
            "image/tiff",
            "text/html",
            "text/plain",
            "text/richtext",
            "text/xml"});
            this.comboBox_type.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
            this.comboBox_type.Size = new System.Drawing.Size(266, 20);
            this.comboBox_type.TabIndex = 5;
            // 
            // checkEdit_Inline
            // 
            this.checkEdit_Inline.Location = new System.Drawing.Point(13, 91);
            this.checkEdit_Inline.Name = "checkEdit_Inline";
            this.checkEdit_Inline.Properties.Caption = "Inline attachment";
            this.checkEdit_Inline.Size = new System.Drawing.Size(126, 20);
            this.checkEdit_Inline.TabIndex = 8;
            // 
            // AttachmentEdit
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(458, 123);
            this.Controls.Add(this.checkEdit_Inline);
            this.Controls.Add(this.labelControl3);
            this.Controls.Add(this.labelControl2);
            this.Controls.Add(this.buttonEdit_FileName);
            this.Controls.Add(this.textEdit_name);
            this.Controls.Add(this.labelControl1);
            this.Controls.Add(this.comboBox_type);
            this.Name = "AttachmentEdit";
            this.Text = "Attachment Information";
            this.Controls.SetChildIndex(this.comboBox_type, 0);
            this.Controls.SetChildIndex(this.simpleButton_OK, 0);
            this.Controls.SetChildIndex(this.simpleButton_Cancel, 0);
            this.Controls.SetChildIndex(this.labelControl1, 0);
            this.Controls.SetChildIndex(this.textEdit_name, 0);
            this.Controls.SetChildIndex(this.buttonEdit_FileName, 0);
            this.Controls.SetChildIndex(this.labelControl2, 0);
            this.Controls.SetChildIndex(this.labelControl3, 0);
            this.Controls.SetChildIndex(this.checkEdit_Inline, 0);
            ((System.ComponentModel.ISupportInitialize)(this.DxErrorProvider1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit_name.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.buttonEdit_FileName.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.comboBox_type.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit_Inline.Properties)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private DevExpress.XtraEditors.LabelControl labelControl1;
        private DevExpress.XtraEditors.TextEdit textEdit_name;
        private DevExpress.XtraEditors.ButtonEdit buttonEdit_FileName;
        private DevExpress.XtraEditors.LabelControl labelControl2;
        private DevExpress.XtraEditors.LabelControl labelControl3;
        private DevExpress.XtraEditors.ComboBoxEdit comboBox_type;
        private DevExpress.XtraEditors.CheckEdit checkEdit_Inline;
    }
}