using System.Windows.Forms;

namespace DebtPlus.UI.TableAdministration.CS.EmailTemplates
{
	partial class EditForm
	{
		//Form overrides dispose to clean up the component list.
		[System.Diagnostics.DebuggerNonUserCode]
		protected override void Dispose(bool disposing)
		{
			try 
            {
				if (disposing)
                {
                    if (components != null) components.Dispose();
				}
                components = null;
            }
    	    finally
            {
				base.Dispose(disposing);
			}
		}

		//Required by the Windows Form Designer
		private System.ComponentModel.IContainer components = null;

		//NOTE: The following procedure is required by the Windows Form Designer
		//It can be modified using the Windows Form Designer.  
		//Do not modify it using the code editor.
		[System.Diagnostics.DebuggerStepThrough]
		private void InitializeComponent()
		{
            this.components = new System.ComponentModel.Container();
            this.LabelControl1 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl_Id = new DevExpress.XtraEditors.LabelControl();
            this.lookUpEdit_language = new DevExpress.XtraEditors.LookUpEdit();
            this.textEdit_description = new DevExpress.XtraEditors.TextEdit();
            this.textEdit_subject = new DevExpress.XtraEditors.TextEdit();
            this.textEdit_sender_email = new DevExpress.XtraEditors.TextEdit();
            this.textEdit_sender_name = new DevExpress.XtraEditors.TextEdit();
            this.textEdit_text_URI = new DevExpress.XtraEditors.TextEdit();
            this.textEdit_html_URI = new DevExpress.XtraEditors.TextEdit();
            this.labelControl3 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl4 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl5 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl6 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl7 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl8 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl9 = new DevExpress.XtraEditors.LabelControl();
            this.gridControl1 = new DevExpress.XtraGrid.GridControl();
            this.gridView1 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gridColumn_name = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn_type = new DevExpress.XtraGrid.Columns.GridColumn();
            this.labelControl2 = new DevExpress.XtraEditors.LabelControl();
            this.barManager1 = new DevExpress.XtraBars.BarManager(this.components);
            this.barDockControlTop = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlBottom = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlLeft = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlRight = new DevExpress.XtraBars.BarDockControl();
            this.barButtonItem_Create = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItem_Update = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItem_Delete = new DevExpress.XtraBars.BarButtonItem();
            this.popupMenu1 = new DevExpress.XtraBars.PopupMenu(this.components);
            ((System.ComponentModel.ISupportInitialize)(this.DxErrorProvider1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lookUpEdit_language.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit_description.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit_subject.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit_sender_email.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit_sender_name.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit_text_URI.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit_html_URI.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.popupMenu1)).BeginInit();
            this.SuspendLayout();
            // 
            // simpleButton_OK
            // 
            this.simpleButton_OK.Location = new System.Drawing.Point(372, 13);
            this.simpleButton_OK.TabIndex = 3;
            // 
            // simpleButton_Cancel
            // 
            this.simpleButton_Cancel.Location = new System.Drawing.Point(372, 56);
            this.simpleButton_Cancel.TabIndex = 4;
            // 
            // DefaultLookAndFeel1
            // 
            this.DefaultLookAndFeel1.LookAndFeel.SkinName = "McSkin";
            // 
            // LabelControl1
            // 
            this.LabelControl1.Location = new System.Drawing.Point(12, 10);
            this.LabelControl1.Name = "LabelControl1";
            this.LabelControl1.Padding = new System.Windows.Forms.Padding(2, 0, 0, 0);
            this.LabelControl1.Size = new System.Drawing.Size(13, 13);
            this.LabelControl1.TabIndex = 0;
            this.LabelControl1.Text = "ID";
            // 
            // labelControl_Id
            // 
            this.labelControl_Id.Location = new System.Drawing.Point(81, 10);
            this.labelControl_Id.Name = "labelControl_Id";
            this.labelControl_Id.Size = new System.Drawing.Size(23, 13);
            this.labelControl_Id.TabIndex = 5;
            this.labelControl_Id.Text = "NEW";
            // 
            // lookUpEdit_language
            // 
            this.lookUpEdit_language.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.lookUpEdit_language.Location = new System.Drawing.Point(81, 186);
            this.lookUpEdit_language.Name = "lookUpEdit_language";
            this.lookUpEdit_language.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.lookUpEdit_language.Properties.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("Id", "ID", 20, DevExpress.Utils.FormatType.Numeric, "f0", false, DevExpress.Utils.HorzAlignment.Far, DevExpress.Data.ColumnSortOrder.None, DevExpress.Utils.DefaultBoolean.True),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("Attribute", "Description", 20, DevExpress.Utils.FormatType.None, "", true, DevExpress.Utils.HorzAlignment.Near, DevExpress.Data.ColumnSortOrder.Ascending, DevExpress.Utils.DefaultBoolean.True)});
            this.lookUpEdit_language.Properties.DisplayMember = "Attribute";
            this.lookUpEdit_language.Properties.NullText = "";
            this.lookUpEdit_language.Properties.ShowFooter = false;
            this.lookUpEdit_language.Properties.ShowHeader = false;
            this.lookUpEdit_language.Properties.SortColumnIndex = 1;
            this.lookUpEdit_language.Properties.ValueMember = "Id";
            this.lookUpEdit_language.Size = new System.Drawing.Size(269, 20);
            this.lookUpEdit_language.TabIndex = 6;
            // 
            // textEdit_description
            // 
            this.textEdit_description.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.textEdit_description.Location = new System.Drawing.Point(81, 30);
            this.textEdit_description.Name = "textEdit_description";
            this.textEdit_description.Properties.MaxLength = 50;
            this.textEdit_description.Size = new System.Drawing.Size(269, 20);
            this.textEdit_description.TabIndex = 7;
            // 
            // textEdit_subject
            // 
            this.textEdit_subject.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.textEdit_subject.Location = new System.Drawing.Point(81, 56);
            this.textEdit_subject.Name = "textEdit_subject";
            this.textEdit_subject.Properties.MaxLength = 50;
            this.textEdit_subject.Size = new System.Drawing.Size(269, 20);
            this.textEdit_subject.TabIndex = 8;
            // 
            // textEdit_sender_email
            // 
            this.textEdit_sender_email.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.textEdit_sender_email.Location = new System.Drawing.Point(81, 82);
            this.textEdit_sender_email.Name = "textEdit_sender_email";
            this.textEdit_sender_email.Properties.MaxLength = 50;
            this.textEdit_sender_email.Size = new System.Drawing.Size(269, 20);
            this.textEdit_sender_email.TabIndex = 9;
            // 
            // textEdit_sender_name
            // 
            this.textEdit_sender_name.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.textEdit_sender_name.Location = new System.Drawing.Point(81, 108);
            this.textEdit_sender_name.Name = "textEdit_sender_name";
            this.textEdit_sender_name.Properties.MaxLength = 50;
            this.textEdit_sender_name.Size = new System.Drawing.Size(269, 20);
            this.textEdit_sender_name.TabIndex = 10;
            // 
            // textEdit_text_URI
            // 
            this.textEdit_text_URI.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.textEdit_text_URI.Location = new System.Drawing.Point(81, 134);
            this.textEdit_text_URI.Name = "textEdit_text_URI";
            this.textEdit_text_URI.Properties.MaxLength = 1024;
            this.textEdit_text_URI.Size = new System.Drawing.Size(269, 20);
            this.textEdit_text_URI.TabIndex = 11;
            // 
            // textEdit_html_URI
            // 
            this.textEdit_html_URI.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.textEdit_html_URI.Location = new System.Drawing.Point(81, 160);
            this.textEdit_html_URI.Name = "textEdit_html_URI";
            this.textEdit_html_URI.Properties.MaxLength = 1024;
            this.textEdit_html_URI.Size = new System.Drawing.Size(269, 20);
            this.textEdit_html_URI.TabIndex = 12;
            // 
            // labelControl3
            // 
            this.labelControl3.Location = new System.Drawing.Point(12, 33);
            this.labelControl3.Name = "labelControl3";
            this.labelControl3.Padding = new System.Windows.Forms.Padding(2, 0, 0, 0);
            this.labelControl3.Size = new System.Drawing.Size(55, 13);
            this.labelControl3.TabIndex = 13;
            this.labelControl3.Text = "Description";
            // 
            // labelControl4
            // 
            this.labelControl4.Location = new System.Drawing.Point(12, 59);
            this.labelControl4.Name = "labelControl4";
            this.labelControl4.Padding = new System.Windows.Forms.Padding(2, 0, 0, 0);
            this.labelControl4.Size = new System.Drawing.Size(38, 13);
            this.labelControl4.TabIndex = 14;
            this.labelControl4.Text = "Subject";
            // 
            // labelControl5
            // 
            this.labelControl5.Location = new System.Drawing.Point(12, 85);
            this.labelControl5.Name = "labelControl5";
            this.labelControl5.Padding = new System.Windows.Forms.Padding(2, 0, 0, 0);
            this.labelControl5.Size = new System.Drawing.Size(52, 13);
            this.labelControl5.TabIndex = 15;
            this.labelControl5.Text = "Email Addr";
            // 
            // labelControl6
            // 
            this.labelControl6.Location = new System.Drawing.Point(12, 111);
            this.labelControl6.Name = "labelControl6";
            this.labelControl6.Padding = new System.Windows.Forms.Padding(2, 0, 0, 0);
            this.labelControl6.Size = new System.Drawing.Size(56, 13);
            this.labelControl6.TabIndex = 16;
            this.labelControl6.Text = "Email Name";
            // 
            // labelControl7
            // 
            this.labelControl7.Location = new System.Drawing.Point(12, 137);
            this.labelControl7.Name = "labelControl7";
            this.labelControl7.Padding = new System.Windows.Forms.Padding(2, 0, 0, 0);
            this.labelControl7.Size = new System.Drawing.Size(45, 13);
            this.labelControl7.TabIndex = 17;
            this.labelControl7.Text = "Text URI";
            // 
            // labelControl8
            // 
            this.labelControl8.Location = new System.Drawing.Point(12, 163);
            this.labelControl8.Name = "labelControl8";
            this.labelControl8.Padding = new System.Windows.Forms.Padding(2, 0, 0, 0);
            this.labelControl8.Size = new System.Drawing.Size(49, 13);
            this.labelControl8.TabIndex = 18;
            this.labelControl8.Text = "HTML URI";
            // 
            // labelControl9
            // 
            this.labelControl9.Location = new System.Drawing.Point(12, 189);
            this.labelControl9.Name = "labelControl9";
            this.labelControl9.Padding = new System.Windows.Forms.Padding(2, 0, 0, 0);
            this.labelControl9.Size = new System.Drawing.Size(49, 13);
            this.labelControl9.TabIndex = 19;
            this.labelControl9.Text = "Language";
            // 
            // gridControl1
            // 
            this.gridControl1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.gridControl1.Location = new System.Drawing.Point(81, 212);
            this.gridControl1.MainView = this.gridView1;
            this.gridControl1.Name = "gridControl1";
            this.gridControl1.Size = new System.Drawing.Size(375, 91);
            this.gridControl1.TabIndex = 20;
            this.gridControl1.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView1});
            // 
            // gridView1
            // 
            this.gridView1.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gridColumn_name,
            this.gridColumn_type});
            this.gridView1.GridControl = this.gridControl1;
            this.gridView1.Name = "gridView1";
            this.gridView1.OptionsBehavior.AutoPopulateColumns = false;
            this.gridView1.OptionsBehavior.Editable = false;
            this.gridView1.OptionsDetail.AllowZoomDetail = false;
            this.gridView1.OptionsDetail.EnableMasterViewMode = false;
            this.gridView1.OptionsDetail.ShowDetailTabs = false;
            this.gridView1.OptionsDetail.SmartDetailExpand = false;
            this.gridView1.OptionsView.ShowDetailButtons = false;
            this.gridView1.OptionsView.ShowGroupExpandCollapseButtons = false;
            this.gridView1.OptionsView.ShowGroupPanel = false;
            this.gridView1.OptionsView.ShowIndicator = false;
            this.gridView1.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.gridColumn_name, DevExpress.Data.ColumnSortOrder.Ascending)});
            // 
            // gridColumn_name
            // 
            this.gridColumn_name.Caption = "Name";
            this.gridColumn_name.FieldName = "Name";
            this.gridColumn_name.Name = "gridColumn_name";
            this.gridColumn_name.OptionsColumn.AllowEdit = false;
            this.gridColumn_name.Visible = true;
            this.gridColumn_name.VisibleIndex = 0;
            // 
            // gridColumn_type
            // 
            this.gridColumn_type.Caption = "Type";
            this.gridColumn_type.FieldName = "Type";
            this.gridColumn_type.Name = "gridColumn_type";
            this.gridColumn_type.OptionsColumn.AllowEdit = false;
            this.gridColumn_type.Visible = true;
            this.gridColumn_type.VisibleIndex = 1;
            // 
            // labelControl2
            // 
            this.labelControl2.Location = new System.Drawing.Point(12, 215);
            this.labelControl2.Name = "labelControl2";
            this.labelControl2.Padding = new System.Windows.Forms.Padding(2, 0, 0, 0);
            this.labelControl2.Size = new System.Drawing.Size(63, 13);
            this.labelControl2.TabIndex = 21;
            this.labelControl2.Text = "Attachments";
            // 
            // barManager1
            // 
            this.barManager1.DockControls.Add(this.barDockControlTop);
            this.barManager1.DockControls.Add(this.barDockControlBottom);
            this.barManager1.DockControls.Add(this.barDockControlLeft);
            this.barManager1.DockControls.Add(this.barDockControlRight);
            this.barManager1.Form = this;
            this.barManager1.Items.AddRange(new DevExpress.XtraBars.BarItem[] {
            this.barButtonItem_Create,
            this.barButtonItem_Update,
            this.barButtonItem_Delete});
            this.barManager1.MaxItemId = 7;
            // 
            // barDockControlTop
            // 
            this.barDockControlTop.CausesValidation = false;
            this.barDockControlTop.Dock = System.Windows.Forms.DockStyle.Top;
            this.barDockControlTop.Location = new System.Drawing.Point(0, 0);
            this.barDockControlTop.Size = new System.Drawing.Size(528, 0);
            // 
            // barDockControlBottom
            // 
            this.barDockControlBottom.CausesValidation = false;
            this.barDockControlBottom.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.barDockControlBottom.Location = new System.Drawing.Point(0, 294);
            this.barDockControlBottom.Size = new System.Drawing.Size(528, 0);
            // 
            // barDockControlLeft
            // 
            this.barDockControlLeft.CausesValidation = false;
            this.barDockControlLeft.Dock = System.Windows.Forms.DockStyle.Left;
            this.barDockControlLeft.Location = new System.Drawing.Point(0, 0);
            this.barDockControlLeft.Size = new System.Drawing.Size(0, 294);
            // 
            // barDockControlRight
            // 
            this.barDockControlRight.CausesValidation = false;
            this.barDockControlRight.Dock = System.Windows.Forms.DockStyle.Right;
            this.barDockControlRight.Location = new System.Drawing.Point(528, 0);
            this.barDockControlRight.Size = new System.Drawing.Size(0, 294);
            // 
            // barButtonItem_Create
            // 
            this.barButtonItem_Create.Caption = "&Add...";
            this.barButtonItem_Create.Id = 4;
            this.barButtonItem_Create.Name = "barButtonItem_Create";
            // 
            // barButtonItem_Update
            // 
            this.barButtonItem_Update.Caption = "&Change...";
            this.barButtonItem_Update.Id = 5;
            this.barButtonItem_Update.Name = "barButtonItem_Update";
            // 
            // barButtonItem_Delete
            // 
            this.barButtonItem_Delete.Caption = "&Delete...";
            this.barButtonItem_Delete.Id = 6;
            this.barButtonItem_Delete.Name = "barButtonItem_Delete";
            // 
            // popupMenu1
            // 
            this.popupMenu1.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.barButtonItem_Create),
            new DevExpress.XtraBars.LinkPersistInfo(this.barButtonItem_Update),
            new DevExpress.XtraBars.LinkPersistInfo(this.barButtonItem_Delete)});
            this.popupMenu1.Manager = this.barManager1;
            this.popupMenu1.Name = "popupMenu1";
            // 
            // EditForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(468, 315);
            this.Controls.Add(this.barDockControlLeft);
            this.Controls.Add(this.barDockControlRight);
            this.Controls.Add(this.barDockControlBottom);
            this.Controls.Add(this.barDockControlTop);
            this.Controls.Add(this.labelControl2);
            this.Controls.Add(this.gridControl1);
            this.Controls.Add(this.labelControl9);
            this.Controls.Add(this.labelControl8);
            this.Controls.Add(this.labelControl7);
            this.Controls.Add(this.labelControl6);
            this.Controls.Add(this.labelControl5);
            this.Controls.Add(this.labelControl4);
            this.Controls.Add(this.labelControl3);
            this.Controls.Add(this.textEdit_html_URI);
            this.Controls.Add(this.textEdit_text_URI);
            this.Controls.Add(this.textEdit_sender_name);
            this.Controls.Add(this.textEdit_sender_email);
            this.Controls.Add(this.textEdit_subject);
            this.Controls.Add(this.textEdit_description);
            this.Controls.Add(this.lookUpEdit_language);
            this.Controls.Add(this.labelControl_Id);
            this.Controls.Add(this.LabelControl1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.Sizable;
            this.MaximizeBox = true;
            this.Name = "EditForm";
            this.Text = "Email Template";
            this.Controls.SetChildIndex(this.LabelControl1, 0);
            this.Controls.SetChildIndex(this.simpleButton_OK, 0);
            this.Controls.SetChildIndex(this.simpleButton_Cancel, 0);
            this.Controls.SetChildIndex(this.labelControl_Id, 0);
            this.Controls.SetChildIndex(this.lookUpEdit_language, 0);
            this.Controls.SetChildIndex(this.textEdit_description, 0);
            this.Controls.SetChildIndex(this.textEdit_subject, 0);
            this.Controls.SetChildIndex(this.textEdit_sender_email, 0);
            this.Controls.SetChildIndex(this.textEdit_sender_name, 0);
            this.Controls.SetChildIndex(this.textEdit_text_URI, 0);
            this.Controls.SetChildIndex(this.textEdit_html_URI, 0);
            this.Controls.SetChildIndex(this.labelControl3, 0);
            this.Controls.SetChildIndex(this.labelControl4, 0);
            this.Controls.SetChildIndex(this.labelControl5, 0);
            this.Controls.SetChildIndex(this.labelControl6, 0);
            this.Controls.SetChildIndex(this.labelControl7, 0);
            this.Controls.SetChildIndex(this.labelControl8, 0);
            this.Controls.SetChildIndex(this.labelControl9, 0);
            this.Controls.SetChildIndex(this.gridControl1, 0);
            this.Controls.SetChildIndex(this.labelControl2, 0);
            this.Controls.SetChildIndex(this.barDockControlTop, 0);
            this.Controls.SetChildIndex(this.barDockControlBottom, 0);
            this.Controls.SetChildIndex(this.barDockControlRight, 0);
            this.Controls.SetChildIndex(this.barDockControlLeft, 0);
            ((System.ComponentModel.ISupportInitialize)(this.DxErrorProvider1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lookUpEdit_language.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit_description.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit_subject.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit_sender_email.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit_sender_name.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit_text_URI.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit_html_URI.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.popupMenu1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

		}
        private DevExpress.XtraEditors.LabelControl LabelControl1;
        private DevExpress.XtraEditors.LabelControl labelControl_Id;
        private DevExpress.XtraEditors.LookUpEdit lookUpEdit_language;
        private DevExpress.XtraEditors.TextEdit textEdit_description;
        private DevExpress.XtraEditors.TextEdit textEdit_subject;
        private DevExpress.XtraEditors.TextEdit textEdit_sender_email;
        private DevExpress.XtraEditors.TextEdit textEdit_sender_name;
        private DevExpress.XtraEditors.TextEdit textEdit_text_URI;
        private DevExpress.XtraEditors.TextEdit textEdit_html_URI;
        private DevExpress.XtraEditors.LabelControl labelControl3;
        private DevExpress.XtraEditors.LabelControl labelControl4;
        private DevExpress.XtraEditors.LabelControl labelControl5;
        private DevExpress.XtraEditors.LabelControl labelControl6;
        private DevExpress.XtraEditors.LabelControl labelControl7;
        private DevExpress.XtraEditors.LabelControl labelControl8;
        private DevExpress.XtraEditors.LabelControl labelControl9;
        private DevExpress.XtraGrid.GridControl gridControl1;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView1;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn_name;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn_type;
        private DevExpress.XtraEditors.LabelControl labelControl2;
        protected DevExpress.XtraBars.BarManager barManager1;
        protected DevExpress.XtraBars.BarDockControl barDockControlTop;
        protected DevExpress.XtraBars.BarDockControl barDockControlBottom;
        protected DevExpress.XtraBars.BarDockControl barDockControlLeft;
        protected DevExpress.XtraBars.BarDockControl barDockControlRight;
        protected DevExpress.XtraBars.BarButtonItem barButtonItem_Create;
        protected DevExpress.XtraBars.BarButtonItem barButtonItem_Update;
        protected DevExpress.XtraBars.BarButtonItem barButtonItem_Delete;
        protected DevExpress.XtraBars.PopupMenu popupMenu1;
    }
}
