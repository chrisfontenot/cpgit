using System;
using DebtPlus.Data.Forms;
using DebtPlus.LINQ;

namespace DebtPlus.UI.TableAdministration.CS.Offices
{
    internal partial class ZipCodeEditForm : DebtPlusForm
    {
        protected zipcode record;

        internal ZipCodeEditForm()
            : base()
        {
            InitializeComponent();
        }

        internal ZipCodeEditForm(zipcode record)
            : this()
        {
            this.record = record;
            RegisterHandlers();
        }

        private void RegisterHandlers()
        {
            Load += ZipCodeEditForm_Load;
            TextEdit1.EditValueChanged += TextEdit1_EditValueChanged;
        }

        private void UnRegisterHandlers()
        {
            Load -= ZipCodeEditForm_Load;
            TextEdit1.EditValueChanged -= TextEdit1_EditValueChanged;
        }

        private void ZipCodeEditForm_Load(object sender, EventArgs e)
        {
            UnRegisterHandlers();
            try
            {
                TextEdit1.EditValue = record.zip_lower.PadRight(5, '0').Substring(0, 5);
                SimpleButton1.Enabled = !HasErrors();
            }
            finally
            {
                RegisterHandlers();
            }
        }

        protected void TextEdit1_EditValueChanged(object sender, EventArgs e)
        {
            SimpleButton1.Enabled = !HasErrors();
        }

        protected bool HasErrors()
        {
            string value = Convert.ToString(TextEdit1.EditValue);
            return DebtPlus.Utils.Format.Strings.DigitsOnly(value).Length != 5;
        }

        protected void SimpleButton1_Click(object sender, EventArgs e)
        {
            string value = Convert.ToString(TextEdit1.EditValue);
            record.zip_lower = value;
            record.zip_upper = value;
        }
    }
}