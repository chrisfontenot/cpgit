namespace DebtPlus.UI.TableAdministration.CS.Offices
{
    partial class OfficeEditForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            try
            {
                if (disposing)
                {
                    if (components != null)
                    {
                        components.Dispose();
                    }
                }
            }
            finally
            {
                base.Dispose(disposing);
            }
        }


        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.LayoutControl1 = new DevExpress.XtraLayout.LayoutControl();
            this.lookupedit_hud_hcs_id = new DevExpress.XtraEditors.LookUpEdit();
            this.zipCodeListControl1 = new ZipCodeListControl();
            this.simpleButton_Cancel = new DevExpress.XtraEditors.SimpleButton();
            this.AddressRecordControl1 = new DebtPlus.Data.Controls.AddressRecordControl();
            this.office_name = new DevExpress.XtraEditors.TextEdit();
            this.office_type = new DevExpress.XtraEditors.LookUpEdit();
            this.TextEdit_NFCC = new DevExpress.XtraEditors.TextEdit();
            this.office_directions = new DevExpress.XtraEditors.MemoEdit();
            this.simpleButton_OK = new DevExpress.XtraEditors.SimpleButton();
            this.TelephoneNumberRecordControl_TelephoneID = new DebtPlus.Data.Controls.TelephoneNumberRecordControl();
            this.office_default_till_missed = new DevExpress.XtraEditors.SpinEdit();
            this.TelephoneNumberRecordControl_FAXID = new DebtPlus.Data.Controls.TelephoneNumberRecordControl();
            this.office_default = new DevExpress.XtraEditors.CheckEdit();
            this.office_district = new DevExpress.XtraEditors.LookUpEdit();
            this.TelephoneNumberRecordControl_AltTelephoneID = new DebtPlus.Data.Controls.TelephoneNumberRecordControl();
            this.office_region = new DevExpress.XtraEditors.LookUpEdit();
            this.office_counselor = new DevExpress.XtraEditors.LookUpEdit();
            this.office_ActiveFlag = new DevExpress.XtraEditors.CheckEdit();
            this.LayoutControlGroup1 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.LayoutControlItem1 = new DevExpress.XtraLayout.LayoutControlItem();
            this.LayoutControlItem13 = new DevExpress.XtraLayout.LayoutControlItem();
            this.item0 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.TabbedControlGroup1 = new DevExpress.XtraLayout.TabbedControlGroup();
            this.LayoutControlGroup3 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.LayoutControlItem10 = new DevExpress.XtraLayout.LayoutControlItem();
            this.LayoutControlItem9 = new DevExpress.XtraLayout.LayoutControlItem();
            this.LayoutControlItem11 = new DevExpress.XtraLayout.LayoutControlItem();
            this.LayoutControlItem5 = new DevExpress.XtraLayout.LayoutControlItem();
            this.LayoutControlItem12 = new DevExpress.XtraLayout.LayoutControlItem();
            this.LayoutControlItem6 = new DevExpress.XtraLayout.LayoutControlItem();
            this.LayoutControlItem8 = new DevExpress.XtraLayout.LayoutControlItem();
            this.LayoutControlItem7 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem16 = new DevExpress.XtraLayout.LayoutControlItem();
            this.LayoutControlGroup4 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.LayoutControlItem4 = new DevExpress.XtraLayout.LayoutControlItem();
            this.LayoutControlItem3 = new DevExpress.XtraLayout.LayoutControlItem();
            this.LayoutControlGroup2 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem19 = new DevExpress.XtraLayout.LayoutControlItem();
            this.LayoutControlItem14 = new DevExpress.XtraLayout.LayoutControlItem();
            this.LayoutControlItem2 = new DevExpress.XtraLayout.LayoutControlItem();
            this.EmptySpaceItem2 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.emptySpaceItem1 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.layoutControlItem17 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem18 = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem3 = new DevExpress.XtraLayout.EmptySpaceItem();
            ((System.ComponentModel.ISupportInitialize)(this.DxErrorProvider1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LayoutControl1)).BeginInit();
            this.LayoutControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.lookupedit_hud_hcs_id.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.AddressRecordControl1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.office_name.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.office_type.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TextEdit_NFCC.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.office_directions.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TelephoneNumberRecordControl_TelephoneID)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.office_default_till_missed.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TelephoneNumberRecordControl_FAXID)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.office_default.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.office_district.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TelephoneNumberRecordControl_AltTelephoneID)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.office_region.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.office_counselor.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.office_ActiveFlag.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LayoutControlGroup1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LayoutControlItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LayoutControlItem13)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.item0)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TabbedControlGroup1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LayoutControlGroup3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LayoutControlItem10)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LayoutControlItem9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LayoutControlItem11)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LayoutControlItem5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LayoutControlItem12)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LayoutControlItem6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LayoutControlItem8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LayoutControlItem7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem16)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LayoutControlGroup4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LayoutControlItem4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LayoutControlItem3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LayoutControlGroup2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem19)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LayoutControlItem14)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LayoutControlItem2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.EmptySpaceItem2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem17)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem18)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem3)).BeginInit();
            this.SuspendLayout();
            // 
            // LayoutControl1
            // 
            this.LayoutControl1.Controls.Add(this.lookupedit_hud_hcs_id);
            this.LayoutControl1.Controls.Add(this.zipCodeListControl1);
            this.LayoutControl1.Controls.Add(this.simpleButton_Cancel);
            this.LayoutControl1.Controls.Add(this.AddressRecordControl1);
            this.LayoutControl1.Controls.Add(this.office_name);
            this.LayoutControl1.Controls.Add(this.office_type);
            this.LayoutControl1.Controls.Add(this.TextEdit_NFCC);
            this.LayoutControl1.Controls.Add(this.office_directions);
            this.LayoutControl1.Controls.Add(this.simpleButton_OK);
            this.LayoutControl1.Controls.Add(this.TelephoneNumberRecordControl_TelephoneID);
            this.LayoutControl1.Controls.Add(this.office_default_till_missed);
            this.LayoutControl1.Controls.Add(this.TelephoneNumberRecordControl_FAXID);
            this.LayoutControl1.Controls.Add(this.office_default);
            this.LayoutControl1.Controls.Add(this.office_district);
            this.LayoutControl1.Controls.Add(this.TelephoneNumberRecordControl_AltTelephoneID);
            this.LayoutControl1.Controls.Add(this.office_region);
            this.LayoutControl1.Controls.Add(this.office_counselor);
            this.LayoutControl1.Controls.Add(this.office_ActiveFlag);
            this.LayoutControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.LayoutControl1.Location = new System.Drawing.Point(0, 0);
            this.LayoutControl1.Name = "LayoutControl1";
            this.LayoutControl1.OptionsCustomizationForm.DesignTimeCustomizationFormPositionAndSize = new System.Drawing.Rectangle(442, 153, 250, 350);
            this.LayoutControl1.Root = this.LayoutControlGroup1;
            this.LayoutControl1.Size = new System.Drawing.Size(666, 337);
            this.LayoutControl1.TabIndex = 30;
            this.LayoutControl1.Text = "LayoutControl1";
            // 
            // lookupedit_hud_hcs_id
            // 
            this.lookupedit_hud_hcs_id.Location = new System.Drawing.Point(370, 191);
            this.lookupedit_hud_hcs_id.Name = "lookupedit_hud_hcs_id";
            this.lookupedit_hud_hcs_id.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.lookupedit_hud_hcs_id.Properties.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("Id", "ID", 5, DevExpress.Utils.FormatType.Numeric, "f0", true, DevExpress.Utils.HorzAlignment.Default),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("Description", "Description", 20, DevExpress.Utils.FormatType.None, "", true, DevExpress.Utils.HorzAlignment.Default, DevExpress.Data.ColumnSortOrder.Ascending, DevExpress.Utils.DefaultBoolean.True)});
            this.lookupedit_hud_hcs_id.Properties.DisplayMember = "Description";
            this.lookupedit_hud_hcs_id.Properties.NullText = "";
            this.lookupedit_hud_hcs_id.Properties.ShowFooter = false;
            this.lookupedit_hud_hcs_id.Properties.ValueMember = "Id";
            this.lookupedit_hud_hcs_id.Size = new System.Drawing.Size(206, 20);
            this.lookupedit_hud_hcs_id.StyleController = this.LayoutControl1;
            this.lookupedit_hud_hcs_id.TabIndex = 34;
            // 
            // zipCodeListControl1
            // 
            this.zipCodeListControl1.Location = new System.Drawing.Point(14, 95);
            this.zipCodeListControl1.Name = "zipCodeListControl1";
            this.zipCodeListControl1.Size = new System.Drawing.Size(562, 176);
            this.zipCodeListControl1.TabIndex = 33;
            // 
            // simpleButton_Cancel
            // 
            this.simpleButton_Cancel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.simpleButton_Cancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.simpleButton_Cancel.Location = new System.Drawing.Point(592, 56);
            this.simpleButton_Cancel.MaximumSize = new System.Drawing.Size(72, 26);
            this.simpleButton_Cancel.MinimumSize = new System.Drawing.Size(72, 26);
            this.simpleButton_Cancel.Name = "simpleButton_Cancel";
            this.simpleButton_Cancel.Size = new System.Drawing.Size(72, 26);
            this.simpleButton_Cancel.StyleController = this.LayoutControl1;
            this.simpleButton_Cancel.TabIndex = 32;
            this.simpleButton_Cancel.Text = "&Cancel";
            this.simpleButton_Cancel.ToolTip = "Click here to cancel the dialog and return to the previous form.";
            this.simpleButton_Cancel.ToolTipController = this.ToolTipController1;
            // 
            // AddressRecordControl1
            // 
            this.AddressRecordControl1.Location = new System.Drawing.Point(116, 95);
            this.AddressRecordControl1.Name = "AddressRecordControl1";
            this.AddressRecordControl1.Size = new System.Drawing.Size(460, 72);
            this.AddressRecordControl1.TabIndex = 27;
            // 
            // office_name
            // 
            this.office_name.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.office_name.Location = new System.Drawing.Point(106, 2);
            this.office_name.Name = "office_name";
            this.office_name.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.False;
            this.office_name.Properties.MaxLength = 50;
            this.office_name.Properties.NullText = "...A Name Is Required...";
            this.office_name.Size = new System.Drawing.Size(480, 20);
            this.office_name.StyleController = this.LayoutControl1;
            this.office_name.TabIndex = 1;
            // 
            // office_type
            // 
            this.office_type.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.office_type.Location = new System.Drawing.Point(106, 26);
            this.office_type.Name = "office_type";
            this.office_type.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.office_type.Properties.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("Id", "type", 20, DevExpress.Utils.FormatType.Numeric, "", false, DevExpress.Utils.HorzAlignment.Default),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("description", "Description", 20, DevExpress.Utils.FormatType.None, "", true, DevExpress.Utils.HorzAlignment.Default, DevExpress.Data.ColumnSortOrder.Ascending)});
            this.office_type.Properties.DisplayMember = "description";
            this.office_type.Properties.NullText = "Please Choose One From The Following...";
            this.office_type.Properties.ShowFooter = false;
            this.office_type.Properties.ShowHeader = false;
            this.office_type.Properties.ShowLines = false;
            this.office_type.Properties.SortColumnIndex = 1;
            this.office_type.Properties.ValueMember = "Id";
            this.office_type.Size = new System.Drawing.Size(480, 20);
            this.office_type.StyleController = this.LayoutControl1;
            this.office_type.TabIndex = 3;
            // 
            // TextEdit_NFCC
            // 
            this.TextEdit_NFCC.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.TextEdit_NFCC.EditValue = "";
            this.TextEdit_NFCC.Location = new System.Drawing.Point(370, 215);
            this.TextEdit_NFCC.Name = "TextEdit_NFCC";
            this.TextEdit_NFCC.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.True;
            this.TextEdit_NFCC.Properties.MaxLength = 4;
            this.TextEdit_NFCC.Size = new System.Drawing.Size(206, 20);
            this.TextEdit_NFCC.StyleController = this.LayoutControl1;
            this.TextEdit_NFCC.TabIndex = 23;
            // 
            // office_directions
            // 
            this.office_directions.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.office_directions.Location = new System.Drawing.Point(116, 171);
            this.office_directions.MinimumSize = new System.Drawing.Size(0, 100);
            this.office_directions.Name = "office_directions";
            this.office_directions.Properties.MaxLength = 4096;
            this.office_directions.Size = new System.Drawing.Size(460, 100);
            this.office_directions.StyleController = this.LayoutControl1;
            this.office_directions.TabIndex = 7;
            // 
            // simpleButton_OK
            // 
            this.simpleButton_OK.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.simpleButton_OK.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.simpleButton_OK.Enabled = false;
            this.simpleButton_OK.Location = new System.Drawing.Point(592, 26);
            this.simpleButton_OK.MaximumSize = new System.Drawing.Size(72, 26);
            this.simpleButton_OK.MinimumSize = new System.Drawing.Size(72, 26);
            this.simpleButton_OK.Name = "simpleButton_OK";
            this.simpleButton_OK.Size = new System.Drawing.Size(72, 26);
            this.simpleButton_OK.StyleController = this.LayoutControl1;
            this.simpleButton_OK.TabIndex = 31;
            this.simpleButton_OK.Text = "&OK";
            this.simpleButton_OK.ToolTip = "Click here to commit the changes to the database";
            this.simpleButton_OK.ToolTipController = this.ToolTipController1;
            // 
            // TelephoneNumberRecordControl_TelephoneID
            // 
            this.TelephoneNumberRecordControl_TelephoneID.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.TelephoneNumberRecordControl_TelephoneID.ErrorIcon = null;
            this.TelephoneNumberRecordControl_TelephoneID.ErrorText = "";
            this.TelephoneNumberRecordControl_TelephoneID.Location = new System.Drawing.Point(116, 167);
            this.TelephoneNumberRecordControl_TelephoneID.Margin = new System.Windows.Forms.Padding(0);
            this.TelephoneNumberRecordControl_TelephoneID.Name = "TelephoneNumberRecordControl_TelephoneID";
            this.TelephoneNumberRecordControl_TelephoneID.Size = new System.Drawing.Size(148, 20);
            this.TelephoneNumberRecordControl_TelephoneID.TabIndex = 9;
            // 
            // office_default_till_missed
            // 
            this.office_default_till_missed.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.office_default_till_missed.EditValue = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.office_default_till_missed.Location = new System.Drawing.Point(370, 167);
            this.office_default_till_missed.Name = "office_default_till_missed";
            this.office_default_till_missed.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.True;
            this.office_default_till_missed.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.office_default_till_missed.Properties.DisplayFormat.FormatString = "f0";
            this.office_default_till_missed.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.office_default_till_missed.Properties.EditFormat.FormatString = "f0";
            this.office_default_till_missed.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.office_default_till_missed.Properties.IsFloatValue = false;
            this.office_default_till_missed.Properties.Mask.EditMask = "f0";
            this.office_default_till_missed.Properties.MaxLength = 3;
            this.office_default_till_missed.Properties.MaxValue = new decimal(new int[] {
            999,
            0,
            0,
            0});
            this.office_default_till_missed.Properties.MinValue = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.office_default_till_missed.Size = new System.Drawing.Size(206, 20);
            this.office_default_till_missed.StyleController = this.LayoutControl1;
            this.office_default_till_missed.TabIndex = 15;
            // 
            // TelephoneNumberRecordControl_FAXID
            // 
            this.TelephoneNumberRecordControl_FAXID.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.TelephoneNumberRecordControl_FAXID.Location = new System.Drawing.Point(116, 215);
            this.TelephoneNumberRecordControl_FAXID.Margin = new System.Windows.Forms.Padding(0);
            this.TelephoneNumberRecordControl_FAXID.Name = "TelephoneNumberRecordControl_FAXID";
            this.TelephoneNumberRecordControl_FAXID.Size = new System.Drawing.Size(148, 20);
            this.TelephoneNumberRecordControl_FAXID.TabIndex = 13;
            // 
            // office_default
            // 
            this.office_default.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.office_default.Location = new System.Drawing.Point(284, 315);
            this.office_default.Name = "office_default";
            this.office_default.Properties.Caption = "Default";
            this.office_default.Size = new System.Drawing.Size(304, 20);
            this.office_default.StyleController = this.LayoutControl1;
            this.office_default.TabIndex = 25;
            // 
            // office_district
            // 
            this.office_district.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.office_district.Location = new System.Drawing.Point(116, 119);
            this.office_district.Name = "office_district";
            this.office_district.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.office_district.Properties.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("Id", "District", 20, DevExpress.Utils.FormatType.Numeric, "f0", false, DevExpress.Utils.HorzAlignment.Default),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("description", "Description", 20, DevExpress.Utils.FormatType.None, "", true, DevExpress.Utils.HorzAlignment.Default, DevExpress.Data.ColumnSortOrder.Ascending)});
            this.office_district.Properties.DisplayMember = "description";
            this.office_district.Properties.NullText = "Please Choose One From The Following...";
            this.office_district.Properties.ShowFooter = false;
            this.office_district.Properties.ShowHeader = false;
            this.office_district.Properties.ShowLines = false;
            this.office_district.Properties.SortColumnIndex = 1;
            this.office_district.Properties.ValueMember = "Id";
            this.office_district.Size = new System.Drawing.Size(460, 20);
            this.office_district.StyleController = this.LayoutControl1;
            this.office_district.TabIndex = 21;
            // 
            // TelephoneNumberRecordControl_AltTelephoneID
            // 
            this.TelephoneNumberRecordControl_AltTelephoneID.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.TelephoneNumberRecordControl_AltTelephoneID.Location = new System.Drawing.Point(116, 191);
            this.TelephoneNumberRecordControl_AltTelephoneID.Margin = new System.Windows.Forms.Padding(0);
            this.TelephoneNumberRecordControl_AltTelephoneID.Name = "TelephoneNumberRecordControl_AltTelephoneID";
            this.TelephoneNumberRecordControl_AltTelephoneID.Size = new System.Drawing.Size(148, 20);
            this.TelephoneNumberRecordControl_AltTelephoneID.TabIndex = 11;
            // 
            // office_region
            // 
            this.office_region.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.office_region.Location = new System.Drawing.Point(116, 95);
            this.office_region.Name = "office_region";
            this.office_region.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.office_region.Properties.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("Id", "Region", 20, DevExpress.Utils.FormatType.Numeric, "f0", false, DevExpress.Utils.HorzAlignment.Default),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("description", "Description", 20, DevExpress.Utils.FormatType.None, "", true, DevExpress.Utils.HorzAlignment.Default, DevExpress.Data.ColumnSortOrder.Ascending)});
            this.office_region.Properties.DisplayMember = "description";
            this.office_region.Properties.NullText = "Please Choose One From The Following...";
            this.office_region.Properties.ShowFooter = false;
            this.office_region.Properties.ShowHeader = false;
            this.office_region.Properties.ShowLines = false;
            this.office_region.Properties.SortColumnIndex = 1;
            this.office_region.Properties.ValueMember = "Id";
            this.office_region.Size = new System.Drawing.Size(460, 20);
            this.office_region.StyleController = this.LayoutControl1;
            this.office_region.TabIndex = 19;
            // 
            // office_counselor
            // 
            this.office_counselor.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.office_counselor.Location = new System.Drawing.Point(116, 143);
            this.office_counselor.Name = "office_counselor";
            this.office_counselor.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.office_counselor.Properties.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("Id", "Counselor", 20, DevExpress.Utils.FormatType.Numeric, "", false, DevExpress.Utils.HorzAlignment.Default),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("Name", "Name", 20, DevExpress.Utils.FormatType.None, "", true, DevExpress.Utils.HorzAlignment.Default, DevExpress.Data.ColumnSortOrder.Ascending)});
            this.office_counselor.Properties.DisplayMember = "Name";
            this.office_counselor.Properties.NullText = "Please Choose One From The Following...";
            this.office_counselor.Properties.ShowFooter = false;
            this.office_counselor.Properties.ShowHeader = false;
            this.office_counselor.Properties.ShowLines = false;
            this.office_counselor.Properties.SortColumnIndex = 1;
            this.office_counselor.Properties.ValueMember = "Id";
            this.office_counselor.Size = new System.Drawing.Size(460, 20);
            this.office_counselor.StyleController = this.LayoutControl1;
            this.office_counselor.TabIndex = 17;
            // 
            // office_ActiveFlag
            // 
            this.office_ActiveFlag.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.office_ActiveFlag.Location = new System.Drawing.Point(2, 315);
            this.office_ActiveFlag.Name = "office_ActiveFlag";
            this.office_ActiveFlag.Properties.Caption = "Active";
            this.office_ActiveFlag.Size = new System.Drawing.Size(278, 20);
            this.office_ActiveFlag.StyleController = this.LayoutControl1;
            this.office_ActiveFlag.TabIndex = 26;
            // 
            // LayoutControlGroup1
            // 
            this.LayoutControlGroup1.CustomizationFormText = "LayoutControlGroup1";
            this.LayoutControlGroup1.GroupBordersVisible = false;
            this.LayoutControlGroup1.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.LayoutControlItem1,
            this.LayoutControlItem13,
            this.item0,
            this.TabbedControlGroup1,
            this.LayoutControlItem14,
            this.LayoutControlItem2,
            this.EmptySpaceItem2,
            this.emptySpaceItem1,
            this.layoutControlItem17,
            this.layoutControlItem18,
            this.emptySpaceItem3});
            this.LayoutControlGroup1.Location = new System.Drawing.Point(0, 0);
            this.LayoutControlGroup1.Name = "Root";
            this.LayoutControlGroup1.Size = new System.Drawing.Size(666, 337);
            this.LayoutControlGroup1.Text = "Root";
            this.LayoutControlGroup1.TextVisible = false;
            // 
            // LayoutControlItem1
            // 
            this.LayoutControlItem1.Control = this.office_name;
            this.LayoutControlItem1.CustomizationFormText = "Name";
            this.LayoutControlItem1.Location = new System.Drawing.Point(0, 0);
            this.LayoutControlItem1.Name = "LayoutControlItem1";
            this.LayoutControlItem1.Padding = new DevExpress.XtraLayout.Utils.Padding(4, 4, 2, 2);
            this.LayoutControlItem1.Size = new System.Drawing.Size(590, 24);
            this.LayoutControlItem1.Text = "Name";
            this.LayoutControlItem1.TextSize = new System.Drawing.Size(98, 13);
            // 
            // LayoutControlItem13
            // 
            this.LayoutControlItem13.Control = this.office_default;
            this.LayoutControlItem13.CustomizationFormText = "Default";
            this.LayoutControlItem13.Location = new System.Drawing.Point(282, 313);
            this.LayoutControlItem13.Name = "LayoutControlItem13";
            this.LayoutControlItem13.Size = new System.Drawing.Size(308, 24);
            this.LayoutControlItem13.Text = "Default";
            this.LayoutControlItem13.TextSize = new System.Drawing.Size(0, 0);
            this.LayoutControlItem13.TextToControlDistance = 0;
            this.LayoutControlItem13.TextVisible = false;
            // 
            // item0
            // 
            this.item0.AllowHotTrack = false;
            this.item0.CustomizationFormText = "item0";
            this.item0.Location = new System.Drawing.Point(0, 285);
            this.item0.MinSize = new System.Drawing.Size(1, 1);
            this.item0.Name = "item0";
            this.item0.Size = new System.Drawing.Size(590, 28);
            this.item0.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.item0.Text = "item0";
            this.item0.TextSize = new System.Drawing.Size(0, 0);
            // 
            // TabbedControlGroup1
            // 
            this.TabbedControlGroup1.CustomizationFormText = "TabbedControlGroup1";
            this.TabbedControlGroup1.Location = new System.Drawing.Point(0, 59);
            this.TabbedControlGroup1.Name = "TabbedControlGroup1";
            this.TabbedControlGroup1.SelectedTabPage = this.LayoutControlGroup3;
            this.TabbedControlGroup1.SelectedTabPageIndex = 0;
            this.TabbedControlGroup1.Size = new System.Drawing.Size(590, 226);
            this.TabbedControlGroup1.TabPages.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.LayoutControlGroup3,
            this.LayoutControlGroup4,
            this.LayoutControlGroup2});
            this.TabbedControlGroup1.Text = "TabbedControlGroup1";
            // 
            // LayoutControlGroup3
            // 
            this.LayoutControlGroup3.CustomizationFormText = "General";
            this.LayoutControlGroup3.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.LayoutControlItem10,
            this.LayoutControlItem9,
            this.LayoutControlItem11,
            this.LayoutControlItem5,
            this.LayoutControlItem12,
            this.LayoutControlItem6,
            this.LayoutControlItem8,
            this.LayoutControlItem7,
            this.layoutControlItem16});
            this.LayoutControlGroup3.Location = new System.Drawing.Point(0, 0);
            this.LayoutControlGroup3.Name = "LayoutControlGroup3";
            this.LayoutControlGroup3.Size = new System.Drawing.Size(566, 180);
            this.LayoutControlGroup3.Text = "General";
            // 
            // LayoutControlItem10
            // 
            this.LayoutControlItem10.Control = this.office_region;
            this.LayoutControlItem10.CustomizationFormText = "Region";
            this.LayoutControlItem10.Location = new System.Drawing.Point(0, 0);
            this.LayoutControlItem10.Name = "LayoutControlItem10";
            this.LayoutControlItem10.Size = new System.Drawing.Size(566, 24);
            this.LayoutControlItem10.Text = "Region";
            this.LayoutControlItem10.TextSize = new System.Drawing.Size(98, 13);
            // 
            // LayoutControlItem9
            // 
            this.LayoutControlItem9.Control = this.office_counselor;
            this.LayoutControlItem9.CustomizationFormText = "Counselor";
            this.LayoutControlItem9.Location = new System.Drawing.Point(0, 48);
            this.LayoutControlItem9.Name = "LayoutControlItem9";
            this.LayoutControlItem9.Size = new System.Drawing.Size(566, 24);
            this.LayoutControlItem9.Text = "Counselor";
            this.LayoutControlItem9.TextSize = new System.Drawing.Size(98, 13);
            // 
            // LayoutControlItem11
            // 
            this.LayoutControlItem11.Control = this.office_district;
            this.LayoutControlItem11.CustomizationFormText = "District";
            this.LayoutControlItem11.Location = new System.Drawing.Point(0, 24);
            this.LayoutControlItem11.Name = "LayoutControlItem11";
            this.LayoutControlItem11.Size = new System.Drawing.Size(566, 24);
            this.LayoutControlItem11.Text = "District";
            this.LayoutControlItem11.TextSize = new System.Drawing.Size(98, 13);
            // 
            // LayoutControlItem5
            // 
            this.LayoutControlItem5.Control = this.TelephoneNumberRecordControl_TelephoneID;
            this.LayoutControlItem5.CustomizationFormText = "Phone";
            this.LayoutControlItem5.Location = new System.Drawing.Point(0, 72);
            this.LayoutControlItem5.Name = "LayoutControlItem5";
            this.LayoutControlItem5.Size = new System.Drawing.Size(254, 24);
            this.LayoutControlItem5.Text = "Phone";
            this.LayoutControlItem5.TextSize = new System.Drawing.Size(98, 13);
            // 
            // LayoutControlItem12
            // 
            this.LayoutControlItem12.Control = this.TextEdit_NFCC;
            this.LayoutControlItem12.CustomizationFormText = "NFCC ID";
            this.LayoutControlItem12.Location = new System.Drawing.Point(254, 120);
            this.LayoutControlItem12.Name = "LayoutControlItem12";
            this.LayoutControlItem12.Size = new System.Drawing.Size(312, 60);
            this.LayoutControlItem12.Text = "NFCC ID";
            this.LayoutControlItem12.TextSize = new System.Drawing.Size(98, 13);
            // 
            // LayoutControlItem6
            // 
            this.LayoutControlItem6.Control = this.TelephoneNumberRecordControl_AltTelephoneID;
            this.LayoutControlItem6.CustomizationFormText = "Alternate Phone";
            this.LayoutControlItem6.Location = new System.Drawing.Point(0, 96);
            this.LayoutControlItem6.Name = "LayoutControlItem6";
            this.LayoutControlItem6.Size = new System.Drawing.Size(254, 24);
            this.LayoutControlItem6.Text = "Alternate Phone";
            this.LayoutControlItem6.TextSize = new System.Drawing.Size(98, 13);
            // 
            // LayoutControlItem8
            // 
            this.LayoutControlItem8.Control = this.office_default_till_missed;
            this.LayoutControlItem8.CustomizationFormText = "Default Until Missed:";
            this.LayoutControlItem8.Location = new System.Drawing.Point(254, 72);
            this.LayoutControlItem8.Name = "LayoutControlItem8";
            this.LayoutControlItem8.Size = new System.Drawing.Size(312, 24);
            this.LayoutControlItem8.Text = "Default Until Missed:";
            this.LayoutControlItem8.TextSize = new System.Drawing.Size(98, 13);
            // 
            // LayoutControlItem7
            // 
            this.LayoutControlItem7.Control = this.TelephoneNumberRecordControl_FAXID;
            this.LayoutControlItem7.CustomizationFormText = "FAX";
            this.LayoutControlItem7.Location = new System.Drawing.Point(0, 120);
            this.LayoutControlItem7.Name = "LayoutControlItem7";
            this.LayoutControlItem7.Size = new System.Drawing.Size(254, 60);
            this.LayoutControlItem7.Text = "FAX";
            this.LayoutControlItem7.TextSize = new System.Drawing.Size(98, 13);
            // 
            // layoutControlItem16
            // 
            this.layoutControlItem16.Control = this.lookupedit_hud_hcs_id;
            this.layoutControlItem16.CustomizationFormText = "HCS ID";
            this.layoutControlItem16.Location = new System.Drawing.Point(254, 96);
            this.layoutControlItem16.Name = "layoutControlItem16";
            this.layoutControlItem16.Size = new System.Drawing.Size(312, 24);
            this.layoutControlItem16.Text = "HCS ID";
            this.layoutControlItem16.TextSize = new System.Drawing.Size(98, 13);
            // 
            // LayoutControlGroup4
            // 
            this.LayoutControlGroup4.CustomizationFormText = "Location";
            this.LayoutControlGroup4.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.LayoutControlItem4,
            this.LayoutControlItem3});
            this.LayoutControlGroup4.Location = new System.Drawing.Point(0, 0);
            this.LayoutControlGroup4.Name = "LayoutControlGroup4";
            this.LayoutControlGroup4.Size = new System.Drawing.Size(566, 180);
            this.LayoutControlGroup4.Text = "Location";
            // 
            // LayoutControlItem4
            // 
            this.LayoutControlItem4.AppearanceItemCaption.Options.UseTextOptions = true;
            this.LayoutControlItem4.AppearanceItemCaption.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Top;
            this.LayoutControlItem4.Control = this.office_directions;
            this.LayoutControlItem4.CustomizationFormText = "Directions";
            this.LayoutControlItem4.Location = new System.Drawing.Point(0, 76);
            this.LayoutControlItem4.Name = "LayoutControlItem4";
            this.LayoutControlItem4.Size = new System.Drawing.Size(566, 104);
            this.LayoutControlItem4.Text = "Directions";
            this.LayoutControlItem4.TextSize = new System.Drawing.Size(98, 13);
            // 
            // LayoutControlItem3
            // 
            this.LayoutControlItem3.AppearanceItemCaption.Options.UseTextOptions = true;
            this.LayoutControlItem3.AppearanceItemCaption.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Top;
            this.LayoutControlItem3.Control = this.AddressRecordControl1;
            this.LayoutControlItem3.CustomizationFormText = "Address";
            this.LayoutControlItem3.Location = new System.Drawing.Point(0, 0);
            this.LayoutControlItem3.Name = "LayoutControlItem3";
            this.LayoutControlItem3.Size = new System.Drawing.Size(566, 76);
            this.LayoutControlItem3.Text = "Address";
            this.LayoutControlItem3.TextSize = new System.Drawing.Size(98, 13);
            // 
            // LayoutControlGroup2
            // 
            this.LayoutControlGroup2.CustomizationFormText = "Postal Codes";
            this.LayoutControlGroup2.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem19});
            this.LayoutControlGroup2.Location = new System.Drawing.Point(0, 0);
            this.LayoutControlGroup2.Name = "LayoutControlGroup2";
            this.LayoutControlGroup2.Size = new System.Drawing.Size(566, 180);
            this.LayoutControlGroup2.Text = "Postal Codes";
            // 
            // layoutControlItem19
            // 
            this.layoutControlItem19.Control = this.zipCodeListControl1;
            this.layoutControlItem19.CustomizationFormText = "layoutControlItem19";
            this.layoutControlItem19.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem19.Name = "layoutControlItem19";
            this.layoutControlItem19.Size = new System.Drawing.Size(566, 180);
            this.layoutControlItem19.Text = "layoutControlItem19";
            this.layoutControlItem19.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem19.TextToControlDistance = 0;
            this.layoutControlItem19.TextVisible = false;
            // 
            // LayoutControlItem14
            // 
            this.LayoutControlItem14.Control = this.office_ActiveFlag;
            this.LayoutControlItem14.CustomizationFormText = "Active";
            this.LayoutControlItem14.Location = new System.Drawing.Point(0, 313);
            this.LayoutControlItem14.Name = "LayoutControlItem14";
            this.LayoutControlItem14.Size = new System.Drawing.Size(282, 24);
            this.LayoutControlItem14.Text = "Active";
            this.LayoutControlItem14.TextSize = new System.Drawing.Size(0, 0);
            this.LayoutControlItem14.TextToControlDistance = 0;
            this.LayoutControlItem14.TextVisible = false;
            // 
            // LayoutControlItem2
            // 
            this.LayoutControlItem2.Control = this.office_type;
            this.LayoutControlItem2.CustomizationFormText = "type";
            this.LayoutControlItem2.Location = new System.Drawing.Point(0, 24);
            this.LayoutControlItem2.Name = "LayoutControlItem2";
            this.LayoutControlItem2.Padding = new DevExpress.XtraLayout.Utils.Padding(4, 4, 2, 2);
            this.LayoutControlItem2.Size = new System.Drawing.Size(590, 24);
            this.LayoutControlItem2.Text = "type";
            this.LayoutControlItem2.TextSize = new System.Drawing.Size(98, 13);
            // 
            // EmptySpaceItem2
            // 
            this.EmptySpaceItem2.AllowHotTrack = false;
            this.EmptySpaceItem2.CustomizationFormText = "EmptySpaceItem2";
            this.EmptySpaceItem2.Location = new System.Drawing.Point(0, 48);
            this.EmptySpaceItem2.Name = "EmptySpaceItem2";
            this.EmptySpaceItem2.Size = new System.Drawing.Size(590, 11);
            this.EmptySpaceItem2.Text = "EmptySpaceItem2";
            this.EmptySpaceItem2.TextSize = new System.Drawing.Size(0, 0);
            // 
            // emptySpaceItem1
            // 
            this.emptySpaceItem1.AllowHotTrack = false;
            this.emptySpaceItem1.CustomizationFormText = "emptySpaceItem1";
            this.emptySpaceItem1.Location = new System.Drawing.Point(590, 84);
            this.emptySpaceItem1.Name = "emptySpaceItem1";
            this.emptySpaceItem1.Size = new System.Drawing.Size(76, 253);
            this.emptySpaceItem1.Text = "emptySpaceItem1";
            this.emptySpaceItem1.TextSize = new System.Drawing.Size(0, 0);
            // 
            // layoutControlItem17
            // 
            this.layoutControlItem17.Control = this.simpleButton_OK;
            this.layoutControlItem17.CustomizationFormText = "layoutControlItem17";
            this.layoutControlItem17.Location = new System.Drawing.Point(590, 24);
            this.layoutControlItem17.Name = "layoutControlItem17";
            this.layoutControlItem17.Size = new System.Drawing.Size(76, 30);
            this.layoutControlItem17.Text = "layoutControlItem17";
            this.layoutControlItem17.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem17.TextToControlDistance = 0;
            this.layoutControlItem17.TextVisible = false;
            // 
            // layoutControlItem18
            // 
            this.layoutControlItem18.Control = this.simpleButton_Cancel;
            this.layoutControlItem18.CustomizationFormText = "layoutControlItem18";
            this.layoutControlItem18.Location = new System.Drawing.Point(590, 54);
            this.layoutControlItem18.Name = "layoutControlItem18";
            this.layoutControlItem18.Size = new System.Drawing.Size(76, 30);
            this.layoutControlItem18.Text = "layoutControlItem18";
            this.layoutControlItem18.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem18.TextToControlDistance = 0;
            this.layoutControlItem18.TextVisible = false;
            // 
            // emptySpaceItem3
            // 
            this.emptySpaceItem3.AllowHotTrack = false;
            this.emptySpaceItem3.CustomizationFormText = "emptySpaceItem3";
            this.emptySpaceItem3.Location = new System.Drawing.Point(590, 0);
            this.emptySpaceItem3.Name = "emptySpaceItem3";
            this.emptySpaceItem3.Size = new System.Drawing.Size(76, 24);
            this.emptySpaceItem3.Text = "emptySpaceItem3";
            this.emptySpaceItem3.TextSize = new System.Drawing.Size(0, 0);
            // 
            // OfficeEditForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(666, 337);
            this.Controls.Add(this.LayoutControl1);
            this.Name = "OfficeEditForm";
            this.Text = "OfficeEditForm";
            ((System.ComponentModel.ISupportInitialize)(this.DxErrorProvider1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LayoutControl1)).EndInit();
            this.LayoutControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.lookupedit_hud_hcs_id.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.AddressRecordControl1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.office_name.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.office_type.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TextEdit_NFCC.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.office_directions.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TelephoneNumberRecordControl_TelephoneID)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.office_default_till_missed.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TelephoneNumberRecordControl_FAXID)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.office_default.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.office_district.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TelephoneNumberRecordControl_AltTelephoneID)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.office_region.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.office_counselor.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.office_ActiveFlag.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LayoutControlGroup1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LayoutControlItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LayoutControlItem13)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.item0)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TabbedControlGroup1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LayoutControlGroup3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LayoutControlItem10)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LayoutControlItem9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LayoutControlItem11)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LayoutControlItem5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LayoutControlItem12)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LayoutControlItem6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LayoutControlItem8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LayoutControlItem7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem16)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LayoutControlGroup4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LayoutControlItem4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LayoutControlItem3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LayoutControlGroup2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem19)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LayoutControlItem14)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LayoutControlItem2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.EmptySpaceItem2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem17)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem18)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem3)).EndInit();
            this.ResumeLayout(false);

        }


        protected DevExpress.XtraLayout.LayoutControl LayoutControl1;
        protected DevExpress.XtraEditors.SimpleButton simpleButton_Cancel;
        protected DebtPlus.Data.Controls.AddressRecordControl AddressRecordControl1;
        protected DevExpress.XtraEditors.TextEdit office_name;
        protected DevExpress.XtraEditors.LookUpEdit office_type;
        protected DevExpress.XtraEditors.TextEdit TextEdit_NFCC;
        protected DevExpress.XtraEditors.MemoEdit office_directions;
        protected DevExpress.XtraEditors.SimpleButton simpleButton_OK;
        protected DebtPlus.Data.Controls.TelephoneNumberRecordControl TelephoneNumberRecordControl_TelephoneID;
        protected DebtPlus.Data.Controls.TelephoneNumberRecordControl TelephoneNumberRecordControl_FAXID;
        protected DebtPlus.Data.Controls.TelephoneNumberRecordControl TelephoneNumberRecordControl_AltTelephoneID;
        protected DevExpress.XtraEditors.SpinEdit office_default_till_missed;
        protected DevExpress.XtraEditors.CheckEdit office_default;
        protected DevExpress.XtraEditors.LookUpEdit office_district;
        protected DevExpress.XtraEditors.LookUpEdit office_region;
        protected DevExpress.XtraEditors.LookUpEdit office_counselor;
        protected DevExpress.XtraEditors.CheckEdit office_ActiveFlag;
        protected DevExpress.XtraLayout.LayoutControlGroup LayoutControlGroup1;
        protected DevExpress.XtraLayout.LayoutControlItem LayoutControlItem1;
        protected DevExpress.XtraLayout.LayoutControlItem LayoutControlItem13;
        protected DevExpress.XtraLayout.EmptySpaceItem item0;
        protected DevExpress.XtraLayout.TabbedControlGroup TabbedControlGroup1;
        protected DevExpress.XtraLayout.LayoutControlGroup LayoutControlGroup2;
        protected DevExpress.XtraLayout.LayoutControlGroup LayoutControlGroup3;
        protected DevExpress.XtraLayout.LayoutControlItem LayoutControlItem10;
        protected DevExpress.XtraLayout.LayoutControlItem LayoutControlItem9;
        protected DevExpress.XtraLayout.LayoutControlItem LayoutControlItem11;
        protected DevExpress.XtraLayout.LayoutControlItem LayoutControlItem5;
        protected DevExpress.XtraLayout.LayoutControlItem LayoutControlItem12;
        protected DevExpress.XtraLayout.LayoutControlItem LayoutControlItem6;
        protected DevExpress.XtraLayout.LayoutControlItem LayoutControlItem8;
        protected DevExpress.XtraLayout.LayoutControlItem LayoutControlItem7;
        protected DevExpress.XtraLayout.LayoutControlGroup LayoutControlGroup4;
        protected DevExpress.XtraLayout.LayoutControlItem LayoutControlItem4;
        protected DevExpress.XtraLayout.LayoutControlItem LayoutControlItem3;
        protected DevExpress.XtraLayout.LayoutControlItem LayoutControlItem14;
        protected DevExpress.XtraLayout.LayoutControlItem LayoutControlItem2;
        protected DevExpress.XtraLayout.EmptySpaceItem EmptySpaceItem2;
        protected DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem1;
        protected DevExpress.XtraLayout.LayoutControlItem layoutControlItem17;
        protected DevExpress.XtraLayout.LayoutControlItem layoutControlItem18;
        protected DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem3;
        protected DevExpress.XtraLayout.LayoutControlItem layoutControlItem19;
        protected ZipCodeListControl zipCodeListControl1;
        protected DevExpress.XtraEditors.LookUpEdit lookupedit_hud_hcs_id;
        protected DevExpress.XtraLayout.LayoutControlItem layoutControlItem16;
    }
}