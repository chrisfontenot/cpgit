using System;
using System.Linq;
using System.Windows.Forms;
using DebtPlus.Data.Forms;
using DebtPlus.LINQ;

namespace DebtPlus.UI.TableAdministration.CS.Offices
{
    public partial class OfficeEditForm : DebtPlusForm
    {
        protected DebtPlus.LINQ.office record = null;
        protected BusinessContext bc = null;

        public OfficeEditForm()
            : base()
        {
            InitializeComponent();
        }

        public OfficeEditForm(BusinessContext bc, DebtPlus.LINQ.office record)
            : this()
        {
            this.bc = bc;
            this.record = record;

            // Define the lookup fields
            office_type.Properties.DataSource = DebtPlus.LINQ.Cache.OfficeType.getList();
            office_region.Properties.DataSource = DebtPlus.LINQ.Cache.region.getList();
            office_district.Properties.DataSource = DebtPlus.LINQ.Cache.district.getList();
            office_counselor.Properties.DataSource = DebtPlus.LINQ.Cache.counselor.getList();

            lookupedit_hud_hcs_id.Properties.DataSource = bc.Housing_ARM_hcs_ids.ToList();

            // Register the event handlers
            RegisterHandlers();
        }

        private void RegisterHandlers()
        {
            Load += OfficeEditForm_Load;

            lookupedit_hud_hcs_id.EditValueChanged += LookupEdit_EditValueChanged;
            office_type.EditValueChanged += LookupEdit_EditValueChanged;
            office_region.EditValueChanged += LookupEdit_EditValueChanged;
            office_district.EditValueChanged += LookupEdit_EditValueChanged;
            office_counselor.EditValueChanged += LookupEdit_EditValueChanged;
            office_name.EditValueChanged += office_name_EditValueChanged;

            lookupedit_hud_hcs_id.EditValueChanging += DebtPlus.Data.Validation.LookUpEdit_ActiveTest;
            office_type.EditValueChanging += DebtPlus.Data.Validation.LookUpEdit_ActiveTest;
            office_region.EditValueChanging += DebtPlus.Data.Validation.LookUpEdit_ActiveTest;
            office_district.EditValueChanging += DebtPlus.Data.Validation.LookUpEdit_ActiveTest;
            office_counselor.EditValueChanging += DebtPlus.Data.Validation.LookUpEdit_ActiveTest;

            simpleButton_OK.Click += simpleButton_OK_Click;
        }

        private void UnRegisterHandlers()
        {
            Load -= OfficeEditForm_Load;

            lookupedit_hud_hcs_id.EditValueChanged -= LookupEdit_EditValueChanged;
            office_type.EditValueChanged -= LookupEdit_EditValueChanged;
            office_region.EditValueChanged -= LookupEdit_EditValueChanged;
            office_district.EditValueChanged -= LookupEdit_EditValueChanged;
            office_counselor.EditValueChanged -= LookupEdit_EditValueChanged;
            office_name.EditValueChanged -= office_name_EditValueChanged;

            lookupedit_hud_hcs_id.EditValueChanging -= DebtPlus.Data.Validation.LookUpEdit_ActiveTest;
            office_type.EditValueChanging -= DebtPlus.Data.Validation.LookUpEdit_ActiveTest;
            office_region.EditValueChanging -= DebtPlus.Data.Validation.LookUpEdit_ActiveTest;
            office_district.EditValueChanging -= DebtPlus.Data.Validation.LookUpEdit_ActiveTest;
            office_counselor.EditValueChanging -= DebtPlus.Data.Validation.LookUpEdit_ActiveTest;

            simpleButton_OK.Click -= simpleButton_OK_Click;
        }

        private void OfficeEditForm_Load(object sender, EventArgs e)
        {
            UnRegisterHandlers();

            try
            {
                // Update the fields with the data
                office_ActiveFlag.Checked = record.ActiveFlag;
                office_counselor.EditValue = record.counselor;
                office_default.Checked = record.Default;
                office_default_till_missed.EditValue = record.default_till_missed;
                office_directions.EditValue = record.directions;
                office_district.EditValue = record.district;
                office_name.EditValue = record.name;
                office_region.EditValue = record.region;
                office_type.EditValue = record.type;
                TextEdit_NFCC.EditValue = record.nfcc;
                lookupedit_hud_hcs_id.EditValue = record.hud_hcs_id;

                TelephoneNumberRecordControl_AltTelephoneID.EditValue = record.AltTelephoneID;
                TelephoneNumberRecordControl_FAXID.EditValue = record.FAXID;
                TelephoneNumberRecordControl_TelephoneID.EditValue = record.TelephoneID;
                AddressRecordControl1.EditValue = record.AddressID;

                // Load the list of zipcodes
                zipCodeListControl1.ReadForm(record);
            }
            finally
            {
                RegisterHandlers();
            }

            simpleButton_OK.Enabled = !HasErrors();
        }

        protected bool HasErrors()
        {
            return office_ActiveFlag.ErrorText != string.Empty
                   || office_counselor.ErrorText != string.Empty
                   || office_default.ErrorText != string.Empty
                   || office_default_till_missed.ErrorText != string.Empty
                   || office_directions.ErrorText != string.Empty
                   || office_district.ErrorText != string.Empty
                   || office_name.ErrorText != string.Empty
                   || office_region.ErrorText != string.Empty
                   || office_type.ErrorText != string.Empty;
        }

        protected void LookupEdit_EditValueChanged(object sender, EventArgs e)
        {
            var errorText = string.Empty;
            if (((DevExpress.XtraEditors.LookUpEdit)sender).EditValue == null) errorText = "A value is required";

            DxErrorProvider1.SetError((Control)sender, errorText);

            simpleButton_OK.Enabled = !HasErrors();
        }

        protected void office_name_EditValueChanged(object sender, EventArgs e)
        {
            var errorText = string.Empty;
            if (DebtPlus.Utils.Nulls.DStr(((DevExpress.XtraEditors.TextEdit)sender).EditValue, string.Empty).Trim() == string.Empty) errorText = "A value is required";

            DxErrorProvider1.SetError((Control)sender, errorText);

            simpleButton_OK.Enabled = !HasErrors();
        }

        protected void simpleButton_OK_Click(object sender, EventArgs e)
        {
            record.ActiveFlag = office_ActiveFlag.Checked;
            record.Default = office_default.Checked;
            record.counselor = DebtPlus.Utils.Nulls.v_Int32(office_counselor.EditValue);
            record.default_till_missed = DebtPlus.Utils.Nulls.v_Int32(office_default_till_missed.EditValue);
            record.directions = DebtPlus.Utils.Nulls.v_String(office_directions.EditValue);
            record.district = Convert.ToInt32(office_district.EditValue);
            record.name = Convert.ToString(office_name.EditValue);
            record.region = Convert.ToInt32(office_region.EditValue);
            record.type = DebtPlus.Utils.Nulls.v_Int32(office_type.EditValue);
            record.nfcc = DebtPlus.Utils.Nulls.v_String(TextEdit_NFCC.EditValue);
            record.hud_hcs_id = DebtPlus.Utils.Nulls.v_Int32(lookupedit_hud_hcs_id.EditValue);
            record.TelephoneID = TelephoneNumberRecordControl_TelephoneID.EditValue;
            record.AltTelephoneID = TelephoneNumberRecordControl_AltTelephoneID.EditValue;
            record.FAXID = TelephoneNumberRecordControl_FAXID.EditValue;
            record.AddressID = AddressRecordControl1.EditValue;
        }
    }
}