using System;
using System.Windows.Forms;
using DebtPlus.LINQ;

namespace DebtPlus.UI.TableAdministration.CS.Offices
{
    public partial class ZipCodeListControl : DevExpress.XtraEditors.XtraUserControl
    {
        protected Int32 popupMenu1_RowHandle = -1;
        protected office record;

        protected void ZipCodeListControl_Dispose(bool disposing)
        {
            if (disposing)
            {
                UnRegisterHandlers();
            }
        }

        internal ZipCodeListControl()
        {
            InitializeComponent();
            RegisterHandlers();
        }

        protected void RegisterHandlers()
        {
            barButtonItem_Add.ItemClick += barButtonItem_Add_Click;
            barButtonItem_Delete.ItemClick += barButtonItem_Delete_Click;
            barButtonItem_Change.ItemClick += barButtonItem_Change_Click;
            gridView1.DoubleClick += gridView1_DoubleClick;
            gridView1.MouseDown += gridView1_MouseDown;
        }

        protected void UnRegisterHandlers()
        {
            barButtonItem_Add.ItemClick += barButtonItem_Add_Click;
            barButtonItem_Delete.ItemClick -= barButtonItem_Delete_Click;
            barButtonItem_Change.ItemClick -= barButtonItem_Change_Click;
            gridView1.DoubleClick -= gridView1_DoubleClick;
            gridView1.MouseDown -= gridView1_MouseDown;
        }

        protected void gridView1_MouseDown(object sender, MouseEventArgs e)
        {
            DevExpress.XtraGrid.Views.Grid.ViewInfo.GridHitInfo hi = gridView1.CalcHitInfo(e.Location);
            popupMenu1_RowHandle = hi.RowHandle;
            gridView1.FocusedRowHandle = popupMenu1_RowHandle;
            if (e.Button != MouseButtons.Right)
            {
                return;
            }

            barButtonItem_Change.Enabled = false;
            barButtonItem_Delete.Enabled = false;
            object obj = gridView1.GetRow(popupMenu1_RowHandle);
            if (obj != null)
            {
                barButtonItem_Change.Enabled = true;
                barButtonItem_Delete.Enabled = true;
            }
            popupMenu1.ShowPopup(gridControl1.PointToScreen(e.Location));
        }

        internal void ReadForm(office record)
        {
            // Save the office for later use
            this.record = record;

            // Load the zipcode list into the grid control
            gridControl1.DataSource = record.zipcodes;
        }

        /// <summary>
        /// Edit the current row
        /// </summary>
        /// <param name="obj"></param>
        internal void UpdateRecord(object obj)
        {
            // Find the zipcode for the edit operation
            zipcode z = obj as zipcode;
            if (z == null)
            {
                return;
            }

            // Edit the zipcode data
            using (var frm = new ZipCodeEditForm(z))
            {
                if (frm.ShowDialog() != System.Windows.Forms.DialogResult.OK)
                {
                    return;
                }
            }
        }

        /// <summary>
        /// Create a new record for the database
        /// </summary>
        protected void CreateRecord()
        {
            // Create the new record
            zipcode z = new zipcode();

            // Edit the data
            using (var frm = new ZipCodeEditForm(z))
            {
                if (frm.ShowDialog() != System.Windows.Forms.DialogResult.OK)
                {
                    return;
                }
            }

            // Submit the new office to the system when the office is updated
            record.zipcodes.Add(z);
        }

        /// <summary>
        /// Delete the office from the list of offices
        /// </summary>
        /// <param name="obj"></param>
        protected void DeleteRecord(object obj)
        {
            // Find the zipcode for the edit operation
            zipcode z = obj as zipcode;
            if (z == null)
            {
                return;
            }

            if (DebtPlus.Data.Prompts.RequestConfirmation_Delete() != System.Windows.Forms.DialogResult.Yes)
            {
                return;
            }

            // Discard the record from the list of zipcodes
            record.zipcodes.Remove(z);
            z.office1 = null;
        }

        /// <summary>
        /// A DOUBLE-CLICK on a row is always an EDIT operation.
        /// </summary>
        protected void gridView1_DoubleClick(object sender, EventArgs e)
        {
            var hi = gridView1.CalcHitInfo((gridControl1.PointToClient(MousePosition)));
            var rowHandle = hi.IsValid && hi.InRow ? hi.RowHandle : -1;
            gridView1.FocusedRowHandle = rowHandle;
            if (rowHandle < 0)
            {
                return;
            }

            object obj = gridView1.GetRow(gridView1.FocusedRowHandle);
            if (obj != null)
            {
                UpdateRecord(obj);
            }
        }

        protected void barButtonItem_Add_Click(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            CreateRecord();
        }

        protected void barButtonItem_Delete_Click(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            var hi = gridView1.CalcHitInfo((gridControl1.PointToClient(MousePosition)));
            var rowHandle = hi.IsValid && hi.InRow ? hi.RowHandle : -1;
            gridView1.FocusedRowHandle = rowHandle;
            if (rowHandle < 0)
            {
                return;
            }

            object obj = gridView1.GetRow(gridView1.FocusedRowHandle);
            if (obj != null)
            {
                DeleteRecord(obj);
            }
        }

        protected void barButtonItem_Change_Click(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            object obj = gridView1.GetRow(gridView1.FocusedRowHandle);
            if (obj != null)
            {
                UpdateRecord(obj);
            }
        }
    }
}