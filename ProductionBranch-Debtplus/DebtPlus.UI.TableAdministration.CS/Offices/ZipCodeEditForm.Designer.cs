namespace DebtPlus.UI.TableAdministration.CS.Offices
{
    partial class ZipCodeEditForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            try
            {
                if (disposing)
                {
                    if (components != null)
                    {
                        components.Dispose();
                    }
                }
            }
            finally
            {
                base.Dispose(disposing);
            }
        }


        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.SimpleButton2 = new DevExpress.XtraEditors.SimpleButton();
            this.SimpleButton1 = new DevExpress.XtraEditors.SimpleButton();
            this.TextEdit1 = new DevExpress.XtraEditors.TextEdit();
            this.LabelControl1 = new DevExpress.XtraEditors.LabelControl();
            ((System.ComponentModel.ISupportInitialize)(this.DxErrorProvider1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TextEdit1.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // DefaultLookAndFeel1
            // 
            this.DefaultLookAndFeel1.LookAndFeel.SkinName = "McSkin";
            // 
            // SimpleButton2
            // 
            this.SimpleButton2.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.SimpleButton2.Location = new System.Drawing.Point(144, 50);
            this.SimpleButton2.Name = "SimpleButton2";
            this.SimpleButton2.Size = new System.Drawing.Size(75, 23);
            this.SimpleButton2.TabIndex = 7;
            this.SimpleButton2.Text = "Cancel";
            // 
            // SimpleButton1
            // 
            this.SimpleButton1.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.SimpleButton1.Location = new System.Drawing.Point(63, 50);
            this.SimpleButton1.Name = "SimpleButton1";
            this.SimpleButton1.Size = new System.Drawing.Size(75, 23);
            this.SimpleButton1.TabIndex = 6;
            this.SimpleButton1.Text = "OK";
            this.SimpleButton1.Click += new System.EventHandler(this.SimpleButton1_Click);
            // 
            // TextEdit1
            // 
            this.TextEdit1.Location = new System.Drawing.Point(123, 9);
            this.TextEdit1.Name = "TextEdit1";
            this.TextEdit1.Properties.Mask.BeepOnError = true;
            this.TextEdit1.Properties.Mask.EditMask = "00000";
            this.TextEdit1.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Simple;
            this.TextEdit1.Properties.Mask.SaveLiteral = false;
            this.TextEdit1.Properties.MaxLength = 5;
            this.TextEdit1.Size = new System.Drawing.Size(100, 20);
            this.TextEdit1.TabIndex = 5;
            // 
            // LabelControl1
            // 
            this.LabelControl1.Location = new System.Drawing.Point(60, 12);
            this.LabelControl1.Name = "LabelControl1";
            this.LabelControl1.Size = new System.Drawing.Size(57, 13);
            this.LabelControl1.TabIndex = 4;
            this.LabelControl1.Text = "Postal Code";
            // 
            // ZipCodeEditForm
            // 
            this.AcceptButton = this.SimpleButton1;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.CancelButton = this.SimpleButton2;
            this.ClientSize = new System.Drawing.Size(282, 82);
            this.Controls.Add(this.SimpleButton2);
            this.Controls.Add(this.SimpleButton1);
            this.Controls.Add(this.TextEdit1);
            this.Controls.Add(this.LabelControl1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.Fixed3D;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "ZipCodeEditForm";
            this.Text = "Postal Code";
            ((System.ComponentModel.ISupportInitialize)(this.DxErrorProvider1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TextEdit1.Properties)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }


        protected DevExpress.XtraEditors.SimpleButton SimpleButton2;
        protected DevExpress.XtraEditors.SimpleButton SimpleButton1;
        protected DevExpress.XtraEditors.TextEdit TextEdit1;
        protected DevExpress.XtraEditors.LabelControl LabelControl1;
    }
}