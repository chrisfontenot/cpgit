using System;
using System.Linq;
using System.Windows.Forms;
using DebtPlus.Data.Forms;
using DebtPlus.LINQ;

namespace DebtPlus.UI.TableAdministration.CS.Offices
{
    internal partial class OfficeListForm : DebtPlusForm
    {
        private Int32 popupMenu1_RowHandle = -1;
        private System.Collections.Generic.List<office> colRecords;

        // Context for editing all of the information in the records.
        private BusinessContext bc = new BusinessContext();

        internal OfficeListForm()
        {
            InitializeComponent();
            RegisterHandlers();
        }

        private void RegisterHandlers()
        {
            Load += MainTemplateForm_Load;
            barButtonItem_Create.ItemClick += barButtonItem_Create_Click;
            barButtonItem_Delete.ItemClick += barButtonItem_Delete_Click;
            barButtonItem_Edit.ItemClick += barButtonItem_Edit_Click;
            gridView1.MouseDown += gridView1_MouseDown;
            gridView1.DoubleClick += gridView1_DoubleClick;
            simpleButton_New.Click += simpleButton_New_Click;
            SimpleButton_Edit.Click += SimpleButton_Edit_Click;
            simpleButton_OK.Click += simpleButton_OK_Click;
        }

        private void UnRegisterHandlers()
        {
            Load -= MainTemplateForm_Load;
            barButtonItem_Create.ItemClick -= barButtonItem_Create_Click;
            barButtonItem_Delete.ItemClick -= barButtonItem_Delete_Click;
            barButtonItem_Edit.ItemClick -= barButtonItem_Edit_Click;
            gridView1.MouseDown -= gridView1_MouseDown;
            gridView1.DoubleClick -= gridView1_DoubleClick;
            simpleButton_New.Click -= simpleButton_New_Click;
            SimpleButton_Edit.Click -= SimpleButton_Edit_Click;
            simpleButton_OK.Click -= simpleButton_OK_Click;
        }

        private void MainTemplateForm_Load(object sender, EventArgs e)
        {
            UnRegisterHandlers();
            Int32 oldCommandTimeout = bc.CommandTimeout;
            try
            {
                // This can take a while since the list is long
                bc.CommandTimeout = System.Math.Max(bc.CommandTimeout, 600);
                using (var cm = new DebtPlus.UI.Common.CursorManager())
                {
                    // Load the directions and zipcode list along with the offices.
                    System.Data.Linq.DataLoadOptions dl = new System.Data.Linq.DataLoadOptions();
                    dl.LoadWith<office>(s => s.directions);
                    dl.LoadWith<office>(s => s.zipcodes);
                    bc.LoadOptions = dl;
                    bc.DeferredLoadingEnabled = false;

                    // Get the list offices and their corresponding zipcodes
                    colRecords = bc.offices.ToList();
                    gridControl1.DataSource = colRecords;

                    if (colRecords.Count > 0)
                    {
                        gridView1.FocusedRowHandle = 0;
                    }
                }
            }
            finally
            {
                bc.CommandTimeout = oldCommandTimeout;
                RegisterHandlers();
            }
        }

        /// <summary>
        /// Edit the current row
        /// </summary>
        /// <param name="obj"></param>
        internal void UpdateRecord(object obj)
        {
            // Find the office for the edit operation
            office o = obj as office;
            if (o == null)
            {
                return;
            }

            // Edit the office data
            using (var frm = new OfficeEditForm(bc, o))
            {
                if (frm.ShowDialog() != System.Windows.Forms.DialogResult.OK)
                {
                    return;
                }
            }

            // Do any pending updates if the dialog was updated successfully.
            bc.SubmitChanges();
        }

        /// <summary>
        /// Create a new record for the database
        /// </summary>
        private void CreateRecord()
        {
            // Create the new office record
            office o = new office();
            o.type = DebtPlus.LINQ.Cache.OfficeType.getDefault();

            // Edit the office data
            using (var frm = new OfficeEditForm(bc, o))
            {
                if (frm.ShowDialog() != System.Windows.Forms.DialogResult.OK)
                {
                    return;
                }
            }

            // Submit the new office to the system
            bc.offices.InsertOnSubmit(o);
            bc.SubmitChanges();

            // Add the new office to the current display list
            colRecords.Add(o);
        }

        /// <summary>
        /// Delete the office from the list of offices
        /// </summary>
        /// <param name="obj"></param>
        private void DeleteRecord(object obj)
        {
            // Find the office for the edit operation
            office o = obj as office;
            if (o == null)
            {
                return;
            }

            if (DebtPlus.Data.Prompts.RequestConfirmation_Delete() != System.Windows.Forms.DialogResult.Yes)
            {
                return;
            }

            // Delete the record from the database
            bc.offices.DeleteOnSubmit(o);
            bc.SubmitChanges();

            // Remove the office from the display list
            colRecords.Remove(o);
        }

        private void gridView1_MouseDown(object sender, MouseEventArgs e)
        {
            var hi = gridView1.CalcHitInfo(gridControl1.PointToClient(MousePosition));
            popupMenu1_RowHandle = hi.RowHandle;
            gridView1.FocusedRowHandle = popupMenu1_RowHandle;
            if (e.Button != MouseButtons.Right)
            {
                return;
            }

            // Enable or disable the edit operation
            barButtonItem_Edit.Enabled = false;
            barButtonItem_Delete.Enabled = false;
            object obj = gridView1.GetRow(gridView1.FocusedRowHandle);
            if (obj != null)
            {
                barButtonItem_Edit.Enabled = true;
                barButtonItem_Delete.Enabled = true;
            }
            popupMenu1.ShowPopup(MousePosition);
        }

        private void gridView1_DoubleClick(object sender, EventArgs e)
        {
            var hi = gridView1.CalcHitInfo((gridControl1.PointToClient(MousePosition)));
            popupMenu1_RowHandle = hi.RowHandle;
            if (popupMenu1_RowHandle < 0)
            {
                return;
            }

            gridView1.FocusedRowHandle = popupMenu1_RowHandle;
            object obj = gridView1.GetRow(gridView1.FocusedRowHandle);
            if (obj != null)
            {
                UpdateRecord(obj);
            }
        }

        private void barButtonItem_Create_Click(object sender, EventArgs e)
        {
            CreateRecord();
        }

        private void barButtonItem_Delete_Click(object sender, EventArgs e)
        {
            var hi = gridView1.CalcHitInfo((gridControl1.PointToClient(MousePosition)));
            popupMenu1_RowHandle = hi.RowHandle;
            if (popupMenu1_RowHandle < 0)
            {
                return;
            }

            gridView1.FocusedRowHandle = popupMenu1_RowHandle;
            object obj = gridView1.GetRow(gridView1.FocusedRowHandle);
            if (obj != null)
            {
                DeleteRecord(obj);
            }
        }

        private void barButtonItem_Edit_Click(object sender, EventArgs e)
        {
            object obj = gridView1.GetRow(gridView1.FocusedRowHandle);
            if (obj != null)
            {
                UpdateRecord(obj);
            }
        }

        private void simpleButton_New_Click(object sender, EventArgs e)
        {
            CreateRecord();
        }

        private void SimpleButton_Edit_Click(object sender, EventArgs e)
        {
            object obj = gridView1.GetRow(gridView1.FocusedRowHandle);
            if (obj != null)
            {
                UpdateRecord(obj);
            }
        }

        private void simpleButton_OK_Click(object sender, EventArgs e)
        {
            DialogResult = DialogResult.Cancel;
            if (!Modal)
            {
                Close();
            }
        }
    }
}