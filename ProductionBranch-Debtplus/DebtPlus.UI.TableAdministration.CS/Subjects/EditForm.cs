﻿using System;
using DebtPlus.LINQ;

namespace DebtPlus.UI.TableAdministration.CS.Subjects
{
    internal partial class EditForm : Templates.EditTemplateForm
    {
        // Current record being edited
        private subject record;

        /// <summary>
        /// Initialize the new class instance
        /// </summary>
        internal EditForm()
            : base()
        {
            InitializeComponent();
        }

        /// <summary>
        /// Initialize the new class instance
        /// </summary>
        /// <param name="record">Pointer to the record to be edited</param>
        internal EditForm(subject record)
            : this()
        {
            this.record = record;
            RegisterHandlers();
        }

        /// <summary>
        /// Add the event handlers to the collection
        /// </summary>
        private void RegisterHandlers()
        {
            Load += EditForm_Load;
            TextEdit_description.EditValueChanged += FormChanged;
        }

        /// <summary>
        /// Remove the event handlers from being processed
        /// </summary>
        private void UnRegisterHandlers()
        {
            Load -= EditForm_Load;
            TextEdit_description.EditValueChanged -= FormChanged;
        }

        /// <summary>
        /// Process the FORM LOAD event
        /// </summary>
        private void EditForm_Load(object sender, System.EventArgs e)
        {
            UnRegisterHandlers();
            try
            {
                LabelControl_ID.Text = record.Id <= 0 ? "NEW" : record.Id.ToString();
                TextEdit_description.Text = record.description.ToString().Trim();
                simpleButton_OK.Enabled = !HasErrors();
            }
            finally
            {
                RegisterHandlers();
            }
        }

        /// <summary>
        /// Override the OK button to save the editing values into the record
        /// </summary>
        protected override void simpleButton_OK_Click(object sender, EventArgs e)
        {
            base.simpleButton_OK_Click(sender, e);
            record.description = TextEdit_description.Text.Trim();
        }

        /// <summary>
        /// Process a change on the form
        /// </summary>
        private void FormChanged(object sender, EventArgs e)
        {
            simpleButton_OK.Enabled = !HasErrors();
        }

        /// <summary>
        /// Determine if the input form has error(s)
        /// </summary>
        /// <returns>TRUE if the input control has one or more errors</returns>
        private bool HasErrors()
        {
            return TextEdit_description.Text.Trim() == string.Empty;
        }
    }
}