#region "Copyright 2000-2012 DebtPlus, L.L.C."

//{*******************************************************************}
//{                                                                   }
//{       DebtPlus Debt Management System                             }
//{                                                                   }
//{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
//{       ALL RIGHTS RESERVED                                         }
//{                                                                   }
//{   The entire contents of this file is protected by U.S. and       }
//{   International Copyright Laws. Unauthorized reproduction,        }
//{   reverse-engineering, and distribution of all or any portion of  }
//{   the code contained in this file is strictly prohibited and may  }
//{   result in severe civil and criminal penalties and will be       }
//{   prosecuted to the maximum extent possible under the law.        }
//{                                                                   }
//{   RESTRICTIONS                                                    }
//{                                                                   }
//{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
//{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
//{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
//{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
//{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
//{                                                                   }
//{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
//{   ADDITIONAL RESTRICTIONS.                                        }
//{                                                                   }
//{*******************************************************************}

#endregion "Copyright 2000-2012 DebtPlus, L.L.C."

using System;
using System.Windows.Forms;

namespace DebtPlus.UI.TableAdministration.CS.Templates
{
    public partial class MainForm : DebtPlus.Data.Forms.DebtPlusForm
    {
        public MainForm()
            : base()
        {
            InitializeComponent();
            RegisterHandlers();
        }

        /// <summary>
        /// Register the event handlers
        /// </summary>
        private void RegisterHandlers()
        {
            barButtonItem_Create.ItemClick += barButtonItem_Create_ItemClick;
            barButtonItem_Update.ItemClick += barButtonItem_Update_ItemClick;
            barButtonItem_Delete.ItemClick += barButtonItem_Delete_ItemClick;
            gridView1.FocusedRowChanged += gridView1_FocusedRowChanged;
            gridView1.DoubleClick += gridView1_DoubleClick;
            gridView1.MouseDown += gridView1_MouseDown;
            SimpleButton_Edit.Click += SimpleButton_Edit_Click;
            simpleButton_Cancel.Click += simpleButton_Cancel_Click;
            simpleButton_New.Click += simpleButton_New_Click;
        }

        /// <summary>
        /// Remove the event handler registration
        /// </summary>
        private void UnRegisterHandlers()
        {
            barButtonItem_Create.ItemClick -= barButtonItem_Create_ItemClick;
            barButtonItem_Update.ItemClick -= barButtonItem_Update_ItemClick;
            barButtonItem_Delete.ItemClick -= barButtonItem_Delete_ItemClick;
            gridView1.FocusedRowChanged -= gridView1_FocusedRowChanged;
            gridView1.DoubleClick -= gridView1_DoubleClick;
            gridView1.MouseDown -= gridView1_MouseDown;
            SimpleButton_Edit.Click -= SimpleButton_Edit_Click;
            simpleButton_Cancel.Click -= simpleButton_Cancel_Click;
            simpleButton_New.Click -= simpleButton_New_Click;
        }

        /// <summary>
        /// Process the Click event on the Cancel button to close the form
        /// </summary>
        protected virtual void simpleButton_Cancel_Click(object sender, System.EventArgs e)
        {
            Close();
        }

        /// <summary>
        /// Process the Click event on the New button
        /// </summary>
        protected virtual void simpleButton_New_Click(object sender, System.EventArgs e)
        {
            CreateRecord();
        }

        /// <summary>
        ///     The focused row changed. Enable the Edit button as needed.
        /// </summary>
        private void gridView1_FocusedRowChanged(object sender, DevExpress.XtraGrid.Views.Base.FocusedRowChangedEventArgs e)
        {
            SimpleButton_Edit.Enabled = e.FocusedRowHandle >= 0;
        }

        /// <summary>
        /// Double click event on the list
        /// </summary>
        private void gridView1_MouseDown(object sender, MouseEventArgs e)
        {
            // Find the record to be edited from the list
            if (e.Button == System.Windows.Forms.MouseButtons.Right)
            {
                DevExpress.XtraGrid.Views.Grid.ViewInfo.GridHitInfo hi = gridView1.CalcHitInfo((gridControl1.PointToClient(System.Windows.Forms.Control.MousePosition)));
                System.Int32 RowHandle = hi.RowHandle;
                gridView1.FocusedRowHandle = RowHandle;

                // Find the record for this row
                object obj = null;
                if (RowHandle >= 0)
                {
                    obj = gridView1.GetRow(RowHandle);
                }

                // If the row is defined then edit the row.
                if (obj != null)
                {
                    barButtonItem_Update.Enabled = true;
                    barButtonItem_Delete.Enabled = true;
                }
                else
                {
                    barButtonItem_Update.Enabled = false;
                    barButtonItem_Delete.Enabled = false;
                }

                popupMenu1.ShowPopup(System.Windows.Forms.Control.MousePosition);
            }
        }

        /// <summary>
        /// Double click event on the list
        /// </summary>
        private void gridView1_DoubleClick(object sender, System.EventArgs e)
        {
            DevExpress.XtraGrid.Views.Grid.ViewInfo.GridHitInfo hi = gridView1.CalcHitInfo((gridControl1.PointToClient(System.Windows.Forms.Control.MousePosition)));
            System.Int32 RowHandle = hi.RowHandle;

            // Find the record to be edited from the list
            if (RowHandle >= 0)
            {
                gridView1.FocusedRowHandle = RowHandle;
                Object obj = gridView1.GetRow(RowHandle);
                if (obj != null)
                {
                    UpdateRecord(obj);
                }
            }
        }

        /// <summary>
        /// Edit the current record
        /// </summary>
        private void SimpleButton_Edit_Click(System.Object sender, System.EventArgs e)
        {
            System.Int32 RowHandle = gridView1.FocusedRowHandle;

            // Find the row that is being edited
            Object obj = gridView1.GetRow(RowHandle);
            if (obj != null)
            {
                UpdateRecord(obj);
            }
        }

        /// <summary>
        /// Edit -> Create menu item clicked
        /// </summary>
        private void barButtonItem_Create_ItemClick(System.Object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            CreateRecord();
        }

        /// <summary>
        /// Edit -> Edit menu item clicked
        /// </summary>
        private void barButtonItem_Update_ItemClick(System.Object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            System.Int32 RowHandle = gridView1.FocusedRowHandle;

            if (RowHandle >= 0)
            {
                gridView1.FocusedRowHandle = RowHandle;
                object obj = gridView1.GetRow(RowHandle);
                if (obj != null)
                {
                    UpdateRecord(obj);
                }
            }
        }

        /// <summary>
        /// Edit -> Delete menu item clicked
        /// </summary>
        private void barButtonItem_Delete_ItemClick(System.Object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            System.Int32 RowHandle = gridView1.FocusedRowHandle;

            if (RowHandle >= 0)
            {
                gridView1.FocusedRowHandle = RowHandle;
                object obj = gridView1.GetRow(RowHandle);
                if (obj != null)
                {
                    DeleteRecord(obj);
                }
            }
        }

        // Routines to process the CUD operations
        protected virtual void CreateRecord() { }

        protected virtual void UpdateRecord(object obj)
        {
        }

        protected virtual void DeleteRecord(object obj)
        {
        }
    }
}