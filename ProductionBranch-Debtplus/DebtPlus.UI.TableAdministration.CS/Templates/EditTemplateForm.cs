#region "Copyright 2000-2012 DebtPlus, L.L.C."

//{*******************************************************************}
//{                                                                   }
//{       DebtPlus Debt Management System                             }
//{                                                                   }
//{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
//{       ALL RIGHTS RESERVED                                         }
//{                                                                   }
//{   The entire contents of this file is protected by U.S. and       }
//{   International Copyright Laws. Unauthorized reproduction,        }
//{   reverse-engineering, and distribution of all or any portion of  }
//{   the code contained in this file is strictly prohibited and may  }
//{   result in severe civil and criminal penalties and will be       }
//{   prosecuted to the maximum extent possible under the law.        }
//{                                                                   }
//{   RESTRICTIONS                                                    }
//{                                                                   }
//{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
//{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
//{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
//{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
//{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
//{                                                                   }
//{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
//{   ADDITIONAL RESTRICTIONS.                                        }
//{                                                                   }
//{*******************************************************************}

#endregion "Copyright 2000-2012 DebtPlus, L.L.C."

namespace DebtPlus.UI.TableAdministration.CS.Templates
{
    internal class EditTemplateForm : DebtPlus.Data.Forms.DebtPlusForm
    {
        /// <summary>
        ///     Data Row View
        /// </summary>
        internal EditTemplateForm()
            : base()
        {
            InitializeComponent();
            RegisterHandlers();
        }

        private void RegisterHandlers()
        {
            simpleButton_OK.Click += simpleButton_OK_Click;
            simpleButton_Cancel.Click += simpleButton_Cancel_Click;
        }

        private void UnRegisterHandlers()
        {
            simpleButton_OK.Click -= simpleButton_OK_Click;
            simpleButton_Cancel.Click -= simpleButton_Cancel_Click;
        }

        #region " WinDows Form Designer generated code "

        //Form overrides dispose to clean up the component list.
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                if (components != null)
                {
                    components.Dispose();
                }
            }
            base.Dispose(disposing);
        }

        //Required by the WinDows Form Designer

        private System.ComponentModel.IContainer components = null;

        //NOTE: The following procedure is required by the WinDows Form Designer
        //It can be modified using the WinDows Form Designer.
        //Do not modify it using the code editor.
        protected DevExpress.XtraEditors.SimpleButton simpleButton_OK;

        protected DevExpress.XtraEditors.SimpleButton simpleButton_Cancel;

        [System.Diagnostics.DebuggerStepThrough()]
        private void InitializeComponent()
        {
            this.simpleButton_OK = new DevExpress.XtraEditors.SimpleButton();
            this.simpleButton_Cancel = new DevExpress.XtraEditors.SimpleButton();
            ((System.ComponentModel.ISupportInitialize)this.DxErrorProvider1).BeginInit();
            this.SuspendLayout();
            //
            //simpleButton_OK
            //
            this.simpleButton_OK.Anchor = (System.Windows.Forms.AnchorStyles)(System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right);
            this.simpleButton_OK.Enabled = false;
            this.simpleButton_OK.Location = new System.Drawing.Point(472, 43);
            this.simpleButton_OK.Name = "simpleButton_OK";
            this.simpleButton_OK.Size = new System.Drawing.Size(72, 26);
            this.simpleButton_OK.TabIndex = 18;
            this.simpleButton_OK.Text = "&OK";
            this.simpleButton_OK.ToolTip = "Click here to commit the changes to the database";
            this.simpleButton_OK.ToolTipController = this.ToolTipController1;
            //
            //simpleButton_Cancel
            //
            this.simpleButton_Cancel.Anchor = (System.Windows.Forms.AnchorStyles)(System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right);
            this.simpleButton_Cancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.simpleButton_Cancel.Location = new System.Drawing.Point(472, 86);
            this.simpleButton_Cancel.Name = "simpleButton_Cancel";
            this.simpleButton_Cancel.Size = new System.Drawing.Size(72, 26);
            this.simpleButton_Cancel.TabIndex = 19;
            this.simpleButton_Cancel.Text = "&Cancel";
            this.simpleButton_Cancel.ToolTip = "Click here to cancel the dialog and return to the previous form.";
            this.simpleButton_Cancel.ToolTipController = this.ToolTipController1;
            //
            //EditTemplateForm
            //
            this.AcceptButton = this.simpleButton_OK;
            this.AutoScaleBaseSize = new System.Drawing.Size(5, 14);
            this.CancelButton = this.simpleButton_Cancel;
            this.ClientSize = new System.Drawing.Size(568, 316);
            this.Controls.Add(this.simpleButton_Cancel);
            this.Controls.Add(this.simpleButton_OK);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.MaximizeBox = false;
            this.Name = "EditTemplateForm";
            this.Text = "Edit Item";
            ((System.ComponentModel.ISupportInitialize)this.DxErrorProvider1).EndInit();
            this.ResumeLayout(false);
        }

        #endregion " WinDows Form Designer generated code "

        /// <summary>
        /// Handle the click of the OK button. This routine would normally generate the OK dialog result
        /// but it may be overridden to do anything else.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        /// <remarks></remarks>
        protected virtual void simpleButton_OK_Click(System.Object sender, System.EventArgs e)
        {
            DialogResult = System.Windows.Forms.DialogResult.OK;
        }

        /// <summary>
        /// Handle the click of the CANCEL button. This routine would normally generate a CANCEL event
        /// but it may be overridden as needed to do anything else on the CANCEL operation.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        /// <remarks></remarks>
        protected virtual void simpleButton_Cancel_Click(System.Object sender, System.EventArgs e)
        {
            DialogResult = System.Windows.Forms.DialogResult.Cancel;
        }
    }
}