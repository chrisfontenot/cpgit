﻿using System;
using System.Linq;
using System.Windows.Forms;
using DebtPlus.LINQ;
using DebtPlus.UI.Common;

namespace DebtPlus.UI.TableAdministration.CS.FICOScoreReasonCodes
{
    public partial class MainForm : Templates.MainForm
    {
        // The record collection being edited
        private System.Collections.Generic.List<FICOScoreReasonCode> colRecords;

        private BusinessContext bc = new BusinessContext();

        public MainForm()
        {
            InitializeComponent();
            RegisterHandlers();
        }

        private void RegisterHandlers()
        {
            this.Load += MainForm_Load;
        }

        private void UnRegisterHandlers()
        {
            this.Load -= MainForm_Load;
        }

        private void MainForm_Load(object sender, EventArgs e)
        {
            UnRegisterHandlers();
            try
            {
                using (var cm = new CursorManager())
                {
                    colRecords = bc.FICOScoreReasonCodes.ToList();
                    gridControl1.DataSource = colRecords;
                    gridView1.BestFitColumns();
                }
            }
            finally
            {
                RegisterHandlers();
            }
        }

        /// <summary>
        /// Update the record in the database
        /// </summary>
        /// <param name="obj"></param>
        protected override void UpdateRecord(object obj)
        {
            FICOScoreReasonCode record = obj as FICOScoreReasonCode;
            if (record == null)
            {
                return;
            }

            // Edit the record to obtain the new values.
            bool priorDefault = record.Default;
            using (var frm = new EditForm(record))
            {
                if (frm.ShowDialog() != System.Windows.Forms.DialogResult.OK)
                {
                    return;
                }
            }

            // If the record is now the new default item then remove the previous one.
            if (!priorDefault && record.Default)
            {
                foreach (var rec in colRecords.FindAll(s => s.Default))
                {
                    rec.Default = false;
                }
                record.Default = true;
            }

            try
            {
                // Correct the database to the local table.
                bc.SubmitChanges();
                gridView1.RefreshData();
                return;
            }
            catch (System.Data.SqlClient.SqlException ex)
            {
                DebtPlus.UI.Common.ErrorHandling.HandleErrors(ex, "Error updating database");
            }
        }

        /// <summary>
        /// Create a new record for the database
        /// </summary>
        protected override void CreateRecord()
        {
            var record = DebtPlus.LINQ.Factory.Manufacture_FICOScoreReasonCode();
            using (var frm = new EditForm(record))
            {
                if (frm.ShowDialog() != DialogResult.OK)
                {
                    return;
                }
            }

            // If the record is the new "default" then clear the others.
            if (record.Default)
            {
                foreach (var rec in colRecords.FindAll(s => s.Default))
                {
                    rec.Default = false;
                }
            }

            // Insert the new record.
            bc.FICOScoreReasonCodes.InsertOnSubmit(record);
            try
            {
                bc.SubmitChanges();
                colRecords.Add(record);
                gridView1.RefreshData();
                return;
            }
            catch (System.Data.SqlClient.SqlException ex)
            {
                DebtPlus.UI.Common.ErrorHandling.HandleErrors(ex, "Error inserting new record into database");
            }
        }

        /// <summary>
        /// Delete the current record from the database
        /// </summary>
        /// <param name="obj"></param>
        protected override void DeleteRecord(object obj)
        {
            var record = obj as FICOScoreReasonCode;
            if (record == null)
            {
                return;
            }

            if (DebtPlus.Data.Prompts.RequestConfirmation_Delete() != System.Windows.Forms.DialogResult.Yes)
            {
                return;
            }

            // Delete the record from the database
            try
            {
                bc.FICOScoreReasonCodes.DeleteOnSubmit(record);
                bc.SubmitChanges();

                colRecords.Remove(record);
                gridView1.RefreshData();
                return;
            }
            catch (System.Data.SqlClient.SqlException ex)
            {
                DebtPlus.UI.Common.ErrorHandling.HandleErrors(ex, "Error deleting record from database");
            }
        }
    }
}