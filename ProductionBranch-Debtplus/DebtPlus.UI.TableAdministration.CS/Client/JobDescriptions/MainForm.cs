﻿using System;
using System.Linq;
using DebtPlus.LINQ;
using DebtPlus.UI.Common;

namespace DebtPlus.UI.TableAdministration.CS.Client.JobDescriptions
{
    public partial class MainForm : Templates.MainForm
    {
        // The record collection being edited
        private System.Collections.Generic.List<job_description> colRecords;

        public MainForm()
        {
            InitializeComponent();
            RegisterHandlers();
        }

        private void RegisterHandlers()
        {
            this.Load += MainForm_Load;
        }

        private void UnRegisterHandlers()
        {
            this.Load -= MainForm_Load;
        }

        private void MainForm_Load(object sender, EventArgs e)
        {
            UnRegisterHandlers();
            try
            {
                using (var cm = new CursorManager())
                {
                    using (var dc = new BusinessContext())
                    {
                        colRecords = dc.job_descriptions.ToList();
                        gridControl1.DataSource = colRecords;
                        gridView1.BestFitColumns();
                    }
                }
            }
            finally
            {
                RegisterHandlers();
            }
        }

        /// <summary>
        /// Update the record in the database
        /// </summary>
        /// <param name="obj"></param>
        protected override void UpdateRecord(object obj)
        {
            job_description record = obj as job_description;
            if (record != null)
            {
                using (var dc = new BusinessContext())
                {
                    // Find the record in the database
                    var qRecord = (from r in dc.job_descriptions where r.Id == record.Id select r).FirstOrDefault();
                    if (qRecord != null)
                    {
                        // Update the record contents and rewrite the record.
                        qRecord.description = record.description;
                        qRecord.nfcc = record.nfcc;

                        dc.SubmitChanges();

                        // Refresh the display list with the update
                        gridView1.RefreshData();
                    }
                }
            }
        }

        /// <summary>
        /// Create a new record for the database
        /// </summary>
        protected override void CreateRecord()
        {
            job_description record = new job_description();
            using (var frm = new EditForm(record))
            {
                if (frm.ShowDialog() != System.Windows.Forms.DialogResult.OK)
                {
                    return;
                }
            }

            using (var dc = new BusinessContext())
            {
                // Insert the new record in the database
                dc.job_descriptions.InsertOnSubmit(record);
                dc.SubmitChanges();

                // Add the new record to the collection and update the display
                colRecords.Add(record);
                gridView1.RefreshData();
            }
        }

        /// <summary>
        /// Delete the current record from the database
        /// </summary>
        /// <param name="obj"></param>
        protected override void DeleteRecord(object obj)
        {
            job_description record = obj as job_description;
            if (record != null)
            {
                if (DebtPlus.Data.Prompts.RequestConfirmation_Delete() != System.Windows.Forms.DialogResult.Yes)
                {
                    return;
                }
                using (var dc = new BusinessContext())
                {
                    // Find the record in the database
                    var qRecord = (from r in dc.job_descriptions where r.Id == record.Id select r).FirstOrDefault();
                    if (qRecord != null)
                    {
                        dc.job_descriptions.DeleteOnSubmit(qRecord);
                        dc.SubmitChanges();
                        colRecords.Remove(record);
                        gridView1.RefreshData();
                    }
                }
            }
        }
    }
}