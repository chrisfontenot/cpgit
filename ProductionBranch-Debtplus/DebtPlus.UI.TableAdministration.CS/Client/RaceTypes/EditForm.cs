﻿using System;
using DebtPlus.LINQ;

namespace DebtPlus.UI.TableAdministration.CS.Client.RaceTypes
{
    internal partial class EditForm : Templates.EditTemplateForm
    {
        private RaceType record;

        internal EditForm()
            : base()
        {
            InitializeComponent();
        }

        internal EditForm(RaceType record)
            : this()
        {
            this.record = record;
            RegisterHandlers();
        }

        private void RegisterHandlers()
        {
            this.Load += EditForm_Load;
            TextEdit_description.EditValueChanging += Form_Changing;
        }

        private void UnRegisterHandlers()
        {
            this.Load -= EditForm_Load;
            TextEdit_description.EditValueChanging -= Form_Changing;
        }

        private void EditForm_Load(object sender, EventArgs e)
        {
            UnRegisterHandlers();
            try
            {
                LabelControl_ID.Text = record.Id <= 0 ? "NEW" : record.Id.ToString();

                TextEdit_description.EditValue = record.description;
                CheckEdit_default.Checked = record.Default;
                CheckEdit_ActiveFlag.Checked = record.ActiveFlag;
                TextEdit_hud_9902_section.EditValue = record.hud_9902_section;
            }
            finally
            {
                RegisterHandlers();
            }
            simpleButton_OK.Enabled = !string.IsNullOrEmpty((string)TextEdit_description.EditValue);
        }

        private void Form_Changing(object sender, DevExpress.XtraEditors.Controls.ChangingEventArgs e)
        {
            simpleButton_OK.Enabled = !string.IsNullOrEmpty((string)e.NewValue);
        }

        protected override void simpleButton_OK_Click(object sender, EventArgs e)
        {
            record.description = DebtPlus.Utils.Nulls.v_String(TextEdit_description.EditValue);
            record.hud_9902_section = DebtPlus.Utils.Nulls.v_String(TextEdit_hud_9902_section.EditValue);
            record.Default = CheckEdit_default.Checked;
            record.ActiveFlag = CheckEdit_ActiveFlag.Checked;

            base.simpleButton_OK_Click(sender, e);
        }
    }
}