﻿using System;
using System.Linq;
using DebtPlus.LINQ;
using DebtPlus.UI.Common;

namespace DebtPlus.UI.TableAdministration.CS.Client.RaceTypes
{
    public partial class MainForm : Templates.MainForm
    {
        // The record collection being edited
        private System.Collections.Generic.List<RaceType> colRaceTypes;

        public MainForm()
        {
            InitializeComponent();
            RegisterHandlers();
        }

        private void RegisterHandlers()
        {
            this.Load += MainForm_Load;
        }

        private void UnRegisterHandlers()
        {
            this.Load -= MainForm_Load;
        }

        private void MainForm_Load(object sender, EventArgs e)
        {
            UnRegisterHandlers();
            try
            {
                using (var cm = new CursorManager())
                {
                    using (var dc = new BusinessContext())
                    {
                        colRaceTypes = dc.RaceTypes.ToList();
                        gridControl1.DataSource = colRaceTypes;
                        gridView1.BestFitColumns();
                    }
                }
            }
            finally
            {
                RegisterHandlers();
            }
        }

        /// <summary>
        /// Update the record in the database
        /// </summary>
        /// <param name="obj"></param>
        protected override void UpdateRecord(object obj)
        {
            RaceType record = obj as RaceType;
            if (record != null)
            {
                bool priorDefault = record.Default;
                using (var frm = new EditForm(record))
                {
                    if (frm.ShowDialog() != System.Windows.Forms.DialogResult.OK)
                    {
                        return;
                    }
                }

                using (var dc = new BusinessContext())
                {
                    // Remove the previous default settings
                    if (record.Default && !priorDefault)
                    {
                        var qDefault = (from r in dc.RaceTypes where r.Default != false select r);
                        foreach (var i in qDefault)
                        {
                            i.Default = false;
                        }
                    }

                    // Find the record in the database
                    var qRecord = (from r in dc.RaceTypes where r.Id == record.Id select r).FirstOrDefault();
                    if (qRecord != null)
                    {
                        // Update the record contents and rewrite the record.
                        qRecord.ActiveFlag = record.ActiveFlag;
                        qRecord.Default = record.Default;
                        qRecord.description = record.description;
                        qRecord.hud_9902_section = record.hud_9902_section;

                        dc.SubmitChanges();

                        // Remove it from the display collection
                        if (record.Default && !priorDefault)
                        {
                            foreach (var qItem in (from r in colRaceTypes where r.Default != false && r != record select r))
                            {
                                qItem.Default = false;
                            }
                        }

                        // Refresh the display list with the update
                        gridView1.RefreshData();
                    }
                }
            }
        }

        /// <summary>
        /// Create a new record for the database
        /// </summary>
        protected override void CreateRecord()
        {
            RaceType record = new RaceType();
            using (var frm = new EditForm(record))
            {
                if (frm.ShowDialog() != System.Windows.Forms.DialogResult.OK)
                {
                    return;
                }
            }

            using (var dc = new BusinessContext())
            {
                // Remove the previous default settings
                if (record.Default)
                {
                    var qDefault = (from r in dc.RaceTypes where r.Default != false select r);
                    foreach (var i in qDefault)
                    {
                        i.Default = false;
                    }
                }

                // Insert the new record in the database
                dc.RaceTypes.InsertOnSubmit(record);
                dc.SubmitChanges();

                // Update the display collection with the default settings
                if (record.Default)
                {
                    foreach (var qItem in (from r in colRaceTypes where r.Default != false select r))
                    {
                        qItem.Default = false;
                    }
                }

                // Add the new record to the collection and update the display
                colRaceTypes.Add(record);
                gridView1.RefreshData();
            }
        }

        /// <summary>
        /// Delete the current record from the database
        /// </summary>
        /// <param name="obj"></param>
        protected override void DeleteRecord(object obj)
        {
            RaceType record = obj as RaceType;
            if (record != null)
            {
                if (DebtPlus.Data.Prompts.RequestConfirmation_Delete() != System.Windows.Forms.DialogResult.Yes)
                {
                    return;
                }
                using (var dc = new BusinessContext())
                {
                    // Find the record in the database
                    var qRecord = (from r in dc.RaceTypes where r.Id == record.Id select r).FirstOrDefault();
                    if (qRecord != null)
                    {
                        dc.RaceTypes.DeleteOnSubmit(qRecord);
                        dc.SubmitChanges();
                        colRaceTypes.Remove(record);
                        gridView1.RefreshData();
                    }
                }
            }
        }
    }
}