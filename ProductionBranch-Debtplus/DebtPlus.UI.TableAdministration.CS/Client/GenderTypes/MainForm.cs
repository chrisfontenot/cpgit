﻿using System;
using System.Linq;
using DebtPlus.LINQ;
using DebtPlus.UI.Common;

namespace DebtPlus.UI.TableAdministration.CS.Client.GenderTypes
{
    public partial class MainForm : Templates.MainForm
    {
        // The record collection being edited
        private System.Collections.Generic.List<GenderType> colRecords;

        public MainForm()
        {
            InitializeComponent();
            RegisterHandlers();
        }

        private void RegisterHandlers()
        {
            this.Load += MainForm_Load;
        }

        private void UnRegisterHandlers()
        {
            this.Load -= MainForm_Load;
        }

        private void MainForm_Load(object sender, EventArgs e)
        {
            UnRegisterHandlers();
            try
            {
                using (var cm = new CursorManager())
                {
                    using (var dc = new BusinessContext())
                    {
                        colRecords = dc.GenderTypes.ToList();
                        gridControl1.DataSource = colRecords;
                        gridView1.BestFitColumns();
                    }
                }
            }
            finally
            {
                RegisterHandlers();
            }
        }

        /// <summary>
        /// Update the record in the database
        /// </summary>
        /// <param name="obj"></param>
        protected override void UpdateRecord(object obj)
        {
            GenderType record = obj as GenderType;
            if (record != null)
            {
                bool priorDefault = record.Default;
                using (var frm = new EditForm(record))
                {
                    if (frm.ShowDialog() != System.Windows.Forms.DialogResult.OK)
                    {
                        return;
                    }
                }

                using (var dc = new BusinessContext())
                {
                    // Clear the default status on the existing records
                    if (record.Default)
                    {
                        ResetDefaults(dc);
                        record.Default = true;
                    }

                    // Find the record in the database
                    var qRecord = (from r in dc.GenderTypes where r.Id == record.Id select r).FirstOrDefault();
                    if (qRecord != null)
                    {
                        // Update the record contents and rewrite the record.
                        qRecord.ActiveFlag = record.ActiveFlag;
                        qRecord.Default = record.Default;
                        qRecord.description = record.description;

                        dc.SubmitChanges();

                        // Refresh the display list with the update
                        gridView1.RefreshData();
                    }
                }
            }
        }

        /// <summary>
        /// Create a new record for the database
        /// </summary>
        protected override void CreateRecord()
        {
            GenderType record = new GenderType();
            using (var frm = new EditForm(record))
            {
                if (frm.ShowDialog() != System.Windows.Forms.DialogResult.OK)
                {
                    return;
                }
            }

            using (var dc = new BusinessContext())
            {
                // Clear the default status on the existing records
                if (record.Default)
                {
                    ResetDefaults(dc);
                    record.Default = true;
                }

                // Insert the new record in the database
                dc.GenderTypes.InsertOnSubmit(record);
                dc.SubmitChanges();

                // Add the new record to the collection and update the display
                colRecords.Add(record);
                gridView1.RefreshData();
            }
        }

        /// <summary>
        /// Delete the current record from the database
        /// </summary>
        /// <param name="obj"></param>
        protected override void DeleteRecord(object obj)
        {
            GenderType record = obj as GenderType;
            if (record != null)
            {
                if (DebtPlus.Data.Prompts.RequestConfirmation_Delete() != System.Windows.Forms.DialogResult.Yes)
                {
                    return;
                }
                using (var dc = new BusinessContext())
                {
                    // Find the record in the database
                    var qRecord = (from r in dc.GenderTypes where r.Id == record.Id select r).FirstOrDefault();
                    if (qRecord != null)
                    {
                        dc.GenderTypes.DeleteOnSubmit(qRecord);
                        dc.SubmitChanges();
                        colRecords.Remove(record);
                        gridView1.RefreshData();
                    }
                }
            }
        }

        /// <summary>
        /// Clear the Default status on the records when the new record is marked Default.
        /// </summary>
        /// <param name="bc">Pointer to the business class layer</param>
        private void ResetDefaults(BusinessContext drm)
        {
            // Turn off any record set as Default.
            foreach (GenderType qDef in (from r in drm.GenderTypes where r.Default select r))
            {
                qDef.Default = false;
            }

            foreach (GenderType qDef in colRecords)
            {
                qDef.Default = false;
            }
        }
    }
}