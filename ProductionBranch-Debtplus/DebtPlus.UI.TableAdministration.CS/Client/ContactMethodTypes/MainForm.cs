﻿using System;
using System.Linq;
using DebtPlus.LINQ;
using DebtPlus.UI.Common;

namespace DebtPlus.UI.TableAdministration.CS.Client.ContactMethodTypes
{
    public partial class MainForm : Templates.MainForm
    {
        // The record collection being edited
        private System.Collections.Generic.List<ContactMethodType> colRecords;

        private BusinessContext bc = null;

        public MainForm()
        {
            InitializeComponent();
            RegisterHandlers();
        }

        private void RegisterHandlers()
        {
            this.Load += MainForm_Load;
        }

        private void UnRegisterHandlers()
        {
            this.Load -= MainForm_Load;
        }

        private void MainForm_Load(object sender, EventArgs e)
        {
            bc = new BusinessContext();
            UnRegisterHandlers();
            try
            {
                using (var cm = new CursorManager())
                {
                    colRecords = bc.ContactMethodTypes.ToList();
                    gridControl1.DataSource = colRecords;
                    gridView1.BestFitColumns();
                }
            }
            finally
            {
                RegisterHandlers();
            }
        }

        /// <summary>
        /// Update the record in the database
        /// </summary>
        /// <param name="obj"></param>
        protected override void UpdateRecord(object obj)
        {
            ContactMethodType record = obj as ContactMethodType;
            if (record == null)
            {
                return;
            }

            bool priorDefault = record.Default;
            using (var frm = new EditForm(record))
            {
                if (frm.ShowDialog() != System.Windows.Forms.DialogResult.OK)
                {
                    return;
                }
            }

            // Clear the default status on the existing records
            if (record.Default && !priorDefault)
            {
                foreach (var defaultRecord in colRecords.Where(s => s.Default))
                {
                    defaultRecord.Default = false;
                }
                record.Default = true;
            }

            try
            {
                bc.SubmitChanges();
                gridView1.RefreshData();
            }
            catch (System.Data.SqlClient.SqlException ex)
            {
                DebtPlus.UI.Common.ErrorHandling.HandleErrors(ex);
            }
        }

        /// <summary>
        /// Create a new record for the database
        /// </summary>
        protected override void CreateRecord()
        {
            ContactMethodType record = DebtPlus.LINQ.Factory.Manufacture_ContactMethodType();

            using (var frm = new EditForm(record))
            {
                if (frm.ShowDialog() != System.Windows.Forms.DialogResult.OK)
                {
                    return;
                }
            }

            // Clear the default status on the existing records
            if (record.Default)
            {
                foreach (var defaultRecord in colRecords.Where(s => s.Default))
                {
                    defaultRecord.Default = false;
                }
                record.Default = true;
            }

            try
            {
                // Insert the new record in the database
                bc.ContactMethodTypes.InsertOnSubmit(record);
                bc.SubmitChanges();

                // Add the new record to the collection and update the display
                colRecords.Add(record);
                gridView1.RefreshData();
            }
            catch (System.Data.SqlClient.SqlException ex)
            {
                DebtPlus.UI.Common.ErrorHandling.HandleErrors(ex);
            }
        }

        /// <summary>
        /// Delete the current record from the database
        /// </summary>
        /// <param name="obj"></param>
        protected override void DeleteRecord(object obj)
        {
            var record = obj as ContactMethodType;
            if (record == null)
            {
                return;
            }

            if (DebtPlus.Data.Prompts.RequestConfirmation_Delete() != System.Windows.Forms.DialogResult.Yes)
            {
                return;
            }

            try
            {
                bc.ContactMethodTypes.DeleteOnSubmit(record);
                bc.SubmitChanges();
                colRecords.Remove(record);
                gridView1.RefreshData();
            }
            catch (System.Data.SqlClient.SqlException ex)
            {
                DebtPlus.UI.Common.ErrorHandling.HandleErrors(ex);
            }
        }
    }
}