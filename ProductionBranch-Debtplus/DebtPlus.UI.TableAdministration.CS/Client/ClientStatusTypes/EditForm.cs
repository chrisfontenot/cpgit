﻿using System;
using DebtPlus.LINQ;

namespace DebtPlus.UI.TableAdministration.CS.Client.StatusTypes
{
    internal partial class EditForm : Templates.EditTemplateForm
    {
        private ClientStatusType record;

        internal EditForm()
            : base()
        {
            InitializeComponent();
        }

        internal EditForm(ClientStatusType record)
            : this()
        {
            this.record = record;
            RegisterHandlers();
        }

        private void RegisterHandlers()
        {
            this.Load += EditForm_Load;
            TextEdit_description.EditValueChanging += Form_Changing;
            TextEdit_Id.EditValueChanging += Form_Changing;
        }

        private void UnRegisterHandlers()
        {
            this.Load -= EditForm_Load;
            TextEdit_description.EditValueChanging -= Form_Changing;
            TextEdit_Id.EditValueChanging -= Form_Changing;
        }

        private void EditForm_Load(object sender, EventArgs e)
        {
            UnRegisterHandlers();
            try
            {
                TextEdit_Id.EditValue = record.Id;
                checkEdit_ActiveFlag.EditValue = record.ActiveFlag;
                checkEdit_Default.EditValue = record.Default;
                TextEdit_description.EditValue = record.description;
            }
            finally
            {
                RegisterHandlers();
            }
            simpleButton_OK.Enabled = !string.IsNullOrEmpty((string)TextEdit_description.EditValue) && !string.IsNullOrEmpty((string)TextEdit_Id.EditValue);
        }

        private void Form_Changing(object sender, DevExpress.XtraEditors.Controls.ChangingEventArgs e)
        {
            simpleButton_OK.Enabled = !string.IsNullOrEmpty((string)TextEdit_description.EditValue) && !string.IsNullOrEmpty((string)TextEdit_Id.EditValue);
        }

        protected override void simpleButton_OK_Click(object sender, EventArgs e)
        {
            record.description = DebtPlus.Utils.Nulls.v_String(TextEdit_description.EditValue);
            record.Id = Convert.ToString(TextEdit_Id.EditValue);
            record.ActiveFlag = Convert.ToBoolean(checkEdit_ActiveFlag.EditValue);
            record.Default = Convert.ToBoolean(checkEdit_Default.EditValue);

            base.simpleButton_OK_Click(sender, e);
        }
    }
}