﻿using System;
using System.Linq;
using DebtPlus.LINQ;
using DebtPlus.UI.Common;

namespace DebtPlus.UI.TableAdministration.CS.Client.StatusTypes
{
    public partial class MainForm : Templates.MainForm
    {
        // The record collection being edited
        private System.Collections.Generic.List<ClientStatusType> colRecords;

        private BusinessContext bc = new BusinessContext();

        public MainForm()
        {
            InitializeComponent();
            RegisterHandlers();
        }

        private void RegisterHandlers()
        {
            this.Load += MainForm_Load;
        }

        private void UnRegisterHandlers()
        {
            this.Load -= MainForm_Load;
        }

        private void MainForm_Load(object sender, EventArgs e)
        {
            UnRegisterHandlers();
            try
            {
                using (var cm = new CursorManager())
                {
                    colRecords = bc.ClientStatusTypes.ToList();
                    gridControl1.DataSource = colRecords;
                    gridView1.BestFitColumns();
                }
            }
            catch (System.Data.SqlClient.SqlException ex)
            {
                DebtPlus.UI.Common.ErrorHandling.HandleErrors(ex);
            }
            finally
            {
                RegisterHandlers();
            }
        }

        /// <summary>
        /// Update the record in the database
        /// </summary>
        /// <param name="obj"></param>
        protected override void UpdateRecord(object obj)
        {
            var record = obj as ClientStatusType;
            if (record == null)
            {
                return;
            }

            string previousStatus = record.Id;
            bool priorDefault = record.Default;

            using (var frm = new EditForm(record))
            {
                if (frm.ShowDialog() != System.Windows.Forms.DialogResult.OK)
                {
                    return;
                }
            }

            // If the default status is now set then remove the previous default item
            if (record.Default && !priorDefault)
            {
                foreach (var defaultRecord in colRecords.Where(s => s.Default))
                {
                    defaultRecord.Default = false;
                }
                record.Default = true;
            }

            // Delete and replace the client status with the new value if it was changed
            if (string.Compare(previousStatus, record.Id, true) != 0)
            {
                // Insert the new record into the database. There is no "update" if the primary key changes.
                ClientStatusType newStatus = new ClientStatusType();
                newStatus.ActiveFlag = record.ActiveFlag;
                newStatus.Default = record.Default;
                newStatus.description = record.description;
                newStatus.Id = record.Id;
                newStatus.ActiveFlag = record.ActiveFlag;
                newStatus.Default = record.Default;

                // Delete the previous status record from the database.
                var q = bc.ClientStatusTypes.Where(s => s.Id == previousStatus).FirstOrDefault();
                if (q != null)
                {
                    bc.ClientStatusTypes.DeleteOnSubmit(q);
                }

                bc.ClientStatusTypes.InsertOnSubmit(newStatus);
            }

            try
            {
                // Refresh the display list with the update
                bc.SubmitChanges();
                gridView1.RefreshData();
            }
            catch (System.Data.SqlClient.SqlException ex)
            {
                DebtPlus.UI.Common.ErrorHandling.HandleErrors(ex);
            }
        }

        /// <summary>
        /// Create a new record for the database
        /// </summary>
        protected override void CreateRecord()
        {
            ClientStatusType record = new ClientStatusType();
            using (var frm = new EditForm(record))
            {
                if (frm.ShowDialog() != System.Windows.Forms.DialogResult.OK)
                {
                    return;
                }
            }

            // Clear the previous default status if needed
            if (record.Default)
            {
                foreach (var defaultRecord in colRecords.Where(s => s.Default))
                {
                    defaultRecord.Default = false;
                }
                record.Default = true;
            }

            try
            {
                // Insert the new record in the database
                bc.ClientStatusTypes.InsertOnSubmit(record);
                bc.SubmitChanges();

                // Add the new record to the collection and update the display
                colRecords.Add(record);
                gridView1.RefreshData();
            }
            catch (System.Data.SqlClient.SqlException ex)
            {
                DebtPlus.UI.Common.ErrorHandling.HandleErrors(ex);
            }
        }

        /// <summary>
        /// Delete the current record from the database
        /// </summary>
        /// <param name="obj"></param>
        protected override void DeleteRecord(object obj)
        {
            ClientStatusType record = obj as ClientStatusType;
            if (record == null)
            {
                return;
            }

            if (DebtPlus.Data.Prompts.RequestConfirmation_Delete() != System.Windows.Forms.DialogResult.Yes)
            {
                return;
            }

            try
            {
                bc.ClientStatusTypes.DeleteOnSubmit(record);
                bc.SubmitChanges();

                colRecords.Remove(record);
                gridView1.RefreshData();
            }
            catch (System.Data.SqlClient.SqlException ex)
            {
                DebtPlus.UI.Common.ErrorHandling.HandleErrors(ex);
            }
        }
    }
}