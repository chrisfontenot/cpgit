﻿using System;
using System.Linq;
using DebtPlus.LINQ;

namespace DebtPlus.UI.TableAdministration.CS.creditor_contact_types
{
    public partial class MainForm : Templates.MainForm
    {
        /// <summary>
        /// List of the records to be edited in the program
        /// </summary>
        private System.Collections.Generic.List<creditor_contact_type> colRecords;
        private BusinessContext bc = null;

        /// <summary>
        /// Initialize the new class
        /// </summary>
        /// <param name="dc">Pointer to the data context in the mainline</param>
        public MainForm()
            : base()
        {
            InitializeComponent();
            RegisterHandlers();
        }

        private void RegisterHandlers()
        {
            Load += MainForm_Load;
        }

        private void UnRegisterHandlers()
        {
            Load -= MainForm_Load;
        }

        /// <summary>
        /// Process the load event on the form
        /// </summary>
        private void MainForm_Load(object sender, System.EventArgs e)
        {
            bc = new BusinessContext();
            using (var cm = new DebtPlus.UI.Common.CursorManager())
            {
                try
                {
                    // Retrieve the list of menu items
                    colRecords = bc.creditor_contact_types.ToList();
                    gridControl1.DataSource = colRecords;
                }
                catch (Exception ex)
                {
                    DebtPlus.UI.Common.ErrorHandling.HandleErrors(ex, "Error retrieving database information");
                }
            }
        }

        /// <summary>
        /// Create the new record
        /// </summary>
        protected override void CreateRecord()
        {
            // Allocate the new record to be inserted.
            var record = DebtPlus.LINQ.Factory.Manufacture_creditor_contact_type();

            // Edit the new record. If OK then attempt to insert the record.
            using (EditForm frm = new EditForm(record))
            {
                if (frm.ShowDialog() != System.Windows.Forms.DialogResult.OK)
                {
                    return;
                }
            }

            // Turn off the default status of the previous record.
            if (record.Default)
            {
                foreach (var q in colRecords.Where(s => s.Default))
                {
                    q.Default = false;
                }
            }

            try
            {
                bc.creditor_contact_types.InsertOnSubmit(record);
                bc.SubmitChanges();

                colRecords.Add(record);
                gridView1.RefreshData();
            }
            catch (System.Data.SqlClient.SqlException ex)
            {
                DebtPlus.UI.Common.ErrorHandling.HandleErrors(ex);
            }
        }

        /// <summary>
        /// Update the existing record
        /// </summary>
        /// <param name="obj">Pointer to the desired record for the operation</param>
        protected override void UpdateRecord(object obj)
        {
            // Find the record
            DebtPlus.LINQ.creditor_contact_type record = obj as creditor_contact_type;
            if (obj == null)
            {
                return;
            }

            bool priorDefault = record.Default;
            using (var frm = new EditForm(record))
            {
                if (frm.ShowDialog() != System.Windows.Forms.DialogResult.OK)
                {
                    return;
                }
            }

            // Turn off the default status of the previous record.
            if (record.Default && !priorDefault)
            {
                foreach(var q in colRecords.Where(s => s.Default))
                {
                    q.Default = false;
                }
                record.Default = true;
            }

            try
            {
                bc.SubmitChanges();
                gridView1.RefreshData();
            }
            catch(System.Data.SqlClient.SqlException ex)
            {
                DebtPlus.UI.Common.ErrorHandling.HandleErrors(ex, "Error updating database");
            }
        }

        /// <summary>
        /// Delete the indicated record from the database
        /// </summary>
        /// <param name="obj">Pointer to the desired record for the operation</param>
        protected override void DeleteRecord(object obj)
        {
            // Find the record
            DebtPlus.LINQ.creditor_contact_type record = obj as creditor_contact_type;
            if (obj == null)
            {
                return;
            }

            if (DebtPlus.Data.Prompts.RequestConfirmation_Delete() != System.Windows.Forms.DialogResult.Yes)
            {
                return;
            }

            bc.creditor_contact_types.DeleteOnSubmit(record);
            try
            {
                bc.SubmitChanges();
                colRecords.Remove(record);
                gridView1.RefreshData();
            }
            catch(System.Data.SqlClient.SqlException ex)
            {
                DebtPlus.UI.Common.ErrorHandling.HandleErrors(ex, "Error updating database");
            }
        }
    }
}