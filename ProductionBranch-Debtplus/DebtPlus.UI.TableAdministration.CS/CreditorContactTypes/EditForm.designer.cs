﻿namespace DebtPlus.UI.TableAdministration.CS.creditor_contact_types
{
    partial class EditForm
    {
		//Form overrides dispose to clean up the component list.
		[System.Diagnostics.DebuggerNonUserCode]
		protected override void Dispose(bool disposing)
		{
			try 
            {
				if (disposing)
                {
                    if (components != null) components.Dispose();
				}
                components = null;
            }
    	    finally
            {
				base.Dispose(disposing);
			}
		}

		//Required by the Windows Form Designer
		private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.LabelControl5 = new DevExpress.XtraEditors.LabelControl();
            this.TextEdit_contact_type = new DevExpress.XtraEditors.TextEdit();
            this.LabelControl_nfcc = new DevExpress.XtraEditors.LabelControl();
            this.TextEdit_name = new DevExpress.XtraEditors.TextEdit();
            this.TextEdit_description = new DevExpress.XtraEditors.TextEdit();
            this.LabelControl_description = new DevExpress.XtraEditors.LabelControl();
            this.LabelControl_creditor_contact_type = new DevExpress.XtraEditors.LabelControl();
            this.LabelControl4 = new DevExpress.XtraEditors.LabelControl();
            this.checkEdit_Default = new DevExpress.XtraEditors.CheckEdit();
            this.checkEdit_ActiveFlag = new DevExpress.XtraEditors.CheckEdit();
            ((System.ComponentModel.ISupportInitialize)(this.DxErrorProvider1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TextEdit_contact_type.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TextEdit_name.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TextEdit_description.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit_Default.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit_ActiveFlag.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // simpleButton_Cancel
            // 
            this.simpleButton_Cancel.Location = new System.Drawing.Point(317, 67);
            this.simpleButton_Cancel.TabIndex = 11;
            // 
            // simpleButton_OK
            // 
            this.simpleButton_OK.Location = new System.Drawing.Point(317, 35);
            this.simpleButton_OK.TabIndex = 10;
            // 
            // DefaultLookAndFeel1
            // 
            this.DefaultLookAndFeel1.LookAndFeel.SkinName = "McSkin";
            // 
            // LabelControl5
            // 
            this.LabelControl5.Location = new System.Drawing.Point(6, 82);
            this.LabelControl5.Name = "LabelControl5";
            this.LabelControl5.Size = new System.Drawing.Size(65, 13);
            this.LabelControl5.TabIndex = 6;
            this.LabelControl5.Text = "Contact Type";
            // 
            // TextEdit_contact_type
            // 
            this.TextEdit_contact_type.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.TextEdit_contact_type.Location = new System.Drawing.Point(79, 79);
            this.TextEdit_contact_type.Name = "TextEdit_contact_type";
            this.TextEdit_contact_type.Properties.MaxLength = 10;
            this.TextEdit_contact_type.Size = new System.Drawing.Size(109, 20);
            this.TextEdit_contact_type.TabIndex = 7;
            // 
            // LabelControl_nfcc
            // 
            this.LabelControl_nfcc.Location = new System.Drawing.Point(6, 56);
            this.LabelControl_nfcc.Name = "LabelControl_nfcc";
            this.LabelControl_nfcc.Size = new System.Drawing.Size(27, 13);
            this.LabelControl_nfcc.TabIndex = 4;
            this.LabelControl_nfcc.Text = "Name";
            // 
            // TextEdit_name
            // 
            this.TextEdit_name.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.TextEdit_name.Location = new System.Drawing.Point(79, 53);
            this.TextEdit_name.Name = "TextEdit_name";
            this.TextEdit_name.Properties.MaxLength = 50;
            this.TextEdit_name.Size = new System.Drawing.Size(219, 20);
            this.TextEdit_name.TabIndex = 5;
            // 
            // TextEdit_description
            // 
            this.TextEdit_description.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.TextEdit_description.Location = new System.Drawing.Point(79, 27);
            this.TextEdit_description.Name = "TextEdit_description";
            this.TextEdit_description.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.False;
            this.TextEdit_description.Properties.MaxLength = 50;
            this.TextEdit_description.Size = new System.Drawing.Size(219, 20);
            this.TextEdit_description.TabIndex = 3;
            // 
            // LabelControl_description
            // 
            this.LabelControl_description.Location = new System.Drawing.Point(6, 30);
            this.LabelControl_description.Name = "LabelControl_description";
            this.LabelControl_description.Size = new System.Drawing.Size(53, 13);
            this.LabelControl_description.TabIndex = 2;
            this.LabelControl_description.Text = "Description";
            // 
            // LabelControl_creditor_contact_type
            // 
            this.LabelControl_creditor_contact_type.Location = new System.Drawing.Point(79, 8);
            this.LabelControl_creditor_contact_type.Name = "LabelControl_creditor_contact_type";
            this.LabelControl_creditor_contact_type.Size = new System.Drawing.Size(23, 13);
            this.LabelControl_creditor_contact_type.TabIndex = 1;
            this.LabelControl_creditor_contact_type.Text = "NEW";
            // 
            // LabelControl4
            // 
            this.LabelControl4.Location = new System.Drawing.Point(6, 8);
            this.LabelControl4.Name = "LabelControl4";
            this.LabelControl4.Size = new System.Drawing.Size(11, 13);
            this.LabelControl4.TabIndex = 0;
            this.LabelControl4.Text = "ID";
            // 
            // checkEdit_Default
            // 
            this.checkEdit_Default.Location = new System.Drawing.Point(6, 105);
            this.checkEdit_Default.Name = "checkEdit_Default";
            this.checkEdit_Default.Properties.Caption = "Default";
            this.checkEdit_Default.Size = new System.Drawing.Size(75, 20);
            this.checkEdit_Default.TabIndex = 8;
            // 
            // checkEdit_ActiveFlag
            // 
            this.checkEdit_ActiveFlag.Location = new System.Drawing.Point(121, 105);
            this.checkEdit_ActiveFlag.Name = "checkEdit_ActiveFlag";
            this.checkEdit_ActiveFlag.Properties.Caption = "Active";
            this.checkEdit_ActiveFlag.Size = new System.Drawing.Size(75, 20);
            this.checkEdit_ActiveFlag.TabIndex = 9;
            // 
            // EditForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(401, 138);
            this.Controls.Add(this.checkEdit_ActiveFlag);
            this.Controls.Add(this.checkEdit_Default);
            this.Controls.Add(this.LabelControl5);
            this.Controls.Add(this.TextEdit_contact_type);
            this.Controls.Add(this.LabelControl_nfcc);
            this.Controls.Add(this.TextEdit_name);
            this.Controls.Add(this.TextEdit_description);
            this.Controls.Add(this.LabelControl_description);
            this.Controls.Add(this.LabelControl_creditor_contact_type);
            this.Controls.Add(this.LabelControl4);
            this.Name = "EditForm";
            this.Text = "Creditor Contact Type";
            this.Controls.SetChildIndex(this.simpleButton_OK, 0);
            this.Controls.SetChildIndex(this.simpleButton_Cancel, 0);
            this.Controls.SetChildIndex(this.LabelControl4, 0);
            this.Controls.SetChildIndex(this.LabelControl_creditor_contact_type, 0);
            this.Controls.SetChildIndex(this.LabelControl_description, 0);
            this.Controls.SetChildIndex(this.TextEdit_description, 0);
            this.Controls.SetChildIndex(this.TextEdit_name, 0);
            this.Controls.SetChildIndex(this.LabelControl_nfcc, 0);
            this.Controls.SetChildIndex(this.TextEdit_contact_type, 0);
            this.Controls.SetChildIndex(this.LabelControl5, 0);
            this.Controls.SetChildIndex(this.checkEdit_Default, 0);
            this.Controls.SetChildIndex(this.checkEdit_ActiveFlag, 0);
            ((System.ComponentModel.ISupportInitialize)(this.DxErrorProvider1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TextEdit_contact_type.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TextEdit_name.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TextEdit_description.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit_Default.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit_ActiveFlag.Properties)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }


        private DevExpress.XtraEditors.LabelControl LabelControl5;
        private DevExpress.XtraEditors.TextEdit TextEdit_contact_type;
        private DevExpress.XtraEditors.LabelControl LabelControl_nfcc;
        private DevExpress.XtraEditors.TextEdit TextEdit_name;
        private DevExpress.XtraEditors.TextEdit TextEdit_description;
        private DevExpress.XtraEditors.LabelControl LabelControl_description;
        private DevExpress.XtraEditors.LabelControl LabelControl_creditor_contact_type;
        private DevExpress.XtraEditors.LabelControl LabelControl4;
        private DevExpress.XtraEditors.CheckEdit checkEdit_Default;
        private DevExpress.XtraEditors.CheckEdit checkEdit_ActiveFlag;

    }
}