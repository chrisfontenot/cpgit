﻿using System;
using DebtPlus.LINQ;

namespace DebtPlus.UI.TableAdministration.CS.creditor_contact_types
{
    internal partial class EditForm : Templates.EditTemplateForm
    {
        // Current record being edited
        private creditor_contact_type record;

        /// <summary>
        /// Initialize the new class instance
        /// </summary>
        internal EditForm()
            : base()
        {
            InitializeComponent();
        }

        /// <summary>
        /// Initialize the new class instance
        /// </summary>
        /// <param name="record"></param>
        internal EditForm(creditor_contact_type record)
            : this()
        {
            this.record = record;
            RegisterHandlers();
        }

        /// <summary>
        /// Add the event handlers to the collection
        /// </summary>
        private void RegisterHandlers()
        {
            Load += EditForm_Load;
            TextEdit_description.EditValueChanged += FormChanged;
        }

        /// <summary>
        /// Remove the event handlers from being processed
        /// </summary>
        private void UnRegisterHandlers()
        {
            Load -= EditForm_Load;
            TextEdit_description.EditValueChanged -= FormChanged;
        }

        /// <summary>
        /// Process the FORM LOAD event
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void EditForm_Load(object sender, System.EventArgs e)
        {
            UnRegisterHandlers();
            try
            {
                LabelControl_creditor_contact_type.Text = record.Id > 0 ? record.Id.ToString() : "NEW";
                checkEdit_ActiveFlag.Checked            = record.ActiveFlag;
                checkEdit_Default.Checked               = record.Default;
                TextEdit_name.EditValue                 = record.name;
                TextEdit_description.EditValue          = record.description;
                TextEdit_contact_type.EditValue         = record.contact_type;

                simpleButton_OK.Enabled                 = !HasErrors();
            }
            finally
            {
                RegisterHandlers();
            }
        }

        /// <summary>
        /// Override the OK button to save the editing values into the record
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected override void simpleButton_OK_Click(object sender, EventArgs e)
        {
            base.simpleButton_OK_Click(sender, e);

            record.ActiveFlag   = checkEdit_ActiveFlag.Checked;
            record.Default      = checkEdit_Default.Checked;
            record.description  = DebtPlus.Utils.Nulls.DStr(TextEdit_description.EditValue);
            record.contact_type = DebtPlus.Utils.Nulls.v_String(TextEdit_contact_type.EditValue);
            record.name         = DebtPlus.Utils.Nulls.v_String(TextEdit_name.EditValue);
        }

        private void FormChanged(object sender, EventArgs e)
        {
            simpleButton_OK.Enabled = !HasErrors();
        }

        /// <summary>
        /// Determine if the input form has error(s)
        /// </summary>
        /// <returns></returns>
        private bool HasErrors()
        {
            if (string.IsNullOrWhiteSpace((string)TextEdit_description.EditValue))
            {
                return true;
            }
            return false;
        }
    }
}