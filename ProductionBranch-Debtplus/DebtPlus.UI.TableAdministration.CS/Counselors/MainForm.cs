// To enable database logging, un-comment the following
#define DATABASE_LOGGING

#region "Copyright 2000-2012 DebtPlus, L.L.C."
//{*******************************************************************}
//{                                                                   }
//{       DebtPlus Debt Management System                             }
//{                                                                   }
//{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
//{       ALL RIGHTS RESERVED                                         }
//{                                                                   }
//{   The entire contents of this file is protected by U.S. and       }
//{   International Copyright Laws. Unauthorized reproduction,        }
//{   reverse-engineering, and distribution of all or any portion of  }
//{   the code contained in this file is strictly prohibited and may  }
//{   result in severe civil and criminal penalties and will be       }
//{   prosecuted to the maximum extent possible under the law.        }
//{                                                                   }
//{   RESTRICTIONS                                                    }
//{                                                                   }
//{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
//{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
//{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
//{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
//{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
//{                                                                   }
//{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
//{   ADDITIONAL RESTRICTIONS.                                        }
//{                                                                   }
//{*******************************************************************}

#endregion "Copyright 2000-2012 DebtPlus, L.L.C."

using System;
using System.Linq;
using System.Windows.Forms;
using DebtPlus.LINQ;

namespace DebtPlus.UI.TableAdministration.CS.Counselors
{
    public partial class MainForm : Templates.MainForm
    {
        /// <summary>
        /// List of the records to be edited in the program
        /// </summary>
        private System.Collections.Generic.List<counselor> colRecords;

        private BusinessContext bc = null;

        /// <summary>
        /// Initialize the new class
        /// </summary>
        public MainForm()
            : base()
        {
            InitializeComponent();
            RegisterHandlers();

            // Allocate a database connection. Set the timeout to 0 to wait as long as it needs to take.
            bc = new BusinessContext();
            bc.CommandTimeout = 0;
        }

        private void RegisterHandlers()
        {
            Load += MainForm_Load;
            gridView1.CustomColumnDisplayText += new DevExpress.XtraGrid.Views.Base.CustomColumnDisplayTextEventHandler(gridView1_CustomColumnDisplayText);
            checkEdit_ShowInactive.CheckStateChanged += checkEdit_ShowInactive_CheckStateChanged;
        }

        private void UnRegisterHandlers()
        {
            Load -= MainForm_Load;
            gridView1.CustomColumnDisplayText -= new DevExpress.XtraGrid.Views.Base.CustomColumnDisplayTextEventHandler(gridView1_CustomColumnDisplayText);
            checkEdit_ShowInactive.CheckStateChanged -= checkEdit_ShowInactive_CheckStateChanged;
        }

        /// <summary>
        /// Process the load event on the form
        /// </summary>
        private void MainForm_Load(object sender, System.EventArgs e)
        {
            // Restore the form size and location
            LoadPlacement("CounselorUpdateMain");

            try
            {
                using (var cm = new DebtPlus.UI.Common.CursorManager())
                {
                    // Be greedy. Load the attributes list for the counselors now.
                    var dl = new System.Data.Linq.DataLoadOptions();
                    dl.LoadWith<DebtPlus.LINQ.counselor>(s => s.counselor_attributes);
                    bc.LoadOptions = dl;

                    // Change the filtering to match the switch status
                    checkEdit_ShowInactive.Checked = GetDefaultShowInactive();
                    ResetFilter();

                    // Retrieve the list of menu items
                    colRecords = bc.counselors.ToList();
                    gridControl1.DataSource = colRecords;
                }
            }
            catch (Exception ex)
            {
                DebtPlus.UI.Common.ErrorHandling.HandleErrors(ex);
            }
        }

        /// <summary>
        /// Handle the change in the checked status to limit the display list to active items only.
        /// </summary>
        private void checkEdit_ShowInactive_CheckStateChanged(object sender, EventArgs e)
        {
            UnRegisterHandlers();
            try
            {
                SetDefaultShowInactive(checkEdit_ShowInactive.Checked);
                ResetFilter();
            }
            finally
            {
                RegisterHandlers();
            }
        }

        /// <summary>
        /// Set the proper filtering criteria for the list.
        /// </summary>
        private void ResetFilter()
        {
            if (!checkEdit_ShowInactive.Checked)
            {
                gridView1.ActiveFilterString = "[ActiveFlag]";
            }
            else
            {
                gridView1.ActiveFilterString = string.Empty;
            }
        }

        /// <summary>
        /// Get the default setting for the "Show Inactive Clients" setting from the user's registry.
        /// </summary>
        private bool GetDefaultShowInactive()
        {
            string keyLocation = "Show Inactive Clients";
            bool value = false;
            string KeyField = System.IO.Path.Combine(DebtPlus.Configuration.AssemblyInfo.InstallationRegistryKey, "Options");

            // Start with the system setting.
            using (Microsoft.Win32.RegistryKey key = Microsoft.Win32.Registry.LocalMachine.OpenSubKey(KeyField, false))
            {
                if (key != null)
                {
                    string stringValue = DebtPlus.Utils.Nulls.DStr(key.GetValue(keyLocation, "True"));
                    bool Answer;
                    if (Boolean.TryParse(stringValue, out Answer))
                    {
                        value = Answer;
                    }
                }
            }

            // Look for the local user to override the system setting
            using (Microsoft.Win32.RegistryKey key = Microsoft.Win32.Registry.CurrentUser.OpenSubKey(KeyField, false))
            {
                if (key != null)
                {
                    bool Answer;
                    string stringValue = DebtPlus.Utils.Nulls.DStr(key.GetValue(keyLocation, value.ToString()));
                    if (Boolean.TryParse(stringValue, out Answer))
                    {
                        value = Answer;
                    }
                }
            }

            return value;
        }

        /// <summary>
        /// Save the status of the checkbox to the registry for the next item
        /// </summary>
        private void SetDefaultShowInactive(bool value)
        {
            string keyLocation = "Show Inactive Clients";
            try
            {
                string KeyField = System.IO.Path.Combine(DebtPlus.Configuration.AssemblyInfo.InstallationRegistryKey, "Options");
                using (Microsoft.Win32.RegistryKey key = Microsoft.Win32.Registry.CurrentUser.OpenSubKey(KeyField, true))
                {
                    key.SetValue(keyLocation, value.ToString());
                }
            }
            catch { }
        }

        /// <summary>
        /// Create the new record
        /// </summary>
        protected override void CreateRecord()
        {
            // Allocate the new record to be inserted.
            var record = DebtPlus.LINQ.Factory.Manufacture_counselor();

            // Edit the new record. If OK then attempt to insert the record.
            using (var frm = new CounselorEditForm(bc, record))
            {
                if (frm.ShowDialog() != System.Windows.Forms.DialogResult.OK)
                {
                    return;
                }

                // If this is the new default then clear the previous one.
                if (record.Default)
                {
                    foreach (var defaultRecord in colRecords.Where(s => s.Default))
                    {
                        defaultRecord.Default = false;
                    }
                    record.Default = true;
                }

                try
                {
                    // Add the record to the collection
                    bc.counselors.InsertOnSubmit(record);
                    bc.SubmitChanges();
                    colRecords.Add(record);
                    gridView1.RefreshData();
                }
                catch (System.Data.SqlClient.SqlException ex)
                {
                    DebtPlus.UI.Common.ErrorHandling.HandleErrors(ex);
                }
            }
        }

        /// <summary>
        /// Update the existing record
        /// </summary>
        /// <param name="obj">Pointer to the desired record for the operation</param>
        protected override void UpdateRecord(object obj)
        {
            // Find the record
            var record = obj as counselor;
            if (obj == null)
            {
                return;
            }

            // Remember the previous value for the default status. We will need it after the update.
            bool priorDefault = record.Default;

            // Update the record now.
            using (var frm = new CounselorEditForm(bc, record))
            {
                if (frm.ShowDialog() != System.Windows.Forms.DialogResult.OK)
                {
                    return;
                }
            }

            try
            {
                // Turn off the default status of all other records in the collection.
                if (record.Default && !priorDefault)
                {
                    foreach (var defaultRecord in colRecords.Where(s => s.Default))
                    {
                        defaultRecord.Default = false;
                    }

                    // Reset the default status back to TRUE for the current record.
                    record.Default = true;
                }

                bc.SubmitChanges();
                gridView1.RefreshData();
            }
            catch (System.Data.SqlClient.SqlException ex)
            {
                DebtPlus.UI.Common.ErrorHandling.HandleErrors(ex);
            }
        }

        /// <summary>
        /// Delete the indicated record from the database
        /// </summary>
        /// <param name="obj">Pointer to the desired record for the operation</param>
        protected override void DeleteRecord(object obj)
        {
            // Find the record
            var record = obj as counselor;
            if (obj == null)
            {
                return;
            }

            if (DebtPlus.Data.Prompts.RequestConfirmation("The delete operation can take quite a while.\r\nAre you sure that you want to do this?") != System.Windows.Forms.DialogResult.Yes)
            {
                return;
            }

            try
            {
                bc.counselors.DeleteOnSubmit(record);
                bc.SubmitChanges();

                colRecords.Remove(record);
                gridView1.RefreshData();
            }
            catch (System.Data.SqlClient.SqlException ex)
            {
                DebtPlus.UI.Common.ErrorHandling.HandleErrors(ex);
            }
        }

        /// <summary>
        /// Handle the customization of the grid control to display status for the attributes
        /// </summary>
        private void gridView1_CustomColumnDisplayText(object sender, DevExpress.XtraGrid.Views.Base.CustomColumnDisplayTextEventArgs e)
        {
            // Find the record that is being formatted
            var record = gridView1.GetRow(e.RowHandle) as counselor;
            if (record == null)
            {
                return;
            }

            // Look for the items that have a valid tag field.
            if (e.Column.Tag is Int32)
            {
                Int32 intKey = (Int32)e.Column.Tag;

                // Find the attribute for this type in the counselor attribute list
                var q = record.counselor_attributes.Where(s => s.Attribute == intKey).FirstOrDefault();
                e.DisplayText = (q != null) ? "Y" : "";
            }
        }
    }
}