
using System;
using System.Windows.Forms;
using System.Drawing;
using DebtPlus.LINQ;
using System.Linq;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Diagnostics;
namespace DebtPlus.UI.TableAdministration.CS.Counselors
{
	partial class TrainingEditForm
	{

		//Form overrides dispose to clean up the component list.
		[System.Diagnostics.DebuggerNonUserCode]
		protected override void Dispose(bool disposing)
		{
			if (disposing && components != null) 
{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		//Required by the Windows Form Designer

		private System.ComponentModel.IContainer components = null;
		//NOTE: The following procedure is required by the Windows Form Designer
		//It can be modified using the Windows Form Designer.  
		//Do not modify it using the code editor.
		[System.Diagnostics.DebuggerStepThrough]
		private void InitializeComponent()
		{
			this.LayoutControl1 = new DevExpress.XtraLayout.LayoutControl();
			this.DateEdit_TrainingDate = new DevExpress.XtraEditors.DateEdit();
			this.SimpleButtonCancel = new DevExpress.XtraEditors.SimpleButton();
			this.SimpleButtonOK = new DevExpress.XtraEditors.SimpleButton();
			this.LookupEdit_TrainingCourse = new DevExpress.XtraEditors.LookUpEdit();
			this.LayoutControlGroup1 = new DevExpress.XtraLayout.LayoutControlGroup();
			this.LayoutControlItem1 = new DevExpress.XtraLayout.LayoutControlItem();
			this.LayoutControlItem8 = new DevExpress.XtraLayout.LayoutControlItem();
			this.LayoutControlItem9 = new DevExpress.XtraLayout.LayoutControlItem();
			this.EmptySpaceItem1 = new DevExpress.XtraLayout.EmptySpaceItem();
			this.EmptySpaceItem2 = new DevExpress.XtraLayout.EmptySpaceItem();
			this.EmptySpaceItem3 = new DevExpress.XtraLayout.EmptySpaceItem();
			this.LayoutControlItem2 = new DevExpress.XtraLayout.LayoutControlItem();
			this.EmptySpaceItem4 = new DevExpress.XtraLayout.EmptySpaceItem();
			((System.ComponentModel.ISupportInitialize)this.DxErrorProvider1).BeginInit();
			((System.ComponentModel.ISupportInitialize)this.LayoutControl1).BeginInit();
			this.LayoutControl1.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)this.DateEdit_TrainingDate.Properties.VistaTimeProperties).BeginInit();
			((System.ComponentModel.ISupportInitialize)this.DateEdit_TrainingDate.Properties).BeginInit();
			((System.ComponentModel.ISupportInitialize)this.LookupEdit_TrainingCourse.Properties).BeginInit();
			((System.ComponentModel.ISupportInitialize)this.LayoutControlGroup1).BeginInit();
			((System.ComponentModel.ISupportInitialize)this.LayoutControlItem1).BeginInit();
			((System.ComponentModel.ISupportInitialize)this.LayoutControlItem8).BeginInit();
			((System.ComponentModel.ISupportInitialize)this.LayoutControlItem9).BeginInit();
			((System.ComponentModel.ISupportInitialize)this.EmptySpaceItem1).BeginInit();
			((System.ComponentModel.ISupportInitialize)this.EmptySpaceItem2).BeginInit();
			((System.ComponentModel.ISupportInitialize)this.EmptySpaceItem3).BeginInit();
			((System.ComponentModel.ISupportInitialize)this.LayoutControlItem2).BeginInit();
			((System.ComponentModel.ISupportInitialize)this.EmptySpaceItem4).BeginInit();
			this.SuspendLayout();
			//
			//LayoutControl1
			//
			this.LayoutControl1.Controls.Add(this.DateEdit_TrainingDate);
			this.LayoutControl1.Controls.Add(this.SimpleButtonCancel);
			this.LayoutControl1.Controls.Add(this.SimpleButtonOK);
			this.LayoutControl1.Controls.Add(this.LookupEdit_TrainingCourse);
			this.LayoutControl1.Dock = DockStyle.Fill;
			this.LayoutControl1.Location = new System.Drawing.Point(0, 0);
			this.LayoutControl1.Name = "LayoutControl1";
			this.LayoutControl1.Root = this.LayoutControlGroup1;
			this.LayoutControl1.Size = new System.Drawing.Size(329, 108);
			this.LayoutControl1.TabIndex = 0;
			this.LayoutControl1.Text = "LayoutControl1";
			//
			//DateEdit_TrainingDate
			//
			this.DateEdit_TrainingDate.EditValue = null;
			this.DateEdit_TrainingDate.Location = new System.Drawing.Point(71, 36);
			this.DateEdit_TrainingDate.Name = "DateEdit_TrainingDate";
			this.DateEdit_TrainingDate.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.False;
			this.DateEdit_TrainingDate.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] { new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo) });
			this.DateEdit_TrainingDate.Properties.VistaTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] { new DevExpress.XtraEditors.Controls.EditorButton() });
			this.DateEdit_TrainingDate.Size = new System.Drawing.Size(91, 20);
			this.DateEdit_TrainingDate.StyleController = this.LayoutControl1;
			this.DateEdit_TrainingDate.TabIndex = 14;
			//
			//SimpleButtonCancel
			//
			this.SimpleButtonCancel.DialogResult = DialogResult.Cancel;
			this.SimpleButtonCancel.Location = new System.Drawing.Point(166, 70);
			this.SimpleButtonCancel.MaximumSize = new System.Drawing.Size(72, 26);
			this.SimpleButtonCancel.MinimumSize = new System.Drawing.Size(72, 26);
			this.SimpleButtonCancel.Name = "SimpleButtonCancel";
			this.SimpleButtonCancel.Size = new System.Drawing.Size(72, 26);
			this.SimpleButtonCancel.StyleController = this.LayoutControl1;
			this.SimpleButtonCancel.TabIndex = 12;
			this.SimpleButtonCancel.Text = "&Cancel";
			//
			//SimpleButtonOK
			//
			this.SimpleButtonOK.DialogResult = DialogResult.OK;
			this.SimpleButtonOK.Location = new System.Drawing.Point(90, 70);
			this.SimpleButtonOK.MaximumSize = new System.Drawing.Size(72, 26);
			this.SimpleButtonOK.MinimumSize = new System.Drawing.Size(72, 26);
			this.SimpleButtonOK.Name = "SimpleButtonOK";
			this.SimpleButtonOK.Size = new System.Drawing.Size(72, 26);
			this.SimpleButtonOK.StyleController = this.LayoutControl1;
			this.SimpleButtonOK.TabIndex = 11;
			this.SimpleButtonOK.Text = "&OK";
			//
			//LookupEdit_TrainingCourse
			//
			this.LookupEdit_TrainingCourse.Location = new System.Drawing.Point(71, 12);
			this.LookupEdit_TrainingCourse.Name = "LookupEdit_TrainingCourse";
			this.LookupEdit_TrainingCourse.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.True;
			this.LookupEdit_TrainingCourse.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] { new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo) });
			this.LookupEdit_TrainingCourse.Properties.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
				new DevExpress.XtraEditors.Controls.LookUpColumnInfo("Id", "ID", 20, DevExpress.Utils.FormatType.Numeric, "f0", false, DevExpress.Utils.HorzAlignment.Default),
				new DevExpress.XtraEditors.Controls.LookUpColumnInfo("TrainingTitle", 50, "Title"),
				new DevExpress.XtraEditors.Controls.LookUpColumnInfo("TrainingDuration", "Duration", 20, DevExpress.Utils.FormatType.Numeric, "f0", true, DevExpress.Utils.HorzAlignment.Default),
				new DevExpress.XtraEditors.Controls.LookUpColumnInfo("Certificate", "Certificate", 20, DevExpress.Utils.FormatType.None, "", true, DevExpress.Utils.HorzAlignment.Default, DevExpress.Data.ColumnSortOrder.Ascending)
			});
			this.LookupEdit_TrainingCourse.Properties.DisplayMember = "TrainingTitle";
			this.LookupEdit_TrainingCourse.Properties.MaxLength = 50;
			this.LookupEdit_TrainingCourse.Properties.NullText = "";
			this.LookupEdit_TrainingCourse.Properties.ShowFooter = false;
			this.LookupEdit_TrainingCourse.Size = new System.Drawing.Size(246, 20);
			this.LookupEdit_TrainingCourse.StyleController = this.LayoutControl1;
			this.LookupEdit_TrainingCourse.TabIndex = 4;
			this.LookupEdit_TrainingCourse.ToolTip = "Title associated with the course";
			this.LookupEdit_TrainingCourse.Properties.SortColumnIndex = 1;
			//
			//LayoutControlGroup1
			//
			this.LayoutControlGroup1.CustomizationFormText = "LayoutControlGroup1";
			this.LayoutControlGroup1.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.True;
			this.LayoutControlGroup1.GroupBordersVisible = false;
			this.LayoutControlGroup1.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
				this.LayoutControlItem1,
				this.LayoutControlItem8,
				this.LayoutControlItem9,
				this.EmptySpaceItem1,
				this.EmptySpaceItem2,
				this.EmptySpaceItem3,
				this.LayoutControlItem2,
				this.EmptySpaceItem4
			});
			this.LayoutControlGroup1.Location = new System.Drawing.Point(0, 0);
			this.LayoutControlGroup1.Name = "LayoutControlGroup1";
			this.LayoutControlGroup1.Size = new System.Drawing.Size(329, 108);
			this.LayoutControlGroup1.Text = "LayoutControlGroup1";
			this.LayoutControlGroup1.TextVisible = false;
			//
			//LayoutControlItem1
			//
			this.LayoutControlItem1.Control = this.LookupEdit_TrainingCourse;
			this.LayoutControlItem1.CustomizationFormText = "Course";
			this.LayoutControlItem1.Location = new System.Drawing.Point(0, 0);
			this.LayoutControlItem1.Name = "LayoutControlItem1";
			this.LayoutControlItem1.Size = new System.Drawing.Size(309, 24);
			this.LayoutControlItem1.Text = "Course";
			this.LayoutControlItem1.TextSize = new System.Drawing.Size(55, 13);
			//
			//LayoutControlItem8
			//
			this.LayoutControlItem8.Control = this.SimpleButtonOK;
			this.LayoutControlItem8.CustomizationFormText = "OK Button";
			this.LayoutControlItem8.Location = new System.Drawing.Point(78, 58);
			this.LayoutControlItem8.Name = "LayoutControlItem8";
			this.LayoutControlItem8.Size = new System.Drawing.Size(76, 30);
			this.LayoutControlItem8.Text = "OK";
			this.LayoutControlItem8.TextSize = new System.Drawing.Size(0, 0);
			this.LayoutControlItem8.TextToControlDistance = 0;
			this.LayoutControlItem8.TextVisible = false;
			//
			//LayoutControlItem9
			//
			this.LayoutControlItem9.Control = this.SimpleButtonCancel;
			this.LayoutControlItem9.CustomizationFormText = "Cancel Button";
			this.LayoutControlItem9.Location = new System.Drawing.Point(154, 58);
			this.LayoutControlItem9.Name = "LayoutControlItem9";
			this.LayoutControlItem9.Size = new System.Drawing.Size(76, 30);
			this.LayoutControlItem9.Text = "Cancel";
			this.LayoutControlItem9.TextSize = new System.Drawing.Size(0, 0);
			this.LayoutControlItem9.TextToControlDistance = 0;
			this.LayoutControlItem9.TextVisible = false;
			//
			//EmptySpaceItem1
			//
			this.EmptySpaceItem1.AllowHotTrack = false;
			this.EmptySpaceItem1.CustomizationFormText = "EmptySpaceItem1";
			this.EmptySpaceItem1.Location = new System.Drawing.Point(230, 58);
			this.EmptySpaceItem1.Name = "EmptySpaceItem1";
			this.EmptySpaceItem1.Size = new System.Drawing.Size(79, 30);
			this.EmptySpaceItem1.Text = "EmptySpaceItem1";
			this.EmptySpaceItem1.TextSize = new System.Drawing.Size(0, 0);
			//
			//EmptySpaceItem2
			//
			this.EmptySpaceItem2.AllowHotTrack = false;
			this.EmptySpaceItem2.CustomizationFormText = "EmptySpaceItem2";
			this.EmptySpaceItem2.Location = new System.Drawing.Point(0, 58);
			this.EmptySpaceItem2.Name = "EmptySpaceItem2";
			this.EmptySpaceItem2.Size = new System.Drawing.Size(78, 30);
			this.EmptySpaceItem2.Text = "EmptySpaceItem2";
			this.EmptySpaceItem2.TextSize = new System.Drawing.Size(0, 0);
			//
			//EmptySpaceItem3
			//
			this.EmptySpaceItem3.AllowHotTrack = false;
			this.EmptySpaceItem3.CustomizationFormText = "EmptySpaceItem3";
			this.EmptySpaceItem3.Location = new System.Drawing.Point(0, 48);
			this.EmptySpaceItem3.Name = "EmptySpaceItem3";
			this.EmptySpaceItem3.Size = new System.Drawing.Size(309, 10);
			this.EmptySpaceItem3.Text = "EmptySpaceItem3";
			this.EmptySpaceItem3.TextSize = new System.Drawing.Size(0, 0);
			//
			//LayoutControlItem2
			//
			this.LayoutControlItem2.Control = this.DateEdit_TrainingDate;
			this.LayoutControlItem2.CustomizationFormText = "Date Taken";
			this.LayoutControlItem2.Location = new System.Drawing.Point(0, 24);
			this.LayoutControlItem2.Name = "LayoutControlItem2";
			this.LayoutControlItem2.Size = new System.Drawing.Size(154, 24);
			this.LayoutControlItem2.Text = "Date Taken";
			this.LayoutControlItem2.TextSize = new System.Drawing.Size(55, 13);
			//
			//EmptySpaceItem4
			//
			this.EmptySpaceItem4.AllowHotTrack = false;
			this.EmptySpaceItem4.CustomizationFormText = "EmptySpaceItem4";
			this.EmptySpaceItem4.Location = new System.Drawing.Point(154, 24);
			this.EmptySpaceItem4.Name = "EmptySpaceItem4";
			this.EmptySpaceItem4.Size = new System.Drawing.Size(155, 24);
			this.EmptySpaceItem4.Text = "EmptySpaceItem4";
			this.EmptySpaceItem4.TextSize = new System.Drawing.Size(0, 0);
			//
			//TrainingEditForm
			//
			this.AcceptButton = this.SimpleButtonOK;
			this.AutoScaleDimensions = new System.Drawing.SizeF(6f, 13f);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.CancelButton = this.SimpleButtonCancel;
			this.ClientSize = new System.Drawing.Size(329, 108);
			this.Controls.Add(this.LayoutControl1);
			this.Name = "TrainingEditForm";
			this.Text = "Counselor Training";
			((System.ComponentModel.ISupportInitialize)this.DxErrorProvider1).EndInit();
			((System.ComponentModel.ISupportInitialize)this.LayoutControl1).EndInit();
			this.LayoutControl1.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)this.DateEdit_TrainingDate.Properties.VistaTimeProperties).EndInit();
			((System.ComponentModel.ISupportInitialize)this.DateEdit_TrainingDate.Properties).EndInit();
			((System.ComponentModel.ISupportInitialize)this.LookupEdit_TrainingCourse.Properties).EndInit();
			((System.ComponentModel.ISupportInitialize)this.LayoutControlGroup1).EndInit();
			((System.ComponentModel.ISupportInitialize)this.LayoutControlItem1).EndInit();
			((System.ComponentModel.ISupportInitialize)this.LayoutControlItem8).EndInit();
			((System.ComponentModel.ISupportInitialize)this.LayoutControlItem9).EndInit();
			((System.ComponentModel.ISupportInitialize)this.EmptySpaceItem1).EndInit();
			((System.ComponentModel.ISupportInitialize)this.EmptySpaceItem2).EndInit();
			((System.ComponentModel.ISupportInitialize)this.EmptySpaceItem3).EndInit();
			((System.ComponentModel.ISupportInitialize)this.LayoutControlItem2).EndInit();
			((System.ComponentModel.ISupportInitialize)this.EmptySpaceItem4).EndInit();
			this.ResumeLayout(false);

		}
		private DevExpress.XtraLayout.LayoutControl LayoutControl1;
		private DevExpress.XtraLayout.LayoutControlGroup LayoutControlGroup1;
		private DevExpress.XtraLayout.LayoutControlItem LayoutControlItem1;
		private DevExpress.XtraEditors.SimpleButton SimpleButtonOK;
		private DevExpress.XtraLayout.LayoutControlItem LayoutControlItem8;
		private DevExpress.XtraEditors.SimpleButton SimpleButtonCancel;
		private DevExpress.XtraLayout.LayoutControlItem LayoutControlItem9;
		private DevExpress.XtraLayout.EmptySpaceItem EmptySpaceItem1;
		private DevExpress.XtraLayout.EmptySpaceItem EmptySpaceItem2;
		private DevExpress.XtraLayout.EmptySpaceItem EmptySpaceItem3;
		private DevExpress.XtraEditors.LookUpEdit LookupEdit_TrainingCourse;
		private DevExpress.XtraEditors.DateEdit DateEdit_TrainingDate;
		private DevExpress.XtraLayout.LayoutControlItem LayoutControlItem2;
		private DevExpress.XtraLayout.EmptySpaceItem EmptySpaceItem4;
	}
}
