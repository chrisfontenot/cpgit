
using System;
using System.Windows.Forms;
using System.Drawing;
using DebtPlus.LINQ;
using System.Linq;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Diagnostics;
using DebtPlus.UI.TableAdministration.CS.Counselors.Controls;
using DebtPlus.UI.Common.Templates;

namespace DebtPlus.UI.TableAdministration.CS.Counselors
{
	partial class CounselorEditForm
	{
		//Form overrides dispose to clean up the component list.
		[System.Diagnostics.DebuggerNonUserCode]
		protected override void Dispose(bool disposing)
		{
			try 
            {
				if (disposing && components != null) 
                {
					components.Dispose();
				}
            }
            finally 
            {
				base.Dispose(disposing);
			}
		}

		//Required by the Windows Form Designer
		private System.ComponentModel.IContainer components = null;

		//NOTE: The following procedure is required by the Windows Form Designer
		//It can be modified using the Windows Form Designer.  
		//Do not modify it using the code editor.
		[System.Diagnostics.DebuggerStepThrough]
		private void InitializeComponent()
		{
            DevExpress.Utils.SuperToolTip superToolTip1 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem1 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem1 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject1 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject2 = new DevExpress.Utils.SerializableAppearanceObject();
            this.LabelControl_ID = new DevExpress.XtraEditors.LabelControl();
            this.LayoutControl1 = new DevExpress.XtraLayout.LayoutControl();
            this.lookUpEdit_Manager = new DevExpress.XtraEditors.LookUpEdit();
            this.HousingAttributeListControl1 = new DebtPlus.UI.TableAdministration.CS.Counselors.Controls.HousingAttributeListControl();
            this.TextEditNote = new DevExpress.XtraEditors.TextEdit();
            this.TrainingListControl1 = new DebtPlus.UI.TableAdministration.CS.Counselors.Controls.TrainingListControl();
            this.LookUpEdit_billing_method = new DevExpress.XtraEditors.LookUpEdit();
            this.TextEdit_HUD_id = new DevExpress.XtraEditors.TextEdit();
            this.Ssn1 = new DebtPlus.Data.Controls.ssn();
            this.DateEdit_emp_start_date = new DevExpress.XtraEditors.DateEdit();
            this.CalcEdit_hourly_rate = new DevExpress.XtraEditors.CalcEdit();
            this.ImageEdit1 = new DevExpress.XtraEditors.ImageEdit();
            this.ColorEdit1 = new DevExpress.XtraEditors.ColorEdit();
            this.DateEdit_emp_end_date = new DevExpress.XtraEditors.DateEdit();
            this.CheckEdit_ActiveFlag = new DevExpress.XtraEditors.CheckEdit();
            this.CheckEdit_default = new DevExpress.XtraEditors.CheckEdit();
            this.EmailRecordControl1 = new DebtPlus.Data.Controls.EmailRecordControl();
            this.TelephoneNumberRecordControl1 = new DebtPlus.Data.Controls.TelephoneNumberRecordControl();
            this.NameRecordControl1 = new DebtPlus.Data.Controls.NameRecordControl();
            this.TextEdit_menu_level = new DevExpress.XtraEditors.TextEdit();
            this.TextEdit_person = new DevExpress.XtraEditors.TextEdit();
            this.LookUpEdit_office = new DevExpress.XtraEditors.LookUpEdit();
            this.CounselorAttributes1 = new DebtPlus.UI.TableAdministration.CS.Counselors.Controls.AttributeListControl();
            this.LayoutControlGroup1 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.LayoutControlItem1 = new DevExpress.XtraLayout.LayoutControlItem();
            this.LayoutControlItem4 = new DevExpress.XtraLayout.LayoutControlItem();
            this.TabbedControlGroup1 = new DevExpress.XtraLayout.TabbedControlGroup();
            this.LayoutControlGroup3 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.LayoutControlItem7 = new DevExpress.XtraLayout.LayoutControlItem();
            this.LayoutControlItem6 = new DevExpress.XtraLayout.LayoutControlItem();
            this.LayoutControlItem5 = new DevExpress.XtraLayout.LayoutControlItem();
            this.LayoutControlGroup4 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.LayoutControlItem3 = new DevExpress.XtraLayout.LayoutControlItem();
            this.LayoutControlItem2 = new DevExpress.XtraLayout.LayoutControlItem();
            this.LayoutControlItem9 = new DevExpress.XtraLayout.LayoutControlItem();
            this.LayoutControlItem10 = new DevExpress.XtraLayout.LayoutControlItem();
            this.EmptySpaceItem1 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.LayoutControlItem12 = new DevExpress.XtraLayout.LayoutControlItem();
            this.LayoutControlItem11 = new DevExpress.XtraLayout.LayoutControlItem();
            this.LayoutControlItem20 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem22 = new DevExpress.XtraLayout.LayoutControlItem();
            this.LayoutControlGroup6 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.LayoutControlItem8 = new DevExpress.XtraLayout.LayoutControlItem();
            this.LayoutControlGroup5 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.LayoutControlItem18 = new DevExpress.XtraLayout.LayoutControlItem();
            this.LayoutControlItem14 = new DevExpress.XtraLayout.LayoutControlItem();
            this.LayoutControlItem15 = new DevExpress.XtraLayout.LayoutControlItem();
            this.LayoutControlItem16 = new DevExpress.XtraLayout.LayoutControlItem();
            this.LayoutControlItem17 = new DevExpress.XtraLayout.LayoutControlItem();
            this.LayoutControlItem13 = new DevExpress.XtraLayout.LayoutControlItem();
            this.LayoutControlItem21 = new DevExpress.XtraLayout.LayoutControlItem();
            this.EmptySpaceItem2 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.LayoutControlGroup2 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.LayoutControlItem19 = new DevExpress.XtraLayout.LayoutControlItem();
            ((System.ComponentModel.ISupportInitialize)(this.DxErrorProvider1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LayoutControl1)).BeginInit();
            this.LayoutControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.lookUpEdit_Manager.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TextEditNote.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LookUpEdit_billing_method.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TextEdit_HUD_id.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Ssn1.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DateEdit_emp_start_date.Properties.VistaTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DateEdit_emp_start_date.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.CalcEdit_hourly_rate.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ImageEdit1.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ColorEdit1.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DateEdit_emp_end_date.Properties.VistaTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DateEdit_emp_end_date.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.CheckEdit_ActiveFlag.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.CheckEdit_default.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.EmailRecordControl1.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TelephoneNumberRecordControl1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TextEdit_menu_level.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TextEdit_person.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LookUpEdit_office.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LayoutControlGroup1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LayoutControlItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LayoutControlItem4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TabbedControlGroup1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LayoutControlGroup3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LayoutControlItem7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LayoutControlItem6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LayoutControlItem5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LayoutControlGroup4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LayoutControlItem3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LayoutControlItem2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LayoutControlItem9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LayoutControlItem10)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.EmptySpaceItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LayoutControlItem12)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LayoutControlItem11)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LayoutControlItem20)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem22)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LayoutControlGroup6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LayoutControlItem8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LayoutControlGroup5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LayoutControlItem18)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LayoutControlItem14)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LayoutControlItem15)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LayoutControlItem16)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LayoutControlItem17)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LayoutControlItem13)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LayoutControlItem21)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.EmptySpaceItem2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LayoutControlGroup2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LayoutControlItem19)).BeginInit();
            this.SuspendLayout();
            // 
            // simpleButton_OK
            // 
            this.simpleButton_OK.Location = new System.Drawing.Point(400, 13);
            this.simpleButton_OK.TabIndex = 17;
            // 
            // simpleButton_Cancel
            // 
            this.simpleButton_Cancel.Location = new System.Drawing.Point(400, 56);
            this.simpleButton_Cancel.TabIndex = 18;
            // 
            // DefaultLookAndFeel1
            // 
            this.DefaultLookAndFeel1.LookAndFeel.SkinName = "McSkin";
            // 
            // LabelControl_ID
            // 
            this.LabelControl_ID.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.LabelControl_ID.Location = new System.Drawing.Point(72, 12);
            this.LabelControl_ID.Name = "LabelControl_ID";
            this.LabelControl_ID.Size = new System.Drawing.Size(312, 13);
            this.LabelControl_ID.StyleController = this.LayoutControl1;
            this.LabelControl_ID.TabIndex = 1;
            this.LabelControl_ID.Text = "NEW";
            this.LabelControl_ID.UseMnemonic = false;
            // 
            // LayoutControl1
            // 
            this.LayoutControl1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.LayoutControl1.Controls.Add(this.lookUpEdit_Manager);
            this.LayoutControl1.Controls.Add(this.HousingAttributeListControl1);
            this.LayoutControl1.Controls.Add(this.TextEditNote);
            this.LayoutControl1.Controls.Add(this.TrainingListControl1);
            this.LayoutControl1.Controls.Add(this.LookUpEdit_billing_method);
            this.LayoutControl1.Controls.Add(this.TextEdit_HUD_id);
            this.LayoutControl1.Controls.Add(this.Ssn1);
            this.LayoutControl1.Controls.Add(this.DateEdit_emp_start_date);
            this.LayoutControl1.Controls.Add(this.CalcEdit_hourly_rate);
            this.LayoutControl1.Controls.Add(this.ImageEdit1);
            this.LayoutControl1.Controls.Add(this.ColorEdit1);
            this.LayoutControl1.Controls.Add(this.DateEdit_emp_end_date);
            this.LayoutControl1.Controls.Add(this.LabelControl_ID);
            this.LayoutControl1.Controls.Add(this.CheckEdit_ActiveFlag);
            this.LayoutControl1.Controls.Add(this.CheckEdit_default);
            this.LayoutControl1.Controls.Add(this.EmailRecordControl1);
            this.LayoutControl1.Controls.Add(this.TelephoneNumberRecordControl1);
            this.LayoutControl1.Controls.Add(this.NameRecordControl1);
            this.LayoutControl1.Controls.Add(this.TextEdit_menu_level);
            this.LayoutControl1.Controls.Add(this.TextEdit_person);
            this.LayoutControl1.Controls.Add(this.LookUpEdit_office);
            this.LayoutControl1.Controls.Add(this.CounselorAttributes1);
            this.LayoutControl1.Location = new System.Drawing.Point(0, 0);
            this.LayoutControl1.Name = "LayoutControl1";
            this.LayoutControl1.OptionsCustomizationForm.DesignTimeCustomizationFormPositionAndSize = new System.Drawing.Rectangle(628, 87, 250, 350);
            this.LayoutControl1.Root = this.LayoutControlGroup1;
            this.LayoutControl1.Size = new System.Drawing.Size(396, 280);
            this.LayoutControl1.TabIndex = 19;
            this.LayoutControl1.Text = "LayoutControl1";
            // 
            // lookUpEdit_Manager
            // 
            this.lookUpEdit_Manager.Location = new System.Drawing.Point(84, 111);
            this.lookUpEdit_Manager.Name = "lookUpEdit_Manager";
            this.lookUpEdit_Manager.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.lookUpEdit_Manager.Properties.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("Id", "Id", 20, DevExpress.Utils.FormatType.Numeric, "f0", false, DevExpress.Utils.HorzAlignment.Far, DevExpress.Data.ColumnSortOrder.None, DevExpress.Utils.DefaultBoolean.True),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("Name", "Name", 20, DevExpress.Utils.FormatType.None, "", true, DevExpress.Utils.HorzAlignment.Near, DevExpress.Data.ColumnSortOrder.Ascending, DevExpress.Utils.DefaultBoolean.True)});
            this.lookUpEdit_Manager.Properties.DisplayMember = "Name";
            this.lookUpEdit_Manager.Properties.NullText = "";
            this.lookUpEdit_Manager.Properties.ShowFooter = false;
            this.lookUpEdit_Manager.Properties.ShowHeader = false;
            this.lookUpEdit_Manager.Properties.SortColumnIndex = 1;
            this.lookUpEdit_Manager.Properties.ValueMember = "Id";
            this.lookUpEdit_Manager.Size = new System.Drawing.Size(288, 20);
            this.lookUpEdit_Manager.StyleController = this.LayoutControl1;
            this.lookUpEdit_Manager.TabIndex = 28;
            // 
            // HousingAttributeListControl1
            // 
            this.HousingAttributeListControl1.Location = new System.Drawing.Point(24, 176);
            this.HousingAttributeListControl1.Name = "HousingAttributeListControl1";
            this.HousingAttributeListControl1.Size = new System.Drawing.Size(348, 80);
            this.HousingAttributeListControl1.TabIndex = 27;
            // 
            // TextEditNote
            // 
            this.TextEditNote.Location = new System.Drawing.Point(84, 135);
            this.TextEditNote.Name = "TextEditNote";
            this.TextEditNote.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.True;
            this.TextEditNote.Properties.MaxLength = 256;
            this.TextEditNote.Size = new System.Drawing.Size(288, 20);
            this.TextEditNote.StyleController = this.LayoutControl1;
            this.TextEditNote.TabIndex = 26;
            this.TextEditNote.ToolTip = "If you wish a small note, add it here.";
            // 
            // TrainingListControl1
            // 
            this.TrainingListControl1.Location = new System.Drawing.Point(24, 87);
            this.TrainingListControl1.Name = "TrainingListControl1";
            this.TrainingListControl1.Size = new System.Drawing.Size(348, 169);
            this.TrainingListControl1.TabIndex = 25;
            // 
            // LookUpEdit_billing_method
            // 
            this.LookUpEdit_billing_method.Location = new System.Drawing.Point(84, 111);
            this.LookUpEdit_billing_method.Name = "LookUpEdit_billing_method";
            this.LookUpEdit_billing_method.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.False;
            this.LookUpEdit_billing_method.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LookUpEdit_billing_method.Properties.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("Id", "ID", 20, DevExpress.Utils.FormatType.None, "", false, DevExpress.Utils.HorzAlignment.Default),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("Description", "Description", 20, DevExpress.Utils.FormatType.None, "", true, DevExpress.Utils.HorzAlignment.Default, DevExpress.Data.ColumnSortOrder.Ascending)});
            this.LookUpEdit_billing_method.Properties.DisplayMember = "Description";
            this.LookUpEdit_billing_method.Properties.NullText = "";
            this.LookUpEdit_billing_method.Properties.NullValuePrompt = "Required";
            this.LookUpEdit_billing_method.Properties.NullValuePromptShowForEmptyValue = true;
            this.LookUpEdit_billing_method.Properties.ShowFooter = false;
            this.LookUpEdit_billing_method.Properties.ShowHeader = false;
            this.LookUpEdit_billing_method.Properties.SortColumnIndex = 1;
            this.LookUpEdit_billing_method.Properties.ValueMember = "Id";
            this.LookUpEdit_billing_method.Size = new System.Drawing.Size(71, 20);
            this.LookUpEdit_billing_method.StyleController = this.LayoutControl1;
            this.LookUpEdit_billing_method.TabIndex = 24;
            this.LookUpEdit_billing_method.ToolTip = "Billing Method is typically \"Fixed\" meaning there is one charge for all time. If " +
    "needed, it may also be \"hourly\" for an hourly rate (variable amount).";
            // 
            // TextEdit_HUD_id
            // 
            this.TextEdit_HUD_id.Location = new System.Drawing.Point(84, 87);
            this.TextEdit_HUD_id.Name = "TextEdit_HUD_id";
            this.TextEdit_HUD_id.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.True;
            this.TextEdit_HUD_id.Properties.DisplayFormat.FormatString = "f0";
            this.TextEdit_HUD_id.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.TextEdit_HUD_id.Properties.EditFormat.FormatString = "f0";
            this.TextEdit_HUD_id.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.TextEdit_HUD_id.Properties.Mask.BeepOnError = true;
            this.TextEdit_HUD_id.Properties.Mask.EditMask = "f0";
            this.TextEdit_HUD_id.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.TextEdit_HUD_id.Size = new System.Drawing.Size(71, 20);
            this.TextEdit_HUD_id.StyleController = this.LayoutControl1;
            toolTipTitleItem1.Text = "HUD FHA ID Number";
            toolTipItem1.LeftIndent = 6;
            toolTipItem1.Text = "Placeholder for future release where counselors register themselves through FHA C" +
    "onnection. For now, no need to provide this field.";
            superToolTip1.Items.Add(toolTipTitleItem1);
            superToolTip1.Items.Add(toolTipItem1);
            this.TextEdit_HUD_id.SuperTip = superToolTip1;
            this.TextEdit_HUD_id.TabIndex = 23;
            // 
            // Ssn1
            // 
            this.Ssn1.Location = new System.Drawing.Point(219, 87);
            this.Ssn1.Name = "Ssn1";
            this.Ssn1.Properties.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.Ssn1.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Custom;
            this.Ssn1.Properties.Mask.AutoComplete = DevExpress.XtraEditors.Mask.AutoCompleteType.Strong;
            this.Ssn1.Properties.Mask.BeepOnError = true;
            this.Ssn1.Properties.Mask.EditMask = "000-00-0000";
            this.Ssn1.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Simple;
            this.Ssn1.Properties.Mask.SaveLiteral = false;
            this.Ssn1.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.Ssn1.Size = new System.Drawing.Size(153, 20);
            this.Ssn1.StyleController = this.LayoutControl1;
            this.Ssn1.TabIndex = 22;
            this.Ssn1.ToolTip = "This is the counselor\'s social security number";
            // 
            // DateEdit_emp_start_date
            // 
            this.DateEdit_emp_start_date.EditValue = new System.DateTime(2015, 11, 12, 0, 0, 0, 0);
            this.DateEdit_emp_start_date.Location = new System.Drawing.Point(84, 135);
            this.DateEdit_emp_start_date.Name = "DateEdit_emp_start_date";
            this.DateEdit_emp_start_date.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.True;
            this.DateEdit_emp_start_date.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.DateEdit_emp_start_date.Properties.NullValuePrompt = "Date of hire";
            this.DateEdit_emp_start_date.Properties.NullValuePromptShowForEmptyValue = true;
            this.DateEdit_emp_start_date.Properties.VistaEditTime = DevExpress.Utils.DefaultBoolean.False;
            this.DateEdit_emp_start_date.Properties.VistaTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.DateEdit_emp_start_date.Size = new System.Drawing.Size(71, 20);
            this.DateEdit_emp_start_date.StyleController = this.LayoutControl1;
            this.DateEdit_emp_start_date.TabIndex = 20;
            this.DateEdit_emp_start_date.ToolTip = "Date the counselor started counseling clients";
            // 
            // CalcEdit_hourly_rate
            // 
            this.CalcEdit_hourly_rate.Location = new System.Drawing.Point(219, 111);
            this.CalcEdit_hourly_rate.Name = "CalcEdit_hourly_rate";
            this.CalcEdit_hourly_rate.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.True;
            this.CalcEdit_hourly_rate.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.CalcEdit_hourly_rate.Properties.DisplayFormat.FormatString = "c";
            this.CalcEdit_hourly_rate.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.CalcEdit_hourly_rate.Properties.EditFormat.FormatString = "c";
            this.CalcEdit_hourly_rate.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.CalcEdit_hourly_rate.Properties.Mask.BeepOnError = true;
            this.CalcEdit_hourly_rate.Properties.Mask.EditMask = "c";
            this.CalcEdit_hourly_rate.Properties.Precision = 2;
            this.CalcEdit_hourly_rate.Size = new System.Drawing.Size(153, 20);
            this.CalcEdit_hourly_rate.StyleController = this.LayoutControl1;
            this.CalcEdit_hourly_rate.TabIndex = 19;
            this.CalcEdit_hourly_rate.ToolTip = "Hourly rate charged by counselor for counseling clients";
            // 
            // ImageEdit1
            // 
            this.ImageEdit1.Location = new System.Drawing.Point(84, 183);
            this.ImageEdit1.Name = "ImageEdit1";
            this.ImageEdit1.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.ImageEdit1.Size = new System.Drawing.Size(288, 20);
            this.ImageEdit1.StyleController = this.LayoutControl1;
            this.ImageEdit1.TabIndex = 18;
            // 
            // ColorEdit1
            // 
            this.ColorEdit1.EditValue = System.Drawing.Color.Empty;
            this.ColorEdit1.Location = new System.Drawing.Point(219, 159);
            this.ColorEdit1.Name = "ColorEdit1";
            this.ColorEdit1.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.ColorEdit1.Properties.StoreColorAsInteger = true;
            this.ColorEdit1.Size = new System.Drawing.Size(153, 20);
            this.ColorEdit1.StyleController = this.LayoutControl1;
            this.ColorEdit1.TabIndex = 17;
            // 
            // DateEdit_emp_end_date
            // 
            this.DateEdit_emp_end_date.EditValue = new System.DateTime(2015, 11, 12, 0, 0, 0, 0);
            this.DateEdit_emp_end_date.Location = new System.Drawing.Point(219, 135);
            this.DateEdit_emp_end_date.Name = "DateEdit_emp_end_date";
            this.DateEdit_emp_end_date.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.True;
            this.DateEdit_emp_end_date.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.DateEdit_emp_end_date.Properties.MaxValue = new System.DateTime(2099, 12, 31, 0, 0, 0, 0);
            this.DateEdit_emp_end_date.Properties.MinValue = new System.DateTime(1900, 1, 1, 0, 0, 0, 0);
            this.DateEdit_emp_end_date.Properties.NullValuePrompt = "Currently Employed";
            this.DateEdit_emp_end_date.Properties.NullValuePromptShowForEmptyValue = true;
            this.DateEdit_emp_end_date.Properties.VistaTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.DateEdit_emp_end_date.Size = new System.Drawing.Size(153, 20);
            this.DateEdit_emp_end_date.StyleController = this.LayoutControl1;
            this.DateEdit_emp_end_date.TabIndex = 21;
            this.DateEdit_emp_end_date.ToolTip = "Termination date or date counelor stopped counseling clients";
            // 
            // CheckEdit_ActiveFlag
            // 
            this.CheckEdit_ActiveFlag.Location = new System.Drawing.Point(138, 236);
            this.CheckEdit_ActiveFlag.Name = "CheckEdit_ActiveFlag";
            this.CheckEdit_ActiveFlag.Properties.Caption = "Active";
            this.CheckEdit_ActiveFlag.Size = new System.Drawing.Size(234, 20);
            this.CheckEdit_ActiveFlag.StyleController = this.LayoutControl1;
            this.CheckEdit_ActiveFlag.TabIndex = 16;
            // 
            // CheckEdit_default
            // 
            this.CheckEdit_default.Location = new System.Drawing.Point(24, 236);
            this.CheckEdit_default.Name = "CheckEdit_default";
            this.CheckEdit_default.Properties.Caption = "Default Counselor";
            this.CheckEdit_default.Size = new System.Drawing.Size(110, 20);
            this.CheckEdit_default.StyleController = this.LayoutControl1;
            this.CheckEdit_default.TabIndex = 15;
            // 
            // EmailRecordControl1
            // 
            this.EmailRecordControl1.ClickedColor = System.Drawing.Color.Maroon;
            this.EmailRecordControl1.Location = new System.Drawing.Point(84, 87);
            this.EmailRecordControl1.Name = "EmailRecordControl1";
            this.EmailRecordControl1.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.False;
            this.EmailRecordControl1.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Underline);
            this.EmailRecordControl1.Properties.Appearance.ForeColor = System.Drawing.Color.Blue;
            this.EmailRecordControl1.Properties.Appearance.Options.UseFont = true;
            this.EmailRecordControl1.Properties.Appearance.Options.UseForeColor = true;
            this.EmailRecordControl1.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.EmailRecordControl1.Properties.MaxLength = 998;
            this.EmailRecordControl1.Size = new System.Drawing.Size(288, 20);
            this.EmailRecordControl1.StyleController = this.LayoutControl1;
            this.EmailRecordControl1.TabIndex = 13;
            // 
            // TelephoneNumberRecordControl1
            // 
            this.TelephoneNumberRecordControl1.ErrorIcon = null;
            this.TelephoneNumberRecordControl1.ErrorText = "";
            this.TelephoneNumberRecordControl1.Location = new System.Drawing.Point(84, 135);
            this.TelephoneNumberRecordControl1.Margin = new System.Windows.Forms.Padding(0);
            this.TelephoneNumberRecordControl1.Name = "TelephoneNumberRecordControl1";
            this.TelephoneNumberRecordControl1.Size = new System.Drawing.Size(288, 20);
            this.TelephoneNumberRecordControl1.TabIndex = 11;
            // 
            // NameRecordControl1
            // 
            this.NameRecordControl1.Location = new System.Drawing.Point(72, 29);
            this.NameRecordControl1.Name = "NameRecordControl1";
            this.NameRecordControl1.Size = new System.Drawing.Size(312, 20);
            this.NameRecordControl1.TabIndex = 7;
            // 
            // TextEdit_menu_level
            // 
            this.TextEdit_menu_level.Location = new System.Drawing.Point(84, 159);
            this.TextEdit_menu_level.Name = "TextEdit_menu_level";
            this.TextEdit_menu_level.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.False;
            this.TextEdit_menu_level.Properties.Appearance.Options.UseTextOptions = true;
            this.TextEdit_menu_level.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.TextEdit_menu_level.Properties.Mask.BeepOnError = true;
            this.TextEdit_menu_level.Properties.Mask.EditMask = "n0";
            this.TextEdit_menu_level.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.TextEdit_menu_level.Properties.MaxLength = 10;
            this.TextEdit_menu_level.Size = new System.Drawing.Size(71, 20);
            this.TextEdit_menu_level.StyleController = this.LayoutControl1;
            this.TextEdit_menu_level.TabIndex = 3;
            // 
            // TextEdit_person
            // 
            this.TextEdit_person.Location = new System.Drawing.Point(84, 87);
            this.TextEdit_person.Name = "TextEdit_person";
            this.TextEdit_person.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.False;
            this.TextEdit_person.Properties.MaxLength = 80;
            this.TextEdit_person.Size = new System.Drawing.Size(288, 20);
            this.TextEdit_person.StyleController = this.LayoutControl1;
            this.TextEdit_person.TabIndex = 5;
            // 
            // LookUpEdit_office
            // 
            this.LookUpEdit_office.Location = new System.Drawing.Point(84, 111);
            this.LookUpEdit_office.Name = "LookUpEdit_office";
            this.LookUpEdit_office.Properties.ActionButtonIndex = 1;
            this.LookUpEdit_office.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Ellipsis, "Schedule", -1, true, true, false, DevExpress.XtraEditors.ImageLocation.MiddleCenter, null, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject1, "Click here to update the office schedule", "schedule", null, true),
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo, "", -1, true, true, false, DevExpress.XtraEditors.ImageLocation.MiddleCenter, null, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject2, "Click here to change the default office", "update", null, true)});
            this.LookUpEdit_office.Properties.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("Id", "ID", 20, DevExpress.Utils.FormatType.Numeric, "f0", false, DevExpress.Utils.HorzAlignment.Default),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("name", "Name", 20, DevExpress.Utils.FormatType.None, "", true, DevExpress.Utils.HorzAlignment.Default, DevExpress.Data.ColumnSortOrder.Ascending)});
            this.LookUpEdit_office.Properties.DisplayMember = "name";
            this.LookUpEdit_office.Properties.NullText = "";
            this.LookUpEdit_office.Properties.ShowFooter = false;
            this.LookUpEdit_office.Properties.ShowHeader = false;
            this.LookUpEdit_office.Properties.SortColumnIndex = 1;
            this.LookUpEdit_office.Properties.ValueMember = "Id";
            this.LookUpEdit_office.Size = new System.Drawing.Size(288, 20);
            this.LookUpEdit_office.StyleController = this.LayoutControl1;
            this.LookUpEdit_office.TabIndex = 9;
            // 
            // CounselorAttributes1
            // 
            this.CounselorAttributes1.Location = new System.Drawing.Point(24, 87);
            this.CounselorAttributes1.Name = "CounselorAttributes1";
            this.CounselorAttributes1.Size = new System.Drawing.Size(348, 169);
            this.CounselorAttributes1.TabIndex = 14;
            // 
            // LayoutControlGroup1
            // 
            this.LayoutControlGroup1.CustomizationFormText = "LayoutControlGroup1";
            this.LayoutControlGroup1.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.True;
            this.LayoutControlGroup1.GroupBordersVisible = false;
            this.LayoutControlGroup1.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.LayoutControlItem1,
            this.LayoutControlItem4,
            this.TabbedControlGroup1});
            this.LayoutControlGroup1.Location = new System.Drawing.Point(0, 0);
            this.LayoutControlGroup1.Name = "LayoutControlGroup1";
            this.LayoutControlGroup1.Size = new System.Drawing.Size(396, 280);
            this.LayoutControlGroup1.Text = "LayoutControlGroup1";
            this.LayoutControlGroup1.TextVisible = false;
            // 
            // LayoutControlItem1
            // 
            this.LayoutControlItem1.Control = this.LabelControl_ID;
            this.LayoutControlItem1.CustomizationFormText = "ID";
            this.LayoutControlItem1.Location = new System.Drawing.Point(0, 0);
            this.LayoutControlItem1.Name = "LayoutControlItem1";
            this.LayoutControlItem1.Size = new System.Drawing.Size(376, 17);
            this.LayoutControlItem1.Text = "ID";
            this.LayoutControlItem1.TextSize = new System.Drawing.Size(57, 13);
            // 
            // LayoutControlItem4
            // 
            this.LayoutControlItem4.Control = this.NameRecordControl1;
            this.LayoutControlItem4.CustomizationFormText = "Name";
            this.LayoutControlItem4.Location = new System.Drawing.Point(0, 17);
            this.LayoutControlItem4.Name = "LayoutControlItem4";
            this.LayoutControlItem4.Size = new System.Drawing.Size(376, 24);
            this.LayoutControlItem4.Text = "Name";
            this.LayoutControlItem4.TextSize = new System.Drawing.Size(57, 13);
            // 
            // TabbedControlGroup1
            // 
            this.TabbedControlGroup1.CustomizationFormText = "TabbedControlGroup1";
            this.TabbedControlGroup1.Location = new System.Drawing.Point(0, 41);
            this.TabbedControlGroup1.Name = "TabbedControlGroup1";
            this.TabbedControlGroup1.SelectedTabPage = this.LayoutControlGroup3;
            this.TabbedControlGroup1.SelectedTabPageIndex = 2;
            this.TabbedControlGroup1.Size = new System.Drawing.Size(376, 219);
            this.TabbedControlGroup1.TabPages.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.LayoutControlGroup4,
            this.LayoutControlGroup6,
            this.LayoutControlGroup3,
            this.LayoutControlGroup5,
            this.LayoutControlGroup2});
            this.TabbedControlGroup1.Text = "TabbedControlGroup1";
            // 
            // LayoutControlGroup3
            // 
            this.LayoutControlGroup3.CustomizationFormText = "Contact Information";
            this.LayoutControlGroup3.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.LayoutControlItem7,
            this.LayoutControlItem6,
            this.LayoutControlItem5});
            this.LayoutControlGroup3.Location = new System.Drawing.Point(0, 0);
            this.LayoutControlGroup3.Name = "LayoutControlGroup3";
            this.LayoutControlGroup3.Size = new System.Drawing.Size(352, 173);
            this.LayoutControlGroup3.Text = "Contact Information";
            // 
            // LayoutControlItem7
            // 
            this.LayoutControlItem7.Control = this.EmailRecordControl1;
            this.LayoutControlItem7.CustomizationFormText = "E-Mail";
            this.LayoutControlItem7.Location = new System.Drawing.Point(0, 0);
            this.LayoutControlItem7.Name = "LayoutControlItem7";
            this.LayoutControlItem7.Size = new System.Drawing.Size(352, 24);
            this.LayoutControlItem7.Text = "E-Mail";
            this.LayoutControlItem7.TextSize = new System.Drawing.Size(57, 13);
            // 
            // LayoutControlItem6
            // 
            this.LayoutControlItem6.Control = this.TelephoneNumberRecordControl1;
            this.LayoutControlItem6.CustomizationFormText = "Phone";
            this.LayoutControlItem6.Location = new System.Drawing.Point(0, 48);
            this.LayoutControlItem6.Name = "LayoutControlItem6";
            this.LayoutControlItem6.Size = new System.Drawing.Size(352, 125);
            this.LayoutControlItem6.Text = "Phone";
            this.LayoutControlItem6.TextSize = new System.Drawing.Size(57, 13);
            // 
            // LayoutControlItem5
            // 
            this.LayoutControlItem5.Control = this.LookUpEdit_office;
            this.LayoutControlItem5.CustomizationFormText = "Office";
            this.LayoutControlItem5.Location = new System.Drawing.Point(0, 24);
            this.LayoutControlItem5.Name = "LayoutControlItem5";
            this.LayoutControlItem5.Size = new System.Drawing.Size(352, 24);
            this.LayoutControlItem5.Text = "Office";
            this.LayoutControlItem5.TextSize = new System.Drawing.Size(57, 13);
            // 
            // LayoutControlGroup4
            // 
            this.LayoutControlGroup4.CustomizationFormText = "Database";
            this.LayoutControlGroup4.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.LayoutControlItem3,
            this.LayoutControlItem2,
            this.LayoutControlItem9,
            this.LayoutControlItem10,
            this.EmptySpaceItem1,
            this.LayoutControlItem12,
            this.LayoutControlItem11,
            this.LayoutControlItem20,
            this.layoutControlItem22});
            this.LayoutControlGroup4.Location = new System.Drawing.Point(0, 0);
            this.LayoutControlGroup4.Name = "LayoutControlGroup4";
            this.LayoutControlGroup4.Size = new System.Drawing.Size(352, 173);
            this.LayoutControlGroup4.Text = "Database";
            // 
            // LayoutControlItem3
            // 
            this.LayoutControlItem3.Control = this.TextEdit_person;
            this.LayoutControlItem3.CustomizationFormText = "Signon";
            this.LayoutControlItem3.Location = new System.Drawing.Point(0, 0);
            this.LayoutControlItem3.Name = "LayoutControlItem3";
            this.LayoutControlItem3.Size = new System.Drawing.Size(352, 24);
            this.LayoutControlItem3.Text = "Signon";
            this.LayoutControlItem3.TextSize = new System.Drawing.Size(57, 13);
            // 
            // LayoutControlItem2
            // 
            this.LayoutControlItem2.Control = this.TextEdit_menu_level;
            this.LayoutControlItem2.CustomizationFormText = "Menu Level";
            this.LayoutControlItem2.Location = new System.Drawing.Point(0, 72);
            this.LayoutControlItem2.Name = "LayoutControlItem2";
            this.LayoutControlItem2.Size = new System.Drawing.Size(135, 24);
            this.LayoutControlItem2.Text = "Menu Level";
            this.LayoutControlItem2.TextSize = new System.Drawing.Size(57, 13);
            // 
            // LayoutControlItem9
            // 
            this.LayoutControlItem9.Control = this.CheckEdit_default;
            this.LayoutControlItem9.CustomizationFormText = "Default Counselor";
            this.LayoutControlItem9.Location = new System.Drawing.Point(0, 149);
            this.LayoutControlItem9.Name = "LayoutControlItem9";
            this.LayoutControlItem9.Size = new System.Drawing.Size(114, 24);
            this.LayoutControlItem9.Text = "Default Counselor";
            this.LayoutControlItem9.TextSize = new System.Drawing.Size(0, 0);
            this.LayoutControlItem9.TextToControlDistance = 0;
            this.LayoutControlItem9.TextVisible = false;
            // 
            // LayoutControlItem10
            // 
            this.LayoutControlItem10.Control = this.CheckEdit_ActiveFlag;
            this.LayoutControlItem10.CustomizationFormText = "Active";
            this.LayoutControlItem10.Location = new System.Drawing.Point(114, 149);
            this.LayoutControlItem10.Name = "LayoutControlItem10";
            this.LayoutControlItem10.Size = new System.Drawing.Size(238, 24);
            this.LayoutControlItem10.Text = "Active";
            this.LayoutControlItem10.TextSize = new System.Drawing.Size(0, 0);
            this.LayoutControlItem10.TextToControlDistance = 0;
            this.LayoutControlItem10.TextVisible = false;
            // 
            // EmptySpaceItem1
            // 
            this.EmptySpaceItem1.AllowHotTrack = false;
            this.EmptySpaceItem1.CustomizationFormText = "EmptySpaceItem1";
            this.EmptySpaceItem1.Location = new System.Drawing.Point(0, 120);
            this.EmptySpaceItem1.Name = "EmptySpaceItem1";
            this.EmptySpaceItem1.Size = new System.Drawing.Size(352, 29);
            this.EmptySpaceItem1.Text = "EmptySpaceItem1";
            this.EmptySpaceItem1.TextSize = new System.Drawing.Size(0, 0);
            // 
            // LayoutControlItem12
            // 
            this.LayoutControlItem12.Control = this.ImageEdit1;
            this.LayoutControlItem12.CustomizationFormText = "Picture";
            this.LayoutControlItem12.Location = new System.Drawing.Point(0, 96);
            this.LayoutControlItem12.Name = "LayoutControlItem12";
            this.LayoutControlItem12.Size = new System.Drawing.Size(352, 24);
            this.LayoutControlItem12.Text = "Picture";
            this.LayoutControlItem12.TextSize = new System.Drawing.Size(57, 13);
            // 
            // LayoutControlItem11
            // 
            this.LayoutControlItem11.Control = this.ColorEdit1;
            this.LayoutControlItem11.CustomizationFormText = "Color";
            this.LayoutControlItem11.Location = new System.Drawing.Point(135, 72);
            this.LayoutControlItem11.Name = "LayoutControlItem11";
            this.LayoutControlItem11.Size = new System.Drawing.Size(217, 24);
            this.LayoutControlItem11.Text = "Color";
            this.LayoutControlItem11.TextSize = new System.Drawing.Size(57, 13);
            // 
            // LayoutControlItem20
            // 
            this.LayoutControlItem20.Control = this.TextEditNote;
            this.LayoutControlItem20.CustomizationFormText = "Note";
            this.LayoutControlItem20.Location = new System.Drawing.Point(0, 48);
            this.LayoutControlItem20.Name = "LayoutControlItem20";
            this.LayoutControlItem20.Size = new System.Drawing.Size(352, 24);
            this.LayoutControlItem20.Text = "Note";
            this.LayoutControlItem20.TextSize = new System.Drawing.Size(57, 13);
            // 
            // layoutControlItem22
            // 
            this.layoutControlItem22.Control = this.lookUpEdit_Manager;
            this.layoutControlItem22.CustomizationFormText = "Manager";
            this.layoutControlItem22.Location = new System.Drawing.Point(0, 24);
            this.layoutControlItem22.Name = "layoutControlItem22";
            this.layoutControlItem22.Size = new System.Drawing.Size(352, 24);
            this.layoutControlItem22.Text = "Manager";
            this.layoutControlItem22.TextSize = new System.Drawing.Size(57, 13);
            // 
            // LayoutControlGroup6
            // 
            this.LayoutControlGroup6.CustomizationFormText = "Attributes";
            this.LayoutControlGroup6.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.LayoutControlItem8});
            this.LayoutControlGroup6.Location = new System.Drawing.Point(0, 0);
            this.LayoutControlGroup6.Name = "LayoutControlGroup6";
            this.LayoutControlGroup6.Size = new System.Drawing.Size(352, 173);
            this.LayoutControlGroup6.Text = "Attributes";
            // 
            // LayoutControlItem8
            // 
            this.LayoutControlItem8.Control = this.CounselorAttributes1;
            this.LayoutControlItem8.CustomizationFormText = "Appointment Attributes";
            this.LayoutControlItem8.Location = new System.Drawing.Point(0, 0);
            this.LayoutControlItem8.Name = "LayoutControlItem8";
            this.LayoutControlItem8.Size = new System.Drawing.Size(352, 173);
            this.LayoutControlItem8.Text = "Appointment Attributes";
            this.LayoutControlItem8.TextSize = new System.Drawing.Size(0, 0);
            this.LayoutControlItem8.TextToControlDistance = 0;
            this.LayoutControlItem8.TextVisible = false;
            // 
            // LayoutControlGroup5
            // 
            this.LayoutControlGroup5.CustomizationFormText = "Housing";
            this.LayoutControlGroup5.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.LayoutControlItem18,
            this.LayoutControlItem14,
            this.LayoutControlItem15,
            this.LayoutControlItem16,
            this.LayoutControlItem17,
            this.LayoutControlItem13,
            this.LayoutControlItem21,
            this.EmptySpaceItem2});
            this.LayoutControlGroup5.Location = new System.Drawing.Point(0, 0);
            this.LayoutControlGroup5.Name = "LayoutControlGroup5";
            this.LayoutControlGroup5.Size = new System.Drawing.Size(352, 173);
            this.LayoutControlGroup5.Text = "Housing";
            // 
            // LayoutControlItem18
            // 
            this.LayoutControlItem18.Control = this.LookUpEdit_billing_method;
            this.LayoutControlItem18.CustomizationFormText = "Pay Method";
            this.LayoutControlItem18.Location = new System.Drawing.Point(0, 24);
            this.LayoutControlItem18.Name = "LayoutControlItem18";
            this.LayoutControlItem18.Size = new System.Drawing.Size(135, 24);
            this.LayoutControlItem18.Text = "Pay Method";
            this.LayoutControlItem18.TextSize = new System.Drawing.Size(57, 13);
            // 
            // LayoutControlItem14
            // 
            this.LayoutControlItem14.Control = this.DateEdit_emp_start_date;
            this.LayoutControlItem14.CustomizationFormText = "Start Date";
            this.LayoutControlItem14.Location = new System.Drawing.Point(0, 48);
            this.LayoutControlItem14.Name = "LayoutControlItem14";
            this.LayoutControlItem14.Size = new System.Drawing.Size(135, 24);
            this.LayoutControlItem14.Text = "Start Date";
            this.LayoutControlItem14.TextSize = new System.Drawing.Size(57, 13);
            // 
            // LayoutControlItem15
            // 
            this.LayoutControlItem15.Control = this.DateEdit_emp_end_date;
            this.LayoutControlItem15.CustomizationFormText = "End Date";
            this.LayoutControlItem15.Location = new System.Drawing.Point(135, 48);
            this.LayoutControlItem15.Name = "LayoutControlItem15";
            this.LayoutControlItem15.Size = new System.Drawing.Size(217, 24);
            this.LayoutControlItem15.Text = "End Date";
            this.LayoutControlItem15.TextSize = new System.Drawing.Size(57, 13);
            // 
            // LayoutControlItem16
            // 
            this.LayoutControlItem16.Control = this.Ssn1;
            this.LayoutControlItem16.CustomizationFormText = "SSN";
            this.LayoutControlItem16.Location = new System.Drawing.Point(135, 0);
            this.LayoutControlItem16.Name = "LayoutControlItem16";
            this.LayoutControlItem16.Size = new System.Drawing.Size(217, 24);
            this.LayoutControlItem16.Text = "SSN";
            this.LayoutControlItem16.TextSize = new System.Drawing.Size(57, 13);
            // 
            // LayoutControlItem17
            // 
            this.LayoutControlItem17.Control = this.TextEdit_HUD_id;
            this.LayoutControlItem17.CustomizationFormText = "HUD ID #";
            this.LayoutControlItem17.Location = new System.Drawing.Point(0, 0);
            this.LayoutControlItem17.Name = "LayoutControlItem17";
            this.LayoutControlItem17.Size = new System.Drawing.Size(135, 24);
            this.LayoutControlItem17.Text = "HUD ID #";
            this.LayoutControlItem17.TextSize = new System.Drawing.Size(57, 13);
            // 
            // LayoutControlItem13
            // 
            this.LayoutControlItem13.Control = this.CalcEdit_hourly_rate;
            this.LayoutControlItem13.CustomizationFormText = "Rate";
            this.LayoutControlItem13.Location = new System.Drawing.Point(135, 24);
            this.LayoutControlItem13.Name = "LayoutControlItem13";
            this.LayoutControlItem13.Size = new System.Drawing.Size(217, 24);
            this.LayoutControlItem13.Text = "Rate";
            this.LayoutControlItem13.TextSize = new System.Drawing.Size(57, 13);
            // 
            // LayoutControlItem21
            // 
            this.LayoutControlItem21.Control = this.HousingAttributeListControl1;
            this.LayoutControlItem21.CustomizationFormText = "Housing Attribues";
            this.LayoutControlItem21.Location = new System.Drawing.Point(0, 89);
            this.LayoutControlItem21.Name = "LayoutControlItem21";
            this.LayoutControlItem21.Size = new System.Drawing.Size(352, 84);
            this.LayoutControlItem21.Text = "Housing Attribues";
            this.LayoutControlItem21.TextSize = new System.Drawing.Size(0, 0);
            this.LayoutControlItem21.TextToControlDistance = 0;
            this.LayoutControlItem21.TextVisible = false;
            // 
            // EmptySpaceItem2
            // 
            this.EmptySpaceItem2.AllowHotTrack = false;
            this.EmptySpaceItem2.CustomizationFormText = "EmptySpaceItem2";
            this.EmptySpaceItem2.Location = new System.Drawing.Point(0, 72);
            this.EmptySpaceItem2.MaxSize = new System.Drawing.Size(0, 17);
            this.EmptySpaceItem2.MinSize = new System.Drawing.Size(10, 17);
            this.EmptySpaceItem2.Name = "EmptySpaceItem2";
            this.EmptySpaceItem2.Size = new System.Drawing.Size(352, 17);
            this.EmptySpaceItem2.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.EmptySpaceItem2.Text = "EmptySpaceItem2";
            this.EmptySpaceItem2.TextSize = new System.Drawing.Size(0, 0);
            // 
            // LayoutControlGroup2
            // 
            this.LayoutControlGroup2.CustomizationFormText = "Training";
            this.LayoutControlGroup2.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.LayoutControlItem19});
            this.LayoutControlGroup2.Location = new System.Drawing.Point(0, 0);
            this.LayoutControlGroup2.Name = "LayoutControlGroup2";
            this.LayoutControlGroup2.Size = new System.Drawing.Size(352, 173);
            this.LayoutControlGroup2.Text = "Training";
            // 
            // LayoutControlItem19
            // 
            this.LayoutControlItem19.Control = this.TrainingListControl1;
            this.LayoutControlItem19.CustomizationFormText = "Training Information";
            this.LayoutControlItem19.Location = new System.Drawing.Point(0, 0);
            this.LayoutControlItem19.Name = "LayoutControlItem19";
            this.LayoutControlItem19.Size = new System.Drawing.Size(352, 173);
            this.LayoutControlItem19.Text = "Training Information";
            this.LayoutControlItem19.TextSize = new System.Drawing.Size(0, 0);
            this.LayoutControlItem19.TextToControlDistance = 0;
            this.LayoutControlItem19.TextVisible = false;
            // 
            // CounselorEditForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(496, 280);
            this.Controls.Add(this.LayoutControl1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.Sizable;
            this.Name = "CounselorEditForm";
            this.Text = "Update Counselor Information";
            this.Controls.SetChildIndex(this.simpleButton_OK, 0);
            this.Controls.SetChildIndex(this.simpleButton_Cancel, 0);
            this.Controls.SetChildIndex(this.LayoutControl1, 0);
            ((System.ComponentModel.ISupportInitialize)(this.DxErrorProvider1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LayoutControl1)).EndInit();
            this.LayoutControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.lookUpEdit_Manager.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TextEditNote.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LookUpEdit_billing_method.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TextEdit_HUD_id.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Ssn1.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DateEdit_emp_start_date.Properties.VistaTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DateEdit_emp_start_date.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.CalcEdit_hourly_rate.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ImageEdit1.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ColorEdit1.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DateEdit_emp_end_date.Properties.VistaTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DateEdit_emp_end_date.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.CheckEdit_ActiveFlag.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.CheckEdit_default.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.EmailRecordControl1.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TelephoneNumberRecordControl1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TextEdit_menu_level.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TextEdit_person.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LookUpEdit_office.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LayoutControlGroup1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LayoutControlItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LayoutControlItem4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TabbedControlGroup1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LayoutControlGroup3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LayoutControlItem7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LayoutControlItem6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LayoutControlItem5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LayoutControlGroup4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LayoutControlItem3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LayoutControlItem2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LayoutControlItem9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LayoutControlItem10)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.EmptySpaceItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LayoutControlItem12)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LayoutControlItem11)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LayoutControlItem20)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem22)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LayoutControlGroup6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LayoutControlItem8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LayoutControlGroup5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LayoutControlItem18)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LayoutControlItem14)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LayoutControlItem15)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LayoutControlItem16)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LayoutControlItem17)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LayoutControlItem13)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LayoutControlItem21)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.EmptySpaceItem2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LayoutControlGroup2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LayoutControlItem19)).EndInit();
            this.ResumeLayout(false);

		}
		private DevExpress.XtraEditors.LabelControl LabelControl_ID;
		private DevExpress.XtraEditors.TextEdit TextEdit_menu_level;
		private DevExpress.XtraEditors.CheckEdit CheckEdit_default;
		private DevExpress.XtraEditors.CheckEdit CheckEdit_ActiveFlag;
		private DevExpress.XtraEditors.LookUpEdit LookUpEdit_office;
		private DevExpress.XtraEditors.TextEdit TextEdit_person;
		private AttributeListControl CounselorAttributes1;
		private global::DebtPlus.Data.Controls.EmailRecordControl EmailRecordControl1;
		private global::DebtPlus.Data.Controls.TelephoneNumberRecordControl TelephoneNumberRecordControl1;
		private global::DebtPlus.Data.Controls.NameRecordControl NameRecordControl1;
		private DevExpress.XtraLayout.LayoutControl LayoutControl1;
		private DevExpress.XtraLayout.LayoutControlGroup LayoutControlGroup1;
		private DevExpress.XtraEditors.ColorEdit ColorEdit1;
		private DevExpress.XtraEditors.ImageEdit ImageEdit1;
		private DevExpress.XtraEditors.CalcEdit CalcEdit_hourly_rate;
		private DevExpress.XtraEditors.DateEdit DateEdit_emp_start_date;
		private DevExpress.XtraEditors.DateEdit DateEdit_emp_end_date;
		private global::DebtPlus.Data.Controls.ssn Ssn1;
		private DevExpress.XtraEditors.TextEdit TextEdit_HUD_id;
		private DevExpress.XtraEditors.LookUpEdit LookUpEdit_billing_method;
		private TrainingListControl TrainingListControl1;
		private DevExpress.XtraEditors.TextEdit TextEditNote;
		private HousingAttributeListControl HousingAttributeListControl1;
		private DevExpress.XtraLayout.LayoutControlItem LayoutControlItem1;
		private DevExpress.XtraLayout.LayoutControlItem LayoutControlItem4;
		private DevExpress.XtraLayout.TabbedControlGroup TabbedControlGroup1;
		private DevExpress.XtraLayout.LayoutControlGroup LayoutControlGroup4;
		private DevExpress.XtraLayout.LayoutControlItem LayoutControlItem3;
		private DevExpress.XtraLayout.LayoutControlItem LayoutControlItem2;
		private DevExpress.XtraLayout.LayoutControlItem LayoutControlItem9;
		private DevExpress.XtraLayout.LayoutControlItem LayoutControlItem10;
		private DevExpress.XtraLayout.EmptySpaceItem EmptySpaceItem1;
		private DevExpress.XtraLayout.LayoutControlItem LayoutControlItem12;
		private DevExpress.XtraLayout.LayoutControlItem LayoutControlItem11;
		private DevExpress.XtraLayout.LayoutControlItem LayoutControlItem20;
		private DevExpress.XtraLayout.LayoutControlGroup LayoutControlGroup6;
		private DevExpress.XtraLayout.LayoutControlItem LayoutControlItem8;
		private DevExpress.XtraLayout.LayoutControlGroup LayoutControlGroup3;
		private DevExpress.XtraLayout.LayoutControlItem LayoutControlItem7;
		private DevExpress.XtraLayout.LayoutControlItem LayoutControlItem6;
		private DevExpress.XtraLayout.LayoutControlItem LayoutControlItem5;
		private DevExpress.XtraLayout.LayoutControlGroup LayoutControlGroup5;
		private DevExpress.XtraLayout.LayoutControlItem LayoutControlItem18;
		private DevExpress.XtraLayout.LayoutControlItem LayoutControlItem14;
		private DevExpress.XtraLayout.LayoutControlItem LayoutControlItem15;
		private DevExpress.XtraLayout.LayoutControlItem LayoutControlItem16;
		private DevExpress.XtraLayout.LayoutControlItem LayoutControlItem17;
		private DevExpress.XtraLayout.LayoutControlItem LayoutControlItem13;
		private DevExpress.XtraLayout.LayoutControlItem LayoutControlItem21;
		private DevExpress.XtraLayout.EmptySpaceItem EmptySpaceItem2;
		private DevExpress.XtraLayout.LayoutControlGroup LayoutControlGroup2;
		private DevExpress.XtraLayout.LayoutControlItem LayoutControlItem19;
        private DevExpress.XtraEditors.LookUpEdit lookUpEdit_Manager;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem22;
	}
}
