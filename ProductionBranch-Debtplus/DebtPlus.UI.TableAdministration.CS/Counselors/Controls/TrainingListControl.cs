#region "Copyright 2000-2012 DebtPlus, L.L.C."
//{*******************************************************************}
//{                                                                   }
//{       DebtPlus Debt Management System                             }
//{                                                                   }
//{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
//{       ALL RIGHTS RESERVED                                         }
//{                                                                   }
//{   The entire contents of this file is protected by U.S. and       }
//{   International Copyright Laws. Unauthorized reproduction,        }
//{   reverse-engineering, and distribution of all or any portion of  }
//{   the code contained in this file is strictly prohibited and may  }
//{   result in severe civil and criminal penalties and will be       }
//{   prosecuted to the maximum extent possible under the law.        }
//{                                                                   }
//{   RESTRICTIONS                                                    }
//{                                                                   }
//{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
//{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
//{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
//{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
//{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
//{                                                                   }
//{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
//{   ADDITIONAL RESTRICTIONS.                                        }
//{                                                                   }
//{*******************************************************************}

#endregion "Copyright 2000-2012 DebtPlus, L.L.C."

using System;
using System.Windows.Forms;
using DebtPlus.LINQ;
using DevExpress.XtraGrid.Views.Grid.ViewInfo;

namespace DebtPlus.UI.TableAdministration.CS.Counselors.Controls
{
    internal partial class TrainingListControl : DevExpress.XtraEditors.XtraUserControl, System.ComponentModel.INotifyPropertyChanged
    {
        // Local storage
        private BusinessContext bc = null;

        private counselor counselorRecord = null;

        /// <summary>
        /// Initialize the new control class
        /// </summary>
        internal TrainingListControl()
            : base()
        {
            InitializeComponent();
        }

        /// <summary>
        /// Event raised when the list information is changed
        /// </summary>
        public event System.ComponentModel.PropertyChangedEventHandler PropertyChanged;

        /// <summary>
        /// Raise the PropertyChanged event
        /// </summary>
        /// <param name="PropertyName"></param>
        protected void RaisePropertyChanged(string PropertyName)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new System.ComponentModel.PropertyChangedEventArgs(PropertyName));
            }
        }

        /// <summary>
        /// Add the event handler routines to the events
        /// </summary>
        private void RegisterHandlers()
        {
            gridView1.DoubleClick += gridView1_DoubleClick;
            ContextMenu1.Popup += ContextMenu1_Popup;
            ContextMenu1_Create.Click += ContextMenu1_Create_Click;
            ContextMenu1_Edit.Click += ContextMenu1_Edit_Click;
            ContextMenu1_Delete.Click += ContextMenu1_Delete_Click;
        }

        /// <summary>
        /// Remove the event handler registrations
        /// </summary>
        private void UnRegisterHandlers()
        {
            gridView1.DoubleClick -= gridView1_DoubleClick;
            ContextMenu1.Popup -= ContextMenu1_Popup;
            ContextMenu1_Create.Click -= ContextMenu1_Create_Click;
            ContextMenu1_Edit.Click -= ContextMenu1_Edit_Click;
            ContextMenu1_Delete.Click -= ContextMenu1_Delete_Click;
        }

        /// <summary>
        /// Retrieve the list of training courses for the counselor and load the grid information.
        /// </summary>
        protected internal virtual void ReadForm(BusinessContext bc, counselor counselorRecord)
        {
            this.bc = bc;
            this.counselorRecord = counselorRecord;

            UnRegisterHandlers();
            try
            {
                // Load the grid control with the counselor training information
                gridControl1.DataSource = counselorRecord.counselor_trainings.GetNewBindingList();
                gridView1.RefreshData();
            }
            finally
            {
                RegisterHandlers();
            }
        }

        /// <summary>
        /// Create a new training record for the counselor.
        /// </summary>
        private void CreateRecord()
        {
            // Generate a copy of the factor
            var record = DebtPlus.LINQ.Factory.Manufacture_counselor_training();
            using (var frm = new TrainingEditForm(bc, record))
            {
                if (frm.ShowDialog() != DialogResult.OK)
                {
                    return;
                }
            }

            // Add the record to the collection. It will be written when the counselor is updated
            counselorRecord.counselor_trainings.Add(record);
            RaisePropertyChanged(null);

            // Update the display list with the new information.
            gridControl1.DataSource = counselorRecord.counselor_trainings.GetNewBindingList();
            gridView1.RefreshData();
        }

        /// <summary>
        /// Delete the indicated training course for the counselor
        /// </summary>
        private void DeleteRecord(object obj)
        {
            // Find the record to delete
            var record = obj as counselor_training;
            if (record == null)
            {
                return;
            }

            // Remove the record from the list and set the pointer to NULL. This will trip the delete operation.
            counselorRecord.counselor_trainings.Remove(record);
            record.counselor1 = null;

            // Update the display with the new list.
            gridControl1.DataSource = counselorRecord.counselor_trainings.GetNewBindingList();
            gridView1.RefreshData();
            RaisePropertyChanged(null);
        }

        /// <summary>
        /// Edit the current row and return the status for the edit
        /// </summary>
        private void UpdateRecord(object obj)
        {
            var record = obj as counselor_training;
            if (record == null)
            {
                return;
            }

            // Edit the record
            using (var frm = new TrainingEditForm(bc, record))
            {
                if (frm.ShowDialog() != DialogResult.OK)
                {
                    return;
                }
            }

            // Update the display with the new list contents
            gridControl1.DataSource = counselorRecord.counselor_trainings.GetNewBindingList();
            gridView1.RefreshData();

            RaisePropertyChanged(null);
        }

        /// <summary>
        /// Double click event on the list
        /// </summary>
        private void gridView1_DoubleClick(object sender, EventArgs e)
        {
            GridHitInfo hi = gridView1.CalcHitInfo((gridControl1.PointToClient(MousePosition)));
            Int32 rowHandle = hi.RowHandle;
            if (rowHandle >= 0)
            {
                UpdateRecord(gridView1.GetRow(rowHandle));
            }
        }

        /// <summary>
        /// Edit -> menu being shown
        /// </summary>
        private void ContextMenu1_Popup(object sender, EventArgs e)
        {
            GridHitInfo hi = gridView1.CalcHitInfo((gridControl1.PointToClient(MousePosition)));
            gridView1.FocusedRowHandle = hi.RowHandle;
            var obj = gridView1.GetRow(hi.RowHandle);

            if (obj != null)
            {
                ContextMenu1_Edit.Enabled = true;
                ContextMenu1_Delete.Enabled = true;
            }
            else
            {
                ContextMenu1_Edit.Enabled = false;
                ContextMenu1_Delete.Enabled = false;
            }
        }

        /// <summary>
        /// Create menu record
        /// </summary>
        private void ContextMenu1_Create_Click(object sender, EventArgs e)
        {
            CreateRecord();
        }

        /// <summary>
        /// Edit menu record
        /// </summary>
        private void ContextMenu1_Edit_Click(object sender, EventArgs e)
        {
            if (gridView1.FocusedRowHandle >= 0)
            {
                UpdateRecord(gridView1.GetRow(gridView1.FocusedRowHandle));
            }
        }

        /// <summary>
        /// Edit -> Delete menu record clicked
        /// </summary>
        private void ContextMenu1_Delete_Click(object sender, EventArgs e)
        {
            if (gridView1.FocusedRowHandle >= 0)
            {
                DeleteRecord(gridView1.GetRow(gridView1.FocusedRowHandle));
            }
        }
    }
}