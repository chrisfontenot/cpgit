using System;
using System.Windows.Forms;
using System.Drawing;
using DebtPlus.LINQ;
using System.Linq;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Diagnostics;

namespace DebtPlus.UI.TableAdministration.CS.Counselors.Controls
{
	partial class TrainingListControl
	{
		//UserControl overrides dispose to clean up the component list.
		[System.Diagnostics.DebuggerNonUserCode]
		protected override void Dispose(bool disposing)
		{
			if (disposing && components != null) 
{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		//Required by the Windows Form Designer

		private System.ComponentModel.IContainer components = null;
		//NOTE: The following procedure is required by the Windows Form Designer
		//It can be modified using the Windows Form Designer.  
		//Do not modify it using the code editor.
		[System.Diagnostics.DebuggerStepThrough]
		private void InitializeComponent()
		{
			this.components = new System.ComponentModel.Container();
			this.gridControl1 = new DevExpress.XtraGrid.GridControl();
			this.gridView1 = new DevExpress.XtraGrid.Views.Grid.GridView();
			this.gridColumn_Training_Title = new DevExpress.XtraGrid.Columns.GridColumn();
			this.gridColumn_TrainingDate = new DevExpress.XtraGrid.Columns.GridColumn();
			this.ContextMenu1 = new ContextMenu();
			this.ContextMenu1_Edit = new MenuItem();
			this.ContextMenu1_Create = new MenuItem();
			this.ContextMenu1_Delete = new MenuItem();
			((System.ComponentModel.ISupportInitialize)this.gridControl1).BeginInit();
			((System.ComponentModel.ISupportInitialize)this.gridView1).BeginInit();
			this.SuspendLayout();
			//
			//gridControl1
			//
			this.gridControl1.Dock = DockStyle.Fill;
			this.gridControl1.Location = new System.Drawing.Point(0, 0);
			this.gridControl1.MainView = this.gridView1;
			this.gridControl1.Name = "gridControl1";
			this.gridControl1.Size = new System.Drawing.Size(409, 189);
			this.gridControl1.TabIndex = 0;
			this.gridControl1.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] { this.gridView1 });
			this.gridControl1.ContextMenu = this.ContextMenu1;
			//
			//gridView1
			//
			this.gridView1.Appearance.ColumnFilterButton.BackColor = System.Drawing.Color.FromArgb(221, 236, 254);
			this.gridView1.Appearance.ColumnFilterButton.BackColor2 = System.Drawing.Color.FromArgb(132, 171, 228);
			this.gridView1.Appearance.ColumnFilterButton.BorderColor = System.Drawing.Color.FromArgb(221, 236, 254);
			this.gridView1.Appearance.ColumnFilterButton.ForeColor = System.Drawing.Color.Black;
			this.gridView1.Appearance.ColumnFilterButton.GradientMode = System.Drawing.Drawing2D.LinearGradientMode.Vertical;
			this.gridView1.Appearance.ColumnFilterButton.Options.UseBackColor = true;
			this.gridView1.Appearance.ColumnFilterButton.Options.UseBorderColor = true;
			this.gridView1.Appearance.ColumnFilterButton.Options.UseForeColor = true;
			this.gridView1.Appearance.ColumnFilterButtonActive.BackColor = System.Drawing.Color.FromArgb(247, 251, 255);
			this.gridView1.Appearance.ColumnFilterButtonActive.BackColor2 = System.Drawing.Color.FromArgb(154, 190, 243);
			this.gridView1.Appearance.ColumnFilterButtonActive.BorderColor = System.Drawing.Color.FromArgb(247, 251, 255);
			this.gridView1.Appearance.ColumnFilterButtonActive.ForeColor = System.Drawing.Color.Black;
			this.gridView1.Appearance.ColumnFilterButtonActive.GradientMode = System.Drawing.Drawing2D.LinearGradientMode.Vertical;
			this.gridView1.Appearance.ColumnFilterButtonActive.Options.UseBackColor = true;
			this.gridView1.Appearance.ColumnFilterButtonActive.Options.UseBorderColor = true;
			this.gridView1.Appearance.ColumnFilterButtonActive.Options.UseForeColor = true;
			this.gridView1.Appearance.Empty.BackColor = System.Drawing.Color.White;
			this.gridView1.Appearance.Empty.Options.UseBackColor = true;
			this.gridView1.Appearance.EvenRow.BackColor = System.Drawing.Color.FromArgb(231, 242, 254);
			this.gridView1.Appearance.EvenRow.ForeColor = System.Drawing.Color.Black;
			this.gridView1.Appearance.EvenRow.Options.UseBackColor = true;
			this.gridView1.Appearance.EvenRow.Options.UseForeColor = true;
			this.gridView1.Appearance.FilterCloseButton.BackColor = System.Drawing.Color.FromArgb(221, 236, 254);
			this.gridView1.Appearance.FilterCloseButton.BackColor2 = System.Drawing.Color.FromArgb(132, 171, 228);
			this.gridView1.Appearance.FilterCloseButton.BorderColor = System.Drawing.Color.FromArgb(221, 236, 254);
			this.gridView1.Appearance.FilterCloseButton.ForeColor = System.Drawing.Color.Black;
			this.gridView1.Appearance.FilterCloseButton.GradientMode = System.Drawing.Drawing2D.LinearGradientMode.Vertical;
			this.gridView1.Appearance.FilterCloseButton.Options.UseBackColor = true;
			this.gridView1.Appearance.FilterCloseButton.Options.UseBorderColor = true;
			this.gridView1.Appearance.FilterCloseButton.Options.UseForeColor = true;
			this.gridView1.Appearance.FilterPanel.BackColor = System.Drawing.Color.FromArgb(62, 109, 185);
			this.gridView1.Appearance.FilterPanel.ForeColor = System.Drawing.Color.White;
			this.gridView1.Appearance.FilterPanel.Options.UseBackColor = true;
			this.gridView1.Appearance.FilterPanel.Options.UseForeColor = true;
			this.gridView1.Appearance.FixedLine.BackColor = System.Drawing.Color.FromArgb(59, 97, 156);
			this.gridView1.Appearance.FixedLine.Options.UseBackColor = true;
			this.gridView1.Appearance.FocusedCell.BackColor = System.Drawing.Color.White;
			this.gridView1.Appearance.FocusedCell.ForeColor = System.Drawing.Color.Black;
			this.gridView1.Appearance.FocusedCell.Options.UseBackColor = true;
			this.gridView1.Appearance.FocusedCell.Options.UseForeColor = true;
			this.gridView1.Appearance.FocusedRow.BackColor = System.Drawing.Color.FromArgb(49, 106, 197);
			this.gridView1.Appearance.FocusedRow.ForeColor = System.Drawing.Color.White;
			this.gridView1.Appearance.FocusedRow.Options.UseBackColor = true;
			this.gridView1.Appearance.FocusedRow.Options.UseForeColor = true;
			this.gridView1.Appearance.FooterPanel.BackColor = System.Drawing.Color.FromArgb(221, 236, 254);
			this.gridView1.Appearance.FooterPanel.BackColor2 = System.Drawing.Color.FromArgb(132, 171, 228);
			this.gridView1.Appearance.FooterPanel.BorderColor = System.Drawing.Color.FromArgb(221, 236, 254);
			this.gridView1.Appearance.FooterPanel.ForeColor = System.Drawing.Color.Black;
			this.gridView1.Appearance.FooterPanel.GradientMode = System.Drawing.Drawing2D.LinearGradientMode.Vertical;
			this.gridView1.Appearance.FooterPanel.Options.UseBackColor = true;
			this.gridView1.Appearance.FooterPanel.Options.UseBorderColor = true;
			this.gridView1.Appearance.FooterPanel.Options.UseForeColor = true;
			this.gridView1.Appearance.GroupButton.BackColor = System.Drawing.Color.FromArgb(193, 216, 247);
			this.gridView1.Appearance.GroupButton.BorderColor = System.Drawing.Color.FromArgb(193, 216, 247);
			this.gridView1.Appearance.GroupButton.ForeColor = System.Drawing.Color.Black;
			this.gridView1.Appearance.GroupButton.Options.UseBackColor = true;
			this.gridView1.Appearance.GroupButton.Options.UseBorderColor = true;
			this.gridView1.Appearance.GroupButton.Options.UseForeColor = true;
			this.gridView1.Appearance.GroupFooter.BackColor = System.Drawing.Color.FromArgb(193, 216, 247);
			this.gridView1.Appearance.GroupFooter.BorderColor = System.Drawing.Color.FromArgb(193, 216, 247);
			this.gridView1.Appearance.GroupFooter.ForeColor = System.Drawing.Color.Black;
			this.gridView1.Appearance.GroupFooter.Options.UseBackColor = true;
			this.gridView1.Appearance.GroupFooter.Options.UseBorderColor = true;
			this.gridView1.Appearance.GroupFooter.Options.UseForeColor = true;
			this.gridView1.Appearance.GroupPanel.BackColor = System.Drawing.Color.FromArgb(62, 109, 185);
			this.gridView1.Appearance.GroupPanel.ForeColor = System.Drawing.Color.FromArgb(221, 236, 254);
			this.gridView1.Appearance.GroupPanel.Options.UseBackColor = true;
			this.gridView1.Appearance.GroupPanel.Options.UseForeColor = true;
			this.gridView1.Appearance.GroupRow.BackColor = System.Drawing.Color.FromArgb(193, 216, 247);
			this.gridView1.Appearance.GroupRow.BorderColor = System.Drawing.Color.FromArgb(193, 216, 247);
			this.gridView1.Appearance.GroupRow.Font = new System.Drawing.Font("Tahoma", 8f, System.Drawing.FontStyle.Bold);
			this.gridView1.Appearance.GroupRow.ForeColor = System.Drawing.Color.Black;
			this.gridView1.Appearance.GroupRow.Options.UseBackColor = true;
			this.gridView1.Appearance.GroupRow.Options.UseBorderColor = true;
			this.gridView1.Appearance.GroupRow.Options.UseFont = true;
			this.gridView1.Appearance.GroupRow.Options.UseForeColor = true;
			this.gridView1.Appearance.HeaderPanel.BackColor = System.Drawing.Color.FromArgb(221, 236, 254);
			this.gridView1.Appearance.HeaderPanel.BackColor2 = System.Drawing.Color.FromArgb(132, 171, 228);
			this.gridView1.Appearance.HeaderPanel.BorderColor = System.Drawing.Color.FromArgb(221, 236, 254);
			this.gridView1.Appearance.HeaderPanel.ForeColor = System.Drawing.Color.Black;
			this.gridView1.Appearance.HeaderPanel.GradientMode = System.Drawing.Drawing2D.LinearGradientMode.Vertical;
			this.gridView1.Appearance.HeaderPanel.Options.UseBackColor = true;
			this.gridView1.Appearance.HeaderPanel.Options.UseBorderColor = true;
			this.gridView1.Appearance.HeaderPanel.Options.UseForeColor = true;
			this.gridView1.Appearance.HideSelectionRow.BackColor = System.Drawing.Color.FromArgb(106, 153, 228);
			this.gridView1.Appearance.HideSelectionRow.ForeColor = System.Drawing.Color.FromArgb(208, 224, 251);
			this.gridView1.Appearance.HideSelectionRow.Options.UseBackColor = true;
			this.gridView1.Appearance.HideSelectionRow.Options.UseForeColor = true;
			this.gridView1.Appearance.HorzLine.BackColor = System.Drawing.Color.FromArgb(99, 127, 196);
			this.gridView1.Appearance.HorzLine.Options.UseBackColor = true;
			this.gridView1.Appearance.OddRow.BackColor = System.Drawing.Color.White;
			this.gridView1.Appearance.OddRow.ForeColor = System.Drawing.Color.Black;
			this.gridView1.Appearance.OddRow.Options.UseBackColor = true;
			this.gridView1.Appearance.OddRow.Options.UseForeColor = true;
			this.gridView1.Appearance.Preview.BackColor = System.Drawing.Color.FromArgb(249, 252, 255);
			this.gridView1.Appearance.Preview.ForeColor = System.Drawing.Color.FromArgb(88, 129, 185);
			this.gridView1.Appearance.Preview.Options.UseBackColor = true;
			this.gridView1.Appearance.Preview.Options.UseForeColor = true;
			this.gridView1.Appearance.Row.BackColor = System.Drawing.Color.White;
			this.gridView1.Appearance.Row.ForeColor = System.Drawing.Color.Black;
			this.gridView1.Appearance.Row.Options.UseBackColor = true;
			this.gridView1.Appearance.Row.Options.UseForeColor = true;
			this.gridView1.Appearance.RowSeparator.BackColor = System.Drawing.Color.White;
			this.gridView1.Appearance.RowSeparator.Options.UseBackColor = true;
			this.gridView1.Appearance.SelectedRow.BackColor = System.Drawing.Color.FromArgb(69, 126, 217);
			this.gridView1.Appearance.SelectedRow.ForeColor = System.Drawing.Color.White;
			this.gridView1.Appearance.SelectedRow.Options.UseBackColor = true;
			this.gridView1.Appearance.SelectedRow.Options.UseForeColor = true;
			this.gridView1.Appearance.VertLine.BackColor = System.Drawing.Color.FromArgb(99, 127, 196);
			this.gridView1.Appearance.VertLine.Options.UseBackColor = true;
			this.gridView1.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
				this.gridColumn_Training_Title,
				this.gridColumn_TrainingDate
			});
			this.gridView1.GridControl = this.gridControl1;
			this.gridView1.Name = "gridView1";
			this.gridView1.OptionsBehavior.AllowAddRows = DevExpress.Utils.DefaultBoolean.False;
			this.gridView1.OptionsBehavior.AllowDeleteRows = DevExpress.Utils.DefaultBoolean.False;
			this.gridView1.OptionsBehavior.Editable = false;
			this.gridView1.OptionsView.ShowGroupPanel = false;
			this.gridView1.OptionsView.ShowIndicator = false;
			//
			//gridColumn_Training_Title
			//
			this.gridColumn_Training_Title.Caption = "Title";
			this.gridColumn_Training_Title.CustomizationCaption = "Course Title";
            this.gridColumn_Training_Title.FieldName = "TrainingTitle";
			this.gridColumn_Training_Title.Name = "gridColumn_Training_Title";
			this.gridColumn_Training_Title.OptionsColumn.AllowEdit = false;
			this.gridColumn_Training_Title.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.True;
			this.gridColumn_Training_Title.Visible = true;
			this.gridColumn_Training_Title.VisibleIndex = 0;
			this.gridColumn_Training_Title.Width = 233;
			//
			//gridColumn_TrainingDate
			//
			this.gridColumn_TrainingDate.Caption = "Date";
			this.gridColumn_TrainingDate.CustomizationCaption = "Date of Course";
			this.gridColumn_TrainingDate.DisplayFormat.FormatString = "d";
			this.gridColumn_TrainingDate.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
			this.gridColumn_TrainingDate.FieldName = "TrainingDate";
			this.gridColumn_TrainingDate.GroupFormat.FormatString = "d";
			this.gridColumn_TrainingDate.GroupFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
			this.gridColumn_TrainingDate.Name = "gridColumn_TrainingDate";
			this.gridColumn_TrainingDate.OptionsColumn.AllowEdit = false;
			this.gridColumn_TrainingDate.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.True;
			this.gridColumn_TrainingDate.Visible = true;
			this.gridColumn_TrainingDate.VisibleIndex = 1;
			this.gridColumn_TrainingDate.Width = 96;
			//
			//ContextMenu1
			//
			this.ContextMenu1.MenuItems.AddRange(new MenuItem[] {
				this.ContextMenu1_Edit,
				this.ContextMenu1_Create,
				this.ContextMenu1_Delete
			});
			//
			//ContextMenu1_Edit
			//
			this.ContextMenu1_Edit.DefaultItem = true;
			this.ContextMenu1_Edit.Index = 0;
			this.ContextMenu1_Edit.Text = "Edit...";
			//
			//ContextMenu1_Create
			//
			this.ContextMenu1_Create.Index = 1;
			this.ContextMenu1_Create.Text = "Create...";
			//
			//ContextMenu1_Delete
			//
			this.ContextMenu1_Delete.Index = 2;
			this.ContextMenu1_Delete.Text = "Delete...";
			//
			//TrainingListControl
			//
			this.AutoScaleDimensions = new System.Drawing.SizeF(6f, 13f);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.Controls.Add(this.gridControl1);
			this.Name = "TrainingListControl";
			this.Size = new System.Drawing.Size(409, 189);
			((System.ComponentModel.ISupportInitialize)this.gridControl1).EndInit();
			((System.ComponentModel.ISupportInitialize)this.gridView1).EndInit();
			this.ResumeLayout(false);
		}
		private DevExpress.XtraGrid.GridControl gridControl1;
		private DevExpress.XtraGrid.Views.Grid.GridView gridView1;
		private DevExpress.XtraGrid.Columns.GridColumn gridColumn_Training_Title;
		private DevExpress.XtraGrid.Columns.GridColumn gridColumn_TrainingDate;
		internal ContextMenu ContextMenu1;
		protected MenuItem ContextMenu1_Edit;
		protected MenuItem ContextMenu1_Create;

		protected MenuItem ContextMenu1_Delete;
	}
}
