#region "Copyright 2000-2012 DebtPlus, L.L.C."
//{*******************************************************************}
//{                                                                   }
//{       DebtPlus Debt Management System                             }
//{                                                                   }
//{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
//{       ALL RIGHTS RESERVED                                         }
//{                                                                   }
//{   The entire contents of this file is protected by U.S. and       }
//{   International Copyright Laws. Unauthorized reproduction,        }
//{   reverse-engineering, and distribution of all or any portion of  }
//{   the code contained in this file is strictly prohibited and may  }
//{   result in severe civil and criminal penalties and will be       }
//{   prosecuted to the maximum extent possible under the law.        }
//{                                                                   }
//{   RESTRICTIONS                                                    }
//{                                                                   }
//{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
//{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
//{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
//{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
//{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
//{                                                                   }
//{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
//{   ADDITIONAL RESTRICTIONS.                                        }
//{                                                                   }
//{*******************************************************************}

#endregion "Copyright 2000-2012 DebtPlus, L.L.C."

using System.Linq;
using System.Windows.Forms;
using DebtPlus.LINQ;

namespace DebtPlus.UI.TableAdministration.CS.Counselors.Controls
{
    internal partial class AttributeListControl : System.ComponentModel.INotifyPropertyChanged
    {
        public event System.ComponentModel.PropertyChangedEventHandler PropertyChanged;

        internal AttributeListControl()
            : base()
        {
            InitializeComponent();
        }

        /// <summary>
        /// Register the event handlers
        /// </summary>
        private void RegisterHandlers()
        {
            CheckedListBoxControl_Attributes.ItemCheck += CheckedListBoxControl_Attributes_ItemCheck;
        }

        /// <summary>
        /// Remove the event handler registrations
        /// </summary>
        private void UnRegisterHandlers()
        {
            CheckedListBoxControl_Attributes.ItemCheck -= CheckedListBoxControl_Attributes_ItemCheck;
        }

        /// <summary>
        /// Handle the change in the checkbox for the control.
        /// </summary>
        private void CheckedListBoxControl_Attributes_ItemCheck(object sender, DevExpress.XtraEditors.Controls.ItemCheckEventArgs e)
        {
            RaisePropertyChanged(null);
        }

        protected void RaisePropertyChanged(string PropertyName)
        {
            var evt = PropertyChanged;
            if (evt != null)
            {
                evt(this, new System.ComponentModel.PropertyChangedEventArgs(PropertyName));
            }
        }

        /// <summary>
        /// Process the new counselor record into the checked list of counselor attributes. The
        /// database is not updated until the counselor record is saved.
        /// </summary>
        internal virtual void ReadForm(BusinessContext bc, counselor counselorRecord)
        {
            UnRegisterHandlers();
            try
            {
                // Read the list of attributes
                CheckedListBoxControl_Attributes.Items.Clear();
                CheckedListBoxControl_Attributes.SortOrder = SortOrder.None;
                var colAttributes = DebtPlus.LINQ.Cache.AttributeType.getList();

                // Add the attributes to the control and then sort it once all attributes are loaded.
                colAttributes.ForEach(s => CheckedListBoxControl_Attributes.Items.Add(new DebtPlus.Data.Controls.SortedCheckedListboxControlItem(s, s.description, System.Windows.Forms.CheckState.Unchecked)));
                CheckedListBoxControl_Attributes.SortOrder = SortOrder.Ascending;

                // Read the list of counselor attributes
                foreach (var counselorAttribute in counselorRecord.counselor_attributes)
                {
                    // Check the corresponding attribute
                    var q = CheckedListBoxControl_Attributes.Items.OfType<DebtPlus.Data.Controls.SortedCheckedListboxControlItem>().Where(s => ((AttributeType)((DebtPlus.Data.Controls.SortedCheckedListboxControlItem)s).Value).Id == counselorAttribute.Attribute).FirstOrDefault();
                    if (q != null)
                    {
                        q.CheckState = CheckState.Checked;
                    }
                }
            }
            finally
            {
                RegisterHandlers();
            }
        }

        /// <summary>
        /// Save the data to the counselor record. The database is not updated until the counselor record is saved.
        /// </summary>
        internal virtual void SaveForm(BusinessContext bc, counselor counselorRecord)
        {
            UnRegisterHandlers();
            try
            {
                foreach (var ctlListItem in CheckedListBoxControl_Attributes.Items.OfType<DebtPlus.Data.Controls.SortedCheckedListboxControlItem>())
                {
                    // Locate the items with the proper types
                    var ctl = ctlListItem.Value as DebtPlus.LINQ.AttributeType;
                    var q = counselorRecord.counselor_attributes.Where(s => s.Attribute == ctl.Id).FirstOrDefault();

                    // Determine if the record is checked or not.
                    if (ctlListItem.CheckState == CheckState.Checked)
                    {
                        // The record is checked. Add it if it is not found.
                        if (q == null)
                        {
                            q = DebtPlus.LINQ.Factory.Manufacture_counselor_attribute(counselorRecord.Id, ctl.Id);
                            counselorRecord.counselor_attributes.Add(q);
                        }
                    }
                    else
                    {
                        // The record is not checked. Delete it if it is found.
                        if (q != null)
                        {
                            counselorRecord.counselor_attributes.Remove(q);
                            q.counselor1 = null;
                        }
                    }
                }
            }
            finally
            {
                RegisterHandlers();
            }
        }
    }
}