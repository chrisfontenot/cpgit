#region "Copyright 2000-2012 DebtPlus, L.L.C."
//{*******************************************************************}
//{                                                                   }
//{       DebtPlus Debt Management System                             }
//{                                                                   }
//{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
//{       ALL RIGHTS RESERVED                                         }
//{                                                                   }
//{   The entire contents of this file is protected by U.S. and       }
//{   International Copyright Laws. Unauthorized reproduction,        }
//{   reverse-engineering, and distribution of all or any portion of  }
//{   the code contained in this file is strictly prohibited and may  }
//{   result in severe civil and criminal penalties and will be       }
//{   prosecuted to the maximum extent possible under the law.        }
//{                                                                   }
//{   RESTRICTIONS                                                    }
//{                                                                   }
//{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
//{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
//{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
//{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
//{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
//{                                                                   }
//{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
//{   ADDITIONAL RESTRICTIONS.                                        }
//{                                                                   }
//{*******************************************************************}

#endregion "Copyright 2000-2012 DebtPlus, L.L.C."

using System;
using System.IO;
using System.Linq;
using DebtPlus.LINQ;

namespace DebtPlus.UI.TableAdministration.CS.Counselors
{
    internal partial class CounselorEditForm : Templates.EditTemplateForm, System.ComponentModel.INotifyPropertyChanged
    {
        private BusinessContext bc = null;
        private counselor counselorRecord = null;

        internal CounselorEditForm()
            : base()
        {
            InitializeComponent();
        }

        internal CounselorEditForm(BusinessContext bc, counselor counselorRecord)
            : this()
        {
            this.bc = bc;
            this.counselorRecord = counselorRecord;
            RegisterHandlers();
        }

        public event System.ComponentModel.PropertyChangedEventHandler PropertyChanged;

        protected void RaisePropertyChanged(string PropertyName)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new System.ComponentModel.PropertyChangedEventArgs(PropertyName));
            }
        }

        private void RegisterHandlers()
        {
            Load += Form_Load;
            CheckEdit_ActiveFlag.CheckedChanged += CheckEdit_ActiveFlag_CheckedChanged;
            LookUpEdit_office.EditValueChanging += DebtPlus.Data.Validation.LookUpEdit_ActiveTest;
            LookUpEdit_billing_method.EditValueChanging += DebtPlus.Data.Validation.LookUpEdit_ActiveTest;

            EmailRecordControl1.TextChanged += Form_Changed;
            NameRecordControl1.TextChanged += Form_Changed;
            TelephoneNumberRecordControl1.TextChanged += Form_Changed;
            TextEditNote.EditValueChanged += Form_Changed;
            ColorEdit1.EditValueChanged += Form_Changed;
            TextEdit_menu_level.EditValueChanged += Form_Changed;
            CheckEdit_ActiveFlag.EditValueChanged += Form_Changed;
            CheckEdit_default.EditValueChanged += Form_Changed;
            TextEdit_person.EditValueChanged += Form_Changed;
            LookUpEdit_office.EditValueChanged += Form_Changed;
            LookUpEdit_billing_method.EditValueChanged += Form_Changed;
            lookUpEdit_Manager.EditValueChanged += Form_Changed;
            lookUpEdit_Manager.EditValueChanging += DebtPlus.Data.Validation.LookUpEdit_ActiveTest;
            DateEdit_emp_end_date.EditValueChanged += Form_Changed;
            DateEdit_emp_start_date.EditValueChanged += Form_Changed;
            CalcEdit_hourly_rate.EditValueChanged += Form_Changed;
            Ssn1.EditValueChanged += Form_Changed;
            TextEdit_HUD_id.EditValueChanged += Form_Changed;
            CounselorAttributes1.PropertyChanged += Form_Changed;
            HousingAttributeListControl1.PropertyChanged += Form_Changed;
        }

        private void UnRegisterHandlers()
        {
            Load -= Form_Load;
            CheckEdit_ActiveFlag.CheckedChanged -= CheckEdit_ActiveFlag_CheckedChanged;
            LookUpEdit_office.EditValueChanging -= DebtPlus.Data.Validation.LookUpEdit_ActiveTest;
            LookUpEdit_billing_method.EditValueChanging -= DebtPlus.Data.Validation.LookUpEdit_ActiveTest;

            EmailRecordControl1.TextChanged -= Form_Changed;
            NameRecordControl1.TextChanged -= Form_Changed;
            TelephoneNumberRecordControl1.TextChanged -= Form_Changed;
            TextEditNote.EditValueChanged -= Form_Changed;
            ColorEdit1.EditValueChanged -= Form_Changed;
            TextEdit_menu_level.EditValueChanged -= Form_Changed;
            CheckEdit_ActiveFlag.EditValueChanged -= Form_Changed;
            CheckEdit_default.EditValueChanged -= Form_Changed;
            TextEdit_person.EditValueChanged -= Form_Changed;
            LookUpEdit_office.EditValueChanged -= Form_Changed;
            LookUpEdit_billing_method.EditValueChanged -= Form_Changed;
            lookUpEdit_Manager.EditValueChanged -= Form_Changed;
            lookUpEdit_Manager.EditValueChanging -= DebtPlus.Data.Validation.LookUpEdit_ActiveTest;
            DateEdit_emp_end_date.EditValueChanged -= Form_Changed;
            DateEdit_emp_start_date.EditValueChanged -= Form_Changed;
            CalcEdit_hourly_rate.EditValueChanged -= Form_Changed;
            Ssn1.EditValueChanged -= Form_Changed;
            TextEdit_HUD_id.EditValueChanged -= Form_Changed;
            CounselorAttributes1.PropertyChanged -= Form_Changed;
            HousingAttributeListControl1.PropertyChanged -= Form_Changed;
        }

        private void Form_Load(object sender, EventArgs e)
        {
            UnRegisterHandlers();
            try
            {
                // Restore the placement and sizing information
                LoadPlacement("Tables.Update.Counselors.EditForm");

                // Load the lookup controls
                lookUpEdit_Manager.Properties.DataSource = DebtPlus.LINQ.Cache.counselor.getList();
                LookUpEdit_office.Properties.DataSource = DebtPlus.LINQ.Cache.office.getList();
                LookUpEdit_billing_method.Properties.DataSource = DebtPlus.LINQ.InMemory.counselorBillingTypes.getList();

                // Define the values into the controls
                LabelControl_ID.Text = counselorRecord.Id < 1 ? "NEW" : counselorRecord.Id.ToString();
                EmailRecordControl1.EditValue = counselorRecord.EmailID;
                NameRecordControl1.EditValue = counselorRecord.NameID;
                TelephoneNumberRecordControl1.EditValue = counselorRecord.TelephoneID;
                TextEditNote.EditValue = counselorRecord.Note;
                ColorEdit1.EditValue = counselorRecord.Color;
                TextEdit_menu_level.EditValue = counselorRecord.Menu_Level;
                CheckEdit_ActiveFlag.Checked = counselorRecord.ActiveFlag;
                CheckEdit_default.Checked = counselorRecord.Default;
                TextEdit_person.EditValue = counselorRecord.Person;
                LookUpEdit_office.EditValue = counselorRecord.Office;
                LookUpEdit_billing_method.EditValue = counselorRecord.billing_mode;
                DateEdit_emp_start_date.EditValue = counselorRecord.emp_start_date;
                DateEdit_emp_end_date.EditValue = counselorRecord.emp_end_date;
                CalcEdit_hourly_rate.EditValue = counselorRecord.rate;
                Ssn1.EditValue = counselorRecord.SSN;
                TextEdit_HUD_id.EditValue = counselorRecord.HUD_id;
                lookUpEdit_Manager.EditValue = counselorRecord.Manager;

                // Load the other sub-controls from the counselor record
                CounselorAttributes1.ReadForm(bc, counselorRecord);
                HousingAttributeListControl1.ReadForm(bc, counselorRecord);
                TrainingListControl1.ReadForm(bc, counselorRecord);

                // Enable or disable the OK button accordingly
                simpleButton_OK.Enabled = !HasErrors();

                // Ensure that the first page is selected, event if it was not previously.
                if (TabbedControlGroup1.TabPages.Count > 0)
                {
                    TabbedControlGroup1.SelectedTabPageIndex = 0;
                }
            }
            finally
            {
                RegisterHandlers();
            }
        }

        protected override void simpleButton_OK_Click(object sender, EventArgs e)
        {
            base.simpleButton_OK_Click(sender, e);

            // Save the sub-controls
            CounselorAttributes1.SaveForm(bc, counselorRecord);
            HousingAttributeListControl1.SaveForm(bc, counselorRecord);

            // These can be records but they are not so we can just store the ID field.
            counselorRecord.EmailID = DebtPlus.Utils.Nulls.v_Int32(EmailRecordControl1.EditValue);
            counselorRecord.Manager = DebtPlus.Utils.Nulls.v_Int32(lookUpEdit_Manager.EditValue);

            // Set the name. It will set the ID field correctly.
            // If the value returned is NULL then, by definition, it came in as NULL so we don't need to
            // change the value. We only need to set the non-null value.
            System.Nullable<Int32> NameID = DebtPlus.Utils.Nulls.v_Int32(NameRecordControl1.EditValue);
            if (NameID != null)
            {
                counselorRecord.Name = bc.Names.Where(s => s.Id == NameID.Value).FirstOrDefault();
            }

            // Set the telephone number. It will set the ID field correctly.
            // If the value returned is NULL then, by definition, it came in as NULL so we don't need to
            // change the value. We only need to set the non-null value.
            System.Nullable<Int32> TelephoneID = DebtPlus.Utils.Nulls.v_Int32(TelephoneNumberRecordControl1.EditValue);
            if (TelephoneID != null)
            {
                counselorRecord.TelephoneNumber = bc.TelephoneNumbers.Where(s => s.Id == TelephoneID.Value).FirstOrDefault();
            }

            // Load the other values from the controls into the record
            counselorRecord.Note = DebtPlus.Utils.Nulls.v_String(TextEditNote.EditValue);
            counselorRecord.Color = DebtPlus.Utils.Nulls.v_Int32(ColorEdit1.EditValue);
            counselorRecord.Menu_Level = DebtPlus.Utils.Nulls.v_Int32(TextEdit_menu_level.EditValue).GetValueOrDefault(9);
            counselorRecord.ActiveFlag = CheckEdit_ActiveFlag.Checked;
            counselorRecord.Default = CheckEdit_default.Checked;
            counselorRecord.Person = DebtPlus.Utils.Nulls.v_String(TextEdit_person.EditValue);
            counselorRecord.Office = DebtPlus.Utils.Nulls.v_Int32(LookUpEdit_office.EditValue).GetValueOrDefault();
            counselorRecord.billing_mode = DebtPlus.Utils.Nulls.v_String(LookUpEdit_billing_method.EditValue);
            counselorRecord.emp_start_date = DebtPlus.Utils.Nulls.v_DateTime(DateEdit_emp_start_date.EditValue).GetValueOrDefault();
            counselorRecord.emp_end_date = DebtPlus.Utils.Nulls.v_DateTime(DateEdit_emp_end_date.EditValue);
            counselorRecord.rate = DebtPlus.Utils.Nulls.v_Decimal(CalcEdit_hourly_rate.EditValue);
            counselorRecord.SSN = DebtPlus.Utils.Nulls.v_String(Ssn1.EditValue);
            counselorRecord.HUD_id = DebtPlus.Utils.Nulls.v_Int32(TextEdit_HUD_id.EditValue);
        }

        private void Form_Changed(object sender, EventArgs e)
        {
            simpleButton_OK.Enabled = !HasErrors();
        }

        private bool HasErrors()
        {
            if (string.IsNullOrWhiteSpace(NameRecordControl1.GetCurrentValues().ToString()))
            {
                return true;
            }

            if (!DebtPlus.Utils.Nulls.v_Int32(LookUpEdit_office.EditValue).HasValue)
            {
                return true;
            }

            if (string.IsNullOrWhiteSpace(DebtPlus.Utils.Nulls.v_String(TextEdit_person.EditValue)))
            {
                return true;
            }

            if (!DebtPlus.Utils.Nulls.v_Int32(TextEdit_menu_level.EditValue).HasValue)
            {
                return true;
            }

            return false;
        }

        /// <summary>
        /// Return the directory to the saved layout information
        /// </summary>
        protected virtual string XMLBasePath()
        {
            return System.IO.Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData), "DebtPlus", "Tables.Counselors");
        }

        private string FileName()
        {
            return Path.Combine(XMLBasePath(), Name + ".Layout.xml");
        }

        /// <summary>
        /// Set the ending date to today if the counselor is no longer active
        /// </summary>
        private void CheckEdit_ActiveFlag_CheckedChanged(object sender, EventArgs e)
        {
            if (!CheckEdit_ActiveFlag.Checked)
            {
                if (!DebtPlus.Utils.Nulls.v_DateTime(DateEdit_emp_end_date.EditValue).HasValue)
                {
                    DateEdit_emp_end_date.EditValue = System.DateTime.Now.Date;
                }
            }
        }
    }
}