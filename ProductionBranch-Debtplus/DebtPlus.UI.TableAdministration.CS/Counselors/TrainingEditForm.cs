#region "Copyright 2000-2012 DebtPlus, L.L.C."
//{*******************************************************************}
//{                                                                   }
//{       DebtPlus Debt Management System                             }
//{                                                                   }
//{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
//{       ALL RIGHTS RESERVED                                         }
//{                                                                   }
//{   The entire contents of this file is protected by U.S. and       }
//{   International Copyright Laws. Unauthorized reproduction,        }
//{   reverse-engineering, and distribution of all or any portion of  }
//{   the code contained in this file is strictly prohibited and may  }
//{   result in severe civil and criminal penalties and will be       }
//{   prosecuted to the maximum extent possible under the law.        }
//{                                                                   }
//{   RESTRICTIONS                                                    }
//{                                                                   }
//{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
//{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
//{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
//{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
//{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
//{                                                                   }
//{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
//{   ADDITIONAL RESTRICTIONS.                                        }
//{                                                                   }
//{*******************************************************************}

#endregion "Copyright 2000-2012 DebtPlus, L.L.C."

using System;
using System.Windows.Forms;
using DebtPlus.LINQ;

namespace DebtPlus.UI.TableAdministration.CS.Counselors
{
    internal partial class TrainingEditForm : DebtPlus.Data.Forms.DebtPlusForm, System.ComponentModel.INotifyPropertyChanged
    {
        // Local storage
        private BusinessContext bc = null;

        private counselor_training record = null;

        /// <summary>
        /// Event raised when the controls are changed
        /// </summary>
        public event System.ComponentModel.PropertyChangedEventHandler PropertyChanged;

        /// <summary>
        /// Local routine to raise the PropertyChanged event
        /// </summary>
        /// <param name="PropertyName"></param>
        protected void RaisePropertyChanged(string PropertyName)
        {
            var evt = PropertyChanged;
            if (evt != null)
            {
                evt(this, new System.ComponentModel.PropertyChangedEventArgs(PropertyName));
            }
        }

        /// <summary>
        /// Initialize the new form class
        /// </summary>
        internal TrainingEditForm()
            : base()
        {
            InitializeComponent();
        }

        /// <summary>
        /// Initialize the new form class
        /// </summary>
        /// <param name="bc"></param>
        /// <param name="record"></param>
        internal TrainingEditForm(BusinessContext bc, counselor_training record)
            : this()
        {
            this.bc = bc;
            this.record = record;
            RegisterHandlers();
        }

        /// <summary>
        /// Register the event handlers
        /// </summary>
        private void RegisterHandlers()
        {
            LookupEdit_TrainingCourse.EditValueChanged += Form_Changed;
            DateEdit_TrainingDate.EditValueChanged += Form_Changed;
            Load += Form_Load;
            SimpleButtonOK.Click += SimpleButtonOK_Click;
        }

        /// <summary>
        /// Remove the event handler registrations
        /// </summary>
        private void UnRegisterHandlers()
        {
            LookupEdit_TrainingCourse.EditValueChanged -= Form_Changed;
            DateEdit_TrainingDate.EditValueChanged -= Form_Changed;
            Load -= Form_Load;
            SimpleButtonOK.Click -= SimpleButtonOK_Click;
        }

        /// <summary>
        /// Process the form LOAD event
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Form_Load(object sender, EventArgs e)
        {
            UnRegisterHandlers();
            try
            {
                // List of the training course types
                LookupEdit_TrainingCourse.Properties.DataSource = DebtPlus.LINQ.Cache.Housing_training_course.getList();
                LookupEdit_TrainingCourse.EditValue = DebtPlus.LINQ.Cache.Housing_training_course.getList().Find(s => string.Compare(s.TrainingTitle, record.TrainingTitle, true) == 0);

                // Set the date of the course event
                DateEdit_TrainingDate.EditValue = record.TrainingDate;
            }
            finally
            {
                RegisterHandlers();
            }

            SimpleButtonOK.Enabled = !HasErrors();
        }

        /// <summary>
        /// Process a change in the form
        /// </summary>
        /// <param name="Sender"></param>
        /// <param name="e"></param>
        private void Form_Changed(object Sender, EventArgs e)
        {
            RaisePropertyChanged(((Control)Sender).Name);
            SimpleButtonOK.Enabled = !HasErrors();
        }

        /// <summary>
        /// Determine if the input controls contain errors
        /// </summary>
        /// <returns></returns>
        private bool HasErrors()
        {
            if (LookupEdit_TrainingCourse.EditValue == null)
            {
                return true;
            }

            if (!DebtPlus.Utils.Nulls.v_DateTime(DateEdit_TrainingDate.EditValue).HasValue)
            {
                return true;
            }

            return false;
        }

        /// <summary>
        /// Process the CLICK event on the OK button
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void SimpleButtonOK_Click(object sender, EventArgs e)
        {
            record.TrainingDate = DebtPlus.Utils.Nulls.v_DateTime(DateEdit_TrainingDate.EditValue).GetValueOrDefault();
            var q = LookupEdit_TrainingCourse.EditValue as Housing_training_course;
            record.TrainingTitle = q.TrainingTitle;
        }
    }
}