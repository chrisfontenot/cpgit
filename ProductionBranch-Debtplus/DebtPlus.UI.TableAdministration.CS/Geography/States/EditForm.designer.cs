﻿namespace DebtPlus.UI.TableAdministration.CS.Geography.States
{
    partial class EditForm
    {
		//Form overrides dispose to clean up the component list.
		[System.Diagnostics.DebuggerNonUserCode]
		protected override void Dispose(bool disposing)
		{
			try 
            {
				if (disposing)
                {
                    if (components != null) components.Dispose();
				}
                components = null;
            }
    	    finally
            {
				base.Dispose(disposing);
			}
		}

		//Required by the Windows Form Designer
		private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            DevExpress.Utils.SuperToolTip superToolTip1 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem1 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem1 = new DevExpress.Utils.ToolTipItem();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(EditForm));
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem2 = new DevExpress.Utils.ToolTipTitleItem();
            this.LayoutControl1 = new DevExpress.XtraLayout.LayoutControl();
            this.CheckEdit_NegativeDebtWarning = new DevExpress.XtraEditors.CheckEdit();
            this.LookUpEdit_TimeZoneID = new DevExpress.XtraEditors.LookUpEdit();
            this.CheckEdit_default = new DevExpress.XtraEditors.CheckEdit();
            this.TextEdit_hud_9902 = new DevExpress.XtraEditors.TextEdit();
            this.CheckEdit_DoingBusiness = new DevExpress.XtraEditors.CheckEdit();
            this.CheckEdit_ActiveFlag = new DevExpress.XtraEditors.CheckEdit();
            this.TextEdit_name = new DevExpress.XtraEditors.TextEdit();
            this.MemoExEdit_AddressFormat = new DevExpress.XtraEditors.MemoExEdit();
            this.LabelControl_state = new DevExpress.XtraEditors.LabelControl();
            this.TextEdit_mailing_code = new DevExpress.XtraEditors.TextEdit();
            this.LookUpEdit_country = new DevExpress.XtraEditors.LookUpEdit();
            this.CheckEdit_USAFormat = new DevExpress.XtraEditors.CheckEdit();
            this.LayoutControlGroup1 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.LayoutControlItem3 = new DevExpress.XtraLayout.LayoutControlItem();
            this.LayoutControlItem7 = new DevExpress.XtraLayout.LayoutControlItem();
            this.LayoutControlItem8 = new DevExpress.XtraLayout.LayoutControlItem();
            this.LayoutControlItem9 = new DevExpress.XtraLayout.LayoutControlItem();
            this.LayoutControlItem10 = new DevExpress.XtraLayout.LayoutControlItem();
            this.LayoutControlItem6 = new DevExpress.XtraLayout.LayoutControlItem();
            this.LayoutControlItem2 = new DevExpress.XtraLayout.LayoutControlItem();
            this.LayoutControlItem5 = new DevExpress.XtraLayout.LayoutControlItem();
            this.EmptySpaceItem1 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.LayoutControlItem11 = new DevExpress.XtraLayout.LayoutControlItem();
            this.LayoutControlItem1 = new DevExpress.XtraLayout.LayoutControlItem();
            this.LayoutControlItem4 = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem2 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.layoutControlItem12 = new DevExpress.XtraLayout.LayoutControlItem();
            ((System.ComponentModel.ISupportInitialize)(this.DxErrorProvider1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LayoutControl1)).BeginInit();
            this.LayoutControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.CheckEdit_NegativeDebtWarning.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LookUpEdit_TimeZoneID.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.CheckEdit_default.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TextEdit_hud_9902.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.CheckEdit_DoingBusiness.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.CheckEdit_ActiveFlag.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TextEdit_name.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.MemoExEdit_AddressFormat.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TextEdit_mailing_code.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LookUpEdit_country.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.CheckEdit_USAFormat.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LayoutControlGroup1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LayoutControlItem3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LayoutControlItem7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LayoutControlItem8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LayoutControlItem9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LayoutControlItem10)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LayoutControlItem6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LayoutControlItem2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LayoutControlItem5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.EmptySpaceItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LayoutControlItem11)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LayoutControlItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LayoutControlItem4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem12)).BeginInit();
            this.SuspendLayout();
            // 
            // simpleButton_Cancel
            // 
            this.simpleButton_Cancel.Location = new System.Drawing.Point(339, 67);
            // 
            // simpleButton_OK
            // 
            this.simpleButton_OK.Location = new System.Drawing.Point(339, 35);
            // 
            // DefaultLookAndFeel1
            // 
            this.DefaultLookAndFeel1.LookAndFeel.SkinName = "McSkin";
            // 
            // LayoutControl1
            // 
            this.LayoutControl1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.LayoutControl1.Controls.Add(this.CheckEdit_NegativeDebtWarning);
            this.LayoutControl1.Controls.Add(this.LookUpEdit_TimeZoneID);
            this.LayoutControl1.Controls.Add(this.CheckEdit_default);
            this.LayoutControl1.Controls.Add(this.TextEdit_hud_9902);
            this.LayoutControl1.Controls.Add(this.CheckEdit_DoingBusiness);
            this.LayoutControl1.Controls.Add(this.CheckEdit_ActiveFlag);
            this.LayoutControl1.Controls.Add(this.TextEdit_name);
            this.LayoutControl1.Controls.Add(this.MemoExEdit_AddressFormat);
            this.LayoutControl1.Controls.Add(this.LabelControl_state);
            this.LayoutControl1.Controls.Add(this.TextEdit_mailing_code);
            this.LayoutControl1.Controls.Add(this.LookUpEdit_country);
            this.LayoutControl1.Controls.Add(this.CheckEdit_USAFormat);
            this.LayoutControl1.Location = new System.Drawing.Point(0, 0);
            this.LayoutControl1.Name = "LayoutControl1";
            this.LayoutControl1.Root = this.LayoutControlGroup1;
            this.LayoutControl1.Size = new System.Drawing.Size(333, 239);
            this.LayoutControl1.TabIndex = 22;
            this.LayoutControl1.Text = "LayoutControl1";
            // 
            // CheckEdit_NegativeDebtWarning
            // 
            this.CheckEdit_NegativeDebtWarning.Location = new System.Drawing.Point(12, 207);
            this.CheckEdit_NegativeDebtWarning.Name = "CheckEdit_NegativeDebtWarning";
            this.CheckEdit_NegativeDebtWarning.Properties.Caption = "Negative Budget Warning";
            this.CheckEdit_NegativeDebtWarning.Size = new System.Drawing.Size(152, 20);
            this.CheckEdit_NegativeDebtWarning.StyleController = this.LayoutControl1;
            this.CheckEdit_NegativeDebtWarning.TabIndex = 23;
            this.CheckEdit_NegativeDebtWarning.ToolTip = "Is negative budget warning required for this state?";
            // 
            // LookUpEdit_TimeZoneID
            // 
            this.LookUpEdit_TimeZoneID.Location = new System.Drawing.Point(91, 53);
            this.LookUpEdit_TimeZoneID.Name = "LookUpEdit_TimeZoneID";
            this.LookUpEdit_TimeZoneID.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.False;
            this.LookUpEdit_TimeZoneID.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LookUpEdit_TimeZoneID.Properties.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("Id", "ID", 20, DevExpress.Utils.FormatType.Numeric, "f0", false, DevExpress.Utils.HorzAlignment.Default),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("Description", "Description", 20, DevExpress.Utils.FormatType.None, "", true, DevExpress.Utils.HorzAlignment.Default, DevExpress.Data.ColumnSortOrder.Ascending)});
            this.LookUpEdit_TimeZoneID.Properties.DisplayMember = "Description";
            this.LookUpEdit_TimeZoneID.Properties.NullText = "";
            this.LookUpEdit_TimeZoneID.Properties.ShowFooter = false;
            this.LookUpEdit_TimeZoneID.Properties.ShowHeader = false;
            this.LookUpEdit_TimeZoneID.Properties.SortColumnIndex = 1;
            this.LookUpEdit_TimeZoneID.Properties.ValueMember = "Id";
            this.LookUpEdit_TimeZoneID.Size = new System.Drawing.Size(230, 20);
            this.LookUpEdit_TimeZoneID.StyleController = this.LayoutControl1;
            this.LookUpEdit_TimeZoneID.TabIndex = 16;
            // 
            // CheckEdit_default
            // 
            this.CheckEdit_default.Location = new System.Drawing.Point(12, 183);
            this.CheckEdit_default.Name = "CheckEdit_default";
            this.CheckEdit_default.Properties.Caption = "Default State";
            this.CheckEdit_default.Size = new System.Drawing.Size(152, 20);
            this.CheckEdit_default.StyleController = this.LayoutControl1;
            this.CheckEdit_default.TabIndex = 12;
            this.CheckEdit_default.ToolTip = "Is this the default state when creating a new address";
            // 
            // TextEdit_hud_9902
            // 
            this.TextEdit_hud_9902.Location = new System.Drawing.Point(91, 125);
            this.TextEdit_hud_9902.Name = "TextEdit_hud_9902";
            this.TextEdit_hud_9902.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.True;
            this.TextEdit_hud_9902.Properties.Appearance.Options.UseTextOptions = true;
            this.TextEdit_hud_9902.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.TextEdit_hud_9902.Properties.DisplayFormat.FormatString = "{0:f0}";
            this.TextEdit_hud_9902.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.TextEdit_hud_9902.Properties.EditFormat.FormatString = "{0:f0}";
            this.TextEdit_hud_9902.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.TextEdit_hud_9902.Properties.Mask.BeepOnError = true;
            this.TextEdit_hud_9902.Properties.Mask.EditMask = "f0";
            this.TextEdit_hud_9902.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.TextEdit_hud_9902.Properties.MaxLength = 3;
            this.TextEdit_hud_9902.Size = new System.Drawing.Size(66, 20);
            this.TextEdit_hud_9902.StyleController = this.LayoutControl1;
            this.TextEdit_hud_9902.TabIndex = 7;
            this.TextEdit_hud_9902.ToolTip = "Enter the numeric value used by HUD. 61 is the default, meaning \"not specified\". " +
    "Use this for all out-of-country values.";
            // 
            // CheckEdit_DoingBusiness
            // 
            this.CheckEdit_DoingBusiness.Location = new System.Drawing.Point(168, 183);
            this.CheckEdit_DoingBusiness.Name = "CheckEdit_DoingBusiness";
            this.CheckEdit_DoingBusiness.Properties.Caption = "Doing Business here";
            this.CheckEdit_DoingBusiness.Size = new System.Drawing.Size(153, 20);
            this.CheckEdit_DoingBusiness.StyleController = this.LayoutControl1;
            this.CheckEdit_DoingBusiness.TabIndex = 13;
            this.CheckEdit_DoingBusiness.ToolTip = "Are you authroized to do business in this state?";
            // 
            // CheckEdit_ActiveFlag
            // 
            this.CheckEdit_ActiveFlag.Location = new System.Drawing.Point(12, 159);
            this.CheckEdit_ActiveFlag.Name = "CheckEdit_ActiveFlag";
            this.CheckEdit_ActiveFlag.Properties.Caption = "Active Entry";
            this.CheckEdit_ActiveFlag.Size = new System.Drawing.Size(152, 20);
            this.CheckEdit_ActiveFlag.StyleController = this.LayoutControl1;
            this.CheckEdit_ActiveFlag.TabIndex = 14;
            this.CheckEdit_ActiveFlag.ToolTip = "Allow this record to be selected from a list of states";
            // 
            // TextEdit_name
            // 
            this.TextEdit_name.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.TextEdit_name.Location = new System.Drawing.Point(91, 29);
            this.TextEdit_name.Name = "TextEdit_name";
            this.TextEdit_name.Size = new System.Drawing.Size(230, 20);
            this.TextEdit_name.StyleController = this.LayoutControl1;
            this.TextEdit_name.TabIndex = 3;
            this.TextEdit_name.ToolTip = "This is the state name used when a state is to be selected from a list";
            // 
            // MemoExEdit_AddressFormat
            // 
            this.MemoExEdit_AddressFormat.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.MemoExEdit_AddressFormat.Location = new System.Drawing.Point(91, 101);
            this.MemoExEdit_AddressFormat.Name = "MemoExEdit_AddressFormat";
            this.MemoExEdit_AddressFormat.Properties.AcceptsTab = false;
            this.MemoExEdit_AddressFormat.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.MemoExEdit_AddressFormat.Properties.MaxLength = 256;
            this.MemoExEdit_AddressFormat.Properties.ShowIcon = false;
            this.MemoExEdit_AddressFormat.Properties.WordWrap = false;
            this.MemoExEdit_AddressFormat.Size = new System.Drawing.Size(230, 20);
            this.MemoExEdit_AddressFormat.StyleController = this.LayoutControl1;
            toolTipTitleItem1.Text = "Last Address Line Formatting";
            toolTipItem1.LeftIndent = 6;
            toolTipItem1.Text = resources.GetString("toolTipItem1.Text");
            toolTipTitleItem2.LeftIndent = 6;
            toolTipTitleItem2.Text = "\r\nDefault value is {0} {1}  {2}";
            superToolTip1.Items.Add(toolTipTitleItem1);
            superToolTip1.Items.Add(toolTipItem1);
            superToolTip1.Items.Add(toolTipTitleItem2);
            this.MemoExEdit_AddressFormat.SuperTip = superToolTip1;
            this.MemoExEdit_AddressFormat.TabIndex = 11;
            this.MemoExEdit_AddressFormat.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            // 
            // LabelControl_state
            // 
            this.LabelControl_state.Location = new System.Drawing.Point(91, 12);
            this.LabelControl_state.Name = "LabelControl_state";
            this.LabelControl_state.Size = new System.Drawing.Size(23, 13);
            this.LabelControl_state.StyleController = this.LayoutControl1;
            this.LabelControl_state.TabIndex = 1;
            this.LabelControl_state.Text = "NEW";
            // 
            // TextEdit_mailing_code
            // 
            this.TextEdit_mailing_code.Location = new System.Drawing.Point(240, 125);
            this.TextEdit_mailing_code.Name = "TextEdit_mailing_code";
            this.TextEdit_mailing_code.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.True;
            this.TextEdit_mailing_code.Properties.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.TextEdit_mailing_code.Properties.MaxLength = 4;
            this.TextEdit_mailing_code.Size = new System.Drawing.Size(81, 20);
            this.TextEdit_mailing_code.StyleController = this.LayoutControl1;
            this.TextEdit_mailing_code.TabIndex = 5;
            this.TextEdit_mailing_code.ToolTip = "Enter the state mailing code, i.e. \"CA\" for \"CALIFORNIA\"";
            // 
            // LookUpEdit_country
            // 
            this.LookUpEdit_country.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.LookUpEdit_country.Location = new System.Drawing.Point(91, 77);
            this.LookUpEdit_country.Name = "LookUpEdit_country";
            this.LookUpEdit_country.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.False;
            this.LookUpEdit_country.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LookUpEdit_country.Properties.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("Id", "ID", 20, DevExpress.Utils.FormatType.Numeric, "f0", true, DevExpress.Utils.HorzAlignment.Default),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("description", "Description", 20, DevExpress.Utils.FormatType.None, "", true, DevExpress.Utils.HorzAlignment.Default, DevExpress.Data.ColumnSortOrder.Ascending)});
            this.LookUpEdit_country.Properties.DisplayMember = "description";
            this.LookUpEdit_country.Properties.NullText = "";
            this.LookUpEdit_country.Properties.ValueMember = "Id";
            this.LookUpEdit_country.Size = new System.Drawing.Size(230, 20);
            this.LookUpEdit_country.StyleController = this.LayoutControl1;
            this.LookUpEdit_country.TabIndex = 9;
            this.LookUpEdit_country.ToolTip = "Please choose the apporpriate country from the list";
            // 
            // CheckEdit_USAFormat
            // 
            this.CheckEdit_USAFormat.Location = new System.Drawing.Point(168, 159);
            this.CheckEdit_USAFormat.Name = "CheckEdit_USAFormat";
            this.CheckEdit_USAFormat.Properties.Caption = "U.S.A. Zipcode Format";
            this.CheckEdit_USAFormat.Properties.ValueChecked = 1;
            this.CheckEdit_USAFormat.Properties.ValueUnchecked = 0;
            this.CheckEdit_USAFormat.Size = new System.Drawing.Size(153, 20);
            this.CheckEdit_USAFormat.StyleController = this.LayoutControl1;
            this.CheckEdit_USAFormat.TabIndex = 15;
            this.CheckEdit_USAFormat.ToolTip = "Is the PostalCode to be formatted as a USA Zip or ZIP+ code";
            // 
            // LayoutControlGroup1
            // 
            this.LayoutControlGroup1.CustomizationFormText = "LayoutControlGroup1";
            this.LayoutControlGroup1.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.True;
            this.LayoutControlGroup1.GroupBordersVisible = false;
            this.LayoutControlGroup1.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.LayoutControlItem3,
            this.LayoutControlItem7,
            this.LayoutControlItem8,
            this.LayoutControlItem9,
            this.LayoutControlItem10,
            this.LayoutControlItem6,
            this.LayoutControlItem2,
            this.LayoutControlItem5,
            this.EmptySpaceItem1,
            this.LayoutControlItem11,
            this.LayoutControlItem1,
            this.LayoutControlItem4,
            this.emptySpaceItem2,
            this.layoutControlItem12});
            this.LayoutControlGroup1.Location = new System.Drawing.Point(0, 0);
            this.LayoutControlGroup1.Name = "LayoutControlGroup1";
            this.LayoutControlGroup1.Size = new System.Drawing.Size(333, 239);
            this.LayoutControlGroup1.Text = "LayoutControlGroup1";
            this.LayoutControlGroup1.TextVisible = false;
            // 
            // LayoutControlItem3
            // 
            this.LayoutControlItem3.Control = this.CheckEdit_ActiveFlag;
            this.LayoutControlItem3.CustomizationFormText = "LayoutControlItem3";
            this.LayoutControlItem3.Location = new System.Drawing.Point(0, 147);
            this.LayoutControlItem3.Name = "LayoutControlItem3";
            this.LayoutControlItem3.Size = new System.Drawing.Size(156, 24);
            this.LayoutControlItem3.Text = "LayoutControlItem3";
            this.LayoutControlItem3.TextSize = new System.Drawing.Size(0, 0);
            this.LayoutControlItem3.TextToControlDistance = 0;
            this.LayoutControlItem3.TextVisible = false;
            // 
            // LayoutControlItem7
            // 
            this.LayoutControlItem7.Control = this.TextEdit_mailing_code;
            this.LayoutControlItem7.CustomizationFormText = "Mailing Code";
            this.LayoutControlItem7.Location = new System.Drawing.Point(149, 113);
            this.LayoutControlItem7.Name = "LayoutControlItem7";
            this.LayoutControlItem7.Size = new System.Drawing.Size(164, 24);
            this.LayoutControlItem7.Text = "Mailing Code";
            this.LayoutControlItem7.TextSize = new System.Drawing.Size(76, 13);
            // 
            // LayoutControlItem8
            // 
            this.LayoutControlItem8.Control = this.TextEdit_hud_9902;
            this.LayoutControlItem8.CustomizationFormText = "HUD ID#";
            this.LayoutControlItem8.Location = new System.Drawing.Point(0, 113);
            this.LayoutControlItem8.Name = "LayoutControlItem8";
            this.LayoutControlItem8.Size = new System.Drawing.Size(149, 24);
            this.LayoutControlItem8.Text = "HUD ID#";
            this.LayoutControlItem8.TextSize = new System.Drawing.Size(76, 13);
            // 
            // LayoutControlItem9
            // 
            this.LayoutControlItem9.Control = this.TextEdit_name;
            this.LayoutControlItem9.CustomizationFormText = "Name";
            this.LayoutControlItem9.Location = new System.Drawing.Point(0, 17);
            this.LayoutControlItem9.Name = "LayoutControlItem9";
            this.LayoutControlItem9.Size = new System.Drawing.Size(313, 24);
            this.LayoutControlItem9.Text = "Name";
            this.LayoutControlItem9.TextSize = new System.Drawing.Size(76, 13);
            // 
            // LayoutControlItem10
            // 
            this.LayoutControlItem10.Control = this.LabelControl_state;
            this.LayoutControlItem10.CustomizationFormText = "Record ID";
            this.LayoutControlItem10.Location = new System.Drawing.Point(0, 0);
            this.LayoutControlItem10.Name = "LayoutControlItem10";
            this.LayoutControlItem10.Size = new System.Drawing.Size(313, 17);
            this.LayoutControlItem10.Text = "Record ID";
            this.LayoutControlItem10.TextSize = new System.Drawing.Size(76, 13);
            // 
            // LayoutControlItem6
            // 
            this.LayoutControlItem6.Control = this.LookUpEdit_country;
            this.LayoutControlItem6.CustomizationFormText = "Country";
            this.LayoutControlItem6.Location = new System.Drawing.Point(0, 65);
            this.LayoutControlItem6.Name = "LayoutControlItem6";
            this.LayoutControlItem6.Size = new System.Drawing.Size(313, 24);
            this.LayoutControlItem6.Text = "Country";
            this.LayoutControlItem6.TextSize = new System.Drawing.Size(76, 13);
            // 
            // LayoutControlItem2
            // 
            this.LayoutControlItem2.Control = this.CheckEdit_DoingBusiness;
            this.LayoutControlItem2.CustomizationFormText = "LayoutControlItem2";
            this.LayoutControlItem2.Location = new System.Drawing.Point(156, 171);
            this.LayoutControlItem2.Name = "LayoutControlItem2";
            this.LayoutControlItem2.Size = new System.Drawing.Size(157, 24);
            this.LayoutControlItem2.Text = "LayoutControlItem2";
            this.LayoutControlItem2.TextSize = new System.Drawing.Size(0, 0);
            this.LayoutControlItem2.TextToControlDistance = 0;
            this.LayoutControlItem2.TextVisible = false;
            // 
            // LayoutControlItem5
            // 
            this.LayoutControlItem5.Control = this.MemoExEdit_AddressFormat;
            this.LayoutControlItem5.CustomizationFormText = "Address Format";
            this.LayoutControlItem5.Location = new System.Drawing.Point(0, 89);
            this.LayoutControlItem5.Name = "LayoutControlItem5";
            this.LayoutControlItem5.Size = new System.Drawing.Size(313, 24);
            this.LayoutControlItem5.Text = "Address Format";
            this.LayoutControlItem5.TextSize = new System.Drawing.Size(76, 13);
            // 
            // EmptySpaceItem1
            // 
            this.EmptySpaceItem1.AllowHotTrack = false;
            this.EmptySpaceItem1.CustomizationFormText = "EmptySpaceItem1";
            this.EmptySpaceItem1.Location = new System.Drawing.Point(0, 137);
            this.EmptySpaceItem1.Name = "EmptySpaceItem1";
            this.EmptySpaceItem1.Size = new System.Drawing.Size(313, 10);
            this.EmptySpaceItem1.Text = "EmptySpaceItem1";
            this.EmptySpaceItem1.TextSize = new System.Drawing.Size(0, 0);
            // 
            // LayoutControlItem11
            // 
            this.LayoutControlItem11.Control = this.LookUpEdit_TimeZoneID;
            this.LayoutControlItem11.CustomizationFormText = "Time Zone";
            this.LayoutControlItem11.Location = new System.Drawing.Point(0, 41);
            this.LayoutControlItem11.Name = "LayoutControlItem11";
            this.LayoutControlItem11.Size = new System.Drawing.Size(313, 24);
            this.LayoutControlItem11.Text = "Time Zone";
            this.LayoutControlItem11.TextSize = new System.Drawing.Size(76, 13);
            // 
            // LayoutControlItem1
            // 
            this.LayoutControlItem1.Control = this.CheckEdit_default;
            this.LayoutControlItem1.CustomizationFormText = "LayoutControlItem1";
            this.LayoutControlItem1.Location = new System.Drawing.Point(0, 171);
            this.LayoutControlItem1.Name = "LayoutControlItem1";
            this.LayoutControlItem1.Size = new System.Drawing.Size(156, 24);
            this.LayoutControlItem1.Text = "LayoutControlItem1";
            this.LayoutControlItem1.TextSize = new System.Drawing.Size(0, 0);
            this.LayoutControlItem1.TextToControlDistance = 0;
            this.LayoutControlItem1.TextVisible = false;
            // 
            // LayoutControlItem4
            // 
            this.LayoutControlItem4.Control = this.CheckEdit_USAFormat;
            this.LayoutControlItem4.CustomizationFormText = "LayoutControlItem4";
            this.LayoutControlItem4.Location = new System.Drawing.Point(156, 147);
            this.LayoutControlItem4.Name = "LayoutControlItem4";
            this.LayoutControlItem4.Size = new System.Drawing.Size(157, 24);
            this.LayoutControlItem4.Text = "LayoutControlItem4";
            this.LayoutControlItem4.TextSize = new System.Drawing.Size(0, 0);
            this.LayoutControlItem4.TextToControlDistance = 0;
            this.LayoutControlItem4.TextVisible = false;
            // 
            // emptySpaceItem2
            // 
            this.emptySpaceItem2.AllowHotTrack = false;
            this.emptySpaceItem2.CustomizationFormText = "emptySpaceItem2";
            this.emptySpaceItem2.Location = new System.Drawing.Point(156, 195);
            this.emptySpaceItem2.Name = "emptySpaceItem2";
            this.emptySpaceItem2.Size = new System.Drawing.Size(157, 24);
            this.emptySpaceItem2.Text = "emptySpaceItem2";
            this.emptySpaceItem2.TextSize = new System.Drawing.Size(0, 0);
            // 
            // layoutControlItem12
            // 
            this.layoutControlItem12.Control = this.CheckEdit_NegativeDebtWarning;
            this.layoutControlItem12.CustomizationFormText = "layoutControlItem12";
            this.layoutControlItem12.Location = new System.Drawing.Point(0, 195);
            this.layoutControlItem12.Name = "layoutControlItem12";
            this.layoutControlItem12.Size = new System.Drawing.Size(156, 24);
            this.layoutControlItem12.Text = "layoutControlItem12";
            this.layoutControlItem12.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem12.TextToControlDistance = 0;
            this.layoutControlItem12.TextVisible = false;
            // 
            // EditForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(423, 241);
            this.Controls.Add(this.LayoutControl1);
            this.Name = "EditForm";
            this.Text = "State Information";
            this.Controls.SetChildIndex(this.simpleButton_OK, 0);
            this.Controls.SetChildIndex(this.simpleButton_Cancel, 0);
            this.Controls.SetChildIndex(this.LayoutControl1, 0);
            ((System.ComponentModel.ISupportInitialize)(this.DxErrorProvider1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LayoutControl1)).EndInit();
            this.LayoutControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.CheckEdit_NegativeDebtWarning.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LookUpEdit_TimeZoneID.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.CheckEdit_default.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TextEdit_hud_9902.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.CheckEdit_DoingBusiness.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.CheckEdit_ActiveFlag.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TextEdit_name.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.MemoExEdit_AddressFormat.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TextEdit_mailing_code.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LookUpEdit_country.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.CheckEdit_USAFormat.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LayoutControlGroup1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LayoutControlItem3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LayoutControlItem7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LayoutControlItem8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LayoutControlItem9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LayoutControlItem10)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LayoutControlItem6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LayoutControlItem2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LayoutControlItem5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.EmptySpaceItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LayoutControlItem11)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LayoutControlItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LayoutControlItem4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem12)).EndInit();
            this.ResumeLayout(false);

        }


        private DevExpress.XtraLayout.LayoutControl LayoutControl1;
        private DevExpress.XtraEditors.LookUpEdit LookUpEdit_TimeZoneID;
        private DevExpress.XtraEditors.CheckEdit CheckEdit_default;
        private DevExpress.XtraEditors.TextEdit TextEdit_hud_9902;
        private DevExpress.XtraEditors.CheckEdit CheckEdit_DoingBusiness;
        private DevExpress.XtraEditors.CheckEdit CheckEdit_ActiveFlag;
        private DevExpress.XtraEditors.TextEdit TextEdit_name;
        private DevExpress.XtraEditors.MemoExEdit MemoExEdit_AddressFormat;
        private DevExpress.XtraEditors.LabelControl LabelControl_state;
        private DevExpress.XtraEditors.TextEdit TextEdit_mailing_code;
        private DevExpress.XtraEditors.LookUpEdit LookUpEdit_country;
        private DevExpress.XtraEditors.CheckEdit CheckEdit_USAFormat;
        private DevExpress.XtraLayout.LayoutControlGroup LayoutControlGroup1;
        private DevExpress.XtraLayout.LayoutControlItem LayoutControlItem3;
        private DevExpress.XtraLayout.LayoutControlItem LayoutControlItem7;
        private DevExpress.XtraLayout.LayoutControlItem LayoutControlItem8;
        private DevExpress.XtraLayout.LayoutControlItem LayoutControlItem9;
        private DevExpress.XtraLayout.LayoutControlItem LayoutControlItem10;
        private DevExpress.XtraLayout.LayoutControlItem LayoutControlItem6;
        private DevExpress.XtraLayout.LayoutControlItem LayoutControlItem2;
        private DevExpress.XtraLayout.LayoutControlItem LayoutControlItem5;
        private DevExpress.XtraLayout.EmptySpaceItem EmptySpaceItem1;
        private DevExpress.XtraLayout.LayoutControlItem LayoutControlItem11;
        private DevExpress.XtraLayout.LayoutControlItem LayoutControlItem1;
        private DevExpress.XtraLayout.LayoutControlItem LayoutControlItem4;
        private DevExpress.XtraEditors.CheckEdit CheckEdit_NegativeDebtWarning;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem2;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem12;

    }
}