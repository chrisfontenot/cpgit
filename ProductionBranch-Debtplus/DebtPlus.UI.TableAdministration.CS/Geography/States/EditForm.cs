﻿using System;
using DebtPlus.LINQ;

namespace DebtPlus.UI.TableAdministration.CS.Geography.States
{
    internal partial class EditForm : Templates.EditTemplateForm
    {
        // Current record being edited
        private state record;

        /// <summary>
        /// Initialize the new class instance
        /// </summary>
        internal EditForm()
            : base()
        {
            InitializeComponent();
        }

        /// <summary>
        /// Initialize the new class instance
        /// </summary>
        /// <param name="record"></param>
        internal EditForm(state record)
            : this()
        {
            this.record = record;
            RegisterHandlers();

            // Load the list of countries
            LookUpEdit_country.Properties.DataSource = DebtPlus.LINQ.Cache.country.getList();
            LookUpEdit_TimeZoneID.Properties.DataSource = DebtPlus.LINQ.Cache.TimeZone.getList();
        }

        /// <summary>
        /// Add the event handlers to the collection
        /// </summary>
        private void RegisterHandlers()
        {
            Load += EditForm_Load;

            TextEdit_name.EditValueChanged += FormChanged;
            LookUpEdit_TimeZoneID.EditValueChanged += FormChanged;
            MemoExEdit_AddressFormat.EditValueChanged += FormChanged;
            TextEdit_hud_9902.EditValueChanged += FormChanged;
            TextEdit_mailing_code.EditValueChanged += FormChanged;
            CheckEdit_ActiveFlag.EditValueChanged += FormChanged;
            CheckEdit_default.EditValueChanged += FormChanged;
            CheckEdit_DoingBusiness.EditValueChanged += FormChanged;
            CheckEdit_USAFormat.EditValueChanged += FormChanged;
            CheckEdit_NegativeDebtWarning.EditValueChanged += FormChanged;
        }

        /// <summary>
        /// Remove the event handlers from being processed
        /// </summary>
        private void UnRegisterHandlers()
        {
            Load -= EditForm_Load;

            TextEdit_name.EditValueChanged += FormChanged;
            LookUpEdit_country.EditValueChanged += FormChanged;
            LookUpEdit_country.EditValueChanging += DebtPlus.Data.Validation.LookUpEdit_ActiveTest;
            LookUpEdit_TimeZoneID.EditValueChanged += FormChanged;
            LookUpEdit_TimeZoneID.EditValueChanging += DebtPlus.Data.Validation.LookUpEdit_ActiveTest;
            MemoExEdit_AddressFormat.EditValueChanged += FormChanged;
            TextEdit_hud_9902.EditValueChanged += FormChanged;
            TextEdit_mailing_code.EditValueChanged += FormChanged;
            CheckEdit_ActiveFlag.EditValueChanged += FormChanged;
            CheckEdit_default.EditValueChanged += FormChanged;
            CheckEdit_DoingBusiness.EditValueChanged += FormChanged;
            CheckEdit_USAFormat.EditValueChanged += FormChanged;
            CheckEdit_NegativeDebtWarning.EditValueChanged += FormChanged;
        }

        /// <summary>
        /// Process the FORM LOAD event
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void EditForm_Load(object sender, System.EventArgs e)
        {
            UnRegisterHandlers();
            try
            {
                LabelControl_state.Text = record.Id <= 0 ? "NEW" : record.Id.ToString();

                TextEdit_name.EditValue = record.Name;
                LookUpEdit_country.EditValue = record.Country;
                LookUpEdit_TimeZoneID.EditValue = record.TimeZoneID;
                MemoExEdit_AddressFormat.EditValue = record.AddressFormat;
                TextEdit_hud_9902.EditValue = record.hud_9902;
                TextEdit_mailing_code.EditValue = record.MailingCode;
                CheckEdit_ActiveFlag.EditValue = record.ActiveFlag;
                CheckEdit_default.EditValue = record.Default;
                CheckEdit_DoingBusiness.EditValue = record.DoingBusiness;
                CheckEdit_USAFormat.EditValue = record.USAFormat;
                CheckEdit_NegativeDebtWarning.EditValue = record.NegativeDebtWarning;

                simpleButton_OK.Enabled = !HasErrors();
            }
            finally
            {
                RegisterHandlers();
            }
        }

        /// <summary>
        /// Override the OK button to save the editing values into the record
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected override void simpleButton_OK_Click(object sender, EventArgs e)
        {
            record.Name = Convert.ToString(TextEdit_name.EditValue);
            record.Country = Convert.ToInt32(LookUpEdit_country.EditValue);
            record.TimeZoneID = Convert.ToInt32(LookUpEdit_TimeZoneID.EditValue);
            record.AddressFormat = Convert.ToString(MemoExEdit_AddressFormat.EditValue);
            record.hud_9902 = Convert.ToInt32(TextEdit_hud_9902.EditValue);
            record.MailingCode = Convert.ToString(TextEdit_mailing_code.EditValue);
            record.ActiveFlag = Convert.ToBoolean(CheckEdit_ActiveFlag.EditValue);
            record.Default = Convert.ToBoolean(CheckEdit_default.EditValue);
            record.DoingBusiness = Convert.ToBoolean(CheckEdit_DoingBusiness.EditValue);
            record.USAFormat = Convert.ToInt32(CheckEdit_USAFormat.EditValue);
            record.NegativeDebtWarning = Convert.ToBoolean(CheckEdit_NegativeDebtWarning.EditValue);

            base.simpleButton_OK_Click(sender, e);
        }

        private void FormChanged(object sender, EventArgs e)
        {
            simpleButton_OK.Enabled = !HasErrors();
        }

        /// <summary>
        /// Determine if the input form has error(s)
        /// </summary>
        /// <returns></returns>
        private bool HasErrors()
        {
            if (TextEdit_name.Text.Trim() == string.Empty)
            {
                return true;
            }

            if (LookUpEdit_country.EditValue == null || LookUpEdit_TimeZoneID.EditValue == null)
            {
                return true;
            }

            return false;
        }
    }
}