﻿using System;
using System.Linq;
using DebtPlus.LINQ;

namespace DebtPlus.UI.TableAdministration.CS.Geography.States
{
    public partial class MainForm : Templates.MainForm
    {
        /// <summary>
        /// List of the records to be edited in the program
        /// </summary>
        private System.Collections.Generic.List<state> colRecords;

        /// <summary>
        /// Initialize the new class
        /// </summary>
        /// <param name="dc">Pointer to the data context in the mainline</param>
        public MainForm()
            : base()
        {
            InitializeComponent();
            RegisterHandlers();
        }

        private void RegisterHandlers()
        {
            Load += MainForm_Load;
        }

        private void UnRegisterHandlers()
        {
            Load -= MainForm_Load;
        }

        /// <summary>
        /// Process the load event on the form
        /// </summary>
        private void MainForm_Load(object sender, System.EventArgs e)
        {
            using (var cm = new DebtPlus.UI.Common.CursorManager())
            {
                try
                {
                    // Retrieve the list of menu items
                    using (var drm = new DebtPlus.LINQ.BusinessContext())
                    {
                        drm.DeferredLoadingEnabled = false;
                        colRecords = drm.states.ToList();
                        gridControl1.DataSource = colRecords;
                    }
                }
                catch (Exception ex)
                {
                    DebtPlus.UI.Common.ErrorHandling.HandleErrors(ex, "Error retrieving database information");
                }
            }
        }

        /// <summary>
        /// Create the new record
        /// </summary>
        protected override void CreateRecord()
        {
            // Allocate the new record to be inserted.
            DebtPlus.LINQ.state record = new DebtPlus.LINQ.state();

            // Edit the new record. If OK then attempt to insert the record.
            using (EditForm frm = new EditForm(record))
            {
                if (frm.ShowDialog() == System.Windows.Forms.DialogResult.OK)
                {
                    try
                    {
                        using (var drm = new DebtPlus.LINQ.BusinessContext())
                        {
                            drm.DeferredLoadingEnabled = false;
                            // Turn off the default status on all records if the new one is marked Default.
                            if (record.Default)
                            {
                                ResetDefaults(drm);
                                record.Default = true;
                            }

                            drm.states.InsertOnSubmit(record);
                            drm.SubmitChanges();

                            // Add the record to the main list if the database is updated
                            colRecords.Add(record);
                            gridControl1.RefreshDataSource();
                        }
                    }
                    catch (System.Data.SqlClient.SqlException ex)
                    {
                        DebtPlus.UI.Common.ErrorHandling.HandleErrors(ex, "Error updating database");
                    }
                }
            }
        }

        /// <summary>
        /// Update the existing record
        /// </summary>
        /// <param name="obj">Pointer to the desired record for the operation</param>
        protected override void UpdateRecord(object obj)
        {
            // Find the record
            DebtPlus.LINQ.state record = obj as state;
            if (obj == null)
            {
                return;
            }

            using (EditForm frm = new EditForm(record))
            {
                if (frm.ShowDialog() == System.Windows.Forms.DialogResult.OK)
                {
                    try
                    {
                        // Update the database with the current values
                        using (var drm = new BusinessContext())
                        {
                            drm.DeferredLoadingEnabled = false;
                            // Turn off the default status on all records if the new one is marked Default.
                            if (record.Default)
                            {
                                ResetDefaults(drm);
                                record.Default = true;
                            }

                            var q = (from r in drm.states where r.Id == record.Id select r).SingleOrDefault();
                            if (q != null)
                            {
                                q.ActiveFlag = record.ActiveFlag;
                                q.AddressFormat = record.AddressFormat;
                                q.Country = record.Country;
                                q.Default = record.Default;
                                q.DoingBusiness = record.DoingBusiness;
                                q.hud_9902 = record.hud_9902;
                                q.MailingCode = record.MailingCode;
                                q.Name = record.Name;
                                q.TimeZoneID = record.TimeZoneID;
                                q.USAFormat = record.USAFormat;
                                q.NegativeDebtWarning = record.NegativeDebtWarning;
                                drm.SubmitChanges();
                                gridControl1.RefreshDataSource();
                            }
                        }
                    }
                    catch (System.Data.SqlClient.SqlException ex)
                    {
                        DebtPlus.UI.Common.ErrorHandling.HandleErrors(ex, "Error updating database");
                    }
                }
            }
        }

        /// <summary>
        /// Delete the indicated record from the database
        /// </summary>
        /// <param name="obj">Pointer to the desired record for the operation</param>
        protected override void DeleteRecord(object obj)
        {
            // Find the record
            DebtPlus.LINQ.state record = obj as state;
            if (obj == null)
            {
                return;
            }

            if (DebtPlus.Data.Prompts.RequestConfirmation_Delete() == System.Windows.Forms.DialogResult.Yes)
            {
                using (var drm = new BusinessContext())
                {
                    drm.DeferredLoadingEnabled = false;
                    // Find the record and remove it from the database if it is still there
                    var query = (from m in drm.states where m.Id == record.Id select m).SingleOrDefault();
                    if (query != null)
                    {
                        drm.states.DeleteOnSubmit(query);
                        drm.SubmitChanges();
                    }

                    // Remove the record from the display list and redisplay it
                    colRecords.Remove(record);
                    gridControl1.RefreshDataSource();
                }
            }
        }

        /// <summary>
        /// Clear the Default status on the records when the new record is marked Default.
        /// </summary>
        /// <param name="bc">Pointer to the business class layer</param>
        private void ResetDefaults(BusinessContext drm)
        {
            // Turn off any record set as Default.
            foreach (state qDef in (from r in drm.states where r.Default select r))
            {
                qDef.Default = false;
            }

            foreach (state qDef in colRecords)
            {
                qDef.Default = false;
            }
        }
    }
}