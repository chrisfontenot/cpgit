﻿namespace DebtPlus.UI.TableAdministration.CS.Geography.Regions
{
    partial class EditForm
    {
		//Form overrides dispose to clean up the component list.
		[System.Diagnostics.DebuggerNonUserCode]
		protected override void Dispose(bool disposing)
		{
			try 
            {
				if (disposing)
                {
                    if (components != null) components.Dispose();
				}
                components = null;
            }
    	    finally
            {
				base.Dispose(disposing);
			}
		}

		//Required by the Windows Form Designer
		private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.LabelControl_id = new DevExpress.XtraEditors.LabelControl();
            this.LabelControl1 = new DevExpress.XtraEditors.LabelControl();
            this.TextEdit_description = new DevExpress.XtraEditors.TextEdit();
            this.LabelControl3 = new DevExpress.XtraEditors.LabelControl();
            this.CheckEdit_default = new DevExpress.XtraEditors.CheckEdit();
            ((System.ComponentModel.ISupportInitialize)(this.DxErrorProvider1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TextEdit_description.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.CheckEdit_default.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // simpleButton_OK
            // 
            this.simpleButton_OK.Location = new System.Drawing.Point(373, 13);
            // 
            // simpleButton_Cancel
            // 
            this.simpleButton_Cancel.Location = new System.Drawing.Point(373, 56);
            // 
            // DefaultLookAndFeel1
            // 
            this.DefaultLookAndFeel1.LookAndFeel.SkinName = "McSkin";
            // 
            // LabelControl_id
            // 
            this.LabelControl_id.Location = new System.Drawing.Point(82, 13);
            this.LabelControl_id.Name = "LabelControl_id";
            this.LabelControl_id.Size = new System.Drawing.Size(66, 13);
            this.LabelControl_id.TabIndex = 26;
            this.LabelControl_id.Text = "LabelControl2";
            // 
            // LabelControl1
            // 
            this.LabelControl1.Location = new System.Drawing.Point(9, 13);
            this.LabelControl1.Name = "LabelControl1";
            this.LabelControl1.Size = new System.Drawing.Size(48, 13);
            this.LabelControl1.TabIndex = 25;
            this.LabelControl1.Text = "Record ID";
            // 
            // TextEdit_description
            // 
            this.TextEdit_description.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.TextEdit_description.Location = new System.Drawing.Point(82, 40);
            this.TextEdit_description.Name = "TextEdit_description";
            this.TextEdit_description.Properties.MaxLength = 50;
            this.TextEdit_description.Size = new System.Drawing.Size(210, 20);
            this.TextEdit_description.TabIndex = 28;
            // 
            // LabelControl3
            // 
            this.LabelControl3.Location = new System.Drawing.Point(8, 43);
            this.LabelControl3.Name = "LabelControl3";
            this.LabelControl3.Size = new System.Drawing.Size(53, 13);
            this.LabelControl3.TabIndex = 27;
            this.LabelControl3.Text = "Description";
            // 
            // CheckEdit_default
            // 
            this.CheckEdit_default.Location = new System.Drawing.Point(6, 93);
            this.CheckEdit_default.Name = "CheckEdit_default";
            this.CheckEdit_default.Properties.Caption = "Default Region";
            this.CheckEdit_default.Size = new System.Drawing.Size(158, 20);
            this.CheckEdit_default.TabIndex = 29;
            // 
            // EditForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(468, 122);
            this.Controls.Add(this.LabelControl_id);
            this.Controls.Add(this.LabelControl1);
            this.Controls.Add(this.TextEdit_description);
            this.Controls.Add(this.LabelControl3);
            this.Controls.Add(this.CheckEdit_default);
            this.Name = "EditForm";
            this.Text = "Region";
            this.Controls.SetChildIndex(this.simpleButton_OK, 0);
            this.Controls.SetChildIndex(this.simpleButton_Cancel, 0);
            this.Controls.SetChildIndex(this.CheckEdit_default, 0);
            this.Controls.SetChildIndex(this.LabelControl3, 0);
            this.Controls.SetChildIndex(this.TextEdit_description, 0);
            this.Controls.SetChildIndex(this.LabelControl1, 0);
            this.Controls.SetChildIndex(this.LabelControl_id, 0);
            ((System.ComponentModel.ISupportInitialize)(this.DxErrorProvider1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TextEdit_description.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.CheckEdit_default.Properties)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }


        private DevExpress.XtraEditors.LabelControl LabelControl_id;
        private DevExpress.XtraEditors.LabelControl LabelControl1;
        private DevExpress.XtraEditors.TextEdit TextEdit_description;
        private DevExpress.XtraEditors.LabelControl LabelControl3;
        private DevExpress.XtraEditors.CheckEdit CheckEdit_default;

    }
}