﻿namespace DebtPlus.UI.TableAdministration.CS.Geography.Counties
{
    partial class EditForm
    {
		//Form overrides dispose to clean up the component list.
		[System.Diagnostics.DebuggerNonUserCode]
		protected override void Dispose(bool disposing)
		{
			try 
            {
				if (disposing)
                {
                    if (components != null) components.Dispose();
				}
                components = null;
            }
    	    finally
            {
				base.Dispose(disposing);
			}
		}

		//Required by the Windows Form Designer
		private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.LayoutControl1 = new DevExpress.XtraLayout.LayoutControl();
            this.checkEdit_default = new DevExpress.XtraEditors.CheckEdit();
            this.lookUpEdit_bankruptcy_district = new DevExpress.XtraEditors.LookUpEdit();
            this.lookUpEdit_state = new DevExpress.XtraEditors.LookUpEdit();
            this.calcEdit_median_income = new DevExpress.XtraEditors.CalcEdit();
            this.textEdit_fips = new DevExpress.XtraEditors.TextEdit();
            this.CheckEdit_ActiveFlag = new DevExpress.XtraEditors.CheckEdit();
            this.TextEdit_name = new DevExpress.XtraEditors.TextEdit();
            this.LabelControl_county = new DevExpress.XtraEditors.LabelControl();
            this.LayoutControlGroup1 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.LayoutControlItem3 = new DevExpress.XtraLayout.LayoutControlItem();
            this.LayoutControlItem9 = new DevExpress.XtraLayout.LayoutControlItem();
            this.LayoutControlItem10 = new DevExpress.XtraLayout.LayoutControlItem();
            this.EmptySpaceItem1 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.layoutControlItem1 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem2 = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem2 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.layoutControlItem4 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem5 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem6 = new DevExpress.XtraLayout.LayoutControlItem();
            ((System.ComponentModel.ISupportInitialize)(this.DxErrorProvider1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LayoutControl1)).BeginInit();
            this.LayoutControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit_default.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lookUpEdit_bankruptcy_district.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lookUpEdit_state.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.calcEdit_median_income.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit_fips.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.CheckEdit_ActiveFlag.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TextEdit_name.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LayoutControlGroup1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LayoutControlItem3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LayoutControlItem9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LayoutControlItem10)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.EmptySpaceItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem6)).BeginInit();
            this.SuspendLayout();
            // 
            // simpleButton_Cancel
            // 
            this.simpleButton_Cancel.Location = new System.Drawing.Point(339, 67);
            // 
            // simpleButton_OK
            // 
            this.simpleButton_OK.Location = new System.Drawing.Point(339, 35);
            // 
            // DefaultLookAndFeel1
            // 
            this.DefaultLookAndFeel1.LookAndFeel.SkinName = "McSkin";
            // 
            // LayoutControl1
            // 
            this.LayoutControl1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.LayoutControl1.Controls.Add(this.checkEdit_default);
            this.LayoutControl1.Controls.Add(this.lookUpEdit_bankruptcy_district);
            this.LayoutControl1.Controls.Add(this.lookUpEdit_state);
            this.LayoutControl1.Controls.Add(this.calcEdit_median_income);
            this.LayoutControl1.Controls.Add(this.textEdit_fips);
            this.LayoutControl1.Controls.Add(this.CheckEdit_ActiveFlag);
            this.LayoutControl1.Controls.Add(this.TextEdit_name);
            this.LayoutControl1.Controls.Add(this.LabelControl_county);
            this.LayoutControl1.Location = new System.Drawing.Point(0, 0);
            this.LayoutControl1.Name = "LayoutControl1";
            this.LayoutControl1.Root = this.LayoutControlGroup1;
            this.LayoutControl1.Size = new System.Drawing.Size(333, 217);
            this.LayoutControl1.TabIndex = 22;
            this.LayoutControl1.Text = "LayoutControl1";
            // 
            // checkEdit_default
            // 
            this.checkEdit_default.Location = new System.Drawing.Point(12, 185);
            this.checkEdit_default.Name = "checkEdit_default";
            this.checkEdit_default.Properties.Caption = "Default";
            this.checkEdit_default.Size = new System.Drawing.Size(152, 20);
            this.checkEdit_default.StyleController = this.LayoutControl1;
            this.checkEdit_default.TabIndex = 19;
            // 
            // lookUpEdit_bankruptcy_district
            // 
            this.lookUpEdit_bankruptcy_district.Location = new System.Drawing.Point(106, 125);
            this.lookUpEdit_bankruptcy_district.Name = "lookUpEdit_bankruptcy_district";
            this.lookUpEdit_bankruptcy_district.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.lookUpEdit_bankruptcy_district.Properties.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("Id", "ID", 20, DevExpress.Utils.FormatType.Numeric, "f0", false, DevExpress.Utils.HorzAlignment.Default),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("description", "Description", 20, DevExpress.Utils.FormatType.None, "", true, DevExpress.Utils.HorzAlignment.Default, DevExpress.Data.ColumnSortOrder.Ascending, DevExpress.Utils.DefaultBoolean.True)});
            this.lookUpEdit_bankruptcy_district.Properties.DisplayMember = "description";
            this.lookUpEdit_bankruptcy_district.Properties.NullText = "";
            this.lookUpEdit_bankruptcy_district.Properties.ShowFooter = false;
            this.lookUpEdit_bankruptcy_district.Properties.ShowHeader = false;
            this.lookUpEdit_bankruptcy_district.Properties.ValueMember = "Id";
            this.lookUpEdit_bankruptcy_district.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.True;
            this.lookUpEdit_bankruptcy_district.Size = new System.Drawing.Size(215, 20);
            this.lookUpEdit_bankruptcy_district.StyleController = this.LayoutControl1;
            this.lookUpEdit_bankruptcy_district.TabIndex = 18;
            // 
            // lookUpEdit_state
            // 
            this.lookUpEdit_state.Location = new System.Drawing.Point(106, 101);
            this.lookUpEdit_state.Name = "lookUpEdit_state";
            this.lookUpEdit_state.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.lookUpEdit_state.Properties.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("Id", "ID", 20, DevExpress.Utils.FormatType.Numeric, "f0", false, DevExpress.Utils.HorzAlignment.Default),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("Name", "Name", 20, DevExpress.Utils.FormatType.None, "", true, DevExpress.Utils.HorzAlignment.Default, DevExpress.Data.ColumnSortOrder.Ascending, DevExpress.Utils.DefaultBoolean.True)});
            this.lookUpEdit_state.Properties.DisplayMember = "Name";
            this.lookUpEdit_state.Properties.ValueMember = "Id";
            this.lookUpEdit_state.Properties.NullText = "";
            this.lookUpEdit_state.Properties.ShowFooter = false;
            this.lookUpEdit_state.Properties.ShowHeader = false;
            this.lookUpEdit_state.Size = new System.Drawing.Size(215, 20);
            this.lookUpEdit_state.StyleController = this.LayoutControl1;
            this.lookUpEdit_state.TabIndex = 17;
            // 
            // calcEdit_median_income
            // 
            this.calcEdit_median_income.Location = new System.Drawing.Point(106, 77);
            this.calcEdit_median_income.Name = "calcEdit_median_income";
            this.calcEdit_median_income.Properties.Appearance.Options.UseTextOptions = true;
            this.calcEdit_median_income.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.calcEdit_median_income.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.calcEdit_median_income.Properties.Mask.BeepOnError = true;
            this.calcEdit_median_income.Properties.Mask.EditMask = "c2";
            this.calcEdit_median_income.Properties.Precision = 2;
            this.calcEdit_median_income.Size = new System.Drawing.Size(92, 20);
            this.calcEdit_median_income.StyleController = this.LayoutControl1;
            this.calcEdit_median_income.TabIndex = 16;
            // 
            // textEdit_fips
            // 
            this.textEdit_fips.Location = new System.Drawing.Point(106, 53);
            this.textEdit_fips.Name = "textEdit_fips";
            this.textEdit_fips.Properties.MaxLength = 50;
            this.textEdit_fips.Size = new System.Drawing.Size(215, 20);
            this.textEdit_fips.StyleController = this.LayoutControl1;
            this.textEdit_fips.TabIndex = 15;
            // 
            // CheckEdit_ActiveFlag
            // 
            this.CheckEdit_ActiveFlag.Location = new System.Drawing.Point(168, 185);
            this.CheckEdit_ActiveFlag.Name = "CheckEdit_ActiveFlag";
            this.CheckEdit_ActiveFlag.Properties.Caption = "Active Entry";
            this.CheckEdit_ActiveFlag.Size = new System.Drawing.Size(153, 20);
            this.CheckEdit_ActiveFlag.StyleController = this.LayoutControl1;
            this.CheckEdit_ActiveFlag.TabIndex = 14;
            this.CheckEdit_ActiveFlag.ToolTip = "Allow this record to be selected from a list of counties";
            // 
            // TextEdit_name
            // 
            this.TextEdit_name.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.TextEdit_name.Location = new System.Drawing.Point(106, 29);
            this.TextEdit_name.Name = "TextEdit_name";
            this.TextEdit_name.Size = new System.Drawing.Size(215, 20);
            this.TextEdit_name.StyleController = this.LayoutControl1;
            this.TextEdit_name.TabIndex = 3;
            this.TextEdit_name.ToolTip = "This is the county name used when a county is to be selected from a list";
            // 
            // LabelControl_county
            // 
            this.LabelControl_county.Location = new System.Drawing.Point(106, 12);
            this.LabelControl_county.Name = "LabelControl_county";
            this.LabelControl_county.Size = new System.Drawing.Size(23, 13);
            this.LabelControl_county.StyleController = this.LayoutControl1;
            this.LabelControl_county.TabIndex = 1;
            this.LabelControl_county.Text = "NEW";
            // 
            // LayoutControlGroup1
            // 
            this.LayoutControlGroup1.CustomizationFormText = "LayoutControlGroup1";
            this.LayoutControlGroup1.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.True;
            this.LayoutControlGroup1.GroupBordersVisible = false;
            this.LayoutControlGroup1.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.LayoutControlItem3,
            this.LayoutControlItem9,
            this.LayoutControlItem10,
            this.EmptySpaceItem1,
            this.layoutControlItem1,
            this.layoutControlItem2,
            this.emptySpaceItem2,
            this.layoutControlItem4,
            this.layoutControlItem5,
            this.layoutControlItem6});
            this.LayoutControlGroup1.Location = new System.Drawing.Point(0, 0);
            this.LayoutControlGroup1.Name = "LayoutControlGroup1";
            this.LayoutControlGroup1.Size = new System.Drawing.Size(333, 217);
            this.LayoutControlGroup1.Text = "LayoutControlGroup1";
            this.LayoutControlGroup1.TextVisible = false;
            // 
            // LayoutControlItem3
            // 
            this.LayoutControlItem3.Control = this.CheckEdit_ActiveFlag;
            this.LayoutControlItem3.CustomizationFormText = "LayoutControlItem3";
            this.LayoutControlItem3.Location = new System.Drawing.Point(156, 173);
            this.LayoutControlItem3.Name = "LayoutControlItem3";
            this.LayoutControlItem3.Size = new System.Drawing.Size(157, 24);
            this.LayoutControlItem3.Text = "LayoutControlItem3";
            this.LayoutControlItem3.TextSize = new System.Drawing.Size(0, 0);
            this.LayoutControlItem3.TextToControlDistance = 0;
            this.LayoutControlItem3.TextVisible = false;
            // 
            // LayoutControlItem9
            // 
            this.LayoutControlItem9.Control = this.TextEdit_name;
            this.LayoutControlItem9.CustomizationFormText = "Name";
            this.LayoutControlItem9.Location = new System.Drawing.Point(0, 17);
            this.LayoutControlItem9.Name = "LayoutControlItem9";
            this.LayoutControlItem9.Size = new System.Drawing.Size(313, 24);
            this.LayoutControlItem9.Text = "Name";
            this.LayoutControlItem9.TextSize = new System.Drawing.Size(90, 13);
            // 
            // LayoutControlItem10
            // 
            this.LayoutControlItem10.Control = this.LabelControl_county;
            this.LayoutControlItem10.CustomizationFormText = "Record ID";
            this.LayoutControlItem10.Location = new System.Drawing.Point(0, 0);
            this.LayoutControlItem10.Name = "LayoutControlItem10";
            this.LayoutControlItem10.Size = new System.Drawing.Size(313, 17);
            this.LayoutControlItem10.Text = "Record ID";
            this.LayoutControlItem10.TextSize = new System.Drawing.Size(90, 13);
            // 
            // EmptySpaceItem1
            // 
            this.EmptySpaceItem1.AllowHotTrack = false;
            this.EmptySpaceItem1.CustomizationFormText = "EmptySpaceItem1";
            this.EmptySpaceItem1.Location = new System.Drawing.Point(0, 137);
            this.EmptySpaceItem1.Name = "EmptySpaceItem1";
            this.EmptySpaceItem1.Size = new System.Drawing.Size(313, 36);
            this.EmptySpaceItem1.Text = "EmptySpaceItem1";
            this.EmptySpaceItem1.TextSize = new System.Drawing.Size(0, 0);
            // 
            // layoutControlItem1
            // 
            this.layoutControlItem1.Control = this.textEdit_fips;
            this.layoutControlItem1.CustomizationFormText = "FIPS";
            this.layoutControlItem1.Location = new System.Drawing.Point(0, 41);
            this.layoutControlItem1.Name = "layoutControlItem1";
            this.layoutControlItem1.Size = new System.Drawing.Size(313, 24);
            this.layoutControlItem1.Text = "FIPS";
            this.layoutControlItem1.TextSize = new System.Drawing.Size(90, 13);
            // 
            // layoutControlItem2
            // 
            this.layoutControlItem2.Control = this.calcEdit_median_income;
            this.layoutControlItem2.CustomizationFormText = "Median Income";
            this.layoutControlItem2.Location = new System.Drawing.Point(0, 65);
            this.layoutControlItem2.Name = "layoutControlItem2";
            this.layoutControlItem2.Size = new System.Drawing.Size(190, 24);
            this.layoutControlItem2.Text = "Median Income";
            this.layoutControlItem2.TextSize = new System.Drawing.Size(90, 13);
            // 
            // emptySpaceItem2
            // 
            this.emptySpaceItem2.AllowHotTrack = false;
            this.emptySpaceItem2.CustomizationFormText = "emptySpaceItem2";
            this.emptySpaceItem2.Location = new System.Drawing.Point(190, 65);
            this.emptySpaceItem2.Name = "emptySpaceItem2";
            this.emptySpaceItem2.Size = new System.Drawing.Size(123, 24);
            this.emptySpaceItem2.Text = "emptySpaceItem2";
            this.emptySpaceItem2.TextSize = new System.Drawing.Size(0, 0);
            // 
            // layoutControlItem_simpleButton_Cancel
            // 
            this.layoutControlItem4.Control = this.lookUpEdit_state;
            this.layoutControlItem4.CustomizationFormText = "State";
            this.layoutControlItem4.Location = new System.Drawing.Point(0, 89);
            this.layoutControlItem4.Name = "layoutControlItem_simpleButton_Cancel";
            this.layoutControlItem4.Size = new System.Drawing.Size(313, 24);
            this.layoutControlItem4.Text = "State";
            this.layoutControlItem4.TextSize = new System.Drawing.Size(90, 13);
            // 
            // layoutControlItem_New
            // 
            this.layoutControlItem5.Control = this.lookUpEdit_bankruptcy_district;
            this.layoutControlItem5.CustomizationFormText = "Bankruptcy District";
            this.layoutControlItem5.Location = new System.Drawing.Point(0, 113);
            this.layoutControlItem5.Name = "layoutControlItem_New";
            this.layoutControlItem5.Size = new System.Drawing.Size(313, 24);
            this.layoutControlItem5.Text = "Bankruptcy District";
            this.layoutControlItem5.TextSize = new System.Drawing.Size(90, 13);
            // 
            // layoutControlItem6
            // 
            this.layoutControlItem6.Control = this.checkEdit_default;
            this.layoutControlItem6.CustomizationFormText = "layoutControlItem6";
            this.layoutControlItem6.Location = new System.Drawing.Point(0, 173);
            this.layoutControlItem6.Name = "layoutControlItem6";
            this.layoutControlItem6.Size = new System.Drawing.Size(156, 24);
            this.layoutControlItem6.Text = "layoutControlItem6";
            this.layoutControlItem6.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem6.TextToControlDistance = 0;
            this.layoutControlItem6.TextVisible = false;
            // 
            // EditForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(423, 219);
            this.Controls.Add(this.LayoutControl1);
            this.Name = "EditForm";
            this.Text = "County Information";
            this.Controls.SetChildIndex(this.simpleButton_OK, 0);
            this.Controls.SetChildIndex(this.simpleButton_Cancel, 0);
            this.Controls.SetChildIndex(this.LayoutControl1, 0);
            ((System.ComponentModel.ISupportInitialize)(this.DxErrorProvider1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LayoutControl1)).EndInit();
            this.LayoutControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit_default.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lookUpEdit_bankruptcy_district.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lookUpEdit_state.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.calcEdit_median_income.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit_fips.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.CheckEdit_ActiveFlag.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TextEdit_name.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LayoutControlGroup1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LayoutControlItem3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LayoutControlItem9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LayoutControlItem10)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.EmptySpaceItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem6)).EndInit();
            this.ResumeLayout(false);

        }


        private DevExpress.XtraLayout.LayoutControl LayoutControl1;
        private DevExpress.XtraEditors.CheckEdit CheckEdit_ActiveFlag;
        private DevExpress.XtraEditors.TextEdit TextEdit_name;
        private DevExpress.XtraEditors.LabelControl LabelControl_county;
        private DevExpress.XtraLayout.LayoutControlGroup LayoutControlGroup1;
        private DevExpress.XtraLayout.LayoutControlItem LayoutControlItem3;
        private DevExpress.XtraLayout.LayoutControlItem LayoutControlItem9;
        private DevExpress.XtraLayout.LayoutControlItem LayoutControlItem10;
        private DevExpress.XtraLayout.EmptySpaceItem EmptySpaceItem1;
        private DevExpress.XtraEditors.TextEdit textEdit_fips;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem1;
        private DevExpress.XtraEditors.CalcEdit calcEdit_median_income;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem2;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem2;
        private DevExpress.XtraEditors.LookUpEdit lookUpEdit_bankruptcy_district;
        private DevExpress.XtraEditors.LookUpEdit lookUpEdit_state;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem4;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem5;
        private DevExpress.XtraEditors.CheckEdit checkEdit_default;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem6;

    }
}