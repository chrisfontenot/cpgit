﻿using System;
using DebtPlus.LINQ;

namespace DebtPlus.UI.TableAdministration.CS.Geography.Counties
{
    internal partial class EditForm : Templates.EditTemplateForm
    {
        // Current record being edited
        private county record;

        /// <summary>
        /// Initialize the new class instance
        /// </summary>
        internal EditForm()
            : base()
        {
            InitializeComponent();
        }

        /// <summary>
        /// Initialize the new class instance
        /// </summary>
        /// <param name="record"></param>
        internal EditForm(county record)
            : this()
        {
            this.record = record;
            RegisterHandlers();
        }

        /// <summary>
        /// Add the event handlers to the collection
        /// </summary>
        private void RegisterHandlers()
        {
            Load                                            += EditForm_Load;
            TextEdit_name.EditValueChanged                  += FormChanged;
            textEdit_fips.EditValueChanged                  += FormChanged;
            CheckEdit_ActiveFlag.EditValueChanged           += FormChanged;
            checkEdit_default.EditValueChanged              += FormChanged;
            calcEdit_median_income.EditValueChanged         += FormChanged;
            lookUpEdit_bankruptcy_district.EditValueChanged += FormChanged;
            lookUpEdit_state.EditValueChanged               += FormChanged;
        }

        /// <summary>
        /// Remove the event handlers from being processed
        /// </summary>
        private void UnRegisterHandlers()
        {
            Load                                            -= EditForm_Load;
            TextEdit_name.EditValueChanged                  -= FormChanged;
            textEdit_fips.EditValueChanged                  -= FormChanged;
            CheckEdit_ActiveFlag.EditValueChanged           -= FormChanged;
            checkEdit_default.EditValueChanged              -= FormChanged;
            calcEdit_median_income.EditValueChanged         -= FormChanged;
            lookUpEdit_bankruptcy_district.EditValueChanged -= FormChanged;
            lookUpEdit_state.EditValueChanged               -= FormChanged;
        }

        /// <summary>
        /// Process the FORM LOAD event
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void EditForm_Load(object sender, System.EventArgs e)
        {
            UnRegisterHandlers();
            try
            {
                LabelControl_county.Text = record.Id <= 0 ? "NEW" : record.Id.ToString();

                TextEdit_name.EditValue                  = record.name;
                textEdit_fips.EditValue                  = record.FIPS;
                CheckEdit_ActiveFlag.EditValue           = record.ActiveFlag;
                checkEdit_default.Checked                = record.Default;
                calcEdit_median_income.EditValue         = record.median_income;
                lookUpEdit_bankruptcy_district.EditValue = record.bankruptcy_district;
                lookUpEdit_state.EditValue               = record.state;

                simpleButton_OK.Enabled = !HasErrors();
            }
            finally
            {
                RegisterHandlers();
            }
        }

        /// <summary>
        /// Override the OK button to save the editing values into the record
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected override void simpleButton_OK_Click(object sender, EventArgs e)
        {
            record.name                = Convert.ToString(TextEdit_name.EditValue);
            record.ActiveFlag          = Convert.ToBoolean(CheckEdit_ActiveFlag.EditValue);
            record.bankruptcy_district = Convert.ToInt32(lookUpEdit_bankruptcy_district.EditValue);
            record.Default             = checkEdit_default.Checked;
            record.FIPS                = Convert.ToString(textEdit_fips.EditValue);
            record.median_income       = Convert.ToDecimal(calcEdit_median_income.EditValue);
            record.state               = Convert.ToInt32(lookUpEdit_state.EditValue);

            base.simpleButton_OK_Click(sender, e);
        }

        private void FormChanged(object sender, EventArgs e)
        {
            simpleButton_OK.Enabled = !HasErrors();
        }

        /// <summary>
        /// Determine if the input form has error(s)
        /// </summary>
        /// <returns></returns>
        private bool HasErrors()
        {
            if (TextEdit_name.Text.Trim() == string.Empty)
            {
                return true;
            }

            return false;
        }
    }
}