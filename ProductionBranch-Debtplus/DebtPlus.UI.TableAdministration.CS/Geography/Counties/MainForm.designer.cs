﻿namespace DebtPlus.UI.TableAdministration.CS.Geography.Counties
{
    partial class MainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            try
            {
                if (disposing)
                {
                    if (components != null) components.Dispose();
                    if (bc != null) bc.Dispose();
                }
                components = null;
                bc = null;
            }
            finally
            {
                base.Dispose(disposing);
            }
        }

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            DevExpress.XtraGrid.StyleFormatCondition styleFormatCondition1 = new DevExpress.XtraGrid.StyleFormatCondition();
            this.gridColumn_ActiveFlag = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn_id = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn_name = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn_median_income = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn_fips = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn_state = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn_default = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn_district = new DevExpress.XtraGrid.Columns.GridColumn();
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl1)).BeginInit();
            this.layoutControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem_simpleButton_Cancel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem_SimpleButton_Edit)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem_simpleButton_New)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.popupMenu1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DxErrorProvider1)).BeginInit();
            this.SuspendLayout();
            this.layoutControl1.Controls.SetChildIndex(this.SimpleButton_Edit, 0);
            this.layoutControl1.Controls.SetChildIndex(this.simpleButton_Cancel, 0);
            this.layoutControl1.Controls.SetChildIndex(this.simpleButton_New, 0);
            this.layoutControl1.Controls.SetChildIndex(this.gridControl1, 0);
            this.gridView1.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gridColumn_id,
            this.gridColumn_name,
            this.gridColumn_state,
            this.gridColumn_median_income,
            this.gridColumn_fips,
            this.gridColumn_default,
            this.gridColumn_ActiveFlag,
            this.gridColumn_district});
            styleFormatCondition1.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(128)))));
            styleFormatCondition1.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            styleFormatCondition1.Appearance.Options.UseBackColor = true;
            styleFormatCondition1.Appearance.Options.UseFont = true;
            styleFormatCondition1.ApplyToRow = true;
            styleFormatCondition1.Column = this.gridColumn_ActiveFlag;
            styleFormatCondition1.Condition = DevExpress.XtraGrid.FormatConditionEnum.NotEqual;
            styleFormatCondition1.Value1 = true;
            this.gridView1.FormatConditions.AddRange(new DevExpress.XtraGrid.StyleFormatCondition[] {
            styleFormatCondition1});
            // 
            // gridColumn_ActiveFlag
            // 
            this.gridColumn_ActiveFlag.Caption = "Active";
            this.gridColumn_ActiveFlag.FieldName = "ActiveFlag";
            this.gridColumn_ActiveFlag.Name = "gridColumn_ActiveFlag";
            this.gridColumn_ActiveFlag.OptionsColumn.AllowEdit = false;
            this.gridColumn_ActiveFlag.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.True;
            this.gridColumn_ActiveFlag.OptionsColumn.ShowCaption = false;
            this.gridColumn_ActiveFlag.OptionsColumn.ShowInCustomizationForm = false;
            // 
            // gridColumn_id
            // 
            this.gridColumn_id.AppearanceCell.Options.UseTextOptions = true;
            this.gridColumn_id.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.gridColumn_id.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColumn_id.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.gridColumn_id.Caption = "ID";
            this.gridColumn_id.FieldName = "Id";
            this.gridColumn_id.Name = "gridColumn_id";
            this.gridColumn_id.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.True;
            this.gridColumn_id.Visible = true;
            this.gridColumn_id.VisibleIndex = 0;
            this.gridColumn_id.Width = 47;
            // 
            // gridColumn_name
            // 
            this.gridColumn_name.Caption = "Name";
            this.gridColumn_name.FieldName = "name";
            this.gridColumn_name.Name = "gridColumn_name";
            this.gridColumn_name.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.True;
            this.gridColumn_name.Visible = true;
            this.gridColumn_name.VisibleIndex = 1;
            this.gridColumn_name.Width = 205;
            // 
            // gridColumn_median_income
            // 
            this.gridColumn_median_income.AppearanceCell.Options.UseTextOptions = true;
            this.gridColumn_median_income.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.gridColumn_median_income.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColumn_median_income.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.gridColumn_median_income.Caption = "Median Income";
            this.gridColumn_median_income.DisplayFormat.FormatString = "{0:c}";
            this.gridColumn_median_income.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.gridColumn_median_income.FieldName = "median_income";
            this.gridColumn_median_income.GroupFormat.FormatString = "{0:c}";
            this.gridColumn_median_income.GroupFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.gridColumn_median_income.Name = "gridColumn_median_income";
            this.gridColumn_median_income.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.True;
            this.gridColumn_median_income.Visible = true;
            this.gridColumn_median_income.VisibleIndex = 3;
            this.gridColumn_median_income.Width = 63;
            // 
            // gridColumn_fips
            // 
            this.gridColumn_fips.Caption = "FIPS";
            this.gridColumn_fips.FieldName = "fips";
            this.gridColumn_fips.Name = "gridColumn_fips";
            this.gridColumn_fips.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.True;
            this.gridColumn_fips.Width = 58;
            // 
            // gridColumn_state
            // 
            this.gridColumn_state.AppearanceCell.Options.UseTextOptions = true;
            this.gridColumn_state.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.gridColumn_state.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColumn_state.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.gridColumn_state.Caption = "State";
            this.gridColumn_state.DisplayFormat.FormatString = "f0";
            this.gridColumn_state.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Custom;
            this.gridColumn_state.FieldName = "state";
            this.gridColumn_state.GroupFormat.FormatString = "f0";
            this.gridColumn_state.GroupFormat.FormatType = DevExpress.Utils.FormatType.Custom;
            this.gridColumn_state.GroupInterval = DevExpress.XtraGrid.ColumnGroupInterval.DisplayText;
            this.gridColumn_state.Name = "gridColumn_state";
            this.gridColumn_state.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.True;
            this.gridColumn_state.SortMode = DevExpress.XtraGrid.ColumnSortMode.DisplayText;
            this.gridColumn_state.Visible = true;
            this.gridColumn_state.VisibleIndex = 2;
            this.gridColumn_state.Width = 43;
            // 
            // gridColumn_default
            // 
            this.gridColumn_default.AppearanceCell.Options.UseTextOptions = true;
            this.gridColumn_default.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumn_default.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColumn_default.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumn_default.Caption = "Default";
            this.gridColumn_default.FieldName = "Default";
            this.gridColumn_default.Name = "gridColumn_default";
            this.gridColumn_default.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.True;
            this.gridColumn_default.Visible = true;
            this.gridColumn_default.VisibleIndex = 4;
            this.gridColumn_default.Width = 58;
            // 
            // gridColumn_district
            // 
            this.gridColumn_district.Caption = "District";
            this.gridColumn_district.DisplayFormat.FormatString = "f0";
            this.gridColumn_district.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.gridColumn_district.FieldName = "bankruptcy_district";
            this.gridColumn_district.GroupFormat.FormatString = "f0";
            this.gridColumn_district.GroupFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.gridColumn_district.Name = "gridColumn_district";
            this.gridColumn_district.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.True;
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.ClientSize = new System.Drawing.Size(528, 294);
            this.Name = "MainForm";
            this.Text = "List of Counties";
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl1)).EndInit();
            this.layoutControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem_simpleButton_Cancel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem_SimpleButton_Edit)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem_simpleButton_New)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.popupMenu1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DxErrorProvider1)).EndInit();
            this.ResumeLayout(false);

        }

        private DevExpress.XtraGrid.Columns.GridColumn gridColumn_id;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn_name;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn_median_income;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn_fips;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn_state;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn_default;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn_ActiveFlag;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn_district;
    }
}