using System;
using System.Windows.Forms;
using System.Drawing;
using DebtPlus.LINQ;
using System.Linq;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Diagnostics;
using DebtPlus.UI.Common.Templates;

namespace DebtPlus.UI.TableAdministration.CS.Geography.Countries
{
	partial class EditForm
	{
		//Form overrides dispose to clean up the component list.
		[System.Diagnostics.DebuggerNonUserCode]
		protected override void Dispose(bool disposing)
		{
			try 
            {
				if (disposing)
                {
                    if (components != null) components.Dispose();
				}
                components = null;
            }
    	    finally
            {
				base.Dispose(disposing);
			}
		}

		//Required by the Windows Form Designer
		private System.ComponentModel.IContainer components = null;

		//NOTE: The following procedure is required by the Windows Form Designer
		//It can be modified using the Windows Form Designer.  
		//Do not modify it using the code editor.
		[System.Diagnostics.DebuggerStepThrough]
		private void InitializeComponent()
		{
            this.LabelControl1 = new DevExpress.XtraEditors.LabelControl();
            this.LabelControl_country = new DevExpress.XtraEditors.LabelControl();
            this.LabelControl3 = new DevExpress.XtraEditors.LabelControl();
            this.LabelControl4 = new DevExpress.XtraEditors.LabelControl();
            this.CheckEdit_default = new DevExpress.XtraEditors.CheckEdit();
            this.CheckEdit_ActiveFlag = new DevExpress.XtraEditors.CheckEdit();
            this.TextEdit_country_code = new DevExpress.XtraEditors.TextEdit();
            this.TextEdit_description = new DevExpress.XtraEditors.TextEdit();
            ((System.ComponentModel.ISupportInitialize)(this.DxErrorProvider1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.CheckEdit_default.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.CheckEdit_ActiveFlag.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TextEdit_country_code.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TextEdit_description.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // simpleButton_OK
            // 
            this.simpleButton_OK.Location = new System.Drawing.Point(314, 43);
            this.simpleButton_OK.TabIndex = 8;
            // 
            // simpleButton_Cancel
            // 
            this.simpleButton_Cancel.Location = new System.Drawing.Point(314, 86);
            this.simpleButton_Cancel.TabIndex = 9;
            // 
            // DefaultLookAndFeel1
            // 
            this.DefaultLookAndFeel1.LookAndFeel.SkinName = "McSkin";
            // 
            // LabelControl1
            // 
            this.LabelControl1.Location = new System.Drawing.Point(13, 13);
            this.LabelControl1.Name = "LabelControl1";
            this.LabelControl1.Size = new System.Drawing.Size(48, 13);
            this.LabelControl1.TabIndex = 0;
            this.LabelControl1.Text = "Record ID";
            // 
            // LabelControl_country
            // 
            this.LabelControl_country.Location = new System.Drawing.Point(85, 12);
            this.LabelControl_country.Name = "LabelControl_country";
            this.LabelControl_country.Size = new System.Drawing.Size(23, 13);
            this.LabelControl_country.TabIndex = 1;
            this.LabelControl_country.Text = "NEW";
            // 
            // LabelControl3
            // 
            this.LabelControl3.Location = new System.Drawing.Point(12, 43);
            this.LabelControl3.Name = "LabelControl3";
            this.LabelControl3.Size = new System.Drawing.Size(53, 13);
            this.LabelControl3.TabIndex = 2;
            this.LabelControl3.Text = "Description";
            // 
            // LabelControl4
            // 
            this.LabelControl4.Location = new System.Drawing.Point(12, 69);
            this.LabelControl4.Name = "LabelControl4";
            this.LabelControl4.Size = new System.Drawing.Size(67, 13);
            this.LabelControl4.TabIndex = 4;
            this.LabelControl4.Text = "Country Code";
            // 
            // CheckEdit_default
            // 
            this.CheckEdit_default.Location = new System.Drawing.Point(10, 105);
            this.CheckEdit_default.Name = "CheckEdit_default";
            this.CheckEdit_default.Properties.Caption = "Default Country";
            this.CheckEdit_default.Size = new System.Drawing.Size(173, 20);
            this.CheckEdit_default.TabIndex = 6;
            this.CheckEdit_default.ToolTip = "This is the default country for new records";
            // 
            // CheckEdit_ActiveFlag
            // 
            this.CheckEdit_ActiveFlag.Location = new System.Drawing.Point(11, 130);
            this.CheckEdit_ActiveFlag.Name = "CheckEdit_ActiveFlag";
            this.CheckEdit_ActiveFlag.Properties.Caption = "Active Entry";
            this.CheckEdit_ActiveFlag.Size = new System.Drawing.Size(175, 20);
            this.CheckEdit_ActiveFlag.TabIndex = 7;
            this.CheckEdit_ActiveFlag.ToolTip = "Allow the country to be choosen from a list of countries";
            // 
            // TextEdit_country_code
            // 
            this.TextEdit_country_code.Location = new System.Drawing.Point(84, 66);
            this.TextEdit_country_code.Name = "TextEdit_country_code";
            this.TextEdit_country_code.Properties.Appearance.Options.UseTextOptions = true;
            this.TextEdit_country_code.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.TextEdit_country_code.Properties.DisplayFormat.FormatString = "f0";
            this.TextEdit_country_code.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.TextEdit_country_code.Properties.EditFormat.FormatString = "f0";
            this.TextEdit_country_code.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.TextEdit_country_code.Properties.Mask.BeepOnError = true;
            this.TextEdit_country_code.Properties.Mask.EditMask = "\\d+";
            this.TextEdit_country_code.Size = new System.Drawing.Size(59, 20);
            this.TextEdit_country_code.TabIndex = 5;
            this.TextEdit_country_code.ToolTip = "Telephone dialing country code for long distance dialing of telephone numbers";
            // 
            // TextEdit_description
            // 
            this.TextEdit_description.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.TextEdit_description.Location = new System.Drawing.Point(84, 40);
            this.TextEdit_description.Name = "TextEdit_description";
            this.TextEdit_description.Size = new System.Drawing.Size(224, 20);
            this.TextEdit_description.TabIndex = 3;
            this.TextEdit_description.ToolTip = "Name used to format addresses and in the list of countries";
            // 
            // EditForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(410, 160);
            this.Controls.Add(this.TextEdit_description);
            this.Controls.Add(this.TextEdit_country_code);
            this.Controls.Add(this.CheckEdit_ActiveFlag);
            this.Controls.Add(this.CheckEdit_default);
            this.Controls.Add(this.LabelControl4);
            this.Controls.Add(this.LabelControl3);
            this.Controls.Add(this.LabelControl_country);
            this.Controls.Add(this.LabelControl1);
            this.Name = "EditForm";
            this.Text = "Country Information";
            this.Controls.SetChildIndex(this.LabelControl1, 0);
            this.Controls.SetChildIndex(this.simpleButton_OK, 0);
            this.Controls.SetChildIndex(this.simpleButton_Cancel, 0);
            this.Controls.SetChildIndex(this.LabelControl_country, 0);
            this.Controls.SetChildIndex(this.LabelControl3, 0);
            this.Controls.SetChildIndex(this.LabelControl4, 0);
            this.Controls.SetChildIndex(this.CheckEdit_default, 0);
            this.Controls.SetChildIndex(this.CheckEdit_ActiveFlag, 0);
            this.Controls.SetChildIndex(this.TextEdit_country_code, 0);
            this.Controls.SetChildIndex(this.TextEdit_description, 0);
            ((System.ComponentModel.ISupportInitialize)(this.DxErrorProvider1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.CheckEdit_default.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.CheckEdit_ActiveFlag.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TextEdit_country_code.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TextEdit_description.Properties)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

		}

		private DevExpress.XtraEditors.LabelControl LabelControl1;
		private DevExpress.XtraEditors.LabelControl LabelControl_country;
		private DevExpress.XtraEditors.LabelControl LabelControl3;
		private DevExpress.XtraEditors.LabelControl LabelControl4;
		private DevExpress.XtraEditors.CheckEdit CheckEdit_default;
		private DevExpress.XtraEditors.CheckEdit CheckEdit_ActiveFlag;
		private DevExpress.XtraEditors.TextEdit TextEdit_country_code;
		private DevExpress.XtraEditors.TextEdit TextEdit_description;
	}
}
