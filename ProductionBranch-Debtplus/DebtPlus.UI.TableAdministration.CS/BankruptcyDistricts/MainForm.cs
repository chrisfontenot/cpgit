﻿using System;
using System.Linq;
using DebtPlus.LINQ;
using DebtPlus.UI.Common;

namespace DebtPlus.UI.TableAdministration.CS.bankruptcy_districts
{
    public partial class MainForm : Templates.MainForm
    {
        // The record collection being edited
        private System.Collections.Generic.List<bankruptcy_district> colRecords;

        private BusinessContext bc = new BusinessContext();

        public MainForm()
        {
            InitializeComponent();
            RegisterHandlers();
        }

        private void RegisterHandlers()
        {
            this.Load += MainForm_Load;
        }

        private void UnRegisterHandlers()
        {
            this.Load -= MainForm_Load;
        }

        private void MainForm_Load(object sender, EventArgs e)
        {
            UnRegisterHandlers();
            try
            {
                using (var cm = new CursorManager())
                {
                    colRecords = bc.bankruptcy_districts.ToList();
                    gridControl1.DataSource = colRecords;
                    gridView1.BestFitColumns();
                }
            }
            finally
            {
                RegisterHandlers();
            }
        }

        /// <summary>
        /// Update the record in the database
        /// </summary>
        /// <param name="obj"></param>
        protected override void UpdateRecord(object obj)
        {
            var record = obj as bankruptcy_district;
            if (record == null)
            {
                return;
            }

            bool previousDefault = record.Default;
            using (var frm = new EditForm(record))
            {
                if (frm.ShowDialog() != System.Windows.Forms.DialogResult.OK)
                {
                    return;
                }
            }

            // Remove the previous default item if the new one is now the default
            if (record.Default && !previousDefault)
            {
                foreach (var defaultRecod in colRecords.Where(s => s.Default))
                {
                    defaultRecod.Default = false;
                }
                record.Default = true;
            }

            try
            {
                bc.SubmitChanges();
                gridView1.RefreshData();
            }
            catch (System.Data.SqlClient.SqlException ex)
            {
                DebtPlus.UI.Common.ErrorHandling.HandleErrors(ex);
            }
        }

        /// <summary>
        /// Create a new record for the database
        /// </summary>
        protected override void CreateRecord()
        {
            // Update the new record
            var record = DebtPlus.LINQ.Factory.Manufacture_bankruptcy_district();
            using (var frm = new EditForm(record))
            {
                if (frm.ShowDialog() != System.Windows.Forms.DialogResult.OK)
                {
                    return;
                }
            }

            // Remove the previous default item if the new one is now the default
            if (record.Default)
            {
                foreach (var defaultRecod in colRecords.Where(s => s.Default))
                {
                    defaultRecod.Default = false;
                }
                record.Default = true;
            }

            // Add the new record to the collection
            bc.bankruptcy_districts.InsertOnSubmit(record);
            colRecords.Add(record);

            try
            {
                bc.SubmitChanges();
                gridView1.RefreshData();
            }
            catch (System.Data.SqlClient.SqlException ex)
            {
                DebtPlus.UI.Common.ErrorHandling.HandleErrors(ex);
            }
        }

        /// <summary>
        /// Delete the current record from the database
        /// </summary>
        /// <param name="obj"></param>
        protected override void DeleteRecord(object obj)
        {
            // Find the record in the grid
            var record = obj as bankruptcy_district;
            if (record != null)
            {
                return;
            }

            // Ask for cofirmation
            if (DebtPlus.Data.Prompts.RequestConfirmation_Delete() != System.Windows.Forms.DialogResult.Yes)
            {
                return;
            }

            // Delete the record
            bc.bankruptcy_districts.DeleteOnSubmit(record);
            colRecords.Remove(record);

            try
            {
                bc.SubmitChanges();
                gridView1.RefreshData();
            }
            catch (System.Data.SqlClient.SqlException ex)
            {
                DebtPlus.UI.Common.ErrorHandling.HandleErrors(ex);
            }
        }
    }
}