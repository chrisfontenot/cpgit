﻿using System;
using DebtPlus.LINQ;

namespace DebtPlus.UI.TableAdministration.CS.bankruptcy_districts
{
    internal partial class EditForm : Templates.EditTemplateForm
    {
        private bankruptcy_district record;

        internal EditForm()
            : base()
        {
            InitializeComponent();
        }

        internal EditForm(bankruptcy_district record)
            : this()
        {
            this.record = record;
            RegisterHandlers();
        }

        private void RegisterHandlers()
        {
            this.Load += EditForm_Load;
            this.TextEdit_description.TextChanged += Form_Changed;
        }

        private void UnRegisterHandlers()
        {
            this.Load -= EditForm_Load;
            this.TextEdit_description.TextChanged -= Form_Changed;
        }

        private void EditForm_Load(object sender, EventArgs e)
        {
            UnRegisterHandlers();
            try
            {
                LabelControl_ID.Text = String.Empty;
                if (record.Id > 0)
                {
                    LabelControl_ID.Text = record.Id.ToString();
                }
                this.TextEdit_description.EditValue = record.Description;
                this.CheckEdit_ActiveFlag.Checked = record.ActiveFlag;
                this.CheckEdit_Default.Checked = record.Default;
            }
            finally
            {
                RegisterHandlers();
            }
            simpleButton_OK.Enabled = this.TextEdit_description.Text != String.Empty;
        }

        private void Form_Changed(object sender, EventArgs e)
        {
            simpleButton_OK.Enabled = !string.IsNullOrEmpty(this.TextEdit_description.Text);
        }

        protected override void simpleButton_OK_Click(object sender, EventArgs e)
        {
            record.Description = Convert.ToString(this.TextEdit_description.EditValue);
            record.ActiveFlag = this.CheckEdit_ActiveFlag.Checked;
            record.Default = this.CheckEdit_Default.Checked;

            base.simpleButton_OK_Click(sender, e);
        }
    }
}