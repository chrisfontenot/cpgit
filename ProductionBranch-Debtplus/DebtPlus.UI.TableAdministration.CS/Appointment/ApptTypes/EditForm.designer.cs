﻿namespace DebtPlus.UI.TableAdministration.CS.Appointment.ApptTypes
{
    partial class EditForm
    {
		//Form overrides dispose to clean up the component list.
		[System.Diagnostics.DebuggerNonUserCode]
		protected override void Dispose(bool disposing)
		{
			try 
            {
				if (disposing)
                {
                    if (components != null) components.Dispose();
				}
                components = null;
            }
    	    finally
            {
				base.Dispose(disposing);
			}
		}

		//Required by the Windows Form Designer
		private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.LookUpEdit_contact_type = new DevExpress.XtraEditors.LookUpEdit();
            this.GroupControl2 = new DevExpress.XtraEditors.GroupControl();
            this.CheckEdit_bankruptcy = new DevExpress.XtraEditors.CheckEdit();
            this.CheckEdit_credit = new DevExpress.XtraEditors.CheckEdit();
            this.CheckEdit_housing = new DevExpress.XtraEditors.CheckEdit();
            this.CheckEdit_initial_appt = new DevExpress.XtraEditors.CheckEdit();
            this.LabelControl9 = new DevExpress.XtraEditors.LabelControl();
            this.LookUpEdit_bankruptcy_class = new DevExpress.XtraEditors.LookUpEdit();
            this.LabelControl8 = new DevExpress.XtraEditors.LabelControl();
            this.LookUpEdit_missed_retention_event = new DevExpress.XtraEditors.LookUpEdit();
            this.GroupControl1 = new DevExpress.XtraEditors.GroupControl();
            this.LabelControl7 = new DevExpress.XtraEditors.LabelControl();
            this.LabelControl6 = new DevExpress.XtraEditors.LabelControl();
            this.LabelControl5 = new DevExpress.XtraEditors.LabelControl();
            this.LookUpEdit_reschedule_letter = new DevExpress.XtraEditors.LookUpEdit();
            this.LookUpEdit_cancel_letter = new DevExpress.XtraEditors.LookUpEdit();
            this.LookUpEdit_create_letter = new DevExpress.XtraEditors.LookUpEdit();
            this.LabelControl4 = new DevExpress.XtraEditors.LabelControl();
            this.SpinEdit_appt_duration = new DevExpress.XtraEditors.SpinEdit();
            this.LabelControl2 = new DevExpress.XtraEditors.LabelControl();
            this.TextEdit_appt_name = new DevExpress.XtraEditors.TextEdit();
            this.LabelControl3 = new DevExpress.XtraEditors.LabelControl();
            this.LabelControl_ID = new DevExpress.XtraEditors.LabelControl();
            this.LabelControl1 = new DevExpress.XtraEditors.LabelControl();
            ((System.ComponentModel.ISupportInitialize)(this.DxErrorProvider1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LookUpEdit_contact_type.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.GroupControl2)).BeginInit();
            this.GroupControl2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.CheckEdit_bankruptcy.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.CheckEdit_credit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.CheckEdit_housing.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.CheckEdit_initial_appt.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LookUpEdit_bankruptcy_class.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LookUpEdit_missed_retention_event.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.GroupControl1)).BeginInit();
            this.GroupControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.LookUpEdit_reschedule_letter.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LookUpEdit_cancel_letter.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LookUpEdit_create_letter.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.SpinEdit_appt_duration.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TextEdit_appt_name.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // simpleButton_Cancel
            // 
            this.simpleButton_Cancel.Location = new System.Drawing.Point(349, 67);
            // 
            // simpleButton_OK
            // 
            this.simpleButton_OK.Location = new System.Drawing.Point(349, 35);
            // 
            // DefaultLookAndFeel1
            // 
            this.DefaultLookAndFeel1.LookAndFeel.SkinName = "McSkin";
            // 
            // LookUpEdit_contact_type
            // 
            this.LookUpEdit_contact_type.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.LookUpEdit_contact_type.Location = new System.Drawing.Point(115, 94);
            this.LookUpEdit_contact_type.Name = "LookUpEdit_contact_type";
            this.LookUpEdit_contact_type.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.True;
            this.LookUpEdit_contact_type.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LookUpEdit_contact_type.Properties.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("Id", "ID", 20, DevExpress.Utils.FormatType.None, "", false, DevExpress.Utils.HorzAlignment.Default),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("description", "Description")});
            this.LookUpEdit_contact_type.Properties.DisplayMember = "description";
            this.LookUpEdit_contact_type.Properties.NullText = "";
            this.LookUpEdit_contact_type.Properties.ShowFooter = false;
            this.LookUpEdit_contact_type.Properties.ShowHeader = false;
            this.LookUpEdit_contact_type.Properties.SortColumnIndex = 1;
            this.LookUpEdit_contact_type.Properties.ValueMember = "Id";
            this.LookUpEdit_contact_type.Size = new System.Drawing.Size(208, 20);
            this.LookUpEdit_contact_type.TabIndex = 29;
            // 
            // GroupControl2
            // 
            this.GroupControl2.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.GroupControl2.Controls.Add(this.CheckEdit_bankruptcy);
            this.GroupControl2.Controls.Add(this.CheckEdit_credit);
            this.GroupControl2.Controls.Add(this.CheckEdit_housing);
            this.GroupControl2.Controls.Add(this.CheckEdit_initial_appt);
            this.GroupControl2.Location = new System.Drawing.Point(8, 291);
            this.GroupControl2.Name = "GroupControl2";
            this.GroupControl2.Size = new System.Drawing.Size(326, 71);
            this.GroupControl2.TabIndex = 35;
            this.GroupControl2.Text = "Counselor Certifications Required";
            // 
            // CheckEdit_bankruptcy
            // 
            this.CheckEdit_bankruptcy.Location = new System.Drawing.Point(146, 42);
            this.CheckEdit_bankruptcy.Name = "CheckEdit_bankruptcy";
            this.CheckEdit_bankruptcy.Properties.Caption = "Bankruptcy Appointment";
            this.CheckEdit_bankruptcy.Size = new System.Drawing.Size(170, 20);
            this.CheckEdit_bankruptcy.TabIndex = 3;
            // 
            // CheckEdit_credit
            // 
            this.CheckEdit_credit.Location = new System.Drawing.Point(146, 23);
            this.CheckEdit_credit.Name = "CheckEdit_credit";
            this.CheckEdit_credit.Properties.Caption = "Credit Appointment";
            this.CheckEdit_credit.Size = new System.Drawing.Size(170, 20);
            this.CheckEdit_credit.TabIndex = 1;
            // 
            // CheckEdit_housing
            // 
            this.CheckEdit_housing.Location = new System.Drawing.Point(5, 42);
            this.CheckEdit_housing.Name = "CheckEdit_housing";
            this.CheckEdit_housing.Properties.Caption = "Housing Appointment";
            this.CheckEdit_housing.Size = new System.Drawing.Size(135, 20);
            this.CheckEdit_housing.TabIndex = 2;
            // 
            // CheckEdit_initial_appt
            // 
            this.CheckEdit_initial_appt.Location = new System.Drawing.Point(5, 23);
            this.CheckEdit_initial_appt.Name = "CheckEdit_initial_appt";
            this.CheckEdit_initial_appt.Properties.Caption = "Initial Appointment";
            this.CheckEdit_initial_appt.Size = new System.Drawing.Size(135, 20);
            this.CheckEdit_initial_appt.TabIndex = 0;
            // 
            // LabelControl9
            // 
            this.LabelControl9.Location = new System.Drawing.Point(8, 149);
            this.LabelControl9.Name = "LabelControl9";
            this.LabelControl9.Size = new System.Drawing.Size(82, 13);
            this.LabelControl9.TabIndex = 32;
            this.LabelControl9.Text = "Bankruptcy Class";
            // 
            // LookUpEdit_bankruptcy_class
            // 
            this.LookUpEdit_bankruptcy_class.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.LookUpEdit_bankruptcy_class.Location = new System.Drawing.Point(115, 146);
            this.LookUpEdit_bankruptcy_class.Name = "LookUpEdit_bankruptcy_class";
            this.LookUpEdit_bankruptcy_class.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.True;
            this.LookUpEdit_bankruptcy_class.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LookUpEdit_bankruptcy_class.Properties.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("Id", "ID", 20, DevExpress.Utils.FormatType.Numeric, "f0", false, DevExpress.Utils.HorzAlignment.Default),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("description", "Description")});
            this.LookUpEdit_bankruptcy_class.Properties.DisplayMember = "description";
            this.LookUpEdit_bankruptcy_class.Properties.NullText = "";
            this.LookUpEdit_bankruptcy_class.Properties.ShowFooter = false;
            this.LookUpEdit_bankruptcy_class.Properties.ShowHeader = false;
            this.LookUpEdit_bankruptcy_class.Properties.SortColumnIndex = 1;
            this.LookUpEdit_bankruptcy_class.Properties.ValueMember = "Id";
            this.LookUpEdit_bankruptcy_class.Size = new System.Drawing.Size(208, 20);
            this.LookUpEdit_bankruptcy_class.TabIndex = 33;
            // 
            // LabelControl8
            // 
            this.LabelControl8.Location = new System.Drawing.Point(8, 123);
            this.LabelControl8.Name = "LabelControl8";
            this.LabelControl8.Size = new System.Drawing.Size(78, 13);
            this.LabelControl8.TabIndex = 30;
            this.LabelControl8.Text = "Retention Event";
            // 
            // LookUpEdit_missed_retention_event
            // 
            this.LookUpEdit_missed_retention_event.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.LookUpEdit_missed_retention_event.Location = new System.Drawing.Point(115, 120);
            this.LookUpEdit_missed_retention_event.Name = "LookUpEdit_missed_retention_event";
            this.LookUpEdit_missed_retention_event.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.True;
            this.LookUpEdit_missed_retention_event.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LookUpEdit_missed_retention_event.Properties.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("Id", "ID", 20, DevExpress.Utils.FormatType.Numeric, "f0", false, DevExpress.Utils.HorzAlignment.Default),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("description", "Description")});
            this.LookUpEdit_missed_retention_event.Properties.DisplayMember = "description";
            this.LookUpEdit_missed_retention_event.Properties.NullText = "";
            this.LookUpEdit_missed_retention_event.Properties.ShowFooter = false;
            this.LookUpEdit_missed_retention_event.Properties.ShowHeader = false;
            this.LookUpEdit_missed_retention_event.Properties.SortColumnIndex = 1;
            this.LookUpEdit_missed_retention_event.Properties.ValueMember = "Id";
            this.LookUpEdit_missed_retention_event.Size = new System.Drawing.Size(208, 20);
            this.LookUpEdit_missed_retention_event.TabIndex = 31;
            // 
            // GroupControl1
            // 
            this.GroupControl1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.GroupControl1.Controls.Add(this.LabelControl7);
            this.GroupControl1.Controls.Add(this.LabelControl6);
            this.GroupControl1.Controls.Add(this.LabelControl5);
            this.GroupControl1.Controls.Add(this.LookUpEdit_reschedule_letter);
            this.GroupControl1.Controls.Add(this.LookUpEdit_cancel_letter);
            this.GroupControl1.Controls.Add(this.LookUpEdit_create_letter);
            this.GroupControl1.Location = new System.Drawing.Point(8, 181);
            this.GroupControl1.Name = "GroupControl1";
            this.GroupControl1.Size = new System.Drawing.Size(326, 104);
            this.GroupControl1.TabIndex = 34;
            this.GroupControl1.Text = "Appointment Letters";
            // 
            // LabelControl7
            // 
            this.LabelControl7.Location = new System.Drawing.Point(5, 78);
            this.LabelControl7.Name = "LabelControl7";
            this.LabelControl7.Size = new System.Drawing.Size(55, 13);
            this.LabelControl7.TabIndex = 4;
            this.LabelControl7.Text = "Reschedule";
            // 
            // LabelControl6
            // 
            this.LabelControl6.Location = new System.Drawing.Point(5, 52);
            this.LabelControl6.Name = "LabelControl6";
            this.LabelControl6.Size = new System.Drawing.Size(32, 13);
            this.LabelControl6.TabIndex = 2;
            this.LabelControl6.Text = "Cancel";
            // 
            // LabelControl5
            // 
            this.LabelControl5.Location = new System.Drawing.Point(5, 26);
            this.LabelControl5.Name = "LabelControl5";
            this.LabelControl5.Size = new System.Drawing.Size(23, 13);
            this.LabelControl5.TabIndex = 0;
            this.LabelControl5.Text = "Book";
            // 
            // LookUpEdit_reschedule_letter
            // 
            this.LookUpEdit_reschedule_letter.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.LookUpEdit_reschedule_letter.Location = new System.Drawing.Point(107, 75);
            this.LookUpEdit_reschedule_letter.Name = "LookUpEdit_reschedule_letter";
            this.LookUpEdit_reschedule_letter.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.True;
            this.LookUpEdit_reschedule_letter.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LookUpEdit_reschedule_letter.Properties.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("letter_code", "ID", 20, DevExpress.Utils.FormatType.None, "", false, DevExpress.Utils.HorzAlignment.Default),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("description", "Description")});
            this.LookUpEdit_reschedule_letter.Properties.DisplayMember = "description";
            this.LookUpEdit_reschedule_letter.Properties.NullText = "";
            this.LookUpEdit_reschedule_letter.Properties.ShowFooter = false;
            this.LookUpEdit_reschedule_letter.Properties.ShowHeader = false;
            this.LookUpEdit_reschedule_letter.Properties.SortColumnIndex = 1;
            this.LookUpEdit_reschedule_letter.Properties.ValueMember = "letter_code";
            this.LookUpEdit_reschedule_letter.Size = new System.Drawing.Size(208, 20);
            this.LookUpEdit_reschedule_letter.TabIndex = 5;
            // 
            // LookUpEdit_cancel_letter
            // 
            this.LookUpEdit_cancel_letter.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.LookUpEdit_cancel_letter.Location = new System.Drawing.Point(107, 49);
            this.LookUpEdit_cancel_letter.Name = "LookUpEdit_cancel_letter";
            this.LookUpEdit_cancel_letter.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.True;
            this.LookUpEdit_cancel_letter.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LookUpEdit_cancel_letter.Properties.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("letter_code", "ID", 20, DevExpress.Utils.FormatType.None, "", false, DevExpress.Utils.HorzAlignment.Default),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("description", "Description")});
            this.LookUpEdit_cancel_letter.Properties.DisplayMember = "description";
            this.LookUpEdit_cancel_letter.Properties.NullText = "";
            this.LookUpEdit_cancel_letter.Properties.ShowFooter = false;
            this.LookUpEdit_cancel_letter.Properties.ShowHeader = false;
            this.LookUpEdit_cancel_letter.Properties.SortColumnIndex = 1;
            this.LookUpEdit_cancel_letter.Properties.ValueMember = "letter_code";
            this.LookUpEdit_cancel_letter.Size = new System.Drawing.Size(208, 20);
            this.LookUpEdit_cancel_letter.TabIndex = 3;
            // 
            // LookUpEdit_create_letter
            // 
            this.LookUpEdit_create_letter.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.LookUpEdit_create_letter.Location = new System.Drawing.Point(107, 23);
            this.LookUpEdit_create_letter.Name = "LookUpEdit_create_letter";
            this.LookUpEdit_create_letter.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.True;
            this.LookUpEdit_create_letter.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LookUpEdit_create_letter.Properties.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("letter_code", "ID", 20, DevExpress.Utils.FormatType.None, "", false, DevExpress.Utils.HorzAlignment.Default),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("description", "Description")});
            this.LookUpEdit_create_letter.Properties.DisplayMember = "description";
            this.LookUpEdit_create_letter.Properties.NullText = "";
            this.LookUpEdit_create_letter.Properties.ShowFooter = false;
            this.LookUpEdit_create_letter.Properties.ShowHeader = false;
            this.LookUpEdit_create_letter.Properties.SortColumnIndex = 1;
            this.LookUpEdit_create_letter.Properties.ValueMember = "letter_code";
            this.LookUpEdit_create_letter.Size = new System.Drawing.Size(208, 20);
            this.LookUpEdit_create_letter.TabIndex = 1;
            // 
            // LabelControl4
            // 
            this.LabelControl4.Location = new System.Drawing.Point(8, 96);
            this.LabelControl4.Name = "LabelControl4";
            this.LabelControl4.Size = new System.Drawing.Size(65, 13);
            this.LabelControl4.TabIndex = 28;
            this.LabelControl4.Text = "Contact Type";
            // 
            // SpinEdit_appt_duration
            // 
            this.SpinEdit_appt_duration.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.SpinEdit_appt_duration.Location = new System.Drawing.Point(115, 67);
            this.SpinEdit_appt_duration.Name = "SpinEdit_appt_duration";
            this.SpinEdit_appt_duration.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.SpinEdit_appt_duration.Properties.Increment = new decimal(new int[] {
            15,
            0,
            0,
            0});
            this.SpinEdit_appt_duration.Properties.IsFloatValue = false;
            this.SpinEdit_appt_duration.Properties.Mask.EditMask = "N00";
            this.SpinEdit_appt_duration.Properties.MaxValue = new decimal(new int[] {
            3599,
            0,
            0,
            0});
            this.SpinEdit_appt_duration.Size = new System.Drawing.Size(62, 20);
            this.SpinEdit_appt_duration.TabIndex = 27;
            // 
            // LabelControl2
            // 
            this.LabelControl2.Location = new System.Drawing.Point(8, 70);
            this.LabelControl2.Name = "LabelControl2";
            this.LabelControl2.Size = new System.Drawing.Size(41, 13);
            this.LabelControl2.TabIndex = 26;
            this.LabelControl2.Text = "Duration";
            // 
            // TextEdit_appt_name
            // 
            this.TextEdit_appt_name.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.TextEdit_appt_name.Location = new System.Drawing.Point(115, 41);
            this.TextEdit_appt_name.Name = "TextEdit_appt_name";
            this.TextEdit_appt_name.Properties.MaxLength = 50;
            this.TextEdit_appt_name.Size = new System.Drawing.Size(208, 20);
            this.TextEdit_appt_name.TabIndex = 25;
            // 
            // LabelControl3
            // 
            this.LabelControl3.Location = new System.Drawing.Point(9, 44);
            this.LabelControl3.Name = "LabelControl3";
            this.LabelControl3.Size = new System.Drawing.Size(53, 13);
            this.LabelControl3.TabIndex = 24;
            this.LabelControl3.Text = "Description";
            // 
            // LabelControl_ID
            // 
            this.LabelControl_ID.Location = new System.Drawing.Point(115, 11);
            this.LabelControl_ID.Name = "LabelControl_ID";
            this.LabelControl_ID.Size = new System.Drawing.Size(23, 13);
            this.LabelControl_ID.TabIndex = 23;
            this.LabelControl_ID.Text = "NEW";
            // 
            // LabelControl1
            // 
            this.LabelControl1.Location = new System.Drawing.Point(9, 11);
            this.LabelControl1.Name = "LabelControl1";
            this.LabelControl1.Size = new System.Drawing.Size(11, 13);
            this.LabelControl1.TabIndex = 22;
            this.LabelControl1.Text = "ID";
            // 
            // EditForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(433, 372);
            this.Controls.Add(this.LookUpEdit_contact_type);
            this.Controls.Add(this.GroupControl2);
            this.Controls.Add(this.LabelControl9);
            this.Controls.Add(this.LookUpEdit_bankruptcy_class);
            this.Controls.Add(this.LabelControl8);
            this.Controls.Add(this.LookUpEdit_missed_retention_event);
            this.Controls.Add(this.GroupControl1);
            this.Controls.Add(this.LabelControl4);
            this.Controls.Add(this.SpinEdit_appt_duration);
            this.Controls.Add(this.LabelControl2);
            this.Controls.Add(this.TextEdit_appt_name);
            this.Controls.Add(this.LabelControl3);
            this.Controls.Add(this.LabelControl_ID);
            this.Controls.Add(this.LabelControl1);
            this.Name = "EditForm";
            this.Text = "Appointment Type";
            this.Controls.SetChildIndex(this.simpleButton_OK, 0);
            this.Controls.SetChildIndex(this.simpleButton_Cancel, 0);
            this.Controls.SetChildIndex(this.LabelControl1, 0);
            this.Controls.SetChildIndex(this.LabelControl_ID, 0);
            this.Controls.SetChildIndex(this.LabelControl3, 0);
            this.Controls.SetChildIndex(this.TextEdit_appt_name, 0);
            this.Controls.SetChildIndex(this.LabelControl2, 0);
            this.Controls.SetChildIndex(this.SpinEdit_appt_duration, 0);
            this.Controls.SetChildIndex(this.LabelControl4, 0);
            this.Controls.SetChildIndex(this.GroupControl1, 0);
            this.Controls.SetChildIndex(this.LookUpEdit_missed_retention_event, 0);
            this.Controls.SetChildIndex(this.LabelControl8, 0);
            this.Controls.SetChildIndex(this.LookUpEdit_bankruptcy_class, 0);
            this.Controls.SetChildIndex(this.LabelControl9, 0);
            this.Controls.SetChildIndex(this.GroupControl2, 0);
            this.Controls.SetChildIndex(this.LookUpEdit_contact_type, 0);
            ((System.ComponentModel.ISupportInitialize)(this.DxErrorProvider1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LookUpEdit_contact_type.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.GroupControl2)).EndInit();
            this.GroupControl2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.CheckEdit_bankruptcy.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.CheckEdit_credit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.CheckEdit_housing.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.CheckEdit_initial_appt.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LookUpEdit_bankruptcy_class.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LookUpEdit_missed_retention_event.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.GroupControl1)).EndInit();
            this.GroupControl1.ResumeLayout(false);
            this.GroupControl1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.LookUpEdit_reschedule_letter.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LookUpEdit_cancel_letter.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LookUpEdit_create_letter.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.SpinEdit_appt_duration.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TextEdit_appt_name.Properties)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }


        private DevExpress.XtraEditors.LookUpEdit LookUpEdit_contact_type;
        private DevExpress.XtraEditors.GroupControl GroupControl2;
        private DevExpress.XtraEditors.CheckEdit CheckEdit_bankruptcy;
        private DevExpress.XtraEditors.CheckEdit CheckEdit_credit;
        private DevExpress.XtraEditors.CheckEdit CheckEdit_housing;
        private DevExpress.XtraEditors.CheckEdit CheckEdit_initial_appt;
        private DevExpress.XtraEditors.LabelControl LabelControl9;
        private DevExpress.XtraEditors.LookUpEdit LookUpEdit_bankruptcy_class;
        private DevExpress.XtraEditors.LabelControl LabelControl8;
        private DevExpress.XtraEditors.LookUpEdit LookUpEdit_missed_retention_event;
        private DevExpress.XtraEditors.GroupControl GroupControl1;
        private DevExpress.XtraEditors.LabelControl LabelControl7;
        private DevExpress.XtraEditors.LabelControl LabelControl6;
        private DevExpress.XtraEditors.LabelControl LabelControl5;
        private DevExpress.XtraEditors.LookUpEdit LookUpEdit_reschedule_letter;
        private DevExpress.XtraEditors.LookUpEdit LookUpEdit_cancel_letter;
        private DevExpress.XtraEditors.LookUpEdit LookUpEdit_create_letter;
        private DevExpress.XtraEditors.LabelControl LabelControl4;
        private DevExpress.XtraEditors.SpinEdit SpinEdit_appt_duration;
        private DevExpress.XtraEditors.LabelControl LabelControl2;
        private DevExpress.XtraEditors.TextEdit TextEdit_appt_name;
        private DevExpress.XtraEditors.LabelControl LabelControl3;
        private DevExpress.XtraEditors.LabelControl LabelControl_ID;
        private DevExpress.XtraEditors.LabelControl LabelControl1;
    }
}