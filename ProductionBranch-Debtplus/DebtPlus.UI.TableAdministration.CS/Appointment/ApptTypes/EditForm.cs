﻿using System;
using DebtPlus.LINQ;

namespace DebtPlus.UI.TableAdministration.CS.Appointment.ApptTypes
{
    internal partial class EditForm : Templates.EditTemplateForm
    {
        // Current record being edited
        private appt_type record;

        /// <summary>
        /// Initialize the new class instance
        /// </summary>
        internal EditForm()
            : base()
        {
            InitializeComponent();
        }

        /// <summary>
        /// Initialize the new class instance
        /// </summary>
        /// <param name="record"></param>
        internal EditForm(appt_type record)
            : this()
        {
            this.record = record;

            // Set the lookup tables
            LookUpEdit_bankruptcy_class.Properties.DataSource = DebtPlus.LINQ.Cache.BankruptcyClassType.getList();
            LookUpEdit_cancel_letter.Properties.DataSource = DebtPlus.LINQ.Cache.letter_type.getList();
            LookUpEdit_create_letter.Properties.DataSource = DebtPlus.LINQ.Cache.letter_type.getList();
            LookUpEdit_reschedule_letter.Properties.DataSource = DebtPlus.LINQ.Cache.letter_type.getList();
            LookUpEdit_contact_type.Properties.DataSource = DebtPlus.LINQ.Cache.ContactMethodType.getList();
            LookUpEdit_missed_retention_event.Properties.DataSource = DebtPlus.LINQ.Cache.retention_event.getList();

            RegisterHandlers();
        }

        /// <summary>
        /// Add the event handlers to the collection
        /// </summary>
        private void RegisterHandlers()
        {
            Load += EditForm_Load;
            TextEdit_appt_name.EditValueChanged += FormChanged;
        }

        /// <summary>
        /// Remove the event handlers from being processed
        /// </summary>
        private void UnRegisterHandlers()
        {
            Load -= EditForm_Load;
            TextEdit_appt_name.EditValueChanged -= FormChanged;
        }

        /// <summary>
        /// Process the FORM LOAD event
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void EditForm_Load(object sender, System.EventArgs e)
        {
            UnRegisterHandlers();
            try
            {
                LabelControl_ID.Text = record.Id <= 0 ? "NEW" : record.Id.ToString();

                TextEdit_appt_name.EditValue = record.appt_name;
                CheckEdit_bankruptcy.Checked = record.bankruptcy;
                CheckEdit_credit.Checked = record.credit;
                CheckEdit_housing.Checked = record.housing;
                CheckEdit_initial_appt.Checked = record.initial_appt;
                LookUpEdit_bankruptcy_class.EditValue = record.bankruptcy_class;
                LookUpEdit_cancel_letter.EditValue = record.cancel_letter;
                LookUpEdit_contact_type.EditValue = record.contact_type;
                LookUpEdit_create_letter.EditValue = record.create_letter;
                LookUpEdit_missed_retention_event.EditValue = record.missed_retention_event;
                LookUpEdit_reschedule_letter.EditValue = record.reschedule_letter;

                simpleButton_OK.Enabled = !HasErrors();
            }
            finally
            {
                RegisterHandlers();
            }
        }

        /// <summary>
        /// Override the OK button to save the editing values into the record
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected override void simpleButton_OK_Click(object sender, EventArgs e)
        {
            record.appt_name = Convert.ToString(TextEdit_appt_name.EditValue);
            record.bankruptcy = CheckEdit_bankruptcy.Checked;
            record.credit = CheckEdit_credit.Checked;
            record.housing = CheckEdit_housing.Checked;
            record.initial_appt = CheckEdit_initial_appt.Checked;
            record.bankruptcy_class = Convert.ToInt32(LookUpEdit_bankruptcy_class.EditValue);
            record.contact_type = Convert.ToInt32(LookUpEdit_contact_type.EditValue);
            record.cancel_letter = DebtPlus.Utils.Nulls.v_String(LookUpEdit_cancel_letter.EditValue);
            record.create_letter = DebtPlus.Utils.Nulls.v_String(LookUpEdit_create_letter.EditValue);
            record.reschedule_letter = DebtPlus.Utils.Nulls.v_String(LookUpEdit_reschedule_letter.EditValue);
            record.missed_retention_event = DebtPlus.Utils.Nulls.v_Int32(LookUpEdit_missed_retention_event.EditValue);

            base.simpleButton_OK_Click(sender, e);
        }

        private void FormChanged(object sender, EventArgs e)
        {
            simpleButton_OK.Enabled = !HasErrors();
        }

        /// <summary>
        /// Determine if the input form has error(s)
        /// </summary>
        /// <returns></returns>
        private bool HasErrors()
        {
            return TextEdit_appt_name.Text.Trim() == string.Empty;
        }
    }
}