﻿using System;
using System.Linq;
using DebtPlus.LINQ;

namespace DebtPlus.UI.TableAdministration.CS.Appointment.ApptTypes
{
    public partial class MainForm : Templates.MainForm
    {
        /// <summary>
        /// List of the records to be edited in the program
        /// </summary>
        private System.Collections.Generic.List<appt_type> colRecords;

        private BusinessContext bc = null;

        /// <summary>
        /// Initialize the new class
        /// </summary>
        /// <param name="dc">Pointer to the data context in the mainline</param>
        public MainForm()
            : base()
        {
            InitializeComponent();
            RegisterHandlers();

            bc = new BusinessContext();
        }

        private void RegisterHandlers()
        {
            Load += MainForm_Load;
        }

        private void UnRegisterHandlers()
        {
            Load -= MainForm_Load;
        }

        /// <summary>
        /// Process the load event on the form
        /// </summary>
        private void MainForm_Load(object sender, System.EventArgs e)
        {
            using (var cm = new global::DebtPlus.UI.Common.CursorManager())
            {
                try
                {
                    // Set the formatting routines for the contact type
                    gridColumn_contact_type.DisplayFormat.Format = new contactTypeFormatter();
                    gridColumn_contact_type.GroupFormat.Format = new contactTypeFormatter();

                    // Retrieve the list of menu items
                    colRecords = bc.appt_types.ToList();
                    gridControl1.DataSource = colRecords;
                }
                catch (Exception ex)
                {
                    DebtPlus.UI.Common.ErrorHandling.HandleErrors(ex, "Error retrieving database information");
                }
            }
        }

        /// <summary>
        /// Create the new record
        /// </summary>
        protected override void CreateRecord()
        {
            // Allocate the new record to be inserted.
            var record = DebtPlus.LINQ.Factory.Manufacture_appt_type();

            // Edit the new record. If OK then attempt to insert the record.
            using (var frm = new EditForm(record))
            {
                if (frm.ShowDialog() != System.Windows.Forms.DialogResult.OK)
                {
                    return;
                }
            }

            // Clear the default status from the prior record when the new record is default.
            if (record.Default)
            {
                foreach (var defaultRecord in colRecords.Where(s => s.Default))
                {
                    defaultRecord.Default = false;
                }
                record.Default = true;
            }

            try
            {
                // Do the database update
                bc.appt_types.InsertOnSubmit(record);
                bc.SubmitChanges();

                // Add the record to the local list for the grid
                colRecords.Add(record);
                gridView1.RefreshData();
            }
            catch (System.Data.SqlClient.SqlException ex)
            {
                DebtPlus.UI.Common.ErrorHandling.HandleErrors(ex, "Error updating database");
            }
        }

        /// <summary>
        /// Update the existing record
        /// </summary>
        /// <param name="obj">Pointer to the desired record for the operation</param>
        protected override void UpdateRecord(object obj)
        {
            // Find the record
            var record = obj as appt_type;
            if (obj == null)
            {
                return;
            }

            bool previousDefault = record.Default;
            using (var frm = new EditForm(record))
            {
                if (frm.ShowDialog() != System.Windows.Forms.DialogResult.OK)
                {
                    return;
                }
            }

            // If the default is now set then clear the previous one
            if (record.Default && !previousDefault)
            {
                foreach (var defaultRecord in colRecords.Where(s => s.Default))
                {
                    defaultRecord.Default = false;
                }
                record.Default = true;
            }

            // Update the database and refresh the grid
            try
            {
                bc.SubmitChanges();
                gridView1.RefreshData();
            }
            catch (System.Data.SqlClient.SqlException ex)
            {
                DebtPlus.UI.Common.ErrorHandling.HandleErrors(ex, "Error updating database");
            }
        }

        /// <summary>
        /// Delete the indicated record from the database
        /// </summary>
        /// <param name="obj">Pointer to the desired record for the operation</param>
        protected override void DeleteRecord(object obj)
        {
            // Find the record
            var record = obj as appt_type;
            if (obj == null)
            {
                return;
            }

            // Ask for confirmation
            if (DebtPlus.Data.Prompts.RequestConfirmation_Delete() != System.Windows.Forms.DialogResult.Yes)
            {
                return;
            }

            try
            {
                // Delete the record from the database
                bc.appt_types.DeleteOnSubmit(record);
                bc.SubmitChanges();

                // Remove the record from the grid
                colRecords.Remove(record);
                gridView1.RefreshData();
            }
            catch (System.Data.SqlClient.SqlException ex)
            {
                DebtPlus.UI.Common.ErrorHandling.HandleErrors(ex, "Error updating database");
            }
        }

        /// <summary>
        /// Local class to format the contact_type field.
        /// </summary>
        private class contactTypeFormatter : System.ICustomFormatter, System.IFormatProvider
        {
            internal contactTypeFormatter()
                : base()
            {
            }

            public object GetFormat(Type formatType)
            {
                return this;
            }

            /// <summary>
            /// Format the input value to the name of the language
            /// </summary>
            public string Format(string format, object arg, IFormatProvider formatProvider)
            {
                Int32? intValue = DebtPlus.Utils.Nulls.v_Int32(arg);
                if (intValue.HasValue && intValue.Value > 0)
                {
                    var q = DebtPlus.LINQ.Cache.ContactMethodType.getList().Find(s => s.Id == intValue.Value);
                    if (q != null)
                    {
                        return q.description;
                    }
                }
                return string.Empty;
            }
        }
    }
}