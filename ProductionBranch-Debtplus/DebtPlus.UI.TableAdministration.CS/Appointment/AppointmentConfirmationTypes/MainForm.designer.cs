﻿namespace DebtPlus.UI.TableAdministration.CS.Appointment.AppointmentConfirmationTypes
{
    partial class MainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            try
            {
                if (disposing)
                {
                    if (components != null) components.Dispose();
                    if (bc != null) bc.Dispose();
                }
                components = null;
                bc = null;
            }
            finally
            {
                base.Dispose(disposing);
            }
        }


        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.gridColumn_id = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn_description = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn_ActiveFlag = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn_default = new DevExpress.XtraGrid.Columns.GridColumn();
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl1)).BeginInit();
            this.layoutControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem_SimpleButton_Edit)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem_SimpleButton_Edit)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem_simpleButton_New)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.popupMenu1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DxErrorProvider1)).BeginInit();
            this.SuspendLayout();
            this.layoutControl1.Controls.SetChildIndex(this.SimpleButton_Edit, 0);
            this.layoutControl1.Controls.SetChildIndex(this.simpleButton_Cancel, 0);
            this.layoutControl1.Controls.SetChildIndex(this.simpleButton_New, 0);
            this.layoutControl1.Controls.SetChildIndex(this.gridControl1, 0);
            // 
            // barButtonItem_Update
            // 
            this.barButtonItem_Update.Enabled = false;
            // 
            // barButtonItem_Delete
            // 
            this.barButtonItem_Delete.Enabled = false;
            // 
            // gridView1
            // 
            this.gridView1.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gridColumn_id,
            this.gridColumn_description,
            this.gridColumn_ActiveFlag,
            this.gridColumn_default});
            // 
            // gridColumn_id
            // 
            this.gridColumn_id.AppearanceCell.Options.UseTextOptions = true;
            this.gridColumn_id.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.gridColumn_id.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColumn_id.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.gridColumn_id.Caption = "ID";
            this.gridColumn_id.FieldName = "Id";
            this.gridColumn_id.Name = "gridColumn_id";
            this.gridColumn_id.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.True;
            this.gridColumn_id.Visible = true;
            this.gridColumn_id.VisibleIndex = 0;
            this.gridColumn_id.Width = 67;
            // 
            // gridColumn_Name
            // 
            this.gridColumn_description.Caption = "Description";
            this.gridColumn_description.FieldName = "description";
            this.gridColumn_description.Name = "gridColumn_Name";
            this.gridColumn_description.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.True;
            this.gridColumn_description.Visible = true;
            this.gridColumn_description.VisibleIndex = 1;
            this.gridColumn_description.Width = 349;
            // 
            // gridColumn_ActiveFlag
            // 
            this.gridColumn_ActiveFlag.AppearanceCell.Options.UseTextOptions = true;
            this.gridColumn_ActiveFlag.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumn_ActiveFlag.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColumn_ActiveFlag.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumn_ActiveFlag.Caption = "Active";
            this.gridColumn_ActiveFlag.FieldName = "ActiveFlag";
            this.gridColumn_ActiveFlag.Name = "gridColumn_ActiveFlag";
            this.gridColumn_ActiveFlag.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.True;
            // 
            // gridColumn_default
            // 
            this.gridColumn_default.AppearanceCell.Options.UseTextOptions = true;
            this.gridColumn_default.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumn_default.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColumn_default.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumn_default.Caption = "Default";
            this.gridColumn_default.FieldName = "default";
            this.gridColumn_default.Name = "gridColumn_default";
            this.gridColumn_default.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.True;
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.ClientSize = new System.Drawing.Size(528, 294);
            this.Name = "MainForm";
            this.Text = "Appointment Confirmation Types";
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl1)).EndInit();
            this.layoutControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem_SimpleButton_Edit)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem_SimpleButton_Edit)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem_simpleButton_New)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.popupMenu1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DxErrorProvider1)).EndInit();
            this.ResumeLayout(false);
        }

        private DevExpress.XtraGrid.Columns.GridColumn gridColumn_id;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn_default;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn_ActiveFlag;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn_description;
    }
}