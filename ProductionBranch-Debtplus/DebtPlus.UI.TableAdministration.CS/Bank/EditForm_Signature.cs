#region "Copyright 2000-2012 DebtPlus, L.L.C."
//{*******************************************************************}
//{                                                                   }
//{       DebtPlus Debt Management System                             }
//{                                                                   }
//{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
//{       ALL RIGHTS RESERVED                                         }
//{                                                                   }
//{   The entire contents of this file is protected by U.S. and       }
//{   International Copyright Laws. Unauthorized reproduction,        }
//{   reverse-engineering, and distribution of all or any portion of  }
//{   the code contained in this file is strictly prohibited and may  }
//{   result in severe civil and criminal penalties and will be       }
//{   prosecuted to the maximum extent possible under the law.        }
//{                                                                   }
//{   RESTRICTIONS                                                    }
//{                                                                   }
//{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
//{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
//{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
//{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
//{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
//{                                                                   }
//{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
//{   ADDITIONAL RESTRICTIONS.                                        }
//{                                                                   }
//{*******************************************************************}

#endregion "Copyright 2000-2012 DebtPlus, L.L.C."

using System;
using System.Drawing;
using System.Windows.Forms;
using DebtPlus.LINQ;
using DevExpress.XtraBars;
using DevExpress.XtraEditors.Controls;

namespace DebtPlus.UI.TableAdministration.CS.Bank
{
    internal partial class EditForm_Signature
    {
        private Image pict = null;
        private BusinessContext bc = null;
        private bank record = null;

        /// <summary>
        /// Initialize the new form class
        /// </summary>
        internal EditForm_Signature()
            : base()
        {
            InitializeComponent();
            this.PictureEdit1.Properties.NullText = "You Have No Signature\r\nPlease Click On Open To Load One";
        }

        /// <summary>
        /// Initialize the new form class
        /// </summary>
        internal EditForm_Signature(BusinessContext bc, bank record)
            : this()
        {
            this.bc = bc;
            this.record = record;
            RegisterHandlers();
        }

        /// <summary>
        /// Register the event handler routines
        /// </summary>
        private void RegisterHandlers()
        {
            FormClosing += EditForm_Signature_FormClosing;
            Load += EditForm_Signature_Load;
            SimpleButton_Open.Click += SimpleButton_Open_Click;
            simpleButton_OK.Click += simpleButton_OK_Click;
            simpleButton_Cancel.Click += simpleButton_Cancel_Click;
            BarButtonItem_File_Exit.ItemClick += BarButtonItem_File_Exit_ItemClick;
            BarButtonItem_File_Open.ItemClick += BarButtonItem_File_Open_ItemClick;
            BarCheckItem_View_Normal.ItemClick += BarCheckItem_View_Normal_ItemClick;
            BarCheckItem_View_Stretch.ItemClick += BarCheckItem_View_Stretch_ItemClick;
            BarCheckItem_View_Squeeze.ItemClick += BarCheckItem_View_Squeeze_ItemClick;
            BarCheckItem_View_Zoom.ItemClick += BarCheckItem_View_Zoom_ItemClick;
            BarButtonItem_Save.ItemClick += BarButtonItem_Save_ItemClick;
            BarSubItem_View_Mode.Popup += BarSubItem_View_Popup;
            BarButtonItem_Flip_Horizontal.ItemClick += BarButtonItem_Flip_Horizontal_ItemClick;
            BarButtonItem_Flip_Vertical.ItemClick += BarButtonItem_Flip_Vertical_ItemClick;
            BarButtonItem_Rotate_Left.ItemClick += BarButtonItem_Rotate_Left_ItemClick;
            BarButtonItem_Rotate_Right.ItemClick += BarButtonItem_Rotate_Right_ItemClick;
            XtraScrollableControl1.Resize += XtraScrollableControl1_Resize;

            PictureEdit1.EditValueChanged += PictureEdit1_EditValueChanged;
        }

        /// <summary>
        /// Remove the event handler registrations
        /// </summary>
        private void UnRegisterHandlers()
        {
            FormClosing -= EditForm_Signature_FormClosing;
            Load -= EditForm_Signature_Load;
            SimpleButton_Open.Click -= SimpleButton_Open_Click;
            simpleButton_OK.Click -= simpleButton_OK_Click;
            simpleButton_Cancel.Click -= simpleButton_Cancel_Click;
            BarButtonItem_File_Exit.ItemClick -= BarButtonItem_File_Exit_ItemClick;
            BarButtonItem_File_Open.ItemClick -= BarButtonItem_File_Open_ItemClick;
            BarCheckItem_View_Normal.ItemClick -= BarCheckItem_View_Normal_ItemClick;
            BarCheckItem_View_Stretch.ItemClick -= BarCheckItem_View_Stretch_ItemClick;
            BarCheckItem_View_Squeeze.ItemClick -= BarCheckItem_View_Squeeze_ItemClick;
            BarCheckItem_View_Zoom.ItemClick -= BarCheckItem_View_Zoom_ItemClick;
            BarButtonItem_Save.ItemClick -= BarButtonItem_Save_ItemClick;
            BarSubItem_View_Mode.Popup -= BarSubItem_View_Popup;
            BarButtonItem_Flip_Horizontal.ItemClick -= BarButtonItem_Flip_Horizontal_ItemClick;
            BarButtonItem_Flip_Vertical.ItemClick -= BarButtonItem_Flip_Vertical_ItemClick;
            BarButtonItem_Rotate_Left.ItemClick -= BarButtonItem_Rotate_Left_ItemClick;
            BarButtonItem_Rotate_Right.ItemClick -= BarButtonItem_Rotate_Right_ItemClick;
            XtraScrollableControl1.Resize -= XtraScrollableControl1_Resize;

            PictureEdit1.EditValueChanged -= PictureEdit1_EditValueChanged;
        }

        /// <summary>
        /// Is the image zoomed?
        /// </summary>
        private bool IsZoom
        {
            get
            {
                return PictureEdit1.Properties.SizeMode == PictureSizeMode.Zoom;
            }
        }

        /// <summary>
        /// If the form is closing then ask if the image should be saved.
        /// </summary>
        private void EditForm_Signature_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (e.CloseReason != CloseReason.FormOwnerClosing && e.CloseReason != CloseReason.UserClosing)
            {
                return;
            }

            if (simpleButton_OK.Enabled)
            {
                switch (MessageBox.Show("You have not saved the image to the database.\r\nDo you wish to do so now?", "Unsaved Changes", MessageBoxButtons.YesNoCancel, MessageBoxIcon.Information, MessageBoxDefaultButton.Button1))
                {
                    case DialogResult.Yes:
                        SaveSignature(PictureEdit1.Image);
                        break;

                    case DialogResult.No:
                        break;

                    case DialogResult.Cancel:
                        e.Cancel = true;
                        break;
                }
            }
        }

        /// <summary>
        /// Process the form LOAD event
        /// </summary>
        private void EditForm_Signature_Load(object sender, EventArgs e)
        {
            UnRegisterHandlers();
            try
            {
                // Set the default parameters for the view port
                PictureEdit1.Properties.PictureAlignment = ContentAlignment.MiddleCenter;
                PictureEdit1.Properties.PictureStoreMode = PictureStoreMode.Image;
                PictureEdit1.Properties.SizeMode = PictureSizeMode.Squeeze;

                // Update the database with the corresponding bitmap image buffer
                pict = ReadSignature();
                if (pict != null)
                {
                    PictureEdit1.Image = pict;
                    UpdatePictureInformation();
                }
                CorrectArea();
            }
            finally
            {
                RegisterHandlers();
            }
        }

        /// <summary>
        /// Handle the CLICK event on the OPEN button
        /// </summary>
        private void SimpleButton_Open_Click(object sender, EventArgs e)
        {
            string filename = string.Empty;

            // Define the file open dialog
            using (var frm = new OpenFileDialog())
            {
                frm.CheckFileExists = true;
                frm.DefaultExt = ".bmp";
                frm.InitialDirectory = Environment.GetFolderPath(Environment.SpecialFolder.MyPictures);
                frm.DereferenceLinks = true;
                frm.Filter = "Image Files (*.bmp;*.jpg;*.png;*.gif;*.tif;*.wmf)|*.bmp;*.jpg;*.png;*.gif;*.tif;*.wmf|All files (*.*)|*.*";
                frm.FilterIndex = 0;
                frm.Multiselect = false;
                frm.ReadOnlyChecked = true;
                frm.RestoreDirectory = true;
                frm.ValidateNames = true;

                // Ask the user for the filename and store it.
                if (frm.ShowDialog() == System.Windows.Forms.DialogResult.OK)
                {
                    filename = frm.FileName;
                }
            }

            // If the user clicked ok then open the file and load the image.
            // (Loading the image will trip the update event and that will set the OK button black.)
            if (!string.IsNullOrEmpty(filename))
            {
                try
                {
                    // Load the picture from the memory image
                    pict = Image.FromFile(filename);

                    // Once it is converted, assign it to the picture window.
                    PictureEdit1.Image = pict;
                    CorrectArea();
                }
                catch (Exception ex)
                {
                    DebtPlus.Data.Forms.MessageBox.Show(ex.Message, "Error loading image file", MessageBoxButtons.OK, MessageBoxIcon.Asterisk);
                }
            }
        }

        /// <summary>
        /// Handle the CLICK event on the OK button
        /// </summary>
        private void simpleButton_OK_Click(object sender, EventArgs e)
        {
            if (pict == null)
            {
                DialogResult = DialogResult.Cancel;
                UnRegisterHandlers();
                return;
            }
            SaveSignature(PictureEdit1.Image);
            DialogResult = DialogResult.OK;
            UnRegisterHandlers();
        }

        /// <summary>
        /// Handle the CLICK event on the CANCEL button
        /// </summary>
        private void simpleButton_Cancel_Click(object sender, EventArgs e)
        {
            simpleButton_OK.Enabled = false;
            DialogResult = DialogResult.Cancel;
            UnRegisterHandlers();
        }

        /// <summary>
        /// Process a change in the picture image
        /// </summary>
        private void PictureEdit1_EditValueChanged(object sender, EventArgs e)
        {
            UpdatePictureInformation();
        }

        /// <summary>
        /// Display the picture information to the user
        /// </summary>
        private void UpdatePictureInformation()
        {
            if (PictureEdit1.Image == null || PictureEdit1.Image.Size.IsEmpty)
            {
                BarStaticItem_Resolution.Caption = "No picture loaded";
                simpleButton_OK.Enabled = false;
                BarSubItem_View_Mode.Enabled = false;
                BarSubItem_View_Transform.Enabled = false;
            }
            else
            {
                float dpi = PictureEdit1.Image.HorizontalResolution;
                float height = PictureEdit1.Image.PhysicalDimension.Height;
                float width = PictureEdit1.Image.PhysicalDimension.Width;

                // Update the static bar information with the resolution
                BarStaticItem_Resolution.Caption = string.Format("{0:f0} DPI\r\n{1:f0}hud9902SectionFormatter{2:f0} pixels", dpi, width, height);
                simpleButton_OK.Enabled = true;
                BarSubItem_View_Mode.Enabled = true;
                BarSubItem_View_Transform.Enabled = true;
            }
        }

        /// <summary>
        /// Process the CLICK event on the EXIT button
        /// </summary>
        private void BarButtonItem_File_Exit_ItemClick(object sender, ItemClickEventArgs e)
        {
            DialogResult = DialogResult.Cancel;
            UnRegisterHandlers();
        }

        /// <summary>
        /// Process the CLICK event on the OPEN button
        /// </summary>
        private void BarButtonItem_File_Open_ItemClick(object sender, ItemClickEventArgs e)
        {
            SimpleButton_Open.PerformClick();
        }

        /// <summary>
        /// Set the viewing mode to NORMAL
        /// </summary>
        private void BarCheckItem_View_Normal_ItemClick(object sender, ItemClickEventArgs e)
        {
            if (pict == null)
            {
                return;
            }
            PictureEdit1.Properties.SizeMode = PictureSizeMode.Clip;
            CorrectArea();
        }

        /// <summary>
        /// Set the viewing mode to STRETCH
        /// </summary>
        private void BarCheckItem_View_Stretch_ItemClick(object sender, ItemClickEventArgs e)
        {
            if (pict == null)
            {
                return;
            }
            PictureEdit1.Properties.SizeMode = PictureSizeMode.Stretch;
            CorrectArea();
        }

        /// <summary>
        /// Set the viewing mode to SQUEEZE
        /// </summary>
        private void BarCheckItem_View_Squeeze_ItemClick(object sender, ItemClickEventArgs e)
        {
            if (pict == null)
            {
                return;
            }
            PictureEdit1.Properties.SizeMode = PictureSizeMode.Squeeze;
            CorrectArea();
        }

        /// <summary>
        /// Zoom on the picture
        /// </summary>
        private void BarCheckItem_View_Zoom_ItemClick(object sender, ItemClickEventArgs e)
        {
            if (pict == null)
            {
                return;
            }
            PictureEdit1.Properties.SizeMode = PictureSizeMode.Zoom;
            CorrectArea();
        }

        /// <summary>
        /// Handle the click event on the SAVE button
        /// </summary>
        private void BarButtonItem_Save_ItemClick(object sender, ItemClickEventArgs e)
        {
            if (pict == null)
            {
                return;
            }
            // Wash the image through a new instance of the image
            System.Drawing.Image img = PictureEdit1.Image;

            using (var frm = new SaveFileDialog())
            {
                frm.DefaultExt = ".jpg";
                frm.Filter = "Bitmap Files (*.bmp)|*.bmp|" + "Emf Files (*.emf)|*.emf" + "Exif Files (*.Exif)|*.Exif" + "Gif Files (*.Gif)|*.Gif" + "Icon Files (*.Icon)|*.Icon" + "Jpeg Files (*.Jpg)|*.Jpeg;*.jpg" + "Png Files (*.Png)|*.Png" + "Tiff Files (*.Tiff)|*.Tiff" + "Wmf Files (*.Wmf)|*.Wmf" + "All Files (*.*)|*.*";

                frm.AddExtension = true;
                frm.AutoUpgradeEnabled = true;
                frm.CheckFileExists = false;
                frm.CheckPathExists = true;
                frm.CreatePrompt = false;
                frm.FilterIndex = 5;
                frm.InitialDirectory = System.Environment.GetFolderPath(Environment.SpecialFolder.MyPictures);
                frm.OverwritePrompt = true;
                frm.RestoreDirectory = true;
                frm.ShowHelp = false;
                frm.Title = "Save Image";

                if (frm.ShowDialog() != DialogResult.OK)
                {
                    return;
                }

                try
                {
                    // Save the file to the designated location
                    img.Save(frm.FileName, ImageFormatFromName(frm.FileName));
                }
                catch (System.Exception ex)
                {
                    DebtPlus.UI.Common.ErrorHandling.HandleErrors(ex, "Error trying to save the image file");
                }
            }
        }

        /// <summary>
        /// Determine the type of image format based upon the file name extension.
        /// </summary>
        private System.Drawing.Imaging.ImageFormat ImageFormatFromName(string Filename)
        {
            string Ext = System.IO.Path.GetExtension(Filename).ToLower().Trim();
            if (string.IsNullOrEmpty(Ext))
            {
                return System.Drawing.Imaging.ImageFormat.Jpeg;
            }

            // Look at the extension to find a match to the file type
            switch (Ext)
            {
                case ".bmp":
                    return System.Drawing.Imaging.ImageFormat.Bmp;

                case ".emf":
                    return System.Drawing.Imaging.ImageFormat.Emf;

                case ".exif":
                    return System.Drawing.Imaging.ImageFormat.Exif;

                case ".gif":
                    return System.Drawing.Imaging.ImageFormat.Gif;

                case ".icon":
                    return System.Drawing.Imaging.ImageFormat.Icon;

                case ".jpeg":
                case ".jpg":
                    return System.Drawing.Imaging.ImageFormat.Jpeg;

                case ".png":
                    return System.Drawing.Imaging.ImageFormat.Png;

                case ".tiff":
                    return System.Drawing.Imaging.ImageFormat.Tiff;

                case ".wmf":
                    return System.Drawing.Imaging.ImageFormat.Wmf;

                default:
                    break;
            }

            return System.Drawing.Imaging.ImageFormat.Jpeg;
        }

        /// <summary>
        /// Process the POPUP event on the menus
        /// </summary>
        private void BarSubItem_View_Popup(object sender, EventArgs e)
        {
            if (pict == null)
            {
                BarCheckItem_View_Normal.Enabled = false;
                BarCheckItem_View_Zoom.Enabled = false;
                BarCheckItem_View_Squeeze.Enabled = false;
                BarCheckItem_View_Stretch.Enabled = false;
                return;
            }

            BarCheckItem_View_Normal.Enabled = true;
            BarCheckItem_View_Zoom.Enabled = true;
            BarCheckItem_View_Squeeze.Enabled = true;
            BarCheckItem_View_Stretch.Enabled = true;

            BarCheckItem_View_Normal.Checked = (PictureEdit1.Properties.SizeMode == PictureSizeMode.Clip);
            BarCheckItem_View_Zoom.Checked = (PictureEdit1.Properties.SizeMode == PictureSizeMode.Zoom);
            BarCheckItem_View_Squeeze.Checked = (PictureEdit1.Properties.SizeMode == PictureSizeMode.Squeeze);
            BarCheckItem_View_Stretch.Checked = (PictureEdit1.Properties.SizeMode == PictureSizeMode.Stretch);
        }

        /// <summary>
        /// Flip the image horizontally
        /// </summary>
        private void BarButtonItem_Flip_Horizontal_ItemClick(object sender, ItemClickEventArgs e)
        {
            if (pict == null)
            {
                return;
            }
            PictureEdit1.Image.RotateFlip(RotateFlipType.RotateNoneFlipX);
            CorrectArea();
            simpleButton_OK.Enabled = PictureEdit1.Image != null && !PictureEdit1.Image.Size.IsEmpty;
        }

        /// <summary>
        /// Flip the image vertically
        /// </summary>
        private void BarButtonItem_Flip_Vertical_ItemClick(object sender, ItemClickEventArgs e)
        {
            if (pict == null)
            {
                return;
            }
            PictureEdit1.Image.RotateFlip(RotateFlipType.RotateNoneFlipY);
            CorrectArea();
            simpleButton_OK.Enabled = PictureEdit1.Image != null && !PictureEdit1.Image.Size.IsEmpty;
        }

        /// <summary>
        /// Rotate the image counter-clockwise
        /// </summary>
        private void BarButtonItem_Rotate_Left_ItemClick(object sender, ItemClickEventArgs e)
        {
            if (pict == null)
            {
                return;
            }
            PictureEdit1.Image.RotateFlip(RotateFlipType.Rotate270FlipNone);
            CorrectArea();
            simpleButton_OK.Enabled = PictureEdit1.Image != null && !PictureEdit1.Image.Size.IsEmpty;
        }

        /// <summary>
        /// Rotate the image clockwise
        /// </summary>
        private void BarButtonItem_Rotate_Right_ItemClick(object sender, ItemClickEventArgs e)
        {
            if (pict == null)
            {
                return;
            }
            PictureEdit1.Image.RotateFlip(RotateFlipType.Rotate90FlipNone);
            CorrectArea();
            simpleButton_OK.Enabled = PictureEdit1.Image != null && !PictureEdit1.Image.Size.IsEmpty;
        }

        /// <summary>
        /// Correct the display area so that it is visible
        /// </summary>
        private void CorrectArea()
        {
            PictureEdit1.Location = new Point(0, 0);

            if (PictureEdit1.Image != null && (!PictureEdit1.Image.Size.IsEmpty) && IsZoom)
            {
                PictureEdit1.Size = new Size(PictureEdit1.Image.Width, PictureEdit1.Image.Height);
                XtraScrollableControl1.HorizontalScroll.Visible = true;
                XtraScrollableControl1.VerticalScroll.Visible = true;
            }
            else
            {
                XtraScrollableControl1.HorizontalScroll.Visible = false;
                XtraScrollableControl1.VerticalScroll.Visible = false;
                PictureEdit1.Size = XtraScrollableControl1.Size;
            }

            XtraScrollableControl1.ScrollControlIntoView(PictureEdit1);
            PictureEdit1.Invalidate();
        }

        /// <summary>
        /// Process the RESIZE event on the display area
        /// </summary>
        private void XtraScrollableControl1_Resize(object sender, EventArgs e)
        {
            CorrectArea();
        }

        /// <summary>
        /// Retrieve the signature from the current bank record
        /// </summary>
        private Image ReadSignature()
        {
            // If there is no entry in the record then there is no image
            if (record.SignatureImage == null)
            {
                return null;
            }

            // Try to convert the record byte stream to an image
            try
            {
                // Translate the signature on the bank to a bitmap.
                byte[] signatureBuffer = record.SignatureImage.ToArray();
                using (var ms = new System.IO.MemoryStream(signatureBuffer))
                {
                    return new System.Drawing.Bitmap(ms);
                }
            }
            catch
            {
                // Return NULL for any error condition.
                return null;
            }
        }

        /// <summary>
        /// Save the signature into the bank record
        /// </summary>
        /// <param name="signatureImage"></param>
        private void SaveSignature(Image signatureImage)
        {
            try
            {
                using (var ms = new System.IO.MemoryStream())
                {
                    signatureImage.Save(ms, System.Drawing.Imaging.ImageFormat.Bmp);
                    ms.Position = 0;
                    byte[] signatureBuffer = ms.ToArray();

                    // Update the bank record with the signature buffer
                    record.SignatureImage = signatureBuffer;

                    // We do not need to save the image again.
                    simpleButton_OK.Enabled = false;
                }
            }
            catch (System.Exception ex)
            {
                DebtPlus.UI.Common.ErrorHandling.HandleErrors(ex, "Error saving signature image");
            }
        }
    }
}