#region "Copyright 2000-2012 DebtPlus, L.L.C."
//{*******************************************************************}
//{                                                                   }
//{       DebtPlus Debt Management System                             }
//{                                                                   }
//{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
//{       ALL RIGHTS RESERVED                                         }
//{                                                                   }
//{   The entire contents of this file is protected by U.S. and       }
//{   International Copyright Laws. Unauthorized reproduction,        }
//{   reverse-engineering, and distribution of all or any portion of  }
//{   the code contained in this file is strictly prohibited and may  }
//{   result in severe civil and criminal penalties and will be       }
//{   prosecuted to the maximum extent possible under the law.        }
//{                                                                   }
//{   RESTRICTIONS                                                    }
//{                                                                   }
//{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
//{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
//{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
//{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
//{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
//{                                                                   }
//{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
//{   ADDITIONAL RESTRICTIONS.                                        }
//{                                                                   }
//{*******************************************************************}

#endregion "Copyright 2000-2012 DebtPlus, L.L.C."

using System;
using System.Linq;
using DebtPlus.LINQ;

namespace DebtPlus.UI.TableAdministration.CS.Bank
{
    public partial class MainForm : Templates.MainForm
    {
        private System.Collections.Generic.List<bank> colRecords = null;
        private BusinessContext bc = new BusinessContext();

        public MainForm()
            : base()
        {
            InitializeComponent();
            RegisterHandlers();
        }

        private void RegisterHandlers()
        {
            Load += MainForm_Load;
        }

        private void UnRegisterHandlers()
        {
            Load -= MainForm_Load;
        }

        /// <summary>
        /// Process the load event on the form
        /// </summary>
        private void MainForm_Load(object sender, EventArgs e)
        {
            UnRegisterHandlers();

            // Load the formatting routine for the bank type
            gridColumn_type.DisplayFormat.Format = new BankTypeFormatter();
            gridColumn_type.GroupFormat.Format = new BankTypeFormatter();

            try
            {
                colRecords = bc.banks.ToList();
                gridControl1.DataSource = colRecords;
            }
            catch (System.Data.SqlClient.SqlException ex)
            {
                DebtPlus.UI.Common.ErrorHandling.HandleErrors(ex);
            }
            finally
            {
                RegisterHandlers();
            }
        }

        /// <summary>
        /// Handle the edit of the information on the form
        /// </summary>
        protected override void UpdateRecord(object obj)
        {
            var record = obj as bank;
            if (record == null)
            {
                return;
            }

            // Save the previous default status
            bool priorDefault = record.Default;

            using (var frm = new EditForm(bc, record))
            {
                if (frm.ShowDialog() != System.Windows.Forms.DialogResult.OK)
                {
                    bc.Refresh(System.Data.Linq.RefreshMode.OverwriteCurrentValues, record);
                    return;
                }
            }

            // Remove the default status from the previous record if needed. Each type has a default so we need to quality it by the type.
            if (record.Default && !priorDefault)
            {
                foreach (var defaultRecord in colRecords.Where(s => s.type == record.type && s.Default))
                {
                    defaultRecord.Default = false;
                }
                record.Default = true;
            }

            try
            {
                bc.SubmitChanges();
                gridView1.RefreshData();
            }
            catch (System.Data.SqlClient.SqlException ex)
            {
                DebtPlus.UI.Common.ErrorHandling.HandleErrors(ex);
            }
        }

        protected override void CreateRecord()
        {
            var record = DebtPlus.LINQ.Factory.Manufacture_bank();

            using (var frm = new EditForm(bc, record))
            {
                if (frm.ShowDialog() != System.Windows.Forms.DialogResult.OK)
                {
                    return;
                }
            }

            // Remove the default status from the previous record if needed. Each type has a default so we need to quality it by the type.
            if (record.Default)
            {
                foreach (var defaultRecord in colRecords.Where(s => s.type == record.type && s.Default))
                {
                    defaultRecord.Default = false;
                }
                record.Default = true;
            }

            try
            {
                bc.SubmitChanges();
                gridView1.RefreshData();
            }
            catch (System.Data.SqlClient.SqlException ex)
            {
                DebtPlus.UI.Common.ErrorHandling.HandleErrors(ex);
            }
        }

        protected override void DeleteRecord(object obj)
        {
            var record = obj as bank;
            if (record == null)
            {
                return;
            }

            // Ensure that the delete is valid
            if (DebtPlus.Data.Prompts.RequestConfirmation_Delete() != System.Windows.Forms.DialogResult.Yes)
            {
                return;
            }

            // Delete the record
            bc.banks.DeleteOnSubmit(record);

            try
            {
                bc.SubmitChanges();
                gridView1.RefreshData();
            }
            catch (System.Data.SqlClient.SqlException ex)
            {
                DebtPlus.UI.Common.ErrorHandling.HandleErrors(ex);
            }
        }

        private class BankTypeFormatter : System.IFormatProvider, System.ICustomFormatter
        {
            internal BankTypeFormatter()
            {
            }

            public object GetFormat(Type formatType)
            {
                return this;
            }

            public string Format(string format, object arg, IFormatProvider formatProvider)
            {
                string strKey = DebtPlus.Utils.Nulls.v_String(arg);
                if (!string.IsNullOrWhiteSpace(strKey))
                {
                    var q = DebtPlus.LINQ.InMemory.bankTypes.getList().Find(s => s.Id == strKey);
                    if (q != null)
                    {
                        return q.description;
                    }
                }
                return string.Empty;
            }
        }
    }
}