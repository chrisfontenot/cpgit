using System;
using DebtPlus.LINQ;
using DebtPlus.UI.TableAdministration.CS.Bank.Controls;

namespace DebtPlus.UI.TableAdministration.CS.Bank
{
    internal partial class EditForm : Templates.EditTemplateForm
    {
        // Local storage
        private bank record = null;

        private BusinessContext bc = null;
        private Banks_Common SelectedPage = null;

        internal EditForm()
            : base()
        {
            InitializeComponent();
        }

        internal EditForm(BusinessContext bc, bank record)
            : this()
        {
            this.record = record;
            this.bc = bc;
            RegisterHandlers();
        }

        private void RegisterHandlers()
        {
            Load += EditForm_Load;
            SimpleButton_Address.Click += SimpleButton_Address_Click;
            ComboBoxEdit_type.SelectedIndexChanged += ComboBoxEdit_type_SelectedIndexChanged;
            Banks_A1.FormChanged += form_Changed;
            Banks_C1.FormChanged += form_Changed;
            Banks_D1.FormChanged += form_Changed;
            Banks_R1.FormChanged += form_Changed;
        }

        private void UnRegisterHandlers()
        {
            Load -= EditForm_Load;
            SimpleButton_Address.Click -= SimpleButton_Address_Click;
            ComboBoxEdit_type.SelectedIndexChanged -= ComboBoxEdit_type_SelectedIndexChanged;
            Banks_A1.FormChanged -= form_Changed;
            Banks_C1.FormChanged -= form_Changed;
            Banks_D1.FormChanged -= form_Changed;
            Banks_R1.FormChanged -= form_Changed;
        }

        private void EditForm_Load(object sender, EventArgs e)
        {
            UnRegisterHandlers();
            try
            {
                string itemType = record.type;

                // Load the combobox items with the proper list and select the current record in the control.
                // The actual page will be processed later.
                ComboBoxEdit_type.Properties.Items.Clear();
                ComboBoxEdit_type.Properties.Sorted = false;
                ComboBoxEdit_type.SelectedItem = null;

                foreach (var bankType in DebtPlus.LINQ.InMemory.bankTypes.getList())
                {
                    ComboBoxEdit_type.Properties.Items.Add(bankType);
                    if (bankType.Id == record.type)
                    {
                        ComboBoxEdit_type.SelectedItem = bankType;
                    }
                }

                // Once the items are loaded, sort them.
                ComboBoxEdit_type.Properties.Sorted = true;

                // Bind the name field to the control
                TextEdit_description.EditValue = record.description;
                CheckEdit_default.Checked = record.Default;
                CheckEdit_ActiveFlag.Checked = record.ActiveFlag;

                // Do any one-time initialization that is needed during our load event
                Banks_A1.ReadForm(bc, record);
                Banks_C1.ReadForm(bc, record);
                Banks_D1.ReadForm(bc, record);
                Banks_R1.ReadForm(bc, record);

                // Go to the first page entry
                switchTo(record.type);
            }
            finally
            {
                RegisterHandlers();
            }
        }

        /// <summary>
        /// Save the current page information before we switch away from the page.
        /// </summary>
        private void switchAway()
        {
            if (SelectedPage != null)
            {
                SelectedPage.SaveForm(bc, record);

                SelectedPage.TabStop = false;
                SelectedPage.Visible = false;
                SelectedPage.SendToBack();

                // We no longer have a selected page at this point.
                SelectedPage = null;
            }
        }

        /// <summary>
        /// Load the new page information before we switch to the page.
        /// </summary>
        /// <param name="pageType"></param>
        private void switchTo(string pageType)
        {
            // Determine the new page from the type string
            switch (pageType)
            {
                case "A":
                    SelectedPage = Banks_A1;
                    break;

                case "C":
                    SelectedPage = Banks_C1;
                    break;

                case "D":
                    SelectedPage = Banks_D1;
                    break;

                case "R":
                    SelectedPage = Banks_R1;
                    break;

                default:
                    System.Diagnostics.Debug.Assert(false, "bank type is invalid");
                    return;
            }

            // Transfer to the new page
            SelectedPage.TabStop = true;
            SelectedPage.Visible = true;
            SelectedPage.BringToFront();
            SelectedPage.Focus();
            SelectedPage.RefreshForm();

            // Enable the OK button accordingly
            simpleButton_OK.Enabled = !HasErrors();
        }

        /// <summary>
        /// Determine if there are errors on the form
        /// </summary>
        private bool HasErrors()
        {
            // We need a page to start
            if (SelectedPage == null)
            {
                return true;
            }

            // The description is required
            if (string.IsNullOrWhiteSpace(DebtPlus.Utils.Nulls.v_String(TextEdit_description.EditValue)))
            {
                return true;
            }

            // If the selected page says no then it is no
            if (SelectedPage.HasErrors())
            {
                return true;
            }

            return false;
        }

        /// <summary>
        /// Process a change in the form controls
        /// </summary>
        private void form_Changed(object sender, EventArgs e)
        {
            simpleButton_OK.Enabled = !HasErrors();
        }

        /// <summary>
        /// Handle the change in the type of bank account
        /// </summary>
        private void ComboBoxEdit_type_SelectedIndexChanged(object Sender, EventArgs e)
        {
            var item = ComboBoxEdit_type.SelectedItem as DebtPlus.LINQ.InMemory.bankType;
            if (item != null)
            {
                switchAway();
                switchTo(item.Id);
            }
        }

        protected override void simpleButton_OK_Click(object sender, EventArgs e)
        {
            // Do the finalization for the selected page
            switchAway();

            // Save the items from the current controls
            record.description = DebtPlus.Utils.Nulls.v_String(TextEdit_description.EditValue);
            record.Default = CheckEdit_default.Checked;
            record.ActiveFlag = CheckEdit_ActiveFlag.Checked;

            base.simpleButton_OK_Click(sender, e);
        }

        private void SimpleButton_Address_Click(object sender, EventArgs e)
        {
            using (var frm = new EditForm_Address(bc, record))
            {
                frm.ShowDialog();
            }
        }
    }
}