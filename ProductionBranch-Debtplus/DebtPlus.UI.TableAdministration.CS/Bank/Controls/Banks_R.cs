#region "Copyright 2000-2012 DebtPlus, L.L.C."
//{*******************************************************************}
//{                                                                   }
//{       DebtPlus Debt Management System                             }
//{                                                                   }
//{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
//{       ALL RIGHTS RESERVED                                         }
//{                                                                   }
//{   The entire contents of this file is protected by U.S. and       }
//{   International Copyright Laws. Unauthorized reproduction,        }
//{   reverse-engineering, and distribution of all or any portion of  }
//{   the code contained in this file is strictly prohibited and may  }
//{   result in severe civil and criminal penalties and will be       }
//{   prosecuted to the maximum extent possible under the law.        }
//{                                                                   }
//{   RESTRICTIONS                                                    }
//{                                                                   }
//{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
//{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
//{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
//{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
//{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
//{                                                                   }
//{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
//{   ADDITIONAL RESTRICTIONS.                                        }
//{                                                                   }
//{*******************************************************************}

#endregion "Copyright 2000-2012 DebtPlus, L.L.C."

using System;
using System.Windows.Forms;
using DebtPlus.LINQ;
using DevExpress.XtraEditors.Controls;

namespace DebtPlus.UI.TableAdministration.CS.Bank.Controls
{
    internal partial class Banks_R : Banks_Common
    {
        private bank record = null;
        private BusinessContext bc = null;

        internal Banks_R()
            : base()
        {
            InitializeComponent();
        }

        /// <summary>
        /// Do any one-time initialization for the data.
        /// </summary>
        /// <param name="bc"></param>
        /// <param name="record"></param>
        internal override void ReadForm(BusinessContext bc, bank record)
        {
            base.ReadForm(bc, record);
            this.record = record;
            this.bc = bc;
        }

        /// <summary>
        /// Register the event handlers
        /// </summary>
        private void RegisterHandlers()
        {
            ButtonEdit_output_directory.ButtonPressed += OutputDirectory_ButtonPressed;
            TextEdit_batch_number.EditValueChanged += HandleFormChanged;
            TextEdit_immediate_destination.EditValueChanged += HandleFormChanged;
            TextEdit_immediate_destination_name.EditValueChanged += HandleFormChanged;
            TextEdit_immediate_origin.EditValueChanged += HandleFormChanged;
            TextEdit_immediate_origin_name.EditValueChanged += HandleFormChanged;
            ButtonEdit_output_directory.EditValueChanged += HandleFormChanged;
            TextEdit_transaction_number.EditValueChanged += HandleFormChanged;
            MemoExEdit_prefix_lines.EditValueChanged += HandleFormChanged;
            MemoExEdit_suffix_lines.EditValueChanged += HandleFormChanged;
        }

        /// <summary>
        /// Remove the event handler registrations
        /// </summary>
        private void UnRegisterHandlers()
        {
            ButtonEdit_output_directory.ButtonPressed -= OutputDirectory_ButtonPressed;
            TextEdit_batch_number.EditValueChanged -= HandleFormChanged;
            TextEdit_immediate_destination.EditValueChanged -= HandleFormChanged;
            TextEdit_immediate_destination_name.EditValueChanged -= HandleFormChanged;
            TextEdit_immediate_origin.EditValueChanged -= HandleFormChanged;
            TextEdit_immediate_origin_name.EditValueChanged -= HandleFormChanged;
            ButtonEdit_output_directory.EditValueChanged -= HandleFormChanged;
            TextEdit_transaction_number.EditValueChanged -= HandleFormChanged;
            MemoExEdit_prefix_lines.EditValueChanged -= HandleFormChanged;
            MemoExEdit_suffix_lines.EditValueChanged -= HandleFormChanged;
        }

        /// <summary>
        /// Handle the loading of the controls from the record when the page is selected
        /// </summary>
        internal override void RefreshForm()
        {
            UnRegisterHandlers();
            try
            {
                ButtonEdit_output_directory.EditValue = record.output_directory;
                MemoExEdit_prefix_lines.EditValue = record.prefix_line;
                MemoExEdit_suffix_lines.EditValue = record.suffix_line;
                TextEdit_batch_number.EditValue = record.batch_number;
                TextEdit_immediate_destination.EditValue = record.immediate_destination;
                TextEdit_immediate_destination_name.EditValue = record.immediate_destination_name;
                TextEdit_immediate_origin.EditValue = record.immediate_origin;
                TextEdit_immediate_origin_name.EditValue = record.immediate_origin_name;
                TextEdit_transaction_number.EditValue = record.transaction_number;
            }
            finally
            {
                base.RefreshForm();
                RegisterHandlers();
            }
        }

        /// <summary>
        /// Save the data from the input control to the record
        /// </summary>
        internal override void SaveForm(BusinessContext bc, bank record)
        {
            record.batch_number = DebtPlus.Utils.Nulls.v_Int32(TextEdit_batch_number.EditValue).GetValueOrDefault();
            record.immediate_destination = DebtPlus.Utils.Nulls.v_String(TextEdit_immediate_destination.EditValue);
            record.immediate_destination_name = DebtPlus.Utils.Nulls.v_String(TextEdit_immediate_destination_name.EditValue);
            record.immediate_origin = DebtPlus.Utils.Nulls.v_String(TextEdit_immediate_origin.EditValue);
            record.immediate_origin_name = DebtPlus.Utils.Nulls.v_String(TextEdit_immediate_origin_name.EditValue);
            record.output_directory = DebtPlus.Utils.Nulls.v_String(ButtonEdit_output_directory.EditValue);
            record.prefix_line = DebtPlus.Utils.Nulls.v_String(MemoExEdit_prefix_lines.EditValue);
            record.suffix_line = DebtPlus.Utils.Nulls.v_String(MemoExEdit_suffix_lines.EditValue);
            record.transaction_number = DebtPlus.Utils.Nulls.v_Int32(TextEdit_transaction_number.EditValue).GetValueOrDefault();

            base.SaveForm(bc, record);
        }

        /// <summary>
        /// Process the CLICK event on the directory button
        /// </summary>
        private void OutputDirectory_ButtonPressed(object sender, ButtonPressedEventArgs e)
        {
            using (var dlg = new FolderBrowserDialog())
            {
                dlg.Description = "Output Directory";
                dlg.RootFolder = Environment.SpecialFolder.MyComputer;
                dlg.ShowNewFolderButton = true;
                if (dlg.ShowDialog() == DialogResult.OK)
                {
                    ButtonEdit_output_directory.Text = dlg.SelectedPath;
                }
            }
        }

        /// <summary>
        /// Determine if the input controls have enough information to have a valid record
        /// </summary>
        /// <returns></returns>
        internal override bool HasErrors()
        {
            // The base must not have errors
            if (base.HasErrors())
            {
                return true;
            }

            // Check the remaining fields to ensure that there are values where they are required.
            if (string.IsNullOrWhiteSpace(DebtPlus.Utils.Nulls.v_String(TextEdit_immediate_destination.EditValue)))
            {
                return true;
            }

            if (string.IsNullOrWhiteSpace(DebtPlus.Utils.Nulls.v_String(TextEdit_immediate_destination_name.EditValue)))
            {
                return true;
            }

            if (string.IsNullOrWhiteSpace(DebtPlus.Utils.Nulls.v_String(TextEdit_immediate_origin.EditValue)))
            {
                return true;
            }

            if (string.IsNullOrWhiteSpace(DebtPlus.Utils.Nulls.v_String(TextEdit_immediate_origin_name.EditValue)))
            {
                return true;
            }

            return false;
        }
    }
}