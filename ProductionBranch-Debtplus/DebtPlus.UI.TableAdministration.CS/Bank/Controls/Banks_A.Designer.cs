
using System;
using System.Windows.Forms;
using System.Drawing;
using DebtPlus.LINQ;
using System.Linq;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Diagnostics;

namespace DebtPlus.UI.TableAdministration.CS.Bank.Controls
{
	partial class Banks_A : Banks_Common
	{

		//UserControl overrides dispose to clean up the component list.
		[System.Diagnostics.DebuggerNonUserCode]
		protected override void Dispose(bool disposing)
		{
			try 
{
				if (disposing && components != null) 
{
					components.Dispose();
				}
}

	 finally 
{
				base.Dispose(disposing);
			}
		}

		//Required by the Windows Form Designer

		private System.ComponentModel.IContainer components = null;
		//NOTE: The following procedure is required by the Windows Form Designer
		//It can be modified using the Windows Form Designer.  
		//Do not modify it using the code editor.
		[System.Diagnostics.DebuggerStepThrough]
		private void InitializeComponent()
		{
			this.GroupControl1 = new DevExpress.XtraEditors.GroupControl();
			this.TextEdit_immediate_origin_name = new DevExpress.XtraEditors.TextEdit();
			this.TextEdit_immediate_origin = new DevExpress.XtraEditors.TextEdit();
			this.LabelControl4 = new DevExpress.XtraEditors.LabelControl();
			this.LabelControl3 = new DevExpress.XtraEditors.LabelControl();
			this.LabelControl5 = new DevExpress.XtraEditors.LabelControl();
			this.LabelControl6 = new DevExpress.XtraEditors.LabelControl();
			this.LabelControl7 = new DevExpress.XtraEditors.LabelControl();
			this.GroupControl2 = new DevExpress.XtraEditors.GroupControl();
			this.TextEdit_account_number = new DevExpress.XtraEditors.TextEdit();
			this.TextEdit_aba = new DevExpress.XtraEditors.TextEdit();
			this.CheckEdit_ach_enable_offset = new DevExpress.XtraEditors.CheckEdit();
			this.LabelControl2 = new DevExpress.XtraEditors.LabelControl();
			this.LabelControl1 = new DevExpress.XtraEditors.LabelControl();
			this.LabelControl8 = new DevExpress.XtraEditors.LabelControl();
			this.LabelControl9 = new DevExpress.XtraEditors.LabelControl();
			this.LabelControl10 = new DevExpress.XtraEditors.LabelControl();
			this.LabelControl13 = new DevExpress.XtraEditors.LabelControl();
			this.LabelControl14 = new DevExpress.XtraEditors.LabelControl();
			this.LabelControl15 = new DevExpress.XtraEditors.LabelControl();
			this.LabelControl16 = new DevExpress.XtraEditors.LabelControl();
			this.LabelControl17 = new DevExpress.XtraEditors.LabelControl();
			this.GroupControl3 = new DevExpress.XtraEditors.GroupControl();
			this.TextEdit_immediate_destination_name = new DevExpress.XtraEditors.TextEdit();
			this.TextEdit_immediate_destination = new DevExpress.XtraEditors.TextEdit();
			this.LabelControl11 = new DevExpress.XtraEditors.LabelControl();
			this.LabelControl12 = new DevExpress.XtraEditors.LabelControl();
			this.TextEdit_ach_priority = new DevExpress.XtraEditors.TextEdit();
			this.TextEdit_ach_company_id = new DevExpress.XtraEditors.TextEdit();
			this.TextEdit_ach_origin_dfi = new DevExpress.XtraEditors.TextEdit();
			this.TextEdit_ach_company_identification = new DevExpress.XtraEditors.TextEdit();
			this.TextEdit_ach_message_authentication = new DevExpress.XtraEditors.TextEdit();
			this.TextEdit_ach_batch_company_id = new DevExpress.XtraEditors.TextEdit();
			this.MemoExEdit_prefix_lines = new DevExpress.XtraEditors.MemoExEdit();
			this.MemoExEdit_suffix_lines = new DevExpress.XtraEditors.MemoExEdit();
			this.TextEdit_batch_number = new DevExpress.XtraEditors.TextEdit();
			this.TextEdit_transaction_number = new DevExpress.XtraEditors.TextEdit();
			this.ButtonEdit_output_directory = new DevExpress.XtraEditors.ButtonEdit();
			((System.ComponentModel.ISupportInitialize)this.GroupControl1).BeginInit();
			this.GroupControl1.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)this.TextEdit_immediate_origin_name.Properties).BeginInit();
			((System.ComponentModel.ISupportInitialize)this.TextEdit_immediate_origin.Properties).BeginInit();
			((System.ComponentModel.ISupportInitialize)this.GroupControl2).BeginInit();
			this.GroupControl2.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)this.TextEdit_account_number.Properties).BeginInit();
			((System.ComponentModel.ISupportInitialize)this.TextEdit_aba.Properties).BeginInit();
			((System.ComponentModel.ISupportInitialize)this.CheckEdit_ach_enable_offset.Properties).BeginInit();
			((System.ComponentModel.ISupportInitialize)this.GroupControl3).BeginInit();
			this.GroupControl3.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)this.TextEdit_immediate_destination_name.Properties).BeginInit();
			((System.ComponentModel.ISupportInitialize)this.TextEdit_immediate_destination.Properties).BeginInit();
			((System.ComponentModel.ISupportInitialize)this.TextEdit_ach_priority.Properties).BeginInit();
			((System.ComponentModel.ISupportInitialize)this.TextEdit_ach_company_id.Properties).BeginInit();
			((System.ComponentModel.ISupportInitialize)this.TextEdit_ach_origin_dfi.Properties).BeginInit();
			((System.ComponentModel.ISupportInitialize)this.TextEdit_ach_company_identification.Properties).BeginInit();
			((System.ComponentModel.ISupportInitialize)this.TextEdit_ach_message_authentication.Properties).BeginInit();
			((System.ComponentModel.ISupportInitialize)this.TextEdit_ach_batch_company_id.Properties).BeginInit();
			((System.ComponentModel.ISupportInitialize)this.MemoExEdit_prefix_lines.Properties).BeginInit();
			((System.ComponentModel.ISupportInitialize)this.MemoExEdit_suffix_lines.Properties).BeginInit();
			((System.ComponentModel.ISupportInitialize)this.TextEdit_batch_number.Properties).BeginInit();
			((System.ComponentModel.ISupportInitialize)this.TextEdit_transaction_number.Properties).BeginInit();
			((System.ComponentModel.ISupportInitialize)this.ButtonEdit_output_directory.Properties).BeginInit();
			this.SuspendLayout();
			//
			//GroupControl1
			//
			this.GroupControl1.Controls.Add(this.TextEdit_immediate_origin_name);
			this.GroupControl1.Controls.Add(this.TextEdit_immediate_origin);
			this.GroupControl1.Controls.Add(this.LabelControl4);
			this.GroupControl1.Controls.Add(this.LabelControl3);
			this.GroupControl1.Location = new System.Drawing.Point(281, 3);
			this.GroupControl1.Name = "GroupControl1";
			this.GroupControl1.Size = new System.Drawing.Size(200, 78);
			this.GroupControl1.TabIndex = 1;
			this.GroupControl1.Text = "Immediate Origin";
			//
			//TextEdit_immediate_origin_name
			//
			this.TextEdit_immediate_origin_name.Location = new System.Drawing.Point(45, 49);
			this.TextEdit_immediate_origin_name.Name = "TextEdit_immediate_origin_name";
			this.TextEdit_immediate_origin_name.Properties.CharacterCasing = CharacterCasing.Upper;
			this.TextEdit_immediate_origin_name.Properties.MaxLength = 30;
			this.TextEdit_immediate_origin_name.Size = new System.Drawing.Size(143, 20);
			this.TextEdit_immediate_origin_name.TabIndex = 3;
			//
			//TextEdit_immediate_origin
			//
			this.TextEdit_immediate_origin.Location = new System.Drawing.Point(45, 23);
			this.TextEdit_immediate_origin.Name = "TextEdit_immediate_origin";
			this.TextEdit_immediate_origin.Properties.Appearance.Options.UseTextOptions = true;
			this.TextEdit_immediate_origin.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
			this.TextEdit_immediate_origin.Properties.AppearanceDisabled.Options.UseTextOptions = true;
			this.TextEdit_immediate_origin.Properties.AppearanceDisabled.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
			this.TextEdit_immediate_origin.Properties.AppearanceFocused.Options.UseTextOptions = true;
			this.TextEdit_immediate_origin.Properties.AppearanceFocused.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
			this.TextEdit_immediate_origin.Properties.AppearanceReadOnly.Options.UseTextOptions = true;
			this.TextEdit_immediate_origin.Properties.AppearanceReadOnly.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
			this.TextEdit_immediate_origin.Properties.Mask.EditMask = "[ 0-9]{0,12}";
			this.TextEdit_immediate_origin.Properties.Mask.IgnoreMaskBlank = false;
			this.TextEdit_immediate_origin.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.RegEx;
			this.TextEdit_immediate_origin.Properties.MaxLength = 12;
			this.TextEdit_immediate_origin.Size = new System.Drawing.Size(143, 20);
			this.TextEdit_immediate_origin.TabIndex = 1;
			//
			//LabelControl4
			//
			this.LabelControl4.Location = new System.Drawing.Point(5, 52);
			this.LabelControl4.Name = "LabelControl4";
			this.LabelControl4.Size = new System.Drawing.Size(27, 13);
			this.LabelControl4.TabIndex = 2;
			this.LabelControl4.Text = "Name";
			//
			//LabelControl3
			//
			this.LabelControl3.Location = new System.Drawing.Point(5, 26);
			this.LabelControl3.Name = "LabelControl3";
			this.LabelControl3.Size = new System.Drawing.Size(11, 13);
			this.LabelControl3.TabIndex = 0;
			this.LabelControl3.Text = "ID";
			//
			//LabelControl5
			//
			this.LabelControl5.Location = new System.Drawing.Point(11, 115);
			this.LabelControl5.Name = "LabelControl5";
			this.LabelControl5.Size = new System.Drawing.Size(34, 13);
			this.LabelControl5.TabIndex = 3;
			this.LabelControl5.Text = "Priority";
			//
			//LabelControl6
			//
			this.LabelControl6.Location = new System.Drawing.Point(11, 137);
			this.LabelControl6.Name = "LabelControl6";
			this.LabelControl6.Size = new System.Drawing.Size(59, 13);
			this.LabelControl6.TabIndex = 5;
			this.LabelControl6.Text = "Company ID";
			//
			//LabelControl7
			//
			this.LabelControl7.Location = new System.Drawing.Point(11, 162);
			this.LabelControl7.Name = "LabelControl7";
			this.LabelControl7.Size = new System.Drawing.Size(48, 13);
			this.LabelControl7.TabIndex = 7;
			this.LabelControl7.Text = "Origin DFI";
			//
			//GroupControl2
			//
			this.GroupControl2.Controls.Add(this.TextEdit_account_number);
			this.GroupControl2.Controls.Add(this.TextEdit_aba);
			this.GroupControl2.Controls.Add(this.CheckEdit_ach_enable_offset);
			this.GroupControl2.Controls.Add(this.LabelControl2);
			this.GroupControl2.Controls.Add(this.LabelControl1);
			this.GroupControl2.Location = new System.Drawing.Point(6, 3);
			this.GroupControl2.Name = "GroupControl2";
			this.GroupControl2.Size = new System.Drawing.Size(200, 101);
			this.GroupControl2.TabIndex = 0;
			this.GroupControl2.Text = "Account Information";
			//
			//TextEdit_account_number
			//
			this.TextEdit_account_number.Location = new System.Drawing.Point(95, 49);
			this.TextEdit_account_number.Name = "TextEdit_account_number";
			this.TextEdit_account_number.Properties.Appearance.Options.UseTextOptions = true;
			this.TextEdit_account_number.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
			this.TextEdit_account_number.Properties.AppearanceDisabled.Options.UseTextOptions = true;
			this.TextEdit_account_number.Properties.AppearanceDisabled.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
			this.TextEdit_account_number.Properties.AppearanceFocused.Options.UseTextOptions = true;
			this.TextEdit_account_number.Properties.AppearanceFocused.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
			this.TextEdit_account_number.Properties.AppearanceReadOnly.Options.UseTextOptions = true;
			this.TextEdit_account_number.Properties.AppearanceReadOnly.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
			this.TextEdit_account_number.Properties.Mask.BeepOnError = true;
			this.TextEdit_account_number.Properties.Mask.EditMask = "\\d+";
			this.TextEdit_account_number.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.RegEx;
			this.TextEdit_account_number.Properties.MaxLength = 40;
			this.TextEdit_account_number.Size = new System.Drawing.Size(100, 20);
			this.TextEdit_account_number.TabIndex = 3;
			//
			//TextEdit_aba
			//
			this.TextEdit_aba.Location = new System.Drawing.Point(95, 23);
			this.TextEdit_aba.Name = "TextEdit_aba";
			this.TextEdit_aba.Properties.Appearance.Options.UseTextOptions = true;
			this.TextEdit_aba.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
			this.TextEdit_aba.Properties.AppearanceDisabled.Options.UseTextOptions = true;
			this.TextEdit_aba.Properties.AppearanceDisabled.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
			this.TextEdit_aba.Properties.AppearanceFocused.Options.UseTextOptions = true;
			this.TextEdit_aba.Properties.AppearanceFocused.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
			this.TextEdit_aba.Properties.AppearanceReadOnly.Options.UseTextOptions = true;
			this.TextEdit_aba.Properties.AppearanceReadOnly.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
			this.TextEdit_aba.Properties.Mask.BeepOnError = true;
			this.TextEdit_aba.Properties.Mask.EditMask = "\\d{9}";
			this.TextEdit_aba.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Regular;
			this.TextEdit_aba.Properties.Mask.SaveLiteral = false;
			this.TextEdit_aba.Properties.MaxLength = 9;
			this.TextEdit_aba.Size = new System.Drawing.Size(100, 20);
			this.TextEdit_aba.TabIndex = 1;
			//
			//CheckEdit_ach_enable_offset
			//
			this.CheckEdit_ach_enable_offset.Location = new System.Drawing.Point(3, 75);
			this.CheckEdit_ach_enable_offset.Name = "CheckEdit_ach_enable_offset";
			this.CheckEdit_ach_enable_offset.Properties.Caption = "Generate Offsetting Credit";
			this.CheckEdit_ach_enable_offset.Size = new System.Drawing.Size(163, 20);
			this.CheckEdit_ach_enable_offset.TabIndex = 4;
			//
			//LabelControl2
			//
			this.LabelControl2.Location = new System.Drawing.Point(5, 52);
			this.LabelControl2.Name = "LabelControl2";
			this.LabelControl2.Size = new System.Drawing.Size(79, 13);
			this.LabelControl2.TabIndex = 2;
			this.LabelControl2.Text = "Account Number";
			//
			//LabelControl1
			//
			this.LabelControl1.Location = new System.Drawing.Point(5, 26);
			this.LabelControl1.Name = "LabelControl1";
			this.LabelControl1.Size = new System.Drawing.Size(60, 13);
			this.LabelControl1.TabIndex = 0;
			this.LabelControl1.Text = "ABA Number";
			//
			//LabelControl8
			//
			this.LabelControl8.Location = new System.Drawing.Point(11, 186);
			this.LabelControl8.Name = "LabelControl8";
			this.LabelControl8.Size = new System.Drawing.Size(111, 13);
			this.LabelControl8.TabIndex = 9;
			this.LabelControl8.Text = "Company Identification";
			//
			//LabelControl9
			//
			this.LabelControl9.Location = new System.Drawing.Point(11, 212);
			this.LabelControl9.Name = "LabelControl9";
			this.LabelControl9.Size = new System.Drawing.Size(115, 13);
			this.LabelControl9.TabIndex = 13;
			this.LabelControl9.Text = "Message Authentication";
			//
			//LabelControl10
			//
			this.LabelControl10.Location = new System.Drawing.Point(11, 238);
			this.LabelControl10.Name = "LabelControl10";
			this.LabelControl10.Size = new System.Drawing.Size(89, 13);
			this.LabelControl10.TabIndex = 17;
			this.LabelControl10.Text = "Batch Company ID";
			//
			//LabelControl13
			//
			this.LabelControl13.Location = new System.Drawing.Point(281, 228);
			this.LabelControl13.Name = "LabelControl13";
			this.LabelControl13.Size = new System.Drawing.Size(67, 13);
			this.LabelControl13.TabIndex = 19;
			this.LabelControl13.Text = "Batch Number";
			//
			//LabelControl14
			//
			this.LabelControl14.Location = new System.Drawing.Point(281, 256);
			this.LabelControl14.Name = "LabelControl14";
			this.LabelControl14.Size = new System.Drawing.Size(96, 13);
			this.LabelControl14.TabIndex = 21;
			this.LabelControl14.Text = "Transaction Number";
			//
			//LabelControl15
			//
			this.LabelControl15.Location = new System.Drawing.Point(281, 175);
			this.LabelControl15.Name = "LabelControl15";
			this.LabelControl15.Size = new System.Drawing.Size(60, 13);
			this.LabelControl15.TabIndex = 11;
			this.LabelControl15.Text = "Prefix line(s)";
			//
			//LabelControl16
			//
			this.LabelControl16.Location = new System.Drawing.Point(281, 201);
			this.LabelControl16.Name = "LabelControl16";
			this.LabelControl16.Size = new System.Drawing.Size(60, 13);
			this.LabelControl16.TabIndex = 15;
			this.LabelControl16.Text = "Suffix line(s)";
			//
			//LabelControl17
			//
			this.LabelControl17.Location = new System.Drawing.Point(11, 283);
			this.LabelControl17.Name = "LabelControl17";
			this.LabelControl17.Size = new System.Drawing.Size(106, 13);
			this.LabelControl17.TabIndex = 23;
			this.LabelControl17.Text = "Output Directory Path";
			//
			//GroupControl3
			//
			this.GroupControl3.Controls.Add(this.TextEdit_immediate_destination_name);
			this.GroupControl3.Controls.Add(this.TextEdit_immediate_destination);
			this.GroupControl3.Controls.Add(this.LabelControl11);
			this.GroupControl3.Controls.Add(this.LabelControl12);
			this.GroupControl3.Location = new System.Drawing.Point(281, 87);
			this.GroupControl3.Name = "GroupControl3";
			this.GroupControl3.Size = new System.Drawing.Size(200, 78);
			this.GroupControl3.TabIndex = 2;
			this.GroupControl3.Text = "Immediate Destination";
			//
			//TextEdit_immediate_destination_name
			//
			this.TextEdit_immediate_destination_name.Location = new System.Drawing.Point(45, 49);
			this.TextEdit_immediate_destination_name.Name = "TextEdit_immediate_destination_name";
			this.TextEdit_immediate_destination_name.Properties.CharacterCasing = CharacterCasing.Upper;
			this.TextEdit_immediate_destination_name.Properties.MaxLength = 30;
			this.TextEdit_immediate_destination_name.Size = new System.Drawing.Size(143, 20);
			this.TextEdit_immediate_destination_name.TabIndex = 3;
			//
			//TextEdit_immediate_destination
			//
			this.TextEdit_immediate_destination.Location = new System.Drawing.Point(45, 23);
			this.TextEdit_immediate_destination.Name = "TextEdit_immediate_destination";
			this.TextEdit_immediate_destination.Properties.Appearance.Options.UseTextOptions = true;
			this.TextEdit_immediate_destination.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
			this.TextEdit_immediate_destination.Properties.AppearanceDisabled.Options.UseTextOptions = true;
			this.TextEdit_immediate_destination.Properties.AppearanceDisabled.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
			this.TextEdit_immediate_destination.Properties.AppearanceFocused.Options.UseTextOptions = true;
			this.TextEdit_immediate_destination.Properties.AppearanceFocused.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
			this.TextEdit_immediate_destination.Properties.AppearanceReadOnly.Options.UseTextOptions = true;
			this.TextEdit_immediate_destination.Properties.AppearanceReadOnly.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
			this.TextEdit_immediate_destination.Properties.Mask.BeepOnError = true;
			this.TextEdit_immediate_destination.Properties.Mask.EditMask = "[ 0-9]{0,12}";
			this.TextEdit_immediate_destination.Properties.Mask.IgnoreMaskBlank = false;
			this.TextEdit_immediate_destination.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.RegEx;
			this.TextEdit_immediate_destination.Properties.MaxLength = 12;
			this.TextEdit_immediate_destination.Size = new System.Drawing.Size(143, 20);
			this.TextEdit_immediate_destination.TabIndex = 1;
			//
			//LabelControl11
			//
			this.LabelControl11.Location = new System.Drawing.Point(5, 52);
			this.LabelControl11.Name = "LabelControl11";
			this.LabelControl11.Size = new System.Drawing.Size(27, 13);
			this.LabelControl11.TabIndex = 2;
			this.LabelControl11.Text = "Name";
			//
			//LabelControl12
			//
			this.LabelControl12.Location = new System.Drawing.Point(5, 26);
			this.LabelControl12.Name = "LabelControl12";
			this.LabelControl12.Size = new System.Drawing.Size(11, 13);
			this.LabelControl12.TabIndex = 0;
			this.LabelControl12.Text = "ID";
			//
			//TextEdit_ach_priority
			//
			this.TextEdit_ach_priority.Location = new System.Drawing.Point(132, 110);
			this.TextEdit_ach_priority.Name = "TextEdit_ach_priority";
			this.TextEdit_ach_priority.Properties.Appearance.Options.UseTextOptions = true;
			this.TextEdit_ach_priority.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
			this.TextEdit_ach_priority.Properties.AppearanceDisabled.Options.UseTextOptions = true;
			this.TextEdit_ach_priority.Properties.AppearanceDisabled.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
			this.TextEdit_ach_priority.Properties.AppearanceFocused.Options.UseTextOptions = true;
			this.TextEdit_ach_priority.Properties.AppearanceFocused.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
			this.TextEdit_ach_priority.Properties.AppearanceReadOnly.Options.UseTextOptions = true;
			this.TextEdit_ach_priority.Properties.AppearanceReadOnly.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
			this.TextEdit_ach_priority.Properties.Mask.BeepOnError = true;
			this.TextEdit_ach_priority.Properties.Mask.EditMask = "\\d{0,2}";
			this.TextEdit_ach_priority.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.RegEx;
			this.TextEdit_ach_priority.Properties.MaxLength = 2;
			this.TextEdit_ach_priority.Size = new System.Drawing.Size(24, 20);
			this.TextEdit_ach_priority.TabIndex = 4;
			//
			//TextEdit_ach_company_id
			//
			this.TextEdit_ach_company_id.Location = new System.Drawing.Point(132, 134);
			this.TextEdit_ach_company_id.Name = "TextEdit_ach_company_id";
			this.TextEdit_ach_company_id.Properties.Appearance.Options.UseTextOptions = true;
			this.TextEdit_ach_company_id.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
			this.TextEdit_ach_company_id.Properties.AppearanceDisabled.Options.UseTextOptions = true;
			this.TextEdit_ach_company_id.Properties.AppearanceDisabled.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
			this.TextEdit_ach_company_id.Properties.AppearanceFocused.Options.UseTextOptions = true;
			this.TextEdit_ach_company_id.Properties.AppearanceFocused.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
			this.TextEdit_ach_company_id.Properties.AppearanceReadOnly.Options.UseTextOptions = true;
			this.TextEdit_ach_company_id.Properties.AppearanceReadOnly.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
			this.TextEdit_ach_company_id.Properties.MaxLength = 10;
			this.TextEdit_ach_company_id.Size = new System.Drawing.Size(81, 20);
			this.TextEdit_ach_company_id.TabIndex = 6;
			//
			//TextEdit_ach_origin_dfi
			//
			this.TextEdit_ach_origin_dfi.Location = new System.Drawing.Point(132, 159);
			this.TextEdit_ach_origin_dfi.Name = "TextEdit_ach_origin_dfi";
			this.TextEdit_ach_origin_dfi.Properties.Appearance.Options.UseTextOptions = true;
			this.TextEdit_ach_origin_dfi.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
			this.TextEdit_ach_origin_dfi.Properties.AppearanceDisabled.Options.UseTextOptions = true;
			this.TextEdit_ach_origin_dfi.Properties.AppearanceDisabled.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
			this.TextEdit_ach_origin_dfi.Properties.AppearanceFocused.Options.UseTextOptions = true;
			this.TextEdit_ach_origin_dfi.Properties.AppearanceFocused.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
			this.TextEdit_ach_origin_dfi.Properties.AppearanceReadOnly.Options.UseTextOptions = true;
			this.TextEdit_ach_origin_dfi.Properties.AppearanceReadOnly.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
			this.TextEdit_ach_origin_dfi.Properties.MaxLength = 12;
			this.TextEdit_ach_origin_dfi.Size = new System.Drawing.Size(81, 20);
			this.TextEdit_ach_origin_dfi.TabIndex = 8;
			//
			//TextEdit_ach_company_identification
			//
			this.TextEdit_ach_company_identification.Location = new System.Drawing.Point(132, 183);
			this.TextEdit_ach_company_identification.Name = "TextEdit_ach_company_identification";
			this.TextEdit_ach_company_identification.Properties.Appearance.Options.UseTextOptions = true;
			this.TextEdit_ach_company_identification.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
			this.TextEdit_ach_company_identification.Properties.AppearanceDisabled.Options.UseTextOptions = true;
			this.TextEdit_ach_company_identification.Properties.AppearanceDisabled.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
			this.TextEdit_ach_company_identification.Properties.AppearanceFocused.Options.UseTextOptions = true;
			this.TextEdit_ach_company_identification.Properties.AppearanceFocused.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
			this.TextEdit_ach_company_identification.Properties.AppearanceReadOnly.Options.UseTextOptions = true;
			this.TextEdit_ach_company_identification.Properties.AppearanceReadOnly.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
			this.TextEdit_ach_company_identification.Properties.MaxLength = 10;
			this.TextEdit_ach_company_identification.Size = new System.Drawing.Size(81, 20);
			this.TextEdit_ach_company_identification.TabIndex = 10;
			//
			//TextEdit_ach_message_authentication
			//
			this.TextEdit_ach_message_authentication.Location = new System.Drawing.Point(132, 209);
			this.TextEdit_ach_message_authentication.Name = "TextEdit_ach_message_authentication";
			this.TextEdit_ach_message_authentication.Properties.Appearance.Options.UseTextOptions = true;
			this.TextEdit_ach_message_authentication.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
			this.TextEdit_ach_message_authentication.Properties.AppearanceDisabled.Options.UseTextOptions = true;
			this.TextEdit_ach_message_authentication.Properties.AppearanceDisabled.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
			this.TextEdit_ach_message_authentication.Properties.AppearanceFocused.Options.UseTextOptions = true;
			this.TextEdit_ach_message_authentication.Properties.AppearanceFocused.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
			this.TextEdit_ach_message_authentication.Properties.AppearanceReadOnly.Options.UseTextOptions = true;
			this.TextEdit_ach_message_authentication.Properties.AppearanceReadOnly.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
			this.TextEdit_ach_message_authentication.Properties.MaxLength = 19;
			this.TextEdit_ach_message_authentication.Size = new System.Drawing.Size(81, 20);
			this.TextEdit_ach_message_authentication.TabIndex = 14;
			//
			//TextEdit_ach_batch_company_id
			//
			this.TextEdit_ach_batch_company_id.Location = new System.Drawing.Point(132, 235);
			this.TextEdit_ach_batch_company_id.Name = "TextEdit_ach_batch_company_id";
			this.TextEdit_ach_batch_company_id.Properties.Appearance.Options.UseTextOptions = true;
			this.TextEdit_ach_batch_company_id.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
			this.TextEdit_ach_batch_company_id.Properties.AppearanceDisabled.Options.UseTextOptions = true;
			this.TextEdit_ach_batch_company_id.Properties.AppearanceDisabled.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
			this.TextEdit_ach_batch_company_id.Properties.AppearanceFocused.Options.UseTextOptions = true;
			this.TextEdit_ach_batch_company_id.Properties.AppearanceFocused.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
			this.TextEdit_ach_batch_company_id.Properties.AppearanceReadOnly.Options.UseTextOptions = true;
			this.TextEdit_ach_batch_company_id.Properties.AppearanceReadOnly.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
			this.TextEdit_ach_batch_company_id.Properties.Mask.BeepOnError = true;
			this.TextEdit_ach_batch_company_id.Properties.Mask.EditMask = "\\d*";
			this.TextEdit_ach_batch_company_id.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.RegEx;
			this.TextEdit_ach_batch_company_id.Properties.MaxLength = 10;
			this.TextEdit_ach_batch_company_id.Size = new System.Drawing.Size(81, 20);
			this.TextEdit_ach_batch_company_id.TabIndex = 18;
			//
			//MemoExEdit_prefix_lines
			//
			this.MemoExEdit_prefix_lines.Location = new System.Drawing.Point(381, 171);
			this.MemoExEdit_prefix_lines.Name = "MemoExEdit_prefix_lines";
			this.MemoExEdit_prefix_lines.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.True;
			this.MemoExEdit_prefix_lines.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] { new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo) });
			this.MemoExEdit_prefix_lines.Properties.MaxLength = 256;
			this.MemoExEdit_prefix_lines.Properties.ScrollBars = ScrollBars.Vertical;
			this.MemoExEdit_prefix_lines.Properties.ShowIcon = false;
			this.MemoExEdit_prefix_lines.Properties.WordWrap = false;
			this.MemoExEdit_prefix_lines.Size = new System.Drawing.Size(100, 20);
			this.MemoExEdit_prefix_lines.TabIndex = 12;
			//
			//MemoExEdit_suffix_lines
			//
			this.MemoExEdit_suffix_lines.Location = new System.Drawing.Point(381, 197);
			this.MemoExEdit_suffix_lines.Name = "MemoExEdit_suffix_lines";
			this.MemoExEdit_suffix_lines.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.True;
			this.MemoExEdit_suffix_lines.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] { new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo) });
			this.MemoExEdit_suffix_lines.Properties.MaxLength = 256;
			this.MemoExEdit_suffix_lines.Properties.ScrollBars = ScrollBars.Vertical;
			this.MemoExEdit_suffix_lines.Properties.ShowIcon = false;
			this.MemoExEdit_suffix_lines.Properties.WordWrap = false;
			this.MemoExEdit_suffix_lines.Size = new System.Drawing.Size(100, 20);
			this.MemoExEdit_suffix_lines.TabIndex = 16;
			//
			//TextEdit_batch_number
			//
			this.TextEdit_batch_number.Location = new System.Drawing.Point(381, 224);
			this.TextEdit_batch_number.Name = "TextEdit_batch_number";
			this.TextEdit_batch_number.Properties.Appearance.Options.UseTextOptions = true;
			this.TextEdit_batch_number.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
			this.TextEdit_batch_number.Properties.AppearanceDisabled.Options.UseTextOptions = true;
			this.TextEdit_batch_number.Properties.AppearanceDisabled.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
			this.TextEdit_batch_number.Properties.AppearanceFocused.Options.UseTextOptions = true;
			this.TextEdit_batch_number.Properties.AppearanceFocused.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
			this.TextEdit_batch_number.Properties.AppearanceReadOnly.Options.UseTextOptions = true;
			this.TextEdit_batch_number.Properties.AppearanceReadOnly.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
			this.TextEdit_batch_number.Properties.DisplayFormat.FormatString = "f0";
			this.TextEdit_batch_number.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
			this.TextEdit_batch_number.Properties.EditFormat.FormatString = "f0";
			this.TextEdit_batch_number.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
			this.TextEdit_batch_number.Properties.Mask.BeepOnError = true;
			this.TextEdit_batch_number.Properties.Mask.EditMask = "f0";
			this.TextEdit_batch_number.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
			this.TextEdit_batch_number.Size = new System.Drawing.Size(100, 20);
			this.TextEdit_batch_number.TabIndex = 20;
			//
			//TextEdit_transaction_number
			//
			this.TextEdit_transaction_number.Location = new System.Drawing.Point(381, 252);
			this.TextEdit_transaction_number.Name = "TextEdit_transaction_number";
			this.TextEdit_transaction_number.Properties.Appearance.Options.UseTextOptions = true;
			this.TextEdit_transaction_number.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
			this.TextEdit_transaction_number.Properties.AppearanceDisabled.Options.UseTextOptions = true;
			this.TextEdit_transaction_number.Properties.AppearanceDisabled.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
			this.TextEdit_transaction_number.Properties.AppearanceFocused.Options.UseTextOptions = true;
			this.TextEdit_transaction_number.Properties.AppearanceFocused.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
			this.TextEdit_transaction_number.Properties.AppearanceReadOnly.Options.UseTextOptions = true;
			this.TextEdit_transaction_number.Properties.AppearanceReadOnly.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
			this.TextEdit_transaction_number.Properties.DisplayFormat.FormatString = "f0";
			this.TextEdit_transaction_number.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
			this.TextEdit_transaction_number.Properties.EditFormat.FormatString = "f0";
			this.TextEdit_transaction_number.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
			this.TextEdit_transaction_number.Properties.Mask.BeepOnError = true;
			this.TextEdit_transaction_number.Properties.Mask.EditMask = "f0";
			this.TextEdit_transaction_number.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
			this.TextEdit_transaction_number.Size = new System.Drawing.Size(100, 20);
			this.TextEdit_transaction_number.TabIndex = 22;
			//
			//ButtonEdit_output_directory
			//
			this.ButtonEdit_output_directory.Location = new System.Drawing.Point(132, 280);
			this.ButtonEdit_output_directory.Name = "ButtonEdit_output_directory";
			this.ButtonEdit_output_directory.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] { new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Right) });
			this.ButtonEdit_output_directory.Properties.MaxLength = 256;
			this.ButtonEdit_output_directory.Size = new System.Drawing.Size(349, 20);
			this.ButtonEdit_output_directory.TabIndex = 51;
			//
			//Banks_A
			//
			this.Appearance.BackColor = System.Drawing.Color.Transparent;
			this.Appearance.Options.UseBackColor = true;
			this.AutoScaleDimensions = new System.Drawing.SizeF(6f, 13f);
			this.AutoScaleMode = AutoScaleMode.Font;
			this.Controls.Add(this.ButtonEdit_output_directory);
			this.Controls.Add(this.TextEdit_transaction_number);
			this.Controls.Add(this.TextEdit_batch_number);
			this.Controls.Add(this.MemoExEdit_suffix_lines);
			this.Controls.Add(this.MemoExEdit_prefix_lines);
			this.Controls.Add(this.TextEdit_ach_batch_company_id);
			this.Controls.Add(this.TextEdit_ach_message_authentication);
			this.Controls.Add(this.TextEdit_ach_company_identification);
			this.Controls.Add(this.TextEdit_ach_origin_dfi);
			this.Controls.Add(this.TextEdit_ach_company_id);
			this.Controls.Add(this.TextEdit_ach_priority);
			this.Controls.Add(this.GroupControl3);
			this.Controls.Add(this.LabelControl17);
			this.Controls.Add(this.LabelControl16);
			this.Controls.Add(this.LabelControl15);
			this.Controls.Add(this.LabelControl14);
			this.Controls.Add(this.LabelControl13);
			this.Controls.Add(this.LabelControl10);
			this.Controls.Add(this.LabelControl9);
			this.Controls.Add(this.LabelControl8);
			this.Controls.Add(this.GroupControl2);
			this.Controls.Add(this.LabelControl7);
			this.Controls.Add(this.LabelControl6);
			this.Controls.Add(this.LabelControl5);
			this.Controls.Add(this.GroupControl1);
			this.Name = "Banks_A";
			((System.ComponentModel.ISupportInitialize)this.GroupControl1).EndInit();
			this.GroupControl1.ResumeLayout(false);
			this.GroupControl1.PerformLayout();
			((System.ComponentModel.ISupportInitialize)this.TextEdit_immediate_origin_name.Properties).EndInit();
			((System.ComponentModel.ISupportInitialize)this.TextEdit_immediate_origin.Properties).EndInit();
			((System.ComponentModel.ISupportInitialize)this.GroupControl2).EndInit();
			this.GroupControl2.ResumeLayout(false);
			this.GroupControl2.PerformLayout();
			((System.ComponentModel.ISupportInitialize)this.TextEdit_account_number.Properties).EndInit();
			((System.ComponentModel.ISupportInitialize)this.TextEdit_aba.Properties).EndInit();
			((System.ComponentModel.ISupportInitialize)this.CheckEdit_ach_enable_offset.Properties).EndInit();
			((System.ComponentModel.ISupportInitialize)this.GroupControl3).EndInit();
			this.GroupControl3.ResumeLayout(false);
			this.GroupControl3.PerformLayout();
			((System.ComponentModel.ISupportInitialize)this.TextEdit_immediate_destination_name.Properties).EndInit();
			((System.ComponentModel.ISupportInitialize)this.TextEdit_immediate_destination.Properties).EndInit();
			((System.ComponentModel.ISupportInitialize)this.TextEdit_ach_priority.Properties).EndInit();
			((System.ComponentModel.ISupportInitialize)this.TextEdit_ach_company_id.Properties).EndInit();
			((System.ComponentModel.ISupportInitialize)this.TextEdit_ach_origin_dfi.Properties).EndInit();
			((System.ComponentModel.ISupportInitialize)this.TextEdit_ach_company_identification.Properties).EndInit();
			((System.ComponentModel.ISupportInitialize)this.TextEdit_ach_message_authentication.Properties).EndInit();
			((System.ComponentModel.ISupportInitialize)this.TextEdit_ach_batch_company_id.Properties).EndInit();
			((System.ComponentModel.ISupportInitialize)this.MemoExEdit_prefix_lines.Properties).EndInit();
			((System.ComponentModel.ISupportInitialize)this.MemoExEdit_suffix_lines.Properties).EndInit();
			((System.ComponentModel.ISupportInitialize)this.TextEdit_batch_number.Properties).EndInit();
			((System.ComponentModel.ISupportInitialize)this.TextEdit_transaction_number.Properties).EndInit();
			((System.ComponentModel.ISupportInitialize)this.ButtonEdit_output_directory.Properties).EndInit();
			this.ResumeLayout(false);
			this.PerformLayout();

		}
		private DevExpress.XtraEditors.GroupControl GroupControl1;
		private DevExpress.XtraEditors.LabelControl LabelControl4;
		private DevExpress.XtraEditors.LabelControl LabelControl3;
		private DevExpress.XtraEditors.LabelControl LabelControl5;
		private DevExpress.XtraEditors.LabelControl LabelControl6;
		private DevExpress.XtraEditors.LabelControl LabelControl7;
		private DevExpress.XtraEditors.GroupControl GroupControl2;
		private DevExpress.XtraEditors.CheckEdit CheckEdit_ach_enable_offset;
		private DevExpress.XtraEditors.LabelControl LabelControl2;
		private DevExpress.XtraEditors.LabelControl LabelControl1;
		private DevExpress.XtraEditors.LabelControl LabelControl8;
		private DevExpress.XtraEditors.LabelControl LabelControl9;
		private DevExpress.XtraEditors.LabelControl LabelControl10;
		private DevExpress.XtraEditors.LabelControl LabelControl13;
		private DevExpress.XtraEditors.LabelControl LabelControl14;
		private DevExpress.XtraEditors.LabelControl LabelControl15;
		private DevExpress.XtraEditors.TextEdit TextEdit_immediate_origin_name;
		private DevExpress.XtraEditors.TextEdit TextEdit_immediate_origin;
		private DevExpress.XtraEditors.TextEdit TextEdit_account_number;
		private DevExpress.XtraEditors.TextEdit TextEdit_aba;
		private DevExpress.XtraEditors.LabelControl LabelControl16;
		private DevExpress.XtraEditors.LabelControl LabelControl17;
		private DevExpress.XtraEditors.GroupControl GroupControl3;
		private DevExpress.XtraEditors.TextEdit TextEdit_immediate_destination_name;
		private DevExpress.XtraEditors.TextEdit TextEdit_immediate_destination;
		private DevExpress.XtraEditors.LabelControl LabelControl11;
		private DevExpress.XtraEditors.LabelControl LabelControl12;
		private DevExpress.XtraEditors.TextEdit TextEdit_ach_priority;
		private DevExpress.XtraEditors.TextEdit TextEdit_ach_company_id;
		private DevExpress.XtraEditors.TextEdit TextEdit_ach_origin_dfi;
		private DevExpress.XtraEditors.TextEdit TextEdit_ach_company_identification;
		private DevExpress.XtraEditors.TextEdit TextEdit_ach_message_authentication;
		private DevExpress.XtraEditors.TextEdit TextEdit_ach_batch_company_id;
		private DevExpress.XtraEditors.MemoExEdit MemoExEdit_prefix_lines;
		private DevExpress.XtraEditors.MemoExEdit MemoExEdit_suffix_lines;
		private DevExpress.XtraEditors.TextEdit TextEdit_batch_number;
		private DevExpress.XtraEditors.TextEdit TextEdit_transaction_number;

		private DevExpress.XtraEditors.ButtonEdit ButtonEdit_output_directory;
	}
}

//=======================================================
//Service provided by Telerik (www.telerik.com)
//Conversion powered by NRefactory.
//Twitter: @telerik
//Facebook: facebook.com/telerik
//=======================================================
