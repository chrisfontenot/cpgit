#region "Copyright 2000-2012 DebtPlus, L.L.C."
//{*******************************************************************}
//{                                                                   }
//{       DebtPlus Debt Management System                             }
//{                                                                   }
//{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
//{       ALL RIGHTS RESERVED                                         }
//{                                                                   }
//{   The entire contents of this file is protected by U.S. and       }
//{   International Copyright Laws. Unauthorized reproduction,        }
//{   reverse-engineering, and distribution of all or any portion of  }
//{   the code contained in this file is strictly prohibited and may  }
//{   result in severe civil and criminal penalties and will be       }
//{   prosecuted to the maximum extent possible under the law.        }
//{                                                                   }
//{   RESTRICTIONS                                                    }
//{                                                                   }
//{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
//{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
//{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
//{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
//{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
//{                                                                   }
//{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
//{   ADDITIONAL RESTRICTIONS.                                        }
//{                                                                   }
//{*******************************************************************}

#endregion "Copyright 2000-2012 DebtPlus, L.L.C."

using DebtPlus.LINQ;

namespace DebtPlus.UI.TableAdministration.CS.Bank.Controls
{
    internal partial class AddressInformation : DevExpress.XtraEditors.XtraUserControl
    {
        internal AddressInformation()
            : base()
        {
            InitializeComponent();
        }

        internal void SaveForm(BusinessContext bc, bank record)
        {
            record.ContactNameID = NameRecordControl1.EditValue;
            record.BankAddressID = AddressRecordControl1.EditValue;
            record.bank_name = DebtPlus.Utils.Nulls.v_String(TextEdit_name.EditValue);
            record.aba = DebtPlus.Utils.Nulls.v_String(TextEdit_aba.EditValue);
            record.account_number = DebtPlus.Utils.Nulls.v_String(TextEdit_account_number.EditValue);
        }

        internal void ReadForm(BusinessContext bc, bank record)
        {
            NameRecordControl1.EditValue = record.ContactNameID;
            AddressRecordControl1.EditValue = record.BankAddressID;
            TextEdit_name.EditValue = record.bank_name;
            TextEdit_aba.EditValue = record.aba;
            TextEdit_account_number.EditValue = record.account_number;
        }

        internal bool HasErrors()
        {
            return false;
        }
    }
}