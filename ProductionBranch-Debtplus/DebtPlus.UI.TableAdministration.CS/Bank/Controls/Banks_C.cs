using System;
using System.Windows.Forms;
using DebtPlus.LINQ;
using DevExpress.XtraEditors.Controls;

namespace DebtPlus.UI.TableAdministration.CS.Bank.Controls
{
    internal partial class Banks_C : Banks_Common
    {
        private bank record = null;
        private BusinessContext bc = null;

        /// <summary>
        /// Initialize the new class instance
        /// </summary>
        internal Banks_C()
            : base()
        {
            InitializeComponent();
        }

        /// <summary>
        /// Do any one-time initialization for the data.
        /// </summary>
        internal override void ReadForm(BusinessContext bc, bank record)
        {
            base.ReadForm(bc, record);
            this.record = record;
            this.bc = bc;
        }

        /// <summary>
        /// Register the event handlers
        /// </summary>
        private void RegisterHandlers()
        {
            SimpleButton_UpdateSignature.Click += SimpleButton_UpdateSignature_Click;
            TextEdit_aba.EditValueChanged += HandleFormChanged;
            TextEdit_account_number.EditValueChanged += HandleFormChanged;
            TextEdit_checknum.EditValueChanged += HandleFormChanged;
            TextEdit_max_amt_per_check.EditValueChanged += HandleFormChanged;
            SpinEdit_max_clients_per_check.EditValueChanged += HandleFormChanged;
            MemoExEdit_prefix_lines.EditValueChanged += HandleFormChanged;
            MemoExEdit_suffix_lines.EditValueChanged += HandleFormChanged;
            ButtonEdit_output_directory.EditValueChanged += HandleFormChanged;
        }

        /// <summary>
        /// Remove the event handler registrations
        /// </summary>
        private void UnRegisterHandlers()
        {
            SimpleButton_UpdateSignature.Click -= SimpleButton_UpdateSignature_Click;
            TextEdit_aba.EditValueChanged -= HandleFormChanged;
            TextEdit_account_number.EditValueChanged -= HandleFormChanged;
            TextEdit_checknum.EditValueChanged -= HandleFormChanged;
            TextEdit_max_amt_per_check.EditValueChanged -= HandleFormChanged;
            SpinEdit_max_clients_per_check.EditValueChanged -= HandleFormChanged;
            MemoExEdit_prefix_lines.EditValueChanged -= HandleFormChanged;
            MemoExEdit_suffix_lines.EditValueChanged -= HandleFormChanged;
            ButtonEdit_output_directory.EditValueChanged -= HandleFormChanged;
        }

        /// <summary>
        /// Reload the controls with the record contents
        /// </summary>
        internal override void RefreshForm()
        {
            UnRegisterHandlers();
            try
            {
                TextEdit_aba.EditValue = record.aba;
                TextEdit_account_number.EditValue = record.account_number;
                TextEdit_checknum.EditValue = record.checknum;
                TextEdit_max_amt_per_check.EditValue = record.max_amt_per_check;
                SpinEdit_max_clients_per_check.EditValue = record.max_clients_per_check;
                MemoExEdit_prefix_lines.EditValue = record.prefix_line;
                MemoExEdit_suffix_lines.EditValue = record.suffix_line;
                ButtonEdit_output_directory.EditValue = record.output_directory;
            }
            finally
            {
                base.RefreshForm();
                RegisterHandlers();
            }
        }

        /// <summary>
        /// Save the data from the input control to the record
        /// </summary>
        internal override void SaveForm(BusinessContext bc, bank record)
        {
            record.aba = DebtPlus.Utils.Nulls.v_String(TextEdit_aba.EditValue);
            record.account_number = DebtPlus.Utils.Nulls.v_String(TextEdit_account_number.EditValue);
            record.checknum = DebtPlus.Utils.Nulls.v_Int32(TextEdit_checknum.EditValue).GetValueOrDefault();
            record.max_amt_per_check = DebtPlus.Utils.Nulls.v_Decimal(TextEdit_max_amt_per_check.EditValue).GetValueOrDefault();
            record.max_clients_per_check = DebtPlus.Utils.Nulls.v_Int32(SpinEdit_max_clients_per_check.EditValue).GetValueOrDefault();
            record.prefix_line = DebtPlus.Utils.Nulls.v_String(MemoExEdit_prefix_lines.EditValue);
            record.suffix_line = DebtPlus.Utils.Nulls.v_String(MemoExEdit_suffix_lines.EditValue);
            record.output_directory = DebtPlus.Utils.Nulls.v_String(ButtonEdit_output_directory.EditValue);

            base.SaveForm(bc, record);
        }

        /// <summary>
        /// Process the CLICK event on the directory button
        /// </summary>
        private void OutputDirectory_ButtonPressed(object sender, ButtonPressedEventArgs e)
        {
            using (var dlg = new FolderBrowserDialog())
            {
                dlg.Description = "Output Directory";
                dlg.RootFolder = Environment.SpecialFolder.MyComputer;
                dlg.ShowNewFolderButton = true;
                if (dlg.ShowDialog() == DialogResult.OK)
                {
                    ButtonEdit_output_directory.Text = dlg.SelectedPath;
                }
            }
        }

        /// <summary>
        /// Process the update on the signature field
        /// </summary>
        private void SimpleButton_UpdateSignature_Click(object sender, EventArgs e)
        {
            using (var frm = new EditForm_Signature(bc, record))
            {
                frm.ShowDialog();
            }
        }
    }
}