
using System;
using System.Windows.Forms;
using System.Drawing;
using DebtPlus.LINQ;
using System.Linq;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Diagnostics;
namespace DebtPlus.UI.TableAdministration.CS.Bank.Controls
{
	partial class Banks_C : Banks_Common
	{

		//UserControl overrides dispose to clean up the component list.
		[System.Diagnostics.DebuggerNonUserCode]
		protected override void Dispose(bool disposing)
		{
			try 
{
				if (disposing && components != null) 
{
					components.Dispose();
				}
}

	 finally 
{
				base.Dispose(disposing);
			}
		}

		//Required by the Windows Form Designer

		private System.ComponentModel.IContainer components = null;
		//NOTE: The following procedure is required by the Windows Form Designer
		//It can be modified using the Windows Form Designer.  
		//Do not modify it using the code editor.
		[System.Diagnostics.DebuggerStepThrough]
		private void InitializeComponent()
		{
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Banks_C));
			this.GroupControl2 = new DevExpress.XtraEditors.GroupControl();
			this.TextEdit_account_number = new DevExpress.XtraEditors.TextEdit();
			this.TextEdit_aba = new DevExpress.XtraEditors.TextEdit();
			this.LabelControl2 = new DevExpress.XtraEditors.LabelControl();
			this.LabelControl1 = new DevExpress.XtraEditors.LabelControl();
			this.LabelControl9 = new DevExpress.XtraEditors.LabelControl();
			this.LabelControl15 = new DevExpress.XtraEditors.LabelControl();
			this.LabelControl16 = new DevExpress.XtraEditors.LabelControl();
			this.LabelControl17 = new DevExpress.XtraEditors.LabelControl();
			this.TextEdit_checknum = new DevExpress.XtraEditors.TextEdit();
			this.MemoExEdit_prefix_lines = new DevExpress.XtraEditors.MemoExEdit();
			this.MemoExEdit_suffix_lines = new DevExpress.XtraEditors.MemoExEdit();
			this.GroupControl1 = new DevExpress.XtraEditors.GroupControl();
			this.TextEdit_max_amt_per_check = new DevExpress.XtraEditors.TextEdit();
			this.SpinEdit_max_clients_per_check = new DevExpress.XtraEditors.SpinEdit();
			this.LabelControl3 = new DevExpress.XtraEditors.LabelControl();
			this.LabelControl10 = new DevExpress.XtraEditors.LabelControl();
			this.LabelControl4 = new DevExpress.XtraEditors.LabelControl();
			this.LabelControl_signature = new DevExpress.XtraEditors.LabelControl();
			this.SimpleButton_UpdateSignature = new DevExpress.XtraEditors.SimpleButton();
			this.ButtonEdit_output_directory = new DevExpress.XtraEditors.ButtonEdit();
			((System.ComponentModel.ISupportInitialize)this.GroupControl2).BeginInit();
			this.GroupControl2.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)this.TextEdit_account_number.Properties).BeginInit();
			((System.ComponentModel.ISupportInitialize)this.TextEdit_aba.Properties).BeginInit();
			((System.ComponentModel.ISupportInitialize)this.TextEdit_checknum.Properties).BeginInit();
			((System.ComponentModel.ISupportInitialize)this.MemoExEdit_prefix_lines.Properties).BeginInit();
			((System.ComponentModel.ISupportInitialize)this.MemoExEdit_suffix_lines.Properties).BeginInit();
			((System.ComponentModel.ISupportInitialize)this.GroupControl1).BeginInit();
			this.GroupControl1.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)this.TextEdit_max_amt_per_check.Properties).BeginInit();
			((System.ComponentModel.ISupportInitialize)this.SpinEdit_max_clients_per_check.Properties).BeginInit();
			((System.ComponentModel.ISupportInitialize)this.ButtonEdit_output_directory.Properties).BeginInit();
			this.SuspendLayout();
			//
			//GroupControl2
			//
			this.GroupControl2.Controls.Add(this.TextEdit_account_number);
			this.GroupControl2.Controls.Add(this.TextEdit_aba);
			this.GroupControl2.Controls.Add(this.LabelControl2);
			this.GroupControl2.Controls.Add(this.LabelControl1);
			this.GroupControl2.Location = new System.Drawing.Point(6, 3);
			this.GroupControl2.Name = "GroupControl2";
			this.GroupControl2.Size = new System.Drawing.Size(200, 80);
			this.GroupControl2.TabIndex = 0;
			this.GroupControl2.Text = "Account Information";
			//
			//TextEdit_account_number
			//
			this.TextEdit_account_number.Location = new System.Drawing.Point(95, 49);
			this.TextEdit_account_number.Name = "TextEdit_account_number";
			this.TextEdit_account_number.Properties.Appearance.Options.UseTextOptions = true;
			this.TextEdit_account_number.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
			this.TextEdit_account_number.Properties.AppearanceDisabled.Options.UseTextOptions = true;
			this.TextEdit_account_number.Properties.AppearanceDisabled.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
			this.TextEdit_account_number.Properties.AppearanceFocused.Options.UseTextOptions = true;
			this.TextEdit_account_number.Properties.AppearanceFocused.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
			this.TextEdit_account_number.Properties.AppearanceReadOnly.Options.UseTextOptions = true;
			this.TextEdit_account_number.Properties.AppearanceReadOnly.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
			this.TextEdit_account_number.Properties.Mask.BeepOnError = true;
			this.TextEdit_account_number.Properties.Mask.EditMask = "\\d+";
			this.TextEdit_account_number.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.RegEx;
			this.TextEdit_account_number.Properties.MaxLength = 40;
			this.TextEdit_account_number.Size = new System.Drawing.Size(100, 20);
			this.TextEdit_account_number.TabIndex = 3;
			//
			//TextEdit_aba
			//
			this.TextEdit_aba.Location = new System.Drawing.Point(95, 23);
			this.TextEdit_aba.Name = "TextEdit_aba";
			this.TextEdit_aba.Properties.Appearance.Options.UseTextOptions = true;
			this.TextEdit_aba.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
			this.TextEdit_aba.Properties.AppearanceDisabled.Options.UseTextOptions = true;
			this.TextEdit_aba.Properties.AppearanceDisabled.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
			this.TextEdit_aba.Properties.AppearanceFocused.Options.UseTextOptions = true;
			this.TextEdit_aba.Properties.AppearanceFocused.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
			this.TextEdit_aba.Properties.AppearanceReadOnly.Options.UseTextOptions = true;
			this.TextEdit_aba.Properties.AppearanceReadOnly.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
			this.TextEdit_aba.Properties.Mask.BeepOnError = true;
			this.TextEdit_aba.Properties.Mask.EditMask = "\\d{9}";
			this.TextEdit_aba.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Regular;
			this.TextEdit_aba.Properties.Mask.SaveLiteral = false;
			this.TextEdit_aba.Properties.MaxLength = 9;
			this.TextEdit_aba.Size = new System.Drawing.Size(100, 20);
			this.TextEdit_aba.TabIndex = 1;
			//
			//LabelControl2
			//
			this.LabelControl2.Location = new System.Drawing.Point(5, 52);
			this.LabelControl2.Name = "LabelControl2";
			this.LabelControl2.Size = new System.Drawing.Size(79, 13);
			this.LabelControl2.TabIndex = 2;
			this.LabelControl2.Text = "Account Number";
			//
			//LabelControl1
			//
			this.LabelControl1.Location = new System.Drawing.Point(5, 26);
			this.LabelControl1.Name = "LabelControl1";
			this.LabelControl1.Size = new System.Drawing.Size(60, 13);
			this.LabelControl1.TabIndex = 0;
			this.LabelControl1.Text = "ABA Number";
			//
			//LabelControl9
			//
			this.LabelControl9.Anchor = (AnchorStyles)(AnchorStyles.Top | AnchorStyles.Right);
			this.LabelControl9.Location = new System.Drawing.Point(293, 92);
			this.LabelControl9.Name = "LabelControl9";
			this.LabelControl9.Size = new System.Drawing.Size(69, 13);
			this.LabelControl9.TabIndex = 2;
			this.LabelControl9.Text = "Check Number";
			//
			//LabelControl15
			//
			this.LabelControl15.Anchor = (AnchorStyles)(AnchorStyles.Bottom | AnchorStyles.Left);
			this.LabelControl15.Location = new System.Drawing.Point(11, 233);
			this.LabelControl15.Name = "LabelControl15";
			this.LabelControl15.Size = new System.Drawing.Size(60, 13);
			this.LabelControl15.TabIndex = 6;
			this.LabelControl15.Text = "Prefix line(s)";
			//
			//LabelControl16
			//
			this.LabelControl16.Anchor = (AnchorStyles)(AnchorStyles.Bottom | AnchorStyles.Left);
			this.LabelControl16.Location = new System.Drawing.Point(11, 259);
			this.LabelControl16.Name = "LabelControl16";
			this.LabelControl16.Size = new System.Drawing.Size(60, 13);
			this.LabelControl16.TabIndex = 8;
			this.LabelControl16.Text = "Suffix line(s)";
			//
			//LabelControl17
			//
			this.LabelControl17.Anchor = (AnchorStyles)(AnchorStyles.Bottom | AnchorStyles.Left);
			this.LabelControl17.Location = new System.Drawing.Point(11, 285);
			this.LabelControl17.Name = "LabelControl17";
			this.LabelControl17.Size = new System.Drawing.Size(106, 13);
			this.LabelControl17.TabIndex = 10;
			this.LabelControl17.Text = "Output Directory Path";
			//
			//TextEdit_checknum
			//
			this.TextEdit_checknum.Anchor = (AnchorStyles)(AnchorStyles.Top | AnchorStyles.Right);
			this.TextEdit_checknum.Location = new System.Drawing.Point(378, 89);
			this.TextEdit_checknum.Name = "TextEdit_checknum";
			this.TextEdit_checknum.Properties.Appearance.Options.UseTextOptions = true;
			this.TextEdit_checknum.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
			this.TextEdit_checknum.Properties.AppearanceDisabled.Options.UseTextOptions = true;
			this.TextEdit_checknum.Properties.AppearanceDisabled.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
			this.TextEdit_checknum.Properties.AppearanceFocused.Options.UseTextOptions = true;
			this.TextEdit_checknum.Properties.AppearanceFocused.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
			this.TextEdit_checknum.Properties.AppearanceReadOnly.Options.UseTextOptions = true;
			this.TextEdit_checknum.Properties.AppearanceReadOnly.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
			this.TextEdit_checknum.Properties.Mask.BeepOnError = true;
			this.TextEdit_checknum.Properties.Mask.EditMask = "\\d+";
			this.TextEdit_checknum.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.RegEx;
			this.TextEdit_checknum.Size = new System.Drawing.Size(100, 20);
			this.TextEdit_checknum.TabIndex = 3;
			//
			//MemoExEdit_prefix_lines
			//
			this.MemoExEdit_prefix_lines.Anchor = (AnchorStyles)((AnchorStyles.Bottom | AnchorStyles.Left) | AnchorStyles.Right);
			this.MemoExEdit_prefix_lines.Location = new System.Drawing.Point(123, 230);
			this.MemoExEdit_prefix_lines.Name = "MemoExEdit_prefix_lines";
			this.MemoExEdit_prefix_lines.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.True;
			this.MemoExEdit_prefix_lines.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] { new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo) });
			this.MemoExEdit_prefix_lines.Properties.MaxLength = 256;
			this.MemoExEdit_prefix_lines.Properties.PopupFormMinSize = new System.Drawing.Size(360, 0);
			this.MemoExEdit_prefix_lines.Properties.ScrollBars = ScrollBars.Vertical;
			this.MemoExEdit_prefix_lines.Properties.ShowIcon = false;
			this.MemoExEdit_prefix_lines.Properties.WordWrap = false;
			this.MemoExEdit_prefix_lines.Size = new System.Drawing.Size(360, 20);
			this.MemoExEdit_prefix_lines.TabIndex = 7;
			//
			//MemoExEdit_suffix_lines
			//
			this.MemoExEdit_suffix_lines.Anchor = (AnchorStyles)((AnchorStyles.Bottom | AnchorStyles.Left) | AnchorStyles.Right);
			this.MemoExEdit_suffix_lines.Location = new System.Drawing.Point(123, 256);
			this.MemoExEdit_suffix_lines.Name = "MemoExEdit_suffix_lines";
			this.MemoExEdit_suffix_lines.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.True;
			this.MemoExEdit_suffix_lines.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] { new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo) });
			this.MemoExEdit_suffix_lines.Properties.MaxLength = 256;
			this.MemoExEdit_suffix_lines.Properties.PopupFormMinSize = new System.Drawing.Size(360, 0);
			this.MemoExEdit_suffix_lines.Properties.ScrollBars = ScrollBars.Vertical;
			this.MemoExEdit_suffix_lines.Properties.ShowIcon = false;
			this.MemoExEdit_suffix_lines.Properties.WordWrap = false;
			this.MemoExEdit_suffix_lines.Size = new System.Drawing.Size(360, 20);
			this.MemoExEdit_suffix_lines.TabIndex = 9;
			//
			//GroupControl1
			//
			this.GroupControl1.Anchor = (AnchorStyles)(AnchorStyles.Top | AnchorStyles.Right);
			this.GroupControl1.Controls.Add(this.TextEdit_max_amt_per_check);
			this.GroupControl1.Controls.Add(this.SpinEdit_max_clients_per_check);
			this.GroupControl1.Controls.Add(this.LabelControl3);
			this.GroupControl1.Controls.Add(this.LabelControl10);
			this.GroupControl1.Location = new System.Drawing.Point(283, 4);
			this.GroupControl1.Name = "GroupControl1";
			this.GroupControl1.Size = new System.Drawing.Size(200, 79);
			this.GroupControl1.TabIndex = 1;
			this.GroupControl1.Text = "Check Limits";
			//
			//TextEdit_max_amt_per_check
			//
			this.TextEdit_max_amt_per_check.Location = new System.Drawing.Point(95, 49);
			this.TextEdit_max_amt_per_check.Name = "TextEdit_max_amt_per_check";
			this.TextEdit_max_amt_per_check.Properties.Appearance.Options.UseTextOptions = true;
			this.TextEdit_max_amt_per_check.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
			this.TextEdit_max_amt_per_check.Properties.AppearanceDisabled.Options.UseTextOptions = true;
			this.TextEdit_max_amt_per_check.Properties.AppearanceDisabled.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
			this.TextEdit_max_amt_per_check.Properties.AppearanceFocused.Options.UseTextOptions = true;
			this.TextEdit_max_amt_per_check.Properties.AppearanceFocused.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
			this.TextEdit_max_amt_per_check.Properties.AppearanceReadOnly.Options.UseTextOptions = true;
			this.TextEdit_max_amt_per_check.Properties.AppearanceReadOnly.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
			this.TextEdit_max_amt_per_check.Properties.DisplayFormat.FormatString = "{0:c2}";
			this.TextEdit_max_amt_per_check.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
			this.TextEdit_max_amt_per_check.Properties.EditFormat.FormatString = "{0:f2}";
			this.TextEdit_max_amt_per_check.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
			this.TextEdit_max_amt_per_check.Properties.Mask.BeepOnError = true;
			this.TextEdit_max_amt_per_check.Properties.Mask.EditMask = "c";
			this.TextEdit_max_amt_per_check.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
			this.TextEdit_max_amt_per_check.Size = new System.Drawing.Size(100, 20);
			this.TextEdit_max_amt_per_check.TabIndex = 3;
			//
			//SpinEdit_max_clients_per_check
			//
			this.SpinEdit_max_clients_per_check.EditValue = new decimal(new int[] {
				0,
				0,
				0,
				0
			});
			this.SpinEdit_max_clients_per_check.Location = new System.Drawing.Point(138, 23);
			this.SpinEdit_max_clients_per_check.Name = "SpinEdit_max_clients_per_check";
			this.SpinEdit_max_clients_per_check.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] { new DevExpress.XtraEditors.Controls.EditorButton() });
			this.SpinEdit_max_clients_per_check.Properties.IsFloatValue = false;
			this.SpinEdit_max_clients_per_check.Properties.Mask.EditMask = "N00";
			this.SpinEdit_max_clients_per_check.Properties.MaxValue = new decimal(new int[] {
				80,
				0,
				0,
				0
			});
			this.SpinEdit_max_clients_per_check.Size = new System.Drawing.Size(57, 20);
			this.SpinEdit_max_clients_per_check.TabIndex = 1;
			//
			//LabelControl3
			//
			this.LabelControl3.Location = new System.Drawing.Point(10, 52);
			this.LabelControl3.Name = "LabelControl3";
			this.LabelControl3.Size = new System.Drawing.Size(60, 13);
			this.LabelControl3.TabIndex = 2;
			this.LabelControl3.Text = "Max Amount";
			//
			//LabelControl10
			//
			this.LabelControl10.Location = new System.Drawing.Point(10, 26);
			this.LabelControl10.Name = "LabelControl10";
			this.LabelControl10.Size = new System.Drawing.Size(55, 13);
			this.LabelControl10.TabIndex = 0;
			this.LabelControl10.Text = "Max Clients";
			//
			//LabelControl4
			//
			this.LabelControl4.Location = new System.Drawing.Point(11, 125);
			this.LabelControl4.Name = "LabelControl4";
			this.LabelControl4.Size = new System.Drawing.Size(31, 13);
			this.LabelControl4.TabIndex = 4;
			this.LabelControl4.Text = "NOTE:";
			//
			//LabelControl_signature
			//
			this.LabelControl_signature.Anchor = (AnchorStyles)(((AnchorStyles.Top | AnchorStyles.Bottom) | AnchorStyles.Left) | AnchorStyles.Right);
			this.LabelControl_signature.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
			this.LabelControl_signature.Appearance.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Top;
			this.LabelControl_signature.Appearance.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
			this.LabelControl_signature.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
			this.LabelControl_signature.Location = new System.Drawing.Point(48, 125);
			this.LabelControl_signature.Name = "LabelControl_signature";
			this.LabelControl_signature.Size = new System.Drawing.Size(435, 70);
			this.LabelControl_signature.TabIndex = 5;
			this.LabelControl_signature.Text = resources.GetString("LabelControl_signature.Text");
			//
			//SimpleButton_UpdateSignature
			//
			this.SimpleButton_UpdateSignature.Anchor = (AnchorStyles)(AnchorStyles.Bottom | AnchorStyles.Right);
			this.SimpleButton_UpdateSignature.Location = new System.Drawing.Point(305, 201);
			this.SimpleButton_UpdateSignature.Name = "SimpleButton_UpdateSignature";
			this.SimpleButton_UpdateSignature.Size = new System.Drawing.Size(178, 23);
			this.SimpleButton_UpdateSignature.TabIndex = 12;
			this.SimpleButton_UpdateSignature.Text = "Update Check Signature...";
			//
			//ButtonEdit_output_directory
			//
			this.ButtonEdit_output_directory.Anchor = (AnchorStyles)((AnchorStyles.Bottom | AnchorStyles.Left) | AnchorStyles.Right);
			this.ButtonEdit_output_directory.Location = new System.Drawing.Point(123, 282);
			this.ButtonEdit_output_directory.Name = "ButtonEdit_output_directory";
			this.ButtonEdit_output_directory.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] { new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Right) });
			this.ButtonEdit_output_directory.Properties.MaxLength = 256;
			this.ButtonEdit_output_directory.Size = new System.Drawing.Size(360, 20);
			this.ButtonEdit_output_directory.TabIndex = 51;
			//
			//Banks_C
			//
			this.Appearance.BackColor = System.Drawing.Color.Transparent;
			this.Appearance.Options.UseBackColor = true;
			this.AutoScaleDimensions = new System.Drawing.SizeF(6f, 13f);
			this.AutoScaleMode = AutoScaleMode.Font;
			this.Controls.Add(this.LabelControl_signature);
			this.Controls.Add(this.ButtonEdit_output_directory);
			this.Controls.Add(this.SimpleButton_UpdateSignature);
			this.Controls.Add(this.LabelControl4);
			this.Controls.Add(this.GroupControl1);
			this.Controls.Add(this.MemoExEdit_suffix_lines);
			this.Controls.Add(this.MemoExEdit_prefix_lines);
			this.Controls.Add(this.TextEdit_checknum);
			this.Controls.Add(this.LabelControl17);
			this.Controls.Add(this.LabelControl16);
			this.Controls.Add(this.LabelControl15);
			this.Controls.Add(this.LabelControl9);
			this.Controls.Add(this.GroupControl2);
			this.Name = "Banks_C";
			((System.ComponentModel.ISupportInitialize)this.GroupControl2).EndInit();
			this.GroupControl2.ResumeLayout(false);
			this.GroupControl2.PerformLayout();
			((System.ComponentModel.ISupportInitialize)this.TextEdit_account_number.Properties).EndInit();
			((System.ComponentModel.ISupportInitialize)this.TextEdit_aba.Properties).EndInit();
			((System.ComponentModel.ISupportInitialize)this.TextEdit_checknum.Properties).EndInit();
			((System.ComponentModel.ISupportInitialize)this.MemoExEdit_prefix_lines.Properties).EndInit();
			((System.ComponentModel.ISupportInitialize)this.MemoExEdit_suffix_lines.Properties).EndInit();
			((System.ComponentModel.ISupportInitialize)this.GroupControl1).EndInit();
			this.GroupControl1.ResumeLayout(false);
			this.GroupControl1.PerformLayout();
			((System.ComponentModel.ISupportInitialize)this.TextEdit_max_amt_per_check.Properties).EndInit();
			((System.ComponentModel.ISupportInitialize)this.SpinEdit_max_clients_per_check.Properties).EndInit();
			((System.ComponentModel.ISupportInitialize)this.ButtonEdit_output_directory.Properties).EndInit();
			this.ResumeLayout(false);
			this.PerformLayout();

		}
		private DevExpress.XtraEditors.GroupControl GroupControl2;
		private DevExpress.XtraEditors.LabelControl LabelControl2;
		private DevExpress.XtraEditors.LabelControl LabelControl1;
		private DevExpress.XtraEditors.LabelControl LabelControl9;
		private DevExpress.XtraEditors.LabelControl LabelControl15;
		private DevExpress.XtraEditors.TextEdit TextEdit_account_number;
		private DevExpress.XtraEditors.TextEdit TextEdit_aba;
		private DevExpress.XtraEditors.LabelControl LabelControl16;
		private DevExpress.XtraEditors.LabelControl LabelControl17;
		private DevExpress.XtraEditors.TextEdit TextEdit_checknum;
		private DevExpress.XtraEditors.MemoExEdit MemoExEdit_prefix_lines;
		private DevExpress.XtraEditors.MemoExEdit MemoExEdit_suffix_lines;
		private DevExpress.XtraEditors.GroupControl GroupControl1;
		private DevExpress.XtraEditors.TextEdit TextEdit_max_amt_per_check;
		private DevExpress.XtraEditors.SpinEdit SpinEdit_max_clients_per_check;
		private DevExpress.XtraEditors.LabelControl LabelControl3;
		private DevExpress.XtraEditors.LabelControl LabelControl10;
		private DevExpress.XtraEditors.LabelControl LabelControl4;
		private DevExpress.XtraEditors.LabelControl LabelControl_signature;
		private DevExpress.XtraEditors.SimpleButton SimpleButton_UpdateSignature;

		private DevExpress.XtraEditors.ButtonEdit ButtonEdit_output_directory;
	}
}

//=======================================================
//Service provided by Telerik (www.telerik.com)
//Conversion powered by NRefactory.
//Twitter: @telerik
//Facebook: facebook.com/telerik
//=======================================================
