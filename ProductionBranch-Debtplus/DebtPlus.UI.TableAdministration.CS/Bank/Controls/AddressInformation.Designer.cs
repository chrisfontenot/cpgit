using System;
using System.Windows.Forms;
using System.Drawing;
using DebtPlus.LINQ;
using System.Linq;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Diagnostics;

namespace DebtPlus.UI.TableAdministration.CS.Bank.Controls
{
	partial class AddressInformation : DevExpress.XtraEditors.XtraUserControl
	{

		//UserControl overrides dispose to clean up the component list.
		[System.Diagnostics.DebuggerNonUserCode]
		protected override void Dispose(bool disposing)
		{
			try 
{
				if (disposing && components != null) 
{
					components.Dispose();
				}
}

	 finally 
{
				base.Dispose(disposing);
			}
		}

		//Required by the Windows Form Designer

		private System.ComponentModel.IContainer components = null;
		//NOTE: The following procedure is required by the Windows Form Designer
		//It can be modified using the Windows Form Designer.  
		//Do not modify it using the code editor.
		[System.Diagnostics.DebuggerStepThrough]
		private void InitializeComponent()
		{
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(AddressInformation));
			this.TextEdit_name = new DevExpress.XtraEditors.TextEdit();
			this.LabelControl1 = new DevExpress.XtraEditors.LabelControl();
			this.LabelControl2 = new DevExpress.XtraEditors.LabelControl();
			this.LabelControl3 = new DevExpress.XtraEditors.LabelControl();
			this.LabelControl4 = new DevExpress.XtraEditors.LabelControl();
			this.LabelControl5 = new DevExpress.XtraEditors.LabelControl();
			this.TextEdit_aba = new DevExpress.XtraEditors.TextEdit();
			this.TextEdit_account_number = new DevExpress.XtraEditors.TextEdit();
			this.AddressRecordControl1 = new global::DebtPlus.Data.Controls.AddressRecordControl();
			this.NameRecordControl1 = new global::DebtPlus.Data.Controls.NameRecordControl();
			((System.ComponentModel.ISupportInitialize)this.TextEdit_name.Properties).BeginInit();
			((System.ComponentModel.ISupportInitialize)this.TextEdit_aba.Properties).BeginInit();
			((System.ComponentModel.ISupportInitialize)this.TextEdit_account_number.Properties).BeginInit();
			this.SuspendLayout();
			//
			//TextEdit_name
			//
			this.TextEdit_name.Anchor = (AnchorStyles)((AnchorStyles.Top | AnchorStyles.Left) | AnchorStyles.Right);
			this.TextEdit_name.Location = new System.Drawing.Point(78, 35);
			this.TextEdit_name.Name = "TextEdit_name";
			this.TextEdit_name.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.True;
			this.TextEdit_name.Properties.MaxLength = 80;
			this.TextEdit_name.Size = new System.Drawing.Size(402, 20);
			this.TextEdit_name.TabIndex = 3;
			//
			//LabelControl1
			//
			this.LabelControl1.Location = new System.Drawing.Point(4, 38);
			this.LabelControl1.Name = "LabelControl1";
			this.LabelControl1.Size = new System.Drawing.Size(53, 13);
			this.LabelControl1.TabIndex = 2;
			this.LabelControl1.Text = "Bank Name";
			//
			//LabelControl2
			//
			this.LabelControl2.Location = new System.Drawing.Point(3, 65);
			this.LabelControl2.Name = "LabelControl2";
			this.LabelControl2.Size = new System.Drawing.Size(65, 13);
			this.LabelControl2.TabIndex = 4;
			this.LabelControl2.Text = "Bank Address";
			//
			//LabelControl3
			//
			this.LabelControl3.Location = new System.Drawing.Point(4, 141);
			this.LabelControl3.Name = "LabelControl3";
			this.LabelControl3.Size = new System.Drawing.Size(60, 13);
			this.LabelControl3.TabIndex = 6;
			this.LabelControl3.Text = "ABA Number";
			//
			//LabelControl4
			//
			this.LabelControl4.Anchor = (AnchorStyles)(AnchorStyles.Top | AnchorStyles.Right);
			this.LabelControl4.Location = new System.Drawing.Point(295, 142);
			this.LabelControl4.Name = "LabelControl4";
			this.LabelControl4.Size = new System.Drawing.Size(79, 13);
			this.LabelControl4.TabIndex = 8;
			this.LabelControl4.Text = "Account Number";
			//
			//LabelControl5
			//
			this.LabelControl5.Location = new System.Drawing.Point(4, 12);
			this.LabelControl5.Name = "LabelControl5";
			this.LabelControl5.Size = new System.Drawing.Size(68, 13);
			this.LabelControl5.TabIndex = 0;
			this.LabelControl5.Text = "Contact Name";
			//
			//TextEdit_aba
			//
			this.TextEdit_aba.Location = new System.Drawing.Point(78, 138);
			this.TextEdit_aba.Name = "TextEdit_aba";
			this.TextEdit_aba.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.True;
			this.TextEdit_aba.Properties.Appearance.Options.UseTextOptions = true;
			this.TextEdit_aba.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
			this.TextEdit_aba.Properties.AppearanceDisabled.Options.UseTextOptions = true;
			this.TextEdit_aba.Properties.AppearanceDisabled.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
			this.TextEdit_aba.Properties.AppearanceFocused.Options.UseTextOptions = true;
			this.TextEdit_aba.Properties.AppearanceFocused.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
			this.TextEdit_aba.Properties.AppearanceReadOnly.Options.UseTextOptions = true;
			this.TextEdit_aba.Properties.AppearanceReadOnly.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
			this.TextEdit_aba.Properties.Mask.BeepOnError = true;
			this.TextEdit_aba.Properties.Mask.EditMask = "\\d{9}";
			this.TextEdit_aba.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Regular;
			this.TextEdit_aba.Properties.Mask.SaveLiteral = false;
			this.TextEdit_aba.Properties.MaxLength = 9;
			this.TextEdit_aba.Size = new System.Drawing.Size(100, 20);
			this.TextEdit_aba.TabIndex = 7;
			//
			//TextEdit_account_number
			//
			this.TextEdit_account_number.Anchor = (AnchorStyles)(AnchorStyles.Top | AnchorStyles.Right);
			this.TextEdit_account_number.Location = new System.Drawing.Point(380, 138);
			this.TextEdit_account_number.Name = "TextEdit_account_number";
			this.TextEdit_account_number.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.True;
			this.TextEdit_account_number.Properties.Appearance.Options.UseTextOptions = true;
			this.TextEdit_account_number.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
			this.TextEdit_account_number.Properties.AppearanceDisabled.Options.UseTextOptions = true;
			this.TextEdit_account_number.Properties.AppearanceDisabled.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
			this.TextEdit_account_number.Properties.AppearanceFocused.Options.UseTextOptions = true;
			this.TextEdit_account_number.Properties.AppearanceFocused.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
			this.TextEdit_account_number.Properties.AppearanceReadOnly.Options.UseTextOptions = true;
			this.TextEdit_account_number.Properties.AppearanceReadOnly.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
			this.TextEdit_account_number.Properties.Mask.BeepOnError = true;
			this.TextEdit_account_number.Properties.Mask.EditMask = "\\d+";
			this.TextEdit_account_number.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.RegEx;
			this.TextEdit_account_number.Properties.MaxLength = 40;
			this.TextEdit_account_number.Size = new System.Drawing.Size(100, 20);
			this.TextEdit_account_number.TabIndex = 9;
			//
			//AddressRecordControl1
			//
			this.AddressRecordControl1.Anchor = (AnchorStyles)((AnchorStyles.Top | AnchorStyles.Left) | AnchorStyles.Right);
			this.AddressRecordControl1.Location = new System.Drawing.Point(78, 61);
			this.AddressRecordControl1.Name = "AddressRecordControl1";
			this.AddressRecordControl1.Size = new System.Drawing.Size(402, 75);
			this.AddressRecordControl1.TabIndex = 5;
			//
			//NameRecordControl1
			//
			this.NameRecordControl1.Anchor = (AnchorStyles)((AnchorStyles.Top | AnchorStyles.Left) | AnchorStyles.Right);
			this.NameRecordControl1.Location = new System.Drawing.Point(78, 9);
			this.NameRecordControl1.Name = "NameRecordControl1";
			this.NameRecordControl1.Size = new System.Drawing.Size(402, 20);
			this.NameRecordControl1.TabIndex = 1;
			//
			//AddressInformation
			//
			this.AutoScaleDimensions = new System.Drawing.SizeF(6f, 13f);
			this.AutoScaleMode = AutoScaleMode.Font;
			this.Controls.Add(this.NameRecordControl1);
			this.Controls.Add(this.AddressRecordControl1);
			this.Controls.Add(this.TextEdit_account_number);
			this.Controls.Add(this.TextEdit_aba);
			this.Controls.Add(this.LabelControl5);
			this.Controls.Add(this.LabelControl4);
			this.Controls.Add(this.LabelControl3);
			this.Controls.Add(this.LabelControl2);
			this.Controls.Add(this.LabelControl1);
			this.Controls.Add(this.TextEdit_name);
			this.MinimumSize = new System.Drawing.Size(484, 162);
			this.Name = "AddressInformation";
			this.Size = new System.Drawing.Size(484, 162);
			((System.ComponentModel.ISupportInitialize)this.TextEdit_name.Properties).EndInit();
			((System.ComponentModel.ISupportInitialize)this.TextEdit_aba.Properties).EndInit();
			((System.ComponentModel.ISupportInitialize)this.TextEdit_account_number.Properties).EndInit();
			this.ResumeLayout(false);
			this.PerformLayout();
		}

		private DevExpress.XtraEditors.TextEdit TextEdit_name;
		private DevExpress.XtraEditors.LabelControl LabelControl1;
		private DevExpress.XtraEditors.LabelControl LabelControl2;
		private DevExpress.XtraEditors.LabelControl LabelControl3;
		private DevExpress.XtraEditors.LabelControl LabelControl4;
		private DevExpress.XtraEditors.LabelControl LabelControl5;
		private DevExpress.XtraEditors.TextEdit TextEdit_aba;
		private DevExpress.XtraEditors.TextEdit TextEdit_account_number;
		private global::DebtPlus.Data.Controls.AddressRecordControl AddressRecordControl1;
		private global::DebtPlus.Data.Controls.NameRecordControl NameRecordControl1;
	}
}
