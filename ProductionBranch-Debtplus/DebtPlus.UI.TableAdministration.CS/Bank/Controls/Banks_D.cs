using DebtPlus.LINQ;

namespace DebtPlus.UI.TableAdministration.CS.Bank.Controls
{
    internal partial class Banks_D : Banks_Common
    {
        private bank record = null;
        private BusinessContext bc = null;

        /// <summary>
        /// Initialize the new class instance
        /// </summary>
        internal Banks_D()
            : base()
        {
            InitializeComponent();
        }

        /// <summary>
        /// Do any one-time initialization for the data.
        /// </summary>
        internal override void ReadForm(BusinessContext bc, bank record)
        {
            base.ReadForm(bc, record);
            this.record = record;
            this.bc = bc;
        }

        /// <summary>
        /// Register the event handlers
        /// </summary>
        private void RegisterHandlers()
        {
        }

        /// <summary>
        /// Reload the controls with the record contents
        /// </summary>
        internal override void RefreshForm()
        {
            AddressInformation1.ReadForm(bc, record);
            base.RefreshForm();
        }

        /// <summary>
        /// Save the data from the input control to the record
        /// </summary>
        internal override void SaveForm(BusinessContext bc, bank record)
        {
            AddressInformation1.SaveForm(bc, record);
            base.SaveForm(bc, record);
        }
    }
}