namespace DebtPlus.UI.TableAdministration.CS.Bank.Controls
{
	partial class Banks_D : Banks_Common
	{

		//UserControl overrides dispose to clean up the component list.
		[System.Diagnostics.DebuggerNonUserCode]
		protected override void Dispose(bool disposing)
		{
			try 
            {
				if (disposing && components != null) 
                {
					components.Dispose();
				}
            }
            finally 
            {
				base.Dispose(disposing);
			}
		}

		//Required by the Windows Form Designer
		private System.ComponentModel.IContainer components = null;

		//NOTE: The following procedure is required by the Windows Form Designer
		//It can be modified using the Windows Form Designer.  
		//Do not modify it using the code editor.
		[System.Diagnostics.DebuggerStepThrough]
		private void InitializeComponent()
		{
			this.AddressInformation1 = new AddressInformation();
			this.SuspendLayout();
			//
			//AddressInformation1
			//
			this.AddressInformation1.Location = new System.Drawing.Point(3, 76);
			this.AddressInformation1.MinimumSize = new System.Drawing.Size(484, 162);
			this.AddressInformation1.Name = "AddressInformation1";
			this.AddressInformation1.Size = new System.Drawing.Size(484, 162);
			this.AddressInformation1.TabIndex = 0;
			//
			//Banks_D
			//
			this.Appearance.BackColor = System.Drawing.Color.Transparent;
			this.Appearance.Options.UseBackColor = true;
			this.AutoScaleDimensions = new System.Drawing.SizeF(6f, 13f);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.Controls.Add(this.AddressInformation1);
			this.Name = "Banks_D";
			this.Size = new System.Drawing.Size(484, 314);
			this.ResumeLayout(false);
		}
		private AddressInformation AddressInformation1;
	}
}
