#region "Copyright 2000-2012 DebtPlus, L.L.C."
//{*******************************************************************}
//{                                                                   }
//{       DebtPlus Debt Management System                             }
//{                                                                   }
//{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
//{       ALL RIGHTS RESERVED                                         }
//{                                                                   }
//{   The entire contents of this file is protected by U.S. and       }
//{   International Copyright Laws. Unauthorized reproduction,        }
//{   reverse-engineering, and distribution of all or any portion of  }
//{   the code contained in this file is strictly prohibited and may  }
//{   result in severe civil and criminal penalties and will be       }
//{   prosecuted to the maximum extent possible under the law.        }
//{                                                                   }
//{   RESTRICTIONS                                                    }
//{                                                                   }
//{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
//{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
//{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
//{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
//{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
//{                                                                   }
//{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
//{   ADDITIONAL RESTRICTIONS.                                        }
//{                                                                   }
//{*******************************************************************}

#endregion "Copyright 2000-2012 DebtPlus, L.L.C."

using DebtPlus.LINQ;

namespace DebtPlus.UI.TableAdministration.CS.Bank.Controls
{
    internal partial class Banks_Common
    {
        internal Banks_Common()
            : base()
        {
            InitializeComponent();
        }

        /// <summary>
        /// Event raised when the controls are changed on the form
        /// </summary>
        public event System.EventHandler FormChanged;

        /// <summary>
        /// Allow the inherited form to raise the FormChanged event
        /// </summary>
        protected void RaiseFormChanged(System.EventArgs e)
        {
            var evt = FormChanged;
            if (evt != null)
            {
                evt(this, e);
            }
        }

        /// <summary>
        /// Handle the changes in the form events
        /// </summary>
        protected void HandleFormChanged(object sender, System.EventArgs e)
        {
            RaiseFormChanged(e);
        }

        /// <summary>
        /// Does the form have any error conditions
        /// </summary>
        /// <returns></returns>
        internal virtual bool HasErrors()
        {
            return false;
        }

        /// <summary>
        /// Do any one-time processing during the "load" sequence for the form
        /// </summary>
        internal virtual void ReadForm(BusinessContext bc, bank record)
        {
        }

        /// <summary>
        /// Save the controls into the bank record during the save processing
        /// </summary>
        internal virtual void SaveForm(BusinessContext bc, bank record)
        {
        }

        /// <summary>
        /// Load the controls with the record contents when the form is being selected.
        /// </summary>
        internal virtual void RefreshForm()
        {
        }
    }
}