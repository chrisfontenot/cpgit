
using System;
using System.Windows.Forms;
using System.Drawing;
using DebtPlus.LINQ;
using System.Linq;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Diagnostics;
namespace DebtPlus.UI.TableAdministration.CS.Bank.Controls
{
	partial class Banks_R : Banks_Common
	{

		//UserControl overrides dispose to clean up the component list.
		[System.Diagnostics.DebuggerNonUserCode]
		protected override void Dispose(bool disposing)
		{
			try 
{
				if (disposing && components != null) 
{
					components.Dispose();
				}
}

	 finally 
{
				base.Dispose(disposing);
			}
		}

		//Required by the Windows Form Designer

		private System.ComponentModel.IContainer components = null;
		//NOTE: The following procedure is required by the Windows Form Designer
		//It can be modified using the Windows Form Designer.  
		//Do not modify it using the code editor.
		[System.Diagnostics.DebuggerStepThrough]
		private void InitializeComponent()
		{
			this.TextEdit_transaction_number = new DevExpress.XtraEditors.TextEdit();
			this.TextEdit_batch_number = new DevExpress.XtraEditors.TextEdit();
			this.MemoExEdit_suffix_lines = new DevExpress.XtraEditors.MemoExEdit();
			this.MemoExEdit_prefix_lines = new DevExpress.XtraEditors.MemoExEdit();
			this.GroupControl3 = new DevExpress.XtraEditors.GroupControl();
			this.TextEdit_immediate_destination_name = new DevExpress.XtraEditors.TextEdit();
			this.TextEdit_immediate_destination = new DevExpress.XtraEditors.TextEdit();
			this.LabelControl11 = new DevExpress.XtraEditors.LabelControl();
			this.LabelControl12 = new DevExpress.XtraEditors.LabelControl();
			this.LabelControl16 = new DevExpress.XtraEditors.LabelControl();
			this.LabelControl15 = new DevExpress.XtraEditors.LabelControl();
			this.LabelControl14 = new DevExpress.XtraEditors.LabelControl();
			this.LabelControl13 = new DevExpress.XtraEditors.LabelControl();
			this.GroupControl1 = new DevExpress.XtraEditors.GroupControl();
			this.TextEdit_immediate_origin_name = new DevExpress.XtraEditors.TextEdit();
			this.TextEdit_immediate_origin = new DevExpress.XtraEditors.TextEdit();
			this.LabelControl4 = new DevExpress.XtraEditors.LabelControl();
			this.LabelControl3 = new DevExpress.XtraEditors.LabelControl();
			this.LabelControl17 = new DevExpress.XtraEditors.LabelControl();
			this.ButtonEdit_output_directory = new DevExpress.XtraEditors.ButtonEdit();
			((System.ComponentModel.ISupportInitialize)this.TextEdit_transaction_number.Properties).BeginInit();
			((System.ComponentModel.ISupportInitialize)this.TextEdit_batch_number.Properties).BeginInit();
			((System.ComponentModel.ISupportInitialize)this.MemoExEdit_suffix_lines.Properties).BeginInit();
			((System.ComponentModel.ISupportInitialize)this.MemoExEdit_prefix_lines.Properties).BeginInit();
			((System.ComponentModel.ISupportInitialize)this.GroupControl3).BeginInit();
			this.GroupControl3.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)this.TextEdit_immediate_destination_name.Properties).BeginInit();
			((System.ComponentModel.ISupportInitialize)this.TextEdit_immediate_destination.Properties).BeginInit();
			((System.ComponentModel.ISupportInitialize)this.GroupControl1).BeginInit();
			this.GroupControl1.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)this.TextEdit_immediate_origin_name.Properties).BeginInit();
			((System.ComponentModel.ISupportInitialize)this.TextEdit_immediate_origin.Properties).BeginInit();
			((System.ComponentModel.ISupportInitialize)this.ButtonEdit_output_directory.Properties).BeginInit();
			this.SuspendLayout();
			//
			//TextEdit_transaction_number
			//
			this.TextEdit_transaction_number.Location = new System.Drawing.Point(381, 254);
			this.TextEdit_transaction_number.Name = "TextEdit_transaction_number";
			this.TextEdit_transaction_number.Properties.Appearance.Options.UseTextOptions = true;
			this.TextEdit_transaction_number.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
			this.TextEdit_transaction_number.Properties.AppearanceDisabled.Options.UseTextOptions = true;
			this.TextEdit_transaction_number.Properties.AppearanceDisabled.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
			this.TextEdit_transaction_number.Properties.AppearanceFocused.Options.UseTextOptions = true;
			this.TextEdit_transaction_number.Properties.AppearanceFocused.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
			this.TextEdit_transaction_number.Properties.AppearanceReadOnly.Options.UseTextOptions = true;
			this.TextEdit_transaction_number.Properties.AppearanceReadOnly.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
			this.TextEdit_transaction_number.Properties.DisplayFormat.FormatString = "f0";
			this.TextEdit_transaction_number.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
			this.TextEdit_transaction_number.Properties.EditFormat.FormatString = "f0";
			this.TextEdit_transaction_number.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
			this.TextEdit_transaction_number.Properties.Mask.BeepOnError = true;
			this.TextEdit_transaction_number.Properties.Mask.EditMask = "f0";
			this.TextEdit_transaction_number.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
			this.TextEdit_transaction_number.Size = new System.Drawing.Size(100, 20);
			this.TextEdit_transaction_number.TabIndex = 34;
			//
			//TextEdit_batch_number
			//
			this.TextEdit_batch_number.Location = new System.Drawing.Point(381, 226);
			this.TextEdit_batch_number.Name = "TextEdit_batch_number";
			this.TextEdit_batch_number.Properties.Appearance.Options.UseTextOptions = true;
			this.TextEdit_batch_number.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
			this.TextEdit_batch_number.Properties.AppearanceDisabled.Options.UseTextOptions = true;
			this.TextEdit_batch_number.Properties.AppearanceDisabled.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
			this.TextEdit_batch_number.Properties.AppearanceFocused.Options.UseTextOptions = true;
			this.TextEdit_batch_number.Properties.AppearanceFocused.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
			this.TextEdit_batch_number.Properties.AppearanceReadOnly.Options.UseTextOptions = true;
			this.TextEdit_batch_number.Properties.AppearanceReadOnly.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
			this.TextEdit_batch_number.Properties.DisplayFormat.FormatString = "f0";
			this.TextEdit_batch_number.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
			this.TextEdit_batch_number.Properties.EditFormat.FormatString = "f0";
			this.TextEdit_batch_number.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
			this.TextEdit_batch_number.Properties.Mask.BeepOnError = true;
			this.TextEdit_batch_number.Properties.Mask.EditMask = "f0";
			this.TextEdit_batch_number.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
			this.TextEdit_batch_number.Size = new System.Drawing.Size(100, 20);
			this.TextEdit_batch_number.TabIndex = 32;
			//
			//MemoExEdit_suffix_lines
			//
			this.MemoExEdit_suffix_lines.Location = new System.Drawing.Point(381, 199);
			this.MemoExEdit_suffix_lines.Name = "MemoExEdit_suffix_lines";
			this.MemoExEdit_suffix_lines.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.True;
			this.MemoExEdit_suffix_lines.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] { new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo) });
			this.MemoExEdit_suffix_lines.Properties.MaxLength = 256;
			this.MemoExEdit_suffix_lines.Properties.ScrollBars = ScrollBars.Vertical;
			this.MemoExEdit_suffix_lines.Properties.ShowIcon = false;
			this.MemoExEdit_suffix_lines.Properties.WordWrap = false;
			this.MemoExEdit_suffix_lines.Size = new System.Drawing.Size(100, 20);
			this.MemoExEdit_suffix_lines.TabIndex = 30;
			//
			//MemoExEdit_prefix_lines
			//
			this.MemoExEdit_prefix_lines.Location = new System.Drawing.Point(381, 173);
			this.MemoExEdit_prefix_lines.Name = "MemoExEdit_prefix_lines";
			this.MemoExEdit_prefix_lines.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.True;
			this.MemoExEdit_prefix_lines.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] { new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo) });
			this.MemoExEdit_prefix_lines.Properties.MaxLength = 256;
			this.MemoExEdit_prefix_lines.Properties.ScrollBars = ScrollBars.Vertical;
			this.MemoExEdit_prefix_lines.Properties.ShowIcon = false;
			this.MemoExEdit_prefix_lines.Properties.WordWrap = false;
			this.MemoExEdit_prefix_lines.Size = new System.Drawing.Size(100, 20);
			this.MemoExEdit_prefix_lines.TabIndex = 28;
			//
			//GroupControl3
			//
			this.GroupControl3.Controls.Add(this.TextEdit_immediate_destination_name);
			this.GroupControl3.Controls.Add(this.TextEdit_immediate_destination);
			this.GroupControl3.Controls.Add(this.LabelControl11);
			this.GroupControl3.Controls.Add(this.LabelControl12);
			this.GroupControl3.Location = new System.Drawing.Point(281, 89);
			this.GroupControl3.Name = "GroupControl3";
			this.GroupControl3.Size = new System.Drawing.Size(200, 78);
			this.GroupControl3.TabIndex = 26;
			this.GroupControl3.Text = "Immediate Destination";
			//
			//TextEdit_immediate_destination_name
			//
			this.TextEdit_immediate_destination_name.Location = new System.Drawing.Point(45, 49);
			this.TextEdit_immediate_destination_name.Name = "TextEdit_immediate_destination_name";
			this.TextEdit_immediate_destination_name.Properties.CharacterCasing = CharacterCasing.Upper;
			this.TextEdit_immediate_destination_name.Properties.MaxLength = 30;
			this.TextEdit_immediate_destination_name.Size = new System.Drawing.Size(143, 20);
			this.TextEdit_immediate_destination_name.TabIndex = 3;
			//
			//TextEdit_immediate_destination
			//
			this.TextEdit_immediate_destination.Location = new System.Drawing.Point(45, 23);
			this.TextEdit_immediate_destination.Name = "TextEdit_immediate_destination";
			this.TextEdit_immediate_destination.Properties.Appearance.Options.UseTextOptions = true;
			this.TextEdit_immediate_destination.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
			this.TextEdit_immediate_destination.Properties.AppearanceDisabled.Options.UseTextOptions = true;
			this.TextEdit_immediate_destination.Properties.AppearanceDisabled.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
			this.TextEdit_immediate_destination.Properties.AppearanceFocused.Options.UseTextOptions = true;
			this.TextEdit_immediate_destination.Properties.AppearanceFocused.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
			this.TextEdit_immediate_destination.Properties.AppearanceReadOnly.Options.UseTextOptions = true;
			this.TextEdit_immediate_destination.Properties.AppearanceReadOnly.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
			this.TextEdit_immediate_destination.Properties.Mask.BeepOnError = true;
			this.TextEdit_immediate_destination.Properties.Mask.EditMask = "[ 0-9]{0,12}";
			this.TextEdit_immediate_destination.Properties.Mask.IgnoreMaskBlank = false;
			this.TextEdit_immediate_destination.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.RegEx;
			this.TextEdit_immediate_destination.Properties.MaxLength = 12;
			this.TextEdit_immediate_destination.Size = new System.Drawing.Size(143, 20);
			this.TextEdit_immediate_destination.TabIndex = 1;
			//
			//LabelControl11
			//
			this.LabelControl11.Location = new System.Drawing.Point(5, 52);
			this.LabelControl11.Name = "LabelControl11";
			this.LabelControl11.Size = new System.Drawing.Size(27, 13);
			this.LabelControl11.TabIndex = 2;
			this.LabelControl11.Text = "Name";
			//
			//LabelControl12
			//
			this.LabelControl12.Location = new System.Drawing.Point(5, 26);
			this.LabelControl12.Name = "LabelControl12";
			this.LabelControl12.Size = new System.Drawing.Size(11, 13);
			this.LabelControl12.TabIndex = 0;
			this.LabelControl12.Text = "ID";
			//
			//LabelControl16
			//
			this.LabelControl16.Location = new System.Drawing.Point(281, 203);
			this.LabelControl16.Name = "LabelControl16";
			this.LabelControl16.Size = new System.Drawing.Size(60, 13);
			this.LabelControl16.TabIndex = 29;
			this.LabelControl16.Text = "Suffix line(s)";
			//
			//LabelControl15
			//
			this.LabelControl15.Location = new System.Drawing.Point(281, 177);
			this.LabelControl15.Name = "LabelControl15";
			this.LabelControl15.Size = new System.Drawing.Size(60, 13);
			this.LabelControl15.TabIndex = 27;
			this.LabelControl15.Text = "Prefix line(s)";
			//
			//LabelControl14
			//
			this.LabelControl14.Location = new System.Drawing.Point(281, 258);
			this.LabelControl14.Name = "LabelControl14";
			this.LabelControl14.Size = new System.Drawing.Size(96, 13);
			this.LabelControl14.TabIndex = 33;
			this.LabelControl14.Text = "Transaction Number";
			//
			//LabelControl13
			//
			this.LabelControl13.Location = new System.Drawing.Point(281, 230);
			this.LabelControl13.Name = "LabelControl13";
			this.LabelControl13.Size = new System.Drawing.Size(67, 13);
			this.LabelControl13.TabIndex = 31;
			this.LabelControl13.Text = "Batch Number";
			//
			//GroupControl1
			//
			this.GroupControl1.Controls.Add(this.TextEdit_immediate_origin_name);
			this.GroupControl1.Controls.Add(this.TextEdit_immediate_origin);
			this.GroupControl1.Controls.Add(this.LabelControl4);
			this.GroupControl1.Controls.Add(this.LabelControl3);
			this.GroupControl1.Location = new System.Drawing.Point(281, 5);
			this.GroupControl1.Name = "GroupControl1";
			this.GroupControl1.Size = new System.Drawing.Size(200, 78);
			this.GroupControl1.TabIndex = 25;
			this.GroupControl1.Text = "Immediate Origin";
			//
			//TextEdit_immediate_origin_name
			//
			this.TextEdit_immediate_origin_name.Location = new System.Drawing.Point(45, 49);
			this.TextEdit_immediate_origin_name.Name = "TextEdit_immediate_origin_name";
			this.TextEdit_immediate_origin_name.Properties.CharacterCasing = CharacterCasing.Upper;
			this.TextEdit_immediate_origin_name.Properties.MaxLength = 30;
			this.TextEdit_immediate_origin_name.Size = new System.Drawing.Size(143, 20);
			this.TextEdit_immediate_origin_name.TabIndex = 3;
			//
			//TextEdit_immediate_origin
			//
			this.TextEdit_immediate_origin.Location = new System.Drawing.Point(45, 23);
			this.TextEdit_immediate_origin.Name = "TextEdit_immediate_origin";
			this.TextEdit_immediate_origin.Properties.Appearance.Options.UseTextOptions = true;
			this.TextEdit_immediate_origin.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
			this.TextEdit_immediate_origin.Properties.AppearanceDisabled.Options.UseTextOptions = true;
			this.TextEdit_immediate_origin.Properties.AppearanceDisabled.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
			this.TextEdit_immediate_origin.Properties.AppearanceFocused.Options.UseTextOptions = true;
			this.TextEdit_immediate_origin.Properties.AppearanceFocused.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
			this.TextEdit_immediate_origin.Properties.AppearanceReadOnly.Options.UseTextOptions = true;
			this.TextEdit_immediate_origin.Properties.AppearanceReadOnly.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
			this.TextEdit_immediate_origin.Properties.Mask.EditMask = "[ 0-9]{0,12}";
			this.TextEdit_immediate_origin.Properties.Mask.IgnoreMaskBlank = false;
			this.TextEdit_immediate_origin.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.RegEx;
			this.TextEdit_immediate_origin.Properties.MaxLength = 12;
			this.TextEdit_immediate_origin.Size = new System.Drawing.Size(143, 20);
			this.TextEdit_immediate_origin.TabIndex = 1;
			//
			//LabelControl4
			//
			this.LabelControl4.Location = new System.Drawing.Point(5, 52);
			this.LabelControl4.Name = "LabelControl4";
			this.LabelControl4.Size = new System.Drawing.Size(27, 13);
			this.LabelControl4.TabIndex = 2;
			this.LabelControl4.Text = "Name";
			//
			//LabelControl3
			//
			this.LabelControl3.Location = new System.Drawing.Point(5, 26);
			this.LabelControl3.Name = "LabelControl3";
			this.LabelControl3.Size = new System.Drawing.Size(11, 13);
			this.LabelControl3.TabIndex = 0;
			this.LabelControl3.Text = "ID";
			//
			//LabelControl17
			//
			this.LabelControl17.Location = new System.Drawing.Point(11, 283);
			this.LabelControl17.Name = "LabelControl17";
			this.LabelControl17.Size = new System.Drawing.Size(106, 13);
			this.LabelControl17.TabIndex = 36;
			this.LabelControl17.Text = "Output Directory Path";
			//
			//ButtonEdit_output_directory
			//
			this.ButtonEdit_output_directory.Location = new System.Drawing.Point(126, 280);
			this.ButtonEdit_output_directory.Name = "ButtonEdit_output_directory";
			this.ButtonEdit_output_directory.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] { new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Right) });
			this.ButtonEdit_output_directory.Properties.MaxLength = 256;
			this.ButtonEdit_output_directory.Size = new System.Drawing.Size(355, 20);
			this.ButtonEdit_output_directory.TabIndex = 50;
			//
			//Banks_R
			//
			this.Appearance.BackColor = System.Drawing.Color.Transparent;
			this.Appearance.Options.UseBackColor = true;
			this.AutoScaleDimensions = new System.Drawing.SizeF(6f, 13f);
			this.AutoScaleMode = AutoScaleMode.Font;
			this.Controls.Add(this.ButtonEdit_output_directory);
			this.Controls.Add(this.LabelControl17);
			this.Controls.Add(this.TextEdit_transaction_number);
			this.Controls.Add(this.TextEdit_batch_number);
			this.Controls.Add(this.MemoExEdit_suffix_lines);
			this.Controls.Add(this.MemoExEdit_prefix_lines);
			this.Controls.Add(this.GroupControl3);
			this.Controls.Add(this.LabelControl16);
			this.Controls.Add(this.LabelControl15);
			this.Controls.Add(this.LabelControl14);
			this.Controls.Add(this.LabelControl13);
			this.Controls.Add(this.GroupControl1);
			this.Name = "Banks_R";
			this.Size = new System.Drawing.Size(484, 314);
			((System.ComponentModel.ISupportInitialize)this.TextEdit_transaction_number.Properties).EndInit();
			((System.ComponentModel.ISupportInitialize)this.TextEdit_batch_number.Properties).EndInit();
			((System.ComponentModel.ISupportInitialize)this.MemoExEdit_suffix_lines.Properties).EndInit();
			((System.ComponentModel.ISupportInitialize)this.MemoExEdit_prefix_lines.Properties).EndInit();
			((System.ComponentModel.ISupportInitialize)this.GroupControl3).EndInit();
			this.GroupControl3.ResumeLayout(false);
			this.GroupControl3.PerformLayout();
			((System.ComponentModel.ISupportInitialize)this.TextEdit_immediate_destination_name.Properties).EndInit();
			((System.ComponentModel.ISupportInitialize)this.TextEdit_immediate_destination.Properties).EndInit();
			((System.ComponentModel.ISupportInitialize)this.GroupControl1).EndInit();
			this.GroupControl1.ResumeLayout(false);
			this.GroupControl1.PerformLayout();
			((System.ComponentModel.ISupportInitialize)this.TextEdit_immediate_origin_name.Properties).EndInit();
			((System.ComponentModel.ISupportInitialize)this.TextEdit_immediate_origin.Properties).EndInit();
			((System.ComponentModel.ISupportInitialize)this.ButtonEdit_output_directory.Properties).EndInit();
			this.ResumeLayout(false);
			this.PerformLayout();

		}
		private DevExpress.XtraEditors.TextEdit TextEdit_transaction_number;
		private DevExpress.XtraEditors.TextEdit TextEdit_batch_number;
		private DevExpress.XtraEditors.MemoExEdit MemoExEdit_suffix_lines;
		private DevExpress.XtraEditors.MemoExEdit MemoExEdit_prefix_lines;
		private DevExpress.XtraEditors.GroupControl GroupControl3;
		private DevExpress.XtraEditors.TextEdit TextEdit_immediate_destination_name;
		private DevExpress.XtraEditors.TextEdit TextEdit_immediate_destination;
		private DevExpress.XtraEditors.LabelControl LabelControl11;
		private DevExpress.XtraEditors.LabelControl LabelControl12;
		private DevExpress.XtraEditors.LabelControl LabelControl16;
		private DevExpress.XtraEditors.LabelControl LabelControl15;
		private DevExpress.XtraEditors.LabelControl LabelControl14;
		private DevExpress.XtraEditors.LabelControl LabelControl13;
		private DevExpress.XtraEditors.GroupControl GroupControl1;
		private DevExpress.XtraEditors.TextEdit TextEdit_immediate_origin_name;
		private DevExpress.XtraEditors.TextEdit TextEdit_immediate_origin;
		private DevExpress.XtraEditors.LabelControl LabelControl4;
		private DevExpress.XtraEditors.LabelControl LabelControl3;
		private DevExpress.XtraEditors.LabelControl LabelControl17;

		private DevExpress.XtraEditors.ButtonEdit ButtonEdit_output_directory;
	}
}

//=======================================================
//Service provided by Telerik (www.telerik.com)
//Conversion powered by NRefactory.
//Twitter: @telerik
//Facebook: facebook.com/telerik
//=======================================================
