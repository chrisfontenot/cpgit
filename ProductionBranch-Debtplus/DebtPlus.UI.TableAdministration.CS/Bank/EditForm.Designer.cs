namespace DebtPlus.UI.TableAdministration.CS.Bank
{
	partial class EditForm
	{
		//Form overrides dispose to clean up the component list.
		[System.Diagnostics.DebuggerNonUserCode]
		protected override void Dispose(bool disposing)
		{
			try 
            {
				if (disposing)
                {
                    if (components != null) components.Dispose();
				}
                components = null;
            }
    	    finally
            {
				base.Dispose(disposing);
			}
		}

		//Required by the Windows Form Designer
		private System.ComponentModel.IContainer components = null;

		//NOTE: The following procedure is required by the Windows Form Designer
		//It can be modified using the Windows Form Designer.  
		//Do not modify it using the code editor.
		[System.Diagnostics.DebuggerStepThrough]
		private void InitializeComponent()
		{
            this.LabelControl1 = new DevExpress.XtraEditors.LabelControl();
            this.TextEdit_description = new DevExpress.XtraEditors.TextEdit();
            this.LabelControl2 = new DevExpress.XtraEditors.LabelControl();
            this.ComboBoxEdit_type = new DevExpress.XtraEditors.ComboBoxEdit();
            this.CheckEdit_default = new DevExpress.XtraEditors.CheckEdit();
            this.SimpleButton_Address = new DevExpress.XtraEditors.SimpleButton();
            this.CheckEdit_ActiveFlag = new DevExpress.XtraEditors.CheckEdit();
            ((System.ComponentModel.ISupportInitialize)(this.DxErrorProvider1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TextEdit_description.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ComboBoxEdit_type.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.CheckEdit_default.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.CheckEdit_ActiveFlag.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // simpleButton_OK
            // 
            this.simpleButton_OK.Enabled = true;
            this.simpleButton_OK.Location = new System.Drawing.Point(522, 43);
            // 
            // simpleButton_Cancel
            // 
            this.simpleButton_Cancel.Location = new System.Drawing.Point(521, 86);
            // 
            // DefaultLookAndFeel1
            // 
            this.DefaultLookAndFeel1.LookAndFeel.SkinName = "McSkin";
            // 
            // LabelControl1
            // 
            this.LabelControl1.Location = new System.Drawing.Point(13, 16);
            this.LabelControl1.Name = "LabelControl1";
            this.LabelControl1.Size = new System.Drawing.Size(53, 13);
            this.LabelControl1.TabIndex = 20;
            this.LabelControl1.Text = "Description";
            // 
            // TextEdit_description
            // 
            this.TextEdit_description.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.TextEdit_description.Location = new System.Drawing.Point(86, 13);
            this.TextEdit_description.Name = "TextEdit_description";
            this.TextEdit_description.Properties.MaxLength = 50;
            this.TextEdit_description.Size = new System.Drawing.Size(406, 20);
            this.TextEdit_description.TabIndex = 21;
            // 
            // LabelControl2
            // 
            this.LabelControl2.Location = new System.Drawing.Point(13, 46);
            this.LabelControl2.Name = "LabelControl2";
            this.LabelControl2.Size = new System.Drawing.Size(24, 13);
            this.LabelControl2.TabIndex = 22;
            this.LabelControl2.Text = "Type";
            // 
            // ComboBoxEdit_type
            // 
            this.ComboBoxEdit_type.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.ComboBoxEdit_type.Location = new System.Drawing.Point(86, 43);
            this.ComboBoxEdit_type.Name = "ComboBoxEdit_type";
            this.ComboBoxEdit_type.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.ComboBoxEdit_type.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
            this.ComboBoxEdit_type.Size = new System.Drawing.Size(406, 20);
            this.ComboBoxEdit_type.TabIndex = 23;
            // 
            // Banks_A1
            // 
            this.Banks_A1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.Banks_A1.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.Banks_A1.Appearance.Options.UseBackColor = true;
            this.Banks_A1.Location = new System.Drawing.Point(8, 69);
            this.Banks_A1.Name = "Banks_A1";
            this.Banks_A1.Size = new System.Drawing.Size(500, 320);
            this.Banks_A1.TabIndex = 24;
            // 
            // Banks_C1
            // 
            this.Banks_C1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.Banks_C1.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.Banks_C1.Appearance.Options.UseBackColor = true;
            this.Banks_C1.Location = new System.Drawing.Point(8, 69);
            this.Banks_C1.Name = "Banks_C1";
            this.Banks_C1.Size = new System.Drawing.Size(500, 320);
            this.Banks_C1.TabIndex = 25;
            // 
            // Banks_R1
            // 
            this.Banks_R1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.Banks_R1.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.Banks_R1.Appearance.Options.UseBackColor = true;
            this.Banks_R1.Location = new System.Drawing.Point(8, 69);
            this.Banks_R1.Name = "Banks_R1";
            this.Banks_R1.Size = new System.Drawing.Size(500, 320);
            this.Banks_R1.TabIndex = 26;
            // 
            // Banks_D1
            // 
            this.Banks_D1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.Banks_D1.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.Banks_D1.Appearance.Options.UseBackColor = true;
            this.Banks_D1.Location = new System.Drawing.Point(8, 69);
            this.Banks_D1.Name = "Banks_D1";
            this.Banks_D1.Size = new System.Drawing.Size(500, 320);
            this.Banks_D1.TabIndex = 32;
            // 
            // CheckEdit_default
            // 
            this.CheckEdit_default.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.CheckEdit_default.Location = new System.Drawing.Point(6, 395);
            this.CheckEdit_default.Name = "CheckEdit_default";
            this.CheckEdit_default.Properties.Caption = "Default Account";
            this.CheckEdit_default.Size = new System.Drawing.Size(111, 20);
            this.CheckEdit_default.TabIndex = 30;
            // 
            // SimpleButton_Address
            // 
            this.SimpleButton_Address.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.SimpleButton_Address.Location = new System.Drawing.Point(521, 129);
            this.SimpleButton_Address.Name = "SimpleButton_Address";
            this.SimpleButton_Address.Size = new System.Drawing.Size(72, 26);
            this.SimpleButton_Address.TabIndex = 31;
            this.SimpleButton_Address.Text = "&Address...";
            // 
            // CheckEdit_ActiveFlag
            // 
            this.CheckEdit_ActiveFlag.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.CheckEdit_ActiveFlag.Location = new System.Drawing.Point(141, 395);
            this.CheckEdit_ActiveFlag.Name = "CheckEdit_ActiveFlag";
            this.CheckEdit_ActiveFlag.Properties.Caption = "Active";
            this.CheckEdit_ActiveFlag.Size = new System.Drawing.Size(111, 20);
            this.CheckEdit_ActiveFlag.TabIndex = 33;
            // 
            // EditForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(618, 427);
            this.Controls.Add(this.CheckEdit_ActiveFlag);
            this.Controls.Add(this.SimpleButton_Address);
            this.Controls.Add(this.CheckEdit_default);
            this.Controls.Add(this.ComboBoxEdit_type);
            this.Controls.Add(this.TextEdit_description);
            this.Controls.Add(this.LabelControl2);
            this.Controls.Add(this.LabelControl1);
            this.Controls.Add(this.Banks_R1);
            this.Controls.Add(this.Banks_A1);
            this.Controls.Add(this.Banks_D1);
            this.Controls.Add(this.Banks_C1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.Sizable;
            this.Name = "EditForm";
            this.Text = "Bank Record Information";
            this.Controls.SetChildIndex(this.Banks_C1, 0);
            this.Controls.SetChildIndex(this.Banks_D1, 0);
            this.Controls.SetChildIndex(this.Banks_A1, 0);
            this.Controls.SetChildIndex(this.Banks_R1, 0);
            this.Controls.SetChildIndex(this.LabelControl1, 0);
            this.Controls.SetChildIndex(this.LabelControl2, 0);
            this.Controls.SetChildIndex(this.TextEdit_description, 0);
            this.Controls.SetChildIndex(this.ComboBoxEdit_type, 0);
            this.Controls.SetChildIndex(this.simpleButton_OK, 0);
            this.Controls.SetChildIndex(this.simpleButton_Cancel, 0);
            this.Controls.SetChildIndex(this.CheckEdit_default, 0);
            this.Controls.SetChildIndex(this.SimpleButton_Address, 0);
            this.Controls.SetChildIndex(this.CheckEdit_ActiveFlag, 0);
            ((System.ComponentModel.ISupportInitialize)(this.DxErrorProvider1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TextEdit_description.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ComboBoxEdit_type.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.CheckEdit_default.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.CheckEdit_ActiveFlag.Properties)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

		}
		private DevExpress.XtraEditors.LabelControl LabelControl1;
        private DevExpress.XtraEditors.LabelControl LabelControl2;
        private DevExpress.XtraEditors.TextEdit TextEdit_description;
        private DevExpress.XtraEditors.ComboBoxEdit ComboBoxEdit_type;
        private DevExpress.XtraEditors.CheckEdit CheckEdit_default;
        private DevExpress.XtraEditors.SimpleButton SimpleButton_Address;
        private DevExpress.XtraEditors.CheckEdit CheckEdit_ActiveFlag;
        private DebtPlus.UI.TableAdministration.CS.Bank.Controls.Banks_A Banks_A1;
        private DebtPlus.UI.TableAdministration.CS.Bank.Controls.Banks_C Banks_C1;
        private DebtPlus.UI.TableAdministration.CS.Bank.Controls.Banks_R Banks_R1;
        private DebtPlus.UI.TableAdministration.CS.Bank.Controls.Banks_D Banks_D1;
	}
}
