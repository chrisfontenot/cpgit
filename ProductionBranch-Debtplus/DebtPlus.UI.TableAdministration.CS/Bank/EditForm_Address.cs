using System;
using DebtPlus.LINQ;

namespace DebtPlus.UI.TableAdministration.CS.Bank
{
    internal partial class EditForm_Address : Templates.EditTemplateForm
    {
        private BusinessContext bc = null;
        private bank record = null;

        internal EditForm_Address()
            : base()
        {
            InitializeComponent();
        }

        internal EditForm_Address(BusinessContext bc, bank record)
            : this()
        {
            this.bc = bc;
            this.record = record;
            RegisterHandlers();
        }

        private void RegisterHandlers()
        {
            Load += EditForm_Address_Load;
        }

        private void UnRegisterHandlers()
        {
            Load -= EditForm_Address_Load;
        }

        private void EditForm_Address_Load(object sender, EventArgs e)
        {
            UnRegisterHandlers();
            try
            {
                AddressInformation1.ReadForm(bc, record);
                simpleButton_OK.Enabled = !AddressInformation1.HasErrors();
            }
            catch (System.Data.SqlClient.SqlException ex)
            {
                DebtPlus.UI.Common.ErrorHandling.HandleErrors(ex);
            }
            finally
            {
                RegisterHandlers();
            }
        }

        protected override void simpleButton_OK_Click(System.Object sender, EventArgs e)
        {
            AddressInformation1.SaveForm(bc, record);
            base.simpleButton_OK_Click(sender, e);
        }
    }
}