using System;
using System.Windows.Forms;
using System.Drawing;
using DebtPlus.LINQ;
using System.Linq;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Diagnostics;

namespace DebtPlus.UI.TableAdministration.CS.Bank
{
	partial class EditForm_Signature : global::DebtPlus.Data.Forms.DebtPlusForm
	{
		//Form overrides dispose to clean up the component list.
		[System.Diagnostics.DebuggerNonUserCode]
		protected override void Dispose(bool disposing)
		{
			try 
            {
				if (disposing)
                {
                    if (components != null) components.Dispose();
				}
                components = null;
            }
    	    finally
            {
				base.Dispose(disposing);
			}
		}

		//Required by the Windows Form Designer
		private System.ComponentModel.IContainer components = null;

		//NOTE: The following procedure is required by the Windows Form Designer
		//It can be modified using the Windows Form Designer.  
		//Do not modify it using the code editor.
		[System.Diagnostics.DebuggerStepThrough]
		private void InitializeComponent()
		{
			this.components = new System.ComponentModel.Container();
			this.PanelControl1 = new DevExpress.XtraEditors.PanelControl();
			this.XtraScrollableControl1 = new DevExpress.XtraEditors.XtraScrollableControl();
			this.PictureEdit1 = new DevExpress.XtraEditors.PictureEdit();
			this.simpleButton_Cancel = new DevExpress.XtraEditors.SimpleButton();
			this.simpleButton_OK = new DevExpress.XtraEditors.SimpleButton();
			this.SimpleButton_Open = new DevExpress.XtraEditors.SimpleButton();
			this.LabelControl1 = new DevExpress.XtraEditors.LabelControl();
			this.barManager1 = new DevExpress.XtraBars.BarManager(this.components);
			this.Bar2 = new DevExpress.XtraBars.Bar();
			this.BarSubItem1 = new DevExpress.XtraBars.BarSubItem();
			this.BarButtonItem_File_Open = new DevExpress.XtraBars.BarButtonItem();
			this.BarButtonItem_File_Exit = new DevExpress.XtraBars.BarButtonItem();
			this.BarButtonItem_Save = new DevExpress.XtraBars.BarButtonItem();
			this.BarSubItem_View = new DevExpress.XtraBars.BarSubItem();
			this.BarSubItem_View_Mode = new DevExpress.XtraBars.BarSubItem();
			this.BarCheckItem_View_Stretch = new DevExpress.XtraBars.BarCheckItem();
			this.BarCheckItem_View_Normal = new DevExpress.XtraBars.BarCheckItem();
			this.BarCheckItem_View_Squeeze = new DevExpress.XtraBars.BarCheckItem();
			this.BarCheckItem_View_Zoom = new DevExpress.XtraBars.BarCheckItem();
			this.BarStaticItem_Resolution = new DevExpress.XtraBars.BarStaticItem();
			this.BarSubItem_View_Transform = new DevExpress.XtraBars.BarSubItem();
			this.BarButtonItem_Rotate_Right = new DevExpress.XtraBars.BarButtonItem();
			this.BarButtonItem_Rotate_Left = new DevExpress.XtraBars.BarButtonItem();
			this.BarButtonItem_Flip_Horizontal = new DevExpress.XtraBars.BarButtonItem();
			this.BarButtonItem_Flip_Vertical = new DevExpress.XtraBars.BarButtonItem();
			this.barDockControlTop = new DevExpress.XtraBars.BarDockControl();
			this.barDockControlBottom = new DevExpress.XtraBars.BarDockControl();
			this.barDockControlLeft = new DevExpress.XtraBars.BarDockControl();
			this.barDockControlRight = new DevExpress.XtraBars.BarDockControl();
			((System.ComponentModel.ISupportInitialize)this.DxErrorProvider1).BeginInit();
			((System.ComponentModel.ISupportInitialize)this.PanelControl1).BeginInit();
			this.PanelControl1.SuspendLayout();
			this.XtraScrollableControl1.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)this.PictureEdit1.Properties).BeginInit();
			((System.ComponentModel.ISupportInitialize)this.barManager1).BeginInit();
			this.SuspendLayout();
			//
			//DefaultLookAndFeel1
			//
			this.DefaultLookAndFeel1.LookAndFeel.SkinName = "McSkin";
			//
			//PanelControl1
			//
			this.PanelControl1.Controls.Add(this.XtraScrollableControl1);
			this.PanelControl1.Controls.Add(this.simpleButton_Cancel);
			this.PanelControl1.Controls.Add(this.simpleButton_OK);
			this.PanelControl1.Controls.Add(this.SimpleButton_Open);
			this.PanelControl1.Controls.Add(this.LabelControl1);
			this.PanelControl1.Dock = DockStyle.Fill;
			this.PanelControl1.Location = new System.Drawing.Point(0, 25);
			this.PanelControl1.Name = "PanelControl1";
			this.PanelControl1.Size = new System.Drawing.Size(292, 176);
			this.PanelControl1.TabIndex = 0;
			//
			//XtraScrollableControl1
			//
			this.XtraScrollableControl1.Anchor = (AnchorStyles)(((AnchorStyles.Top | AnchorStyles.Bottom) | AnchorStyles.Left) | AnchorStyles.Right);
			this.XtraScrollableControl1.Controls.Add(this.PictureEdit1);
			this.XtraScrollableControl1.Location = new System.Drawing.Point(13, 32);
			this.XtraScrollableControl1.Name = "XtraScrollableControl1";
			this.XtraScrollableControl1.Size = new System.Drawing.Size(267, 103);
			this.XtraScrollableControl1.TabIndex = 11;
			//
			//PictureEdit1
			//
			this.PictureEdit1.Location = new System.Drawing.Point(0, 0);
			this.PictureEdit1.Name = "PictureEdit1";
			this.PictureEdit1.Properties.SizeMode = DevExpress.XtraEditors.Controls.PictureSizeMode.Stretch;
			this.PictureEdit1.Size = new System.Drawing.Size(267, 104);
			this.PictureEdit1.TabIndex = 7;
			//
			//simpleButton_Cancel
			//
			this.simpleButton_Cancel.Anchor = AnchorStyles.Bottom;
			this.simpleButton_Cancel.Location = new System.Drawing.Point(196, 141);
			this.simpleButton_Cancel.Name = "simpleButton_Cancel";
			this.simpleButton_Cancel.Size = new System.Drawing.Size(75, 23);
			this.simpleButton_Cancel.TabIndex = 10;
			this.simpleButton_Cancel.Text = "&Cancel";
			//
			//simpleButton_OK
			//
			this.simpleButton_OK.Anchor = AnchorStyles.Bottom;
			this.simpleButton_OK.Enabled = false;
			this.simpleButton_OK.Location = new System.Drawing.Point(106, 141);
			this.simpleButton_OK.Name = "simpleButton_OK";
			this.simpleButton_OK.Size = new System.Drawing.Size(75, 23);
			this.simpleButton_OK.TabIndex = 9;
			this.simpleButton_OK.Text = "&OK";
			//
			//SimpleButton_Open
			//
			this.SimpleButton_Open.Anchor = AnchorStyles.Bottom;
			this.SimpleButton_Open.Location = new System.Drawing.Point(13, 141);
			this.SimpleButton_Open.Name = "SimpleButton_Open";
			this.SimpleButton_Open.Size = new System.Drawing.Size(75, 23);
			this.SimpleButton_Open.TabIndex = 8;
			this.SimpleButton_Open.Text = "&Open...";
			//
			//LabelControl1
			//
			this.LabelControl1.Location = new System.Drawing.Point(13, 12);
			this.LabelControl1.Name = "LabelControl1";
			this.LabelControl1.Size = new System.Drawing.Size(183, 13);
			this.LabelControl1.TabIndex = 7;
			this.LabelControl1.Text = "The current signature is shown below.";
			//
			//barManager1
			//
			this.barManager1.Bars.AddRange(new DevExpress.XtraBars.Bar[] { this.Bar2 });
			this.barManager1.DockControls.Add(this.barDockControlTop);
			this.barManager1.DockControls.Add(this.barDockControlBottom);
			this.barManager1.DockControls.Add(this.barDockControlLeft);
			this.barManager1.DockControls.Add(this.barDockControlRight);
			this.barManager1.Form = this;
			this.barManager1.Items.AddRange(new DevExpress.XtraBars.BarItem[] {
				this.BarSubItem1,
				this.BarSubItem_View,
				this.BarCheckItem_View_Stretch,
				this.BarCheckItem_View_Normal,
				this.BarButtonItem_File_Open,
				this.BarButtonItem_File_Exit,
				this.BarCheckItem_View_Squeeze,
				this.BarCheckItem_View_Zoom,
				this.BarSubItem_View_Mode,
				this.BarSubItem_View_Transform,
				this.BarButtonItem_Rotate_Right,
				this.BarButtonItem_Rotate_Left,
				this.BarButtonItem_Flip_Horizontal,
				this.BarButtonItem_Flip_Vertical,
				this.BarStaticItem_Resolution,
				this.BarButtonItem_Save
			});
			this.barManager1.MainMenu = this.Bar2;
			this.barManager1.MaxItemId = 16;
			//
			//Bar2
			//
			this.Bar2.BarName = "Main menu";
			this.Bar2.DockCol = 0;
			this.Bar2.DockRow = 0;
			this.Bar2.DockStyle = DevExpress.XtraBars.BarDockStyle.Top;
			this.Bar2.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
				new DevExpress.XtraBars.LinkPersistInfo(this.BarSubItem1),
				new DevExpress.XtraBars.LinkPersistInfo(this.BarSubItem_View),
				new DevExpress.XtraBars.LinkPersistInfo(this.BarSubItem_View_Transform)
			});
			this.Bar2.OptionsBar.MultiLine = true;
			this.Bar2.OptionsBar.UseWholeRow = true;
			this.Bar2.Text = "Main menu";
			//
			//BarSubItem1
			//
			this.BarSubItem1.Caption = "&File";
			this.BarSubItem1.Id = 0;
			this.BarSubItem1.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
				new DevExpress.XtraBars.LinkPersistInfo(this.BarButtonItem_File_Open),
				new DevExpress.XtraBars.LinkPersistInfo(this.BarButtonItem_File_Exit),
				new DevExpress.XtraBars.LinkPersistInfo(this.BarButtonItem_Save)
			});
			this.BarSubItem1.Name = "BarSubItem1";
			//
			//BarButtonItem_File_Open
			//
			this.BarButtonItem_File_Open.Caption = "&Open...";
			this.BarButtonItem_File_Open.Id = 4;
			this.BarButtonItem_File_Open.Name = "BarButtonItem_File_Open";
			//
			//BarButtonItem_File_Exit
			//
			this.BarButtonItem_File_Exit.Caption = "&Exit";
			this.BarButtonItem_File_Exit.Id = 5;
			this.BarButtonItem_File_Exit.Name = "BarButtonItem_File_Exit";
			//
			//BarButtonItem_Save
			//
			this.BarButtonItem_Save.Caption = "&Save...";
			this.BarButtonItem_Save.Id = 15;
			this.BarButtonItem_Save.Name = "BarButtonItem_Save";
			//
			//BarSubItem_View
			//
			this.BarSubItem_View.Caption = "&View";
			this.BarSubItem_View.Id = 1;
			this.BarSubItem_View.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
				new DevExpress.XtraBars.LinkPersistInfo(this.BarSubItem_View_Mode),
				new DevExpress.XtraBars.LinkPersistInfo(this.BarStaticItem_Resolution, true)
			});
			this.BarSubItem_View.Name = "BarSubItem_View";
			//
			//BarSubItem_View_Mode
			//
			this.BarSubItem_View_Mode.Caption = "Mode";
			this.BarSubItem_View_Mode.Id = 8;
			this.BarSubItem_View_Mode.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
				new DevExpress.XtraBars.LinkPersistInfo(this.BarCheckItem_View_Stretch),
				new DevExpress.XtraBars.LinkPersistInfo(this.BarCheckItem_View_Normal),
				new DevExpress.XtraBars.LinkPersistInfo(this.BarCheckItem_View_Squeeze),
				new DevExpress.XtraBars.LinkPersistInfo(this.BarCheckItem_View_Zoom)
			});
			this.BarSubItem_View_Mode.Name = "BarSubItem_View_Mode";
			//
			//BarCheckItem_View_Stretch
			//
			this.BarCheckItem_View_Stretch.Caption = "&Stretch";
			this.BarCheckItem_View_Stretch.Id = 2;
			this.BarCheckItem_View_Stretch.Name = "BarCheckItem_View_Stretch";
			//
			//BarCheckItem_View_Normal
			//
			this.BarCheckItem_View_Normal.Caption = "&Normal";
			this.BarCheckItem_View_Normal.Id = 3;
			this.BarCheckItem_View_Normal.Name = "BarCheckItem_View_Normal";
			//
			//BarCheckItem_View_Squeeze
			//
			this.BarCheckItem_View_Squeeze.Caption = "S&queeze";
			this.BarCheckItem_View_Squeeze.Id = 6;
			this.BarCheckItem_View_Squeeze.Name = "BarCheckItem_View_Squeeze";
			//
			//BarCheckItem_View_Zoom
			//
			this.BarCheckItem_View_Zoom.Caption = "&Zoom";
			this.BarCheckItem_View_Zoom.Id = 7;
			this.BarCheckItem_View_Zoom.Name = "BarCheckItem_View_Zoom";
			//
			//BarStaticItem_Resolution
			//
			this.BarStaticItem_Resolution.Caption = "Resolution";
			this.BarStaticItem_Resolution.Id = 14;
			this.BarStaticItem_Resolution.Name = "BarStaticItem_Resolution";
			this.BarStaticItem_Resolution.TextAlignment = System.Drawing.StringAlignment.Near;
			//
			//BarSubItem_View_Transform
			//
			this.BarSubItem_View_Transform.Caption = "&Transform";
			this.BarSubItem_View_Transform.Id = 9;
			this.BarSubItem_View_Transform.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
				new DevExpress.XtraBars.LinkPersistInfo(this.BarButtonItem_Rotate_Right),
				new DevExpress.XtraBars.LinkPersistInfo(this.BarButtonItem_Rotate_Left),
				new DevExpress.XtraBars.LinkPersistInfo(this.BarButtonItem_Flip_Horizontal),
				new DevExpress.XtraBars.LinkPersistInfo(this.BarButtonItem_Flip_Vertical)
			});
			this.BarSubItem_View_Transform.Name = "BarSubItem_View_Transform";
			//
			//BarButtonItem_Rotate_Right
			//
			this.BarButtonItem_Rotate_Right.Caption = "Rotate Right";
			this.BarButtonItem_Rotate_Right.Id = 10;
			this.BarButtonItem_Rotate_Right.Name = "BarButtonItem_Rotate_Right";
			//
			//BarButtonItem_Rotate_Left
			//
			this.BarButtonItem_Rotate_Left.Caption = "Rotate Left";
			this.BarButtonItem_Rotate_Left.Id = 11;
			this.BarButtonItem_Rotate_Left.Name = "BarButtonItem_Rotate_Left";
			//
			//BarButtonItem_Flip_Horizontal
			//
			this.BarButtonItem_Flip_Horizontal.Caption = "Flip Horizontal";
			this.BarButtonItem_Flip_Horizontal.Id = 12;
			this.BarButtonItem_Flip_Horizontal.Name = "BarButtonItem_Flip_Horizontal";
			//
			//BarButtonItem_Flip_Vertical
			//
			this.BarButtonItem_Flip_Vertical.Caption = "Flip Vertical";
			this.BarButtonItem_Flip_Vertical.Id = 13;
			this.BarButtonItem_Flip_Vertical.Name = "BarButtonItem_Flip_Vertical";
			//
			//barDockControlTop
			//
			this.barDockControlTop.CausesValidation = false;
			this.barDockControlTop.Dock = DockStyle.Top;
			this.barDockControlTop.Location = new System.Drawing.Point(0, 0);
			this.barDockControlTop.Size = new System.Drawing.Size(292, 25);
			//
			//barDockControlBottom
			//
			this.barDockControlBottom.CausesValidation = false;
			this.barDockControlBottom.Dock = DockStyle.Bottom;
			this.barDockControlBottom.Location = new System.Drawing.Point(0, 201);
			this.barDockControlBottom.Size = new System.Drawing.Size(292, 0);
			//
			//barDockControlLeft
			//
			this.barDockControlLeft.CausesValidation = false;
			this.barDockControlLeft.Dock = DockStyle.Left;
			this.barDockControlLeft.Location = new System.Drawing.Point(0, 25);
			this.barDockControlLeft.Size = new System.Drawing.Size(0, 176);
			//
			//barDockControlRight
			//
			this.barDockControlRight.CausesValidation = false;
			this.barDockControlRight.Dock = DockStyle.Right;
			this.barDockControlRight.Location = new System.Drawing.Point(292, 25);
			this.barDockControlRight.Size = new System.Drawing.Size(0, 176);
			//
			//EditForm_Signature
			//
			this.AutoScaleDimensions = new System.Drawing.SizeF(6f, 13f);
			this.AutoScaleMode = AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(292, 201);
			this.Controls.Add(this.PanelControl1);
			this.Controls.Add(this.barDockControlLeft);
			this.Controls.Add(this.barDockControlRight);
			this.Controls.Add(this.barDockControlBottom);
			this.Controls.Add(this.barDockControlTop);
			this.Name = "EditForm_Signature";
			this.Text = "Check Signature";
			((System.ComponentModel.ISupportInitialize)this.DxErrorProvider1).EndInit();
			((System.ComponentModel.ISupportInitialize)this.PanelControl1).EndInit();
			this.PanelControl1.ResumeLayout(false);
			this.PanelControl1.PerformLayout();
			this.XtraScrollableControl1.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)this.PictureEdit1.Properties).EndInit();
			((System.ComponentModel.ISupportInitialize)this.barManager1).EndInit();
			this.ResumeLayout(false);
		}

		private DevExpress.XtraEditors.PanelControl PanelControl1;
		private DevExpress.XtraEditors.SimpleButton simpleButton_Cancel;
		private DevExpress.XtraEditors.SimpleButton simpleButton_OK;
		private DevExpress.XtraEditors.SimpleButton SimpleButton_Open;
		private DevExpress.XtraEditors.LabelControl LabelControl1;
		private DevExpress.XtraBars.BarManager barManager1;
		private DevExpress.XtraBars.Bar Bar2;
		private DevExpress.XtraBars.BarDockControl barDockControlTop;
		private DevExpress.XtraBars.BarDockControl barDockControlBottom;
		private DevExpress.XtraBars.BarDockControl barDockControlLeft;
		private DevExpress.XtraBars.BarDockControl barDockControlRight;
		private DevExpress.XtraBars.BarSubItem BarSubItem1;
		private DevExpress.XtraBars.BarButtonItem BarButtonItem_File_Open;
		private DevExpress.XtraBars.BarButtonItem BarButtonItem_File_Exit;
		private DevExpress.XtraBars.BarSubItem BarSubItem_View;
		private DevExpress.XtraBars.BarCheckItem BarCheckItem_View_Stretch;
		private DevExpress.XtraBars.BarCheckItem BarCheckItem_View_Normal;
		private DevExpress.XtraBars.BarCheckItem BarCheckItem_View_Squeeze;
		private DevExpress.XtraBars.BarCheckItem BarCheckItem_View_Zoom;
		private DevExpress.XtraBars.BarSubItem BarSubItem_View_Mode;
		private DevExpress.XtraBars.BarSubItem BarSubItem_View_Transform;
		private DevExpress.XtraBars.BarButtonItem BarButtonItem_Rotate_Right;
		private DevExpress.XtraBars.BarButtonItem BarButtonItem_Rotate_Left;
		private DevExpress.XtraBars.BarButtonItem BarButtonItem_Flip_Horizontal;
		private DevExpress.XtraBars.BarButtonItem BarButtonItem_Flip_Vertical;
		private DevExpress.XtraBars.BarStaticItem BarStaticItem_Resolution;
		private DevExpress.XtraEditors.XtraScrollableControl XtraScrollableControl1;
		private DevExpress.XtraEditors.PictureEdit PictureEdit1;
		private DevExpress.XtraBars.BarButtonItem BarButtonItem_Save;
	}
}
