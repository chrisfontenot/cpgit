using System;
using System.Windows.Forms;
using System.Drawing;
using DebtPlus.LINQ;
using System.Linq;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Diagnostics;
using DebtPlus.UI.TableAdministration.CS.Bank.Controls;
using DebtPlus.UI.Common.Templates;

namespace DebtPlus.UI.TableAdministration.CS.Bank
{
	partial class EditForm_Address
	{
		//Form overrides dispose to clean up the component list.
		[System.Diagnostics.DebuggerNonUserCode]
		protected override void Dispose(bool disposing)
		{
			try 
            {
				if (disposing)
                {
                    if (components != null) components.Dispose();
				}
                components = null;
            }
    	    finally
            {
				base.Dispose(disposing);
			}
		}

		//Required by the Windows Form Designer
		private System.ComponentModel.IContainer components = null;

		//NOTE: The following procedure is required by the Windows Form Designer
		//It can be modified using the Windows Form Designer.  
		//Do not modify it using the code editor.
		[System.Diagnostics.DebuggerStepThrough]
		private void InitializeComponent()
		{
			this.AddressInformation1 = new AddressInformation();
			((System.ComponentModel.ISupportInitialize)this.DxErrorProvider1).BeginInit();
			this.SuspendLayout();
			//
			//simpleButton_OK
			//
			this.simpleButton_OK.Location = new System.Drawing.Point(511, 43);
			this.simpleButton_OK.TabIndex = 1;
			//
			//simpleButton_Cancel
			//
			this.simpleButton_Cancel.Location = new System.Drawing.Point(511, 86);
			this.simpleButton_Cancel.TabIndex = 2;
			//
			//AddressInformation1
			//
			this.AddressInformation1.Anchor = (AnchorStyles)(((AnchorStyles.Top | AnchorStyles.Bottom) | AnchorStyles.Left) | AnchorStyles.Right);
			this.AddressInformation1.Location = new System.Drawing.Point(12, 12);
			this.AddressInformation1.MinimumSize = new System.Drawing.Size(484, 162);
			this.AddressInformation1.Name = "AddressInformation1";
			this.AddressInformation1.Size = new System.Drawing.Size(484, 162);
			this.AddressInformation1.TabIndex = 0;
			//
			//EditForm_Address
			//
			this.AutoScaleDimensions = new System.Drawing.SizeF(6f, 13f);
			this.AutoScaleMode = AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(595, 184);
			this.Controls.Add(this.AddressInformation1);
			this.Name = "EditForm_Address";
			this.Text = "Address Information";
			this.Controls.SetChildIndex(this.simpleButton_Cancel, 0);
			this.Controls.SetChildIndex(this.simpleButton_OK, 0);
			this.Controls.SetChildIndex(this.AddressInformation1, 0);
			((System.ComponentModel.ISupportInitialize)this.DxErrorProvider1).EndInit();
			this.ResumeLayout(false);

		}
		private AddressInformation AddressInformation1;
	}
}

//=======================================================
//Service provided by Telerik (www.telerik.com)
//Conversion powered by NRefactory.
//Twitter: @telerik
//Facebook: facebook.com/telerik
//=======================================================
