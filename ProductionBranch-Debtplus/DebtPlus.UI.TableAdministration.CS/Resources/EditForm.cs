#region "Copyright 2000-2012 DebtPlus, L.L.C."
//{*******************************************************************}
//{                                                                   }
//{       DebtPlus Debt Management System                             }
//{                                                                   }
//{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
//{       ALL RIGHTS RESERVED                                         }
//{                                                                   }
//{   The entire contents of this file is protected by U.S. and       }
//{   International Copyright Laws. Unauthorized reproduction,        }
//{   reverse-engineering, and distribution of all or any portion of  }
//{   the code contained in this file is strictly prohibited and may  }
//{   result in severe civil and criminal penalties and will be       }
//{   prosecuted to the maximum extent possible under the law.        }
//{                                                                   }
//{   RESTRICTIONS                                                    }
//{                                                                   }
//{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
//{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
//{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
//{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
//{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
//{                                                                   }
//{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
//{   ADDITIONAL RESTRICTIONS.                                        }
//{                                                                   }
//{*******************************************************************}

#endregion "Copyright 2000-2012 DebtPlus, L.L.C."

using System;
using System.Linq;
using System.Windows.Forms;
using DebtPlus.LINQ;
using DevExpress.XtraEditors.Controls;

namespace DebtPlus.UI.TableAdministration.CS.Resources
{
    internal partial class EditForm : DebtPlus.UI.TableAdministration.CS.Templates.EditTemplateForm
    {
        private BusinessContext bc = null;
        private DebtPlus.LINQ.resource record = null;
        private System.Collections.Generic.List<resource_type> colResourceTypes = null;

        internal EditForm()
            : base()
        {
            InitializeComponent();
        }

        internal EditForm(DebtPlus.LINQ.BusinessContext bc, DebtPlus.LINQ.resource record)
            : this()
        {
            this.bc = bc;
            this.record = record;
            RegisterHandlers();
        }

        /// <summary>
        /// Register the event handlers
        /// </summary>
        private void RegisterHandlers()
        {
            Load += EditForm_Load;
            TextEdit_Name.EditValueChanged += Form_Changed;
            TextEdit_WWW.EditValueChanged += Form_Changed;
            TextEdit_WWW.Validating += TextEdit_WWW_Validating;
            ComboBoxEdit_Type.EditValueChanged += Form_Changed;
            ComboBoxEdit_Type.ButtonPressed += ComboBoxEdit_Type_ButtonPressed;
        }

        /// <summary>
        /// Remove the event handler registrations
        /// </summary>
        private void UnRegisterHandlers()
        {
            Load -= EditForm_Load;
            TextEdit_Name.EditValueChanged -= Form_Changed;
            TextEdit_WWW.EditValueChanged -= Form_Changed;
            TextEdit_WWW.Validating -= TextEdit_WWW_Validating;
            ComboBoxEdit_Type.EditValueChanged -= Form_Changed;
            ComboBoxEdit_Type.ButtonPressed -= ComboBoxEdit_Type_ButtonPressed;
        }

        /// <summary>
        /// Initialize the form with the proper control values
        /// </summary>
        private void EditForm_Load(object sender, EventArgs e)
        {
            UnRegisterHandlers();
            try
            {
                // Build a temporary list of offices so that we may check the items that we want.
                var lstOffices = new System.Collections.Generic.List<DebtPlus.Data.Controls.SortedCheckedListboxControlItem>();
                foreach (var office in DebtPlus.LINQ.Cache.office.getList())
                {
                    lstOffices.Add(new DebtPlus.Data.Controls.SortedCheckedListboxControlItem(office.Id, office.name, CheckState.Unchecked, office.ActiveFlag));
                }

                // If there is a resource record then read the list of offices and check the corresponding item
                // We don't bother with new records as they have no default office entry.
                if (record.Id > 0)
                {
                    foreach (var item in bc.resource_regions.Where(s => s.resource == record.Id))
                    {
                        var officeID = item.office;
                        var q = lstOffices.Find(s => (Int32)s.Value == officeID);
                        if (q != null)
                        {
                            q.CheckState = CheckState.Checked;
                        }
                    }
                }

                // Update the checkbox for the office list
                CheckedListBoxControl_Offices.Items.Clear();
                CheckedListBoxControl_Offices.SortOrder = SortOrder.Ascending;
                CheckedListBoxControl_Offices.Items.AddRange(lstOffices.ToArray());

                // Populate the other controls on the editing form
                TextEdit_Name.EditValue = record.Name;
                TextEdit_WWW.EditValue = record.WWW;
                AddressRecordControl1.EditValue = record.AddressID;
                EmailRecordControl1.EditValue = record.EmailID;
                TelephoneNumberRecordControl1.EditValue = record.TelephoneID;
                TelephoneNumberRecordControl2.EditValue = record.FAXID;

                // Populate the resource types control
                colResourceTypes = bc.resource_types.ToList();
                ComboBoxEdit_Type.Properties.Items.Clear();
                ComboBoxEdit_Type.Properties.Items.AddRange(colResourceTypes);
                ComboBoxEdit_Type.SelectedItem = colResourceTypes.Find(s => s.Id == record.Resource_type); ;

                simpleButton_OK.Enabled = !HasErrors();
            }
            finally
            {
                RegisterHandlers();
            }
        }

        /// <summary>
        /// Save the values for the controls when the OK button is pressed
        /// </summary>
        protected override void simpleButton_OK_Click(object sender, System.EventArgs e)
        {
            // Question the validity if there are no checked offices.
            bool hasOffice = CheckedListBoxControl_Offices.Items.OfType<DebtPlus.Data.Controls.SortedCheckedListboxControlItem>().Any(s => s.CheckState == CheckState.Checked);
            if (!hasOffice && DebtPlus.Data.Forms.MessageBox.Show("There are no offices chosen for this resource.\r\n\r\nThis means that the resource will never be printed for a client.\r\n\r\nAre you really sure that you want this condition?", "Are you sure?", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2) != System.Windows.Forms.DialogResult.Yes)
            {
                return;
            }

            // Supply the fields for the record
            record.AddressID = AddressRecordControl1.EditValue;
            record.EmailID = EmailRecordControl1.EditValue;
            record.TelephoneID = TelephoneNumberRecordControl1.EditValue;
            record.FAXID = TelephoneNumberRecordControl2.EditValue;
            record.Name = TextEdit_Name.Text.Trim();

            // The type should be defined. It is not allowed to be "null".
            var qType = ComboBoxEdit_Type.SelectedItem as resource_type;
            record.Resource_type = (qType == null) ? 0 : qType.Id;

            // We require an ID for the resource before we can update the offices. So, if this is "new" record then
            // we need to update the database first.
            if (record.Id < 1)
            {
                bc.resources.InsertOnSubmit(record);
                bc.SubmitChanges();
            }

            // Now, with the ID defined, we can add/delete the associated regions
            var colRegionList = bc.resource_regions.Where(s => s.resource == record.Id).ToList();
            foreach (DebtPlus.Data.Controls.SortedCheckedListboxControlItem item in CheckedListBoxControl_Offices.Items)
            {
                // Locate the office in the list of offices for this resource.
                var officeId = (Int32)item.Value;
                resource_region qOffice = colRegionList.Find(s => s.office == officeId);
                if (item.CheckState == CheckState.Checked)
                {
                    // There is at least one office checked for this resource
                    hasOffice = true;

                    // The item is checked. We need to add it if it is not found.
                    if (qOffice == null)
                    {
                        qOffice = DebtPlus.LINQ.Factory.Manufacture_resource_region();
                        qOffice.office = officeId;
                        qOffice.resource = record.Id;

                        bc.resource_regions.InsertOnSubmit(qOffice);
                    }
                }
                else
                {
                    // The item is not checked. Remove it if we have found a match to a checked item
                    if (qOffice != null)
                    {
                        bc.resource_regions.DeleteOnSubmit(qOffice);
                    }
                }
            }

            // Complete the normal processing
            base.simpleButton_OK_Click(sender, e);
        }

        /// <summary>
        /// Process a button press on the combobox.
        /// </summary>
        private void ComboBoxEdit_Type_ButtonPressed(object sender, ButtonPressedEventArgs e)
        {
            switch (Convert.ToString(e.Button.Tag))
            {
                case "new":
                    AddNewClass();
                    break;
            }
        }

        /// <summary>
        /// Request the new class label for the list
        /// </summary>
        private void AddNewClass()
        {
            // Create a new record of the type "resource_type"
            resource_type typeRecord = DebtPlus.LINQ.Factory.Manufacture_resource_type();

            // Edit the resource type record.
            using (var frm = new ResourceTypeForm(typeRecord))
            {
                if (frm.ShowDialog() != System.Windows.Forms.DialogResult.OK)
                {
                    return;
                }
            }

            // Add the resource type to the list
            bc.resource_types.InsertOnSubmit(typeRecord);
            bc.SubmitChanges();

            // Add the item to the list of resource types gathered so far and update the combo list
            colResourceTypes.Add(typeRecord);
            ComboBoxEdit_Type.Properties.Items.Add(typeRecord);

            // Change the type to the new record
            ComboBoxEdit_Type.SelectedItem = typeRecord;
        }

        /// <summary>
        /// Validate the name of for the web site.
        /// </summary>
        private void TextEdit_WWW_Validating(object sender, System.ComponentModel.CancelEventArgs e)
        {
            // An empty name is allowed
            if (string.IsNullOrWhiteSpace(TextEdit_WWW.Text))
            {
                TextEdit_WWW.EditValue = null;
                TextEdit_WWW.ErrorText = null;
                return;
            }

            // Otherwise, the name must map to a valid getURL. NO NOTES!
            try
            {
                Uri targetURI = new Uri(TextEdit_WWW.Text.Trim(), false);
                if (targetURI.IsAbsoluteUri && (targetURI.Scheme == Uri.UriSchemeHttp || targetURI.Scheme == Uri.UriSchemeHttps))
                {
                    TextEdit_WWW.ErrorText = null;
                    return;
                }
            }
            catch { }

            // The getURL is not a valid item.
            TextEdit_WWW.ErrorText = "The WWW getURL needs to be a valid web page, such as http://www.msn.com or https://www.msn.com";
            e.Cancel = true;
        }

        /// <summary>
        /// Enable the OK button when the form changes
        /// </summary>
        private void Form_Changed(object sender, EventArgs e)
        {
            simpleButton_OK.Enabled = !HasErrors();
        }

        /// <summary>
        /// Determine if the input form has any error conditions
        /// </summary>
        private bool HasErrors()
        {
            // We need a type of the resource. It is not legal to have it "missing".
            if (ComboBoxEdit_Type.SelectedItem == null)
            {
                return true;
            }

            // We need a name for the resource
            if (string.IsNullOrWhiteSpace(TextEdit_Name.Text))
            {
                return true;
            }

            // There can't be a problem with the URI
            if (!string.IsNullOrWhiteSpace(TextEdit_WWW.ErrorText))
            {
                return true;
            }

            // All others are optional.
            return false;
        }
    }
}