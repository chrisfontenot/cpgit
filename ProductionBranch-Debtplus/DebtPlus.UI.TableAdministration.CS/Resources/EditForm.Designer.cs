﻿namespace DebtPlus.UI.TableAdministration.CS.Resources
{
    partial class EditForm
    {
		//Form overrides dispose to clean up the component list.
		[System.Diagnostics.DebuggerNonUserCode]
		protected override void Dispose(bool disposing)
		{
			try 
            {
				if (disposing)
                {
                    if (components != null) components.Dispose();
				}
                components = null;
            }
    	    finally
            {
				base.Dispose(disposing);
			}
		}

		//Required by the Windows Form Designer
		private System.ComponentModel.IContainer components = null;

        //NOTE: The following procedure is required by the Windows Form Designer
        //It can be modified using the Windows Form Designer.
        //Do not modify it using the code editor.
        private DevExpress.XtraEditors.LabelControl Label1;
        private DevExpress.XtraEditors.LabelControl Label2;
        private DevExpress.XtraEditors.LabelControl Label3;
        private DevExpress.XtraEditors.LabelControl Label4;
        private DevExpress.XtraEditors.LabelControl Label5;
        private DevExpress.XtraEditors.LabelControl Label6;
        private DevExpress.XtraEditors.ComboBoxEdit ComboBoxEdit_Type;
        private DevExpress.XtraEditors.TextEdit TextEdit_Name;
        private DevExpress.XtraEditors.LabelControl Label7;
        private DevExpress.XtraEditors.TextEdit TextEdit_WWW;
        private DevExpress.XtraEditors.CheckedListBoxControl CheckedListBoxControl_Offices;
        private DebtPlus.Data.Controls.AddressRecordControl AddressRecordControl1;
        private DebtPlus.Data.Controls.TelephoneNumberRecordControl TelephoneNumberRecordControl1;
        private DebtPlus.Data.Controls.TelephoneNumberRecordControl TelephoneNumberRecordControl2;
        private DebtPlus.Data.Controls.EmailRecordControl EmailRecordControl1;
        private DevExpress.XtraEditors.LabelControl Label8;

        [System.Diagnostics.DebuggerStepThrough]
        private void InitializeComponent()
        {
            DevExpress.Utils.SerializableAppearanceObject SerializableAppearanceObject1 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject SerializableAppearanceObject2 = new DevExpress.Utils.SerializableAppearanceObject();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(EditForm));
            this.Label1 = new DevExpress.XtraEditors.LabelControl();
            this.Label2 = new DevExpress.XtraEditors.LabelControl();
            this.Label3 = new DevExpress.XtraEditors.LabelControl();
            this.Label4 = new DevExpress.XtraEditors.LabelControl();
            this.Label5 = new DevExpress.XtraEditors.LabelControl();
            this.Label6 = new DevExpress.XtraEditors.LabelControl();
            this.ComboBoxEdit_Type = new DevExpress.XtraEditors.ComboBoxEdit();
            this.TextEdit_Name = new DevExpress.XtraEditors.TextEdit();
            this.TextEdit_WWW = new DevExpress.XtraEditors.TextEdit();
            this.CheckedListBoxControl_Offices = new DevExpress.XtraEditors.CheckedListBoxControl();
            this.Label7 = new DevExpress.XtraEditors.LabelControl();
            this.Label8 = new DevExpress.XtraEditors.LabelControl();
            this.AddressRecordControl1 = new global::DebtPlus.Data.Controls.AddressRecordControl();
            this.TelephoneNumberRecordControl1 = new global::DebtPlus.Data.Controls.TelephoneNumberRecordControl();
            this.TelephoneNumberRecordControl2 = new global::DebtPlus.Data.Controls.TelephoneNumberRecordControl();
            this.EmailRecordControl1 = new global::DebtPlus.Data.Controls.EmailRecordControl();
            ((System.ComponentModel.ISupportInitialize)this.DxErrorProvider1).BeginInit();
            ((System.ComponentModel.ISupportInitialize)this.ComboBoxEdit_Type.Properties).BeginInit();
            ((System.ComponentModel.ISupportInitialize)this.TextEdit_Name.Properties).BeginInit();
            ((System.ComponentModel.ISupportInitialize)this.TextEdit_WWW.Properties).BeginInit();
            ((System.ComponentModel.ISupportInitialize)this.CheckedListBoxControl_Offices).BeginInit();
            ((System.ComponentModel.ISupportInitialize)this.AddressRecordControl1).BeginInit();
            ((System.ComponentModel.ISupportInitialize)this.TelephoneNumberRecordControl1).BeginInit();
            ((System.ComponentModel.ISupportInitialize)this.TelephoneNumberRecordControl2).BeginInit();
            ((System.ComponentModel.ISupportInitialize)this.EmailRecordControl1.Properties).BeginInit();
            this.SuspendLayout();
            //
            //simpleButton_Cancel
            //
            this.simpleButton_Cancel.Location = new System.Drawing.Point(484, 75);
            this.simpleButton_Cancel.TabIndex = 17;
            //
            //simpleButton_OK
            //
            this.simpleButton_OK.Location = new System.Drawing.Point(484, 43);
            this.simpleButton_OK.TabIndex = 16;
            //
            //DefaultLookAndFeel1
            //
            this.DefaultLookAndFeel1.LookAndFeel.SkinName = "McSkin";
            //
            //Label1
            //
            this.Label1.Location = new System.Drawing.Point(16, 17);
            this.Label1.Name = "Label1";
            this.Label1.Size = new System.Drawing.Size(24, 13);
            this.Label1.TabIndex = 0;
            this.Label1.Text = "Type";
            this.Label1.ToolTipController = this.ToolTipController1;
            //
            //Label2
            //
            this.Label2.Location = new System.Drawing.Point(16, 43);
            this.Label2.Name = "Label2";
            this.Label2.Size = new System.Drawing.Size(27, 13);
            this.Label2.TabIndex = 2;
            this.Label2.Text = "Name";
            this.Label2.ToolTipController = this.ToolTipController1;
            //
            //Label3
            //
            this.Label3.Location = new System.Drawing.Point(16, 69);
            this.Label3.Name = "Label3";
            this.Label3.Size = new System.Drawing.Size(39, 13);
            this.Label3.TabIndex = 4;
            this.Label3.Text = "Address";
            this.Label3.ToolTipController = this.ToolTipController1;
            //
            //Label4
            //
            this.Label4.Location = new System.Drawing.Point(16, 150);
            this.Label4.Name = "Label4";
            this.Label4.Size = new System.Drawing.Size(30, 13);
            this.Label4.TabIndex = 6;
            this.Label4.Text = "Phone";
            this.Label4.ToolTipController = this.ToolTipController1;
            //
            //Label5
            //
            this.Label5.Location = new System.Drawing.Point(16, 176);
            this.Label5.Name = "Label5";
            this.Label5.Size = new System.Drawing.Size(28, 13);
            this.Label5.TabIndex = 10;
            this.Label5.Text = "E-Mail";
            this.Label5.ToolTipController = this.ToolTipController1;
            //
            //Label6
            //
            this.Label6.Location = new System.Drawing.Point(16, 202);
            this.Label6.Name = "Label6";
            this.Label6.Size = new System.Drawing.Size(49, 13);
            this.Label6.TabIndex = 12;
            this.Label6.Text = "Web Page";
            this.Label6.ToolTipController = this.ToolTipController1;
            //
            //ComboBoxEdit_Type
            //
            this.ComboBoxEdit_Type.Anchor = (System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) | System.Windows.Forms.AnchorStyles.Right);
            this.ComboBoxEdit_Type.Location = new System.Drawing.Point(80, 17);
            this.ComboBoxEdit_Type.Name = "ComboBoxEdit_Type";
            this.ComboBoxEdit_Type.Properties.ActionButtonIndex = 1;
            this.ComboBoxEdit_Type.Properties.Sorted = true;
            this.ComboBoxEdit_Type.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Plus, "", -1, true, true, false, DevExpress.XtraEditors.ImageLocation.MiddleCenter, null, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), SerializableAppearanceObject1,"", "new", null, true),new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo, "", -1, true, true, false, DevExpress.XtraEditors.ImageLocation.MiddleCenter, null, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), SerializableAppearanceObject2,"", "popup", null, true)});
            this.ComboBoxEdit_Type.Size = new System.Drawing.Size(368, 20);
            this.ComboBoxEdit_Type.TabIndex = 1;
            this.ComboBoxEdit_Type.ToolTip = "Choose the type of the resource. Each resource belongs to a type that is used as " + "the heading for the printed report.";
            this.ComboBoxEdit_Type.ToolTipController = this.ToolTipController1;
            //
            //TextEdit_Name
            //
            this.TextEdit_Name.Anchor = (System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) | System.Windows.Forms.AnchorStyles.Right);
            this.TextEdit_Name.Location = new System.Drawing.Point(80, 42);
            this.TextEdit_Name.Name = "TextEdit_Name";
            this.TextEdit_Name.Properties.MaxLength = 50;
            this.TextEdit_Name.Size = new System.Drawing.Size(368, 20);
            this.TextEdit_Name.TabIndex = 3;
            this.TextEdit_Name.ToolTip = "Enter the name of the organization in this field";
            this.TextEdit_Name.ToolTipController = this.ToolTipController1;
            //
            //TextEdit_WWW
            //
            this.TextEdit_WWW.Anchor = (System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) | System.Windows.Forms.AnchorStyles.Right);
            this.TextEdit_WWW.Location = new System.Drawing.Point(80, 198);
            this.TextEdit_WWW.Name = "TextEdit_WWW";
            this.TextEdit_WWW.Properties.MaxLength = 50;
            this.TextEdit_WWW.Size = new System.Drawing.Size(368, 20);
            this.TextEdit_WWW.TabIndex = 13;
            this.TextEdit_WWW.ToolTip = "If the organization has web page, enter the page reference here. Omit the \"http://" + "\" and just put the \"www......\" page reference.";
            this.TextEdit_WWW.ToolTipController = this.ToolTipController1;
            //
            //CheckedListBoxControl_Offices
            //
            this.CheckedListBoxControl_Offices.Anchor = (System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) | System.Windows.Forms.AnchorStyles.Left) | System.Windows.Forms.AnchorStyles.Right);
            this.CheckedListBoxControl_Offices.CheckOnClick = true;
            this.CheckedListBoxControl_Offices.ItemHeight = 17;
            this.CheckedListBoxControl_Offices.Location = new System.Drawing.Point(80, 224);
            this.CheckedListBoxControl_Offices.Name = "CheckedListBoxControl_Offices";
            this.CheckedListBoxControl_Offices.Size = new System.Drawing.Size(368, 83);
            this.CheckedListBoxControl_Offices.SortOrder = System.Windows.Forms.SortOrder.Ascending;
            this.CheckedListBoxControl_Offices.TabIndex = 15;
            this.CheckedListBoxControl_Offices.ToolTip = "Choose the offices that you would consider are close to this organization. The in" + "formation is printed relative to these offices.";
            this.CheckedListBoxControl_Offices.ToolTipController = this.ToolTipController1;
            //
            //Label7
            //
            this.Label7.Location = new System.Drawing.Point(16, 224);
            this.Label7.Name = "Label7";
            this.Label7.Size = new System.Drawing.Size(42, 13);
            this.Label7.TabIndex = 14;
            this.Label7.Text = "Office(s)";
            this.Label7.ToolTipController = this.ToolTipController1;
            //
            //Label8
            //
            this.Label8.Anchor = (System.Windows.Forms.AnchorStyles)(System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right);
            this.Label8.Location = new System.Drawing.Point(248, 150);
            this.Label8.Name = "Label8";
            this.Label8.Size = new System.Drawing.Size(19, 13);
            this.Label8.TabIndex = 8;
            this.Label8.Text = "FAX";
            this.Label8.ToolTipController = this.ToolTipController1;
            //
            //AddressRecordControl1
            //
            this.AddressRecordControl1.Anchor = (System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) | System.Windows.Forms.AnchorStyles.Right);
            this.AddressRecordControl1.Location = new System.Drawing.Point(80, 68);
            this.AddressRecordControl1.Name = "AddressRecordControl1";
            this.AddressRecordControl1.Size = new System.Drawing.Size(368, 76);
            this.AddressRecordControl1.TabIndex = 5;
            //
            //TelephoneNumberRecordControl1
            //
            this.TelephoneNumberRecordControl1.Location = new System.Drawing.Point(80, 146);
            this.TelephoneNumberRecordControl1.Margin = new System.Windows.Forms.Padding(0);
            this.TelephoneNumberRecordControl1.Name = "TelephoneNumberRecordControl1";
            this.TelephoneNumberRecordControl1.Size = new System.Drawing.Size(150, 20);
            this.TelephoneNumberRecordControl1.TabIndex = 7;
            //
            //TelephoneNumberRecordControl2
            //
            this.TelephoneNumberRecordControl2.Anchor = (System.Windows.Forms.AnchorStyles)(System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right);
            this.TelephoneNumberRecordControl2.Location = new System.Drawing.Point(298, 146);
            this.TelephoneNumberRecordControl2.Margin = new System.Windows.Forms.Padding(0);
            this.TelephoneNumberRecordControl2.Name = "TelephoneNumberRecordControl2";
            this.TelephoneNumberRecordControl2.Size = new System.Drawing.Size(150, 20);
            this.TelephoneNumberRecordControl2.TabIndex = 9;
            //
            //EmailRecordControl1
            //
            this.EmailRecordControl1.Anchor = (System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) | System.Windows.Forms.AnchorStyles.Right);
            this.EmailRecordControl1.ClickedColor = System.Drawing.Color.Maroon;
            this.EmailRecordControl1.Location = new System.Drawing.Point(80, 173);
            this.EmailRecordControl1.Name = "EmailRecordControl1";
            this.EmailRecordControl1.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25f, System.Drawing.FontStyle.Underline);
            this.EmailRecordControl1.Properties.Appearance.ForeColor = System.Drawing.Color.Blue;
            this.EmailRecordControl1.Properties.Appearance.Options.UseFont = true;
            this.EmailRecordControl1.Properties.Appearance.Options.UseForeColor = true;
            this.EmailRecordControl1.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] { new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo) });
            this.EmailRecordControl1.Size = new System.Drawing.Size(368, 20);
            this.EmailRecordControl1.TabIndex = 11;
            //
            //EditForm
            //
            this.AutoScaleDimensions = new System.Drawing.SizeF(6f, 13f);
            this.ClientSize = new System.Drawing.Size(568, 316);
            this.Controls.Add(this.Label8);
            this.Controls.Add(this.AddressRecordControl1);
            this.Controls.Add(this.TelephoneNumberRecordControl2);
            this.Controls.Add(this.Label7);
            this.Controls.Add(this.EmailRecordControl1);
            this.Controls.Add(this.TelephoneNumberRecordControl1);
            this.Controls.Add(this.Label6);
            this.Controls.Add(this.Label5);
            this.Controls.Add(this.Label4);
            this.Controls.Add(this.Label3);
            this.Controls.Add(this.Label2);
            this.Controls.Add(this.Label1);
            this.Controls.Add(this.TextEdit_Name);
            this.Controls.Add(this.ComboBoxEdit_Type);
            this.Controls.Add(this.CheckedListBoxControl_Offices);
            this.Controls.Add(this.TextEdit_WWW);
            this.Name = "EditForm";
            this.Text = "Resource Information";
            this.Controls.SetChildIndex(this.TextEdit_WWW, 0);
            this.Controls.SetChildIndex(this.CheckedListBoxControl_Offices, 0);
            this.Controls.SetChildIndex(this.ComboBoxEdit_Type, 0);
            this.Controls.SetChildIndex(this.TextEdit_Name, 0);
            this.Controls.SetChildIndex(this.Label1, 0);
            this.Controls.SetChildIndex(this.Label2, 0);
            this.Controls.SetChildIndex(this.Label3, 0);
            this.Controls.SetChildIndex(this.Label4, 0);
            this.Controls.SetChildIndex(this.Label5, 0);
            this.Controls.SetChildIndex(this.Label6, 0);
            this.Controls.SetChildIndex(this.TelephoneNumberRecordControl1, 0);
            this.Controls.SetChildIndex(this.EmailRecordControl1, 0);
            this.Controls.SetChildIndex(this.Label7, 0);
            this.Controls.SetChildIndex(this.TelephoneNumberRecordControl2, 0);
            this.Controls.SetChildIndex(this.AddressRecordControl1, 0);
            this.Controls.SetChildIndex(this.Label8, 0);
            this.Controls.SetChildIndex(this.simpleButton_OK, 0);
            this.Controls.SetChildIndex(this.simpleButton_Cancel, 0);
            ((System.ComponentModel.ISupportInitialize)this.DxErrorProvider1).EndInit();
            ((System.ComponentModel.ISupportInitialize)this.ComboBoxEdit_Type.Properties).EndInit();
            ((System.ComponentModel.ISupportInitialize)this.TextEdit_Name.Properties).EndInit();
            ((System.ComponentModel.ISupportInitialize)this.TextEdit_WWW.Properties).EndInit();
            ((System.ComponentModel.ISupportInitialize)this.CheckedListBoxControl_Offices).EndInit();
            ((System.ComponentModel.ISupportInitialize)this.AddressRecordControl1).EndInit();
            ((System.ComponentModel.ISupportInitialize)this.TelephoneNumberRecordControl1).EndInit();
            ((System.ComponentModel.ISupportInitialize)this.TelephoneNumberRecordControl2).EndInit();
            ((System.ComponentModel.ISupportInitialize)this.EmailRecordControl1.Properties).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();
        }
    }
}