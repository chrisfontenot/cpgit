﻿namespace DebtPlus.UI.TableAdministration.CS.Resources
{
    partial class ResourceTypeForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.TextEdit_Label = new DevExpress.XtraEditors.TextEdit();
            this.Label2 = new DevExpress.XtraEditors.LabelControl();
            this.Label1 = new DevExpress.XtraEditors.LabelControl();
            this.SimpleButton_Cancel = new DevExpress.XtraEditors.SimpleButton();
            this.SimpleButton_OK = new DevExpress.XtraEditors.SimpleButton();
            this.TextEdit_Report = new DevExpress.XtraEditors.TextEdit();
            ((System.ComponentModel.ISupportInitialize)(this.DxErrorProvider1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TextEdit_Label.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TextEdit_Report.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // TextEdit_Label
            // 
            this.TextEdit_Label.EditValue = "";
            this.TextEdit_Label.Location = new System.Drawing.Point(85, 20);
            this.TextEdit_Label.Name = "TextEdit_Label";
            this.TextEdit_Label.Properties.MaxLength = 14;
            this.TextEdit_Label.Properties.NullText = "...Short Name here...";
            this.TextEdit_Label.Properties.ValidateOnEnterKey = true;
            this.TextEdit_Label.Size = new System.Drawing.Size(75, 20);
            this.TextEdit_Label.TabIndex = 7;
            this.TextEdit_Label.ToolTipController = this.ToolTipController1;
            // 
            // Label2
            // 
            this.Label2.Location = new System.Drawing.Point(13, 46);
            this.Label2.Name = "Label2";
            this.Label2.Size = new System.Drawing.Size(57, 13);
            this.Label2.TabIndex = 8;
            this.Label2.Text = "Description:";
            this.Label2.ToolTipController = this.ToolTipController1;
            // 
            // Label1
            // 
            this.Label1.Location = new System.Drawing.Point(13, 20);
            this.Label1.Name = "Label1";
            this.Label1.Size = new System.Drawing.Size(29, 13);
            this.Label1.TabIndex = 6;
            this.Label1.Text = "Label:";
            this.Label1.ToolTipController = this.ToolTipController1;
            // 
            // SimpleButton_Cancel
            // 
            this.SimpleButton_Cancel.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.SimpleButton_Cancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.SimpleButton_Cancel.Location = new System.Drawing.Point(236, 89);
            this.SimpleButton_Cancel.Name = "SimpleButton_Cancel";
            this.SimpleButton_Cancel.Size = new System.Drawing.Size(75, 25);
            this.SimpleButton_Cancel.TabIndex = 11;
            this.SimpleButton_Cancel.Text = "&Cancel";
            this.SimpleButton_Cancel.ToolTip = "Click here to cancel the update and return to the previous form";
            this.SimpleButton_Cancel.ToolTipController = this.ToolTipController1;
            // 
            // SimpleButton_OK
            // 
            this.SimpleButton_OK.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.SimpleButton_OK.Enabled = false;
            this.SimpleButton_OK.Location = new System.Drawing.Point(109, 89);
            this.SimpleButton_OK.Name = "SimpleButton_OK";
            this.SimpleButton_OK.Size = new System.Drawing.Size(75, 25);
            this.SimpleButton_OK.TabIndex = 10;
            this.SimpleButton_OK.Text = "&OK";
            this.SimpleButton_OK.ToolTip = "Click here to store the name in the database";
            this.SimpleButton_OK.ToolTipController = this.ToolTipController1;
            // 
            // TextEdit_Report
            // 
            this.TextEdit_Report.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.TextEdit_Report.Location = new System.Drawing.Point(85, 46);
            this.TextEdit_Report.Name = "TextEdit_Report";
            this.TextEdit_Report.Properties.MaxLength = 50;
            this.TextEdit_Report.Properties.NullText = "......Text for the report goes here.........";
            this.TextEdit_Report.Size = new System.Drawing.Size(312, 20);
            this.TextEdit_Report.TabIndex = 9;
            this.TextEdit_Report.ToolTip = "Enter the text for the label in this field";
            this.TextEdit_Report.ToolTipController = this.ToolTipController1;
            // 
            // ResourceTypeForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(410, 135);
            this.Controls.Add(this.TextEdit_Label);
            this.Controls.Add(this.Label2);
            this.Controls.Add(this.Label1);
            this.Controls.Add(this.SimpleButton_Cancel);
            this.Controls.Add(this.SimpleButton_OK);
            this.Controls.Add(this.TextEdit_Report);
            this.Name = "ResourceTypeForm";
            this.Text = "Resource Type";
            ((System.ComponentModel.ISupportInitialize)(this.DxErrorProvider1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TextEdit_Label.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TextEdit_Report.Properties)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        public DevExpress.XtraEditors.TextEdit TextEdit_Label;
        public DevExpress.XtraEditors.LabelControl Label2;
        public DevExpress.XtraEditors.LabelControl Label1;
        public DevExpress.XtraEditors.SimpleButton SimpleButton_Cancel;
        public DevExpress.XtraEditors.SimpleButton SimpleButton_OK;
        public DevExpress.XtraEditors.TextEdit TextEdit_Report;
    }
}