#region "Copyright 2000-2012 DebtPlus, L.L.C."
//{*******************************************************************}
//{                                                                   }
//{       DebtPlus Debt Management System                             }
//{                                                                   }
//{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
//{       ALL RIGHTS RESERVED                                         }
//{                                                                   }
//{   The entire contents of this file is protected by U.S. and       }
//{   International Copyright Laws. Unauthorized reproduction,        }
//{   reverse-engineering, and distribution of all or any portion of  }
//{   the code contained in this file is strictly prohibited and may  }
//{   result in severe civil and criminal penalties and will be       }
//{   prosecuted to the maximum extent possible under the law.        }
//{                                                                   }
//{   RESTRICTIONS                                                    }
//{                                                                   }
//{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
//{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
//{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
//{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
//{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
//{                                                                   }
//{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
//{   ADDITIONAL RESTRICTIONS.                                        }
//{                                                                   }
//{*******************************************************************}

#endregion "Copyright 2000-2012 DebtPlus, L.L.C."

using System;
using System.Windows.Forms;
using DebtPlus.Data.Forms;
using DebtPlus.LINQ;

namespace DebtPlus.UI.TableAdministration.CS.Resources
{
    internal partial class ResourceTypeForm : DebtPlusForm
    {
        private DebtPlus.LINQ.resource_type record = null;

        internal ResourceTypeForm()
            : base()
        {
            InitializeComponent();
        }

        internal ResourceTypeForm(resource_type record)
            : this()
        {
            this.record = record;
            RegisterHandlers();
        }

        /// <summary>
        /// Register the event handlers
        /// </summary>
        private void RegisterHandlers()
        {
            Load += ResourceTypeForm_Load;
            simpleButton_OK.Click += simpleButton_OK_Click;
            TextEdit_Report.EditValueChanged += Form_Changed;
            TextEdit_Label.EditValueChanged += Form_Changed;
        }

        /// <summary>
        /// Remove the event handler registrations
        /// </summary>
        private void UnRegisterHandlers()
        {
            Load -= ResourceTypeForm_Load;
            simpleButton_OK.Click -= simpleButton_OK_Click;
            TextEdit_Report.EditValueChanged -= Form_Changed;
            TextEdit_Label.EditValueChanged -= Form_Changed;
        }

        #region " Windows Form Designer generated code "

        //Form overrides dispose to clean up the component list.
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                if (components != null)
                {
                    components.Dispose();
                }
            }
            base.Dispose(disposing);
        }

        //Required by the Windows Form Designer

        private System.ComponentModel.IContainer components = null;

        //NOTE: The following procedure is required by the Windows Form Designer
        //It can be modified using the Windows Form Designer.
        //Do not modify it using the code editor.
        internal DevExpress.XtraEditors.SimpleButton simpleButton_OK;

        internal DevExpress.XtraEditors.SimpleButton simpleButton_Cancel;
        internal DevExpress.XtraEditors.LabelControl Label1;
        internal DevExpress.XtraEditors.LabelControl Label2;
        internal DevExpress.XtraEditors.TextEdit TextEdit_Report;

        internal DevExpress.XtraEditors.TextEdit TextEdit_Label;

        [System.Diagnostics.DebuggerStepThrough]
        private void InitializeComponent()
        {
            System.Resources.ResourceManager resources = new System.Resources.ResourceManager(typeof(ResourceTypeForm));
            this.TextEdit_Report = new DevExpress.XtraEditors.TextEdit();
            this.simpleButton_OK = new DevExpress.XtraEditors.SimpleButton();
            this.simpleButton_Cancel = new DevExpress.XtraEditors.SimpleButton();
            this.Label1 = new DevExpress.XtraEditors.LabelControl();
            this.Label2 = new DevExpress.XtraEditors.LabelControl();
            this.TextEdit_Label = new DevExpress.XtraEditors.TextEdit();

            ((System.ComponentModel.ISupportInitialize)this.TextEdit_Report.Properties).BeginInit();
            ((System.ComponentModel.ISupportInitialize)this.TextEdit_Label.Properties).BeginInit();
            this.SuspendLayout();

            //
            //TextEdit_Report
            //
            this.TextEdit_Report.Anchor = (AnchorStyles)(((AnchorStyles.Top | AnchorStyles.Bottom) | AnchorStyles.Left) | AnchorStyles.Right);
            this.TextEdit_Report.Location = new System.Drawing.Point(80, 52);
            this.TextEdit_Report.Name = "TextEdit_Report";
            //
            //TextEdit_Report.Properties
            //
            this.TextEdit_Report.Properties.MaxLength = 50;
            this.TextEdit_Report.Properties.NullText = "......Text for the report goes here.........";
            this.TextEdit_Report.Size = new System.Drawing.Size(312, 20);

            this.TextEdit_Report.TabIndex = 3;
            this.TextEdit_Report.ToolTip = "Enter the text for the label in this field";
            this.TextEdit_Report.ToolTipController = this.ToolTipController1;
            //
            //simpleButton_OK
            //
            this.simpleButton_OK.Anchor = AnchorStyles.Bottom;
            this.simpleButton_OK.Enabled = false;
            this.simpleButton_OK.Location = new System.Drawing.Point(104, 95);
            this.simpleButton_OK.Name = "simpleButton_OK";
            this.simpleButton_OK.Size = new System.Drawing.Size(75, 25);

            this.simpleButton_OK.TabIndex = 4;
            this.simpleButton_OK.Text = "&OK";
            this.simpleButton_OK.ToolTip = "Click here to store the name in the database";
            this.simpleButton_OK.ToolTipController = this.ToolTipController1;
            //
            //simpleButton_Cancel
            //
            this.simpleButton_Cancel.Anchor = AnchorStyles.Bottom;
            this.simpleButton_Cancel.DialogResult = DialogResult.Cancel;
            this.simpleButton_Cancel.Location = new System.Drawing.Point(231, 95);
            this.simpleButton_Cancel.Name = "simpleButton_Cancel";
            this.simpleButton_Cancel.Size = new System.Drawing.Size(75, 25);

            this.simpleButton_Cancel.TabIndex = 5;
            this.simpleButton_Cancel.Text = "&Cancel";
            this.simpleButton_Cancel.ToolTip = "Click here to cancel the update and return to the previous form";
            this.simpleButton_Cancel.ToolTipController = this.ToolTipController1;
            //
            //Label1
            //
            this.Label1.Location = new System.Drawing.Point(8, 26);
            this.Label1.Name = "Label1";
            this.Label1.Size = new System.Drawing.Size(64, 25);

            this.Label1.TabIndex = 0;
            this.Label1.Text = "Label:";
            this.Label1.ToolTipController = this.ToolTipController1;
            //
            //Label2
            //
            this.Label2.Location = new System.Drawing.Point(8, 52);
            this.Label2.Name = "Label2";
            this.Label2.Size = new System.Drawing.Size(64, 24);

            this.Label2.TabIndex = 2;
            this.Label2.Text = "Description:";
            this.Label2.ToolTipController = this.ToolTipController1;
            //
            //TextEdit_Label
            //
            this.TextEdit_Label.EditValue = "";
            this.TextEdit_Label.Location = new System.Drawing.Point(80, 26);
            this.TextEdit_Label.Name = "TextEdit_Label";
            //
            //TextEdit_Label.Properties
            //
            this.TextEdit_Label.Properties.MaxLength = 14;
            this.TextEdit_Label.Properties.NullText = "...Short Name here...";
            this.TextEdit_Label.Properties.ValidateOnEnterKey = true;
            this.TextEdit_Label.Size = new System.Drawing.Size(75, 20);

            this.TextEdit_Label.TabIndex = 1;
            this.TextEdit_Label.ToolTipController = this.ToolTipController1;
            //
            //ResourceTypeForm
            //
            this.AcceptButton = this.simpleButton_OK;
            this.AutoScaleBaseSize = new System.Drawing.Size(5, 14);
            this.CancelButton = this.simpleButton_Cancel;
            this.ClientSize = new System.Drawing.Size(410, 135);
            this.Controls.Add(this.TextEdit_Label);
            this.Controls.Add(this.Label2);
            this.Controls.Add(this.Label1);
            this.Controls.Add(this.simpleButton_Cancel);
            this.Controls.Add(this.simpleButton_OK);
            this.Controls.Add(this.TextEdit_Report);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.Icon = (System.Drawing.Icon)resources.GetObject("$this.Icon");
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "ResourceTypeForm";
            this.ToolTipController1.SetSuperTip(this, null);
            this.Text = "Resource Type";

            ((System.ComponentModel.ISupportInitialize)this.TextEdit_Report.Properties).EndInit();
            ((System.ComponentModel.ISupportInitialize)this.TextEdit_Label.Properties).EndInit();
            this.ResumeLayout(false);
        }

        #endregion " Windows Form Designer generated code "

        /// <summary>
        /// Handle the LOAD event for the form
        /// </summary>
        private void ResourceTypeForm_Load(object sender, EventArgs e)
        {
            UnRegisterHandlers();
            try
            {
                TextEdit_Label.EditValue = record.label;
                TextEdit_Report.EditValue = record.description;

                simpleButton_OK.Enabled = !HasErrors();
            }
            finally
            {
                RegisterHandlers();
            }
        }

        /// <summary>
        /// Process a change in the form controls
        /// </summary>
        private void Form_Changed(object sender, EventArgs e)
        {
            simpleButton_OK.Enabled = !HasErrors();
        }

        /// <summary>
        /// Determine if the input controls are sufficient for the record
        /// </summary>
        private bool HasErrors()
        {
            string errorString = "This field is required. Please enter a value";
            if (string.IsNullOrWhiteSpace(TextEdit_Report.Text))
            {
                TextEdit_Report.ErrorText = errorString;
            }
            else
            {
                TextEdit_Report.ErrorText = null;
            }

            if (string.IsNullOrWhiteSpace(TextEdit_Label.Text))
            {
                TextEdit_Label.ErrorText = errorString;
            }
            else
            {
                TextEdit_Label.ErrorText = null;
            }

            return TextEdit_Report.ErrorText != null || TextEdit_Label.ErrorText != null;
        }

        /// <summary>
        /// Process the OK button CLICK event
        /// </summary>
        private void simpleButton_OK_Click(object sender, EventArgs e)
        {
            record.label = DebtPlus.Utils.Nulls.v_String(TextEdit_Label.EditValue);
            record.description = DebtPlus.Utils.Nulls.v_String(TextEdit_Report.EditValue);

            // Successful completion of our function.
            DialogResult = DialogResult.OK;
        }
    }
}