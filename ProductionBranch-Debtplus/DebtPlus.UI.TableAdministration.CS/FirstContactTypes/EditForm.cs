﻿using System;
using DebtPlus.LINQ;

namespace DebtPlus.UI.TableAdministration.CS.FirstContactTypes
{
    internal partial class EditForm : Templates.EditTemplateForm
    {
        // Current record being edited
        private FirstContactType record;

        /// <summary>
        /// Initialize the new class instance
        /// </summary>
        internal EditForm()
            : base()
        {
            InitializeComponent();
        }

        /// <summary>
        /// Initialize the new class instance
        /// </summary>
        /// <param name="record"></param>
        internal EditForm(FirstContactType record)
            : this()
        {
            this.record = record;
            RegisterHandlers();
        }

        /// <summary>
        /// Add the event handlers to the collection
        /// </summary>
        private void RegisterHandlers()
        {
            Load += EditForm_Load;
        }

        /// <summary>
        /// Remove the event handlers from being processed
        /// </summary>
        private void UnRegisterHandlers()
        {
            Load -= EditForm_Load;
        }

        /// <summary>
        /// Process the FORM LOAD event
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void EditForm_Load(object sender, System.EventArgs e)
        {
            UnRegisterHandlers();
            try
            {
                simpleButton_OK.Enabled = !HasErrors();
            }
            finally
            {
                RegisterHandlers();
            }
        }

        /// <summary>
        /// Override the OK button to save the editing values into the record
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected override void simpleButton_OK_Click(object sender, EventArgs e)
        {
            base.simpleButton_OK_Click(sender, e);
        }

        private void FormChanged(object sender, EventArgs e)
        {
            simpleButton_OK.Enabled = !HasErrors();
        }

        /// <summary>
        /// Determine if the input form has error(s)
        /// </summary>
        /// <returns></returns>
        private bool HasErrors()
        {
            return false;
        }
    }
}