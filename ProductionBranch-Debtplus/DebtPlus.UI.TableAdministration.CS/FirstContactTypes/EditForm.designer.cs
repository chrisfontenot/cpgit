﻿namespace DebtPlus.UI.TableAdministration.CS.FirstContactTypes
{
    partial class EditForm
    {
		//Form overrides dispose to clean up the component list.
		[System.Diagnostics.DebuggerNonUserCode]
		protected override void Dispose(bool disposing)
		{
			try 
            {
				if (disposing)
                {
                    if (components != null) components.Dispose();
				}
                components = null;
            }
    	    finally
            {
				base.Dispose(disposing);
			}
		}

		//Required by the Windows Form Designer
		private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            ((System.ComponentModel.ISupportInitialize)(this.DxErrorProvider1)).BeginInit();
            this.SuspendLayout();
            // 
            // simpleButton_Cancel
            // 
            this.simpleButton_Cancel.Location = new System.Drawing.Point(325, 67);
            // 
            // simpleButton_OK
            // 
            this.simpleButton_OK.Location = new System.Drawing.Point(325, 35);
            // 
            // DefaultLookAndFeel1
            // 
            this.DefaultLookAndFeel1.LookAndFeel.SkinName = "McSkin";
            // 
            // EditForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(409, 119);
            this.Name = "EditForm";
            this.Text = "Message Item";
            ((System.ComponentModel.ISupportInitialize)(this.DxErrorProvider1)).EndInit();
            this.ResumeLayout(false);
        }


    }
}