﻿using System;
using System.Linq;
using DebtPlus.LINQ;
using DebtPlus.UI.Common;

namespace DebtPlus.UI.TableAdministration.CS.ProposalStatusTypes
{
    public partial class MainForm : Templates.MainForm
    {
        // The record collection being edited
        private System.Collections.Generic.List<ProposalStatusType> colRecords;

        private BusinessContext bc = null;

        public MainForm()
        {
            InitializeComponent();
            RegisterHandlers();
        }

        private void RegisterHandlers()
        {
            this.Load += MainForm_Load;
        }

        private void UnRegisterHandlers()
        {
            this.Load -= MainForm_Load;
        }

        private void MainForm_Load(object sender, EventArgs e)
        {
            UnRegisterHandlers();
            bc = new BusinessContext();
            try
            {
                using (var cm = new CursorManager())
                {
                    colRecords = bc.ProposalStatusTypes.ToList();
                    gridControl1.DataSource = colRecords;
                    gridView1.BestFitColumns();
                }
            }
            finally
            {
                RegisterHandlers();
            }
        }

        /// <summary>
        /// Update the record in the database
        /// </summary>
        /// <param name="obj"></param>
        protected override void UpdateRecord(object obj)
        {
            ProposalStatusType record = obj as ProposalStatusType;
            if (record == null)
            {
                return;
            }

            bool priorDefault = record.Default;
            using (var frm = new EditForm(record))
            {
                if (frm.ShowDialog() != System.Windows.Forms.DialogResult.OK)
                {
                    return;
                }
            }

            // Clear the default status on the existing records
            if (record.Default && !priorDefault)
            {
                foreach (var recDefault in colRecords.Where(s => s.Default))
                {
                    recDefault.Default = false;
                }
                record.Default = true;
            }

            // Find the record in the database
            bc.SubmitChanges();
            gridView1.RefreshData();
        }

        /// <summary>
        /// Create a new record for the database
        /// </summary>
        protected override void CreateRecord()
        {
            ProposalStatusType record = new ProposalStatusType();
            using (var frm = new EditForm(record))
            {
                if (frm.ShowDialog() != System.Windows.Forms.DialogResult.OK)
                {
                    return;
                }
            }

            // Clear the default status on the existing records
            if (record.Default)
            {
                foreach (var recDefault in colRecords.Where(s => s.Default))
                {
                    recDefault.Default = false;
                }
            }

            // Insert the new record in the database
            bc.ProposalStatusTypes.InsertOnSubmit(record);
            bc.SubmitChanges();

            // Add the new record to the collection and update the display
            colRecords.Add(record);
            gridView1.RefreshData();
        }

        /// <summary>
        /// Delete the current record from the database
        /// </summary>
        /// <param name="obj"></param>
        protected override void DeleteRecord(object obj)
        {
            ProposalStatusType record = obj as ProposalStatusType;
            if (record == null)
            {
                return;
            }

            if (DebtPlus.Data.Prompts.RequestConfirmation_Delete() != System.Windows.Forms.DialogResult.Yes)
            {
                return;
            }

            bc.ProposalStatusTypes.DeleteOnSubmit(record);
            bc.SubmitChanges();

            colRecords.Remove(record);
            gridView1.RefreshData();
        }
    }
}