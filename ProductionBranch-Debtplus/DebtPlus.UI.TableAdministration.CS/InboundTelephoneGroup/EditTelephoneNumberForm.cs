using System;

namespace DebtPlus.UI.TableAdministration.CS.InboundTelephoneGroup
{
    internal partial class EditTelephoneNumberForm : Templates.EditTemplateForm
    {
        private DebtPlus.LINQ.InboundTelephoneNumber record = null;

        internal EditTelephoneNumberForm()
            : base()
        {
            InitializeComponent();
        }

        internal EditTelephoneNumberForm(DebtPlus.LINQ.InboundTelephoneNumber record)
            : this()
        {
            this.record = record;
            RegisterHandlers();
        }

        private void RegisterHandlers()
        {
            Load += EditForm_Load;
            textEdit_Description.EditValueChanged += FormChanged;
            telephoneNumberControl_TelephoneNumber.EditValueChanged += FormChanged;
        }

        private void UnRegisterHandlers()
        {
            this.Load -= EditForm_Load;
            textEdit_Description.EditValueChanged -= FormChanged;
            telephoneNumberControl_TelephoneNumber.EditValueChanged -= FormChanged;
        }

        private void FormChanged(object sender, EventArgs e)
        {
            simpleButton_OK.Enabled = !HasErrors();
        }

        private bool HasErrors()
        {
            return string.IsNullOrWhiteSpace(textEdit_Description.Text);
        }

        private void EditForm_Load(object sender, EventArgs e)
        {
            UnRegisterHandlers();
            try
            {
                labelControl_Id.Text = (record.Id > 0) ? record.Id.ToString() : "NEW";
                textEdit_Description.EditValue = record.Description;
                checkEdit_ActiveFlag.Checked = record.ActiveFlag;
                checkEdit_Default.Checked = record.Default;

                // Parse the telephone number. If valid then update the phone number fields correctly.
                var phone = DebtPlus.LINQ.TelephoneNumber.TryParse(record.TelephoneNumber);
                if (phone != null)
                {
                    telephoneNumberControl_TelephoneNumber.SetCurrentValues(phone);
                }
                simpleButton_OK.Enabled = !HasErrors();
            }
            finally
            {
                RegisterHandlers();
            }
        }

        protected override void simpleButton_OK_Click(object sender, EventArgs e)
        {
            base.simpleButton_OK_Click(sender, e);

            // Retrieve the current values from the controls for the groupRecord.
            record.Description = DebtPlus.Utils.Nulls.v_String(textEdit_Description.EditValue);
            record.ActiveFlag = checkEdit_ActiveFlag.Checked;
            record.Default = checkEdit_Default.Checked;

            // Retrieve the telephone number fields from the control and format them as a string.
            var phone = telephoneNumberControl_TelephoneNumber.GetCurrentValues();
            if (phone != null && !string.IsNullOrEmpty(phone.Number))
            {
                record.TelephoneNumber = phone.ToString();
            }
        }
    }
}