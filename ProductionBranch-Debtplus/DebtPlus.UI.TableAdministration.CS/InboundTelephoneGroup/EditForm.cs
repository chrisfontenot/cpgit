﻿using System;
using System.Windows.Forms;
using DebtPlus.LINQ;

namespace DebtPlus.UI.TableAdministration.CS.InboundTelephoneGroup
{
    internal partial class EditForm : Templates.EditTemplateForm
    {
        private DebtPlus.LINQ.InBoundTelephoneNumberGroup groupRecord;

        internal EditForm()
            : base()
        {
            InitializeComponent();
        }

        internal EditForm(DebtPlus.LINQ.InBoundTelephoneNumberGroup record)
            : this()
        {
            this.groupRecord = record;
            RegisterHandlers();
        }

        /// <summary>
        /// Register the event handlers
        /// </summary>
        private void RegisterHandlers()
        {
            barButtonItem_Add.ItemClick += barButtonItem_Add_ItemClick;
            barButtonItem_Change.ItemClick += barButtonItem_Change_ItemClick;
            barButtonItem_Delete.ItemClick += barButtonItem_Delete_ItemClick;
            gridView1.DoubleClick += gridView1_DoubleClick;
            gridView1.MouseDown += gridView1_MouseDown;
            Load += EditForm_Load;
            TextEdit_description.EditValueChanged += FormChanged;
            checkEdit_ActiveFlag.EditValueChanged += FormChanged;
            CheckEdit_default.EditValueChanged += FormChanged;
            lookUpEdit1.EditValueChanging += DebtPlus.Data.Validation.LookUpEdit_ActiveTest;
        }

        /// <summary>
        /// Remove the event handler registration
        /// </summary>
        private void UnRegisterHandlers()
        {
            barButtonItem_Add.ItemClick -= barButtonItem_Add_ItemClick;
            barButtonItem_Change.ItemClick -= barButtonItem_Change_ItemClick;
            barButtonItem_Delete.ItemClick -= barButtonItem_Delete_ItemClick;
            gridView1.DoubleClick -= gridView1_DoubleClick;
            gridView1.MouseDown -= gridView1_MouseDown;
            Load -= EditForm_Load;
            TextEdit_description.EditValueChanged -= FormChanged;
            checkEdit_ActiveFlag.EditValueChanged -= FormChanged;
            CheckEdit_default.EditValueChanged -= FormChanged;
            lookUpEdit1.EditValueChanging -= DebtPlus.Data.Validation.LookUpEdit_ActiveTest;
        }

        private void EditForm_Load(object sender, EventArgs e)
        {
            UnRegisterHandlers();

            try
            {
                // Load the list of referral sources into the control
                lookUpEdit1.Properties.DataSource = DebtPlus.LINQ.Cache.referred_by.getList();

                LabelControl_id.Text = groupRecord.Id <= 0 ? "NEW" : groupRecord.Id.ToString();
                TextEdit_description.Text = groupRecord.Description.ToString().Trim();
                CheckEdit_default.Checked = groupRecord.Default;
                checkEdit_ActiveFlag.Checked = groupRecord.ActiveFlag;
                lookUpEdit1.EditValue = groupRecord.referred_by;

                // Set the data source for the list of records.
                gridControl1.DataSource = groupRecord.InboundTelephoneNumbers;
            }
            finally
            {
                RegisterHandlers();
            }
            simpleButton_OK.Enabled = !HasErrors();
        }

        private void FormChanged(object sender, EventArgs e)
        {
            simpleButton_OK.Enabled = !HasErrors();
        }

        protected bool HasErrors()
        {
            return string.IsNullOrWhiteSpace(TextEdit_description.Text);
        }

        protected override void simpleButton_OK_Click(object sender, EventArgs e)
        {
            base.simpleButton_OK_Click(sender, e);

            groupRecord.Description = TextEdit_description.Text.Trim();
            groupRecord.Default = CheckEdit_default.Checked;
            groupRecord.ActiveFlag = checkEdit_ActiveFlag.Checked;
            groupRecord.referred_by = DebtPlus.Utils.Nulls.v_Int32(lookUpEdit1.EditValue);
        }

        /// <summary>
        /// Double click event on the list
        /// </summary>
        private void gridView1_MouseDown(object sender, MouseEventArgs e)
        {
            // Find the groupRecord to be edited from the list
            if (e.Button != System.Windows.Forms.MouseButtons.Right)
            {
                return;
            }

            DevExpress.XtraGrid.Views.Grid.ViewInfo.GridHitInfo hi = gridView1.CalcHitInfo((gridControl1.PointToClient(System.Windows.Forms.Control.MousePosition)));
            System.Int32 RowHandle = hi.RowHandle;
            gridView1.FocusedRowHandle = RowHandle;

            // Find the groupRecord for this row
            object obj = null;
            if (RowHandle >= 0)
            {
                obj = gridView1.GetRow(RowHandle);
            }

            // If the row is defined then edit the row.
            if (obj != null)
            {
                barButtonItem_Change.Enabled = true;
                barButtonItem_Delete.Enabled = true;
            }
            else
            {
                barButtonItem_Change.Enabled = false;
                barButtonItem_Delete.Enabled = false;
            }

            popupMenu1.ShowPopup(System.Windows.Forms.Control.MousePosition);
        }

        /// <summary>
        /// Double click event on the list
        /// </summary>
        private void gridView1_DoubleClick(object sender, System.EventArgs e)
        {
            DevExpress.XtraGrid.Views.Grid.ViewInfo.GridHitInfo hi = gridView1.CalcHitInfo((gridControl1.PointToClient(System.Windows.Forms.Control.MousePosition)));
            System.Int32 RowHandle = hi.RowHandle;

            // Find the groupRecord to be edited from the list
            if (RowHandle >= 0)
            {
                gridView1.FocusedRowHandle = RowHandle;
                Object obj = gridView1.GetRow(RowHandle);
                if (obj != null)
                {
                    UpdateRecord(obj);
                }
            }
        }

        /// <summary>
        /// Edit -> Create menu item clicked
        /// </summary>
        private void barButtonItem_Add_ItemClick(System.Object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            CreateRecord();
        }

        /// <summary>
        /// Edit -> Edit menu item clicked
        /// </summary>
        private void barButtonItem_Change_ItemClick(System.Object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            System.Int32 RowHandle = gridView1.FocusedRowHandle;

            if (RowHandle >= 0)
            {
                gridView1.FocusedRowHandle = RowHandle;
                object obj = gridView1.GetRow(RowHandle);
                if (obj != null)
                {
                    UpdateRecord(obj);
                }
            }
        }

        /// <summary>
        /// Edit -> Delete menu item clicked
        /// </summary>
        private void barButtonItem_Delete_ItemClick(System.Object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            System.Int32 RowHandle = gridView1.FocusedRowHandle;

            if (RowHandle >= 0)
            {
                gridView1.FocusedRowHandle = RowHandle;
                object obj = gridView1.GetRow(RowHandle);
                if (obj != null)
                {
                    DeleteRecord(obj);
                }
            }
        }

        // Routines to process the CUD operations
        protected void CreateRecord()
        {
            var record = DebtPlus.LINQ.Factory.Manufacture_InboundTelephoneNumber();
            using (var frm = new EditTelephoneNumberForm(record))
            {
                if (frm.ShowDialog() != System.Windows.Forms.DialogResult.OK)
                {
                    return;
                }
            }

            // Add the groupRecord to the list. It will be updated when the top level groupRecord is updated.
            groupRecord.InboundTelephoneNumbers.Add(record);
            gridControl1.DataSource = groupRecord.InboundTelephoneNumbers.GetNewBindingList();
            gridView1.RefreshData();
        }

        protected void UpdateRecord(object obj)
        {
            // Find the record
            var record = obj as InboundTelephoneNumber;
            if (record == null)
            {
                return;
            }

            // Edit the values
            using (var frm = new EditTelephoneNumberForm(record))
            {
                if (frm.ShowDialog() != System.Windows.Forms.DialogResult.OK)
                {
                    return;
                }
            }

            // Refresh the grid. The record is already updated in place.
            gridControl1.DataSource = groupRecord.InboundTelephoneNumbers.GetNewBindingList();
            gridView1.RefreshData();
        }

        protected void DeleteRecord(object obj)
        {
            // Find the record
            var record = obj as InboundTelephoneNumber;
            if (record == null)
            {
                return;
            }

            // Ask if it is desired to delete the record.
            if (DebtPlus.Data.Prompts.RequestConfirmation_Delete() != System.Windows.Forms.DialogResult.Yes)
            {
                return;
            }

            // Remove the record from the system
            groupRecord.InboundTelephoneNumbers.Remove(record);
            gridControl1.DataSource = groupRecord.InboundTelephoneNumbers.GetNewBindingList();
            gridView1.RefreshData();
        }
    }
}