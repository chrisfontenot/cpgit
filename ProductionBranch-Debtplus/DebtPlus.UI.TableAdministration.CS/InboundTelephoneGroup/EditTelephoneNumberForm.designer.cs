namespace DebtPlus.UI.TableAdministration.CS.InboundTelephoneGroup
{
    partial class EditTelephoneNumberForm
	{
		//Form overrides dispose to clean up the component list.
		[System.Diagnostics.DebuggerNonUserCode]
		protected override void Dispose(bool disposing)
		{
			try 
            {
				if (disposing)
                {
                    if (components != null) components.Dispose();
				}
                components = null;
            }
    	    finally
            {
				base.Dispose(disposing);
			}
		}

		//Required by the Windows Form Designer
		private System.ComponentModel.IContainer components = null;

		//NOTE: The following procedure is required by the Windows Form Designer
		//It can be modified using the Windows Form Designer.  
		//Do not modify it using the code editor.
		[System.Diagnostics.DebuggerStepThrough]
		private void InitializeComponent()
		{
            this.checkEdit_Default = new DevExpress.XtraEditors.CheckEdit();
            this.checkEdit_ActiveFlag = new DevExpress.XtraEditors.CheckEdit();
            this.textEdit_Description = new DevExpress.XtraEditors.TextEdit();
            this.labelControl1 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl2 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl3 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl_Id = new DevExpress.XtraEditors.LabelControl();
            this.telephoneNumberControl_TelephoneNumber = new DebtPlus.Data.Controls.TelephoneNumberControl();
            ((System.ComponentModel.ISupportInitialize)(this.DxErrorProvider1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit_Default.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit_ActiveFlag.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit_Description.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.telephoneNumberControl_TelephoneNumber)).BeginInit();
            this.SuspendLayout();
            // 
            // simpleButton_OK
            // 
            this.simpleButton_OK.Location = new System.Drawing.Point(306, 12);
            // 
            // simpleButton_Cancel
            // 
            this.simpleButton_Cancel.Location = new System.Drawing.Point(306, 44);
            // 
            // DefaultLookAndFeel1
            // 
            this.DefaultLookAndFeel1.LookAndFeel.SkinName = "McSkin";
            // 
            // checkEdit_Default
            // 
            this.checkEdit_Default.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.checkEdit_Default.Location = new System.Drawing.Point(10, 85);
            this.checkEdit_Default.Name = "checkEdit_Default";
            this.checkEdit_Default.Properties.Caption = "Default";
            this.checkEdit_Default.Size = new System.Drawing.Size(75, 20);
            this.checkEdit_Default.TabIndex = 20;
            // 
            // checkEdit_ActiveFlag
            // 
            this.checkEdit_ActiveFlag.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.checkEdit_ActiveFlag.Location = new System.Drawing.Point(109, 85);
            this.checkEdit_ActiveFlag.Name = "checkEdit_ActiveFlag";
            this.checkEdit_ActiveFlag.Properties.Caption = "Active";
            this.checkEdit_ActiveFlag.Size = new System.Drawing.Size(75, 20);
            this.checkEdit_ActiveFlag.TabIndex = 21;
            // 
            // textEdit_Description
            // 
            this.textEdit_Description.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.textEdit_Description.Location = new System.Drawing.Point(96, 31);
            this.textEdit_Description.Name = "textEdit_Description";
            this.textEdit_Description.Properties.MaxLength = 50;
            this.textEdit_Description.Size = new System.Drawing.Size(190, 20);
            this.textEdit_Description.TabIndex = 22;
            // 
            // labelControl1
            // 
            this.labelControl1.Location = new System.Drawing.Point(12, 34);
            this.labelControl1.Name = "labelControl1";
            this.labelControl1.Size = new System.Drawing.Size(53, 13);
            this.labelControl1.TabIndex = 23;
            this.labelControl1.Text = "Description";
            // 
            // labelControl2
            // 
            this.labelControl2.Location = new System.Drawing.Point(12, 60);
            this.labelControl2.Name = "labelControl2";
            this.labelControl2.Size = new System.Drawing.Size(66, 13);
            this.labelControl2.TabIndex = 25;
            this.labelControl2.Text = "Telephone No";
            // 
            // labelControl3
            // 
            this.labelControl3.Location = new System.Drawing.Point(12, 12);
            this.labelControl3.Name = "labelControl3";
            this.labelControl3.Size = new System.Drawing.Size(48, 13);
            this.labelControl3.TabIndex = 27;
            this.labelControl3.Text = "Record ID";
            // 
            // labelControl_Id
            // 
            this.labelControl_Id.Location = new System.Drawing.Point(96, 12);
            this.labelControl_Id.Name = "labelControl_Id";
            this.labelControl_Id.Size = new System.Drawing.Size(0, 13);
            this.labelControl_Id.TabIndex = 28;
            // 
            // telephoneNumberControl_TelephoneNumber
            // 
            this.telephoneNumberControl_TelephoneNumber.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.telephoneNumberControl_TelephoneNumber.ErrorIcon = null;
            this.telephoneNumberControl_TelephoneNumber.ErrorText = "";
            this.telephoneNumberControl_TelephoneNumber.Location = new System.Drawing.Point(96, 57);
            this.telephoneNumberControl_TelephoneNumber.Name = "telephoneNumberControl_TelephoneNumber";
            this.telephoneNumberControl_TelephoneNumber.Size = new System.Drawing.Size(190, 20);
            this.telephoneNumberControl_TelephoneNumber.TabIndex = 29;
            // 
            // EditTelephoneNumberForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(390, 114);
            this.Controls.Add(this.telephoneNumberControl_TelephoneNumber);
            this.Controls.Add(this.labelControl_Id);
            this.Controls.Add(this.labelControl3);
            this.Controls.Add(this.labelControl2);
            this.Controls.Add(this.labelControl1);
            this.Controls.Add(this.textEdit_Description);
            this.Controls.Add(this.checkEdit_ActiveFlag);
            this.Controls.Add(this.checkEdit_Default);
            this.Name = "EditTelephoneNumberForm";
            this.Text = "Telephone Number";
            this.Controls.SetChildIndex(this.simpleButton_OK, 0);
            this.Controls.SetChildIndex(this.simpleButton_Cancel, 0);
            this.Controls.SetChildIndex(this.checkEdit_Default, 0);
            this.Controls.SetChildIndex(this.checkEdit_ActiveFlag, 0);
            this.Controls.SetChildIndex(this.textEdit_Description, 0);
            this.Controls.SetChildIndex(this.labelControl1, 0);
            this.Controls.SetChildIndex(this.labelControl2, 0);
            this.Controls.SetChildIndex(this.labelControl3, 0);
            this.Controls.SetChildIndex(this.labelControl_Id, 0);
            this.Controls.SetChildIndex(this.telephoneNumberControl_TelephoneNumber, 0);
            ((System.ComponentModel.ISupportInitialize)(this.DxErrorProvider1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit_Default.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit_ActiveFlag.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit_Description.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.telephoneNumberControl_TelephoneNumber)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

		}

        private DevExpress.XtraEditors.CheckEdit checkEdit_Default;
        private DevExpress.XtraEditors.CheckEdit checkEdit_ActiveFlag;
        private DevExpress.XtraEditors.TextEdit textEdit_Description;
        private DevExpress.XtraEditors.LabelControl labelControl1;
        private DevExpress.XtraEditors.LabelControl labelControl2;
        private DevExpress.XtraEditors.LabelControl labelControl3;
        private DevExpress.XtraEditors.LabelControl labelControl_Id;
        private Data.Controls.TelephoneNumberControl telephoneNumberControl_TelephoneNumber;
	}
}
