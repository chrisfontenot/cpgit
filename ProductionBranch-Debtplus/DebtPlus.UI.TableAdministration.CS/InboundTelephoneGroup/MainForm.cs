﻿using System;
using System.Linq;
using DebtPlus.UI.Common;

namespace DebtPlus.UI.TableAdministration.CS.InboundTelephoneGroup
{
    public partial class MainForm : Templates.MainForm
    {
        // The groupRecord collection being edited
        private System.Collections.Generic.List<DebtPlus.LINQ.InBoundTelephoneNumberGroup> colRecords;

        private DebtPlus.LINQ.BusinessContext bc = null;

        public MainForm()
        {
            InitializeComponent();

            // Allocate a new business context to retrieve the records
            bc = new DebtPlus.LINQ.BusinessContext();
            var dl = new System.Data.Linq.DataLoadOptions();
            dl.LoadWith<DebtPlus.LINQ.InBoundTelephoneNumberGroup>(s => s.InboundTelephoneNumbers);
            bc.LoadOptions = dl;
            bc.DeferredLoadingEnabled = false;

            RegisterHandlers();
        }

        private void RegisterHandlers()
        {
            this.Load += MainForm_Load;
        }

        private void UnRegisterHandlers()
        {
            this.Load -= MainForm_Load;
        }

        private void MainForm_Load(object sender, EventArgs e)
        {
            UnRegisterHandlers();
            try
            {
                using (var cm = new CursorManager())
                {
                    colRecords = bc.InBoundTelephoneNumberGroups.ToList();
                    gridControl1.DataSource = colRecords;
                    gridView1.BestFitColumns();
                }
            }
            finally
            {
                RegisterHandlers();
            }
        }

        /// <summary>
        /// Update the groupRecord in the database
        /// </summary>
        /// <param name="obj"></param>
        protected override void UpdateRecord(object obj)
        {
            // Find the groupRecord to be edited.
            DebtPlus.LINQ.InBoundTelephoneNumberGroup record = obj as DebtPlus.LINQ.InBoundTelephoneNumberGroup;
            if (record == null)
            {
                return;
            }

            // Edit the new groupRecord.
            bool priorDefault = record.Default;
            using (var frm = new EditForm(record))
            {
                if (frm.ShowDialog() != System.Windows.Forms.DialogResult.OK)
                {
                    return;
                }
            }

            // Remove the previous default setting if this is the new default item.
            if (record.Default && !priorDefault)
            {
                foreach (var rec in colRecords.Where(s => s.Default))
                {
                    rec.Default = false;
                }
                record.Default = true;
            }

            try
            {
                bc.SubmitChanges();
                gridView1.RefreshData();
            }
            catch (System.Data.SqlClient.SqlException ex)
            {
                DebtPlus.UI.Common.ErrorHandling.HandleErrors(ex, "Error submitting changes to the database");
            }
        }

        /// <summary>
        /// Create a new groupRecord for the database
        /// </summary>
        protected override void CreateRecord()
        {
            var record = DebtPlus.LINQ.Factory.Manufacture_InboundTelephoneNumberGroup();
            using (var frm = new EditForm(record))
            {
                if (frm.ShowDialog() != System.Windows.Forms.DialogResult.OK)
                {
                    return;
                }
            }

            // If this is the default groupRecord then remove the previous one.
            if (record.Default)
            {
                foreach (var rec in colRecords.Where(s => s.Default))
                {
                    rec.Default = false;
                }
            }

            // Add the new groupRecord
            bc.InBoundTelephoneNumberGroups.InsertOnSubmit(record);
            try
            {
                bc.SubmitChanges();

                colRecords.Add(record);
                gridView1.RefreshData();
            }
            catch (System.Data.SqlClient.SqlException ex)
            {
                DebtPlus.UI.Common.ErrorHandling.HandleErrors(ex, "Error submitting changes to the database");
            }
        }

        /// <summary>
        /// Delete the current groupRecord from the database
        /// </summary>
        /// <param name="obj"></param>
        protected override void DeleteRecord(object obj)
        {
            var record = obj as DebtPlus.LINQ.InBoundTelephoneNumberGroup;
            if (record == null)
            {
                return;
            }

            if (DebtPlus.Data.Prompts.RequestConfirmation_Delete() != System.Windows.Forms.DialogResult.Yes)
            {
                return;
            }

            bc.InBoundTelephoneNumberGroups.DeleteOnSubmit(record);
            try
            {
                bc.SubmitChanges();

                colRecords.Remove(record);
                gridView1.RefreshData();
            }
            catch (System.Data.SqlClient.SqlException ex)
            {
                DebtPlus.UI.Common.ErrorHandling.HandleErrors(ex, "Error submitting changes to the database");
            }
        }
    }
}