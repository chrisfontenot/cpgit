﻿using System;
using DebtPlus.LINQ;

namespace DebtPlus.UI.TableAdministration.CS.AttributeTypes
{
    internal partial class EditForm : Templates.EditTemplateForm
    {
        private AttributeType record;

        internal EditForm()
            : base()
        {
            InitializeComponent();
        }

        internal EditForm(AttributeType record)
            : this()
        {
            this.record = record;
            RegisterHandlers();
        }

        private void RegisterHandlers()
        {
            this.Load += EditForm_Load;
            this.TextEdit_Attribute.TextChanged += Form_Changed;
        }

        private void UnRegisterHandlers()
        {
            this.Load -= EditForm_Load;
            this.TextEdit_Attribute.TextChanged -= Form_Changed;
        }

        private void EditForm_Load(object sender, EventArgs e)
        {
            UnRegisterHandlers();
            try
            {
                LabelControl_ID.Text = String.Empty;
                if (record.Id > 0)
                {
                    LabelControl_ID.Text = record.Id.ToString();
                }

                // Find the selected record for the grouping value
                ComboBoxEdit_Grouping.Text = record.Grouping.Trim().ToUpper();
                ComboBoxEdit_Grouping.SelectedItem = ComboBoxEdit_Grouping.Text;

                TextEdit_Attribute.EditValue = record.Attribute;
                TextEdit_HudLanguage.EditValue = record.HUDLanguage;
                TextEdit_HUDServiceType.EditValue = record.HUDServiceType;
                CheckEdit_Default.Checked = record.Default;
                CheckEdit_ActiveFlag.Checked = record.ActiveFlag;

                // Find the list of known languages
                System.Globalization.CultureInfo[] LanguageLists = System.Globalization.CultureInfo.GetCultures(System.Globalization.CultureTypes.AllCultures);
                LookUpEdit_LanguageID.Properties.DataSource = LanguageLists;
                LookUpEdit_LanguageID.Properties.PopupWidth = LookUpEdit_LanguageID.Width + 200;
                LookUpEdit_LanguageID.EditValue = record.LanguageID;
            }
            finally
            {
                RegisterHandlers();
            }
            simpleButton_OK.Enabled = TextEdit_Attribute.Text != String.Empty;
        }

        private void Form_Changed(object sender, EventArgs e)
        {
            simpleButton_OK.Enabled = !string.IsNullOrEmpty(TextEdit_Attribute.Text);
        }

        protected override void simpleButton_OK_Click(object sender, EventArgs e)
        {
            // Retrieve the values from the controls when the record is accepted.
            record.Grouping = ComboBoxEdit_Grouping.Text.Trim();
            record.Attribute = DebtPlus.Utils.Nulls.v_String(TextEdit_Attribute.EditValue);
            record.HUDLanguage = DebtPlus.Utils.Nulls.v_String(TextEdit_HudLanguage.EditValue);
            record.HUDServiceType = DebtPlus.Utils.Nulls.v_String(TextEdit_HUDServiceType.EditValue);
            record.LanguageID = DebtPlus.Utils.Nulls.v_String(LookUpEdit_LanguageID.EditValue);
            record.Default = CheckEdit_Default.Checked;
            record.ActiveFlag = CheckEdit_ActiveFlag.Checked;

            base.simpleButton_OK_Click(sender, e);
        }
    }
}