﻿using System;
using System.Linq;
using DebtPlus.LINQ;

namespace DebtPlus.UI.TableAdministration.CS.AttributeTypes
{
    public partial class MainForm : Templates.MainForm
    {
        /// <summary>
        /// List of the records to be edited in the program
        /// </summary>
        private System.Collections.Generic.List<AttributeType> colRecords;
        private BusinessContext bc = null;

        /// <summary>
        /// Initialize the new class
        /// </summary>
        public MainForm()
            : base()
        {
            InitializeComponent();
            RegisterHandlers();
        }

        private void RegisterHandlers()
        {
            Load += MainForm_Load;
        }

        private void UnRegisterHandlers()
        {
            Load -= MainForm_Load;
        }

        /// <summary>
        /// Process the load event on the form
        /// </summary>
        private void MainForm_Load(object sender, System.EventArgs e)
        {
            bc = new BusinessContext();
            try
            {
                // Retrieve the list of menu items
                colRecords = bc.AttributeTypes.ToList();
                gridControl1.DataSource = colRecords;
            }
            catch (Exception ex)
            {
                DebtPlus.UI.Common.ErrorHandling.HandleErrors(ex, "Error retrieving database information");
            }
        }

        /// <summary>
        /// Create the new record
        /// </summary>
        protected override void CreateRecord()
        {
            // Allocate the new record to be inserted.
            var record = DebtPlus.LINQ.Factory.Manufacture_AttributeType();

            // Edit the new record. If OK then attempt to insert the record.
            using (EditForm frm = new EditForm(record))
            {
                if (frm.ShowDialog() != System.Windows.Forms.DialogResult.OK)
                {
                    return;
                }

                // If this is the new default then clear the previous one.
                if (record.Default)
                {
                    foreach (var defaultRecord in colRecords.Where(s => s.Default))
                    {
                        defaultRecord.Default = false;
                    }
                    record.Default = true;
                }

                try
                {
                    // Add the record to the collection
                    bc.AttributeTypes.InsertOnSubmit(record);
                    bc.SubmitChanges();
                    colRecords.Add(record);
                    gridView1.RefreshData();
                }
                catch (System.Data.SqlClient.SqlException ex)
                {
                    DebtPlus.UI.Common.ErrorHandling.HandleErrors(ex, "Error updating database");
                }
            }
        }

        /// <summary>
        /// Update the existing record
        /// </summary>
        /// <param name="obj">Pointer to the desired record for the operation</param>
        protected override void UpdateRecord(object obj)
        {
            // Find the record
            DebtPlus.LINQ.AttributeType record = obj as AttributeType;
            if (obj == null)
            {
                return;
            }

            // Remember the previous value for the default status. We will need it after the update.
            bool priorDefault = record.Default;

            // Update the record now.
            using (EditForm frm = new EditForm(record))
            {
                if (frm.ShowDialog() != System.Windows.Forms.DialogResult.OK)
                {
                    return;
                }
            }

            try
            {
                // Turn off the default status of all other records in the collection.
                if (record.Default && !priorDefault)
                {
                    foreach (var defaultRecord in colRecords.Where(s => s.Default))
                    {
                        defaultRecord.Default = false;
                    }

                    // Reset the default status back to TRUE for the current record.
                    record.Default = true;
                }

                bc.SubmitChanges();
                gridView1.RefreshData();
            }
            catch (System.Data.SqlClient.SqlException ex)
            {
                DebtPlus.UI.Common.ErrorHandling.HandleErrors(ex, "Error updating database");
            }
        }

        /// <summary>
        /// Delete the indicated record from the database
        /// </summary>
        /// <param name="obj">Pointer to the desired record for the operation</param>
        protected override void DeleteRecord(object obj)
        {
            // Find the record
            DebtPlus.LINQ.AttributeType record = obj as AttributeType;
            if (obj == null)
            {
                return;
            }

            if (DebtPlus.Data.Prompts.RequestConfirmation_Delete() != System.Windows.Forms.DialogResult.Yes)
            {
                return;
            }

            try
            {
                bc.AttributeTypes.DeleteOnSubmit(record);
                bc.SubmitChanges();

                colRecords.Remove(record);
                gridView1.RefreshData();
            }
            catch (System.Data.SqlClient.SqlException ex)
            {
                DebtPlus.UI.Common.ErrorHandling.HandleErrors(ex, "Error updating database");
            }
        }
    }
}