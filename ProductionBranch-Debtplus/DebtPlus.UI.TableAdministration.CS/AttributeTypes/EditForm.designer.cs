﻿namespace DebtPlus.UI.TableAdministration.CS.AttributeTypes
{
    partial class EditForm
    {
		//Form overrides dispose to clean up the component list.
		[System.Diagnostics.DebuggerNonUserCode]
		protected override void Dispose(bool disposing)
		{
			try 
            {
				if (disposing)
                {
                    if (components != null) components.Dispose();
				}
                components = null;
            }
    	    finally
            {
				base.Dispose(disposing);
			}
		}

		//Required by the Windows Form Designer
		private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.TextEdit_HUDServiceType = new DevExpress.XtraEditors.TextEdit();
            this.LabelControl6 = new DevExpress.XtraEditors.LabelControl();
            this.CheckEdit_ActiveFlag = new DevExpress.XtraEditors.CheckEdit();
            this.CheckEdit_Default = new DevExpress.XtraEditors.CheckEdit();
            this.LookUpEdit_LanguageID = new DevExpress.XtraEditors.LookUpEdit();
            this.ComboBoxEdit_Grouping = new DevExpress.XtraEditors.ComboBoxEdit();
            this.LabelControl3 = new DevExpress.XtraEditors.LabelControl();
            this.TextEdit_Attribute = new DevExpress.XtraEditors.TextEdit();
            this.LabelControl2 = new DevExpress.XtraEditors.LabelControl();
            this.LabelControl_description = new DevExpress.XtraEditors.LabelControl();
            this.LabelControl_ID = new DevExpress.XtraEditors.LabelControl();
            this.LabelControl1 = new DevExpress.XtraEditors.LabelControl();
            this.TextEdit_HudLanguage = new DevExpress.XtraEditors.TextEdit();
            this.LabelControl4 = new DevExpress.XtraEditors.LabelControl();
            ((System.ComponentModel.ISupportInitialize)(this.DxErrorProvider1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TextEdit_HUDServiceType.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.CheckEdit_ActiveFlag.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.CheckEdit_Default.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LookUpEdit_LanguageID.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ComboBoxEdit_Grouping.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TextEdit_Attribute.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TextEdit_HudLanguage.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // simpleButton_OK
            // 
            this.simpleButton_OK.Location = new System.Drawing.Point(372, 43);
            // 
            // simpleButton_Cancel
            // 
            this.simpleButton_Cancel.Location = new System.Drawing.Point(372, 86);
            // 
            // DefaultLookAndFeel1
            // 
            this.DefaultLookAndFeel1.LookAndFeel.SkinName = "McSkin";
            // 
            // TextEdit_HUDServiceType
            // 
            this.TextEdit_HUDServiceType.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.TextEdit_HUDServiceType.Location = new System.Drawing.Point(281, 106);
            this.TextEdit_HUDServiceType.Name = "TextEdit_HUDServiceType";
            this.TextEdit_HUDServiceType.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.True;
            this.TextEdit_HUDServiceType.Properties.MaxLength = 10;
            this.TextEdit_HUDServiceType.Size = new System.Drawing.Size(76, 20);
            this.TextEdit_HUDServiceType.TabIndex = 33;
            // 
            // LabelControl6
            // 
            this.LabelControl6.Location = new System.Drawing.Point(189, 109);
            this.LabelControl6.Name = "LabelControl6";
            this.LabelControl6.Size = new System.Drawing.Size(86, 13);
            this.LabelControl6.TabIndex = 32;
            this.LabelControl6.Text = "HUD Service Type";
            // 
            // CheckEdit_ActiveFlag
            // 
            this.CheckEdit_ActiveFlag.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.CheckEdit_ActiveFlag.Location = new System.Drawing.Point(279, 136);
            this.CheckEdit_ActiveFlag.Name = "CheckEdit_ActiveFlag";
            this.CheckEdit_ActiveFlag.Properties.Caption = "Active";
            this.CheckEdit_ActiveFlag.Size = new System.Drawing.Size(75, 20);
            this.CheckEdit_ActiveFlag.TabIndex = 35;
            // 
            // CheckEdit_Default
            // 
            this.CheckEdit_Default.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.CheckEdit_Default.Location = new System.Drawing.Point(4, 136);
            this.CheckEdit_Default.Name = "CheckEdit_Default";
            this.CheckEdit_Default.Properties.Caption = "Default";
            this.CheckEdit_Default.Size = new System.Drawing.Size(75, 20);
            this.CheckEdit_Default.TabIndex = 34;
            // 
            // LookUpEdit_LanguageID
            // 
            this.LookUpEdit_LanguageID.Location = new System.Drawing.Point(81, 80);
            this.LookUpEdit_LanguageID.Name = "LookUpEdit_LanguageID";
            this.LookUpEdit_LanguageID.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.True;
            this.LookUpEdit_LanguageID.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LookUpEdit_LanguageID.Properties.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("EnglishName", "Name", 20, DevExpress.Utils.FormatType.None, "", true, DevExpress.Utils.HorzAlignment.Default, DevExpress.Data.ColumnSortOrder.Ascending),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("Name", 5, "ID")});
            this.LookUpEdit_LanguageID.Properties.DisplayMember = "EnglishName";
            this.LookUpEdit_LanguageID.Properties.NullText = "";
            this.LookUpEdit_LanguageID.Properties.ValueMember = "Name";
            this.LookUpEdit_LanguageID.Size = new System.Drawing.Size(276, 20);
            this.LookUpEdit_LanguageID.TabIndex = 29;
            // 
            // ComboBoxEdit_Grouping
            // 
            this.ComboBoxEdit_Grouping.Location = new System.Drawing.Point(81, 28);
            this.ComboBoxEdit_Grouping.Name = "ComboBoxEdit_Grouping";
            this.ComboBoxEdit_Grouping.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.True;
            this.ComboBoxEdit_Grouping.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.ComboBoxEdit_Grouping.Properties.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.ComboBoxEdit_Grouping.Properties.Items.AddRange(new object[] {
            "LANGUAGE",
            "ROLE",
            "HOUSING"});
            this.ComboBoxEdit_Grouping.Properties.MaxLength = 50;
            this.ComboBoxEdit_Grouping.Size = new System.Drawing.Size(276, 20);
            this.ComboBoxEdit_Grouping.TabIndex = 25;
            // 
            // LabelControl3
            // 
            this.LabelControl3.Location = new System.Drawing.Point(4, 83);
            this.LabelControl3.Name = "LabelControl3";
            this.LabelControl3.Size = new System.Drawing.Size(61, 13);
            this.LabelControl3.TabIndex = 28;
            this.LabelControl3.Text = "Language ID";
            // 
            // TextEdit_Attribute
            // 
            this.TextEdit_Attribute.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.TextEdit_Attribute.Location = new System.Drawing.Point(81, 54);
            this.TextEdit_Attribute.Name = "TextEdit_Attribute";
            this.TextEdit_Attribute.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.False;
            this.TextEdit_Attribute.Properties.MaxLength = 50;
            this.TextEdit_Attribute.Size = new System.Drawing.Size(276, 20);
            this.TextEdit_Attribute.TabIndex = 27;
            // 
            // LabelControl2
            // 
            this.LabelControl2.Location = new System.Drawing.Point(4, 57);
            this.LabelControl2.Name = "LabelControl2";
            this.LabelControl2.Size = new System.Drawing.Size(43, 13);
            this.LabelControl2.TabIndex = 26;
            this.LabelControl2.Text = "Attribute";
            // 
            // LabelControl_description
            // 
            this.LabelControl_description.Location = new System.Drawing.Point(4, 31);
            this.LabelControl_description.Name = "LabelControl_description";
            this.LabelControl_description.Size = new System.Drawing.Size(29, 13);
            this.LabelControl_description.TabIndex = 24;
            this.LabelControl_description.Text = "Group";
            // 
            // LabelControl_ID
            // 
            this.LabelControl_ID.Location = new System.Drawing.Point(81, 9);
            this.LabelControl_ID.Name = "LabelControl_ID";
            this.LabelControl_ID.Size = new System.Drawing.Size(0, 13);
            this.LabelControl_ID.TabIndex = 23;
            // 
            // LabelControl1
            // 
            this.LabelControl1.Location = new System.Drawing.Point(4, 9);
            this.LabelControl1.Name = "LabelControl1";
            this.LabelControl1.Size = new System.Drawing.Size(11, 13);
            this.LabelControl1.TabIndex = 22;
            this.LabelControl1.Text = "ID";
            // 
            // TextEdit_HudLanguage
            // 
            this.TextEdit_HudLanguage.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.TextEdit_HudLanguage.Location = new System.Drawing.Point(81, 106);
            this.TextEdit_HudLanguage.Name = "TextEdit_HudLanguage";
            this.TextEdit_HudLanguage.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.True;
            this.TextEdit_HudLanguage.Properties.MaxLength = 10;
            this.TextEdit_HudLanguage.Size = new System.Drawing.Size(76, 20);
            this.TextEdit_HudLanguage.TabIndex = 31;
            // 
            // LabelControl4
            // 
            this.LabelControl4.Location = new System.Drawing.Point(4, 109);
            this.LabelControl4.Name = "LabelControl4";
            this.LabelControl4.Size = new System.Drawing.Size(71, 13);
            this.LabelControl4.TabIndex = 30;
            this.LabelControl4.Text = "HUD Language";
            // 
            // EditForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(468, 163);
            this.Controls.Add(this.TextEdit_HUDServiceType);
            this.Controls.Add(this.LabelControl6);
            this.Controls.Add(this.CheckEdit_ActiveFlag);
            this.Controls.Add(this.CheckEdit_Default);
            this.Controls.Add(this.LookUpEdit_LanguageID);
            this.Controls.Add(this.ComboBoxEdit_Grouping);
            this.Controls.Add(this.LabelControl3);
            this.Controls.Add(this.TextEdit_Attribute);
            this.Controls.Add(this.LabelControl2);
            this.Controls.Add(this.LabelControl_description);
            this.Controls.Add(this.LabelControl_ID);
            this.Controls.Add(this.LabelControl1);
            this.Controls.Add(this.TextEdit_HudLanguage);
            this.Controls.Add(this.LabelControl4);
            this.Name = "EditForm";
            this.Text = "AttributeType";
            this.Controls.SetChildIndex(this.simpleButton_OK, 0);
            this.Controls.SetChildIndex(this.simpleButton_Cancel, 0);
            this.Controls.SetChildIndex(this.LabelControl4, 0);
            this.Controls.SetChildIndex(this.TextEdit_HudLanguage, 0);
            this.Controls.SetChildIndex(this.LabelControl1, 0);
            this.Controls.SetChildIndex(this.LabelControl_ID, 0);
            this.Controls.SetChildIndex(this.LabelControl_description, 0);
            this.Controls.SetChildIndex(this.LabelControl2, 0);
            this.Controls.SetChildIndex(this.TextEdit_Attribute, 0);
            this.Controls.SetChildIndex(this.LabelControl3, 0);
            this.Controls.SetChildIndex(this.ComboBoxEdit_Grouping, 0);
            this.Controls.SetChildIndex(this.LookUpEdit_LanguageID, 0);
            this.Controls.SetChildIndex(this.CheckEdit_Default, 0);
            this.Controls.SetChildIndex(this.CheckEdit_ActiveFlag, 0);
            this.Controls.SetChildIndex(this.LabelControl6, 0);
            this.Controls.SetChildIndex(this.TextEdit_HUDServiceType, 0);
            ((System.ComponentModel.ISupportInitialize)(this.DxErrorProvider1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TextEdit_HUDServiceType.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.CheckEdit_ActiveFlag.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.CheckEdit_Default.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LookUpEdit_LanguageID.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ComboBoxEdit_Grouping.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TextEdit_Attribute.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TextEdit_HudLanguage.Properties)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }


        private DevExpress.XtraEditors.TextEdit TextEdit_HUDServiceType;
        private DevExpress.XtraEditors.LabelControl LabelControl6;
        private DevExpress.XtraEditors.CheckEdit CheckEdit_ActiveFlag;
        private DevExpress.XtraEditors.CheckEdit CheckEdit_Default;
        private DevExpress.XtraEditors.LookUpEdit LookUpEdit_LanguageID;
        private DevExpress.XtraEditors.ComboBoxEdit ComboBoxEdit_Grouping;
        private DevExpress.XtraEditors.LabelControl LabelControl3;
        private DevExpress.XtraEditors.TextEdit TextEdit_Attribute;
        private DevExpress.XtraEditors.LabelControl LabelControl2;
        private DevExpress.XtraEditors.LabelControl LabelControl_description;
        private DevExpress.XtraEditors.LabelControl LabelControl_ID;
        private DevExpress.XtraEditors.LabelControl LabelControl1;
        private DevExpress.XtraEditors.TextEdit TextEdit_HudLanguage;
        private DevExpress.XtraEditors.LabelControl LabelControl4;

    }
}