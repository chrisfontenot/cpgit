#region "Copyright 2000-2012 DebtPlus, L.L.C."
//{*******************************************************************}
//{                                                                   }
//{       DebtPlus Debt Management System                             }
//{                                                                   }
//{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
//{       ALL RIGHTS RESERVED                                         }
//{                                                                   }
//{   The entire contents of this file is protected by U.S. and       }
//{   International Copyright Laws. Unauthorized reproduction,        }
//{   reverse-engineering, and distribution of all or any portion of  }
//{   the code contained in this file is strictly prohibited and may  }
//{   result in severe civil and criminal penalties and will be       }
//{   prosecuted to the maximum extent possible under the law.        }
//{                                                                   }
//{   RESTRICTIONS                                                    }
//{                                                                   }
//{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
//{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
//{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
//{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
//{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
//{                                                                   }
//{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
//{   ADDITIONAL RESTRICTIONS.                                        }
//{                                                                   }
//{*******************************************************************}

#endregion "Copyright 2000-2012 DebtPlus, L.L.C."

using System;
using DebtPlus.LINQ;

namespace DebtPlus.UI.TableAdministration.CS.RelationshipTypes
{
    internal partial class EditForm : Templates.EditTemplateForm
    {
        private RelationType record = null;

        /// <summary>
        /// Initialize the new class
        /// </summary>
        internal EditForm()
            : base()
        {
            InitializeComponent();
        }

        /// <summary>
        /// Initialize the new class
        /// </summary>
        internal EditForm(RelationType record)
            : this()
        {
            this.record = record;
            RegisterHandlers();
        }

        /// <summary>
        /// Register the event handlers
        /// </summary>
        private void RegisterHandlers()
        {
            this.Load += EditForm_Load;
            TextEdit_description.EditValueChanged += Form_Changed;
            TextEdit_hud_9902_section.EditValueChanged += Form_Changed;
            TextEdit_rpps.EditValueChanged += Form_Changed;
            CheckEdit_ActiveFlag.EditValueChanged += Form_Changed;
            CheckEdit_default.EditValueChanged += Form_Changed;
        }

        /// <summary>
        /// Remove the event registrations
        /// </summary>
        private void UnRegisterHandlers()
        {
            this.Load -= EditForm_Load;
            TextEdit_description.EditValueChanged -= Form_Changed;
            TextEdit_hud_9902_section.EditValueChanged -= Form_Changed;
            TextEdit_rpps.EditValueChanged -= Form_Changed;
            CheckEdit_ActiveFlag.EditValueChanged -= Form_Changed;
            CheckEdit_default.EditValueChanged -= Form_Changed;
        }

        /// <summary>
        /// Process the LOAD event
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void EditForm_Load(object sender, EventArgs e)
        {
            UnRegisterHandlers();
            try
            {
                LabelControl_ID.Text = record.Id < 1 ? "NEW" : record.Id.ToString();
                TextEdit_description.EditValue = record.description;
                TextEdit_hud_9902_section.EditValue = record.hud_9902_section;
                TextEdit_rpps.EditValue = record.rpps;
                CheckEdit_ActiveFlag.Checked = record.ActiveFlag;
                CheckEdit_default.Checked = record.Default;

                simpleButton_OK.Enabled = !HasErrors();
            }
            finally
            {
                RegisterHandlers();
            }
        }

        /// <summary>
        /// Determine if the controls have any errors for the record
        /// </summary>
        /// <returns></returns>
        private bool HasErrors()
        {
            if (string.IsNullOrWhiteSpace(TextEdit_description.Text))
            {
                return true;
            }

            return false;
        }

        /// <summary>
        /// Process a change in the form controls
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Form_Changed(object sender, EventArgs e)
        {
            simpleButton_OK.Enabled = !HasErrors();
        }

        /// <summary>
        /// Handle the CLICK event on the OK button
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected override void simpleButton_OK_Click(object sender, EventArgs e)
        {
            base.simpleButton_OK_Click(sender, e);

            record.description = DebtPlus.Utils.Nulls.v_String(TextEdit_description.EditValue) ?? string.Empty;
            record.hud_9902_section = DebtPlus.Utils.Nulls.v_String(TextEdit_hud_9902_section.EditValue) ?? string.Empty;
            record.rpps = DebtPlus.Utils.Nulls.v_String(TextEdit_rpps.EditValue) ?? string.Empty;
            record.ActiveFlag = CheckEdit_ActiveFlag.Checked;
            record.Default = CheckEdit_default.Checked;
        }
    }
}