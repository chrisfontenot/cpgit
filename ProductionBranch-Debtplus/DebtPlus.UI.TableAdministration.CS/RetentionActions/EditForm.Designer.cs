using System.Windows.Forms;

namespace DebtPlus.UI.TableAdministration.CS.RetentionActions
{
	partial class EditForm
	{
		//Form overrides dispose to clean up the component list.
		[System.Diagnostics.DebuggerNonUserCode]
		protected override void Dispose(bool disposing)
		{
			try 
            {
				if (disposing)
                {
                    if (components != null) components.Dispose();
				}
                components = null;
            }
    	    finally
            {
				base.Dispose(disposing);
			}
		}

		//Required by the Windows Form Designer
		private System.ComponentModel.IContainer components = null;

		//NOTE: The following procedure is required by the Windows Form Designer
		//It can be modified using the Windows Form Designer.  
		//Do not modify it using the code editor.
		[System.Diagnostics.DebuggerStepThrough]
		private void InitializeComponent()
		{
            this.LabelControl1 = new DevExpress.XtraEditors.LabelControl();
            this.LabelControl_ID = new DevExpress.XtraEditors.LabelControl();
            this.LabelControl_description = new DevExpress.XtraEditors.LabelControl();
            this.TextEdit_description = new DevExpress.XtraEditors.TextEdit();
            this.LabelControl3 = new DevExpress.XtraEditors.LabelControl();
            this.LookUpEdit_letter_code = new DevExpress.XtraEditors.LookUpEdit();
            ((System.ComponentModel.ISupportInitialize)(this.DxErrorProvider1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TextEdit_description.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LookUpEdit_letter_code.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // simpleButton_OK
            // 
            this.simpleButton_OK.Location = new System.Drawing.Point(372, 13);
            this.simpleButton_OK.TabIndex = 10;
            // 
            // simpleButton_Cancel
            // 
            this.simpleButton_Cancel.Location = new System.Drawing.Point(372, 56);
            this.simpleButton_Cancel.TabIndex = 11;
            // 
            // DefaultLookAndFeel1
            // 
            this.DefaultLookAndFeel1.LookAndFeel.SkinName = "Caramel";
            // 
            // LabelControl1
            // 
            this.LabelControl1.Location = new System.Drawing.Point(13, 13);
            this.LabelControl1.Name = "LabelControl1";
            this.LabelControl1.Size = new System.Drawing.Size(11, 13);
            this.LabelControl1.TabIndex = 0;
            this.LabelControl1.Text = "ID";
            // 
            // LabelControl_ID
            // 
            this.LabelControl_ID.Location = new System.Drawing.Point(86, 13);
            this.LabelControl_ID.Name = "LabelControl_ID";
            this.LabelControl_ID.Size = new System.Drawing.Size(0, 13);
            this.LabelControl_ID.TabIndex = 1;
            // 
            // LabelControl_description
            // 
            this.LabelControl_description.Location = new System.Drawing.Point(13, 35);
            this.LabelControl_description.Name = "LabelControl_description";
            this.LabelControl_description.Size = new System.Drawing.Size(53, 13);
            this.LabelControl_description.TabIndex = 2;
            this.LabelControl_description.Text = "Description";
            // 
            // TextEdit_description
            // 
            this.TextEdit_description.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.TextEdit_description.Location = new System.Drawing.Point(86, 32);
            this.TextEdit_description.Name = "TextEdit_description";
            this.TextEdit_description.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.False;
            this.TextEdit_description.Properties.MaxLength = 50;
            this.TextEdit_description.Size = new System.Drawing.Size(280, 20);
            this.TextEdit_description.TabIndex = 3;
            // 
            // LabelControl3
            // 
            this.LabelControl3.Location = new System.Drawing.Point(13, 61);
            this.LabelControl3.Name = "LabelControl3";
            this.LabelControl3.Size = new System.Drawing.Size(56, 13);
            this.LabelControl3.TabIndex = 4;
            this.LabelControl3.Text = "Letter Type";
            // 
            // LookUpEdit_letter_code
            // 
            this.LookUpEdit_letter_code.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.LookUpEdit_letter_code.Location = new System.Drawing.Point(86, 58);
            this.LookUpEdit_letter_code.Name = "LookUpEdit_letter_code";
            this.LookUpEdit_letter_code.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.True;
            this.LookUpEdit_letter_code.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LookUpEdit_letter_code.Properties.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("letter_code", 5, "ID"),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("description", "Description")});
            this.LookUpEdit_letter_code.Properties.DisplayMember = "description";
            this.LookUpEdit_letter_code.Properties.NullText = "";
            this.LookUpEdit_letter_code.Properties.ShowFooter = false;
            this.LookUpEdit_letter_code.Properties.SortColumnIndex = 1;
            this.LookUpEdit_letter_code.Properties.ValueMember = "letter_code";
            this.LookUpEdit_letter_code.Size = new System.Drawing.Size(280, 20);
            this.LookUpEdit_letter_code.TabIndex = 5;
            // 
            // EditForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(468, 97);
            this.Controls.Add(this.LabelControl3);
            this.Controls.Add(this.LookUpEdit_letter_code);
            this.Controls.Add(this.TextEdit_description);
            this.Controls.Add(this.LabelControl_description);
            this.Controls.Add(this.LabelControl_ID);
            this.Controls.Add(this.LabelControl1);
            this.Name = "EditForm";
            this.Text = "Retention Action";
            this.Controls.SetChildIndex(this.LabelControl1, 0);
            this.Controls.SetChildIndex(this.LabelControl_ID, 0);
            this.Controls.SetChildIndex(this.simpleButton_OK, 0);
            this.Controls.SetChildIndex(this.simpleButton_Cancel, 0);
            this.Controls.SetChildIndex(this.LabelControl_description, 0);
            this.Controls.SetChildIndex(this.TextEdit_description, 0);
            this.Controls.SetChildIndex(this.LookUpEdit_letter_code, 0);
            this.Controls.SetChildIndex(this.LabelControl3, 0);
            ((System.ComponentModel.ISupportInitialize)(this.DxErrorProvider1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TextEdit_description.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LookUpEdit_letter_code.Properties)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

		}
		private DevExpress.XtraEditors.LabelControl LabelControl1;
		private DevExpress.XtraEditors.LabelControl LabelControl_ID;
		private DevExpress.XtraEditors.LabelControl LabelControl_description;
		private DevExpress.XtraEditors.TextEdit TextEdit_description;
		private DevExpress.XtraEditors.LabelControl LabelControl3;
		private DevExpress.XtraEditors.LookUpEdit LookUpEdit_letter_code;
	}
}

