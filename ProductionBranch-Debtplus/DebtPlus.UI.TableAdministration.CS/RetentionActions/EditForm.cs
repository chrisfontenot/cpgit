using System;
using DebtPlus.LINQ;

#region "Copyright 2000-2012 DebtPlus, L.L.C."
//{*******************************************************************}
//{                                                                   }
//{       DebtPlus Debt Management System                             }
//{                                                                   }
//{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
//{       ALL RIGHTS RESERVED                                         }
//{                                                                   }
//{   The entire contents of this file is protected by U.S. and       }
//{   International Copyright Laws. Unauthorized reproduction,        }
//{   reverse-engineering, and distribution of all or any portion of  }
//{   the code contained in this file is strictly prohibited and may  }
//{   result in severe civil and criminal penalties and will be       }
//{   prosecuted to the maximum extent possible under the law.        }
//{                                                                   }
//{   RESTRICTIONS                                                    }
//{                                                                   }
//{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
//{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
//{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
//{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
//{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
//{                                                                   }
//{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
//{   ADDITIONAL RESTRICTIONS.                                        }
//{                                                                   }
//{*******************************************************************}

#endregion "Copyright 2000-2012 DebtPlus, L.L.C."

namespace DebtPlus.UI.TableAdministration.CS.RetentionActions
{
    internal partial class EditForm : DebtPlus.UI.TableAdministration.CS.Templates.EditTemplateForm
    {
        internal EditForm()
            : base()
        {
            InitializeComponent();
        }

        private BusinessContext bc = null;
        private retention_action record = null;

        internal EditForm(BusinessContext bc, retention_action record)
            : this()
        {
            this.bc = bc;
            this.record = record;
            InitializeComponent();
            RegisterHandlers();
        }

        private void RegisterHandlers()
        {
            this.Load += EditForm_Load;
        }

        private void UnRegisterHandlers()
        {
            this.Load -= EditForm_Load;
        }

        private void EditForm_Load(object sender, EventArgs e)
        {
            UnRegisterHandlers();
            try
            {
                // Set the list of letter types. Choose only distinct values
                LookUpEdit_letter_code.Properties.DataSource = DebtPlus.LINQ.Cache.letter_type.getList().FindAll(s => s.language == DebtPlus.LINQ.Cache.AttributeType.getDefaultLanguage());

                // Set the ID of the new retention action
                LabelControl_ID.Text = record.Id < 1 ? "NEW" : record.Id.ToString();

                // Define the description
                TextEdit_description.EditValue = record.description;
                TextEdit_description.EditValueChanged += Form_Changed;

                // And the letter code
                LookUpEdit_letter_code.EditValue = record.letter_code;
                LookUpEdit_letter_code.EditValueChanged += Form_Changed;

                simpleButton_OK.Enabled = !HasErrors();
            }
            finally
            {
                RegisterHandlers();
            }
        }

        private void Form_Changed(object sender, EventArgs e)
        {
            simpleButton_OK.Enabled = !HasErrors();
        }

        private bool HasErrors()
        {
            if (string.IsNullOrWhiteSpace(TextEdit_description.Text))
            {
                return true;
            }

            return false;
        }

        protected override void simpleButton_OK_Click(object sender, EventArgs e)
        {
            base.simpleButton_OK_Click(sender, e);

            // Save the fields
            record.description = DebtPlus.Utils.Nulls.v_String(TextEdit_description.EditValue);
            record.letter_code = DebtPlus.Utils.Nulls.v_String(LookUpEdit_letter_code.EditValue);
        }
    }
}