namespace DebtPlus.UI.TableAdministration.CS.RetentionActions
{
	partial class MainForm
	{
		//Form overrides dispose to clean up the component list.
		[System.Diagnostics.DebuggerNonUserCode]
		protected override void Dispose(bool disposing)
		{
            try
            {
                if (disposing)
                {
                    if (components != null) components.Dispose();
                    if (bc != null) bc.Dispose();
                }
                bc = null;
                components = null;
            }

            finally
            {
                base.Dispose(disposing);
            }
		}

		//Required by the Windows Form Designer
		private System.ComponentModel.IContainer components = null;

		//NOTE: The following procedure is required by the Windows Form Designer
		//It can be modified using the Windows Form Designer.  
		//Do not modify it using the code editor.
		[System.Diagnostics.DebuggerStepThrough]
		private void InitializeComponent()
		{
			this.gridColumn_ID = new DevExpress.XtraGrid.Columns.GridColumn();
			this.gridColumn_ID.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.True;
			this.gridColumn_description = new DevExpress.XtraGrid.Columns.GridColumn();
			this.gridColumn_description.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.True;
			this.gridColumn_letter_code = new DevExpress.XtraGrid.Columns.GridColumn();
			this.gridColumn_letter_code.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.True;
			((System.ComponentModel.ISupportInitialize)this.gridControl1).BeginInit();
			((System.ComponentModel.ISupportInitialize)this.gridView1).BeginInit();
			((System.ComponentModel.ISupportInitialize)this.DxErrorProvider1).BeginInit();
			this.SuspendLayout();
			//
			//gridControl1
			//
			this.gridControl1.EmbeddedNavigator.Name = "";
			//
			//gridView1
			//
			this.gridView1.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
				this.gridColumn_ID,
				this.gridColumn_description,
				this.gridColumn_letter_code
			});
            this.gridView1.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] { new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.gridColumn_description, DevExpress.Data.ColumnSortOrder.Ascending) });
			//
			//gridColumn_ID
			//
			this.gridColumn_ID.Caption = "ID";
			this.gridColumn_ID.DisplayFormat.FormatString = "f0";
			this.gridColumn_ID.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
			this.gridColumn_ID.FieldName = "Id";
			this.gridColumn_ID.GroupFormat.FormatString = "f0";
			this.gridColumn_ID.GroupFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
			this.gridColumn_ID.Name = "gridColumn_ID";
			this.gridColumn_ID.Visible = true;
			this.gridColumn_ID.VisibleIndex = 0;
			//
			//gridColumn_Name
			//
			this.gridColumn_description.Caption = "Description";
			this.gridColumn_description.FieldName = "description";
			this.gridColumn_description.Name = "gridColumn_Name";
			this.gridColumn_description.Visible = true;
			this.gridColumn_description.VisibleIndex = 1;
			//
			//gridColumn_letter_code
			//
			this.gridColumn_letter_code.Caption = "Letter Code";
			this.gridColumn_letter_code.FieldName = "letter_code";
			this.gridColumn_letter_code.Name = "gridColumn_letter_code";
			this.gridColumn_letter_code.Visible = true;
			this.gridColumn_letter_code.VisibleIndex = 2;
			//
			//MainForm
			//
			this.AutoScaleDimensions = new System.Drawing.SizeF(6f, 13f);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(528, 294);
			this.Name = "MainForm";
			this.ToolTipController1.SetSuperTip(this, null);
			this.Text = "Retention Actions";
			((System.ComponentModel.ISupportInitialize)this.gridControl1).EndInit();
			((System.ComponentModel.ISupportInitialize)this.gridView1).EndInit();
			((System.ComponentModel.ISupportInitialize)this.DxErrorProvider1).EndInit();
			this.ResumeLayout(false);
		}
		private DevExpress.XtraGrid.Columns.GridColumn gridColumn_ID;
		private DevExpress.XtraGrid.Columns.GridColumn gridColumn_description;
		private DevExpress.XtraGrid.Columns.GridColumn gridColumn_letter_code;
	}
}
