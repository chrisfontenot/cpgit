﻿using System;
using System.Linq;
using DebtPlus.LINQ;

namespace DebtPlus.UI.TableAdministration.CS.ReferredBy
{
    internal partial class EditForm : Templates.EditTemplateForm
    {
        // Current record being edited
        private referred_by record = null;

        private BusinessContext bc = null;

        // Collection of types that may be modified as needed
        private System.Collections.Generic.List<referred_by_type> colTypes = null;

        /// <summary>
        /// Initialize the new class instance
        /// </summary>
        internal EditForm()
            : base()
        {
            InitializeComponent();
        }

        /// <summary>
        /// Initialize the new class instance
        /// </summary>
        /// <param name="record"></param>
        internal EditForm(BusinessContext bc, referred_by record)
            : this()
        {
            this.record = record;
            this.bc = bc;
            RegisterHandlers();
        }

        /// <summary>
        /// Add the event handlers to the collection
        /// </summary>
        private void RegisterHandlers()
        {
            Load += EditForm_Load;
            TextEdit_description.EditValueChanged += FormChanged;
            LookUpEdit_referred_by_type.ButtonClick += LookUpEdit_referred_by_type_ButtonClick;
        }

        /// <summary>
        /// Remove the event handlers from being processed
        /// </summary>
        private void UnRegisterHandlers()
        {
            Load -= EditForm_Load;
            TextEdit_description.EditValueChanged -= FormChanged;
            LookUpEdit_referred_by_type.ButtonClick -= LookUpEdit_referred_by_type_ButtonClick;
        }

        /// <summary>
        /// Process the FORM LOAD event
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void EditForm_Load(object sender, System.EventArgs e)
        {
            UnRegisterHandlers();
            try
            {
                // Retrieve the collection of types from the system.
                // The DataContext (bc) will cache the entries for us automatically.
                colTypes = bc.referred_by_types.ToList();
                LookUpEdit_referred_by_type.Properties.DataSource = colTypes;

                LabelControl_ID.Text = record.Id <= 0 ? "NEW" : record.Id.ToString();
                TextEdit_description.EditValue = record.description;
                textEdit_partnerValidation.EditValue = record.partnerValidation;
                CheckEdit_ActiveFlag.EditValue = record.ActiveFlag;
                CheckEdit_default.EditValue = record.Default;
                MemoExEdit_note.EditValue = record.note;
                LookUpEdit_referred_by_type.EditValue = record.referred_by_type;
                TextEdit_hud_9902_section.EditValue = record.hud_9902_section;

                simpleButton_OK.Enabled = !HasErrors();
            }
            finally
            {
                RegisterHandlers();
            }
        }

        /// <summary>
        /// Handle the CLICK of the extra button on the type list.
        /// </summary>
        private void LookUpEdit_referred_by_type_ButtonClick(object sender, DevExpress.XtraEditors.Controls.ButtonPressedEventArgs e)
        {
            if (e.Button.Kind != DevExpress.XtraEditors.Controls.ButtonPredefines.Plus)
            {
                return;
            }

            string newDescription = string.Empty;
            if (DebtPlus.Data.Forms.InputBox.Show("What is the new type description?", ref newDescription, "Referral Source Types", newDescription, 50) != System.Windows.Forms.DialogResult.OK)
            {
                return;
            }

            newDescription = newDescription.Trim();
            if (string.IsNullOrWhiteSpace(newDescription))
            {
                return;
            }

            try
            {
                var newTypeRecord = DebtPlus.LINQ.Factory.Manufacture_referred_by_type();
                newTypeRecord.description = newDescription;

                bc.referred_by_types.InsertOnSubmit(newTypeRecord);
                bc.SubmitChanges();

                // Add the new item to the current collection
                colTypes.Add(newTypeRecord);

                // Ignore the cached list and simple re-read it from the database if it is added.
                // This may not be ideal but it "works" because we don't want to change the cached copies at all, even to add a new item.
                LookUpEdit_referred_by_type.Properties.DataSource = colTypes;
                LookUpEdit_referred_by_type.EditValue = newTypeRecord.Id;
            }
            catch (System.Data.SqlClient.SqlException ex)
            {
                DebtPlus.UI.Common.ErrorHandling.HandleErrors(ex, "Error updating referred_by_types table");
            }
        }

        /// <summary>
        /// Override the OK button to save the editing values into the record
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected override void simpleButton_OK_Click(object sender, EventArgs e)
        {
            record.description = Convert.ToString(TextEdit_description.EditValue);
            record.ActiveFlag = Convert.ToBoolean(CheckEdit_ActiveFlag.EditValue);
            record.Default = Convert.ToBoolean(CheckEdit_default.EditValue);
            record.note = Convert.ToString(MemoExEdit_note.EditValue);
            record.partnerValidation = DebtPlus.Utils.Nulls.v_String(textEdit_partnerValidation.EditValue);
            record.referred_by_type = DebtPlus.Utils.Nulls.v_Int32(LookUpEdit_referred_by_type.EditValue);
            record.hud_9902_section = Convert.ToString(TextEdit_hud_9902_section.EditValue);

            base.simpleButton_OK_Click(sender, e);
        }

        private void FormChanged(object sender, EventArgs e)
        {
            simpleButton_OK.Enabled = !HasErrors();
        }

        /// <summary>
        /// Determine if the input form has error(s)
        /// </summary>
        /// <returns></returns>
        private bool HasErrors()
        {
            return TextEdit_description.Text.Trim() == string.Empty;
        }
    }
}