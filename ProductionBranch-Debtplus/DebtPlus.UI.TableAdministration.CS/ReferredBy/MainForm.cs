﻿using System;
using System.Linq;
using DebtPlus.LINQ;

namespace DebtPlus.UI.TableAdministration.CS.ReferredBy
{
    public partial class MainForm : Templates.MainForm
    {
        /// <summary>
        /// List of the records to be edited in the program
        /// </summary>
        private System.Collections.Generic.List<referred_by> colRecords;

        private BusinessContext bc = new BusinessContext();

        /// <summary>
        /// Initialize the new class
        /// </summary>
        /// <param name="dc">Pointer to the data context in the mainline</param>
        public MainForm()
            : base()
        {
            InitializeComponent();
            RegisterHandlers();
        }

        private void RegisterHandlers()
        {
            Load += MainForm_Load;
        }

        private void UnRegisterHandlers()
        {
            Load -= MainForm_Load;
        }

        /// <summary>
        /// Process the load event on the form
        /// </summary>
        private void MainForm_Load(object sender, System.EventArgs e)
        {
            using (var cm = new DebtPlus.UI.Common.CursorManager())
            {
                try
                {
                    // Retrieve the list of menu items
                    colRecords = bc.referred_bies.ToList();
                    gridControl1.DataSource = colRecords;
                    gridView1.BestFitColumns();
                }
                catch (Exception ex)
                {
                    DebtPlus.UI.Common.ErrorHandling.HandleErrors(ex, "Error retrieving database information");
                }
            }
        }

        /// <summary>
        /// Create the new record
        /// </summary>
        protected override void CreateRecord()
        {
            var record = DebtPlus.LINQ.Factory.Manufacture_referred_by();

            // Edit the new record. If OK then attempt to insert the record.
            using (EditForm frm = new EditForm(bc, record))
            {
                if (frm.ShowDialog() != System.Windows.Forms.DialogResult.OK)
                {
                    return;
                }
            }

            try
            {
                // If the default status is now set then we need to remove the default status from the previous records
                if (record.Default)
                {
                    foreach (var defaultRecord in colRecords.Where(s => s.Default))
                    {
                        defaultRecord.Default = false;
                    }
                }

                // Insert the new record into the collection
                bc.referred_bies.InsertOnSubmit(record);
                bc.SubmitChanges();

                // Add the record to the collection of items
                colRecords.Add(record);
                gridView1.RefreshData();
            }
            catch (System.Data.SqlClient.SqlException ex)
            {
                DebtPlus.UI.Common.ErrorHandling.HandleErrors(ex, "Error updating database");
            }
        }

        /// <summary>
        /// Update the existing record
        /// </summary>
        /// <param name="obj">Pointer to the desired record for the operation</param>
        protected override void UpdateRecord(object obj)
        {
            // Find the record
            DebtPlus.LINQ.referred_by record = obj as referred_by;
            if (obj == null)
            {
                return;
            }

            // Remember the default status before we edit the record
            bool priorDefault = record.Default;

            // Edit the record
            using (var frm = new EditForm(bc, record))
            {
                if (frm.ShowDialog() != System.Windows.Forms.DialogResult.OK)
                {
                    return;
                }
            }

            try
            {
                // If the default status is now set and it was not set earlier
                // then we need to remove the default status from the previous records
                if (record.Default && !priorDefault)
                {
                    foreach (var defaultRecord in colRecords.Where(s => s.Default))
                    {
                        defaultRecord.Default = false;
                    }

                    record.Default = true;
                }

                // Commit the changes to the database
                bc.SubmitChanges();
                gridView1.RefreshData();
            }
            catch (System.Data.SqlClient.SqlException ex)
            {
                DebtPlus.UI.Common.ErrorHandling.HandleErrors(ex, "Error updating database");
            }
        }

        /// <summary>
        /// Delete the indicated record from the database
        /// </summary>
        /// <param name="obj">Pointer to the desired record for the operation</param>
        protected override void DeleteRecord(object obj)
        {
            // Find the record
            DebtPlus.LINQ.referred_by record = obj as referred_by;
            if (obj == null)
            {
                return;
            }

            if (DebtPlus.Data.Prompts.RequestConfirmation_Delete() != System.Windows.Forms.DialogResult.Yes)
            {
                return;
            }

            // Purge the record from the database
            bc.referred_bies.DeleteOnSubmit(record);
            bc.SubmitChanges();

            // Discard the record from the collection
            colRecords.Remove(record);
            gridView1.RefreshData();
        }
    }
}