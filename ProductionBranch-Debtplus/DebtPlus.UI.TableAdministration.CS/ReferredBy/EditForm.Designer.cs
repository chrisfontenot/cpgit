﻿namespace DebtPlus.UI.TableAdministration.CS.ReferredBy
{
    partial class EditForm
    {
		//Form overrides dispose to clean up the component list.
		[System.Diagnostics.DebuggerNonUserCode]
		protected override void Dispose(bool disposing)
		{
			try 
            {
				if (disposing)
                {
                    if (components != null) components.Dispose();
				}
                components = null;
            }
    	    finally
            {
				base.Dispose(disposing);
			}
		}

		//Required by the Windows Form Designer
		private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            DevExpress.Utils.SuperToolTip superToolTip1 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem1 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem1 = new DevExpress.Utils.ToolTipItem();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(EditForm));
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject1 = new DevExpress.Utils.SerializableAppearanceObject();
            this.LayoutControl1 = new DevExpress.XtraLayout.LayoutControl();
            this.textEdit_partnerValidation = new DevExpress.XtraEditors.TextEdit();
            this.LabelControl_ID = new DevExpress.XtraEditors.LabelControl();
            this.CheckEdit_ActiveFlag = new DevExpress.XtraEditors.CheckEdit();
            this.TextEdit_description = new DevExpress.XtraEditors.TextEdit();
            this.CheckEdit_default = new DevExpress.XtraEditors.CheckEdit();
            this.LookUpEdit_referred_by_type = new DevExpress.XtraEditors.LookUpEdit();
            this.MemoExEdit_note = new DevExpress.XtraEditors.MemoExEdit();
            this.LayoutControlGroup1 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.LayoutControlItem1 = new DevExpress.XtraLayout.LayoutControlItem();
            this.LayoutControlItem2 = new DevExpress.XtraLayout.LayoutControlItem();
            this.LayoutControlItem3 = new DevExpress.XtraLayout.LayoutControlItem();
            this.LayoutControlItem4 = new DevExpress.XtraLayout.LayoutControlItem();
            this.LayoutControlItem6 = new DevExpress.XtraLayout.LayoutControlItem();
            this.LayoutControlItem7 = new DevExpress.XtraLayout.LayoutControlItem();
            this.EmptySpaceItem1 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.LayoutControlItem5 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem8 = new DevExpress.XtraLayout.LayoutControlItem();
            this.TextEdit_hud_9902_section = new DevExpress.XtraEditors.TextEdit();
            ((System.ComponentModel.ISupportInitialize)(this.DxErrorProvider1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LayoutControl1)).BeginInit();
            this.LayoutControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit_partnerValidation.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.CheckEdit_ActiveFlag.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TextEdit_description.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.CheckEdit_default.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LookUpEdit_referred_by_type.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.MemoExEdit_note.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LayoutControlGroup1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LayoutControlItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LayoutControlItem2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LayoutControlItem3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LayoutControlItem4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LayoutControlItem6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LayoutControlItem7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.EmptySpaceItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LayoutControlItem5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TextEdit_hud_9902_section.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // simpleButton_Cancel
            // 
            this.simpleButton_Cancel.Location = new System.Drawing.Point(347, 67);
            // 
            // simpleButton_OK
            // 
            this.simpleButton_OK.Location = new System.Drawing.Point(347, 35);
            // 
            // DefaultLookAndFeel1
            // 
            this.DefaultLookAndFeel1.LookAndFeel.SkinName = "McSkin";
            // 
            // LayoutControl1
            // 
            this.LayoutControl1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.LayoutControl1.Controls.Add(this.textEdit_partnerValidation);
            this.LayoutControl1.Controls.Add(this.LabelControl_ID);
            this.LayoutControl1.Controls.Add(this.CheckEdit_ActiveFlag);
            this.LayoutControl1.Controls.Add(this.TextEdit_description);
            this.LayoutControl1.Controls.Add(this.CheckEdit_default);
            this.LayoutControl1.Controls.Add(this.LookUpEdit_referred_by_type);
            this.LayoutControl1.Controls.Add(this.MemoExEdit_note);
            this.LayoutControl1.Controls.Add(this.TextEdit_hud_9902_section);
            this.LayoutControl1.Location = new System.Drawing.Point(-7, -4);
            this.LayoutControl1.Name = "LayoutControl1";
            this.LayoutControl1.Root = this.LayoutControlGroup1;
            this.LayoutControl1.Size = new System.Drawing.Size(350, 201);
            this.LayoutControl1.TabIndex = 22;
            this.LayoutControl1.Text = "LayoutControl1";
            // 
            // textEdit_partnerValidation
            // 
            this.textEdit_partnerValidation.Location = new System.Drawing.Point(100, 125);
            this.textEdit_partnerValidation.Name = "textEdit_partnerValidation";
            this.textEdit_partnerValidation.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.True;
            this.textEdit_partnerValidation.Properties.MaxLength = 50;
            this.textEdit_partnerValidation.Properties.NullText = "-- NO PARTNER CODE ALLOWED --";
            this.textEdit_partnerValidation.Size = new System.Drawing.Size(238, 20);
            this.textEdit_partnerValidation.StyleController = this.LayoutControl1;
            superToolTip1.AllowHtmlText = DevExpress.Utils.DefaultBoolean.True;
            toolTipTitleItem1.Text = "Partner Code Validation";
            toolTipItem1.LeftIndent = 6;
            toolTipItem1.Text = resources.GetString("toolTipItem1.Text");
            superToolTip1.Items.Add(toolTipTitleItem1);
            superToolTip1.Items.Add(toolTipItem1);
            this.textEdit_partnerValidation.SuperTip = superToolTip1;
            this.textEdit_partnerValidation.TabIndex = 12;
            // 
            // LabelControl_ID
            // 
            this.LabelControl_ID.Location = new System.Drawing.Point(100, 12);
            this.LabelControl_ID.Name = "LabelControl_ID";
            this.LabelControl_ID.Size = new System.Drawing.Size(238, 13);
            this.LabelControl_ID.StyleController = this.LayoutControl1;
            this.LabelControl_ID.TabIndex = 1;
            // 
            // CheckEdit_ActiveFlag
            // 
            this.CheckEdit_ActiveFlag.Location = new System.Drawing.Point(176, 169);
            this.CheckEdit_ActiveFlag.Name = "CheckEdit_ActiveFlag";
            this.CheckEdit_ActiveFlag.Properties.Caption = "Active Item";
            this.CheckEdit_ActiveFlag.Size = new System.Drawing.Size(162, 20);
            this.CheckEdit_ActiveFlag.StyleController = this.LayoutControl1;
            this.CheckEdit_ActiveFlag.TabIndex = 11;
            // 
            // TextEdit_description
            // 
            this.TextEdit_description.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.TextEdit_description.Location = new System.Drawing.Point(100, 29);
            this.TextEdit_description.Name = "TextEdit_description";
            this.TextEdit_description.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.False;
            this.TextEdit_description.Properties.MaxLength = 50;
            this.TextEdit_description.Size = new System.Drawing.Size(238, 20);
            this.TextEdit_description.StyleController = this.LayoutControl1;
            this.TextEdit_description.TabIndex = 3;
            // 
            // CheckEdit_default
            // 
            this.CheckEdit_default.Location = new System.Drawing.Point(12, 169);
            this.CheckEdit_default.Name = "CheckEdit_default";
            this.CheckEdit_default.Properties.Caption = "Default";
            this.CheckEdit_default.Size = new System.Drawing.Size(160, 20);
            this.CheckEdit_default.StyleController = this.LayoutControl1;
            this.CheckEdit_default.TabIndex = 10;
            // 
            // LookUpEdit_referred_by_type
            // 
            this.LookUpEdit_referred_by_type.Location = new System.Drawing.Point(100, 53);
            this.LookUpEdit_referred_by_type.Name = "LookUpEdit_referred_by_type";
            this.LookUpEdit_referred_by_type.Properties.ActionButtonIndex = 1;
            this.LookUpEdit_referred_by_type.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Plus, "", -1, true, true, false, DevExpress.XtraEditors.ImageLocation.MiddleCenter, null, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject1, "Add a new type...", null, null, true),
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LookUpEdit_referred_by_type.Properties.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("Id", "ID", 20, DevExpress.Utils.FormatType.Numeric, "f0", false, DevExpress.Utils.HorzAlignment.Default),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("description", "Description", 20, DevExpress.Utils.FormatType.None, "", true, DevExpress.Utils.HorzAlignment.Default, DevExpress.Data.ColumnSortOrder.Ascending, DevExpress.Utils.DefaultBoolean.True)});
            this.LookUpEdit_referred_by_type.Properties.DisplayMember = "description";
            this.LookUpEdit_referred_by_type.Properties.NullText = "";
            this.LookUpEdit_referred_by_type.Properties.ShowFooter = false;
            this.LookUpEdit_referred_by_type.Properties.ShowHeader = false;
            this.LookUpEdit_referred_by_type.Properties.SortColumnIndex = 1;
            this.LookUpEdit_referred_by_type.Properties.ValueMember = "Id";
            this.LookUpEdit_referred_by_type.Size = new System.Drawing.Size(238, 20);
            this.LookUpEdit_referred_by_type.StyleController = this.LayoutControl1;
            this.LookUpEdit_referred_by_type.TabIndex = 5;
            // 
            // MemoExEdit_note
            // 
            this.MemoExEdit_note.Location = new System.Drawing.Point(100, 101);
            this.MemoExEdit_note.Name = "MemoExEdit_note";
            this.MemoExEdit_note.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.True;
            this.MemoExEdit_note.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.MemoExEdit_note.Properties.MaxLength = 256;
            this.MemoExEdit_note.Properties.ShowIcon = false;
            this.MemoExEdit_note.Size = new System.Drawing.Size(238, 20);
            this.MemoExEdit_note.StyleController = this.LayoutControl1;
            this.MemoExEdit_note.TabIndex = 7;
            // 
            // LayoutControlGroup1
            // 
            this.LayoutControlGroup1.CustomizationFormText = "LayoutControlGroup1";
            this.LayoutControlGroup1.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.True;
            this.LayoutControlGroup1.GroupBordersVisible = false;
            this.LayoutControlGroup1.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.LayoutControlItem1,
            this.LayoutControlItem2,
            this.LayoutControlItem3,
            this.LayoutControlItem4,
            this.LayoutControlItem6,
            this.LayoutControlItem7,
            this.EmptySpaceItem1,
            this.LayoutControlItem5,
            this.layoutControlItem8});
            this.LayoutControlGroup1.Location = new System.Drawing.Point(0, 0);
            this.LayoutControlGroup1.Name = "LayoutControlGroup1";
            this.LayoutControlGroup1.Size = new System.Drawing.Size(350, 201);
            this.LayoutControlGroup1.Text = "LayoutControlGroup1";
            this.LayoutControlGroup1.TextVisible = false;
            // 
            // LayoutControlItem1
            // 
            this.LayoutControlItem1.Control = this.LabelControl_ID;
            this.LayoutControlItem1.CustomizationFormText = "ID";
            this.LayoutControlItem1.Location = new System.Drawing.Point(0, 0);
            this.LayoutControlItem1.Name = "LayoutControlItem1";
            this.LayoutControlItem1.Size = new System.Drawing.Size(330, 17);
            this.LayoutControlItem1.Text = "ID";
            this.LayoutControlItem1.TextSize = new System.Drawing.Size(85, 13);
            // 
            // LayoutControlItem2
            // 
            this.LayoutControlItem2.Control = this.TextEdit_description;
            this.LayoutControlItem2.CustomizationFormText = "Description";
            this.LayoutControlItem2.Location = new System.Drawing.Point(0, 17);
            this.LayoutControlItem2.Name = "LayoutControlItem2";
            this.LayoutControlItem2.Size = new System.Drawing.Size(330, 24);
            this.LayoutControlItem2.Text = "Description";
            this.LayoutControlItem2.TextSize = new System.Drawing.Size(85, 13);
            // 
            // LayoutControlItem3
            // 
            this.LayoutControlItem3.Control = this.LookUpEdit_referred_by_type;
            this.LayoutControlItem3.CustomizationFormText = "Type";
            this.LayoutControlItem3.Location = new System.Drawing.Point(0, 41);
            this.LayoutControlItem3.Name = "LayoutControlItem3";
            this.LayoutControlItem3.Size = new System.Drawing.Size(330, 24);
            this.LayoutControlItem3.Text = "Type";
            this.LayoutControlItem3.TextSize = new System.Drawing.Size(85, 13);
            // 
            // LayoutControlItem4
            // 
            this.LayoutControlItem4.Control = this.MemoExEdit_note;
            this.LayoutControlItem4.CustomizationFormText = "Note";
            this.LayoutControlItem4.Location = new System.Drawing.Point(0, 89);
            this.LayoutControlItem4.Name = "LayoutControlItem4";
            this.LayoutControlItem4.Size = new System.Drawing.Size(330, 24);
            this.LayoutControlItem4.Text = "Note";
            this.LayoutControlItem4.TextSize = new System.Drawing.Size(85, 13);
            // 
            // LayoutControlItem6
            // 
            this.LayoutControlItem6.Control = this.CheckEdit_default;
            this.LayoutControlItem6.CustomizationFormText = "LayoutControlItem6";
            this.LayoutControlItem6.Location = new System.Drawing.Point(0, 157);
            this.LayoutControlItem6.Name = "LayoutControlItem6";
            this.LayoutControlItem6.Size = new System.Drawing.Size(164, 24);
            this.LayoutControlItem6.Text = "LayoutControlItem6";
            this.LayoutControlItem6.TextSize = new System.Drawing.Size(0, 0);
            this.LayoutControlItem6.TextToControlDistance = 0;
            this.LayoutControlItem6.TextVisible = false;
            // 
            // LayoutControlItem7
            // 
            this.LayoutControlItem7.Control = this.CheckEdit_ActiveFlag;
            this.LayoutControlItem7.CustomizationFormText = "LayoutControlItem7";
            this.LayoutControlItem7.Location = new System.Drawing.Point(164, 157);
            this.LayoutControlItem7.Name = "LayoutControlItem7";
            this.LayoutControlItem7.Size = new System.Drawing.Size(166, 24);
            this.LayoutControlItem7.Text = "LayoutControlItem7";
            this.LayoutControlItem7.TextSize = new System.Drawing.Size(0, 0);
            this.LayoutControlItem7.TextToControlDistance = 0;
            this.LayoutControlItem7.TextVisible = false;
            // 
            // EmptySpaceItem1
            // 
            this.EmptySpaceItem1.AllowHotTrack = false;
            this.EmptySpaceItem1.CustomizationFormText = "EmptySpaceItem1";
            this.EmptySpaceItem1.Location = new System.Drawing.Point(0, 137);
            this.EmptySpaceItem1.Name = "EmptySpaceItem1";
            this.EmptySpaceItem1.Size = new System.Drawing.Size(330, 20);
            this.EmptySpaceItem1.Text = "EmptySpaceItem1";
            this.EmptySpaceItem1.TextSize = new System.Drawing.Size(0, 0);
            // 
            // LayoutControlItem5
            // 
            this.LayoutControlItem5.Control = this.TextEdit_hud_9902_section;
            this.LayoutControlItem5.CustomizationFormText = "Housing";
            this.LayoutControlItem5.Location = new System.Drawing.Point(0, 65);
            this.LayoutControlItem5.Name = "LayoutControlItem5";
            this.LayoutControlItem5.Size = new System.Drawing.Size(330, 24);
            this.LayoutControlItem5.Text = "Housing";
            this.LayoutControlItem5.TextSize = new System.Drawing.Size(85, 13);
            // 
            // layoutControlItem8
            // 
            this.layoutControlItem8.Control = this.textEdit_partnerValidation;
            this.layoutControlItem8.CustomizationFormText = "Partner Validation";
            this.layoutControlItem8.Location = new System.Drawing.Point(0, 113);
            this.layoutControlItem8.Name = "layoutControlItem8";
            this.layoutControlItem8.Size = new System.Drawing.Size(330, 24);
            this.layoutControlItem8.Text = "Partner Validation";
            this.layoutControlItem8.TextSize = new System.Drawing.Size(85, 13);
            // 
            // lookupEdit_hud_9902_section
            // 
            this.TextEdit_hud_9902_section.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.TextEdit_hud_9902_section.Location = new System.Drawing.Point(100, 77);
            this.TextEdit_hud_9902_section.Name = "lookupEdit_hud_9902_section";
            this.TextEdit_hud_9902_section.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.True;
            this.TextEdit_hud_9902_section.Properties.MaxLength = 256;
            this.TextEdit_hud_9902_section.Size = new System.Drawing.Size(238, 20);
            this.TextEdit_hud_9902_section.StyleController = this.LayoutControl1;
            this.TextEdit_hud_9902_section.TabIndex = 9;
            // 
            // EditForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(431, 192);
            this.Controls.Add(this.LayoutControl1);
            this.Name = "EditForm";
            this.Text = "Referral Source Record";
            this.Controls.SetChildIndex(this.simpleButton_OK, 0);
            this.Controls.SetChildIndex(this.simpleButton_Cancel, 0);
            this.Controls.SetChildIndex(this.LayoutControl1, 0);
            ((System.ComponentModel.ISupportInitialize)(this.DxErrorProvider1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LayoutControl1)).EndInit();
            this.LayoutControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.textEdit_partnerValidation.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.CheckEdit_ActiveFlag.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TextEdit_description.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.CheckEdit_default.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LookUpEdit_referred_by_type.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.MemoExEdit_note.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LayoutControlGroup1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LayoutControlItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LayoutControlItem2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LayoutControlItem3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LayoutControlItem4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LayoutControlItem6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LayoutControlItem7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.EmptySpaceItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LayoutControlItem5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TextEdit_hud_9902_section.Properties)).EndInit();
            this.ResumeLayout(false);

        }


        private DevExpress.XtraLayout.LayoutControl LayoutControl1;
        private DevExpress.XtraEditors.LabelControl LabelControl_ID;
        private DevExpress.XtraEditors.CheckEdit CheckEdit_ActiveFlag;
        private DevExpress.XtraEditors.TextEdit TextEdit_description;
        private DevExpress.XtraEditors.CheckEdit CheckEdit_default;
        private DevExpress.XtraEditors.LookUpEdit LookUpEdit_referred_by_type;
        private DevExpress.XtraEditors.MemoExEdit MemoExEdit_note;
        private DevExpress.XtraLayout.LayoutControlGroup LayoutControlGroup1;
        private DevExpress.XtraLayout.LayoutControlItem LayoutControlItem1;
        private DevExpress.XtraLayout.LayoutControlItem LayoutControlItem2;
        private DevExpress.XtraLayout.LayoutControlItem LayoutControlItem3;
        private DevExpress.XtraLayout.LayoutControlItem LayoutControlItem4;
        private DevExpress.XtraLayout.LayoutControlItem LayoutControlItem6;
        private DevExpress.XtraLayout.LayoutControlItem LayoutControlItem7;
        private DevExpress.XtraLayout.EmptySpaceItem EmptySpaceItem1;
        private DevExpress.XtraLayout.LayoutControlItem LayoutControlItem5;
        private DevExpress.XtraEditors.TextEdit textEdit_partnerValidation;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem8;
        private DevExpress.XtraEditors.TextEdit TextEdit_hud_9902_section;


    }
}