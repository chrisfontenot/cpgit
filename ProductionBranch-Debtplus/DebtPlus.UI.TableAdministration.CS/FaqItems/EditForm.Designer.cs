using System.Windows.Forms;

namespace DebtPlus.UI.TableAdministration.CS.FaqItems
{
	partial class EditForm
	{
		//Form overrides dispose to clean up the component list.
		[System.Diagnostics.DebuggerNonUserCode]
		protected override void Dispose(bool disposing)
		{
			try 
            {
				if (disposing)
                {
                    if (components != null) components.Dispose();
				}
                components = null;
            }
    	    finally
            {
				base.Dispose(disposing);
			}
		}

		//Required by the Windows Form Designer
		private System.ComponentModel.IContainer components = null;

		//NOTE: The following procedure is required by the Windows Form Designer
		//It can be modified using the Windows Form Designer.  
		//Do not modify it using the code editor.
		[System.Diagnostics.DebuggerStepThrough]
		private void InitializeComponent()
		{
			this.LabelControl1 = new DevExpress.XtraEditors.LabelControl();
			this.LabelControl_ID = new DevExpress.XtraEditors.LabelControl();
			this.LabelControl_description = new DevExpress.XtraEditors.LabelControl();
			this.TextEdit_description = new DevExpress.XtraEditors.TextEdit();
			this.TextEdit_attributes = new DevExpress.XtraEditors.TextEdit();
			this.LabelControl2 = new DevExpress.XtraEditors.LabelControl();
			this.LabelControl3 = new DevExpress.XtraEditors.LabelControl();
			this.ComboBoxEdit1 = new DevExpress.XtraEditors.ComboBoxEdit();
			this.ComboBoxEdit2 = new DevExpress.XtraEditors.ComboBoxEdit();
			this.ComboBoxEdit3 = new DevExpress.XtraEditors.ComboBoxEdit();
			((System.ComponentModel.ISupportInitialize)this.DxErrorProvider1).BeginInit();
			((System.ComponentModel.ISupportInitialize)this.TextEdit_description.Properties).BeginInit();
			((System.ComponentModel.ISupportInitialize)this.TextEdit_attributes.Properties).BeginInit();
			((System.ComponentModel.ISupportInitialize)this.ComboBoxEdit1.Properties).BeginInit();
			((System.ComponentModel.ISupportInitialize)this.ComboBoxEdit2.Properties).BeginInit();
			((System.ComponentModel.ISupportInitialize)this.ComboBoxEdit3.Properties).BeginInit();
			this.SuspendLayout();
			//
			//simpleButton_OK
			//
			this.simpleButton_OK.Location = new System.Drawing.Point(372, 13);
			this.simpleButton_OK.TabIndex = 10;
			//
			//simpleButton_Cancel
			//
			this.simpleButton_Cancel.Location = new System.Drawing.Point(372, 56);
			this.simpleButton_Cancel.TabIndex = 11;
			//
			//LabelControl1
			//
			this.LabelControl1.Location = new System.Drawing.Point(13, 13);
			this.LabelControl1.Name = "LabelControl1";
			this.LabelControl1.Size = new System.Drawing.Size(11, 13);
			this.LabelControl1.TabIndex = 0;
			this.LabelControl1.Text = "ID";
			//
			//LabelControl_ID
			//
			this.LabelControl_ID.Location = new System.Drawing.Point(86, 13);
			this.LabelControl_ID.Name = "LabelControl_ID";
			this.LabelControl_ID.Size = new System.Drawing.Size(23, 13);
			this.LabelControl_ID.TabIndex = 1;
			this.LabelControl_ID.Text = "NEW";
			//
			//LabelControl_description
			//
			this.LabelControl_description.Location = new System.Drawing.Point(13, 41);
			this.LabelControl_description.Name = "LabelControl_description";
			this.LabelControl_description.Size = new System.Drawing.Size(53, 13);
			this.LabelControl_description.TabIndex = 2;
			this.LabelControl_description.Text = "Description";
			//
			//TextEdit_description
			//
			this.TextEdit_description.Anchor = (AnchorStyles)((AnchorStyles.Top | AnchorStyles.Left) | AnchorStyles.Right);
			this.TextEdit_description.Location = new System.Drawing.Point(86, 38);
			this.TextEdit_description.Name = "TextEdit_description";
			this.TextEdit_description.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.False;
			this.TextEdit_description.Properties.MaxLength = 50;
			this.TextEdit_description.Size = new System.Drawing.Size(280, 20);
			this.TextEdit_description.TabIndex = 3;
			//
			//TextEdit_attributes
			//
			this.TextEdit_attributes.Anchor = (AnchorStyles)((AnchorStyles.Top | AnchorStyles.Left) | AnchorStyles.Right);
			this.TextEdit_attributes.Location = new System.Drawing.Point(86, 70);
			this.TextEdit_attributes.Name = "TextEdit_attributes";
			this.TextEdit_attributes.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.False;
			this.TextEdit_attributes.Properties.MaxLength = 50;
			this.TextEdit_attributes.Size = new System.Drawing.Size(280, 20);
			this.TextEdit_attributes.TabIndex = 5;
			//
			//LabelControl2
			//
			this.LabelControl2.Location = new System.Drawing.Point(13, 73);
			this.LabelControl2.Name = "LabelControl2";
			this.LabelControl2.Size = new System.Drawing.Size(48, 13);
			this.LabelControl2.TabIndex = 4;
			this.LabelControl2.Text = "Attributes";
			//
			//LabelControl3
			//
			this.LabelControl3.Location = new System.Drawing.Point(12, 105);
			this.LabelControl3.Name = "LabelControl3";
			this.LabelControl3.Size = new System.Drawing.Size(43, 13);
			this.LabelControl3.TabIndex = 6;
			this.LabelControl3.Text = "Grouping";
			//
			//ComboBoxEdit1
			//
			this.ComboBoxEdit1.Location = new System.Drawing.Point(86, 102);
			this.ComboBoxEdit1.Name = "ComboBoxEdit1";
			this.ComboBoxEdit1.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] { new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo) });
			this.ComboBoxEdit1.Properties.Items.AddRange(new object[] {
				"",
				"EXISTING",
				"NEW",
				"OTHER"
			});
			this.ComboBoxEdit1.Properties.Sorted = true;
			this.ComboBoxEdit1.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
			this.ComboBoxEdit1.Size = new System.Drawing.Size(83, 20);
			this.ComboBoxEdit1.TabIndex = 7;
			//
			//ComboBoxEdit2
			//
			this.ComboBoxEdit2.Location = new System.Drawing.Point(175, 102);
			this.ComboBoxEdit2.Name = "ComboBoxEdit2";
			this.ComboBoxEdit2.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] { new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo) });
			this.ComboBoxEdit2.Properties.CharacterCasing = CharacterCasing.Upper;
			this.ComboBoxEdit2.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
			this.ComboBoxEdit2.Size = new System.Drawing.Size(163, 20);
			this.ComboBoxEdit2.TabIndex = 8;
			//
			//ComboBoxEdit3
			//
			this.ComboBoxEdit3.Location = new System.Drawing.Point(344, 102);
			this.ComboBoxEdit3.Name = "ComboBoxEdit3";
			this.ComboBoxEdit3.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] { new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo) });
			this.ComboBoxEdit3.Properties.Items.AddRange(new object[] {
				"TYPE",
				"REASON",
				"REFERRAL",
				"DISPOSITION"
			});
			this.ComboBoxEdit3.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
			this.ComboBoxEdit3.Size = new System.Drawing.Size(100, 20);
			this.ComboBoxEdit3.TabIndex = 9;
			//
			//EditForm
			//
			this.AutoScaleDimensions = new System.Drawing.SizeF(6f, 13f);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(468, 136);
			this.Controls.Add(this.ComboBoxEdit3);
			this.Controls.Add(this.ComboBoxEdit2);
			this.Controls.Add(this.ComboBoxEdit1);
			this.Controls.Add(this.LabelControl3);
			this.Controls.Add(this.TextEdit_attributes);
			this.Controls.Add(this.LabelControl2);
			this.Controls.Add(this.TextEdit_description);
			this.Controls.Add(this.LabelControl_description);
			this.Controls.Add(this.LabelControl_ID);
			this.Controls.Add(this.LabelControl1);
			this.Name = "EditForm";
			this.ToolTipController1.SetSuperTip(this, null);
			this.Text = "FAQ Item";
			this.Controls.SetChildIndex(this.LabelControl1, 0);
			this.Controls.SetChildIndex(this.LabelControl_ID, 0);
			this.Controls.SetChildIndex(this.simpleButton_OK, 0);
			this.Controls.SetChildIndex(this.simpleButton_Cancel, 0);
			this.Controls.SetChildIndex(this.LabelControl_description, 0);
			this.Controls.SetChildIndex(this.TextEdit_description, 0);
			this.Controls.SetChildIndex(this.LabelControl2, 0);
			this.Controls.SetChildIndex(this.TextEdit_attributes, 0);
			this.Controls.SetChildIndex(this.LabelControl3, 0);
			this.Controls.SetChildIndex(this.ComboBoxEdit1, 0);
			this.Controls.SetChildIndex(this.ComboBoxEdit2, 0);
			this.Controls.SetChildIndex(this.ComboBoxEdit3, 0);
			((System.ComponentModel.ISupportInitialize)this.DxErrorProvider1).EndInit();
			((System.ComponentModel.ISupportInitialize)this.TextEdit_description.Properties).EndInit();
			((System.ComponentModel.ISupportInitialize)this.TextEdit_attributes.Properties).EndInit();
			((System.ComponentModel.ISupportInitialize)this.ComboBoxEdit1.Properties).EndInit();
			((System.ComponentModel.ISupportInitialize)this.ComboBoxEdit2.Properties).EndInit();
			((System.ComponentModel.ISupportInitialize)this.ComboBoxEdit3.Properties).EndInit();
			this.ResumeLayout(false);
			this.PerformLayout();
		}

		private DevExpress.XtraEditors.LabelControl LabelControl1;
		private DevExpress.XtraEditors.LabelControl LabelControl_ID;
		private DevExpress.XtraEditors.LabelControl LabelControl_description;
		private DevExpress.XtraEditors.TextEdit TextEdit_description;
		private DevExpress.XtraEditors.TextEdit TextEdit_attributes;
		private DevExpress.XtraEditors.LabelControl LabelControl2;
		private DevExpress.XtraEditors.LabelControl LabelControl3;
		private DevExpress.XtraEditors.ComboBoxEdit ComboBoxEdit1;
		private DevExpress.XtraEditors.ComboBoxEdit ComboBoxEdit2;
		private DevExpress.XtraEditors.ComboBoxEdit ComboBoxEdit3;
	}
}
