#region "Copyright 2000-2012 DebtPlus, L.L.C."
//{*******************************************************************}
//{                                                                   }
//{       DebtPlus Debt Management System                             }
//{                                                                   }
//{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
//{       ALL RIGHTS RESERVED                                         }
//{                                                                   }
//{   The entire contents of this file is protected by U.S. and       }
//{   International Copyright Laws. Unauthorized reproduction,        }
//{   reverse-engineering, and distribution of all or any portion of  }
//{   the code contained in this file is strictly prohibited and may  }
//{   result in severe civil and criminal penalties and will be       }
//{   prosecuted to the maximum extent possible under the law.        }
//{                                                                   }
//{   RESTRICTIONS                                                    }
//{                                                                   }
//{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
//{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
//{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
//{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
//{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
//{                                                                   }
//{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
//{   ADDITIONAL RESTRICTIONS.                                        }
//{                                                                   }
//{*******************************************************************}

#endregion "Copyright 2000-2012 DebtPlus, L.L.C."

using System;
using System.Linq;
using System.Text;
using DebtPlus.LINQ;

namespace DebtPlus.UI.TableAdministration.CS.FaqItems
{
    internal partial class EditForm : Templates.EditTemplateForm
    {
        // Local storage
        private faq_item record = null;

        private System.Collections.Generic.List<faq_item> colRecords = null;

        /// <summary>
        /// Initialize the new form class
        /// </summary>
        internal EditForm()
            : base()
        {
            InitializeComponent();
        }

        /// <summary>
        /// Initialize the new form class
        /// </summary>
        internal EditForm(System.Collections.Generic.List<faq_item> colRecords, faq_item record)
            : this()
        {
            this.colRecords = colRecords;
            this.record = record;
            RegisterHandlers();
        }

        /// <summary>
        /// Register the event handlers
        /// </summary>
        private void RegisterHandlers()
        {
            Load += EditForm_Load;
            ComboBoxEdit1.SelectedIndexChanged += ComboBoxEdit1_SelectedIndexChanged;
            ComboBoxEdit3.SelectedIndexChanged += ComboBoxEdit3_SelectedIndexChanged;
            TextEdit_description.EditValueChanged += FormChanged;
            TextEdit_attributes.EditValueChanged += FormChanged;
            ComboBoxEdit1.SelectedIndexChanged += FormChanged;
            ComboBoxEdit2.SelectedIndexChanged += FormChanged;
            ComboBoxEdit3.SelectedIndexChanged += FormChanged;
        }

        /// <summary>
        /// Remove the event handler registrations
        /// </summary>
        private void UnRegisterHandlers()
        {
            Load -= EditForm_Load;
            ComboBoxEdit1.SelectedIndexChanged -= ComboBoxEdit1_SelectedIndexChanged;
            ComboBoxEdit3.SelectedIndexChanged -= ComboBoxEdit3_SelectedIndexChanged;
            TextEdit_description.EditValueChanged -= FormChanged;
            TextEdit_attributes.EditValueChanged -= FormChanged;
            ComboBoxEdit1.SelectedIndexChanged -= FormChanged;
            ComboBoxEdit2.SelectedIndexChanged -= FormChanged;
            ComboBoxEdit3.SelectedIndexChanged -= FormChanged;
        }

        /// <summary>
        /// Process the LOAD event on the form
        /// </summary>
        private void EditForm_Load(object sender, EventArgs e)
        {
            UnRegisterHandlers();
            try
            {
                // Define the 2nd level items from the possible lists
                ComboBoxEdit2.Properties.Items.Clear();
                ComboBoxEdit2.Properties.Items.Add(string.Empty);

                // Find the items that are to go into the first ComboBox.
                foreach (var selItem in colRecords.FindAll(s => (new string[] { "TYPE", "NEW TYPE", "EXISTING TYPE" }).Contains(s.grouping)).OrderBy(s => s.attributes).Select(s => s.attributes).Distinct())
                {
                    if (!string.IsNullOrWhiteSpace(selItem))
                    {
                        ComboBoxEdit2.Properties.Items.Add(selItem);
                    }
                }

                LabelControl_ID.Text = (record.Id < 1) ? "NEW" : record.Id.ToString();

                TextEdit_description.EditValue = record.description;
                TextEdit_attributes.EditValue = record.attributes;

                // Set the grouping information
                setGrouping((record.grouping ?? string.Empty).ToUpper().Trim());

                // Process the changes in the combobox entries so that the lists may be corrected.
                process_ComboBoxEdit1_Change();
                process_ComboBoxEdit3_Change();

                // Enable the functions
                ComboBoxEdit1.Enabled = ShouldEnable1();
                ComboBoxEdit2.Enabled = ShouldEnable2();
                ComboBoxEdit3.Enabled = ShouldEnable3();

                simpleButton_OK.Enabled = !HasErrors();
            }
            finally
            {
                RegisterHandlers();
            }
        }

        /// <summary>
        /// Change the grouping information that is displayed
        /// </summary>
        /// <param name="value"></param>
        private void setGrouping(string value)
        {
            string string1 = string.Empty;
            string string2 = string.Empty;
            string string3 = value;

            // Remove the last word and hold it in string3.
            // Move the remainder to string2
            Int32 pos = string3.LastIndexOf(' ');
            if (pos >= 0)
            {
                string2 = string3.Substring(0, pos).Trim();
                string3 = string3.Substring(pos).Trim();
            }

            // Assume that the record is "TYPE" if the record is not a valid term.
            if (ComboBoxEdit3.Properties.Items.IndexOf(string3) < 0)
            {
                string3 = "TYPE";
                string2 = string.Empty;
            }

            // "OTHER" is only "DISPOSITION"
            if (string2 == "OTHER" && string3 == "DISPOSITION")
            {
                string1 = string2;
                string2 = string.Empty;
            }
            else if (string3 == "TYPE")
            {
                string1 = string2;
                string2 = string.Empty;
            }
            else
            {
                // If the string2 matches a valid type field then that is all.
                // Otherwise, remove the leading "NEW" or "EXISTING" from the record
                if (ComboBoxEdit2.Properties.Items.IndexOf(string2) < 0)
                {
                    pos = string2.IndexOf(' ');
                    if (pos >= 0)
                    {
                        string newString1 = string2.Substring(0, pos).Trim();
                        if (ComboBoxEdit1.Properties.Items.IndexOf(newString1) >= 0)
                        {
                            string2 = string2.Substring(pos).Trim();
                            string1 = newString1;
                        }
                    }
                }
            }

            // Load the items into the list controls
            ComboBoxEdit3.SelectedIndex = ComboBoxEdit3.Properties.Items.IndexOf(string3);
            ComboBoxEdit2.SelectedIndex = ComboBoxEdit2.Properties.Items.IndexOf(string2);
            ComboBoxEdit1.SelectedIndex = ComboBoxEdit1.Properties.Items.IndexOf(string1);

            // Define the text fields accordingly
            ComboBoxEdit3.Text = string3;
            ComboBoxEdit2.Text = string2;
            ComboBoxEdit1.Text = string1;
        }

        /// <summary>
        /// Process the click on the OK button
        /// </summary>
        protected override void simpleButton_OK_Click(object sender, EventArgs e)
        {
            record.description = DebtPlus.Utils.Nulls.v_String(TextEdit_description.EditValue);
            record.attributes = DebtPlus.Utils.Nulls.v_String(TextEdit_attributes.EditValue);
            record.grouping = getGrouping();

            base.simpleButton_OK_Click(sender, e);
        }

        /// <summary>
        /// Set the grouping field into the controls. It needs to be split into three items.
        /// </summary>
        private string getGrouping()
        {
            string value1 = ComboBoxEdit1.Text.Trim();
            string value2 = ComboBoxEdit2.Text.Trim();
            string value3 = ComboBoxEdit3.Text.Trim();

            StringBuilder sb = new StringBuilder();
            if (!string.IsNullOrWhiteSpace(value1))
            {
                sb.Append(" ");
                sb.Append(value1);
            }

            if (!string.IsNullOrWhiteSpace(value2))
            {
                sb.Append(" ");
                sb.Append(value2);
            }

            if (!string.IsNullOrWhiteSpace(value3))
            {
                sb.Append(" ");
                sb.Append(value3);
            }

            if (sb.Length > 0)
            {
                sb.Remove(0, 1);
            }
            return sb.ToString();
        }

        /// <summary>
        /// Handle the change in the first grouping field
        /// </summary>
        private void ComboBoxEdit1_SelectedIndexChanged(object sender, EventArgs e)
        {
            process_ComboBoxEdit1_Change();
        }

        /// <summary>
        /// Handle the change in the first grouping field
        /// </summary>
        private void process_ComboBoxEdit1_Change()
        {
            // Change the record to "OTHER DISPOSITION" if "OTHER" is chosen
            if (ComboBoxEdit1.Text == "OTHER")
            {
                ComboBoxEdit2.SelectedIndex = 0;
                ComboBoxEdit3.SelectedIndex = ComboBoxEdit3.Properties.Items.IndexOf("DISPOSITION");
                process_ComboBoxEdit3_Change();
            }

            ComboBoxEdit1.Enabled = ShouldEnable1();
            ComboBoxEdit2.Enabled = ShouldEnable2();
            ComboBoxEdit3.Enabled = ShouldEnable3();
        }

        /// <summary>
        /// Handle the change in the 3rd grouping field
        /// </summary>
        private void ComboBoxEdit3_SelectedIndexChanged(object sender, EventArgs e)
        {
            process_ComboBoxEdit3_Change();
        }

        /// <summary>
        /// Handle the change in the 3rd grouping field
        /// </summary>
        private void process_ComboBoxEdit3_Change()
        {
            if (ComboBoxEdit3.Text == "TYPE")
            {
                ComboBoxEdit2.SelectedIndex = 0;
            }

            ComboBoxEdit1.Enabled = ShouldEnable1();
            ComboBoxEdit2.Enabled = ShouldEnable2();
            ComboBoxEdit3.Enabled = ShouldEnable3();
        }

        /// <summary>
        /// Determine if the input controls define a valid record
        /// </summary>
        private bool HasErrors()
        {
            // A description is required
            if (string.IsNullOrWhiteSpace(TextEdit_description.Text))
            {
                return true;
            }

            // There must be a 3rd record for the grouping.
            if (ComboBoxEdit3.SelectedIndex < 0)
            {
                return true;
            }

            // If the 3rd record is Type then we need a valid 2nd record
            if (ComboBoxEdit3.Text == "TYPE" && string.IsNullOrWhiteSpace(TextEdit_attributes.Text))
            {
                return true;
            }

            // Finally, the selection of the comboboxes must match the type field
            string needed_attributes = ComboBoxEdit2.Text;
            if (!string.IsNullOrWhiteSpace(needed_attributes))
            {
                string needed_grouping = ComboBoxEdit1.Text.Trim().ToUpper();

                // It may be a TYPE or a NEW TYPE. If so, then we are good
                if (colRecords.Exists(s => s.attributes == needed_attributes && string.Compare((s.grouping ?? string.Empty), needed_grouping + " TYPE", true) == 0))
                {
                    return false;
                }

                if (colRecords.Exists(s => s.attributes == needed_attributes && s.grouping == "TYPE"))
                {
                    return false;
                }

                return true;
            }

            // All is well with the world.
            return false;
        }

        /// <summary>
        /// Process a change in the controls
        /// </summary>
        private void FormChanged(object Sender, EventArgs e)
        {
            simpleButton_OK.Enabled = !HasErrors();
        }

        /// <summary>
        /// Should we enable the combobox1 control?
        /// </summary>
        private bool ShouldEnable1()
        {
            return true;
        }

        /// <summary>
        /// Should we enable the combobox2 control?
        /// </summary>
        /// <returns></returns>
        private bool ShouldEnable2()
        {
            return ShouldEnable3() && (ComboBoxEdit3.Text != "TYPE");
        }

        /// <summary>
        /// Should we enable the combobox3 control?
        /// </summary>
        /// <returns></returns>
        private bool ShouldEnable3()
        {
            return ComboBoxEdit1.Text != "OTHER";
        }
    }
}