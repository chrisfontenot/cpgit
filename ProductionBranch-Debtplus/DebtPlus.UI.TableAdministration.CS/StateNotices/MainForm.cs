﻿using System;
using System.Linq;
using System.Windows.Forms;
using DebtPlus.LINQ;

namespace DebtPlus.UI.TableAdministration.CS.StateNotices
{
    public partial class MainForm : Templates.MainForm
    {
        private System.Collections.Generic.List<StateMessage> colRecords = new System.Collections.Generic.List<StateMessage>();

        public MainForm()
        {
            InitializeComponent();

            // Set the pointer to the formatter
            gridColumn_MessageType.DisplayFormat.Format = new Format_MessageType();
            gridColumn_MessageType.GroupFormat.Format = new Format_MessageType();

            if (!InDesignMode)
            {
                RegisterHandlers();

                // Load the list of states / countries into the dropdown
                lookUpEdit_State.Properties.DataSource = DebtPlus.LINQ.Cache.state.getList();
            }
        }

        private void RegisterHandlers()
        {
            Load += MainForm_Load;
            lookUpEdit_State.EditValueChanged += LoadItems;
            dateEdit_Current.EditValueChanged += LoadItems;
        }

        private void UnRegisterHandlers()
        {
            Load -= MainForm_Load;
            lookUpEdit_State.EditValueChanged -= LoadItems;
            dateEdit_Current.EditValueChanged -= LoadItems;
        }

        private void MainForm_Load(object sender, EventArgs e)
        {
            UnRegisterHandlers();
            try
            {
                // Default the settings to the form information
                dateEdit_Current.DateTime = DebtPlus.LINQ.BusinessContext.getdate();
                lookUpEdit_State.EditValue = DebtPlus.LINQ.Cache.state.getDefault();
                LoadItems();
            }
            finally
            {
                RegisterHandlers();
            }
        }

        private void LoadItems()
        {
            UnRegisterHandlers();
            try
            {
                LoadItems(DebtPlus.Utils.Nulls.v_Int32(lookUpEdit_State.EditValue), Convert.ToDateTime(dateEdit_Current.DateTime));
            }
            finally
            {
                RegisterHandlers();
            }
        }

        private void LoadItems(object sender, EventArgs e)
        {
            UnRegisterHandlers();
            try
            {
                LoadItems(DebtPlus.Utils.Nulls.v_Int32(lookUpEdit_State.EditValue), Convert.ToDateTime(dateEdit_Current.DateTime));
            }
            finally
            {
                RegisterHandlers();
            }
        }

        /// <summary>
        ///     Load the grid with the appropriate items
        /// </summary>
        /// <param name="state">State ID or NULL if none</param>
        /// <param name="current">Current effective date or NULL if none</param>
        private void LoadItems(Int32? state, DateTime? current)
        {
            // If there is nothing to display then empty the grid control
            if (!state.HasValue || !current.HasValue)
            {
                gridControl1.DataSource = null;
                gridControl1.RefreshDataSource();
                return;
            }

            try
            {
                using (var bc = new BusinessContext())
                {
                    // Build a list of the state items with the effective date and date created
                    System.Collections.Generic.List<DebtPlus.LINQ.StateMessage> dbRecords = (from st in bc.StateMessages where st.State == state.Value && st.Effective <= current.Value.AddDays(1).Date select st).ToList();

                    // Ensure that we only get one item for each type in the list. Take the last one.
                    colRecords = (from st in dbRecords
                                  group st by st.StateMessageType into grp
                                  select grp.OrderByDescending(a => a.StateMessageType).ThenByDescending(a => a.Effective).First()
                                  ).ToList();

                    gridControl1.DataSource = colRecords;
                    gridControl1.RefreshDataSource();
                }
            }
            catch (System.Exception ex)
            {
                DebtPlus.UI.Common.ErrorHandling.HandleErrors(ex, "Error reading State Messages table");
            }
        }

        /// <summary>
        ///     Edit (or create) a row in the table
        /// </summary>
        protected override void UpdateRecord(object obj)
        {
            StateMessage record = obj as StateMessage;
            if (record == null)
            {
                return;
            }

            using (Edit_Form frm = new Edit_Form(record))
            {
                if (frm.ShowDialog() != System.Windows.Forms.DialogResult.OK)
                {
                    return;
                }

                using (BusinessContext bc = new BusinessContext())
                {
                    var q = (from rec in bc.StateMessages where rec.Id == record.Id select rec).FirstOrDefault();
                    if (q != null)
                    {
                        q.Details = record.Details;
                        q.Effective = record.Effective;
                        q.Reason = record.Reason;
                        q.State = record.State;
                        q.StateMessageType = record.StateMessageType;

                        if (bc.GetChangeSet().Updates.Count > 0)
                        {
                            // Record the person and date when the record was changed.
                            q.date_updated = DebtPlus.LINQ.BusinessContext.getdate();
                            q.updated_by = DebtPlus.LINQ.BusinessContext.suser_sname();

                            // Do the change operation now.
                            bc.SubmitChanges();
                            LoadItems();
                            gridView1.RefreshData();
                        }
                    }
                }
            }
        }

        protected override void DeleteRecord(object obj)
        {
            StateMessage record = obj as StateMessage;
            if (record == null)
            {
                return;
            }

            if (DebtPlus.Data.Prompts.RequestConfirmation_Delete() != DialogResult.Yes)
            {
                return;
            }

            using (BusinessContext bc = new BusinessContext())
            {
                var q = (from rec in bc.StateMessages where rec.Id == record.Id select rec).FirstOrDefault();
                if (q != null)
                {
                    bc.StateMessages.DeleteOnSubmit(q);
                    bc.SubmitChanges();
                    LoadItems();
                    gridView1.RefreshData();
                }
            }
        }

        protected override void CreateRecord()
        {
            StateMessage record = new StateMessage()
            {
                Effective = DateTime.Now.Date,
                State = Convert.ToInt32(lookUpEdit_State.EditValue)
            };

            using (Edit_Form frm = new Edit_Form(record))
            {
                if (frm.ShowDialog() != System.Windows.Forms.DialogResult.OK)
                {
                    return;
                }

                using (BusinessContext bc = new BusinessContext())
                {
                    // Record the person and date when the record was changed.
                    record.date_updated = DebtPlus.LINQ.BusinessContext.getdate();
                    record.updated_by = DebtPlus.LINQ.BusinessContext.suser_sname();

                    bc.StateMessages.InsertOnSubmit(record);
                    bc.SubmitChanges();
                    LoadItems();
                    gridView1.RefreshData();
                }
            }
        }

        /// <summary>
        ///     Class to format the MessageType field from a number to the corresponding description string
        /// </summary>
        internal class Format_MessageType : IFormatProvider, ICustomFormatter
        {
            internal Format_MessageType()
            {
            }

            /// <summary>
            ///     Format the value from a number to the corresponding string.
            /// </summary>
            public string Format(string format, object arg, IFormatProvider formatProvider)
            {
                var answer = string.Empty;
                if (arg != null && arg != DBNull.Value)
                {
                    StateMessageType smt = DebtPlus.LINQ.Cache.StateMesageType.getList().Find(sm => sm.Id == Convert.ToInt32(arg));
                    if (smt != null)
                    {
                        answer = smt.Description;
                    }
                }
                return answer;
            }

            /// <summary>
            ///     Return the format provider for the given type. Since we are only one item, it is always "us".
            /// </summary>
            public object GetFormat(Type formatType)
            {
                return this;
            }
        }
    }
}