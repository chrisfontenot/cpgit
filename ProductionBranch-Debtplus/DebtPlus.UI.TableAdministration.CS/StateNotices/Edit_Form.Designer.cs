namespace DebtPlus.UI.TableAdministration.CS.StateNotices
{
    partial class Edit_Form
    {
		//Form overrides dispose to clean up the component list.
		[System.Diagnostics.DebuggerNonUserCode]
		protected override void Dispose(bool disposing)
		{
			try 
            {
				if (disposing)
                {
                    if (components != null) components.Dispose();
				}
                components = null;
            }
    	    finally
            {
				base.Dispose(disposing);
			}
		}

		//Required by the Windows Form Designer
		private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.layoutControl1 = new DevExpress.XtraLayout.LayoutControl();
            this.simpleButton_Cancel = new DevExpress.XtraEditors.SimpleButton();
            this.memoEdit_Details = new DevExpress.XtraEditors.MemoEdit();
            this.simpleButton_OK = new DevExpress.XtraEditors.SimpleButton();
            this.memoEdit_Reason = new DevExpress.XtraEditors.MemoEdit();
            this.lookUpEdit_Type = new DevExpress.XtraEditors.LookUpEdit();
            this.dateEdit_Effective = new DevExpress.XtraEditors.DateEdit();
            this.layoutControlGroup1 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem1 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem2 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem4 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem5 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem6 = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem_Left = new DevExpress.XtraLayout.EmptySpaceItem();
            this.emptySpaceItem_Right = new DevExpress.XtraLayout.EmptySpaceItem();
            this.layoutControlItem3 = new DevExpress.XtraLayout.LayoutControlItem();
            ((System.ComponentModel.ISupportInitialize)(this.DxErrorProvider1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl1)).BeginInit();
            this.layoutControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.memoEdit_Details.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.memoEdit_Reason.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lookUpEdit_Type.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEdit_Effective.Properties.VistaTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEdit_Effective.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem_Left)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem_Right)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem3)).BeginInit();
            this.SuspendLayout();
            // 
            // DefaultLookAndFeel1
            // 
            this.DefaultLookAndFeel1.LookAndFeel.SkinName = "McSkin";
            // 
            // layoutControl1
            // 
            this.layoutControl1.Controls.Add(this.simpleButton_Cancel);
            this.layoutControl1.Controls.Add(this.memoEdit_Details);
            this.layoutControl1.Controls.Add(this.simpleButton_OK);
            this.layoutControl1.Controls.Add(this.memoEdit_Reason);
            this.layoutControl1.Controls.Add(this.lookUpEdit_Type);
            this.layoutControl1.Controls.Add(this.dateEdit_Effective);
            this.layoutControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.layoutControl1.Location = new System.Drawing.Point(0, 0);
            this.layoutControl1.Name = "layoutControl1";
            this.layoutControl1.Root = this.layoutControlGroup1;
            this.layoutControl1.Size = new System.Drawing.Size(441, 266);
            this.layoutControl1.TabIndex = 0;
            this.layoutControl1.Text = "layoutControl1";
            // 
            // simpleButton_Cancel
            // 
            this.simpleButton_Cancel.Location = new System.Drawing.Point(225, 231);
            this.simpleButton_Cancel.MaximumSize = new System.Drawing.Size(75, 23);
            this.simpleButton_Cancel.MinimumSize = new System.Drawing.Size(75, 23);
            this.simpleButton_Cancel.Name = "simpleButton_Cancel";
            this.simpleButton_Cancel.Size = new System.Drawing.Size(75, 23);
            this.simpleButton_Cancel.StyleController = this.layoutControl1;
            this.simpleButton_Cancel.TabIndex = 11;
            this.simpleButton_Cancel.Text = "&Cancel";
            // 
            // memoEdit_Details
            // 
            this.memoEdit_Details.Location = new System.Drawing.Point(92, 36);
            this.memoEdit_Details.Name = "memoEdit_Details";
            this.memoEdit_Details.Size = new System.Drawing.Size(337, 141);
            this.memoEdit_Details.StyleController = this.layoutControl1;
            this.memoEdit_Details.TabIndex = 7;
            // 
            // simpleButton_OK
            // 
            this.simpleButton_OK.Location = new System.Drawing.Point(146, 231);
            this.simpleButton_OK.MaximumSize = new System.Drawing.Size(75, 23);
            this.simpleButton_OK.MinimumSize = new System.Drawing.Size(75, 23);
            this.simpleButton_OK.Name = "simpleButton_OK";
            this.simpleButton_OK.Size = new System.Drawing.Size(75, 23);
            this.simpleButton_OK.StyleController = this.layoutControl1;
            this.simpleButton_OK.TabIndex = 10;
            this.simpleButton_OK.Text = "&OK";
            // 
            // memoEdit_Reason
            // 
            this.memoEdit_Reason.Location = new System.Drawing.Point(92, 181);
            this.memoEdit_Reason.Name = "memoEdit_Reason";
            this.memoEdit_Reason.Size = new System.Drawing.Size(337, 46);
            this.memoEdit_Reason.StyleController = this.layoutControl1;
            this.memoEdit_Reason.TabIndex = 6;
            // 
            // lookUpEdit_Type
            // 
            this.lookUpEdit_Type.Location = new System.Drawing.Point(92, 12);
            this.lookUpEdit_Type.Name = "lookUpEdit_Type";
            this.lookUpEdit_Type.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.lookUpEdit_Type.Properties.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("Id", "ID", 20, DevExpress.Utils.FormatType.Numeric, "f0", false, DevExpress.Utils.HorzAlignment.Default),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("Description", "description")});
            this.lookUpEdit_Type.Properties.DisplayMember = "Description";
            this.lookUpEdit_Type.Properties.NullText = "";
            this.lookUpEdit_Type.Properties.ShowFooter = false;
            this.lookUpEdit_Type.Properties.ShowHeader = false;
            this.lookUpEdit_Type.Properties.ValueMember = "Id";
            this.lookUpEdit_Type.Size = new System.Drawing.Size(202, 20);
            this.lookUpEdit_Type.StyleController = this.layoutControl1;
            this.lookUpEdit_Type.TabIndex = 4;
            // 
            // dateEdit_Effective
            // 
            this.dateEdit_Effective.EditValue = new System.DateTime(2012, 10, 23, 6, 51, 50, 0);
            this.dateEdit_Effective.Location = new System.Drawing.Point(346, 12);
            this.dateEdit_Effective.MinimumSize = new System.Drawing.Size(80, 0);
            this.dateEdit_Effective.Name = "dateEdit_Effective";
            this.dateEdit_Effective.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dateEdit_Effective.Properties.VistaTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.dateEdit_Effective.Size = new System.Drawing.Size(83, 20);
            this.dateEdit_Effective.StyleController = this.layoutControl1;
            this.dateEdit_Effective.TabIndex = 5;
            // 
            // layoutControlGroup1
            // 
            this.layoutControlGroup1.CustomizationFormText = "layoutControlGroup1";
            this.layoutControlGroup1.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.True;
            this.layoutControlGroup1.GroupBordersVisible = false;
            this.layoutControlGroup1.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem1,
            this.layoutControlItem2,
            this.layoutControlItem4,
            this.layoutControlItem5,
            this.layoutControlItem6,
            this.emptySpaceItem_Left,
            this.emptySpaceItem_Right,
            this.layoutControlItem3});
            this.layoutControlGroup1.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup1.Name = "layoutControlGroup1";
            this.layoutControlGroup1.Size = new System.Drawing.Size(441, 266);
            this.layoutControlGroup1.Text = "layoutControlGroup1";
            this.layoutControlGroup1.TextVisible = false;
            // 
            // layoutControlItem1
            // 
            this.layoutControlItem1.Control = this.lookUpEdit_Type;
            this.layoutControlItem1.CustomizationFormText = "type";
            this.layoutControlItem1.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem1.Name = "layoutControlItem1";
            this.layoutControlItem1.Size = new System.Drawing.Size(286, 24);
            this.layoutControlItem1.Text = "type";
            this.layoutControlItem1.TextSize = new System.Drawing.Size(76, 13);
            // 
            // layoutControlItem2
            // 
            this.layoutControlItem2.Control = this.dateEdit_Effective;
            this.layoutControlItem2.CustomizationFormText = "Effective";
            this.layoutControlItem2.Location = new System.Drawing.Point(286, 0);
            this.layoutControlItem2.MaxSize = new System.Drawing.Size(135, 0);
            this.layoutControlItem2.MinSize = new System.Drawing.Size(135, 24);
            this.layoutControlItem2.Name = "layoutControlItem2";
            this.layoutControlItem2.Size = new System.Drawing.Size(135, 24);
            this.layoutControlItem2.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem2.Text = "Effective";
            this.layoutControlItem2.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.AutoSize;
            this.layoutControlItem2.TextSize = new System.Drawing.Size(43, 13);
            this.layoutControlItem2.TextToControlDistance = 5;
            // 
            // layoutControlItem_simpleButton_Cancel
            // 
            this.layoutControlItem4.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem4.AppearanceItemCaption.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Top;
            this.layoutControlItem4.Control = this.memoEdit_Details;
            this.layoutControlItem4.CustomizationFormText = "Detail Information";
            this.layoutControlItem4.Location = new System.Drawing.Point(0, 24);
            this.layoutControlItem4.Name = "layoutControlItem_simpleButton_Cancel";
            this.layoutControlItem4.Size = new System.Drawing.Size(421, 145);
            this.layoutControlItem4.Text = "Details";
            this.layoutControlItem4.TextSize = new System.Drawing.Size(76, 13);
            // 
            // layoutControlItem_New
            // 
            this.layoutControlItem5.Control = this.simpleButton_OK;
            this.layoutControlItem5.CustomizationFormText = "layoutControlItem_New";
            this.layoutControlItem5.Location = new System.Drawing.Point(134, 219);
            this.layoutControlItem5.Name = "layoutControlItem_New";
            this.layoutControlItem5.Size = new System.Drawing.Size(79, 27);
            this.layoutControlItem5.Text = "layoutControlItem_New";
            this.layoutControlItem5.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem5.TextToControlDistance = 0;
            this.layoutControlItem5.TextVisible = false;
            // 
            // layoutControlItem6
            // 
            this.layoutControlItem6.Control = this.simpleButton_Cancel;
            this.layoutControlItem6.CustomizationFormText = "layoutControlItem6";
            this.layoutControlItem6.Location = new System.Drawing.Point(213, 219);
            this.layoutControlItem6.Name = "layoutControlItem6";
            this.layoutControlItem6.Size = new System.Drawing.Size(79, 27);
            this.layoutControlItem6.Text = "layoutControlItem6";
            this.layoutControlItem6.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem6.TextToControlDistance = 0;
            this.layoutControlItem6.TextVisible = false;
            // 
            // emptySpaceItem_Left
            // 
            this.emptySpaceItem_Left.AllowHotTrack = false;
            this.emptySpaceItem_Left.CustomizationFormText = "emptySpaceItem_Left";
            this.emptySpaceItem_Left.Location = new System.Drawing.Point(0, 219);
            this.emptySpaceItem_Left.Name = "emptySpaceItem_Left";
            this.emptySpaceItem_Left.Size = new System.Drawing.Size(134, 27);
            this.emptySpaceItem_Left.Text = "emptySpaceItem_Left";
            this.emptySpaceItem_Left.TextSize = new System.Drawing.Size(0, 0);
            // 
            // emptySpaceItem_Right
            // 
            this.emptySpaceItem_Right.AllowHotTrack = false;
            this.emptySpaceItem_Right.CustomizationFormText = "emptySpaceItem_Right";
            this.emptySpaceItem_Right.Location = new System.Drawing.Point(292, 219);
            this.emptySpaceItem_Right.Name = "emptySpaceItem_Right";
            this.emptySpaceItem_Right.Size = new System.Drawing.Size(129, 27);
            this.emptySpaceItem_Right.Text = "emptySpaceItem_Right";
            this.emptySpaceItem_Right.TextSize = new System.Drawing.Size(0, 0);
            // 
            // layoutControlItem_Cancel
            // 
            this.layoutControlItem3.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem3.AppearanceItemCaption.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Top;
            this.layoutControlItem3.Control = this.memoEdit_Reason;
            this.layoutControlItem3.CustomizationFormText = "Change Reason";
            this.layoutControlItem3.Location = new System.Drawing.Point(0, 169);
            this.layoutControlItem3.Name = "layoutControlItem_Cancel";
            this.layoutControlItem3.Size = new System.Drawing.Size(421, 50);
            this.layoutControlItem3.Text = "Change Reason";
            this.layoutControlItem3.TextSize = new System.Drawing.Size(76, 13);
            // 
            // Edit_Form
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(441, 266);
            this.Controls.Add(this.layoutControl1);
            this.Name = "Edit_Form";
            this.Text = "State Notice";
            ((System.ComponentModel.ISupportInitialize)(this.DxErrorProvider1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl1)).EndInit();
            this.layoutControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.memoEdit_Details.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.memoEdit_Reason.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lookUpEdit_Type.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEdit_Effective.Properties.VistaTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEdit_Effective.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem_Left)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem_Right)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem3)).EndInit();
            this.ResumeLayout(false);

        }


        private DevExpress.XtraLayout.LayoutControl layoutControl1;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup1;
        private DevExpress.XtraEditors.MemoEdit memoEdit_Details;
        private DevExpress.XtraEditors.MemoEdit memoEdit_Reason;
        private DevExpress.XtraEditors.DateEdit dateEdit_Effective;
        private DevExpress.XtraEditors.LookUpEdit lookUpEdit_Type;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem1;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem2;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem3;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem4;
        private DevExpress.XtraEditors.SimpleButton simpleButton_Cancel;
        private DevExpress.XtraEditors.SimpleButton simpleButton_OK;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem5;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem6;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem_Left;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem_Right;
    }
}