using System;
using DebtPlus.Data.Forms;
using DebtPlus.LINQ;

namespace DebtPlus.UI.TableAdministration.CS.StateNotices
{
    internal partial class Edit_Form : DebtPlusForm
    {
        private readonly StateMessage record;

        internal Edit_Form(StateMessage record)
        {
            this.record = record;
            InitializeComponent();
            RegisterHandlers();

            lookUpEdit_Type.Properties.DataSource = DebtPlus.LINQ.Cache.StateMesageType.getList();
        }

        private void RegisterHandlers()
        {
            Load += Edit_Form_Load;
            layoutControl1.Resize += layoutControl1_Resize;
            memoEdit_Details.TextChanged += memoEdit_Details_TextChanged;
            simpleButton_OK.Click += simpleButton_OK_Click;
            simpleButton_Cancel.Click += simpleButton_Cancel_Click;
        }

        private void UnRegisterHandlers()
        {
            Load -= Edit_Form_Load;
            layoutControl1.Resize -= layoutControl1_Resize;
            memoEdit_Details.TextChanged -= memoEdit_Details_TextChanged;
            simpleButton_OK.Click -= simpleButton_OK_Click;
            simpleButton_Cancel.Click -= simpleButton_Cancel_Click;
        }

        private void Edit_Form_Load(object sender, EventArgs e)
        {
            UnRegisterHandlers();
            try
            {
                // Load the form with the values from the record
                lookUpEdit_Type.EditValue = record.StateMessageType;
                dateEdit_Effective.EditValue = record.Effective;
                memoEdit_Reason.EditValue = record.Reason;
                memoEdit_Details.EditValue = record.Details;

                // Restore the previous form location and size
                base.LoadPlacement("Tables.Update.StateNotices");

                // Center the buttons.
                emptySpaceItem_Left.Width = (emptySpaceItem_Left.Width + emptySpaceItem_Right.Width) / 2;

                // Enable or disable the OK button
                simpleButton_OK.Enabled = !HasErrors();
            }
            finally
            {
                RegisterHandlers();
            }
        }

        /// <summary>
        /// Handle the CLICK event on the OK button
        /// </summary>
        protected virtual void simpleButton_OK_Click(object sender, EventArgs e)
        {
            record.StateMessageType = Convert.ToInt32(lookUpEdit_Type.EditValue);
            record.Effective = Convert.ToDateTime(dateEdit_Effective.EditValue);
            record.Reason = Convert.ToString(memoEdit_Reason.EditValue);
            record.Details = Convert.ToString(memoEdit_Details.EditValue);

            DialogResult = System.Windows.Forms.DialogResult.OK;
        }

        /// <summary>
        /// Handle the CLICK event on the CANCEL button
        /// </summary>
        protected virtual void simpleButton_Cancel_Click(object sender, EventArgs e)
        {
            DialogResult = System.Windows.Forms.DialogResult.Cancel;
        }

        private void layoutControl1_Resize(object sender, EventArgs e)
        {
            emptySpaceItem_Left.Width = (emptySpaceItem_Left.Width + emptySpaceItem_Right.Width) / 2;
        }

        private void memoEdit_Details_TextChanged(object sender, EventArgs e)
        {
            simpleButton_OK.Enabled = !HasErrors();
        }

        private bool HasErrors()
        {
            return memoEdit_Details.Text.Trim() == string.Empty;
        }

        private void Edit_Form_Dispose(bool disposing)
        {
            if (!DesignMode)
            {
                UnRegisterHandlers();
            }
        }
    }
}