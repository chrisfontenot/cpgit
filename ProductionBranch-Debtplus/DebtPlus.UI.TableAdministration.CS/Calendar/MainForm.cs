#region "Copyright 2000-2012 DebtPlus, L.L.C."
//{*******************************************************************}
//{                                                                   }
//{       DebtPlus Debt Management System                             }
//{                                                                   }
//{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
//{       ALL RIGHTS RESERVED                                         }
//{                                                                   }
//{   The entire contents of this file is protected by U.S. and       }
//{   International Copyright Laws. Unauthorized reproduction,        }
//{   reverse-engineering, and distribution of all or any portion of  }
//{   the code contained in this file is strictly prohibited and may  }
//{   result in severe civil and criminal penalties and will be       }
//{   prosecuted to the maximum extent possible under the law.        }
//{                                                                   }
//{   RESTRICTIONS                                                    }
//{                                                                   }
//{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
//{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
//{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
//{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
//{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
//{                                                                   }
//{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
//{   ADDITIONAL RESTRICTIONS.                                        }
//{                                                                   }
//{*******************************************************************}

#endregion "Copyright 2000-2012 DebtPlus, L.L.C."

using System;
using System.Linq;
using DebtPlus.LINQ;

namespace DebtPlus.UI.TableAdministration.CS.Calendar
{
    public partial class MainForm : Templates.MainForm
    {
        /// <summary>
        /// List of the records to be edited in the program
        /// </summary>
        private System.Collections.Generic.List<DebtPlus.LINQ.Calendar> colRecords;

        private BusinessContext bc = new BusinessContext();

        /// <summary>
        /// Initialize the new class
        /// </summary>
        public MainForm()
            : base()
        {
            InitializeComponent();
            RegisterHandlers();
        }

        private void RegisterHandlers()
        {
            Load += MainForm_Load;
        }

        private void UnRegisterHandlers()
        {
            Load -= MainForm_Load;
        }

        /// <summary>
        /// Process the load event on the form
        /// </summary>
        private void MainForm_Load(object sender, System.EventArgs e)
        {
            try
            {
                // Retrieve the list of menu items
                colRecords = bc.Calendars.ToList();
                gridControl1.DataSource = colRecords;
            }
            catch (Exception ex)
            {
                DebtPlus.UI.Common.ErrorHandling.HandleErrors(ex, "Error retrieving database information");
            }
        }

        /// <summary>
        /// Create the new record
        /// </summary>
        protected override void CreateRecord()
        {
            // Allocate the new record to be inserted.
            var record = DebtPlus.LINQ.Factory.Manufacture_Calendar();

            // Edit the new record. If OK then attempt to insert the record.
            using (EditForm frm = new EditForm(record))
            {
                if (frm.ShowDialog() != System.Windows.Forms.DialogResult.OK)
                {
                    return;
                }
            }

            try
            {
                // Add the record to the collection
                bc.Calendars.InsertOnSubmit(record);
                bc.SubmitChanges();
                colRecords.Add(record);
                gridView1.RefreshData();
            }
            catch (System.Data.SqlClient.SqlException ex)
            {
                DebtPlus.UI.Common.ErrorHandling.HandleErrors(ex, "Error updating database");
            }
        }

        /// <summary>
        /// Update the existing record
        /// </summary>
        /// <param name="obj">Pointer to the desired record for the operation</param>
        protected override void UpdateRecord(object obj)
        {
            // Find the record
            var record = obj as DebtPlus.LINQ.Calendar;
            if (obj == null)
            {
                return;
            }

            // Remember the previous ID value
            DateTime priorID = record.Id;

            // Update the record now.
            using (EditForm frm = new EditForm(record))
            {
                if (frm.ShowDialog() != System.Windows.Forms.DialogResult.OK)
                {
                    return;
                }
            }

            try
            {
                // If the ID is changed then we need to delete the previous record
                // This is ugly but there is no way to change the primary key with LINQ.
                if (priorID != record.Id)
                {
                    var qDelete = bc.Calendars.Where(s => s.Id == priorID).FirstOrDefault();
                    if (qDelete != null)
                    {
                        bc.Calendars.DeleteOnSubmit(qDelete);
                    }

                    // Create a new record to be inserted into the database
                    // We don't need to manufacture a blank record since we are copying all of the fields.
                    var newRecord = new DebtPlus.LINQ.Calendar()
                    {
                        D = record.D,
                        dayname = record.dayname,
                        DayNumber = record.DayNumber,
                        DOY = record.DOY,
                        DW = record.DW,
                        FM = record.FM,
                        FY = record.FY,
                        HolidayDescription = record.HolidayDescription,
                        Id = record.Id,
                        isBankHoliday = record.isBankHoliday,
                        isHoliday = record.isHoliday,
                        isWeekday = record.isWeekday,
                        M = record.M,
                        monthname = record.monthname,
                        Q = record.Q,
                        UTCOffset = record.UTCOffset,
                        W = record.W,
                        Y = record.Y
                    };

                    // Add the new record to the list
                    colRecords.Remove(record);
                    bc.Calendars.InsertOnSubmit(newRecord);
                    colRecords.Add(newRecord);
                }

                bc.SubmitChanges();
                gridView1.RefreshData();
            }
            catch (System.Data.SqlClient.SqlException ex)
            {
                DebtPlus.UI.Common.ErrorHandling.HandleErrors(ex, "Error updating database");
            }
        }

        /// <summary>
        /// Delete the indicated record from the database
        /// </summary>
        /// <param name="obj">Pointer to the desired record for the operation</param>
        protected override void DeleteRecord(object obj)
        {
            // Find the record
            var record = obj as DebtPlus.LINQ.Calendar;
            if (obj == null)
            {
                return;
            }

            if (DebtPlus.Data.Prompts.RequestConfirmation_Delete() != System.Windows.Forms.DialogResult.Yes)
            {
                return;
            }

            try
            {
                bc.Calendars.DeleteOnSubmit(record);
                bc.SubmitChanges();

                colRecords.Remove(record);
                gridView1.RefreshData();
            }
            catch (System.Data.SqlClient.SqlException ex)
            {
                DebtPlus.UI.Common.ErrorHandling.HandleErrors(ex, "Error updating database");
            }
        }
    }
}