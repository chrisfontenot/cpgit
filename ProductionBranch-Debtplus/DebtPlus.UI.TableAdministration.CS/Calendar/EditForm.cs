#region "Copyright 2000-2012 DebtPlus, L.L.C."
//{*******************************************************************}
//{                                                                   }
//{       DebtPlus Debt Management System                             }
//{                                                                   }
//{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
//{       ALL RIGHTS RESERVED                                         }
//{                                                                   }
//{   The entire contents of this file is protected by U.S. and       }
//{   International Copyright Laws. Unauthorized reproduction,        }
//{   reverse-engineering, and distribution of all or any portion of  }
//{   the code contained in this file is strictly prohibited and may  }
//{   result in severe civil and criminal penalties and will be       }
//{   prosecuted to the maximum extent possible under the law.        }
//{                                                                   }
//{   RESTRICTIONS                                                    }
//{                                                                   }
//{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
//{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
//{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
//{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
//{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
//{                                                                   }
//{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
//{   ADDITIONAL RESTRICTIONS.                                        }
//{                                                                   }
//{*******************************************************************}

#endregion "Copyright 2000-2012 DebtPlus, L.L.C."

using System;

namespace DebtPlus.UI.TableAdministration.CS.Calendar
{
    internal partial class EditForm : Templates.EditTemplateForm
    {
        private DebtPlus.LINQ.Calendar record = null;

        internal EditForm()
            : base()
        {
            InitializeComponent();
        }

        internal EditForm(DebtPlus.LINQ.Calendar record)
            : this()
        {
            this.record = record;
            this.Load += EditForm_Load;
        }

        private void EditForm_Load(object sender, EventArgs e)
        {
            DateEdit_dt.EditValue = record.Id.Date;
            CheckEdit_BankHoliday.EditValue = record.isBankHoliday;
            CheckEdit_isHoliday.EditValue = record.isHoliday;
            CheckEdit_isWeekday.EditValue = record.isWeekday;
            TextEdit_d.EditValue = record.D;
            TextEdit_dayname.EditValue = record.dayname;
            TextEdit_daynumber.EditValue = record.DayNumber;
            TextEdit_dw.EditValue = record.DW;
            TextEdit_doy.EditValue = record.DOY;
            TextEdit_fm.EditValue = record.FM;
            TextEdit_fy.EditValue = record.FY;
            TextEdit_holidaydescription.EditValue = record.HolidayDescription;
            TextEdit_monthname.EditValue = record.monthname;
            TextEdit_q.EditValue = record.Q;
            TextEdit_uftoffset.EditValue = record.UTCOffset;
            TextEdit_w.EditValue = record.W;
            TextEdit_y.EditValue = record.Y;
        }

        protected override void simpleButton_OK_Click(object sender, EventArgs e)
        {
            base.simpleButton_OK_Click(sender, e);

            // Retrieve the current values from the controls for the record.
            record.Id = DateEdit_dt.DateTime.Date;
            record.isBankHoliday = CheckEdit_BankHoliday.Checked;
            record.isHoliday = CheckEdit_isHoliday.Checked;
            record.isWeekday = CheckEdit_isWeekday.Checked;
            record.D = DebtPlus.Utils.Nulls.v_Int32(TextEdit_d.EditValue).GetValueOrDefault();
            record.dayname = DebtPlus.Utils.Nulls.v_String(TextEdit_dayname.EditValue);
            record.DayNumber = DebtPlus.Utils.Nulls.v_Int32(TextEdit_daynumber.EditValue).GetValueOrDefault();
            record.DW = DebtPlus.Utils.Nulls.v_Int32(TextEdit_dw.EditValue).GetValueOrDefault();
            record.DOY = DebtPlus.Utils.Nulls.v_Int32(TextEdit_doy.EditValue).GetValueOrDefault();
            record.FM = DebtPlus.Utils.Nulls.v_Int32(TextEdit_fm.EditValue).GetValueOrDefault();
            record.FY = DebtPlus.Utils.Nulls.v_Int32(TextEdit_fy.EditValue).GetValueOrDefault();
            record.HolidayDescription = DebtPlus.Utils.Nulls.v_String(TextEdit_holidaydescription.EditValue);
            record.monthname = DebtPlus.Utils.Nulls.v_String(TextEdit_monthname.EditValue);
            record.Q = DebtPlus.Utils.Nulls.v_Int32(TextEdit_q.EditValue).GetValueOrDefault();
            record.UTCOffset = DebtPlus.Utils.Nulls.v_Int32(TextEdit_uftoffset.EditValue).GetValueOrDefault();
            record.W = DebtPlus.Utils.Nulls.v_Int32(TextEdit_w.EditValue).GetValueOrDefault();
            record.Y = DebtPlus.Utils.Nulls.v_Int32(TextEdit_y.EditValue).GetValueOrDefault();
        }
    }
}