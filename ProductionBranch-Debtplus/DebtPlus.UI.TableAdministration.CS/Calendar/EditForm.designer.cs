using System;
using System.Windows.Forms;
using System.Drawing;
using DebtPlus.LINQ;
using System.Linq;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Diagnostics;
using DebtPlus.UI.Common.Templates;

namespace DebtPlus.UI.TableAdministration.CS.Calendar
{
	partial class EditForm
	{
		//Form overrides dispose to clean up the component list.
		[System.Diagnostics.DebuggerNonUserCode]
		protected override void Dispose(bool disposing)
		{
			try 
            {
				if (disposing)
                {
                    if (components != null) components.Dispose();
				}
                components = null;
            }
    	    finally
            {
				base.Dispose(disposing);
			}
		}

		//Required by the Windows Form Designer
		private System.ComponentModel.IContainer components = null;

		//NOTE: The following procedure is required by the Windows Form Designer
		//It can be modified using the Windows Form Designer.  
		//Do not modify it using the code editor.
		[System.Diagnostics.DebuggerStepThrough]
		private void InitializeComponent()
		{
			this.LabelControl1 = new DevExpress.XtraEditors.LabelControl();
			this.DateEdit_dt = new DevExpress.XtraEditors.DateEdit();
			this.CheckEdit_isHoliday = new DevExpress.XtraEditors.CheckEdit();
			this.CheckEdit_isWeekday = new DevExpress.XtraEditors.CheckEdit();
			this.CheckEdit_BankHoliday = new DevExpress.XtraEditors.CheckEdit();
			this.LabelControl2 = new DevExpress.XtraEditors.LabelControl();
			this.TextEdit_y = new DevExpress.XtraEditors.TextEdit();
			this.TextEdit_fy = new DevExpress.XtraEditors.TextEdit();
			this.LabelControl3 = new DevExpress.XtraEditors.LabelControl();
			this.TextEdit_q = new DevExpress.XtraEditors.TextEdit();
			this.LabelControl4 = new DevExpress.XtraEditors.LabelControl();
			this.TextEdit_dw = new DevExpress.XtraEditors.TextEdit();
			this.LabelControl5 = new DevExpress.XtraEditors.LabelControl();
			this.TextEdit_d = new DevExpress.XtraEditors.TextEdit();
			this.LabelControl6 = new DevExpress.XtraEditors.LabelControl();
			this.TextEdit_fm = new DevExpress.XtraEditors.TextEdit();
			this.LabelControl7 = new DevExpress.XtraEditors.LabelControl();
			this.TextEdit_monthname = new DevExpress.XtraEditors.TextEdit();
			this.LabelControl8 = new DevExpress.XtraEditors.LabelControl();
			this.TextEdit_daynumber = new DevExpress.XtraEditors.TextEdit();
			this.LabelControl9 = new DevExpress.XtraEditors.LabelControl();
			this.TextEdit_doy = new DevExpress.XtraEditors.TextEdit();
			this.LabelControl10 = new DevExpress.XtraEditors.LabelControl();
			this.TextEdit_dayname = new DevExpress.XtraEditors.TextEdit();
			this.LabelControl11 = new DevExpress.XtraEditors.LabelControl();
			this.TextEdit_w = new DevExpress.XtraEditors.TextEdit();
			this.LabelControl12 = new DevExpress.XtraEditors.LabelControl();
			this.TextEdit_uftoffset = new DevExpress.XtraEditors.TextEdit();
			this.LabelControl13 = new DevExpress.XtraEditors.LabelControl();
			this.TextEdit_holidaydescription = new DevExpress.XtraEditors.TextEdit();
			this.LabelControl14 = new DevExpress.XtraEditors.LabelControl();
			((System.ComponentModel.ISupportInitialize)this.DxErrorProvider1).BeginInit();
			((System.ComponentModel.ISupportInitialize)this.DateEdit_dt.Properties.VistaTimeProperties).BeginInit();
			((System.ComponentModel.ISupportInitialize)this.DateEdit_dt.Properties).BeginInit();
			((System.ComponentModel.ISupportInitialize)this.CheckEdit_isHoliday.Properties).BeginInit();
			((System.ComponentModel.ISupportInitialize)this.CheckEdit_isWeekday.Properties).BeginInit();
			((System.ComponentModel.ISupportInitialize)this.CheckEdit_BankHoliday.Properties).BeginInit();
			((System.ComponentModel.ISupportInitialize)this.TextEdit_y.Properties).BeginInit();
			((System.ComponentModel.ISupportInitialize)this.TextEdit_fy.Properties).BeginInit();
			((System.ComponentModel.ISupportInitialize)this.TextEdit_q.Properties).BeginInit();
			((System.ComponentModel.ISupportInitialize)this.TextEdit_dw.Properties).BeginInit();
			((System.ComponentModel.ISupportInitialize)this.TextEdit_d.Properties).BeginInit();
			((System.ComponentModel.ISupportInitialize)this.TextEdit_fm.Properties).BeginInit();
			((System.ComponentModel.ISupportInitialize)this.TextEdit_monthname.Properties).BeginInit();
			((System.ComponentModel.ISupportInitialize)this.TextEdit_daynumber.Properties).BeginInit();
			((System.ComponentModel.ISupportInitialize)this.TextEdit_doy.Properties).BeginInit();
			((System.ComponentModel.ISupportInitialize)this.TextEdit_dayname.Properties).BeginInit();
			((System.ComponentModel.ISupportInitialize)this.TextEdit_w.Properties).BeginInit();
			((System.ComponentModel.ISupportInitialize)this.TextEdit_uftoffset.Properties).BeginInit();
			((System.ComponentModel.ISupportInitialize)this.TextEdit_holidaydescription.Properties).BeginInit();
			this.SuspendLayout();
			//
			//simpleButton_OK
			//
			this.simpleButton_OK.Enabled = true;
			this.simpleButton_OK.Location = new System.Drawing.Point(413, 43);
			this.simpleButton_OK.TabIndex = 31;
			//
			//simpleButton_Cancel
			//
			this.simpleButton_Cancel.Location = new System.Drawing.Point(413, 86);
			this.simpleButton_Cancel.TabIndex = 32;
			//
			//LabelControl1
			//
			this.LabelControl1.Location = new System.Drawing.Point(14, 16);
			this.LabelControl1.Name = "LabelControl1";
			this.LabelControl1.Size = new System.Drawing.Size(23, 13);
			this.LabelControl1.TabIndex = 0;
			this.LabelControl1.Text = "Date";
			//
			//DateEdit_dt
			//
			this.DateEdit_dt.EditValue = null;
			this.DateEdit_dt.Location = new System.Drawing.Point(86, 13);
			this.DateEdit_dt.Name = "DateEdit_dt";
			this.DateEdit_dt.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] { new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo) });
			this.DateEdit_dt.Properties.VistaTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] { new DevExpress.XtraEditors.Controls.EditorButton() });
			this.DateEdit_dt.Size = new System.Drawing.Size(100, 20);
			this.DateEdit_dt.TabIndex = 1;
			//
			//CheckEdit_isHoliday
			//
			this.CheckEdit_isHoliday.Location = new System.Drawing.Point(270, 153);
			this.CheckEdit_isHoliday.Name = "CheckEdit_isHoliday";
			this.CheckEdit_isHoliday.Properties.Caption = "Holiday";
			this.CheckEdit_isHoliday.Size = new System.Drawing.Size(173, 19);
			this.CheckEdit_isHoliday.TabIndex = 24;
			//
			//CheckEdit_isWeekday
			//
			this.CheckEdit_isWeekday.Location = new System.Drawing.Point(270, 178);
			this.CheckEdit_isWeekday.Name = "CheckEdit_isWeekday";
			this.CheckEdit_isWeekday.Properties.Caption = "Weekday";
			this.CheckEdit_isWeekday.Size = new System.Drawing.Size(173, 19);
			this.CheckEdit_isWeekday.TabIndex = 27;
			//
			//CheckEdit_BankHoliday
			//
			this.CheckEdit_BankHoliday.Location = new System.Drawing.Point(270, 203);
			this.CheckEdit_BankHoliday.Name = "CheckEdit_BankHoliday";
			this.CheckEdit_BankHoliday.Properties.Caption = "Banking Holiday";
			this.CheckEdit_BankHoliday.Size = new System.Drawing.Size(173, 19);
			this.CheckEdit_BankHoliday.TabIndex = 30;
			//
			//LabelControl2
			//
			this.LabelControl2.Location = new System.Drawing.Point(15, 42);
			this.LabelControl2.Name = "LabelControl2";
			this.LabelControl2.Size = new System.Drawing.Size(22, 13);
			this.LabelControl2.TabIndex = 2;
			this.LabelControl2.Text = "Year";
			//
			//TextEdit_y
			//
			this.TextEdit_y.Location = new System.Drawing.Point(86, 39);
			this.TextEdit_y.Name = "TextEdit_y";
			this.TextEdit_y.Properties.Appearance.Options.UseTextOptions = true;
			this.TextEdit_y.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
			this.TextEdit_y.Properties.AppearanceDisabled.Options.UseTextOptions = true;
			this.TextEdit_y.Properties.AppearanceDisabled.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
			this.TextEdit_y.Properties.AppearanceFocused.Options.UseTextOptions = true;
			this.TextEdit_y.Properties.AppearanceFocused.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
			this.TextEdit_y.Properties.AppearanceReadOnly.Options.UseTextOptions = true;
			this.TextEdit_y.Properties.AppearanceReadOnly.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
			this.TextEdit_y.Properties.DisplayFormat.FormatString = "{0:f0}";
			this.TextEdit_y.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
			this.TextEdit_y.Properties.Mask.BeepOnError = true;
			this.TextEdit_y.Properties.Mask.EditMask = "\\d+";
			this.TextEdit_y.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.RegEx;
			this.TextEdit_y.Size = new System.Drawing.Size(48, 20);
			this.TextEdit_y.TabIndex = 3;
			//
			//TextEdit_fy
			//
			this.TextEdit_fy.Location = new System.Drawing.Point(86, 65);
			this.TextEdit_fy.Name = "TextEdit_fy";
			this.TextEdit_fy.Properties.Appearance.Options.UseTextOptions = true;
			this.TextEdit_fy.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
			this.TextEdit_fy.Properties.AppearanceDisabled.Options.UseTextOptions = true;
			this.TextEdit_fy.Properties.AppearanceDisabled.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
			this.TextEdit_fy.Properties.AppearanceFocused.Options.UseTextOptions = true;
			this.TextEdit_fy.Properties.AppearanceFocused.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
			this.TextEdit_fy.Properties.AppearanceReadOnly.Options.UseTextOptions = true;
			this.TextEdit_fy.Properties.AppearanceReadOnly.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
			this.TextEdit_fy.Properties.DisplayFormat.FormatString = "{0:f0}";
			this.TextEdit_fy.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
			this.TextEdit_fy.Properties.Mask.BeepOnError = true;
			this.TextEdit_fy.Properties.Mask.EditMask = "\\d+";
			this.TextEdit_fy.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.RegEx;
			this.TextEdit_fy.Size = new System.Drawing.Size(48, 20);
			this.TextEdit_fy.TabIndex = 9;
			//
			//LabelControl3
			//
			this.LabelControl3.Location = new System.Drawing.Point(15, 68);
			this.LabelControl3.Name = "LabelControl3";
			this.LabelControl3.Size = new System.Drawing.Size(51, 13);
			this.LabelControl3.TabIndex = 8;
			this.LabelControl3.Text = "Fiscal Year";
			//
			//TextEdit_q
			//
			this.TextEdit_q.Location = new System.Drawing.Point(86, 92);
			this.TextEdit_q.Name = "TextEdit_q";
			this.TextEdit_q.Properties.Appearance.Options.UseTextOptions = true;
			this.TextEdit_q.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
			this.TextEdit_q.Properties.AppearanceDisabled.Options.UseTextOptions = true;
			this.TextEdit_q.Properties.AppearanceDisabled.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
			this.TextEdit_q.Properties.AppearanceFocused.Options.UseTextOptions = true;
			this.TextEdit_q.Properties.AppearanceFocused.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
			this.TextEdit_q.Properties.AppearanceReadOnly.Options.UseTextOptions = true;
			this.TextEdit_q.Properties.AppearanceReadOnly.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
			this.TextEdit_q.Properties.DisplayFormat.FormatString = "{0:f0}";
			this.TextEdit_q.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
			this.TextEdit_q.Properties.Mask.BeepOnError = true;
			this.TextEdit_q.Properties.Mask.EditMask = "\\d+";
			this.TextEdit_q.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.RegEx;
			this.TextEdit_q.Size = new System.Drawing.Size(48, 20);
			this.TextEdit_q.TabIndex = 15;
			//
			//LabelControl4
			//
			this.LabelControl4.Location = new System.Drawing.Point(15, 95);
			this.LabelControl4.Name = "LabelControl4";
			this.LabelControl4.Size = new System.Drawing.Size(38, 13);
			this.LabelControl4.TabIndex = 14;
			this.LabelControl4.Text = "Quarter";
			//
			//TextEdit_dw
			//
			this.TextEdit_dw.Location = new System.Drawing.Point(211, 92);
			this.TextEdit_dw.Name = "TextEdit_dw";
			this.TextEdit_dw.Properties.Appearance.Options.UseTextOptions = true;
			this.TextEdit_dw.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
			this.TextEdit_dw.Properties.AppearanceDisabled.Options.UseTextOptions = true;
			this.TextEdit_dw.Properties.AppearanceDisabled.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
			this.TextEdit_dw.Properties.AppearanceFocused.Options.UseTextOptions = true;
			this.TextEdit_dw.Properties.AppearanceFocused.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
			this.TextEdit_dw.Properties.AppearanceReadOnly.Options.UseTextOptions = true;
			this.TextEdit_dw.Properties.AppearanceReadOnly.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
			this.TextEdit_dw.Properties.DisplayFormat.FormatString = "{0:f0}";
			this.TextEdit_dw.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
			this.TextEdit_dw.Properties.Mask.BeepOnError = true;
			this.TextEdit_dw.Properties.Mask.EditMask = "\\d+";
			this.TextEdit_dw.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.RegEx;
			this.TextEdit_dw.Size = new System.Drawing.Size(48, 20);
			this.TextEdit_dw.TabIndex = 17;
			//
			//LabelControl5
			//
			this.LabelControl5.Location = new System.Drawing.Point(140, 95);
			this.LabelControl5.Name = "LabelControl5";
			this.LabelControl5.Size = new System.Drawing.Size(62, 13);
			this.LabelControl5.TabIndex = 16;
			this.LabelControl5.Text = "Day of Week";
			//
			//TextEdit_d
			//
			this.TextEdit_d.Location = new System.Drawing.Point(211, 65);
			this.TextEdit_d.Name = "TextEdit_d";
			this.TextEdit_d.Properties.Appearance.Options.UseTextOptions = true;
			this.TextEdit_d.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
			this.TextEdit_d.Properties.AppearanceDisabled.Options.UseTextOptions = true;
			this.TextEdit_d.Properties.AppearanceDisabled.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
			this.TextEdit_d.Properties.AppearanceFocused.Options.UseTextOptions = true;
			this.TextEdit_d.Properties.AppearanceFocused.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
			this.TextEdit_d.Properties.AppearanceReadOnly.Options.UseTextOptions = true;
			this.TextEdit_d.Properties.AppearanceReadOnly.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
			this.TextEdit_d.Properties.DisplayFormat.FormatString = "{0:f0}";
			this.TextEdit_d.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
			this.TextEdit_d.Properties.Mask.BeepOnError = true;
			this.TextEdit_d.Properties.Mask.EditMask = "\\d+";
			this.TextEdit_d.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.RegEx;
			this.TextEdit_d.Size = new System.Drawing.Size(48, 20);
			this.TextEdit_d.TabIndex = 11;
			//
			//LabelControl6
			//
			this.LabelControl6.Location = new System.Drawing.Point(140, 68);
			this.LabelControl6.Name = "LabelControl6";
			this.LabelControl6.Size = new System.Drawing.Size(65, 13);
			this.LabelControl6.TabIndex = 10;
			this.LabelControl6.Text = "Day of Month";
			//
			//TextEdit_fm
			//
			this.TextEdit_fm.Location = new System.Drawing.Point(211, 39);
			this.TextEdit_fm.Name = "TextEdit_fm";
			this.TextEdit_fm.Properties.Appearance.Options.UseTextOptions = true;
			this.TextEdit_fm.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
			this.TextEdit_fm.Properties.AppearanceDisabled.Options.UseTextOptions = true;
			this.TextEdit_fm.Properties.AppearanceDisabled.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
			this.TextEdit_fm.Properties.AppearanceFocused.Options.UseTextOptions = true;
			this.TextEdit_fm.Properties.AppearanceFocused.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
			this.TextEdit_fm.Properties.AppearanceReadOnly.Options.UseTextOptions = true;
			this.TextEdit_fm.Properties.AppearanceReadOnly.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
			this.TextEdit_fm.Properties.DisplayFormat.FormatString = "{0:f0}";
			this.TextEdit_fm.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
			this.TextEdit_fm.Properties.Mask.BeepOnError = true;
			this.TextEdit_fm.Properties.Mask.EditMask = "\\d+";
			this.TextEdit_fm.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.RegEx;
			this.TextEdit_fm.Size = new System.Drawing.Size(48, 20);
			this.TextEdit_fm.TabIndex = 5;
			//
			//LabelControl7
			//
			this.LabelControl7.Location = new System.Drawing.Point(140, 42);
			this.LabelControl7.Name = "LabelControl7";
			this.LabelControl7.Size = new System.Drawing.Size(59, 13);
			this.LabelControl7.TabIndex = 4;
			this.LabelControl7.Text = "Fiscal Month";
			//
			//TextEdit_monthname
			//
			this.TextEdit_monthname.Location = new System.Drawing.Point(114, 152);
			this.TextEdit_monthname.Name = "TextEdit_monthname";
			this.TextEdit_monthname.Size = new System.Drawing.Size(145, 20);
			this.TextEdit_monthname.TabIndex = 23;
			//
			//LabelControl8
			//
			this.LabelControl8.Location = new System.Drawing.Point(14, 155);
			this.LabelControl8.Name = "LabelControl8";
			this.LabelControl8.Size = new System.Drawing.Size(60, 13);
			this.LabelControl8.TabIndex = 22;
			this.LabelControl8.Text = "Month Name";
			//
			//TextEdit_daynumber
			//
			this.TextEdit_daynumber.Location = new System.Drawing.Point(343, 65);
			this.TextEdit_daynumber.Name = "TextEdit_daynumber";
			this.TextEdit_daynumber.Properties.Appearance.Options.UseTextOptions = true;
			this.TextEdit_daynumber.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
			this.TextEdit_daynumber.Properties.AppearanceDisabled.Options.UseTextOptions = true;
			this.TextEdit_daynumber.Properties.AppearanceDisabled.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
			this.TextEdit_daynumber.Properties.AppearanceFocused.Options.UseTextOptions = true;
			this.TextEdit_daynumber.Properties.AppearanceFocused.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
			this.TextEdit_daynumber.Properties.AppearanceReadOnly.Options.UseTextOptions = true;
			this.TextEdit_daynumber.Properties.AppearanceReadOnly.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
			this.TextEdit_daynumber.Properties.DisplayFormat.FormatString = "{0:f0}";
			this.TextEdit_daynumber.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
			this.TextEdit_daynumber.Properties.Mask.BeepOnError = true;
			this.TextEdit_daynumber.Properties.Mask.EditMask = "\\d+";
			this.TextEdit_daynumber.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.RegEx;
			this.TextEdit_daynumber.Size = new System.Drawing.Size(48, 20);
			this.TextEdit_daynumber.TabIndex = 13;
			//
			//LabelControl9
			//
			this.LabelControl9.Location = new System.Drawing.Point(272, 68);
			this.LabelControl9.Name = "LabelControl9";
			this.LabelControl9.Size = new System.Drawing.Size(59, 13);
			this.LabelControl9.TabIndex = 12;
			this.LabelControl9.Text = "Day Number";
			//
			//TextEdit_doy
			//
			this.TextEdit_doy.Location = new System.Drawing.Point(343, 39);
			this.TextEdit_doy.Name = "TextEdit_doy";
			this.TextEdit_doy.Properties.Appearance.Options.UseTextOptions = true;
			this.TextEdit_doy.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
			this.TextEdit_doy.Properties.AppearanceDisabled.Options.UseTextOptions = true;
			this.TextEdit_doy.Properties.AppearanceDisabled.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
			this.TextEdit_doy.Properties.AppearanceFocused.Options.UseTextOptions = true;
			this.TextEdit_doy.Properties.AppearanceFocused.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
			this.TextEdit_doy.Properties.AppearanceReadOnly.Options.UseTextOptions = true;
			this.TextEdit_doy.Properties.AppearanceReadOnly.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
			this.TextEdit_doy.Properties.DisplayFormat.FormatString = "{0:f0}";
			this.TextEdit_doy.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
			this.TextEdit_doy.Properties.Mask.BeepOnError = true;
			this.TextEdit_doy.Properties.Mask.EditMask = "\\d+";
			this.TextEdit_doy.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.RegEx;
			this.TextEdit_doy.Size = new System.Drawing.Size(48, 20);
			this.TextEdit_doy.TabIndex = 7;
			//
			//LabelControl10
			//
			this.LabelControl10.Location = new System.Drawing.Point(272, 42);
			this.LabelControl10.Name = "LabelControl10";
			this.LabelControl10.Size = new System.Drawing.Size(57, 13);
			this.LabelControl10.TabIndex = 6;
			this.LabelControl10.Text = "Day of Year";
			//
			//TextEdit_dayname
			//
			this.TextEdit_dayname.Location = new System.Drawing.Point(114, 178);
			this.TextEdit_dayname.Name = "TextEdit_dayname";
			this.TextEdit_dayname.Size = new System.Drawing.Size(145, 20);
			this.TextEdit_dayname.TabIndex = 26;
			//
			//LabelControl11
			//
			this.LabelControl11.Location = new System.Drawing.Point(14, 181);
			this.LabelControl11.Name = "LabelControl11";
			this.LabelControl11.Size = new System.Drawing.Size(49, 13);
			this.LabelControl11.TabIndex = 25;
			this.LabelControl11.Text = "Day Name";
			//
			//TextEdit_w
			//
			this.TextEdit_w.Location = new System.Drawing.Point(343, 91);
			this.TextEdit_w.Name = "TextEdit_w";
			this.TextEdit_w.Properties.Appearance.Options.UseTextOptions = true;
			this.TextEdit_w.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
			this.TextEdit_w.Properties.AppearanceDisabled.Options.UseTextOptions = true;
			this.TextEdit_w.Properties.AppearanceDisabled.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
			this.TextEdit_w.Properties.AppearanceFocused.Options.UseTextOptions = true;
			this.TextEdit_w.Properties.AppearanceFocused.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
			this.TextEdit_w.Properties.AppearanceReadOnly.Options.UseTextOptions = true;
			this.TextEdit_w.Properties.AppearanceReadOnly.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
			this.TextEdit_w.Properties.DisplayFormat.FormatString = "{0:f0}";
			this.TextEdit_w.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
			this.TextEdit_w.Properties.Mask.BeepOnError = true;
			this.TextEdit_w.Properties.Mask.EditMask = "\\d+";
			this.TextEdit_w.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.RegEx;
			this.TextEdit_w.Size = new System.Drawing.Size(48, 20);
			this.TextEdit_w.TabIndex = 19;
			//
			//LabelControl12
			//
			this.LabelControl12.Location = new System.Drawing.Point(272, 94);
			this.LabelControl12.Name = "LabelControl12";
			this.LabelControl12.Size = new System.Drawing.Size(67, 13);
			this.LabelControl12.TabIndex = 18;
			this.LabelControl12.Text = "Week Number";
			//
			//TextEdit_uftoffset
			//
			this.TextEdit_uftoffset.Location = new System.Drawing.Point(211, 118);
			this.TextEdit_uftoffset.Name = "TextEdit_uftoffset";
			this.TextEdit_uftoffset.Properties.Appearance.Options.UseTextOptions = true;
			this.TextEdit_uftoffset.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
			this.TextEdit_uftoffset.Properties.AppearanceDisabled.Options.UseTextOptions = true;
			this.TextEdit_uftoffset.Properties.AppearanceDisabled.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
			this.TextEdit_uftoffset.Properties.AppearanceFocused.Options.UseTextOptions = true;
			this.TextEdit_uftoffset.Properties.AppearanceFocused.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
			this.TextEdit_uftoffset.Properties.AppearanceReadOnly.Options.UseTextOptions = true;
			this.TextEdit_uftoffset.Properties.AppearanceReadOnly.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
			this.TextEdit_uftoffset.Properties.DisplayFormat.FormatString = "{0:f0}";
			this.TextEdit_uftoffset.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
			this.TextEdit_uftoffset.Properties.Mask.BeepOnError = true;
			this.TextEdit_uftoffset.Properties.Mask.EditMask = "[01]";
			this.TextEdit_uftoffset.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.RegEx;
			this.TextEdit_uftoffset.Size = new System.Drawing.Size(48, 20);
			this.TextEdit_uftoffset.TabIndex = 21;
			//
			//LabelControl13
			//
			this.LabelControl13.Location = new System.Drawing.Point(140, 122);
			this.LabelControl13.Name = "LabelControl13";
			this.LabelControl13.Size = new System.Drawing.Size(54, 13);
			this.LabelControl13.TabIndex = 20;
			this.LabelControl13.Text = "UTC Offset";
			//
			//TextEdit_holidaydescription
			//
			this.TextEdit_holidaydescription.Location = new System.Drawing.Point(114, 204);
			this.TextEdit_holidaydescription.Name = "TextEdit_holidaydescription";
			this.TextEdit_holidaydescription.Size = new System.Drawing.Size(145, 20);
			this.TextEdit_holidaydescription.TabIndex = 29;
			//
			//LabelControl14
			//
			this.LabelControl14.Location = new System.Drawing.Point(12, 207);
			this.LabelControl14.Name = "LabelControl14";
			this.LabelControl14.Size = new System.Drawing.Size(91, 13);
			this.LabelControl14.TabIndex = 28;
			this.LabelControl14.Text = "Holiday Description";
			//
			//EditForm
			//
			this.AutoScaleDimensions = new System.Drawing.SizeF(6f, 13f);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(509, 234);
			this.Controls.Add(this.TextEdit_holidaydescription);
			this.Controls.Add(this.LabelControl14);
			this.Controls.Add(this.LabelControl13);
			this.Controls.Add(this.TextEdit_uftoffset);
			this.Controls.Add(this.TextEdit_w);
			this.Controls.Add(this.LabelControl12);
			this.Controls.Add(this.TextEdit_dayname);
			this.Controls.Add(this.LabelControl11);
			this.Controls.Add(this.TextEdit_monthname);
			this.Controls.Add(this.LabelControl8);
			this.Controls.Add(this.TextEdit_daynumber);
			this.Controls.Add(this.LabelControl9);
			this.Controls.Add(this.TextEdit_doy);
			this.Controls.Add(this.LabelControl10);
			this.Controls.Add(this.TextEdit_dw);
			this.Controls.Add(this.LabelControl5);
			this.Controls.Add(this.TextEdit_d);
			this.Controls.Add(this.LabelControl6);
			this.Controls.Add(this.TextEdit_fm);
			this.Controls.Add(this.LabelControl7);
			this.Controls.Add(this.TextEdit_q);
			this.Controls.Add(this.LabelControl4);
			this.Controls.Add(this.TextEdit_fy);
			this.Controls.Add(this.LabelControl3);
			this.Controls.Add(this.TextEdit_y);
			this.Controls.Add(this.LabelControl2);
			this.Controls.Add(this.CheckEdit_BankHoliday);
			this.Controls.Add(this.CheckEdit_isWeekday);
			this.Controls.Add(this.CheckEdit_isHoliday);
			this.Controls.Add(this.DateEdit_dt);
			this.Controls.Add(this.LabelControl1);
			this.Name = "EditForm";
			this.ToolTipController1.SetSuperTip(this, null);
			this.Text = "EditForm";
			this.Controls.SetChildIndex(this.LabelControl1, 0);
			this.Controls.SetChildIndex(this.DateEdit_dt, 0);
			this.Controls.SetChildIndex(this.CheckEdit_isHoliday, 0);
			this.Controls.SetChildIndex(this.simpleButton_OK, 0);
			this.Controls.SetChildIndex(this.simpleButton_Cancel, 0);
			this.Controls.SetChildIndex(this.CheckEdit_isWeekday, 0);
			this.Controls.SetChildIndex(this.CheckEdit_BankHoliday, 0);
			this.Controls.SetChildIndex(this.LabelControl2, 0);
			this.Controls.SetChildIndex(this.TextEdit_y, 0);
			this.Controls.SetChildIndex(this.LabelControl3, 0);
			this.Controls.SetChildIndex(this.TextEdit_fy, 0);
			this.Controls.SetChildIndex(this.LabelControl4, 0);
			this.Controls.SetChildIndex(this.TextEdit_q, 0);
			this.Controls.SetChildIndex(this.LabelControl7, 0);
			this.Controls.SetChildIndex(this.TextEdit_fm, 0);
			this.Controls.SetChildIndex(this.LabelControl6, 0);
			this.Controls.SetChildIndex(this.TextEdit_d, 0);
			this.Controls.SetChildIndex(this.LabelControl5, 0);
			this.Controls.SetChildIndex(this.TextEdit_dw, 0);
			this.Controls.SetChildIndex(this.LabelControl10, 0);
			this.Controls.SetChildIndex(this.TextEdit_doy, 0);
			this.Controls.SetChildIndex(this.LabelControl9, 0);
			this.Controls.SetChildIndex(this.TextEdit_daynumber, 0);
			this.Controls.SetChildIndex(this.LabelControl8, 0);
			this.Controls.SetChildIndex(this.TextEdit_monthname, 0);
			this.Controls.SetChildIndex(this.LabelControl11, 0);
			this.Controls.SetChildIndex(this.TextEdit_dayname, 0);
			this.Controls.SetChildIndex(this.LabelControl12, 0);
			this.Controls.SetChildIndex(this.TextEdit_w, 0);
			this.Controls.SetChildIndex(this.TextEdit_uftoffset, 0);
			this.Controls.SetChildIndex(this.LabelControl13, 0);
			this.Controls.SetChildIndex(this.LabelControl14, 0);
			this.Controls.SetChildIndex(this.TextEdit_holidaydescription, 0);
			((System.ComponentModel.ISupportInitialize)this.DxErrorProvider1).EndInit();
			((System.ComponentModel.ISupportInitialize)this.DateEdit_dt.Properties.VistaTimeProperties).EndInit();
			((System.ComponentModel.ISupportInitialize)this.DateEdit_dt.Properties).EndInit();
			((System.ComponentModel.ISupportInitialize)this.CheckEdit_isHoliday.Properties).EndInit();
			((System.ComponentModel.ISupportInitialize)this.CheckEdit_isWeekday.Properties).EndInit();
			((System.ComponentModel.ISupportInitialize)this.CheckEdit_BankHoliday.Properties).EndInit();
			((System.ComponentModel.ISupportInitialize)this.TextEdit_y.Properties).EndInit();
			((System.ComponentModel.ISupportInitialize)this.TextEdit_fy.Properties).EndInit();
			((System.ComponentModel.ISupportInitialize)this.TextEdit_q.Properties).EndInit();
			((System.ComponentModel.ISupportInitialize)this.TextEdit_dw.Properties).EndInit();
			((System.ComponentModel.ISupportInitialize)this.TextEdit_d.Properties).EndInit();
			((System.ComponentModel.ISupportInitialize)this.TextEdit_fm.Properties).EndInit();
			((System.ComponentModel.ISupportInitialize)this.TextEdit_monthname.Properties).EndInit();
			((System.ComponentModel.ISupportInitialize)this.TextEdit_daynumber.Properties).EndInit();
			((System.ComponentModel.ISupportInitialize)this.TextEdit_doy.Properties).EndInit();
			((System.ComponentModel.ISupportInitialize)this.TextEdit_dayname.Properties).EndInit();
			((System.ComponentModel.ISupportInitialize)this.TextEdit_w.Properties).EndInit();
			((System.ComponentModel.ISupportInitialize)this.TextEdit_uftoffset.Properties).EndInit();
			((System.ComponentModel.ISupportInitialize)this.TextEdit_holidaydescription.Properties).EndInit();
			this.ResumeLayout(false);
			this.PerformLayout();

		}
		private DevExpress.XtraEditors.LabelControl LabelControl1;
		private DevExpress.XtraEditors.DateEdit DateEdit_dt;
		private DevExpress.XtraEditors.CheckEdit CheckEdit_isHoliday;
		private DevExpress.XtraEditors.CheckEdit CheckEdit_isWeekday;
		private DevExpress.XtraEditors.CheckEdit CheckEdit_BankHoliday;
		private DevExpress.XtraEditors.LabelControl LabelControl2;
		private DevExpress.XtraEditors.TextEdit TextEdit_y;
		private DevExpress.XtraEditors.TextEdit TextEdit_fy;
		private DevExpress.XtraEditors.LabelControl LabelControl3;
		private DevExpress.XtraEditors.TextEdit TextEdit_q;
		private DevExpress.XtraEditors.LabelControl LabelControl4;
		private DevExpress.XtraEditors.TextEdit TextEdit_dw;
		private DevExpress.XtraEditors.LabelControl LabelControl5;
		private DevExpress.XtraEditors.TextEdit TextEdit_d;
		private DevExpress.XtraEditors.LabelControl LabelControl6;
		private DevExpress.XtraEditors.TextEdit TextEdit_fm;
		private DevExpress.XtraEditors.LabelControl LabelControl7;
		private DevExpress.XtraEditors.TextEdit TextEdit_monthname;
		private DevExpress.XtraEditors.LabelControl LabelControl8;
		private DevExpress.XtraEditors.TextEdit TextEdit_daynumber;
		private DevExpress.XtraEditors.LabelControl LabelControl9;
		private DevExpress.XtraEditors.TextEdit TextEdit_doy;
		private DevExpress.XtraEditors.LabelControl LabelControl10;
		private DevExpress.XtraEditors.TextEdit TextEdit_dayname;
		private DevExpress.XtraEditors.LabelControl LabelControl11;
		private DevExpress.XtraEditors.TextEdit TextEdit_w;
		private DevExpress.XtraEditors.LabelControl LabelControl12;
		private DevExpress.XtraEditors.TextEdit TextEdit_uftoffset;
		private DevExpress.XtraEditors.LabelControl LabelControl13;
		private DevExpress.XtraEditors.TextEdit TextEdit_holidaydescription;
		private DevExpress.XtraEditors.LabelControl LabelControl14;
	}
}
