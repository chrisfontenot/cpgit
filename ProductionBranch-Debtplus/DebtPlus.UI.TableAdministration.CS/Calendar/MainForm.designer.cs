
using System;
using System.Windows.Forms;
using System.Drawing;
using DebtPlus.LINQ;
using System.Linq;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Diagnostics;
using DebtPlus.UI.Common.Templates;

namespace DebtPlus.UI.TableAdministration.CS.Calendar
{
	partial class MainForm
	{
		//Form overrides dispose to clean up the component list.
		[System.Diagnostics.DebuggerNonUserCode]
		protected override void Dispose(bool disposing)
		{
			try 
            {
				if (disposing) 
                {
					if (components != null) components.Dispose();
                    if (bc != null) bc.Dispose();
                }
                components = null;
                bc         = null;
            }

            finally 
            {
				base.Dispose(disposing);
			}
		}

		//Required by the Windows Form Designer
		private System.ComponentModel.IContainer components = null;

		//NOTE: The following procedure is required by the Windows Form Designer
		//It can be modified using the Windows Form Designer.  
		//Do not modify it using the code editor.
		[System.Diagnostics.DebuggerStepThrough]
		private void InitializeComponent()
		{
			this.gridColumn_Id = new DevExpress.XtraGrid.Columns.GridColumn();
			this.gridColumn_Id.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.True;
			this.gridColumn_isHoliday = new DevExpress.XtraGrid.Columns.GridColumn();
			this.gridColumn_isHoliday.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.True;
			this.gridColumn_weekday = new DevExpress.XtraGrid.Columns.GridColumn();
			this.gridColumn_weekday.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.True;
			this.gridColumn_isBankHoliday = new DevExpress.XtraGrid.Columns.GridColumn();
			this.gridColumn_isBankHoliday.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.True;
			this.gridColumn_HolidayDescription = new DevExpress.XtraGrid.Columns.GridColumn();
			this.gridColumn_HolidayDescription.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.True;
			((System.ComponentModel.ISupportInitialize)this.gridControl1).BeginInit();
			((System.ComponentModel.ISupportInitialize)this.gridView1).BeginInit();
			((System.ComponentModel.ISupportInitialize)this.DxErrorProvider1).BeginInit();
			this.SuspendLayout();
			//
			//gridView1
			//
			this.gridView1.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
				this.gridColumn_Id,
				this.gridColumn_isHoliday,
				this.gridColumn_weekday,
				this.gridColumn_isBankHoliday,
				this.gridColumn_HolidayDescription
			});
			//
			//gridColumn_Id
			//
			this.gridColumn_Id.Caption = "Date";
			this.gridColumn_Id.DisplayFormat.FormatString = "d";
			this.gridColumn_Id.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
			this.gridColumn_Id.FieldName = "Id";
			this.gridColumn_Id.GroupFormat.FormatString = "d";
			this.gridColumn_Id.GroupFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
			this.gridColumn_Id.GroupInterval = DevExpress.XtraGrid.ColumnGroupInterval.DateMonth;
			this.gridColumn_Id.Name = "gridColumn_Id";
			this.gridColumn_Id.Visible = true;
			this.gridColumn_Id.VisibleIndex = 0;
			//
			//gridColumn_isHoliday
			//
			this.gridColumn_isHoliday.Caption = "Holiday";
			this.gridColumn_isHoliday.FieldName = "isHoliday";
			this.gridColumn_isHoliday.Name = "gridColumn_isHoliday";
			this.gridColumn_isHoliday.Visible = true;
			this.gridColumn_isHoliday.VisibleIndex = 1;
			//
			//gridColumn_weekday
			//
			this.gridColumn_weekday.Caption = "Weekday";
			this.gridColumn_weekday.FieldName = "isWeekday";
			this.gridColumn_weekday.Name = "gridColumn_weekday";
			this.gridColumn_weekday.Visible = true;
			this.gridColumn_weekday.VisibleIndex = 2;
			//
			//gridColumn_isBankHoliday
			//
			this.gridColumn_isBankHoliday.Caption = "Bank Holiday";
			this.gridColumn_isBankHoliday.FieldName = "isBankHoliday";
			this.gridColumn_isBankHoliday.Name = "gridColumn_isBankHoliday";
			this.gridColumn_isBankHoliday.Visible = true;
			this.gridColumn_isBankHoliday.VisibleIndex = 3;
			//
			//gridColumn_HolidayDescription
			//
			this.gridColumn_HolidayDescription.Caption = "Description";
			this.gridColumn_HolidayDescription.FieldName = "HolidayDescription";
			this.gridColumn_HolidayDescription.Name = "gridColumn_HolidayDescription";
			this.gridColumn_HolidayDescription.Visible = true;
			this.gridColumn_HolidayDescription.VisibleIndex = 4;
			//
			//MainForm
			//
			this.AutoScaleDimensions = new System.Drawing.SizeF(6f, 13f);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(528, 294);
			this.Name = "MainForm";
			this.ToolTipController1.SetSuperTip(this, null);
			this.Text = "Calendar Dates";
			((System.ComponentModel.ISupportInitialize)this.gridControl1).EndInit();
			((System.ComponentModel.ISupportInitialize)this.gridView1).EndInit();
			((System.ComponentModel.ISupportInitialize)this.DxErrorProvider1).EndInit();
			this.ResumeLayout(false);
		}

		private DevExpress.XtraGrid.Columns.GridColumn gridColumn_Id;
		private DevExpress.XtraGrid.Columns.GridColumn gridColumn_isHoliday;
		private DevExpress.XtraGrid.Columns.GridColumn gridColumn_weekday;
		private DevExpress.XtraGrid.Columns.GridColumn gridColumn_isBankHoliday;
		private DevExpress.XtraGrid.Columns.GridColumn gridColumn_HolidayDescription;
	}
}
