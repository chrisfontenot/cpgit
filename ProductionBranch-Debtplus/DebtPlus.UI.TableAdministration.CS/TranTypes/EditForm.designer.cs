using System;
using System.Windows.Forms;
using System.Drawing;
using DebtPlus.LINQ;
using System.Linq;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Diagnostics;
using DebtPlus.UI.Common.Templates;

namespace DebtPlus.UI.TableAdministration.CS.TranTypes
{
	partial class EditForm
	{
		//Form overrides dispose to clean up the component list.
		[System.Diagnostics.DebuggerNonUserCode]
		protected override void Dispose(bool disposing)
		{
			try 
            {
				if (disposing)
                {
                    if (components != null) components.Dispose();
				}
                components = null;
            }
    	    finally
            {
				base.Dispose(disposing);
			}
		}

		//Required by the Windows Form Designer
		private System.ComponentModel.IContainer components = null;

		//NOTE: The following procedure is required by the Windows Form Designer
		//It can be modified using the Windows Form Designer.  
		//Do not modify it using the code editor.
		[System.Diagnostics.DebuggerStepThrough]
		private void InitializeComponent()
		{
			this.LabelControl1 = new DevExpress.XtraEditors.LabelControl();
			this.LabelControl_description = new DevExpress.XtraEditors.LabelControl();
			this.TextEdit_description = new DevExpress.XtraEditors.TextEdit();
			this.LabelControl_nfcc = new DevExpress.XtraEditors.LabelControl();
			this.TextEdit_tran_type = new DevExpress.XtraEditors.TextEdit();
			this.MemoExEdit_note = new DevExpress.XtraEditors.MemoExEdit();
			this.TextEdit_sort_order = new DevExpress.XtraEditors.TextEdit();
			this.LabelControl2 = new DevExpress.XtraEditors.LabelControl();
			this.CheckEdit1 = new DevExpress.XtraEditors.CheckEdit();
			((System.ComponentModel.ISupportInitialize)this.DxErrorProvider1).BeginInit();
			((System.ComponentModel.ISupportInitialize)this.TextEdit_description.Properties).BeginInit();
			((System.ComponentModel.ISupportInitialize)this.TextEdit_tran_type.Properties).BeginInit();
			((System.ComponentModel.ISupportInitialize)this.MemoExEdit_note.Properties).BeginInit();
			((System.ComponentModel.ISupportInitialize)this.TextEdit_sort_order.Properties).BeginInit();
			((System.ComponentModel.ISupportInitialize)this.CheckEdit1.Properties).BeginInit();
			this.SuspendLayout();
			//
			//simpleButton_OK
			//
			this.simpleButton_OK.Location = new System.Drawing.Point(372, 13);
			this.simpleButton_OK.TabIndex = 9;
			//
			//simpleButton_Cancel
			//
			this.simpleButton_Cancel.Location = new System.Drawing.Point(372, 56);
			this.simpleButton_Cancel.TabIndex = 10;
			//
			//LabelControl1
			//
			this.LabelControl1.Location = new System.Drawing.Point(13, 9);
			this.LabelControl1.Name = "LabelControl1";
			this.LabelControl1.Size = new System.Drawing.Size(11, 13);
			this.LabelControl1.TabIndex = 0;
			this.LabelControl1.Text = "ID";
			//
			//LabelControl_description
			//
			this.LabelControl_description.Location = new System.Drawing.Point(13, 35);
			this.LabelControl_description.Name = "LabelControl_description";
			this.LabelControl_description.Size = new System.Drawing.Size(53, 13);
			this.LabelControl_description.TabIndex = 2;
			this.LabelControl_description.Text = "Description";
			//
			//TextEdit_description
			//
			this.TextEdit_description.Anchor = (AnchorStyles)((AnchorStyles.Top | AnchorStyles.Left) | AnchorStyles.Right);
			this.TextEdit_description.Location = new System.Drawing.Point(86, 32);
			this.TextEdit_description.Name = "TextEdit_description";
			this.TextEdit_description.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.False;
			this.TextEdit_description.Properties.MaxLength = 50;
			this.TextEdit_description.Size = new System.Drawing.Size(280, 20);
			this.TextEdit_description.TabIndex = 3;
			//
			//LabelControl_nfcc
			//
			this.LabelControl_nfcc.Location = new System.Drawing.Point(13, 61);
			this.LabelControl_nfcc.Name = "LabelControl_nfcc";
			this.LabelControl_nfcc.Size = new System.Drawing.Size(23, 13);
			this.LabelControl_nfcc.TabIndex = 4;
			this.LabelControl_nfcc.Text = "Note";
			//
			//TextEdit_tran_type
			//
			this.TextEdit_tran_type.Location = new System.Drawing.Point(86, 6);
			this.TextEdit_tran_type.Name = "TextEdit_tran_type";
			this.TextEdit_tran_type.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.False;
			this.TextEdit_tran_type.Properties.CharacterCasing = CharacterCasing.Upper;
			this.TextEdit_tran_type.Properties.MaxLength = 2;
			this.TextEdit_tran_type.Size = new System.Drawing.Size(44, 20);
			this.TextEdit_tran_type.TabIndex = 1;
			//
			//MemoExEdit_note
			//
			this.MemoExEdit_note.Location = new System.Drawing.Point(86, 61);
			this.MemoExEdit_note.Name = "MemoExEdit_note";
			this.MemoExEdit_note.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] { new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo) });
			this.MemoExEdit_note.Properties.MaxLength = 512;
			this.MemoExEdit_note.Properties.ShowIcon = false;
			this.MemoExEdit_note.Size = new System.Drawing.Size(280, 20);
			this.MemoExEdit_note.TabIndex = 5;
			//
			//TextEdit_sort_order
			//
			this.TextEdit_sort_order.Location = new System.Drawing.Point(86, 88);
			this.TextEdit_sort_order.Name = "TextEdit_sort_order";
			this.TextEdit_sort_order.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.False;
			this.TextEdit_sort_order.Properties.Appearance.Options.UseTextOptions = true;
			this.TextEdit_sort_order.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
			this.TextEdit_sort_order.Properties.AppearanceDisabled.Options.UseTextOptions = true;
			this.TextEdit_sort_order.Properties.AppearanceDisabled.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
			this.TextEdit_sort_order.Properties.AppearanceFocused.Options.UseTextOptions = true;
			this.TextEdit_sort_order.Properties.AppearanceFocused.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
			this.TextEdit_sort_order.Properties.AppearanceReadOnly.Options.UseTextOptions = true;
			this.TextEdit_sort_order.Properties.AppearanceReadOnly.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
			this.TextEdit_sort_order.Properties.DisplayFormat.FormatString = "{0:f0}";
			this.TextEdit_sort_order.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
			this.TextEdit_sort_order.Properties.EditFormat.FormatString = "{0:f0}";
			this.TextEdit_sort_order.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
			this.TextEdit_sort_order.Properties.Mask.BeepOnError = true;
			this.TextEdit_sort_order.Properties.Mask.EditMask = "\\d+";
			this.TextEdit_sort_order.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.RegEx;
			this.TextEdit_sort_order.Properties.MaxLength = 8;
			this.TextEdit_sort_order.Size = new System.Drawing.Size(80, 20);
			this.TextEdit_sort_order.TabIndex = 7;
			//
			//LabelControl2
			//
			this.LabelControl2.Location = new System.Drawing.Point(13, 91);
			this.LabelControl2.Name = "LabelControl2";
			this.LabelControl2.Size = new System.Drawing.Size(51, 13);
			this.LabelControl2.TabIndex = 6;
			this.LabelControl2.Text = "Sort Order";
			//
			//CheckEdit1
			//
			this.CheckEdit1.Anchor = (AnchorStyles)(AnchorStyles.Bottom | AnchorStyles.Left);
			this.CheckEdit1.Location = new System.Drawing.Point(12, 126);
			this.CheckEdit1.Name = "CheckEdit1";
			this.CheckEdit1.Properties.Caption = "Transaction Type is for deposits as a subtype";
			this.CheckEdit1.Size = new System.Drawing.Size(247, 19);
			this.CheckEdit1.TabIndex = 8;
			//
			//EditForm
			//
			this.AutoScaleDimensions = new System.Drawing.SizeF(6f, 13f);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(468, 157);
			this.Controls.Add(this.CheckEdit1);
			this.Controls.Add(this.TextEdit_sort_order);
			this.Controls.Add(this.LabelControl2);
			this.Controls.Add(this.MemoExEdit_note);
			this.Controls.Add(this.TextEdit_tran_type);
			this.Controls.Add(this.LabelControl_nfcc);
			this.Controls.Add(this.TextEdit_description);
			this.Controls.Add(this.LabelControl_description);
			this.Controls.Add(this.LabelControl1);
			this.Name = "EditForm";
			this.ToolTipController1.SetSuperTip(this, null);
			this.Text = "EditForm";
			this.Controls.SetChildIndex(this.LabelControl1, 0);
			this.Controls.SetChildIndex(this.simpleButton_OK, 0);
			this.Controls.SetChildIndex(this.simpleButton_Cancel, 0);
			this.Controls.SetChildIndex(this.LabelControl_description, 0);
			this.Controls.SetChildIndex(this.TextEdit_description, 0);
			this.Controls.SetChildIndex(this.LabelControl_nfcc, 0);
			this.Controls.SetChildIndex(this.TextEdit_tran_type, 0);
			this.Controls.SetChildIndex(this.MemoExEdit_note, 0);
			this.Controls.SetChildIndex(this.LabelControl2, 0);
			this.Controls.SetChildIndex(this.TextEdit_sort_order, 0);
			this.Controls.SetChildIndex(this.CheckEdit1, 0);
			((System.ComponentModel.ISupportInitialize)this.DxErrorProvider1).EndInit();
			((System.ComponentModel.ISupportInitialize)this.TextEdit_description.Properties).EndInit();
			((System.ComponentModel.ISupportInitialize)this.TextEdit_tran_type.Properties).EndInit();
			((System.ComponentModel.ISupportInitialize)this.MemoExEdit_note.Properties).EndInit();
			((System.ComponentModel.ISupportInitialize)this.TextEdit_sort_order.Properties).EndInit();
			((System.ComponentModel.ISupportInitialize)this.CheckEdit1.Properties).EndInit();
			this.ResumeLayout(false);
			this.PerformLayout();
		}

		private DevExpress.XtraEditors.LabelControl LabelControl1;
		private DevExpress.XtraEditors.LabelControl LabelControl_description;
		private DevExpress.XtraEditors.TextEdit TextEdit_description;
		private DevExpress.XtraEditors.LabelControl LabelControl_nfcc;
		private DevExpress.XtraEditors.TextEdit TextEdit_tran_type;
		private DevExpress.XtraEditors.MemoExEdit MemoExEdit_note;
		private DevExpress.XtraEditors.TextEdit TextEdit_sort_order;
		private DevExpress.XtraEditors.LabelControl LabelControl2;
		private DevExpress.XtraEditors.CheckEdit CheckEdit1;
	}
}

