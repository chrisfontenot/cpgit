#region "Copyright 2000-2012 DebtPlus, L.L.C."
//{*******************************************************************}
//{                                                                   }
//{       DebtPlus Debt Management System                             }
//{                                                                   }
//{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
//{       ALL RIGHTS RESERVED                                         }
//{                                                                   }
//{   The entire contents of this file is protected by U.S. and       }
//{   International Copyright Laws. Unauthorized reproduction,        }
//{   reverse-engineering, and distribution of all or any portion of  }
//{   the code contained in this file is strictly prohibited and may  }
//{   result in severe civil and criminal penalties and will be       }
//{   prosecuted to the maximum extent possible under the law.        }
//{                                                                   }
//{   RESTRICTIONS                                                    }
//{                                                                   }
//{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
//{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
//{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
//{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
//{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
//{                                                                   }
//{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
//{   ADDITIONAL RESTRICTIONS.                                        }
//{                                                                   }
//{*******************************************************************}

#endregion "Copyright 2000-2012 DebtPlus, L.L.C."

using System;
using DebtPlus.LINQ;

namespace DebtPlus.UI.TableAdministration.CS.TranTypes
{
    internal partial class EditForm : Templates.EditTemplateForm
    {
        private tran_type record = null;

        internal EditForm()
            : base()
        {
            InitializeComponent();
        }

        internal EditForm(tran_type record)
            : this()
        {
            this.record = record;
            RegisterHandlers();
        }

        private void RegisterHandlers()
        {
            Load += EditForm_Load;
            TextEdit_tran_type.EditValueChanged += form_Changed;
            TextEdit_description.EditValueChanged += form_Changed;
            TextEdit_sort_order.EditValueChanged += form_Changed;
            CheckEdit1.CheckedChanged += form_Changed;
            MemoExEdit_note.EditValueChanged += form_Changed;
        }

        private void UnRegisterHandlers()
        {
            Load -= EditForm_Load;
            TextEdit_tran_type.EditValueChanged -= form_Changed;
            TextEdit_description.EditValueChanged -= form_Changed;
            TextEdit_sort_order.EditValueChanged -= form_Changed;
            CheckEdit1.CheckedChanged -= form_Changed;
            MemoExEdit_note.EditValueChanged -= form_Changed;
        }

        private void EditForm_Load(object sender, EventArgs e)
        {
            UnRegisterHandlers();
            try
            {
                TextEdit_tran_type.EditValue = record.Id;
                TextEdit_description.EditValue = record.description;
                TextEdit_sort_order.EditValue = record.sort_order;
                CheckEdit1.Checked = record.deposit_subtype;
                MemoExEdit_note.EditValue = record.note;

                simpleButton_OK.Enabled = !HasErrors();
            }
            finally
            {
                RegisterHandlers();
            }
        }

        private void form_Changed(object sender, EventArgs e)
        {
            simpleButton_OK.Enabled = !HasErrors();
        }

        private bool HasErrors()
        {
            if (string.IsNullOrWhiteSpace(DebtPlus.Utils.Nulls.v_String(TextEdit_description.EditValue)))
            {
                return true;
            }

            if (!DebtPlus.Utils.Nulls.v_Int32(TextEdit_sort_order.EditValue).HasValue)
            {
                return true;
            }

            return false;
        }

        protected override void simpleButton_OK_Click(object sender, EventArgs e)
        {
            record.Id = DebtPlus.Utils.Nulls.v_String(TextEdit_tran_type.EditValue);
            record.description = DebtPlus.Utils.Nulls.v_String(TextEdit_description.EditValue);
            record.sort_order = DebtPlus.Utils.Nulls.v_Int32(TextEdit_sort_order.EditValue).GetValueOrDefault();
            record.deposit_subtype = CheckEdit1.Checked;
            record.note = DebtPlus.Utils.Nulls.v_String(MemoExEdit_note.EditValue);

            base.simpleButton_OK_Click(sender, e);
        }
    }
}