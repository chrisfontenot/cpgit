
namespace DebtPlus.UI.TableAdministration.CS.TranTypes
{
    partial class MainForm
    {
		//Form overrides dispose to clean up the component list.
		[System.Diagnostics.DebuggerNonUserCode]
		protected override void Dispose(bool disposing)
		{
            try
            {
                if (disposing)
                {
                    if (components != null) components.Dispose();
                    if (bc != null) bc.Dispose();
                }
                components = null;
                bc = null;
            }

            finally
            {
                base.Dispose(disposing);
            }
        }

		//Required by the Windows Form Designer
		private System.ComponentModel.IContainer components = null;

		//NOTE: The following procedure is required by the Windows Form Designer
		//It can be modified using the Windows Form Designer.  
		//Do not modify it using the code editor.
		[System.Diagnostics.DebuggerStepThrough]
		private void InitializeComponent()
		{
			this.gridColumn_ID = new DevExpress.XtraGrid.Columns.GridColumn();
			this.gridColumn_ID.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.True;
			this.gridColumn_description = new DevExpress.XtraGrid.Columns.GridColumn();
			this.gridColumn_description.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.True;
			this.gridColumn_sort_order = new DevExpress.XtraGrid.Columns.GridColumn();
			this.gridColumn_sort_order.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.True;
			this.gridColumn_deposit_subtype = new DevExpress.XtraGrid.Columns.GridColumn();
			this.gridColumn_deposit_subtype.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.True;
			this.gridColumn_note = new DevExpress.XtraGrid.Columns.GridColumn();
			this.gridColumn_note.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.True;
			((System.ComponentModel.ISupportInitialize)this.gridControl1).BeginInit();
			((System.ComponentModel.ISupportInitialize)this.gridView1).BeginInit();
			((System.ComponentModel.ISupportInitialize)this.DxErrorProvider1).BeginInit();
			this.SuspendLayout();
			//
			//gridView1
			//
			this.gridView1.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
				this.gridColumn_ID,
				this.gridColumn_deposit_subtype,
				this.gridColumn_description,
				this.gridColumn_sort_order,
				this.gridColumn_note
			});
			this.gridView1.OptionsBehavior.Editable = false;
			this.gridView1.OptionsNavigation.AutoFocusNewRow = true;
			this.gridView1.OptionsSelection.InvertSelection = true;
			this.gridView1.OptionsView.RowAutoHeight = true;
			this.gridView1.OptionsView.ShowGroupPanel = false;
			this.gridView1.OptionsView.ShowIndicator = false;
			this.gridView1.PreviewFieldName = "note";
			this.gridView1.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] { new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.gridColumn_sort_order, DevExpress.Data.ColumnSortOrder.Ascending) });
			//
			//gridColumn_ID
			//
			this.gridColumn_ID.Caption = "Type";
			this.gridColumn_ID.FieldName = "Id";
			this.gridColumn_ID.Name = "gridColumn_ID";
			this.gridColumn_ID.Visible = true;
			this.gridColumn_ID.VisibleIndex = 0;
			this.gridColumn_ID.Width = 84;
			//
			//gridColumn_Name
			//
			this.gridColumn_description.Caption = "Description";
			this.gridColumn_description.FieldName = "description";
			this.gridColumn_description.Name = "gridColumn_Name";
			this.gridColumn_description.Visible = true;
			this.gridColumn_description.VisibleIndex = 2;
			this.gridColumn_description.Width = 290;
			//
			//gridColumn_sort_order
			//
			this.gridColumn_sort_order.Caption = "Sort Order";
			this.gridColumn_sort_order.DisplayFormat.FormatString = "{0:f0}";
			this.gridColumn_sort_order.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
			this.gridColumn_sort_order.FieldName = "sort_order";
			this.gridColumn_sort_order.GroupFormat.FormatString = "{0:f0}";
			this.gridColumn_sort_order.GroupFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
			this.gridColumn_sort_order.Name = "gridColumn_sort_order";
			this.gridColumn_sort_order.SortMode = DevExpress.XtraGrid.ColumnSortMode.Value;
			this.gridColumn_sort_order.Visible = true;
			this.gridColumn_sort_order.VisibleIndex = 3;
			this.gridColumn_sort_order.Width = 121;
			//
			//gridColumn_deposit_subtype
			//
			this.gridColumn_deposit_subtype.AppearanceCell.Options.UseTextOptions = true;
			this.gridColumn_deposit_subtype.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
			this.gridColumn_deposit_subtype.AppearanceHeader.Options.UseTextOptions = true;
			this.gridColumn_deposit_subtype.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
			this.gridColumn_deposit_subtype.Caption = "Deposit";
			this.gridColumn_deposit_subtype.FieldName = "deposit_subtype";
			this.gridColumn_deposit_subtype.Name = "gridColumn_deposit_subtype";
			this.gridColumn_deposit_subtype.Visible = true;
			this.gridColumn_deposit_subtype.VisibleIndex = 1;
			this.gridColumn_deposit_subtype.Width = 94;
			//
			//gridColumn_note
			//
			this.gridColumn_note.Caption = "Note";
			this.gridColumn_note.FieldName = "note";
			this.gridColumn_note.Name = "gridColumn_note";
			//
			//MainForm
			//
			this.AutoScaleDimensions = new System.Drawing.SizeF(6f, 13f);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(528, 294);
			this.Name = "MainForm";
			this.ToolTipController1.SetSuperTip(this, null);
			this.Text = "Transaction Types";
			((System.ComponentModel.ISupportInitialize)this.gridControl1).EndInit();
			((System.ComponentModel.ISupportInitialize)this.gridView1).EndInit();
			((System.ComponentModel.ISupportInitialize)this.DxErrorProvider1).EndInit();
			this.ResumeLayout(false);
		}

		private DevExpress.XtraGrid.Columns.GridColumn gridColumn_ID;
		private DevExpress.XtraGrid.Columns.GridColumn gridColumn_description;
		private DevExpress.XtraGrid.Columns.GridColumn gridColumn_sort_order;
		private DevExpress.XtraGrid.Columns.GridColumn gridColumn_deposit_subtype;
		private DevExpress.XtraGrid.Columns.GridColumn gridColumn_note;
	}
}


