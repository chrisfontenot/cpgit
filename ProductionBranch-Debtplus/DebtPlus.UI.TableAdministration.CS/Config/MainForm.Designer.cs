using System;
using System.Windows.Forms;
using System.Drawing;
using DebtPlus.LINQ;
using System.Linq;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Diagnostics;
using DebtPlus.UI.Creditor.Widgets.Controls;

namespace DebtPlus.UI.TableAdministration.CS.Config
{
	partial class MainForm : global::DebtPlus.Data.Forms.DebtPlusForm
	{
		//Form overrides dispose to clean up the component list.
		[System.Diagnostics.DebuggerNonUserCode]
		protected override void Dispose(bool disposing)
		{
            try
            {
                if (disposing)
                {
                    if (components != null) components.Dispose();
                    if (bc != null) bc.Dispose();
                    UnRegisterHandlers();
                }
                bc = null;
                components = null;
            }
            finally
            {
                base.Dispose(disposing);
            }
        }

		//Required by the Windows Form Designer
		private System.ComponentModel.IContainer components = null;

		//NOTE: The following procedure is required by the Windows Form Designer
		//It can be modified using the Windows Form Designer.  
		//Do not modify it using the code editor.
		[System.Diagnostics.DebuggerStepThrough]
		private void InitializeComponent()
		{
            this.components = new System.ComponentModel.Container();
            DevExpress.Utils.SuperToolTip superToolTip1 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipItem toolTipItem1 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SuperToolTip superToolTip2 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipItem toolTipItem2 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SuperToolTip superToolTip3 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipItem toolTipItem3 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SuperToolTip superToolTip4 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem1 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem4 = new DevExpress.Utils.ToolTipItem();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MainForm));
            DevExpress.Utils.SuperToolTip superToolTip5 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem2 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem5 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SuperToolTip superToolTip6 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem3 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem6 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SuperToolTip superToolTip7 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem4 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem7 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SuperToolTip superToolTip8 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem5 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem8 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SuperToolTip superToolTip9 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem6 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem9 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SuperToolTip superToolTip10 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem7 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem10 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem8 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.SuperToolTip superToolTip11 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem9 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem11 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem10 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.SuperToolTip superToolTip12 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem11 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem12 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject1 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject2 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject3 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject4 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SuperToolTip superToolTip13 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem12 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem13 = new DevExpress.Utils.ToolTipItem();
            this.ConfigFees1 = new DebtPlus.UI.TableAdministration.CS.Config.ConfigFees();
            this.PopupMenu1 = new DevExpress.XtraBars.PopupMenu(this.components);
            this.TelephoneNumberRecordControl3 = new DebtPlus.Data.Controls.TelephoneNumberRecordControl();
            this.TelephoneNumberRecordControl2 = new DebtPlus.Data.Controls.TelephoneNumberRecordControl();
            this.TelephoneNumberRecordControl1 = new DebtPlus.Data.Controls.TelephoneNumberRecordControl();
            this.TextEdit_areacode = new DevExpress.XtraEditors.TextEdit();
            this.LayoutControl1 = new DevExpress.XtraLayout.LayoutControl();
            this.PercentEdit_ComparisonRate = new DebtPlus.Data.Controls.PercentEdit();
            this.barManager1 = new DevExpress.XtraBars.BarManager(this.components);
            this.Bar2 = new DevExpress.XtraBars.Bar();
            this.BarSubItem1 = new DevExpress.XtraBars.BarSubItem();
            this.BarButtonItem_File_Exit = new DevExpress.XtraBars.BarButtonItem();
            this.barDockControlTop = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlBottom = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlLeft = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlRight = new DevExpress.XtraBars.BarDockControl();
            this.LookUpEdit_ComparisonPercent = new DevExpress.XtraEditors.LookUpEdit();
            this.CalcEdit_ComparisonAmount = new DevExpress.XtraEditors.CalcEdit();
            this.SpinEdit_chks_per_invoice = new DevExpress.XtraEditors.SpinEdit();
            this.CalcEdit_bal_verify_months = new DevExpress.XtraEditors.CalcEdit();
            this.SpinEdit_default_till_missed = new DevExpress.XtraEditors.SpinEdit();
            this.SpinEdit_acceptance_days = new DevExpress.XtraEditors.SpinEdit();
            this.TextEdit_web_site = new DevExpress.XtraEditors.TextEdit();
            this.CheckEdit_rpps_resend_msg = new DevExpress.XtraEditors.CheckEdit();
            this.CalcEdit_payments_padd_greater = new DevExpress.XtraEditors.CalcEdit();
            this.CalcEdit_payments_padd_less = new DevExpress.XtraEditors.CalcEdit();
            this.AddressRecordControl_Alternate = new DebtPlus.Data.Controls.AddressRecordControl();
            this.CreditorID_deduct_creditor = new DebtPlus.UI.Creditor.Widgets.Controls.CreditorID();
            this.CreditorID_counseling_creditor = new DebtPlus.UI.Creditor.Widgets.Controls.CreditorID();
            this.PercentEdit_threshold = new DebtPlus.Data.Controls.PercentEdit();
            this.TextEdit_name = new DevExpress.XtraEditors.TextEdit();
            this.CalcEdit_min_amount = new DevExpress.XtraEditors.CalcEdit();
            this.CreditorID_paf_creditor = new DebtPlus.UI.Creditor.Widgets.Controls.CreditorID();
            this.CreditorID_setup_creditor = new DebtPlus.UI.Creditor.Widgets.Controls.CreditorID();
            this.AddressRecordControl1 = new DebtPlus.Data.Controls.AddressRecordControl();
            this.LayoutControlGroup1 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.TabbedControlGroup1 = new DevExpress.XtraLayout.TabbedControlGroup();
            this.LayoutControlGroup2 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.LayoutControlItem2 = new DevExpress.XtraLayout.LayoutControlItem();
            this.LayoutControlItem4 = new DevExpress.XtraLayout.LayoutControlItem();
            this.LayoutControlItem5 = new DevExpress.XtraLayout.LayoutControlItem();
            this.LayoutControlItem6 = new DevExpress.XtraLayout.LayoutControlItem();
            this.LayoutControlItem14 = new DevExpress.XtraLayout.LayoutControlItem();
            this.EmptySpaceItem1 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.EmptySpaceItem2 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.EmptySpaceItem5 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.EmptySpaceItem6 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.LayoutControlItem22 = new DevExpress.XtraLayout.LayoutControlItem();
            this.LayoutControlItem3 = new DevExpress.XtraLayout.LayoutControlItem();
            this.LayoutControlItem1 = new DevExpress.XtraLayout.LayoutControlItem();
            this.LayoutControlGroup4 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.LayoutControlItem11 = new DevExpress.XtraLayout.LayoutControlItem();
            this.LayoutControlGroup12 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.LayoutControlItem12 = new DevExpress.XtraLayout.LayoutControlItem();
            this.LayoutControlItem13 = new DevExpress.XtraLayout.LayoutControlItem();
            this.LayoutControlGroup13 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.LayoutControlItem25 = new DevExpress.XtraLayout.LayoutControlItem();
            this.LayoutControlItem23 = new DevExpress.XtraLayout.LayoutControlItem();
            this.LayoutControlItem24 = new DevExpress.XtraLayout.LayoutControlItem();
            this.LayoutControlGroup3 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.LayoutControlItem7 = new DevExpress.XtraLayout.LayoutControlItem();
            this.LayoutControlItem8 = new DevExpress.XtraLayout.LayoutControlItem();
            this.LayoutControlItem9 = new DevExpress.XtraLayout.LayoutControlItem();
            this.LayoutControlItem10 = new DevExpress.XtraLayout.LayoutControlItem();
            this.EmptySpaceItem3 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.LayoutControlGroup5 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.LayoutControlGroup6 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.LayoutControlItem17 = new DevExpress.XtraLayout.LayoutControlItem();
            this.LayoutControlGroup7 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.LayoutControlItem16 = new DevExpress.XtraLayout.LayoutControlItem();
            this.LayoutControlItem15 = new DevExpress.XtraLayout.LayoutControlItem();
            this.LayoutControlGroup8 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.LayoutControlItem18 = new DevExpress.XtraLayout.LayoutControlItem();
            this.LayoutControlGroup9 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.LayoutControlItem19 = new DevExpress.XtraLayout.LayoutControlItem();
            this.LayoutControlGroup10 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.LayoutControlItem20 = new DevExpress.XtraLayout.LayoutControlItem();
            this.EmptySpaceItem4 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.LayoutControlGroup11 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.LayoutControlItem21 = new DevExpress.XtraLayout.LayoutControlItem();
            ((System.ComponentModel.ISupportInitialize)(this.DxErrorProvider1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.PopupMenu1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TelephoneNumberRecordControl3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TelephoneNumberRecordControl2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TelephoneNumberRecordControl1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TextEdit_areacode.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LayoutControl1)).BeginInit();
            this.LayoutControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.PercentEdit_ComparisonRate.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LookUpEdit_ComparisonPercent.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.CalcEdit_ComparisonAmount.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.SpinEdit_chks_per_invoice.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.CalcEdit_bal_verify_months.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.SpinEdit_default_till_missed.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.SpinEdit_acceptance_days.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TextEdit_web_site.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.CheckEdit_rpps_resend_msg.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.CalcEdit_payments_padd_greater.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.CalcEdit_payments_padd_less.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.AddressRecordControl_Alternate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.CreditorID_deduct_creditor.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.CreditorID_counseling_creditor.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.PercentEdit_threshold.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TextEdit_name.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.CalcEdit_min_amount.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.CreditorID_paf_creditor.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.CreditorID_setup_creditor.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.AddressRecordControl1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LayoutControlGroup1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TabbedControlGroup1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LayoutControlGroup2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LayoutControlItem2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LayoutControlItem4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LayoutControlItem5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LayoutControlItem6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LayoutControlItem14)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.EmptySpaceItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.EmptySpaceItem2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.EmptySpaceItem5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.EmptySpaceItem6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LayoutControlItem22)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LayoutControlItem3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LayoutControlItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LayoutControlGroup4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LayoutControlItem11)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LayoutControlGroup12)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LayoutControlItem12)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LayoutControlItem13)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LayoutControlGroup13)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LayoutControlItem25)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LayoutControlItem23)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LayoutControlItem24)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LayoutControlGroup3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LayoutControlItem7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LayoutControlItem8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LayoutControlItem9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LayoutControlItem10)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.EmptySpaceItem3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LayoutControlGroup5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LayoutControlGroup6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LayoutControlItem17)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LayoutControlGroup7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LayoutControlItem16)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LayoutControlItem15)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LayoutControlGroup8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LayoutControlItem18)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LayoutControlGroup9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LayoutControlItem19)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LayoutControlGroup10)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LayoutControlItem20)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.EmptySpaceItem4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LayoutControlGroup11)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LayoutControlItem21)).BeginInit();
            this.SuspendLayout();
            // 
            // DefaultLookAndFeel1
            // 
            this.DefaultLookAndFeel1.LookAndFeel.SkinName = "McSkin";
            // 
            // ConfigFees1
            // 
            this.ConfigFees1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.ConfigFees1.Location = new System.Drawing.Point(24, 162);
            this.ConfigFees1.Name = "ConfigFees1";
            this.ConfigFees1.Size = new System.Drawing.Size(572, 180);
            this.ConfigFees1.TabIndex = 4;
            // 
            // PopupMenu1
            // 
            this.PopupMenu1.Name = "PopupMenu1";
            // 
            // TelephoneNumberRecordControl3
            // 
            this.TelephoneNumberRecordControl3.ErrorIcon = null;
            this.TelephoneNumberRecordControl3.ErrorText = "";
            this.TelephoneNumberRecordControl3.Location = new System.Drawing.Point(132, 322);
            this.TelephoneNumberRecordControl3.Margin = new System.Windows.Forms.Padding(0);
            this.TelephoneNumberRecordControl3.Name = "TelephoneNumberRecordControl3";
            this.TelephoneNumberRecordControl3.Size = new System.Drawing.Size(136, 20);
            toolTipItem1.Text = "This is the FAX number of your organization. It should be a general purpose FAX n" +
    "umber and not a specific counselor\'s FAX number.";
            superToolTip1.Items.Add(toolTipItem1);
            this.ToolTipController1.SetSuperTip(this.TelephoneNumberRecordControl3, superToolTip1);
            this.TelephoneNumberRecordControl3.TabIndex = 11;
            // 
            // TelephoneNumberRecordControl2
            // 
            this.TelephoneNumberRecordControl2.ErrorIcon = null;
            this.TelephoneNumberRecordControl2.ErrorText = "";
            this.TelephoneNumberRecordControl2.Location = new System.Drawing.Point(380, 322);
            this.TelephoneNumberRecordControl2.Margin = new System.Windows.Forms.Padding(0);
            this.TelephoneNumberRecordControl2.Name = "TelephoneNumberRecordControl2";
            this.TelephoneNumberRecordControl2.Size = new System.Drawing.Size(141, 20);
            toolTipItem2.Text = "If you have a second phone number, such as a toll free number, list it here.";
            superToolTip2.Items.Add(toolTipItem2);
            this.ToolTipController1.SetSuperTip(this.TelephoneNumberRecordControl2, superToolTip2);
            this.TelephoneNumberRecordControl2.TabIndex = 9;
            // 
            // TelephoneNumberRecordControl1
            // 
            this.TelephoneNumberRecordControl1.ErrorIcon = null;
            this.TelephoneNumberRecordControl1.ErrorText = "";
            this.TelephoneNumberRecordControl1.Location = new System.Drawing.Point(380, 298);
            this.TelephoneNumberRecordControl1.Margin = new System.Windows.Forms.Padding(0);
            this.TelephoneNumberRecordControl1.Name = "TelephoneNumberRecordControl1";
            this.TelephoneNumberRecordControl1.Size = new System.Drawing.Size(141, 20);
            toolTipItem3.Text = "This is the primary telephone number for your organization. It is not normally a " +
    "toll-free number.";
            superToolTip3.Items.Add(toolTipItem3);
            this.ToolTipController1.SetSuperTip(this.TelephoneNumberRecordControl1, superToolTip3);
            this.TelephoneNumberRecordControl1.TabIndex = 7;
            // 
            // TextEdit_areacode
            // 
            this.TextEdit_areacode.Location = new System.Drawing.Point(132, 298);
            this.TextEdit_areacode.Name = "TextEdit_areacode";
            this.TextEdit_areacode.Properties.Mask.EditMask = "000";
            this.TextEdit_areacode.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Simple;
            this.TextEdit_areacode.Properties.MaxLength = 3;
            this.TextEdit_areacode.Size = new System.Drawing.Size(136, 20);
            this.TextEdit_areacode.StyleController = this.LayoutControl1;
            this.TextEdit_areacode.TabIndex = 5;
            this.TextEdit_areacode.ToolTip = "Telephone Area (or city) code when one is not given";
            // 
            // LayoutControl1
            // 
            this.LayoutControl1.Controls.Add(this.PercentEdit_ComparisonRate);
            this.LayoutControl1.Controls.Add(this.LookUpEdit_ComparisonPercent);
            this.LayoutControl1.Controls.Add(this.CalcEdit_ComparisonAmount);
            this.LayoutControl1.Controls.Add(this.SpinEdit_chks_per_invoice);
            this.LayoutControl1.Controls.Add(this.CalcEdit_bal_verify_months);
            this.LayoutControl1.Controls.Add(this.SpinEdit_default_till_missed);
            this.LayoutControl1.Controls.Add(this.SpinEdit_acceptance_days);
            this.LayoutControl1.Controls.Add(this.TextEdit_web_site);
            this.LayoutControl1.Controls.Add(this.CheckEdit_rpps_resend_msg);
            this.LayoutControl1.Controls.Add(this.CalcEdit_payments_padd_greater);
            this.LayoutControl1.Controls.Add(this.CalcEdit_payments_padd_less);
            this.LayoutControl1.Controls.Add(this.AddressRecordControl_Alternate);
            this.LayoutControl1.Controls.Add(this.ConfigFees1);
            this.LayoutControl1.Controls.Add(this.CreditorID_deduct_creditor);
            this.LayoutControl1.Controls.Add(this.TelephoneNumberRecordControl3);
            this.LayoutControl1.Controls.Add(this.CreditorID_counseling_creditor);
            this.LayoutControl1.Controls.Add(this.PercentEdit_threshold);
            this.LayoutControl1.Controls.Add(this.TextEdit_name);
            this.LayoutControl1.Controls.Add(this.CalcEdit_min_amount);
            this.LayoutControl1.Controls.Add(this.CreditorID_paf_creditor);
            this.LayoutControl1.Controls.Add(this.TelephoneNumberRecordControl2);
            this.LayoutControl1.Controls.Add(this.CreditorID_setup_creditor);
            this.LayoutControl1.Controls.Add(this.AddressRecordControl1);
            this.LayoutControl1.Controls.Add(this.TelephoneNumberRecordControl1);
            this.LayoutControl1.Controls.Add(this.TextEdit_areacode);
            this.LayoutControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.LayoutControl1.Location = new System.Drawing.Point(0, 25);
            this.LayoutControl1.Name = "LayoutControl1";
            this.LayoutControl1.OptionsCustomizationForm.DesignTimeCustomizationFormPositionAndSize = new System.Drawing.Rectangle(404, 219, 250, 350);
            this.LayoutControl1.Root = this.LayoutControlGroup1;
            this.LayoutControl1.Size = new System.Drawing.Size(620, 366);
            this.LayoutControl1.TabIndex = 4;
            this.LayoutControl1.Text = "LayoutControl1";
            // 
            // PercentEdit_ComparisonRate
            // 
            this.PercentEdit_ComparisonRate.Location = new System.Drawing.Point(432, 78);
            this.PercentEdit_ComparisonRate.MenuManager = this.barManager1;
            this.PercentEdit_ComparisonRate.Name = "PercentEdit_ComparisonRate";
            this.PercentEdit_ComparisonRate.Properties.Allow100Percent = false;
            this.PercentEdit_ComparisonRate.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.PercentEdit_ComparisonRate.Properties.DisplayFormat.FormatString = "{0:p3}";
            this.PercentEdit_ComparisonRate.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.PercentEdit_ComparisonRate.Properties.EditFormat.FormatString = "{0:f3}";
            this.PercentEdit_ComparisonRate.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.PercentEdit_ComparisonRate.Properties.Precision = 3;
            this.PercentEdit_ComparisonRate.Size = new System.Drawing.Size(152, 20);
            this.PercentEdit_ComparisonRate.StyleController = this.LayoutControl1;
            this.PercentEdit_ComparisonRate.TabIndex = 23;
            this.PercentEdit_ComparisonRate.ToolTip = "This is the normal interest amount for the DMP plan when the creditor is not know" +
    "n. For a known creditor, we use the figure in the creditor.";
            // 
            // barManager1
            // 
            this.barManager1.Bars.AddRange(new DevExpress.XtraBars.Bar[] {
            this.Bar2});
            this.barManager1.DockControls.Add(this.barDockControlTop);
            this.barManager1.DockControls.Add(this.barDockControlBottom);
            this.barManager1.DockControls.Add(this.barDockControlLeft);
            this.barManager1.DockControls.Add(this.barDockControlRight);
            this.barManager1.Form = this;
            this.barManager1.Items.AddRange(new DevExpress.XtraBars.BarItem[] {
            this.BarSubItem1,
            this.BarButtonItem_File_Exit});
            this.barManager1.MainMenu = this.Bar2;
            this.barManager1.MaxItemId = 2;
            // 
            // Bar2
            // 
            this.Bar2.BarName = "Main menu";
            this.Bar2.DockCol = 0;
            this.Bar2.DockRow = 0;
            this.Bar2.DockStyle = DevExpress.XtraBars.BarDockStyle.Top;
            this.Bar2.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.BarSubItem1)});
            this.Bar2.OptionsBar.MultiLine = true;
            this.Bar2.OptionsBar.UseWholeRow = true;
            this.Bar2.Text = "Main menu";
            // 
            // BarSubItem1
            // 
            this.BarSubItem1.Caption = "&File";
            this.BarSubItem1.Id = 0;
            this.BarSubItem1.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.BarButtonItem_File_Exit)});
            this.BarSubItem1.Name = "BarSubItem1";
            // 
            // BarButtonItem_File_Exit
            // 
            this.BarButtonItem_File_Exit.Caption = "&Exit";
            this.BarButtonItem_File_Exit.Id = 1;
            this.BarButtonItem_File_Exit.Name = "BarButtonItem_File_Exit";
            // 
            // barDockControlTop
            // 
            this.barDockControlTop.CausesValidation = false;
            this.barDockControlTop.Dock = System.Windows.Forms.DockStyle.Top;
            this.barDockControlTop.Location = new System.Drawing.Point(0, 0);
            this.barDockControlTop.Size = new System.Drawing.Size(620, 25);
            // 
            // barDockControlBottom
            // 
            this.barDockControlBottom.CausesValidation = false;
            this.barDockControlBottom.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.barDockControlBottom.Location = new System.Drawing.Point(0, 391);
            this.barDockControlBottom.Size = new System.Drawing.Size(620, 0);
            // 
            // barDockControlLeft
            // 
            this.barDockControlLeft.CausesValidation = false;
            this.barDockControlLeft.Dock = System.Windows.Forms.DockStyle.Left;
            this.barDockControlLeft.Location = new System.Drawing.Point(0, 25);
            this.barDockControlLeft.Size = new System.Drawing.Size(0, 366);
            // 
            // barDockControlRight
            // 
            this.barDockControlRight.CausesValidation = false;
            this.barDockControlRight.Dock = System.Windows.Forms.DockStyle.Right;
            this.barDockControlRight.Location = new System.Drawing.Point(620, 25);
            this.barDockControlRight.Size = new System.Drawing.Size(0, 366);
            // 
            // LookUpEdit_ComparisonPercent
            // 
            this.LookUpEdit_ComparisonPercent.Location = new System.Drawing.Point(432, 126);
            this.LookUpEdit_ComparisonPercent.MenuManager = this.barManager1;
            this.LookUpEdit_ComparisonPercent.Name = "LookUpEdit_ComparisonPercent";
            this.LookUpEdit_ComparisonPercent.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LookUpEdit_ComparisonPercent.Properties.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("rate", "Rate", 20, DevExpress.Utils.FormatType.Numeric, "p3", false, DevExpress.Utils.HorzAlignment.Far),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("description", "Description", 20, DevExpress.Utils.FormatType.None, "", true, DevExpress.Utils.HorzAlignment.Near)});
            this.LookUpEdit_ComparisonPercent.Properties.DisplayMember = "description";
            this.LookUpEdit_ComparisonPercent.Properties.NullText = "";
            this.LookUpEdit_ComparisonPercent.Properties.ShowFooter = false;
            this.LookUpEdit_ComparisonPercent.Properties.ShowHeader = false;
            this.LookUpEdit_ComparisonPercent.Properties.ValueMember = "rate";
            this.LookUpEdit_ComparisonPercent.Size = new System.Drawing.Size(152, 20);
            this.LookUpEdit_ComparisonPercent.StyleController = this.LayoutControl1;
            this.LookUpEdit_ComparisonPercent.TabIndex = 22;
            this.LookUpEdit_ComparisonPercent.ToolTip = "This is the figure used by default when creating new debts. It is the normal amou" +
    "nt that the client pays off the \"Self\" plan.";
            // 
            // CalcEdit_ComparisonAmount
            // 
            this.CalcEdit_ComparisonAmount.Location = new System.Drawing.Point(432, 102);
            this.CalcEdit_ComparisonAmount.MenuManager = this.barManager1;
            this.CalcEdit_ComparisonAmount.Name = "CalcEdit_ComparisonAmount";
            this.CalcEdit_ComparisonAmount.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.False;
            this.CalcEdit_ComparisonAmount.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.CalcEdit_ComparisonAmount.Properties.DisplayFormat.FormatString = "c";
            this.CalcEdit_ComparisonAmount.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.CalcEdit_ComparisonAmount.Properties.EditFormat.FormatString = "c";
            this.CalcEdit_ComparisonAmount.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.CalcEdit_ComparisonAmount.Properties.Mask.BeepOnError = true;
            this.CalcEdit_ComparisonAmount.Properties.Mask.EditMask = "c";
            this.CalcEdit_ComparisonAmount.Properties.Precision = 2;
            this.CalcEdit_ComparisonAmount.Size = new System.Drawing.Size(152, 20);
            this.CalcEdit_ComparisonAmount.StyleController = this.LayoutControl1;
            this.CalcEdit_ComparisonAmount.TabIndex = 21;
            this.CalcEdit_ComparisonAmount.ToolTip = "Minimum payment amount for the Self plan. This is the floor payment amount for th" +
    "e debts. It is typically $15.";
            // 
            // SpinEdit_chks_per_invoice
            // 
            this.SpinEdit_chks_per_invoice.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.SpinEdit_chks_per_invoice.Location = new System.Drawing.Point(144, 170);
            this.SpinEdit_chks_per_invoice.MenuManager = this.barManager1;
            this.SpinEdit_chks_per_invoice.Name = "SpinEdit_chks_per_invoice";
            this.SpinEdit_chks_per_invoice.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.SpinEdit_chks_per_invoice.Size = new System.Drawing.Size(175, 20);
            this.SpinEdit_chks_per_invoice.StyleController = this.LayoutControl1;
            toolTipTitleItem1.Text = "INVOICE";
            toolTipItem4.LeftIndent = 6;
            toolTipItem4.Text = resources.GetString("toolTipItem4.Text");
            superToolTip4.Items.Add(toolTipTitleItem1);
            superToolTip4.Items.Add(toolTipItem4);
            this.SpinEdit_chks_per_invoice.SuperTip = superToolTip4;
            this.SpinEdit_chks_per_invoice.TabIndex = 19;
            // 
            // CalcEdit_bal_verify_months
            // 
            this.CalcEdit_bal_verify_months.Location = new System.Drawing.Point(455, 78);
            this.CalcEdit_bal_verify_months.MenuManager = this.barManager1;
            this.CalcEdit_bal_verify_months.Name = "CalcEdit_bal_verify_months";
            this.CalcEdit_bal_verify_months.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.CalcEdit_bal_verify_months.Size = new System.Drawing.Size(129, 20);
            this.CalcEdit_bal_verify_months.StyleController = this.LayoutControl1;
            toolTipTitleItem2.Text = "MONTHS BEFORE PAYOFF";
            toolTipItem5.LeftIndent = 6;
            toolTipItem5.Text = resources.GetString("toolTipItem5.Text");
            superToolTip5.Items.Add(toolTipTitleItem2);
            superToolTip5.Items.Add(toolTipItem5);
            this.CalcEdit_bal_verify_months.SuperTip = superToolTip5;
            this.CalcEdit_bal_verify_months.TabIndex = 18;
            // 
            // SpinEdit_default_till_missed
            // 
            this.SpinEdit_default_till_missed.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.SpinEdit_default_till_missed.Location = new System.Drawing.Point(455, 146);
            this.SpinEdit_default_till_missed.MenuManager = this.barManager1;
            this.SpinEdit_default_till_missed.Name = "SpinEdit_default_till_missed";
            this.SpinEdit_default_till_missed.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.SpinEdit_default_till_missed.Size = new System.Drawing.Size(129, 20);
            this.SpinEdit_default_till_missed.StyleController = this.LayoutControl1;
            toolTipTitleItem3.Text = "MISSED APPOINTMENTS";
            toolTipItem6.LeftIndent = 6;
            toolTipItem6.Text = resources.GetString("toolTipItem6.Text");
            superToolTip6.Items.Add(toolTipTitleItem3);
            superToolTip6.Items.Add(toolTipItem6);
            this.SpinEdit_default_till_missed.SuperTip = superToolTip6;
            this.SpinEdit_default_till_missed.TabIndex = 17;
            // 
            // SpinEdit_acceptance_days
            // 
            this.SpinEdit_acceptance_days.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.SpinEdit_acceptance_days.Location = new System.Drawing.Point(455, 214);
            this.SpinEdit_acceptance_days.MenuManager = this.barManager1;
            this.SpinEdit_acceptance_days.Name = "SpinEdit_acceptance_days";
            this.SpinEdit_acceptance_days.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.SpinEdit_acceptance_days.Size = new System.Drawing.Size(129, 20);
            this.SpinEdit_acceptance_days.StyleController = this.LayoutControl1;
            toolTipTitleItem4.Text = "Proposal ACCEPTED BY DEFAULT";
            toolTipItem7.LeftIndent = 6;
            toolTipItem7.Text = "This is the number of days that a proposal may be outstanding with the creditor b" +
    "efore the system will assume that the proposal was accepted.";
            superToolTip7.Items.Add(toolTipTitleItem4);
            superToolTip7.Items.Add(toolTipItem7);
            this.SpinEdit_acceptance_days.SuperTip = superToolTip7;
            this.SpinEdit_acceptance_days.TabIndex = 16;
            // 
            // TextEdit_web_site
            // 
            this.TextEdit_web_site.Location = new System.Drawing.Point(132, 274);
            this.TextEdit_web_site.MenuManager = this.barManager1;
            this.TextEdit_web_site.Name = "TextEdit_web_site";
            this.TextEdit_web_site.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.True;
            this.TextEdit_web_site.Properties.MaxLength = 256;
            this.TextEdit_web_site.Properties.NullValuePrompt = "http://... or https://...";
            this.TextEdit_web_site.Properties.NullValuePromptShowForEmptyValue = true;
            this.TextEdit_web_site.Properties.ValidateOnEnterKey = true;
            this.TextEdit_web_site.Size = new System.Drawing.Size(464, 20);
            this.TextEdit_web_site.StyleController = this.LayoutControl1;
            superToolTip8.AllowHtmlText = DevExpress.Utils.DefaultBoolean.True;
            toolTipTitleItem5.Text = "Website Address";
            toolTipItem8.LeftIndent = 6;
            toolTipItem8.Text = "This is the company\'s website location. It must be prefixed with <b>http://</b> o" +
    "r <b>https://</b> and may look similar to the following:/r/n/r/nhttp://www.DebtP" +
    "lus.com";
            superToolTip8.Items.Add(toolTipTitleItem5);
            superToolTip8.Items.Add(toolTipItem8);
            this.TextEdit_web_site.SuperTip = superToolTip8;
            this.TextEdit_web_site.TabIndex = 20;
            // 
            // CheckEdit_rpps_resend_msg
            // 
            this.CheckEdit_rpps_resend_msg.Location = new System.Drawing.Point(36, 238);
            this.CheckEdit_rpps_resend_msg.MenuManager = this.barManager1;
            this.CheckEdit_rpps_resend_msg.Name = "CheckEdit_rpps_resend_msg";
            this.CheckEdit_rpps_resend_msg.Properties.Caption = "Include note to resend funds on non-serious rejects";
            this.CheckEdit_rpps_resend_msg.Size = new System.Drawing.Size(283, 20);
            this.CheckEdit_rpps_resend_msg.StyleController = this.LayoutControl1;
            toolTipTitleItem6.Text = "RESUBMIT MESSAGES";
            toolTipItem9.LeftIndent = 6;
            toolTipItem9.Text = resources.GetString("toolTipItem9.Text");
            superToolTip9.Items.Add(toolTipTitleItem6);
            superToolTip9.Items.Add(toolTipItem9);
            this.CheckEdit_rpps_resend_msg.SuperTip = superToolTip9;
            this.CheckEdit_rpps_resend_msg.TabIndex = 15;
            // 
            // CalcEdit_payments_padd_greater
            // 
            this.CalcEdit_payments_padd_greater.Location = new System.Drawing.Point(144, 102);
            this.CalcEdit_payments_padd_greater.MenuManager = this.barManager1;
            this.CalcEdit_payments_padd_greater.Name = "CalcEdit_payments_padd_greater";
            this.CalcEdit_payments_padd_greater.Properties.Appearance.Options.UseTextOptions = true;
            this.CalcEdit_payments_padd_greater.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.CalcEdit_payments_padd_greater.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.CalcEdit_payments_padd_greater.Properties.DisplayFormat.FormatString = "c";
            this.CalcEdit_payments_padd_greater.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.CalcEdit_payments_padd_greater.Properties.EditFormat.FormatString = "c";
            this.CalcEdit_payments_padd_greater.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.CalcEdit_payments_padd_greater.Properties.Precision = 2;
            this.CalcEdit_payments_padd_greater.Size = new System.Drawing.Size(175, 20);
            this.CalcEdit_payments_padd_greater.StyleController = this.LayoutControl1;
            toolTipTitleItem7.Text = "PRORATING AMOUNTS";
            toolTipItem10.LeftIndent = 6;
            toolTipItem10.Text = resources.GetString("toolTipItem10.Text");
            toolTipTitleItem8.LeftIndent = 6;
            toolTipTitleItem8.Text = "See the \"LESS\" figure above";
            superToolTip10.Items.Add(toolTipTitleItem7);
            superToolTip10.Items.Add(toolTipItem10);
            superToolTip10.Items.Add(toolTipTitleItem8);
            this.CalcEdit_payments_padd_greater.SuperTip = superToolTip10;
            this.CalcEdit_payments_padd_greater.TabIndex = 14;
            // 
            // CalcEdit_payments_padd_less
            // 
            this.CalcEdit_payments_padd_less.Location = new System.Drawing.Point(144, 78);
            this.CalcEdit_payments_padd_less.MenuManager = this.barManager1;
            this.CalcEdit_payments_padd_less.Name = "CalcEdit_payments_padd_less";
            this.CalcEdit_payments_padd_less.Properties.Appearance.Options.UseTextOptions = true;
            this.CalcEdit_payments_padd_less.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.CalcEdit_payments_padd_less.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.CalcEdit_payments_padd_less.Properties.DisplayFormat.FormatString = "c";
            this.CalcEdit_payments_padd_less.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.CalcEdit_payments_padd_less.Properties.EditFormat.FormatString = "c";
            this.CalcEdit_payments_padd_less.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.CalcEdit_payments_padd_less.Properties.Precision = 2;
            this.CalcEdit_payments_padd_less.Size = new System.Drawing.Size(175, 20);
            this.CalcEdit_payments_padd_less.StyleController = this.LayoutControl1;
            toolTipTitleItem9.Text = "PRORATING AMOUNTS";
            toolTipItem11.LeftIndent = 6;
            toolTipItem11.Text = resources.GetString("toolTipItem11.Text");
            toolTipTitleItem10.LeftIndent = 6;
            toolTipTitleItem10.Text = "See the \"MORE\" figure below";
            superToolTip11.Items.Add(toolTipTitleItem9);
            superToolTip11.Items.Add(toolTipItem11);
            superToolTip11.Items.Add(toolTipTitleItem10);
            this.CalcEdit_payments_padd_less.SuperTip = superToolTip11;
            this.CalcEdit_payments_padd_less.TabIndex = 13;
            // 
            // AddressRecordControl_Alternate
            // 
            this.AddressRecordControl_Alternate.Location = new System.Drawing.Point(132, 166);
            this.AddressRecordControl_Alternate.Name = "AddressRecordControl_Alternate";
            this.AddressRecordControl_Alternate.Size = new System.Drawing.Size(464, 72);
            toolTipTitleItem11.Text = "Alternate Address";
            toolTipItem12.LeftIndent = 6;
            toolTipItem12.Text = "This is the secondary address to your organization. If you have a Post Office Box" +
    ", it should be listed here rather than in the primary address field.";
            superToolTip12.Items.Add(toolTipTitleItem11);
            superToolTip12.Items.Add(toolTipItem12);
            this.ToolTipController1.SetSuperTip(this.AddressRecordControl_Alternate, superToolTip12);
            this.AddressRecordControl_Alternate.TabIndex = 12;
            // 
            // CreditorID_deduct_creditor
            // 
            this.CreditorID_deduct_creditor.AllowDrop = true;
            this.CreditorID_deduct_creditor.EditValue = null;
            this.CreditorID_deduct_creditor.Location = new System.Drawing.Point(132, 118);
            this.CreditorID_deduct_creditor.MenuManager = this.barManager1;
            this.CreditorID_deduct_creditor.Name = "CreditorID_deduct_creditor";
            this.CreditorID_deduct_creditor.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph, "", -1, true, true, false, DevExpress.XtraEditors.ImageLocation.MiddleCenter, ((System.Drawing.Image)(resources.GetObject("CreditorID_deduct_creditor.Properties.Buttons"))), new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject1, "Click here to search for a creditor ID", "search", null, true)});
            this.CreditorID_deduct_creditor.Properties.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.CreditorID_deduct_creditor.Properties.Mask.AutoComplete = DevExpress.XtraEditors.Mask.AutoCompleteType.Strong;
            this.CreditorID_deduct_creditor.Properties.Mask.BeepOnError = true;
            this.CreditorID_deduct_creditor.Properties.Mask.EditMask = "[A-Z]{1,2}[0-9]{4,}";
            this.CreditorID_deduct_creditor.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.RegEx;
            this.CreditorID_deduct_creditor.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.CreditorID_deduct_creditor.Properties.MaxLength = 10;
            this.CreditorID_deduct_creditor.Size = new System.Drawing.Size(162, 20);
            this.CreditorID_deduct_creditor.StyleController = this.LayoutControl1;
            this.CreditorID_deduct_creditor.TabIndex = 7;
            this.CreditorID_deduct_creditor.ToolTip = "This is the deduction creditor account. It is normaly Z9999. All deduct amounts f" +
    "or a disbursement are autoamtically paid to this creditor.";
            // 
            // CreditorID_counseling_creditor
            // 
            this.CreditorID_counseling_creditor.AllowDrop = true;
            this.CreditorID_counseling_creditor.EditValue = null;
            this.CreditorID_counseling_creditor.Location = new System.Drawing.Point(132, 94);
            this.CreditorID_counseling_creditor.MenuManager = this.barManager1;
            this.CreditorID_counseling_creditor.Name = "CreditorID_counseling_creditor";
            this.CreditorID_counseling_creditor.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph, "", -1, true, true, false, DevExpress.XtraEditors.ImageLocation.MiddleCenter, ((System.Drawing.Image)(resources.GetObject("CreditorID_counseling_creditor.Properties.Buttons"))), new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject2, "Click here to search for a creditor ID", "search", null, true)});
            this.CreditorID_counseling_creditor.Properties.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.CreditorID_counseling_creditor.Properties.Mask.AutoComplete = DevExpress.XtraEditors.Mask.AutoCompleteType.Strong;
            this.CreditorID_counseling_creditor.Properties.Mask.BeepOnError = true;
            this.CreditorID_counseling_creditor.Properties.Mask.EditMask = "[A-Z]{1,2}[0-9]{4,}";
            this.CreditorID_counseling_creditor.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.RegEx;
            this.CreditorID_counseling_creditor.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.CreditorID_counseling_creditor.Properties.MaxLength = 10;
            this.CreditorID_counseling_creditor.Size = new System.Drawing.Size(162, 20);
            this.CreditorID_counseling_creditor.StyleController = this.LayoutControl1;
            this.CreditorID_counseling_creditor.TabIndex = 5;
            this.CreditorID_counseling_creditor.ToolTip = "If you charge a record for a counseling session and pay that record to a creditor then " +
    "put the creditor ID here";
            // 
            // PercentEdit_threshold
            // 
            this.PercentEdit_threshold.Location = new System.Drawing.Point(144, 102);
            this.PercentEdit_threshold.Name = "PercentEdit_threshold";
            this.PercentEdit_threshold.Properties.Allow100Percent = false;
            this.PercentEdit_threshold.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.PercentEdit_threshold.Properties.DisplayFormat.FormatString = "{0:p3}";
            this.PercentEdit_threshold.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.PercentEdit_threshold.Properties.EditFormat.FormatString = "{0:f3}";
            this.PercentEdit_threshold.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.PercentEdit_threshold.Properties.Precision = 3;
            this.PercentEdit_threshold.Size = new System.Drawing.Size(152, 20);
            this.PercentEdit_threshold.StyleController = this.LayoutControl1;
            this.PercentEdit_threshold.TabIndex = 3;
            this.PercentEdit_threshold.ToolTip = "This is the default threshold for all debts. This defines the minimum amount that" +
    " a debt is to be paid based on a percentage of the balance.";
            // 
            // TextEdit_name
            // 
            this.TextEdit_name.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.TextEdit_name.Location = new System.Drawing.Point(132, 46);
            this.TextEdit_name.Name = "TextEdit_name";
            this.TextEdit_name.Properties.MaxLength = 50;
            this.TextEdit_name.Size = new System.Drawing.Size(464, 20);
            this.TextEdit_name.StyleController = this.LayoutControl1;
            this.TextEdit_name.TabIndex = 1;
            this.TextEdit_name.ToolTip = "This is the name of your organization.";
            // 
            // CalcEdit_min_amount
            // 
            this.CalcEdit_min_amount.Location = new System.Drawing.Point(144, 78);
            this.CalcEdit_min_amount.Name = "CalcEdit_min_amount";
            this.CalcEdit_min_amount.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.CalcEdit_min_amount.Properties.DisplayFormat.FormatString = "{0:c}";
            this.CalcEdit_min_amount.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.CalcEdit_min_amount.Properties.EditFormat.FormatString = "{0:c}";
            this.CalcEdit_min_amount.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.CalcEdit_min_amount.Properties.Mask.BeepOnError = true;
            this.CalcEdit_min_amount.Properties.Mask.EditMask = "c";
            this.CalcEdit_min_amount.Properties.Precision = 2;
            this.CalcEdit_min_amount.Size = new System.Drawing.Size(152, 20);
            this.CalcEdit_min_amount.StyleController = this.LayoutControl1;
            this.CalcEdit_min_amount.TabIndex = 1;
            this.CalcEdit_min_amount.ToolTip = "This is the minimum amount that you would normally configure a client\'s debt to b" +
    "e paid. It may be something like $10.00.";
            // 
            // CreditorID_paf_creditor
            // 
            this.CreditorID_paf_creditor.AllowDrop = true;
            this.CreditorID_paf_creditor.EditValue = null;
            this.CreditorID_paf_creditor.Location = new System.Drawing.Point(132, 70);
            this.CreditorID_paf_creditor.MenuManager = this.barManager1;
            this.CreditorID_paf_creditor.Name = "CreditorID_paf_creditor";
            this.CreditorID_paf_creditor.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph, "", -1, true, true, false, DevExpress.XtraEditors.ImageLocation.MiddleCenter, ((System.Drawing.Image)(resources.GetObject("CreditorID_paf_creditor.Properties.Buttons"))), new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject3, "Click here to search for a creditor ID", "search", null, true)});
            this.CreditorID_paf_creditor.Properties.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.CreditorID_paf_creditor.Properties.Mask.AutoComplete = DevExpress.XtraEditors.Mask.AutoCompleteType.Strong;
            this.CreditorID_paf_creditor.Properties.Mask.BeepOnError = true;
            this.CreditorID_paf_creditor.Properties.Mask.EditMask = "[A-Z]{1,2}[0-9]{4,}";
            this.CreditorID_paf_creditor.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.RegEx;
            this.CreditorID_paf_creditor.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.CreditorID_paf_creditor.Properties.MaxLength = 10;
            this.CreditorID_paf_creditor.Size = new System.Drawing.Size(162, 20);
            this.CreditorID_paf_creditor.StyleController = this.LayoutControl1;
            this.CreditorID_paf_creditor.TabIndex = 3;
            this.CreditorID_paf_creditor.ToolTip = "If you have a creditor which is paid a monthly re-occuring record then put the credi" +
    "tor ID here";
            // 
            // CreditorID_setup_creditor
            // 
            this.CreditorID_setup_creditor.AllowDrop = true;
            this.CreditorID_setup_creditor.EditValue = null;
            this.CreditorID_setup_creditor.Location = new System.Drawing.Point(132, 46);
            this.CreditorID_setup_creditor.MenuManager = this.barManager1;
            this.CreditorID_setup_creditor.Name = "CreditorID_setup_creditor";
            this.CreditorID_setup_creditor.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph, "", -1, true, true, false, DevExpress.XtraEditors.ImageLocation.MiddleCenter, ((System.Drawing.Image)(resources.GetObject("CreditorID_setup_creditor.Properties.Buttons"))), new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject4, "Click here to search for a creditor ID", "search", null, true)});
            this.CreditorID_setup_creditor.Properties.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.CreditorID_setup_creditor.Properties.Mask.AutoComplete = DevExpress.XtraEditors.Mask.AutoCompleteType.Strong;
            this.CreditorID_setup_creditor.Properties.Mask.BeepOnError = true;
            this.CreditorID_setup_creditor.Properties.Mask.EditMask = "[A-Z]{1,2}[0-9]{4,}";
            this.CreditorID_setup_creditor.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.RegEx;
            this.CreditorID_setup_creditor.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.CreditorID_setup_creditor.Properties.MaxLength = 10;
            this.CreditorID_setup_creditor.Size = new System.Drawing.Size(162, 20);
            this.CreditorID_setup_creditor.StyleController = this.LayoutControl1;
            this.CreditorID_setup_creditor.TabIndex = 1;
            this.CreditorID_setup_creditor.ToolTip = "If you have a creditor for charing an account setup (one time record) then put the c" +
    "reditor here.";
            // 
            // AddressRecordControl1
            // 
            this.AddressRecordControl1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.AddressRecordControl1.Location = new System.Drawing.Point(132, 80);
            this.AddressRecordControl1.Name = "AddressRecordControl1";
            this.AddressRecordControl1.Size = new System.Drawing.Size(464, 72);
            toolTipTitleItem12.Text = "PRIMARY ADDRESS";
            toolTipItem13.LeftIndent = 6;
            toolTipItem13.Text = "This is the street address of your organization.";
            superToolTip13.Items.Add(toolTipTitleItem12);
            superToolTip13.Items.Add(toolTipItem13);
            this.ToolTipController1.SetSuperTip(this.AddressRecordControl1, superToolTip13);
            this.AddressRecordControl1.TabIndex = 3;
            // 
            // LayoutControlGroup1
            // 
            this.LayoutControlGroup1.CustomizationFormText = "LayoutControlGroup1";
            this.LayoutControlGroup1.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.True;
            this.LayoutControlGroup1.GroupBordersVisible = false;
            this.LayoutControlGroup1.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.TabbedControlGroup1});
            this.LayoutControlGroup1.Location = new System.Drawing.Point(0, 0);
            this.LayoutControlGroup1.Name = "LayoutControlGroup1";
            this.LayoutControlGroup1.Size = new System.Drawing.Size(620, 366);
            this.LayoutControlGroup1.Text = "LayoutControlGroup1";
            this.LayoutControlGroup1.TextVisible = false;
            // 
            // TabbedControlGroup1
            // 
            this.TabbedControlGroup1.CustomizationFormText = "TabbedControlGroup1";
            this.TabbedControlGroup1.Location = new System.Drawing.Point(0, 0);
            this.TabbedControlGroup1.Name = "TabbedControlGroup1";
            this.TabbedControlGroup1.SelectedTabPage = this.LayoutControlGroup2;
            this.TabbedControlGroup1.SelectedTabPageIndex = 0;
            this.TabbedControlGroup1.Size = new System.Drawing.Size(600, 346);
            this.TabbedControlGroup1.TabPages.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.LayoutControlGroup2,
            this.LayoutControlGroup4,
            this.LayoutControlGroup3,
            this.LayoutControlGroup5});
            this.TabbedControlGroup1.Text = "TabbedControlGroup1";
            // 
            // LayoutControlGroup2
            // 
            this.LayoutControlGroup2.CustomizationFormText = "General";
            this.LayoutControlGroup2.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.LayoutControlItem2,
            this.LayoutControlItem4,
            this.LayoutControlItem5,
            this.LayoutControlItem6,
            this.LayoutControlItem14,
            this.EmptySpaceItem1,
            this.EmptySpaceItem2,
            this.EmptySpaceItem5,
            this.EmptySpaceItem6,
            this.LayoutControlItem22,
            this.LayoutControlItem3,
            this.LayoutControlItem1});
            this.LayoutControlGroup2.Location = new System.Drawing.Point(0, 0);
            this.LayoutControlGroup2.Name = "LayoutControlGroup2";
            this.LayoutControlGroup2.Size = new System.Drawing.Size(576, 300);
            this.LayoutControlGroup2.Text = "General";
            // 
            // LayoutControlItem2
            // 
            this.LayoutControlItem2.Control = this.TelephoneNumberRecordControl2;
            this.LayoutControlItem2.CustomizationFormText = "Toll-Free Telephone";
            this.LayoutControlItem2.Location = new System.Drawing.Point(248, 276);
            this.LayoutControlItem2.Name = "LayoutControlItem2";
            this.LayoutControlItem2.Size = new System.Drawing.Size(253, 24);
            this.LayoutControlItem2.Text = "Toll-Free Telephone";
            this.LayoutControlItem2.TextSize = new System.Drawing.Size(105, 13);
            // 
            // LayoutControlItem4
            // 
            this.LayoutControlItem4.Control = this.TextEdit_areacode;
            this.LayoutControlItem4.CustomizationFormText = "Default Area Code";
            this.LayoutControlItem4.Location = new System.Drawing.Point(0, 252);
            this.LayoutControlItem4.Name = "LayoutControlItem4";
            this.LayoutControlItem4.Size = new System.Drawing.Size(248, 24);
            this.LayoutControlItem4.Text = "Default Area Code";
            this.LayoutControlItem4.TextSize = new System.Drawing.Size(105, 13);
            // 
            // LayoutControlItem5
            // 
            this.LayoutControlItem5.AppearanceItemCaption.Options.UseTextOptions = true;
            this.LayoutControlItem5.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.LayoutControlItem5.AppearanceItemCaption.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Top;
            this.LayoutControlItem5.Control = this.AddressRecordControl1;
            this.LayoutControlItem5.CustomizationFormText = "Physical Address";
            this.LayoutControlItem5.Location = new System.Drawing.Point(0, 34);
            this.LayoutControlItem5.Name = "LayoutControlItem5";
            this.LayoutControlItem5.Size = new System.Drawing.Size(576, 76);
            this.LayoutControlItem5.Text = "Physical Address";
            this.LayoutControlItem5.TextSize = new System.Drawing.Size(105, 13);
            // 
            // LayoutControlItem6
            // 
            this.LayoutControlItem6.Control = this.TextEdit_name;
            this.LayoutControlItem6.CustomizationFormText = "Organization";
            this.LayoutControlItem6.Location = new System.Drawing.Point(0, 0);
            this.LayoutControlItem6.Name = "LayoutControlItem6";
            this.LayoutControlItem6.Size = new System.Drawing.Size(576, 24);
            this.LayoutControlItem6.Text = "Organization";
            this.LayoutControlItem6.TextSize = new System.Drawing.Size(105, 13);
            // 
            // LayoutControlItem14
            // 
            this.LayoutControlItem14.AppearanceItemCaption.Options.UseTextOptions = true;
            this.LayoutControlItem14.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.LayoutControlItem14.AppearanceItemCaption.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Top;
            this.LayoutControlItem14.Control = this.AddressRecordControl_Alternate;
            this.LayoutControlItem14.CustomizationFormText = "Mailing Address";
            this.LayoutControlItem14.Location = new System.Drawing.Point(0, 120);
            this.LayoutControlItem14.Name = "LayoutControlItem14";
            this.LayoutControlItem14.Size = new System.Drawing.Size(576, 76);
            this.LayoutControlItem14.Text = "Mailing Address";
            this.LayoutControlItem14.TextSize = new System.Drawing.Size(105, 13);
            // 
            // EmptySpaceItem1
            // 
            this.EmptySpaceItem1.AllowHotTrack = false;
            this.EmptySpaceItem1.CustomizationFormText = "EmptySpaceItem1";
            this.EmptySpaceItem1.Location = new System.Drawing.Point(501, 252);
            this.EmptySpaceItem1.Name = "EmptySpaceItem1";
            this.EmptySpaceItem1.Size = new System.Drawing.Size(75, 48);
            this.EmptySpaceItem1.Text = "EmptySpaceItem1";
            this.EmptySpaceItem1.TextSize = new System.Drawing.Size(0, 0);
            // 
            // EmptySpaceItem2
            // 
            this.EmptySpaceItem2.AllowHotTrack = false;
            this.EmptySpaceItem2.CustomizationFormText = "EmptySpaceItem2";
            this.EmptySpaceItem2.Location = new System.Drawing.Point(0, 196);
            this.EmptySpaceItem2.Name = "EmptySpaceItem2";
            this.EmptySpaceItem2.Size = new System.Drawing.Size(576, 32);
            this.EmptySpaceItem2.Text = "EmptySpaceItem2";
            this.EmptySpaceItem2.TextSize = new System.Drawing.Size(0, 0);
            // 
            // EmptySpaceItem5
            // 
            this.EmptySpaceItem5.AllowHotTrack = false;
            this.EmptySpaceItem5.CustomizationFormText = "EmptySpaceItem5";
            this.EmptySpaceItem5.Location = new System.Drawing.Point(0, 110);
            this.EmptySpaceItem5.Name = "EmptySpaceItem5";
            this.EmptySpaceItem5.Size = new System.Drawing.Size(576, 10);
            this.EmptySpaceItem5.Text = "EmptySpaceItem5";
            this.EmptySpaceItem5.TextSize = new System.Drawing.Size(0, 0);
            // 
            // EmptySpaceItem6
            // 
            this.EmptySpaceItem6.AllowHotTrack = false;
            this.EmptySpaceItem6.CustomizationFormText = "EmptySpaceItem6";
            this.EmptySpaceItem6.Location = new System.Drawing.Point(0, 24);
            this.EmptySpaceItem6.Name = "EmptySpaceItem6";
            this.EmptySpaceItem6.Size = new System.Drawing.Size(576, 10);
            this.EmptySpaceItem6.Text = "EmptySpaceItem6";
            this.EmptySpaceItem6.TextSize = new System.Drawing.Size(0, 0);
            // 
            // LayoutControlItem22
            // 
            this.LayoutControlItem22.Control = this.TextEdit_web_site;
            this.LayoutControlItem22.CustomizationFormText = "Website Address";
            this.LayoutControlItem22.Location = new System.Drawing.Point(0, 228);
            this.LayoutControlItem22.Name = "LayoutControlItem22";
            this.LayoutControlItem22.Size = new System.Drawing.Size(576, 24);
            this.LayoutControlItem22.Text = "Website Address";
            this.LayoutControlItem22.TextSize = new System.Drawing.Size(105, 13);
            // 
            // LayoutControlItem3
            // 
            this.LayoutControlItem3.Control = this.TelephoneNumberRecordControl1;
            this.LayoutControlItem3.CustomizationFormText = "Telephone Number";
            this.LayoutControlItem3.Location = new System.Drawing.Point(248, 252);
            this.LayoutControlItem3.Name = "LayoutControlItem3";
            this.LayoutControlItem3.Size = new System.Drawing.Size(253, 24);
            this.LayoutControlItem3.Text = "Telephone Number";
            this.LayoutControlItem3.TextSize = new System.Drawing.Size(105, 13);
            // 
            // LayoutControlItem1
            // 
            this.LayoutControlItem1.Control = this.TelephoneNumberRecordControl3;
            this.LayoutControlItem1.CustomizationFormText = "FAX Number";
            this.LayoutControlItem1.Location = new System.Drawing.Point(0, 276);
            this.LayoutControlItem1.Name = "LayoutControlItem1";
            this.LayoutControlItem1.Size = new System.Drawing.Size(248, 24);
            this.LayoutControlItem1.Text = "FAX Number";
            this.LayoutControlItem1.TextSize = new System.Drawing.Size(105, 13);
            // 
            // LayoutControlGroup4
            // 
            this.LayoutControlGroup4.CustomizationFormText = "Fees";
            this.LayoutControlGroup4.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.LayoutControlItem11,
            this.LayoutControlGroup12,
            this.LayoutControlGroup13});
            this.LayoutControlGroup4.Location = new System.Drawing.Point(0, 0);
            this.LayoutControlGroup4.Name = "LayoutControlGroup4";
            this.LayoutControlGroup4.Size = new System.Drawing.Size(576, 300);
            this.LayoutControlGroup4.Text = "Fees";
            // 
            // LayoutControlItem11
            // 
            this.LayoutControlItem11.Control = this.ConfigFees1;
            this.LayoutControlItem11.CustomizationFormText = "LayoutControlItem11";
            this.LayoutControlItem11.Location = new System.Drawing.Point(0, 116);
            this.LayoutControlItem11.Name = "LayoutControlItem11";
            this.LayoutControlItem11.Size = new System.Drawing.Size(576, 184);
            this.LayoutControlItem11.Text = "LayoutControlItem11";
            this.LayoutControlItem11.TextSize = new System.Drawing.Size(0, 0);
            this.LayoutControlItem11.TextToControlDistance = 0;
            this.LayoutControlItem11.TextVisible = false;
            // 
            // LayoutControlGroup12
            // 
            this.LayoutControlGroup12.CustomizationFormText = "DMP Debts";
            this.LayoutControlGroup12.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.LayoutControlItem12,
            this.LayoutControlItem13});
            this.LayoutControlGroup12.Location = new System.Drawing.Point(0, 0);
            this.LayoutControlGroup12.Name = "LayoutControlGroup12";
            this.LayoutControlGroup12.Size = new System.Drawing.Size(288, 116);
            this.LayoutControlGroup12.Text = "DMP Debts";
            // 
            // LayoutControlItem12
            // 
            this.LayoutControlItem12.Control = this.CalcEdit_min_amount;
            this.LayoutControlItem12.CustomizationFormText = "Minimum Payment";
            this.LayoutControlItem12.Location = new System.Drawing.Point(0, 0);
            this.LayoutControlItem12.Name = "LayoutControlItem12";
            this.LayoutControlItem12.Size = new System.Drawing.Size(264, 24);
            this.LayoutControlItem12.Text = "Minimum Payment";
            this.LayoutControlItem12.TextSize = new System.Drawing.Size(105, 13);
            // 
            // LayoutControlItem13
            // 
            this.LayoutControlItem13.Control = this.PercentEdit_threshold;
            this.LayoutControlItem13.CustomizationFormText = "Default Threshold";
            this.LayoutControlItem13.Location = new System.Drawing.Point(0, 24);
            this.LayoutControlItem13.Name = "LayoutControlItem13";
            this.LayoutControlItem13.Size = new System.Drawing.Size(264, 48);
            this.LayoutControlItem13.Text = "Default Threshold";
            this.LayoutControlItem13.TextSize = new System.Drawing.Size(105, 13);
            // 
            // LayoutControlGroup13
            // 
            this.LayoutControlGroup13.CustomizationFormText = "Comparison";
            this.LayoutControlGroup13.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.LayoutControlItem25,
            this.LayoutControlItem23,
            this.LayoutControlItem24});
            this.LayoutControlGroup13.Location = new System.Drawing.Point(288, 0);
            this.LayoutControlGroup13.Name = "LayoutControlGroup13";
            this.LayoutControlGroup13.Size = new System.Drawing.Size(288, 116);
            this.LayoutControlGroup13.Text = "Comparison";
            // 
            // LayoutControlItem25
            // 
            this.LayoutControlItem25.Control = this.PercentEdit_ComparisonRate;
            this.LayoutControlItem25.CustomizationFormText = "Interest Rate";
            this.LayoutControlItem25.Location = new System.Drawing.Point(0, 0);
            this.LayoutControlItem25.Name = "LayoutControlItem25";
            this.LayoutControlItem25.Size = new System.Drawing.Size(264, 24);
            this.LayoutControlItem25.Text = "Interest Rate";
            this.LayoutControlItem25.TextSize = new System.Drawing.Size(105, 13);
            // 
            // LayoutControlItem23
            // 
            this.LayoutControlItem23.Control = this.CalcEdit_ComparisonAmount;
            this.LayoutControlItem23.CustomizationFormText = "Payment Amount";
            this.LayoutControlItem23.Location = new System.Drawing.Point(0, 24);
            this.LayoutControlItem23.Name = "LayoutControlItem23";
            this.LayoutControlItem23.Size = new System.Drawing.Size(264, 24);
            this.LayoutControlItem23.Text = "Payment Amount";
            this.LayoutControlItem23.TextSize = new System.Drawing.Size(105, 13);
            // 
            // LayoutControlItem24
            // 
            this.LayoutControlItem24.Control = this.LookUpEdit_ComparisonPercent;
            this.LayoutControlItem24.CustomizationFormText = "Payment Percent";
            this.LayoutControlItem24.Location = new System.Drawing.Point(0, 48);
            this.LayoutControlItem24.Name = "LayoutControlItem24";
            this.LayoutControlItem24.Size = new System.Drawing.Size(264, 24);
            this.LayoutControlItem24.Text = "Payment Percent";
            this.LayoutControlItem24.TextSize = new System.Drawing.Size(105, 13);
            // 
            // LayoutControlGroup3
            // 
            this.LayoutControlGroup3.CustomizationFormText = "LayoutControlGroup3";
            this.LayoutControlGroup3.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.LayoutControlItem7,
            this.LayoutControlItem8,
            this.LayoutControlItem9,
            this.LayoutControlItem10,
            this.EmptySpaceItem3});
            this.LayoutControlGroup3.Location = new System.Drawing.Point(0, 0);
            this.LayoutControlGroup3.Name = "LayoutControlGroup3";
            this.LayoutControlGroup3.Size = new System.Drawing.Size(576, 300);
            this.LayoutControlGroup3.Text = "Creditors";
            // 
            // LayoutControlItem7
            // 
            this.LayoutControlItem7.Control = this.CreditorID_setup_creditor;
            this.LayoutControlItem7.CustomizationFormText = "Setup Creditor";
            this.LayoutControlItem7.Location = new System.Drawing.Point(0, 0);
            this.LayoutControlItem7.Name = "LayoutControlItem7";
            this.LayoutControlItem7.Size = new System.Drawing.Size(274, 24);
            this.LayoutControlItem7.Text = "Setup Creditor";
            this.LayoutControlItem7.TextSize = new System.Drawing.Size(105, 13);
            // 
            // LayoutControlItem8
            // 
            this.LayoutControlItem8.Control = this.CreditorID_paf_creditor;
            this.LayoutControlItem8.CustomizationFormText = "Monthly Fee Creditor";
            this.LayoutControlItem8.Location = new System.Drawing.Point(0, 24);
            this.LayoutControlItem8.Name = "LayoutControlItem8";
            this.LayoutControlItem8.Size = new System.Drawing.Size(274, 24);
            this.LayoutControlItem8.Text = "Monthly Fee Creditor";
            this.LayoutControlItem8.TextSize = new System.Drawing.Size(105, 13);
            // 
            // LayoutControlItem9
            // 
            this.LayoutControlItem9.Control = this.CreditorID_counseling_creditor;
            this.LayoutControlItem9.CustomizationFormText = "Counseling Creditor";
            this.LayoutControlItem9.Location = new System.Drawing.Point(0, 48);
            this.LayoutControlItem9.Name = "LayoutControlItem9";
            this.LayoutControlItem9.Size = new System.Drawing.Size(274, 24);
            this.LayoutControlItem9.Text = "Counseling Creditor";
            this.LayoutControlItem9.TextSize = new System.Drawing.Size(105, 13);
            // 
            // LayoutControlItem10
            // 
            this.LayoutControlItem10.Control = this.CreditorID_deduct_creditor;
            this.LayoutControlItem10.CustomizationFormText = "Deduction Creditor";
            this.LayoutControlItem10.Location = new System.Drawing.Point(0, 72);
            this.LayoutControlItem10.Name = "LayoutControlItem10";
            this.LayoutControlItem10.Size = new System.Drawing.Size(274, 228);
            this.LayoutControlItem10.Text = "Deduction Creditor";
            this.LayoutControlItem10.TextSize = new System.Drawing.Size(105, 13);
            // 
            // EmptySpaceItem3
            // 
            this.EmptySpaceItem3.AllowHotTrack = false;
            this.EmptySpaceItem3.CustomizationFormText = "EmptySpaceItem3";
            this.EmptySpaceItem3.Location = new System.Drawing.Point(274, 0);
            this.EmptySpaceItem3.Name = "EmptySpaceItem3";
            this.EmptySpaceItem3.Size = new System.Drawing.Size(302, 300);
            this.EmptySpaceItem3.Text = "EmptySpaceItem3";
            this.EmptySpaceItem3.TextSize = new System.Drawing.Size(0, 0);
            // 
            // LayoutControlGroup5
            // 
            this.LayoutControlGroup5.CustomizationFormText = "Others";
            this.LayoutControlGroup5.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.LayoutControlGroup6,
            this.LayoutControlGroup7,
            this.LayoutControlGroup8,
            this.LayoutControlGroup9,
            this.LayoutControlGroup10,
            this.EmptySpaceItem4,
            this.LayoutControlGroup11});
            this.LayoutControlGroup5.Location = new System.Drawing.Point(0, 0);
            this.LayoutControlGroup5.Name = "LayoutControlGroup5";
            this.LayoutControlGroup5.Size = new System.Drawing.Size(576, 300);
            this.LayoutControlGroup5.Text = "Others";
            // 
            // LayoutControlGroup6
            // 
            this.LayoutControlGroup6.CustomizationFormText = "RPPS Rejects";
            this.LayoutControlGroup6.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.LayoutControlItem17});
            this.LayoutControlGroup6.Location = new System.Drawing.Point(0, 160);
            this.LayoutControlGroup6.Name = "LayoutControlGroup6";
            this.LayoutControlGroup6.Size = new System.Drawing.Size(311, 68);
            this.LayoutControlGroup6.Text = "RPPS Rejects";
            // 
            // LayoutControlItem17
            // 
            this.LayoutControlItem17.Control = this.CheckEdit_rpps_resend_msg;
            this.LayoutControlItem17.CustomizationFormText = "LayoutControlItem17";
            this.LayoutControlItem17.Location = new System.Drawing.Point(0, 0);
            this.LayoutControlItem17.Name = "LayoutControlItem17";
            this.LayoutControlItem17.Size = new System.Drawing.Size(287, 24);
            this.LayoutControlItem17.Text = "LayoutControlItem17";
            this.LayoutControlItem17.TextSize = new System.Drawing.Size(0, 0);
            this.LayoutControlItem17.TextToControlDistance = 0;
            this.LayoutControlItem17.TextVisible = false;
            // 
            // LayoutControlGroup7
            // 
            this.LayoutControlGroup7.CustomizationFormText = "Padding Factors";
            this.LayoutControlGroup7.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.LayoutControlItem16,
            this.LayoutControlItem15});
            this.LayoutControlGroup7.Location = new System.Drawing.Point(0, 0);
            this.LayoutControlGroup7.Name = "LayoutControlGroup7";
            this.LayoutControlGroup7.Size = new System.Drawing.Size(311, 92);
            this.LayoutControlGroup7.Text = "Padding Factors";
            // 
            // LayoutControlItem16
            // 
            this.LayoutControlItem16.Control = this.CalcEdit_payments_padd_greater;
            this.LayoutControlItem16.CustomizationFormText = "More";
            this.LayoutControlItem16.Location = new System.Drawing.Point(0, 24);
            this.LayoutControlItem16.Name = "LayoutControlItem16";
            this.LayoutControlItem16.Size = new System.Drawing.Size(287, 24);
            this.LayoutControlItem16.Text = "More";
            this.LayoutControlItem16.TextSize = new System.Drawing.Size(105, 13);
            // 
            // LayoutControlItem15
            // 
            this.LayoutControlItem15.Control = this.CalcEdit_payments_padd_less;
            this.LayoutControlItem15.CustomizationFormText = "Less";
            this.LayoutControlItem15.Location = new System.Drawing.Point(0, 0);
            this.LayoutControlItem15.Name = "LayoutControlItem15";
            this.LayoutControlItem15.Size = new System.Drawing.Size(287, 24);
            this.LayoutControlItem15.Text = "Less";
            this.LayoutControlItem15.TextSize = new System.Drawing.Size(105, 13);
            // 
            // LayoutControlGroup8
            // 
            this.LayoutControlGroup8.CustomizationFormText = "Proposals";
            this.LayoutControlGroup8.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.LayoutControlItem18});
            this.LayoutControlGroup8.Location = new System.Drawing.Point(311, 136);
            this.LayoutControlGroup8.Name = "LayoutControlGroup8";
            this.LayoutControlGroup8.Size = new System.Drawing.Size(265, 92);
            this.LayoutControlGroup8.Text = "Proposals";
            // 
            // LayoutControlItem18
            // 
            this.LayoutControlItem18.Control = this.SpinEdit_acceptance_days;
            this.LayoutControlItem18.CustomizationFormText = "Acceptance Days";
            this.LayoutControlItem18.Location = new System.Drawing.Point(0, 0);
            this.LayoutControlItem18.Name = "LayoutControlItem18";
            this.LayoutControlItem18.Size = new System.Drawing.Size(241, 48);
            this.LayoutControlItem18.Text = "Acceptance Days";
            this.LayoutControlItem18.TextSize = new System.Drawing.Size(105, 13);
            // 
            // LayoutControlGroup9
            // 
            this.LayoutControlGroup9.CustomizationFormText = "Appointments";
            this.LayoutControlGroup9.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.LayoutControlItem19});
            this.LayoutControlGroup9.Location = new System.Drawing.Point(311, 68);
            this.LayoutControlGroup9.Name = "LayoutControlGroup9";
            this.LayoutControlGroup9.Size = new System.Drawing.Size(265, 68);
            this.LayoutControlGroup9.Text = "Appointments";
            // 
            // LayoutControlItem19
            // 
            this.LayoutControlItem19.Control = this.SpinEdit_default_till_missed;
            this.LayoutControlItem19.CustomizationFormText = "Default Until Missed";
            this.LayoutControlItem19.Location = new System.Drawing.Point(0, 0);
            this.LayoutControlItem19.Name = "LayoutControlItem19";
            this.LayoutControlItem19.Size = new System.Drawing.Size(241, 24);
            this.LayoutControlItem19.Text = "Default Until Missed";
            this.LayoutControlItem19.TextSize = new System.Drawing.Size(105, 13);
            // 
            // LayoutControlGroup10
            // 
            this.LayoutControlGroup10.CustomizationFormText = "Balance Verification";
            this.LayoutControlGroup10.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.LayoutControlItem20});
            this.LayoutControlGroup10.Location = new System.Drawing.Point(311, 0);
            this.LayoutControlGroup10.Name = "LayoutControlGroup10";
            this.LayoutControlGroup10.Size = new System.Drawing.Size(265, 68);
            this.LayoutControlGroup10.Text = "Balance Verification";
            // 
            // LayoutControlItem20
            // 
            this.LayoutControlItem20.Control = this.CalcEdit_bal_verify_months;
            this.LayoutControlItem20.CustomizationFormText = "Months Before Payoff";
            this.LayoutControlItem20.Location = new System.Drawing.Point(0, 0);
            this.LayoutControlItem20.Name = "LayoutControlItem20";
            this.LayoutControlItem20.Size = new System.Drawing.Size(241, 24);
            this.LayoutControlItem20.Text = "Months Before Payoff";
            this.LayoutControlItem20.TextSize = new System.Drawing.Size(105, 13);
            // 
            // EmptySpaceItem4
            // 
            this.EmptySpaceItem4.AllowHotTrack = false;
            this.EmptySpaceItem4.CustomizationFormText = "EmptySpaceItem4";
            this.EmptySpaceItem4.Location = new System.Drawing.Point(0, 228);
            this.EmptySpaceItem4.Name = "EmptySpaceItem4";
            this.EmptySpaceItem4.Size = new System.Drawing.Size(576, 72);
            this.EmptySpaceItem4.Text = "EmptySpaceItem4";
            this.EmptySpaceItem4.TextSize = new System.Drawing.Size(0, 0);
            // 
            // LayoutControlGroup11
            // 
            this.LayoutControlGroup11.CustomizationFormText = "Invoicing";
            this.LayoutControlGroup11.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.LayoutControlItem21});
            this.LayoutControlGroup11.Location = new System.Drawing.Point(0, 92);
            this.LayoutControlGroup11.Name = "LayoutControlGroup11";
            this.LayoutControlGroup11.Size = new System.Drawing.Size(311, 68);
            this.LayoutControlGroup11.Text = "Invoicing";
            // 
            // LayoutControlItem21
            // 
            this.LayoutControlItem21.Control = this.SpinEdit_chks_per_invoice;
            this.LayoutControlItem21.CustomizationFormText = "Checks per invoice";
            this.LayoutControlItem21.Location = new System.Drawing.Point(0, 0);
            this.LayoutControlItem21.Name = "LayoutControlItem21";
            this.LayoutControlItem21.Size = new System.Drawing.Size(287, 24);
            this.LayoutControlItem21.Text = "Checks per invoice";
            this.LayoutControlItem21.TextSize = new System.Drawing.Size(105, 13);
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(620, 391);
            this.Controls.Add(this.LayoutControl1);
            this.Controls.Add(this.barDockControlLeft);
            this.Controls.Add(this.barDockControlRight);
            this.Controls.Add(this.barDockControlBottom);
            this.Controls.Add(this.barDockControlTop);
            this.Name = "MainForm";
            this.Text = "Configuration Items";
            ((System.ComponentModel.ISupportInitialize)(this.DxErrorProvider1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.PopupMenu1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TelephoneNumberRecordControl3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TelephoneNumberRecordControl2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TelephoneNumberRecordControl1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TextEdit_areacode.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LayoutControl1)).EndInit();
            this.LayoutControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.PercentEdit_ComparisonRate.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LookUpEdit_ComparisonPercent.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.CalcEdit_ComparisonAmount.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.SpinEdit_chks_per_invoice.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.CalcEdit_bal_verify_months.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.SpinEdit_default_till_missed.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.SpinEdit_acceptance_days.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TextEdit_web_site.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.CheckEdit_rpps_resend_msg.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.CalcEdit_payments_padd_greater.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.CalcEdit_payments_padd_less.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.AddressRecordControl_Alternate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.CreditorID_deduct_creditor.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.CreditorID_counseling_creditor.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.PercentEdit_threshold.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TextEdit_name.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.CalcEdit_min_amount.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.CreditorID_paf_creditor.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.CreditorID_setup_creditor.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.AddressRecordControl1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LayoutControlGroup1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TabbedControlGroup1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LayoutControlGroup2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LayoutControlItem2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LayoutControlItem4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LayoutControlItem5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LayoutControlItem6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LayoutControlItem14)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.EmptySpaceItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.EmptySpaceItem2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.EmptySpaceItem5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.EmptySpaceItem6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LayoutControlItem22)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LayoutControlItem3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LayoutControlItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LayoutControlGroup4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LayoutControlItem11)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LayoutControlGroup12)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LayoutControlItem12)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LayoutControlItem13)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LayoutControlGroup13)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LayoutControlItem25)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LayoutControlItem23)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LayoutControlItem24)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LayoutControlGroup3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LayoutControlItem7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LayoutControlItem8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LayoutControlItem9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LayoutControlItem10)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.EmptySpaceItem3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LayoutControlGroup5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LayoutControlGroup6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LayoutControlItem17)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LayoutControlGroup7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LayoutControlItem16)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LayoutControlItem15)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LayoutControlGroup8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LayoutControlItem18)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LayoutControlGroup9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LayoutControlItem19)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LayoutControlGroup10)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LayoutControlItem20)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.EmptySpaceItem4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LayoutControlGroup11)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LayoutControlItem21)).EndInit();
            this.ResumeLayout(false);

		}

		internal DevExpress.XtraBars.BarManager barManager1;
		private DevExpress.XtraBars.Bar Bar2;
		private DevExpress.XtraBars.BarSubItem BarSubItem1;
		private DevExpress.XtraBars.BarButtonItem BarButtonItem_File_Exit;
		private DevExpress.XtraBars.BarDockControl barDockControlTop;
		private DevExpress.XtraBars.BarDockControl barDockControlBottom;
		private DevExpress.XtraBars.BarDockControl barDockControlLeft;
		private DevExpress.XtraBars.BarDockControl barDockControlRight;
		private ConfigFees ConfigFees1;
		private global::DebtPlus.Data.Controls.PercentEdit PercentEdit_threshold;
		private DevExpress.XtraEditors.CalcEdit CalcEdit_min_amount;
		private DevExpress.XtraEditors.TextEdit TextEdit_areacode;
		private DevExpress.XtraEditors.TextEdit TextEdit_name;
		private global::DebtPlus.Data.Controls.AddressRecordControl AddressRecordControl1;
		private global::DebtPlus.Data.Controls.TelephoneNumberRecordControl TelephoneNumberRecordControl1;
		private global::DebtPlus.Data.Controls.TelephoneNumberRecordControl TelephoneNumberRecordControl2;
		private global::DebtPlus.Data.Controls.TelephoneNumberRecordControl TelephoneNumberRecordControl3;
		private CreditorID CreditorID_deduct_creditor;
		private CreditorID CreditorID_counseling_creditor;
		private CreditorID CreditorID_paf_creditor;
		private CreditorID CreditorID_setup_creditor;
		private DevExpress.XtraLayout.LayoutControl LayoutControl1;
		private DevExpress.XtraLayout.LayoutControlGroup LayoutControlGroup1;
		private DevExpress.XtraLayout.TabbedControlGroup TabbedControlGroup1;
		private DevExpress.XtraLayout.LayoutControlGroup LayoutControlGroup2;
		private DevExpress.XtraLayout.LayoutControlItem LayoutControlItem1;
		private DevExpress.XtraLayout.LayoutControlItem LayoutControlItem2;
		private DevExpress.XtraLayout.LayoutControlItem LayoutControlItem3;
		private DevExpress.XtraLayout.LayoutControlItem LayoutControlItem4;
		private DevExpress.XtraLayout.LayoutControlItem LayoutControlItem5;
		private DevExpress.XtraLayout.LayoutControlItem LayoutControlItem6;
		private DevExpress.XtraLayout.LayoutControlGroup LayoutControlGroup3;
		private DevExpress.XtraLayout.LayoutControlItem LayoutControlItem7;
		private DevExpress.XtraLayout.LayoutControlItem LayoutControlItem8;
		private DevExpress.XtraLayout.LayoutControlItem LayoutControlItem9;
		private DevExpress.XtraLayout.LayoutControlItem LayoutControlItem10;
		private DevExpress.XtraLayout.LayoutControlGroup LayoutControlGroup4;
		private DevExpress.XtraLayout.LayoutControlItem LayoutControlItem11;
		private DevExpress.XtraLayout.LayoutControlItem LayoutControlItem12;
		private DevExpress.XtraLayout.LayoutControlItem LayoutControlItem13;
		private global::DebtPlus.Data.Controls.AddressRecordControl AddressRecordControl_Alternate;
		private DevExpress.XtraLayout.LayoutControlItem LayoutControlItem14;
		private DevExpress.XtraLayout.EmptySpaceItem EmptySpaceItem3;
		private DevExpress.XtraEditors.CalcEdit CalcEdit_payments_padd_greater;
		private DevExpress.XtraEditors.CalcEdit CalcEdit_payments_padd_less;
		private DevExpress.XtraLayout.LayoutControlGroup LayoutControlGroup5;
		private DevExpress.XtraLayout.LayoutControlItem LayoutControlItem16;
		private DevExpress.XtraLayout.LayoutControlItem LayoutControlItem15;
		private DevExpress.XtraEditors.CheckEdit CheckEdit_rpps_resend_msg;
		private DevExpress.XtraLayout.LayoutControlGroup LayoutControlGroup6;
		private DevExpress.XtraLayout.LayoutControlItem LayoutControlItem17;
		private DevExpress.XtraLayout.LayoutControlGroup LayoutControlGroup7;
		private DevExpress.XtraLayout.EmptySpaceItem EmptySpaceItem4;
		private DevExpress.XtraEditors.SpinEdit SpinEdit_acceptance_days;
		private DevExpress.XtraLayout.LayoutControlItem LayoutControlItem18;
		private DevExpress.XtraEditors.CalcEdit CalcEdit_bal_verify_months;
		private DevExpress.XtraEditors.SpinEdit SpinEdit_default_till_missed;
		private DevExpress.XtraLayout.LayoutControlGroup LayoutControlGroup8;
		private DevExpress.XtraLayout.LayoutControlGroup LayoutControlGroup9;
		private DevExpress.XtraLayout.LayoutControlItem LayoutControlItem19;
		private DevExpress.XtraLayout.LayoutControlGroup LayoutControlGroup10;
		private DevExpress.XtraLayout.LayoutControlItem LayoutControlItem20;
		private DevExpress.XtraLayout.EmptySpaceItem EmptySpaceItem1;
		private DevExpress.XtraLayout.EmptySpaceItem EmptySpaceItem2;
		private DevExpress.XtraLayout.EmptySpaceItem EmptySpaceItem5;
		private DevExpress.XtraLayout.EmptySpaceItem EmptySpaceItem6;
		private DevExpress.XtraEditors.SpinEdit SpinEdit_chks_per_invoice;
		private DevExpress.XtraLayout.LayoutControlGroup LayoutControlGroup11;
		private DevExpress.XtraLayout.LayoutControlItem LayoutControlItem21;
		private DevExpress.XtraEditors.TextEdit TextEdit_web_site;
		private DevExpress.XtraLayout.LayoutControlItem LayoutControlItem22;
		private DevExpress.XtraBars.PopupMenu PopupMenu1;
		private global::DebtPlus.Data.Controls.PercentEdit PercentEdit_ComparisonRate;
		private DevExpress.XtraEditors.LookUpEdit LookUpEdit_ComparisonPercent;
		private DevExpress.XtraEditors.CalcEdit CalcEdit_ComparisonAmount;
		private DevExpress.XtraLayout.LayoutControlGroup LayoutControlGroup12;
		private DevExpress.XtraLayout.LayoutControlGroup LayoutControlGroup13;
		private DevExpress.XtraLayout.LayoutControlItem LayoutControlItem25;
		private DevExpress.XtraLayout.LayoutControlItem LayoutControlItem23;
		private DevExpress.XtraLayout.LayoutControlItem LayoutControlItem24;
	}
}
