#region "Copyright 2000-2012 DebtPlus, L.L.C."
//{*******************************************************************}
//{                                                                   }
//{       DebtPlus Debt Management System                             }
//{                                                                   }
//{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
//{       ALL RIGHTS RESERVED                                         }
//{                                                                   }
//{   The entire contents of this file is protected by U.S. and       }
//{   International Copyright Laws. Unauthorized reproduction,        }
//{   reverse-engineering, and distribution of all or any portion of  }
//{   the code contained in this file is strictly prohibited and may  }
//{   result in severe civil and criminal penalties and will be       }
//{   prosecuted to the maximum extent possible under the law.        }
//{                                                                   }
//{   RESTRICTIONS                                                    }
//{                                                                   }
//{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
//{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
//{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
//{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
//{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
//{                                                                   }
//{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
//{   ADDITIONAL RESTRICTIONS.                                        }
//{                                                                   }
//{*******************************************************************}

#endregion "Copyright 2000-2012 DebtPlus, L.L.C."

using System;
using DebtPlus.LINQ;

namespace DebtPlus.UI.TableAdministration.CS.Config
{
    internal partial class ConfigFeeForm
    {
        private class feeType
        {
            internal Int32 Id
            { get; set; }

            internal string description
            { get; set; }

            internal feeType(Int32 Id, string description)
            {
                this.Id = Id;
                this.description = description;
            }
        }

        private config_fee record;
        private System.Collections.Generic.List<feeType> colFeeTypes;

        internal ConfigFeeForm()
            : base()
        {
            InitializeComponent();

            // Load the list of record types
            colFeeTypes = new System.Collections.Generic.List<feeType>();
            colFeeTypes.Add(new feeType(1, "fixed rate record"));
            colFeeTypes.Add(new feeType(2, "percentage of disbursed dollars"));
            colFeeTypes.Add(new feeType(3, "amount per disbursed creditor"));
        }

        internal ConfigFeeForm(config_fee fee)
            : this()
        {
            this.record = fee;
            RegisterHandlers();
        }

        private void RegisterHandlers()
        {
            Load += ConfigFeeForm_Load;
            simpleButton_OK.Click += simpleButton_OK_Click;
            LookUpEdit_fee_type.EditValueChanged += FormChanged;
            LookUpEdit_state.EditValueChanged += FormChanged;
            CalcEdit_fee_balance_inc.EditValueChanged += FormChanged;
            CalcEdit_fee_disb_max.EditValueChanged += FormChanged;
            CalcEdit_fee_base.EditValueChanged += FormChanged;
            CalcEdit_fee_monthly_max.EditValueChanged += FormChanged;
            CalcEdit_fee_per_creditor.EditValueChanged += FormChanged;
            CalcEdit_fee_sched_payment.EditValueChanged += FormChanged;
            PercentEdit_fee_percent.EditValueChanged += FormChanged;
            CalcEdit_fee_maximum.EditValueChanged += FormChanged;
            TextEdit_description.EditValueChanged += FormChanged;
            CheckEdit_fee_balance_max.EditValueChanged += FormChanged;
            CheckEdit_default.EditValueChanged += FormChanged;
            checkEdit_ActiveFlag.EditValueChanged += FormChanged;
            LookUpEdit_fee_type.EditValueChanged += FormChanged;
            LookUpEdit_state.EditValueChanged += FormChanged;
            checkEdit_preferred.EditValueChanged += FormChanged;

            LookUpEdit_state.EditValueChanging += DebtPlus.Data.Validation.LookUpEdit_ActiveTest;
        }

        private void UnRegisterHandlers()
        {
            Load -= ConfigFeeForm_Load;
            simpleButton_OK.Click -= simpleButton_OK_Click;
            LookUpEdit_fee_type.EditValueChanged -= FormChanged;
            LookUpEdit_state.EditValueChanged -= FormChanged;
            CalcEdit_fee_balance_inc.EditValueChanged -= FormChanged;
            CalcEdit_fee_disb_max.EditValueChanged -= FormChanged;
            CalcEdit_fee_base.EditValueChanged -= FormChanged;
            CalcEdit_fee_monthly_max.EditValueChanged -= FormChanged;
            CalcEdit_fee_per_creditor.EditValueChanged -= FormChanged;
            CalcEdit_fee_sched_payment.EditValueChanged -= FormChanged;
            PercentEdit_fee_percent.EditValueChanged -= FormChanged;
            CalcEdit_fee_maximum.EditValueChanged -= FormChanged;
            TextEdit_description.EditValueChanged -= FormChanged;
            CheckEdit_fee_balance_max.EditValueChanged -= FormChanged;
            CheckEdit_default.EditValueChanged -= FormChanged;
            checkEdit_ActiveFlag.EditValueChanged -= FormChanged;
            LookUpEdit_fee_type.EditValueChanged -= FormChanged;
            LookUpEdit_state.EditValueChanged -= FormChanged;
            checkEdit_preferred.EditValueChanged -= FormChanged;

            LookUpEdit_state.EditValueChanging -= DebtPlus.Data.Validation.LookUpEdit_ActiveTest;
        }

        /// <summary>
        /// Load and bind the record to the controls
        /// </summary>
        private void ConfigFeeForm_Load(object sender, EventArgs e)
        {
            UnRegisterHandlers();
            try
            {
                System.Collections.Generic.List<DebtPlus.LINQ.state> colStates = DebtPlus.LINQ.Cache.state.getList();

                // Load the datasets for the controls
                LookUpEdit_fee_type.Properties.DataSource = colFeeTypes;
                LookUpEdit_state.Properties.DataSource = colStates;

                // Load the current record into the controls
                CalcEdit_fee_balance_inc.EditValue = record.fee_balance_inc;
                CalcEdit_fee_disb_max.EditValue = record.fee_disb_max;
                CalcEdit_fee_base.EditValue = record.fee_base;
                CalcEdit_fee_monthly_max.EditValue = record.fee_monthly_max;
                CalcEdit_fee_per_creditor.EditValue = record.fee_per_creditor;
                CalcEdit_fee_sched_payment.EditValue = record.fee_sched_payment;
                PercentEdit_fee_percent.EditValue = record.fee_percent;
                CalcEdit_fee_maximum.EditValue = record.fee_maximum;
                TextEdit_description.EditValue = record.description;
                CheckEdit_fee_balance_max.EditValue = record.fee_balance_max;
                CheckEdit_default.EditValue = record.Default;
                LookUpEdit_fee_type.EditValue = record.fee_type;
                LookUpEdit_state.EditValue = record.state;
                checkEdit_ActiveFlag.Checked = record.ActiveFlag;
                checkEdit_preferred.Checked = record.preferred;
            }
            finally
            {
                RegisterHandlers();
                simpleButton_OK.Enabled = !HasErrors();
            }
        }

        /// <summary>
        /// Determine if the input form has a valid set for the record
        /// </summary>
        private bool HasErrors()
        {
            // A name is required
            if (string.IsNullOrWhiteSpace(TextEdit_description.Text))
            {
                return true;
            }

            // A type is required
            if (!DebtPlus.Utils.Nulls.v_Int32(LookUpEdit_fee_type.EditValue).HasValue)
            {
                return true;
            }

            // All's well with the world.
            return false;
        }

        private void FormChanged(object sender, EventArgs e)
        {
            simpleButton_OK.Enabled = !HasErrors();
        }

        private void simpleButton_OK_Click(System.Object sender, System.EventArgs e)
        {
            // Replace the values in the record when the OK button is pressed
            record.fee_balance_inc = DebtPlus.Utils.Nulls.v_Decimal(CalcEdit_fee_balance_inc.EditValue);
            record.fee_disb_max = DebtPlus.Utils.Nulls.v_Decimal(CalcEdit_fee_disb_max.EditValue);
            record.fee_base = DebtPlus.Utils.Nulls.v_Decimal(CalcEdit_fee_base.EditValue).GetValueOrDefault();
            record.fee_monthly_max = DebtPlus.Utils.Nulls.v_Decimal(CalcEdit_fee_monthly_max.EditValue);
            record.fee_per_creditor = DebtPlus.Utils.Nulls.v_Decimal(CalcEdit_fee_per_creditor.EditValue);
            record.fee_sched_payment = DebtPlus.Utils.Nulls.v_Decimal(CalcEdit_fee_sched_payment.EditValue);
            record.fee_percent = DebtPlus.Utils.Nulls.v_Double(PercentEdit_fee_percent.EditValue);
            record.fee_maximum = DebtPlus.Utils.Nulls.v_Decimal(CalcEdit_fee_maximum.EditValue);
            record.description = DebtPlus.Utils.Nulls.v_String(TextEdit_description.EditValue);
            record.fee_balance_max = CheckEdit_fee_balance_max.Checked;
            record.Default = CheckEdit_default.Checked;
            record.fee_type = DebtPlus.Utils.Nulls.v_Int32(LookUpEdit_fee_type.EditValue).GetValueOrDefault();
            record.state = DebtPlus.Utils.Nulls.v_Int32(LookUpEdit_state.EditValue);
            record.ActiveFlag = checkEdit_ActiveFlag.Checked;
            record.preferred = checkEdit_preferred.Checked;
        }
    }
}