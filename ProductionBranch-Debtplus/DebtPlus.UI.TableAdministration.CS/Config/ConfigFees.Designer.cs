
using System;
using System.Windows.Forms;
using System.Drawing;
using DebtPlus.LINQ;
using System.Linq;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Diagnostics;
namespace DebtPlus.UI.TableAdministration.CS.Config
{
	partial class ConfigFees : DevExpress.XtraEditors.XtraUserControl
	{

		//UserControl overrides dispose to clean up the component list.
		[System.Diagnostics.DebuggerNonUserCode]
		protected override void Dispose(bool disposing)
		{
			if (disposing && components != null) 
{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		//Required by the Windows Form Designer

		private System.ComponentModel.IContainer components = null;
		//NOTE: The following procedure is required by the Windows Form Designer
		//It can be modified using the Windows Form Designer.  
		//Do not modify it using the code editor.
		[System.Diagnostics.DebuggerStepThrough]
		private void InitializeComponent()
		{
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ConfigFees));
            this.gridControl1 = new DevExpress.XtraGrid.GridControl();
            this.gridView1 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gridColumn_config_fee = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn_Name = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn_default = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn3 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.barManager1 = new DevExpress.XtraBars.BarManager(this.components);
            this.barDockControlTop = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlBottom = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlLeft = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlRight = new DevExpress.XtraBars.BarDockControl();
            this.BarButtonItem_Add = new DevExpress.XtraBars.BarButtonItem();
            this.BarButtonItem_Edit = new DevExpress.XtraBars.BarButtonItem();
            this.BarButtonItem_Delete = new DevExpress.XtraBars.BarButtonItem();
            this.LabelControl1 = new DevExpress.XtraEditors.LabelControl();
            this.PopupMenu1 = new DevExpress.XtraBars.PopupMenu(this.components);
            this.gridColumn_preferred = new DevExpress.XtraGrid.Columns.GridColumn();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.PopupMenu1)).BeginInit();
            this.SuspendLayout();
            // 
            // gridControl1
            // 
            this.gridControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridControl1.Location = new System.Drawing.Point(0, 60);
            this.gridControl1.MainView = this.gridView1;
            this.gridControl1.MenuManager = this.barManager1;
            this.gridControl1.Name = "gridControl1";
            this.gridControl1.Size = new System.Drawing.Size(350, 174);
            this.gridControl1.TabIndex = 0;
            this.gridControl1.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView1});
            // 
            // gridView1
            // 
            this.gridView1.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gridColumn_config_fee,
            this.gridColumn_Name,
            this.gridColumn_default,
            this.gridColumn_preferred,
            this.gridColumn3});
            this.gridView1.GridControl = this.gridControl1;
            this.gridView1.Name = "gridView1";
            this.gridView1.OptionsBehavior.Editable = false;
            this.gridView1.OptionsView.ShowGroupPanel = false;
            this.gridView1.OptionsView.ShowIndicator = false;
            this.gridView1.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.gridColumn_Name, DevExpress.Data.ColumnSortOrder.Ascending)});
            // 
            // gridColumn_config_fee
            // 
            this.gridColumn_config_fee.AppearanceCell.Options.UseTextOptions = true;
            this.gridColumn_config_fee.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.gridColumn_config_fee.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColumn_config_fee.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.gridColumn_config_fee.Caption = "ID";
            this.gridColumn_config_fee.CustomizationCaption = "Primary Key";
            this.gridColumn_config_fee.DisplayFormat.FormatString = "f0";
            this.gridColumn_config_fee.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.gridColumn_config_fee.FieldName = "config_fee";
            this.gridColumn_config_fee.GroupFormat.FormatString = "f0";
            this.gridColumn_config_fee.GroupFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.gridColumn_config_fee.Name = "gridColumn_config_fee";
            this.gridColumn_config_fee.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.True;
            // 
            // gridColumn_Name
            // 
            this.gridColumn_Name.Caption = "Description";
            this.gridColumn_Name.CustomizationCaption = "Description";
            this.gridColumn_Name.FieldName = "description";
            this.gridColumn_Name.Name = "gridColumn_Name";
            this.gridColumn_Name.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.True;
            this.gridColumn_Name.SortMode = DevExpress.XtraGrid.ColumnSortMode.DisplayText;
            this.gridColumn_Name.Visible = true;
            this.gridColumn_Name.VisibleIndex = 2;
            this.gridColumn_Name.Width = 565;
            // 
            // gridColumn_default
            // 
            this.gridColumn_default.AppearanceCell.Options.UseTextOptions = true;
            this.gridColumn_default.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumn_default.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColumn_default.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumn_default.Caption = "Default";
            this.gridColumn_default.CustomizationCaption = "Default Indicator";
            this.gridColumn_default.FieldName = "Default";
            this.gridColumn_default.Name = "gridColumn_default";
            this.gridColumn_default.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.True;
            this.gridColumn_default.Visible = true;
            this.gridColumn_default.VisibleIndex = 0;
            this.gridColumn_default.Width = 120;
            // 
            // gridColumn3
            // 
            this.gridColumn3.AppearanceCell.Options.UseTextOptions = true;
            this.gridColumn3.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.gridColumn3.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColumn3.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.gridColumn3.Caption = "Date";
            this.gridColumn3.CustomizationCaption = "Date Created";
            this.gridColumn3.DisplayFormat.FormatString = "d";
            this.gridColumn3.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.gridColumn3.FieldName = "date_created";
            this.gridColumn3.GroupFormat.FormatString = "d";
            this.gridColumn3.GroupFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.gridColumn3.GroupInterval = DevExpress.XtraGrid.ColumnGroupInterval.Date;
            this.gridColumn3.Name = "gridColumn3";
            this.gridColumn3.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.True;
            this.gridColumn3.Visible = true;
            this.gridColumn3.VisibleIndex = 1;
            this.gridColumn3.Width = 166;
            // 
            // barManager1
            // 
            this.barManager1.DockControls.Add(this.barDockControlTop);
            this.barManager1.DockControls.Add(this.barDockControlBottom);
            this.barManager1.DockControls.Add(this.barDockControlLeft);
            this.barManager1.DockControls.Add(this.barDockControlRight);
            this.barManager1.Form = this;
            this.barManager1.Items.AddRange(new DevExpress.XtraBars.BarItem[] {
            this.BarButtonItem_Add,
            this.BarButtonItem_Edit,
            this.BarButtonItem_Delete});
            this.barManager1.MaxItemId = 3;
            // 
            // barDockControlTop
            // 
            this.barDockControlTop.CausesValidation = false;
            this.barDockControlTop.Dock = System.Windows.Forms.DockStyle.Top;
            this.barDockControlTop.Location = new System.Drawing.Point(0, 0);
            this.barDockControlTop.Size = new System.Drawing.Size(350, 0);
            // 
            // barDockControlBottom
            // 
            this.barDockControlBottom.CausesValidation = false;
            this.barDockControlBottom.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.barDockControlBottom.Location = new System.Drawing.Point(0, 234);
            this.barDockControlBottom.Size = new System.Drawing.Size(350, 0);
            // 
            // barDockControlLeft
            // 
            this.barDockControlLeft.CausesValidation = false;
            this.barDockControlLeft.Dock = System.Windows.Forms.DockStyle.Left;
            this.barDockControlLeft.Location = new System.Drawing.Point(0, 0);
            this.barDockControlLeft.Size = new System.Drawing.Size(0, 234);
            // 
            // barDockControlRight
            // 
            this.barDockControlRight.CausesValidation = false;
            this.barDockControlRight.Dock = System.Windows.Forms.DockStyle.Right;
            this.barDockControlRight.Location = new System.Drawing.Point(350, 0);
            this.barDockControlRight.Size = new System.Drawing.Size(0, 234);
            // 
            // BarButtonItem_Add
            // 
            this.BarButtonItem_Add.Caption = "&Add...";
            this.BarButtonItem_Add.Id = 0;
            this.BarButtonItem_Add.Name = "BarButtonItem_Add";
            // 
            // BarButtonItem_Edit
            // 
            this.BarButtonItem_Edit.Caption = "&Edit...";
            this.BarButtonItem_Edit.Id = 1;
            this.BarButtonItem_Edit.Name = "BarButtonItem_Edit";
            // 
            // BarButtonItem_Delete
            // 
            this.BarButtonItem_Delete.Caption = "&Delete";
            this.BarButtonItem_Delete.Enabled = false;
            this.BarButtonItem_Delete.Id = 2;
            this.BarButtonItem_Delete.Name = "BarButtonItem_Delete";
            // 
            // LabelControl1
            // 
            this.LabelControl1.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.LabelControl1.Appearance.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Top;
            this.LabelControl1.Appearance.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.LabelControl1.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.LabelControl1.Dock = System.Windows.Forms.DockStyle.Top;
            this.LabelControl1.Location = new System.Drawing.Point(0, 0);
            this.LabelControl1.Name = "LabelControl1";
            this.LabelControl1.Padding = new System.Windows.Forms.Padding(3);
            this.LabelControl1.Size = new System.Drawing.Size(350, 60);
            this.LabelControl1.TabIndex = 1;
            this.LabelControl1.Text = resources.GetString("LabelControl1.Text");
            this.LabelControl1.UseMnemonic = false;
            // 
            // PopupMenu1
            // 
            this.PopupMenu1.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.BarButtonItem_Add),
            new DevExpress.XtraBars.LinkPersistInfo(this.BarButtonItem_Edit),
            new DevExpress.XtraBars.LinkPersistInfo(this.BarButtonItem_Delete)});
            this.PopupMenu1.Manager = this.barManager1;
            this.PopupMenu1.Name = "PopupMenu1";
            // 
            // gridColumn_preferred
            // 
            this.gridColumn_preferred.Caption = "Preferrd";
            this.gridColumn_preferred.CustomizationCaption = "Preferred Fee";
            this.gridColumn_preferred.FieldName = "preferred";
            this.gridColumn_preferred.GroupInterval = DevExpress.XtraGrid.ColumnGroupInterval.Value;
            this.gridColumn_preferred.Name = "gridColumn_preferred";
            this.gridColumn_preferred.OptionsColumn.AllowEdit = false;
            this.gridColumn_preferred.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.True;
            this.gridColumn_preferred.SortMode = DevExpress.XtraGrid.ColumnSortMode.Value;
            // 
            // ConfigFees
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.gridControl1);
            this.Controls.Add(this.LabelControl1);
            this.Controls.Add(this.barDockControlLeft);
            this.Controls.Add(this.barDockControlRight);
            this.Controls.Add(this.barDockControlBottom);
            this.Controls.Add(this.barDockControlTop);
            this.Name = "ConfigFees";
            this.Size = new System.Drawing.Size(350, 234);
            ((System.ComponentModel.ISupportInitialize)(this.gridControl1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.PopupMenu1)).EndInit();
            this.ResumeLayout(false);

		}
		private DevExpress.XtraGrid.GridControl gridControl1;
		private DevExpress.XtraGrid.Views.Grid.GridView gridView1;
		internal DevExpress.XtraEditors.LabelControl LabelControl1;
		private DevExpress.XtraGrid.Columns.GridColumn gridColumn_config_fee;
		private DevExpress.XtraGrid.Columns.GridColumn gridColumn_default;
		private DevExpress.XtraGrid.Columns.GridColumn gridColumn3;
		internal DevExpress.XtraBars.BarManager barManager1;
		private DevExpress.XtraBars.BarDockControl barDockControlTop;
		private DevExpress.XtraBars.BarDockControl barDockControlBottom;
		private DevExpress.XtraBars.BarDockControl barDockControlLeft;
		private DevExpress.XtraBars.BarDockControl barDockControlRight;
		internal DevExpress.XtraBars.PopupMenu PopupMenu1;
		private DevExpress.XtraBars.BarButtonItem BarButtonItem_Add;
		private DevExpress.XtraBars.BarButtonItem BarButtonItem_Edit;

		private DevExpress.XtraBars.BarButtonItem BarButtonItem_Delete;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn_Name;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn_preferred;
	}
}
