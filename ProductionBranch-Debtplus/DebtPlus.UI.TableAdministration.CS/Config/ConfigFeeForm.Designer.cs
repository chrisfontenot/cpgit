namespace DebtPlus.UI.TableAdministration.CS.Config
{
	partial class ConfigFeeForm : DebtPlus.Data.Forms.DebtPlusForm
	{
		//Form overrides dispose to clean up the component list.
		[System.Diagnostics.DebuggerNonUserCode]
		protected override void Dispose(bool disposing)
        {
            try
            {
                if (disposing)
                {
                    if (components != null) components.Dispose();
                    UnRegisterHandlers();
                }
                components = null;
            }
            finally
            {
                base.Dispose(disposing);
            }
        }

		//Required by the Windows Form Designer
		private System.ComponentModel.IContainer components = null;

		//NOTE: The following procedure is required by the Windows Form Designer
		//It can be modified using the Windows Form Designer.  
		//Do not modify it using the code editor.
		[System.Diagnostics.DebuggerStepThrough]
		private void InitializeComponent()
		{
            this.LayoutControl1 = new DevExpress.XtraLayout.LayoutControl();
            this.checkEdit_ActiveFlag = new DevExpress.XtraEditors.CheckEdit();
            this.CalcEdit_fee_base = new DevExpress.XtraEditors.CalcEdit();
            this.CheckEdit_default = new DevExpress.XtraEditors.CheckEdit();
            this.TextEdit_description = new DevExpress.XtraEditors.TextEdit();
            this.simpleButton_Cancel = new DevExpress.XtraEditors.SimpleButton();
            this.simpleButton_OK = new DevExpress.XtraEditors.SimpleButton();
            this.LookUpEdit_state = new DevExpress.XtraEditors.LookUpEdit();
            this.CalcEdit_fee_disb_max = new DevExpress.XtraEditors.CalcEdit();
            this.CalcEdit_fee_balance_inc = new DevExpress.XtraEditors.CalcEdit();
            this.CalcEdit_fee_sched_payment = new DevExpress.XtraEditors.CalcEdit();
            this.CalcEdit_fee_monthly_max = new DevExpress.XtraEditors.CalcEdit();
            this.CalcEdit_fee_maximum = new DevExpress.XtraEditors.CalcEdit();
            this.CalcEdit_fee_per_creditor = new DevExpress.XtraEditors.CalcEdit();
            this.PercentEdit_fee_percent = new DebtPlus.Data.Controls.PercentEdit();
            this.LookUpEdit_fee_type = new DevExpress.XtraEditors.LookUpEdit();
            this.CheckEdit_fee_balance_max = new DevExpress.XtraEditors.CheckEdit();
            this.LayoutControlGroup1 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.LayoutControlItem1 = new DevExpress.XtraLayout.LayoutControlItem();
            this.LayoutControlItem2 = new DevExpress.XtraLayout.LayoutControlItem();
            this.LayoutControlItem3 = new DevExpress.XtraLayout.LayoutControlItem();
            this.LayoutControlItem4 = new DevExpress.XtraLayout.LayoutControlItem();
            this.LayoutControlItem5 = new DevExpress.XtraLayout.LayoutControlItem();
            this.LayoutControlItem6 = new DevExpress.XtraLayout.LayoutControlItem();
            this.LayoutControlItem7 = new DevExpress.XtraLayout.LayoutControlItem();
            this.LayoutControlItem8 = new DevExpress.XtraLayout.LayoutControlItem();
            this.LayoutControlItem9 = new DevExpress.XtraLayout.LayoutControlItem();
            this.LayoutControlItem11 = new DevExpress.XtraLayout.LayoutControlItem();
            this.LayoutControlItem12 = new DevExpress.XtraLayout.LayoutControlItem();
            this.LayoutControlItem13 = new DevExpress.XtraLayout.LayoutControlItem();
            this.EmptySpaceItem1 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.LayoutControlItem14 = new DevExpress.XtraLayout.LayoutControlItem();
            this.LayoutControlItem10 = new DevExpress.XtraLayout.LayoutControlItem();
            this.LayoutControlItem15 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem16 = new DevExpress.XtraLayout.LayoutControlItem();
            this.checkEdit_preferred = new DevExpress.XtraEditors.CheckEdit();
            this.layoutControlItem17 = new DevExpress.XtraLayout.LayoutControlItem();
            ((System.ComponentModel.ISupportInitialize)(this.DxErrorProvider1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LayoutControl1)).BeginInit();
            this.LayoutControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit_ActiveFlag.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.CalcEdit_fee_base.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.CheckEdit_default.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TextEdit_description.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LookUpEdit_state.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.CalcEdit_fee_disb_max.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.CalcEdit_fee_balance_inc.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.CalcEdit_fee_sched_payment.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.CalcEdit_fee_monthly_max.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.CalcEdit_fee_maximum.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.CalcEdit_fee_per_creditor.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.PercentEdit_fee_percent.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LookUpEdit_fee_type.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.CheckEdit_fee_balance_max.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LayoutControlGroup1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LayoutControlItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LayoutControlItem2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LayoutControlItem3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LayoutControlItem4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LayoutControlItem5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LayoutControlItem6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LayoutControlItem7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LayoutControlItem8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LayoutControlItem9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LayoutControlItem11)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LayoutControlItem12)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LayoutControlItem13)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.EmptySpaceItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LayoutControlItem14)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LayoutControlItem10)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LayoutControlItem15)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem16)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit_preferred.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem17)).BeginInit();
            this.SuspendLayout();
            // 
            // DefaultLookAndFeel1
            // 
            this.DefaultLookAndFeel1.LookAndFeel.SkinName = "Caramel";
            // 
            // LayoutControl1
            // 
            this.LayoutControl1.Controls.Add(this.checkEdit_preferred);
            this.LayoutControl1.Controls.Add(this.checkEdit_ActiveFlag);
            this.LayoutControl1.Controls.Add(this.CalcEdit_fee_base);
            this.LayoutControl1.Controls.Add(this.CheckEdit_default);
            this.LayoutControl1.Controls.Add(this.TextEdit_description);
            this.LayoutControl1.Controls.Add(this.simpleButton_Cancel);
            this.LayoutControl1.Controls.Add(this.simpleButton_OK);
            this.LayoutControl1.Controls.Add(this.LookUpEdit_state);
            this.LayoutControl1.Controls.Add(this.CalcEdit_fee_disb_max);
            this.LayoutControl1.Controls.Add(this.CalcEdit_fee_balance_inc);
            this.LayoutControl1.Controls.Add(this.CalcEdit_fee_sched_payment);
            this.LayoutControl1.Controls.Add(this.CalcEdit_fee_monthly_max);
            this.LayoutControl1.Controls.Add(this.CalcEdit_fee_maximum);
            this.LayoutControl1.Controls.Add(this.CalcEdit_fee_per_creditor);
            this.LayoutControl1.Controls.Add(this.PercentEdit_fee_percent);
            this.LayoutControl1.Controls.Add(this.LookUpEdit_fee_type);
            this.LayoutControl1.Controls.Add(this.CheckEdit_fee_balance_max);
            this.LayoutControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.LayoutControl1.Location = new System.Drawing.Point(0, 0);
            this.LayoutControl1.Name = "LayoutControl1";
            this.LayoutControl1.Root = this.LayoutControlGroup1;
            this.LayoutControl1.Size = new System.Drawing.Size(433, 372);
            this.LayoutControl1.TabIndex = 0;
            this.LayoutControl1.Text = "LayoutControl1";
            // 
            // checkEdit_ActiveFlag
            // 
            this.checkEdit_ActiveFlag.Location = new System.Drawing.Point(218, 275);
            this.checkEdit_ActiveFlag.Name = "checkEdit_ActiveFlag";
            this.checkEdit_ActiveFlag.Properties.Caption = "Active";
            this.checkEdit_ActiveFlag.Size = new System.Drawing.Size(203, 19);
            this.checkEdit_ActiveFlag.StyleController = this.LayoutControl1;
            this.checkEdit_ActiveFlag.TabIndex = 15;
            // 
            // CalcEdit_fee_base
            // 
            this.CalcEdit_fee_base.Location = new System.Drawing.Point(250, 84);
            this.CalcEdit_fee_base.Name = "CalcEdit_fee_base";
            this.CalcEdit_fee_base.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.False;
            this.CalcEdit_fee_base.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.CalcEdit_fee_base.Properties.DisplayFormat.FormatString = "{0:c}";
            this.CalcEdit_fee_base.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.CalcEdit_fee_base.Properties.EditFormat.FormatString = "{0:c}";
            this.CalcEdit_fee_base.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.CalcEdit_fee_base.Properties.Mask.BeepOnError = true;
            this.CalcEdit_fee_base.Properties.Mask.EditMask = "c";
            this.CalcEdit_fee_base.Properties.Precision = 2;
            this.CalcEdit_fee_base.Size = new System.Drawing.Size(68, 20);
            this.CalcEdit_fee_base.StyleController = this.LayoutControl1;
            this.CalcEdit_fee_base.TabIndex = 14;
            // 
            // CheckEdit_default
            // 
            this.CheckEdit_default.Location = new System.Drawing.Point(12, 275);
            this.CheckEdit_default.Name = "CheckEdit_default";
            this.CheckEdit_default.Properties.Caption = "Default";
            this.CheckEdit_default.Size = new System.Drawing.Size(202, 19);
            this.CheckEdit_default.StyleController = this.LayoutControl1;
            this.CheckEdit_default.TabIndex = 13;
            // 
            // TextEdit_description
            // 
            this.TextEdit_description.Location = new System.Drawing.Point(126, 12);
            this.TextEdit_description.Name = "TextEdit_description";
            this.TextEdit_description.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.False;
            this.TextEdit_description.Properties.MaxLength = 50;
            this.TextEdit_description.Size = new System.Drawing.Size(295, 20);
            this.TextEdit_description.StyleController = this.LayoutControl1;
            this.TextEdit_description.TabIndex = 12;
            // 
            // simpleButton_Cancel
            // 
            this.simpleButton_Cancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.simpleButton_Cancel.Location = new System.Drawing.Point(322, 62);
            this.simpleButton_Cancel.Name = "simpleButton_Cancel";
            this.simpleButton_Cancel.Size = new System.Drawing.Size(99, 22);
            this.simpleButton_Cancel.StyleController = this.LayoutControl1;
            this.simpleButton_Cancel.TabIndex = 11;
            this.simpleButton_Cancel.Text = "&Cancel";
            // 
            // simpleButton_OK
            // 
            this.simpleButton_OK.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.simpleButton_OK.Location = new System.Drawing.Point(322, 36);
            this.simpleButton_OK.Name = "simpleButton_OK";
            this.simpleButton_OK.Size = new System.Drawing.Size(99, 22);
            this.simpleButton_OK.StyleController = this.LayoutControl1;
            this.simpleButton_OK.TabIndex = 10;
            this.simpleButton_OK.Text = "&OK";
            // 
            // LookUpEdit_state
            // 
            this.LookUpEdit_state.Location = new System.Drawing.Point(126, 228);
            this.LookUpEdit_state.Name = "LookUpEdit_state";
            this.LookUpEdit_state.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LookUpEdit_state.Properties.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("MailingCode", "State", 5, DevExpress.Utils.FormatType.None, "", true, DevExpress.Utils.HorzAlignment.Center),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("Name", "Name", 20, DevExpress.Utils.FormatType.None, "", true, DevExpress.Utils.HorzAlignment.Near),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("CountryName", "Country", 20, DevExpress.Utils.FormatType.None, "", true, DevExpress.Utils.HorzAlignment.Near)});
            this.LookUpEdit_state.Properties.DisplayMember = "Name";
            this.LookUpEdit_state.Properties.NullText = "";
            this.LookUpEdit_state.Properties.PopupWidth = 400;
            this.LookUpEdit_state.Properties.SearchMode = DevExpress.XtraEditors.Controls.SearchMode.AutoComplete;
            this.LookUpEdit_state.Properties.ShowFooter = false;
            this.LookUpEdit_state.Properties.SortColumnIndex = 1;
            this.LookUpEdit_state.Properties.ValueMember = "Id";
            this.LookUpEdit_state.Size = new System.Drawing.Size(192, 20);
            this.LookUpEdit_state.StyleController = this.LayoutControl1;
            this.LookUpEdit_state.TabIndex = 8;
            // 
            // CalcEdit_fee_disb_max
            // 
            this.CalcEdit_fee_disb_max.Location = new System.Drawing.Point(126, 156);
            this.CalcEdit_fee_disb_max.Name = "CalcEdit_fee_disb_max";
            this.CalcEdit_fee_disb_max.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.True;
            this.CalcEdit_fee_disb_max.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.CalcEdit_fee_disb_max.Properties.DisplayFormat.FormatString = "{0:c}";
            this.CalcEdit_fee_disb_max.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.CalcEdit_fee_disb_max.Properties.EditFormat.FormatString = "{0:c}";
            this.CalcEdit_fee_disb_max.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.CalcEdit_fee_disb_max.Properties.Mask.BeepOnError = true;
            this.CalcEdit_fee_disb_max.Properties.Mask.EditMask = "c";
            this.CalcEdit_fee_disb_max.Properties.Mask.SaveLiteral = false;
            this.CalcEdit_fee_disb_max.Properties.Precision = 2;
            this.CalcEdit_fee_disb_max.Size = new System.Drawing.Size(192, 20);
            this.CalcEdit_fee_disb_max.StyleController = this.LayoutControl1;
            this.CalcEdit_fee_disb_max.TabIndex = 5;
            // 
            // CalcEdit_fee_balance_inc
            // 
            this.CalcEdit_fee_balance_inc.Location = new System.Drawing.Point(126, 204);
            this.CalcEdit_fee_balance_inc.Name = "CalcEdit_fee_balance_inc";
            this.CalcEdit_fee_balance_inc.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.True;
            this.CalcEdit_fee_balance_inc.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.CalcEdit_fee_balance_inc.Properties.DisplayFormat.FormatString = "{0:c}";
            this.CalcEdit_fee_balance_inc.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.CalcEdit_fee_balance_inc.Properties.EditFormat.FormatString = "{0:c}";
            this.CalcEdit_fee_balance_inc.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.CalcEdit_fee_balance_inc.Properties.Mask.BeepOnError = true;
            this.CalcEdit_fee_balance_inc.Properties.Mask.EditMask = "c";
            this.CalcEdit_fee_balance_inc.Properties.Mask.SaveLiteral = false;
            this.CalcEdit_fee_balance_inc.Properties.Precision = 2;
            this.CalcEdit_fee_balance_inc.Size = new System.Drawing.Size(192, 20);
            this.CalcEdit_fee_balance_inc.StyleController = this.LayoutControl1;
            this.CalcEdit_fee_balance_inc.TabIndex = 7;
            // 
            // CalcEdit_fee_sched_payment
            // 
            this.CalcEdit_fee_sched_payment.Location = new System.Drawing.Point(126, 180);
            this.CalcEdit_fee_sched_payment.Name = "CalcEdit_fee_sched_payment";
            this.CalcEdit_fee_sched_payment.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.True;
            this.CalcEdit_fee_sched_payment.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.CalcEdit_fee_sched_payment.Properties.DisplayFormat.FormatString = "{0:c}";
            this.CalcEdit_fee_sched_payment.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.CalcEdit_fee_sched_payment.Properties.EditFormat.FormatString = "{0:c}";
            this.CalcEdit_fee_sched_payment.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.CalcEdit_fee_sched_payment.Properties.Mask.BeepOnError = true;
            this.CalcEdit_fee_sched_payment.Properties.Mask.EditMask = "c";
            this.CalcEdit_fee_sched_payment.Properties.Mask.SaveLiteral = false;
            this.CalcEdit_fee_sched_payment.Properties.Precision = 2;
            this.CalcEdit_fee_sched_payment.Size = new System.Drawing.Size(192, 20);
            this.CalcEdit_fee_sched_payment.StyleController = this.LayoutControl1;
            this.CalcEdit_fee_sched_payment.TabIndex = 6;
            // 
            // CalcEdit_fee_monthly_max
            // 
            this.CalcEdit_fee_monthly_max.Location = new System.Drawing.Point(126, 132);
            this.CalcEdit_fee_monthly_max.Name = "CalcEdit_fee_monthly_max";
            this.CalcEdit_fee_monthly_max.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.True;
            this.CalcEdit_fee_monthly_max.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.CalcEdit_fee_monthly_max.Properties.DisplayFormat.FormatString = "{0:c}";
            this.CalcEdit_fee_monthly_max.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.CalcEdit_fee_monthly_max.Properties.EditFormat.FormatString = "{0:c}";
            this.CalcEdit_fee_monthly_max.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.CalcEdit_fee_monthly_max.Properties.Mask.BeepOnError = true;
            this.CalcEdit_fee_monthly_max.Properties.Mask.EditMask = "c";
            this.CalcEdit_fee_monthly_max.Properties.Mask.SaveLiteral = false;
            this.CalcEdit_fee_monthly_max.Properties.Precision = 2;
            this.CalcEdit_fee_monthly_max.Size = new System.Drawing.Size(192, 20);
            this.CalcEdit_fee_monthly_max.StyleController = this.LayoutControl1;
            this.CalcEdit_fee_monthly_max.TabIndex = 4;
            // 
            // CalcEdit_fee_maximum
            // 
            this.CalcEdit_fee_maximum.Location = new System.Drawing.Point(126, 108);
            this.CalcEdit_fee_maximum.Name = "CalcEdit_fee_maximum";
            this.CalcEdit_fee_maximum.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.True;
            this.CalcEdit_fee_maximum.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.CalcEdit_fee_maximum.Properties.DisplayFormat.FormatString = "{0:c}";
            this.CalcEdit_fee_maximum.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.CalcEdit_fee_maximum.Properties.EditFormat.FormatString = "{0:c}";
            this.CalcEdit_fee_maximum.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.CalcEdit_fee_maximum.Properties.Mask.BeepOnError = true;
            this.CalcEdit_fee_maximum.Properties.Mask.EditMask = "c";
            this.CalcEdit_fee_maximum.Properties.Mask.SaveLiteral = false;
            this.CalcEdit_fee_maximum.Properties.Precision = 2;
            this.CalcEdit_fee_maximum.Size = new System.Drawing.Size(192, 20);
            this.CalcEdit_fee_maximum.StyleController = this.LayoutControl1;
            this.CalcEdit_fee_maximum.TabIndex = 3;
            // 
            // CalcEdit_fee_per_creditor
            // 
            this.CalcEdit_fee_per_creditor.Location = new System.Drawing.Point(126, 84);
            this.CalcEdit_fee_per_creditor.Name = "CalcEdit_fee_per_creditor";
            this.CalcEdit_fee_per_creditor.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.True;
            this.CalcEdit_fee_per_creditor.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.CalcEdit_fee_per_creditor.Properties.DisplayFormat.FormatString = "{0:c}";
            this.CalcEdit_fee_per_creditor.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.CalcEdit_fee_per_creditor.Properties.EditFormat.FormatString = "{0:c}";
            this.CalcEdit_fee_per_creditor.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.CalcEdit_fee_per_creditor.Properties.Mask.BeepOnError = true;
            this.CalcEdit_fee_per_creditor.Properties.Mask.EditMask = "c";
            this.CalcEdit_fee_per_creditor.Properties.Mask.SaveLiteral = false;
            this.CalcEdit_fee_per_creditor.Properties.Precision = 2;
            this.CalcEdit_fee_per_creditor.Size = new System.Drawing.Size(70, 20);
            this.CalcEdit_fee_per_creditor.StyleController = this.LayoutControl1;
            this.CalcEdit_fee_per_creditor.TabIndex = 2;
            // 
            // PercentEdit_fee_percent
            // 
            this.PercentEdit_fee_percent.Location = new System.Drawing.Point(126, 60);
            this.PercentEdit_fee_percent.Name = "PercentEdit_fee_percent";
            this.PercentEdit_fee_percent.Properties.Allow100Percent = false;
            this.PercentEdit_fee_percent.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.PercentEdit_fee_percent.Properties.DisplayFormat.FormatString = "{0:p}";
            this.PercentEdit_fee_percent.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.PercentEdit_fee_percent.Properties.EditFormat.FormatString = "{0:p}";
            this.PercentEdit_fee_percent.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.PercentEdit_fee_percent.Properties.Mask.BeepOnError = true;
            this.PercentEdit_fee_percent.Properties.Mask.EditMask = "p";
            this.PercentEdit_fee_percent.Properties.Precision = 3;
            this.PercentEdit_fee_percent.Size = new System.Drawing.Size(192, 20);
            this.PercentEdit_fee_percent.StyleController = this.LayoutControl1;
            this.PercentEdit_fee_percent.TabIndex = 1;
            // 
            // LookUpEdit_fee_type
            // 
            this.LookUpEdit_fee_type.Location = new System.Drawing.Point(126, 36);
            this.LookUpEdit_fee_type.Name = "LookUpEdit_fee_type";
            this.LookUpEdit_fee_type.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.False;
            this.LookUpEdit_fee_type.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LookUpEdit_fee_type.Properties.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("fee_type", "ID", 5, DevExpress.Utils.FormatType.Numeric, "f0", false, DevExpress.Utils.HorzAlignment.Far),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("description", "Description", 20, DevExpress.Utils.FormatType.None, "", true, DevExpress.Utils.HorzAlignment.Near)});
            this.LookUpEdit_fee_type.Properties.DisplayMember = "description";
            this.LookUpEdit_fee_type.Properties.NullText = "";
            this.LookUpEdit_fee_type.Properties.ShowFooter = false;
            this.LookUpEdit_fee_type.Properties.ShowHeader = false;
            this.LookUpEdit_fee_type.Properties.SortColumnIndex = 1;
            this.LookUpEdit_fee_type.Properties.ValueMember = "Id";
            this.LookUpEdit_fee_type.Size = new System.Drawing.Size(192, 20);
            this.LookUpEdit_fee_type.StyleController = this.LayoutControl1;
            this.LookUpEdit_fee_type.TabIndex = 0;
            // 
            // CheckEdit_fee_balance_max
            // 
            this.CheckEdit_fee_balance_max.Location = new System.Drawing.Point(12, 252);
            this.CheckEdit_fee_balance_max.Name = "CheckEdit_fee_balance_max";
            this.CheckEdit_fee_balance_max.Properties.Caption = "Limit disbursement to balance on debt";
            this.CheckEdit_fee_balance_max.Size = new System.Drawing.Size(409, 19);
            this.CheckEdit_fee_balance_max.StyleController = this.LayoutControl1;
            this.CheckEdit_fee_balance_max.TabIndex = 9;
            // 
            // LayoutControlGroup1
            // 
            this.LayoutControlGroup1.CustomizationFormText = "LayoutControlGroup1";
            this.LayoutControlGroup1.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.LayoutControlItem1,
            this.LayoutControlItem2,
            this.LayoutControlItem3,
            this.LayoutControlItem4,
            this.LayoutControlItem5,
            this.LayoutControlItem6,
            this.LayoutControlItem7,
            this.LayoutControlItem8,
            this.LayoutControlItem9,
            this.LayoutControlItem11,
            this.LayoutControlItem12,
            this.LayoutControlItem13,
            this.EmptySpaceItem1,
            this.LayoutControlItem14,
            this.LayoutControlItem10,
            this.LayoutControlItem15,
            this.layoutControlItem16,
            this.layoutControlItem17});
            this.LayoutControlGroup1.Location = new System.Drawing.Point(0, 0);
            this.LayoutControlGroup1.Name = "LayoutControlGroup1";
            this.LayoutControlGroup1.Size = new System.Drawing.Size(433, 372);
            this.LayoutControlGroup1.Text = "LayoutControlGroup1";
            this.LayoutControlGroup1.TextVisible = false;
            // 
            // LayoutControlItem1
            // 
            this.LayoutControlItem1.Control = this.LookUpEdit_fee_type;
            this.LayoutControlItem1.CustomizationFormText = "Fee Type";
            this.LayoutControlItem1.Location = new System.Drawing.Point(0, 24);
            this.LayoutControlItem1.Name = "LayoutControlItem1";
            this.LayoutControlItem1.Size = new System.Drawing.Size(310, 24);
            this.LayoutControlItem1.Text = "Fee Type";
            this.LayoutControlItem1.TextSize = new System.Drawing.Size(111, 13);
            // 
            // LayoutControlItem2
            // 
            this.LayoutControlItem2.Control = this.PercentEdit_fee_percent;
            this.LayoutControlItem2.CustomizationFormText = "Disbursement Percent";
            this.LayoutControlItem2.Location = new System.Drawing.Point(0, 48);
            this.LayoutControlItem2.Name = "LayoutControlItem2";
            this.LayoutControlItem2.Size = new System.Drawing.Size(310, 24);
            this.LayoutControlItem2.Text = "Disbursement Percent";
            this.LayoutControlItem2.TextSize = new System.Drawing.Size(111, 13);
            // 
            // LayoutControlItem3
            // 
            this.LayoutControlItem3.Control = this.CalcEdit_fee_per_creditor;
            this.LayoutControlItem3.CustomizationFormText = "Amount per creditor";
            this.LayoutControlItem3.Location = new System.Drawing.Point(0, 72);
            this.LayoutControlItem3.Name = "LayoutControlItem3";
            this.LayoutControlItem3.Size = new System.Drawing.Size(188, 24);
            this.LayoutControlItem3.Text = "Amount per creditor";
            this.LayoutControlItem3.TextSize = new System.Drawing.Size(111, 13);
            // 
            // LayoutControlItem4
            // 
            this.LayoutControlItem4.Control = this.CalcEdit_fee_maximum;
            this.LayoutControlItem4.CustomizationFormText = "Limit amount";
            this.LayoutControlItem4.Location = new System.Drawing.Point(0, 96);
            this.LayoutControlItem4.Name = "LayoutControlItem4";
            this.LayoutControlItem4.Size = new System.Drawing.Size(310, 24);
            this.LayoutControlItem4.Text = "Limit amount";
            this.LayoutControlItem4.TextSize = new System.Drawing.Size(111, 13);
            // 
            // LayoutControlItem5
            // 
            this.LayoutControlItem5.Control = this.CalcEdit_fee_monthly_max;
            this.LayoutControlItem5.CustomizationFormText = "Max per month";
            this.LayoutControlItem5.Location = new System.Drawing.Point(0, 120);
            this.LayoutControlItem5.Name = "LayoutControlItem5";
            this.LayoutControlItem5.Size = new System.Drawing.Size(310, 24);
            this.LayoutControlItem5.Text = "Max per month";
            this.LayoutControlItem5.TextSize = new System.Drawing.Size(111, 13);
            // 
            // LayoutControlItem6
            // 
            this.LayoutControlItem6.Control = this.CalcEdit_fee_sched_payment;
            this.LayoutControlItem6.CustomizationFormText = "Sched Payment";
            this.LayoutControlItem6.Location = new System.Drawing.Point(0, 168);
            this.LayoutControlItem6.Name = "LayoutControlItem6";
            this.LayoutControlItem6.Size = new System.Drawing.Size(310, 24);
            this.LayoutControlItem6.Text = "Sched Payment";
            this.LayoutControlItem6.TextSize = new System.Drawing.Size(111, 13);
            // 
            // LayoutControlItem7
            // 
            this.LayoutControlItem7.Control = this.CalcEdit_fee_balance_inc;
            this.LayoutControlItem7.CustomizationFormText = "Balance Increment Amt";
            this.LayoutControlItem7.Location = new System.Drawing.Point(0, 192);
            this.LayoutControlItem7.Name = "LayoutControlItem7";
            this.LayoutControlItem7.Size = new System.Drawing.Size(310, 24);
            this.LayoutControlItem7.Text = "Balance Increment Amt";
            this.LayoutControlItem7.TextSize = new System.Drawing.Size(111, 13);
            // 
            // LayoutControlItem8
            // 
            this.LayoutControlItem8.Control = this.CalcEdit_fee_disb_max;
            this.LayoutControlItem8.CustomizationFormText = "Disbursement Max";
            this.LayoutControlItem8.Location = new System.Drawing.Point(0, 144);
            this.LayoutControlItem8.Name = "LayoutControlItem8";
            this.LayoutControlItem8.Size = new System.Drawing.Size(310, 24);
            this.LayoutControlItem8.Text = "Disbursement Max";
            this.LayoutControlItem8.TextSize = new System.Drawing.Size(111, 13);
            // 
            // LayoutControlItem9
            // 
            this.LayoutControlItem9.Control = this.LookUpEdit_state;
            this.LayoutControlItem9.CustomizationFormText = "State";
            this.LayoutControlItem9.Location = new System.Drawing.Point(0, 216);
            this.LayoutControlItem9.Name = "LayoutControlItem9";
            this.LayoutControlItem9.Size = new System.Drawing.Size(310, 24);
            this.LayoutControlItem9.Text = "State";
            this.LayoutControlItem9.TextSize = new System.Drawing.Size(111, 13);
            // 
            // LayoutControlItem11
            // 
            this.LayoutControlItem11.Control = this.simpleButton_OK;
            this.LayoutControlItem11.CustomizationFormText = "LayoutControlItem11";
            this.LayoutControlItem11.Location = new System.Drawing.Point(310, 24);
            this.LayoutControlItem11.Name = "LayoutControlItem11";
            this.LayoutControlItem11.Size = new System.Drawing.Size(103, 26);
            this.LayoutControlItem11.Text = "LayoutControlItem11";
            this.LayoutControlItem11.TextSize = new System.Drawing.Size(0, 0);
            this.LayoutControlItem11.TextToControlDistance = 0;
            this.LayoutControlItem11.TextVisible = false;
            // 
            // LayoutControlItem12
            // 
            this.LayoutControlItem12.Control = this.simpleButton_Cancel;
            this.LayoutControlItem12.CustomizationFormText = "LayoutControlItem12";
            this.LayoutControlItem12.Location = new System.Drawing.Point(310, 50);
            this.LayoutControlItem12.Name = "LayoutControlItem12";
            this.LayoutControlItem12.Size = new System.Drawing.Size(103, 26);
            this.LayoutControlItem12.Text = "LayoutControlItem12";
            this.LayoutControlItem12.TextSize = new System.Drawing.Size(0, 0);
            this.LayoutControlItem12.TextToControlDistance = 0;
            this.LayoutControlItem12.TextVisible = false;
            // 
            // LayoutControlItem13
            // 
            this.LayoutControlItem13.Control = this.TextEdit_description;
            this.LayoutControlItem13.CustomizationFormText = "Description";
            this.LayoutControlItem13.Location = new System.Drawing.Point(0, 0);
            this.LayoutControlItem13.Name = "LayoutControlItem13";
            this.LayoutControlItem13.Size = new System.Drawing.Size(413, 24);
            this.LayoutControlItem13.Text = "Description";
            this.LayoutControlItem13.TextSize = new System.Drawing.Size(111, 13);
            // 
            // EmptySpaceItem1
            // 
            this.EmptySpaceItem1.AllowHotTrack = false;
            this.EmptySpaceItem1.CustomizationFormText = "EmptySpaceItem1";
            this.EmptySpaceItem1.Location = new System.Drawing.Point(310, 76);
            this.EmptySpaceItem1.Name = "EmptySpaceItem1";
            this.EmptySpaceItem1.Size = new System.Drawing.Size(103, 164);
            this.EmptySpaceItem1.Text = "EmptySpaceItem1";
            this.EmptySpaceItem1.TextSize = new System.Drawing.Size(0, 0);
            // 
            // LayoutControlItem14
            // 
            this.LayoutControlItem14.Control = this.CheckEdit_default;
            this.LayoutControlItem14.CustomizationFormText = "LayoutControlItem14";
            this.LayoutControlItem14.Location = new System.Drawing.Point(0, 263);
            this.LayoutControlItem14.Name = "LayoutControlItem14";
            this.LayoutControlItem14.Size = new System.Drawing.Size(206, 23);
            this.LayoutControlItem14.Text = "LayoutControlItem14";
            this.LayoutControlItem14.TextSize = new System.Drawing.Size(0, 0);
            this.LayoutControlItem14.TextToControlDistance = 0;
            this.LayoutControlItem14.TextVisible = false;
            // 
            // LayoutControlItem10
            // 
            this.LayoutControlItem10.Control = this.CheckEdit_fee_balance_max;
            this.LayoutControlItem10.CustomizationFormText = "LayoutControlItem10";
            this.LayoutControlItem10.Location = new System.Drawing.Point(0, 240);
            this.LayoutControlItem10.Name = "LayoutControlItem10";
            this.LayoutControlItem10.Size = new System.Drawing.Size(413, 23);
            this.LayoutControlItem10.Text = "LayoutControlItem10";
            this.LayoutControlItem10.TextSize = new System.Drawing.Size(0, 0);
            this.LayoutControlItem10.TextToControlDistance = 0;
            this.LayoutControlItem10.TextVisible = false;
            // 
            // LayoutControlItem15
            // 
            this.LayoutControlItem15.Control = this.CalcEdit_fee_base;
            this.LayoutControlItem15.CustomizationFormText = "Plus base";
            this.LayoutControlItem15.Location = new System.Drawing.Point(188, 72);
            this.LayoutControlItem15.Name = "LayoutControlItem15";
            this.LayoutControlItem15.Size = new System.Drawing.Size(122, 24);
            this.LayoutControlItem15.Text = "Plus Base";
            this.LayoutControlItem15.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.AutoSize;
            this.LayoutControlItem15.TextSize = new System.Drawing.Size(45, 13);
            this.LayoutControlItem15.TextToControlDistance = 5;
            // 
            // layoutControlItem16
            // 
            this.layoutControlItem16.Control = this.checkEdit_ActiveFlag;
            this.layoutControlItem16.CustomizationFormText = "layoutControlItem16";
            this.layoutControlItem16.Location = new System.Drawing.Point(206, 263);
            this.layoutControlItem16.Name = "layoutControlItem16";
            this.layoutControlItem16.Size = new System.Drawing.Size(207, 89);
            this.layoutControlItem16.Text = "layoutControlItem16";
            this.layoutControlItem16.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem16.TextToControlDistance = 0;
            this.layoutControlItem16.TextVisible = false;
            // 
            // checkEdit_preferred
            // 
            this.checkEdit_preferred.Location = new System.Drawing.Point(12, 298);
            this.checkEdit_preferred.Name = "checkEdit_preferred";
            this.checkEdit_preferred.Properties.Caption = "Preferred";
            this.checkEdit_preferred.Size = new System.Drawing.Size(202, 19);
            this.checkEdit_preferred.StyleController = this.LayoutControl1;
            this.checkEdit_preferred.TabIndex = 16;
            this.checkEdit_preferred.ToolTip = "When debts are created, should we use this fee for clients in the state?";
            // 
            // layoutControlItem17
            // 
            this.layoutControlItem17.Control = this.checkEdit_preferred;
            this.layoutControlItem17.CustomizationFormText = "layoutControlItem17";
            this.layoutControlItem17.Location = new System.Drawing.Point(0, 286);
            this.layoutControlItem17.Name = "layoutControlItem17";
            this.layoutControlItem17.Size = new System.Drawing.Size(206, 66);
            this.layoutControlItem17.Text = "layoutControlItem17";
            this.layoutControlItem17.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem17.TextToControlDistance = 0;
            this.layoutControlItem17.TextVisible = false;
            // 
            // ConfigFeeForm
            // 
            this.AcceptButton = this.simpleButton_OK;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.CancelButton = this.simpleButton_Cancel;
            this.ClientSize = new System.Drawing.Size(433, 372);
            this.Controls.Add(this.LayoutControl1);
            this.Name = "ConfigFeeForm";
            this.Text = "System Fee Entry";
            ((System.ComponentModel.ISupportInitialize)(this.DxErrorProvider1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LayoutControl1)).EndInit();
            this.LayoutControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit_ActiveFlag.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.CalcEdit_fee_base.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.CheckEdit_default.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TextEdit_description.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LookUpEdit_state.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.CalcEdit_fee_disb_max.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.CalcEdit_fee_balance_inc.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.CalcEdit_fee_sched_payment.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.CalcEdit_fee_monthly_max.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.CalcEdit_fee_maximum.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.CalcEdit_fee_per_creditor.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.PercentEdit_fee_percent.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LookUpEdit_fee_type.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.CheckEdit_fee_balance_max.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LayoutControlGroup1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LayoutControlItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LayoutControlItem2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LayoutControlItem3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LayoutControlItem4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LayoutControlItem5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LayoutControlItem6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LayoutControlItem7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LayoutControlItem8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LayoutControlItem9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LayoutControlItem11)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LayoutControlItem12)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LayoutControlItem13)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.EmptySpaceItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LayoutControlItem14)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LayoutControlItem10)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LayoutControlItem15)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem16)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit_preferred.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem17)).EndInit();
            this.ResumeLayout(false);

		}

		private DevExpress.XtraLayout.LayoutControl LayoutControl1;
		private DevExpress.XtraLayout.LayoutControlGroup LayoutControlGroup1;
		private DevExpress.XtraEditors.LookUpEdit LookUpEdit_fee_type;
		private DevExpress.XtraLayout.LayoutControlItem LayoutControlItem1;
		private DevExpress.XtraEditors.CalcEdit CalcEdit_fee_per_creditor;
		private DebtPlus.Data.Controls.PercentEdit PercentEdit_fee_percent;
		private DevExpress.XtraLayout.LayoutControlItem LayoutControlItem2;
		private DevExpress.XtraLayout.LayoutControlItem LayoutControlItem3;
		private DevExpress.XtraEditors.CalcEdit CalcEdit_fee_maximum;
		private DevExpress.XtraLayout.LayoutControlItem LayoutControlItem4;
		private DevExpress.XtraEditors.CalcEdit CalcEdit_fee_sched_payment;
		private DevExpress.XtraEditors.CalcEdit CalcEdit_fee_monthly_max;
		private DevExpress.XtraLayout.LayoutControlItem LayoutControlItem5;
		private DevExpress.XtraLayout.LayoutControlItem LayoutControlItem6;
		private DevExpress.XtraEditors.CalcEdit CalcEdit_fee_balance_inc;
		private DevExpress.XtraLayout.LayoutControlItem LayoutControlItem7;
		private DevExpress.XtraEditors.CalcEdit CalcEdit_fee_disb_max;
		private DevExpress.XtraLayout.LayoutControlItem LayoutControlItem8;
		private DevExpress.XtraLayout.EmptySpaceItem EmptySpaceItem1;
		private DevExpress.XtraEditors.CheckEdit CheckEdit_fee_balance_max;
		private DevExpress.XtraEditors.LookUpEdit LookUpEdit_state;
		private DevExpress.XtraLayout.LayoutControlItem LayoutControlItem9;
		private DevExpress.XtraLayout.LayoutControlItem LayoutControlItem10;
		private DevExpress.XtraEditors.SimpleButton simpleButton_Cancel;
		private DevExpress.XtraEditors.SimpleButton simpleButton_OK;
		private DevExpress.XtraLayout.LayoutControlItem LayoutControlItem11;
		private DevExpress.XtraLayout.LayoutControlItem LayoutControlItem12;
		private DevExpress.XtraEditors.TextEdit TextEdit_description;
		private DevExpress.XtraLayout.LayoutControlItem LayoutControlItem13;
		private DevExpress.XtraEditors.CheckEdit CheckEdit_default;
		private DevExpress.XtraLayout.LayoutControlItem LayoutControlItem14;
		private DevExpress.XtraEditors.CalcEdit CalcEdit_fee_base;
		private DevExpress.XtraLayout.LayoutControlItem LayoutControlItem15;
        private DevExpress.XtraEditors.CheckEdit checkEdit_ActiveFlag;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem16;
        private DevExpress.XtraEditors.CheckEdit checkEdit_preferred;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem17;
	}
}
