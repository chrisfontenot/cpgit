#region "Copyright 2000-2012 DebtPlus, L.L.C."
//{*******************************************************************}
//{                                                                   }
//{       DebtPlus Debt Management System                             }
//{                                                                   }
//{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
//{       ALL RIGHTS RESERVED                                         }
//{                                                                   }
//{   The entire contents of this file is protected by U.S. and       }
//{   International Copyright Laws. Unauthorized reproduction,        }
//{   reverse-engineering, and distribution of all or any portion of  }
//{   the code contained in this file is strictly prohibited and may  }
//{   result in severe civil and criminal penalties and will be       }
//{   prosecuted to the maximum extent possible under the law.        }
//{                                                                   }
//{   RESTRICTIONS                                                    }
//{                                                                   }
//{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
//{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
//{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
//{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
//{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
//{                                                                   }
//{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
//{   ADDITIONAL RESTRICTIONS.                                        }
//{                                                                   }
//{*******************************************************************}

#endregion "Copyright 2000-2012 DebtPlus, L.L.C."

using System;
using System.Linq;
using System.Windows.Forms;
using DebtPlus.LINQ;
using DebtPlus.UI.Common;
using DevExpress.XtraGrid.Views.Grid.ViewInfo;

namespace DebtPlus.UI.TableAdministration.CS.Config
{
    internal partial class ConfigFees
    {
        private Int32 ControlRow = -1;
        private BusinessContext bc = null;
        private System.Collections.Generic.List<config_fee> colConfigFee;

        internal ConfigFees()
            : base()
        {
            InitializeComponent();
        }

        private void RegisterHandlers()
        {
            BarButtonItem_Edit.ItemClick += BarButtonItem_Edit_ItemClick;
            BarButtonItem_Add.ItemClick += BarButtonItem_Add_ItemClick;
            BarButtonItem_Delete.ItemClick += BarButtonItem_Delete_ItemClick;
            gridView1.DoubleClick += gridView1_DoubleClick;
            gridView1.MouseUp += gridView1_MouseUp;
            PopupMenu1.Popup += ContextMenu1_Popup;
        }

        private void UnRegisterHandlers()
        {
            BarButtonItem_Edit.ItemClick -= BarButtonItem_Edit_ItemClick;
            BarButtonItem_Add.ItemClick -= BarButtonItem_Add_ItemClick;
            BarButtonItem_Delete.ItemClick -= BarButtonItem_Delete_ItemClick;
            gridView1.DoubleClick -= gridView1_DoubleClick;
            gridView1.MouseUp -= gridView1_MouseUp;
            PopupMenu1.Popup -= ContextMenu1_Popup;
        }

        internal void ReadForm(BusinessContext bc)
        {
            this.bc = bc;
            UnRegisterHandlers();
            try
            {
                using (var cm = new CursorManager())
                {
                    try
                    {
                        colConfigFee = bc.config_fees.ToList();
                        gridControl1.DataSource = colConfigFee;
                    }
                    catch (System.Data.SqlClient.SqlException ex)
                    {
                        DebtPlus.UI.Common.ErrorHandling.HandleErrors(ex, "Error reading config_fee table");
                    }
                }
            }
            finally
            {
                RegisterHandlers();
            }
        }

        /// <summary>
        /// Update the record
        /// </summary>
        /// <param name="obj">Pointer to the record to be updated</param>
        private void UpdateRecord(object obj)
        {
            var record = obj as config_fee;
            if (record == null)
            {
                return;
            }

            // Edit the record
            bool priorDefault = record.Default;
            bool priorDeferred = record.preferred;
            using (ConfigFeeForm frm = new ConfigFeeForm(record))
            {
                if (frm.ShowDialog() != DialogResult.OK)
                {
                    return;
                }
            }

            // If the record is now the default record then clear the previous default
            if (record.Default && !priorDefault)
            {
                foreach (var defaultRecord in colConfigFee.Where(s => s.Default))
                {
                    defaultRecord.Default = false;
                }
                record.Default = true;
            }

            if (record.preferred && !priorDeferred && record.state.HasValue)
            {
                foreach (var preferredRecord in colConfigFee.Where(s => s.state.GetValueOrDefault() == record.state.GetValueOrDefault() && s.preferred))
                {
                    preferredRecord.preferred = false;
                }
                record.preferred = true;
            }

            // Update the database and the collection
            bc.SubmitChanges();
            gridView1.RefreshData();
        }

        /// <summary>
        /// Delete the indicated record
        /// </summary>
        /// <param name="obj">Pointer to the record to be deleted</param>
        private void DeleteRecord(object obj)
        {
            // There needs to be a valid record
            var record = obj as config_fee;
            if (record == null)
            {
                return;
            }

            // If the record is the preferred item then make the next candidate the preferred key.
            if (record.preferred && record.state.HasValue)
            {
                var q = colConfigFee.Where(s => s.state.GetValueOrDefault() == record.state.GetValueOrDefault() && !s.preferred).OrderBy(s => s.Id).FirstOrDefault();
                if (q != null)
                {
                    q.preferred = true;
                }
            }

            // Do the record deletion
            colConfigFee.Remove(record);
            bc.config_fees.DeleteOnSubmit(record);
            bc.SubmitChanges();
            gridView1.RefreshData();
        }

        /// <summary>
        /// Do the record creation
        /// </summary>
        private void CreateRecord()
        {
            var record = DebtPlus.LINQ.Factory.Manufacture_config_fee();

            using (ConfigFeeForm frm = new ConfigFeeForm(record))
            {
                if (frm.ShowDialog() != DialogResult.OK)
                {
                    return;
                }
            }

            if (record.Default)
            {
                foreach (var defaultRecord in colConfigFee.Where(s => s.Default))
                {
                    defaultRecord.Default = false;
                }
                record.Default = true;
            }

            // Turn off the preferred status for each state that match the current records value.
            if (record.preferred && record.state.HasValue)
            {
                foreach (var preferredRecord in colConfigFee.Where(s => s.state.GetValueOrDefault() == record.state.GetValueOrDefault()))
                {
                    preferredRecord.preferred = false;
                }
                record.preferred = true;
            }

            colConfigFee.Add(record);
            bc.config_fees.InsertOnSubmit(record);
            bc.SubmitChanges();
            gridView1.RefreshData();
        }

        /// <summary>
        /// Do an EDIT function
        /// </summary>
        private void BarButtonItem_Edit_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            if (ControlRow >= 0)
            {
                UpdateRecord(gridView1.GetRow(ControlRow));
            }
        }

        /// <summary>
        /// Do an Add function
        /// </summary>
        private void BarButtonItem_Add_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            CreateRecord();
        }

        /// <summary>
        /// Do an Add function
        /// </summary>
        private void BarButtonItem_Delete_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            if (ControlRow >= 0)
            {
                DeleteRecord(gridView1.GetRow(ControlRow));
            }
        }

        /// <summary>
        /// Double click event on the list
        /// </summary>
        private void gridView1_DoubleClick(object sender, EventArgs e)
        {
            GridHitInfo hi = gridView1.CalcHitInfo((gridControl1.PointToClient(MousePosition)));

            // Find the record in the list
            if (hi.IsValid && hi.RowHandle >= 0)
            {
                UpdateRecord(gridView1.GetRow(hi.RowHandle));
            }
        }

        /// <summary>
        /// Handle the condition where the popup menu was activated
        /// </summary>
        private void gridView1_MouseUp(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Right)
            {
                GridHitInfo hi = gridView1.CalcHitInfo((gridControl1.PointToClient(MousePosition)));
                ControlRow = hi.IsValid ? hi.RowHandle : -1;
                PopupMenu1.ShowPopup(MousePosition);
            }
        }

        /// <summary>
        /// Edit -> menu being shown
        /// </summary>
        private void ContextMenu1_Popup(object sender, EventArgs e)
        {
            // Find the row view in the dataview object for this row.
            config_fee record = null;
            if (ControlRow >= 0)
            {
                gridView1.FocusedRowHandle = ControlRow;
                record = (config_fee)gridView1.GetRow(ControlRow);
            }

            // If the row is defined then edit the row.
            BarButtonItem_Edit.Enabled = (record != null);
            BarButtonItem_Delete.Enabled = BarButtonItem_Edit.Enabled;
            BarButtonItem_Add.Enabled = true;
        }
    }
}