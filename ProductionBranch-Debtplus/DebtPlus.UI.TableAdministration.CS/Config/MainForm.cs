#region "Copyright 2000-2012 DebtPlus, L.L.C."
//{*******************************************************************}
//{                                                                   }
//{       DebtPlus Debt Management System                             }
//{                                                                   }
//{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
//{       ALL RIGHTS RESERVED                                         }
//{                                                                   }
//{   The entire contents of this file is protected by U.S. and       }
//{   International Copyright Laws. Unauthorized reproduction,        }
//{   reverse-engineering, and distribution of all or any portion of  }
//{   the code contained in this file is strictly prohibited and may  }
//{   result in severe civil and criminal penalties and will be       }
//{   prosecuted to the maximum extent possible under the law.        }
//{                                                                   }
//{   RESTRICTIONS                                                    }
//{                                                                   }
//{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
//{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
//{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
//{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
//{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
//{                                                                   }
//{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
//{   ADDITIONAL RESTRICTIONS.                                        }
//{                                                                   }
//{*******************************************************************}

#endregion "Copyright 2000-2012 DebtPlus, L.L.C."

using System;
using System.ComponentModel;
using System.Linq;
using System.Windows.Forms;
using DebtPlus.LINQ;
using DebtPlus.UI.Common;
using DevExpress.XtraEditors.Controls;

namespace DebtPlus.UI.TableAdministration.CS.Config
{
    public partial class MainForm
    {
        private DebtPlus.LINQ.config configRecord;
        private BusinessContext bc = new BusinessContext();
        private System.Collections.Generic.List<PaymentPercent> colPaymentPercent;

        /// <summary>
        /// Initialize the new form class
        /// </summary>
        public MainForm()
            : base()
        {
            InitializeComponent();
            RegisterHandlers();

            // Create the collection of percentage figures
            colPaymentPercent = new System.Collections.Generic.List<PaymentPercent>();
            colPaymentPercent.Add(new PaymentPercent(0.01, "interest + 1%"));
            colPaymentPercent.Add(new PaymentPercent(0.02, "2.0%"));
            colPaymentPercent.Add(new PaymentPercent(0.0208, "2.08%"));
            colPaymentPercent.Add(new PaymentPercent(0.025, "2.5%"));
            colPaymentPercent.Add(new PaymentPercent(0.0278, "2.78%"));
            colPaymentPercent.Add(new PaymentPercent(0.03, "3.0%"));
            colPaymentPercent.Add(new PaymentPercent(0.035, "3.5%"));
            colPaymentPercent.Add(new PaymentPercent(0.04, "4.0%"));
            colPaymentPercent.Add(new PaymentPercent(0.045, "4.5%"));
            colPaymentPercent.Add(new PaymentPercent(0.05, "5.0%"));

            // Bind the datasource for the list to the collection
            LookUpEdit_ComparisonPercent.Properties.DataSource = colPaymentPercent;
        }

        /// <summary>
        /// Class to help with the payment percentages
        /// </summary>
        private class PaymentPercent
        {
            internal double rate
            { get; set; }

            internal string description
            { get; set; }

            internal PaymentPercent(double rate, string description)
            {
                this.rate = rate;
                this.description = description;
            }
        }

        /// <summary>
        /// Register the event handlers
        /// </summary>
        private void RegisterHandlers()
        {
            Load += MainForm_Load;
            this.FormClosing += MainForm_FormClosing;
            SpinEdit_acceptance_days.EditValueChanging += SpinEdit_EditValueChanging;
            SpinEdit_default_till_missed.EditValueChanging += SpinEdit_EditValueChanging;
            SpinEdit_chks_per_invoice.EditValueChanging += SpinEdit_EditValueChanging;
            TextEdit_web_site.Validating += TextEdit_web_site_Validating;
            BarButtonItem_File_Exit.ItemClick += BarButtonItem_File_Exit_ItemClick;
        }

        /// <summary>
        /// Remove the event handler registrations
        /// </summary>
        private void UnRegisterHandlers()
        {
            Load -= MainForm_Load;
            this.FormClosing -= MainForm_FormClosing;
            SpinEdit_acceptance_days.EditValueChanging -= SpinEdit_EditValueChanging;
            SpinEdit_default_till_missed.EditValueChanging -= SpinEdit_EditValueChanging;
            SpinEdit_chks_per_invoice.EditValueChanging -= SpinEdit_EditValueChanging;
            TextEdit_web_site.Validating -= TextEdit_web_site_Validating;
            BarButtonItem_File_Exit.ItemClick -= BarButtonItem_File_Exit_ItemClick;
        }

        /// <summary>
        /// Save the information when the form is closing.
        /// </summary>
        private void MainForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            // If we are shutting down then exit immediately.
            if (e.CloseReason == CloseReason.WindowsShutDown)
            {
                return;
            }

            try
            {
                // Retrieve the values from the input control into the current record
                configRecord.AddressID = AddressRecordControl1.EditValue;
                configRecord.AltAddressID = AddressRecordControl_Alternate.EditValue;
                configRecord.TelephoneID = TelephoneNumberRecordControl1.EditValue;
                configRecord.AltTelephoneID = TelephoneNumberRecordControl2.EditValue;
                configRecord.FAXID = TelephoneNumberRecordControl3.EditValue;
                configRecord.name = DebtPlus.Utils.Nulls.v_String(TextEdit_name.EditValue);
                configRecord.areacode = DebtPlus.Utils.Nulls.v_String(TextEdit_areacode.EditValue);
                configRecord.web_site = DebtPlus.Utils.Nulls.v_String(TextEdit_web_site.EditValue);
                configRecord.payment_minimum = DebtPlus.Utils.Nulls.v_Decimal(CalcEdit_min_amount.EditValue);
                configRecord.threshold = DebtPlus.Utils.Nulls.v_Double(PercentEdit_threshold.EditValue).GetValueOrDefault();
                configRecord.counseling_creditor = CreditorID_counseling_creditor.EditValue;
                configRecord.deduct_creditor = CreditorID_deduct_creditor.EditValue;
                configRecord.paf_creditor = CreditorID_paf_creditor.EditValue;
                configRecord.setup_creditor = CreditorID_setup_creditor.EditValue;
                configRecord.payments_pad_less = DebtPlus.Utils.Nulls.v_Decimal(CalcEdit_payments_padd_less.EditValue).GetValueOrDefault();
                configRecord.payments_pad_greater = DebtPlus.Utils.Nulls.v_Decimal(CalcEdit_payments_padd_greater.EditValue).GetValueOrDefault();
                configRecord.bal_verify_months = DebtPlus.Utils.Nulls.v_Int32(CalcEdit_bal_verify_months.EditValue).GetValueOrDefault();
                configRecord.acceptance_days = DebtPlus.Utils.Nulls.v_Int32(SpinEdit_acceptance_days.EditValue).GetValueOrDefault();
                configRecord.default_till_missed = DebtPlus.Utils.Nulls.v_Int32(SpinEdit_default_till_missed.EditValue).GetValueOrDefault();
                configRecord.chks_per_invoice = DebtPlus.Utils.Nulls.v_Int32(SpinEdit_chks_per_invoice.EditValue);
                configRecord.rpps_resend_msg = CheckEdit_rpps_resend_msg.Checked;
                configRecord.ComparisonPercent = DebtPlus.Utils.Nulls.v_Double(LookUpEdit_ComparisonPercent.EditValue).GetValueOrDefault();
                configRecord.ComparisonAmount = DebtPlus.Utils.Nulls.v_Decimal(CalcEdit_ComparisonAmount.EditValue).GetValueOrDefault();
                configRecord.ComparisonRate = DebtPlus.Utils.Nulls.v_Double(PercentEdit_ComparisonRate.EditValue).GetValueOrDefault();

                // Submit the changes to the database
                bc.SubmitChanges();
            }
            catch (Exception ex)
            {
                DebtPlus.UI.Common.ErrorHandling.HandleErrors(ex, "Error saving information");
                e.Cancel = true;
            }
        }

        /// <summary>
        /// Process the LOAD event on the form
        /// </summary>
        private void MainForm_Load(object sender, EventArgs e)
        {
            using (var cm = new CursorManager())
            {
                // Retrieve the config record from the database
                configRecord = bc.configs.FirstOrDefault();

                // If there is no config row then create a new record with the default values
                if (configRecord == null)
                {
                    configRecord = DebtPlus.LINQ.Factory.Manufacture_config();
                    configRecord.Id = 1; // Use the first ID for the config file.

                    // Add the record to the tables. It will be added when we do the submitchanges method request.
                    bc.configs.InsertOnSubmit(configRecord);
                }
            }

            // Load the controls with the values from the config record
            AddressRecordControl1.EditValue = configRecord.AddressID;
            AddressRecordControl_Alternate.EditValue = configRecord.AltAddressID;
            TelephoneNumberRecordControl1.EditValue = configRecord.TelephoneID;
            TelephoneNumberRecordControl2.EditValue = configRecord.AltTelephoneID;
            TelephoneNumberRecordControl3.EditValue = configRecord.FAXID;
            TextEdit_name.EditValue = configRecord.name;
            TextEdit_areacode.EditValue = configRecord.areacode;
            TextEdit_web_site.EditValue = configRecord.web_site;
            CalcEdit_min_amount.EditValue = configRecord.payment_minimum;
            PercentEdit_threshold.EditValue = configRecord.threshold;
            CreditorID_counseling_creditor.EditValue = configRecord.counseling_creditor;
            CreditorID_deduct_creditor.EditValue = configRecord.deduct_creditor;
            CreditorID_paf_creditor.EditValue = configRecord.paf_creditor;
            CreditorID_setup_creditor.EditValue = configRecord.setup_creditor;
            CalcEdit_payments_padd_less.EditValue = configRecord.payments_pad_less;
            CalcEdit_payments_padd_greater.EditValue = configRecord.payments_pad_greater;
            CalcEdit_bal_verify_months.EditValue = configRecord.bal_verify_months;
            SpinEdit_acceptance_days.EditValue = configRecord.acceptance_days;
            SpinEdit_default_till_missed.EditValue = configRecord.default_till_missed;
            SpinEdit_chks_per_invoice.EditValue = configRecord.chks_per_invoice;
            CheckEdit_rpps_resend_msg.EditValue = configRecord.rpps_resend_msg;
            LookUpEdit_ComparisonPercent.EditValue = configRecord.ComparisonPercent;
            CalcEdit_ComparisonAmount.EditValue = configRecord.ComparisonAmount;
            PercentEdit_ComparisonRate.EditValue = configRecord.ComparisonRate;

            // Read the list of config fees
            ConfigFees1.ReadForm(bc);
        }

        /// <summary>
        /// Do not allow the spin values to go negative
        /// </summary>
        private void SpinEdit_EditValueChanging(object sender, ChangingEventArgs e)
        {
            System.Nullable<Int32> v = DebtPlus.Utils.Nulls.v_Int32(e.NewValue);
            if (v.HasValue && v.Value < 0)
            {
                e.Cancel = true;
            }
        }

        /// <summary>
        /// Validate the web site to ensure that it will be legal for external references
        /// </summary>
        private void TextEdit_web_site_Validating(object sender, CancelEventArgs e)
        {
            // Accept a null value
            if (TextEdit_web_site.EditValue == null)
            {
                return;
            }

            // Accept an empty value as being null
            if (string.IsNullOrWhiteSpace(TextEdit_web_site.Text))
            {
                TextEdit_web_site.EditValue = null;
                return;
            }

            // Try to determine the validity of the website
            try
            {
                // Try to parse the input string into a URI. If this does not work then the entry is not legal.
                var uriSite = new System.Uri(TextEdit_web_site.Text);

                // The site must not be a file nor loopback reference
                if (uriSite.IsFile || uriSite.IsLoopback || !uriSite.IsAbsoluteUri)
                {
                    e.Cancel = true;
                    TextEdit_web_site.ErrorText = "Site must not be a file reference, nor look-back, and must not be a partial reference";
                    return;
                }

                // The scheme must be HTTP or HTTPS.
                if (uriSite.Scheme == System.Uri.UriSchemeHttp || uriSite.Scheme == System.Uri.UriSchemeHttps)
                {
                    // Finally, there must not be any entry in the query text field.
                    if (string.IsNullOrWhiteSpace(uriSite.Query))
                    {
                        TextEdit_web_site.ErrorText = string.Empty;
                        return;
                    }
                }
            }
            catch { }

            // The site reference is not valid.
            e.Cancel = true;
            TextEdit_web_site.ErrorText = "Website must be absolute with HTTP or HTTPS protocol.";
        }

        /// <summary>
        /// Process the FILE -> EXIT function from the menu.
        /// </summary>
        private void BarButtonItem_File_Exit_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            Close();
        }
    }
}