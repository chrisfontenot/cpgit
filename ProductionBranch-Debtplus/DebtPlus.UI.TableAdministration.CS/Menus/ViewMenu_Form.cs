#region "Copyright 2000-2012 DebtPlus, L.L.C."

//{*******************************************************************}
//{                                                                   }
//{       DebtPlus Debt Management System                             }
//{                                                                   }
//{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
//{       ALL RIGHTS RESERVED                                         }
//{                                                                   }
//{   The entire contents of this file is protected by U.S. and       }
//{   International Copyright Laws. Unauthorized reproduction,        }
//{   reverse-engineering, and distribution of all or any portion of  }
//{   the code contained in this file is strictly prohibited and may  }
//{   result in severe civil and criminal penalties and will be       }
//{   prosecuted to the maximum extent possible under the law.        }
//{                                                                   }
//{   RESTRICTIONS                                                    }
//{                                                                   }
//{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
//{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
//{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
//{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
//{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
//{                                                                   }
//{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
//{   ADDITIONAL RESTRICTIONS.                                        }
//{                                                                   }
//{*******************************************************************}

#endregion "Copyright 2000-2012 DebtPlus, L.L.C."

using System;
using System.Drawing;
using System.Linq;

namespace DebtPlus.UI.TableAdministration.CS.Menus
{
    internal partial class ViewMenu_Form
    {
        private System.Collections.Generic.List<DebtPlus.LINQ.menus> colItems = null;

        public class EditItemEventArgs : System.EventArgs
        {
            public EditItemEventArgs()
                : base()
            {
            }

            public DebtPlus.LINQ.menus editItem { get; set; }
        }

        public delegate void EditItemEventHandler(object sender, EditItemEventArgs e);
        public event EditItemEventHandler EditItem;

        protected void RaiseEditItem(EditItemEventArgs e)
        {
            var evt = EditItem;
            if (evt != null)
            {
                evt(this, e);
            }
        }

        protected virtual void OnEditItem(DebtPlus.LINQ.menus Item)
        {
            RaiseEditItem(new EditItemEventArgs() { editItem = Item });
        }

        /// <summary>
        /// Initialize the class
        /// </summary>
        /// <param name="dc">Pointer to the data context for the program</param>
        internal ViewMenu_Form(System.Collections.Generic.List<DebtPlus.LINQ.menus> colItems)
            : base()
        {
            this.colItems = colItems;
            InitializeComponent();

            SpinEdit1.EditValue = 9;
            TreeList1.ParentFieldName = "parentSeq";
            TreeList1.KeyFieldName = "Id";
            TreeListColumn_text.Caption = "Menu Layout for this level of user";

            RegisterHandlers();
        }

        /// <summary>
        /// Register the event handlers
        /// </summary>
        private void RegisterHandlers()
        {
            Load += ViewMenu_Form_Load;
            SpinEdit1.EditValueChanged += SpinEdit1_EditValueChanged;
            SpinEdit1.EditValueChanging += SpinEdit1_EditValueChanging;
            TreeList1.MouseDoubleClick += TreeList1_MouseDoubleClick;
        }

        /// <summary>
        /// Remove the event handlers
        /// </summary>
        private void UnRegisterHandlers()
        {
            this.Load -= ViewMenu_Form_Load;
            SpinEdit1.EditValueChanged -= SpinEdit1_EditValueChanged;
            SpinEdit1.EditValueChanging -= SpinEdit1_EditValueChanging;
            TreeList1.MouseDoubleClick -= TreeList1_MouseDoubleClick;
        }

        /// <summary>
        /// Process the LOAD event for the form
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ViewMenu_Form_Load(System.Object sender, System.EventArgs e)
        {
            RebuildLayout();
            TreeList1.CollapseAll();
        }

        /// <summary>
        /// Handle the change in the spin control
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void SpinEdit1_EditValueChanged(object sender, System.EventArgs e)
        {
            RebuildLayout();
        }

        /// <summary>
        /// Handle the spin control being changed. Ensure that the limits are being respected.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void SpinEdit1_EditValueChanging(object sender, DevExpress.XtraEditors.Controls.ChangingEventArgs e)
        {
            if (e.NewValue == null)
            {
                e.Cancel = true;
                return;
            }

            if (Convert.ToInt32(e.NewValue) < 0 || Convert.ToInt32(e.NewValue) > 9)
            {
                e.Cancel = true;
            }
        }

        /// <summary>
        /// Rebuild the grid layout for a new menu level
        /// </summary>
        private void RebuildLayout()
        {
            System.Int32 MenuLevel = Convert.ToInt32(SpinEdit1.EditValue);

            // Bind the data to the display control
            TreeList1.DataSource = colItems.Where(s => s.menu_level >= MenuLevel).OrderBy(s => s.Id).ToList();
            TreeList1.RefreshDataSource();
        }

        /// <summary>
        /// Handle the double-click event on the grid
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void TreeList1_MouseDoubleClick(object sender, System.Windows.Forms.MouseEventArgs e)
        {
            ProcessHit(TreeList1.CalcHitInfo(new Point(e.X, e.Y)));
        }

        /// <summary>
        /// Handle the HIT event on the tree-list
        /// </summary>
        /// <param name="hi"></param>
        private void ProcessHit(DevExpress.XtraTreeList.TreeListHitInfo hi)
        {
            if (hi.Node != null)
            {
                // Find the record to be edited and call the edit routine
                DebtPlus.LINQ.menus record = TreeList1.GetDataRecordByNode(hi.Node) as DebtPlus.LINQ.menus;
                if (record != null)
                {
                    OnEditItem(record);
                    RebuildLayout();
                }
            }
        }
    }
}