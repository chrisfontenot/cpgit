﻿using Microsoft.VisualBasic;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Diagnostics;

namespace DebtPlus.UI.TableAdministration.CS.Menus
{
    partial class EditForm
    {
		//Form overrides dispose to clean up the component list.
		[System.Diagnostics.DebuggerNonUserCode]
		protected override void Dispose(bool disposing)
		{
			try 
            {
				if (disposing)
                {
                    if (components != null) components.Dispose();
				}
                components = null;
            }
    	    finally
            {
				base.Dispose(disposing);
			}
		}

		//Required by the Windows Form Designer
		private System.ComponentModel.IContainer components = null;

        //NOTE: The following procedure is required by the Windows Form Designer
        //It can be modified using the Windows Form Designer.  
        //Do not modify it using the code editor.
        [System.Diagnostics.DebuggerStepThrough()]
        private void InitializeComponent()
        {
            this.LabelControl1 = new DevExpress.XtraEditors.LabelControl();
            this.LabelControl2 = new DevExpress.XtraEditors.LabelControl();
            this.TextEdit_menu = new DevExpress.XtraEditors.TextEdit();
            this.TextEdit_text = new DevExpress.XtraEditors.TextEdit();
            this.LabelControl_type = new DevExpress.XtraEditors.LabelControl();
            this.CheckEdit_attributes = new DevExpress.XtraEditors.CheckEdit();
            this.TextEdit_comment = new DevExpress.XtraEditors.TextEdit();
            this.LabelControl3 = new DevExpress.XtraEditors.LabelControl();
            this.SpinEdit_menu_level = new DevExpress.XtraEditors.SpinEdit();
            this.LabelControl4 = new DevExpress.XtraEditors.LabelControl();
            this.LabelControl5 = new DevExpress.XtraEditors.LabelControl();
            this.TextEdit_ParentSeq = new DevExpress.XtraEditors.TextEdit();
            this.LabelControl_ParentName = new DevExpress.XtraEditors.LabelControl();
            this.TextEdit_type = new DevExpress.XtraEditors.TextEdit();
            ((System.ComponentModel.ISupportInitialize)(this.DxErrorProvider1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TextEdit_menu.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TextEdit_text.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.CheckEdit_attributes.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TextEdit_comment.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.SpinEdit_menu_level.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TextEdit_ParentSeq.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TextEdit_type.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // simpleButton_OK
            // 
            this.simpleButton_OK.Enabled = true;
            this.simpleButton_OK.Location = new System.Drawing.Point(483, 34);
            this.simpleButton_OK.TabIndex = 14;
            // 
            // simpleButton_Cancel
            // 
            this.simpleButton_Cancel.Location = new System.Drawing.Point(483, 77);
            this.simpleButton_Cancel.TabIndex = 15;
            // 
            // DefaultLookAndFeel1
            // 
            this.DefaultLookAndFeel1.LookAndFeel.SkinName = "McSkin";
            // 
            // LabelControl1
            // 
            this.LabelControl1.Location = new System.Drawing.Point(13, 15);
            this.LabelControl1.Name = "LabelControl1";
            this.LabelControl1.Size = new System.Drawing.Size(32, 13);
            this.LabelControl1.TabIndex = 0;
            this.LabelControl1.Text = "Parent";
            // 
            // LabelControl2
            // 
            this.LabelControl2.Location = new System.Drawing.Point(13, 68);
            this.LabelControl2.Name = "LabelControl2";
            this.LabelControl2.Size = new System.Drawing.Size(53, 13);
            this.LabelControl2.TabIndex = 5;
            this.LabelControl2.Text = "Description";
            // 
            // TextEdit_menu
            // 
            this.TextEdit_menu.Location = new System.Drawing.Point(95, 38);
            this.TextEdit_menu.Name = "TextEdit_menu";
            this.TextEdit_menu.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.False;
            this.TextEdit_menu.Properties.Appearance.Options.UseTextOptions = true;
            this.TextEdit_menu.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.TextEdit_menu.Properties.Mask.BeepOnError = true;
            this.TextEdit_menu.Properties.Mask.EditMask = "\\d+";
            this.TextEdit_menu.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.RegEx;
            this.TextEdit_menu.Properties.MaxLength = 10;
            this.TextEdit_menu.Size = new System.Drawing.Size(99, 20);
            this.TextEdit_menu.TabIndex = 4;
            // 
            // TextEdit_text
            // 
            this.TextEdit_text.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.TextEdit_text.Location = new System.Drawing.Point(95, 64);
            this.TextEdit_text.Name = "TextEdit_text";
            this.TextEdit_text.Properties.MaxLength = 512;
            this.TextEdit_text.Size = new System.Drawing.Size(362, 20);
            this.TextEdit_text.TabIndex = 6;
            // 
            // LabelControl_type
            // 
            this.LabelControl_type.Location = new System.Drawing.Point(12, 41);
            this.LabelControl_type.Name = "LabelControl_type";
            this.LabelControl_type.Size = new System.Drawing.Size(47, 13);
            this.LabelControl_type.TabIndex = 3;
            this.LabelControl_type.Text = "Sequence";
            // 
            // CheckEdit_attributes
            // 
            this.CheckEdit_attributes.Location = new System.Drawing.Point(11, 182);
            this.CheckEdit_attributes.Name = "CheckEdit_attributes";
            this.CheckEdit_attributes.Properties.Caption = "Menu Group Heading";
            this.CheckEdit_attributes.Properties.NullStyle = DevExpress.XtraEditors.Controls.StyleIndeterminate.Unchecked;
            this.CheckEdit_attributes.Properties.ValueChecked = 1;
            this.CheckEdit_attributes.Properties.ValueUnchecked = 0;
            this.CheckEdit_attributes.Size = new System.Drawing.Size(136, 20);
            this.CheckEdit_attributes.TabIndex = 13;
            // 
            // TextEdit_comment
            // 
            this.TextEdit_comment.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.TextEdit_comment.Location = new System.Drawing.Point(95, 116);
            this.TextEdit_comment.Name = "TextEdit_comment";
            this.TextEdit_comment.Properties.MaxLength = 2048;
            this.TextEdit_comment.Size = new System.Drawing.Size(362, 20);
            this.TextEdit_comment.TabIndex = 10;
            // 
            // LabelControl3
            // 
            this.LabelControl3.Location = new System.Drawing.Point(13, 121);
            this.LabelControl3.Name = "LabelControl3";
            this.LabelControl3.Size = new System.Drawing.Size(60, 13);
            this.LabelControl3.TabIndex = 9;
            this.LabelControl3.Text = "Argument(s)";
            // 
            // SpinEdit_menu_level
            // 
            this.SpinEdit_menu_level.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.SpinEdit_menu_level.Location = new System.Drawing.Point(95, 142);
            this.SpinEdit_menu_level.Name = "SpinEdit_menu_level";
            this.SpinEdit_menu_level.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.False;
            this.SpinEdit_menu_level.Properties.Appearance.Options.UseTextOptions = true;
            this.SpinEdit_menu_level.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.SpinEdit_menu_level.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.SpinEdit_menu_level.Properties.DisplayFormat.FormatString = "{0:f0}";
            this.SpinEdit_menu_level.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.SpinEdit_menu_level.Properties.EditFormat.FormatString = "{0:f0}";
            this.SpinEdit_menu_level.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.SpinEdit_menu_level.Properties.IsFloatValue = false;
            this.SpinEdit_menu_level.Properties.Mask.EditMask = "\\d+";
            this.SpinEdit_menu_level.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.RegEx;
            this.SpinEdit_menu_level.Properties.MaxLength = 5;
            this.SpinEdit_menu_level.Properties.MaxValue = new decimal(new int[] {
            32767,
            0,
            0,
            0});
            this.SpinEdit_menu_level.Properties.NullText = "0";
            this.SpinEdit_menu_level.Size = new System.Drawing.Size(40, 20);
            this.SpinEdit_menu_level.TabIndex = 12;
            // 
            // LabelControl4
            // 
            this.LabelControl4.Location = new System.Drawing.Point(13, 147);
            this.LabelControl4.Name = "LabelControl4";
            this.LabelControl4.Size = new System.Drawing.Size(76, 13);
            this.LabelControl4.TabIndex = 11;
            this.LabelControl4.Text = "Counselor Level";
            // 
            // LabelControl5
            // 
            this.LabelControl5.Location = new System.Drawing.Point(13, 94);
            this.LabelControl5.Name = "LabelControl5";
            this.LabelControl5.Size = new System.Drawing.Size(55, 13);
            this.LabelControl5.TabIndex = 7;
            this.LabelControl5.Text = "Class Name";
            // 
            // TextEdit_ParentSeq
            // 
            this.TextEdit_ParentSeq.Location = new System.Drawing.Point(95, 12);
            this.TextEdit_ParentSeq.Name = "TextEdit_ParentSeq";
            this.TextEdit_ParentSeq.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.False;
            this.TextEdit_ParentSeq.Properties.Appearance.Options.UseTextOptions = true;
            this.TextEdit_ParentSeq.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.TextEdit_ParentSeq.Properties.Mask.BeepOnError = true;
            this.TextEdit_ParentSeq.Properties.Mask.EditMask = "\\d+";
            this.TextEdit_ParentSeq.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.RegEx;
            this.TextEdit_ParentSeq.Properties.MaxLength = 10;
            this.TextEdit_ParentSeq.Size = new System.Drawing.Size(99, 20);
            this.TextEdit_ParentSeq.TabIndex = 1;
            // 
            // LabelControl_ParentName
            // 
            this.LabelControl_ParentName.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.Horizontal;
            this.LabelControl_ParentName.Location = new System.Drawing.Point(200, 15);
            this.LabelControl_ParentName.Name = "LabelControl_ParentName";
            this.LabelControl_ParentName.Size = new System.Drawing.Size(0, 13);
            this.LabelControl_ParentName.TabIndex = 2;
            this.LabelControl_ParentName.UseMnemonic = false;
            // 
            // TextEdit_type
            // 
            this.TextEdit_type.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.TextEdit_type.Location = new System.Drawing.Point(95, 90);
            this.TextEdit_type.Name = "TextEdit_type";
            this.TextEdit_type.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.True;
            this.TextEdit_type.Properties.Appearance.Options.UseTextOptions = true;
            this.TextEdit_type.Properties.Appearance.TextOptions.Trimming = DevExpress.Utils.Trimming.EllipsisCharacter;
            this.TextEdit_type.Properties.MaxLength = 512;
            this.TextEdit_type.Size = new System.Drawing.Size(362, 20);
            this.TextEdit_type.TabIndex = 8;
            // 
            // EditForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(579, 213);
            this.Controls.Add(this.LabelControl_ParentName);
            this.Controls.Add(this.TextEdit_ParentSeq);
            this.Controls.Add(this.LabelControl5);
            this.Controls.Add(this.SpinEdit_menu_level);
            this.Controls.Add(this.LabelControl4);
            this.Controls.Add(this.TextEdit_comment);
            this.Controls.Add(this.LabelControl3);
            this.Controls.Add(this.CheckEdit_attributes);
            this.Controls.Add(this.LabelControl_type);
            this.Controls.Add(this.TextEdit_menu);
            this.Controls.Add(this.TextEdit_text);
            this.Controls.Add(this.LabelControl2);
            this.Controls.Add(this.LabelControl1);
            this.Controls.Add(this.TextEdit_type);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.Sizable;
            this.Name = "EditForm";
            this.Text = "Menu Item";
            this.Controls.SetChildIndex(this.TextEdit_type, 0);
            this.Controls.SetChildIndex(this.LabelControl1, 0);
            this.Controls.SetChildIndex(this.LabelControl2, 0);
            this.Controls.SetChildIndex(this.TextEdit_text, 0);
            this.Controls.SetChildIndex(this.TextEdit_menu, 0);
            this.Controls.SetChildIndex(this.simpleButton_OK, 0);
            this.Controls.SetChildIndex(this.simpleButton_Cancel, 0);
            this.Controls.SetChildIndex(this.LabelControl_type, 0);
            this.Controls.SetChildIndex(this.CheckEdit_attributes, 0);
            this.Controls.SetChildIndex(this.LabelControl3, 0);
            this.Controls.SetChildIndex(this.TextEdit_comment, 0);
            this.Controls.SetChildIndex(this.LabelControl4, 0);
            this.Controls.SetChildIndex(this.SpinEdit_menu_level, 0);
            this.Controls.SetChildIndex(this.LabelControl5, 0);
            this.Controls.SetChildIndex(this.TextEdit_ParentSeq, 0);
            this.Controls.SetChildIndex(this.LabelControl_ParentName, 0);
            ((System.ComponentModel.ISupportInitialize)(this.DxErrorProvider1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TextEdit_menu.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TextEdit_text.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.CheckEdit_attributes.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TextEdit_comment.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.SpinEdit_menu_level.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TextEdit_ParentSeq.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TextEdit_type.Properties)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }
        private DevExpress.XtraEditors.LabelControl LabelControl1;
        private DevExpress.XtraEditors.LabelControl LabelControl2;
        private DevExpress.XtraEditors.TextEdit TextEdit_menu;
        private DevExpress.XtraEditors.TextEdit TextEdit_text;
        private DevExpress.XtraEditors.LabelControl LabelControl_type;
        private DevExpress.XtraEditors.CheckEdit CheckEdit_attributes;
        private DevExpress.XtraEditors.TextEdit TextEdit_comment;
        private DevExpress.XtraEditors.LabelControl LabelControl3;
        private DevExpress.XtraEditors.SpinEdit SpinEdit_menu_level;
        private DevExpress.XtraEditors.LabelControl LabelControl4;
        private DevExpress.XtraEditors.LabelControl LabelControl5;
        private DevExpress.XtraEditors.TextEdit TextEdit_ParentSeq;
        private DevExpress.XtraEditors.LabelControl LabelControl_ParentName;
        private DevExpress.XtraEditors.TextEdit TextEdit_type;
    }
}
