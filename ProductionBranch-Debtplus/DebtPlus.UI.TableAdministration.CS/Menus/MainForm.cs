#region "Copyright 2000-2012 DebtPlus, L.L.C."

//{*******************************************************************}
//{                                                                   }
//{       DebtPlus Debt Management System                             }
//{                                                                   }
//{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
//{       ALL RIGHTS RESERVED                                         }
//{                                                                   }
//{   The entire contents of this file is protected by U.S. and       }
//{   International Copyright Laws. Unauthorized reproduction,        }
//{   reverse-engineering, and distribution of all or any portion of  }
//{   the code contained in this file is strictly prohibited and may  }
//{   result in severe civil and criminal penalties and will be       }
//{   prosecuted to the maximum extent possible under the law.        }
//{                                                                   }
//{   RESTRICTIONS                                                    }
//{                                                                   }
//{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
//{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
//{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
//{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
//{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
//{                                                                   }
//{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
//{   ADDITIONAL RESTRICTIONS.                                        }
//{                                                                   }
//{*******************************************************************}

#endregion "Copyright 2000-2012 DebtPlus, L.L.C."

using System;
using System.Linq;
using DebtPlus.LINQ;

namespace DebtPlus.UI.TableAdministration.CS.Menus
{
    public partial class MainForm : Templates.MainForm
    {
        /// <summary>
        /// List of the records to be edited in the program
        /// </summary>
        private System.Collections.Generic.List<DebtPlus.LINQ.menus> colRecords;

        private BusinessContext bc = new BusinessContext();
        private Int32 menuLevel = 0;

        /// <summary>
        /// Initialize the new class
        /// </summary>
        /// <param name="dc">Pointer to the data context in the mainline</param>
        public MainForm()
            : base()
        {
            InitializeComponent();
            RegisterHandlers();
        }

        private void RegisterHandlers()
        {
            Load += MainForm_Load;
            barButton_File_Exit.ItemClick += barButton_File_Exit_ItemClick;
            barButtonItem_View_Tree.ItemClick += barButtonItem_View_Tree_ItemClick;
        }

        private void UnRegisterHandlers()
        {
            Load -= MainForm_Load;
            barButton_File_Exit.ItemClick -= barButton_File_Exit_ItemClick;
            barButtonItem_View_Tree.ItemClick -= barButtonItem_View_Tree_ItemClick;
        }

        /// <summary>
        /// Process the load event on the form
        /// </summary>
        private void MainForm_Load(object sender, System.EventArgs e)
        {
            try
            {
                using (var cm = new DebtPlus.UI.Common.CursorManager())
                {
                    colRecords = bc.menus.ToList();
                    gridControl1.DataSource = colRecords.Where(s => s.menu_level >= menuLevel).ToList();
                    gridControl1.RefreshDataSource();
                }
                gridView1.RefreshData();
                gridView1.BestFitColumns();
            }
            catch (System.Data.SqlClient.SqlException ex)
            {
                DebtPlus.UI.Common.ErrorHandling.HandleErrors(ex);
            }
        }

        /// <summary>
        /// Handle the edit of the information on the form
        /// </summary>
        protected override void UpdateRecord(object obj)
        {
            var record = obj as DebtPlus.LINQ.menus;
            if (record == null)
            {
                return;
            }

            Int32 priorID = record.Id;
            using (var frm = new EditForm(colRecords, record))
            {
                if (frm.ShowDialog() != System.Windows.Forms.DialogResult.OK)
                {
                    return;
                }
            }

            // If the ID has changed then we need to delete the previous item and insert the new one.
            if (record.Id != priorID)
            {
                var qDel = bc.menus.Where(s => s.Id == priorID).FirstOrDefault();
                if (qDel != null)
                {
                    bc.menus.DeleteOnSubmit(qDel);
                }
                colRecords.Remove(record);

                var newRecord = new DebtPlus.LINQ.menus()
                {
                    argument = record.argument,
                    attributes = record.attributes,
                    comment = record.comment,
                    dotNet_program_key = record.dotNet_program_key,
                    Id = record.Id,
                    menu_level = record.menu_level,
                    parentSeq = record.parentSeq,
                    text = record.text,
                    type = record.type
                };

                bc.menus.InsertOnSubmit(newRecord);
                colRecords.Add(newRecord);
            }

            // Update the database
            try
            {
                bc.SubmitChanges();
                gridView1.RefreshData();
            }
            catch (System.Data.SqlClient.SqlException ex)
            {
                DebtPlus.UI.Common.ErrorHandling.HandleErrors(ex);
            }
        }

        /// <summary>
        /// Create the new record
        /// </summary>
        protected override void CreateRecord()
        {
            var record = new DebtPlus.LINQ.menus() { Id = default(Int32), argument = null, attributes = null, comment = null, dotNet_program_key = null, menu_level = 9, parentSeq = 0, text = null, type = null }; // DebtPlus.LINQ.Factory.Manufacture_menu();

            // Edit the new record
            using (var frm = new EditForm(colRecords, record))
            {
                if (frm.ShowDialog() != System.Windows.Forms.DialogResult.OK)
                {
                    return;
                }
            }

            // Update the database
            colRecords.Add(record);
            bc.menus.InsertOnSubmit(record);

            try
            {
                bc.SubmitChanges();
                gridView1.RefreshData();
            }
            catch (System.Data.SqlClient.SqlException ex)
            {
                DebtPlus.UI.Common.ErrorHandling.HandleErrors(ex);
            }
        }

        /// <summary>
        /// Delete the indicated record from the database
        /// </summary>
        /// <param name="obj">Pointer to the desired record for the operation</param>
        protected override void DeleteRecord(object obj)
        {
            DebtPlus.LINQ.menus record = obj as DebtPlus.LINQ.menus;
            if (record == null)
            {
                return;
            }

            if (DebtPlus.Data.Prompts.RequestConfirmation_Delete() != System.Windows.Forms.DialogResult.Yes)
            {
                return;
            }

            bc.menus.DeleteOnSubmit(record);
            colRecords.Remove(record);

            // Update the database
            try
            {
                bc.SubmitChanges();
                gridView1.RefreshData();
            }
            catch (System.Data.SqlClient.SqlException ex)
            {
                DebtPlus.UI.Common.ErrorHandling.HandleErrors(ex);
            }
        }

        /// <summary>
        /// FILE->CLOSE menu
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void barButton_File_Exit_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            Close();
        }

        /// <summary>
        /// Process the VIEW -> TREE menu item
        /// </summary>
        private void barButtonItem_View_Tree_ItemClick(System.Object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            using (var frm = new ViewMenu_Form(colRecords))
            {
                frm.EditItem += frm_MenuLevel;
                frm.ShowDialog();
                frm.EditItem -= frm_MenuLevel;
            }
        }

        /// <summary>
        /// Handle the condition where we are to edit the current item
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void frm_MenuLevel(object sender, ViewMenu_Form.EditItemEventArgs e)
        {
            UpdateRecord(e.editItem);
        }
    }
}