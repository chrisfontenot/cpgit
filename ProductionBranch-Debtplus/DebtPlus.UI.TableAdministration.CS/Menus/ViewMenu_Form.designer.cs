﻿using Microsoft.VisualBasic;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Diagnostics;

namespace DebtPlus.UI.TableAdministration.CS.Menus
{
    partial class ViewMenu_Form : DebtPlus.Data.Forms.DebtPlusForm
    {
        //Form overrides dispose to clean up the component list.
        [System.Diagnostics.DebuggerNonUserCode]
        protected override void Dispose(bool disposing)
        {
            try
            {
                if (disposing && components != null)
                {
                    components.Dispose();
                }
            }
            finally
            {
                base.Dispose(disposing);
            }
        }

        //Required by the Windows Form Designer

        private System.ComponentModel.IContainer components = null;
        //NOTE: The following procedure is required by the Windows Form Designer
        //It can be modified using the Windows Form Designer.  
        //Do not modify it using the code editor.
        [System.Diagnostics.DebuggerStepThrough()]
        private void InitializeComponent()
        {
            DevExpress.XtraTreeList.StyleFormatConditions.StyleFormatCondition StyleFormatCondition1 = new DevExpress.XtraTreeList.StyleFormatConditions.StyleFormatCondition();
            this.TreeListColumn_attributes = new DevExpress.XtraTreeList.Columns.TreeListColumn();
            this.LabelControl1 = new DevExpress.XtraEditors.LabelControl();
            this.SpinEdit1 = new DevExpress.XtraEditors.SpinEdit();
            this.TreeList1 = new DevExpress.XtraTreeList.TreeList();
            this.TreeListColumn_seq = new DevExpress.XtraTreeList.Columns.TreeListColumn();
            this.TreeListColumn_parentseq = new DevExpress.XtraTreeList.Columns.TreeListColumn();
            this.TreeListColumn_text = new DevExpress.XtraTreeList.Columns.TreeListColumn();
            this.TreeListColumn_type = new DevExpress.XtraTreeList.Columns.TreeListColumn();
            this.TreeListColumn_program_key = new DevExpress.XtraTreeList.Columns.TreeListColumn();
            this.SimpleButton1 = new DevExpress.XtraEditors.SimpleButton();
            ((System.ComponentModel.ISupportInitialize)this.DxErrorProvider1).BeginInit();
            ((System.ComponentModel.ISupportInitialize)this.SpinEdit1.Properties).BeginInit();
            ((System.ComponentModel.ISupportInitialize)this.TreeList1).BeginInit();
            this.SuspendLayout();
            //
            //TreeListColumn_attributes
            //
            this.TreeListColumn_attributes.Caption = "Attributes";
            this.TreeListColumn_attributes.CustomizationCaption = "Attributes";
            this.TreeListColumn_attributes.FieldName = "attributes";
            this.TreeListColumn_attributes.Name = "TreeListColumn_attributes";
            this.TreeListColumn_attributes.OptionsColumn.AllowEdit = false;
            //
            //LabelControl1
            //
            this.LabelControl1.Location = new System.Drawing.Point(12, 12);
            this.LabelControl1.Name = "LabelControl1";
            this.LabelControl1.Size = new System.Drawing.Size(54, 13);
            this.LabelControl1.TabIndex = 0;
            this.LabelControl1.Text = "Menu Level";
            //
            //SpinEdit1
            //
            this.SpinEdit1.EditValue = new decimal(new int[] {0,0,0,0});
            this.SpinEdit1.Location = new System.Drawing.Point(72, 9);
            this.SpinEdit1.Name = "SpinEdit1";
            this.SpinEdit1.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] { new DevExpress.XtraEditors.Controls.EditorButton() });
            this.SpinEdit1.Properties.IsFloatValue = false;
            this.SpinEdit1.Properties.Mask.EditMask = "N00";
            this.SpinEdit1.Size = new System.Drawing.Size(54, 20);
            this.SpinEdit1.TabIndex = 1;
            //
            //TreeList1
            //
            this.TreeList1.Anchor = (System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) | System.Windows.Forms.AnchorStyles.Left) | System.Windows.Forms.AnchorStyles.Right);
            this.TreeList1.Appearance.Empty.BackColor = System.Drawing.Color.White;
            this.TreeList1.Appearance.Empty.Options.UseBackColor = true;
            this.TreeList1.Appearance.EvenRow.BackColor = System.Drawing.Color.FromArgb(231, 242, 254);
            this.TreeList1.Appearance.EvenRow.ForeColor = System.Drawing.Color.Black;
            this.TreeList1.Appearance.EvenRow.Options.UseBackColor = true;
            this.TreeList1.Appearance.EvenRow.Options.UseForeColor = true;
            this.TreeList1.Appearance.FocusedCell.BackColor = System.Drawing.Color.White;
            this.TreeList1.Appearance.FocusedCell.ForeColor = System.Drawing.Color.Black;
            this.TreeList1.Appearance.FocusedCell.Options.UseBackColor = true;
            this.TreeList1.Appearance.FocusedCell.Options.UseForeColor = true;
            this.TreeList1.Appearance.FocusedRow.BackColor = System.Drawing.Color.FromArgb(49, 106, 197);
            this.TreeList1.Appearance.FocusedRow.ForeColor = System.Drawing.Color.White;
            this.TreeList1.Appearance.FocusedRow.Options.UseBackColor = true;
            this.TreeList1.Appearance.FocusedRow.Options.UseForeColor = true;
            this.TreeList1.Appearance.FooterPanel.BackColor = System.Drawing.Color.FromArgb(221, 236, 254);
            this.TreeList1.Appearance.FooterPanel.BackColor2 = System.Drawing.Color.FromArgb(132, 171, 228);
            this.TreeList1.Appearance.FooterPanel.BorderColor = System.Drawing.Color.FromArgb(221, 236, 254);
            this.TreeList1.Appearance.FooterPanel.ForeColor = System.Drawing.Color.Black;
            this.TreeList1.Appearance.FooterPanel.GradientMode = System.Drawing.Drawing2D.LinearGradientMode.Vertical;
            this.TreeList1.Appearance.FooterPanel.Options.UseBackColor = true;
            this.TreeList1.Appearance.FooterPanel.Options.UseBorderColor = true;
            this.TreeList1.Appearance.FooterPanel.Options.UseForeColor = true;
            this.TreeList1.Appearance.GroupButton.BackColor = System.Drawing.Color.FromArgb(193, 216, 247);
            this.TreeList1.Appearance.GroupButton.BorderColor = System.Drawing.Color.FromArgb(193, 216, 247);
            this.TreeList1.Appearance.GroupButton.ForeColor = System.Drawing.Color.Black;
            this.TreeList1.Appearance.GroupButton.Options.UseBackColor = true;
            this.TreeList1.Appearance.GroupButton.Options.UseBorderColor = true;
            this.TreeList1.Appearance.GroupButton.Options.UseForeColor = true;
            this.TreeList1.Appearance.GroupFooter.BackColor = System.Drawing.Color.FromArgb(193, 216, 247);
            this.TreeList1.Appearance.GroupFooter.BorderColor = System.Drawing.Color.FromArgb(193, 216, 247);
            this.TreeList1.Appearance.GroupFooter.ForeColor = System.Drawing.Color.Black;
            this.TreeList1.Appearance.GroupFooter.Options.UseBackColor = true;
            this.TreeList1.Appearance.GroupFooter.Options.UseBorderColor = true;
            this.TreeList1.Appearance.GroupFooter.Options.UseForeColor = true;
            this.TreeList1.Appearance.HeaderPanel.BackColor = System.Drawing.Color.FromArgb(221, 236, 254);
            this.TreeList1.Appearance.HeaderPanel.BackColor2 = System.Drawing.Color.FromArgb(132, 171, 228);
            this.TreeList1.Appearance.HeaderPanel.BorderColor = System.Drawing.Color.FromArgb(221, 236, 254);
            this.TreeList1.Appearance.HeaderPanel.ForeColor = System.Drawing.Color.Black;
            this.TreeList1.Appearance.HeaderPanel.GradientMode = System.Drawing.Drawing2D.LinearGradientMode.Vertical;
            this.TreeList1.Appearance.HeaderPanel.Options.UseBackColor = true;
            this.TreeList1.Appearance.HeaderPanel.Options.UseBorderColor = true;
            this.TreeList1.Appearance.HeaderPanel.Options.UseForeColor = true;
            this.TreeList1.Appearance.HideSelectionRow.BackColor = System.Drawing.Color.FromArgb(106, 153, 228);
            this.TreeList1.Appearance.HideSelectionRow.ForeColor = System.Drawing.Color.FromArgb(208, 224, 251);
            this.TreeList1.Appearance.HideSelectionRow.Options.UseBackColor = true;
            this.TreeList1.Appearance.HideSelectionRow.Options.UseForeColor = true;
            this.TreeList1.Appearance.HorzLine.BackColor = System.Drawing.Color.FromArgb(99, 127, 196);
            this.TreeList1.Appearance.HorzLine.Options.UseBackColor = true;
            this.TreeList1.Appearance.OddRow.BackColor = System.Drawing.Color.White;
            this.TreeList1.Appearance.OddRow.ForeColor = System.Drawing.Color.Black;
            this.TreeList1.Appearance.OddRow.Options.UseBackColor = true;
            this.TreeList1.Appearance.OddRow.Options.UseForeColor = true;
            this.TreeList1.Appearance.Preview.BackColor = System.Drawing.Color.FromArgb(249, 252, 255);
            this.TreeList1.Appearance.Preview.ForeColor = System.Drawing.Color.FromArgb(88, 129, 185);
            this.TreeList1.Appearance.Preview.Options.UseBackColor = true;
            this.TreeList1.Appearance.Preview.Options.UseForeColor = true;
            this.TreeList1.Appearance.Row.BackColor = System.Drawing.Color.White;
            this.TreeList1.Appearance.Row.ForeColor = System.Drawing.Color.Black;
            this.TreeList1.Appearance.Row.Options.UseBackColor = true;
            this.TreeList1.Appearance.Row.Options.UseForeColor = true;
            this.TreeList1.Appearance.SelectedRow.BackColor = System.Drawing.Color.FromArgb(69, 126, 217);
            this.TreeList1.Appearance.SelectedRow.ForeColor = System.Drawing.Color.White;
            this.TreeList1.Appearance.SelectedRow.Options.UseBackColor = true;
            this.TreeList1.Appearance.SelectedRow.Options.UseForeColor = true;
            this.TreeList1.Appearance.TreeLine.BackColor = System.Drawing.Color.FromArgb(59, 97, 156);
            this.TreeList1.Appearance.TreeLine.Options.UseBackColor = true;
            this.TreeList1.Appearance.VertLine.BackColor = System.Drawing.Color.FromArgb(99, 127, 196);
            this.TreeList1.Appearance.VertLine.Options.UseBackColor = true;
            this.TreeList1.Columns.AddRange(new DevExpress.XtraTreeList.Columns.TreeListColumn[] {
			this.TreeListColumn_seq,
			this.TreeListColumn_parentseq,
			this.TreeListColumn_text,
			this.TreeListColumn_attributes,
			this.TreeListColumn_type,
			this.TreeListColumn_program_key
		});
            StyleFormatCondition1.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25f, System.Drawing.FontStyle.Bold);
            StyleFormatCondition1.Appearance.Options.UseFont = true;
            StyleFormatCondition1.ApplyToRow = true;
            StyleFormatCondition1.Column = this.TreeListColumn_attributes;
            StyleFormatCondition1.Condition = DevExpress.XtraGrid.FormatConditionEnum.NotEqual;
            StyleFormatCondition1.Value1 = 0;
            this.TreeList1.FormatConditions.AddRange(new DevExpress.XtraTreeList.StyleFormatConditions.StyleFormatCondition[] { StyleFormatCondition1 });
            this.TreeList1.Location = new System.Drawing.Point(13, 35);
            this.TreeList1.Name = "TreeList1";
            this.TreeList1.OptionsBehavior.AutoPopulateColumns = false;
            this.TreeList1.OptionsBehavior.Editable = false;
            this.TreeList1.OptionsView.EnableAppearanceEvenRow = true;
            this.TreeList1.OptionsView.EnableAppearanceOddRow = true;
            this.TreeList1.OptionsView.ShowIndicator = false;
            this.TreeList1.Size = new System.Drawing.Size(396, 223);
            this.TreeList1.TabIndex = 2;
            //
            //TreeListColumn_seq
            //
            this.TreeListColumn_seq.Caption = "Seq#";
            this.TreeListColumn_seq.CustomizationCaption = "Sequence Number";
            this.TreeListColumn_seq.FieldName = "Id";
            this.TreeListColumn_seq.Name = "TreeListColumn_seq";
            this.TreeListColumn_seq.OptionsColumn.AllowEdit = false;
            //
            //TreeListColumn_parentseq
            //
            this.TreeListColumn_parentseq.Caption = "Parent Seq";
            this.TreeListColumn_parentseq.CustomizationCaption = "Parent Sequence #";
            this.TreeListColumn_parentseq.FieldName = "parentseq";
            this.TreeListColumn_parentseq.Name = "TreeListColumn_parentseq";
            this.TreeListColumn_parentseq.OptionsColumn.AllowEdit = false;
            //
            //TreeListColumn_text
            //
            this.TreeListColumn_text.Caption = "Description";
            this.TreeListColumn_text.CustomizationCaption = "Description";
            this.TreeListColumn_text.FieldName = "text";
            this.TreeListColumn_text.Name = "TreeListColumn_text";
            this.TreeListColumn_text.OptionsColumn.AllowEdit = false;
            this.TreeListColumn_text.Visible = true;
            this.TreeListColumn_text.VisibleIndex = 0;
            this.TreeListColumn_text.Width = 288;
            //
            //TreeListColumn_type
            //
            this.TreeListColumn_type.Caption = "type";
            this.TreeListColumn_type.CustomizationCaption = "type";
            this.TreeListColumn_type.FieldName = "type";
            this.TreeListColumn_type.Format.FormatString = "f0";
            this.TreeListColumn_type.Format.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.TreeListColumn_type.Name = "TreeListColumn_type";
            this.TreeListColumn_type.OptionsColumn.AllowEdit = false;
            //
            //TreeListColumn_program_key
            //
            this.TreeListColumn_program_key.Caption = "Program Key";
            this.TreeListColumn_program_key.CustomizationCaption = "Program to be executed";
            this.TreeListColumn_program_key.FieldName = "program_key";
            this.TreeListColumn_program_key.Name = "TreeListColumn_program_key";
            this.TreeListColumn_program_key.OptionsColumn.AllowEdit = false;
            //
            //SimpleButton1
            //
            this.SimpleButton1.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.SimpleButton1.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.SimpleButton1.Location = new System.Drawing.Point(173, 267);
            this.SimpleButton1.Name = "SimpleButton1";
            this.SimpleButton1.Size = new System.Drawing.Size(75, 23);
            this.SimpleButton1.TabIndex = 3;
            this.SimpleButton1.Text = "&OK";
            //
            //ViewMenu_Form
            //
            this.AcceptButton = this.SimpleButton1;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6f, 13f);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.CancelButton = this.SimpleButton1;
            this.ClientSize = new System.Drawing.Size(421, 302);
            this.Controls.Add(this.SimpleButton1);
            this.Controls.Add(this.TreeList1);
            this.Controls.Add(this.SpinEdit1);
            this.Controls.Add(this.LabelControl1);
            this.Name = "ViewMenu_Form";
            this.Text = "Menu Layout";
            ((System.ComponentModel.ISupportInitialize)this.DxErrorProvider1).EndInit();
            ((System.ComponentModel.ISupportInitialize)this.SpinEdit1.Properties).EndInit();
            ((System.ComponentModel.ISupportInitialize)this.TreeList1).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();
        }

        private DevExpress.XtraEditors.LabelControl LabelControl1;
        private DevExpress.XtraEditors.SpinEdit SpinEdit1;
        private DevExpress.XtraTreeList.TreeList TreeList1;
        private DevExpress.XtraEditors.SimpleButton SimpleButton1;
        private DevExpress.XtraTreeList.Columns.TreeListColumn TreeListColumn_seq;
        private DevExpress.XtraTreeList.Columns.TreeListColumn TreeListColumn_parentseq;
        private DevExpress.XtraTreeList.Columns.TreeListColumn TreeListColumn_text;
        private DevExpress.XtraTreeList.Columns.TreeListColumn TreeListColumn_attributes;
        private DevExpress.XtraTreeList.Columns.TreeListColumn TreeListColumn_type;
        private DevExpress.XtraTreeList.Columns.TreeListColumn TreeListColumn_program_key;
    }
}
