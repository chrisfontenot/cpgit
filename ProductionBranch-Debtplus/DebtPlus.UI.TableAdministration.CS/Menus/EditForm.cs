#region "Copyright 2000-2012 DebtPlus, L.L.C."

//{*******************************************************************}
//{                                                                   }
//{       DebtPlus Debt Management System                             }
//{                                                                   }
//{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
//{       ALL RIGHTS RESERVED                                         }
//{                                                                   }
//{   The entire contents of this file is protected by U.S. and       }
//{   International Copyright Laws. Unauthorized reproduction,        }
//{   reverse-engineering, and distribution of all or any portion of  }
//{   the code contained in this file is strictly prohibited and may  }
//{   result in severe civil and criminal penalties and will be       }
//{   prosecuted to the maximum extent possible under the law.        }
//{                                                                   }
//{   RESTRICTIONS                                                    }
//{                                                                   }
//{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
//{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
//{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
//{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
//{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
//{                                                                   }
//{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
//{   ADDITIONAL RESTRICTIONS.                                        }
//{                                                                   }
//{*******************************************************************}

#endregion "Copyright 2000-2012 DebtPlus, L.L.C."

using System;
using System.Linq;

namespace DebtPlus.UI.TableAdministration.CS.Menus
{
    internal partial class EditForm : Templates.EditTemplateForm
    {
        private DebtPlus.LINQ.menus record;
        private System.Collections.Generic.List<DebtPlus.LINQ.menus> colRecords;

        internal EditForm(System.Collections.Generic.List<DebtPlus.LINQ.menus> colRecords, DebtPlus.LINQ.menus record)
            : base()
        {
            this.record = record;
            this.colRecords = colRecords;
            InitializeComponent();
            RegisterHandlers();
        }

        private void RegisterHandlers()
        {
            Load += EditForm_Load;
            TextEdit_comment.EditValueChanged += FormChanged;
            TextEdit_menu.EditValueChanged += FormChanged;
            TextEdit_ParentSeq.EditValueChanged += FormChanged;
            TextEdit_ParentSeq.EditValueChanged += UpdateLabel;
            TextEdit_text.EditValueChanged += FormChanged;
            SpinEdit_menu_level.EditValueChanged += FormChanged;
            CheckEdit_attributes.EditValueChanged += FormChanged;
            this.Resize += new EventHandler(EditForm_Resize);
        }

        private void UnRegisterHandlers()
        {
            Load -= EditForm_Load;
            TextEdit_comment.EditValueChanged -= FormChanged;
            TextEdit_menu.EditValueChanged -= FormChanged;
            TextEdit_ParentSeq.EditValueChanged -= FormChanged;
            TextEdit_ParentSeq.EditValueChanged -= UpdateLabel;
            TextEdit_text.EditValueChanged -= FormChanged;
            SpinEdit_menu_level.EditValueChanged -= FormChanged;
            CheckEdit_attributes.EditValueChanged -= FormChanged;
            this.Resize -= new EventHandler(EditForm_Resize);
        }

        protected override void simpleButton_OK_Click(object sender, EventArgs e)
        {
            base.simpleButton_OK_Click(sender, e);

            record.Id = Id();
            record.parentSeq = ParentSeq();
            record.text = DebtPlus.Utils.Nulls.v_String(TextEdit_text.EditValue);
            record.type = DebtPlus.Utils.Nulls.v_String(TextEdit_type.EditValue);
            record.menu_level = DebtPlus.Utils.Nulls.v_Int32(SpinEdit_menu_level.EditValue);
            record.argument = DebtPlus.Utils.Nulls.v_String(TextEdit_comment.EditValue);
            record.attributes = CheckEdit_attributes.Checked ? 1 : 0;
        }

        private System.Drawing.Size sz;

        private void EditForm_Resize(object sender, EventArgs e)
        {
            sz = this.Size;
        }

        private void EditForm_Load(object sender, System.EventArgs e)
        {
            UnRegisterHandlers();
            try
            {
                // Bind the controls
                TextEdit_menu.EditValue = record.Id;
                TextEdit_text.EditValue = record.text;
                TextEdit_type.EditValue = record.type;
                TextEdit_ParentSeq.EditValue = record.parentSeq;
                SpinEdit_menu_level.EditValue = record.menu_level;
                TextEdit_comment.EditValue = record.argument;
                CheckEdit_attributes.EditValue = record.attributes;

                DoUpdateLabel();
                simpleButton_OK.Enabled = !HasErrors();
            }
            finally
            {
                RegisterHandlers();
            }
        }

        private System.Int32 Id()
        {
            System.Int32 Answer = 0;
            string txt = TextEdit_menu.Text.Trim();
            if (txt != string.Empty)
            {
                if (!int.TryParse(txt, System.Globalization.NumberStyles.Integer, System.Globalization.CultureInfo.InvariantCulture, out Answer))
                {
                    Answer = -1;
                }
            }
            return Answer;
        }

        private System.Int32 ParentSeq()
        {
            System.Int32 Answer = 0;
            string txt = TextEdit_ParentSeq.Text.Trim();
            if (txt != string.Empty)
            {
                if (!int.TryParse(txt, System.Globalization.NumberStyles.Integer, System.Globalization.CultureInfo.InvariantCulture, out Answer))
                {
                    Answer = -1;
                }
            }
            return Answer;
        }

        private DebtPlus.LINQ.menus ParentRow()
        {
            System.Int32 Value = ParentSeq();
            if (Value >= 0)
            {
                return colRecords.Where(s => s.Id == Value).FirstOrDefault();
            }

            return null;
        }

        private void UpdateLabel(object Sender, System.EventArgs e)
        {
            DoUpdateLabel();
        }

        private void DoUpdateLabel()
        {
            DebtPlus.LINQ.menus parentRecord = ParentRow();
            LabelControl_ParentName.Text = (parentRecord != null) ? parentRecord.text : string.Empty;
        }

        private void FormChanged(object Sender, System.EventArgs e)
        {
            simpleButton_OK.Enabled = !HasErrors();
        }

        private bool HasErrors()
        {
            // Ensure that there is an ID for the row
            if (Id() <= 0)
            {
                return true;
            }

            // Ensure that there is a description
            if (TextEdit_text.Text.Trim() == string.Empty)
            {
                return true;
            }

            // Ensure that the parent is defined or is empty
            if (Id() == ParentSeq())
            {
                return true;
            }

            if (ParentSeq() > 0 && ParentRow() == null)
            {
                return true;
            }

            // Finally, everything is valid.
            return false;
        }
    }
}