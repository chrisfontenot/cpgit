﻿using System;
using DebtPlus.LINQ;

namespace DebtPlus.UI.TableAdministration.CS.ActionItems
{
    internal partial class EditForm : Templates.EditTemplateForm
    {
        // Current record being edited
        private action_item record;

        /// <summary>
        /// Initialize the new class instance
        /// </summary>
        internal EditForm()
            : base()
        {
            InitializeComponent();
        }

        /// <summary>
        /// Initialize the new class instance
        /// </summary>
        /// <param name="record"></param>
        internal EditForm(action_item record)
            : this()
        {
            this.record = record;
            RegisterHandlers();
            ComboBoxEdit_item_group.Properties.DataSource = DebtPlus.LINQ.InMemory.action_itemTypes.getList();
        }

        /// <summary>
        /// Add the event handlers to the collection
        /// </summary>
        private void RegisterHandlers()
        {
            Load += EditForm_Load;
        }

        /// <summary>
        /// Remove the event handlers from being processed
        /// </summary>
        private void UnRegisterHandlers()
        {
            Load -= EditForm_Load;
        }

        /// <summary>
        /// Process the FORM LOAD event
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void EditForm_Load(object sender, System.EventArgs e)
        {
            UnRegisterHandlers();
            try
            {
                LabelControl_ID.Text = record.Id <= 0 ? "NEW" : record.Id.ToString();
                ComboBoxEdit_item_group.EditValue = record.item_group;
                TextEdit_description.EditValue = record.description;

                ComboBoxEdit_item_group.EditValueChanged += FormChanged;
                TextEdit_description.EditValueChanged += FormChanged;

                simpleButton_OK.Enabled = !HasErrors();
            }
            finally
            {
                RegisterHandlers();
            }
        }

        /// <summary>
        /// Override the OK button to save the editing values into the record
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected override void simpleButton_OK_Click(object sender, EventArgs e)
        {
            // Retrieve the values when the OK is pressed

            record.item_group = Convert.ToInt32(ComboBoxEdit_item_group.EditValue);
            record.description = Convert.ToString(TextEdit_description.EditValue).Trim();

            base.simpleButton_OK_Click(sender, e);
        }

        private void FormChanged(object sender, EventArgs e)
        {
            simpleButton_OK.Enabled = !HasErrors();
        }

        /// <summary>
        /// Determine if the input form has error(s)
        /// </summary>
        /// <returns></returns>
        private bool HasErrors()
        {
            return String.IsNullOrEmpty(Convert.ToString(TextEdit_description.EditValue));
        }
    }
}