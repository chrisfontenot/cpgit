﻿namespace DebtPlus.UI.TableAdministration.CS.ActionItems
{
    partial class MainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
			try
			{
				if (disposing)
				{
					if (components != null) components.Dispose();
					if (bc != null) bc.Dispose();
				}
				components = null;
				bc = null;
			}
			finally
			{
				base.Dispose(disposing);
			}
        }

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.gridColumn_ID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn_item_group = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn_description = new DevExpress.XtraGrid.Columns.GridColumn();
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl1)).BeginInit();
            this.layoutControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem_simpleButton_Cancel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem_SimpleButton_Edit)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem_simpleButton_New)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.popupMenu1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DxErrorProvider1)).BeginInit();
            this.SuspendLayout();
            this.layoutControl1.Controls.SetChildIndex(this.SimpleButton_Edit, 0);
            this.layoutControl1.Controls.SetChildIndex(this.simpleButton_Cancel, 0);
            this.layoutControl1.Controls.SetChildIndex(this.simpleButton_New, 0);
            this.layoutControl1.Controls.SetChildIndex(this.gridControl1, 0);
            // 
            // gridView1
            // 
            this.gridView1.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gridColumn_ID,
            this.gridColumn_item_group,
            this.gridColumn_description});
            this.gridView1.GroupCount = 1;
            this.gridView1.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.gridColumn_item_group, DevExpress.Data.ColumnSortOrder.Ascending)});
            // 
            // gridColumn_ID
            // 
            this.gridColumn_ID.Caption = "ID";
            this.gridColumn_ID.FieldName = "Id";
            this.gridColumn_ID.Name = "gridColumn_ID";
            this.gridColumn_ID.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.True;
            this.gridColumn_ID.Visible = true;
            this.gridColumn_ID.VisibleIndex = 0;
            this.gridColumn_ID.Width = 50;
            // 
            // gridColumn_item_group
            // 
            this.gridColumn_item_group.AppearanceCell.Options.UseTextOptions = true;
            this.gridColumn_item_group.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.gridColumn_item_group.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColumn_item_group.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.gridColumn_item_group.Caption = "Group";
            this.gridColumn_item_group.DisplayFormat.FormatString = "f0";
            this.gridColumn_item_group.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Custom;
            this.gridColumn_item_group.FieldName = "item_group";
            this.gridColumn_item_group.GroupFormat.FormatString = "f0";
            this.gridColumn_item_group.GroupFormat.FormatType = DevExpress.Utils.FormatType.Custom;
            this.gridColumn_item_group.GroupInterval = DevExpress.XtraGrid.ColumnGroupInterval.DisplayText;
            this.gridColumn_item_group.Name = "gridColumn_item_group";
            this.gridColumn_item_group.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.True;
            this.gridColumn_item_group.SortMode = DevExpress.XtraGrid.ColumnSortMode.DisplayText;
            this.gridColumn_item_group.Visible = true;
            this.gridColumn_item_group.VisibleIndex = 1;
            // 
            // gridColumn_Name
            // 
            this.gridColumn_description.Caption = "Description";
            this.gridColumn_description.FieldName = "description";
            this.gridColumn_description.Name = "gridColumn_Name";
            this.gridColumn_description.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.True;
            this.gridColumn_description.Visible = true;
            this.gridColumn_description.VisibleIndex = 1;
            this.gridColumn_description.Width = 123;
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(528, 294);
            this.Name = "MainForm";
            this.Text = "Action Items";
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl1)).EndInit();
            this.layoutControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem_simpleButton_Cancel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem_SimpleButton_Edit)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem_simpleButton_New)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.popupMenu1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DxErrorProvider1)).EndInit();
            this.ResumeLayout(false);
        }
		
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn_ID;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn_description;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn_item_group;
    }
}