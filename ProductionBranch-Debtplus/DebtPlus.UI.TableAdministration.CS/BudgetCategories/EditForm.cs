﻿using System;
using DebtPlus.LINQ;

namespace DebtPlus.UI.TableAdministration.CS.BudgetCategories
{
    internal partial class EditForm : Templates.EditTemplateForm
    {
        private budget_category record;

        internal EditForm()
            : base()
        {
            InitializeComponent();
        }

        internal EditForm(budget_category record)
            : this()
        {
            this.record = record;
            RegisterHandlers();
        }

        private void RegisterHandlers()
        {
            Load += EditForm_Load;

            TextEdit_budget_category.EditValueChanged += Form_Changed;
            TextEdit_description.EditValueChanged += Form_Changed;
            TextEdit_rpps_code.EditValueChanged += Form_Changed;
            textEdit_spanish_description.EditValueChanged += Form_Changed;
            CalcEdit_auto_factor.EditValueChanged += Form_Changed;
            CalcEdit_maximum_factor.EditValueChanged += Form_Changed;
            CalcEdit_person_factor.EditValueChanged += Form_Changed;
            CheckEdit_detail.EditValueChanged += Form_Changed;
            CheckEdit_heading.EditValueChanged += Form_Changed;
            CheckEdit_housing_expense.EditValueChanged += Form_Changed;
            CheckEdit_living_expense.EditValueChanged += Form_Changed;
        }

        private void UnRegisterHandlers()
        {
            Load -= EditForm_Load;

            TextEdit_budget_category.EditValueChanged -= Form_Changed;
            TextEdit_description.EditValueChanged -= Form_Changed;
            textEdit_spanish_description.EditValueChanged -= Form_Changed;
            TextEdit_rpps_code.EditValueChanged -= Form_Changed;
            CalcEdit_auto_factor.EditValueChanged -= Form_Changed;
            CalcEdit_maximum_factor.EditValueChanged -= Form_Changed;
            CalcEdit_person_factor.EditValueChanged -= Form_Changed;
            CheckEdit_detail.EditValueChanged -= Form_Changed;
            CheckEdit_heading.EditValueChanged -= Form_Changed;
            CheckEdit_housing_expense.EditValueChanged -= Form_Changed;
            CheckEdit_living_expense.EditValueChanged -= Form_Changed;
        }

        private void EditForm_Load(object sender, EventArgs e)
        {
            UnRegisterHandlers();
            try
            {
                TextEdit_budget_category.EditValue = record.Id;
                TextEdit_description.EditValue = record.description;
                textEdit_spanish_description.EditValue = record.spanish_description;
                TextEdit_rpps_code.EditValue = record.rpps_code;
                CalcEdit_auto_factor.EditValue = record.auto_factor;
                CalcEdit_maximum_factor.EditValue = record.maximum_factor;
                CalcEdit_person_factor.EditValue = record.maximum_factor;
                CheckEdit_detail.EditValue = record.detail;
                CheckEdit_heading.EditValue = record.heading;
                CheckEdit_housing_expense.EditValue = record.housing_expense;
                CheckEdit_living_expense.EditValue = record.living_expense;

                // Do not allow the ID to be changed once it is defined
                TextEdit_budget_category.Properties.ReadOnly = (record.Id > 0);

                // Enable or disable the OK button based upon the required values
                simpleButton_OK.Enabled = !HasErrors();
            }
            finally
            {
                RegisterHandlers();
            }
        }

        private void Form_Changed(object sender, EventArgs e)
        {
            simpleButton_OK.Enabled = !HasErrors();
        }

        /// <summary>
        /// Does the input form have a valid record?
        /// </summary>
        private bool HasErrors()
        {
            // The budget category must be supplied
            if (string.IsNullOrWhiteSpace(DebtPlus.Utils.Nulls.v_String(TextEdit_budget_category.EditValue)))
            {
                return true;
            }

            // The description must be supplied
            if (string.IsNullOrWhiteSpace(DebtPlus.Utils.Nulls.v_String(TextEdit_description.Text)) || string.IsNullOrWhiteSpace(DebtPlus.Utils.Nulls.v_String(textEdit_spanish_description.EditValue)))
            {
                return true;
            }

            // Do not allow the ID to be invalid.
            Int32 Id;
            if (!Int32.TryParse(TextEdit_budget_category.Text.Trim(), out Id) || Id <= 0 || Id >= 10000)
            {
                return true;
            }

            // Ensure that the maximum values are not negative
            decimal? calcField = DebtPlus.Utils.Nulls.v_Decimal(CalcEdit_auto_factor.EditValue);
            if (calcField.GetValueOrDefault() < 0M)
            {
                return true;
            }

            calcField = DebtPlus.Utils.Nulls.v_Decimal(CalcEdit_person_factor.EditValue);
            if (calcField.GetValueOrDefault() < 0M)
            {
                return true;
            }

            calcField = DebtPlus.Utils.Nulls.v_Decimal(CalcEdit_maximum_factor.EditValue);
            if (calcField.GetValueOrDefault() < 0M)
            {
                return true;
            }

            // The RPPS code is limited to 16 bits (by Mastercard)
            Int32 rppsID = DebtPlus.Utils.Nulls.DInt(TextEdit_rpps_code.EditValue);
            if (rppsID < 0 || rppsID > 32767)
            {
                return true;
            }

            // The record is acceptable for the time being
            return false;
        }

        protected override void simpleButton_OK_Click(object sender, EventArgs e)
        {
            base.simpleButton_OK_Click(sender, e);

            // Update the record with the input values
            record.Id = DebtPlus.Utils.Nulls.v_Int32(TextEdit_budget_category.EditValue).GetValueOrDefault();
            record.description = DebtPlus.Utils.Nulls.v_String(TextEdit_description.EditValue);
            record.spanish_description = DebtPlus.Utils.Nulls.v_String(textEdit_spanish_description.EditValue);
            record.rpps_code = DebtPlus.Utils.Nulls.v_Int32(TextEdit_rpps_code.EditValue).GetValueOrDefault(6);
            record.auto_factor = DebtPlus.Utils.Nulls.v_Decimal(CalcEdit_auto_factor.EditValue);
            record.maximum_factor = DebtPlus.Utils.Nulls.v_Decimal(CalcEdit_maximum_factor.EditValue);
            record.maximum_factor = DebtPlus.Utils.Nulls.v_Decimal(CalcEdit_person_factor.EditValue);
            record.detail = CheckEdit_detail.Checked;
            record.heading = CheckEdit_heading.Checked;
            record.housing_expense = CheckEdit_housing_expense.Checked;
            record.living_expense = CheckEdit_living_expense.Checked;
        }
    }
}