﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Diagnostics;
using DebtPlus.UI.Common.Templates;

namespace DebtPlus.UI.TableAdministration.CS.BudgetCategories
{
    partial class MainForm
    {
        //Form overrides dispose to clean up the component list.
        [System.Diagnostics.DebuggerNonUserCode]
        protected override void Dispose(bool disposing)
        {
            try
            {
                if (disposing)
                {
                    if (components != null) components.Dispose();
                    if (bc != null) bc.Dispose();
                }
                bc = null;
                components = null;
            }
            finally
            {
                base.Dispose(disposing);
            }
        }

        //Required by the Windows Form Designer
        private System.ComponentModel.IContainer components = null;

        //NOTE: The following procedure is required by the Windows Form Designer
        //It can be modified using the Windows Form Designer.  
        //Do not modify it using the code editor.
        [System.Diagnostics.DebuggerStepThrough]
        private void InitializeComponent()
        {
            DevExpress.XtraGrid.StyleFormatCondition StyleFormatCondition1 = new DevExpress.XtraGrid.StyleFormatCondition();
            this.gridColumn_ID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn_ID.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.True;
            this.gridColumn_description = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn_description.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.True;
            this.gridColumn_heading = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn_heading.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.True;
            this.gridColumn_detail = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn_detail.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.True;
            this.gridColumn_rpps_code = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn_rpps_code.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.True;
            ((System.ComponentModel.ISupportInitialize)this.gridControl1).BeginInit();
            ((System.ComponentModel.ISupportInitialize)this.gridView1).BeginInit();
            ((System.ComponentModel.ISupportInitialize)this.DxErrorProvider1).BeginInit();
            this.SuspendLayout();
            //
            //gridControl1
            //
            this.gridControl1.EmbeddedNavigator.Name = "";
            //
            //gridView1
            //
            this.gridView1.Appearance.ColumnFilterButton.BackColor = System.Drawing.Color.FromArgb(104, 184, 251);
            this.gridView1.Appearance.ColumnFilterButton.BorderColor = System.Drawing.Color.FromArgb(104, 184, 251);
            this.gridView1.Appearance.ColumnFilterButton.ForeColor = System.Drawing.Color.White;
            this.gridView1.Appearance.ColumnFilterButton.Options.UseBackColor = true;
            this.gridView1.Appearance.ColumnFilterButton.Options.UseBorderColor = true;
            this.gridView1.Appearance.ColumnFilterButton.Options.UseForeColor = true;
            this.gridView1.Appearance.ColumnFilterButtonActive.BackColor = System.Drawing.Color.FromArgb(170, 216, 254);
            this.gridView1.Appearance.ColumnFilterButtonActive.BorderColor = System.Drawing.Color.FromArgb(170, 216, 254);
            this.gridView1.Appearance.ColumnFilterButtonActive.ForeColor = System.Drawing.Color.Black;
            this.gridView1.Appearance.ColumnFilterButtonActive.Options.UseBackColor = true;
            this.gridView1.Appearance.ColumnFilterButtonActive.Options.UseBorderColor = true;
            this.gridView1.Appearance.ColumnFilterButtonActive.Options.UseForeColor = true;
            this.gridView1.Appearance.Empty.BackColor = System.Drawing.Color.FromArgb(236, 246, 255);
            this.gridView1.Appearance.Empty.BackColor2 = System.Drawing.Color.White;
            this.gridView1.Appearance.Empty.Options.UseBackColor = true;
            this.gridView1.Appearance.EvenRow.BackColor = System.Drawing.Color.FromArgb(247, 251, 255);
            this.gridView1.Appearance.EvenRow.BorderColor = System.Drawing.Color.FromArgb(247, 251, 255);
            this.gridView1.Appearance.EvenRow.ForeColor = System.Drawing.Color.Black;
            this.gridView1.Appearance.EvenRow.Options.UseBackColor = true;
            this.gridView1.Appearance.EvenRow.Options.UseBorderColor = true;
            this.gridView1.Appearance.EvenRow.Options.UseForeColor = true;
            this.gridView1.Appearance.FilterCloseButton.BackColor = System.Drawing.Color.FromArgb(104, 184, 251);
            this.gridView1.Appearance.FilterCloseButton.BorderColor = System.Drawing.Color.FromArgb(104, 184, 251);
            this.gridView1.Appearance.FilterCloseButton.ForeColor = System.Drawing.Color.White;
            this.gridView1.Appearance.FilterCloseButton.Options.UseBackColor = true;
            this.gridView1.Appearance.FilterCloseButton.Options.UseBorderColor = true;
            this.gridView1.Appearance.FilterCloseButton.Options.UseForeColor = true;
            this.gridView1.Appearance.FilterPanel.BackColor = System.Drawing.Color.FromArgb(236, 246, 255);
            this.gridView1.Appearance.FilterPanel.BackColor2 = System.Drawing.Color.White;
            this.gridView1.Appearance.FilterPanel.ForeColor = System.Drawing.Color.Black;
            this.gridView1.Appearance.FilterPanel.Options.UseBackColor = true;
            this.gridView1.Appearance.FilterPanel.Options.UseForeColor = true;
            this.gridView1.Appearance.FixedLine.BackColor = System.Drawing.Color.FromArgb(59, 133, 195);
            this.gridView1.Appearance.FixedLine.Options.UseBackColor = true;
            this.gridView1.Appearance.FocusedCell.BackColor = System.Drawing.Color.White;
            this.gridView1.Appearance.FocusedCell.ForeColor = System.Drawing.Color.Black;
            this.gridView1.Appearance.FocusedCell.Options.UseBackColor = true;
            this.gridView1.Appearance.FocusedCell.Options.UseForeColor = true;
            this.gridView1.Appearance.FocusedRow.BackColor = System.Drawing.Color.FromArgb(38, 109, 189);
            this.gridView1.Appearance.FocusedRow.BorderColor = System.Drawing.Color.FromArgb(59, 139, 206);
            this.gridView1.Appearance.FocusedRow.ForeColor = System.Drawing.Color.White;
            this.gridView1.Appearance.FocusedRow.Options.UseBackColor = true;
            this.gridView1.Appearance.FocusedRow.Options.UseBorderColor = true;
            this.gridView1.Appearance.FocusedRow.Options.UseForeColor = true;
            this.gridView1.Appearance.FooterPanel.BackColor = System.Drawing.Color.FromArgb(104, 184, 251);
            this.gridView1.Appearance.FooterPanel.BorderColor = System.Drawing.Color.FromArgb(104, 184, 251);
            this.gridView1.Appearance.FooterPanel.ForeColor = System.Drawing.Color.Black;
            this.gridView1.Appearance.FooterPanel.Options.UseBackColor = true;
            this.gridView1.Appearance.FooterPanel.Options.UseBorderColor = true;
            this.gridView1.Appearance.FooterPanel.Options.UseForeColor = true;
            this.gridView1.Appearance.GroupButton.BackColor = System.Drawing.Color.FromArgb(104, 184, 251);
            this.gridView1.Appearance.GroupButton.BorderColor = System.Drawing.Color.FromArgb(104, 184, 251);
            this.gridView1.Appearance.GroupButton.Options.UseBackColor = true;
            this.gridView1.Appearance.GroupButton.Options.UseBorderColor = true;
            this.gridView1.Appearance.GroupFooter.BackColor = System.Drawing.Color.FromArgb(170, 216, 254);
            this.gridView1.Appearance.GroupFooter.BorderColor = System.Drawing.Color.FromArgb(170, 216, 254);
            this.gridView1.Appearance.GroupFooter.ForeColor = System.Drawing.Color.Black;
            this.gridView1.Appearance.GroupFooter.Options.UseBackColor = true;
            this.gridView1.Appearance.GroupFooter.Options.UseBorderColor = true;
            this.gridView1.Appearance.GroupFooter.Options.UseForeColor = true;
            this.gridView1.Appearance.GroupPanel.BackColor = System.Drawing.Color.FromArgb(236, 246, 255);
            this.gridView1.Appearance.GroupPanel.BackColor2 = System.Drawing.Color.White;
            this.gridView1.Appearance.GroupPanel.ForeColor = System.Drawing.Color.Black;
            this.gridView1.Appearance.GroupPanel.Options.UseBackColor = true;
            this.gridView1.Appearance.GroupPanel.Options.UseForeColor = true;
            this.gridView1.Appearance.GroupRow.BackColor = System.Drawing.Color.FromArgb(170, 216, 254);
            this.gridView1.Appearance.GroupRow.BorderColor = System.Drawing.Color.FromArgb(170, 216, 254);
            this.gridView1.Appearance.GroupRow.ForeColor = System.Drawing.Color.Black;
            this.gridView1.Appearance.GroupRow.Options.UseBackColor = true;
            this.gridView1.Appearance.GroupRow.Options.UseBorderColor = true;
            this.gridView1.Appearance.GroupRow.Options.UseForeColor = true;
            this.gridView1.Appearance.HeaderPanel.BackColor = System.Drawing.Color.FromArgb(139, 201, 254);
            this.gridView1.Appearance.HeaderPanel.BorderColor = System.Drawing.Color.FromArgb(139, 201, 254);
            this.gridView1.Appearance.HeaderPanel.ForeColor = System.Drawing.Color.Black;
            this.gridView1.Appearance.HeaderPanel.Options.UseBackColor = true;
            this.gridView1.Appearance.HeaderPanel.Options.UseBorderColor = true;
            this.gridView1.Appearance.HeaderPanel.Options.UseForeColor = true;
            this.gridView1.Appearance.HideSelectionRow.BackColor = System.Drawing.Color.FromArgb(105, 170, 225);
            this.gridView1.Appearance.HideSelectionRow.BorderColor = System.Drawing.Color.FromArgb(83, 155, 215);
            this.gridView1.Appearance.HideSelectionRow.ForeColor = System.Drawing.Color.FromArgb(236, 246, 255);
            this.gridView1.Appearance.HideSelectionRow.Options.UseBackColor = true;
            this.gridView1.Appearance.HideSelectionRow.Options.UseBorderColor = true;
            this.gridView1.Appearance.HideSelectionRow.Options.UseForeColor = true;
            this.gridView1.Appearance.HorzLine.BackColor = System.Drawing.Color.FromArgb(104, 184, 251);
            this.gridView1.Appearance.HorzLine.Options.UseBackColor = true;
            this.gridView1.Appearance.OddRow.BackColor = System.Drawing.Color.FromArgb(236, 246, 255);
            this.gridView1.Appearance.OddRow.BorderColor = System.Drawing.Color.FromArgb(236, 246, 255);
            this.gridView1.Appearance.OddRow.ForeColor = System.Drawing.Color.Black;
            this.gridView1.Appearance.OddRow.Options.UseBackColor = true;
            this.gridView1.Appearance.OddRow.Options.UseBorderColor = true;
            this.gridView1.Appearance.OddRow.Options.UseForeColor = true;
            this.gridView1.Appearance.Preview.Font = new System.Drawing.Font("Verdana", 7.5f);
            this.gridView1.Appearance.Preview.ForeColor = System.Drawing.Color.FromArgb(83, 155, 215);
            this.gridView1.Appearance.Preview.Options.UseFont = true;
            this.gridView1.Appearance.Preview.Options.UseForeColor = true;
            this.gridView1.Appearance.Row.BackColor = System.Drawing.Color.FromArgb(247, 251, 255);
            this.gridView1.Appearance.Row.ForeColor = System.Drawing.Color.Black;
            this.gridView1.Appearance.Row.Options.UseBackColor = true;
            this.gridView1.Appearance.Row.Options.UseForeColor = true;
            this.gridView1.Appearance.RowSeparator.BackColor = System.Drawing.Color.FromArgb(236, 246, 255);
            this.gridView1.Appearance.RowSeparator.BackColor2 = System.Drawing.Color.White;
            this.gridView1.Appearance.RowSeparator.Options.UseBackColor = true;
            this.gridView1.Appearance.SelectedRow.BackColor = System.Drawing.Color.FromArgb(83, 155, 215);
            this.gridView1.Appearance.SelectedRow.ForeColor = System.Drawing.Color.White;
            this.gridView1.Appearance.SelectedRow.Options.UseBackColor = true;
            this.gridView1.Appearance.SelectedRow.Options.UseForeColor = true;
            this.gridView1.Appearance.TopNewRow.BackColor = System.Drawing.Color.White;
            this.gridView1.Appearance.TopNewRow.Options.UseBackColor = true;
            this.gridView1.Appearance.VertLine.BackColor = System.Drawing.Color.FromArgb(104, 184, 251);
            this.gridView1.Appearance.VertLine.Options.UseBackColor = true;
            this.gridView1.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
			this.gridColumn_ID,
			this.gridColumn_description,
			this.gridColumn_heading,
			this.gridColumn_detail,
			this.gridColumn_rpps_code
		});
            StyleFormatCondition1.Appearance.BackColor = System.Drawing.Color.Red;
            StyleFormatCondition1.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25f, System.Drawing.FontStyle.Bold);
            StyleFormatCondition1.Appearance.ForeColor = System.Drawing.Color.White;
            StyleFormatCondition1.Appearance.Options.UseBackColor = true;
            StyleFormatCondition1.Appearance.Options.UseFont = true;
            StyleFormatCondition1.Appearance.Options.UseForeColor = true;
            StyleFormatCondition1.ApplyToRow = true;
            StyleFormatCondition1.Column = this.gridColumn_heading;
            StyleFormatCondition1.Condition = DevExpress.XtraGrid.FormatConditionEnum.NotEqual;
            StyleFormatCondition1.Value1 = false;
            this.gridView1.FormatConditions.AddRange(new DevExpress.XtraGrid.StyleFormatCondition[] { StyleFormatCondition1 });
            this.gridView1.OptionsBehavior.Editable = false;
            this.gridView1.OptionsNavigation.AutoFocusNewRow = true;
            this.gridView1.OptionsSelection.InvertSelection = true;
            this.gridView1.OptionsView.RowAutoHeight = true;
            this.gridView1.OptionsView.ShowGroupPanel = false;
            this.gridView1.OptionsView.ShowIndicator = false;
            //
            //gridColumn_ID
            //
            this.gridColumn_ID.AppearanceCell.Options.UseTextOptions = true;
            this.gridColumn_ID.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.gridColumn_ID.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColumn_ID.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.gridColumn_ID.Caption = "ID";
            this.gridColumn_ID.DisplayFormat.FormatString = "f0";
            this.gridColumn_ID.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.gridColumn_ID.FieldName = "Id";
            this.gridColumn_ID.GroupFormat.FormatString = "f0";
            this.gridColumn_ID.GroupFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.gridColumn_ID.Name = "gridColumn_ID";
            this.gridColumn_ID.Visible = true;
            this.gridColumn_ID.VisibleIndex = 0;
            //
            //gridColumn_Name
            //
            this.gridColumn_description.Caption = "Description";
            this.gridColumn_description.FieldName = "description";
            this.gridColumn_description.Name = "gridColumn_Name";
            this.gridColumn_description.Visible = true;
            this.gridColumn_description.VisibleIndex = 1;
            //
            //gridColumn_heading
            //
            this.gridColumn_heading.Caption = "Heading";
            this.gridColumn_heading.FieldName = "heading";
            this.gridColumn_heading.Name = "gridColumn_heading";
            this.gridColumn_heading.Visible = true;
            this.gridColumn_heading.VisibleIndex = 2;
            //
            //gridColumn_detail
            //
            this.gridColumn_detail.Caption = "Detail";
            this.gridColumn_detail.FieldName = "detail";
            this.gridColumn_detail.Name = "gridColumn_detail";
            this.gridColumn_detail.Visible = true;
            this.gridColumn_detail.VisibleIndex = 3;
            //
            //gridColumn_rpps_code
            //
            this.gridColumn_rpps_code.AppearanceCell.Options.UseTextOptions = true;
            this.gridColumn_rpps_code.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.gridColumn_rpps_code.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColumn_rpps_code.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.gridColumn_rpps_code.Caption = "RPPS";
            this.gridColumn_rpps_code.DisplayFormat.FormatString = "f0";
            this.gridColumn_rpps_code.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.gridColumn_rpps_code.FieldName = "rpps_code";
            this.gridColumn_rpps_code.GroupFormat.FormatString = "f0";
            this.gridColumn_rpps_code.GroupFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.gridColumn_rpps_code.Name = "gridColumn_rpps_code";
            this.gridColumn_rpps_code.Visible = true;
            this.gridColumn_rpps_code.VisibleIndex = 4;
            //
            //MainForm
            //
            this.AutoScaleDimensions = new System.Drawing.SizeF(6f, 13f);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(528, 294);
            this.Name = "MainForm";
            this.ToolTipController1.SetSuperTip(this, null);
            this.Text = "Budget Categories";
            ((System.ComponentModel.ISupportInitialize)this.gridControl1).EndInit();
            ((System.ComponentModel.ISupportInitialize)this.gridView1).EndInit();
            ((System.ComponentModel.ISupportInitialize)this.DxErrorProvider1).EndInit();
            this.ResumeLayout(false);
        }

        private DevExpress.XtraGrid.Columns.GridColumn gridColumn_ID;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn_description;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn_heading;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn_detail;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn_rpps_code;
    }
}
