﻿namespace DebtPlus.UI.TableAdministration.CS.BudgetCategories
{
    partial class EditForm
    {
		//Form overrides dispose to clean up the component list.
		[System.Diagnostics.DebuggerNonUserCode]
		protected override void Dispose(bool disposing)
		{
			try 
            {
				if (disposing)
                {
                    if (components != null) components.Dispose();
				}
                components = null;
            }
    	    finally
            {
				base.Dispose(disposing);
			}
		}

		//Required by the Windows Form Designer
		private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.TextEdit_budget_category = new DevExpress.XtraEditors.TextEdit();
            this.LabelControl2 = new DevExpress.XtraEditors.LabelControl();
            this.CheckEdit_living_expense = new DevExpress.XtraEditors.CheckEdit();
            this.CheckEdit_housing_expense = new DevExpress.XtraEditors.CheckEdit();
            this.CheckEdit_detail = new DevExpress.XtraEditors.CheckEdit();
            this.CheckEdit_heading = new DevExpress.XtraEditors.CheckEdit();
            this.TextEdit_rpps_code = new DevExpress.XtraEditors.TextEdit();
            this.LabelControl7 = new DevExpress.XtraEditors.LabelControl();
            this.CalcEdit_maximum_factor = new DevExpress.XtraEditors.CalcEdit();
            this.LabelControl6 = new DevExpress.XtraEditors.LabelControl();
            this.CalcEdit_person_factor = new DevExpress.XtraEditors.CalcEdit();
            this.LabelControl5 = new DevExpress.XtraEditors.LabelControl();
            this.CalcEdit_auto_factor = new DevExpress.XtraEditors.CalcEdit();
            this.LabelControl4 = new DevExpress.XtraEditors.LabelControl();
            this.TextEdit_description = new DevExpress.XtraEditors.TextEdit();
            this.LabelControl3 = new DevExpress.XtraEditors.LabelControl();
            this.textEdit_spanish_description = new DevExpress.XtraEditors.TextEdit();
            this.labelControl1 = new DevExpress.XtraEditors.LabelControl();
            ((System.ComponentModel.ISupportInitialize)(this.DxErrorProvider1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TextEdit_budget_category.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.CheckEdit_living_expense.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.CheckEdit_housing_expense.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.CheckEdit_detail.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.CheckEdit_heading.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TextEdit_rpps_code.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.CalcEdit_maximum_factor.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.CalcEdit_person_factor.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.CalcEdit_auto_factor.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TextEdit_description.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit_spanish_description.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // simpleButton_Cancel
            // 
            this.simpleButton_Cancel.Location = new System.Drawing.Point(387, 86);
            this.simpleButton_Cancel.TabIndex = 19;
            // 
            // simpleButton_OK
            // 
            this.simpleButton_OK.Location = new System.Drawing.Point(387, 43);
            this.simpleButton_OK.TabIndex = 18;
            // 
            // DefaultLookAndFeel1
            // 
            this.DefaultLookAndFeel1.LookAndFeel.SkinName = "McSkin";
            // 
            // TextEdit_budget_category
            // 
            this.TextEdit_budget_category.Location = new System.Drawing.Point(93, 5);
            this.TextEdit_budget_category.Name = "TextEdit_budget_category";
            this.TextEdit_budget_category.Properties.Appearance.Options.UseTextOptions = true;
            this.TextEdit_budget_category.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.TextEdit_budget_category.Properties.DisplayFormat.FormatString = "f0";
            this.TextEdit_budget_category.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.TextEdit_budget_category.Properties.EditFormat.FormatString = "f0";
            this.TextEdit_budget_category.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.TextEdit_budget_category.Properties.Mask.BeepOnError = true;
            this.TextEdit_budget_category.Properties.Mask.EditMask = "\\d+";
            this.TextEdit_budget_category.Size = new System.Drawing.Size(80, 20);
            this.TextEdit_budget_category.TabIndex = 1;
            // 
            // LabelControl2
            // 
            this.LabelControl2.Location = new System.Drawing.Point(3, 9);
            this.LabelControl2.Name = "LabelControl2";
            this.LabelControl2.Size = new System.Drawing.Size(82, 13);
            this.LabelControl2.TabIndex = 0;
            this.LabelControl2.Text = "Budget Category";
            // 
            // CheckEdit_living_expense
            // 
            this.CheckEdit_living_expense.Location = new System.Drawing.Point(199, 159);
            this.CheckEdit_living_expense.Name = "CheckEdit_living_expense";
            this.CheckEdit_living_expense.Properties.Caption = "Living Expense";
            this.CheckEdit_living_expense.Size = new System.Drawing.Size(100, 20);
            this.CheckEdit_living_expense.TabIndex = 17;
            // 
            // CheckEdit_housing_expense
            // 
            this.CheckEdit_housing_expense.Location = new System.Drawing.Point(199, 133);
            this.CheckEdit_housing_expense.Name = "CheckEdit_housing_expense";
            this.CheckEdit_housing_expense.Properties.Caption = "Housing Expense";
            this.CheckEdit_housing_expense.Size = new System.Drawing.Size(100, 20);
            this.CheckEdit_housing_expense.TabIndex = 16;
            // 
            // CheckEdit_detail
            // 
            this.CheckEdit_detail.Location = new System.Drawing.Point(199, 107);
            this.CheckEdit_detail.Name = "CheckEdit_detail";
            this.CheckEdit_detail.Properties.Caption = "Allow Detail Information";
            this.CheckEdit_detail.Size = new System.Drawing.Size(152, 20);
            this.CheckEdit_detail.TabIndex = 15;
            // 
            // CheckEdit_heading
            // 
            this.CheckEdit_heading.Location = new System.Drawing.Point(199, 81);
            this.CheckEdit_heading.Name = "CheckEdit_heading";
            this.CheckEdit_heading.Properties.Caption = "Heading Category";
            this.CheckEdit_heading.Size = new System.Drawing.Size(125, 20);
            this.CheckEdit_heading.TabIndex = 14;
            // 
            // TextEdit_rpps_code
            // 
            this.TextEdit_rpps_code.Location = new System.Drawing.Point(93, 161);
            this.TextEdit_rpps_code.Name = "TextEdit_rpps_code";
            this.TextEdit_rpps_code.Properties.Appearance.Options.UseTextOptions = true;
            this.TextEdit_rpps_code.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.TextEdit_rpps_code.Properties.DisplayFormat.FormatString = "f0";
            this.TextEdit_rpps_code.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.TextEdit_rpps_code.Properties.EditFormat.FormatString = "f0";
            this.TextEdit_rpps_code.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.TextEdit_rpps_code.Properties.Mask.BeepOnError = true;
            this.TextEdit_rpps_code.Properties.Mask.EditMask = "\\d+";
            this.TextEdit_rpps_code.Size = new System.Drawing.Size(80, 20);
            this.TextEdit_rpps_code.TabIndex = 13;
            // 
            // LabelControl7
            // 
            this.LabelControl7.Location = new System.Drawing.Point(3, 165);
            this.LabelControl7.Name = "LabelControl7";
            this.LabelControl7.Size = new System.Drawing.Size(53, 13);
            this.LabelControl7.TabIndex = 12;
            this.LabelControl7.Text = "RPPS Code";
            // 
            // CalcEdit_maximum_factor
            // 
            this.CalcEdit_maximum_factor.Location = new System.Drawing.Point(93, 135);
            this.CalcEdit_maximum_factor.Name = "CalcEdit_maximum_factor";
            this.CalcEdit_maximum_factor.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.True;
            this.CalcEdit_maximum_factor.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.CalcEdit_maximum_factor.Size = new System.Drawing.Size(80, 20);
            this.CalcEdit_maximum_factor.TabIndex = 11;
            // 
            // LabelControl6
            // 
            this.LabelControl6.Location = new System.Drawing.Point(3, 139);
            this.LabelControl6.Name = "LabelControl6";
            this.LabelControl6.Size = new System.Drawing.Size(78, 13);
            this.LabelControl6.TabIndex = 10;
            this.LabelControl6.Text = "Maximum Factor";
            // 
            // CalcEdit_person_factor
            // 
            this.CalcEdit_person_factor.Location = new System.Drawing.Point(93, 109);
            this.CalcEdit_person_factor.Name = "CalcEdit_person_factor";
            this.CalcEdit_person_factor.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.True;
            this.CalcEdit_person_factor.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.CalcEdit_person_factor.Size = new System.Drawing.Size(80, 20);
            this.CalcEdit_person_factor.TabIndex = 9;
            // 
            // LabelControl5
            // 
            this.LabelControl5.Location = new System.Drawing.Point(3, 113);
            this.LabelControl5.Name = "LabelControl5";
            this.LabelControl5.Size = new System.Drawing.Size(67, 13);
            this.LabelControl5.TabIndex = 8;
            this.LabelControl5.Text = "Person Factor";
            // 
            // CalcEdit_auto_factor
            // 
            this.CalcEdit_auto_factor.Location = new System.Drawing.Point(93, 83);
            this.CalcEdit_auto_factor.Name = "CalcEdit_auto_factor";
            this.CalcEdit_auto_factor.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.True;
            this.CalcEdit_auto_factor.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.CalcEdit_auto_factor.Size = new System.Drawing.Size(80, 20);
            this.CalcEdit_auto_factor.TabIndex = 7;
            // 
            // LabelControl4
            // 
            this.LabelControl4.Location = new System.Drawing.Point(3, 87);
            this.LabelControl4.Name = "LabelControl4";
            this.LabelControl4.Size = new System.Drawing.Size(57, 13);
            this.LabelControl4.TabIndex = 6;
            this.LabelControl4.Text = "Auto Factor";
            // 
            // TextEdit_description
            // 
            this.TextEdit_description.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.TextEdit_description.Location = new System.Drawing.Point(93, 31);
            this.TextEdit_description.Name = "TextEdit_description";
            this.TextEdit_description.Size = new System.Drawing.Size(288, 20);
            this.TextEdit_description.TabIndex = 3;
            // 
            // LabelControl3
            // 
            this.LabelControl3.Location = new System.Drawing.Point(3, 35);
            this.LabelControl3.Name = "LabelControl3";
            this.LabelControl3.Size = new System.Drawing.Size(53, 13);
            this.LabelControl3.TabIndex = 2;
            this.LabelControl3.Text = "Description";
            // 
            // textEdit_spanish_description
            // 
            this.textEdit_spanish_description.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.textEdit_spanish_description.Location = new System.Drawing.Point(93, 57);
            this.textEdit_spanish_description.Name = "textEdit_spanish_description";
            this.textEdit_spanish_description.Size = new System.Drawing.Size(288, 20);
            this.textEdit_spanish_description.TabIndex = 5;
            // 
            // labelControl1
            // 
            this.labelControl1.Location = new System.Drawing.Point(3, 61);
            this.labelControl1.Name = "labelControl1";
            this.labelControl1.Size = new System.Drawing.Size(37, 13);
            this.labelControl1.TabIndex = 4;
            this.labelControl1.Text = "Spanish";
            // 
            // EditForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(483, 187);
            this.Controls.Add(this.textEdit_spanish_description);
            this.Controls.Add(this.labelControl1);
            this.Controls.Add(this.TextEdit_budget_category);
            this.Controls.Add(this.LabelControl2);
            this.Controls.Add(this.CheckEdit_living_expense);
            this.Controls.Add(this.CheckEdit_housing_expense);
            this.Controls.Add(this.CheckEdit_detail);
            this.Controls.Add(this.CheckEdit_heading);
            this.Controls.Add(this.TextEdit_rpps_code);
            this.Controls.Add(this.LabelControl7);
            this.Controls.Add(this.CalcEdit_maximum_factor);
            this.Controls.Add(this.LabelControl6);
            this.Controls.Add(this.CalcEdit_person_factor);
            this.Controls.Add(this.LabelControl5);
            this.Controls.Add(this.CalcEdit_auto_factor);
            this.Controls.Add(this.LabelControl4);
            this.Controls.Add(this.TextEdit_description);
            this.Controls.Add(this.LabelControl3);
            this.Name = "EditForm";
            this.Text = "Budget Category";
            this.Controls.SetChildIndex(this.simpleButton_OK, 0);
            this.Controls.SetChildIndex(this.simpleButton_Cancel, 0);
            this.Controls.SetChildIndex(this.LabelControl3, 0);
            this.Controls.SetChildIndex(this.TextEdit_description, 0);
            this.Controls.SetChildIndex(this.LabelControl4, 0);
            this.Controls.SetChildIndex(this.CalcEdit_auto_factor, 0);
            this.Controls.SetChildIndex(this.LabelControl5, 0);
            this.Controls.SetChildIndex(this.CalcEdit_person_factor, 0);
            this.Controls.SetChildIndex(this.LabelControl6, 0);
            this.Controls.SetChildIndex(this.CalcEdit_maximum_factor, 0);
            this.Controls.SetChildIndex(this.LabelControl7, 0);
            this.Controls.SetChildIndex(this.TextEdit_rpps_code, 0);
            this.Controls.SetChildIndex(this.CheckEdit_heading, 0);
            this.Controls.SetChildIndex(this.CheckEdit_detail, 0);
            this.Controls.SetChildIndex(this.CheckEdit_housing_expense, 0);
            this.Controls.SetChildIndex(this.CheckEdit_living_expense, 0);
            this.Controls.SetChildIndex(this.LabelControl2, 0);
            this.Controls.SetChildIndex(this.TextEdit_budget_category, 0);
            this.Controls.SetChildIndex(this.labelControl1, 0);
            this.Controls.SetChildIndex(this.textEdit_spanish_description, 0);
            ((System.ComponentModel.ISupportInitialize)(this.DxErrorProvider1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TextEdit_budget_category.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.CheckEdit_living_expense.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.CheckEdit_housing_expense.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.CheckEdit_detail.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.CheckEdit_heading.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TextEdit_rpps_code.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.CalcEdit_maximum_factor.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.CalcEdit_person_factor.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.CalcEdit_auto_factor.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TextEdit_description.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit_spanish_description.Properties)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }


        private DevExpress.XtraEditors.TextEdit TextEdit_budget_category;
        private DevExpress.XtraEditors.LabelControl LabelControl2;
        private DevExpress.XtraEditors.CheckEdit CheckEdit_living_expense;
        private DevExpress.XtraEditors.CheckEdit CheckEdit_housing_expense;
        private DevExpress.XtraEditors.CheckEdit CheckEdit_detail;
        private DevExpress.XtraEditors.CheckEdit CheckEdit_heading;
        private DevExpress.XtraEditors.TextEdit TextEdit_rpps_code;
        private DevExpress.XtraEditors.LabelControl LabelControl7;
        private DevExpress.XtraEditors.CalcEdit CalcEdit_maximum_factor;
        private DevExpress.XtraEditors.LabelControl LabelControl6;
        private DevExpress.XtraEditors.CalcEdit CalcEdit_person_factor;
        private DevExpress.XtraEditors.LabelControl LabelControl5;
        private DevExpress.XtraEditors.CalcEdit CalcEdit_auto_factor;
        private DevExpress.XtraEditors.LabelControl LabelControl4;
        private DevExpress.XtraEditors.TextEdit TextEdit_description;
        private DevExpress.XtraEditors.LabelControl LabelControl3;
        private DevExpress.XtraEditors.TextEdit textEdit_spanish_description;
        private DevExpress.XtraEditors.LabelControl labelControl1;

    }
}
