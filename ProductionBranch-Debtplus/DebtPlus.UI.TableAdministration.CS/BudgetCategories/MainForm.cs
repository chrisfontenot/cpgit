﻿using System;
using System.Linq;
using DebtPlus.LINQ;
using DebtPlus.UI.Common;

namespace DebtPlus.UI.TableAdministration.CS.BudgetCategories
{
    public partial class MainForm : Templates.MainForm
    {
        // The record collection being edited
        private System.Collections.Generic.List<budget_category> colRecords;

        private BusinessContext bc = null;

        public MainForm()
        {
            InitializeComponent();
            RegisterHandlers();
        }

        private void RegisterHandlers()
        {
            this.Load += MainForm_Load;
        }

        private void UnRegisterHandlers()
        {
            this.Load -= MainForm_Load;
        }

        private void MainForm_Load(object sender, EventArgs e)
        {
            UnRegisterHandlers();
            bc = new BusinessContext();
            try
            {
                using (var cm = new CursorManager())
                {
                    colRecords = bc.budget_categories.ToList();
                    gridControl1.DataSource = colRecords;
                    gridView1.BestFitColumns();
                }
            }
            finally
            {
                RegisterHandlers();
            }
        }

        /// <summary>
        /// Update the record in the database
        /// </summary>
        /// <param name="obj"></param>
        protected override void UpdateRecord(object obj)
        {
            budget_category record = obj as budget_category;
            if (record == null)
            {
                return;
            }

            // Save the ID of the record and do the edit operation. The edit may change the
            // record ID and so we need to know if this has occurred.
            Int32 previousID = record.Id;
            using (var frm = new EditForm(record))
            {
                if (frm.ShowDialog() != System.Windows.Forms.DialogResult.OK)
                {
                    return;
                }
            }

            try
            {
                // Since the ID may not be changed (it may be created, but not changed) then there is no
                // need to worry about it moving. Just update the record with the changed information.
                bc.SubmitChanges();
                gridView1.RefreshData();
            }
            catch (System.Exception ex)
            {
                DebtPlus.UI.Common.ErrorHandling.HandleErrors(ex, "Error updating budget categories");
            }
        }

        /// <summary>
        /// Create a new record for the database
        /// </summary>
        protected override void CreateRecord()
        {
            var record = DebtPlus.LINQ.Factory.Manufacture_budget_category();
            using (var frm = new EditForm(record))
            {
                if (frm.ShowDialog() != System.Windows.Forms.DialogResult.OK)
                {
                    return;
                }
            }

            try
            {
                // Add the new record
                bc.budget_categories.InsertOnSubmit(record);
                bc.SubmitChanges();

                // Refresh the data list
                colRecords.Add(record);
                gridView1.RefreshData();
            }
            catch (System.Data.SqlClient.SqlException ex)
            {
                DebtPlus.UI.Common.ErrorHandling.HandleErrors(ex, "Error updating the record");
            }
        }

        /// <summary>
        /// Delete the current record from the database
        /// </summary>
        /// <param name="obj"></param>
        protected override void DeleteRecord(object obj)
        {
            budget_category record = obj as budget_category;
            if (record == null)
            {
                return;
            }

            if (DebtPlus.Data.Prompts.RequestConfirmation_Delete() != System.Windows.Forms.DialogResult.Yes)
            {
                return;
            }

            // Do the delete operation
            try
            {
                bc.budget_categories.DeleteOnSubmit(record);
                bc.SubmitChanges();
                colRecords.Remove(record);
                gridView1.RefreshData();
            }
            catch (System.Data.SqlClient.SqlException ex)
            {
                DebtPlus.UI.Common.ErrorHandling.HandleErrors(ex, "Error deleting the record");
            }
        }
    }
}