﻿using System;
using DebtPlus.LINQ;

namespace DebtPlus.UI.TableAdministration.CS.Client.BankruptcyClassTypes
{
    internal partial class EditForm : Templates.EditTemplateForm
    {
        private BankruptcyClassType record;

        internal EditForm()
            : base()
        {
            InitializeComponent();
        }

        internal EditForm(BankruptcyClassType record)
            : this()
        {
            this.record = record;
            RegisterHandlers();
        }

        private void RegisterHandlers()
        {
            this.Load += EditForm_Load;
            this.TextEdit_description.TextChanged += Form_Changed;
        }

        private void UnRegisterHandlers()
        {
            this.Load -= EditForm_Load;
            this.TextEdit_description.TextChanged -= Form_Changed;
        }

        private void EditForm_Load(object sender, EventArgs e)
        {
            UnRegisterHandlers();
            try
            {
                LabelControl_ID.Text = String.Empty;
                if (record.Id > 0)
                {
                    LabelControl_ID.Text = record.Id.ToString();
                }
                this.CheckEdit_ActiveFlag.Checked = record.ActiveFlag;
                this.CheckEdit_default.Checked = record.Default;
                this.TextEdit_description.EditValue = record.description;
            }
            finally
            {
                RegisterHandlers();
            }
            simpleButton_OK.Enabled = this.TextEdit_description.Text != String.Empty;
        }

        private void Form_Changed(object sender, EventArgs e)
        {
            simpleButton_OK.Enabled = !string.IsNullOrEmpty(this.TextEdit_description.Text);
        }

        protected override void simpleButton_OK_Click(object sender, EventArgs e)
        {
            record.ActiveFlag = this.CheckEdit_ActiveFlag.Checked;
            record.Default = this.CheckEdit_default.Checked;
            record.description = Convert.ToString(this.TextEdit_description.EditValue);
            base.simpleButton_OK_Click(sender, e);
        }
    }
}