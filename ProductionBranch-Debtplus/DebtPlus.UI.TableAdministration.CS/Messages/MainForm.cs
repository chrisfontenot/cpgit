﻿using System;
using System.Linq;
using System.Windows.Forms;
using DebtPlus.LINQ;

namespace DebtPlus.UI.TableAdministration.CS.Messages
{
    public partial class MainForm : Templates.MainForm
    {
        /// <summary>
        /// List of the records to be edited in the program
        /// </summary>
        private System.Collections.Generic.List<message> colRecords;

        private BusinessContext bc = new BusinessContext();

        /// <summary>
        /// Initialize the new class
        /// </summary>
        /// <param name="dc">Pointer to the data context in the mainline</param>
        public MainForm()
            : base()
        {
            InitializeComponent();
            RegisterHandlers();
        }

        private void RegisterHandlers()
        {
            Load += MainForm_Load;
            LookUpEdit_item_type.EditValueChanged += LookUpEdit_item_type_EditValueChanged;
            LookUpEdit_item_type.ButtonPressed += LookUpEdit_item_type_ButtonPressed;
        }

        private void UnRegisterHandlers()
        {
            Load -= MainForm_Load;
            LookUpEdit_item_type.EditValueChanged -= LookUpEdit_item_type_EditValueChanged;
            LookUpEdit_item_type.ButtonPressed -= LookUpEdit_item_type_ButtonPressed;
        }

        /// <summary>
        /// Process the load event on the form
        /// </summary>
        private void MainForm_Load(object sender, System.EventArgs e)
        {
            UnRegisterHandlers();
            try
            {
                using (var cm = new DebtPlus.UI.Common.CursorManager())
                {
                    colRecords = bc.messages.ToList();
                    gridControl1.DataSource = colRecords;

                    // Build the list of message types
                    LookUpEdit_item_type.Properties.Items.Clear();
                    LookUpEdit_item_type.SelectedIndex = -1;
                    foreach (string newItem in (from r in colRecords select r.item_type.ToUpper()).Distinct())
                    {
                        AddType(newItem);
                    }

                    // Set the grid selection to the subset of items that match the type
                    if (LookUpEdit_item_type.Properties.Items.Count > 0)
                    {
                        LookUpEdit_item_type.SelectedIndex = 0;
                        SetGridFilter((string)((DebtPlus.Data.Controls.ComboboxItem)LookUpEdit_item_type.SelectedItem).value);
                    }
                }
            }
            catch (Exception ex)
            {
                DebtPlus.UI.Common.ErrorHandling.HandleErrors(ex);
            }
            finally
            {
                RegisterHandlers();
            }
        }

        /// <summary>
        /// Process a click of the buttons on the selection group
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void LookUpEdit_item_type_ButtonPressed(object sender, DevExpress.XtraEditors.Controls.ButtonPressedEventArgs e)
        {
            // Look for the PLUS button click event
            if (e.Button.Kind == DevExpress.XtraEditors.Controls.ButtonPredefines.Plus)
            {
                string newItem = string.Empty;
                if (DebtPlus.Data.Forms.InputBox.Show("What is the name for the new section?", ref newItem, "Add New Section", String.Empty, 50) == DialogResult.OK && newItem.Trim() != String.Empty)
                {
                    Int32 newIndex = AddType(newItem);
                    LookUpEdit_item_type.SelectedIndex = newIndex;
                }
            }
        }

        /// <summary>
        /// Add the type to the collection of type strings
        /// </summary>
        /// <param name="typeString"></param>
        private Int32 AddType(string typeString)
        {
            typeString = typeString.ToUpper();
            for (Int32 itemIndex = 0; itemIndex < LookUpEdit_item_type.Properties.Items.Count; ++itemIndex)
            {
                DebtPlus.Data.Controls.ComboboxItem cbx = LookUpEdit_item_type.Properties.Items[itemIndex] as DebtPlus.Data.Controls.ComboboxItem;
                if (string.Compare((string)cbx.value, typeString, true) == 0)
                {
                    return itemIndex;
                }
            }

            // Add the new item to the list
            LookUpEdit_item_type.Properties.Items.Add(new DebtPlus.Data.Controls.ComboboxItem(typeString, typeString));

            // And return the pointer to the new item
            return AddType(typeString);
        }

        /// <summary>
        /// Process a change in the list of item groups
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void LookUpEdit_item_type_EditValueChanged(object sender, EventArgs e)
        {
            string selected = ((DebtPlus.Data.Controls.ComboboxItem)LookUpEdit_item_type.EditValue).value as string;
            SetGridFilter(selected);
        }

        /// <summary>
        /// Update the filter on the selection grid to only show items that are selected.
        /// </summary>
        /// <param name="selected"></param>
        private void SetGridFilter(string selected)
        {
            gridView1.BeginDataUpdate();
            try
            {
                gridView1.ActiveFilter.Clear();
                DevExpress.XtraGrid.Columns.ColumnFilterInfo filter = new DevExpress.XtraGrid.Columns.ColumnFilterInfo(gridColumn_item_type, (object)selected);
                gridView1.ActiveFilter.Add(new DevExpress.XtraGrid.Views.Base.ViewColumnFilterInfo(gridColumn_item_type, filter));
            }
            finally
            {
                gridView1.EndDataUpdate();
            }
        }

        /// <summary>
        /// Create the new record
        /// </summary>
        protected override void CreateRecord()
        {
            // Allocate the new record to be inserted.
            var record = DebtPlus.LINQ.Factory.Manufacture_message();
            record.item_type = DebtPlus.Utils.Nulls.v_String(LookUpEdit_item_type.EditValue) ?? string.Empty;

            // Edit the new record. If OK then attempt to insert the record.
            using (EditForm frm = new EditForm(record))
            {
                if (frm.ShowDialog() != System.Windows.Forms.DialogResult.OK)
                {
                    return;
                }
            }

            colRecords.Add(record);
            bc.messages.InsertOnSubmit(record);

            try
            {
                bc.SubmitChanges();
                gridView1.RefreshData();
            }
            catch (System.Data.SqlClient.SqlException ex)
            {
                DebtPlus.UI.Common.ErrorHandling.HandleErrors(ex);
            }
        }

        /// <summary>
        /// Update the existing record
        /// </summary>
        /// <param name="obj">Pointer to the desired record for the operation</param>
        protected override void UpdateRecord(object obj)
        {
            // Find the record
            var record = obj as message;
            if (obj == null)
            {
                return;
            }

            using (EditForm frm = new EditForm(record))
            {
                if (frm.ShowDialog() != System.Windows.Forms.DialogResult.OK)
                {
                    return;
                }
            }

            try
            {
                bc.SubmitChanges();
                gridView1.RefreshData();
            }
            catch (System.Data.SqlClient.SqlException ex)
            {
                DebtPlus.UI.Common.ErrorHandling.HandleErrors(ex);
            }
        }

        /// <summary>
        /// Delete the indicated record from the database
        /// </summary>
        /// <param name="obj">Pointer to the desired record for the operation</param>
        protected override void DeleteRecord(object obj)
        {
            // Find the record
            var record = obj as message;
            if (obj == null)
            {
                return;
            }

            if (DebtPlus.Data.Prompts.RequestConfirmation_Delete() != System.Windows.Forms.DialogResult.Yes)
            {
                return;
            }

            bc.messages.DeleteOnSubmit(record);
            colRecords.Remove(record);

            try
            {
                bc.SubmitChanges();
                gridView1.RefreshData();
            }
            catch (System.Data.SqlClient.SqlException ex)
            {
                DebtPlus.UI.Common.ErrorHandling.HandleErrors(ex);
            }
        }
    }
}