﻿
using Microsoft.VisualBasic;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Diagnostics;

namespace DebtPlus.UI.TableAdministration.CS.Workshops.workshop_locations
{
    partial class EditWorkshopLocationsForm
    {
		//Form overrides dispose to clean up the component list.
		[System.Diagnostics.DebuggerNonUserCode]
		protected override void Dispose(bool disposing)
		{
			try 
            {
				if (disposing)
                {
                    if (components != null) components.Dispose();
				}
                components = null;
            }
    	    finally
            {
				base.Dispose(disposing);
			}
		}

		//Required by the Windows Form Designer
		private System.ComponentModel.IContainer components = null;

        //NOTE: The following procedure is required by the Windows Form Designer
        //It can be modified using the Windows Form Designer.  
        //Do not modify it using the code editor.
        [System.Diagnostics.DebuggerStepThrough]
        private void InitializeComponent()
        {
            DevExpress.Utils.SerializableAppearanceObject SerializableAppearanceObject1 = new DevExpress.Utils.SerializableAppearanceObject();
            this.LabelControl_ID = new DevExpress.XtraEditors.LabelControl();
            this.LayoutControl1 = new DevExpress.XtraLayout.LayoutControl();
            this.CheckEdit_ActiveFlag = new DevExpress.XtraEditors.CheckEdit();
            this.AddressRecordControl1 = new DebtPlus.Data.Controls.AddressRecordControl();
            this.simpleButton_Cancel = new DevExpress.XtraEditors.SimpleButton();
            this.TextEdit_organization = new DevExpress.XtraEditors.TextEdit();
            this.simpleButton_OK = new DevExpress.XtraEditors.SimpleButton();
            this.TextEdit_name = new DevExpress.XtraEditors.TextEdit();
            this.MemoEdit_directions = new DevExpress.XtraEditors.MemoEdit();
            this.TelephoneNumberRecordControl1 = new DebtPlus.Data.Controls.TelephoneNumberRecordControl();
            this.textedit_milage = new DevExpress.XtraEditors.TextEdit();
            this.LookUpEdit_workshop_organization_type = new DevExpress.XtraEditors.LookUpEdit();
            this.LayoutControlGroup1 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.LayoutControlItem7 = new DevExpress.XtraLayout.LayoutControlItem();
            this.LayoutControlItem6 = new DevExpress.XtraLayout.LayoutControlItem();
            this.LayoutControlItem5 = new DevExpress.XtraLayout.LayoutControlItem();
            this.LayoutControlItem8 = new DevExpress.XtraLayout.LayoutControlItem();
            this.LayoutControlItem2 = new DevExpress.XtraLayout.LayoutControlItem();
            this.LayoutControlItem3 = new DevExpress.XtraLayout.LayoutControlItem();
            this.LayoutControlItem4 = new DevExpress.XtraLayout.LayoutControlItem();
            this.EmptySpaceItem1 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.LayoutControlItem10 = new DevExpress.XtraLayout.LayoutControlItem();
            this.LayoutControlItem9 = new DevExpress.XtraLayout.LayoutControlItem();
            this.LayoutControlItem11 = new DevExpress.XtraLayout.LayoutControlItem();
            this.LayoutControlGroup2 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.LayoutControlItem1 = new DevExpress.XtraLayout.LayoutControlItem();
            this.LabelControl_description = new DevExpress.XtraEditors.LabelControl();
            this.LabelControl4 = new DevExpress.XtraEditors.LabelControl();
            this.LookUpEdit_HCS_ID = new DevExpress.XtraEditors.LookUpEdit();
            this.LayoutControlItem12 = new DevExpress.XtraLayout.LayoutControlItem();
            ((System.ComponentModel.ISupportInitialize)this.DxErrorProvider1).BeginInit();
            ((System.ComponentModel.ISupportInitialize)this.LayoutControl1).BeginInit();
            this.LayoutControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)this.CheckEdit_ActiveFlag.Properties).BeginInit();
            ((System.ComponentModel.ISupportInitialize)this.AddressRecordControl1).BeginInit();
            ((System.ComponentModel.ISupportInitialize)this.TextEdit_organization.Properties).BeginInit();
            ((System.ComponentModel.ISupportInitialize)this.TextEdit_name.Properties).BeginInit();
            ((System.ComponentModel.ISupportInitialize)this.MemoEdit_directions.Properties).BeginInit();
            ((System.ComponentModel.ISupportInitialize)this.TelephoneNumberRecordControl1).BeginInit();
            ((System.ComponentModel.ISupportInitialize)this.textedit_milage.Properties).BeginInit();
            ((System.ComponentModel.ISupportInitialize)this.LookUpEdit_workshop_organization_type.Properties).BeginInit();
            ((System.ComponentModel.ISupportInitialize)this.LayoutControlGroup1).BeginInit();
            ((System.ComponentModel.ISupportInitialize)this.LayoutControlItem7).BeginInit();
            ((System.ComponentModel.ISupportInitialize)this.LayoutControlItem6).BeginInit();
            ((System.ComponentModel.ISupportInitialize)this.LayoutControlItem5).BeginInit();
            ((System.ComponentModel.ISupportInitialize)this.LayoutControlItem8).BeginInit();
            ((System.ComponentModel.ISupportInitialize)this.LayoutControlItem2).BeginInit();
            ((System.ComponentModel.ISupportInitialize)this.LayoutControlItem3).BeginInit();
            ((System.ComponentModel.ISupportInitialize)this.LayoutControlItem4).BeginInit();
            ((System.ComponentModel.ISupportInitialize)this.EmptySpaceItem1).BeginInit();
            ((System.ComponentModel.ISupportInitialize)this.LayoutControlItem10).BeginInit();
            ((System.ComponentModel.ISupportInitialize)this.LayoutControlItem9).BeginInit();
            ((System.ComponentModel.ISupportInitialize)this.LayoutControlItem11).BeginInit();
            ((System.ComponentModel.ISupportInitialize)this.LayoutControlGroup2).BeginInit();
            ((System.ComponentModel.ISupportInitialize)this.LayoutControlItem1).BeginInit();
            ((System.ComponentModel.ISupportInitialize)this.LookUpEdit_HCS_ID.Properties).BeginInit();
            ((System.ComponentModel.ISupportInitialize)this.LayoutControlItem12).BeginInit();
            this.SuspendLayout();
            //
            //LabelControl_ID
            //
            this.LabelControl_ID.Location = new System.Drawing.Point(106, 12);
            this.LabelControl_ID.Name = "LabelControl_ID";
            this.LabelControl_ID.Size = new System.Drawing.Size(445, 13);
            this.LabelControl_ID.StyleController = this.LayoutControl1;
            this.LabelControl_ID.TabIndex = 1;
            //
            //LayoutControl1
            //
            this.LayoutControl1.Controls.Add(this.LookUpEdit_HCS_ID);
            this.LayoutControl1.Controls.Add(this.CheckEdit_ActiveFlag);
            this.LayoutControl1.Controls.Add(this.AddressRecordControl1);
            this.LayoutControl1.Controls.Add(this.simpleButton_Cancel);
            this.LayoutControl1.Controls.Add(this.TextEdit_organization);
            this.LayoutControl1.Controls.Add(this.simpleButton_OK);
            this.LayoutControl1.Controls.Add(this.TextEdit_name);
            this.LayoutControl1.Controls.Add(this.MemoEdit_directions);
            this.LayoutControl1.Controls.Add(this.TelephoneNumberRecordControl1);
            this.LayoutControl1.Controls.Add(this.textedit_milage);
            this.LayoutControl1.Controls.Add(this.LookUpEdit_workshop_organization_type);
            this.LayoutControl1.Controls.Add(this.LabelControl_ID);
            this.LayoutControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.LayoutControl1.Location = new System.Drawing.Point(0, 0);
            this.LayoutControl1.Name = "LayoutControl1";
            this.LayoutControl1.Root = this.LayoutControlGroup1;
            this.LayoutControl1.Size = new System.Drawing.Size(563, 370);
            this.LayoutControl1.TabIndex = 19;
            this.LayoutControl1.Text = "LayoutControl1";
            //
            //CheckEdit_ActiveFlag
            //
            this.CheckEdit_ActiveFlag.Location = new System.Drawing.Point(12, 339);
            this.CheckEdit_ActiveFlag.Name = "CheckEdit_ActiveFlag";
            this.CheckEdit_ActiveFlag.Properties.Caption = "Active";
            this.CheckEdit_ActiveFlag.Size = new System.Drawing.Size(539, 19);
            this.CheckEdit_ActiveFlag.StyleController = this.LayoutControl1;
            this.CheckEdit_ActiveFlag.TabIndex = 20;
            //
            //AddressRecordControl1
            //
            this.AddressRecordControl1.Location = new System.Drawing.Point(106, 101);
            this.AddressRecordControl1.Name = "AddressRecordControl1";
            this.AddressRecordControl1.Size = new System.Drawing.Size(366, 72);
            this.AddressRecordControl1.TabIndex = 19;
            //
            //simpleButton_Cancel
            //
            this.simpleButton_Cancel.Anchor = (System.Windows.Forms.AnchorStyles)(System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right);
            this.simpleButton_Cancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.simpleButton_Cancel.Location = new System.Drawing.Point(476, 56);
            this.simpleButton_Cancel.MaximumSize = new System.Drawing.Size(75, 23);
            this.simpleButton_Cancel.MinimumSize = new System.Drawing.Size(75, 23);
            this.simpleButton_Cancel.Name = "simpleButton_Cancel";
            this.simpleButton_Cancel.Size = new System.Drawing.Size(75, 23);
            this.simpleButton_Cancel.StyleController = this.LayoutControl1;
            this.simpleButton_Cancel.TabIndex = 16;
            this.simpleButton_Cancel.Text = "&Cancel";
            //
            //TextEdit_organization
            //
            this.TextEdit_organization.Anchor = (System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) | System.Windows.Forms.AnchorStyles.Right);
            this.TextEdit_organization.Location = new System.Drawing.Point(106, 53);
            this.TextEdit_organization.Name = "TextEdit_organization";
            this.TextEdit_organization.Properties.MaxLength = 50;
            this.TextEdit_organization.Size = new System.Drawing.Size(366, 20);
            this.TextEdit_organization.StyleController = this.LayoutControl1;
            this.TextEdit_organization.TabIndex = 5;
            //
            //simpleButton_OK
            //
            this.simpleButton_OK.Anchor = (System.Windows.Forms.AnchorStyles)(System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right);
            this.simpleButton_OK.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.simpleButton_OK.Location = new System.Drawing.Point(476, 29);
            this.simpleButton_OK.MaximumSize = new System.Drawing.Size(75, 23);
            this.simpleButton_OK.MinimumSize = new System.Drawing.Size(75, 23);
            this.simpleButton_OK.Name = "simpleButton_OK";
            this.simpleButton_OK.Size = new System.Drawing.Size(75, 23);
            this.simpleButton_OK.StyleController = this.LayoutControl1;
            this.simpleButton_OK.TabIndex = 15;
            this.simpleButton_OK.Text = "&OK";
            //
            //TextEdit_name
            //
            this.TextEdit_name.Anchor = (System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) | System.Windows.Forms.AnchorStyles.Right);
            this.TextEdit_name.Location = new System.Drawing.Point(106, 29);
            this.TextEdit_name.Name = "TextEdit_name";
            this.TextEdit_name.Properties.MaxLength = 50;
            this.TextEdit_name.Size = new System.Drawing.Size(366, 20);
            this.TextEdit_name.StyleController = this.LayoutControl1;
            this.TextEdit_name.TabIndex = 3;
            //
            //MemoEdit_directions
            //
            this.MemoEdit_directions.Location = new System.Drawing.Point(25, 243);
            this.MemoEdit_directions.Name = "MemoEdit_directions";
            this.MemoEdit_directions.Size = new System.Drawing.Size(513, 79);
            this.MemoEdit_directions.StyleController = this.LayoutControl1;
            this.MemoEdit_directions.TabIndex = 18;
            //
            //TelephoneNumberRecordControl1
            //
            this.TelephoneNumberRecordControl1.Location = new System.Drawing.Point(106, 177);
            this.TelephoneNumberRecordControl1.Margin = new System.Windows.Forms.Padding(0);
            this.TelephoneNumberRecordControl1.Name = "TelephoneNumberRecordControl1";
            this.TelephoneNumberRecordControl1.Size = new System.Drawing.Size(135, 20);
            this.TelephoneNumberRecordControl1.TabIndex = 17;
            //
            //textedit_milage
            //
            this.textedit_milage.Anchor = (System.Windows.Forms.AnchorStyles)(System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right);
            this.textedit_milage.Location = new System.Drawing.Point(339, 177);
            this.textedit_milage.Name = "textedit_milage";
            this.textedit_milage.Properties.Appearance.Options.UseTextOptions = true;
            this.textedit_milage.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.textedit_milage.Properties.Mask.BeepOnError = true;
            this.textedit_milage.Properties.Mask.EditMask = "f";
            this.textedit_milage.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.textedit_milage.Size = new System.Drawing.Size(133, 20);
            this.textedit_milage.StyleController = this.LayoutControl1;
            this.textedit_milage.TabIndex = 13;
            //
            //LookUpEdit_workshop_organization_type
            //
            this.LookUpEdit_workshop_organization_type.Anchor = (System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) | System.Windows.Forms.AnchorStyles.Right);
            this.LookUpEdit_workshop_organization_type.Location = new System.Drawing.Point(106, 77);
            this.LookUpEdit_workshop_organization_type.Name = "LookUpEdit_workshop_organization_type";
            this.LookUpEdit_workshop_organization_type.Properties.ActionButtonIndex = 1;
            this.LookUpEdit_workshop_organization_type.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
				new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Plus, "", -1, true, true, false, DevExpress.XtraEditors.ImageLocation.MiddleCenter, null, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), SerializableAppearanceObject1,
				"Add a new type...", null, null, true),
				new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)
			});
            this.LookUpEdit_workshop_organization_type.Properties.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
				new DevExpress.XtraEditors.Controls.LookUpColumnInfo("organization_type", "ID", 20, DevExpress.Utils.FormatType.Numeric, "f0", false, DevExpress.Utils.HorzAlignment.Default),
				new DevExpress.XtraEditors.Controls.LookUpColumnInfo("name", "Description", 20, DevExpress.Utils.FormatType.None, "", true, DevExpress.Utils.HorzAlignment.Default, DevExpress.Data.ColumnSortOrder.Ascending)
			});
            this.LookUpEdit_workshop_organization_type.Properties.NullText = "";
            this.LookUpEdit_workshop_organization_type.Properties.ShowFooter = false;
            this.LookUpEdit_workshop_organization_type.Properties.ShowHeader = false;
            this.LookUpEdit_workshop_organization_type.Properties.SortColumnIndex = 1;
            this.LookUpEdit_workshop_organization_type.Size = new System.Drawing.Size(162, 20);
            this.LookUpEdit_workshop_organization_type.StyleController = this.LayoutControl1;
            this.LookUpEdit_workshop_organization_type.TabIndex = 7;
            //
            //LayoutControlGroup1
            //
            this.LayoutControlGroup1.CustomizationFormText = "LayoutControlGroup1";
            this.LayoutControlGroup1.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.True;
            this.LayoutControlGroup1.GroupBordersVisible = false;
            this.LayoutControlGroup1.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
				this.LayoutControlItem7,
				this.LayoutControlItem6,
				this.LayoutControlItem5,
				this.LayoutControlItem8,
				this.LayoutControlItem2,
				this.LayoutControlItem3,
				this.LayoutControlItem4,
				this.EmptySpaceItem1,
				this.LayoutControlItem10,
				this.LayoutControlItem9,
				this.LayoutControlItem11,
				this.LayoutControlGroup2,
				this.LayoutControlItem12
			});
            this.LayoutControlGroup1.Location = new System.Drawing.Point(0, 0);
            this.LayoutControlGroup1.Name = "LayoutControlGroup1";
            this.LayoutControlGroup1.Size = new System.Drawing.Size(563, 370);
            this.LayoutControlGroup1.Text = "LayoutControlGroup1";
            this.LayoutControlGroup1.TextVisible = false;
            //
            //LayoutControlItem7
            //
            this.LayoutControlItem7.Control = this.TextEdit_name;
            this.LayoutControlItem7.CustomizationFormText = "Name";
            this.LayoutControlItem7.Location = new System.Drawing.Point(0, 17);
            this.LayoutControlItem7.Name = "LayoutControlItem7";
            this.LayoutControlItem7.Size = new System.Drawing.Size(464, 24);
            this.LayoutControlItem7.Text = "Name";
            this.LayoutControlItem7.TextSize = new System.Drawing.Size(90, 13);
            //
            //LayoutControlItem6
            //
            this.LayoutControlItem6.Control = this.TextEdit_organization;
            this.LayoutControlItem6.CustomizationFormText = "Organization";
            this.LayoutControlItem6.Location = new System.Drawing.Point(0, 41);
            this.LayoutControlItem6.Name = "LayoutControlItem6";
            this.LayoutControlItem6.Size = new System.Drawing.Size(464, 24);
            this.LayoutControlItem6.Text = "Organization";
            this.LayoutControlItem6.TextSize = new System.Drawing.Size(90, 13);
            //
            //LayoutControlItem5
            //
            this.LayoutControlItem5.Control = this.LookUpEdit_workshop_organization_type;
            this.LayoutControlItem5.CustomizationFormText = "Organization Type";
            this.LayoutControlItem5.Location = new System.Drawing.Point(0, 65);
            this.LayoutControlItem5.Name = "LayoutControlItem5";
            this.LayoutControlItem5.Size = new System.Drawing.Size(260, 24);
            this.LayoutControlItem5.Text = "Organization Type";
            this.LayoutControlItem5.TextSize = new System.Drawing.Size(90, 13);
            //
            //LayoutControlItem8
            //
            this.LayoutControlItem8.Control = this.LabelControl_ID;
            this.LayoutControlItem8.CustomizationFormText = "ID";
            this.LayoutControlItem8.Location = new System.Drawing.Point(0, 0);
            this.LayoutControlItem8.Name = "LayoutControlItem8";
            this.LayoutControlItem8.Size = new System.Drawing.Size(543, 17);
            this.LayoutControlItem8.Text = "ID";
            this.LayoutControlItem8.TextSize = new System.Drawing.Size(90, 13);
            //
            //LayoutControlItem2
            //
            this.LayoutControlItem2.Control = this.TelephoneNumberRecordControl1;
            this.LayoutControlItem2.CustomizationFormText = "Telephone Number";
            this.LayoutControlItem2.Location = new System.Drawing.Point(0, 165);
            this.LayoutControlItem2.Name = "LayoutControlItem2";
            this.LayoutControlItem2.Size = new System.Drawing.Size(233, 24);
            this.LayoutControlItem2.Text = "Telephone Number";
            this.LayoutControlItem2.TextSize = new System.Drawing.Size(90, 13);
            //
            //LayoutControlItem3
            //
            this.LayoutControlItem3.AppearanceItemCaption.Options.UseTextOptions = true;
            this.LayoutControlItem3.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.LayoutControlItem3.Control = this.textedit_milage;
            this.LayoutControlItem3.CustomizationFormText = "Milage";
            this.LayoutControlItem3.Location = new System.Drawing.Point(233, 165);
            this.LayoutControlItem3.Name = "LayoutControlItem3";
            this.LayoutControlItem3.Size = new System.Drawing.Size(231, 24);
            this.LayoutControlItem3.Text = "Mileage";
            this.LayoutControlItem3.TextSize = new System.Drawing.Size(90, 13);
            //
            //LayoutControlItem4
            //
            this.LayoutControlItem4.AppearanceItemCaption.Options.UseTextOptions = true;
            this.LayoutControlItem4.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.LayoutControlItem4.AppearanceItemCaption.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Top;
            this.LayoutControlItem4.Control = this.AddressRecordControl1;
            this.LayoutControlItem4.CustomizationFormText = "Address";
            this.LayoutControlItem4.Location = new System.Drawing.Point(0, 89);
            this.LayoutControlItem4.Name = "LayoutControlItem4";
            this.LayoutControlItem4.Size = new System.Drawing.Size(464, 76);
            this.LayoutControlItem4.Text = "Address";
            this.LayoutControlItem4.TextSize = new System.Drawing.Size(90, 13);
            //
            //EmptySpaceItem1
            //
            this.EmptySpaceItem1.AllowHotTrack = false;
            this.EmptySpaceItem1.CustomizationFormText = "EmptySpaceItem1";
            this.EmptySpaceItem1.Location = new System.Drawing.Point(0, 189);
            this.EmptySpaceItem1.MaxSize = new System.Drawing.Size(0, 10);
            this.EmptySpaceItem1.MinSize = new System.Drawing.Size(10, 10);
            this.EmptySpaceItem1.Name = "EmptySpaceItem1";
            this.EmptySpaceItem1.Size = new System.Drawing.Size(464, 10);
            this.EmptySpaceItem1.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.EmptySpaceItem1.Text = "EmptySpaceItem1";
            this.EmptySpaceItem1.TextSize = new System.Drawing.Size(0, 0);
            //
            //LayoutControlItem10
            //
            this.LayoutControlItem10.Control = this.simpleButton_Cancel;
            this.LayoutControlItem10.CustomizationFormText = "CANCEL button";
            this.LayoutControlItem10.Location = new System.Drawing.Point(464, 44);
            this.LayoutControlItem10.Name = "LayoutControlItem10";
            this.LayoutControlItem10.Size = new System.Drawing.Size(79, 155);
            this.LayoutControlItem10.Text = "CANCEL button";
            this.LayoutControlItem10.TextSize = new System.Drawing.Size(0, 0);
            this.LayoutControlItem10.TextToControlDistance = 0;
            this.LayoutControlItem10.TextVisible = false;
            //
            //LayoutControlItem9
            //
            this.LayoutControlItem9.Control = this.simpleButton_OK;
            this.LayoutControlItem9.CustomizationFormText = "OK Button";
            this.LayoutControlItem9.Location = new System.Drawing.Point(464, 17);
            this.LayoutControlItem9.Name = "LayoutControlItem9";
            this.LayoutControlItem9.Size = new System.Drawing.Size(79, 27);
            this.LayoutControlItem9.Text = "OK Button";
            this.LayoutControlItem9.TextSize = new System.Drawing.Size(0, 0);
            this.LayoutControlItem9.TextToControlDistance = 0;
            this.LayoutControlItem9.TextVisible = false;
            //
            //LayoutControlItem11
            //
            this.LayoutControlItem11.Control = this.CheckEdit_ActiveFlag;
            this.LayoutControlItem11.CustomizationFormText = "Active?";
            this.LayoutControlItem11.Location = new System.Drawing.Point(0, 327);
            this.LayoutControlItem11.Name = "LayoutControlItem11";
            this.LayoutControlItem11.Size = new System.Drawing.Size(543, 23);
            this.LayoutControlItem11.Text = "Active?";
            this.LayoutControlItem11.TextSize = new System.Drawing.Size(0, 0);
            this.LayoutControlItem11.TextToControlDistance = 0;
            this.LayoutControlItem11.TextVisible = false;
            //
            //LayoutControlGroup2
            //
            this.LayoutControlGroup2.CustomizationFormText = "Driving Directions";
            this.LayoutControlGroup2.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] { this.LayoutControlItem1 });
            this.LayoutControlGroup2.Location = new System.Drawing.Point(0, 199);
            this.LayoutControlGroup2.Name = "LayoutControlGroup2";
            this.LayoutControlGroup2.Size = new System.Drawing.Size(543, 128);
            this.LayoutControlGroup2.Text = "Driving Directions";
            //
            //LayoutControlItem1
            //
            this.LayoutControlItem1.AppearanceItemCaption.Options.UseTextOptions = true;
            this.LayoutControlItem1.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.LayoutControlItem1.AppearanceItemCaption.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Top;
            this.LayoutControlItem1.Control = this.MemoEdit_directions;
            this.LayoutControlItem1.CustomizationFormText = "Directions";
            this.LayoutControlItem1.Location = new System.Drawing.Point(0, 0);
            this.LayoutControlItem1.Name = "LayoutControlItem1";
            this.LayoutControlItem1.Size = new System.Drawing.Size(517, 83);
            this.LayoutControlItem1.Text = "Directions";
            this.LayoutControlItem1.TextSize = new System.Drawing.Size(0, 0);
            this.LayoutControlItem1.TextToControlDistance = 0;
            this.LayoutControlItem1.TextVisible = false;
            //
            //LabelControl_description
            //
            this.LabelControl_description.Location = new System.Drawing.Point(12, 199);
            this.LabelControl_description.Name = "LabelControl_description";
            this.LabelControl_description.Size = new System.Drawing.Size(90, 13);
            this.LabelControl_description.TabIndex = 10;
            this.LabelControl_description.Text = "Telephone Number";
            //
            //LabelControl4
            //
            this.LabelControl4.Anchor = (System.Windows.Forms.AnchorStyles)(System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right);
            this.LabelControl4.Location = new System.Drawing.Point(406, 199);
            this.LabelControl4.Name = "LabelControl4";
            this.LabelControl4.Size = new System.Drawing.Size(30, 13);
            this.LabelControl4.TabIndex = 12;
            this.LabelControl4.Text = "Mileage";
            //
            //LookUpEdit_HCS_ID
            //
            this.LookUpEdit_HCS_ID.Location = new System.Drawing.Point(311, 77);
            this.LookUpEdit_HCS_ID.Name = "LookUpEdit_HCS_ID";
            this.LookUpEdit_HCS_ID.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] { new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo) });
            this.LookUpEdit_HCS_ID.Properties.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
				new DevExpress.XtraEditors.Controls.LookUpColumnInfo("Id", "ID", 5, DevExpress.Utils.FormatType.Numeric, "f0", true, DevExpress.Utils.HorzAlignment.Far, DevExpress.Data.ColumnSortOrder.None, DevExpress.Utils.DefaultBoolean.True),
				new DevExpress.XtraEditors.Controls.LookUpColumnInfo("description", "Description", 20, DevExpress.Utils.FormatType.None, "", true, DevExpress.Utils.HorzAlignment.Near, DevExpress.Data.ColumnSortOrder.Ascending, DevExpress.Utils.DefaultBoolean.True)
			});
            this.LookUpEdit_HCS_ID.Properties.DisplayMember = "description";
            this.LookUpEdit_HCS_ID.Properties.NullText = "";
            this.LookUpEdit_HCS_ID.Properties.ShowFooter = false;
            this.LookUpEdit_HCS_ID.Properties.SortColumnIndex = 1;
            this.LookUpEdit_HCS_ID.Properties.ValueMember = "Id";
            this.LookUpEdit_HCS_ID.Size = new System.Drawing.Size(161, 20);
            this.LookUpEdit_HCS_ID.StyleController = this.LayoutControl1;
            this.LookUpEdit_HCS_ID.TabIndex = 21;
            //
            //LayoutControlItem12
            //
            this.LayoutControlItem12.Control = this.LookUpEdit_HCS_ID;
            this.LayoutControlItem12.CustomizationFormText = "HCS ID";
            this.LayoutControlItem12.Location = new System.Drawing.Point(260, 65);
            this.LayoutControlItem12.Name = "LayoutControlItem12";
            this.LayoutControlItem12.Size = new System.Drawing.Size(204, 24);
            this.LayoutControlItem12.Text = "HCS ID";
            this.LayoutControlItem12.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.AutoSize;
            this.LayoutControlItem12.TextSize = new System.Drawing.Size(34, 13);
            this.LayoutControlItem12.TextToControlDistance = 5;
            //
            this.LookUpEdit_workshop_organization_type.Properties.DisplayMember = "name";
            this.LookUpEdit_workshop_organization_type.Properties.ValueMember = "Id";
            //
            //EditWorkshopLocationsForm
            //
            this.AcceptButton = this.simpleButton_OK;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6f, 13f);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.CancelButton = this.simpleButton_Cancel;
            this.ClientSize = new System.Drawing.Size(563, 370);
            this.Controls.Add(this.LayoutControl1);
            this.Controls.Add(this.LabelControl4);
            this.Controls.Add(this.LabelControl_description);
            this.Name = "EditWorkshopLocationsForm";
            this.Text = "Workshop Location Record";
            ((System.ComponentModel.ISupportInitialize)this.DxErrorProvider1).EndInit();
            ((System.ComponentModel.ISupportInitialize)this.LayoutControl1).EndInit();
            this.LayoutControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)this.CheckEdit_ActiveFlag.Properties).EndInit();
            ((System.ComponentModel.ISupportInitialize)this.AddressRecordControl1).EndInit();
            ((System.ComponentModel.ISupportInitialize)this.TextEdit_organization.Properties).EndInit();
            ((System.ComponentModel.ISupportInitialize)this.TextEdit_name.Properties).EndInit();
            ((System.ComponentModel.ISupportInitialize)this.MemoEdit_directions.Properties).EndInit();
            ((System.ComponentModel.ISupportInitialize)this.TelephoneNumberRecordControl1).EndInit();
            ((System.ComponentModel.ISupportInitialize)this.textedit_milage.Properties).EndInit();
            ((System.ComponentModel.ISupportInitialize)this.LookUpEdit_workshop_organization_type.Properties).EndInit();
            ((System.ComponentModel.ISupportInitialize)this.LayoutControlGroup1).EndInit();
            ((System.ComponentModel.ISupportInitialize)this.LayoutControlItem7).EndInit();
            ((System.ComponentModel.ISupportInitialize)this.LayoutControlItem6).EndInit();
            ((System.ComponentModel.ISupportInitialize)this.LayoutControlItem5).EndInit();
            ((System.ComponentModel.ISupportInitialize)this.LayoutControlItem8).EndInit();
            ((System.ComponentModel.ISupportInitialize)this.LayoutControlItem2).EndInit();
            ((System.ComponentModel.ISupportInitialize)this.LayoutControlItem3).EndInit();
            ((System.ComponentModel.ISupportInitialize)this.LayoutControlItem4).EndInit();
            ((System.ComponentModel.ISupportInitialize)this.EmptySpaceItem1).EndInit();
            ((System.ComponentModel.ISupportInitialize)this.LayoutControlItem10).EndInit();
            ((System.ComponentModel.ISupportInitialize)this.LayoutControlItem9).EndInit();
            ((System.ComponentModel.ISupportInitialize)this.LayoutControlItem11).EndInit();
            ((System.ComponentModel.ISupportInitialize)this.LayoutControlGroup2).EndInit();
            ((System.ComponentModel.ISupportInitialize)this.LayoutControlItem1).EndInit();
            ((System.ComponentModel.ISupportInitialize)this.LookUpEdit_HCS_ID.Properties).EndInit();
            ((System.ComponentModel.ISupportInitialize)this.LayoutControlItem12).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }
        private DevExpress.XtraEditors.LabelControl LabelControl_ID;
        private DevExpress.XtraEditors.LabelControl LabelControl_description;
        private DevExpress.XtraEditors.LookUpEdit LookUpEdit_workshop_organization_type;
        private DevExpress.XtraEditors.LabelControl LabelControl4;
        private DevExpress.XtraEditors.SimpleButton simpleButton_OK;
        private DevExpress.XtraEditors.SimpleButton simpleButton_Cancel;
        private DevExpress.XtraEditors.TextEdit TextEdit_name;
        private DevExpress.XtraEditors.TextEdit textedit_milage;
        private DevExpress.XtraEditors.TextEdit TextEdit_organization;
        private DebtPlus.Data.Controls.TelephoneNumberRecordControl TelephoneNumberRecordControl1;
        private DevExpress.XtraEditors.MemoEdit MemoEdit_directions;
        private DevExpress.XtraLayout.LayoutControl LayoutControl1;
        private DevExpress.XtraLayout.LayoutControlGroup LayoutControlGroup1;
        private DevExpress.XtraLayout.LayoutControlItem LayoutControlItem1;
        private DebtPlus.Data.Controls.AddressRecordControl AddressRecordControl1;
        private DevExpress.XtraLayout.LayoutControlGroup LayoutControlGroup2;
        private DevExpress.XtraLayout.LayoutControlItem LayoutControlItem2;
        private DevExpress.XtraLayout.LayoutControlItem LayoutControlItem3;
        private DevExpress.XtraLayout.LayoutControlItem LayoutControlItem4;
        private DevExpress.XtraLayout.LayoutControlItem LayoutControlItem5;
        private DevExpress.XtraLayout.LayoutControlItem LayoutControlItem6;
        private DevExpress.XtraLayout.LayoutControlItem LayoutControlItem7;
        private DevExpress.XtraLayout.LayoutControlItem LayoutControlItem8;
        private DevExpress.XtraLayout.LayoutControlItem LayoutControlItem9;
        private DevExpress.XtraLayout.LayoutControlItem LayoutControlItem10;
        private DevExpress.XtraLayout.EmptySpaceItem EmptySpaceItem1;
        private DevExpress.XtraEditors.CheckEdit CheckEdit_ActiveFlag;
        private DevExpress.XtraLayout.LayoutControlItem LayoutControlItem11;
        private DevExpress.XtraEditors.LookUpEdit LookUpEdit_HCS_ID;
        private DevExpress.XtraLayout.LayoutControlItem LayoutControlItem12;
    }
}

