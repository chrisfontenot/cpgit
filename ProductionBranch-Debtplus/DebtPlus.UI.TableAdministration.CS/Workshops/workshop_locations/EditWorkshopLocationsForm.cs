#region "Copyright 2000-2012 DebtPlus, L.L.C."
//{*******************************************************************}
//{                                                                   }
//{       DebtPlus Debt Management System                             }
//{                                                                   }
//{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
//{       ALL RIGHTS RESERVED                                         }
//{                                                                   }
//{   The entire contents of this file is protected by U.S. and       }
//{   International Copyright Laws. Unauthorized reproduction,        }
//{   reverse-engineering, and distribution of all or any portion of  }
//{   the code contained in this file is strictly prohibited and may  }
//{   result in severe civil and criminal penalties and will be       }
//{   prosecuted to the maximum extent possible under the law.        }
//{                                                                   }
//{   RESTRICTIONS                                                    }
//{                                                                   }
//{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
//{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
//{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
//{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
//{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
//{                                                                   }
//{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
//{   ADDITIONAL RESTRICTIONS.                                        }
//{                                                                   }
//{*******************************************************************}

#endregion "Copyright 2000-2012 DebtPlus, L.L.C."

using System;
using System.Linq;
using DebtPlus.Data.Forms;
using DebtPlus.LINQ;
using DevExpress.XtraEditors.Controls;

namespace DebtPlus.UI.TableAdministration.CS.Workshops.workshop_locations
{
    internal partial class EditWorkshopLocationsForm : DebtPlusForm
    {
        private workshop_location record = null;
        private BusinessContext bc = null;
        private System.Collections.Generic.List<workshop_organization_type> colOrgTypes = null;

        internal EditWorkshopLocationsForm()
            : base()
        {
            InitializeComponent();
        }

        internal EditWorkshopLocationsForm(BusinessContext bc, workshop_location record)
            : this()
        {
            this.bc = bc;
            this.record = record;
            RegisterHandlers();
        }

        /// <summary>
        /// Register the event handlers
        /// </summary>
        private void RegisterHandlers()
        {
            simpleButton_OK.Click += simpleButton_OK_Click;
            Load += EditForm_Load;
            LookUpEdit_workshop_organization_type.ButtonClick += LookUpEdit_workshop_organization_type_ButtonClick;
            TextEdit_name.EditValueChanged += FormChanged;
            TextEdit_organization.EditValueChanged += FormChanged;
            textedit_milage.EditValueChanged += FormChanged;
            MemoEdit_directions.EditValueChanged += FormChanged;
            CheckEdit_ActiveFlag.EditValueChanged += FormChanged;
            LookUpEdit_HCS_ID.EditValueChanged += FormChanged;
            LookUpEdit_workshop_organization_type.EditValueChanged += FormChanged;
        }

        /// <summary>
        /// Remove the event handler registrations
        /// </summary>
        private void UnRegiserHandlers()
        {
            simpleButton_OK.Click -= simpleButton_OK_Click;
            Load -= EditForm_Load;
            LookUpEdit_workshop_organization_type.ButtonClick -= LookUpEdit_workshop_organization_type_ButtonClick;
            TextEdit_name.EditValueChanged -= FormChanged;
            TextEdit_organization.EditValueChanged -= FormChanged;
            textedit_milage.EditValueChanged -= FormChanged;
            MemoEdit_directions.EditValueChanged -= FormChanged;
            CheckEdit_ActiveFlag.EditValueChanged -= FormChanged;
            LookUpEdit_HCS_ID.EditValueChanged -= FormChanged;
            LookUpEdit_workshop_organization_type.EditValueChanged -= FormChanged;
        }

        /// <summary>
        /// Process the form LOAD sequence
        /// </summary>
        private void EditForm_Load(object sender, EventArgs e)
        {
            UnRegiserHandlers();
            try
            {
                // Get a list of the organization types. We can change this list here so we need it from the database directly.
                colOrgTypes = bc.workshop_organization_types.ToList();

                // Define the lookup control records
                LookUpEdit_HCS_ID.Properties.DataSource = DebtPlus.LINQ.Cache.Housing_ARM_hcs_id.getList();
                LookUpEdit_workshop_organization_type.Properties.DataSource = colOrgTypes;

                // Set the display controls with the record contents
                LabelControl_ID.Text = record.Id < 1 ? string.Empty : record.Id.ToString();

                TextEdit_name.EditValue = record.name;
                TextEdit_organization.EditValue = record.organization;
                textedit_milage.EditValue = record.milage;
                MemoEdit_directions.EditValue = record.directions;
                CheckEdit_ActiveFlag.EditValue = record.ActiveFlag;
                LookUpEdit_workshop_organization_type.EditValue = record.type;
                AddressRecordControl1.EditValue = record.AddressID;
                TelephoneNumberRecordControl1.EditValue = record.TelephoneID;
                LookUpEdit_HCS_ID.EditValue = record.hcs_id;

                simpleButton_OK.Enabled = !HasErrors();
            }
            catch (System.Data.SqlClient.SqlException ex)
            {
                DebtPlus.UI.Common.ErrorHandling.HandleErrors(ex);
            }
            finally
            {
                RegisterHandlers();
            }
        }

        /// <summary>
        /// Process a change in the input controls.
        /// </summary>
        private void FormChanged(object Sender, EventArgs e)
        {
            simpleButton_OK.Enabled = !HasErrors();
        }

        /// <summary>
        /// Determine if the input form has any error conditions
        /// </summary>
        /// <returns></returns>
        private bool HasErrors()
        {
            if (string.IsNullOrWhiteSpace(TextEdit_name.Text))
            {
                return true;
            }

            if (LookUpEdit_HCS_ID.EditValue == null)
            {
                return true;
            }

            return false;
        }

        /// <summary>
        /// Handle the click of the ADD button to the organization list.
        /// </summary>
        private void LookUpEdit_workshop_organization_type_ButtonClick(object sender, ButtonPressedEventArgs e)
        {
            // If the item is "+" then add a new description
            if (e.Button.Kind != ButtonPredefines.Plus)
            {
                return;
            }

            // Add the organization type to the system.
            var newRecord = DebtPlus.LINQ.Factory.Manufacture_workshop_organization_type();

            // Request the edit of the workshop location.
            string NewDescription = string.Empty;
            if (InputBox.Show("What is the new type description?", ref NewDescription, "Referral Source Types", string.Empty, 50) != System.Windows.Forms.DialogResult.OK)
            {
                return;
            }

            // Update the name. It must not be empty.
            newRecord.name = NewDescription.Trim();
            if (string.IsNullOrEmpty(newRecord.name))
            {
                return;
            }

            try
            {
                // Submit the new record to the database.
                bc.workshop_organization_types.InsertOnSubmit(newRecord);
                bc.SubmitChanges();
                colOrgTypes.Add(newRecord);

                // Reset the list of organization types
                LookUpEdit_workshop_organization_type.Properties.DataSource = colOrgTypes;
                LookUpEdit_workshop_organization_type.Properties.ForceInitialize();

                // Reset the ID value and force the control to re-initialize with the new list.
                LookUpEdit_workshop_organization_type.EditValue = newRecord.Id;
                LookUpEdit_workshop_organization_type.RefreshEditValue();
            }
            catch (System.Data.SqlClient.SqlException ex)
            {
                DebtPlus.UI.Common.ErrorHandling.HandleErrors(ex);
            }
        }

        /// <summary>
        /// Handle the CLICK event on the OK button
        /// </summary>
        private void simpleButton_OK_Click(object sender, EventArgs e)
        {
            record.name = DebtPlus.Utils.Nulls.v_String(TextEdit_name.EditValue);
            record.organization = DebtPlus.Utils.Nulls.v_String(TextEdit_organization.EditValue);
            record.milage = DebtPlus.Utils.Nulls.v_Double(textedit_milage.EditValue);
            record.directions = DebtPlus.Utils.Nulls.v_String(MemoEdit_directions.EditValue);
            record.ActiveFlag = CheckEdit_ActiveFlag.Checked;
            record.type = DebtPlus.Utils.Nulls.v_Int32(LookUpEdit_workshop_organization_type.EditValue);
            record.AddressID = AddressRecordControl1.EditValue;
            record.TelephoneID = TelephoneNumberRecordControl1.EditValue;
            record.hcs_id = DebtPlus.Utils.Nulls.v_Int32(LookUpEdit_HCS_ID.EditValue);
        }
    }
}