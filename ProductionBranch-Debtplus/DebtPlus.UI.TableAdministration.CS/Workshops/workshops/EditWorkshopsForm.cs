#region "Copyright 2000-2012 DebtPlus, L.L.C."
//{*******************************************************************}
//{                                                                   }
//{       DebtPlus Debt Management System                             }
//{                                                                   }
//{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
//{       ALL RIGHTS RESERVED                                         }
//{                                                                   }
//{   The entire contents of this file is protected by U.S. and       }
//{   International Copyright Laws. Unauthorized reproduction,        }
//{   reverse-engineering, and distribution of all or any portion of  }
//{   the code contained in this file is strictly prohibited and may  }
//{   result in severe civil and criminal penalties and will be       }
//{   prosecuted to the maximum extent possible under the law.        }
//{                                                                   }
//{   RESTRICTIONS                                                    }
//{                                                                   }
//{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
//{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
//{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
//{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
//{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
//{                                                                   }
//{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
//{   ADDITIONAL RESTRICTIONS.                                        }
//{                                                                   }
//{*******************************************************************}

#endregion "Copyright 2000-2012 DebtPlus, L.L.C."

using System;
using System.Linq;
using DebtPlus.LINQ;
using DebtPlus.UI.TableAdministration.CS.Workshops.workshop_locations;
using DebtPlus.UI.TableAdministration.CS.Workshops.workshop_types;
using DevExpress.XtraEditors.Controls;

namespace DebtPlus.UI.TableAdministration.CS.Workshops.workshops
{
    partial class EditWorkshopForm : Templates.EditTemplateForm
    {
        private workshop workshopRecord = null;
        private BusinessContext bc = null;
        private System.Collections.Generic.List<workshop_location> colLocations = null;
        private System.Collections.Generic.List<workshop_type> colTypes = null;

        internal EditWorkshopForm()
            : base()
        {
            InitializeComponent();
        }

        internal EditWorkshopForm(BusinessContext bc, workshop workshopRecord)
            : this()
        {
            this.workshopRecord = workshopRecord;
            this.bc = bc;
            RegisterHandlers();
        }

        private void RegisterHandlers()
        {
            simpleButton_OK.Click += simpleButton_OK_Click;
            Load += EditForm_Load;
            LookUpEdit_counselor.EditValueChanging += DebtPlus.Data.Validation.LookUpEdit_ActiveTest;
            LookUpEdit_HUD_Grant.EditValueChanging += DebtPlus.Data.Validation.LookUpEdit_ActiveTest;
            SpinEdit_seats.EditValueChanging += SpinEdit_seats_EditValueChanging;
            CalcEdit_HousingFeeAmount.Validating += DebtPlus.Data.Validation.NonNegative;
            TextEdit_GuestName.Validated += TextEdit_GuestName_Validated;
            TextEdit_GuestName.Validating += TextEdit_GuestName_Validating;
            LookUpEdit_workshop_location.ButtonClick += LookUpEdit_workshop_location_ButtonClick;
            LookUpEdit_workshop_location.EditValueChanging += DebtPlus.Data.Validation.LookUpEdit_ActiveTest;
            LookUpEdit_workshop_type.ButtonClick += LookUpEdit_workshop_type_ButtonClick;
            LookUpEdit_workshop_type.EditValueChanging += DebtPlus.Data.Validation.LookUpEdit_ActiveTest;
            LookUpEdit_workshop_type.EditValueChanged += LookUpEdit_workshop_type_EditValueChanged;

            LookUpEdit_counselor.EditValueChanged += FieldChanged;
            LookUpEdit_HUD_Grant.EditValueChanged += FieldChanged;
            LookUpEdit_workshop_location.EditValueChanged += FieldChanged;
            LookUpEdit_workshop_type.EditValueChanged += FieldChanged;
            SpinEdit_seats.EditValueChanged += FieldChanged;
        }

        private void UnRegisterHandlers()
        {
            simpleButton_OK.Click -= simpleButton_OK_Click;
            Load -= EditForm_Load;
            LookUpEdit_counselor.EditValueChanging -= DebtPlus.Data.Validation.LookUpEdit_ActiveTest;
            LookUpEdit_HUD_Grant.EditValueChanging -= DebtPlus.Data.Validation.LookUpEdit_ActiveTest;
            SpinEdit_seats.EditValueChanging -= SpinEdit_seats_EditValueChanging;
            CalcEdit_HousingFeeAmount.Validating -= DebtPlus.Data.Validation.NonNegative;
            TextEdit_GuestName.Validated -= TextEdit_GuestName_Validated;
            TextEdit_GuestName.Validating -= TextEdit_GuestName_Validating;
            LookUpEdit_workshop_location.ButtonClick -= LookUpEdit_workshop_location_ButtonClick;
            LookUpEdit_workshop_location.EditValueChanging -= DebtPlus.Data.Validation.LookUpEdit_ActiveTest;
            LookUpEdit_workshop_type.ButtonClick -= LookUpEdit_workshop_type_ButtonClick;
            LookUpEdit_workshop_type.EditValueChanging -= DebtPlus.Data.Validation.LookUpEdit_ActiveTest;
            LookUpEdit_workshop_type.EditValueChanged -= LookUpEdit_workshop_type_EditValueChanged;

            LookUpEdit_counselor.EditValueChanged -= FieldChanged;
            LookUpEdit_HUD_Grant.EditValueChanged -= FieldChanged;
            LookUpEdit_workshop_location.EditValueChanged -= FieldChanged;
            LookUpEdit_workshop_type.EditValueChanged -= FieldChanged;
            SpinEdit_seats.EditValueChanged -= FieldChanged;
        }

        private void EditForm_Load(object sender, EventArgs e)
        {
            UnRegisterHandlers();
            try
            {
                colLocations = bc.workshop_locations.ToList();
                colTypes = bc.workshop_types.ToList();

                // Bind the lookup controls to the lists
                LookUpEdit_workshop_type.Properties.DataSource = colTypes;
                LookUpEdit_workshop_location.Properties.DataSource = colLocations;
                LookUpEdit_counselor.Properties.DataSource = DebtPlus.LINQ.Cache.counselor.EducatorsList();
                LookUpEdit_HUD_Grant.Properties.DataSource = DebtPlus.LINQ.Cache.Housing_GrantType.getList();

                // Set the other controls from the record
                LookUpEdit_workshop_type.EditValue = workshopRecord.workshop_type;
                LookUpEdit_counselor.EditValue = workshopRecord.CounselorID;
                LookUpEdit_HUD_Grant.EditValue = workshopRecord.HUD_Grant;
                LabelControl_ID.Text = workshopRecord.Id < 1 ? string.Empty : workshopRecord.Id.ToString();
                DateEdit_starting_time.EditValue = workshopRecord.start_time;
                SpinEdit_seats.EditValue = workshopRecord.seats_available;
                LookUpEdit_workshop_location.EditValue = workshopRecord.workshop_location;
                CalcEdit_HousingFeeAmount.EditValue = workshopRecord.HousingFeeAmount;
                guestId = workshopRecord.Guest;

                // Enable the OK button and leave
                simpleButton_OK.Enabled = !HasErrors();
            }
            finally
            {
                RegisterHandlers();
            }
        }

        /// <summary>
        /// Process a change in the data control to enable the OK button.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void FieldChanged(object sender, EventArgs e)
        {
            simpleButton_OK.Enabled = !HasErrors();
        }

        /// <summary>
        /// Determine if there is an error condition that would prevent the workshop
        /// from being created. We don't enable the OK unless the required items are
        /// supplied.
        /// </summary>
        /// <returns></returns>
        private bool HasErrors()
        {
            if (Convert.ToInt32(SpinEdit_seats.EditValue) <= 0)
            {
                return true;
            }

            if (LookUpEdit_workshop_location.EditValue == null)
            {
                return true;
            }

            if (LookUpEdit_workshop_type.EditValue == null)
            {
                return true;
            }

            return false;
        }

        /// <summary>
        /// Process a button press on the workshop type control
        /// </summary>
        private void LookUpEdit_workshop_type_ButtonClick(object sender, ButtonPressedEventArgs e)
        {
            // If the item is "+" then add a new description
            if (e.Button.Kind == ButtonPredefines.Plus)
            {
                AddNewType();
            }
        }

        /// <summary>
        /// Add a new workshop type to the collection of types
        /// </summary>
        private void AddNewType()
        {
            // Allocate a new record. Do not use a HUD ID unless one is chosen.
            var newTypeRecord = DebtPlus.LINQ.Factory.Manufacture_workshop_type();

            using (var frm = new EditWorkshopTypesForm(bc, newTypeRecord))
            {
                if (frm.ShowDialog() != System.Windows.Forms.DialogResult.OK)
                {
                    return;
                }
            }

            // Save the workshop type before the form is dismissed
            bc.workshop_types.InsertOnSubmit(newTypeRecord);
            bc.SubmitChanges();

            // Add the new record to the collection and set the value into the control
            colTypes.Add(newTypeRecord);
            LookUpEdit_workshop_type.Properties.DataSource = colTypes;
            LookUpEdit_workshop_type.Properties.ForceInitialize();
            LookUpEdit_workshop_type.EditValue = newTypeRecord.Id;
        }

        /// <summary>
        /// Process a button click event on the location control
        /// </summary>
        private void LookUpEdit_workshop_location_ButtonClick(object sender, ButtonPressedEventArgs e)
        {
            UnRegisterHandlers();
            try
            {
                // If this is the "+" button then add a new location
                if (e.Button.Kind == ButtonPredefines.Plus)
                {
                    AddNewLocation();
                    return;
                }

                // If this is the "^" button then edit the location
                if (e.Button.Kind == ButtonPredefines.Up)
                {
                    EditLocation();
                    return;
                }
            }
            finally
            {
                RegisterHandlers();
            }
        }

        /// <summary>
        /// Edit the location record
        /// </summary>
        private void EditLocation()
        {
            // Obtain the current record for the edit operation
            var locationRecord = LookUpEdit_workshop_location.GetSelectedDataRow() as DebtPlus.LINQ.workshop_location;
            if (locationRecord == null)
            {
                return;
            }

            // Do the edit operation
            using (var frm = new EditWorkshopLocationsForm(bc, locationRecord))
            {
                if (frm.ShowDialog() != System.Windows.Forms.DialogResult.OK)
                {
                    return;
                }
            }

            try
            {
                bc.SubmitChanges();
                LookUpEdit_workshop_location.RefreshEditValue();
            }
            catch (System.Data.SqlClient.SqlException ex)
            {
                DebtPlus.UI.Common.ErrorHandling.HandleErrors(ex);
            }
        }

        /// <summary>
        /// Add a new location record into the collection
        /// </summary>
        private void AddNewLocation()
        {
            // Allocate a new record. Do not use a HUD ID unless one is chosen.
            var newLocationRecord = DebtPlus.LINQ.Factory.Manufacture_workshop_location();

            using (var frm = new EditWorkshopLocationsForm(bc, newLocationRecord))
            {
                if (frm.ShowDialog() != System.Windows.Forms.DialogResult.OK)
                {
                    return;
                }
            }

            // Save the workshop type before the form is dismissed
            bc.workshop_locations.InsertOnSubmit(newLocationRecord);
            bc.SubmitChanges();

            // Add the new record to the collection and set the value into the control
            colLocations.Add(newLocationRecord);
            LookUpEdit_workshop_location.Properties.DataSource = null;
            LookUpEdit_workshop_location.Properties.DataSource = colLocations;
            LookUpEdit_workshop_location.EditValue = newLocationRecord.Id;
        }

        private void SpinEdit_seats_EditValueChanging(object sender, ChangingEventArgs e)
        {
            if (DebtPlus.Utils.Nulls.DInt(e.NewValue) <= 0)
            {
                e.Cancel = true;
            }
        }

        /// <summary>
        /// Process a change in the workshop type
        /// </summary>
        private void LookUpEdit_workshop_type_EditValueChanged(object sender, EventArgs e)
        {
            // Obtain the current record for the edit operation
            var typeRecord = LookUpEdit_workshop_type.GetSelectedDataRow() as DebtPlus.LINQ.workshop_type;
            if (typeRecord == null)
            {
                return;
            }

            // Default the grant information from the type record
            if (LookUpEdit_HUD_Grant.EditValue == null)
            {
                LookUpEdit_HUD_Grant.EditValue = typeRecord.HUD_Grant;
            }

            // Default the housing record amount from the record
            if (CalcEdit_HousingFeeAmount.EditValue == null)
            {
                CalcEdit_HousingFeeAmount.EditValue = typeRecord.HousingFeeAmount;
            }
        }

        /// <summary>
        /// Process the OK button CLICK event
        /// </summary>
        protected override void simpleButton_OK_Click(object sender, EventArgs e)
        {
            base.simpleButton_OK_Click(sender, e);

            workshopRecord.workshop_type = Convert.ToInt32(LookUpEdit_workshop_type.EditValue);
            workshopRecord.CounselorID = DebtPlus.Utils.Nulls.v_Int32(LookUpEdit_counselor.EditValue);
            workshopRecord.HUD_Grant = DebtPlus.Utils.Nulls.v_Int32(LookUpEdit_HUD_Grant.EditValue);
            workshopRecord.start_time = Convert.ToDateTime(DateEdit_starting_time.EditValue);
            workshopRecord.seats_available = Convert.ToInt32(SpinEdit_seats.EditValue);
            workshopRecord.workshop_location = Convert.ToInt32(LookUpEdit_workshop_location.EditValue);
            workshopRecord.HousingFeeAmount = Convert.ToDecimal(CalcEdit_HousingFeeAmount.EditValue);
            workshopRecord.Guest = guestId;
        }
    }
}