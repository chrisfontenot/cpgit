using System;
using System.Linq;
using DebtPlus.LINQ;

namespace DebtPlus.UI.TableAdministration.CS.Workshops.workshops
{
    partial class EditWorkshopForm
    {
        /// <summary>
        /// Values retrieved from the database for the guest
        /// </summary>
        private workshop_guest original_values;

        /// <summary>
        /// Record pointer to the guest record
        /// </summary>
        private Int32? guestId
        {
            get
            {
                return SaveRecord();
            }
            set
            {
                original_values = getRecord(value);
                SetCurrentValues(original_values);
            }
        }

        /// <summary>
        /// Get the current values from the control
        /// </summary>
        /// <returns></returns>
        private workshop_guest GetCurrentValues()
        {
            return new workshop_guest()
            {
                Id = original_values.Id,
                Name = DebtPlus.Utils.Nulls.v_String(TextEdit_GuestName.EditValue),
                Email = DebtPlus.Utils.Nulls.v_String(TextEdit_GuestEmail.EditValue),
                Telephone = DebtPlus.Utils.Nulls.v_String(TextEdit_GuestPhone.EditValue)
            };
        }

        /// <summary>
        /// Give the record pointer, obtain the current values from the database
        /// </summary>
        /// <param name="currentRecord"></param>
        /// <returns></returns>
        private workshop_guest getRecord(Int32? currentRecord)
        {
            Int32 Id = currentRecord.GetValueOrDefault(0);

            // Return the record form the database if there is a record pointer
            if (Id > 0)
            {
                var q = (from wg in bc.workshop_guests where wg.Id == Id select wg).FirstOrDefault();
                if (q != null)
                {
                    return q;
                }
            }

            // Return an empty record
            return new workshop_guest();
        }

        /// <summary>
        /// Update the control with the current values
        /// </summary>
        /// <param name="currentValues"></param>
        private void SetCurrentValues(workshop_guest currentValues)
        {
            TextEdit_GuestEmail.EditValue = currentValues.Email;
            TextEdit_GuestName.EditValue = currentValues.Name;
            TextEdit_GuestPhone.EditValue = currentValues.Telephone;
        }

        /// <summary>
        /// Save the record and return the record ID
        /// </summary>
        /// <returns></returns>
        private Int32? SaveRecord()
        {
            workshop_guest currentValues = GetCurrentValues();

            // If there is a pointer then update the record with the new values.
            if (currentValues.Id > 0)
            {
                var q = (from wg in bc.workshop_guests where wg.Id == currentValues.Id select wg).FirstOrDefault();
                if (q != null)
                {
                    q.Email = currentValues.Email;
                    q.Name = currentValues.Name;
                    q.Telephone = currentValues.Telephone;
                    bc.SubmitChanges();
                    return q.Id;
                }
            }

            // If there is no record then insert a new one if the current values have some entry
            if (!string.IsNullOrWhiteSpace(currentValues.Email) ||
                !string.IsNullOrWhiteSpace(currentValues.Name) ||
                !string.IsNullOrWhiteSpace(currentValues.Telephone))
            {
                bc.workshop_guests.InsertOnSubmit(currentValues);
                bc.SubmitChanges();
                return currentValues.Id;
            }

            // The current record is empty. Return the EMPTY record pointer.
            return new Int32?();
        }

        /// <summary>
        /// Process the validation for the guest name.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void TextEdit_GuestName_Validated(object sender, EventArgs e)
        {
            // Read the appropriate item from the database
            var q = ReadGuestSpeakerByName(TextEdit_GuestName.Text.Trim());

            // If there is a match then use the current values for the new record.
            if (q != null)
            {
                original_values = q;
                SetCurrentValues(original_values);
            }
        }

        /// <summary>
        /// Retrieve the current values given the speaker name
        /// </summary>
        private workshop_guest ReadGuestSpeakerByName(string SearchName)
        {
            using (var bc = new BusinessContext())
            {
                string SearchString = SearchName.Replace(" ", "").ToUpper();
                return (from wg in bc.workshop_guests where wg.SearchString == SearchString select wg).FirstOrDefault();
            }
        }

        /// <summary>
        /// Remove the spaces around the guest speaker name when it is complete
        /// </summary>
        private void TextEdit_GuestName_Validating(object sender, System.ComponentModel.CancelEventArgs e)
        {
            TextEdit_GuestName.Text = TextEdit_GuestName.Text.Trim();
        }
    }
}