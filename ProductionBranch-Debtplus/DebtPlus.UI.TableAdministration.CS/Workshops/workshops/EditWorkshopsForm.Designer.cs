﻿
using Microsoft.VisualBasic;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Diagnostics;
namespace DebtPlus.UI.TableAdministration.CS.Workshops.workshops
{
    partial class EditWorkshopForm
    {
		//Form overrides dispose to clean up the component list.
		[System.Diagnostics.DebuggerNonUserCode]
		protected override void Dispose(bool disposing)
		{
			try 
            {
				if (disposing)
                {
                    if (components != null) components.Dispose();
				}
                components = null;
            }
    	    finally
            {
				base.Dispose(disposing);
			}
		}

		//Required by the Windows Form Designer
		private System.ComponentModel.IContainer components = null;

        //NOTE: The following procedure is required by the Windows Form Designer
        //It can be modified using the Windows Form Designer.  
        //Do not modify it using the code editor.
        [System.Diagnostics.DebuggerStepThrough]
        private void InitializeComponent()
        {
            DevExpress.Utils.SerializableAppearanceObject SerializableAppearanceObject1 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject SerializableAppearanceObject2 = new DevExpress.Utils.SerializableAppearanceObject();
            this.LabelControl_ID = new DevExpress.XtraEditors.LabelControl();
            this.LayoutControl1 = new DevExpress.XtraLayout.LayoutControl();
            this.TextEdit_GuestPhone = new DevExpress.XtraEditors.TextEdit();
            this.TextEdit_GuestEmail = new DevExpress.XtraEditors.TextEdit();
            this.TextEdit_GuestName = new DevExpress.XtraEditors.TextEdit();
            this.DateEdit_starting_time = new DevExpress.XtraEditors.DateEdit();
            this.CalcEdit_HousingFeeAmount = new DevExpress.XtraEditors.CalcEdit();
            this.LookUpEdit_workshop_type = new DevExpress.XtraEditors.LookUpEdit();
            this.SpinEdit_seats = new DevExpress.XtraEditors.SpinEdit();
            this.LookUpEdit_workshop_location = new DevExpress.XtraEditors.LookUpEdit();
            this.LookUpEdit_HUD_Grant = new DevExpress.XtraEditors.LookUpEdit();
            this.LookUpEdit_counselor = new DevExpress.XtraEditors.LookUpEdit();
            this.LayoutControlGroup1 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.LayoutControlItem1 = new DevExpress.XtraLayout.LayoutControlItem();
            this.LayoutControlItem2 = new DevExpress.XtraLayout.LayoutControlItem();
            this.LayoutControlItem3 = new DevExpress.XtraLayout.LayoutControlItem();
            this.LayoutControlItem4 = new DevExpress.XtraLayout.LayoutControlItem();
            this.LayoutControlItem10 = new DevExpress.XtraLayout.LayoutControlItem();
            this.LayoutControlItem7 = new DevExpress.XtraLayout.LayoutControlItem();
            this.EmptySpaceItem1 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.LayoutControlItem9 = new DevExpress.XtraLayout.LayoutControlItem();
            this.LayoutControlItem8 = new DevExpress.XtraLayout.LayoutControlItem();
            this.LayoutControlGroup2 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.LayoutControlItem5 = new DevExpress.XtraLayout.LayoutControlItem();
            this.LayoutControlItem6 = new DevExpress.XtraLayout.LayoutControlItem();
            this.LayoutControlItem11 = new DevExpress.XtraLayout.LayoutControlItem();
            ((System.ComponentModel.ISupportInitialize)this.DxErrorProvider1).BeginInit();
            ((System.ComponentModel.ISupportInitialize)this.LayoutControl1).BeginInit();
            this.LayoutControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)this.TextEdit_GuestPhone.Properties).BeginInit();
            ((System.ComponentModel.ISupportInitialize)this.TextEdit_GuestEmail.Properties).BeginInit();
            ((System.ComponentModel.ISupportInitialize)this.TextEdit_GuestName.Properties).BeginInit();
            ((System.ComponentModel.ISupportInitialize)this.DateEdit_starting_time.Properties.VistaTimeProperties).BeginInit();
            ((System.ComponentModel.ISupportInitialize)this.DateEdit_starting_time.Properties).BeginInit();
            ((System.ComponentModel.ISupportInitialize)this.CalcEdit_HousingFeeAmount.Properties).BeginInit();
            ((System.ComponentModel.ISupportInitialize)this.LookUpEdit_workshop_type.Properties).BeginInit();
            ((System.ComponentModel.ISupportInitialize)this.SpinEdit_seats.Properties).BeginInit();
            ((System.ComponentModel.ISupportInitialize)this.LookUpEdit_workshop_location.Properties).BeginInit();
            ((System.ComponentModel.ISupportInitialize)this.LookUpEdit_HUD_Grant.Properties).BeginInit();
            ((System.ComponentModel.ISupportInitialize)this.LookUpEdit_counselor.Properties).BeginInit();
            ((System.ComponentModel.ISupportInitialize)this.LayoutControlGroup1).BeginInit();
            ((System.ComponentModel.ISupportInitialize)this.LayoutControlItem1).BeginInit();
            ((System.ComponentModel.ISupportInitialize)this.LayoutControlItem2).BeginInit();
            ((System.ComponentModel.ISupportInitialize)this.LayoutControlItem3).BeginInit();
            ((System.ComponentModel.ISupportInitialize)this.LayoutControlItem4).BeginInit();
            ((System.ComponentModel.ISupportInitialize)this.LayoutControlItem10).BeginInit();
            ((System.ComponentModel.ISupportInitialize)this.LayoutControlItem7).BeginInit();
            ((System.ComponentModel.ISupportInitialize)this.EmptySpaceItem1).BeginInit();
            ((System.ComponentModel.ISupportInitialize)this.LayoutControlItem9).BeginInit();
            ((System.ComponentModel.ISupportInitialize)this.LayoutControlItem8).BeginInit();
            ((System.ComponentModel.ISupportInitialize)this.LayoutControlGroup2).BeginInit();
            ((System.ComponentModel.ISupportInitialize)this.LayoutControlItem5).BeginInit();
            ((System.ComponentModel.ISupportInitialize)this.LayoutControlItem6).BeginInit();
            ((System.ComponentModel.ISupportInitialize)this.LayoutControlItem11).BeginInit();
            this.SuspendLayout();
            //
            //simpleButton_OK
            //
            this.simpleButton_OK.Location = new System.Drawing.Point(372, 13);
            this.simpleButton_OK.TabIndex = 13;
            //
            //simpleButton_Cancel
            //
            this.simpleButton_Cancel.Location = new System.Drawing.Point(372, 56);
            this.simpleButton_Cancel.TabIndex = 14;
            //
            //LabelControl_ID
            //
            this.LabelControl_ID.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.LabelControl_ID.Location = new System.Drawing.Point(89, 12);
            this.LabelControl_ID.Name = "LabelControl_ID";
            this.LabelControl_ID.Size = new System.Drawing.Size(91, 13);
            this.LabelControl_ID.StyleController = this.LayoutControl1;
            this.LabelControl_ID.TabIndex = 1;
            //
            //LayoutControl1
            //
            this.LayoutControl1.Anchor = (System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) | System.Windows.Forms.AnchorStyles.Left) | System.Windows.Forms.AnchorStyles.Right);
            this.LayoutControl1.Controls.Add(this.TextEdit_GuestPhone);
            this.LayoutControl1.Controls.Add(this.TextEdit_GuestEmail);
            this.LayoutControl1.Controls.Add(this.TextEdit_GuestName);
            this.LayoutControl1.Controls.Add(this.DateEdit_starting_time);
            this.LayoutControl1.Controls.Add(this.CalcEdit_HousingFeeAmount);
            this.LayoutControl1.Controls.Add(this.LabelControl_ID);
            this.LayoutControl1.Controls.Add(this.LookUpEdit_workshop_type);
            this.LayoutControl1.Controls.Add(this.SpinEdit_seats);
            this.LayoutControl1.Controls.Add(this.LookUpEdit_workshop_location);
            this.LayoutControl1.Controls.Add(this.LookUpEdit_HUD_Grant);
            this.LayoutControl1.Controls.Add(this.LookUpEdit_counselor);
            this.LayoutControl1.Location = new System.Drawing.Point(0, 0);
            this.LayoutControl1.Name = "LayoutControl1";
            this.LayoutControl1.OptionsCustomizationForm.DesignTimeCustomizationFormPositionAndSize = new System.Drawing.Rectangle(447, 310, 250, 350);
            this.LayoutControl1.Root = this.LayoutControlGroup1;
            this.LayoutControl1.Size = new System.Drawing.Size(365, 301);
            this.LayoutControl1.TabIndex = 15;
            this.LayoutControl1.Text = "LayoutControl1";
            //
            //TextEdit_GuestPhone
            //
            this.TextEdit_GuestPhone.Location = new System.Drawing.Point(101, 253);
            this.TextEdit_GuestPhone.Name = "TextEdit_GuestPhone";
            this.TextEdit_GuestPhone.Properties.MaxLength = 50;
            this.TextEdit_GuestPhone.Size = new System.Drawing.Size(240, 20);
            this.TextEdit_GuestPhone.StyleController = this.LayoutControl1;
            this.TextEdit_GuestPhone.TabIndex = 18;
            //
            //TextEdit_GuestEmail
            //
            this.TextEdit_GuestEmail.Location = new System.Drawing.Point(101, 229);
            this.TextEdit_GuestEmail.Name = "TextEdit_GuestEmail";
            this.TextEdit_GuestEmail.Properties.MaxLength = 50;
            this.TextEdit_GuestEmail.Size = new System.Drawing.Size(240, 20);
            this.TextEdit_GuestEmail.StyleController = this.LayoutControl1;
            this.TextEdit_GuestEmail.TabIndex = 17;
            //
            //TextEdit_GuestName
            //
            this.TextEdit_GuestName.Location = new System.Drawing.Point(101, 205);
            this.TextEdit_GuestName.Name = "TextEdit_GuestName";
            this.TextEdit_GuestName.Properties.MaxLength = 50;
            this.TextEdit_GuestName.Size = new System.Drawing.Size(240, 20);
            this.TextEdit_GuestName.StyleController = this.LayoutControl1;
            this.TextEdit_GuestName.TabIndex = 16;
            //
            //DateEdit_starting_time
            //
            this.DateEdit_starting_time.EditValue = null;
            this.DateEdit_starting_time.Location = new System.Drawing.Point(89, 101);
            this.DateEdit_starting_time.Name = "DateEdit_starting_time";
            this.DateEdit_starting_time.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.False;
            this.DateEdit_starting_time.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] { new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo) });
            this.DateEdit_starting_time.Properties.DisplayFormat.FormatString = "{0:ddd M/dd/yyyy} at {0:h:mm tt}";
            this.DateEdit_starting_time.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.DateEdit_starting_time.Properties.EditFormat.FormatString = "{0:M/dd/yyyy} at {0:h:mm tt}";
            this.DateEdit_starting_time.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.DateEdit_starting_time.Properties.Mask.EditMask = "M/dd/yyyy \\a\\t h:mm tt";
            this.DateEdit_starting_time.Properties.VistaDisplayMode = DevExpress.Utils.DefaultBoolean.True;
            this.DateEdit_starting_time.Properties.VistaEditTime = DevExpress.Utils.DefaultBoolean.True;
            this.DateEdit_starting_time.Properties.VistaTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] { new DevExpress.XtraEditors.Controls.EditorButton() });
            this.DateEdit_starting_time.Size = new System.Drawing.Size(264, 20);
            this.DateEdit_starting_time.StyleController = this.LayoutControl1;
            this.DateEdit_starting_time.TabIndex = 15;
            this.DateEdit_starting_time.ToolTip = "When does the workshop start?";
            //
            //CalcEdit_HousingFeeAmount
            //
            this.CalcEdit_HousingFeeAmount.Location = new System.Drawing.Point(261, 125);
            this.CalcEdit_HousingFeeAmount.Name = "CalcEdit_HousingFeeAmount";
            this.CalcEdit_HousingFeeAmount.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] { new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo) });
            this.CalcEdit_HousingFeeAmount.Properties.DisplayFormat.FormatString = "c";
            this.CalcEdit_HousingFeeAmount.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.CalcEdit_HousingFeeAmount.Properties.EditFormat.FormatString = "c";
            this.CalcEdit_HousingFeeAmount.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.CalcEdit_HousingFeeAmount.Properties.Mask.BeepOnError = true;
            this.CalcEdit_HousingFeeAmount.Properties.Mask.EditMask = "c";
            this.CalcEdit_HousingFeeAmount.Properties.Precision = 2;
            this.CalcEdit_HousingFeeAmount.Size = new System.Drawing.Size(92, 20);
            this.CalcEdit_HousingFeeAmount.StyleController = this.LayoutControl1;
            this.CalcEdit_HousingFeeAmount.TabIndex = 14;
            this.CalcEdit_HousingFeeAmount.ToolTip = "If you charge a record, what is the amount for each client?";
            //
            //LookUpEdit_workshop_type
            //
            this.LookUpEdit_workshop_type.Location = new System.Drawing.Point(89, 29);
            this.LookUpEdit_workshop_type.Name = "LookUpEdit_workshop_type";
            this.LookUpEdit_workshop_type.Properties.ActionButtonIndex = 1;
            this.LookUpEdit_workshop_type.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
				new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Plus, "", -1, true, true, false, DevExpress.XtraEditors.ImageLocation.MiddleCenter, null, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), SerializableAppearanceObject1,
				"Add a new type...", null, null, true),
				new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)
			});
            this.LookUpEdit_workshop_type.Properties.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
				new DevExpress.XtraEditors.Controls.LookUpColumnInfo("Id", "ID", 20, DevExpress.Utils.FormatType.Numeric, "f0", false, DevExpress.Utils.HorzAlignment.Default),
				new DevExpress.XtraEditors.Controls.LookUpColumnInfo("description", "Description", 20, DevExpress.Utils.FormatType.None, "", true, DevExpress.Utils.HorzAlignment.Default, DevExpress.Data.ColumnSortOrder.Ascending)
			});
            this.LookUpEdit_workshop_type.Properties.NullText = "";
            this.LookUpEdit_workshop_type.Properties.ShowFooter = false;
            this.LookUpEdit_workshop_type.Properties.ShowHeader = false;
            this.LookUpEdit_workshop_type.Properties.SortColumnIndex = 1;
            this.LookUpEdit_workshop_type.Size = new System.Drawing.Size(264, 20);
            this.LookUpEdit_workshop_type.StyleController = this.LayoutControl1;
            this.LookUpEdit_workshop_type.TabIndex = 3;
            //
            //SpinEdit_seats
            //
            this.SpinEdit_seats.EditValue = new decimal(new Int32[] {
				0,
				0,
				0,
				0
			});
            this.SpinEdit_seats.Location = new System.Drawing.Point(89, 125);
            this.SpinEdit_seats.Name = "SpinEdit_seats";
            this.SpinEdit_seats.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] { new DevExpress.XtraEditors.Controls.EditorButton() });
            this.SpinEdit_seats.Properties.IsFloatValue = false;
            this.SpinEdit_seats.Properties.Mask.EditMask = "N00";
            this.SpinEdit_seats.Size = new System.Drawing.Size(91, 20);
            this.SpinEdit_seats.StyleController = this.LayoutControl1;
            this.SpinEdit_seats.TabIndex = 12;
            //
            //LookUpEdit_workshop_location
            //
            this.LookUpEdit_workshop_location.Location = new System.Drawing.Point(89, 53);
            this.LookUpEdit_workshop_location.Name = "LookUpEdit_workshop_location";
            this.LookUpEdit_workshop_location.Properties.ActionButtonIndex = 2;
            this.LookUpEdit_workshop_location.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
				new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Up),
				new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Plus, "", -1, true, true, false, DevExpress.XtraEditors.ImageLocation.MiddleCenter, null, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), SerializableAppearanceObject2,
				"Add a new location...", null, null, true),
				new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)
			});
            this.LookUpEdit_workshop_location.Properties.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
				new DevExpress.XtraEditors.Controls.LookUpColumnInfo("Id", "ID", 20, DevExpress.Utils.FormatType.Numeric, "f0", false, DevExpress.Utils.HorzAlignment.Default),
				new DevExpress.XtraEditors.Controls.LookUpColumnInfo("name", "Description", 20, DevExpress.Utils.FormatType.None, "", true, DevExpress.Utils.HorzAlignment.Default, DevExpress.Data.ColumnSortOrder.Ascending)
			});
            this.LookUpEdit_workshop_location.Properties.NullText = "";
            this.LookUpEdit_workshop_location.Properties.ShowFooter = false;
            this.LookUpEdit_workshop_location.Properties.ShowHeader = false;
            this.LookUpEdit_workshop_location.Properties.SortColumnIndex = 1;
            this.LookUpEdit_workshop_location.Size = new System.Drawing.Size(264, 20);
            this.LookUpEdit_workshop_location.StyleController = this.LayoutControl1;
            this.LookUpEdit_workshop_location.TabIndex = 5;
            //
            //LookUpEdit_HUD_Grant
            //
            this.LookUpEdit_HUD_Grant.Location = new System.Drawing.Point(89, 149);
            this.LookUpEdit_HUD_Grant.Name = "LookUpEdit_HUD_Grant";
            this.LookUpEdit_HUD_Grant.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.True;
            this.LookUpEdit_HUD_Grant.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] { new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo) });
            this.LookUpEdit_HUD_Grant.Properties.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
				new DevExpress.XtraEditors.Controls.LookUpColumnInfo("Id", "ID", 20, DevExpress.Utils.FormatType.Numeric, "f0", false, DevExpress.Utils.HorzAlignment.Default),
				new DevExpress.XtraEditors.Controls.LookUpColumnInfo("description", "Description", 20, DevExpress.Utils.FormatType.None, "", true, DevExpress.Utils.HorzAlignment.Default, DevExpress.Data.ColumnSortOrder.Ascending)
			});
            this.LookUpEdit_HUD_Grant.Properties.DisplayMember = "description";
            this.LookUpEdit_HUD_Grant.Properties.NullText = "";
            this.LookUpEdit_HUD_Grant.Properties.ShowFooter = false;
            this.LookUpEdit_HUD_Grant.Properties.ShowHeader = false;
            this.LookUpEdit_HUD_Grant.Properties.SortColumnIndex = 1;
            this.LookUpEdit_HUD_Grant.Properties.ValueMember = "Id";
            this.LookUpEdit_HUD_Grant.Size = new System.Drawing.Size(264, 20);
            this.LookUpEdit_HUD_Grant.StyleController = this.LayoutControl1;
            this.LookUpEdit_HUD_Grant.TabIndex = 13;
            //
            //LookUpEdit_counselor
            //
            this.LookUpEdit_counselor.Location = new System.Drawing.Point(89, 77);
            this.LookUpEdit_counselor.Name = "LookUpEdit_counselor";
            this.LookUpEdit_counselor.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] { new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo) });
            this.LookUpEdit_counselor.Properties.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
				new DevExpress.XtraEditors.Controls.LookUpColumnInfo("Id", "ID", 20, DevExpress.Utils.FormatType.Numeric, "f0", false, DevExpress.Utils.HorzAlignment.Default),
				new DevExpress.XtraEditors.Controls.LookUpColumnInfo("Name", "Description", 20, DevExpress.Utils.FormatType.None, "", true, DevExpress.Utils.HorzAlignment.Default, DevExpress.Data.ColumnSortOrder.Ascending)
			});
            this.LookUpEdit_counselor.Properties.NullText = "";
            this.LookUpEdit_counselor.Properties.ShowFooter = false;
            this.LookUpEdit_counselor.Properties.ShowHeader = false;
            this.LookUpEdit_counselor.Properties.SortColumnIndex = 1;
            this.LookUpEdit_counselor.Size = new System.Drawing.Size(264, 20);
            this.LookUpEdit_counselor.StyleController = this.LayoutControl1;
            this.LookUpEdit_counselor.TabIndex = 7;
            //
            //LayoutControlGroup1
            //
            this.LayoutControlGroup1.CustomizationFormText = "LayoutControlGroup1";
            this.LayoutControlGroup1.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.True;
            this.LayoutControlGroup1.GroupBordersVisible = false;
            this.LayoutControlGroup1.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
				this.LayoutControlItem1,
				this.LayoutControlItem2,
				this.LayoutControlItem3,
				this.LayoutControlItem4,
				this.LayoutControlItem10,
				this.LayoutControlItem7,
				this.EmptySpaceItem1,
				this.LayoutControlItem9,
				this.LayoutControlItem8,
				this.LayoutControlGroup2
			});
            this.LayoutControlGroup1.Location = new System.Drawing.Point(0, 0);
            this.LayoutControlGroup1.Name = "LayoutControlGroup1";
            this.LayoutControlGroup1.Size = new System.Drawing.Size(365, 301);
            this.LayoutControlGroup1.Text = "LayoutControlGroup1";
            this.LayoutControlGroup1.TextVisible = false;
            //
            //LayoutControlItem1
            //
            this.LayoutControlItem1.Control = this.LabelControl_ID;
            this.LayoutControlItem1.CustomizationFormText = "ID";
            this.LayoutControlItem1.Location = new System.Drawing.Point(0, 0);
            this.LayoutControlItem1.Name = "LayoutControlItem1";
            this.LayoutControlItem1.Size = new System.Drawing.Size(172, 17);
            this.LayoutControlItem1.Text = "ID";
            this.LayoutControlItem1.TextSize = new System.Drawing.Size(73, 13);
            //
            //LayoutControlItem2
            //
            this.LayoutControlItem2.Control = this.LookUpEdit_workshop_type;
            this.LayoutControlItem2.CustomizationFormText = "Type";
            this.LayoutControlItem2.Location = new System.Drawing.Point(0, 17);
            this.LayoutControlItem2.Name = "LayoutControlItem2";
            this.LayoutControlItem2.Size = new System.Drawing.Size(345, 24);
            this.LayoutControlItem2.Text = "Type";
            this.LayoutControlItem2.TextSize = new System.Drawing.Size(73, 13);
            //
            //LayoutControlItem3
            //
            this.LayoutControlItem3.Control = this.LookUpEdit_workshop_location;
            this.LayoutControlItem3.CustomizationFormText = "Location";
            this.LayoutControlItem3.Location = new System.Drawing.Point(0, 41);
            this.LayoutControlItem3.Name = "LayoutControlItem3";
            this.LayoutControlItem3.Size = new System.Drawing.Size(345, 24);
            this.LayoutControlItem3.Text = "Location";
            this.LayoutControlItem3.TextSize = new System.Drawing.Size(73, 13);
            //
            //LayoutControlItem4
            //
            this.LayoutControlItem4.Control = this.LookUpEdit_counselor;
            this.LayoutControlItem4.CustomizationFormText = "Presenter";
            this.LayoutControlItem4.Location = new System.Drawing.Point(0, 65);
            this.LayoutControlItem4.Name = "LayoutControlItem4";
            this.LayoutControlItem4.Size = new System.Drawing.Size(345, 24);
            this.LayoutControlItem4.Text = "Presenter";
            this.LayoutControlItem4.TextSize = new System.Drawing.Size(73, 13);
            //
            //LayoutControlItem10
            //
            this.LayoutControlItem10.Control = this.DateEdit_starting_time;
            this.LayoutControlItem10.CustomizationFormText = "Starting Time";
            this.LayoutControlItem10.Location = new System.Drawing.Point(0, 89);
            this.LayoutControlItem10.Name = "LayoutControlItem10";
            this.LayoutControlItem10.Size = new System.Drawing.Size(345, 24);
            this.LayoutControlItem10.Text = "Starting Time";
            this.LayoutControlItem10.TextSize = new System.Drawing.Size(73, 13);
            //
            //LayoutControlItem7
            //
            this.LayoutControlItem7.Control = this.SpinEdit_seats;
            this.LayoutControlItem7.CustomizationFormText = "Seats Available";
            this.LayoutControlItem7.Location = new System.Drawing.Point(0, 113);
            this.LayoutControlItem7.Name = "LayoutControlItem7";
            this.LayoutControlItem7.Size = new System.Drawing.Size(172, 24);
            this.LayoutControlItem7.Text = "Seats Available";
            this.LayoutControlItem7.TextSize = new System.Drawing.Size(73, 13);
            //
            //EmptySpaceItem1
            //
            this.EmptySpaceItem1.AllowHotTrack = false;
            this.EmptySpaceItem1.CustomizationFormText = "EmptySpaceItem1";
            this.EmptySpaceItem1.Location = new System.Drawing.Point(172, 0);
            this.EmptySpaceItem1.Name = "EmptySpaceItem1";
            this.EmptySpaceItem1.Size = new System.Drawing.Size(173, 17);
            this.EmptySpaceItem1.Text = "EmptySpaceItem1";
            this.EmptySpaceItem1.TextSize = new System.Drawing.Size(0, 0);
            //
            //LayoutControlItem9
            //
            this.LayoutControlItem9.Control = this.CalcEdit_HousingFeeAmount;
            this.LayoutControlItem9.CustomizationFormText = "Fee Amount";
            this.LayoutControlItem9.Location = new System.Drawing.Point(172, 113);
            this.LayoutControlItem9.Name = "LayoutControlItem9";
            this.LayoutControlItem9.Size = new System.Drawing.Size(173, 24);
            this.LayoutControlItem9.Text = "Fee";
            this.LayoutControlItem9.TextSize = new System.Drawing.Size(73, 13);
            //
            //LayoutControlItem8
            //
            this.LayoutControlItem8.Control = this.LookUpEdit_HUD_Grant;
            this.LayoutControlItem8.CustomizationFormText = "Housing Grant";
            this.LayoutControlItem8.Location = new System.Drawing.Point(0, 137);
            this.LayoutControlItem8.Name = "LayoutControlItem8";
            this.LayoutControlItem8.Size = new System.Drawing.Size(345, 24);
            this.LayoutControlItem8.Text = "Housing Grant";
            this.LayoutControlItem8.TextSize = new System.Drawing.Size(73, 13);
            //
            //LayoutControlGroup2
            //
            this.LayoutControlGroup2.CustomizationFormText = "Guest Speaker";
            this.LayoutControlGroup2.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
				this.LayoutControlItem5,
				this.LayoutControlItem6,
				this.LayoutControlItem11
			});
            this.LayoutControlGroup2.Location = new System.Drawing.Point(0, 161);
            this.LayoutControlGroup2.Name = "LayoutControlGroup2";
            this.LayoutControlGroup2.Size = new System.Drawing.Size(345, 120);
            this.LayoutControlGroup2.Text = "Guest Speaker";
            //
            //LayoutControlItem5
            //
            this.LayoutControlItem5.Control = this.TextEdit_GuestName;
            this.LayoutControlItem5.CustomizationFormText = "Guest Speaker Name";
            this.LayoutControlItem5.Location = new System.Drawing.Point(0, 0);
            this.LayoutControlItem5.Name = "LayoutControlItem5";
            this.LayoutControlItem5.Size = new System.Drawing.Size(321, 24);
            this.LayoutControlItem5.Text = "Name";
            this.LayoutControlItem5.TextSize = new System.Drawing.Size(73, 13);
            //
            //LayoutControlItem6
            //
            this.LayoutControlItem6.Control = this.TextEdit_GuestEmail;
            this.LayoutControlItem6.CustomizationFormText = "Guest Speaker E-Mail";
            this.LayoutControlItem6.Location = new System.Drawing.Point(0, 24);
            this.LayoutControlItem6.Name = "LayoutControlItem6";
            this.LayoutControlItem6.Size = new System.Drawing.Size(321, 24);
            this.LayoutControlItem6.Text = "E-Mail";
            this.LayoutControlItem6.TextSize = new System.Drawing.Size(73, 13);
            //
            //LayoutControlItem11
            //
            this.LayoutControlItem11.Control = this.TextEdit_GuestPhone;
            this.LayoutControlItem11.CustomizationFormText = "Guest Speaker Telephone number";
            this.LayoutControlItem11.Location = new System.Drawing.Point(0, 48);
            this.LayoutControlItem11.Name = "LayoutControlItem11";
            this.LayoutControlItem11.Size = new System.Drawing.Size(321, 28);
            this.LayoutControlItem11.Text = "Telephone";
            this.LayoutControlItem11.TextSize = new System.Drawing.Size(73, 13);
            //
            this.LookUpEdit_workshop_type.Properties.DisplayMember = "description";
            this.LookUpEdit_workshop_type.Properties.ValueMember = "Id";
            this.LookUpEdit_counselor.Properties.DisplayMember = "Name";
            this.LookUpEdit_counselor.Properties.ValueMember = "Id";
            this.LookUpEdit_workshop_location.Properties.DisplayMember = "name";
            this.LookUpEdit_workshop_location.Properties.ValueMember = "Id";
            //
            //EditWorkshopForm
            //
            this.AutoScaleDimensions = new System.Drawing.SizeF(6f, 13f);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(468, 300);
            this.Controls.Add(this.LayoutControl1);
            this.Name = "EditWorkshopForm";
            this.Text = "Workshop Record";
            this.Controls.SetChildIndex(this.simpleButton_OK, 0);
            this.Controls.SetChildIndex(this.simpleButton_Cancel, 0);
            this.Controls.SetChildIndex(this.LayoutControl1, 0);
            ((System.ComponentModel.ISupportInitialize)this.DxErrorProvider1).EndInit();
            ((System.ComponentModel.ISupportInitialize)this.LayoutControl1).EndInit();
            this.LayoutControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)this.TextEdit_GuestPhone.Properties).EndInit();
            ((System.ComponentModel.ISupportInitialize)this.TextEdit_GuestEmail.Properties).EndInit();
            ((System.ComponentModel.ISupportInitialize)this.TextEdit_GuestName.Properties).EndInit();
            ((System.ComponentModel.ISupportInitialize)this.DateEdit_starting_time.Properties.VistaTimeProperties).EndInit();
            ((System.ComponentModel.ISupportInitialize)this.DateEdit_starting_time.Properties).EndInit();
            ((System.ComponentModel.ISupportInitialize)this.CalcEdit_HousingFeeAmount.Properties).EndInit();
            ((System.ComponentModel.ISupportInitialize)this.LookUpEdit_workshop_type.Properties).EndInit();
            ((System.ComponentModel.ISupportInitialize)this.SpinEdit_seats.Properties).EndInit();
            ((System.ComponentModel.ISupportInitialize)this.LookUpEdit_workshop_location.Properties).EndInit();
            ((System.ComponentModel.ISupportInitialize)this.LookUpEdit_HUD_Grant.Properties).EndInit();
            ((System.ComponentModel.ISupportInitialize)this.LookUpEdit_counselor.Properties).EndInit();
            ((System.ComponentModel.ISupportInitialize)this.LayoutControlGroup1).EndInit();
            ((System.ComponentModel.ISupportInitialize)this.LayoutControlItem1).EndInit();
            ((System.ComponentModel.ISupportInitialize)this.LayoutControlItem2).EndInit();
            ((System.ComponentModel.ISupportInitialize)this.LayoutControlItem3).EndInit();
            ((System.ComponentModel.ISupportInitialize)this.LayoutControlItem4).EndInit();
            ((System.ComponentModel.ISupportInitialize)this.LayoutControlItem10).EndInit();
            ((System.ComponentModel.ISupportInitialize)this.LayoutControlItem7).EndInit();
            ((System.ComponentModel.ISupportInitialize)this.EmptySpaceItem1).EndInit();
            ((System.ComponentModel.ISupportInitialize)this.LayoutControlItem9).EndInit();
            ((System.ComponentModel.ISupportInitialize)this.LayoutControlItem8).EndInit();
            ((System.ComponentModel.ISupportInitialize)this.LayoutControlGroup2).EndInit();
            ((System.ComponentModel.ISupportInitialize)this.LayoutControlItem5).EndInit();
            ((System.ComponentModel.ISupportInitialize)this.LayoutControlItem6).EndInit();
            ((System.ComponentModel.ISupportInitialize)this.LayoutControlItem11).EndInit();
            this.ResumeLayout(false);

        }
        private DevExpress.XtraEditors.LabelControl LabelControl_ID;
        private DevExpress.XtraEditors.LookUpEdit LookUpEdit_workshop_type;
        private DevExpress.XtraEditors.LookUpEdit LookUpEdit_workshop_location;
        private DevExpress.XtraEditors.LookUpEdit LookUpEdit_counselor;
        private DevExpress.XtraEditors.SpinEdit SpinEdit_seats;
        private DevExpress.XtraLayout.LayoutControl LayoutControl1;
        private DevExpress.XtraLayout.LayoutControlGroup LayoutControlGroup1;
        private DevExpress.XtraEditors.CalcEdit CalcEdit_HousingFeeAmount;
        private DevExpress.XtraEditors.LookUpEdit LookUpEdit_HUD_Grant;
        private DevExpress.XtraEditors.DateEdit DateEdit_starting_time;
        private DevExpress.XtraLayout.LayoutControlItem LayoutControlItem9;
        private DevExpress.XtraLayout.LayoutControlItem LayoutControlItem8;
        private DevExpress.XtraLayout.LayoutControlItem LayoutControlItem1;
        private DevExpress.XtraLayout.LayoutControlItem LayoutControlItem2;
        private DevExpress.XtraLayout.LayoutControlItem LayoutControlItem3;
        private DevExpress.XtraLayout.LayoutControlItem LayoutControlItem4;
        private DevExpress.XtraLayout.LayoutControlItem LayoutControlItem10;
        private DevExpress.XtraLayout.LayoutControlItem LayoutControlItem7;
        private DevExpress.XtraLayout.EmptySpaceItem EmptySpaceItem1;
        private DevExpress.XtraEditors.TextEdit TextEdit_GuestPhone;
        private DevExpress.XtraEditors.TextEdit TextEdit_GuestEmail;
        private DevExpress.XtraEditors.TextEdit TextEdit_GuestName;
        private DevExpress.XtraLayout.LayoutControlGroup LayoutControlGroup2;
        private DevExpress.XtraLayout.LayoutControlItem LayoutControlItem5;
        private DevExpress.XtraLayout.LayoutControlItem LayoutControlItem6;
        private DevExpress.XtraLayout.LayoutControlItem LayoutControlItem11;
    }
}

