#region "Copyright 2000-2012 DebtPlus, L.L.C."
//{*******************************************************************}
//{                                                                   }
//{       DebtPlus Debt Management System                             }
//{                                                                   }
//{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
//{       ALL RIGHTS RESERVED                                         }
//{                                                                   }
//{   The entire contents of this file is protected by U.S. and       }
//{   International Copyright Laws. Unauthorized reproduction,        }
//{   reverse-engineering, and distribution of all or any portion of  }
//{   the code contained in this file is strictly prohibited and may  }
//{   result in severe civil and criminal penalties and will be       }
//{   prosecuted to the maximum extent possible under the law.        }
//{                                                                   }
//{   RESTRICTIONS                                                    }
//{                                                                   }
//{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
//{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
//{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
//{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
//{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
//{                                                                   }
//{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
//{   ADDITIONAL RESTRICTIONS.                                        }
//{                                                                   }
//{*******************************************************************}

#endregion "Copyright 2000-2012 DebtPlus, L.L.C."

using System;
using DebtPlus.Data.Forms;
using DebtPlus.LINQ;

namespace DebtPlus.UI.TableAdministration.CS.Workshops.workshop_types
{
    internal partial class EditWorkshopTypesForm : DebtPlusForm
    {
        private BusinessContext bc = null;
        private workshop_type typeRecord;

        /// <summary>
        /// Initialize the new form context
        /// </summary>
        internal EditWorkshopTypesForm()
            : base()
        {
            InitializeComponent();
        }

        /// <summary>
        /// Initialize the new form context
        /// </summary>
        internal EditWorkshopTypesForm(BusinessContext bc, workshop_type typeRecord)
            : this()
        {
            this.bc = bc;
            this.typeRecord = typeRecord;

            RegisterHandlers();
        }

        /// <summary>
        /// Register the event handlers
        /// </summary>
        private void RegisterHandlers()
        {
            simpleButton_OK.Click += simpleButton_OK_Click;
            Load += EditForm_Load;
            CalcEdit_HousingFeeAmount.EditValueChanging += CalcEdit_HousingFeeAmount_Validating;
            TextEdit_description.EditValueChanged += Form_Changed;
            ContentTypes1.ListChanged += Form_Changed;
        }

        /// <summary>
        /// Remove the event handler registrations
        /// </summary>
        private void UnRegiserHandlers()
        {
            simpleButton_OK.Click -= simpleButton_OK_Click;
            Load -= EditForm_Load;
            CalcEdit_HousingFeeAmount.EditValueChanging -= CalcEdit_HousingFeeAmount_Validating;
            TextEdit_description.EditValueChanged -= Form_Changed;
            ContentTypes1.ListChanged -= Form_Changed;
        }

        private bool HasErrors()
        {
            // There needs to be something checked.
            if (ContentTypes1.GetCount() < 1)
            {
                return true;
            }

            // There needs to be a description
            if (string.IsNullOrWhiteSpace(DebtPlus.Utils.Nulls.v_String(TextEdit_description.EditValue)))
            {
                return true;
            }

            return false;
        }

        private void EditForm_Load(object sender, EventArgs e)
        {
            UnRegiserHandlers();

            // Load the list of grant types
            LookUpEdit_HUD_Grant.Properties.DataSource = DebtPlus.LINQ.Cache.Housing_GrantType.getList();

            try
            {
                // Set the ID for the record
                LabelControl_ID.Text = typeRecord.Id < 1 ? string.Empty : typeRecord.Id.ToString();

                // Load the list of content types
                ContentTypes1.ReadForm(bc, typeRecord);

                // Update the fields with the edit information
                textedit_duration.EditValue = typeRecord.duration;
                TextEdit_description.EditValue = typeRecord.description;
                LookUpEdit_HUD_Grant.EditValue = typeRecord.HUD_Grant;
                CalcEdit_HousingFeeAmount.EditValue = typeRecord.HousingFeeAmount;
                CheckEdit_ActiveFlag.Checked = typeRecord.ActiveFlag;

                simpleButton_OK.Enabled = !HasErrors();
            }
            finally
            {
                RegisterHandlers();
            }
        }

        private void Form_Changed(object sender, EventArgs e)
        {
            simpleButton_OK.Enabled = !HasErrors();
        }

        /// <summary>
        /// Process the CLICK event on the OK button
        /// </summary>
        private void simpleButton_OK_Click(object sender, EventArgs e)
        {
            // Save the information about the workshop type
            typeRecord.duration = DebtPlus.Utils.Nulls.v_Int32(textedit_duration.EditValue).GetValueOrDefault(0);
            typeRecord.description = DebtPlus.Utils.Nulls.v_String(TextEdit_description.EditValue) ?? string.Empty;
            typeRecord.HUD_Grant = DebtPlus.Utils.Nulls.v_Int32(LookUpEdit_HUD_Grant.EditValue);
            typeRecord.HousingFeeAmount = DebtPlus.Utils.Nulls.v_Decimal(CalcEdit_HousingFeeAmount.EditValue).GetValueOrDefault(0M);
            typeRecord.ActiveFlag = CheckEdit_ActiveFlag.Checked;

            // Save the content types as well
            ContentTypes1.SaveForm(bc, typeRecord);
        }

        /// <summary>
        /// Handle the change in the numeric fields.
        /// </summary>
        private void CalcEdit_HousingFeeAmount_Validating(object sender, System.ComponentModel.CancelEventArgs e)
        {
            // The value must be defined and having a value that is not negative.
            var value = DebtPlus.Utils.Nulls.v_Decimal(CalcEdit_HousingFeeAmount.EditValue);
            if (value.HasValue && value >= 0M)
            {
                return;
            }

            e.Cancel = true;
        }
    }
}