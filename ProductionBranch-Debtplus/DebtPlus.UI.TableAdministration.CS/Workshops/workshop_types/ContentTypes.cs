#region "Copyright 2000-2012 DebtPlus, L.L.C."
//{*******************************************************************}
//{                                                                   }
//{       DebtPlus Debt Management System                             }
//{                                                                   }
//{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
//{       ALL RIGHTS RESERVED                                         }
//{                                                                   }
//{   The entire contents of this file is protected by U.S. and       }
//{   International Copyright Laws. Unauthorized reproduction,        }
//{   reverse-engineering, and distribution of all or any portion of  }
//{   the code contained in this file is strictly prohibited and may  }
//{   result in severe civil and criminal penalties and will be       }
//{   prosecuted to the maximum extent possible under the law.        }
//{                                                                   }
//{   RESTRICTIONS                                                    }
//{                                                                   }
//{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
//{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
//{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
//{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
//{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
//{                                                                   }
//{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
//{   ADDITIONAL RESTRICTIONS.                                        }
//{                                                                   }
//{*******************************************************************}

#endregion "Copyright 2000-2012 DebtPlus, L.L.C."

using System;
using System.Data.SqlClient;
using System.Linq;
using System.Windows.Forms;
using DebtPlus.LINQ;

namespace DebtPlus.UI.TableAdministration.CS.Workshops.workshop_types
{
    internal partial class ContentTypes : DevExpress.XtraEditors.XtraUserControl
    {
        /// <summary>
        /// Event raised when the list of items check status is changed
        /// </summary>
        public event EventHandler ListChanged;

        /// <summary>
        /// Raise the ListChanged event
        /// </summary>
        protected void RaiseListChanged()
        {
            var evt = ListChanged;
            if (evt != null)
            {
                evt(this, EventArgs.Empty);
            }
        }

        /// <summary>
        /// Initialize the new type
        /// </summary>
        internal ContentTypes()
            : base()
        {
            InitializeComponent();
        }

        /// <summary>
        /// Register the event handlers
        /// </summary>
        private void RegisterHandlers()
        {
            CheckedListBoxControl1.ItemCheck += CheckedListBoxControl1_ItemCheck;
        }

        /// <summary>
        /// Remove the event handler registrations
        /// </summary>
        private void UnRegiserHandlers()
        {
            CheckedListBoxControl1.ItemCheck -= CheckedListBoxControl1_ItemCheck;
        }

        /// <summary>
        /// Process a change in the checked status for the list
        /// </summary>
        private void CheckedListBoxControl1_ItemCheck(object sender, DevExpress.XtraEditors.Controls.ItemCheckEventArgs e)
        {
            RaiseListChanged();
        }

        /// <summary>
        /// Return the number of checked items in the list.
        /// </summary>
        internal Int32 GetCount()
        {
            int counter = 0;
            foreach (var item in CheckedListBoxControl1.Items.OfType<DebtPlus.Data.Controls.SortedCheckedListboxControlItem>().Where(s => s.CheckState == CheckState.Checked))
            {
                counter += 1;
            }
            return counter;
        }

        /// <summary>
        /// Read the list of types and set the controls appropriately
        /// </summary>
        internal void ReadForm(BusinessContext bc, workshop_type typeRecord)
        {
            UnRegiserHandlers();
            try
            {
                // Load the list into the control as a series of items
                CheckedListBoxControl1.Items.Clear();
                foreach (var contentType in DebtPlus.LINQ.Cache.workshop_content_type.getList())
                {
                    var ctl = new DebtPlus.Data.Controls.SortedCheckedListboxControlItem(contentType, contentType.name, CheckState.Unchecked);
                    CheckedListBoxControl1.Items.Add(ctl);
                }

                // Check the items in the collection
                foreach (workshop_content content in typeRecord.workshop_contents)
                {
                    // Find the item in the list and check it
                    var q = CheckedListBoxControl1.Items.OfType<DebtPlus.Data.Controls.SortedCheckedListboxControlItem>().Where(s => ((workshop_content_type)(s.Value)).Id == content.content_type).FirstOrDefault();
                    if (q != null)
                    {
                        q.CheckState = CheckState.Checked;
                    }
                }
            }
            catch (SqlException ex)
            {
                DebtPlus.UI.Common.ErrorHandling.HandleErrors(ex, "Error reading workshop content list");
            }
            finally
            {
                RegisterHandlers();
            }
        }

        /// <summary>
        /// Save the form contents to the record
        /// </summary>
        internal void SaveForm(BusinessContext bc, workshop_type typeRecord)
        {
            // process the items in the collection
            foreach (DebtPlus.Data.Controls.SortedCheckedListboxControlItem item in CheckedListBoxControl1.Items)
            {
                Int32 Id = ((workshop_content_type)(item.Value)).Id;
                var q = typeRecord.workshop_contents.Where(s => s.content_type == Id).FirstOrDefault();
                if (item.CheckState == CheckState.Checked)
                {
                    // Add the item if it is checked and not present.
                    if (q == null)
                    {
                        q = DebtPlus.LINQ.Factory.Manufacture_workshop_content();
                        q.content_type = Id;
                        q.workshop_type1 = typeRecord;
                    }
                }
                else
                {
                    // Remove the un-checked item if it is present. The delete will occur when the type is saved.
                    if (q != null)
                    {
                        q.workshop_type1 = null;
                    }
                }
            }
        }
    }
}