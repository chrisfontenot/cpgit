﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Diagnostics;
namespace DebtPlus.UI.TableAdministration.CS.Workshops.workshop_types
{
    partial class EditWorkshopTypesForm
    {
		//Form overrides dispose to clean up the component list.
		[System.Diagnostics.DebuggerNonUserCode]
		protected override void Dispose(bool disposing)
		{
			try 
            {
				if (disposing)
                {
                    if (components != null) components.Dispose();
				}
                components = null;
            }
    	    finally
            {
				base.Dispose(disposing);
			}
		}

		//Required by the Windows Form Designer
		private System.ComponentModel.IContainer components = null;

        //NOTE: The following procedure is required by the Windows Form Designer
        //It can be modified using the Windows Form Designer.  
        //Do not modify it using the code editor.
        [System.Diagnostics.DebuggerStepThrough]
        private void InitializeComponent()
        {
            this.LabelControl_ID = new DevExpress.XtraEditors.LabelControl();
            this.LayoutControl1 = new DevExpress.XtraLayout.LayoutControl();
            this.CheckEdit_ActiveFlag = new DevExpress.XtraEditors.CheckEdit();
            this.ContentTypes1 = new DebtPlus.UI.TableAdministration.CS.Workshops.workshop_types.ContentTypes();
            this.CalcEdit_HousingFeeAmount = new DevExpress.XtraEditors.CalcEdit();
            this.LookUpEdit_HUD_Grant = new DevExpress.XtraEditors.LookUpEdit();
            this.LabelControl2 = new DevExpress.XtraEditors.LabelControl();
            this.TextEdit_description = new DevExpress.XtraEditors.TextEdit();
            this.textedit_duration = new DevExpress.XtraEditors.TextEdit();
            this.LayoutControlItem6 = new DevExpress.XtraLayout.LayoutControlItem();
            this.LayoutControlItem5 = new DevExpress.XtraLayout.LayoutControlItem();
            this.LayoutControlGroup1 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.LayoutControlItem1 = new DevExpress.XtraLayout.LayoutControlItem();
            this.LayoutControlItem2 = new DevExpress.XtraLayout.LayoutControlItem();
            this.LayoutControlItem3 = new DevExpress.XtraLayout.LayoutControlItem();
            this.LayoutControlItem4 = new DevExpress.XtraLayout.LayoutControlItem();
            this.EmptySpaceItem1 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.EmptySpaceItem2 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.LayoutControlItem8 = new DevExpress.XtraLayout.LayoutControlItem();
            this.LayoutControlGroup2 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.LayoutControlItem7 = new DevExpress.XtraLayout.LayoutControlItem();
            this.simpleButton_OK = new DevExpress.XtraEditors.SimpleButton();
            this.simpleButton_Cancel = new DevExpress.XtraEditors.SimpleButton();
            ((System.ComponentModel.ISupportInitialize)(this.DxErrorProvider1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LayoutControl1)).BeginInit();
            this.LayoutControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.CheckEdit_ActiveFlag.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.CalcEdit_HousingFeeAmount.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LookUpEdit_HUD_Grant.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TextEdit_description.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textedit_duration.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LayoutControlItem6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LayoutControlItem5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LayoutControlGroup1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LayoutControlItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LayoutControlItem2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LayoutControlItem3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LayoutControlItem4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.EmptySpaceItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.EmptySpaceItem2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LayoutControlItem8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LayoutControlGroup2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LayoutControlItem7)).BeginInit();
            this.SuspendLayout();
            // 
            // LabelControl_ID
            // 
            this.LabelControl_ID.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.LabelControl_ID.Location = new System.Drawing.Point(68, 12);
            this.LabelControl_ID.Name = "LabelControl_ID";
            this.LabelControl_ID.Size = new System.Drawing.Size(113, 13);
            this.LabelControl_ID.StyleController = this.LayoutControl1;
            this.LabelControl_ID.TabIndex = 1;
            // 
            // LayoutControl1
            // 
            this.LayoutControl1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.LayoutControl1.Controls.Add(this.CheckEdit_ActiveFlag);
            this.LayoutControl1.Controls.Add(this.ContentTypes1);
            this.LayoutControl1.Controls.Add(this.CalcEdit_HousingFeeAmount);
            this.LayoutControl1.Controls.Add(this.LookUpEdit_HUD_Grant);
            this.LayoutControl1.Controls.Add(this.LabelControl_ID);
            this.LayoutControl1.Controls.Add(this.LabelControl2);
            this.LayoutControl1.Controls.Add(this.TextEdit_description);
            this.LayoutControl1.Controls.Add(this.textedit_duration);
            this.LayoutControl1.HiddenItems.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.LayoutControlItem6,
            this.LayoutControlItem5});
            this.LayoutControl1.Location = new System.Drawing.Point(3, 3);
            this.LayoutControl1.Name = "LayoutControl1";
            this.LayoutControl1.Root = this.LayoutControlGroup1;
            this.LayoutControl1.Size = new System.Drawing.Size(366, 253);
            this.LayoutControl1.TabIndex = 19;
            this.LayoutControl1.Text = "LayoutControl1";
            // 
            // CheckEdit_ActiveFlag
            // 
            this.CheckEdit_ActiveFlag.Location = new System.Drawing.Point(12, 222);
            this.CheckEdit_ActiveFlag.Name = "CheckEdit_ActiveFlag";
            this.CheckEdit_ActiveFlag.Properties.Caption = "Active";
            this.CheckEdit_ActiveFlag.Size = new System.Drawing.Size(342, 19);
            this.CheckEdit_ActiveFlag.StyleController = this.LayoutControl1;
            this.CheckEdit_ActiveFlag.TabIndex = 21;
            this.CheckEdit_ActiveFlag.ToolTip = "Is the item Active (may it be used for new workshop types)?";
            // 
            // ContentTypes1
            // 
            this.ContentTypes1.Location = new System.Drawing.Point(24, 108);
            this.ContentTypes1.Name = "ContentTypes1";
            this.ContentTypes1.Size = new System.Drawing.Size(318, 98);
            this.ContentTypes1.TabIndex = 20;
            this.ToolTipController1.SetToolTip(this.ContentTypes1, "Items that this workshop type discusses");
            // 
            // CalcEdit_HousingFeeAmount
            // 
            this.CalcEdit_HousingFeeAmount.Location = new System.Drawing.Point(80, 77);
            this.CalcEdit_HousingFeeAmount.Name = "CalcEdit_HousingFeeAmount";
            this.CalcEdit_HousingFeeAmount.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.False;
            this.CalcEdit_HousingFeeAmount.Properties.Appearance.Options.UseTextOptions = true;
            this.CalcEdit_HousingFeeAmount.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.CalcEdit_HousingFeeAmount.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.CalcEdit_HousingFeeAmount.Properties.DisplayFormat.FormatString = "c";
            this.CalcEdit_HousingFeeAmount.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.CalcEdit_HousingFeeAmount.Properties.EditFormat.FormatString = "c";
            this.CalcEdit_HousingFeeAmount.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.CalcEdit_HousingFeeAmount.Properties.Mask.BeepOnError = true;
            this.CalcEdit_HousingFeeAmount.Properties.Mask.EditMask = "c";
            this.CalcEdit_HousingFeeAmount.Properties.Precision = 2;
            this.CalcEdit_HousingFeeAmount.Size = new System.Drawing.Size(103, 20);
            this.CalcEdit_HousingFeeAmount.StyleController = this.LayoutControl1;
            this.CalcEdit_HousingFeeAmount.TabIndex = 20;
            this.CalcEdit_HousingFeeAmount.ToolTip = "If you charge a record for this type of workshop, what is the record per client?";
            // 
            // LookUpEdit_HUD_Grant
            // 
            this.LookUpEdit_HUD_Grant.Location = new System.Drawing.Point(80, 101);
            this.LookUpEdit_HUD_Grant.Name = "LookUpEdit_HUD_Grant";
            this.LookUpEdit_HUD_Grant.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.True;
            this.LookUpEdit_HUD_Grant.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LookUpEdit_HUD_Grant.Properties.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("Id", "ID", 20, DevExpress.Utils.FormatType.Numeric, "f0", false, DevExpress.Utils.HorzAlignment.Default),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("description", "Description", 20, DevExpress.Utils.FormatType.None, "", true, DevExpress.Utils.HorzAlignment.Default, DevExpress.Data.ColumnSortOrder.Ascending)});
            this.LookUpEdit_HUD_Grant.Properties.DisplayMember = "description";
            this.LookUpEdit_HUD_Grant.Properties.NullText = "";
            this.LookUpEdit_HUD_Grant.Properties.ShowFooter = false;
            this.LookUpEdit_HUD_Grant.Properties.ShowHeader = false;
            this.LookUpEdit_HUD_Grant.Properties.SortColumnIndex = 1;
            this.LookUpEdit_HUD_Grant.Properties.ValueMember = "Id";
            this.LookUpEdit_HUD_Grant.Size = new System.Drawing.Size(274, 20);
            this.LookUpEdit_HUD_Grant.StyleController = this.LayoutControl1;
            this.LookUpEdit_HUD_Grant.TabIndex = 19;
            // 
            // LabelControl2
            // 
            this.LabelControl2.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.LabelControl2.Appearance.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.LabelControl2.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.LabelControl2.Location = new System.Drawing.Point(205, 56);
            this.LabelControl2.Name = "LabelControl2";
            this.LabelControl2.Size = new System.Drawing.Size(37, 13);
            this.LabelControl2.StyleController = this.LayoutControl1;
            this.LabelControl2.TabIndex = 18;
            this.LabelControl2.Text = "minutes";
            // 
            // TextEdit_description
            // 
            this.TextEdit_description.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.TextEdit_description.Location = new System.Drawing.Point(68, 29);
            this.TextEdit_description.Name = "TextEdit_description";
            this.TextEdit_description.Properties.MaxLength = 50;
            this.TextEdit_description.Size = new System.Drawing.Size(286, 20);
            this.TextEdit_description.StyleController = this.LayoutControl1;
            this.TextEdit_description.TabIndex = 3;
            this.TextEdit_description.ToolTip = "Textual description of the workshop";
            // 
            // textedit_duration
            // 
            this.textedit_duration.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.textedit_duration.Location = new System.Drawing.Point(68, 53);
            this.textedit_duration.Name = "textedit_duration";
            this.textedit_duration.Properties.Appearance.Options.UseTextOptions = true;
            this.textedit_duration.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.textedit_duration.Properties.DisplayFormat.FormatString = "n0";
            this.textedit_duration.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.textedit_duration.Properties.Mask.BeepOnError = true;
            this.textedit_duration.Properties.Mask.EditMask = "[1-9]\\d*";
            this.textedit_duration.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.RegEx;
            this.textedit_duration.Size = new System.Drawing.Size(115, 20);
            this.textedit_duration.StyleController = this.LayoutControl1;
            this.textedit_duration.TabIndex = 13;
            this.textedit_duration.ToolTip = "Length of the workshop in minutes";
            // 
            // LayoutControlItem6
            // 
            this.LayoutControlItem6.Control = this.CalcEdit_HousingFeeAmount;
            this.LayoutControlItem6.CustomizationFormText = "Default Fee Amount";
            this.LayoutControlItem6.Location = new System.Drawing.Point(0, 89);
            this.LayoutControlItem6.Name = "LayoutControlItem6";
            this.LayoutControlItem6.Size = new System.Drawing.Size(173, 24);
            this.LayoutControlItem6.Text = "Default Fee";
            this.LayoutControlItem6.TextSize = new System.Drawing.Size(65, 13);
            this.LayoutControlItem6.TextToControlDistance = 5;
            // 
            // LayoutControlItem5
            // 
            this.LayoutControlItem5.Control = this.LookUpEdit_HUD_Grant;
            this.LayoutControlItem5.CustomizationFormText = "Default Grant";
            this.LayoutControlItem5.Location = new System.Drawing.Point(0, 65);
            this.LayoutControlItem5.Name = "LayoutControlItem5";
            this.LayoutControlItem5.Size = new System.Drawing.Size(346, 24);
            this.LayoutControlItem5.Text = "Default Grant";
            this.LayoutControlItem5.TextSize = new System.Drawing.Size(65, 13);
            this.LayoutControlItem5.TextToControlDistance = 5;
            // 
            // LayoutControlGroup1
            // 
            this.LayoutControlGroup1.CustomizationFormText = "LayoutControlGroup1";
            this.LayoutControlGroup1.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.True;
            this.LayoutControlGroup1.GroupBordersVisible = false;
            this.LayoutControlGroup1.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.LayoutControlItem1,
            this.LayoutControlItem2,
            this.LayoutControlItem3,
            this.LayoutControlItem4,
            this.EmptySpaceItem1,
            this.EmptySpaceItem2,
            this.LayoutControlItem8,
            this.LayoutControlGroup2});
            this.LayoutControlGroup1.Location = new System.Drawing.Point(0, 0);
            this.LayoutControlGroup1.Name = "LayoutControlGroup1";
            this.LayoutControlGroup1.Size = new System.Drawing.Size(366, 253);
            this.LayoutControlGroup1.Text = "LayoutControlGroup1";
            this.LayoutControlGroup1.TextVisible = false;
            // 
            // LayoutControlItem1
            // 
            this.LayoutControlItem1.Control = this.LabelControl_ID;
            this.LayoutControlItem1.CustomizationFormText = "ID";
            this.LayoutControlItem1.Location = new System.Drawing.Point(0, 0);
            this.LayoutControlItem1.Name = "LayoutControlItem1";
            this.LayoutControlItem1.Size = new System.Drawing.Size(173, 17);
            this.LayoutControlItem1.Text = "ID";
            this.LayoutControlItem1.TextSize = new System.Drawing.Size(53, 13);
            // 
            // LayoutControlItem2
            // 
            this.LayoutControlItem2.Control = this.TextEdit_description;
            this.LayoutControlItem2.CustomizationFormText = "Description";
            this.LayoutControlItem2.Location = new System.Drawing.Point(0, 17);
            this.LayoutControlItem2.Name = "LayoutControlItem2";
            this.LayoutControlItem2.Size = new System.Drawing.Size(346, 24);
            this.LayoutControlItem2.Text = "Description";
            this.LayoutControlItem2.TextSize = new System.Drawing.Size(53, 13);
            // 
            // LayoutControlItem3
            // 
            this.LayoutControlItem3.Control = this.textedit_duration;
            this.LayoutControlItem3.CustomizationFormText = "Duration";
            this.LayoutControlItem3.Location = new System.Drawing.Point(0, 41);
            this.LayoutControlItem3.Name = "LayoutControlItem3";
            this.LayoutControlItem3.Size = new System.Drawing.Size(175, 24);
            this.LayoutControlItem3.Text = "Duration";
            this.LayoutControlItem3.TextSize = new System.Drawing.Size(53, 13);
            // 
            // LayoutControlItem4
            // 
            this.LayoutControlItem4.Control = this.LabelControl2;
            this.LayoutControlItem4.ControlAlignment = System.Drawing.ContentAlignment.MiddleLeft;
            this.LayoutControlItem4.CustomizationFormText = "Minutes";
            this.LayoutControlItem4.Location = new System.Drawing.Point(175, 41);
            this.LayoutControlItem4.Name = "LayoutControlItem4";
            this.LayoutControlItem4.Padding = new DevExpress.XtraLayout.Utils.Padding(20, 2, 5, 2);
            this.LayoutControlItem4.Size = new System.Drawing.Size(59, 24);
            this.LayoutControlItem4.Text = "Minutes";
            this.LayoutControlItem4.TextSize = new System.Drawing.Size(0, 0);
            this.LayoutControlItem4.TextToControlDistance = 0;
            this.LayoutControlItem4.TextVisible = false;
            // 
            // EmptySpaceItem1
            // 
            this.EmptySpaceItem1.AllowHotTrack = false;
            this.EmptySpaceItem1.CustomizationFormText = "EmptySpaceItem1";
            this.EmptySpaceItem1.Location = new System.Drawing.Point(234, 41);
            this.EmptySpaceItem1.Name = "EmptySpaceItem1";
            this.EmptySpaceItem1.Size = new System.Drawing.Size(112, 24);
            this.EmptySpaceItem1.Text = "EmptySpaceItem1";
            this.EmptySpaceItem1.TextSize = new System.Drawing.Size(0, 0);
            // 
            // EmptySpaceItem2
            // 
            this.EmptySpaceItem2.AllowHotTrack = false;
            this.EmptySpaceItem2.CustomizationFormText = "EmptySpaceItem2";
            this.EmptySpaceItem2.Location = new System.Drawing.Point(173, 0);
            this.EmptySpaceItem2.Name = "EmptySpaceItem2";
            this.EmptySpaceItem2.Size = new System.Drawing.Size(173, 17);
            this.EmptySpaceItem2.Text = "EmptySpaceItem2";
            this.EmptySpaceItem2.TextSize = new System.Drawing.Size(0, 0);
            // 
            // LayoutControlItem8
            // 
            this.LayoutControlItem8.Control = this.CheckEdit_ActiveFlag;
            this.LayoutControlItem8.CustomizationFormText = "Active Type?";
            this.LayoutControlItem8.Location = new System.Drawing.Point(0, 210);
            this.LayoutControlItem8.Name = "LayoutControlItem8";
            this.LayoutControlItem8.Size = new System.Drawing.Size(346, 23);
            this.LayoutControlItem8.Text = "Active Type?";
            this.LayoutControlItem8.TextSize = new System.Drawing.Size(0, 0);
            this.LayoutControlItem8.TextToControlDistance = 0;
            this.LayoutControlItem8.TextVisible = false;
            // 
            // LayoutControlGroup2
            // 
            this.LayoutControlGroup2.CustomizationFormText = "Contents";
            this.LayoutControlGroup2.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.LayoutControlItem7});
            this.LayoutControlGroup2.Location = new System.Drawing.Point(0, 65);
            this.LayoutControlGroup2.Name = "LayoutControlGroup2";
            this.LayoutControlGroup2.Size = new System.Drawing.Size(346, 145);
            this.LayoutControlGroup2.Text = "Contents";
            // 
            // LayoutControlItem7
            // 
            this.LayoutControlItem7.Control = this.ContentTypes1;
            this.LayoutControlItem7.CustomizationFormText = "Contents";
            this.LayoutControlItem7.Location = new System.Drawing.Point(0, 0);
            this.LayoutControlItem7.Name = "LayoutControlItem7";
            this.LayoutControlItem7.Size = new System.Drawing.Size(322, 102);
            this.LayoutControlItem7.Text = "Contents";
            this.LayoutControlItem7.TextSize = new System.Drawing.Size(0, 0);
            this.LayoutControlItem7.TextToControlDistance = 0;
            this.LayoutControlItem7.TextVisible = false;
            // 
            // simpleButton_OK
            // 
            this.simpleButton_OK.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.simpleButton_OK.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.simpleButton_OK.Location = new System.Drawing.Point(372, 13);
            this.simpleButton_OK.Name = "simpleButton_OK";
            this.simpleButton_OK.Size = new System.Drawing.Size(75, 23);
            this.simpleButton_OK.TabIndex = 15;
            this.simpleButton_OK.Text = "&OK";
            // 
            // simpleButton_Cancel
            // 
            this.simpleButton_Cancel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.simpleButton_Cancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.simpleButton_Cancel.Location = new System.Drawing.Point(372, 56);
            this.simpleButton_Cancel.Name = "simpleButton_Cancel";
            this.simpleButton_Cancel.Size = new System.Drawing.Size(75, 23);
            this.simpleButton_Cancel.TabIndex = 16;
            this.simpleButton_Cancel.Text = "&Cancel";
            // 
            // EditWorkshopTypesForm
            // 
            this.AcceptButton = this.simpleButton_OK;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.CancelButton = this.simpleButton_Cancel;
            this.ClientSize = new System.Drawing.Size(468, 255);
            this.Controls.Add(this.LayoutControl1);
            this.Controls.Add(this.simpleButton_Cancel);
            this.Controls.Add(this.simpleButton_OK);
            this.Name = "EditWorkshopTypesForm";
            this.Text = "Workshop Type Record";
            ((System.ComponentModel.ISupportInitialize)(this.DxErrorProvider1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LayoutControl1)).EndInit();
            this.LayoutControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.CheckEdit_ActiveFlag.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.CalcEdit_HousingFeeAmount.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LookUpEdit_HUD_Grant.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TextEdit_description.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textedit_duration.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LayoutControlItem6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LayoutControlItem5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LayoutControlGroup1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LayoutControlItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LayoutControlItem2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LayoutControlItem3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LayoutControlItem4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.EmptySpaceItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.EmptySpaceItem2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LayoutControlItem8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LayoutControlGroup2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LayoutControlItem7)).EndInit();
            this.ResumeLayout(false);

        }
        private ContentTypes ContentTypes1;
        private DevExpress.XtraEditors.LabelControl LabelControl_ID;
        private DevExpress.XtraEditors.SimpleButton simpleButton_OK;
        private DevExpress.XtraEditors.SimpleButton simpleButton_Cancel;
        private DevExpress.XtraEditors.TextEdit TextEdit_description;
        private DevExpress.XtraEditors.TextEdit textedit_duration;
        private DevExpress.XtraEditors.LabelControl LabelControl2;
        private DevExpress.XtraLayout.LayoutControl LayoutControl1;
        private DevExpress.XtraLayout.LayoutControlGroup LayoutControlGroup1;
        private DevExpress.XtraEditors.LookUpEdit LookUpEdit_HUD_Grant;
        private DevExpress.XtraEditors.CalcEdit CalcEdit_HousingFeeAmount;
        private DevExpress.XtraEditors.CheckEdit CheckEdit_ActiveFlag;
        private DevExpress.XtraLayout.LayoutControlItem LayoutControlItem6;
        private DevExpress.XtraLayout.LayoutControlItem LayoutControlItem5;
        private DevExpress.XtraLayout.LayoutControlItem LayoutControlItem1;
        private DevExpress.XtraLayout.LayoutControlItem LayoutControlItem2;
        private DevExpress.XtraLayout.LayoutControlItem LayoutControlItem3;
        private DevExpress.XtraLayout.LayoutControlItem LayoutControlItem4;
        private DevExpress.XtraLayout.EmptySpaceItem EmptySpaceItem1;
        private DevExpress.XtraLayout.EmptySpaceItem EmptySpaceItem2;
        private DevExpress.XtraLayout.LayoutControlItem LayoutControlItem8;
        private DevExpress.XtraLayout.LayoutControlGroup LayoutControlGroup2;
        private DevExpress.XtraLayout.LayoutControlItem LayoutControlItem7;
    }
}

