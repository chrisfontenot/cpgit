
using Microsoft.VisualBasic;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Diagnostics;
namespace DebtPlus.UI.TableAdministration.CS.Workshops.workshop_types
{
    partial class ContentTypes
    {

        //UserControl overrides dispose to clean up the component list.
        [System.Diagnostics.DebuggerNonUserCode]
        protected override void Dispose(bool disposing)
        {
            if (disposing && components != null)
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        //Required by the Windows Form Designer
        private System.ComponentModel.IContainer components = null;

        //NOTE: The following procedure is required by the Windows Form Designer
        //It can be modified using the Windows Form Designer.  
        //Do not modify it using the code editor.
        [System.Diagnostics.DebuggerStepThrough]
        private void InitializeComponent()
        {
            this.CheckedListBoxControl1 = new DevExpress.XtraEditors.CheckedListBoxControl();
            ((System.ComponentModel.ISupportInitialize)this.CheckedListBoxControl1).BeginInit();
            this.SuspendLayout();
            //
            //CheckedListBoxControl1
            //
            this.CheckedListBoxControl1.CheckOnClick = true;
            this.CheckedListBoxControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.CheckedListBoxControl1.Location = new System.Drawing.Point(0, 0);
            this.CheckedListBoxControl1.Name = "CheckedListBoxControl1";
            this.CheckedListBoxControl1.SelectionMode = System.Windows.Forms.SelectionMode.One;
            this.CheckedListBoxControl1.Size = new System.Drawing.Size(150, 150);
            this.CheckedListBoxControl1.SortOrder = System.Windows.Forms.SortOrder.Ascending;
            this.CheckedListBoxControl1.TabIndex = 0;
            //
            //CounselorLanguages
            //
            this.AutoScaleDimensions = new System.Drawing.SizeF(6f, 13f);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.CheckedListBoxControl1);
            this.Name = "CounselorLanguages";
            ((System.ComponentModel.ISupportInitialize)this.CheckedListBoxControl1).EndInit();
            this.ResumeLayout(false);

        }

        private DevExpress.XtraEditors.CheckedListBoxControl CheckedListBoxControl1;
    }
}

