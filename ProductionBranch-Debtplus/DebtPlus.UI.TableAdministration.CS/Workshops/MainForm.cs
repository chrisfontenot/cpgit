#region "Copyright 2000-2012 DebtPlus, L.L.C."
//{*******************************************************************}
//{                                                                   }
//{       DebtPlus Debt Management System                             }
//{                                                                   }
//{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
//{       ALL RIGHTS RESERVED                                         }
//{                                                                   }
//{   The entire contents of this file is protected by U.S. and       }
//{   International Copyright Laws. Unauthorized reproduction,        }
//{   reverse-engineering, and distribution of all or any portion of  }
//{   the code contained in this file is strictly prohibited and may  }
//{   result in severe civil and criminal penalties and will be       }
//{   prosecuted to the maximum extent possible under the law.        }
//{                                                                   }
//{   RESTRICTIONS                                                    }
//{                                                                   }
//{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
//{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
//{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
//{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
//{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
//{                                                                   }
//{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
//{   ADDITIONAL RESTRICTIONS.                                        }
//{                                                                   }
//{*******************************************************************}

#endregion "Copyright 2000-2012 DebtPlus, L.L.C."

using System;
using System.Linq;
using DebtPlus.LINQ;
using DebtPlus.UI.TableAdministration.CS.Workshops.workshops;

namespace DebtPlus.UI.TableAdministration.CS.Workshops
{
    public partial class MainForm : Templates.MainForm
    {
        private System.Collections.Generic.List<workshop> colWorkshops = null;
        private BusinessContext bc = new BusinessContext();

        public MainForm()
            : base()
        {
            InitializeComponent();
            RegisterHandlers();
        }

        /// <summary>
        /// Register the event handlers
        /// </summary>
        private void RegisterHandlers()
        {
            Load += MainForm_Load;
            gridView1.CustomColumnDisplayText += gridView1_CustomColumnDisplayText;
        }

        /// <summary>
        /// Remove the event handler registrations
        /// </summary>
        private void UnRegiserHandlers()
        {
            Load -= MainForm_Load;
            gridView1.CustomColumnDisplayText -= gridView1_CustomColumnDisplayText;
        }

        private void gridView1_CustomColumnDisplayText(object sender, DevExpress.XtraGrid.Views.Base.CustomColumnDisplayTextEventArgs e)
        {
            // Find the current record being displayed
            var workshopRecord = gridView1.GetRow(e.RowHandle) as workshop;
            if (workshopRecord == null)
            {
                return;
            }

            // Process the counselor name
            if (e.Column == gridColumn_counselor)
            {
                var key = workshopRecord.CounselorID;
                if (key.HasValue)
                {
                    var counselorRecord = DebtPlus.LINQ.Cache.counselor.getList().Find(s => s.Id == key.Value);
                    if (counselorRecord != null && counselorRecord.Name != null)
                    {
                        e.DisplayText = counselorRecord.Name.ToString();
                        return;
                    }
                }
                e.DisplayText = string.Empty;
                return;
            }

            // Look for the type of the workshop
            if (e.Column == gridColumn_workshop)
            {
                var key = workshopRecord.workshop_type;
                var q = bc.workshop_types.Where(s => s.Id == key).FirstOrDefault();
                if (q != null)
                {
                    e.DisplayText = q.description;
                    return;
                }
                e.DisplayText = string.Empty;
                return;
            }

            // Look for the location
            if (e.Column == gridColumn_location)
            {
                var key = workshopRecord.workshop_location;
                var q = bc.workshop_locations.Where(s => s.Id == key).FirstOrDefault();
                if (q != null)
                {
                    e.DisplayText = q.name;
                    return;
                }
                e.DisplayText = string.Empty;
                return;
            }
        }

        /// <summary>
        /// Process the LOAD sequence for the form
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void MainForm_Load(object sender, EventArgs e)
        {
            UnRegiserHandlers();
            try
            {
                // Starting date for the workshop selection is 10 days ago.
                DateTime startingDate = DateTime.Now.AddDays(-10).Date;

                colWorkshops = (from w in bc.workshops where w.start_time >= startingDate select w).ToList();
                gridControl1.DataSource = colWorkshops;
            }
            finally
            {
                RegisterHandlers();
            }
        }

        /// <summary>
        /// Handle the CREATE event
        /// </summary>
        protected override void CreateRecord()
        {
            var record = DebtPlus.LINQ.Factory.Manufacture_workshop();

            // Edit the record
            using (var frm = new EditWorkshopForm(bc, record))
            {
                if (frm.ShowDialog() != System.Windows.Forms.DialogResult.OK)
                {
                    return;
                }
            }

            // Insert the new record into the collection
            try
            {
                bc.workshops.InsertOnSubmit(record);
                bc.SubmitChanges();

                colWorkshops.Add(record);
                gridView1.RefreshData();
            }
            catch (System.Data.SqlClient.SqlException ex)
            {
                DebtPlus.UI.Common.ErrorHandling.HandleErrors(ex);
            }
        }

        /// <summary>
        /// Handle the EDIT event
        /// </summary>
        /// <param name="obj"></param>
        protected override void UpdateRecord(object obj)
        {
            // Find the record to be modified.
            workshop record = obj as workshop;
            if (obj == null)
            {
                return;
            }

            // Edit the record
            using (var frm = new EditWorkshopForm(bc, record))
            {
                if (frm.ShowDialog() != System.Windows.Forms.DialogResult.OK)
                {
                    return;
                }
            }

            try
            {
                bc.SubmitChanges();
                gridView1.RefreshData();
            }
            catch (System.Data.SqlClient.SqlException ex)
            {
                DebtPlus.UI.Common.ErrorHandling.HandleErrors(ex);
            }
        }

        /// <summary>
        /// Handle the DELETE event
        /// </summary>
        /// <param name="obj"></param>
        protected override void DeleteRecord(object obj)
        {
            // Find the record to be modified.
            workshop record = obj as workshop;
            if (obj == null)
            {
                return;
            }

            if (DebtPlus.Data.Prompts.RequestConfirmation_Delete() != System.Windows.Forms.DialogResult.Yes)
            {
                return;
            }

            bc.workshops.DeleteOnSubmit(record);
            try
            {
                bc.SubmitChanges();

                colWorkshops.Remove(record);
                gridView1.RefreshData();
            }
            catch (System.Data.SqlClient.SqlException ex)
            {
                DebtPlus.UI.Common.ErrorHandling.HandleErrors(ex);
            }
        }
    }
}