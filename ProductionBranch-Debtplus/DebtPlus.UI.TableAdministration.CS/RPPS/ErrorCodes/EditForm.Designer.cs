using System;
using System.Windows.Forms;
using System.Drawing;
using DebtPlus.LINQ;
using System.Linq;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Diagnostics;
using DebtPlus.UI.Common.Templates;

namespace DebtPlus.UI.TableAdministration.CS.RPPS.ErrorCodes
{
	partial class EditForm
	{
		//Form overrides dispose to clean up the component list.
		[System.Diagnostics.DebuggerNonUserCode]
		protected override void Dispose(bool disposing)
		{
			try 
            {
				if (disposing)
                {
                    if (components != null) components.Dispose();
				}
                components = null;
            }
    	    finally
            {
				base.Dispose(disposing);
			}
		}

		//Required by the Windows Form Designer
		private System.ComponentModel.IContainer components = null;

		//NOTE: The following procedure is required by the Windows Form Designer
		//It can be modified using the Windows Form Designer.  
		//Do not modify it using the code editor.
		[System.Diagnostics.DebuggerStepThrough]
		private void InitializeComponent()
		{
			this.LabelControl1 = new DevExpress.XtraEditors.LabelControl();
			this.LabelControl2 = new DevExpress.XtraEditors.LabelControl();
			this.CheckEdit_resend_funds = new DevExpress.XtraEditors.CheckEdit();
			this.TextEdit_ach_error_code = new DevExpress.XtraEditors.TextEdit();
			this.TextEdit_description = new DevExpress.XtraEditors.TextEdit();
			((System.ComponentModel.ISupportInitialize)this.DxErrorProvider1).BeginInit();
			((System.ComponentModel.ISupportInitialize)this.CheckEdit_resend_funds.Properties).BeginInit();
			((System.ComponentModel.ISupportInitialize)this.TextEdit_ach_error_code.Properties).BeginInit();
			((System.ComponentModel.ISupportInitialize)this.TextEdit_description.Properties).BeginInit();
			this.SuspendLayout();
			//
			//simpleButton_OK
			//
			this.simpleButton_OK.Enabled = true;
			this.simpleButton_OK.Location = new System.Drawing.Point(313, 34);
			//
			//simpleButton_Cancel
			//
			this.simpleButton_Cancel.Location = new System.Drawing.Point(313, 77);
			//
			//LabelControl1
			//
			this.LabelControl1.Location = new System.Drawing.Point(13, 16);
			this.LabelControl1.Name = "LabelControl1";
			this.LabelControl1.Size = new System.Drawing.Size(52, 13);
			this.LabelControl1.TabIndex = 20;
			this.LabelControl1.Text = "Error Code";
			//
			//LabelControl2
			//
			this.LabelControl2.Location = new System.Drawing.Point(13, 43);
			this.LabelControl2.Name = "LabelControl2";
			this.LabelControl2.Size = new System.Drawing.Size(53, 13);
			this.LabelControl2.TabIndex = 21;
			this.LabelControl2.Text = "Description";
			//
			//CheckEdit_resend_funds
			//
			this.CheckEdit_resend_funds.Location = new System.Drawing.Point(10, 75);
			this.CheckEdit_resend_funds.Name = "CheckEdit_resend_funds";
			this.CheckEdit_resend_funds.Properties.Caption = "Send the funds again if this error occurs";
			this.CheckEdit_resend_funds.Size = new System.Drawing.Size(270, 19);
			this.CheckEdit_resend_funds.TabIndex = 24;
			//
			//TextEdit_ach_error_code
			//
			this.TextEdit_ach_error_code.Location = new System.Drawing.Point(88, 13);
			this.TextEdit_ach_error_code.Name = "TextEdit_ach_error_code";
			this.TextEdit_ach_error_code.Properties.Mask.BeepOnError = true;
            this.TextEdit_ach_error_code.Properties.Mask.EditMask = "[A-Z0-9][0-9][0-9]";
			this.TextEdit_ach_error_code.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.RegEx;
			this.TextEdit_ach_error_code.Properties.MaxLength = 3;
			this.TextEdit_ach_error_code.Size = new System.Drawing.Size(192, 20);
			this.TextEdit_ach_error_code.TabIndex = 25;
			//
			//TextEdit_description
			//
			this.TextEdit_description.Location = new System.Drawing.Point(88, 40);
			this.TextEdit_description.Name = "TextEdit_description";
			this.TextEdit_description.Properties.MaxLength = 50;
			this.TextEdit_description.Size = new System.Drawing.Size(192, 20);
			this.TextEdit_description.TabIndex = 26;
			//
			//EditForm
			//
			this.AutoScaleDimensions = new System.Drawing.SizeF(6f, 13f);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(409, 136);
			this.Controls.Add(this.TextEdit_ach_error_code);
			this.Controls.Add(this.TextEdit_description);
			this.Controls.Add(this.LabelControl2);
			this.Controls.Add(this.LabelControl1);
			this.Controls.Add(this.CheckEdit_resend_funds);
			this.Name = "EditForm";
			this.ToolTipController1.SetSuperTip(this, null);
			this.Text = "EditForm";
			this.Controls.SetChildIndex(this.CheckEdit_resend_funds, 0);
			this.Controls.SetChildIndex(this.LabelControl1, 0);
			this.Controls.SetChildIndex(this.LabelControl2, 0);
			this.Controls.SetChildIndex(this.TextEdit_description, 0);
			this.Controls.SetChildIndex(this.TextEdit_ach_error_code, 0);
			this.Controls.SetChildIndex(this.simpleButton_OK, 0);
			this.Controls.SetChildIndex(this.simpleButton_Cancel, 0);
			((System.ComponentModel.ISupportInitialize)this.DxErrorProvider1).EndInit();
			((System.ComponentModel.ISupportInitialize)this.CheckEdit_resend_funds.Properties).EndInit();
			((System.ComponentModel.ISupportInitialize)this.TextEdit_ach_error_code.Properties).EndInit();
			((System.ComponentModel.ISupportInitialize)this.TextEdit_description.Properties).EndInit();
			this.ResumeLayout(false);
			this.PerformLayout();

		}
		private DevExpress.XtraEditors.LabelControl LabelControl1;
		private DevExpress.XtraEditors.LabelControl LabelControl2;
		private DevExpress.XtraEditors.CheckEdit CheckEdit_resend_funds;
		private DevExpress.XtraEditors.TextEdit TextEdit_ach_error_code;
		private DevExpress.XtraEditors.TextEdit TextEdit_description;
	}
}

