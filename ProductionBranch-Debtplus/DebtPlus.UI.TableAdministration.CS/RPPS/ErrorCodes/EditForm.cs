using System;
using DebtPlus.LINQ;

#region "Copyright 2000-2012 DebtPlus, L.L.C."
//{*******************************************************************}
//{                                                                   }
//{       DebtPlus Debt Management System                             }
//{                                                                   }
//{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
//{       ALL RIGHTS RESERVED                                         }
//{                                                                   }
//{   The entire contents of this file is protected by U.S. and       }
//{   International Copyright Laws. Unauthorized reproduction,        }
//{   reverse-engineering, and distribution of all or any portion of  }
//{   the code contained in this file is strictly prohibited and may  }
//{   result in severe civil and criminal penalties and will be       }
//{   prosecuted to the maximum extent possible under the law.        }
//{                                                                   }
//{   RESTRICTIONS                                                    }
//{                                                                   }
//{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
//{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
//{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
//{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
//{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
//{                                                                   }
//{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
//{   ADDITIONAL RESTRICTIONS.                                        }
//{                                                                   }
//{*******************************************************************}

#endregion "Copyright 2000-2012 DebtPlus, L.L.C."

namespace DebtPlus.UI.TableAdministration.CS.RPPS.ErrorCodes
{
    internal partial class EditForm : Templates.EditTemplateForm
    {
        private BusinessContext bc = null;
        private rpps_reject_code record = null;
        private System.Collections.Generic.List<rpps_reject_code> colRecords = null;

        /// <summary>
        /// Initialize the new class
        /// </summary>
        internal EditForm()
            : base()
        {
            InitializeComponent();
        }

        /// <summary>
        /// Initialize the new class
        /// </summary>
        internal EditForm(BusinessContext bc, System.Collections.Generic.List<rpps_reject_code> colRecords, rpps_reject_code record)
            : this()
        {
            this.bc = bc;
            this.record = record;
            this.colRecords = colRecords;

            RegisterHandlers();
        }

        /// <summary>
        /// Register the event handlers
        /// </summary>
        private void RegisterHandlers()
        {
            this.Load += EditForm_Load;
            TextEdit_ach_error_code.Validating += TextEdit_ach_error_code_Validating;
            TextEdit_ach_error_code.TextChanged += Form_Changed;
            TextEdit_description.TextChanged += Form_Changed;
        }

        /// <summary>
        /// Remove the event handler registrations
        /// </summary>
        private void UnRegisterHandlers()
        {
            this.Load -= EditForm_Load;
            TextEdit_ach_error_code.Validating -= TextEdit_ach_error_code_Validating;
            TextEdit_ach_error_code.TextChanged -= Form_Changed;
            TextEdit_description.TextChanged -= Form_Changed;
        }

        /// <summary>
        /// Process the LOAD event on the form
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void EditForm_Load(object sender, EventArgs e)
        {
            UnRegisterHandlers();
            try
            {
                TextEdit_ach_error_code.EditValue = record.Id;
                TextEdit_description.EditValue = record.description;
                CheckEdit_resend_funds.Checked = record.resend_funds;

                simpleButton_OK.Enabled = !HasErrors();
            }
            finally
            {
                RegisterHandlers();
            }
        }

        /// <summary>
        /// Validate a change to the error code
        /// </summary>
        private void TextEdit_ach_error_code_Validating(object sender, System.ComponentModel.CancelEventArgs e)
        {
            // Remove the leading and trailing spaces from the code
            string newCode = TextEdit_ach_error_code.Text.Trim().ToUpper();
            if (newCode != TextEdit_ach_error_code.Text)
            {
                TextEdit_ach_error_code.Text = newCode;
            }

            // If there is no code then we can't accept the update
            if (string.IsNullOrWhiteSpace(newCode))
            {
                TextEdit_ach_error_code.ErrorText = "This field is required";
                e.Cancel = true;
                return;
            }

            // Ensure that the code does not currently exist
            var q = colRecords.Find(s => string.Compare(s.Id, newCode, true) == 0);
            if (q != null && q != record)
            {
                TextEdit_ach_error_code.ErrorText = "The error code already exists.";
                e.Cancel = true;
                return;
            }

            // All's well with the error code value
            TextEdit_ach_error_code.ErrorText = string.Empty;
        }

        /// <summary>
        /// Process a change on the form
        /// </summary>
        private void Form_Changed(object sender, EventArgs e)
        {
            simpleButton_OK.Enabled = !HasErrors();
        }

        /// <summary>
        /// Determine if the controls will properly represent the new record
        /// </summary>
        /// <returns></returns>
        private bool HasErrors()
        {
            if (string.IsNullOrWhiteSpace(TextEdit_description.Text))
            {
                return true;
            }

            if (string.IsNullOrWhiteSpace(TextEdit_ach_error_code.Text))
            {
                return true;
            }

            return false;
        }

        /// <summary>
        /// Process a CLICK event on the OK button
        /// </summary>
        protected override void simpleButton_OK_Click(object sender, EventArgs e)
        {
            base.simpleButton_OK_Click(sender, e);

            // Retrieve the values from the editing controls for the new record
            record.Id = TextEdit_ach_error_code.Text.Trim();
            record.description = TextEdit_description.Text.Trim();
            record.resend_funds = CheckEdit_resend_funds.Checked;
        }
    }
}