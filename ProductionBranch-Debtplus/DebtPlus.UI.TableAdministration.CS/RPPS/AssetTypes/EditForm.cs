#region "Copyright 2000-2012 DebtPlus, L.L.C."
//{*******************************************************************}
//{                                                                   }
//{       DebtPlus Debt Management System                             }
//{                                                                   }
//{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
//{       ALL RIGHTS RESERVED                                         }
//{                                                                   }
//{   The entire contents of this file is protected by U.S. and       }
//{   International Copyright Laws. Unauthorized reproduction,        }
//{   reverse-engineering, and distribution of all or any portion of  }
//{   the code contained in this file is strictly prohibited and may  }
//{   result in severe civil and criminal penalties and will be       }
//{   prosecuted to the maximum extent possible under the law.        }
//{                                                                   }
//{   RESTRICTIONS                                                    }
//{                                                                   }
//{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
//{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
//{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
//{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
//{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
//{                                                                   }
//{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
//{   ADDITIONAL RESTRICTIONS.                                        }
//{                                                                   }
//{*******************************************************************}

#endregion "Copyright 2000-2012 DebtPlus, L.L.C."

using System;
using DebtPlus.LINQ;

namespace DebtPlus.UI.TableAdministration.CS.RPPS.AssetTypes
{
    internal partial class EditForm : Templates.EditTemplateForm
    {
        private RPPSAssetType record = null;

        /// <summary>
        /// Initialize the new class
        /// </summary>
        internal EditForm()
            : base()
        {
            InitializeComponent();
        }

        /// <summary>
        /// Initialize the new class
        /// </summary>
        internal EditForm(RPPSAssetType record)
            : this()
        {
            this.record = record;
            RegisterHandlers();
        }

        /// <summary>
        /// Register the event handlers
        /// </summary>
        private void RegisterHandlers()
        {
            Load -= EditForm_Load;
            TextEdit_description.EditValueChanged += Form_Changed;
            CheckEdit_ActiveFlag.CheckedChanged += Form_Changed;
            CheckEdit_default.CheckedChanged += Form_Changed;
            TextEdit_oID.EditValueChanged += Form_Changed;
        }

        /// <summary>
        /// Remove the event registrations
        /// </summary>
        private void UnRegisterHandlers()
        {
            Load -= EditForm_Load;
            TextEdit_description.EditValueChanged -= Form_Changed;
            CheckEdit_ActiveFlag.CheckedChanged -= Form_Changed;
            CheckEdit_default.CheckedChanged -= Form_Changed;
            TextEdit_oID.EditValueChanged -= Form_Changed;
        }

        /// <summary>
        /// Process the LOAD event
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void EditForm_Load(object sender, EventArgs e)
        {
            UnRegisterHandlers();
            try
            {
                TextEdit_oID.EditValue = record.Id;
                TextEdit_description.EditValue = record.description;
                CheckEdit_default.Checked = record.Default;
                CheckEdit_ActiveFlag.Checked = record.ActiveFlag;

                simpleButton_OK.Enabled = !HasErrors();
            }
            finally
            {
                RegisterHandlers();
            }
        }

        /// <summary>
        /// Determine if the controls have any errors for the record
        /// </summary>
        /// <returns></returns>
        private bool HasErrors()
        {
            if (string.IsNullOrWhiteSpace(TextEdit_description.Text))
            {
                return true;
            }

            if (string.IsNullOrWhiteSpace(TextEdit_oID.Text))
            {
                return true;
            }

            return false;
        }

        /// <summary>
        /// Process a change in the form controls
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Form_Changed(object sender, EventArgs e)
        {
            simpleButton_OK.Enabled = !HasErrors();
        }

        /// <summary>
        /// Handle the CLICK event on the OK button
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected override void simpleButton_OK_Click(object sender, EventArgs e)
        {
            base.simpleButton_OK_Click(sender, e);

            record.description = TextEdit_description.Text.Trim();
            record.Default = CheckEdit_default.Checked;
            record.ActiveFlag = CheckEdit_ActiveFlag.Checked;
            record.Id = DebtPlus.Utils.Nulls.v_Int32(TextEdit_oID.EditValue).GetValueOrDefault();
        }
    }
}