#region "Copyright 2000-2012 DebtPlus, L.L.C."
//{*******************************************************************}
//{                                                                   }
//{       DebtPlus Debt Management System                             }
//{                                                                   }
//{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
//{       ALL RIGHTS RESERVED                                         }
//{                                                                   }
//{   The entire contents of this file is protected by U.S. and       }
//{   International Copyright Laws. Unauthorized reproduction,        }
//{   reverse-engineering, and distribution of all or any portion of  }
//{   the code contained in this file is strictly prohibited and may  }
//{   result in severe civil and criminal penalties and will be       }
//{   prosecuted to the maximum extent possible under the law.        }
//{                                                                   }
//{   RESTRICTIONS                                                    }
//{                                                                   }
//{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
//{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
//{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
//{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
//{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
//{                                                                   }
//{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
//{   ADDITIONAL RESTRICTIONS.                                        }
//{                                                                   }
//{*******************************************************************}

#endregion "Copyright 2000-2012 DebtPlus, L.L.C."

using System;
using DebtPlus.LINQ;

namespace DebtPlus.UI.TableAdministration.CS.RPPS.BillerTypes
{
    internal partial class EditForm : Templates.EditTemplateForm
    {
        private RPPSBillerType record = null;

        internal EditForm()
            : base()
        {
            InitializeComponent();
        }

        internal EditForm(RPPSBillerType record)
            : this()
        {
            this.record = record;
            RegisterHandlers();
        }

        private void RegisterHandlers()
        {
            Load += EditForm_Load;
            TextEdit_description.EditValueChanged += formChanged;
            TextEdit_oID.EditValueChanged += formChanged;
            CheckEdit_ActiveFlag.CheckedChanged += formChanged;
            CheckEdit_default.CheckedChanged += formChanged;
        }

        private void UnRegisterHandlers()
        {
            Load -= EditForm_Load;
            TextEdit_description.EditValueChanged -= formChanged;
            TextEdit_oID.EditValueChanged -= formChanged;
            CheckEdit_ActiveFlag.CheckedChanged -= formChanged;
            CheckEdit_default.CheckedChanged -= formChanged;
        }

        private void EditForm_Load(object sender, EventArgs e)
        {
            UnRegisterHandlers();
            try
            {
                TextEdit_oID.EditValue = record.Id;
                TextEdit_description.EditValue = record.description;
                CheckEdit_ActiveFlag.Checked = record.ActiveFlag;
                CheckEdit_default.Checked = record.Default;

                simpleButton_OK.Enabled = !HasErrors();
            }
            finally
            {
                RegisterHandlers();
            }
        }

        private void formChanged(object sender, EventArgs e)
        {
            simpleButton_OK.Enabled = !HasErrors();
        }

        private bool HasErrors()
        {
            if (string.IsNullOrWhiteSpace(DebtPlus.Utils.Nulls.v_String(TextEdit_description.EditValue)))
            {
                return true;
            }

            if (DebtPlus.Utils.Nulls.v_Int32(TextEdit_oID.EditValue).GetValueOrDefault(0) <= 0)
            {
                return true;
            }
            return false;
        }

        protected override void simpleButton_OK_Click(object sender, EventArgs e)
        {
            record.description = DebtPlus.Utils.Nulls.v_String(TextEdit_description.EditValue);
            record.Id = DebtPlus.Utils.Nulls.v_Int32(TextEdit_oID.EditValue).Value;
            record.Default = CheckEdit_default.Checked;
            record.ActiveFlag = CheckEdit_ActiveFlag.Checked;

            base.simpleButton_OK_Click(sender, e);
        }
    }
}