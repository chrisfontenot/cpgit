using System.Windows.Forms;

namespace DebtPlus.UI.TableAdministration.CS.RPPS.BillerTypes
{
	partial class EditForm
	{
		//Form overrides dispose to clean up the component list.
		[System.Diagnostics.DebuggerNonUserCode]
		protected override void Dispose(bool disposing)
		{
			try 
            {
				if (disposing)
                {
                    if (components != null) components.Dispose();
				}
                components = null;
            }
    	    finally
            {
				base.Dispose(disposing);
			}
		}

		//Required by the Windows Form Designer
		private System.ComponentModel.IContainer components = null;

		//NOTE: The following procedure is required by the Windows Form Designer
		//It can be modified using the Windows Form Designer.  
		//Do not modify it using the code editor.
		[System.Diagnostics.DebuggerStepThrough]
		private void InitializeComponent()
		{
            this.LayoutControl1 = new DevExpress.XtraLayout.LayoutControl();
            this.CheckEdit_ActiveFlag = new DevExpress.XtraEditors.CheckEdit();
            this.CheckEdit_default = new DevExpress.XtraEditors.CheckEdit();
            this.TextEdit_description = new DevExpress.XtraEditors.TextEdit();
            this.TextEdit_oID = new DevExpress.XtraEditors.TextEdit();
            this.LayoutControlGroup1 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.LayoutControlItem3 = new DevExpress.XtraLayout.LayoutControlItem();
            this.LayoutControlItem8 = new DevExpress.XtraLayout.LayoutControlItem();
            this.EmptySpaceItem1 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.LayoutControlItem9 = new DevExpress.XtraLayout.LayoutControlItem();
            this.LayoutControlItem6 = new DevExpress.XtraLayout.LayoutControlItem();
            this.EmptySpaceItem2 = new DevExpress.XtraLayout.EmptySpaceItem();
            ((System.ComponentModel.ISupportInitialize)(this.DxErrorProvider1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LayoutControl1)).BeginInit();
            this.LayoutControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.CheckEdit_ActiveFlag.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.CheckEdit_default.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TextEdit_description.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TextEdit_oID.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LayoutControlGroup1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LayoutControlItem3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LayoutControlItem8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.EmptySpaceItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LayoutControlItem9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LayoutControlItem6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.EmptySpaceItem2)).BeginInit();
            this.SuspendLayout();
            // 
            // simpleButton_OK
            // 
            this.simpleButton_OK.Location = new System.Drawing.Point(372, 13);
            this.simpleButton_OK.TabIndex = 21;
            // 
            // simpleButton_Cancel
            // 
            this.simpleButton_Cancel.Location = new System.Drawing.Point(372, 56);
            this.simpleButton_Cancel.TabIndex = 22;
            // 
            // DefaultLookAndFeel1
            // 
            this.DefaultLookAndFeel1.LookAndFeel.SkinName = "McSkin";
            // 
            // LayoutControl1
            // 
            this.LayoutControl1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.LayoutControl1.Controls.Add(this.CheckEdit_ActiveFlag);
            this.LayoutControl1.Controls.Add(this.CheckEdit_default);
            this.LayoutControl1.Controls.Add(this.TextEdit_description);
            this.LayoutControl1.Controls.Add(this.TextEdit_oID);
            this.LayoutControl1.Location = new System.Drawing.Point(0, 0);
            this.LayoutControl1.Name = "LayoutControl1";
            this.LayoutControl1.OptionsCustomizationForm.DesignTimeCustomizationFormPositionAndSize = new System.Drawing.Rectangle(450, 95, 250, 350);
            this.LayoutControl1.Root = this.LayoutControlGroup1;
            this.LayoutControl1.Size = new System.Drawing.Size(370, 172);
            this.LayoutControl1.TabIndex = 23;
            this.LayoutControl1.Text = "LayoutControl1";
            // 
            // CheckEdit_ActiveFlag
            // 
            this.CheckEdit_ActiveFlag.Location = new System.Drawing.Point(186, 140);
            this.CheckEdit_ActiveFlag.Name = "CheckEdit_ActiveFlag";
            this.CheckEdit_ActiveFlag.Properties.Caption = "Active";
            this.CheckEdit_ActiveFlag.Size = new System.Drawing.Size(172, 20);
            this.CheckEdit_ActiveFlag.StyleController = this.LayoutControl1;
            this.CheckEdit_ActiveFlag.TabIndex = 21;
            // 
            // CheckEdit_default
            // 
            this.CheckEdit_default.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.CheckEdit_default.Location = new System.Drawing.Point(12, 140);
            this.CheckEdit_default.Name = "CheckEdit_default";
            this.CheckEdit_default.Properties.Caption = "Default Item";
            this.CheckEdit_default.Size = new System.Drawing.Size(170, 20);
            this.CheckEdit_default.StyleController = this.LayoutControl1;
            this.CheckEdit_default.TabIndex = 20;
            // 
            // TextEdit_description
            // 
            this.TextEdit_description.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.TextEdit_description.Location = new System.Drawing.Point(68, 36);
            this.TextEdit_description.Name = "TextEdit_description";
            this.TextEdit_description.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.False;
            this.TextEdit_description.Properties.MaxLength = 50;
            this.TextEdit_description.Size = new System.Drawing.Size(290, 20);
            this.TextEdit_description.StyleController = this.LayoutControl1;
            this.TextEdit_description.TabIndex = 7;
            // 
            // TextEdit_oID
            // 
            this.TextEdit_oID.Location = new System.Drawing.Point(68, 12);
            this.TextEdit_oID.Name = "TextEdit_oID";
            this.TextEdit_oID.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.True;
            this.TextEdit_oID.Properties.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.TextEdit_oID.Properties.DisplayFormat.FormatString = "f0";
            this.TextEdit_oID.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.TextEdit_oID.Properties.EditFormat.FormatString = "f0";
            this.TextEdit_oID.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.TextEdit_oID.Properties.Mask.BeepOnError = true;
            this.TextEdit_oID.Properties.Mask.EditMask = "f0";
            this.TextEdit_oID.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.TextEdit_oID.Properties.MaxLength = 4;
            this.TextEdit_oID.Size = new System.Drawing.Size(115, 20);
            this.TextEdit_oID.StyleController = this.LayoutControl1;
            this.TextEdit_oID.TabIndex = 15;
            // 
            // LayoutControlGroup1
            // 
            this.LayoutControlGroup1.CustomizationFormText = "LayoutControlGroup1";
            this.LayoutControlGroup1.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.True;
            this.LayoutControlGroup1.GroupBordersVisible = false;
            this.LayoutControlGroup1.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.LayoutControlItem3,
            this.LayoutControlItem8,
            this.EmptySpaceItem1,
            this.LayoutControlItem9,
            this.LayoutControlItem6,
            this.EmptySpaceItem2});
            this.LayoutControlGroup1.Location = new System.Drawing.Point(0, 0);
            this.LayoutControlGroup1.Name = "LayoutControlGroup1";
            this.LayoutControlGroup1.Size = new System.Drawing.Size(370, 172);
            this.LayoutControlGroup1.Text = "LayoutControlGroup1";
            this.LayoutControlGroup1.TextVisible = false;
            // 
            // LayoutControlItem3
            // 
            this.LayoutControlItem3.Control = this.TextEdit_description;
            this.LayoutControlItem3.CustomizationFormText = "Description";
            this.LayoutControlItem3.Location = new System.Drawing.Point(0, 24);
            this.LayoutControlItem3.Name = "LayoutControlItem3";
            this.LayoutControlItem3.Size = new System.Drawing.Size(350, 24);
            this.LayoutControlItem3.Text = "Description";
            this.LayoutControlItem3.TextSize = new System.Drawing.Size(53, 13);
            // 
            // LayoutControlItem8
            // 
            this.LayoutControlItem8.Control = this.CheckEdit_default;
            this.LayoutControlItem8.CustomizationFormText = "Default Item";
            this.LayoutControlItem8.Location = new System.Drawing.Point(0, 128);
            this.LayoutControlItem8.Name = "LayoutControlItem8";
            this.LayoutControlItem8.Size = new System.Drawing.Size(174, 24);
            this.LayoutControlItem8.Text = "Default Item";
            this.LayoutControlItem8.TextSize = new System.Drawing.Size(0, 0);
            this.LayoutControlItem8.TextToControlDistance = 0;
            this.LayoutControlItem8.TextVisible = false;
            // 
            // EmptySpaceItem1
            // 
            this.EmptySpaceItem1.AllowHotTrack = false;
            this.EmptySpaceItem1.CustomizationFormText = "EmptySpaceItem1";
            this.EmptySpaceItem1.Location = new System.Drawing.Point(0, 48);
            this.EmptySpaceItem1.Name = "EmptySpaceItem1";
            this.EmptySpaceItem1.Size = new System.Drawing.Size(350, 80);
            this.EmptySpaceItem1.Text = "EmptySpaceItem1";
            this.EmptySpaceItem1.TextSize = new System.Drawing.Size(0, 0);
            // 
            // LayoutControlItem9
            // 
            this.LayoutControlItem9.Control = this.CheckEdit_ActiveFlag;
            this.LayoutControlItem9.CustomizationFormText = "Active Status";
            this.LayoutControlItem9.Location = new System.Drawing.Point(174, 128);
            this.LayoutControlItem9.Name = "LayoutControlItem9";
            this.LayoutControlItem9.Size = new System.Drawing.Size(176, 24);
            this.LayoutControlItem9.Text = "Active Status";
            this.LayoutControlItem9.TextSize = new System.Drawing.Size(0, 0);
            this.LayoutControlItem9.TextToControlDistance = 0;
            this.LayoutControlItem9.TextVisible = false;
            // 
            // LayoutControlItem6
            // 
            this.LayoutControlItem6.Control = this.TextEdit_oID;
            this.LayoutControlItem6.CustomizationFormText = "ID Value";
            this.LayoutControlItem6.Location = new System.Drawing.Point(0, 0);
            this.LayoutControlItem6.Name = "LayoutControlItem6";
            this.LayoutControlItem6.Size = new System.Drawing.Size(175, 24);
            this.LayoutControlItem6.Text = "ID";
            this.LayoutControlItem6.TextSize = new System.Drawing.Size(53, 13);
            // 
            // EmptySpaceItem2
            // 
            this.EmptySpaceItem2.AllowHotTrack = false;
            this.EmptySpaceItem2.CustomizationFormText = "EmptySpaceItem2";
            this.EmptySpaceItem2.Location = new System.Drawing.Point(175, 0);
            this.EmptySpaceItem2.Name = "EmptySpaceItem2";
            this.EmptySpaceItem2.Size = new System.Drawing.Size(175, 24);
            this.EmptySpaceItem2.Text = "EmptySpaceItem2";
            this.EmptySpaceItem2.TextSize = new System.Drawing.Size(0, 0);
            // 
            // EditForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(468, 163);
            this.Controls.Add(this.LayoutControl1);
            this.Name = "EditForm";
            this.Text = "RPPS Billers Type";
            this.Controls.SetChildIndex(this.simpleButton_OK, 0);
            this.Controls.SetChildIndex(this.simpleButton_Cancel, 0);
            this.Controls.SetChildIndex(this.LayoutControl1, 0);
            ((System.ComponentModel.ISupportInitialize)(this.DxErrorProvider1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LayoutControl1)).EndInit();
            this.LayoutControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.CheckEdit_ActiveFlag.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.CheckEdit_default.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TextEdit_description.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TextEdit_oID.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LayoutControlGroup1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LayoutControlItem3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LayoutControlItem8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.EmptySpaceItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LayoutControlItem9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LayoutControlItem6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.EmptySpaceItem2)).EndInit();
            this.ResumeLayout(false);

		}

		private DevExpress.XtraEditors.TextEdit TextEdit_description;
		private DevExpress.XtraEditors.TextEdit TextEdit_oID;
		private DevExpress.XtraEditors.CheckEdit CheckEdit_default;
		private DevExpress.XtraLayout.LayoutControl LayoutControl1;
		private DevExpress.XtraLayout.LayoutControlGroup LayoutControlGroup1;
		private DevExpress.XtraEditors.CheckEdit CheckEdit_ActiveFlag;
		private DevExpress.XtraLayout.LayoutControlItem LayoutControlItem3;
		private DevExpress.XtraLayout.LayoutControlItem LayoutControlItem8;
		private DevExpress.XtraLayout.LayoutControlItem LayoutControlItem6;
		private DevExpress.XtraLayout.EmptySpaceItem EmptySpaceItem1;
		private DevExpress.XtraLayout.LayoutControlItem LayoutControlItem9;
		private DevExpress.XtraLayout.EmptySpaceItem EmptySpaceItem2;
	}
}
