﻿namespace DebtPlus.UI.TableAdministration.CS.OfficeTypes
{
    partial class MainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            try
            {
                if (disposing)
                {
                    if (components != null) components.Dispose();
                    if (bc != null) bc.Dispose();
                }
                components = null;
                bc = null;
            }

            finally
            {
                base.Dispose(disposing);
            }
        }


        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.gridColumn_ID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn_Description = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn_ActiveFlag = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn_Default = new DevExpress.XtraGrid.Columns.GridColumn();
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl1)).BeginInit();
            this.layoutControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem_simpleButton_Cancel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem_SimpleButton_Edit)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem_simpleButton_New)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.popupMenu1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DxErrorProvider1)).BeginInit();
            this.SuspendLayout();
            this.layoutControl1.Controls.SetChildIndex(this.SimpleButton_Edit, 0);
            this.layoutControl1.Controls.SetChildIndex(this.simpleButton_Cancel, 0);
            this.layoutControl1.Controls.SetChildIndex(this.simpleButton_New, 0);
            this.layoutControl1.Controls.SetChildIndex(this.gridControl1, 0);
            // 
            // gridView1
            // 
            this.gridView1.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gridColumn_ID,
            this.gridColumn_Description,
            this.gridColumn_ActiveFlag,
            this.gridColumn_Default});
            // 
            // gridColumn_ID
            // 
            this.gridColumn_ID.AppearanceCell.Options.UseTextOptions = true;
            this.gridColumn_ID.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.gridColumn_ID.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColumn_ID.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.gridColumn_ID.Caption = "ID";
            this.gridColumn_ID.CustomizationCaption = "Record ID";
            this.gridColumn_ID.DisplayFormat.FormatString = "f0";
            this.gridColumn_ID.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.gridColumn_ID.FieldName = "Id";
            this.gridColumn_ID.GroupFormat.FormatString = "f0";
            this.gridColumn_ID.GroupFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.gridColumn_ID.Name = "gridColumn_ID";
            this.gridColumn_ID.Visible = true;
            this.gridColumn_ID.VisibleIndex = 0;
            this.gridColumn_ID.Width = 83;
            // 
            // gridColumn_Description
            // 
            this.gridColumn_Description.Caption = "Description";
            this.gridColumn_Description.CustomizationCaption = "Record Description";
            this.gridColumn_Description.FieldName = "description";
            this.gridColumn_Description.Name = "gridColumn_Description";
            this.gridColumn_Description.Visible = true;
            this.gridColumn_Description.VisibleIndex = 1;
            this.gridColumn_Description.Width = 415;
            // 
            // gridColumn_ActiveFlag
            // 
            this.gridColumn_ActiveFlag.AppearanceCell.Options.UseTextOptions = true;
            this.gridColumn_ActiveFlag.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumn_ActiveFlag.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColumn_ActiveFlag.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumn_ActiveFlag.Caption = "Active";
            this.gridColumn_ActiveFlag.CustomizationCaption = "Active Indicator";
            this.gridColumn_ActiveFlag.FieldName = "ActiveFlag";
            this.gridColumn_ActiveFlag.Name = "gridColumn_ActiveFlag";
            this.gridColumn_ActiveFlag.Visible = true;
            this.gridColumn_ActiveFlag.VisibleIndex = 2;
            this.gridColumn_ActiveFlag.Width = 90;
            // 
            // gridColumn_Default
            // 
            this.gridColumn_Default.AppearanceCell.Options.UseTextOptions = true;
            this.gridColumn_Default.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumn_Default.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColumn_Default.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumn_Default.Caption = "Default";
            this.gridColumn_Default.CustomizationCaption = "Default Item Flag";
            this.gridColumn_Default.FieldName = "default";
            this.gridColumn_Default.Name = "gridColumn_Default";
            this.gridColumn_Default.Visible = true;
            this.gridColumn_Default.VisibleIndex = 3;
            this.gridColumn_Default.Width = 78;
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(528, 294);
            this.Name = "MainForm";
            this.Text = "Office Types";
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl1)).EndInit();
            this.layoutControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem_simpleButton_Cancel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem_SimpleButton_Edit)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem_simpleButton_New)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.popupMenu1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DxErrorProvider1)).EndInit();
            this.ResumeLayout(false);

        }


        private DevExpress.XtraGrid.Columns.GridColumn gridColumn_ID;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn_Description;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn_ActiveFlag;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn_Default;
    }
}