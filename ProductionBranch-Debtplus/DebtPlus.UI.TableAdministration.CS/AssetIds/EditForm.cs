﻿using System;
using DebtPlus.LINQ;

namespace DebtPlus.UI.TableAdministration.CS.AssetIds
{
    internal partial class EditForm : Templates.EditTemplateForm
    {
        // Current record being edited
        private asset_id record;

        /// <summary>
        /// Initialize the new class instance
        /// </summary>
        internal EditForm()
            : base()
        {
            InitializeComponent();
        }

        /// <summary>
        /// Initialize the new class instance
        /// </summary>
        /// <param name="record"></param>
        internal EditForm(asset_id record)
            : this()
        {
            this.record = record;
            RegisterHandlers();
        }

        /// <summary>
        /// Add the event handlers to the collection
        /// </summary>
        private void RegisterHandlers()
        {
            Load += EditForm_Load;
            TextEdit_description.EditValueChanged += FormChanged;
        }

        /// <summary>
        /// Remove the event handlers from being processed
        /// </summary>
        private void UnRegisterHandlers()
        {
            Load -= EditForm_Load;
            TextEdit_description.EditValueChanged -= FormChanged;
        }

        /// <summary>
        /// Process the FORM LOAD event
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void EditForm_Load(object sender, System.EventArgs e)
        {
            UnRegisterHandlers();
            try
            {
                LabelControl_ID.Text = record.Id <= 0 ? "NEW" : record.Id.ToString();
                TextEdit_description.EditValue = record.description;
                TextEdit_rpps_code.EditValue = record.rpps_code;
                CalcEdit_maximum.EditValue = record.maximum;

                simpleButton_OK.Enabled = !HasErrors();
            }
            finally
            {
                RegisterHandlers();
            }
        }

        /// <summary>
        /// Override the OK button to save the editing values into the record
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected override void simpleButton_OK_Click(object sender, EventArgs e)
        {
            record.description = Convert.ToString(TextEdit_description.EditValue);
            record.rpps_code = Convert.ToInt32(TextEdit_rpps_code.EditValue);
            record.maximum = DebtPlus.Utils.Nulls.v_Decimal(CalcEdit_maximum.EditValue);

            base.simpleButton_OK_Click(sender, e);
        }

        private void FormChanged(object sender, EventArgs e)
        {
            simpleButton_OK.Enabled = !HasErrors();
        }

        /// <summary>
        /// Determine if the input form has error(s)
        /// </summary>
        /// <returns></returns>
        private bool HasErrors()
        {
            return TextEdit_description.Text.Trim() == string.Empty;
        }
    }
}