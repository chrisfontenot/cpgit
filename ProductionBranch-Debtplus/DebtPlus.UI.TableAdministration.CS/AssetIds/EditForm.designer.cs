﻿namespace DebtPlus.UI.TableAdministration.CS.AssetIds
{
    partial class EditForm
    {
		//Form overrides dispose to clean up the component list.
		[System.Diagnostics.DebuggerNonUserCode]
		protected override void Dispose(bool disposing)
		{
			try 
            {
				if (disposing)
                {
                    if (components != null) components.Dispose();
				}
                components = null;
            }
    	    finally
            {
				base.Dispose(disposing);
			}
		}

		//Required by the Windows Form Designer
		private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.TextEdit_rpps_code = new DevExpress.XtraEditors.TextEdit();
            this.LabelControl5 = new DevExpress.XtraEditors.LabelControl();
            this.CalcEdit_maximum = new DevExpress.XtraEditors.CalcEdit();
            this.LabelControl4 = new DevExpress.XtraEditors.LabelControl();
            this.TextEdit_description = new DevExpress.XtraEditors.TextEdit();
            this.LabelControl3 = new DevExpress.XtraEditors.LabelControl();
            this.LabelControl_ID = new DevExpress.XtraEditors.LabelControl();
            this.LabelControl1 = new DevExpress.XtraEditors.LabelControl();
            ((System.ComponentModel.ISupportInitialize)(this.DxErrorProvider1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TextEdit_rpps_code.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.CalcEdit_maximum.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TextEdit_description.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // simpleButton_Cancel
            // 
            this.simpleButton_Cancel.Location = new System.Drawing.Point(347, 67);
            // 
            // simpleButton_OK
            // 
            this.simpleButton_OK.Location = new System.Drawing.Point(347, 35);
            // 
            // DefaultLookAndFeel1
            // 
            this.DefaultLookAndFeel1.LookAndFeel.SkinName = "McSkin";
            // 
            // TextEdit_rpps_code
            // 
            this.TextEdit_rpps_code.Location = new System.Drawing.Point(103, 104);
            this.TextEdit_rpps_code.Name = "TextEdit_rpps_code";
            this.TextEdit_rpps_code.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.False;
            this.TextEdit_rpps_code.Properties.Appearance.Options.UseTextOptions = true;
            this.TextEdit_rpps_code.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.TextEdit_rpps_code.Properties.DisplayFormat.FormatString = "f0";
            this.TextEdit_rpps_code.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.TextEdit_rpps_code.Properties.EditFormat.FormatString = "f0";
            this.TextEdit_rpps_code.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.TextEdit_rpps_code.Properties.Mask.BeepOnError = true;
            this.TextEdit_rpps_code.Properties.Mask.EditMask = "\\d+";
            this.TextEdit_rpps_code.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.RegEx;
            this.TextEdit_rpps_code.Size = new System.Drawing.Size(100, 20);
            this.TextEdit_rpps_code.TabIndex = 35;
            // 
            // LabelControl5
            // 
            this.LabelControl5.Location = new System.Drawing.Point(10, 107);
            this.LabelControl5.Name = "LabelControl5";
            this.LabelControl5.Size = new System.Drawing.Size(53, 13);
            this.LabelControl5.TabIndex = 34;
            this.LabelControl5.Text = "RPPS Code";
            // 
            // CalcEdit_maximum
            // 
            this.CalcEdit_maximum.Location = new System.Drawing.Point(103, 78);
            this.CalcEdit_maximum.Name = "CalcEdit_maximum";
            this.CalcEdit_maximum.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.True;
            this.CalcEdit_maximum.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.CalcEdit_maximum.Properties.DisplayFormat.FormatString = "{0:c}";
            this.CalcEdit_maximum.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.CalcEdit_maximum.Properties.EditFormat.FormatString = "{0:f2}";
            this.CalcEdit_maximum.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.CalcEdit_maximum.Properties.Mask.EditMask = "c";
            this.CalcEdit_maximum.Properties.Precision = 2;
            this.CalcEdit_maximum.Size = new System.Drawing.Size(100, 20);
            this.CalcEdit_maximum.TabIndex = 33;
            // 
            // LabelControl4
            // 
            this.LabelControl4.Location = new System.Drawing.Point(10, 81);
            this.LabelControl4.Name = "LabelControl4";
            this.LabelControl4.Size = new System.Drawing.Size(73, 13);
            this.LabelControl4.TabIndex = 32;
            this.LabelControl4.Text = "Maximum Value";
            // 
            // TextEdit_description
            // 
            this.TextEdit_description.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.TextEdit_description.Location = new System.Drawing.Point(103, 52);
            this.TextEdit_description.Name = "TextEdit_description";
            this.TextEdit_description.Properties.MaxLength = 50;
            this.TextEdit_description.Size = new System.Drawing.Size(219, 20);
            this.TextEdit_description.TabIndex = 31;
            // 
            // LabelControl3
            // 
            this.LabelControl3.Location = new System.Drawing.Point(10, 55);
            this.LabelControl3.Name = "LabelControl3";
            this.LabelControl3.Size = new System.Drawing.Size(53, 13);
            this.LabelControl3.TabIndex = 30;
            this.LabelControl3.Text = "Description";
            // 
            // LabelControl_ID
            // 
            this.LabelControl_ID.Location = new System.Drawing.Point(103, 22);
            this.LabelControl_ID.Name = "LabelControl_ID";
            this.LabelControl_ID.Size = new System.Drawing.Size(23, 13);
            this.LabelControl_ID.TabIndex = 29;
            this.LabelControl_ID.Text = "NEW";
            // 
            // LabelControl1
            // 
            this.LabelControl1.Location = new System.Drawing.Point(10, 22);
            this.LabelControl1.Name = "LabelControl1";
            this.LabelControl1.Size = new System.Drawing.Size(11, 13);
            this.LabelControl1.TabIndex = 28;
            this.LabelControl1.Text = "ID";
            // 
            // EditForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(431, 158);
            this.Controls.Add(this.TextEdit_rpps_code);
            this.Controls.Add(this.LabelControl5);
            this.Controls.Add(this.CalcEdit_maximum);
            this.Controls.Add(this.LabelControl4);
            this.Controls.Add(this.TextEdit_description);
            this.Controls.Add(this.LabelControl3);
            this.Controls.Add(this.LabelControl_ID);
            this.Controls.Add(this.LabelControl1);
            this.Name = "EditForm";
            this.Text = "Asset ID";
            this.Controls.SetChildIndex(this.simpleButton_OK, 0);
            this.Controls.SetChildIndex(this.simpleButton_Cancel, 0);
            this.Controls.SetChildIndex(this.LabelControl1, 0);
            this.Controls.SetChildIndex(this.LabelControl_ID, 0);
            this.Controls.SetChildIndex(this.LabelControl3, 0);
            this.Controls.SetChildIndex(this.TextEdit_description, 0);
            this.Controls.SetChildIndex(this.LabelControl4, 0);
            this.Controls.SetChildIndex(this.CalcEdit_maximum, 0);
            this.Controls.SetChildIndex(this.LabelControl5, 0);
            this.Controls.SetChildIndex(this.TextEdit_rpps_code, 0);
            ((System.ComponentModel.ISupportInitialize)(this.DxErrorProvider1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TextEdit_rpps_code.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.CalcEdit_maximum.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TextEdit_description.Properties)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }


        private DevExpress.XtraEditors.TextEdit TextEdit_rpps_code;
        private DevExpress.XtraEditors.LabelControl LabelControl5;
        private DevExpress.XtraEditors.CalcEdit CalcEdit_maximum;
        private DevExpress.XtraEditors.LabelControl LabelControl4;
        private DevExpress.XtraEditors.TextEdit TextEdit_description;
        private DevExpress.XtraEditors.LabelControl LabelControl3;
        private DevExpress.XtraEditors.LabelControl LabelControl_ID;
        private DevExpress.XtraEditors.LabelControl LabelControl1;

    }
}