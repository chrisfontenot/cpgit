#region "Copyright 2000-2012 DebtPlus, L.L.C."
//{*******************************************************************}
//{                                                                   }
//{       DebtPlus Debt Management System                             }
//{                                                                   }
//{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
//{       ALL RIGHTS RESERVED                                         }
//{                                                                   }
//{   The entire contents of this file is protected by U.S. and       }
//{   International Copyright Laws. Unauthorized reproduction,        }
//{   reverse-engineering, and distribution of all or any portion of  }
//{   the code contained in this file is strictly prohibited and may  }
//{   result in severe civil and criminal penalties and will be       }
//{   prosecuted to the maximum extent possible under the law.        }
//{                                                                   }
//{   RESTRICTIONS                                                    }
//{                                                                   }
//{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
//{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
//{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
//{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
//{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
//{                                                                   }
//{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
//{   ADDITIONAL RESTRICTIONS.                                        }
//{                                                                   }
//{*******************************************************************}

#endregion "Copyright 2000-2012 DebtPlus, L.L.C."

using System;
using System.Linq;
using System.Windows.Forms;
using DebtPlus.LINQ;
using DebtPlus.UI.Common;

namespace DebtPlus.UI.TableAdministration.CS.CreditorTypes
{
    public partial class MainForm : Templates.MainForm
    {
        private System.Collections.Generic.List<creditor_type> colRecords = null;
        private BusinessContext bc = null;

        public MainForm()
            : base()
        {
            InitializeComponent();
            RegisterHandlers();
        }

        private void RegisterHandlers()
        {
            Load += MainForm_Load;
        }

        private void UnRegisterHandlers()
        {
            Load -= MainForm_Load;
        }

        /// <summary>
        /// Process the load event on the form
        /// </summary>
        private void MainForm_Load(object sender, EventArgs e)
        {
            UnRegisterHandlers();
            try
            {
                bc = new BusinessContext();
                using (var cm = new CursorManager())
                {
                    colRecords = bc.creditor_types.ToList();
                    gridControl1.DataSource = colRecords;
                    gridView1.BestFitColumns();
                }
            }
            catch (System.Data.SqlClient.SqlException ex)
            {
                DebtPlus.UI.Common.ErrorHandling.HandleErrors(ex);
            }
            finally
            {
                RegisterHandlers();
            }
        }

        /// <summary>
        /// Handle the edit of the information on the form
        /// </summary>
        protected override void UpdateRecord(object obj)
        {
            creditor_type record = obj as creditor_type;
            if (record == null)
            {
                return;
            }

            // Remember the previous ID for the record
            char previousId = record.Id;

            using (var frm = new EditForm(record))
            {
                if (frm.ShowDialog() != DialogResult.OK)
                {
                    return;
                }
            }

            // Look for a change in the record ID
            if (!record.Id.Equals(previousId))
            {
                // Find the previous ID in the database and delete it.
                var q = bc.creditor_types.Where(s => s.Id == previousId).FirstOrDefault();
                if (q != null)
                {
                    bc.creditor_types.DeleteOnSubmit(q);
                }

                // Allocate a new record and copy the fields to it.
                // We don't need to manufacture the record since we are copying all of the fields.
                q = new DebtPlus.LINQ.creditor_type()
                {
                    contribution_pct = record.contribution_pct,
                    cycle = record.cycle,
                    description = record.description,
                    Id = record.Id,
                    nfcc_type = record.nfcc_type,
                    proposals = record.proposals,
                    send_car_letter = record.send_car_letter,
                    send_contribution_letter = record.send_contribution_letter,
                    send_pledge_letter = record.send_pledge_letter,
                    sic_prefix = record.sic_prefix,
                    status = record.status
                };

                // Add the new record to the database
                bc.creditor_types.InsertOnSubmit(q);

                // Discard the previous record from the collection and add the new one.
                colRecords.Remove(record);
                colRecords.Add(q);
            }

            try
            {
                // Do the database updates and refresh the grid control
                bc.SubmitChanges();
                gridView1.RefreshData();
            }

#pragma warning disable 168
            catch (System.Data.ConstraintException ex)
            {
                MessageBox.Show("The ID is already defined and may not be duplicated. Please use a new ID.", "Duplicated Record", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
#pragma warning restore 168
            catch (System.Data.SqlClient.SqlException ex)
            {
                DebtPlus.UI.Common.ErrorHandling.HandleErrors(ex);
            }
        }

        /// <summary>
        /// Delete the indicated record in the system
        /// </summary>
        /// <param name="obj"></param>
        protected override void DeleteRecord(object obj)
        {
            creditor_type record = obj as creditor_type;
            if (record == null)
            {
                return;
            }

            if (DebtPlus.Data.Prompts.RequestConfirmation_Delete() != System.Windows.Forms.DialogResult.Yes)
            {
                return;
            }

            // Delete the record from the database
            bc.creditor_types.DeleteOnSubmit(record);
            bc.SubmitChanges();

            // Remove the record from the collection
            colRecords.Remove(record);
            gridView1.RefreshData();
        }

        /// <summary>
        /// Create the new record for the system
        /// </summary>
        protected override void CreateRecord()
        {
            var record = DebtPlus.LINQ.Factory.Manufacture_creditor_type();
            using (var frm = new EditForm(record))
            {
                if (frm.ShowDialog() != System.Windows.Forms.DialogResult.OK)
                {
                    return;
                }
            }

            // Add the new record
            bc.creditor_types.InsertOnSubmit(record);
            bc.SubmitChanges();

            colRecords.Add(record);
            gridView1.RefreshData();
        }
    }
}