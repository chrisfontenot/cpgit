
using System;
using System.Windows.Forms;
using System.Drawing;
using DebtPlus.LINQ;
using System.Linq;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Diagnostics;
using DebtPlus.UI.Common.Templates;

namespace DebtPlus.UI.TableAdministration.CS.CreditorTypes
{
	partial class EditForm
	{
		//Form overrides dispose to clean up the component list.
		[System.Diagnostics.DebuggerNonUserCode]
		protected override void Dispose(bool disposing)
		{
			try 
            {
				if (disposing)
                {
                    if (components != null) components.Dispose();
				}
                components = null;
            }
    	    finally
            {
				base.Dispose(disposing);
			}
		}

		//Required by the Windows Form Designer
		private System.ComponentModel.IContainer components = null;

		//NOTE: The following procedure is required by the Windows Form Designer
		//It can be modified using the Windows Form Designer.  
		//Do not modify it using the code editor.
		[System.Diagnostics.DebuggerStepThrough]
		private void InitializeComponent()
		{
			this.LabelControl1 = new DevExpress.XtraEditors.LabelControl();
			this.LabelControl2 = new DevExpress.XtraEditors.LabelControl();
			this.TextEdit_creditor_type = new DevExpress.XtraEditors.TextEdit();
			this.TextEdit_description = new DevExpress.XtraEditors.TextEdit();
			this.TextEdit_nfcc_type = new DevExpress.XtraEditors.TextEdit();
			this.LabelControl3 = new DevExpress.XtraEditors.LabelControl();
			this.LookUpEdit_proposals = new DevExpress.XtraEditors.LookUpEdit();
			this.LabelControl4 = new DevExpress.XtraEditors.LabelControl();
			this.LabelControl5 = new DevExpress.XtraEditors.LabelControl();
			this.LookUpEdit_send_pledge_letter = new DevExpress.XtraEditors.LookUpEdit();
			this.LabelControl6 = new DevExpress.XtraEditors.LabelControl();
			this.LookUpEdit_send_contribution_letter = new DevExpress.XtraEditors.LookUpEdit();
			this.LabelControl7 = new DevExpress.XtraEditors.LabelControl();
			this.LookUpEdit_send_car_letter = new DevExpress.XtraEditors.LookUpEdit();
			this.LabelControl8 = new DevExpress.XtraEditors.LabelControl();
			this.PercentEdit_contribution_pct = new global::DebtPlus.Data.Controls.PercentEdit();
			this.LabelControl9 = new DevExpress.XtraEditors.LabelControl();
			this.LookUpEdit_billing_status = new DevExpress.XtraEditors.LookUpEdit();
			((System.ComponentModel.ISupportInitialize)this.DxErrorProvider1).BeginInit();
			((System.ComponentModel.ISupportInitialize)this.TextEdit_creditor_type.Properties).BeginInit();
			((System.ComponentModel.ISupportInitialize)this.TextEdit_description.Properties).BeginInit();
			((System.ComponentModel.ISupportInitialize)this.TextEdit_nfcc_type.Properties).BeginInit();
			((System.ComponentModel.ISupportInitialize)this.LookUpEdit_proposals.Properties).BeginInit();
			((System.ComponentModel.ISupportInitialize)this.LookUpEdit_send_pledge_letter.Properties).BeginInit();
			((System.ComponentModel.ISupportInitialize)this.LookUpEdit_send_contribution_letter.Properties).BeginInit();
			((System.ComponentModel.ISupportInitialize)this.LookUpEdit_send_car_letter.Properties).BeginInit();
			((System.ComponentModel.ISupportInitialize)this.PercentEdit_contribution_pct.Properties).BeginInit();
			((System.ComponentModel.ISupportInitialize)this.LookUpEdit_billing_status.Properties).BeginInit();
			this.SuspendLayout();
			//
			//simpleButton_OK
			//
			this.simpleButton_OK.Enabled = true;
			this.simpleButton_OK.Location = new System.Drawing.Point(314, 21);
			//
			//simpleButton_Cancel
			//
			this.simpleButton_Cancel.Location = new System.Drawing.Point(314, 64);
			//
			//LabelControl1
			//
			this.LabelControl1.Location = new System.Drawing.Point(12, 12);
			this.LabelControl1.Name = "LabelControl1";
			this.LabelControl1.Size = new System.Drawing.Size(66, 13);
			this.LabelControl1.TabIndex = 0;
			this.LabelControl1.Text = "Creditor Type";
			//
			//LabelControl2
			//
			this.LabelControl2.Location = new System.Drawing.Point(12, 39);
			this.LabelControl2.Name = "LabelControl2";
			this.LabelControl2.Size = new System.Drawing.Size(53, 13);
			this.LabelControl2.TabIndex = 2;
			this.LabelControl2.Text = "Description";
			//
			//TextEdit_creditor_type
			//
			this.TextEdit_creditor_type.Location = new System.Drawing.Point(112, 9);
			this.TextEdit_creditor_type.Name = "TextEdit_creditor_type";
			this.TextEdit_creditor_type.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.False;
			this.TextEdit_creditor_type.Properties.CharacterCasing = CharacterCasing.Upper;
			this.TextEdit_creditor_type.Properties.Mask.BeepOnError = true;
			this.TextEdit_creditor_type.Properties.Mask.EditMask = "[A-Z]{1,2}";
			this.TextEdit_creditor_type.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.RegEx;
			this.TextEdit_creditor_type.Properties.MaxLength = 1;
			this.TextEdit_creditor_type.Size = new System.Drawing.Size(37, 20);
			this.TextEdit_creditor_type.TabIndex = 1;
			//
			//TextEdit_description
			//
			this.TextEdit_description.Anchor = (AnchorStyles)((AnchorStyles.Top | AnchorStyles.Left) | AnchorStyles.Right);
			this.TextEdit_description.Location = new System.Drawing.Point(112, 36);
			this.TextEdit_description.Name = "TextEdit_description";
			this.TextEdit_description.Properties.MaxLength = 50;
			this.TextEdit_description.Size = new System.Drawing.Size(192, 20);
			this.TextEdit_description.TabIndex = 3;
			//
			//TextEdit_nfcc_type
			//
			this.TextEdit_nfcc_type.Location = new System.Drawing.Point(112, 62);
			this.TextEdit_nfcc_type.Name = "TextEdit_nfcc_type";
			this.TextEdit_nfcc_type.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.False;
			this.TextEdit_nfcc_type.Properties.CharacterCasing = CharacterCasing.Upper;
			this.TextEdit_nfcc_type.Properties.Mask.BeepOnError = true;
			this.TextEdit_nfcc_type.Properties.Mask.EditMask = "[A-Z]{1,2}";
			this.TextEdit_nfcc_type.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.RegEx;
			this.TextEdit_nfcc_type.Properties.MaxLength = 2;
			this.TextEdit_nfcc_type.Size = new System.Drawing.Size(37, 20);
			this.TextEdit_nfcc_type.TabIndex = 5;
			//
			//LabelControl3
			//
			this.LabelControl3.Location = new System.Drawing.Point(12, 65);
			this.LabelControl3.Name = "LabelControl3";
			this.LabelControl3.Size = new System.Drawing.Size(27, 13);
			this.LabelControl3.TabIndex = 4;
			this.LabelControl3.Text = "NFCC";
			//
			//LookUpEdit_proposals
			//
			this.LookUpEdit_proposals.Anchor = (AnchorStyles)((AnchorStyles.Top | AnchorStyles.Left) | AnchorStyles.Right);
			this.LookUpEdit_proposals.Location = new System.Drawing.Point(112, 89);
			this.LookUpEdit_proposals.Name = "LookUpEdit_proposals";
			this.LookUpEdit_proposals.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] { new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo) });
			this.LookUpEdit_proposals.Properties.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
				new DevExpress.XtraEditors.Controls.LookUpColumnInfo("Id", "ID", 20, DevExpress.Utils.FormatType.Numeric, "f0", false, DevExpress.Utils.HorzAlignment.Default),
				new DevExpress.XtraEditors.Controls.LookUpColumnInfo("description", "Description", 20, DevExpress.Utils.FormatType.None, "", true, DevExpress.Utils.HorzAlignment.Default, DevExpress.Data.ColumnSortOrder.Ascending)
			});
			this.LookUpEdit_proposals.Properties.DisplayMember = "description";
			this.LookUpEdit_proposals.Properties.NullText = "";
			this.LookUpEdit_proposals.Properties.ShowFooter = false;
			this.LookUpEdit_proposals.Properties.ShowHeader = false;
			this.LookUpEdit_proposals.Properties.ValueMember = "Id";
			this.LookUpEdit_proposals.Size = new System.Drawing.Size(192, 20);
			this.LookUpEdit_proposals.TabIndex = 7;
			this.LookUpEdit_proposals.Properties.SortColumnIndex = 1;
			//
			//LabelControl4
			//
			this.LabelControl4.Location = new System.Drawing.Point(12, 92);
			this.LabelControl4.Name = "LabelControl4";
			this.LabelControl4.Size = new System.Drawing.Size(46, 13);
			this.LabelControl4.TabIndex = 6;
			this.LabelControl4.Text = "Proposals";
			//
			//LabelControl5
			//
			this.LabelControl5.Location = new System.Drawing.Point(12, 118);
			this.LabelControl5.Name = "LabelControl5";
			this.LabelControl5.Size = new System.Drawing.Size(64, 13);
			this.LabelControl5.TabIndex = 8;
			this.LabelControl5.Text = "Pledge Letter";
			//
			//LookUpEdit_send_pledge_letter
			//
			this.LookUpEdit_send_pledge_letter.Anchor = (AnchorStyles)((AnchorStyles.Top | AnchorStyles.Left) | AnchorStyles.Right);
			this.LookUpEdit_send_pledge_letter.Location = new System.Drawing.Point(112, 115);
			this.LookUpEdit_send_pledge_letter.Name = "LookUpEdit_send_pledge_letter";
			this.LookUpEdit_send_pledge_letter.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.True;
			this.LookUpEdit_send_pledge_letter.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] { new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo) });
			this.LookUpEdit_send_pledge_letter.Properties.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
				new DevExpress.XtraEditors.Controls.LookUpColumnInfo("Id", "ID", 20, DevExpress.Utils.FormatType.None, "", false, DevExpress.Utils.HorzAlignment.Default),
				new DevExpress.XtraEditors.Controls.LookUpColumnInfo("description", "Description", 20, DevExpress.Utils.FormatType.None, "", true, DevExpress.Utils.HorzAlignment.Default, DevExpress.Data.ColumnSortOrder.Ascending)
			});
			this.LookUpEdit_send_pledge_letter.Properties.DisplayMember = "description";
			this.LookUpEdit_send_pledge_letter.Properties.NullText = "";
			this.LookUpEdit_send_pledge_letter.Properties.ShowFooter = false;
			this.LookUpEdit_send_pledge_letter.Properties.ShowHeader = false;
			this.LookUpEdit_send_pledge_letter.Properties.ValueMember = "Id";
			this.LookUpEdit_send_pledge_letter.Size = new System.Drawing.Size(192, 20);
			this.LookUpEdit_send_pledge_letter.TabIndex = 9;
			this.LookUpEdit_send_pledge_letter.Properties.SortColumnIndex = 1;
			//
			//LabelControl6
			//
			this.LabelControl6.Location = new System.Drawing.Point(12, 144);
			this.LabelControl6.Name = "LabelControl6";
			this.LabelControl6.Size = new System.Drawing.Size(91, 13);
			this.LabelControl6.TabIndex = 10;
			this.LabelControl6.Text = "Contribution Letter";
			//
			//LookUpEdit_send_contribution_letter
			//
			this.LookUpEdit_send_contribution_letter.Anchor = (AnchorStyles)((AnchorStyles.Top | AnchorStyles.Left) | AnchorStyles.Right);
			this.LookUpEdit_send_contribution_letter.Location = new System.Drawing.Point(112, 141);
			this.LookUpEdit_send_contribution_letter.Name = "LookUpEdit_send_contribution_letter";
			this.LookUpEdit_send_contribution_letter.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.True;
			this.LookUpEdit_send_contribution_letter.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] { new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo) });
			this.LookUpEdit_send_contribution_letter.Properties.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
				new DevExpress.XtraEditors.Controls.LookUpColumnInfo("Id", "ID", 20, DevExpress.Utils.FormatType.None, "", false, DevExpress.Utils.HorzAlignment.Default),
				new DevExpress.XtraEditors.Controls.LookUpColumnInfo("description", "Description", 20, DevExpress.Utils.FormatType.None, "", true, DevExpress.Utils.HorzAlignment.Default, DevExpress.Data.ColumnSortOrder.Ascending)
			});
			this.LookUpEdit_send_contribution_letter.Properties.DisplayMember = "description";
			this.LookUpEdit_send_contribution_letter.Properties.NullText = "";
			this.LookUpEdit_send_contribution_letter.Properties.ShowFooter = false;
			this.LookUpEdit_send_contribution_letter.Properties.ShowHeader = false;
			this.LookUpEdit_send_contribution_letter.Properties.ValueMember = "Id";
			this.LookUpEdit_send_contribution_letter.Size = new System.Drawing.Size(192, 20);
			this.LookUpEdit_send_contribution_letter.TabIndex = 11;
			this.LookUpEdit_send_contribution_letter.Properties.SortColumnIndex = 1;
			//
			//LabelControl7
			//
			this.LabelControl7.Location = new System.Drawing.Point(12, 170);
			this.LabelControl7.Name = "LabelControl7";
			this.LabelControl7.Size = new System.Drawing.Size(53, 13);
			this.LabelControl7.TabIndex = 12;
			this.LabelControl7.Text = "CAR Letter";
			//
			//LookUpEdit_send_car_letter
			//
			this.LookUpEdit_send_car_letter.Anchor = (AnchorStyles)((AnchorStyles.Top | AnchorStyles.Left) | AnchorStyles.Right);
			this.LookUpEdit_send_car_letter.Location = new System.Drawing.Point(112, 167);
			this.LookUpEdit_send_car_letter.Name = "LookUpEdit_send_car_letter";
			this.LookUpEdit_send_car_letter.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.True;
			this.LookUpEdit_send_car_letter.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] { new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo) });
			this.LookUpEdit_send_car_letter.Properties.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
				new DevExpress.XtraEditors.Controls.LookUpColumnInfo("Id", "ID", 20, DevExpress.Utils.FormatType.None, "", false, DevExpress.Utils.HorzAlignment.Default),
				new DevExpress.XtraEditors.Controls.LookUpColumnInfo("description", "Description", 20, DevExpress.Utils.FormatType.None, "", true, DevExpress.Utils.HorzAlignment.Default, DevExpress.Data.ColumnSortOrder.Ascending)
			});
			this.LookUpEdit_send_car_letter.Properties.DisplayMember = "description";
			this.LookUpEdit_send_car_letter.Properties.NullText = "";
			this.LookUpEdit_send_car_letter.Properties.ShowFooter = false;
			this.LookUpEdit_send_car_letter.Properties.ShowHeader = false;
			this.LookUpEdit_send_car_letter.Properties.ValueMember = "Id";
			this.LookUpEdit_send_car_letter.Size = new System.Drawing.Size(192, 20);
			this.LookUpEdit_send_car_letter.TabIndex = 13;
			this.LookUpEdit_send_car_letter.Properties.SortColumnIndex = 1;
			//
			//LabelControl8
			//
			this.LabelControl8.Location = new System.Drawing.Point(12, 199);
			this.LabelControl8.Name = "LabelControl8";
			this.LabelControl8.Size = new System.Drawing.Size(63, 13);
			this.LabelControl8.TabIndex = 14;
			this.LabelControl8.Text = "Fairshare Pct";
			//
			//PercentEdit_contribution_pct
			//
			this.PercentEdit_contribution_pct.Location = new System.Drawing.Point(112, 196);
			this.PercentEdit_contribution_pct.Name = "PercentEdit_contribution_pct";
			this.PercentEdit_contribution_pct.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] { new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo) });
			this.PercentEdit_contribution_pct.Size = new System.Drawing.Size(100, 20);
			this.PercentEdit_contribution_pct.TabIndex = 15;
			//
			//LabelControl9
			//
			this.LabelControl9.Location = new System.Drawing.Point(12, 225);
			this.LabelControl9.Name = "LabelControl9";
			this.LabelControl9.Size = new System.Drawing.Size(60, 13);
			this.LabelControl9.TabIndex = 16;
			this.LabelControl9.Text = "Billing Status";
			//
			//LookUpEdit_billing_status
			//
			this.LookUpEdit_billing_status.Anchor = (AnchorStyles)((AnchorStyles.Top | AnchorStyles.Left) | AnchorStyles.Right);
			this.LookUpEdit_billing_status.Location = new System.Drawing.Point(112, 222);
			this.LookUpEdit_billing_status.Name = "LookUpEdit_billing_status";
			this.LookUpEdit_billing_status.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] { new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo) });
			this.LookUpEdit_billing_status.Properties.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
				new DevExpress.XtraEditors.Controls.LookUpColumnInfo("Id", "ID", 20, DevExpress.Utils.FormatType.Numeric, "f0", false, DevExpress.Utils.HorzAlignment.Default),
				new DevExpress.XtraEditors.Controls.LookUpColumnInfo("description", "Description", 20, DevExpress.Utils.FormatType.None, "", true, DevExpress.Utils.HorzAlignment.Default, DevExpress.Data.ColumnSortOrder.Ascending)
			});
			this.LookUpEdit_billing_status.Properties.DisplayMember = "description";
			this.LookUpEdit_billing_status.Properties.NullText = "";
			this.LookUpEdit_billing_status.Properties.ShowFooter = false;
			this.LookUpEdit_billing_status.Properties.ShowHeader = false;
			this.LookUpEdit_billing_status.Size = new System.Drawing.Size(192, 20);
			this.LookUpEdit_billing_status.TabIndex = 17;
			this.LookUpEdit_billing_status.Properties.SortColumnIndex = 1;
			//
			//EditForm
			//
			this.AutoScaleDimensions = new System.Drawing.SizeF(6f, 13f);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(409, 258);
			this.Controls.Add(this.LabelControl9);
			this.Controls.Add(this.LookUpEdit_billing_status);
			this.Controls.Add(this.PercentEdit_contribution_pct);
			this.Controls.Add(this.LabelControl8);
			this.Controls.Add(this.LabelControl7);
			this.Controls.Add(this.LookUpEdit_send_car_letter);
			this.Controls.Add(this.LabelControl6);
			this.Controls.Add(this.LookUpEdit_send_contribution_letter);
			this.Controls.Add(this.LabelControl5);
			this.Controls.Add(this.LookUpEdit_send_pledge_letter);
			this.Controls.Add(this.LabelControl4);
			this.Controls.Add(this.LookUpEdit_proposals);
			this.Controls.Add(this.TextEdit_nfcc_type);
			this.Controls.Add(this.LabelControl3);
			this.Controls.Add(this.TextEdit_creditor_type);
			this.Controls.Add(this.LabelControl1);
			this.Controls.Add(this.LabelControl2);
			this.Controls.Add(this.TextEdit_description);
			this.Name = "EditForm";
			this.Text = "Creditor Type";
			this.Controls.SetChildIndex(this.TextEdit_description, 0);
			this.Controls.SetChildIndex(this.LabelControl2, 0);
			this.Controls.SetChildIndex(this.simpleButton_OK, 0);
			this.Controls.SetChildIndex(this.simpleButton_Cancel, 0);
			this.Controls.SetChildIndex(this.LabelControl1, 0);
			this.Controls.SetChildIndex(this.TextEdit_creditor_type, 0);
			this.Controls.SetChildIndex(this.LabelControl3, 0);
			this.Controls.SetChildIndex(this.TextEdit_nfcc_type, 0);
			this.Controls.SetChildIndex(this.LookUpEdit_proposals, 0);
			this.Controls.SetChildIndex(this.LabelControl4, 0);
			this.Controls.SetChildIndex(this.LookUpEdit_send_pledge_letter, 0);
			this.Controls.SetChildIndex(this.LabelControl5, 0);
			this.Controls.SetChildIndex(this.LookUpEdit_send_contribution_letter, 0);
			this.Controls.SetChildIndex(this.LabelControl6, 0);
			this.Controls.SetChildIndex(this.LookUpEdit_send_car_letter, 0);
			this.Controls.SetChildIndex(this.LabelControl7, 0);
			this.Controls.SetChildIndex(this.LabelControl8, 0);
			this.Controls.SetChildIndex(this.PercentEdit_contribution_pct, 0);
			this.Controls.SetChildIndex(this.LookUpEdit_billing_status, 0);
			this.Controls.SetChildIndex(this.LabelControl9, 0);
			((System.ComponentModel.ISupportInitialize)this.DxErrorProvider1).EndInit();
			((System.ComponentModel.ISupportInitialize)this.TextEdit_creditor_type.Properties).EndInit();
			((System.ComponentModel.ISupportInitialize)this.TextEdit_description.Properties).EndInit();
			((System.ComponentModel.ISupportInitialize)this.TextEdit_nfcc_type.Properties).EndInit();
			((System.ComponentModel.ISupportInitialize)this.LookUpEdit_proposals.Properties).EndInit();
			((System.ComponentModel.ISupportInitialize)this.LookUpEdit_send_pledge_letter.Properties).EndInit();
			((System.ComponentModel.ISupportInitialize)this.LookUpEdit_send_contribution_letter.Properties).EndInit();
			((System.ComponentModel.ISupportInitialize)this.LookUpEdit_send_car_letter.Properties).EndInit();
			((System.ComponentModel.ISupportInitialize)this.PercentEdit_contribution_pct.Properties).EndInit();
			((System.ComponentModel.ISupportInitialize)this.LookUpEdit_billing_status.Properties).EndInit();
			this.ResumeLayout(false);
			this.PerformLayout();

		}
		private DevExpress.XtraEditors.LabelControl LabelControl1;
		private DevExpress.XtraEditors.LabelControl LabelControl2;
		private DevExpress.XtraEditors.TextEdit TextEdit_creditor_type;
		private DevExpress.XtraEditors.TextEdit TextEdit_description;
		private DevExpress.XtraEditors.TextEdit TextEdit_nfcc_type;
		private DevExpress.XtraEditors.LabelControl LabelControl3;
		private DevExpress.XtraEditors.LookUpEdit LookUpEdit_proposals;
		private DevExpress.XtraEditors.LabelControl LabelControl4;
		private DevExpress.XtraEditors.LabelControl LabelControl5;
		private DevExpress.XtraEditors.LookUpEdit LookUpEdit_send_pledge_letter;
		private DevExpress.XtraEditors.LabelControl LabelControl6;
		private DevExpress.XtraEditors.LookUpEdit LookUpEdit_send_contribution_letter;
		private DevExpress.XtraEditors.LabelControl LabelControl7;
		private DevExpress.XtraEditors.LookUpEdit LookUpEdit_send_car_letter;
		private DevExpress.XtraEditors.LabelControl LabelControl8;
		private global::DebtPlus.Data.Controls.PercentEdit PercentEdit_contribution_pct;
		private DevExpress.XtraEditors.LabelControl LabelControl9;
		private DevExpress.XtraEditors.LookUpEdit LookUpEdit_billing_status;
	}
}
