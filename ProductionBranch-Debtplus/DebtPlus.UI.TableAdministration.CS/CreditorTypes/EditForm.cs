#region "Copyright 2000-2012 DebtPlus, L.L.C."
//{*******************************************************************}
//{                                                                   }
//{       DebtPlus Debt Management System                             }
//{                                                                   }
//{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
//{       ALL RIGHTS RESERVED                                         }
//{                                                                   }
//{   The entire contents of this file is protected by U.S. and       }
//{   International Copyright Laws. Unauthorized reproduction,        }
//{   reverse-engineering, and distribution of all or any portion of  }
//{   the code contained in this file is strictly prohibited and may  }
//{   result in severe civil and criminal penalties and will be       }
//{   prosecuted to the maximum extent possible under the law.        }
//{                                                                   }
//{   RESTRICTIONS                                                    }
//{                                                                   }
//{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
//{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
//{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
//{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
//{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
//{                                                                   }
//{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
//{   ADDITIONAL RESTRICTIONS.                                        }
//{                                                                   }
//{*******************************************************************}

#endregion "Copyright 2000-2012 DebtPlus, L.L.C."

using System;
using DebtPlus.LINQ;

namespace DebtPlus.UI.TableAdministration.CS.CreditorTypes
{
    internal partial class EditForm : Templates.EditTemplateForm
    {
        private creditor_type record = null;

        /// <summary>
        /// Initialize the new form class
        /// </summary>
        internal EditForm()
            : base()
        {
            InitializeComponent();
        }

        /// <summary>
        /// Initialize the new form class
        /// </summary>
        /// <param name="record">The creditor_type record to be edited</param>
        internal EditForm(creditor_type record)
            : this()
        {
            this.record = record;
            RegisterHandlers();
        }

        /// <summary>
        /// Register the event handlers
        /// </summary>
        private void RegisterHandlers()
        {
            Load += EditForm_Load;
            TextEdit_creditor_type.EditValueChanged += form_Changed;
            TextEdit_description.EditValueChanged += form_Changed;
            LookUpEdit_send_car_letter.EditValueChanging += DebtPlus.Data.Validation.LookUpEdit_ActiveTest;
            LookUpEdit_send_contribution_letter.EditValueChanging += DebtPlus.Data.Validation.LookUpEdit_ActiveTest;
            LookUpEdit_send_pledge_letter.EditValueChanging += DebtPlus.Data.Validation.LookUpEdit_ActiveTest;
        }

        /// <summary>
        /// Remove the event handler registrations
        /// </summary>
        private void UnRegisterHandlers()
        {
            Load -= EditForm_Load;
            TextEdit_creditor_type.EditValueChanged -= form_Changed;
            TextEdit_description.EditValueChanged -= form_Changed;
            LookUpEdit_send_car_letter.EditValueChanging -= DebtPlus.Data.Validation.LookUpEdit_ActiveTest;
            LookUpEdit_send_contribution_letter.EditValueChanging -= DebtPlus.Data.Validation.LookUpEdit_ActiveTest;
            LookUpEdit_send_pledge_letter.EditValueChanging -= DebtPlus.Data.Validation.LookUpEdit_ActiveTest;
        }

        /// <summary>
        /// Process the form load event
        /// </summary>
        private void EditForm_Load(object sender, EventArgs e)
        {
            UnRegisterHandlers();
            try
            {
                // Bind the controls
                LookUpEdit_proposals.Properties.DataSource = DebtPlus.LINQ.InMemory.ProposalStatusTypes.getList();
                LookUpEdit_send_car_letter.Properties.DataSource = DebtPlus.LINQ.Cache.letter_type.getList();
                LookUpEdit_send_contribution_letter.Properties.DataSource = DebtPlus.LINQ.Cache.letter_type.getList();
                LookUpEdit_send_pledge_letter.Properties.DataSource = DebtPlus.LINQ.Cache.letter_type.getList();
                LookUpEdit_billing_status.Properties.DataSource = DebtPlus.LINQ.InMemory.contribCycleTypes.getList().FindAll(s => s.Id != 'B');  // Ignore the simple billing mode. It is duplicated many times.

                // Find the billing cycle information
                char status = record.status.GetValueOrDefault('N');
                char cycle = record.cycle.GetValueOrDefault(' ');
                LookUpEdit_billing_status.EditValue = DebtPlus.LINQ.InMemory.contribCycleTypes.getList().Find(s => s.Cycle == cycle && s.Status == status);

                // Load the remainder of the items from the record
                TextEdit_creditor_type.EditValue = record.Id;
                LookUpEdit_proposals.EditValue = record.proposals;
                PercentEdit_contribution_pct.EditValue = record.contribution_pct;
                TextEdit_description.EditValue = record.description;
                TextEdit_nfcc_type.EditValue = record.nfcc_type;
                LookUpEdit_send_car_letter.EditValue = record.send_car_letter;
                LookUpEdit_send_contribution_letter.EditValue = record.send_contribution_letter;
                LookUpEdit_send_pledge_letter.EditValue = record.send_pledge_letter;

                simpleButton_OK.Enabled = !HasErrors();
            }
            finally
            {
                RegisterHandlers();
            }
        }

        /// <summary>
        /// Handle a change in the input field
        /// </summary>
        private void form_Changed(object sender, EventArgs e)
        {
            simpleButton_OK.Enabled = !HasErrors();
        }

        /// <summary>
        /// Determine if the input controls will define a valid record
        /// </summary>
        private bool HasErrors()
        {
            if (string.IsNullOrWhiteSpace(TextEdit_description.Text))
            {
                return true;
            }

            if (!System.Char.IsLetter((TextEdit_creditor_type.Text ?? string.Empty + " ")[0]))
            {
                return true;
            }

            return false;
        }

        /// <summary>
        /// Process the OK button click event
        /// </summary>
        protected override void simpleButton_OK_Click(object sender, System.EventArgs e)
        {
            base.simpleButton_OK_Click(sender, e);

            // Correct the billing status and cycle values
            var item = LookUpEdit_billing_status.GetSelectedDataRow() as DebtPlus.LINQ.InMemory.contribCycleType;
            if (item == null)
            {
                record.cycle = 'N';
                record.status = 'M';
            }
            else
            {
                record.cycle = item.Cycle;
                record.status = item.Status;
            }

            // The remainder are taken from the input controls
            record.Id = DebtPlus.Utils.Nulls.v_Char(TextEdit_creditor_type.EditValue).GetValueOrDefault();
            record.proposals = DebtPlus.Utils.Nulls.v_Int32(LookUpEdit_proposals.EditValue);
            record.contribution_pct = DebtPlus.Utils.Nulls.v_Double(PercentEdit_contribution_pct.EditValue);
            record.description = DebtPlus.Utils.Nulls.v_String(TextEdit_description.EditValue);
            record.nfcc_type = DebtPlus.Utils.Nulls.v_String(TextEdit_nfcc_type.EditValue);
            record.send_car_letter = DebtPlus.Utils.Nulls.v_Int32(LookUpEdit_send_car_letter.EditValue);
            record.send_contribution_letter = DebtPlus.Utils.Nulls.v_Int32(LookUpEdit_send_contribution_letter.EditValue);
            record.send_pledge_letter = DebtPlus.Utils.Nulls.v_Int32(LookUpEdit_send_pledge_letter.EditValue);
        }
    }
}