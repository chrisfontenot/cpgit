
using System;
using System.Windows.Forms;
using System.Drawing;
using DebtPlus.LINQ;
using System.Linq;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Diagnostics;
using DebtPlus.UI.Common.Templates;

namespace DebtPlus.UI.TableAdministration.CS.VendorTypes
{
	partial class EditForm
	{
		//Form overrides dispose to clean up the component list.
		[System.Diagnostics.DebuggerNonUserCode]
		protected override void Dispose(bool disposing)
		{
			try 
            {
				if (disposing)
                {
                    if (components != null) components.Dispose();
				}
                components = null;
            }
    	    finally
            {
				base.Dispose(disposing);
			}
		}

		//Required by the Windows Form Designer
		private System.ComponentModel.IContainer components = null;

		//NOTE: The following procedure is required by the Windows Form Designer
		//It can be modified using the Windows Form Designer.  
		//Do not modify it using the code editor.
		[System.Diagnostics.DebuggerStepThrough]
		private void InitializeComponent()
		{
            this.LabelControl1 = new DevExpress.XtraEditors.LabelControl();
            this.LabelControl2 = new DevExpress.XtraEditors.LabelControl();
            this.TextEdit_Id = new DevExpress.XtraEditors.TextEdit();
            this.TextEdit_description = new DevExpress.XtraEditors.TextEdit();
            this.checkEdit_ActiveFlag = new DevExpress.XtraEditors.CheckEdit();
            this.checkEdit_Default = new DevExpress.XtraEditors.CheckEdit();
            ((System.ComponentModel.ISupportInitialize)(this.DxErrorProvider1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TextEdit_Id.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TextEdit_description.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit_ActiveFlag.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit_Default.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // simpleButton_OK
            // 
            this.simpleButton_OK.Enabled = true;
            this.simpleButton_OK.Location = new System.Drawing.Point(314, 21);
            // 
            // simpleButton_Cancel
            // 
            this.simpleButton_Cancel.Location = new System.Drawing.Point(314, 64);
            // 
            // DefaultLookAndFeel1
            // 
            this.DefaultLookAndFeel1.LookAndFeel.SkinName = "McSkin";
            // 
            // LabelControl1
            // 
            this.LabelControl1.Location = new System.Drawing.Point(12, 12);
            this.LabelControl1.Name = "LabelControl1";
            this.LabelControl1.Size = new System.Drawing.Size(66, 13);
            this.LabelControl1.TabIndex = 0;
            this.LabelControl1.Text = "Creditor Type";
            // 
            // LabelControl2
            // 
            this.LabelControl2.Location = new System.Drawing.Point(12, 39);
            this.LabelControl2.Name = "LabelControl2";
            this.LabelControl2.Size = new System.Drawing.Size(53, 13);
            this.LabelControl2.TabIndex = 2;
            this.LabelControl2.Text = "Description";
            // 
            // TextEdit_Id
            // 
            this.TextEdit_Id.Location = new System.Drawing.Point(112, 9);
            this.TextEdit_Id.Name = "TextEdit_Id";
            this.TextEdit_Id.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.False;
            this.TextEdit_Id.Properties.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.TextEdit_Id.Properties.Mask.BeepOnError = true;
            this.TextEdit_Id.Properties.Mask.EditMask = "[A-Z]{1,2}";
            this.TextEdit_Id.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.RegEx;
            this.TextEdit_Id.Properties.MaxLength = 1;
            this.TextEdit_Id.Size = new System.Drawing.Size(37, 20);
            this.TextEdit_Id.TabIndex = 1;
            // 
            // TextEdit_description
            // 
            this.TextEdit_description.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.TextEdit_description.Location = new System.Drawing.Point(112, 36);
            this.TextEdit_description.Name = "TextEdit_description";
            this.TextEdit_description.Properties.MaxLength = 50;
            this.TextEdit_description.Size = new System.Drawing.Size(192, 20);
            this.TextEdit_description.TabIndex = 3;
            // 
            // checkEdit_ActiveFlag
            // 
            this.checkEdit_ActiveFlag.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.checkEdit_ActiveFlag.Location = new System.Drawing.Point(12, 71);
            this.checkEdit_ActiveFlag.Name = "checkEdit_ActiveFlag";
            this.checkEdit_ActiveFlag.Properties.Caption = "Active";
            this.checkEdit_ActiveFlag.Size = new System.Drawing.Size(75, 20);
            this.checkEdit_ActiveFlag.TabIndex = 20;
            // 
            // checkEdit_Default
            // 
            this.checkEdit_Default.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.checkEdit_Default.Location = new System.Drawing.Point(110, 71);
            this.checkEdit_Default.Name = "checkEdit_Default";
            this.checkEdit_Default.Properties.Caption = "Default";
            this.checkEdit_Default.Size = new System.Drawing.Size(75, 20);
            this.checkEdit_Default.TabIndex = 21;
            // 
            // EditForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(409, 103);
            this.Controls.Add(this.checkEdit_Default);
            this.Controls.Add(this.checkEdit_ActiveFlag);
            this.Controls.Add(this.TextEdit_Id);
            this.Controls.Add(this.LabelControl1);
            this.Controls.Add(this.LabelControl2);
            this.Controls.Add(this.TextEdit_description);
            this.Name = "EditForm";
            this.Text = "Creditor Type";
            this.Controls.SetChildIndex(this.TextEdit_description, 0);
            this.Controls.SetChildIndex(this.LabelControl2, 0);
            this.Controls.SetChildIndex(this.simpleButton_OK, 0);
            this.Controls.SetChildIndex(this.simpleButton_Cancel, 0);
            this.Controls.SetChildIndex(this.LabelControl1, 0);
            this.Controls.SetChildIndex(this.TextEdit_Id, 0);
            this.Controls.SetChildIndex(this.checkEdit_ActiveFlag, 0);
            this.Controls.SetChildIndex(this.checkEdit_Default, 0);
            ((System.ComponentModel.ISupportInitialize)(this.DxErrorProvider1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TextEdit_Id.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TextEdit_description.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit_ActiveFlag.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit_Default.Properties)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

		}
		private DevExpress.XtraEditors.LabelControl LabelControl1;
		private DevExpress.XtraEditors.LabelControl LabelControl2;
		private DevExpress.XtraEditors.TextEdit TextEdit_Id;
        private DevExpress.XtraEditors.TextEdit TextEdit_description;
        private DevExpress.XtraEditors.CheckEdit checkEdit_ActiveFlag;
        private DevExpress.XtraEditors.CheckEdit checkEdit_Default;
	}
}
