#region "Copyright 2000-2012 DebtPlus, L.L.C."
//{*******************************************************************}
//{                                                                   }
//{       DebtPlus Debt Management System                             }
//{                                                                   }
//{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
//{       ALL RIGHTS RESERVED                                         }
//{                                                                   }
//{   The entire contents of this file is protected by U.S. and       }
//{   International Copyright Laws. Unauthorized reproduction,        }
//{   reverse-engineering, and distribution of all or any portion of  }
//{   the code contained in this file is strictly prohibited and may  }
//{   result in severe civil and criminal penalties and will be       }
//{   prosecuted to the maximum extent possible under the law.        }
//{                                                                   }
//{   RESTRICTIONS                                                    }
//{                                                                   }
//{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
//{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
//{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
//{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
//{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
//{                                                                   }
//{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
//{   ADDITIONAL RESTRICTIONS.                                        }
//{                                                                   }
//{*******************************************************************}

#endregion "Copyright 2000-2012 DebtPlus, L.L.C."

using System;
using DebtPlus.LINQ;

namespace DebtPlus.UI.TableAdministration.CS.VendorTypes
{
    internal partial class EditForm : Templates.EditTemplateForm
    {
        private vendor_type record = null;

        /// <summary>
        /// Initialize the new form class
        /// </summary>
        internal EditForm()
            : base()
        {
            InitializeComponent();
        }

        /// <summary>
        /// Initialize the new form class
        /// </summary>
        /// <param name="record">The vendor_type record to be edited</param>
        internal EditForm(vendor_type record)
            : this()
        {
            this.record = record;
            RegisterHandlers();
        }

        /// <summary>
        /// Register the event handlers
        /// </summary>
        private void RegisterHandlers()
        {
            Load += EditForm_Load;
            TextEdit_Id.EditValueChanged += form_Changed;
            TextEdit_description.EditValueChanged += form_Changed;
        }

        /// <summary>
        /// Remove the event handler registrations
        /// </summary>
        private void UnRegisterHandlers()
        {
            Load -= EditForm_Load;
            TextEdit_Id.EditValueChanged -= form_Changed;
            TextEdit_description.EditValueChanged -= form_Changed;
        }

        /// <summary>
        /// Process the form load event
        /// </summary>
        private void EditForm_Load(object sender, EventArgs e)
        {
            UnRegisterHandlers();
            try
            {
                // Load the remainder of the items from the record
                TextEdit_Id.EditValue = record.Id;
                TextEdit_description.EditValue = record.description;
                checkEdit_ActiveFlag.Checked = record.ActiveFlag;
                checkEdit_Default.Checked = record.Default;

                simpleButton_OK.Enabled = !HasErrors();
            }
            finally
            {
                RegisterHandlers();
            }
        }

        /// <summary>
        /// Handle a change in the input field
        /// </summary>
        private void form_Changed(object sender, EventArgs e)
        {
            simpleButton_OK.Enabled = !HasErrors();
        }

        /// <summary>
        /// Determine if the input controls will define a valid record
        /// </summary>
        private bool HasErrors()
        {
            if (string.IsNullOrWhiteSpace(TextEdit_description.Text))
            {
                return true;
            }

            if (!System.Char.IsLetter((TextEdit_Id.Text ?? string.Empty + " ")[0]))
            {
                return true;
            }

            return false;
        }

        /// <summary>
        /// Process the OK button click event
        /// </summary>
        protected override void simpleButton_OK_Click(object sender, System.EventArgs e)
        {
            base.simpleButton_OK_Click(sender, e);

            // The remainder are taken from the input controls
            record.Id = DebtPlus.Utils.Nulls.v_String(TextEdit_Id.EditValue) ?? string.Empty;
            record.description = DebtPlus.Utils.Nulls.v_String(TextEdit_description.EditValue);
            record.Default = checkEdit_Default.Checked;
            record.ActiveFlag = checkEdit_ActiveFlag.Checked;
        }
    }
}