﻿using System;
using System.Linq;
using DebtPlus.LINQ;

namespace DebtPlus.UI.TableAdministration.CS.product_resolution_types
{
    public partial class MainForm : Templates.MainForm
    {
        /// <summary>
        /// List of the records to be edited in the program
        /// </summary>
        private System.Collections.Generic.List<product_resolution_type> colRecords;

        private BusinessContext bc = null;

        /// <summary>
        /// Initialize the new class
        /// </summary>
        /// <param name="dc">Pointer to the data context in the mainline</param>
        public MainForm()
            : base()
        {
            InitializeComponent();
            RegisterHandlers();
        }

        private void RegisterHandlers()
        {
            Load += MainForm_Load;
        }

        private void UnRegisterHandlers()
        {
            Load -= MainForm_Load;
        }

        /// <summary>
        /// Process the load event on the form
        /// </summary>
        private void MainForm_Load(object sender, System.EventArgs e)
        {
            using (var cm = new DebtPlus.UI.Common.CursorManager())
            {
                try
                {
                    bc = new BusinessContext();

                    // Retrieve the list of menu items
                    colRecords = bc.product_resolution_types.ToList();
                    gridControl1.DataSource = colRecords;
                }
                catch (Exception ex)
                {
                    DebtPlus.UI.Common.ErrorHandling.HandleErrors(ex, "Error retrieving database information");
                }
            }
        }

        /// <summary>
        /// Create the new record
        /// </summary>
        protected override void CreateRecord()
        {
            // Allocate the new record to be inserted.
            var record = DebtPlus.LINQ.Factory.Manufacture_product_resolution_type();

            // Edit the new record. If OK then attempt to insert the record.
            using (EditForm frm = new EditForm(record))
            {
                if (frm.ShowDialog() != System.Windows.Forms.DialogResult.OK)
                {
                    return;
                }
            }

            try
            {
                bc.product_resolution_types.InsertOnSubmit(record);
                bc.SubmitChanges();

                // Add the record to the main list if the database is updated
                colRecords.Add(record);
                gridControl1.RefreshDataSource();
            }
            catch (System.Data.SqlClient.SqlException ex)
            {
                DebtPlus.UI.Common.ErrorHandling.HandleErrors(ex);
            }
        }

        /// <summary>
        /// Update the existing record
        /// </summary>
        /// <param name="obj">Pointer to the desired record for the operation</param>
        protected override void UpdateRecord(object obj)
        {
            // Find the record
            DebtPlus.LINQ.product_resolution_type record = obj as product_resolution_type;
            if (obj == null)
            {
                return;
            }

            using (EditForm frm = new EditForm(record))
            {
                if (frm.ShowDialog() != System.Windows.Forms.DialogResult.OK)
                {
                    return;
                }

                try
                {
                    bc.SubmitChanges();
                    gridControl1.RefreshDataSource();
                }
                catch (System.Data.SqlClient.SqlException ex)
                {
                    DebtPlus.UI.Common.ErrorHandling.HandleErrors(ex, "Error updating database");
                }
            }
        }

        /// <summary>
        /// Delete the indicated record from the database
        /// </summary>
        /// <param name="obj">Pointer to the desired record for the operation</param>
        protected override void DeleteRecord(object obj)
        {
            // Find the record
            DebtPlus.LINQ.product_resolution_type record = obj as product_resolution_type;
            if (obj == null)
            {
                return;
            }

            if (DebtPlus.Data.Prompts.RequestConfirmation_Delete() == System.Windows.Forms.DialogResult.Yes)
            {
                using (var drm = new BusinessContext())
                {
                    // Find the record and remove it from the database if it is still there
                    var query = (from m in drm.product_resolution_types where m.Id == record.Id select m).SingleOrDefault();
                    if (query != null)
                    {
                        drm.product_resolution_types.DeleteOnSubmit(query);
                        drm.SubmitChanges();
                    }

                    // Remove the record from the display list and redisplay it
                    colRecords.Remove(record);
                    gridControl1.RefreshDataSource();
                }
            }
        }
    }
}