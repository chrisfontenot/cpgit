﻿using System;
using System.Windows.Forms;
using DebtPlus.LINQ;

namespace DebtPlus.UI.TableAdministration.CS.Products
{
    internal partial class Product_StatesGrid : UserControl
    {
        private BusinessContext bc = null;
        private product productRecord = null;

        internal Product_StatesGrid()
            : base()
        {
            InitializeComponent();
            RegisterHandlers();
        }

        /// <summary>
        /// Register the event handlers
        /// </summary>
        private void RegisterHandlers()
        {
            // Generic entries
            barButtonItem_Create.ItemClick += barButtonItem_Create_ItemClick;
            barButtonItem_Update.ItemClick += barButtonItem_Update_ItemClick;
            barButtonItem_Delete.ItemClick += barButtonItem_Delete_ItemClick;
            gridView1.FocusedRowChanged += gridView1_FocusedRowChanged;
            gridView1.DoubleClick += gridView1_DoubleClick;
            gridView1.MouseDown += gridView1_MouseDown;

            // Specific entries
            gridView1.CustomColumnDisplayText += gridView1_CustomColumnDisplayText;
        }

        /// <summary>
        /// Remove the event handler registration
        /// </summary>
        private void UnRegisterHandlers()
        {
            // Generic entries
            barButtonItem_Create.ItemClick -= barButtonItem_Create_ItemClick;
            barButtonItem_Update.ItemClick -= barButtonItem_Update_ItemClick;
            barButtonItem_Delete.ItemClick -= barButtonItem_Delete_ItemClick;
            gridView1.FocusedRowChanged -= gridView1_FocusedRowChanged;
            gridView1.DoubleClick -= gridView1_DoubleClick;
            gridView1.MouseDown -= gridView1_MouseDown;

            // Specific entries
            gridView1.CustomColumnDisplayText -= gridView1_CustomColumnDisplayText;
        }

        /// <summary>
        ///     The focused row changed. Enable the Edit button as needed.
        /// </summary>
        private void gridView1_FocusedRowChanged(object sender, DevExpress.XtraGrid.Views.Base.FocusedRowChangedEventArgs e)
        {
        }

        /// <summary>
        /// Double click event on the list
        /// </summary>
        private void gridView1_MouseDown(object sender, MouseEventArgs e)
        {
            // Find the record to be edited from the list
            if (e.Button == System.Windows.Forms.MouseButtons.Right)
            {
                DevExpress.XtraGrid.Views.Grid.ViewInfo.GridHitInfo hi = gridView1.CalcHitInfo((gridControl1.PointToClient(System.Windows.Forms.Control.MousePosition)));
                System.Int32 RowHandle = hi.RowHandle;
                gridView1.FocusedRowHandle = RowHandle;

                // Find the record for this row
                object obj = null;
                if (RowHandle >= 0)
                {
                    obj = gridView1.GetRow(RowHandle);
                }

                // If the row is defined then edit the row.
                if (obj != null)
                {
                    barButtonItem_Update.Enabled = true;
                    barButtonItem_Delete.Enabled = true;
                }
                else
                {
                    barButtonItem_Update.Enabled = false;
                    barButtonItem_Delete.Enabled = false;
                }

                popupMenu1.ShowPopup(System.Windows.Forms.Control.MousePosition);
            }
        }

        /// <summary>
        /// Double click event on the list
        /// </summary>
        private void gridView1_DoubleClick(object sender, System.EventArgs e)
        {
            DevExpress.XtraGrid.Views.Grid.ViewInfo.GridHitInfo hi = gridView1.CalcHitInfo((gridControl1.PointToClient(System.Windows.Forms.Control.MousePosition)));
            System.Int32 RowHandle = hi.RowHandle;

            // Find the record to be edited from the list
            if (RowHandle >= 0)
            {
                gridView1.FocusedRowHandle = RowHandle;
                Object obj = gridView1.GetRow(RowHandle);
                if (obj != null)
                {
                    UpdateRecord(obj);
                }
            }
        }

        /// <summary>
        /// Edit the current record
        /// </summary>
        private void SimpleButton_Edit_Click(System.Object sender, System.EventArgs e)
        {
            System.Int32 RowHandle = gridView1.FocusedRowHandle;

            // Find the row that is being edited
            Object obj = gridView1.GetRow(RowHandle);
            if (obj != null)
            {
                UpdateRecord(obj);
            }
        }

        /// <summary>
        /// Edit -> Create menu item clicked
        /// </summary>
        private void barButtonItem_Create_ItemClick(System.Object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            CreateRecord();
        }

        /// <summary>
        /// Edit -> Edit menu item clicked
        /// </summary>
        private void barButtonItem_Update_ItemClick(System.Object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            System.Int32 RowHandle = gridView1.FocusedRowHandle;

            if (RowHandle >= 0)
            {
                gridView1.FocusedRowHandle = RowHandle;
                object obj = gridView1.GetRow(RowHandle);
                if (obj != null)
                {
                    UpdateRecord(obj);
                }
            }
        }

        /// <summary>
        /// Edit -> Delete menu item clicked
        /// </summary>
        private void barButtonItem_Delete_ItemClick(System.Object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            System.Int32 RowHandle = gridView1.FocusedRowHandle;

            if (RowHandle >= 0)
            {
                gridView1.FocusedRowHandle = RowHandle;
                object obj = gridView1.GetRow(RowHandle);
                if (obj != null)
                {
                    DeleteRecord(obj);
                }
            }
        }

        // Routines to process the CUD operations
        protected virtual void CreateRecord()
        {
            var record = DebtPlus.LINQ.Factory.Manufacture_product_state();
            using (var frm = new ProductStatesEditForm(productRecord, record))
            {
                if (frm.ShowDialog() != DialogResult.OK)
                {
                    return;
                }
            }

            // Add the record to the product
            productRecord.product_states.Add(record);

            // Refresh the list
            gridControl1.DataSource = productRecord.product_states.GetNewBindingList();
            gridView1.RefreshData();
        }

        protected virtual void UpdateRecord(object obj)
        {
            var record = obj as product_state;
            if (record == null)
            {
                return;
            }

            using (var frm = new ProductStatesEditForm(productRecord, record))
            {
                if (frm.ShowDialog() != DialogResult.OK)
                {
                    return;
                }
            }

            // Refresh the list
            gridControl1.DataSource = productRecord.product_states.GetNewBindingList();
            gridView1.RefreshData();
        }

        protected virtual void DeleteRecord(object obj)
        {
            var record = obj as product_state;
            if (record == null)
            {
                return;
            }

            // Remove the record to the product
            productRecord.product_states.Remove(record);

            // Refresh the list
            gridControl1.DataSource = productRecord.product_states.GetNewBindingList();
            gridView1.RefreshData();
        }

        /// <summary>
        /// Read the list of state overrides to the current product record.
        /// </summary>
        /// <param name="bc"></param>
        /// <param name="productRecord"></param>
        internal void ReadForm(BusinessContext bc, product ProductRecord)
        {
            this.bc = bc;
            this.productRecord = ProductRecord;

            // Load the grid with the list of state overrides and refresh the grid
            gridControl1.DataSource = productRecord.product_states;
            gridControl1.RefreshDataSource();
            gridView1.RefreshData();
        }

        /// <summary>
        /// Complete the edit of the product record and save the changes to the database.
        /// </summary>
        internal void SaveForm(BusinessContext bc, product ProductRecord)
        {
        }

        /// <summary>
        /// Display custom data in the grid
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void gridView1_CustomColumnDisplayText(object sender, DevExpress.XtraGrid.Views.Base.CustomColumnDisplayTextEventArgs e)
        {
            var record = gridView1.GetRow(e.RowHandle) as product_state;
            if (record == null)
            {
                return;
            }

            // Determine the column being displayed. Handlethe state name
            if (e.Column == gridColumn_State)
            {
                var stateID = record.stateID;
                if (stateID > 0)
                {
                    var q = DebtPlus.LINQ.Cache.state.getList().Find(s => s.Id == stateID);
                    if (q != null)
                    {
                        e.DisplayText = q.Name;
                        return;
                    }
                }
                e.DisplayText = string.Empty;
            }
        }
    }
}