﻿namespace DebtPlus.UI.TableAdministration.CS.Products
{
    partial class Product_StatesGrid
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            try
            {
                if (disposing)
                {
                    if (components != null) components.Dispose();
                }
                components = null;
                bc = null;
            }
            finally
            {
                base.Dispose(disposing);
            }
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.gridControl1 = new DevExpress.XtraGrid.GridControl();
            this.gridView1 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gridColumn_State = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn_Amount = new DevExpress.XtraGrid.Columns.GridColumn();
            this.barManager1 = new DevExpress.XtraBars.BarManager(this.components);
            this.barDockControlTop = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlBottom = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlLeft = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlRight = new DevExpress.XtraBars.BarDockControl();
            this.barButtonItem_Create = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItem_Update = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItem_Delete = new DevExpress.XtraBars.BarButtonItem();
            this.popupMenu1 = new DevExpress.XtraBars.PopupMenu(this.components);
            ((System.ComponentModel.ISupportInitialize)(this.gridControl1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.popupMenu1)).BeginInit();
            this.SuspendLayout();
            // 
            // gridControl1
            // 
            this.gridControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridControl1.Location = new System.Drawing.Point(0, 0);
            this.gridControl1.MainView = this.gridView1;
            this.gridControl1.Name = "gridControl1";
            this.gridControl1.Size = new System.Drawing.Size(405, 150);
            this.gridControl1.TabIndex = 0;
            this.gridControl1.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView1});
            // 
            // gridView1
            // 
            this.gridView1.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gridColumn_State,
            this.gridColumn_Amount});
            this.gridView1.GridControl = this.gridControl1;
            this.gridView1.Name = "gridView1";
            this.gridView1.OptionsBehavior.Editable = false;
            this.gridView1.OptionsView.ShowGroupPanel = false;
            this.gridView1.OptionsView.ShowIndicator = false;
            this.gridView1.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.gridColumn_State, DevExpress.Data.ColumnSortOrder.Ascending)});
            // 
            // gridColumn_State
            // 
            this.gridColumn_State.Caption = "State";
            this.gridColumn_State.FieldName = "gridColumn_State";
            this.gridColumn_State.Name = "gridColumn_State";
            this.gridColumn_State.SortMode = DevExpress.XtraGrid.ColumnSortMode.DisplayText;
            this.gridColumn_State.UnboundType = DevExpress.Data.UnboundColumnType.String;
            this.gridColumn_State.Visible = true;
            this.gridColumn_State.VisibleIndex = 0;
            this.gridColumn_State.Width = 452;
            // 
            // gridColumn_Amount
            // 
            this.gridColumn_Amount.AppearanceCell.Options.UseTextOptions = true;
            this.gridColumn_Amount.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.gridColumn_Amount.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColumn_Amount.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.gridColumn_Amount.Caption = "Amount";
            this.gridColumn_Amount.DisplayFormat.FormatString = "c";
            this.gridColumn_Amount.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.gridColumn_Amount.FieldName = "Amount";
            this.gridColumn_Amount.GroupFormat.FormatString = "c";
            this.gridColumn_Amount.GroupFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.gridColumn_Amount.Name = "gridColumn_Amount";
            this.gridColumn_Amount.SortMode = DevExpress.XtraGrid.ColumnSortMode.Value;
            this.gridColumn_Amount.Visible = true;
            this.gridColumn_Amount.VisibleIndex = 1;
            this.gridColumn_Amount.Width = 165;
            // 
            // barManager1
            // 
            this.barManager1.DockControls.Add(this.barDockControlTop);
            this.barManager1.DockControls.Add(this.barDockControlBottom);
            this.barManager1.DockControls.Add(this.barDockControlLeft);
            this.barManager1.DockControls.Add(this.barDockControlRight);
            this.barManager1.Form = this;
            this.barManager1.Items.AddRange(new DevExpress.XtraBars.BarItem[] {
            this.barButtonItem_Create,
            this.barButtonItem_Update,
            this.barButtonItem_Delete});
            this.barManager1.MaxItemId = 7;
            // 
            // barDockControlTop
            // 
            this.barDockControlTop.CausesValidation = false;
            this.barDockControlTop.Dock = System.Windows.Forms.DockStyle.Top;
            this.barDockControlTop.Location = new System.Drawing.Point(0, 0);
            this.barDockControlTop.Size = new System.Drawing.Size(405, 0);
            // 
            // barDockControlBottom
            // 
            this.barDockControlBottom.CausesValidation = false;
            this.barDockControlBottom.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.barDockControlBottom.Location = new System.Drawing.Point(0, 150);
            this.barDockControlBottom.Size = new System.Drawing.Size(405, 0);
            // 
            // barDockControlLeft
            // 
            this.barDockControlLeft.CausesValidation = false;
            this.barDockControlLeft.Dock = System.Windows.Forms.DockStyle.Left;
            this.barDockControlLeft.Location = new System.Drawing.Point(0, 0);
            this.barDockControlLeft.Size = new System.Drawing.Size(0, 150);
            // 
            // barDockControlRight
            // 
            this.barDockControlRight.CausesValidation = false;
            this.barDockControlRight.Dock = System.Windows.Forms.DockStyle.Right;
            this.barDockControlRight.Location = new System.Drawing.Point(405, 0);
            this.barDockControlRight.Size = new System.Drawing.Size(0, 150);
            // 
            // barButtonItem_Create
            // 
            this.barButtonItem_Create.Caption = "&Add...";
            this.barButtonItem_Create.Id = 4;
            this.barButtonItem_Create.Name = "barButtonItem_Create";
            // 
            // barButtonItem_Update
            // 
            this.barButtonItem_Update.Caption = "&Change...";
            this.barButtonItem_Update.Id = 5;
            this.barButtonItem_Update.Name = "barButtonItem_Update";
            // 
            // barButtonItem_Delete
            // 
            this.barButtonItem_Delete.Caption = "&Delete...";
            this.barButtonItem_Delete.Id = 6;
            this.barButtonItem_Delete.Name = "barButtonItem_Delete";
            // 
            // popupMenu1
            // 
            this.popupMenu1.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.barButtonItem_Create),
            new DevExpress.XtraBars.LinkPersistInfo(this.barButtonItem_Update),
            new DevExpress.XtraBars.LinkPersistInfo(this.barButtonItem_Delete)});
            this.popupMenu1.Manager = this.barManager1;
            this.popupMenu1.Name = "popupMenu1";
            // 
            // Product_StatesGrid
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.gridControl1);
            this.Controls.Add(this.barDockControlLeft);
            this.Controls.Add(this.barDockControlRight);
            this.Controls.Add(this.barDockControlBottom);
            this.Controls.Add(this.barDockControlTop);
            this.Name = "Product_StatesGrid";
            this.Size = new System.Drawing.Size(405, 150);
            ((System.ComponentModel.ISupportInitialize)(this.gridControl1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.popupMenu1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        protected DevExpress.XtraBars.BarManager barManager1;
        protected DevExpress.XtraBars.BarDockControl barDockControlTop;
        protected DevExpress.XtraBars.BarDockControl barDockControlBottom;
        protected DevExpress.XtraBars.BarDockControl barDockControlLeft;
        protected DevExpress.XtraBars.BarDockControl barDockControlRight;
        protected DevExpress.XtraBars.BarButtonItem barButtonItem_Create;
        protected DevExpress.XtraBars.BarButtonItem barButtonItem_Update;
        protected DevExpress.XtraBars.BarButtonItem barButtonItem_Delete;
        protected DevExpress.XtraBars.PopupMenu popupMenu1;
        private DevExpress.XtraGrid.GridControl gridControl1;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView1;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn_State;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn_Amount;
    }
}
