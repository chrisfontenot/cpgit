#region "Copyright 2000-2012 DebtPlus, L.L.C."
//{*******************************************************************}
//{                                                                   }
//{       DebtPlus Debt Management System                             }
//{                                                                   }
//{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
//{       ALL RIGHTS RESERVED                                         }
//{                                                                   }
//{   The entire contents of this file is protected by U.S. and       }
//{   International Copyright Laws. Unauthorized reproduction,        }
//{   reverse-engineering, and distribution of all or any portion of  }
//{   the code contained in this file is strictly prohibited and may  }
//{   result in severe civil and criminal penalties and will be       }
//{   prosecuted to the maximum extent possible under the law.        }
//{                                                                   }
//{   RESTRICTIONS                                                    }
//{                                                                   }
//{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
//{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
//{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
//{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
//{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
//{                                                                   }
//{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
//{   ADDITIONAL RESTRICTIONS.                                        }
//{                                                                   }
//{*******************************************************************}

#endregion "Copyright 2000-2012 DebtPlus, L.L.C."

using System;
using System.Linq;
using DebtPlus.LINQ;

namespace DebtPlus.UI.TableAdministration.CS.Products
{
    internal partial class ProductStatesEditForm : Templates.EditTemplateForm
    {
        private product productRecord = null;
        private product_state record = null;

        internal ProductStatesEditForm()
            : base()
        {
            InitializeComponent();
        }

        internal ProductStatesEditForm(product ProductRecord, product_state Record)
            : this()
        {
            this.record = Record;
            this.productRecord = ProductRecord;
            RegisterHandlers();
        }

        private void RegisterHandlers()
        {
            Load += ProductStatesEditForm_Load;
            lookUpEdit_State.EditValueChanged += form_changed;
            lookUpEdit_State.EditValueChanging += DebtPlus.Data.Validation.LookUpEdit_ActiveTest;
        }

        private void UnRegisterHandlers()
        {
            Load -= ProductStatesEditForm_Load;
            lookUpEdit_State.EditValueChanged -= form_changed;
            lookUpEdit_State.EditValueChanging -= DebtPlus.Data.Validation.LookUpEdit_ActiveTest;
        }

        private void ProductStatesEditForm_Load(object sender, EventArgs e)
        {
            UnRegisterHandlers();
            try
            {
                if (record.Id > 0)
                {
                    // If the operation is an "edit" then allow all of the states but block the value from being changed
                    lookUpEdit_State.Properties.ReadOnly = true;
                    lookUpEdit_State.Properties.DataSource = DebtPlus.LINQ.Cache.state.getList().FindAll(s => s.Id > 0);
                }
                else
                {
                    // If the operation is a create then show only the states that are not currently used but allow the edit operation.
                    Int32[] stateList = productRecord.product_states.AsQueryable<product_state>().Select(s => s.stateID).ToArray();
                    var colStates = DebtPlus.LINQ.Cache.state.getList().Where(s => !stateList.Contains(s.Id) && s.Id > 0).ToList();
                    lookUpEdit_State.Properties.DataSource = colStates;
                    lookUpEdit_State.Properties.ReadOnly = false;
                }

                // Load the record into the controls
                labelControl_Id.Text = record.Id < 1 ? "NEW" : record.Id.ToString();
                calcEdit_Amuont.EditValue = record.Amount;
                lookUpEdit_State.EditValue = record.stateID;

                simpleButton_OK.Enabled = !HasErrors();
            }
            finally
            {
                RegisterHandlers();
            }
        }

        private void form_changed(object sender, EventArgs e)
        {
            simpleButton_OK.Enabled = !HasErrors();
        }

        private bool HasErrors()
        {
            // The state is required.
            return !DebtPlus.Utils.Nulls.v_Int32(lookUpEdit_State.EditValue).HasValue;
        }

        protected override void simpleButton_OK_Click(object sender, EventArgs e)
        {
            base.simpleButton_OK_Click(sender, e);

            record.stateID = DebtPlus.Utils.Nulls.v_Int32(lookUpEdit_State.EditValue).GetValueOrDefault();
            record.Amount = DebtPlus.Utils.Nulls.v_Decimal(calcEdit_Amuont.EditValue).GetValueOrDefault();
        }
    }
}