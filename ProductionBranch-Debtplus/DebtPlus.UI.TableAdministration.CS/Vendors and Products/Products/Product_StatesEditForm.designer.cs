using System;
using System.Windows.Forms;
using System.Drawing;
using DebtPlus.LINQ;
using System.Linq;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Diagnostics;
using DebtPlus.UI.Common.Templates;

namespace DebtPlus.UI.TableAdministration.CS.Products
{
    partial class ProductStatesEditForm
	{
		//Form overrides dispose to clean up the component list.
		[System.Diagnostics.DebuggerNonUserCode]
		protected override void Dispose(bool disposing)
		{
			try 
            {
				if (disposing)
                {
                    if (components != null) components.Dispose();
				}
                components = null;
            }
    	    finally
            {
				base.Dispose(disposing);
			}
		}

		//Required by the Windows Form Designer
		private System.ComponentModel.IContainer components = null;

		//NOTE: The following procedure is required by the Windows Form Designer
		//It can be modified using the Windows Form Designer.  
		//Do not modify it using the code editor.
		[System.Diagnostics.DebuggerStepThrough]
		private void InitializeComponent()
		{
            this.LabelControl1 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl_Id = new DevExpress.XtraEditors.LabelControl();
            this.lookUpEdit_State = new DevExpress.XtraEditors.LookUpEdit();
            this.labelControl3 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl2 = new DevExpress.XtraEditors.LabelControl();
            this.calcEdit_Amuont = new DevExpress.XtraEditors.CalcEdit();
            ((System.ComponentModel.ISupportInitialize)(this.DxErrorProvider1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lookUpEdit_State.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.calcEdit_Amuont.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // simpleButton_OK
            // 
            this.simpleButton_OK.Location = new System.Drawing.Point(275, 13);
            this.simpleButton_OK.TabIndex = 9;
            // 
            // simpleButton_Cancel
            // 
            this.simpleButton_Cancel.Location = new System.Drawing.Point(275, 56);
            this.simpleButton_Cancel.TabIndex = 10;
            // 
            // DefaultLookAndFeel1
            // 
            this.DefaultLookAndFeel1.LookAndFeel.SkinName = "McSkin";
            // 
            // LabelControl1
            // 
            this.LabelControl1.Location = new System.Drawing.Point(13, 9);
            this.LabelControl1.Name = "LabelControl1";
            this.LabelControl1.Size = new System.Drawing.Size(11, 13);
            this.LabelControl1.TabIndex = 0;
            this.LabelControl1.Text = "ID";
            // 
            // labelControl_Id
            // 
            this.labelControl_Id.Location = new System.Drawing.Point(86, 9);
            this.labelControl_Id.Name = "labelControl_Id";
            this.labelControl_Id.Size = new System.Drawing.Size(23, 13);
            this.labelControl_Id.TabIndex = 11;
            this.labelControl_Id.Text = "NEW";
            // 
            // lookUpEdit_State
            // 
            this.lookUpEdit_State.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.lookUpEdit_State.Location = new System.Drawing.Point(86, 28);
            this.lookUpEdit_State.Name = "lookUpEdit_State";
            this.lookUpEdit_State.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.lookUpEdit_State.Properties.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("Id", "Id", 20, DevExpress.Utils.FormatType.Numeric, "f0", false, DevExpress.Utils.HorzAlignment.Far, DevExpress.Data.ColumnSortOrder.None, DevExpress.Utils.DefaultBoolean.True),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("Name", "Description", 20, DevExpress.Utils.FormatType.None, "", true, DevExpress.Utils.HorzAlignment.Near, DevExpress.Data.ColumnSortOrder.None, DevExpress.Utils.DefaultBoolean.True)});
            this.lookUpEdit_State.Properties.DisplayMember = "Name";
            this.lookUpEdit_State.Properties.NullText = "";
            this.lookUpEdit_State.Properties.ShowFooter = false;
            this.lookUpEdit_State.Properties.ShowHeader = false;
            this.lookUpEdit_State.Properties.ValueMember = "Id";
            this.lookUpEdit_State.Size = new System.Drawing.Size(175, 20);
            this.lookUpEdit_State.TabIndex = 15;
            // 
            // labelControl3
            // 
            this.labelControl3.Location = new System.Drawing.Point(12, 31);
            this.labelControl3.Name = "labelControl3";
            this.labelControl3.Size = new System.Drawing.Size(26, 13);
            this.labelControl3.TabIndex = 17;
            this.labelControl3.Text = "State";
            // 
            // labelControl2
            // 
            this.labelControl2.Location = new System.Drawing.Point(12, 57);
            this.labelControl2.Name = "labelControl2";
            this.labelControl2.Size = new System.Drawing.Size(37, 13);
            this.labelControl2.TabIndex = 16;
            this.labelControl2.Text = "Amount";
            // 
            // calcEdit_Amuont
            // 
            this.calcEdit_Amuont.Location = new System.Drawing.Point(86, 54);
            this.calcEdit_Amuont.Name = "calcEdit_Amuont";
            this.calcEdit_Amuont.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.calcEdit_Amuont.Properties.DisplayFormat.FormatString = "c";
            this.calcEdit_Amuont.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.calcEdit_Amuont.Properties.EditFormat.FormatString = "f2";
            this.calcEdit_Amuont.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.calcEdit_Amuont.Properties.Precision = 2;
            this.calcEdit_Amuont.Size = new System.Drawing.Size(100, 20);
            this.calcEdit_Amuont.TabIndex = 12;
            // 
            // ProductStatesEditForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(371, 92);
            this.Controls.Add(this.labelControl3);
            this.Controls.Add(this.labelControl2);
            this.Controls.Add(this.lookUpEdit_State);
            this.Controls.Add(this.calcEdit_Amuont);
            this.Controls.Add(this.labelControl_Id);
            this.Controls.Add(this.LabelControl1);
            this.Name = "ProductStatesEditForm";
            this.Text = "Product and State Information";
            this.Controls.SetChildIndex(this.LabelControl1, 0);
            this.Controls.SetChildIndex(this.labelControl_Id, 0);
            this.Controls.SetChildIndex(this.calcEdit_Amuont, 0);
            this.Controls.SetChildIndex(this.lookUpEdit_State, 0);
            this.Controls.SetChildIndex(this.labelControl2, 0);
            this.Controls.SetChildIndex(this.labelControl3, 0);
            this.Controls.SetChildIndex(this.simpleButton_OK, 0);
            this.Controls.SetChildIndex(this.simpleButton_Cancel, 0);
            ((System.ComponentModel.ISupportInitialize)(this.DxErrorProvider1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lookUpEdit_State.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.calcEdit_Amuont.Properties)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

		}

        private DevExpress.XtraEditors.LabelControl LabelControl1;
        private DevExpress.XtraEditors.LabelControl labelControl_Id;
        private DevExpress.XtraEditors.LookUpEdit lookUpEdit_State;
        private DevExpress.XtraEditors.LabelControl labelControl3;
        private DevExpress.XtraEditors.LabelControl labelControl2;
        private DevExpress.XtraEditors.CalcEdit calcEdit_Amuont;
	}
}

