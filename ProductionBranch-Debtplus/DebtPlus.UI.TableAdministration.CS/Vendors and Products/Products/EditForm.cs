#region "Copyright 2000-2012 DebtPlus, L.L.C."
//{*******************************************************************}
//{                                                                   }
//{       DebtPlus Debt Management System                             }
//{                                                                   }
//{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
//{       ALL RIGHTS RESERVED                                         }
//{                                                                   }
//{   The entire contents of this file is protected by U.S. and       }
//{   International Copyright Laws. Unauthorized reproduction,        }
//{   reverse-engineering, and distribution of all or any portion of  }
//{   the code contained in this file is strictly prohibited and may  }
//{   result in severe civil and criminal penalties and will be       }
//{   prosecuted to the maximum extent possible under the law.        }
//{                                                                   }
//{   RESTRICTIONS                                                    }
//{                                                                   }
//{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
//{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
//{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
//{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
//{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
//{                                                                   }
//{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
//{   ADDITIONAL RESTRICTIONS.                                        }
//{                                                                   }
//{*******************************************************************}

#endregion "Copyright 2000-2012 DebtPlus, L.L.C."

using System;
using DebtPlus.LINQ;

namespace DebtPlus.UI.TableAdministration.CS.Products
{
    internal partial class EditForm : Templates.EditTemplateForm
    {
        private product record = null;
        private BusinessContext bc = null;

        internal EditForm()
            : base()
        {
            InitializeComponent();
        }

        internal EditForm(BusinessContext Bc, product record)
            : this()
        {
            this.bc = Bc;
            this.record = record;
            RegisterHandlers();
        }

        private void RegisterHandlers()
        {
            Load += EditForm_Load;
            lookUpEdit_InvoiceEmailTemplate.EditValueChanging += DebtPlus.Data.Validation.LookUpEdit_ActiveTest;
            calcEdit_Amount.Validating += DebtPlus.Data.Validation.NonNegative;
            lookUpEdit_InvoiceEmailTemplate.EditValueChanged += form_Changed;
        }

        private void UnRegisterHandlers()
        {
            Load -= EditForm_Load;
            lookUpEdit_InvoiceEmailTemplate.EditValueChanging -= DebtPlus.Data.Validation.LookUpEdit_ActiveTest;
            lookUpEdit_InvoiceEmailTemplate.EditValueChanged -= form_Changed;
        }

        private void EditForm_Load(object sender, EventArgs e)
        {
            UnRegisterHandlers();
            try
            {
                // Populate the lookupedit control lists
                lookUpEdit_InvoiceEmailTemplate.Properties.DataSource = DebtPlus.LINQ.Cache.email_template.getList();

                // Set the record values into the controls
                labelControl_Id.Text = record.Id < 1 ? "NEW" : record.Id.ToString();
                TextEdit_description.EditValue = record.description;
                lookUpEdit_InvoiceEmailTemplate.EditValue = record.InvoiceEmailTemplateID;
                calcEdit_Amount.EditValue = record.Amount;
                checkEdit_ActiveFlag.Checked = record.ActiveFlag;
                checkEdit_Default.Checked = record.Default;

                // Load the list of state references
                product_States1.ReadForm(bc, record);

                simpleButton_OK.Enabled = !HasErrors();
            }
            finally
            {
                RegisterHandlers();
            }
        }

        private void form_Changed(object sender, EventArgs e)
        {
            simpleButton_OK.Enabled = !HasErrors();
        }

        private bool HasErrors()
        {
            // A description is required.
            return string.IsNullOrWhiteSpace(TextEdit_description.Text);
        }

        protected override void simpleButton_OK_Click(object sender, EventArgs e)
        {
            base.simpleButton_OK_Click(sender, e);

            // Save the list of state references
            product_States1.SaveForm(bc, record);

            // Save the record settings from the controls
            record.description = DebtPlus.Utils.Nulls.v_String(TextEdit_description.EditValue).Trim();
            record.InvoiceEmailTemplateID = DebtPlus.Utils.Nulls.v_Int32(lookUpEdit_InvoiceEmailTemplate.EditValue);
            record.Amount = DebtPlus.Utils.Nulls.v_Decimal(calcEdit_Amount.EditValue).GetValueOrDefault();
            record.ActiveFlag = checkEdit_ActiveFlag.Checked;
            record.Default = checkEdit_Default.Checked;
        }
    }
}