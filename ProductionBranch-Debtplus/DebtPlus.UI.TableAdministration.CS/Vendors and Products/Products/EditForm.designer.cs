using System;
using System.Windows.Forms;
using System.Drawing;
using DebtPlus.LINQ;
using System.Linq;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Diagnostics;
using DebtPlus.UI.Common.Templates;

namespace DebtPlus.UI.TableAdministration.CS.Products
{
	partial class EditForm
	{
		//Form overrides dispose to clean up the component list.
		[System.Diagnostics.DebuggerNonUserCode]
		protected override void Dispose(bool disposing)
		{
			try 
            {
				if (disposing)
                {
                    if (components != null) components.Dispose();
				}
                components = null;
            }
    	    finally
            {
				base.Dispose(disposing);
			}
		}

		//Required by the Windows Form Designer
		private System.ComponentModel.IContainer components = null;

		//NOTE: The following procedure is required by the Windows Form Designer
		//It can be modified using the Windows Form Designer.  
		//Do not modify it using the code editor.
		[System.Diagnostics.DebuggerStepThrough]
		private void InitializeComponent()
		{
            this.LabelControl1 = new DevExpress.XtraEditors.LabelControl();
            this.LabelControl_description = new DevExpress.XtraEditors.LabelControl();
            this.TextEdit_description = new DevExpress.XtraEditors.TextEdit();
            this.labelControl_Id = new DevExpress.XtraEditors.LabelControl();
            this.calcEdit_Amount = new DevExpress.XtraEditors.CalcEdit();
            this.checkEdit_ActiveFlag = new DevExpress.XtraEditors.CheckEdit();
            this.checkEdit_Default = new DevExpress.XtraEditors.CheckEdit();
            this.lookUpEdit_InvoiceEmailTemplate = new DevExpress.XtraEditors.LookUpEdit();
            this.labelControl2 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl3 = new DevExpress.XtraEditors.LabelControl();
            this.product_States1 = new DebtPlus.UI.TableAdministration.CS.Products.Product_StatesGrid();
            ((System.ComponentModel.ISupportInitialize)(this.DxErrorProvider1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TextEdit_description.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.calcEdit_Amount.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit_ActiveFlag.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit_Default.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lookUpEdit_InvoiceEmailTemplate.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // simpleButton_OK
            // 
            this.simpleButton_OK.Location = new System.Drawing.Point(372, 13);
            this.simpleButton_OK.TabIndex = 11;
            // 
            // simpleButton_Cancel
            // 
            this.simpleButton_Cancel.Location = new System.Drawing.Point(372, 56);
            this.simpleButton_Cancel.TabIndex = 12;
            // 
            // DefaultLookAndFeel1
            // 
            this.DefaultLookAndFeel1.LookAndFeel.SkinName = "McSkin";
            // 
            // LabelControl1
            // 
            this.LabelControl1.Location = new System.Drawing.Point(13, 9);
            this.LabelControl1.Name = "LabelControl1";
            this.LabelControl1.Size = new System.Drawing.Size(11, 13);
            this.LabelControl1.TabIndex = 0;
            this.LabelControl1.Text = "ID";
            // 
            // LabelControl_description
            // 
            this.LabelControl_description.Location = new System.Drawing.Point(13, 35);
            this.LabelControl_description.Name = "LabelControl_description";
            this.LabelControl_description.Size = new System.Drawing.Size(53, 13);
            this.LabelControl_description.TabIndex = 2;
            this.LabelControl_description.Text = "Description";
            // 
            // TextEdit_description
            // 
            this.TextEdit_description.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.TextEdit_description.Location = new System.Drawing.Point(86, 32);
            this.TextEdit_description.Name = "TextEdit_description";
            this.TextEdit_description.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.False;
            this.TextEdit_description.Properties.MaxLength = 50;
            this.TextEdit_description.Size = new System.Drawing.Size(280, 20);
            this.TextEdit_description.TabIndex = 3;
            // 
            // labelControl_Id
            // 
            this.labelControl_Id.Location = new System.Drawing.Point(86, 9);
            this.labelControl_Id.Name = "labelControl_Id";
            this.labelControl_Id.Size = new System.Drawing.Size(23, 13);
            this.labelControl_Id.TabIndex = 1;
            this.labelControl_Id.Text = "NEW";
            // 
            // calcEdit_Amount
            // 
            this.calcEdit_Amount.Location = new System.Drawing.Point(86, 84);
            this.calcEdit_Amount.Name = "calcEdit_Amount";
            this.calcEdit_Amount.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.calcEdit_Amount.Properties.DisplayFormat.FormatString = "c";
            this.calcEdit_Amount.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.calcEdit_Amount.Properties.EditFormat.FormatString = "f2";
            this.calcEdit_Amount.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.calcEdit_Amount.Properties.Precision = 2;
            this.calcEdit_Amount.Size = new System.Drawing.Size(100, 20);
            this.calcEdit_Amount.TabIndex = 7;
            // 
            // checkEdit_ActiveFlag
            // 
            this.checkEdit_ActiveFlag.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.checkEdit_ActiveFlag.Location = new System.Drawing.Point(11, 220);
            this.checkEdit_ActiveFlag.Name = "checkEdit_ActiveFlag";
            this.checkEdit_ActiveFlag.Properties.Caption = "Active";
            this.checkEdit_ActiveFlag.Size = new System.Drawing.Size(75, 20);
            this.checkEdit_ActiveFlag.TabIndex = 9;
            // 
            // checkEdit_Default
            // 
            this.checkEdit_Default.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.checkEdit_Default.Location = new System.Drawing.Point(101, 220);
            this.checkEdit_Default.Name = "checkEdit_Default";
            this.checkEdit_Default.Properties.Caption = "Default";
            this.checkEdit_Default.Size = new System.Drawing.Size(75, 20);
            this.checkEdit_Default.TabIndex = 10;
            // 
            // lookUpEdit_InvoiceEmailTemplate
            // 
            this.lookUpEdit_InvoiceEmailTemplate.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.lookUpEdit_InvoiceEmailTemplate.Location = new System.Drawing.Point(86, 58);
            this.lookUpEdit_InvoiceEmailTemplate.Name = "lookUpEdit_InvoiceEmailTemplate";
            this.lookUpEdit_InvoiceEmailTemplate.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.lookUpEdit_InvoiceEmailTemplate.Properties.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("Id", "Id", 20, DevExpress.Utils.FormatType.Numeric, "f0", false, DevExpress.Utils.HorzAlignment.Far, DevExpress.Data.ColumnSortOrder.None, DevExpress.Utils.DefaultBoolean.True),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("description", "Description", 20, DevExpress.Utils.FormatType.None, "", true, DevExpress.Utils.HorzAlignment.Near, DevExpress.Data.ColumnSortOrder.Ascending, DevExpress.Utils.DefaultBoolean.True)});
            this.lookUpEdit_InvoiceEmailTemplate.Properties.DisplayMember = "description";
            this.lookUpEdit_InvoiceEmailTemplate.Properties.NullText = "";
            this.lookUpEdit_InvoiceEmailTemplate.Properties.ShowFooter = false;
            this.lookUpEdit_InvoiceEmailTemplate.Properties.ShowHeader = false;
            this.lookUpEdit_InvoiceEmailTemplate.Properties.ValueMember = "Id";
            this.lookUpEdit_InvoiceEmailTemplate.Size = new System.Drawing.Size(280, 20);
            this.lookUpEdit_InvoiceEmailTemplate.TabIndex = 5;
            // 
            // labelControl2
            // 
            this.labelControl2.Location = new System.Drawing.Point(13, 87);
            this.labelControl2.Name = "labelControl2";
            this.labelControl2.Size = new System.Drawing.Size(37, 13);
            this.labelControl2.TabIndex = 6;
            this.labelControl2.Text = "Amount";
            // 
            // labelControl3
            // 
            this.labelControl3.Location = new System.Drawing.Point(12, 61);
            this.labelControl3.Name = "labelControl3";
            this.labelControl3.Size = new System.Drawing.Size(62, 13);
            this.labelControl3.TabIndex = 4;
            this.labelControl3.Text = "Invoice Email";
            // 
            // product_States1
            // 
            this.product_States1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.product_States1.Location = new System.Drawing.Point(12, 110);
            this.product_States1.Name = "product_States1";
            this.product_States1.Size = new System.Drawing.Size(431, 104);
            this.product_States1.TabIndex = 8;
            // 
            // EditForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(468, 252);
            this.Controls.Add(this.product_States1);
            this.Controls.Add(this.labelControl3);
            this.Controls.Add(this.labelControl2);
            this.Controls.Add(this.lookUpEdit_InvoiceEmailTemplate);
            this.Controls.Add(this.checkEdit_Default);
            this.Controls.Add(this.checkEdit_ActiveFlag);
            this.Controls.Add(this.calcEdit_Amount);
            this.Controls.Add(this.labelControl_Id);
            this.Controls.Add(this.TextEdit_description);
            this.Controls.Add(this.LabelControl_description);
            this.Controls.Add(this.LabelControl1);
            this.Name = "EditForm";
            this.Text = "Product Information";
            this.Controls.SetChildIndex(this.LabelControl1, 0);
            this.Controls.SetChildIndex(this.simpleButton_OK, 0);
            this.Controls.SetChildIndex(this.simpleButton_Cancel, 0);
            this.Controls.SetChildIndex(this.LabelControl_description, 0);
            this.Controls.SetChildIndex(this.TextEdit_description, 0);
            this.Controls.SetChildIndex(this.labelControl_Id, 0);
            this.Controls.SetChildIndex(this.calcEdit_Amount, 0);
            this.Controls.SetChildIndex(this.checkEdit_ActiveFlag, 0);
            this.Controls.SetChildIndex(this.checkEdit_Default, 0);
            this.Controls.SetChildIndex(this.lookUpEdit_InvoiceEmailTemplate, 0);
            this.Controls.SetChildIndex(this.labelControl2, 0);
            this.Controls.SetChildIndex(this.labelControl3, 0);
            this.Controls.SetChildIndex(this.product_States1, 0);
            ((System.ComponentModel.ISupportInitialize)(this.DxErrorProvider1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TextEdit_description.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.calcEdit_Amount.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit_ActiveFlag.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit_Default.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lookUpEdit_InvoiceEmailTemplate.Properties)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

		}

		private DevExpress.XtraEditors.LabelControl LabelControl1;
		private DevExpress.XtraEditors.LabelControl LabelControl_description;
        private DevExpress.XtraEditors.TextEdit TextEdit_description;
        private DevExpress.XtraEditors.LabelControl labelControl_Id;
        private DevExpress.XtraEditors.CalcEdit calcEdit_Amount;
        private DevExpress.XtraEditors.CheckEdit checkEdit_ActiveFlag;
        private DevExpress.XtraEditors.CheckEdit checkEdit_Default;
        private DevExpress.XtraEditors.LookUpEdit lookUpEdit_InvoiceEmailTemplate;
        private DevExpress.XtraEditors.LabelControl labelControl2;
        private DevExpress.XtraEditors.LabelControl labelControl3;
        private Product_StatesGrid product_States1;
	}
}

