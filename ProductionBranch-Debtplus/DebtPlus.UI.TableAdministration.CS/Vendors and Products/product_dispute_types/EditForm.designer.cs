﻿namespace DebtPlus.UI.TableAdministration.CS.product_dispute_types
{
    partial class EditForm
    {
		//Form overrides dispose to clean up the component list.
		[System.Diagnostics.DebuggerNonUserCode]
		protected override void Dispose(bool disposing)
		{
			try 
            {
				if (disposing)
                {
                    if (components != null) components.Dispose();
				}
                components = null;
            }
    	    finally
            {
				base.Dispose(disposing);
			}
		}

		//Required by the Windows Form Designer
		private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.TextEdit_description = new DevExpress.XtraEditors.TextEdit();
            this.LabelControl_description = new DevExpress.XtraEditors.LabelControl();
            this.LabelControl_Id = new DevExpress.XtraEditors.LabelControl();
            this.LabelControl4 = new DevExpress.XtraEditors.LabelControl();
            ((System.ComponentModel.ISupportInitialize)(this.DxErrorProvider1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TextEdit_description.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // simpleButton_OK
            // 
            this.simpleButton_OK.Location = new System.Drawing.Point(317, 35);
            this.simpleButton_OK.TabIndex = 10;
            // 
            // simpleButton_Cancel
            // 
            this.simpleButton_Cancel.Location = new System.Drawing.Point(317, 67);
            this.simpleButton_Cancel.TabIndex = 11;
            // 
            // DefaultLookAndFeel1
            // 
            this.DefaultLookAndFeel1.LookAndFeel.SkinName = "McSkin";
            // 
            // TextEdit_description
            // 
            this.TextEdit_description.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.TextEdit_description.Location = new System.Drawing.Point(81, 54);
            this.TextEdit_description.Name = "TextEdit_description";
            this.TextEdit_description.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.False;
            this.TextEdit_description.Properties.MaxLength = 50;
            this.TextEdit_description.Size = new System.Drawing.Size(219, 20);
            this.TextEdit_description.TabIndex = 3;
            // 
            // LabelControl_description
            // 
            this.LabelControl_description.Location = new System.Drawing.Point(8, 57);
            this.LabelControl_description.Name = "LabelControl_description";
            this.LabelControl_description.Size = new System.Drawing.Size(53, 13);
            this.LabelControl_description.TabIndex = 2;
            this.LabelControl_description.Text = "Description";
            // 
            // LabelControl_Id
            // 
            this.LabelControl_Id.Location = new System.Drawing.Point(81, 35);
            this.LabelControl_Id.Name = "LabelControl_Id";
            this.LabelControl_Id.Size = new System.Drawing.Size(23, 13);
            this.LabelControl_Id.TabIndex = 1;
            this.LabelControl_Id.Text = "NEW";
            // 
            // LabelControl4
            // 
            this.LabelControl4.Location = new System.Drawing.Point(8, 35);
            this.LabelControl4.Name = "LabelControl4";
            this.LabelControl4.Size = new System.Drawing.Size(11, 13);
            this.LabelControl4.TabIndex = 0;
            this.LabelControl4.Text = "ID";
            // 
            // EditForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(401, 108);
            this.Controls.Add(this.TextEdit_description);
            this.Controls.Add(this.LabelControl_description);
            this.Controls.Add(this.LabelControl_Id);
            this.Controls.Add(this.LabelControl4);
            this.Name = "EditForm";
            this.Text = "Product Dispute Type";
            this.Controls.SetChildIndex(this.simpleButton_OK, 0);
            this.Controls.SetChildIndex(this.simpleButton_Cancel, 0);
            this.Controls.SetChildIndex(this.LabelControl4, 0);
            this.Controls.SetChildIndex(this.LabelControl_Id, 0);
            this.Controls.SetChildIndex(this.LabelControl_description, 0);
            this.Controls.SetChildIndex(this.TextEdit_description, 0);
            ((System.ComponentModel.ISupportInitialize)(this.DxErrorProvider1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TextEdit_description.Properties)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }
        private DevExpress.XtraEditors.TextEdit TextEdit_description;
        private DevExpress.XtraEditors.LabelControl LabelControl_description;
        private DevExpress.XtraEditors.LabelControl LabelControl_Id;
        private DevExpress.XtraEditors.LabelControl LabelControl4;

    }
}