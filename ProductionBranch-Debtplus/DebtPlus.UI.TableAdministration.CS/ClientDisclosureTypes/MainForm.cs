#region "Copyright 2000-2012 DebtPlus, L.L.C."
//{*******************************************************************}
//{                                                                   }
//{       DebtPlus Debt Management System                             }
//{                                                                   }
//{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
//{       ALL RIGHTS RESERVED                                         }
//{                                                                   }
//{   The entire contents of this file is protected by U.S. and       }
//{   International Copyright Laws. Unauthorized reproduction,        }
//{   reverse-engineering, and distribution of all or any portion of  }
//{   the code contained in this file is strictly prohibited and may  }
//{   result in severe civil and criminal penalties and will be       }
//{   prosecuted to the maximum extent possible under the law.        }
//{                                                                   }
//{   RESTRICTIONS                                                    }
//{                                                                   }
//{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
//{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
//{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
//{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
//{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
//{                                                                   }
//{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
//{   ADDITIONAL RESTRICTIONS.                                        }
//{                                                                   }
//{*******************************************************************}

#endregion "Copyright 2000-2012 DebtPlus, L.L.C."

using System;
using System.Linq;
using DebtPlus.LINQ;
using DebtPlus.UI.Common;

namespace DebtPlus.UI.TableAdministration.CS.ClentDisclosureTypes
{
    public partial class MainForm : Templates.MainForm
    {
        private BusinessContext bc = new BusinessContext();
        private System.Collections.Generic.List<client_DisclosureType> colRecords = null;

        /// <summary>
        /// Initialize the new class
        /// </summary>
        public MainForm()
            : base()
        {
            InitializeComponent();
            RegisterHandlers();
        }

        /// <summary>
        /// Register the event handlers
        /// </summary>
        private void RegisterHandlers()
        {
            Load += MainForm_Load;
        }

        /// <summary>
        /// Remove the event handler registrations
        /// </summary>
        private void UnRegisterHandlers()
        {
            Load -= MainForm_Load;
        }

        /// <summary>
        /// Process the load event on the form
        /// </summary>
        private void MainForm_Load(object sender, EventArgs e)
        {
            using (var cm = new CursorManager())
            {
                // Retrieve the collection of records
                colRecords = bc.client_DisclosureTypes.ToList();
                gridControl1.DataSource = colRecords;

                // Update the grid with the record collection
                gridView1.BestFitColumns();
                gridView1.RefreshData();
            }
        }

        /// <summary>
        /// Handle the edit of the information on the form
        /// </summary>
        protected override void UpdateRecord(object obj)
        {
            // Locate the record to edit
            client_DisclosureType record = obj as client_DisclosureType;
            if (record == null)
            {
                return;
            }

            // Default setting before the edit
            bool priorDefault = record.Default;

            // Edit the record
            using (var frm = new EditForm(bc, record))
            {
                if (frm.ShowDialog() != System.Windows.Forms.DialogResult.OK)
                {
                    return;
                }
            }

            // If the record is now default then remove the previous default
            if (record.Default && !priorDefault)
            {
                foreach (var defaultRecord in colRecords.Where(s => s.Default))
                {
                    defaultRecord.Default = false;
                }
                record.Default = true;
            }

            // Commit the changes to the database
            bc.SubmitChanges();
            gridView1.RefreshData();
        }

        /// <summary>
        /// Create a new record for the database
        /// </summary>
        protected override void CreateRecord()
        {
            // Create a new blank record
            var record = DebtPlus.LINQ.Factory.Manufacture_client_DisclosureType();
            using (var frm = new EditForm(bc, record))
            {
                if (frm.ShowDialog() != System.Windows.Forms.DialogResult.OK)
                {
                    return;
                }
            }

            // If the record is now default then remove the previous default
            if (record.Default)
            {
                foreach (var defaultRecord in colRecords.Where(s => s.Default))
                {
                    defaultRecord.Default = false;
                }
                record.Default = true;
            }

            // Insert the record into the database
            bc.client_DisclosureTypes.InsertOnSubmit(record);
            bc.SubmitChanges();

            // Add it to the display list
            colRecords.Add(record);
            gridView1.RefreshData();
        }

        /// <summary>
        /// Delete the current record from the database
        /// </summary>
        protected override void DeleteRecord(object obj)
        {
            // Locate the record to be deleted
            client_DisclosureType record = obj as client_DisclosureType;
            if (record == null)
            {
                return;
            }

            // Confirm the deletion
            if (DebtPlus.Data.Prompts.RequestConfirmation_Delete() != System.Windows.Forms.DialogResult.Yes)
            {
                return;
            }

            // Purge the record from the database
            bc.client_DisclosureTypes.DeleteOnSubmit(record);
            bc.SubmitChanges();

            // Purge the record from the list
            colRecords.Remove(record);
            gridView1.RefreshData();
        }
    }
}