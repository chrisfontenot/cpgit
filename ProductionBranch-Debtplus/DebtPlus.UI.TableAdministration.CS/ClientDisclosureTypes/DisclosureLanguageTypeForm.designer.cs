
using System.Windows.Forms;

namespace DebtPlus.UI.TableAdministration.CS.ClientDisclosureTypes
{
    partial class DisclosureLanguageTypeForm
	{
		//Form overrides dispose to clean up the component list.
		[System.Diagnostics.DebuggerNonUserCode]
		protected override void Dispose(bool disposing)
		{
			try 
            {
				if (disposing)
                {
                    if (components != null) components.Dispose();
				}
                components = null;
            }

            finally 
            {
				base.Dispose(disposing);
			}
		}

		//Required by the Windows Form Designer
		private System.ComponentModel.IContainer components = null;

		//NOTE: The following procedure is required by the Windows Form Designer
		//It can be modified using the Windows Form Designer.  
		//Do not modify it using the code editor.
		[System.Diagnostics.DebuggerStepThrough]
		private void InitializeComponent()
		{
            this.LabelControl1 = new DevExpress.XtraEditors.LabelControl();
            this.LabelControl_ID = new DevExpress.XtraEditors.LabelControl();
            this.textEdit_uri = new DevExpress.XtraEditors.TextEdit();
            this.labelControl3 = new DevExpress.XtraEditors.LabelControl();
            this.lookUpEdit_languageID = new DevExpress.XtraEditors.LookUpEdit();
            this.labelControl2 = new DevExpress.XtraEditors.LabelControl();
            this.dateEdit_effective = new DevExpress.XtraEditors.DateEdit();
            this.dateEdit_expires = new DevExpress.XtraEditors.DateEdit();
            this.labelControl4 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl5 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl6 = new DevExpress.XtraEditors.LabelControl();
            this.dateEdit_revision_date = new DevExpress.XtraEditors.DateEdit();
            ((System.ComponentModel.ISupportInitialize)(this.DxErrorProvider1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit_uri.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lookUpEdit_languageID.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEdit_effective.Properties.VistaTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEdit_effective.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEdit_expires.Properties.VistaTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEdit_expires.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEdit_revision_date.Properties.VistaTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEdit_revision_date.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // simpleButton_OK
            // 
            this.simpleButton_OK.Location = new System.Drawing.Point(372, 22);
            this.simpleButton_OK.TabIndex = 10;
            // 
            // simpleButton_Cancel
            // 
            this.simpleButton_Cancel.Location = new System.Drawing.Point(372, 65);
            this.simpleButton_Cancel.TabIndex = 11;
            // 
            // DefaultLookAndFeel1
            // 
            this.DefaultLookAndFeel1.LookAndFeel.SkinName = "Caramel";
            // 
            // LabelControl1
            // 
            this.LabelControl1.Location = new System.Drawing.Point(13, 13);
            this.LabelControl1.Name = "LabelControl1";
            this.LabelControl1.Size = new System.Drawing.Size(11, 13);
            this.LabelControl1.TabIndex = 0;
            this.LabelControl1.Text = "ID";
            // 
            // LabelControl_ID
            // 
            this.LabelControl_ID.Location = new System.Drawing.Point(86, 13);
            this.LabelControl_ID.Name = "LabelControl_ID";
            this.LabelControl_ID.Size = new System.Drawing.Size(23, 13);
            this.LabelControl_ID.TabIndex = 1;
            this.LabelControl_ID.Text = "NEW";
            // 
            // textEdit_uri
            // 
            this.textEdit_uri.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.textEdit_uri.Location = new System.Drawing.Point(86, 59);
            this.textEdit_uri.Name = "textEdit_uri";
            this.textEdit_uri.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.False;
            this.textEdit_uri.Properties.MaxLength = 150;
            this.textEdit_uri.Properties.NullText = "Required Value";
            this.textEdit_uri.Size = new System.Drawing.Size(267, 20);
            this.textEdit_uri.TabIndex = 5;
            // 
            // labelControl3
            // 
            this.labelControl3.Location = new System.Drawing.Point(13, 63);
            this.labelControl3.Name = "labelControl3";
            this.labelControl3.Size = new System.Drawing.Size(18, 13);
            this.labelControl3.TabIndex = 4;
            this.labelControl3.Text = "&URI";
            // 
            // lookUpEdit_languageID
            // 
            this.lookUpEdit_languageID.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.lookUpEdit_languageID.Location = new System.Drawing.Point(86, 32);
            this.lookUpEdit_languageID.Name = "lookUpEdit_languageID";
            this.lookUpEdit_languageID.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.False;
            this.lookUpEdit_languageID.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.lookUpEdit_languageID.Properties.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("Id", "ID", 20, DevExpress.Utils.FormatType.Numeric, "f0", false, DevExpress.Utils.HorzAlignment.Far, DevExpress.Data.ColumnSortOrder.None, DevExpress.Utils.DefaultBoolean.True),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("Attribute", "Description", 20, DevExpress.Utils.FormatType.None, "", true, DevExpress.Utils.HorzAlignment.Near, DevExpress.Data.ColumnSortOrder.Ascending, DevExpress.Utils.DefaultBoolean.True)});
            this.lookUpEdit_languageID.Properties.DisplayMember = "Attribute";
            this.lookUpEdit_languageID.Properties.NullText = "";
            this.lookUpEdit_languageID.Properties.ShowFooter = false;
            this.lookUpEdit_languageID.Properties.ShowHeader = false;
            this.lookUpEdit_languageID.Properties.ValueMember = "Id";
            this.lookUpEdit_languageID.Size = new System.Drawing.Size(267, 20);
            this.lookUpEdit_languageID.TabIndex = 3;
            // 
            // labelControl2
            // 
            this.labelControl2.Location = new System.Drawing.Point(13, 36);
            this.labelControl2.Name = "labelControl2";
            this.labelControl2.Size = new System.Drawing.Size(47, 13);
            this.labelControl2.TabIndex = 2;
            this.labelControl2.Text = "&Language";
            // 
            // dateEdit_effective
            // 
            this.dateEdit_effective.EditValue = null;
            this.dateEdit_effective.Location = new System.Drawing.Point(86, 86);
            this.dateEdit_effective.Name = "dateEdit_effective";
            this.dateEdit_effective.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.False;
            this.dateEdit_effective.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dateEdit_effective.Properties.VistaTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.dateEdit_effective.Size = new System.Drawing.Size(100, 20);
            this.dateEdit_effective.TabIndex = 7;
            // 
            // dateEdit_expires
            // 
            this.dateEdit_expires.EditValue = null;
            this.dateEdit_expires.Location = new System.Drawing.Point(86, 113);
            this.dateEdit_expires.Name = "dateEdit_expires";
            this.dateEdit_expires.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.True;
            this.dateEdit_expires.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dateEdit_expires.Properties.VistaTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.dateEdit_expires.Size = new System.Drawing.Size(100, 20);
            this.dateEdit_expires.TabIndex = 9;
            // 
            // labelControl4
            // 
            this.labelControl4.Location = new System.Drawing.Point(13, 90);
            this.labelControl4.Name = "labelControl4";
            this.labelControl4.Size = new System.Drawing.Size(43, 13);
            this.labelControl4.TabIndex = 6;
            this.labelControl4.Text = "Effective";
            // 
            // labelControl5
            // 
            this.labelControl5.Location = new System.Drawing.Point(13, 117);
            this.labelControl5.Name = "labelControl5";
            this.labelControl5.Size = new System.Drawing.Size(35, 13);
            this.labelControl5.TabIndex = 8;
            this.labelControl5.Text = "Expires";
            // 
            // labelControl6
            // 
            this.labelControl6.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.labelControl6.Location = new System.Drawing.Point(209, 88);
            this.labelControl6.Name = "labelControl6";
            this.labelControl6.Size = new System.Drawing.Size(38, 13);
            this.labelControl6.TabIndex = 12;
            this.labelControl6.Text = "Revised";
            // 
            // dateEdit_revision_date
            // 
            this.dateEdit_revision_date.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.dateEdit_revision_date.EditValue = null;
            this.dateEdit_revision_date.Location = new System.Drawing.Point(253, 85);
            this.dateEdit_revision_date.Name = "dateEdit_revision_date";
            this.dateEdit_revision_date.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.False;
            this.dateEdit_revision_date.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dateEdit_revision_date.Properties.VistaTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.dateEdit_revision_date.Size = new System.Drawing.Size(100, 20);
            this.dateEdit_revision_date.TabIndex = 13;
            // 
            // DisclosureLanguageTypeForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(468, 146);
            this.Controls.Add(this.labelControl6);
            this.Controls.Add(this.dateEdit_revision_date);
            this.Controls.Add(this.labelControl5);
            this.Controls.Add(this.labelControl4);
            this.Controls.Add(this.dateEdit_expires);
            this.Controls.Add(this.dateEdit_effective);
            this.Controls.Add(this.labelControl2);
            this.Controls.Add(this.lookUpEdit_languageID);
            this.Controls.Add(this.labelControl3);
            this.Controls.Add(this.textEdit_uri);
            this.Controls.Add(this.LabelControl_ID);
            this.Controls.Add(this.LabelControl1);
            this.Name = "DisclosureLanguageTypeForm";
            this.Text = "Disclosure Language";
            this.Controls.SetChildIndex(this.LabelControl1, 0);
            this.Controls.SetChildIndex(this.LabelControl_ID, 0);
            this.Controls.SetChildIndex(this.simpleButton_OK, 0);
            this.Controls.SetChildIndex(this.simpleButton_Cancel, 0);
            this.Controls.SetChildIndex(this.textEdit_uri, 0);
            this.Controls.SetChildIndex(this.labelControl3, 0);
            this.Controls.SetChildIndex(this.lookUpEdit_languageID, 0);
            this.Controls.SetChildIndex(this.labelControl2, 0);
            this.Controls.SetChildIndex(this.dateEdit_effective, 0);
            this.Controls.SetChildIndex(this.dateEdit_expires, 0);
            this.Controls.SetChildIndex(this.labelControl4, 0);
            this.Controls.SetChildIndex(this.labelControl5, 0);
            this.Controls.SetChildIndex(this.dateEdit_revision_date, 0);
            this.Controls.SetChildIndex(this.labelControl6, 0);
            ((System.ComponentModel.ISupportInitialize)(this.DxErrorProvider1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit_uri.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lookUpEdit_languageID.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEdit_effective.Properties.VistaTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEdit_effective.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEdit_expires.Properties.VistaTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEdit_expires.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEdit_revision_date.Properties.VistaTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEdit_revision_date.Properties)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

		}
		private DevExpress.XtraEditors.LabelControl LabelControl1;
        private DevExpress.XtraEditors.LabelControl LabelControl_ID;
        private DevExpress.XtraEditors.TextEdit textEdit_uri;
        private DevExpress.XtraEditors.LabelControl labelControl3;
        private DevExpress.XtraEditors.LookUpEdit lookUpEdit_languageID;
        private DevExpress.XtraEditors.LabelControl labelControl2;
        private DevExpress.XtraEditors.DateEdit dateEdit_effective;
        private DevExpress.XtraEditors.DateEdit dateEdit_expires;
        private DevExpress.XtraEditors.LabelControl labelControl4;
        private DevExpress.XtraEditors.LabelControl labelControl5;
        private DevExpress.XtraEditors.LabelControl labelControl6;
        private DevExpress.XtraEditors.DateEdit dateEdit_revision_date;
	}
}
