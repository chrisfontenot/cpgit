﻿using System;
using System.Windows.Forms;
using DebtPlus.LINQ;

namespace DebtPlus.UI.TableAdministration.CS.ClientDisclosureTypes
{
    internal partial class DisclosureLanguageGridControl : DevExpress.XtraEditors.XtraUserControl
    {
        private BusinessContext bc = null;
        private client_DisclosureType DisclosureTypeRecord = null;

        internal DisclosureLanguageGridControl()
        {
            InitializeComponent();
        }

        /// <summary>
        /// Register the event handlers
        /// </summary>
        private void RegisterHandlers()
        {
            barButtonItem_Create.ItemClick += barButtonItem_Create_ItemClick;
            barButtonItem_Update.ItemClick += barButtonItem_Update_ItemClick;
            barButtonItem_Delete.ItemClick += barButtonItem_Delete_ItemClick;
            gridView1.DoubleClick += gridView1_DoubleClick;
            gridView1.MouseDown += gridView1_MouseDown;
        }

        /// <summary>
        /// Remove the event handler registration
        /// </summary>
        private void UnRegisterHandlers()
        {
            barButtonItem_Create.ItemClick -= barButtonItem_Create_ItemClick;
            barButtonItem_Update.ItemClick -= barButtonItem_Update_ItemClick;
            barButtonItem_Delete.ItemClick -= barButtonItem_Delete_ItemClick;
            gridView1.DoubleClick -= gridView1_DoubleClick;
            gridView1.MouseDown -= gridView1_MouseDown;
        }

        /// <summary>
        /// Double click event on the list
        /// </summary>
        private void gridView1_MouseDown(object sender, MouseEventArgs e)
        {
            // Find the record to be edited from the list
            if (e.Button == System.Windows.Forms.MouseButtons.Right)
            {
                DevExpress.XtraGrid.Views.Grid.ViewInfo.GridHitInfo hi = gridView1.CalcHitInfo((gridControl1.PointToClient(System.Windows.Forms.Control.MousePosition)));
                System.Int32 RowHandle = hi.RowHandle;
                gridView1.FocusedRowHandle = RowHandle;

                // Find the record for this row
                object obj = null;
                if (RowHandle >= 0)
                {
                    obj = gridView1.GetRow(RowHandle);
                }

                // If the row is defined then edit the row.
                if (obj != null)
                {
                    barButtonItem_Update.Enabled = true;
                    barButtonItem_Delete.Enabled = true;
                }
                else
                {
                    barButtonItem_Update.Enabled = false;
                    barButtonItem_Delete.Enabled = false;
                }

                popupMenu1.ShowPopup(System.Windows.Forms.Control.MousePosition);
            }
        }

        /// <summary>
        /// Double click event on the list
        /// </summary>
        private void gridView1_DoubleClick(object sender, System.EventArgs e)
        {
            DevExpress.XtraGrid.Views.Grid.ViewInfo.GridHitInfo hi = gridView1.CalcHitInfo((gridControl1.PointToClient(System.Windows.Forms.Control.MousePosition)));
            System.Int32 RowHandle = hi.RowHandle;

            // Find the record to be edited from the list
            if (RowHandle >= 0)
            {
                gridView1.FocusedRowHandle = RowHandle;
                Object obj = gridView1.GetRow(RowHandle);
                if (obj != null)
                {
                    UpdateRecord(obj);
                }
            }
        }

        /// <summary>
        /// Edit -> Create menu record clicked
        /// </summary>
        private void barButtonItem_Create_ItemClick(System.Object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            CreateRecord();
        }

        /// <summary>
        /// Edit -> Edit menu record clicked
        /// </summary>
        private void barButtonItem_Update_ItemClick(System.Object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            System.Int32 RowHandle = gridView1.FocusedRowHandle;

            if (RowHandle >= 0)
            {
                gridView1.FocusedRowHandle = RowHandle;
                object obj = gridView1.GetRow(RowHandle);
                if (obj != null)
                {
                    UpdateRecord(obj);
                }
            }
        }

        /// <summary>
        /// Edit -> Delete menu record clicked
        /// </summary>
        private void barButtonItem_Delete_ItemClick(System.Object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            System.Int32 RowHandle = gridView1.FocusedRowHandle;

            if (RowHandle >= 0)
            {
                gridView1.FocusedRowHandle = RowHandle;
                object obj = gridView1.GetRow(RowHandle);
                if (obj != null)
                {
                    DeleteRecord(obj);
                }
            }
        }

        /// <summary>
        /// Read and display the information for the grid control
        /// </summary>
        /// <param name="bc">Pointer to the business data context</param>
        /// <param name="record">Current disclosure record being processed</param>
        internal void ReadForm(BusinessContext bc, client_DisclosureType DisclosureTypeRecord)
        {
            UnRegisterHandlers();

            // Help convert the language to a suitable string.
            gridColumn_Language.DisplayFormat.Format = new LanguageFormatter();
            gridColumn_Language.GroupFormat.Format = new LanguageFormatter();

            try
            {
                // Save the parameters as passed for future reference
                this.bc = bc;
                this.DisclosureTypeRecord = DisclosureTypeRecord;

                // The grid comes from the disclosure type record directly
                gridControl1.DataSource = DisclosureTypeRecord.client_DisclosureLanguageTypes.GetNewBindingList();
                gridView1.RefreshData();
            }
            finally
            {
                RegisterHandlers();
            }
        }

        /// <summary>
        /// Handle the edit of the information on the form
        /// </summary>
        protected void UpdateRecord(object obj)
        {
            var record = obj as client_DisclosureLanguageType;
            if (record == null)
            {
                return;
            }

            try
            {
                // Edit the record
                using (var frm = new DisclosureLanguageTypeForm(bc, record))
                {
                    if (frm.ShowDialog() != DialogResult.OK)
                    {
                        return;
                    }
                }

                // Update the grid control with the new changes. The record is written when we return normally.
                gridControl1.DataSource = DisclosureTypeRecord.client_DisclosureLanguageTypes.GetNewBindingList();
                gridView1.RefreshData();
            }
            catch (System.Data.SqlClient.SqlException ex)
            {
                DebtPlus.UI.Common.ErrorHandling.HandleErrors(ex, "Error updating the database entries");
            }
        }

        /// <summary>
        /// Create a new record for the database
        /// </summary>
        protected void CreateRecord()
        {
            var record = DebtPlus.LINQ.Factory.Manufacture_client_DisclosureLanguageType();

            // Edit the record
            using (var frm = new DisclosureLanguageTypeForm(bc, record))
            {
                if (frm.ShowDialog() != DialogResult.OK)
                {
                    return;
                }
            }

            // Add the record to the collection and update the grid with the new values.
            DisclosureTypeRecord.client_DisclosureLanguageTypes.Add(record);
            gridControl1.DataSource = DisclosureTypeRecord.client_DisclosureLanguageTypes.GetNewBindingList();
            gridView1.RefreshData();
        }

        /// <summary>
        /// Delete the current record from the database
        /// </summary>
        protected void DeleteRecord(object obj)
        {
            var record = obj as client_DisclosureLanguageType;
            if (record == null)
            {
                return;
            }

            // Ask the user if the record should be deleted
            if (DebtPlus.Data.Forms.MessageBox.Show("Are you sure that you want to delete this record?\r\n\r\n If you delete this record then it will remove the\r\n disclosures from all clients who used this version.\r\n\r\n It may be better to simply mark it as EXPIRED and\r\n leave the disclosure in the system rather than to\r\n remove the references.", "Are you really sure?", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2) != DialogResult.Yes)
            {
                return;
            }

            // Delete the reference from the system.
            DisclosureTypeRecord.client_DisclosureLanguageTypes.Remove(record);
            record.client_DisclosureType = null;
            record.clientDisclosureTypeID = default(Int32);

            // Reload the grid control with the new values
            gridControl1.DataSource = DisclosureTypeRecord.client_DisclosureLanguageTypes.GetNewBindingList();
            gridView1.RefreshData();
        }

        /// <summary>
        /// Class to help the grid convert the language ID number to a suitable string.
        /// </summary>
        private class LanguageFormatter : System.ICustomFormatter, System.IFormatProvider
        {
            internal LanguageFormatter()
                : base()
            {
            }

            public object GetFormat(Type formatType)
            {
                return this;
            }

            /// <summary>
            /// Format the input value to the name of the language
            /// </summary>
            public string Format(string format, object arg, IFormatProvider formatProvider)
            {
                Int32? intValue = DebtPlus.Utils.Nulls.v_Int32(arg);
                if (intValue.HasValue && intValue.Value > 0)
                {
                    // Take the entire attribute type list. Since languages are unique in that table, we don't need just the language entries.
                    var q = DebtPlus.LINQ.Cache.AttributeType.getList().Find(s => s.Id == intValue.Value);
                    if (q != null)
                    {
                        return q.Attribute;
                    }
                }
                return string.Empty;
            }
        }
    }
}