using System.Windows.Forms;

namespace DebtPlus.UI.TableAdministration.CS.ClentDisclosureTypes
{
	partial class EditForm
	{
		//Form overrides dispose to clean up the component list.
		[System.Diagnostics.DebuggerNonUserCode]
		protected override void Dispose(bool disposing)
		{
			try 
            {
				if (disposing)
                {
                    if (components != null) components.Dispose();
				}
                components = null;
            }
    	    finally
            {
				base.Dispose(disposing);
			}
		}

		//Required by the Windows Form Designer
		private System.ComponentModel.IContainer components = null;

		//NOTE: The following procedure is required by the Windows Form Designer
		//It can be modified using the Windows Form Designer.  
		//Do not modify it using the code editor.
		[System.Diagnostics.DebuggerStepThrough]
		private void InitializeComponent()
		{
            this.LabelControl1 = new DevExpress.XtraEditors.LabelControl();
            this.LabelControl_ID = new DevExpress.XtraEditors.LabelControl();
            this.textEdit_description = new DevExpress.XtraEditors.TextEdit();
            this.textEdit_assembly = new DevExpress.XtraEditors.TextEdit();
            this.checkEdit_ActiveFlag = new DevExpress.XtraEditors.CheckEdit();
            this.checkEdit_Default = new DevExpress.XtraEditors.CheckEdit();
            this.checkEdit_RequiresProperty = new DevExpress.XtraEditors.CheckEdit();
            this.checkEdit_AppliesApplicant = new DevExpress.XtraEditors.CheckEdit();
            this.checkEdit_AppliesCoApplicant = new DevExpress.XtraEditors.CheckEdit();
            this.labelControl2 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl3 = new DevExpress.XtraEditors.LabelControl();
            this.disclosureLanguageGridControl1 = new DebtPlus.UI.TableAdministration.CS.ClientDisclosureTypes.DisclosureLanguageGridControl();
            ((System.ComponentModel.ISupportInitialize)(this.DxErrorProvider1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit_description.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit_assembly.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit_ActiveFlag.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit_Default.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit_RequiresProperty.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit_AppliesApplicant.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit_AppliesCoApplicant.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // simpleButton_OK
            // 
            this.simpleButton_OK.Location = new System.Drawing.Point(372, 22);
            this.simpleButton_OK.TabIndex = 12;
            // 
            // simpleButton_Cancel
            // 
            this.simpleButton_Cancel.Location = new System.Drawing.Point(372, 65);
            this.simpleButton_Cancel.TabIndex = 13;
            // 
            // DefaultLookAndFeel1
            // 
            this.DefaultLookAndFeel1.LookAndFeel.SkinName = "McSkin";
            // 
            // LabelControl1
            // 
            this.LabelControl1.Location = new System.Drawing.Point(13, 13);
            this.LabelControl1.Name = "LabelControl1";
            this.LabelControl1.Size = new System.Drawing.Size(11, 13);
            this.LabelControl1.TabIndex = 0;
            this.LabelControl1.Text = "ID";
            // 
            // LabelControl_ID
            // 
            this.LabelControl_ID.Location = new System.Drawing.Point(86, 13);
            this.LabelControl_ID.Name = "LabelControl_ID";
            this.LabelControl_ID.Size = new System.Drawing.Size(23, 13);
            this.LabelControl_ID.TabIndex = 1;
            this.LabelControl_ID.Text = "NEW";
            // 
            // textEdit_description
            // 
            this.textEdit_description.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.textEdit_description.Location = new System.Drawing.Point(86, 32);
            this.textEdit_description.Name = "textEdit_description";
            this.textEdit_description.Properties.MaxLength = 50;
            this.textEdit_description.Properties.NullText = "Required Value";
            this.textEdit_description.Size = new System.Drawing.Size(267, 20);
            this.textEdit_description.TabIndex = 3;
            // 
            // textEdit_assembly
            // 
            this.textEdit_assembly.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.textEdit_assembly.Location = new System.Drawing.Point(86, 62);
            this.textEdit_assembly.Name = "textEdit_assembly";
            this.textEdit_assembly.Properties.MaxLength = 200;
            this.textEdit_assembly.Properties.NullText = "Required Value";
            this.textEdit_assembly.Size = new System.Drawing.Size(267, 20);
            this.textEdit_assembly.TabIndex = 5;
            // 
            // checkEdit_ActiveFlag
            // 
            this.checkEdit_ActiveFlag.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.checkEdit_ActiveFlag.Location = new System.Drawing.Point(92, 264);
            this.checkEdit_ActiveFlag.Name = "checkEdit_ActiveFlag";
            this.checkEdit_ActiveFlag.Properties.Caption = "Ac&tive";
            this.checkEdit_ActiveFlag.Size = new System.Drawing.Size(66, 20);
            this.checkEdit_ActiveFlag.TabIndex = 9;
            // 
            // checkEdit_Default
            // 
            this.checkEdit_Default.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.checkEdit_Default.Location = new System.Drawing.Point(11, 264);
            this.checkEdit_Default.Name = "checkEdit_Default";
            this.checkEdit_Default.Properties.Caption = "&Default";
            this.checkEdit_Default.Size = new System.Drawing.Size(75, 20);
            this.checkEdit_Default.TabIndex = 8;
            // 
            // checkEdit_RequiresProperty
            // 
            this.checkEdit_RequiresProperty.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.checkEdit_RequiresProperty.Location = new System.Drawing.Point(92, 239);
            this.checkEdit_RequiresProperty.Name = "checkEdit_RequiresProperty";
            this.checkEdit_RequiresProperty.Properties.Caption = "&Requires Property Reference";
            this.checkEdit_RequiresProperty.Size = new System.Drawing.Size(172, 20);
            this.checkEdit_RequiresProperty.TabIndex = 7;
            // 
            // checkEdit_AppliesApplicant
            // 
            this.checkEdit_AppliesApplicant.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.checkEdit_AppliesApplicant.Location = new System.Drawing.Point(322, 239);
            this.checkEdit_AppliesApplicant.Name = "checkEdit_AppliesApplicant";
            this.checkEdit_AppliesApplicant.Properties.Caption = "Applies to Applicant";
            this.checkEdit_AppliesApplicant.Size = new System.Drawing.Size(122, 20);
            this.checkEdit_AppliesApplicant.TabIndex = 10;
            // 
            // checkEdit_AppliesCoApplicant
            // 
            this.checkEdit_AppliesCoApplicant.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.checkEdit_AppliesCoApplicant.Location = new System.Drawing.Point(322, 264);
            this.checkEdit_AppliesCoApplicant.Name = "checkEdit_AppliesCoApplicant";
            this.checkEdit_AppliesCoApplicant.Properties.Caption = "Applies to CoApplicant";
            this.checkEdit_AppliesCoApplicant.Size = new System.Drawing.Size(140, 20);
            this.checkEdit_AppliesCoApplicant.TabIndex = 11;
            // 
            // labelControl2
            // 
            this.labelControl2.Location = new System.Drawing.Point(14, 35);
            this.labelControl2.Name = "labelControl2";
            this.labelControl2.Size = new System.Drawing.Size(53, 13);
            this.labelControl2.TabIndex = 2;
            this.labelControl2.Text = "&Description";
            // 
            // labelControl3
            // 
            this.labelControl3.Location = new System.Drawing.Point(14, 66);
            this.labelControl3.Name = "labelControl3";
            this.labelControl3.Size = new System.Drawing.Size(45, 13);
            this.labelControl3.TabIndex = 4;
            this.labelControl3.Text = "&Assembly";
            // 
            // disclosureLanguageGridControl1
            // 
            this.disclosureLanguageGridControl1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.disclosureLanguageGridControl1.Location = new System.Drawing.Point(14, 97);
            this.disclosureLanguageGridControl1.Name = "disclosureLanguageGridControl1";
            this.disclosureLanguageGridControl1.Size = new System.Drawing.Size(442, 136);
            this.disclosureLanguageGridControl1.TabIndex = 6;
            // 
            // EditForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(468, 288);
            this.Controls.Add(this.disclosureLanguageGridControl1);
            this.Controls.Add(this.labelControl3);
            this.Controls.Add(this.labelControl2);
            this.Controls.Add(this.checkEdit_AppliesCoApplicant);
            this.Controls.Add(this.checkEdit_AppliesApplicant);
            this.Controls.Add(this.checkEdit_RequiresProperty);
            this.Controls.Add(this.checkEdit_Default);
            this.Controls.Add(this.checkEdit_ActiveFlag);
            this.Controls.Add(this.textEdit_assembly);
            this.Controls.Add(this.textEdit_description);
            this.Controls.Add(this.LabelControl_ID);
            this.Controls.Add(this.LabelControl1);
            this.Name = "EditForm";
            this.Text = "Disclosure Type";
            this.Controls.SetChildIndex(this.LabelControl1, 0);
            this.Controls.SetChildIndex(this.LabelControl_ID, 0);
            this.Controls.SetChildIndex(this.simpleButton_OK, 0);
            this.Controls.SetChildIndex(this.simpleButton_Cancel, 0);
            this.Controls.SetChildIndex(this.textEdit_description, 0);
            this.Controls.SetChildIndex(this.textEdit_assembly, 0);
            this.Controls.SetChildIndex(this.checkEdit_ActiveFlag, 0);
            this.Controls.SetChildIndex(this.checkEdit_Default, 0);
            this.Controls.SetChildIndex(this.checkEdit_RequiresProperty, 0);
            this.Controls.SetChildIndex(this.checkEdit_AppliesApplicant, 0);
            this.Controls.SetChildIndex(this.checkEdit_AppliesCoApplicant, 0);
            this.Controls.SetChildIndex(this.labelControl2, 0);
            this.Controls.SetChildIndex(this.labelControl3, 0);
            this.Controls.SetChildIndex(this.disclosureLanguageGridControl1, 0);
            ((System.ComponentModel.ISupportInitialize)(this.DxErrorProvider1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit_description.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit_assembly.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit_ActiveFlag.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit_Default.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit_RequiresProperty.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit_AppliesApplicant.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit_AppliesCoApplicant.Properties)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

		}
		private DevExpress.XtraEditors.LabelControl LabelControl1;
        private DevExpress.XtraEditors.LabelControl LabelControl_ID;
        private DevExpress.XtraEditors.TextEdit textEdit_description;
        private DevExpress.XtraEditors.TextEdit textEdit_assembly;
        private DevExpress.XtraEditors.CheckEdit checkEdit_ActiveFlag;
        private DevExpress.XtraEditors.CheckEdit checkEdit_Default;
        private DevExpress.XtraEditors.CheckEdit checkEdit_RequiresProperty;
        private DevExpress.XtraEditors.CheckEdit checkEdit_AppliesApplicant;
        private DevExpress.XtraEditors.CheckEdit checkEdit_AppliesCoApplicant;
        private DevExpress.XtraEditors.LabelControl labelControl2;
        private DevExpress.XtraEditors.LabelControl labelControl3;
        private ClientDisclosureTypes.DisclosureLanguageGridControl disclosureLanguageGridControl1;
	}
}
