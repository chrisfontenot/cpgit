#region "Copyright 2000-2012 DebtPlus, L.L.C."
//{*******************************************************************}
//{                                                                   }
//{       DebtPlus Debt Management System                             }
//{                                                                   }
//{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
//{       ALL RIGHTS RESERVED                                         }
//{                                                                   }
//{   The entire contents of this file is protected by U.S. and       }
//{   International Copyright Laws. Unauthorized reproduction,        }
//{   reverse-engineering, and distribution of all or any portion of  }
//{   the code contained in this file is strictly prohibited and may  }
//{   result in severe civil and criminal penalties and will be       }
//{   prosecuted to the maximum extent possible under the law.        }
//{                                                                   }
//{   RESTRICTIONS                                                    }
//{                                                                   }
//{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
//{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
//{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
//{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
//{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
//{                                                                   }
//{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
//{   ADDITIONAL RESTRICTIONS.                                        }
//{                                                                   }
//{*******************************************************************}

#endregion "Copyright 2000-2012 DebtPlus, L.L.C."

using System;
using DebtPlus.LINQ;

namespace DebtPlus.UI.TableAdministration.CS.ClentDisclosureTypes
{
    internal partial class EditForm : Templates.EditTemplateForm
    {
        private BusinessContext bc = null;
        private client_DisclosureType DisclosureTypeRecord = null;

        /// <summary>
        /// Initialize the new class
        /// </summary>
        internal EditForm()
            : base()
        {
            InitializeComponent();
        }

        /// <summary>
        /// Initialize the new class
        /// </summary>
        internal EditForm(BusinessContext bc, client_DisclosureType DisclosureTypeRecord)
            : this()
        {
            this.DisclosureTypeRecord = DisclosureTypeRecord;
            this.bc = bc;
            RegisterHandlers();
        }

        /// <summary>
        /// Register the event handlers
        /// </summary>
        private void RegisterHandlers()
        {
            Load += EditForm_Load;
            textEdit_description.EditValueChanged += Form_Changed;
            textEdit_assembly.EditValueChanged += Form_Changed;
            checkEdit_ActiveFlag.CheckedChanged += Form_Changed;
            checkEdit_AppliesApplicant.CheckedChanged += Form_Changed;
            checkEdit_AppliesCoApplicant.CheckedChanged += Form_Changed;
            checkEdit_Default.CheckedChanged += Form_Changed;
            checkEdit_RequiresProperty.CheckedChanged += Form_Changed;
        }

        /// <summary>
        /// Remove the event registrations
        /// </summary>
        private void UnRegisterHandlers()
        {
            Load -= EditForm_Load;
            textEdit_description.EditValueChanged -= Form_Changed;
            textEdit_assembly.EditValueChanged -= Form_Changed;
            checkEdit_ActiveFlag.CheckedChanged -= Form_Changed;
            checkEdit_AppliesApplicant.CheckedChanged -= Form_Changed;
            checkEdit_AppliesCoApplicant.CheckedChanged -= Form_Changed;
            checkEdit_Default.CheckedChanged -= Form_Changed;
            checkEdit_RequiresProperty.CheckedChanged -= Form_Changed;
        }

        /// <summary>
        /// Process the LOAD event
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void EditForm_Load(object sender, EventArgs e)
        {
            UnRegisterHandlers();
            try
            {
                LabelControl_ID.Text = DisclosureTypeRecord.Id < 1 ? "NEW" : DisclosureTypeRecord.Id.ToString();

                textEdit_assembly.EditValue = DisclosureTypeRecord.Assembly;
                textEdit_description.EditValue = DisclosureTypeRecord.description;

                checkEdit_ActiveFlag.Checked = DisclosureTypeRecord.ActiveFlag;
                checkEdit_AppliesApplicant.Checked = DisclosureTypeRecord.applies_applicant;
                checkEdit_AppliesCoApplicant.Checked = DisclosureTypeRecord.applies_coapplicant;
                checkEdit_Default.Checked = DisclosureTypeRecord.Default;
                checkEdit_RequiresProperty.Checked = DisclosureTypeRecord.required_propertyID;

                // Display the language information items
                disclosureLanguageGridControl1.ReadForm(bc, DisclosureTypeRecord);

                simpleButton_OK.Enabled = !HasErrors();
            }
            finally
            {
                RegisterHandlers();
            }
        }

        /// <summary>
        /// Determine if the controls have any errors for the record
        /// </summary>
        /// <returns></returns>
        private bool HasErrors()
        {
            // The description is required
            if (string.IsNullOrWhiteSpace(DebtPlus.Utils.Nulls.v_String(textEdit_description.EditValue)))
            {
                return true;
            }

            // The assembly is required
            if (string.IsNullOrWhiteSpace(DebtPlus.Utils.Nulls.v_String(textEdit_assembly.EditValue)))
            {
                return true;
            }

            return false;
        }

        /// <summary>
        /// Process a change in the form controls
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Form_Changed(object sender, EventArgs e)
        {
            simpleButton_OK.Enabled = !HasErrors();
        }

        /// <summary>
        /// Handle the CLICK event on the OK button
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected override void simpleButton_OK_Click(object sender, EventArgs e)
        {
            base.simpleButton_OK_Click(sender, e);

            DisclosureTypeRecord.Assembly = DebtPlus.Utils.Nulls.v_String(textEdit_assembly.EditValue);
            DisclosureTypeRecord.description = DebtPlus.Utils.Nulls.v_String(textEdit_description.EditValue);

            DisclosureTypeRecord.ActiveFlag = checkEdit_ActiveFlag.Checked;
            DisclosureTypeRecord.applies_applicant = checkEdit_AppliesApplicant.Checked;
            DisclosureTypeRecord.applies_coapplicant = checkEdit_AppliesCoApplicant.Checked;
            DisclosureTypeRecord.Default = checkEdit_Default.Checked;
            DisclosureTypeRecord.required_propertyID = checkEdit_RequiresProperty.Checked;
        }
    }
}