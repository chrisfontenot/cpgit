#region "Copyright 2000-2012 DebtPlus, L.L.C."
//{*******************************************************************}
//{                                                                   }
//{       DebtPlus Debt Management System                             }
//{                                                                   }
//{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
//{       ALL RIGHTS RESERVED                                         }
//{                                                                   }
//{   The entire contents of this file is protected by U.S. and       }
//{   International Copyright Laws. Unauthorized reproduction,        }
//{   reverse-engineering, and distribution of all or any portion of  }
//{   the code contained in this file is strictly prohibited and may  }
//{   result in severe civil and criminal penalties and will be       }
//{   prosecuted to the maximum extent possible under the law.        }
//{                                                                   }
//{   RESTRICTIONS                                                    }
//{                                                                   }
//{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
//{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
//{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
//{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
//{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
//{                                                                   }
//{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
//{   ADDITIONAL RESTRICTIONS.                                        }
//{                                                                   }
//{*******************************************************************}

#endregion "Copyright 2000-2012 DebtPlus, L.L.C."

using System;
using DebtPlus.LINQ;

namespace DebtPlus.UI.TableAdministration.CS.ClientDisclosureTypes
{
    internal partial class DisclosureLanguageTypeForm : Templates.EditTemplateForm
    {
        private client_DisclosureLanguageType DisclosureLanguageTypeRecord = null;
        private BusinessContext bc = null;

        /// <summary>
        /// Initialize the new class
        /// </summary>
        internal DisclosureLanguageTypeForm()
            : base()
        {
            InitializeComponent();
        }

        /// <summary>
        /// Initialize the new class
        /// </summary>
        internal DisclosureLanguageTypeForm(BusinessContext bc, client_DisclosureLanguageType DisclosureLanguageTypeRecord)
            : this()
        {
            this.bc = bc;
            this.DisclosureLanguageTypeRecord = DisclosureLanguageTypeRecord;
            RegisterHandlers();
        }

        /// <summary>
        /// Register the event handlers
        /// </summary>
        private void RegisterHandlers()
        {
            this.Load += EditForm_Load;
        }

        /// <summary>
        /// Remove the event registrations
        /// </summary>
        private void UnRegisterHandlers()
        {
            this.Load -= EditForm_Load;
        }

        /// <summary>
        /// Process the LOAD event
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void EditForm_Load(object sender, EventArgs e)
        {
            UnRegisterHandlers();
            try
            {
                lookUpEdit_languageID.Properties.DataSource = DebtPlus.LINQ.Cache.AttributeType.getLanguageList();

                LabelControl_ID.Text = DisclosureLanguageTypeRecord.Id < 1 ? "NEW" : DisclosureLanguageTypeRecord.Id.ToString();

                textEdit_uri.EditValue = DisclosureLanguageTypeRecord.template_url;
                lookUpEdit_languageID.EditValue = DisclosureLanguageTypeRecord.languageID;
                dateEdit_revision_date.EditValue = DisclosureLanguageTypeRecord.revision_date;
                dateEdit_effective.EditValue = DisclosureLanguageTypeRecord.effective_date;
                dateEdit_expires.EditValue = DisclosureLanguageTypeRecord.expiration_date;

                simpleButton_OK.Enabled = !HasErrors();
            }
            finally
            {
                RegisterHandlers();
            }
        }

        /// <summary>
        /// Determine if the controls have any errors for the record
        /// </summary>
        /// <returns></returns>
        private bool HasErrors()
        {
            return false;
        }

        /// <summary>
        /// Process a change in the form controls
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Form_Changed(object sender, EventArgs e)
        {
            simpleButton_OK.Enabled = !HasErrors();
        }

        /// <summary>
        /// Handle the CLICK event on the OK button
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected override void simpleButton_OK_Click(object sender, EventArgs e)
        {
            try
            {
                System.Uri fileURI = new System.Uri(textEdit_uri.Text.Trim());
                DisclosureLanguageTypeRecord.template_url = fileURI.ToString();
            }
            catch
            {
                DisclosureLanguageTypeRecord.template_url = textEdit_uri.Text.Trim();
            }

            DisclosureLanguageTypeRecord.languageID = DebtPlus.Utils.Nulls.v_Int32(lookUpEdit_languageID.EditValue).GetValueOrDefault(1);
            DisclosureLanguageTypeRecord.revision_date = DebtPlus.Utils.Nulls.v_DateTime(dateEdit_revision_date.EditValue).GetValueOrDefault();
            DisclosureLanguageTypeRecord.effective_date = DebtPlus.Utils.Nulls.v_DateTime(dateEdit_effective.EditValue).GetValueOrDefault();
            DisclosureLanguageTypeRecord.expiration_date = DebtPlus.Utils.Nulls.v_DateTime(dateEdit_expires.EditValue);

            base.simpleButton_OK_Click(sender, e);
        }
    }
}