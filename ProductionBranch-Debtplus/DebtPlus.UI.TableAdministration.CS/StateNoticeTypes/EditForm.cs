﻿using System;

namespace DebtPlus.UI.TableAdministration.CS.StateNoticeTypes
{
    internal partial class EditForm : Templates.EditTemplateForm
    {
        private DebtPlus.LINQ.StateMessageType record;

        internal EditForm()
            : base()
        {
            InitializeComponent();
        }

        internal EditForm(DebtPlus.LINQ.StateMessageType record)
            : this()
        {
            this.record = record;
            RegisterHandlers();
        }

        private void RegisterHandlers()
        {
            Load += EditForm_Load;
            TextEdit_description.EditValueChanging += Form_Changing;
        }

        private void UnRegisterHandlers()
        {
            Load -= EditForm_Load;
            TextEdit_description.EditValueChanging -= Form_Changing;
        }

        private void EditForm_Load(object sender, EventArgs e)
        {
            UnRegisterHandlers();
            try
            {
                LabelControl_id.Text = record.Id <= 0 ? "NEW" : record.Id.ToString();
                TextEdit_description.EditValue = record.Description;
                CheckEdit_default.Checked = record.Default;
                checkEdit_ActiveFlag.Checked = record.ActiveFlag;
            }
            finally
            {
                RegisterHandlers();
            }
            simpleButton_OK.Enabled = !string.IsNullOrEmpty((string)TextEdit_description.EditValue);
        }

        private void Form_Changing(object sender, DevExpress.XtraEditors.Controls.ChangingEventArgs e)
        {
            simpleButton_OK.Enabled = !string.IsNullOrEmpty((string)e.NewValue);
        }

        protected override void simpleButton_OK_Click(object sender, EventArgs e)
        {
            base.simpleButton_OK_Click(sender, e);

            record.Description = Convert.ToString(TextEdit_description.EditValue) ?? string.Empty;
            record.Default = CheckEdit_default.Checked;
            record.ActiveFlag = checkEdit_ActiveFlag.Checked;
        }
    }
}