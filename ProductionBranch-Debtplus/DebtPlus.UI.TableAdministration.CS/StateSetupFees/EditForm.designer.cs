﻿namespace DebtPlus.UI.TableAdministration.CS.StateSetupFees
{
    partial class EditForm
    {
		//Form overrides dispose to clean up the component list.
		[System.Diagnostics.DebuggerNonUserCode]
		protected override void Dispose(bool disposing)
		{
			try 
            {
				if (disposing)
                {
                    if (components != null) components.Dispose();
				}
                components = null;
            }
    	    finally
            {
				base.Dispose(disposing);
			}
		}

		//Required by the Windows Form Designer
		private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.LayoutControl1 = new DevExpress.XtraLayout.LayoutControl();
            this.dateEdit_expires_date = new DevExpress.XtraEditors.DateEdit();
            this.dateEdit_effective_date = new DevExpress.XtraEditors.DateEdit();
            this.calcEdit_amount = new DevExpress.XtraEditors.CalcEdit();
            this.lookUpEdit_state = new DevExpress.XtraEditors.LookUpEdit();
            this.LabelControl_ID = new DevExpress.XtraEditors.LabelControl();
            this.LayoutControlGroup1 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.LayoutControlItem1 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem2 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem3 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem4 = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem1 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.layoutControlItem5 = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem3 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.emptySpaceItem4 = new DevExpress.XtraLayout.EmptySpaceItem();
            ((System.ComponentModel.ISupportInitialize)(this.DxErrorProvider1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LayoutControl1)).BeginInit();
            this.LayoutControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dateEdit_expires_date.Properties.VistaTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEdit_expires_date.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEdit_effective_date.Properties.VistaTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEdit_effective_date.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.calcEdit_amount.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lookUpEdit_state.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LayoutControlGroup1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LayoutControlItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem4)).BeginInit();
            this.SuspendLayout();
            // 
            // simpleButton_OK
            // 
            this.simpleButton_OK.Location = new System.Drawing.Point(254, 31);
            // 
            // simpleButton_Cancel
            // 
            this.simpleButton_Cancel.Location = new System.Drawing.Point(254, 74);
            // 
            // DefaultLookAndFeel1
            // 
            this.DefaultLookAndFeel1.LookAndFeel.SkinName = "McSkin";
            // 
            // LayoutControl1
            // 
            this.LayoutControl1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.LayoutControl1.Controls.Add(this.dateEdit_expires_date);
            this.LayoutControl1.Controls.Add(this.dateEdit_effective_date);
            this.LayoutControl1.Controls.Add(this.calcEdit_amount);
            this.LayoutControl1.Controls.Add(this.lookUpEdit_state);
            this.LayoutControl1.Controls.Add(this.LabelControl_ID);
            this.LayoutControl1.Location = new System.Drawing.Point(-5, -5);
            this.LayoutControl1.Name = "LayoutControl1";
            this.LayoutControl1.Root = this.LayoutControlGroup1;
            this.LayoutControl1.Size = new System.Drawing.Size(252, 139);
            this.LayoutControl1.TabIndex = 24;
            this.LayoutControl1.Text = "LayoutControl1";
            // 
            // dateEdit_expires_date
            // 
            this.dateEdit_expires_date.EditValue = null;
            this.dateEdit_expires_date.Location = new System.Drawing.Point(58, 101);
            this.dateEdit_expires_date.Name = "dateEdit_expires_date";
            this.dateEdit_expires_date.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.True;
            this.dateEdit_expires_date.Properties.Appearance.Options.UseTextOptions = true;
            this.dateEdit_expires_date.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.dateEdit_expires_date.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dateEdit_expires_date.Properties.VistaTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.dateEdit_expires_date.Size = new System.Drawing.Size(113, 20);
            this.dateEdit_expires_date.StyleController = this.LayoutControl1;
            this.dateEdit_expires_date.TabIndex = 7;
            // 
            // dateEdit_effective_date
            // 
            this.dateEdit_effective_date.EditValue = null;
            this.dateEdit_effective_date.Location = new System.Drawing.Point(58, 77);
            this.dateEdit_effective_date.Name = "dateEdit_effective_date";
            this.dateEdit_effective_date.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.False;
            this.dateEdit_effective_date.Properties.Appearance.Options.UseTextOptions = true;
            this.dateEdit_effective_date.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.dateEdit_effective_date.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dateEdit_effective_date.Properties.VistaTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.dateEdit_effective_date.Size = new System.Drawing.Size(113, 20);
            this.dateEdit_effective_date.StyleController = this.LayoutControl1;
            this.dateEdit_effective_date.TabIndex = 6;
            // 
            // calcEdit_amount
            // 
            this.calcEdit_amount.Location = new System.Drawing.Point(58, 53);
            this.calcEdit_amount.Name = "calcEdit_amount";
            this.calcEdit_amount.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.False;
            this.calcEdit_amount.Properties.Appearance.Options.UseTextOptions = true;
            this.calcEdit_amount.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.calcEdit_amount.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.calcEdit_amount.Properties.DisplayFormat.FormatString = "C2";
            this.calcEdit_amount.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.calcEdit_amount.Properties.EditFormat.FormatString = "C2";
            this.calcEdit_amount.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.calcEdit_amount.Properties.Mask.EditMask = "c";
            this.calcEdit_amount.Properties.Precision = 2;
            this.calcEdit_amount.Size = new System.Drawing.Size(113, 20);
            this.calcEdit_amount.StyleController = this.LayoutControl1;
            this.calcEdit_amount.TabIndex = 5;
            // 
            // lookUpEdit_state
            // 
            this.lookUpEdit_state.Location = new System.Drawing.Point(58, 29);
            this.lookUpEdit_state.Name = "lookUpEdit_state";
            this.lookUpEdit_state.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.False;
            this.lookUpEdit_state.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.lookUpEdit_state.Properties.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("Id", "ID", 20, DevExpress.Utils.FormatType.Numeric, "f0", false, DevExpress.Utils.HorzAlignment.Default),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("Name", "Name", 20, DevExpress.Utils.FormatType.None, "", true, DevExpress.Utils.HorzAlignment.Default, DevExpress.Data.ColumnSortOrder.Ascending, DevExpress.Utils.DefaultBoolean.True)});
            this.lookUpEdit_state.Properties.DisplayMember = "Name";
            this.lookUpEdit_state.Properties.NullText = "";
            this.lookUpEdit_state.Properties.ShowFooter = false;
            this.lookUpEdit_state.Properties.ShowHeader = false;
            this.lookUpEdit_state.Properties.ValueMember = "Id";
            this.lookUpEdit_state.Size = new System.Drawing.Size(182, 20);
            this.lookUpEdit_state.StyleController = this.LayoutControl1;
            this.lookUpEdit_state.TabIndex = 4;
            this.lookUpEdit_state.ToolTip = "The appropriate state for the fee";
            // 
            // LabelControl_ID
            // 
            this.LabelControl_ID.Location = new System.Drawing.Point(58, 12);
            this.LabelControl_ID.Name = "LabelControl_ID";
            this.LabelControl_ID.Size = new System.Drawing.Size(23, 13);
            this.LabelControl_ID.StyleController = this.LayoutControl1;
            this.LabelControl_ID.TabIndex = 1;
            this.LabelControl_ID.Text = "NEW";
            // 
            // LayoutControlGroup1
            // 
            this.LayoutControlGroup1.CustomizationFormText = "LayoutControlGroup1";
            this.LayoutControlGroup1.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.True;
            this.LayoutControlGroup1.GroupBordersVisible = false;
            this.LayoutControlGroup1.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.LayoutControlItem1,
            this.layoutControlItem2,
            this.layoutControlItem3,
            this.layoutControlItem4,
            this.emptySpaceItem1,
            this.layoutControlItem5,
            this.emptySpaceItem3,
            this.emptySpaceItem4});
            this.LayoutControlGroup1.Location = new System.Drawing.Point(0, 0);
            this.LayoutControlGroup1.Name = "LayoutControlGroup1";
            this.LayoutControlGroup1.Size = new System.Drawing.Size(252, 139);
            this.LayoutControlGroup1.Text = "LayoutControlGroup1";
            this.LayoutControlGroup1.TextVisible = false;
            // 
            // LayoutControlItem1
            // 
            this.LayoutControlItem1.Control = this.LabelControl_ID;
            this.LayoutControlItem1.CustomizationFormText = "ID";
            this.LayoutControlItem1.Location = new System.Drawing.Point(0, 0);
            this.LayoutControlItem1.Name = "LayoutControlItem1";
            this.LayoutControlItem1.Size = new System.Drawing.Size(232, 17);
            this.LayoutControlItem1.Text = "ID";
            this.LayoutControlItem1.TextSize = new System.Drawing.Size(43, 13);
            // 
            // layoutControlItem2
            // 
            this.layoutControlItem2.Control = this.lookUpEdit_state;
            this.layoutControlItem2.CustomizationFormText = "State";
            this.layoutControlItem2.Location = new System.Drawing.Point(0, 17);
            this.layoutControlItem2.Name = "layoutControlItem2";
            this.layoutControlItem2.Size = new System.Drawing.Size(232, 24);
            this.layoutControlItem2.Text = "State";
            this.layoutControlItem2.TextSize = new System.Drawing.Size(43, 13);
            // 
            // layoutControlItem3
            // 
            this.layoutControlItem3.Control = this.calcEdit_amount;
            this.layoutControlItem3.CustomizationFormText = "Amount";
            this.layoutControlItem3.Location = new System.Drawing.Point(0, 41);
            this.layoutControlItem3.Name = "layoutControlItem3";
            this.layoutControlItem3.Size = new System.Drawing.Size(163, 24);
            this.layoutControlItem3.Text = "Amount";
            this.layoutControlItem3.TextSize = new System.Drawing.Size(43, 13);
            // 
            // layoutControlItem4
            // 
            this.layoutControlItem4.Control = this.dateEdit_effective_date;
            this.layoutControlItem4.CustomizationFormText = "Effective";
            this.layoutControlItem4.Location = new System.Drawing.Point(0, 65);
            this.layoutControlItem4.Name = "layoutControlItem4";
            this.layoutControlItem4.Size = new System.Drawing.Size(163, 24);
            this.layoutControlItem4.Text = "Effective";
            this.layoutControlItem4.TextSize = new System.Drawing.Size(43, 13);
            // 
            // emptySpaceItem1
            // 
            this.emptySpaceItem1.AllowHotTrack = false;
            this.emptySpaceItem1.CustomizationFormText = "emptySpaceItem1";
            this.emptySpaceItem1.Location = new System.Drawing.Point(163, 41);
            this.emptySpaceItem1.Name = "emptySpaceItem1";
            this.emptySpaceItem1.Size = new System.Drawing.Size(69, 24);
            this.emptySpaceItem1.Text = "emptySpaceItem1";
            this.emptySpaceItem1.TextSize = new System.Drawing.Size(0, 0);
            // 
            // layoutControlItem5
            // 
            this.layoutControlItem5.Control = this.dateEdit_expires_date;
            this.layoutControlItem5.CustomizationFormText = "Expires";
            this.layoutControlItem5.Location = new System.Drawing.Point(0, 89);
            this.layoutControlItem5.Name = "layoutControlItem5";
            this.layoutControlItem5.Size = new System.Drawing.Size(163, 30);
            this.layoutControlItem5.Text = "Expires";
            this.layoutControlItem5.TextSize = new System.Drawing.Size(43, 13);
            // 
            // emptySpaceItem3
            // 
            this.emptySpaceItem3.AllowHotTrack = false;
            this.emptySpaceItem3.CustomizationFormText = "emptySpaceItem3";
            this.emptySpaceItem3.Location = new System.Drawing.Point(163, 65);
            this.emptySpaceItem3.Name = "emptySpaceItem3";
            this.emptySpaceItem3.Size = new System.Drawing.Size(69, 24);
            this.emptySpaceItem3.Text = "emptySpaceItem3";
            this.emptySpaceItem3.TextSize = new System.Drawing.Size(0, 0);
            // 
            // emptySpaceItem4
            // 
            this.emptySpaceItem4.AllowHotTrack = false;
            this.emptySpaceItem4.CustomizationFormText = "emptySpaceItem4";
            this.emptySpaceItem4.Location = new System.Drawing.Point(163, 89);
            this.emptySpaceItem4.Name = "emptySpaceItem4";
            this.emptySpaceItem4.Size = new System.Drawing.Size(69, 30);
            this.emptySpaceItem4.Text = "emptySpaceItem4";
            this.emptySpaceItem4.TextSize = new System.Drawing.Size(0, 0);
            // 
            // EditForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(350, 130);
            this.Controls.Add(this.LayoutControl1);
            this.Name = "EditForm";
            this.Text = "State Setup Fee";
            this.Controls.SetChildIndex(this.simpleButton_OK, 0);
            this.Controls.SetChildIndex(this.simpleButton_Cancel, 0);
            this.Controls.SetChildIndex(this.LayoutControl1, 0);
            ((System.ComponentModel.ISupportInitialize)(this.DxErrorProvider1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LayoutControl1)).EndInit();
            this.LayoutControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dateEdit_expires_date.Properties.VistaTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEdit_expires_date.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEdit_effective_date.Properties.VistaTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEdit_effective_date.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.calcEdit_amount.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lookUpEdit_state.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LayoutControlGroup1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LayoutControlItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem4)).EndInit();
            this.ResumeLayout(false);

        }


        private DevExpress.XtraLayout.LayoutControl LayoutControl1;
        private DevExpress.XtraEditors.LabelControl LabelControl_ID;
        private DevExpress.XtraLayout.LayoutControlGroup LayoutControlGroup1;
        private DevExpress.XtraLayout.LayoutControlItem LayoutControlItem1;
        private DevExpress.XtraEditors.DateEdit dateEdit_expires_date;
        private DevExpress.XtraEditors.DateEdit dateEdit_effective_date;
        private DevExpress.XtraEditors.CalcEdit calcEdit_amount;
        private DevExpress.XtraEditors.LookUpEdit lookUpEdit_state;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem2;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem3;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem4;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem1;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem5;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem3;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem4;
    }
}