﻿using System;
using System.Linq;
using DebtPlus.LINQ;

namespace DebtPlus.UI.TableAdministration.CS.StateSetupFees
{
    public partial class MainForm : Templates.MainForm
    {
        /// <summary>
        /// List of the records to be edited in the program
        /// </summary>
        private System.Collections.Generic.List<StateSetupFee> colRecords = null;

        private BusinessContext bc = new BusinessContext();

        /// <summary>
        /// Initialize the new class
        /// </summary>
        public MainForm()
            : base()
        {
            InitializeComponent();
            RegisterHandlers();
        }

        /// <summary>
        /// Register the event handlers
        /// </summary>
        private void RegisterHandlers()
        {
            Load += MainForm_Load;
            checkEdit_ShowAll.CheckedChanged += checkEdit_ShowAll_CheckedChanged;
            gridView1.CustomRowFilter += gridView1_CustomRowFilter;
        }

        /// <summary>
        /// Remove the event handler registrations
        /// </summary>
        private void UnRegisterHandlers()
        {
            Load -= MainForm_Load;
            checkEdit_ShowAll.CheckedChanged -= checkEdit_ShowAll_CheckedChanged;
            gridView1.CustomRowFilter -= gridView1_CustomRowFilter;
        }

        /// <summary>
        /// Process a change in the checkbox to show all items
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void checkEdit_ShowAll_CheckedChanged(object sender, EventArgs e)
        {
            // Redraw the grid. This will invoke the filter and sorting functions again.
            gridView1.RefreshData();
        }

        /// <summary>
        /// Since we can't properly filter on NULL values in the filter string, do it with a row filter.
        /// </summary>
        private void gridView1_CustomRowFilter(object sender, DevExpress.XtraGrid.Views.Base.RowFilterEventArgs e)
        {
            // If no data or the "show all" is selected then accept all rows in the list.
            if (checkEdit_ShowAll.Checked || colRecords == null)
            {
                return;
            }

            // Find the record that we are displaying
            var record = colRecords[e.ListSourceRow];

            // Look at the record to determine if the item is acceptable
            var now = DateTime.Now.Date;

            // If the record is effective beyond today then we can't accept it.
            if (record.effective_date.Date > now)
            {
                e.Visible = false;
                e.Handled = true;
                return;
            }

            // If there is a cutoff date and it is before today then we can't accept the record.
            if (record.cutoff_date.HasValue && record.cutoff_date.Value.Date < now)
            {
                e.Visible = false;
                e.Handled = true;
                return;
            }
        }

        /// <summary>
        /// Process the load event on the form
        /// </summary>
        private void MainForm_Load(object sender, System.EventArgs e)
        {
            // Set the formatting for the state ID to name
            gridColumn_state.DisplayFormat.Format = new StateFormatter();
            gridColumn_state.GroupFormat.Format = new StateFormatter();

            UnRegisterHandlers();
            try
            {
                // Retrieve the list of menu items
                colRecords = bc.StateSetupFees.ToList();
                gridControl1.DataSource = colRecords;
            }
            catch (Exception ex)
            {
                DebtPlus.UI.Common.ErrorHandling.HandleErrors(ex);
            }
            finally
            {
                RegisterHandlers();
            }

            // Once the event handlers are in place, refresh the grid control to select the items.
            gridView1.RefreshData();
        }

        /// <summary>
        /// Create the new record
        /// </summary>
        protected override void CreateRecord()
        {
            // Allocate the new record to be inserted.
            var record = DebtPlus.LINQ.Factory.Manufacture_StateSetupFee();

            // Edit the new record. If OK then attempt to insert the record.
            using (var frm = new EditForm(record))
            {
                if (frm.ShowDialog() != System.Windows.Forms.DialogResult.OK)
                {
                    return;
                }
            }

            try
            {
                // Add the record to the collection
                bc.StateSetupFees.InsertOnSubmit(record);
                bc.SubmitChanges();
                colRecords.Add(record);
                gridView1.RefreshData();
            }
            catch (System.Data.SqlClient.SqlException ex)
            {
                DebtPlus.UI.Common.ErrorHandling.HandleErrors(ex);
            }
        }

        /// <summary>
        /// Update the existing record
        /// </summary>
        /// <param name="obj">Pointer to the desired record for the operation</param>
        protected override void UpdateRecord(object obj)
        {
            // Find the record
            var record = obj as StateSetupFee;
            if (obj == null)
            {
                return;
            }

            // Update the record now.
            using (var frm = new EditForm(record))
            {
                if (frm.ShowDialog() != System.Windows.Forms.DialogResult.OK)
                {
                    return;
                }
            }

            try
            {
                bc.SubmitChanges();
                gridView1.RefreshData();
            }
            catch (System.Data.SqlClient.SqlException ex)
            {
                DebtPlus.UI.Common.ErrorHandling.HandleErrors(ex);
            }
        }

        /// <summary>
        /// Delete the indicated record from the database
        /// </summary>
        /// <param name="obj">Pointer to the desired record for the operation</param>
        protected override void DeleteRecord(object obj)
        {
            // Find the record
            var record = obj as StateSetupFee;
            if (obj == null)
            {
                return;
            }

            if (DebtPlus.Data.Prompts.RequestConfirmation_Delete() != System.Windows.Forms.DialogResult.Yes)
            {
                return;
            }

            try
            {
                bc.StateSetupFees.DeleteOnSubmit(record);
                bc.SubmitChanges();

                colRecords.Remove(record);
                gridView1.RefreshData();
            }
            catch (System.Data.SqlClient.SqlException ex)
            {
                DebtPlus.UI.Common.ErrorHandling.HandleErrors(ex);
            }
        }

        /// <summary>
        /// Private class to format the state name given the state ID
        /// </summary>
        internal class StateFormatter : System.IFormatProvider, System.ICustomFormatter
        {
            /// <summary>
            /// Initialize the new class structure
            /// </summary>
            internal StateFormatter() { }

            /// <summary>
            /// Determine which routines will format items of a specific type
            /// </summary>
            /// <param name="formatType"></param>
            /// <returns></returns>
            public object GetFormat(Type formatType)
            {
                return this;
            }

            /// <summary>
            /// Format the number value to a suitable string. We use the states table to find the name.
            /// </summary>
            public string Format(string format, object arg, IFormatProvider formatProvider)
            {
                var key = DebtPlus.Utils.Nulls.v_Int32(arg);
                if (key.HasValue)
                {
                    var q = DebtPlus.LINQ.Cache.state.getList().Find(s => s.Id == key.Value);
                    if (q != null)
                    {
                        return q.Name;
                    }
                    return "Unknown State ID";
                }
                return string.Empty;
            }
        }
    }
}