﻿using System;
using DebtPlus.LINQ;

namespace DebtPlus.UI.TableAdministration.CS.StateSetupFees
{
    /// <summary>
    /// Form to edit the state setup fee table row
    /// </summary>
    internal partial class EditForm : Templates.EditTemplateForm
    {
        // Local storage
        private StateSetupFee record = null;

        /// <summary>
        /// Initialize a new instance of our form class
        /// </summary>
        internal EditForm()
            : base()
        {
            InitializeComponent();
        }

        /// <summary>
        /// Initialize a new instance of our form class
        /// </summary>
        internal EditForm(StateSetupFee record)
            : this()
        {
            this.record = record;
            RegisterHandlers();
        }

        /// <summary>
        /// Register the event handlers
        /// </summary>
        private void RegisterHandlers()
        {
            Load                                     += EditForm_Load;
            lookUpEdit_state.EditValueChanged        += Form_Changed;
            calcEdit_amount.EditValueChanged         += Form_Changed;
            dateEdit_expires_date.EditValueChanged   += Form_Changed;
            dateEdit_effective_date.EditValueChanged += Form_Changed;
            lookUpEdit_state.EditValueChanging       += DebtPlus.Data.Validation.LookUpEdit_ActiveTest;
            calcEdit_amount.Validating               += DebtPlus.Data.Validation.NonNegative;
            dateEdit_expires_date.Validating         += CheckDateLimit;
            dateEdit_effective_date.Validating       += CheckDateLimit;
        }

        /// <summary>
        /// Remove the event handler registrations
        /// </summary>
        private void UnRegisterHandlers()
        {
            Load                                     -= EditForm_Load;
            lookUpEdit_state.EditValueChanged        -= Form_Changed;
            calcEdit_amount.EditValueChanged         -= Form_Changed;
            dateEdit_expires_date.EditValueChanged   -= Form_Changed;
            dateEdit_effective_date.EditValueChanged -= Form_Changed;
            lookUpEdit_state.EditValueChanging       -= DebtPlus.Data.Validation.LookUpEdit_ActiveTest;
            calcEdit_amount.Validating               -= DebtPlus.Data.Validation.NonNegative;
            dateEdit_expires_date.Validating         -= CheckDateLimit;
            dateEdit_effective_date.Validating       -= CheckDateLimit;
        }

        /// <summary>
        /// Handle the LOAD sequence for the form
        /// </summary>
        private void EditForm_Load(object sender, EventArgs e)
        {
            UnRegisterHandlers();
            try
            {
                // Set the list of state codes
                lookUpEdit_state.Properties.DataSource = DebtPlus.LINQ.Cache.state.getList();

                // Define the other field controls
                lookUpEdit_state.EditValue        = record.state;
                calcEdit_amount.EditValue         = record.amount;
                dateEdit_effective_date.EditValue = record.effective_date;
                dateEdit_expires_date.EditValue   = record.cutoff_date;

                // The ID of the record is just a display field
                LabelControl_ID.Text = record.Id <= 0 ? "NEW" : record.Id.ToString();

                simpleButton_OK.Enabled = !HasErrors();
            }
            finally
            {
                RegisterHandlers();
            }
        }

        /// <summary>
        /// Process a change in the form controls
        /// </summary>
        private void Form_Changed(object sender, EventArgs e)
        {
            simpleButton_OK.Enabled = !HasErrors();
        }

        /// <summary>
        /// Determine if the controls reflect enough to create a new record
        /// </summary>
        private bool HasErrors()
        {
            // The state is required.
            if (!DebtPlus.Utils.Nulls.v_Int32(lookUpEdit_state.EditValue).HasValue)
            {
                return true;
            }

            // The dates must be in the proper order. The starting must occur before the ending value.
            var endingDate = DebtPlus.Utils.Nulls.v_DateTime(dateEdit_expires_date.EditValue);
            var startingDate = DebtPlus.Utils.Nulls.v_DateTime(dateEdit_effective_date.EditValue);
            if (startingDate.HasValue && endingDate.HasValue)
            {
                if (startingDate.Value.Date > endingDate.Value.Date)
                {
                    return true;
                }
            }

            // The amount must not be negative
            var newValue = DebtPlus.Utils.Nulls.v_Decimal(calcEdit_amount.EditValue).GetValueOrDefault();
            if (newValue < 0M)
            {
                return true;
            }

            return false;
        }

        /// <summary>
        /// Ensure that the date is not too small by accident.
        /// </summary>
        private void CheckDateLimit(object sender, System.ComponentModel.CancelEventArgs e)
        {
            // Find the current date that we are validating
            var newDate = DebtPlus.Utils.Nulls.v_DateTime(((DevExpress.XtraEditors.DateEdit)sender).EditValue);

            // Ensure that the date is not before today.
            if (newDate.HasValue && newDate.Value.Date < DateTime.Now.Date.AddDays(1))
            {
                if (DebtPlus.Data.Forms.MessageBox.Show("The date that you entered is before today. Are you really sure\r\nthat you want this figure. It is probably incorrect.", "Are you sure?", System.Windows.Forms.MessageBoxButtons.YesNo, System.Windows.Forms.MessageBoxIcon.Question, System.Windows.Forms.MessageBoxDefaultButton.Button2) != System.Windows.Forms.DialogResult.Yes)
                {
                    e.Cancel = true;
                }
            }
        }

        /// <summary>
        /// Process the CLICK event on the OK button
        /// </summary>
        protected override void simpleButton_OK_Click(object sender, EventArgs e)
        {
            record.state          = DebtPlus.Utils.Nulls.v_Int32(lookUpEdit_state.EditValue).GetValueOrDefault();
            record.amount         = DebtPlus.Utils.Nulls.v_Decimal(calcEdit_amount.EditValue).GetValueOrDefault();
            record.effective_date = DebtPlus.Utils.Nulls.v_DateTime(dateEdit_effective_date.EditValue).GetValueOrDefault();
            record.cutoff_date    = DebtPlus.Utils.Nulls.v_DateTime(dateEdit_expires_date.EditValue);

            base.simpleButton_OK_Click(sender, e);
        }
    }
}