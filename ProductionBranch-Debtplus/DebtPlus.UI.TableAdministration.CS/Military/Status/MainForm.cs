﻿using System;
using System.Linq;
using DebtPlus.LINQ;
using DebtPlus.UI.Common;

namespace DebtPlus.UI.TableAdministration.CS.Military.Status
{
    public partial class MainForm : Templates.MainForm
    {
        // The record collection being edited
        private System.Collections.Generic.List<DebtPlus.LINQ.militaryStatusType> colRecords;

        public MainForm()
        {
            InitializeComponent();
            RegisterHandlers();
        }

        private void RegisterHandlers()
        {
            this.Load += MainForm_Load;
        }

        private void UnRegisterHandlers()
        {
            this.Load -= MainForm_Load;
        }

        private void MainForm_Load(object sender, EventArgs e)
        {
            UnRegisterHandlers();
            try
            {
                using (var cm = new CursorManager())
                {
                    using (var dc = new BusinessContext())
                    {
                        colRecords = dc.militaryStatusTypes.ToList();
                        gridControl1.DataSource = colRecords;
                        gridView1.BestFitColumns();
                    }
                }
            }
            finally
            {
                RegisterHandlers();
            }
        }

        /// <summary>
        /// Update the record in the database
        /// </summary>
        /// <param name="obj"></param>
        protected override void UpdateRecord(object obj)
        {
            DebtPlus.LINQ.militaryStatusType record = obj as DebtPlus.LINQ.militaryStatusType;
            if (record != null)
            {
                bool priorDefault = record.Default;
                using (var frm = new EditForm(record))
                {
                    if (frm.ShowDialog() != System.Windows.Forms.DialogResult.OK)
                    {
                        return;
                    }
                }

                using (var dc = new BusinessContext())
                {
                    // Remove the previous default settings
                    if (record.Default && !priorDefault)
                    {
                        var qDefault = (from r in dc.militaryStatusTypes where r.Default != false select r);
                        foreach (var i in qDefault)
                        {
                            i.Default = false;
                        }
                    }

                    // Find the record in the database
                    var qRecord = (from r in dc.militaryStatusTypes where r.Id == record.Id select r).FirstOrDefault();
                    if (qRecord != null)
                    {
                        // Update the record contents and rewrite the record.
                        qRecord.ActiveFlag = record.ActiveFlag;
                        qRecord.Default = record.Default;
                        qRecord.description = record.description;

                        dc.SubmitChanges();

                        // Remove it from the display collection
                        if (record.Default && !priorDefault)
                        {
                            foreach (var qItem in (from r in colRecords where r.Default != false && r != record select r))
                            {
                                qItem.Default = false;
                            }
                        }

                        // Refresh the display list with the update
                        gridView1.RefreshData();
                    }
                }
            }
        }

        /// <summary>
        /// Create a new record for the database
        /// </summary>
        protected override void CreateRecord()
        {
            DebtPlus.LINQ.militaryStatusType record = new DebtPlus.LINQ.militaryStatusType();
            using (var frm = new EditForm(record))
            {
                if (frm.ShowDialog() != System.Windows.Forms.DialogResult.OK)
                {
                    return;
                }
            }

            using (var dc = new BusinessContext())
            {
                // Remove the previous default settings
                if (record.Default)
                {
                    var qDefault = (from r in dc.militaryStatusTypes where r.Default != false select r);
                    foreach (var i in qDefault)
                    {
                        i.Default = false;
                    }
                }

                // Insert the new record in the database
                dc.militaryStatusTypes.InsertOnSubmit(record);
                dc.SubmitChanges();

                // Update the display collection with the default settings
                if (record.Default)
                {
                    foreach (var qItem in (from r in colRecords where r.Default != false select r))
                    {
                        qItem.Default = false;
                    }
                }

                // Add the new record to the collection and update the display
                colRecords.Add(record);
                gridView1.RefreshData();
            }
        }

        /// <summary>
        /// Delete the current record from the database
        /// </summary>
        /// <param name="obj"></param>
        protected override void DeleteRecord(object obj)
        {
            DebtPlus.LINQ.militaryStatusType record = obj as DebtPlus.LINQ.militaryStatusType;
            if (record != null)
            {
                if (DebtPlus.Data.Prompts.RequestConfirmation_Delete() != System.Windows.Forms.DialogResult.Yes)
                {
                    return;
                }
                using (var dc = new BusinessContext())
                {
                    // Find the record in the database
                    var qRecord = (from r in dc.militaryStatusTypes where r.Id == record.Id select r).FirstOrDefault();
                    if (qRecord != null)
                    {
                        dc.militaryStatusTypes.DeleteOnSubmit(qRecord);
                        dc.SubmitChanges();
                        colRecords.Remove(record);
                        gridView1.RefreshData();
                    }
                }
            }
        }
    }
}