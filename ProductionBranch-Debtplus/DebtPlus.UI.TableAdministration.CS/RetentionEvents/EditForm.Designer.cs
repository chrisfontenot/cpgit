using System;
using System.Windows.Forms;
using System.Drawing;
using DebtPlus.LINQ;
using System.Linq;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Diagnostics;
using DebtPlus.UI.Common.Templates;

namespace DebtPlus.UI.TableAdministration.CS.RetentionEvents
{
	partial class EditForm
	{
		//Form overrides dispose to clean up the component list.
		[System.Diagnostics.DebuggerNonUserCode]
		protected override void Dispose(bool disposing)
		{
			try 
            {
				if (disposing)
                {
                    if (components != null) components.Dispose();
				}
                components = null;
            }
    	    finally
            {
				base.Dispose(disposing);
			}
		}

		//Required by the Windows Form Designer
		private System.ComponentModel.IContainer components = null;

		//NOTE: The following procedure is required by the Windows Form Designer
		//It can be modified using the Windows Form Designer.  
		//Do not modify it using the code editor.
		[System.Diagnostics.DebuggerStepThrough]
		private void InitializeComponent()
		{
			this.LabelControl1 = new DevExpress.XtraEditors.LabelControl();
			this.LabelControl_ID = new DevExpress.XtraEditors.LabelControl();
			this.LabelControl_description = new DevExpress.XtraEditors.LabelControl();
			this.TextEdit_description = new DevExpress.XtraEditors.TextEdit();
			this.LabelControl_nfcc = new DevExpress.XtraEditors.LabelControl();
			this.SpinEdit_priority = new DevExpress.XtraEditors.SpinEdit();
			this.LookUpEdit_expire_type = new DevExpress.XtraEditors.LookUpEdit();
			this.LabelControl2 = new DevExpress.XtraEditors.LabelControl();
			this.LabelControl3 = new DevExpress.XtraEditors.LabelControl();
			this.LookUpEdit_initial_retention_action = new DevExpress.XtraEditors.LookUpEdit();
			((System.ComponentModel.ISupportInitialize)this.DxErrorProvider1).BeginInit();
			((System.ComponentModel.ISupportInitialize)this.TextEdit_description.Properties).BeginInit();
			((System.ComponentModel.ISupportInitialize)this.SpinEdit_priority.Properties).BeginInit();
			((System.ComponentModel.ISupportInitialize)this.LookUpEdit_expire_type.Properties).BeginInit();
			((System.ComponentModel.ISupportInitialize)this.LookUpEdit_initial_retention_action.Properties).BeginInit();
			this.SuspendLayout();
			//
			//simpleButton_OK
			//
			this.simpleButton_OK.Location = new System.Drawing.Point(372, 13);
			this.simpleButton_OK.TabIndex = 10;
			//
			//simpleButton_Cancel
			//
			this.simpleButton_Cancel.Location = new System.Drawing.Point(372, 56);
			this.simpleButton_Cancel.TabIndex = 11;
			//
			//LabelControl1
			//
			this.LabelControl1.Location = new System.Drawing.Point(13, 13);
			this.LabelControl1.Name = "LabelControl1";
			this.LabelControl1.Size = new System.Drawing.Size(11, 13);
			this.LabelControl1.TabIndex = 0;
			this.LabelControl1.Text = "ID";
			//
			//LabelControl_ID
			//
			this.LabelControl_ID.Location = new System.Drawing.Point(86, 13);
			this.LabelControl_ID.Name = "LabelControl_ID";
			this.LabelControl_ID.Size = new System.Drawing.Size(23, 13);
			this.LabelControl_ID.TabIndex = 1;
			this.LabelControl_ID.Text = "";
			//
			//LabelControl_description
			//
			this.LabelControl_description.Location = new System.Drawing.Point(13, 35);
			this.LabelControl_description.Name = "LabelControl_description";
			this.LabelControl_description.Size = new System.Drawing.Size(53, 13);
			this.LabelControl_description.TabIndex = 2;
			this.LabelControl_description.Text = "Description";
			//
			//TextEdit_description
			//
			this.TextEdit_description.Anchor = (AnchorStyles)((AnchorStyles.Top | AnchorStyles.Left) | AnchorStyles.Right);
			this.TextEdit_description.Location = new System.Drawing.Point(86, 32);
			this.TextEdit_description.Name = "TextEdit_description";
			this.TextEdit_description.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.False;
			this.TextEdit_description.Properties.MaxLength = 50;
			this.TextEdit_description.Size = new System.Drawing.Size(280, 20);
			this.TextEdit_description.TabIndex = 3;
			//
			//LabelControl_nfcc
			//
			this.LabelControl_nfcc.Location = new System.Drawing.Point(13, 87);
			this.LabelControl_nfcc.Name = "LabelControl_nfcc";
			this.LabelControl_nfcc.Size = new System.Drawing.Size(34, 13);
			this.LabelControl_nfcc.TabIndex = 6;
			this.LabelControl_nfcc.Text = "Priority";
			//
			//SpinEdit_priority
			//
			this.SpinEdit_priority.EditValue = new decimal(0);
			this.SpinEdit_priority.Location = new System.Drawing.Point(86, 84);
			this.SpinEdit_priority.Name = "SpinEdit_priority";
			this.SpinEdit_priority.Properties.Appearance.Options.UseTextOptions = true;
			this.SpinEdit_priority.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
			this.SpinEdit_priority.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] { new DevExpress.XtraEditors.Controls.EditorButton() });
			this.SpinEdit_priority.Properties.DisplayFormat.FormatString = "{0:f0}";
			this.SpinEdit_priority.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
			this.SpinEdit_priority.Properties.EditFormat.FormatString = "{0:f0}";
			this.SpinEdit_priority.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
			this.SpinEdit_priority.Properties.IsFloatValue = false;
			this.SpinEdit_priority.Properties.Mask.BeepOnError = true;
			this.SpinEdit_priority.Properties.Mask.EditMask = "\\d";
			this.SpinEdit_priority.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.RegEx;
			this.SpinEdit_priority.Properties.MaxLength = 1;
			this.SpinEdit_priority.Properties.MaxValue = new decimal(9);
			this.SpinEdit_priority.Size = new System.Drawing.Size(38, 20);
			this.SpinEdit_priority.TabIndex = 7;
			//
			//LookUpEdit_expire_type
			//
			this.LookUpEdit_expire_type.Anchor = (AnchorStyles)((AnchorStyles.Top | AnchorStyles.Left) | AnchorStyles.Right);
			this.LookUpEdit_expire_type.Location = new System.Drawing.Point(86, 111);
			this.LookUpEdit_expire_type.Name = "LookUpEdit_expire_type";
			this.LookUpEdit_expire_type.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.False;
			this.LookUpEdit_expire_type.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] { new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo) });
			this.LookUpEdit_expire_type.Properties.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
				new DevExpress.XtraEditors.Controls.LookUpColumnInfo("Id", "ID", 20, DevExpress.Utils.FormatType.Numeric, "f0", false, DevExpress.Utils.HorzAlignment.Default, DevExpress.Data.ColumnSortOrder.None),
				new DevExpress.XtraEditors.Controls.LookUpColumnInfo("description", "Description", 20, DevExpress.Utils.FormatType.None, "", true, DevExpress.Utils.HorzAlignment.Default, DevExpress.Data.ColumnSortOrder.None)
			});
			this.LookUpEdit_expire_type.Properties.DisplayMember = "description";
			this.LookUpEdit_expire_type.Properties.NullText = "";
			this.LookUpEdit_expire_type.Properties.ShowFooter = false;
			this.LookUpEdit_expire_type.Properties.ShowHeader = false;
			this.LookUpEdit_expire_type.Properties.ValueMember = "Id";
			this.LookUpEdit_expire_type.Size = new System.Drawing.Size(280, 20);
			this.LookUpEdit_expire_type.TabIndex = 9;
			this.LookUpEdit_expire_type.Properties.SortColumnIndex = 1;
			//
			//LabelControl2
			//
			this.LabelControl2.Location = new System.Drawing.Point(13, 114);
			this.LabelControl2.Name = "LabelControl2";
			this.LabelControl2.Size = new System.Drawing.Size(35, 13);
			this.LabelControl2.TabIndex = 8;
			this.LabelControl2.Text = "Expires";
			//
			//LabelControl3
			//
			this.LabelControl3.Location = new System.Drawing.Point(13, 61);
			this.LabelControl3.Name = "LabelControl3";
			this.LabelControl3.Size = new System.Drawing.Size(59, 13);
			this.LabelControl3.TabIndex = 4;
			this.LabelControl3.Text = "Initial Action";
			//
			//LookUpEdit_initial_retention_action
			//
			this.LookUpEdit_initial_retention_action.Anchor = (AnchorStyles)((AnchorStyles.Top | AnchorStyles.Left) | AnchorStyles.Right);
			this.LookUpEdit_initial_retention_action.Location = new System.Drawing.Point(86, 58);
			this.LookUpEdit_initial_retention_action.Name = "LookUpEdit_initial_retention_action";
			this.LookUpEdit_initial_retention_action.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.True;
			this.LookUpEdit_initial_retention_action.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] { new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo) });
			this.LookUpEdit_initial_retention_action.Properties.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
				new DevExpress.XtraEditors.Controls.LookUpColumnInfo("Id", "ID", 20, DevExpress.Utils.FormatType.Numeric, "f0", false, DevExpress.Utils.HorzAlignment.Default, DevExpress.Data.ColumnSortOrder.None),
				new DevExpress.XtraEditors.Controls.LookUpColumnInfo("description", "Description", 20, DevExpress.Utils.FormatType.None, "", true, DevExpress.Utils.HorzAlignment.Default, DevExpress.Data.ColumnSortOrder.None)
			});
			this.LookUpEdit_initial_retention_action.Properties.DisplayMember = "description";
			this.LookUpEdit_initial_retention_action.Properties.NullText = "";
			this.LookUpEdit_initial_retention_action.Properties.ShowFooter = false;
			this.LookUpEdit_initial_retention_action.Properties.ShowHeader = false;
			this.LookUpEdit_initial_retention_action.Properties.ValueMember = "Id";
			this.LookUpEdit_initial_retention_action.Size = new System.Drawing.Size(280, 20);
			this.LookUpEdit_initial_retention_action.TabIndex = 5;
			this.LookUpEdit_initial_retention_action.Properties.SortColumnIndex = 1;
			//
			//EditForm
			//
			this.AutoScaleDimensions = new System.Drawing.SizeF(6f, 13f);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(468, 145);
			this.Controls.Add(this.LabelControl3);
			this.Controls.Add(this.LookUpEdit_initial_retention_action);
			this.Controls.Add(this.LabelControl2);
			this.Controls.Add(this.LookUpEdit_expire_type);
			this.Controls.Add(this.SpinEdit_priority);
			this.Controls.Add(this.LabelControl_nfcc);
			this.Controls.Add(this.TextEdit_description);
			this.Controls.Add(this.LabelControl_description);
			this.Controls.Add(this.LabelControl_ID);
			this.Controls.Add(this.LabelControl1);
			this.Name = "EditForm";
			this.Text = "Retention Event";
			this.Controls.SetChildIndex(this.LabelControl1, 0);
			this.Controls.SetChildIndex(this.LabelControl_ID, 0);
			this.Controls.SetChildIndex(this.simpleButton_OK, 0);
			this.Controls.SetChildIndex(this.simpleButton_Cancel, 0);
			this.Controls.SetChildIndex(this.LabelControl_description, 0);
			this.Controls.SetChildIndex(this.TextEdit_description, 0);
			this.Controls.SetChildIndex(this.LabelControl_nfcc, 0);
			this.Controls.SetChildIndex(this.SpinEdit_priority, 0);
			this.Controls.SetChildIndex(this.LookUpEdit_expire_type, 0);
			this.Controls.SetChildIndex(this.LabelControl2, 0);
			this.Controls.SetChildIndex(this.LookUpEdit_initial_retention_action, 0);
			this.Controls.SetChildIndex(this.LabelControl3, 0);
			((System.ComponentModel.ISupportInitialize)this.DxErrorProvider1).EndInit();
			((System.ComponentModel.ISupportInitialize)this.TextEdit_description.Properties).EndInit();
			((System.ComponentModel.ISupportInitialize)this.SpinEdit_priority.Properties).EndInit();
			((System.ComponentModel.ISupportInitialize)this.LookUpEdit_expire_type.Properties).EndInit();
			((System.ComponentModel.ISupportInitialize)this.LookUpEdit_initial_retention_action.Properties).EndInit();
			this.ResumeLayout(false);
			this.PerformLayout();
		}
		private DevExpress.XtraEditors.LabelControl LabelControl1;
		private DevExpress.XtraEditors.LabelControl LabelControl_ID;
		private DevExpress.XtraEditors.LabelControl LabelControl_description;
		private DevExpress.XtraEditors.TextEdit TextEdit_description;
		private DevExpress.XtraEditors.LabelControl LabelControl_nfcc;
		private DevExpress.XtraEditors.SpinEdit SpinEdit_priority;
		private DevExpress.XtraEditors.LookUpEdit LookUpEdit_expire_type;
		private DevExpress.XtraEditors.LabelControl LabelControl2;
		private DevExpress.XtraEditors.LabelControl LabelControl3;
		private DevExpress.XtraEditors.LookUpEdit LookUpEdit_initial_retention_action;
	}
}

