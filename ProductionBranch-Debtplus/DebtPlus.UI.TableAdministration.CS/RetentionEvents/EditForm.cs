using System;
using DebtPlus.LINQ;

#region "Copyright 2000-2012 DebtPlus, L.L.C."
//{*******************************************************************}
//{                                                                   }
//{       DebtPlus Debt Management System                             }
//{                                                                   }
//{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
//{       ALL RIGHTS RESERVED                                         }
//{                                                                   }
//{   The entire contents of this file is protected by U.S. and       }
//{   International Copyright Laws. Unauthorized reproduction,        }
//{   reverse-engineering, and distribution of all or any portion of  }
//{   the code contained in this file is strictly prohibited and may  }
//{   result in severe civil and criminal penalties and will be       }
//{   prosecuted to the maximum extent possible under the law.        }
//{                                                                   }
//{   RESTRICTIONS                                                    }
//{                                                                   }
//{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
//{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
//{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
//{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
//{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
//{                                                                   }
//{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
//{   ADDITIONAL RESTRICTIONS.                                        }
//{                                                                   }
//{*******************************************************************}

#endregion "Copyright 2000-2012 DebtPlus, L.L.C."

namespace DebtPlus.UI.TableAdministration.CS.RetentionEvents
{
    internal partial class EditForm : Templates.EditTemplateForm
    {
        private BusinessContext bc = null;
        private retention_event record = null;

        internal EditForm()
            : base()
        {
            InitializeComponent();
        }

        internal EditForm(BusinessContext bc, retention_event record)
            : this()
        {
            this.bc = bc;
            this.record = record;
            RegisterHandlers();
        }

        private void RegisterHandlers()
        {
            Load += EditForm_Load;
            TextEdit_description.EditValueChanged += Form_Changed;
            SpinEdit_priority.EditValueChanged += Form_Changed;
            LookUpEdit_expire_type.EditValueChanged += Form_Changed;
            LookUpEdit_initial_retention_action.EditValueChanged += Form_Changed;
        }

        private void UnRegisterHandlers()
        {
            Load -= EditForm_Load;
            TextEdit_description.EditValueChanged -= Form_Changed;
            SpinEdit_priority.EditValueChanged -= Form_Changed;
            LookUpEdit_expire_type.EditValueChanged -= Form_Changed;
            LookUpEdit_initial_retention_action.EditValueChanged -= Form_Changed;
        }

        private void EditForm_Load(object sender, EventArgs e)
        {
            UnRegisterHandlers();
            try
            {
                // Load the lookup controls
                LookUpEdit_initial_retention_action.Properties.DataSource = DebtPlus.LINQ.Cache.retention_action.getList();
                LookUpEdit_expire_type.Properties.DataSource = DebtPlus.LINQ.InMemory.retention_expire_types.getList();

                // Populate the controls with the record contents
                LabelControl_ID.Text = record.Id < 1 ? "NEW" : record.Id.ToString();
                SpinEdit_priority.EditValue = record.priority;
                TextEdit_description.EditValue = record.description;
                LookUpEdit_expire_type.EditValue = record.expire_type;
                LookUpEdit_initial_retention_action.EditValue = record.initial_retention_action;

                // Enable/Disable the OK button
                simpleButton_OK.Enabled = !HasErrors();
            }
            finally
            {
                RegisterHandlers();
            }
        }

        /// <summary>
        /// Handle a change in the controls
        /// </summary>
        private void Form_Changed(object sender, EventArgs e)
        {
            simpleButton_OK.Enabled = !HasErrors();
        }

        /// <summary>
        /// Determine if the controls have any pending errors
        /// </summary>
        /// <returns></returns>
        private bool HasErrors()
        {
            if (!DebtPlus.Utils.Nulls.v_Int32(LookUpEdit_expire_type.EditValue).HasValue)
            {
                return true;
            }

            if (string.IsNullOrWhiteSpace(TextEdit_description.Text))
            {
                return true;
            }

            return false;
        }

        /// <summary>
        /// Handle the OK button being pressed
        /// </summary>
        protected override void simpleButton_OK_Click(object sender, EventArgs e)
        {
            base.simpleButton_OK_Click(sender, e);

            // Update the record with the control settings
            record.priority = DebtPlus.Utils.Nulls.v_Int32(SpinEdit_priority.EditValue).GetValueOrDefault(9);
            record.description = DebtPlus.Utils.Nulls.v_String(TextEdit_description.EditValue);
            record.expire_type = DebtPlus.Utils.Nulls.v_Int32(LookUpEdit_expire_type.EditValue);
            record.initial_retention_action = DebtPlus.Utils.Nulls.v_Int32(LookUpEdit_initial_retention_action.EditValue);
        }
    }
}