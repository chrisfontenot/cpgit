#region "Copyright 2000-2012 DebtPlus, L.L.C."

//{*******************************************************************}
//{                                                                   }
//{       DebtPlus Debt Management System                             }
//{                                                                   }
//{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
//{       ALL RIGHTS RESERVED                                         }
//{                                                                   }
//{   The entire contents of this file is protected by U.S. and       }
//{   International Copyright Laws. Unauthorized reproduction,        }
//{   reverse-engineering, and distribution of all or any portion of  }
//{   the code contained in this file is strictly prohibited and may  }
//{   result in severe civil and criminal penalties and will be       }
//{   prosecuted to the maximum extent possible under the law.        }
//{                                                                   }
//{   RESTRICTIONS                                                    }
//{                                                                   }
//{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
//{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
//{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
//{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
//{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
//{                                                                   }
//{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
//{   ADDITIONAL RESTRICTIONS.                                        }
//{                                                                   }
//{*******************************************************************}

#endregion "Copyright 2000-2012 DebtPlus, L.L.C."

namespace DebtPlus.UI.TableAdministration.CS.Reports
{
    internal partial class EditForm : Templates.EditTemplateForm
    {
        /// <summary>
        /// Current record being edited
        /// </summary>
        private DebtPlus.LINQ.report record;

        /// <summary>
        /// Class to represent the list items for the report menu type
        /// </summary>
        private class TypeDescription
        {
            internal string type { get; set; }
            internal string description { get; set; }

            internal TypeDescription(string type, string description)
            {
                this.type = type;
                this.description = description;
            }
        }

        /// <summary>
        /// List of the acceptable values for the menu type
        /// </summary>
        private static System.Collections.Generic.List<TypeDescription> TypeDescriptions = new System.Collections.Generic.List<TypeDescription>
        {
                new TypeDescription("CL", "Client"),
                new TypeDescription("CR", "Creditor"),
                new TypeDescription("DB", "Debt"),
                new TypeDescription("OT", "Other")
        };

        /// <summary>
        /// Initialize the class object
        /// </summary>
        /// <param name="record"></param>
        internal EditForm(DebtPlus.LINQ.report record)
            : base()
        {
            this.record = record;

            InitializeComponent();
            LookUpEdit_Type.Properties.DataSource = TypeDescriptions;

            RegisterHandlers();
        }

        /// <summary>
        /// Register the event handlers for this class
        /// </summary>
        private void RegisterHandlers()
        {
            Load += EditForm_Load;
            simpleButton_OK.Click += SaveRecord;
            TextEdit_Argument.EditValueChanged += FormChanged;
            TextEdit_ClassName.EditValueChanged += FormChanged;
            TextEdit_description.EditValueChanged += FormChanged;
            TextEdit_label.EditValueChanged += FormChanged;
            TextEdit_menu_name.EditValueChanged += FormChanged;
            LookUpEdit_Type.EditValueChanged += FormChanged;
        }

        /// <summary>
        /// Remove the event handler registration
        /// </summary>
        private void UnRegisterHandlers()
        {
            Load -= EditForm_Load;
            simpleButton_OK.Click -= SaveRecord;
            TextEdit_Argument.EditValueChanged -= FormChanged;
            TextEdit_ClassName.EditValueChanged -= FormChanged;
            TextEdit_description.EditValueChanged -= FormChanged;
            TextEdit_label.EditValueChanged -= FormChanged;
            TextEdit_menu_name.EditValueChanged -= FormChanged;
            LookUpEdit_Type.EditValueChanged -= FormChanged;
        }

        /// <summary>
        /// Process the LOAD event for the FORM
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void EditForm_Load(object sender, System.EventArgs e)
        {
            UnRegisterHandlers();
            try
            {
                LoadValues(record);
                simpleButton_OK.Enabled = !HasErrors();
            }
            finally
            {
                RegisterHandlers();
            }
        }

        /// <summary>
        /// Load the values into the form
        /// </summary>
        /// <param name="record">Pointer to the record for the form information</param>
        private void LoadValues(DebtPlus.LINQ.report record)
        {
            // Bind the controls
            LabelControl_Id.Text = record.Id > 0 ? record.Id.ToString() : "NEW";

            // Rebind the controls to the current record
            TextEdit_Argument.EditValue = record.Argument;
            TextEdit_ClassName.EditValue = record.ClassName;
            TextEdit_description.EditValue = record.description;
            TextEdit_label.EditValue = record.label;
            TextEdit_menu_name.EditValue = record.menu_name;
            LookUpEdit_Type.EditValue = record.Type;
        }

        /// <summary>
        /// When the OK button is pressed, copy the values back to the record
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void SaveRecord(object sender, System.EventArgs e)
        {
            SaveValues(record);
        }

        /// <summary>
        /// Save the values into the form
        /// </summary>
        /// <param name="record">Pointer to the record for the form information</param>
        private void SaveValues(DebtPlus.LINQ.report record)
        {
            // Refresh the record with the input values
            record.description = (string)TextEdit_description.EditValue;
            record.Argument = (string)TextEdit_Argument.EditValue;
            record.label = (string)TextEdit_label.EditValue;
            record.menu_name = (string)TextEdit_menu_name.EditValue;
            record.Type = (string)LookUpEdit_Type.EditValue;

            // It is critical that the class name have no leading or trailing blanks or the
            // item will not be available to the program as it is searched by the class name.
            record.ClassName = (string)TextEdit_ClassName.EditValue;
            if (record.ClassName != null)
            {
                record.ClassName = record.ClassName.Trim();
            }
        }

        /// <summary>
        /// When the form is changed, update the status of the OK button
        /// </summary>
        /// <param name="Sender"></param>
        /// <param name="e"></param>
        private void FormChanged(object Sender, System.EventArgs e)
        {
            simpleButton_OK.Enabled = !HasErrors();
        }

        /// <summary>
        /// Determine if the input form is valid for submission
        /// </summary>
        /// <returns></returns>
        private bool HasErrors()
        {
            // Ensure that there is a description
            if (TextEdit_description.Text.Trim() == string.Empty)
            {
                return true;
            }

            // A type must be specified.
            if (LookUpEdit_Type.EditValue == null)
            {
                return true;
            }

            // Finally, everything is valid.
            return false;
        }
    }
}