#region "Copyright 2000-2012 DebtPlus, L.L.C."

//{*******************************************************************}
//{                                                                   }
//{       DebtPlus Debt Management System                             }
//{                                                                   }
//{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
//{       ALL RIGHTS RESERVED                                         }
//{                                                                   }
//{   The entire contents of this file is protected by U.S. and       }
//{   International Copyright Laws. Unauthorized reproduction,        }
//{   reverse-engineering, and distribution of all or any portion of  }
//{   the code contained in this file is strictly prohibited and may  }
//{   result in severe civil and criminal penalties and will be       }
//{   prosecuted to the maximum extent possible under the law.        }
//{                                                                   }
//{   RESTRICTIONS                                                    }
//{                                                                   }
//{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
//{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
//{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
//{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
//{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
//{                                                                   }
//{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
//{   ADDITIONAL RESTRICTIONS.                                        }
//{                                                                   }
//{*******************************************************************}

#endregion "Copyright 2000-2012 DebtPlus, L.L.C."

using System;
using System.Linq;
using System.Windows.Forms;
using DebtPlus.LINQ;

namespace DebtPlus.UI.TableAdministration.CS.Reports
{
    public partial class MainForm : Templates.MainForm
    {
        /// <summary>
        /// List of the records to be edited in the program
        /// </summary>
        private System.Collections.Generic.List<DebtPlus.LINQ.report> reportList;

        /// <summary>
        /// Initialize the new class
        /// </summary>
        /// <param name="dc">Pointer to the data context in the mainline</param>
        public MainForm()
            : base()
        {
            InitializeComponent();
            RegisterHandlers();
        }

        private void RegisterHandlers()
        {
            Load += MainForm_Load;
        }

        private void UnRegisterHandlers()
        {
            Load -= MainForm_Load;
        }

        /// <summary>
        /// Process the load event on the form
        /// </summary>
        private void MainForm_Load(object sender, System.EventArgs e)
        {
            Cursor current_cursor = Cursor.Current;
            try
            {
                Cursor.Current = Cursors.WaitCursor;

                // Retrieve the list of menu items
                using (DebtPlus.LINQ.BusinessContext drm = new DebtPlus.LINQ.BusinessContext())
                {
                    reportList = (from m in drm.reports select m).ToList();
                }

                // Bind the data to the grid and set the column widths
                bind();
                gridView1.BestFitColumns();
            }
            catch (Exception ex)
            {
                DebtPlus.UI.Common.ErrorHandling.HandleErrors(ex, "Error retrieving database information");
            }
            finally
            {
                Cursor.Current = current_cursor;
            }
        }

        /// <summary>
        /// Bind the data source for the grid to the list of menu items
        /// </summary>
        protected void bind()
        {
            gridControl1.DataSource = (from r in reportList orderby r.Id select r).ToList();
            gridControl1.RefreshDataSource();
        }

        /// <summary>
        /// Handle the edit of the information on the form
        /// </summary>
        protected DialogResult EditRecord(object obj)
        {
            DebtPlus.LINQ.report record = (DebtPlus.LINQ.report)obj;
            using (var frm = new EditForm(record))
            {
                return frm.ShowDialog();
            }
        }

        /// <summary>
        /// Refresh the display list when the item is changed in the view form
        /// </summary>
        private void ViewMenu_refresh(object sender, EventArgs e)
        {
            bind();
        }

        /// <summary>
        /// Create the new record
        /// </summary>
        protected override void CreateRecord()
        {
            // Allocate the new record to be inserted.
            DebtPlus.LINQ.report record = new DebtPlus.LINQ.report();

            // Edit the new record. If OK then attempt to insert the record.
            if (EditRecord(record) == System.Windows.Forms.DialogResult.OK)
            {
                try
                {
                    // Update the database with the current values
                    using (var drm = new BusinessContext())
                    {
                        // Insert the record into the database. It will update the Id field.
                        drm.reports.InsertOnSubmit(record);
                        drm.SubmitChanges();

                        // After the record is updated, add it to the list.
                        reportList.Add(record);

                        // Refresh the displayed list with the new addition
                        bind();
                    }
                }
                catch (System.Data.SqlClient.SqlException ex)
                {
                    DebtPlus.UI.Common.ErrorHandling.HandleErrors(ex, "Error updating database");
                }
            }
        }

        /// <summary>
        /// Update the existing record
        /// </summary>
        /// <param name="obj">Pointer to the desired record for the operation</param>
        protected override void UpdateRecord(object obj)
        {
            DebtPlus.LINQ.report record = (DebtPlus.LINQ.report)obj;

            if (EditRecord(record) == System.Windows.Forms.DialogResult.OK)
            {
                try
                {
                    // Update the database with the current values
                    using (var drm = new BusinessContext())
                    {
                        var q = (from r in drm.reports where r.Id == record.Id select r).SingleOrDefault();
                        if (q != null)
                        {
                            q.Argument = record.Argument;
                            q.ClassName = record.ClassName;
                            q.description = record.description;
                            q.filename = record.filename;
                            q.label = record.label;
                            q.menu_name = record.menu_name;
                            q.Type = record.Type;
                        }
                        else // The ID was deleted before we could finish the update. Insert the new record now.
                        {
                            drm.reports.InsertOnSubmit(record);
                        }

                        drm.SubmitChanges();

                        // Update the list being displayed to the user
                        bind();
                    }
                }
                catch (System.Data.SqlClient.SqlException ex)
                {
                    DebtPlus.UI.Common.ErrorHandling.HandleErrors(ex, "Error updating database");
                }
            }
        }

        /// <summary>
        /// Delete the indicated record from the database
        /// </summary>
        /// <param name="obj">Pointer to the desired record for the operation</param>
        protected override void DeleteRecord(object obj)
        {
            DebtPlus.LINQ.report record = (DebtPlus.LINQ.report)obj;
            if (DebtPlus.Data.Prompts.RequestConfirmation_Delete() == System.Windows.Forms.DialogResult.Yes)
            {
                using (var drm = new BusinessContext())
                {
                    // Find the record and remove it from the database if it is still there
                    var query = (from m in drm.reports where m.Id == record.Id select m).SingleOrDefault();
                    if (query != null)
                    {
                        drm.reports.DeleteOnSubmit(query);
                        drm.SubmitChanges();
                    }

                    // Remove the record from the display list and redisplay it
                    reportList.Remove(record);
                    bind();
                }
            }
        }
    }
}