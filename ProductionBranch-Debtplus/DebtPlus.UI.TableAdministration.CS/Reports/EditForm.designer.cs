﻿using Microsoft.VisualBasic;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Diagnostics;

namespace DebtPlus.UI.TableAdministration.CS.Reports
{
    partial class EditForm
    {
		//Form overrides dispose to clean up the component list.
		[System.Diagnostics.DebuggerNonUserCode]
		protected override void Dispose(bool disposing)
		{
			try 
            {
				if (disposing)
                {
                    if (components != null) components.Dispose();
				}
                components = null;
            }
    	    finally
            {
				base.Dispose(disposing);
			}
		}

		//Required by the Windows Form Designer
		private System.ComponentModel.IContainer components = null;

        //NOTE: The following procedure is required by the Windows Form Designer
        //It can be modified using the Windows Form Designer.  
        //Do not modify it using the code editor.
        [System.Diagnostics.DebuggerStepThrough()]
        private void InitializeComponent()
        {
            this.LookUpEdit_Type = new DevExpress.XtraEditors.LookUpEdit();
            this.LabelControl4 = new DevExpress.XtraEditors.LabelControl();
            this.TextEdit_menu_name = new DevExpress.XtraEditors.TextEdit();
            this.LabelControl3 = new DevExpress.XtraEditors.LabelControl();
            this.TextEdit_ClassName = new DevExpress.XtraEditors.TextEdit();
            this.TextEdit_label = new DevExpress.XtraEditors.TextEdit();
            this.LabelControl8 = new DevExpress.XtraEditors.LabelControl();
            this.LabelControl7 = new DevExpress.XtraEditors.LabelControl();
            this.TextEdit_description = new DevExpress.XtraEditors.TextEdit();
            this.LabelControl_description = new DevExpress.XtraEditors.LabelControl();
            this.LabelControl_Id = new DevExpress.XtraEditors.LabelControl();
            this.LabelControl1 = new DevExpress.XtraEditors.LabelControl();
            this.TextEdit_Argument = new DevExpress.XtraEditors.TextEdit();
            this.labelControl2 = new DevExpress.XtraEditors.LabelControl();
            ((System.ComponentModel.ISupportInitialize)(this.DxErrorProvider1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LookUpEdit_Type.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TextEdit_menu_name.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TextEdit_ClassName.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TextEdit_label.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TextEdit_description.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TextEdit_Argument.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // simpleButton_OK
            // 
            this.simpleButton_OK.Enabled = true;
            this.simpleButton_OK.Location = new System.Drawing.Point(322, 34);
            this.simpleButton_OK.TabIndex = 14;
            // 
            // simpleButton_Cancel
            // 
            this.simpleButton_Cancel.Location = new System.Drawing.Point(322, 77);
            this.simpleButton_Cancel.TabIndex = 15;
            // 
            // DefaultLookAndFeel1
            // 
            this.DefaultLookAndFeel1.LookAndFeel.SkinName = "McSkin";
            // 
            // LookUpEdit_Type
            // 
            this.LookUpEdit_Type.Location = new System.Drawing.Point(97, 144);
            this.LookUpEdit_Type.Name = "LookUpEdit_Type";
            this.LookUpEdit_Type.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.False;
            this.LookUpEdit_Type.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LookUpEdit_Type.Properties.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("type", "type", 20, DevExpress.Utils.FormatType.None, "", false, DevExpress.Utils.HorzAlignment.Default),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("description", "Description", 20, DevExpress.Utils.FormatType.None, "", true, DevExpress.Utils.HorzAlignment.Default, DevExpress.Data.ColumnSortOrder.Ascending)});
            this.LookUpEdit_Type.Properties.DisplayMember = "description";
            this.LookUpEdit_Type.Properties.NullText = "";
            this.LookUpEdit_Type.Properties.ShowFooter = false;
            this.LookUpEdit_Type.Properties.ShowHeader = false;
            this.LookUpEdit_Type.Properties.SortColumnIndex = 1;
            this.LookUpEdit_Type.Properties.ValueMember = "type";
            this.LookUpEdit_Type.Size = new System.Drawing.Size(208, 20);
            this.LookUpEdit_Type.TabIndex = 11;
            // 
            // LabelControl4
            // 
            this.LabelControl4.Location = new System.Drawing.Point(13, 148);
            this.LabelControl4.Name = "LabelControl4";
            this.LabelControl4.Size = new System.Drawing.Size(22, 13);
            this.LabelControl4.TabIndex = 10;
            this.LabelControl4.Text = "type";
            // 
            // TextEdit_menu_name
            // 
            this.TextEdit_menu_name.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.TextEdit_menu_name.Location = new System.Drawing.Point(97, 170);
            this.TextEdit_menu_name.Name = "TextEdit_menu_name";
            this.TextEdit_menu_name.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.True;
            this.TextEdit_menu_name.Properties.MaxLength = 50;
            this.TextEdit_menu_name.Size = new System.Drawing.Size(208, 20);
            this.TextEdit_menu_name.TabIndex = 13;
            // 
            // LabelControl3
            // 
            this.LabelControl3.Location = new System.Drawing.Point(12, 174);
            this.LabelControl3.Name = "LabelControl3";
            this.LabelControl3.Size = new System.Drawing.Size(56, 13);
            this.LabelControl3.TabIndex = 12;
            this.LabelControl3.Text = "Menu Name";
            // 
            // TextEdit_ClassName
            // 
            this.TextEdit_ClassName.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.TextEdit_ClassName.Location = new System.Drawing.Point(97, 92);
            this.TextEdit_ClassName.Name = "TextEdit_ClassName";
            this.TextEdit_ClassName.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.False;
            this.TextEdit_ClassName.Properties.MaxLength = 50;
            this.TextEdit_ClassName.Size = new System.Drawing.Size(208, 20);
            this.TextEdit_ClassName.TabIndex = 7;
            // 
            // TextEdit_label
            // 
            this.TextEdit_label.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.TextEdit_label.Location = new System.Drawing.Point(97, 66);
            this.TextEdit_label.Name = "TextEdit_label";
            this.TextEdit_label.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.True;
            this.TextEdit_label.Properties.MaxLength = 50;
            this.TextEdit_label.Size = new System.Drawing.Size(208, 20);
            this.TextEdit_label.TabIndex = 5;
            // 
            // LabelControl8
            // 
            this.LabelControl8.Location = new System.Drawing.Point(13, 96);
            this.LabelControl8.Name = "LabelControl8";
            this.LabelControl8.Size = new System.Drawing.Size(55, 13);
            this.LabelControl8.TabIndex = 6;
            this.LabelControl8.Text = "Class Name";
            // 
            // LabelControl7
            // 
            this.LabelControl7.Location = new System.Drawing.Point(12, 70);
            this.LabelControl7.Name = "LabelControl7";
            this.LabelControl7.Size = new System.Drawing.Size(25, 13);
            this.LabelControl7.TabIndex = 4;
            this.LabelControl7.Text = "Label";
            // 
            // TextEdit_description
            // 
            this.TextEdit_description.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.TextEdit_description.Location = new System.Drawing.Point(97, 40);
            this.TextEdit_description.Name = "TextEdit_description";
            this.TextEdit_description.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.False;
            this.TextEdit_description.Properties.MaxLength = 50;
            this.TextEdit_description.Size = new System.Drawing.Size(208, 20);
            this.TextEdit_description.TabIndex = 3;
            // 
            // LabelControl_description
            // 
            this.LabelControl_description.Location = new System.Drawing.Point(13, 44);
            this.LabelControl_description.Name = "LabelControl_description";
            this.LabelControl_description.Size = new System.Drawing.Size(53, 13);
            this.LabelControl_description.TabIndex = 2;
            this.LabelControl_description.Text = "Description";
            // 
            // LabelControl_Id
            // 
            this.LabelControl_Id.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.LabelControl_Id.Location = new System.Drawing.Point(97, 21);
            this.LabelControl_Id.Name = "LabelControl_Id";
            this.LabelControl_Id.Size = new System.Drawing.Size(208, 13);
            this.LabelControl_Id.TabIndex = 1;
            // 
            // LabelControl1
            // 
            this.LabelControl1.Location = new System.Drawing.Point(13, 21);
            this.LabelControl1.Name = "LabelControl1";
            this.LabelControl1.Size = new System.Drawing.Size(11, 13);
            this.LabelControl1.TabIndex = 0;
            this.LabelControl1.Text = "ID";
            // 
            // TextEdit_Argument
            // 
            this.TextEdit_Argument.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.TextEdit_Argument.Location = new System.Drawing.Point(97, 118);
            this.TextEdit_Argument.Name = "TextEdit_Argument";
            this.TextEdit_Argument.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.True;
            this.TextEdit_Argument.Properties.MaxLength = 50;
            this.TextEdit_Argument.Size = new System.Drawing.Size(208, 20);
            this.TextEdit_Argument.TabIndex = 9;
            // 
            // labelControl2
            // 
            this.labelControl2.Location = new System.Drawing.Point(13, 122);
            this.labelControl2.Name = "labelControl2";
            this.labelControl2.Size = new System.Drawing.Size(60, 13);
            this.labelControl2.TabIndex = 8;
            this.labelControl2.Text = "Argument(s)";
            // 
            // EditForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(418, 207);
            this.Controls.Add(this.TextEdit_Argument);
            this.Controls.Add(this.labelControl2);
            this.Controls.Add(this.LookUpEdit_Type);
            this.Controls.Add(this.LabelControl4);
            this.Controls.Add(this.TextEdit_menu_name);
            this.Controls.Add(this.LabelControl3);
            this.Controls.Add(this.TextEdit_ClassName);
            this.Controls.Add(this.TextEdit_label);
            this.Controls.Add(this.LabelControl8);
            this.Controls.Add(this.LabelControl7);
            this.Controls.Add(this.TextEdit_description);
            this.Controls.Add(this.LabelControl_description);
            this.Controls.Add(this.LabelControl_Id);
            this.Controls.Add(this.LabelControl1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.Sizable;
            this.Name = "EditForm";
            this.Text = "Menu Item";
            this.Controls.SetChildIndex(this.simpleButton_OK, 0);
            this.Controls.SetChildIndex(this.simpleButton_Cancel, 0);
            this.Controls.SetChildIndex(this.LabelControl1, 0);
            this.Controls.SetChildIndex(this.LabelControl_Id, 0);
            this.Controls.SetChildIndex(this.LabelControl_description, 0);
            this.Controls.SetChildIndex(this.TextEdit_description, 0);
            this.Controls.SetChildIndex(this.LabelControl7, 0);
            this.Controls.SetChildIndex(this.LabelControl8, 0);
            this.Controls.SetChildIndex(this.TextEdit_label, 0);
            this.Controls.SetChildIndex(this.TextEdit_ClassName, 0);
            this.Controls.SetChildIndex(this.LabelControl3, 0);
            this.Controls.SetChildIndex(this.TextEdit_menu_name, 0);
            this.Controls.SetChildIndex(this.LabelControl4, 0);
            this.Controls.SetChildIndex(this.LookUpEdit_Type, 0);
            this.Controls.SetChildIndex(this.labelControl2, 0);
            this.Controls.SetChildIndex(this.TextEdit_Argument, 0);
            ((System.ComponentModel.ISupportInitialize)(this.DxErrorProvider1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LookUpEdit_Type.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TextEdit_menu_name.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TextEdit_ClassName.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TextEdit_label.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TextEdit_description.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TextEdit_Argument.Properties)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        private DevExpress.XtraEditors.LookUpEdit LookUpEdit_Type;
        private DevExpress.XtraEditors.LabelControl LabelControl4;
        private DevExpress.XtraEditors.TextEdit TextEdit_menu_name;
        private DevExpress.XtraEditors.LabelControl LabelControl3;
        private DevExpress.XtraEditors.TextEdit TextEdit_ClassName;
        private DevExpress.XtraEditors.TextEdit TextEdit_label;
        private DevExpress.XtraEditors.LabelControl LabelControl8;
        private DevExpress.XtraEditors.LabelControl LabelControl7;
        private DevExpress.XtraEditors.TextEdit TextEdit_description;
        private DevExpress.XtraEditors.LabelControl LabelControl_description;
        private DevExpress.XtraEditors.LabelControl LabelControl_Id;
        private DevExpress.XtraEditors.LabelControl LabelControl1;
        private DevExpress.XtraEditors.TextEdit TextEdit_Argument;
        private DevExpress.XtraEditors.LabelControl labelControl2;
    }
}
