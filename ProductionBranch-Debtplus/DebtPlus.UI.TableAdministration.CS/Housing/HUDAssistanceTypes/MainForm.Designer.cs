using System;
using System.Windows.Forms;
using System.Drawing;
using DebtPlus.LINQ;
using System.Linq;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Diagnostics;
using DebtPlus.UI.Common.Templates;
namespace DebtPlus.UI.TableAdministration.CS.Housing.HUDAssistanceTypes
{
    partial class MainForm
    {
        //Form overrides dispose to clean up the component list.
        [System.Diagnostics.DebuggerNonUserCode]
        protected override void Dispose(bool disposing)
        {
            try
            {
                if (disposing)
                {
                    if (components != null) components.Dispose();
                    if (bc != null) bc.Dispose();
                }
                components = null;
                bc = null;
            }
            finally
            {
                base.Dispose(disposing);
            }
        }

        //Required by the Windows Form Designer
        private System.ComponentModel.IContainer components = null;

        //NOTE: The following procedure is required by the Windows Form Designer
		//It can be modified using the Windows Form Designer.  
		//Do not modify it using the code editor.
		[System.Diagnostics.DebuggerStepThrough]
		private void InitializeComponent()
		{
			this.gridColumn_Default = new DevExpress.XtraGrid.Columns.GridColumn();
			this.gridColumn_Default.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.True;
			this.gridColumn_ActiveFlag = new DevExpress.XtraGrid.Columns.GridColumn();
			this.gridColumn_ActiveFlag.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.True;
			this.gridColumn_ID = new DevExpress.XtraGrid.Columns.GridColumn();
			this.gridColumn_ID.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.True;
			this.gridColumn_description = new DevExpress.XtraGrid.Columns.GridColumn();
			this.gridColumn_description.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.True;
			((System.ComponentModel.ISupportInitialize)this.gridControl1).BeginInit();
			((System.ComponentModel.ISupportInitialize)this.gridView1).BeginInit();
			((System.ComponentModel.ISupportInitialize)this.DxErrorProvider1).BeginInit();
			this.SuspendLayout();
			//
			//gridControl1
			//
			this.gridControl1.EmbeddedNavigator.Name = "";
			//
			//gridView1
			//
			this.gridView1.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
				this.gridColumn_ID,
				this.gridColumn_description,
				this.gridColumn_ActiveFlag,
				this.gridColumn_Default
			});
			//
			//gridColumn_ID
			//
			this.gridColumn_ID.Caption = "ID";
			this.gridColumn_ID.DisplayFormat.FormatString = "f0";
			this.gridColumn_ID.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
			this.gridColumn_ID.FieldName = "Id";
			this.gridColumn_ID.GroupFormat.FormatString = "f0";
			this.gridColumn_ID.GroupFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
			this.gridColumn_ID.Name = "gridColumn_ID";
			this.gridColumn_ID.Visible = true;
			this.gridColumn_ID.VisibleIndex = 0;
			//
			//gridColumn_Name
			//
			this.gridColumn_description.Caption = "Description";
			this.gridColumn_description.FieldName = "description";
			this.gridColumn_description.Name = "gridColumn_Name";
			this.gridColumn_description.Visible = true;
			this.gridColumn_description.VisibleIndex = 1;
			//
			//gridColumn_Default
			//
			this.gridColumn_Default.Caption = "Default";
			this.gridColumn_Default.FieldName = "Default";
			this.gridColumn_Default.Name = "gridColumn_Default";
			this.gridColumn_Default.Visible = true;
			this.gridColumn_Default.VisibleIndex = 2;
			//
			//gridColumn_ActiveFlag
			//
			this.gridColumn_ActiveFlag.Caption = "Active";
			this.gridColumn_ActiveFlag.FieldName = "ActiveFlag";
			this.gridColumn_ActiveFlag.Name = "gridColumn_ActiveFlag";
			this.gridColumn_ActiveFlag.Visible = true;
			this.gridColumn_ActiveFlag.VisibleIndex = 2;
			//
			//MainForm
			//
			this.AutoScaleDimensions = new System.Drawing.SizeF(6f, 13f);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(528, 294);
			this.Name = "MainForm";
			this.ToolTipController1.SetSuperTip(this, null);
			this.Text = "HUD Assistance Types";
			((System.ComponentModel.ISupportInitialize)this.gridControl1).EndInit();
			((System.ComponentModel.ISupportInitialize)this.gridView1).EndInit();
			((System.ComponentModel.ISupportInitialize)this.DxErrorProvider1).EndInit();
			this.ResumeLayout(false);
		}
		private DevExpress.XtraGrid.Columns.GridColumn gridColumn_ID;
		private DevExpress.XtraGrid.Columns.GridColumn gridColumn_Default;
		private DevExpress.XtraGrid.Columns.GridColumn gridColumn_ActiveFlag;
		private DevExpress.XtraGrid.Columns.GridColumn gridColumn_description;
	}
}

