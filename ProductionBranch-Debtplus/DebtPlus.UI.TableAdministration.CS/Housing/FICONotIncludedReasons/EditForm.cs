using System;
using DebtPlus.LINQ;

namespace DebtPlus.UI.TableAdministration.CS.Housing.FICONotIncludedReasons
{
    internal partial class EditForm : Templates.EditTemplateForm
    {
        private Housing_FICONotIncludedReason record;

        internal EditForm()
            : base()
        {
            InitializeComponent();
        }

        internal EditForm(Housing_FICONotIncludedReason record)
            : this()
        {
            this.record = record;
            RegisterHandlers();
        }

        private void RegisterHandlers()
        {
            this.Load += EditForm_Load;
            TextEdit_description.EditValueChanged += form_Changed;
        }

        private void UnRegisterHandlers()
        {
            this.Load -= EditForm_Load;
            TextEdit_description.EditValueChanged -= form_Changed;
        }

        private void EditForm_Load(object sender, EventArgs e)
        {
            UnRegisterHandlers();
            try
            {
                LookupEdit_hud_9902_section.Properties.DataSource = DebtPlus.LINQ.Cache.Housing_ARM_referenceInfo.getList().FindAll(s => s.groupId == 33);

                LabelControl_ID.Text = record.Id <= 0 ? "NEW" : record.Id.ToString();

                LookupEdit_hud_9902_section.EditValue = record.hud_9902_section;
                TextEdit_description.EditValue = record.description;
                CheckEdit_default.Checked = record.Default;
                CheckEdit_ActiveFlag.Checked = record.ActiveFlag;
            }
            finally
            {
                RegisterHandlers();
            }
            simpleButton_OK.Enabled = !HasErrors();
        }

        private void form_Changed(object sender, EventArgs e)
        {
            simpleButton_OK.Enabled = !HasErrors();
        }

        private bool HasErrors()
        {
            if (string.IsNullOrWhiteSpace(DebtPlus.Utils.Nulls.v_String(TextEdit_description.EditValue)))
            {
                return true;
            }

            return false;
        }

        protected override void simpleButton_OK_Click(object sender, EventArgs e)
        {
            record.description = DebtPlus.Utils.Nulls.v_String(TextEdit_description.EditValue);
            record.Default = CheckEdit_default.Checked;
            record.ActiveFlag = CheckEdit_ActiveFlag.Checked;
            record.hud_9902_section = DebtPlus.Utils.Nulls.v_String(LookupEdit_hud_9902_section.EditValue);

            base.simpleButton_OK_Click(sender, e);
        }
    }
}