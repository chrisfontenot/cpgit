
using System;
using System.Windows.Forms;
using System.Drawing;
using DebtPlus.LINQ;
using System.Linq;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Diagnostics;
using DebtPlus.UI.Common.Templates;

namespace DebtPlus.UI.TableAdministration.CS.Housing.TrainingCourses
{
	partial class EditForm
	{
		//Form overrides dispose to clean up the component list.
		[System.Diagnostics.DebuggerNonUserCode]
		protected override void Dispose(bool disposing)
		{
			try 
            {
				if (disposing)
                {
                    if (components != null) components.Dispose();
				}
                components = null;
            }
    	    finally
            {
				base.Dispose(disposing);
			}
		}

		//Required by the Windows Form Designer
		private System.ComponentModel.IContainer components = null;

		//NOTE: The following procedure is required by the Windows Form Designer
		//It can be modified using the Windows Form Designer.  
		//Do not modify it using the code editor.
		[System.Diagnostics.DebuggerStepThrough]
		private void InitializeComponent()
		{
			this.TextEdit_TrainingOrganizationOther = new DevExpress.XtraEditors.TextEdit();
			this.LayoutControl1 = new DevExpress.XtraLayout.LayoutControl();
			this.CheckEdit_Certificate = new DevExpress.XtraEditors.CheckEdit();
			this.TextEdit_TrainingTitle = new DevExpress.XtraEditors.TextEdit();
			this.CheckEdit_ActiveFlag = new DevExpress.XtraEditors.CheckEdit();
			this.LookupEdit_TrainingOrganization = new DevExpress.XtraEditors.LookUpEdit();
			this.LookUpEdit_TrainingSponsor = new DevExpress.XtraEditors.LookUpEdit();
			this.TextEdit_TrainingSponsorOther = new DevExpress.XtraEditors.TextEdit();
			this.TextEdit_TrainingDuration = new DevExpress.XtraEditors.TextEdit();
			this.LayoutControlGroup1 = new DevExpress.XtraLayout.LayoutControlGroup();
			this.LayoutControlItem1 = new DevExpress.XtraLayout.LayoutControlItem();
			this.LayoutControlItem2 = new DevExpress.XtraLayout.LayoutControlItem();
			this.LayoutControlItem3 = new DevExpress.XtraLayout.LayoutControlItem();
			this.LayoutControlItem4 = new DevExpress.XtraLayout.LayoutControlItem();
			this.LayoutControlItem5 = new DevExpress.XtraLayout.LayoutControlItem();
			this.LayoutControlItem7 = new DevExpress.XtraLayout.LayoutControlItem();
			this.LayoutControlItem8 = new DevExpress.XtraLayout.LayoutControlItem();
			this.EmptySpaceItem1 = new DevExpress.XtraLayout.EmptySpaceItem();
			this.LayoutControlItem6 = new DevExpress.XtraLayout.LayoutControlItem();
			this.EmptySpaceItem2 = new DevExpress.XtraLayout.EmptySpaceItem();
			((System.ComponentModel.ISupportInitialize)this.DxErrorProvider1).BeginInit();
			((System.ComponentModel.ISupportInitialize)this.TextEdit_TrainingOrganizationOther.Properties).BeginInit();
			((System.ComponentModel.ISupportInitialize)this.LayoutControl1).BeginInit();
			this.LayoutControl1.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)this.CheckEdit_Certificate.Properties).BeginInit();
			((System.ComponentModel.ISupportInitialize)this.TextEdit_TrainingTitle.Properties).BeginInit();
			((System.ComponentModel.ISupportInitialize)this.CheckEdit_ActiveFlag.Properties).BeginInit();
			((System.ComponentModel.ISupportInitialize)this.LookupEdit_TrainingOrganization.Properties).BeginInit();
			((System.ComponentModel.ISupportInitialize)this.LookUpEdit_TrainingSponsor.Properties).BeginInit();
			((System.ComponentModel.ISupportInitialize)this.TextEdit_TrainingSponsorOther.Properties).BeginInit();
			((System.ComponentModel.ISupportInitialize)this.TextEdit_TrainingDuration.Properties).BeginInit();
			((System.ComponentModel.ISupportInitialize)this.LayoutControlGroup1).BeginInit();
			((System.ComponentModel.ISupportInitialize)this.LayoutControlItem1).BeginInit();
			((System.ComponentModel.ISupportInitialize)this.LayoutControlItem2).BeginInit();
			((System.ComponentModel.ISupportInitialize)this.LayoutControlItem3).BeginInit();
			((System.ComponentModel.ISupportInitialize)this.LayoutControlItem4).BeginInit();
			((System.ComponentModel.ISupportInitialize)this.LayoutControlItem5).BeginInit();
			((System.ComponentModel.ISupportInitialize)this.LayoutControlItem7).BeginInit();
			((System.ComponentModel.ISupportInitialize)this.LayoutControlItem8).BeginInit();
			((System.ComponentModel.ISupportInitialize)this.EmptySpaceItem1).BeginInit();
			((System.ComponentModel.ISupportInitialize)this.LayoutControlItem6).BeginInit();
			((System.ComponentModel.ISupportInitialize)this.EmptySpaceItem2).BeginInit();
			this.SuspendLayout();
			//
			//simpleButton_OK
			//
			this.simpleButton_OK.Enabled = true;
			this.simpleButton_OK.Location = new System.Drawing.Point(313, 34);
			this.simpleButton_OK.TabIndex = 15;
			//
			//simpleButton_Cancel
			//
			this.simpleButton_Cancel.Location = new System.Drawing.Point(313, 77);
			this.simpleButton_Cancel.TabIndex = 16;
			//
			//TextEdit_TrainingOrganizationOther
			//
			this.TextEdit_TrainingOrganizationOther.Enabled = false;
			this.TextEdit_TrainingOrganizationOther.Location = new System.Drawing.Point(108, 60);
			this.TextEdit_TrainingOrganizationOther.Name = "TextEdit_TrainingOrganizationOther";
			this.TextEdit_TrainingOrganizationOther.Properties.Mask.AutoComplete = DevExpress.XtraEditors.Mask.AutoCompleteType.None;
			this.TextEdit_TrainingOrganizationOther.Properties.Mask.BeepOnError = true;
			this.TextEdit_TrainingOrganizationOther.Properties.Mask.EditMask = "[^ ].+";
			this.TextEdit_TrainingOrganizationOther.Properties.Mask.IgnoreMaskBlank = false;
			this.TextEdit_TrainingOrganizationOther.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.RegEx;
			this.TextEdit_TrainingOrganizationOther.Properties.Mask.ShowPlaceHolders = false;
			this.TextEdit_TrainingOrganizationOther.Properties.MaxLength = 50;
			this.TextEdit_TrainingOrganizationOther.Size = new System.Drawing.Size(192, 20);
			this.TextEdit_TrainingOrganizationOther.StyleController = this.LayoutControl1;
			this.TextEdit_TrainingOrganizationOther.TabIndex = 5;
			this.TextEdit_TrainingOrganizationOther.ToolTip = "If \"Other\", enter the organization name here";
			//
			//LayoutControl1
			//
			this.LayoutControl1.Anchor = (AnchorStyles)(((AnchorStyles.Top | AnchorStyles.Bottom) | AnchorStyles.Left) | AnchorStyles.Right);
			this.LayoutControl1.Controls.Add(this.CheckEdit_Certificate);
			this.LayoutControl1.Controls.Add(this.TextEdit_TrainingTitle);
			this.LayoutControl1.Controls.Add(this.CheckEdit_ActiveFlag);
			this.LayoutControl1.Controls.Add(this.LookupEdit_TrainingOrganization);
			this.LayoutControl1.Controls.Add(this.TextEdit_TrainingOrganizationOther);
			this.LayoutControl1.Controls.Add(this.LookUpEdit_TrainingSponsor);
			this.LayoutControl1.Controls.Add(this.TextEdit_TrainingSponsorOther);
			this.LayoutControl1.Controls.Add(this.TextEdit_TrainingDuration);
			this.LayoutControl1.Location = new System.Drawing.Point(0, 0);
			this.LayoutControl1.Name = "LayoutControl1";
			this.LayoutControl1.Root = this.LayoutControlGroup1;
			this.LayoutControl1.Size = new System.Drawing.Size(312, 202);
			this.LayoutControl1.TabIndex = 17;
			this.LayoutControl1.Text = "LayoutControl1";
			//
			//CheckEdit_Certificate
			//
			this.CheckEdit_Certificate.Location = new System.Drawing.Point(159, 171);
			this.CheckEdit_Certificate.Name = "CheckEdit_Certificate";
			this.CheckEdit_Certificate.Properties.Caption = "Certified Course";
			this.CheckEdit_Certificate.Size = new System.Drawing.Size(141, 19);
			this.CheckEdit_Certificate.StyleController = this.LayoutControl1;
			this.CheckEdit_Certificate.TabIndex = 15;
			//
			//TextEdit_TrainingTitle
			//
			this.TextEdit_TrainingTitle.Location = new System.Drawing.Point(108, 12);
			this.TextEdit_TrainingTitle.Name = "TextEdit_TrainingTitle";
			this.TextEdit_TrainingTitle.Properties.Mask.AutoComplete = DevExpress.XtraEditors.Mask.AutoCompleteType.None;
			this.TextEdit_TrainingTitle.Properties.Mask.BeepOnError = true;
			this.TextEdit_TrainingTitle.Properties.Mask.EditMask = "[^ ].+";
			this.TextEdit_TrainingTitle.Properties.Mask.IgnoreMaskBlank = false;
			this.TextEdit_TrainingTitle.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.RegEx;
			this.TextEdit_TrainingTitle.Properties.Mask.ShowPlaceHolders = false;
			this.TextEdit_TrainingTitle.Properties.MaxLength = 50;
			this.TextEdit_TrainingTitle.Size = new System.Drawing.Size(192, 20);
			this.TextEdit_TrainingTitle.StyleController = this.LayoutControl1;
			this.TextEdit_TrainingTitle.TabIndex = 1;
			this.TextEdit_TrainingTitle.ToolTip = "Title associated with the training course";
			//
			//CheckEdit_ActiveFlag
			//
			this.CheckEdit_ActiveFlag.Location = new System.Drawing.Point(12, 171);
			this.CheckEdit_ActiveFlag.Name = "CheckEdit_ActiveFlag";
			this.CheckEdit_ActiveFlag.Properties.Caption = "Active";
			this.CheckEdit_ActiveFlag.Size = new System.Drawing.Size(143, 19);
			this.CheckEdit_ActiveFlag.StyleController = this.LayoutControl1;
			this.CheckEdit_ActiveFlag.TabIndex = 14;
			this.CheckEdit_ActiveFlag.ToolTip = "Check if the course may be selected for counselors. If not active, the course may" + " not be chosen for a counselor's training.";
			//
			//LookupEdit_TrainingOrganization
			//
			this.LookupEdit_TrainingOrganization.Location = new System.Drawing.Point(108, 36);
			this.LookupEdit_TrainingOrganization.Name = "LookupEdit_TrainingOrganization";
			this.LookupEdit_TrainingOrganization.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] { new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo) });
			this.LookupEdit_TrainingOrganization.Properties.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
				new DevExpress.XtraEditors.Controls.LookUpColumnInfo("name", "ID", 20, DevExpress.Utils.FormatType.None, "", false, DevExpress.Utils.HorzAlignment.Default),
				new DevExpress.XtraEditors.Controls.LookUpColumnInfo("longDesc", "Description", 20, DevExpress.Utils.FormatType.None, "", true, DevExpress.Utils.HorzAlignment.Default, DevExpress.Data.ColumnSortOrder.Ascending)
			});
			this.LookupEdit_TrainingOrganization.Properties.DisplayMember = "longDesc";
			this.LookupEdit_TrainingOrganization.Properties.MaxLength = 10;
			this.LookupEdit_TrainingOrganization.Properties.NullText = "";
			this.LookupEdit_TrainingOrganization.Properties.ShowFooter = false;
			this.LookupEdit_TrainingOrganization.Properties.ShowHeader = false;
			this.LookupEdit_TrainingOrganization.Properties.ValueMember = "name";
			this.LookupEdit_TrainingOrganization.Size = new System.Drawing.Size(192, 20);
			this.LookupEdit_TrainingOrganization.StyleController = this.LayoutControl1;
			this.LookupEdit_TrainingOrganization.TabIndex = 3;
			this.LookupEdit_TrainingOrganization.ToolTip = "Organization that offered the training";
			this.LookupEdit_TrainingOrganization.Properties.SortColumnIndex = 1;
			//
			//LookUpEdit_TrainingSponsor
			//
			this.LookUpEdit_TrainingSponsor.Location = new System.Drawing.Point(108, 84);
			this.LookUpEdit_TrainingSponsor.Name = "LookUpEdit_TrainingSponsor";
			this.LookUpEdit_TrainingSponsor.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] { new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo) });
			this.LookUpEdit_TrainingSponsor.Properties.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
				new DevExpress.XtraEditors.Controls.LookUpColumnInfo("name", "ID", 20, DevExpress.Utils.FormatType.None, "f0", false, DevExpress.Utils.HorzAlignment.Default),
				new DevExpress.XtraEditors.Controls.LookUpColumnInfo("longDesc", "Description", 20, DevExpress.Utils.FormatType.None, "", true, DevExpress.Utils.HorzAlignment.Default, DevExpress.Data.ColumnSortOrder.Ascending)
			});
			this.LookUpEdit_TrainingSponsor.Properties.DisplayMember = "longDesc";
			this.LookUpEdit_TrainingSponsor.Properties.MaxLength = 10;
			this.LookUpEdit_TrainingSponsor.Properties.NullText = "";
			this.LookUpEdit_TrainingSponsor.Properties.ShowFooter = false;
			this.LookUpEdit_TrainingSponsor.Properties.ShowHeader = false;
			this.LookUpEdit_TrainingSponsor.Properties.ValueMember = "name";
			this.LookUpEdit_TrainingSponsor.Size = new System.Drawing.Size(192, 20);
			this.LookUpEdit_TrainingSponsor.StyleController = this.LayoutControl1;
			this.LookUpEdit_TrainingSponsor.TabIndex = 7;
			this.LookUpEdit_TrainingSponsor.ToolTip = "Organization who sponsored (paid for) the course";
			this.LookUpEdit_TrainingSponsor.Properties.SortColumnIndex = 1;
			//
			//TextEdit_TrainingSponsorOther
			//
			this.TextEdit_TrainingSponsorOther.Enabled = false;
			this.TextEdit_TrainingSponsorOther.Location = new System.Drawing.Point(108, 108);
			this.TextEdit_TrainingSponsorOther.Name = "TextEdit_TrainingSponsorOther";
			this.TextEdit_TrainingSponsorOther.Properties.Mask.AutoComplete = DevExpress.XtraEditors.Mask.AutoCompleteType.None;
			this.TextEdit_TrainingSponsorOther.Properties.Mask.BeepOnError = true;
			this.TextEdit_TrainingSponsorOther.Properties.Mask.EditMask = "[^ ].+";
			this.TextEdit_TrainingSponsorOther.Properties.Mask.IgnoreMaskBlank = false;
			this.TextEdit_TrainingSponsorOther.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.RegEx;
			this.TextEdit_TrainingSponsorOther.Properties.Mask.ShowPlaceHolders = false;
			this.TextEdit_TrainingSponsorOther.Properties.MaxLength = 50;
			this.TextEdit_TrainingSponsorOther.Size = new System.Drawing.Size(192, 20);
			this.TextEdit_TrainingSponsorOther.StyleController = this.LayoutControl1;
			this.TextEdit_TrainingSponsorOther.TabIndex = 9;
			this.TextEdit_TrainingSponsorOther.ToolTip = "If \"Other\", enter the name of the sponsor organization here";
			//
			//TextEdit_TrainingDuration
			//
			this.TextEdit_TrainingDuration.Location = new System.Drawing.Point(108, 132);
			this.TextEdit_TrainingDuration.Name = "TextEdit_TrainingDuration";
			this.TextEdit_TrainingDuration.Properties.Appearance.Options.UseTextOptions = true;
			this.TextEdit_TrainingDuration.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
			this.TextEdit_TrainingDuration.Properties.DisplayFormat.FormatString = "f0";
			this.TextEdit_TrainingDuration.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
			this.TextEdit_TrainingDuration.Properties.EditFormat.FormatString = "f0";
			this.TextEdit_TrainingDuration.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
			this.TextEdit_TrainingDuration.Properties.Mask.BeepOnError = true;
			this.TextEdit_TrainingDuration.Properties.Mask.EditMask = "f0";
			this.TextEdit_TrainingDuration.Properties.Mask.IgnoreMaskBlank = false;
			this.TextEdit_TrainingDuration.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
			this.TextEdit_TrainingDuration.Properties.Mask.ShowPlaceHolders = false;
			this.TextEdit_TrainingDuration.Properties.MaxLength = 4;
			this.TextEdit_TrainingDuration.Size = new System.Drawing.Size(50, 20);
			this.TextEdit_TrainingDuration.StyleController = this.LayoutControl1;
			this.TextEdit_TrainingDuration.TabIndex = 13;
			this.TextEdit_TrainingDuration.ToolTip = "Duration for the class (unspecified)";
			//
			//LayoutControlGroup1
			//
			this.LayoutControlGroup1.CustomizationFormText = "LayoutControlGroup1";
			this.LayoutControlGroup1.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.True;
			this.LayoutControlGroup1.GroupBordersVisible = false;
			this.LayoutControlGroup1.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
				this.LayoutControlItem1,
				this.LayoutControlItem2,
				this.LayoutControlItem3,
				this.LayoutControlItem4,
				this.LayoutControlItem5,
				this.LayoutControlItem7,
				this.LayoutControlItem8,
				this.EmptySpaceItem1,
				this.LayoutControlItem6,
				this.EmptySpaceItem2
			});
			this.LayoutControlGroup1.Location = new System.Drawing.Point(0, 0);
			this.LayoutControlGroup1.Name = "LayoutControlGroup1";
			this.LayoutControlGroup1.Size = new System.Drawing.Size(312, 202);
			this.LayoutControlGroup1.Text = "LayoutControlGroup1";
			this.LayoutControlGroup1.TextVisible = false;
			//
			//LayoutControlItem1
			//
			this.LayoutControlItem1.Control = this.TextEdit_TrainingTitle;
			this.LayoutControlItem1.CustomizationFormText = "Title";
			this.LayoutControlItem1.Location = new System.Drawing.Point(0, 0);
			this.LayoutControlItem1.Name = "LayoutControlItem1";
			this.LayoutControlItem1.Size = new System.Drawing.Size(292, 24);
			this.LayoutControlItem1.Text = "Title";
			this.LayoutControlItem1.TextSize = new System.Drawing.Size(92, 13);
			//
			//LayoutControlItem2
			//
			this.LayoutControlItem2.Control = this.LookupEdit_TrainingOrganization;
			this.LayoutControlItem2.CustomizationFormText = "Organization";
			this.LayoutControlItem2.Location = new System.Drawing.Point(0, 24);
			this.LayoutControlItem2.Name = "LayoutControlItem2";
			this.LayoutControlItem2.Size = new System.Drawing.Size(292, 24);
			this.LayoutControlItem2.Text = "Organization";
			this.LayoutControlItem2.TextSize = new System.Drawing.Size(92, 13);
			//
			//LayoutControlItem3
			//
			this.LayoutControlItem3.Control = this.TextEdit_TrainingOrganizationOther;
			this.LayoutControlItem3.CustomizationFormText = "Other Organization";
			this.LayoutControlItem3.Location = new System.Drawing.Point(0, 48);
			this.LayoutControlItem3.Name = "LayoutControlItem3";
			this.LayoutControlItem3.Size = new System.Drawing.Size(292, 24);
			this.LayoutControlItem3.Text = "Other Organization";
			this.LayoutControlItem3.TextSize = new System.Drawing.Size(92, 13);
			//
			//LayoutControlItem4
			//
			this.LayoutControlItem4.Control = this.LookUpEdit_TrainingSponsor;
			this.LayoutControlItem4.CustomizationFormText = "Sponsor";
			this.LayoutControlItem4.Location = new System.Drawing.Point(0, 72);
			this.LayoutControlItem4.Name = "LayoutControlItem4";
			this.LayoutControlItem4.Size = new System.Drawing.Size(292, 24);
			this.LayoutControlItem4.Text = "Sponsor";
			this.LayoutControlItem4.TextSize = new System.Drawing.Size(92, 13);
			//
			//LayoutControlItem5
			//
			this.LayoutControlItem5.Control = this.TextEdit_TrainingSponsorOther;
			this.LayoutControlItem5.CustomizationFormText = "Other Sponsor";
			this.LayoutControlItem5.Location = new System.Drawing.Point(0, 96);
			this.LayoutControlItem5.Name = "LayoutControlItem5";
			this.LayoutControlItem5.Size = new System.Drawing.Size(292, 24);
			this.LayoutControlItem5.Text = "Other Sponsor";
			this.LayoutControlItem5.TextSize = new System.Drawing.Size(92, 13);
			//
			//LayoutControlItem7
			//
			this.LayoutControlItem7.Control = this.TextEdit_TrainingDuration;
			this.LayoutControlItem7.CustomizationFormText = "Duration";
			this.LayoutControlItem7.Location = new System.Drawing.Point(0, 120);
			this.LayoutControlItem7.Name = "LayoutControlItem7";
			this.LayoutControlItem7.Size = new System.Drawing.Size(150, 24);
			this.LayoutControlItem7.Text = "Duration";
			this.LayoutControlItem7.TextSize = new System.Drawing.Size(92, 13);
			//
			//LayoutControlItem8
			//
			this.LayoutControlItem8.Control = this.CheckEdit_ActiveFlag;
			this.LayoutControlItem8.CustomizationFormText = "Active Checkbox";
			this.LayoutControlItem8.Location = new System.Drawing.Point(0, 159);
			this.LayoutControlItem8.Name = "LayoutControlItem8";
			this.LayoutControlItem8.Size = new System.Drawing.Size(147, 23);
			this.LayoutControlItem8.Text = "Active";
			this.LayoutControlItem8.TextSize = new System.Drawing.Size(0, 0);
			this.LayoutControlItem8.TextToControlDistance = 0;
			this.LayoutControlItem8.TextVisible = false;
			//
			//EmptySpaceItem1
			//
			this.EmptySpaceItem1.AllowHotTrack = false;
			this.EmptySpaceItem1.CustomizationFormText = "EmptySpaceItem1";
			this.EmptySpaceItem1.Location = new System.Drawing.Point(0, 144);
			this.EmptySpaceItem1.Name = "EmptySpaceItem1";
			this.EmptySpaceItem1.Size = new System.Drawing.Size(292, 15);
			this.EmptySpaceItem1.Text = "EmptySpaceItem1";
			this.EmptySpaceItem1.TextSize = new System.Drawing.Size(0, 0);
			//
			//LayoutControlItem6
			//
			this.LayoutControlItem6.Control = this.CheckEdit_Certificate;
			this.LayoutControlItem6.CustomizationFormText = "LayoutControlItem6";
			this.LayoutControlItem6.Location = new System.Drawing.Point(147, 159);
			this.LayoutControlItem6.Name = "LayoutControlItem6";
			this.LayoutControlItem6.Size = new System.Drawing.Size(145, 23);
			this.LayoutControlItem6.Text = "LayoutControlItem6";
			this.LayoutControlItem6.TextSize = new System.Drawing.Size(0, 0);
			this.LayoutControlItem6.TextToControlDistance = 0;
			this.LayoutControlItem6.TextVisible = false;
			//
			//EmptySpaceItem2
			//
			this.EmptySpaceItem2.AllowHotTrack = false;
			this.EmptySpaceItem2.CustomizationFormText = "EmptySpaceItem2";
			this.EmptySpaceItem2.Location = new System.Drawing.Point(150, 120);
			this.EmptySpaceItem2.Name = "EmptySpaceItem2";
			this.EmptySpaceItem2.Size = new System.Drawing.Size(142, 24);
			this.EmptySpaceItem2.Text = "EmptySpaceItem2";
			this.EmptySpaceItem2.TextSize = new System.Drawing.Size(0, 0);
			//
			//EditForm
			//
			this.AutoScaleDimensions = new System.Drawing.SizeF(6f, 13f);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(409, 203);
			this.Controls.Add(this.LayoutControl1);
			this.Name = "EditForm";
			this.Text = "Housing Training Course";
			this.Controls.SetChildIndex(this.simpleButton_OK, 0);
			this.Controls.SetChildIndex(this.simpleButton_Cancel, 0);
			this.Controls.SetChildIndex(this.LayoutControl1, 0);
			((System.ComponentModel.ISupportInitialize)this.DxErrorProvider1).EndInit();
			((System.ComponentModel.ISupportInitialize)this.TextEdit_TrainingOrganizationOther.Properties).EndInit();
			((System.ComponentModel.ISupportInitialize)this.LayoutControl1).EndInit();
			this.LayoutControl1.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)this.CheckEdit_Certificate.Properties).EndInit();
			((System.ComponentModel.ISupportInitialize)this.TextEdit_TrainingTitle.Properties).EndInit();
			((System.ComponentModel.ISupportInitialize)this.CheckEdit_ActiveFlag.Properties).EndInit();
			((System.ComponentModel.ISupportInitialize)this.LookupEdit_TrainingOrganization.Properties).EndInit();
			((System.ComponentModel.ISupportInitialize)this.LookUpEdit_TrainingSponsor.Properties).EndInit();
			((System.ComponentModel.ISupportInitialize)this.TextEdit_TrainingSponsorOther.Properties).EndInit();
			((System.ComponentModel.ISupportInitialize)this.TextEdit_TrainingDuration.Properties).EndInit();
			((System.ComponentModel.ISupportInitialize)this.LayoutControlGroup1).EndInit();
			((System.ComponentModel.ISupportInitialize)this.LayoutControlItem1).EndInit();
			((System.ComponentModel.ISupportInitialize)this.LayoutControlItem2).EndInit();
			((System.ComponentModel.ISupportInitialize)this.LayoutControlItem3).EndInit();
			((System.ComponentModel.ISupportInitialize)this.LayoutControlItem4).EndInit();
			((System.ComponentModel.ISupportInitialize)this.LayoutControlItem5).EndInit();
			((System.ComponentModel.ISupportInitialize)this.LayoutControlItem7).EndInit();
			((System.ComponentModel.ISupportInitialize)this.LayoutControlItem8).EndInit();
			((System.ComponentModel.ISupportInitialize)this.EmptySpaceItem1).EndInit();
			((System.ComponentModel.ISupportInitialize)this.LayoutControlItem6).EndInit();
			((System.ComponentModel.ISupportInitialize)this.EmptySpaceItem2).EndInit();
			this.ResumeLayout(false);
		}

		private DevExpress.XtraEditors.TextEdit TextEdit_TrainingOrganizationOther;
		private DevExpress.XtraEditors.TextEdit TextEdit_TrainingTitle;
		private DevExpress.XtraEditors.TextEdit TextEdit_TrainingSponsorOther;
		private DevExpress.XtraEditors.TextEdit TextEdit_TrainingDuration;
		private DevExpress.XtraEditors.LookUpEdit LookupEdit_TrainingOrganization;
		private DevExpress.XtraEditors.LookUpEdit LookUpEdit_TrainingSponsor;
		private DevExpress.XtraEditors.CheckEdit CheckEdit_ActiveFlag;
        private DevExpress.XtraEditors.CheckEdit CheckEdit_Certificate;
		private DevExpress.XtraLayout.LayoutControl LayoutControl1;
		private DevExpress.XtraLayout.LayoutControlGroup LayoutControlGroup1;
		private DevExpress.XtraLayout.LayoutControlItem LayoutControlItem1;
		private DevExpress.XtraLayout.LayoutControlItem LayoutControlItem2;
		private DevExpress.XtraLayout.LayoutControlItem LayoutControlItem3;
		private DevExpress.XtraLayout.LayoutControlItem LayoutControlItem4;
		private DevExpress.XtraLayout.LayoutControlItem LayoutControlItem5;
		private DevExpress.XtraLayout.LayoutControlItem LayoutControlItem7;
		private DevExpress.XtraLayout.LayoutControlItem LayoutControlItem8;
		private DevExpress.XtraLayout.LayoutControlItem LayoutControlItem6;
        private DevExpress.XtraLayout.EmptySpaceItem EmptySpaceItem1;
        private DevExpress.XtraLayout.EmptySpaceItem EmptySpaceItem2;
	}
}
