using System;
using System.Linq;
using DebtPlus.LINQ;

namespace DebtPlus.UI.TableAdministration.CS.Housing.TrainingCourses
{
    internal partial class EditForm : Templates.EditTemplateForm
    {
        // Local storage
        private Housing_training_course record = null;

        private BusinessContext bc = null;

        /// <summary>
        /// Initialize the new form class
        /// </summary>
        internal EditForm()
            : base()
        {
            InitializeComponent();
        }

        /// <summary>
        /// Initialize the new form class
        /// </summary>
        internal EditForm(BusinessContext bc, Housing_training_course record)
            : this()
        {
            this.bc = bc;
            this.record = record;
            RegisterHandlers();
        }

        /// <summary>
        /// Register the event handlers
        /// </summary>
        private void RegisterHandlers()
        {
            Load += EditForm_Load;
            TextEdit_TrainingOrganizationOther.EditValueChanged += form_Changed;
            TextEdit_TrainingTitle.EditValueChanged += form_Changed;
            TextEdit_TrainingSponsorOther.EditValueChanged += form_Changed;
            TextEdit_TrainingDuration.EditValueChanged += form_Changed;
            LookupEdit_TrainingOrganization.EditValueChanged += form_Changed;
            LookUpEdit_TrainingSponsor.EditValueChanged += form_Changed;
            CheckEdit_ActiveFlag.CheckedChanged += form_Changed;
            CheckEdit_Certificate.CheckedChanged += form_Changed;

            LookupEdit_TrainingOrganization.EditValueChanged += LookupEdit_TrainingOrganization_EditValueChanged;
            LookUpEdit_TrainingSponsor.EditValueChanged += LookUpEdit_TrainingSponsor_EditValueChanged;
        }

        /// <summary>
        /// Remove the event handler registrations
        /// </summary>
        private void UnRegisterHandlers()
        {
            Load -= EditForm_Load;
            TextEdit_TrainingOrganizationOther.EditValueChanged -= form_Changed;
            TextEdit_TrainingTitle.EditValueChanged -= form_Changed;
            TextEdit_TrainingSponsorOther.EditValueChanged -= form_Changed;
            TextEdit_TrainingDuration.EditValueChanged -= form_Changed;
            LookupEdit_TrainingOrganization.EditValueChanged -= form_Changed;
            LookUpEdit_TrainingSponsor.EditValueChanged -= form_Changed;
            CheckEdit_ActiveFlag.CheckedChanged -= form_Changed;
            CheckEdit_Certificate.CheckedChanged -= form_Changed;

            LookupEdit_TrainingOrganization.EditValueChanged -= LookupEdit_TrainingOrganization_EditValueChanged;
            LookUpEdit_TrainingSponsor.EditValueChanged -= LookUpEdit_TrainingSponsor_EditValueChanged;
        }

        /// <summary>
        /// Process the LOAD event for the form
        /// </summary>
        private void EditForm_Load(object sender, EventArgs e)
        {
            UnRegisterHandlers();
            try
            {
                // Retrieve the possible entries from the ARM Reference table
                System.Collections.Generic.List<Housing_ARM_referenceInfo> armReferenceList = bc.Housing_ARM_referenceInfos.Where(s => (new Int32[] { 26, 27 }).Contains(s.groupId)).ToList();
                LookupEdit_TrainingOrganization.Properties.DataSource = armReferenceList.Where(s => s.groupId == 26).ToList();
                LookUpEdit_TrainingSponsor.Properties.DataSource = armReferenceList.Where(s => s.groupId == 27).ToList();

                // Load the record contents
                TextEdit_TrainingOrganizationOther.EditValue = record.TrainingOrganizationOther;
                TextEdit_TrainingTitle.EditValue = record.TrainingTitle;
                TextEdit_TrainingSponsorOther.EditValue = record.TrainingOrganizationOther;
                TextEdit_TrainingDuration.EditValue = record.TrainingDuration;
                LookupEdit_TrainingOrganization.EditValue = record.TrainingOrganization;
                LookUpEdit_TrainingSponsor.EditValue = record.TrainingSponsor;
                CheckEdit_ActiveFlag.Checked = record.ActiveFlag;
                CheckEdit_Certificate.Checked = record.Certificate;

                simpleButton_OK.Enabled = !HasErrors();
            }
            finally
            {
                RegisterHandlers();
            }
        }

        /// <summary>
        /// Process the change in the training sponsor lookup entry
        /// </summary>
        private void LookUpEdit_TrainingSponsor_EditValueChanged(object sender, EventArgs e)
        {
            TextEdit_TrainingSponsorOther.Enabled = shouldEnableTrainingSponsorOther();
        }

        /// <summary>
        /// Should the Other Training Sponsor be enabled?
        /// </summary>
        private bool shouldEnableTrainingSponsorOther()
        {
            var selectedItem = LookUpEdit_TrainingSponsor.GetSelectedDataRow() as Housing_ARM_referenceInfo;
            if (selectedItem != null)
            {
                if (selectedItem.name.StartsWith("OTH"))
                {
                    return true;
                }
            }
            return false;
        }

        /// <summary>
        /// Process the change in the training organization lookup entry
        /// </summary>
        private void LookupEdit_TrainingOrganization_EditValueChanged(object sender, EventArgs e)
        {
            TextEdit_TrainingOrganizationOther.Enabled = shouldEnableTrainingOrganizationOther();
        }

        /// <summary>
        /// Should the Other Training Organization be enabled?
        /// </summary>
        private bool shouldEnableTrainingOrganizationOther()
        {
            var selectedItem = LookupEdit_TrainingOrganization.GetSelectedDataRow() as Housing_ARM_referenceInfo;
            if (selectedItem != null)
            {
                if (selectedItem.name.StartsWith("OTH"))
                {
                    return true;
                }
            }
            return false;
        }

        /// <summary>
        /// Are the controls containing a valid record?
        /// </summary>
        private bool HasErrors()
        {
            // A title is required
            if (string.IsNullOrWhiteSpace(DebtPlus.Utils.Nulls.v_String(TextEdit_TrainingTitle.EditValue)))
            {
                return true;
            }

            // The organization is required if enabled
            if (shouldEnableTrainingOrganizationOther() && string.IsNullOrWhiteSpace(DebtPlus.Utils.Nulls.v_String(TextEdit_TrainingOrganizationOther.EditValue)))
            {
                return true;
            }

            // The sponsor is required if enabled
            if (shouldEnableTrainingSponsorOther() && string.IsNullOrWhiteSpace(DebtPlus.Utils.Nulls.v_String(TextEdit_TrainingSponsorOther.EditValue)))
            {
                return true;
            }

            // The duration is required
            Int32 duration = default(Int32);
            if (!Int32.TryParse(DebtPlus.Utils.Nulls.v_String(TextEdit_TrainingDuration.EditValue), out duration) || duration < 0)
            {
                return true;
            }

            return false;
        }

        /// <summary>
        /// Process a change on a control
        /// </summary>
        private void form_Changed(object sender, System.EventArgs e)
        {
            simpleButton_OK.Enabled = !HasErrors();
        }

        /// <summary>
        /// Process the OK button CLICK event
        /// </summary>
        protected override void simpleButton_OK_Click(object sender, EventArgs e)
        {
            record.TrainingOrganizationOther = DebtPlus.Utils.Nulls.v_String(TextEdit_TrainingOrganizationOther.EditValue);
            record.TrainingTitle = DebtPlus.Utils.Nulls.v_String(TextEdit_TrainingTitle.EditValue);
            record.TrainingOrganizationOther = DebtPlus.Utils.Nulls.v_String(TextEdit_TrainingSponsorOther.EditValue);
            record.TrainingDuration = DebtPlus.Utils.Nulls.v_Int32(TextEdit_TrainingDuration.EditValue).GetValueOrDefault();
            record.TrainingOrganization = DebtPlus.Utils.Nulls.v_String(LookupEdit_TrainingOrganization.EditValue);
            record.TrainingSponsor = DebtPlus.Utils.Nulls.v_String(LookUpEdit_TrainingSponsor.EditValue);
            record.ActiveFlag = CheckEdit_ActiveFlag.Checked;
            record.Certificate = CheckEdit_Certificate.Checked;

            base.simpleButton_OK_Click(sender, e);
        }
    }
}