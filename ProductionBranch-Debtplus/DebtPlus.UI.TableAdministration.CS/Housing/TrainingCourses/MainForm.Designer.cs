namespace DebtPlus.UI.TableAdministration.CS.Housing.TrainingCourses
{
    partial class MainForm
    {
        //Form overrides dispose to clean up the component list.
        [System.Diagnostics.DebuggerNonUserCode]
        protected override void Dispose(bool disposing)
        {
            try
            {
                if (disposing)
                {
                    if (components != null) components.Dispose();
                    if (bc         != null) bc.Dispose();
                }
                components = null;
                bc         = null;
            }
            finally
            {
                base.Dispose(disposing);
            }
        }

        //Required by the Windows Form Designer
        private System.ComponentModel.IContainer components = null;

		//NOTE: The following procedure is required by the Windows Form Designer
		//It can be modified using the Windows Form Designer.  
		//Do not modify it using the code editor.
		[System.Diagnostics.DebuggerStepThrough]
		private void InitializeComponent()
		{
			this.gridColumn_ID = new DevExpress.XtraGrid.Columns.GridColumn();
			this.gridColumn_TrainingTitle = new DevExpress.XtraGrid.Columns.GridColumn();
			((System.ComponentModel.ISupportInitialize)this.DxErrorProvider1).BeginInit();
			this.SuspendLayout();
			//
			//gridColumn_ID
			//
			this.gridColumn_ID.Caption = "ID";
			this.gridColumn_ID.FieldName = "Id";
			this.gridColumn_ID.Name = "gridColumn_ID";
			this.gridColumn_ID.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.True;
			this.gridColumn_ID.Visible = true;
			this.gridColumn_ID.VisibleIndex = 0;
			this.gridColumn_ID.Width = 50;
			//
			//gridColumn_TrainingTitle
			//
			this.gridColumn_TrainingTitle.Caption = "Title";
			this.gridColumn_TrainingTitle.FieldName = "TrainingTitle";
			this.gridColumn_TrainingTitle.Name = "gridColumn_TrainingTitle";
			this.gridColumn_TrainingTitle.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.True;
			this.gridColumn_TrainingTitle.Visible = true;
			this.gridColumn_TrainingTitle.VisibleIndex = 1;
			this.gridColumn_TrainingTitle.Width = 123;
			this.gridColumn_TrainingTitle.SortIndex = 0;
			this.gridColumn_TrainingTitle.SortMode = DevExpress.XtraGrid.ColumnSortMode.DisplayText;
			this.gridColumn_TrainingTitle.SortOrder = DevExpress.Data.ColumnSortOrder.Ascending;
			//
			//gridView1
			//
			this.gridView1.Columns.Clear();
			this.gridView1.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
				this.gridColumn_ID,
				this.gridColumn_TrainingTitle
			});
			//
			//MainForm
			//
			this.AutoScaleDimensions = new System.Drawing.SizeF(6f, 13f);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(528, 294);
			this.LookAndFeel.UseDefaultLookAndFeel = true;
			this.Name = "MainForm";
			this.Text = "Training Courses";
			((System.ComponentModel.ISupportInitialize)this.DxErrorProvider1).EndInit();
			this.ResumeLayout(false);
		}
		private DevExpress.XtraGrid.Columns.GridColumn gridColumn_ID;
		private DevExpress.XtraGrid.Columns.GridColumn gridColumn_TrainingTitle;
	}
}

