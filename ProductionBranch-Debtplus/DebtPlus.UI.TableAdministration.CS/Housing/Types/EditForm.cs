using System;
using DebtPlus.LINQ;

namespace DebtPlus.UI.TableAdministration.CS.Housing.Types
{
    internal partial class EditForm : Templates.EditTemplateForm
    {
        private HousingType record;

        internal EditForm()
            : base()
        {
            InitializeComponent();
        }

        internal EditForm(HousingType record)
            : this()
        {
            this.record = record;
            RegisterHandlers();
        }

        private void RegisterHandlers()
        {
            Load += EditForm_Load;
            TextEdit_description.EditValueChanged += form_Changed;
        }

        private void UnRegisterHandlers()
        {
            Load -= EditForm_Load;
            TextEdit_description.EditValueChanged -= form_Changed;
        }

        private void EditForm_Load(object sender, EventArgs e)
        {
            UnRegisterHandlers();
            try
            {
                LabelControl_ID.Text = record.Id <= 0 ? "NEW" : record.Id.ToString();

                TextEdit_description.EditValue = record.description;
                textEdit_hpf.EditValue = record.hpf;
                CheckEdit_SingleFamilyHomeIND.Checked = record.SingleFamilyHomeIND;
                CheckEdit_default.Checked = record.Default;
                CheckEdit_ActiveFlag.Checked = record.ActiveFlag;

                simpleButton_OK.Enabled = !HasErrors();
            }
            finally
            {
                RegisterHandlers();
            }
        }

        private bool HasErrors()
        {
            if (string.IsNullOrEmpty(DebtPlus.Utils.Nulls.v_String(TextEdit_description.EditValue)))
            {
                return true;
            }

            return false;
        }

        private void form_Changed(object sender, System.EventArgs e)
        {
            simpleButton_OK.Enabled = !HasErrors();
        }

        protected override void simpleButton_OK_Click(object sender, EventArgs e)
        {
            record.description = DebtPlus.Utils.Nulls.v_String(TextEdit_description.EditValue);
            record.hpf = DebtPlus.Utils.Nulls.v_String(textEdit_hpf.EditValue);
            record.Default = CheckEdit_default.Checked;
            record.ActiveFlag = CheckEdit_ActiveFlag.Checked;

            base.simpleButton_OK_Click(sender, e);
        }
    }
}