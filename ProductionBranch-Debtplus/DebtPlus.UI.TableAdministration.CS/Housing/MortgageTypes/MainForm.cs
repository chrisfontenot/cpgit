﻿using System;
using System.Linq;
using DebtPlus.LINQ;

namespace DebtPlus.UI.TableAdministration.CS.Housing.MortgageTypes
{
    public partial class MainForm : Templates.MainForm
    {
        /// <summary>
        /// List of the records to be edited in the program
        /// </summary>
        private System.Collections.Generic.List<Housing_MortgageType> colRecords;

        /// <summary>
        /// Initialize the new class
        /// </summary>
        /// <param name="dc">Pointer to the data context in the mainline</param>
        public MainForm()
            : base()
        {
            InitializeComponent();
            RegisterHandlers();
        }

        private void RegisterHandlers()
        {
            Load += MainForm_Load;
        }

        private void UnRegisterHandlers()
        {
            Load -= MainForm_Load;
        }

        /// <summary>
        /// Process the load event on the form
        /// </summary>
        private void MainForm_Load(object sender, System.EventArgs e)
        {
            using (var cm = new DebtPlus.UI.Common.CursorManager())
            {
                try
                {
                    // Retrieve the list of menu items
                    using (var drm = new DebtPlus.LINQ.BusinessContext())
                    {
                        colRecords = drm.Housing_MortgageTypes.ToList();
                        gridControl1.DataSource = colRecords;
                    }
                }
                catch (Exception ex)
                {
                    DebtPlus.UI.Common.ErrorHandling.HandleErrors(ex, "Error retrieving database information");
                }
            }
        }

        /// <summary>
        /// Handle the EDIT sequence
        /// </summary>
        /// <param name="obj"></param>
        protected override void UpdateRecord(object obj)
        {
            var record = obj as Housing_MortgageType;
            if (record == null)
            {
                return;
            }

            bool previousDefault = record.Default;
            using (var frm = new EditForm(record))
            {
                if (frm.ShowDialog() != System.Windows.Forms.DialogResult.OK)
                {
                    return;
                }
            }

            // Save the new record
            using (var bc = new BusinessContext())
            {
                // If the default was changed from FALSE to TRUE then clear the other defaults
                if (record.Default && !previousDefault)
                {
                    bc.Housing_MortgageTypes.Where(s => s.Default).ToList().ForEach(s => s.Default = false);
                    colRecords.Where(s => s.Default && s.Id != record.Id).ToList().ForEach(s => s.Default = false);
                }

                var q = bc.Housing_MortgageTypes.Where(s => s.Id == record.Id).FirstOrDefault();
                if (q != null)
                {
                    q.ActiveFlag = record.ActiveFlag;
                    q.Default = record.Default;
                    q.description = record.description;
                    q.hud_9902_section = record.hud_9902_section;
                }
                bc.SubmitChanges();
                gridControl1.RefreshDataSource();
            }
        }

        /// <summary>
        /// Handle the DELETE sequence
        /// </summary>
        /// <param name="obj"></param>
        protected override void DeleteRecord(object obj)
        {
            // Find the record to purge
            var record = obj as Housing_MortgageType;
            if (record == null)
            {
                return;
            }

            // Ask for confirmation before removing a record
            if (DebtPlus.Data.Prompts.RequestConfirmation_Delete() != System.Windows.Forms.DialogResult.Yes)
            {
                return;
            }

            // Remove the record form the database and the list
            using (var bc = new BusinessContext())
            {
                var q = bc.Housing_MortgageTypes.Where(s => s.Id == record.Id).FirstOrDefault();
                if (q != null)
                {
                    bc.Housing_MortgageTypes.DeleteOnSubmit(q);
                    bc.SubmitChanges();
                    colRecords.Remove(record);
                    gridControl1.RefreshDataSource();
                }
            }
        }

        /// <summary>
        /// Handle the CREATE sequence
        /// </summary>
        protected override void CreateRecord()
        {
            var record = new Housing_MortgageType()
            {
                Default          = false,
                ActiveFlag       = true,
                description      = string.Empty,
                hud_9902_section = null
            };

            using (var frm = new EditForm(record))
            {
                if (frm.ShowDialog() != System.Windows.Forms.DialogResult.OK)
                {
                    return;
                }
            }

            // Save the new record
            using (var bc = new BusinessContext())
            {
                // If the default was changed from FALSE to TRUE then clear the other defaults
                if (record.Default)
                {
                    bc.Housing_MortgageTypes.Where(s => s.Default).ToList().ForEach(s => s.Default = false);
                    colRecords.Where(s => s.Default).ToList().ForEach(s => s.Default = false);
                }

                bc.Housing_MortgageTypes.InsertOnSubmit(record);
                bc.SubmitChanges();
                colRecords.Add(record);
                gridControl1.RefreshDataSource();
            }
        }
    }
}