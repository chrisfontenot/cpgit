#region "Copyright 2000-2012 DebtPlus, L.L.C."
//{*******************************************************************}
//{                                                                   }
//{       DebtPlus Debt Management System                             }
//{                                                                   }
//{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
//{       ALL RIGHTS RESERVED                                         }
//{                                                                   }
//{   The entire contents of this file is protected by U.S. and       }
//{   International Copyright Laws. Unauthorized reproduction,        }
//{   reverse-engineering, and distribution of all or any portion of  }
//{   the code contained in this file is strictly prohibited and may  }
//{   result in severe civil and criminal penalties and will be       }
//{   prosecuted to the maximum extent possible under the law.        }
//{                                                                   }
//{   RESTRICTIONS                                                    }
//{                                                                   }
//{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
//{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
//{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
//{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
//{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
//{                                                                   }
//{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
//{   ADDITIONAL RESTRICTIONS.                                        }
//{                                                                   }
//{*******************************************************************}

#endregion "Copyright 2000-2012 DebtPlus, L.L.C."

using System;
using System.Linq;
using DebtPlus.LINQ;

namespace DebtPlus.UI.TableAdministration.CS.Housing.PurposeOfVisitTypes
{
    public partial class MainForm : Templates.MainForm
    {
        /// <summary>
        /// List of the records to be edited in the program
        /// </summary>
        private System.Collections.Generic.List<Housing_PurposeOfVisitType> colRecords;

        private BusinessContext bc = new BusinessContext();

        /// <summary>
        /// Initialize the new class
        /// </summary>
        public MainForm()
            : base()
        {
            InitializeComponent();
            RegisterHandlers();
        }

        private void RegisterHandlers()
        {
            Load += MainForm_Load;
        }

        private void UnRegisterHandlers()
        {
            Load -= MainForm_Load;
        }

        /// <summary>
        /// Process the load event on the form
        /// </summary>
        private void MainForm_Load(object sender, System.EventArgs e)
        {
            gridColumn_hud_9902_section.DisplayFormat.Format = new hud9902SectionFormatter();
            gridColumn_hud_9902_section.GroupFormat.Format = new hud9902SectionFormatter();

            try
            {
                // Retrieve the list of menu items
                colRecords = bc.Housing_PurposeOfVisitTypes.ToList();
                gridControl1.DataSource = colRecords;
            }
            catch (Exception ex)
            {
                DebtPlus.UI.Common.ErrorHandling.HandleErrors(ex);
            }
        }

        /// <summary>
        /// Create the new record
        /// </summary>
        protected override void CreateRecord()
        {
            // Allocate the new record to be inserted.
            var record = DebtPlus.LINQ.Factory.Manufacture_Housing_PurposeOfVisitType();

            // Edit the new record. If OK then attempt to insert the record.
            using (var frm = new EditForm(record))
            {
                if (frm.ShowDialog() != System.Windows.Forms.DialogResult.OK)
                {
                    return;
                }

                // If this is the new default then clear the previous one.
                if (record.Default)
                {
                    foreach (var defaultRecord in colRecords.Where(s => s.Default))
                    {
                        defaultRecord.Default = false;
                    }
                    record.Default = true;
                }

                try
                {
                    // Add the record to the collection
                    bc.Housing_PurposeOfVisitTypes.InsertOnSubmit(record);
                    bc.SubmitChanges();
                    colRecords.Add(record);
                    gridView1.RefreshData();
                }
                catch (System.Data.SqlClient.SqlException ex)
                {
                    DebtPlus.UI.Common.ErrorHandling.HandleErrors(ex);
                }
            }
        }

        /// <summary>
        /// Update the existing record
        /// </summary>
        /// <param name="obj">Pointer to the desired record for the operation</param>
        protected override void UpdateRecord(object obj)
        {
            // Find the record
            var record = obj as Housing_PurposeOfVisitType;
            if (obj == null)
            {
                return;
            }

            // Remember the previous value for the default status. We will need it after the update.
            bool priorDefault = record.Default;

            // Update the record now.
            using (var frm = new EditForm(record))
            {
                if (frm.ShowDialog() != System.Windows.Forms.DialogResult.OK)
                {
                    return;
                }
            }

            try
            {
                // Turn off the default status of all other records in the collection.
                if (record.Default && !priorDefault)
                {
                    foreach (var defaultRecord in colRecords.Where(s => s.Default))
                    {
                        defaultRecord.Default = false;
                    }

                    // Reset the default status back to TRUE for the current record.
                    record.Default = true;
                }

                bc.SubmitChanges();
                gridView1.RefreshData();
            }
            catch (System.Data.SqlClient.SqlException ex)
            {
                DebtPlus.UI.Common.ErrorHandling.HandleErrors(ex);
            }
        }

        /// <summary>
        /// Delete the indicated record from the database
        /// </summary>
        /// <param name="obj">Pointer to the desired record for the operation</param>
        protected override void DeleteRecord(object obj)
        {
            // Find the record
            var record = obj as Housing_PurposeOfVisitType;
            if (obj == null)
            {
                return;
            }

            if (DebtPlus.Data.Prompts.RequestConfirmation_Delete() != System.Windows.Forms.DialogResult.Yes)
            {
                return;
            }

            try
            {
                bc.Housing_PurposeOfVisitTypes.DeleteOnSubmit(record);
                bc.SubmitChanges();

                colRecords.Remove(record);
                gridView1.RefreshData();
            }
            catch (System.Data.SqlClient.SqlException ex)
            {
                DebtPlus.UI.Common.ErrorHandling.HandleErrors(ex);
            }
        }

        /// <summary>
        /// Format the HUD 9902 section information for the display grid
        /// </summary>
        private class hud9902SectionFormatter : System.IFormatProvider, System.ICustomFormatter
        {
            /// <summary>
            /// Initialize the new class instance
            /// </summary>
            internal hud9902SectionFormatter() { }

            /// <summary>
            /// Return the format provider for the indicated type of value
            /// </summary>
            public object GetFormat(Type formatType)
            {
                return this;
            }

            /// <summary>
            /// Do the formatting logic to translate the value to a string.
            /// </summary>
            public string Format(string format, object arg, IFormatProvider formatProvider)
            {
                string hudKey = DebtPlus.Utils.Nulls.v_String(arg);
                if (!string.IsNullOrWhiteSpace(hudKey))
                {
                    var q = DebtPlus.LINQ.Cache.Housing_ARM_referenceInfo.getList().Find(s => s.groupId == 31 && s.name == hudKey);
                    if (q != null)
                    {
                        return q.longDesc;
                    }
                }
                return string.Empty;
            }
        }
    }
}