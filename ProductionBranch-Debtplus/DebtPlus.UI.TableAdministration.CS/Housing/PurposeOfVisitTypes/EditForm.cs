#region "Copyright 2000-2012 DebtPlus, L.L.C."
//{*******************************************************************}
//{                                                                   }
//{       DebtPlus Debt Management System                             }
//{                                                                   }
//{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
//{       ALL RIGHTS RESERVED                                         }
//{                                                                   }
//{   The entire contents of this file is protected by U.S. and       }
//{   International Copyright Laws. Unauthorized reproduction,        }
//{   reverse-engineering, and distribution of all or any portion of  }
//{   the code contained in this file is strictly prohibited and may  }
//{   result in severe civil and criminal penalties and will be       }
//{   prosecuted to the maximum extent possible under the law.        }
//{                                                                   }
//{   RESTRICTIONS                                                    }
//{                                                                   }
//{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
//{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
//{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
//{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
//{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
//{                                                                   }
//{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
//{   ADDITIONAL RESTRICTIONS.                                        }
//{                                                                   }
//{*******************************************************************}

#endregion "Copyright 2000-2012 DebtPlus, L.L.C."

using System;
using DebtPlus.LINQ;

namespace DebtPlus.UI.TableAdministration.CS.Housing.PurposeOfVisitTypes
{
    internal partial class EditForm : Templates.EditTemplateForm
    {
        private Housing_PurposeOfVisitType record;

        /// <summary>
        /// Initialize the new form class instance
        /// </summary>
        internal EditForm()
            : base()
        {
            InitializeComponent();
        }

        /// <summary>
        /// Initialize the new form class instance
        /// </summary>
        /// <param name="record"></param>
        internal EditForm(Housing_PurposeOfVisitType record)
            : this()
        {
            this.record = record;
            RegisterHandlers();
        }

        /// <summary>
        /// Register the event handlers
        /// </summary>
        private void RegisterHandlers()
        {
            Load += EditForm_Load;
            CheckEdit_ActiveFlag.CheckedChanged += HandleFormChanged;
            CheckEdit_Default.CheckedChanged += HandleFormChanged;
            lookupEdit_hud_9902_section.EditValueChanged += HandleFormChanged;
            TextEdit_description.EditValueChanged += HandleFormChanged;
            CalcEdit_HourlyGrantAmountUsed.EditValueChanged += HandleFormChanged;
            CalcEdit_HousingFeeAmount.EditValueChanged += HandleFormChanged;
        }

        /// <summary>
        /// Remove the event handler registrations
        /// </summary>
        private void UnRegisterHandlers()
        {
            Load -= EditForm_Load;
            CheckEdit_ActiveFlag.CheckedChanged -= HandleFormChanged;
            CheckEdit_Default.CheckedChanged -= HandleFormChanged;
            lookupEdit_hud_9902_section.EditValueChanged -= HandleFormChanged;
            TextEdit_description.EditValueChanged -= HandleFormChanged;
            CalcEdit_HourlyGrantAmountUsed.EditValueChanged -= HandleFormChanged;
            CalcEdit_HousingFeeAmount.EditValueChanged -= HandleFormChanged;
        }

        /// <summary>
        /// Process the form LOAD event
        /// </summary>
        private void EditForm_Load(object sender, EventArgs e)
        {
            UnRegisterHandlers();
            try
            {
                // Load the list of HUD mappings for POV entries
                lookupEdit_hud_9902_section.Properties.DataSource = DebtPlus.LINQ.Cache.Housing_ARM_referenceInfo.getList().FindAll(s => s.groupId == 31);

                // Set the other controls to the record information
                CheckEdit_ActiveFlag.Checked = record.ActiveFlag;
                CheckEdit_Default.Checked = record.Default;
                lookupEdit_hud_9902_section.EditValue = record.hud_9902_section;
                TextEdit_description.EditValue = record.description;
                CalcEdit_HourlyGrantAmountUsed.EditValue = record.HourlyGrantAmountUsed;
                CalcEdit_HousingFeeAmount.EditValue = record.HousingFeeAmount;

                simpleButton_OK.Enabled = !HasErrors();
            }
            finally
            {
                RegisterHandlers();
            }
        }

        /// <summary>
        /// Handle the change in a control value
        /// </summary>
        private void HandleFormChanged(object sender, EventArgs e)
        {
            simpleButton_OK.Enabled = !HasErrors();
        }

        /// <summary>
        /// Determine if there are any errors on the form
        /// </summary>
        private bool HasErrors()
        {
            if (string.IsNullOrWhiteSpace(DebtPlus.Utils.Nulls.v_String(TextEdit_description.EditValue)))
            {
                return true;
            }
            return false;
        }

        /// <summary>
        /// Update the record when the OK button is pressed
        /// </summary>
        protected override void simpleButton_OK_Click(object sender, EventArgs e)
        {
            record.ActiveFlag = CheckEdit_ActiveFlag.Checked;
            record.Default = CheckEdit_Default.Checked;
            record.hud_9902_section = DebtPlus.Utils.Nulls.v_String(lookupEdit_hud_9902_section.EditValue);
            record.description = DebtPlus.Utils.Nulls.v_String(TextEdit_description.EditValue);
            record.HourlyGrantAmountUsed = DebtPlus.Utils.Nulls.v_Decimal(CalcEdit_HourlyGrantAmountUsed.EditValue).GetValueOrDefault();
            record.HousingFeeAmount = DebtPlus.Utils.Nulls.v_Decimal(CalcEdit_HousingFeeAmount.EditValue).GetValueOrDefault();

            base.simpleButton_OK_Click(sender, e);
        }
    }
}