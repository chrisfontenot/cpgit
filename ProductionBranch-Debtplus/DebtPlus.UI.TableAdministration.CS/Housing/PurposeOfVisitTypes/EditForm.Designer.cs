namespace DebtPlus.UI.TableAdministration.CS.Housing.PurposeOfVisitTypes
{
	partial class EditForm
	{
		//Form overrides dispose to clean up the component list.
		[System.Diagnostics.DebuggerNonUserCode]
		protected override void Dispose(bool disposing)
		{
			try 
            {
				if (disposing)
                {
                    if (components != null) components.Dispose();
				}
                components = null;
            }
    	    finally
            {
				base.Dispose(disposing);
			}
		}

		//Required by the Windows Form Designer
		private System.ComponentModel.IContainer components = null;

		//NOTE: The following procedure is required by the Windows Form Designer
		//It can be modified using the Windows Form Designer.  
		//Do not modify it using the code editor.
		[System.Diagnostics.DebuggerStepThrough]
		private void InitializeComponent()
		{
			this.TextEdit_description = new DevExpress.XtraEditors.TextEdit();
			this.CheckEdit_ActiveFlag = new DevExpress.XtraEditors.CheckEdit();
			this.CalcEdit_HourlyGrantAmountUsed = new DevExpress.XtraEditors.CalcEdit();
			this.CalcEdit_HousingFeeAmount = new DevExpress.XtraEditors.CalcEdit();
			this.CheckEdit_Default = new DevExpress.XtraEditors.CheckEdit();
			this.LayoutControl1 = new DevExpress.XtraLayout.LayoutControl();
			this.LayoutControlGroup1 = new DevExpress.XtraLayout.LayoutControlGroup();
			this.LayoutControlItem1 = new DevExpress.XtraLayout.LayoutControlItem();
			this.LayoutControlItem2 = new DevExpress.XtraLayout.LayoutControlItem();
			this.LayoutControlItem3 = new DevExpress.XtraLayout.LayoutControlItem();
			this.LayoutControlItem4 = new DevExpress.XtraLayout.LayoutControlItem();
			this.LayoutControlItem5 = new DevExpress.XtraLayout.LayoutControlItem();
			this.LayoutControlItem6 = new DevExpress.XtraLayout.LayoutControlItem();
			this.EmptySpaceItem1 = new DevExpress.XtraLayout.EmptySpaceItem();
			this.EmptySpaceItem2 = new DevExpress.XtraLayout.EmptySpaceItem();
			this.lookupEdit_hud_9902_section = new DevExpress.XtraEditors.LookUpEdit();
			((System.ComponentModel.ISupportInitialize)this.DxErrorProvider1).BeginInit();
			((System.ComponentModel.ISupportInitialize)this.TextEdit_description.Properties).BeginInit();
			((System.ComponentModel.ISupportInitialize)this.CheckEdit_ActiveFlag.Properties).BeginInit();
			((System.ComponentModel.ISupportInitialize)this.CalcEdit_HourlyGrantAmountUsed.Properties).BeginInit();
			((System.ComponentModel.ISupportInitialize)this.CalcEdit_HousingFeeAmount.Properties).BeginInit();
			((System.ComponentModel.ISupportInitialize)this.CheckEdit_Default.Properties).BeginInit();
			((System.ComponentModel.ISupportInitialize)this.LayoutControl1).BeginInit();
			this.LayoutControl1.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)this.LayoutControlGroup1).BeginInit();
			((System.ComponentModel.ISupportInitialize)this.LayoutControlItem1).BeginInit();
			((System.ComponentModel.ISupportInitialize)this.LayoutControlItem2).BeginInit();
			((System.ComponentModel.ISupportInitialize)this.LayoutControlItem3).BeginInit();
			((System.ComponentModel.ISupportInitialize)this.LayoutControlItem4).BeginInit();
			((System.ComponentModel.ISupportInitialize)this.LayoutControlItem5).BeginInit();
			((System.ComponentModel.ISupportInitialize)this.LayoutControlItem6).BeginInit();
			((System.ComponentModel.ISupportInitialize)this.EmptySpaceItem1).BeginInit();
			((System.ComponentModel.ISupportInitialize)this.EmptySpaceItem2).BeginInit();
			((System.ComponentModel.ISupportInitialize)this.lookupEdit_hud_9902_section.Properties).BeginInit();
			this.SuspendLayout();
			//
			//simpleButton_OK
			//
			this.simpleButton_OK.Enabled = true;
			this.simpleButton_OK.Location = new System.Drawing.Point(313, 34);
			this.simpleButton_OK.TabIndex = 10;
			//
			//simpleButton_Cancel
			//
			this.simpleButton_Cancel.Location = new System.Drawing.Point(313, 77);
			this.simpleButton_Cancel.TabIndex = 11;
			//
			//TextEdit_description
			//
			this.TextEdit_description.Location = new System.Drawing.Point(83, 12);
			this.TextEdit_description.Name = "TextEdit_description";
			this.TextEdit_description.Properties.Mask.AutoComplete = DevExpress.XtraEditors.Mask.AutoCompleteType.None;
			this.TextEdit_description.Properties.Mask.BeepOnError = true;
			this.TextEdit_description.Properties.Mask.EditMask = "[^ ].+";
			this.TextEdit_description.Properties.Mask.IgnoreMaskBlank = false;
			this.TextEdit_description.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.RegEx;
			this.TextEdit_description.Properties.Mask.ShowPlaceHolders = false;
			this.TextEdit_description.Properties.MaxLength = 50;
			this.TextEdit_description.Size = new System.Drawing.Size(216, 20);
			this.TextEdit_description.StyleController = this.LayoutControl1;
			this.TextEdit_description.TabIndex = 1;
			this.TextEdit_description.ToolTip = "Title associated with the training course";
			//
			//CheckEdit_ActiveFlag
			//
			this.CheckEdit_ActiveFlag.Anchor = (System.Windows.Forms.AnchorStyles)(System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left);
			this.CheckEdit_ActiveFlag.Location = new System.Drawing.Point(12, 130);
			this.CheckEdit_ActiveFlag.Name = "CheckEdit_ActiveFlag";
			this.CheckEdit_ActiveFlag.Properties.Caption = "Active";
			this.CheckEdit_ActiveFlag.Size = new System.Drawing.Size(141, 19);
			this.CheckEdit_ActiveFlag.StyleController = this.LayoutControl1;
			this.CheckEdit_ActiveFlag.TabIndex = 8;
			this.CheckEdit_ActiveFlag.ToolTip = "Check if the course may be selected for counselors. If not active, the course may" + " not be choosen for a counselor's training.";
			//
			//CalcEdit_HourlyGrantAmountUsed
			//
			this.CalcEdit_HourlyGrantAmountUsed.Location = new System.Drawing.Point(83, 60);
			this.CalcEdit_HourlyGrantAmountUsed.Name = "CalcEdit_HourlyGrantAmountUsed";
			this.CalcEdit_HourlyGrantAmountUsed.Properties.Appearance.Options.UseTextOptions = true;
			this.CalcEdit_HourlyGrantAmountUsed.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
			this.CalcEdit_HourlyGrantAmountUsed.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] { new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo) });
			this.CalcEdit_HourlyGrantAmountUsed.Properties.DisplayFormat.FormatString = "c";
			this.CalcEdit_HourlyGrantAmountUsed.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
			this.CalcEdit_HourlyGrantAmountUsed.Properties.EditFormat.FormatString = "c";
			this.CalcEdit_HourlyGrantAmountUsed.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
			this.CalcEdit_HourlyGrantAmountUsed.Properties.Mask.EditMask = "c";
			this.CalcEdit_HourlyGrantAmountUsed.Properties.Precision = 2;
			this.CalcEdit_HourlyGrantAmountUsed.Size = new System.Drawing.Size(108, 20);
			this.CalcEdit_HourlyGrantAmountUsed.StyleController = this.LayoutControl1;
			this.CalcEdit_HourlyGrantAmountUsed.TabIndex = 5;
			//
			//CalcEdit_HousingFeeAmount
			//
			this.CalcEdit_HousingFeeAmount.Location = new System.Drawing.Point(83, 84);
			this.CalcEdit_HousingFeeAmount.Name = "CalcEdit_HousingFeeAmount";
			this.CalcEdit_HousingFeeAmount.Properties.Appearance.Options.UseTextOptions = true;
			this.CalcEdit_HousingFeeAmount.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
			this.CalcEdit_HousingFeeAmount.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] { new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo) });
			this.CalcEdit_HousingFeeAmount.Properties.DisplayFormat.FormatString = "c";
			this.CalcEdit_HousingFeeAmount.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
			this.CalcEdit_HousingFeeAmount.Properties.EditFormat.FormatString = "c";
			this.CalcEdit_HousingFeeAmount.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
			this.CalcEdit_HousingFeeAmount.Properties.Mask.EditMask = "c";
			this.CalcEdit_HousingFeeAmount.Properties.Precision = 2;
			this.CalcEdit_HousingFeeAmount.Size = new System.Drawing.Size(108, 20);
			this.CalcEdit_HousingFeeAmount.StyleController = this.LayoutControl1;
			this.CalcEdit_HousingFeeAmount.TabIndex = 7;
			//
			//CheckEdit_Default
			//
			this.CheckEdit_Default.Anchor = (System.Windows.Forms.AnchorStyles)(System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left);
			this.CheckEdit_Default.Location = new System.Drawing.Point(157, 130);
			this.CheckEdit_Default.Name = "CheckEdit_Default";
			this.CheckEdit_Default.Properties.Caption = "Default";
			this.CheckEdit_Default.Size = new System.Drawing.Size(142, 19);
			this.CheckEdit_Default.StyleController = this.LayoutControl1;
			this.CheckEdit_Default.TabIndex = 9;
			this.CheckEdit_Default.ToolTip = "Check if the course may be selected for counselors. If not active, the course may" + " not be choosen for a counselor's training.";
			//
			//LayoutControl1
			//
			this.LayoutControl1.Anchor = (System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) | System.Windows.Forms.AnchorStyles.Left) | System.Windows.Forms.AnchorStyles.Right);
			this.LayoutControl1.Controls.Add(this.TextEdit_description);
			this.LayoutControl1.Controls.Add(this.CheckEdit_Default);
			this.LayoutControl1.Controls.Add(this.CalcEdit_HousingFeeAmount);
			this.LayoutControl1.Controls.Add(this.CheckEdit_ActiveFlag);
			this.LayoutControl1.Controls.Add(this.CalcEdit_HourlyGrantAmountUsed);
			this.LayoutControl1.Controls.Add(this.lookupEdit_hud_9902_section);
			this.LayoutControl1.Location = new System.Drawing.Point(0, 0);
			this.LayoutControl1.Name = "LayoutControl1";
			this.LayoutControl1.Root = this.LayoutControlGroup1;
			this.LayoutControl1.Size = new System.Drawing.Size(311, 161);
			this.LayoutControl1.TabIndex = 12;
			this.LayoutControl1.Text = "LayoutControl1";
			//
			//LayoutControlGroup1
			//
			this.LayoutControlGroup1.CustomizationFormText = "LayoutControlGroup1";
			this.LayoutControlGroup1.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.True;
			this.LayoutControlGroup1.GroupBordersVisible = false;
			this.LayoutControlGroup1.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
				this.LayoutControlItem1,
				this.LayoutControlItem2,
				this.LayoutControlItem3,
				this.LayoutControlItem4,
				this.LayoutControlItem5,
				this.LayoutControlItem6,
				this.EmptySpaceItem1,
				this.EmptySpaceItem2
			});
			this.LayoutControlGroup1.Location = new System.Drawing.Point(0, 0);
			this.LayoutControlGroup1.Name = "LayoutControlGroup1";
			this.LayoutControlGroup1.Size = new System.Drawing.Size(311, 161);
			this.LayoutControlGroup1.Text = "LayoutControlGroup1";
			this.LayoutControlGroup1.TextVisible = false;
			//
			//LayoutControlItem1
			//
			this.LayoutControlItem1.Control = this.TextEdit_description;
			this.LayoutControlItem1.CustomizationFormText = "Description";
			this.LayoutControlItem1.Location = new System.Drawing.Point(0, 0);
			this.LayoutControlItem1.Name = "LayoutControlItem1";
			this.LayoutControlItem1.Size = new System.Drawing.Size(291, 24);
			this.LayoutControlItem1.Text = "Description";
			this.LayoutControlItem1.TextSize = new System.Drawing.Size(67, 13);
			//
			//LayoutControlItem2
			//
			this.LayoutControlItem2.Control = this.lookupEdit_hud_9902_section;
			this.LayoutControlItem2.CustomizationFormText = "H.U.D.";
			this.LayoutControlItem2.Location = new System.Drawing.Point(0, 24);
			this.LayoutControlItem2.Name = "LayoutControlItem2";
			this.LayoutControlItem2.Size = new System.Drawing.Size(291, 24);
			this.LayoutControlItem2.Text = "H.U.D.";
			this.LayoutControlItem2.TextSize = new System.Drawing.Size(67, 13);
			//
			//LayoutControlItem3
			//
			this.LayoutControlItem3.Control = this.CalcEdit_HourlyGrantAmountUsed;
			this.LayoutControlItem3.CustomizationFormText = "Grant Amount";
			this.LayoutControlItem3.Location = new System.Drawing.Point(0, 48);
			this.LayoutControlItem3.Name = "LayoutControlItem3";
			this.LayoutControlItem3.Size = new System.Drawing.Size(183, 24);
			this.LayoutControlItem3.Text = "Grant Amount";
			this.LayoutControlItem3.TextSize = new System.Drawing.Size(67, 13);
			//
			//LayoutControlItem4
			//
			this.LayoutControlItem4.Control = this.CalcEdit_HousingFeeAmount;
			this.LayoutControlItem4.CustomizationFormText = "Client Fee";
			this.LayoutControlItem4.Location = new System.Drawing.Point(0, 72);
			this.LayoutControlItem4.Name = "LayoutControlItem4";
			this.LayoutControlItem4.Size = new System.Drawing.Size(183, 24);
			this.LayoutControlItem4.Text = "Client Fee";
			this.LayoutControlItem4.TextSize = new System.Drawing.Size(67, 13);
			//
			//LayoutControlItem5
			//
			this.LayoutControlItem5.Control = this.CheckEdit_ActiveFlag;
			this.LayoutControlItem5.CustomizationFormText = "Active Entry?";
			this.LayoutControlItem5.Location = new System.Drawing.Point(0, 118);
			this.LayoutControlItem5.Name = "LayoutControlItem5";
			this.LayoutControlItem5.Size = new System.Drawing.Size(145, 23);
			this.LayoutControlItem5.Text = "Active Entry?";
			this.LayoutControlItem5.TextSize = new System.Drawing.Size(0, 0);
			this.LayoutControlItem5.TextToControlDistance = 0;
			this.LayoutControlItem5.TextVisible = false;
			//
			//LayoutControlItem6
			//
			this.LayoutControlItem6.Control = this.CheckEdit_Default;
			this.LayoutControlItem6.CustomizationFormText = "Default Entry?";
			this.LayoutControlItem6.Location = new System.Drawing.Point(145, 118);
			this.LayoutControlItem6.Name = "LayoutControlItem6";
			this.LayoutControlItem6.Size = new System.Drawing.Size(146, 23);
			this.LayoutControlItem6.Text = "Default Entry?";
			this.LayoutControlItem6.TextSize = new System.Drawing.Size(0, 0);
			this.LayoutControlItem6.TextToControlDistance = 0;
			this.LayoutControlItem6.TextVisible = false;
			//
			//EmptySpaceItem1
			//
			this.EmptySpaceItem1.AllowHotTrack = false;
			this.EmptySpaceItem1.CustomizationFormText = "EmptySpaceItem1";
			this.EmptySpaceItem1.Location = new System.Drawing.Point(0, 96);
			this.EmptySpaceItem1.Name = "EmptySpaceItem1";
			this.EmptySpaceItem1.Size = new System.Drawing.Size(291, 22);
			this.EmptySpaceItem1.Text = "EmptySpaceItem1";
			this.EmptySpaceItem1.TextSize = new System.Drawing.Size(0, 0);
			//
			//EmptySpaceItem2
			//
			this.EmptySpaceItem2.AllowHotTrack = false;
			this.EmptySpaceItem2.CustomizationFormText = "EmptySpaceItem2";
			this.EmptySpaceItem2.Location = new System.Drawing.Point(183, 48);
			this.EmptySpaceItem2.Name = "EmptySpaceItem2";
			this.EmptySpaceItem2.Size = new System.Drawing.Size(108, 48);
			this.EmptySpaceItem2.Text = "EmptySpaceItem2";
			this.EmptySpaceItem2.TextSize = new System.Drawing.Size(0, 0);
			//
			//lookupEdit_hud_9902_section
			//
			this.lookupEdit_hud_9902_section.Location = new System.Drawing.Point(83, 36);
			this.lookupEdit_hud_9902_section.Name = "lookupEdit_hud_9902_section";
			this.lookupEdit_hud_9902_section.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.True;
			this.lookupEdit_hud_9902_section.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] { new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo) });
			this.lookupEdit_hud_9902_section.Properties.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
				new DevExpress.XtraEditors.Controls.LookUpColumnInfo("name", "ID", 20, DevExpress.Utils.FormatType.None, "", false, DevExpress.Utils.HorzAlignment.Default),
				new DevExpress.XtraEditors.Controls.LookUpColumnInfo("longDesc", "Description", 20, DevExpress.Utils.FormatType.None, "", true, DevExpress.Utils.HorzAlignment.Default, DevExpress.Data.ColumnSortOrder.Ascending)
			});
			this.lookupEdit_hud_9902_section.Properties.DisplayMember = "longDesc";
			this.lookupEdit_hud_9902_section.Properties.MaxLength = 50;
			this.lookupEdit_hud_9902_section.Properties.NullText = "";
			this.lookupEdit_hud_9902_section.Properties.ShowFooter = false;
			this.lookupEdit_hud_9902_section.Properties.ShowHeader = false;
			this.lookupEdit_hud_9902_section.Properties.ValueMember = "name";
			this.lookupEdit_hud_9902_section.Size = new System.Drawing.Size(216, 20);
			this.lookupEdit_hud_9902_section.StyleController = this.LayoutControl1;
			this.lookupEdit_hud_9902_section.TabIndex = 3;
			this.lookupEdit_hud_9902_section.ToolTip = "If \"Other\", enter the organization name here";
			this.lookupEdit_hud_9902_section.Properties.SortColumnIndex = 1;
			//
			//EditForm
			//
			this.AutoScaleDimensions = new System.Drawing.SizeF(6f, 13f);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(409, 152);
			this.Controls.Add(this.LayoutControl1);
			this.Name = "EditForm";
			this.Text = "Housing Purpose Of Visit Type";
			this.Controls.SetChildIndex(this.simpleButton_OK, 0);
			this.Controls.SetChildIndex(this.simpleButton_Cancel, 0);
			this.Controls.SetChildIndex(this.LayoutControl1, 0);
			((System.ComponentModel.ISupportInitialize)this.DxErrorProvider1).EndInit();
			((System.ComponentModel.ISupportInitialize)this.TextEdit_description.Properties).EndInit();
			((System.ComponentModel.ISupportInitialize)this.CheckEdit_ActiveFlag.Properties).EndInit();
			((System.ComponentModel.ISupportInitialize)this.CalcEdit_HourlyGrantAmountUsed.Properties).EndInit();
			((System.ComponentModel.ISupportInitialize)this.CalcEdit_HousingFeeAmount.Properties).EndInit();
			((System.ComponentModel.ISupportInitialize)this.CheckEdit_Default.Properties).EndInit();
			((System.ComponentModel.ISupportInitialize)this.LayoutControl1).EndInit();
			this.LayoutControl1.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)this.LayoutControlGroup1).EndInit();
			((System.ComponentModel.ISupportInitialize)this.LayoutControlItem1).EndInit();
			((System.ComponentModel.ISupportInitialize)this.LayoutControlItem2).EndInit();
			((System.ComponentModel.ISupportInitialize)this.LayoutControlItem3).EndInit();
			((System.ComponentModel.ISupportInitialize)this.LayoutControlItem4).EndInit();
			((System.ComponentModel.ISupportInitialize)this.LayoutControlItem5).EndInit();
			((System.ComponentModel.ISupportInitialize)this.LayoutControlItem6).EndInit();
			((System.ComponentModel.ISupportInitialize)this.EmptySpaceItem1).EndInit();
			((System.ComponentModel.ISupportInitialize)this.EmptySpaceItem2).EndInit();
			((System.ComponentModel.ISupportInitialize)this.lookupEdit_hud_9902_section.Properties).EndInit();
			this.ResumeLayout(false);

		}
		private DevExpress.XtraEditors.TextEdit TextEdit_description;
		private DevExpress.XtraEditors.CheckEdit CheckEdit_ActiveFlag;
		private DevExpress.XtraEditors.CalcEdit CalcEdit_HourlyGrantAmountUsed;
		private DevExpress.XtraEditors.CalcEdit CalcEdit_HousingFeeAmount;
		private DevExpress.XtraEditors.CheckEdit CheckEdit_Default;
		private DevExpress.XtraLayout.LayoutControl LayoutControl1;
		private DevExpress.XtraEditors.LookUpEdit lookupEdit_hud_9902_section;
		private DevExpress.XtraLayout.LayoutControlGroup LayoutControlGroup1;
		private DevExpress.XtraLayout.LayoutControlItem LayoutControlItem1;
		private DevExpress.XtraLayout.LayoutControlItem LayoutControlItem2;
		private DevExpress.XtraLayout.LayoutControlItem LayoutControlItem3;
		private DevExpress.XtraLayout.LayoutControlItem LayoutControlItem4;
		private DevExpress.XtraLayout.LayoutControlItem LayoutControlItem5;
		private DevExpress.XtraLayout.LayoutControlItem LayoutControlItem6;
		private DevExpress.XtraLayout.EmptySpaceItem EmptySpaceItem1;
		private DevExpress.XtraLayout.EmptySpaceItem EmptySpaceItem2;
	}
}

