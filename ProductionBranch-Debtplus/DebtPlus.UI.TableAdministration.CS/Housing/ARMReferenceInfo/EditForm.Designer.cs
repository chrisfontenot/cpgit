using System;
using System.Windows.Forms;
using System.Drawing;
using DebtPlus.LINQ;
using System.Linq;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Diagnostics;
using DebtPlus.UI.Common.Templates;

namespace DebtPlus.UI.TableAdministration.CS.Housing.ARMReferenceInfo
{
	partial class EditForm
	{
        protected override void Dispose(bool disposing)
        {
            try
            {
                if (disposing)
                {
                    if (components != null) components.Dispose();
                }
                components = null;
            }
            finally
            {
                base.Dispose(disposing);
            }
        }

        //Required by the Windows Form Designer
        private System.ComponentModel.IContainer components = null;

        //NOTE: The following procedure is required by the Windows Form Designer
		//It can be modified using the Windows Form Designer.  
		//Do not modify it using the code editor.
		[System.Diagnostics.DebuggerStepThrough]
		private void InitializeComponent()
		{
			this.LayoutControl1 = new DevExpress.XtraLayout.LayoutControl();
			this.TextEdit_shortDesc = new DevExpress.XtraEditors.TextEdit();
			this.TextEdit_longDesc = new DevExpress.XtraEditors.TextEdit();
			this.TextEdit_name = new DevExpress.XtraEditors.TextEdit();
			this.TextEdit_ID = new DevExpress.XtraEditors.TextEdit();
			this.LookUpEdit_groupId = new DevExpress.XtraEditors.LookUpEdit();
			this.LayoutControlGroup1 = new DevExpress.XtraLayout.LayoutControlGroup();
			this.LayoutControlItem1 = new DevExpress.XtraLayout.LayoutControlItem();
			this.LayoutControlItem3 = new DevExpress.XtraLayout.LayoutControlItem();
			this.LayoutControlItem4 = new DevExpress.XtraLayout.LayoutControlItem();
			this.LayoutControlItem5 = new DevExpress.XtraLayout.LayoutControlItem();
			this.LayoutControlItem2 = new DevExpress.XtraLayout.LayoutControlItem();
			((System.ComponentModel.ISupportInitialize)this.DxErrorProvider1).BeginInit();
			((System.ComponentModel.ISupportInitialize)this.LayoutControl1).BeginInit();
			this.LayoutControl1.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)this.TextEdit_shortDesc.Properties).BeginInit();
			((System.ComponentModel.ISupportInitialize)this.TextEdit_longDesc.Properties).BeginInit();
			((System.ComponentModel.ISupportInitialize)this.TextEdit_name.Properties).BeginInit();
			((System.ComponentModel.ISupportInitialize)this.TextEdit_ID.Properties).BeginInit();
			((System.ComponentModel.ISupportInitialize)this.LookUpEdit_groupId.Properties).BeginInit();
			((System.ComponentModel.ISupportInitialize)this.LayoutControlGroup1).BeginInit();
			((System.ComponentModel.ISupportInitialize)this.LayoutControlItem1).BeginInit();
			((System.ComponentModel.ISupportInitialize)this.LayoutControlItem3).BeginInit();
			((System.ComponentModel.ISupportInitialize)this.LayoutControlItem4).BeginInit();
			((System.ComponentModel.ISupportInitialize)this.LayoutControlItem5).BeginInit();
			((System.ComponentModel.ISupportInitialize)this.LayoutControlItem2).BeginInit();
			this.SuspendLayout();
			//
			//simpleButton_OK
			//
			this.simpleButton_OK.Enabled = true;
			this.simpleButton_OK.Location = new System.Drawing.Point(317, 34);
			//
			//simpleButton_Cancel
			//
			this.simpleButton_Cancel.Location = new System.Drawing.Point(317, 77);
			//
			//LayoutControl1
			//
			this.LayoutControl1.Anchor = (AnchorStyles)(((AnchorStyles.Top | AnchorStyles.Bottom) | AnchorStyles.Left) | AnchorStyles.Right);
			this.LayoutControl1.Controls.Add(this.TextEdit_shortDesc);
			this.LayoutControl1.Controls.Add(this.TextEdit_longDesc);
			this.LayoutControl1.Controls.Add(this.TextEdit_name);
			this.LayoutControl1.Controls.Add(this.TextEdit_ID);
			this.LayoutControl1.Controls.Add(this.LookUpEdit_groupId);
			this.LayoutControl1.Location = new System.Drawing.Point(13, 0);
			this.LayoutControl1.Name = "LayoutControl1";
			this.LayoutControl1.Root = this.LayoutControlGroup1;
			this.LayoutControl1.Size = new System.Drawing.Size(304, 148);
			this.LayoutControl1.TabIndex = 20;
			this.LayoutControl1.Text = "LayoutControl1";
			//
			//TextEdit_shortDesc
			//
			this.TextEdit_shortDesc.Location = new System.Drawing.Point(68, 108);
			this.TextEdit_shortDesc.Name = "TextEdit_shortDesc";
			this.TextEdit_shortDesc.Properties.Mask.BeepOnError = true;
			this.TextEdit_shortDesc.Properties.Mask.EditMask = "[^ ].*";
			this.TextEdit_shortDesc.Properties.Mask.IgnoreMaskBlank = false;
			this.TextEdit_shortDesc.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.RegEx;
			this.TextEdit_shortDesc.Properties.Mask.ShowPlaceHolders = false;
			this.TextEdit_shortDesc.Properties.MaxLength = 1024;
			this.TextEdit_shortDesc.Size = new System.Drawing.Size(224, 20);
			this.TextEdit_shortDesc.StyleController = this.LayoutControl1;
			this.TextEdit_shortDesc.TabIndex = 8;
			//
			//TextEdit_longDesc
			//
			this.TextEdit_longDesc.Location = new System.Drawing.Point(68, 84);
			this.TextEdit_longDesc.Name = "TextEdit_longDesc";
			this.TextEdit_longDesc.Properties.Mask.BeepOnError = true;
			this.TextEdit_longDesc.Properties.Mask.EditMask = "[^ ].*";
			this.TextEdit_longDesc.Properties.Mask.IgnoreMaskBlank = false;
			this.TextEdit_longDesc.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.RegEx;
			this.TextEdit_longDesc.Properties.Mask.ShowPlaceHolders = false;
			this.TextEdit_longDesc.Properties.MaxLength = 1024;
			this.TextEdit_longDesc.Size = new System.Drawing.Size(224, 20);
			this.TextEdit_longDesc.StyleController = this.LayoutControl1;
			this.TextEdit_longDesc.TabIndex = 7;
			//
			//TextEdit_name
			//
			this.TextEdit_name.Location = new System.Drawing.Point(68, 60);
			this.TextEdit_name.Name = "TextEdit_name";
			this.TextEdit_name.Properties.Mask.EditMask = "[^ ].*";
			this.TextEdit_name.Properties.Mask.IgnoreMaskBlank = false;
			this.TextEdit_name.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.RegEx;
			this.TextEdit_name.Properties.Mask.ShowPlaceHolders = false;
			this.TextEdit_name.Properties.MaxLength = 1024;
			this.TextEdit_name.Size = new System.Drawing.Size(224, 20);
			this.TextEdit_name.StyleController = this.LayoutControl1;
			this.TextEdit_name.TabIndex = 6;
			//
			//TextEdit_ID
			//
			this.TextEdit_ID.Location = new System.Drawing.Point(68, 36);
			this.TextEdit_ID.Name = "TextEdit_ID";
			this.TextEdit_ID.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.False;
			this.TextEdit_ID.Properties.DisplayFormat.FormatString = "{0:f0}";
			this.TextEdit_ID.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
			this.TextEdit_ID.Properties.EditFormat.FormatString = "{0:f0}";
			this.TextEdit_ID.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
			this.TextEdit_ID.Properties.Mask.BeepOnError = true;
			this.TextEdit_ID.Properties.Mask.EditMask = "f0";
			this.TextEdit_ID.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
			this.TextEdit_ID.Size = new System.Drawing.Size(224, 20);
			this.TextEdit_ID.StyleController = this.LayoutControl1;
			this.TextEdit_ID.TabIndex = 5;
			//
			//LookUpEdit_groupId
			//
			this.LookUpEdit_groupId.Location = new System.Drawing.Point(68, 12);
			this.LookUpEdit_groupId.Name = "LookUpEdit_groupId";
			this.LookUpEdit_groupId.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] { new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo) });
			this.LookUpEdit_groupId.Properties.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
				new DevExpress.XtraEditors.Controls.LookUpColumnInfo("Id", "ID", 5, DevExpress.Utils.FormatType.Numeric, "f0", true, DevExpress.Utils.HorzAlignment.Default),
				new DevExpress.XtraEditors.Controls.LookUpColumnInfo("description", "Description", 20, DevExpress.Utils.FormatType.None, "", true, DevExpress.Utils.HorzAlignment.Default, DevExpress.Data.ColumnSortOrder.Ascending)
			});
			this.LookUpEdit_groupId.Properties.DisplayMember = "description";
			this.LookUpEdit_groupId.Properties.NullText = "";
			this.LookUpEdit_groupId.Properties.ShowFooter = false;
			this.LookUpEdit_groupId.Properties.ValueMember = "Id";
			this.LookUpEdit_groupId.Size = new System.Drawing.Size(224, 20);
			this.LookUpEdit_groupId.StyleController = this.LayoutControl1;
			this.LookUpEdit_groupId.TabIndex = 4;
			this.LookUpEdit_groupId.Properties.SortColumnIndex = 1;
			//
			//LayoutControlGroup1
			//
			this.LayoutControlGroup1.CustomizationFormText = "LayoutControlGroup1";
			this.LayoutControlGroup1.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.True;
			this.LayoutControlGroup1.GroupBordersVisible = false;
			this.LayoutControlGroup1.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
				this.LayoutControlItem1,
				this.LayoutControlItem3,
				this.LayoutControlItem4,
				this.LayoutControlItem5,
				this.LayoutControlItem2
			});
			this.LayoutControlGroup1.Location = new System.Drawing.Point(0, 0);
			this.LayoutControlGroup1.Name = "LayoutControlGroup1";
			this.LayoutControlGroup1.Size = new System.Drawing.Size(304, 148);
			this.LayoutControlGroup1.Spacing = new DevExpress.XtraLayout.Utils.Padding(0, 0, 0, 0);
			this.LayoutControlGroup1.Text = "LayoutControlGroup1";
			this.LayoutControlGroup1.TextVisible = false;
			//
			//LayoutControlItem1
			//
			this.LayoutControlItem1.Control = this.LookUpEdit_groupId;
			this.LayoutControlItem1.CustomizationFormText = "Group ID";
			this.LayoutControlItem1.Location = new System.Drawing.Point(0, 0);
			this.LayoutControlItem1.Name = "LayoutControlItem1";
			this.LayoutControlItem1.Size = new System.Drawing.Size(284, 24);
			this.LayoutControlItem1.Text = "Group";
			this.LayoutControlItem1.TextSize = new System.Drawing.Size(52, 13);
			//
			//LayoutControlItem3
			//
			this.LayoutControlItem3.Control = this.TextEdit_name;
			this.LayoutControlItem3.CustomizationFormText = "Name";
			this.LayoutControlItem3.Location = new System.Drawing.Point(0, 48);
			this.LayoutControlItem3.Name = "LayoutControlItem3";
			this.LayoutControlItem3.Size = new System.Drawing.Size(284, 24);
			this.LayoutControlItem3.Text = "Name";
			this.LayoutControlItem3.TextSize = new System.Drawing.Size(52, 13);
			//
			//LayoutControlItem4
			//
			this.LayoutControlItem4.Control = this.TextEdit_longDesc;
			this.LayoutControlItem4.CustomizationFormText = "Long Desc";
			this.LayoutControlItem4.Location = new System.Drawing.Point(0, 72);
			this.LayoutControlItem4.Name = "LayoutControlItem4";
			this.LayoutControlItem4.Size = new System.Drawing.Size(284, 24);
			this.LayoutControlItem4.Text = "Long Desc";
			this.LayoutControlItem4.TextSize = new System.Drawing.Size(52, 13);
			//
			//LayoutControlItem5
			//
			this.LayoutControlItem5.Control = this.TextEdit_shortDesc;
			this.LayoutControlItem5.CustomizationFormText = "Short Desc";
			this.LayoutControlItem5.Location = new System.Drawing.Point(0, 96);
			this.LayoutControlItem5.Name = "LayoutControlItem5";
			this.LayoutControlItem5.Size = new System.Drawing.Size(284, 32);
			this.LayoutControlItem5.Text = "Short Desc";
			this.LayoutControlItem5.TextSize = new System.Drawing.Size(52, 13);
			//
			//LayoutControlItem2
			//
			this.LayoutControlItem2.Control = this.TextEdit_ID;
			this.LayoutControlItem2.CustomizationFormText = "ID";
			this.LayoutControlItem2.Location = new System.Drawing.Point(0, 24);
			this.LayoutControlItem2.Name = "LayoutControlItem2";
			this.LayoutControlItem2.Size = new System.Drawing.Size(284, 24);
			this.LayoutControlItem2.Text = "ID";
			this.LayoutControlItem2.TextSize = new System.Drawing.Size(52, 13);
			//
			//EditForm
			//
			this.AutoScaleDimensions = new System.Drawing.SizeF(6f, 13f);
			this.AutoScaleMode = AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(413, 146);
			this.Controls.Add(this.LayoutControl1);
			this.LookAndFeel.UseDefaultLookAndFeel = true;
			this.Name = "EditForm";
			this.Text = "HUD Reference Item";
			this.Controls.SetChildIndex(this.LayoutControl1, 0);
			this.Controls.SetChildIndex(this.simpleButton_OK, 0);
			this.Controls.SetChildIndex(this.simpleButton_Cancel, 0);
			((System.ComponentModel.ISupportInitialize)this.DxErrorProvider1).EndInit();
			((System.ComponentModel.ISupportInitialize)this.LayoutControl1).EndInit();
			this.LayoutControl1.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)this.TextEdit_shortDesc.Properties).EndInit();
			((System.ComponentModel.ISupportInitialize)this.TextEdit_longDesc.Properties).EndInit();
			((System.ComponentModel.ISupportInitialize)this.TextEdit_name.Properties).EndInit();
			((System.ComponentModel.ISupportInitialize)this.TextEdit_ID.Properties).EndInit();
			((System.ComponentModel.ISupportInitialize)this.LookUpEdit_groupId.Properties).EndInit();
			((System.ComponentModel.ISupportInitialize)this.LayoutControlGroup1).EndInit();
			((System.ComponentModel.ISupportInitialize)this.LayoutControlItem1).EndInit();
			((System.ComponentModel.ISupportInitialize)this.LayoutControlItem3).EndInit();
			((System.ComponentModel.ISupportInitialize)this.LayoutControlItem4).EndInit();
			((System.ComponentModel.ISupportInitialize)this.LayoutControlItem5).EndInit();
			((System.ComponentModel.ISupportInitialize)this.LayoutControlItem2).EndInit();
			this.ResumeLayout(false);

		}
		private DevExpress.XtraLayout.LayoutControl LayoutControl1;
		private DevExpress.XtraLayout.LayoutControlGroup LayoutControlGroup1;
		private DevExpress.XtraEditors.LookUpEdit LookUpEdit_groupId;
		private DevExpress.XtraLayout.LayoutControlItem LayoutControlItem1;
		private DevExpress.XtraEditors.TextEdit TextEdit_shortDesc;
		private DevExpress.XtraEditors.TextEdit TextEdit_longDesc;
		private DevExpress.XtraEditors.TextEdit TextEdit_name;
		private DevExpress.XtraEditors.TextEdit TextEdit_ID;
		private DevExpress.XtraLayout.LayoutControlItem LayoutControlItem3;
		private DevExpress.XtraLayout.LayoutControlItem LayoutControlItem4;
		private DevExpress.XtraLayout.LayoutControlItem LayoutControlItem5;
		private DevExpress.XtraLayout.LayoutControlItem LayoutControlItem2;
	}
}

//=======================================================
//Service provided by Telerik (www.telerik.com)
//Conversion powered by NRefactory.
//Twitter: @telerik
//Facebook: facebook.com/telerik
//=======================================================
