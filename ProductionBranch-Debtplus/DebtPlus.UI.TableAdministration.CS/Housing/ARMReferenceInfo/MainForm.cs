#region "Copyright 2000-2012 DebtPlus, L.L.C."
//{*******************************************************************}
//{                                                                   }
//{       DebtPlus Debt Management System                             }
//{                                                                   }
//{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
//{       ALL RIGHTS RESERVED                                         }
//{                                                                   }
//{   The entire contents of this file is protected by U.S. and       }
//{   International Copyright Laws. Unauthorized reproduction,        }
//{   reverse-engineering, and distribution of all or any portion of  }
//{   the code contained in this file is strictly prohibited and may  }
//{   result in severe civil and criminal penalties and will be       }
//{   prosecuted to the maximum extent possible under the law.        }
//{                                                                   }
//{   RESTRICTIONS                                                    }
//{                                                                   }
//{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
//{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
//{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
//{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
//{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
//{                                                                   }
//{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
//{   ADDITIONAL RESTRICTIONS.                                        }
//{                                                                   }
//{*******************************************************************}

#endregion "Copyright 2000-2012 DebtPlus, L.L.C."

using System;
using System.Linq;
using System.Net;
using System.Net.Security;
using System.Security.Cryptography.X509Certificates;
using DebtPlus.LINQ;
using DebtPlus.UI.Common;

namespace DebtPlus.UI.TableAdministration.CS.Housing.ARMReferenceInfo
{
    public partial class MainForm : Templates.MainForm
    {
        // Local storage
        private BusinessContext bc = new BusinessContext();

        private System.Collections.Generic.List<Housing_ARM_referenceInfo> colRecords = null;

        /// <summary>
        /// Process the form initialization
        /// </summary>
        public MainForm()
            : base()
        {
            InitializeComponent();
            RegisterHandlers();
        }

        /// <summary>
        /// Register the event handlers
        /// </summary>
        private void RegisterHandlers()
        {
            this.Load += MainForm_Load;
            barButtonItem_Download.ItemClick += barButtonItem_Download_ItemClick;
        }

        /// <summary>
        /// Remove the event handler registrations
        /// </summary>
        private void UnRegisterHandlers()
        {
            this.Load -= MainForm_Load;
            barButtonItem_Download.ItemClick -= barButtonItem_Download_ItemClick;
        }

        /// <summary>
        /// Called when we need to handle invalid SSL certificates
        /// </summary>
        private bool AllowInvalidCertificates(object Sender, X509Certificate certificate, X509Chain chain, SslPolicyErrors policyErrors)
        {
            return true;
        }

        /// <summary>
        /// Process the load event on the form
        /// </summary>
        private void MainForm_Load(object sender, EventArgs e)
        {
            UnRegisterHandlers();

            // Hook into the certificate validation logic
            ServicePointManager.ServerCertificateValidationCallback = new RemoteCertificateValidationCallback(AllowInvalidCertificates);

            // Correct the grid display to have the proper formatter
            gridColumn_groupId.GroupFormat.Format = new GroupIDFormatter();
            gridColumn_groupId.DisplayFormat.Format = new GroupIDFormatter();

            try
            {
                using (var cm = new CursorManager())
                {
                    colRecords = bc.Housing_ARM_referenceInfos.ToList();
                    gridControl1.DataSource = colRecords;

                    // Update the view portal to display the data properly.
                    gridView1.ExpandAllGroups();
                    gridView1.BestFitColumns();
                    gridView1.RefreshData();
                }
            }
            catch (System.Data.SqlClient.SqlException ex)
            {
                DebtPlus.UI.Common.ErrorHandling.HandleErrors(ex);
            }
            finally
            {
                RegisterHandlers();
            }
        }

        /// <summary>
        /// Handle the edit of the information on the form
        /// </summary>
        protected override void UpdateRecord(object obj)
        {
            var record = obj as Housing_ARM_referenceInfo;
            if (record == null)
            {
                return;
            }

            // Do the update operation
            using (var frm = new EditForm(bc, record))
            {
                if (frm.ShowDialog() != System.Windows.Forms.DialogResult.OK)
                {
                    return;
                }
            }

            // Update the database with the changes
            try
            {
                bc.SubmitChanges();
                gridView1.RefreshData();
            }
            catch (System.Data.SqlClient.SqlException ex)
            {
                DebtPlus.UI.Common.ErrorHandling.HandleErrors(ex);
            }
        }

        /// <summary>
        /// Process the CREATE menu item
        /// </summary>
        protected override void CreateRecord()
        {
            // Create the new record and edit it.
            var record = DebtPlus.LINQ.Factory.Manufacture_Housing_ARM_referenceInfo();

            // Do the edit operation
            using (var frm = new EditForm(bc, record))
            {
                if (frm.ShowDialog() != System.Windows.Forms.DialogResult.OK)
                {
                    return;
                }
            }

            // Insert the new record
            bc.Housing_ARM_referenceInfos.InsertOnSubmit(record);
            colRecords.Add(record);

            // Update the database once the insert is complete
            try
            {
                bc.SubmitChanges();
                gridView1.RefreshData();
            }
            catch (System.Data.SqlClient.SqlException ex)
            {
                DebtPlus.UI.Common.ErrorHandling.HandleErrors(ex);
            }
        }

        /// <summary>
        /// Delete the current record from the database
        /// </summary>
        protected override void DeleteRecord(object obj)
        {
            var record = obj as Housing_ARM_referenceInfo;
            if (record == null)
            {
                return;
            }

            // Ensure that the user really wanted to delete the record
            if (DebtPlus.Data.Prompts.RequestConfirmation_Delete() != System.Windows.Forms.DialogResult.Yes)
            {
                return;
            }

            // Delete the record from the database and the list
            bc.Housing_ARM_referenceInfos.DeleteOnSubmit(record);
            colRecords.Remove(record);

            // Update the database with the changes
            try
            {
                bc.SubmitChanges();
                gridView1.RefreshData();
            }
            catch (System.Data.SqlClient.SqlException ex)
            {
                DebtPlus.UI.Common.ErrorHandling.HandleErrors(ex);
            }
        }

        /// <summary>
        /// Formatting routine to format the GroupID
        /// </summary>
        private class GroupIDFormatter : ICustomFormatter, IFormatProvider
        {
            /// <summary>
            /// Convert the input argument to the string format of the group ID
            /// </summary>
            public string Format(string format1, object arg, IFormatProvider formatProvider)
            {
                var key = DebtPlus.Utils.Nulls.v_Int32(arg);
                if (key.HasValue)
                {
                    // Get the collection of group names
                    var col = Tables.GroupIDTable();
                    var q = col.Find(s => s.Id == key.Value);
                    if (q != null)
                    {
                        return q.description;
                    }
                }

                return string.Empty;
            }

            /// <summary>
            /// Determine which formatting routine handles which types of data
            /// </summary>
            public object GetFormat(Type formatType)
            {
                return this;
            }
        }

        /// <summary>
        /// Process the download button CLICK event
        /// </summary>
        private void barButtonItem_Download_ItemClick(object sender, EventArgs e)
        {
            using (var frm = new DownloadForm(bc, ref colRecords))
            {
                if (frm.ShowDialog() == System.Windows.Forms.DialogResult.OK)
                {
                    gridView1.RefreshData();
                }
            }
        }
    }
}