// You should enable one of the following
// #define TEST_PING
#define TEST_GROUP

// If you test with "TEST_GROUP", the "SAVE_GROUP" controls whether to save the data as a new XML document.
// #define SAVE_GROUP

#region "Copyright 2000-2012 DebtPlus, L.L.C."
//{*******************************************************************}
//{                                                                   }
//{       DebtPlus Debt Management System                             }
//{                                                                   }
//{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
//{       ALL RIGHTS RESERVED                                         }
//{                                                                   }
//{   The entire contents of this file is protected by U.S. and       }
//{   International Copyright Laws. Unauthorized reproduction,        }
//{   reverse-engineering, and distribution of all or any portion of  }
//{   the code contained in this file is strictly prohibited and may  }
//{   result in severe civil and criminal penalties and will be       }
//{   prosecuted to the maximum extent possible under the law.        }
//{                                                                   }
//{   RESTRICTIONS                                                    }
//{                                                                   }
//{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
//{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
//{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
//{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
//{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
//{                                                                   }
//{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
//{   ADDITIONAL RESTRICTIONS.                                        }
//{                                                                   }
//{*******************************************************************}

#endregion "Copyright 2000-2012 DebtPlus, L.L.C."

using System;
using System.ComponentModel;
using System.Linq;
using System.Net;
using System.Text;
using System.Windows.Forms;
using DebtPlus.LINQ;
using DevExpress.XtraWizard;

namespace DebtPlus.UI.TableAdministration.CS.Housing.ARMReferenceInfo
{
    internal partial class DownloadForm : DebtPlus.Data.Forms.DebtPlusForm
    {
        // Local storage
        private BackgroundWorker btValidate = new BackgroundWorker();

        private BackgroundWorker btDownload = new BackgroundWorker();

        private delegate void Validate_Completed_Delegate(bool Result);

        private delegate void WriteStatus_Delegate(string StatusBuffer);

        // Collection of referenced items
        private System.Collections.Generic.List<Housing_ARM_referenceInfo> colItems = null;

        private BusinessContext bc = null;

        /// <summary>
        /// Initialize the new form class instance
        /// </summary>
        internal DownloadForm()
            : base()
        {
            InitializeComponent();
        }

        /// <summary>
        /// Initialize the new form class instance
        /// </summary>
        internal DownloadForm(BusinessContext bc, ref System.Collections.Generic.List<Housing_ARM_referenceInfo> colItems)
            : this()
        {
            this.bc = bc;
            this.colItems = colItems;
            RegisterHandlers();
        }

        private void RegisterHandlers()
        {
            WizardControl1.SelectedPageChanged += WizardControl1_SelectedPageChanged;
            btValidate.RunWorkerCompleted += validate_bt_RunWorkerCompleted;
            btValidate.DoWork += validate_bt_DoWork;
            btDownload.RunWorkerCompleted += bt_Download_RunWorkerCompleted;
            btDownload.DoWork += bt_Download_DoWork;
            WizardControl1.NextClick += WizardControl1_Input_NextClick;
            WizardControl1.PrevClick += WizardControl1_Validate_PrevClick;
        }

        private void UnRegisterHandlers()
        {
            WizardControl1.SelectedPageChanged -= WizardControl1_SelectedPageChanged;
            btValidate.RunWorkerCompleted -= validate_bt_RunWorkerCompleted;
            btValidate.DoWork -= validate_bt_DoWork;
            btDownload.RunWorkerCompleted -= bt_Download_RunWorkerCompleted;
            btDownload.DoWork -= bt_Download_DoWork;
            WizardControl1.NextClick -= WizardControl1_Input_NextClick;
            WizardControl1.PrevClick -= WizardControl1_Validate_PrevClick;
        }

        /// <summary>
        /// Process the page change event from the wizard control
        /// </summary>
        private void WizardControl1_SelectedPageChanged(object sender, WizardPageChangedEventArgs e)
        {
            // Process the INPUT page
            if (object.ReferenceEquals(WizardControl1.SelectedPage, WizardPage_Input))
            {
                ProcessInputPage();
                return;
            }

            // Process the DOWNLOADING page
            if (object.ReferenceEquals(WizardControl1.SelectedPage, WizardPage_Downloading))
            {
                ProcessDownloadingPage();
                return;
            }

            // Process the READY page
            if (object.ReferenceEquals(WizardControl1.SelectedPage, WizardPage_Ready))
            {
                ProcessReadyPage();
                return;
            }

            // Process the SELECTED page
            if (object.ReferenceEquals(WizardControl1.SelectedPage, WizardPage_Validate))
            {
                ProcessValidatePage();
                return;
            }
        }

        /// <summary>
        /// Handle the READY page events
        /// </summary>
        private void ProcessReadyPage()
        {
        }

        /// <summary>
        /// Process the VALIDATE page
        /// </summary>
        private void ProcessValidatePage()
        {
            // Do the validation on the parameters in the background
            btValidate.RunWorkerAsync(createInputParameters());
        }

        /// <summary>
        /// Process the INPUT page setup dialog.
        /// </summary>
        private void ProcessInputPage()
        {
            var hcsID = bc.Housing_ARM_hcs_ids.Where(s => s.ActiveFlag).OrderBy(s => s.Id).Take(1).FirstOrDefault();
            if (string.IsNullOrWhiteSpace(TextEdit_HcsId.Text))
            {
                TextEdit_HcsId.EditValue = hcsID.Id;
            }

            if (string.IsNullOrWhiteSpace(TextEdit_UserName.Text))
            {
                TextEdit_UserName.EditValue = hcsID.Username;
            }

            if (string.IsNullOrWhiteSpace(TextEdit_Password.Text))
            {
                TextEdit_Password.Text = hcsID.Password;
            }

            if (string.IsNullOrWhiteSpace(TextEdit_URL.Text))
            {
                var cnf = new DebtPlus.SOAP.HUD.ConfigurationData();
                TextEdit_URL.Text = cnf.WebService;
            }
        }

        /// <summary>
        /// Process the Downloading page to do the download operation
        /// </summary>
        private void ProcessDownloadingPage()
        {
            btDownload.RunWorkerAsync(createInputParameters());
        }

        /// <summary>
        /// Retrieve the request structure from the input fields
        /// </summary>
        /// <returns></returns>
        private InputParameters createInputParameters()
        {
            var answer = new InputParameters()
            {
                HcsId = DebtPlus.Utils.Nulls.v_Int32(TextEdit_HcsId.EditValue).GetValueOrDefault(),
                Password = DebtPlus.Utils.Nulls.v_String(TextEdit_Password.EditValue),
                URL = DebtPlus.Utils.Nulls.v_String(TextEdit_URL.EditValue),
                UserName = DebtPlus.Utils.Nulls.v_String(TextEdit_UserName.EditValue)
            };

            return answer;
        }

        /// <summary>
        /// Process the NEXT button click event on the wizard control
        /// </summary>
        private void WizardControl1_Validate_NextClick(object sender, WizardCommandButtonClickEventArgs e)
        {
            // If the page is VALIDATE then we can't be busy.
            if (object.ReferenceEquals(WizardControl1.SelectedPage, WizardPage_Validate))
            {
                if (btValidate.IsBusy)
                {
                    e.Handled = true;
                }
            }
        }

        /// <summary>
        /// Process the CLICK event on the PREV button for the wizard
        /// </summary>
        private void WizardControl1_Validate_PrevClick(object sender, WizardCommandButtonClickEventArgs e)
        {
            // Do not allow the validate page to have previous.
            if (object.ReferenceEquals(WizardControl1.SelectedPage, WizardPage_Validate))
            {
                if (btValidate.IsBusy)
                {
                    e.Handled = true;
                }
            }
        }

        /// <summary>
        /// Handle the background worker completion logic
        /// </summary>
        private void validate_bt_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            // Determine the proper request to the Completed logic.
            object answer = e.Result;
            if (e.Cancelled || e.Error != null)
            {
                answer = false;
            }

            Validate_Completed(Convert.ToBoolean(answer));
        }

        /// <summary>
        /// Handle the COMPLETED event logic
        /// </summary>
        private void Validate_Completed(bool Success)
        {
            // If we need to invoke our code then do so now.
            if (WizardControl1.InvokeRequired)
            {
                WizardControl1.EndInvoke(WizardControl1.BeginInvoke(new Validate_Completed_Delegate(Validate_Completed), new object[] { Success }));
                return;
            }

            // Transfer to the next page based upon the success flag.
            WizardControl1.SelectedPage = Success ? WizardPage_Ready : WizardPage_Input;
        }

        /// <summary>
        /// Do the background work for the download operation
        /// </summary>
        private void validate_bt_DoWork(object sender, DoWorkEventArgs e)
        {
            InputParameters req = (InputParameters)e.Argument;
            e.Result = false;

            // Do a ping operation on the remote server to ensure connectivity
            try
            {
                var credentials = new NetworkCredential()
                {
                    UserName = req.UserName,
                    Password = req.Password
                };

                var svc = new DebtPlus.SOAP.HUD.ARM.ArmServiceImplService();
                svc.PreAuthenticate = true;
                svc.UseDefaultCredentials = false;
                svc.Url = req.URL;
                svc.Credentials = credentials;

#if TEST_PING
                var pingIn = new DebtPlus.SOAP.HUD.ARM.ping()
                {
                    agcHcsId = req.HcsId
                };

				// Do the ping operation. We don't care about the result, only that there is no error.
				string pingOut = svc.ping(pingIn);
#endif

#if TEST_GROUP
                DebtPlus.SOAP.HUD.ARM.referenceGroup[] refItem = null;
                var reqst = new DebtPlus.SOAP.HUD.ARM.getReferenceGroupNames();
                reqst.agcHcsId = req.HcsId;
                refItem = svc.getReferenceGroupNames(reqst);

#if SAVE_GROUP  // We may test by getting the groups but we don't necessarily save the data.
                string fileName = System.IO.Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments), "Groups.xml");
                if (System.IO.File.Exists(fileName))
                {
                    System.IO.File.Delete(fileName);
                }

				var fs = new System.IO.FileStream(fileName, System.IO.FileMode.Create);
				var txt = new System.IO.StreamWriter(fs);

				txt.WriteLine("<?xml version=\"1.0\" encoding=\"utf-8\" ?>");
				txt.WriteLine("<groups>");
				foreach (var x in refItem)
                {
					txt.WriteLine("    <group>");
					txt.WriteLine(string.Format("        <id>{0:f0}</id>", x.id));
					txt.WriteLine(string.Format("        <description>{0}</description>", DebtPlus.Utils.Format.Strings.toHTML(x.name)));
                    txt.WriteLine(string.Format("        <comment>{0}</comment>", DebtPlus.Utils.Format.Strings.toHTML(x.name)));
					txt.WriteLine("    </group>");
				}
				txt.WriteLine("</groups>");
				txt.Close();
				fs.Close();
#endif
#endif

                e.Result = true;
            }
            catch (Exception ex)
            {
                DebtPlus.Data.Forms.MessageBox.Show(string.Format("{0}\r\n\r\nAn error occurred with the information that you supplied.\r\nPlease correct it or click CANCEL to ignore the download", ex.Message), "Unable to perform download operation", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        /// <summary>
        /// Process the click on the NEXT button for the wizard
        /// </summary>
        private void WizardControl1_Input_NextClick(object sender, WizardCommandButtonClickEventArgs e)
        {
            // If the current page is the input, do not go on until we are complete.
            if (object.ReferenceEquals(WizardControl1.SelectedPage, WizardPage_Input) && createInputParameters().HasError())
            {
                e.Handled = true;
            }
        }

        /// <summary>
        /// The download operation has completed. Set the final page in the wizard.
        /// </summary>
        private void bt_Download_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            WizardControl1.SelectedPage = CompletionWizardPage_Completed;
        }

        /// <summary>
        /// Thread-safe function to update the status buffer with text.
        /// </summary>
        /// <param name="StatusBuffer">Message for the status buffer</param>
        private void WriteStatus(string StatusBuffer)
        {
            if (WizardControl1.InvokeRequired)
            {
                WizardControl1.EndInvoke(WizardControl1.BeginInvoke(new WriteStatus_Delegate(WriteStatus), new object[] { StatusBuffer }));
                return;
            }

            // Set the status text
            MemoEdit_Status.Text = StatusBuffer;

            // Position the cursor to the end of the input area
            MemoEdit_Status.SelectionLength = 0;
            MemoEdit_Status.SelectionStart = MemoEdit_Status.Text.Length;
            MemoEdit_Status.ScrollToCaret();

            MemoEdit_Status.Refresh();
        }

        private void bt_Download_DoWork(object sender, DoWorkEventArgs e)
        {
            StringBuilder sbStatus = new StringBuilder();
            InputParameters req = (InputParameters)e.Argument;

            // Initially indicate that the operation failed.
            e.Result = false;

            try
            {
                // Generate the service implementation and the security codes
                var svc = new DebtPlus.SOAP.HUD.ARM.ArmServiceImplService();

                // Allocate credentials to the new connection
                var credentials = new NetworkCredential()
                {
                    UserName = req.UserName,
                    Password = req.Password
                };

                svc.Url = req.URL;
                svc.Credentials = credentials;
                svc.PreAuthenticate = true;
                svc.Timeout = Int32.MaxValue;

                // Process all group IDs
                foreach (var idRecord in Tables.GroupIDTable())
                {
                    Int32 groupId = idRecord.Id;
                    sbStatus.AppendFormat("Requesting ID = {0:f0} ...", groupId);
                    WriteStatus(sbStatus.ToString());

                    DebtPlus.SOAP.HUD.ARM.referenceItem[] itemList = null;
                    try
                    {
                        var reqReference = new DebtPlus.SOAP.HUD.ARM.getReference()
                            {
                                agcHcsId = req.HcsId,
                                referenceId = groupId
                            };

                        itemList = svc.getReference(reqReference);
                        if (itemList != null)
                        {
                            sbStatus.AppendFormat(" {0:n0} item(s)", itemList.GetUpperBound(0) + 1);
                            WriteStatus(sbStatus.ToString());

                            // Process the items in the new transaction space
                            foreach (DebtPlus.SOAP.HUD.ARM.referenceItem refItem in itemList)
                            {
                                var qSearch = colItems.Where(s => s.groupId == groupId && s.arm_id == refItem.id).FirstOrDefault();
                                if (qSearch == null)
                                {
                                    qSearch = new DebtPlus.LINQ.Housing_ARM_referenceInfo()
                                    {
                                        groupId = groupId,
                                        arm_id = refItem.id
                                    };
                                    bc.Housing_ARM_referenceInfos.InsertOnSubmit(qSearch);
                                    colItems.Add(qSearch);
                                }

                                // Mark the item as being referenced so that it is not deleted and update the record correctly.
                                qSearch.Referenced = true;
                                qSearch.shortDesc = refItem.shortDesc;
                                qSearch.name = refItem.name;
                                qSearch.longDesc = refItem.longDesc;
                            }
                        }

                        // Write the status line to the console list
                        sbStatus.Append("\r\n");
                        WriteStatus(sbStatus.ToString());

                        // Finally, return success to the caller.
                        e.Result = true;
                    }
                    catch (Exception ex)
                    {
                        sbStatus.AppendFormat(" {0}", ex.Message);
                    }
                }

                // Go through the list and delete the items that are not referenced
                sbStatus.Append("Removing obsolete items\r\n");
                var qDel = colItems.Where(s => !s.Referenced).FirstOrDefault();
                while (qDel != null)
                {
                    bc.Housing_ARM_referenceInfos.DeleteOnSubmit(qDel);
                    colItems.Remove(qDel);
                    qDel = colItems.Where(s => !s.Referenced).FirstOrDefault();
                }

                // Update the database accordingly.
                sbStatus.Append("Submitting changes to the database\r\n");
                bc.SubmitChanges();
                sbStatus.Append("Completed\r\n");
            }
            catch (Exception ex)
            {
                DebtPlus.UI.Common.ErrorHandling.HandleErrors(ex);
            }
        }

        internal partial class InputParameters
        {
            internal Int32 HcsId;
            internal string UserName;
            internal string Password;
            internal string URL;

            /// <summary>
            /// Is the input structure valid to start processing the request?
            /// </summary>
            internal bool HasError()
            {
                if (HcsId <= 0)
                {
                    return true;
                }

                if (string.IsNullOrWhiteSpace(UserName))
                {
                    return true;
                }

                if (string.IsNullOrWhiteSpace(Password))
                {
                    return true;
                }

                if (string.IsNullOrWhiteSpace(URL))
                {
                    return true;
                }

                return false;
            }
        }
    }
}