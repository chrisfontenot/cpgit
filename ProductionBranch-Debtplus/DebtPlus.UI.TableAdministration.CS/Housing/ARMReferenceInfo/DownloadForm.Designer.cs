
using System.Windows.Forms;
namespace DebtPlus.UI.TableAdministration.CS.Housing.ARMReferenceInfo
{
	partial class DownloadForm
	{
        protected override void Dispose(bool disposing)
        {
            try
            {
                if (disposing)
                {
                    if (components != null) components.Dispose();
                }
                components = null;
            }
            finally
            {
                base.Dispose(disposing);
            }
        }

        //Required by the Windows Form Designer
        private System.ComponentModel.IContainer components = null;

        //NOTE: The following procedure is required by the Windows Form Designer
		//It can be modified using the Windows Form Designer.  
		//Do not modify it using the code editor.
		[System.Diagnostics.DebuggerStepThrough]
		private void InitializeComponent()
		{
			this.WizardControl1 = new DevExpress.XtraWizard.WizardControl();
			this.WelcomeWizardPage1 = new DevExpress.XtraWizard.WelcomeWizardPage();
			this.WizardPage_Input = new DevExpress.XtraWizard.WizardPage();
			this.TextEdit_URL = new DevExpress.XtraEditors.TextEdit();
			this.TextEdit_Password = new DevExpress.XtraEditors.TextEdit();
			this.TextEdit_HcsId = new DevExpress.XtraEditors.TextEdit();
			this.TextEdit_UserName = new DevExpress.XtraEditors.TextEdit();
			this.LabelControl4 = new DevExpress.XtraEditors.LabelControl();
			this.LabelControl3 = new DevExpress.XtraEditors.LabelControl();
			this.LabelControl2 = new DevExpress.XtraEditors.LabelControl();
			this.LabelControl1 = new DevExpress.XtraEditors.LabelControl();
			this.CompletionWizardPage_Completed = new DevExpress.XtraWizard.CompletionWizardPage();
			this.WizardPage_Downloading = new DevExpress.XtraWizard.WizardPage();
			this.MemoEdit_Status = new DevExpress.XtraEditors.MemoEdit();
			this.WizardPage_Validate = new DevExpress.XtraWizard.WizardPage();
			this.LabelControl6 = new DevExpress.XtraEditors.LabelControl();
			this.WizardPage_Ready = new DevExpress.XtraWizard.WizardPage();
			this.LabelControl5 = new DevExpress.XtraEditors.LabelControl();
			((System.ComponentModel.ISupportInitialize)this.DxErrorProvider1).BeginInit();
			((System.ComponentModel.ISupportInitialize)this.WizardControl1).BeginInit();
			this.WizardControl1.SuspendLayout();
			this.WizardPage_Input.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)this.TextEdit_URL.Properties).BeginInit();
			((System.ComponentModel.ISupportInitialize)this.TextEdit_Password.Properties).BeginInit();
			((System.ComponentModel.ISupportInitialize)this.TextEdit_HcsId.Properties).BeginInit();
			((System.ComponentModel.ISupportInitialize)this.TextEdit_UserName.Properties).BeginInit();
			this.WizardPage_Downloading.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)this.MemoEdit_Status.Properties).BeginInit();
			this.WizardPage_Validate.SuspendLayout();
			this.WizardPage_Ready.SuspendLayout();
			this.SuspendLayout();
			//
			//WizardControl1
			//
			this.WizardControl1.Controls.Add(this.WelcomeWizardPage1);
			this.WizardControl1.Controls.Add(this.WizardPage_Input);
			this.WizardControl1.Controls.Add(this.CompletionWizardPage_Completed);
			this.WizardControl1.Controls.Add(this.WizardPage_Downloading);
			this.WizardControl1.Controls.Add(this.WizardPage_Validate);
			this.WizardControl1.Controls.Add(this.WizardPage_Ready);
			this.WizardControl1.Dock = DockStyle.Fill;
			this.WizardControl1.Location = new System.Drawing.Point(0, 0);
			this.WizardControl1.Name = "WizardControl1";
			this.WizardControl1.Pages.AddRange(new DevExpress.XtraWizard.BaseWizardPage[] {
				this.WelcomeWizardPage1,
				this.WizardPage_Input,
				this.WizardPage_Validate,
				this.WizardPage_Ready,
				this.WizardPage_Downloading,
				this.CompletionWizardPage_Completed
			});
			this.WizardControl1.Size = new System.Drawing.Size(411, 266);
			this.WizardControl1.Text = "Input Specifications";
			//
			//WelcomeWizardPage1
			//
			this.WelcomeWizardPage1.IntroductionText = "Warning: This operation can not be un-done.\r\n\r\nIt will replace the current file contents with the values at the remote HUD site.";
			this.WelcomeWizardPage1.Name = "WelcomeWizardPage1";
			this.WelcomeWizardPage1.Size = new System.Drawing.Size(194, 110);
			this.WelcomeWizardPage1.Text = "Download table contents";
			//
			//WizardPage_Input
			//
			this.WizardPage_Input.Controls.Add(this.TextEdit_URL);
			this.WizardPage_Input.Controls.Add(this.TextEdit_Password);
			this.WizardPage_Input.Controls.Add(this.TextEdit_HcsId);
			this.WizardPage_Input.Controls.Add(this.TextEdit_UserName);
			this.WizardPage_Input.Controls.Add(this.LabelControl4);
			this.WizardPage_Input.Controls.Add(this.LabelControl3);
			this.WizardPage_Input.Controls.Add(this.LabelControl2);
			this.WizardPage_Input.Controls.Add(this.LabelControl1);
			this.WizardPage_Input.DescriptionText = "Specify the remote HUD system for access";
			this.WizardPage_Input.Name = "WizardPage_Input";
			this.WizardPage_Input.Size = new System.Drawing.Size(379, 121);
			this.WizardPage_Input.Text = "Input specifications";
			//
			//TextEdit_URL
			//
			this.TextEdit_URL.Location = new System.Drawing.Point(133, 85);
			this.TextEdit_URL.Name = "TextEdit_URL";
			this.TextEdit_URL.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.False;
			this.TextEdit_URL.Properties.Mask.BeepOnError = true;
			this.TextEdit_URL.Properties.Mask.EditMask = "http[s]?://.*";
			this.TextEdit_URL.Properties.Mask.IgnoreMaskBlank = false;
			this.TextEdit_URL.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.RegEx;
			this.TextEdit_URL.Properties.Mask.ShowPlaceHolders = false;
			this.TextEdit_URL.Size = new System.Drawing.Size(243, 20);
			this.TextEdit_URL.TabIndex = 7;
			//
			//TextEdit_Password
			//
			this.TextEdit_Password.Location = new System.Drawing.Point(133, 59);
			this.TextEdit_Password.Name = "TextEdit_Password";
			this.TextEdit_Password.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.False;
			this.TextEdit_Password.Properties.Mask.BeepOnError = true;
			this.TextEdit_Password.Properties.Mask.EditMask = "[^ ].*";
			this.TextEdit_Password.Properties.Mask.IgnoreMaskBlank = false;
			this.TextEdit_Password.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.RegEx;
			this.TextEdit_Password.Properties.Mask.ShowPlaceHolders = false;
			this.TextEdit_Password.Properties.PasswordChar = '*';
			this.TextEdit_Password.Size = new System.Drawing.Size(100, 20);
			this.TextEdit_Password.TabIndex = 5;
			//
			//TextEdit_HcsId
			//
			this.TextEdit_HcsId.Location = new System.Drawing.Point(133, 7);
			this.TextEdit_HcsId.Name = "TextEdit_HcsId";
			this.TextEdit_HcsId.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.False;
			this.TextEdit_HcsId.Properties.DisplayFormat.FormatString = "{0:f0}";
			this.TextEdit_HcsId.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
			this.TextEdit_HcsId.Properties.EditFormat.FormatString = "{0:f0}";
			this.TextEdit_HcsId.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
			this.TextEdit_HcsId.Properties.Mask.BeepOnError = true;
			this.TextEdit_HcsId.Properties.Mask.EditMask = "f0";
			this.TextEdit_HcsId.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
			this.TextEdit_HcsId.Size = new System.Drawing.Size(100, 20);
			this.TextEdit_HcsId.TabIndex = 1;
			//
			//TextEdit_UserName
			//
			this.TextEdit_UserName.Location = new System.Drawing.Point(133, 33);
			this.TextEdit_UserName.Name = "TextEdit_UserName";
			this.TextEdit_UserName.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.False;
			this.TextEdit_UserName.Properties.Mask.BeepOnError = true;
			this.TextEdit_UserName.Properties.Mask.EditMask = "[^ ].*";
			this.TextEdit_UserName.Properties.Mask.IgnoreMaskBlank = false;
			this.TextEdit_UserName.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.RegEx;
			this.TextEdit_UserName.Properties.Mask.ShowPlaceHolders = false;
			this.TextEdit_UserName.Size = new System.Drawing.Size(100, 20);
			this.TextEdit_UserName.TabIndex = 3;
			//
			//LabelControl4
			//
			this.LabelControl4.Location = new System.Drawing.Point(13, 88);
			this.LabelControl4.Name = "LabelControl4";
			this.LabelControl4.Size = new System.Drawing.Size(72, 13);
			this.LabelControl4.TabIndex = 6;
			this.LabelControl4.Text = "Remote Server";
			//
			//LabelControl3
			//
			this.LabelControl3.Location = new System.Drawing.Point(13, 62);
			this.LabelControl3.Name = "LabelControl3";
			this.LabelControl3.Size = new System.Drawing.Size(46, 13);
			this.LabelControl3.TabIndex = 4;
			this.LabelControl3.Text = "getPassword";
			//
			//LabelControl2
			//
			this.LabelControl2.Location = new System.Drawing.Point(13, 36);
			this.LabelControl2.Name = "LabelControl2";
			this.LabelControl2.Size = new System.Drawing.Size(88, 13);
			this.LabelControl2.TabIndex = 2;
			this.LabelControl2.Text = "Access User Name";
			//
			//LabelControl1
			//
			this.LabelControl1.Location = new System.Drawing.Point(13, 10);
			this.LabelControl1.Name = "LabelControl1";
			this.LabelControl1.Size = new System.Drawing.Size(87, 13);
			this.LabelControl1.TabIndex = 0;
			this.LabelControl1.Text = "Access ID Number";
			//
			//CompletionWizardPage_Completed
			//
			this.CompletionWizardPage_Completed.AllowBack = false;
			this.CompletionWizardPage_Completed.AllowNext = false;
			this.CompletionWizardPage_Completed.FinishText = "You have completed the file download operation. The table list will be refreshed " + "when you complete this form.";
			this.CompletionWizardPage_Completed.Name = "CompletionWizardPage_Completed";
			this.CompletionWizardPage_Completed.ProceedText = "To close this form, click Finish";
			this.CompletionWizardPage_Completed.Size = new System.Drawing.Size(194, 110);
			this.CompletionWizardPage_Completed.Text = "Operation Completed";
			//
			//WizardPage_Downloading
			//
			this.WizardPage_Downloading.AllowBack = false;
			this.WizardPage_Downloading.AllowNext = false;
			this.WizardPage_Downloading.Controls.Add(this.MemoEdit_Status);
			this.WizardPage_Downloading.DescriptionText = "Attempting the file download operation";
			this.WizardPage_Downloading.Margin = new Padding(0);
			this.WizardPage_Downloading.Name = "WizardPage_Downloading";
			this.WizardPage_Downloading.Size = new System.Drawing.Size(379, 121);
			this.WizardPage_Downloading.Text = "Download information";
			//
			//MemoEdit_Status
			//
			this.MemoEdit_Status.Dock = DockStyle.Fill;
			this.MemoEdit_Status.Location = new System.Drawing.Point(0, 0);
			this.MemoEdit_Status.Name = "MemoEdit_Status";
			this.MemoEdit_Status.Properties.AcceptsReturn = false;
			this.MemoEdit_Status.Size = new System.Drawing.Size(379, 121);
			this.MemoEdit_Status.TabIndex = 0;
			//
			//WizardPage_Validate
			//
			this.WizardPage_Validate.AllowBack = false;
			this.WizardPage_Validate.AllowNext = false;
			this.WizardPage_Validate.Controls.Add(this.LabelControl6);
			this.WizardPage_Validate.DescriptionText = "The connection information that you supplied is being validated";
			this.WizardPage_Validate.Name = "WizardPage_Validate";
			this.WizardPage_Validate.Size = new System.Drawing.Size(379, 121);
			this.WizardPage_Validate.Text = "Validating Connection";
			//
			//LabelControl6
			//
			this.LabelControl6.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
			this.LabelControl6.Appearance.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Top;
			this.LabelControl6.Appearance.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
			this.LabelControl6.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
			this.LabelControl6.Location = new System.Drawing.Point(4, 0);
			this.LabelControl6.Name = "LabelControl6";
			this.LabelControl6.Size = new System.Drawing.Size(372, 42);
			this.LabelControl6.TabIndex = 0;
			this.LabelControl6.Text = "Please wait while the connection information is being validated. This should only" + " take a moment.";
			//
			//WizardPage_Ready
			//
			this.WizardPage_Ready.AllowBack = false;
			this.WizardPage_Ready.Controls.Add(this.LabelControl5);
			this.WizardPage_Ready.DescriptionText = "The connection information is valid.";
			this.WizardPage_Ready.Name = "WizardPage_Ready";
			this.WizardPage_Ready.Size = new System.Drawing.Size(379, 121);
			this.WizardPage_Ready.Text = "Ready to start";
			//
			//LabelControl5
			//
			this.LabelControl5.AllowHtmlString = true;
			this.LabelControl5.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
			this.LabelControl5.Appearance.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Top;
			this.LabelControl5.Appearance.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
			this.LabelControl5.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
			this.LabelControl5.Location = new System.Drawing.Point(4, 4);
			this.LabelControl5.Name = "LabelControl5";
			this.LabelControl5.Size = new System.Drawing.Size(372, 52);
			this.LabelControl5.TabIndex = 0;
			this.LabelControl5.Text = "The information that you supplied is valid and a connection can be made. To start" + " the download, click the <b>NEXT</b> button below.";
			//
			//DownloadForm
			//
			this.AutoScaleDimensions = new System.Drawing.SizeF(6f, 13f);
			this.AutoScaleMode = AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(411, 266);
			this.Controls.Add(this.WizardControl1);
			this.LookAndFeel.UseDefaultLookAndFeel = true;
			this.Name = "DownloadForm";
			this.Text = "Download From HUD's site";
			((System.ComponentModel.ISupportInitialize)this.DxErrorProvider1).EndInit();
			((System.ComponentModel.ISupportInitialize)this.WizardControl1).EndInit();
			this.WizardControl1.ResumeLayout(false);
			this.WizardPage_Input.ResumeLayout(false);
			this.WizardPage_Input.PerformLayout();
			((System.ComponentModel.ISupportInitialize)this.TextEdit_URL.Properties).EndInit();
			((System.ComponentModel.ISupportInitialize)this.TextEdit_Password.Properties).EndInit();
			((System.ComponentModel.ISupportInitialize)this.TextEdit_HcsId.Properties).EndInit();
			((System.ComponentModel.ISupportInitialize)this.TextEdit_UserName.Properties).EndInit();
			this.WizardPage_Downloading.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)this.MemoEdit_Status.Properties).EndInit();
			this.WizardPage_Validate.ResumeLayout(false);
			this.WizardPage_Ready.ResumeLayout(false);
			this.ResumeLayout(false);

		}
		private DevExpress.XtraWizard.WizardControl WizardControl1;
		private DevExpress.XtraWizard.WelcomeWizardPage WelcomeWizardPage1;
		private DevExpress.XtraWizard.WizardPage WizardPage_Input;
		private DevExpress.XtraWizard.CompletionWizardPage CompletionWizardPage_Completed;
		private DevExpress.XtraWizard.WizardPage WizardPage_Downloading;
		private DevExpress.XtraEditors.TextEdit TextEdit_UserName;
		private DevExpress.XtraEditors.LabelControl LabelControl4;
		private DevExpress.XtraEditors.LabelControl LabelControl3;
		private DevExpress.XtraEditors.LabelControl LabelControl2;
		private DevExpress.XtraEditors.LabelControl LabelControl1;
		private DevExpress.XtraEditors.TextEdit TextEdit_URL;
		private DevExpress.XtraEditors.TextEdit TextEdit_Password;
		private DevExpress.XtraEditors.TextEdit TextEdit_HcsId;
		internal DevExpress.XtraEditors.MemoEdit MemoEdit_Status;
		private DevExpress.XtraWizard.WizardPage WizardPage_Validate;
		private DevExpress.XtraWizard.WizardPage WizardPage_Ready;
		private DevExpress.XtraEditors.LabelControl LabelControl5;
		private DevExpress.XtraEditors.LabelControl LabelControl6;
	}
}

//=======================================================
//Service provided by Telerik (www.telerik.com)
//Conversion powered by NRefactory.
//Twitter: @telerik
//Facebook: facebook.com/telerik
//=======================================================
