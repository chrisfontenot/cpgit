#region "Copyright 2000-2012 DebtPlus, L.L.C."
//{*******************************************************************}
//{                                                                   }
//{       DebtPlus Debt Management System                             }
//{                                                                   }
//{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
//{       ALL RIGHTS RESERVED                                         }
//{                                                                   }
//{   The entire contents of this file is protected by U.S. and       }
//{   International Copyright Laws. Unauthorized reproduction,        }
//{   reverse-engineering, and distribution of all or any portion of  }
//{   the code contained in this file is strictly prohibited and may  }
//{   result in severe civil and criminal penalties and will be       }
//{   prosecuted to the maximum extent possible under the law.        }
//{                                                                   }
//{   RESTRICTIONS                                                    }
//{                                                                   }
//{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
//{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
//{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
//{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
//{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
//{                                                                   }
//{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
//{   ADDITIONAL RESTRICTIONS.                                        }
//{                                                                   }
//{*******************************************************************}

#endregion "Copyright 2000-2012 DebtPlus, L.L.C."

using System;
using System.IO;
using System.Xml;

namespace DebtPlus.UI.TableAdministration.CS.Housing.ARMReferenceInfo
{
    internal partial class Tables
    {
        /// <summary>
        /// Item reflecting the group information in the XML document.
        /// </summary>
        internal class ARM_GroupID
        {
            internal ARM_GroupID()
            {
            }

            internal Int32 Id { get; set; }
            internal string description { get; set; }
            internal string comment { get; set; }
        }

        // Allocate the resulting collection
        private static System.Collections.Generic.List<ARM_GroupID> colItems = null;

        /// <summary>
        /// Local routine to find the collection of ARM Group IDs for translation
        /// </summary>
        /// <returns></returns>
        internal static System.Collections.Generic.List<ARM_GroupID> GroupIDTable()
        {
            // If the table was loaded previously then just return that value to the caller.
            if (colItems != null)
            {
                return colItems;
            }

            // Load the table with the row descriptions
            XmlDocument doc = new XmlDocument();
            string docList = string.Empty;

            try
            {
                // Read the XML document from the resource stream.
                var thisAssembly = System.Reflection.Assembly.GetExecutingAssembly();
                string fullName = thisAssembly.FullName.Split(',')[0];
                string ManifestName = string.Format("{0}.{1}.{2}", fullName, "Housing.ARMReferenceInfo", "Groups.xml");
                using (var fs = thisAssembly.GetManifestResourceStream(ManifestName))
                {
                    using (var sr = new StreamReader(fs))
                    {
                        docList = sr.ReadToEnd();
                        sr.Close();
                    }
                    fs.Close();
                }

                // If there is no document then we can't load it.
                if (docList.Length <= 0)
                {
                    return null;
                }

                // Try to load the document.
                doc.LoadXml(docList);
            }

            // On any error just return NULL to the caller for the condition.
            catch
            {
                return null;
            }

            // Allocate a list of the items for the collection. Now, the result may be an empty set.
            colItems = new System.Collections.Generic.List<ARM_GroupID>();
            foreach (XmlNode nodeItem in doc)
            {
                // There should be a match to the top-level node name.
                if (nodeItem.NodeType == XmlNodeType.Element && string.Compare(nodeItem.Name, "groups", true) == 0)
                {
                    // Look down the children nodes
                    foreach (XmlNode groupItem in nodeItem.ChildNodes)
                    {
                        Int32 id = -1;
                        string description = string.Empty;
                        string comment = string.Empty;

                        // Process each node in the list
                        foreach (XmlNode fieldItem in groupItem.ChildNodes)
                        {
                            if (fieldItem.NodeType == XmlNodeType.Element)
                            {
                                switch (fieldItem.Name.ToLower())
                                {
                                    case "id":
                                        Int32.TryParse(fieldItem.InnerText, out id);
                                        break;

                                    case "description":
                                        description = fieldItem.InnerText.Trim();
                                        break;

                                    case "comment":
                                        comment = fieldItem.InnerText.Trim();
                                        break;
                                }
                            }
                        }

                        // If there is something to add then add it.
                        if (id >= 0 && description != string.Empty)
                        {
                            var newItem = new ARM_GroupID()
                            {
                                comment = comment,
                                description = description,
                                Id = id
                            };
                            colItems.Add(newItem);
                        }
                    }
                }
            }

            // Return the new list to the caller.
            return colItems;
        }
    }
}