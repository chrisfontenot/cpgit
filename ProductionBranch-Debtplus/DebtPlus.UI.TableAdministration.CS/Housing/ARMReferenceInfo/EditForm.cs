#region "Copyright 2000-2012 DebtPlus, L.L.C."
//{*******************************************************************}
//{                                                                   }
//{       DebtPlus Debt Management System                             }
//{                                                                   }
//{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
//{       ALL RIGHTS RESERVED                                         }
//{                                                                   }
//{   The entire contents of this file is protected by U.S. and       }
//{   International Copyright Laws. Unauthorized reproduction,        }
//{   reverse-engineering, and distribution of all or any portion of  }
//{   the code contained in this file is strictly prohibited and may  }
//{   result in severe civil and criminal penalties and will be       }
//{   prosecuted to the maximum extent possible under the law.        }
//{                                                                   }
//{   RESTRICTIONS                                                    }
//{                                                                   }
//{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
//{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
//{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
//{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
//{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
//{                                                                   }
//{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
//{   ADDITIONAL RESTRICTIONS.                                        }
//{                                                                   }
//{*******************************************************************}

#endregion "Copyright 2000-2012 DebtPlus, L.L.C."

using System;
using DebtPlus.LINQ;

namespace DebtPlus.UI.TableAdministration.CS.Housing.ARMReferenceInfo
{
    internal partial class EditForm : Templates.EditTemplateForm
    {
        private BusinessContext bc = null;
        private Housing_ARM_referenceInfo record = null;

        internal EditForm()
            : base()
        {
            InitializeComponent();
        }

        internal EditForm(BusinessContext bc, Housing_ARM_referenceInfo record)
            : this()
        {
            this.bc = bc;
            this.record = record;
            RegisterHandlers();
        }

        private void RegisterHandlers()
        {
            this.Load += EditForm_Load;
        }

        private void UnRegisterHandlers()
        {
            this.Load -= EditForm_Load;
        }

        private void EditForm_Load(object sender, EventArgs e)
        {
            UnRegisterHandlers();
            try
            {
                // Define the lookup routines
                LookUpEdit_groupId.Properties.DataSource = Tables.GroupIDTable();

                // Load the record fields
                LookUpEdit_groupId.EditValue = record.groupId;
                TextEdit_ID.EditValue = record.arm_id;
                TextEdit_longDesc.EditValue = record.longDesc;
                TextEdit_name.EditValue = record.name;
                TextEdit_shortDesc.EditValue = record.shortDesc;

                simpleButton_OK.Enabled = !HasErrors();
            }
            finally
            {
                RegisterHandlers();
            }
        }

        private void Form_Changed(object sender, EventArgs e)
        {
            simpleButton_OK.Enabled = !HasErrors();
        }

        private bool HasErrors()
        {
            if (!DebtPlus.Utils.Nulls.v_Int32(LookUpEdit_groupId.EditValue).HasValue)
            {
                return true;
            }

            if (DebtPlus.Utils.Nulls.v_Int32(TextEdit_ID.EditValue).GetValueOrDefault(-1) < 0)
            {
                return true;
            }

            return false;
        }

        protected override void simpleButton_OK_Click(object sender, EventArgs e)
        {
            base.simpleButton_OK_Click(sender, e);

            record.arm_id = DebtPlus.Utils.Nulls.v_Int32(TextEdit_ID.EditValue).GetValueOrDefault();
            record.name = DebtPlus.Utils.Nulls.v_String(TextEdit_name.EditValue);
            record.groupId = DebtPlus.Utils.Nulls.v_Int32(LookUpEdit_groupId.EditValue).GetValueOrDefault();
            record.longDesc = DebtPlus.Utils.Nulls.v_String(TextEdit_longDesc.EditValue);
            record.shortDesc = DebtPlus.Utils.Nulls.v_String(TextEdit_shortDesc.EditValue);
        }
    }
}