﻿namespace DebtPlus.UI.TableAdministration.CS.Housing.LoanTypes
{
    partial class EditForm
    {
		//Form overrides dispose to clean up the component list.
		[System.Diagnostics.DebuggerNonUserCode]
		protected override void Dispose(bool disposing)
		{
			try 
            {
				if (disposing)
                {
                    if (components != null) components.Dispose();
				}
                components = null;
            }
    	    finally
            {
				base.Dispose(disposing);
			}
		}

		//Required by the Windows Form Designer
		private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.LayoutControl1 = new DevExpress.XtraLayout.LayoutControl();
            this.CheckEdit_ActiveFlag = new DevExpress.XtraEditors.CheckEdit();
            this.LabelControl_ID = new DevExpress.XtraEditors.LabelControl();
            this.CheckEdit_default = new DevExpress.XtraEditors.CheckEdit();
            this.TextEdit_description = new DevExpress.XtraEditors.TextEdit();
            this.LookupEdit_hud_9902_section = new DevExpress.XtraEditors.LookUpEdit();
            this.LayoutControlGroup1 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.LayoutControlItem1 = new DevExpress.XtraLayout.LayoutControlItem();
            this.LayoutControlItem3 = new DevExpress.XtraLayout.LayoutControlItem();
            this.LayoutControlItem8 = new DevExpress.XtraLayout.LayoutControlItem();
            this.LayoutControlItem2 = new DevExpress.XtraLayout.LayoutControlItem();
            this.EmptySpaceItem1 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.LayoutControlItem9 = new DevExpress.XtraLayout.LayoutControlItem();
            ((System.ComponentModel.ISupportInitialize)(this.DxErrorProvider1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LayoutControl1)).BeginInit();
            this.LayoutControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.CheckEdit_ActiveFlag.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.CheckEdit_default.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TextEdit_description.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LookupEdit_hud_9902_section.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LayoutControlGroup1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LayoutControlItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LayoutControlItem3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LayoutControlItem8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LayoutControlItem2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.EmptySpaceItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LayoutControlItem9)).BeginInit();
            this.SuspendLayout();
            // 
            // simpleButton_OK
            // 
            this.simpleButton_OK.Location = new System.Drawing.Point(378, 12);
            // 
            // simpleButton_Cancel
            // 
            this.simpleButton_Cancel.Location = new System.Drawing.Point(378, 56);
            // 
            // DefaultLookAndFeel1
            // 
            this.DefaultLookAndFeel1.LookAndFeel.SkinName = "McSkin";
            // 
            // LayoutControl1
            // 
            this.LayoutControl1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.LayoutControl1.Controls.Add(this.CheckEdit_ActiveFlag);
            this.LayoutControl1.Controls.Add(this.LabelControl_ID);
            this.LayoutControl1.Controls.Add(this.CheckEdit_default);
            this.LayoutControl1.Controls.Add(this.TextEdit_description);
            this.LayoutControl1.Controls.Add(this.LookupEdit_hud_9902_section);
            this.LayoutControl1.Location = new System.Drawing.Point(0, 0);
            this.LayoutControl1.Name = "LayoutControl1";
            this.LayoutControl1.OptionsCustomizationForm.DesignTimeCustomizationFormPositionAndSize = new System.Drawing.Rectangle(447, 200, 250, 350);
            this.LayoutControl1.Root = this.LayoutControlGroup1;
            this.LayoutControl1.Size = new System.Drawing.Size(370, 188);
            this.LayoutControl1.TabIndex = 24;
            this.LayoutControl1.Text = "LayoutControl1";
            // 
            // CheckEdit_ActiveFlag
            // 
            this.CheckEdit_ActiveFlag.Location = new System.Drawing.Point(187, 156);
            this.CheckEdit_ActiveFlag.Name = "CheckEdit_ActiveFlag";
            this.CheckEdit_ActiveFlag.Properties.Caption = "Active";
            this.CheckEdit_ActiveFlag.Size = new System.Drawing.Size(171, 20);
            this.CheckEdit_ActiveFlag.StyleController = this.LayoutControl1;
            this.CheckEdit_ActiveFlag.TabIndex = 21;
            // 
            // LabelControl_ID
            // 
            this.LabelControl_ID.Location = new System.Drawing.Point(101, 12);
            this.LabelControl_ID.Name = "LabelControl_ID";
            this.LabelControl_ID.Size = new System.Drawing.Size(23, 13);
            this.LabelControl_ID.StyleController = this.LayoutControl1;
            this.LabelControl_ID.TabIndex = 1;
            this.LabelControl_ID.Text = "NEW";
            // 
            // CheckEdit_default
            // 
            this.CheckEdit_default.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.CheckEdit_default.Location = new System.Drawing.Point(12, 156);
            this.CheckEdit_default.Name = "CheckEdit_default";
            this.CheckEdit_default.Properties.Caption = "Default Item";
            this.CheckEdit_default.Size = new System.Drawing.Size(171, 20);
            this.CheckEdit_default.StyleController = this.LayoutControl1;
            this.CheckEdit_default.TabIndex = 20;
            // 
            // TextEdit_description
            // 
            this.TextEdit_description.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.TextEdit_description.Location = new System.Drawing.Point(101, 29);
            this.TextEdit_description.Name = "TextEdit_description";
            this.TextEdit_description.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.False;
            this.TextEdit_description.Properties.MaxLength = 50;
            this.TextEdit_description.Size = new System.Drawing.Size(257, 20);
            this.TextEdit_description.StyleController = this.LayoutControl1;
            this.TextEdit_description.TabIndex = 7;
            // 
            // LookupEdit_hud_9902_section
            // 
            this.LookupEdit_hud_9902_section.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.LookupEdit_hud_9902_section.Location = new System.Drawing.Point(101, 53);
            this.LookupEdit_hud_9902_section.Name = "LookupEdit_hud_9902_section";
            this.LookupEdit_hud_9902_section.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.True;
            this.LookupEdit_hud_9902_section.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LookupEdit_hud_9902_section.Properties.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("name", "ID", 20, DevExpress.Utils.FormatType.None, "", false, DevExpress.Utils.HorzAlignment.Default),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("longDesc", "Description", 20, DevExpress.Utils.FormatType.None, "", true, DevExpress.Utils.HorzAlignment.Default, DevExpress.Data.ColumnSortOrder.Ascending)});
            this.LookupEdit_hud_9902_section.Properties.DisplayMember = "longDesc";
            this.LookupEdit_hud_9902_section.Properties.MaxLength = 50;
            this.LookupEdit_hud_9902_section.Properties.NullText = "";
            this.LookupEdit_hud_9902_section.Properties.ShowFooter = false;
            this.LookupEdit_hud_9902_section.Properties.ShowHeader = false;
            this.LookupEdit_hud_9902_section.Properties.SortColumnIndex = 1;
            this.LookupEdit_hud_9902_section.Properties.ValueMember = "name";
            this.LookupEdit_hud_9902_section.Size = new System.Drawing.Size(257, 20);
            this.LookupEdit_hud_9902_section.StyleController = this.LayoutControl1;
            this.LookupEdit_hud_9902_section.TabIndex = 5;
            // 
            // LayoutControlGroup1
            // 
            this.LayoutControlGroup1.CustomizationFormText = "LayoutControlGroup1";
            this.LayoutControlGroup1.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.True;
            this.LayoutControlGroup1.GroupBordersVisible = false;
            this.LayoutControlGroup1.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.LayoutControlItem1,
            this.LayoutControlItem3,
            this.LayoutControlItem8,
            this.LayoutControlItem2,
            this.EmptySpaceItem1,
            this.LayoutControlItem9});
            this.LayoutControlGroup1.Location = new System.Drawing.Point(0, 0);
            this.LayoutControlGroup1.Name = "LayoutControlGroup1";
            this.LayoutControlGroup1.Size = new System.Drawing.Size(370, 188);
            this.LayoutControlGroup1.Text = "LayoutControlGroup1";
            this.LayoutControlGroup1.TextVisible = false;
            // 
            // LayoutControlItem1
            // 
            this.LayoutControlItem1.Control = this.LabelControl_ID;
            this.LayoutControlItem1.CustomizationFormText = "Message ID";
            this.LayoutControlItem1.Location = new System.Drawing.Point(0, 0);
            this.LayoutControlItem1.Name = "LayoutControlItem1";
            this.LayoutControlItem1.Size = new System.Drawing.Size(350, 17);
            this.LayoutControlItem1.Text = "Message ID";
            this.LayoutControlItem1.TextSize = new System.Drawing.Size(86, 13);
            // 
            // LayoutControlItem3
            // 
            this.LayoutControlItem3.Control = this.TextEdit_description;
            this.LayoutControlItem3.CustomizationFormText = "Description";
            this.LayoutControlItem3.Location = new System.Drawing.Point(0, 17);
            this.LayoutControlItem3.Name = "LayoutControlItem3";
            this.LayoutControlItem3.Size = new System.Drawing.Size(350, 24);
            this.LayoutControlItem3.Text = "Description";
            this.LayoutControlItem3.TextSize = new System.Drawing.Size(86, 13);
            // 
            // LayoutControlItem8
            // 
            this.LayoutControlItem8.Control = this.CheckEdit_default;
            this.LayoutControlItem8.CustomizationFormText = "Default Item";
            this.LayoutControlItem8.Location = new System.Drawing.Point(0, 144);
            this.LayoutControlItem8.Name = "LayoutControlItem8";
            this.LayoutControlItem8.Size = new System.Drawing.Size(175, 24);
            this.LayoutControlItem8.Text = "Default Item";
            this.LayoutControlItem8.TextSize = new System.Drawing.Size(0, 0);
            this.LayoutControlItem8.TextToControlDistance = 0;
            this.LayoutControlItem8.TextVisible = false;
            // 
            // LayoutControlItem2
            // 
            this.LayoutControlItem2.Control = this.LookupEdit_hud_9902_section;
            this.LayoutControlItem2.CustomizationFormText = "HUD 9902 Section";
            this.LayoutControlItem2.Location = new System.Drawing.Point(0, 41);
            this.LayoutControlItem2.Name = "LayoutControlItem2";
            this.LayoutControlItem2.Size = new System.Drawing.Size(350, 24);
            this.LayoutControlItem2.Text = "HUD 9902 Section";
            this.LayoutControlItem2.TextSize = new System.Drawing.Size(86, 13);
            // 
            // EmptySpaceItem1
            // 
            this.EmptySpaceItem1.AllowHotTrack = false;
            this.EmptySpaceItem1.CustomizationFormText = "EmptySpaceItem1";
            this.EmptySpaceItem1.Location = new System.Drawing.Point(0, 65);
            this.EmptySpaceItem1.Name = "EmptySpaceItem1";
            this.EmptySpaceItem1.Size = new System.Drawing.Size(350, 79);
            this.EmptySpaceItem1.Text = "EmptySpaceItem1";
            this.EmptySpaceItem1.TextSize = new System.Drawing.Size(0, 0);
            // 
            // LayoutControlItem9
            // 
            this.LayoutControlItem9.Control = this.CheckEdit_ActiveFlag;
            this.LayoutControlItem9.CustomizationFormText = "Active Status";
            this.LayoutControlItem9.Location = new System.Drawing.Point(175, 144);
            this.LayoutControlItem9.Name = "LayoutControlItem9";
            this.LayoutControlItem9.Size = new System.Drawing.Size(175, 24);
            this.LayoutControlItem9.Text = "Active Status";
            this.LayoutControlItem9.TextSize = new System.Drawing.Size(0, 0);
            this.LayoutControlItem9.TextToControlDistance = 0;
            this.LayoutControlItem9.TextVisible = false;
            // 
            // EditForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(462, 190);
            this.Controls.Add(this.LayoutControl1);
            this.Name = "EditForm";
            this.Text = "Loan Type";
            this.Controls.SetChildIndex(this.simpleButton_OK, 0);
            this.Controls.SetChildIndex(this.simpleButton_Cancel, 0);
            this.Controls.SetChildIndex(this.LayoutControl1, 0);
            ((System.ComponentModel.ISupportInitialize)(this.DxErrorProvider1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LayoutControl1)).EndInit();
            this.LayoutControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.CheckEdit_ActiveFlag.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.CheckEdit_default.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TextEdit_description.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LookupEdit_hud_9902_section.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LayoutControlGroup1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LayoutControlItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LayoutControlItem3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LayoutControlItem8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LayoutControlItem2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.EmptySpaceItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LayoutControlItem9)).EndInit();
            this.ResumeLayout(false);

        }


        private DevExpress.XtraLayout.LayoutControl LayoutControl1;
        private DevExpress.XtraEditors.CheckEdit CheckEdit_ActiveFlag;
        private DevExpress.XtraEditors.LabelControl LabelControl_ID;
        private DevExpress.XtraEditors.CheckEdit CheckEdit_default;
        private DevExpress.XtraEditors.TextEdit TextEdit_description;
        private DevExpress.XtraEditors.LookUpEdit LookupEdit_hud_9902_section;
        private DevExpress.XtraLayout.LayoutControlGroup LayoutControlGroup1;
        private DevExpress.XtraLayout.LayoutControlItem LayoutControlItem1;
        private DevExpress.XtraLayout.LayoutControlItem LayoutControlItem3;
        private DevExpress.XtraLayout.LayoutControlItem LayoutControlItem8;
        private DevExpress.XtraLayout.LayoutControlItem LayoutControlItem2;
        private DevExpress.XtraLayout.EmptySpaceItem EmptySpaceItem1;
        private DevExpress.XtraLayout.LayoutControlItem LayoutControlItem9;
    }
}