﻿using System;
using System.Linq;
using DebtPlus.LINQ;

namespace DebtPlus.UI.TableAdministration.CS.Housing.LoanTypes
{
    public partial class MainForm : Templates.MainForm
    {
        /// <summary>
        /// List of the records to be edited in the program
        /// </summary>
        private System.Collections.Generic.List<Housing_LoanType> colRecords;
        private BusinessContext bc = new BusinessContext();

        /// <summary>
        /// Initialize the new class
        /// </summary>
        public MainForm()
            : base()
        {
            InitializeComponent();
            RegisterHandlers();
        }

        private void RegisterHandlers()
        {
            Load += MainForm_Load;
        }

        private void UnRegisterHandlers()
        {
            Load -= MainForm_Load;
        }

        /// <summary>
        /// Process the load event on the form
        /// </summary>
        private void MainForm_Load(object sender, System.EventArgs e)
        {
            using (var cm = new DebtPlus.UI.Common.CursorManager())
            {
                try
                {
                    // Retrieve the list of menu items
                    colRecords = bc.Housing_LoanTypes.ToList();
                    gridControl1.DataSource = colRecords;
                }
                catch (System.Data.SqlClient.SqlException ex)
                {
                    DebtPlus.UI.Common.ErrorHandling.HandleErrors(ex);
                }
            }
        }

        /// <summary>
        /// Handle the EDIT sequence
        /// </summary>
        /// <param name="obj"></param>
        protected override void UpdateRecord(object obj)
        {
            var record = obj as Housing_LoanType;
            if (record == null)
            {
                return;
            }

            bool previousDefault = record.Default;
            using (var frm = new EditForm(record))
            {
                if (frm.ShowDialog() != System.Windows.Forms.DialogResult.OK)
                {
                    return;
                }
            }

            // If the default was changed from FALSE to TRUE then clear the other defaults
            if (record.Default && !previousDefault)
            {
                foreach(var q in colRecords.Where(s => s.Default))
                {
                    q.Default = false;
                }
                record.Default = true;
            }

            try
            {
                bc.SubmitChanges();
                gridView1.RefreshData();
            }
            catch(System.Data.SqlClient.SqlException ex)
            {
                DebtPlus.UI.Common.ErrorHandling.HandleErrors(ex);
            }
        }

        /// <summary>
        /// Handle the DELETE sequence
        /// </summary>
        /// <param name="obj"></param>
        protected override void DeleteRecord(object obj)
        {
            // Find the record to purge
            var record = obj as Housing_LoanType;
            if (record == null)
            {
                return;
            }

            // Ask for confirmation before removing a record
            if (DebtPlus.Data.Prompts.RequestConfirmation_Delete() != System.Windows.Forms.DialogResult.Yes)
            {
                return;
            }

            // Remove the record form the database and the list
            bc.Housing_LoanTypes.DeleteOnSubmit(record);
            try
            {
                bc.SubmitChanges();
                colRecords.Remove(record);
                gridView1.RefreshData();
            }
            catch (System.Data.SqlClient.SqlException ex)
            {
                DebtPlus.UI.Common.ErrorHandling.HandleErrors(ex);
            }
        }

        /// <summary>
        /// Handle the CREATE sequence
        /// </summary>
        protected override void CreateRecord()
        {
            Housing_LoanType record = DebtPlus.LINQ.Factory.Manufacture_Housing_LoanType();
            using (var frm = new EditForm(record))
            {
                if (frm.ShowDialog() != System.Windows.Forms.DialogResult.OK)
                {
                    return;
                }
            }

            // If the default was changed from FALSE to TRUE then clear the other defaults
            if (record.Default)
            {
                foreach (var q in colRecords.Where(s => s.Default))
                {
                    q.Default = false;
                }
            }

            bc.Housing_LoanTypes.InsertOnSubmit(record);
            try
            {
                bc.SubmitChanges();
                colRecords.Add(record);
                gridView1.RefreshData();
            }
            catch (System.Data.SqlClient.SqlException ex)
            {
                DebtPlus.UI.Common.ErrorHandling.HandleErrors(ex);
            }
        }
    }
}