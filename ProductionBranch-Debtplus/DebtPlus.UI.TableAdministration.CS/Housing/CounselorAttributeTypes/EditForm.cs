using System;
using DebtPlus.LINQ;

namespace DebtPlus.UI.TableAdministration.CS.Housing.CounselorAttributeTypes
{
    internal partial class EditForm : Templates.EditTemplateForm
    {
        private Housing_CounselorAttributeType record;

        internal EditForm()
            : base()
        {
            InitializeComponent();
        }

        internal EditForm(Housing_CounselorAttributeType record)
            : this()
        {
            this.record = record;
            RegisterHandlers();
        }

        private void RegisterHandlers()
        {
            Load += EditForm_Load;
            LookupEdit_hud_9902_section.EditValueChanged += Form_Changed;
            CheckEdit_ActiveFlag.CheckedChanged += Form_Changed;
            TextEdit_description.EditValueChanged += Form_Changed;
        }

        private void UnRegisterHandlers()
        {
            Load -= EditForm_Load;
            LookupEdit_hud_9902_section.EditValueChanged -= Form_Changed;
            CheckEdit_ActiveFlag.CheckedChanged -= Form_Changed;
            TextEdit_description.EditValueChanged -= Form_Changed;
        }

        private void EditForm_Load(object sender, EventArgs e)
        {
            UnRegisterHandlers();
            try
            {
                // Load the list of items into the HUD translation
                LookupEdit_hud_9902_section.Properties.DataSource = DebtPlus.LINQ.Cache.Housing_ARM_referenceInfo.getList().FindAll(s => s.groupId == 36);

                // The ID of the record is a simple text buffer
                LabelControl_ID.Text = (record.Id < 1) ? "NEW" : record.Id.ToString();

                // Define the fields that may be changed
                CheckEdit_ActiveFlag.Checked = record.ActiveFlag;
                TextEdit_description.EditValue = record.description;
                LookupEdit_hud_9902_section.EditValue = record.hud_9902_section;
            }
            finally
            {
                RegisterHandlers();
            }
            simpleButton_OK.Enabled = !HasErrors();
        }

        private bool HasErrors()
        {
            // The name is required.
            if (string.IsNullOrWhiteSpace(DebtPlus.Utils.Nulls.v_String(TextEdit_description.EditValue)))
            {
                return true;
            }

            return false;
        }

        private void Form_Changed(object sender, EventArgs e)
        {
            simpleButton_OK.Enabled = !HasErrors();
        }

        protected override void simpleButton_OK_Click(object sender, EventArgs e)
        {
            // Retrieve the values from the controls when the record is accepted.
            record.ActiveFlag = CheckEdit_ActiveFlag.Checked;
            record.description = DebtPlus.Utils.Nulls.v_String(TextEdit_description.EditValue);
            record.hud_9902_section = DebtPlus.Utils.Nulls.v_String(LookupEdit_hud_9902_section.EditValue);

            base.simpleButton_OK_Click(sender, e);
        }
    }
}