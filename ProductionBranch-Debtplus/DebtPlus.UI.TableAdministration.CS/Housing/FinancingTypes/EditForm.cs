﻿using System;
using System.Linq;

namespace DebtPlus.UI.TableAdministration.CS.Housing.FinancingTypes
{
    internal partial class EditForm : DebtPlus.UI.TableAdministration.CS.Templates.EditTemplateForm
    {
        private DebtPlus.LINQ.Housing_FinancingType record;

        /// <summary>
        /// Initialize the new class
        /// </summary>
        internal EditForm()
        {
            InitializeComponent();
        }

        /// <summary>
        /// Initialize the new class
        /// </summary>
        internal EditForm(DebtPlus.LINQ.Housing_FinancingType record)
            : this()
        {
            this.record = record;
            LookupEdit_hud_9902_section.Properties.DataSource = DebtPlus.LINQ.Cache.Housing_ARM_referenceInfo.getList().Where(s => s.groupId == 14).ToList();
            RegisterHandlers();
        }

        /// <summary>
        /// Register the event handlers
        /// </summary>
        private void RegisterHandlers()
        {
            this.Load += EditForm_Load;
            this.TextEdit_description.EditValueChanged += FormChanged;
        }

        /// <summary>
        /// Remove the event handler registration
        /// </summary>
        private void UnRegisterHandlers()
        {
            this.Load -= EditForm_Load;
            this.TextEdit_description.EditValueChanged -= FormChanged;
        }

        /// <summary>
        /// Handle the LOAD sequence
        /// </summary>
        private void EditForm_Load(object sender, EventArgs e)
        {
            UnRegisterHandlers();
            try
            {
                // Set the record into the controls
                CheckEdit_ActiveFlag.Checked          = record.ActiveFlag;
                CheckEdit_default.Checked             = record.Default;
                TextEdit_description.EditValue        = record.description;
                LookupEdit_hud_9902_section.EditValue = record.hud_9902_section;

                // Include the record ID
                LabelControl_ID.Text = (record.Id > 0) ? record.Id.ToString() : "NEW";

                simpleButton_OK.Enabled = !HasErrors();
            }
            finally
            {
                RegisterHandlers();
            }
        }

        /// <summary>
        /// Process a change in the form that effects the enabling of the OK button
        /// </summary>
        private void FormChanged(object sender, EventArgs e)
        {
            simpleButton_OK.Enabled = !HasErrors();
        }

        /// <summary>
        /// Does the form contain a valid record?
        /// </summary>
        private bool HasErrors()
        {
            return TextEdit_description.EditValue != null;
        }

        /// <summary>
        /// Handle the CLICK event on the OK button
        /// </summary>
        protected override void simpleButton_OK_Click(object sender, EventArgs e)
        {
            base.simpleButton_OK_Click(sender, e);

            record.ActiveFlag       = CheckEdit_ActiveFlag.Checked;
            record.Default          = CheckEdit_default.Checked;
            record.description      = Convert.ToString(TextEdit_description.EditValue);
            record.hud_9902_section = DebtPlus.Utils.Nulls.v_String(LookupEdit_hud_9902_section.EditValue);
        }
    }
}