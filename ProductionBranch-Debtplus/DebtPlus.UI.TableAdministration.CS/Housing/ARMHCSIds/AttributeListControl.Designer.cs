
using System;
using System.Windows.Forms;
using System.Drawing;
using DebtPlus.LINQ;
using System.Linq;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Diagnostics;

namespace DebtPlus.UI.TableAdministration.CS.Housing.ARMHCSIds
{
	partial class AttributeListControl
	{
		//UserControl overrides dispose to clean up the component list.
		[System.Diagnostics.DebuggerNonUserCode]
		protected override void Dispose(bool disposing)
		{
			if (disposing && components != null) 
{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		//Required by the Windows Form Designer

		private System.ComponentModel.IContainer components = null;
		//NOTE: The following procedure is required by the Windows Form Designer
		//It can be modified using the Windows Form Designer.  
		//Do not modify it using the code editor.
		[System.Diagnostics.DebuggerStepThrough]
		private void InitializeComponent()
		{
			this.GroupControl1 = new DevExpress.XtraEditors.GroupControl();
			this.CheckedListBoxControl_Items = new DevExpress.XtraEditors.CheckedListBoxControl();
			((System.ComponentModel.ISupportInitialize)this.GroupControl1).BeginInit();
			this.GroupControl1.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)this.CheckedListBoxControl_Items).BeginInit();
			this.SuspendLayout();
			//
			//GroupControl1
			//
			this.GroupControl1.Controls.Add(this.CheckedListBoxControl_Items);
			this.GroupControl1.Dock = DockStyle.Fill;
			this.GroupControl1.Location = new System.Drawing.Point(0, 0);
			this.GroupControl1.Name = "GroupControl1";
			this.GroupControl1.Size = new System.Drawing.Size(150, 150);
			this.GroupControl1.TabIndex = 1;
			this.GroupControl1.Text = "Languages";
			//
			//CheckedListBoxControl_Items
			//
			this.CheckedListBoxControl_Items.CheckOnClick = true;
			this.CheckedListBoxControl_Items.Dock = DockStyle.Fill;
			this.CheckedListBoxControl_Items.Location = new System.Drawing.Point(2, 22);
			this.CheckedListBoxControl_Items.Name = "CheckedListBoxControl_Items";
			this.CheckedListBoxControl_Items.Size = new System.Drawing.Size(146, 126);
			this.CheckedListBoxControl_Items.TabIndex = 1;
			this.CheckedListBoxControl_Items.ToolTip = "Choose the attributes associated with the record";
			this.CheckedListBoxControl_Items.ToolTipTitle = "Item Attributes";
			//
			//AttributeListControl
			//
			this.AutoScaleDimensions = new System.Drawing.SizeF(6f, 13f);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.Controls.Add(this.GroupControl1);
			this.Name = "AttributeListControl";
			((System.ComponentModel.ISupportInitialize)this.GroupControl1).EndInit();
			this.GroupControl1.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)this.CheckedListBoxControl_Items).EndInit();
			this.ResumeLayout(false);

		}
		protected DevExpress.XtraEditors.GroupControl GroupControl1;

		protected DevExpress.XtraEditors.CheckedListBoxControl CheckedListBoxControl_Items;
	}
}
