#region "Copyright 2000-2012 DebtPlus, L.L.C."
//{*******************************************************************}
//{                                                                   }
//{       DebtPlus Debt Management System                             }
//{                                                                   }
//{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
//{       ALL RIGHTS RESERVED                                         }
//{                                                                   }
//{   The entire contents of this file is protected by U.S. and       }
//{   International Copyright Laws. Unauthorized reproduction,        }
//{   reverse-engineering, and distribution of all or any portion of  }
//{   the code contained in this file is strictly prohibited and may  }
//{   result in severe civil and criminal penalties and will be       }
//{   prosecuted to the maximum extent possible under the law.        }
//{                                                                   }
//{   RESTRICTIONS                                                    }
//{                                                                   }
//{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
//{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
//{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
//{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
//{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
//{                                                                   }
//{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
//{   ADDITIONAL RESTRICTIONS.                                        }
//{                                                                   }
//{*******************************************************************}

#endregion "Copyright 2000-2012 DebtPlus, L.L.C."

using System;
using System.Linq;
using System.Windows.Forms;
using DebtPlus.LINQ;

namespace DebtPlus.UI.TableAdministration.CS.Housing.ARMHCSIds
{
    internal partial class AttributeListControl : DevExpress.XtraEditors.XtraUserControl, DevExpress.Utils.Controls.IXtraResizableControl
    {
        /// <summary>
        /// Initialize the new control class
        /// </summary>
        internal AttributeListControl()
            : base()
        {
            InitializeComponent();
        }

        /// <summary>
        /// Register the event handlers
        /// </summary>
        private void RegisterHandlers()
        {
            CheckedListBoxControl_Items.ItemCheck += CheckedListBoxControl_Attributes_ItemCheck;
        }

        /// <summary>
        /// Remove the event handler registrations
        /// </summary>
        private void UnRegisterHandlers()
        {
            CheckedListBoxControl_Items.ItemCheck -= CheckedListBoxControl_Attributes_ItemCheck;
        }

        /// <summary>
        /// Handle the change in the checkbox for the control.
        /// </summary>
        private void CheckedListBoxControl_Attributes_ItemCheck(object sender, DevExpress.XtraEditors.Controls.ItemCheckEventArgs e)
        {
            RaiseEditValueChanged();
        }

        /// <summary>
        /// Raised when the check list is modified
        /// </summary>
        public event System.EventHandler EditValueChanged;

        /// <summary>
        /// Raise the EditValueChanged event
        /// </summary>
        protected void RaiseEditValueChanged()
        {
            var evt = EditValueChanged;
            if (evt != null)
            {
                evt(this, EventArgs.Empty);
            }
        }

        /// <summary>
        /// Load the information into the displayed form
        /// </summary>
        protected internal virtual void ReadForm(BusinessContext bc, Housing_ARM_hcs_id record)
        {
            UnRegisterHandlers();
            try
            {
                // Read the list of attributes
                CheckedListBoxControl_Items.Items.Clear();
                CheckedListBoxControl_Items.SortOrder = SortOrder.None;
                var colAttributes = DebtPlus.LINQ.Cache.AttributeType.getLanguageList();

                // Add the attributes to the control and then sort it once all attributes are loaded.
                colAttributes.ForEach(s => CheckedListBoxControl_Items.Items.Add(new DebtPlus.Data.Controls.SortedCheckedListboxControlItem(s, s.description, System.Windows.Forms.CheckState.Unchecked)));
                CheckedListBoxControl_Items.SortOrder = SortOrder.Ascending;

                // Read the list of counselor attributes
                foreach (var attr in record.Housing_ARM_hcs_id_languages)
                {
                    // Check the corresponding attribute
                    var q = CheckedListBoxControl_Items.Items.OfType<DebtPlus.Data.Controls.SortedCheckedListboxControlItem>().Where(s => ((AttributeType)((DebtPlus.Data.Controls.SortedCheckedListboxControlItem)s).Value).Id == attr.attribute).FirstOrDefault();
                    if (q != null)
                    {
                        q.CheckState = CheckState.Checked;
                    }
                }
            }
            finally
            {
                RegisterHandlers();
            }
        }

        /// <summary>
        /// Save the changes to the record collection. Update the list of checked items so that it matches the collection.
        /// </summary>
        protected internal virtual void SaveForm(BusinessContext bc, Housing_ARM_hcs_id record)
        {
            UnRegisterHandlers();
            try
            {
                foreach (var ctlListItem in CheckedListBoxControl_Items.Items.OfType<DebtPlus.Data.Controls.SortedCheckedListboxControlItem>())
                {
                    // Locate the items with the proper types
                    var ctl = ctlListItem.Value as DebtPlus.LINQ.AttributeType;
                    var q = record.Housing_ARM_hcs_id_languages.Where(s => s.attribute == ctl.Id).FirstOrDefault();

                    // Determine if the record is checked or not.
                    if (ctlListItem.CheckState == CheckState.Checked)
                    {
                        // The record is checked. Add it if it is not found.
                        if (q == null)
                        {
                            q = new Housing_ARM_hcs_id_language(); // fac.Housing_ARM_hcs_id_language();
                            q.attribute = ctl.Id;
                            record.Housing_ARM_hcs_id_languages.Add(q);
                        }
                    }
                    else
                    {
                        // The record is not checked. Delete it if it is found.
                        if (q != null)
                        {
                            record.Housing_ARM_hcs_id_languages.Remove(q);
                            q.Housing_ARM_hcs_id = null;
                        }
                    }
                }
            }
            finally
            {
                RegisterHandlers();
            }
        }

#pragma warning disable 67

        // Required for the DevExpress.Utils.Controls.IXtraResizableControl interface. We don't use it.
        public event EventHandler Changed;

#pragma warning restore 67

        public bool IsCaptionVisible
        {
            get { return false; }
        }

        public System.Drawing.Size MaxSize
        {
            get { return new System.Drawing.Size(0, 0); }
        }

        public System.Drawing.Size MinSize
        {
            get { return new System.Drawing.Size(0, 0); }
        }
    }
}