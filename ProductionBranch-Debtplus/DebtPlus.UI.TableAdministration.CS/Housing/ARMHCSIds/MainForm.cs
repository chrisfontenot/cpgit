#region "Copyright 2000-2012 DebtPlus, L.L.C."
//{*******************************************************************}
//{                                                                   }
//{       DebtPlus Debt Management System                             }
//{                                                                   }
//{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
//{       ALL RIGHTS RESERVED                                         }
//{                                                                   }
//{   The entire contents of this file is protected by U.S. and       }
//{   International Copyright Laws. Unauthorized reproduction,        }
//{   reverse-engineering, and distribution of all or any portion of  }
//{   the code contained in this file is strictly prohibited and may  }
//{   result in severe civil and criminal penalties and will be       }
//{   prosecuted to the maximum extent possible under the law.        }
//{                                                                   }
//{   RESTRICTIONS                                                    }
//{                                                                   }
//{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
//{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
//{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
//{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
//{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
//{                                                                   }
//{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
//{   ADDITIONAL RESTRICTIONS.                                        }
//{                                                                   }
//{*******************************************************************}

#endregion "Copyright 2000-2012 DebtPlus, L.L.C."

using System;
using System.Linq;
using DebtPlus.LINQ;
using DebtPlus.UI.Common;

namespace DebtPlus.UI.TableAdministration.CS.Housing.ARMHCSIds
{
    public partial class MainForm : Templates.MainForm
    {
        private BusinessContext bc = new BusinessContext();
        private System.Collections.Generic.List<Housing_ARM_hcs_id> colRecords = null;

        /// <summary>
        /// Process the initialization of the form
        /// </summary>
        public MainForm()
            : base()
        {
            InitializeComponent();
            RegisterHandlers();
        }

        /// <summary>
        /// Register the event handlers
        /// </summary>
        private void RegisterHandlers()
        {
            Load += MainForm_Load;
        }

        /// <summary>
        /// Remove the event handler registrations
        /// </summary>
        private void UnRegisterHandlers()
        {
            Load -= MainForm_Load;
        }

        /// <summary>
        /// Process the load event on the form
        /// </summary>
        private void MainForm_Load(object sender, EventArgs e)
        {
            UnRegisterHandlers();
            try
            {
                using (var cm = new CursorManager())
                {
                    colRecords = bc.Housing_ARM_hcs_ids.ToList();
                    gridControl1.DataSource = colRecords;
                    gridView1.BestFitColumns();
                }
            }
            catch (System.Data.SqlClient.SqlException ex)
            {
                DebtPlus.UI.Common.ErrorHandling.HandleErrors(ex);
            }
            finally
            {
                RegisterHandlers();
            }
        }

        /// <summary>
        /// Handle the edit of the information on the form
        /// </summary>
        protected override void UpdateRecord(object obj)
        {
            var record = obj as Housing_ARM_hcs_id;
            if (record == null)
            {
                return;
            }

            // Save the previous ID to detect a change in the table entry
            Int32 priorID = record.Id;
            bool priorDefault = record.Default;

            // Edit the record definition
            using (var frm = new HCSIDEditForm(bc, record))
            {
                if (frm.ShowDialog() != System.Windows.Forms.DialogResult.OK)
                {
                    bc.Refresh(System.Data.Linq.RefreshMode.OverwriteCurrentValues, record);
                    return;
                }
            }

            // If the default status is now set then clear the previous one
            if (record.Default && !priorDefault)
            {
                foreach (var defaultRecord in colRecords.Where(s => s.Default))
                {
                    defaultRecord.Default = false;
                }
                record.Default = true;
            }

            // If the ID has changed then we need to duplicate the record to a new item
            // and remove the previous entries from the system. Ugh!!
            if (priorID != record.Id)
            {
                // Create a new ID record
                var newRecord = new Housing_ARM_hcs_id()
                {
                    ActiveFlag = record.ActiveFlag,
                    colonias = record.colonias,
                    counseling_amount = record.counseling_amount,
                    Default = record.Default,
                    description = record.description,
                    faith_based = record.faith_based,
                    Id = record.Id,
                    migrant_farm_worker = record.migrant_farm_worker,
                    Password = record.Password,
                    SendClients = record.SendClients,
                    SendWorkshops = record.SendWorkshops,
                    Username = record.Username,
                    validation_date = record.validation_date
                };

                // Duplicate the appointment information
                while (record.Housing_ARM_hcs_id_AppointmentTypes.Count > 0)
                {
                    var apt = record.Housing_ARM_hcs_id_AppointmentTypes[0];
                    var newApt = new Housing_ARM_hcs_id_AppointmentType()
                    {
                        AppointmentType = apt.AppointmentType
                    };
                    newRecord.Housing_ARM_hcs_id_AppointmentTypes.Add(newApt);
                    record.Housing_ARM_hcs_id_AppointmentTypes.Remove(apt);
                    apt.Housing_ARM_hcs_id = null;
                }

                // Duplicate the languages
                while (record.Housing_ARM_hcs_id_languages.Count > 0)
                {
                    var lng = record.Housing_ARM_hcs_id_languages[0];
                    var newLng = new Housing_ARM_hcs_id_language()
                    {
                        attribute = lng.attribute
                    };
                    newRecord.Housing_ARM_hcs_id_languages.Add(newLng);
                    record.Housing_ARM_hcs_id_languages.Remove(lng);
                    lng.Housing_ARM_hcs_id = null;
                }

                // Delete the previous record from the database
                var qDelete = bc.Housing_ARM_hcs_ids.Where(s => s.Id == priorID).FirstOrDefault();
                if (qDelete != null)
                {
                    bc.Housing_ARM_hcs_ids.DeleteOnSubmit(qDelete);
                }
                colRecords.Remove(record);

                // Insert the new record into the database as an "add"
                colRecords.Add(newRecord);
                bc.Housing_ARM_hcs_ids.InsertOnSubmit(newRecord);
            }

            try
            {
                // Commit the changes to the database
                bc.SubmitChanges();
                gridView1.RefreshData();
            }
            catch (System.Data.SqlClient.SqlException ex)
            {
                DebtPlus.UI.Common.ErrorHandling.HandleErrors(ex);
            }
        }

        /// <summary>
        /// Delete the record from the database
        /// </summary>
        protected override void DeleteRecord(object obj)
        {
            // Find the record to delete
            var record = obj as Housing_ARM_hcs_id;
            if (record == null)
            {
                return;
            }

            // Ensure that the user really wanted to delete it.
            if (DebtPlus.Data.Prompts.RequestConfirmation_Delete() != System.Windows.Forms.DialogResult.Yes)
            {
                return;
            }

            // Delete the record. The system will do the rest.
            bc.Housing_ARM_hcs_ids.DeleteOnSubmit(record);
            colRecords.Remove(record);

            try
            {
                // Commit the changes to the database
                bc.SubmitChanges();
                gridView1.RefreshData();
            }
            catch (System.Data.SqlClient.SqlException ex)
            {
                DebtPlus.UI.Common.ErrorHandling.HandleErrors(ex);
            }
        }

        /// <summary>
        /// Create a new record in the database
        /// </summary>
        protected override void CreateRecord()
        {
            var record = DebtPlus.LINQ.Factory.Manufacture_Housing_ARM_hcs_id();

            // Edit the record definition
            using (var frm = new HCSIDEditForm(bc, record))
            {
                if (frm.ShowDialog() != System.Windows.Forms.DialogResult.OK)
                {
                    bc.Refresh(System.Data.Linq.RefreshMode.OverwriteCurrentValues, record);
                    return;
                }
            }

            // If the default status is now set then clear the previous one
            if (record.Default)
            {
                foreach (var defaultRecord in colRecords.Where(s => s.Default))
                {
                    defaultRecord.Default = false;
                }
                record.Default = true;
            }

            // Insert the new record into the database as an "add"
            colRecords.Add(record);
            bc.Housing_ARM_hcs_ids.InsertOnSubmit(record);

            try
            {
                // Commit the changes to the database
                bc.SubmitChanges();
                gridView1.RefreshData();
            }
            catch (System.Data.SqlClient.SqlException ex)
            {
                DebtPlus.UI.Common.ErrorHandling.HandleErrors(ex);
            }
        }
    }
}