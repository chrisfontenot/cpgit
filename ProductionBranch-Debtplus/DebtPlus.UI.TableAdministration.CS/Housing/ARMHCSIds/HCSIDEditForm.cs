#region "Copyright 2000-2012 DebtPlus, L.L.C."
//{*******************************************************************}
//{                                                                   }
//{       DebtPlus Debt Management System                             }
//{                                                                   }
//{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
//{       ALL RIGHTS RESERVED                                         }
//{                                                                   }
//{   The entire contents of this file is protected by U.S. and       }
//{   International Copyright Laws. Unauthorized reproduction,        }
//{   reverse-engineering, and distribution of all or any portion of  }
//{   the code contained in this file is strictly prohibited and may  }
//{   result in severe civil and criminal penalties and will be       }
//{   prosecuted to the maximum extent possible under the law.        }
//{                                                                   }
//{   RESTRICTIONS                                                    }
//{                                                                   }
//{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
//{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
//{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
//{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
//{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
//{                                                                   }
//{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
//{   ADDITIONAL RESTRICTIONS.                                        }
//{                                                                   }
//{*******************************************************************}

#endregion "Copyright 2000-2012 DebtPlus, L.L.C."

using System;
using DebtPlus.LINQ;

namespace DebtPlus.UI.TableAdministration.CS.Housing.ARMHCSIds
{
    internal partial class HCSIDEditForm : Templates.EditTemplateForm
    {
        private BusinessContext bc = null;
        private Housing_ARM_hcs_id record = null;

        /// <summary>
        /// Initialize the new form class instance
        /// </summary>
        internal HCSIDEditForm()
            : base()
        {
            InitializeComponent();
        }

        /// <summary>
        /// Initialize the new form class instance
        /// </summary>
        internal HCSIDEditForm(BusinessContext bc, Housing_ARM_hcs_id record)
            : this()
        {
            this.bc = bc;
            this.record = record;
            RegisterHandlers();
        }

        /// <summary>
        /// Register the event handlers
        /// </summary>
        private void RegisterHandlers()
        {
            Load += EditForm_Load;
            TextEdit_hcs_id.EditValueChanged += Form_Changed;
            TextEdit_Description.EditValueChanged += Form_Changed;
            TextEdit_password.EditValueChanged += Form_Changed;
            TextEdit_username.EditValueChanged += Form_Changed;
            DateEdit_validation_date.EditValueChanged += Form_Changed;
            CalcEdit_counseling_amount.EditValueChanged += Form_Changed;
            CheckEdit_activeflag.EditValueChanged += Form_Changed;
            CheckEdit_colonias.EditValueChanged += Form_Changed;
            CheckEdit_default.EditValueChanged += Form_Changed;
            CheckEdit_faith_based.EditValueChanged += Form_Changed;
            CheckEdit_migrant_farm_worker.EditValueChanged += Form_Changed;
            AppointmentTypeListControl1.EditValueChanged += Form_Changed;
            AttributeListControl1.EditValueChanged += Form_Changed;
        }

        /// <summary>
        /// Remove the event handler registrations
        /// </summary>
        private void UnRegisterHandlers()
        {
            Load -= EditForm_Load;
            TextEdit_hcs_id.EditValueChanged -= Form_Changed;
            TextEdit_Description.EditValueChanged -= Form_Changed;
            TextEdit_password.EditValueChanged -= Form_Changed;
            TextEdit_username.EditValueChanged -= Form_Changed;
            DateEdit_validation_date.EditValueChanged -= Form_Changed;
            CalcEdit_counseling_amount.EditValueChanged -= Form_Changed;
            CheckEdit_activeflag.EditValueChanged -= Form_Changed;
            CheckEdit_colonias.EditValueChanged -= Form_Changed;
            CheckEdit_default.EditValueChanged -= Form_Changed;
            CheckEdit_faith_based.EditValueChanged -= Form_Changed;
            CheckEdit_migrant_farm_worker.EditValueChanged -= Form_Changed;
            AppointmentTypeListControl1.EditValueChanged -= Form_Changed;
            AttributeListControl1.EditValueChanged -= Form_Changed;
        }

        /// <summary>
        /// Process the form LOAD event
        /// </summary>
        private void EditForm_Load(object sender, EventArgs e)
        {
            UnRegisterHandlers();
            try
            {
                // Bind the items to the editing controls
                TextEdit_hcs_id.EditValue = record.Id;
                TextEdit_Description.EditValue = record.description;
                TextEdit_password.EditValue = record.Password;
                TextEdit_username.EditValue = record.Username;
                DateEdit_validation_date.EditValue = record.validation_date;
                CalcEdit_counseling_amount.EditValue = record.counseling_amount;
                CheckEdit_activeflag.EditValue = record.ActiveFlag;
                CheckEdit_colonias.EditValue = record.colonias;
                CheckEdit_default.EditValue = record.Default;
                CheckEdit_faith_based.EditValue = record.faith_based;
                CheckEdit_migrant_farm_worker.EditValue = record.migrant_farm_worker;

                // Load the list of AppointmentTypes
                AppointmentTypeListControl1.ReadForm(bc, record);
                AttributeListControl1.ReadForm(bc, record);

                simpleButton_OK.Enabled = !HasErrors();
            }
            finally
            {
                RegisterHandlers();
            }
        }

        /// <summary>
        /// Process the click event on the OK button
        /// </summary>
        protected override void simpleButton_OK_Click(object sender, EventArgs e)
        {
            base.simpleButton_OK_Click(sender, e);

            // Update the record with the control values
            record.Id = DebtPlus.Utils.Nulls.v_Int32(TextEdit_hcs_id.EditValue).Value;
            record.description = DebtPlus.Utils.Nulls.v_String(TextEdit_Description.EditValue);
            record.Password = DebtPlus.Utils.Nulls.v_String(TextEdit_password.EditValue);
            record.Username = DebtPlus.Utils.Nulls.v_String(TextEdit_username.EditValue);
            record.validation_date = DebtPlus.Utils.Nulls.v_DateTime(DateEdit_validation_date.EditValue);
            record.counseling_amount = DebtPlus.Utils.Nulls.v_Decimal(CalcEdit_counseling_amount.EditValue).GetValueOrDefault();
            record.ActiveFlag = CheckEdit_activeflag.Checked;
            record.colonias = CheckEdit_colonias.Checked;
            record.Default = CheckEdit_default.Checked;
            record.faith_based = CheckEdit_faith_based.Checked;
            record.migrant_farm_worker = CheckEdit_migrant_farm_worker.Checked;

            // Save the list of AppointmentTypes
            AppointmentTypeListControl1.SaveForm(bc, record);
            AttributeListControl1.SaveForm(bc, record);
        }

        /// <summary>
        /// Process a change in the control for the form
        /// </summary>
        private void Form_Changed(object sender, EventArgs e)
        {
            simpleButton_OK.Enabled = !HasErrors();
        }

        /// <summary>
        /// Determine if the form has any error conditions for a valid record
        /// </summary>
        private bool HasErrors()
        {
            if (DebtPlus.Utils.Nulls.v_Int32(TextEdit_hcs_id.EditValue).GetValueOrDefault() <= 0)
            {
                return true;
            }

            if (string.IsNullOrWhiteSpace(DebtPlus.Utils.Nulls.v_String(TextEdit_Description.EditValue)))
            {
                return true;
            }

            if (string.IsNullOrWhiteSpace(DebtPlus.Utils.Nulls.v_String(TextEdit_username.EditValue)))
            {
                return true;
            }

            if (string.IsNullOrWhiteSpace(DebtPlus.Utils.Nulls.v_String(TextEdit_password.EditValue)))
            {
                return true;
            }

            return false;
        }
    }
}