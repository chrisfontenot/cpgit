
using System;
using System.Windows.Forms;
using System.Drawing;
using DebtPlus.LINQ;
using System.Linq;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Diagnostics;
using DebtPlus.UI.Common.Templates;

namespace DebtPlus.UI.TableAdministration.CS.Housing.ARMHCSIds
{
	partial class HCSIDEditForm
	{

		//Form overrides dispose to clean up the component list.
		[System.Diagnostics.DebuggerNonUserCode]
		protected override void Dispose(bool disposing)
		{
			try 
            {
				if (disposing && components != null) 
                {
					components.Dispose();
				}
            }
            finally 
            {
				base.Dispose(disposing);
			}
		}

		//Required by the Windows Form Designer
		private System.ComponentModel.IContainer components = null;

		//NOTE: The following procedure is required by the Windows Form Designer
		//It can be modified using the Windows Form Designer.  
		//Do not modify it using the code editor.
		[System.Diagnostics.DebuggerStepThrough]
		private void InitializeComponent()
		{
            this.LayoutControl1 = new DevExpress.XtraLayout.LayoutControl();
            this.AppointmentTypeListControl1 = new DebtPlus.UI.TableAdministration.CS.Housing.ARMHCSIds.AppointmentTypeListControl();
            this.TextEdit_Description = new DevExpress.XtraEditors.TextEdit();
            this.CheckEdit_activeflag = new DevExpress.XtraEditors.CheckEdit();
            this.CheckEdit_migrant_farm_worker = new DevExpress.XtraEditors.CheckEdit();
            this.AttributeListControl1 = new DebtPlus.UI.TableAdministration.CS.Housing.ARMHCSIds.AttributeListControl();
            this.CheckEdit_colonias = new DevExpress.XtraEditors.CheckEdit();
            this.CheckEdit_faith_based = new DevExpress.XtraEditors.CheckEdit();
            this.DateEdit_validation_date = new DevExpress.XtraEditors.DateEdit();
            this.CheckEdit_default = new DevExpress.XtraEditors.CheckEdit();
            this.CalcEdit_counseling_amount = new DevExpress.XtraEditors.CalcEdit();
            this.TextEdit_password = new DevExpress.XtraEditors.TextEdit();
            this.TextEdit_username = new DevExpress.XtraEditors.TextEdit();
            this.TextEdit_hcs_id = new DevExpress.XtraEditors.TextEdit();
            this.LayoutControlGroup1 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.LayoutControlItem2 = new DevExpress.XtraLayout.LayoutControlItem();
            this.LayoutControlItem5 = new DevExpress.XtraLayout.LayoutControlItem();
            this.LayoutControlItem6 = new DevExpress.XtraLayout.LayoutControlItem();
            this.LayoutControlItem7 = new DevExpress.XtraLayout.LayoutControlItem();
            this.LayoutControlItem8 = new DevExpress.XtraLayout.LayoutControlItem();
            this.LayoutControlItem9 = new DevExpress.XtraLayout.LayoutControlItem();
            this.LayoutControlItem10 = new DevExpress.XtraLayout.LayoutControlItem();
            this.LayoutControlItem12 = new DevExpress.XtraLayout.LayoutControlItem();
            this.LayoutControlItem4 = new DevExpress.XtraLayout.LayoutControlItem();
            this.LayoutControlItem11 = new DevExpress.XtraLayout.LayoutControlItem();
            this.LayoutControlItem3 = new DevExpress.XtraLayout.LayoutControlItem();
            this.LayoutControlItem1 = new DevExpress.XtraLayout.LayoutControlItem();
            this.LayoutControlItem13 = new DevExpress.XtraLayout.LayoutControlItem();
            ((System.ComponentModel.ISupportInitialize)(this.DxErrorProvider1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LayoutControl1)).BeginInit();
            this.LayoutControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.TextEdit_Description.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.CheckEdit_activeflag.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.CheckEdit_migrant_farm_worker.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.CheckEdit_colonias.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.CheckEdit_faith_based.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DateEdit_validation_date.Properties.VistaTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DateEdit_validation_date.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.CheckEdit_default.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.CalcEdit_counseling_amount.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TextEdit_password.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TextEdit_username.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TextEdit_hcs_id.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LayoutControlGroup1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LayoutControlItem2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LayoutControlItem5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LayoutControlItem6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LayoutControlItem7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LayoutControlItem8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LayoutControlItem9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LayoutControlItem10)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LayoutControlItem12)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LayoutControlItem4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LayoutControlItem11)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LayoutControlItem3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LayoutControlItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LayoutControlItem13)).BeginInit();
            this.SuspendLayout();
            // 
            // simpleButton_OK
            // 
            this.simpleButton_OK.Enabled = true;
            this.simpleButton_OK.Location = new System.Drawing.Point(437, 34);
            // 
            // simpleButton_Cancel
            // 
            this.simpleButton_Cancel.Location = new System.Drawing.Point(437, 77);
            // 
            // DefaultLookAndFeel1
            // 
            this.DefaultLookAndFeel1.LookAndFeel.SkinName = "McSkin";
            // 
            // LayoutControl1
            // 
            this.LayoutControl1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.LayoutControl1.Controls.Add(this.AppointmentTypeListControl1);
            this.LayoutControl1.Controls.Add(this.TextEdit_Description);
            this.LayoutControl1.Controls.Add(this.CheckEdit_activeflag);
            this.LayoutControl1.Controls.Add(this.CheckEdit_migrant_farm_worker);
            this.LayoutControl1.Controls.Add(this.AttributeListControl1);
            this.LayoutControl1.Controls.Add(this.CheckEdit_colonias);
            this.LayoutControl1.Controls.Add(this.CheckEdit_faith_based);
            this.LayoutControl1.Controls.Add(this.DateEdit_validation_date);
            this.LayoutControl1.Controls.Add(this.CheckEdit_default);
            this.LayoutControl1.Controls.Add(this.CalcEdit_counseling_amount);
            this.LayoutControl1.Controls.Add(this.TextEdit_password);
            this.LayoutControl1.Controls.Add(this.TextEdit_username);
            this.LayoutControl1.Controls.Add(this.TextEdit_hcs_id);
            this.LayoutControl1.Location = new System.Drawing.Point(13, 0);
            this.LayoutControl1.Name = "LayoutControl1";
            this.LayoutControl1.Root = this.LayoutControlGroup1;
            this.LayoutControl1.Size = new System.Drawing.Size(424, 348);
            this.LayoutControl1.TabIndex = 20;
            this.LayoutControl1.Text = "LayoutControl1";
            // 
            // AppointmentTypeListControl1
            // 
            this.AppointmentTypeListControl1.Location = new System.Drawing.Point(212, 108);
            this.AppointmentTypeListControl1.Name = "AppointmentTypeListControl1";
            this.AppointmentTypeListControl1.Size = new System.Drawing.Size(200, 180);
            this.AppointmentTypeListControl1.TabIndex = 15;
            // 
            // TextEdit_Description
            // 
            this.TextEdit_Description.Location = new System.Drawing.Point(87, 36);
            this.TextEdit_Description.Name = "TextEdit_Description";
            this.TextEdit_Description.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.False;
            this.TextEdit_Description.Properties.MaxLength = 50;
            this.TextEdit_Description.Properties.NullText = "A value is required";
            this.TextEdit_Description.Properties.NullValuePrompt = "A value is required";
            this.TextEdit_Description.Properties.NullValuePromptShowForEmptyValue = true;
            this.TextEdit_Description.Size = new System.Drawing.Size(325, 20);
            this.TextEdit_Description.StyleController = this.LayoutControl1;
            this.TextEdit_Description.TabIndex = 14;
            // 
            // CheckEdit_activeflag
            // 
            this.CheckEdit_activeflag.Location = new System.Drawing.Point(222, 316);
            this.CheckEdit_activeflag.Name = "CheckEdit_activeflag";
            this.CheckEdit_activeflag.Properties.Caption = "Active";
            this.CheckEdit_activeflag.Size = new System.Drawing.Size(190, 20);
            this.CheckEdit_activeflag.StyleController = this.LayoutControl1;
            this.CheckEdit_activeflag.TabIndex = 13;
            this.CheckEdit_activeflag.ToolTip = "Should we send this record to HUD?";
            // 
            // CheckEdit_migrant_farm_worker
            // 
            this.CheckEdit_migrant_farm_worker.Location = new System.Drawing.Point(222, 292);
            this.CheckEdit_migrant_farm_worker.Name = "CheckEdit_migrant_farm_worker";
            this.CheckEdit_migrant_farm_worker.Properties.Caption = "Migrant Farm Worker";
            this.CheckEdit_migrant_farm_worker.Size = new System.Drawing.Size(190, 20);
            this.CheckEdit_migrant_farm_worker.StyleController = this.LayoutControl1;
            this.CheckEdit_migrant_farm_worker.TabIndex = 11;
            this.CheckEdit_migrant_farm_worker.ToolTip = "Is this an office that counsels migrant farm workers?";
            // 
            // AttributeListControl1
            // 
            this.AttributeListControl1.Location = new System.Drawing.Point(12, 108);
            this.AttributeListControl1.Name = "AttributeListControl1";
            this.AttributeListControl1.Size = new System.Drawing.Size(196, 180);
            this.AttributeListControl1.TabIndex = 16;
            // 
            // CheckEdit_colonias
            // 
            this.CheckEdit_colonias.Location = new System.Drawing.Point(137, 292);
            this.CheckEdit_colonias.Name = "CheckEdit_colonias";
            this.CheckEdit_colonias.Properties.Caption = "Colonias";
            this.CheckEdit_colonias.Size = new System.Drawing.Size(81, 20);
            this.CheckEdit_colonias.StyleController = this.LayoutControl1;
            this.CheckEdit_colonias.TabIndex = 10;
            this.CheckEdit_colonias.ToolTip = "Are you along the mexican border and counsel people from Mexico?";
            // 
            // CheckEdit_faith_based
            // 
            this.CheckEdit_faith_based.Location = new System.Drawing.Point(12, 292);
            this.CheckEdit_faith_based.Name = "CheckEdit_faith_based";
            this.CheckEdit_faith_based.Properties.Caption = "Faith Based";
            this.CheckEdit_faith_based.Size = new System.Drawing.Size(121, 20);
            this.CheckEdit_faith_based.StyleController = this.LayoutControl1;
            this.CheckEdit_faith_based.TabIndex = 9;
            this.CheckEdit_faith_based.ToolTip = "Is this a religious organization?";
            // 
            // DateEdit_validation_date
            // 
            this.DateEdit_validation_date.EditValue = null;
            this.DateEdit_validation_date.Location = new System.Drawing.Point(87, 84);
            this.DateEdit_validation_date.Name = "DateEdit_validation_date";
            this.DateEdit_validation_date.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.DateEdit_validation_date.Properties.VistaTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.DateEdit_validation_date.Size = new System.Drawing.Size(96, 20);
            this.DateEdit_validation_date.StyleController = this.LayoutControl1;
            this.DateEdit_validation_date.TabIndex = 8;
            this.DateEdit_validation_date.ToolTip = "Next validation date";
            // 
            // CheckEdit_default
            // 
            this.CheckEdit_default.Location = new System.Drawing.Point(12, 316);
            this.CheckEdit_default.Name = "CheckEdit_default";
            this.CheckEdit_default.Properties.Caption = "Default";
            this.CheckEdit_default.Size = new System.Drawing.Size(206, 20);
            this.CheckEdit_default.StyleController = this.LayoutControl1;
            this.CheckEdit_default.TabIndex = 12;
            this.CheckEdit_default.ToolTip = "Default record when creating new offices and used when there is no other referenc" +
    "e in an office";
            // 
            // CalcEdit_counseling_amount
            // 
            this.CalcEdit_counseling_amount.Location = new System.Drawing.Point(262, 84);
            this.CalcEdit_counseling_amount.Name = "CalcEdit_counseling_amount";
            this.CalcEdit_counseling_amount.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.CalcEdit_counseling_amount.Properties.DisplayFormat.FormatString = "c";
            this.CalcEdit_counseling_amount.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.CalcEdit_counseling_amount.Properties.EditFormat.FormatString = "c";
            this.CalcEdit_counseling_amount.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.CalcEdit_counseling_amount.Properties.Mask.EditMask = "c";
            this.CalcEdit_counseling_amount.Properties.Precision = 2;
            this.CalcEdit_counseling_amount.Size = new System.Drawing.Size(150, 20);
            this.CalcEdit_counseling_amount.StyleController = this.LayoutControl1;
            this.CalcEdit_counseling_amount.TabIndex = 7;
            this.CalcEdit_counseling_amount.ToolTip = "Budget amount for counseling";
            // 
            // TextEdit_password
            // 
            this.TextEdit_password.Location = new System.Drawing.Point(262, 60);
            this.TextEdit_password.Name = "TextEdit_password";
            this.TextEdit_password.Properties.MaxLength = 255;
            this.TextEdit_password.Size = new System.Drawing.Size(150, 20);
            this.TextEdit_password.StyleController = this.LayoutControl1;
            this.TextEdit_password.TabIndex = 6;
            this.TextEdit_password.ToolTip = "Pasword when submitting entries via ARM";
            // 
            // TextEdit_username
            // 
            this.TextEdit_username.Location = new System.Drawing.Point(87, 60);
            this.TextEdit_username.Name = "TextEdit_username";
            this.TextEdit_username.Properties.MaxLength = 255;
            this.TextEdit_username.Size = new System.Drawing.Size(96, 20);
            this.TextEdit_username.StyleController = this.LayoutControl1;
            this.TextEdit_username.TabIndex = 5;
            this.TextEdit_username.ToolTip = "User name when submitting entries via ARM";
            // 
            // TextEdit_hcs_id
            // 
            this.TextEdit_hcs_id.Location = new System.Drawing.Point(87, 12);
            this.TextEdit_hcs_id.Name = "TextEdit_hcs_id";
            this.TextEdit_hcs_id.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.False;
            this.TextEdit_hcs_id.Properties.DisplayFormat.FormatString = "f0";
            this.TextEdit_hcs_id.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.TextEdit_hcs_id.Properties.EditFormat.FormatString = "f0";
            this.TextEdit_hcs_id.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.TextEdit_hcs_id.Properties.Mask.EditMask = "f0";
            this.TextEdit_hcs_id.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.TextEdit_hcs_id.Properties.NullText = "REQUIRED";
            this.TextEdit_hcs_id.Size = new System.Drawing.Size(325, 20);
            this.TextEdit_hcs_id.StyleController = this.LayoutControl1;
            this.TextEdit_hcs_id.TabIndex = 4;
            this.TextEdit_hcs_id.ToolTip = "ID associated with this entry";
            // 
            // LayoutControlGroup1
            // 
            this.LayoutControlGroup1.CustomizationFormText = "LayoutControlGroup1";
            this.LayoutControlGroup1.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.True;
            this.LayoutControlGroup1.GroupBordersVisible = false;
            this.LayoutControlGroup1.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.LayoutControlItem2,
            this.LayoutControlItem5,
            this.LayoutControlItem6,
            this.LayoutControlItem7,
            this.LayoutControlItem8,
            this.LayoutControlItem9,
            this.LayoutControlItem10,
            this.LayoutControlItem12,
            this.LayoutControlItem4,
            this.LayoutControlItem11,
            this.LayoutControlItem3,
            this.LayoutControlItem1,
            this.LayoutControlItem13});
            this.LayoutControlGroup1.Location = new System.Drawing.Point(0, 0);
            this.LayoutControlGroup1.Name = "LayoutControlGroup1";
            this.LayoutControlGroup1.Size = new System.Drawing.Size(424, 348);
            this.LayoutControlGroup1.Text = "LayoutControlGroup1";
            this.LayoutControlGroup1.TextVisible = false;
            // 
            // LayoutControlItem2
            // 
            this.LayoutControlItem2.Control = this.TextEdit_username;
            this.LayoutControlItem2.CustomizationFormText = "User Name";
            this.LayoutControlItem2.Location = new System.Drawing.Point(0, 48);
            this.LayoutControlItem2.Name = "LayoutControlItem2";
            this.LayoutControlItem2.Size = new System.Drawing.Size(175, 24);
            this.LayoutControlItem2.Text = "User Name";
            this.LayoutControlItem2.TextSize = new System.Drawing.Size(72, 13);
            // 
            // LayoutControlItem5
            // 
            this.LayoutControlItem5.Control = this.DateEdit_validation_date;
            this.LayoutControlItem5.CustomizationFormText = "Validation Date";
            this.LayoutControlItem5.Location = new System.Drawing.Point(0, 72);
            this.LayoutControlItem5.Name = "LayoutControlItem5";
            this.LayoutControlItem5.Size = new System.Drawing.Size(175, 24);
            this.LayoutControlItem5.Text = "Validation Date";
            this.LayoutControlItem5.TextSize = new System.Drawing.Size(72, 13);
            // 
            // LayoutControlItem6
            // 
            this.LayoutControlItem6.Control = this.CheckEdit_faith_based;
            this.LayoutControlItem6.CustomizationFormText = "LayoutControlItem6";
            this.LayoutControlItem6.Location = new System.Drawing.Point(0, 280);
            this.LayoutControlItem6.Name = "LayoutControlItem6";
            this.LayoutControlItem6.Size = new System.Drawing.Size(125, 24);
            this.LayoutControlItem6.Text = "LayoutControlItem6";
            this.LayoutControlItem6.TextSize = new System.Drawing.Size(0, 0);
            this.LayoutControlItem6.TextToControlDistance = 0;
            this.LayoutControlItem6.TextVisible = false;
            // 
            // LayoutControlItem7
            // 
            this.LayoutControlItem7.Control = this.CheckEdit_colonias;
            this.LayoutControlItem7.CustomizationFormText = "LayoutControlItem7";
            this.LayoutControlItem7.Location = new System.Drawing.Point(125, 280);
            this.LayoutControlItem7.Name = "LayoutControlItem7";
            this.LayoutControlItem7.Size = new System.Drawing.Size(85, 24);
            this.LayoutControlItem7.Text = "LayoutControlItem7";
            this.LayoutControlItem7.TextSize = new System.Drawing.Size(0, 0);
            this.LayoutControlItem7.TextToControlDistance = 0;
            this.LayoutControlItem7.TextVisible = false;
            // 
            // LayoutControlItem8
            // 
            this.LayoutControlItem8.Control = this.CheckEdit_migrant_farm_worker;
            this.LayoutControlItem8.CustomizationFormText = "LayoutControlItem8";
            this.LayoutControlItem8.Location = new System.Drawing.Point(210, 280);
            this.LayoutControlItem8.Name = "LayoutControlItem8";
            this.LayoutControlItem8.Size = new System.Drawing.Size(194, 24);
            this.LayoutControlItem8.Text = "LayoutControlItem8";
            this.LayoutControlItem8.TextSize = new System.Drawing.Size(0, 0);
            this.LayoutControlItem8.TextToControlDistance = 0;
            this.LayoutControlItem8.TextVisible = false;
            // 
            // LayoutControlItem9
            // 
            this.LayoutControlItem9.Control = this.CheckEdit_default;
            this.LayoutControlItem9.CustomizationFormText = "LayoutControlItem9";
            this.LayoutControlItem9.Location = new System.Drawing.Point(0, 304);
            this.LayoutControlItem9.Name = "LayoutControlItem9";
            this.LayoutControlItem9.Size = new System.Drawing.Size(210, 24);
            this.LayoutControlItem9.Text = "LayoutControlItem9";
            this.LayoutControlItem9.TextSize = new System.Drawing.Size(0, 0);
            this.LayoutControlItem9.TextToControlDistance = 0;
            this.LayoutControlItem9.TextVisible = false;
            // 
            // LayoutControlItem10
            // 
            this.LayoutControlItem10.Control = this.CheckEdit_activeflag;
            this.LayoutControlItem10.CustomizationFormText = "LayoutControlItem10";
            this.LayoutControlItem10.Location = new System.Drawing.Point(210, 304);
            this.LayoutControlItem10.Name = "LayoutControlItem10";
            this.LayoutControlItem10.Size = new System.Drawing.Size(194, 24);
            this.LayoutControlItem10.Text = "LayoutControlItem10";
            this.LayoutControlItem10.TextSize = new System.Drawing.Size(0, 0);
            this.LayoutControlItem10.TextToControlDistance = 0;
            this.LayoutControlItem10.TextVisible = false;
            // 
            // LayoutControlItem12
            // 
            this.LayoutControlItem12.Control = this.AppointmentTypeListControl1;
            this.LayoutControlItem12.CustomizationFormText = "Languages";
            this.LayoutControlItem12.Location = new System.Drawing.Point(200, 96);
            this.LayoutControlItem12.Name = "LayoutControlItem12";
            this.LayoutControlItem12.Size = new System.Drawing.Size(204, 184);
            this.LayoutControlItem12.Text = "Languages";
            this.LayoutControlItem12.TextSize = new System.Drawing.Size(0, 0);
            this.LayoutControlItem12.TextToControlDistance = 0;
            this.LayoutControlItem12.TextVisible = false;
            // 
            // LayoutControlItem4
            // 
            this.LayoutControlItem4.Control = this.CalcEdit_counseling_amount;
            this.LayoutControlItem4.CustomizationFormText = "Budget";
            this.LayoutControlItem4.Location = new System.Drawing.Point(175, 72);
            this.LayoutControlItem4.Name = "LayoutControlItem4";
            this.LayoutControlItem4.Size = new System.Drawing.Size(229, 24);
            this.LayoutControlItem4.Text = "Budget";
            this.LayoutControlItem4.TextSize = new System.Drawing.Size(72, 13);
            // 
            // LayoutControlItem11
            // 
            this.LayoutControlItem11.Control = this.TextEdit_Description;
            this.LayoutControlItem11.CustomizationFormText = "Description";
            this.LayoutControlItem11.Location = new System.Drawing.Point(0, 24);
            this.LayoutControlItem11.Name = "LayoutControlItem11";
            this.LayoutControlItem11.Size = new System.Drawing.Size(404, 24);
            this.LayoutControlItem11.Text = "Description";
            this.LayoutControlItem11.TextSize = new System.Drawing.Size(72, 13);
            // 
            // LayoutControlItem3
            // 
            this.LayoutControlItem3.Control = this.TextEdit_password;
            this.LayoutControlItem3.CustomizationFormText = "getPassword";
            this.LayoutControlItem3.Location = new System.Drawing.Point(175, 48);
            this.LayoutControlItem3.Name = "LayoutControlItem3";
            this.LayoutControlItem3.Size = new System.Drawing.Size(229, 24);
            this.LayoutControlItem3.Text = "getPassword";
            this.LayoutControlItem3.TextSize = new System.Drawing.Size(72, 13);
            // 
            // LayoutControlItem1
            // 
            this.LayoutControlItem1.Control = this.TextEdit_hcs_id;
            this.LayoutControlItem1.CustomizationFormText = "ID";
            this.LayoutControlItem1.Location = new System.Drawing.Point(0, 0);
            this.LayoutControlItem1.Name = "LayoutControlItem1";
            this.LayoutControlItem1.Size = new System.Drawing.Size(404, 24);
            this.LayoutControlItem1.Text = "ID";
            this.LayoutControlItem1.TextSize = new System.Drawing.Size(72, 13);
            // 
            // LayoutControlItem13
            // 
            this.LayoutControlItem13.Control = this.AttributeListControl1;
            this.LayoutControlItem13.CustomizationFormText = "LayoutControlItem13";
            this.LayoutControlItem13.Location = new System.Drawing.Point(0, 96);
            this.LayoutControlItem13.Name = "LayoutControlItem13";
            this.LayoutControlItem13.Size = new System.Drawing.Size(200, 184);
            this.LayoutControlItem13.Text = "LayoutControlItem13";
            this.LayoutControlItem13.TextSize = new System.Drawing.Size(0, 0);
            this.LayoutControlItem13.TextToControlDistance = 0;
            this.LayoutControlItem13.TextVisible = false;
            // 
            // HCSIDEditForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(533, 346);
            this.Controls.Add(this.LayoutControl1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.Sizable;
            this.Name = "HCSIDEditForm";
            this.Text = "HCS ID Record";
            this.Controls.SetChildIndex(this.LayoutControl1, 0);
            this.Controls.SetChildIndex(this.simpleButton_OK, 0);
            this.Controls.SetChildIndex(this.simpleButton_Cancel, 0);
            ((System.ComponentModel.ISupportInitialize)(this.DxErrorProvider1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LayoutControl1)).EndInit();
            this.LayoutControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.TextEdit_Description.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.CheckEdit_activeflag.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.CheckEdit_migrant_farm_worker.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.CheckEdit_colonias.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.CheckEdit_faith_based.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DateEdit_validation_date.Properties.VistaTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DateEdit_validation_date.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.CheckEdit_default.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.CalcEdit_counseling_amount.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TextEdit_password.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TextEdit_username.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TextEdit_hcs_id.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LayoutControlGroup1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LayoutControlItem2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LayoutControlItem5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LayoutControlItem6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LayoutControlItem7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LayoutControlItem8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LayoutControlItem9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LayoutControlItem10)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LayoutControlItem12)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LayoutControlItem4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LayoutControlItem11)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LayoutControlItem3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LayoutControlItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LayoutControlItem13)).EndInit();
            this.ResumeLayout(false);

		}
		private DevExpress.XtraLayout.LayoutControl LayoutControl1;
		private DevExpress.XtraLayout.LayoutControlGroup LayoutControlGroup1;
		private DevExpress.XtraEditors.CheckEdit CheckEdit_activeflag;
		private DevExpress.XtraEditors.CheckEdit CheckEdit_default;
		private DevExpress.XtraEditors.CheckEdit CheckEdit_migrant_farm_worker;
		private DevExpress.XtraEditors.CheckEdit CheckEdit_colonias;
		private DevExpress.XtraEditors.CheckEdit CheckEdit_faith_based;
		private DevExpress.XtraEditors.DateEdit DateEdit_validation_date;
		private DevExpress.XtraEditors.CalcEdit CalcEdit_counseling_amount;
		private DevExpress.XtraEditors.TextEdit TextEdit_password;
		private DevExpress.XtraEditors.TextEdit TextEdit_username;
		private DevExpress.XtraEditors.TextEdit TextEdit_hcs_id;
		private DevExpress.XtraLayout.LayoutControlItem LayoutControlItem1;
		private DevExpress.XtraLayout.LayoutControlItem LayoutControlItem2;
		private DevExpress.XtraLayout.LayoutControlItem LayoutControlItem3;
		private DevExpress.XtraLayout.LayoutControlItem LayoutControlItem4;
		private DevExpress.XtraLayout.LayoutControlItem LayoutControlItem5;
		private DevExpress.XtraLayout.LayoutControlItem LayoutControlItem6;
		private DevExpress.XtraLayout.LayoutControlItem LayoutControlItem7;
		private DevExpress.XtraLayout.LayoutControlItem LayoutControlItem8;
		private DevExpress.XtraLayout.LayoutControlItem LayoutControlItem9;
		private DevExpress.XtraLayout.LayoutControlItem LayoutControlItem10;
		private DevExpress.XtraEditors.TextEdit TextEdit_Description;
		private DevExpress.XtraLayout.LayoutControlItem LayoutControlItem11;
		private AppointmentTypeListControl AppointmentTypeListControl1;
		private DevExpress.XtraLayout.LayoutControlItem LayoutControlItem12;
		private AttributeListControl AttributeListControl1;
		private DevExpress.XtraLayout.LayoutControlItem LayoutControlItem13;
	}
}
