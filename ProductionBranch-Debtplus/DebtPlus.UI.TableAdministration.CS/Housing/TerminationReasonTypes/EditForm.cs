using System;
using System.Linq;
using DebtPlus.LINQ;

namespace DebtPlus.UI.TableAdministration.CS.Housing.TerminationReasonTypes
{
    internal partial class EditForm : Templates.EditTemplateForm
    {
        private Housing_TerminationReasonType record = null;
        private BusinessContext bc = null;

        internal EditForm()
            : base()
        {
            InitializeComponent();
        }

        internal EditForm(BusinessContext bc, Housing_TerminationReasonType record)
            : this()
        {
            this.bc = bc;
            this.record = record;
            RegisterHandlers();
        }

        private void RegisterHandlers()
        {
            this.Load += EditForm_Load;
            TextEdit_description.EditValueChanging += Form_Changing;
        }

        private void UnRegisterHandlers()
        {
            this.Load -= EditForm_Load;
            TextEdit_description.EditValueChanging -= Form_Changing;
        }

        private void EditForm_Load(object sender, EventArgs e)
        {
            UnRegisterHandlers();
            try
            {
                LookupEdit_hud_9902_section.Properties.DataSource = bc.Housing_ARM_referenceInfos.Where(s => s.groupId == 11).ToList();
                LookupEdit_hud_9902_section.EditValue = record.hud_9902_section;

                LabelControl_ID.Text = record.Id <= 0 ? "NEW" : record.Id.ToString();

                TextEdit_description.EditValue = record.description;
                textEdit_hpf.EditValue = record.hpf;
                CheckEdit_default.Checked = record.Default;
                CheckEdit_ActiveFlag.Checked = record.ActiveFlag;
            }
            finally
            {
                RegisterHandlers();
            }
            simpleButton_OK.Enabled = !string.IsNullOrEmpty((string)TextEdit_description.EditValue);
        }

        private void Form_Changing(object sender, DevExpress.XtraEditors.Controls.ChangingEventArgs e)
        {
            simpleButton_OK.Enabled = !string.IsNullOrEmpty((string)e.NewValue);
        }

        protected override void simpleButton_OK_Click(object sender, EventArgs e)
        {
            record.hud_9902_section = DebtPlus.Utils.Nulls.v_String(LookupEdit_hud_9902_section.EditValue);
            record.description = DebtPlus.Utils.Nulls.v_String(TextEdit_description.EditValue);
            record.hpf = DebtPlus.Utils.Nulls.v_String(textEdit_hpf.EditValue);
            record.Default = CheckEdit_default.Checked;
            record.ActiveFlag = CheckEdit_ActiveFlag.Checked;

            base.simpleButton_OK_Click(sender, e);
        }
    }
}