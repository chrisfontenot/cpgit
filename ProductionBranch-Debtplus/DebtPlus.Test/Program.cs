﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DebtPlus.LINQ;

namespace DebtPlus.Test
{
    class Program
    {
        /// <summary>
        /// Encrypt the password with MD5 encryption that is used in the client_www record.
        /// </summary>
        /// <param name="Password">Input password string</param>
        /// <returns></returns>
        public static string GetMD5Hash(string Password)
        {
            // Do not allow for blank passwords.
            if (string.IsNullOrWhiteSpace(Password))
            {
                return string.Empty;
            }

            // Encrypt the password for the entry
            var TempSource = System.Text.ASCIIEncoding.ASCII.GetBytes(Password.ToLower());

            // Next, encode the password stream
            var MD5_Class = new System.Security.Cryptography.MD5CryptoServiceProvider();
            var TempHash = MD5_Class.ComputeHash(TempSource);

            // Finally, merge the byte stream to a string
            var sbPasswd = new System.Text.StringBuilder();
            for (Int32 index = 0; index <= TempHash.GetUpperBound(0); ++index)
            {
                sbPasswd.Append(TempHash[index].ToString("X2"));
            }

            // The encoded password is the combined string
            return sbPasswd.ToString().ToUpper();
        }

        static void Main(string[] args)
        {
            System.Collections.Generic.List<client_www> colRecords = null;
            using (var bc = new BusinessContext())
            {
                colRecords = bc.client_wwws.ToList();
                foreach (var record in colRecords)
                {
                    if (System.Text.RegularExpressions.Regex.IsMatch("^[A-Fa-f0-9]{32}$", record.PasswordAnswer))
                    {
                        record.PasswordAnswer = record.PasswordAnswer.ToUpper();
                        continue;
                    }
                    record.PasswordAnswer = GetMD5Hash(record.PasswordAnswer);
                }
                bc.SubmitChanges();
            }
            return;

#if false
            //var nationStarTest = new NationStarTest();
            //nationStarTest.Setup();
            //nationStarTest.Execute();
            //nationStarTest.Cleanup();
            var eiTest = new EITest();
            eiTest.Setup();
            eiTest.Execute();
            eiTest.Cleanup();

            Console.WriteLine("\n\nPress ENTER to exit...");
            Console.ReadLine();
#endif
        }
    }
}
