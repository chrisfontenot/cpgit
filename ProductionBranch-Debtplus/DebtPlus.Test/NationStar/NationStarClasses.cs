﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DebtPlus.Test
{
    public class NationStarClient
    {
        public string ApplicantFirstName { get; set; }
        public string ApplicantLastName { get; set; }
        public string HomePhone { get; set; }
        public string MessagePhone { get; set; }
        public string ApplicantCell { get; set; }
        public string ApplicantWorkPhone { get; set; }
        public string CoapplicantFirstName { get; set; }
        public string CoApplicantLastName { get; set; }
        public string CoapplicantCell { get; set; }
        public string CoapplicantWorkPhone { get; set; }
        public string Addressline { get; set; }
        public string Street { get; set; }
        public string City { get; set; }
        public string State { get; set; }
        public string LoanNumber { get; set; }
        public int? NationStarReferralCode { get; set; }
        public int? TrialModification { get; set; }

        public int? ApplicantNameID { get; set; }
        public int? CoapplicantNameID { get; set; }
        public int? HomePhoneID { get; set; }
        public int? ApplicantCellID { get; set; }
        public int? CoapplicantCellID { get; set; }
        public int? MessagePhoneID { get; set; }
        public int? ApplicantWorkPhoneID { get; set; }
        public int? CoapplicantWorkPhoneID { get; set; }
        public int? AddressID { get; set; }

        public int ClientID { get; set; }

        public int? ApplicantPersonID { get; set; }
        public int? CoapplicantPersonID { get; set; }
    }

    public enum QueueType
    {
        HTQ,
        COQ
    }

    public class Trigger
    {
        public string ActiveFlag { get; set; }
        public string OcsStatus { get; set; }
        public QueueType Queue { get; set; }
        public string LastChangeFlag { get; set; }
        public string LastChangeDate { get; set; }
        public string ContactAttempts { get; set; }
    }

    public class TriggerResult
    {
        public string ActiveFlag { get; set; }
        public string OcsStatus { get; set; }
        public string Queue { get; set; }
        public string LastChangeFlag { get; set; }
        public string LastChangeDate { get; set; }
        public string ContactAttempts { get; set; }
    }

    public class Scenario
    {
        public string Name { get; set; }

        public Trigger ExclusionReport { get; set; }
        public Trigger NotInterested { get; set; }
        public Trigger Counsel { get; set; }
        public Trigger ImmediateCounseling { get; set; }
        public Trigger SecondCounsel { get; set; }
        public Trigger ScheduleAppointment { get; set; }
        public Trigger TransferredToCounselor { get; set; }
        public Trigger SpokeToClient { get; set; }
        public Trigger InboundVoicemail { get; set; }
        public Trigger NoContact { get; set; }
        public Trigger NoShow { get; set; }
        public Trigger LeftMessage { get; set; }
        public Trigger Skip { get; set; }
        public Trigger BadNumber { get; set; }

        public Trigger CounselorToCallBack { get; set; }
        public Trigger CCRToCallBack { get; set; }
        public Trigger Deceased { get; set; }
        public Trigger WorkingWithLender { get; set; }
    }

    public class ScenarioResult
    {
        public string Name { get; set; }

        public TriggerResult ExclusionReport { get; set; }
        public TriggerResult NotInterested { get; set; }
        public TriggerResult Counsel { get; set; }
        public TriggerResult ImmediateCounseling { get; set; }
        public TriggerResult SecondCounsel { get; set; }
        public TriggerResult ScheduleAppointment { get; set; }
        public TriggerResult TransferredToCounselor { get; set; }
        public TriggerResult SpokeToClient { get; set; }
        public TriggerResult InboundVoicemail { get; set; }
        public TriggerResult NoContact { get; set; }
        public TriggerResult NoShow { get; set; }
        public TriggerResult LeftMessage { get; set; }
        public TriggerResult Skip { get; set; }
        public TriggerResult BadNumber { get; set; }

        public TriggerResult CounselorToCallBack { get; set; }
        public TriggerResult CCRToCallBack { get; set; }
        public TriggerResult Deceased { get; set; }
        public TriggerResult WorkingWithLender { get; set; }
    }
}
