﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using DebtPlus.LINQ;
using DebtPlus.OCS.Domain;
using DebtPlus.Svc.OCS.Rules.FMACEI;
using Excel;

namespace DebtPlus.Test
{
    public class NationStarTest : ITest
    {
        public enum TriggerOrder
        {
            ExclusionReport_EX = 1,
            NotInterested_NI = 2,
            Counsel_CO = 3,
            ImmediateCounseling_IM = 4,
            SecondCounsel_ST = 5,
            ScheduleAppointment_SA = 6,
            TransferredCallToCounselor_TR = 7,
            RPSpoketoClient_RP = 8,
            InboundVoicemailMsg_IV = 9,
            NoContact_NC = 10,
            NoShow_NS = 11,
            LeftMessage_LM = 12,
            Skip_SK = 13,
            BadNumber_BN = 14
        }

        public enum ScenarioOrder
        {
            Untouched = 1,
            InitialContact_C1 = 2,
            InitialContact_C2 = 3,
            InitialContact_C3 = 4,
            InitialContact_C4 = 5,
            AdditionalAttempt_OT = 6,
            PaymentReminder_P1 = 7,
            PaymentReminder_P2 = 8,
            PaymentReminder_P3 = 9,
            PaymentReminder_P4 = 10,
            PaymentReminder_P5 = 11,
            PaymentReminder_P6 = 12,
            ClientNotInterested_NI = 13,
            BadNumber_BN = 14,
            Pending_PN = 15
        }

        private void DatabaseSetup()
        {
            using (DebtPlus.LINQ.SQLInfoClass sql = DebtPlus.LINQ.SQLInfoClass.getDefault())
            {
                //sql.ConnectionString = sql.ConnectionString.Replace("(local)\\SQLEXPRESS", "10.0.0.97");
                Repository.Setup.ConnectionString = sql.ConnectionString.Replace("(local)\\SQLEXPRESS", "10.0.0.97");
                Repository.Setup.CommandTimeout = sql.CommandTimeout;
            }
        }

        private List<NationStarClient> clients = new List<NationStarClient>();
        private List<Scenario> scenarios = new List<Scenario>();
        private List<ScenarioResult> scenarioResults = new List<ScenarioResult>();

        #region "SET UP"

        public void Setup()
        {
            var filename = "NationStarClients.xlsx";
            DatabaseSetup();
            ReadSetupData(filename);
        }

        private void ReadSetupData(string filename)
        {
            var path = System.IO.Path.Combine(
                System.IO.Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().Location),
                "TestSpreadsheets",
                filename);

            using (var stream = new System.IO.FileStream(path, System.IO.FileMode.Open))
            {
                IExcelDataReader reader = Excel.ExcelReaderFactory.CreateOpenXmlReader(stream);
                reader.IsFirstRowAsColumnNames = true;

                var ds = reader.AsDataSet();
                var clientsSheet = ds.Tables[0];//for now, we are only handling worksheet 1
                var columns = clientsSheet.Columns;

                clients.Clear();

                foreach (System.Data.DataRow row in clientsSheet.Rows)
                {
                    var client = new NationStarClient();
                    foreach (var col in columns)
                    {
                        switch (col.ToString())
                        {
                            case "ApplicantFirstName":
                                client.ApplicantFirstName = row[col.ToString()] as string;
                                break;

                            case "ApplicantLastName":
                                client.ApplicantLastName = row[col.ToString()] as string;
                                break;

                            case "HomePhone":
                                client.HomePhone = row[col.ToString()] as string;
                                break;

                            case "MessagePhone":
                                client.MessagePhone = row[col.ToString()] as string;
                                break;

                            case "ApplicantCell":
                                client.ApplicantCell = row[col.ToString()] as string;
                                break;

                            case "ApplicantWorkPhone":
                                client.ApplicantWorkPhone = row[col.ToString()] as string;
                                break;

                            case "CoapplicantFirstName":
                                client.CoapplicantFirstName = row[col.ToString()] as string;
                                break;

                            case "CoApplicantLastName":
                                client.CoApplicantLastName = row[col.ToString()] as string;
                                break;

                            case "CoapplicantCell":
                                client.CoapplicantCell = row[col.ToString()] as string;
                                break;

                            case "CoapplicantWorkPhone":
                                client.CoapplicantWorkPhone = row[col.ToString()] as string;
                                break;

                            case "Addressline":
                                client.Addressline = row[col.ToString()] as string;
                                break;

                            case "Street":
                                client.Street = row[col.ToString()] as string;
                                break;

                            case "City":
                                client.City = row[col.ToString()] as string;
                                break;

                            case "State":
                                client.State = row[col.ToString()] as string;
                                break;

                            case "LoanNumber":
                                client.LoanNumber = row[col.ToString()] as string;
                                break;

                            case "NationStarReferralCode":
                                client.NationStarReferralCode = (row[col.ToString()] != null) ? row[col.ToString()] as int? : null;
                                break;

                            case "TrialModification":
                                client.TrialModification = (row[col.ToString()] != null) ? row[col.ToString()] as int? : null;
                                break;
                        }
                    }
                    clients.Add(client);
                }

                CreateClients();

                if (ds.Tables[1] != null)
                {
                    ReadTestConfiguration(ds.Tables[1]);
                }
            }
        }

        private void ReadTestConfiguration(System.Data.DataTable dataTable)
        {
            // Read Untouched Scenario
            for (int i = 0; i < dataTable.Rows.Count; i = i + 8)
            {
                var rows = dataTable.Rows;
                var scenario = new Scenario();
                scenario.Name = rows[i][0].ToString();

                scenario.ExclusionReport = GetTrigger(rows, i, (int)TriggerOrder.ExclusionReport_EX);
                scenario.NotInterested = GetTrigger(rows, i, (int)TriggerOrder.NotInterested_NI);
                scenario.Counsel = GetTrigger(rows, i, (int)TriggerOrder.Counsel_CO);
                scenario.ImmediateCounseling = GetTrigger(rows, i, (int)TriggerOrder.ImmediateCounseling_IM);
                scenario.SecondCounsel = GetTrigger(rows, i, (int)TriggerOrder.SecondCounsel_ST);
                scenario.ScheduleAppointment = GetTrigger(rows, i, (int)TriggerOrder.ScheduleAppointment_SA);
                scenario.TransferredToCounselor = GetTrigger(rows, i, (int)TriggerOrder.TransferredCallToCounselor_TR);
                scenario.SpokeToClient = GetTrigger(rows, i, (int)TriggerOrder.RPSpoketoClient_RP);
                scenario.InboundVoicemail = GetTrigger(rows, i, (int)TriggerOrder.InboundVoicemailMsg_IV);
                scenario.NoContact = GetTrigger(rows, i, (int)TriggerOrder.NoContact_NC);
                scenario.NoShow = GetTrigger(rows, i, (int)TriggerOrder.NoShow_NS);
                scenario.LeftMessage = GetTrigger(rows, i, (int)TriggerOrder.LeftMessage_LM);
                scenario.Skip = GetTrigger(rows, i, (int)TriggerOrder.Skip_SK);
                scenario.BadNumber = GetTrigger(rows, i, (int)TriggerOrder.BadNumber_BN);

                scenarios.Add(scenario);
            }
        }

        private Trigger GetTrigger(DataRowCollection rows, int baseIndex, int triggerIndex)
        {
            const int ActiveFlagIndex = 1;
            const int OcsFlagIndex = 2;
            const int QueueIndex = 3;
            const int LastChanceFlagIndex = 4;
            const int LastChangeDateIndex = 5;
            const int ContactAttemptsIndex = 7;

            var trigger = new Trigger
            {
                ActiveFlag = rows[baseIndex + ActiveFlagIndex][triggerIndex].ToString(),
                OcsStatus = rows[baseIndex + OcsFlagIndex][triggerIndex].ToString(),
                Queue = (rows[baseIndex + QueueIndex][triggerIndex].ToString().Trim() == "COQ") ? QueueType.COQ : QueueType.HTQ,
                LastChangeFlag = rows[baseIndex + LastChanceFlagIndex][triggerIndex].ToString(),
                LastChangeDate = rows[baseIndex + LastChangeDateIndex][triggerIndex].ToString(),
                ContactAttempts = rows[baseIndex + ContactAttemptsIndex][triggerIndex].ToString(),
            };
            return trigger;
        }

        private void CreateClients()
        {
            using (var bc = new BusinessContext())
            {
                foreach (var c in clients)
                {
                    c.ApplicantNameID = InsertClientName(bc, c.ApplicantFirstName, c.ApplicantLastName);
                    c.CoapplicantNameID = InsertClientName(bc, c.CoapplicantFirstName, c.CoApplicantLastName);

                    c.HomePhoneID = InsertPhone(bc, c.HomePhone);
                    c.ApplicantCellID = InsertPhone(bc, c.ApplicantCell);
                    c.ApplicantWorkPhoneID = InsertPhone(bc, c.ApplicantWorkPhone);
                    c.CoapplicantCellID = InsertPhone(bc, c.CoapplicantCell);
                    c.CoapplicantWorkPhoneID = InsertPhone(bc, c.CoapplicantWorkPhone);
                    c.MessagePhoneID = InsertPhone(bc, c.MessagePhone);
                    c.AddressID = InsertAddress(bc, c.Addressline, c.Street, c.City, c.State);

                    //c.ClientID =  bc.xpr_create_client(c.HomePhoneID);

                    //c.ApplicantPersonID = InsertPerson(bc, c.ClientID, 1, c.ApplicantNameID, c.ApplicantCellID, c.ApplicantWorkPhoneID);
                    //c.CoapplicantPersonID = InsertPerson(bc, c.ClientID, 2, c.CoapplicantNameID, c.CoapplicantCellID, c.CoapplicantWorkPhoneID);

                    //var result = bc.xpr_create_nationstar_client(c.NationStarReferralCode
                    //    , c.LoanNumber, c.TrialModification, c.ApplicantNameID, c.CoapplicantNameID, c.HomePhoneID, c.ApplicantCellID
                    //    , c.CoapplicantCellID, c.MessagePhoneID, c.ApplicantWorkPhoneID, c.CoapplicantWorkPhoneID, c.AddressID, defaultMethodOfContact);
                    //var list = result.ToList();

                    //c.ClientID = (int)list[0].ClientID;

                    if (c.ClientID > 0)
                    {
                        var client = bc.clients.Where(cl => cl.Id == c.ClientID).FirstOrDefault();

                        if (client != null)
                        {
                            client.AddressID = c.AddressID;
                            client.MsgTelephoneID = c.MessagePhoneID;

                            bc.SubmitChanges();
                        }
                    }
                }
            }
        }

        private int? InsertPerson(BusinessContext bc, int clientID, int type, int? nameID, int? cellPhoneID, int? workPhoneID)
        {
            if (clientID <= 0)
            {
                return null;
            }

            var person = DebtPlus.LINQ.Factory.Manufacture_people(clientID);
            person.Relation = type;
            person.CellTelephoneID = cellPhoneID;
            person.NameID = nameID;
            person.WorkTelephoneID = workPhoneID;

            bc.peoples.InsertOnSubmit(person);
            bc.SubmitChanges();

            return person.Id;
        }

        private int? InsertAddress(BusinessContext bc, string addressline, string street, string city, string state)
        {
            if (string.IsNullOrWhiteSpace(addressline) || string.IsNullOrWhiteSpace(street) || string.IsNullOrWhiteSpace(city) || string.IsNullOrWhiteSpace(state))
            {
                return null;
            }

            var addr = DebtPlus.LINQ.Factory.Manufacture_address();

            if (!string.IsNullOrWhiteSpace(addressline))
            {
                addr.address_line_2 = addressline;
            }

            if (!string.IsNullOrWhiteSpace(street))
            {
                addr.street = street;
            }

            if (!string.IsNullOrWhiteSpace(city))
            {
                addr.city = city;
            }

            if (!string.IsNullOrWhiteSpace(state))
            {
                addr.state = int.Parse(state);
            }

            bc.addresses.InsertOnSubmit(addr);
            bc.SubmitChanges();

            return addr.Id;
        }

        private int? InsertPhone(BusinessContext bc, string phoneString)
        {
            var phone = DebtPlus.LINQ.TelephoneNumber.TryParse(phoneString);
            if (phone == null)
            {
                return null;
            }

            bc.TelephoneNumbers.InsertOnSubmit(phone);
            bc.SubmitChanges();

            return phone.Id;
        }

        private int? InsertClientName(BusinessContext bc, string firstName, string lastName)
        {
            if (string.IsNullOrWhiteSpace(firstName) && string.IsNullOrWhiteSpace(lastName))
            {
                return null;
            }

            var name = DebtPlus.LINQ.Factory.Manufacture_Name();
            name.First = firstName;
            name.Last = lastName;

            bc.Names.InsertOnSubmit(name);
            return name.Id;
        }

        #endregion "SET UP"


        #region "EXECUTE"

        public void Execute()
        {
            scenarioResults.Clear();

            ExecuteScenario(ScenarioOrder.Untouched);

            // Reset Status to InitialContact1
            ResetClientStatus(ScenarioOrder.InitialContact_C1);
            ExecuteScenario(ScenarioOrder.InitialContact_C1);

            // Reset Status to InitialContact2
            ResetClientStatus(ScenarioOrder.InitialContact_C2);
            ExecuteScenario(ScenarioOrder.InitialContact_C2);

            // Reset Status to InitialContact3
            ResetClientStatus(ScenarioOrder.InitialContact_C3);
            ExecuteScenario(ScenarioOrder.InitialContact_C3);

            // Reset Status to InitialContact4
            ResetClientStatus(ScenarioOrder.InitialContact_C4);
            ExecuteScenario(ScenarioOrder.InitialContact_C4);

            // Reset Status to AdditionalAttempt
            ResetClientStatus(ScenarioOrder.AdditionalAttempt_OT);
            ExecuteScenario(ScenarioOrder.AdditionalAttempt_OT);

            // Reset Status to PaymentReminder_P1
            ResetClientStatus(ScenarioOrder.PaymentReminder_P1);
            ExecuteScenario(ScenarioOrder.PaymentReminder_P1);

            // Reset Status to PaymentReminder_P2
            ResetClientStatus(ScenarioOrder.PaymentReminder_P2);
            ExecuteScenario(ScenarioOrder.PaymentReminder_P2);

            // Reset Status to PaymentReminder_P3
            ResetClientStatus(ScenarioOrder.PaymentReminder_P3);
            ExecuteScenario(ScenarioOrder.PaymentReminder_P3);

            // Reset Status to PaymentReminder_P4
            ResetClientStatus(ScenarioOrder.PaymentReminder_P4);
            ExecuteScenario(ScenarioOrder.PaymentReminder_P4);

            // Reset Status to PaymentReminder_P5
            ResetClientStatus(ScenarioOrder.PaymentReminder_P5);
            ExecuteScenario(ScenarioOrder.PaymentReminder_P5);

            // Reset Status to PaymentReminder_P5
            ResetClientStatus(ScenarioOrder.PaymentReminder_P6);
            ExecuteScenario(ScenarioOrder.PaymentReminder_P6);

            // Reset Status to ClientNotInterested_NI
            ResetClientStatus(ScenarioOrder.ClientNotInterested_NI);
            ExecuteScenario(ScenarioOrder.ClientNotInterested_NI);

            // Reset Status to BadNumber_BN
            ResetClientStatus(ScenarioOrder.BadNumber_BN);
            ExecuteScenario(ScenarioOrder.BadNumber_BN);

            // Reset Status to Pending_PN
            ResetClientStatus(ScenarioOrder.Pending_PN);
            ExecuteScenario(ScenarioOrder.Pending_PN);
        }

        private void ExecuteScenario(ScenarioOrder scenarioOrder)
        {
            using (var bc = new BusinessContext())
            {
                var scenario = scenarios[(int)scenarioOrder - 1];

                var scenarioResult = new ScenarioResult();
                scenarioResult.Name = scenario.Name;

                Trigger trigger = null;
                for (int i = 0; i < clients.Count; i++)
                {
                    var triggerResult = new TriggerResult();

                    var cl = bc.OCS_Clients.Where(ocs => ocs.ClientId == clients[i].ClientID).FirstOrDefault();

                    var ocsClient = new OCSClient();
                    ocsClient.CurrentAttempt = new ContactAttempt();
                    //ocsClient.CurrentAttempt.StartedAttempt(loadedClient.CurrentAttempt.Begin);
                    //ocsClient.CurrentAttempt.EndedAttempt(loadedClient.CurrentAttempt.End);
                    ocsClient.CurrentAttempt.Outcomes = new List<ContactAttempt.CallOutcome>();
                    ocsClient.CurrentAttempt.Outcomes.Add(new ContactAttempt.CallOutcome()
                    {
                        PhoneNumber = new TelephoneNumber(1, "111", "2222222", ""),
                    });

                    switch (i)
                    {
                        case 0:
                            ocsClient.CurrentAttempt.Outcomes[0].Result = RESULT_CODE.ExclusionReport;
                            trigger = scenario.ExclusionReport;
                            scenarioResult.ExclusionReport = triggerResult;
                            break;

                        case 1:
                            ocsClient.CurrentAttempt.Outcomes[0].Result = RESULT_CODE.ClientNotInterested;
                            trigger = scenario.NotInterested;
                            scenarioResult.NotInterested = triggerResult;
                            break;

                        case 2:
                            ocsClient.CurrentAttempt.Outcomes[0].Result = RESULT_CODE.Counseled;
                            trigger = scenario.Counsel;
                            scenarioResult.Counsel = triggerResult;
                            break;

                        case 3:
                            ocsClient.CurrentAttempt.Outcomes[0].Result = RESULT_CODE.ImmediateCounseling;
                            trigger = scenario.ImmediateCounseling;
                            scenarioResult.ImmediateCounseling = triggerResult;
                            break;

                        case 4:
                            ocsClient.CurrentAttempt.Outcomes[0].Result = RESULT_CODE.SecondCounsel;
                            trigger = scenario.SecondCounsel;
                            scenarioResult.SecondCounsel = triggerResult;
                            break;

                        case 5:
                            ocsClient.CurrentAttempt.Outcomes[0].Result = RESULT_CODE.ScheduledAppointment;
                            trigger = scenario.ScheduleAppointment;
                            scenarioResult.ScheduleAppointment = triggerResult;
                            break;

                        case 6:
                            ocsClient.CurrentAttempt.Outcomes[0].Result = RESULT_CODE.TransferredToCounselor;
                            trigger = scenario.TransferredToCounselor;
                            scenarioResult.TransferredToCounselor = triggerResult;
                            break;

                        case 7:
                            ocsClient.CurrentAttempt.Outcomes[0].Result = RESULT_CODE.RightParty;
                            trigger = scenario.SpokeToClient;
                            scenarioResult.SpokeToClient = triggerResult;
                            break;

                        case 8:
                            ocsClient.CurrentAttempt.Outcomes[0].Result = RESULT_CODE.InboundVoicemail;
                            trigger = scenario.InboundVoicemail;
                            scenarioResult.InboundVoicemail = triggerResult;
                            break;

                        case 9:
                            ocsClient.CurrentAttempt.Outcomes[0].Result = RESULT_CODE.NoContact;
                            trigger = scenario.NoContact;
                            scenarioResult.NoContact = triggerResult;
                            break;

                        case 10:
                            ocsClient.CurrentAttempt.Outcomes[0].Result = RESULT_CODE.NoShow;
                            trigger = scenario.NoShow;
                            scenarioResult.NoShow = triggerResult;
                            break;

                        case 11:
                            ocsClient.CurrentAttempt.Outcomes[0].Result = RESULT_CODE.LeftMessage;
                            trigger = scenario.LeftMessage;
                            scenarioResult.LeftMessage = triggerResult;
                            break;

                        case 12:
                            ocsClient.CurrentAttempt.Outcomes[0].Result = RESULT_CODE.Skip;
                            trigger = scenario.Skip;
                            scenarioResult.Skip = triggerResult;
                            break;

                        case 13:
                            ocsClient.CurrentAttempt.Outcomes[0].Result = RESULT_CODE.BadNumber;
                            trigger = scenario.BadNumber;
                            scenarioResult.BadNumber = triggerResult;
                            break;

                        default:
                            break;
                    }

                    /*if (ocsClient.CurrentAttempt == null)
                    {
                        ocsClient.StartContactAttempt();
                    }*/

                    if (cl != null)
                    {
                        bool oldActiveFlag = cl.ActiveFlag;
                        string queue = cl.QueueCode;
                        int oldStatus = cl.StatusCode;
                        int oldCurrentAttemptCount = 0;
                        ocsClient.Queue = OCS_QUEUE.HighTouch;
                        oldCurrentAttemptCount = ocsClient.ContactAttemptCount;

                        var result = StateMachine.UpdateOnTrigger(OCS.Domain.TRIGGER.NoTrigger, OCS.Domain.STATUS_CODE.NewRecord, ocsClient);

                        if (i == 12)
                        {
                            if (result.ActiveFlag == cl.ActiveFlag)
                            {
                                triggerResult.ActiveFlag = "Y";
                            }
                            else
                            {
                                triggerResult.ActiveFlag = "N";
                            }

                            if ((int)result.StatusCode == cl.StatusCode)
                            {
                                triggerResult.OcsStatus = "Y";
                            }
                            else
                            {
                                triggerResult.OcsStatus = "N";
                            }

                            if (result.Queue.ToString() == cl.QueueCode)
                            {
                                triggerResult.Queue = "Y";
                            }
                            else
                            {
                                triggerResult.Queue = "N";
                            }
                        }
                        else
                        {
                            var activeFlag = (trigger.ActiveFlag == "I" ? false : true);
                            if (result.ActiveFlag == activeFlag)
                            {
                                triggerResult.ActiveFlag = "Y";
                            }
                            else
                            {
                                triggerResult.ActiveFlag = "N";
                            }

                            // Transferred To Counselor
                            if (i == 6)
                            {
                                if (oldStatus == 0 && (int)result.StatusCode == 21)
                                {
                                    triggerResult.OcsStatus = "Y";
                                }
                                else
                                {
                                    if ((int)result.StatusCode == (oldStatus + 1))
                                    {
                                        triggerResult.OcsStatus = "Y";
                                    }
                                    else
                                    {
                                        triggerResult.OcsStatus = "Y";
                                    }
                                }
                            }
                            else if (i == 8 || i == 9 || i == 1) // IV and NC and LM
                            {
                                if (scenarioOrder == ScenarioOrder.Untouched)
                                {
                                    if ((int)result.StatusCode == 17)
                                    {
                                        triggerResult.OcsStatus = "Y";
                                    }
                                    else
                                    {
                                        triggerResult.OcsStatus = "Y";
                                    }
                                }
                            }
                            else
                            {
                                var statusMap = GetOcsStatusMapping(result.StatusCode);
                                if (statusMap == trigger.OcsStatus)
                                {
                                    triggerResult.OcsStatus = "Y";
                                }
                                else
                                {
                                    triggerResult.OcsStatus = "N";
                                }
                            }

                            OCS_QUEUE queueName = (trigger.Queue == QueueType.HTQ) ? OCS_QUEUE.HighTouch : OCS_QUEUE.Counselor;
                            if (result.Queue == queueName)
                            {
                                triggerResult.Queue = "Y";
                            }
                            else
                            {
                                triggerResult.Queue = "N";
                            }

                            if (trigger.ContactAttempts == "N" || (trigger.ContactAttempts == "+1" && result.ContactAttemptCount > oldCurrentAttemptCount))
                            {
                                triggerResult.ContactAttempts = "Y";
                            }
                            else
                            {
                                triggerResult.ContactAttempts = "N";
                            }
                        }
                    }
                }
                PrintScenarioResult(scenarioResult);
                scenarioResults.Add(scenarioResult);
                //bc.SubmitChanges();
            }
        }

        private string GetOcsStatusMapping(STATUS_CODE statusCode)
        {
            string status = string.Empty;
            switch (statusCode)
            {
                case STATUS_CODE.NewRecord:
                    status = "";
                    break;

                case STATUS_CODE.BadNumber:
                    status = "BN";
                    break;

                case STATUS_CODE.CounselorWillCallback1:
                    status = "";
                    break;

                case STATUS_CODE.CounselorWillCallback2:
                    status = "";
                    break;

                case STATUS_CODE.CounselorWillCallback3:
                    status = "";
                    break;

                case STATUS_CODE.CSRWillCallBack:
                    status = "";
                    break;

                case STATUS_CODE.Counseled:
                    status = "CO";
                    break;

                case STATUS_CODE.ContactMade:
                    status = "";
                    break;

                case STATUS_CODE.NoContact:
                    status = "NC";
                    break;

                case STATUS_CODE.NoShow:
                    status = "NS";
                    break;

                case STATUS_CODE.OptOut:
                    status = "NI";
                    break;

                case STATUS_CODE.PendingAppointment:
                    status = "";
                    break;

                case STATUS_CODE.TransferToCounselor:
                    status = "";
                    break;
                //case STATUS_CODE.ScheduledAppointment:
                //    status = "SA";
                //    break;
                case STATUS_CODE.ExclusionReport:
                    status = "EX";
                    break;

                case STATUS_CODE.GeneralQuestions:
                    status = "";
                    break;

                case STATUS_CODE.ImmediateCounseling:
                    status = "IM";
                    break;

                case STATUS_CODE.InitialContactAttempt1:
                    status = "";
                    break;

                case STATUS_CODE.InitialContactAttempt2:
                    status = "";
                    break;

                case STATUS_CODE.InitialContactAttempt3:
                    status = "";
                    break;

                case STATUS_CODE.InitialContactAttemptExtras:
                    status = "";
                    break;

                case STATUS_CODE.PaymentReminder1:
                    status = "P1";
                    break;

                case STATUS_CODE.PaymentReminder2:
                    status = "P2";
                    break;

                case STATUS_CODE.PaymentReminder3:
                    status = "P3";
                    break;

                case STATUS_CODE.PaymentReminder4:
                    status = "P4";
                    break;

                case STATUS_CODE.PaymentReminder5:
                    status = "P5";
                    break;

                case STATUS_CODE.PaymentReminder6:
                    status = "P6";
                    break;

                case STATUS_CODE.PaymentReminder7:
                    status = "";
                    break;

                case STATUS_CODE.PaymentReminder8:
                    status = "";
                    break;

                case STATUS_CODE.PaymentReminder9:
                    status = "";
                    break;

                case STATUS_CODE.PaymentReminder10:
                    status = "";
                    break;

                case STATUS_CODE.PaymentReminder11:
                    status = "";
                    break;

                case STATUS_CODE.PaymentReminder12:
                    status = "";
                    break;

                case STATUS_CODE.SecondCounseling:
                    status = "ST";
                    break;
                //case STATUS_CODE.InitialContactAttempt4:
                //    status = "";
                //    break;
                //case STATUS_CODE.Pending:
                //    status = "PE";
                //    break;
                //case STATUS_CODE.AppointmentConfirmed:
                //    status = "";
                //    break;
                //case STATUS_CODE.CounselHighTouch:
                //    status = "";
                //    break;
                default:
                    break;
            }
            return status;
        }

        private void PrintScenarioResult(ScenarioResult result)
        {
            Console.WriteLine("SCENARIO NAME: " + result.Name);
            Console.WriteLine("                 EX  NI  CO  IM  ST  SA  TR  RP  IV  NC  NS  LM  SK  BN");

            var activeFlagResult = GetTriggerResult(result, 1);
            var ocsStatusResult = GetTriggerResult(result, 2);
            var queueResult = GetTriggerResult(result, 3);
            var lastChangeFlag = GetTriggerResult(result, 4);
            var lastChangeDate = GetTriggerResult(result, 5);
            var contactAttempt = GetTriggerResult(result, 6);
            Console.WriteLine(activeFlagResult);
            Console.WriteLine(ocsStatusResult);
            Console.WriteLine(queueResult);
            Console.WriteLine(lastChangeFlag);
            Console.WriteLine(lastChangeDate);
            Console.WriteLine(contactAttempt);
            Console.WriteLine("\n");
        }

        private string GetTriggerResult(ScenarioResult scenario, int type)
        {
            const int ActiveFlag = 1;
            string printFormat = "{0, 17} {1, 3} {2,3} {3,3} {4,3} {5,3} {6,3} {7,3} {8,3} {9,3} {10,3} {11,3} {12,3} {13,3} {14,3}";
            string result = "";

            if (type == ActiveFlag)
            {
                result = string.Format(printFormat,
                                              "Active Flag",
                                              scenario.ExclusionReport.ActiveFlag,
                                              scenario.NotInterested.ActiveFlag,
                                              scenario.Counsel.ActiveFlag,
                                              scenario.ImmediateCounseling.ActiveFlag,
                                              scenario.SecondCounsel.ActiveFlag,
                                              scenario.ScheduleAppointment.ActiveFlag,
                                              scenario.TransferredToCounselor.ActiveFlag,
                                              scenario.SpokeToClient.ActiveFlag,
                                              scenario.InboundVoicemail.ActiveFlag,
                                              scenario.NoContact.ActiveFlag,
                                              scenario.NoShow.ActiveFlag,
                                              scenario.LeftMessage.ActiveFlag,
                                              scenario.Skip.ActiveFlag,
                                              scenario.BadNumber.ActiveFlag);
            }
            else if (type == 2)
            {
                result = string.Format(printFormat,
                                              "OCS Status",
                                              scenario.ExclusionReport.OcsStatus,
                                              scenario.NotInterested.OcsStatus,
                                              scenario.Counsel.OcsStatus,
                                              scenario.ImmediateCounseling.OcsStatus,
                                              scenario.SecondCounsel.OcsStatus,
                                              scenario.ScheduleAppointment.OcsStatus,
                                              scenario.TransferredToCounselor.OcsStatus,
                                              scenario.SpokeToClient.OcsStatus,
                                              scenario.InboundVoicemail.OcsStatus,
                                              scenario.NoContact.OcsStatus,
                                              scenario.NoShow.OcsStatus,
                                              scenario.LeftMessage.OcsStatus,
                                              scenario.Skip.OcsStatus,
                                              scenario.BadNumber.OcsStatus);
            }
            else if (type == 3)
            {
                result = string.Format(printFormat,
                                              "Queue",
                                              scenario.ExclusionReport.Queue,
                                              scenario.NotInterested.Queue,
                                              scenario.Counsel.Queue,
                                              scenario.ImmediateCounseling.Queue,
                                              scenario.SecondCounsel.Queue,
                                              scenario.ScheduleAppointment.Queue,
                                              scenario.TransferredToCounselor.Queue,
                                              scenario.SpokeToClient.Queue,
                                              scenario.InboundVoicemail.Queue,
                                              scenario.NoContact.Queue,
                                              scenario.NoShow.Queue,
                                              scenario.LeftMessage.Queue,
                                              scenario.Skip.Queue,
                                              scenario.BadNumber.Queue);
            }
            else if (type == 4)
            {
                result = string.Format(printFormat,
                                              "Last Change Flag",
                                              scenario.ExclusionReport.LastChangeFlag,
                                              scenario.NotInterested.LastChangeFlag,
                                              scenario.Counsel.LastChangeFlag,
                                              scenario.ImmediateCounseling.LastChangeFlag,
                                              scenario.SecondCounsel.LastChangeFlag,
                                              scenario.ScheduleAppointment.LastChangeFlag,
                                              scenario.TransferredToCounselor.LastChangeFlag,
                                              scenario.SpokeToClient.LastChangeFlag,
                                              scenario.InboundVoicemail.LastChangeFlag,
                                              scenario.NoContact.LastChangeFlag,
                                              scenario.NoShow.LastChangeFlag,
                                              scenario.LeftMessage.LastChangeFlag,
                                              scenario.Skip.LastChangeFlag,
                                              scenario.BadNumber.LastChangeFlag);
            }
            else if (type == 5)
            {
                result = string.Format(printFormat,
                                              "Last Change Date",
                                              scenario.ExclusionReport.LastChangeDate,
                                              scenario.NotInterested.LastChangeDate,
                                              scenario.Counsel.LastChangeDate,
                                              scenario.ImmediateCounseling.LastChangeDate,
                                              scenario.SecondCounsel.LastChangeDate,
                                              scenario.ScheduleAppointment.LastChangeDate,
                                              scenario.TransferredToCounselor.LastChangeDate,
                                              scenario.SpokeToClient.LastChangeDate,
                                              scenario.InboundVoicemail.LastChangeDate,
                                              scenario.NoContact.LastChangeDate,
                                              scenario.NoShow.LastChangeDate,
                                              scenario.LeftMessage.LastChangeDate,
                                              scenario.Skip.LastChangeDate,
                                              scenario.BadNumber.LastChangeDate);
            }
            else if (type == 6)
            {
                result = string.Format(printFormat,
                                              "Contact Attempts",
                                              scenario.ExclusionReport.ContactAttempts,
                                              scenario.NotInterested.ContactAttempts,
                                              scenario.Counsel.ContactAttempts,
                                              scenario.ImmediateCounseling.ContactAttempts,
                                              scenario.SecondCounsel.ContactAttempts,
                                              scenario.ScheduleAppointment.ContactAttempts,
                                              scenario.TransferredToCounselor.ContactAttempts,
                                              scenario.SpokeToClient.ContactAttempts,
                                              scenario.InboundVoicemail.ContactAttempts,
                                              scenario.NoContact.ContactAttempts,
                                              scenario.NoShow.ContactAttempts,
                                              scenario.LeftMessage.ContactAttempts,
                                              scenario.Skip.ContactAttempts,
                                              scenario.BadNumber.ContactAttempts);
            }

            return result;
        }

        private void ResetClientStatus(ScenarioOrder scenario)
        {
            using (var bc = new BusinessContext())
            {
                foreach (var client in clients)
                {
                    var ocsclient = bc.OCS_Clients.Where(ocs => ocs.ClientId == client.ClientID).FirstOrDefault();

                    if (ocsclient != null)
                    {
                        switch (scenario)
                        {
                            case ScenarioOrder.Untouched:
                                ocsclient.StatusCode = 0;
                                break;

                            case ScenarioOrder.InitialContact_C1:
                                ocsclient.StatusCode = 17;
                                break;

                            case ScenarioOrder.InitialContact_C2:
                                ocsclient.StatusCode = 18;
                                break;

                            case ScenarioOrder.InitialContact_C3:
                                ocsclient.StatusCode = 19;
                                break;

                            case ScenarioOrder.InitialContact_C4:
                                ocsclient.StatusCode = 0;
                                break;

                            case ScenarioOrder.AdditionalAttempt_OT:
                                ocsclient.StatusCode = 20;
                                break;

                            case ScenarioOrder.PaymentReminder_P1:
                                ocsclient.StatusCode = 21;
                                break;

                            case ScenarioOrder.PaymentReminder_P2:
                                ocsclient.StatusCode = 22;
                                break;

                            case ScenarioOrder.PaymentReminder_P3:
                                ocsclient.StatusCode = 23;
                                break;

                            case ScenarioOrder.PaymentReminder_P4:
                                ocsclient.StatusCode = 24;
                                break;

                            case ScenarioOrder.PaymentReminder_P5:
                                ocsclient.StatusCode = 25;
                                break;

                            case ScenarioOrder.PaymentReminder_P6:
                                ocsclient.StatusCode = 26;
                                break;

                            case ScenarioOrder.ClientNotInterested_NI:
                                ocsclient.StatusCode = 10;
                                break;

                            case ScenarioOrder.BadNumber_BN:
                                ocsclient.StatusCode = 1;
                                break;

                            case ScenarioOrder.Pending_PN:
                                ocsclient.StatusCode = 11;
                                break;

                            default:
                                break;
                        }
                        ocsclient.ActiveFlag = true;
                    }
                }
                bc.SubmitChanges();
            }
        }

        #endregion "EXECUTE"

        #region "CLEAN UP"

        public enum TableType
        {
            Address,
            Client,
            Name,
            Phone,
            People,
            OcsClient
        }

        private void DeleteTableRow(int? id, TableType type)
        {
            using (var bc = new BusinessContext())
            {
                switch (type)
                {
                    case TableType.Address:
                        var address = bc.GetTable<address>();
                        var addr = bc.addresses.Where(a => a.Id == id).FirstOrDefault();
                        if (addr != null)
                        {
                            address.DeleteOnSubmit(addr);
                        }
                        break;

                    case TableType.Client:
                        var client = bc.GetTable<client>();
                        var cl = bc.clients.Where(a => a.Id == id).FirstOrDefault();
                        if (cl != null)
                        {
                            client.DeleteOnSubmit(cl);
                        }
                        break;

                    case TableType.Name:
                        var name = bc.GetTable<Name>();
                        var n = bc.Names.Where(a => a.Id == id).FirstOrDefault();
                        if (n != null)
                        {
                            name.DeleteOnSubmit(n);
                        }
                        break;

                    case TableType.Phone:
                        var phone = bc.GetTable<TelephoneNumber>();
                        var p = bc.TelephoneNumbers.Where(a => a.Id == id).FirstOrDefault();
                        if (p != null)
                        {
                            phone.DeleteOnSubmit(p);
                        }
                        break;

                    case TableType.People:
                        var person = bc.GetTable<people>();
                        var per1 = bc.peoples.Where(a => a.Client == id && a.Relation == 1).FirstOrDefault();
                        var per2 = bc.peoples.Where(a => a.Client == id && a.Relation == 2).FirstOrDefault();
                        if (per1 != null)
                        {
                            person.DeleteOnSubmit(per1);
                        }
                        if (per2 != null)
                        {
                            person.DeleteOnSubmit(per2);
                        }
                        break;

                    case TableType.OcsClient:
                        var ocsClient = bc.GetTable<OCS_Client>();
                        var ocs = bc.OCS_Clients.Where(a => a.Id == id).FirstOrDefault();
                        if (ocs != null)
                        {
                            ocsClient.DeleteOnSubmit(ocs);
                        }
                        break;

                    default:
                        break;
                }
                bc.SubmitChanges();
            }
        }

        public void Cleanup()
        {
            foreach (var client in clients)
            {
                DeleteTableRow(client.ClientID, TableType.People);

                if (client.ClientID > 0)
                {
                    DeleteTableRow(client.ClientID, TableType.Client);
                }

                if (client.AddressID != null)
                {
                    DeleteTableRow(client.AddressID, TableType.Address);
                }

                if (client.HomePhoneID != null)
                {
                    DeleteTableRow(client.HomePhoneID, TableType.Phone);
                }

                if (client.ApplicantCellID != null)
                {
                    DeleteTableRow(client.ApplicantCellID, TableType.Phone);
                }

                if (client.ApplicantWorkPhoneID != null)
                {
                    DeleteTableRow(client.ApplicantWorkPhoneID, TableType.Phone);
                }

                if (client.CoapplicantCellID != null)
                {
                    DeleteTableRow(client.CoapplicantCellID, TableType.Phone);
                }

                if (client.CoapplicantWorkPhoneID != null)
                {
                    DeleteTableRow(client.CoapplicantWorkPhoneID, TableType.Phone);
                }

                if (client.MessagePhoneID != null)
                {
                    DeleteTableRow(client.MessagePhoneID, TableType.Phone);
                }

                if (client.CoapplicantNameID != null)
                {
                    DeleteTableRow(client.CoapplicantNameID, TableType.Name);
                }
            }
        }

        #endregion "CLEAN UP"
    }
}