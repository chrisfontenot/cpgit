﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DebtPlus.Test
{
    public class EIClient
    {
        public string QueueCode { get; set; }
        public string Street { get; set; }
        public string City { get; set; }
        public int State { get; set; }
        public string Zipcode { get; set; }

        public bool HasPerson1 { get; set; }
        public string FirstName1 { get; set; }
        public string LastName1 { get; set; }
        public string SSN1 { get; set; }

        public bool HasPerson2 { get; set; }
        public string FirstName2 { get; set; }
        public string LastName2 { get; set; }
        public string SSN2 { get; set; }

        public string p1Area { get; set; }
        public string p1Num { get; set; }
        public string p2Area { get; set; }
        public string p2Num { get; set; }
        public string p3Area { get; set; }
        public string p3Num { get; set; }
        public string p4Area { get; set; }
        public string p4Num { get; set; }
        public string p5Area { get; set; }
        public string p5Num { get; set; }
        public string p6Area { get; set; }
        public string p6Num { get; set; }

        public int ClientID { get; set; }
    }
}
