﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Excel;
using DebtPlus.LINQ;
using System.Data;
using DebtPlus.Svc.OCS.Rules.FMACEI;
using DebtPlus.Svc.OCS;
using DebtPlus.OCS.Domain;

namespace DebtPlus.Test
{
    public class EITest : ITest
    {
        public enum TriggerOrder
        {
            BadNumber_BN = 1,
            CounselorToCallBack_CB = 2,
            CCRToCallBack_CC = 3,
            Counsel_CO = 4,
            Deceased_DS = 5,
            LeftMessage_LM = 6,
            NoShow_NS = 7,
            NoContact_NC = 8,
            ClientNotInterested_NI = 9,
            ScheduleAppointment_SA = 10,
            Skip_SK = 11,
            TransferredCallToCounselor_TR = 12,
            WorkingWithLender_WL = 13,
        }

        public enum ScenarioOrder
        {
            Untouched = 1,
            CounselorToCallBack_CB1 = 2,
            CounselorToCallBack_CB2 = 3,
            CounselorToCallBack_CB3 = 4,
            Counsel_CO = 5,
            BadNumber_BN = 6,
            ContactMade_CT = 7,
            NoContact_NC = 8,
            NoShow_NS = 9,
            OptOut_OP = 10,
            PendingAppointment_PE = 11,
            TransferToCounsel_TR = 12,
            CCRToCallBack_CC = 13,
        }

        string connectionString = null;
        private void DatabaseSetup()
        {
            using (DebtPlus.LINQ.SQLInfoClass sql = DebtPlus.LINQ.SQLInfoClass.getDefault())
            {
                Repository.Setup.ConnectionString = sql.ConnectionString.Replace("(local)\\SQLEXPRESS", "10.0.0.97");
                Repository.Setup.CommandTimeout = sql.CommandTimeout;
                DebtPlus.LINQ.SQLInfoClass SqlInfo = DebtPlus.LINQ.SQLInfoClass.getDefault();
                connectionString = SqlInfo.ConnectionString;
            }
        }

        List<Scenario> scenarios = new List<Scenario>();
        List<ScenarioResult> scenarioResults = new List<ScenarioResult>();

        #region "SET UP"
        public void Setup()
        {
            var filename = "EI.xlsx";
            DatabaseSetup();
            ReadSetupData(filename);
        }

        private void ReadSetupData(string filename)
        {
            var path = System.IO.Path.Combine(
                System.IO.Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().Location),
                "TestSpreadsheets",
                filename);

            using (var stream = new System.IO.FileStream(path, System.IO.FileMode.Open))
            {
                IExcelDataReader reader = Excel.ExcelReaderFactory.CreateOpenXmlReader(stream);
                reader.IsFirstRowAsColumnNames = true;

                var ds = reader.AsDataSet();

                if (ds.Tables[2] != null)
                {
                    ReadTestConfiguration(ds.Tables[2]);
                }
            }
        }

        private void ReadTestConfiguration(System.Data.DataTable dataTable)
        {
            // Read Untouched Scenario
            var rows = dataTable.Rows;
            int rowCount = rows.Count;

            int columnOffset = 4;
            for (int i = 5; i < rowCount; i = i + 5)
            {
                var scenario = new Scenario();
                scenario.Name = rows[i][0].ToString();

                scenario.BadNumber = GetTrigger(rows, i, (int)TriggerOrder.BadNumber_BN + columnOffset);
                scenario.CounselorToCallBack = GetTrigger(rows, i, (int)TriggerOrder.CounselorToCallBack_CB + columnOffset);
                scenario.CCRToCallBack = GetTrigger(rows, i, (int)TriggerOrder.CCRToCallBack_CC + columnOffset);
                scenario.Counsel = GetTrigger(rows, i, (int)TriggerOrder.Counsel_CO + columnOffset);
                scenario.Deceased = GetTrigger(rows, i, (int)TriggerOrder.Deceased_DS + columnOffset);
                scenario.LeftMessage = GetTrigger(rows, i, (int)TriggerOrder.LeftMessage_LM + columnOffset);
                scenario.NoShow = GetTrigger(rows, i, (int)TriggerOrder.NoShow_NS + columnOffset);
                scenario.NoContact = GetTrigger(rows, i, (int)TriggerOrder.NoContact_NC + columnOffset);
                scenario.NotInterested = GetTrigger(rows, i, (int)TriggerOrder.ClientNotInterested_NI + columnOffset);
                scenario.ScheduleAppointment = GetTrigger(rows, i, (int)TriggerOrder.ScheduleAppointment_SA + columnOffset);
                scenario.Skip = GetTrigger(rows, i, (int)TriggerOrder.Skip_SK + columnOffset);
                scenario.TransferredToCounselor = GetTrigger(rows, i, (int)TriggerOrder.TransferredCallToCounselor_TR + columnOffset);
                scenario.WorkingWithLender = GetTrigger(rows, i, (int)TriggerOrder.WorkingWithLender_WL + columnOffset);

                scenarios.Add(scenario);
            }

        }

        private Trigger GetTrigger(DataRowCollection rows, int baseIndex, int triggerIndex)
        {
            const int ActiveFlagIndex = 1;
            const int OcsFlagIndex = 2;
            const int ContactAttemptsIndex = 4;

            var trigger = new Trigger
                {
                    ActiveFlag = rows[baseIndex + ActiveFlagIndex][triggerIndex].ToString(),
                    OcsStatus = rows[baseIndex + OcsFlagIndex][triggerIndex].ToString(),
                    ContactAttempts = rows[baseIndex + ContactAttemptsIndex][triggerIndex].ToString(),
                };
            return trigger;
        }
        #endregion "SET UP"

        #region "EXECUTE"

        public void Execute()
        {
            scenarioResults.Clear();

            ExecuteScenario(ScenarioOrder.Untouched);

            // Reset Status to CounselorToCallBack_CB1
            ResetClientStatus(ScenarioOrder.CounselorToCallBack_CB1);
            ExecuteScenario(ScenarioOrder.CounselorToCallBack_CB1);

            // Reset Status to CounselorToCallBack_CB2
            ResetClientStatus(ScenarioOrder.CounselorToCallBack_CB2);
            ExecuteScenario(ScenarioOrder.CounselorToCallBack_CB2);

            // Reset Status to CounselorToCallBack_CB3
            ResetClientStatus(ScenarioOrder.CounselorToCallBack_CB3);
            ExecuteScenario(ScenarioOrder.CounselorToCallBack_CB3);

            // Reset Status to Counsel_CO
            ResetClientStatus(ScenarioOrder.Counsel_CO);
            ExecuteScenario(ScenarioOrder.Counsel_CO);

            // Reset Status to BadNumber_BN
            ResetClientStatus(ScenarioOrder.BadNumber_BN);
            ExecuteScenario(ScenarioOrder.BadNumber_BN);

            // Reset Status to ContactMade_CT
            ResetClientStatus(ScenarioOrder.ContactMade_CT);
            ExecuteScenario(ScenarioOrder.ContactMade_CT);

            // Reset Status to NoContact_NC
            ResetClientStatus(ScenarioOrder.NoContact_NC);
            ExecuteScenario(ScenarioOrder.NoContact_NC);

            // Reset Status to NoShow_NS
            ResetClientStatus(ScenarioOrder.NoShow_NS);
            ExecuteScenario(ScenarioOrder.NoShow_NS);

            // Reset Status to OptOut_OP
            ResetClientStatus(ScenarioOrder.OptOut_OP);
            ExecuteScenario(ScenarioOrder.OptOut_OP);

            // Reset Status to PendingAppointment_PE
            ResetClientStatus(ScenarioOrder.PendingAppointment_PE);
            ExecuteScenario(ScenarioOrder.PendingAppointment_PE);

            // Reset Status to TransferToCounsel_TR
            ResetClientStatus(ScenarioOrder.TransferToCounsel_TR);
            ExecuteScenario(ScenarioOrder.TransferToCounsel_TR);

            // Reset Status to CC_CC
            ResetClientStatus(ScenarioOrder.CCRToCallBack_CC);
            ExecuteScenario(ScenarioOrder.CCRToCallBack_CC);

        }

        private void ExecuteScenario(ScenarioOrder scenarioOrder)
        {
            var scenario = scenarios[(int)scenarioOrder - 1];

            OCS.Domain.STATUS_CODE statusCode = GetStatusCode(scenarioOrder);

            var scenarioResult = new ScenarioResult();
            scenarioResult.Name = scenario.Name;

            Trigger trigger = null;
            for (int i = 0; i < 14; i++)
            {
                var triggerResult = new TriggerResult();

                var ocsClient = new OCSClient();
                ocsClient.CurrentAttempt = new ContactAttempt();
                ocsClient.CurrentAttempt.Outcomes = new List<ContactAttempt.CallOutcome>();
                ocsClient.CurrentAttempt.Outcomes.Add(new ContactAttempt.CallOutcome()
                {
                    PhoneNumber = new TelephoneNumber(1, "111", "2222222", ""),
                });

                switch (i)
                {
                    case 0:
                        ocsClient.CurrentAttempt.Outcomes[0].Result = RESULT_CODE.BadNumber;
                        trigger = scenario.BadNumber;
                        scenarioResult.BadNumber = triggerResult;
                        break;
                    case 1:
                        ocsClient.CurrentAttempt.Outcomes[0].Result = RESULT_CODE.CounselorWillCallBack;
                        trigger = scenario.CounselorToCallBack;
                        scenarioResult.CounselorToCallBack = triggerResult;
                        break;
                    case 2:
                        ocsClient.CurrentAttempt.Outcomes[0].Result = RESULT_CODE.CSRWillCallBack;
                        trigger = scenario.CCRToCallBack;
                        scenarioResult.CCRToCallBack = triggerResult;
                        break;
                    case 3:
                        ocsClient.CurrentAttempt.Outcomes[0].Result = RESULT_CODE.Counseled;
                        trigger = scenario.Counsel;
                        scenarioResult.Counsel = triggerResult;
                        break;
                    case 4:
                        ocsClient.CurrentAttempt.Outcomes[0].Result = RESULT_CODE.Deceased;
                        trigger = scenario.Deceased;
                        scenarioResult.Deceased = triggerResult;
                        break;
                    case 5:
                        ocsClient.CurrentAttempt.Outcomes[0].Result = RESULT_CODE.LeftMessage;
                        trigger = scenario.LeftMessage;
                        scenarioResult.LeftMessage = triggerResult;
                        break;
                    case 6:
                        ocsClient.CurrentAttempt.Outcomes[0].Result = RESULT_CODE.NoShow;
                        trigger = scenario.NoShow;
                        scenarioResult.NoShow = triggerResult;
                        break;
                    case 7:
                        ocsClient.CurrentAttempt.Outcomes[0].Result = RESULT_CODE.NoContact;
                        trigger = scenario.NoContact;
                        scenarioResult.NoContact = triggerResult;
                        break;
                    case 8:
                        ocsClient.CurrentAttempt.Outcomes[0].Result = RESULT_CODE.ClientNotInterested;
                        trigger = scenario.NotInterested;
                        scenarioResult.NotInterested = triggerResult;
                        break;
                    case 9:
                        ocsClient.CurrentAttempt.Outcomes[0].Result = RESULT_CODE.ScheduledAppointment;
                        trigger = scenario.ScheduleAppointment;
                        scenarioResult.ScheduleAppointment = triggerResult;
                        break;
                    case 10:
                        ocsClient.CurrentAttempt.Outcomes[0].Result = RESULT_CODE.Skip;
                        trigger = scenario.Skip;
                        scenarioResult.Skip = triggerResult;
                        break;
                    case 11:
                        ocsClient.CurrentAttempt.Outcomes[0].Result = RESULT_CODE.TransferredToCounselor;
                        trigger = scenario.TransferredToCounselor;
                        scenarioResult.TransferredToCounselor = triggerResult;
                        break;
                    case 12:
                        ocsClient.CurrentAttempt.Outcomes[0].Result = RESULT_CODE.WorkingWithLender;
                        trigger = scenario.WorkingWithLender;
                        scenarioResult.WorkingWithLender = triggerResult;
                        break;
                    default:
                        break;
                }

                bool oldActiveFlag = true;
                int oldStatus = (int)STATUS_CODE.NewRecord;
                int oldCurrentAttemptCount = 0;
                oldCurrentAttemptCount = ocsClient.ContactAttemptCount;

                ocsClient.ActiveFlag = oldActiveFlag;

                var result = StateMachine.UpdateOnTrigger(OCS.Domain.TRIGGER.NoTrigger, statusCode, ocsClient);

                if (i == 10) //SKIP
                {
                    if (result.ActiveFlag == oldActiveFlag)
                    {
                        triggerResult.ActiveFlag = "Y";
                    }
                    else
                    {
                        triggerResult.ActiveFlag = "N";
                    }

                    if ((int)result.StatusCode == oldStatus)
                    {
                        triggerResult.OcsStatus = "Y";
                    }
                    else
                    {
                        triggerResult.OcsStatus = "N";
                    }

                    if (oldCurrentAttemptCount == result.ContactAttemptCount)
                    {
                        triggerResult.ContactAttempts = "Y";
                    }
                    else
                    {
                        triggerResult.ContactAttempts = "N";
                    }
                }
                else
                {
                    var activeFlag = (trigger.ActiveFlag == "I" ? false : true);

                    if (result.ActiveFlag == activeFlag)
                    {
                        triggerResult.ActiveFlag = "Y";
                    }
                    else
                    {
                        triggerResult.ActiveFlag = "N";
                    }

                    switch (i)
                    {
                        case 0: // Bad Number
                            triggerResult.OcsStatus = (result.StatusCode == STATUS_CODE.BadNumber) ? "Y" : "N";
                            break;
                        case 1: // Counselor To Call Back
                            STATUS_CODE cbCode = STATUS_CODE.NoShow; // set default
                            switch (scenarioOrder)
	                        {
                                case ScenarioOrder.CounselorToCallBack_CB1:
                                    cbCode = STATUS_CODE.CounselorWillCallback2;
                                    break;
                                case ScenarioOrder.CounselorToCallBack_CB2:
                                    cbCode = STATUS_CODE.CounselorWillCallback3;
                                    break;
                                case ScenarioOrder.CounselorToCallBack_CB3:
                                    cbCode = STATUS_CODE.CounselorWillCallback3;
                                    break;
                                case ScenarioOrder.Untouched:
                                case ScenarioOrder.Counsel_CO:
                                case ScenarioOrder.BadNumber_BN:
                                case ScenarioOrder.ContactMade_CT:
                                case ScenarioOrder.NoContact_NC:
                                case ScenarioOrder.NoShow_NS:
                                case ScenarioOrder.OptOut_OP:
                                case ScenarioOrder.PendingAppointment_PE:
                                case ScenarioOrder.TransferToCounsel_TR:
                                case ScenarioOrder.CCRToCallBack_CC:
                                    cbCode = STATUS_CODE.CounselorWillCallback1;
                                    break;
	                        }
                            triggerResult.OcsStatus = (result.StatusCode == cbCode) ? "Y" : "N";
                            break;
                        case 2: // CCR To Call Back
                            triggerResult.OcsStatus = (result.StatusCode == STATUS_CODE.CSRWillCallBack) ? "Y" : "N";
                            break;
                        case 3: // Counseled
                            triggerResult.OcsStatus = (result.StatusCode == STATUS_CODE.Counseled) ? "Y" : "N";
                            break;
                        case 4: // Deceased
                            triggerResult.OcsStatus = (result.StatusCode == STATUS_CODE.ContactMade) ? "Y" : "N";
                            break;
                        case 5: // Left Message
                            STATUS_CODE lmCode = STATUS_CODE.NoShow; // set default
                            switch (scenarioOrder)
	                        {
                                case ScenarioOrder.CounselorToCallBack_CB1:
                                    lmCode = STATUS_CODE.CounselorWillCallback2;
                                    break;
                                case ScenarioOrder.CounselorToCallBack_CB2:
                                    lmCode = STATUS_CODE.CounselorWillCallback3;
                                    break;
                                case ScenarioOrder.CounselorToCallBack_CB3:
                                    lmCode = STATUS_CODE.CounselorWillCallback3;
                                    break;
                                case ScenarioOrder.Untouched:
                                case ScenarioOrder.Counsel_CO:
                                case ScenarioOrder.BadNumber_BN:
                                case ScenarioOrder.ContactMade_CT:
                                case ScenarioOrder.NoContact_NC:
                                case ScenarioOrder.NoShow_NS:
                                case ScenarioOrder.OptOut_OP:
                                case ScenarioOrder.PendingAppointment_PE:
                                case ScenarioOrder.TransferToCounsel_TR:
                                case ScenarioOrder.CCRToCallBack_CC:
                                    lmCode = STATUS_CODE.NoContact;
                                    break;
	                        }
                            triggerResult.OcsStatus = (result.StatusCode == lmCode) ? "Y" : "N";
                            break;
                        case 6: // No Show
                            triggerResult.OcsStatus = (result.StatusCode == STATUS_CODE.NoShow) ? "Y" : "N";
                            break;
                        case 7: // No Contact
                            STATUS_CODE ncCode = STATUS_CODE.NoShow; // set default
                            switch (scenarioOrder)
	                        {
                                case ScenarioOrder.CounselorToCallBack_CB1:
                                    ncCode = STATUS_CODE.CounselorWillCallback2;
                                    break;
                                case ScenarioOrder.CounselorToCallBack_CB2:
                                    ncCode = STATUS_CODE.CounselorWillCallback3;
                                    break;
                                case ScenarioOrder.CounselorToCallBack_CB3:
                                    ncCode = STATUS_CODE.CounselorWillCallback3;
                                    break;
                                case ScenarioOrder.Untouched:
                                case ScenarioOrder.Counsel_CO:
                                case ScenarioOrder.BadNumber_BN:
                                case ScenarioOrder.ContactMade_CT:
                                case ScenarioOrder.NoContact_NC:
                                case ScenarioOrder.NoShow_NS:
                                case ScenarioOrder.OptOut_OP:
                                case ScenarioOrder.PendingAppointment_PE:
                                case ScenarioOrder.TransferToCounsel_TR:
                                case ScenarioOrder.CCRToCallBack_CC:
                                    ncCode = STATUS_CODE.NoContact;
                                    break;
	                        }
                            triggerResult.OcsStatus = (result.StatusCode == ncCode) ? "Y" : "N";
                            break;
                        case 8: // Client Not Interested
                            triggerResult.OcsStatus = (result.StatusCode == STATUS_CODE.OptOut) ? "Y" : "N";
                            break;
                        case 9: // Scheduled Appointment
                            triggerResult.OcsStatus = (result.StatusCode == STATUS_CODE.PendingAppointment) ? "Y" : "N";
                            break;
                        case 11: // Transfer to Couinselor
                            triggerResult.OcsStatus = (result.StatusCode == STATUS_CODE.TransferToCounselor) ? "Y" : "N";
                            break;
                        case 12: // Working With Lender
                            triggerResult.OcsStatus = (result.StatusCode == STATUS_CODE.ContactMade) ? "Y" : "N";
                            break;
                    }

                    if (trigger.ContactAttempts == "N" || (trigger.ContactAttempts == "+1" && result.ContactAttemptCount > oldCurrentAttemptCount))
                    {
                        triggerResult.ContactAttempts = "Y";
                    }
                    else
                    {
                        triggerResult.ContactAttempts = "N";
                    }
                }
            }
            PrintScenarioResult(scenarioResult);
            scenarioResults.Add(scenarioResult);
        }

        private STATUS_CODE GetStatusCode(ScenarioOrder scenarioOrder)
        {
            OCS.Domain.STATUS_CODE statusCode = STATUS_CODE.NewRecord;

            switch (scenarioOrder)
            {
                case ScenarioOrder.Untouched:
                    statusCode = STATUS_CODE.NewRecord;
                    break;
                case ScenarioOrder.CounselorToCallBack_CB1:
                    statusCode = STATUS_CODE.CounselorWillCallback1;
                    break;
                case ScenarioOrder.CounselorToCallBack_CB2:
                    statusCode = STATUS_CODE.CounselorWillCallback2;
                    break;
                case ScenarioOrder.CounselorToCallBack_CB3:
                    statusCode = STATUS_CODE.CounselorWillCallback3;
                    break;
                case ScenarioOrder.Counsel_CO:
                    statusCode = STATUS_CODE.Counseled;
                    break;
                case ScenarioOrder.BadNumber_BN:
                    statusCode = STATUS_CODE.BadNumber;
                    break;
                case ScenarioOrder.ContactMade_CT:
                    statusCode = STATUS_CODE.ContactMade;
                    break;
                case ScenarioOrder.NoContact_NC:
                    statusCode = STATUS_CODE.NoContact;
                    break;
                case ScenarioOrder.NoShow_NS:
                    statusCode = STATUS_CODE.NoShow;
                    break;
                case ScenarioOrder.OptOut_OP:
                    statusCode = STATUS_CODE.OptOut;
                    break;
                case ScenarioOrder.PendingAppointment_PE:
                    statusCode = STATUS_CODE.PendingAppointment;
                    break;
                case ScenarioOrder.TransferToCounsel_TR:
                    statusCode = STATUS_CODE.TransferToCounselor;
                    break;
                case ScenarioOrder.CCRToCallBack_CC:
                    statusCode = STATUS_CODE.CSRWillCallBack;
                    break;
                default:
                    break;
            }
            return statusCode;
        }

        private string GetOcsStatusMapping(STATUS_CODE statusCode)
        {
            string status = string.Empty;
            switch (statusCode)
            {
                case STATUS_CODE.NewRecord:
                    status = "";
                    break;
                case STATUS_CODE.BadNumber:
                    status = "BN";
                    break;
                case STATUS_CODE.CounselorWillCallback1:
                    status = "";
                    break;
                case STATUS_CODE.CounselorWillCallback2:
                    status = "";
                    break;
                case STATUS_CODE.CounselorWillCallback3:
                    status = "";
                    break;
                case STATUS_CODE.CSRWillCallBack:
                    status = "";
                    break;
                case STATUS_CODE.Counseled:
                    status = "CO";
                    break;
                case STATUS_CODE.ContactMade:
                    status = "";
                    break;
                case STATUS_CODE.NoContact:
                    status = "NC";
                    break;
                case STATUS_CODE.NoShow:
                    status = "NS";
                    break;
                case STATUS_CODE.OptOut:
                    status = "NI";
                    break;
                case STATUS_CODE.PendingAppointment:
                    status = "";
                    break;
                case STATUS_CODE.TransferToCounselor:
                    status = "";
                    break;
                case STATUS_CODE.ExclusionReport:
                    status = "EX";
                    break;
                case STATUS_CODE.GeneralQuestions:
                    status = "";
                    break;
                case STATUS_CODE.ImmediateCounseling:
                    status = "IM";
                    break;
                case STATUS_CODE.InitialContactAttempt1:
                    status = "";
                    break;
                case STATUS_CODE.InitialContactAttempt2:
                    status = "";
                    break;
                case STATUS_CODE.InitialContactAttempt3:
                    status = "";
                    break;
                case STATUS_CODE.InitialContactAttemptExtras:
                    status = "";
                    break;
                case STATUS_CODE.PaymentReminder1:
                    status = "P1";
                    break;
                case STATUS_CODE.PaymentReminder2:
                    status = "P2";
                    break;
                case STATUS_CODE.PaymentReminder3:
                    status = "P3";
                    break;
                case STATUS_CODE.PaymentReminder4:
                    status = "P4";
                    break;
                case STATUS_CODE.PaymentReminder5:
                    status = "P5";
                    break;
                case STATUS_CODE.PaymentReminder6:
                    status = "P6";
                    break;
                case STATUS_CODE.PaymentReminder7:
                    status = "";
                    break;
                case STATUS_CODE.PaymentReminder8:
                    status = "";
                    break;
                case STATUS_CODE.PaymentReminder9:
                    status = "";
                    break;
                case STATUS_CODE.PaymentReminder10:
                    status = "";
                    break;
                case STATUS_CODE.PaymentReminder11:
                    status = "";
                    break;
                case STATUS_CODE.PaymentReminder12:
                    status = "";
                    break;
                case STATUS_CODE.SecondCounseling:
                    status = "ST";
                    break;
                default:
                    break;
            }
            return status;
        }

        private void PrintScenarioResult(ScenarioResult result)
        {
            Console.WriteLine("SCENARIO NAME: " + result.Name);
            Console.WriteLine("---------------------------------------------------------------------");
            Console.WriteLine("                   BN  CB  CC  CO  DS  LM  NS  NC  NI  SA  SK  TR  WL");
            Console.WriteLine("---------------------------------------------------------------------");

            var activeFlagResult = GetTriggerResult(result, 1);
            var ocsStatusResult = GetTriggerResult(result, 2);
            var contactAttempt = GetTriggerResult(result, 3);

            PrintColorResult(activeFlagResult);
            PrintColorResult(ocsStatusResult);
            PrintColorResult(contactAttempt);
            Console.WriteLine("\n");
        }

        private void PrintColorResult(string activeFlagResult)
        {
            List<string> result = activeFlagResult.Split(new char[] { ' ' }).ToList();

            foreach (var item in result)
            {
                if (string.Compare(item, "y", true) == 0)
                {
                    Console.ForegroundColor = ConsoleColor.Green;
                    Console.Write("Y ");
                    Console.ResetColor();
                }
                else if (string.Compare(item, "n", true) == 0)
                {
                    Console.ForegroundColor = ConsoleColor.Red;
                    Console.Write("N ");
                    Console.ResetColor();
                }
                else
                {
                    Console.Write(item + " ");
                }
            }
            Console.WriteLine("");
        }

        private string GetTriggerResult(ScenarioResult scenario, int type)
        {
            const int ActiveFlag = 1;
            string printFormat = "{0, 17} {1, 3} {2,3} {3,3} {4,3} {5,3} {6,3} {7,3} {8,3} {9,3} {10,3} {11,3} {12,3} {13,3}";
            string result = "";

            if (type == ActiveFlag)
            {
                result = string.Format(printFormat,
                                              "Active Flag",
                                              scenario.BadNumber.ActiveFlag,
                                              scenario.CounselorToCallBack.ActiveFlag,
                                              scenario.CCRToCallBack.ActiveFlag,
                                              scenario.Counsel.ActiveFlag,
                                              scenario.Deceased.ActiveFlag,
                                              scenario.LeftMessage.ActiveFlag,
                                              scenario.NoShow.ActiveFlag,
                                              scenario.NoContact.ActiveFlag,
                                              scenario.NotInterested.ActiveFlag,
                                              scenario.ScheduleAppointment.ActiveFlag,
                                              scenario.Skip.ActiveFlag,
                                              scenario.TransferredToCounselor.ActiveFlag,
                                              scenario.WorkingWithLender.ActiveFlag);
            }
            else if (type == 2)
            {
                result = string.Format(printFormat,
                                              "OCS Status",
                                              scenario.BadNumber.OcsStatus,
                                              scenario.CounselorToCallBack.OcsStatus,
                                              scenario.CCRToCallBack.OcsStatus,
                                              scenario.Counsel.OcsStatus,
                                              scenario.Deceased.OcsStatus,
                                              scenario.LeftMessage.OcsStatus,
                                              scenario.NoShow.OcsStatus,
                                              scenario.NoContact.OcsStatus,
                                              scenario.NotInterested.OcsStatus,
                                              scenario.ScheduleAppointment.OcsStatus,
                                              scenario.Skip.OcsStatus,
                                              scenario.TransferredToCounselor.OcsStatus,
                                              scenario.WorkingWithLender.OcsStatus);
            }
            else if (type == 3)
            {
                result = string.Format(printFormat,
                                              "Contact Attempts",
                                              scenario.BadNumber.ContactAttempts,
                                              scenario.CounselorToCallBack.ContactAttempts,
                                              scenario.CCRToCallBack.ContactAttempts,
                                              scenario.Counsel.ContactAttempts,
                                              scenario.Deceased.ContactAttempts,
                                              scenario.LeftMessage.ContactAttempts,
                                              scenario.NoShow.ContactAttempts,
                                              scenario.NoContact.ContactAttempts,
                                              scenario.NotInterested.ContactAttempts,
                                              scenario.ScheduleAppointment.ContactAttempts,
                                              scenario.Skip.ContactAttempts,
                                              scenario.TransferredToCounselor.ContactAttempts,
                                              scenario.WorkingWithLender.ContactAttempts);
            }

            return result;
        }

        private void ResetClientStatus(ScenarioOrder scenario)
        {
        }

        #endregion "EXECUTE"

        #region "CLEAN UP"

        public enum TableType
        {
            Address,
            Client,
            Name,
            Phone,
            People,
            OcsClient
        }

        private void DeleteTableRow(int? id, TableType type)
        {
            using (var bc = new BusinessContext())
            {
                switch (type)
                {
                    case TableType.Address:
                        var address = bc.GetTable<address>();
                        var addr = bc.addresses.Where(a => a.Id == id).FirstOrDefault();
                        if (addr != null)
                        {
                            address.DeleteOnSubmit(addr);
                        }
                        break;
                    case TableType.Client:
                        var client = bc.GetTable<client>();
                        var cl = bc.clients.Where(a => a.Id == id).FirstOrDefault();
                        if (cl != null)
                        {
                            client.DeleteOnSubmit(cl);
                        }
                        break;
                    case TableType.Name:
                        var name = bc.GetTable<Name>();
                        var n = bc.Names.Where(a => a.Id == id).FirstOrDefault();
                        if (n != null)
                        {
                            name.DeleteOnSubmit(n);
                        }
                        break;
                    case TableType.Phone:
                        var phone = bc.GetTable<TelephoneNumber>();
                        var p = bc.TelephoneNumbers.Where(a => a.Id == id).FirstOrDefault();
                        if (p != null)
                        {
                            phone.DeleteOnSubmit(p);
                        }
                        break;
                    case TableType.People:
                        var person = bc.GetTable<people>();
                        var per1 = bc.peoples.Where(a => a.Client == id && a.Relation == 1).FirstOrDefault();
                        var per2 = bc.peoples.Where(a => a.Client == id && a.Relation == 2).FirstOrDefault();
                        if (per1 != null)
                        {
                            person.DeleteOnSubmit(per1);
                        }
                        if (per2 != null)
                        {
                            person.DeleteOnSubmit(per2);
                        }
                        break;
                    case TableType.OcsClient:
                        var ocsClient = bc.GetTable<OCS_Client>();
                        var ocs = bc.OCS_Clients.Where(a => a.Id == id).FirstOrDefault();
                        if (ocs != null)
                        {
                            ocsClient.DeleteOnSubmit(ocs);
                        }
                        break;
                    default:
                        break;
                }
                bc.SubmitChanges();
            }
        }

        public void Cleanup()
        {
        }

        #endregion "CLEAN UP"
    }
}
