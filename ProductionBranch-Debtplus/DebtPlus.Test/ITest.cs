﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DebtPlus.Test
{
    public interface ITest
    {
        void Setup();
        void Execute();
        void Cleanup();
    }
}
