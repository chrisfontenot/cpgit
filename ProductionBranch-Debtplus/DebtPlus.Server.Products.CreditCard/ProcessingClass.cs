﻿using DebtPlus.LINQ;
using DebtPlus.Svc.Payment;
using log4net;
using MMI.Payment;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.Linq;
using DebtPlus.Svc.Payment;
using System.Reflection;

namespace DebtPlus.Server.Products.CreditCard
{
    public class ProcessingClass : IDisposable
    {
        protected static readonly ILog Log = LogManager.GetLogger(MethodBase.GetCurrentMethod().DeclaringType);

        /// <summary>
        /// Process the main entry to the "program.
        /// </summary>
        /// <param name="args"></param>
        /// <returns></returns>
        public Int32 Main(string[] args)
        {
            try
            {
                Log.Debug("Creating database connection");
                // Connection string to the database
                var connectionString = System.Configuration.ConfigurationManager.ConnectionStrings["DebtPlus"].ConnectionString;
                using (var cn = new System.Data.SqlClient.SqlConnection(connectionString))
                {
                    cn.Open();
                    using (var bc = new LINQ.DebtPlusDataContext(cn))
                    {
                        // Find the value for "tomorrow's" date
                        var tomorrow = DateTime.Now;
                        var debugDate = System.Configuration.ConfigurationManager.AppSettings["DEBUG_DATE"];
                        if (!string.IsNullOrEmpty(debugDate))
                        {
                            tomorrow = DateTime.Parse(debugDate);
                        }
                        tomorrow = tomorrow.Date.AddDays(1);
                        Log.DebugFormat("Cutoff date is {0:d}", tomorrow);

                        // List of states and the abbreviations
                        var stateList = bc.states.ToList();

                        // List of responses
                        var colResponses = (from cp in bc.client_products
                                            join v in bc.vendors on cp.vendor equals v.Id
                                            join pr in bc.products on cp.product equals pr.Id
                                            join vp in bc.vendor_products on cp.vendor equals vp.vendor
                                            where cp.product == vp.product
                                               && cp.invoice_status < 2
                                               && cp.invoice_date < tomorrow
                                               && cp.cost - cp.discount - cp.tendered - cp.disputed_amount > 0M
                                               && !v.SupressInvoices
                                               && v.BillingMode == "CreditCard"
                                               && vp.escrowProBono == 1
                                               && v.ActiveFlag
                                            select new ProductReferenceClass(cp, v, vp, pr)).ToList();

                        Log.DebugFormat("Found {0:n0} items to be processed", colResponses.Count);

                        // If there are no items then just leave without doing anything.
                        if (colResponses.Count == 0)
                        {
                            return 0;
                        }

                        // Find the billing contact information
                        var mainContact = bc.vendor_contact_types.FirstOrDefault(s => s.description.ToUpper() == "MAIN");
                        var billingContact = bc.vendor_contact_types.FirstOrDefault(s => s.description.ToUpper() == "INVOICING");
                        
                        // Get List of all the vendor IDs
                        var vendorList = colResponses.Select(res => res.cp.vendor).ToList();

                        // Get disctinct list of vendors
                        var uniqueVendors = vendorList.Distinct().ToList();

                        Log.DebugFormat("Unique Vendor Count : {0}", uniqueVendors.Count);

                        foreach (var vendorID in uniqueVendors)
                        {
                            string vendorEmail = null;
                            address vendorAddress = null;
                            vendor_contact vendorContact = null;
                            Name vendorName = null;

                            Log.DebugFormat("-- Processing VendorID: {0}", vendorID);

                            var vendor = bc.vendors.Where(v => v.Id == vendorID).FirstOrDefault();

                            SetVendorContact(bc, vendorID, billingContact, mainContact, out vendorContact, out vendorEmail);

                            if (vendorContact != null)
                            {
                                Log.DebugFormat("-- Scanning address for vendor '{0}'", vendorID);
                                vendorAddress = bc.addresses.FirstOrDefault(s => s.Id == vendorContact.addressID);
                                vendorName = bc.Names.FirstOrDefault(s => s.Id == vendorContact.nameID);
                            }

                            var vendorClients = colResponses.Where(s => s.cp.vendor == vendorID).ToList();

                            var vendorBalance = vendorClients.Sum(s => s.cp.balance);

                            // Generate the payment information for the credit card
                            var cardInfo = GetVendorCreditCardDetails(vendor, vendorAddress, stateList, vendorName, vendorEmail);

                            var pmtInfo = new Payment()
                            {
                                Amount = vendorBalance,
                                InvoiceNumber = string.Format("{0}{1}", vendorID, DateTime.Now.ToString("yyyyMMddHHMMss")),
                                SettlementDate = DateTime.Now.Date.AddDays(1)
                            };

                            var productReference = new ProductReferenceClass(null, vendor, null, null);
                            productReference.emailAddress = vendorEmail;
                            productReference.n = vendorName;

                            Log.DebugFormat("Sending payment request for card {0} in amount of {1:c} for vendor {2}", cardInfo.CardNumber, pmtInfo.Amount, vendorID);

                            var response = FirstData.Submit(cardInfo, pmtInfo);

                            if (response.StatusCode == MMI.Payment.Status.Code.SUCCESS)
                            {
                                Log.DebugFormat("Submission to FirstData Successful for Vendor : {0}", vendorID);
                                Log.DebugFormat("SUCCESS payment. Transaction ID = '{0}'", ((FirstData.SuccessResponse)response).TransactionID);

                                // Update the payment info with the success result
                                pmtInfo.ResponseMessage = "";
                                pmtInfo.ResponseStatus = response.StatusCode.ToString();
                                pmtInfo.Confirmation = ((FirstData.SuccessResponse)response).TransactionID;

                                foreach (var item in vendorClients)
                                {
                                    var clientProduct = bc.client_products.Where(c => c.Id == item.cp.Id).FirstOrDefault();

                                    decimal clientBalance = 0;
                                    if (clientProduct != null)
                                    {
                                        clientProduct.invoice_status = 3;
                                        // Set Client balance value to be inserted in the product transaction and credit card payment detail table
                                        // The clientProduct.balance is set to 0 as soon as the value for clientProduct.tenedred is set.
                                        clientBalance = clientProduct.balance;

                                        clientProduct.tendered += clientProduct.balance;
                                    }

                                    // Update the transaction log entry to show that we paid the amount now
                                    var qRegister = CreateProductTransaction(clientProduct.Id, clientBalance, vendorID, ((FirstData.SuccessResponse)response).ReferenceCode, clientProduct.product);
                                    Log.Debug("Create Product Transaction Entry for Client Product :" + clientProduct.Id);
                                    bc.product_transactions.InsertOnSubmit(qRegister);

                                    // Generate the entry for the transaction log
                                    var logEntry = CreateCreditCardPaymentDetail(SubmissionBatch.BatchID, SubmissionBatch.SubmissionDate, clientProduct, pmtInfo, cardInfo, clientBalance);
                                    Log.Debug("Create Credit Card Payment Entry for Client Product :" + clientProduct.Id);
                                    bc.credit_card_payment_details.InsertOnSubmit(logEntry);
                                }
                                Log.DebugFormat("Sending Success Email for Vendor : {0}", vendorID);
                                SendEmailSuccess(bc, vendorBalance, productReference);

                                bc.SubmitChanges();
                            }
                            else
                            {
                                decimal amountError = 0M;
                                Log.DebugFormat("Submission to FirstData Failed for Vendor : {0}", vendorID);
                                Log.DebugFormat("FAILURE payment. Status = '{0}', Message = '{1}'", ((FirstData.FailResponse)response).StatusCode.ToString(), ((FirstData.FailResponse)response).Message);
                                pmtInfo.ResponseMessage = ((FirstData.FailResponse)response).Message;
                                pmtInfo.ResponseStatus = ((FirstData.FailResponse)response).StatusCode.ToString();

                                Status.Code statusCode = ((FirstData.FailResponse)response).StatusCode;
                                // Update the message text with the failure reason
                                if (statusCode == MMI.Payment.Status.Code.CARD_EXPIRED ||
                                    statusCode == MMI.Payment.Status.Code.INVALID_CUSTOMER_ACCOUNT ||
                                    statusCode == MMI.Payment.Status.Code.INVALID_PARAMETER ||
                                    statusCode == MMI.Payment.Status.Code.PAYMENT_DECLINED)
                                {
                                    amountError = vendorBalance;
                                }

                                foreach (var item in vendorClients)
                                {
                                    var clientProduct = bc.client_products.Where(c => c.Id == item.cp.Id).FirstOrDefault();

                                    if (clientProduct != null)
                                    {
                                        clientProduct.invoice_date = tomorrow.AddDays(13);

                                        // Generate the entry for the transaction log
                                        var logEntry = CreateCreditCardPaymentDetail(SubmissionBatch.BatchID, SubmissionBatch.SubmissionDate, clientProduct, pmtInfo, cardInfo, clientProduct.balance);
                                        Log.Debug("Create Credit Card Payment Entry for Client Product :" + clientProduct.Id);
                                        bc.credit_card_payment_details.InsertOnSubmit(logEntry);
                                    }
                                }
                                Log.Debug("Sending Error Email for vendor: " + vendorID);
                                SendEmailError(bc, amountError, productReference);

                                Log.Debug("Submitting error details to the database for vendor: " + vendorID);
                                bc.SubmitChanges();
                            }
                        }
                    }
                }
                Log.Debug("Successful execution");
                return 0;
            }
            catch (Exception ex)
            {
                Log.DebugFormat("EXCEPTION occured. Type='{0}', Message = '{1}'", ex.GetType(), ex.Message);
                Console.WriteLine();
                Console.WriteLine("Exception occurred: {0}", ex.Message);
                Log.Debug("Fatal error execution");
                return 2;
            }
        }

        private credit_card_payment_detail CreateCreditCardPaymentDetail(Guid batchID, DateTime? submissionDate, client_product clientProduct, Svc.Payment.Payment pmtInfo, Svc.Payment.CreditCard cardInfo, decimal clientBalance)
        {
            var logEntry = LINQ.Factory.Manufacture_credit_card_payment_detail();

            logEntry.submission_batch = batchID;
            logEntry.submission_date = submissionDate;
            logEntry.client_product = clientProduct.Id;
            
            logEntry.amount = clientBalance;
            logEntry.confirmation_number = pmtInfo.Confirmation;
            logEntry.result_message = pmtInfo.ResponseMessage;
            logEntry.return_code = pmtInfo.ResponseStatus;
            logEntry.settlement_submission_date = pmtInfo.SettlementDate;

            logEntry.account_number = cardInfo.CardNumber;
            logEntry.avs_address = cardInfo.Address;
            logEntry.avs_city = cardInfo.City;
            logEntry.avs_full_name = cardInfo.FullName;
            logEntry.avs_state = cardInfo.State;
            logEntry.avs_zipcode = cardInfo.Zipcode;
            logEntry.credit_card_type = cardInfo.CardStatusType;
            logEntry.cvv = cardInfo.CVV;
            logEntry.payment_medium = cardInfo.PaymentMedium;

            return logEntry;
        }

        private product_transaction CreateProductTransaction(int clientProductID, decimal amount, int vendorID, string referenceCode, int productID)
        {
            var trans = LINQ.Factory.Manufacture_product_transaction();
            trans.client_product = clientProductID;
            trans.payment = amount;
            trans.transaction_type = "RC";
            trans.vendor = vendorID;
            trans.product = productID;
            trans.note = string.Format("Confirmation:{0}", referenceCode);
            return trans;
        }

        private Svc.Payment.CreditCard GetVendorCreditCardDetails(vendor vendor, address vendorAddress, List<state> stateList, Name vendorName, string vendorEmail)
        {
            var cardInfo = new Svc.Payment.CreditCard()
            {
                Id = string.Format("BKAttorney {0:0000}", vendor.Id),
                Address = vendorAddress.AddressLine1,
                State = stateList.Find(s => s.Id == vendorAddress.state).MailingCode,
                City = vendorAddress.city,
                Zipcode = vendorAddress.PostalCode,
                Expiration_Month = vendor.CC_ExpirationDate.GetValueOrDefault().Month,
                Expiration_Year = vendor.CC_ExpirationDate.GetValueOrDefault().Year,
                CardNumber = vendor.ACH_AccountNumber,
                CVV = vendor.CC_CVV,
                FirstName = vendorName.First,
                LastName = vendorName.Last,
                CardStatusType = "WEB",
                CreditCardType = null,
                PaymentMedium = "Credit Card",
                EmailAddress = vendorEmail
            };
            return cardInfo;
        }

        private void SetVendorContact(DebtPlusDataContext bc, int vendorID, vendor_contact_type billing, vendor_contact_type main, out vendor_contact vendorContact, out string vendorEmail)
        {
            vendorContact = null;
            vendorEmail = null;

            Log.DebugFormat("-- Scanning email address for vendor '{0}'", vendorID);
            if (billing != null)
            {
                var q = (from vc in bc.vendor_contacts
                         join em in bc.EmailAddresses on vc.emailID equals em.Id
                         where vc.vendor == vendorID && vc.contact_type == billing.Id
                         select new { vc, em }).FirstOrDefault();

                if (q != null)
                {
                    vendorContact = q.vc;
                    vendorEmail = q.em.Address;
                }

                if (!string.IsNullOrWhiteSpace(vendorEmail))
                {
                    //Log.Debug("Ignored because billing email address is missing for vendor " + vendorID);
                    return;
                }
            }

            if (main != null)
            {
                var q = (from vc in bc.vendor_contacts
                         join em in bc.EmailAddresses on vc.emailID equals em.Id
                         where vc.vendor == vendorID && vc.contact_type == main.Id
                         select new { vc, em }).FirstOrDefault();

                if (q != null)
                {
                    vendorContact = q.vc;
                    vendorEmail = q.em.Address;
                }

                if (string.IsNullOrWhiteSpace(vendorEmail))
                {
                    Log.Debug("Ignored because main contact email address is missing for vendor " + vendorID);
                }
            }
        }

        /// <summary>
        /// Send the email message out with the failure condition
        /// </summary>
        protected void SendEmailError(LINQ.DebtPlusDataContext bc, decimal amountError, ProductReferenceClass target)
        {
            SendEmail(bc, amountError, target, "Credit Card Failure");
        }

        /// <summary>
        /// Send the email message out with the success condition
        /// </summary>
        protected void SendEmailSuccess(LINQ.DebtPlusDataContext bc, decimal amountSuccess, ProductReferenceClass target)
        {
            SendEmail(bc, amountSuccess, target, "Credit Card Success");
        }

        protected void SendEmail(LINQ.DebtPlusDataContext bc, decimal amountValue, ProductReferenceClass target, string templateName)
        {
            var qTemplate = bc.email_templates.FirstOrDefault(s => s.description == templateName);
            if (qTemplate == null)
            {
                Log.DebugFormat("Message Type '{0}' was not found", templateName);
                return;
            }

            var sb = new System.Text.StringBuilder();

            sb.Append("<substitutions>");
            sb.AppendFormat("<substitution><field>{0}</field><value>{1:c}</value></substitution>", "amount", amountValue);
            sb.AppendFormat("<substitution><field>{0}</field><value>{1}</value></substitution>", "card", ToXml(Last4(target.v.ACH_AccountNumber)));
            sb.AppendFormat("<substitution><field>{0}</field><value>{1}</value></substitution>", "name", ToXml(target.v.Name));
            sb.AppendFormat("<substitution><field>{0}</field><value>{1}</value></substitution>", "owner", ToXml(target.v.NameOnCard));
            sb.AppendFormat("<substitution><field>{0}</field><value>{1}</value></substitution>", "date", DateTime.Now.ToShortDateString());
            sb.Append("</substitutions>");

            Log.DebugFormat("Message Substition string is '{0}'", sb);

            var qQueue = LINQ.Factory.Manufacture_email_queue();
            qQueue.template = qTemplate.Id;
            qQueue.email_address = target.emailAddress;
            qQueue.email_name = target.n.ToString();
            qQueue.substitutions = System.Xml.Linq.XElement.Parse(sb.ToString());

            Log.DebugFormat("Email message of type '{0}' generated for {1:c}", templateName, amountValue);
            bc.email_queues.InsertOnSubmit(qQueue);
        }

        private static string ToXml(string input)
        {
            if (string.IsNullOrEmpty(input))
            {
                return string.Empty;
            }

            var sb = new System.Text.StringBuilder(input);

            sb.Replace("&", "&amp;");
            sb.Replace("<", "&lt;");
            sb.Replace(">", "&gt;");

            return sb.ToString();
        }

        private static string Last4(string input)
        {
            if (!string.IsNullOrEmpty(input))
            {
                if (input.Length > 4)
                {
                    return input.Substring(input.Length - 4);
                }
            }
            return input;
        }

        /// <summary>
        /// Process the disposal of the storage items
        /// </summary>
        /// <param name="disposing"></param>
        protected virtual void Dispose(bool disposing)
        {
        }

        /// <summary>
        /// Orderly disposal of allocated storage
        /// </summary>
        public void Dispose()
        {
            try
            {
                Dispose(true);
            }
            finally
            {
                GC.SuppressFinalize(this);
            }
        }

        /// <summary>
        /// Handle the finalize event for the disposal of storage
        /// </summary>
        ~ProcessingClass()
        {
            try
            {
                Dispose(false);
            }
            catch
            {
                // ignored
            }
        }
    }
}
