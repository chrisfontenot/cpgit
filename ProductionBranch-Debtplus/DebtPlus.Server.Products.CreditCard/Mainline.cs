﻿using System;

// Required for the logger information
[assembly: log4net.Config.XmlConfigurator(Watch = true)]

namespace DebtPlus.Server.Products.CreditCard
{
    static class Mainline
    {
        static Int32 Main(string[] args)
        {
            using (var processing = new DebtPlus.Server.Products.CreditCard.ProcessingClass())
            {
                return processing.Main(args);
            }
        }
    }
}
