﻿using System;

namespace DebtPlus.Server.Products.CreditCard
{
    public class ProductReferenceClass : IDisposable
    {
        public ProductReferenceClass()
            : base()
        {
            cp           = null;
            v            = null;
            vp           = null;
            pr           = null;
            vc           = null;
            n            = null;
            a            = null;
            emailAddress = null;
        }

        public DebtPlus.LINQ.client_product cp { get; private set; }
        public DebtPlus.LINQ.vendor v { get; private set; }
        public DebtPlus.LINQ.vendor_product vp { get; private set; }
        public DebtPlus.LINQ.product pr { get; private set; }
        public DebtPlus.LINQ.vendor_contact vc { get; set; }
        public DebtPlus.LINQ.Name n { get; set; }
        public DebtPlus.LINQ.address a { get; set; }
        public string emailAddress { get; set; }

        private System.Nullable<int> _client_product { get; set; }
        private System.Nullable<int> _client { get; set; }

        public ProductReferenceClass(DebtPlus.LINQ.client_product cp, DebtPlus.LINQ.vendor v, DebtPlus.LINQ.vendor_product vp, DebtPlus.LINQ.product pr)
            : this()
        {
            this.cp = cp;
            this.v = v;
            this.vp = vp;
            this.pr = pr;
        }

        // Information to record the data for the request
        private System.Xml.Linq.XElement _request { get; set; }
        private System.Xml.Linq.XElement _response { get; set; }

        // Storage Management
        ~ProductReferenceClass()
        {
            Dispose(false);
        }

        public void Dispose()
        {
            try
            {
                Dispose(true);
            }
            finally
            {
                GC.SuppressFinalize(this);
            }
        }

        protected virtual void Dispose(bool Disposing)
        {
        }
    }
}
