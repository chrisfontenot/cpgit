using System;
using DebtPlus.LINQ;
using DebtPlus.UI.Vendor.Update.Controls;

namespace DebtPlus.UI.Vendor.Update.Pages
{
    internal partial class Page_Statistics : ControlBase
    {
        // Current creditor record being updated
        private vendor vendorRecord = null;

        public Page_Statistics() : base()
        {
            InitializeComponent();
            Resize += new EventHandler(Page_Statistics_Resize);
        }

        /// <summary>
        /// Handle the resize event to center the table items
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Page_Statistics_Resize(object sender, System.EventArgs e)
        {
            try
            {
                emptySpaceItem_Left.Width = (emptySpaceItem_Left.Width + emptySpaceItem_Right.Width) >> 1;
            }
            catch
            {
            }
        }

        internal override void ReadForm(DebtPlus.LINQ.BusinessContext bc, DebtPlus.LINQ.vendor VendorRecord)
        {
            this.vendorRecord = VendorRecord;

            // Format the statistic information
            LabelControl_st_DisputeCnt.Text            = string.Format("{0:n0}", vendorRecord.st_DisputeCnt);
            LabelControl_st_ClientCountTotalCount.Text = string.Format("{0:n0}", vendorRecord.st_ClientCountTotalCount);
            LabelControl_st_ProductsOfferedCount.Text  = string.Format("{0:n0}", vendorRecord.st_ProductsOfferedCount);
            LabelControl_st_BillingAdjustmentsAmt.Text = string.Format("{0:c}", vendorRecord.st_BillingAdjustmentsAmt);
            LabelControl_st_ProductsSoldCnt.Text       = string.Format("{0:n0}", vendorRecord.st_ProductsSoldCnt);
            LabelControl_st_PaymentsReceivedAmt.Text   = string.Format("{0:c}", vendorRecord.st_PaymentsReceivedAmt);
        }

        //Required by the Windows Form Designer
        private DevExpress.XtraEditors.LabelControl LabelControl_st_DisputeCnt;
        private DevExpress.XtraEditors.LabelControl LabelControl_st_ClientCountTotalCount;
        private DevExpress.XtraEditors.LabelControl LabelControl_st_ProductsOfferedCount;
        private DevExpress.XtraEditors.LabelControl LabelControl_st_BillingAdjustmentsAmt;
        private DevExpress.XtraEditors.LabelControl LabelControl_st_ProductsSoldCnt;
        private DevExpress.XtraEditors.LabelControl LabelControl_st_PaymentsReceivedAmt;
        private DevExpress.XtraEditors.LabelControl LabelControl7;
        private DevExpress.XtraLayout.LayoutControl LayoutControl1;
        private DevExpress.XtraLayout.LayoutControlGroup LayoutControlGroup1;
        private DevExpress.XtraLayout.LayoutControlItem LayoutControlItem2;
        private DevExpress.XtraLayout.LayoutControlItem LayoutControlItem3;
        private DevExpress.XtraLayout.LayoutControlItem LayoutControlItem4;
        private DevExpress.XtraLayout.LayoutControlItem LayoutControlItem5;
        private DevExpress.XtraLayout.LayoutControlItem LayoutControlItem6;
        private DevExpress.XtraLayout.LayoutControlItem LayoutControlItem7;
        private DevExpress.XtraLayout.LayoutControlItem LayoutControlItem8;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem_Left;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem_Right;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem3;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem4;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup2;

        private System.ComponentModel.IContainer components = null;

        //NOTE: The following procedure is required by the Windows Form Designer
        //It can be modified using the Windows Form Designer.
        //Do not modify it using the code editor.

        //Form overrides dispose to clean up the component list.
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                if (components != null)
                {
                    components.Dispose();
                }
            }
            base.Dispose(disposing);
        }

        [System.Diagnostics.DebuggerStepThrough()]
        private void InitializeComponent()
        {
            this.LabelControl7 = new DevExpress.XtraEditors.LabelControl();
            this.LayoutControl1 = new DevExpress.XtraLayout.LayoutControl();
            this.LabelControl_st_ProductsOfferedCount = new DevExpress.XtraEditors.LabelControl();
            this.LabelControl_st_ClientCountTotalCount = new DevExpress.XtraEditors.LabelControl();
            this.LabelControl_st_ProductsSoldCnt = new DevExpress.XtraEditors.LabelControl();
            this.LabelControl_st_PaymentsReceivedAmt = new DevExpress.XtraEditors.LabelControl();
            this.LabelControl_st_DisputeCnt = new DevExpress.XtraEditors.LabelControl();
            this.LabelControl_st_BillingAdjustmentsAmt = new DevExpress.XtraEditors.LabelControl();
            this.LayoutControlGroup1 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.LayoutControlItem8 = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem_Left = new DevExpress.XtraLayout.EmptySpaceItem();
            this.emptySpaceItem_Right = new DevExpress.XtraLayout.EmptySpaceItem();
            this.emptySpaceItem3 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.emptySpaceItem4 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.layoutControlGroup2 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.LayoutControlItem2 = new DevExpress.XtraLayout.LayoutControlItem();
            this.LayoutControlItem3 = new DevExpress.XtraLayout.LayoutControlItem();
            this.LayoutControlItem4 = new DevExpress.XtraLayout.LayoutControlItem();
            this.LayoutControlItem5 = new DevExpress.XtraLayout.LayoutControlItem();
            this.LayoutControlItem6 = new DevExpress.XtraLayout.LayoutControlItem();
            this.LayoutControlItem7 = new DevExpress.XtraLayout.LayoutControlItem();
            ((System.ComponentModel.ISupportInitialize)(this.LayoutControl1)).BeginInit();
            this.LayoutControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.LayoutControlGroup1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LayoutControlItem8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem_Left)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem_Right)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LayoutControlItem2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LayoutControlItem3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LayoutControlItem4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LayoutControlItem5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LayoutControlItem6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LayoutControlItem7)).BeginInit();
            this.SuspendLayout();
            // 
            // LabelControl7
            // 
            this.LabelControl7.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.LabelControl7.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.Horizontal;
            this.LabelControl7.Location = new System.Drawing.Point(86, 12);
            this.LabelControl7.Name = "LabelControl7";
            this.LabelControl7.Size = new System.Drawing.Size(404, 19);
            this.LabelControl7.StyleController = this.LayoutControl1;
            this.LabelControl7.TabIndex = 6;
            this.LabelControl7.Text = "Creditor Statistical Information For Active Clients";
            // 
            // LayoutControl1
            // 
            this.LayoutControl1.Controls.Add(this.LabelControl7);
            this.LayoutControl1.Controls.Add(this.LabelControl_st_ProductsOfferedCount);
            this.LayoutControl1.Controls.Add(this.LabelControl_st_ClientCountTotalCount);
            this.LayoutControl1.Controls.Add(this.LabelControl_st_ProductsSoldCnt);
            this.LayoutControl1.Controls.Add(this.LabelControl_st_PaymentsReceivedAmt);
            this.LayoutControl1.Controls.Add(this.LabelControl_st_DisputeCnt);
            this.LayoutControl1.Controls.Add(this.LabelControl_st_BillingAdjustmentsAmt);
            this.LayoutControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.LayoutControl1.Location = new System.Drawing.Point(0, 0);
            this.LayoutControl1.Name = "LayoutControl1";
            this.LayoutControl1.OptionsCustomizationForm.DesignTimeCustomizationFormPositionAndSize = new System.Drawing.Rectangle(448, 25, 450, 350);
            this.LayoutControl1.OptionsCustomizationForm.ShowPropertyGrid = true;
            this.LayoutControl1.Root = this.LayoutControlGroup1;
            this.LayoutControl1.Size = new System.Drawing.Size(576, 232);
            this.LayoutControl1.TabIndex = 14;
            this.LayoutControl1.Text = "LayoutControl1";
            // 
            // LabelControl_st_ProductsOfferedCount
            // 
            this.LabelControl_st_ProductsOfferedCount.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.LabelControl_st_ProductsOfferedCount.Location = new System.Drawing.Point(381, 88);
            this.LabelControl_st_ProductsOfferedCount.Name = "LabelControl_st_ProductsOfferedCount";
            this.LabelControl_st_ProductsOfferedCount.Size = new System.Drawing.Size(6, 13);
            this.LabelControl_st_ProductsOfferedCount.StyleController = this.LayoutControl1;
            this.LabelControl_st_ProductsOfferedCount.TabIndex = 7;
            this.LabelControl_st_ProductsOfferedCount.Text = "0";
            // 
            // LabelControl_st_ClientCountTotalCount
            // 
            this.LabelControl_st_ClientCountTotalCount.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.LabelControl_st_ClientCountTotalCount.Location = new System.Drawing.Point(381, 105);
            this.LabelControl_st_ClientCountTotalCount.Name = "LabelControl_st_ClientCountTotalCount";
            this.LabelControl_st_ClientCountTotalCount.Size = new System.Drawing.Size(6, 13);
            this.LabelControl_st_ClientCountTotalCount.StyleController = this.LayoutControl1;
            this.LabelControl_st_ClientCountTotalCount.TabIndex = 8;
            this.LabelControl_st_ClientCountTotalCount.Text = "0";
            // 
            // LabelControl_st_ProductsSoldCnt
            // 
            this.LabelControl_st_ProductsSoldCnt.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.LabelControl_st_ProductsSoldCnt.Location = new System.Drawing.Point(381, 122);
            this.LabelControl_st_ProductsSoldCnt.Name = "LabelControl_st_ProductsSoldCnt";
            this.LabelControl_st_ProductsSoldCnt.Size = new System.Drawing.Size(28, 13);
            this.LabelControl_st_ProductsSoldCnt.StyleController = this.LayoutControl1;
            this.LabelControl_st_ProductsSoldCnt.TabIndex = 9;
            this.LabelControl_st_ProductsSoldCnt.Text = "$0.00";
            // 
            // LabelControl_st_PaymentsReceivedAmt
            // 
            this.LabelControl_st_PaymentsReceivedAmt.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.LabelControl_st_PaymentsReceivedAmt.Location = new System.Drawing.Point(381, 139);
            this.LabelControl_st_PaymentsReceivedAmt.Name = "LabelControl_st_PaymentsReceivedAmt";
            this.LabelControl_st_PaymentsReceivedAmt.Size = new System.Drawing.Size(6, 13);
            this.LabelControl_st_PaymentsReceivedAmt.StyleController = this.LayoutControl1;
            this.LabelControl_st_PaymentsReceivedAmt.TabIndex = 10;
            this.LabelControl_st_PaymentsReceivedAmt.Text = "0";
            // 
            // LabelControl_st_DisputeCnt
            // 
            this.LabelControl_st_DisputeCnt.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.LabelControl_st_DisputeCnt.Location = new System.Drawing.Point(381, 156);
            this.LabelControl_st_DisputeCnt.Name = "LabelControl_st_DisputeCnt";
            this.LabelControl_st_DisputeCnt.Size = new System.Drawing.Size(28, 13);
            this.LabelControl_st_DisputeCnt.StyleController = this.LayoutControl1;
            this.LabelControl_st_DisputeCnt.TabIndex = 11;
            this.LabelControl_st_DisputeCnt.Text = "$0.00";
            // 
            // LabelControl_st_BillingAdjustmentsAmt
            // 
            this.LabelControl_st_BillingAdjustmentsAmt.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.LabelControl_st_BillingAdjustmentsAmt.Location = new System.Drawing.Point(381, 173);
            this.LabelControl_st_BillingAdjustmentsAmt.Name = "LabelControl_st_BillingAdjustmentsAmt";
            this.LabelControl_st_BillingAdjustmentsAmt.Size = new System.Drawing.Size(28, 13);
            this.LabelControl_st_BillingAdjustmentsAmt.StyleController = this.LayoutControl1;
            this.LabelControl_st_BillingAdjustmentsAmt.TabIndex = 12;
            this.LabelControl_st_BillingAdjustmentsAmt.Text = "$0.00";
            // 
            // LayoutControlGroup1
            // 
            this.LayoutControlGroup1.CustomizationFormText = "LayoutControlGroup1";
            this.LayoutControlGroup1.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.True;
            this.LayoutControlGroup1.GroupBordersVisible = false;
            this.LayoutControlGroup1.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.LayoutControlItem8,
            this.emptySpaceItem_Left,
            this.emptySpaceItem_Right,
            this.emptySpaceItem3,
            this.emptySpaceItem4,
            this.layoutControlGroup2});
            this.LayoutControlGroup1.Location = new System.Drawing.Point(0, 0);
            this.LayoutControlGroup1.Name = "Root";
            this.LayoutControlGroup1.Size = new System.Drawing.Size(576, 232);
            this.LayoutControlGroup1.Text = "Root";
            this.LayoutControlGroup1.TextVisible = false;
            // 
            // LayoutControlItem8
            // 
            this.LayoutControlItem8.AppearanceItemCaption.Options.UseTextOptions = true;
            this.LayoutControlItem8.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.LayoutControlItem8.AppearanceItemCaption.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.LayoutControlItem8.Control = this.LabelControl7;
            this.LayoutControlItem8.ControlAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            this.LayoutControlItem8.CustomizationFormText = "LayoutControlItem8";
            this.LayoutControlItem8.Location = new System.Drawing.Point(0, 0);
            this.LayoutControlItem8.Name = "LayoutControlItem8";
            this.LayoutControlItem8.Size = new System.Drawing.Size(556, 23);
            this.LayoutControlItem8.Text = "LayoutControlItem8";
            this.LayoutControlItem8.TextSize = new System.Drawing.Size(0, 0);
            this.LayoutControlItem8.TextToControlDistance = 0;
            this.LayoutControlItem8.TextVisible = false;
            // 
            // emptySpaceItem_Left
            // 
            this.emptySpaceItem_Left.AllowHotTrack = false;
            this.emptySpaceItem_Left.CustomizationFormText = "emptySpaceItem_Left";
            this.emptySpaceItem_Left.Location = new System.Drawing.Point(0, 45);
            this.emptySpaceItem_Left.Name = "emptySpaceItem_Left";
            this.emptySpaceItem_Left.Size = new System.Drawing.Size(164, 145);
            this.emptySpaceItem_Left.Text = "emptySpaceItem_Left";
            this.emptySpaceItem_Left.TextSize = new System.Drawing.Size(0, 0);
            // 
            // emptySpaceItem_Right
            // 
            this.emptySpaceItem_Right.AllowHotTrack = false;
            this.emptySpaceItem_Right.CustomizationFormText = "emptySpaceItem_Right";
            this.emptySpaceItem_Right.Location = new System.Drawing.Point(413, 45);
            this.emptySpaceItem_Right.Name = "emptySpaceItem_Right";
            this.emptySpaceItem_Right.Size = new System.Drawing.Size(143, 145);
            this.emptySpaceItem_Right.Text = "emptySpaceItem_Right";
            this.emptySpaceItem_Right.TextSize = new System.Drawing.Size(0, 0);
            // 
            // emptySpaceItem3
            // 
            this.emptySpaceItem3.AllowHotTrack = false;
            this.emptySpaceItem3.CustomizationFormText = "emptySpaceItem3";
            this.emptySpaceItem3.Location = new System.Drawing.Point(0, 23);
            this.emptySpaceItem3.MaxSize = new System.Drawing.Size(0, 22);
            this.emptySpaceItem3.MinSize = new System.Drawing.Size(10, 22);
            this.emptySpaceItem3.Name = "emptySpaceItem3";
            this.emptySpaceItem3.Size = new System.Drawing.Size(556, 22);
            this.emptySpaceItem3.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.emptySpaceItem3.Text = "emptySpaceItem3";
            this.emptySpaceItem3.TextSize = new System.Drawing.Size(0, 0);
            // 
            // emptySpaceItem4
            // 
            this.emptySpaceItem4.AllowHotTrack = false;
            this.emptySpaceItem4.CustomizationFormText = "emptySpaceItem4";
            this.emptySpaceItem4.Location = new System.Drawing.Point(0, 190);
            this.emptySpaceItem4.Name = "emptySpaceItem4";
            this.emptySpaceItem4.Size = new System.Drawing.Size(556, 22);
            this.emptySpaceItem4.Text = "emptySpaceItem4";
            this.emptySpaceItem4.TextSize = new System.Drawing.Size(0, 0);
            // 
            // layoutControlGroup2
            // 
            this.layoutControlGroup2.AppearanceGroup.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.layoutControlGroup2.AppearanceGroup.Options.UseFont = true;
            this.layoutControlGroup2.CustomizationFormText = "Billing Stats";
            this.layoutControlGroup2.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.LayoutControlItem2,
            this.LayoutControlItem3,
            this.LayoutControlItem4,
            this.LayoutControlItem5,
            this.LayoutControlItem6,
            this.LayoutControlItem7});
            this.layoutControlGroup2.Location = new System.Drawing.Point(164, 45);
            this.layoutControlGroup2.Name = "layoutControlGroup2";
            this.layoutControlGroup2.Size = new System.Drawing.Size(249, 145);
            this.layoutControlGroup2.Text = "Billing Stats";
            // 
            // LayoutControlItem2
            // 
            this.LayoutControlItem2.Control = this.LabelControl_st_BillingAdjustmentsAmt;
            this.LayoutControlItem2.CustomizationFormText = "TotalAmountOwed";
            this.LayoutControlItem2.Location = new System.Drawing.Point(0, 85);
            this.LayoutControlItem2.Name = "LayoutControlItem2";
            this.LayoutControlItem2.Size = new System.Drawing.Size(225, 17);
            this.LayoutControlItem2.Text = "Billing Adjustments(Total)";
            this.LayoutControlItem2.TextSize = new System.Drawing.Size(190, 13);
            // 
            // LayoutControlItem3
            // 
            this.LayoutControlItem3.Control = this.LabelControl_st_DisputeCnt;
            this.LayoutControlItem3.CustomizationFormText = "Average Disbursement Factor";
            this.LayoutControlItem3.Location = new System.Drawing.Point(0, 68);
            this.LayoutControlItem3.Name = "LayoutControlItem3";
            this.LayoutControlItem3.Size = new System.Drawing.Size(225, 17);
            this.LayoutControlItem3.Text = "Number Of Disputes(Total)";
            this.LayoutControlItem3.TextSize = new System.Drawing.Size(190, 13);
            // 
            // LayoutControlItem4
            // 
            this.LayoutControlItem4.Control = this.LabelControl_st_PaymentsReceivedAmt;
            this.LayoutControlItem4.CustomizationFormText = "Zero Balance Debts";
            this.LayoutControlItem4.Location = new System.Drawing.Point(0, 51);
            this.LayoutControlItem4.Name = "LayoutControlItem4";
            this.LayoutControlItem4.Size = new System.Drawing.Size(225, 17);
            this.LayoutControlItem4.Text = "Payments Received(Total)";
            this.LayoutControlItem4.TextSize = new System.Drawing.Size(190, 13);
            // 
            // LayoutControlItem5
            // 
            this.LayoutControlItem5.Control = this.LabelControl_st_ProductsSoldCnt;
            this.LayoutControlItem5.CustomizationFormText = "Total Scheduled Payments";
            this.LayoutControlItem5.Location = new System.Drawing.Point(0, 34);
            this.LayoutControlItem5.Name = "LayoutControlItem5";
            this.LayoutControlItem5.Size = new System.Drawing.Size(225, 17);
            this.LayoutControlItem5.Text = "Count Of Products & Services Sold(Total)";
            this.LayoutControlItem5.TextSize = new System.Drawing.Size(190, 13);
            // 
            // LayoutControlItem6
            // 
            this.LayoutControlItem6.Control = this.LabelControl_st_ClientCountTotalCount;
            this.LayoutControlItem6.CustomizationFormText = "Clients With A Balance";
            this.LayoutControlItem6.Location = new System.Drawing.Point(0, 17);
            this.LayoutControlItem6.Name = "LayoutControlItem6";
            this.LayoutControlItem6.Size = new System.Drawing.Size(225, 17);
            this.LayoutControlItem6.Text = "Count Of Clients(Total)";
            this.LayoutControlItem6.TextSize = new System.Drawing.Size(190, 13);
            // 
            // LayoutControlItem7
            // 
            this.LayoutControlItem7.Control = this.LabelControl_st_ProductsOfferedCount;
            this.LayoutControlItem7.CustomizationFormText = "Client Count";
            this.LayoutControlItem7.Location = new System.Drawing.Point(0, 0);
            this.LayoutControlItem7.Name = "LayoutControlItem7";
            this.LayoutControlItem7.Size = new System.Drawing.Size(225, 17);
            this.LayoutControlItem7.Text = "Products and Services Offered(Total)";
            this.LayoutControlItem7.TextSize = new System.Drawing.Size(190, 13);
            // 
            // Page_Statistics
            // 
            this.Controls.Add(this.LayoutControl1);
            this.Name = "Page_Statistics";
            this.Size = new System.Drawing.Size(576, 232);
            ((System.ComponentModel.ISupportInitialize)(this.LayoutControl1)).EndInit();
            this.LayoutControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.LayoutControlGroup1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LayoutControlItem8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem_Left)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem_Right)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LayoutControlItem2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LayoutControlItem3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LayoutControlItem4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LayoutControlItem5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LayoutControlItem6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LayoutControlItem7)).EndInit();
            this.ResumeLayout(false);

        }
    }
}