#region "Copyright 2000-2012 DebtPlus, L.L.C."

//{*******************************************************************}
//{                                                                   }
//{       DebtPlus Debt Management System                             }
//{                                                                   }
//{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
//{       ALL RIGHTS RESERVED                                         }
//{                                                                   }
//{   The entire contents of this file is protected by U.S. and       }
//{   International Copyright Laws. Unauthorized reproduction,        }
//{   reverse-engineering, and distribution of all or any portion of  }
//{   the code contained in this file is strictly prohibited and may  }
//{   result in severe civil and criminal penalties and will be       }
//{   prosecuted to the maximum extent possible under the law.        }
//{                                                                   }
//{   RESTRICTIONS                                                    }
//{                                                                   }
//{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
//{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
//{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
//{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
//{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
//{                                                                   }
//{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
//{   ADDITIONAL RESTRICTIONS.                                        }
//{                                                                   }
//{*******************************************************************}

#endregion "Copyright 2000-2012 DebtPlus, L.L.C."

using System;
using System.Linq;
using System.Windows.Forms;
using DebtPlus.LINQ;
using DebtPlus.UI.Vendor.Update.Controls;
using DevExpress.XtraEditors;

namespace DebtPlus.UI.Vendor.Update.Pages
{
    internal partial class Page_General : ControlBase
    {
        // Current creditor record being edited with this page
        private vendor vendorRecord = null;

        public Page_General()
            : base()
        {
            InitializeComponent();
            RegisterHandlers();
        }

        private void RegisterHandlers()
        {
        }

        private void UnRegisterHandlers()
        {
        }

        #region " Windows Form Designer generated code "


        private DevExpress.XtraEditors.TextEdit comment;

        private System.ComponentModel.IContainer components = null;

        //private DevExpress.XtraEditors.LabelControl vendor_category;

        private DevExpress.XtraEditors.LabelControl mtd_received;

        private DevExpress.XtraEditors.LabelControl ytd_received;

        private DevExpress.XtraEditors.LabelControl date_created;

        private DevExpress.XtraEditors.LabelControl billed_mtd;

        private DevExpress.XtraEditors.LabelControl billed_ytd;

        //private DevExpress.XtraEditors.LabelControl eft_contribution;

        private DevExpress.XtraEditors.LabelControl fax;

        //Required by the Windows Form Designer
        //NOTE: The following procedure is required by the Windows Form Designer
        //It can be modified using the Windows Form Designer.
        //Do not modify it using the code editor.
        private DevExpress.XtraEditors.LabelControl label_address;

        private DevExpress.XtraLayout.LayoutControl LayoutControl1;

        private DevExpress.XtraLayout.LayoutControlGroup LayoutControlGroup1;

        private DevExpress.XtraLayout.LayoutControlGroup LayoutControlGroup2;

        private DevExpress.XtraLayout.LayoutControlGroup LayoutControlGroup3;

        private DevExpress.XtraLayout.LayoutControlGroup LayoutControlGroup4;

        private DevExpress.XtraLayout.LayoutControlItem LayoutControlItem1;

        private DevExpress.XtraLayout.LayoutControlItem LayoutControlItem12;

        private DevExpress.XtraLayout.LayoutControlItem LayoutControlItem13;

        private DevExpress.XtraLayout.LayoutControlItem LayoutControlItem15;

        private DevExpress.XtraLayout.LayoutControlItem LayoutControlItem16;

        private DevExpress.XtraLayout.LayoutControlItem LayoutControlItem17;

        private DevExpress.XtraLayout.LayoutControlItem LayoutControlItem19;

        private DevExpress.XtraLayout.LayoutControlItem LayoutControlItem2;

        private DevExpress.XtraLayout.LayoutControlItem LayoutControlItem3;

        private DevExpress.XtraLayout.LayoutControlItem LayoutControlItem4;

        //private DevExpress.XtraLayout.LayoutControlItem LayoutControlItem8;

        private DevExpress.XtraEditors.LabelControl phone;

        private DevExpress.XtraEditors.LabelControl returned_mail;
        private LabelControl billing_cycle;
        private LabelControl vendor_category;
        private LabelControl first_billing_date;
        private DevExpress.XtraLayout.LayoutControlItem LayoutControlItem20;
        private DevExpress.XtraLayout.LayoutControlItem LayoutControlItem7;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup5;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem1;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem2;
        private DevExpress.XtraLayout.LayoutControlItem LayoutControlItem21;

        //UserControl overrides dispose to clean up the component list.
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                if (components != null)
                {
                    components.Dispose();
                }
            }
            base.Dispose(disposing);
        }

        [System.Diagnostics.DebuggerStepThrough()]
        private void InitializeComponent()
        {
            this.comment = new DevExpress.XtraEditors.TextEdit();
            this.LayoutControl1 = new DevExpress.XtraLayout.LayoutControl();
            this.billed_mtd = new DevExpress.XtraEditors.LabelControl();
            this.date_created = new DevExpress.XtraEditors.LabelControl();
            this.billed_ytd = new DevExpress.XtraEditors.LabelControl();
            this.mtd_received = new DevExpress.XtraEditors.LabelControl();
            this.returned_mail = new DevExpress.XtraEditors.LabelControl();
            this.fax = new DevExpress.XtraEditors.LabelControl();
            this.label_address = new DevExpress.XtraEditors.LabelControl();
            this.phone = new DevExpress.XtraEditors.LabelControl();
            this.ytd_received = new DevExpress.XtraEditors.LabelControl();
            this.billing_cycle = new DevExpress.XtraEditors.LabelControl();
            this.vendor_category = new DevExpress.XtraEditors.LabelControl();
            this.first_billing_date = new DevExpress.XtraEditors.LabelControl();
            this.LayoutControlGroup1 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.LayoutControlGroup2 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.LayoutControlItem4 = new DevExpress.XtraLayout.LayoutControlItem();
            this.LayoutControlItem3 = new DevExpress.XtraLayout.LayoutControlItem();
            this.LayoutControlItem2 = new DevExpress.XtraLayout.LayoutControlItem();
            this.LayoutControlItem1 = new DevExpress.XtraLayout.LayoutControlItem();
            this.LayoutControlGroup4 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlGroup5 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.LayoutControlItem12 = new DevExpress.XtraLayout.LayoutControlItem();
            this.LayoutControlItem13 = new DevExpress.XtraLayout.LayoutControlItem();
            this.LayoutControlItem15 = new DevExpress.XtraLayout.LayoutControlItem();
            this.LayoutControlItem19 = new DevExpress.XtraLayout.LayoutControlItem();
            this.LayoutControlGroup3 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.LayoutControlItem20 = new DevExpress.XtraLayout.LayoutControlItem();
            this.LayoutControlItem7 = new DevExpress.XtraLayout.LayoutControlItem();
            this.LayoutControlItem21 = new DevExpress.XtraLayout.LayoutControlItem();
            this.LayoutControlItem16 = new DevExpress.XtraLayout.LayoutControlItem();
            this.LayoutControlItem17 = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem1 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.emptySpaceItem2 = new DevExpress.XtraLayout.EmptySpaceItem();
            ((System.ComponentModel.ISupportInitialize)(this.comment.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LayoutControl1)).BeginInit();
            this.LayoutControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.LayoutControlGroup1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LayoutControlGroup2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LayoutControlItem4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LayoutControlItem3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LayoutControlItem2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LayoutControlItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LayoutControlGroup4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LayoutControlItem12)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LayoutControlItem13)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LayoutControlItem15)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LayoutControlItem19)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LayoutControlGroup3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LayoutControlItem20)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LayoutControlItem7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LayoutControlItem21)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LayoutControlItem16)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LayoutControlItem17)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem2)).BeginInit();
            this.SuspendLayout();
            // 
            // comment
            // 
            this.comment.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.comment.Location = new System.Drawing.Point(62, 233);
            this.comment.Name = "comment";
            this.comment.Properties.MaxLength = 50;
            this.comment.Size = new System.Drawing.Size(502, 20);
            this.comment.StyleController = this.LayoutControl1;
            this.comment.TabIndex = 18;
            this.comment.ToolTip = "Enter today comment field here that is displayed with the creditor.";
            // 
            // LayoutControl1
            // 
            this.LayoutControl1.Controls.Add(this.billed_mtd);
            this.LayoutControl1.Controls.Add(this.comment);
            this.LayoutControl1.Controls.Add(this.date_created);
            this.LayoutControl1.Controls.Add(this.billed_ytd);
            this.LayoutControl1.Controls.Add(this.mtd_received);
            this.LayoutControl1.Controls.Add(this.returned_mail);
            this.LayoutControl1.Controls.Add(this.fax);
            this.LayoutControl1.Controls.Add(this.label_address);
            this.LayoutControl1.Controls.Add(this.phone);
            this.LayoutControl1.Controls.Add(this.ytd_received);
            this.LayoutControl1.Controls.Add(this.billing_cycle);
            this.LayoutControl1.Controls.Add(this.vendor_category);
            this.LayoutControl1.Controls.Add(this.first_billing_date);
            this.LayoutControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.LayoutControl1.Location = new System.Drawing.Point(0, 0);
            this.LayoutControl1.Name = "LayoutControl1";
            this.LayoutControl1.OptionsCustomizationForm.DesignTimeCustomizationFormPositionAndSize = new System.Drawing.Rectangle(448, 63, 450, 350);
            this.LayoutControl1.OptionsCustomizationForm.ShowPropertyGrid = true;
            this.LayoutControl1.Root = this.LayoutControlGroup1;
            this.LayoutControl1.Size = new System.Drawing.Size(576, 265);
            this.LayoutControl1.TabIndex = 37;
            this.LayoutControl1.Text = "LayoutControl1";
            // 
            // billed_mtd
            // 
            this.billed_mtd.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.billed_mtd.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.billed_mtd.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.billed_mtd.CausesValidation = false;
            this.billed_mtd.Location = new System.Drawing.Point(433, 12);
            this.billed_mtd.Name = "billed_mtd";
            this.billed_mtd.Size = new System.Drawing.Size(131, 13);
            this.billed_mtd.StyleController = this.LayoutControl1;
            this.billed_mtd.TabIndex = 20;
            this.billed_mtd.Tag = "*";
            this.billed_mtd.Text = "$0.00";
            this.billed_mtd.ToolTip = "How much money we have sent the creditor this month";
            this.billed_mtd.UseMnemonic = false;
            // 
            // date_created
            // 
            this.date_created.CausesValidation = false;
            this.date_created.Location = new System.Drawing.Point(213, 63);
            this.date_created.Name = "date_created";
            this.date_created.Size = new System.Drawing.Size(111, 13);
            this.date_created.StyleController = this.LayoutControl1;
            this.date_created.TabIndex = 30;
            this.date_created.Tag = "*";
            this.date_created.ToolTip = "Date when the vendor was created";
            // 
            // billed_ytd
            // 
            this.billed_ytd.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.billed_ytd.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.billed_ytd.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.billed_ytd.CausesValidation = false;
            this.billed_ytd.Location = new System.Drawing.Point(433, 29);
            this.billed_ytd.Name = "billed_ytd";
            this.billed_ytd.Size = new System.Drawing.Size(131, 13);
            this.billed_ytd.StyleController = this.LayoutControl1;
            this.billed_ytd.TabIndex = 21;
            this.billed_ytd.Tag = "*";
            this.billed_ytd.Text = "$0.00";
            this.billed_ytd.ToolTip = "How much money we have sent the creditor this year";
            this.billed_ytd.UseMnemonic = false;
            // 
            // mtd_received
            // 
            this.mtd_received.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.mtd_received.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.mtd_received.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.mtd_received.CausesValidation = false;
            this.mtd_received.Location = new System.Drawing.Point(433, 46);
            this.mtd_received.Name = "mtd_received";
            this.mtd_received.Size = new System.Drawing.Size(131, 13);
            this.mtd_received.StyleController = this.LayoutControl1;
            this.mtd_received.TabIndex = 22;
            this.mtd_received.Tag = "*";
            this.mtd_received.Text = "$0.00";
            this.mtd_received.ToolTip = "How much money the creditor has given the agency this month";
            this.mtd_received.UseMnemonic = false;
            // 
            // returned_mail
            // 
            this.returned_mail.CausesValidation = false;
            this.returned_mail.Location = new System.Drawing.Point(83, 63);
            this.returned_mail.Name = "returned_mail";
            this.returned_mail.Size = new System.Drawing.Size(93, 13);
            this.returned_mail.StyleController = this.LayoutControl1;
            this.returned_mail.TabIndex = 31;
            this.returned_mail.Tag = "*";
            this.returned_mail.ToolTip = "Date that mail was returned. Further mailings are not sent until the address is c" +
    "orrected and this date is removed.";
            this.returned_mail.UseMnemonic = false;
            // 
            // fax
            // 
            this.fax.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.fax.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.fax.CausesValidation = false;
            this.fax.Location = new System.Drawing.Point(436, 128);
            this.fax.Name = "fax";
            this.fax.Size = new System.Drawing.Size(116, 13);
            this.fax.StyleController = this.LayoutControl1;
            this.fax.TabIndex = 28;
            this.fax.Tag = "*";
            this.fax.ToolTip = "Primary contact FAX number";
            this.fax.UseMnemonic = false;
            // 
            // label_address
            // 
            this.label_address.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label_address.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.label_address.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.label_address.Appearance.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Top;
            this.label_address.Appearance.TextOptions.WordWrap = DevExpress.Utils.WordWrap.NoWrap;
            this.label_address.CausesValidation = false;
            this.label_address.Location = new System.Drawing.Point(20, 88);
            this.label_address.MinimumSize = new System.Drawing.Size(0, 65);
            this.label_address.Name = "label_address";
            this.label_address.Size = new System.Drawing.Size(307, 65);
            this.label_address.StyleController = this.LayoutControl1;
            this.label_address.TabIndex = 19;
            this.label_address.ToolTip = "Vendor\'s general address.";
            // 
            // phone
            // 
            this.phone.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.phone.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.phone.CausesValidation = false;
            this.phone.Location = new System.Drawing.Point(436, 111);
            this.phone.Name = "phone";
            this.phone.Size = new System.Drawing.Size(116, 13);
            this.phone.StyleController = this.LayoutControl1;
            this.phone.TabIndex = 27;
            this.phone.Tag = "*";
            this.phone.ToolTip = "Primary contact telephone number";
            this.phone.UseMnemonic = false;
            // 
            // ytd_received
            // 
            this.ytd_received.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.ytd_received.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.ytd_received.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.ytd_received.CausesValidation = false;
            this.ytd_received.Location = new System.Drawing.Point(433, 63);
            this.ytd_received.Name = "ytd_received";
            this.ytd_received.Size = new System.Drawing.Size(131, 13);
            this.ytd_received.StyleController = this.LayoutControl1;
            this.ytd_received.TabIndex = 23;
            this.ytd_received.Tag = "*";
            this.ytd_received.Text = "$0.00";
            this.ytd_received.ToolTip = "How much money the creditor has given the agency this year";
            this.ytd_received.UseMnemonic = false;
            // 
            // billing_cycle
            // 
            this.billing_cycle.CausesValidation = false;
            this.billing_cycle.Location = new System.Drawing.Point(97, 29);
            this.billing_cycle.Name = "billing_cycle";
            this.billing_cycle.Size = new System.Drawing.Size(227, 13);
            this.billing_cycle.StyleController = this.LayoutControl1;
            this.billing_cycle.TabIndex = 33;
            this.billing_cycle.Tag = "*";
            this.billing_cycle.ToolTip = "If the vendor is billed, what is the billing frequency";
            // 
            // vendor_category
            // 
            this.vendor_category.CausesValidation = false;
            this.vendor_category.Location = new System.Drawing.Point(97, 12);
            this.vendor_category.Name = "vendor_category";
            this.vendor_category.Size = new System.Drawing.Size(227, 13);
            this.vendor_category.StyleController = this.LayoutControl1;
            this.vendor_category.TabIndex = 33;
            this.vendor_category.Tag = "";
            this.vendor_category.ToolTip = "If the vendor is billed, what is the billing frequency";
            // 
            // first_billing_date
            // 
            this.first_billing_date.CausesValidation = false;
            this.first_billing_date.Location = new System.Drawing.Point(97, 46);
            this.first_billing_date.Name = "first_billing_date";
            this.first_billing_date.Size = new System.Drawing.Size(227, 13);
            this.first_billing_date.StyleController = this.LayoutControl1;
            this.first_billing_date.TabIndex = 33;
            this.first_billing_date.Tag = "*";
            this.first_billing_date.ToolTip = "If the vendor is billed, what is the billing frequency";
            // 
            // LayoutControlGroup1
            // 
            this.LayoutControlGroup1.CustomizationFormText = "LayoutControlGroup1";
            this.LayoutControlGroup1.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.True;
            this.LayoutControlGroup1.GroupBordersVisible = false;
            this.LayoutControlGroup1.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.LayoutControlGroup2,
            this.LayoutControlGroup4,
            this.LayoutControlItem19,
            this.LayoutControlGroup3,
            this.LayoutControlItem16,
            this.LayoutControlItem17,
            this.emptySpaceItem1,
            this.emptySpaceItem2});
            this.LayoutControlGroup1.Location = new System.Drawing.Point(0, 0);
            this.LayoutControlGroup1.Name = "LayoutControlGroup1";
            this.LayoutControlGroup1.Size = new System.Drawing.Size(576, 265);
            this.LayoutControlGroup1.Text = "LayoutControlGroup1";
            this.LayoutControlGroup1.TextVisible = false;
            // 
            // LayoutControlGroup2
            // 
            this.LayoutControlGroup2.CustomizationFormText = "Dollar Amounts";
            this.LayoutControlGroup2.GroupBordersVisible = false;
            this.LayoutControlGroup2.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.LayoutControlItem4,
            this.LayoutControlItem3,
            this.LayoutControlItem2,
            this.LayoutControlItem1});
            this.LayoutControlGroup2.Location = new System.Drawing.Point(326, 0);
            this.LayoutControlGroup2.Name = "LayoutControlGroup2";
            this.LayoutControlGroup2.Padding = new DevExpress.XtraLayout.Utils.Padding(0, 0, 0, 0);
            this.LayoutControlGroup2.Size = new System.Drawing.Size(230, 68);
            this.LayoutControlGroup2.Text = "Dollar Amounts";
            this.LayoutControlGroup2.TextVisible = false;
            // 
            // LayoutControlItem4
            // 
            this.LayoutControlItem4.Control = this.ytd_received;
            this.LayoutControlItem4.CustomizationFormText = "YTD Received";
            this.LayoutControlItem4.Location = new System.Drawing.Point(0, 51);
            this.LayoutControlItem4.Name = "LayoutControlItem4";
            this.LayoutControlItem4.Size = new System.Drawing.Size(230, 17);
            this.LayoutControlItem4.Spacing = new DevExpress.XtraLayout.Utils.Padding(10, 0, 0, 0);
            this.LayoutControlItem4.Text = "YTD Received";
            this.LayoutControlItem4.TextSize = new System.Drawing.Size(82, 13);
            // 
            // LayoutControlItem3
            // 
            this.LayoutControlItem3.Control = this.mtd_received;
            this.LayoutControlItem3.CustomizationFormText = "MTD Received";
            this.LayoutControlItem3.Location = new System.Drawing.Point(0, 34);
            this.LayoutControlItem3.Name = "LayoutControlItem3";
            this.LayoutControlItem3.Size = new System.Drawing.Size(230, 17);
            this.LayoutControlItem3.Spacing = new DevExpress.XtraLayout.Utils.Padding(10, 0, 0, 0);
            this.LayoutControlItem3.Text = "MTD Received";
            this.LayoutControlItem3.TextSize = new System.Drawing.Size(82, 13);
            // 
            // LayoutControlItem2
            // 
            this.LayoutControlItem2.Control = this.billed_ytd;
            this.LayoutControlItem2.CustomizationFormText = "YTD Billed";
            this.LayoutControlItem2.Location = new System.Drawing.Point(0, 17);
            this.LayoutControlItem2.Name = "LayoutControlItem2";
            this.LayoutControlItem2.Size = new System.Drawing.Size(230, 17);
            this.LayoutControlItem2.Spacing = new DevExpress.XtraLayout.Utils.Padding(10, 0, 0, 0);
            this.LayoutControlItem2.Text = "YTD Billed";
            this.LayoutControlItem2.TextSize = new System.Drawing.Size(82, 13);
            // 
            // LayoutControlItem1
            // 
            this.LayoutControlItem1.Control = this.billed_mtd;
            this.LayoutControlItem1.CustomizationFormText = "MTD Billed";
            this.LayoutControlItem1.Location = new System.Drawing.Point(0, 0);
            this.LayoutControlItem1.Name = "LayoutControlItem1";
            this.LayoutControlItem1.Size = new System.Drawing.Size(230, 17);
            this.LayoutControlItem1.Spacing = new DevExpress.XtraLayout.Utils.Padding(10, 0, 0, 0);
            this.LayoutControlItem1.Text = "MTD Billed";
            this.LayoutControlItem1.TextSize = new System.Drawing.Size(82, 13);
            // 
            // LayoutControlGroup4
            // 
            this.LayoutControlGroup4.CustomizationFormText = "Misc";
            this.LayoutControlGroup4.GroupBordersVisible = false;
            this.LayoutControlGroup4.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlGroup5,
            this.LayoutControlItem15});
            this.LayoutControlGroup4.Location = new System.Drawing.Point(0, 68);
            this.LayoutControlGroup4.Name = "LayoutControlGroup4";
            this.LayoutControlGroup4.Padding = new DevExpress.XtraLayout.Utils.Padding(0, 0, 0, 0);
            this.LayoutControlGroup4.Size = new System.Drawing.Size(556, 77);
            this.LayoutControlGroup4.Text = "Misc";
            this.LayoutControlGroup4.TextVisible = false;
            // 
            // layoutControlGroup5
            // 
            this.layoutControlGroup5.CustomizationFormText = "Main Contact";
            this.layoutControlGroup5.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.LayoutControlItem12,
            this.LayoutControlItem13});
            this.layoutControlGroup5.Location = new System.Drawing.Point(327, 0);
            this.layoutControlGroup5.Name = "layoutControlGroup5";
            this.layoutControlGroup5.Size = new System.Drawing.Size(229, 77);
            this.layoutControlGroup5.Text = "Main Contact";
            // 
            // LayoutControlItem12
            // 
            this.LayoutControlItem12.Control = this.phone;
            this.LayoutControlItem12.CustomizationFormText = "Phone";
            this.LayoutControlItem12.Location = new System.Drawing.Point(0, 0);
            this.LayoutControlItem12.Name = "LayoutControlItem12";
            this.LayoutControlItem12.Size = new System.Drawing.Size(205, 17);
            this.LayoutControlItem12.Text = "Phone";
            this.LayoutControlItem12.TextSize = new System.Drawing.Size(82, 13);
            // 
            // LayoutControlItem13
            // 
            this.LayoutControlItem13.Control = this.fax;
            this.LayoutControlItem13.CustomizationFormText = "FAX";
            this.LayoutControlItem13.Location = new System.Drawing.Point(0, 17);
            this.LayoutControlItem13.Name = "LayoutControlItem13";
            this.LayoutControlItem13.Size = new System.Drawing.Size(205, 17);
            this.LayoutControlItem13.Text = "FAX";
            this.LayoutControlItem13.TextSize = new System.Drawing.Size(82, 13);
            // 
            // LayoutControlItem15
            // 
            this.LayoutControlItem15.Control = this.label_address;
            this.LayoutControlItem15.CustomizationFormText = "Address";
            this.LayoutControlItem15.Location = new System.Drawing.Point(0, 0);
            this.LayoutControlItem15.MaxSize = new System.Drawing.Size(0, 33);
            this.LayoutControlItem15.MinSize = new System.Drawing.Size(303, 33);
            this.LayoutControlItem15.Name = "LayoutControlItem15";
            this.LayoutControlItem15.Padding = new DevExpress.XtraLayout.Utils.Padding(10, 10, 10, 10);
            this.LayoutControlItem15.Size = new System.Drawing.Size(327, 77);
            this.LayoutControlItem15.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.LayoutControlItem15.Text = "Address";
            this.LayoutControlItem15.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.AutoSize;
            this.LayoutControlItem15.TextSize = new System.Drawing.Size(0, 0);
            this.LayoutControlItem15.TextToControlDistance = 0;
            this.LayoutControlItem15.TextVisible = false;
            // 
            // LayoutControlItem19
            // 
            this.LayoutControlItem19.Control = this.comment;
            this.LayoutControlItem19.CustomizationFormText = "Comment";
            this.LayoutControlItem19.Location = new System.Drawing.Point(0, 221);
            this.LayoutControlItem19.Name = "LayoutControlItem19";
            this.LayoutControlItem19.Size = new System.Drawing.Size(556, 24);
            this.LayoutControlItem19.Text = "Comment";
            this.LayoutControlItem19.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.AutoSize;
            this.LayoutControlItem19.TextSize = new System.Drawing.Size(45, 13);
            this.LayoutControlItem19.TextToControlDistance = 5;
            // 
            // LayoutControlGroup3
            // 
            this.LayoutControlGroup3.CustomizationFormText = "Cycles And Contributions";
            this.LayoutControlGroup3.GroupBordersVisible = false;
            this.LayoutControlGroup3.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.LayoutControlItem20,
            this.LayoutControlItem7,
            this.LayoutControlItem21});
            this.LayoutControlGroup3.Location = new System.Drawing.Point(0, 0);
            this.LayoutControlGroup3.Name = "LayoutControlGroup3";
            this.LayoutControlGroup3.Padding = new DevExpress.XtraLayout.Utils.Padding(0, 0, 0, 0);
            this.LayoutControlGroup3.Size = new System.Drawing.Size(316, 51);
            this.LayoutControlGroup3.Text = "Cycles And Contributions";
            this.LayoutControlGroup3.TextVisible = false;
            // 
            // LayoutControlItem20
            // 
            this.LayoutControlItem20.Control = this.vendor_category;
            this.LayoutControlItem20.CustomizationFormText = "Vendor Category";
            this.LayoutControlItem20.Location = new System.Drawing.Point(0, 0);
            this.LayoutControlItem20.Name = "LayoutControlItem20";
            this.LayoutControlItem20.Size = new System.Drawing.Size(316, 17);
            this.LayoutControlItem20.Text = "Vendor Category";
            this.LayoutControlItem20.TextSize = new System.Drawing.Size(82, 13);
            // 
            // LayoutControlItem7
            // 
            this.LayoutControlItem7.Control = this.billing_cycle;
            this.LayoutControlItem7.CustomizationFormText = "Billing Cycle";
            this.LayoutControlItem7.Location = new System.Drawing.Point(0, 17);
            this.LayoutControlItem7.Name = "LayoutControlItem7";
            this.LayoutControlItem7.Size = new System.Drawing.Size(316, 17);
            this.LayoutControlItem7.Text = "Billing Cycle";
            this.LayoutControlItem7.TextSize = new System.Drawing.Size(82, 13);
            // 
            // LayoutControlItem21
            // 
            this.LayoutControlItem21.Control = this.first_billing_date;
            this.LayoutControlItem21.CustomizationFormText = "First Billing Date";
            this.LayoutControlItem21.Location = new System.Drawing.Point(0, 34);
            this.LayoutControlItem21.Name = "LayoutControlItem21";
            this.LayoutControlItem21.Size = new System.Drawing.Size(316, 17);
            this.LayoutControlItem21.Text = "First Billing Date";
            this.LayoutControlItem21.TextSize = new System.Drawing.Size(82, 13);
            // 
            // LayoutControlItem16
            // 
            this.LayoutControlItem16.Control = this.returned_mail;
            this.LayoutControlItem16.CustomizationFormText = "Returned Mail";
            this.LayoutControlItem16.Location = new System.Drawing.Point(0, 51);
            this.LayoutControlItem16.MaxSize = new System.Drawing.Size(173, 17);
            this.LayoutControlItem16.MinSize = new System.Drawing.Size(75, 17);
            this.LayoutControlItem16.Name = "LayoutControlItem16";
            this.LayoutControlItem16.Size = new System.Drawing.Size(168, 17);
            this.LayoutControlItem16.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.LayoutControlItem16.Text = "Returned Mail";
            this.LayoutControlItem16.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.AutoSize;
            this.LayoutControlItem16.TextSize = new System.Drawing.Size(66, 13);
            this.LayoutControlItem16.TextToControlDistance = 5;
            // 
            // LayoutControlItem17
            // 
            this.LayoutControlItem17.Control = this.date_created;
            this.LayoutControlItem17.CustomizationFormText = "Setup";
            this.LayoutControlItem17.Location = new System.Drawing.Point(168, 51);
            this.LayoutControlItem17.Name = "LayoutControlItem17";
            this.LayoutControlItem17.Size = new System.Drawing.Size(148, 17);
            this.LayoutControlItem17.Text = "Setup";
            this.LayoutControlItem17.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.AutoSize;
            this.LayoutControlItem17.TextSize = new System.Drawing.Size(28, 13);
            this.LayoutControlItem17.TextToControlDistance = 5;
            // 
            // emptySpaceItem1
            // 
            this.emptySpaceItem1.AllowHotTrack = false;
            this.emptySpaceItem1.CustomizationFormText = "emptySpaceItem1";
            this.emptySpaceItem1.Location = new System.Drawing.Point(0, 145);
            this.emptySpaceItem1.Name = "emptySpaceItem1";
            this.emptySpaceItem1.Size = new System.Drawing.Size(556, 76);
            this.emptySpaceItem1.Text = "emptySpaceItem1";
            this.emptySpaceItem1.TextSize = new System.Drawing.Size(0, 0);
            // 
            // emptySpaceItem2
            // 
            this.emptySpaceItem2.AllowHotTrack = false;
            this.emptySpaceItem2.CustomizationFormText = "emptySpaceItem2";
            this.emptySpaceItem2.Location = new System.Drawing.Point(316, 0);
            this.emptySpaceItem2.Name = "emptySpaceItem2";
            this.emptySpaceItem2.Size = new System.Drawing.Size(10, 68);
            this.emptySpaceItem2.Text = "emptySpaceItem2";
            this.emptySpaceItem2.TextSize = new System.Drawing.Size(0, 0);
            // 
            // Page_General
            // 
            this.CausesValidation = false;
            this.Controls.Add(this.LayoutControl1);
            this.Name = "Page_General";
            this.Size = new System.Drawing.Size(576, 265);
            ((System.ComponentModel.ISupportInitialize)(this.comment.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LayoutControl1)).EndInit();
            this.LayoutControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.LayoutControlGroup1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LayoutControlGroup2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LayoutControlItem4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LayoutControlItem3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LayoutControlItem2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LayoutControlItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LayoutControlGroup4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LayoutControlItem12)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LayoutControlItem13)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LayoutControlItem15)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LayoutControlItem19)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LayoutControlGroup3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LayoutControlItem20)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LayoutControlItem7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LayoutControlItem21)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LayoutControlItem16)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LayoutControlItem17)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem2)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion " Windows Form Designer generated code "

        internal override void ReadForm(BusinessContext bc, vendor VendorRecord)
        {
            base.ReadForm(bc, VendorRecord);

            // Save the record pointer
            this.vendorRecord       = VendorRecord;

            // Load the controls with the static text.
            billing_cycle.Text      = "MONTHLY";
            billed_mtd.Text         = string.Format("{0:c}", vendorRecord.st_BilledMTDAmt);
            billed_ytd.Text         = string.Format("{0:c}", vendorRecord.st_BilledMTDAmt + vendorRecord.st_BilledYTDAmt);
            mtd_received.Text       = string.Format("{0:c}", vendorRecord.st_ReceivedMTDAmt);
            ytd_received.Text       = string.Format("{0:c}", vendorRecord.st_ReceivedMTDAmt + vendorRecord.st_ReceivedYTDAmt);
            first_billing_date.Text = vendorRecord.st_FirstBillingDate.HasValue ? vendorRecord.st_FirstBillingDate.Value.ToShortDateString() : string.Empty;
            returned_mail.Text      = vendorRecord.st_ReturnedMailDate.HasValue ? vendorRecord.st_ReturnedMailDate.Value.ToShortDateString() : string.Empty;
            date_created.Text       = vendorRecord.date_created.ToShortDateString();

            // Find vendor category
            var cat = DebtPlus.LINQ.Cache.vendor_type.getList().Find(s => s.Id == vendorRecord.Type);
            vendor_category.Text = cat != null ? cat.description : string.Empty;

            // There is one edited field for this page. Load it.
            comment.EditValue = vendorRecord.Comment;
        }

        internal override void SaveForm(BusinessContext bc)
        {
 	        base.SaveForm(bc);

            // Retrieve our one field
            vendorRecord.Comment = (DebtPlus.Utils.Nulls.v_String(comment.EditValue) ?? string.Empty).Trim();
        }

        internal override void RefreshForm(DebtPlus.LINQ.BusinessContext bc, DebtPlus.LINQ.vendor VendorRecord)
        {
            base.RefreshForm(bc, vendorRecord);

            // Save the creditor reference for later
            this.vendorRecord = VendorRecord;

            // Find the contact information
            var contactTypeMain = DebtPlus.LINQ.Cache.vendor_contact_type.getList().Find(s => string.Compare(s.description, "Main", true) == 0);
            if (contactTypeMain != null)
            {
                var contact = vendorRecord.vendor_contacts.Where(s => s.contact_type == contactTypeMain.Id).FirstOrDefault();
                if (contact != null)
                {
                    if (contact.telephoneID.HasValue)
                    {
                        var phoneRecord = bc.TelephoneNumbers.Where(s => s.Id == contact.telephoneID.Value).FirstOrDefault();
                        if (phoneRecord != null)
                        {
                            phone.Text = phoneRecord.ToString();
                        }
                    }

                    if (contact.faxID.HasValue)
                    {
                        var faxRecord = bc.TelephoneNumbers.Where(s => s.Id == contact.faxID.Value).FirstOrDefault();
                        if (faxRecord != null)
                        {
                            fax.Text = faxRecord.ToString();
                        }
                    }
                }
            }

            // Find the address label information. We need to have a new data context to flush the cache
            // since the address is changed "behind the scene" in the address control. Therefore, we need
            // to create a new datacontext so that it will retrieve the data from the database and not use
            // the cached copy of the address record (which would be old.)
            using (var newBc = new BusinessContext())
            {
                var q = (from va in bc.vendor_addresses
                            join a in bc.addresses on va.addressID equals a.Id
                            where va.vendor == vendorRecord.Id && va.address_type == DebtPlus.LINQ.vendor_address.AddressType_General
                            select new DebtPlus.LINQ.VendorAddressEntry(a, va.attn)).FirstOrDefault();

                label_address.Text = (q != null) ? q.ToString() : string.Empty;
            }
        }
    }
}
