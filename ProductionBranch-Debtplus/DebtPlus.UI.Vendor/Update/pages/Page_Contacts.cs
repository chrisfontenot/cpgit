#region "Copyright 2000-2012 DebtPlus, L.L.C."

//{*******************************************************************}
//{                                                                   }
//{       DebtPlus Debt Management System                             }
//{                                                                   }
//{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
//{       ALL RIGHTS RESERVED                                         }
//{                                                                   }
//{   The entire contents of this file is protected by U.S. and       }
//{   International Copyright Laws. Unauthorized reproduction,        }
//{   reverse-engineering, and distribution of all or any portion of  }
//{   the code contained in this file is strictly prohibited and may  }
//{   result in severe civil and criminal penalties and will be       }
//{   prosecuted to the maximum extent possible under the law.        }
//{                                                                   }
//{   RESTRICTIONS                                                    }
//{                                                                   }
//{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
//{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
//{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
//{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
//{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
//{                                                                   }
//{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
//{   ADDITIONAL RESTRICTIONS.                                        }
//{                                                                   }
//{*******************************************************************}

#endregion "Copyright 2000-2012 DebtPlus, L.L.C."

using System.Linq;
using System.Windows.Forms;
using DebtPlus.Data;
using DebtPlus.LINQ;
using DebtPlus.UI.Vendor.Update.Controls;
using DebtPlus.UI.Vendor.Update.Forms;
using DevExpress.XtraGrid.Columns;
using DevExpress.XtraGrid.Views.Base;
using state = DebtPlus.LINQ.Cache.state;
using vendor_contact_type = DebtPlus.LINQ.Cache.vendor_contact_type;

namespace DebtPlus.UI.Vendor.Update.Pages
{
    internal class Page_Contacts : MyGridControl
    {
        // Current vendor being edited
        private vendor vendorRecord;
        private GridColumn Address;
        private BusinessContext bc;

        public Page_Contacts() : base()
        {
            InitializeComponent();
            EnableMenus = true;
            RegisterHandlers();
        }

        private void RegisterHandlers()
        {
            GridView1.CustomColumnDisplayText += GridView1_CustomColumnDisplayText;
        }

        private void UnRegisterHandlers()
        {
            GridView1.CustomColumnDisplayText -= GridView1_CustomColumnDisplayText;
        }

        /// <summary>
        /// Handle the conversion of specific columns to show the proper text values
        /// </summary>
        private void GridView1_CustomColumnDisplayText(object sender, CustomColumnDisplayTextEventArgs e)
        {
            var record = GridView1.GetRow(e.RowHandle) as vendor_contact;
            if (record == null)
            {
                return;
            }

            // Determine the field that we want to display
            object tag = e.Column.Tag;
            if (tag == null)
            {
                return;
            }

            switch ((string) tag)
            {
                case "contact_type":
                    var typeRecord = vendor_contact_type.getList().Find(s => s.Id == record.contact_type);
                    e.DisplayText = typeRecord == null ? string.Empty : typeRecord.description ?? string.Empty;
                    return;

                ////
                //// Name references
                ////
                case "nameID":
                    var nId = record.nameID.HasValue ? bc.Names.FirstOrDefault(s => s.Id == record.nameID.Value) : null;
                    e.DisplayText = nId == null ? string.Empty : nId.ToString();
                    return;

                case "nameID.first":
                    var nFirst = record.nameID.HasValue ? bc.Names.FirstOrDefault(s => s.Id == record.nameID.Value) : null;
                    e.DisplayText = nFirst == null ? string.Empty : nFirst.First;
                    return;

                case "nameID.middle":
                    var nMiddle = record.nameID.HasValue ? bc.Names.FirstOrDefault(s => s.Id == record.nameID.Value) : null;
                    e.DisplayText = nMiddle == null ? string.Empty : nMiddle.Middle;
                    return;

                case "nameID.last":
                    var nLast = record.nameID.HasValue ? bc.Names.FirstOrDefault(s => s.Id == record.nameID.Value) : null;
                    e.DisplayText = nLast == null ? string.Empty : nLast.Last;
                    return;

                case "nameID.prefix":
                    var nPrefix = record.nameID.HasValue ? bc.Names.FirstOrDefault(s => s.Id == record.nameID.Value) : null;
                    e.DisplayText = nPrefix == null ? string.Empty : nPrefix.Last;
                    return;

                case "nameID.suffix":
                    var nSuffix = record.nameID.HasValue ? bc.Names.FirstOrDefault(s => s.Id == record.nameID.Value) : null;
                    e.DisplayText = nSuffix == null ? string.Empty : nSuffix.Last;
                    return;

                ////
                //// Telephone Number
                ////
                case "telephoneID":
                    var telephoneId = record.telephoneID.HasValue ? bc.TelephoneNumbers.FirstOrDefault(s => s.Id == record.telephoneID.Value) : null;
                    e.DisplayText = telephoneId == null ? string.Empty : telephoneId.ToString();
                    return;

                case "telephoneID.ext":
                    var telephoneExt = record.telephoneID.HasValue ? bc.TelephoneNumbers.FirstOrDefault(s => s.Id == record.telephoneID.Value) : null;
                    e.DisplayText = telephoneExt == null || string.IsNullOrEmpty(telephoneExt.Ext) ? string.Empty : telephoneExt.Ext;
                    return;

                ////
                //// FAX Number
                ////
                case "faxID":
                    var faxId = record.faxID.HasValue ? bc.TelephoneNumbers.FirstOrDefault(s => s.Id == record.faxID.Value) : null;
                    e.DisplayText = faxId == null ? string.Empty : faxId.ToString();
                    return;

                ////
                //// Email Address
                ////
                case "emailID":
                    var emailId = record.emailID.HasValue ? bc.EmailAddresses.FirstOrDefault(s => s.Id == record.emailID.Value) : null;
                    e.DisplayText = emailId == null ? string.Empty : emailId.Address;
                    return;

                ////
                //// Mailing addressees
                ////
                case "addressID.line1":
                    var addressRecordLine1 = record.addressID.HasValue ? bc.addresses.FirstOrDefault(s => s.Id == record.addressID.Value) : null;
                    e.DisplayText = addressRecordLine1 == null ? string.Empty : addressRecordLine1.AddressLine1;
                    return;

                case "addressID.line2":
                    var addressRecordLine2 = record.addressID.HasValue ? bc.addresses.FirstOrDefault(s => s.Id == record.addressID.Value) : null;
                    e.DisplayText = addressRecordLine2 == null ? string.Empty : addressRecordLine2.address_line_2;
                    return;

                case "addressID.line3":
                    var addressRecordLine3 = record.addressID.HasValue ? bc.addresses.FirstOrDefault(s => s.Id == record.addressID.Value) : null;
                    e.DisplayText = addressRecordLine3 == null ? string.Empty : addressRecordLine3.address_line_3;
                    return;

                case "addressID.city":
                    var addressRecordCity = record.addressID.HasValue ? bc.addresses.FirstOrDefault(s => s.Id == record.addressID.Value) : null;
                    e.DisplayText = addressRecordCity == null ? string.Empty : addressRecordCity.city;
                    return;

                case "addressID.state":
                    var addressRecordState = record.addressID.HasValue ? bc.addresses.FirstOrDefault(s => s.Id == record.addressID.Value) : null;
                    var stateRecord = (addressRecordState == null) ? null : state.getList().Find(s => s.Id == addressRecordState.state);
                    e.DisplayText = stateRecord == null ? string.Empty : stateRecord.MailingCode;
                    return;

                case "addressID.postalCode":
                    var addressRecordPostalCode = record.addressID.HasValue ? bc.addresses.FirstOrDefault(s => s.Id == record.addressID.Value) : null;
                    e.DisplayText = addressRecordPostalCode == null ? string.Empty : addressRecordPostalCode.PostalCode;
                    return;
            }
        }

        internal override void ReadForm(BusinessContext bc, vendor VendorRecord)
        {
            UnRegisterHandlers();
            try
            {
                vendorRecord = VendorRecord;
                this.bc = bc;

                GridControl1.DataSource = vendorRecord.vendor_contacts.GetNewBindingList();

                RegisterHandlers();
                GridView1.RefreshData();

                // Clear the row selection if there are no contacts.
                if (vendorRecord.vendor_contacts.Count == 0)
                {
                    GridView1.FocusedRowHandle = -1;
                    return;
                }

                // Select the first row in the list.
                GridView1.FocusedRowHandle = 0;
                OnSelectRecord(GridView1.GetRow(0));
            }
            finally
            {
                RegisterHandlers();
            }
        }

        protected void SaveForm()
        {
            UnRegisterHandlers();
        }

        protected override void OnCreateRecord()
        {
            vendor_contact record = Factory.Manufacture_vendor_contact();
            using (var frm = new Form_Contact(record))
            {
                if (frm.ShowDialog() != DialogResult.OK)
                {
                    return;
                }
            }

            // Add the new record
            vendorRecord.vendor_contacts.Add(record);
            GridControl1.DataSource = vendorRecord.vendor_contacts.GetNewBindingList();
            GridView1.RefreshData();
        }

        protected override void OnDeleteRecord(object obj)
        {
            // Find the record to be deleted
            var record = obj as vendor_contact;
            if (obj == null)
            {
                return;
            }

            // Ask if the item is to be removed
            if (Prompts.RequestConfirmation_Delete() != DialogResult.Yes)
            {
                return;
            }

            // Remove the item from the database
            vendorRecord.vendor_contacts.Remove(record);
            GridControl1.DataSource = vendorRecord.vendor_contacts.GetNewBindingList();
            GridView1.RefreshData();
        }

        /// <summary>
        /// Edit the current record
        /// </summary>
        /// <param name="obj">Record to be updated</param>
        protected override void OnEditRecord(object obj)
        {
            var record = obj as vendor_contact;
            if (obj == null)
            {
                return;
            }

            // Display the record to the user
            using (var frm = new Form_Contact(record))
            {
                if (frm.ShowDialog() != DialogResult.OK)
                {
                    return;
                }
            }

            // Refresh the grid information
            GridControl1.DataSource = vendorRecord.vendor_contacts.GetNewBindingList();
            GridView1.RefreshData();

            // Update the selected record with the item if this is the selected record.
            if (Equals(GridView1.GetRow(GridView1.FocusedRowHandle), record))
            {
                OnSelectRecord(record);
            }
        }

        protected override void OnSelectRecord(object obj)
        {
            GroupControl1.Text = "Contact Detail";

            // Find the record to be deleted
            var record = obj as vendor_contact;
            if (record == null)
            {
                // Blank the current information
                label_name.Text    = string.Empty;
                label_title.Text   = string.Empty;
                label_address.Text = string.Empty;
                label_notes.Text   = string.Empty;
                label_phone.Text   = string.Empty;
                label_fax.Text     = string.Empty;
                label_email.Text   = string.Empty;
                return;
            }

            // These fields come directly from the contact record
            label_notes.Text = record.note ?? string.Empty;
            label_title.Text = record.Title ?? string.Empty;

            // LINQ/SQL will cache the entries. So, use their cache.
            var nameRecord = record.nameID.HasValue ? bc.Names.FirstOrDefault(s => s.Id == record.nameID.Value) : null;
            label_name.Text = nameRecord == null ? string.Empty : nameRecord.ToString();

            var phoneRecord = record.telephoneID.HasValue ? bc.TelephoneNumbers.FirstOrDefault(s => s.Id == record.telephoneID.Value) : null;
            label_phone.Text = phoneRecord == null ? string.Empty : phoneRecord.ToString();

            phoneRecord = record.faxID.HasValue ? bc.TelephoneNumbers.FirstOrDefault(s => s.Id == record.faxID.Value) : null;
            label_fax.Text = phoneRecord == null ? string.Empty : phoneRecord.ToString();

            var emailRecord = record.emailID.HasValue ? bc.EmailAddresses.FirstOrDefault(s => s.Id == record.emailID.Value) : null;
            label_email.Text = emailRecord == null ? string.Empty : emailRecord.ToString();

            var addressRecord = record.addressID.HasValue ? bc.addresses.FirstOrDefault(s => s.Id == record.addressID.Value) : null;
            label_address.Text = addressRecord == null ? string.Empty : addressRecord.ToString();
        }

        #region Windows Form Designer generated code

        //Required by the Windows Form Designer
        private System.ComponentModel.IContainer components = null;

        //Form overrides dispose to clean up the component list.
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                if (components != null)
                {
                    components.Dispose();
                }
            }
            base.Dispose(disposing);
        }

        //NOTE: The following procedure is required by the Windows Form Designer
        //It can be modified using the Windows Form Designer.
        //Do not modify it using the code editor.
        [System.Diagnostics.DebuggerStepThrough]
        private void InitializeComponent()
        {
            this.GridColumn1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.GridColumn2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.GridColumn3 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.GridColumn4 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.GridColumn5 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.GridColumn6 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.GridColumn7 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.GridColumn8 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.GridColumn9 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.GridColumn10 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.GridColumn11 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.GridColumn12 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.GridColumn13 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.GridColumn14 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.GridColumn15 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.GridColumn16 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.GridColumn17 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.GridColumn18 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.GridColumn19 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.GridColumn20 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.GridColumn21 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.GridColumn22 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.GridColumn23 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.Address = new DevExpress.XtraGrid.Columns.GridColumn();
            this.GroupControl1 = new DevExpress.XtraEditors.GroupControl();
            this.label_notes = new DevExpress.XtraEditors.LabelControl();
            this.LabelControl13 = new DevExpress.XtraEditors.LabelControl();
            this.label_address = new DevExpress.XtraEditors.LabelControl();
            this.LabelControl11 = new DevExpress.XtraEditors.LabelControl();
            this.label_email = new DevExpress.XtraEditors.LabelControl();
            this.label_fax = new DevExpress.XtraEditors.LabelControl();
            this.label_phone = new DevExpress.XtraEditors.LabelControl();
            this.label_title = new DevExpress.XtraEditors.LabelControl();
            this.label_name = new DevExpress.XtraEditors.LabelControl();
            this.LabelControl5 = new DevExpress.XtraEditors.LabelControl();
            this.LabelControl4 = new DevExpress.XtraEditors.LabelControl();
            this.LabelControl3 = new DevExpress.XtraEditors.LabelControl();
            this.LabelControl2 = new DevExpress.XtraEditors.LabelControl();
            this.LabelControl1 = new DevExpress.XtraEditors.LabelControl();
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.PopupMenu_Items)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.GridControl1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.GridView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.GroupControl1)).BeginInit();
            this.GroupControl1.SuspendLayout();
            this.SuspendLayout();
            // 
            // GridControl1
            // 
            this.GridControl1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.GridControl1.Dock = System.Windows.Forms.DockStyle.None;
            this.GridControl1.Size = new System.Drawing.Size(576, 152);
            // 
            // GridView1
            // 
            this.GridView1.Appearance.ColumnFilterButton.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(104)))), ((int)(((byte)(184)))), ((int)(((byte)(251)))));
            this.GridView1.Appearance.ColumnFilterButton.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(104)))), ((int)(((byte)(184)))), ((int)(((byte)(251)))));
            this.GridView1.Appearance.ColumnFilterButton.ForeColor = System.Drawing.Color.White;
            this.GridView1.Appearance.ColumnFilterButton.Options.UseBackColor = true;
            this.GridView1.Appearance.ColumnFilterButton.Options.UseBorderColor = true;
            this.GridView1.Appearance.ColumnFilterButton.Options.UseForeColor = true;
            this.GridView1.Appearance.ColumnFilterButtonActive.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(170)))), ((int)(((byte)(216)))), ((int)(((byte)(254)))));
            this.GridView1.Appearance.ColumnFilterButtonActive.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(170)))), ((int)(((byte)(216)))), ((int)(((byte)(254)))));
            this.GridView1.Appearance.ColumnFilterButtonActive.ForeColor = System.Drawing.Color.Black;
            this.GridView1.Appearance.ColumnFilterButtonActive.Options.UseBackColor = true;
            this.GridView1.Appearance.ColumnFilterButtonActive.Options.UseBorderColor = true;
            this.GridView1.Appearance.ColumnFilterButtonActive.Options.UseForeColor = true;
            this.GridView1.Appearance.Empty.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(236)))), ((int)(((byte)(246)))), ((int)(((byte)(255)))));
            this.GridView1.Appearance.Empty.BackColor2 = System.Drawing.Color.White;
            this.GridView1.Appearance.Empty.Options.UseBackColor = true;
            this.GridView1.Appearance.EvenRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(247)))), ((int)(((byte)(251)))), ((int)(((byte)(255)))));
            this.GridView1.Appearance.EvenRow.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(247)))), ((int)(((byte)(251)))), ((int)(((byte)(255)))));
            this.GridView1.Appearance.EvenRow.ForeColor = System.Drawing.Color.Black;
            this.GridView1.Appearance.EvenRow.Options.UseBackColor = true;
            this.GridView1.Appearance.EvenRow.Options.UseBorderColor = true;
            this.GridView1.Appearance.EvenRow.Options.UseForeColor = true;
            this.GridView1.Appearance.FilterCloseButton.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(104)))), ((int)(((byte)(184)))), ((int)(((byte)(251)))));
            this.GridView1.Appearance.FilterCloseButton.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(104)))), ((int)(((byte)(184)))), ((int)(((byte)(251)))));
            this.GridView1.Appearance.FilterCloseButton.ForeColor = System.Drawing.Color.White;
            this.GridView1.Appearance.FilterCloseButton.Options.UseBackColor = true;
            this.GridView1.Appearance.FilterCloseButton.Options.UseBorderColor = true;
            this.GridView1.Appearance.FilterCloseButton.Options.UseForeColor = true;
            this.GridView1.Appearance.FilterPanel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(236)))), ((int)(((byte)(246)))), ((int)(((byte)(255)))));
            this.GridView1.Appearance.FilterPanel.BackColor2 = System.Drawing.Color.White;
            this.GridView1.Appearance.FilterPanel.ForeColor = System.Drawing.Color.Black;
            this.GridView1.Appearance.FilterPanel.Options.UseBackColor = true;
            this.GridView1.Appearance.FilterPanel.Options.UseForeColor = true;
            this.GridView1.Appearance.FixedLine.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(59)))), ((int)(((byte)(133)))), ((int)(((byte)(195)))));
            this.GridView1.Appearance.FixedLine.Options.UseBackColor = true;
            this.GridView1.Appearance.FocusedCell.BackColor = System.Drawing.Color.White;
            this.GridView1.Appearance.FocusedCell.ForeColor = System.Drawing.Color.Black;
            this.GridView1.Appearance.FocusedCell.Options.UseBackColor = true;
            this.GridView1.Appearance.FocusedCell.Options.UseForeColor = true;
            this.GridView1.Appearance.FocusedRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(38)))), ((int)(((byte)(109)))), ((int)(((byte)(189)))));
            this.GridView1.Appearance.FocusedRow.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(59)))), ((int)(((byte)(139)))), ((int)(((byte)(206)))));
            this.GridView1.Appearance.FocusedRow.ForeColor = System.Drawing.Color.White;
            this.GridView1.Appearance.FocusedRow.Options.UseBackColor = true;
            this.GridView1.Appearance.FocusedRow.Options.UseBorderColor = true;
            this.GridView1.Appearance.FocusedRow.Options.UseForeColor = true;
            this.GridView1.Appearance.FooterPanel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(104)))), ((int)(((byte)(184)))), ((int)(((byte)(251)))));
            this.GridView1.Appearance.FooterPanel.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(104)))), ((int)(((byte)(184)))), ((int)(((byte)(251)))));
            this.GridView1.Appearance.FooterPanel.ForeColor = System.Drawing.Color.Black;
            this.GridView1.Appearance.FooterPanel.Options.UseBackColor = true;
            this.GridView1.Appearance.FooterPanel.Options.UseBorderColor = true;
            this.GridView1.Appearance.FooterPanel.Options.UseForeColor = true;
            this.GridView1.Appearance.GroupButton.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(104)))), ((int)(((byte)(184)))), ((int)(((byte)(251)))));
            this.GridView1.Appearance.GroupButton.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(104)))), ((int)(((byte)(184)))), ((int)(((byte)(251)))));
            this.GridView1.Appearance.GroupButton.Options.UseBackColor = true;
            this.GridView1.Appearance.GroupButton.Options.UseBorderColor = true;
            this.GridView1.Appearance.GroupFooter.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(170)))), ((int)(((byte)(216)))), ((int)(((byte)(254)))));
            this.GridView1.Appearance.GroupFooter.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(170)))), ((int)(((byte)(216)))), ((int)(((byte)(254)))));
            this.GridView1.Appearance.GroupFooter.ForeColor = System.Drawing.Color.Black;
            this.GridView1.Appearance.GroupFooter.Options.UseBackColor = true;
            this.GridView1.Appearance.GroupFooter.Options.UseBorderColor = true;
            this.GridView1.Appearance.GroupFooter.Options.UseForeColor = true;
            this.GridView1.Appearance.GroupPanel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(236)))), ((int)(((byte)(246)))), ((int)(((byte)(255)))));
            this.GridView1.Appearance.GroupPanel.BackColor2 = System.Drawing.Color.White;
            this.GridView1.Appearance.GroupPanel.ForeColor = System.Drawing.Color.Black;
            this.GridView1.Appearance.GroupPanel.Options.UseBackColor = true;
            this.GridView1.Appearance.GroupPanel.Options.UseForeColor = true;
            this.GridView1.Appearance.GroupRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(170)))), ((int)(((byte)(216)))), ((int)(((byte)(254)))));
            this.GridView1.Appearance.GroupRow.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(170)))), ((int)(((byte)(216)))), ((int)(((byte)(254)))));
            this.GridView1.Appearance.GroupRow.ForeColor = System.Drawing.Color.Black;
            this.GridView1.Appearance.GroupRow.Options.UseBackColor = true;
            this.GridView1.Appearance.GroupRow.Options.UseBorderColor = true;
            this.GridView1.Appearance.GroupRow.Options.UseForeColor = true;
            this.GridView1.Appearance.HeaderPanel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(139)))), ((int)(((byte)(201)))), ((int)(((byte)(254)))));
            this.GridView1.Appearance.HeaderPanel.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(139)))), ((int)(((byte)(201)))), ((int)(((byte)(254)))));
            this.GridView1.Appearance.HeaderPanel.ForeColor = System.Drawing.Color.Black;
            this.GridView1.Appearance.HeaderPanel.Options.UseBackColor = true;
            this.GridView1.Appearance.HeaderPanel.Options.UseBorderColor = true;
            this.GridView1.Appearance.HeaderPanel.Options.UseForeColor = true;
            this.GridView1.Appearance.HideSelectionRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(105)))), ((int)(((byte)(170)))), ((int)(((byte)(225)))));
            this.GridView1.Appearance.HideSelectionRow.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(83)))), ((int)(((byte)(155)))), ((int)(((byte)(215)))));
            this.GridView1.Appearance.HideSelectionRow.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(236)))), ((int)(((byte)(246)))), ((int)(((byte)(255)))));
            this.GridView1.Appearance.HideSelectionRow.Options.UseBackColor = true;
            this.GridView1.Appearance.HideSelectionRow.Options.UseBorderColor = true;
            this.GridView1.Appearance.HideSelectionRow.Options.UseForeColor = true;
            this.GridView1.Appearance.HorzLine.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(104)))), ((int)(((byte)(184)))), ((int)(((byte)(251)))));
            this.GridView1.Appearance.HorzLine.Options.UseBackColor = true;
            this.GridView1.Appearance.OddRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(236)))), ((int)(((byte)(246)))), ((int)(((byte)(255)))));
            this.GridView1.Appearance.OddRow.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(236)))), ((int)(((byte)(246)))), ((int)(((byte)(255)))));
            this.GridView1.Appearance.OddRow.ForeColor = System.Drawing.Color.Black;
            this.GridView1.Appearance.OddRow.Options.UseBackColor = true;
            this.GridView1.Appearance.OddRow.Options.UseBorderColor = true;
            this.GridView1.Appearance.OddRow.Options.UseForeColor = true;
            this.GridView1.Appearance.Preview.Font = new System.Drawing.Font("Verdana", 7.5F);
            this.GridView1.Appearance.Preview.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(83)))), ((int)(((byte)(155)))), ((int)(((byte)(215)))));
            this.GridView1.Appearance.Preview.Options.UseFont = true;
            this.GridView1.Appearance.Preview.Options.UseForeColor = true;
            this.GridView1.Appearance.Row.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(247)))), ((int)(((byte)(251)))), ((int)(((byte)(255)))));
            this.GridView1.Appearance.Row.ForeColor = System.Drawing.Color.Black;
            this.GridView1.Appearance.Row.Options.UseBackColor = true;
            this.GridView1.Appearance.Row.Options.UseForeColor = true;
            this.GridView1.Appearance.RowSeparator.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(236)))), ((int)(((byte)(246)))), ((int)(((byte)(255)))));
            this.GridView1.Appearance.RowSeparator.BackColor2 = System.Drawing.Color.White;
            this.GridView1.Appearance.RowSeparator.Options.UseBackColor = true;
            this.GridView1.Appearance.SelectedRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(83)))), ((int)(((byte)(155)))), ((int)(((byte)(215)))));
            this.GridView1.Appearance.SelectedRow.ForeColor = System.Drawing.Color.White;
            this.GridView1.Appearance.SelectedRow.Options.UseBackColor = true;
            this.GridView1.Appearance.SelectedRow.Options.UseForeColor = true;
            this.GridView1.Appearance.TopNewRow.BackColor = System.Drawing.Color.White;
            this.GridView1.Appearance.TopNewRow.Options.UseBackColor = true;
            this.GridView1.Appearance.VertLine.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(104)))), ((int)(((byte)(184)))), ((int)(((byte)(251)))));
            this.GridView1.Appearance.VertLine.Options.UseBackColor = true;
            this.GridView1.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.GridColumn1,
            this.GridColumn2,
            this.GridColumn3,
            this.GridColumn4,
            this.GridColumn22,
            this.GridColumn5,
            this.GridColumn6,
            this.GridColumn7,
            this.GridColumn8,
            this.GridColumn9,
            this.GridColumn10,
            this.GridColumn11,
            this.GridColumn12,
            this.GridColumn13,
            this.GridColumn14,
            this.GridColumn15,
            this.GridColumn16,
            this.GridColumn17,
            this.GridColumn18,
            this.GridColumn19,
            this.GridColumn20,
            this.GridColumn21,
            this.GridColumn23,
            this.Address});
            this.GridView1.OptionsBehavior.AutoPopulateColumns = false;
            this.GridView1.OptionsBehavior.Editable = false;
            this.GridView1.OptionsView.EnableAppearanceEvenRow = true;
            this.GridView1.OptionsView.EnableAppearanceOddRow = true;
            this.GridView1.OptionsView.ShowGroupPanel = false;
            this.GridView1.OptionsView.ShowIndicator = false;
            // 
            // GridColumn1
            // 
            this.GridColumn1.Caption = "Type";
            this.GridColumn1.CustomizationCaption = "Formatted Type";
            this.GridColumn1.Name = "GridColumn1";
            this.GridColumn1.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.True;
            this.GridColumn1.Tag = "contact_type";
            this.GridColumn1.UnboundType = DevExpress.Data.UnboundColumnType.String;
            this.GridColumn1.Visible = true;
            this.GridColumn1.VisibleIndex = 0;
            // 
            // GridColumn2
            // 
            this.GridColumn2.Caption = "Name";
            this.GridColumn2.CustomizationCaption = "Formatted Name";
            this.GridColumn2.Name = "GridColumn2";
            this.GridColumn2.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.True;
            this.GridColumn2.Tag = "nameID";
            this.GridColumn2.UnboundType = DevExpress.Data.UnboundColumnType.String;
            this.GridColumn2.Visible = true;
            this.GridColumn2.VisibleIndex = 1;
            // 
            // GridColumn3
            // 
            this.GridColumn3.Caption = "Phone";
            this.GridColumn3.CustomizationCaption = "Formatted Phone";
            this.GridColumn3.Name = "GridColumn3";
            this.GridColumn3.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.True;
            this.GridColumn3.Tag = "telephoneID";
            this.GridColumn3.UnboundType = DevExpress.Data.UnboundColumnType.String;
            this.GridColumn3.Visible = true;
            this.GridColumn3.VisibleIndex = 2;
            // 
            // GridColumn4
            // 
            this.GridColumn4.Caption = "FAX";
            this.GridColumn4.CustomizationCaption = "Formatted FAX";
            this.GridColumn4.Name = "GridColumn4";
            this.GridColumn4.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.True;
            this.GridColumn4.Tag = "faxID";
            this.GridColumn4.UnboundType = DevExpress.Data.UnboundColumnType.String;
            this.GridColumn4.Visible = true;
            this.GridColumn4.VisibleIndex = 3;
            // 
            // GridColumn5
            // 
            this.GridColumn5.Caption = "ID";
            this.GridColumn5.CustomizationCaption = "Record Number";
            this.GridColumn5.FieldName = "Id";
            this.GridColumn5.Name = "GridColumn5";
            this.GridColumn5.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.True;
            // 
            // GridColumn6
            // 
            this.GridColumn6.Caption = "Type";
            this.GridColumn6.CustomizationCaption = "Raw Contact Type";
            this.GridColumn6.FieldName = "contact_type";
            this.GridColumn6.Name = "GridColumn6";
            this.GridColumn6.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.True;
            // 
            // GridColumn7
            // 
            this.GridColumn7.Caption = "Prefix";
            this.GridColumn7.CustomizationCaption = "Name : Prefix";
            this.GridColumn7.Name = "GridColumn7";
            this.GridColumn7.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.True;
            this.GridColumn7.Tag = "nameID.prefix";
            this.GridColumn7.UnboundType = DevExpress.Data.UnboundColumnType.String;
            // 
            // GridColumn8
            // 
            this.GridColumn8.Caption = "First";
            this.GridColumn8.CustomizationCaption = "Name : First";
            this.GridColumn8.Name = "GridColumn8";
            this.GridColumn8.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.True;
            this.GridColumn8.Tag = "nameID.first";
            this.GridColumn8.UnboundType = DevExpress.Data.UnboundColumnType.String;
            // 
            // GridColumn9
            // 
            this.GridColumn9.Caption = "Middle";
            this.GridColumn9.CustomizationCaption = "Name : Middle";
            this.GridColumn9.Name = "GridColumn9";
            this.GridColumn9.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.True;
            this.GridColumn9.Tag = "nameID.middle";
            this.GridColumn9.UnboundType = DevExpress.Data.UnboundColumnType.String;
            // 
            // GridColumn10
            // 
            this.GridColumn10.Caption = "Last";
            this.GridColumn10.CustomizationCaption = "Name : Last";
            this.GridColumn10.Name = "GridColumn10";
            this.GridColumn10.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.True;
            this.GridColumn10.Tag = "nameID.last";
            this.GridColumn10.UnboundType = DevExpress.Data.UnboundColumnType.String;
            // 
            // GridColumn11
            // 
            this.GridColumn11.Caption = "Suffix";
            this.GridColumn11.CustomizationCaption = "Name : Suffix";
            this.GridColumn11.Name = "GridColumn11";
            this.GridColumn11.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.True;
            this.GridColumn11.Tag = "nameID.suffix";
            this.GridColumn11.UnboundType = DevExpress.Data.UnboundColumnType.String;
            // 
            // GridColumn12
            // 
            this.GridColumn12.Caption = "Title";
            this.GridColumn12.CustomizationCaption = "Title";
            this.GridColumn12.FieldName = "title";
            this.GridColumn12.Name = "GridColumn12";
            this.GridColumn12.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.True;
            // 
            // GridColumn13
            // 
            this.GridColumn13.Caption = "Phone";
            this.GridColumn13.CustomizationCaption = "Raw Phone Number";
            this.GridColumn13.FieldName = "telephoneID";
            this.GridColumn13.Name = "GridColumn13";
            this.GridColumn13.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.True;
            // 
            // GridColumn14
            // 
            this.GridColumn14.Caption = "Ext";
            this.GridColumn14.CustomizationCaption = "Raw Phone Extension";
            this.GridColumn14.Name = "GridColumn14";
            this.GridColumn14.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.True;
            this.GridColumn14.Tag = "telephoneID.ext";
            this.GridColumn14.UnboundType = DevExpress.Data.UnboundColumnType.String;
            // 
            // GridColumn15
            // 
            this.GridColumn15.Caption = "FAX";
            this.GridColumn15.CustomizationCaption = "Raw FAX number";
            this.GridColumn15.FieldName = "faxID";
            this.GridColumn15.Name = "GridColumn15";
            this.GridColumn15.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.True;
            // 
            // GridColumn16
            // 
            this.GridColumn16.Caption = "Address1";
            this.GridColumn16.CustomizationCaption = "Address : Line 1";
            this.GridColumn16.Name = "GridColumn16";
            this.GridColumn16.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.True;
            this.GridColumn16.Tag = "addressID.line1";
            this.GridColumn16.UnboundType = DevExpress.Data.UnboundColumnType.String;
            // 
            // GridColumn17
            // 
            this.GridColumn17.Caption = "Address2";
            this.GridColumn17.CustomizationCaption = "Address : Line 2";
            this.GridColumn17.Name = "GridColumn17";
            this.GridColumn17.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.True;
            this.GridColumn17.Tag = "addressID.line2";
            this.GridColumn17.UnboundType = DevExpress.Data.UnboundColumnType.String;
            // 
            // GridColumn18
            // 
            this.GridColumn18.Caption = "City";
            this.GridColumn18.CustomizationCaption = "Address : City";
            this.GridColumn18.Name = "GridColumn18";
            this.GridColumn18.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.True;
            this.GridColumn18.Tag = "addressID.city";
            this.GridColumn18.UnboundType = DevExpress.Data.UnboundColumnType.String;
            // 
            // GridColumn19
            // 
            this.GridColumn19.Caption = "State";
            this.GridColumn19.CustomizationCaption = "Address : State";
            this.GridColumn19.Name = "GridColumn19";
            this.GridColumn19.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.True;
            this.GridColumn19.Tag = "addressID.state";
            this.GridColumn19.UnboundType = DevExpress.Data.UnboundColumnType.String;
            // 
            // GridColumn20
            // 
            this.GridColumn20.Caption = "Postal Code";
            this.GridColumn20.CustomizationCaption = "Address : ZIPCode";
            this.GridColumn20.Name = "GridColumn20";
            this.GridColumn20.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.True;
            this.GridColumn20.Tag = "addressID.PostalCode";
            this.GridColumn20.UnboundType = DevExpress.Data.UnboundColumnType.String;
            // 
            // GridColumn21
            // 
            this.GridColumn21.Caption = "Address3";
            this.GridColumn21.CustomizationCaption = "Address : Line 3";
            this.GridColumn21.Name = "GridColumn21";
            this.GridColumn21.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.True;
            this.GridColumn21.Tag = "addressID.line3";
            this.GridColumn21.UnboundType = DevExpress.Data.UnboundColumnType.String;
            // 
            // GridColumn22
            // 
            this.GridColumn22.Caption = "Email";
            this.GridColumn22.CustomizationCaption = "Email Address";
            this.GridColumn22.Name = "GridColumn22";
            this.GridColumn22.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.True;
            this.GridColumn22.Tag = "emailID";
            this.GridColumn22.UnboundType = DevExpress.Data.UnboundColumnType.String;
            this.GridColumn22.Visible = true;
            this.GridColumn22.VisibleIndex = 4;
            // 
            // GridColumn23
            // 
            this.GridColumn23.Caption = "Notes";
            this.GridColumn23.CustomizationCaption = "Notes";
            this.GridColumn23.FieldName = "notes";
            this.GridColumn23.Name = "GridColumn23";
            this.GridColumn23.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.True;
            // 
            // Address
            // 
            this.Address.Caption = "Email Address";
            this.Address.CustomizationCaption = "Email Address";
            this.Address.FieldName = "emailID";
            this.Address.Name = "Address";
            this.Address.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.True;
            // 
            // GroupControl1
            // 
            this.GroupControl1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.GroupControl1.Controls.Add(this.label_notes);
            this.GroupControl1.Controls.Add(this.LabelControl13);
            this.GroupControl1.Controls.Add(this.label_address);
            this.GroupControl1.Controls.Add(this.LabelControl11);
            this.GroupControl1.Controls.Add(this.label_email);
            this.GroupControl1.Controls.Add(this.label_fax);
            this.GroupControl1.Controls.Add(this.label_phone);
            this.GroupControl1.Controls.Add(this.label_title);
            this.GroupControl1.Controls.Add(this.label_name);
            this.GroupControl1.Controls.Add(this.LabelControl5);
            this.GroupControl1.Controls.Add(this.LabelControl4);
            this.GroupControl1.Controls.Add(this.LabelControl3);
            this.GroupControl1.Controls.Add(this.LabelControl2);
            this.GroupControl1.Controls.Add(this.LabelControl1);
            this.GroupControl1.Location = new System.Drawing.Point(0, 152);
            this.GroupControl1.Name = "GroupControl1";
            this.GroupControl1.Size = new System.Drawing.Size(576, 144);
            this.GroupControl1.TabIndex = 1;
            // 
            // label_notes
            // 
            this.label_notes.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label_notes.Location = new System.Drawing.Point(56, 100);
            this.label_notes.Name = "label_notes";
            this.label_notes.Size = new System.Drawing.Size(0, 13);
            this.label_notes.TabIndex = 13;
            // 
            // LabelControl13
            // 
            this.LabelControl13.Location = new System.Drawing.Point(8, 100);
            this.LabelControl13.Name = "LabelControl13";
            this.LabelControl13.Size = new System.Drawing.Size(32, 13);
            this.LabelControl13.TabIndex = 12;
            this.LabelControl13.Text = "Notes:";
            // 
            // label_address
            // 
            this.label_address.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.label_address.Location = new System.Drawing.Point(56, 52);
            this.label_address.Name = "label_address";
            this.label_address.Size = new System.Drawing.Size(0, 13);
            this.label_address.TabIndex = 11;
            // 
            // LabelControl11
            // 
            this.LabelControl11.Location = new System.Drawing.Point(8, 52);
            this.LabelControl11.Name = "LabelControl11";
            this.LabelControl11.Size = new System.Drawing.Size(39, 13);
            this.LabelControl11.TabIndex = 10;
            this.LabelControl11.Text = "Address";
            // 
            // label_email
            // 
            this.label_email.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label_email.Location = new System.Drawing.Point(344, 52);
            this.label_email.Name = "label_email";
            this.label_email.Size = new System.Drawing.Size(0, 13);
            this.label_email.TabIndex = 9;
            // 
            // label_fax
            // 
            this.label_fax.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label_fax.Location = new System.Drawing.Point(344, 38);
            this.label_fax.Name = "label_fax";
            this.label_fax.Size = new System.Drawing.Size(0, 13);
            this.label_fax.TabIndex = 8;
            // 
            // label_phone
            // 
            this.label_phone.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label_phone.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.label_phone.Location = new System.Drawing.Point(344, 24);
            this.label_phone.Name = "label_phone";
            this.label_phone.Size = new System.Drawing.Size(0, 13);
            this.label_phone.TabIndex = 7;
            // 
            // label_title
            // 
            this.label_title.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.label_title.Location = new System.Drawing.Point(56, 38);
            this.label_title.Name = "label_title";
            this.label_title.Size = new System.Drawing.Size(0, 13);
            this.label_title.TabIndex = 6;
            // 
            // label_name
            // 
            this.label_name.Location = new System.Drawing.Point(56, 24);
            this.label_name.Name = "label_name";
            this.label_name.Size = new System.Drawing.Size(0, 13);
            this.label_name.TabIndex = 5;
            // 
            // LabelControl5
            // 
            this.LabelControl5.Location = new System.Drawing.Point(304, 52);
            this.LabelControl5.Name = "LabelControl5";
            this.LabelControl5.Size = new System.Drawing.Size(28, 13);
            this.LabelControl5.TabIndex = 4;
            this.LabelControl5.Text = "E-Mail";
            // 
            // LabelControl4
            // 
            this.LabelControl4.Location = new System.Drawing.Point(304, 38);
            this.LabelControl4.Name = "LabelControl4";
            this.LabelControl4.Size = new System.Drawing.Size(19, 13);
            this.LabelControl4.TabIndex = 3;
            this.LabelControl4.Text = "FAX";
            // 
            // LabelControl3
            // 
            this.LabelControl3.Location = new System.Drawing.Point(304, 24);
            this.LabelControl3.Name = "LabelControl3";
            this.LabelControl3.Size = new System.Drawing.Size(30, 13);
            this.LabelControl3.TabIndex = 2;
            this.LabelControl3.Text = "Phone";
            // 
            // LabelControl2
            // 
            this.LabelControl2.Location = new System.Drawing.Point(8, 38);
            this.LabelControl2.Name = "LabelControl2";
            this.LabelControl2.Size = new System.Drawing.Size(20, 13);
            this.LabelControl2.TabIndex = 1;
            this.LabelControl2.Text = "Title";
            // 
            // LabelControl1
            // 
            this.LabelControl1.Location = new System.Drawing.Point(8, 24);
            this.LabelControl1.Name = "LabelControl1";
            this.LabelControl1.Size = new System.Drawing.Size(27, 13);
            this.LabelControl1.TabIndex = 0;
            this.LabelControl1.Text = "Name";
            // 
            // Page_Contacts
            // 
            this.Controls.Add(this.GroupControl1);
            this.Name = "PageContacts";
            this.Controls.SetChildIndex(this.barDockControlTop, 0);
            this.Controls.SetChildIndex(this.barDockControlBottom, 0);
            this.Controls.SetChildIndex(this.barDockControlRight, 0);
            this.Controls.SetChildIndex(this.barDockControlLeft, 0);
            this.Controls.SetChildIndex(this.GroupControl1, 0);
            this.Controls.SetChildIndex(this.GridControl1, 0);
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.PopupMenu_Items)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.GridControl1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.GridView1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.GroupControl1)).EndInit();
            this.GroupControl1.ResumeLayout(false);
            this.GroupControl1.PerformLayout();
            this.ResumeLayout(false);

        }

        private DevExpress.XtraEditors.GroupControl GroupControl1;
        private DevExpress.XtraEditors.LabelControl label_address;
        private DevExpress.XtraEditors.LabelControl label_email;
        private DevExpress.XtraEditors.LabelControl label_fax;
        private DevExpress.XtraEditors.LabelControl label_name;
        private DevExpress.XtraEditors.LabelControl label_notes;
        private DevExpress.XtraEditors.LabelControl label_phone;
        private DevExpress.XtraEditors.LabelControl label_title;
        private DevExpress.XtraEditors.LabelControl LabelControl1;
        private DevExpress.XtraEditors.LabelControl LabelControl11;
        private DevExpress.XtraEditors.LabelControl LabelControl13;
        private DevExpress.XtraEditors.LabelControl LabelControl2;
        private DevExpress.XtraEditors.LabelControl LabelControl3;
        private DevExpress.XtraEditors.LabelControl LabelControl4;
        private DevExpress.XtraEditors.LabelControl LabelControl5;
        private DevExpress.XtraGrid.Columns.GridColumn GridColumn1;
        private DevExpress.XtraGrid.Columns.GridColumn GridColumn10;
        private DevExpress.XtraGrid.Columns.GridColumn GridColumn11;
        private DevExpress.XtraGrid.Columns.GridColumn GridColumn12;
        private DevExpress.XtraGrid.Columns.GridColumn GridColumn13;
        private DevExpress.XtraGrid.Columns.GridColumn GridColumn14;
        private DevExpress.XtraGrid.Columns.GridColumn GridColumn15;
        private DevExpress.XtraGrid.Columns.GridColumn GridColumn16;
        private DevExpress.XtraGrid.Columns.GridColumn GridColumn17;
        private DevExpress.XtraGrid.Columns.GridColumn GridColumn18;
        private DevExpress.XtraGrid.Columns.GridColumn GridColumn19;
        private DevExpress.XtraGrid.Columns.GridColumn GridColumn2;
        private DevExpress.XtraGrid.Columns.GridColumn GridColumn20;
        private DevExpress.XtraGrid.Columns.GridColumn GridColumn21;
        private DevExpress.XtraGrid.Columns.GridColumn GridColumn22;
        private DevExpress.XtraGrid.Columns.GridColumn GridColumn23;
        private DevExpress.XtraGrid.Columns.GridColumn GridColumn3;
        private DevExpress.XtraGrid.Columns.GridColumn GridColumn4;
        private DevExpress.XtraGrid.Columns.GridColumn GridColumn5;
        private DevExpress.XtraGrid.Columns.GridColumn GridColumn6;
        private DevExpress.XtraGrid.Columns.GridColumn GridColumn7;
        private DevExpress.XtraGrid.Columns.GridColumn GridColumn8;
        private DevExpress.XtraGrid.Columns.GridColumn GridColumn9;

        #endregion Windows Form Designer generated code

    }
}