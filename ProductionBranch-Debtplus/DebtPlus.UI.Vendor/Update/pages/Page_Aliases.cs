﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using DebtPlus.LINQ;

namespace DebtPlus.UI.Vendor.Update.Pages
{
    internal partial class Page_Aliases : Controls.ControlBase
    {
        private vendor vendorRecord = null;
        private BusinessContext bc = null;

        public Page_Aliases() : base()
        {
            InitializeComponent();
            RegisterHandlers();
        }

        /// <summary>
        /// Load the record collection and initialize the page.
        /// This is the same as the "load" event but we don't load these pages so there is no event.
        /// </summary>
        internal override void ReadForm(DebtPlus.LINQ.BusinessContext bc, DebtPlus.LINQ.vendor VendorRecord)
        {
            this.vendorRecord = VendorRecord;
            this.bc = bc;

            // Read the alias list
            aliasesGrid1.ReadForm(bc, vendorRecord);
        }

        /// <summary>
        /// Handle the save operation on the grid
        /// </summary>
        /// <param name="bc"></param>
        internal override void SaveForm(BusinessContext bc)
        {
            // Save the alias list. We have no data other than the grid to process.
            aliasesGrid1.ReadForm(bc, vendorRecord);
        }

        private void RegisterHandlers()
        {
        }

        private void UnRegisterHandlers()
        {
        }
    }
}
