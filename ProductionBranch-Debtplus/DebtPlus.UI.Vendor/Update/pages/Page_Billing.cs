#region "Copyright 2000-2012 DebtPlus, L.L.C."
//{*******************************************************************}
//{                                                                   }
//{       DebtPlus Debt Management System                             }
//{                                                                   }
//{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
//{       ALL RIGHTS RESERVED                                         }
//{                                                                   }
//{   The entire contents of this file is protected by U.S. and       }
//{   International Copyright Laws. Unauthorized reproduction,        }
//{   reverse-engineering, and distribution of all or any portion of  }
//{   the code contained in this file is strictly prohibited and may  }
//{   result in severe civil and criminal penalties and will be       }
//{   prosecuted to the maximum extent possible under the law.        }
//{                                                                   }
//{   RESTRICTIONS                                                    }
//{                                                                   }
//{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
//{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
//{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
//{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
//{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
//{                                                                   }
//{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
//{   ADDITIONAL RESTRICTIONS.                                        }
//{                                                                   }
//{*******************************************************************}
#endregion "Copyright 2000-2012 DebtPlus, L.L.C."

using System;
using System.ComponentModel;
using DebtPlus.LINQ;
using DebtPlus.UI.Vendor.Update.Controls;
using DevExpress.XtraEditors;
using System.Linq;
using System.Collections.Generic;
using System.Reflection;
using System.Text;

namespace DebtPlus.UI.Vendor.Update.Pages
{
    internal partial class Page_Billing : ControlBase
    {
        // Current creditor being edited
        private vendor vendorRecord = null;
        private BusinessContext bc = null;
        private DevExpress.XtraLayout.LayoutControlItem LayoutControlItem2;
        private DevExpress.XtraLayout.LayoutControlItem LayoutControlItem10;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup5;
        private TextEdit NameOnCard;
        private TextEdit textACH_RoutingNumber;
        private DevExpress.XtraLayout.LayoutControlGroup checkingAccountInfoGroup;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem1;
        private DevExpress.XtraLayout.LayoutControlGroup creditCardAccountInfoGroup;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem5;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup8;
        private DateEdit CC_ExpirationDate;
        private TextEdit textACH_AccountNumber;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem6;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem12;
        private CheckEdit checkActiveFlag;
        private DevExpress.XtraLayout.LayoutControlGroup InactiveGroup4;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem17;
        private TextEdit txtCVV;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem15;
        private DevExpress.XtraLayout.LayoutControlItem lookupPaymentType;
        private LabelControl last_billed_date;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem3;
        private LookUpEdit lookUpEdit_CheckingSavings;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem2;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem7;
        private LookUpEdit payment_method_type;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem1;

        public Page_Billing()
            : base()
        {
            InitializeComponent();
            RegisterHandlers();
        }

        #region " Windows Form Designer generated code "

        private DevExpress.XtraLayout.LayoutControl LayoutControl1;
        private DevExpress.XtraLayout.LayoutControlGroup LayoutControlGroup1;
        private DevExpress.XtraLayout.LayoutControlGroup LayoutControlGroup2;
        private DevExpress.XtraLayout.LayoutControlGroup LayoutControlGroup3;
        private DevExpress.XtraLayout.LayoutControlGroup LayoutControlGroup4;
        private DevExpress.XtraLayout.LayoutControlGroup LayoutControlGroup6;
        private DevExpress.XtraLayout.LayoutControlItem LayoutControlItem4;
        private DevExpress.XtraLayout.LayoutControlItem LayoutControlItem14;
        internal VendorProducts VendorProducts1;
        private DevExpress.XtraEditors.SpinEdit MonthlyBillingDay;
        private DevExpress.XtraEditors.CheckEdit checkSupressPrintingPayments;
        private DevExpress.XtraEditors.CheckEdit checkSupressInvoices;

        //Required by the Windows Form Designer
        private System.ComponentModel.IContainer components = null;

        //NOTE: The following procedure is required by the Windows Form Designer
        //It can be modified using the Windows Form Designer.
        //Do not modify it using the code editor.

        //Form overrides dispose to clean up the component list.
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                if (components != null)
                {
                    components.Dispose();
                }
            }
            base.Dispose(disposing);
        }

        [System.Diagnostics.DebuggerStepThrough]
        private void InitializeComponent()
        {
            this.LayoutControlGroup6 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlGroup8 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.LayoutControl1 = new DevExpress.XtraLayout.LayoutControl();
            this.lookUpEdit_CheckingSavings = new DevExpress.XtraEditors.LookUpEdit();
            this.last_billed_date = new DevExpress.XtraEditors.LabelControl();
            this.txtCVV = new DevExpress.XtraEditors.TextEdit();
            this.checkActiveFlag = new DevExpress.XtraEditors.CheckEdit();
            this.CC_ExpirationDate = new DevExpress.XtraEditors.DateEdit();
            this.textACH_AccountNumber = new DevExpress.XtraEditors.TextEdit();
            this.NameOnCard = new DevExpress.XtraEditors.TextEdit();
            this.textACH_RoutingNumber = new DevExpress.XtraEditors.TextEdit();
            this.VendorProducts1 = new DebtPlus.UI.Vendor.Update.Controls.VendorProducts();
            this.checkSupressPrintingPayments = new DevExpress.XtraEditors.CheckEdit();
            this.checkSupressInvoices = new DevExpress.XtraEditors.CheckEdit();
            this.MonthlyBillingDay = new DevExpress.XtraEditors.SpinEdit();
            this.payment_method_type = new DevExpress.XtraEditors.LookUpEdit();
            this.LayoutControlGroup1 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.LayoutControlGroup4 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.LayoutControlItem10 = new DevExpress.XtraLayout.LayoutControlItem();
            this.LayoutControlGroup3 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.LayoutControlItem4 = new DevExpress.XtraLayout.LayoutControlItem();
            this.LayoutControlItem14 = new DevExpress.XtraLayout.LayoutControlItem();
            this.LayoutControlGroup2 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.LayoutControlItem2 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem3 = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem2 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.layoutControlGroup5 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.checkingAccountInfoGroup = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem1 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem7 = new DevExpress.XtraLayout.LayoutControlItem();
            this.creditCardAccountInfoGroup = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem5 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem12 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem15 = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem1 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.InactiveGroup4 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem17 = new DevExpress.XtraLayout.LayoutControlItem();
            this.lookupPaymentType = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem6 = new DevExpress.XtraLayout.LayoutControlItem();
            ((System.ComponentModel.ISupportInitialize)(this.LayoutControlGroup6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LayoutControl1)).BeginInit();
            this.LayoutControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.lookUpEdit_CheckingSavings.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCVV.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkActiveFlag.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.CC_ExpirationDate.Properties.VistaTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.CC_ExpirationDate.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textACH_AccountNumber.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.NameOnCard.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textACH_RoutingNumber.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkSupressPrintingPayments.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkSupressInvoices.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.MonthlyBillingDay.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.payment_method_type.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LayoutControlGroup1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LayoutControlGroup4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LayoutControlItem10)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LayoutControlGroup3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LayoutControlItem4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LayoutControlItem14)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LayoutControlGroup2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LayoutControlItem2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkingAccountInfoGroup)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.creditCardAccountInfoGroup)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem12)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem15)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.InactiveGroup4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem17)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lookupPaymentType)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem6)).BeginInit();
            this.SuspendLayout();
            // 
            // LayoutControlGroup6
            // 
            this.LayoutControlGroup6.CustomizationFormText = "LayoutControlGroup6";
            this.LayoutControlGroup6.Location = new System.Drawing.Point(0, 0);
            this.LayoutControlGroup6.Name = "LayoutControlGroup6";
            this.LayoutControlGroup6.Size = new System.Drawing.Size(50, 25);
            this.LayoutControlGroup6.Text = "LayoutControlGroup6";
            // 
            // layoutControlGroup8
            // 
            this.layoutControlGroup8.CustomizationFormText = "Account Info";
            this.layoutControlGroup8.Location = new System.Drawing.Point(0, 24);
            this.layoutControlGroup8.Name = "layoutControlGroup7";
            this.layoutControlGroup8.Size = new System.Drawing.Size(278, 133);
            this.layoutControlGroup8.Text = "Account Info";
            // 
            // LayoutControl1
            // 
            this.LayoutControl1.Controls.Add(this.lookUpEdit_CheckingSavings);
            this.LayoutControl1.Controls.Add(this.last_billed_date);
            this.LayoutControl1.Controls.Add(this.txtCVV);
            this.LayoutControl1.Controls.Add(this.checkActiveFlag);
            this.LayoutControl1.Controls.Add(this.CC_ExpirationDate);
            this.LayoutControl1.Controls.Add(this.textACH_AccountNumber);
            this.LayoutControl1.Controls.Add(this.NameOnCard);
            this.LayoutControl1.Controls.Add(this.textACH_RoutingNumber);
            this.LayoutControl1.Controls.Add(this.VendorProducts1);
            this.LayoutControl1.Controls.Add(this.checkSupressPrintingPayments);
            this.LayoutControl1.Controls.Add(this.checkSupressInvoices);
            this.LayoutControl1.Controls.Add(this.MonthlyBillingDay);
            this.LayoutControl1.Controls.Add(this.payment_method_type);
            this.LayoutControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.LayoutControl1.Location = new System.Drawing.Point(0, 0);
            this.LayoutControl1.Name = "LayoutControl1";
            this.LayoutControl1.Root = this.LayoutControlGroup1;
            this.LayoutControl1.Size = new System.Drawing.Size(617, 311);
            this.LayoutControl1.TabIndex = 11;
            this.LayoutControl1.Text = "LayoutControl1";
            // 
            // lookUpEdit_CheckingSavings
            // 
            this.lookUpEdit_CheckingSavings.Location = new System.Drawing.Point(419, 233);
            this.lookUpEdit_CheckingSavings.Name = "lookUpEdit_CheckingSavings";
            this.lookUpEdit_CheckingSavings.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.lookUpEdit_CheckingSavings.Properties.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("Id", "ID", 20, DevExpress.Utils.FormatType.None, "", false, DevExpress.Utils.HorzAlignment.Near, DevExpress.Data.ColumnSortOrder.None, DevExpress.Utils.DefaultBoolean.True),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("description", "Description", 20, DevExpress.Utils.FormatType.None, "", true, DevExpress.Utils.HorzAlignment.Near, DevExpress.Data.ColumnSortOrder.Ascending, DevExpress.Utils.DefaultBoolean.True)});
            this.lookUpEdit_CheckingSavings.Properties.DisplayMember = "description";
            this.lookUpEdit_CheckingSavings.Properties.NullText = "";
            this.lookUpEdit_CheckingSavings.Properties.ShowFooter = false;
            this.lookUpEdit_CheckingSavings.Properties.ShowHeader = false;
            this.lookUpEdit_CheckingSavings.Properties.ValueMember = "Id";
            this.lookUpEdit_CheckingSavings.Size = new System.Drawing.Size(145, 20);
            this.lookUpEdit_CheckingSavings.StyleController = this.LayoutControl1;
            this.lookUpEdit_CheckingSavings.TabIndex = 29;
            // 
            // last_billed_date
            // 
            this.last_billed_date.Location = new System.Drawing.Point(407, 68);
            this.last_billed_date.Name = "last_billed_date";
            this.last_billed_date.Size = new System.Drawing.Size(169, 13);
            this.last_billed_date.StyleController = this.LayoutControl1;
            this.last_billed_date.TabIndex = 28;
            // 
            // txtCVV
            // 
            this.txtCVV.Location = new System.Drawing.Point(419, 349);
            this.txtCVV.Name = "txtCVV";
            this.txtCVV.Properties.Mask.EditMask = "\\d\\d\\d(\\d)?";
            this.txtCVV.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.RegEx;
            this.txtCVV.Properties.MaxLength = 4;
            this.txtCVV.Size = new System.Drawing.Size(69, 20);
            this.txtCVV.StyleController = this.LayoutControl1;
            this.txtCVV.TabIndex = 22;
            // 
            // checkActiveFlag
            // 
            this.checkActiveFlag.Location = new System.Drawing.Point(323, 417);
            this.checkActiveFlag.Name = "checkActiveFlag";
            this.checkActiveFlag.Properties.Caption = "Active";
            this.checkActiveFlag.Size = new System.Drawing.Size(241, 20);
            this.checkActiveFlag.StyleController = this.LayoutControl1;
            this.checkActiveFlag.TabIndex = 21;
            // 
            // CC_ExpirationDate
            // 
            this.CC_ExpirationDate.EditValue = null;
            this.CC_ExpirationDate.Location = new System.Drawing.Point(419, 325);
            this.CC_ExpirationDate.Name = "CC_ExpirationDate";
            this.CC_ExpirationDate.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.CC_ExpirationDate.Properties.VistaTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.CC_ExpirationDate.Size = new System.Drawing.Size(145, 20);
            this.CC_ExpirationDate.StyleController = this.LayoutControl1;
            this.CC_ExpirationDate.TabIndex = 18;
            // 
            // textACH_AccountNumber
            // 
            this.textACH_AccountNumber.Location = new System.Drawing.Point(407, 153);
            this.textACH_AccountNumber.Name = "textACH_AccountNumber";
            this.textACH_AccountNumber.Properties.Mask.EditMask = "f0";
            this.textACH_AccountNumber.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.textACH_AccountNumber.Size = new System.Drawing.Size(169, 20);
            this.textACH_AccountNumber.StyleController = this.LayoutControl1;
            this.textACH_AccountNumber.TabIndex = 13;
            // 
            // NameOnCard
            // 
            this.NameOnCard.Location = new System.Drawing.Point(419, 301);
            this.NameOnCard.Name = "NameOnCard";
            this.NameOnCard.Size = new System.Drawing.Size(145, 20);
            this.NameOnCard.StyleController = this.LayoutControl1;
            this.NameOnCard.TabIndex = 12;
            // 
            // textACH_RoutingNumber
            // 
            this.textACH_RoutingNumber.Location = new System.Drawing.Point(419, 209);
            this.textACH_RoutingNumber.Name = "textACH_RoutingNumber";
            this.textACH_RoutingNumber.Properties.Mask.EditMask = "f0";
            this.textACH_RoutingNumber.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.textACH_RoutingNumber.Properties.MaxLength = 9;
            this.textACH_RoutingNumber.Size = new System.Drawing.Size(145, 20);
            this.textACH_RoutingNumber.StyleController = this.LayoutControl1;
            this.textACH_RoutingNumber.TabIndex = 11;
            // 
            // VendorProducts1
            // 
            this.VendorProducts1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.VendorProducts1.Location = new System.Drawing.Point(24, 49);
            this.VendorProducts1.Name = "VendorProducts1";
            this.VendorProducts1.Size = new System.Drawing.Size(259, 313);
            this.VendorProducts1.TabIndex = 23;
            // 
            // checkSupressPrintingPayments
            // 
            this.checkSupressPrintingPayments.Location = new System.Drawing.Point(24, 405);
            this.checkSupressPrintingPayments.Name = "checkSupressPrintingPayments";
            this.checkSupressPrintingPayments.Properties.Caption = "Suppress Printing Invoices";
            this.checkSupressPrintingPayments.Size = new System.Drawing.Size(259, 20);
            this.checkSupressPrintingPayments.StyleController = this.LayoutControl1;
            this.checkSupressPrintingPayments.TabIndex = 0;
            // 
            // checkSupressInvoices
            // 
            this.checkSupressInvoices.Location = new System.Drawing.Point(24, 429);
            this.checkSupressInvoices.Name = "checkSupressInvoices";
            this.checkSupressInvoices.Properties.Caption = "Suppress Generating New Invoices";
            this.checkSupressInvoices.Size = new System.Drawing.Size(259, 20);
            this.checkSupressInvoices.StyleController = this.LayoutControl1;
            this.checkSupressInvoices.TabIndex = 0;
            // 
            // MonthlyBillingDay
            // 
            this.MonthlyBillingDay.EditValue = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.MonthlyBillingDay.Location = new System.Drawing.Point(407, 44);
            this.MonthlyBillingDay.Name = "MonthlyBillingDay";
            this.MonthlyBillingDay.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.MonthlyBillingDay.Properties.DisplayFormat.FormatString = "f0";
            this.MonthlyBillingDay.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.MonthlyBillingDay.Properties.EditFormat.FormatString = "f0";
            this.MonthlyBillingDay.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.MonthlyBillingDay.Properties.IsFloatValue = false;
            this.MonthlyBillingDay.Properties.Mask.BeepOnError = true;
            this.MonthlyBillingDay.Properties.Mask.EditMask = "N0";
            this.MonthlyBillingDay.Properties.MaxLength = 2;
            this.MonthlyBillingDay.Properties.MaxValue = new decimal(new int[] {
            31,
            0,
            0,
            0});
            this.MonthlyBillingDay.Properties.MinValue = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.MonthlyBillingDay.Size = new System.Drawing.Size(50, 20);
            this.MonthlyBillingDay.StyleController = this.LayoutControl1;
            this.MonthlyBillingDay.TabIndex = 3;
            // 
            // payment_method_type
            // 
            this.payment_method_type.Location = new System.Drawing.Point(407, 129);
            this.payment_method_type.Name = "payment_method_type";
            this.payment_method_type.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.False;
            this.payment_method_type.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.payment_method_type.Properties.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("Id", "Id", 20, DevExpress.Utils.FormatType.None, "", false, DevExpress.Utils.HorzAlignment.Near, DevExpress.Data.ColumnSortOrder.None, DevExpress.Utils.DefaultBoolean.True),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("description", "Description", 20, DevExpress.Utils.FormatType.None, "", true, DevExpress.Utils.HorzAlignment.Near, DevExpress.Data.ColumnSortOrder.Ascending, DevExpress.Utils.DefaultBoolean.True)});
            this.payment_method_type.Properties.DisplayMember = "description";
            this.payment_method_type.Properties.NullText = "";
            this.payment_method_type.Properties.ShowFooter = false;
            this.payment_method_type.Properties.ShowHeader = false;
            this.payment_method_type.Properties.SortColumnIndex = 1;
            this.payment_method_type.Properties.ValueMember = "Id";
            this.payment_method_type.Size = new System.Drawing.Size(169, 20);
            this.payment_method_type.StyleController = this.LayoutControl1;
            this.payment_method_type.TabIndex = 27;
            // 
            // LayoutControlGroup1
            // 
            this.LayoutControlGroup1.CustomizationFormText = "LayoutControlGroup1";
            this.LayoutControlGroup1.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.True;
            this.LayoutControlGroup1.GroupBordersVisible = false;
            this.LayoutControlGroup1.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.LayoutControlGroup4,
            this.LayoutControlGroup3,
            this.LayoutControlGroup2,
            this.layoutControlGroup5});
            this.LayoutControlGroup1.Location = new System.Drawing.Point(0, 0);
            this.LayoutControlGroup1.Name = "LayoutControlGroup1";
            this.LayoutControlGroup1.Size = new System.Drawing.Size(600, 473);
            this.LayoutControlGroup1.Text = "LayoutControlGroup1";
            this.LayoutControlGroup1.TextVisible = false;
            // 
            // LayoutControlGroup4
            // 
            this.LayoutControlGroup4.CustomizationFormText = "Products And Services";
            this.LayoutControlGroup4.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.LayoutControlItem10});
            this.LayoutControlGroup4.Location = new System.Drawing.Point(0, 0);
            this.LayoutControlGroup4.Name = "LayoutControlGroup4";
            this.LayoutControlGroup4.Size = new System.Drawing.Size(287, 361);
            this.LayoutControlGroup4.Text = "Products And Services";
            // 
            // LayoutControlItem10
            // 
            this.LayoutControlItem10.Control = this.VendorProducts1;
            this.LayoutControlItem10.CustomizationFormText = "LayoutControlItem10";
            this.LayoutControlItem10.Location = new System.Drawing.Point(0, 5);
            this.LayoutControlItem10.Name = "LayoutControlItem10";
            this.LayoutControlItem10.Size = new System.Drawing.Size(263, 317);
            this.LayoutControlItem10.Text = "LayoutControlItem10";
            this.LayoutControlItem10.TextSize = new System.Drawing.Size(0, 0);
            this.LayoutControlItem10.TextToControlDistance = 0;
            this.LayoutControlItem10.TextVisible = false;
            // 
            // LayoutControlGroup3
            // 
            this.LayoutControlGroup3.CustomizationFormText = "Invoicing Options";
            this.LayoutControlGroup3.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.LayoutControlItem4,
            this.LayoutControlItem14});
            this.LayoutControlGroup3.Location = new System.Drawing.Point(0, 361);
            this.LayoutControlGroup3.Name = "LayoutControlGroup3";
            this.LayoutControlGroup3.Size = new System.Drawing.Size(287, 92);
            this.LayoutControlGroup3.Text = "Invoicing Options";
            // 
            // LayoutControlItem4
            // 
            this.LayoutControlItem4.Control = this.checkSupressPrintingPayments;
            this.LayoutControlItem4.CustomizationFormText = "Suppress Printng Invoice";
            this.LayoutControlItem4.Location = new System.Drawing.Point(0, 0);
            this.LayoutControlItem4.Name = "LayoutControlItem4";
            this.LayoutControlItem4.Size = new System.Drawing.Size(263, 24);
            this.LayoutControlItem4.Text = "Suppress Printng Invoice";
            this.LayoutControlItem4.TextSize = new System.Drawing.Size(0, 0);
            this.LayoutControlItem4.TextToControlDistance = 0;
            this.LayoutControlItem4.TextVisible = false;
            // 
            // LayoutControlItem14
            // 
            this.LayoutControlItem14.Control = this.checkSupressInvoices;
            this.LayoutControlItem14.CustomizationFormText = "Suppress Generating New Invoices";
            this.LayoutControlItem14.Location = new System.Drawing.Point(0, 24);
            this.LayoutControlItem14.Name = "LayoutControlItem14";
            this.LayoutControlItem14.Size = new System.Drawing.Size(263, 24);
            this.LayoutControlItem14.Text = "Suppress Generating New Invoices";
            this.LayoutControlItem14.TextSize = new System.Drawing.Size(0, 0);
            this.LayoutControlItem14.TextToControlDistance = 0;
            this.LayoutControlItem14.TextVisible = false;
            // 
            // LayoutControlGroup2
            // 
            this.LayoutControlGroup2.CustomizationFormText = "Invoice and Billing Information";
            this.LayoutControlGroup2.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.LayoutControlItem2,
            this.layoutControlItem3,
            this.emptySpaceItem2});
            this.LayoutControlGroup2.Location = new System.Drawing.Point(287, 0);
            this.LayoutControlGroup2.Name = "LayoutControlGroup2";
            this.LayoutControlGroup2.Size = new System.Drawing.Size(293, 85);
            this.LayoutControlGroup2.Text = "Invoice and Billing Information";
            // 
            // LayoutControlItem2
            // 
            this.LayoutControlItem2.Control = this.MonthlyBillingDay;
            this.LayoutControlItem2.CustomizationFormText = "Monthly Billing Day:";
            this.LayoutControlItem2.Location = new System.Drawing.Point(0, 0);
            this.LayoutControlItem2.Name = "LayoutControlItem2";
            this.LayoutControlItem2.Size = new System.Drawing.Size(150, 24);
            this.LayoutControlItem2.Text = "Monthly Billing Day:";
            this.LayoutControlItem2.TextSize = new System.Drawing.Size(93, 13);
            // 
            // layoutControlItem3
            // 
            this.layoutControlItem3.Control = this.last_billed_date;
            this.layoutControlItem3.CustomizationFormText = "Last Billed Date";
            this.layoutControlItem3.Location = new System.Drawing.Point(0, 24);
            this.layoutControlItem3.Name = "layoutControlItem3";
            this.layoutControlItem3.Size = new System.Drawing.Size(269, 17);
            this.layoutControlItem3.Text = "Last Billed Date";
            this.layoutControlItem3.TextSize = new System.Drawing.Size(93, 13);
            // 
            // emptySpaceItem2
            // 
            this.emptySpaceItem2.AllowHotTrack = false;
            this.emptySpaceItem2.CustomizationFormText = "emptySpaceItem2";
            this.emptySpaceItem2.Location = new System.Drawing.Point(150, 0);
            this.emptySpaceItem2.Name = "emptySpaceItem2";
            this.emptySpaceItem2.Size = new System.Drawing.Size(119, 24);
            this.emptySpaceItem2.Text = "emptySpaceItem2";
            this.emptySpaceItem2.TextSize = new System.Drawing.Size(0, 0);
            // 
            // layoutControlGroup5
            // 
            this.layoutControlGroup5.CustomizationFormText = "Payment Method";
            this.layoutControlGroup5.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.checkingAccountInfoGroup,
            this.creditCardAccountInfoGroup,
            this.InactiveGroup4,
            this.lookupPaymentType,
            this.layoutControlItem6});
            this.layoutControlGroup5.Location = new System.Drawing.Point(287, 85);
            this.layoutControlGroup5.Name = "layoutControlGroup5";
            this.layoutControlGroup5.Size = new System.Drawing.Size(293, 368);
            this.layoutControlGroup5.Text = "Payment Method";
            // 
            // checkingAccountInfoGroup
            // 
            this.checkingAccountInfoGroup.CustomizationFormText = "Account Info";
            this.checkingAccountInfoGroup.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem1,
            this.layoutControlItem7});
            this.checkingAccountInfoGroup.Location = new System.Drawing.Point(0, 48);
            this.checkingAccountInfoGroup.Name = "checkingAccountInfoGroup";
            this.checkingAccountInfoGroup.Size = new System.Drawing.Size(269, 92);
            this.checkingAccountInfoGroup.Text = "Account Info";
            // 
            // layoutControlItem1
            // 
            this.layoutControlItem1.Control = this.textACH_RoutingNumber;
            this.layoutControlItem1.CustomizationFormText = "Routing Number";
            this.layoutControlItem1.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem1.Name = "layoutControlItem1";
            this.layoutControlItem1.Size = new System.Drawing.Size(245, 24);
            this.layoutControlItem1.Text = "Routing Number";
            this.layoutControlItem1.TextSize = new System.Drawing.Size(93, 13);
            // 
            // layoutControlItem7
            // 
            this.layoutControlItem7.Control = this.lookUpEdit_CheckingSavings;
            this.layoutControlItem7.CustomizationFormText = "Type";
            this.layoutControlItem7.Location = new System.Drawing.Point(0, 24);
            this.layoutControlItem7.Name = "layoutControlItem7";
            this.layoutControlItem7.Size = new System.Drawing.Size(245, 24);
            this.layoutControlItem7.Text = "Type";
            this.layoutControlItem7.TextSize = new System.Drawing.Size(93, 13);
            // 
            // creditCardAccountInfoGroup
            // 
            this.creditCardAccountInfoGroup.CustomizationFormText = "Account Info";
            this.creditCardAccountInfoGroup.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem5,
            this.layoutControlItem12,
            this.layoutControlItem15,
            this.emptySpaceItem1});
            this.creditCardAccountInfoGroup.Location = new System.Drawing.Point(0, 140);
            this.creditCardAccountInfoGroup.Name = "creditCardAccountInfoGroup";
            this.creditCardAccountInfoGroup.Size = new System.Drawing.Size(269, 116);
            this.creditCardAccountInfoGroup.Text = "Account Info";
            // 
            // layoutControlItem5
            // 
            this.layoutControlItem5.Control = this.NameOnCard;
            this.layoutControlItem5.CustomizationFormText = "Name On Card";
            this.layoutControlItem5.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem5.Name = "layoutControlItem5";
            this.layoutControlItem5.Size = new System.Drawing.Size(245, 24);
            this.layoutControlItem5.Text = "Name On Card";
            this.layoutControlItem5.TextSize = new System.Drawing.Size(93, 13);
            // 
            // layoutControlItem12
            // 
            this.layoutControlItem12.Control = this.CC_ExpirationDate;
            this.layoutControlItem12.CustomizationFormText = "CVV#";
            this.layoutControlItem12.Location = new System.Drawing.Point(0, 24);
            this.layoutControlItem12.Name = "layoutControlItem12";
            this.layoutControlItem12.Size = new System.Drawing.Size(245, 24);
            this.layoutControlItem12.Text = "Expiration";
            this.layoutControlItem12.TextSize = new System.Drawing.Size(93, 13);
            // 
            // layoutControlItem15
            // 
            this.layoutControlItem15.Control = this.txtCVV;
            this.layoutControlItem15.CustomizationFormText = "CVV#";
            this.layoutControlItem15.Location = new System.Drawing.Point(0, 48);
            this.layoutControlItem15.Name = "layoutControlItem15";
            this.layoutControlItem15.Size = new System.Drawing.Size(169, 24);
            this.layoutControlItem15.Text = "CVV#";
            this.layoutControlItem15.TextSize = new System.Drawing.Size(93, 13);
            // 
            // emptySpaceItem1
            // 
            this.emptySpaceItem1.AllowHotTrack = false;
            this.emptySpaceItem1.CustomizationFormText = "emptySpaceItem1";
            this.emptySpaceItem1.Location = new System.Drawing.Point(169, 48);
            this.emptySpaceItem1.Name = "emptySpaceItem1";
            this.emptySpaceItem1.Size = new System.Drawing.Size(76, 24);
            this.emptySpaceItem1.Text = "emptySpaceItem1";
            this.emptySpaceItem1.TextSize = new System.Drawing.Size(0, 0);
            // 
            // InactiveGroup4
            // 
            this.InactiveGroup4.CustomizationFormText = "layoutControlGroup7";
            this.InactiveGroup4.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem17});
            this.InactiveGroup4.Location = new System.Drawing.Point(0, 256);
            this.InactiveGroup4.Name = "InactiveGroup4";
            this.InactiveGroup4.Size = new System.Drawing.Size(269, 68);
            this.InactiveGroup4.Text = "Active Status";
            // 
            // layoutControlItem17
            // 
            this.layoutControlItem17.Control = this.checkActiveFlag;
            this.layoutControlItem17.CustomizationFormText = "layoutControlItem17";
            this.layoutControlItem17.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem17.Name = "layoutControlItem17";
            this.layoutControlItem17.Size = new System.Drawing.Size(245, 24);
            this.layoutControlItem17.Text = "layoutControlItem17";
            this.layoutControlItem17.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem17.TextToControlDistance = 0;
            this.layoutControlItem17.TextVisible = false;
            // 
            // lookupPaymentType
            // 
            this.lookupPaymentType.Control = this.payment_method_type;
            this.lookupPaymentType.CustomizationFormText = "Type";
            this.lookupPaymentType.Location = new System.Drawing.Point(0, 0);
            this.lookupPaymentType.Name = "lookupPaymentType";
            this.lookupPaymentType.Size = new System.Drawing.Size(269, 24);
            this.lookupPaymentType.Text = "Type";
            this.lookupPaymentType.TextSize = new System.Drawing.Size(93, 13);
            // 
            // layoutControlItem6
            // 
            this.layoutControlItem6.Control = this.textACH_AccountNumber;
            this.layoutControlItem6.CustomizationFormText = "layoutControlItem6";
            this.layoutControlItem6.Location = new System.Drawing.Point(0, 24);
            this.layoutControlItem6.Name = "layoutControlItem6";
            this.layoutControlItem6.Size = new System.Drawing.Size(269, 24);
            this.layoutControlItem6.Text = "Account Number";
            this.layoutControlItem6.TextSize = new System.Drawing.Size(93, 13);
            // 
            // Page_Billing
            // 
            this.Controls.Add(this.LayoutControl1);
            this.Name = "Page_Billing";
            this.Size = new System.Drawing.Size(617, 311);
            ((System.ComponentModel.ISupportInitialize)(this.LayoutControlGroup6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LayoutControl1)).EndInit();
            this.LayoutControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.lookUpEdit_CheckingSavings.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCVV.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkActiveFlag.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.CC_ExpirationDate.Properties.VistaTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.CC_ExpirationDate.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textACH_AccountNumber.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.NameOnCard.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textACH_RoutingNumber.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkSupressPrintingPayments.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkSupressInvoices.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.MonthlyBillingDay.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.payment_method_type.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LayoutControlGroup1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LayoutControlGroup4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LayoutControlItem10)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LayoutControlGroup3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LayoutControlItem4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LayoutControlItem14)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LayoutControlGroup2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LayoutControlItem2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkingAccountInfoGroup)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.creditCardAccountInfoGroup)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem12)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem15)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.InactiveGroup4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem17)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lookupPaymentType)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem6)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion " Windows Form Designer generated code "

        internal override void ReadForm(DebtPlus.LINQ.BusinessContext bc, DebtPlus.LINQ.vendor VendorRecord)
        {
            this.bc = bc;
            this.vendorRecord = VendorRecord;
            UnRegisterHandlers();

            // Set the contribution cycles
            payment_method_type.Properties.DataSource = DebtPlus.LINQ.InMemory.vendorBillingModes.getList().Where(s => s.ActiveFlag).ToList();
            lookUpEdit_CheckingSavings.Properties.DataSource = DebtPlus.LINQ.InMemory.ACHAccountTypes.getList();

            try
            {
                // Load the other controls
                payment_method_type.EditValue        = vendorRecord.BillingMode;
                checkActiveFlag.Checked              = vendorRecord.ActiveFlag;
                checkSupressInvoices.Checked         = vendorRecord.SupressInvoices;
                checkSupressPrintingPayments.Checked = vendorRecord.SupressPrintingPayments;
                MonthlyBillingDay.EditValue          = vendorRecord.MonthlyBillingDay;
                NameOnCard.EditValue                 = vendorRecord.NameOnCard;
                CC_ExpirationDate.EditValue          = vendorRecord.CC_ExpirationDate;
                txtCVV.EditValue                     = vendorRecord.CC_CVV;
                textACH_RoutingNumber.EditValue      = vendorRecord.ACH_RoutingNumber;
                textACH_AccountNumber.EditValue      = vendorRecord.ACH_AccountNumber;
                lookUpEdit_CheckingSavings.EditValue = vendorRecord.ACH_CheckingSavings;

                // Load the list of products
                VendorProducts1.ReadForm(bc, vendorRecord);

                // Correct the display
                ChangeBillingDisplay(vendorRecord.BillingMode);
            }
            finally
            {
                RegisterHandlers();
            }
        }

        private void ChangeBillingDisplay(string EditValue)
        {
            // ACH
            if (string.Equals(EditValue ?? string.Empty, DebtPlus.LINQ.InMemory.vendorBillingMode.TypeACH))
            {
                checkingAccountInfoGroup.Visibility = DevExpress.XtraLayout.Utils.LayoutVisibility.Always;
                creditCardAccountInfoGroup.Visibility = DevExpress.XtraLayout.Utils.LayoutVisibility.Never;
                return;
            }

            // Credit Card
            if (string.Equals(EditValue ?? string.Empty, DebtPlus.LINQ.InMemory.vendorBillingMode.TypeCreditCard))
            {
                checkingAccountInfoGroup.Visibility = DevExpress.XtraLayout.Utils.LayoutVisibility.Never;
                creditCardAccountInfoGroup.Visibility = DevExpress.XtraLayout.Utils.LayoutVisibility.Always;
                return;
            }

            // NONE
            checkingAccountInfoGroup.Visibility = DevExpress.XtraLayout.Utils.LayoutVisibility.Never;
            creditCardAccountInfoGroup.Visibility = DevExpress.XtraLayout.Utils.LayoutVisibility.Never;
        }

        internal override void SaveForm(DebtPlus.LINQ.BusinessContext bc)
        {
            vendorRecord.ActiveFlag              = checkActiveFlag.Checked;
            vendorRecord.SupressInvoices         = checkSupressInvoices.Checked;
            vendorRecord.SupressPrintingPayments = checkSupressPrintingPayments.Checked;
            vendorRecord.MonthlyBillingDay       = DebtPlus.Utils.Nulls.v_Int32(MonthlyBillingDay.EditValue);
            vendorRecord.NameOnCard              = DebtPlus.Utils.Nulls.v_String(NameOnCard.EditValue);
            vendorRecord.CC_ExpirationDate       = DebtPlus.Utils.Nulls.v_DateTime(CC_ExpirationDate.EditValue);
            vendorRecord.CC_CVV                  = DebtPlus.Utils.Nulls.v_String(txtCVV.EditValue);
            vendorRecord.ACH_RoutingNumber       = DebtPlus.Utils.Nulls.v_String(textACH_RoutingNumber.EditValue);
            vendorRecord.ACH_AccountNumber       = DebtPlus.Utils.Nulls.v_String(textACH_AccountNumber.EditValue);
            vendorRecord.BillingMode             = DebtPlus.Utils.Nulls.v_String(payment_method_type.EditValue) ?? DebtPlus.LINQ.InMemory.vendorBillingMode.TypeNone;
            vendorRecord.ACH_CheckingSavings     = DebtPlus.Utils.Nulls.v_Char(lookUpEdit_CheckingSavings.EditValue).GetValueOrDefault('C');

            // Save the controls
            VendorProducts1.SaveForm(bc);
        }

        private void RegisterHandlers()
        {
            payment_method_type.EditValueChanged += edit_type_EditValueChanged;
        }

        private void UnRegisterHandlers()
        {
            payment_method_type.EditValueChanged -= edit_type_EditValueChanged;
        }

        private void edit_type_EditValueChanged(object sender, EventArgs e)
        {
            ChangeBillingDisplay(DebtPlus.Utils.Nulls.v_String(payment_method_type.EditValue) ?? string.Empty);
        }

#if false // Cute but not needed
        /// <summary>
        /// Shallowly compare objects
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="Object1"></param>
        /// <param name="object2"></param>
        /// <returns></returns>
        static bool Compare<T>(T Object1, T object2)
        {
            // true if both objects are null.
            if (Object1 == null && object2 == null)
                return true;

            // false if either object is null.
            if (Object1 == null || object2 == null)
                return false;

            //Get the type of the object
            Type type = typeof(T);

            // Loop through each properties inside class and get values for the property from both the objects and compare
            foreach (System.Reflection.PropertyInfo property in type.GetProperties())
            {
                if (property.Name != "ExtensionData")
                {
                    string Object1Value = string.Empty;
                    string Object2Value = string.Empty;
                    if (type.GetProperty(property.Name).GetValue(Object1, null) != null)
                        Object1Value = type.GetProperty(property.Name).GetValue(Object1, null).ToString();
                    if (type.GetProperty(property.Name).GetValue(object2, null) != null)
                        Object2Value = type.GetProperty(property.Name).GetValue(object2, null).ToString();
                    if (Object1Value.Trim() != Object2Value.Trim())
                    {
                        return false;
                    }
                }
            }
            return true;
        }
#endif
    }
}
