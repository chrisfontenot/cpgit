﻿namespace DebtPlus.UI.Vendor.Update.Pages
{
    partial class Page_Aliases
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            try
            {
                if (disposing)
                {
                    if (components != null) components.Dispose();

                    // These are just pointers. Clear them for now.
                    vendorRecord = null;
                    bc = null;
                }
                components = null;
            }
            finally
            {
                base.Dispose(disposing);
            }
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.labelControl1 = new DevExpress.XtraEditors.LabelControl();
            this.aliasesGrid1 = new DebtPlus.UI.Vendor.Update.Pages.AliasesGrid();
            this.SuspendLayout();
            // 
            // labelControl1
            // 
            this.labelControl1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.labelControl1.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.labelControl1.Appearance.TextOptions.Trimming = DevExpress.Utils.Trimming.None;
            this.labelControl1.Appearance.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Top;
            this.labelControl1.Appearance.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.labelControl1.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.labelControl1.Location = new System.Drawing.Point(10, 10);
            this.labelControl1.Name = "labelControl1";
            this.labelControl1.Size = new System.Drawing.Size(530, 26);
            this.labelControl1.TabIndex = 0;
            this.labelControl1.Text = "This form lists the alias names by which the vendor may have been known. You are " +
    "free to use any additional names or sequences by which this vendor may be found." +
    "";
            this.labelControl1.UseMnemonic = false;
            // 
            // aliasesGrid1
            // 
            this.aliasesGrid1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.aliasesGrid1.Location = new System.Drawing.Point(4, 42);
            this.aliasesGrid1.Name = "aliasesGrid1";
            this.aliasesGrid1.Size = new System.Drawing.Size(543, 179);
            this.aliasesGrid1.TabIndex = 1;
            // 
            // Page_Aliases
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.aliasesGrid1);
            this.Controls.Add(this.labelControl1);
            this.Name = "Page_Aliases";
            this.Size = new System.Drawing.Size(550, 224);
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraEditors.LabelControl labelControl1;
        private AliasesGrid aliasesGrid1;
    }
}
