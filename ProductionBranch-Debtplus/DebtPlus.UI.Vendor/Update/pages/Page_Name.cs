#region "Copyright 2000-2012 DebtPlus, L.L.C."

//{*******************************************************************}
//{                                                                   }
//{       DebtPlus Debt Management System                             }
//{                                                                   }
//{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
//{       ALL RIGHTS RESERVED                                         }
//{                                                                   }
//{   The entire contents of this file is protected by U.S. and       }
//{   International Copyright Laws. Unauthorized reproduction,        }
//{   reverse-engineering, and distribution of all or any portion of  }
//{   the code contained in this file is strictly prohibited and may  }
//{   result in severe civil and criminal penalties and will be       }
//{   prosecuted to the maximum extent possible under the law.        }
//{                                                                   }
//{   RESTRICTIONS                                                    }
//{                                                                   }
//{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
//{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
//{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
//{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
//{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
//{                                                                   }
//{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
//{   ADDITIONAL RESTRICTIONS.                                        }
//{                                                                   }
//{*******************************************************************}

#endregion "Copyright 2000-2012 DebtPlus, L.L.C."

using System;
using DebtPlus.LINQ;
using DebtPlus.UI.Vendor.Update.Controls;

namespace DebtPlus.UI.Vendor.Update.Pages
{
    internal partial class Page_Name : ControlBase
    {
        // Current creditor record
        private vendor vendorRecord = null;
        private DevExpress.XtraEditors.PanelControl panelControl1;
        private AddressInformation AddressInformation3;
        private AddressInformation AddressInformation2;
        private AddressInformation AddressInformation1;
        private BusinessContext bc = null;

        public Page_Name()
            : base()
        {
            InitializeComponent();
            RegisterHandlers();
        }

        internal override void ReadForm(DebtPlus.LINQ.BusinessContext bc, DebtPlus.LINQ.vendor VendorRecord)
        {
            this.vendorRecord = VendorRecord;
            this.bc = bc;

            UnRegisterHandlers();
            try
            {
                Vendor_name.EditValue      = vendorRecord.Name;
                textEdit_website.EditValue = VendorRecord.website;

                // Read the address entries
                AddressInformation1.ReadForm(bc, vendorRecord);
                AddressInformation2.ReadForm(bc, vendorRecord);
                AddressInformation3.ReadForm(bc, vendorRecord);
            }
            finally
            {
                RegisterHandlers();
            }
        }

        /// <summary>
        /// Save the form data
        /// </summary>
        internal override void SaveForm(DebtPlus.LINQ.BusinessContext bc)
        {
            vendorRecord.Name    = DebtPlus.Utils.Nulls.v_String(Vendor_name.EditValue);
            vendorRecord.website = DebtPlus.Utils.Nulls.v_String(textEdit_website.EditValue);

            // Save the address information if needed
            AddressInformation1.SaveForm(bc);
            AddressInformation2.SaveForm(bc);
            AddressInformation3.SaveForm(bc);

            UnRegisterHandlers();
        }

        private void RegisterHandlers()
        {
            textEdit_website.Validating += textEdit_website_Validating;
            panelControl1.Resize        += panelControl1_Resize;
        }

        private void UnRegisterHandlers()
        {
            textEdit_website.Validating -= textEdit_website_Validating;
            panelControl1.Resize        -= panelControl1_Resize;
        }

        private void panelControl1_Resize(object sender, EventArgs e)
        {
            // Divide the panel into three areas. Assign the addresses to each area
            Int32 vert = panelControl1.Size.Height / 3;
            Int32 horiz = panelControl1.Size.Width;
            Int32 x = 0;
            Int32 y = 0;

            AddressInformation1.Location = new System.Drawing.Point(x, y);
            AddressInformation1.Size = new System.Drawing.Size(horiz, vert);
            y += vert;

            AddressInformation2.Location = new System.Drawing.Point(x, y);
            AddressInformation2.Size = new System.Drawing.Size(horiz, vert);
            y += vert;

            AddressInformation3.Location = new System.Drawing.Point(x, y);
            AddressInformation3.Size = new System.Drawing.Size(horiz, vert);
        }

        /// <summary>
        /// Validate the website entry
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void textEdit_website_Validating(object sender, System.ComponentModel.CancelEventArgs e)
        {
            // Find the website from the input field.
            var str = DebtPlus.Utils.Nulls.v_String(textEdit_website.EditValue);
            if (string.IsNullOrEmpty(str))
            {
                textEdit_website.ErrorText = string.Empty;
                return;
            }

            try
            {
                var uri = new System.Uri(str);
                if (!uri.IsFile && !uri.IsLoopback && uri.IsAbsoluteUri)
                {
                    if (uri.Scheme == System.Uri.UriSchemeHttp || uri.Scheme == System.Uri.UriSchemeHttps)
                    {
                        textEdit_website.ErrorText = string.Empty;
                        return;
                    }
                }
            }
            catch { }

            // The URI is not valid
            textEdit_website.ErrorText = "Invalid website address";
            e.Cancel = true;
        }

        #region " Windows Form Designer generated code "


        //Required by the Windows Form Designer
        private System.ComponentModel.IContainer components = null;

        private DevExpress.XtraEditors.TextEdit Vendor_name;

        //NOTE: The following procedure is required by the Windows Form Designer
        //It can be modified using the Windows Form Designer.
        //Do not modify it using the code editor.
        private DevExpress.XtraEditors.LabelControl LabelControl1;

        private DevExpress.XtraEditors.LabelControl LabelControl3;

        private DevExpress.XtraEditors.LabelControl LabelControl4;

        private DevExpress.XtraEditors.LabelControl LabelControl5;

        private DevExpress.XtraEditors.LabelControl LabelControl6;

        private DevExpress.XtraEditors.CalcEdit min_accept_amt;

        private DebtPlus.Data.Controls.PercentEdit min_accept_pct;

        private DevExpress.XtraEditors.ComboBoxEdit payment_balance;
        private DevExpress.XtraEditors.LabelControl labelControlWebSite;
        private DevExpress.XtraEditors.TextEdit textEdit_website;

        //Form overrides dispose to clean up the component list.
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                if (components != null)
                {
                    components.Dispose();
                }
            }
            base.Dispose(disposing);
        }

        [System.Diagnostics.DebuggerStepThrough]
        private void InitializeComponent()
        {
            this.payment_balance = new DevExpress.XtraEditors.ComboBoxEdit();
            this.LabelControl6 = new DevExpress.XtraEditors.LabelControl();
            this.LabelControl5 = new DevExpress.XtraEditors.LabelControl();
            this.LabelControl4 = new DevExpress.XtraEditors.LabelControl();
            this.LabelControl3 = new DevExpress.XtraEditors.LabelControl();
            this.min_accept_amt = new DevExpress.XtraEditors.CalcEdit();
            this.min_accept_pct = new DebtPlus.Data.Controls.PercentEdit();
            this.textEdit_website = new DevExpress.XtraEditors.TextEdit();
            this.labelControlWebSite = new DevExpress.XtraEditors.LabelControl();
            this.Vendor_name = new DevExpress.XtraEditors.TextEdit();
            this.LabelControl1 = new DevExpress.XtraEditors.LabelControl();
            this.panelControl1 = new DevExpress.XtraEditors.PanelControl();
            this.AddressInformation3 = new DebtPlus.UI.Vendor.Update.Controls.AddressInformation();
            this.AddressInformation2 = new DebtPlus.UI.Vendor.Update.Controls.AddressInformation();
            this.AddressInformation1 = new DebtPlus.UI.Vendor.Update.Controls.AddressInformation();
            ((System.ComponentModel.ISupportInitialize)(this.payment_balance.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.min_accept_amt.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.min_accept_pct.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit_website.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Vendor_name.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).BeginInit();
            this.panelControl1.SuspendLayout();
            this.SuspendLayout();
            // 
            // payment_balance
            // 
            this.payment_balance.Location = new System.Drawing.Point(56, 128);
            this.payment_balance.Name = "payment_balance";
            this.payment_balance.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.payment_balance.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
            this.payment_balance.Size = new System.Drawing.Size(104, 20);
            this.payment_balance.TabIndex = 6;
            this.payment_balance.ToolTip = "Choose either Payment or the Balance. Payment is normally used for Finance compan" +
    "ies while Balance is used for all other types.";
            // 
            // LabelControl6
            // 
            this.LabelControl6.Location = new System.Drawing.Point(16, 131);
            this.LabelControl6.Name = "LabelControl6";
            this.LabelControl6.Size = new System.Drawing.Size(29, 13);
            this.LabelControl6.TabIndex = 5;
            this.LabelControl6.Text = "of the";
            // 
            // LabelControl5
            // 
            this.LabelControl5.Location = new System.Drawing.Point(123, 99);
            this.LabelControl5.Name = "LabelControl5";
            this.LabelControl5.Size = new System.Drawing.Size(37, 13);
            this.LabelControl5.TabIndex = 4;
            this.LabelControl5.Text = "percent";
            // 
            // LabelControl4
            // 
            this.LabelControl4.Location = new System.Drawing.Point(24, 35);
            this.LabelControl4.Name = "LabelControl4";
            this.LabelControl4.Size = new System.Drawing.Size(6, 13);
            this.LabelControl4.TabIndex = 0;
            this.LabelControl4.Text = "$";
            // 
            // LabelControl3
            // 
            this.LabelControl3.Appearance.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LabelControl3.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.LabelControl3.Location = new System.Drawing.Point(8, 72);
            this.LabelControl3.Name = "LabelControl3";
            this.LabelControl3.Size = new System.Drawing.Size(112, 16);
            this.LabelControl3.TabIndex = 2;
            this.LabelControl3.Text = "Or the greater of";
            // 
            // min_accept_amt
            // 
            this.min_accept_amt.Location = new System.Drawing.Point(56, 32);
            this.min_accept_amt.Name = "min_accept_amt";
            this.min_accept_amt.Properties.Appearance.Options.UseTextOptions = true;
            this.min_accept_amt.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.min_accept_amt.Properties.AppearanceDisabled.Options.UseTextOptions = true;
            this.min_accept_amt.Properties.AppearanceDisabled.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.min_accept_amt.Properties.AppearanceDropDown.Options.UseTextOptions = true;
            this.min_accept_amt.Properties.AppearanceDropDown.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.min_accept_amt.Properties.AppearanceFocused.Options.UseTextOptions = true;
            this.min_accept_amt.Properties.AppearanceFocused.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.min_accept_amt.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.min_accept_amt.Properties.DisplayFormat.FormatString = "c2";
            this.min_accept_amt.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.min_accept_amt.Properties.EditFormat.FormatString = "f2";
            this.min_accept_amt.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.min_accept_amt.Properties.Mask.BeepOnError = true;
            this.min_accept_amt.Properties.Precision = 2;
            this.min_accept_amt.Size = new System.Drawing.Size(104, 20);
            this.min_accept_amt.TabIndex = 1;
            this.min_accept_amt.ToolTip = "Enter the minimum dollar amount that will be configured for debts of this credito" +
    "r. This is today monthly amount figure.";
            // 
            // min_accept_pct
            // 
            this.min_accept_pct.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.min_accept_pct.Location = new System.Drawing.Point(16, 96);
            this.min_accept_pct.Name = "min_accept_pct";
            this.min_accept_pct.Properties.Allow100Percent = false;
            this.min_accept_pct.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.min_accept_pct.Size = new System.Drawing.Size(91, 20);
            this.min_accept_pct.TabIndex = 3;
            this.min_accept_pct.ToolTip = "Choose the minimum percentage allowed for today monthly payment.";
            // 
            // textEdit_website
            // 
            this.textEdit_website.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.textEdit_website.Location = new System.Drawing.Point(96, 24);
            this.textEdit_website.Name = "textEdit_website";
            this.textEdit_website.Properties.MaxLength = 50;
            this.textEdit_website.Size = new System.Drawing.Size(535, 20);
            this.textEdit_website.TabIndex = 1;
            // 
            // labelControlWebSite
            // 
            this.labelControlWebSite.Location = new System.Drawing.Point(10, 26);
            this.labelControlWebSite.Name = "labelControlWebSite";
            this.labelControlWebSite.Size = new System.Drawing.Size(43, 13);
            this.labelControlWebSite.TabIndex = 0;
            this.labelControlWebSite.Text = "Website:";
            // 
            // Vendor_name
            // 
            this.Vendor_name.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.Vendor_name.Location = new System.Drawing.Point(96, 3);
            this.Vendor_name.Name = "Vendor_name";
            this.Vendor_name.Properties.MaxLength = 50;
            this.Vendor_name.Size = new System.Drawing.Size(535, 20);
            this.Vendor_name.TabIndex = 1;
            // 
            // LabelControl1
            // 
            this.LabelControl1.Location = new System.Drawing.Point(10, 6);
            this.LabelControl1.Name = "LabelControl1";
            this.LabelControl1.Size = new System.Drawing.Size(31, 13);
            this.LabelControl1.TabIndex = 0;
            this.LabelControl1.Text = "Name:";
            // 
            // panelControl1
            // 
            this.panelControl1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.panelControl1.Controls.Add(this.AddressInformation3);
            this.panelControl1.Controls.Add(this.AddressInformation2);
            this.panelControl1.Controls.Add(this.AddressInformation1);
            this.panelControl1.Location = new System.Drawing.Point(4, 50);
            this.panelControl1.Name = "panelControl1";
            this.panelControl1.Size = new System.Drawing.Size(648, 197);
            this.panelControl1.TabIndex = 7;
            // 
            // AddressInformation3
            // 
            this.AddressInformation3.AddressType = 'O';
            this.AddressInformation3.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.AddressInformation3.AutoScroll = true;
            this.AddressInformation3.Caption = "Other";
            this.AddressInformation3.Location = new System.Drawing.Point(6, 135);
            this.AddressInformation3.Name = "AddressInformation3";
            this.AddressInformation3.Size = new System.Drawing.Size(623, 62);
            this.AddressInformation3.TabIndex = 9;
            // 
            // AddressInformation2
            // 
            this.AddressInformation2.AddressType = 'I';
            this.AddressInformation2.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.AddressInformation2.AutoScroll = true;
            this.AddressInformation2.Caption = "Invoices";
            this.AddressInformation2.Location = new System.Drawing.Point(4, 69);
            this.AddressInformation2.Name = "AddressInformation2";
            this.AddressInformation2.Size = new System.Drawing.Size(623, 62);
            this.AddressInformation2.TabIndex = 8;
            // 
            // AddressInformation1
            // 
            this.AddressInformation1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.AddressInformation1.AutoScroll = true;
            this.AddressInformation1.Caption = "General";
            this.AddressInformation1.Location = new System.Drawing.Point(5, 1);
            this.AddressInformation1.Name = "AddressInformation1";
            this.AddressInformation1.Size = new System.Drawing.Size(623, 62);
            this.AddressInformation1.TabIndex = 7;
            // 
            // Page_Name
            // 
            this.Controls.Add(this.panelControl1);
            this.Controls.Add(this.textEdit_website);
            this.Controls.Add(this.labelControlWebSite);
            this.Controls.Add(this.Vendor_name);
            this.Controls.Add(this.LabelControl1);
            this.Name = "Page_Name";
            this.Size = new System.Drawing.Size(655, 250);
            ((System.ComponentModel.ISupportInitialize)(this.payment_balance.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.min_accept_amt.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.min_accept_pct.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit_website.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Vendor_name.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).EndInit();
            this.panelControl1.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion " Windows Form Designer generated code "
    }
}
