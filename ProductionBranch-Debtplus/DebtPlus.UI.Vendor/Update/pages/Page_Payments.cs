#region "Copyright 2000-2012 DebtPlus, L.L.C."

//{*******************************************************************}
//{                                                                   }
//{       DebtPlus Debt Management System                             }
//{                                                                   }
//{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
//{       ALL RIGHTS RESERVED                                         }
//{                                                                   }
//{   The entire contents of this file is protected by U.S. and       }
//{   International Copyright Laws. Unauthorized reproduction,        }
//{   reverse-engineering, and distribution of all or any portion of  }
//{   the code contained in this file is strictly prohibited and may  }
//{   result in severe civil and criminal penalties and will be       }
//{   prosecuted to the maximum extent possible under the law.        }
//{                                                                   }
//{   RESTRICTIONS                                                    }
//{                                                                   }
//{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
//{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
//{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
//{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
//{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
//{                                                                   }
//{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
//{   ADDITIONAL RESTRICTIONS.                                        }
//{                                                                   }
//{*******************************************************************}

#endregion "Copyright 2000-2012 DebtPlus, L.L.C."

using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using DebtPlus.LINQ;
using DebtPlus.UI.Common;
using DebtPlus.UI.Vendor.Update.Controls;
using DevExpress.XtraGrid.Columns;

namespace DebtPlus.UI.Vendor.Update.Pages
{
    internal class Page_Payments : MyGridControl
    {
        private BusinessContext bc;

        private List<RecordType> colRecords;

        private GridColumn gridColumn_client;
        private GridColumn gridColumn_cost;
        private GridColumn gridColumn_date_created;
        private GridColumn gridColumn_discount;
        private GridColumn gridColumn_disputed;
        private GridColumn gridColumn_Id;
        private GridColumn gridColumn_payment;
        private GridColumn gridColumn_product;
        private GridColumn gridColumn_transaction_type;

        // Current creditor being updated
        private vendor vendorRecord;

        /// <summary>
        ///     Initialize the new class
        /// </summary>
        public Page_Payments() : base()
        {
            InitializeComponent();
            //
            this.gridColumn_Id.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.True;
            this.gridColumn_client.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.True;
            this.gridColumn_cost.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.True;
            this.gridColumn_date_created.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.True;
            this.gridColumn_discount.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.True;
            this.gridColumn_disputed.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.True;
            this.gridColumn_payment.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.True;
            this.gridColumn_product.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.True;
            this.gridColumn_transaction_type.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.True;

            this.GridView1.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] { new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.gridColumn_date_created, DevExpress.Data.ColumnSortOrder.Descending) });
        }

        private DateTime? fromDate
        {
            get { return new DateTime?(); }
        }

        private DateTime? toDate
        {
            get { return new DateTime?(); }
        }

        /// <summary>
        ///     Read the list of invoice records and update the display information with them
        /// </summary>
        internal override void ReadForm(BusinessContext bc, vendor VendorRecord)
        {
            vendorRecord = VendorRecord;
            this.bc = bc;
            RefreshData();
        }

        private void RefreshData()
        {
            try
            {
                using (new CursorManager())
                {
                    var query =
                        bc.product_transactions.Join(bc.client_products, pt => pt.client_product, cp => cp.Id, (pt, cp) => new {pt, cp})
                          .Join(bc.products, @t => @t.cp.product, pr => pr.Id, (@t, pr) => new {@t, pr})
                          .Where(@t => @t.@t.pt.vendor == vendorRecord.Id)
                          .Select(@t => new RecordType
                            {
                                Id              = @t.@t.pt.Id,
                                Client          = @t.@t.cp.client,
                                TransactionType = @t.@t.pt.transaction_type,
                                Product         = @t.pr.description,
                                Vendor          = @t.@t.pt.vendor,
                                Cost            = @t.@t.pt.cost,
                                Discount        = @t.@t.pt.discount,
                                Payment         = @t.@t.pt.payment,
                                Disputed        = @t.@t.pt.disputed,
                                DateCreated     = @t.@t.pt.date_created
                            });

                    // Include the starting date if there is one
                    if (fromDate.HasValue)
                    {
                        var dt = fromDate.Value.Date;
                        query = query.Where(s => s.DateCreated >= dt);
                    }

                    // Include the ending date if there is one
                    if (toDate.HasValue)
                    {
                        var dt = toDate.Value.Date.AddDays(1);
                        query = query.Where(s => s.DateCreated < dt);
                    }

                    // Collect the records for the grid view
                    colRecords = query.ToList();
                    GridControl1.DataSource = colRecords;
                    GridView1.RefreshData();
                }
            }
            catch (SqlException ex)
            {
                ErrorHandling.HandleErrors(ex, "Error reading invoice information");
            }
        }

        private class RecordType
        {
            public int Id { get; set; }
            public int Client { get; set; }
            public string TransactionType { get; set; }
            public string Product { get; set; }
            public int? Vendor { get; set; }
            public decimal? Cost { get; set; }
            public decimal? Discount { get; set; }
            public decimal? Disputed { get; set; }
            public decimal? Payment { get; set; }
            public DateTime DateCreated { get; set; }
        }

        #region Windows Form Designer generated code

        //Required by the Windows Form Designer
        private System.ComponentModel.IContainer components = null;

        //Form overrides dispose to clean up the component list.
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                if (components != null)
                {
                    components.Dispose();
                }
            }
            base.Dispose(disposing);
        }

        //NOTE: The following procedure is required by the Windows Form Designer
        //It can be modified using the Windows Form Designer.
        //Do not modify it using the code editor.

        [System.Diagnostics.DebuggerStepThrough]
        private void InitializeComponent()
        {
            this.gridColumn_Id = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn_product = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn_client = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn_cost = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn_discount = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn_payment = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn_disputed = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn_date_created = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn_transaction_type = new DevExpress.XtraGrid.Columns.GridColumn();
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.PopupMenu_Items)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.GridControl1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.GridView1)).BeginInit();
            this.SuspendLayout();
            // 
            // GridView1
            // 
            this.GridView1.Appearance.ColumnFilterButton.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(104)))), ((int)(((byte)(184)))), ((int)(((byte)(251)))));
            this.GridView1.Appearance.ColumnFilterButton.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(104)))), ((int)(((byte)(184)))), ((int)(((byte)(251)))));
            this.GridView1.Appearance.ColumnFilterButton.ForeColor = System.Drawing.Color.White;
            this.GridView1.Appearance.ColumnFilterButton.Options.UseBackColor = true;
            this.GridView1.Appearance.ColumnFilterButton.Options.UseBorderColor = true;
            this.GridView1.Appearance.ColumnFilterButton.Options.UseForeColor = true;
            this.GridView1.Appearance.ColumnFilterButtonActive.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(170)))), ((int)(((byte)(216)))), ((int)(((byte)(254)))));
            this.GridView1.Appearance.ColumnFilterButtonActive.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(170)))), ((int)(((byte)(216)))), ((int)(((byte)(254)))));
            this.GridView1.Appearance.ColumnFilterButtonActive.ForeColor = System.Drawing.Color.Black;
            this.GridView1.Appearance.ColumnFilterButtonActive.Options.UseBackColor = true;
            this.GridView1.Appearance.ColumnFilterButtonActive.Options.UseBorderColor = true;
            this.GridView1.Appearance.ColumnFilterButtonActive.Options.UseForeColor = true;
            this.GridView1.Appearance.Empty.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(236)))), ((int)(((byte)(246)))), ((int)(((byte)(255)))));
            this.GridView1.Appearance.Empty.BackColor2 = System.Drawing.Color.White;
            this.GridView1.Appearance.Empty.Options.UseBackColor = true;
            this.GridView1.Appearance.EvenRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(247)))), ((int)(((byte)(251)))), ((int)(((byte)(255)))));
            this.GridView1.Appearance.EvenRow.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(247)))), ((int)(((byte)(251)))), ((int)(((byte)(255)))));
            this.GridView1.Appearance.EvenRow.ForeColor = System.Drawing.Color.Black;
            this.GridView1.Appearance.EvenRow.Options.UseBackColor = true;
            this.GridView1.Appearance.EvenRow.Options.UseBorderColor = true;
            this.GridView1.Appearance.EvenRow.Options.UseForeColor = true;
            this.GridView1.Appearance.FilterCloseButton.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(104)))), ((int)(((byte)(184)))), ((int)(((byte)(251)))));
            this.GridView1.Appearance.FilterCloseButton.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(104)))), ((int)(((byte)(184)))), ((int)(((byte)(251)))));
            this.GridView1.Appearance.FilterCloseButton.ForeColor = System.Drawing.Color.White;
            this.GridView1.Appearance.FilterCloseButton.Options.UseBackColor = true;
            this.GridView1.Appearance.FilterCloseButton.Options.UseBorderColor = true;
            this.GridView1.Appearance.FilterCloseButton.Options.UseForeColor = true;
            this.GridView1.Appearance.FilterPanel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(236)))), ((int)(((byte)(246)))), ((int)(((byte)(255)))));
            this.GridView1.Appearance.FilterPanel.BackColor2 = System.Drawing.Color.White;
            this.GridView1.Appearance.FilterPanel.ForeColor = System.Drawing.Color.Black;
            this.GridView1.Appearance.FilterPanel.Options.UseBackColor = true;
            this.GridView1.Appearance.FilterPanel.Options.UseForeColor = true;
            this.GridView1.Appearance.FixedLine.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(59)))), ((int)(((byte)(133)))), ((int)(((byte)(195)))));
            this.GridView1.Appearance.FixedLine.Options.UseBackColor = true;
            this.GridView1.Appearance.FocusedCell.BackColor = System.Drawing.Color.White;
            this.GridView1.Appearance.FocusedCell.ForeColor = System.Drawing.Color.Black;
            this.GridView1.Appearance.FocusedCell.Options.UseBackColor = true;
            this.GridView1.Appearance.FocusedCell.Options.UseForeColor = true;
            this.GridView1.Appearance.FocusedRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(38)))), ((int)(((byte)(109)))), ((int)(((byte)(189)))));
            this.GridView1.Appearance.FocusedRow.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(59)))), ((int)(((byte)(139)))), ((int)(((byte)(206)))));
            this.GridView1.Appearance.FocusedRow.ForeColor = System.Drawing.Color.White;
            this.GridView1.Appearance.FocusedRow.Options.UseBackColor = true;
            this.GridView1.Appearance.FocusedRow.Options.UseBorderColor = true;
            this.GridView1.Appearance.FocusedRow.Options.UseForeColor = true;
            this.GridView1.Appearance.FooterPanel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(104)))), ((int)(((byte)(184)))), ((int)(((byte)(251)))));
            this.GridView1.Appearance.FooterPanel.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(104)))), ((int)(((byte)(184)))), ((int)(((byte)(251)))));
            this.GridView1.Appearance.FooterPanel.ForeColor = System.Drawing.Color.Black;
            this.GridView1.Appearance.FooterPanel.Options.UseBackColor = true;
            this.GridView1.Appearance.FooterPanel.Options.UseBorderColor = true;
            this.GridView1.Appearance.FooterPanel.Options.UseForeColor = true;
            this.GridView1.Appearance.GroupButton.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(104)))), ((int)(((byte)(184)))), ((int)(((byte)(251)))));
            this.GridView1.Appearance.GroupButton.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(104)))), ((int)(((byte)(184)))), ((int)(((byte)(251)))));
            this.GridView1.Appearance.GroupButton.Options.UseBackColor = true;
            this.GridView1.Appearance.GroupButton.Options.UseBorderColor = true;
            this.GridView1.Appearance.GroupFooter.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(170)))), ((int)(((byte)(216)))), ((int)(((byte)(254)))));
            this.GridView1.Appearance.GroupFooter.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(170)))), ((int)(((byte)(216)))), ((int)(((byte)(254)))));
            this.GridView1.Appearance.GroupFooter.ForeColor = System.Drawing.Color.Black;
            this.GridView1.Appearance.GroupFooter.Options.UseBackColor = true;
            this.GridView1.Appearance.GroupFooter.Options.UseBorderColor = true;
            this.GridView1.Appearance.GroupFooter.Options.UseForeColor = true;
            this.GridView1.Appearance.GroupPanel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(236)))), ((int)(((byte)(246)))), ((int)(((byte)(255)))));
            this.GridView1.Appearance.GroupPanel.BackColor2 = System.Drawing.Color.White;
            this.GridView1.Appearance.GroupPanel.ForeColor = System.Drawing.Color.Black;
            this.GridView1.Appearance.GroupPanel.Options.UseBackColor = true;
            this.GridView1.Appearance.GroupPanel.Options.UseForeColor = true;
            this.GridView1.Appearance.GroupRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(170)))), ((int)(((byte)(216)))), ((int)(((byte)(254)))));
            this.GridView1.Appearance.GroupRow.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(170)))), ((int)(((byte)(216)))), ((int)(((byte)(254)))));
            this.GridView1.Appearance.GroupRow.ForeColor = System.Drawing.Color.Black;
            this.GridView1.Appearance.GroupRow.Options.UseBackColor = true;
            this.GridView1.Appearance.GroupRow.Options.UseBorderColor = true;
            this.GridView1.Appearance.GroupRow.Options.UseForeColor = true;
            this.GridView1.Appearance.HeaderPanel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(139)))), ((int)(((byte)(201)))), ((int)(((byte)(254)))));
            this.GridView1.Appearance.HeaderPanel.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(139)))), ((int)(((byte)(201)))), ((int)(((byte)(254)))));
            this.GridView1.Appearance.HeaderPanel.ForeColor = System.Drawing.Color.Black;
            this.GridView1.Appearance.HeaderPanel.Options.UseBackColor = true;
            this.GridView1.Appearance.HeaderPanel.Options.UseBorderColor = true;
            this.GridView1.Appearance.HeaderPanel.Options.UseForeColor = true;
            this.GridView1.Appearance.HideSelectionRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(105)))), ((int)(((byte)(170)))), ((int)(((byte)(225)))));
            this.GridView1.Appearance.HideSelectionRow.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(83)))), ((int)(((byte)(155)))), ((int)(((byte)(215)))));
            this.GridView1.Appearance.HideSelectionRow.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(236)))), ((int)(((byte)(246)))), ((int)(((byte)(255)))));
            this.GridView1.Appearance.HideSelectionRow.Options.UseBackColor = true;
            this.GridView1.Appearance.HideSelectionRow.Options.UseBorderColor = true;
            this.GridView1.Appearance.HideSelectionRow.Options.UseForeColor = true;
            this.GridView1.Appearance.HorzLine.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(104)))), ((int)(((byte)(184)))), ((int)(((byte)(251)))));
            this.GridView1.Appearance.HorzLine.Options.UseBackColor = true;
            this.GridView1.Appearance.OddRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(236)))), ((int)(((byte)(246)))), ((int)(((byte)(255)))));
            this.GridView1.Appearance.OddRow.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(236)))), ((int)(((byte)(246)))), ((int)(((byte)(255)))));
            this.GridView1.Appearance.OddRow.ForeColor = System.Drawing.Color.Black;
            this.GridView1.Appearance.OddRow.Options.UseBackColor = true;
            this.GridView1.Appearance.OddRow.Options.UseBorderColor = true;
            this.GridView1.Appearance.OddRow.Options.UseForeColor = true;
            this.GridView1.Appearance.Preview.Font = new System.Drawing.Font("Verdana", 7.5F);
            this.GridView1.Appearance.Preview.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(83)))), ((int)(((byte)(155)))), ((int)(((byte)(215)))));
            this.GridView1.Appearance.Preview.Options.UseFont = true;
            this.GridView1.Appearance.Preview.Options.UseForeColor = true;
            this.GridView1.Appearance.Row.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(247)))), ((int)(((byte)(251)))), ((int)(((byte)(255)))));
            this.GridView1.Appearance.Row.ForeColor = System.Drawing.Color.Black;
            this.GridView1.Appearance.Row.Options.UseBackColor = true;
            this.GridView1.Appearance.Row.Options.UseForeColor = true;
            this.GridView1.Appearance.RowSeparator.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(236)))), ((int)(((byte)(246)))), ((int)(((byte)(255)))));
            this.GridView1.Appearance.RowSeparator.BackColor2 = System.Drawing.Color.White;
            this.GridView1.Appearance.RowSeparator.Options.UseBackColor = true;
            this.GridView1.Appearance.SelectedRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(83)))), ((int)(((byte)(155)))), ((int)(((byte)(215)))));
            this.GridView1.Appearance.SelectedRow.ForeColor = System.Drawing.Color.White;
            this.GridView1.Appearance.SelectedRow.Options.UseBackColor = true;
            this.GridView1.Appearance.SelectedRow.Options.UseForeColor = true;
            this.GridView1.Appearance.TopNewRow.BackColor = System.Drawing.Color.White;
            this.GridView1.Appearance.TopNewRow.Options.UseBackColor = true;
            this.GridView1.Appearance.VertLine.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(104)))), ((int)(((byte)(184)))), ((int)(((byte)(251)))));
            this.GridView1.Appearance.VertLine.Options.UseBackColor = true;
            this.GridView1.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gridColumn_Id,
            this.gridColumn_client,
            this.gridColumn_cost,
            this.gridColumn_date_created,
            this.gridColumn_discount,
            this.gridColumn_disputed,
            this.gridColumn_payment,
            this.gridColumn_product,
            this.gridColumn_transaction_type});
            this.GridView1.OptionsBehavior.AutoPopulateColumns = false;
            this.GridView1.OptionsBehavior.Editable = false;
            this.GridView1.OptionsView.EnableAppearanceEvenRow = true;
            this.GridView1.OptionsView.EnableAppearanceOddRow = true;
            this.GridView1.OptionsView.ShowGroupPanel = false;
            this.GridView1.OptionsView.ShowIndicator = false;
            // 
            // gridColumn_Id
            // 
            this.gridColumn_Id.AppearanceCell.Options.UseTextOptions = true;
            this.gridColumn_Id.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.gridColumn_Id.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColumn_Id.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.gridColumn_Id.Caption = "ID";
            this.gridColumn_Id.CustomizationCaption = "Record ID";
            this.gridColumn_Id.DisplayFormat.FormatString = "f0";
            this.gridColumn_Id.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.gridColumn_Id.FieldName = "Id";
            this.gridColumn_Id.GroupFormat.FormatString = "f0";
            this.gridColumn_Id.GroupFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.gridColumn_Id.Name = "gridColumn_Id";
            this.gridColumn_Id.OptionsColumn.AllowEdit = false;
            this.gridColumn_Id.SortMode = DevExpress.XtraGrid.ColumnSortMode.Value;
            this.gridColumn_Id.Width = 48;
            // 
            // gridColumn_product
            // 
            this.gridColumn_product.Caption = "Product";
            this.gridColumn_product.FieldName = "Product";
            this.gridColumn_product.FilterMode = DevExpress.XtraGrid.ColumnFilterMode.DisplayText;
            this.gridColumn_product.Name = "gridColumn_product";
            this.gridColumn_product.OptionsColumn.AllowEdit = false;
            this.gridColumn_product.SortMode = DevExpress.XtraGrid.ColumnSortMode.DisplayText;
            this.gridColumn_product.Visible = true;
            this.gridColumn_product.VisibleIndex = 3;
            this.gridColumn_product.Width = 194;
            // 
            // gridColumn_client
            // 
            this.gridColumn_client.AppearanceCell.Options.UseTextOptions = true;
            this.gridColumn_client.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.gridColumn_client.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColumn_client.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.gridColumn_client.Caption = "Client";
            this.gridColumn_client.DisplayFormat.FormatString = "0000000";
            this.gridColumn_client.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.gridColumn_client.FieldName = "Client";
            this.gridColumn_client.GroupFormat.FormatString = "0000000";
            this.gridColumn_client.GroupFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.gridColumn_client.Name = "gridColumn_client";
            this.gridColumn_client.OptionsColumn.AllowEdit = false;
            this.gridColumn_client.SortMode = DevExpress.XtraGrid.ColumnSortMode.Value;
            this.gridColumn_client.Visible = true;
            this.gridColumn_client.VisibleIndex = 2;
            this.gridColumn_client.Width = 74;
            // 
            // gridColumn_cost
            // 
            this.gridColumn_cost.AppearanceCell.Options.UseTextOptions = true;
            this.gridColumn_cost.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.gridColumn_cost.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColumn_cost.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.gridColumn_cost.Caption = "Cost";
            this.gridColumn_cost.DisplayFormat.FormatString = "c";
            this.gridColumn_cost.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.gridColumn_cost.FieldName = "Cost";
            this.gridColumn_cost.GroupFormat.FormatString = "c";
            this.gridColumn_cost.GroupFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.gridColumn_cost.Name = "gridColumn_cost";
            this.gridColumn_cost.OptionsColumn.AllowEdit = false;
            this.gridColumn_cost.SortMode = DevExpress.XtraGrid.ColumnSortMode.Value;
            this.gridColumn_cost.Visible = true;
            this.gridColumn_cost.VisibleIndex = 4;
            this.gridColumn_cost.Width = 59;
            // 
            // gridColumn_discount
            // 
            this.gridColumn_discount.AppearanceCell.Options.UseTextOptions = true;
            this.gridColumn_discount.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.gridColumn_discount.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColumn_discount.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.gridColumn_discount.Caption = "Discount";
            this.gridColumn_discount.DisplayFormat.FormatString = "c";
            this.gridColumn_discount.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.gridColumn_discount.FieldName = "Discount";
            this.gridColumn_discount.GroupFormat.FormatString = "c";
            this.gridColumn_discount.GroupFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.gridColumn_discount.Name = "gridColumn_discount";
            this.gridColumn_discount.OptionsColumn.AllowEdit = false;
            this.gridColumn_discount.SortMode = DevExpress.XtraGrid.ColumnSortMode.Value;
            // 
            // gridColumn_payment
            // 
            this.gridColumn_payment.AppearanceCell.Options.UseTextOptions = true;
            this.gridColumn_payment.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.gridColumn_payment.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColumn_payment.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.gridColumn_payment.Caption = "Payment";
            this.gridColumn_payment.DisplayFormat.FormatString = "c";
            this.gridColumn_payment.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.gridColumn_payment.FieldName = "Payment";
            this.gridColumn_payment.GroupFormat.FormatString = "c";
            this.gridColumn_payment.GroupFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.gridColumn_payment.Name = "gridColumn_payment";
            this.gridColumn_payment.OptionsColumn.AllowEdit = false;
            this.gridColumn_payment.SortMode = DevExpress.XtraGrid.ColumnSortMode.Value;
            this.gridColumn_payment.Visible = true;
            this.gridColumn_payment.VisibleIndex = 5;
            this.gridColumn_payment.Width = 58;
            // 
            // gridColumn_disputed
            // 
            this.gridColumn_disputed.AppearanceCell.Options.UseTextOptions = true;
            this.gridColumn_disputed.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.gridColumn_disputed.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColumn_disputed.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.gridColumn_disputed.Caption = "Disputed";
            this.gridColumn_disputed.DisplayFormat.FormatString = "c";
            this.gridColumn_disputed.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.gridColumn_disputed.FieldName = "Disputed";
            this.gridColumn_disputed.GroupFormat.FormatString = "c";
            this.gridColumn_disputed.GroupFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.gridColumn_disputed.Name = "gridColumn_disputed";
            this.gridColumn_disputed.OptionsColumn.AllowEdit = false;
            this.gridColumn_disputed.SortMode = DevExpress.XtraGrid.ColumnSortMode.Value;
            this.gridColumn_disputed.Visible = true;
            this.gridColumn_disputed.VisibleIndex = 6;
            this.gridColumn_disputed.Width = 60;
            // 
            // gridColumn_date_created
            // 
            this.gridColumn_date_created.AppearanceCell.Options.UseTextOptions = true;
            this.gridColumn_date_created.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.gridColumn_date_created.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColumn_date_created.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.gridColumn_date_created.Caption = "Date";
            this.gridColumn_date_created.DisplayFormat.FormatString = "d";
            this.gridColumn_date_created.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.gridColumn_date_created.FieldName = "DateCreated";
            this.gridColumn_date_created.GroupFormat.FormatString = "d";
            this.gridColumn_date_created.GroupFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.gridColumn_date_created.Name = "gridColumn_date_created";
            this.gridColumn_date_created.OptionsColumn.AllowEdit = false;
            this.gridColumn_date_created.SortMode = DevExpress.XtraGrid.ColumnSortMode.Value;
            this.gridColumn_date_created.Visible = true;
            this.gridColumn_date_created.VisibleIndex = 0;
            this.gridColumn_date_created.Width = 72;
            // 
            // gridColumn_transaction_type
            // 
            this.gridColumn_transaction_type.Caption = "Type";
            this.gridColumn_transaction_type.CustomizationCaption = "Type of transaction";
            this.gridColumn_transaction_type.FieldName = "TransactionType";
            this.gridColumn_transaction_type.Name = "gridColumn_transaction_type";
            this.gridColumn_transaction_type.OptionsColumn.AllowEdit = false;
            this.gridColumn_transaction_type.Visible = true;
            this.gridColumn_transaction_type.VisibleIndex = 1;
            this.gridColumn_transaction_type.Width = 52;
            // 
            // Page_Payments
            // 
            this.Name = "Page_Payments";
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.PopupMenu_Items)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.GridControl1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.GridView1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion Windows Form Designer generated code
    }
}