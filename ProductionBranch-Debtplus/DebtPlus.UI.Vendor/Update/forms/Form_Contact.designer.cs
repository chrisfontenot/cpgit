namespace DebtPlus.UI.Vendor.Update.Forms
{
    partial class Form_Contact
    {
        //Form overrides dispose to clean up the component list.
        [System.Diagnostics.DebuggerNonUserCode]
        protected override void Dispose(bool disposing)
        {
            try
            {
                if (disposing)
                {
                    if (components != null)
                    {
                        components.Dispose();
                    }
                }
            }
            finally
            {
                base.Dispose(disposing);
            }
        }

        //Required by the Windows Form Designer
        private System.ComponentModel.IContainer components = null;

        //NOTE: The following procedure is required by the Windows Form Designer
        //It can be modified using the Windows Form Designer.  
        //Do not modify it using the code editor.
        [System.Diagnostics.DebuggerStepThrough]
        private void InitializeComponent()
        {
            this.button_OK = new DevExpress.XtraEditors.SimpleButton();
            this.LayoutControl1 = new DevExpress.XtraLayout.LayoutControl();
            this.lookUpEdit_ContactType = new DevExpress.XtraEditors.LookUpEdit();
            this.emailRecordControl1 = new DebtPlus.Data.Controls.EmailRecordControl();
            this.Button_Cancel = new DevExpress.XtraEditors.SimpleButton();
            this.memoEdit_notes = new DevExpress.XtraEditors.MemoEdit();
            this.telephoneNumberRecordControl_FAX = new DebtPlus.Data.Controls.TelephoneNumberRecordControl();
            this.telephoneNumberRecordControl_Telephone = new DebtPlus.Data.Controls.TelephoneNumberRecordControl();
            this.addressRecordControl1 = new DebtPlus.Data.Controls.AddressRecordControl();
            this.nameRecordControl1 = new DebtPlus.Data.Controls.NameRecordControl();
            this.textEdit_title = new DevExpress.XtraEditors.TextEdit();
            this.LayoutControlGroup1 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.LayoutControlItem2 = new DevExpress.XtraLayout.LayoutControlItem();
            this.LayoutControlItem3 = new DevExpress.XtraLayout.LayoutControlItem();
            this.LayoutControlItem4 = new DevExpress.XtraLayout.LayoutControlItem();
            this.LayoutControlItem10 = new DevExpress.XtraLayout.LayoutControlItem();
            this.LayoutControlItem7 = new DevExpress.XtraLayout.LayoutControlItem();
            this.LayoutControlItem5 = new DevExpress.XtraLayout.LayoutControlItem();
            this.LayoutControlItem6 = new DevExpress.XtraLayout.LayoutControlItem();
            this.LayoutControlItem9 = new DevExpress.XtraLayout.LayoutControlItem();
            this.EmptySpaceItem1 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.EmptySpaceItem2 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.LayoutControlItem8 = new DevExpress.XtraLayout.LayoutControlItem();
            this.LayoutControlItem1 = new DevExpress.XtraLayout.LayoutControlItem();
            ((System.ComponentModel.ISupportInitialize)(this.DxErrorProvider1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LayoutControl1)).BeginInit();
            this.LayoutControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.lookUpEdit_ContactType.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emailRecordControl1.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.memoEdit_notes.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.telephoneNumberRecordControl_FAX)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.telephoneNumberRecordControl_Telephone)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.addressRecordControl1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit_title.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LayoutControlGroup1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LayoutControlItem2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LayoutControlItem3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LayoutControlItem4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LayoutControlItem10)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LayoutControlItem7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LayoutControlItem5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LayoutControlItem6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LayoutControlItem9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.EmptySpaceItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.EmptySpaceItem2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LayoutControlItem8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LayoutControlItem1)).BeginInit();
            this.SuspendLayout();
            // 
            // DefaultLookAndFeel1
            // 
            this.DefaultLookAndFeel1.LookAndFeel.SkinName = "McSkin";
            // 
            // button_OK
            // 
            this.button_OK.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.button_OK.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.button_OK.Location = new System.Drawing.Point(205, 291);
            this.button_OK.MaximumSize = new System.Drawing.Size(75, 23);
            this.button_OK.MinimumSize = new System.Drawing.Size(75, 23);
            this.button_OK.Name = "button_OK";
            this.button_OK.Size = new System.Drawing.Size(75, 23);
            this.button_OK.StyleController = this.LayoutControl1;
            this.button_OK.TabIndex = 18;
            this.button_OK.Text = "&OK";
            this.button_OK.ToolTipController = this.ToolTipController1;
            // 
            // LayoutControl1
            // 
            this.LayoutControl1.Controls.Add(this.lookUpEdit_ContactType);
            this.LayoutControl1.Controls.Add(this.emailRecordControl1);
            this.LayoutControl1.Controls.Add(this.Button_Cancel);
            this.LayoutControl1.Controls.Add(this.memoEdit_notes);
            this.LayoutControl1.Controls.Add(this.button_OK);
            this.LayoutControl1.Controls.Add(this.telephoneNumberRecordControl_FAX);
            this.LayoutControl1.Controls.Add(this.telephoneNumberRecordControl_Telephone);
            this.LayoutControl1.Controls.Add(this.addressRecordControl1);
            this.LayoutControl1.Controls.Add(this.nameRecordControl1);
            this.LayoutControl1.Controls.Add(this.textEdit_title);
            this.LayoutControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.LayoutControl1.Location = new System.Drawing.Point(0, 0);
            this.LayoutControl1.Name = "LayoutControl1";
            this.LayoutControl1.Root = this.LayoutControlGroup1;
            this.LayoutControl1.Size = new System.Drawing.Size(560, 326);
            this.LayoutControl1.TabIndex = 22;
            this.LayoutControl1.Text = "LayoutControl1";
            // 
            // lookUpEdit_ContactType
            // 
            this.lookUpEdit_ContactType.Location = new System.Drawing.Point(65, 12);
            this.lookUpEdit_ContactType.Name = "lookUpEdit_ContactType";
            this.lookUpEdit_ContactType.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.False;
            this.lookUpEdit_ContactType.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.lookUpEdit_ContactType.Properties.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("Id", "ID", 20, DevExpress.Utils.FormatType.Numeric, "f0", false, DevExpress.Utils.HorzAlignment.Default),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("description", "Description", 20, DevExpress.Utils.FormatType.None, "", true, DevExpress.Utils.HorzAlignment.Default, DevExpress.Data.ColumnSortOrder.Ascending)});
            this.lookUpEdit_ContactType.Properties.DisplayMember = "description";
            this.lookUpEdit_ContactType.Properties.NullText = "";
            this.lookUpEdit_ContactType.Properties.ShowFooter = false;
            this.lookUpEdit_ContactType.Properties.ShowHeader = false;
            this.lookUpEdit_ContactType.Properties.SortColumnIndex = 1;
            this.lookUpEdit_ContactType.Properties.ValueMember = "Id";
            this.lookUpEdit_ContactType.Size = new System.Drawing.Size(483, 20);
            this.lookUpEdit_ContactType.StyleController = this.LayoutControl1;
            this.lookUpEdit_ContactType.TabIndex = 25;
            // 
            // emailRecordControl1
            // 
            this.emailRecordControl1.ClickedColor = System.Drawing.Color.Maroon;
            this.emailRecordControl1.Location = new System.Drawing.Point(65, 184);
            this.emailRecordControl1.Name = "emailRecordControl1";
            this.emailRecordControl1.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Underline);
            this.emailRecordControl1.Properties.Appearance.ForeColor = System.Drawing.Color.Blue;
            this.emailRecordControl1.Properties.Appearance.Options.UseFont = true;
            this.emailRecordControl1.Properties.Appearance.Options.UseForeColor = true;
            this.emailRecordControl1.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.emailRecordControl1.Size = new System.Drawing.Size(483, 20);
            this.emailRecordControl1.StyleController = this.LayoutControl1;
            this.emailRecordControl1.TabIndex = 24;
            // 
            // Button_Cancel
            // 
            this.Button_Cancel.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.Button_Cancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.Button_Cancel.Location = new System.Drawing.Point(284, 291);
            this.Button_Cancel.MaximumSize = new System.Drawing.Size(75, 23);
            this.Button_Cancel.MinimumSize = new System.Drawing.Size(75, 23);
            this.Button_Cancel.Name = "Button_Cancel";
            this.Button_Cancel.Size = new System.Drawing.Size(75, 23);
            this.Button_Cancel.StyleController = this.LayoutControl1;
            this.Button_Cancel.TabIndex = 19;
            this.Button_Cancel.Text = "&Cancel";
            this.Button_Cancel.ToolTipController = this.ToolTipController1;
            // 
            // memoEdit_notes
            // 
            this.memoEdit_notes.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.memoEdit_notes.Location = new System.Drawing.Point(65, 208);
            this.memoEdit_notes.Name = "memoEdit_notes";
            this.memoEdit_notes.Properties.MaxLength = 256;
            this.memoEdit_notes.Size = new System.Drawing.Size(483, 79);
            this.memoEdit_notes.StyleController = this.LayoutControl1;
            this.memoEdit_notes.TabIndex = 17;
            this.memoEdit_notes.ToolTipController = this.ToolTipController1;
            // 
            // telephoneNumberRecordControl_FAX
            // 
            this.telephoneNumberRecordControl_FAX.ErrorIcon = null;
            this.telephoneNumberRecordControl_FAX.ErrorText = "";
            this.telephoneNumberRecordControl_FAX.Location = new System.Drawing.Point(334, 160);
            this.telephoneNumberRecordControl_FAX.Margin = new System.Windows.Forms.Padding(0);
            this.telephoneNumberRecordControl_FAX.Name = "telephoneNumberRecordControl_FAX";
            this.telephoneNumberRecordControl_FAX.Size = new System.Drawing.Size(214, 20);
            this.telephoneNumberRecordControl_FAX.TabIndex = 23;
            // 
            // telephoneNumberRecordControl_Telephone
            // 
            this.telephoneNumberRecordControl_Telephone.ErrorIcon = null;
            this.telephoneNumberRecordControl_Telephone.ErrorText = "";
            this.telephoneNumberRecordControl_Telephone.Location = new System.Drawing.Point(65, 160);
            this.telephoneNumberRecordControl_Telephone.Margin = new System.Windows.Forms.Padding(0);
            this.telephoneNumberRecordControl_Telephone.Name = "telephoneNumberRecordControl_Telephone";
            this.telephoneNumberRecordControl_Telephone.Size = new System.Drawing.Size(212, 20);
            this.telephoneNumberRecordControl_Telephone.TabIndex = 22;
            // 
            // addressRecordControl1
            // 
            this.addressRecordControl1.Location = new System.Drawing.Point(65, 84);
            this.addressRecordControl1.Name = "addressRecordControl1";
            this.addressRecordControl1.Size = new System.Drawing.Size(483, 72);
            this.addressRecordControl1.TabIndex = 21;
            // 
            // nameRecordControl1
            // 
            this.nameRecordControl1.Location = new System.Drawing.Point(65, 36);
            this.nameRecordControl1.Name = "nameRecordControl1";
            this.nameRecordControl1.Size = new System.Drawing.Size(483, 20);
            this.nameRecordControl1.TabIndex = 20;
            // 
            // textEdit_title
            // 
            this.textEdit_title.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.textEdit_title.Location = new System.Drawing.Point(65, 60);
            this.textEdit_title.Name = "textEdit_title";
            this.textEdit_title.Properties.MaxLength = 255;
            this.textEdit_title.Size = new System.Drawing.Size(483, 20);
            this.textEdit_title.StyleController = this.LayoutControl1;
            this.textEdit_title.TabIndex = 5;
            this.textEdit_title.ToolTipController = this.ToolTipController1;
            // 
            // LayoutControlGroup1
            // 
            this.LayoutControlGroup1.CustomizationFormText = "LayoutControlGroup1";
            this.LayoutControlGroup1.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.LayoutControlItem2,
            this.LayoutControlItem3,
            this.LayoutControlItem4,
            this.LayoutControlItem10,
            this.LayoutControlItem7,
            this.LayoutControlItem5,
            this.LayoutControlItem6,
            this.LayoutControlItem9,
            this.EmptySpaceItem1,
            this.EmptySpaceItem2,
            this.LayoutControlItem8,
            this.LayoutControlItem1});
            this.LayoutControlGroup1.Location = new System.Drawing.Point(0, 0);
            this.LayoutControlGroup1.Name = "LayoutControlGroup1";
            this.LayoutControlGroup1.Size = new System.Drawing.Size(560, 326);
            this.LayoutControlGroup1.Text = "LayoutControlGroup1";
            this.LayoutControlGroup1.TextVisible = false;
            // 
            // LayoutControlItem2
            // 
            this.LayoutControlItem2.Control = this.nameRecordControl1;
            this.LayoutControlItem2.CustomizationFormText = "Name";
            this.LayoutControlItem2.Location = new System.Drawing.Point(0, 24);
            this.LayoutControlItem2.Name = "LayoutControlItem2";
            this.LayoutControlItem2.Size = new System.Drawing.Size(540, 24);
            this.LayoutControlItem2.Text = "Name";
            this.LayoutControlItem2.TextSize = new System.Drawing.Size(50, 13);
            // 
            // LayoutControlItem3
            // 
            this.LayoutControlItem3.Control = this.textEdit_title;
            this.LayoutControlItem3.CustomizationFormText = "Title";
            this.LayoutControlItem3.Location = new System.Drawing.Point(0, 48);
            this.LayoutControlItem3.Name = "LayoutControlItem3";
            this.LayoutControlItem3.Size = new System.Drawing.Size(540, 24);
            this.LayoutControlItem3.Text = "Title";
            this.LayoutControlItem3.TextSize = new System.Drawing.Size(50, 13);
            // 
            // LayoutControlItem4
            // 
            this.LayoutControlItem4.AppearanceItemCaption.Options.UseTextOptions = true;
            this.LayoutControlItem4.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.LayoutControlItem4.AppearanceItemCaption.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Top;
            this.LayoutControlItem4.Control = this.addressRecordControl1;
            this.LayoutControlItem4.CustomizationFormText = "Address";
            this.LayoutControlItem4.Location = new System.Drawing.Point(0, 72);
            this.LayoutControlItem4.Name = "LayoutControlItem4";
            this.LayoutControlItem4.Size = new System.Drawing.Size(540, 76);
            this.LayoutControlItem4.Text = "Address";
            this.LayoutControlItem4.TextSize = new System.Drawing.Size(50, 13);
            // 
            // LayoutControlItem10
            // 
            this.LayoutControlItem10.Control = this.Button_Cancel;
            this.LayoutControlItem10.CustomizationFormText = "LayoutControlItem10";
            this.LayoutControlItem10.Location = new System.Drawing.Point(272, 279);
            this.LayoutControlItem10.Name = "LayoutControlItem10";
            this.LayoutControlItem10.Size = new System.Drawing.Size(79, 27);
            this.LayoutControlItem10.Text = "LayoutControlItem10";
            this.LayoutControlItem10.TextSize = new System.Drawing.Size(0, 0);
            this.LayoutControlItem10.TextToControlDistance = 0;
            this.LayoutControlItem10.TextVisible = false;
            // 
            // LayoutControlItem7
            // 
            this.LayoutControlItem7.Control = this.emailRecordControl1;
            this.LayoutControlItem7.CustomizationFormText = "Email";
            this.LayoutControlItem7.Location = new System.Drawing.Point(0, 172);
            this.LayoutControlItem7.Name = "LayoutControlItem7";
            this.LayoutControlItem7.Size = new System.Drawing.Size(540, 24);
            this.LayoutControlItem7.Text = "Email";
            this.LayoutControlItem7.TextSize = new System.Drawing.Size(50, 13);
            // 
            // LayoutControlItem5
            // 
            this.LayoutControlItem5.Control = this.telephoneNumberRecordControl_Telephone;
            this.LayoutControlItem5.CustomizationFormText = "Telephone";
            this.LayoutControlItem5.Location = new System.Drawing.Point(0, 148);
            this.LayoutControlItem5.Name = "LayoutControlItem5";
            this.LayoutControlItem5.Size = new System.Drawing.Size(269, 24);
            this.LayoutControlItem5.Text = "Telephone";
            this.LayoutControlItem5.TextSize = new System.Drawing.Size(50, 13);
            // 
            // LayoutControlItem6
            // 
            this.LayoutControlItem6.Control = this.telephoneNumberRecordControl_FAX;
            this.LayoutControlItem6.CustomizationFormText = "FAX";
            this.LayoutControlItem6.Location = new System.Drawing.Point(269, 148);
            this.LayoutControlItem6.Name = "LayoutControlItem6";
            this.LayoutControlItem6.Size = new System.Drawing.Size(271, 24);
            this.LayoutControlItem6.Text = "FAX";
            this.LayoutControlItem6.TextSize = new System.Drawing.Size(50, 13);
            // 
            // LayoutControlItem9
            // 
            this.LayoutControlItem9.Control = this.button_OK;
            this.LayoutControlItem9.CustomizationFormText = "LayoutControlItem9";
            this.LayoutControlItem9.Location = new System.Drawing.Point(193, 279);
            this.LayoutControlItem9.Name = "LayoutControlItem9";
            this.LayoutControlItem9.Size = new System.Drawing.Size(79, 27);
            this.LayoutControlItem9.Text = "LayoutControlItem9";
            this.LayoutControlItem9.TextSize = new System.Drawing.Size(0, 0);
            this.LayoutControlItem9.TextToControlDistance = 0;
            this.LayoutControlItem9.TextVisible = false;
            // 
            // EmptySpaceItem1
            // 
            this.EmptySpaceItem1.AllowHotTrack = false;
            this.EmptySpaceItem1.CustomizationFormText = "EmptySpaceItem1";
            this.EmptySpaceItem1.Location = new System.Drawing.Point(351, 279);
            this.EmptySpaceItem1.Name = "EmptySpaceItem1";
            this.EmptySpaceItem1.Size = new System.Drawing.Size(189, 27);
            this.EmptySpaceItem1.Text = "EmptySpaceItem1";
            this.EmptySpaceItem1.TextSize = new System.Drawing.Size(0, 0);
            // 
            // EmptySpaceItem2
            // 
            this.EmptySpaceItem2.AllowHotTrack = false;
            this.EmptySpaceItem2.CustomizationFormText = "EmptySpaceItem2";
            this.EmptySpaceItem2.Location = new System.Drawing.Point(0, 279);
            this.EmptySpaceItem2.Name = "EmptySpaceItem2";
            this.EmptySpaceItem2.Size = new System.Drawing.Size(193, 27);
            this.EmptySpaceItem2.Text = "EmptySpaceItem2";
            this.EmptySpaceItem2.TextSize = new System.Drawing.Size(0, 0);
            // 
            // LayoutControlItem8
            // 
            this.LayoutControlItem8.AppearanceItemCaption.Options.UseTextOptions = true;
            this.LayoutControlItem8.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.LayoutControlItem8.AppearanceItemCaption.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Top;
            this.LayoutControlItem8.Control = this.memoEdit_notes;
            this.LayoutControlItem8.CustomizationFormText = "Notes";
            this.LayoutControlItem8.Location = new System.Drawing.Point(0, 196);
            this.LayoutControlItem8.Name = "LayoutControlItem8";
            this.LayoutControlItem8.Size = new System.Drawing.Size(540, 83);
            this.LayoutControlItem8.Text = "Notes";
            this.LayoutControlItem8.TextSize = new System.Drawing.Size(50, 13);
            // 
            // LayoutControlItem1
            // 
            this.LayoutControlItem1.Control = this.lookUpEdit_ContactType;
            this.LayoutControlItem1.CustomizationFormText = "Type";
            this.LayoutControlItem1.Location = new System.Drawing.Point(0, 0);
            this.LayoutControlItem1.Name = "LayoutControlItem1";
            this.LayoutControlItem1.Size = new System.Drawing.Size(540, 24);
            this.LayoutControlItem1.Text = "Type";
            this.LayoutControlItem1.TextSize = new System.Drawing.Size(50, 13);
            // 
            // Form_Contact
            // 
            this.AcceptButton = this.button_OK;
            this.CancelButton = this.Button_Cancel;
            this.ClientSize = new System.Drawing.Size(560, 326);
            this.Controls.Add(this.LayoutControl1);
            this.Name = "Form_Contact";
            this.Text = "Vendor Contact Information Update";
            ((System.ComponentModel.ISupportInitialize)(this.DxErrorProvider1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LayoutControl1)).EndInit();
            this.LayoutControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.lookUpEdit_ContactType.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emailRecordControl1.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.memoEdit_notes.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.telephoneNumberRecordControl_FAX)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.telephoneNumberRecordControl_Telephone)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.addressRecordControl1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit_title.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LayoutControlGroup1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LayoutControlItem2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LayoutControlItem3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LayoutControlItem4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LayoutControlItem10)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LayoutControlItem7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LayoutControlItem5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LayoutControlItem6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LayoutControlItem9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.EmptySpaceItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.EmptySpaceItem2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LayoutControlItem8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LayoutControlItem1)).EndInit();
            this.ResumeLayout(false);

        }
        private DevExpress.XtraEditors.SimpleButton button_OK;
        private DevExpress.XtraEditors.SimpleButton Button_Cancel;
        private DevExpress.XtraEditors.TextEdit textEdit_title;
        private DevExpress.XtraEditors.MemoEdit memoEdit_notes;
        private DebtPlus.Data.Controls.NameRecordControl nameRecordControl1;
        private DebtPlus.Data.Controls.AddressRecordControl addressRecordControl1;
        private DevExpress.XtraLayout.LayoutControl LayoutControl1;
        private DevExpress.XtraLayout.LayoutControlGroup LayoutControlGroup1;
        private DevExpress.XtraLayout.LayoutControlItem LayoutControlItem2;
        private DevExpress.XtraLayout.LayoutControlItem LayoutControlItem3;
        private DevExpress.XtraLayout.LayoutControlItem LayoutControlItem4;
        private DevExpress.XtraLayout.LayoutControlItem LayoutControlItem5;
        private DebtPlus.Data.Controls.EmailRecordControl emailRecordControl1;
        private DebtPlus.Data.Controls.TelephoneNumberRecordControl telephoneNumberRecordControl_Telephone;
        private DebtPlus.Data.Controls.TelephoneNumberRecordControl telephoneNumberRecordControl_FAX;
        private DevExpress.XtraLayout.LayoutControlItem LayoutControlItem8;
        private DevExpress.XtraLayout.LayoutControlItem LayoutControlItem10;
        private DevExpress.XtraLayout.LayoutControlItem LayoutControlItem7;
        private DevExpress.XtraLayout.LayoutControlItem LayoutControlItem6;
        private DevExpress.XtraLayout.LayoutControlItem LayoutControlItem9;
        private DevExpress.XtraLayout.EmptySpaceItem EmptySpaceItem1;
        private DevExpress.XtraLayout.EmptySpaceItem EmptySpaceItem2;
        private DevExpress.XtraEditors.LookUpEdit lookUpEdit_ContactType;
        private DevExpress.XtraLayout.LayoutControlItem LayoutControlItem1;
    }
}