using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Diagnostics;
using DebtPlus.UI.Vendor.Update.Controls;
using DebtPlus.UI.Vendor.Update.Pages;

namespace DebtPlus.UI.Vendor.Update.Forms
{
    partial class Form_VendorUpdate : DebtPlus.Data.Forms.DebtPlusForm
    {
        //Form overrides dispose to clean up the component list.
        [System.Diagnostics.DebuggerNonUserCode]
        protected override void Dispose(bool disposing)
        {
            try
            {
                if (disposing && components != null)
                {
                    components.Dispose();
                }
            }
            finally
            {
                base.Dispose(disposing);
            }
        }

        //Required by the Windows Form Designer
        private System.ComponentModel.IContainer components = null;

        //NOTE: The following procedure is required by the Windows Form Designer
        //It can be modified using the Windows Form Designer.  
        //Do not modify it using the code editor.
        [System.Diagnostics.DebuggerStepThrough]
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.XtraTabControl1 = new DevExpress.XtraTab.XtraTabControl();
            this.XtraTabPage_General = new DevExpress.XtraTab.XtraTabPage();
            this.XtraTabPage_Name = new DevExpress.XtraTab.XtraTabPage();
            this.XtraTabPage_Contacts = new DevExpress.XtraTab.XtraTabPage();
            this.XtraTabPage_Billing = new DevExpress.XtraTab.XtraTabPage();
            this.XtraTabPage_Payments = new DevExpress.XtraTab.XtraTabPage();
            this.XtraTabPage_Notes = new DevExpress.XtraTab.XtraTabPage();
            this.XtraTabPage_Aliases = new DevExpress.XtraTab.XtraTabPage();
            this.XtraTabPage_Statistics = new DevExpress.XtraTab.XtraTabPage();
            this.XtraTabPage_Interest = new DevExpress.XtraTab.XtraTabPage();
            this.barManager1 = new DevExpress.XtraBars.BarManager(this.components);
            this.StatusBar = new DevExpress.XtraBars.Bar();
            this.barStaticItem_Vendor = new DevExpress.XtraBars.BarStaticItem();
            this.barStaticItem_VendorName = new DevExpress.XtraBars.BarStaticItem();
            this.MainMenu = new DevExpress.XtraBars.Bar();
            this.barSubItem_File = new DevExpress.XtraBars.BarSubItem();
            this.barButtonItem_File_Exit = new DevExpress.XtraBars.BarButtonItem();
            this.barSubItem_Items = new DevExpress.XtraBars.BarSubItem();
            this.barSubItem_View = new DevExpress.XtraBars.BarSubItem();
            this.barSubItem_Letters = new DevExpress.XtraBars.BarSubItem();
            this.barSubItem_WWW = new DevExpress.XtraBars.BarSubItem();
            this.barButtonItem_www_user = new DevExpress.XtraBars.BarSubItem();
            this.barButtonItem_www_user_create = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItem_www_messages = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItem_www_password = new DevExpress.XtraBars.BarButtonItem();
            this.barSubItem_Help = new DevExpress.XtraBars.BarSubItem();
            this.barDockControlTop = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlBottom = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlLeft = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlRight = new DevExpress.XtraBars.BarDockControl();
            this.barButtonItem_File = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItem_View = new DevExpress.XtraBars.BarButtonItem();
            this.barSubItem_Reports = new DevExpress.XtraBars.BarSubItem();
            ((System.ComponentModel.ISupportInitialize)(this.DxErrorProvider1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.XtraTabControl1)).BeginInit();
            this.XtraTabControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).BeginInit();
            this.SuspendLayout();
            // 
            // DefaultLookAndFeel1
            // 
            this.DefaultLookAndFeel1.LookAndFeel.SkinName = "McSkin";
            // 
            // XtraTabControl1
            // 
            this.XtraTabControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.XtraTabControl1.Location = new System.Drawing.Point(0, 25);
            this.XtraTabControl1.MultiLine = DevExpress.Utils.DefaultBoolean.True;
            this.XtraTabControl1.Name = "XtraTabControl1";
            this.XtraTabControl1.SelectedTabPage = this.XtraTabPage_General;
            this.XtraTabControl1.Size = new System.Drawing.Size(584, 332);
            this.XtraTabControl1.TabIndex = 2;
            this.XtraTabControl1.TabPages.AddRange(new DevExpress.XtraTab.XtraTabPage[] {
            this.XtraTabPage_General,
            this.XtraTabPage_Name,
            this.XtraTabPage_Contacts,
            this.XtraTabPage_Billing,
            this.XtraTabPage_Payments,
            this.XtraTabPage_Notes,
            this.XtraTabPage_Aliases,
            this.XtraTabPage_Statistics});
            // 
            // XtraTabPage_General
            // 
            this.XtraTabPage_General.Name = "XtraTabPage_General";
            this.XtraTabPage_General.Size = new System.Drawing.Size(576, 302);
            this.XtraTabPage_General.Tag = "Page_General";
            this.XtraTabPage_General.Text = "General";
            // 
            // XtraTabPage_Name
            // 
            this.XtraTabPage_Name.Name = "XtraTabPage_Name";
            this.XtraTabPage_Name.Size = new System.Drawing.Size(576, 302);
            this.XtraTabPage_Name.Tag = "Page_Name";
            this.XtraTabPage_Name.Text = "Name";
            // 
            // XtraTabPage_Contacts
            // 
            this.XtraTabPage_Contacts.Name = "XtraTabPage_Contacts";
            this.XtraTabPage_Contacts.Size = new System.Drawing.Size(576, 302);
            this.XtraTabPage_Contacts.Tag = "Page_Contacts";
            this.XtraTabPage_Contacts.Text = "Contacts";
            // 
            // XtraTabPage_Billing
            // 
            this.XtraTabPage_Billing.Name = "XtraTabPage_Billing";
            this.XtraTabPage_Billing.Size = new System.Drawing.Size(576, 302);
            this.XtraTabPage_Billing.Tag = "Page_Billing";
            this.XtraTabPage_Billing.Text = "Billing";
            // 
            // XtraTabPage_Payments
            // 
            this.XtraTabPage_Payments.Name = "XtraTabPage_Payments";
            this.XtraTabPage_Payments.Size = new System.Drawing.Size(576, 302);
            this.XtraTabPage_Payments.Tag = "Page_Payments";
            this.XtraTabPage_Payments.Text = "Payment History";
            // 
            // XtraTabPage_Notes
            // 
            this.XtraTabPage_Notes.Name = "XtraTabPage_Notes";
            this.XtraTabPage_Notes.Size = new System.Drawing.Size(576, 302);
            this.XtraTabPage_Notes.Tag = "Page_Notes";
            this.XtraTabPage_Notes.Text = "Notes";
            // 
            // XtraTabPage_Aliases
            // 
            this.XtraTabPage_Aliases.Name = "XtraTabPage_Aliases";
            this.XtraTabPage_Aliases.Size = new System.Drawing.Size(576, 302);
            this.XtraTabPage_Aliases.Tag = "Page_Aliases";
            this.XtraTabPage_Aliases.Text = "Aliases";
            // 
            // XtraTabPage_Statistics
            // 
            this.XtraTabPage_Statistics.Name = "XtraTabPage_Statistics";
            this.XtraTabPage_Statistics.Size = new System.Drawing.Size(576, 302);
            this.XtraTabPage_Statistics.Tag = "Page_Statistics";
            this.XtraTabPage_Statistics.Text = "Statistics";
            // 
            // XtraTabPage_Interest
            // 
            this.XtraTabPage_Interest.Name = "XtraTabPage_Interest";
            this.XtraTabPage_Interest.Size = new System.Drawing.Size(0, 0);
            // 
            // barManager1
            // 
            this.barManager1.Bars.AddRange(new DevExpress.XtraBars.Bar[] {
            this.StatusBar,
            this.MainMenu});
            this.barManager1.DockControls.Add(this.barDockControlTop);
            this.barManager1.DockControls.Add(this.barDockControlBottom);
            this.barManager1.DockControls.Add(this.barDockControlLeft);
            this.barManager1.DockControls.Add(this.barDockControlRight);
            this.barManager1.Form = this;
            this.barManager1.Items.AddRange(new DevExpress.XtraBars.BarItem[] {
            this.barButtonItem_File,
            this.barButtonItem_View,
            this.barSubItem_File,
            this.barButtonItem_File_Exit,
            this.barSubItem_Items,
            this.barSubItem_View,
            this.barSubItem_Letters,
            this.barSubItem_Reports,
            this.barSubItem_WWW,
            this.barSubItem_Help,
            this.barButtonItem_www_user,
            this.barButtonItem_www_user_create,
            this.barButtonItem_www_messages,
            this.barButtonItem_www_password,
            this.barStaticItem_Vendor,
            this.barStaticItem_VendorName});
            this.barManager1.MainMenu = this.MainMenu;
            this.barManager1.MaxItemId = 20;
            this.barManager1.StatusBar = this.StatusBar;
            // 
            // StatusBar
            // 
            this.StatusBar.BarName = "Status bar";
            this.StatusBar.CanDockStyle = DevExpress.XtraBars.BarCanDockStyle.Bottom;
            this.StatusBar.DockCol = 0;
            this.StatusBar.DockRow = 0;
            this.StatusBar.DockStyle = DevExpress.XtraBars.BarDockStyle.Bottom;
            this.StatusBar.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.barStaticItem_Vendor),
            new DevExpress.XtraBars.LinkPersistInfo(this.barStaticItem_VendorName)});
            this.StatusBar.OptionsBar.AllowQuickCustomization = false;
            this.StatusBar.OptionsBar.DrawDragBorder = false;
            this.StatusBar.OptionsBar.UseWholeRow = true;
            this.StatusBar.Text = "Status bar";
            // 
            // barStaticItem_Vendor
            // 
            this.barStaticItem_Vendor.Id = 15;
            this.barStaticItem_Vendor.Name = "barStaticItem_Vendor";
            this.barStaticItem_Vendor.TextAlignment = System.Drawing.StringAlignment.Near;
            // 
            // barStaticItem_VendorName
            // 
            this.barStaticItem_VendorName.AutoSize = DevExpress.XtraBars.BarStaticItemSize.Spring;
            this.barStaticItem_VendorName.Id = 16;
            this.barStaticItem_VendorName.Name = "barStaticItem_VendorName";
            this.barStaticItem_VendorName.TextAlignment = System.Drawing.StringAlignment.Near;
            this.barStaticItem_VendorName.Width = 32;
            // 
            // MainMenu
            // 
            this.MainMenu.BarName = "Main menu";
            this.MainMenu.DockCol = 0;
            this.MainMenu.DockRow = 0;
            this.MainMenu.DockStyle = DevExpress.XtraBars.BarDockStyle.Top;
            this.MainMenu.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.barSubItem_File),
            new DevExpress.XtraBars.LinkPersistInfo(this.barSubItem_Items),
            new DevExpress.XtraBars.LinkPersistInfo(this.barSubItem_View),
            new DevExpress.XtraBars.LinkPersistInfo(this.barSubItem_Letters),
            new DevExpress.XtraBars.LinkPersistInfo(this.barSubItem_Reports),
            new DevExpress.XtraBars.LinkPersistInfo(this.barSubItem_WWW),
            new DevExpress.XtraBars.LinkPersistInfo(this.barSubItem_Help)});
            this.MainMenu.OptionsBar.MultiLine = true;
            this.MainMenu.OptionsBar.UseWholeRow = true;
            this.MainMenu.Text = "Main menu";
            // 
            // barSubItem_File
            // 
            this.barSubItem_File.Caption = "&File";
            this.barSubItem_File.Id = 2;
            this.barSubItem_File.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.barButtonItem_File_Exit)});
            this.barSubItem_File.Name = "barSubItem_File";
            // 
            // barButtonItem_File_Exit
            // 
            this.barButtonItem_File_Exit.Caption = "&Exit";
            this.barButtonItem_File_Exit.Id = 3;
            this.barButtonItem_File_Exit.Name = "barButtonItem_File_Exit";
            // 
            // barSubItem_Items
            // 
            this.barSubItem_Items.Caption = "&Items";
            this.barSubItem_Items.Id = 4;
            this.barSubItem_Items.Name = "barSubItem_Items";
            // 
            // barSubItem_View
            // 
            this.barSubItem_View.Caption = "&View";
            this.barSubItem_View.Id = 5;
            this.barSubItem_View.Name = "barSubItem_View";
            // 
            // barSubItem_Letters
            // 
            this.barSubItem_Letters.Caption = "&Letters";
            this.barSubItem_Letters.Id = 6;
            this.barSubItem_Letters.Name = "barSubItem_Letters";
            // 
            // barSubItem_WWW
            // 
            this.barSubItem_WWW.Caption = "&WWW";
            this.barSubItem_WWW.Id = 8;
            this.barSubItem_WWW.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.barButtonItem_www_user),
            new DevExpress.XtraBars.LinkPersistInfo(this.barButtonItem_www_messages),
            new DevExpress.XtraBars.LinkPersistInfo(this.barButtonItem_www_password)});
            this.barSubItem_WWW.Name = "barSubItem_WWW";
            // 
            // barButtonItem_www_user
            // 
            this.barButtonItem_www_user.Caption = "&User";
            this.barButtonItem_www_user.Id = 11;
            this.barButtonItem_www_user.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.barButtonItem_www_user_create)});
            this.barButtonItem_www_user.Name = "barButtonItem_www_user";
            // 
            // barButtonItem_www_user_create
            // 
            this.barButtonItem_www_user_create.Caption = "&Create....";
            this.barButtonItem_www_user_create.Id = 12;
            this.barButtonItem_www_user_create.Name = "barButtonItem_www_user_create";
            // 
            // barButtonItem_www_messages
            // 
            this.barButtonItem_www_messages.Caption = "&Messages...";
            this.barButtonItem_www_messages.Id = 13;
            this.barButtonItem_www_messages.Name = "barButtonItem_www_messages";
            // 
            // barButtonItem_www_password
            // 
            this.barButtonItem_www_password.Caption = "&Password...";
            this.barButtonItem_www_password.Id = 14;
            this.barButtonItem_www_password.Name = "barButtonItem_www_password";
            // 
            // barSubItem_Help
            // 
            this.barSubItem_Help.Caption = "&Help";
            this.barSubItem_Help.Id = 9;
            this.barSubItem_Help.Name = "barSubItem_Help";
            // 
            // barDockControlTop
            // 
            this.barDockControlTop.CausesValidation = false;
            this.barDockControlTop.Dock = System.Windows.Forms.DockStyle.Top;
            this.barDockControlTop.Location = new System.Drawing.Point(0, 0);
            this.barDockControlTop.Size = new System.Drawing.Size(584, 25);
            // 
            // barDockControlBottom
            // 
            this.barDockControlBottom.CausesValidation = false;
            this.barDockControlBottom.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.barDockControlBottom.Location = new System.Drawing.Point(0, 357);
            this.barDockControlBottom.Size = new System.Drawing.Size(584, 28);
            // 
            // barDockControlLeft
            // 
            this.barDockControlLeft.CausesValidation = false;
            this.barDockControlLeft.Dock = System.Windows.Forms.DockStyle.Left;
            this.barDockControlLeft.Location = new System.Drawing.Point(0, 25);
            this.barDockControlLeft.Size = new System.Drawing.Size(0, 332);
            // 
            // barDockControlRight
            // 
            this.barDockControlRight.CausesValidation = false;
            this.barDockControlRight.Dock = System.Windows.Forms.DockStyle.Right;
            this.barDockControlRight.Location = new System.Drawing.Point(584, 25);
            this.barDockControlRight.Size = new System.Drawing.Size(0, 332);
            // 
            // barButtonItem_File
            // 
            this.barButtonItem_File.Caption = "&File";
            this.barButtonItem_File.Id = 0;
            this.barButtonItem_File.Name = "barButtonItem_File";
            // 
            // barButtonItem_View
            // 
            this.barButtonItem_View.Caption = "&View";
            this.barButtonItem_View.Id = 1;
            this.barButtonItem_View.Name = "barButtonItem_View";
            // 
            // barSubItem_Reports
            // 
            this.barSubItem_Reports.Caption = "&Reports";
            this.barSubItem_Reports.Id = 19;
            this.barSubItem_Reports.Name = "barSubItem_Reports";
            // 
            // Form_VendorUpdate
            // 
            this.ClientSize = new System.Drawing.Size(584, 385);
            this.Controls.Add(this.XtraTabControl1);
            this.Controls.Add(this.barDockControlLeft);
            this.Controls.Add(this.barDockControlRight);
            this.Controls.Add(this.barDockControlBottom);
            this.Controls.Add(this.barDockControlTop);
            this.Name = "Form_VendorUpdate";
            this.Text = "Vendor Information";
            ((System.ComponentModel.ISupportInitialize)(this.DxErrorProvider1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.XtraTabControl1)).EndInit();
            this.XtraTabControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).EndInit();
            this.ResumeLayout(false);

        }
        private DevExpress.XtraTab.XtraTabControl XtraTabControl1;
        private DevExpress.XtraTab.XtraTabPage XtraTabPage_General;
        private DevExpress.XtraTab.XtraTabPage XtraTabPage_Name;
        private DevExpress.XtraTab.XtraTabPage XtraTabPage_Contacts;
        private DevExpress.XtraTab.XtraTabPage XtraTabPage_Billing;
        private DevExpress.XtraTab.XtraTabPage XtraTabPage_Interest;
        private DevExpress.XtraTab.XtraTabPage XtraTabPage_Payments;
        private DevExpress.XtraTab.XtraTabPage XtraTabPage_Notes;
        private DevExpress.XtraTab.XtraTabPage XtraTabPage_Aliases;
        private DevExpress.XtraTab.XtraTabPage XtraTabPage_Statistics;
        private DevExpress.XtraBars.BarManager barManager1;
        private DevExpress.XtraBars.Bar MainMenu;
        private DevExpress.XtraBars.Bar StatusBar;
        private DevExpress.XtraBars.BarDockControl barDockControlTop;
        private DevExpress.XtraBars.BarDockControl barDockControlBottom;
        private DevExpress.XtraBars.BarDockControl barDockControlLeft;
        private DevExpress.XtraBars.BarDockControl barDockControlRight;
        private DevExpress.XtraBars.BarSubItem barSubItem_File;
        private DevExpress.XtraBars.BarButtonItem barButtonItem_File_Exit;
        private DevExpress.XtraBars.BarSubItem barSubItem_Items;
        private DevExpress.XtraBars.BarButtonItem barButtonItem_File;
        private DevExpress.XtraBars.BarButtonItem barButtonItem_View;
        private DevExpress.XtraBars.BarSubItem barSubItem_View;
        private DevExpress.XtraBars.BarSubItem barSubItem_Letters;
        private DevExpress.XtraBars.BarSubItem barSubItem_WWW;
        private DevExpress.XtraBars.BarSubItem barSubItem_Help;
        private DevExpress.XtraBars.BarSubItem barButtonItem_www_user;
        private DevExpress.XtraBars.BarButtonItem barButtonItem_www_messages;
        private DevExpress.XtraBars.BarButtonItem barButtonItem_www_password;
        private DevExpress.XtraBars.BarStaticItem barStaticItem_Vendor;
        private DevExpress.XtraBars.BarStaticItem barStaticItem_VendorName;
        private DevExpress.XtraBars.BarButtonItem barButtonItem_www_user_create;
        private DevExpress.XtraBars.BarSubItem barSubItem_Reports;
    }
}