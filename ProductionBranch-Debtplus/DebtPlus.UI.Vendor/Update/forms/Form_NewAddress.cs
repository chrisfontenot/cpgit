#region "Copyright 2000-2012 DebtPlus, L.L.C."

//{*******************************************************************}
//{                                                                   }
//{       DebtPlus Debt Management System                             }
//{                                                                   }
//{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
//{       ALL RIGHTS RESERVED                                         }
//{                                                                   }
//{   The entire contents of this file is protected by U.S. and       }
//{   International Copyright Laws. Unauthorized reproduction,        }
//{   reverse-engineering, and distribution of all or any portion of  }
//{   the code contained in this file is strictly prohibited and may  }
//{   result in severe civil and criminal penalties and will be       }
//{   prosecuted to the maximum extent possible under the law.        }
//{                                                                   }
//{   RESTRICTIONS                                                    }
//{                                                                   }
//{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
//{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
//{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
//{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
//{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
//{                                                                   }
//{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
//{   ADDITIONAL RESTRICTIONS.                                        }
//{                                                                   }
//{*******************************************************************}

#endregion "Copyright 2000-2012 DebtPlus, L.L.C."

using System;
using System.Data.SqlClient;
using System.Text;
using DebtPlus.LINQ;

namespace DebtPlus.UI.Vendor.Update.Forms
{
    internal partial class Form_NewAddress
    {
        // Current record being updated
        private vendor_address record = null;

        private vendor vendorRecord = null;
        private DebtPlus.LINQ.BusinessContext bc = null;

        public Form_NewAddress()
            : base()
        {
            InitializeComponent();

            // We need an attention and first line of the "vendor" address field. Others, supress.
            adr.ShowAttn = true;
            adr.ShowCreditor1 = true;
            adr.ShowCreditor2 = false;
            adr.ShowLine3 = false;
        }

        public Form_NewAddress(DebtPlus.LINQ.BusinessContext bc, vendor VendorRecord, vendor_address Record)
            : this()
        {
            this.record = Record;
            this.vendorRecord = VendorRecord;
            this.bc = bc;
            RegisterHandlers();
        }

        /// <summary>
        /// Write the system note to the notes table. This is today static function so that it may be called
        /// from other modules without having to create the class. This probably should be moved out
        /// of this module and placed into today static module for later use.
        /// </summary>
        static internal void WriteVendorSystemNote(DebtPlus.LINQ.BusinessContext bc, int VendorID, string Subject, string Note)
        {
            try
            {
                var n = DebtPlus.LINQ.Factory.Manufacture_vendor_note();
                n.vendor = VendorID;
                n.subject = Subject;
                n.note = Note;

                bc.vendor_notes.InsertOnSubmit(n);
                bc.SubmitChanges();
            }
            catch (SqlException ex)
            {
                DebtPlus.UI.Common.ErrorHandling.HandleErrors(ex, "Error writing system note");
            }
        }

        /// <summary>
        /// Write today vendor system note
        /// </summary>
        internal void WriteVendorSystemNote(string Subject, string Note)
        {
            WriteVendorSystemNote(bc, vendorRecord.Id, Subject, Note);
        }

        /// <summary>
        /// When the address changes, update the system notes.
        /// </summary>
        private void adr_AddressChanged(object Sender, DebtPlus.Events.AddressChangedEventArgs e)
        {
            // Generate the address type from the row being updated
            string AddressDescription = DebtPlus.LINQ.InMemory.vendorAddressTypes.getList().Find(s => s.Id == record.address_type).description;

            // Build the address block
            StringBuilder Note = new StringBuilder();
            Note.Append("Changed ");
            Note.Append(string.IsNullOrWhiteSpace(AddressDescription) ? "Address" : AddressDescription);
            Note.Append(" from:");
            Note.Append(Environment.NewLine);
            Note.Append(e.OldAddress.ToString());

            Note.Append(Environment.NewLine);
            Note.Append(Environment.NewLine);

            Note.Append("To:");
            Note.Append(Environment.NewLine);
            Note.Append(e.NewAddress.ToString());

            string Subject = "Changed " + (string.IsNullOrWhiteSpace(AddressDescription) ? "Address" : AddressDescription);
            WriteVendorSystemNote(Subject, Note.ToString());
        }

        /// <summary>
        /// When OK button is clicked, save the address block
        /// </summary>
        private void Button_OK_Click(object sender, EventArgs e)
        {
            adr.AddressChanged += adr_AddressChanged;
            try
            {
                record.attn = DebtPlus.Utils.Nulls.v_String(adr.Attention);
                record.addressID = adr.EditValue;
            }
            finally
            {
                adr.AddressChanged -= adr_AddressChanged;
            }
        }

        /// <summary>
        /// Process the LOAD event for the form
        /// </summary>
        private void Form_NewAddress_Load(object sender, EventArgs e)
        {
            UnRegisterHandlers();
            try
            {
                // Load the value controls
                adr.EditValue = record.addressID;
                adr.Attention = record.attn;
            }
            finally
            {
                RegisterHandlers();
            }
        }

        private void RegisterHandlers()
        {
            Load += Form_NewAddress_Load;
            Button_OK.Click += Button_OK_Click;
        }

        private void UnRegisterHandlers()
        {
            Load -= Form_NewAddress_Load;
            Button_OK.Click -= Button_OK_Click;
        }
    }
}