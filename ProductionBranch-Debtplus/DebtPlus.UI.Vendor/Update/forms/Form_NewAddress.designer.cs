namespace DebtPlus.UI.Vendor.Update.Forms
{
    partial class Form_NewAddress : DebtPlus.Data.Forms.DebtPlusForm
    {
        //Form overrides dispose to clean up the component list.
        [System.Diagnostics.DebuggerNonUserCode]
        protected override void Dispose(bool disposing)
        {
            try
            {
                if (disposing && components != null)
                {
                    components.Dispose();
                }
            }
            finally
            {
                base.Dispose(disposing);
            }
        }

        //Required by the Windows Form Designer
        private System.ComponentModel.IContainer components = null;

        //NOTE: The following procedure is required by the Windows Form Designer
        //It can be modified using the Windows Form Designer.  
        //Do not modify it using the code editor.
        [System.Diagnostics.DebuggerStepThrough]
        private void InitializeComponent()
        {
            this.Button_Cancel = new DevExpress.XtraEditors.SimpleButton();
            this.Button_OK = new DevExpress.XtraEditors.SimpleButton();
            this.Address1 = new DevExpress.XtraEditors.TextEdit();
            this.adr = new DebtPlus.Data.Controls.AddressRecordControl();
            ((System.ComponentModel.ISupportInitialize)(this.DxErrorProvider1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Address1.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.adr)).BeginInit();
            this.SuspendLayout();
            // 
            // DefaultLookAndFeel1
            // 
            this.DefaultLookAndFeel1.LookAndFeel.SkinName = "McSkin";
            // 
            // Button_Cancel
            // 
            this.Button_Cancel.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.Button_Cancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.Button_Cancel.Location = new System.Drawing.Point(222, 151);
            this.Button_Cancel.Name = "Button_Cancel";
            this.Button_Cancel.Size = new System.Drawing.Size(75, 23);
            this.Button_Cancel.TabIndex = 3;
            this.Button_Cancel.Text = "&Cancel";
            // 
            // Button_OK
            // 
            this.Button_OK.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.Button_OK.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.Button_OK.Location = new System.Drawing.Point(142, 151);
            this.Button_OK.Name = "Button_OK";
            this.Button_OK.Size = new System.Drawing.Size(75, 23);
            this.Button_OK.TabIndex = 2;
            this.Button_OK.Text = "&OK";
            // 
            // Address1
            // 
            this.Address1.Location = new System.Drawing.Point(13, 20);
            this.Address1.Name = "Address1";
            this.Address1.Properties.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.Address1.Properties.MaxLength = 80;
            this.Address1.Size = new System.Drawing.Size(427, 20);
            this.Address1.TabIndex = 5;
            // 
            // adr
            // 
            this.adr.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.adr.Location = new System.Drawing.Point(5, 12);
            this.adr.Name = "adr";
            this.adr.Size = new System.Drawing.Size(434, 129);
            this.adr.TabIndex = 4;
            // 
            // Form_NewAddress
            // 
            this.ClientSize = new System.Drawing.Size(444, 189);
            this.Controls.Add(this.adr);
            this.Controls.Add(this.Button_OK);
            this.Controls.Add(this.Button_Cancel);
            this.Name = "Form_NewAddress";
            this.Text = "Update Address Information";
            ((System.ComponentModel.ISupportInitialize)(this.DxErrorProvider1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Address1.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.adr)).EndInit();
            this.ResumeLayout(false);

        }

        private DevExpress.XtraEditors.SimpleButton Button_Cancel;
        private DevExpress.XtraEditors.SimpleButton Button_OK;
        private DevExpress.XtraEditors.TextEdit Address1;
        private Data.Controls.AddressRecordControl adr;
    }
}
