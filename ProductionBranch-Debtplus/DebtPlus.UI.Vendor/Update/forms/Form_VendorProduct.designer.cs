namespace DebtPlus.UI.Vendor.Update.Forms
{
    partial class Form_VendorProduct
    {
        [System.Diagnostics.DebuggerNonUserCode]
        protected override void Dispose(bool disposing)
        {
            try
            {
                if (disposing && components != null)
                {
                    components.Dispose();
                }
            }
            finally
            {
                base.Dispose(disposing);
            }
        }

        //Required by the Windows Form Designer
        private System.ComponentModel.IContainer components = null;

        //NOTE: The following procedure is required by the Windows Form Designer
        //It can be modified using the Windows Form Designer.  
        //Do not modify it using the code editor.
        [System.Diagnostics.DebuggerStepThrough]
        private void InitializeComponent()
        {
            this.effective_date = new DevExpress.XtraEditors.DateEdit();
            this.layoutControl1 = new DevExpress.XtraLayout.LayoutControl();
            this.labelControlPrice = new DevExpress.XtraEditors.LabelControl();
            this.Button_Cancel = new DevExpress.XtraEditors.SimpleButton();
            this.Button_OK = new DevExpress.XtraEditors.SimpleButton();
            this.lookUpEdit_ProductAndService = new DevExpress.XtraEditors.LookUpEdit();
            this.layoutControlGroup1 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem2 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem5 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem6 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem1 = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem1 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.emptySpaceItemLeft = new DevExpress.XtraLayout.EmptySpaceItem();
            this.emptySpaceItemRight = new DevExpress.XtraLayout.EmptySpaceItem();
            this.layoutControlItem3 = new DevExpress.XtraLayout.LayoutControlItem();
            this.lookUpEdit_EscrowProBono = new DevExpress.XtraEditors.LookUpEdit();
            this.layoutControlItem4 = new DevExpress.XtraLayout.LayoutControlItem();
            ((System.ComponentModel.ISupportInitialize)(this.DxErrorProvider1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.effective_date.Properties.VistaTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.effective_date.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl1)).BeginInit();
            this.layoutControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.lookUpEdit_ProductAndService.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItemLeft)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItemRight)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lookUpEdit_EscrowProBono.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem4)).BeginInit();
            this.SuspendLayout();
            // 
            // DefaultLookAndFeel1
            // 
            this.DefaultLookAndFeel1.LookAndFeel.SkinName = "McSkin";
            // 
            // effective_date
            // 
            this.effective_date.EditValue = null;
            this.effective_date.Location = new System.Drawing.Point(91, 12);
            this.effective_date.Name = "effective_date";
            this.effective_date.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.False;
            this.effective_date.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.effective_date.Properties.VistaTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.effective_date.Size = new System.Drawing.Size(209, 20);
            this.effective_date.StyleController = this.layoutControl1;
            this.effective_date.TabIndex = 1;
            this.effective_date.ToolTipController = this.ToolTipController1;
            // 
            // layoutControl1
            // 
            this.layoutControl1.Controls.Add(this.lookUpEdit_EscrowProBono);
            this.layoutControl1.Controls.Add(this.labelControlPrice);
            this.layoutControl1.Controls.Add(this.Button_Cancel);
            this.layoutControl1.Controls.Add(this.Button_OK);
            this.layoutControl1.Controls.Add(this.effective_date);
            this.layoutControl1.Controls.Add(this.lookUpEdit_ProductAndService);
            this.layoutControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.layoutControl1.Location = new System.Drawing.Point(0, 0);
            this.layoutControl1.Name = "layoutControl1";
            this.layoutControl1.Root = this.layoutControlGroup1;
            this.layoutControl1.Size = new System.Drawing.Size(312, 174);
            this.layoutControl1.TabIndex = 15;
            this.layoutControl1.Text = "layoutControl1";
            // 
            // labelControlPrice
            // 
            this.labelControlPrice.Location = new System.Drawing.Point(91, 84);
            this.labelControlPrice.Name = "labelControlPrice";
            this.labelControlPrice.Size = new System.Drawing.Size(28, 13);
            this.labelControlPrice.StyleController = this.layoutControl1;
            this.labelControlPrice.TabIndex = 15;
            this.labelControlPrice.Text = "$0.00";
            // 
            // Button_Cancel
            // 
            this.Button_Cancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.Button_Cancel.Location = new System.Drawing.Point(79, 139);
            this.Button_Cancel.MaximumSize = new System.Drawing.Size(75, 23);
            this.Button_Cancel.MinimumSize = new System.Drawing.Size(75, 23);
            this.Button_Cancel.Name = "Button_Cancel";
            this.Button_Cancel.Size = new System.Drawing.Size(75, 23);
            this.Button_Cancel.StyleController = this.layoutControl1;
            this.Button_Cancel.TabIndex = 11;
            this.Button_Cancel.Text = "&Cancel";
            this.Button_Cancel.ToolTipController = this.ToolTipController1;
            // 
            // Button_OK
            // 
            this.Button_OK.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.Button_OK.Enabled = false;
            this.Button_OK.Location = new System.Drawing.Point(158, 139);
            this.Button_OK.MaximumSize = new System.Drawing.Size(75, 23);
            this.Button_OK.MinimumSize = new System.Drawing.Size(75, 23);
            this.Button_OK.Name = "Button_OK";
            this.Button_OK.Size = new System.Drawing.Size(75, 23);
            this.Button_OK.StyleController = this.layoutControl1;
            this.Button_OK.TabIndex = 10;
            this.Button_OK.Text = "&OK";
            this.Button_OK.ToolTipController = this.ToolTipController1;
            // 
            // lookUpEdit_ProductAndService
            // 
            this.lookUpEdit_ProductAndService.Location = new System.Drawing.Point(91, 36);
            this.lookUpEdit_ProductAndService.Name = "lookUpEdit_ProductAndService";
            this.lookUpEdit_ProductAndService.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.False;
            this.lookUpEdit_ProductAndService.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.lookUpEdit_ProductAndService.Properties.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("Id", "ID", 20, DevExpress.Utils.FormatType.Numeric, "", false, DevExpress.Utils.HorzAlignment.Default),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("description", "Description", 20, DevExpress.Utils.FormatType.None, "", true, DevExpress.Utils.HorzAlignment.Default, DevExpress.Data.ColumnSortOrder.Ascending)});
            this.lookUpEdit_ProductAndService.Properties.DisplayMember = "description";
            this.lookUpEdit_ProductAndService.Properties.NullText = "";
            this.lookUpEdit_ProductAndService.Properties.PopupSizeable = false;
            this.lookUpEdit_ProductAndService.Properties.ShowFooter = false;
            this.lookUpEdit_ProductAndService.Properties.ShowHeader = false;
            this.lookUpEdit_ProductAndService.Properties.SortColumnIndex = 1;
            this.lookUpEdit_ProductAndService.Properties.ValueMember = "Id";
            this.lookUpEdit_ProductAndService.Size = new System.Drawing.Size(209, 20);
            this.lookUpEdit_ProductAndService.StyleController = this.layoutControl1;
            this.lookUpEdit_ProductAndService.TabIndex = 4;
            this.lookUpEdit_ProductAndService.ToolTipController = this.ToolTipController1;
            // 
            // layoutControlGroup1
            // 
            this.layoutControlGroup1.CustomizationFormText = "layoutControlGroup1";
            this.layoutControlGroup1.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.True;
            this.layoutControlGroup1.GroupBordersVisible = false;
            this.layoutControlGroup1.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem2,
            this.layoutControlItem5,
            this.layoutControlItem6,
            this.layoutControlItem1,
            this.emptySpaceItem1,
            this.emptySpaceItemLeft,
            this.emptySpaceItemRight,
            this.layoutControlItem3,
            this.layoutControlItem4});
            this.layoutControlGroup1.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup1.Name = "layoutControlGroup1";
            this.layoutControlGroup1.Size = new System.Drawing.Size(312, 174);
            this.layoutControlGroup1.Text = "layoutControlGroup1";
            this.layoutControlGroup1.TextVisible = false;
            // 
            // layoutControlItem2
            // 
            this.layoutControlItem2.Control = this.lookUpEdit_ProductAndService;
            this.layoutControlItem2.CustomizationFormText = "Product/Service";
            this.layoutControlItem2.Location = new System.Drawing.Point(0, 24);
            this.layoutControlItem2.Name = "layoutControlItem2";
            this.layoutControlItem2.Size = new System.Drawing.Size(292, 24);
            this.layoutControlItem2.Text = "Product/Service";
            this.layoutControlItem2.TextSize = new System.Drawing.Size(76, 13);
            // 
            // layoutControlItem5
            // 
            this.layoutControlItem5.Control = this.Button_OK;
            this.layoutControlItem5.CustomizationFormText = "layoutControlItem5";
            this.layoutControlItem5.Location = new System.Drawing.Point(146, 127);
            this.layoutControlItem5.Name = "layoutControlItem5";
            this.layoutControlItem5.Size = new System.Drawing.Size(79, 27);
            this.layoutControlItem5.Text = "layoutControlItem5";
            this.layoutControlItem5.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem5.TextToControlDistance = 0;
            this.layoutControlItem5.TextVisible = false;
            // 
            // layoutControlItem6
            // 
            this.layoutControlItem6.Control = this.Button_Cancel;
            this.layoutControlItem6.CustomizationFormText = "layoutControlItem6";
            this.layoutControlItem6.Location = new System.Drawing.Point(67, 127);
            this.layoutControlItem6.Name = "layoutControlItem6";
            this.layoutControlItem6.Size = new System.Drawing.Size(79, 27);
            this.layoutControlItem6.Text = "layoutControlItem6";
            this.layoutControlItem6.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem6.TextToControlDistance = 0;
            this.layoutControlItem6.TextVisible = false;
            // 
            // layoutControlItem1
            // 
            this.layoutControlItem1.Control = this.effective_date;
            this.layoutControlItem1.CustomizationFormText = "Effective &Date:";
            this.layoutControlItem1.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem1.Name = "layoutControlItem1";
            this.layoutControlItem1.Size = new System.Drawing.Size(292, 24);
            this.layoutControlItem1.Text = "Effective &Date:";
            this.layoutControlItem1.TextSize = new System.Drawing.Size(76, 13);
            // 
            // emptySpaceItem1
            // 
            this.emptySpaceItem1.AllowHotTrack = false;
            this.emptySpaceItem1.CustomizationFormText = "emptySpaceItem1";
            this.emptySpaceItem1.Location = new System.Drawing.Point(0, 89);
            this.emptySpaceItem1.Name = "emptySpaceItem1";
            this.emptySpaceItem1.Size = new System.Drawing.Size(292, 38);
            this.emptySpaceItem1.Text = "emptySpaceItem1";
            this.emptySpaceItem1.TextSize = new System.Drawing.Size(0, 0);
            // 
            // emptySpaceItemLeft
            // 
            this.emptySpaceItemLeft.AllowHotTrack = false;
            this.emptySpaceItemLeft.CustomizationFormText = "emptySpaceItemLeft";
            this.emptySpaceItemLeft.Location = new System.Drawing.Point(0, 127);
            this.emptySpaceItemLeft.Name = "emptySpaceItemLeft";
            this.emptySpaceItemLeft.Size = new System.Drawing.Size(67, 27);
            this.emptySpaceItemLeft.Text = "emptySpaceItemLeft";
            this.emptySpaceItemLeft.TextSize = new System.Drawing.Size(0, 0);
            // 
            // emptySpaceItemRight
            // 
            this.emptySpaceItemRight.AllowHotTrack = false;
            this.emptySpaceItemRight.CustomizationFormText = "emptySpaceItemRight";
            this.emptySpaceItemRight.Location = new System.Drawing.Point(225, 127);
            this.emptySpaceItemRight.Name = "emptySpaceItemRight";
            this.emptySpaceItemRight.Size = new System.Drawing.Size(67, 27);
            this.emptySpaceItemRight.Text = "emptySpaceItemRight";
            this.emptySpaceItemRight.TextSize = new System.Drawing.Size(0, 0);
            // 
            // layoutControlItem3
            // 
            this.layoutControlItem3.Control = this.labelControlPrice;
            this.layoutControlItem3.CustomizationFormText = "Price";
            this.layoutControlItem3.Location = new System.Drawing.Point(0, 72);
            this.layoutControlItem3.Name = "layoutControlItem3";
            this.layoutControlItem3.Size = new System.Drawing.Size(292, 17);
            this.layoutControlItem3.Text = "Price";
            this.layoutControlItem3.TextSize = new System.Drawing.Size(76, 13);
            // 
            // lookUpEdit_EscrowProBono
            // 
            this.lookUpEdit_EscrowProBono.Location = new System.Drawing.Point(91, 60);
            this.lookUpEdit_EscrowProBono.Name = "lookUpEdit_EscrowProBono";
            this.lookUpEdit_EscrowProBono.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.lookUpEdit_EscrowProBono.Properties.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("Id", "ID", 20, DevExpress.Utils.FormatType.Numeric, "f0", false, DevExpress.Utils.HorzAlignment.Far, DevExpress.Data.ColumnSortOrder.None, DevExpress.Utils.DefaultBoolean.True),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("description", "Description", 20, DevExpress.Utils.FormatType.None, "", true, DevExpress.Utils.HorzAlignment.Near, DevExpress.Data.ColumnSortOrder.Ascending, DevExpress.Utils.DefaultBoolean.True)});
            this.lookUpEdit_EscrowProBono.Properties.DisplayMember = "description";
            this.lookUpEdit_EscrowProBono.Properties.NullText = "";
            this.lookUpEdit_EscrowProBono.Properties.ShowFooter = false;
            this.lookUpEdit_EscrowProBono.Properties.ShowHeader = false;
            this.lookUpEdit_EscrowProBono.Properties.ValueMember = "Id";
            this.lookUpEdit_EscrowProBono.Size = new System.Drawing.Size(209, 20);
            this.lookUpEdit_EscrowProBono.StyleController = this.layoutControl1;
            this.lookUpEdit_EscrowProBono.TabIndex = 16;
            // 
            // layoutControlItem4
            // 
            this.layoutControlItem4.Control = this.lookUpEdit_EscrowProBono;
            this.layoutControlItem4.CustomizationFormText = "Escrow Status";
            this.layoutControlItem4.Location = new System.Drawing.Point(0, 48);
            this.layoutControlItem4.Name = "layoutControlItem4";
            this.layoutControlItem4.Size = new System.Drawing.Size(292, 24);
            this.layoutControlItem4.Text = "Escrow Status";
            this.layoutControlItem4.TextSize = new System.Drawing.Size(76, 13);
            // 
            // Form_VendorProduct
            // 
            this.AcceptButton = this.Button_OK;
            this.CancelButton = this.Button_Cancel;
            this.ClientSize = new System.Drawing.Size(312, 174);
            this.Controls.Add(this.layoutControl1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.MaximizeBox = false;
            this.Name = "Form_VendorProduct";
            this.ShowInTaskbar = false;
            this.Text = "Vendor Product Offering";
            ((System.ComponentModel.ISupportInitialize)(this.DxErrorProvider1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.effective_date.Properties.VistaTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.effective_date.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl1)).EndInit();
            this.layoutControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.lookUpEdit_ProductAndService.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItemLeft)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItemRight)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lookUpEdit_EscrowProBono.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem4)).EndInit();
            this.ResumeLayout(false);

        }
        private DevExpress.XtraEditors.SimpleButton Button_OK;
        private DevExpress.XtraEditors.SimpleButton Button_Cancel;
        private DevExpress.XtraEditors.DateEdit effective_date;
        private DevExpress.XtraEditors.LookUpEdit lookUpEdit_ProductAndService;
        private DevExpress.XtraLayout.LayoutControl layoutControl1;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup1;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem2;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem5;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem6;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem1;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem1;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItemLeft;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItemRight;
        private DevExpress.XtraEditors.LabelControl labelControlPrice;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem3;
        private DevExpress.XtraEditors.LookUpEdit lookUpEdit_EscrowProBono;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem4;
    }
}