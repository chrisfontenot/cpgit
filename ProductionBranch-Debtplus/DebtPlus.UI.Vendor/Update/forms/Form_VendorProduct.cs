#region "Copyright 2000-2012 DebtPlus, L.L.C."

//{*******************************************************************}
//{                                                                   }
//{       DebtPlus Debt Management System                             }
//{                                                                   }
//{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
//{       ALL RIGHTS RESERVED                                         }
//{                                                                   }
//{   The entire contents of this file is protected by U.S. and       }
//{   International Copyright Laws. Unauthorized reproduction,        }
//{   reverse-engineering, and distribution of all or any portion of  }
//{   the code contained in this file is strictly prohibited and may  }
//{   result in severe civil and criminal penalties and will be       }
//{   prosecuted to the maximum extent possible under the law.        }
//{                                                                   }
//{   RESTRICTIONS                                                    }
//{                                                                   }
//{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
//{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
//{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
//{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
//{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
//{                                                                   }
//{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
//{   ADDITIONAL RESTRICTIONS.                                        }
//{                                                                   }
//{*******************************************************************}

#endregion "Copyright 2000-2012 DebtPlus, L.L.C."

using System;
using System.Linq;
using DebtPlus.LINQ;

namespace DebtPlus.UI.Vendor.Update.Forms
{
    internal partial class Form_VendorProduct : DebtPlus.Data.Forms.DebtPlusForm
    {
        // Current record being edited
        private vendor_product record;
        private DebtPlus.LINQ.BusinessContext bc = null;

        /// <summary>
        /// Initialize the form
        /// </summary>
        public Form_VendorProduct()
            : base()
        {
            InitializeComponent();
        }

        public Form_VendorProduct(DebtPlus.LINQ.BusinessContext bc, vendor_product Record) : this()
        {
            this.bc = bc;
            this.record = Record;

            effective_date.Properties.MinValue = DateTime.Now.Date;

            RegisterHandlers();
        }

        private void RegisterHandlers()
        {
            Load += Form_VendorProduct_Load;
            effective_date.EditValueChanged += EditValueChanged;
            Button_OK.Click += Button_OK_Click;
            lookUpEdit_ProductAndService.EditValueChanged += lookUpEdit_ProductAndService_EditValueChanged;
            lookUpEdit_ProductAndService.EditValueChanging += DebtPlus.Data.Validation.LookUpEdit_ActiveTest;
            Resize += Form_VendorProduct_Resize;
        }

        private void UnRegisterHandlers()
        {
            Load -= Form_VendorProduct_Load;
            effective_date.EditValueChanged -= EditValueChanged;
            Button_OK.Click -= Button_OK_Click;
            lookUpEdit_ProductAndService.EditValueChanged -= lookUpEdit_ProductAndService_EditValueChanged;
            lookUpEdit_ProductAndService.EditValueChanging -= DebtPlus.Data.Validation.LookUpEdit_ActiveTest;
            Resize -= Form_VendorProduct_Resize;
        }

        private void Button_OK_Click(object sender, EventArgs e)
        {
            record.escrowProBono  = DebtPlus.Utils.Nulls.v_Int32(lookUpEdit_EscrowProBono.EditValue).GetValueOrDefault();
            record.effective_date = effective_date.DateTime.Date;

            // THe product reference must be relative to the current datacontext (businesscontext) object
            Int32? productID = DebtPlus.Utils.Nulls.v_Int32(lookUpEdit_ProductAndService.EditValue);
            record.product1  = productID.HasValue ? bc.products.Where(s => s.Id == productID.Value).FirstOrDefault() : null;
        }

        /// <summary>
        /// Resize the button spacing to move them to the center of the form
        /// </summary>
        private void Form_VendorProduct_Resize(object sender, EventArgs e)
        {
            try
            {
                emptySpaceItemLeft.Width = (emptySpaceItemLeft.Width + emptySpaceItemRight.Width) >> 1;
            }
            catch { }
        }

        private void EditValueChanged(object sender, EventArgs e)
        {
            Button_OK.Enabled = !HasErrors();
        }

        private void Form_VendorProduct_Load(object sender, EventArgs e)
        {
            UnRegisterHandlers();
            try
            {
                lookUpEdit_ProductAndService.Properties.DataSource = DebtPlus.LINQ.Cache.Product.getList().Where(s => s.ActiveFlag || s.Id == record.product).ToList();
                lookUpEdit_EscrowProBono.Properties.DataSource = DebtPlus.LINQ.InMemory.EscrowProBonoTypes.getList();

                lookUpEdit_EscrowProBono.EditValue     = record.escrowProBono;
                lookUpEdit_ProductAndService.EditValue = record.product;
                effective_date.EditValue               = record.effective_date;

                // Find the amount of the item if one is present
                labelControlPrice.Text = record.product1 != null ? string.Format("{0:c}", record.product1.Amount) : string.Empty;

                Button_OK.Enabled = !HasErrors();
            }
            finally
            {
                RegisterHandlers();
            }
        }

        private bool HasErrors()
        {
            // The escrow status must not be empty
            if (! DebtPlus.Utils.Nulls.v_Int32(lookUpEdit_EscrowProBono.EditValue).HasValue)
            {
                return true;
            }

            // The product must be defined.
            return ! DebtPlus.Utils.Nulls.v_Int32(lookUpEdit_ProductAndService.EditValue).HasValue;
        }

        private void lookUpEdit_ProductAndService_EditValueChanged(object sender, EventArgs e)
        {
            // Find the selected row. From that, take the amount as the figure for the service amount.
            // The amount is display only and can not be changed by choosing a product nor vendor.
            var q = lookUpEdit_ProductAndService.GetSelectedDataRow() as DebtPlus.LINQ.product;
            labelControlPrice.Text = q == null ? string.Empty : string.Format("{0:c}", q.Amount);

            Button_OK.Enabled = !HasErrors();
        }
    }
}