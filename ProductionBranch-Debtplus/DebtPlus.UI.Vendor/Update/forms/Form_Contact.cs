#region "Copyright 2000-2012 DebtPlus, L.L.C."

//{*******************************************************************}
//{                                                                   }
//{       DebtPlus Debt Management System                             }
//{                                                                   }
//{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
//{       ALL RIGHTS RESERVED                                         }
//{                                                                   }
//{   The entire contents of this file is protected by U.S. and       }
//{   International Copyright Laws. Unauthorized reproduction,        }
//{   reverse-engineering, and distribution of all or any portion of  }
//{   the code contained in this file is strictly prohibited and may  }
//{   result in severe civil and criminal penalties and will be       }
//{   prosecuted to the maximum extent possible under the law.        }
//{                                                                   }
//{   RESTRICTIONS                                                    }
//{                                                                   }
//{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
//{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
//{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
//{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
//{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
//{                                                                   }
//{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
//{   ADDITIONAL RESTRICTIONS.                                        }
//{                                                                   }
//{*******************************************************************}

#endregion "Copyright 2000-2012 DebtPlus, L.L.C."

using System;
using System.Linq;
using DebtPlus.LINQ;

namespace DebtPlus.UI.Vendor.Update.Forms
{
    internal partial class Form_Contact : DebtPlus.Data.Forms.DebtPlusForm
    {
        // Current record being updated
        private vendor_contact record = null;

        /// <summary>
        /// Initialize the form
        /// </summary>
        public Form_Contact()
            : base()
        {
            InitializeComponent();
        }

        /// <summary>
        /// Initialize the form
        /// </summary>
        public Form_Contact(vendor_contact Record)
            : this()
        {
            this.record = Record;

            RegisterHandlers();
        }

        private void Button_OK_Click(object sender, EventArgs e)
        {
            record.nameID       = nameRecordControl1.EditValue;
            record.addressID    = addressRecordControl1.EditValue;
            record.telephoneID  = telephoneNumberRecordControl_Telephone.EditValue;
            record.faxID        = telephoneNumberRecordControl_FAX.EditValue;
            record.emailID      = emailRecordControl1.EditValue;
            record.contact_type = DebtPlus.Utils.Nulls.v_Int32(lookUpEdit_ContactType.EditValue).GetValueOrDefault();
            record.Title        = DebtPlus.Utils.Nulls.v_String(textEdit_title.EditValue);
            record.note         = DebtPlus.Utils.Nulls.v_String(memoEdit_notes.EditValue);
        }

        private void Form_Changed(object sender, System.EventArgs e)
        {
            button_OK.Enabled = !HasErrors();
        }

        /// <summary>
        /// Initialize the form with context information
        /// </summary>
        private void Form_Contact_Load(object sender, EventArgs e)
        {
            UnRegisterHandlers();
            try
            {
                // Set the lookup entries as needed
                lookUpEdit_ContactType.Properties.DataSource = DebtPlus.LINQ.Cache.vendor_contact_type.getList().Where(s => s.ActiveFlag).ToList();

                // Hook the record into the controls
                nameRecordControl1.EditValue                     = record.nameID;
                addressRecordControl1.EditValue                  = record.addressID;
                telephoneNumberRecordControl_Telephone.EditValue = record.telephoneID;
                telephoneNumberRecordControl_FAX.EditValue       = record.faxID;
                emailRecordControl1.EditValue                    = record.emailID;
                lookUpEdit_ContactType.EditValue                 = record.contact_type;
                textEdit_title.EditValue                         = record.Title;
                memoEdit_notes.EditValue                         = record.note;

                // Enable or disable the OK button based upon the control settings
                button_OK.Enabled = !HasErrors();
            }
            finally
            {
                RegisterHandlers();
            }
        }

        private bool HasErrors()
        {
            return lookUpEdit_ContactType.EditValue == null;
        }

        private void RegisterHandlers()
        {
            Load                                     += Form_Contact_Load;
            lookUpEdit_ContactType.EditValueChanged  += Form_Changed;
            lookUpEdit_ContactType.EditValueChanging += DebtPlus.Data.Validation.LookUpEdit_ActiveTest;
            button_OK.Click                          += Button_OK_Click;
        }

        private void UnRegisterHandlers()
        {
            Load                                     -= Form_Contact_Load;
            lookUpEdit_ContactType.EditValueChanged  -= Form_Changed;
            lookUpEdit_ContactType.EditValueChanging -= DebtPlus.Data.Validation.LookUpEdit_ActiveTest;
            button_OK.Click                          -= Button_OK_Click;
        }
    }
}