﻿#region "Copyright 2000-2012 DebtPlus, L.L.C."

//{*******************************************************************}
//{                                                                   }
//{       DebtPlus Debt Management System                             }
//{                                                                   }
//{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
//{       ALL RIGHTS RESERVED                                         }
//{                                                                   }
//{   The entire contents of this file is protected by U.S. and       }
//{   International Copyright Laws. Unauthorized reproduction,        }
//{   reverse-engineering, and distribution of all or any portion of  }
//{   the code contained in this file is strictly prohibited and may  }
//{   result in severe civil and criminal penalties and will be       }
//{   prosecuted to the maximum extent possible under the law.        }
//{                                                                   }
//{   RESTRICTIONS                                                    }
//{                                                                   }
//{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
//{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
//{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
//{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
//{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
//{                                                                   }
//{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
//{   ADDITIONAL RESTRICTIONS.                                        }
//{                                                                   }
//{*******************************************************************}

#endregion "Copyright 2000-2012 DebtPlus, L.L.C."

using System.Windows.Forms;
using System.Linq;

namespace DebtPlus.UI.Vendor.Update.Forms
{
    partial class Form_VendorUpdate
    {
        /// <summary>
        /// Routine called when a report or letter menu item is chosen. This will update the database
        /// at the current time to ensure that the data is consistent with the entries for the current page.
        /// </summary>
        /// <param name="sender">BarManager when the event occurs</param>
        /// <param name="e">Pointer to the menus that were chosen</param>
        private void LetterReport_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            // Make sure that we are in the proper thread context to save the data.
            if (InvokeRequired)
            {
                var ia = BeginInvoke(new DevExpress.XtraBars.ItemClickEventHandler(LetterReport_ItemClick), new object[] { sender, e });
                EndInvoke(ia);
            }
            else
            {
                SaveForm();
            }
        }

        /// <summary>
        /// Save the form data to the database. The current tab page data is saved before the creditor
        /// record information is updated.
        /// </summary>
        private void SaveForm()
        {
            // Force the current control to be validated if this is needed. We do this by forcing
            // a tab switch to the next control in the page.
            Control ctl = this.ActiveControl;
            if (ctl != null)
            {
                SelectNextControl(ctl, true, false, true, true);
            }

            // Do the save operation on the current tab page before we try to save the creditor.
            var tabPage = XtraTabControl1.SelectedTabPage;
            if (tabPage != null && tabPage.Controls.Count > 0)
            {
                var CurrentPage = tabPage.Controls[0] as DebtPlus.UI.Vendor.Update.Controls.ControlBase;
                if (CurrentPage != null)
                {
                    CurrentPage.SaveForm(bc);
                }
            }

            try
            {
                // Generate the system note showing the change to the data and save the data
                GenerateSystemNote(vendorRecord);
                bc.SubmitChanges();
            }
            catch (System.Data.SqlClient.SqlException ex)
            {
                DebtPlus.UI.Common.ErrorHandling.HandleErrors(ex, "Error updating vendor information");
            }
        }

        /// <summary>
        /// Generate a system note reflecting the changes to the vendor record.
        /// </summary>
        /// <param name="vendor"></param>
        private void GenerateSystemNote(DebtPlus.LINQ.vendor record)
        {
            // Get the list of changed fields since the last system note was generated
            var colChanges = record.getChangedFieldItems;
            if (colChanges.Count < 1)
            {
                return;
            }

            // Generate the note with the changed information.
            var sbNote = new System.Text.StringBuilder();
            var sbSubject = new System.Text.StringBuilder();
            foreach (var item in colChanges)
            {
                // Get the name changes from the collection. Sometimes people will change
                // the field and then change it back. While the database won't update it,
                // we capture the original change and say that it is being changed to the
                // same value. So, don't generate the change note in these instances.
                string fieldName = item.FieldName ?? string.Empty;
                string oldValue = (item.OldValue ?? string.Empty).ToString();
                string newValue = (item.NewValue ?? string.Empty).ToString();

                if (!string.IsNullOrEmpty(fieldName) && string.Compare(oldValue, newValue, true) != 0)
                {
                    sbSubject.AppendFormat(",{0}", fieldName);
                    sbNote.AppendFormat("\r\nChanged {0} from '{1}' to '{2}'", item.FieldName, (item.OldValue ?? string.Empty).ToString(), (item.NewValue ?? string.Empty).ToString());
                }
            }

            // Toss the leading cr/lf sequence.
            if (sbNote.Length > 0)
            {
                sbNote.Remove(0, 2);
                sbSubject.Remove(0, 1);

                // The subject is limited to 80 characters.
                string subject = string.Format("Changed {0}", sbSubject.ToString());
                if (subject.Length > 77)
                {
                    subject = subject.Substring(0, 77) + "...";
                }

                // Generate the note. It will be saved with the record itself later.
                var n = DebtPlus.LINQ.Factory.Manufacture_vendor_note();
                n.type = (int)DebtPlus.LINQ.InMemory.Notes.NoteTypes.System;
                n.vendor = record.Id;

                n.dont_delete = true;
                n.dont_edit = true;
                n.dont_print = true;

                n.subject = subject;
                n.note = sbNote.ToString();
                bc.vendor_notes.InsertOnSubmit(n);
            }

            // Clear the change list for the next system note.
            record.ClearChangedFieldItems();
        }
    }
}
