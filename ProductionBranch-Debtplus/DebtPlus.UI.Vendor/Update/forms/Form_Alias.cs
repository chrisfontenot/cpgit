#region "Copyright 2000-2012 DebtPlus, L.L.C."

//{*******************************************************************}
//{                                                                   }
//{       DebtPlus Debt Management System                             }
//{                                                                   }
//{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
//{       ALL RIGHTS RESERVED                                         }
//{                                                                   }
//{   The entire contents of this file is protected by U.S. and       }
//{   International Copyright Laws. Unauthorized reproduction,        }
//{   reverse-engineering, and distribution of all or any portion of  }
//{   the code contained in this file is strictly prohibited and may  }
//{   result in severe civil and criminal penalties and will be       }
//{   prosecuted to the maximum extent possible under the law.        }
//{                                                                   }
//{   RESTRICTIONS                                                    }
//{                                                                   }
//{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
//{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
//{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
//{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
//{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
//{                                                                   }
//{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
//{   ADDITIONAL RESTRICTIONS.                                        }
//{                                                                   }
//{*******************************************************************}

#endregion "Copyright 2000-2012 DebtPlus, L.L.C."

using System;

namespace DebtPlus.UI.Vendor.Update.Forms
{
    internal partial class Form_Alias : DebtPlus.Data.Forms.DebtPlusForm
    {
        private DebtPlus.LINQ.vendor_addkey record = null;

        /// <summary>
        /// Initialize the form
        /// </summary>
        public Form_Alias()
            : base()
        {
            InitializeComponent();
        }

        public Form_Alias(DebtPlus.LINQ.vendor_addkey record)
            : base()
        {
            InitializeComponent();
            this.record = record;

            RegisterHandlers();
        }

        private void Button_OK_Click(object sender, EventArgs e)
        {
            record.additional = DebtPlus.Utils.Nulls.v_String(textEdit_Additional.EditValue) ?? string.Empty;
            DialogResult = System.Windows.Forms.DialogResult.OK;
        }

        private void Form_Alias_Load(object sender, EventArgs e)
        {
            UnRegisterHandlers();
            try
            {
                textEdit_Additional.EditValue = record.additional;
                button_OK.Enabled = !HasErrors();
            }
            finally
            {
                RegisterHandlers();
            }
        }

        private void Form_Changed(object sender, EventArgs e)
        {
            button_OK.Enabled = !HasErrors();
        }

        private bool HasErrors()
        {
            if (textEdit_Additional.EditValue == null || textEdit_Additional.Text.Trim() == string.Empty)
            {
                return true;
            }

            return false;
        }

        private void RegisterHandlers()
        {
            Load += Form_Alias_Load;
            textEdit_Additional.EditValueChanged += Form_Changed;
            button_OK.Click += Button_OK_Click;
        }

        private void UnRegisterHandlers()
        {
            Load -= Form_Alias_Load;
            textEdit_Additional.EditValueChanged -= Form_Changed;
            button_OK.Click -= Button_OK_Click;
        }
    }
}