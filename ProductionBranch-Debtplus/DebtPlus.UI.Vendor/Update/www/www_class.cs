#region "Copyright 2000-2012 DebtPlus, L.L.C."

//{*******************************************************************}
//{                                                                   }
//{       DebtPlus Debt Management System                             }
//{                                                                   }
//{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
//{       ALL RIGHTS RESERVED                                         }
//{                                                                   }
//{   The entire contents of this file is protected by U.S. and       }
//{   International Copyright Laws. Unauthorized reproduction,        }
//{   reverse-engineering, and distribution of all or any portion of  }
//{   the code contained in this file is strictly prohibited and may  }
//{   result in severe civil and criminal penalties and will be       }
//{   prosecuted to the maximum extent possible under the law.        }
//{                                                                   }
//{   RESTRICTIONS                                                    }
//{                                                                   }
//{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
//{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
//{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
//{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
//{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
//{                                                                   }
//{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
//{   ADDITIONAL RESTRICTIONS.                                        }
//{                                                                   }
//{*******************************************************************}

#endregion

using System;
using System.Data;
using System.Data.SqlClient;
using System.Windows.Forms;
using DebtPlus.LINQ;
using System.Linq;

namespace DebtPlus.UI.Vendor.Update.WWW
{
    internal partial class www_class : IDisposable
    {
        // Information about the current creditor
        private DebtPlus.LINQ.BusinessContext bc = null;
        private client_www cached_record = null;
        private vendor vendorRecord = null;

        private client_www record
        {
            get
            {
                if (cached_record == null)
                {
                    cached_record = bc.client_wwws.Where(s => s.ApplicationName == "/AttorneyPortal" && s.DatabaseKeyID == vendorRecord.Id).FirstOrDefault();
                }
                return cached_record;
            }
        }

        /// <summary>
        /// Initialize the new class
        /// </summary>
        /// <param name="CreditorLabel"></param>
        public www_class(DebtPlus.LINQ.BusinessContext bc, vendor vendorRecord) : base()
        {
            this.bc = bc;
            this.vendorRecord = vendorRecord;
        }

        /// <summary>
        /// Display the creditor messages list
        /// </summary>
        internal void ShowMessagesForm()
        {
            // The creditor record must exist.
            if (!IsCreated())
            {
                return;
            }

            // Process the notes
            using (var frm = new Form_Messages(bc, record))
            {
                frm.ShowDialog();
            }
        }

        /// <summary>
        /// Returns TRUE if the record has been created and should not be created again.
        /// </summary>
        /// <returns></returns>
        public bool IsCreated()
        {
            return (record != null);
        }

        /// <summary>
        /// Create today new web reference entry to allow the user signon permissions
        /// </summary>
        internal void CreateUser()
        {
            if (IsCreated())
            {
                return;
            }

            // Create today new record
            var newRecord             = DebtPlus.LINQ.Factory.Manufacture_client_www();
            newRecord.ApplicationName = "/AttorneyPortal";
            newRecord.DatabaseKeyID   = vendorRecord.Id;
            string newPassword        = Manufacture_Password();

            using (var frm = new Form_NewUser(bc, newRecord, newPassword))
            {
                if (frm.ShowDialog() != DialogResult.OK)
                {
                    return;
                }

                try
                {
                    bc.client_wwws.InsertOnSubmit(newRecord);
                    bc.SubmitChanges();

                    // Update the cached copy of the record once it was inserted into the database
                    cached_record = newRecord;
                }
                catch (System.Data.SqlClient.SqlException ex)
                {
                    DebtPlus.UI.Common.ErrorHandling.HandleErrors(ex, "Error updating client_www table");
                }
            }
        }

        /// <summary>
        /// Change the password on the existing entry
        /// </summary>
        internal void ChangePassword()
        {
            if (!IsCreated())
            {
                return;
            }

            // Display the request for today new password
            string NewValue = string.Empty;
            if (DebtPlus.Data.Forms.InputBox.Show("What is the new password?", ref NewValue, "Change Password", Manufacture_Password()) != DialogResult.OK || string.IsNullOrEmpty(NewValue))
            {
                return;
            }

            record.Password                               = DebtPlus.LINQ.Factory.MD5(NewValue);
            record.LastPasswordChangeDate                 = System.DateTime.UtcNow;
            record.FailedPasswordAnswerAttemptWindowStart = System.DateTime.UtcNow;
            record.FailedPasswordAnswerAttemptCount       = 0;

            try
            {
                bc.SubmitChanges();
            }
            catch (System.Data.SqlClient.SqlException ex)
            {
                DebtPlus.UI.Common.ErrorHandling.HandleErrors(ex, "Error updating client_www table");
            }
        }

        /// <summary>
        /// Dispose of local storage
        /// </summary>
        protected virtual void Dispose(bool Disposing)
        {
            if (Disposing)
            {
                if (bc != null) bc.Dispose();
            }
            bc = null;
            vendorRecord = null;
        }

        /// <summary>
        /// Dispose of the class
        /// </summary>
        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        /// <summary>
        /// Handle the finalization event for this class
        /// </summary>
        ~www_class()
        {
            Dispose(false);
        }

        /// <summary>
        /// Manufacture a password for the user's account. We use two letters followed by a four digit number
        /// as this is the same format that "host" used. Of course, the user is free to override the password
        /// with any value that they wish to enter.
        /// </summary>
        /// <returns></returns>
        private string Manufacture_Password()
        {
            var rnd = new System.Random((int) (System.DateTime.Now.Ticks % 1000000L));
            var value1 = rnd.Next(26 * 26);
            Int32 ltr2;
            Int32 ltr1 = Math.DivRem(value1, 26, out ltr2);

            var value2 = rnd.Next(10000);
            return (string.Format("{0}{1}{2:0000}", Convert.ToChar(ltr1 + 0x41), Convert.ToChar(ltr2 + 0x41), value2));
        }
    }
}