#region "Copyright 2000-2012 DebtPlus, L.L.C."

//{*******************************************************************}
//{                                                                   }
//{       DebtPlus Debt Management System                             }
//{                                                                   }
//{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
//{       ALL RIGHTS RESERVED                                         }
//{                                                                   }
//{   The entire contents of this file is protected by U.S. and       }
//{   International Copyright Laws. Unauthorized reproduction,        }
//{   reverse-engineering, and distribution of all or any portion of  }
//{   the code contained in this file is strictly prohibited and may  }
//{   result in severe civil and criminal penalties and will be       }
//{   prosecuted to the maximum extent possible under the law.        }
//{                                                                   }
//{   RESTRICTIONS                                                    }
//{                                                                   }
//{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
//{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
//{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
//{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
//{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
//{                                                                   }
//{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
//{   ADDITIONAL RESTRICTIONS.                                        }
//{                                                                   }
//{*******************************************************************}

#endregion

using System;
using System.Data;
using System.Windows.Forms;
using DebtPlus.LINQ;
using System.Linq;
using DevExpress.XtraRichEdit.API.Word;
using DevExpress.XtraRichEdit.Utils;

namespace DebtPlus.UI.Vendor.Update.WWW
{
    internal partial class Form_NewUser : DebtPlus.Data.Forms.DebtPlusForm
    {
        private DebtPlus.LINQ.client_www record = null;
        private DevExpress.XtraEditors.ComboBoxEdit cboEmail;
        private DebtPlus.LINQ.BusinessContext bc = null;
        private string clearPassword = null;
        private DebtPlus.LINQ.client_www duplicateUser = null;

        public Form_NewUser() : base()
        {
            InitializeComponent();
        }

        public Form_NewUser(DebtPlus.LINQ.BusinessContext bc, DebtPlus.LINQ.client_www Record, string ClearPassword) : this()
        {
            this.bc            = bc;
            this.record        = Record;
            this.clearPassword = ClearPassword;
            this.duplicateUser = null;

            RegisterHandlers();
        }

        private void RegisterHandlers()
        {
            Load                          += Form_NewUser_Load;
            Button_OK.Click               += Button_OK_Click;
            textEditUser.Validating       += textEditUser_Validating;
            textEditUser.EditValueChanged += FormChanged;
            cboEmail.EditValueChanged     += FormChanged;
            password.EditValueChanged     += FormChanged;
        }

        private void UnRegisterHandlers()
        {
            Load                          -= Form_NewUser_Load;
            Button_OK.Click               -= Button_OK_Click;
            textEditUser.Validating       -= textEditUser_Validating;
            textEditUser.EditValueChanged -= FormChanged;
            cboEmail.EditValueChanged     -= FormChanged;
            password.EditValueChanged     -= FormChanged;
        }

        #region " Windows Form Designer generated code "

        //Form overrides dispose to clean up the component list.
        protected override void Dispose(bool disposing)
        {
            try
            {
                if (disposing)
                {
                    if (components != null) components.Dispose();
                }
                components = null;
            }
            finally
            {
                base.Dispose(disposing);
            }
        }

        //Required by the Windows Form Designer
        private System.ComponentModel.IContainer components = null;
        
        //NOTE: The following procedure is required by the Windows Form Designer
        //It can be modified using the Windows Form Designer.  
        //Do not modify it using the code editor.
        private DevExpress.XtraEditors.LabelControl LabelControl1;
        private DevExpress.XtraEditors.LabelControl LabelControl2;
        private DevExpress.XtraEditors.LabelControl LabelControl3;
        private DevExpress.XtraEditors.LabelControl LabelControl4;
        private DevExpress.XtraEditors.SimpleButton Button_OK;
        private DevExpress.XtraEditors.SimpleButton Button_Cancel;
        private DevExpress.XtraEditors.TextEdit textEditUser;
        private DevExpress.XtraEditors.TextEdit password;
        [System.Diagnostics.DebuggerStepThrough]
        private void InitializeComponent()
        {
            this.LabelControl1 = new DevExpress.XtraEditors.LabelControl();
            this.LabelControl2 = new DevExpress.XtraEditors.LabelControl();
            this.LabelControl3 = new DevExpress.XtraEditors.LabelControl();
            this.LabelControl4 = new DevExpress.XtraEditors.LabelControl();
            this.Button_OK = new DevExpress.XtraEditors.SimpleButton();
            this.Button_Cancel = new DevExpress.XtraEditors.SimpleButton();
            this.password = new DevExpress.XtraEditors.TextEdit();
            this.textEditUser = new DevExpress.XtraEditors.TextEdit();
            this.cboEmail = new DevExpress.XtraEditors.ComboBoxEdit();
            ((System.ComponentModel.ISupportInitialize)(this.DxErrorProvider1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.password.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEditUser.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cboEmail.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // DefaultLookAndFeel1
            // 
            this.DefaultLookAndFeel1.LookAndFeel.SkinName = "McSkin";
            // 
            // LabelControl1
            // 
            this.LabelControl1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.LabelControl1.Location = new System.Drawing.Point(8, 8);
            this.LabelControl1.Name = "LabelControl1";
            this.LabelControl1.Size = new System.Drawing.Size(316, 26);
            this.LabelControl1.TabIndex = 0;
            this.LabelControl1.Text = "This form will create an access account for the vendor on the web \r\n interface. P" +
    "lease enter the information for the user data";
            this.LabelControl1.ToolTipController = this.ToolTipController1;
            // 
            // LabelControl2
            // 
            this.LabelControl2.Location = new System.Drawing.Point(8, 81);
            this.LabelControl2.Name = "LabelControl2";
            this.LabelControl2.Size = new System.Drawing.Size(46, 13);
            this.LabelControl2.TabIndex = 1;
            this.LabelControl2.Text = "Password";
            this.LabelControl2.ToolTipController = this.ToolTipController1;
            // 
            // LabelControl3
            // 
            this.LabelControl3.Location = new System.Drawing.Point(8, 55);
            this.LabelControl3.Name = "LabelControl3";
            this.LabelControl3.Size = new System.Drawing.Size(48, 13);
            this.LabelControl3.TabIndex = 3;
            this.LabelControl3.Text = "Username";
            this.LabelControl3.ToolTipController = this.ToolTipController1;
            // 
            // LabelControl4
            // 
            this.LabelControl4.Location = new System.Drawing.Point(8, 107);
            this.LabelControl4.Name = "LabelControl4";
            this.LabelControl4.Size = new System.Drawing.Size(102, 13);
            this.LabelControl4.TabIndex = 5;
            this.LabelControl4.Text = "Email(From Contacts)";
            this.LabelControl4.ToolTipController = this.ToolTipController1;
            // 
            // Button_OK
            // 
            this.Button_OK.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.Button_OK.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.Button_OK.Location = new System.Drawing.Point(77, 152);
            this.Button_OK.Name = "Button_OK";
            this.Button_OK.Size = new System.Drawing.Size(75, 23);
            this.Button_OK.TabIndex = 7;
            this.Button_OK.Text = "&OK";
            this.Button_OK.ToolTip = "Click here to create the user\'s access to the system";
            this.Button_OK.ToolTipController = this.ToolTipController1;
            // 
            // Button_Cancel
            // 
            this.Button_Cancel.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.Button_Cancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.Button_Cancel.Location = new System.Drawing.Point(181, 152);
            this.Button_Cancel.Name = "Button_Cancel";
            this.Button_Cancel.Size = new System.Drawing.Size(75, 23);
            this.Button_Cancel.TabIndex = 8;
            this.Button_Cancel.Text = "&Cancel";
            this.Button_Cancel.ToolTip = "Click here to cancel the data entry and return to the previous display";
            this.Button_Cancel.ToolTipController = this.ToolTipController1;
            // 
            // password
            // 
            this.password.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.password.Location = new System.Drawing.Point(116, 78);
            this.password.Name = "password";
            this.password.Size = new System.Drawing.Size(205, 20);
            this.password.TabIndex = 10;
            this.password.ToolTip = "Enter the account password here.";
            this.password.ToolTipController = this.ToolTipController1;
            // 
            // textEditUser
            // 
            this.textEditUser.Location = new System.Drawing.Point(116, 52);
            this.textEditUser.Name = "textEditUser";
            this.textEditUser.Size = new System.Drawing.Size(205, 20);
            this.textEditUser.TabIndex = 9;
            // 
            // cboEmail
            // 
            this.cboEmail.Location = new System.Drawing.Point(116, 104);
            this.cboEmail.Name = "cboEmail";
            this.cboEmail.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cboEmail.Properties.PopupSizeable = true;
            this.cboEmail.Size = new System.Drawing.Size(205, 20);
            this.cboEmail.TabIndex = 11;
            // 
            // Form_NewUser
            // 
            this.AcceptButton = this.Button_OK;
            this.AutoScaleBaseSize = new System.Drawing.Size(5, 14);
            this.CancelButton = this.Button_Cancel;
            this.ClientSize = new System.Drawing.Size(333, 190);
            this.Controls.Add(this.textEditUser);
            this.Controls.Add(this.password);
            this.Controls.Add(this.Button_Cancel);
            this.Controls.Add(this.Button_OK);
            this.Controls.Add(this.LabelControl4);
            this.Controls.Add(this.LabelControl3);
            this.Controls.Add(this.LabelControl2);
            this.Controls.Add(this.LabelControl1);
            this.Controls.Add(this.cboEmail);
            this.MaximizeBox = false;
            this.Name = "Form_NewUser";
            this.Text = "New User";
            ((System.ComponentModel.ISupportInitialize)(this.DxErrorProvider1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.password.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEditUser.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cboEmail.Properties)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private void Form_NewUser_Load(object sender, EventArgs e)
        {
            UnRegisterHandlers();
            try
            {
                // Set the default password field.
                password.Text     = clearPassword;
                textEditUser.Text = record.UserName;

                // Set the properties for the email addresses
                cboEmail.Properties.Items.Clear();
                cboEmail.Properties.Items.AddRange( (from v in bc.vendor_contacts
                                                     join ea in bc.EmailAddresses on v.emailID equals ea.Id
                                                     where v.vendor == record.DatabaseKeyID && v.emailID != null && ea.Address != null && ea.Validation == 1
                                                     orderby ea.Address
                                                     select new DebtPlus.Data.Controls.ComboboxItem(ea.Address, ea.Address, true)
                                                    ).ToArray());

                cboEmail.Text     = record.Email;
                duplicateUser     = null;
                Button_OK.Enabled = !HasErrors();
            }
            finally
            {
                RegisterHandlers();
            }
        }

        private void FormChanged(object Sender, EventArgs e)
        {
            Button_OK.Enabled = !HasErrors();
        }

        /// <summary>
        /// Look for an error condition in the display.
        /// </summary>
        private bool HasErrors()
        {
            // Look at the user name for a match
            if (textEditUser.Text.Trim() == string.Empty)
            {
                DxErrorProvider1.SetError(textEditUser, "Required entry");
                return true;
            }

            if (duplicateUser != null)
            {
                DxErrorProvider1.SetError(textEditUser, "The user name is currently in use by some other vendor");
                return true;
            }

            DxErrorProvider1.SetError(textEditUser, null);

            if (password.Text.Trim() == string.Empty)
            {
                DxErrorProvider1.SetError(password, "Required entry");
                return true;
            }

            // Clear the pending error condition and return "ok to submit data"
            DxErrorProvider1.SetError(password, null);
            return false;
        }

        private void Button_OK_Click(object sender, EventArgs e)
        {
            record.Password = DebtPlus.LINQ.Factory.MD5(password.Text.Trim());
            record.UserName = textEditUser.Text;
            record.Email    = cboEmail.Text.Trim();
        }

        private void textEditUser_Validating(object sender, System.ComponentModel.CancelEventArgs e)
        {
            // Obtain the name of the user as it was entered
            string newUserName = textEditUser.Text.Trim();
            if (newUserName != textEditUser.Text)
            {
                textEditUser.Text = newUserName;
            }

            // The form validation will fail later
            if (newUserName == string.Empty)
            {
                duplicateUser = null;
                Button_OK.Enabled = !HasErrors();
                return;
            }

            // If the person did not change the username then accept the change
            if (string.Compare(record.UserName, newUserName, true) == 0)
            {
                duplicateUser = null;
                Button_OK.Enabled = !HasErrors();
                return;
            }

            // Ensure that the user name is not currently defined for someone else.
            try
            {
                duplicateUser = bc.client_wwws.Where(s => s.ApplicationName == "/AttorneyPortal" && s.UserName == newUserName).FirstOrDefault();
                if (duplicateUser != null)
                {
                    DxErrorProvider1.SetError(textEditUser, "The user name is currently in use by some other vendor");
                    e.Cancel = true;
                }
            }

            catch (System.Data.SqlClient.SqlException ex)
            {
                DxErrorProvider1.SetError(textEditUser, ex.Message);
                e.Cancel = true;
            }
            Button_OK.Enabled = !HasErrors();
        }
    }
}