namespace DebtPlus.UI.Vendor.Update.Controls
{
    internal class ControlBase : DevExpress.XtraEditors.XtraUserControl
    {
        public ControlBase() : base()
        {
            InitializeComponent();
        }

        /// <summary>
        /// Read the initial record at the time of creation
        /// </summary>
        /// <param name="bc">Pointer to the database context</param>
        /// <param name="VendorRecord">Pointer to the current vendor record being edited</param>
        internal virtual void ReadForm(DebtPlus.LINQ.BusinessContext bc, DebtPlus.LINQ.vendor VendorRecord)
        {
        }

        /// <summary>
        /// Refresh the data when the page is selected
        /// </summary>
        /// <param name="VendorRecord">Pointer to the current vendor record being edited</param>
        internal virtual void RefreshForm(DebtPlus.LINQ.BusinessContext bc, DebtPlus.LINQ.vendor VendorRecord)
        {
        }

        /// <summary>
        /// Save the form contents when the page is no longer active
        /// </summary>
        internal virtual void SaveForm(DebtPlus.LINQ.BusinessContext bc)
        {
        }

        #region " Windows Form Designer generated code "

        private System.ComponentModel.IContainer components = null;

        //UserControl overrides dispose to clean up the component list.
        protected override void Dispose(bool disposing)
        {
            try
            {
                if (disposing)
                {
                    if (components != null) components.Dispose();
                }
            }
            finally
            {
                base.Dispose(disposing);
            }
        }

        //Required by the Windows Form Designer
        //NOTE: The following procedure is required by the Windows Form Designer
        //It can be modified using the Windows Form Designer.
        //Do not modify it using the code editor.
        [System.Diagnostics.DebuggerStepThrough]
        private void InitializeComponent()
        {
            //
            //ControlBase
            //
            this.Name = "ControlBase";
            this.Size = new System.Drawing.Size(576, 296);
        }
        #endregion " Windows Form Designer generated code "
    }
}