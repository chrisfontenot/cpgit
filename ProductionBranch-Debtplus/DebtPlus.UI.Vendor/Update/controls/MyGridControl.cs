#region Copyright 2000-2012 DebtPlus, L.L.C.

//{*******************************************************************}
//{                                                                   }
//{       DebtPlus Debt Management System                             }
//{                                                                   }
//{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
//{       ALL RIGHTS RESERVED                                         }
//{                                                                   }
//{   The entire contents of this file is protected by U.S. and       }
//{   International Copyright Laws. Unauthorized reproduction,        }
//{   reverse-engineering, and distribution of all or any portion of  }
//{   the code contained in this file is strictly prohibited and may  }
//{   result in severe civil and criminal penalties and will be       }
//{   prosecuted to the maximum extent possible under the law.        }
//{                                                                   }
//{   RESTRICTIONS                                                    }
//{                                                                   }
//{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
//{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
//{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
//{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
//{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
//{                                                                   }
//{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
//{   ADDITIONAL RESTRICTIONS.                                        }
//{                                                                   }
//{*******************************************************************}

#endregion Copyright 2000-2012 DebtPlus, L.L.C.

using System;
using System.Drawing;
using System.Windows.Forms;
using DevExpress.XtraGrid.Views.Base;
using DevExpress.XtraGrid.Views.Grid.ViewInfo;

namespace DebtPlus.UI.Vendor.Update.Controls
{
    internal partial class MyGridControl : ControlBase
    {
        protected Int32 ControlRow;
        protected bool EnableMenus = false;

        public MyGridControl()
            : base()
        {
            InitializeComponent();
            RegisterHandlers();
        }

        private void RegisterHandlers()
        {
            Load                             += MyGridControl_Load;
            GridView1.MouseDown              += GridView1_MouseDown;
            GridView1.MouseUp                += GridView1_MouseUp;
            GridView1.DoubleClick            += GridView1_DoubleClick;
            GridView1.FocusedRowChanged      += GridView1_FocusedRowChanged;
            GridView1.Layout                 += GridView1_Layout;
            PopupMenu_Items.Popup            += PopupMenu_Items_Popup;
            BarButtonItem_Add.ItemClick      += BarButtonItem_Add_ItemClick;
            BarButtonItem_Change.ItemClick   += BarButtonItem_Change_ItemClick;
            BarButtonItem_Delete.ItemClick   += BarButtonItem_Delete_ItemClick;
            BarButtonItem_Extra2.ItemClick   += BarButtonItem_Extra2_ItemClick;
            BarButtonItem_Reassign.ItemClick += BarButtonItem_Reassign_ItemClick;
        }

        private void UnRegisterHandlers()
        {
            Load                             -= MyGridControl_Load;
            GridView1.MouseDown              -= GridView1_MouseDown;
            GridView1.MouseUp                -= GridView1_MouseUp;
            GridView1.DoubleClick            -= GridView1_DoubleClick;
            GridView1.FocusedRowChanged      -= GridView1_FocusedRowChanged;
            GridView1.Layout                 -= GridView1_Layout;
            PopupMenu_Items.Popup            -= PopupMenu_Items_Popup;
            BarButtonItem_Add.ItemClick      -= BarButtonItem_Add_ItemClick;
            BarButtonItem_Change.ItemClick   -= BarButtonItem_Change_ItemClick;
            BarButtonItem_Delete.ItemClick   -= BarButtonItem_Delete_ItemClick;
            BarButtonItem_Extra2.ItemClick   -= BarButtonItem_Extra2_ItemClick;
            BarButtonItem_Reassign.ItemClick -= BarButtonItem_Reassign_ItemClick;
        }

        #region Windows Form Designer generated code

        //NOTE: The following procedure is required by the Windows Form Designer
        //It can be modified using the Windows Form Designer.
        //Do not modify it using the code editor.

        //Required by the Windows Form Designer
        private System.ComponentModel.IContainer components = null;

        //Form overrides dispose to clean up the component list.
        protected override void Dispose(bool disposing)
        {
            UnRegisterHandlers();
            try
            {
                if (disposing)
                {
                    if (components != null)
                    {
                        components.Dispose();
                    }
                }
            }
            finally
            {
                base.Dispose(disposing);
            }
        }

        [System.Diagnostics.DebuggerStepThrough]
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.GridControl1 = new DevExpress.XtraGrid.GridControl();
            this.GridView1 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.barManager1 = new DevExpress.XtraBars.BarManager(this.components);
            this.barDockControlTop = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlBottom = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlLeft = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlRight = new DevExpress.XtraBars.BarDockControl();
            this.BarButtonItem_Add = new DevExpress.XtraBars.BarButtonItem();
            this.BarButtonItem_Change = new DevExpress.XtraBars.BarButtonItem();
            this.BarButtonItem_Delete = new DevExpress.XtraBars.BarButtonItem();
            this.BarButtonItem_Reassign = new DevExpress.XtraBars.BarButtonItem();
            this.BarButtonItem_Extra2 = new DevExpress.XtraBars.BarButtonItem();
            this.PopupMenu_Items = new DevExpress.XtraBars.PopupMenu(this.components);
            ((System.ComponentModel.ISupportInitialize)(this.GridControl1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.GridView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.PopupMenu_Items)).BeginInit();
            this.SuspendLayout();
            // 
            // GridControl1
            // 
            this.GridControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.GridControl1.Location = new System.Drawing.Point(0, 0);
            this.GridControl1.MainView = this.GridView1;
            this.GridControl1.Name = "GridControl1";
            this.GridControl1.Size = new System.Drawing.Size(576, 296);
            this.GridControl1.TabIndex = 0;
            this.GridControl1.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.GridView1});
            // 
            // GridView1
            // 
            this.GridView1.Appearance.ColumnFilterButton.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(104)))), ((int)(((byte)(184)))), ((int)(((byte)(251)))));
            this.GridView1.Appearance.ColumnFilterButton.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(104)))), ((int)(((byte)(184)))), ((int)(((byte)(251)))));
            this.GridView1.Appearance.ColumnFilterButton.ForeColor = System.Drawing.Color.White;
            this.GridView1.Appearance.ColumnFilterButton.Options.UseBackColor = true;
            this.GridView1.Appearance.ColumnFilterButton.Options.UseBorderColor = true;
            this.GridView1.Appearance.ColumnFilterButton.Options.UseForeColor = true;
            this.GridView1.Appearance.ColumnFilterButtonActive.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(170)))), ((int)(((byte)(216)))), ((int)(((byte)(254)))));
            this.GridView1.Appearance.ColumnFilterButtonActive.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(170)))), ((int)(((byte)(216)))), ((int)(((byte)(254)))));
            this.GridView1.Appearance.ColumnFilterButtonActive.ForeColor = System.Drawing.Color.Black;
            this.GridView1.Appearance.ColumnFilterButtonActive.Options.UseBackColor = true;
            this.GridView1.Appearance.ColumnFilterButtonActive.Options.UseBorderColor = true;
            this.GridView1.Appearance.ColumnFilterButtonActive.Options.UseForeColor = true;
            this.GridView1.Appearance.Empty.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(236)))), ((int)(((byte)(246)))), ((int)(((byte)(255)))));
            this.GridView1.Appearance.Empty.BackColor2 = System.Drawing.Color.White;
            this.GridView1.Appearance.Empty.Options.UseBackColor = true;
            this.GridView1.Appearance.EvenRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(247)))), ((int)(((byte)(251)))), ((int)(((byte)(255)))));
            this.GridView1.Appearance.EvenRow.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(247)))), ((int)(((byte)(251)))), ((int)(((byte)(255)))));
            this.GridView1.Appearance.EvenRow.ForeColor = System.Drawing.Color.Black;
            this.GridView1.Appearance.EvenRow.Options.UseBackColor = true;
            this.GridView1.Appearance.EvenRow.Options.UseBorderColor = true;
            this.GridView1.Appearance.EvenRow.Options.UseForeColor = true;
            this.GridView1.Appearance.FilterCloseButton.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(104)))), ((int)(((byte)(184)))), ((int)(((byte)(251)))));
            this.GridView1.Appearance.FilterCloseButton.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(104)))), ((int)(((byte)(184)))), ((int)(((byte)(251)))));
            this.GridView1.Appearance.FilterCloseButton.ForeColor = System.Drawing.Color.White;
            this.GridView1.Appearance.FilterCloseButton.Options.UseBackColor = true;
            this.GridView1.Appearance.FilterCloseButton.Options.UseBorderColor = true;
            this.GridView1.Appearance.FilterCloseButton.Options.UseForeColor = true;
            this.GridView1.Appearance.FilterPanel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(236)))), ((int)(((byte)(246)))), ((int)(((byte)(255)))));
            this.GridView1.Appearance.FilterPanel.BackColor2 = System.Drawing.Color.White;
            this.GridView1.Appearance.FilterPanel.ForeColor = System.Drawing.Color.Black;
            this.GridView1.Appearance.FilterPanel.Options.UseBackColor = true;
            this.GridView1.Appearance.FilterPanel.Options.UseForeColor = true;
            this.GridView1.Appearance.FixedLine.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(59)))), ((int)(((byte)(133)))), ((int)(((byte)(195)))));
            this.GridView1.Appearance.FixedLine.Options.UseBackColor = true;
            this.GridView1.Appearance.FocusedCell.BackColor = System.Drawing.Color.White;
            this.GridView1.Appearance.FocusedCell.ForeColor = System.Drawing.Color.Black;
            this.GridView1.Appearance.FocusedCell.Options.UseBackColor = true;
            this.GridView1.Appearance.FocusedCell.Options.UseForeColor = true;
            this.GridView1.Appearance.FocusedRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(38)))), ((int)(((byte)(109)))), ((int)(((byte)(189)))));
            this.GridView1.Appearance.FocusedRow.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(59)))), ((int)(((byte)(139)))), ((int)(((byte)(206)))));
            this.GridView1.Appearance.FocusedRow.ForeColor = System.Drawing.Color.White;
            this.GridView1.Appearance.FocusedRow.Options.UseBackColor = true;
            this.GridView1.Appearance.FocusedRow.Options.UseBorderColor = true;
            this.GridView1.Appearance.FocusedRow.Options.UseForeColor = true;
            this.GridView1.Appearance.FooterPanel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(104)))), ((int)(((byte)(184)))), ((int)(((byte)(251)))));
            this.GridView1.Appearance.FooterPanel.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(104)))), ((int)(((byte)(184)))), ((int)(((byte)(251)))));
            this.GridView1.Appearance.FooterPanel.ForeColor = System.Drawing.Color.Black;
            this.GridView1.Appearance.FooterPanel.Options.UseBackColor = true;
            this.GridView1.Appearance.FooterPanel.Options.UseBorderColor = true;
            this.GridView1.Appearance.FooterPanel.Options.UseForeColor = true;
            this.GridView1.Appearance.GroupButton.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(104)))), ((int)(((byte)(184)))), ((int)(((byte)(251)))));
            this.GridView1.Appearance.GroupButton.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(104)))), ((int)(((byte)(184)))), ((int)(((byte)(251)))));
            this.GridView1.Appearance.GroupButton.Options.UseBackColor = true;
            this.GridView1.Appearance.GroupButton.Options.UseBorderColor = true;
            this.GridView1.Appearance.GroupFooter.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(170)))), ((int)(((byte)(216)))), ((int)(((byte)(254)))));
            this.GridView1.Appearance.GroupFooter.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(170)))), ((int)(((byte)(216)))), ((int)(((byte)(254)))));
            this.GridView1.Appearance.GroupFooter.ForeColor = System.Drawing.Color.Black;
            this.GridView1.Appearance.GroupFooter.Options.UseBackColor = true;
            this.GridView1.Appearance.GroupFooter.Options.UseBorderColor = true;
            this.GridView1.Appearance.GroupFooter.Options.UseForeColor = true;
            this.GridView1.Appearance.GroupPanel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(236)))), ((int)(((byte)(246)))), ((int)(((byte)(255)))));
            this.GridView1.Appearance.GroupPanel.BackColor2 = System.Drawing.Color.White;
            this.GridView1.Appearance.GroupPanel.ForeColor = System.Drawing.Color.Black;
            this.GridView1.Appearance.GroupPanel.Options.UseBackColor = true;
            this.GridView1.Appearance.GroupPanel.Options.UseForeColor = true;
            this.GridView1.Appearance.GroupRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(170)))), ((int)(((byte)(216)))), ((int)(((byte)(254)))));
            this.GridView1.Appearance.GroupRow.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(170)))), ((int)(((byte)(216)))), ((int)(((byte)(254)))));
            this.GridView1.Appearance.GroupRow.ForeColor = System.Drawing.Color.Black;
            this.GridView1.Appearance.GroupRow.Options.UseBackColor = true;
            this.GridView1.Appearance.GroupRow.Options.UseBorderColor = true;
            this.GridView1.Appearance.GroupRow.Options.UseForeColor = true;
            this.GridView1.Appearance.HeaderPanel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(139)))), ((int)(((byte)(201)))), ((int)(((byte)(254)))));
            this.GridView1.Appearance.HeaderPanel.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(139)))), ((int)(((byte)(201)))), ((int)(((byte)(254)))));
            this.GridView1.Appearance.HeaderPanel.ForeColor = System.Drawing.Color.Black;
            this.GridView1.Appearance.HeaderPanel.Options.UseBackColor = true;
            this.GridView1.Appearance.HeaderPanel.Options.UseBorderColor = true;
            this.GridView1.Appearance.HeaderPanel.Options.UseForeColor = true;
            this.GridView1.Appearance.HideSelectionRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(105)))), ((int)(((byte)(170)))), ((int)(((byte)(225)))));
            this.GridView1.Appearance.HideSelectionRow.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(83)))), ((int)(((byte)(155)))), ((int)(((byte)(215)))));
            this.GridView1.Appearance.HideSelectionRow.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(236)))), ((int)(((byte)(246)))), ((int)(((byte)(255)))));
            this.GridView1.Appearance.HideSelectionRow.Options.UseBackColor = true;
            this.GridView1.Appearance.HideSelectionRow.Options.UseBorderColor = true;
            this.GridView1.Appearance.HideSelectionRow.Options.UseForeColor = true;
            this.GridView1.Appearance.HorzLine.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(104)))), ((int)(((byte)(184)))), ((int)(((byte)(251)))));
            this.GridView1.Appearance.HorzLine.Options.UseBackColor = true;
            this.GridView1.Appearance.OddRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(236)))), ((int)(((byte)(246)))), ((int)(((byte)(255)))));
            this.GridView1.Appearance.OddRow.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(236)))), ((int)(((byte)(246)))), ((int)(((byte)(255)))));
            this.GridView1.Appearance.OddRow.ForeColor = System.Drawing.Color.Black;
            this.GridView1.Appearance.OddRow.Options.UseBackColor = true;
            this.GridView1.Appearance.OddRow.Options.UseBorderColor = true;
            this.GridView1.Appearance.OddRow.Options.UseForeColor = true;
            this.GridView1.Appearance.Preview.Font = new System.Drawing.Font("Verdana", 7.5F);
            this.GridView1.Appearance.Preview.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(83)))), ((int)(((byte)(155)))), ((int)(((byte)(215)))));
            this.GridView1.Appearance.Preview.Options.UseFont = true;
            this.GridView1.Appearance.Preview.Options.UseForeColor = true;
            this.GridView1.Appearance.Row.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(247)))), ((int)(((byte)(251)))), ((int)(((byte)(255)))));
            this.GridView1.Appearance.Row.ForeColor = System.Drawing.Color.Black;
            this.GridView1.Appearance.Row.Options.UseBackColor = true;
            this.GridView1.Appearance.Row.Options.UseForeColor = true;
            this.GridView1.Appearance.RowSeparator.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(236)))), ((int)(((byte)(246)))), ((int)(((byte)(255)))));
            this.GridView1.Appearance.RowSeparator.BackColor2 = System.Drawing.Color.White;
            this.GridView1.Appearance.RowSeparator.Options.UseBackColor = true;
            this.GridView1.Appearance.SelectedRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(83)))), ((int)(((byte)(155)))), ((int)(((byte)(215)))));
            this.GridView1.Appearance.SelectedRow.ForeColor = System.Drawing.Color.White;
            this.GridView1.Appearance.SelectedRow.Options.UseBackColor = true;
            this.GridView1.Appearance.SelectedRow.Options.UseForeColor = true;
            this.GridView1.Appearance.TopNewRow.BackColor = System.Drawing.Color.White;
            this.GridView1.Appearance.TopNewRow.Options.UseBackColor = true;
            this.GridView1.Appearance.VertLine.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(104)))), ((int)(((byte)(184)))), ((int)(((byte)(251)))));
            this.GridView1.Appearance.VertLine.Options.UseBackColor = true;
            this.GridView1.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus;
            this.GridView1.GridControl = this.GridControl1;
            this.GridView1.Name = "GridView1";
            this.GridView1.OptionsBehavior.AutoPopulateColumns = false;
            this.GridView1.OptionsBehavior.Editable = false;
            this.GridView1.OptionsView.EnableAppearanceEvenRow = true;
            this.GridView1.OptionsView.EnableAppearanceOddRow = true;
            this.GridView1.OptionsView.ShowGroupPanel = false;
            this.GridView1.OptionsView.ShowIndicator = false;
            this.GridView1.ShowButtonMode = DevExpress.XtraGrid.Views.Base.ShowButtonModeEnum.ShowForFocusedRow;
            // 
            // barManager1
            // 
            this.barManager1.DockControls.Add(this.barDockControlTop);
            this.barManager1.DockControls.Add(this.barDockControlBottom);
            this.barManager1.DockControls.Add(this.barDockControlLeft);
            this.barManager1.DockControls.Add(this.barDockControlRight);
            this.barManager1.Form = this;
            this.barManager1.Items.AddRange(new DevExpress.XtraBars.BarItem[] {
            this.BarButtonItem_Add,
            this.BarButtonItem_Change,
            this.BarButtonItem_Delete,
            this.BarButtonItem_Reassign,
            this.BarButtonItem_Extra2});
            this.barManager1.MaxItemId = 5;
            // 
            // barDockControlTop
            // 
            this.barDockControlTop.CausesValidation = false;
            this.barDockControlTop.Dock = System.Windows.Forms.DockStyle.Top;
            this.barDockControlTop.Location = new System.Drawing.Point(0, 0);
            this.barDockControlTop.Size = new System.Drawing.Size(576, 0);
            // 
            // barDockControlBottom
            // 
            this.barDockControlBottom.CausesValidation = false;
            this.barDockControlBottom.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.barDockControlBottom.Location = new System.Drawing.Point(0, 296);
            this.barDockControlBottom.Size = new System.Drawing.Size(576, 0);
            // 
            // barDockControlLeft
            // 
            this.barDockControlLeft.CausesValidation = false;
            this.barDockControlLeft.Dock = System.Windows.Forms.DockStyle.Left;
            this.barDockControlLeft.Location = new System.Drawing.Point(0, 0);
            this.barDockControlLeft.Size = new System.Drawing.Size(0, 296);
            // 
            // barDockControlRight
            // 
            this.barDockControlRight.CausesValidation = false;
            this.barDockControlRight.Dock = System.Windows.Forms.DockStyle.Right;
            this.barDockControlRight.Location = new System.Drawing.Point(576, 0);
            this.barDockControlRight.Size = new System.Drawing.Size(0, 296);
            // 
            // BarButtonItem_Add
            // 
            this.BarButtonItem_Add.Caption = "&Add";
            this.BarButtonItem_Add.Description = "Add an item to the list";
            this.BarButtonItem_Add.Id = 0;
            this.BarButtonItem_Add.Name = "BarButtonItem_Add";
            // 
            // BarButtonItem_Change
            // 
            this.BarButtonItem_Change.Caption = "&Change";
            this.BarButtonItem_Change.Description = "Change the list item";
            this.BarButtonItem_Change.Id = 1;
            this.BarButtonItem_Change.Name = "BarButtonItem_Change";
            // 
            // BarButtonItem_Delete
            // 
            this.BarButtonItem_Delete.Caption = "&Delete";
            this.BarButtonItem_Delete.Description = "Remove the item from the list";
            this.BarButtonItem_Delete.Id = 2;
            this.BarButtonItem_Delete.Name = "BarButtonItem_Delete";
            // 
            // BarButtonItem_Reassign
            // 
            this.BarButtonItem_Reassign.Caption = "&Reassign";
            this.BarButtonItem_Reassign.Description = "Transfer the debt to today new vendor";
            this.BarButtonItem_Reassign.Id = 3;
            this.BarButtonItem_Reassign.Name = "BarButtonItem_Reassign";
            this.BarButtonItem_Reassign.Visibility = DevExpress.XtraBars.BarItemVisibility.Never;
            // 
            // BarButtonItem_Extra2
            // 
            this.BarButtonItem_Extra2.Caption = "&Extra2";
            this.BarButtonItem_Extra2.Description = "Extra Item";
            this.BarButtonItem_Extra2.Id = 4;
            this.BarButtonItem_Extra2.Name = "BarButtonItem_Extra2";
            this.BarButtonItem_Extra2.Visibility = DevExpress.XtraBars.BarItemVisibility.Never;
            // 
            // PopupMenu_Items
            // 
            this.PopupMenu_Items.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.BarButtonItem_Add),
            new DevExpress.XtraBars.LinkPersistInfo(this.BarButtonItem_Change),
            new DevExpress.XtraBars.LinkPersistInfo(this.BarButtonItem_Delete),
            new DevExpress.XtraBars.LinkPersistInfo(this.BarButtonItem_Reassign, true),
            new DevExpress.XtraBars.LinkPersistInfo(this.BarButtonItem_Extra2)});
            this.PopupMenu_Items.Manager = this.barManager1;
            this.PopupMenu_Items.MenuCaption = "Right Click Menus";
            this.PopupMenu_Items.Name = "PopupMenu_Items";
            // 
            // MyGridControl
            // 
            this.Controls.Add(this.GridControl1);
            this.Controls.Add(this.barDockControlLeft);
            this.Controls.Add(this.barDockControlRight);
            this.Controls.Add(this.barDockControlBottom);
            this.Controls.Add(this.barDockControlTop);
            this.Name = "MyGridControl";
            ((System.ComponentModel.ISupportInitialize)(this.GridControl1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.GridView1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.PopupMenu_Items)).EndInit();
            this.ResumeLayout(false);

        }

        protected DevExpress.XtraBars.BarButtonItem BarButtonItem_Add;
        protected DevExpress.XtraBars.BarButtonItem BarButtonItem_Change;
        protected DevExpress.XtraBars.BarButtonItem BarButtonItem_Delete;
        protected DevExpress.XtraBars.BarButtonItem BarButtonItem_Extra2;
        protected DevExpress.XtraBars.BarButtonItem BarButtonItem_Reassign;
        protected DevExpress.XtraBars.BarDockControl barDockControlBottom;
        protected DevExpress.XtraBars.BarDockControl barDockControlLeft;
        protected DevExpress.XtraBars.BarDockControl barDockControlRight;
        protected DevExpress.XtraBars.BarDockControl barDockControlTop;
        protected DevExpress.XtraBars.BarManager barManager1;
        protected DevExpress.XtraBars.PopupMenu PopupMenu_Items;
        protected DevExpress.XtraGrid.GridControl GridControl1;
        protected DevExpress.XtraGrid.Views.Grid.GridView GridView1;

        #endregion Windows Form Designer generated code

        #region Mouse

        private void GridView1_MouseDown(object sender, MouseEventArgs e)
        {
            GridHitInfo hi = GridView1.CalcHitInfo(new Point(e.X, e.Y));
            ControlRow = (hi.IsValid && hi.InRow) ? hi.RowHandle : -1;
        }

        private void GridView1_MouseUp(object sender, MouseEventArgs e)
        {
            if (EnableMenus && e.Button == MouseButtons.Right)
            {
                PopupMenu_Items.ShowPopup(MousePosition);
            }
        }

        #endregion Mouse

        #region Menus

        protected void BarButtonItem_Add_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            OnCreateRecord();
        }

        protected void BarButtonItem_Change_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            if (ControlRow >= 0)
            {
                OnEditRecord(GridView1.GetRow(ControlRow));
            }
        }

        protected void BarButtonItem_Delete_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            if (ControlRow >= 0)
            {
                OnDeleteRecord(GridView1.GetRow(ControlRow));
            }
        }

        protected virtual void BarButtonItem_Extra2_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
        }

        protected virtual void BarButtonItem_Reassign_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
        }

        protected virtual void PopupMenu_Items_Popup(object sender, EventArgs e)
        {
            BarButtonItem_Add.Enabled = true;//OkToCreateRecord();
            BarButtonItem_Delete.Enabled = (ControlRow >= 0) && OkToDeleteRecord(GridView1.GetRow(ControlRow));
            BarButtonItem_Change.Enabled = (ControlRow >= 0) && OkToEditRecord(GridView1.GetRow(ControlRow));
        }

        #endregion Menus

        #region Create

        protected virtual bool OkToCreateRecord()
        {
            return (GridControl1.DataSource != null);
        }

        protected virtual void OnCreateRecord()
        {
        }

        #endregion Create

        #region Edit

        protected virtual bool OkToEditRecord(object obj)
        {
            return (obj != null);
        }

        protected virtual void OnEditRecord(object obj)
        {
        }

        #endregion Edit

        #region Delete

        protected virtual bool OkToDeleteRecord(object obj)
        {
            return (obj != null);
        }

        protected virtual void OnDeleteRecord(object obj)
        {
        }

        #endregion Delete

        #region Select

        protected virtual void OnSelectRecord(object obj)
        {
        }

        #endregion Select

        #region Grid

        protected virtual string XMLBasePath()
        {
            if (ParentForm == null)
            {
                return string.Empty;
            }
            return System.IO.Path.Combine(System.Environment.GetFolderPath(System.Environment.SpecialFolder.LocalApplicationData),
                                          "DebtPlus",
                                          "Vendor.Update",
                                          ParentForm.Name);
        }

        private void GridView1_DoubleClick(object sender, EventArgs e)
        {
            GridHitInfo hi = GridView1.CalcHitInfo((GridControl1.PointToClient(MousePosition)));
            Int32 ControlRow = hi.RowHandle;

            if (hi.IsValid && hi.InRow)
            {
                OnEditRecord(GridView1.GetRow(ControlRow));
            }
        }

        private void GridView1_FocusedRowChanged(object sender, FocusedRowChangedEventArgs e)
        {
            object obj = GridView1.FocusedRowHandle >= 0 ? GridView1.GetRow(GridView1.FocusedRowHandle) : null;
            OnSelectRecord(obj);
        }

        private void GridView1_Layout(object Sender, EventArgs e)
        {
            UnRegisterHandlers();
            try
            {
                string PathName = XMLBasePath();
                if (!string.IsNullOrEmpty(PathName))
                {
                    if (!System.IO.Directory.Exists(PathName))
                    {
                        System.IO.Directory.CreateDirectory(PathName);
                    }

                    string FileName = System.IO.Path.Combine(PathName, Name + ".Grid.xml");
                    GridView1.SaveLayoutToXml(FileName);
                }
            }
            catch { }
            finally
            {
                RegisterHandlers();
            }
        }

        private void MyGridControl_Load(object sender, EventArgs e)
        {
            UnRegisterHandlers();
            try
            {
                string PathName = XMLBasePath();
                if (!string.IsNullOrEmpty(PathName))
                {
                    try
                    {
                        string FileName = System.IO.Path.Combine(PathName, Name + ".Grid.xml");

                        if (System.IO.File.Exists(FileName))
                        {
                            GridView1.RestoreLayoutFromXml(FileName);
                        }
                    }
#pragma warning disable 168
                    catch (System.IO.DirectoryNotFoundException ex) { }
                    catch (System.IO.FileNotFoundException ex) { }
#pragma warning restore 168
                }
            }
            finally
            {
                RegisterHandlers();
            }
        }

        #endregion Grid
    }
}