#region "Copyright 2000-2012 DebtPlus, L.L.C."

//{*******************************************************************}
//{                                                                   }
//{       DebtPlus Debt Management System                             }
//{                                                                   }
//{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
//{       ALL RIGHTS RESERVED                                         }
//{                                                                   }
//{   The entire contents of this file is protected by U.S. and       }
//{   International Copyright Laws. Unauthorized reproduction,        }
//{   reverse-engineering, and distribution of all or any portion of  }
//{   the code contained in this file is strictly prohibited and may  }
//{   result in severe civil and criminal penalties and will be       }
//{   prosecuted to the maximum extent possible under the law.        }
//{                                                                   }
//{   RESTRICTIONS                                                    }
//{                                                                   }
//{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
//{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
//{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
//{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
//{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
//{                                                                   }
//{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
//{   ADDITIONAL RESTRICTIONS.                                        }
//{                                                                   }
//{*******************************************************************}

#endregion "Copyright 2000-2012 DebtPlus, L.L.C."

using System;
using System.Data;
using System.Linq;
using System.Windows.Forms;
using DebtPlus.LINQ;
using System.Data.Linq;
using System.Collections.Generic;

namespace DebtPlus.UI.Vendor.Update.Controls
{
    internal partial class VendorProducts : MyGridControl
    {
        // Current vendor record being edited
        private vendor vendorRecord = null;
        private DebtPlus.LINQ.BusinessContext bc = null;

        public VendorProducts()
            : base()
        {
            InitializeComponent();
            EnableMenus = true;
            RegisterHandlers();
        }

        /// <summary>
        /// Read the form information
        /// </summary>
        internal override void ReadForm(DebtPlus.LINQ.BusinessContext bc, DebtPlus.LINQ.vendor VendorRecord)
        {
            this.vendorRecord = VendorRecord;
            this.bc = bc;

            GridControl1.DataSource = vendorRecord.vendor_products.GetNewBindingList();
            GridView1.RefreshData();
        }

        /// <summary>
        /// Process an item creation
        /// </summary>
        protected override void OnCreateRecord()
        {
            var record = DebtPlus.LINQ.Factory.Manufacture_vendor_product();
            record.vendor1 = vendorRecord;

            using (var frm = new Forms.Form_VendorProduct(bc, record))
            {
                if (frm.ShowDialog() != DialogResult.OK)
                {
                    return;
                }
            }

            vendorRecord.vendor_products.Add(record);
            GridControl1.DataSource = vendorRecord.vendor_products.GetNewBindingList();
            GridView1.RefreshData();
        }

        /// <summary>
        /// Process an item deletion
        /// </summary>
        protected override void OnDeleteRecord(object obj)
        {
            // Find the record to be deleted
            var record = obj as vendor_product;
            if (record == null)
            {
                return;
            }

            // Ask if the deletion is to be performed
            if (DebtPlus.Data.Prompts.RequestConfirmation_Delete() != DialogResult.Yes)
            {
                return;
            }

            vendorRecord.vendor_products.Remove(record);
            GridControl1.DataSource = vendorRecord.vendor_products.GetNewBindingList();
            GridView1.RefreshData();
        }

        /// <summary>
        /// Process an item edit
        /// </summary>
        protected override void OnEditRecord(object obj)
        {
            // Find the record to be deleted
            var record = obj as vendor_product;
            if (record == null)
            {
                return;
            }

            // Do the edit operation
            using (var frm = new Forms.Form_VendorProduct(bc, record))
            {
                if (frm.ShowDialog() != DialogResult.OK)
                {
                    return;
               }
            }

            GridView1.RefreshData();
        }

        private void RegisterHandlers()
        {
            GridView1.CustomColumnDisplayText += GridView1_CustomColumnDisplayText;
        }

        private void UnRegisterHandlers()
        {
            GridView1.CustomColumnDisplayText -= GridView1_CustomColumnDisplayText;
        }

        /// <summary>
        /// Do a custom display to display the product description rather than the code value
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void GridView1_CustomColumnDisplayText(object sender, DevExpress.XtraGrid.Views.Base.CustomColumnDisplayTextEventArgs e)
        {
            // Find the record to be displayed
            var record = GridView1.GetRow(e.RowHandle) as vendor_product;
            if (record == null)
            {
                return;
            }

            // Display the escrow status
            if (e.Column == GridColumn_escrow)
            {
                e.DisplayText = Helpers.GetEscrowProBonoText(record.escrowProBono);
                return;
            }

            // Display the counselor without the domain name
            if (e.Column == GridColumn_created_by)
            {
                e.DisplayText = DebtPlus.Utils.Format.Counselor.FormatCounselor(record.created_by);
                return;
            }
        }

        #region " Windows Form Designer generated code "

        //Required by the Windows Form Designer
        private System.ComponentModel.IContainer components = null;

        private DevExpress.XtraGrid.Columns.GridColumn GridColumn_escrow;
        private DevExpress.XtraGrid.Columns.GridColumn GridColumn_date_created;
        private DevExpress.XtraGrid.Columns.GridColumn GridColumn_effective_date;
        private DevExpress.XtraGrid.Columns.GridColumn GridColumn_Amount;
        private DevExpress.XtraGrid.Columns.GridColumn GridColumn_description;
        private DevExpress.XtraGrid.Columns.GridColumn GridColumn_created_by;
        private DevExpress.XtraGrid.Columns.GridColumn GridColumn_product;

        //Form overrides dispose to clean up the component list.
        protected override void Dispose(bool disposing)
        {
            try
            {
                if (disposing)
                {
                    if (components != null) components.Dispose();
                }
                bc = null;
            }
            finally
            {
                base.Dispose(disposing);
            }
        }

        //NOTE: The following procedure is required by the Windows Form Designer
        //It can be modified using the Windows Form Designer.
        //Do not modify it using the code editor.
        [System.Diagnostics.DebuggerStepThrough()]
        private void InitializeComponent()
        {
            this.GridColumn_created_by = new DevExpress.XtraGrid.Columns.GridColumn();
            this.GridColumn_effective_date = new DevExpress.XtraGrid.Columns.GridColumn();
            this.GridColumn_description = new DevExpress.XtraGrid.Columns.GridColumn();
            this.GridColumn_Amount = new DevExpress.XtraGrid.Columns.GridColumn();
            this.GridColumn_escrow = new DevExpress.XtraGrid.Columns.GridColumn();
            this.GridColumn_date_created = new DevExpress.XtraGrid.Columns.GridColumn();
            this.GridColumn_product = new DevExpress.XtraGrid.Columns.GridColumn();
            ((System.ComponentModel.ISupportInitialize)(this.PopupMenu_Items)).BeginInit();
            this.SuspendLayout();
            // 
            // GridColumn_effective
            // 
            this.GridColumn_effective_date.Caption = "Effective";
            this.GridColumn_effective_date.CustomizationCaption = "Effective Date";
            this.GridColumn_effective_date.DisplayFormat.FormatString = "d";
            this.GridColumn_effective_date.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.GridColumn_effective_date.FieldName = "effective_date";
            this.GridColumn_effective_date.GroupFormat.FormatString = "d";
            this.GridColumn_effective_date.GroupFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.GridColumn_effective_date.Name = "GridColumn_effective_date";
            this.GridColumn_effective_date.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.True;
            this.GridColumn_effective_date.SortMode = DevExpress.XtraGrid.ColumnSortMode.Value;
            this.GridColumn_effective_date.Visible = true;
            this.GridColumn_effective_date.VisibleIndex = 0;
            this.GridColumn_effective_date.Width = 77;
            //
            // GridColumn_description
            //
            this.GridColumn_description.Caption = "Type";
            this.GridColumn_description.CustomizationCaption = "Formatted Type";
            this.GridColumn_description.FieldName = "product1.description";
            this.GridColumn_description.Name = "GridColumn_description";
            this.GridColumn_description.Visible = true;
            this.GridColumn_description.VisibleIndex = 1;
            //
            // GridColumn_product
            //
            this.GridColumn_product.Caption = "Type";
            this.GridColumn_product.CustomizationCaption = "Raw Product Type";
            this.GridColumn_product.FieldName = "product";
            this.GridColumn_product.Name = "GridColumn_product";
            // 
            // GridColumn_Amount
            // 
            this.GridColumn_Amount.AppearanceCell.Options.UseTextOptions = true;
            this.GridColumn_Amount.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.GridColumn_Amount.AppearanceHeader.Options.UseTextOptions = true;
            this.GridColumn_Amount.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.GridColumn_Amount.Caption = "Amount";
            this.GridColumn_Amount.DisplayFormat.FormatString = "c2";
            this.GridColumn_Amount.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.GridColumn_Amount.FieldName = "product1.Amount";
            this.GridColumn_Amount.GroupFormat.FormatString = "c";
            this.GridColumn_Amount.GroupFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.GridColumn_Amount.Name = "GridColumn_Amount";
            this.GridColumn_Amount.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.True;
            this.GridColumn_Amount.Visible = true;
            this.GridColumn_Amount.VisibleIndex = 2;
            this.GridColumn_Amount.Width = 52;
            // 
            // GridColumn_escrow
            // 
            this.GridColumn_escrow.Caption = "Escrow";
            this.GridColumn_escrow.CustomizationCaption = "Is the item an escrow figure?";
            this.GridColumn_escrow.FieldName = "escrow";
            this.GridColumn_escrow.Name = "GridColumn_escrow";
            this.GridColumn_escrow.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.True;
            this.GridColumn_escrow.Visible = true;
            this.GridColumn_escrow.VisibleIndex = 3;
            this.GridColumn_escrow.Width = 51;
            // 
            // GridColumn_date_created
            // 
            this.GridColumn_date_created.Caption = "Created";
            this.GridColumn_date_created.CustomizationCaption = "Date Created";
            this.GridColumn_date_created.DisplayFormat.FormatString = "d";
            this.GridColumn_date_created.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.GridColumn_date_created.FieldName = "GridColumn_date_created";
            this.GridColumn_date_created.GroupFormat.FormatString = "d";
            this.GridColumn_date_created.GroupFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.GridColumn_date_created.Name = "GridColumn_date_created";
            this.GridColumn_date_created.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.True;
            // 
            // GridColumn_created_by
            // 
            this.GridColumn_created_by.Caption = "Creator";
            this.GridColumn_created_by.CustomizationCaption = "Person who created the item";
            this.GridColumn_created_by.FieldName = "created_by";
            this.GridColumn_created_by.Name = "GridColumn_created_by";
            this.GridColumn_created_by.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.True;
            // 
            // VendorProducts
            // 
            this.Name = "VendorProducts";
            this.Size = new System.Drawing.Size(248, 96);
            ((System.ComponentModel.ISupportInitialize)(this.PopupMenu_Items)).EndInit();
            this.ResumeLayout(false);

            this.GridView1.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
				this.GridColumn_created_by ,
                this.GridColumn_effective_date ,
                this.GridColumn_description ,
                this.GridColumn_Amount ,
                this.GridColumn_escrow,
                this.GridColumn_date_created});
        }

        #endregion " Windows Form Designer generated code "
    }
}

namespace DebtPlus.UI.Vendor.Update
{
    public static class Helpers
    {
        public static string GetEscrowProBonoText(System.Nullable<System.Int32> key)
        {
            if (key.HasValue)
            {
                var q = DebtPlus.LINQ.InMemory.EscrowProBonoTypes.getList().Find(s => s.Id == key.Value);
                if (q != null)
                {
                    return q.description;
                }
            }
            return string.Empty;
        }
    }
}
