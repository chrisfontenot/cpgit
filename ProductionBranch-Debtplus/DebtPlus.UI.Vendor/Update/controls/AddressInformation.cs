using System;
using System.ComponentModel;
using System.Linq;
using System.Windows.Forms;
using DebtPlus.LINQ;

namespace DebtPlus.UI.Vendor.Update.Controls
{
    internal partial class AddressInformation : ControlBase
    {
        // Current Vendor being updated
        private vendor vendorRecord = null;

        private BusinessContext bc = null;

        // Current record for the address associated with the control.
        private vendor_address record = null;

        /// <summary>
        /// Initialize the new class
        /// </summary>
        public AddressInformation()
            : base()
        {
            InitializeComponent();
            AddressType = vendor_address.AddressType_General;
            label_Address.Text = string.Empty;
            RegisterHandlers();
        }

        /// <summary>
        /// Type of the address entry
        /// </summary>
        [Description("Set or Get the type of the address block for the vendor"), Category("DebtPlus"), DefaultValue('G')]
        public Char AddressType { get; set; }

        /// <summary>
        /// Caption for the button
        /// </summary>
        [Description("Get or Set the text for the update button"), Category("DebtPlus"), DefaultValue("")]
        public string Caption
        {
            get
            {
                return Button_Update.Text;
            }
            set
            {
                Button_Update.Text = value;
            }
        }

        /// <summary>
        /// Read the address information associated with this type
        /// </summary>
        internal override void ReadForm(BusinessContext bc, vendor VendorRecord)
        {
            vendorRecord = VendorRecord;
            this.bc = bc;

            UnRegisterHandlers();
            try
            {
                // Find the address record in the list of addresses for this vendor
                record = vendorRecord.vendor_addresses.FirstOrDefault(s => s.address_type == AddressType);
                RefreshData(record);
            }
            catch (System.Data.SqlClient.SqlException ex)
            {
                Common.ErrorHandling.HandleErrors(ex, "Error reading vendor addresses");
            }
            finally
            {
                RegisterHandlers();
            }
        }

        /// <summary>
        /// Process the update event on the button click
        /// </summary>
        private void Button_Update_Click(object sender, EventArgs e)
        {
            // Retrieve the current record to be edited
            if (record == null)
            {
                CreateNewRecord();
            }
            else
            {
                EditRecord();
            }
        }

        /// <summary>
        /// Create today new address record
        /// </summary>
        private void CreateNewRecord()
        {
            var addressRecord = Factory.Manufacture_vendor_address();
            addressRecord.vendor = vendorRecord.Id;
            addressRecord.address_type = AddressType;

            using (var frm = new Forms.Form_NewAddress(bc, vendorRecord, addressRecord))
            {
                if (frm.ShowDialog() != DialogResult.OK)
                {
                    return;
                }
            }

            // Insert the new record
            record = addressRecord;
            vendorRecord.vendor_addresses.Add(addressRecord);
            RefreshData(record);
        }

        /// <summary>
        /// Edit the current address record
        /// </summary>
        private void EditRecord()
        {
            // Edit the address record
            using (var frm = new Forms.Form_NewAddress(bc, vendorRecord, record))
            {
                // Look at the response. It determines what we do with the record.
                switch (frm.ShowDialog())
                {
                    case DialogResult.No:   // This is the "Delete" button event.
                        record = null;
                        vendorRecord.vendor_addresses.Remove(record);
                        RefreshData(null);
                        return;

                    case DialogResult.OK:   // This is the "OK" button event.
                        break;

                    default:                // The only other one should be CANCEL.
                        return;
                }
            }

            RefreshData(record);
        }

        /// <summary>
        /// Refresh the address record display
        /// </summary>
        private void RefreshData(vendor_address addressRecord)
        {
            using (var newBc = new BusinessContext())
            {
                label_Address.Text = ReadVendorAddress(newBc, addressRecord);
            }
        }

        /// <summary>
        /// Read the vendor address and return it as a suitable string
        /// </summary>
        /// <returns></returns>
        public static string ReadVendorAddress(BusinessContext bc, vendor_address AddressRecord)
        {
            // If there is no address then there is no string.
            if (AddressRecord == null)
            {
                return string.Empty;
            }

            // Get the address record from the system
            if (AddressRecord.addressID.HasValue)
            {
                var adr = bc.addresses.Where(s => s.Id == AddressRecord.addressID.Value).Select(s => new VendorAddressEntry(s)).FirstOrDefault();
                if (adr != null)
                {
                    adr.attn = AddressRecord.attn;
                    return adr.ToString();
                }
            }

            // If there is no valid address record then just return the attention string.
            return AddressRecord.attn;
        }

        /// <summary>
        /// Add the event handler registrations
        /// </summary>
        private void RegisterHandlers()
        {
            Button_Update.Click += Button_Update_Click;
        }

        /// <summary>
        /// Remove the event handler registrations
        /// </summary>
        private void UnRegisterHandlers()
        {
            Button_Update.Click -= Button_Update_Click;
        }

        //Required by the Windows Form Designer
        private IContainer components = null;

        //Form overrides dispose to clean up the component list.
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                if (components != null)
                {
                    components.Dispose();
                }
            }
            base.Dispose(disposing);
        }

        //NOTE: The following procedure is required by the Windows Form Designer
        //It can be modified using the Windows Form Designer.
        //Do not modify it using the code editor.
        [System.Diagnostics.DebuggerStepThrough]
        private void InitializeComponent()
        {
            Button_Update = new DevExpress.XtraEditors.SimpleButton();
            label_Address = new DevExpress.XtraEditors.MemoEdit();
            ((ISupportInitialize)(label_Address.Properties)).BeginInit();
            SuspendLayout();
            //
            // Button_Update
            //
            Button_Update.AllowFocus = false;
            Button_Update.CausesValidation = false;
            Button_Update.Location = new System.Drawing.Point(0, 0);
            Button_Update.Name = "Button_Update";
            Button_Update.Size = new System.Drawing.Size(75, 23);
            Button_Update.TabIndex = 1;
            Button_Update.TabStop = false;
            Button_Update.Text = "Update...";
            Button_Update.ToolTip = "Click here to update the address information.";
            //
            // label_Address
            //
            label_Address.Location = new System.Drawing.Point(88, 0);
            label_Address.Name = "label_Address";
            label_Address.Properties.AcceptsReturn = false;
            label_Address.Properties.AllowFocused = false;
            label_Address.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.True;
            label_Address.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(255, 255, 192);
            label_Address.Properties.Appearance.Options.UseBackColor = true;
            label_Address.Properties.ReadOnly = true;
            label_Address.Properties.ScrollBars = ScrollBars.None;
            label_Address.Properties.WordWrap = false;
            label_Address.Size = new System.Drawing.Size(450, 56);
            label_Address.TabIndex = 0;
            label_Address.TabStop = false;
            //
            // AddressInformation
            //
            Controls.Add(Button_Update);
            Controls.Add(label_Address);
            Name = "AddressInformation";
            Size = new System.Drawing.Size(550, 56);
            ((ISupportInitialize)(label_Address.Properties)).EndInit();
            ResumeLayout(false);
        }

        private DevExpress.XtraEditors.SimpleButton Button_Update;
        private DevExpress.XtraEditors.MemoEdit label_Address;
    }
}