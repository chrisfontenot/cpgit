#region "Copyright 2000-2012 DebtPlus, L.L.C."

//{*******************************************************************}
//{                                                                   }
//{       DebtPlus Debt Management System                             }
//{                                                                   }
//{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
//{       ALL RIGHTS RESERVED                                         }
//{                                                                   }
//{   The entire contents of this file is protected by U.S. and       }
//{   International Copyright Laws. Unauthorized reproduction,        }
//{   reverse-engineering, and distribution of all or any portion of  }
//{   the code contained in this file is strictly prohibited and may  }
//{   result in severe civil and criminal penalties and will be       }
//{   prosecuted to the maximum extent possible under the law.        }
//{                                                                   }
//{   RESTRICTIONS                                                    }
//{                                                                   }
//{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
//{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
//{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
//{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
//{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
//{                                                                   }
//{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
//{   ADDITIONAL RESTRICTIONS.                                        }
//{                                                                   }
//{*******************************************************************}

#endregion

using System;
using System.Data;
using System.Windows.Forms;
using DebtPlus.Interfaces.Vendor;
using DevExpress.XtraBars;
using DebtPlus.LINQ;

namespace DebtPlus.UI.Vendor.Update
{
    internal partial class ReportsMenuItem : BarButtonItem
    {
        private report reportRecord = null;
        private vendor vendorRecord = null;

        public ReportsMenuItem() : base() { }

        public ReportsMenuItem(vendor VendorRecord, report ReportRecord, string MenuName) : this()
        {
            this.vendorRecord = VendorRecord;
            this.reportRecord = ReportRecord;
            Caption = MenuName;
        }

        protected override void OnClick(BarItemLink link)
        {
            // Save the data tables when the report link is clicked
            base.OnClick(link);

            // Load the report to be printed
            System.Threading.Thread thrd = new System.Threading.Thread(new System.Threading.ParameterizedThreadStart(threadProcedure))
            {
                Name = "Vendor Report",
                IsBackground = false
            };

            thrd.SetApartmentState(System.Threading.ApartmentState.STA);
            thrd.Start(null);
        }

        /// <summary>
        /// Local procedure to process the report thread
        /// </summary>
        /// <param name="obj"></param>
        private void threadProcedure(object obj)
        {
            try
            {
                DebtPlus.Interfaces.Reports.IReports rpt = DebtPlus.Reports.ReportLoader.LoadReport(reportRecord.ClassName);
                if (rpt != null)
                {
                    IVendor paramType = rpt as IVendor;
                    if (paramType != null)
                    {
                        paramType.Vendor = vendorRecord.Id;
                    }
                    else
                    {
                        rpt.SetReportParameter("ParameterVendor", vendorRecord.Id);
                    }

                    if (rpt.RequestReportParameters() == DialogResult.OK)
                    {
                        rpt.RunReportInSeparateThread();
                    }
                }
            }

            catch (Exception ex)
            {
                DebtPlus.UI.Common.ErrorHandling.HandleErrors(ex, "Error running report");
            }
        }
    }
}