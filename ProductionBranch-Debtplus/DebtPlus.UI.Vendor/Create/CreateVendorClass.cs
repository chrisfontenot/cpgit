using System;
using System.Data;
using DebtPlus.Interfaces.Vendor;
using DebtPlus.LINQ;

namespace DebtPlus.UI.Vendor.Create
{
    public partial class CreateVendorClass : DebtPlus.Data.Forms.DebtPlusForm, IVendorCreate
    {
        public CreateVendorClass()
            : base()
        {
            InitializeComponent();
            RegisterHandlers();
        }

        private void RegisterHandlers()
        {
            Load                           += CreateVendorClass_Load;
            Button_OK.Click                += Button_OK_Click;
            vendor_type.EditValueChanged += vendor_type_EditValueChanged;
        }

        private void UnRegisterHandlers()
        {
            Load -= CreateVendorClass_Load;
            Button_OK.Click -= Button_OK_Click;
            vendor_type.EditValueChanged -= vendor_type_EditValueChanged;
        }

        /// <summary>
        /// The id of the newly created vendor
        /// </summary>
        public System.Nullable<System.Int32> Vendor { get; set; }

        private void CreateVendorClass_Load(object sender, EventArgs e)
        {
            UnRegisterHandlers();
            try
            {
                vendor_type.Properties.DataSource   = DebtPlus.LINQ.Cache.vendor_type.getList();
                vendor_type.EditValue               = DebtPlus.LINQ.Cache.vendor_type.getDefault();
                Button_OK.Enabled                   = !HasErrors();
            }
            finally
            {
                RegisterHandlers();
            }
        }

        private void vendor_type_EditValueChanged(object sender, EventArgs e)
        {
            Button_OK.Enabled = !HasErrors();
        }

        private bool HasErrors()
        {
            return string.IsNullOrEmpty(DebtPlus.Utils.Nulls.v_String(vendor_type.EditValue));
        }

        private void Button_OK_Click(object sender, EventArgs e)
        {
            string vendorType = DebtPlus.Utils.Nulls.v_String(vendor_type.EditValue);
            if (string.IsNullOrEmpty(vendorType))
            {
                return;
            }

            try
            {
                // Generate a random number for the label field. It just needs to be as "unique" as possible.
                var rand     = new System.Random((int)(System.DateTime.Now.Ticks % 1000000));
                var randomId = rand.Next(1000000);

                // Allocate a connection object so that we can generate a transaction
                using (var cn = new System.Data.SqlClient.SqlConnection())
                {
                    cn.ConnectionString = DebtPlus.LINQ.SQLInfoClass.getDefault().ConnectionString;
                    cn.Open();

                    // Make a repeatable-read transaction (to hold the locks) so that the updates are performed properly.
                    using (var txn = cn.BeginTransaction(IsolationLevel.RepeatableRead))
                    {
                        using (var bc = new BusinessContext(cn))
                        {
                            bc.Transaction = txn;

                            // Create the blank vendor record
                            var record   = DebtPlus.LINQ.Factory.Manufacture_vendor();
                            record.Type  = vendorType;
                            record.Label = "UK-" + randomId.ToString("0000");

                            // Insert the new record into the database
                            bc.vendors.InsertOnSubmit(record);
                            bc.SubmitChanges();

                            // Change the label field to show the type and the record ID by default
                            record.Label = vendorType + "-" + record.Id.ToString("0000");
                            bc.SubmitChanges();

                            // Commit the changes once we have set the label field correctly.
                            txn.Commit();

                            // The ID of the new record is what we want from the dialog.
                            Vendor = record.Id;
                            DialogResult = System.Windows.Forms.DialogResult.OK;
                            return;
                        }
                    }
                }
            }

            catch (System.Data.SqlClient.SqlException ex)
            {
                DebtPlus.UI.Common.ErrorHandling.HandleErrors(ex);
            }
        }

        #region " Windows Form Designer generated code "

        //Form overrides dispose to clean up the component list.
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                if (components != null)
                {
                    components.Dispose();
                }
            }
            base.Dispose(disposing);
        }

        //Required by the Windows Form Designer
        private System.ComponentModel.IContainer components = null;

        //NOTE: The following procedure is required by the Windows Form Designer
        //It can be modified using the Windows Form Designer.  
        //Do not modify it using the code editor.
        private DevExpress.XtraEditors.LabelControl LabelControl1;
        private DevExpress.XtraEditors.LabelControl LabelControl2;
        private DevExpress.XtraEditors.LabelControl LabelControl3;
        private DevExpress.XtraEditors.LookUpEdit vendor_type;
        private DevExpress.XtraEditors.SimpleButton Button_OK;
        private DevExpress.XtraEditors.SimpleButton Button_Cancel;

        [System.Diagnostics.DebuggerStepThrough]
        private void InitializeComponent()
        {
            this.LabelControl1 = new DevExpress.XtraEditors.LabelControl();
            this.LabelControl2 = new DevExpress.XtraEditors.LabelControl();
            this.LabelControl3 = new DevExpress.XtraEditors.LabelControl();
            this.vendor_type = new DevExpress.XtraEditors.LookUpEdit();
            this.Button_OK = new DevExpress.XtraEditors.SimpleButton();
            this.Button_Cancel = new DevExpress.XtraEditors.SimpleButton();
            ((System.ComponentModel.ISupportInitialize)(this.DxErrorProvider1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.vendor_type.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // DefaultLookAndFeel1
            // 
            this.DefaultLookAndFeel1.LookAndFeel.SkinName = "McSkin";
            // 
            // LabelControl1
            // 
            this.LabelControl1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.LabelControl1.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(128)))));
            this.LabelControl1.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.LabelControl1.Appearance.TextOptions.Trimming = DevExpress.Utils.Trimming.None;
            this.LabelControl1.Appearance.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Top;
            this.LabelControl1.Appearance.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.LabelControl1.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.LabelControl1.Location = new System.Drawing.Point(72, 16);
            this.LabelControl1.Name = "LabelControl1";
            this.LabelControl1.Size = new System.Drawing.Size(228, 74);
            this.LabelControl1.TabIndex = 0;
            this.LabelControl1.Text = "Creating today vendor requires specific permissions. You may not have the require" +
    "d permission so the insert operation may fail. If you chose the New function by " +
    "todayccident then cancel now.";
            this.LabelControl1.ToolTipController = this.ToolTipController1;
            // 
            // LabelControl2
            // 
            this.LabelControl2.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LabelControl2.Location = new System.Drawing.Point(8, 16);
            this.LabelControl2.Name = "LabelControl2";
            this.LabelControl2.Size = new System.Drawing.Size(57, 13);
            this.LabelControl2.TabIndex = 1;
            this.LabelControl2.Text = "WARNING:";
            this.LabelControl2.ToolTipController = this.ToolTipController1;
            // 
            // LabelControl3
            // 
            this.LabelControl3.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.LabelControl3.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.LabelControl3.Appearance.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Top;
            this.LabelControl3.Appearance.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.LabelControl3.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.LabelControl3.Location = new System.Drawing.Point(8, 96);
            this.LabelControl3.Name = "LabelControl3";
            this.LabelControl3.Size = new System.Drawing.Size(296, 50);
            this.LabelControl3.TabIndex = 2;
            this.LabelControl3.Text = "Choose the type of the vendor from the following list. A vendor ID will be automa" +
    "tically created once the type has been choosen.";
            this.LabelControl3.ToolTipController = this.ToolTipController1;
            // 
            // vendor_type
            // 
            this.vendor_type.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.vendor_type.Location = new System.Drawing.Point(8, 152);
            this.vendor_type.Name = "vendor_type";
            this.vendor_type.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.False;
            this.vendor_type.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.vendor_type.Properties.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("Id", "Type", 20, DevExpress.Utils.FormatType.None, "", true, DevExpress.Utils.HorzAlignment.Center),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("description", 90, "Description")});
            this.vendor_type.Properties.DisplayMember = "description";
            this.vendor_type.Properties.NullText = "Select today type from this list...";
            this.vendor_type.Properties.ShowFooter = false;
            this.vendor_type.Properties.ValueMember = "Id";
            this.vendor_type.Size = new System.Drawing.Size(296, 20);
            this.vendor_type.TabIndex = 3;
            this.vendor_type.ToolTip = "Choose the type of the vendor from the list";
            this.vendor_type.ToolTipController = this.ToolTipController1;
            // 
            // Button_OK
            // 
            this.Button_OK.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.Button_OK.Enabled = false;
            this.Button_OK.Location = new System.Drawing.Point(71, 184);
            this.Button_OK.Name = "Button_OK";
            this.Button_OK.Size = new System.Drawing.Size(75, 23);
            this.Button_OK.TabIndex = 4;
            this.Button_OK.Text = "&OK";
            this.Button_OK.ToolTip = "Click here to create the new vendor";
            this.Button_OK.ToolTipController = this.ToolTipController1;
            // 
            // Button_Cancel
            // 
            this.Button_Cancel.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.Button_Cancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.Button_Cancel.Location = new System.Drawing.Point(167, 184);
            this.Button_Cancel.Name = "Button_Cancel";
            this.Button_Cancel.Size = new System.Drawing.Size(75, 23);
            this.Button_Cancel.TabIndex = 5;
            this.Button_Cancel.Text = "&Cancel";
            this.Button_Cancel.ToolTip = "Click here to cancel the creation and return to the previous screen";
            this.Button_Cancel.ToolTipController = this.ToolTipController1;
            // 
            // CreateVendorClass
            // 
            this.AutoScaleBaseSize = new System.Drawing.Size(5, 14);
            this.CancelButton = this.Button_Cancel;
            this.ClientSize = new System.Drawing.Size(312, 222);
            this.Controls.Add(this.Button_Cancel);
            this.Controls.Add(this.Button_OK);
            this.Controls.Add(this.vendor_type);
            this.Controls.Add(this.LabelControl3);
            this.Controls.Add(this.LabelControl1);
            this.Controls.Add(this.LabelControl2);
            this.MaximizeBox = false;
            this.Name = "CreateVendorClass";
            this.Text = "Create New Vendor";
            ((System.ComponentModel.ISupportInitialize)(this.DxErrorProvider1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.vendor_type.Properties)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }
        #endregion
    }
}