﻿#pragma warning disable 168

using System;
using System.Text;
using System.Data;
using System.Data.SqlTypes;
using System.Data.SqlClient;
using Microsoft.SqlServer.Server;

namespace DebtPlus.Server.Housing.Rural
{
    public partial class StoredProcedures
    {
        public static int ProcessUpdateAddresses(string ConnectionString)
        {
            using (var cn = new System.Data.SqlClient.SqlConnection(ConnectionString))
            {
                cn.Open();
                return ProcessRequests(cn);
            }
        }

        private static int ProcessRequests(System.Data.SqlClient.SqlConnection cn)
        {
            using (var ds = new System.Data.DataSet("ds"))
            {
                // Retrieve the list of known states
                using (var cmd = new System.Data.SqlClient.SqlCommand())
                {
                    cmd.Connection = cn;
                    cmd.CommandText = "SELECT [state],[MailingCode],[Name],[Country],[TimeZoneID],[USAFormat],[Default],[ActiveFlag],[AddressFormat],[FIPS],[hud_9902],[DoingBusiness],[NegativeDebtWarning],[created_by],[date_created] FROM [states]";
                    cmd.CommandType = CommandType.Text;
                    using (var da = new System.Data.SqlClient.SqlDataAdapter(cmd))
                    {
                        da.Fill(ds, "states");
                        var tbl = ds.Tables["states"];
                        tbl.PrimaryKey = new System.Data.DataColumn[] { tbl.Columns[0] };
                    }
                }

                // Retrieve the list of addresses to be processed
                using (var cmd = new System.Data.SqlClient.SqlCommand())
                {
                    cmd.Connection = cn;
                    cmd.CommandText = "SELECT";
                    if (global::DebtPlus.Server.Housing.Rural.Properties.Settings.Default.RowCount > 0)
                    {
                        cmd.CommandText += string.Format(" TOP {0:f0}", global::DebtPlus.Server.Housing.Rural.Properties.Settings.Default.RowCount);
                    }

                    cmd.CommandText += " [address],[creditor_prefix_1],[creditor_prefix_2],[house],[direction],[street],[suffix],[modifier],[modifier_value],[address_line_2],[address_line_3],[city],[state],[PostalCode],[USDA_Status] FROM [addresses] WHERE [USDA_Status]='?'";
                    cmd.CommandType = CommandType.Text;
                    using (var da = new System.Data.SqlClient.SqlDataAdapter(cmd))
                    {
                        da.Fill(ds, "addresses");
                        var tbl = ds.Tables["addresses"];
                        tbl.PrimaryKey = new System.Data.DataColumn[] { tbl.Columns[0] };
                        if (tbl.Rows.Count == 0)
                        {
                            return 1;
                        }
                    }
                }

                // Process the entries in the tables
                var NewClass = new Property();
                foreach (System.Data.DataRow row in ds.Tables["addresses"].Rows)
                {
                    var Req = new Property.PropertyRequest(row);
                    var Resp = NewClass.Submit(Req);

                    row.BeginEdit();
                    try
                    {
                        if (Util.IsNullOrWhiteSpace(Resp.ErrorResponse.Class))
                        {
                            if (Resp.Property.Eligibility == "Eligible")
                            {
                                row["USDA_Status"] = 'Y';
                            }
                            else
                            {
                                row["USDA_Status"] = 'N';
                            }
                        }
                        else
                        {
                            row["USDA_Status"] = 'U';
                        }
                    }
                    finally
                    {
                        row.EndEdit();
                    }
                }

                // Perform the update operation on the changed rows
                using (var da = new System.Data.SqlClient.SqlDataAdapter())
                {
                    using (var cmd = new System.Data.SqlClient.SqlCommand())
                    {
                        cmd.Connection = cn;
                        cmd.CommandType = CommandType.Text;
                        cmd.CommandText = @"UPDATE addresses SET [USDA_Status]=@USDA_Status WHERE [address]=@address;";
                        cmd.Parameters.Add("@USDA_Status", SqlDbType.Char, 1, "USDA_Status");
                        cmd.Parameters.Add("@address", SqlDbType.Int, 0, "address");

                        da.UpdateCommand = cmd;
                        da.Update(ds.Tables["addresses"]);
                    }
                }

                return 0;
            }
        }
    }
}
