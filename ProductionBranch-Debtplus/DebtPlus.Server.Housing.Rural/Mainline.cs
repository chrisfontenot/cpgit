﻿// -----------------------------------------------------------------------
// <copyright file="TestingMainline.cs" company="">
// TODO: Update copyright text.
// </copyright>
// -----------------------------------------------------------------------

namespace DebtPlus.Server.Housing.Rural
{
    using System;
    using System.Collections.Generic;
    using System.Text;
    using System.Configuration;
    using System.Data;
    using System.Data.SqlClient;

    /// <summary
    /// TODO: Update summary.
    /// </summary>
    public class Mainline
    {
        [STAThread]
        public static Int32 Main()
        {
            try
            {
                var ConnectionString = ConfigurationManager.ConnectionStrings["DebtPlus"].ConnectionString;
                var cls = new DebtPlus.Server.Housing.Rural.StoredProcedures();
                return DebtPlus.Server.Housing.Rural.StoredProcedures.ProcessUpdateAddresses(ConnectionString);
            }
            catch (Exception ex)
            {
                Console.WriteLine(string.Format("Exception Occurred: {0}", ex.Message));
                return 1;
            }
        }
    }
}
