using DevExpress.XtraEditors.Repository;
using DevExpress.XtraEditors.Registrator;
using System;
using System.Collections.Generic;
using System.Linq;

using DevExpress.LookAndFeel;
using DevExpress.Utils;
using DevExpress.Utils.Drawing;
using DevExpress.Utils.Drawing.Animation;
using DevExpress.XtraEditors;
using DevExpress.XtraEditors.ViewInfo;
using DevExpress.XtraEditors.Controls;
using DevExpress.XtraEditors.Drawing;
using DevExpress.XtraEditors.Mask;
using DevExpress.Skins;

namespace DebtPlus.UI.Client.Widgets.Controls
{
    [UserRepositoryItem("Register")]
    public class RepositoryItemClientId : RepositoryItemButtonEdit
    {
        /// <summary>
        /// Register the control information when the control is initially created the first time.
        /// </summary>
        /// <remarks></remarks>
        static RepositoryItemClientId()
        {
            Register();
        }

        public static void Register()
        {
            EditorRegistrationInfo.Default.Editors.Add(new EditorClassInfo(EditorName, typeof(ClientID), typeof(RepositoryItemClientId), typeof(DevExpress.XtraEditors.ViewInfo.ButtonEditViewInfo), new DevExpress.XtraEditors.Drawing.ButtonEditPainter(), true));
        }

        public RepositoryItemClientId()
        {
        }

        internal const string EditorName = "ClientId";

        /// <summary>
        /// Name for the editor. The specific created items use this string as a base name.
        /// </summary>
        /// <value></value>
        /// <returns></returns>
        /// <remarks></remarks>
        public override string EditorTypeName
        {
            get
            {
                return EditorName;
            }
        }

		public override void CreateDefaultButton()
        {
            Buttons.Add(new DebtPlus.UI.Common.Controls.SearchButton(true));
		}
    }
}
