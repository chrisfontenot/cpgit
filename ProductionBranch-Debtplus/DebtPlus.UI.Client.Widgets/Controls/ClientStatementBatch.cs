using DebtPlus;

using System;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Windows.Forms;
using System.Threading;

namespace DebtPlus.UI.Client.Widgets.Controls
{
    public partial class ClientStatementBatch : DevExpress.XtraEditors.XtraUserControl
    {
        protected DataSet ds = new DataSet("ds");
        public readonly DebtPlus.LINQ.SQLInfoClass SqlInfo = DebtPlus.LINQ.SQLInfoClass.getDefault();

        Int32 _BatchLimit = 50;

        /// <summary>
        /// Allow updating only the notes
        /// </summary>
        public bool NotesOnly { get; set; }

        /// <summary>
        /// Dispose of the local storage
        /// </summary>
        /// <param name="disposing"></param>
        private void ClientStatementBatch_Dispose(bool disposing)
        {
            if (disposing)
            {
                ds.Dispose();
            }
        }

        /// <summary>
        /// Create an instance of the form
        /// </summary>
        public ClientStatementBatch() : base()
        {
            InitializeComponent();
            NotesOnly = false;
            _BatchLimit = 50;

            if (!DesignMode)
            {
                RegisterHandlers();
            }
        }

        /// <summary>
        /// Register the event handlers
        /// </summary>
        private void RegisterHandlers()
        {
            Button_OK.Click += Button_OK_Click;
            Button_Cancel.Click += Button_Cancel_Click;
            GridView1.SelectionChanged += GridView1_SelectionChanged;
            GridView1.RowUpdated += GridView1_RowUpdated;
            GridView1.DoubleClick += GridView1_DoubleClick;
        }

        /// <summary>
        /// Remove the Event Registration
        /// </summary>
        private void UnRegisterHandlers()
        {
            Button_OK.Click -= Button_OK_Click;
            Button_Cancel.Click -= Button_Cancel_Click;
            GridView1.SelectionChanged -= GridView1_SelectionChanged;
            GridView1.RowUpdated -= GridView1_RowUpdated;
            GridView1.DoubleClick -= GridView1_DoubleClick;
        }

        /// <summary>
        /// Number of rows to display in the form for the list of batches
        /// </summary>
        public Int32 BatchLimit
        {
            get
            {
                return _BatchLimit;
            }
            set
            {
                if (value <= 0)
                {
                    throw new ArgumentOutOfRangeException("Invalid BatchLimit value");
                }
                _BatchLimit = value;
            }
        }

        /// <summary>
        /// Small partial class to aid in the proper sorting of the items
        /// </summary>
        private partial class ItemGroup : IComparable
        {
            private DataRow row;
            public ItemGroup(DataRow row)
            {
                this.row = row;
            }

            public Int32 BatchID
            {
                get
                {
                    return Convert.ToInt32(row["client_statement_batch"], System.Globalization.CultureInfo.InvariantCulture);
                }
            }

            public DateTime StatementDate
            {
                get
                {
                    if (row["statement_date"] == System.DBNull.Value)
                    {
                        return DateTime.MinValue;
                    }
                    return Convert.ToDateTime(row["statement_date"]);
                }
            }

            // Needed for the sort funciton to work.
            public Int32 CompareTo(object obj) // Implements System.IComparable.CompareTo
            {
                return StatementDate.CompareTo(((ItemGroup)obj).StatementDate);
            }
        }

        /// <summary>
        /// return the list of selected batches in no specific order
        /// </summary>
        /// <param name="index"></param>
        /// <returns></returns>
        public Int32 SelectedBatch(Int32 index)
        {
            Int32 Answer = -1;
            Int32[] SelectedRows = GridView1.GetSelectedRows();

            if (index <= SelectedRows.GetUpperBound(0))
            {
                // We need to ensure that the items are returned in the proper order. So, sort them.
                System.Collections.Generic.List<ItemGroup> ItemList = new System.Collections.Generic.List<ItemGroup>();
                foreach (Int32 RowHandle in SelectedRows)
                {
                    ItemList.Add(new ItemGroup(GridView1.GetDataRow(RowHandle)));
                }
                ItemList.Sort();

                // Then choose the correct item from the list.
                Answer = ItemList[index].BatchID;
            }

            return Answer;
        }

        /// <summary>
        /// Return the selected batch
        /// </summary>
        /// <returns></returns>
        public Int32 SelectedBatch()
        {
            return SelectedBatch(0);
        }

        private Int32 _BatchCount = 1;
        /// <summary>
        /// Process the batch count
        /// </summary>
        public Int32 BatchCount
        {
            get
            {
                return _BatchCount;
            }
            set
            {
                switch (value)
                {
                    case 1:
                    case 3:
                        _BatchCount = value;
                        break;
                    default:
                        throw new ArgumentOutOfRangeException("Invalid BatchCount value");
                }
            }
        }

        /// <summary>
        /// Event when the batch is selected
        /// </summary>
        public event EventHandler Selected;
        protected void RaiseSelected(EventArgs e)
        {
            var evt = Selected;
            if (evt != null)
            {
                evt(this, e);
            }
        }

        /// <summary>
        /// Handle the OK event
        /// </summary>
        /// <param name="e"></param>
        protected virtual void OnSelected(EventArgs e)
        {
            RaiseSelected(e);
        }

        /// <summary>
        /// Process the OK button
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Button_OK_Click(object sender, EventArgs e) // Handles Button_OK.Click
        {
            SaveChanges();
            RaiseSelected(e);
        }

        public event EventHandler Canceled;
        protected virtual void RaiseCancelled(EventArgs e)
        {
            var evt = Canceled;
            if (evt != null)
            {
                evt(this, e);
            }
        }

        protected virtual void OnCancelled(EventArgs e)
        {
            RaiseCancelled(e);
        }

        /// <summary>
        /// Process the CANCEL button
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Button_Cancel_Click(object sender, EventArgs e) // Handles Button_Cancel.Click
        {
            OnCancelled(e);
        }

        /// <summary>
        /// Load the control list from the database
        /// </summary>
        public virtual void Reload()
        {
            UnRegisterHandlers();
            Cursor current_cursor = Cursor.Current;

            // Enable/Disable the other buttons as needed
            Button_OK.Visible = !NotesOnly;
            try
            {
                Cursor.Current = Cursors.WaitCursor;
                using (var cmd = new SqlCommand())
                {
                    cmd.Connection = new SqlConnection(DebtPlus.LINQ.SQLInfoClass.getDefault().ConnectionString);
                    cmd.CommandText = string.Format(System.Globalization.CultureInfo.InvariantCulture, "SELECT TOP {0:f0} client_statement_batch, type, disbursement_date, note, statement_date, period_start, period_end, quarter_1_batch, quarter_2_batch, quarter_3_batch, date_printed, printed_by, date_created, created_by FROM client_statement_batches WITH (NOLOCK) ORDER BY date_created desc", BatchLimit);
                    cmd.CommandTimeout = System.Math.Min(600, DebtPlus.LINQ.SQLInfoClass.getDefault().CommandTimeout);
                    cmd.CommandType = CommandType.Text;
                    using (var da = new SqlDataAdapter(cmd))
                    {
                        da.Fill(ds, "client_statement_batches");
                    }
                }

                GridControl1.DataSource = ds.Tables["client_statement_batches"].DefaultView;
                GridControl1.RefreshDataSource();
                GridView1.ClearSelection();
            }

            catch (SqlException ex)
            {
                DebtPlus.UI.Common.ErrorHandling.HandleErrors(ex, "Error reading client statement batches table");
            }

            finally
            {
                Cursor.Current = current_cursor;
                RegisterHandlers();
            }

            EnableOK();
        }

        /// <summary>
        /// Enable the OK button if the selected row count is proper
        /// </summary>
        private void EnableOK()
        {
            Button_OK.Enabled = (GridView1.SelectedRowsCount == BatchCount);
        }

        /// <summary>
        /// When the selection items change, enable the OK button
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void GridView1_SelectionChanged(object sender, DevExpress.Data.SelectionChangedEventArgs e) // Handles GridView1.SelectionChanged
        {
            EnableOK();
        }

        /// <summary>
        /// Process an update event on the row
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void GridView1_RowUpdated(object sender, DevExpress.XtraGrid.Views.Base.RowObjectEventArgs e) // Handles GridView1.RowUpdated
        {
            SaveChanges();
        }

        /// <summary>
        /// Process a DOUBLE CLICK event on the row
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void GridView1_DoubleClick(object sender, EventArgs e) // Handles GridView1.DoubleClick
        {
            // Find the row which was clicked
            DevExpress.XtraGrid.Views.Grid.GridView gv = ((DevExpress.XtraGrid.Views.Grid.GridView)sender);
            DevExpress.XtraGrid.GridControl ctl = gv.GridControl;
            DevExpress.XtraGrid.Views.Grid.ViewInfo.GridHitInfo hi = gv.CalcHitInfo((ctl.PointToClient(Control.MousePosition)));
            Int32 RowHandle = hi.RowHandle;

            // if there is a row then change the control
            if (RowHandle >= 0)
            {
                gv.FocusedRowHandle = RowHandle;

                // if there is only one row to be selected then select that row and trip the selected event
                if (BatchCount == 1)
                {
                    GridView1.ClearSelection();
                }

                GridView1.SelectRow(RowHandle);
                EnableOK();
                if (Button_OK.Enabled)
                {
                    SaveChanges();
                    RaiseSelected(e);
                }
            }
        }

        /// <summary>
        /// Hook into the termination event to save any changes first
        /// </summary>
        /// <param name="e"></param>
        protected override void OnLeave(EventArgs e)
        {
            base.OnLeave(e);
            SaveChanges();
        }

        /// <summary>
        /// Save any changes to the table
        /// </summary>
        protected virtual void SaveChanges()
        {
            DataTable tbl = ds.Tables["client_statement_batches"];

            // Determine if we have any rows that should be changed.
            // if not, then don't bother doing the work to find the changes and open the connection object.
            using (var vue = new DataView(tbl, string.Empty, string.Empty, DataViewRowState.ModifiedCurrent))
            {
                if (vue.Count > 0)
                {
                    try
                    {
                        // Build the update command to change the note buffer
                        using (var cm = new DebtPlus.UI.Common.CursorManager())
                        {
                            using (var cmd = new SqlCommand())
                            {
                                cmd.Connection = new SqlConnection(DebtPlus.LINQ.SQLInfoClass.getDefault().ConnectionString);
                                cmd.CommandText = "UPDATE client_statement_batches SET note=@note WHERE client_statement_batch=@client_statement_batch";
                                cmd.Parameters.Add("@note", SqlDbType.VarChar, 1024, "note");
                                cmd.Parameters.Add("@client_statement_batch", SqlDbType.Int, 0, "client_statement_batch");

                                // Construct the update operation for the table rows
                                using (var da = new SqlDataAdapter())
                                {
                                    da.UpdateCommand = cmd;
                                    da.Update(tbl);
                                }
                            }

                            // Update the database with the changes to the tables
                            tbl.AcceptChanges();
                        }
                    }
                    catch (SqlException ex)
                    {
                        DebtPlus.UI.Common.ErrorHandling.HandleErrors(ex, "Error updating client statement note");
                    }
                }
            }
        }
    }
}
