﻿using DebtPlus;

using System;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Windows.Forms;
using System.Threading;

namespace DebtPlus.UI.Client.Widgets.Controls
{
    partial class ClientStatementBatch
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            ClientStatementBatch_Dispose(disposing);
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.Button_Cancel = new DevExpress.XtraEditors.SimpleButton();
            this.Button_OK = new DevExpress.XtraEditors.SimpleButton();
            this.GridControl1 = new DevExpress.XtraGrid.GridControl();
            this.GridView1 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.GridColumn_client_statement_batch = new DevExpress.XtraGrid.Columns.GridColumn();
            this.GridColumn_disbursement_date = new DevExpress.XtraGrid.Columns.GridColumn();
            this.GridColumn_period_start = new DevExpress.XtraGrid.Columns.GridColumn();
            this.GridColumn_period_end = new DevExpress.XtraGrid.Columns.GridColumn();
            this.GridColumn_note = new DevExpress.XtraGrid.Columns.GridColumn();
            this.RepositoryItemMemoExEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit();
            this.RepositoryItemMemoEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemMemoEdit();
            ((System.ComponentModel.ISupportInitialize)(this.GridControl1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.GridView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.RepositoryItemMemoExEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.RepositoryItemMemoEdit1)).BeginInit();
            this.SuspendLayout();
            // 
            // Button_Cancel
            // 
            this.Button_Cancel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.Button_Cancel.Location = new System.Drawing.Point(416, 56);
            this.Button_Cancel.Name = "Button_Cancel";
            this.Button_Cancel.Size = new System.Drawing.Size(75, 23);
            this.Button_Cancel.TabIndex = 5;
            this.Button_Cancel.Text = "&Quit";
            // 
            // Button_OK
            // 
            this.Button_OK.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.Button_OK.Location = new System.Drawing.Point(416, 16);
            this.Button_OK.Name = "Button_OK";
            this.Button_OK.Size = new System.Drawing.Size(75, 23);
            this.Button_OK.TabIndex = 4;
            this.Button_OK.Text = "&OK";
            // 
            // GridControl1
            // 
            this.GridControl1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.GridControl1.Location = new System.Drawing.Point(0, 0);
            this.GridControl1.MainView = this.GridView1;
            this.GridControl1.Name = "GridControl1";
            this.GridControl1.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.RepositoryItemMemoEdit1,
            this.RepositoryItemMemoExEdit1});
            this.GridControl1.Size = new System.Drawing.Size(408, 240);
            this.GridControl1.TabIndex = 3;
            this.GridControl1.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.GridView1});
            // 
            // GridView1
            // 
            this.GridView1.Appearance.ColumnFilterButton.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(104)))), ((int)(((byte)(184)))), ((int)(((byte)(251)))));
            this.GridView1.Appearance.ColumnFilterButton.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(104)))), ((int)(((byte)(184)))), ((int)(((byte)(251)))));
            this.GridView1.Appearance.ColumnFilterButton.ForeColor = System.Drawing.Color.White;
            this.GridView1.Appearance.ColumnFilterButton.Options.UseBackColor = true;
            this.GridView1.Appearance.ColumnFilterButton.Options.UseBorderColor = true;
            this.GridView1.Appearance.ColumnFilterButton.Options.UseForeColor = true;
            this.GridView1.Appearance.ColumnFilterButtonActive.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(170)))), ((int)(((byte)(216)))), ((int)(((byte)(254)))));
            this.GridView1.Appearance.ColumnFilterButtonActive.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(170)))), ((int)(((byte)(216)))), ((int)(((byte)(254)))));
            this.GridView1.Appearance.ColumnFilterButtonActive.ForeColor = System.Drawing.Color.Black;
            this.GridView1.Appearance.ColumnFilterButtonActive.Options.UseBackColor = true;
            this.GridView1.Appearance.ColumnFilterButtonActive.Options.UseBorderColor = true;
            this.GridView1.Appearance.ColumnFilterButtonActive.Options.UseForeColor = true;
            this.GridView1.Appearance.Empty.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(236)))), ((int)(((byte)(246)))), ((int)(((byte)(255)))));
            this.GridView1.Appearance.Empty.BackColor2 = System.Drawing.Color.White;
            this.GridView1.Appearance.Empty.Options.UseBackColor = true;
            this.GridView1.Appearance.EvenRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(247)))), ((int)(((byte)(251)))), ((int)(((byte)(255)))));
            this.GridView1.Appearance.EvenRow.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(247)))), ((int)(((byte)(251)))), ((int)(((byte)(255)))));
            this.GridView1.Appearance.EvenRow.ForeColor = System.Drawing.Color.Black;
            this.GridView1.Appearance.EvenRow.Options.UseBackColor = true;
            this.GridView1.Appearance.EvenRow.Options.UseBorderColor = true;
            this.GridView1.Appearance.EvenRow.Options.UseForeColor = true;
            this.GridView1.Appearance.FilterCloseButton.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(104)))), ((int)(((byte)(184)))), ((int)(((byte)(251)))));
            this.GridView1.Appearance.FilterCloseButton.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(104)))), ((int)(((byte)(184)))), ((int)(((byte)(251)))));
            this.GridView1.Appearance.FilterCloseButton.ForeColor = System.Drawing.Color.White;
            this.GridView1.Appearance.FilterCloseButton.Options.UseBackColor = true;
            this.GridView1.Appearance.FilterCloseButton.Options.UseBorderColor = true;
            this.GridView1.Appearance.FilterCloseButton.Options.UseForeColor = true;
            this.GridView1.Appearance.FilterPanel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(236)))), ((int)(((byte)(246)))), ((int)(((byte)(255)))));
            this.GridView1.Appearance.FilterPanel.BackColor2 = System.Drawing.Color.White;
            this.GridView1.Appearance.FilterPanel.ForeColor = System.Drawing.Color.Black;
            this.GridView1.Appearance.FilterPanel.Options.UseBackColor = true;
            this.GridView1.Appearance.FilterPanel.Options.UseForeColor = true;
            this.GridView1.Appearance.FixedLine.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(59)))), ((int)(((byte)(133)))), ((int)(((byte)(195)))));
            this.GridView1.Appearance.FixedLine.Options.UseBackColor = true;
            this.GridView1.Appearance.FocusedCell.BackColor = System.Drawing.Color.White;
            this.GridView1.Appearance.FocusedCell.ForeColor = System.Drawing.Color.Black;
            this.GridView1.Appearance.FocusedCell.Options.UseBackColor = true;
            this.GridView1.Appearance.FocusedCell.Options.UseForeColor = true;
            this.GridView1.Appearance.FocusedRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(38)))), ((int)(((byte)(109)))), ((int)(((byte)(189)))));
            this.GridView1.Appearance.FocusedRow.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(59)))), ((int)(((byte)(139)))), ((int)(((byte)(206)))));
            this.GridView1.Appearance.FocusedRow.ForeColor = System.Drawing.Color.White;
            this.GridView1.Appearance.FocusedRow.Options.UseBackColor = true;
            this.GridView1.Appearance.FocusedRow.Options.UseBorderColor = true;
            this.GridView1.Appearance.FocusedRow.Options.UseForeColor = true;
            this.GridView1.Appearance.FooterPanel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(104)))), ((int)(((byte)(184)))), ((int)(((byte)(251)))));
            this.GridView1.Appearance.FooterPanel.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(104)))), ((int)(((byte)(184)))), ((int)(((byte)(251)))));
            this.GridView1.Appearance.FooterPanel.ForeColor = System.Drawing.Color.Black;
            this.GridView1.Appearance.FooterPanel.Options.UseBackColor = true;
            this.GridView1.Appearance.FooterPanel.Options.UseBorderColor = true;
            this.GridView1.Appearance.FooterPanel.Options.UseForeColor = true;
            this.GridView1.Appearance.GroupButton.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(104)))), ((int)(((byte)(184)))), ((int)(((byte)(251)))));
            this.GridView1.Appearance.GroupButton.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(104)))), ((int)(((byte)(184)))), ((int)(((byte)(251)))));
            this.GridView1.Appearance.GroupButton.Options.UseBackColor = true;
            this.GridView1.Appearance.GroupButton.Options.UseBorderColor = true;
            this.GridView1.Appearance.GroupFooter.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(170)))), ((int)(((byte)(216)))), ((int)(((byte)(254)))));
            this.GridView1.Appearance.GroupFooter.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(170)))), ((int)(((byte)(216)))), ((int)(((byte)(254)))));
            this.GridView1.Appearance.GroupFooter.ForeColor = System.Drawing.Color.Black;
            this.GridView1.Appearance.GroupFooter.Options.UseBackColor = true;
            this.GridView1.Appearance.GroupFooter.Options.UseBorderColor = true;
            this.GridView1.Appearance.GroupFooter.Options.UseForeColor = true;
            this.GridView1.Appearance.GroupPanel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(236)))), ((int)(((byte)(246)))), ((int)(((byte)(255)))));
            this.GridView1.Appearance.GroupPanel.BackColor2 = System.Drawing.Color.White;
            this.GridView1.Appearance.GroupPanel.ForeColor = System.Drawing.Color.Black;
            this.GridView1.Appearance.GroupPanel.Options.UseBackColor = true;
            this.GridView1.Appearance.GroupPanel.Options.UseForeColor = true;
            this.GridView1.Appearance.GroupRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(170)))), ((int)(((byte)(216)))), ((int)(((byte)(254)))));
            this.GridView1.Appearance.GroupRow.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(170)))), ((int)(((byte)(216)))), ((int)(((byte)(254)))));
            this.GridView1.Appearance.GroupRow.ForeColor = System.Drawing.Color.Black;
            this.GridView1.Appearance.GroupRow.Options.UseBackColor = true;
            this.GridView1.Appearance.GroupRow.Options.UseBorderColor = true;
            this.GridView1.Appearance.GroupRow.Options.UseForeColor = true;
            this.GridView1.Appearance.HeaderPanel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(139)))), ((int)(((byte)(201)))), ((int)(((byte)(254)))));
            this.GridView1.Appearance.HeaderPanel.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(139)))), ((int)(((byte)(201)))), ((int)(((byte)(254)))));
            this.GridView1.Appearance.HeaderPanel.ForeColor = System.Drawing.Color.Black;
            this.GridView1.Appearance.HeaderPanel.Options.UseBackColor = true;
            this.GridView1.Appearance.HeaderPanel.Options.UseBorderColor = true;
            this.GridView1.Appearance.HeaderPanel.Options.UseForeColor = true;
            this.GridView1.Appearance.HideSelectionRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(105)))), ((int)(((byte)(170)))), ((int)(((byte)(225)))));
            this.GridView1.Appearance.HideSelectionRow.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(83)))), ((int)(((byte)(155)))), ((int)(((byte)(215)))));
            this.GridView1.Appearance.HideSelectionRow.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(236)))), ((int)(((byte)(246)))), ((int)(((byte)(255)))));
            this.GridView1.Appearance.HideSelectionRow.Options.UseBackColor = true;
            this.GridView1.Appearance.HideSelectionRow.Options.UseBorderColor = true;
            this.GridView1.Appearance.HideSelectionRow.Options.UseForeColor = true;
            this.GridView1.Appearance.HorzLine.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(104)))), ((int)(((byte)(184)))), ((int)(((byte)(251)))));
            this.GridView1.Appearance.HorzLine.Options.UseBackColor = true;
            this.GridView1.Appearance.OddRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(236)))), ((int)(((byte)(246)))), ((int)(((byte)(255)))));
            this.GridView1.Appearance.OddRow.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(236)))), ((int)(((byte)(246)))), ((int)(((byte)(255)))));
            this.GridView1.Appearance.OddRow.ForeColor = System.Drawing.Color.Black;
            this.GridView1.Appearance.OddRow.Options.UseBackColor = true;
            this.GridView1.Appearance.OddRow.Options.UseBorderColor = true;
            this.GridView1.Appearance.OddRow.Options.UseForeColor = true;
            this.GridView1.Appearance.Preview.Font = new System.Drawing.Font("Verdana", 7.5F);
            this.GridView1.Appearance.Preview.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(83)))), ((int)(((byte)(155)))), ((int)(((byte)(215)))));
            this.GridView1.Appearance.Preview.Options.UseFont = true;
            this.GridView1.Appearance.Preview.Options.UseForeColor = true;
            this.GridView1.Appearance.Row.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(247)))), ((int)(((byte)(251)))), ((int)(((byte)(255)))));
            this.GridView1.Appearance.Row.ForeColor = System.Drawing.Color.Black;
            this.GridView1.Appearance.Row.Options.UseBackColor = true;
            this.GridView1.Appearance.Row.Options.UseForeColor = true;
            this.GridView1.Appearance.RowSeparator.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(236)))), ((int)(((byte)(246)))), ((int)(((byte)(255)))));
            this.GridView1.Appearance.RowSeparator.BackColor2 = System.Drawing.Color.White;
            this.GridView1.Appearance.RowSeparator.Options.UseBackColor = true;
            this.GridView1.Appearance.SelectedRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(83)))), ((int)(((byte)(155)))), ((int)(((byte)(215)))));
            this.GridView1.Appearance.SelectedRow.ForeColor = System.Drawing.Color.White;
            this.GridView1.Appearance.SelectedRow.Options.UseBackColor = true;
            this.GridView1.Appearance.SelectedRow.Options.UseForeColor = true;
            this.GridView1.Appearance.TopNewRow.BackColor = System.Drawing.Color.White;
            this.GridView1.Appearance.TopNewRow.Options.UseBackColor = true;
            this.GridView1.Appearance.VertLine.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(104)))), ((int)(((byte)(184)))), ((int)(((byte)(251)))));
            this.GridView1.Appearance.VertLine.Options.UseBackColor = true;
            this.GridView1.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.GridColumn_client_statement_batch,
            this.GridColumn_disbursement_date,
            this.GridColumn_period_start,
            this.GridColumn_period_end,
            this.GridColumn_note});
            this.GridView1.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus;
            this.GridView1.GridControl = this.GridControl1;
            this.GridView1.Name = "GridView1";
            this.GridView1.OptionsSelection.MultiSelect = true;
            this.GridView1.OptionsView.ShowGroupPanel = false;
            this.GridView1.OptionsView.ShowIndicator = false;
            this.GridView1.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.GridColumn_client_statement_batch, DevExpress.Data.ColumnSortOrder.Descending)});
            // 
            // GridColumn_client_statement_batch
            // 
            this.GridColumn_client_statement_batch.AppearanceCell.Options.UseTextOptions = true;
            this.GridColumn_client_statement_batch.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.GridColumn_client_statement_batch.AppearanceHeader.Options.UseTextOptions = true;
            this.GridColumn_client_statement_batch.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.GridColumn_client_statement_batch.Caption = "Batch";
            this.GridColumn_client_statement_batch.CustomizationCaption = "Batch ID";
            this.GridColumn_client_statement_batch.DisplayFormat.FormatString = "f0";
            this.GridColumn_client_statement_batch.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.GridColumn_client_statement_batch.FieldName = "client_statement_batch";
            this.GridColumn_client_statement_batch.GroupFormat.FormatString = "f0";
            this.GridColumn_client_statement_batch.GroupFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.GridColumn_client_statement_batch.Name = "GridColumn_client_statement_batch";
            this.GridColumn_client_statement_batch.OptionsColumn.AllowEdit = false;
            this.GridColumn_client_statement_batch.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.True;
            this.GridColumn_client_statement_batch.SortMode = DevExpress.XtraGrid.ColumnSortMode.Value;
            this.GridColumn_client_statement_batch.ToolTip = "ID for the client statement batches. System use only.";
            this.GridColumn_client_statement_batch.Visible = true;
            this.GridColumn_client_statement_batch.VisibleIndex = 0;
            this.GridColumn_client_statement_batch.Width = 56;
            // 
            // GridColumn_disbursement_date
            // 
            this.GridColumn_disbursement_date.AppearanceCell.Options.UseTextOptions = true;
            this.GridColumn_disbursement_date.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.GridColumn_disbursement_date.AppearanceHeader.Options.UseTextOptions = true;
            this.GridColumn_disbursement_date.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.GridColumn_disbursement_date.Caption = "Group";
            this.GridColumn_disbursement_date.CustomizationCaption = "Disbursement Cycle Group";
            this.GridColumn_disbursement_date.DisplayFormat.FormatString = "f0";
            this.GridColumn_disbursement_date.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.GridColumn_disbursement_date.FieldName = "disbursement_date";
            this.GridColumn_disbursement_date.GroupFormat.FormatString = "f0";
            this.GridColumn_disbursement_date.GroupFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.GridColumn_disbursement_date.Name = "GridColumn_disbursement_date";
            this.GridColumn_disbursement_date.OptionsColumn.AllowEdit = false;
            this.GridColumn_disbursement_date.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.True;
            this.GridColumn_disbursement_date.ToolTip = "Disbursement cycle group. Only clients with this disbursement cycle are in this g" +
    "roup.";
            this.GridColumn_disbursement_date.Visible = true;
            this.GridColumn_disbursement_date.VisibleIndex = 3;
            this.GridColumn_disbursement_date.Width = 45;
            // 
            // GridColumn_period_start
            // 
            this.GridColumn_period_start.AppearanceCell.Options.UseTextOptions = true;
            this.GridColumn_period_start.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.GridColumn_period_start.AppearanceHeader.Options.UseTextOptions = true;
            this.GridColumn_period_start.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.GridColumn_period_start.Caption = "Starting";
            this.GridColumn_period_start.CustomizationCaption = "Starting Date";
            this.GridColumn_period_start.DisplayFormat.FormatString = "d";
            this.GridColumn_period_start.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.GridColumn_period_start.FieldName = "period_start";
            this.GridColumn_period_start.GroupFormat.FormatString = "d";
            this.GridColumn_period_start.GroupFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.GridColumn_period_start.GroupInterval = DevExpress.XtraGrid.ColumnGroupInterval.Date;
            this.GridColumn_period_start.Name = "GridColumn_period_start";
            this.GridColumn_period_start.OptionsColumn.AllowEdit = false;
            this.GridColumn_period_start.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.True;
            this.GridColumn_period_start.ToolTip = "Period starting date for the transactions";
            this.GridColumn_period_start.Visible = true;
            this.GridColumn_period_start.VisibleIndex = 1;
            this.GridColumn_period_start.Width = 56;
            // 
            // GridColumn_period_end
            // 
            this.GridColumn_period_end.AppearanceCell.Options.UseTextOptions = true;
            this.GridColumn_period_end.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.GridColumn_period_end.AppearanceHeader.Options.UseTextOptions = true;
            this.GridColumn_period_end.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.GridColumn_period_end.Caption = "Ending";
            this.GridColumn_period_end.CustomizationCaption = "Ending Date";
            this.GridColumn_period_end.DisplayFormat.FormatString = "d";
            this.GridColumn_period_end.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.GridColumn_period_end.FieldName = "period_end";
            this.GridColumn_period_end.GroupFormat.FormatString = "d";
            this.GridColumn_period_end.GroupFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.GridColumn_period_end.GroupInterval = DevExpress.XtraGrid.ColumnGroupInterval.Date;
            this.GridColumn_period_end.Name = "GridColumn_period_end";
            this.GridColumn_period_end.OptionsColumn.AllowEdit = false;
            this.GridColumn_period_end.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.True;
            this.GridColumn_period_end.ToolTip = "Period ending date for the transactions";
            this.GridColumn_period_end.Visible = true;
            this.GridColumn_period_end.VisibleIndex = 2;
            this.GridColumn_period_end.Width = 51;
            // 
            // GridColumn_note
            // 
            this.GridColumn_note.Caption = "Note";
            this.GridColumn_note.ColumnEdit = this.RepositoryItemMemoExEdit1;
            this.GridColumn_note.CustomizationCaption = "Statement Note";
            this.GridColumn_note.FieldName = "note";
            this.GridColumn_note.Name = "GridColumn_note";
            this.GridColumn_note.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.True;
            this.GridColumn_note.ToolTip = "Note printed on all statements";
            this.GridColumn_note.Visible = true;
            this.GridColumn_note.VisibleIndex = 4;
            this.GridColumn_note.Width = 196;
            // 
            // RepositoryItemMemoExEdit1
            // 
            this.RepositoryItemMemoExEdit1.AcceptsReturn = false;
            this.RepositoryItemMemoExEdit1.AcceptsTab = false;
            this.RepositoryItemMemoExEdit1.AutoHeight = false;
            this.RepositoryItemMemoExEdit1.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.RepositoryItemMemoExEdit1.MaxLength = 1024;
            this.RepositoryItemMemoExEdit1.Name = "RepositoryItemMemoExEdit1";
            this.RepositoryItemMemoExEdit1.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.RepositoryItemMemoExEdit1.ShowIcon = false;
            // 
            // RepositoryItemMemoEdit1
            // 
            this.RepositoryItemMemoEdit1.AcceptsReturn = false;
            this.RepositoryItemMemoEdit1.MaxLength = 1024;
            this.RepositoryItemMemoEdit1.Name = "RepositoryItemMemoEdit1";
            // 
            // ClientStatementBatch
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.Button_Cancel);
            this.Controls.Add(this.Button_OK);
            this.Controls.Add(this.GridControl1);
            this.Name = "ClientStatementBatch";
            this.Size = new System.Drawing.Size(504, 240);
            ((System.ComponentModel.ISupportInitialize)(this.GridControl1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.GridView1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.RepositoryItemMemoExEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.RepositoryItemMemoEdit1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        protected DevExpress.XtraEditors.SimpleButton Button_Cancel;
        protected DevExpress.XtraEditors.SimpleButton Button_OK;
        protected DevExpress.XtraGrid.GridControl GridControl1;
        protected DevExpress.XtraGrid.Views.Grid.GridView GridView1;
        protected DevExpress.XtraGrid.Columns.GridColumn GridColumn_client_statement_batch;
        protected DevExpress.XtraGrid.Columns.GridColumn GridColumn_disbursement_date;
        protected DevExpress.XtraGrid.Columns.GridColumn GridColumn_period_start;
        protected DevExpress.XtraGrid.Columns.GridColumn GridColumn_period_end;
        protected DevExpress.XtraGrid.Columns.GridColumn GridColumn_note;
        protected DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit RepositoryItemMemoExEdit1;
        protected DevExpress.XtraEditors.Repository.RepositoryItemMemoEdit RepositoryItemMemoEdit1;
    }
}
