#region Copyright 2000-2012 DebtPlus, L.L.C.
//{*******************************************************************}
//{                                                                   }
//{       DebtPlus Debt Management System                             }
//{                                                                   }
//{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
//{       ALL RIGHTS RESERVED                                         }
//{                                                                   }
//{   The entire contents of this file is protected by U.S. and       }
//{   International Copyright Laws. Unauthorized reproduction,        }
//{   reverse-engineering, and distribution of all or any portion of  }
//{   the code contained in this file is strictly prohibited and may  }
//{   result in severe civil and criminal penalties and will be       }
//{   prosecuted to the maximum extent possible under the law.        }
//{                                                                   }
//{   RESTRICTIONS                                                    }
//{                                                                   }
//{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
//{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
//{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
//{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
//{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
//{                                                                   }
//{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
//{   ADDITIONAL RESTRICTIONS.                                        }
//{                                                                   }
//{*******************************************************************}
#endregion

using System;
using System.Data;
using System.Data.SqlClient;
using System.Collections.Generic;
using System.Windows.Forms;
using System.Linq;
using System.ComponentModel;
using DebtPlus.LINQ;

namespace DebtPlus.UI.Client.Widgets.Controls
{
    public partial class ClientID : DevExpress.XtraEditors.ButtonEdit
    {
        private void RegisterHandlers()
        {
        }

        private void UnRegisterHandlers()
        {
        }

        static ClientID()
        {
            RepositoryItemClientId.Register();
        }

        public override string EditorTypeName
        {
            get
            {
                return RepositoryItemClientId.EditorName;
            }
        }

        [DesignerSerializationVisibility(DesignerSerializationVisibility.Content)]
        public new RepositoryItemClientId Properties
        {
            get
            {
                return base.Properties as RepositoryItemClientId;
            }
        }

        public ClientID()
        {
            EditValue = null;
            TabOnSearch = true;

            // This may cause an error for some cases. Don't trap on the error condition.
            try
            {
                AllowDrop = true;
            }
            catch { }
        }

        public event CancelEventHandler SearchCompleted;
        /// <summary>
        /// When search is completed issue this event before doing processing
        /// </summary>
        /// <param name="e"></param>
        /// <remarks></remarks>
        protected virtual void OnSearchCompleted(CancelEventArgs e)
        {
            RaiseSearchCompleted(e);
        }

        protected void RaiseSearchCompleted(CancelEventArgs e)
        {
            CancelEventHandler evt = SearchCompleted;
            if (evt != null)
            {
                evt(this, e);
            }
        }

        protected bool RaiseSearchCompleted(bool CancelState)
        {
            CancelEventArgs e = new CancelEventArgs(CancelState);
            OnSearchCompleted(e);
            return e.Cancel;
        }

        /// <summary>
        /// Validation that we perform on the client ID
        /// </summary>
        /// <remarks></remarks>
        [System.Xml.Serialization.XmlType(Namespace = "urn:ClientId")]
        [System.Xml.Serialization.XmlRoot(Namespace = "urn:ClientId")]
        public enum ValidationLevelEnum
        {
            None = 0,                      // none. anything goes.
            NotZero,                       // client 0 is not allowed. Negative values are invalid too.
            Exists,                        // client must exist. (client 0 is assumed to exist)
            Active,                        // client must be marked active (client 0 is assumed active)
            NotTerminating,                // client must not be EX or I status.
            NotInactive                    // client must not be I status. (client 0 is assumed active)
        }

        /// <summary>
        /// Raised when we change the validation level
        /// </summary>
        /// <remarks></remarks>
        public event EventHandler ValidationLevelChanged;

        /// <summary>
        /// Trip the ValidationLevelChanged event
        /// </summary>
        /// <param name="e"></param>
        protected void RaiseValidationLevelChanged(EventArgs e)
        {
            EventHandler evt = ValidationLevelChanged;
            if (evt != null)
            {
                evt(this, e);
            }
        }

        /// <summary>
        /// Raised when we change the validation level
        /// </summary>
        /// <param name="e"></param>
        /// <remarks></remarks>
        protected virtual void OnValidationLevelChanged(EventArgs e)
        {
            RaiseValidationLevelChanged(e);
        }

        private ValidationLevelEnum _ValidationLevel = ValidationLevelEnum.None;
        /// <summary>
        /// Validation that we perform on the creditor ID
        /// </summary>
        /// <value></value>
        /// <returns></returns>
        /// <remarks></remarks>
        [Description("Types of validation that we perform"), Category("DebtPlus"), Browsable(true), DefaultValue(typeof(ValidationLevelEnum), "None")]
        public ValidationLevelEnum ValidationLevel
        {
            get
            {
                return _ValidationLevel;
            }
            set
            {
                if (value != _ValidationLevel)
                {
                    _ValidationLevel = value;
                    OnValidationLevelChanged(EventArgs.Empty);
                }
            }
        }

        /// <summary>
        /// Should we do a tab operation when search is complete
        /// </summary>
        /// <value></value>
        /// <returns></returns>
        /// <remarks></remarks>
        [Description("Should next control be selected when search is completed"), Category("DebtPlus"), Browsable(true), DefaultValue(typeof(bool), "true")]
        public bool TabOnSearch { get; set; }

        /// <summary>
        /// Our client ID. Int32.MinValue is an error condition
        /// </summary>
        /// <value></value>
        /// <returns></returns>
        /// <remarks></remarks>
        [Description("Decoded edit value for the client ID"), Category("Data"), Browsable(true), DefaultValue(typeof(object), "null"), DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        public new Int32? EditValue
        {
            get
            {
                Int32 IntValue;
                if (!Int32.TryParse(base.Text, System.Globalization.NumberStyles.AllowLeadingWhite | System.Globalization.NumberStyles.AllowTrailingWhite, System.Globalization.CultureInfo.CurrentCulture, out IntValue) || IntValue < 0)
                {
                    return new Int32?();
                }
                return new Int32?(IntValue);
            }

            set
            {
                if (value == null || !value.HasValue || value.Value < 0)
                {
                    base.Text = string.Empty;
                }
                else
                {
                    base.Text = String.Format("{0:0000000}", value.Value);
                }
            }
        }

        #region  drag/drop
        /// <summary>
        /// Handle the mouse movement in the drag/drop condition
        /// </summary>
        /// <param name="e"></param>
        /// <remarks></remarks>
        protected override void OnMouseMove(MouseEventArgs e)
        {
            base.OnMouseMove(e);

            // Do nothing if the operation is not valid
            if ((e.Button == 0) || (e.X >= 0 && e.X < Width && e.Y >= 0 && e.Y < Height))
            {
                return;
            }

            string txt;

            // if this is a text box then look for the selected text component of the text field
            DevExpress.XtraEditors.TextEdit ctl = (DevExpress.XtraEditors.TextEdit)this;
            if (ctl.SelectionLength > 0)
            {
                txt = SelectedText;
            }
            else
            {
                txt = string.Empty;
            }

            // if the text is missing then try the entire text field
            if (txt == string.Empty)
            {
                txt = Text.Trim();
            }

            // if there is no text then do nothing
            if (txt == string.Empty)
            {
                return;
            }

            // Start a drag-drop operation when the mouse moves outside the control
            DataObject dobj = new DataObject();
            dobj.SetData(DataFormats.Text, true, txt);

            // Do the operation. It will return when the operation is complete.
            DragDropEffects effect = DragDropEffects.Copy;
            effect = DoDragDrop(dobj, effect);
        }

        /// <summary>
        /// Process the start of the drag/drop
        /// </summary>
        /// <param name="e"></param>
        /// <remarks></remarks>
        protected override void OnDragEnter(DragEventArgs e)
        {
            base.OnDragEnter(e);

            // if there is a text field then look to determine the accepable processing
            if (e.Data.GetDataPresent(DataFormats.Text, true))
            {
                e.Effect = e.AllowedEffect & DragDropEffects.Copy;
            }
            else
            {
                e.Effect = DragDropEffects.None;
            }
        }

        /// <summary>
        /// Process the drop condition
        /// </summary>
        /// <param name="e"></param>
        /// <remarks></remarks>
        protected override void OnDragDrop(DragEventArgs e)
        {
            base.OnDragDrop(e);

            // if there is a text field then look to determine the accepable processing
            if (!e.Data.GetDataPresent(DataFormats.Text, true))
            {
                return;
            }

            // We only support copy. No modifier needs to be tested
            e.Effect = e.AllowedEffect & DragDropEffects.Copy;

            // Paste the text into the control
            EditValue = Convert.ToInt32(e.Data.GetData(DataFormats.Text, true));
        }

        /// <summary>
        /// Determine if the drag event should be cancelled.
        /// </summary>
        /// <param name="e"></param>
        /// <remarks></remarks>
        protected override void OnQueryContinueDrag(QueryContinueDragEventArgs e)
        {
            // if the escape key is pressed then cancel the operation
            if (e.EscapePressed)
            {
                e.Action = DragAction.Cancel;
                return;
            }

            base.OnQueryContinueDrag(e);
        }
        #endregion

        #region  client search
        /// <summary>
        /// Process the search button click event
        /// </summary>
        /// <param name="buttonInfo"></param>
        /// <remarks></remarks>
        protected override void OnClickButton(DevExpress.XtraEditors.Drawing.EditorButtonObjectInfoArgs buttonInfo)
        {
            string strTag = buttonInfo.Button.Tag as string;
            if (strTag != null && string.Compare(strTag, "search", true, System.Globalization.CultureInfo.InvariantCulture) == 0)
            {
                // Process the search button.
                PerformClientSearch();
            }
            else
            {
                // Pass along the button to the event handler
                base.OnClickButton(buttonInfo);
            }
        }

        /// <summary>
        /// Perform the client search operation
        /// </summary>
        /// <remarks></remarks>
        public virtual void PerformClientSearch()
        {
            using (var search = new DebtPlus.UI.Client.Widgets.Search.ClientSearchClass())
            {
                if (search.ShowDialog() == DialogResult.OK)
                {
                    EditValue = search.ClientId;
                    if (!RaiseSearchCompleted(false) && TabOnSearch)
                    {
                        Parent.SelectNextControl(this, true, true, false, true);
                    }
                }
            }
        }
        #endregion

        /// <summary>
        /// Create the control
        /// </summary>
        /// <remarks></remarks>
        protected override void OnCreateControl()
        {
            base.OnCreateControl();

            Properties.NullText = string.Empty;
            Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.False;
            Properties.ValidateOnEnterKey = true;
            Properties.CharacterCasing = CharacterCasing.Upper;

            // Move the text to the far side of the input
            Properties.Appearance.Options.UseTextOptions = true;
            Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;

            Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Custom;
            Properties.DisplayFormat.Format = new DebtPlus.Utils.Format.Client.CustomFormatter();
            Properties.DisplayFormat.FormatString = "0000000";

            Properties.EditFormat.FormatString = "f0";
            Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric;

            // Numbers only, please.
            Properties.Mask.BeepOnError = true;
            Properties.Mask.EditMask = "\\d*";
            Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.RegEx;
        }

        /// <summary>
        /// Validate the client against the level of testing desired
        /// </summary>
        /// <param name="e"></param>
        /// <remarks></remarks>
        protected override void OnValidating(CancelEventArgs e)
        {
            // Do the base logic first. This updates the edit value.
            base.OnValidating(e);
            if (e.Cancel || EditValue == null || ValidationLevel == ValidationLevelEnum.None)
            {
                return;
            }

            // Clear the error text from the previous call
            ErrorText = string.Empty;
            Int32 newClientId = Convert.ToInt32(EditValue);

            // Process tests for client 0
            if (newClientId == 0 && ValidationLevel == ValidationLevelEnum.NotZero)
            {
                ErrorText = "Client 0 is not valid";
                e.Cancel = true;
                return;
            }

            // Read the active status from the clients table
            using (var bc = new BusinessContext())
            {
                try
                {
                    var result = (from clnt in bc.clients where clnt.Id == newClientId select new { clnt.active_status }).FirstOrDefault();

                    // The client needs to exist first.
                    if (result == null)
                    {
                        ErrorText = "Client does not exist";
                        e.Cancel = true;
                        return;
                    }

                    // Look at the active status
                    switch (result.active_status)
                    {
                        case "A":
                        case "AR":
                            break;  // Client is active. It is acceptable at this point.

                        case "EX":
                            if (ValidationLevel == ValidationLevelEnum.NotTerminating)
                            {
                                ErrorText = "Client is TERMINATING";
                                e.Cancel = true;
                            }
                            break;

                        case "I":
                            if (ValidationLevel == ValidationLevelEnum.NotInactive || ValidationLevel == ValidationLevelEnum.NotTerminating)
                            {
                                ErrorText = "Client is INACTIVE";
                                e.Cancel = true;
                            }
                            break;

                        default:
                            if (ValidationLevel == ValidationLevelEnum.Active)
                            {
                                ErrorText = "Client is NOT ACTIVE";
                                e.Cancel = true;
                            }
                            break;
                    }
                }
                catch (System.Data.SqlClient.SqlException ex)
                {
                    DebtPlus.UI.Common.ErrorHandling.HandleErrors(ex, "Error reading client active status");
                    e.Cancel = true;
                }
            }
        }

        /// <summary>
        /// When the control gets the focus, select all of the text
        /// </summary>
        /// <param name="e"></param>
        /// <remarks></remarks>
        protected override void OnEditorEnter(EventArgs e)
        {
            base.OnEditorEnter(e);
            base.SelectAll();
        }

        protected override void OnGotFocus(EventArgs e)
        {
            base.OnGotFocus(e);
            SelectAll();
        }
    }
}