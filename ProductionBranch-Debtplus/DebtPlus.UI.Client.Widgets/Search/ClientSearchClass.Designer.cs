namespace DebtPlus.UI.Client.Widgets.Search
{
    partial class ClientSearchClass
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            FormDispose(disposing);
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            ((System.ComponentModel.ISupportInitialize)(this.DxErrorProvider1)).BeginInit();
            this.SuspendLayout();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ClientSearchClass));

            this.components = new System.ComponentModel.Container();
            this.Combo_Category = new DevExpress.XtraEditors.ComboBoxEdit();
            this.Combo_Relation = new DevExpress.XtraEditors.ComboBoxEdit();
            this.Check_InactiveClients = new DevExpress.XtraEditors.CheckEdit();
            this.Label1 = new DevExpress.XtraEditors.LabelControl();
            this.Button_OK = new DevExpress.XtraEditors.SimpleButton();
            this.Button_Cancel = new DevExpress.XtraEditors.SimpleButton();
            this.Combo_Value2 = new DevExpress.XtraEditors.TextEdit();
            this.label_and = new DevExpress.XtraEditors.LabelControl();
            this.Combo_Value1 = new DevExpress.XtraEditors.ComboBoxEdit();
            ((System.ComponentModel.ISupportInitialize) this.DxErrorProvider1).BeginInit();
            ((System.ComponentModel.ISupportInitialize) this.Combo_Category.Properties).BeginInit();
            ((System.ComponentModel.ISupportInitialize) this.Combo_Relation.Properties).BeginInit();
            ((System.ComponentModel.ISupportInitialize) this.Check_InactiveClients.Properties).BeginInit();
            ((System.ComponentModel.ISupportInitialize) this.Combo_Value2.Properties).BeginInit();
            ((System.ComponentModel.ISupportInitialize) this.Combo_Value1.Properties).BeginInit();
            this.SuspendLayout();
            //
            //Combo_Category
            //
            this.Combo_Category.Location = new System.Drawing.Point(8, 43);
            this.Combo_Category.Name = "Combo_Category";
            this.Combo_Category.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Style3D;
            this.Combo_Category.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.Combo_Category.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
            this.Combo_Category.Size = new System.Drawing.Size(192, 22);
            this.Combo_Category.TabIndex = 4;
            this.Combo_Category.ToolTip = "Select the major criteria that you wish to perform a search from one of the items in this list.";
            this.Combo_Category.ToolTipController = this.ToolTipController1;
            //
            //Combo_Relation
            //
            this.Combo_Relation.Location = new System.Drawing.Point(206, 43);
            this.Combo_Relation.Name = "Combo_Relation";
            this.Combo_Relation.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Style3D;
            this.Combo_Relation.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.Combo_Relation.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
            this.Combo_Relation.Size = new System.Drawing.Size(128, 22);
            this.Combo_Relation.TabIndex = 5;
            this.Combo_Relation.ToolTip = "Choose how the object is related to the value on the right from one of the items on this list";
            this.Combo_Relation.ToolTipController = this.ToolTipController1;
            //
            //Check_InactiveClients
            //
            this.Check_InactiveClients.EditValue = true;
            this.Check_InactiveClients.Location = new System.Drawing.Point(12, 115);
            this.Check_InactiveClients.Name = "Check_InactiveClients";
            this.Check_InactiveClients.Properties.Appearance.ForeColor = System.Drawing.SystemColors.WindowText;
            this.Check_InactiveClients.Properties.Appearance.Options.UseForeColor = true;
            this.Check_InactiveClients.Properties.Caption = "Ignore INACTIVE clients";
            this.Check_InactiveClients.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Style7;
            this.Check_InactiveClients.Size = new System.Drawing.Size(152, 22);
            this.Check_InactiveClients.TabIndex = 6;
            this.Check_InactiveClients.ToolTip = "Check this box to exclude inactive clients.";
            this.Check_InactiveClients.ToolTipController = this.ToolTipController1;
            //
            //Label1
            //
            this.Label1.Appearance.Font = new System.Drawing.Font("Microsoft Sans Serif", 12.0F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, (byte) 0);
            this.Label1.Appearance.ForeColor = System.Drawing.Color.Red;
            this.Label1.Appearance.Options.UseFont = true;
            this.Label1.Appearance.Options.UseForeColor = true;
            this.Label1.Appearance.Options.UseTextOptions = true;
            this.Label1.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.Label1.Location = new System.Drawing.Point(8, 12);
            this.Label1.Name = "Label1";
            this.Label1.Size = new System.Drawing.Size(108, 20);
            this.Label1.TabIndex = 0;
            this.Label1.Text = "Client Search";
            this.Label1.ToolTipController = this.ToolTipController1;
            this.Label1.UseMnemonic = false;
            //
            //Button_OK
            //
            this.Button_OK.Location = new System.Drawing.Point(277, 112);
            this.Button_OK.Name = "Button_OK";
            this.Button_OK.Size = new System.Drawing.Size(75, 25);
            this.Button_OK.TabIndex = 7;
            this.Button_OK.Text = "&OK";
            this.Button_OK.ToolTipController = this.ToolTipController1;
            //
            //Button_Cancel
            //
            this.Button_Cancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.Button_Cancel.Location = new System.Drawing.Point(384, 112);
            this.Button_Cancel.Name = "Button_Cancel";
            this.Button_Cancel.Size = new System.Drawing.Size(75, 25);
            this.Button_Cancel.TabIndex = 8;
            this.Button_Cancel.Text = "&Cancel";
            this.Button_Cancel.ToolTipController = this.ToolTipController1;
            //
            //Combo_Value2
            //
            this.Combo_Value2.Location = new System.Drawing.Point(344, 80);
            this.Combo_Value2.Name = "Combo_Value2";
            this.Combo_Value2.Size = new System.Drawing.Size(144, 20);
            this.Combo_Value2.TabIndex = 3;
            this.Combo_Value2.ToolTip = "Enter the upper value for the range here.";
            this.Combo_Value2.ToolTipController = this.ToolTipController1;
            //
            //label_and
            //
            this.label_and.Appearance.Options.UseTextOptions = true;
            this.label_and.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.label_and.Location = new System.Drawing.Point(236, 87);
            this.label_and.Name = "label_and";
            this.label_and.Size = new System.Drawing.Size(18, 13);
            this.label_and.TabIndex = 2;
            this.label_and.Text = "And";
            this.label_and.ToolTipController = this.ToolTipController1;
            this.label_and.UseMnemonic = false;
            //
            //Combo_Value1
            //
            this.Combo_Value1.Location = new System.Drawing.Point(344, 43);
            this.Combo_Value1.Name = "Combo_Value1";
            this.Combo_Value1.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.True;
            this.Combo_Value1.Properties.AutoComplete = false;
            this.Combo_Value1.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Style3D;
            this.Combo_Value1.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.Combo_Value1.Size = new System.Drawing.Size(144, 22);
            this.Combo_Value1.TabIndex = 1;
            this.Combo_Value1.ToolTip = "Enter or choose the search criteria for the Client. if( \"Between\" is used, this is the lower limit.";
            this.Combo_Value1.ToolTipController = this.ToolTipController1;
            //
            //ClientSearchClass
            //
            this.AcceptButton = this.Button_OK;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6.0F, 13.0F);
            this.CancelButton = this.Button_Cancel;
            this.ClientSize = new System.Drawing.Size(498, 151);
            this.Controls.Add(this.Combo_Value1);
            this.Controls.Add(this.label_and);
            this.Controls.Add(this.Combo_Value2);
            this.Controls.Add(this.Button_Cancel);
            this.Controls.Add(this.Button_OK);
            this.Controls.Add(this.Label1);
            this.Controls.Add(this.Check_InactiveClients);
            this.Controls.Add(this.Combo_Relation);
            this.Controls.Add(this.Combo_Category);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.Name = "ClientSearchClass";
            this.Text = "Client Search";
            ((System.ComponentModel.ISupportInitialize) this.DxErrorProvider1).EndInit();
            ((System.ComponentModel.ISupportInitialize) this.Combo_Category.Properties).EndInit();
            ((System.ComponentModel.ISupportInitialize) this.Combo_Relation.Properties).EndInit();
            ((System.ComponentModel.ISupportInitialize) this.Check_InactiveClients.Properties).EndInit();
            ((System.ComponentModel.ISupportInitialize) this.Combo_Value2.Properties).EndInit();
            ((System.ComponentModel.ISupportInitialize) this.Combo_Value1.Properties).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

            ((System.ComponentModel.ISupportInitialize)(this.DxErrorProvider1)).EndInit();
            this.ResumeLayout(false);
        }
        internal DevExpress.XtraEditors.ComboBoxEdit Combo_Category;
        internal DevExpress.XtraEditors.CheckEdit Check_InactiveClients;
        internal DevExpress.XtraEditors.ComboBoxEdit Combo_Relation;
        internal DevExpress.XtraEditors.LabelControl Label1;
        internal DevExpress.XtraEditors.SimpleButton Button_OK;
        internal DevExpress.XtraEditors.SimpleButton Button_Cancel;
        internal DevExpress.XtraEditors.TextEdit Combo_Value2;
        internal DevExpress.XtraEditors.LabelControl label_and;
        internal DevExpress.XtraEditors.ComboBoxEdit Combo_Value1;
#endregion
    }
}

