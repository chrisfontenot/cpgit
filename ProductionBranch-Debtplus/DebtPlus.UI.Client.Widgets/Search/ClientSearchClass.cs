using System;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using DebtPlus.LINQ;

namespace DebtPlus.UI.Client.Widgets.Search
{
    public partial class ClientSearchClass : DebtPlus.Data.Forms.DebtPlusForm, DebtPlus.Interfaces.Client.IClientSearch
    {
        // Information about the Client search
        private ClientSearchSelectForm formClientList;      // This is our form listing the results of the search.

        protected Int32 lastClient;                 // Last Client in the selection list. Used for the "MORE" function as a starting point.
        protected string selectClause;              // Selection clause. Used to build the SQL statement
        private DataSet ds = new DataSet("ds");     // Dataset to hold the items

        // Values used for the categories (1st input field)
        private const string category_birthdate = "birthdate";

        private const string category_client = "Client";
        private const string category_last_name = "last name";
        private const string category_alias = "alias";
        private const string category_ssn = "ssn";
        private const string category_account_number = "account number";
        private const string category_deposit_amt = "deposit amt";
        private const string category_deposit_ref = "deposit ref";
        private const string category_email = "email";
        private const string category_emp_address = "emp address";
        private const string category_emp_city = "emp city";
        private const string category_emp_name = "emp name";
        private const string category_home_address = "home address";
        private const string category_home_city = "home city";
        private const string category_personal_phone = "personal phone";
        private const string category_work_phone = "work phone";

        // Values used for the relation (2nd input field)
        private const string relation_EQ = "=";

        private const string relation_GE = ">=";
        private const string relation_GT = ">";
        private const string relation_LT = "<";
        private const string relation_LE = "<=";
        private const string relation_NE = "not =";
        private const string relation_between = "between";
        private const string relation_not_between = "not between";
        private const string relation_contains = "contains";
        private const string relation_not_contains = "not contain";
        private const string relation_ends = "ends";
        private const string relation_starts = "starts";
        private const string relation_not_starts = "not starts";

        // Keys in the MRU list
        private const string MRU_client = "Clients";

        private const string MRU_birthdate = "Client Birthdate";
        private const string MRU_last_name = "Client Last Name";
        private const string MRU_alias = "Client Alias";
        private const string MRU_ssn = "Client SSN";
        private const string MRU_account_number = "Client Account Number";
        private const string MRU_deposit_amt = "Client Deposit";
        private const string MRU_deposit_ref = "Client Deposit Ref";
        private const string MRU_email = "Client Email";
        private const string MRU_emp_address = "Client Emp Address";
        private const string MRU_emp_city = "Client Emp City";
        private const string MRU_emp_name = "Client Emp Name";
        private const string MRU_home_address = "Client Home Address";
        private const string MRU_home_city = "Client Home City";
        private const string MRU_personal_phone = "Client Personal Phone";
        private const string MRU_work_phone = "Client Work Phone";

        /// <summary>
        /// Create an instance of our new class
        /// </summary>
        /// <param name="DatabaseInfo"></param>
        public ClientSearchClass()
            : base()
        {
            InitializeComponent();
            ClientId = Int32.MinValue;
            RegisterHandlers();
        }

        /// <summary>
        /// Register the event handlers
        /// </summary>
        private void RegisterHandlers()
        {
            this.Load                            += ClientSearchClass_Load;
            Combo_Category.SelectedIndexChanged  += Combo_Category_SelectedIndexChanged;
            Combo_Category.SelectedValueChanged  += Combo_Category_SelectedValueChanged;
            Combo_Category.TextChanged           += Combo_Category_ValueChanged;
            Combo_Relation.SelectedIndexChanged  += Combo_Relation_SelectedIndexChanged;
            Combo_Value1.Enter                   += Combo_Value1_Enter;
            Combo_Value1.Leave                   += Combo_Value1_Leave;
            Combo_Value1.TextChanged             += Form_Changed;
            Combo_Value1.SelectedIndexChanged    += Form_Changed;
            Combo_Value2.TextChanged             += Form_Changed;
            Button_OK.Click                      += Button_OK_Click;
            Check_InactiveClients.CheckedChanged += Check_InactiveClients_CheckedChanged;
        }

        /// <summary>
        /// Remove the registration for the events
        /// </summary>
        private void UnRegisterHandlers()
        {
            this.Load                            -= new EventHandler(ClientSearchClass_Load);
            Combo_Category.SelectedIndexChanged  -= new EventHandler(Combo_Category_SelectedIndexChanged);
            Combo_Category.SelectedValueChanged  -= new EventHandler(Combo_Category_SelectedValueChanged);
            Combo_Category.TextChanged           -= new EventHandler(Combo_Category_ValueChanged);
            Combo_Relation.SelectedIndexChanged  -= new EventHandler(Combo_Relation_SelectedIndexChanged);
            Combo_Value1.Enter                   -= new EventHandler(Combo_Value1_Enter);
            Combo_Value1.Leave                   -= new EventHandler(Combo_Value1_Leave);
            Combo_Value1.TextChanged             -= new EventHandler(Form_Changed);
            Combo_Value1.SelectedIndexChanged    -= new EventHandler(Form_Changed);
            Combo_Value2.TextChanged             -= new EventHandler(Form_Changed);
            Button_OK.Click                      -= new EventHandler(Button_OK_Click);
            Check_InactiveClients.CheckedChanged -= Check_InactiveClients_CheckedChanged;
        }

        /// <summary>
        /// Process a change in the checked status for the active only client search
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Check_InactiveClients_CheckedChanged(object sender, EventArgs e)
        {
            EnableActiveOnly = Check_InactiveClients.Checked;
        }

        /// <summary>
        /// Call the validating routine that the user may have set
        /// </summary>
        /// <param name="e"></param>
        /// <remarks></remarks>
        protected void RaiseValidating(CancelEventArgs e)
        {
            OnValidating(e);
        }

        /// <summary>
        /// Dispose the items generated in the form
        /// </summary>
        /// <remarks></remarks>
        private void FormDispose(bool disposing)
        {
            // Dispose the larger objects
            if (disposing)
            {
                if (ds != null)
                {
                    ds.Dispose();
                }
                if (formClientList != null)
                {
                    formClientList.Dispose();
                }
            }
        }

        /// <summary>
        /// return the label associated with the selected category
        /// </summary>
        /// <returns></returns>
        /// <remarks></remarks>
        protected string CategoryLabel
        {
            get
            {
                string Answer = string.Empty;
                if (Combo_Category.SelectedIndex >= 0)
                {
                    Answer = Convert.ToString(((DebtPlus.Data.Controls.ComboboxItem)Combo_Category.SelectedItem).value);
                }
                return Answer;
            }
        }

        /// <summary>
        /// return the label associated with the selected relation
        /// </summary>
        /// <returns></returns>
        /// <remarks></remarks>
        protected string RelationLabel
        {
            get
            {
                string Answer = string.Empty;
                if (Combo_Relation.SelectedIndex >= 0)
                {
                    Answer = Convert.ToString(((DebtPlus.Data.Controls.ComboboxItem)Combo_Relation.SelectedItem).value);
                }
                return Answer;
            }
        }

        /// <summary>
        /// Process the load of the category list
        /// </summary>
        /// <remarks></remarks>
        protected virtual void Load_Category()
        {
            Combo_Category.Properties.Items.Clear();

            // Add the item to the list. This is the default value.
            Combo_Category.Properties.Items.Add(new DebtPlus.Data.Controls.ComboboxItem("Birth date", category_birthdate));
            Combo_Category.Properties.Items.Add(new DebtPlus.Data.Controls.ComboboxItem("Client ID", category_client));
            Combo_Category.Properties.Items.Add(new DebtPlus.Data.Controls.ComboboxItem("Client Last/Former Name only", category_last_name));
            Combo_Category.Properties.Items.Add(new DebtPlus.Data.Controls.ComboboxItem("Client Last/Former Name or Alias", category_alias));
            Combo_Category.Properties.Items.Add(new DebtPlus.Data.Controls.ComboboxItem("Client S.S.N.", category_ssn));
            Combo_Category.Properties.Items.Add(new DebtPlus.Data.Controls.ComboboxItem("Creditor Account Number", category_account_number));
            Combo_Category.Properties.Items.Add(new DebtPlus.Data.Controls.ComboboxItem("Deposit Amount", category_deposit_amt));
            Combo_Category.Properties.Items.Add(new DebtPlus.Data.Controls.ComboboxItem("Deposit Reference", category_deposit_ref));
            Combo_Category.Properties.Items.Add(new DebtPlus.Data.Controls.ComboboxItem("Email Address", category_email));
            Combo_Category.Properties.Items.Add(new DebtPlus.Data.Controls.ComboboxItem("Employer's Address", category_emp_address));
            Combo_Category.Properties.Items.Add(new DebtPlus.Data.Controls.ComboboxItem("Employer's City", category_emp_city));
            Combo_Category.Properties.Items.Add(new DebtPlus.Data.Controls.ComboboxItem("Employer's Name", category_emp_name));
            Combo_Category.Properties.Items.Add(new DebtPlus.Data.Controls.ComboboxItem("Home Address", category_home_address));
            Combo_Category.Properties.Items.Add(new DebtPlus.Data.Controls.ComboboxItem("Home city", category_home_city));
            Combo_Category.Properties.Items.Add(new DebtPlus.Data.Controls.ComboboxItem("Home/Cell/Msg Phone Number", category_personal_phone));
            Combo_Category.Properties.Items.Add(new DebtPlus.Data.Controls.ComboboxItem("Work Phone Number", category_work_phone));

            // Select the defaulted item to be "Client ID"
            Combo_Category.SelectedIndex = 1;
        }

        /// <summary>
        /// Process the load of the relation list
        /// </summary>
        /// <remarks></remarks>
        protected void Load_Relation()
        {
            Combo_Relation.Properties.Items.Clear();
            switch (CategoryLabel)
            {
                case "Client":
                    Combo_Relation.SelectedIndex = Combo_Relation.Properties.Items.Add(new DebtPlus.Data.Controls.ComboboxItem("=", relation_EQ));
                    Combo_Relation.Properties.Items.Add(new DebtPlus.Data.Controls.ComboboxItem("not =", relation_NE));
                    Combo_Relation.Properties.Items.Add(new DebtPlus.Data.Controls.ComboboxItem("less than", relation_LT));
                    Combo_Relation.Properties.Items.Add(new DebtPlus.Data.Controls.ComboboxItem("is at least", relation_GE));
                    Combo_Relation.Properties.Items.Add(new DebtPlus.Data.Controls.ComboboxItem("is greater than", relation_GT));
                    Combo_Relation.Properties.Items.Add(new DebtPlus.Data.Controls.ComboboxItem("not more than", relation_LE));
                    Combo_Relation.Properties.Items.Add(new DebtPlus.Data.Controls.ComboboxItem("between", relation_between));
                    Combo_Relation.Properties.Items.Add(new DebtPlus.Data.Controls.ComboboxItem("not between", relation_not_between));
                    break;

                case "deposit amt":
                case "birthdate":
                    Combo_Relation.SelectedIndex = Combo_Relation.Properties.Items.Add(new DebtPlus.Data.Controls.ComboboxItem("=", relation_EQ));
                    Combo_Relation.Properties.Items.Add(new DebtPlus.Data.Controls.ComboboxItem("not =", relation_NE));
                    Combo_Relation.Properties.Items.Add(new DebtPlus.Data.Controls.ComboboxItem("less than", relation_LT));
                    Combo_Relation.Properties.Items.Add(new DebtPlus.Data.Controls.ComboboxItem("is at least", relation_GE));
                    Combo_Relation.Properties.Items.Add(new DebtPlus.Data.Controls.ComboboxItem("is greater than", relation_GT));
                    Combo_Relation.Properties.Items.Add(new DebtPlus.Data.Controls.ComboboxItem("not more than", relation_LE));
                    Combo_Relation.Properties.Items.Add(new DebtPlus.Data.Controls.ComboboxItem("between", relation_between));
                    Combo_Relation.Properties.Items.Add(new DebtPlus.Data.Controls.ComboboxItem("not between", relation_not_between));
                    break;

                case "last name":
                case "alias":
                case "account number":
                case "deposit ref":
                case "email":
                case "emp address":
                case "emp city":
                case "emp name":
                case "home address":
                case "home city":
                    Combo_Relation.SelectedIndex = Combo_Relation.Properties.Items.Add(new DebtPlus.Data.Controls.ComboboxItem("=", relation_EQ));
                    Combo_Relation.Properties.Items.Add(new DebtPlus.Data.Controls.ComboboxItem("not =", relation_NE));
                    Combo_Relation.Properties.Items.Add(new DebtPlus.Data.Controls.ComboboxItem("starts with", relation_starts));
                    Combo_Relation.Properties.Items.Add(new DebtPlus.Data.Controls.ComboboxItem("ends with", relation_ends));
                    Combo_Relation.Properties.Items.Add(new DebtPlus.Data.Controls.ComboboxItem("doesn't start with", relation_not_starts));
                    Combo_Relation.Properties.Items.Add(new DebtPlus.Data.Controls.ComboboxItem("contains", relation_contains));
                    Combo_Relation.Properties.Items.Add(new DebtPlus.Data.Controls.ComboboxItem("doesn't contain", relation_not_contains));
                    break;

                case "personal phone":
                case "work phone":
                case "ssn":
                    Combo_Relation.SelectedIndex = Combo_Relation.Properties.Items.Add(new DebtPlus.Data.Controls.ComboboxItem("=", relation_EQ));
                    Combo_Relation.Properties.Items.Add(new DebtPlus.Data.Controls.ComboboxItem("not =", relation_NE));
                    Combo_Relation.Properties.Items.Add(new DebtPlus.Data.Controls.ComboboxItem("less than", relation_LT));
                    Combo_Relation.Properties.Items.Add(new DebtPlus.Data.Controls.ComboboxItem("is at least", relation_GE));
                    Combo_Relation.Properties.Items.Add(new DebtPlus.Data.Controls.ComboboxItem("is greater than", relation_GT));
                    Combo_Relation.Properties.Items.Add(new DebtPlus.Data.Controls.ComboboxItem("not more than", relation_LE));
                    Combo_Relation.Properties.Items.Add(new DebtPlus.Data.Controls.ComboboxItem("between", relation_between));
                    Combo_Relation.Properties.Items.Add(new DebtPlus.Data.Controls.ComboboxItem("not between", relation_not_between));
                    Combo_Relation.Properties.Items.Add(new DebtPlus.Data.Controls.ComboboxItem("starts with", relation_starts));
                    Combo_Relation.Properties.Items.Add(new DebtPlus.Data.Controls.ComboboxItem("ends with", relation_ends));
                    Combo_Relation.Properties.Items.Add(new DebtPlus.Data.Controls.ComboboxItem("doesn't start with", relation_not_starts));
                    Combo_Relation.Properties.Items.Add(new DebtPlus.Data.Controls.ComboboxItem("contains", relation_contains));
                    Combo_Relation.Properties.Items.Add(new DebtPlus.Data.Controls.ComboboxItem("doesn't contain", relation_not_contains));
                    break;

                default:
                    break;
            }

            // Load the combo lists
            Load_Value_1();
            Load_Value_2();

            // Hide the second value box until it is selected by the relation
            label_and.Visible = false;
            Combo_Value2.Visible = false;
        }

        /// <summary>
        /// Process the load of the value #1 list
        /// </summary>
        /// <remarks></remarks>
        protected void Load_Value_1()
        {
            // Load the list of previous clients. Select the first one.
            string SavedText = Combo_Value1.Text;

            // Remove the items from the list so that they may be replaced en-mass.
            Combo_Value1.Properties.ReadOnly = false;
            Combo_Value1.Properties.Items.Clear();

            // Load the list of recent clients
            if (MruKey() == "Clients")
            {
                using (DebtPlus.Data.MRU mruList = new DebtPlus.Data.MRU("Clients", "Recent Clients"))
                {
                    string[] mruValues = mruList.GetList;
                    foreach (string mruString in mruValues)
                    {
                        Combo_Value1.Properties.Items.Add(new DebtPlus.Data.Controls.ComboboxItem(mruString));
                    }
                }
            }
            else
            {
                using (DebtPlus.Data.MRU mruList = new DebtPlus.Data.MRU(MruKey()))
                {
                    string[] mruValues = mruList.GetList;
                    foreach (string mruString in mruValues)
                    {
                        Combo_Value1.Properties.Items.Add(new DebtPlus.Data.Controls.ComboboxItem(mruString));
                    }
                }
            }

            // Change the alignment to far/near based upon the type of field.
            Combo_Value1.Properties.Appearance.TextOptions.HAlignment = TextAlignment;

            // Restore the previous text into the input area
            Combo_Value1.SelectedIndex = -1;
            Combo_Value1.Text = SavedText;
        }

        /// <summary>
        /// Process the load of the value #2 list
        /// </summary>
        /// <remarks></remarks>
        protected void Load_Value_2()
        {
            string SavedText = Combo_Value2.Text;

            Combo_Value2.Properties.ReadOnly = false;
            Combo_Value2.Properties.Appearance.TextOptions.HAlignment = TextAlignment;
            Combo_Value2.Text = SavedText;
        }

        /// <summary>
        /// Return the proper horizontal alignment for the input fields.
        /// </summary>
        private DevExpress.Utils.HorzAlignment TextAlignment
        {
            get
            {
                switch (CategoryLabel)
                {
                    case category_deposit_amt:
                    case category_ssn:
                    case category_client:
                        return DevExpress.Utils.HorzAlignment.Far;

                    default:
                        return DevExpress.Utils.HorzAlignment.Near;
                }
            }
        }

        /// <summary>
        /// Do the load sequence for the form
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        /// <remarks></remarks>
        private void ClientSearchClass_Load(Object sender, EventArgs e)
        {
            UnRegisterHandlers();                    // Prevent event tripping when we are configuring the form.
            try
            {
                // Set the checkbox for the active client search from the registry
                Check_InactiveClients.Checked = EnableActiveOnly;

                // Load the category information
                Load_Category();

                // Set the default category information
                string category = DefaultCategory;
                foreach (DebtPlus.Data.Controls.ComboboxItem item in Combo_Category.Properties.Items)
                {
                    if (string.Compare((string)item.value, category, true) == 0)
                    {
                        Combo_Category.SelectedItem = item;
                        break;
                    }
                }

                // Load the relation information
                Load_Relation();

                // Set the default relation
                category = DefaultRelation;
                foreach (DebtPlus.Data.Controls.ComboboxItem item in Combo_Relation.Properties.Items)
                {
                    if (string.Compare((string)item.value, category, true) == 0)
                    {
                        Combo_Relation.SelectedItem = item;
                        break;
                    }
                }

                // Load the controls based upon the selection information since we don't have
                // the events being tripped here.
                Load_Value_1();                      // Load the search list
                Load_Value_2();                      // for both terms
                EnableOK();                          // Disable the OK button for now
            }
            finally
            {
                RegisterHandlers();
            }
        }

        /// <summary>
        /// Process the change of the category
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        /// <remarks></remarks>
        protected void Combo_Category_SelectedValueChanged(object sender, EventArgs e)
        {
            Load_Relation();
            EnableOK();
        }

        /// <summary>
        /// Handle the change in the relation list
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        /// <remarks></remarks>
        protected void Combo_Relation_SelectedIndexChanged(object sender, EventArgs e)
        {
            bool ShouldEnable    = IsValue2Enabled;
            label_and.Visible    = ShouldEnable;
            Combo_Value2.Visible = ShouldEnable;
            EnableOK();
        }

        /// <summary>
        /// Should the second input value be enabled?
        /// </summary>
        /// <returns></returns>
        /// <remarks></remarks>
        protected bool IsValue2Enabled
        {
            get
            {
                // If the item is "between" or "not between" then enable the second value
                switch (RelationLabel)
                {
                    case relation_between:
                    case relation_not_between:
                        return true;

                    default:
                        break;
                }

                // Otherwise do not enable it.
                return false;
            }
        }

        /// <summary>
        /// Should the "OK" button be enabled?
        /// </summary>
        /// <remarks></remarks>
        protected virtual void EnableOK()
        {
            string CategorySelected = CategoryLabel;
            string RelationSelected = RelationLabel;

            // Enable or disable the checkbox based upon the criteria
            Check_InactiveClients.Enabled = !(CategorySelected == "Client" && RelationSelected == "=");

            // Validate the proper parameters
            if (CategorySelected == string.Empty)
            {
                Button_OK.Enabled = false;
                return;
            }

            if (RelationSelected == string.Empty)
            {
                Button_OK.Enabled = false;
                return;
            }

            switch (CategorySelected)
            {
                case category_client:
                    if (!valid_number(Combo_Value1.Text))
                    {
                        Button_OK.Enabled = false;
                        return;
                    }

                    if (IsValue2Enabled && !valid_number(Combo_Value2.Text))
                    {
                        Button_OK.Enabled = false;
                        return;
                    }
                    break;

                case category_ssn:
                    if (!valid_ssn(Combo_Value1.Text))
                    {
                        Button_OK.Enabled = false;
                        return;
                    }

                    if (IsValue2Enabled && !valid_ssn(Combo_Value2.Text))
                    {
                        Button_OK.Enabled = false;
                        return;
                    }
                    break;

                case category_personal_phone:
                case category_work_phone:
                    if (!valid_phone(Combo_Value1.Text))
                    {
                        Button_OK.Enabled = false;
                        return;
                    }

                    if (IsValue2Enabled && !valid_phone(Combo_Value2.Text))
                    {
                        Button_OK.Enabled = false;
                        return;
                    }
                    break;

                case category_deposit_amt:
                    if (!valid_decimal(Combo_Value1.Text))
                    {
                        Button_OK.Enabled = false;
                        return;
                    }

                    if (IsValue2Enabled && !valid_decimal(Combo_Value2.Text))
                    {
                        Button_OK.Enabled = false;
                        return;
                    }
                    break;

                case category_birthdate:
                    if (!valid_date(Combo_Value1.Text))
                    {
                        Button_OK.Enabled = false;
                        return;
                    }

                    if (IsValue2Enabled && !valid_date(Combo_Value2.Text))
                    {
                        Button_OK.Enabled = false;
                        return;
                    }
                    break;

                case category_last_name:
                case category_alias:
                case category_account_number:
                case category_deposit_ref:
                case category_emp_address:
                case category_emp_city:
                case category_emp_name:
                case category_home_address:
                case category_home_city:
                    if (!valid_string(Combo_Value1.Text))
                    {
                        Button_OK.Enabled = false;
                        return;
                    }

                    if (IsValue2Enabled && !valid_string(Combo_Value2.Text))
                    {
                        Button_OK.Enabled = false;
                        return;
                    }
                    break;

                case category_email:
                    if (!valid_email(Combo_Value1.Text))
                    {
                        Button_OK.Enabled = false;
                        return;
                    }

                    if (IsValue2Enabled && !valid_email(Combo_Value2.Text))
                    {
                        Button_OK.Enabled = false;
                        return;
                    }
                    break;

                default:
                    Debug.Assert(false);
                    break;
            }

            // Everything is valid. Enable the OK button.
            Button_OK.Enabled = true;
        }

        /// <summary>
        /// Is the item a valid numeric value?
        /// </summary>
        /// <param name="InputString">Source string to be tested</param>
        /// <returns>TRUE if valid. FALSE otherwise.</returns>
        /// <remarks></remarks>
        protected static bool valid_number(string InputString)
        {
            Int32 Result;
            return Int32.TryParse(InputString, System.Globalization.NumberStyles.Integer, System.Globalization.CultureInfo.CurrentCulture, out Result);
        }

        /// <summary>
        /// Is the item a valid telephone number?
        /// </summary>
        /// <param name="InputString">Source string to be tested</param>
        /// <returns>TRUE if valid. FALSE otherwise.</returns>
        /// <remarks></remarks>
        protected bool valid_phone(string InputString)
        {
            switch (RelationLabel)
            {
                case relation_contains:
                case relation_not_contains:
                case relation_starts:
                case relation_not_starts:
                case relation_ends:
                    return valid_string(InputString);    // Portion only. Do not validate the whole string.
                default:
                    return valid_string(InputString);    // Check the phone number later
            }
        }

        /// <summary>
        /// Is the item a valid social security number?
        /// </summary>
        /// <param name="InputString">Source string to be tested</param>
        /// <returns>TRUE if valid. FALSE otherwise.</returns>
        /// <remarks></remarks>
        protected bool valid_ssn(string InputString)
        {
            switch (RelationLabel)
            {
                case relation_contains:
                case relation_not_contains:
                case relation_starts:
                case relation_not_starts:
                case relation_ends:
                    return valid_string(InputString);    // Portion only. Do not validate the whole string.
                default:
                    return valid_string(InputString);    // check the ssn later
            }
        }

        /// <summary>
        /// Is the item a valid monetary number?
        /// </summary>
        /// <param name="InputString">Source string to be tested</param>
        /// <returns>TRUE if valid. FALSE otherwise.</returns>
        /// <remarks></remarks>
        protected static bool valid_decimal(string InputString)
        {
            decimal NumberTest;
            return decimal.TryParse(InputString, out NumberTest);
        }

        /// <summary>
        /// Is the item a valid string?
        /// </summary>
        /// <param name="InputString">Source string to be tested</param>
        /// <returns>TRUE if valid. FALSE otherwise.</returns>
        /// <remarks></remarks>
        protected static bool valid_string(string InputString)
        {
            return InputString.Trim().Length > 0;
        }

        /// <summary>
        /// Is the item a valid date?
        /// </summary>
        /// <param name="InputString">Source string to be tested</param>
        /// <returns>TRUE if valid. FALSE otherwise.</returns>
        /// <remarks></remarks>
        protected static bool valid_date(string InputString)
        {
            DateTime NumberTest;
            return DateTime.TryParse(InputString, out NumberTest);
        }

        /// <summary>
        /// Is the item a valid email address?
        /// </summary>
        /// <param name="InputString">Source string to be tested</param>
        /// <returns>TRUE if valid. FALSE otherwise.</returns>
        /// <remarks></remarks>
        protected bool valid_email(string InputString)
        {
            switch (RelationLabel)
            {
                case relation_contains:
                case relation_not_contains:
                case relation_starts:
                case relation_not_starts:
                case relation_ends:
                    return valid_string(InputString);    // Portion only. Do not validate the whole string.

                default:
                    return valid_string(InputString);    // Check the email address later
            }
        }

        /// <summary>
        /// Process the entry to the input fields. Select all of the text.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        /// <remarks></remarks>
        protected void Combo_Value1_Enter(Object sender, EventArgs e)
        {
            ((DevExpress.XtraEditors.ComboBoxEdit)sender).SelectAll();
        }

        /// <summary>
        /// Process the change in the second input box.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        /// <remarks></remarks>
        protected void Form_Changed(Object sender, EventArgs e)
        {
            EnableOK();
        }

        /// <summary>
        /// Leave the input fields.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        /// <remarks></remarks>
        protected void Combo_Value1_Leave(Object sender, EventArgs e)
        {
            DevExpress.XtraEditors.ComboBoxEdit cbx = (DevExpress.XtraEditors.ComboBoxEdit)sender;
            cbx.Text = cbx.Text.Trim();
            EnableOK();
        }

        /// <summary>
        /// Convert the input value to proper format
        /// </summary>
        /// <param name="ctl">Pointer to the text control to be used for the value</param>
        /// <returns></returns>
        protected virtual string Item_Value(string ctl)
        {
            return Item_Value(ctl, string.Empty, string.Empty);
        }

        /// <summary>
        /// Convert the input value to proper format
        /// </summary>
        /// <param name="ctl">Pointer to the text control to be used for the value</param>
        /// <param name="strPrefix">Wildcard prefix string "%" or empty</param>
        /// <param name="strSuffix">Wildcard prefix string "%" or empty</param>
        /// <returns></returns>
        /// <remarks></remarks>
        protected virtual string Item_Value(string ctl, string strPrefix, string strSuffix)
        {
            switch (CategoryLabel)
            {
                case category_client:
                    return strPrefix + Convert.ToInt32(ctl).ToString() + strSuffix;

                case category_ssn:
                    if (RelationLabel == relation_EQ || RelationLabel == relation_NE)
                    {
                        return string.Format("'{0}{1}{2}'", strPrefix, DebtPlus.Utils.Format.Strings.DigitsOnly(ctl).PadLeft(9, '0'), strSuffix);
                    }
                    else
                    {
                        return string.Format("'{0}{1}{2}'", strPrefix, DebtPlus.Utils.Format.Strings.DigitsOnly(ctl), strSuffix);
                    }

                case category_deposit_amt:
                    return strPrefix + Convert.ToDouble(ctl).ToString() + strSuffix;

                case category_birthdate:
                    return string.Format("'{0}{1:yyyyMMdd}{2}'", strPrefix, Convert.ToDateTime(ctl), strSuffix);

                case category_personal_phone:
                case category_work_phone:
                    return string.Format("'{0}{1}{2}'", strPrefix, DebtPlus.Utils.Format.Strings.DigitsOnly(ctl), strSuffix);

                default:
                    return string.Format("'{0}{1}{2}'", strPrefix, ctl.Trim().Replace("'", "''"), strSuffix);
            }
        }

        /// <summary>
        /// return the relation and value strings
        /// </summary>
        /// <param name="key">Field name</param>
        /// <returns></returns>
        /// <remarks></remarks>
        protected virtual string Relation_Value(string key)
        {
            string RelationString = RelationLabel;
            string Answer = string.Empty;
            switch (RelationString)
            {
                case relation_EQ:
                    Answer = key + "=" + Item_Value(Combo_Value1.Text);
                    break;

                case relation_LT:
                    Answer = key + "<" + Item_Value(Combo_Value1.Text);
                    break;

                case relation_LE:
                    Answer = key + "<=" + Item_Value(Combo_Value1.Text);
                    break;

                case relation_GT:
                    Answer = key + ">" + Item_Value(Combo_Value1.Text);
                    break;

                case relation_GE:
                    Answer = key + ">=" + Item_Value(Combo_Value1.Text);
                    break;

                case relation_NE:
                    Answer = key + "<>" + Item_Value(Combo_Value1.Text);
                    break;

                case relation_starts:
                    Answer = string.Format("{0} LIKE {1}", key, Item_Value(Combo_Value1.Text, string.Empty, "%"));
                    break;

                case relation_ends:
                    Answer = string.Format("{0} LIKE {1}", key, Item_Value(Combo_Value1.Text, "%", string.Empty));
                    break;

                case relation_not_starts:
                    Answer = string.Format("{0} NOT LIKE {1}", key, Item_Value(Combo_Value1.Text, string.Empty, "%"));
                    break;

                case relation_contains:
                    Answer = string.Format("{0} LIKE {1}", key, Item_Value(Combo_Value1.Text, "%", "%"));
                    break;

                case relation_not_contains:
                    Answer = string.Format("{0} NOT LIKE {1}", key, Item_Value(Combo_Value1.Text, "%", "%"));
                    break;

                case relation_between:
                    Answer = string.Format("{0} BETWEEN {1} AND {2}", key, Item_Value(Combo_Value1.Text), Item_Value(Combo_Value2.Text));
                    break;

                default:
                    break;
            }

            return Answer;
        }

        /// <summary>
        /// Fetch a match using the alias table.
        /// </summary>
        /// <returns></returns>
        /// <remarks></remarks>
        protected string Fetch_Alias()
        {
            string Answer = string.Format("(({0})", Relation_Value("last"));
            Answer += string.Format(" OR ({0})", Relation_Value("former"));
            Answer += string.Format(" OR (client IN (SELECT client FROM client_addkeys WHERE {0})))", Relation_Value("additional"));
            return Answer;
        }

        /// <summary>
        /// Fetch a match using the alias table.
        /// </summary>
        /// <param name="strName">Field name to be used in the search</param>
        /// <returns></returns>
        /// <remarks></remarks>
        protected string Fetch_Name(string strName)
        {
            return Relation_Value(strName);
        }

        /// <summary>
        /// We want a Client field. Use the fields.
        /// </summary>
        /// <param name="strName">Field name to be used in the search</param>
        /// <returns></returns>
        /// <remarks></remarks>
        protected string Fetch_Client(string strName)
        {
            return Relation_Value(strName);
        }

        /// <summary>
        /// We want a Client address field
        /// </summary>
        /// <param name="strName">Field name to be used in the search</param>
        /// <returns></returns>
        /// <remarks></remarks>
        protected string Fetch_ClientAddress(string strName)
        {
            string strSelect = string.Format("SELECT client FROM clients c with (nolock) inner join Addresses a with (nolock) on c.AddressID strSelect = a.Address WHERE {0}", Relation_Value("a." + strName));
            return string.Format("client IN ({0})", strSelect);
        }

        /// <summary>
        /// We want a Client employer address field
        /// </summary>
        /// <param name="strName">Field name to be used in the search</param>
        /// <returns></returns>
        /// <remarks></remarks>
        protected string Fetch_EmployerAddress(string strName)
        {
            string strSelect = string.Format("SELECT client FROM people p with (nolock) inner join employers e with (nolock) ON p.employer = e.employer inner join Addresses a with (nolock) on e.AddressID strSelect = a.Address WHERE {0}", Relation_Value(strName));
            return string.Format("client IN ({0})", strSelect);
        }

        /// <summary>
        /// We want a Client employer address field
        /// </summary>
        /// <param name="strName">Field name to be used in the search</param>
        /// <returns></returns>
        /// <remarks></remarks>
        protected string Fetch_EmployerName(string strName)
        {
            string strSelect = string.Format("SELECT client FROM people p with (nolock) inner join employers e with (nolock) ON p.employer strSelect = e.employer WHERE {0}", Relation_Value("e." + strName));
            return string.Format("client IN ({0})", strSelect);
        }

        /// <summary>
        /// We want a person field. Use the fields.
        /// </summary>
        /// <param name="strName">Field name to be used in the search</param>
        /// <returns></returns>
        /// <remarks></remarks>
        protected string Fetch_Person(string strName)
        {
            string strSelect = string.Format("SELECT client FROM people p with (nolock) WHERE {0}", Relation_Value(strName));
            return string.Format("client IN ({0})", strSelect);
        }

        /// <summary>
        /// We want a non-work phone number field.
        /// </summary>
        /// <param name="txn">Transaction object on the connection</param>
        /// <remarks></remarks>
        protected void BuildTelephoneList(SqlTransaction txn)
        {
            using (var cmd = new System.Data.SqlClient.SqlCommand())
            {
                cmd.Transaction = txn;
                cmd.Connection = txn.Connection;
                cmd.CommandText = string.Format("SELECT TelephoneNumber INTO #TelephoneNumbers FROM TelephoneNumbers WITH (NOLOCK) WHERE {0} create unique clustered index ix1_client_search_telephone on #TelephoneNumbers ( TelephoneNumber );", Relation_Value("SearchValue"));
                cmd.CommandType = CommandType.Text;
                cmd.ExecuteNonQuery();
            }
        }

        /// <summary>
        /// Discard the telephone list from the database
        /// </summary>
        /// <param name="txn">Transaction object on the connection</param>
        /// <remarks></remarks>
        protected void DestroyTelephoneList(SqlTransaction txn)
        {
            using (var cmd = new System.Data.SqlClient.SqlCommand())
            {
                cmd.Transaction = txn;
                cmd.Connection = txn.Connection;
                cmd.CommandText = "DROP TABLE #TelephoneNumbers";
                cmd.CommandType = CommandType.Text;
                cmd.ExecuteNonQuery();
            }
        }

        /// <summary>
        /// We want a work phone number field.
        /// </summary>
        /// <returns></returns>
        /// <remarks></remarks>
        protected string Fetch_Work_Phone()
        {
            StringBuilder sb = new StringBuilder();

            string TelephoneSelect = string.Format("(SELECT TelephoneNumber FROM TelephoneNumbers WHERE {0})", Relation_Value("SearchValue"));
            sb.AppendFormat("SELECT client FROM people WHERE WorkTelephoneID IN {0}", TelephoneSelect);
            return string.Format("client IN ({0})", sb.ToString());
        }

        /// <summary>
        /// We want an employer field. Use the fields.
        /// </summary>
        /// <param name="strName1"></param>
        /// <param name="strName2"></param>
        /// <returns></returns>
        /// <remarks></remarks>
        protected string Fetch_Employer(string strName1, string strName2)
        {
            string strSelect;

            if (!string.IsNullOrEmpty(strName2))
            {
#if false // multiple jobs for each person (new way)
                strSelect = "SELECT pj.employer FROM people p INNER JOIN people_jobs pj ON p.client=pj.client AND p.person=pj.person INNER JOIN employers e ON pj.employer=e.employer WHERE";
#else    // single job per person (old way)
                strSelect = "SELECT p.employer FROM people p WITH (NOLOCK) INNER JOIN employers e WITH (NOLOCK) ON p.employer=e.employer WHERE";
#endif
                strSelect += string.Format(" ({0}) OR ({1})", Relation_Value(strName1), Relation_Value(strName2));
            }
            else
            {
#if false // multiple jobs for each person (new way)
                strSelect = string.Format("SELECT employer FROM employers e WITH (NOLOCK) INNER JOIN people_jobs pj WITH (NOLOCK) on e.employer=pj.employer WHERE {0}", Relation_Value(strName1));
#else    // single job per person (old way)
                strSelect = string.Format("SELECT employer FROM employers e WITH (NOLOCK) INNER JOIN people p WITH (NOLOCK) on e.employer=p.employer WHERE {0}", Relation_Value(strName1));
#endif
            }

            return string.Format("employer IN ({0})", strSelect);
        }

        /// <summary>
        /// We want the deposit reference field.
        /// </summary>
        /// <returns></returns>
        /// <remarks></remarks>
        protected string Fetch_Reference()
        {
            string strSelect = string.Format("SELECT client FROM registers_client with (nolock) WHERE tran_type='DP' AND {0}", Relation_Value("message"));
            return string.Format("client IN ({0})", strSelect);
        }

        /// <summary>
        /// We want the deposit reference field.
        /// </summary>
        /// <param name="strName"></param>
        /// <returns></returns>
        /// <remarks></remarks>
        protected string Fetch_Email(string strName)
        {
            string strSelect = string.Format("SELECT client FROM people p with (nolock) inner join EmailAddresses e with (o on p.{0} strSelect = e.Email where {1}", strName, Relation_Value("e.Address"));
            return string.Format("client IN ({0})", strSelect);
        }

        /// <summary>
        /// We want the deposit amount. Look in deposits.
        /// </summary>
        /// <returns></returns>
        /// <remarks></remarks>
        protected string Fetch_Deposit()
        {
#if false // new way -- ach is seperate
            string strSelect = "SELECT client FROM client_deposits WHERE " + Relation_Value("deposit_amount") + " UNION ALL SELECT client FROM client_ach WHERE " + Relation_Value("pull_amount");
#else // old way -- ach is part of the client_deposits table;
            string strSelect = string.Format("SELECT client FROM client_deposits with (nolock) WHERE {0}", Relation_Value("deposit_amount"));
#endif
            return string.Format("client IN ({0})", strSelect);
        }

        /// <summary>
        /// We want a creditor account number. Look in the client_creditor table for the match.
        /// </summary>
        /// <returns></returns>
        /// <remarks></remarks>
        protected string Fetch_CreditorAccount()
        {
            return string.Format("client IN (SELECT Client FROM client_creditor with (nolock) WHERE {0})", Relation_Value("account_number"));
        }

        /// <summary>
        /// Handle the "OK" button. Do the database search operation.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        /// <remarks></remarks>
        protected void Button_OK_Click(Object sender, EventArgs e)
        {
            ProcessOKButton();
        }

        /// <summary>
        /// Process the click of the OK button
        /// </summary>
        /// <remarks></remarks>
        protected virtual void ProcessOKButton()
        {
            switch (CategoryLabel)
            {
                case category_alias:
                    selectClause = Fetch_Alias();
                    break;

                case category_work_phone:
                    selectClause = Fetch_Work_Phone();
                    break;

                case category_client:
                    selectClause = Fetch_Client("client");
                    break;

                case category_last_name:
                    selectClause = Fetch_Name("last");
                    break;

                case category_ssn:
                    selectClause = Fetch_Person("replace(ssn,'-','')");
                    break;

                case category_birthdate:
                    selectClause = Fetch_Person("CONVERT(varchar(8),birthdate,112)");
                    break;

                case category_account_number:
                    selectClause = Fetch_CreditorAccount();
                    break;

                case category_deposit_amt:
                    selectClause = Fetch_Deposit();
                    break;

                case category_home_address:
                    selectClause = Relation_Value("replace(v.[addr_1],'  ', ' ')");
                    break;

                case category_home_city:
                    selectClause = Relation_Value("v.[city]");
                    break;

                case category_emp_name:
                    selectClause = Fetch_EmployerName("name");
                    break;

                case category_emp_address:
                    selectClause = Fetch_EmployerAddress("street");
                    break;

                case category_emp_city:
                    selectClause = Fetch_EmployerAddress("city");
                    break;

                case category_email:
                    selectClause = Relation_Value("e.Address");
                    break;

                case category_deposit_ref:
                    selectClause = Fetch_Reference();
                    break;

                case category_personal_phone:
                    selectClause = string.Empty; // This is handled below.
                    break;

                default:
                    Debug.Assert(false);
                    return;
            }

            // Determine if the Client is present
            if (CategoryLabel == category_client && RelationLabel == relation_EQ)
            {
                // Convert the client id and determine if the client exists. We don't care about the Id, but we want something to determine existence.
                Int32 ClientId = Convert.ToInt32(Combo_Value1.Text.Trim());
                using (var bc = new BusinessContext())
                {
                    try
                    {
                        var rslt = (from clnt in bc.clients where clnt.Id == ClientId select new { clnt.Id }).FirstOrDefault();
                        if (rslt == null)
                        {
                            DebtPlus.Data.Forms.MessageBox.Show("The Client is not valid. Please choose an existing Client.", "Information ! Found", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Stop);
                            return;
                        }
                    }
                    catch (SqlException ex)
                    {
                        DebtPlus.UI.Common.ErrorHandling.HandleErrors(ex, "Error reading client status");
                        return;
                    }
                }

                if (SetClient(ClientId))
                {
                    DefaultCategory = CategoryLabel;
                    DefaultRelation = RelationLabel;
                    DialogResult = System.Windows.Forms.DialogResult.OK;
                }
                return;
            }

            // Do the Client search operation
            lastClient = -1;
            DataTable tbl = ReadSearchList(lastClient);
            if (tbl == null)
            {
                return;
            }

            if (tbl.Rows.Count == 0)
            {
                DebtPlus.Data.Forms.MessageBox.Show("There are no matches in the database to the search criteria that you entered. Please make a new choice.", "Information ! Found", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Stop);
                return;
            }

            if (tbl.Rows.Count == 1)
            {
                if (SetClient(lastClient))
                {
                    DialogResult = System.Windows.Forms.DialogResult.OK;
                }
                return;
            }

            // Create a form to hold the excess
            formClientList = new ClientSearchSelectForm(this);
            System.Windows.Forms.DialogResult result;
            using (formClientList)
            {
                formClientList.NextPage += new EventHandler(FormClientList_NextPage);

                // Load the items into the form and enable the controls
                LoadListControl(tbl);

                // Run the dialog
                result = formClientList.ShowDialog();

                // Release the form
                formClientList.NextPage -= new EventHandler(FormClientList_NextPage);
            }
            formClientList = null;

            // If the dialog completed successfully then complete the search the same way.
            if (result == System.Windows.Forms.DialogResult.OK)
            {
                DefaultCategory = CategoryLabel;
                DefaultRelation = RelationLabel;
                DialogResult = System.Windows.Forms.DialogResult.OK;
            }
        }

        /// <summary>
        /// Process the "MORE" button from the selection form.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        /// <remarks></remarks>
        protected void FormClientList_NextPage(Object sender, EventArgs e)
        {
            DataTable tbl = ReadSearchList(lastClient);
            if (tbl != null)
            {
                LoadListControl(tbl);
            }
        }

        /// <summary>
        /// Retrieve or set the registry setting for the "Active Only" checkbox status.
        /// </summary>
        private bool EnableActiveOnly
        {
            get
            {
                object obj = GetUserOption("Client Search Active Only");
                if (obj != null)
                {
                    bool value;
                    if (Boolean.TryParse((string)obj, out value))
                    {
                        return value;
                    }
                }
                return false;
            }
            set
            {
                setUserOption("Client Search Active Only", value.ToString());
            }
        }

        /// <summary>
        /// Return a value from the registry for the User Options on this person
        /// </summary>
        /// <param name="key"></param>
        /// <returns></returns>
        protected static object GetUserOption(string key)
        {
            Microsoft.Win32.RegistryKey RegKey = Microsoft.Win32.Registry.CurrentUser;

            try
            {
                RegKey = RegKey.OpenSubKey(string.Format(@"{0}\options", DebtPlus.Configuration.AssemblyInfo.InstallationRegistryKey), false);
                if (RegKey != null)
                {
                    return RegKey.GetValue(key);
                }
            }
            catch { }
            finally
            {
                if (RegKey != null)
                {
                    RegKey.Close();
                }
            }
            return null;
        }

        /// <summary>
        /// Find the number of clients to return in the selection data
        /// </summary>
        /// <returns></returns>
        /// <remarks></remarks>
        protected static Int32 SearchLimit()
        {
            object obj = GetUserOption("Client search limit") ?? GetMachineOption("Client search limit");
            if (obj != null)
            {
                if (obj is Int32 || obj is Int64)
                {
                    return Convert.ToInt32(obj);
                }

                if (obj is string)
                {
                    Int32 value;
                    if (Int32.TryParse((string) obj, out value))
                    {
                        return value;
                    }
                }
            }

            return 1000;
        }

        /// <summary>
        /// Return a value from the registry for the Machine Options on this person
        /// </summary>
        /// <param name="key"></param>
        /// <returns></returns>
        protected static object GetMachineOption(string key)
        {
            Microsoft.Win32.RegistryKey RegKey = Microsoft.Win32.Registry.LocalMachine;

            try
            {
                RegKey = RegKey.OpenSubKey(string.Format(@"{0}\options", DebtPlus.Configuration.AssemblyInfo.InstallationRegistryKey), false);
                if (RegKey != null)
                {
                    return RegKey.GetValue(key);
                }
                return null;
            }
            catch
            {
                return null;
            }
            finally
            {
                if (RegKey != null)
                {
                    RegKey.Close();
                }
            }
        }

        /// <summary>
        /// Set the user local option
        /// </summary>
        /// <param name="key">Key field</param>
        /// <param name="value">Value for the setting</param>
        private static bool setUserOption(string key, object value)
        {
            Microsoft.Win32.RegistryKey RegKey = Microsoft.Win32.Registry.CurrentUser;
            try
            {
                RegKey = RegKey.OpenSubKey(string.Format(@"{0}\options", DebtPlus.Configuration.AssemblyInfo.InstallationRegistryKey), true);
                if (RegKey != null)
                {
                    RegKey.SetValue(key, value);
                    return true;
                }
            }
            catch { }
            finally
            {
                if (RegKey != null)
                {
                    RegKey.Close();
                }
            }

            return false;
        }

        /// <summary>
        /// Retrieve the recordset from the database
        /// </summary>
        /// <param name="ds">Pointer to the dataset to be updated with the results</param>
        /// <param name="PreviousClient">if "MORE" was last used, this is the Client ID where to start the search.</param>
        /// <returns></returns>
        /// <remarks></remarks>
        protected DataTable ReadSearchList(Int32 PreviousClient)
        {
            const string TableName = "ClientList";
            string connectionString;
            Int32 commandTimeout;

            DebtPlus.LINQ.SQLInfoClass SqlInfo = DebtPlus.LINQ.SQLInfoClass.getDefault();
            connectionString = DebtPlus.LINQ.SQLInfoClass.getDefault().ConnectionString;
            commandTimeout = DebtPlus.LINQ.SQLInfoClass.getDefault().CommandTimeout;

            SqlConnection cn = new SqlConnection(connectionString);
            SqlTransaction txn = null;

            // Toss the current table contents in prep for loading a new block
            DataTable tbl = ds.Tables[TableName];
            if (tbl != null)
            {
                tbl.Clear();
            }

            try
            {
                using (var cm = new DebtPlus.UI.Common.CursorManager())
                {
                    // Open the connection and start the transaction for now.
                    cn.Open();
                    txn = cn.BeginTransaction(IsolationLevel.ReadUncommitted);

                    using (var cmd = new SqlCommand())
                    {
                        cmd.Connection = cn;
                        cmd.Transaction = txn;
                        cmd.CommandType = CommandType.Text;
                        cmd.CommandTimeout = Math.Max(180, commandTimeout);

                        // We start with the command
                        System.Text.StringBuilder strSQL = new System.Text.StringBuilder();
                        strSQL.Append("SELECT ");

                        // Do something special for email addresses
                        if (CategoryLabel == category_email)
                        {
                            strSQL.Append("DISTINCT ");
                        }

                        // Limit the search if desired
                        Int32 limit = SearchLimit();
                        if (limit > 0)
                        {
                            strSQL.AppendFormat("TOP {0:f0} ", (limit + 1));
                        }

                        // Standard items to be received ...
                        strSQL.AppendFormat("v.[Client],v.[SSN],v.[Status],v.[HomePhone],v.[Name],v.[addr_1],v.[addr_2],v.[addr_3],dbo.address_block_5(v.[addr_1],v.[addr_2],v.[addr_3],default,default) As Address FROM view_client_search v WITH (NOLOCK)");

                        // If we are searching for a personal phone number then we need to build the list of telephone numbers
                        string CurrentSelection = string.Empty;
                        if (CategoryLabel == category_personal_phone)
                        {
                            try
                            {
                                BuildTelephoneList(txn);
                            }
                            catch (SqlException ex)
                            {
                                DebtPlus.UI.Common.ErrorHandling.HandleErrors(ex, "Error building telephone list");
                                return null;
                            }

                            strSQL.Append(" INNER JOIN #TelephoneNumbers n WITH (NOLOCK) ON n.TelephoneNumber IN (v.HomeTelephoneID, v.MsgTelephoneID, v.CellTelephoneID_1, v.CellTelephoneID_2)");
                        }
                        else
                        {
                            if (CategoryLabel == category_email)
                            {
                                strSQL.Append(" INNER JOIN people p with (nolock) ON v.Client = p.Client");
                                strSQL.Append(" INNER JOIN EmailAddresses e with (nolock) ON p.EmailID = e.Email");
                            }
                            CurrentSelection = " AND " + selectClause;
                        }

                        // Add the last Client in the list if there is one
                        if (lastClient >= 0)
                        {
                            CurrentSelection += " AND (v.Client >= @lastClient)";
                            cmd.Parameters.Add("@lastClient", SqlDbType.Int).Value = lastClient;
                        }

                        // Suppress the active clients if needed
                        if (Check_InactiveClients.Checked)
                        {
                            CurrentSelection += " AND v.Status in ('A','AR')";
                        }

                        // Include the "WHERE" clause and remove the leading "AND"
                        if (CurrentSelection != string.Empty)
                        {
                            strSQL.Append(" WHERE ");
                            strSQL.Append(CurrentSelection.Substring(5));
                        }

                        strSQL.Append(" ORDER BY v.Client");

                        // Build a dataset for the control
                        cmd.CommandText = strSQL.ToString();

                        using (var da = new SqlDataAdapter(cmd))
                        {
                            da.Fill(ds, TableName);
                        }
                    }

                    // If we are searching for a personal phone number then we need to destroy the list of telephone numbers
                    if (CategoryLabel == category_personal_phone)
                    {
                        try
                        {
                            DestroyTelephoneList(txn);
                        }
                        catch (SqlException ex)
                        {
                            DebtPlus.UI.Common.ErrorHandling.HandleErrors(ex, "Error destroying telephone list");
                            return null;
                        }
                    }

                    // Find the last Client in the list if there is a limit to the number of clients
                    tbl = ds.Tables[TableName];
                    Int32 nRowCount = tbl.Rows.Count - 1;
                    if (nRowCount >= 0)
                    {
                        lastClient = Convert.ToInt32(tbl.Rows[nRowCount]["Client"]);
                    }
                }

                // Commit the transaction now that we are complete.
                if (txn != null)
                {
                    txn.Commit();
                    txn.Dispose();
                    txn = null;
                }
            }
            finally
            {
                // If there is a pending transaction, roll it back.
                if (txn != null)
                {
                    try
                    {
                        txn.Rollback();
                    }
                    catch { }
                    txn.Dispose();
                    txn = null;
                }
                cn.Dispose();
            }
            return tbl;
        }

        /// <summary>
        /// Load the table into the display form.
        /// </summary>
        /// <param name="tbl"></param>
        /// <remarks></remarks>
        protected void LoadListControl(DataTable tbl)
        {
            Int32 limit = SearchLimit();

            // Enable or disable the MORE button if there are too many items
            formClientList.Button_More.Visible = false;
            if (limit > 0)
            {
                if (tbl.Rows.Count >= limit)
                {
                    formClientList.Button_More.Visible = true;
                    tbl.Rows.RemoveAt(limit);
                }
            }

            // Bind the dataset to the grid control and populate it.
            formClientList.GridControl1.DataSource = tbl;
            formClientList.GridControl1.RefreshDataSource();
        }

        /// <summary>
        /// Process the change of the category
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        /// <remarks></remarks>
        protected virtual void Combo_Category_SelectedIndexChanged(Object sender, EventArgs e)
        {
            Load_Relation();
            EnableOK();
        }

        /// <summary>
        /// Process the change of the category
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        /// <remarks></remarks>
        protected virtual void Combo_Category_ValueChanged(Object sender, EventArgs e)
        {
            Load_Relation();
            EnableOK();
        }

        /// <summary>
        /// Client for the search
        /// </summary>
        /// <value></value>
        /// <returns></returns>
        /// <remarks></remarks>
        public Int32 ClientId { get; set; }

        /// <summary>
        /// Attempt to set the Client result. This will trip the validating event.
        /// </summary>
        /// <param name="ClientId">The Client ID found by the search</param>
        /// <returns>TRUE if the Client is validated. FALSE otherwise.</returns>
        /// <remarks></remarks>
        protected internal bool SetClient(Int32 ClientId)
        {
            this.ClientId = ClientId;
            CancelEventArgs e = new CancelEventArgs(false);
            RaiseValidating(e);
            if (e.Cancel)
            {
                return false;
            }

            // Save the Client for the next time in the history
            Save_Recent(ClientId);
            return true;
        }

        /// <summary>
        /// Default category information is in the registry
        /// </summary>
        /// <value></value>
        /// <returns></returns>
        /// <remarks></remarks>
        private static string DefaultCategory
        {
            get
            {
                string Answer = string.Empty;
                string Path = System.IO.Path.Combine(DebtPlus.Configuration.AssemblyInfo.InstallationRegistryKey, "history");
                using (Microsoft.Win32.RegistryKey reg = Microsoft.Win32.Registry.CurrentUser.OpenSubKey(Path, false))
                {
                    if (reg != null)
                    {
                        Answer = DebtPlus.Utils.Nulls.DStr(reg.GetValue("Client search method"));
                    }
                }

                // Default the answer
                if (string.IsNullOrEmpty(Answer))
                {
                    Answer = category_client;
                }
                return Answer;
            }

            set
            {
                string Path = System.IO.Path.Combine(DebtPlus.Configuration.AssemblyInfo.InstallationRegistryKey, "history");
                using (Microsoft.Win32.RegistryKey reg = Microsoft.Win32.Registry.CurrentUser.CreateSubKey(Path))
                {
                    if (reg != null)
                    {
                        reg.SetValue("Client search method", value.ToLower(), Microsoft.Win32.RegistryValueKind.String);
                    }
                }
            }
        }

        /// <summary>
        /// Default relation information is in the registry
        /// </summary>
        /// <value></value>
        /// <returns></returns>
        /// <remarks></remarks>
        private static string DefaultRelation
        {
            get
            {
                string Answer = string.Empty;
                Microsoft.Win32.RegistryKey reg = Microsoft.Win32.Registry.CurrentUser;
                try
                {
                    string Path = System.IO.Path.Combine(DebtPlus.Configuration.AssemblyInfo.InstallationRegistryKey, "history");
                    reg = reg.OpenSubKey(Path, false);
                    if (reg != null)
                    {
                        Answer = DebtPlus.Utils.Nulls.DStr(reg.GetValue("Client search relation"));
                    }
                }
                catch { }
                finally
                {
                    if (reg != null)
                    {
                        reg.Close();
                    }
                }

                // Default the answer
                if (string.IsNullOrEmpty(Answer))
                {
                    Answer = relation_EQ;
                }
                return Answer;
            }

            set
            {
                Microsoft.Win32.RegistryKey reg = Microsoft.Win32.Registry.CurrentUser;
                try
                {
                    string Path = System.IO.Path.Combine(DebtPlus.Configuration.AssemblyInfo.InstallationRegistryKey, "history");
                    reg = reg.OpenSubKey(Path, true);
                    if (reg != null)
                    {
                        reg.SetValue("Client search relation", value.ToLower(), Microsoft.Win32.RegistryValueKind.String);
                    }
                }
                catch { }
                finally
                {
                    if (reg != null)
                    {
                        reg.Close();
                    }
                }
            }
        }

        /// <summary>
        /// Return the key string to the MRU setting based upon the selection criteria
        /// </summary>
        /// <returns></returns>
        internal string MruKey()
        {
            switch (CategoryLabel)
            {
                case category_client:
                    return MRU_client;
                case category_deposit_amt:
                    return MRU_deposit_amt;
                case category_ssn:
                    return MRU_ssn;
                case category_birthdate:
                    return MRU_birthdate;
                case category_last_name:
                    return MRU_last_name;
                case category_alias:
                    return MRU_alias;
                case category_account_number:
                    return MRU_account_number;
                case category_deposit_ref:
                    return MRU_deposit_ref;
                case category_email:
                    return MRU_email;
                case category_emp_address:
                    return MRU_emp_address;
                case category_emp_city:
                    return MRU_emp_city;
                case category_emp_name:
                    return MRU_emp_name;
                case category_home_address:
                    return MRU_home_address;
                case category_home_city:
                    return MRU_home_city;
                case category_personal_phone:
                    return MRU_personal_phone;
                case category_work_phone:
                    return MRU_work_phone;
                default:
                    throw new System.ArgumentOutOfRangeException("CategoryLabel");
            }
        }

        /// <summary>
        /// Save the item to the history table
        /// </summary>
        /// <param name="Client"></param>
        internal void Save_Recent(Int32 ClientId)
        {
            // The Client is always saved in the Clients key.
            using (DebtPlus.Data.MRU mruList = new DebtPlus.Data.MRU("Clients"))
            {
                mruList.InsertTopMost(ClientId);
                mruList.SaveKey();
            }

            // Save the value in the selection index for the next operation
            if (MruKey() != "Clients")
            {
                using (DebtPlus.Data.MRU mruList = new DebtPlus.Data.MRU(MruKey()))
                {
                    mruList.InsertTopMost(Combo_Value1.Text.Trim());
                    mruList.SaveKey();
                }
            }
        }

        /// <summary>
        /// Perform the client search operation
        /// </summary>
        /// <returns>A dialog result for the search.</returns>
        public DialogResult PerformClientSearch()
        {
            return ShowDialog();
        }

        /// <summary>
        /// Perform the client search operation
        /// </summary>
        /// <param name="owner">Owner window for the dialogs</param>
        /// <returns>A dialog result for the search.</returns>
        public DialogResult PerformClientSearch(IWin32Window owner)
        {
            return ShowDialog(owner);
        }
    }
}