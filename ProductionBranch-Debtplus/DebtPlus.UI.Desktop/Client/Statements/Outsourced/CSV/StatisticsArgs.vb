#Region "Copyright 2000-2012 DebtPlus, L.L.C."

'{*******************************************************************}
'{                                                                   }
'{       DebtPlus Debt Management System                             }
'{                                                                   }
'{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
'{       ALL RIGHTS RESERVED                                         }
'{                                                                   }
'{   The entire contents of this file is protected by U.S. and       }
'{   International Copyright Laws. Unauthorized reproduction,        }
'{   reverse-engineering, and distribution of all or any portion of  }
'{   the code contained in this file is strictly prohibited and may  }
'{   result in severe civil and criminal penalties and will be       }
'{   prosecuted to the maximum extent possible under the law.        }
'{                                                                   }
'{   RESTRICTIONS                                                    }
'{                                                                   }
'{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
'{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
'{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
'{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
'{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
'{                                                                   }
'{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
'{   ADDITIONAL RESTRICTIONS.                                        }
'{                                                                   }
'{*******************************************************************}

#End Region

Option Compare Binary
Option Explicit On
Option Strict On

Namespace Client.Statements.Outsourced.CSV
    Friend Class StatisticsArgs
        Inherits EventArgs

        ''' <summary>
        ''' Create an instance of this class
        ''' </summary>
        Public Sub New(ByVal client As Int32, ByVal TotalClientCount As Int32, ByVal CurrentClientCount As Int32,
                       ByVal output_line_count As Int32)
            MyBase.New()
            _client = client
            _TotalClientCount = TotalClientCount
            _CurrentClientCount = CurrentClientCount
            _ProcessedClientCount = CurrentClientCount
            _output_line_count = output_line_count
        End Sub

        Public Sub New(ByVal client As Int32, ByVal TotalClientCount As Int32, ByVal CurrentClientCount As Int32,
                       ByVal ProcessedClientCount As Int32, ByVal output_line_count As Int32)
            MyBase.New()
            _client = client
            _TotalClientCount = TotalClientCount
            _CurrentClientCount = CurrentClientCount
            _ProcessedClientCount = ProcessedClientCount
            _output_line_count = output_line_count
        End Sub

        ''' <summary>
        ''' Client ID number
        ''' </summary>
        Private _client As Int32

        Public ReadOnly Property client() As Int32
            Get
                Return _client
            End Get
        End Property

        ''' <summary>
        ''' Number of clients in the table to be processed
        ''' </summary>
        Private _TotalClientCount As Int32

        Public ReadOnly Property TotalClientCount() As Int32
            Get
                Return _TotalClientCount
            End Get
        End Property

        ''' <summary>
        ''' Number of clients in the table that have been processed
        ''' </summary>
        Private _CurrentClientCount As Int32

        Public ReadOnly Property CurrentClientCount() As Int32
            Get
                Return _CurrentClientCount
            End Get
        End Property

        ''' <summary>
        ''' Number of clients in the table that have been processed
        ''' </summary>
        Private _ProcessedClientCount As Int32

        Public ReadOnly Property ProcessedClientCount() As Int32
            Get
                Return _ProcessedClientCount
            End Get
        End Property

        ''' <summary>
        ''' Number of lines written to the output file
        ''' </summary>
        Private _output_line_count As Int32

        Public ReadOnly Property output_line_count() As Int32
            Get
                Return _output_line_count
            End Get
        End Property
    End Class
End Namespace
