#Region "Copyright 2000-2012 DebtPlus, L.L.C."

'{*******************************************************************}
'{                                                                   }
'{       DebtPlus Debt Management System                             }
'{                                                                   }
'{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
'{       ALL RIGHTS RESERVED                                         }
'{                                                                   }
'{   The entire contents of this file is protected by U.S. and       }
'{   International Copyright Laws. Unauthorized reproduction,        }
'{   reverse-engineering, and distribution of all or any portion of  }
'{   the code contained in this file is strictly prohibited and may  }
'{   result in severe civil and criminal penalties and will be       }
'{   prosecuted to the maximum extent possible under the law.        }
'{                                                                   }
'{   RESTRICTIONS                                                    }
'{                                                                   }
'{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
'{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
'{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
'{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
'{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
'{                                                                   }
'{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
'{   ADDITIONAL RESTRICTIONS.                                        }
'{                                                                   }
'{*******************************************************************}

#End Region

Option Compare Binary
Option Explicit On
Option Strict On

Imports System.Data.SqlClient
Imports DevExpress.Utils
Imports System.Windows.Forms

Namespace Client.Statements.Outsourced.CSV
    Friend Class MonthlyStatement
        Inherits Statement

        Protected TotalClientCount As Int32
        Protected ExpectedClientCount As Int32
        Protected TotalDebtCount As Int32
        Protected StatementDetailsDataReader As SqlDataReader

        ''' <summary>
        ''' Create an instance of our class
        ''' </summary>
        Public Sub New(ByVal ap As OutsourceArgParser)
            MyBase.new(ap)
        End Sub

        ''' <summary>
        ''' Read the data records from the database
        ''' </summary>
        Protected Overridable Function FillDataSet() As Boolean
            Dim answer As Boolean = False
            Dim tbl As DataTable

            Using dlg As New WaitDialogForm()
                Dim cn As SqlConnection = New SqlConnection(DebtPlus.LINQ.SQLInfoClass.getDefault().ConnectionString)
                Try
                    Using cm As New DebtPlus.UI.Common.CursorManager()
                        dlg.Show()

                        cn.Open()
                        dlg.SetCaption("Reading client statement information")
                        Using cmd As SqlCommand = New SqlCommand()
                            cmd.CommandTimeout = 0
                            cmd.Connection = cn
                            cmd.CommandText = ap.SelectStatement
                            cmd.Parameters.Add("@client_statement_batch", SqlDbType.Int).Value = ap.GetBatch

                            ' Ask the database to fill our local offline buffer area
                            Using da As New SqlDataAdapter(cmd)
                                da.Fill(ds, "client_statement_clients")
                            End Using
                        End Using
                    End Using

                    ' Ensure that there is a primary key to the table because we want the information sorted
                    tbl = ds.Tables("client_statement_clients")
                    tbl.PrimaryKey = New DataColumn() {tbl.Columns("client")}
                    ExpectedClientCount = tbl.Rows.Count

                    ' We are successful to this point.
                    answer = True

                Catch ex As SqlException
                    DebtPlus.UI.Common.ErrorHandling.HandleErrors(ex, "Error reading client information")

                Finally
                    dlg.Close()

                    If cn IsNot Nothing Then cn.Dispose()
                End Try
            End Using

            Return answer
        End Function

        ''' <summary>
        ''' Format the output record and write it to the output file
        ''' </summary>
        Protected Overridable Function WriteFileHeader(ByVal CurrentTime As DateTime, ByVal PeriodStart As DateTime, ByVal PeriodEnd As DateTime, ByVal StatementNote As String) As Boolean
            Return True
        End Function

        ''' <summary>
        ''' Write the header at the start of each client statement
        ''' </summary>
        Protected Overridable Function WriteStatementHeader(ByVal client As Int32, ByVal PeriodStart As DateTime, ByVal PeriodEnd As DateTime) As Boolean
            Return True
        End Function

        ''' <summary>
        ''' Generate the next client statement
        ''' </summary>
        Protected Overridable Function ProcessNextStatement(ByVal PeriodStart As DateTime, ByVal PeriodEnd As DateTime) As Boolean
            Return True
        End Function

        ''' <summary>
        ''' Write the trailer at the end of the client statement
        ''' </summary>
        Protected Overridable Function WriteStatementTrailer(ByVal DebtCount As Int32, ByVal Total_OriginalBalance As Decimal, ByVal total_CurrentDisbursement As Decimal, ByVal Total_PaymentsToDate As Decimal, ByVal Total_CurrentBalance As Decimal) As Boolean
            Return True
        End Function

        ''' <summary>
        ''' Format the output record and write it to the output file
        ''' </summary>
        Protected Overridable Function WriteFileTrailer(ByVal CurrentTime As DateTime, ByVal LineCount As Int32, ByVal ClientCount As Int32, ByVal DebtCount As Int32) As Boolean
            Return True
        End Function

        ''' <summary>
        ''' Write all records to the output file
        ''' </summary>
        Public Overrides Function WriteStatements() As Boolean
            Dim answer As Boolean = MyBase.WriteStatements()

            Dim CurrentTime As DateTime
            Dim PeriodStart As DateTime = DateTime.MinValue
            Dim PeriodEnd As DateTime = DateTime.MaxValue

            ' Retrieve the database information
            answer = FillDataSet()
            If answer Then
                If ds.Tables(0).Rows.Count <= 0 Then
                    DebtPlus.Data.Forms.MessageBox.Show("There do not seem to be any clients for this batch.", "Sorry, but there are no clients", MessageBoxButtons.OK, MessageBoxIcon.Information)
                    answer = False
                End If
            End If

            ' Open the output file
            If answer Then
                answer = CreateOutputFile()
            End If

            ' Do the work here
            Dim StatementNote As String = String.Empty
            If answer Then

                If ap.FileHeader <> String.Empty Then

                    ' Generate the standard file header
                    Dim cn As SqlConnection = New SqlConnection(DebtPlus.LINQ.SQLInfoClass.getDefault().ConnectionString)

                    Try
                        Using cm As New DebtPlus.UI.Common.CursorManager()
                            cn.Open()
                            Using cmd As New SqlCommand()
                                cmd.Connection = cn
                                cmd.CommandText = "SELECT note, statement_date, period_start, period_end FROM client_statement_batches WITH (NOLOCK) WHERE client_statement_batch=@client_statement_batch"
                                cmd.Parameters.Add("@client_statement_batch", SqlDbType.Int).Value = ap.GetBatch
                                StatementDetailsDataReader = cmd.ExecuteReader(CommandBehavior.SingleRow)
                            End Using
                        End Using

                        answer = StatementDetailsDataReader.Read
                        If answer Then
                            If Not StatementDetailsDataReader.IsDBNull(0) Then StatementNote = StatementDetailsDataReader.GetString(0).Trim()
                            If Not StatementDetailsDataReader.IsDBNull(1) Then CurrentTime = StatementDetailsDataReader.GetDateTime(1)
                            If Not StatementDetailsDataReader.IsDBNull(2) Then PeriodStart = StatementDetailsDataReader.GetDateTime(2)
                            If Not StatementDetailsDataReader.IsDBNull(3) Then PeriodEnd = StatementDetailsDataReader.GetDateTime(3)
                        End If

                        ' Close down the reader so that the conntection may be used again.
                        StatementDetailsDataReader.Close()

                        TotalDebtCount = 0
                        TotalClientCount = 0
                        If answer Then

                            ' Write the header for the file
                            answer = WriteFileHeader(CurrentTime, PeriodStart, PeriodEnd, StatementNote)

                            Using cm As New DebtPlus.UI.Common.CursorManager()
                                Using cmd As SqlCommand = New SqlCommand()
                                    cmd.Connection = cn
                                    cmd.CommandText = "SELECT creditor_name, formatted_account_number, original_balance, net_debits, credits, interest_rate, payments_to_date, current_balance, client, client_statement_batch, creditor FROM view_client_statement_details WITH (NOLOCK) WHERE client_statement_batch=@client_statement_batch AND client > 0 ORDER BY client_statement_batch, client, creditor"
                                    cmd.CommandTimeout = Math.Max(600, DebtPlus.LINQ.SQLInfoClass.getDefault().CommandTimeout)
                                    cmd.CommandType = CommandType.Text
                                    cmd.Parameters.Add("@client_statement_batch", SqlDbType.Int).Value = ap.GetBatch
                                    StatementDetailsDataReader = cmd.ExecuteReader(CommandBehavior.CloseConnection Or CommandBehavior.SingleResult)
                                End Using
                            End Using

                            If StatementDetailsDataReader IsNot Nothing AndAlso StatementDetailsDataReader.Read Then
                                Do
                                    TotalClientCount += 1
                                    RaiseStatistics(Convert.ToInt32(StatementDetailsDataReader("client")), ExpectedClientCount, TotalClientCount, TotalLineCount)
                                    If Not ProcessNextStatement(PeriodStart, PeriodEnd) Then Exit Do
                                Loop
                            End If

                            RaiseStatistics(0, TotalClientCount, TotalClientCount, TotalLineCount)
                            WriteFileTrailer(CurrentTime, TotalLineCount, TotalClientCount, TotalDebtCount)
                            CloseOutputFile()
                        End If

                    Catch ex As SqlException
                        Using gdr As New Repository.GetDataResult()
                            gdr.HandleException(ex)
                            DebtPlus.UI.Common.ErrorHandling.HandleErrors(gdr, "Error writing header information")
                        End Using

                    Finally
                        If StatementDetailsDataReader IsNot Nothing Then
                            StatementDetailsDataReader.Dispose()
                        End If

                        If cn IsNot Nothing Then
                            cn.Dispose()
                        End If
                    End Try
                End If
            End If

            Return answer
        End Function
    End Class
End Namespace
