#Region "Copyright 2000-2012 DebtPlus, L.L.C."

'{*******************************************************************}
'{                                                                   }
'{       DebtPlus Debt Management System                             }
'{                                                                   }
'{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
'{       ALL RIGHTS RESERVED                                         }
'{                                                                   }
'{   The entire contents of this file is protected by U.S. and       }
'{   International Copyright Laws. Unauthorized reproduction,        }
'{   reverse-engineering, and distribution of all or any portion of  }
'{   the code contained in this file is strictly prohibited and may  }
'{   result in severe civil and criminal penalties and will be       }
'{   prosecuted to the maximum extent possible under the law.        }
'{                                                                   }
'{   RESTRICTIONS                                                    }
'{                                                                   }
'{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
'{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
'{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
'{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
'{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
'{                                                                   }
'{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
'{   ADDITIONAL RESTRICTIONS.                                        }
'{                                                                   }
'{*******************************************************************}

#End Region

Option Explicit On
Option Strict On
Option Compare Binary

Imports DebtPlus.Data.Forms

Namespace Client.Statements.Outsourced.CSV
    Friend Class NewClientStatementBatchForm
        Inherits DebtPlusForm

        Private drv As DataRowView

        Public Sub New()
            MyBase.New()

            'This call is required by the Windows Form Designer.
            InitializeComponent()
            AddHandler Me.Load, AddressOf NewClientStatementBatchForm_Load

            'Add any initialization after the InitializeComponent() call
            With disbursement_date
                With .Properties
                    With .DisplayFormat
                        .Format = New DisbursementCycleFormatter
                        .FormatType = DevExpress.Utils.FormatType.Custom
                    End With
                End With
            End With
        End Sub


        Public Sub New(ByVal DataRow As DataRowView)
            MyClass.New()
            drv = DataRow
        End Sub

#Region " Windows Form Designer generated code "

        'Form overrides dispose to clean up the component list.
        Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
            If disposing Then
                If components IsNot Nothing Then
                    components.Dispose()
                End If
            End If
            MyBase.Dispose(disposing)
        End Sub

        'Required by the Windows Form Designer
        Private components As System.ComponentModel.IContainer

        'NOTE: The following procedure is required by the Windows Form Designer
        'It can be modified using the Windows Form Designer.  
        'Do not modify it using the code editor.
        Friend WithEvents LabelControl1 As DevExpress.XtraEditors.LabelControl
        Friend WithEvents disbursement_date As DevExpress.XtraEditors.SpinEdit
        Friend WithEvents LabelControl2 As DevExpress.XtraEditors.LabelControl
        Friend WithEvents LabelControl3 As DevExpress.XtraEditors.LabelControl
        Friend WithEvents LabelControl4 As DevExpress.XtraEditors.LabelControl
        Friend WithEvents Button_OK As DevExpress.XtraEditors.SimpleButton
        Friend WithEvents Button_Cancel As DevExpress.XtraEditors.SimpleButton
        Friend WithEvents note As DevExpress.XtraEditors.MemoEdit
        Friend WithEvents period_start As DevExpress.XtraEditors.DateEdit
        Friend WithEvents period_end As DevExpress.XtraEditors.DateEdit

        <System.Diagnostics.DebuggerStepThrough()>
        Private Sub InitializeComponent()
            Me.LabelControl1 = New DevExpress.XtraEditors.LabelControl
            Me.disbursement_date = New DevExpress.XtraEditors.SpinEdit
            Me.LabelControl2 = New DevExpress.XtraEditors.LabelControl
            Me.LabelControl3 = New DevExpress.XtraEditors.LabelControl
            Me.LabelControl4 = New DevExpress.XtraEditors.LabelControl
            Me.Button_OK = New DevExpress.XtraEditors.SimpleButton
            Me.Button_Cancel = New DevExpress.XtraEditors.SimpleButton
            Me.note = New DevExpress.XtraEditors.MemoEdit
            Me.period_start = New DevExpress.XtraEditors.DateEdit
            Me.period_end = New DevExpress.XtraEditors.DateEdit

            CType(Me.disbursement_date.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.note.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.period_start.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.period_end.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
            Me.SuspendLayout()

            '
            'LabelControl1
            '
            Me.LabelControl1.Location = New System.Drawing.Point(8, 19)
            Me.LabelControl1.Name = "LabelControl1"
            Me.LabelControl1.TabIndex = 0
            Me.LabelControl1.Text = "Group"
            '
            'disbursement_date
            '
            Me.disbursement_date.EditValue = New Decimal(New Int32() {0, 0, 0, 0})
            Me.disbursement_date.Location = New System.Drawing.Point(96, 16)
            Me.disbursement_date.Name = "disbursement_date"
            '
            'disbursement_date.Properties
            '
            Me.disbursement_date.Properties.Appearance.Options.UseTextOptions = True
            Me.disbursement_date.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
            Me.disbursement_date.Properties.Buttons.AddRange(
                New DevExpress.XtraEditors.Controls.EditorButton() _
                                                                {New DevExpress.XtraEditors.Controls.EditorButton})
            Me.disbursement_date.Properties.IsFloatValue = False
            Me.disbursement_date.Properties.Mask.BeepOnError = True
            Me.disbursement_date.Properties.Mask.EditMask = "f0"
            Me.disbursement_date.Properties.MaxLength = 2
            Me.disbursement_date.Properties.MaxValue = New Decimal(New Int32() {31, 0, 0, 0})
            Me.disbursement_date.TabIndex = 1
            '
            'LabelControl2
            '
            Me.LabelControl2.Location = New System.Drawing.Point(8, 43)
            Me.LabelControl2.Name = "LabelControl2"
            Me.LabelControl2.TabIndex = 2
            Me.LabelControl2.Text = "Start Date"
            '
            'LabelControl3
            '
            Me.LabelControl3.Location = New System.Drawing.Point(8, 67)
            Me.LabelControl3.Name = "LabelControl3"
            Me.LabelControl3.TabIndex = 4
            Me.LabelControl3.Text = "End Date"
            '
            'LabelControl4
            '
            Me.LabelControl4.Location = New System.Drawing.Point(8, 91)
            Me.LabelControl4.Name = "LabelControl4"
            Me.LabelControl4.TabIndex = 6
            Me.LabelControl4.Text = "Note"
            '
            'Button_OK
            '
            Me.Button_OK.Anchor = System.Windows.Forms.AnchorStyles.Bottom
            Me.Button_OK.DialogResult = System.Windows.Forms.DialogResult.OK
            Me.Button_OK.Location = New System.Drawing.Point(112, 200)
            Me.Button_OK.Name = "Button_OK"
            Me.Button_OK.TabIndex = 8
            Me.Button_OK.Text = "&OK"
            '
            'Button_Cancel
            '
            Me.Button_Cancel.Anchor = System.Windows.Forms.AnchorStyles.Bottom
            Me.Button_Cancel.DialogResult = System.Windows.Forms.DialogResult.Cancel
            Me.Button_Cancel.Location = New System.Drawing.Point(208, 200)
            Me.Button_Cancel.Name = "Button_Cancel"
            Me.Button_Cancel.TabIndex = 9
            Me.Button_Cancel.Text = "&Cancel"
            '
            'note
            '
            Me.note.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                                     Or System.Windows.Forms.AnchorStyles.Left) _
                                    Or System.Windows.Forms.AnchorStyles.Right), 
                                   System.Windows.Forms.AnchorStyles)
            Me.note.Location = New System.Drawing.Point(96, 88)
            Me.note.Name = "note"
            Me.note.Size = New System.Drawing.Size(280, 96)
            Me.note.TabIndex = 7
            '
            'period_start
            '
            Me.period_start.EditValue = Nothing
            Me.period_start.Location = New System.Drawing.Point(96, 40)
            Me.period_start.Name = "period_start"
            '
            'period_start.Properties
            '
            Me.period_start.Properties.Buttons.AddRange(
                New DevExpress.XtraEditors.Controls.EditorButton() _
                                                           {New DevExpress.XtraEditors.Controls.EditorButton(
                                                               DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
            Me.period_start.TabIndex = 3
            '
            'period_end
            '
            Me.period_end.EditValue = Nothing
            Me.period_end.Location = New System.Drawing.Point(96, 64)
            Me.period_end.Name = "period_end"
            '
            'period_end.Properties
            '
            Me.period_end.Properties.Buttons.AddRange(
                New DevExpress.XtraEditors.Controls.EditorButton() _
                                                         {New DevExpress.XtraEditors.Controls.EditorButton(
                                                             DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
            Me.period_end.TabIndex = 5
            '
            'NewClientStatementBatchForm
            '
            Me.AcceptButton = Me.Button_OK
            Me.AutoScaleBaseSize = New System.Drawing.Size(5, 14)
            Me.CancelButton = Me.Button_Cancel
            Me.ClientSize = New System.Drawing.Size(392, 238)
            Me.Controls.Add(Me.period_end)
            Me.Controls.Add(Me.period_start)
            Me.Controls.Add(Me.note)
            Me.Controls.Add(Me.Button_Cancel)
            Me.Controls.Add(Me.Button_OK)
            Me.Controls.Add(Me.LabelControl4)
            Me.Controls.Add(Me.LabelControl3)
            Me.Controls.Add(Me.LabelControl2)
            Me.Controls.Add(Me.disbursement_date)
            Me.Controls.Add(Me.LabelControl1)
            Me.Name = "NewClientStatementBatchForm"
            Me.ToolTipController1.SetSuperTip(Me, Nothing)
            Me.Text = "NewClientStatementBatchForm"

            CType(Me.disbursement_date.Properties, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.note.Properties, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.period_start.Properties, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.period_end.Properties, System.ComponentModel.ISupportInitialize).EndInit()
            Me.ResumeLayout(False)
        End Sub

#End Region

        Private Sub NewClientStatementBatchForm_Load(ByVal sender As Object, ByVal e As EventArgs)

            ' IF this is a new row then supply the default values
            If drv.IsNew Then
                Dim StartingDate As Date = New Date(Now.Year, Now.Month, 1).AddMonths(-1)
                Dim EndingDate As Date = New Date(Now.Year, Now.Month, 1).AddSeconds(-1)
                drv("disbursement_date") = 0
                drv("period_start") = StartingDate
                drv("period_end") = EndingDate
                drv("note") = String.Empty
            End If

            ' Bind the record to the controls
            disbursement_date.DataBindings.Add("EditValue", drv, "disbursement_date")
            period_start.DataBindings.Add("EditValue", drv, "period_start")
            period_end.DataBindings.Add("EditValue", drv, "period_end")
            note.DataBindings.Add("EditValue", drv, "note")
        End Sub

        Private Class DisbursementCycleFormatter
            Implements ICustomFormatter
            Implements IFormatProvider

            Public Function Format(ByVal formatString As String, ByVal arg As Object,
                                   ByVal formatProvider As IFormatProvider) As String Implements ICustomFormatter.Format
                Dim value As Int32 = DebtPlus.Utils.Nulls.DInt(arg)
                If value = 0 Then Return "All"
                Return value.ToString()
            End Function

            Public Function GetFormat(ByVal formatType As Type) As Object Implements IFormatProvider.GetFormat
                Return Me
            End Function
        End Class
    End Class
End Namespace
