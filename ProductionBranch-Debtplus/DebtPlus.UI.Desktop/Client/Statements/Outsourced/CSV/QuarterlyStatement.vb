#Region "Copyright 2000-2012 DebtPlus, L.L.C."

'{*******************************************************************}
'{                                                                   }
'{       DebtPlus Debt Management System                             }
'{                                                                   }
'{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
'{       ALL RIGHTS RESERVED                                         }
'{                                                                   }
'{   The entire contents of this file is protected by U.S. and       }
'{   International Copyright Laws. Unauthorized reproduction,        }
'{   reverse-engineering, and distribution of all or any portion of  }
'{   the code contained in this file is strictly prohibited and may  }
'{   result in severe civil and criminal penalties and will be       }
'{   prosecuted to the maximum extent possible under the law.        }
'{                                                                   }
'{   RESTRICTIONS                                                    }
'{                                                                   }
'{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
'{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
'{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
'{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
'{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
'{                                                                   }
'{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
'{   ADDITIONAL RESTRICTIONS.                                        }
'{                                                                   }
'{*******************************************************************}

#End Region

Option Compare Binary
Option Explicit On
Option Strict On

Imports System.Data.SqlClient
Imports DevExpress.Utils
Imports System.Windows.Forms

Namespace Client.Statements.Outsourced.CSV
    Friend Class QuarterlyStatement
        Inherits Statement

        Protected TotalClientCount As Int32 = 0
        Protected ExpectedClientCount As Int32 = 0
        Protected TotalDebtCount As Int32 = 0
        Protected StatementDetailsDataReader As SqlDataReader = Nothing

        ''' <summary>
        ''' Create an instance of our class
        ''' </summary>
        Public Sub New(ByVal ap As OutsourceArgParser)
            MyBase.new(ap)
        End Sub

        ''' <summary>
        ''' Read the data records from the database
        ''' </summary>
        Protected Overridable Function FillDataSet() As Boolean
            Dim answer As Boolean = False

            Dim cn As SqlConnection = New SqlConnection(DebtPlus.LINQ.SQLInfoClass.getDefault().ConnectionString)
            Dim current_cursor As Cursor = Cursor.Current
            Dim dlg As New WaitDialogForm
            Dim tbl As DataTable

            Try
                Cursor.Current = Cursors.WaitCursor
                dlg.Show()

                cn.Open()
                dlg.SetCaption("Reading statement batch information")
                Using cmd As SqlCommand = New SqlCommand
                    With cmd
                        .Connection = cn
                        .CommandText = "SELECT [client_statement_batch],[type],[disbursement_date],[note],[statement_date],[period_start],[period_end],[quarter_1_batch],[quarter_2_batch],[quarter_3_batch],[date_printed],[printed_by],[date_created],[created_by] FROM client_statement_batches WHERE client_statement_batch IN (@batch_0,@batch_1,@batch_2)"
                        .Parameters.Add("@batch_0", SqlDbType.Int).Value = ap.GetBatch(0)
                        .Parameters.Add("@batch_1", SqlDbType.Int).Value = ap.GetBatch(1)
                        .Parameters.Add("@batch_2", SqlDbType.Int).Value = ap.GetBatch(2)
                    End With

                    ' Ask the database to fill our local offline buffer area
                    Using da As New SqlDataAdapter(cmd)
                        da.Fill(ds, "client_statement_batches")
                    End Using
                End Using

                ' Ensure that there is a primary key to the table because we want the information sorted
                tbl = ds.Tables("client_statement_batches")
                With tbl
                    .PrimaryKey = New DataColumn() {.Columns("client_statement_batch")}
                End With

                ' Set the batch IDs in the proper sequence for processing. We need them in the order of date ranges
                Using _
                    vue As _
                        New DataView(tbl, String.Empty, "period_start, period_end, client_statement_batch",
                                     DataViewRowState.CurrentRows)
                    For Each drv As DataRowView In vue
                        ap.SetBatch(0, ap.GetBatch(1))
                        ap.SetBatch(1, ap.GetBatch(2))
                        ap.SetBatch(2, Convert.ToInt32(drv("client_statement_batch")))
                    Next
                End Using

                ' Read the client information
                dlg.SetCaption("Reading client statement information")
                Using cmd As SqlCommand = New SqlCommand
                    With cmd
                        .CommandTimeout = System.Math.Max(600, DebtPlus.LINQ.SQLInfoClass.getDefault().CommandTimeout)
                        .Connection = cn
                        .CommandText =
                            "SELECT [client_statement_batch],[client],[last_deposit_date],[last_deposit_amt],[deposit_amt],[refund_amt],[disbursement_amt],[held_in_trust],[reserved_in_trust],[starting_trust_balance],[counselor],[office],[expected_deposit_date],[expected_deposit_amt],[delivery_method],[delivery_date],[active_status],[active_debts],[counselor_name],[office_name],[address_1],[address_2],[address_3],[address_4],[postalcode],[company_id],[mail_error_date],[ElectronicStatements],[ElectronicCorrespondence] FROM view_client_statement_clients WITH (NOLOCK) WHERE mail_error_date IS NULL AND client_statement_batch IN (@batch_0,@batch_1,@batch_2)"
                        .Parameters.Add("@batch_0", SqlDbType.Int).Value = ap.GetBatch(0)
                        .Parameters.Add("@batch_1", SqlDbType.Int).Value = ap.GetBatch(1)
                        .Parameters.Add("@batch_2", SqlDbType.Int).Value = ap.GetBatch(2)
                    End With

                    ' Ask the database to fill our local offline buffer area
                    Using da As New SqlDataAdapter(cmd)
                        da.Fill(ds, "client_statement_clients")
                    End Using
                End Using

                ' Ensure that there is a primary key to the table because we want the information sorted
                tbl = ds.Tables("client_statement_clients")
                With tbl
                    .PrimaryKey = New DataColumn() {.Columns("client"), .Columns("client_statement_batch")}
                End With

                ' We are successful to this point.
                answer = True

            Catch ex As SqlException
                dlg.Close()
                dlg = Nothing
                DebtPlus.UI.Common.ErrorHandling.HandleErrors(ex, "Error reading client information")

            Finally
                If dlg IsNot Nothing Then
                    dlg.Close()
                    dlg = Nothing
                End If

                If cn IsNot Nothing Then cn.Dispose()

                Cursor.Current = current_cursor
            End Try

            Return answer
        End Function

        ''' <summary>
        ''' Format the output record and write it to the output file
        ''' </summary>
        Protected Overridable Function WriteFileHeader(ByVal Batch1 As DataRow, ByVal Batch2 As DataRow,
                                                       ByVal Batch3 As DataRow) As Boolean
            Return True
        End Function

        ''' <summary>
        ''' Write the header at the start of each client statement
        ''' </summary>
        Protected Overridable Function WriteStatementHeader(ByVal client As Int32, ByVal PeriodStart As DateTime,
                                                            ByVal PeriodEnd As DateTime) As Boolean
            Return True
        End Function

        ''' <summary>
        ''' Generate the next client statement
        ''' </summary>
        Protected Overridable Function ProcessNextStatement(ByVal PeriodStart As DateTime, ByVal PeriodEnd As DateTime,
                                                            ByVal Batch_0 As Int32, ByVal Batch_1 As Int32,
                                                            ByVal Batch_2 As Int32) As Boolean
            Return True
        End Function

        ''' <summary>
        ''' Write the trailer at the end of the client statement
        ''' </summary>
        Protected Overridable Function WriteStatementTrailer(ByVal DebtCount As Int32,
                                                             ByVal Total_OriginalBalance As Decimal,
                                                             ByVal total_CurrentDisbursement As Decimal,
                                                             ByVal Total_PaymentsToDate As Decimal,
                                                             ByVal Total_CurrentBalance As Decimal) As Boolean
            Return True
        End Function

        ''' <summary>
        ''' Format the output record and write it to the output file
        ''' </summary>
        Protected Overridable Function WriteFileTrailer(ByVal CurrentTime As DateTime, ByVal LineCount As Int32,
                                                        ByVal ClientCount As Int32, ByVal DebtCount As Int32) As Boolean
            Return True
        End Function

        ''' <summary>
        ''' Write all records to the output file
        ''' </summary>
        Public Overrides Function WriteStatements() As Boolean
            Dim answer As Boolean = MyBase.WriteStatements()

            ' Retrieve the database information
            answer = FillDataSet()
            If answer Then
                If ds.Tables(0).Rows.Count <= 0 Then
                    DebtPlus.Data.Forms.MessageBox.Show("There do not seem to be any clients for this batch.", "Sorry, but there are no clients",
                                    MessageBoxButtons.OK, MessageBoxIcon.Information)
                    answer = False
                End If
            End If

            ' Open the output file
            If answer Then
                answer = CreateOutputFile()
            End If

            ' Do the work here
            Dim StatementNote As String = String.Empty
            If answer Then

                ' Find the number of clients for the marquee
                ExpectedClientCount = DebtPlus.Utils.Nulls.DInt(ds.Tables("client_statement_clients").Compute("count(client)", String.Empty))

                ' Identify the batch numbers
                Dim Batch_0 As Int32 = ap.GetBatch(0)
                Dim Batch_1 As Int32 = ap.GetBatch(1)
                Dim Batch_2 As Int32 = ap.GetBatch(2)

                ' Find the period starting and ending dates from the batches
                Dim PeriodStart As DateTime =
                        Convert.ToDateTime(ds.Tables("client_statement_batches").Rows.Find(Batch_0)("period_start"))
                Dim PeriodEnd As DateTime =
                        Convert.ToDateTime(ds.Tables("client_statement_batches").Rows.Find(Batch_2)("period_end"))
                Dim CurrentTime As DateTime =
                        Convert.ToDateTime(ds.Tables("client_statement_batches").Rows.Find(Batch_2)("statement_date"))

                ' Generate the standard file header
                Dim cn As SqlConnection = New SqlConnection(DebtPlus.LINQ.SQLInfoClass.getDefault().ConnectionString)
                Dim current_cursor As Cursor = Cursor.Current

                Try
                    Cursor.Current = Cursors.WaitCursor
                    cn.Open()

                    TotalDebtCount = 0
                    TotalClientCount = 0

                    ' Write the header for the file
                    If ap.FileHeader <> String.Empty Then
                        Dim tbl As DataTable = ds.Tables("client_statement_batches")
                        answer = WriteFileHeader(tbl.Rows.Find(Batch_0), tbl.Rows.Find(Batch_1), tbl.Rows.Find(Batch_2))
                    End If

                    Using cmd As SqlCommand = New SqlCommand
                        With cmd
                            .Connection = cn
                            .CommandText = "SELECT creditor_name, formatted_account_number, original_balance, net_debits, credits, interest_rate, payments_to_date, current_balance, client, client_statement_batch, creditor, client_creditor FROM view_client_statement_details WITH (NOLOCK) WHERE client_statement_batch IN (@batch_0, @batch_1, @batch_2) AND client > 0 ORDER BY client, client_creditor, client_statement_batch"
                            .CommandType = CommandType.Text
                            .Parameters.Add("@batch_0", SqlDbType.Int).Value = Batch_0
                            .Parameters.Add("@batch_1", SqlDbType.Int).Value = Batch_1
                            .Parameters.Add("@batch_2", SqlDbType.Int).Value = Batch_2
                            StatementDetailsDataReader =
                                .ExecuteReader(CommandBehavior.CloseConnection Or CommandBehavior.SingleResult)
                        End With
                    End Using

                    If StatementDetailsDataReader IsNot Nothing AndAlso StatementDetailsDataReader.Read Then
                        Do
                            TotalClientCount += 1
                            RaiseStatistics(Convert.ToInt32(StatementDetailsDataReader("client")), ExpectedClientCount,
                                            TotalClientCount, TotalClientCount * 3, TotalLineCount)
                            If Not ProcessNextStatement(PeriodStart, PeriodEnd, Batch_0, Batch_1, Batch_2) Then Exit Do
                        Loop
                    End If

                    RaiseStatistics(0, TotalClientCount, TotalClientCount, TotalLineCount)
                    If ap.FileTrailer <> String.Empty Then
                        WriteFileTrailer(CurrentTime, TotalLineCount, TotalClientCount, TotalDebtCount)
                    End If
                    CloseOutputFile()

                Catch ex As SqlException
                    DebtPlus.UI.Common.ErrorHandling.HandleErrors(ex, "Error processing client statements")

                Finally
                    If StatementDetailsDataReader IsNot Nothing Then StatementDetailsDataReader.Dispose()
                    If cn IsNot Nothing Then cn.Dispose()
                    Cursor.Current = current_cursor
                End Try
            End If

            Return answer
        End Function
    End Class
End Namespace
