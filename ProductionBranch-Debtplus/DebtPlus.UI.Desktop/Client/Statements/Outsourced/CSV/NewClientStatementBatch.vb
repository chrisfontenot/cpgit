#Region "Copyright 2000-2012 DebtPlus, L.L.C."

'{*******************************************************************}
'{                                                                   }
'{       DebtPlus Debt Management System                             }
'{                                                                   }
'{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
'{       ALL RIGHTS RESERVED                                         }
'{                                                                   }
'{   The entire contents of this file is protected by U.S. and       }
'{   International Copyright Laws. Unauthorized reproduction,        }
'{   reverse-engineering, and distribution of all or any portion of  }
'{   the code contained in this file is strictly prohibited and may  }
'{   result in severe civil and criminal penalties and will be       }
'{   prosecuted to the maximum extent possible under the law.        }
'{                                                                   }
'{   RESTRICTIONS                                                    }
'{                                                                   }
'{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
'{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
'{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
'{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
'{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
'{                                                                   }
'{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
'{   ADDITIONAL RESTRICTIONS.                                        }
'{                                                                   }
'{*******************************************************************}

#End Region
Imports DebtPlus.UI.Client.Widgets.Controls
Imports System.Data.SqlClient
Imports System.Windows.Forms

Namespace Client.Statements.Outsourced.CSV
    Public Class NewClientStatementBatch
        Inherits ClientStatementBatch

        Public Sub New()
            MyBase.New()
            InitializeComponent()
            AddHandler Button_New.Click, AddressOf Button_New_Click
        End Sub

#Region " Windows Form Designer generated code "

        'Form overrides dispose to clean up the component list.
        Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
            If disposing Then
                If components IsNot Nothing Then
                    components.Dispose()
                End If
            End If
            MyBase.Dispose(disposing)
        End Sub

        'Required by the Windows Form Designer
        Private components As System.ComponentModel.IContainer

        'NOTE: The following procedure is required by the Windows Form Designer
        'It can be modified using the Windows Form Designer.  
        'Do not modify it using the code editor.
        Friend WithEvents Button_New As DevExpress.XtraEditors.SimpleButton

        <System.Diagnostics.DebuggerStepThrough()>
        Private Sub InitializeComponent()
            Me.Button_New = New DevExpress.XtraEditors.SimpleButton
            CType(Me.RepositoryItemMemoExEdit1, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.GridView1, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.GridControl1, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.RepositoryItemMemoEdit1, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.ds, System.ComponentModel.ISupportInitialize).BeginInit()
            Me.SuspendLayout()
            '
            'GridColumn_period_start
            '
            Me.GridColumn_period_start.AppearanceCell.Options.UseTextOptions = True
            Me.GridColumn_period_start.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
            Me.GridColumn_period_start.AppearanceHeader.Options.UseTextOptions = True
            Me.GridColumn_period_start.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
            Me.GridColumn_period_start.DisplayFormat.FormatString = "d"
            Me.GridColumn_period_start.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime
            Me.GridColumn_period_start.GroupFormat.FormatString = "d"
            Me.GridColumn_period_start.GroupFormat.FormatType = DevExpress.Utils.FormatType.DateTime
            Me.GridColumn_period_start.OptionsColumn.AllowEdit = False
            '
            'GridColumn_client_statement_batch
            '
            Me.GridColumn_client_statement_batch.AppearanceCell.Options.UseTextOptions = True
            Me.GridColumn_client_statement_batch.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
            Me.GridColumn_client_statement_batch.AppearanceHeader.Options.UseTextOptions = True
            Me.GridColumn_client_statement_batch.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
            Me.GridColumn_client_statement_batch.DisplayFormat.FormatString = "f0"
            Me.GridColumn_client_statement_batch.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
            Me.GridColumn_client_statement_batch.GroupFormat.FormatString = "f0"
            Me.GridColumn_client_statement_batch.GroupFormat.FormatType = DevExpress.Utils.FormatType.Numeric
            Me.GridColumn_client_statement_batch.OptionsColumn.AllowEdit = False
            '
            'GridView1
            '
            Me.GridView1.Appearance.ColumnFilterButton.BackColor = System.Drawing.Color.FromArgb(CType(104, Byte),
                                                                                                 CType(184, Byte),
                                                                                                 CType(251, Byte))
            Me.GridView1.Appearance.ColumnFilterButton.BorderColor = System.Drawing.Color.FromArgb(CType(104, Byte),
                                                                                                   CType(184, Byte),
                                                                                                   CType(251, Byte))
            Me.GridView1.Appearance.ColumnFilterButton.ForeColor = System.Drawing.Color.White
            Me.GridView1.Appearance.ColumnFilterButton.Options.UseBackColor = True
            Me.GridView1.Appearance.ColumnFilterButton.Options.UseBorderColor = True
            Me.GridView1.Appearance.ColumnFilterButton.Options.UseForeColor = True
            Me.GridView1.Appearance.ColumnFilterButtonActive.BackColor = System.Drawing.Color.FromArgb(CType(170, Byte),
                                                                                                       CType(216, Byte),
                                                                                                       CType(254, Byte))
            Me.GridView1.Appearance.ColumnFilterButtonActive.BorderColor = System.Drawing.Color.FromArgb(CType(170, Byte),
                                                                                                         CType(216, Byte),
                                                                                                         CType(254, Byte))
            Me.GridView1.Appearance.ColumnFilterButtonActive.ForeColor = System.Drawing.Color.Black
            Me.GridView1.Appearance.ColumnFilterButtonActive.Options.UseBackColor = True
            Me.GridView1.Appearance.ColumnFilterButtonActive.Options.UseBorderColor = True
            Me.GridView1.Appearance.ColumnFilterButtonActive.Options.UseForeColor = True
            Me.GridView1.Appearance.Empty.BackColor = System.Drawing.Color.FromArgb(CType(236, Byte), CType(246, Byte),
                                                                                    CType(255, Byte))
            Me.GridView1.Appearance.Empty.BackColor2 = System.Drawing.Color.White
            Me.GridView1.Appearance.Empty.Options.UseBackColor = True
            Me.GridView1.Appearance.EvenRow.BackColor = System.Drawing.Color.FromArgb(CType(247, Byte), CType(251, Byte),
                                                                                      CType(255, Byte))
            Me.GridView1.Appearance.EvenRow.BorderColor = System.Drawing.Color.FromArgb(CType(247, Byte), CType(251, Byte),
                                                                                        CType(255, Byte))
            Me.GridView1.Appearance.EvenRow.ForeColor = System.Drawing.Color.Black
            Me.GridView1.Appearance.EvenRow.Options.UseBackColor = True
            Me.GridView1.Appearance.EvenRow.Options.UseBorderColor = True
            Me.GridView1.Appearance.EvenRow.Options.UseForeColor = True
            Me.GridView1.Appearance.FilterCloseButton.BackColor = System.Drawing.Color.FromArgb(CType(104, Byte),
                                                                                                CType(184, Byte),
                                                                                                CType(251, Byte))
            Me.GridView1.Appearance.FilterCloseButton.BorderColor = System.Drawing.Color.FromArgb(CType(104, Byte),
                                                                                                  CType(184, Byte),
                                                                                                  CType(251, Byte))
            Me.GridView1.Appearance.FilterCloseButton.ForeColor = System.Drawing.Color.White
            Me.GridView1.Appearance.FilterCloseButton.Options.UseBackColor = True
            Me.GridView1.Appearance.FilterCloseButton.Options.UseBorderColor = True
            Me.GridView1.Appearance.FilterCloseButton.Options.UseForeColor = True
            Me.GridView1.Appearance.FilterPanel.BackColor = System.Drawing.Color.FromArgb(CType(236, Byte), CType(246, Byte),
                                                                                          CType(255, Byte))
            Me.GridView1.Appearance.FilterPanel.BackColor2 = System.Drawing.Color.White
            Me.GridView1.Appearance.FilterPanel.ForeColor = System.Drawing.Color.Black
            Me.GridView1.Appearance.FilterPanel.Options.UseBackColor = True
            Me.GridView1.Appearance.FilterPanel.Options.UseForeColor = True
            Me.GridView1.Appearance.FixedLine.BackColor = System.Drawing.Color.FromArgb(CType(59, Byte), CType(133, Byte),
                                                                                        CType(195, Byte))
            Me.GridView1.Appearance.FixedLine.Options.UseBackColor = True
            Me.GridView1.Appearance.FocusedCell.BackColor = System.Drawing.Color.White
            Me.GridView1.Appearance.FocusedCell.ForeColor = System.Drawing.Color.Black
            Me.GridView1.Appearance.FocusedCell.Options.UseBackColor = True
            Me.GridView1.Appearance.FocusedCell.Options.UseForeColor = True
            Me.GridView1.Appearance.FocusedRow.BackColor = System.Drawing.Color.FromArgb(CType(38, Byte), CType(109, Byte),
                                                                                         CType(189, Byte))
            Me.GridView1.Appearance.FocusedRow.BorderColor = System.Drawing.Color.FromArgb(CType(59, Byte), CType(139, Byte),
                                                                                           CType(206, Byte))
            Me.GridView1.Appearance.FocusedRow.ForeColor = System.Drawing.Color.White
            Me.GridView1.Appearance.FocusedRow.Options.UseBackColor = True
            Me.GridView1.Appearance.FocusedRow.Options.UseBorderColor = True
            Me.GridView1.Appearance.FocusedRow.Options.UseForeColor = True
            Me.GridView1.Appearance.FooterPanel.BackColor = System.Drawing.Color.FromArgb(CType(104, Byte), CType(184, Byte),
                                                                                          CType(251, Byte))
            Me.GridView1.Appearance.FooterPanel.BorderColor = System.Drawing.Color.FromArgb(CType(104, Byte),
                                                                                            CType(184, Byte),
                                                                                            CType(251, Byte))
            Me.GridView1.Appearance.FooterPanel.ForeColor = System.Drawing.Color.Black
            Me.GridView1.Appearance.FooterPanel.Options.UseBackColor = True
            Me.GridView1.Appearance.FooterPanel.Options.UseBorderColor = True
            Me.GridView1.Appearance.FooterPanel.Options.UseForeColor = True
            Me.GridView1.Appearance.GroupButton.BackColor = System.Drawing.Color.FromArgb(CType(104, Byte), CType(184, Byte),
                                                                                          CType(251, Byte))
            Me.GridView1.Appearance.GroupButton.BorderColor = System.Drawing.Color.FromArgb(CType(104, Byte),
                                                                                            CType(184, Byte),
                                                                                            CType(251, Byte))
            Me.GridView1.Appearance.GroupButton.Options.UseBackColor = True
            Me.GridView1.Appearance.GroupButton.Options.UseBorderColor = True
            Me.GridView1.Appearance.GroupFooter.BackColor = System.Drawing.Color.FromArgb(CType(170, Byte), CType(216, Byte),
                                                                                          CType(254, Byte))
            Me.GridView1.Appearance.GroupFooter.BorderColor = System.Drawing.Color.FromArgb(CType(170, Byte),
                                                                                            CType(216, Byte),
                                                                                            CType(254, Byte))
            Me.GridView1.Appearance.GroupFooter.ForeColor = System.Drawing.Color.Black
            Me.GridView1.Appearance.GroupFooter.Options.UseBackColor = True
            Me.GridView1.Appearance.GroupFooter.Options.UseBorderColor = True
            Me.GridView1.Appearance.GroupFooter.Options.UseForeColor = True
            Me.GridView1.Appearance.GroupPanel.BackColor = System.Drawing.Color.FromArgb(CType(236, Byte), CType(246, Byte),
                                                                                         CType(255, Byte))
            Me.GridView1.Appearance.GroupPanel.BackColor2 = System.Drawing.Color.White
            Me.GridView1.Appearance.GroupPanel.ForeColor = System.Drawing.Color.Black
            Me.GridView1.Appearance.GroupPanel.Options.UseBackColor = True
            Me.GridView1.Appearance.GroupPanel.Options.UseForeColor = True
            Me.GridView1.Appearance.GroupRow.BackColor = System.Drawing.Color.FromArgb(CType(170, Byte), CType(216, Byte),
                                                                                       CType(254, Byte))
            Me.GridView1.Appearance.GroupRow.BorderColor = System.Drawing.Color.FromArgb(CType(170, Byte), CType(216, Byte),
                                                                                         CType(254, Byte))
            Me.GridView1.Appearance.GroupRow.ForeColor = System.Drawing.Color.Black
            Me.GridView1.Appearance.GroupRow.Options.UseBackColor = True
            Me.GridView1.Appearance.GroupRow.Options.UseBorderColor = True
            Me.GridView1.Appearance.GroupRow.Options.UseForeColor = True
            Me.GridView1.Appearance.HeaderPanel.BackColor = System.Drawing.Color.FromArgb(CType(139, Byte), CType(201, Byte),
                                                                                          CType(254, Byte))
            Me.GridView1.Appearance.HeaderPanel.BorderColor = System.Drawing.Color.FromArgb(CType(139, Byte),
                                                                                            CType(201, Byte),
                                                                                            CType(254, Byte))
            Me.GridView1.Appearance.HeaderPanel.ForeColor = System.Drawing.Color.Black
            Me.GridView1.Appearance.HeaderPanel.Options.UseBackColor = True
            Me.GridView1.Appearance.HeaderPanel.Options.UseBorderColor = True
            Me.GridView1.Appearance.HeaderPanel.Options.UseForeColor = True
            Me.GridView1.Appearance.HideSelectionRow.BackColor = System.Drawing.Color.FromArgb(CType(105, Byte),
                                                                                               CType(170, Byte),
                                                                                               CType(225, Byte))
            Me.GridView1.Appearance.HideSelectionRow.BorderColor = System.Drawing.Color.FromArgb(CType(83, Byte),
                                                                                                 CType(155, Byte),
                                                                                                 CType(215, Byte))
            Me.GridView1.Appearance.HideSelectionRow.ForeColor = System.Drawing.Color.FromArgb(CType(236, Byte),
                                                                                               CType(246, Byte),
                                                                                               CType(255, Byte))
            Me.GridView1.Appearance.HideSelectionRow.Options.UseBackColor = True
            Me.GridView1.Appearance.HideSelectionRow.Options.UseBorderColor = True
            Me.GridView1.Appearance.HideSelectionRow.Options.UseForeColor = True
            Me.GridView1.Appearance.HorzLine.BackColor = System.Drawing.Color.FromArgb(CType(104, Byte), CType(184, Byte),
                                                                                       CType(251, Byte))
            Me.GridView1.Appearance.HorzLine.Options.UseBackColor = True
            Me.GridView1.Appearance.OddRow.BackColor = System.Drawing.Color.FromArgb(CType(236, Byte), CType(246, Byte),
                                                                                     CType(255, Byte))
            Me.GridView1.Appearance.OddRow.BorderColor = System.Drawing.Color.FromArgb(CType(236, Byte), CType(246, Byte),
                                                                                       CType(255, Byte))
            Me.GridView1.Appearance.OddRow.ForeColor = System.Drawing.Color.Black
            Me.GridView1.Appearance.OddRow.Options.UseBackColor = True
            Me.GridView1.Appearance.OddRow.Options.UseBorderColor = True
            Me.GridView1.Appearance.OddRow.Options.UseForeColor = True
            Me.GridView1.Appearance.Preview.Font = New System.Drawing.Font("Verdana", 7.5!)
            Me.GridView1.Appearance.Preview.ForeColor = System.Drawing.Color.FromArgb(CType(83, Byte), CType(155, Byte),
                                                                                      CType(215, Byte))
            Me.GridView1.Appearance.Preview.Options.UseFont = True
            Me.GridView1.Appearance.Preview.Options.UseForeColor = True
            Me.GridView1.Appearance.Row.BackColor = System.Drawing.Color.FromArgb(CType(247, Byte), CType(251, Byte),
                                                                                  CType(255, Byte))
            Me.GridView1.Appearance.Row.ForeColor = System.Drawing.Color.Black
            Me.GridView1.Appearance.Row.Options.UseBackColor = True
            Me.GridView1.Appearance.Row.Options.UseForeColor = True
            Me.GridView1.Appearance.RowSeparator.BackColor = System.Drawing.Color.FromArgb(CType(236, Byte),
                                                                                           CType(246, Byte),
                                                                                           CType(255, Byte))
            Me.GridView1.Appearance.RowSeparator.BackColor2 = System.Drawing.Color.White
            Me.GridView1.Appearance.RowSeparator.Options.UseBackColor = True
            Me.GridView1.Appearance.SelectedRow.BackColor = System.Drawing.Color.FromArgb(CType(83, Byte), CType(155, Byte),
                                                                                          CType(215, Byte))
            Me.GridView1.Appearance.SelectedRow.ForeColor = System.Drawing.Color.White
            Me.GridView1.Appearance.SelectedRow.Options.UseBackColor = True
            Me.GridView1.Appearance.SelectedRow.Options.UseForeColor = True
            Me.GridView1.Appearance.TopNewRow.BackColor = System.Drawing.Color.White
            Me.GridView1.Appearance.TopNewRow.Options.UseBackColor = True
            Me.GridView1.Appearance.VertLine.BackColor = System.Drawing.Color.FromArgb(CType(104, Byte), CType(184, Byte),
                                                                                       CType(251, Byte))
            Me.GridView1.Appearance.VertLine.Options.UseBackColor = True
            Me.GridView1.OptionsSelection.MultiSelect = True
            Me.GridView1.OptionsView.ShowGroupPanel = False
            Me.GridView1.OptionsView.ShowIndicator = False
            '
            'GridColumn_period_end
            '
            Me.GridColumn_period_end.AppearanceCell.Options.UseTextOptions = True
            Me.GridColumn_period_end.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
            Me.GridColumn_period_end.AppearanceHeader.Options.UseTextOptions = True
            Me.GridColumn_period_end.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
            Me.GridColumn_period_end.DisplayFormat.FormatString = "d"
            Me.GridColumn_period_end.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime
            Me.GridColumn_period_end.GroupFormat.FormatString = "d"
            Me.GridColumn_period_end.GroupFormat.FormatType = DevExpress.Utils.FormatType.DateTime
            Me.GridColumn_period_end.OptionsColumn.AllowEdit = False
            '
            'GridControl1
            '
            '
            'GridControl1.EmbeddedNavigator
            '
            Me.GridControl1.EmbeddedNavigator.Name = ""
            Me.GridControl1.Name = "GridControl1"
            '
            'Button_Cancel
            '
            Me.Button_Cancel.Name = "Button_Cancel"
            '
            'Button_OK
            '
            Me.Button_OK.Name = "Button_OK"
            '
            'GridColumn_disbursement_date
            '
            Me.GridColumn_disbursement_date.AppearanceCell.Options.UseTextOptions = True
            Me.GridColumn_disbursement_date.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
            Me.GridColumn_disbursement_date.AppearanceHeader.Options.UseTextOptions = True
            Me.GridColumn_disbursement_date.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
            Me.GridColumn_disbursement_date.DisplayFormat.FormatString = "f0"
            Me.GridColumn_disbursement_date.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
            Me.GridColumn_disbursement_date.GroupFormat.FormatString = "f0"
            Me.GridColumn_disbursement_date.GroupFormat.FormatType = DevExpress.Utils.FormatType.Numeric
            Me.GridColumn_disbursement_date.OptionsColumn.AllowEdit = False
            '
            'Button_New
            '
            Me.Button_New.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), 
                                         System.Windows.Forms.AnchorStyles)
            Me.Button_New.Location = New System.Drawing.Point(416, 96)
            Me.Button_New.Name = "Button_New"
            Me.Button_New.TabIndex = 3
            Me.Button_New.Text = "&New..."
            '
            'NewClientStatementBatch
            '
            Me.Controls.Add(Me.Button_New)
            Me.Name = "NewClientStatementBatch"
            Me.Controls.SetChildIndex(Me.Button_New, 0)
            Me.Controls.SetChildIndex(Me.GridControl1, 0)
            Me.Controls.SetChildIndex(Me.Button_OK, 0)
            Me.Controls.SetChildIndex(Me.Button_Cancel, 0)
            CType(Me.RepositoryItemMemoExEdit1, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.GridView1, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.GridControl1, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.RepositoryItemMemoEdit1, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.ds, System.ComponentModel.ISupportInitialize).EndInit()
            Me.ResumeLayout(False)
        End Sub

#End Region

        Public Overrides Sub Reload()
            MyBase.Reload()
            Me.Button_New.Visible = Not NotesOnly
        End Sub

        Protected Overridable Sub OnNew(ByVal e As EventArgs)
            Dim tbl As DataTable = ds.Tables("client_statement_batches")
            Dim vue As DataView = tbl.DefaultView
            Dim drv As DataRowView = vue.AddNew

            ' Create the new batch
            Using frm As New NewClientStatementBatchForm(drv)
                With frm
                    If .ShowDialog() = DialogResult.OK AndAlso SaveChanges(drv) Then
                        drv.EndEdit()

                        ' Refresh the grid with the new changes so that there is no "new" row pointer
                        GridControl1.RefreshDataSource()

                        ' Select the current row
                        Dim client_statement_batch As Int32 = Convert.ToInt32(drv("client_statement_batch"))
                        Dim RowHandle As Int32 = GridView1.LocateByValue(0, GridColumn_client_statement_batch,
                                                                           client_statement_batch)

                        ' If we can find the row then try to select that row with the OK button
                        If RowHandle >= 0 Then
                            If BatchCount = 1 Then GridView1.ClearSelection()
                            GridView1.SelectRow(RowHandle)
                            If GridView1.SelectedRowsCount = BatchCount Then RaiseSelected(New EventArgs())
                        End If

                    Else

                        ' The form was Canceled. Remove the added row.
                        drv.CancelEdit()
                    End If
                End With
            End Using
        End Sub

        Protected Sub RaiseNew()
            OnNew(EventArgs.Empty)
        End Sub

        Private Sub Button_New_Click(ByVal sender As Object, ByVal e As EventArgs)
            RaiseNew()
        End Sub

        Private Shadows Function SaveChanges(ByRef drv As DataRowView) As Boolean
            Dim answer As Boolean = False

            ' Insert any rows that need to be inserted
            Dim cn As SqlConnection = New SqlConnection(DebtPlus.LINQ.SQLInfoClass.getDefault().ConnectionString)
            Dim current_cursor As Cursor = Cursor.Current
            Try
                Cursor.Current = Cursors.WaitCursor
                cn.Open()

                Using cmd As SqlCommand = New SqlCommand
                    With cmd
                        .Connection = cn
                        .CommandText = "xpr_create_client_statement_batch"
                        .CommandType = CommandType.StoredProcedure
                        .Parameters.Add("RETURN", SqlDbType.Int).Direction = ParameterDirection.ReturnValue
                        .Parameters.Add("@disbursement_date", SqlDbType.Int).Value = drv("disbursement_date")
                        .Parameters.Add("@note", SqlDbType.VarChar, 1024).Value = drv("note")
                        .Parameters.Add("@period_start", SqlDbType.DateTime).Value = drv("period_start")
                        .Parameters.Add("@period_end", SqlDbType.DateTime).Value = drv("period_end")
                        .ExecuteNonQuery()

                        Dim client_statement_batch As Int32 = DebtPlus.Utils.Nulls.DInt(.Parameters(0).Value)
                        drv("client_statement_batch") = client_statement_batch
                        If client_statement_batch > 0 Then answer = True
                    End With
                End Using

            Catch ex As SqlException
                DebtPlus.UI.Common.ErrorHandling.HandleErrors(ex, "Error creating client statement batch")

            Finally
                If cn IsNot Nothing Then cn.Dispose()
                Cursor.Current = current_cursor
            End Try

            ' Process any changes to the notes
            If answer Then MyBase.SaveChanges()
            Return answer
        End Function
    End Class
End Namespace