#Region "Copyright 2000-2012 DebtPlus, L.L.C."

'{*******************************************************************}
'{                                                                   }
'{       DebtPlus Debt Management System                             }
'{                                                                   }
'{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
'{       ALL RIGHTS RESERVED                                         }
'{                                                                   }
'{   The entire contents of this file is protected by U.S. and       }
'{   International Copyright Laws. Unauthorized reproduction,        }
'{   reverse-engineering, and distribution of all or any portion of  }
'{   the code contained in this file is strictly prohibited and may  }
'{   result in severe civil and criminal penalties and will be       }
'{   prosecuted to the maximum extent possible under the law.        }
'{                                                                   }
'{   RESTRICTIONS                                                    }
'{                                                                   }
'{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
'{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
'{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
'{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
'{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
'{                                                                   }
'{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
'{   ADDITIONAL RESTRICTIONS.                                        }
'{                                                                   }
'{*******************************************************************}

#End Region

Option Compare Binary
Option Explicit On
Option Strict On

Imports DevExpress.XtraEditors
Imports System.Threading
Imports System.Windows.Forms

Namespace Client.Statements.Outsourced.CSV
    Friend Class Statistics
        Inherits XtraUserControl

        Private Delegate Sub ThreadDelegate()

        Private ThreadItem As ThreadClass
        Friend WithEvents LabelControlRemainingTime As LabelControl
        Friend WithEvents LabelControl3 As LabelControl
        Public Event Completed As EventHandler
        Friend thrd As Thread

        Public Sub New()
            MyBase.New()
            InitializeComponent()
            AddHandler Timer1.Tick, AddressOf Timer1_Tick
            AddHandler Button_Cancel.Click, AddressOf Button_Cancel_Click
        End Sub

#Region " Windows Form Designer generated code "

        'UserControl overrides dispose to clean up the component list.
        Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
            If disposing Then
                If components IsNot Nothing Then
                    components.Dispose()
                End If
            End If
            MyBase.Dispose(disposing)
        End Sub

        'Required by the Windows Form Designer
        Private components As System.ComponentModel.IContainer

        'NOTE: The following procedure is required by the Windows Form Designer
        'It can be modified using the Windows Form Designer.  
        'Do not modify it using the code editor.
        Friend WithEvents LabelControl1 As DevExpress.XtraEditors.LabelControl
        Friend WithEvents lbl_client As DevExpress.XtraEditors.LabelControl
        Friend WithEvents lbl_client_count As DevExpress.XtraEditors.LabelControl
        Friend WithEvents lbl_line_count As DevExpress.XtraEditors.LabelControl
        Friend WithEvents LabelControl6 As DevExpress.XtraEditors.LabelControl
        Friend WithEvents LabelControl7 As DevExpress.XtraEditors.LabelControl
        Friend WithEvents LabelControl8 As DevExpress.XtraEditors.LabelControl
        Friend WithEvents Timer1 As System.Windows.Forms.Timer
        Private WithEvents Button_Cancel As DevExpress.XtraEditors.SimpleButton
        Friend WithEvents ProgressBarControl1 As DevExpress.XtraEditors.ProgressBarControl
        Friend WithEvents elapsed_time As DevExpress.XtraEditors.LabelControl

        <System.Diagnostics.DebuggerStepThrough()>
        Private Sub InitializeComponent()
            Me.components = New System.ComponentModel.Container
            Me.LabelControl1 = New DevExpress.XtraEditors.LabelControl
            Me.lbl_client = New DevExpress.XtraEditors.LabelControl
            Me.lbl_client_count = New DevExpress.XtraEditors.LabelControl
            Me.lbl_line_count = New DevExpress.XtraEditors.LabelControl
            Me.LabelControl6 = New DevExpress.XtraEditors.LabelControl
            Me.LabelControl7 = New DevExpress.XtraEditors.LabelControl
            Me.LabelControl8 = New DevExpress.XtraEditors.LabelControl
            Me.Timer1 = New System.Windows.Forms.Timer(Me.components)
            Me.Button_Cancel = New DevExpress.XtraEditors.SimpleButton
            Me.ProgressBarControl1 = New DevExpress.XtraEditors.ProgressBarControl
            Me.elapsed_time = New DevExpress.XtraEditors.LabelControl
            Me.LabelControlRemainingTime = New DevExpress.XtraEditors.LabelControl
            Me.LabelControl3 = New DevExpress.XtraEditors.LabelControl
            CType(Me.ProgressBarControl1.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
            Me.SuspendLayout()
            '
            'LabelControl1
            '
            Me.LabelControl1.Location = New System.Drawing.Point(40, 16)
            Me.LabelControl1.Name = "LabelControl1"
            Me.LabelControl1.Size = New System.Drawing.Size(45, 13)
            Me.LabelControl1.TabIndex = 0
            Me.LabelControl1.Text = "Client ID:"
            Me.LabelControl1.UseMnemonic = False
            '
            'lbl_client
            '
            Me.lbl_client.Appearance.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold)
            Me.lbl_client.Appearance.Options.UseFont = True
            Me.lbl_client.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None
            Me.lbl_client.Location = New System.Drawing.Point(181, 16)
            Me.lbl_client.Name = "lbl_client"
            Me.lbl_client.Size = New System.Drawing.Size(99, 13)
            Me.lbl_client.TabIndex = 1
            Me.lbl_client.Text = "----"
            Me.lbl_client.UseMnemonic = False
            '
            'lbl_client_count
            '
            Me.lbl_client_count.Appearance.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold)
            Me.lbl_client_count.Appearance.Options.UseFont = True
            Me.lbl_client_count.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None
            Me.lbl_client_count.Location = New System.Drawing.Point(181, 38)
            Me.lbl_client_count.Name = "lbl_client_count"
            Me.lbl_client_count.Size = New System.Drawing.Size(99, 13)
            Me.lbl_client_count.TabIndex = 2
            Me.lbl_client_count.Text = "----"
            Me.lbl_client_count.UseMnemonic = False
            '
            'lbl_line_count
            '
            Me.lbl_line_count.Appearance.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold)
            Me.lbl_line_count.Appearance.Options.UseFont = True
            Me.lbl_line_count.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None
            Me.lbl_line_count.Location = New System.Drawing.Point(181, 60)
            Me.lbl_line_count.Name = "lbl_line_count"
            Me.lbl_line_count.Size = New System.Drawing.Size(99, 13)
            Me.lbl_line_count.TabIndex = 3
            Me.lbl_line_count.Text = "----"
            Me.lbl_line_count.UseMnemonic = False
            '
            'LabelControl6
            '
            Me.LabelControl6.Location = New System.Drawing.Point(40, 82)
            Me.LabelControl6.Name = "LabelControl6"
            Me.LabelControl6.Size = New System.Drawing.Size(66, 13)
            Me.LabelControl6.TabIndex = 7
            Me.LabelControl6.Text = "Elapsed Time:"
            Me.LabelControl6.UseMnemonic = False
            '
            'LabelControl7
            '
            Me.LabelControl7.Location = New System.Drawing.Point(40, 60)
            Me.LabelControl7.Name = "LabelControl7"
            Me.LabelControl7.Size = New System.Drawing.Size(54, 13)
            Me.LabelControl7.TabIndex = 6
            Me.LabelControl7.Text = "# Records:"
            Me.LabelControl7.UseMnemonic = False
            '
            'LabelControl8
            '
            Me.LabelControl8.Location = New System.Drawing.Point(40, 38)
            Me.LabelControl8.Name = "LabelControl8"
            Me.LabelControl8.Size = New System.Drawing.Size(47, 13)
            Me.LabelControl8.TabIndex = 5
            Me.LabelControl8.Text = "# Clients:"
            Me.LabelControl8.UseMnemonic = False
            '
            'Timer1
            '
            '
            'Button_Cancel
            '
            Me.Button_Cancel.Anchor = System.Windows.Forms.AnchorStyles.Bottom
            Me.Button_Cancel.Location = New System.Drawing.Point(165, 197)
            Me.Button_Cancel.Name = "Button_Cancel"
            Me.Button_Cancel.Size = New System.Drawing.Size(75, 23)
            Me.Button_Cancel.TabIndex = 10
            Me.Button_Cancel.Text = "&Cancel"
            '
            'ProgressBarControl1
            '
            Me.ProgressBarControl1.Anchor =
                CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                       Or System.Windows.Forms.AnchorStyles.Right), 
                      System.Windows.Forms.AnchorStyles)
            Me.ProgressBarControl1.Location = New System.Drawing.Point(3, 138)
            Me.ProgressBarControl1.Name = "ProgressBarControl1"
            Me.ProgressBarControl1.Properties.ProgressViewStyle = DevExpress.XtraEditors.Controls.ProgressViewStyle.Solid
            Me.ProgressBarControl1.Size = New System.Drawing.Size(392, 18)
            Me.ProgressBarControl1.TabIndex = 11
            '
            'elapsed_time
            '
            Me.elapsed_time.Appearance.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold)
            Me.elapsed_time.Appearance.Options.UseFont = True
            Me.elapsed_time.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None
            Me.elapsed_time.Location = New System.Drawing.Point(181, 82)
            Me.elapsed_time.Name = "elapsed_time"
            Me.elapsed_time.Size = New System.Drawing.Size(99, 13)
            Me.elapsed_time.TabIndex = 12
            Me.elapsed_time.Text = "----"
            Me.elapsed_time.UseMnemonic = False
            '
            'LabelControlRemainingTime
            '
            Me.LabelControlRemainingTime.Appearance.Font = New System.Drawing.Font("Tahoma", 8.25!,
                                                                                   System.Drawing.FontStyle.Bold)
            Me.LabelControlRemainingTime.Appearance.Options.UseFont = True
            Me.LabelControlRemainingTime.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None
            Me.LabelControlRemainingTime.Location = New System.Drawing.Point(181, 104)
            Me.LabelControlRemainingTime.Name = "LabelControlRemainingTime"
            Me.LabelControlRemainingTime.Size = New System.Drawing.Size(99, 13)
            Me.LabelControlRemainingTime.TabIndex = 14
            Me.LabelControlRemainingTime.Text = "----"
            Me.LabelControlRemainingTime.UseMnemonic = False
            '
            'LabelControl3
            '
            Me.LabelControl3.Location = New System.Drawing.Point(40, 104)
            Me.LabelControl3.Name = "LabelControl3"
            Me.LabelControl3.Size = New System.Drawing.Size(103, 13)
            Me.LabelControl3.TabIndex = 13
            Me.LabelControl3.Text = "Estimated Remaining:"
            Me.LabelControl3.UseMnemonic = False
            '
            'Statistics
            '
            Me.Controls.Add(Me.LabelControlRemainingTime)
            Me.Controls.Add(Me.LabelControl3)
            Me.Controls.Add(Me.elapsed_time)
            Me.Controls.Add(Me.ProgressBarControl1)
            Me.Controls.Add(Me.Button_Cancel)
            Me.Controls.Add(Me.LabelControl6)
            Me.Controls.Add(Me.LabelControl7)
            Me.Controls.Add(Me.LabelControl8)
            Me.Controls.Add(Me.lbl_line_count)
            Me.Controls.Add(Me.lbl_client_count)
            Me.Controls.Add(Me.lbl_client)
            Me.Controls.Add(Me.LabelControl1)
            Me.Name = "Statistics"
            Me.Size = New System.Drawing.Size(408, 240)
            CType(Me.ProgressBarControl1.Properties, System.ComponentModel.ISupportInitialize).EndInit()
            Me.ResumeLayout(False)
            Me.PerformLayout()
        End Sub

#End Region

        ''' <summary>
        ''' Generate the output file once we have the information
        ''' </summary>
        Public Sub Run(ByRef Args As OutsourceArgParser)

            With ProgressBarControl1
                .Enabled = True
            End With

            ' Start the timer when the thread is about to start
            With Timer1
                .Enabled = True
                .Interval = 1000
                .Start()
            End With
            BaseTime = Now

            ' Create a thread to process the output request
            ThreadItem = New ThreadClass(Args, New ThreadDelegate(AddressOf StatusProc), New ThreadDelegate(AddressOf CompletionProc))
            thrd = New Thread(AddressOf ThreadItem.Run)
            With thrd
                .SetApartmentState(ApartmentState.STA)
                .IsBackground = True
                .Priority = ThreadPriority.Normal
                .Start()
            End With
        End Sub

        ''' <summary>
        ''' Statistics event processing
        ''' </summary>
        Private Sub StatusProc()

            ' Ensure that we are in the proper thread to update the display.
            If InvokeRequired Then
                BeginInvoke(New MethodInvoker(AddressOf StatusProc))
            Else
                ' Display the information on the status window
                With ThreadItem
                    If .CurrentClientCount < 4 Then
                        ProgressBarControl1.Properties.Maximum = .TotalClientCount
                        ProgressBarControl1.Properties.Minimum = 0
                    End If
                    ProgressBarControl1.Position = .CurrentClientCount

                    lbl_client.Text = String.Format("{0:0000000}", .client)
                    lbl_client_count.Text = String.Format("{0:n0}", .CurrentClientCount)
                    lbl_line_count.Text = String.Format("{0:n0}", .output_line_count)

                    ' Calculate the estimated time remaining
                    Dim Diff As TimeSpan = Now.Subtract(BaseTime)
                    Dim Seconds As Double = Diff.TotalSeconds

                    ' Calculate the number of clients per second
                    If .CurrentClientCount > 10 AndAlso Seconds > 10.0 Then
                        Dim ClientsPerSecond As Double = Convert.ToDouble(.CurrentClientCount) / Seconds
                        If ClientsPerSecond > 0 Then
                            Dim ClientsRemaining As Int32 = .TotalClientCount - .ProcessedClientCount
                            If ClientsRemaining < 0 Then ClientsRemaining = 0
                            Dim SecondsRemaining As Int32 =
                                    Convert.ToInt32(Convert.ToDouble(ClientsRemaining) / ClientsPerSecond)

                            Dim Hours As Int32 = SecondsRemaining \ 3600
                            SecondsRemaining = SecondsRemaining Mod 3600

                            Dim Minutes As Int32 = SecondsRemaining \ 60
                            SecondsRemaining = SecondsRemaining Mod 60

                            ' Update the expected remaining time
                            LabelControlRemainingTime.Text = String.Format("{0:f0}:{1:00}:{2:00}", Hours, Minutes, SecondsRemaining)
                        End If
                    End If
                End With
            End If
        End Sub

        ''' <summary>
        ''' Completion event processing
        ''' </summary>
        Private Sub CompletionProc()

            ' Ensure that we are in the proper thread to process the message.
            If InvokeRequired Then
                BeginInvoke(New MethodInvoker(AddressOf CompletionProc))
            Else

                ' Stop the elapsed timer
                With Timer1
                    .Stop()
                    .Enabled = False
                End With

                ' Stop the progress bar
                With ProgressBarControl1
                    .Properties.Maximum = 100
                    .Properties.Minimum = 0
                    .Position = 100
                End With

                ' Tell the user that the processing is complete and wait for the Close button.
                thrd = Nothing
                Button_Cancel.Text = "Close"
                DebtPlus.Data.Forms.MessageBox.Show("The file export is complete", "Operation Completed", MessageBoxButtons.OK)
            End If
        End Sub

        ''' <summary>
        ''' Handle the tick of the timer event
        ''' </summary>
        Private Sub Timer1_Tick(ByVal sender As Object, ByVal e As EventArgs)
            ShowElapsed()
        End Sub

        ''' <summary>
        ''' Display the elapsed time since we started the operation
        ''' </summary>
        Private BaseTime As Date

        Private Sub ShowElapsed()
            Dim difference As TimeSpan = Now.Subtract(BaseTime)
            elapsed_time.Text = String.Format("{0:f0}:{1:00}:{2:00}", difference.Hours, difference.Minutes, difference.Seconds)
        End Sub

        ''' <summary>
        ''' Handle the Cancel button
        ''' </summary>
        Private Sub Button_Cancel_Click(ByVal sender As Object, ByVal e As EventArgs)
            If thrd IsNot Nothing Then thrd.Abort()
            RaiseEvent Completed(Me, EventArgs.Empty)
        End Sub

        ''' <summary>
        ''' Private class to wrap the processing
        ''' </summary>
        Private Class ThreadClass
            Private ap As OutsourceArgParser
            Private StatusProc As ThreadDelegate
            Private CompletionProc As ThreadDelegate

            Public client As Int32
            Public output_line_count As Int32
            Public TotalClientCount As Int32
            Public CurrentClientCount As Int32
            Public ProcessedClientCount As Int32

            Public Sub New(ByVal ap As OutsourceArgParser, ByVal StatusProc As ThreadDelegate,
                           ByVal CompletionProc As ThreadDelegate)
                Me.ap = ap
                Me.StatusProc = StatusProc
                Me.CompletionProc = CompletionProc
            End Sub

            ''' <summary>
            ''' Run the thread
            ''' </summary>
            Friend Sub Run()
                If ap.Monthly Then
                    Dim clsx As New MonthlyStatement_Myriad(ap)
                    AddHandler clsx.Statistics, AddressOf StatsRoutine
                    clsx.WriteStatements()
                Else
                    Dim clsx As New QuarterlyStatement_Myriad(ap)
                    AddHandler clsx.Statistics, AddressOf StatsRoutine
                    clsx.WriteStatements()
                End If

                ' Tell the world that we are complete now
                CompletionProc.Invoke()
            End Sub

            ''' <summary>
            ''' Handle the statistics routine
            ''' </summary>
            Private Sub StatsRoutine(ByVal sender As Object, ByVal e As StatisticsArgs)
                TotalClientCount = e.TotalClientCount
                CurrentClientCount = e.CurrentClientCount
                ProcessedClientCount = e.ProcessedClientCount
                client = e.client
                output_line_count = e.output_line_count
                StatusProc.Invoke()
            End Sub
        End Class
    End Class
End Namespace
