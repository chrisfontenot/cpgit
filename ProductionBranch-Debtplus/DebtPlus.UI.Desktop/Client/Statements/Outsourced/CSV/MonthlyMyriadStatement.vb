#Region "Copyright 2000-2012 DebtPlus, L.L.C."

'{*******************************************************************}
'{                                                                   }
'{       DebtPlus Debt Management System                             }
'{                                                                   }
'{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
'{       ALL RIGHTS RESERVED                                         }
'{                                                                   }
'{   The entire contents of this file is protected by U.S. and       }
'{   International Copyright Laws. Unauthorized reproduction,        }
'{   reverse-engineering, and distribution of all or any portion of  }
'{   the code contained in this file is strictly prohibited and may  }
'{   result in severe civil and criminal penalties and will be       }
'{   prosecuted to the maximum extent possible under the law.        }
'{                                                                   }
'{   RESTRICTIONS                                                    }
'{                                                                   }
'{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
'{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
'{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
'{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
'{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
'{                                                                   }
'{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
'{   ADDITIONAL RESTRICTIONS.                                        }
'{                                                                   }
'{*******************************************************************}

#End Region

Option Compare Binary
Option Explicit On
Option Strict On

Imports System.Globalization

Namespace Client.Statements.Outsourced.CSV
    Friend Class MonthlyStatement_Myriad
        Inherits MonthlyStatement

        ''' <summary>
        ''' Create an instance of our class
        ''' </summary>
        Public Sub New(ByVal ap As OutsourceArgParser)
            MyBase.new(ap)
        End Sub

        ''' <summary>
        ''' Write the Myraid file header
        ''' </summary>
        Protected Overrides Function WriteFileHeader(ByVal CurrentTime As DateTime, ByVal PeriodStart As DateTime, ByVal PeriodEnd As DateTime, ByVal StatementNote As String) As Boolean
            Dim answer As Boolean = MyBase.WriteFileHeader(CurrentTime, PeriodStart, PeriodEnd, StatementNote)

            ' Split the note on word bounderies to form lines of at most 80 characters
            If answer Then
                Dim BufferArea(10) As Object
                BufferArea(4) = StringIfy(String.Empty)
                BufferArea(5) = StringIfy(String.Empty)
                BufferArea(6) = StringIfy(String.Empty)
                BufferArea(7) = StringIfy(String.Empty)
                BufferArea(8) = StringIfy(String.Empty)

                Dim nIndex As Int32 = 3
                Do While nIndex < 5
                    StatementNote = StatementNote.Trim()
                    If StatementNote.Length < 80 Then
                        BufferArea(nIndex) = StringIfy(StatementNote)
                        Exit Do
                    End If

                    ' Find the first blank to break the string.
                    Dim WordBreak As Int32 = StatementNote.LastIndexOf(" "c, 80)
                    If WordBreak > 80 Then WordBreak = 80

                    ' Break the string at the word break location
                    BufferArea(nIndex) = StringIfy(StatementNote.Substring(0, WordBreak).Trim())
                    StatementNote = StatementNote.Substring(WordBreak)
                    nIndex += 1
                Loop

                ' Complete the processing for the buffer areas.
                BufferArea(0) = StringIfy(PeriodStart)
                BufferArea(1) = StringIfy(PeriodEnd)
                BufferArea(2) = StringIfy(CurrentTime)

                ' Write the corresponding string to the output file
                Dim TextLine As String = String.Format(CultureInfo.InvariantCulture, ap.FileHeader, BufferArea)
                WriteTextLine(TextLine)
            End If

            Return answer
        End Function

        ''' <summary>
        ''' Write the Myraid detail record for the client
        ''' </summary>
        Protected Overrides Function WriteStatementHeader(ByVal client As Int32, ByVal PeriodStart As DateTime,
                                                          ByVal PeriodEnd As DateTime) As Boolean
            Dim answer As Boolean = MyBase.WriteStatementHeader(client, PeriodStart, PeriodEnd)

            If answer Then
                Dim row As DataRow = ds.Tables("client_statement_clients").Rows.Find(client)
                If row Is Nothing Then
                    answer = False
                Else

                    Dim counselor_name As String = String.Empty
                    Dim expected_deposit_date As String = String.Empty
                    Dim expected_deposit_amt As String = String.Empty
                    Dim deposit_amt As Decimal = 0D
                    Dim refund_amt As Decimal = 0D
                    Dim starting_trust_balance As Decimal = 0D
                    Dim held_in_trust As Decimal = 0D

                    ' Retrieve the fields from the client header
                    If row("counselor_name") IsNot Nothing AndAlso row("counselor_name") IsNot DBNull.Value Then counselor_name = Convert.ToString(row("counselor_name"), CultureInfo.InvariantCulture).Trim()
                    If row("expected_deposit_date") IsNot Nothing AndAlso row("expected_deposit_date") IsNot DBNull.Value Then expected_deposit_date = Convert.ToString(row("expected_deposit_date"), CultureInfo.InvariantCulture)
                    If row("expected_deposit_amt") IsNot Nothing AndAlso row("expected_deposit_amt") IsNot DBNull.Value Then expected_deposit_amt = String.Format("{0:f0}", toCents(Convert.ToDecimal(row("expected_deposit_amt"), CultureInfo.InvariantCulture)))
                    If row("deposit_amt") IsNot Nothing AndAlso row("deposit_amt") IsNot DBNull.Value Then deposit_amt = Convert.ToDecimal(row("deposit_amt"), CultureInfo.InvariantCulture)
                    If row("refund_amt") IsNot Nothing AndAlso row("refund_amt") IsNot DBNull.Value Then refund_amt = Convert.ToDecimal(row("refund_amt"), CultureInfo.InvariantCulture)
                    If row("starting_trust_balance") IsNot Nothing AndAlso row("starting_trust_balance") IsNot DBNull.Value Then starting_trust_balance = Convert.ToDecimal(row("starting_trust_balance"), CultureInfo.InvariantCulture)
                    If row("held_in_trust") IsNot Nothing AndAlso row("held_in_trust") IsNot DBNull.Value Then held_in_trust = Convert.ToDecimal(row("held_in_trust"), CultureInfo.InvariantCulture)

                    If ap.DebtHeader <> String.Empty Then
                        Dim BufferArea(20) As Object
                        BufferArea(0) = StringIfy(PeriodStart)
                        BufferArea(1) = StringIfy(PeriodEnd)
                        BufferArea(2) = StringIfy(String.Format("{0:0000000}", client))
                        BufferArea(3) = StringIfy(counselor_name)
                        BufferArea(4) = StringIfy(expected_deposit_date)
                        BufferArea(5) = StringIfy(expected_deposit_amt)
                        BufferArea(6) = StringIfy(toCents(deposit_amt))
                        BufferArea(7) = StringIfy(toCents(refund_amt))
                        BufferArea(8) = StringIfy(toCents(starting_trust_balance))
                        BufferArea(9) = StringIfy(toCents(held_in_trust))
                        BufferArea(10) = StringIfy(String.Empty)
                        ' Client Alert Note
                        BufferArea(11) = BufferArea(10)
                        ' Client name and address must be specified as empty strings.
                        BufferArea(12) = BufferArea(10)
                        BufferArea(13) = BufferArea(10)
                        BufferArea(14) = BufferArea(10)

                        ' Include the client address information. Skip blank lines
                        Dim nIndex As Int32 = 11
                        Dim txt As String = DebtPlus.Utils.Nulls.DStr(row("address_1"))
                        If txt <> String.Empty Then
                            BufferArea(nIndex) = StringIfy(txt)
                            nIndex += 1
                        End If

                        txt = DebtPlus.Utils.Nulls.DStr(row("address_2"))
                        If txt <> String.Empty Then
                            BufferArea(nIndex) = StringIfy(txt)
                            nIndex += 1
                        End If

                        txt = DebtPlus.Utils.Nulls.DStr(row("address_3"))
                        If txt <> String.Empty Then
                            BufferArea(nIndex) = StringIfy(txt)
                            nIndex += 1
                        End If

                        txt = DebtPlus.Utils.Nulls.DStr(row("address_4"))
                        If txt <> String.Empty Then
                            BufferArea(nIndex) = StringIfy(txt)
                            nIndex += 1
                        End If

                        ' Write the debt header record (it is also the client information)
                        Dim TextLine As String = String.Format(CultureInfo.InvariantCulture, ap.DebtHeader, BufferArea)
                        WriteTextLine(TextLine)
                    End If
                End If
            End If
            Return answer
        End Function

        Protected Overrides Function ProcessNextStatement(ByVal PeriodStart As DateTime, ByVal PeriodEnd As DateTime) _
            As Boolean
            Dim answer As Boolean = True

            ' Process the data in the current debt record
            Dim CurrentClient As Int32 =
                    StatementDetailsDataReader.GetInt32(StatementDetailsDataReader.GetOrdinal("client"))

            ' If we can not write a header then we need to flush the details for this client
            If Not WriteStatementHeader(CurrentClient, PeriodStart, PeriodEnd) Then
                Do
                    If Not StatementDetailsDataReader.Read Then
                        answer = False
                        Exit Do
                    End If
                Loop _
                    Until _
                        CurrentClient <>
                        StatementDetailsDataReader.GetInt32(StatementDetailsDataReader.GetOrdinal("client"))
                Return answer
            End If

            ' Information for the trailer record
            Dim Total_OriginalBalance As Decimal = 0D
            Dim Total_CurrentDisbursement As Decimal = 0D
            Dim Total_PaymentsToDate As Decimal = 0D
            Dim Total_CurrentBalance As Decimal = 0D
            Dim CurrentDebtCount As Int32 = 0

            ' Generate the lines for all of the client's debts until the ClientId changes.
            Do While CurrentClient = StatementDetailsDataReader.GetInt32(StatementDetailsDataReader.GetOrdinal("client"))

                ' Generate the detial information
                Dim creditor_name As String = String.Empty
                Dim account_number As String = String.Empty
                Dim original_balance As Decimal = 0D
                Dim debits As Decimal = 0D
                Dim credits As Decimal = 0D
                Dim current_balance As Decimal = 0D
                Dim payments_to_date As Decimal = 0D
                Dim interest_rate As Double = 0.0

                For FieldNo As Int32 = 0 To StatementDetailsDataReader.FieldCount - 1
                    If Not StatementDetailsDataReader.IsDBNull(FieldNo) Then
                        Select Case StatementDetailsDataReader.GetName(FieldNo).ToLower()
                            Case "creditor_name"
                                creditor_name = StatementDetailsDataReader.GetString(FieldNo).Trim()
                            Case "formatted_account_number"
                                account_number = StatementDetailsDataReader.GetString(FieldNo).Trim()
                            Case "original_balance"
                                original_balance = StatementDetailsDataReader.GetDecimal(FieldNo)
                            Case "net_debits"
                                debits = StatementDetailsDataReader.GetDecimal(FieldNo)
                            Case "credits"
                                credits = StatementDetailsDataReader.GetDecimal(FieldNo)
                            Case "interest_rate"
                                interest_rate = StatementDetailsDataReader.GetDouble(FieldNo)
                            Case "payments_to_date"
                                payments_to_date = StatementDetailsDataReader.GetDecimal(FieldNo)
                            Case "current_balance"
                                current_balance = StatementDetailsDataReader.GetDecimal(FieldNo)
                            Case Else
                                ' ignore items that we don't want
                        End Select
                    End If
                Next

                ' Make the strings have a limit
                If creditor_name.Length > 25 Then creditor_name = creditor_name.Substring(0, 25).Trim()
                If account_number.Length > 13 Then _
                    account_number = account_number.Substring(account_number.Length - 13, 13).Trim()

                ' Update the global information
                Total_OriginalBalance += original_balance
                Total_CurrentDisbursement += debits
                Total_PaymentsToDate += payments_to_date
                Total_CurrentBalance += current_balance

                ' Format the output detail line
                Dim BufferArea(20) As Object
                BufferArea(0) = StringIfy(creditor_name)
                BufferArea(1) = StringIfy(account_number)
                BufferArea(2) = StringIfy(toCents(original_balance))
                BufferArea(3) = StringIfy(toCents(debits))
                BufferArea(4) = StringIfy(toCents(credits))
                BufferArea(5) = StringIfy(toCents(payments_to_date))
                BufferArea(6) = StringIfy(toCents(current_balance))
                BufferArea(7) = If(interest_rate <> 0.0, "*", String.Empty)
                BufferArea(8) = If(credits <> 0D, "#", String.Empty)
                BufferArea(9) = StringIfy(interest_rate, "{0:p}")

                Dim TextLine As String = String.Format(CultureInfo.InvariantCulture, ap.DebtDetail, BufferArea)
                WriteTextLine(TextLine)
                CurrentDebtCount += 1

                ' Go the next detail line. Stop reading at the end of the list
                If Not StatementDetailsDataReader.Read Then
                    answer = False
                    Exit Do
                End If
            Loop

            ' Write the trailer record if we can
            If _
                Not _
                WriteStatementTrailer(CurrentDebtCount, Total_OriginalBalance, Total_CurrentDisbursement,
                                      Total_PaymentsToDate, Total_CurrentBalance) Then
                answer = False
            End If

            ' Always advance the total number of debts written by the count for this client
            TotalDebtCount += CurrentDebtCount

            Return answer
        End Function

        Protected Overrides Function WriteStatementTrailer(ByVal DebtsWritten As Int32,
                                                           ByVal Total_OriginalBalance As Decimal,
                                                           ByVal total_CurrentDisbursement As Decimal,
                                                           ByVal Total_PaymentsToDate As Decimal,
                                                           ByVal Total_CurrentBalance As Decimal) As Boolean
            Dim answer As Boolean = True

            ' Write the debt trailer
            If ap.DebtTrailer <> String.Empty Then
                Dim BufferArea(5) As Object
                BufferArea(0) = StringIfy(DebtsWritten)
                BufferArea(1) = StringIfy(toCents(Total_OriginalBalance))
                BufferArea(2) = StringIfy(toCents(total_CurrentDisbursement))
                BufferArea(3) = StringIfy(toCents(Total_PaymentsToDate))
                BufferArea(4) = StringIfy(toCents(Total_CurrentBalance))

                Dim TextLine As String = String.Format(CultureInfo.InvariantCulture, ap.DebtTrailer, BufferArea)
                WriteTextLine(TextLine)
            End If

            Return answer
        End Function

        ''' <summary>
        ''' Write the Myraid file trailer
        ''' </summary>
        Protected Overrides Function WriteFileTrailer(ByVal CurrentTime As DateTime, ByVal LineCount As Int32,
                                                      ByVal ClientCount As Int32, ByVal DebtCount As Int32) As Boolean
            Dim answer As Boolean = MyBase.WriteFileTrailer(CurrentTime, LineCount, ClientCount, DebtCount)

            If answer Then
                If ap.FileTrailer <> String.Empty Then
                    Dim BufferArea(3) As Object
                    BufferArea(0) = StringIfy(CurrentTime)
                    BufferArea(1) = StringIfy(LineCount)
                    BufferArea(2) = StringIfy(ClientCount)
                    BufferArea(3) = StringIfy(DebtCount)

                    WriteTextLine(String.Format(CultureInfo.InvariantCulture, ap.FileTrailer, BufferArea))
                End If
            End If

            Return answer
        End Function
    End Class
End Namespace
