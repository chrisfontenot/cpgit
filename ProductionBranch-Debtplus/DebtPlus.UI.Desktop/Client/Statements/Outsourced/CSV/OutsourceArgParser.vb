Option Compare Binary
Option Explicit On
Option Strict On

Imports System.Configuration
Imports System.Collections.Specialized
Imports System.Globalization
Imports System.Windows.Forms

Namespace Client.Statements.Outsourced.CSV
    Friend Class OutsourceArgParser
        Inherits DebtPlus.Utils.ArgParserBase

        ''' <summary>
        ''' How much quoting must be performed on the output text
        ''' </summary>
        Public Enum QuoteLevelSetting
            [None] = 0
            [Auto]
            [Strings]
            [All]
        End Enum

        Private privateQuoteLevel As QuoteLevelSetting = QuoteLevelSetting.Auto

        Public ReadOnly Property QuoteLevel() As QuoteLevelSetting
            Get
                Return privateQuoteLevel
            End Get
        End Property

        ''' <summary>
        ''' Find client information to process
        ''' </summary>
        Private privateSelectStatement As String

        Public ReadOnly Property SelectStatement() As String
            Get
                Return privateSelectStatement
            End Get
        End Property

        ''' <summary>
        ''' Output formatting for the file header
        ''' </summary>
        Private privateFileHeader As String

        Public ReadOnly Property FileHeader() As String
            Get
                Return privateFileHeader
            End Get
        End Property

        ''' <summary>
        ''' Update the batch notes only
        ''' </summary>
        Private privateNotesOnly As Boolean = False

        Public ReadOnly Property NotesOnly() As Boolean
            Get
                Return privateNotesOnly
            End Get
        End Property

        ''' <summary>
        ''' Output formatting for the Debt header
        ''' </summary>
        Private privateDebtHeader As String

        Public ReadOnly Property DebtHeader() As String
            Get
                Return privateDebtHeader
            End Get
        End Property

        ''' <summary>
        ''' Output formatting for the Debt detail
        ''' </summary>
        Private privateDebtDetail As String

        Public ReadOnly Property DebtDetail() As String
            Get
                Return privateDebtDetail
            End Get
        End Property

        ''' <summary>
        ''' Output formatting for the Debt trailer
        ''' </summary>
        Private privateDebtTrailer As String

        Public ReadOnly Property DebtTrailer() As String
            Get
                Return privateDebtTrailer
            End Get
        End Property

        ''' <summary>
        ''' Output formatting for the file trailer
        ''' </summary>
        Private privateFileTrailer As String

        Public ReadOnly Property FileTrailer() As String
            Get
                Return privateFileTrailer
            End Get
        End Property

        ''' <summary>
        ''' String quoting character
        ''' </summary>
        Private privateQuoteChar As String

        Public ReadOnly Property QuoteChar() As String
            Get
                Return privateQuoteChar
            End Get
        End Property

        ''' <summary>
        ''' Field seperator character
        ''' </summary>
        Private privateSepChar As String

        Public ReadOnly Property SepChar() As String
            Get
                Return privateSepChar
            End Get
        End Property

        ''' <summary>
        ''' Batch ID to process
        ''' </summary>
        Private privateBatch() As Int32 = New Int32() {-1, -1, -1}

        Public Function GetBatch() As Int32
            Return GetBatch(0)
        End Function

        Public Function GetBatch(ByVal Index As Int32) As Int32
            Return privateBatch(Index)
        End Function

        Public Sub SetBatch(ByVal value As Int32)
            SetBatch(0, value)
        End Sub

        Public Sub SetBatch(ByVal Index As Int32, ByVal value As Int32)
            privateBatch(Index) = value
        End Sub

        ''' <summary>
        ''' Flag for monthly statements
        ''' </summary>
        Private flagMonthly As Boolean = False

        Public ReadOnly Property Monthly() As Boolean
            Get
                Return flagMonthly
            End Get
        End Property

        ''' <summary>
        ''' Current output file name
        ''' </summary>
        Private OutputName As String = String.Empty

        Public ReadOnly Property OutputFileName() As String
            Get
                Return OutputName
            End Get
        End Property

        ''' <summary>
        ''' Create a new instance of our class. Set the option flags.
        ''' </summary>
        Public Sub New()
            MyBase.New(New String() {"m", "b", "q", "n"})

            ' Configuration settings
            Dim config As NameValueCollection = DebtPlus.Configuration.Config.GetConfigSettings(DebtPlus.Configuration.Config.ConfigSet.ClientStatementsOutsourcedCsv)

            ' Find the file header, trailer, debt header, detail, and trailer records
            privateFileHeader = config.Item("FileHeader")
            If privateFileHeader Is Nothing Then privateFileHeader = String.Empty
            privateFileHeader = Mainline.UnEscape(privateFileHeader)

            privateDebtHeader = config.Item("DebtHeader")
            If privateDebtHeader Is Nothing Then privateDebtHeader = String.Empty
            privateDebtHeader = Mainline.UnEscape(privateDebtHeader)

            privateDebtDetail = config.Item("DebtDetail")
            If privateDebtDetail Is Nothing Then privateDebtDetail = String.Empty
            privateDebtDetail = Mainline.UnEscape(privateDebtDetail)

            privateDebtTrailer = config.Item("DebtTrailer")
            If privateDebtTrailer Is Nothing Then privateDebtTrailer = String.Empty
            privateDebtTrailer = Mainline.UnEscape(privateDebtTrailer)

            privateFileTrailer = config.Item("FileTrailer")
            If privateFileTrailer Is Nothing Then privateFileTrailer = String.Empty
            privateFileTrailer = Mainline.UnEscape(privateFileTrailer)

            privateSelectStatement = config.Item("SelectStatement")
            If privateSelectStatement Is Nothing Then privateSelectStatement = String.Empty
            privateSelectStatement = Mainline.UnEscape(privateSelectStatement).Trim()
            If privateSelectStatement = String.Empty Then
                privateSelectStatement = "SELECT [client_statement_batch],[client],[last_deposit_date],[last_deposit_amt],[deposit_amt],[refund_amt],[disbursement_amt],[held_in_trust],[reserved_in_trust],[starting_trust_balance],[counselor],[office],[expected_deposit_date],[expected_deposit_amt],[delivery_method],[delivery_date],[active_status],[active_debts],[counselor_name],[office_name],[address_1],[address_2],[address_3],[address_4],[postalcode],[company_id],[mail_error_date],[ElectronicStatements],[ElectronicCorrespondence] FROM view_client_statement_clients WHERE client_statement_batch=@client_statement_batch"
            End If

            ' Find the formatting characters
            privateQuoteChar = config.Item("QuoteChar")
            If privateQuoteChar Is Nothing Then privateQuoteChar = String.Empty
            privateQuoteChar = Mainline.UnEscape(privateQuoteChar)

            privateSepChar = config.Item("SepChar")
            If privateSepChar Is Nothing Then privateSepChar = String.Empty
            privateSepChar = Mainline.UnEscape(privateSepChar)

            Dim TempLevel As String = config.Item("QuotedFieldLevel")
            If TempLevel IsNot Nothing Then
                Select Case TempLevel.ToLower()
                    Case "auto" : privateQuoteLevel = QuoteLevelSetting.Auto
                    Case "strings" : privateQuoteLevel = QuoteLevelSetting.Strings
                    Case "all" : privateQuoteLevel = QuoteLevelSetting.All
                    Case Else : privateQuoteLevel = QuoteLevelSetting.None
                End Select
            End If

            ' Find the processing mode from the config file
            Dim Mode As String = config.Item("Mode")
            If Mode IsNot Nothing Then
                Select Case Mode.ToLower()
                    Case "quarterly" : flagMonthly = False
                    Case Else : flagMonthly = True
                End Select
            End If

            ' If there is no quote or no seperator character then we can't quote fields
            If QuoteChar = String.Empty OrElse SepChar = String.Empty Then
                If QuoteLevel <> QuoteLevelSetting.None Then
                    privateQuoteLevel = QuoteLevelSetting.None
                    DebtPlus.Data.Forms.MessageBox.Show(
                        "A seperator and quote character are required for quoted fields. The quoting is disabled.",
                        "Configuration Error", MessageBoxButtons.OK)
                End If
            End If
        End Sub

        ''' <summary>
        ''' Handle the parameter items
        ''' </summary>
        Private BatchCount As Int32 = 0

        Protected Overrides Function OnSwitch(ByVal switchSymbol As String, ByVal switchValue As String) As SwitchStatus
            Dim ss As SwitchStatus = DebtPlus.Utils.ArgParserBase.SwitchStatus.NoError
            Select Case switchSymbol
                Case "m"
                    Select Case switchValue
                        Case "0" : flagMonthly = False
                        Case "1" : flagMonthly = True
                        Case Else
                            ss = DebtPlus.Utils.ArgParserBase.SwitchStatus.YesError
                    End Select

                Case "n"
                    privateNotesOnly = True

                Case "q"
                    Select Case switchValue.ToLower()
                        Case "none" : privateQuoteLevel = QuoteLevelSetting.None
                        Case "auto" : privateQuoteLevel = QuoteLevelSetting.Auto
                        Case "strings" : privateQuoteLevel = QuoteLevelSetting.Strings
                        Case "all" : privateQuoteLevel = QuoteLevelSetting.All
                        Case Else
                            ss = DebtPlus.Utils.ArgParserBase.SwitchStatus.YesError
                    End Select

                Case "b"
                    Dim BatchTemp As Double
                    If Not Double.TryParse(switchValue, NumberStyles.Integer, CultureInfo.CurrentCulture, BatchTemp) Then
                        ss = DebtPlus.Utils.ArgParserBase.SwitchStatus.YesError
                    Else
                        If BatchCount = 3 Then
                            AppendErrorLine("Too many batches specified. The limit is 3.")
                            ss = DebtPlus.Utils.ArgParserBase.SwitchStatus.YesError
                        Else
                            SetBatch(BatchCount, Convert.ToInt32(BatchTemp))
                            BatchCount += 1
                        End If
                    End If

                Case "?" : ss = DebtPlus.Utils.ArgParserBase.SwitchStatus.ShowUsage
                Case Else : ss = DebtPlus.Utils.ArgParserBase.SwitchStatus.YesError
            End Select
            Return ss
        End Function

        ''' <summary>
        ''' Handle the condition where the item is not a parameter.
        ''' </summary>
        Protected Overrides Function OnNonSwitch(ByVal switchValue As String) As SwitchStatus
            Dim ss As SwitchStatus = DebtPlus.Utils.ArgParserBase.SwitchStatus.NoError
            OutputName = switchValue
            Return ss
        End Function

        ''' <summary>
        ''' Ensure that we have a output file name when appropriate
        ''' </summary>
        Protected Overrides Function OnDoneParse() As SwitchStatus
            Dim ss As SwitchStatus = MyBase.OnDoneParse()

            If Not NotesOnly AndAlso (ss = DebtPlus.Utils.ArgParserBase.SwitchStatus.NoError AndAlso OutputFileName = String.Empty) Then
                Using frm As New SaveFileDialog()
                    With frm
                        .AddExtension = True
                        .AutoUpgradeEnabled = True
                        .CheckFileExists = False
                        .CheckPathExists = True
                        .DefaultExt = "txt"
                        .DereferenceLinks = True
                        .Filter = "Text Files (*.txt)|*.txt|All Files (*.*)|*.*"
                        .FilterIndex = 0
                        .InitialDirectory = Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments)
                        .RestoreDirectory = True
                        .Title = "Filename for Extracted Statements"
                        .ValidateNames = True
                        Dim answer As DialogResult = .ShowDialog()
                        If answer = DialogResult.OK Then
                            OutputName = .FileName
                        Else
                            ss = DebtPlus.Utils.ArgParserBase.SwitchStatus.Quit
                        End If
                    End With
                End Using
            End If

            Return ss
        End Function

        ''' <summary>
        ''' Print the program usage information
        ''' </summary>
        Protected Overrides Sub OnUsage(ByVal errorInfo As String)
            If errorInfo IsNot Nothing Then
                AppendErrorLine(String.Format("Invalid parameter: {0}" + Environment.NewLine, errorInfo))
            End If

            AppendErrorLine(
                "Usage: DebtPlus.client.Statements.Outsourced.Myraid.exe [-q] [-b#] [-b#] [-b#] OutputFile..." +
                Environment.NewLine)
            AppendErrorLine("       -q   : Quoting level [None, Auto, Strings, All]")
            AppendErrorLine("       -n   : Update statement notes only")
            AppendErrorLine("       -b   : Statement Batch Number")
        End Sub
    End Class
End Namespace
