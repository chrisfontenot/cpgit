#Region "Copyright 2000-2012 DebtPlus, L.L.C."

'{*******************************************************************}
'{                                                                   }
'{       DebtPlus Debt Management System                             }
'{                                                                   }
'{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
'{       ALL RIGHTS RESERVED                                         }
'{                                                                   }
'{   The entire contents of this file is protected by U.S. and       }
'{   International Copyright Laws. Unauthorized reproduction,        }
'{   reverse-engineering, and distribution of all or any portion of  }
'{   the code contained in this file is strictly prohibited and may  }
'{   result in severe civil and criminal penalties and will be       }
'{   prosecuted to the maximum extent possible under the law.        }
'{                                                                   }
'{   RESTRICTIONS                                                    }
'{                                                                   }
'{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
'{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
'{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
'{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
'{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
'{                                                                   }
'{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
'{   ADDITIONAL RESTRICTIONS.                                        }
'{                                                                   }
'{*******************************************************************}

#End Region

Option Compare Binary
Option Explicit On
Option Strict On

Imports System.IO
Imports System.Text
Imports System.Windows.Forms

Namespace Client.Statements.Outsourced.CSV
    Friend Module Globals
        Public EmptyString As Object
    End Module

    Friend MustInherit Class Statement
        Inherits Object

        Protected OutputStream As StreamWriter
        Protected TotalLineCount As Int32

        ' Dataset for the client information
        Protected ds As New DataSet("ds")

        ''' <summary>
        ''' Linkage to the statitics routine
        ''' </summary>
        Public Delegate Sub StatisticsHandler(ByVal Sender As Object, ByVal e As StatisticsArgs)

        Public Event Statistics As StatisticsHandler

        Protected Overridable Sub OnStatistics(ByVal e As StatisticsArgs)
            RaiseEvent Statistics(Me, e)
        End Sub

        Protected Sub RaiseStatistics(ByVal Client As Int32, ByVal TotalClientCount As Int32, ByVal CurrentClientCount As Int32, ByVal LinesWritten As Int32)
            OnStatistics(New StatisticsArgs(Client, TotalClientCount, CurrentClientCount, LinesWritten))
        End Sub

        Protected Sub RaiseStatistics(ByVal Client As Int32, ByVal TotalClientCount As Int32, ByVal CurrentClientCount As Int32, ByVal ProcessedClientCount As Int32, ByVal LinesWritten As Int32)
            OnStatistics(New StatisticsArgs(Client, TotalClientCount, CurrentClientCount, ProcessedClientCount, LinesWritten))
        End Sub

        ''' <summary>
        ''' Create an instance of our class
        ''' </summary>
        Protected Shared ap As OutsourceArgParser

        Public Sub New(ByVal InputParser As OutsourceArgParser)
            MyBase.new()
            ap = InputParser
        End Sub

        ''' <summary>
        ''' Create the output file
        ''' </summary>
        Protected Overridable Function CreateOutputFile() As Boolean
            Dim answer As Boolean = True

            ' If there is no name then ask for a name
            Dim OutputFileName As String = ap.OutputFileName

            If OutputFileName = String.Empty Then
                Using dlg As New SaveFileDialog
                    With dlg
                        .AddExtension = True
                        .CheckFileExists = False
                        .CheckPathExists = True
                        .DefaultExt = "txt"
                        .DereferenceLinks = True
                        .Filter = "Text Files (*.txt)|(*.txt)|All Files (*.*)|*.*"
                        .FilterIndex = 0
                        .InitialDirectory = Environment.GetFolderPath(Environment.SpecialFolder.Personal)
                        .OverwritePrompt = True
                        .RestoreDirectory = True
                        .Title = "Output File Name"
                        .ValidateNames = True

                        answer = (.ShowDialog() = DialogResult.OK)
                        If answer Then OutputFileName = .FileName
                    End With
                End Using
            End If

            ' If successful then attempt to open the file
            If answer Then
                Try
                    answer = False
                    OutputStream = New StreamWriter(OutputFileName, False, Encoding.ASCII, 4096)
                    answer = True

                Catch ex As DirectoryNotFoundException
                    DebtPlus.Data.Forms.MessageBox.Show("The output file's directory can not be found", "Error Opening Output File", MessageBoxButtons.OK, MessageBoxIcon.Hand)

                Catch ex As PathTooLongException
                    DebtPlus.Data.Forms.MessageBox.Show("The output file's path is too long. Shorten the name or the path.", "Error Opening Output File", MessageBoxButtons.OK, MessageBoxIcon.Hand)

                End Try
            End If

            Return answer
        End Function

        ''' <summary>
        ''' Write the current batch of statements
        ''' </summary>
        Public Overridable Function WriteStatements() As Boolean
            EmptyString = StringIfy(String.Empty)
            Return True
        End Function

        ''' <summary>
        ''' Write the text line to the output file
        ''' </summary>
        Protected Sub WriteTextLine(ByVal TextLine As String)
            OutputStream.WriteLine(TextLine)
            TotalLineCount += 1
        End Sub

        ''' <summary>
        ''' Close the output stream as needed
        ''' </summary>
        Protected Overridable Function CloseOutputFile() As Boolean

            ' Close the file stream
            If OutputStream IsNot Nothing Then
                OutputStream.Close()
                OutputStream = Nothing
            End If

            ' Return success to the caller
            Return True
        End Function

        ''' <summary>
        ''' Allow for generic object to be converted
        ''' </summary>
        Protected Shared Function StringIfy(ByVal Item As Object) As Object
            Dim answer As String = String.Empty
            If Item Is Nothing OrElse Item Is DBNull.Value Then Return EmptyString
            If TypeOf Item Is DateTime Then Return StringIfy(Convert.ToDateTime(Item))
            If TypeOf Item Is String Then Return StringIfy(Convert.ToString(Item))
            If TypeOf Item Is Double Then Return StringIfy(Convert.ToDouble(Item))
            If TypeOf Item Is Decimal Then Return StringIfy(Convert.ToDecimal(Item))
            Return StringIfy(Convert.ToInt64(Item))
        End Function

        ''' <summary>
        ''' Convert decimals to properly delimited output values
        ''' </summary>
        Protected Shared Function StringIfy(ByVal InputDecimal As Decimal) As Object
            Return StringIfy(InputDecimal, "{0:f2}")
        End Function

        Protected Shared Function StringIfy(ByVal InputDecimal As Decimal, ByVal FormatString As String) As Object
            If ap.QuoteLevel = OutsourceArgParser.QuoteLevelSetting.All Then
                Return StringIfy(String.Format(FormatString, InputDecimal))
            Else
                Return InputDecimal
            End If
        End Function

        ''' <summary>
        ''' Convert doubles to properly delimited output values
        ''' </summary>
        Protected Shared Function StringIfy(ByVal InputDouble As Double) As Object
            Return StringIfy(InputDouble, "{0:f2}")
        End Function

        Protected Shared Function StringIfy(ByVal InputDouble As Double, ByVal FormatString As String) As Object
            If ap.QuoteLevel = OutsourceArgParser.QuoteLevelSetting.All Then
                Return StringIfy(String.Format(FormatString, InputDouble))
            Else
                Return InputDouble
            End If
        End Function

        ''' <summary>
        ''' Convert longs to properly delimited output values
        ''' </summary>
        Protected Shared Function StringIfy(ByVal InputLong As Long) As Object
            Return StringIfy(InputLong, "{0:f0}")
        End Function

        Protected Shared Function StringIfy(ByVal InputLong As Long, ByVal FormatString As String) As Object
            If ap.QuoteLevel = OutsourceArgParser.QuoteLevelSetting.All Then
                Return StringIfy(String.Format(FormatString, InputLong))
            Else
                Return InputLong
            End If
        End Function

        ''' <summary>
        ''' Convert dates to properly delimited output values
        ''' </summary>
        Protected Shared Function StringIfy(ByVal InputDate As Date) As Object
            Return StringIfy(InputDate, "{0:d}")
        End Function

        Protected Shared Function StringIfy(ByVal InputDate As Date, ByVal FormatString As String) As Object
            If ap.QuoteLevel = OutsourceArgParser.QuoteLevelSetting.All Then
                Return StringIfy(String.Format(FormatString, InputDate))
            Else
                Return InputDate
            End If
        End Function

        Private Shared ReadOnly sb As New StringBuilder()

        ''' <summary>
        ''' Convert strings to properly delimited output values
        ''' </summary>
        Protected Shared Function Stringify(ByVal InputString As String) As Object

            ' If there is no string then return an empty item
            If InputString Is Nothing Then InputString = String.Empty
            If sb.Length > 0 Then sb.Remove(0, sb.Length)
            sb.Append(InputString)

            ' If quoting is none then do not do anything to the string. User beware!!
            If ap.QuoteLevel <> OutsourceArgParser.QuoteLevelSetting.None Then

                ' If not automatic then the string is quoted.
                Dim NeedQuotes As Boolean = False
                If ap.QuoteLevel <> OutsourceArgParser.QuoteLevelSetting.Auto Then
                    NeedQuotes = True
                Else
                    ' Automatic items look for the need to quote the string.
                    If InputString.IndexOf(ap.QuoteChar) >= 0 Then NeedQuotes = True
                    If InputString.IndexOf(ap.SepChar) >= 0 Then NeedQuotes = True

                    ' Leading or trailing spaces also require quotes
                    If InputString.StartsWith(" ") OrElse InputString.EndsWith(" ") Then NeedQuotes = True
                End If

                If NeedQuotes Then
                    sb.Replace(ap.QuoteChar, ap.QuoteChar + ap.QuoteChar, 0, sb.Length)
                    sb.Insert(0, ap.QuoteChar)
                    sb.Append(ap.QuoteChar)
                End If
            End If

            ' Return the input string
            Return sb.ToString()
        End Function

        ''' <summary>
        ''' Translate the input decimal to a value with an assumed radix
        ''' </summary>
        Protected Shared Function toCents(ByVal InputDec As Decimal) As Int64
            ' Convert the value to an Int32 number of cents
            Return Convert.ToInt64(InputDec * 100D)
        End Function
    End Class
End Namespace
