#Region "Copyright 2000-2012 DebtPlus, L.L.C."

'{*******************************************************************}
'{                                                                   }
'{       DebtPlus Debt Management System                             }
'{                                                                   }
'{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
'{       ALL RIGHTS RESERVED                                         }
'{                                                                   }
'{   The entire contents of this file is protected by U.S. and       }
'{   International Copyright Laws. Unauthorized reproduction,        }
'{   reverse-engineering, and distribution of all or any portion of  }
'{   the code contained in this file is strictly prohibited and may  }
'{   result in severe civil and criminal penalties and will be       }
'{   prosecuted to the maximum extent possible under the law.        }
'{                                                                   }
'{   RESTRICTIONS                                                    }
'{                                                                   }
'{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
'{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
'{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
'{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
'{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
'{                                                                   }
'{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
'{   ADDITIONAL RESTRICTIONS.                                        }
'{                                                                   }
'{*******************************************************************}

#End Region

Option Compare Binary
Option Explicit On
Option Strict On

Namespace Client.Statements.Outsourced.CSV
    Friend Class QuarterlyStatement_Myriad
        Inherits QuarterlyStatement

        Public Sub New(ByVal ap As OutsourceArgParser)
            MyBase.new(ap)
        End Sub

        ''' <summary>
        ''' Write the Myraid file trailer
        ''' </summary>
        Protected Overrides Function WriteFileTrailer(ByVal CurrentTime As DateTime, ByVal LineCount As Int32,
                                                      ByVal ClientCount As Int32, ByVal DebtCount As Int32) As Boolean

            If ap.FileTrailer <> String.Empty Then
                Dim BufferArea(3) As Object
                BufferArea(0) = StringIfy(CurrentTime)
                BufferArea(1) = StringIfy(LineCount)
                BufferArea(2) = StringIfy(ClientCount)
                BufferArea(3) = StringIfy(DebtCount)

                WriteTextLine(String.Format(ap.FileTrailer, BufferArea))
            End If

            Return True
        End Function

        ''' <summary>
        ''' Write the Myraid file header
        ''' </summary>
        Protected Overrides Function WriteFileHeader(ByVal Batch1 As DataRow, ByVal Batch2 As DataRow,
                                                     ByVal Batch3 As DataRow) As Boolean

            ' If there is a header record then generate the file header record
            If ap.FileHeader <> String.Empty Then
                Dim period_start As Date = Date.MinValue
                Dim period_end As Date = Date.MaxValue
                Dim StatementNote As String = String.Empty

                Dim BufferArea(15) As Object

                ' Load the information for the period information
                BufferArea(0) = StringIfy(Batch1("period_start"))
                BufferArea(1) = StringIfy(Batch1("period_end"))
                BufferArea(2) = StringIfy(Batch1("statement_date"))
                BufferArea(3) = StringIfy(Batch1("note"))

                BufferArea(4) = StringIfy(Batch2("period_start"))
                BufferArea(5) = StringIfy(Batch2("period_end"))
                BufferArea(6) = StringIfy(Batch2("statement_date"))
                BufferArea(7) = StringIfy(Batch2("note"))

                BufferArea(8) = StringIfy(Batch3("period_start"))
                BufferArea(9) = StringIfy(Batch3("period_end"))
                BufferArea(10) = StringIfy(Batch3("statement_date"))
                BufferArea(11) = StringIfy(Batch3("note"))

                ' Take the last note and split it into 80 byte chunks
                BufferArea(12) = EmptyString
                BufferArea(13) = EmptyString
                BufferArea(14) = EmptyString
                BufferArea(15) = EmptyString

                Dim idx As Int32 = 12
                Dim NoteText As String = DebtPlus.Utils.Nulls.DStr(Batch3("note"))
                Do While NoteText.Length > 80 AndAlso idx < 15
                    Dim pos As Int32 = NoteText.LastIndexOf(" "c, 80)
                    If pos < 0 Then pos = 80
                    BufferArea(idx) = StringIfy(NoteText.Substring(0, pos).Trim())
                    NoteText = NoteText.Substring(pos).Trim()
                    idx += 1
                Loop
                BufferArea(idx) = StringIfy(NoteText)

                ' Write the corresponding string to the output file
                Dim TextLine As String = String.Format(ap.FileHeader, BufferArea)
                WriteTextLine(TextLine)
            End If

            Return True
        End Function

        Protected Class ClientHeader
            Implements IDisposable

            Public Batch As Int32 = Int32.MinValue
            Public CounselorName As Object = EmptyString
            Public ExpectedDepositDate As Object = EmptyString
            Public ExpectedDepositAmt As Object = EmptyString
            Public HeldInTrust As Object = EmptyString
            Public StartingTrustBalance As Object = EmptyString
            Public DepositAmt As Object = EmptyString
            Public RefundAmt As Object = EmptyString

            Public Sub New()
            End Sub

            Private row As DataRow = Nothing

            Public Sub New(ByVal row As DataRow)
                MyClass.New()
                Me.row = row
                If row IsNot Nothing Then
                    If row("client_statement_batch") IsNot DBNull.Value Then _
                        Batch = Convert.ToInt32(row("client_statement_batch"))

                    ExpectedDepositAmt = StringIfy(toCents(DebtPlus.Utils.Nulls.DDec(row("expected_deposit_amt"))))
                    ExpectedDepositDate = StringIfy(toCents(DebtPlus.Utils.Nulls.DDec(row("expected_deposit_date"))))
                    CounselorName = StringIfy(row("counselor_name"))
                    HeldInTrust = StringIfy(toCents(DebtPlus.Utils.Nulls.DDec(row("held_in_trust"))))
                    StartingTrustBalance = StringIfy(toCents(DebtPlus.Utils.Nulls.DDec(row("starting_trust_balance"))))
                    RefundAmt = StringIfy(toCents(DebtPlus.Utils.Nulls.DDec(row("refund_amt"))))
                    DepositAmt = StringIfy(toCents(DebtPlus.Utils.Nulls.DDec(row("deposit_amt"))))
                End If
            End Sub

            Public Sub SetAllValues(ByRef Output() As Object, ByVal idx As Int32)
                Output(idx) = DepositAmt
                Output(idx + 1) = RefundAmt
            End Sub

            Public Sub SetHeaderValues(ByRef output() As Object)
                output(3) = CounselorName
                output(4) = ExpectedDepositDate
                output(5) = ExpectedDepositAmt
                output(12) = StartingTrustBalance
                output(13) = HeldInTrust
                output(14) = EmptyString
                ' Marker for exception message. Not used here.

                ' Inlcude the client address
                output(15) = EmptyString
                output(16) = EmptyString
                output(17) = EmptyString
                output(18) = EmptyString
                output(19) = EmptyString

                Dim idx As Int32 = 15
                If row IsNot Nothing Then
                    Dim txt As String = DebtPlus.Utils.Nulls.DStr(row("address_1")).Trim()
                    If txt <> String.Empty Then
                        output(idx) = StringIfy(txt)
                        idx += 1
                    End If

                    txt = DebtPlus.Utils.Nulls.DStr(row("address_2")).Trim()
                    If txt <> String.Empty Then
                        output(idx) = StringIfy(txt)
                        idx += 1
                    End If

                    txt = DebtPlus.Utils.Nulls.DStr(row("address_3")).Trim()
                    If txt <> String.Empty Then
                        output(idx) = StringIfy(txt)
                        idx += 1
                    End If

                    txt = DebtPlus.Utils.Nulls.DStr(row("address_4")).Trim()
                    If txt <> String.Empty Then
                        output(idx) = StringIfy(txt)
                        idx += 1
                    End If
                End If
            End Sub

#Region "IDisposable Support"

            Private disposedValue As Boolean

            Protected Overridable Sub Dispose(ByVal disposing As Boolean)
                If Not Me.disposedValue Then
                    If disposing Then
                    End If
                    row = Nothing
                End If
                Me.disposedValue = True
            End Sub

            Public Sub Dispose() Implements IDisposable.Dispose
                Dispose(True)
                GC.SuppressFinalize(Me)
            End Sub

#End Region
        End Class

        Protected Class DebtHeader
            Implements IDisposable

            Public Sub New()
            End Sub

            Public Debits As Decimal = 0D
            Public Credits As Decimal = 0D
            Public OriginalBalance As Decimal = 0D
            Public PaymentsToDate As Decimal = 0D
            Public CurrentBalance As Decimal = 0D
            Public ClientCreditor As Int32 = 0
            Public CreditorName As String = String.Empty
            Public AccountNumber As String = String.Empty
            Public InterestRate As Double = 0.0#

            Public Sub New(ByVal rdr As IDataReader)
                For FieldNo As Int32 = 0 To rdr.FieldCount - 1
                    If Not rdr.IsDBNull(FieldNo) Then
                        Select Case rdr.GetName(FieldNo).ToLower()
                            Case "net_debits"
                                Debits = rdr.GetDecimal(FieldNo)
                            Case "credits"
                                Credits = rdr.GetDecimal(FieldNo)
                            Case "original_balance"
                                OriginalBalance = rdr.GetDecimal(FieldNo)
                            Case "payments_to_date"
                                PaymentsToDate = rdr.GetDecimal(FieldNo)
                            Case "current_balance"
                                CurrentBalance = rdr.GetDecimal(FieldNo)
                            Case "client_creditor"
                                ClientCreditor = Convert.ToInt32(rdr.GetValue(FieldNo))
                            Case "creditor_name"
                                CreditorName = rdr.GetString(FieldNo)
                            Case "formatted_account_number"
                                AccountNumber = rdr.GetString(FieldNo)
                            Case "interest_rate"
                                InterestRate = Convert.ToDouble(rdr.GetValue(FieldNo))
                            Case Else
                                ' skip other items
                        End Select
                    End If
                Next
            End Sub

#Region "IDisposable Support"

            Private disposedValue As Boolean
            ' To detect redundant calls
            Protected Overridable Sub Dispose(ByVal disposing As Boolean)
                If Not Me.disposedValue Then
                    If disposing Then
                    End If
                End If
                Me.disposedValue = True
            End Sub

            Public Sub Dispose() Implements IDisposable.Dispose
                Dispose(True)
                GC.SuppressFinalize(Me)
            End Sub

#End Region

            Public Overridable Sub SetHeaderFields(ByRef Output() As Object)
                Output(0) = StringIfy(CreditorName)
                Output(1) = StringIfy(AccountNumber)
                Output(2) = StringIfy(toCents(OriginalBalance))
                Output(3) = StringIfy(toCents(PaymentsToDate))
                Output(4) = StringIfy(toCents(CurrentBalance))
                Output(5) = StringIfy(InterestRate, "{0:p}")
                Output(6) = If(InterestRate <> 0.0, "*", String.Empty)
            End Sub

            Public Overridable Sub SetItemFields(ByRef Output() As Object, ByVal idx As Int32)
                Output(idx) = StringIfy(toCents(Debits))
                Output(idx + 1) = StringIfy(toCents(Credits))
                Output(idx + 2) = If(Credits <> 0D, "#", String.Empty)
            End Sub
        End Class

        Protected Class DebtTotals
            Inherits DebtHeader

            Public Sub New()
            End Sub

            Public Sub AddTotals(ByVal ref As DebtHeader)
                Credits += ref.Credits
                Debits += ref.Debits

                OriginalBalance += ref.OriginalBalance
                PaymentsToDate += ref.PaymentsToDate
                CurrentBalance += ref.CurrentBalance
            End Sub

            Public Overrides Sub SetHeaderFields(ByRef Output() As Object)
                Output(1) = StringIfy(toCents(OriginalBalance))
                Output(2) = StringIfy(toCents(PaymentsToDate))
                Output(3) = StringIfy(toCents(CurrentBalance))
            End Sub

            Public Overrides Sub SetItemFields(ByRef Output() As Object, ByVal idx As Int32)
                Output(idx) = StringIfy(toCents(Debits))
            End Sub
        End Class

        Protected Sub WriteCombinedDebts(ByRef Totals1 As DebtTotals, ByRef Totals2 As DebtTotals,
                                         ByRef Totals3 As DebtTotals, ByRef Debt1 As DebtHeader, ByRef Debt2 As DebtHeader,
                                         ByRef Debt3 As DebtHeader)
            Dim BufferArea(20) As Object

            If Debt3 IsNot Nothing Then
                Debt3.SetHeaderFields(BufferArea)
                Debt3.SetItemFields(BufferArea, 13)
                Totals3.AddTotals(Debt3)
                Debt3.Dispose()
                Debt3 = Nothing
            End If

            If Debt2 IsNot Nothing Then
                Debt2.SetHeaderFields(BufferArea)
                Debt2.SetItemFields(BufferArea, 10)
                Totals2.AddTotals(Debt2)
                Debt2.Dispose()
                Debt2 = Nothing
            End If

            If Debt1 IsNot Nothing Then
                Debt1.SetHeaderFields(BufferArea)
                Debt1.SetItemFields(BufferArea, 7)
                Totals1.AddTotals(Debt1)
                Debt1.Dispose()
                Debt1 = Nothing
            End If

            ' Write the detail line if possible
            If ap.DebtDetail <> String.Empty Then
                Dim txt As String = String.Format(ap.DebtDetail, BufferArea)
                WriteTextLine(txt)
            End If
        End Sub

        ''' <summary>
        ''' Write the Myraid detail record for the client
        ''' </summary>
        Protected Overrides Function ProcessNextStatement(ByVal PeriodStart As DateTime, ByVal PeriodEnd As DateTime,
                                                          ByVal Batch_0 As Int32, ByVal Batch_1 As Int32,
                                                          ByVal Batch_2 As Int32) As Boolean
            Dim answer As Boolean = MyBase.ProcessNextStatement(PeriodStart, PeriodEnd, Batch_0, Batch_1, Batch_2)
            Dim Client As Int32 = StatementDetailsDataReader.GetInt32(StatementDetailsDataReader.GetOrdinal("client"))

            ' Information from the header record
            If ap.DebtHeader <> String.Empty Then
                Dim BufferArea(20) As Object

                ' Information about the current client
                BufferArea(0) = StringIfy(PeriodStart)
                BufferArea(1) = StringIfy(PeriodEnd)
                BufferArea(2) = StringIfy(String.Format("{0:0000000}", Client))

                ' Process the header information
                Dim tbl As DataTable = ds.Tables("client_statement_clients")
                Using hdr As New ClientHeader(tbl.Rows.Find(New Object() {Client, Batch_2}))
                    hdr.SetHeaderValues(BufferArea)
                    hdr.SetAllValues(BufferArea, 10)
                End Using

                Using hdr As New ClientHeader(tbl.Rows.Find(New Object() {Client, Batch_1}))
                    hdr.SetHeaderValues(BufferArea)
                    hdr.SetAllValues(BufferArea, 8)
                End Using

                Using hdr As New ClientHeader(tbl.Rows.Find(New Object() {Client, Batch_0}))
                    hdr.SetHeaderValues(BufferArea)
                    hdr.SetAllValues(BufferArea, 6)
                End Using

                ' Write the debt header record (it is also the client information)
                Dim TextLine As String = String.Format(ap.DebtHeader, BufferArea)
                WriteTextLine(TextLine)
            End If

            ' Initialize our counters for the trailer record
            Dim Totals1 As New DebtTotals
            Dim Totals2 As New DebtTotals
            Dim Totals3 As New DebtTotals

            Try
                Dim Period1 As DebtHeader = Nothing
                Dim Period2 As DebtHeader = Nothing
                Dim Period3 As DebtHeader = Nothing

                Dim DebtsWritten As Int32 = 0
                Dim LastClientCreditor As Int32 =
                        StatementDetailsDataReader.GetInt32(StatementDetailsDataReader.GetOrdinal("client_creditor"))

                ' Debts until we are on a different client
                Do While Client = StatementDetailsDataReader.GetInt32(StatementDetailsDataReader.GetOrdinal("client"))

                    ' When the debt changes, write the records
                    Dim ClientCreditor As Int32 =
                            StatementDetailsDataReader.GetInt32(StatementDetailsDataReader.GetOrdinal("client_creditor"))
                    If LastClientCreditor <> ClientCreditor Then
                        WriteCombinedDebts(Totals1, Totals2, Totals3, Period1, Period2, Period3)
                        DebtsWritten += 1
                        LastClientCreditor = ClientCreditor
                    End If

                    ' Put the current line into the apporpriate location for the debt collection
                    Dim ClientStatementBatch As Int32 =
                            StatementDetailsDataReader.GetInt32(
                                StatementDetailsDataReader.GetOrdinal("client_statement_batch"))
                    If ClientStatementBatch = Batch_0 Then
                        Period1 = New DebtHeader(StatementDetailsDataReader)
                    ElseIf ClientStatementBatch = Batch_1 Then
                        Period2 = New DebtHeader(StatementDetailsDataReader)
                    Else
                        Period3 = New DebtHeader(StatementDetailsDataReader)
                    End If

                    ' Process the next record
                    If Not StatementDetailsDataReader.Read Then
                        answer = False
                        Exit Do
                    End If
                Loop

                ' If there is something to write then write the last debt to the list
                If Period1 IsNot Nothing OrElse Period2 IsNot Nothing OrElse Period3 IsNot Nothing Then
                    WriteCombinedDebts(Totals1, Totals2, Totals3, Period1, Period2, Period3)
                    DebtsWritten += 1
                End If

                ' Write the debt trailer
                If ap.DebtTrailer <> String.Empty Then
                    Dim BufferArea(6) As Object

                    BufferArea(0) = StringIfy(DebtsWritten)
                    Totals1.SetHeaderFields(BufferArea)
                    Totals1.SetItemFields(BufferArea, 4)
                    Totals2.SetItemFields(BufferArea, 5)
                    Totals3.SetItemFields(BufferArea, 6)

                    Dim TextLine As String = String.Format(ap.DebtTrailer, BufferArea)
                    WriteTextLine(TextLine)
                End If

                ' Count the debts into the total field
                MyBase.TotalDebtCount += DebtsWritten

            Finally
                ' Discard the client totals
                Totals1.Dispose()
                Totals2.Dispose()
                Totals3.Dispose()
            End Try

            Return answer
        End Function
    End Class
End Namespace
