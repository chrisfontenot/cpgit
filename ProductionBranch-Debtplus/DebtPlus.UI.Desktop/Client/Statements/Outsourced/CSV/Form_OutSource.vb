#Region "Copyright 2000-2012 DebtPlus, L.L.C."

'{*******************************************************************}
'{                                                                   }
'{       DebtPlus Debt Management System                             }
'{                                                                   }
'{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
'{       ALL RIGHTS RESERVED                                         }
'{                                                                   }
'{   The entire contents of this file is protected by U.S. and       }
'{   International Copyright Laws. Unauthorized reproduction,        }
'{   reverse-engineering, and distribution of all or any portion of  }
'{   the code contained in this file is strictly prohibited and may  }
'{   result in severe civil and criminal penalties and will be       }
'{   prosecuted to the maximum extent possible under the law.        }
'{                                                                   }
'{   RESTRICTIONS                                                    }
'{                                                                   }
'{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
'{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
'{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
'{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
'{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
'{                                                                   }
'{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
'{   ADDITIONAL RESTRICTIONS.                                        }
'{                                                                   }
'{*******************************************************************}

#End Region

Option Compare Binary
Option Explicit On
Option Strict On

Imports System.ComponentModel
Imports DebtPlus.Data.Forms

Namespace Client.Statements.Outsourced.CSV
    Friend Class Form_OutSource
        Inherits DebtPlusForm

        ''' <summary>
        ''' Initialize the class with the normal sequence
        ''' </summary>
        Private Args As OutsourceArgParser

        Public Sub New()
            MyBase.New()
            InitializeComponent()
            AddHandler GotFocus, AddressOf Form_OutSource_GotFocus
            AddHandler Load, AddressOf Form_OutSource_Load
            AddHandler Closing, AddressOf Form_OutSource_Closing
            AddHandler Statistics1.Completed, AddressOf Statistics1_Completed
            AddHandler ClientStatementBatch1.Canceled, AddressOf ClientStatementBatch1_Canceled
            AddHandler ClientStatementBatch1.Selected, AddressOf ClientStatementBatch1_Selected
        End Sub

        Public Sub New(ByVal ap As OutsourceArgParser)
            MyClass.New()
            Args = ap

            ' Pass along the flag to indicate that we only want to update the notes.
            ClientStatementBatch1.NotesOnly = ap.NotesOnly

            ' If monthly then select only one batch. Quarterly requires 3.
            If ap.Monthly Then
                ClientStatementBatch1.BatchCount = 1
            Else
                ClientStatementBatch1.BatchCount = 3
            End If
        End Sub

#Region " Windows Form Designer generated code "

        'Form overrides dispose to clean up the component list.
        Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
            If disposing Then
                If components IsNot Nothing Then
                    components.Dispose()
                End If
            End If
            MyBase.Dispose(disposing)
        End Sub

        'Required by the Windows Form Designer
        Private components As System.ComponentModel.IContainer

        'NOTE: The following procedure is required by the Windows Form Designer
        'It can be modified using the Windows Form Designer.  
        'Do not modify it using the code editor.
        Friend WithEvents ClientStatementBatch1 As NewClientStatementBatch
        Friend WithEvents Statistics1 As DebtPlus.UI.Desktop.Client.Statements.Outsourced.CSV.Statistics

        <System.Diagnostics.DebuggerStepThrough()>
        Private Sub InitializeComponent()
            Me.ClientStatementBatch1 = New DebtPlus.UI.Desktop.Client.Statements.Outsourced.CSV.NewClientStatementBatch
            Me.Statistics1 = New DebtPlus.UI.Desktop.Client.Statements.Outsourced.CSV.Statistics
            CType(Me.DxErrorProvider1, System.ComponentModel.ISupportInitialize).BeginInit()
            Me.SuspendLayout()
            '
            'ClientStatementBatch1
            '
            Me.ClientStatementBatch1.Dock = System.Windows.Forms.DockStyle.Fill
            Me.ClientStatementBatch1.Location = New System.Drawing.Point(0, 0)
            Me.ClientStatementBatch1.Name = "ClientStatementBatch1"
            Me.ClientStatementBatch1.NotesOnly = False
            Me.ClientStatementBatch1.Size = New System.Drawing.Size(528, 266)
            Me.ClientStatementBatch1.TabIndex = 0
            '
            'Statistics1
            '
            Me.Statistics1.Dock = System.Windows.Forms.DockStyle.Fill
            Me.Statistics1.Location = New System.Drawing.Point(0, 0)
            Me.Statistics1.Name = "Statistics1"
            Me.Statistics1.Size = New System.Drawing.Size(528, 266)
            Me.Statistics1.TabIndex = 1
            '
            'Form_OutSource
            '
            Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
            Me.ClientSize = New System.Drawing.Size(528, 266)
            Me.Controls.Add(Me.Statistics1)
            Me.Controls.Add(Me.ClientStatementBatch1)
            Me.Name = "Form_OutSource"
            Me.Text = "Generate Client Statements for Outsource Agency"
            Me.TopMost = True
            CType(Me.DxErrorProvider1, System.ComponentModel.ISupportInitialize).EndInit()
            Me.ResumeLayout(False)
        End Sub

#End Region

        ''' <summary>
        ''' Process the CANCEL event from the statement list
        ''' </summary>
        Private Sub ClientStatementBatch1_Canceled(ByVal sender As Object, ByVal e As EventArgs)
            Close()
        End Sub

        ''' <summary>
        ''' The statement batch list is complete. Go to the next pannel.
        ''' </summary>
        Private Sub ClientStatementBatch1_Selected(ByVal sender As Object, ByVal e As EventArgs)

            If Not ClientStatementBatch1.NotesOnly Then
                Args.SetBatch(0, ClientStatementBatch1.SelectedBatch(0))
                If Not Args.Monthly Then
                    Args.SetBatch(1, ClientStatementBatch1.SelectedBatch(1))
                    Args.SetBatch(2, ClientStatementBatch1.SelectedBatch(2))
                End If
                GenerateOutput()
            End If
        End Sub

        ''' <summary>
        ''' Run the output statement processing logic
        ''' </summary>
        Private Sub GenerateOutput()

            ' Disable the client statement portion of the form
            With ClientStatementBatch1
                .Visible = False
            End With

            ' Enable the statistics and run the process
            With Statistics1
                .Visible = True
                .Run(Args)
            End With
        End Sub

        ''' <summary>
        ''' Remove the topmost status when we get the focus
        ''' </summary>
        Private Sub Form_OutSource_GotFocus(ByVal sender As Object, ByVal e As EventArgs)
            TopMost = False
        End Sub

        ''' <summary>
        ''' On the form load, enable the list of batches
        ''' </summary>
        Private Sub Form_OutSource_Load(ByVal sender As Object, ByVal e As EventArgs)

            ' Determine if we have the batches that we need to process the operation
            Dim Complete As Boolean = (Args.GetBatch(2) >= 0)
            If Not Complete AndAlso Args.Monthly AndAlso Args.GetBatch(0) >= 0 Then Complete = True

            ' If we are not complete the enable the input form.
            If Complete Then
                GenerateOutput()
            Else
                With Statistics1
                    .Visible = False
                End With

                With ClientStatementBatch1
                    .Visible = True
                    .Reload()
                End With
            End If
        End Sub

        ''' <summary>
        ''' When our file is generated, terminate the form
        ''' </summary>
        Private Sub Statistics1_Completed(ByVal sender As Object, ByVal e As EventArgs)
            Close()
        End Sub

        ''' <summary>
        ''' We are closing the form. Abort the processing thread.
        ''' </summary>
        Private Sub Form_OutSource_Closing(ByVal sender As Object, ByVal e As CancelEventArgs)
            With Statistics1
                If .Visible AndAlso .thrd IsNot Nothing Then .thrd.Abort()
            End With
        End Sub
    End Class
End Namespace
