Option Compare Binary
Option Explicit On
Option Strict On

Namespace Client.Refund
    Friend Class ArgParser
        Inherits DebtPlus.Utils.ArgParserBase

        ''' <summary>
        ''' Create an instance of our class
        ''' </summary>
        Public Sub New()
            MyBase.New(New String() {})
        End Sub

        ''' <summary>
        ''' Generate the command usage information
        ''' </summary>
        Protected Overrides Sub OnUsage(ByVal errorInfo As String)
            ' deprecated
        End Sub
    End Class
End NameSpace
