#Region "Copyright 2000-2012 DebtPlus, L.L.C."

'{*******************************************************************}
'{                                                                   }
'{       DebtPlus Debt Management System                             }
'{                                                                   }
'{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
'{       ALL RIGHTS RESERVED                                         }
'{                                                                   }
'{   The entire contents of this file is protected by U.S. and       }
'{   International Copyright Laws. Unauthorized reproduction,        }
'{   reverse-engineering, and distribution of all or any portion of  }
'{   the code contained in this file is strictly prohibited and may  }
'{   result in severe civil and criminal penalties and will be       }
'{   prosecuted to the maximum extent possible under the law.        }
'{                                                                   }
'{   RESTRICTIONS                                                    }
'{                                                                   }
'{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
'{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
'{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
'{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
'{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
'{                                                                   }
'{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
'{   ADDITIONAL RESTRICTIONS.                                        }
'{                                                                   }
'{*******************************************************************}

#End Region
Imports DevExpress.XtraEditors.Controls
Imports DebtPlus.Utils
Imports System.Data.SqlClient
Imports DebtPlus.Check.Print.Library
Imports ComboboxItem = DebtPlus.Data.Controls.ComboboxItem

Namespace Client.Refund

    Friend Class Form1

        Private ds As New DataSet("ds")
        Private ap As ArgParser = Nothing

        Public Sub New()
            MyBase.New()
            InitializeComponent()
        End Sub

        Public Sub New(ByVal ap As ArgParser)
            MyClass.New()
            Me.ap = ap
            AddHandler Me.Load, AddressOf Form1_Load
            AddHandler SimpleButton_ok.Click, AddressOf SimpleButton_ok_Click
            AddHandler SimpleButton_cancel.Click, AddressOf SimpleButton_cancel_Click
            AddHandler CalcEdit_amount.EditValueChanging, AddressOf CalcEdit_amount_EditValueChanging
            AddHandler ClientID1.EditValueChanged, AddressOf ClientID1_EditValueChanged
        End Sub

        Private Sub SimpleButton_cancel_Click(ByVal sender As Object, ByVal e As EventArgs)
            Close()
        End Sub

        Private Sub Form1_Load(ByVal sender As Object, ByVal e As EventArgs)

            ' Build the list of check dispositions
            With ComboBoxEdit_disposition
                With .Properties
                    With .Items
                        .Clear()
                        .Add(New DebtPlus.Data.Controls.ComboboxItem("Save the check for the next print run", "QUEUE"))
                        .Add(New DebtPlus.Data.Controls.ComboboxItem("Print the check immediately", "PRINT"))
                        .Add(New DebtPlus.Data.Controls.ComboboxItem("Enter the information for a previously printed check", "MANUAL"))
                    End With
                End With
                .SelectedIndex = 0
            End With

            ' Bind the list of reasons to the control
            With ComboBoxEdit_reason
                With .Properties
                    With .Items
                        .Clear()
                        For Each row As DataRow In ReasonsTable.Rows
                            .Add(New DebtPlus.Data.Controls.ComboboxItem(Convert.ToString(row("description"))))
                        Next
                    End With
                End With
                .SelectedIndex = 0
            End With
        End Sub

        Private Function ReasonsTable() As DataTable
            Dim tbl As DataTable = ds.Tables("lst_descriptions_CR")
            If tbl Is Nothing Then
                Try
                    Using cmd As SqlCommand = New SqlCommand
                        With cmd
                            .Connection = New SqlConnection(DebtPlus.LINQ.SQLInfoClass.getDefault().ConnectionString)
                            .CommandText = "lst_descriptions_CR"
                            .CommandType = CommandType.StoredProcedure
                        End With

                        Using da As New SqlDataAdapter(cmd)
                            da.Fill(ds, "lst_descriptions_CR")
                        End Using
                    End Using

                    tbl = ds.Tables("lst_descriptions_CR")
                    If tbl.PrimaryKey.Count = 0 Then
                        With tbl
                            .PrimaryKey = New DataColumn() {.Columns("client")}
                        End With
                    End If

                Catch ex As SqlException
                    DebtPlus.UI.Common.ErrorHandling.HandleErrors(ex, "Error reading lst_descripitons_CR")
                End Try
            End If

            Return tbl
        End Function

        Private Sub ClientID1_EditValueChanged(ByVal sender As Object, ByVal e As EventArgs)
            Dim row As DataRow = ReadClient(ClientID1.EditValue)
            If row Is Nothing Then
                LabelControl_Address.Text = String.Empty
                SetBalance(0D, 0D)
            Else
                LabelControl_Address.Text = Convert.ToString(row("address"))
                SetBalance(row("held_in_trust"), CalcEdit_amount.EditValue)
            End If
        End Sub

        Private LastCurrentBalance As Decimal = 0D

        Private Sub SetBalance(ByVal CurrentBalance As Object, ByVal Amount As Object)
            If CurrentBalance Is Nothing OrElse CurrentBalance Is DBNull.Value Then CurrentBalance = 0D
            If Amount Is Nothing OrElse Amount Is DBNull.Value Then Amount = 0D

            ' Save the last balance for later refresn
            LastCurrentBalance = Convert.ToDecimal(CurrentBalance)

            ' Update the display with the amounts
            LabelControl_current_balance.Text = String.Format("{0:c}", LastCurrentBalance)
            LabelControl_ending_balance.Text = String.Format("{0:c}",
                                                             Convert.ToDecimal(CurrentBalance) - Convert.ToDecimal(Amount))
            SimpleButton_ok.Enabled = Convert.ToDecimal(Amount) > 0D AndAlso
                                      (Convert.ToDecimal(Amount) <= Convert.ToDecimal(CurrentBalance)) AndAlso
                                      (LabelControl_Address.Text <> String.Empty)
        End Sub

        Private Sub SetBalance(ByVal Amount As Object)
            SetBalance(LastCurrentBalance, Amount)
        End Sub

        ''' <summary>
        ''' Define the clients table
        ''' </summary>
        Protected Function ReadClient(ByVal client As Int32?) As DataRow
            Dim answer As DataRow = Nothing

            If client IsNot Nothing Then
                ' If the table is not defined then read it from the database.
                Dim tbl As DataTable = ds.Tables("clients")
                If tbl IsNot Nothing Then answer = tbl.Rows.Find(client.Value)

                If answer Is Nothing Then
                    Try
                        Using cmd As SqlCommand = New SqlCommand
                            With cmd
                                .Connection = New SqlConnection(DebtPlus.LINQ.SQLInfoClass.getDefault().ConnectionString)
                                .CommandText = "SELECT c.client, c.held_in_trust, c.active_status, dbo.address_block(dbo.format_normal_name(pn.prefix,pn.first,pn.middle,pn.last,pn.suffix),dbo.format_address_line_1(a.house,a.direction,a.street,a.suffix,a.modifier,a.modifier_value),a.address_line_2,dbo.format_city_state_zip(a.city,a.state,a.postalcode),default) as address FROM clients c WITH (NOLOCK) LEFT OUTER JOIN people p ON c.client = p.client AND 1 = p.relation LEFT OUTER JOIN names pn WITH (NOLOCK) ON p.NameID = pn.Name LEFT OUTER JOIN addresses a WITH (NOLOCK) ON c.AddressID = a.Address WHERE c.client = @client"
                                .CommandType = CommandType.Text
                                .Parameters.Add("@client", SqlDbType.Int).Value = client.Value
                            End With

                            Using da As New SqlDataAdapter(cmd)
                                da.Fill(ds, "clients")
                            End Using
                        End Using

                        tbl = ds.Tables("clients")
                        If tbl.PrimaryKey.Count = 0 Then
                            With tbl
                                .PrimaryKey = New DataColumn() {.Columns("client")}
                            End With
                        End If

                        answer = tbl.Rows.Find(client.Value)

                    Catch ex As SqlException
                        DebtPlus.UI.Common.ErrorHandling.HandleErrors(ex, "Error reading clients")
                    End Try
                End If
            End If

            Return answer
        End Function

        Private Sub CalcEdit_amount_EditValueChanging(ByVal sender As Object, ByVal e As ChangingEventArgs)
            If Convert.ToDecimal(e.NewValue) < 0D Then
                e.Cancel = True
            Else
                SetBalance(e.NewValue)
            End If
        End Sub

        Private Sub SimpleButton_ok_Click(ByVal sender As Object, ByVal e As EventArgs)
            Dim trust_register As Int32
            Dim cn As SqlConnection = New SqlConnection(DebtPlus.LINQ.SQLInfoClass.getDefault().ConnectionString)
            Try
                cn.Open()
                Using cmd As SqlCommand = New SqlCommand
                    With cmd
                        .Connection = cn
                        .CommandText = "xpr_client_refund_CR"
                        .CommandType = CommandType.StoredProcedure
                        .Parameters.Add("RETURN", SqlDbType.Int).Direction = ParameterDirection.ReturnValue
                        .Parameters.Add("@client", SqlDbType.Int).Value = DebtPlus.Utils.Nulls.ToDbNull(ClientID1.EditValue)
                        .Parameters.Add("@amount", SqlDbType.Decimal).Value = CalcEdit_amount.EditValue
                        .Parameters.Add("@message", SqlDbType.VarChar, 50).Value = ComboBoxEdit_reason.Text.Trim()
                        .Parameters.Add("@tran_type", SqlDbType.VarChar, 10).Value = "CR"
                        .ExecuteNonQuery()
                        trust_register = Convert.ToInt32(.Parameters(0).Value)
                    End With
                End Using

                ' Toss the current client record from the database because the trust balance is now wrong
                Dim row As DataRow = ReadClient(ClientID1.EditValue)
                If row IsNot Nothing Then
                    row.Delete()
                    ds.Tables("clients").AcceptChanges()
                End If

            Catch ex As SqlException
                DebtPlus.UI.Common.ErrorHandling.HandleErrors(ex, "Error generating client refund check")
            End Try

            ' Process the disposition once the check has been created
            If trust_register > 0 Then
                Select Case Convert.ToString(CType(ComboBoxEdit_disposition.SelectedItem, DebtPlus.Data.Controls.ComboboxItem).value)
                    Case "MANUAL"
                        With New PrintChecksClass
                            .RecordSingleCheck(trust_register)
                        End With

                    Case "QUEUE"
                        DoMessageBeep(MessageBeepEnum.OK)

                    Case "PRINT"
                        With New PrintChecksClass
                            .PrintSingleCheck(trust_register)
                        End With
                End Select

                ' Clear the information for the next cycle
                LabelControl_Address.Text = String.Empty
                ClientID1.EditValue = Nothing
                CalcEdit_amount.EditValue = 0D
                ClientID1.Focus()
            End If
        End Sub
    End Class
End Namespace
