Imports DebtPlus.UI.Client.Widgets.Controls

Namespace Client.Refund
    <Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
    Partial Class Form1
        Inherits DebtPlus.Data.Forms.DebtPlusForm

        'Form overrides dispose to clean up the component list.
        <System.Diagnostics.DebuggerNonUserCode()> _
        Protected Overrides Sub Dispose(ByVal disposing As Boolean)
            Try
                If disposing AndAlso components IsNot Nothing Then
                    components.Dispose()
                End If
            Finally
                MyBase.Dispose(disposing)
            End Try
        End Sub

        'Required by the Windows Form Designer
        Private components As System.ComponentModel.IContainer

        'NOTE: The following procedure is required by the Windows Form Designer
        'It can be modified using the Windows Form Designer.  
        'Do not modify it using the code editor.
        <System.Diagnostics.DebuggerStepThrough()> _
        Private Sub InitializeComponent()
            Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(Form1))
            Dim SerializableAppearanceObject1 As DevExpress.Utils.SerializableAppearanceObject = New DevExpress.Utils.SerializableAppearanceObject
            Me.LabelControl1 = New DevExpress.XtraEditors.LabelControl
            Me.LabelControl_Address = New DevExpress.XtraEditors.LabelControl
            Me.LabelControl3 = New DevExpress.XtraEditors.LabelControl
            Me.LabelControl4 = New DevExpress.XtraEditors.LabelControl
            Me.LabelControl5 = New DevExpress.XtraEditors.LabelControl
            Me.LabelControl_current_balance = New DevExpress.XtraEditors.LabelControl
            Me.CalcEdit_amount = New DevExpress.XtraEditors.CalcEdit
            Me.LabelControl_ending_balance = New DevExpress.XtraEditors.LabelControl
            Me.LabelControl8 = New DevExpress.XtraEditors.LabelControl
            Me.ComboBoxEdit_reason = New DevExpress.XtraEditors.ComboBoxEdit
            Me.ComboBoxEdit_disposition = New DevExpress.XtraEditors.ComboBoxEdit
            Me.LabelControl9 = New DevExpress.XtraEditors.LabelControl
            Me.SimpleButton_ok = New DevExpress.XtraEditors.SimpleButton
            Me.SimpleButton_cancel = New DevExpress.XtraEditors.SimpleButton
            Me.ClientID1 = New DebtPlus.UI.Client.Widgets.Controls.ClientID
            CType(Me.DxErrorProvider1, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.CalcEdit_amount.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.ComboBoxEdit_reason.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.ComboBoxEdit_disposition.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.ClientID1.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
            Me.SuspendLayout()
            '
            'LabelControl1
            '
            Me.LabelControl1.Location = New System.Drawing.Point(13, 13)
            Me.LabelControl1.Name = "LabelControl1"
            Me.LabelControl1.Size = New System.Drawing.Size(27, 13)
            Me.LabelControl1.TabIndex = 0
            Me.LabelControl1.Text = "C&lient"
            '
            'LabelControl_Address
            '
            Me.LabelControl_Address.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                                                     Or System.Windows.Forms.AnchorStyles.Left) _
                                                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
            Me.LabelControl_Address.Appearance.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Int32), CType(CType(255, Byte), Int32), CType(CType(192, Byte), Int32))
            Me.LabelControl_Address.Appearance.Options.UseBackColor = True
            Me.LabelControl_Address.Appearance.Options.UseTextOptions = True
            Me.LabelControl_Address.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
            Me.LabelControl_Address.Appearance.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Top
            Me.LabelControl_Address.Appearance.TextOptions.WordWrap = DevExpress.Utils.WordWrap.NoWrap
            Me.LabelControl_Address.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None
            Me.LabelControl_Address.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Office2003
            Me.LabelControl_Address.Location = New System.Drawing.Point(12, 39)
            Me.LabelControl_Address.Name = "LabelControl_Address"
            Me.LabelControl_Address.Padding = New System.Windows.Forms.Padding(4)
            Me.LabelControl_Address.Size = New System.Drawing.Size(229, 103)
            Me.LabelControl_Address.TabIndex = 2
            Me.LabelControl_Address.UseMnemonic = False
            '
            'LabelControl3
            '
            Me.LabelControl3.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
            Me.LabelControl3.Location = New System.Drawing.Point(268, 49)
            Me.LabelControl3.Name = "LabelControl3"
            Me.LabelControl3.Size = New System.Drawing.Size(77, 13)
            Me.LabelControl3.TabIndex = 3
            Me.LabelControl3.Text = "Current Balance"
            '
            'LabelControl4
            '
            Me.LabelControl4.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
            Me.LabelControl4.Location = New System.Drawing.Point(268, 71)
            Me.LabelControl4.Name = "LabelControl4"
            Me.LabelControl4.Size = New System.Drawing.Size(37, 13)
            Me.LabelControl4.TabIndex = 5
            Me.LabelControl4.Text = "&Amount"
            '
            'LabelControl5
            '
            Me.LabelControl5.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
            Me.LabelControl5.Location = New System.Drawing.Point(268, 98)
            Me.LabelControl5.Name = "LabelControl5"
            Me.LabelControl5.Size = New System.Drawing.Size(72, 13)
            Me.LabelControl5.TabIndex = 7
            Me.LabelControl5.Text = "Ending Balance"
            '
            'LabelControl_current_balance
            '
            Me.LabelControl_current_balance.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
            Me.LabelControl_current_balance.Appearance.Options.UseTextOptions = True
            Me.LabelControl_current_balance.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
            Me.LabelControl_current_balance.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None
            Me.LabelControl_current_balance.Location = New System.Drawing.Point(351, 49)
            Me.LabelControl_current_balance.Name = "LabelControl_current_balance"
            Me.LabelControl_current_balance.Size = New System.Drawing.Size(85, 13)
            Me.LabelControl_current_balance.TabIndex = 4
            Me.LabelControl_current_balance.Text = "$0.00"
            Me.LabelControl_current_balance.UseMnemonic = False
            '
            'CalcEdit_amount
            '
            Me.CalcEdit_amount.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
            Me.CalcEdit_amount.Location = New System.Drawing.Point(351, 68)
            Me.CalcEdit_amount.Name = "CalcEdit_amount"
            Me.CalcEdit_amount.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[False]
            Me.CalcEdit_amount.Properties.Appearance.Options.UseTextOptions = True
            Me.CalcEdit_amount.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
            Me.CalcEdit_amount.Properties.AppearanceDisabled.Options.UseTextOptions = True
            Me.CalcEdit_amount.Properties.AppearanceDisabled.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
            Me.CalcEdit_amount.Properties.AppearanceDropDown.Options.UseTextOptions = True
            Me.CalcEdit_amount.Properties.AppearanceDropDown.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
            Me.CalcEdit_amount.Properties.AppearanceFocused.Options.UseTextOptions = True
            Me.CalcEdit_amount.Properties.AppearanceFocused.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
            Me.CalcEdit_amount.Properties.AppearanceReadOnly.Options.UseTextOptions = True
            Me.CalcEdit_amount.Properties.AppearanceReadOnly.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
            Me.CalcEdit_amount.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
            Me.CalcEdit_amount.Properties.DisplayFormat.FormatString = "{0:c}"
            Me.CalcEdit_amount.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
            Me.CalcEdit_amount.Properties.EditFormat.FormatString = "{0:f2}"
            Me.CalcEdit_amount.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric
            Me.CalcEdit_amount.Properties.Mask.BeepOnError = True
            Me.CalcEdit_amount.Properties.Mask.EditMask = "c"
            Me.CalcEdit_amount.Properties.Precision = 2
            Me.CalcEdit_amount.Size = New System.Drawing.Size(100, 20)
            Me.CalcEdit_amount.TabIndex = 6
            '
            'LabelControl_ending_balance
            '
            Me.LabelControl_ending_balance.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
            Me.LabelControl_ending_balance.Appearance.Options.UseTextOptions = True
            Me.LabelControl_ending_balance.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
            Me.LabelControl_ending_balance.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None
            Me.LabelControl_ending_balance.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder
            Me.LabelControl_ending_balance.LineColor = System.Drawing.Color.Transparent
            Me.LabelControl_ending_balance.Location = New System.Drawing.Point(351, 98)
            Me.LabelControl_ending_balance.Name = "LabelControl_ending_balance"
            Me.LabelControl_ending_balance.Size = New System.Drawing.Size(85, 13)
            Me.LabelControl_ending_balance.TabIndex = 8
            Me.LabelControl_ending_balance.Text = "$0.00"
            Me.LabelControl_ending_balance.UseMnemonic = False
            '
            'LabelControl8
            '
            Me.LabelControl8.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
            Me.LabelControl8.Location = New System.Drawing.Point(12, 152)
            Me.LabelControl8.Name = "LabelControl8"
            Me.LabelControl8.Size = New System.Drawing.Size(102, 13)
            Me.LabelControl8.TabIndex = 9
            Me.LabelControl8.Text = "Reason for the check"
            '
            'ComboBoxEdit_reason
            '
            Me.ComboBoxEdit_reason.Anchor = CType(((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left) _
                                                   Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
            Me.ComboBoxEdit_reason.Location = New System.Drawing.Point(120, 149)
            Me.ComboBoxEdit_reason.Name = "ComboBoxEdit_reason"
            Me.ComboBoxEdit_reason.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
            Me.ComboBoxEdit_reason.Properties.MaxLength = 50
            Me.ComboBoxEdit_reason.Size = New System.Drawing.Size(331, 20)
            Me.ComboBoxEdit_reason.TabIndex = 10
            '
            'ComboBoxEdit_disposition
            '
            Me.ComboBoxEdit_disposition.Anchor = CType(((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left) _
                                                        Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
            Me.ComboBoxEdit_disposition.Location = New System.Drawing.Point(120, 175)
            Me.ComboBoxEdit_disposition.Name = "ComboBoxEdit_disposition"
            Me.ComboBoxEdit_disposition.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
            Me.ComboBoxEdit_disposition.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor
            Me.ComboBoxEdit_disposition.Size = New System.Drawing.Size(331, 20)
            Me.ComboBoxEdit_disposition.TabIndex = 12
            '
            'LabelControl9
            '
            Me.LabelControl9.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
            Me.LabelControl9.Location = New System.Drawing.Point(12, 178)
            Me.LabelControl9.Name = "LabelControl9"
            Me.LabelControl9.Size = New System.Drawing.Size(83, 13)
            Me.LabelControl9.TabIndex = 11
            Me.LabelControl9.Text = "Check Disposition"
            '
            'SimpleButton_ok
            '
            Me.SimpleButton_ok.Anchor = System.Windows.Forms.AnchorStyles.Bottom
            Me.SimpleButton_ok.Enabled = False
            Me.SimpleButton_ok.Location = New System.Drawing.Point(142, 221)
            Me.SimpleButton_ok.Name = "SimpleButton_ok"
            Me.SimpleButton_ok.Size = New System.Drawing.Size(75, 23)
            Me.SimpleButton_ok.TabIndex = 13
            Me.SimpleButton_ok.Text = "&Record"
            '
            'SimpleButton_cancel
            '
            Me.SimpleButton_cancel.Anchor = System.Windows.Forms.AnchorStyles.Bottom
            Me.SimpleButton_cancel.CausesValidation = False
            Me.SimpleButton_cancel.DialogResult = System.Windows.Forms.DialogResult.Cancel
            Me.SimpleButton_cancel.Location = New System.Drawing.Point(245, 221)
            Me.SimpleButton_cancel.Name = "SimpleButton_cancel"
            Me.SimpleButton_cancel.Size = New System.Drawing.Size(75, 23)
            Me.SimpleButton_cancel.TabIndex = 14
            Me.SimpleButton_cancel.Text = "&Cancel"
            '
            'ClientID1
            '
            Me.ClientID1.Location = New System.Drawing.Point(85, 10)
            Me.ClientID1.Name = "ClientID1"
            Me.ClientID1.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[False]
            Me.ClientID1.Properties.Appearance.Options.UseTextOptions = True
            Me.ClientID1.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
            Me.ClientID1.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph, "", -1, True, True, False, DevExpress.XtraEditors.ImageLocation.MiddleCenter, CType(resources.GetObject("ClientID2.Properties.Buttons"), System.Drawing.Image), New DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), SerializableAppearanceObject1, "Click here to search for a client ID", "search", Nothing, True)})
            Me.ClientID1.Properties.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
            Me.ClientID1.Properties.DisplayFormat.FormatString = "0000000"
            Me.ClientID1.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Custom
            Me.ClientID1.Properties.EditFormat.FormatString = "f0"
            Me.ClientID1.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric
            Me.ClientID1.Properties.Mask.BeepOnError = True
            Me.ClientID1.Properties.Mask.EditMask = "\d*"
            Me.ClientID1.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.RegEx
            Me.ClientID1.Properties.ValidateOnEnterKey = True
            Me.ClientID1.Size = New System.Drawing.Size(100, 20)
            Me.ClientID1.TabIndex = 1
            '
            'Form1
            '
            Me.AcceptButton = Me.SimpleButton_ok
            Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
            Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
            Me.CancelButton = Me.SimpleButton_cancel
            Me.ClientSize = New System.Drawing.Size(463, 266)
            Me.Controls.Add(Me.LabelControl_ending_balance)
            Me.Controls.Add(Me.CalcEdit_amount)
            Me.Controls.Add(Me.LabelControl_current_balance)
            Me.Controls.Add(Me.LabelControl5)
            Me.Controls.Add(Me.LabelControl4)
            Me.Controls.Add(Me.LabelControl3)
            Me.Controls.Add(Me.SimpleButton_cancel)
            Me.Controls.Add(Me.SimpleButton_ok)
            Me.Controls.Add(Me.LabelControl9)
            Me.Controls.Add(Me.ComboBoxEdit_disposition)
            Me.Controls.Add(Me.ComboBoxEdit_reason)
            Me.Controls.Add(Me.LabelControl8)
            Me.Controls.Add(Me.LabelControl_Address)
            Me.Controls.Add(Me.LabelControl1)
            Me.Controls.Add(Me.ClientID1)
            Me.Name = "Form1"
            Me.Text = "Client Refund Check Entry"
            CType(Me.DxErrorProvider1, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.CalcEdit_amount.Properties, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.ComboBoxEdit_reason.Properties, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.ComboBoxEdit_disposition.Properties, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.ClientID1.Properties, System.ComponentModel.ISupportInitialize).EndInit()
            Me.ResumeLayout(False)
            Me.PerformLayout()

        End Sub
        Friend WithEvents LabelControl1 As DevExpress.XtraEditors.LabelControl
        Friend WithEvents LabelControl_Address As DevExpress.XtraEditors.LabelControl
        Friend WithEvents LabelControl3 As DevExpress.XtraEditors.LabelControl
        Friend WithEvents LabelControl4 As DevExpress.XtraEditors.LabelControl
        Friend WithEvents LabelControl5 As DevExpress.XtraEditors.LabelControl
        Friend WithEvents LabelControl_current_balance As DevExpress.XtraEditors.LabelControl
        Friend WithEvents CalcEdit_amount As DevExpress.XtraEditors.CalcEdit
        Friend WithEvents LabelControl_ending_balance As DevExpress.XtraEditors.LabelControl
        Friend WithEvents LabelControl8 As DevExpress.XtraEditors.LabelControl
        Friend WithEvents ComboBoxEdit_reason As DevExpress.XtraEditors.ComboBoxEdit
        Friend WithEvents ComboBoxEdit_disposition As DevExpress.XtraEditors.ComboBoxEdit
        Friend WithEvents LabelControl9 As DevExpress.XtraEditors.LabelControl
        Friend WithEvents SimpleButton_ok As DevExpress.XtraEditors.SimpleButton
        Friend WithEvents SimpleButton_cancel As DevExpress.XtraEditors.SimpleButton
        Friend WithEvents ClientID1 As DebtPlus.UI.Client.Widgets.Controls.ClientID

    End Class
End Namespace