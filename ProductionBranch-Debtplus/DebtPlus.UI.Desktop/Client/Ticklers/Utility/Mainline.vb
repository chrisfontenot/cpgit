#Region "Copyright 2000-2012 DebtPlus, L.L.C."

'{*******************************************************************}
'{                                                                   }
'{       DebtPlus Debt Management System                             }
'{                                                                   }
'{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
'{       ALL RIGHTS RESERVED                                         }
'{                                                                   }
'{   The entire contents of this file is protected by U.S. and       }
'{   International Copyright Laws. Unauthorized reproduction,        }
'{   reverse-engineering, and distribution of all or any portion of  }
'{   the code contained in this file is strictly prohibited and may  }
'{   result in severe civil and criminal penalties and will be       }
'{   prosecuted to the maximum extent possible under the law.        }
'{                                                                   }
'{   RESTRICTIONS                                                    }
'{                                                                   }
'{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
'{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
'{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
'{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
'{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
'{                                                                   }
'{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
'{   ADDITIONAL RESTRICTIONS.                                        }
'{                                                                   }
'{*******************************************************************}

#End Region

Option Compare Binary
Option Explicit On
Option Strict On

Imports DebtPlus.Svc.DataSets
Imports DebtPlus.UI.Client.Ticklers
Imports DebtPlus.Interfaces.Desktop
Imports System.Windows.Forms

Namespace Client.Ticklers.Utility

    Public Class Mainline
        Implements IDesktopMainline

        ''' <summary>
        ''' Mainline routine to edit the ticklers
        ''' </summary>
        Public Sub OldAppMain(ByVal args() As String) Implements IDesktopMainline.OldAppMain
            Dim ap As New MyArgumentParser
            If ap.Parse(args) Then

                ' Start a new thread to handle the sequencing from this point.
                ' We need to return to the main menu long before the dialogs are completed.
                If ap.Parse(args) Then
                    Dim thrd As New System.Threading.Thread(New System.Threading.ParameterizedThreadStart(AddressOf UpdateThread))
                    thrd.SetApartmentState(Threading.ApartmentState.STA)
                    thrd.IsBackground = False
                    thrd.Name = "TicklerUpdate"
                    thrd.Start(ap)
                End If
            End If
        End Sub

        ''' <summary>
        ''' Thread processing for the tickler update
        ''' </summary>
        Private Sub UpdateThread(ByVal obj As Object)
            Dim ap As MyArgumentParser = DirectCast(obj, MyArgumentParser)

            ' Process the counselor list
            Using frm As New DebtPlus.UI.Client.Ticklers.Form_Ticklers_CounselorList(DebtPlus.UI.Client.Ticklers.Form_Ticklers_List.DefaultCounselor())
                frm.ShowDialog()
            End Using
        End Sub
    End Class
End Namespace