Option Compare Binary
Option Explicit On
Option Strict On

Imports DebtPlus.Interfaces.Desktop

Namespace Client.Update.Utility

    Public Class Mainline
        Implements IDesktopMainline

        Public Sub OldAppMain(ByVal args() As String) Implements IDesktopMainline.OldAppMain
            Dim ap As New ArgParser

            ' Start a new thread to handle the sequencing from this point.
            ' We need to return to the main menu long before the dialogs are completed.
            If ap.Parse(args) Then
                Dim thrd As New System.Threading.Thread(New System.Threading.ParameterizedThreadStart(AddressOf ClientUpdateThread))
                thrd.SetApartmentState(Threading.ApartmentState.STA)
                thrd.IsBackground = False
                thrd.Name = "ClientUpdate"
                thrd.Start(ap)
            End If
        End Sub

        Private Sub ClientUpdateThread(ByVal obj As Object)
            Dim ap As ArgParser = DirectCast(obj, ArgParser)
            With New ClientUpdateMain(ap.Client, ap.noAlerts)
                .ShowDialog()
            End With
        End Sub
    End Class

End Namespace
