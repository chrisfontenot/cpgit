Option Compare Binary
Option Explicit On
Option Strict On

Imports System.Globalization

Namespace Client.Update.Utility
    Friend Class ArgParser
        Inherits DebtPlus.Utils.ArgParserBase

        ''' <summary>
        ''' Supress alert messages for the client
        ''' </summary>
        Private _noAlerts As Boolean = False

        Public ReadOnly Property noAlerts() As Boolean
            Get
                Return _noAlerts
            End Get
        End Property

        ''' <summary>
        ''' Client for the update operation
        ''' </summary>
        Private _client As Int32 = -1

        Public ReadOnly Property Client() As Int32
            Get
                Return _client
            End Get
        End Property

        ''' <summary>
        ''' Create an instance of our class
        ''' </summary>
        Public Sub New()
            MyBase.New(New String() {"a", "c"})
        End Sub

        ''' <summary>
        ''' Process the switch options
        ''' </summary>
        Protected Overrides Function OnSwitch(ByVal switchSymbol As String, ByVal switchValue As String) As SwitchStatus
            Dim ss As SwitchStatus = DebtPlus.Utils.ArgParserBase.SwitchStatus.NoError
            Select Case switchSymbol
                Case "a"
                    _noAlerts = True
                Case "c"
                    Dim TempClient As Double
                    If Double.TryParse(switchValue, NumberStyles.Integer, CultureInfo.InvariantCulture, TempClient) Then
                        If TempClient >= 0 AndAlso TempClient <= Convert.ToDouble(Int32.MaxValue) Then
                            _client = Convert.ToInt32(TempClient)
                            Return ss
                        End If
                    End If
                    ss = DebtPlus.Utils.ArgParserBase.SwitchStatus.YesError
                Case "?"
                    ss = DebtPlus.Utils.ArgParserBase.SwitchStatus.ShowUsage
                Case Else
                    ss = DebtPlus.Utils.ArgParserBase.SwitchStatus.YesError
            End Select
            Return ss
        End Function

        ''' <summary>
        ''' Generate the command usage information
        ''' </summary>
        Protected Overrides Sub OnUsage(ByVal errorInfo As String)
            'AppendErrorLine("Usage: " + fname + " [-a] [-c#]")
            'AppendErrorLine("       -a : supress alert notes")
            'AppendErrorLine("       -c : specify the client for the update operation")
        End Sub
    End Class
End NameSpace
