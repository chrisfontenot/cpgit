#Region "Copyright 2000-2012 DebtPlus, L.L.C."

'{*******************************************************************}
'{                                                                   }
'{       DebtPlus Debt Management System                             }
'{                                                                   }
'{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
'{       ALL RIGHTS RESERVED                                         }
'{                                                                   }
'{   The entire contents of this file is protected by U.S. and       }
'{   International Copyright Laws. Unauthorized reproduction,        }
'{   reverse-engineering, and distribution of all or any portion of  }
'{   the code contained in this file is strictly prohibited and may  }
'{   result in severe civil and criminal penalties and will be       }
'{   prosecuted to the maximum extent possible under the law.        }
'{                                                                   }
'{   RESTRICTIONS                                                    }
'{                                                                   }
'{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
'{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
'{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
'{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
'{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
'{                                                                   }
'{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
'{   ADDITIONAL RESTRICTIONS.                                        }
'{                                                                   }
'{*******************************************************************}

#End Region

Option Compare Binary
Option Explicit On
Option Strict On

Imports DebtPlus.Interfaces.Client
Imports DebtPlus.UI.Client.Widgets.Search
Imports System.Threading
Imports DebtPlus.UI.Client.Service
Imports System.Windows.Forms

Namespace Client.Update.Utility
    Public Class ClientUpdateMain
        Inherits Object

        Private m_NoAlerts As Boolean = False
        Private ClientID As Int32 = -1

        Public Sub New()
            MyBase.new()
        End Sub

        Public Sub New(ByVal ClientId As Int32)
            MyClass.New(ClientId, False)
        End Sub

        Public Sub New(ByVal ClientId As Int32, ByVal NoAlerts As Boolean)
            MyClass.New()
            Me.ClientID = ClientId
            m_NoAlerts = NoAlerts
        End Sub

        Public Sub New(ByVal Client As String)
            MyClass.New(Client, False)
        End Sub

        Public Sub New(ByVal Client As String, ByVal NoAlerts As Boolean)
            MyClass.New()
            If Client <> String.Empty Then
                Try
                    ClientID = Convert.ToInt32(Client)
                Catch ex As Exception
                    ClientID = -1
                End Try
            End If
            m_NoAlerts = NoAlerts
        End Sub

        Public Sub ShowDialog()

            ' If the client is not present then search for it
            If ClientID < 0 Then
                Using search As New DebtPlus.UI.Client.Widgets.Search.ClientSearchClass()
                    If search.PerformClientSearch = DialogResult.Cancel Then Return
                    ClientID = search.ClientId
                End Using
            End If

            If Not m_NoAlerts Then
                Dim thrd As New Thread(AddressOf ShowAlertNotes)
                With thrd
                    .IsBackground = True
                    .SetApartmentState(ApartmentState.STA)
                    .Name = "ShowAlerts"
                    .Start()
                End With
            End If

            ' Display the client
            Using cuc As ClientUpdateClass = New ClientUpdateClass()
                cuc.ShowEditDialog(ClientID, False)
            End Using
        End Sub

        Private Sub ShowAlertNotes()
            Dim Alerts As IAlertNotes = New Notes.AlertNotes.Client()
            Try
                Alerts.ShowAlerts(ClientID)

            Finally
                If TypeOf Alerts Is IDisposable Then
                    CType(Alerts, IDisposable).Dispose()
                End If
            End Try
        End Sub
    End Class
End Namespace