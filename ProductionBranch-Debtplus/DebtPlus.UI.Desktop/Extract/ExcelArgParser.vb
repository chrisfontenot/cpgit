Option Compare Binary
Option Explicit On
Option Strict On

Namespace Extract
    Public Class ExcelArgParser
        Inherits DebtPlus.Utils.ArgParserBase

        Private ReadOnly _configFilePath As String
        Private ReadOnly _privateFiles As New List(Of String)
        Public ReadOnly Property Files() As List(Of String)
            Get
                Return _privateFiles
            End Get
        End Property

        Private _commandFile As String = String.Empty

        Public ReadOnly Property CommandFile() As String
            Get
                Return _commandFile
            End Get
        End Property

        Public Sub New()
            MyBase.New(New String() {})
            _configFilePath = ""
        End Sub

        Public Sub New(ByVal configFilePath As String)
            MyBase.New(New String() {})
            _configFilePath = configFilePath
        End Sub

        Protected Overrides Sub OnUsage(ByVal errorInfo As String)
            If errorInfo IsNot Nothing Then
                AppendErrorLine(String.Format("Invalid parameter: {0}" + Environment.NewLine, errorInfo))
            End If

            AppendErrorLine("Usage: DebtPlus.Extract.CSV.exe ControlFile.xml")
        End Sub

        Protected Overrides Function OnNonSwitch(ByVal switchValue As String) As SwitchStatus
            Dim ss As SwitchStatus = DebtPlus.Utils.ArgParserBase.SwitchStatus.NoError
            If _commandFile = String.Empty Then
                _commandFile = ParseAndQualifyFileName(switchValue)
            Else
                _privateFiles.Add(switchValue)
            End If
            Return ss
        End Function

        Protected Overrides Function OnDoneParse() As SwitchStatus
            Dim ss As SwitchStatus = DebtPlus.Utils.ArgParserBase.SwitchStatus.NoError
            If _commandFile = String.Empty Then
                AppendErrorLine("This program requires a control file.")
                AppendErrorLine("")
                ss = DebtPlus.Utils.ArgParserBase.SwitchStatus.ShowUsage
            End If
            Return ss
        End Function

        Public ArgumentList() As String

        Protected Shadows Function Parse(ByVal arguments() As String) As Boolean
            ArgumentList = arguments
            Return MyBase.Parse(arguments)
        End Function

        Private Function ParseAndQualifyFileName(ByVal fileArg As String) As String
            Dim fileName As String = fileArg.Trim()

            ' Remove the quotes if there are any
            Dim fnStart As Int32 = fileArg.IndexOf("""")
            If fnStart >= 0 Then
                Dim fnEnd As Int32 = fileArg.LastIndexOf("""")
                fileName = fileArg.Substring(fnStart + 1, fnEnd - fnStart - 1)
            End If

            ' If the file name is not rooted then join it with the current directory
            If Not System.IO.Path.IsPathRooted(fileName) Then
                fileName = System.IO.Path.Combine(System.Environment.CurrentDirectory, fileName)
            End If

            ' Ensure that the file exists
            If Not System.IO.File.Exists(fileName) Then
                DebtPlus.Data.Forms.MessageBox.Show(String.Format("The filename {0} can not be found.", fileName), "Error opening config file", Windows.Forms.MessageBoxButtons.OK, Windows.Forms.MessageBoxIcon.Error)
                Return Nothing
            End If

            Return fileName
        End Function
    End Class
End Namespace
