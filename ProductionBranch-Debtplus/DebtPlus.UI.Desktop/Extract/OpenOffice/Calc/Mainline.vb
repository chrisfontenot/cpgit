#Region "Copyright 2000-2012 DebtPlus, L.L.C."
'{*******************************************************************}
'{                                                                   }
'{       DebtPlus Debt Management System                             }
'{                                                                   }
'{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
'{       ALL RIGHTS RESERVED                                         }
'{                                                                   }
'{   The entire contents of this file is protected by U.S. and       }
'{   International Copyright Laws. Unauthorized reproduction,        }
'{   reverse-engineering, and distribution of all or any portion of  }
'{   the code contained in this file is strictly prohibited and may  }
'{   result in severe civil and criminal penalties and will be       }
'{   prosecuted to the maximum extent possible under the law.        }
'{                                                                   }
'{   RESTRICTIONS                                                    }
'{                                                                   }
'{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
'{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
'{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
'{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
'{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
'{                                                                   }
'{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
'{   ADDITIONAL RESTRICTIONS.                                        }
'{                                                                   }
'{*******************************************************************}

#End Region

Option Compare Binary
Option Explicit Off
Option Strict Off

Imports DevExpress.Utils
Imports System.Xml.Serialization
Imports System.IO
Imports DebtPlus.Reports.Template.Forms
Imports System.Data.SqlClient
Imports DebtPlus.Interfaces.Desktop
Imports System.Windows.Forms

Namespace Extract.OpenOffice.Calc
    Public Class Mainline
        Implements IDesktopMainline

        ' global storage for the program
        Friend ap As New ExcelArgParser(DebtPlus.Configuration.Config.GetConfigValueForKey(DebtPlus.Configuration.Config.GetConfigSettings(DebtPlus.Configuration.Config.ConfigSet.ExtractOpenOfficeCalc), "configFilePath"))
        Private Waitdlg As WaitDialogForm
        Private Waitdlg_caption As String = String.Empty
        Private ds As DataSet
        Private tbl As DataTable

        Public Sub OldAppMain(ByVal args() As String) Implements IDesktopMainline.OldAppMain
            If ap.Parse(args) Then
                Try
                    Dim serializer As New XmlSerializer(GetType(List(Of Sheet)))
                    Using inputfile As New StreamReader(ap.CommandFile)
                        Dim sheets As List(Of Sheet) = CType(serializer.Deserialize(inputfile), List(Of Sheet))
                        inputfile.Close()

                        ' Set the pointer to the new application
                        If sheets.Count > 0 Then
                            ProcessAllSheets(sheets)
                        End If
                    End Using

                Catch ex As Exception
                    DebtPlus.Svc.Common.ErrorHandling.HandleErrors(ex)

                    ' Trap all other error conditions here
                    DebtPlus.Data.Forms.MessageBox.Show(ex.ToString(), "Error processing sheet", MessageBoxButtons.OK, MessageBoxIcon.Hand)
                End Try
            End If
        End Sub

        Private Sub ProcessAllSheets(ByVal Sheets As List(Of Sheet))

            ' Create the connection to the Calc service
            Dim oSM As Object = CreateObject("com.sun.star.ServiceManager")
            If oSM Is Nothing Then
                Throw New ApplicationException("Unable to create ServiceManager object")
            End If

            ' Create the desktop
            Dim oDesk As Object = oSM.createInstance("com.sun.star.frame.Desktop")
            If oDesk Is Nothing Then
                Throw New ApplicationException("Unable to create a Desktop instance")
            End If

            For Each sht As Sheet In Sheets
                If sht.hasOption("exist") Then
                    If sht.ResolveName() Then
                        ProcessExistingSheet(oSM, oDesk, sht)
                    End If
                Else
                    ProcessNewSheet(oSM, oDesk, sht)
                End If
            Next
        End Sub

        Private Sub ProcessNewSheet(ByVal oSM As Object, ByVal oDesk As Object, ByVal sht As Sheet)

            Dim oDoc As Object = oDesk.LoadComponentFromURL("private:factory/scalc", "_blank", 0, dummyArray)
            If oDoc Is Nothing Then
                Throw New ApplicationException("Unable to create the file")
            End If

            Dim TemporaryName As String = String.Empty
            Dim Successful As Boolean = False
            Try
                ProcessDocument(oSM, oDesk, oDoc, sht)

                If sht.hasOption("save") Then
                    If sht.SaveName() Then
                        TemporaryName = sht.File
                    End If
                Else
                    ' Allocate a temporary file name
                    TemporaryName = Path.GetTempFileName

                    ' Rename the file to have the extension of our current file
                    Dim NewName As String = TemporaryName + ".ods"
                    File.Move(TemporaryName, NewName)
                    TemporaryName = NewName
                End If

                ' Save the document back to the temporary location
                If TemporaryName <> String.Empty Then
                    Dim filterprops(0) As Object
                    filterprops(0) = MakePropertyValue(oSM, "FilterName", GetFilterFromName(TemporaryName))
                    oDoc.storeAsUrl(GetURL(TemporaryName), filterprops)
                    Successful = True
                End If

            Finally
                oDoc.close(True)
            End Try

            ' Start the editing of the document
            If TemporaryName <> String.Empty AndAlso Successful Then
                If sht.hasOption("show") Then
                    Process.Start(TemporaryName)
                End If
            End If
        End Sub

        Private Sub ProcessExistingSheet(ByVal oSM As Object, ByVal oDesk As Object, ByVal sht As Sheet)

            ' Allocate a temporary file name
            Dim TemporaryName As String

            If sht.hasOption("save") Then
                TemporaryName = sht.Name
            Else
                TemporaryName = Path.GetTempFileName

                ' Rename the file to have the extension of our current file
                Dim Ext As String = Path.GetExtension(sht.File)
                If String.IsNullOrEmpty(Ext) Then Ext = ".xls"

                Dim NewName As String = TemporaryName + Ext
                File.Move(TemporaryName, NewName)
                TemporaryName = NewName

                ' Copy the source file to the temporary location and evaluate the extract
                File.Delete(TemporaryName)
                File.Copy(sht.File, TemporaryName)
            End If

            ' Attempt to open the file
            Dim oDoc As Object = oDesk.LoadComponentFromURL(GetURL(TemporaryName), "_blank", 0, dummyArray)
            If oDoc Is Nothing Then
                Throw New ApplicationException("Unable to open the file " + TemporaryName)
            End If

            Dim Successful As Boolean = False
            Try
                ProcessDocument(oSM, oDesk, oDoc, sht)

                ' Save the document back to the temporary location
                If sht.hasOption("show") OrElse sht.hasOption("save") Then
                    Dim filterprops(0) As Object
                    filterprops(0) = MakePropertyValue(oSM, "FilterName", GetFilterFromName(TemporaryName))
                    oDoc.storeAsUrl(GetURL(TemporaryName), filterprops)
                End If

                Successful = True

            Finally
                oDoc.close(True)
            End Try

            ' Start the editing of the document
            If TemporaryName <> String.Empty AndAlso Successful Then
                If sht.hasOption("show") Then
                    Process.Start(TemporaryName)
                End If
            End If
        End Sub

        Private Sub ProcessDocument(ByVal oSM As Object, ByVal oDesk As Object, ByVal oDoc As Object, ByVal sht As Sheet)

            ' Evaluate the result set for the various queries on the sheet
            For Each qry As Query In sht.Queries
                ProcessQuery(oSM, oDesk, oDoc, sht, qry)
            Next
        End Sub

        Private Sub ProcessQuery(ByVal oSM As Object, ByVal oDesk As Object, ByVal oDoc As Object, ByVal sht As Sheet, ByVal qry As Query)
            Static FromDate As DateTime
            Static ToDate As DateTime
            Static RequestedDates As Boolean = False

            ' Request the date range if so desired
            If qry.NeedDateDialog Then
                If Not RequestedDates Then
                    RequestedDates = True

                    Dim dlg As New DateReportParametersForm
                    Dim answer As DialogResult = dlg.ShowDialog()
                    FromDate = dlg.Parameter_FromDate
                    ToDate = dlg.Parameter_ToDate
                    dlg.Dispose()

                    ' If the answer is not "OK" then terminate the application now.
                    If answer <> DialogResult.OK Then
                        Application.ExitThread()
                        Return
                    End If
                End If

                ' For the date values, find the corresponding items in the parameters
                For Each parm As Parameter In qry.Parameters
                    Select Case parm.DialogTypeValue
                        Case Parameter.DialogType.FromDate
                            parm.Value = FromDate.ToShortDateString
                        Case Parameter.DialogType.ToDate
                            parm.Value = ToDate.ToShortDateString + " 23:59:59"
                    End Select
                Next
            End If

            ' Decode the query information
            Dim cmd As SqlCommand = qry.BuildCommand()

            Waitdlg_caption = cmd.CommandText
            If Waitdlg_caption.Length > 25 Then Waitdlg_caption = Waitdlg_caption.Substring(0, 25)
            Waitdlg = New WaitDialogForm(Waitdlg_caption, "Processing Command")
            Waitdlg.Show()

            ' Issue the query
            Dim current_cursor As Cursor = Cursor.Current
            Try
                current_cursor = Cursors.WaitCursor
                cmd.Connection = New SqlConnection(DebtPlus.LINQ.SQLInfoClass.getDefault().ConnectionString)

                ds = New DataSet("ds")
                Using da As New SqlDataAdapter(cmd)
                    da.Fill(ds, "table1")
                End Using

                If ds.Tables.Count > 0 Then
                    tbl = ds.Tables(0)
                    If sht.Type = "extract" Then
                        Process_Extract(oSM, oDesk, oDoc, sht, qry, tbl)
                    ElseIf sht.Type = "values" Then
                        Process_Values(oSM, oDesk, oDoc, sht, qry, tbl)
                    End If
                End If

            Catch ex As SqlException
                DebtPlus.Svc.Common.ErrorHandling.HandleErrors(ex)
                DebtPlus.Data.Forms.MessageBox.Show(ex.ToString(), "Error reading values from database", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)

            Finally
                cmd.Dispose()
                Cursor.Current = current_cursor

                ' Dispose of the dialog when we are completed
                If Waitdlg IsNot Nothing Then
                    Waitdlg.Close()
                    Waitdlg.Dispose()
                End If
            End Try
        End Sub

        Private Sub Process_Extract(ByVal oSM As Object, ByVal oDesk As Object, ByVal oDoc As Object, ByVal sht As Sheet, ByVal qry As Query, ByVal tbl As DataTable)
            Dim oSpreadSheet As Object

            ' If there is no sheet specified, use the first sheet in the system
            ' If, by some chance, there are no sheets then create one with a standard name.
            If sht.Name = String.Empty Then
                oSpreadSheet = oDoc.getSheets().GetByIndex(0)
                If oSpreadSheet Is Nothing Then
                    Throw New ApplicationException("Spreadsheet is not defined and can not be created")
                End If

                Process_Extract(oSM, oDesk, oDoc, oSpreadSheet, sht, qry, tbl)
            Else

                ' If the value is a number, use the ordinal to find the sheet. We start with 1. Calc starts with 0.
                Dim SheetNumber As Int32 = -1
                If Int32.TryParse(sht.Name, SheetNumber) AndAlso SheetNumber >= 1 Then
                    oSpreadSheet = oDoc.getSheets().GetByIndex(SheetNumber - 1)
                    If oSpreadSheet IsNot Nothing Then
                        Process_Extract(oSM, oDesk, oDoc, oSpreadSheet, sht, qry, tbl)
                    End If

                Else

                    ' Name was specified. Either use the named object or create it.
                    Dim SheetList As Object = oDoc.Sheets
                    If Not SheetList.HasByName(sht.Name) Then
                        oDoc.getSheets.insertNewByName(sht.Name, 0)
                    End If
                    oSpreadSheet = oDoc.getSheets().GetByName(sht.Name)
                    Process_Extract(oSM, oDesk, oDoc, oSpreadSheet, sht, qry, tbl)
                End If
            End If
        End Sub

        Private Sub Process_Extract(ByVal oSM As Object, ByVal oDesk As Object, ByVal oDoc As Object, ByVal oSpreadSheet As Object, ByVal sht As Sheet, ByVal qry As Query, ByVal tbl As DataTable)
            Dim col As Int32
            Dim FirstLine As Int32 = sht.FirstLine

            ' Write the first line with the column names
            If sht.hasOption("headers") Then
                col = sht.FirstColumn - 1
                For Each DataCol As DataColumn In tbl.Columns
                    If DataCol.DataType IsNot GetType(Byte()) Then ' ignore timestamp columns
                        col += 1
                        SetValue(oSpreadSheet, Cell(FirstLine, col), DataCol.ColumnName)
                    End If
                Next
                FirstLine += 1
            End If

            ' Load the data for the item
            For Each row As DataRow In tbl.Rows
                col = sht.FirstColumn - 1
                For Each DataCol As DataColumn In tbl.Columns
                    Dim colName As String = DataCol.ColumnName

                    ' Just ignore and remove any byte() objects from the list
                    If DataCol.DataType Is GetType(Byte()) Then
                        Continue For
                    End If

                    ' Advance to the next column
                    col += 1

                    ' Find the value for the item. If null, ignore it now.
                    Dim value As Object = row(DataCol.Ordinal)
                    If value Is Nothing OrElse value Is System.DBNull.Value Then
                        Continue For
                    End If

                    ' Find the column in the list of columns for this query sheet
                    If qry.Columns IsNot Nothing AndAlso qry.Columns.Count > 0 Then
                        Dim colData As Extract.OpenOffice.Calc.Column = (From selCol In qry.Columns Where String.Compare(selCol.Name, colName, True) = 0 Select selCol).FirstOrDefault()
                        If colData IsNot Nothing Then
                            value = colData.Translate(value)
                        End If

                        SetValue(oSpreadSheet, Cell(FirstLine, col), value)
                    End If
                Next
                FirstLine += 1
            Next row
        End Sub

        Private Sub Retrieve_Values(ByVal row As DataRow, ByRef Page As String, ByRef Location As String, ByRef Value As Object)
            For Each DataCol As DataColumn In row.Table.Columns
                If DataCol.DataType IsNot GetType(Byte()) Then
                    Select Case DataCol.ColumnName.ToLower()
                        Case "sheet", "page"
                            Page = Convert.ToString(row(DataCol.Ordinal))
                        Case "location", "cell"
                            Location = Convert.ToString(row(DataCol.Ordinal))
                        Case "value"
                            Value = row(DataCol.Ordinal)
                        Case Else
                    End Select
                End If
            Next
        End Sub

        Private Sub Process_Values(ByVal oSM As Object, ByVal oDesk As Object, ByVal oDoc As Object, ByVal sht As Sheet, ByVal qry As Query, ByVal tbl As DataTable)
            Dim RowNumber As Int32 = 0

            Do While RowNumber < tbl.Rows.Count
                Dim CurrentPageName As String = String.Empty
                Dim CurrentLocation As String = String.Empty
                Dim CurrentValue As Object = Nothing
                Retrieve_Values(tbl.Rows(RowNumber), CurrentPageName, CurrentLocation, CurrentValue)

                ' Translate the page name to a spreadsheet location
                Dim SheetNumber As Int32 = -1
                If Int32.TryParse(CurrentPageName, SheetNumber) AndAlso SheetNumber >= 1 Then
                    Dim oSpreadSheet As Object = oDoc.getSheets().GetByIndex(SheetNumber - 1)
                    If oSpreadSheet IsNot Nothing Then
                        Process_Values(oSM, oDesk, oDoc, oSpreadSheet, sht, qry, tbl, RowNumber, CurrentPageName)
                    End If
                Else

                    ' Avoid the missing exception by making sure that the sheet first exists.
                    Dim SheetList As Object = oDoc.Sheets
                    If Not SheetList.HasByName(CurrentPageName) Then
                        oDoc.getSheets.insertNewByName(CurrentPageName, 0)
                    End If

                    Dim oSpreadSheet As Object = oDoc.getSheets().GetByName(CurrentPageName)
                    If oSpreadSheet IsNot Nothing Then
                        Process_Values(oSM, oDesk, oDoc, oSpreadSheet, sht, qry, tbl, RowNumber, CurrentPageName)
                    End If
                End If
            Loop
        End Sub

        Private Sub Process_Values(ByVal oSM As Object, ByVal oDesk As Object, ByVal oDoc As Object, ByVal oSpreadSheet As Object, ByVal sht As Sheet, ByVal qry As Query, ByVal tbl As DataTable, ByRef RowNumber As Int32, ByVal CurrentPageName As String)

            ' Find the first set of values for the current row
            Do While RowNumber < tbl.Rows.Count
                Dim Page As String = String.Empty
                Dim Location As String = String.Empty
                Dim Value As Object = Nothing

                Retrieve_Values(tbl.Rows(RowNumber), Page, Location, Value)
                If Page <> CurrentPageName Then Exit Do
                RowNumber += 1

                ' Ignore null values
                If Value Is Nothing OrElse Value Is System.DBNull.Value Then
                    Continue Do
                End If

                ' Find any mapping that is needed
                Dim colItem As Extract.OpenOffice.Calc.Column = (From colSearch In qry.Columns Where String.Compare(colSearch.Name, Location, True) = 0 Select colSearch).FirstOrDefault()
                If colItem IsNot Nothing Then
                    Value = colItem.Translate(Value)
                End If

                SetValue(oSpreadSheet, Location, Value)
            Loop
        End Sub

        Private Function Cell(ByVal Row As Int32, ByVal Col As Int32) As String
            Return ColString(Col - 1) + RowString(Row - 1)
        End Function

        Private Function RowString(ByVal row As Int32) As String
            If row < 0 Then
                Throw New ArgumentOutOfRangeException("row can not be negative")
            End If

            Return (row + 1).ToString()
        End Function

        Private Function ColString(ByVal col As Int32) As String
            If col < 0 Then
                Throw New ArgumentOutOfRangeException("col can not be negative")
            End If

            If col < 26 Then
                Return "ABCDEFGHIJKLMNOPQRSTUVWXYZ".Substring(col, 1)
            End If

            Return ColString((col \ 26) - 1) + ColString(col Mod 26)
        End Function

        Function MakePropertyValue(ByVal oSM As Object, ByVal PropName As String, ByVal PropValue As Object) As Object
            Dim Result As Object = oSM.Bridge_GetStruct("com.sun.star.beans.PropertyValue")
            With Result
                .Name = PropName
                .Value = PropValue
            End With

            Return Result
        End Function

        Private Sub SetValue(ByVal oSpreadSheet As Object, ByVal Location As String, ByVal Value As Object)
            Dim oCell As Object = oSpreadSheet.getCellRangeByName(Location)
            If oCell IsNot Nothing Then
                If TypeOf Value Is String Then
                    oCell.String = Value
                ElseIf TypeOf Value Is DateTime Then
                    oCell.String = Convert.ToDateTime(Value).ToShortDateString
                Else
                    oCell.Value = Value
                End If
            End If
        End Sub

        Function dummyArray() As Object
            ' creates an empty array for an empty list
            Dim Result(-1) As Object
            dummyArray = Result
        End Function

        Private Function GetURL(ByVal FileName As String) As String
            Dim answer As String = Path.GetFullPath(FileName).Replace("\"c, "/"c)
            answer = answer.Replace(":"c, "|"c)
            answer = answer.Replace(" ", "%20")
            Return "file:///" + answer
        End Function

        Private Function GetFilterFromName(ByVal FileName As String) As String
            Dim answer As String = "calc8"

            Dim ext As String = Path.GetExtension(FileName).Trim().ToLower()
            Select Case ext
                Case ".xls"
                    answer = "MS Excel 97"
                Case ".dif"
                    answer = "DIF"
                Case ".htm", ".html"
                    answer = "HTML (StarCalc)"
                Case ".ods"
                    answer = "calc8"
                Case Else
                    answer = "calc8"
            End Select

            Return answer
        End Function
    End Class
End Namespace