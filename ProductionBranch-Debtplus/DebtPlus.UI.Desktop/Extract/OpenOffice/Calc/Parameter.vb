#Region "Copyright 2000-2012 DebtPlus, L.L.C."
'{*******************************************************************}
'{                                                                   }
'{       DebtPlus Debt Management System                             }
'{                                                                   }
'{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
'{       ALL RIGHTS RESERVED                                         }
'{                                                                   }
'{   The entire contents of this file is protected by U.S. and       }
'{   International Copyright Laws. Unauthorized reproduction,        }
'{   reverse-engineering, and distribution of all or any portion of  }
'{   the code contained in this file is strictly prohibited and may  }
'{   result in severe civil and criminal penalties and will be       }
'{   prosecuted to the maximum extent possible under the law.        }
'{                                                                   }
'{   RESTRICTIONS                                                    }
'{                                                                   }
'{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
'{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
'{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
'{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
'{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
'{                                                                   }
'{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
'{   ADDITIONAL RESTRICTIONS.                                        }
'{                                                                   }
'{*******************************************************************}

#End Region

Option Compare Binary
Option Explicit Off
Option Strict Off

Imports System.Data.SqlClient

Namespace Extract.OpenOffice.Calc
    Public Class Parameter
        Public Enum DialogType
            [Standard] = 0
            [FromDate] = 1
            [ToDate] = 2
        End Enum

        Private privateDialogTypeValue As DialogType = DialogType.Standard

        Public Property DialogTypeValue() As DialogType
            Get
                Return privateDialogTypeValue
            End Get
            Set(ByVal value As DialogType)
                privateDialogTypeValue = value
            End Set
        End Property

        Private privateName As String = String.Empty

        Public Property Name() As String
            Get
                Return privateName
            End Get
            Set(ByVal value As String)
                privateName = value
            End Set
        End Property

        Private privateType As String = "int"

        Public Property Type() As String
            Get
                Return privateType
            End Get
            Set(ByVal value As String)
                privateType = value
            End Set
        End Property

        Private privateSize As Int32 = 0

        Public Property Size() As Int32
            Get
                Return privateSize
            End Get
            Set(ByVal value As Int32)
                privateSize = value
            End Set
        End Property

        Private privateValue As String = Nothing

        Public Property Value() As String
            Get
                Return privateValue
            End Get
            Set(ByVal value As String)
                Me.privateValue = value
            End Set
        End Property

        Private privateScale As Byte = CByte(0)

        Public Property Scale() As Byte
            Get
                Return privateScale
            End Get
            Set(ByVal value As Byte)
                privateScale = value
            End Set
        End Property

        Private privatePrecision As Byte = CByte(0)

        Public Property Precision() As Byte
            Get
                Return privatePrecision
            End Get
            Set(ByVal value As Byte)
                privatePrecision = value
            End Set
        End Property

        Private privateDirection As String = "input"

        Public Property Direction() As String
            Get
                Return privateDirection
            End Get
            Set(ByVal value As String)
                privateDirection = value
            End Set
        End Property

        Public Function GetDirection() As ParameterDirection
            Dim answer As ParameterDirection = ParameterDirection.Input
            Select Case Direction.ToLower()
                Case "input"
                    answer = ParameterDirection.Input
                Case "inputoutput"
                    answer = ParameterDirection.InputOutput
                Case "output"
                    answer = ParameterDirection.Output
                Case "returnvalue"
                    answer = ParameterDirection.ReturnValue
            End Select
            Return answer
        End Function

        Public Function GetDbType() As SqlDbType
            Dim answer As SqlDbType = SqlDbType.VarChar
            Select Case Type.ToLower()
                Case "bigInt" : answer = SqlDbType.BigInt
                Case "binary" : answer = SqlDbType.Binary
                Case "bit" : answer = SqlDbType.Bit
                Case "char" : answer = SqlDbType.Char
                Case "date" : answer = SqlDbType.Date
                Case "datetime" : answer = SqlDbType.DateTime
                Case "datetime2" : answer = SqlDbType.DateTime2
                Case "datetimeoffset" : answer = SqlDbType.DateTimeOffset
                Case "decimal" : answer = SqlDbType.Decimal
                Case "float" : answer = SqlDbType.Float
                Case "image" : answer = SqlDbType.Image
                Case "int" : answer = SqlDbType.Int
                Case "money" : answer = SqlDbType.Money
                Case "nchar" : answer = SqlDbType.NChar
                Case "ntext" : answer = SqlDbType.NText
                Case "nvarchar" : answer = SqlDbType.NVarChar
                Case "real" : answer = SqlDbType.Real
                Case "smalldatetime" : answer = SqlDbType.SmallDateTime
                Case "smallint" : answer = SqlDbType.SmallInt
                Case "smallmoney" : answer = SqlDbType.SmallMoney
                Case "structured" : answer = SqlDbType.Structured
                Case "text" : answer = SqlDbType.Text
                Case "time" : answer = SqlDbType.Time
                Case "timestamp" : answer = SqlDbType.Timestamp
                Case "tinyint" : answer = SqlDbType.TinyInt
                Case "udt" : answer = SqlDbType.Udt
                Case "uniqueidentifier" : answer = SqlDbType.UniqueIdentifier
                Case "varbinary" : answer = SqlDbType.VarBinary
                Case "varchar" : answer = SqlDbType.VarChar
                Case "variant" : answer = SqlDbType.Variant
                Case "xml" : answer = SqlDbType.Xml
            End Select

            Return answer
        End Function

        Public Function GetValue() As Object
            Dim answer As Object = DBNull.Value
            If Value IsNot Nothing Then
                Select Case Type.ToLower()
                    Case "bit"
                        Dim BooleanAnswer As Boolean
                        If Boolean.TryParse(Value, BooleanAnswer) Then answer = BooleanAnswer
                    Case "date", "datetime", "smalldatetime"
                        Dim DateAnswer As Date
                        If Date.TryParse(Value, DateAnswer) Then answer = DateAnswer
                    Case "decimal", "money"
                        Dim DecimalAnswer As Decimal
                        If Decimal.TryParse(Value, DecimalAnswer) Then answer = DecimalAnswer
                    Case "float", "real"
                        Dim DoubleAnswer As Double
                        If Double.TryParse(Value, DoubleAnswer) Then answer = DoubleAnswer
                    Case "int", "smallint", "tinyint"
                        Dim IntAnswer As Int32
                        If Int32.TryParse(Value, IntAnswer) Then answer = IntAnswer
                    Case Else
                        answer = Value
                End Select
            End If
            Return answer
        End Function

        Public Function BuldParameter() As SqlParameter
            Dim answer As New SqlParameter
            With answer
                .Direction = GetDirection()
                .SqlDbType = GetDbType()
                .ParameterName = Name
                .Value = GetValue()
                .SourceVersion = DataRowVersion.Current
                .Size = Size
                .Scale = Scale
                .Precision = Precision
            End With
            Return answer
        End Function
    End Class
End Namespace