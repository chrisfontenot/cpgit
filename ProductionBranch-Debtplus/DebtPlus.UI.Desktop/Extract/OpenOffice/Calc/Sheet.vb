#Region "Copyright 2000-2012 DebtPlus, L.L.C."
'{*******************************************************************}
'{                                                                   }
'{       DebtPlus Debt Management System                             }
'{                                                                   }
'{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
'{       ALL RIGHTS RESERVED                                         }
'{                                                                   }
'{   The entire contents of this file is protected by U.S. and       }
'{   International Copyright Laws. Unauthorized reproduction,        }
'{   reverse-engineering, and distribution of all or any portion of  }
'{   the code contained in this file is strictly prohibited and may  }
'{   result in severe civil and criminal penalties and will be       }
'{   prosecuted to the maximum extent possible under the law.        }
'{                                                                   }
'{   RESTRICTIONS                                                    }
'{                                                                   }
'{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
'{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
'{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
'{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
'{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
'{                                                                   }
'{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
'{   ADDITIONAL RESTRICTIONS.                                        }
'{                                                                   }
'{*******************************************************************}

#End Region

Option Compare Binary
Option Explicit Off
Option Strict Off

Imports System.IO
Imports System.Windows.Forms

Namespace Extract.OpenOffice.Calc
    Public Class Sheet
        Private privateName As String = String.Empty

        Public Property Name() As String
            Get
                Return privateName
            End Get
            Set(ByVal value As String)
                privateName = value.Trim()
            End Set
        End Property

        Private privateType As String = "extract"

        Public Property Type() As String
            Get
                Return privateType
            End Get
            Set(ByVal value As String)
                privateType = value.ToLower().Trim()
            End Set
        End Property

        Private privateTitle As String = String.Empty

        Public Property Title() As String
            Get
                Return privateTitle
            End Get
            Set(ByVal value As String)
                privateTitle = value.Trim()
            End Set
        End Property

        Private privateFile As String = "*.xls"

        Public Property File() As String
            Get
                Return privateFile
            End Get
            Set(ByVal value As String)
                privateFile = value
            End Set
        End Property

        Public Function SaveName() As Boolean
            Dim Name As String = File
            Dim answer As DialogResult = DialogResult.OK

            ' Replace the string [My Documents] with the current documents directory
            If Name.LastIndexOf("[My Documents]") >= 0 Then
                Dim DocumentsDirectory As String = Environment.GetFolderPath(Environment.SpecialFolder.Personal)
                File = Name.Replace("[My Documents]", DocumentsDirectory)
            End If

            ' If the file is a wildcard item then ask the user for the filename
            If Name.IndexOfAny(New Char() {"*"c, "?"c}) >= 0 Then
                Using dlg As New SaveFileDialog
                    With dlg
                        .AddExtension = True
                        .CheckFileExists = False
                        .DereferenceLinks = True
                        .CreatePrompt = False
                        .CheckPathExists = True
                        .DefaultExt = "xls"
                        .FileName = Path.GetFileName(File)
                        .InitialDirectory = Path.GetDirectoryName(File)
                        .Filter = "Excel 2009 Documents (*.xls)|*.xls|Excel 2010 Documents (*.xlsx)|*.xlsx|Open Office Document (*.ods)|*.ods|All Files (*.*)|*.*"
                        .FilterIndex = 1
                        .RestoreDirectory = True
                        .Title = Title
                        .ValidateNames = True

                        answer = .ShowDialog()
                        File = .FileName
                    End With
                End Using
            End If

            Return answer = DialogResult.OK
        End Function

        Public Function ResolveName() As Boolean
            Dim Name As String = File
            Dim answer As DialogResult = DialogResult.OK

            ' Replace the string [My Documents] with the current documents directory
            If Name.LastIndexOf("[My Documents]") >= 0 Then
                Dim DocumentsDirectory As String = Environment.GetFolderPath(Environment.SpecialFolder.Personal)
                File = Name.Replace("[My Documents]", DocumentsDirectory)
            End If

            ' If the file is a wildcard item then ask the user for the filename
            If Name.IndexOfAny(New Char() {"*"c, "?"c}) >= 0 Then
                Using dlg As New OpenFileDialog
                    With dlg
                        .AddExtension = True
                        .CheckFileExists = True
                        .CheckPathExists = True
                        .DefaultExt = "xls"
                        .DereferenceLinks = True
                        .FileName = Path.GetFileName(File)
                        .Filter = "Excel 2009 Documents (*.xls)|*.xls|Excel 2010 Documents (*.xlsx)|*.xlsx|Open Office Document (*.ods)|*.ods|All Files (*.*)|*.*"
                        .FilterIndex = 1
                        .InitialDirectory = Path.GetDirectoryName(File)
                        .Multiselect = False
                        .RestoreDirectory = True
                        .ShowReadOnly = False
                        .Title = Title
                        .ValidateNames = True

                        answer = .ShowDialog()
                        File = .FileName
                    End With
                End Using
            End If

            Return answer = DialogResult.OK
        End Function

        Private privateOptions As String = String.Empty

        Public Property Options() As String
            Get
                Return privateOptions
            End Get
            Set(ByVal value As String)
                privateOptions = value.ToLower().Replace(" ", "")
            End Set
        End Property

        Public Function hasOption(ByVal OptionString As String) As Boolean
            Return ("," + Options + ",").IndexOf("," + OptionString.ToLower() + ",") >= 0
        End Function

        Private privateFirstLine As Int32 = 1

        Public Property FirstLine() As Int32
            Get
                Return privateFirstLine
            End Get
            Set(ByVal value As Int32)
                privateFirstLine = value
            End Set
        End Property

        Private privateFirstColumn As Int32 = 1

        Public Property FirstColumn() As Int32
            Get
                Return privateFirstColumn
            End Get
            Set(ByVal value As Int32)
                privateFirstColumn = value
            End Set
        End Property

        Private privateQueries As New List(Of Query)

        Public Property Queries() As List(Of Query)
            Get
                Return privateQueries
            End Get
            Set(ByVal value As List(Of Query))
                privateQueries = value
            End Set
        End Property
    End Class
End Namespace