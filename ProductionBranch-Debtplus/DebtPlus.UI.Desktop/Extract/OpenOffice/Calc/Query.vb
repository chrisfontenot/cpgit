#Region "Copyright 2000-2012 DebtPlus, L.L.C."
'{*******************************************************************}
'{                                                                   }
'{       DebtPlus Debt Management System                             }
'{                                                                   }
'{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
'{       ALL RIGHTS RESERVED                                         }
'{                                                                   }
'{   The entire contents of this file is protected by U.S. and       }
'{   International Copyright Laws. Unauthorized reproduction,        }
'{   reverse-engineering, and distribution of all or any portion of  }
'{   the code contained in this file is strictly prohibited and may  }
'{   result in severe civil and criminal penalties and will be       }
'{   prosecuted to the maximum extent possible under the law.        }
'{                                                                   }
'{   RESTRICTIONS                                                    }
'{                                                                   }
'{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
'{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
'{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
'{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
'{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
'{                                                                   }
'{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
'{   ADDITIONAL RESTRICTIONS.                                        }
'{                                                                   }
'{*******************************************************************}

#End Region

Option Compare Binary
Option Explicit Off
Option Strict Off

Imports System.Data.SqlClient
Imports System.Text

Namespace Extract.OpenOffice.Calc
    Public Class Query
        Private privateCommand As String

        Public Property Command() As String
            Get
                Return privateCommand
            End Get
            Set(ByVal value As String)
                Dim sb As New StringBuilder(value)
                sb.Replace(ControlChars.Cr, "")
                sb.Replace(ControlChars.Lf, "")
                sb.Replace(ControlChars.Tab, "")
                sb.Replace(ControlChars.VerticalTab, "")
                privateCommand = sb.ToString().Trim()
            End Set
        End Property

        Private privateParameters As New List(Of Parameter)

        Public Property Parameters() As List(Of Parameter)
            Get
                Return privateParameters
            End Get
            Set(ByVal value As List(Of Parameter))
                privateParameters = value
            End Set
        End Property

        Private privateCommandTimeout As Int32 = 0

        Public Property CommandTimeout() As Int32
            Get
                Return privateCommandTimeout
            End Get
            Set(ByVal value As Int32)
                privateCommandTimeout = value
            End Set
        End Property

        Private privateType As String = "text"

        Public Property Type() As String
            Get
                Return privateType
            End Get
            Set(ByVal value As String)
                privateType = value
            End Set
        End Property

        Public Property Columns As System.Collections.Generic.List(Of Column)

        Public Function CommandType() As CommandType
            Dim answer As CommandType = System.Data.CommandType.Text
            Select Case Type.ToLower()
                Case "proc", "procedure", "storedprocedure"
                    answer = System.Data.CommandType.StoredProcedure
                Case "table"
                    answer = System.Data.CommandType.TableDirect
            End Select
            Return answer
        End Function

        Public Function BuildCommand() As SqlCommand
            Dim cmd As SqlCommand = New SqlCommand
            With cmd
                .CommandText = Command
                .CommandType = CommandType()
                .CommandTimeout = System.Math.Max(DebtPlus.LINQ.SQLInfoClass.getDefault().CommandTimeout, CommandTimeout)
                For Each parm As Parameter In Parameters
                    .Parameters.Add(parm.BuldParameter)
                Next
            End With
            Return cmd
        End Function

        Public Function NeedDateDialog() As Boolean
            Dim answer As Boolean = False
            For Each parm As Parameter In Parameters
                If parm.DialogTypeValue <> Parameter.DialogType.Standard Then
                    answer = True
                    Exit For
                End If
            Next
            Return answer
        End Function
    End Class
End Namespace