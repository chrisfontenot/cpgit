﻿Option Compare Binary
Option Explicit On
Option Strict On

Namespace Extract
    Public Class RTFText
        Implements IConversion, IDisposable

        Public Sub New()
        End Sub

        Public Overrides Function ToString() As String
            Return String.Empty
        End Function

        Public Overloads Function ToText(ByVal inputText As Object) As String Implements IConversion.ToText
            ' NULLS are always an empty string and are stopped here
            If inputText Is Nothing OrElse inputText Is System.DBNull.Value Then
                Return String.Empty
            End If

            ' If the source is not a string then just use the normal conversion
            If Not (TypeOf inputText Is String) Then
                Return Convert.ToString(inputText)
            End If

            ' Convert the input to a string
            Dim strText As String = TryCast(inputText, String)
            If strText Is Nothing Then
                Return String.Empty
            End If

            ' If the item is not RTF then just use the text as given
            If Not DebtPlus.Utils.Format.Strings.IsRTF(strText) Then
                Return strText
            End If

            ' Convert the text string from RTF text to suitable text
            Using ctl As New DevExpress.XtraRichEdit.RichEditControl()
                Try
                    ctl.RtfText = strText
                    Return ctl.Text
                Catch ex As ArgumentException
                End Try
            End Using

            ' Return the un-changed text if this is not valid RTF text
            Return strText
        End Function

#Region "IDisposable Support"
        Private disposedValue As Boolean ' To detect redundant calls
        Protected Overridable Sub Dispose(disposing As Boolean)
            If Not Me.disposedValue Then
                If disposing Then
                End If
            End If
            Me.disposedValue = True
        End Sub

        Public Sub Dispose() Implements IDisposable.Dispose
            Dispose(True)
            GC.SuppressFinalize(Me)
        End Sub
#End Region

    End Class
End Namespace
