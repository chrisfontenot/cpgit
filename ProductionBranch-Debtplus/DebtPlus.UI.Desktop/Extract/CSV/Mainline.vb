#Region "Copyright 2000-2012 DebtPlus, L.L.C."
'{*******************************************************************}
'{                                                                   }
'{       DebtPlus Debt Management System                             }
'{                                                                   }
'{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
'{       ALL RIGHTS RESERVED                                         }
'{                                                                   }
'{   The entire contents of this file is protected by U.S. and       }
'{   International Copyright Laws. Unauthorized reproduction,        }
'{   reverse-engineering, and distribution of all or any portion of  }
'{   the code contained in this file is strictly prohibited and may  }
'{   result in severe civil and criminal penalties and will be       }
'{   prosecuted to the maximum extent possible under the law.        }
'{                                                                   }
'{   RESTRICTIONS                                                    }
'{                                                                   }
'{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
'{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
'{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
'{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
'{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
'{                                                                   }
'{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
'{   ADDITIONAL RESTRICTIONS.                                        }
'{                                                                   }
'{*******************************************************************}

#End Region

Option Compare Binary
Option Explicit On
Option Strict On

Imports System.IO
Imports DebtPlus.Interfaces.Desktop
Imports System.Windows.Forms

Namespace Extract.CSV
    Public Class Mainline
        Implements IDesktopMainline

        ''' <summary>
        ''' Interface to the main application
        ''' </summary>
        Public Sub OldAppMain(ByVal args() As String) Implements IDesktopMainline.OldAppMain
            Dim cls As New MyThreadClass(args)
            Dim thrd As New System.Threading.Thread(AddressOf cls.ProcessThread)
            thrd.SetApartmentState(Threading.ApartmentState.STA)
            thrd.Name = "ExtractCSV"
            thrd.Start()
        End Sub

        ''' <summary>
        ''' Class to perform the extract process.
        ''' </summary>
        ''' <remarks>This class inherits the processing class and overrides some of the functions therein</remarks>
        Private Class MyThreadClass
            Inherits DebtPlus.Svc.Extract.CSV.ProcessingClass

            Dim args() As String
            Private WaitDlg As DevExpress.Utils.WaitDialogForm = Nothing

            Public Sub New(ByVal args() As String)
                Me.args = args
            End Sub

            ''' <summary>
            ''' Handle the feedback request to display the progress dialog as needed
            ''' </summary>
            ''' <param name="e"></param>
            ''' <remarks></remarks>
            Protected Overrides Sub OnFeedback(e As Svc.Extract.CSV.ProcessingClass.FeedbackArgs)
                MyBase.OnFeedback(e)

                ' If there is no waiting dialog, create one.
                If WaitDlg Is Nothing Then
                    WaitDlg = New DevExpress.Utils.WaitDialogForm("Performing extract operation")
                    WaitDlg.Show()
                End If

                ' Update the caption accordingly
                WaitDlg.SetCaption(e.FileName)
            End Sub

            ''' <summary>
            ''' Handle the request to retrieve the dates from the user. This will call up the dialog for the dates.
            ''' </summary>
            ''' <param name="e"></param>
            ''' <remarks></remarks>
            Protected Overrides Sub OnGetDates(e As Svc.Extract.CSV.ProcessingClass.GetDatesArgs)
                MyBase.OnGetDates(e)
                If e.IsValid Then
                    Using frm As New DebtPlus.Reports.Template.Forms.DateReportParametersForm()
                        If frm.ShowDialog() <> DialogResult.OK Then
                            e.IsValid = False
                        Else
                            e.FromDate = frm.Parameter_FromDate
                            e.ToDate = frm.Parameter_ToDate
                        End If
                    End Using
                End If
            End Sub

            ''' <summary>
            ''' Handle the request to resolve the filename as needed
            ''' </summary>
            ''' <param name="e"></param>
            ''' <remarks></remarks>
            Protected Overrides Sub OnGetFilename(e As Svc.Extract.CSV.ProcessingClass.GetFilenameArgs)
                MyBase.OnGetFilename(e)

                ' Look for a wild-card in the name. If there is one then ask for the filenaem.
                If e.IsValid AndAlso (e.FileName.Contains("?") OrElse e.FileName.Contains("*")) Then
                    Using dlg As New OpenFileDialog()
                        dlg.AddExtension = True
                        dlg.CheckFileExists = False
                        dlg.CheckPathExists = True
                        dlg.DefaultExt = "txt"
                        dlg.DereferenceLinks = True
                        dlg.FileName = e.FileName
                        dlg.Filter = "Text Files (*.txt)|*.txt|Comma Separated Files (*.csv)|*.csv|All Files (*.*)|*.*"
                        dlg.FilterIndex = 0
                        dlg.InitialDirectory = System.IO.Path.GetDirectoryName(e.FileName)
                        dlg.Multiselect = False
                        dlg.RestoreDirectory = True
                        dlg.ShowReadOnly = False
                        dlg.Title = e.Title
                        dlg.ValidateNames = True

                        If dlg.ShowDialog() <> DialogResult.OK Then
                            e.IsValid = False
                        Else
                            e.FileName = dlg.FileName
                        End If
                    End Using
                End If
            End Sub

            ''' <summary>
            ''' Handle the thread to perform the extract operation
            ''' </summary>
            ''' <remarks></remarks>
            Public Sub ProcessThread()
                Try
                    Dim ControlFile As String = If(args IsNot Nothing AndAlso args.Count > 0, args(0), String.Empty)

                    ' If there is a control file then go ahead and do the extract
                    If ControlFile <> String.Empty Then
                        PerformExtract(args(0))
                    Else
                        ' The control file is missing. This is bad news.
                        DebtPlus.Data.Forms.MessageBox.Show("Missing the name of the control file", "Parameter Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
                    End If

                Catch ex As Exception
                    DebtPlus.UI.Common.ErrorHandling.HandleErrors(ex)

                Finally
                    ' When the extract completes then remove the waiting dialog.
                    If WaitDlg IsNot Nothing Then
                        WaitDlg.Close()
                        WaitDlg.Dispose()
                        WaitDlg = Nothing
                    End If
                End Try
            End Sub
        End Class
    End Class
End Namespace