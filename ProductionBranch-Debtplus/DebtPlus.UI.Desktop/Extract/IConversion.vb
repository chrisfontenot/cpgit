﻿Option Compare Binary
Option Explicit On
Option Strict On

Namespace Extract
    Public Interface IConversion
        Inherits IDisposable
        Function ToText(ByVal InputString As Object) As String
    End Interface
End Namespace
