Option Compare Binary
Option Explicit On
Option Strict On

Namespace Extract.Fixed
    Public Class Sheet
        Private privateTitle As String = String.Empty

        Public Property Title() As String
            Get
                Return privateTitle
            End Get
            Set(ByVal value As String)
                privateTitle = value.Trim()
            End Set
        End Property

        Private privateFile As String = "*.xls"

        Public Property File() As String
            Get
                Return privateFile
            End Get
            Set(ByVal value As String)
                privateFile = value
            End Set
        End Property

        Private privateOptions As String = String.Empty

        Public Property Options() As String
            Get
                Return privateOptions
            End Get
            Set(ByVal value As String)
                privateOptions = value.ToLower().Replace(" ", "")
            End Set
        End Property

        Public Function hasOption(ByVal OptionString As String) As Boolean
            Return ("," + Options + ",").IndexOf("," + OptionString.ToLower() + ",") >= 0
        End Function

        Private privateLineEnd As String = "CR,LF"

        Public Property LineEnd() As String
            Get
                Return privateLineEnd
            End Get
            Set(ByVal value As String)
                privateLineEnd = value.ToUpper().Replace(" ", "")
            End Set
        End Property

        Private privatePrefix As String = String.Empty

        Public Property Prefix() As String
            Get
                Return privatePrefix
            End Get
            Set(ByVal value As String)
                privatePrefix = value
            End Set
        End Property

        Private privateSuffix As String = String.Empty

        Public Property Suffix() As String
            Get
                Return privateSuffix
            End Get
            Set(ByVal value As String)
                privateSuffix = value
            End Set
        End Property

        Private privateQueries As New List(Of Query)

        Public Property Queries() As List(Of Query)
            Get
                Return privateQueries
            End Get
            Set(ByVal value As List(Of Query))
                privateQueries = value
            End Set
        End Property
    End Class
End Namespace