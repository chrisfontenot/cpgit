Option Compare Binary
Option Explicit On
Option Strict On

Imports System.Xml.Serialization

Namespace Extract.Fixed
    Public Class Column
        Public Enum AlignEnum
            [Left] = 0
            [Right] = 1
        End Enum

        Private privateAlign As AlignEnum = AlignEnum.Left

        <XmlIgnore()>
        Public Property Align() As AlignEnum
            Get
                Return privateAlign
            End Get
            Set(ByVal value As AlignEnum)
                privateAlign = value
            End Set
        End Property

        Private privateOrdinal As Int32 = -1

        Public Property Ordinal() As Int32
            Get
                Return privateOrdinal
            End Get
            Set(ByVal value As Int32)
                privateOrdinal = value
            End Set
        End Property

        Public privateName As String = String.Empty

        Public Property Name() As String
            Get
                Return privateName
            End Get
            Set(ByVal value As String)
                privateName = value.Trim()
            End Set
        End Property

        Private privateSpacer As Boolean = False

        Public Property Spacer() As Boolean
            Get
                Return privateSpacer
            End Get
            Set(ByVal value As Boolean)
                privateSpacer = value
            End Set
        End Property

        Public privateFormat As String = String.Empty

        Public Property Format() As String
            Get
                Return privateFormat
            End Get
            Set(ByVal value As String)
                privateFormat = value
            End Set
        End Property

        Public privateFill As Char = " "c

        Public Property Fill() As Char
            Get
                Return privateFill
            End Get
            Set(ByVal value As Char)
                privateFill = value
            End Set
        End Property

        Private privateLength As Int32 = 0

        <XmlIgnore()>
        Public Property PaddLeft() As Int32
            Get
                Return privateLength
            End Get
            Set(ByVal value As Int32)
                privateLength = value
                Align = AlignEnum.Right
            End Set
        End Property

        <XmlIgnore()>
        Public Property PaddRight() As Int32
            Get
                Return privateLength
            End Get
            Set(ByVal value As Int32)
                privateLength = value
                Align = AlignEnum.Left
            End Set
        End Property

        Private Function Right(ByVal InputString As String, ByVal Size As Int32) As String
            If InputString.Length > Size Then InputString = InputString.Substring(InputString.Length - Size)
            Return InputString
        End Function

        Private Function Left(ByVal InputString As String, ByVal Size As Int32) As String
            If InputString.Length > Size Then InputString = InputString.Substring(0, Size)
            Return InputString
        End Function

        Public Function Encode(ByVal obj As Object) As String
            Dim answer As String = String.Empty

            If obj IsNot DBNull.Value Then
                If Format <> String.Empty Then
                    answer = String.Format(Format, obj)
                Else
                    answer = Convert.ToString(obj)
                End If
            End If

            Return AlignString(answer)
        End Function

        Public Function AlignString(ByVal InputString As String) As String

            ' If the field has a size then adjust the string to the desired length
            If privateLength > 0 Then
                If Align = AlignEnum.Right Then
                    InputString = Right(InputString.PadLeft(privateLength, Fill), privateLength)
                Else
                    InputString = Left(InputString.PadRight(privateLength, Fill), privateLength)
                End If
            End If

            Return InputString
        End Function
    End Class
End Namespace