#Region "Copyright 2000-2012 DebtPlus, L.L.C."
'{*******************************************************************}
'{                                                                   }
'{       DebtPlus Debt Management System                             }
'{                                                                   }
'{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
'{       ALL RIGHTS RESERVED                                         }
'{                                                                   }
'{   The entire contents of this file is protected by U.S. and       }
'{   International Copyright Laws. Unauthorized reproduction,        }
'{   reverse-engineering, and distribution of all or any portion of  }
'{   the code contained in this file is strictly prohibited and may  }
'{   result in severe civil and criminal penalties and will be       }
'{   prosecuted to the maximum extent possible under the law.        }
'{                                                                   }
'{   RESTRICTIONS                                                    }
'{                                                                   }
'{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
'{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
'{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
'{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
'{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
'{                                                                   }
'{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
'{   ADDITIONAL RESTRICTIONS.                                        }
'{                                                                   }
'{*******************************************************************}

#End Region

Option Compare Binary
Option Explicit On
Option Strict On

Imports System.Data.SqlClient
Imports System.IO
Imports System.Xml.Serialization
Imports System.Text.RegularExpressions
Imports System.Text
Imports DevExpress.Utils
Imports DebtPlus.Reports.Template.Forms
Imports DebtPlus.Interfaces.Desktop
Imports System.Windows.Forms

Namespace Extract.Fixed
    Public Class Mainline
        Implements IDesktopMainline

        ' global storage for the program
        Friend ap As New ExcelArgParser(DebtPlus.Configuration.Config.GetConfigValueForKey(DebtPlus.Configuration.Config.GetConfigSettings(DebtPlus.Configuration.Config.ConfigSet.ExtractFixed), "configFilePath"))
        'Private Sheets As New LinkedList(Of Sheet)

        Public Sub OldAppMain(ByVal args() As String) Implements IDesktopMainline.OldAppMain
            If ap.Parse(args) Then
                Dim inputfile As StreamReader = Nothing
                Try

                    ' Parse and load the parameter file contents
                    Dim serializer As New XmlSerializer(GetType(List(Of Sheet)))
                    inputfile = New StreamReader(ap.CommandFile)
                    Dim sheets As List(Of Sheet) = CType(serializer.Deserialize(inputfile), List(Of Sheet))
                    inputfile.Close()
                    inputfile = Nothing

                    ' Process each sheet entry to generate the output file
                    For Each sht As Sheet In sheets
                        If Not ProcessSheet(sht) Then Exit For
                    Next

                Catch ex As Exception
                    DebtPlus.Svc.Common.ErrorHandling.HandleErrors(ex)
                    DebtPlus.Data.Forms.MessageBox.Show(ex.ToString(), "Error reading configuration file", MessageBoxButtons.OK, MessageBoxIcon.Error)
                    Return

                Finally
                    If inputfile IsNot Nothing Then inputfile.Close()
                End Try
            End If
        End Sub

        Private Function GetFileName(ByVal sht As Sheet) As String
            Dim Fname As String = sht.File

            ' Try to find a match to the command line parameters. We will take any filename referenced in the list.
            Dim rx As New Regex("\[CommandLine\:\s*(?<argument>\d+)\]")
            Dim FoundMatch As Match = rx.Match(Fname)
            Dim Argument As Int32
            If FoundMatch IsNot Nothing AndAlso Int32.TryParse(FoundMatch.Groups(1).Value, Argument) Then
                If Argument <= ap.Files.Count Then
                    Fname = ap.Files(Argument - 1)
                End If
            End If

            ' Replace the string [My Documents] with the current documents directory
            If Fname.LastIndexOf("[My Documents]") >= 0 Then
                Dim DocumentsDirectory As String = Environment.GetFolderPath(Environment.SpecialFolder.Personal)
                Fname = Fname.Replace("[My Documents]", DocumentsDirectory)
            End If

            ' If the file is a wildcard item then ask the user for the filename
            If Fname.IndexOfAny(New Char() {"*"c, "?"c}) >= 0 Then
                Dim answer As DialogResult
                Using dlg As New OpenFileDialog()
                    With dlg
                        .AddExtension = True
                        .CheckFileExists = False
                        .CheckPathExists = True
                        .DefaultExt = "txt"
                        .DereferenceLinks = True
                        .FileName = Fname
                        .Filter = "Text Files (*.txt)|*.txt|All Files (*.*)|*.*"
                        .FilterIndex = 0
                        .InitialDirectory = Path.GetDirectoryName(Fname)
                        .Multiselect = False
                        .RestoreDirectory = True
                        .ShowReadOnly = False
                        .Title = sht.Title
                        .ValidateNames = True
                        answer = .ShowDialog()
                        Fname = .FileName
                    End With
                End Using

                ' If the user cancelled the dialog then die gracefully
                If answer = DialogResult.Cancel Then Fname = String.Empty
            End If

            Return Fname
        End Function

        Public Function ProcessSheet(ByVal sht As Sheet) As Boolean
            ' Decode the query information
            Dim rd As SqlDataReader = Nothing

            ' Create the output file
            Dim OutputName As String = GetFileName(sht)
            If OutputName = String.Empty Then
                Return False
            End If

            ' Open the file. It will create it if needed else simply overwrite it.
            Dim outputfile As New StreamWriter(OutputName, False, Encoding.ASCII, 4096)
            Try
                ' Change the line ending squence to match what is requested
                Select Case sht.LineEnd.ToLower()
                    Case "cr"
                        outputfile.NewLine = ControlChars.Cr
                    Case "lf"
                        outputfile.NewLine = ControlChars.Lf
                    Case "lf,cr"
                        outputfile.NewLine = ControlChars.Lf + ControlChars.Cr
                    Case ""
                        outputfile.NewLine = String.Empty
                    Case Else
                        outputfile.NewLine = Environment.NewLine
                End Select

                ' If there is a prefix line, write it
                If sht.Prefix <> String.Empty Then
                    outputfile.WriteLine(sht.Prefix)
                End If

                ' Issue the query to retrieve the results set
                Dim dlg As WaitDialogForm = Nothing

                For Each qry As Query In sht.Queries
                    Static FromDate As DateTime
                    Static ToDate As DateTime
                    Static RequestedDates As Boolean = False

                    ' Request the date range if so desired
                    If qry.NeedDateDialog Then
                        If Not RequestedDates Then
                            RequestedDates = True

                            Dim DateDlg As New DateReportParametersForm
                            Dim answer As DialogResult = dlg.ShowDialog()
                            FromDate = DateDlg.Parameter_FromDate
                            ToDate = DateDlg.Parameter_ToDate
                            DateDlg.Dispose()

                            ' If the answer is not "OK" then terminate the application now.
                            If answer <> DialogResult.OK Then
                                Return False
                            End If
                        End If

                        ' For the date values, find the corresponding items in the parameters
                        For Each parm As Parameter In qry.Parameters
                            Select Case parm.DialogTypeValue
                                Case Parameter.DialogType.FromDate
                                    parm.Value = FromDate.ToShortDateString
                                Case Parameter.DialogType.ToDate
                                    parm.Value = ToDate.ToShortDateString + " 23:59:59"
                            End Select
                        Next
                    End If

                    If dlg Is Nothing Then
                        dlg = New WaitDialogForm(qry.Command, "Processing command:")
                        dlg.Show()
                    End If

                    Dim cn As SqlConnection = New SqlConnection(DebtPlus.LINQ.SQLInfoClass.getDefault().ConnectionString)
                    Try
                        cn.Open()
                        Dim cmd As SqlCommand = qry.BuildCommand()
                        cmd.Connection = cn
                        rd = cmd.ExecuteReader(CommandBehavior.CloseConnection Or CommandBehavior.SingleResult)
                        If rd IsNot Nothing AndAlso rd.Read AndAlso rd.FieldCount > 0 Then

                            ' If we are to auto-fill the column names then do so now
                            If sht.hasOption("autofill") Then
                                qry.Columns.Clear()

                                ' Populate the list of columns from the input set
                                For FieldNo As Int32 = 0 To rd.FieldCount - 1
                                    Dim FieldName As String = rd.GetName(FieldNo)
                                    If String.IsNullOrEmpty(FieldName) Then FieldName = String.Format("Col{0:000}", FieldNo + 1)
                                    Dim col As New Column()
                                    With col
                                        .Name = FieldName
                                        .Ordinal = FieldNo
                                    End With
                                    qry.Columns.Add(col)
                                Next
                            Else
                                ' Find the ordinal positions for the fields by field name
                                For FieldNo As Int32 = 0 To qry.Columns.Count - 1
                                    With qry.Columns(FieldNo)
                                        .Ordinal = FindOrdinal(rd, .Name)
                                    End With
                                Next
                            End If

                            ' If we need to write the field names then do so here before the first record
                            If sht.hasOption("fieldnames") Then
                                Dim sb As New StringBuilder
                                For FieldNo As Int32 = 0 To qry.Columns.Count - 1
                                    With qry.Columns(FieldNo)
                                        If .Spacer Then
                                            sb.Append(.AlignString(String.Empty))
                                        Else
                                            If .Ordinal >= 0 Then
                                                sb.Append(.Encode(.Name))
                                            End If
                                        End If
                                    End With
                                Next
                                outputfile.WriteLine(sb.ToString())
                            End If

                            ' Process the records until there are no more
                            Do
                                Dim sb As New StringBuilder
                                For FieldNo As Int32 = 0 To qry.Columns.Count - 1
                                    With qry.Columns(FieldNo)
                                        If .Spacer Then
                                            sb.Append(.AlignString(String.Empty))
                                        Else
                                            If .Ordinal >= 0 Then
                                                sb.Append(.Encode(rd(.Ordinal)))
                                            End If
                                        End If
                                    End With
                                Next
                                outputfile.WriteLine(sb.ToString())
                            Loop Until Not rd.Read
                        End If

                    Catch ex As SqlException
                        DebtPlus.UI.Common.ErrorHandling.HandleErrors(ex, "Error reading information")

                    Finally
                        If cn IsNot Nothing Then cn.Dispose()

                        If dlg IsNot Nothing Then
                            dlg.Close()
                            dlg = Nothing
                        End If
                    End Try
                Next

                ' If there is a suffix line, write it
                If sht.Suffix <> String.Empty Then
                    outputfile.WriteLine(sht.Suffix)
                End If

            Finally
                ' Close the file
                outputfile.Close()
                outputfile.Dispose()
                outputfile = Nothing
            End Try

            Return True
        End Function

        Private Function FindOrdinal(ByRef rd As SqlDataReader, ByVal Name As String) As Int32
            Dim answer As Int32 = -1
            For FieldNo As Int32 = 0 To rd.FieldCount - 1
                If String.Compare(Name, rd.GetName(FieldNo), True) = 0 Then
                    answer = FieldNo
                    Exit For
                End If
            Next
            Return answer
        End Function
    End Class
End Namespace
