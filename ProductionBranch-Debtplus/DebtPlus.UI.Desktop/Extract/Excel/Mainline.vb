#Region "Copyright 2000-2012 DebtPlus, L.L.C."
'{*******************************************************************}
'{                                                                   }
'{       DebtPlus Debt Management System                             }
'{                                                                   }
'{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
'{       ALL RIGHTS RESERVED                                         }
'{                                                                   }
'{   The entire contents of this file is protected by U.S. and       }
'{   International Copyright Laws. Unauthorized reproduction,        }
'{   reverse-engineering, and distribution of all or any portion of  }
'{   the code contained in this file is strictly prohibited and may  }
'{   result in severe civil and criminal penalties and will be       }
'{   prosecuted to the maximum extent possible under the law.        }
'{                                                                   }
'{   RESTRICTIONS                                                    }
'{                                                                   }
'{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
'{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
'{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
'{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
'{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
'{                                                                   }
'{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
'{   ADDITIONAL RESTRICTIONS.                                        }
'{                                                                   }
'{*******************************************************************}

#End Region

Option Compare Binary
Option Explicit On
Option Strict On

Imports DevExpress.Utils
Imports System.Security
Imports System.Xml.Serialization
Imports System.IO
Imports System.Reflection
Imports DebtPlus.Reports.Template.Forms
Imports System.Data.SqlClient
Imports System.Text.RegularExpressions
Imports DebtPlus.Interfaces.Desktop
Imports System.Windows.Forms

Namespace Extract.Excel
    Public Class Mainline
        Implements IDesktopMainline

        ' global storage for the program
        Private Last_Filename As String = String.Empty
        Friend ap As New ExcelArgParser(DebtPlus.Configuration.Config.GetConfigValueForKey(DebtPlus.Configuration.Config.GetConfigSettings(DebtPlus.Configuration.Config.ConfigSet.ExtractExcel), "configFilePath"))
        Private Waitdlg As WaitDialogForm = Nothing
        Private Waitdlg_caption As String = String.Empty
        Private ds As DataSet = Nothing
        Private tbl As DataTable = Nothing

        ' For all other versions of Office without PIAs
        Private ThisApplication As Object = Nothing
        Private ThisWorkbook As Object = Nothing
        Private ThisSheet As Object = Nothing
        Private ThisRange As Object = Nothing

        Public Sub OldAppMain(ByVal args() As String) Implements IDesktopMainline.OldAppMain
            If Not ap.Parse(args) Then Return

            Try
                Dim serializer As New XmlSerializer(GetType(List(Of Sheet)))
                Dim inputfile As New StreamReader(ap.CommandFile)
                Dim sheets As List(Of Sheet) = CType(serializer.Deserialize(inputfile), List(Of Sheet))
                inputfile.Close()

                ' Set the pointer to the new application
                If OpenApplication() Then
                    For Each sht As Sheet In sheets
                        ProcessSheet(sht)
                    Next
                End If

            Catch ex As Exception
                DebtPlus.Svc.Common.ErrorHandling.HandleErrors(ex)

                ' Trap all other error conditions here
                DebtPlus.Data.Forms.MessageBox.Show(ex.ToString(), "Error processing sheet", MessageBoxButtons.OK, MessageBoxIcon.Hand)

            Finally
                If ThisApplication IsNot Nothing Then CloseApplication()
            End Try
        End Sub

        <SecuritySafeCritical()>
        Private Function OpenApplication() As Boolean

            ' From the registry, allocate a type for the excel object
            Dim objClassType As Type = Type.GetTypeFromProgID("Excel.Application")

            ' If we can not create the type from the registry then excel is not installed.
            If objClassType Is Nothing Then
                DebtPlus.Data.Forms.MessageBox.Show("The type EXCEL.APPLICATION could not be found in the registry." + Environment.NewLine + Environment.NewLine + "Did you forget to install Microsoft Excel? It is required for this application.", "Excel Could not be found", MessageBoxButtons.OK, MessageBoxIcon.Hand)
                Return False

            Else

                ' Given the type, create the application instance
                ThisApplication = Activator.CreateInstance(objClassType)

                ' Set the properties
                SetProperty(ThisApplication, "EnableAnimations", False)
                SetProperty(ThisApplication, "EnableAutoComplete", False)
                SetProperty(ThisApplication, "EnableEvents", False)
                SetProperty(ThisApplication, "EnableSound", False)
                SetProperty(ThisApplication, "DisplayAlerts", False)

                Return True
            End If
        End Function

        Private Sub CloseApplication()
            If ThisApplication IsNot Nothing Then

                ' Restore the settings back to the default values
                SetProperty(ThisApplication, "EnableAnimations", True)
                SetProperty(ThisApplication, "EnableAutoComplete", True)
                SetProperty(ThisApplication, "EnableEvents", True)
                SetProperty(ThisApplication, "EnableSound", True)
                SetProperty(ThisApplication, "DisplayAlerts", True)

                ' Restore control for the keyboard back to the excel sheet
                SetProperty(ThisApplication, "UserControl", True)
                ThisApplication = Nothing
            End If
        End Sub

        Private Function QuitApplication() As Boolean
            Try
                InvokeMethod(ThisApplication, "Quit")
                ThisApplication = Nothing
                Return True

            Catch ex As Exception
                DebtPlus.Svc.Common.ErrorHandling.HandleErrors(ex)
                DebtPlus.Data.Forms.MessageBox.Show(ex.ToString(), "Error quitting application", MessageBoxButtons.OK, MessageBoxIcon.Error)
                Return False
            End Try
        End Function

        Private Function SheetCount() As Int32
            Dim answer As Int32 = 0
            Try
                Dim Sheets As Object = GetProperty(ThisWorkbook, "Sheets")
                answer = Convert.ToInt32(GetProperty(Sheets, "Count"))

            Catch ex As Exception
                DebtPlus.Svc.Common.ErrorHandling.HandleErrors(ex)
                DebtPlus.Data.Forms.MessageBox.Show(ex.ToString(), "Error obtaining sheet count", MessageBoxButtons.OK, MessageBoxIcon.Error)
            End Try
            Return answer
        End Function

        Private Function CreateSheet() As Boolean
            Try
                Dim Sheets As Object = GetProperty(ThisWorkbook, "Sheets")
                InvokeMethod(Sheets, "Add")

            Catch ex As Exception
                DebtPlus.Svc.Common.ErrorHandling.HandleErrors(ex)
                DebtPlus.Data.Forms.MessageBox.Show(ex.ToString(), "Error adding new sheet", MessageBoxButtons.OK, MessageBoxIcon.Error)
                Return False
            End Try
            Return True
        End Function

        Private Function ActivateSheet() As Boolean
            Try
                InvokeMethod(ThisSheet, "Activate")

            Catch ex As Exception
                DebtPlus.Svc.Common.ErrorHandling.HandleErrors(ex)
                DebtPlus.Data.Forms.MessageBox.Show(ex.ToString(), "Error activating sheet", MessageBoxButtons.OK, MessageBoxIcon.Error)
                Return False
            End Try
            Return True
        End Function

        Private Function GetSheet(ByVal Name As Object) As Boolean

            Try
                Dim Sheets As Object = GetProperty(ThisWorkbook, "Sheets")
                ThisSheet = GetProperty(Sheets, "Item", Name)

            Catch ex As Exception
                DebtPlus.Svc.Common.ErrorHandling.HandleErrors(ex)
                DebtPlus.Data.Forms.MessageBox.Show(ex.ToString(), "Error finding sheet", MessageBoxButtons.OK, MessageBoxIcon.Error)
                Return False
            End Try

            Return True
        End Function

        Private Sub SetVisible(ByVal Value As Boolean)
            SetProperty(ThisApplication, "Visible", Value)
        End Sub

        Private Function GetRange(ByVal CellName As String) As Object
            Return GetProperty(ThisSheet, "Range", CellName)
        End Function

        Private Function SetValue(Rng As Object, ByVal Value As Object) As Boolean
            If IsNumeric(Value) Then
                SetProperty(Rng, "Value", Convert.ToDouble(Value))
            Else
                SetProperty(Rng, "Value", Value)
            End If
            Return True
        End Function

        Private Function OpenWorkbook(ByVal Fname As String) As Boolean
            Try
                Dim Workbooks As Object = GetProperty(ThisApplication, "Workbooks")
                ThisWorkbook = InvokeMethod(Workbooks, "Open", Fname)

            Catch ex As Exception
                DebtPlus.Svc.Common.ErrorHandling.HandleErrors(ex)
                DebtPlus.Data.Forms.MessageBox.Show(ex.ToString(), "Error opening input file", MessageBoxButtons.OK, MessageBoxIcon.Error)
                Return False
            End Try

            Return True
        End Function

        Private Function CreateWorkbook(Optional ByVal Fname As Object = Nothing) As Boolean
            Dim Workbooks As Object = GetProperty(ThisApplication, "Workbooks")

            If Fname IsNot Nothing Then
                ThisWorkbook = InvokeMethod(Workbooks, "Add", Fname)
            Else
                ThisWorkbook = InvokeMethod(Workbooks, "Add")
            End If

            Return True
        End Function

        Private Function SaveWorkbook(WkBk As Object, ByVal Fname As String) As Boolean

            Try
                InvokeMethod(ThisWorkbook, "SaveAs", Fname)

            Catch ex As Exception
                DebtPlus.Svc.Common.ErrorHandling.HandleErrors(ex)
                DebtPlus.Data.Forms.MessageBox.Show(ex.ToString(), "Error saving workbook", MessageBoxButtons.OK, MessageBoxIcon.Error)
                Return False
            End Try

            Return True
        End Function

#Region " Wrappers "

        <SecuritySafeCritical()>
        Private Sub SetProperty(ByVal obj As Object, ByVal sProperty As String, ByVal oValue As Object)
            Dim oParam(0) As Object
            oParam(0) = oValue
            obj.GetType().InvokeMember(sProperty, BindingFlags.SetProperty, Nothing, obj, oParam)
        End Sub

        <SecuritySafeCritical()>
        Private Function GetProperty(ByVal obj As Object, ByVal sProperty As String, ByVal oValue As Object) As Object
            Dim oParam(0) As Object
            oParam(0) = oValue
            Return obj.GetType().InvokeMember(sProperty, BindingFlags.GetProperty, Nothing, obj, oParam)
        End Function

        <SecuritySafeCritical()>
        Private Function GetProperty(ByVal obj As Object, ByVal sProperty As String, ByVal oValue1 As Object, ByVal oValue2 As Object) As Object
            Dim oParam(1) As Object
            oParam(0) = oValue1
            oParam(1) = oValue2
            Return obj.GetType().InvokeMember(sProperty, BindingFlags.GetProperty, Nothing, obj, oParam)
        End Function

        <SecuritySafeCritical()>
        Private Function GetProperty(ByVal obj As Object, ByVal sProperty As String) As Object
            Return obj.GetType().InvokeMember(sProperty, BindingFlags.GetProperty, Nothing, obj, Nothing)
        End Function

        <SecuritySafeCritical()>
        Private Function InvokeMethod(ByVal obj As Object, ByVal sProperty As String, ByVal oParam As Object()) As Object
            Return obj.GetType().InvokeMember(sProperty, BindingFlags.InvokeMethod, Nothing, obj, oParam)
        End Function

        <SecuritySafeCritical()>
        Private Function InvokeMethod(ByVal obj As Object, ByVal sProperty As String, ByVal oValue As Object) As Object
            Dim oParam(0) As Object
            oParam(0) = oValue
            Return obj.GetType().InvokeMember(sProperty, BindingFlags.InvokeMethod, Nothing, obj, oParam)
        End Function

        <SecuritySafeCritical()>
        Private Function InvokeMethod(ByVal obj As Object, ByVal sProperty As String) As Object
            Return obj.GetType().InvokeMember(sProperty, BindingFlags.InvokeMethod, Nothing, obj, Nothing)
        End Function

#End Region

        Private Sub ProcessSheet(ByVal sht As Sheet)

            ' Open or create the workbook file.
            If sht.hasOption("exist") Then
                If Not OpenWorkbook(sht.File, sht.Title) Then Return
            Else
                If Not CreateWorkbook() Then Return
            End If

            ' Display the sheets if desired
            If sht.hasOption("show") Then SetVisible(True)

            ' Allocate a sheet for the data. Use either the name or the first sheet in the list
            If sht.Name <> String.Empty Then
                Dim SheetNumber As Int32 = -1
                If Int32.TryParse(sht.Name, SheetNumber) AndAlso SheetNumber >= 1 Then
                    If Not GetSheet(SheetNumber) Then Return
                Else
                    If Not GetSheet(sht.Name) Then Return
                End If
            Else
                If SheetCount() < 1 Then CreateSheet()
                If Not GetSheet(1) Then Return
            End If

            For Each qry As Query In sht.Queries
                ProcessQuery(sht, qry)
            Next

            ' Save and Quit the application if so desired
            If sht.hasOption("quit") Then
                SaveWorkbook(sht.File, sht.Title)
                QuitApplication()
            Else
                ' Save the sheet if so desired
                If sht.hasOption("save") Then SaveWorkbook(sht.File, sht.Title)
            End If
        End Sub

        Private FromDate As DateTime
        Private ToDate As DateTime
        Private RequestedDates As Boolean = False

        Private Sub ProcessQuery(ByVal Sht As Sheet, ByVal qry As Query)

            ' Request the date range if so desired
            If qry.NeedDateDialog Then
                If Not RequestedDates Then
                    RequestedDates = True

                    Using dlg As New DateReportParametersForm
                        If dlg.ShowDialog() <> DialogResult.OK Then
                            Application.ExitThread()
                            Return
                        End If

                        FromDate = dlg.Parameter_FromDate
                        ToDate = dlg.Parameter_ToDate
                    End Using
                End If

                ' For the date values, find the corresponding items in the parameters
                For Each parm As Parameter In qry.Parameters
                    Select Case parm.DialogTypeValue
                        Case Parameter.DialogType.FromDate
                            parm.Value = FromDate.ToShortDateString
                        Case Parameter.DialogType.ToDate
                            parm.Value = ToDate.ToShortDateString + " 23:59:59"
                    End Select
                Next
            End If

            Try
                ' Issue the query
                Using cm As New DebtPlus.UI.Common.CursorManager()
                    Using cmd As SqlCommand = qry.BuildCommand()
                        cmd.Connection = New SqlConnection(DebtPlus.LINQ.SQLInfoClass.getDefault().ConnectionString)

                        ' Display the wait dialog
                        Dim Waitdlg_caption As String = cmd.CommandText
                        If Waitdlg_caption.Length > 25 Then Waitdlg_caption = Waitdlg_caption.Substring(0, 25)
                        Waitdlg = New WaitDialogForm(Waitdlg_caption, "Processing Command")
                        Waitdlg.Show()

                        ds = New DataSet("ds")
                        Using da As New SqlDataAdapter(cmd)
                            da.Fill(ds, "table1")

                            tbl = Nothing
                            If ds.Tables.Count > 0 Then tbl = ds.Tables(0)
                        End Using
                    End Using
                End Using

                If tbl IsNot Nothing Then

                    ' Determine the type of the processing based upon the sheet directive
                    If Sht.Type = "extract" Then
                        Process_Extract(Sht, qry)
                    ElseIf Sht.Type = "values" Then
                        Process_Values(qry)
                    End If

                End If

            Catch ex As SqlException
                DebtPlus.Svc.Common.ErrorHandling.HandleErrors(ex)
                DebtPlus.Data.Forms.MessageBox.Show(ex.ToString(), "Error reading values from database", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)

            Finally

                ' Dispose of the dialog when we are completed
                If Waitdlg IsNot Nothing Then
                    Waitdlg.Close()
                    Waitdlg.Dispose()
                End If
            End Try
        End Sub

        Private Sub Process_Extract(ByVal sht As Sheet, ByVal qry As Query)
            Dim col As Int32
            Dim FirstLine As Int32 = sht.FirstLine - 1

            ' Write the first line with the column names
            If sht.hasOption("headers") Then
                col = sht.FirstColumn
                FirstLine += 1
                For Each DataCol As DataColumn In ds.Tables(0).Columns
                    If DataCol.DataType IsNot GetType(Byte()) Then ' ignore time-stamp columns
                        SetValue(GetRange(Cell(FirstLine, col)), DataCol.ColumnName)
                        col += 1
                    End If
                Next
            End If

            ' Load the data for the item
            For Each row As DataRow In ds.Tables(0).Rows
                col = sht.FirstColumn - 1
                FirstLine += 1
                For Each DataCol As DataColumn In ds.Tables(0).Columns
                    Dim colName As String = DataCol.ColumnName

                    ' Just ignore and remove any byte() objects from the list
                    If DataCol.DataType Is GetType(Byte()) Then
                        Continue For
                    End If

                    ' Advance to the next column
                    col += 1

                    ' Find the value for the item. If null, ignore it now.
                    Dim value As Object = row(DataCol.Ordinal)
                    If value Is Nothing OrElse value Is System.DBNull.Value Then
                        Continue For
                    End If

                    ' Find the column in the list of columns for this query sheet
                    If qry.Columns IsNot Nothing AndAlso qry.Columns.Count > 0 Then
                        Dim colData As Extract.Excel.Column = (From selCol In qry.Columns Where String.Compare(selCol.Name, colName, True) = 0 Select selCol).FirstOrDefault()
                        If colData IsNot Nothing Then
                            value = colData.Translate(value)
                        End If

                        If value.GetType() Is GetType(System.Guid) Then
                            value = value.ToString()
                        End If

                        SetValue(GetRange(Cell(FirstLine, col)), value)
                    End If
                Next
            Next row
        End Sub

        Private Sub Process_Values(ByVal qry As Query)

            ' Process the non-extract value to simply update the cell contents
            Dim last_sheet_name As String = String.Empty

            For Each row As DataRow In ds.Tables(0).Rows
                Dim sheet_name As String = String.Empty
                Dim cell_name As String = String.Empty
                Dim cell_value As Object = Nothing

                For Each DataCol As DataColumn In ds.Tables(0).Columns
                    If DataCol.DataType IsNot GetType(Byte()) Then
                        Select Case DataCol.ColumnName.ToLower()
                            Case "sheet", "page"
                                sheet_name = Convert.ToString(row(DataCol.Ordinal))
                            Case "location", "cell"
                                cell_name = Convert.ToString(row(DataCol.Ordinal))
                            Case "value"
                                cell_value = row(DataCol.Ordinal)
                        End Select
                    End If
                Next

                ' Ignore null values
                If cell_value Is Nothing OrElse cell_value Is System.DBNull.Value Then
                    Continue For
                End If

                ' Find the sheet for the new value
                If sheet_name = String.Empty OrElse cell_name = String.Empty Then
                    Continue For
                End If

                ' Find any mapping that is needed
                Dim colItem As Extract.Excel.Column = (From colSearch In qry.Columns Where String.Compare(colSearch.Name, cell_name, True) = 0 Select colSearch).FirstOrDefault()
                If colItem IsNot Nothing Then
                    cell_value = colItem.Translate(cell_value)
                End If

                If String.Compare(sheet_name, last_sheet_name, True) <> 0 OrElse ThisSheet Is Nothing Then
                    Dim IntegerSheetNumber As Int32
                    If Int32.TryParse(sheet_name, IntegerSheetNumber) Then
                        If Not GetSheet(IntegerSheetNumber) Then Return
                    Else
                        If Not GetSheet(sheet_name) Then Return
                    End If
                    last_sheet_name = sheet_name
                End If

                ' Update the value on the sheet
                ThisRange = GetRange(cell_name)
                SetValue(ThisRange, cell_value)

                ' Update the dialog with the cell range
                Waitdlg.SetCaption(Waitdlg_caption + " (" + cell_name + ")")
            Next
        End Sub

        Private Function OpenWorkbook(ByVal Filename As String, ByVal Title As String) As Boolean
            Dim Current_Filename As String = Filename

            ' If the filename matches the previous setting then just return the last application
            If ThisWorkbook IsNot Nothing Then
                If Last_Filename <> String.Empty Then
                    If Last_Filename = Filename Then Return True
                End If
            End If

            ' Try to find a match to the command line parameters. We will take any filename referenced in the list.
            Dim rx As New Regex("\[CommandLine\:\s*(?<argument>\d+)\]", RegexOptions.IgnoreCase Or RegexOptions.Singleline)
            Dim FoundMatch As Match = rx.Match(Current_Filename)
            Dim Argument As Int32
            If FoundMatch IsNot Nothing AndAlso Int32.TryParse(FoundMatch.Groups(1).Value, Argument) Then
                If Argument >= 1 AndAlso Argument <= ap.Files.Count Then
                    Current_Filename = ap.Files(Argument - 1)
                End If
            End If

            ' Replace the string [My Documents] with the current documents directory
            If Current_Filename.LastIndexOf("[My Documents]") >= 0 Then
                Dim DocumentsDirectory As String = Environment.GetFolderPath(Environment.SpecialFolder.Personal)
                Current_Filename = Current_Filename.Replace("[My Documents]", DocumentsDirectory)
            End If

            ' If the file is a wild-card item then ask the user for the filename
            If Current_Filename.IndexOfAny(New Char() {"*"c, "?"c}) >= 0 Then
                Dim answer As DialogResult

                Using dlg As New OpenFileDialog()
                    dlg.AddExtension = True
                    dlg.CheckFileExists = True
                    dlg.CheckPathExists = True
                    dlg.DefaultExt = "xls"
                    dlg.DereferenceLinks = True
                    dlg.FileName = Path.GetFileName(Current_Filename)
                    dlg.Filter = "2010 Excel Documents (*.xlsx)|*.xlsx|2003/XP Excel Documents (*.xls)|*.xls|All Files (*.*)|*.*"
                    dlg.FilterIndex = 1
                    dlg.InitialDirectory = Path.GetDirectoryName(Current_Filename)
                    dlg.Multiselect = False
                    dlg.RestoreDirectory = True
                    dlg.ShowReadOnly = False
                    dlg.Title = Title
                    dlg.ValidateNames = True
                    answer = dlg.ShowDialog()
                    Current_Filename = dlg.FileName
                End Using

                ' If the user canceled the dialog then die gracefully
                If answer = DialogResult.Cancel Then Return False
            End If

            ' Load the excel sheet
            If Not OpenWorkbook(Current_Filename) Then Return False
            Last_Filename = Filename
            Return True
        End Function

        Private Function SaveWorkbook(ByVal Filename As String, Optional ByVal Title As String = "Save Excel Document") As Boolean
            Dim Current_Filename As String = Filename

            ' Try to find a match to the command line parameters. We will take any filename referenced in the list.
            Dim rx As New Regex("\[CommandLine\:\s*(?<argument>\d+)\]", RegexOptions.IgnoreCase Or RegexOptions.Singleline)
            Dim FoundMatch As Match = rx.Match(Current_Filename)
            Dim Argument As Int32
            If FoundMatch IsNot Nothing AndAlso Int32.TryParse(FoundMatch.Groups(1).Value, Argument) Then
                If Argument >= 1 AndAlso Argument <= ap.Files.Count Then
                    Current_Filename = ap.Files(Argument - 1)
                End If
            End If

            ' Replace the string [My Documents] with the current documents directory
            If Current_Filename.LastIndexOf("[My Documents]") >= 0 Then
                Dim DocumentsDirectory As String = Environment.GetFolderPath(Environment.SpecialFolder.Personal)
                Current_Filename = Current_Filename.Replace("[My Documents]", DocumentsDirectory)
            End If

            ' If the file is a wild-card item then ask the user for the filename
            If Current_Filename.IndexOfAny(New Char() {"*"c, "?"c}) >= 0 Then
                Dim answer As DialogResult
                Using dlg As New SaveFileDialog()
                    dlg.AddExtension = True
                    dlg.CheckFileExists = False
                    dlg.CheckPathExists = True
                    dlg.DefaultExt = "xlsx"
                    dlg.DereferenceLinks = True
                    dlg.FileName = String.Empty
                    dlg.Filter = "Excel 2010 Documents (*.xlsx)|*.xlsx|Excel 2007 Documents (*.xls)|*.xls|All Files (*.*)|*.*"
                    dlg.FilterIndex = 0
                    dlg.InitialDirectory = Path.GetDirectoryName(Current_Filename)
                    dlg.RestoreDirectory = True
                    dlg.Title = Title
                    dlg.ValidateNames = True

                    answer = dlg.ShowDialog()
                    Current_Filename = dlg.FileName
                End Using

                ' If the user canceled the dialog then die gracefully
                If answer = DialogResult.Cancel Then Return False
            End If

            Return SaveWorkbook(ThisWorkbook, Current_Filename)
        End Function

        Private Function Cell(ByVal Row As Int32, ByVal Col As Int32) As String
            Return ColString(Col - 1) + RowString(Row - 1)
        End Function

        Private Function RowString(ByVal row As Int32) As String
            If row < 0 Then
                Throw New ArgumentOutOfRangeException("row can not be negative")
            End If

            Return (row + 1).ToString()
        End Function

        Private Function ColString(ByVal col As Int32) As String
            If col < 0 Then
                Throw New ArgumentOutOfRangeException("col can not be negative")
            End If

            If col < 26 Then
                Return "ABCDEFGHIJKLMNOPQRSTUVWXYZ".Substring(col, 1)
            End If

            Return ColString((col \ 26) - 1) + ColString(col Mod 26)
        End Function
    End Class
End Namespace
