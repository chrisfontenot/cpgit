﻿Namespace Extract.Excel
    Public Class Column

        Public Property Name As String
        Public Property Type As String

        Public Function Translate(ByVal Input As Object) As String

            ' Get a pointer to the translation class
            Dim asm As System.Reflection.Assembly = System.Reflection.Assembly.GetExecutingAssembly()

            ' From the type, create an instance of the class
            Dim typ As System.Type = asm.GetType(Type)
            If typ IsNot Nothing Then
                Using instance As Extract.IConversion = System.Activator.CreateInstance(typ)
                    If instance IsNot Nothing Then
                        Return instance.ToText(Input)
                    End If
                End Using
            End If

            Return Input
        End Function
    End Class
End Namespace
