Option Compare Binary
Option Explicit On
Option Strict On

Namespace Extract.Excel
    Public Class Sheet
        Private privateName As String = String.Empty

        Public Property Name() As String
            Get
                Return privateName
            End Get
            Set(ByVal value As String)
                privateName = value.Trim()
            End Set
        End Property

        Private privateType As String = "extract"

        Public Property Type() As String
            Get
                Return privateType
            End Get
            Set(ByVal value As String)
                privateType = value.ToLower().Trim()
            End Set
        End Property

        Private privateTitle As String = String.Empty

        Public Property Title() As String
            Get
                Return privateTitle
            End Get
            Set(ByVal value As String)
                privateTitle = value.Trim()
            End Set
        End Property

        Private privateFile As String = "*.xls"

        Public Property File() As String
            Get
                Return privateFile
            End Get
            Set(ByVal value As String)
                privateFile = value
            End Set
        End Property

        Private privateOptions As String = String.Empty

        Public Property Options() As String
            Get
                Return privateOptions
            End Get
            Set(ByVal value As String)
                privateOptions = value.ToLower().Replace(" ", "")
            End Set
        End Property

        Public Function hasOption(ByVal OptionString As String) As Boolean
            Return ("," + Options + ",").IndexOf("," + OptionString.ToLower() + ",") >= 0
        End Function

        Private privateFirstLine As Int32 = 1

        Public Property FirstLine() As Int32
            Get
                Return privateFirstLine
            End Get
            Set(ByVal value As Int32)
                privateFirstLine = value
            End Set
        End Property

        Private privateFirstColumn As Int32 = 1

        Public Property FirstColumn() As Int32
            Get
                Return privateFirstColumn
            End Get
            Set(ByVal value As Int32)
                privateFirstColumn = value
            End Set
        End Property

        Private privateQueries As New List(Of Query)

        Public Property Queries() As List(Of Query)
            Get
                Return privateQueries
            End Get
            Set(ByVal value As List(Of Query))
                privateQueries = value
            End Set
        End Property
    End Class
End Namespace