Option Compare Binary
Option Explicit On
Option Strict On

Namespace Extract.ARM.v40
    Public Class ArgParser
        Inherits ArgParserBase

        Public Sub New()
            MyBase.New(New String() {})
        End Sub

        Public Sub New(ByVal switchSymbols As String())
            MyBase.New(switchSymbols)
        End Sub

        Public Sub New(ByVal switchSymbols As String(), ByVal caseSensitiveSwitches As Boolean)
            MyBase.New(switchSymbols, caseSensitiveSwitches)
        End Sub

        Public Sub New(ByVal switchSymbols As String(), ByVal caseSensitiveSwitches As Boolean, ByVal switchChars As String())
            MyBase.New(switchSymbols, caseSensitiveSwitches, switchChars)
        End Sub

        ''' <summary>
        '''     Generate the command usage information
        ''' </summary>
        Protected Overrides Sub OnUsage(ByVal errorInfo As String)
            ' deprecated
        End Sub
    End Class
End Namespace
