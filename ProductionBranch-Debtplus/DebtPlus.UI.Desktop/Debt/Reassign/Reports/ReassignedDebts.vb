#Region "Copyright 2000-2012 DebtPlus, L.L.C."
'{*******************************************************************}
'{                                                                   }
'{       DebtPlus Debt Management System                             }
'{                                                                   }
'{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
'{       ALL RIGHTS RESERVED                                         }
'{                                                                   }
'{   The entire contents of this file is protected by U.S. and       }
'{   International Copyright Laws. Unauthorized reproduction,        }
'{   reverse-engineering, and distribution of all or any portion of  }
'{   the code contained in this file is strictly prohibited and may  }
'{   result in severe civil and criminal penalties and will be       }
'{   prosecuted to the maximum extent possible under the law.        }
'{                                                                   }
'{   RESTRICTIONS                                                    }
'{                                                                   }
'{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
'{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
'{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
'{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
'{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
'{                                                                   }
'{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
'{   ADDITIONAL RESTRICTIONS.                                        }
'{                                                                   }
'{*******************************************************************}
#End Region

Option Compare Binary
Option Explicit On 
Option Strict On

Imports DebtPlus.Reports.Template
Imports DevExpress.XtraReports.UI
Imports System.Data.SqlClient
Imports System.Windows.Forms

Namespace Debt.Reassign.Reports

    Friend Class ReassignedDebtsReport
        Inherits TemplateXtraReportClass

        Private ap As XmlArgParser = Nothing
        Public Sub New(ByVal ap As XmlArgParser)
            MyClass.New()
            Me.ap = ap
            AddHandler XrLabel_old_creditor.BeforePrint, AddressOf XrLabel_old_creditor_BeforePrint
            AddHandler new_creditor.BeforePrint, AddressOf new_creditor_BeforePrint
            AddHandler client_id_and_name.BeforePrint, AddressOf client_id_and_name_BeforePrint
        End Sub

        ''' <summary>
        ''' Title associated with the report
        ''' </summary>
        Public Overrides ReadOnly Property ReportTitle() As String
            Get
                Return "Reassigned Debt List"
            End Get
        End Property

        ''' <summary>
        ''' Create the report object
        ''' </summary>
        Public Sub New()
            MyBase.New()
            InitializeComponent()
        End Sub

#Region " Component Designer generated code "

        'Component overrides dispose to clean up the component list.
        Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
            If disposing Then
                If components IsNot Nothing Then
                    components.Dispose()
                End If
            End If
            MyBase.Dispose(disposing)
        End Sub

        'Required by the Designer
        Private components As System.ComponentModel.IContainer

        'NOTE: The following procedure is required by the Designer
        'It can be modified using the Designer.
        'Do not modify it using the code editor.
        Friend WithEvents GroupHeader1 As GroupHeaderBand
        Friend WithEvents GroupFooter1 As GroupFooterBand
        Friend WithEvents XrPanel1 As XRPanel
        Friend WithEvents XrLabel_old_creditor As XRLabel
        Friend WithEvents XrLabel2 As XRLabel
        Friend WithEvents XrLabel1 As XRLabel
        Friend WithEvents XrLabel3 As XRLabel
        Friend WithEvents new_creditor As XRLabel
        Friend WithEvents client_id_and_name As XRLabel
        Friend WithEvents account_number As XRLabel
        Friend WithEvents XrLabel5 As XRLabel
        Friend WithEvents XrLabel_Balance As XRLabel
        Friend WithEvents XrLabel_debt_count As XRLabel
        <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
            Dim XrSummary1 As DevExpress.XtraReports.UI.XRSummary = New DevExpress.XtraReports.UI.XRSummary()
            Me.GroupHeader1 = New DevExpress.XtraReports.UI.GroupHeaderBand()
            Me.XrLabel_old_creditor = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrPanel1 = New DevExpress.XtraReports.UI.XRPanel()
            Me.XrLabel5 = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel3 = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel1 = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel2 = New DevExpress.XtraReports.UI.XRLabel()
            Me.GroupFooter1 = New DevExpress.XtraReports.UI.GroupFooterBand()
            Me.XrLabel_debt_count = New DevExpress.XtraReports.UI.XRLabel()
            Me.new_creditor = New DevExpress.XtraReports.UI.XRLabel()
            Me.client_id_and_name = New DevExpress.XtraReports.UI.XRLabel()
            Me.account_number = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel_Balance = New DevExpress.XtraReports.UI.XRLabel()
            CType(Me, System.ComponentModel.ISupportInitialize).BeginInit()
            '
            'Detail
            '
            Me.Detail.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrLabel_Balance, Me.account_number, Me.client_id_and_name, Me.new_creditor})
            Me.Detail.HeightF = 16.0!
            '
            'PageHeader
            '
            Me.PageHeader.HeightF = 122.0!
            '
            'XrLabel_Title
            '
            Me.XrLabel_Title.StylePriority.UseFont = False
            '
            'XrPageInfo_PageNumber
            '
            Me.XrPageInfo_PageNumber.StylePriority.UseTextAlignment = False
            '
            'XRLabel_Agency_Name
            '
            Me.XRLabel_Agency_Name.StylePriority.UseTextAlignment = False
            '
            'XrLabel_Agency_Address3
            '
            Me.XrLabel_Agency_Address3.StylePriority.UseFont = False
            Me.XrLabel_Agency_Address3.StylePriority.UseTextAlignment = False
            '
            'XrLabel_Agency_Address1
            '
            Me.XrLabel_Agency_Address1.StylePriority.UseFont = False
            Me.XrLabel_Agency_Address1.StylePriority.UseTextAlignment = False
            '
            'XrLabel_Agency_Phone
            '
            Me.XrLabel_Agency_Phone.StylePriority.UseFont = False
            Me.XrLabel_Agency_Phone.StylePriority.UseTextAlignment = False
            '
            'XrLabel_Agency_Address2
            '
            Me.XrLabel_Agency_Address2.StylePriority.UseFont = False
            Me.XrLabel_Agency_Address2.StylePriority.UseTextAlignment = False
            '
            'GroupHeader1
            '
            Me.GroupHeader1.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrLabel_old_creditor, Me.XrPanel1})
            Me.GroupHeader1.GroupUnion = DevExpress.XtraReports.UI.GroupUnion.WholePage
            Me.GroupHeader1.HeightF = 56.0!
            Me.GroupHeader1.Name = "GroupHeader1"
            Me.GroupHeader1.RepeatEveryPage = True
            '
            'XrLabel_old_creditor
            '
            Me.XrLabel_old_creditor.CanGrow = False
            Me.XrLabel_old_creditor.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Bold)
            Me.XrLabel_old_creditor.ForeColor = System.Drawing.Color.Maroon
            Me.XrLabel_old_creditor.LocationFloat = New DevExpress.Utils.PointFloat(0.0!, 8.0!)
            Me.XrLabel_old_creditor.Name = "XrLabel_old_creditor"
            Me.XrLabel_old_creditor.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel_old_creditor.SizeF = New System.Drawing.SizeF(800.0!, 17.0!)
            '
            'XrPanel1
            '
            Me.XrPanel1.BackColor = System.Drawing.Color.Teal
            Me.XrPanel1.BorderColor = System.Drawing.Color.Teal
            Me.XrPanel1.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrLabel5, Me.XrLabel3, Me.XrLabel1, Me.XrLabel2})
            Me.XrPanel1.LocationFloat = New DevExpress.Utils.PointFloat(0.0!, 33.0!)
            Me.XrPanel1.Name = "XrPanel1"
            Me.XrPanel1.SizeF = New System.Drawing.SizeF(800.0!, 17.0!)
            '
            'XrLabel5
            '
            Me.XrLabel5.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Bold)
            Me.XrLabel5.ForeColor = System.Drawing.Color.White
            Me.XrLabel5.LocationFloat = New DevExpress.Utils.PointFloat(692.0!, 0.0!)
            Me.XrLabel5.Name = "XrLabel5"
            Me.XrLabel5.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel5.SizeF = New System.Drawing.SizeF(100.0!, 16.0!)
            Me.XrLabel5.StylePriority.UseTextAlignment = False
            Me.XrLabel5.Text = "BALANCE"
            Me.XrLabel5.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            '
            'XrLabel3
            '
            Me.XrLabel3.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Bold)
            Me.XrLabel3.ForeColor = System.Drawing.Color.White
            Me.XrLabel3.LocationFloat = New DevExpress.Utils.PointFloat(508.0!, 0.0!)
            Me.XrLabel3.Name = "XrLabel3"
            Me.XrLabel3.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel3.SizeF = New System.Drawing.SizeF(117.0!, 16.0!)
            Me.XrLabel3.Text = "NEW CREDITOR"
            '
            'XrLabel1
            '
            Me.XrLabel1.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Bold)
            Me.XrLabel1.ForeColor = System.Drawing.Color.White
            Me.XrLabel1.LocationFloat = New DevExpress.Utils.PointFloat(0.0!, 0.0!)
            Me.XrLabel1.Name = "XrLabel1"
            Me.XrLabel1.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel1.SizeF = New System.Drawing.SizeF(159.0!, 16.0!)
            Me.XrLabel1.Text = "Client ID AND NAME"
            '
            'XrLabel2
            '
            Me.XrLabel2.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Bold)
            Me.XrLabel2.ForeColor = System.Drawing.Color.White
            Me.XrLabel2.LocationFloat = New DevExpress.Utils.PointFloat(267.0!, 0.0!)
            Me.XrLabel2.Name = "XrLabel2"
            Me.XrLabel2.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel2.SizeF = New System.Drawing.SizeF(141.0!, 16.0!)
            Me.XrLabel2.Text = "ACCOUNT NUMBER"
            '
            'GroupFooter1
            '
            Me.GroupFooter1.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrLabel_debt_count})
            Me.GroupFooter1.HeightF = 52.0!
            Me.GroupFooter1.Name = "GroupFooter1"
            '
            'XrLabel_debt_count
            '
            Me.XrLabel_debt_count.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Bold)
            Me.XrLabel_debt_count.ForeColor = System.Drawing.Color.Maroon
            Me.XrLabel_debt_count.LocationFloat = New DevExpress.Utils.PointFloat(0.0!, 17.0!)
            Me.XrLabel_debt_count.Name = "XrLabel_debt_count"
            Me.XrLabel_debt_count.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel_debt_count.SizeF = New System.Drawing.SizeF(550.0!, 16.0!)
            XrSummary1.FormatString = "{0:n0} Account(s) are to be reassigned"
            XrSummary1.Func = DevExpress.XtraReports.UI.SummaryFunc.Count
            XrSummary1.Running = DevExpress.XtraReports.UI.SummaryRunning.Report
            Me.XrLabel_debt_count.Summary = XrSummary1
            Me.XrLabel_debt_count.Text = "Count of debts to be reassigned"
            '
            'new_creditor
            '
            Me.new_creditor.CanGrow = False
            Me.new_creditor.ForeColor = System.Drawing.Color.Black
            Me.new_creditor.LocationFloat = New DevExpress.Utils.PointFloat(508.0!, 0.0!)
            Me.new_creditor.Name = "new_creditor"
            Me.new_creditor.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.new_creditor.SizeF = New System.Drawing.SizeF(175.0!, 16.0!)
            '
            'client_id_and_name
            '
            Me.client_id_and_name.CanGrow = False
            Me.client_id_and_name.ForeColor = System.Drawing.Color.Black
            Me.client_id_and_name.LocationFloat = New DevExpress.Utils.PointFloat(0.0!, 0.0!)
            Me.client_id_and_name.Name = "client_id_and_name"
            Me.client_id_and_name.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.client_id_and_name.SizeF = New System.Drawing.SizeF(242.0!, 16.0!)
            '
            'account_number
            '
            Me.account_number.CanGrow = False
            Me.account_number.ForeColor = System.Drawing.Color.Black
            Me.account_number.LocationFloat = New DevExpress.Utils.PointFloat(267.0!, 0.0!)
            Me.account_number.Name = "account_number"
            Me.account_number.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.account_number.SizeF = New System.Drawing.SizeF(233.0!, 16.0!)
            '
            'XrLabel_Balance
            '
            Me.XrLabel_Balance.CanGrow = False
            Me.XrLabel_Balance.ForeColor = System.Drawing.Color.Black
            Me.XrLabel_Balance.LocationFloat = New DevExpress.Utils.PointFloat(684.0!, 0.0!)
            Me.XrLabel_Balance.Name = "XrLabel_Balance"
            Me.XrLabel_Balance.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel_Balance.SizeF = New System.Drawing.SizeF(108.0!, 16.0!)
            Me.XrLabel_Balance.StylePriority.UseTextAlignment = False
            Me.XrLabel_Balance.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            '
            Me.account_number.DataBindings.Add("Text", Nothing, "account_number")
            Me.XrLabel_Balance.DataBindings.Add("Text", Nothing, "balance", "{0:c}")
            Me.XrLabel_debt_count.DataBindings.Add("Text", Nothing, "client")
            '
            'ReassignedDebtsReport
            '
            Me.Bands.AddRange(New DevExpress.XtraReports.UI.Band() {Me.Detail, Me.PageHeader, Me.PageFooter, Me.TopMarginBand1, Me.BottomMarginBand1, Me.GroupHeader1, Me.GroupFooter1})
            Me.StyleSheet.AddRange(New DevExpress.XtraReports.UI.XRControlStyle() {Me.XrControlStyle_Header, Me.XrControlStyle_Totals, Me.XrControlStyle_HeaderPannel, Me.XrControlStyle_GroupHeader})
            Me.Version = "11.2"
            Me.Controls.SetChildIndex(Me.GroupFooter1, 0)
            Me.Controls.SetChildIndex(Me.GroupHeader1, 0)
            Me.Controls.SetChildIndex(Me.BottomMarginBand1, 0)
            Me.Controls.SetChildIndex(Me.TopMarginBand1, 0)
            Me.Controls.SetChildIndex(Me.PageFooter, 0)
            Me.Controls.SetChildIndex(Me.PageHeader, 0)
            Me.Controls.SetChildIndex(Me.Detail, 0)
            CType(Me, System.ComponentModel.ISupportInitialize).EndInit()

        End Sub

#End Region

        ''' <summary>
        ''' Print the old creditor ID
        ''' </summary>
        Private Sub XrLabel_old_creditor_BeforePrint(ByVal sender As Object, ByVal e As System.Drawing.Printing.PrintEventArgs)
            With CType(sender, XRLabel)
                Dim rpt As XtraReport = CType(.Report, XtraReport)
                .Text = CreditorLabel(rpt.GetCurrentColumnValue("old_creditor"))
            End With
        End Sub

        ''' <summary>
        ''' Print the new creditor ID
        ''' </summary>
        Private Sub new_creditor_BeforePrint(ByVal sender As Object, ByVal e As System.Drawing.Printing.PrintEventArgs)
            With CType(sender, XRLabel)
                Dim rpt As XtraReport = CType(.Report, XtraReport)
                .Text = CreditorLabel(rpt.GetCurrentColumnValue("new_creditor"))
            End With
        End Sub

        ''' <summary>
        ''' Print the client ID
        ''' </summary>
        Private Sub client_id_and_name_BeforePrint(ByVal sender As Object, ByVal e As System.Drawing.Printing.PrintEventArgs)
            With CType(sender, XRLabel)
                Dim rpt As XtraReport = CType(.Report, XtraReport)
                .Text = ClientLabel(rpt.GetCurrentColumnValue("client"))
            End With
        End Sub

        ''' <summary>
        ''' Retrieve the information for the creditor ID
        ''' </summary>
        Private Function CreditorLabel(ByVal creditorid As Object) As String
            Dim answer As String = String.Empty

            ' Conver the input to a string and find the row in the creditor table which is a match to the desired value.
            Dim Creditor As String
            If creditorid IsNot Nothing AndAlso creditorid IsNot System.DBNull.Value Then
                Creditor = Convert.ToString(creditorid)

                Dim row As DataRow
                If Creditor <> String.Empty Then
                    row = CreditorRow(Creditor)

                    ' If there is a row then return the creditor and the name
                    If row IsNot Nothing Then
                        answer = String.Format("[{0}] {1}", Creditor, row("creditor_name"))
                    Else
                        answer = String.Format("[{0}]", Creditor)
                    End If
                End If
            End If

            Return answer
        End Function

        ''' <summary>
        ''' Database access to find the creditor row in the table
        ''' </summary>
        Private Function CreditorRow(ByVal Creditor As String) As DataRow
            Const TableName As String = "creditors"
            Dim tbl As DataTable = ap.ds.Tables(TableName)

            If tbl IsNot Nothing Then
                Dim row As DataRow = tbl.Rows.Find(Creditor)
                If row IsNot Nothing Then Return row
            End If

            Dim current_cursor As Cursor = Cursor.Current
            Try
                Cursor.Current = Cursors.WaitCursor

                Using cmd As SqlClient.SqlCommand = New SqlCommand
                    With cmd
                        .Connection = New SqlConnection(DebtPlus.LINQ.SQLInfoClass.getDefault().ConnectionString)
                        .CommandText = "SELECT creditor, creditor_name FROM creditors WITH (NOLOCK) WHERE [creditor] = @creditor"
                        .CommandType = CommandType.Text
                        .Parameters.Add("@creditor", SqlDbType.VarChar, 10).Value = Creditor
                    End With

                    Using da As New SqlClient.SqlDataAdapter(cmd)
                        da.Fill(ap.ds, TableName)
                        tbl = ap.ds.Tables(TableName)

                        ' Ensure that there is a primary key to the table
                        If tbl.PrimaryKey.GetUpperBound(0) < 0 Then
                            tbl.PrimaryKey = New DataColumn() {tbl.Columns("creditor")}
                        End If
                    End Using
                End Using

            Catch ex As SqlClient.SqlException
                DebtPlus.UI.Common.ErrorHandling.HandleErrors(ex, "Error reading creditor information")

            Finally
                Cursor.Current = current_cursor
            End Try

            ' Do the search operation again
            If tbl IsNot Nothing Then
                Dim row As DataRow = tbl.Rows.Find(Creditor)
                If row IsNot Nothing Then Return row
            End If

            ' Return the failure condition
            Return Nothing
        End Function

        ''' <summary>
        ''' Retrieve the information for the client ID
        ''' </summary>
        Private Function ClientLabel(ByVal clientid As Object) As String
            Dim answer As String = String.Empty

            ' Conver the input to a string and find the row in the client table which is a match to the desired value.
            Dim Client As Int32
            If clientid IsNot Nothing AndAlso clientid IsNot System.DBNull.Value Then
                Client = Convert.ToInt32(clientid)
                Dim row As DataRow = ClientRow(Client)

                ' If there is a row then return the client and the name
                If row IsNot Nothing Then
                    answer = String.Format("[{0}] {1}", String.Format("{0:0000000}", Client), row("Client_name"))
                Else
                    answer = String.Format("[{0}]", String.Format("{0:0000000}", Client))
                End If
            End If

            Return answer
        End Function

        ''' <summary>
        ''' Database access to find the creditor row in the table
        ''' </summary>
        Private Function ClientRow(ByVal Client As Int32) As DataRow
            Const TableName As String = "clients"
            Dim tbl As DataTable = ap.ds.Tables(TableName)

            If tbl IsNot Nothing Then
                Dim row As DataRow = tbl.Rows.Find(Client)
                If row IsNot Nothing Then Return row
            End If

            Dim current_cursor As Cursor = Cursor.Current
            Try
                Cursor.Current = Cursors.WaitCursor

                Using cmd As SqlClient.SqlCommand = New SqlCommand
                    With cmd
                        .Connection = New SqlConnection(DebtPlus.LINQ.SQLInfoClass.getDefault().ConnectionString)
                        .CommandText = "SELECT client, name as client_name FROM view_client_address WITH (NOLOCK) WHERE [client] = @client"
                        .CommandType = CommandType.Text
                        .Parameters.Add("@client", SqlDbType.Int).Value = Client
                    End With

                    Using da As New SqlClient.SqlDataAdapter(cmd)
                        da.Fill(ap.ds, TableName)
                        tbl = ap.ds.Tables(TableName)

                        ' Ensure that there is a primary key to the table
                        If tbl.PrimaryKey.GetUpperBound(0) < 0 Then
                            tbl.PrimaryKey = New DataColumn() {tbl.Columns("client")}
                        End If
                    End Using
                End Using

            Catch ex As SqlClient.SqlException
                DebtPlus.UI.Common.ErrorHandling.HandleErrors(ex, "Error reading client information")

            Finally
                Cursor.Current = current_cursor
            End Try

            ' Do the search operation again
            If tbl IsNot Nothing Then
                Dim row As DataRow = tbl.Rows.Find(Client)
                If row IsNot Nothing Then Return row
            End If

            ' Return the failure condition
            Return Nothing
        End Function
    End Class

End Namespace
