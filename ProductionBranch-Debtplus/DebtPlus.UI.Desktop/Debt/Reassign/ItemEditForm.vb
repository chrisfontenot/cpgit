#Region "Copyright 2000-2012 DebtPlus, L.L.C."
'{*******************************************************************}
'{                                                                   }
'{       DebtPlus Debt Management System                             }
'{                                                                   }
'{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
'{       ALL RIGHTS RESERVED                                         }
'{                                                                   }
'{   The entire contents of this file is protected by U.S. and       }
'{   International Copyright Laws. Unauthorized reproduction,        }
'{   reverse-engineering, and distribution of all or any portion of  }
'{   the code contained in this file is strictly prohibited and may  }
'{   result in severe civil and criminal penalties and will be       }
'{   prosecuted to the maximum extent possible under the law.        }
'{                                                                   }
'{   RESTRICTIONS                                                    }
'{                                                                   }
'{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
'{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
'{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
'{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
'{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
'{                                                                   }
'{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
'{   ADDITIONAL RESTRICTIONS.                                        }
'{                                                                   }
'{*******************************************************************}
#End Region

Imports DebtPlus.UI.Creditor.Widgets.Controls
Imports System.Windows.Forms

Namespace Debt.Reassign
    Friend Class ItemEditForm

        ''' <summary>
        ''' The "Normal" way to create an instance of our class
        ''' </summary>
        Public Sub New()
            MyBase.New()
            InitializeComponent()

        End Sub

        Public Sub New(ByVal drv As System.Data.DataRowView)
            MyBase.New(drv)
            InitializeComponent()
            AddHandler Me.Load, AddressOf ItemEditForm_Load

        End Sub

        ''' <summary>
        ''' Load the form with the current information
        ''' </summary>
        Private Sub ItemEditForm_Load(ByVal sender As Object, ByVal e As System.EventArgs)

            With CreditorID1
                If drv IsNot Nothing Then
                    .DataBindings.Clear()
                    .DataBindings.Add(New Binding("EditValue", drv, "old_creditor"))
                End If
                lbl_old_creditor_name.Text = CreditorLabel(.EditValue)
                AddHandler .EditValueChanged, AddressOf EditValueChanged
                AddHandler .Validated, AddressOf CreditorID1_Validated
            End With
        End Sub

        ''' <summary>
        ''' Enable or disable the OK button
        ''' </summary>
        Protected Overrides Function HasErrors() As Boolean
            Return MyBase.HasErrors() OrElse Not ValidOldCreditor()
        End Function

        ''' <summary>
        ''' The new creditor is completly passed the input test. Look it up.
        ''' </summary>
        Protected Sub CreditorID1_Validated(ByVal sender As Object, ByVal e As System.EventArgs)
            With CType(sender, CreditorID)
                lbl_old_creditor_name.Text = CreditorLabel(.EditValue)
            End With
            Button_OK.Enabled = Not HasErrors()
        End Sub

        ''' <summary>
        ''' Validate the old creditor field
        ''' </summary>
        Private Function ValidOldCreditor() As Boolean
            Dim answer As Boolean = True
            If lbl_old_creditor_name.Text = String.Empty Then answer = False
            Return answer
        End Function
    End Class
End Namespace
