﻿
Imports DebtPlus.UI.Creditor.Widgets.Controls
Imports DebtPlus.UI.Creditor.Widgets

Namespace Debt.Reassign

    <Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
    Partial Class ItemEditForm
        Inherits Form_Reassign_EditTemplate

        'Form overrides dispose to clean up the component list.
        <System.Diagnostics.DebuggerNonUserCode()> _
        Protected Overrides Sub Dispose(ByVal disposing As Boolean)
            Try
                If disposing AndAlso components IsNot Nothing Then
                    components.Dispose()
                End If
            Finally
                MyBase.Dispose(disposing)
            End Try
        End Sub

        'Required by the Windows Form Designer
        Private components As System.ComponentModel.IContainer

        'NOTE: The following procedure is required by the Windows Form Designer
        'It can be modified using the Windows Form Designer.  
        'Do not modify it using the code editor.
        <System.Diagnostics.DebuggerStepThrough()> _
        Private Sub InitializeComponent()
            Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(ItemEditForm))
            Dim SerializableAppearanceObject1 As DevExpress.Utils.SerializableAppearanceObject = New DevExpress.Utils.SerializableAppearanceObject
            Me.LabelControl1 = New DevExpress.XtraEditors.LabelControl
            Me.CreditorID1 = New CreditorID
            Me.lbl_old_creditor_name = New DevExpress.XtraEditors.LabelControl
            CType(Me.TextEdit1.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.CreditorID2.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.DxErrorProvider1, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.CreditorID1.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
            Me.SuspendLayout()
            '
            'lbl_new_creditor_name
            '
            Me.lbl_new_creditor_name.Appearance.Options.UseTextOptions = True
            Me.lbl_new_creditor_name.Appearance.TextOptions.Trimming = DevExpress.Utils.Trimming.Character
            Me.lbl_new_creditor_name.Appearance.TextOptions.WordWrap = DevExpress.Utils.WordWrap.NoWrap
            Me.lbl_new_creditor_name.Location = New System.Drawing.Point(215, 69)
            Me.lbl_new_creditor_name.Size = New System.Drawing.Size(77, 13)
            Me.lbl_new_creditor_name.TabIndex = 7
            Me.lbl_new_creditor_name.Text = "XXXX NEW XXXX"
            '
            'Button_Cancel
            '
            Me.Button_Cancel.Location = New System.Drawing.Point(227, 118)
            Me.Button_Cancel.TabIndex = 9
            '
            'Button_OK
            '
            Me.Button_OK.Location = New System.Drawing.Point(123, 118)
            Me.Button_OK.TabIndex = 8
            '
            'TextEdit1
            '
            Me.TextEdit1.Location = New System.Drawing.Point(103, 39)
            Me.TextEdit1.Size = New System.Drawing.Size(294, 20)
            Me.TextEdit1.TabIndex = 4
            '
            'CreditorID2
            '
            Me.CreditorID2.Location = New System.Drawing.Point(103, 65)
            Me.CreditorID2.Properties.Mask.AutoComplete = DevExpress.XtraEditors.Mask.AutoCompleteType.Strong
            Me.CreditorID2.Properties.Mask.BeepOnError = True
            Me.CreditorID2.Properties.Mask.EditMask = "[A-Z]{1,2}[0-9]{4,}"
            Me.CreditorID2.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.RegEx
            Me.CreditorID2.Properties.Mask.UseMaskAsDisplayFormat = True
            Me.CreditorID2.TabIndex = 6
            '
            'LabelControl3
            '
            Me.LabelControl3.Location = New System.Drawing.Point(12, 43)
            Me.LabelControl3.TabIndex = 3
            '
            'LabelControl2
            '
            Me.LabelControl2.Location = New System.Drawing.Point(12, 69)
            Me.LabelControl2.TabIndex = 5

            '
            'LabelControl1
            '
            Me.LabelControl1.Location = New System.Drawing.Point(12, 17)
            Me.LabelControl1.Name = "LabelControl1"
            Me.LabelControl1.Size = New System.Drawing.Size(58, 13)
            Me.LabelControl1.TabIndex = 0
            Me.LabelControl1.Text = "Old Creditor"
            '
            'CreditorID1
            '
            Me.CreditorID1.EditValue = Nothing
            Me.CreditorID1.Location = New System.Drawing.Point(103, 13)
            Me.CreditorID1.Name = "CreditorID1"
            Me.CreditorID1.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph, "", -1, True, True, False, DevExpress.XtraEditors.ImageLocation.MiddleCenter, CType(resources.GetObject("CreditorID1.Properties.Buttons"), System.Drawing.Image), New DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), SerializableAppearanceObject1, "Click here to search for a creditor ID", "search", Nothing, True)})
            Me.CreditorID1.Properties.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
            Me.CreditorID1.Properties.Mask.AutoComplete = DevExpress.XtraEditors.Mask.AutoCompleteType.Strong
            Me.CreditorID1.Properties.Mask.BeepOnError = True
            Me.CreditorID1.Properties.Mask.EditMask = "[A-Z]{1,2}[0-9]{4,}"
            Me.CreditorID1.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.RegEx
            Me.CreditorID1.Properties.Mask.UseMaskAsDisplayFormat = True
            Me.CreditorID1.Properties.MaxLength = 10
            Me.CreditorID1.Size = New System.Drawing.Size(100, 20)
            Me.CreditorID1.TabIndex = 1
            '
            'lbl_old_creditor_name
            '
            Me.lbl_old_creditor_name.Location = New System.Drawing.Point(215, 17)
            Me.lbl_old_creditor_name.Name = "lbl_old_creditor_name"
            Me.lbl_old_creditor_name.Size = New System.Drawing.Size(68, 13)
            Me.lbl_old_creditor_name.TabIndex = 2
            Me.lbl_old_creditor_name.Text = "XXX OLD XXXX"
            Me.lbl_old_creditor_name.UseMnemonic = False
            '
            'ItemEditForm
            '
            Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
            Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
            Me.ClientSize = New System.Drawing.Size(409, 153)
            Me.Controls.Add(Me.lbl_old_creditor_name)
            Me.Controls.Add(Me.LabelControl1)
            Me.Controls.Add(Me.CreditorID1)
            Me.Name = "ItemEditForm"
            Me.Text = "Assignment"
            Me.Controls.SetChildIndex(Me.CreditorID1, 0)
            Me.Controls.SetChildIndex(Me.LabelControl1, 0)
            Me.Controls.SetChildIndex(Me.lbl_new_creditor_name, 0)
            Me.Controls.SetChildIndex(Me.TextEdit1, 0)
            Me.Controls.SetChildIndex(Me.LabelControl3, 0)
            Me.Controls.SetChildIndex(Me.LabelControl2, 0)
            Me.Controls.SetChildIndex(Me.CreditorID2, 0)
            Me.Controls.SetChildIndex(Me.Button_OK, 0)
            Me.Controls.SetChildIndex(Me.Button_Cancel, 0)
            Me.Controls.SetChildIndex(Me.lbl_old_creditor_name, 0)
            CType(Me.TextEdit1.Properties, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.CreditorID2.Properties, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.DxErrorProvider1, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.CreditorID1.Properties, System.ComponentModel.ISupportInitialize).EndInit()
            Me.ResumeLayout(False)
            Me.PerformLayout()

        End Sub
        Friend WithEvents LabelControl1 As DevExpress.XtraEditors.LabelControl
        Friend WithEvents CreditorID1 As CreditorID
        Friend WithEvents lbl_old_creditor_name As DevExpress.XtraEditors.LabelControl
    End Class
End Namespace