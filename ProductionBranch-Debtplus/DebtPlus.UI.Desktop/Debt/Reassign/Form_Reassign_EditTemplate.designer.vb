﻿
Imports DebtPlus.UI.Creditor.Widgets.Controls
Imports DebtPlus.UI.Creditor.Widgets

Namespace Debt.Reassign

    <Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
    Partial Class Form_Reassign_EditTemplate
        Inherits DebtPlus.Data.Forms.DebtPlusForm

        'Form overrides dispose to clean up the component list.
        <System.Diagnostics.DebuggerNonUserCode()> _
        Protected Overrides Sub Dispose(ByVal disposing As Boolean)
            Try
                If disposing AndAlso components IsNot Nothing Then
                    components.Dispose()
                End If
            Finally
                MyBase.Dispose(disposing)
            End Try
        End Sub

        'Required by the Windows Form Designer
        Private components As System.ComponentModel.IContainer

        'NOTE: The following procedure is required by the Windows Form Designer
        'It can be modified using the Windows Form Designer.  
        'Do not modify it using the code editor.
        <System.Diagnostics.DebuggerStepThrough()> _
        Private Sub InitializeComponent()
            Dim SuperToolTip1 As DevExpress.Utils.SuperToolTip = New DevExpress.Utils.SuperToolTip
            Dim ToolTipTitleItem1 As DevExpress.Utils.ToolTipTitleItem = New DevExpress.Utils.ToolTipTitleItem
            Dim ToolTipItem1 As DevExpress.Utils.ToolTipItem = New DevExpress.Utils.ToolTipItem
            Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(Form_Reassign_EditTemplate))
            Dim SerializableAppearanceObject1 As DevExpress.Utils.SerializableAppearanceObject = New DevExpress.Utils.SerializableAppearanceObject
            Me.lbl_new_creditor_name = New DevExpress.XtraEditors.LabelControl
            Me.Button_Cancel = New DevExpress.XtraEditors.SimpleButton
            Me.Button_OK = New DevExpress.XtraEditors.SimpleButton
            Me.TextEdit1 = New DevExpress.XtraEditors.TextEdit
            Me.CreditorID2 = New CreditorID
            Me.LabelControl3 = New DevExpress.XtraEditors.LabelControl
            Me.LabelControl2 = New DevExpress.XtraEditors.LabelControl
            CType(Me.DxErrorProvider1, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.TextEdit1.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.CreditorID2.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
            Me.SuspendLayout()
            '
            'lbl_new_creditor_name
            '
            Me.lbl_new_creditor_name.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                        Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
            Me.lbl_new_creditor_name.Appearance.Options.UseTextOptions = True
            Me.lbl_new_creditor_name.Appearance.TextOptions.Trimming = DevExpress.Utils.Trimming.Character
            Me.lbl_new_creditor_name.Appearance.TextOptions.WordWrap = DevExpress.Utils.WordWrap.NoWrap
            Me.lbl_new_creditor_name.Location = New System.Drawing.Point(209, 42)
            Me.lbl_new_creditor_name.Name = "lbl_new_creditor_name"
            Me.lbl_new_creditor_name.Size = New System.Drawing.Size(0, 13)
            Me.lbl_new_creditor_name.TabIndex = 4
            Me.lbl_new_creditor_name.UseMnemonic = False
            '
            'Button_Cancel
            '
            Me.Button_Cancel.Anchor = System.Windows.Forms.AnchorStyles.Bottom
            Me.Button_Cancel.DialogResult = System.Windows.Forms.DialogResult.Cancel
            Me.Button_Cancel.Location = New System.Drawing.Point(205, 84)
            Me.Button_Cancel.Name = "Button_Cancel"
            Me.Button_Cancel.Size = New System.Drawing.Size(75, 23)
            Me.Button_Cancel.TabIndex = 6
            Me.Button_Cancel.Text = "&Cancel"
            '
            'Button_OK
            '
            Me.Button_OK.Anchor = System.Windows.Forms.AnchorStyles.Bottom
            Me.Button_OK.DialogResult = System.Windows.Forms.DialogResult.OK
            Me.Button_OK.Location = New System.Drawing.Point(101, 84)
            Me.Button_OK.Name = "Button_OK"
            Me.Button_OK.Size = New System.Drawing.Size(75, 23)
            Me.Button_OK.TabIndex = 5
            Me.Button_OK.Text = "&OK"
            '
            'TextEdit1
            '
            Me.TextEdit1.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                        Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
            Me.TextEdit1.Location = New System.Drawing.Point(103, 12)
            Me.TextEdit1.Name = "TextEdit1"
            Me.TextEdit1.Size = New System.Drawing.Size(246, 20)
            ToolTipTitleItem1.Text = "Account Number Mask"
            ToolTipItem1.LeftIndent = 6
            ToolTipItem1.Text = resources.GetString("ToolTipItem1.Text")
            SuperToolTip1.Items.Add(ToolTipTitleItem1)
            SuperToolTip1.Items.Add(ToolTipItem1)
            Me.TextEdit1.SuperTip = SuperToolTip1
            Me.TextEdit1.TabIndex = 1
            '
            'CreditorID2
            '
            Me.CreditorID2.EditValue = Nothing
            Me.CreditorID2.Location = New System.Drawing.Point(103, 39)
            Me.CreditorID2.Name = "CreditorID2"
            Me.CreditorID2.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph, "", -1, True, True, False, DevExpress.XtraEditors.ImageLocation.MiddleCenter, CType(resources.GetObject("CreditorID2.Properties.Buttons"), System.Drawing.Image), New DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), SerializableAppearanceObject1, "Click here to search for a creditor ID", "search", Nothing, True)})
            Me.CreditorID2.Properties.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
            Me.CreditorID2.Properties.Mask.AutoComplete = DevExpress.XtraEditors.Mask.AutoCompleteType.Strong
            Me.CreditorID2.Properties.Mask.BeepOnError = True
            Me.CreditorID2.Properties.Mask.EditMask = "[A-Z]{1,2}[0-9]{4,}"
            Me.CreditorID2.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.RegEx
            Me.CreditorID2.Properties.Mask.UseMaskAsDisplayFormat = True
            Me.CreditorID2.Properties.MaxLength = 10
            Me.CreditorID2.Size = New System.Drawing.Size(100, 20)
            Me.CreditorID2.TabIndex = 3
            Me.CreditorID2.ValidationLevel = CreditorID.ValidationLevelEnum.NotProhibitUse
            '
            'LabelControl3
            '
            Me.LabelControl3.Location = New System.Drawing.Point(12, 15)
            Me.LabelControl3.Name = "LabelControl3"
            Me.LabelControl3.Size = New System.Drawing.Size(79, 13)
            Me.LabelControl3.TabIndex = 0
            Me.LabelControl3.Text = "Account Number"
            '
            'LabelControl2
            '
            Me.LabelControl2.Location = New System.Drawing.Point(12, 42)
            Me.LabelControl2.Name = "LabelControl2"
            Me.LabelControl2.Size = New System.Drawing.Size(63, 13)
            Me.LabelControl2.TabIndex = 2
            Me.LabelControl2.Text = "New Creditor"

            '
            'Form_Reassign_EditTemplate
            '
            Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
            Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
            Me.ClientSize = New System.Drawing.Size(361, 119)
            Me.Controls.Add(Me.lbl_new_creditor_name)
            Me.Controls.Add(Me.Button_Cancel)
            Me.Controls.Add(Me.Button_OK)
            Me.Controls.Add(Me.TextEdit1)
            Me.Controls.Add(Me.CreditorID2)
            Me.Controls.Add(Me.LabelControl3)
            Me.Controls.Add(Me.LabelControl2)
            Me.Name = "Form_Reassign_EditTemplate"
            Me.ToolTipController1.SetSuperTip(Me, Nothing)
            Me.Text = "Reassign Debts"
            CType(Me.DxErrorProvider1, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.TextEdit1.Properties, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.CreditorID2.Properties, System.ComponentModel.ISupportInitialize).EndInit()
            Me.ResumeLayout(False)
            Me.PerformLayout()

        End Sub
        Protected Friend WithEvents lbl_new_creditor_name As DevExpress.XtraEditors.LabelControl
        Protected Friend WithEvents Button_Cancel As DevExpress.XtraEditors.SimpleButton
        Protected Friend WithEvents Button_OK As DevExpress.XtraEditors.SimpleButton
        Protected Friend WithEvents TextEdit1 As DevExpress.XtraEditors.TextEdit
        Protected Friend WithEvents CreditorID2 As CreditorID
        Protected Friend WithEvents LabelControl3 As DevExpress.XtraEditors.LabelControl
        Protected Friend WithEvents LabelControl2 As DevExpress.XtraEditors.LabelControl
    End Class
End Namespace