#Region "Copyright 2000-2012 DebtPlus, L.L.C."
'{*******************************************************************}
'{                                                                   }
'{       DebtPlus Debt Management System                             }
'{                                                                   }
'{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
'{       ALL RIGHTS RESERVED                                         }
'{                                                                   }
'{   The entire contents of this file is protected by U.S. and       }
'{   International Copyright Laws. Unauthorized reproduction,        }
'{   reverse-engineering, and distribution of all or any portion of  }
'{   the code contained in this file is strictly prohibited and may  }
'{   result in severe civil and criminal penalties and will be       }
'{   prosecuted to the maximum extent possible under the law.        }
'{                                                                   }
'{   RESTRICTIONS                                                    }
'{                                                                   }
'{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
'{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
'{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
'{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
'{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
'{                                                                   }
'{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
'{   ADDITIONAL RESTRICTIONS.                                        }
'{                                                                   }
'{*******************************************************************}
#End Region

Imports DebtPlus.UI.Creditor.Widgets.Controls
Imports DebtPlus.Utils
Imports System.Data.SqlClient
Imports System.Windows.Forms

Namespace Debt.Reassign
    Friend Class Form_Reassign_EditTemplate

        Protected drv As System.Data.DataRowView = Nothing

        ''' <summary>
        ''' The "Normal" way to create an instance of our class
        ''' </summary>
        Public Sub New()
            MyBase.New()
            InitializeComponent()
        End Sub

        Public Sub New(ByVal drv As System.Data.DataRowView)
            MyClass.New()
            Me.drv = drv

            AddHandler Me.Load, AddressOf ItemEditForm_Load
        End Sub

        ''' <summary>
        ''' Load the form with the current information
        ''' </summary>
        Private Sub ItemEditForm_Load(ByVal sender As Object, ByVal e As System.EventArgs)
            With TextEdit1
                If drv IsNot Nothing Then
                    .DataBindings.Clear()
                    .DataBindings.Add(New System.Windows.Forms.Binding("EditValue", drv, "account_number"))
                End If
                AddHandler .EditValueChanged, AddressOf EditValueChanged
            End With

            With CreditorID2
                If drv IsNot Nothing Then
                    .DataBindings.Clear()
                    .DataBindings.Add(New Binding("EditValue", drv, "new_creditor"))
                End If
                lbl_new_creditor_name.Text = CreditorLabel(.EditValue)
                AddHandler .EditValueChanged, AddressOf EditValueChanged
                AddHandler .Validated, AddressOf CreditorID2_Validated
            End With

            Button_OK.Enabled = Not HasErrors()
        End Sub

        ''' <summary>
        ''' Handle a change in the field
        ''' </summary>
        Protected Sub EditValueChanged(ByVal sender As Object, ByVal e As System.EventArgs)
            Button_OK.Enabled = Not HasErrors()
        End Sub

        ''' <summary>
        ''' Determine if the form has an error
        ''' </summary>
        Protected Overridable Function HasErrors() As Boolean
            Dim answer As Boolean = True

            Do
                If Not ValidNewCreditor() Then Exit Do
                If Not ValidMask() Then Exit Do
                answer = False
                Exit Do
            Loop

            Return answer
        End Function

        ''' <summary>
        ''' Validate the new creditor field
        ''' </summary>
        Protected Function ValidNewCreditor() As Boolean
            Dim answer As Boolean = True
            If lbl_new_creditor_name.Text = String.Empty Then answer = False
            Return answer
        End Function

        ''' <summary>
        ''' Validate the account number mask field
        ''' </summary>
        Protected Function ValidMask() As Boolean
            Dim answer As Boolean = True

            Do
                Dim MaskString As String = TextEdit1.Text.Trim()

                ' There must be at least two characters or it is not acceptable
                If MaskString.Length >= 1 Then

                    ' Wild card characters are acceptable anywhere
                    If MaskString.IndexOfAny(New Char() {"%"c, "_"c}) >= 0 Then Exit Do

                    ' Look for a [....] syntax
                    Dim First As System.Int32 = MaskString.IndexOf("["c)
                    If First >= 0 Then
                        If MaskString.IndexOf("]"c, First) > 0 Then
                            Exit Do
                        End If
                    End If
                End If

                answer = False
                Exit Do
            Loop

            Return answer
        End Function

        ''' <summary>
        ''' The new creditor is completly passed the input test. Look it up.
        ''' </summary>
        Protected Sub CreditorID2_Validated(ByVal sender As Object, ByVal e As System.EventArgs)
            With CType(sender, CreditorID)
                lbl_new_creditor_name.Text = CreditorLabel(.EditValue)
            End With
            Button_OK.Enabled = Not HasErrors()
        End Sub

        ''' <summary>
        ''' Retrieve the information for the creditor ID
        ''' </summary>
        Private dsLocal As New System.Data.DataSet("dsLocal")
        Protected Function CreditorLabel(ByVal old_creditor_id As String) As String
            Dim answer As String = String.Empty

            ' If there is a creditor then try to find the creditor name
            If old_creditor_id <> String.Empty Then
                Dim creditor_view As System.Data.DataView = Nothing
                Dim creditors As System.Data.DataTable = dsLocal.Tables("creditors")

                If creditors IsNot Nothing Then
                    creditor_view = New System.Data.DataView(creditors, String.Format("[creditor]='{0}'", old_creditor_id), String.Empty, DataViewRowState.CurrentRows)
                    If creditor_view.Count = 0 Then
                        creditor_view = Nothing
                    End If
                End If

                ' If the creditor is not in the cache, load it.
                If creditor_view Is Nothing Then
                    Dim cmd As SqlClient.SqlCommand = New SqlCommand
                    With cmd
                        .Connection = New SqlConnection(DebtPlus.LINQ.SQLInfoClass.getDefault().ConnectionString)
                        .CommandText = "SELECT creditor,creditor_name FROM creditors WITH (NOLOCK) WHERE creditor = @creditor"
                        .Parameters.Add("@creditor", SqlDbType.VarChar, 10).Value = old_creditor_id
                    End With

                    Dim current_cursor As System.Windows.Forms.Cursor = System.Windows.Forms.Cursor.Current
                    Try
                        System.Windows.Forms.Cursor.Current = System.Windows.Forms.Cursors.WaitCursor
                        Dim da As New SqlClient.SqlDataAdapter(cmd)
                        da.Fill(dsLocal, "creditors")
                        creditors = dsLocal.Tables("creditors")
                        If creditors IsNot Nothing Then
                            creditor_view = New System.Data.DataView(creditors, String.Format("[creditor]='{0}'", old_creditor_id), String.Empty, DataViewRowState.CurrentRows)
                        End If

                    Catch ex As SqlClient.SqlException
                        DebtPlus.UI.Common.ErrorHandling.HandleErrors(ex, "Error reading creditor information")

                    Finally
                        System.Windows.Forms.Cursor.Current = current_cursor
                    End Try
                End If

                ' Find the one item for the view of this creditor ID
                If creditor_view IsNot Nothing AndAlso creditor_view.Count > 0 Then
                    Dim drv As System.Data.DataRowView = creditor_view(0)
                    If drv IsNot Nothing AndAlso drv("creditor_name") IsNot Nothing AndAlso drv("creditor_name") IsNot System.DBNull.Value Then
                        answer = Convert.ToString(drv("creditor_name"))
                    End If
                End If
            End If

            Return answer
        End Function
    End Class
End Namespace
