#Region "Copyright 2000-2012 DebtPlus, L.L.C."
'{*******************************************************************}
'{                                                                   }
'{       DebtPlus Debt Management System                             }
'{                                                                   }
'{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
'{       ALL RIGHTS RESERVED                                         }
'{                                                                   }
'{   The entire contents of this file is protected by U.S. and       }
'{   International Copyright Laws. Unauthorized reproduction,        }
'{   reverse-engineering, and distribution of all or any portion of  }
'{   the code contained in this file is strictly prohibited and may  }
'{   result in severe civil and criminal penalties and will be       }
'{   prosecuted to the maximum extent possible under the law.        }
'{                                                                   }
'{   RESTRICTIONS                                                    }
'{                                                                   }
'{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
'{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
'{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
'{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
'{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
'{                                                                   }
'{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
'{   ADDITIONAL RESTRICTIONS.                                        }
'{                                                                   }
'{*******************************************************************}
#End Region

Option Compare Binary
Option Explicit On
Option Strict On

Imports System.Data.SqlClient
Imports DebtPlus.UI.Desktop.Debt.Reassign.Reports
Imports System.Windows.Forms
Imports DevExpress.XtraReports.UI

Namespace Debt.Reassign

    Friend Class Form1
        Inherits DebtPlus.Data.Forms.DebtPlusForm

        Friend ap As XmlArgParser = Nothing
        Public Sub New(ByVal ap As XmlArgParser)
            MyClass.New()
            Me.ap = ap

            AddHandler Button_Cancel.Click, AddressOf Button_Cancel_Click
            AddHandler Button_OK.Click, AddressOf Button_OK_Click
            AddHandler MenuItem_Add.Click, AddressOf MenuItem_Add_Click
            AddHandler MenuItem_Change.Click, AddressOf MenuItem_Change_Click
            AddHandler MenuItem_Delete.Click, AddressOf MenuItem_Delete_Click
            AddHandler MenuItem_Print.Click, AddressOf MenuItem_Print_Click
            AddHandler bt.DoWork, AddressOf bt_DoWork
            AddHandler ContextMenu1.Popup, AddressOf ContextMenu1_Popup
            AddHandler BarButtonItem_File_Print.ItemClick, AddressOf BarButtonItem_File_Print_ItemClick
            AddHandler BarButtonItem_File_Exit.ItemClick, AddressOf BarButtonItem_File_Exit_ItemClick
            AddHandler Me.Load, AddressOf Form1_Load
        End Sub

        Dim vue_change_list As System.Data.DataView
        Friend WithEvents barManager1 As DevExpress.XtraBars.BarManager
        Friend WithEvents Bar2 As DevExpress.XtraBars.Bar
        Friend WithEvents BarSubItem1 As DevExpress.XtraBars.BarSubItem
        Friend WithEvents BarButtonItem_File_Print As DevExpress.XtraBars.BarButtonItem
        Friend WithEvents BarButtonItem_File_Exit As DevExpress.XtraBars.BarButtonItem
        Friend WithEvents barDockControlTop As DevExpress.XtraBars.BarDockControl
        Friend WithEvents barDockControlBottom As DevExpress.XtraBars.BarDockControl
        Friend WithEvents barDockControlLeft As DevExpress.XtraBars.BarDockControl
        Friend WithEvents barDockControlRight As DevExpress.XtraBars.BarDockControl
        Friend WithEvents PanelControl1 As DevExpress.XtraEditors.PanelControl
        Friend WithEvents Button_Cancel As DevExpress.XtraEditors.SimpleButton
        Friend WithEvents Button_OK As DevExpress.XtraEditors.SimpleButton
        Friend WithEvents generate_proposals As DevExpress.XtraEditors.CheckEdit
        Friend WithEvents GridControl1 As DevExpress.XtraGrid.GridControl
        Friend WithEvents GridView1 As DevExpress.XtraGrid.Views.Grid.GridView
        Friend WithEvents col_old_creditor As DevExpress.XtraGrid.Columns.GridColumn
        Friend WithEvents col_account_number As DevExpress.XtraGrid.Columns.GridColumn
        Friend WithEvents col_new_creditor As DevExpress.XtraGrid.Columns.GridColumn
        Dim WithEvents bt As New System.ComponentModel.BackgroundWorker

#Region " Windows Form Designer generated code "

        Public Sub New()
            MyBase.New()

            'This call is required by the Windows Form Designer.
            InitializeComponent()

            'Add any initialization after the InitializeComponent() call

        End Sub

        'Form overrides dispose to clean up the component list.
        Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
            If disposing Then
                If components IsNot Nothing Then
                    components.Dispose()
                End If
            End If
            MyBase.Dispose(disposing)
        End Sub

        'Required by the Windows Form Designer
        Private components As System.ComponentModel.IContainer

        'NOTE: The following procedure is required by the Windows Form Designer
        'It can be modified using the Windows Form Designer.  
        'Do not modify it using the code editor.
        Friend WithEvents ContextMenu1 As System.Windows.Forms.ContextMenu
        Friend WithEvents MenuItem_Add As System.Windows.Forms.MenuItem
        Friend WithEvents MenuItem_Change As System.Windows.Forms.MenuItem
        Friend WithEvents MenuItem_Delete As System.Windows.Forms.MenuItem
        Friend WithEvents MenuItem4 As System.Windows.Forms.MenuItem
        Friend WithEvents MenuItem_Print As System.Windows.Forms.MenuItem
        <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
            Me.components = New System.ComponentModel.Container
            Me.ContextMenu1 = New System.Windows.Forms.ContextMenu
            Me.MenuItem_Add = New System.Windows.Forms.MenuItem
            Me.MenuItem_Change = New System.Windows.Forms.MenuItem
            Me.MenuItem_Delete = New System.Windows.Forms.MenuItem
            Me.MenuItem4 = New System.Windows.Forms.MenuItem
            Me.MenuItem_Print = New System.Windows.Forms.MenuItem
            Me.barManager1 = New DevExpress.XtraBars.BarManager(Me.components)
            Me.Bar2 = New DevExpress.XtraBars.Bar
            Me.BarSubItem1 = New DevExpress.XtraBars.BarSubItem
            Me.BarButtonItem_File_Print = New DevExpress.XtraBars.BarButtonItem
            Me.BarButtonItem_File_Exit = New DevExpress.XtraBars.BarButtonItem
            Me.barDockControlTop = New DevExpress.XtraBars.BarDockControl
            Me.barDockControlBottom = New DevExpress.XtraBars.BarDockControl
            Me.barDockControlLeft = New DevExpress.XtraBars.BarDockControl
            Me.barDockControlRight = New DevExpress.XtraBars.BarDockControl
            Me.PanelControl1 = New DevExpress.XtraEditors.PanelControl
            Me.Button_Cancel = New DevExpress.XtraEditors.SimpleButton
            Me.Button_OK = New DevExpress.XtraEditors.SimpleButton
            Me.generate_proposals = New DevExpress.XtraEditors.CheckEdit
            Me.GridControl1 = New DevExpress.XtraGrid.GridControl
            Me.GridView1 = New DevExpress.XtraGrid.Views.Grid.GridView
            Me.col_old_creditor = New DevExpress.XtraGrid.Columns.GridColumn
            Me.col_old_creditor.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.True
            Me.col_account_number = New DevExpress.XtraGrid.Columns.GridColumn
            Me.col_account_number.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.True
            Me.col_new_creditor = New DevExpress.XtraGrid.Columns.GridColumn
            Me.col_new_creditor.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.True
            CType(Me.DxErrorProvider1, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.barManager1, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.PanelControl1, System.ComponentModel.ISupportInitialize).BeginInit()
            Me.PanelControl1.SuspendLayout()
            CType(Me.generate_proposals.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.GridControl1, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.GridView1, System.ComponentModel.ISupportInitialize).BeginInit()
            Me.SuspendLayout()
            '
            'ContextMenu1
            '
            Me.ContextMenu1.MenuItems.AddRange(New System.Windows.Forms.MenuItem() {Me.MenuItem_Add, Me.MenuItem_Change, Me.MenuItem_Delete, Me.MenuItem4, Me.MenuItem_Print})
            '
            'MenuItem_Add
            '
            Me.MenuItem_Add.Index = 0
            Me.MenuItem_Add.Text = "&Add"
            '
            'MenuItem_Change
            '
            Me.MenuItem_Change.Index = 1
            Me.MenuItem_Change.Text = "&Change"
            '
            'MenuItem_Delete
            '
            Me.MenuItem_Delete.Index = 2
            Me.MenuItem_Delete.Text = "&Delete"
            '
            'MenuItem4
            '
            Me.MenuItem4.Index = 3
            Me.MenuItem4.Text = "-"
            '
            'MenuItem_Print
            '
            Me.MenuItem_Print.Index = 4
            Me.MenuItem_Print.Text = "&Print..."
            '
            'barManager1
            '
            Me.barManager1.Bars.AddRange(New DevExpress.XtraBars.Bar() {Me.Bar2})
            Me.barManager1.DockControls.Add(Me.barDockControlTop)
            Me.barManager1.DockControls.Add(Me.barDockControlBottom)
            Me.barManager1.DockControls.Add(Me.barDockControlLeft)
            Me.barManager1.DockControls.Add(Me.barDockControlRight)
            Me.barManager1.Form = Me
            Me.barManager1.Items.AddRange(New DevExpress.XtraBars.BarItem() {Me.BarSubItem1, Me.BarButtonItem_File_Print, Me.BarButtonItem_File_Exit})
            Me.barManager1.MainMenu = Me.Bar2
            Me.barManager1.MaxItemId = 3
            '
            'Bar2
            '
            Me.Bar2.BarName = "Main menu"
            Me.Bar2.DockCol = 0
            Me.Bar2.DockRow = 0
            Me.Bar2.DockStyle = DevExpress.XtraBars.BarDockStyle.Top
            Me.Bar2.LinksPersistInfo.AddRange(New DevExpress.XtraBars.LinkPersistInfo() {New DevExpress.XtraBars.LinkPersistInfo(Me.BarSubItem1)})
            Me.Bar2.OptionsBar.MultiLine = True
            Me.Bar2.OptionsBar.UseWholeRow = True
            Me.Bar2.Text = "Main menu"
            '
            'BarSubItem1
            '
            Me.BarSubItem1.Caption = "&File"
            Me.BarSubItem1.Id = 0
            Me.BarSubItem1.LinksPersistInfo.AddRange(New DevExpress.XtraBars.LinkPersistInfo() {New DevExpress.XtraBars.LinkPersistInfo(Me.BarButtonItem_File_Print), New DevExpress.XtraBars.LinkPersistInfo(Me.BarButtonItem_File_Exit)})
            Me.BarSubItem1.Name = "BarSubItem1"
            '
            'BarButtonItem_File_Print
            '
            Me.BarButtonItem_File_Print.Caption = "&Print..."
            Me.BarButtonItem_File_Print.Id = 1
            Me.BarButtonItem_File_Print.Name = "BarButtonItem_File_Print"
            '
            'BarButtonItem_File_Exit
            '
            Me.BarButtonItem_File_Exit.Caption = "&Exit..."
            Me.BarButtonItem_File_Exit.Id = 2
            Me.BarButtonItem_File_Exit.Name = "BarButtonItem_File_Exit"
            '
            'barDockControlTop
            '
            Me.ToolTipController1.SetSuperTip(Me.barDockControlTop, Nothing)
            '
            'barDockControlBottom
            '
            Me.ToolTipController1.SetSuperTip(Me.barDockControlBottom, Nothing)
            '
            'barDockControlLeft
            '
            Me.ToolTipController1.SetSuperTip(Me.barDockControlLeft, Nothing)
            '
            'barDockControlRight
            '
            Me.ToolTipController1.SetSuperTip(Me.barDockControlRight, Nothing)
            '
            'PanelControl1
            '
            Me.PanelControl1.Controls.Add(Me.Button_Cancel)
            Me.PanelControl1.Controls.Add(Me.Button_OK)
            Me.PanelControl1.Controls.Add(Me.generate_proposals)
            Me.PanelControl1.Controls.Add(Me.GridControl1)
            Me.PanelControl1.Dock = System.Windows.Forms.DockStyle.Fill
            Me.PanelControl1.Location = New System.Drawing.Point(0, 24)
            Me.PanelControl1.Name = "PanelControl1"
            Me.PanelControl1.Size = New System.Drawing.Size(504, 242)
            Me.ToolTipController1.SetSuperTip(Me.PanelControl1, Nothing)
            Me.PanelControl1.TabIndex = 4
            '
            'Button_Cancel
            '
            Me.Button_Cancel.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
            Me.Button_Cancel.DialogResult = System.Windows.Forms.DialogResult.Cancel
            Me.Button_Cancel.Location = New System.Drawing.Point(423, 50)
            Me.Button_Cancel.Name = "Button_Cancel"
            Me.Button_Cancel.Size = New System.Drawing.Size(75, 23)
            Me.Button_Cancel.TabIndex = 7
            Me.Button_Cancel.Text = "&Cancel"
            '
            'Button_OK
            '
            Me.Button_OK.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
            Me.Button_OK.Enabled = False
            Me.Button_OK.Location = New System.Drawing.Point(423, 21)
            Me.Button_OK.Name = "Button_OK"
            Me.Button_OK.Size = New System.Drawing.Size(75, 23)
            Me.Button_OK.TabIndex = 6
            Me.Button_OK.Text = "&OK"
            '
            'generate_proposals
            '
            Me.generate_proposals.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
            Me.generate_proposals.Location = New System.Drawing.Point(7, 215)
            Me.generate_proposals.Name = "generate_proposals"
            Me.generate_proposals.Properties.Caption = "Generate proposals for all of the changes"
            Me.generate_proposals.Size = New System.Drawing.Size(392, 19)
            Me.generate_proposals.TabIndex = 5
            '
            'GridControl1
            '
            Me.GridControl1.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                        Or System.Windows.Forms.AnchorStyles.Left) _
                        Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
            Me.GridControl1.ContextMenu = Me.ContextMenu1
            Me.GridControl1.EmbeddedNavigator.Name = ""
            Me.GridControl1.Location = New System.Drawing.Point(9, 9)
            Me.GridControl1.MainView = Me.GridView1
            Me.GridControl1.Name = "GridControl1"
            Me.GridControl1.Size = New System.Drawing.Size(400, 200)
            Me.GridControl1.TabIndex = 4
            Me.GridControl1.ViewCollection.AddRange(New DevExpress.XtraGrid.Views.Base.BaseView() {Me.GridView1})
            '
            'GridView1
            '
            Me.GridView1.Appearance.ColumnFilterButton.BackColor = System.Drawing.Color.FromArgb(CType(CType(104, Byte), Int32), CType(CType(184, Byte), Int32), CType(CType(251, Byte), Int32))
            Me.GridView1.Appearance.ColumnFilterButton.BorderColor = System.Drawing.Color.FromArgb(CType(CType(104, Byte), Int32), CType(CType(184, Byte), Int32), CType(CType(251, Byte), Int32))
            Me.GridView1.Appearance.ColumnFilterButton.ForeColor = System.Drawing.Color.White
            Me.GridView1.Appearance.ColumnFilterButton.Options.UseBackColor = True
            Me.GridView1.Appearance.ColumnFilterButton.Options.UseBorderColor = True
            Me.GridView1.Appearance.ColumnFilterButton.Options.UseForeColor = True
            Me.GridView1.Appearance.ColumnFilterButtonActive.BackColor = System.Drawing.Color.FromArgb(CType(CType(170, Byte), Int32), CType(CType(216, Byte), Int32), CType(CType(254, Byte), Int32))
            Me.GridView1.Appearance.ColumnFilterButtonActive.BorderColor = System.Drawing.Color.FromArgb(CType(CType(170, Byte), Int32), CType(CType(216, Byte), Int32), CType(CType(254, Byte), Int32))
            Me.GridView1.Appearance.ColumnFilterButtonActive.ForeColor = System.Drawing.Color.Black
            Me.GridView1.Appearance.ColumnFilterButtonActive.Options.UseBackColor = True
            Me.GridView1.Appearance.ColumnFilterButtonActive.Options.UseBorderColor = True
            Me.GridView1.Appearance.ColumnFilterButtonActive.Options.UseForeColor = True
            Me.GridView1.Appearance.Empty.BackColor = System.Drawing.Color.FromArgb(CType(CType(236, Byte), Int32), CType(CType(246, Byte), Int32), CType(CType(255, Byte), Int32))
            Me.GridView1.Appearance.Empty.BackColor2 = System.Drawing.Color.White
            Me.GridView1.Appearance.Empty.Options.UseBackColor = True
            Me.GridView1.Appearance.EvenRow.BackColor = System.Drawing.Color.FromArgb(CType(CType(247, Byte), Int32), CType(CType(251, Byte), Int32), CType(CType(255, Byte), Int32))
            Me.GridView1.Appearance.EvenRow.BorderColor = System.Drawing.Color.FromArgb(CType(CType(247, Byte), Int32), CType(CType(251, Byte), Int32), CType(CType(255, Byte), Int32))
            Me.GridView1.Appearance.EvenRow.ForeColor = System.Drawing.Color.Black
            Me.GridView1.Appearance.EvenRow.Options.UseBackColor = True
            Me.GridView1.Appearance.EvenRow.Options.UseBorderColor = True
            Me.GridView1.Appearance.EvenRow.Options.UseForeColor = True
            Me.GridView1.Appearance.FilterCloseButton.BackColor = System.Drawing.Color.FromArgb(CType(CType(104, Byte), Int32), CType(CType(184, Byte), Int32), CType(CType(251, Byte), Int32))
            Me.GridView1.Appearance.FilterCloseButton.BorderColor = System.Drawing.Color.FromArgb(CType(CType(104, Byte), Int32), CType(CType(184, Byte), Int32), CType(CType(251, Byte), Int32))
            Me.GridView1.Appearance.FilterCloseButton.ForeColor = System.Drawing.Color.White
            Me.GridView1.Appearance.FilterCloseButton.Options.UseBackColor = True
            Me.GridView1.Appearance.FilterCloseButton.Options.UseBorderColor = True
            Me.GridView1.Appearance.FilterCloseButton.Options.UseForeColor = True
            Me.GridView1.Appearance.FilterPanel.BackColor = System.Drawing.Color.FromArgb(CType(CType(236, Byte), Int32), CType(CType(246, Byte), Int32), CType(CType(255, Byte), Int32))
            Me.GridView1.Appearance.FilterPanel.BackColor2 = System.Drawing.Color.White
            Me.GridView1.Appearance.FilterPanel.ForeColor = System.Drawing.Color.Black
            Me.GridView1.Appearance.FilterPanel.Options.UseBackColor = True
            Me.GridView1.Appearance.FilterPanel.Options.UseForeColor = True
            Me.GridView1.Appearance.FixedLine.BackColor = System.Drawing.Color.FromArgb(CType(CType(59, Byte), Int32), CType(CType(133, Byte), Int32), CType(CType(195, Byte), Int32))
            Me.GridView1.Appearance.FixedLine.Options.UseBackColor = True
            Me.GridView1.Appearance.FocusedCell.BackColor = System.Drawing.Color.White
            Me.GridView1.Appearance.FocusedCell.ForeColor = System.Drawing.Color.Black
            Me.GridView1.Appearance.FocusedCell.Options.UseBackColor = True
            Me.GridView1.Appearance.FocusedCell.Options.UseForeColor = True
            Me.GridView1.Appearance.FocusedRow.BackColor = System.Drawing.Color.FromArgb(CType(CType(38, Byte), Int32), CType(CType(109, Byte), Int32), CType(CType(189, Byte), Int32))
            Me.GridView1.Appearance.FocusedRow.BorderColor = System.Drawing.Color.FromArgb(CType(CType(59, Byte), Int32), CType(CType(139, Byte), Int32), CType(CType(206, Byte), Int32))
            Me.GridView1.Appearance.FocusedRow.ForeColor = System.Drawing.Color.White
            Me.GridView1.Appearance.FocusedRow.Options.UseBackColor = True
            Me.GridView1.Appearance.FocusedRow.Options.UseBorderColor = True
            Me.GridView1.Appearance.FocusedRow.Options.UseForeColor = True
            Me.GridView1.Appearance.FooterPanel.BackColor = System.Drawing.Color.FromArgb(CType(CType(104, Byte), Int32), CType(CType(184, Byte), Int32), CType(CType(251, Byte), Int32))
            Me.GridView1.Appearance.FooterPanel.BorderColor = System.Drawing.Color.FromArgb(CType(CType(104, Byte), Int32), CType(CType(184, Byte), Int32), CType(CType(251, Byte), Int32))
            Me.GridView1.Appearance.FooterPanel.ForeColor = System.Drawing.Color.Black
            Me.GridView1.Appearance.FooterPanel.Options.UseBackColor = True
            Me.GridView1.Appearance.FooterPanel.Options.UseBorderColor = True
            Me.GridView1.Appearance.FooterPanel.Options.UseForeColor = True
            Me.GridView1.Appearance.GroupButton.BackColor = System.Drawing.Color.FromArgb(CType(CType(104, Byte), Int32), CType(CType(184, Byte), Int32), CType(CType(251, Byte), Int32))
            Me.GridView1.Appearance.GroupButton.BorderColor = System.Drawing.Color.FromArgb(CType(CType(104, Byte), Int32), CType(CType(184, Byte), Int32), CType(CType(251, Byte), Int32))
            Me.GridView1.Appearance.GroupButton.Options.UseBackColor = True
            Me.GridView1.Appearance.GroupButton.Options.UseBorderColor = True
            Me.GridView1.Appearance.GroupFooter.BackColor = System.Drawing.Color.FromArgb(CType(CType(170, Byte), Int32), CType(CType(216, Byte), Int32), CType(CType(254, Byte), Int32))
            Me.GridView1.Appearance.GroupFooter.BorderColor = System.Drawing.Color.FromArgb(CType(CType(170, Byte), Int32), CType(CType(216, Byte), Int32), CType(CType(254, Byte), Int32))
            Me.GridView1.Appearance.GroupFooter.ForeColor = System.Drawing.Color.Black
            Me.GridView1.Appearance.GroupFooter.Options.UseBackColor = True
            Me.GridView1.Appearance.GroupFooter.Options.UseBorderColor = True
            Me.GridView1.Appearance.GroupFooter.Options.UseForeColor = True
            Me.GridView1.Appearance.GroupPanel.BackColor = System.Drawing.Color.FromArgb(CType(CType(236, Byte), Int32), CType(CType(246, Byte), Int32), CType(CType(255, Byte), Int32))
            Me.GridView1.Appearance.GroupPanel.BackColor2 = System.Drawing.Color.White
            Me.GridView1.Appearance.GroupPanel.ForeColor = System.Drawing.Color.Black
            Me.GridView1.Appearance.GroupPanel.Options.UseBackColor = True
            Me.GridView1.Appearance.GroupPanel.Options.UseForeColor = True
            Me.GridView1.Appearance.GroupRow.BackColor = System.Drawing.Color.FromArgb(CType(CType(170, Byte), Int32), CType(CType(216, Byte), Int32), CType(CType(254, Byte), Int32))
            Me.GridView1.Appearance.GroupRow.BorderColor = System.Drawing.Color.FromArgb(CType(CType(170, Byte), Int32), CType(CType(216, Byte), Int32), CType(CType(254, Byte), Int32))
            Me.GridView1.Appearance.GroupRow.ForeColor = System.Drawing.Color.Black
            Me.GridView1.Appearance.GroupRow.Options.UseBackColor = True
            Me.GridView1.Appearance.GroupRow.Options.UseBorderColor = True
            Me.GridView1.Appearance.GroupRow.Options.UseForeColor = True
            Me.GridView1.Appearance.HeaderPanel.BackColor = System.Drawing.Color.FromArgb(CType(CType(139, Byte), Int32), CType(CType(201, Byte), Int32), CType(CType(254, Byte), Int32))
            Me.GridView1.Appearance.HeaderPanel.BorderColor = System.Drawing.Color.FromArgb(CType(CType(139, Byte), Int32), CType(CType(201, Byte), Int32), CType(CType(254, Byte), Int32))
            Me.GridView1.Appearance.HeaderPanel.ForeColor = System.Drawing.Color.Black
            Me.GridView1.Appearance.HeaderPanel.Options.UseBackColor = True
            Me.GridView1.Appearance.HeaderPanel.Options.UseBorderColor = True
            Me.GridView1.Appearance.HeaderPanel.Options.UseForeColor = True
            Me.GridView1.Appearance.HideSelectionRow.BackColor = System.Drawing.Color.FromArgb(CType(CType(105, Byte), Int32), CType(CType(170, Byte), Int32), CType(CType(225, Byte), Int32))
            Me.GridView1.Appearance.HideSelectionRow.BorderColor = System.Drawing.Color.FromArgb(CType(CType(83, Byte), Int32), CType(CType(155, Byte), Int32), CType(CType(215, Byte), Int32))
            Me.GridView1.Appearance.HideSelectionRow.ForeColor = System.Drawing.Color.FromArgb(CType(CType(236, Byte), Int32), CType(CType(246, Byte), Int32), CType(CType(255, Byte), Int32))
            Me.GridView1.Appearance.HideSelectionRow.Options.UseBackColor = True
            Me.GridView1.Appearance.HideSelectionRow.Options.UseBorderColor = True
            Me.GridView1.Appearance.HideSelectionRow.Options.UseForeColor = True
            Me.GridView1.Appearance.HorzLine.BackColor = System.Drawing.Color.FromArgb(CType(CType(104, Byte), Int32), CType(CType(184, Byte), Int32), CType(CType(251, Byte), Int32))
            Me.GridView1.Appearance.HorzLine.Options.UseBackColor = True
            Me.GridView1.Appearance.OddRow.BackColor = System.Drawing.Color.FromArgb(CType(CType(236, Byte), Int32), CType(CType(246, Byte), Int32), CType(CType(255, Byte), Int32))
            Me.GridView1.Appearance.OddRow.BorderColor = System.Drawing.Color.FromArgb(CType(CType(236, Byte), Int32), CType(CType(246, Byte), Int32), CType(CType(255, Byte), Int32))
            Me.GridView1.Appearance.OddRow.ForeColor = System.Drawing.Color.Black
            Me.GridView1.Appearance.OddRow.Options.UseBackColor = True
            Me.GridView1.Appearance.OddRow.Options.UseBorderColor = True
            Me.GridView1.Appearance.OddRow.Options.UseForeColor = True
            Me.GridView1.Appearance.Preview.Font = New System.Drawing.Font("Verdana", 7.5!)
            Me.GridView1.Appearance.Preview.ForeColor = System.Drawing.Color.FromArgb(CType(CType(83, Byte), Int32), CType(CType(155, Byte), Int32), CType(CType(215, Byte), Int32))
            Me.GridView1.Appearance.Preview.Options.UseFont = True
            Me.GridView1.Appearance.Preview.Options.UseForeColor = True
            Me.GridView1.Appearance.Row.BackColor = System.Drawing.Color.FromArgb(CType(CType(247, Byte), Int32), CType(CType(251, Byte), Int32), CType(CType(255, Byte), Int32))
            Me.GridView1.Appearance.Row.ForeColor = System.Drawing.Color.Black
            Me.GridView1.Appearance.Row.Options.UseBackColor = True
            Me.GridView1.Appearance.Row.Options.UseForeColor = True
            Me.GridView1.Appearance.RowSeparator.BackColor = System.Drawing.Color.FromArgb(CType(CType(236, Byte), Int32), CType(CType(246, Byte), Int32), CType(CType(255, Byte), Int32))
            Me.GridView1.Appearance.RowSeparator.BackColor2 = System.Drawing.Color.White
            Me.GridView1.Appearance.RowSeparator.Options.UseBackColor = True
            Me.GridView1.Appearance.SelectedRow.BackColor = System.Drawing.Color.FromArgb(CType(CType(83, Byte), Int32), CType(CType(155, Byte), Int32), CType(CType(215, Byte), Int32))
            Me.GridView1.Appearance.SelectedRow.ForeColor = System.Drawing.Color.White
            Me.GridView1.Appearance.SelectedRow.Options.UseBackColor = True
            Me.GridView1.Appearance.SelectedRow.Options.UseForeColor = True
            Me.GridView1.Appearance.TopNewRow.BackColor = System.Drawing.Color.White
            Me.GridView1.Appearance.TopNewRow.Options.UseBackColor = True
            Me.GridView1.Appearance.VertLine.BackColor = System.Drawing.Color.FromArgb(CType(CType(104, Byte), Int32), CType(CType(184, Byte), Int32), CType(CType(251, Byte), Int32))
            Me.GridView1.Appearance.VertLine.Options.UseBackColor = True
            Me.GridView1.Columns.AddRange(New DevExpress.XtraGrid.Columns.GridColumn() {Me.col_old_creditor, Me.col_account_number, Me.col_new_creditor})
            Me.GridView1.GridControl = Me.GridControl1
            Me.GridView1.Name = "GridView1"
            Me.GridView1.OptionsBehavior.Editable = False
            Me.GridView1.OptionsView.ShowGroupPanel = False
            Me.GridView1.OptionsView.ShowIndicator = False
            Me.GridView1.ShowButtonMode = DevExpress.XtraGrid.Views.Base.ShowButtonModeEnum.ShowForFocusedRow
            Me.GridView1.SortInfo.AddRange(New DevExpress.XtraGrid.Columns.GridColumnSortInfo() {New DevExpress.XtraGrid.Columns.GridColumnSortInfo(Me.col_old_creditor, DevExpress.Data.ColumnSortOrder.Ascending)})
            '
            'col_old_creditor
            '
            Me.col_old_creditor.Caption = "Creditor"
            Me.col_old_creditor.CustomizationCaption = "Old Creditor ID"
            Me.col_old_creditor.FieldName = "old_creditor"
            Me.col_old_creditor.Name = "col_old_creditor"
            Me.col_old_creditor.SortMode = DevExpress.XtraGrid.ColumnSortMode.DisplayText
            Me.col_old_creditor.ToolTip = "Old creditor label. This is the existing creditor for the debts."
            Me.col_old_creditor.UnboundType = DevExpress.Data.UnboundColumnType.[String]
            Me.col_old_creditor.Visible = True
            Me.col_old_creditor.VisibleIndex = 0
            Me.col_old_creditor.Width = 95
            '
            'col_account_number
            '
            Me.col_account_number.Caption = "Account Number"
            Me.col_account_number.CustomizationCaption = "Account Number Mask"
            Me.col_account_number.FieldName = "account_number"
            Me.col_account_number.Name = "col_account_number"
            Me.col_account_number.ToolTip = "This is the mask to find the accounts that are to be reassigned."
            Me.col_account_number.UnboundType = DevExpress.Data.UnboundColumnType.[String]
            Me.col_account_number.Visible = True
            Me.col_account_number.VisibleIndex = 1
            Me.col_account_number.Width = 150
            '
            'col_new_creditor
            '
            Me.col_new_creditor.Caption = "New Creditor"
            Me.col_new_creditor.CustomizationCaption = "New Creditor ID"
            Me.col_new_creditor.FieldName = "new_creditor"
            Me.col_new_creditor.Name = "col_new_creditor"
            Me.col_new_creditor.ToolTip = "New creditor. This is the creditor to which the debts are to be assigned."
            Me.col_new_creditor.UnboundType = DevExpress.Data.UnboundColumnType.[String]
            Me.col_new_creditor.Visible = True
            Me.col_new_creditor.VisibleIndex = 2
            Me.col_new_creditor.Width = 151
            '
            'Form1
            '
            Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
            Me.ClientSize = New System.Drawing.Size(504, 266)
            Me.Controls.Add(Me.PanelControl1)
            Me.Controls.Add(Me.barDockControlLeft)
            Me.Controls.Add(Me.barDockControlRight)
            Me.Controls.Add(Me.barDockControlBottom)
            Me.Controls.Add(Me.barDockControlTop)
            Me.Name = "Form1"
            Me.ToolTipController1.SetSuperTip(Me, Nothing)
            Me.Text = "Perform a reassignment based upon the account number mask"
            CType(Me.DxErrorProvider1, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.barManager1, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.PanelControl1, System.ComponentModel.ISupportInitialize).EndInit()
            Me.PanelControl1.ResumeLayout(False)
            CType(Me.generate_proposals.Properties, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.GridControl1, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.GridView1, System.ComponentModel.ISupportInitialize).EndInit()
            Me.ResumeLayout(False)

        End Sub

#End Region

        Private Sub Form1_Load(ByVal sender As Object, ByVal e As System.EventArgs)

            ' Add the tables to the dataset
            ap.ds.Tables.Add(selection_list_table)
            ap.ds.Tables.Add(debt_list_table)

            ' Create an edit view of the change list table for additions and bind it to the grid
            vue_change_list = ap.ds.Tables("change_list").DefaultView
            GridControl1.DataSource = vue_change_list

            ' Enable/Disable the OK button
            EnableOK()
        End Sub

        Private Function selection_list_table() As System.Data.DataTable
            ' Create the list of debts that are to be reassigned
            Dim tbl As New System.Data.DataTable
            With tbl

                ' Define the columns
                With .Columns
                    With .Add("id", GetType(System.Int32), String.Empty)
                        .Caption = "ID"
                    End With

                    With .Add("count", GetType(System.Int32), String.Empty)
                        .Caption = "# of debts"
                    End With

                    With .Add("old_creditor", GetType(String), String.Empty)
                        .Caption = "Old Creditor"
                        .MaxLength = 10
                    End With

                    With .Add("old_creditor_name", GetType(String), String.Empty)
                        .Caption = "Old Creditor Name"
                        .MaxLength = 50
                    End With

                    With .Add("account_number", GetType(String), String.Empty)
                        .Caption = "Account Number"
                        .MaxLength = 128
                    End With

                    With .Add("new_creditor", GetType(String), String.Empty)
                        .Caption = "New Creditor"
                        .MaxLength = 10
                    End With

                    With .Add("new_creditor_name", GetType(String), String.Empty)
                        .Caption = "New Creditor Name"
                        .MaxLength = 50
                    End With
                End With

                ' Set the information for the table
                .PrimaryKey = New System.Data.DataColumn() {.Columns(0)}    ' ID
                .TableName = "change_list"
            End With

            Return tbl
        End Function

        Private Function debt_list_table() As System.Data.DataTable

            ' Create the list of debts that are to be reassigned
            Dim tbl As New System.Data.DataTable
            With tbl

                ' Define the columns
                With .Columns
                    With .Add("selection", GetType(System.Int32), String.Empty)
                        .Caption = "Selection"
                    End With

                    With .Add("client_creditor", GetType(System.Int32), String.Empty)
                        .Caption = "Debt Record"
                    End With

                    With .Add("old_creditor", GetType(String), String.Empty)
                        .Caption = "Old Creditor"
                        .MaxLength = 10
                    End With

                    With .Add("old_creditor_name", GetType(String), String.Empty)
                        .Caption = "Old Creditor Name"
                        .MaxLength = 50
                    End With

                    With .Add("account_number", GetType(String), String.Empty)
                        .Caption = "Account Number"
                        .MaxLength = 128
                    End With

                    With .Add("client", GetType(System.Int32), String.Empty)
                        .Caption = "Client ID"
                    End With

                    With .Add("client_name", GetType(String), String.Empty)
                        .Caption = "Client Name"
                        .MaxLength = 128
                    End With

                    With .Add("new_creditor", GetType(String), String.Empty)
                        .Caption = "New Creditor"
                        .MaxLength = 10
                    End With

                    With .Add("new_creditor_name", GetType(String), String.Empty)
                        .Caption = "New Creditor Name"
                        .MaxLength = 50
                    End With

                    With .Add("id", GetType(System.Int32), String.Empty)
                        .AllowDBNull = False
                        .AutoIncrement = True
                        .AutoIncrementSeed = 1
                        .AutoIncrementStep = 1
                        .Caption = "ID"
                    End With
                End With

                ' Set the information for the table
                .PrimaryKey = New System.Data.DataColumn() {.Columns("id")}
                .TableName = "debt_list"
            End With

            Return tbl
        End Function

        Private Sub Button_Cancel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
            Close()
        End Sub

        Private Sub Button_OK_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
            If DebtPlus.Data.Forms.MessageBox.Show("Warning: Are you really sure that you have checked this list?" + Environment.NewLine + "It will be extremely difficult to reverse these changes if they are incorrect." + Environment.NewLine + "This is your last warning. If you press YES, the changes will be committed to the database.", "Are you sure?", MessageBoxButtons.YesNo, MessageBoxIcon.Question) = DialogResult.Yes Then
                Dim answer As Boolean = False

                ' Do the reassignment operation here.
                Dim dlg As New DevExpress.Utils.WaitDialogForm("Processing debt reassignment", "Reassigning debts. Please wait")
                dlg.Show()
                answer = ReassignDebts(ap.ds.Tables("debt_list"))
                dlg.Close()
                dlg.Dispose()

                ' Tell the user that we are complete if the reassignment was successful.
                If answer Then
                    Button_OK.Enabled = False
                    If DebtPlus.Data.Forms.MessageBox.Show("The operation completed successfully." + Environment.NewLine + Environment.NewLine + "Do you wish a report of the reassigned debts? (Once this program is terminated, you can no longer get the report.)", "Completed", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button1) = Windows.Forms.DialogResult.Yes Then
                        AddHandler bt.RunWorkerCompleted, AddressOf bt_RunWorkerCompleted
                        PrintReport()
                        System.Windows.Forms.Cursor.Current = Cursors.WaitCursor
                    Else
                        Close()
                    End If
                End If
            End If
        End Sub

        Private Function ReassignDebts(ByVal tbl As System.Data.DataTable) As Boolean
            Dim answer As Boolean = False
            Dim current_cursor As System.Windows.Forms.Cursor = System.Windows.Forms.Cursor.Current
            Dim cn As SqlClient.SqlConnection = New SqlConnection(DebtPlus.LINQ.SQLInfoClass.getDefault().ConnectionString)
            Dim txn As SqlClient.SqlTransaction = Nothing

            Try
                System.Windows.Forms.Cursor.Current = System.Windows.Forms.Cursors.WaitCursor
                cn.Open()
                txn = cn.BeginTransaction

                ' Process the items in the translation table to reassign the debts
                For Each row As System.Data.DataRow In tbl.Rows
                    answer = ReassignSingleDebt(cn, txn, Convert.ToInt32(row("client_creditor")), Convert.ToString(row("new_creditor")), generate_proposals.Checked)
                    If Not answer Then Exit For
                Next

                ' All is complete. Commit the changes to the database.
                If answer Then
                    txn.Commit()
                    txn = Nothing
                End If

            Catch ex As SqlClient.SqlException
                DebtPlus.UI.Common.ErrorHandling.HandleErrors(ex, "Error reassigning the debts")

            Finally
                If txn IsNot Nothing Then
                    Try
                        txn.Rollback()
                    Catch exRollback As System.Exception
                    End Try
                    txn.Dispose()
                    txn = Nothing
                End If

                If cn IsNot Nothing Then cn.Dispose()
                System.Windows.Forms.Cursor.Current = current_cursor
            End Try

            Return answer
        End Function

        Private Function ReassignSingleDebt(ByVal cn As SqlClient.SqlConnection, ByVal txn As SqlClient.SqlTransaction, ByVal client_creditor As System.Int32, ByVal new_creditor As String, ByVal generate_proposals As Boolean) As Boolean
            Dim NewRecord As System.Int32 = -1

            Using cmd As SqlClient.SqlCommand = New SqlCommand
                With cmd
                    .Connection = cn
                    .Transaction = txn
                    .CommandText = "xpr_sell_debt"
                    .CommandType = CommandType.StoredProcedure

                    SqlClient.SqlCommandBuilder.DeriveParameters(cmd)
                    .Parameters(1).Value = client_creditor
                    .Parameters(2).Value = new_creditor
                    .Parameters(3).Value = If(generate_proposals, 1, 0)

                    .ExecuteNonQuery()

                    ' Find the new debt record for this client
                    NewRecord = Convert.ToInt32(.Parameters(0).Value)
                End With
            End Using

            Return NewRecord > 0
        End Function

        Private Sub EnableOK()
            Dim answer As Boolean = False
            Do
                If ap.ds.Tables("change_list").Rows.Count = 0 Then Exit Do
                answer = True
                Exit Do
            Loop

            Button_OK.Enabled = answer
        End Sub

        Private Sub AddRow()
            Dim drv As System.Data.DataRowView = vue_change_list.AddNew
            drv("id") = -1
            If EditRow(drv) Then
                drv.EndEdit()
            Else
                drv.CancelEdit()
            End If

            EnableOK()
        End Sub

        Private Function EditRow(ByVal drv As System.Data.DataRowView) As Boolean
            Dim answer As Boolean = False

            ' Create the edit form and allow it to be changed
            With New ItemEditForm(drv)
                Dim DlgAnswer As System.Windows.Forms.DialogResult = .ShowDialog()
                If DlgAnswer = DialogResult.OK Then answer = True
                .Dispose()
            End With

            ' If the value is an edit then remove all of the old items from the list
            If answer Then
                Static NewID As System.Int32 = 0
                NewID += 1

                answer = ReadDebtList(drv, NewID)
                If answer Then
                    Dim OldID As System.Int32 = Convert.ToInt32(drv("id"))
                    If OldID > 0 Then
                        Dim vue As New System.Data.DataView(ap.ds.Tables("debt_list"), String.Format("[selection]={0:f0}", OldID), String.Empty, DataViewRowState.CurrentRows)
                        For Item As System.Int32 = vue.Count - 1 To 0 Step -1
                            vue(Item).Delete()
                        Next
                        ap.ds.Tables("debt_list").AcceptChanges()
                    End If
                    drv("id") = NewID
                End If
            End If

            Return answer
        End Function

        Private Function ReadDebtList(ByVal drv As System.Data.DataRowView, ByVal ID As System.Int32) As Boolean
            Dim answer As Boolean = False

            ' Build the list of debts that are to be reassigned with the OLDID as the key to the origination string
            Dim cmd As SqlClient.SqlCommand = New SqlCommand
            With cmd
                .Connection = New SqlConnection(DebtPlus.LINQ.SQLInfoClass.getDefault().ConnectionString)
                .CommandTimeout = System.Math.Max(DebtPlus.LINQ.SQLInfoClass.getDefault().CommandTimeout, 600)

                Dim sb As New System.Text.StringBuilder

                sb.Append("SELECT ")
                sb.AppendFormat("{0:f0} AS selection, ", ID)
                sb.Append("cc.client_creditor AS client_creditor, ")

                sb.Append("cc.creditor AS old_creditor, ")
                sb.Append("cro.creditor_name as old_creditor_name, ")

                sb.Append("cc.account_number as account_number, ")

                sb.Append("cc.client as client, ")
                sb.Append("isnull(dbo.format_normal_name(pn.prefix, pn.first, pn.middle, pn.last, pn.suffix),'') as client_name, ")

                sb.AppendFormat("'{0}' as new_creditor, ", Convert.ToString(drv("new_creditor")).Replace("'", "''"))
                sb.Append("crn.creditor_name as new_creditor_name, ")
                sb.Append("bal.orig_balance + bal.orig_balance_adjustment + bal.total_interest - bal.total_payments as balance ")

                sb.Append("FROM client_creditor cc WITH (NOLOCK) ")
                sb.Append("INNER JOIN clients c WITH (NOLOCK) ON cc.client = c.client ")
                sb.Append("INNER JOIN client_creditor_balances bal WITH (NOLOCK) ON cc.client_creditor_balance = bal.client_creditor_balance ")
                sb.Append("LEFT OUTER JOIN people p WITH (NOLOCK) ON c.client = p.client AND 1 = p.relation ")
                sb.Append("LEFT OUTER JOIN names pn WITH (NOLOCK) ON p.NameID = pn.Name ")
                sb.Append("LEFT OUTER JOIN creditors cro WITH (NOLOCK) ON cc.creditor = cro.creditor ")
                sb.AppendFormat("LEFT OUTER JOIN creditors crn WITH (NOLOCK) ON '{0}' = crn.creditor ", Convert.ToString(drv("new_creditor")).Replace("'", "''"))

                sb.Append("WHERE c.active_status NOT IN ('CRE','WKS','I') ")
                sb.Append("AND isnull(bal.orig_balance,0) + isnull(bal.orig_balance_adjustment,0) + isnull(bal.total_interest,0) > isnull(bal.total_payments,0) ")
                sb.AppendFormat("AND cc.creditor = '{0}' ", Convert.ToString(drv("old_creditor")).Replace("'", "''"))
                sb.Append("AND isnull(cc.reassigned_debt,0) = 0")

                ' If we are not accepting all account numbers then filter it by the account number.
                Dim Mask As String = Convert.ToString(drv("account_number")).Replace("'", "''")
                If Mask <> "%" Then
                    sb.AppendFormat(" AND cc.account_number LIKE '{0}'", Mask)
                End If

                sb.Append(";")

                .CommandText = sb.ToString()
            End With

            Dim current_cursor As System.Windows.Forms.Cursor = System.Windows.Forms.Cursor.Current
            Try

                ' Tell the user that we are reading the debt list. This may take a moment.
                Dim dlg As New DevExpress.Utils.WaitDialogForm("Reading debt list")
                dlg.Show()

                System.Windows.Forms.Cursor.Current = System.Windows.Forms.Cursors.WaitCursor
                Dim da As New SqlClient.SqlDataAdapter(cmd)
                drv("count") = da.Fill(ap.ds, "debt_list")

                ' Close and dispose of the alert window
                dlg.Close()
                dlg.Dispose()

                answer = True
                If Convert.ToInt32(drv("count")) <= 0 Then
                    If DebtPlus.Data.Forms.MessageBox.Show("This selection does not select any valid debts. Do you still wish to include it?", "Sorry, but there are no debts", MessageBoxButtons.YesNo, MessageBoxIcon.Question) = DialogResult.No Then answer = False
                End If

            Catch ex As SqlClient.SqlException
                DebtPlus.UI.Common.ErrorHandling.HandleErrors(ex, "Error reading list of debts")

            Finally
                System.Windows.Forms.Cursor.Current = current_cursor
            End Try

            Return answer
        End Function

        Private Sub MenuItem_Add_Click(ByVal sender As Object, ByVal e As System.EventArgs)
            AddRow()
        End Sub

        Private Sub MenuItem_Change_Click(ByVal sender As Object, ByVal e As System.EventArgs)
            Dim drv As System.Data.DataRowView = CType(GridView1.GetRow(GridView1.FocusedRowHandle), System.Data.DataRowView)
            If drv IsNot Nothing Then
                If EditRow(drv) Then
                    drv.EndEdit()
                    ap.ds.Tables("change_list").AcceptChanges()
                Else
                    drv.CancelEdit()
                End If

                EnableOK()
            End If
        End Sub

        Private Sub MenuItem_Delete_Click(ByVal sender As Object, ByVal e As System.EventArgs)
            Dim drv As System.Data.DataRowView = CType(GridView1.GetRow(GridView1.FocusedRowHandle), System.Data.DataRowView)

            If drv IsNot Nothing Then
                Dim id As System.Int32 = Convert.ToInt32(drv("id"))
                If id > 0 Then
                    drv.Delete()
                    ap.ds.Tables("change_list").AcceptChanges()

                    Dim vue As New System.Data.DataView(ap.ds.Tables("debt_list"), String.Format("[selection]={0:f0}", id), String.Empty, DataViewRowState.CurrentRows)
                    For Item As System.Int32 = vue.Count - 1 To 0 Step -1
                        vue(Item).Delete()
                    Next
                    ap.ds.Tables("debt_list").AcceptChanges()
                End If

                EnableOK()
            End If
        End Sub

        Private Sub MenuItem_Print_Click(ByVal sender As Object, ByVal e As System.EventArgs)
            PrintReport()
        End Sub

        Private Sub PrintReport()
            With bt
                Dim tbl As System.Data.DataTable = ap.ds.Tables("debt_list")
                .RunWorkerAsync(tbl)
            End With
        End Sub

        Private Sub bt_RunWorkerCompleted(ByVal sender As Object, ByVal e As System.ComponentModel.RunWorkerCompletedEventArgs)
            Close()
        End Sub

        Private Sub bt_DoWork(ByVal sender As Object, ByVal e As System.ComponentModel.DoWorkEventArgs)
            Dim tbl As System.Data.DataTable = CType(e.Argument, System.Data.DataTable)
            Using rpt As New ReassignedDebtsReport(ap)
                With rpt
                    .DataSource = tbl
                    .DisplayPreviewDialog()
                End With
            End Using
        End Sub

        Private Sub ContextMenu1_Popup(ByVal sender As Object, ByVal e As System.EventArgs)

            ' Enable the edit/delete menu items
            If GridView1.FocusedRowHandle >= 0 Then
                MenuItem_Change.Enabled = True
                MenuItem_Delete.Enabled = True
            Else
                MenuItem_Change.Enabled = False
                MenuItem_Delete.Enabled = False
            End If

            ' Enable the print function if there is some data to print
            MenuItem_Print.Enabled = (ap.ds.Tables("debt_list").Rows.Count > 0)
        End Sub

        ''' <summary>
        ''' Process a DOUBLE CLICK event on the row
        ''' </summary>
        Private Sub GridView1_DoubleClick(ByVal sender As System.Object, ByVal e As System.EventArgs)

            ' Find the row which was clicked
            Dim gv As DevExpress.XtraGrid.Views.Grid.GridView = CType(sender, DevExpress.XtraGrid.Views.Grid.GridView)
            Dim ctl As DevExpress.XtraGrid.GridControl = gv.GridControl
            Dim hi As DevExpress.XtraGrid.Views.Grid.ViewInfo.GridHitInfo = gv.CalcHitInfo((ctl.PointToClient(Windows.Forms.Control.MousePosition)))
            Dim RowHandle As System.Int32 = hi.RowHandle

            ' If there is a row then change the control
            If RowHandle >= 0 Then
                gv.FocusedRowHandle = RowHandle
                Dim drv As System.Data.DataRowView = CType(GridView1.GetRow(RowHandle), System.Data.DataRowView)
                If drv IsNot Nothing Then
                    If EditRow(drv) Then
                        drv.EndEdit()
                        ap.ds.Tables("change_list").AcceptChanges()
                    Else
                        drv.CancelEdit()
                    End If

                    EnableOK()
                End If
            End If
        End Sub

        Private Sub BarButtonItem_File_Print_ItemClick(ByVal sender As System.Object, ByVal e As DevExpress.XtraBars.ItemClickEventArgs)
            PrintReport()
        End Sub

        Private Sub BarButtonItem_File_Exit_ItemClick(ByVal sender As System.Object, ByVal e As DevExpress.XtraBars.ItemClickEventArgs)
            Close()
        End Sub
    End Class
End Namespace
