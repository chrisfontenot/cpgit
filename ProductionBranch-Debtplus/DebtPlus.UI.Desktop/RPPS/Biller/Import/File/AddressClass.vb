#Region "Copyright 2000-2012 DebtPlus, L.L.C."
'{*******************************************************************}
'{                                                                   }
'{       DebtPlus Debt Management System                             }
'{                                                                   }
'{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
'{       ALL RIGHTS RESERVED                                         }
'{                                                                   }
'{   The entire contents of this file is protected by U.S. and       }
'{   International Copyright Laws. Unauthorized reproduction,        }
'{   reverse-engineering, and distribution of all or any portion of  }
'{   the code contained in this file is strictly prohibited and may  }
'{   result in severe civil and criminal penalties and will be       }
'{   prosecuted to the maximum extent possible under the law.        }
'{                                                                   }
'{   RESTRICTIONS                                                    }
'{                                                                   }
'{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
'{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
'{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
'{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
'{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
'{                                                                   }
'{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
'{   ADDITIONAL RESTRICTIONS.                                        }
'{                                                                   }
'{*******************************************************************}
#End Region

Option Strict Off
Option Explicit On

Imports System.Data.SqlClient

Namespace RPPS.Biller.Import.File
    Friend Class AddressClass
        Implements IDisposable

        Public AddressType As String = "Standard"
        Public effdate As String = String.Empty
        Private Key As Object = System.DBNull.Value
        Private privateaddr1 As String = String.Empty
        Private privateaddr2 As String = String.Empty
        Private privatecity As String = String.Empty
        Private privatestate As String = String.Empty
        Private privatezipcode As String = String.Empty
        Private privatecountry As String = String.Empty

        Public Property zipcode() As String
            Get
                Return privatezipcode
            End Get
            Set(ByVal Value As String)
                privatezipcode = Value.Trim()
                If privatezipcode.Length > 14 Then privatezipcode = privatezipcode.Substring(0, 14)
            End Set
        End Property

        Public Property state() As String
            Get
                Return privatestate
            End Get
            Set(ByVal Value As String)
                privatestate = Value.Trim()

                Dim rx As New System.Text.RegularExpressions.Regex("^[a-zA-Z]{2}$", System.Text.RegularExpressions.RegexOptions.IgnoreCase Or System.Text.RegularExpressions.RegexOptions.Singleline)
                If Not rx.IsMatch(state) Then
                    privatestate = String.Empty
                End If
            End Set
        End Property

        Public Property addr1() As String
            Get
                Return privateaddr1
            End Get
            Set(ByVal Value As String)
                privateaddr1 = Value.Trim()
                If privateaddr1.Length > 80 Then privateaddr1 = privateaddr1.Substring(0, 80)
            End Set
        End Property

        Public Property addr2() As String
            Get
                Return privateaddr2
            End Get
            Set(ByVal Value As String)
                privateaddr2 = Value.Trim()
                If privateaddr2.Length > 80 Then privateaddr2 = privateaddr2.Substring(0, 80)
            End Set
        End Property

        Public Property city() As String
            Get
                Return privatecity
            End Get
            Set(ByVal Value As String)
                privatecity = Value.Trim()
                If privatecity.Length > 60 Then privatecity = privatecity.Substring(0, 60)
            End Set
        End Property

        Public Property country() As String
            Get
                Return privatecountry
            End Get
            Set(ByVal Value As String)
                privatecountry = Value.Trim()
                If privatecountry.Length > 10 Then privatecountry = privatecountry.Substring(0, 10)
            End Set
        End Property

        Friend Sub Parse(ByRef xmlInput As RPPS_XML)

            With xmlInput

                ' Find the key to the biller if there is one.
                If .XMLReader.HasAttributes Then
                    For AttributeID As System.Int32 = 0 To xmlInput.XMLReader.AttributeCount - 1
                        xmlInput.XMLReader.MoveToAttribute(AttributeID)
                        Select Case xmlInput.XMLReader.Name.ToLower()
                            Case "key"
                                xmlInput.XMLReader.ReadAttributeValue()
                                Key = xmlInput.XMLReader.Value
                        End Select
                    Next
                End If

                .XMLReader.MoveToContent()

                ' Process the fields until we are at the end of the biller
                Do While Not .BadFile AndAlso .XMLReader.Read
                    Dim ElementName As String = .XMLReader.Name.ToLower()

                    Select Case .XMLReader.NodeType
                        Case Xml.XmlNodeType.EndElement
                            If ElementName = "address" Then Exit Do

                        Case Xml.XmlNodeType.Element
                            Select Case ElementName
                                Case "address1", "addr1"
                                    .parse_Field(addr1)
                                Case "address2", "addr2"
                                    .parse_Field(addr2)
                                Case "city"
                                    .parse_Field(city)
                                Case "state"
                                    .parse_Field(state)
                                Case "zipcode", "postalcode"
                                    .parse_Field(zipcode)
                                Case "country"
                                    .parse_Field(country)
                                Case "effdate"
                                    .parse_Field(effdate)
                                Case "type"
                                    .parse_Field(AddressType)
                                Case Else
                                    .SkipField()
                            End Select
                    End Select
                Loop
            End With
        End Sub

        Private Function Nullify(ByVal InputString As String) As Object
            Dim answer As Object = System.DBNull.Value
            If Not String.IsNullOrEmpty(InputString) Then answer = InputString
            Return answer
        End Function

        Public Sub WriteInfo(ByVal cn As SqlClient.SqlConnection, ByVal txn As SqlClient.SqlTransaction, ByRef biller_class As BillerClass)

            Using cmd As SqlClient.SqlCommand = New SqlCommand
                With cmd
                    .Connection = cn
                    .Transaction = txn
                    .CommandText = "INSERT INTO rpps_addresses(rpps_biller_id,biller_addr1,biller_addr2,biller_city,biller_state,biller_zipcode,biller_country) VALUES (@rpps_biller_id,@biller_addr1,@biller_addr2,@biller_city,@biller_state,@biller_zipcode,@biller_country)"
                    .CommandType = CommandType.Text

                    .Parameters.Add("@rpps_biller_id", SqlDbType.VarChar, 80).Value = biller_class.billerinfo.blrid
                    .Parameters.Add("@biller_addr1", SqlDbType.VarChar, 80).Value = Nullify(addr1)
                    .Parameters.Add("@biller_addr2", SqlDbType.VarChar, 80).Value = Nullify(addr2)
                    .Parameters.Add("@biller_city", SqlDbType.VarChar, 80).Value = Nullify(city)
                    .Parameters.Add("@biller_state", SqlDbType.VarChar, 10).Value = Nullify(state)
                    .Parameters.Add("@biller_zipcode", SqlDbType.VarChar, 14).Value = Nullify(zipcode)
                    .Parameters.Add("@biller_country", SqlDbType.VarChar, 80).Value = Nullify(country)
                    .ExecuteNonQuery()
                End With
            End Using
        End Sub

        Private disposedValue As Boolean = False        ' To detect redundant calls
        Protected Overridable Sub Dispose(ByVal disposing As Boolean)
            If Not Me.disposedValue Then
                If disposing Then
                    ' free other state (managed objects).
                End If

                ' Release the storage
                privateaddr1 = Nothing
                privateaddr2 = Nothing
                privatecity = Nothing
                privatestate = Nothing
                privatezipcode = Nothing
                country = Nothing
            End If

            Me.disposedValue = True
        End Sub

#Region " IDisposable Support "
        Public Sub Dispose() Implements IDisposable.Dispose
            Dispose(True)
            GC.SuppressFinalize(Me)
        End Sub
#End Region

    End Class
End Namespace
