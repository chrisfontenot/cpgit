Namespace RPPS.Biller.Import.File

    <Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
    Partial Class StatusForm
        Inherits DebtPlus.Data.Forms.DebtPlusForm

        'Form overrides dispose to clean up the component list.
        <System.Diagnostics.DebuggerNonUserCode()> _
        Protected Overrides Sub Dispose(ByVal disposing As Boolean)
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
            MyBase.Dispose(disposing)
        End Sub

        'Required by the Windows Form Designer
        Private components As System.ComponentModel.IContainer

        'NOTE: The following procedure is required by the Windows Form Designer
        'It can be modified using the Windows Form Designer.  
        'Do not modify it using the code editor.
        <System.Diagnostics.DebuggerStepThrough()> _
        Private Sub InitializeComponent()
            Me.components = New System.ComponentModel.Container
            Me.LabelControl1 = New DevExpress.XtraEditors.LabelControl
            Me.LabelControl2 = New DevExpress.XtraEditors.LabelControl
            Me.LabelControl3 = New DevExpress.XtraEditors.LabelControl
            Me.lblBillerName = New DevExpress.XtraEditors.LabelControl
            Me.lblBillerID = New DevExpress.XtraEditors.LabelControl
            Me.lblElapsedTime = New DevExpress.XtraEditors.LabelControl
            Me.SimpleButton_Cancel = New DevExpress.XtraEditors.SimpleButton
            Me.Timer1 = New System.Windows.Forms.Timer(Me.components)
            CType(Me.DxErrorProvider1, System.ComponentModel.ISupportInitialize).BeginInit()
            Me.SuspendLayout()
            '
            'LabelControl1
            '
            Me.LabelControl1.Appearance.Options.UseTextOptions = True
            Me.LabelControl1.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
            Me.LabelControl1.Appearance.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Top
            Me.LabelControl1.Appearance.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap
            Me.LabelControl1.Location = New System.Drawing.Point(13, 13)
            Me.LabelControl1.Name = "LabelControl1"
            Me.LabelControl1.Size = New System.Drawing.Size(66, 13)
            Me.LabelControl1.TabIndex = 0
            Me.LabelControl1.Text = "Elapsed Time:"
            '
            'LabelControl2
            '
            Me.LabelControl2.Appearance.Options.UseTextOptions = True
            Me.LabelControl2.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
            Me.LabelControl2.Appearance.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Top
            Me.LabelControl2.Appearance.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap
            Me.LabelControl2.Location = New System.Drawing.Point(13, 32)
            Me.LabelControl2.Name = "LabelControl2"
            Me.LabelControl2.Size = New System.Drawing.Size(94, 13)
            Me.LabelControl2.TabIndex = 1
            Me.LabelControl2.Text = "Processing Biller ID:"
            '
            'LabelControl3
            '
            Me.LabelControl3.Appearance.Options.UseTextOptions = True
            Me.LabelControl3.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
            Me.LabelControl3.Appearance.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Top
            Me.LabelControl3.Appearance.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap
            Me.LabelControl3.Location = New System.Drawing.Point(13, 51)
            Me.LabelControl3.Name = "LabelControl3"
            Me.LabelControl3.Size = New System.Drawing.Size(56, 13)
            Me.LabelControl3.TabIndex = 2
            Me.LabelControl3.Text = "Biller Name:"
            '
            'lblBillerName
            '
            Me.lblBillerName.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                        Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
            Me.lblBillerName.Appearance.Options.UseTextOptions = True
            Me.lblBillerName.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
            Me.lblBillerName.Appearance.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Top
            Me.lblBillerName.Appearance.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap
            Me.lblBillerName.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None
            Me.lblBillerName.Location = New System.Drawing.Point(113, 51)
            Me.lblBillerName.Name = "lblBillerName"
            Me.lblBillerName.Size = New System.Drawing.Size(167, 55)
            Me.lblBillerName.TabIndex = 5
            '
            'lblBillerID
            '
            Me.lblBillerID.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                        Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
            Me.lblBillerID.Appearance.Options.UseTextOptions = True
            Me.lblBillerID.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
            Me.lblBillerID.Appearance.TextOptions.Trimming = DevExpress.Utils.Trimming.EllipsisCharacter
            Me.lblBillerID.Appearance.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Top
            Me.lblBillerID.Appearance.TextOptions.WordWrap = DevExpress.Utils.WordWrap.NoWrap
            Me.lblBillerID.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None
            Me.lblBillerID.Location = New System.Drawing.Point(113, 32)
            Me.lblBillerID.Name = "lblBillerID"
            Me.lblBillerID.Size = New System.Drawing.Size(167, 13)
            Me.lblBillerID.TabIndex = 4
            '
            'lblElapsedTime
            '
            Me.lblElapsedTime.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                        Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
            Me.lblElapsedTime.Appearance.Options.UseTextOptions = True
            Me.lblElapsedTime.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
            Me.lblElapsedTime.Appearance.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Top
            Me.lblElapsedTime.Appearance.TextOptions.WordWrap = DevExpress.Utils.WordWrap.NoWrap
            Me.lblElapsedTime.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None
            Me.lblElapsedTime.Location = New System.Drawing.Point(113, 13)
            Me.lblElapsedTime.Name = "lblElapsedTime"
            Me.lblElapsedTime.Size = New System.Drawing.Size(167, 13)
            Me.lblElapsedTime.TabIndex = 3
            '
            'SimpleButton_Cancel
            '
            Me.SimpleButton_Cancel.Anchor = System.Windows.Forms.AnchorStyles.Bottom
            Me.SimpleButton_Cancel.Location = New System.Drawing.Point(109, 112)
            Me.SimpleButton_Cancel.Name = "SimpleButton_Cancel"
            Me.SimpleButton_Cancel.Size = New System.Drawing.Size(75, 23)
            Me.SimpleButton_Cancel.TabIndex = 6
            Me.SimpleButton_Cancel.Text = "&Cancel"
            '
            'Timer1
            '
            '
            'StatusForm
            '
            Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
            Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
            Me.ClientSize = New System.Drawing.Size(292, 148)
            Me.Controls.Add(Me.SimpleButton_Cancel)
            Me.Controls.Add(Me.lblBillerName)
            Me.Controls.Add(Me.lblBillerID)
            Me.Controls.Add(Me.lblElapsedTime)
            Me.Controls.Add(Me.LabelControl3)
            Me.Controls.Add(Me.LabelControl2)
            Me.Controls.Add(Me.LabelControl1)
            Me.Name = "StatusForm"
            Me.StartPosition = System.Windows.Forms.FormStartPosition.WindowsDefaultLocation
            Me.Text = "RPS Table Import Function"
            Me.TopMost = True
            CType(Me.DxErrorProvider1, System.ComponentModel.ISupportInitialize).EndInit()
            Me.ResumeLayout(False)
            Me.PerformLayout()

        End Sub
        Friend WithEvents LabelControl1 As DevExpress.XtraEditors.LabelControl
        Friend WithEvents LabelControl2 As DevExpress.XtraEditors.LabelControl
        Friend WithEvents LabelControl3 As DevExpress.XtraEditors.LabelControl
        Friend WithEvents lblBillerName As DevExpress.XtraEditors.LabelControl
        Friend WithEvents lblBillerID As DevExpress.XtraEditors.LabelControl
        Friend WithEvents lblElapsedTime As DevExpress.XtraEditors.LabelControl
        Friend WithEvents SimpleButton_Cancel As DevExpress.XtraEditors.SimpleButton
        Friend WithEvents Timer1 As System.Windows.Forms.Timer
    End Class
End Namespace
