#Region "Copyright 2000-2012 DebtPlus, L.L.C."
'{*******************************************************************}
'{                                                                   }
'{       DebtPlus Debt Management System                             }
'{                                                                   }
'{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
'{       ALL RIGHTS RESERVED                                         }
'{                                                                   }
'{   The entire contents of this file is protected by U.S. and       }
'{   International Copyright Laws. Unauthorized reproduction,        }
'{   reverse-engineering, and distribution of all or any portion of  }
'{   the code contained in this file is strictly prohibited and may  }
'{   result in severe civil and criminal penalties and will be       }
'{   prosecuted to the maximum extent possible under the law.        }
'{                                                                   }
'{   RESTRICTIONS                                                    }
'{                                                                   }
'{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
'{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
'{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
'{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
'{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
'{                                                                   }
'{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
'{   ADDITIONAL RESTRICTIONS.                                        }
'{                                                                   }
'{*******************************************************************}
#End Region

Option Strict Off
Option Explicit On

Imports DebtPlus.Utils
Imports System.Data.SqlClient

Namespace RPPS.Biller.Import.File
    Friend Class BillerClass
        Implements IDisposable

        Public input_class As RPPS_Input = Nothing

        Public col_addresses As New System.Collections.Generic.List(Of AddressClass)
        Public col_akas As New System.Collections.Generic.List(Of AkaClass)
        Public col_masks As New System.Collections.Generic.List(Of MaskClass)
        Public col_contacts As New System.Collections.Generic.List(Of ContactClass)

        Public acdind As String = "A"
        Private Key As Object = System.DBNull.Value

        Public privatebillerinfo As BillerInfoClass = Nothing
        Public Property billerinfo() As BillerInfoClass
            Get
                If privatebillerinfo Is Nothing Then
                    privatebillerinfo = New BillerInfoClass
                    privatebillerinfo.input_class = input_class
                End If
                billerinfo = privatebillerinfo
            End Get
            Set(ByVal Value As BillerInfoClass)
                privatebillerinfo = Value
            End Set
        End Property

        Friend Sub Parse(ByRef xmlInput As RPPS_XML)

            With xmlInput

                ' Find the key to the biller if there is one.
                If .XMLReader.HasAttributes Then
                    For AttributeID As System.Int32 = 0 To xmlInput.XMLReader.AttributeCount - 1
                        xmlInput.XMLReader.MoveToAttribute(AttributeID)
                        Select Case xmlInput.XMLReader.Name.ToLower()
                            Case "key"
                                xmlInput.XMLReader.ReadAttributeValue()
                                billerinfo.Key = xmlInput.XMLReader.Value
                        End Select
                    Next
                End If

                ' Go to the biller content to read the fields
                .XMLReader.MoveToContent()

                ' Process the fields until we are at the end of the biller
                Do While Not .BadFile AndAlso .XMLReader.Read
                    Dim ElementName As String = .XMLReader.Name.ToLower()

                    Select Case .XMLReader.NodeType
                        Case Xml.XmlNodeType.EndElement
                            If ElementName = "biller" Then Exit Do

                        Case Xml.XmlNodeType.Element
                            Select Case ElementName
                                Case "acdind"
                                    xmlInput.parse_Field(acdind)

                                Case "effdate"
                                    xmlInput.parse_Field(billerinfo.effdate)

                                Case "blrid"
                                    xmlInput.parse_Field(billerinfo.blrid)

                                Case "billerinfo"
                                    billerinfo.Parse(xmlInput)

                                Case "addresses"
                                    Parse_Addresses(xmlInput)

                                Case "contacts"
                                    Parse_Contacts(xmlInput)

                                Case "lineofbusiness"
                                    Parse_LineOfBusinesses(xmlInput)

                                Case "akas"
                                    Parse_BlrAKA(xmlInput)

                                Case "billermasks", "masks"
                                    Parse_Masks(xmlInput)

                                Case Else
                                    .SkipField()
                            End Select
                    End Select
                Loop
            End With
        End Sub

        Private Sub Parse_Addresses(ByRef xmlInput As RPPS_XML)

            With xmlInput
                Dim SectionName As String = .XMLReader.Name.ToLower()

                .XMLReader.MoveToContent()

                Do While Not .BadFile AndAlso .XMLReader.Read
                    Dim ElementName As String = .XMLReader.Name.ToLower()

                    Select Case .XMLReader.NodeType
                        Case Xml.XmlNodeType.EndElement
                            If ElementName = SectionName Then Exit Do

                        Case Xml.XmlNodeType.Element
                            If ElementName = "address" Then
                                Dim nxt As New AddressClass
                                With nxt
                                    .Parse(xmlInput)
                                    col_addresses.Add(nxt)
                                End With
                            End If
                    End Select
                Loop
            End With
        End Sub

        Private Sub Parse_LineOfBusinesses(ByRef xmlInput As RPPS_XML)

            With xmlInput
                Dim SectionName As String = .XMLReader.Name.ToLower()

                .XMLReader.MoveToContent()

                Do While Not .BadFile AndAlso .XMLReader.Read
                    Dim ElementName As String = .XMLReader.Name.ToLower()
                    Select Case .XMLReader.NodeType
                        Case Xml.XmlNodeType.EndElement
                            If ElementName = SectionName Then Exit Do

                        Case Xml.XmlNodeType.Element
                            If ElementName = "lineofbusiness" Then
                                Dim nxt As New LineOfBusinessClass
                                With nxt
                                    .Parse(xmlInput)
                                    billerinfo.col_lob.Add(nxt)
                                End With
                            End If
                    End Select
                Loop
            End With
        End Sub

        Private Sub Parse_BlrAKA(ByRef xmlInput As RPPS_XML)

            With xmlInput
                Dim SectionName As String = .XMLReader.Name.ToLower()

                .XMLReader.MoveToContent()

                Do While Not .BadFile AndAlso .XMLReader.Read
                    Dim ElementName As String = .XMLReader.Name.ToLower()
                    Select Case .XMLReader.NodeType
                        Case Xml.XmlNodeType.EndElement
                            If ElementName = SectionName Then Exit Do

                        Case Xml.XmlNodeType.Element
                            If ElementName = "aka" Then
                                Dim nxt As New AkaClass
                                With nxt
                                    .Parse(xmlInput)
                                    col_akas.Add(nxt)
                                End With
                                If .BadFile Then Exit Sub
                            End If
                    End Select
                Loop
            End With
        End Sub

        Private Sub Parse_Masks(ByRef xmlInput As RPPS_XML)

            With xmlInput
                Dim SectionName As String = xmlInput.XMLReader.Name.ToLower()

                .XMLReader.MoveToContent()

                Do While Not .BadFile AndAlso .XMLReader.Read
                    Dim ElementName As String = .XMLReader.Name.ToLower()

                    Select Case .XMLReader.NodeType
                        Case Xml.XmlNodeType.EndElement
                            If ElementName = SectionName Then Exit Do

                        Case Xml.XmlNodeType.Element
                            If ElementName = "mask" OrElse ElementName = "billermask" Then
                                Dim nxt As New MaskClass
                                With nxt
                                    .Parse(xmlInput)
                                    col_masks.Add(nxt)
                                End With
                            End If
                    End Select

                Loop
            End With
        End Sub

        Private Sub Parse_Contacts(ByVal XMLInput As RPPS_XML)
            With XMLInput
                Dim SectionName As String = .XMLReader.Name.ToLower()
                .XMLReader.MoveToContent()

                ' Process the fields until we are at the end of the biller
                Do While Not .BadFile AndAlso .XMLReader.Read
                    Dim ElementName As String = .XMLReader.Name.ToLower()

                    Select Case .XMLReader.NodeType
                        Case Xml.XmlNodeType.EndElement
                            If ElementName = SectionName Then Exit Do

                        Case Xml.XmlNodeType.Element
                            Select Case ElementName
                                Case "contact"
                                    Dim nxt As New ContactClass
                                    nxt.parse(XMLInput)
                                    col_contacts.Add(nxt)
                                Case Else
                                    .SkipField()
                            End Select
                    End Select
                Loop
            End With
        End Sub

        Public Sub WriteInformation()

            Dim cn As SqlClient.SqlConnection = New SqlConnection(DebtPlus.LINQ.SQLInfoClass.getDefault().ConnectionString)
            Dim txn As SqlClient.SqlTransaction = Nothing
            Try
                cn.Open()
                txn = cn.BeginTransaction

                DeleteItems(cn, txn)

                Select Case UCase(acdind)
                    Case "D"
                    Case Else

                        ' Create the biller ID information
                        Write_Biller_ID(cn, txn)

                        Dim isCC As Boolean = False

                        ' Update the checkdigit to show "reality"
                        With billerinfo
                            .WriteInfo(cn, txn, Me)

                            ' Find the LOB being a CC for a credit card
                            For Each item As LineOfBusinessClass In .col_lob
                                If String.Compare(item.LOB, "cc", True) = 0 Then isCC = True
                            Next
                        End With

                        ' Create the ADDRESS items
                        For Each addr As AddressClass In col_addresses
                            addr.WriteInfo(cn, txn, Me)
                        Next

                        ' Create the MASK items
                        For Each mask As MaskClass In col_masks

                            ' Determine if the mask has a LUHN checkdigit.
                            ' List of credit card masks, first is mastercard, then visa, then amex, then discover. These are all standard LUHN checkdigits.
                            Static RegexMask As String = "^" + _
                                                         "(5[1-5][0-9#][0-9#][0-9#][0-9#][0-9#][0-9#][0-9#][0-9#][0-9#][0-9#][0-9#][0-9#][0-9#][0-9#]|" + _
                                                         "4([0-9#][0-9#][0-9#][0-9#][0-9#][0-9#][0-9#][0-9#][0-9#][0-9#][0-9#][0-9#][0-9#][0-9#]|[0-9#][0-9#][0-9#][0-9#][0-9#][0-9#][0-9#][0-9#][0-9#][0-9#][0-9#][0-9#][0-9#][0-9#][0-9#])|" + _
                                                         "(34|37)[0-9#][0-9#][0-9#][0-9#][0-9#][0-9#][0-9#][0-9#][0-9#][0-9#][0-9#][0-9#][0-9#]|" + _
                                                         "6011[0-9#][0-9#][0-9#][0-9#][0-9#][0-9#][0-9#][0-9#][0-9#][0-9#][0-9#][0-9#])" + _
                                                         "$"

                            mask.MaskInfo.checkdigit = If(isCC AndAlso System.Text.RegularExpressions.Regex.IsMatch(mask.MaskInfo.mask, RegexMask), "Y", "N")
                            mask.WriteInfo(cn, txn, Me)
                        Next

                        ' Add the AKA for the original name of the creditor.
                        Dim aka_class As New AkaClass
                        aka_class.name = billerinfo.billername
                        col_akas.Add(aka_class)

                        ' Create the AKA items
                        For Each aka As AkaClass In col_akas
                            aka.WriteInfo(cn, txn, Me)
                        Next
                End Select

                ' Commit the changes at this point
                txn.Commit()
                txn = Nothing

            Finally
                If txn IsNot Nothing Then
                    Try
                        txn.Rollback()
                    Catch exRollback As System.Exception
                    End Try
                    txn.Dispose()
                    txn = Nothing
                End If

                If cn IsNot Nothing Then cn.Dispose()
            End Try
        End Sub

        Private Sub DeleteItems(ByVal cn As SqlClient.SqlConnection, ByVal txn As SqlClient.SqlTransaction)
            DeleteItems_biller_akas(cn, txn)
            DeleteItems_biller_addresses(cn, txn)
            DeleteItems_biller_lobs(cn, txn)
            DeleteItems_biller_masks(cn, txn)
            DeleteItems_biller_id(cn, txn)
        End Sub

        Private Sub DeleteItems_biller_akas(ByVal cn As SqlClient.SqlConnection, ByVal txn As SqlClient.SqlTransaction)

            ' Delete all of the AKA items for this specific biller id
            Using cmd As SqlClient.SqlCommand = New SqlCommand
                With cmd
                    .Connection = cn
                    .Transaction = txn
                    .CommandType = CommandType.Text
                    .CommandText = "DELETE FROM rpps_akas WHERE rpps_biller_id=@rpps_biller_id"
                    .Parameters.Add("@rpps_biller_id", SqlDbType.VarChar, 20).Value = billerinfo.blrid
                    .ExecuteNonQuery()
                End With
            End Using
        End Sub

        Private Sub DeleteItems_biller_addresses(ByVal cn As SqlClient.SqlConnection, ByVal txn As SqlClient.SqlTransaction)

            ' Delete all of the ADDRESS items for this specific biller id
            Using cmd As SqlClient.SqlCommand = New SqlCommand
                With cmd
                    .Connection = cn
                    .Transaction = txn
                    .CommandType = CommandType.Text
                    .CommandText = "DELETE FROM rpps_addresses WHERE rpps_biller_id=@rpps_biller_id"
                    .Parameters.Add("@rpps_biller_id", SqlDbType.VarChar, 20).Value = billerinfo.blrid
                    .ExecuteNonQuery()
                End With
            End Using

        End Sub

        Private Sub DeleteItems_biller_lobs(ByVal cn As SqlClient.SqlConnection, ByVal txn As SqlClient.SqlTransaction)

            ' Delete all of the LOB items for this specific biller id
            Using cmd As SqlClient.SqlCommand = New SqlCommand
                With cmd
                    .Connection = cn
                    .Transaction = txn
                    .CommandType = CommandType.Text
                    .CommandText = "DELETE FROM rpps_lobs WHERE rpps_biller_id=@rpps_biller_id"
                    .Parameters.Add("@rpps_biller_id", SqlDbType.VarChar, 20).Value = billerinfo.blrid
                    .ExecuteNonQuery()
                End With
            End Using

        End Sub

        Private Sub DeleteItems_biller_masks(ByVal cn As SqlClient.SqlConnection, ByVal txn As SqlClient.SqlTransaction)

            ' Delete all of the MASK items for this specific biller id
            Using cmd As SqlClient.SqlCommand = New SqlCommand
                With cmd
                    .Connection = cn
                    .Transaction = txn
                    .CommandType = CommandType.Text
                    .CommandText = "DELETE FROM rpps_masks WHERE rpps_biller_id=@rpps_biller_id"
                    .Parameters.Add("@rpps_biller_id", SqlDbType.VarChar, 20).Value = billerinfo.blrid
                    .ExecuteNonQuery()
                End With
            End Using

        End Sub

        Private Sub DeleteItems_biller_id(ByVal cn As SqlClient.SqlConnection, ByVal txn As SqlClient.SqlTransaction)

            ' Delete all of the biller information item for this specific biller id
            Using cmd As SqlClient.SqlCommand = New SqlCommand
                With cmd
                    .Connection = cn
                    .Transaction = txn
                    .CommandType = CommandType.Text
                    Dim strSQL As String = "DELETE FROM rpps_biller_ids WHERE rpps_biller_id=@rpps_biller_id"
                    .Parameters.Add("@rpps_biller_id", SqlDbType.VarChar, 20).Value = billerinfo.blrid

                    If billerinfo.trnaba <> String.Empty Then
                        strSQL += " OR biller_aba=@rpps_biller_aba"
                        .Parameters.Add("@rpps_biller_aba", SqlDbType.VarChar, 20).Value = billerinfo.trnaba
                    End If

                    .CommandText = strSQL
                    .ExecuteNonQuery()
                End With
            End Using
        End Sub

        Private Sub Write_Biller_ID(ByVal cn As SqlClient.SqlConnection, ByVal txn As SqlClient.SqlTransaction)

            Try
                Using cmd As SqlClient.SqlCommand = New SqlCommand
                    With cmd
                        .Connection = cn
                        .Transaction = txn
                        .CommandText = "INSERT INTO rpps_biller_ids(rpps_biller_id, effdate, biller_aba) VALUES (@rpps_biller_id, @effdate, @biller_aba)"
                        .Parameters.Add("@rpps_biller_id", SqlDbType.VarChar, 20).Value = billerinfo.blrid
                        .Parameters.Add("@effdate", SqlDbType.DateTime).Value = Date_Value(billerinfo.effdate)
                        .Parameters.Add("@biller_aba", SqlDbType.VarChar, 20).Value = billerinfo.trnaba
                        .ExecuteNonQuery()
                    End With
                End Using

            Catch ex As SqlClient.SqlException

                ' Ignore a duplicate key for now. It is not really important here.
                If ex.Message.IndexOf(" duplicate key row ") < 0 Then Throw ex
            End Try

        End Sub

        Private disposedValue As Boolean = False        ' To detect redundant calls

        ' IDisposable
        Protected Overridable Sub Dispose(ByVal disposing As Boolean)
            If Not Me.disposedValue Then
                If disposing Then
                    col_addresses.Clear()
                    col_akas.Clear()
                    col_masks.Clear()
                End If

                col_addresses = Nothing
                col_akas = Nothing
                col_masks = Nothing

                privatebillerinfo = Nothing
                input_class = Nothing

                acdind = String.Empty
            End If

            Me.disposedValue = True
        End Sub

#Region " IDisposable Support "
        ' This code added by Visual Basic to correctly implement the disposable pattern.
        Public Sub Dispose() Implements IDisposable.Dispose
            ' Do not change this code.  Put cleanup code in Dispose(ByVal disposing As Boolean) above.
            Dispose(True)
            GC.SuppressFinalize(Me)
        End Sub
#End Region

    End Class
End Namespace
