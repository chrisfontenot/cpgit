#Region "Copyright 2000-2012 DebtPlus, L.L.C."
'{*******************************************************************}
'{                                                                   }
'{       DebtPlus Debt Management System                             }
'{                                                                   }
'{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
'{       ALL RIGHTS RESERVED                                         }
'{                                                                   }
'{   The entire contents of this file is protected by U.S. and       }
'{   International Copyright Laws. Unauthorized reproduction,        }
'{   reverse-engineering, and distribution of all or any portion of  }
'{   the code contained in this file is strictly prohibited and may  }
'{   result in severe civil and criminal penalties and will be       }
'{   prosecuted to the maximum extent possible under the law.        }
'{                                                                   }
'{   RESTRICTIONS                                                    }
'{                                                                   }
'{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
'{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
'{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
'{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
'{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
'{                                                                   }
'{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
'{   ADDITIONAL RESTRICTIONS.                                        }
'{                                                                   }
'{*******************************************************************}
#End Region

Option Strict Off
Option Explicit On

Namespace RPPS.Biller.Import.File
    Friend Class PhoneClass
        Public Key As Object = System.DBNull.Value
        Public PhoneType As String = String.Empty
        Public PhoneNum As String = String.Empty
        Public effdate As String = String.Empty

        Public Sub Parse(ByVal XmlInput As RPPS_XML)

            With XmlInput
                Dim SectionName As String = .XMLReader.Name.ToLower()

                ' Find the key to the biller if there is one.
                If .XMLReader.HasAttributes Then
                    For AttributeID As System.Int32 = 0 To XmlInput.XMLReader.AttributeCount - 1
                        XmlInput.XMLReader.MoveToAttribute(AttributeID)
                        Select Case XmlInput.XMLReader.Name.ToLower()
                            Case "key"
                                XmlInput.XMLReader.ReadAttributeValue()
                                Key = XmlInput.XMLReader.Value
                        End Select
                    Next
                End If

                .XMLReader.MoveToContent()

                ' Process the fields until we are at the end of the biller
                Do While Not .BadFile AndAlso .XMLReader.Read
                    Dim ElementName As String = .XMLReader.Name.ToLower()

                    Select Case .XMLReader.NodeType
                        Case Xml.XmlNodeType.EndElement
                            If ElementName = SectionName Then Exit Do

                        Case Xml.XmlNodeType.Element
                            Select Case ElementName
                                Case "phonetype"
                                    XmlInput.parse_Field(PhoneType)
                                Case "phonenum"
                                    XmlInput.parse_Field(PhoneNum)
                                Case "effdate"
                                    XmlInput.parse_Field(effdate)
                                Case Else
                                    .SkipField()
                            End Select
                    End Select
                Loop
            End With
        End Sub
    End Class

    Friend Class ContactInfoClass
        Public Key As Object = System.DBNull.Value
        Public effdate As String = String.Empty
        Public ContactInfoType As String = String.Empty
        Public Organization As String = String.Empty
        Public FirstName As String = String.Empty
        Public LastName As String = String.Empty
        Public Address1 As String = String.Empty
        Public Address2 As String = String.Empty
        Public City As String = String.Empty
        Public State As String = String.Empty
        Public Country As String = String.Empty
        Public PostalCode As String = String.Empty
        Public Email As String = String.Empty
        Public Title As String = String.Empty

        Public Sub Parse(ByRef XmlInput As RPPS_XML)

            With XmlInput
                Dim SectionName As String = .XMLReader.Name.ToLower()

                ' Find the key to the biller if there is one.
                If .XMLReader.HasAttributes Then
                    For AttributeID As System.Int32 = 0 To XmlInput.XMLReader.AttributeCount - 1
                        XmlInput.XMLReader.MoveToAttribute(AttributeID)
                        Select Case XmlInput.XMLReader.Name.ToLower()
                            Case "key"
                                XmlInput.XMLReader.ReadAttributeValue()
                                Key = XmlInput.XMLReader.Value
                        End Select
                    Next
                End If

                .XMLReader.MoveToContent()

                ' Process the fields until we are at the end of the biller
                Do While Not .BadFile AndAlso .XMLReader.Read
                    Dim ElementName As String = .XMLReader.Name.ToLower()

                    Select Case .XMLReader.NodeType
                        Case Xml.XmlNodeType.EndElement
                            If ElementName = SectionName Then Exit Do

                        Case Xml.XmlNodeType.Element
                            Select Case ElementName
                                Case "effdate"
                                    XmlInput.parse_Field(effdate)
                                Case "type"
                                    XmlInput.parse_Field(ContactInfoType)
                                Case "title"
                                    XmlInput.parse_Field(Title)
                                Case "organization"
                                    XmlInput.parse_Field(Organization)
                                Case "firstname"
                                    XmlInput.parse_Field(FirstName)
                                Case "lastname"
                                    XmlInput.parse_Field(LastName)
                                Case "address1"
                                    XmlInput.parse_Field(Address1)
                                Case "address2"
                                    XmlInput.parse_Field(Address2)
                                Case "city"
                                    XmlInput.parse_Field(City)
                                Case "state"
                                    XmlInput.parse_Field(State)
                                Case "postalcode"
                                    XmlInput.parse_Field(PostalCode)
                                Case "country"
                                    XmlInput.parse_Field(Country)
                                Case "email"
                                    XmlInput.parse_Field(Email)
                                Case Else
                                    .SkipField()
                            End Select
                    End Select
                Loop
            End With
        End Sub
    End Class

    Friend Class ContactClass
        Implements IDisposable

        Public col_phones As New System.Collections.Generic.List(Of PhoneClass)
        Public ContactInfo As New ContactInfoClass

        Friend Sub parse(ByRef xmlInput As RPPS_XML)

            With xmlInput
                Dim SectionName As String = .XMLReader.Name.ToLower()
                .XMLReader.MoveToContent()

                ' Process the fields until we are at the end of the biller
                Do While Not .BadFile AndAlso .XMLReader.Read
                    Dim ElementName As String = .XMLReader.Name.ToLower()

                    Select Case .XMLReader.NodeType
                        Case Xml.XmlNodeType.EndElement
                            If ElementName = SectionName Then Exit Do

                        Case Xml.XmlNodeType.Element
                            Select Case ElementName
                                Case "contactinfo"
                                    ContactInfo.Parse(xmlInput)
                                Case "phones"
                                    Parse_Phones(xmlInput)
                                Case Else
                                    .SkipField()
                            End Select
                    End Select
                Loop
            End With
        End Sub

        Private Sub Parse_Phones(ByVal XMLInput As RPPS_XML)
            With XMLInput
                Dim SectionName As String = .XMLReader.Name.ToLower()
                .XMLReader.MoveToContent()

                ' Process the fields until we are at the end of the biller
                Do While Not .BadFile AndAlso .XMLReader.Read
                    Dim ElementName As String = .XMLReader.Name.ToLower()

                    Select Case .XMLReader.NodeType
                        Case Xml.XmlNodeType.EndElement
                            If ElementName = SectionName Then Exit Do

                        Case Xml.XmlNodeType.Element
                            Select Case ElementName
                                Case "phone"
                                    Dim nxt As New PhoneClass
                                    nxt.Parse(XMLInput)
                                    col_phones.Add(nxt)
                                Case Else
                                    .SkipField()
                            End Select
                    End Select
                Loop
            End With
        End Sub

        Private disposedValue As Boolean = False        ' To detect redundant calls

        ' IDisposable
        Protected Overridable Sub Dispose(ByVal disposing As Boolean)
            If Not Me.disposedValue Then
                If disposing Then
                    col_phones.Clear()
                End If
            End If
            Me.disposedValue = True
        End Sub

#Region " IDisposable Support "
        ' This code added by Visual Basic to correctly implement the disposable pattern.
        Public Sub Dispose() Implements IDisposable.Dispose
            ' Do not change this code.  Put cleanup code in Dispose(ByVal disposing As Boolean) above.
            Dispose(True)
            GC.SuppressFinalize(Me)
        End Sub
#End Region

    End Class
End Namespace
