#Region "Copyright 2000-2012 DebtPlus, L.L.C."
'{*******************************************************************}
'{                                                                   }
'{       DebtPlus Debt Management System                             }
'{                                                                   }
'{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
'{       ALL RIGHTS RESERVED                                         }
'{                                                                   }
'{   The entire contents of this file is protected by U.S. and       }
'{   International Copyright Laws. Unauthorized reproduction,        }
'{   reverse-engineering, and distribution of all or any portion of  }
'{   the code contained in this file is strictly prohibited and may  }
'{   result in severe civil and criminal penalties and will be       }
'{   prosecuted to the maximum extent possible under the law.        }
'{                                                                   }
'{   RESTRICTIONS                                                    }
'{                                                                   }
'{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
'{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
'{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
'{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
'{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
'{                                                                   }
'{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
'{   ADDITIONAL RESTRICTIONS.                                        }
'{                                                                   }
'{*******************************************************************}
#End Region

Option Strict Off
Option Explicit On

Imports System.Data.SqlClient

Namespace RPPS.Biller.Import.File
    Friend Class BillerInfoClass
        Public input_class As RPPS_Input
        Public col_lob As New System.Collections.Generic.List(Of LineOfBusinessClass)

        Public Key As Object = System.DBNull.Value
        Public livedate As String = String.Empty
        Public effdate As String = String.Empty
        Public trnaba As String = String.Empty
        Public privatebillername As String = String.Empty
        Public ClassName As String = String.Empty
        Public billertype As String = String.Empty
        Public prenotes As String = String.Empty
        Public guarpayonly As String = String.Empty
        Public dmpprenote As String = String.Empty
        Public dmppayonly As String = String.Empty
        Public blroldname As String = String.Empty
        Public pvtblr As String = String.Empty
        Public blrnote As String = String.Empty

        Public acceptFBD As String = "N"
        Public acceptFBC As String = "N"
        Public acceptCDN As String = "N"
        Public acceptCDF As String = "N"
        Public acceptExceptionPay As String = "N"
        Public returnCDR As String = "N"
        Public returnCDT As String = "N"
        Public returnCDA As String = "N"
        Public returnCDV As String = "N"
        Public returnCDC As String = "N"
        Public returnCDM As String = "N"
        Public ReqAdndaRev As String = "N"
        Public CheckDigit As String = "N"
        Public AvgResponseTime As String = String.Empty
        Public Country As String = String.Empty
        Public State As String = String.Empty
        Public TerritoryCode As String = String.Empty
        Public FileFormat As String = String.Empty
        Private privateblrid As String = String.Empty
        Private privateacceptCDD As String = "N"
        Private privateacceptCDV As String = "N"
        Private privateacceptCDP As String = "N"
        Private privateCurrency As String = "840"

        Public Property Currency() As String
            Get
                Return privateCurrency
            End Get
            Set(ByVal value As String)
                privateCurrency = value
            End Set
        End Property

        Public Property acceptCDP() As String
            Get
                Return privateacceptCDP
            End Get
            Set(ByVal value As String)
                privateacceptCDP = value
            End Set
        End Property

        Public Property acceptCDV() As String
            Get
                Return privateacceptCDV
            End Get
            Set(ByVal value As String)
                privateacceptCDV = value
            End Set
        End Property

        Public Property acceptCDD() As String
            Get
                Return privateacceptCDD
            End Get
            Set(ByVal value As String)
                privateacceptCDD = value
            End Set
        End Property

        Public Property blrid() As String
            Get
                blrid = privateblrid
            End Get
            Set(ByVal Value As String)
                privateblrid = Value
                input_class.Set_BillerID(privateblrid)
            End Set
        End Property

        Public Property billername() As String
            Get
                billername = privatebillername
            End Get
            Set(ByVal Value As String)
                privatebillername = Value
                input_class.Set_BillerName(Value)
            End Set
        End Property

        Public Sub Parse(ByRef xmlInput As RPPS_XML)

            With xmlInput

                ' Start with the content of the billerinfo area
                xmlInput.XMLReader.MoveToContent()

                ' Process the fields until we are at the end of the biller
                Do While Not .BadFile AndAlso .XMLReader.Read
                    Dim ElementName As String = .XMLReader.Name.ToLower()

                    Select Case .XMLReader.NodeType
                        Case Xml.XmlNodeType.EndElement
                            If ElementName = "billerinfo" Then Exit Do

                        Case Xml.XmlNodeType.Element
                            Select Case ElementName

                                Case "fileformat"
                                    xmlInput.parse_Field(FileFormat)

                                Case "country"
                                    xmlInput.parse_Field(Country)

                                Case "state"
                                    xmlInput.parse_Field(State)

                                Case "territorycode"
                                    xmlInput.parse_Field(TerritoryCode)

                                Case "effdate"
                                    xmlInput.parse_Field(effdate)

                                Case "livedate"
                                    xmlInput.parse_Field(livedate)

                                Case "lineofbusiness"
                                    Dim nxt As New LineOfBusinessClass
                                    With nxt
                                        xmlInput.parse_Field(nxt.LOB)
                                        col_lob.Add(nxt)
                                    End With

                                Case "trnaba"
                                    xmlInput.parse_Field(trnaba)

                                Case "billerid"
                                    xmlInput.parse_Field(blrid)

                                Case "billername"
                                    xmlInput.parse_Field(billername)

                                Case "billerclass"
                                    xmlInput.parse_Field(ClassName)

                                Case "billertype"
                                    xmlInput.parse_Field(billertype)

                                Case "blroldname", "prevname"
                                    xmlInput.parse_Field(blroldname)

                                Case "pvtblr"
                                    xmlInput.parse_Field(pvtblr)

                                Case "blrnote", "billernote"
                                    xmlInput.parse_Field(blrnote)

                                Case "currency"
                                    xmlInput.parse_Field(Currency)

                                Case "prenotes"
                                    xmlInput.parse_Field(prenotes)

                                Case "guarpayonly"
                                    xmlInput.parse_Field(guarpayonly)

                                Case "dmpprenote"
                                    xmlInput.parse_Field(dmpprenote)

                                Case "dmppayonly"
                                    xmlInput.parse_Field(dmppayonly)

                                Case "acceptcdp"
                                    xmlInput.parse_Field(acceptCDP)

                                Case "acceptcdv"
                                    xmlInput.parse_Field(acceptCDV)

                                Case "acceptcdd"
                                    xmlInput.parse_Field(acceptCDD)

                                Case "acceptfbd"
                                    xmlInput.parse_Field(acceptFBD)

                                Case "acceptfbc"
                                    xmlInput.parse_Field(acceptFBC)

                                Case "acceptcdn"
                                    xmlInput.parse_Field(acceptCDN)

                                Case "acceptcdf"
                                    xmlInput.parse_Field(acceptCDF)

                                Case "acceptexceptionpay"
                                    xmlInput.parse_Field(acceptExceptionPay)

                                Case "returncdr"
                                    xmlInput.parse_Field(returnCDR)

                                Case "returncdt"
                                    xmlInput.parse_Field(returnCDT)

                                Case "returncda"
                                    xmlInput.parse_Field(returnCDA)

                                Case "returncdv"
                                    xmlInput.parse_Field(returnCDV)

                                Case "returncdc"
                                    xmlInput.parse_Field(returnCDC)

                                Case "returncdm"
                                    xmlInput.parse_Field(returnCDM)

                                Case "reqadndarev"
                                    xmlInput.parse_Field(ReqAdndaRev)

                                Case "checkdigit"
                                    xmlInput.parse_Field(CheckDigit)

                                Case "avgresponsetime"
                                    xmlInput.parse_Field(AvgResponseTime)

                                Case Else
                                    xmlInput.SkipField()
                            End Select
                    End Select
                Loop
            End With
        End Sub

        Private Function billertype_value() As System.Int32
            Dim answer As System.Int32 = BILLER_TYPE_OTHER

            Select Case billertype.ToUpper()
                Case "GROSS"
                    answer = BILLER_TYPE_GROSS
                Case "MODIFIED GROSS"
                    answer = BILLER_TYPE_MODIFIED_GROSS
                Case "NET"
                    answer = BILLER_TYPE_NET
            End Select

            Return answer
        End Function

        Public Shared Function bool_value(ByVal input_string As String) As System.Int32

            ' Look for specific strings
            Select Case input_string.ToLower()
                Case "y", "yes", "1", "t", "true", "+"
                    Return 1
                Case "n", "no", "0", "f", "false", "-"
                    Return 0
                Case Else
                    Return 0
            End Select

        End Function

        Friend Sub WriteInfo(ByVal cn As SqlClient.SqlConnection, ByVal txn As SqlClient.SqlTransaction, ByRef biller As BillerClass)
            Using cmd As SqlClient.SqlCommand = New SqlCommand
                With cmd
                    .Connection = cn
                    .Transaction = txn
                    .CommandText = "UPDATE rpps_biller_ids SET [reqAdndaRev]=@reqAdndaRev,[returns_cdr]=@returns_cdr,[returns_cdt]=@returns_cdt,[returns_cda]=@returns_cda,[returns_cdv]=@returns_cdv,[returns_cdc]=@returns_cdc,[currency]=@currency,[effdate]=@effdate,[livedate]=@livedate,[Mastercard_key]=@Mastercard_key,[biller_name]=@biller_name,[biller_aba]=@biller_aba,[biller_class]=@biller_class,[biller_type]=@biller_type,[payment_prenote]=@payment_prenote,[guaranteed_funds]=@guaranteed_funds,[proposal_prenote]=@proposal_prenote,[dmppayonly]=@dmppayonly,[blroldname]=@blroldname,[pvt_biller_id]=@pvt_biller_id,[note]=@note,[send_cdp]=@send_cdp,[send_fbc]=@send_fbc,[send_fbd]=@send_fbd,[send_cdv]=@send_cdv,[send_cdd]=@send_cdd,[send_cdn]=@send_cdn,[send_cdf]=@send_cdf,[checkdigit]=@checkdigit WHERE [rpps_biller_id]=@rpps_biller_id"
                    .CommandType = CommandType.Text

                    .Parameters.Add("@reqAdndaRev", SqlDbType.Bit).Value = bool_value(ReqAdndaRev)
                    .Parameters.Add("@returns_cdr", SqlDbType.Bit).Value = bool_value(returnCDR)
                    .Parameters.Add("@returns_cdt", SqlDbType.Bit).Value = bool_value(returnCDT)
                    .Parameters.Add("@returns_cda", SqlDbType.Bit).Value = bool_value(returnCDA)
                    .Parameters.Add("@returns_cdv", SqlDbType.Bit).Value = bool_value(returnCDV)
                    .Parameters.Add("@returns_cdc", SqlDbType.Bit).Value = bool_value(returnCDC)
                    .Parameters.Add("@currency", SqlDbType.VarChar, 50).Value = Currency
                    .Parameters.Add("@effdate", SqlDbType.DateTime).Value = Date_Value(effdate)
                    .Parameters.Add("@livedate", SqlDbType.DateTime).Value = Date_Value(livedate)
                    .Parameters.Add("@Mastercard_key", SqlDbType.VarChar, 50).Value = Key
                    .Parameters.Add("@biller_aba", SqlDbType.VarChar, 80).Value = trnaba
                    .Parameters.Add("@biller_name", SqlDbType.VarChar, 80).Value = billername.PadLeft(16).Substring(0, 16).Trim()
                    .Parameters.Add("@biller_class", SqlDbType.VarChar, 80).Value = ClassName
                    .Parameters.Add("@biller_type", SqlDbType.Int).Value = billertype_value()
                    .Parameters.Add("@payment_prenote", SqlDbType.Bit).Value = bool_value(prenotes)
                    .Parameters.Add("@guaranteed_funds", SqlDbType.Bit).Value = bool_value(guarpayonly)
                    .Parameters.Add("@proposal_prenote", SqlDbType.Bit).Value = bool_value(dmpprenote)
                    .Parameters.Add("@dmppayonly", SqlDbType.Bit).Value = bool_value(dmppayonly)
                    .Parameters.Add("@blroldname", SqlDbType.VarChar, 80).Value = blroldname
                    .Parameters.Add("@pvt_biller_id", SqlDbType.Bit).Value = bool_value(pvtblr)
                    .Parameters.Add("@note", SqlDbType.VarChar, 80).Value = blrnote
                    .Parameters.Add("@rpps_biller_id", SqlDbType.VarChar, 80).Value = biller.billerinfo.blrid
                    .Parameters.Add("@send_cdp", SqlDbType.Bit).Value = bool_value(acceptCDP)
                    .Parameters.Add("@send_fbc", SqlDbType.Bit).Value = bool_value(acceptFBC)
                    .Parameters.Add("@send_fbd", SqlDbType.Bit).Value = bool_value(acceptFBD)
                    .Parameters.Add("@send_cdd", SqlDbType.Bit).Value = bool_value(acceptCDD)
                    .Parameters.Add("@send_cdv", SqlDbType.Bit).Value = bool_value(acceptCDV)
                    .Parameters.Add("@send_cdn", SqlDbType.Bit).Value = bool_value(acceptCDN)
                    .Parameters.Add("@send_cdf", SqlDbType.Bit).Value = bool_value(acceptCDF)
                    .Parameters.Add("@checkdigit", SqlDbType.Bit).Value = bool_value(CheckDigit)

                    .ExecuteNonQuery()
                End With
            End Using

            ' Create the LOB items
            Dim ie As IEnumerator = col_lob.GetEnumerator
            Do While ie.MoveNext
                Dim lob_class As LineOfBusinessClass = ie.Current
                lob_class.WriteInfo(cn, txn, biller)
            Loop

        End Sub
    End Class
End Namespace
