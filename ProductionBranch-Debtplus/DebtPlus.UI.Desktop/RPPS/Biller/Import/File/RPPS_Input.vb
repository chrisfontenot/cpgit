#Region "Copyright 2000-2012 DebtPlus, L.L.C."
'{*******************************************************************}
'{                                                                   }
'{       DebtPlus Debt Management System                             }
'{                                                                   }
'{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
'{       ALL RIGHTS RESERVED                                         }
'{                                                                   }
'{   The entire contents of this file is protected by U.S. and       }
'{   International Copyright Laws. Unauthorized reproduction,        }
'{   reverse-engineering, and distribution of all or any portion of  }
'{   the code contained in this file is strictly prohibited and may  }
'{   result in severe civil and criminal penalties and will be       }
'{   prosecuted to the maximum extent possible under the law.        }
'{                                                                   }
'{   RESTRICTIONS                                                    }
'{                                                                   }
'{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
'{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
'{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
'{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
'{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
'{                                                                   }
'{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
'{   ADDITIONAL RESTRICTIONS.                                        }
'{                                                                   }
'{*******************************************************************}
#End Region

Option Strict Off
Option Explicit On

Imports DebtPlus.Utils
Imports System.Data.SqlClient
Imports System.Windows.Forms

Namespace RPPS.Biller.Import.File
    Friend MustInherit Class RPPS_Input

        Dim pushback As String = String.Empty
        Dim LineCol As System.Int32 = 0
        Dim MaxCol As System.Int32 = 0
        Public LineString As String = String.Empty

        ''' <summary>
        ''' Creation of the input class
        ''' </summary>
        Protected privatestatus_form As StatusForm = Nothing
        Public Sub New(ByVal sts As StatusForm)
            privatestatus_form = sts
        End Sub

        Friend ReadOnly Property status_form() As StatusForm
            Get
                Return privatestatus_form
            End Get
        End Property

        Friend Sub Set_BillerID(ByVal biller_id As String)
            status_form.SetBillerID(biller_id)
        End Sub

        Friend Sub Set_BillerName(ByVal biller_name As String)
            status_form.SetBillerName(biller_name)
        End Sub

        Dim iStream As System.IO.FileStream = Nothing
        Protected txtStream As System.IO.StreamReader = Nothing

        Friend Overridable Function OpenFile() As Boolean
            Dim answer As Boolean = False

            Try
                iStream = New System.IO.FileStream(status_form.ap.FileName, System.IO.FileMode.Open)
                txtStream = New System.IO.StreamReader(iStream)
                answer = True

            Catch ex As System.IO.FileNotFoundException
                DebtPlus.Data.Forms.MessageBox.Show(ex.Message, "Error opening input file", MessageBoxButtons.OK, MessageBoxIcon.Error)
            Catch ex As System.IO.PathTooLongException
                DebtPlus.Data.Forms.MessageBox.Show(ex.Message, "Error opening input file", MessageBoxButtons.OK, MessageBoxIcon.Error)
            Catch ex As System.IO.DirectoryNotFoundException
                DebtPlus.Data.Forms.MessageBox.Show(ex.Message, "Error opening input file", MessageBoxButtons.OK, MessageBoxIcon.Error)
            End Try

            Return answer
        End Function

        Public Function NextLine() As Boolean

            ' At the end of the file, return failure
            If txtStream.EndOfStream Then
                Return False
            End If

            ' Otherwise, return success
            LineString = txtStream.ReadLine
            Return True
        End Function

        Public Function NextChar() As String

            ' If there is a pending "returned to input stream" character, return it.
            If pushback.Length = 1 Then
                NextChar = pushback
                pushback = String.Empty
                Exit Function
            End If

            ' If there are more than one "returned to input stream" characters, return the next character
            If pushback.Length > 0 Then
                NextChar = pushback.Substring(0, 1)
                pushback = pushback.Substring(1)
                Exit Function
            End If

            ' If the input is at EOF then return the magic marker
            Do
                If LineCol < 0 Then
                    If Not NextLine() Then
                        NextChar = "EOF"
                        Exit Function
                    End If
                    LineCol = 0
                    MaxCol = LineString.Length
                End If

                ' Retrieve the next input character from the input stream
                If LineCol >= MaxCol Then
                    LineCol = -1
                Else
                    NextChar = LineString.Substring(LineCol, 1)
                    LineCol = LineCol + 1
                    Exit Function
                End If
            Loop
        End Function

        Public Sub PushBackString(ByVal InputString As String)

            ' Do not attempt to return the EOF sequence to the input.
            If InputString <> "EOF" Then pushback += InputString

        End Sub

        Public Overridable Sub Complete()
            Dim cn As SqlClient.SqlConnection = New SqlConnection(DebtPlus.LINQ.SQLInfoClass.getDefault().ConnectionString)
            Try
                cn.Open()
                Using cmd As SqlClient.SqlCommand = New SqlCommand
                    With cmd
                        .Connection = cn
                        .CommandText = "xpr_rpps_download_cleanup"
                        .CommandTimeout = System.Math.Max(DebtPlus.LINQ.SQLInfoClass.getDefault().CommandTimeout, 600)
                        .CommandType = CommandType.StoredProcedure
                        .ExecuteNonQuery()
                    End With
                End Using

            Catch ex As SqlClient.SqlException
                DebtPlus.UI.Common.ErrorHandling.HandleErrors(ex, "Error running the RPPS cleanup procedure")

            Finally
                If cn IsNot Nothing AndAlso cn.State <> ConnectionState.Closed Then cn.Close()
            End Try
        End Sub

        Public Sub DeleteTables(ByVal cn As SqlClient.SqlConnection, ByVal txn As SqlClient.SqlTransaction)

            Using cmd As SqlClient.SqlCommand = New SqlCommand
                With cmd
                    .Connection = cn
                    .Transaction = txn
                    .CommandText = "DELETE rpps_masks"
                    .CommandTimeout = System.Math.Max(DebtPlus.LINQ.SQLInfoClass.getDefault().CommandTimeout, 600)
                    .CommandType = CommandType.Text
                    .ExecuteNonQuery()
                End With
            End Using

            Using cmd As SqlClient.SqlCommand = New SqlCommand
                With cmd
                    .Connection = cn
                    .Transaction = txn
                    .CommandText = "DELETE rpps_addresses"
                    .CommandTimeout = System.Math.Max(DebtPlus.LINQ.SQLInfoClass.getDefault().CommandTimeout, 600)
                    .CommandType = CommandType.Text
                    .ExecuteNonQuery()
                End With
            End Using

            Using cmd As SqlClient.SqlCommand = New SqlCommand
                With cmd
                    .Connection = cn
                    .Transaction = txn
                    .CommandText = "DELETE rpps_lobs"
                    .CommandTimeout = System.Math.Max(DebtPlus.LINQ.SQLInfoClass.getDefault().CommandTimeout, 600)
                    .CommandType = CommandType.Text
                    .ExecuteNonQuery()
                End With
            End Using

            Using cmd As SqlClient.SqlCommand = New SqlCommand
                With cmd
                    .Connection = cn
                    .Transaction = txn
                    .CommandText = "DELETE rpps_akas"
                    .CommandTimeout = System.Math.Max(DebtPlus.LINQ.SQLInfoClass.getDefault().CommandTimeout, 600)
                    .CommandType = CommandType.Text
                    .ExecuteNonQuery()
                End With
            End Using

            Using cmd As SqlClient.SqlCommand = New SqlCommand
                With cmd
                    .Connection = cn
                    .Transaction = txn
                    .CommandText = "DELETE rpps_biller_ids"
                    .CommandTimeout = System.Math.Max(DebtPlus.LINQ.SQLInfoClass.getDefault().CommandTimeout, 600)
                    .CommandType = CommandType.Text
                    .ExecuteNonQuery()
                End With
            End Using
        End Sub

        Public Overridable Function ParseFile() As Boolean
            Return False
        End Function

        Public BadFile As Boolean = False
    End Class
End Namespace
