#Region "Copyright 2000-2012 DebtPlus, L.L.C."
'{*******************************************************************}
'{                                                                   }
'{       DebtPlus Debt Management System                             }
'{                                                                   }
'{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
'{       ALL RIGHTS RESERVED                                         }
'{                                                                   }
'{   The entire contents of this file is protected by U.S. and       }
'{   International Copyright Laws. Unauthorized reproduction,        }
'{   reverse-engineering, and distribution of all or any portion of  }
'{   the code contained in this file is strictly prohibited and may  }
'{   result in severe civil and criminal penalties and will be       }
'{   prosecuted to the maximum extent possible under the law.        }
'{                                                                   }
'{   RESTRICTIONS                                                    }
'{                                                                   }
'{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
'{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
'{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
'{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
'{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
'{                                                                   }
'{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
'{   ADDITIONAL RESTRICTIONS.                                        }
'{                                                                   }
'{*******************************************************************}
#End Region

Option Strict Off
Option Explicit On

Namespace RPPS.Biller.Import.File
    Module modConstants
        ' Set to "1" for a special version for the Citrix server
        Public Const CITRIX_SERVER As System.Int32 = 1

        ' Information for the resource strings
        Public Const RESOURCE_APP As String = "DebtPlus"

        ' At this point the monthly amounts get a warning that it is too much for a month.
        Public Const MAXIMUM_MONTHLY_AMOUNT As Decimal = (90000.0# / 12.0#)

        ' Appointment states
        Public Const APPT_STATUS_PENDING As String = "P" ' Appointment is yet to be kept
        Public Const APPT_STATUS_KEPT As String = "K" ' The appointment was made and kept
        Public Const APPT_STATUS_CANCELLED As String = "C" ' The appointment was cancelled without reschedule
        Public Const APPT_STATUS_MISSED As String = "M" ' The appointment was made and never kept
        Public Const APPT_STATUS_RESCHEDULED As String = "R" ' The appointment was rescheduled to a different time

        ' Appointment Result types
        Public Const APPT_RESULT_DMP As String = "DMP" ' Debt Management Program
        Public Const APPT_RESULT_FCO As String = "FCO" ' Financial Counseling Only (no program, just talk)
        Public Const APPT_RESULT_ROA As String = "ROA" ' Referred to Other Agency (sent client elsewhere -- outside of our agency)
        Public Const APPT_RESULT_PND As String = "PND" ' The result is pending to be assigned
        Public Const APPT_RESULT_WKS As String = "WKS" ' We referred the client to a workshop for further help

        ' Resrouce registery strings
        Public Const RESOURCE_REGISTRY_DATABASE As String = "DebtPlus_SQL.CDatabase"
        Public Const RESOURCE_REGISTRY_SQLFIELD As String = "DebtPlus_SQL.CSQLfield"
        Public Const RESOURCE_REGISTRY_OPTIONS As String = "DebtPlus_SQL.COptions"
        Public Const RESOURCE_REGISTRY_LETTERFIELD As String = "DebtPlus_Letters.CLetterField"
        Public Const RESOURCE_REGISTRY_LETTERFIELDS As String = "DebtPlus_Letters.CLetterFields"
        Public Const RESOURCE_REGISTRY_LETTER As String = "DebtPlus_Letters.CLetter"
        Public Const RESOURCE_REGISTRY_RESOURCEREPORT As String = "DebtPlus_ResourceGuide.ResourceReport"
        Public Const RESOURCE_REGISTRY_VOID As String = "DebtPlus_Void.CVoid"
        Public Const RESOURCE_REGISTRY_SELLDEBT As String = "DebtPlus_SellDebt.SellDebt"
        Public Const RESOURCE_REGISTRY_CLIENTUPDATE As String = "DebtPlus_Client.CClientUpdate"
        Public Const RESOURCE_REGISTRY_CREDITORUPDATE As String = "DebtPlus_Creditor.CCreditorUpdate"
        Public Const RESOURCE_REGISTRY_APPOINTMENTS As String = "DP_Appointment.CAppointments"
        Public Const RESOURCE_REGISTRY_SOUNDS As String = "Sounds.CSounds"
        Public Const RESOURCE_REGISTRY_CHECK As String = "DP_PrintChecks.CCheckWriter"
        Public Const RESOURCE_REGISTRY_SALES_TOOL As String = "DP_SalesTool.Sales_Tool"
        Public Const RESOURCE_REGISTRY_REPORTS As String = "DP_Reports.CReports"

        ' Proposal status information
        Public Const PROPOSAL_STATUS_NONE As System.Int32 = 0 ' A proposal letter has not been sent
        Public Const PROPOSAL_STATUS_PENDING As System.Int32 = 1 ' A proposal letter was sent and a reply is pending
        Public Const PROPOSAL_STATUS_DEFAULT As System.Int32 = 2 ' A proposal letter was accepted by default
        Public Const PROPOSAL_STATUS_ACCEPTED As System.Int32 = 3 ' A proposal letter was accepted by a person
        Public Const PROPOSAL_STATUS_REJECTED As System.Int32 = 4 ' A proposal letter was rejected

        ' Values for the "cleared" indiction in the registers_trust and registers_invoices tables
        Public Const REGISTER_STATE_PRINT As String = "P" ' Print is pending
        Public Const REGISTER_STATE_PENDING As String = " " ' Item has been printed and is now waiting to be cleared
        Public Const REGISTER_STATE_VOID As String = "V" ' Item has been voided
        Public Const REGISTER_STATE_CASHED As String = "E" ' Item has been chashed but no statement has been received
        Public Const REGISTER_STATE_MARKED As String = "C" ' Item has been cleared but not reconciled
        Public Const REGISTER_STATE_RECONSILED As String = "R" ' Item has been reconciled against a statement

        ' Notes are stored in the database with these types
        Public Const NOTE_TYPE_PERM As System.Int32 = 1
        Public Const NOTE_TYPE_TEMP As System.Int32 = 2
        Public Const NOTE_TYPE_SYSTEM As System.Int32 = 3
        Public Const NOTE_TYPE_ALERT As System.Int32 = 4
        Public Const NOTE_TYPE_DISBURSEMENT As System.Int32 = 5
        Public Const NOTE_TYPE_RESEARCH As System.Int32 = 6

        ' These are the type markers in the contact_types
        Public Const CONTACT_TYPE_FAIRSHARE As String = "I"
        Public Const CONTACT_TYPE_CORRESPONDENCE As String = "L"
        Public Const CONTACT_TYPE_TERMINATIONS As String = "T"
        Public Const CONTACT_TYPE_PAYMENTS As String = "P"
        Public Const CONTACT_TYPE_PROPOSALS As String = "R"

        ' These are the type codes in the creditor address table
        Public Const CREDITOR_ADDRESS_INVOICE As String = "I"
        Public Const CREDITOR_ADDRESS_PAYMENT As String = "P"
        Public Const CREDITOR_ADDRESS_CORRESPONDENCE As String = "C"
        Public Const CREDITOR_ADDRESS_PROPOSALS As String = "L"

        ' Type of disbursement required for the creditor
        Public Const CREDITOR_DISBURSEMENT_CHECK As System.Int32 = 0
        Public Const CREDITOR_DISBURSEMENT_RPS As System.Int32 = 1

        ' Types of billing information required by the item
        Public Const BILLER_TYPE_NET As System.Int32 = 1
        Public Const BILLER_TYPE_GROSS As System.Int32 = 2
        Public Const BILLER_TYPE_MODIFIED_GROSS As System.Int32 = 3
        Public Const BILLER_TYPE_OTHER As System.Int32 = 4

        ' Format strings for the various types
        Public Const DATE_FORMAT As String = "MM/dd/yy"

        ' Visual Basic F5 key
        Public Const VB_F5 As System.Int32 = 116
    End Module
End Namespace
