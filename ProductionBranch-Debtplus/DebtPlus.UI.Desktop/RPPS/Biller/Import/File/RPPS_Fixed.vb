#Region "Copyright 2000-2012 DebtPlus, L.L.C."
'{*******************************************************************}
'{                                                                   }
'{       DebtPlus Debt Management System                             }
'{                                                                   }
'{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
'{       ALL RIGHTS RESERVED                                         }
'{                                                                   }
'{   The entire contents of this file is protected by U.S. and       }
'{   International Copyright Laws. Unauthorized reproduction,        }
'{   reverse-engineering, and distribution of all or any portion of  }
'{   the code contained in this file is strictly prohibited and may  }
'{   result in severe civil and criminal penalties and will be       }
'{   prosecuted to the maximum extent possible under the law.        }
'{                                                                   }
'{   RESTRICTIONS                                                    }
'{                                                                   }
'{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
'{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
'{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
'{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
'{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
'{                                                                   }
'{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
'{   ADDITIONAL RESTRICTIONS.                                        }
'{                                                                   }
'{*******************************************************************}
#End Region

Option Strict Off
Option Explicit On

Namespace RPPS.Biller.Import.File
    Friend Class RPPS_Fixed
        Inherits RPPS_Input

        Dim strCol As System.Int32

        Public Sub New(ByVal sts As StatusForm)
            MyBase.New(sts)
        End Sub

        Private Function Field(ByVal size As System.Int32) As String
            Field = Trim(Mid(LineString, strCol, size))
            ' Debug.Print "'" + Mid$(LineString, strCol, size) + "'"
            strCol = strCol + size
        End Function

        Private Function ParseBiller() As Boolean
            Dim count_addresses As System.Int32
            Dim count_masks As System.Int32
            Dim index As System.Int32

            Dim strField As String

            Dim biller As BillerClass
            'UPGRADE_NOTE: Object biller may not be destroyed until it is garbage collected. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6E35BFF6-CD74-4B09-9689-3E1A43DF8969"'
            biller = Nothing

            ' Assume that the biller is proper.
            ParseBiller = True
            strCol = 1

            ' If this record is simply "there are no changes" then just terminate processing
            strField = Field(2)
            If strField = "X0" Then
                ParseBiller = False ' Simulate EOF
                Exit Function
            End If

            Dim strLob As String
            Dim cls_lob As LineOfBusinessClass
            Dim strAKA As String
            Dim cls_aka As AkaClass
            Dim Filler As String
            Dim addr1 As String
            Dim addr2 As String
            Dim city As String
            Dim state As String
            Dim zipcode As String
            Dim country As String
            Dim addr_class As AddressClass
            Dim strLen As String
            Dim strMask As String
            Dim strDesc As String
            Dim mask_class As MaskClass
            Do
                Select Case Mid(strField, 2, 1)
                    Case "0"
                        biller = New BillerClass
                        With biller
                            .input_class = Me
                            .acdind = Left(strField, 1)
                            .billerinfo.blrid = Field(10)
                            .billerinfo.effdate = Field(8)
                            .billerinfo.trnaba = Field(10)
                            .billerinfo.billername = Field(128)
                            .billerinfo.ClassName = Field(50)
                            .billerinfo.billertype = Field(50)

                            ' Line of business are the next group of fields
                            For index = 1 To 10
                                strLob = Field(3)
                                If strLob <> String.Empty Then
                                    cls_lob = New LineOfBusinessClass
                                    cls_lob.LOB = strLob
                                    .billerinfo.col_lob.Add(cls_lob)
                                End If
                            Next index

                            ' Parse the other "flags"
                            .billerinfo.prenotes = Field(1)
                            .billerinfo.guarpayonly = Field(1)
                            .billerinfo.dmpprenote = Field(1)
                            .billerinfo.guarpayonly = Field(1)
                            .billerinfo.pvtblr = Field(1)
                            .billerinfo.blroldname = Field(128)
                            .billerinfo.blrnote = Field(1000)

                            ' Parse the AKA list
                            For index = 1 To 8
                                strAKA = Field(128)
                                If strAKA <> String.Empty Then
                                    cls_aka = New AkaClass
                                    cls_aka.name = strAKA
                                    .col_akas.Add(cls_aka)
                                End If
                            Next index

                            ' The count of items follows this field
                            count_addresses = Val(Field(6))
                            count_masks = Val(Field(6))
                        End With

                    Case "1"
                        strCol = strCol + 10 ' Ignore the biller ID
                        If Not (biller Is Nothing) Then
                            While strCol < (Len(LineString) - 20)

                                ' Obtain the record for the current address information

                                Filler = Field(50)
                                addr1 = Field(50)
                                addr2 = Field(50)
                                city = Field(50)
                                state = Field(6)
                                zipcode = Field(12)
                                country = Field(3)

                                ' A "valid" address has a line for the address, city, and state. Others are "optional".
                                If addr1 <> String.Empty AndAlso city <> String.Empty And state <> String.Empty Then
                                    addr_class = New AddressClass
                                    With addr_class
                                        .addr1 = addr1
                                        .addr2 = addr2
                                        .city = city
                                        .state = state
                                        .zipcode = zipcode
                                        .country = country
                                    End With
                                    biller.col_addresses.Add(addr_class)
                                    count_addresses = count_addresses - 1
                                End If
                            End While
                        End If

                    Case "2"
                        strCol = strCol + 10 ' Ignore the biller ID
                        If Not (biller Is Nothing) Then
                            While strCol < (Len(LineString) - 72)

                                strLen = Field(2)
                                strMask = Field(22)
                                strDesc = Field(50)

                                If strMask <> String.Empty AndAlso strLen <> String.Empty Then
                                    mask_class = New MaskClass
                                    With mask_class
                                        .MaskInfo.mask = strMask + String.Empty
                                        .MaskInfo.masklen = strLen + String.Empty
                                        .MaskInfo.maskdesc = strDesc + String.Empty
                                    End With
                                    biller.col_masks.Add(mask_class)
                                    count_masks = count_masks - 1
                                End If
                            End While
                        End If
                End Select

                ' Read the next record
                If Not NextLine() Then
                    ParseBiller = False
                    Exit Do
                End If

                ' Split the record into the fields
                strCol = 1
                strField = Mid(LineString, 3, 10)

                ' If this is for a different biller then the current one is complete.
                If Not (biller Is Nothing) Then
                    If strField <> biller.billerinfo.blrid Then Exit Do
                End If
                strField = Field(2)
            Loop

            ' If we have reached this point then add the biller to the list. All of the additional data should be present.
            If Not (biller Is Nothing) AndAlso (count_masks = 0) AndAlso (count_addresses = 0) Then
                biller.WriteInformation()
            End If

        End Function

        Public Overrides Function ParseFile() As Boolean
            Dim answer As Boolean = False

            ' Read the first line of the input
            If NextLine() Then
                ParseFile = True
                Do While ParseBiller()
                Loop
            End If

            Return answer
        End Function
    End Class
End Namespace
