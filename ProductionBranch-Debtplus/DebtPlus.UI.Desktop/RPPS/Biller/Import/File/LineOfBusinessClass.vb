#Region "Copyright 2000-2012 DebtPlus, L.L.C."
'{*******************************************************************}
'{                                                                   }
'{       DebtPlus Debt Management System                             }
'{                                                                   }
'{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
'{       ALL RIGHTS RESERVED                                         }
'{                                                                   }
'{   The entire contents of this file is protected by U.S. and       }
'{   International Copyright Laws. Unauthorized reproduction,        }
'{   reverse-engineering, and distribution of all or any portion of  }
'{   the code contained in this file is strictly prohibited and may  }
'{   result in severe civil and criminal penalties and will be       }
'{   prosecuted to the maximum extent possible under the law.        }
'{                                                                   }
'{   RESTRICTIONS                                                    }
'{                                                                   }
'{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
'{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
'{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
'{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
'{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
'{                                                                   }
'{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
'{   ADDITIONAL RESTRICTIONS.                                        }
'{                                                                   }
'{*******************************************************************}
#End Region

Option Strict Off
Option Explicit On

Imports System.Data.SqlClient

Namespace RPPS.Biller.Import.File
    Friend Class LineOfBusinessClass
        Implements IDisposable

        Private privateLOB As String = String.Empty
        Public Property LOB() As String
            Get
                Return privateLOB
            End Get
            Set(ByVal value As String)
                privateLOB = value.Trim()
                If privateLOB.Length > 60 Then privateLOB = privateLOB.Substring(0, 60)
            End Set
        End Property

        Friend Sub Parse(ByRef xmlInput As RPPS_XML)
            xmlInput.parse_Field(LOB)
        End Sub

        Public Sub WriteInfo(ByVal cn As SqlClient.SqlConnection, ByVal txn As SqlClient.SqlTransaction, ByRef biller_class As BillerClass)

            Using cmd As SqlClient.SqlCommand = New SqlCommand
                With cmd
                    .Connection = cn
                    .Transaction = txn
                    .CommandText = "INSERT INTO rpps_lobs(rpps_biller_id,biller_lob) VALUES (@rpps_biller_id,@biller_lob)"
                    .CommandType = CommandType.Text

                    .Parameters.Add("@rpps_biller_id", SqlDbType.VarChar, 80).Value = biller_class.billerinfo.blrid
                    .Parameters.Add("@biller_lob", SqlDbType.VarChar, 80).Value = LOB
                    .ExecuteNonQuery()
                End With
            End Using

        End Sub

        Private disposedValue As Boolean = False        ' To detect redundant calls
        Protected Overridable Sub Dispose(ByVal disposing As Boolean)
            If Not Me.disposedValue Then
                If disposing Then
                    ' free other state (managed objects).
                End If

                privateLOB = Nothing
            End If
            Me.disposedValue = True
        End Sub

#Region " IDisposable Support "
        Public Sub Dispose() Implements IDisposable.Dispose
            Dispose(True)
            GC.SuppressFinalize(Me)
        End Sub
#End Region

    End Class
End Namespace
