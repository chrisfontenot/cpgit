#Region "Copyright 2000-2012 DebtPlus, L.L.C."
'{*******************************************************************}
'{                                                                   }
'{       DebtPlus Debt Management System                             }
'{                                                                   }
'{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
'{       ALL RIGHTS RESERVED                                         }
'{                                                                   }
'{   The entire contents of this file is protected by U.S. and       }
'{   International Copyright Laws. Unauthorized reproduction,        }
'{   reverse-engineering, and distribution of all or any portion of  }
'{   the code contained in this file is strictly prohibited and may  }
'{   result in severe civil and criminal penalties and will be       }
'{   prosecuted to the maximum extent possible under the law.        }
'{                                                                   }
'{   RESTRICTIONS                                                    }
'{                                                                   }
'{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
'{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
'{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
'{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
'{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
'{                                                                   }
'{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
'{   ADDITIONAL RESTRICTIONS.                                        }
'{                                                                   }
'{*******************************************************************}
#End Region
Imports DebtPlus.Utils
Imports System.Data.SqlClient
Imports System.Windows.Forms

Namespace RPPS.Biller.Import.File
    Friend Class StatusForm

        Dim date_start As Date

        Friend ap As ArgParser = Nothing
        Private WithEvents bt As New System.ComponentModel.BackgroundWorker

        Public Sub New()
            MyBase.New()
            InitializeComponent()
        End Sub

        Public Sub New(ByVal ap As ArgParser)
            MyBase.New()
            InitializeComponent()

            Me.ap = ap
            AddHandler SimpleButton_Cancel.Click, AddressOf cmd_Cancel_Click
            AddHandler Me.Load, AddressOf StatusForm_Load
            AddHandler Timer1.Tick, AddressOf Timer1_Tick
            AddHandler bt.DoWork, AddressOf bt_Worker
            AddHandler bt.RunWorkerCompleted, AddressOf bt_RunWorkerCompleted
        End Sub

        Private Sub cmd_Cancel_Click(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs)
            Close()
        End Sub

        Dim cls As RPPS_Input = Nothing
        Private Sub StatusForm_Load(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs)

            ' Determine the class
            Select Case ap.FileType
                Case ArgParser.FileTypes.Fixed
                    cls = New RPPS_Fixed(Me)

                Case ArgParser.FileTypes.Tab
                    cls = New RPPS_Tab(Me)

                Case ArgParser.FileTypes.xml
                    cls = New RPPS_XML(Me)

            End Select

            ' Attempt to open the input file. If failed then cancel the program.
            If cls.OpenFile Then
                If ap.FullDownload = DevExpress.Utils.DefaultBoolean.Default Then
                    ap.FullDownload = DirectCast(IIf(DebtPlus.Data.Forms.MessageBox.Show("Was this a full download of the entire biller database?", "Clear the existing tables", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2) = DialogResult.Yes, DevExpress.Utils.DefaultBoolean.True, DevExpress.Utils.DefaultBoolean.False), DevExpress.Utils.DefaultBoolean)
                End If

                bt.RunWorkerAsync()
                date_start = Now
                Timer1.Interval = 1000
                Timer1.Enabled = True

                SimpleButton_Cancel.Enabled = True
                SimpleButton_Cancel.Visible = True
            End If
        End Sub

        Public Sub StopTimer()
            Timer1.Enabled = False
            SimpleButton_Cancel.Enabled = False
            SimpleButton_Cancel.Visible = False
            Me.Close()
        End Sub

        Private Sub Timer1_Tick(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs)
            Dim lElapsed As TimeSpan = Now.Subtract(date_start)
            Dim nMin As System.Int32
            Dim nHr As System.Int32
            Dim nSec As System.Int32 = lElapsed.TotalSeconds

            nHr = nSec \ 3600
            nSec = nSec Mod 3600

            nMin = nSec \ 60
            nSec = nSec Mod 60

            lblElapsedTime.Text = String.Format("{0:00}:{1:00}:{2:00}", nHr, nMin, nSec)
        End Sub

        Public Sub bt_Worker(ByVal Sender As Object, ByVal e As System.EventArgs)

            ' Determine if this is a full download. If so indicated then clear the tables
            If ap.FullDownload = DevExpress.Utils.DefaultBoolean.True Then
                Dim cn As SqlClient.SqlConnection = New SqlConnection(DebtPlus.LINQ.SQLInfoClass.getDefault().ConnectionString)
                Dim txn As SqlClient.SqlTransaction = Nothing

                Try
                    cn.Open()
                    txn = cn.BeginTransaction
                    cls.DeleteTables(cn, txn)
                    txn.Commit()
                    txn = Nothing

                Catch ex As SqlClient.SqlException
                    DebtPlus.UI.Common.ErrorHandling.HandleErrors(ex, "Error deleting database tables")

                Finally
                    If txn IsNot Nothing Then txn.Commit()
                    If cn IsNot Nothing AndAlso cn.State <> ConnectionState.Closed Then cn.Close()
                End Try
            End If

            ' Do the parse of the input file
            cls.ParseFile()
            If Not cls.BadFile Then cls.Complete()
        End Sub

        Friend Delegate Sub BillerDelegate(ByVal InputString As String)
        Friend Sub SetBillerID(ByVal BillerID As String)
            If InvokeRequired Then
                BeginInvoke(New BillerDelegate(AddressOf SetBillerID), New Object() {BillerID})
            Else
                lblBillerID.Text = BillerID
            End If
        End Sub

        Friend Sub SetBillerName(ByVal BillerName As String)
            If InvokeRequired Then
                BeginInvoke(New BillerDelegate(AddressOf SetBillerName), New Object() {BillerName})
            Else
                lblBillerName.Text = BillerName
            End If
        End Sub

        Private Sub bt_RunWorkerCompleted(ByVal sender As Object, ByVal e As System.ComponentModel.RunWorkerCompletedEventArgs)
            Timer1.Enabled = False
            SimpleButton_Cancel.Text = "OK"

            lblBillerID.Text = "Load Completed"

            LabelControl2.Text = String.Empty
            LabelControl3.Text = String.Empty

            lblBillerName.Text = Environment.NewLine + "Press OK to terminate"
        End Sub
    End Class
End Namespace
