#Region "Copyright 2000-2012 DebtPlus, L.L.C."
'{*******************************************************************}
'{                                                                   }
'{       DebtPlus Debt Management System                             }
'{                                                                   }
'{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
'{       ALL RIGHTS RESERVED                                         }
'{                                                                   }
'{   The entire contents of this file is protected by U.S. and       }
'{   International Copyright Laws. Unauthorized reproduction,        }
'{   reverse-engineering, and distribution of all or any portion of  }
'{   the code contained in this file is strictly prohibited and may  }
'{   result in severe civil and criminal penalties and will be       }
'{   prosecuted to the maximum extent possible under the law.        }
'{                                                                   }
'{   RESTRICTIONS                                                    }
'{                                                                   }
'{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
'{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
'{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
'{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
'{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
'{                                                                   }
'{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
'{   ADDITIONAL RESTRICTIONS.                                        }
'{                                                                   }
'{*******************************************************************}
#End Region

Option Strict Off
Option Explicit On

Namespace RPPS.Biller.Import.File
    Friend Class RPPS_Tab
        Inherits RPPS_Input

        Public Sub New(ByVal sts As StatusForm)
            MyBase.New(sts)
        End Sub

        Private Function ParseBiller() As Boolean
            Dim count_addresses As System.Int32
            Dim count_masks As System.Int32
            Dim index As System.Int32
            Dim flds() As String

            Dim biller As BillerClass
            'UPGRADE_NOTE: Object biller may not be destroyed until it is garbage collected. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6E35BFF6-CD74-4B09-9689-3E1A43DF8969"'
            biller = Nothing

            ' Assume that the biller is proper.
            ParseBiller = True

            ' Split the input record into the proper types.
            flds = Split(LineString, vbTab)

            ' If this record is simply "there are no changes" then just terminate processing
            If flds(0) = "X0" Then
                ParseBiller = False ' Simulate EOF
                Exit Function
            End If

            Dim strLob As String
            Dim cls_lob As LineOfBusinessClass
            Dim strAKA As String
            Dim cls_aka As AkaClass
            Dim BoundLimit As System.Int32
            Dim addr_class As AddressClass
            Dim strLen As String
            Dim strMask As String
            Dim strDesc As String
            Dim mask_class As MaskClass
            Do
                Select Case Mid(flds(0), 2, 1)
                    Case "0"
                        biller = New BillerClass
                        With biller
                            .input_class = Me
                            .acdind = Left(flds(0), 1)
                            .billerinfo.blrid = flds(1)
                            .billerinfo.effdate = flds(2)
                            .billerinfo.trnaba = flds(3)
                            .billerinfo.billername = flds(4)
                            .billerinfo.ClassName = flds(5)
                            .billerinfo.billertype = flds(6)

                            ' Line of business are the next group of fields
                            For index = 1 To 10
                                strLob = Trim(flds(index + 6))
                                If strLob <> String.Empty Then
                                    cls_lob = New LineOfBusinessClass
                                    cls_lob.LOB = strLob
                                    .billerinfo.col_lob.Add(cls_lob)
                                End If
                            Next index

                            ' Parse the other "flags"
                            .billerinfo.prenotes = flds(17)
                            .billerinfo.guarpayonly = flds(18)
                            .billerinfo.dmpprenote = flds(19)
                            .billerinfo.guarpayonly = flds(20)
                            .billerinfo.pvtblr = flds(21)
                            .billerinfo.blroldname = flds(22)
                            .billerinfo.blrnote = flds(23)

                            ' Parse the AKA list
                            For index = 1 To 8
                                strAKA = Trim(flds(index + 23))
                                If strAKA <> String.Empty Then
                                    cls_aka = New AkaClass
                                    cls_aka.name = strAKA
                                    .col_akas.Add(cls_aka)
                                End If
                            Next index

                            ' The count of items follows this field
                            count_addresses = Val(flds(32))
                            count_masks = Val(flds(33))
                        End With

                    Case "1"
                        If Not (biller Is Nothing) Then
                            BoundLimit = UBound(flds) - 1
                            For index = 2 To BoundLimit Step 6
                                addr_class = New AddressClass

                                With addr_class
                                    ' Ignore the "reserved for future use field" at "index+0"
                                    If index + 1 <= BoundLimit Then .addr1 = flds(index + 1)
                                    If index + 1 <= BoundLimit Then .addr2 = flds(index + 2)
                                    If index + 2 <= BoundLimit Then .city = flds(index + 3)
                                    If index + 3 <= BoundLimit Then .state = flds(index + 4)
                                    If index + 4 <= BoundLimit Then .zipcode = flds(index + 5)
                                    If index + 5 <= BoundLimit Then .country = flds(index + 6)
                                End With
                                biller.col_addresses.Add(addr_class)
                                count_addresses = count_addresses - 1
                                If count_addresses = 0 Then Exit For
                            Next index

                        End If

                    Case "2"
                        If Not (biller Is Nothing) Then
                            For index = 2 To UBound(flds) - 1 Step 3

                                strLen = flds(index)
                                strMask = String.Empty
                                strDesc = String.Empty
                                If (index + 1) <= UBound(flds) Then strMask = flds(index + 1)
                                If (index + 2) <= UBound(flds) Then strDesc = flds(index + 2)

                                If strMask <> String.Empty AndAlso strLen <> String.Empty Then
                                    mask_class = New MaskClass
                                    With mask_class
                                        .MaskInfo.mask = strMask + String.Empty
                                        .MaskInfo.maskdesc = strDesc + String.Empty
                                        .MaskInfo.masklen = strLen + String.Empty
                                    End With
                                    biller.col_masks.Add(mask_class)
                                    count_masks = count_masks - 1
                                End If
                            Next index
                        End If
                End Select

                ' Read the next record
                If Not NextLine() Then
                    ParseBiller = False
                    Exit Do
                End If

                ' Split the record into the fields
                flds = Split(LineString, vbTab)

                ' If this is for a different biller then the current one is complete.
                If Not (biller Is Nothing) Then
                    If flds(1) <> biller.billerinfo.blrid Then Exit Do
                End If
            Loop

            ' If we have reached this point then add the biller to the list. All of the additional data should be present.
            System.Diagnostics.Debug.Assert((Not (biller Is Nothing)) AndAlso (count_masks = 0) AndAlso (count_addresses = 0), String.Empty)
            If Not (biller Is Nothing) AndAlso (count_masks = 0) AndAlso (count_addresses = 0) Then
                biller.WriteInformation()
            End If
        End Function

        Public Overrides Function ParseFile() As Boolean

            ' Read the first line of the input
            If Not NextLine() Then
                Return False
            End If

            ' Build a new collection of biller information
            Do While ParseBiller()
            Loop

            Return True
        End Function
    End Class
End Namespace
