#Region "Copyright 2000-2012 DebtPlus, L.L.C."
'{*******************************************************************}
'{                                                                   }
'{       DebtPlus Debt Management System                             }
'{                                                                   }
'{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
'{       ALL RIGHTS RESERVED                                         }
'{                                                                   }
'{   The entire contents of this file is protected by U.S. and       }
'{   International Copyright Laws. Unauthorized reproduction,        }
'{   reverse-engineering, and distribution of all or any portion of  }
'{   the code contained in this file is strictly prohibited and may  }
'{   result in severe civil and criminal penalties and will be       }
'{   prosecuted to the maximum extent possible under the law.        }
'{                                                                   }
'{   RESTRICTIONS                                                    }
'{                                                                   }
'{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
'{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
'{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
'{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
'{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
'{                                                                   }
'{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
'{   ADDITIONAL RESTRICTIONS.                                        }
'{                                                                   }
'{*******************************************************************}
#End Region

Option Strict Off
Option Explicit On

Imports System.Windows.Forms

Namespace RPPS.Biller.Import.File
    Friend Class RPPS_XML
        Inherits RPPS_Input

        Public Sub New(ByVal sts As StatusForm)
            MyBase.New(sts)
        End Sub

        Protected Friend Sub SkipField()
            XMLReader.Skip()
        End Sub

        Protected Friend TagModifier As String = String.Empty
        Friend XMLReader As System.Xml.XmlReader

        Friend Overrides Function OpenFile() As Boolean
            Dim answer As Boolean = False

            ' If the file is open, try to use it as an XML document.
            Try
                Dim settings As New Xml.XmlReaderSettings()
                With settings
                    .IgnoreWhitespace = True
                    .ConformanceLevel = Xml.ConformanceLevel.Document
                    .IgnoreWhitespace = True
                    .IgnoreComments = True
                End With

                ' Open the file directly without the stream. Let it manage it as-is.
                XMLReader = Xml.XmlReader.Create(status_form.ap.FileName, settings)
                answer = True

            Catch ex As Exception
                DebtPlus.Data.Forms.MessageBox.Show(ex.Message, "Error opening XML document", MessageBoxButtons.OK, MessageBoxIcon.Error)
                answer = False
            End Try

            ' Generate an error condition if there is no reader
            If XMLReader Is Nothing Then answer = False

            Return answer
        End Function

        Public Overrides Function ParseFile() As Boolean

            Try
                ' Go to the first node
                If Not XMLReader.Read Then Return False

                Do
                    Dim ElementName As String = XMLReader.Name.ToLower()

                    Select Case XMLReader.NodeType
                        Case Xml.XmlNodeType.Element
                            Select Case ElementName
                                Case "billers"
                                    parse_billers()

                                Case Else
                                    SkipField()

                            End Select
                    End Select
                Loop Until BadFile OrElse Not XMLReader.Read

                Return Not BadFile

            Catch ex As System.Xml.XmlException
                DebtPlus.Data.Forms.MessageBox.Show(ex.Message, "Error parsing bad XML file", MessageBoxButtons.OK, MessageBoxIcon.Error)
                Return False
            End Try
        End Function

        Protected Friend Sub parse_Field(ByRef Field As String)

            ' Go to the biller content to read the fields
            XMLReader.MoveToContent()

            ' Process the fields until we are at the end of the biller
            Do While Not BadFile AndAlso XMLReader.Read
                Select Case XMLReader.NodeType
                    Case Xml.XmlNodeType.EndElement
                        Exit Do

                    Case Xml.XmlNodeType.Text
                        Field = XMLReader.Value

                    Case Else

                End Select
            Loop
        End Sub

        Public Sub parse_billers()
            Dim SectionName As String = XMLReader.Name.ToLower()

            ' Go to the billers content. Skip the attributes.
            XMLReader.MoveToContent()

            Do While Not BadFile AndAlso XMLReader.Read
                Dim ElementName As String = XMLReader.Name.ToLower()

                Select Case XMLReader.NodeType
                    Case Xml.XmlNodeType.EndElement
                        If ElementName = SectionName Then Exit Do

                    Case Xml.XmlNodeType.Element
                        If ElementName = "biller" Then
                            Dim biller As New BillerClass
                            With biller
                                .input_class = Me
                                .Parse(Me)
                                If Not BadFile Then .WriteInformation()
                                .Dispose()
                            End With
                        End If

                    Case Else
                End Select
            Loop
        End Sub

        Public Overrides Sub Complete()
            ' Do nothing since the XML format has the flags that we want
        End Sub
    End Class
End Namespace
