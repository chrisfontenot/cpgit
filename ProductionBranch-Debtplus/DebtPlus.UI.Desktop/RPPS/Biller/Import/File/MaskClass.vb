#Region "Copyright 2000-2012 DebtPlus, L.L.C."
'{*******************************************************************}
'{                                                                   }
'{       DebtPlus Debt Management System                             }
'{                                                                   }
'{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
'{       ALL RIGHTS RESERVED                                         }
'{                                                                   }
'{   The entire contents of this file is protected by U.S. and       }
'{   International Copyright Laws. Unauthorized reproduction,        }
'{   reverse-engineering, and distribution of all or any portion of  }
'{   the code contained in this file is strictly prohibited and may  }
'{   result in severe civil and criminal penalties and will be       }
'{   prosecuted to the maximum extent possible under the law.        }
'{                                                                   }
'{   RESTRICTIONS                                                    }
'{                                                                   }
'{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
'{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
'{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
'{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
'{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
'{                                                                   }
'{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
'{   ADDITIONAL RESTRICTIONS.                                        }
'{                                                                   }
'{*******************************************************************}
#End Region

Option Strict Off
Option Explicit On

Imports System.Data.SqlClient

Namespace RPPS.Biller.Import.File
    Friend Class MaskDescriptionClass
        Public Key As Object = System.DBNull.Value
        Public MaskDesc As String = String.Empty
        Public effdate As String = String.Empty

        Public Sub Parse(ByVal XmlInput As RPPS_XML)

            With XmlInput
                Dim SectionName As String = .XMLReader.Name.ToLower()

                ' Find the key to the biller if there is one.
                If .XMLReader.HasAttributes Then
                    For AttributeID As System.Int32 = 0 To XmlInput.XMLReader.AttributeCount - 1
                        XmlInput.XMLReader.MoveToAttribute(AttributeID)
                        Select Case XmlInput.XMLReader.Name.ToLower()
                            Case "key"
                                XmlInput.XMLReader.ReadAttributeValue()
                                Key = XmlInput.XMLReader.Value
                        End Select
                    Next
                End If

                .XMLReader.MoveToContent()

                ' Process the fields until we are at the end of the biller
                Do While Not .BadFile AndAlso .XMLReader.Read
                    Dim ElementName As String = .XMLReader.Name.ToLower()

                    Select Case .XMLReader.NodeType
                        Case Xml.XmlNodeType.EndElement
                            If ElementName = SectionName Then Exit Do

                        Case Xml.XmlNodeType.Element
                            Select Case ElementName
                                Case "maskdesc"
                                    XmlInput.parse_Field(MaskDesc)
                                Case "effdate"
                                    XmlInput.parse_Field(effdate)
                                Case Else
                                    .SkipField()
                            End Select
                    End Select
                Loop
            End With
        End Sub
    End Class

    Friend Class MaskInfoClass

        Private privateMaskLen As String = "0"
        Public Property masklen() As String
            Get
                Return privateMaskLen
            End Get
            Set(ByVal value As String)
                privateMaskLen = value
            End Set
        End Property

        Private privateMask As String = String.Empty
        Public Property mask() As String
            Get
                Return privateMask
            End Get
            Set(ByVal value As String)
                privateMask = value.Trim()
            End Set
        End Property

        Private privateMaskDesc As String = String.Empty
        Public Property maskdesc() As String
            Get
                Return privateMaskDesc
            End Get
            Set(ByVal value As String)
                privateMaskDesc = value.Trim()
            End Set
        End Property

        Private privateeffdate As String = String.Empty
        Public Property effdate() As String
            Get
                Return privateeffdate
            End Get
            Set(ByVal value As String)
                privateeffdate = value
            End Set
        End Property

        Private privateexceptionmask As String = "N"
        Public Property exceptionmask() As String
            Get
                Return privateexceptionmask
            End Get
            Set(ByVal value As String)
                privateexceptionmask = value
            End Set
        End Property

        Private privatecheckdigit As String = "N"
        Public Property checkdigit() As String
            Get
                Return privatecheckdigit
            End Get
            Set(ByVal value As String)
                privatecheckdigit = value
            End Set
        End Property

        Friend Sub parse(ByRef xmlInput As RPPS_XML)

            With xmlInput
                Dim SectionName As String = .XMLReader.Name.ToLower()
                .XMLReader.MoveToContent()

                ' Process the fields until we are at the end of the biller
                Do While Not .BadFile AndAlso .XMLReader.Read
                    Dim ElementName As String = .XMLReader.Name.ToLower()

                    Select Case .XMLReader.NodeType
                        Case Xml.XmlNodeType.EndElement
                            If ElementName = SectionName Then Exit Do

                        Case Xml.XmlNodeType.Element
                            Select Case ElementName
                                Case "mask", "maskformat"
                                    .parse_Field(mask)
                                Case "masklen", "masklength"
                                    .parse_Field(masklen)
                                Case "maskdesc"
                                    .parse_Field(maskdesc)
                                Case "effdate"
                                    .parse_Field(effdate)
                                Case "exceptionmask"
                                    .parse_Field(exceptionmask)
                                Case "checkdigit"
                                    .parse_Field(checkdigit)
                                Case Else
                                    .SkipField()
                            End Select
                    End Select
                Loop
            End With
        End Sub
    End Class

    Friend Class MaskClass
        Implements IDisposable

        Public Key As Object = System.DBNull.Value
        Public MaskInfo As New MaskInfoClass
        Public col_MaskDescriptions As New System.Collections.Generic.List(Of MaskDescriptionClass)

        Friend Sub Parse(ByRef xmlInput As RPPS_XML)

            With xmlInput
                Dim SectionName As String = .XMLReader.Name.ToLower()

                ' Find the key to the biller if there is one.
                If .XMLReader.HasAttributes Then
                    For AttributeID As System.Int32 = 0 To xmlInput.XMLReader.AttributeCount - 1
                        xmlInput.XMLReader.MoveToAttribute(AttributeID)
                        Select Case xmlInput.XMLReader.Name.ToLower()
                            Case "key"
                                xmlInput.XMLReader.ReadAttributeValue()
                                Key = xmlInput.XMLReader.Value
                        End Select
                    Next
                End If

                .XMLReader.MoveToContent()

                ' Process the fields until we are at the end of the biller
                Do While Not .BadFile AndAlso .XMLReader.Read
                    Dim ElementName As String = .XMLReader.Name.ToLower()

                    Select Case .XMLReader.NodeType
                        Case Xml.XmlNodeType.EndElement
                            If ElementName = SectionName Then Exit Do

                        Case Xml.XmlNodeType.Element
                            Select Case ElementName
                                Case "mask", "maskformat"
                                    .parse_Field(MaskInfo.mask)
                                Case "masklen", "masklength"
                                    .parse_Field(MaskInfo.masklen)
                                Case "maskdesc"
                                    Dim nxt As New MaskDescriptionClass
                                    With nxt
                                        xmlInput.parse_Field(.MaskDesc)
                                    End With
                                    col_MaskDescriptions.Add(nxt)
                                Case "maskdescriptions"
                                    Parse_MaskDescriptions(xmlInput)
                                Case "effdate"
                                    .parse_Field(MaskInfo.effdate)
                                Case "exceptionmask"
                                    .parse_Field(MaskInfo.exceptionmask)
                                Case "checkdigit"
                                    .parse_Field(MaskInfo.checkdigit)
                                Case "maskinfo"
                                    MaskInfo.parse(xmlInput)
                                Case Else
                                    .SkipField()
                            End Select
                    End Select
                Loop
            End With
        End Sub

        Private Sub Parse_MaskDescriptions(ByRef xmlInput As RPPS_XML)

            With xmlInput
                Dim SectionName As String = .XMLReader.Name.ToLower()

                .XMLReader.MoveToContent()

                Do While Not .BadFile AndAlso .XMLReader.Read
                    Dim ElementName As String = .XMLReader.Name.ToLower()

                    Select Case .XMLReader.NodeType
                        Case Xml.XmlNodeType.EndElement
                            If ElementName = SectionName Then Exit Do

                        Case Xml.XmlNodeType.Element
                            Select Case ElementName
                                Case "maskdescription"
                                    Dim nxt As New MaskDescriptionClass
                                    With nxt
                                        .Parse(xmlInput)
                                        col_MaskDescriptions.Add(nxt)
                                    End With
                                Case Else
                                    .SkipField()
                            End Select
                    End Select
                Loop
            End With
        End Sub

        Public Sub WriteInfo(ByVal cn As SqlClient.SqlConnection, ByVal txn As SqlClient.SqlTransaction, ByRef biller_class As BillerClass)

            Using cmd As SqlClient.SqlCommand = New SqlCommand
                With cmd
                    .Connection = cn
                    .Transaction = txn
                    .CommandText = "INSERT INTO rpps_masks(ExceptionMask,CheckDigit,Mastercard_key,rpps_biller_id,length,mask,description,effdate) VALUES (@ExceptionMask,@CheckDigit,@Mastercard_key,@rpps_biller_id,@length,@mask,@description,@effdate)"
                    .CommandType = CommandType.Text

                    .Parameters.Add("@ExceptionMask", SqlDbType.Bit).Value = BillerInfoClass.bool_value(MaskInfo.exceptionmask)
                    .Parameters.Add("@CheckDigit", SqlDbType.Bit).Value = BillerInfoClass.bool_value(MaskInfo.checkdigit)
                    .Parameters.Add("@Mastercard_key", SqlDbType.Int).Value = Key
                    .Parameters.Add("@rpps_biller_id", SqlDbType.VarChar, 80).Value = biller_class.billerinfo.blrid
                    .Parameters.Add("@length", SqlDbType.Int).Value = Int_Value(MaskInfo.masklen)
                    .Parameters.Add("@mask", SqlDbType.VarChar, 80).Value = MaskInfo.mask
                    .Parameters.Add("@description", SqlDbType.VarChar, 80).Value = MaskInfo.maskdesc
                    .Parameters.Add("@effdate", SqlDbType.DateTime, 0).Value = Date_Value(MaskInfo.effdate)
                    .ExecuteNonQuery()
                End With
            End Using
        End Sub

        Private disposedValue As Boolean = False        ' To detect redundant calls
        Protected Overridable Sub Dispose(ByVal disposing As Boolean)
            If Not Me.disposedValue Then
                If disposing Then
                    MaskInfo = Nothing
                End If

                ' Release the storage
            End If

            Me.disposedValue = True
        End Sub

#Region " IDisposable Support "
        Public Sub Dispose() Implements IDisposable.Dispose
            Dispose(True)
            GC.SuppressFinalize(Me)
        End Sub
#End Region

    End Class
End Namespace
