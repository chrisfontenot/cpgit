Imports System.Text.RegularExpressions
Imports System.Globalization

Namespace RPPS.Biller.Import.File
    Public Module TypeUtilities
        Friend Function Date_Value(ByVal inputString As String) As Date

            ' Try to find a match to the simple formats
            Dim matchList As Match = Regex.Match(inputString, "^(?<year>\d\d\d\d)-(?<month>\d\d)-(?<day>\d\d)$")
            If Not matchList.Success Then
                matchList = Regex.Match(inputString, "^(?<year>\d\d\d\d)(?<month>\d\d)(?<day>\d\d)$")
            End If

            ' If there is a match then try to find the dates
            If matchList.Success Then
                Try
                    Dim answer As DateTime = New DateTime(Int32.Parse(matchList.Groups("year").Value), Int32.Parse(matchList.Groups("month").Value), Int32.Parse(matchList.Groups("day").Value))
                    If answer.Year >= 1900 Then
                        Return answer
                    End If
                Catch
                End Try
            End If

            Return New DateTime(1900, 1, 1)
        End Function

        Friend Function Int_Value(ByVal inputString As String) As Int32
            Dim answer As Int32 = 0
            If Not Int32.TryParse(inputString, NumberStyles.Integer, NumberFormatInfo.InvariantInfo, answer) Then answer = 0
            Return answer
        End Function
    End Module
End Namespace