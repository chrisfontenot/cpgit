Option Compare Binary
Option Explicit On 
Option Strict On

Imports System.Windows.Forms

Namespace RPPS.Biller.Import.File
    Friend Class ArgParser
        Inherits DebtPlus.Utils.ArgParserBase

        Public Enum FileTypes
            xml = 0
            Fixed = 1
            Tab = 2
        End Enum

        ''' <summary>
        ''' Is this a full download?
        ''' </summary>
        Private privateFullDownload As DevExpress.Utils.DefaultBoolean = DevExpress.Utils.DefaultBoolean.Default
        Public Property FullDownload() As DevExpress.Utils.DefaultBoolean
            Get
                Return privateFullDownload
            End Get
            Set(ByVal value As DevExpress.Utils.DefaultBoolean)
                privateFullDownload = value
            End Set
        End Property

        ''' <summary>
        ''' Type of the input file
        ''' </summary>
        Private privateType As FileTypes = FileTypes.xml
        Public ReadOnly Property FileType() As FileTypes
            Get
                Select Case System.IO.Path.GetExtension(FileName).ToLower()
                    Case ".xml"
                        Return FileTypes.xml
                    Case ".fixed"
                        Return FileTypes.Fixed
                    Case ".tab"
                        Return FileTypes.Tab
                    Case Else
                        Return FileTypes.xml
                End Select
            End Get
        End Property

        Protected Overrides Function OnNonSwitch(ByVal value As String) As SwitchStatus
            privateFileName = value
            Return DebtPlus.Utils.ArgParserBase.SwitchStatus.NoError
        End Function

        Protected Overrides Function OnSwitch(ByVal switchSymbol As String, ByVal switchValue As String) As SwitchStatus
            Dim ss As SwitchStatus = DebtPlus.Utils.ArgParserBase.SwitchStatus.NoError

            Select Case switchSymbol
                Case "p"
                    privateFullDownload = DevExpress.Utils.DefaultBoolean.False
                Case "f"
                    privateFullDownload = DevExpress.Utils.DefaultBoolean.True
                Case "?"
                    ss = DebtPlus.Utils.ArgParserBase.SwitchStatus.ShowUsage
                Case Else
                    ss = DebtPlus.Utils.ArgParserBase.SwitchStatus.YesError
            End Select

            Return ss
        End Function

        Protected Overrides Function OnDoneParse() As SwitchStatus
            Dim answer As SwitchStatus = MyBase.OnDoneParse()

            If answer = DebtPlus.Utils.ArgParserBase.SwitchStatus.NoError AndAlso FileName = String.Empty Then
                With New System.Windows.Forms.OpenFileDialog
                    .CheckFileExists = True
                    .CheckPathExists = True
                    .Filter = "Fixed Format (*.fixed)|*.fixed|Tab Format (*.tab)|*.tab|XML Format (*.xml)|*.xml|All Files (*.*) - XML format only|*.*"
                    .FilterIndex = 2
                    .Multiselect = False
                    .RestoreDirectory = True
                    .FileName = System.IO.Path.Combine(System.Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments), "*.xml")
                    .ValidateNames = True
                    If .ShowDialog() = DialogResult.OK Then
                        privateFileName = .FileName
                    End If
                    .Dispose()
                End With

                ' If there is no filename then terminate
                If FileName = String.Empty Then answer = DebtPlus.Utils.ArgParserBase.SwitchStatus.Quit
                Return answer
            End If

            Return answer
        End Function

        ''' <summary>
        ''' Name of the input file
        ''' </summary>
        Private privateFileName As String = String.Empty
        Public ReadOnly Property FileName() As String
            Get
                Return privateFileName
            End Get
        End Property

        ''' <summary>
        ''' Create an instance of our class
        ''' </summary>
        Public Sub New()
            MyBase.New(New String() {"f", "p"})
        End Sub

        ''' <summary>
        ''' Generate the command usage information
        ''' </summary>
        Protected Overrides Sub OnUsage(ByVal errorInfo As String)
            ' deprecated
        End Sub
    End Class
End Namespace
