#Region "Copyright 2000-2012 DebtPlus, L.L.C."
'{*******************************************************************}
'{                                                                   }
'{       DebtPlus Debt Management System                             }
'{                                                                   }
'{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
'{       ALL RIGHTS RESERVED                                         }
'{                                                                   }
'{   The entire contents of this file is protected by U.S. and       }
'{   International Copyright Laws. Unauthorized reproduction,        }
'{   reverse-engineering, and distribution of all or any portion of  }
'{   the code contained in this file is strictly prohibited and may  }
'{   result in severe civil and criminal penalties and will be       }
'{   prosecuted to the maximum extent possible under the law.        }
'{                                                                   }
'{   RESTRICTIONS                                                    }
'{                                                                   }
'{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
'{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
'{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
'{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
'{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
'{                                                                   }
'{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
'{   ADDITIONAL RESTRICTIONS.                                        }
'{                                                                   }
'{*******************************************************************}

#End Region

Option Strict Off
Option Explicit On

Imports System.Data.SqlClient
Imports System.Xml

Namespace RPPS.Biller.Import.File
    Friend Class AkaClass
        Private _key As Object = DBNull.Value
        Private _privatename As String = String.Empty
        Private _privateeffdate As String = String.Empty

        Public Property Name() As String
            Get
                Return _privatename
            End Get
            Set(ByVal value As String)
                _privatename = Value.Trim()
                If _privatename.Length > 80 Then _privatename = _privatename.Substring(0, 80)
            End Set
        End Property

        Public Property Effdate() As String
            Get
                Return _privateeffdate
            End Get
            Set(ByVal value As String)
                _privateeffdate = value
            End Set
        End Property

        Friend Sub Parse(ByRef xmlInput As RPPS_XML)
            With xmlInput
                Dim sectionName As String = .XMLReader.Name.ToLower()

                ' Find the key to the biller if there is one.
                If .XMLReader.HasAttributes Then
                    For attributeID As Int32 = 0 To xmlInput.XMLReader.AttributeCount - 1
                        xmlInput.XMLReader.MoveToAttribute(attributeID)
                        Select Case xmlInput.XMLReader.Name.ToLower()
                            Case "key"
                                xmlInput.XMLReader.ReadAttributeValue()
                                _key = xmlInput.XMLReader.Value
                        End Select
                    Next
                End If

                .XMLReader.MoveToContent()

                ' Process the fields until we are at the end of the biller
                Do While Not .BadFile AndAlso .XMLReader.Read
                    Dim elementName As String = .XMLReader.Name.ToLower()

                    Select Case .XMLReader.NodeType
                        Case XmlNodeType.EndElement
                            If elementName = sectionName Then Exit Do

                        Case XmlNodeType.Element
                            Select Case elementName
                                Case "name"
                                    .parse_Field(name)
                                Case "effdate"
                                    .parse_Field(effdate)
                                Case Else
                                    .SkipField()
                            End Select
                    End Select
                Loop
            End With
        End Sub

        Public Sub WriteInfo(ByVal cn As SqlConnection, ByVal txn As SqlTransaction, ByRef billerClass As BillerClass)
            Using cmd As SqlCommand = New SqlCommand
                With cmd
                    .Connection = cn
                    .Transaction = txn
                    .CommandText = "INSERT INTO rpps_akas(Mastercard_key,effdate,rpps_biller_id,name) VALUES (@Mastercard_key,@effdate,@rpps_biller_id,@name)"
                    .CommandType = CommandType.Text
                    .Parameters.Add("@Mastercard_key", SqlDbType.VarChar, 50).Value = _key
                    .Parameters.Add("@EffDate", SqlDbType.DateTime).Value = Date_Value(Effdate)
                    .Parameters.Add("@rpps_biller_id", SqlDbType.VarChar, 80).Value = billerClass.billerinfo.blrid
                    .Parameters.Add("@name", SqlDbType.VarChar, 80).Value = Name
                    .ExecuteNonQuery()
                End With
            End Using
        End Sub
    End Class
End Namespace
