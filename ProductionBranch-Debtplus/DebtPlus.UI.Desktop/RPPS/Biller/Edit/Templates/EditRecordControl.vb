#Region "Copyright 2000-2012 DebtPlus, L.L.C."
'{*******************************************************************}
'{                                                                   }
'{       DebtPlus Debt Management System                             }
'{                                                                   }
'{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
'{       ALL RIGHTS RESERVED                                         }
'{                                                                   }
'{   The entire contents of this file is protected by U.S. and       }
'{   International Copyright Laws. Unauthorized reproduction,        }
'{   reverse-engineering, and distribution of all or any portion of  }
'{   the code contained in this file is strictly prohibited and may  }
'{   result in severe civil and criminal penalties and will be       }
'{   prosecuted to the maximum extent possible under the law.        }
'{                                                                   }
'{   RESTRICTIONS                                                    }
'{                                                                   }
'{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
'{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
'{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
'{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
'{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
'{                                                                   }
'{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
'{   ADDITIONAL RESTRICTIONS.                                        }
'{                                                                   }
'{*******************************************************************}
#End Region
Imports DebtPlus.UI.Desktop.RPPS.Biller.Edit.BILLER
Imports DebtPlus.UI.Common
Imports System.Windows.Forms

Namespace RPPS.Biller.Edit.Templates
    Friend Class EditRecordControl

        Public Sub New()
            MyBase.New()
            InitializeComponent()
            AddHandler GridView1.DoubleClick, AddressOf GridView1_DoubleClick
            AddHandler GridView1.Click, AddressOf GridView1_Click
            AddHandler BarButtonItem_Change.ItemClick, AddressOf BarButtonItemChange_ItemClick
            AddHandler BarButtonItem_Delete.ItemClick, AddressOf BarButtonItem_Delete_ItemClick
            AddHandler BarButtonItem_Add.ItemClick, AddressOf BarButtonItem_Add_ItemClick
            AddHandler GridView1.MouseUp, AddressOf GridView1_MouseUp
            AddHandler Me.Load, AddressOf MyGridControl_Load
        End Sub

        Protected ctx As Context = Nothing

        Public Sub LoadControl(ByVal ctx As Context)
            Me.ctx = ctx
        End Sub

        ''' <summary>
        ''' Find a pointer to the data row view that is being edited
        ''' </summary>
        Protected Friend ReadOnly Property drv() As DataRowView
            Get
                Return CType(ParentForm, EditForm).drv
            End Get
        End Property

        ''' <summary>
        ''' Retrieve the rpps biller id from the edit record
        ''' </summary>
        Protected Friend ReadOnly Property rpps_biller_id() As String
            Get
                Return DebtPlus.Utils.Nulls.DStr(drv("rpps_biller_id"))
            End Get
        End Property

        ''' <summary>
        ''' Biller to be used in newly created rows
        ''' </summary>
        Protected Friend ReadOnly Property OldBiller() As String
            Get
                Return CType(ParentForm, EditForm).OldBiller
            End Get
        End Property

        ''' <summary>
        ''' Edit the values for the current row
        ''' </summary>
        Protected Overridable Sub EditRow(ByVal EditDrv As DataRowView)
        End Sub

        ''' <summary>
        ''' Create a new blank row in the database
        ''' </summary>
        Protected Overridable Sub CreateRow(ByVal vue As DataView)
        End Sub

        ''' <summary>
        ''' Delete the current row from the database
        ''' </summary>
        Protected Overridable Sub DeleteRow(ByVal EditDrv As DataRowView)
        End Sub

        ''' <summary>
        ''' Double click event on the list
        ''' </summary>
        Private Sub GridView1_DoubleClick(ByVal sender As Object, ByVal e As EventArgs)
            Dim gv As DevExpress.XtraGrid.Views.Grid.GridView = GridView1
            Dim ctl As DevExpress.XtraGrid.GridControl = gv.GridControl
            Dim hi As DevExpress.XtraGrid.Views.Grid.ViewInfo.GridHitInfo = gv.CalcHitInfo((ctl.PointToClient(Windows.Forms.Control.MousePosition)))

            Dim RowHandle As Int32 = -1
            If hi.InRow OrElse hi.InRowCell Then
                RowHandle = hi.RowHandle
            End If

            ' Find the row view in the dataview object for this row.
            Dim drv As DataRowView = Nothing
            If RowHandle >= 0 Then
                gv.FocusedRowHandle = RowHandle
                drv = CType(gv.GetRow(RowHandle), DataRowView)
            End If

            ' If the row is defined then edit the row.
            If drv IsNot Nothing Then EditRow(drv)
        End Sub

        ''' <summary>
        ''' Double click event on the list
        ''' </summary>
        Dim RowHandle As Int32 = -1
        Private Sub GridView1_Click(ByVal sender As Object, ByVal e As EventArgs)
            Dim gv As DevExpress.XtraGrid.Views.Grid.GridView = GridView1
            Dim ctl As DevExpress.XtraGrid.GridControl = gv.GridControl
            Dim hi As DevExpress.XtraGrid.Views.Grid.ViewInfo.GridHitInfo = gv.CalcHitInfo((ctl.PointToClient(Windows.Forms.Control.MousePosition)))

            RowHandle = -1
            If hi.InRow OrElse hi.InRowCell Then
                RowHandle = hi.RowHandle
            End If

            If RowHandle >= 0 AndAlso GridView1.GetRow(RowHandle) IsNot Nothing Then RowHandle = -1

            ' Enable the popup menu items according to the position clicked
            BarButtonItem_Change.Enabled = (RowHandle >= 0)
            BarButtonItem_Delete.Enabled = (RowHandle >= 0)
        End Sub

        ''' <summary>
        ''' Process the change item in the popup menu
        ''' </summary>
        Private Sub BarButtonItemChange_ItemClick(ByVal sender As Object, ByVal e As DevExpress.XtraBars.ItemClickEventArgs)

            Dim drv As DataRowView = Nothing
            If RowHandle >= 0 Then
                drv = CType(GridView1.GetRow(RowHandle), DataRowView)
            End If

            ' If the row is defined then edit the row.
            If drv IsNot Nothing Then EditRow(drv)
        End Sub

        ''' <summary>
        ''' Process the delete item in the popup menu
        ''' </summary>
        Private Sub BarButtonItem_Delete_ItemClick(ByVal sender As Object, ByVal e As DevExpress.XtraBars.ItemClickEventArgs)
            Dim drv As DataRowView = Nothing
            If RowHandle >= 0 Then
                drv = CType(GridView1.GetRow(RowHandle), DataRowView)
            End If

            ' If the row is defined then edit the row.
            If drv IsNot Nothing Then DeleteRow(drv)
        End Sub

        ''' <summary>
        ''' Process the add item in the popup menu
        ''' </summary>
        Private Sub BarButtonItem_Add_ItemClick(ByVal sender As Object, ByVal e As DevExpress.XtraBars.ItemClickEventArgs)
            CreateRow(CType(GridControl1.DataSource, DataView))
        End Sub

        ''' <summary>
        ''' Show the popup menu when the button is released
        ''' </summary>
        Private Sub GridView1_MouseUp(ByVal sender As Object, ByVal e As MouseEventArgs)
            If e.Button = Windows.Forms.MouseButtons.Right Then
                PopupMenu1.ShowPopup(Windows.Forms.Control.MousePosition)
            End If
        End Sub

        ''' <summary>
        ''' Return the directory to the saved layout information
        ''' </summary>
        Protected Overridable Function XMLBasePath() As String
            Dim BasePath As String = Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData) + System.IO.Path.DirectorySeparatorChar + "DebtPlus"
            Return System.IO.Path.Combine(BasePath, "Tables.RPPS.Billers" + System.IO.Path.PathSeparator + ParentForm.Name)
        End Function

        ''' <summary>
        ''' When loading the control, restore the layout from the file
        ''' </summary>
        Private Sub MyGridControl_Load(ByVal sender As Object, ByVal e As EventArgs)
            Dim PathName As String = XMLBasePath()
            Dim FileName As String = System.IO.Path.Combine(PathName, Name + ".Grid.xml")

            Try
                If System.IO.File.Exists(FileName) Then
                    GridView1.RestoreLayoutFromXml(FileName)
                End If
            Catch ex As System.IO.DirectoryNotFoundException
            Catch ex As System.IO.FileNotFoundException
            End Try

            ' Hook into the changes of the layout from this point
            AddHandler GridView1.Layout, AddressOf LayoutChanged
        End Sub

        ''' <summary>
        ''' If the layout is changed then update the file
        ''' </summary>
        Private Sub LayoutChanged(ByVal Sender As Object, ByVal e As EventArgs)
            Dim PathName As String = XMLBasePath()
            If Not System.IO.Directory.Exists(PathName) Then
                System.IO.Directory.CreateDirectory(PathName)
            End If

            Dim FileName As String = System.IO.Path.Combine(PathName, Name + ".Grid.xml")
            GridView1.SaveLayoutToXml(FileName)
        End Sub
    End Class
End Namespace
