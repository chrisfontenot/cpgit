#Region "Copyright 2000-2012 DebtPlus, L.L.C."
'{*******************************************************************}
'{                                                                   }
'{       DebtPlus Debt Management System                             }
'{                                                                   }
'{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
'{       ALL RIGHTS RESERVED                                         }
'{                                                                   }
'{   The entire contents of this file is protected by U.S. and       }
'{   International Copyright Laws. Unauthorized reproduction,        }
'{   reverse-engineering, and distribution of all or any portion of  }
'{   the code contained in this file is strictly prohibited and may  }
'{   result in severe civil and criminal penalties and will be       }
'{   prosecuted to the maximum extent possible under the law.        }
'{                                                                   }
'{   RESTRICTIONS                                                    }
'{                                                                   }
'{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
'{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
'{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
'{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
'{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
'{                                                                   }
'{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
'{   ADDITIONAL RESTRICTIONS.                                        }
'{                                                                   }
'{*******************************************************************}
#End Region
Imports DebtPlus.UI.Common

Namespace RPPS.Biller.Edit.BILLER
    Friend Class EditForm

        Public Sub New()
            MyBase.New()
            InitializeComponent()
            AddHandler Me.Load, AddressOf EditForm_Load
        End Sub

        Public Sub New(ByVal ctx As Context, ByVal drv As System.Data.DataRowView)
            MyBase.New(ctx, drv)
            InitializeComponent()
            AddHandler Me.Load, AddressOf EditForm_Load
        End Sub

        Dim privateOldBiller As String = "0000000000"
        Friend Property OldBiller() As String
            Get
                Return privateOldBiller
            End Get
            Set(ByVal value As String)
                privateOldBiller = value
            End Set
        End Property

        Private Sub EditForm_Load(ByVal sender As Object, ByVal e As System.EventArgs)

            MaskGridControl1.LoadControl(ctx)
            AddressGridControl1.LoadControl(ctx)
            AkaGridControl1.LoadControl(ctx)
            LobGridControl1.LoadControl(ctx)

            ' If there is a row then obtain the current row setting for the biller id
            If drv("rpps_biller_id") IsNot Nothing AndAlso drv("rpps_biller_id") IsNot System.DBNull.Value Then OldBiller = Convert.ToString(drv("rpps_biller_id"))

            ' Set the various control lists
            MaskGridControl1.ReadForm(OldBiller)
            AddressGridControl1.ReadForm(OldBiller)
            AkaGridControl1.ReadForm(OldBiller)
            LobGridControl1.ReadForm(OldBiller)

            With TextEdit_rpps_biller_id
                .DataBindings.Clear()
                .DataBindings.Add("EditValue", drv, "rpps_biller_id")
                AddHandler .EditValueChanged, AddressOf FormChanged
            End With

            With TextEdit_biller_aba
                .DataBindings.Clear()
                .DataBindings.Add("EditValue", drv, "biller_aba")
                AddHandler .EditValueChanged, AddressOf FormChanged
            End With

            With TextEdit_biller_class
                .DataBindings.Clear()
                .DataBindings.Add("EditValue", drv, "biller_class")
                AddHandler .EditValueChanged, AddressOf FormChanged
            End With

            With TextEdit_reversal_biller_id
                .DataBindings.Clear()
                .DataBindings.Add("EditValue", drv, "reversal_biller_id")
                AddHandler .EditValueChanged, AddressOf FormChanged
            End With

            With TextEdit_biller_name
                .DataBindings.Clear()
                .DataBindings.Add("EditValue", drv, "biller_name")
                AddHandler .EditValueChanged, AddressOf FormChanged
            End With

            With MemoEdit_note
                .DataBindings.Clear()
                .DataBindings.Add("EditValue", drv, "note")
                AddHandler .EditValueChanged, AddressOf FormChanged
            End With

            With DateEdit_eff_date
                .DataBindings.Clear()
                .DataBindings.Add("EditValue", drv, "effdate")
                AddHandler .EditValueChanged, AddressOf FormChanged
            End With

            With DateEdit_live_date
                .DataBindings.Clear()
                .DataBindings.Add("EditValue", drv, "livedate")
                AddHandler .EditValueChanged, AddressOf FormChanged
            End With

            With LookUpEdit_biller_type
                .Properties.DataSource = BillerTypeTable()
                .DataBindings.Clear()
                .DataBindings.Add("EditValue", drv, "biller_type")
                AddHandler .EditValueChanged, AddressOf FormChanged
            End With

            With CheckEdit_dmppayonly
                .DataBindings.Clear()
                .DataBindings.Add("EditValue", drv, "dmppayonly")
                AddHandler .EditValueChanged, AddressOf FormChanged
            End With

            With CheckEdit_payment_prenote
                .DataBindings.Clear()
                .DataBindings.Add("EditValue", drv, "payment_prenote")
                AddHandler .EditValueChanged, AddressOf FormChanged
            End With

            With CheckEdit_proposal_prenote
                .DataBindings.Clear()
                .DataBindings.Add("EditValue", drv, "proposal_prenote")
                AddHandler .EditValueChanged, AddressOf FormChanged
            End With

            With CheckEdit_send_cdd
                .DataBindings.Clear()
                .DataBindings.Add("EditValue", drv, "send_cdd")
                AddHandler .EditValueChanged, AddressOf FormChanged
            End With

            With CheckEdit_send_cdp
                .DataBindings.Clear()
                .DataBindings.Add("EditValue", drv, "send_cdp")
                AddHandler .EditValueChanged, AddressOf FormChanged
            End With

            With CheckEdit_send_cdv
                .DataBindings.Clear()
                .DataBindings.Add("EditValue", drv, "send_cdv")
                AddHandler .EditValueChanged, AddressOf FormChanged
            End With

            With CheckEdit_send_fbc
                .DataBindings.Clear()
                .DataBindings.Add("EditValue", drv, "send_fbc")
                AddHandler .EditValueChanged, AddressOf FormChanged
            End With

            With CheckEdit_send_fbd
                .DataBindings.Clear()
                .DataBindings.Add("EditValue", drv, "send_fbd")
                AddHandler .EditValueChanged, AddressOf FormChanged
            End With

            With CheckEdit_checkdigit
                .DataBindings.Clear()
                .DataBindings.Add("EditValue", drv, "checkdigit")
                AddHandler .EditValueChanged, AddressOf FormChanged
            End With

            With CheckEdit_send_cdn
                .DataBindings.Clear()
                .DataBindings.Add("EditValue", drv, "send_cdn")
                AddHandler .EditValueChanged, AddressOf FormChanged
            End With

            With CheckEdit_send_cdf
                .DataBindings.Clear()
                .DataBindings.Add("EditValue", drv, "send_cdf")
                AddHandler .EditValueChanged, AddressOf FormChanged
            End With

            With CheckEdit_guaranteed_funds
                .DataBindings.Clear()
                .DataBindings.Add("EditValue", drv, "guaranteed_funds")
                AddHandler .EditValueChanged, AddressOf FormChanged
            End With

            With CheckEdit_reqAdndaRev
                .DataBindings.Clear()
                .DataBindings.Add("EditValue", drv, "reqAdndaRev")
                AddHandler .EditValueChanged, AddressOf FormChanged
            End With

            With CheckEdit_returns_cda
                .DataBindings.Clear()
                .DataBindings.Add("EditValue", drv, "returns_cda")
                AddHandler .EditValueChanged, AddressOf FormChanged
            End With

            With CheckEdit_returns_cdm
                .DataBindings.Clear()
                .DataBindings.Add("EditValue", drv, "returns_cdm")
                AddHandler .EditValueChanged, AddressOf FormChanged
            End With

            With CheckEdit_returns_CDR
                .DataBindings.Clear()
                .DataBindings.Add("EditValue", drv, "returns_cdr")
                AddHandler .EditValueChanged, AddressOf FormChanged
            End With

            With CheckEdit_returns_cdt
                .DataBindings.Clear()
                .DataBindings.Add("EditValue", drv, "returns_cdt")
                AddHandler .EditValueChanged, AddressOf FormChanged
            End With

            With CheckEdit_returns_cdv
                .DataBindings.Clear()
                .DataBindings.Add("EditValue", drv, "returns_cdv")
                AddHandler .EditValueChanged, AddressOf FormChanged
            End With

            With CheckEdit_send_exception_pay
                .DataBindings.Clear()
                .DataBindings.Add("EditValue", drv, "send_exception_pay")
                AddHandler .EditValueChanged, AddressOf FormChanged
            End With

            With TextEdit_currency
                .DataBindings.Clear()
                .DataBindings.Add("EditValue", drv, "currency")
                AddHandler .EditValueChanged, AddressOf FormChanged
            End With

            With TextEdit_blroldname
                .DataBindings.Clear()
                .DataBindings.Add("EditValue", drv, "blroldname")
                AddHandler .EditValueChanged, AddressOf FormChanged
            End With

            SimpleButton_OK.Enabled = Not HasErrors()
        End Sub

        Private Function HasErrors() As Boolean
            If TextEdit_rpps_biller_id.EditValue Is Nothing OrElse TextEdit_rpps_biller_id.EditValue Is System.DBNull.Value Then Return True
            Return Convert.ToString(TextEdit_rpps_biller_id.EditValue) = "0000000000" OrElse Convert.ToString(TextEdit_rpps_biller_id.EditValue).Length <> 10
        End Function

        Private Sub FormChanged(ByVal Sender As Object, ByVal e As System.EventArgs)
            SimpleButton_OK.Enabled = Not HasErrors()
        End Sub

        Protected Overrides Sub SimpleButton_OK_Click(ByVal Sender As Object, ByVal e As System.EventArgs)
            MyBase.SimpleButton_OK_Click(Sender, e)

            Dim ds As System.Data.DataSet = drv.Row.Table.DataSet

            ' Find the previous items in the tables and change them to the new biller id
            Dim NewBiller As String = "0000000000"
            If drv("rpps_biller_id") IsNot Nothing AndAlso drv("rpps_biller_id") IsNot System.DBNull.Value Then NewBiller = Convert.ToString(drv("rpps_biller_id"))
            If NewBiller <> OldBiller Then

                ' Update the LOB table
                Dim vue As New System.Data.DataView(ds.Tables("rpps_lobs"), String.Format("[rpps_biller_id]='{0}'", OldBiller), String.Empty, DataViewRowState.CurrentRows)
                For Each Updatedrv As System.Data.DataRowView In vue
                    Updatedrv.BeginEdit()
                    Updatedrv("rpps_biller_id") = NewBiller
                    Updatedrv.EndEdit()
                Next

                ' Update the ADDRESSES table
                vue = New System.Data.DataView(ds.Tables("rpps_addresses"), String.Format("[rpps_biller_id]='{0}'", OldBiller), String.Empty, DataViewRowState.CurrentRows)
                For Each Updatedrv As System.Data.DataRowView In vue
                    Updatedrv.BeginEdit()
                    Updatedrv("rpps_biller_id") = NewBiller
                    Updatedrv.EndEdit()
                Next

                ' Update the MASKS table
                vue = New System.Data.DataView(ds.Tables("rpps_masks"), String.Format("[rpps_biller_id]='{0}'", OldBiller), String.Empty, DataViewRowState.CurrentRows)
                For Each Updatedrv As System.Data.DataRowView In vue
                    Updatedrv.BeginEdit()
                    Updatedrv("rpps_biller_id") = NewBiller
                    Updatedrv.EndEdit()
                Next

                ' Update the AKAS table
                vue = New System.Data.DataView(ds.Tables("rpps_akas"), String.Format("[rpps_biller_id]='{0}'", OldBiller), String.Empty, DataViewRowState.CurrentRows)
                For Each Updatedrv As System.Data.DataRowView In vue
                    Updatedrv.BeginEdit()
                    Updatedrv("rpps_biller_id") = NewBiller
                    Updatedrv.EndEdit()
                Next
            End If
        End Sub
    End Class

    Friend Module EditTable
        Friend Function BillerTypeTable() As System.Data.DataTable
            Dim tbl As New System.Data.DataTable("biller_types")
            With tbl
                .Columns.Add("biller_type", GetType(System.Int32))
                .Columns.Add("description", GetType(String))
                .PrimaryKey = New System.Data.DataColumn() {.Columns(0)}

                .Rows.Add(1, "NET")
                .Rows.Add(2, "GROSS")
                .Rows.Add(3, "MODIFIED GROSS")
                .Rows.Add(4, "DMP")
                .Rows.Add(11, "DMP/NET")
                .Rows.Add(12, "DMP/GROSS")
                .Rows.Add(13, "DMP/MODIFIED GROSS")
            End With

            Return tbl
        End Function
    End Module
End Namespace
