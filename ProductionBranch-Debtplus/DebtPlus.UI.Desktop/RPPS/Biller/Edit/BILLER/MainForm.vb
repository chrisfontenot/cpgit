#Region "Copyright 2000-2012 DebtPlus, L.L.C."
'{*******************************************************************}
'{                                                                   }
'{       DebtPlus Debt Management System                             }
'{                                                                   }
'{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
'{       ALL RIGHTS RESERVED                                         }
'{                                                                   }
'{   The entire contents of this file is protected by U.S. and       }
'{   International Copyright Laws. Unauthorized reproduction,        }
'{   reverse-engineering, and distribution of all or any portion of  }
'{   the code contained in this file is strictly prohibited and may  }
'{   result in severe civil and criminal penalties and will be       }
'{   prosecuted to the maximum extent possible under the law.        }
'{                                                                   }
'{   RESTRICTIONS                                                    }
'{                                                                   }
'{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
'{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
'{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
'{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
'{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
'{                                                                   }
'{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
'{   ADDITIONAL RESTRICTIONS.                                        }
'{                                                                   }
'{*******************************************************************}
#End Region

Imports DebtPlus.UI.Common
Imports System.Data.SqlClient
Imports System.Windows.Forms

Namespace RPPS.Biller.Edit.BILLER
    Friend Class MainForm

        ' Public Property ctx As Context = New Context()

        Public Sub New()
            MyBase.New()
            ctx = New Context()
            InitializeComponent()
            AddHandler Me.Load, AddressOf MainForm_Load
            AddHandler Me.Load, AddressOf MyGridControl_Load

            GridColumn6.GroupFormat.Format = New FormatBillerType
            GridColumn6.DisplayFormat.Format = New FormatBillerType
        End Sub

        ''' <summary>
        ''' Format the contact method to the suiltable text string
        ''' </summary>
        Private Class FormatBillerType
            Implements IFormatProvider, ICustomFormatter

            Public Function GetFormat(ByVal formatType As System.Type) As Object Implements System.IFormatProvider.GetFormat
                Return Me
            End Function

            Public Function Format(ByVal format1 As String, ByVal arg As Object, ByVal formatProvider As System.IFormatProvider) As String Implements System.ICustomFormatter.Format
                If arg Is System.DBNull.Value OrElse arg Is Nothing Then arg = 0
                Select Case Convert.ToInt32(arg)
                    Case 1
                        Return "NET"
                    Case 2
                        Return "GROSS"
                    Case 3
                        Return "MODIFIED GROSS"
                    Case 4
                        Return "DMP"
                    Case 11
                        Return "DMP/NET"
                    Case 12
                        Return "DMP/GROSS"
                    Case 13
                        Return "DMP/MODIFIED GROSS"
                    Case Else
                        Return Convert.ToInt32(arg).ToString()
                End Select
            End Function
        End Class

        ''' <summary>
        ''' Process the load event on the form
        ''' </summary>
        Private Sub MainForm_Load(ByVal sender As Object, ByVal e As System.EventArgs)
            MyBase.LoadPlacement("Tables.RPPS.Billers.MainForm")
            RefreshInformation()
        End Sub

        ''' <summary>
        ''' Refresh the data on the form
        ''' </summary>
        Protected Sub RefreshInformation()

            ' Commit any pending changes before refresing the form
            If ctx.ds.Tables.Count > 0 Then CommitChanges()

            Dim current_cursor As Cursor = System.Windows.Forms.Cursor.Current
            Try
                System.Windows.Forms.Cursor.Current = Cursors.WaitCursor
                ctx.ds.Clear()

                Dim da As New SqlClient.SqlDataAdapter(SelectCommand())
                da.Fill(ctx.ds, "rpps_biller_ids")
                RecordTable = ctx.ds.Tables("rpps_biller_ids")
                With RecordTable

                    If .PrimaryKey.GetUpperBound(0) < 0 Then .PrimaryKey = New System.Data.DataColumn() {.Columns("rpps_biller_id")}

                    .Columns("biller_type").DefaultValue = 1
                    .Columns("payment_prenote").DefaultValue = False
                    .Columns("proposal_prenote").DefaultValue = False
                    .Columns("dmppayonly").DefaultValue = False
                    .Columns("guaranteed_funds").DefaultValue = False
                    .Columns("pvt_biller_id").DefaultValue = False

                    .Columns("send_cdd").DefaultValue = False
                    .Columns("send_cdp").DefaultValue = False
                    .Columns("send_fbc").DefaultValue = False
                    .Columns("send_fbd").DefaultValue = False
                    .Columns("send_cdv").DefaultValue = False
                    .Columns("send_cdn").DefaultValue = False
                    .Columns("send_cdf").DefaultValue = False
                    .Columns("send_exception_pay").DefaultValue = False

                    .Columns("returns_cdr").DefaultValue = False
                    .Columns("returns_cdt").DefaultValue = False
                    .Columns("returns_cda").DefaultValue = False
                    .Columns("returns_cdv").DefaultValue = False
                    .Columns("returns_cdc").DefaultValue = False
                    .Columns("returns_cdm").DefaultValue = False
                    .Columns("reqAdndaRev").DefaultValue = False
                    .Columns("checkdigit").DefaultValue = False

                    .Columns("reversal_biller_id").DefaultValue = "0000000000"
                End With

                ' Bind the dataset to the control
                With GridControl1
                    .DataSource = RecordTable.DefaultView
                    .RefreshDataSource()
                End With
                GridView1.BestFitColumns()

            Catch ex As Exception
                DebtPlus.UI.Common.ErrorHandling.HandleErrors(ex, "Error retriving database information")

            Finally
                System.Windows.Forms.Cursor.Current = current_cursor
            End Try
        End Sub

        ''' <summary>
        ''' Handle the edit of the information on the form
        ''' </summary>
        Protected Overrides Function EditRow(ByVal drv As System.Data.DataRowView) As System.Windows.Forms.DialogResult
            Dim answer As System.Windows.Forms.DialogResult = Windows.Forms.DialogResult.Cancel
            Dim frm As EditForm = Nothing

            Try
                ' Start with the row in an "edit" mode
                frm = New EditForm(ctx, drv)
                With frm
                    drv.BeginEdit()
                    answer = .ShowDialog()

                    ' If the dialog was successful then write the changes to the database
                    If answer = System.Windows.Forms.DialogResult.OK Then
                        drv.EndEdit()
                        SimpleButton_SaveChanges.Enabled = Not RecordTable.HasErrors
                    End If
                End With

            Catch ex As System.Data.ConstraintException
                DebtPlus.Data.Forms.MessageBox.Show("The ID is already defined and may not be duplicated. Please use a new ID.", "Duplicated ID", MessageBoxButtons.OK, MessageBoxIcon.Error)
                answer = Windows.Forms.DialogResult.Cancel

            Catch ex As System.Data.DataException
                DebtPlus.Data.Forms.MessageBox.Show(ex.Message, "Data Exception", MessageBoxButtons.OK, MessageBoxIcon.Error)
                answer = Windows.Forms.DialogResult.Cancel

            Finally
                If drv.IsEdit Then drv.CancelEdit()
                If frm IsNot Nothing Then
                    If frm.Visible Then frm.Close()
                    If Not frm.IsDisposed Then frm.Dispose()
                End If
            End Try

            Return answer
        End Function

        ''' <summary>
        ''' Return the selection command block
        ''' </summary>
        Private Function SelectCommand() As SqlClient.SqlCommand

            Dim cmd As SqlClient.SqlCommand = New SqlCommand
            With cmd
                .Connection = New SqlConnection(DebtPlus.LINQ.SQLInfoClass.getDefault().ConnectionString)
                .CommandText = "SELECT [rpps_biller_id],[Mastercard_Key],[reversal_biller_id],[effdate],[livedate],[biller_aba],[biller_name],[biller_class],[biller_type],[currency],[payment_prenote],[proposal_prenote],[dmppayonly],[guaranteed_funds],[pvt_biller_id],[note],[send_cdp],[send_fbc],[send_fbd],[send_cdv],[send_cdd],[send_cdn],[send_cdf],[send_exception_pay],[returns_cdr],[returns_cdt],[returns_cda],[returns_cdv],[returns_cdc],[returns_cdm],[reqAdndaRev],[checkdigit],[blroldname],[date_created],[created_by] FROM rpps_biller_ids"
                .CommandType = CommandType.Text
            End With
            Return cmd
        End Function

        ''' <summary>
        ''' Commit the changes to the database
        ''' </summary>
        Protected Overrides Sub CommitChanges()
            Dim cn As SqlClient.SqlConnection = New SqlConnection(DebtPlus.LINQ.SQLInfoClass.getDefault().ConnectionString)
            Dim txn As SqlClient.SqlTransaction = Nothing

            Dim current_cursor As Cursor = System.Windows.Forms.Cursor.Current
            Try
                System.Windows.Forms.Cursor.Current = Cursors.WaitCursor
                cn.Open()
                txn = cn.BeginTransaction

                Commit_Billers_Changes(cn, txn)
                Commit_akas_Changes(cn, txn)
                Commit_lobs_Changes(cn, txn)
                Commit_masks_Changes(cn, txn)
                Commit_addresses_Changes(cn, txn)

                txn.Commit()
                txn = Nothing

                SimpleButton_SaveChanges.Enabled = False

            Finally
                If txn IsNot Nothing Then
                    Try
                        txn.Rollback()
                    Catch exRollback As System.Exception
                    End Try
                    txn.Dispose()
                    txn = Nothing
                End If

                If cn IsNot Nothing Then cn.Dispose()

                System.Windows.Forms.Cursor.Current = current_cursor
            End Try

        End Sub

#Region " rpps_biller_ids "

        ''' <summary>
        ''' Commit the changes to the biller table
        ''' </summary>
        Private Sub Commit_Billers_Changes(ByVal cn As SqlClient.SqlConnection, ByVal txn As SqlClient.SqlTransaction)

            Dim da As New SqlClient.SqlDataAdapter
            With da
                Try
                    .UpdateCommand = Update_Billers_Command(cn, txn)
                    .DeleteCommand = Delete_Billers_Command(cn, txn)
                    .InsertCommand = Insert_Billers_Command(cn, txn)

                    Dim UpdateCount As System.Int32 = da.Update(ctx.ds.Tables("rpps_biller_ids"))
                    UpdateCount += 0

                Catch ex As System.Data.SqlClient.SqlException
                    DebtPlus.UI.Common.ErrorHandling.HandleErrors(ex)
                End Try
            End With
        End Sub

        ''' <summary>
        ''' Return the insert command block
        ''' </summary>
        Protected Function Insert_Billers_Command(ByVal cn As SqlClient.SqlConnection, ByVal txn As SqlClient.SqlTransaction) As SqlClient.SqlCommand

            Dim cmd As SqlClient.SqlCommand = New SqlCommand
            With cmd
                .Connection = cn
                .Transaction = txn
                .CommandText = "INSERT INTO rpps_biller_ids ([rpps_biller_id],[Mastercard_Key],[reversal_biller_id],[effdate],[livedate],[biller_aba],[biller_name],[biller_class],[biller_type],[currency],[payment_prenote],[proposal_prenote],[dmppayonly],[guaranteed_funds],[pvt_biller_id],[note],[send_cdp],[send_fbc],[send_fbd],[send_cdv],[send_cdd],[send_cdn],[send_cdf],[send_exception_pay],[returns_cdr],[returns_cdt],[returns_cda],[returns_cdv],[returns_cdc],[returns_cdm],[reqAdndaRev],[checkdigit],[blroldname]) VALUES (@rpps_biller_id,@Mastercard_Key,@reversal_biller_id,@effdate,@livedate,@biller_aba,@biller_name,@biller_class,@biller_type,@currency,@payment_prenote,@proposal_prenote,@dmppayonly,@guaranteed_funds,@pvt_biller_id,@note,@send_cdp,@send_fbc,@send_fbd,@send_cdv,@send_cdd,@send_cdn,@send_cdf,@send_exception_pay,@returns_cdr,@returns_cdt,@returns_cda,@returns_cdv,@returns_cdc,@returns_cdm,@reqAdndaRev,@checkdigit,@blroldname)"
                .CommandType = CommandType.Text

                .Parameters.Add("@rpps_biller_id", SqlDbType.VarChar, 20, "rpps_biller_id")
                .Parameters.Add("@Mastercard_Key", SqlDbType.Int, 0, "Mastercard_Key")
                .Parameters.Add("@reversal_biller_id", SqlDbType.VarChar, 20, "reversal_biller_id")
                .Parameters.Add("@effdate", SqlDbType.DateTime, 0, "effdate")
                .Parameters.Add("@livedate", SqlDbType.DateTime, 0, "livedate")
                .Parameters.Add("@biller_aba", SqlDbType.VarChar, 20, "biller_aba")
                .Parameters.Add("@biller_name", SqlDbType.VarChar, 80, "biller_name")
                .Parameters.Add("@biller_class", SqlDbType.VarChar, 50, "biller_class")
                .Parameters.Add("@biller_type", SqlDbType.Int, 0, "biller_type")
                .Parameters.Add("@currency", SqlDbType.Int, 0, "currency")
                .Parameters.Add("@payment_prenote", SqlDbType.Bit, 0, "payment_prenote")
                .Parameters.Add("@proposal_prenote", SqlDbType.Bit, 0, "proposal_prenote")
                .Parameters.Add("@dmppayonly", SqlDbType.Bit, 0, "dmppayonly")
                .Parameters.Add("@guaranteed_funds", SqlDbType.Bit, 0, "guaranteed_funds")
                .Parameters.Add("@pvt_biller_id", SqlDbType.Bit, 0, "pvt_biller_id")
                .Parameters.Add("@note", SqlDbType.VarChar, 1024, "note")
                .Parameters.Add("@send_cdp", SqlDbType.Bit, 0, "send_cdp")
                .Parameters.Add("@send_fbc", SqlDbType.Bit, 0, "send_fbc")
                .Parameters.Add("@send_fbd", SqlDbType.Bit, 0, "send_fbd")
                .Parameters.Add("@send_cdv", SqlDbType.Bit, 0, "send_cdv")
                .Parameters.Add("@send_cdd", SqlDbType.Bit, 0, "send_cdd")
                .Parameters.Add("@send_cdn", SqlDbType.Bit, 0, "send_cdn")
                .Parameters.Add("@send_cdf", SqlDbType.Bit, 0, "send_cdf")
                .Parameters.Add("@send_exception_pay", SqlDbType.Bit, 0, "send_exception_pay")
                .Parameters.Add("@returns_cdr", SqlDbType.Bit, 0, "returns_cdr")
                .Parameters.Add("@returns_cdt", SqlDbType.Bit, 0, "returns_cdt")
                .Parameters.Add("@returns_cda", SqlDbType.Bit, 0, "returns_cda")
                .Parameters.Add("@returns_cdv", SqlDbType.Bit, 0, "returns_cdv")
                .Parameters.Add("@returns_cdc", SqlDbType.Bit, 0, "returns_cdc")
                .Parameters.Add("@returns_cdm", SqlDbType.Bit, 0, "returns_cdm")
                .Parameters.Add("@reqAdndaRev", SqlDbType.Bit, 0, "reqAdndaRev")
                .Parameters.Add("@checkdigit", SqlDbType.Bit, 0, "checkdigit")
                .Parameters.Add("@blroldname", SqlDbType.VarChar, 1024, "blroldname")
            End With
            Return cmd
        End Function

        ''' <summary>
        ''' Return the update command block
        ''' </summary>
        Protected Function Update_Billers_Command(ByVal cn As SqlClient.SqlConnection, ByVal txn As SqlClient.SqlTransaction) As SqlClient.SqlCommand

            Dim cmd As SqlClient.SqlCommand = New SqlCommand
            With cmd
                .Connection = cn
                .Transaction = txn
                .CommandText = "UPDATE rpps_biller_ids SET [rpps_biller_id]=@rpps_biller_id,[Mastercard_Key]=@Mastercard_Key,[reversal_biller_id]=@reversal_biller_id,[effdate]=@effdate,[livedate]=@livedate,[biller_aba]=@biller_aba,[biller_name]=@biller_name,[biller_class]=@biller_class,[biller_type]=@biller_type,[currency]=@currency,[payment_prenote]=@payment_prenote,[proposal_prenote]=@proposal_prenote,[dmppayonly]=@dmppayonly,[guaranteed_funds]=@guaranteed_funds,[pvt_biller_id]=@pvt_biller_id,[note]=@note,[send_cdp]=@send_cdp,[send_fbc]=@send_fbc,[send_fbd]=@send_fbd,[send_cdv]=@send_cdv,[send_cdd]=@send_cdd,[send_cdn]=@send_cdn,[send_cdf]=@send_cdf,[send_exception_pay]=@send_exception_pay,[returns_cdr]=@returns_cdr,[returns_cdt]=@returns_cdt,[returns_cda]=@returns_cda,[returns_cdv]=@returns_cdv,[returns_cdc]=@returns_cdc,[returns_cdm]=@returns_cdm,[reqAdndaRev]=@reqAdndaRev,[checkdigit]=@checkdigit,[blroldname]=@blroldname WHERE [rpps_biller_id]=@old_rpps_biller_id"
                .CommandType = CommandType.Text
                .Parameters.Add("@rpps_biller_id", SqlDbType.VarChar, 20, "rpps_biller_id")
                .Parameters.Add("@Mastercard_Key", SqlDbType.Int, 0, "Mastercard_Key")
                .Parameters.Add("@reversal_biller_id", SqlDbType.VarChar, 20, "reversal_biller_id")
                .Parameters.Add("@effdate", SqlDbType.DateTime, 0, "effdate")
                .Parameters.Add("@livedate", SqlDbType.DateTime, 0, "livedate")
                .Parameters.Add("@biller_aba", SqlDbType.VarChar, 20, "biller_aba")
                .Parameters.Add("@biller_name", SqlDbType.VarChar, 80, "biller_name")
                .Parameters.Add("@biller_class", SqlDbType.VarChar, 50, "biller_class")
                .Parameters.Add("@biller_type", SqlDbType.Int, 0, "biller_type")
                .Parameters.Add("@currency", SqlDbType.Int, 0, "currency")
                .Parameters.Add("@payment_prenote", SqlDbType.Bit, 0, "payment_prenote")
                .Parameters.Add("@proposal_prenote", SqlDbType.Bit, 0, "proposal_prenote")
                .Parameters.Add("@dmppayonly", SqlDbType.Bit, 0, "dmppayonly")
                .Parameters.Add("@guaranteed_funds", SqlDbType.Bit, 0, "guaranteed_funds")
                .Parameters.Add("@pvt_biller_id", SqlDbType.Bit, 0, "pvt_biller_id")
                .Parameters.Add("@note", SqlDbType.VarChar, 1024, "note")
                .Parameters.Add("@send_cdp", SqlDbType.Bit, 0, "send_cdp")
                .Parameters.Add("@send_fbc", SqlDbType.Bit, 0, "send_fbc")
                .Parameters.Add("@send_fbd", SqlDbType.Bit, 0, "send_fbd")
                .Parameters.Add("@send_cdv", SqlDbType.Bit, 0, "send_cdv")
                .Parameters.Add("@send_cdd", SqlDbType.Bit, 0, "send_cdd")
                .Parameters.Add("@send_cdn", SqlDbType.Bit, 0, "send_cdn")
                .Parameters.Add("@send_cdf", SqlDbType.Bit, 0, "send_cdf")
                .Parameters.Add("@send_exception_pay", SqlDbType.Bit, 0, "send_exception_pay")
                .Parameters.Add("@returns_cdr", SqlDbType.Bit, 0, "returns_cdr")
                .Parameters.Add("@returns_cdt", SqlDbType.Bit, 0, "returns_cdt")
                .Parameters.Add("@returns_cda", SqlDbType.Bit, 0, "returns_cda")
                .Parameters.Add("@returns_cdv", SqlDbType.Bit, 0, "returns_cdv")
                .Parameters.Add("@returns_cdc", SqlDbType.Bit, 0, "returns_cdc")
                .Parameters.Add("@returns_cdm", SqlDbType.Bit, 0, "returns_cdm")
                .Parameters.Add("@reqAdndaRev", SqlDbType.Bit, 0, "reqAdndaRev")
                .Parameters.Add("@checkdigit", SqlDbType.Bit, 0, "checkdigit")
                .Parameters.Add("@blroldname", SqlDbType.VarChar, 1024, "blroldname")

                .Parameters.Add(New SqlClient.SqlParameter("@old_rpps_biller_id", SqlDbType.VarChar, 80, ParameterDirection.Input, False, CByte(0), CByte(0), "rpps_biller_id", DataRowVersion.Original, Nothing))
            End With
            Return cmd

        End Function

        ''' <summary>
        ''' Return the delete command block
        ''' </summary>
        Protected Function Delete_Billers_Command(ByVal cn As SqlClient.SqlConnection, ByVal txn As SqlClient.SqlTransaction) As SqlClient.SqlCommand
            Dim cmd As SqlClient.SqlCommand = New SqlCommand
            With cmd
                .Connection = cn
                .Transaction = txn
                .CommandText = "DELETE FROM rpps_biller_ids WHERE rpps_biller_id=@rpps_biller_id;"
                .Parameters.Add("@rpps_biller_id", SqlDbType.VarChar, 80, "rpps_biller_id")
            End With
            Return cmd
        End Function
#End Region

#Region " rpps_akas "

        ''' <summary>
        ''' Commit the changes to the biller table
        ''' </summary>
        Private Sub Commit_akas_Changes(ByVal cn As SqlClient.SqlConnection, ByVal txn As SqlClient.SqlTransaction)

            If ctx.ds.Tables("rpps_akas") IsNot Nothing Then
                Dim da As New SqlClient.SqlDataAdapter
                With da

                    .UpdateCommand = Update_akas_Command(cn, txn)
                    .DeleteCommand = Delete_akas_Command(cn, txn)
                    .InsertCommand = Insert_akas_Command(cn, txn)

                    Try
                        Dim UpdateCount As System.Int32 = da.Update(ctx.ds.Tables("rpps_akas"))
                        UpdateCount += 0

                    Catch ex As System.Data.SqlClient.SqlException
                        DebtPlus.UI.Common.ErrorHandling.HandleErrors(ex, "Error updating rpps_akas table")
                    End Try
                End With
            End If
        End Sub

        ''' <summary>
        ''' Return the insert command block
        ''' </summary>
        Protected Function Insert_akas_Command(ByVal cn As SqlClient.SqlConnection, ByVal txn As SqlClient.SqlTransaction) As SqlClient.SqlCommand

            Dim cmd As SqlClient.SqlCommand = New SqlCommand
            With cmd
                .Connection = cn
                .Transaction = txn
                .CommandText = "xpr_insert_rpps_aka"
                .CommandType = CommandType.StoredProcedure
                .Parameters.Add("@RETURN_VALUE", SqlDbType.Int, 0, "rpps_aka").Direction = ParameterDirection.ReturnValue
                .Parameters.Add("@rpps_biller_id", SqlDbType.VarChar, 80, "rpps_biller_id")
                .Parameters.Add("@name", SqlDbType.VarChar, 80, "name")
            End With
            Return cmd
        End Function

        ''' <summary>
        ''' Return the update command block
        ''' </summary>
        Protected Function Update_akas_Command(ByVal cn As SqlClient.SqlConnection, ByVal txn As SqlClient.SqlTransaction) As SqlClient.SqlCommand

            Dim cmd As SqlClient.SqlCommand = New SqlCommand
            With cmd
                .Connection = cn
                .Transaction = txn
                .CommandText = "UPDATE rpps_akas SET name=@name, rpps_biller_id=@rpps_biller_id WHERE rpps_aka=@rpps_aka"
                .CommandType = CommandType.Text
                .Parameters.Add("@name", SqlDbType.VarChar, 80, "name")
                .Parameters.Add("@rpps_biller_id", SqlDbType.VarChar, 80, "rpps_biller_id")
                .Parameters.Add("@rpps_aka", SqlDbType.Int, 0, "rpps_aka")
            End With
            Return cmd

        End Function

        ''' <summary>
        ''' Return the delete command block
        ''' </summary>
        Protected Function Delete_akas_Command(ByVal cn As SqlClient.SqlConnection, ByVal txn As SqlClient.SqlTransaction) As SqlClient.SqlCommand
            Dim cmd As SqlClient.SqlCommand = New SqlCommand
            With cmd
                .Connection = cn
                .Transaction = txn
                .CommandText = "DELETE FROM rpps_akas WHERE rpps_aka=@rpps_aka"
                .Parameters.Add("@rpps_aka", SqlDbType.Int, 0, "rpps_aka")
            End With
            Return cmd
        End Function
#End Region

#Region " rpps_lobs "

        ''' <summary>
        ''' Commit the changes to the biller table
        ''' </summary>
        Private Sub Commit_lobs_Changes(ByVal cn As SqlClient.SqlConnection, ByVal txn As SqlClient.SqlTransaction)

            If ctx.ds.Tables("rpps_lobs") IsNot Nothing Then
                Dim da As New SqlClient.SqlDataAdapter()
                With da

                    .UpdateCommand = Update_lobs_Command(cn, txn)
                    .DeleteCommand = Delete_lobs_Command(cn, txn)
                    .InsertCommand = Insert_lobs_Command(cn, txn)

                    Try
                        da.Update(ctx.ds.Tables("rpps_lobs"))

                    Catch ex As System.Data.SqlClient.SqlException
                        DebtPlus.UI.Common.ErrorHandling.HandleErrors(ex, "Error updating rpps_lobs table")
                    End Try
                End With
            End If
        End Sub

        ''' <summary>
        ''' Return the insert command block
        ''' </summary>
        Protected Function Insert_lobs_Command(ByVal cn As SqlClient.SqlConnection, ByVal txn As SqlClient.SqlTransaction) As SqlClient.SqlCommand

            Dim cmd As SqlClient.SqlCommand = New SqlCommand
            With cmd
                .Connection = cn
                .Transaction = txn
                .CommandText = "xpr_insert_rpps_lob"
                .CommandType = CommandType.StoredProcedure
                .Parameters.Add("@RETURN_VALUE", SqlDbType.Int, 0, "rpps_lob").Direction = ParameterDirection.ReturnValue
                .Parameters.Add("@rpps_biller_id", SqlDbType.VarChar, 80, "rpps_biller_id")
                .Parameters.Add("@biller_lob", SqlDbType.VarChar, 80, "biller_lob")
            End With
            Return cmd
        End Function

        ''' <summary>
        ''' Return the update command block
        ''' </summary>
        Protected Function Update_lobs_Command(ByVal cn As SqlClient.SqlConnection, ByVal txn As SqlClient.SqlTransaction) As SqlClient.SqlCommand

            Dim cmd As SqlClient.SqlCommand = New SqlCommand
            With cmd
                .Connection = cn
                .Transaction = txn
                .CommandText = "UPDATE rpps_lobs SET biller_lob=@biller_lob, rpps_biller_id=@rpps_biller_id WHERE rpps_biller_lob=@rpps_biller_lob"
                .CommandType = CommandType.Text
                .Parameters.Add("@biller_lob", SqlDbType.VarChar, 80, "biller_lob")
                .Parameters.Add("@rpps_biller_id", SqlDbType.VarChar, 80, "rpps_biller_id")
                .Parameters.Add("@rpps_biller_lob", SqlDbType.Int, 0, "rpps_biller_lob")
            End With
            Return cmd

        End Function

        ''' <summary>
        ''' Return the delete command block
        ''' </summary>
        Protected Function Delete_lobs_Command(ByVal cn As SqlClient.SqlConnection, ByVal txn As SqlClient.SqlTransaction) As SqlClient.SqlCommand
            Dim cmd As SqlClient.SqlCommand = New SqlCommand
            With cmd
                .Connection = cn
                .Transaction = txn
                .CommandText = "DELETE FROM rpps_lobs WHERE rpps_biller_lob=@rpps_biller_lob"
                .Parameters.Add("@rpps_biller_lob", SqlDbType.Int, 0, "rpps_biller_lob")
            End With
            Return cmd
        End Function
#End Region

#Region " rpps_masks "

        ''' <summary>
        ''' Commit the changes to the biller table
        ''' </summary>
        Private Sub Commit_masks_Changes(ByVal cn As SqlClient.SqlConnection, ByVal txn As SqlClient.SqlTransaction)

            If ctx.ds.Tables("rpps_masks") IsNot Nothing Then
                Dim da As New SqlClient.SqlDataAdapter
                With da

                    .UpdateCommand = Update_masks_Command(cn, txn)
                    .DeleteCommand = Delete_masks_Command(cn, txn)
                    .InsertCommand = Insert_masks_Command(cn, txn)

                    Try
                        Dim UpdateCount As System.Int32 = da.Update(ctx.ds.Tables("rpps_masks"))
                        UpdateCount += 0

                    Catch ex As System.Data.SqlClient.SqlException
                        DebtPlus.UI.Common.ErrorHandling.HandleErrors(ex, "Error updating rpps_masks table")
                    End Try
                End With
            End If
        End Sub

        ''' <summary>
        ''' Return the insert command block
        ''' </summary>
        Protected Function Insert_masks_Command(ByVal cn As SqlClient.SqlConnection, ByVal txn As SqlClient.SqlTransaction) As SqlClient.SqlCommand

            Dim cmd As SqlClient.SqlCommand = New SqlCommand
            With cmd
                .Connection = cn
                .Transaction = txn
                .CommandText = "xpr_insert_rpps_mask"
                .CommandType = CommandType.StoredProcedure
                .Parameters.Add("@RETURN_VALUE", SqlDbType.Int, 0, "rpps_mask").Direction = ParameterDirection.ReturnValue
                .Parameters.Add("@rpps_biller_id", SqlDbType.VarChar, 80, "rpps_biller_id")
                .Parameters.Add("@length", SqlDbType.Int, 0, "length")
                .Parameters.Add("@mask", SqlDbType.VarChar, 80, "mask")
                .Parameters.Add("@ExceptionMask", SqlDbType.Bit, 0, "ExceptionMask")
                .Parameters.Add("@CheckDigit", SqlDbType.Bit, 0, "CheckDigit")
                .Parameters.Add("@description", SqlDbType.VarChar, 80, "description")
            End With
            Return cmd
        End Function

        ''' <summary>
        ''' Return the update command block
        ''' </summary>
        Protected Function Update_masks_Command(ByVal cn As SqlClient.SqlConnection, ByVal txn As SqlClient.SqlTransaction) As SqlClient.SqlCommand

            Dim cmd As SqlClient.SqlCommand = New SqlCommand
            With cmd
                .Connection = cn
                .Transaction = txn
                .CommandText = "UPDATE rpps_masks SET [length]=@length, [mask]=@mask, [description]=@description, [rpps_biller_id]=@rpps_biller_id, [ExceptionMask]=@ExceptionMask, [CheckDigit]=@CheckDigit WHERE [rpps_mask]=@rpps_mask"
                .CommandType = CommandType.Text
                .Parameters.Add("@length", SqlDbType.Int, 0, "length")
                .Parameters.Add("@mask", SqlDbType.VarChar, 80, "mask")
                .Parameters.Add("@description", SqlDbType.VarChar, 80, "description")
                .Parameters.Add("@ExceptionMask", SqlDbType.Bit, 0, "ExceptionMask")
                .Parameters.Add("@CheckDigit", SqlDbType.Bit, 0, "CheckDigit")
                .Parameters.Add("@rpps_biller_id", SqlDbType.VarChar, 80, "rpps_biller_id")
                .Parameters.Add("@rpps_mask", SqlDbType.Int, 0, "rpps_mask")
            End With
            Return cmd

        End Function

        ''' <summary>
        ''' Return the delete command block
        ''' </summary>
        Protected Function Delete_masks_Command(ByVal cn As SqlClient.SqlConnection, ByVal txn As SqlClient.SqlTransaction) As SqlClient.SqlCommand
            Dim cmd As SqlClient.SqlCommand = New SqlCommand
            With cmd
                .Connection = cn
                .Transaction = txn
                .CommandText = "DELETE FROM rpps_masks WHERE rpps_mask=@rpps_mask"
                .Parameters.Add("@rpps_mask", SqlDbType.Int, 0, "rpps_mask")
            End With
            Return cmd
        End Function
#End Region

#Region " rpps_addresses "

        ''' <summary>
        ''' Commit the changes to the biller table
        ''' </summary>
        Private Sub Commit_addresses_Changes(ByVal cn As SqlClient.SqlConnection, ByVal txn As SqlClient.SqlTransaction)

            If ctx.ds.Tables("rpps_addresses") IsNot Nothing Then
                Dim da As New SqlClient.SqlDataAdapter
                With da

                    .UpdateCommand = Update_addresses_Command(cn, txn)
                    .DeleteCommand = Delete_addresses_Command(cn, txn)
                    .InsertCommand = Insert_addresses_Command(cn, txn)

                    Try
                        da.Update(ctx.ds.Tables("rpps_addresses"))

                    Catch ex As System.Data.SqlClient.SqlException
                        DebtPlus.UI.Common.ErrorHandling.HandleErrors(ex, "Error updating rpps_addresses table")
                    End Try
                End With
            End If
        End Sub

        ''' <summary>
        ''' Return the insert command block
        ''' </summary>
        Protected Function Insert_addresses_Command(ByVal cn As SqlClient.SqlConnection, ByVal txn As SqlClient.SqlTransaction) As SqlClient.SqlCommand

            Dim cmd As SqlClient.SqlCommand = New SqlCommand
            With cmd
                .Connection = cn
                .Transaction = txn
                .CommandText = "INSERT INTO rpps_addresses (rpps_biller_id, biller_addr1, biller_addr2, biller_city, biller_state, biller_zipcode, biller_country) VALUES (@rpps_biller_id, @biller_addr1, @biller_addr2, @biller_city, @biller_state, @biller_zipcode, @biller_country);"
                .CommandType = CommandType.Text
                .Parameters.Add("@rpps_biller_id", SqlDbType.VarChar, 80, "rpps_biller_id")
                .Parameters.Add("@biller_addr1", SqlDbType.VarChar, 80, "biller_addr1")
                .Parameters.Add("@biller_addr2", SqlDbType.VarChar, 80, "biller_addr2")
                .Parameters.Add("@biller_city", SqlDbType.VarChar, 80, "biller_city")
                .Parameters.Add("@biller_state", SqlDbType.VarChar, 80, "biller_state")
                .Parameters.Add("@biller_zipcode", SqlDbType.VarChar, 80, "biller_zipcode")
                .Parameters.Add("@biller_country", SqlDbType.VarChar, 80, "biller_country")
            End With
            Return cmd
        End Function

        ''' <summary>
        ''' Return the update command block
        ''' </summary>
        Protected Function Update_addresses_Command(ByVal cn As SqlClient.SqlConnection, ByVal txn As SqlClient.SqlTransaction) As SqlClient.SqlCommand

            Dim cmd As SqlClient.SqlCommand = New SqlCommand
            With cmd
                .Connection = cn
                .Transaction = txn
                .CommandText = "UPDATE rpps_addresses SET biller_addr1=@biller_addr1, biller_addr2=@biller_addr2, biller_city=@biller_city, biller_state=@biller_state, biller_zipcode=@biller_zipcode, biller_country=@biller_country, rpps_biller_id=@rpps_biller_id WHERE rpps_biller_address=@rpps_biller_address"
                .CommandType = CommandType.Text
                .Parameters.Add("@biller_addr1", SqlDbType.VarChar, 80, "biller_addr1")
                .Parameters.Add("@biller_addr2", SqlDbType.VarChar, 80, "biller_addr2")
                .Parameters.Add("@biller_city", SqlDbType.VarChar, 80, "biller_city")
                .Parameters.Add("@biller_state", SqlDbType.VarChar, 80, "biller_state")
                .Parameters.Add("@biller_zipcode", SqlDbType.VarChar, 80, "biller_zipcode")
                .Parameters.Add("@biller_country", SqlDbType.VarChar, 80, "biller_country")
                .Parameters.Add("@rpps_biller_id", SqlDbType.VarChar, 80, "rpps_biller_id")
                .Parameters.Add("@rpps_biller_address", SqlDbType.Int, 0, "rpps_biller_address")
            End With
            Return cmd

        End Function

        ''' <summary>
        ''' Return the delete command block
        ''' </summary>
        Protected Function Delete_addresses_Command(ByVal cn As SqlClient.SqlConnection, ByVal txn As SqlClient.SqlTransaction) As SqlClient.SqlCommand
            Dim cmd As SqlClient.SqlCommand = New SqlCommand
            With cmd
                .Connection = cn
                .Transaction = txn
                .CommandText = "DELETE FROM rpps_addresses WHERE rpps_biller_address=@rpps_biller_address"
                .Parameters.Add("@rpps_biller_address", SqlDbType.Int, 0, "rpps_biller_address")
            End With
            Return cmd
        End Function
#End Region

        ''' <summary>
        ''' Return the directory to the saved layout information
        ''' </summary>
        Protected Overridable Function XMLBasePath() As String
            Dim BasePath As String = System.Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData) + System.IO.Path.DirectorySeparatorChar + "DebtPlus"
            Return System.IO.Path.Combine(BasePath, "Tables.RPPS.Billers.MainForm")
        End Function

        ''' <summary>
        ''' When loading the control, restore the layout from the file
        ''' </summary>
        Private Sub MyGridControl_Load(ByVal sender As Object, ByVal e As System.EventArgs)
            Dim PathName As String = XMLBasePath()
            Dim FileName As String = System.IO.Path.Combine(PathName, GridView1.Name + ".Grid.xml")

            Try
                If System.IO.File.Exists(FileName) Then
                    GridView1.RestoreLayoutFromXml(FileName)
                End If
            Catch ex As System.IO.DirectoryNotFoundException
            Catch ex As System.IO.FileNotFoundException
            End Try

            ' Hook into the changes of the layout from this point
            AddHandler GridView1.Layout, AddressOf LayoutChanged
        End Sub

        ''' <summary>
        ''' If the layout is changed then update the file
        ''' </summary>
        Private Sub LayoutChanged(ByVal Sender As Object, ByVal e As System.EventArgs)
            Dim PathName As String = XMLBasePath()
            If Not System.IO.Directory.Exists(PathName) Then
                System.IO.Directory.CreateDirectory(PathName)
            End If

            Dim FileName As String = System.IO.Path.Combine(PathName, GridView1.Name + ".Grid.xml")
            GridView1.SaveLayoutToXml(FileName)
        End Sub

        ''' <summary>
        ''' Do not allow the sub-form to ask for saving changes
        ''' </summary>
        Protected Overrides Sub MainTemplateForm_Closing(sender As Object, e As System.Windows.Forms.FormClosingEventArgs)
            CommitChanges()     ' Just save the changes.
            MyBase.MainTemplateForm_Closing(sender, e)
        End Sub
    End Class
End Namespace
