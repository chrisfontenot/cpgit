﻿Imports DebtPlus.UI.Desktop.RPPS.Biller.Edit.ADDRESS
Imports DebtPlus.UI.Desktop.RPPS.Biller.Edit.AKA
Imports DebtPlus.UI.Common.Templates
Imports DebtPlus.UI.Desktop.RPPS.Biller.Edit.LOB
Imports DebtPlus.UI.Desktop.RPPS.Biller.Edit.MASK

Namespace RPPS.Biller.Edit.BILLER

    <Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
    Partial Class EditForm
        Inherits DebtPlus.UI.Common.Templates.ADO.Net.EditTemplateForm

        'Form overrides dispose to clean up the component list.
        <System.Diagnostics.DebuggerNonUserCode()> _
        Protected Overrides Sub Dispose(ByVal disposing As Boolean)
            Try
                If disposing AndAlso components IsNot Nothing Then
                    components.Dispose()
                End If
            Finally
                MyBase.Dispose(disposing)
            End Try
        End Sub

        'Required by the Windows Form Designer
        Private components As System.ComponentModel.IContainer

        'NOTE: The following procedure is required by the Windows Form Designer
        'It can be modified using the Windows Form Designer.  
        'Do not modify it using the code editor.
        <System.Diagnostics.DebuggerStepThrough()> _
        Private Sub InitializeComponent()
            Me.LobGridControl1 = New LOBGridControl
            Me.AkaGridControl1 = New AKAGridControl
            Me.AddressGridControl1 = New AddressGridControl
            Me.MaskGridControl1 = New MaskGridControl
            Me.LayoutControl1 = New DevExpress.XtraLayout.LayoutControl
            Me.CheckEdit_send_cdf = New DevExpress.XtraEditors.CheckEdit
            Me.CheckEdit_send_cdn = New DevExpress.XtraEditors.CheckEdit
            Me.CheckEdit_checkdigit = New DevExpress.XtraEditors.CheckEdit
            Me.DateEdit_eff_date = New DevExpress.XtraEditors.DateEdit
            Me.CheckEdit_payment_prenote = New DevExpress.XtraEditors.CheckEdit
            Me.CheckEdit_send_cdv = New DevExpress.XtraEditors.CheckEdit
            Me.CheckEdit_dmppayonly = New DevExpress.XtraEditors.CheckEdit
            Me.CheckEdit_proposal_prenote = New DevExpress.XtraEditors.CheckEdit
            Me.CheckEdit_send_fbd = New DevExpress.XtraEditors.CheckEdit
            Me.CheckEdit_send_cdp = New DevExpress.XtraEditors.CheckEdit
            Me.CheckEdit_send_cdd = New DevExpress.XtraEditors.CheckEdit
            Me.XtraTabControl1 = New DevExpress.XtraTab.XtraTabControl
            Me.XtraTabPage1 = New DevExpress.XtraTab.XtraTabPage
            Me.XtraTabPage2 = New DevExpress.XtraTab.XtraTabPage
            Me.XtraTabPage3 = New DevExpress.XtraTab.XtraTabPage
            Me.XtraTabPage4 = New DevExpress.XtraTab.XtraTabPage
            Me.MemoEdit_note = New DevExpress.XtraEditors.MemoEdit
            Me.LookUpEdit_biller_type = New DevExpress.XtraEditors.LookUpEdit
            Me.TextEdit_biller_class = New DevExpress.XtraEditors.TextEdit
            Me.TextEdit_biller_name = New DevExpress.XtraEditors.TextEdit
            Me.TextEdit_reversal_biller_id = New DevExpress.XtraEditors.TextEdit
            Me.CheckEdit_send_fbc = New DevExpress.XtraEditors.CheckEdit
            Me.TextEdit_rpps_biller_id = New DevExpress.XtraEditors.TextEdit
            Me.TextEdit_biller_aba = New DevExpress.XtraEditors.TextEdit
            Me.LayoutControlGroup1 = New DevExpress.XtraLayout.LayoutControlGroup
            Me.LayoutControlItem1 = New DevExpress.XtraLayout.LayoutControlItem
            Me.LayoutControlItem3 = New DevExpress.XtraLayout.LayoutControlItem
            Me.LayoutControlItem4 = New DevExpress.XtraLayout.LayoutControlItem
            Me.LayoutControlItem2 = New DevExpress.XtraLayout.LayoutControlItem
            Me.LayoutControlItem5 = New DevExpress.XtraLayout.LayoutControlItem
            Me.LayoutControlItem6 = New DevExpress.XtraLayout.LayoutControlItem
            Me.LayoutControlItem9 = New DevExpress.XtraLayout.LayoutControlItem
            Me.LayoutControlItem12 = New DevExpress.XtraLayout.LayoutControlItem
            Me.LayoutControlItem7 = New DevExpress.XtraLayout.LayoutControlItem
            Me.LayoutControlItem8 = New DevExpress.XtraLayout.LayoutControlItem
            Me.LayoutControlItem10 = New DevExpress.XtraLayout.LayoutControlItem
            Me.LayoutControlItem11 = New DevExpress.XtraLayout.LayoutControlItem
            Me.LayoutControlItem13 = New DevExpress.XtraLayout.LayoutControlItem
            Me.LayoutControlItem14 = New DevExpress.XtraLayout.LayoutControlItem
            Me.LayoutControlItem15 = New DevExpress.XtraLayout.LayoutControlItem
            Me.LayoutControlItem16 = New DevExpress.XtraLayout.LayoutControlItem
            Me.LayoutControlItem17 = New DevExpress.XtraLayout.LayoutControlItem
            Me.LayoutControlItem18 = New DevExpress.XtraLayout.LayoutControlItem
            Me.LayoutControlItem19 = New DevExpress.XtraLayout.LayoutControlItem
            Me.LayoutControlItem20 = New DevExpress.XtraLayout.LayoutControlItem
            Me.CheckEdit_send_exception_pay = New DevExpress.XtraEditors.CheckEdit
            Me.LayoutControlItem21 = New DevExpress.XtraLayout.LayoutControlItem
            Me.CheckEdit_returns_CDR = New DevExpress.XtraEditors.CheckEdit
            Me.LayoutControlItem22 = New DevExpress.XtraLayout.LayoutControlItem
            Me.CheckEdit_reqAdndaRev = New DevExpress.XtraEditors.CheckEdit
            Me.LayoutControlItem23 = New DevExpress.XtraLayout.LayoutControlItem
            Me.CheckEdit_returns_cdm = New DevExpress.XtraEditors.CheckEdit
            Me.LayoutControlItem24 = New DevExpress.XtraLayout.LayoutControlItem
            Me.CheckEdit_returns_cdv = New DevExpress.XtraEditors.CheckEdit
            Me.LayoutControlItem25 = New DevExpress.XtraLayout.LayoutControlItem
            Me.CheckEdit_returns_cda = New DevExpress.XtraEditors.CheckEdit
            Me.LayoutControlItem26 = New DevExpress.XtraLayout.LayoutControlItem
            Me.CheckEdit_returns_cdt = New DevExpress.XtraEditors.CheckEdit
            Me.LayoutControlItem27 = New DevExpress.XtraLayout.LayoutControlItem
            Me.EmptySpaceItem1 = New DevExpress.XtraLayout.EmptySpaceItem
            Me.TextEdit_blroldname = New DevExpress.XtraEditors.TextEdit
            Me.LayoutControlItem28 = New DevExpress.XtraLayout.LayoutControlItem
            Me.CheckEdit_guaranteed_funds = New DevExpress.XtraEditors.CheckEdit
            Me.LayoutControlItem29 = New DevExpress.XtraLayout.LayoutControlItem
            Me.TextEdit_currency = New DevExpress.XtraEditors.TextEdit
            Me.LayoutControlItem30 = New DevExpress.XtraLayout.LayoutControlItem
            Me.DateEdit_live_date = New DevExpress.XtraEditors.DateEdit
            Me.LayoutControlItem31 = New DevExpress.XtraLayout.LayoutControlItem
            CType(Me.DxErrorProvider1, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.LayoutControl1, System.ComponentModel.ISupportInitialize).BeginInit()
            Me.LayoutControl1.SuspendLayout()
            CType(Me.CheckEdit_send_cdf.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.CheckEdit_send_cdn.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.CheckEdit_checkdigit.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.DateEdit_eff_date.Properties.VistaTimeProperties, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.DateEdit_eff_date.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.CheckEdit_payment_prenote.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.CheckEdit_send_cdv.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.CheckEdit_dmppayonly.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.CheckEdit_proposal_prenote.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.CheckEdit_send_fbd.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.CheckEdit_send_cdp.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.CheckEdit_send_cdd.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.XtraTabControl1, System.ComponentModel.ISupportInitialize).BeginInit()
            Me.XtraTabControl1.SuspendLayout()
            Me.XtraTabPage1.SuspendLayout()
            Me.XtraTabPage2.SuspendLayout()
            Me.XtraTabPage3.SuspendLayout()
            Me.XtraTabPage4.SuspendLayout()
            CType(Me.MemoEdit_note.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.LookUpEdit_biller_type.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.TextEdit_biller_class.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.TextEdit_biller_name.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.TextEdit_reversal_biller_id.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.CheckEdit_send_fbc.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.TextEdit_rpps_biller_id.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.TextEdit_biller_aba.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.LayoutControlGroup1, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.LayoutControlItem1, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.LayoutControlItem3, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.LayoutControlItem4, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.LayoutControlItem2, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.LayoutControlItem5, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.LayoutControlItem6, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.LayoutControlItem9, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.LayoutControlItem12, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.LayoutControlItem7, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.LayoutControlItem8, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.LayoutControlItem10, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.LayoutControlItem11, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.LayoutControlItem13, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.LayoutControlItem14, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.LayoutControlItem15, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.LayoutControlItem16, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.LayoutControlItem17, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.LayoutControlItem18, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.LayoutControlItem19, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.LayoutControlItem20, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.CheckEdit_send_exception_pay.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.LayoutControlItem21, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.CheckEdit_returns_CDR.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.LayoutControlItem22, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.CheckEdit_reqAdndaRev.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.LayoutControlItem23, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.CheckEdit_returns_cdm.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.LayoutControlItem24, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.CheckEdit_returns_cdv.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.LayoutControlItem25, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.CheckEdit_returns_cda.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.LayoutControlItem26, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.CheckEdit_returns_cdt.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.LayoutControlItem27, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.EmptySpaceItem1, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.TextEdit_blroldname.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.LayoutControlItem28, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.CheckEdit_guaranteed_funds.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.LayoutControlItem29, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.TextEdit_currency.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.LayoutControlItem30, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.DateEdit_live_date.Properties.VistaTimeProperties, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.DateEdit_live_date.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.LayoutControlItem31, System.ComponentModel.ISupportInitialize).BeginInit()
            Me.SuspendLayout()
            '
            'SimpleButton_OK
            '
            Me.SimpleButton_OK.Anchor = System.Windows.Forms.AnchorStyles.Bottom
            Me.SimpleButton_OK.Location = New System.Drawing.Point(156, 442)
            Me.SimpleButton_OK.TabIndex = 14
            '
            'SimpleButton_Cancel
            '
            Me.SimpleButton_Cancel.Anchor = System.Windows.Forms.AnchorStyles.Bottom
            Me.SimpleButton_Cancel.Location = New System.Drawing.Point(258, 442)
            Me.SimpleButton_Cancel.TabIndex = 15
            '
            'LobGridControl1
            '
            Me.LobGridControl1.Dock = System.Windows.Forms.DockStyle.Fill
            Me.LobGridControl1.Location = New System.Drawing.Point(0, 0)
            Me.LobGridControl1.Name = "LobGridControl1"
            Me.LobGridControl1.Size = New System.Drawing.Size(432, 108)
            Me.LobGridControl1.TabIndex = 16
            '
            'AkaGridControl1
            '
            Me.AkaGridControl1.Dock = System.Windows.Forms.DockStyle.Fill
            Me.AkaGridControl1.Location = New System.Drawing.Point(0, 0)
            Me.AkaGridControl1.Name = "AkaGridControl1"
            Me.AkaGridControl1.Size = New System.Drawing.Size(432, 118)
            Me.AkaGridControl1.TabIndex = 17
            '
            'AddressGridControl1
            '
            Me.AddressGridControl1.Dock = System.Windows.Forms.DockStyle.Fill
            Me.AddressGridControl1.Location = New System.Drawing.Point(0, 0)
            Me.AddressGridControl1.Name = "AddressGridControl1"
            Me.AddressGridControl1.Size = New System.Drawing.Size(432, 108)
            Me.AddressGridControl1.TabIndex = 18
            '
            'MaskGridControl1
            '
            Me.MaskGridControl1.Dock = System.Windows.Forms.DockStyle.Fill
            Me.MaskGridControl1.Location = New System.Drawing.Point(0, 0)
            Me.MaskGridControl1.Name = "MaskGridControl1"
            Me.MaskGridControl1.Size = New System.Drawing.Size(432, 108)
            Me.MaskGridControl1.TabIndex = 19
            '
            'LayoutControl1
            '
            Me.LayoutControl1.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                        Or System.Windows.Forms.AnchorStyles.Left) _
                        Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
            Me.LayoutControl1.Appearance.Control.Options.UseTextOptions = True
            Me.LayoutControl1.Appearance.Control.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
            Me.LayoutControl1.Appearance.Control.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Top
            Me.LayoutControl1.Controls.Add(Me.DateEdit_live_date)
            Me.LayoutControl1.Controls.Add(Me.TextEdit_currency)
            Me.LayoutControl1.Controls.Add(Me.CheckEdit_guaranteed_funds)
            Me.LayoutControl1.Controls.Add(Me.TextEdit_blroldname)
            Me.LayoutControl1.Controls.Add(Me.CheckEdit_returns_cdt)
            Me.LayoutControl1.Controls.Add(Me.CheckEdit_returns_cda)
            Me.LayoutControl1.Controls.Add(Me.CheckEdit_returns_cdv)
            Me.LayoutControl1.Controls.Add(Me.CheckEdit_returns_cdm)
            Me.LayoutControl1.Controls.Add(Me.CheckEdit_returns_CDR)
            Me.LayoutControl1.Controls.Add(Me.CheckEdit_send_exception_pay)
            Me.LayoutControl1.Controls.Add(Me.CheckEdit_send_cdf)
            Me.LayoutControl1.Controls.Add(Me.CheckEdit_send_cdn)
            Me.LayoutControl1.Controls.Add(Me.CheckEdit_reqAdndaRev)
            Me.LayoutControl1.Controls.Add(Me.CheckEdit_checkdigit)
            Me.LayoutControl1.Controls.Add(Me.DateEdit_eff_date)
            Me.LayoutControl1.Controls.Add(Me.CheckEdit_payment_prenote)
            Me.LayoutControl1.Controls.Add(Me.CheckEdit_send_cdv)
            Me.LayoutControl1.Controls.Add(Me.CheckEdit_dmppayonly)
            Me.LayoutControl1.Controls.Add(Me.CheckEdit_proposal_prenote)
            Me.LayoutControl1.Controls.Add(Me.CheckEdit_send_fbd)
            Me.LayoutControl1.Controls.Add(Me.CheckEdit_send_cdp)
            Me.LayoutControl1.Controls.Add(Me.CheckEdit_send_cdd)
            Me.LayoutControl1.Controls.Add(Me.XtraTabControl1)
            Me.LayoutControl1.Controls.Add(Me.MemoEdit_note)
            Me.LayoutControl1.Controls.Add(Me.LookUpEdit_biller_type)
            Me.LayoutControl1.Controls.Add(Me.TextEdit_biller_class)
            Me.LayoutControl1.Controls.Add(Me.TextEdit_biller_name)
            Me.LayoutControl1.Controls.Add(Me.TextEdit_reversal_biller_id)
            Me.LayoutControl1.Controls.Add(Me.CheckEdit_send_fbc)
            Me.LayoutControl1.Controls.Add(Me.TextEdit_rpps_biller_id)
            Me.LayoutControl1.Controls.Add(Me.TextEdit_biller_aba)
            Me.LayoutControl1.HiddenItems.AddRange(New DevExpress.XtraLayout.BaseLayoutItem() {Me.LayoutControlItem21, Me.LayoutControlItem24, Me.LayoutControlItem25, Me.LayoutControlItem26, Me.LayoutControlItem23, Me.LayoutControlItem27, Me.LayoutControlItem22, Me.LayoutControlItem28, Me.LayoutControlItem29, Me.LayoutControlItem30, Me.LayoutControlItem31})
            Me.LayoutControl1.Location = New System.Drawing.Point(12, 12)
            Me.LayoutControl1.Name = "LayoutControl1"
            Me.LayoutControl1.OptionsCustomizationForm.ShowPropertyGrid = True
            Me.LayoutControl1.Root = Me.LayoutControlGroup1
            Me.LayoutControl1.Size = New System.Drawing.Size(463, 416)
            Me.LayoutControl1.TabIndex = 20
            Me.LayoutControl1.Text = "LayoutControl1"
            '
            'CheckEdit_send_cdf
            '
            Me.CheckEdit_send_cdf.Location = New System.Drawing.Point(200, 362)
            Me.CheckEdit_send_cdf.Name = "CheckEdit_send_cdf"
            Me.CheckEdit_send_cdf.Properties.Caption = "Send CDF"
            Me.CheckEdit_send_cdf.Size = New System.Drawing.Size(90, 19)
            Me.CheckEdit_send_cdf.StyleController = Me.LayoutControl1
            Me.CheckEdit_send_cdf.TabIndex = 32
            '
            'CheckEdit_send_cdn
            '
            Me.CheckEdit_send_cdn.Location = New System.Drawing.Point(106, 385)
            Me.CheckEdit_send_cdn.Name = "CheckEdit_send_cdn"
            Me.CheckEdit_send_cdn.Properties.Caption = "Send CDN"
            Me.CheckEdit_send_cdn.Size = New System.Drawing.Size(90, 19)
            Me.CheckEdit_send_cdn.StyleController = Me.LayoutControl1
            Me.CheckEdit_send_cdn.TabIndex = 31
            '
            'CheckEdit_checkdigit
            '
            Me.CheckEdit_checkdigit.Location = New System.Drawing.Point(12, 385)
            Me.CheckEdit_checkdigit.Name = "CheckEdit_checkdigit"
            Me.CheckEdit_checkdigit.Properties.Caption = "Checkdigit"
            Me.CheckEdit_checkdigit.Size = New System.Drawing.Size(90, 19)
            Me.CheckEdit_checkdigit.StyleController = Me.LayoutControl1
            Me.CheckEdit_checkdigit.TabIndex = 30
            '
            'DateEdit_eff_date
            '
            Me.DateEdit_eff_date.EditValue = Nothing
            Me.DateEdit_eff_date.Location = New System.Drawing.Point(312, 36)
            Me.DateEdit_eff_date.Name = "DateEdit_eff_date"
            Me.DateEdit_eff_date.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
            Me.DateEdit_eff_date.Properties.VistaTimeProperties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton})
            Me.DateEdit_eff_date.Size = New System.Drawing.Size(139, 20)
            Me.DateEdit_eff_date.StyleController = Me.LayoutControl1
            Me.DateEdit_eff_date.TabIndex = 29
            '
            'CheckEdit_payment_prenote
            '
            Me.CheckEdit_payment_prenote.Location = New System.Drawing.Point(294, 362)
            Me.CheckEdit_payment_prenote.Name = "CheckEdit_payment_prenote"
            Me.CheckEdit_payment_prenote.Properties.Caption = "Paymt Prenote"
            Me.CheckEdit_payment_prenote.Size = New System.Drawing.Size(157, 19)
            Me.CheckEdit_payment_prenote.StyleController = Me.LayoutControl1
            Me.CheckEdit_payment_prenote.TabIndex = 28
            '
            'CheckEdit_send_cdv
            '
            Me.CheckEdit_send_cdv.Location = New System.Drawing.Point(106, 362)
            Me.CheckEdit_send_cdv.Name = "CheckEdit_send_cdv"
            Me.CheckEdit_send_cdv.Properties.Caption = "Send CDV"
            Me.CheckEdit_send_cdv.Size = New System.Drawing.Size(90, 19)
            Me.CheckEdit_send_cdv.StyleController = Me.LayoutControl1
            Me.CheckEdit_send_cdv.TabIndex = 27
            '
            'CheckEdit_dmppayonly
            '
            Me.CheckEdit_dmppayonly.Location = New System.Drawing.Point(294, 385)
            Me.CheckEdit_dmppayonly.Name = "CheckEdit_dmppayonly"
            Me.CheckEdit_dmppayonly.Properties.Caption = "DMP Only"
            Me.CheckEdit_dmppayonly.Size = New System.Drawing.Size(157, 19)
            Me.CheckEdit_dmppayonly.StyleController = Me.LayoutControl1
            Me.CheckEdit_dmppayonly.TabIndex = 26
            '
            'CheckEdit_proposal_prenote
            '
            Me.CheckEdit_proposal_prenote.Location = New System.Drawing.Point(294, 339)
            Me.CheckEdit_proposal_prenote.Name = "CheckEdit_proposal_prenote"
            Me.CheckEdit_proposal_prenote.Properties.Caption = "Propsl Prenote"
            Me.CheckEdit_proposal_prenote.Size = New System.Drawing.Size(157, 19)
            Me.CheckEdit_proposal_prenote.StyleController = Me.LayoutControl1
            Me.CheckEdit_proposal_prenote.TabIndex = 25
            '
            'CheckEdit_send_fbd
            '
            Me.CheckEdit_send_fbd.Location = New System.Drawing.Point(106, 339)
            Me.CheckEdit_send_fbd.Name = "CheckEdit_send_fbd"
            Me.CheckEdit_send_fbd.Properties.Caption = "Send FBD"
            Me.CheckEdit_send_fbd.Size = New System.Drawing.Size(90, 19)
            Me.CheckEdit_send_fbd.StyleController = Me.LayoutControl1
            Me.CheckEdit_send_fbd.TabIndex = 24
            '
            'CheckEdit_send_cdp
            '
            Me.CheckEdit_send_cdp.Location = New System.Drawing.Point(12, 339)
            Me.CheckEdit_send_cdp.Name = "CheckEdit_send_cdp"
            Me.CheckEdit_send_cdp.Properties.Caption = "Send CDP"
            Me.CheckEdit_send_cdp.Size = New System.Drawing.Size(90, 19)
            Me.CheckEdit_send_cdp.StyleController = Me.LayoutControl1
            Me.CheckEdit_send_cdp.TabIndex = 22
            '
            'CheckEdit_send_cdd
            '
            Me.CheckEdit_send_cdd.Location = New System.Drawing.Point(200, 339)
            Me.CheckEdit_send_cdd.Name = "CheckEdit_send_cdd"
            Me.CheckEdit_send_cdd.Properties.Caption = "Send CDD"
            Me.CheckEdit_send_cdd.Size = New System.Drawing.Size(90, 19)
            Me.CheckEdit_send_cdd.StyleController = Me.LayoutControl1
            Me.CheckEdit_send_cdd.TabIndex = 4
            '
            'XtraTabControl1
            '
            Me.XtraTabControl1.HeaderLocation = DevExpress.XtraTab.TabHeaderLocation.Bottom
            Me.XtraTabControl1.Location = New System.Drawing.Point(12, 188)
            Me.XtraTabControl1.Name = "XtraTabControl1"
            Me.XtraTabControl1.SelectedTabPage = Me.XtraTabPage1
            Me.XtraTabControl1.Size = New System.Drawing.Size(439, 147)
            Me.XtraTabControl1.TabIndex = 21
            Me.XtraTabControl1.TabPages.AddRange(New DevExpress.XtraTab.XtraTabPage() {Me.XtraTabPage1, Me.XtraTabPage2, Me.XtraTabPage3, Me.XtraTabPage4})
            '
            'XtraTabPage1
            '
            Me.XtraTabPage1.Controls.Add(Me.AkaGridControl1)
            Me.XtraTabPage1.Name = "XtraTabPage1"
            Me.XtraTabPage1.Size = New System.Drawing.Size(432, 118)
            Me.XtraTabPage1.Text = "Aliases"
            '
            'XtraTabPage2
            '
            Me.XtraTabPage2.Controls.Add(Me.MaskGridControl1)
            Me.XtraTabPage2.Name = "XtraTabPage2"
            Me.XtraTabPage2.Size = New System.Drawing.Size(432, 108)
            Me.XtraTabPage2.Text = "Masks"
            '
            'XtraTabPage3
            '
            Me.XtraTabPage3.Controls.Add(Me.LobGridControl1)
            Me.XtraTabPage3.Name = "XtraTabPage3"
            Me.XtraTabPage3.Size = New System.Drawing.Size(432, 108)
            Me.XtraTabPage3.Text = "Lines of Business"
            '
            'XtraTabPage4
            '
            Me.XtraTabPage4.Controls.Add(Me.AddressGridControl1)
            Me.XtraTabPage4.Name = "XtraTabPage4"
            Me.XtraTabPage4.Size = New System.Drawing.Size(432, 108)
            Me.XtraTabPage4.Text = "Addresses"
            '
            'MemoEdit_note
            '
            Me.MemoEdit_note.Location = New System.Drawing.Point(97, 132)
            Me.MemoEdit_note.Name = "MemoEdit_note"
            Me.MemoEdit_note.Properties.MaxLength = 1024
            Me.MemoEdit_note.Size = New System.Drawing.Size(354, 52)
            Me.MemoEdit_note.StyleController = Me.LayoutControl1
            Me.MemoEdit_note.TabIndex = 20
            '
            'LookUpEdit_biller_type
            '
            Me.LookUpEdit_biller_type.Location = New System.Drawing.Point(97, 108)
            Me.LookUpEdit_biller_type.Name = "LookUpEdit_biller_type"
            Me.LookUpEdit_biller_type.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[False]
            Me.LookUpEdit_biller_type.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
            Me.LookUpEdit_biller_type.Properties.Columns.AddRange(New DevExpress.XtraEditors.Controls.LookUpColumnInfo() {New DevExpress.XtraEditors.Controls.LookUpColumnInfo("biller_type", "ID", 20, DevExpress.Utils.FormatType.Numeric, "f0", False, DevExpress.Utils.HorzAlignment.[Default]), New DevExpress.XtraEditors.Controls.LookUpColumnInfo("description", "Description", 20, DevExpress.Utils.FormatType.None, "", True, DevExpress.Utils.HorzAlignment.[Default], DevExpress.Data.ColumnSortOrder.Ascending)})
            Me.LookUpEdit_biller_type.Properties.DisplayMember = "description"
            Me.LookUpEdit_biller_type.Properties.NullText = ""
            Me.LookUpEdit_biller_type.Properties.ShowFooter = False
            Me.LookUpEdit_biller_type.Properties.ShowHeader = False
            Me.LookUpEdit_biller_type.Properties.ValueMember = "biller_type"
            Me.LookUpEdit_biller_type.Size = New System.Drawing.Size(354, 20)
            Me.LookUpEdit_biller_type.StyleController = Me.LayoutControl1
            Me.LookUpEdit_biller_type.TabIndex = 9
            Me.LookUpEdit_biller_type.Properties.SortColumnIndex = 1
            '
            'TextEdit_biller_class
            '
            Me.TextEdit_biller_class.Location = New System.Drawing.Point(97, 84)
            Me.TextEdit_biller_class.Name = "TextEdit_biller_class"
            Me.TextEdit_biller_class.Properties.MaxLength = 50
            Me.TextEdit_biller_class.Size = New System.Drawing.Size(354, 20)
            Me.TextEdit_biller_class.StyleController = Me.LayoutControl1
            Me.TextEdit_biller_class.TabIndex = 8
            '
            'TextEdit_biller_name
            '
            Me.TextEdit_biller_name.Location = New System.Drawing.Point(97, 60)
            Me.TextEdit_biller_name.Name = "TextEdit_biller_name"
            Me.TextEdit_biller_name.Properties.MaxLength = 16
            Me.TextEdit_biller_name.Size = New System.Drawing.Size(354, 20)
            Me.TextEdit_biller_name.StyleController = Me.LayoutControl1
            Me.TextEdit_biller_name.TabIndex = 7
            '
            'TextEdit_reversal_biller_id
            '
            Me.TextEdit_reversal_biller_id.Location = New System.Drawing.Point(97, 36)
            Me.TextEdit_reversal_biller_id.Name = "TextEdit_reversal_biller_id"
            Me.TextEdit_reversal_biller_id.Properties.Mask.BeepOnError = True
            Me.TextEdit_reversal_biller_id.Properties.Mask.EditMask = "\d{10}"
            Me.TextEdit_reversal_biller_id.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.RegEx
            Me.TextEdit_reversal_biller_id.Properties.MaxLength = 10
            Me.TextEdit_reversal_biller_id.Size = New System.Drawing.Size(126, 20)
            Me.TextEdit_reversal_biller_id.StyleController = Me.LayoutControl1
            Me.TextEdit_reversal_biller_id.TabIndex = 6
            '
            'CheckEdit_send_fbc
            '
            Me.CheckEdit_send_fbc.Location = New System.Drawing.Point(12, 362)
            Me.CheckEdit_send_fbc.Name = "CheckEdit_send_fbc"
            Me.CheckEdit_send_fbc.Properties.Caption = "Send FBC"
            Me.CheckEdit_send_fbc.Size = New System.Drawing.Size(90, 19)
            Me.CheckEdit_send_fbc.StyleController = Me.LayoutControl1
            Me.CheckEdit_send_fbc.TabIndex = 23
            '
            'TextEdit_rpps_biller_id
            '
            Me.TextEdit_rpps_biller_id.Location = New System.Drawing.Point(97, 12)
            Me.TextEdit_rpps_biller_id.Name = "TextEdit_rpps_biller_id"
            Me.TextEdit_rpps_biller_id.Properties.Mask.BeepOnError = True
            Me.TextEdit_rpps_biller_id.Properties.Mask.EditMask = "\d{10}"
            Me.TextEdit_rpps_biller_id.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.RegEx
            Me.TextEdit_rpps_biller_id.Properties.MaxLength = 10
            Me.TextEdit_rpps_biller_id.Size = New System.Drawing.Size(126, 20)
            Me.TextEdit_rpps_biller_id.StyleController = Me.LayoutControl1
            Me.TextEdit_rpps_biller_id.TabIndex = 4
            '
            'TextEdit_biller_aba
            '
            Me.TextEdit_biller_aba.Location = New System.Drawing.Point(312, 12)
            Me.TextEdit_biller_aba.Name = "TextEdit_biller_aba"
            Me.TextEdit_biller_aba.Properties.Mask.BeepOnError = True
            Me.TextEdit_biller_aba.Properties.Mask.EditMask = "\d{10}"
            Me.TextEdit_biller_aba.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.RegEx
            Me.TextEdit_biller_aba.Properties.MaxLength = 10
            Me.TextEdit_biller_aba.Size = New System.Drawing.Size(139, 20)
            Me.TextEdit_biller_aba.StyleController = Me.LayoutControl1
            Me.TextEdit_biller_aba.TabIndex = 5
            '
            'LayoutControlGroup1
            '
            Me.LayoutControlGroup1.CustomizationFormText = "LayoutControlGroup1"
            Me.LayoutControlGroup1.Items.AddRange(New DevExpress.XtraLayout.BaseLayoutItem() {Me.LayoutControlItem1, Me.LayoutControlItem3, Me.LayoutControlItem4, Me.LayoutControlItem2, Me.LayoutControlItem5, Me.LayoutControlItem6, Me.LayoutControlItem9, Me.LayoutControlItem12, Me.LayoutControlItem7, Me.LayoutControlItem8, Me.LayoutControlItem10, Me.LayoutControlItem11, Me.LayoutControlItem13, Me.LayoutControlItem15, Me.LayoutControlItem16, Me.LayoutControlItem17, Me.LayoutControlItem18, Me.LayoutControlItem19, Me.LayoutControlItem14, Me.LayoutControlItem20, Me.EmptySpaceItem1})
            Me.LayoutControlGroup1.Location = New System.Drawing.Point(0, 0)
            Me.LayoutControlGroup1.Name = "LayoutControlGroup1"
            Me.LayoutControlGroup1.Size = New System.Drawing.Size(463, 416)
            Me.LayoutControlGroup1.Text = "LayoutControlGroup1"
            Me.LayoutControlGroup1.TextVisible = False
            '
            'LayoutControlItem1
            '
            Me.LayoutControlItem1.Control = Me.TextEdit_rpps_biller_id
            Me.LayoutControlItem1.CustomizationFormText = "Biller ID"
            Me.LayoutControlItem1.Location = New System.Drawing.Point(0, 0)
            Me.LayoutControlItem1.Name = "LayoutControlItem1"
            Me.LayoutControlItem1.Size = New System.Drawing.Size(215, 24)
            Me.LayoutControlItem1.Text = "Biller ID"
            Me.LayoutControlItem1.TextSize = New System.Drawing.Size(81, 13)
            '
            'LayoutControlItem3
            '
            Me.LayoutControlItem3.Control = Me.TextEdit_reversal_biller_id
            Me.LayoutControlItem3.CustomizationFormText = "Reversal Biller ID"
            Me.LayoutControlItem3.Location = New System.Drawing.Point(0, 24)
            Me.LayoutControlItem3.Name = "LayoutControlItem3"
            Me.LayoutControlItem3.Size = New System.Drawing.Size(215, 24)
            Me.LayoutControlItem3.Text = "Reversal Biller ID"
            Me.LayoutControlItem3.TextSize = New System.Drawing.Size(81, 13)
            '
            'LayoutControlItem4
            '
            Me.LayoutControlItem4.Control = Me.TextEdit_biller_name
            Me.LayoutControlItem4.CustomizationFormText = "Biller Name"
            Me.LayoutControlItem4.Location = New System.Drawing.Point(0, 48)
            Me.LayoutControlItem4.Name = "LayoutControlItem4"
            Me.LayoutControlItem4.Size = New System.Drawing.Size(443, 24)
            Me.LayoutControlItem4.Text = "Biller Name"
            Me.LayoutControlItem4.TextSize = New System.Drawing.Size(81, 13)
            '
            'LayoutControlItem2
            '
            Me.LayoutControlItem2.Control = Me.TextEdit_biller_aba
            Me.LayoutControlItem2.CustomizationFormText = "Biller ABA"
            Me.LayoutControlItem2.Location = New System.Drawing.Point(215, 0)
            Me.LayoutControlItem2.Name = "LayoutControlItem2"
            Me.LayoutControlItem2.Size = New System.Drawing.Size(228, 24)
            Me.LayoutControlItem2.Text = "Biller ABA"
            Me.LayoutControlItem2.TextSize = New System.Drawing.Size(81, 13)
            '
            'LayoutControlItem5
            '
            Me.LayoutControlItem5.Control = Me.TextEdit_biller_class
            Me.LayoutControlItem5.CustomizationFormText = "Biller Class"
            Me.LayoutControlItem5.Location = New System.Drawing.Point(0, 72)
            Me.LayoutControlItem5.Name = "LayoutControlItem5"
            Me.LayoutControlItem5.Size = New System.Drawing.Size(443, 24)
            Me.LayoutControlItem5.Text = "Biller Class"
            Me.LayoutControlItem5.TextSize = New System.Drawing.Size(81, 13)
            '
            'LayoutControlItem6
            '
            Me.LayoutControlItem6.Control = Me.LookUpEdit_biller_type
            Me.LayoutControlItem6.CustomizationFormText = "Biller Type"
            Me.LayoutControlItem6.Location = New System.Drawing.Point(0, 96)
            Me.LayoutControlItem6.Name = "LayoutControlItem6"
            Me.LayoutControlItem6.Size = New System.Drawing.Size(443, 24)
            Me.LayoutControlItem6.Text = "Biller Type"
            Me.LayoutControlItem6.TextSize = New System.Drawing.Size(81, 13)
            '
            'LayoutControlItem9
            '
            Me.LayoutControlItem9.AppearanceItemCaption.Options.UseTextOptions = True
            Me.LayoutControlItem9.AppearanceItemCaption.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Top
            Me.LayoutControlItem9.Control = Me.MemoEdit_note
            Me.LayoutControlItem9.CustomizationFormText = "Note"
            Me.LayoutControlItem9.Location = New System.Drawing.Point(0, 120)
            Me.LayoutControlItem9.Name = "LayoutControlItem9"
            Me.LayoutControlItem9.Size = New System.Drawing.Size(443, 56)
            Me.LayoutControlItem9.Text = "Note"
            Me.LayoutControlItem9.TextSize = New System.Drawing.Size(81, 13)
            '
            'LayoutControlItem12
            '
            Me.LayoutControlItem12.Control = Me.XtraTabControl1
            Me.LayoutControlItem12.CustomizationFormText = "LayoutControlItem12"
            Me.LayoutControlItem12.Location = New System.Drawing.Point(0, 176)
            Me.LayoutControlItem12.Name = "LayoutControlItem12"
            Me.LayoutControlItem12.Size = New System.Drawing.Size(443, 151)
            Me.LayoutControlItem12.Text = "LayoutControlItem12"
            Me.LayoutControlItem12.TextSize = New System.Drawing.Size(0, 0)
            Me.LayoutControlItem12.TextToControlDistance = 0
            Me.LayoutControlItem12.TextVisible = False
            '
            'LayoutControlItem7
            '
            Me.LayoutControlItem7.Control = Me.CheckEdit_send_cdd
            Me.LayoutControlItem7.CustomizationFormText = "LayoutControlItem7"
            Me.LayoutControlItem7.Location = New System.Drawing.Point(188, 327)
            Me.LayoutControlItem7.Name = "LayoutControlItem7"
            Me.LayoutControlItem7.Size = New System.Drawing.Size(94, 23)
            Me.LayoutControlItem7.Text = "LayoutControlItem7"
            Me.LayoutControlItem7.TextSize = New System.Drawing.Size(0, 0)
            Me.LayoutControlItem7.TextToControlDistance = 0
            Me.LayoutControlItem7.TextVisible = False
            '
            'LayoutControlItem8
            '
            Me.LayoutControlItem8.Control = Me.CheckEdit_send_cdp
            Me.LayoutControlItem8.CustomizationFormText = "LayoutControlItem8"
            Me.LayoutControlItem8.Location = New System.Drawing.Point(0, 327)
            Me.LayoutControlItem8.Name = "LayoutControlItem8"
            Me.LayoutControlItem8.Size = New System.Drawing.Size(94, 23)
            Me.LayoutControlItem8.Text = "LayoutControlItem8"
            Me.LayoutControlItem8.TextSize = New System.Drawing.Size(0, 0)
            Me.LayoutControlItem8.TextToControlDistance = 0
            Me.LayoutControlItem8.TextVisible = False
            '
            'LayoutControlItem10
            '
            Me.LayoutControlItem10.Control = Me.CheckEdit_send_fbc
            Me.LayoutControlItem10.CustomizationFormText = "LayoutControlItem10"
            Me.LayoutControlItem10.Location = New System.Drawing.Point(0, 350)
            Me.LayoutControlItem10.Name = "LayoutControlItem10"
            Me.LayoutControlItem10.Size = New System.Drawing.Size(94, 23)
            Me.LayoutControlItem10.Text = "LayoutControlItem10"
            Me.LayoutControlItem10.TextSize = New System.Drawing.Size(0, 0)
            Me.LayoutControlItem10.TextToControlDistance = 0
            Me.LayoutControlItem10.TextVisible = False
            '
            'LayoutControlItem11
            '
            Me.LayoutControlItem11.Control = Me.CheckEdit_send_fbd
            Me.LayoutControlItem11.CustomizationFormText = "LayoutControlItem11"
            Me.LayoutControlItem11.Location = New System.Drawing.Point(94, 327)
            Me.LayoutControlItem11.Name = "LayoutControlItem11"
            Me.LayoutControlItem11.Size = New System.Drawing.Size(94, 23)
            Me.LayoutControlItem11.Text = "LayoutControlItem11"
            Me.LayoutControlItem11.TextSize = New System.Drawing.Size(0, 0)
            Me.LayoutControlItem11.TextToControlDistance = 0
            Me.LayoutControlItem11.TextVisible = False
            '
            'LayoutControlItem13
            '
            Me.LayoutControlItem13.Control = Me.CheckEdit_proposal_prenote
            Me.LayoutControlItem13.CustomizationFormText = "LayoutControlItem13"
            Me.LayoutControlItem13.Location = New System.Drawing.Point(282, 327)
            Me.LayoutControlItem13.Name = "LayoutControlItem13"
            Me.LayoutControlItem13.Size = New System.Drawing.Size(161, 23)
            Me.LayoutControlItem13.Text = "LayoutControlItem13"
            Me.LayoutControlItem13.TextSize = New System.Drawing.Size(0, 0)
            Me.LayoutControlItem13.TextToControlDistance = 0
            Me.LayoutControlItem13.TextVisible = False
            '
            'LayoutControlItem14
            '
            Me.LayoutControlItem14.Control = Me.CheckEdit_dmppayonly
            Me.LayoutControlItem14.CustomizationFormText = "LayoutControlItem14"
            Me.LayoutControlItem14.Location = New System.Drawing.Point(282, 373)
            Me.LayoutControlItem14.Name = "LayoutControlItem14"
            Me.LayoutControlItem14.Size = New System.Drawing.Size(161, 23)
            Me.LayoutControlItem14.Text = "LayoutControlItem14"
            Me.LayoutControlItem14.TextSize = New System.Drawing.Size(0, 0)
            Me.LayoutControlItem14.TextToControlDistance = 0
            Me.LayoutControlItem14.TextVisible = False
            '
            'LayoutControlItem15
            '
            Me.LayoutControlItem15.Control = Me.CheckEdit_send_cdv
            Me.LayoutControlItem15.CustomizationFormText = "LayoutControlItem15"
            Me.LayoutControlItem15.Location = New System.Drawing.Point(94, 350)
            Me.LayoutControlItem15.Name = "LayoutControlItem15"
            Me.LayoutControlItem15.Size = New System.Drawing.Size(94, 23)
            Me.LayoutControlItem15.Text = "LayoutControlItem15"
            Me.LayoutControlItem15.TextSize = New System.Drawing.Size(0, 0)
            Me.LayoutControlItem15.TextToControlDistance = 0
            Me.LayoutControlItem15.TextVisible = False
            '
            'LayoutControlItem16
            '
            Me.LayoutControlItem16.Control = Me.CheckEdit_payment_prenote
            Me.LayoutControlItem16.CustomizationFormText = "LayoutControlItem16"
            Me.LayoutControlItem16.Location = New System.Drawing.Point(282, 350)
            Me.LayoutControlItem16.Name = "LayoutControlItem16"
            Me.LayoutControlItem16.Size = New System.Drawing.Size(161, 23)
            Me.LayoutControlItem16.Text = "LayoutControlItem16"
            Me.LayoutControlItem16.TextSize = New System.Drawing.Size(0, 0)
            Me.LayoutControlItem16.TextToControlDistance = 0
            Me.LayoutControlItem16.TextVisible = False
            '
            'LayoutControlItem17
            '
            Me.LayoutControlItem17.Control = Me.DateEdit_eff_date
            Me.LayoutControlItem17.CustomizationFormText = "Effective Date"
            Me.LayoutControlItem17.Location = New System.Drawing.Point(215, 24)
            Me.LayoutControlItem17.Name = "LayoutControlItem17"
            Me.LayoutControlItem17.Size = New System.Drawing.Size(228, 24)
            Me.LayoutControlItem17.Text = "Effective"
            Me.LayoutControlItem17.TextSize = New System.Drawing.Size(81, 13)
            '
            'LayoutControlItem18
            '
            Me.LayoutControlItem18.Control = Me.CheckEdit_checkdigit
            Me.LayoutControlItem18.CustomizationFormText = "LayoutControlItem18"
            Me.LayoutControlItem18.Location = New System.Drawing.Point(0, 373)
            Me.LayoutControlItem18.Name = "LayoutControlItem18"
            Me.LayoutControlItem18.Size = New System.Drawing.Size(94, 23)
            Me.LayoutControlItem18.Text = "LayoutControlItem18"
            Me.LayoutControlItem18.TextSize = New System.Drawing.Size(0, 0)
            Me.LayoutControlItem18.TextToControlDistance = 0
            Me.LayoutControlItem18.TextVisible = False
            Me.LayoutControlItem18.Visibility = DevExpress.XtraLayout.Utils.LayoutVisibility.Never
            '
            'LayoutControlItem19
            '
            Me.LayoutControlItem19.Control = Me.CheckEdit_send_cdn
            Me.LayoutControlItem19.CustomizationFormText = "LayoutControlItem19"
            Me.LayoutControlItem19.Location = New System.Drawing.Point(94, 373)
            Me.LayoutControlItem19.Name = "LayoutControlItem19"
            Me.LayoutControlItem19.Size = New System.Drawing.Size(94, 23)
            Me.LayoutControlItem19.Text = "LayoutControlItem19"
            Me.LayoutControlItem19.TextSize = New System.Drawing.Size(0, 0)
            Me.LayoutControlItem19.TextToControlDistance = 0
            Me.LayoutControlItem19.TextVisible = False
            '
            'LayoutControlItem20
            '
            Me.LayoutControlItem20.Control = Me.CheckEdit_send_cdf
            Me.LayoutControlItem20.CustomizationFormText = "LayoutControlItem20"
            Me.LayoutControlItem20.Location = New System.Drawing.Point(188, 350)
            Me.LayoutControlItem20.Name = "LayoutControlItem20"
            Me.LayoutControlItem20.Size = New System.Drawing.Size(94, 23)
            Me.LayoutControlItem20.Text = "LayoutControlItem20"
            Me.LayoutControlItem20.TextSize = New System.Drawing.Size(0, 0)
            Me.LayoutControlItem20.TextToControlDistance = 0
            Me.LayoutControlItem20.TextVisible = False
            '
            'CheckEdit_send_exception_pay
            '
            Me.CheckEdit_send_exception_pay.Location = New System.Drawing.Point(257, 345)
            Me.CheckEdit_send_exception_pay.Name = "CheckEdit_send_exception_pay"
            Me.CheckEdit_send_exception_pay.Properties.Caption = "Exception Pay"
            Me.CheckEdit_send_exception_pay.Size = New System.Drawing.Size(104, 19)
            Me.CheckEdit_send_exception_pay.StyleController = Me.LayoutControl1
            Me.CheckEdit_send_exception_pay.TabIndex = 33
            '
            'LayoutControlItem21
            '
            Me.LayoutControlItem21.Control = Me.CheckEdit_send_exception_pay
            Me.LayoutControlItem21.CustomizationFormText = "Send Exception Pay"
            Me.LayoutControlItem21.Location = New System.Drawing.Point(338, 333)
            Me.LayoutControlItem21.Name = "LayoutControlItem21"
            Me.LayoutControlItem21.Size = New System.Drawing.Size(108, 23)
            Me.LayoutControlItem21.Text = "Send Exception Pay"
            Me.LayoutControlItem21.TextSize = New System.Drawing.Size(0, 0)
            Me.LayoutControlItem21.TextToControlDistance = 0
            Me.LayoutControlItem21.TextVisible = False
            '
            'CheckEdit_returns_CDR
            '
            Me.CheckEdit_returns_CDR.Location = New System.Drawing.Point(12, 385)
            Me.CheckEdit_returns_CDR.Name = "CheckEdit_returns_CDR"
            Me.CheckEdit_returns_CDR.Properties.Caption = "Returns CDR"
            Me.CheckEdit_returns_CDR.Size = New System.Drawing.Size(439, 19)
            Me.CheckEdit_returns_CDR.StyleController = Me.LayoutControl1
            Me.CheckEdit_returns_CDR.TabIndex = 34
            Me.CheckEdit_returns_CDR.Visible = False
            '
            'LayoutControlItem22
            '
            Me.LayoutControlItem22.Control = Me.CheckEdit_returns_CDR
            Me.LayoutControlItem22.CustomizationFormText = "Returns CDR record"
            Me.LayoutControlItem22.Location = New System.Drawing.Point(0, 373)
            Me.LayoutControlItem22.Name = "LayoutControlItem22"
            Me.LayoutControlItem22.Size = New System.Drawing.Size(443, 23)
            Me.LayoutControlItem22.Text = "Returns CDR record"
            Me.LayoutControlItem22.TextSize = New System.Drawing.Size(0, 0)
            Me.LayoutControlItem22.TextToControlDistance = 0
            Me.LayoutControlItem22.TextVisible = False
            '
            'CheckEdit_reqAdndaRev
            '
            Me.CheckEdit_reqAdndaRev.Location = New System.Drawing.Point(189, 385)
            Me.CheckEdit_reqAdndaRev.Name = "CheckEdit_reqAdndaRev"
            Me.CheckEdit_reqAdndaRev.Properties.Caption = "Req Addenda"
            Me.CheckEdit_reqAdndaRev.Size = New System.Drawing.Size(262, 19)
            Me.CheckEdit_reqAdndaRev.StyleController = Me.LayoutControl1
            Me.CheckEdit_reqAdndaRev.TabIndex = 35
            Me.CheckEdit_reqAdndaRev.Visible = False
            '
            'LayoutControlItem23
            '
            Me.LayoutControlItem23.Control = Me.CheckEdit_reqAdndaRev
            Me.LayoutControlItem23.CustomizationFormText = "Requires Addenda Revision"
            Me.LayoutControlItem23.Location = New System.Drawing.Point(177, 373)
            Me.LayoutControlItem23.Name = "LayoutControlItem23"
            Me.LayoutControlItem23.Size = New System.Drawing.Size(266, 23)
            Me.LayoutControlItem23.Text = "Requires Addenda Revision"
            Me.LayoutControlItem23.TextSize = New System.Drawing.Size(0, 0)
            Me.LayoutControlItem23.TextToControlDistance = 0
            Me.LayoutControlItem23.TextVisible = False
            '
            'CheckEdit_returns_cdm
            '
            Me.CheckEdit_returns_cdm.Location = New System.Drawing.Point(273, 368)
            Me.CheckEdit_returns_cdm.Name = "CheckEdit_returns_cdm"
            Me.CheckEdit_returns_cdm.Properties.Caption = "Returns CDM"
            Me.CheckEdit_returns_cdm.Size = New System.Drawing.Size(86, 19)
            Me.CheckEdit_returns_cdm.StyleController = Me.LayoutControl1
            Me.CheckEdit_returns_cdm.TabIndex = 36
            Me.CheckEdit_returns_cdm.Visible = False
            '
            'LayoutControlItem24
            '
            Me.LayoutControlItem24.Control = Me.CheckEdit_returns_cdm
            Me.LayoutControlItem24.CustomizationFormText = "Returns CDM record"
            Me.LayoutControlItem24.Location = New System.Drawing.Point(354, 356)
            Me.LayoutControlItem24.Name = "LayoutControlItem24"
            Me.LayoutControlItem24.Size = New System.Drawing.Size(90, 23)
            Me.LayoutControlItem24.Text = "Returns CDM record"
            Me.LayoutControlItem24.TextSize = New System.Drawing.Size(0, 0)
            Me.LayoutControlItem24.TextToControlDistance = 0
            Me.LayoutControlItem24.TextVisible = False
            '
            'CheckEdit_returns_cdv
            '
            Me.CheckEdit_returns_cdv.Location = New System.Drawing.Point(275, 368)
            Me.CheckEdit_returns_cdv.Name = "CheckEdit_returns_cdv"
            Me.CheckEdit_returns_cdv.Properties.Caption = "Returns CDV"
            Me.CheckEdit_returns_cdv.Size = New System.Drawing.Size(84, 19)
            Me.CheckEdit_returns_cdv.StyleController = Me.LayoutControl1
            Me.CheckEdit_returns_cdv.TabIndex = 37
            Me.CheckEdit_returns_cdv.Visible = False
            '
            'LayoutControlItem25
            '
            Me.LayoutControlItem25.Control = Me.CheckEdit_returns_cdv
            Me.LayoutControlItem25.CustomizationFormText = "Returns CDV record"
            Me.LayoutControlItem25.Location = New System.Drawing.Point(266, 356)
            Me.LayoutControlItem25.Name = "LayoutControlItem25"
            Me.LayoutControlItem25.Size = New System.Drawing.Size(88, 23)
            Me.LayoutControlItem25.Text = "Returns CDV record"
            Me.LayoutControlItem25.TextSize = New System.Drawing.Size(0, 0)
            Me.LayoutControlItem25.TextToControlDistance = 0
            Me.LayoutControlItem25.TextVisible = False
            '
            'CheckEdit_returns_cda
            '
            Me.CheckEdit_returns_cda.Location = New System.Drawing.Point(189, 385)
            Me.CheckEdit_returns_cda.Name = "CheckEdit_returns_cda"
            Me.CheckEdit_returns_cda.Properties.Caption = "Returns CDA"
            Me.CheckEdit_returns_cda.Size = New System.Drawing.Size(85, 19)
            Me.CheckEdit_returns_cda.StyleController = Me.LayoutControl1
            Me.CheckEdit_returns_cda.TabIndex = 38
            Me.CheckEdit_returns_cda.Visible = False
            '
            'LayoutControlItem26
            '
            Me.LayoutControlItem26.Control = Me.CheckEdit_returns_cda
            Me.LayoutControlItem26.CustomizationFormText = "Returns CDA record"
            Me.LayoutControlItem26.Location = New System.Drawing.Point(177, 373)
            Me.LayoutControlItem26.Name = "LayoutControlItem26"
            Me.LayoutControlItem26.Size = New System.Drawing.Size(89, 23)
            Me.LayoutControlItem26.Text = "Returns CDA record"
            Me.LayoutControlItem26.TextSize = New System.Drawing.Size(0, 0)
            Me.LayoutControlItem26.TextToControlDistance = 0
            Me.LayoutControlItem26.TextVisible = False
            '
            'CheckEdit_returns_cdt
            '
            Me.CheckEdit_returns_cdt.Location = New System.Drawing.Point(101, 385)
            Me.CheckEdit_returns_cdt.Name = "CheckEdit_returns_cdt"
            Me.CheckEdit_returns_cdt.Properties.Caption = "Returns CDT"
            Me.CheckEdit_returns_cdt.Size = New System.Drawing.Size(350, 19)
            Me.CheckEdit_returns_cdt.StyleController = Me.LayoutControl1
            Me.CheckEdit_returns_cdt.TabIndex = 39
            Me.CheckEdit_returns_cdt.Visible = False
            '
            'LayoutControlItem27
            '
            Me.LayoutControlItem27.Control = Me.CheckEdit_returns_cdt
            Me.LayoutControlItem27.CustomizationFormText = "Returns CDT record"
            Me.LayoutControlItem27.Location = New System.Drawing.Point(89, 373)
            Me.LayoutControlItem27.Name = "LayoutControlItem27"
            Me.LayoutControlItem27.Size = New System.Drawing.Size(354, 23)
            Me.LayoutControlItem27.Text = "Returns CDT record"
            Me.LayoutControlItem27.TextSize = New System.Drawing.Size(0, 0)
            Me.LayoutControlItem27.TextToControlDistance = 0
            Me.LayoutControlItem27.TextVisible = False
            '
            'EmptySpaceItem1
            '
            Me.EmptySpaceItem1.CustomizationFormText = "EmptySpaceItem1"
            Me.EmptySpaceItem1.Location = New System.Drawing.Point(188, 373)
            Me.EmptySpaceItem1.Name = "EmptySpaceItem1"
            Me.EmptySpaceItem1.Size = New System.Drawing.Size(94, 23)
            Me.EmptySpaceItem1.Text = "EmptySpaceItem1"
            Me.EmptySpaceItem1.TextSize = New System.Drawing.Size(0, 0)
            '
            'TextEdit_blroldname
            '
            Me.TextEdit_blroldname.Location = New System.Drawing.Point(97, 84)
            Me.TextEdit_blroldname.Name = "TextEdit_blroldname"
            Me.TextEdit_blroldname.Properties.MaxLength = 1024
            Me.TextEdit_blroldname.Size = New System.Drawing.Size(354, 20)
            Me.TextEdit_blroldname.StyleController = Me.LayoutControl1
            Me.TextEdit_blroldname.TabIndex = 40
            '
            'LayoutControlItem28
            '
            Me.LayoutControlItem28.Control = Me.TextEdit_blroldname
            Me.LayoutControlItem28.CustomizationFormText = "Biller Old Name"
            Me.LayoutControlItem28.Location = New System.Drawing.Point(0, 72)
            Me.LayoutControlItem28.Name = "LayoutControlItem28"
            Me.LayoutControlItem28.Size = New System.Drawing.Size(443, 24)
            Me.LayoutControlItem28.Text = "Biller Old Name"
            Me.LayoutControlItem28.TextSize = New System.Drawing.Size(50, 20)
            Me.LayoutControlItem28.TextToControlDistance = 5
            '
            'CheckEdit_guaranteed_funds
            '
            Me.CheckEdit_guaranteed_funds.Location = New System.Drawing.Point(12, 60)
            Me.CheckEdit_guaranteed_funds.Name = "CheckEdit_guaranteed_funds"
            Me.CheckEdit_guaranteed_funds.Properties.Caption = "Guaranteed Funds"
            Me.CheckEdit_guaranteed_funds.Size = New System.Drawing.Size(217, 19)
            Me.CheckEdit_guaranteed_funds.StyleController = Me.LayoutControl1
            Me.CheckEdit_guaranteed_funds.TabIndex = 41
            '
            'LayoutControlItem29
            '
            Me.LayoutControlItem29.Control = Me.CheckEdit_guaranteed_funds
            Me.LayoutControlItem29.CustomizationFormText = "Guaranteed Funds"
            Me.LayoutControlItem29.Location = New System.Drawing.Point(0, 48)
            Me.LayoutControlItem29.Name = "LayoutControlItem29"
            Me.LayoutControlItem29.Size = New System.Drawing.Size(221, 24)
            Me.LayoutControlItem29.Text = "Guaranteed Funds"
            Me.LayoutControlItem29.TextSize = New System.Drawing.Size(0, 0)
            Me.LayoutControlItem29.TextToControlDistance = 0
            Me.LayoutControlItem29.TextVisible = False
            '
            'TextEdit_currency
            '
            Me.TextEdit_currency.Location = New System.Drawing.Point(97, 60)
            Me.TextEdit_currency.Name = "TextEdit_currency"
            Me.TextEdit_currency.Properties.Appearance.Options.UseTextOptions = True
            Me.TextEdit_currency.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
            Me.TextEdit_currency.Properties.DisplayFormat.FormatString = "{0:f0}"
            Me.TextEdit_currency.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
            Me.TextEdit_currency.Properties.EditFormat.FormatString = "{0:f0}"
            Me.TextEdit_currency.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric
            Me.TextEdit_currency.Properties.Mask.BeepOnError = True
            Me.TextEdit_currency.Properties.Mask.EditMask = "f0"
            Me.TextEdit_currency.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric
            Me.TextEdit_currency.Size = New System.Drawing.Size(354, 20)
            Me.TextEdit_currency.StyleController = Me.LayoutControl1
            Me.TextEdit_currency.TabIndex = 42
            '
            'LayoutControlItem30
            '
            Me.LayoutControlItem30.Control = Me.TextEdit_currency
            Me.LayoutControlItem30.CustomizationFormText = "Currency"
            Me.LayoutControlItem30.Location = New System.Drawing.Point(0, 48)
            Me.LayoutControlItem30.Name = "LayoutControlItem30"
            Me.LayoutControlItem30.Size = New System.Drawing.Size(443, 24)
            Me.LayoutControlItem30.Text = "Currency"
            Me.LayoutControlItem30.TextSize = New System.Drawing.Size(50, 20)
            Me.LayoutControlItem30.TextToControlDistance = 5
            '
            'DateEdit_live_date
            '
            Me.DateEdit_live_date.EditValue = Nothing
            Me.DateEdit_live_date.Location = New System.Drawing.Point(312, 60)
            Me.DateEdit_live_date.Name = "DateEdit_live_date"
            Me.DateEdit_live_date.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
            Me.DateEdit_live_date.Properties.VistaTimeProperties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton})
            Me.DateEdit_live_date.Size = New System.Drawing.Size(139, 20)
            Me.DateEdit_live_date.StyleController = Me.LayoutControl1
            Me.DateEdit_live_date.TabIndex = 43
            '
            'LayoutControlItem31
            '
            Me.LayoutControlItem31.Control = Me.DateEdit_live_date
            Me.LayoutControlItem31.CustomizationFormText = "Live Date"
            Me.LayoutControlItem31.Location = New System.Drawing.Point(215, 48)
            Me.LayoutControlItem31.Name = "LayoutControlItem31"
            Me.LayoutControlItem31.Size = New System.Drawing.Size(228, 24)
            Me.LayoutControlItem31.Text = "Live"
            Me.LayoutControlItem31.TextSize = New System.Drawing.Size(50, 20)
            Me.LayoutControlItem31.TextToControlDistance = 5
            '
            'EditForm
            '
            Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
            Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
            Me.ClientSize = New System.Drawing.Size(486, 480)
            Me.Controls.Add(Me.LayoutControl1)
            Me.Name = "EditForm"
            Me.Text = "RPPS Biller"
            Me.Controls.SetChildIndex(Me.SimpleButton_OK, 0)
            Me.Controls.SetChildIndex(Me.SimpleButton_Cancel, 0)
            Me.Controls.SetChildIndex(Me.LayoutControl1, 0)
            CType(Me.DxErrorProvider1, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.LayoutControl1, System.ComponentModel.ISupportInitialize).EndInit()
            Me.LayoutControl1.ResumeLayout(False)
            CType(Me.CheckEdit_send_cdf.Properties, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.CheckEdit_send_cdn.Properties, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.CheckEdit_checkdigit.Properties, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.DateEdit_eff_date.Properties.VistaTimeProperties, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.DateEdit_eff_date.Properties, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.CheckEdit_payment_prenote.Properties, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.CheckEdit_send_cdv.Properties, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.CheckEdit_dmppayonly.Properties, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.CheckEdit_proposal_prenote.Properties, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.CheckEdit_send_fbd.Properties, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.CheckEdit_send_cdp.Properties, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.CheckEdit_send_cdd.Properties, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.XtraTabControl1, System.ComponentModel.ISupportInitialize).EndInit()
            Me.XtraTabControl1.ResumeLayout(False)
            Me.XtraTabPage1.ResumeLayout(False)
            Me.XtraTabPage2.ResumeLayout(False)
            Me.XtraTabPage3.ResumeLayout(False)
            Me.XtraTabPage4.ResumeLayout(False)
            CType(Me.MemoEdit_note.Properties, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.LookUpEdit_biller_type.Properties, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.TextEdit_biller_class.Properties, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.TextEdit_biller_name.Properties, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.TextEdit_reversal_biller_id.Properties, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.CheckEdit_send_fbc.Properties, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.TextEdit_rpps_biller_id.Properties, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.TextEdit_biller_aba.Properties, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.LayoutControlGroup1, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.LayoutControlItem1, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.LayoutControlItem3, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.LayoutControlItem4, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.LayoutControlItem2, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.LayoutControlItem5, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.LayoutControlItem6, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.LayoutControlItem9, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.LayoutControlItem12, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.LayoutControlItem7, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.LayoutControlItem8, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.LayoutControlItem10, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.LayoutControlItem11, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.LayoutControlItem13, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.LayoutControlItem14, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.LayoutControlItem15, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.LayoutControlItem16, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.LayoutControlItem17, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.LayoutControlItem18, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.LayoutControlItem19, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.LayoutControlItem20, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.CheckEdit_send_exception_pay.Properties, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.LayoutControlItem21, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.CheckEdit_returns_CDR.Properties, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.LayoutControlItem22, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.CheckEdit_reqAdndaRev.Properties, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.LayoutControlItem23, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.CheckEdit_returns_cdm.Properties, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.LayoutControlItem24, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.CheckEdit_returns_cdv.Properties, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.LayoutControlItem25, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.CheckEdit_returns_cda.Properties, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.LayoutControlItem26, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.CheckEdit_returns_cdt.Properties, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.LayoutControlItem27, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.EmptySpaceItem1, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.TextEdit_blroldname.Properties, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.LayoutControlItem28, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.CheckEdit_guaranteed_funds.Properties, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.LayoutControlItem29, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.TextEdit_currency.Properties, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.LayoutControlItem30, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.DateEdit_live_date.Properties.VistaTimeProperties, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.DateEdit_live_date.Properties, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.LayoutControlItem31, System.ComponentModel.ISupportInitialize).EndInit()
            Me.ResumeLayout(False)

        End Sub
        Friend WithEvents LobGridControl1 As LOBGridControl
        Friend WithEvents AkaGridControl1 As AKAGridControl
        Friend WithEvents AddressGridControl1 As AddressGridControl
        Friend WithEvents MaskGridControl1 As MaskGridControl
        Friend WithEvents LayoutControl1 As DevExpress.XtraLayout.LayoutControl
        Friend WithEvents LayoutControlGroup1 As DevExpress.XtraLayout.LayoutControlGroup
        Friend WithEvents TextEdit_biller_class As DevExpress.XtraEditors.TextEdit
        Friend WithEvents TextEdit_biller_name As DevExpress.XtraEditors.TextEdit
        Friend WithEvents TextEdit_reversal_biller_id As DevExpress.XtraEditors.TextEdit
        Friend WithEvents TextEdit_rpps_biller_id As DevExpress.XtraEditors.TextEdit
        Friend WithEvents TextEdit_biller_aba As DevExpress.XtraEditors.TextEdit
        Friend WithEvents LayoutControlItem1 As DevExpress.XtraLayout.LayoutControlItem
        Friend WithEvents LayoutControlItem3 As DevExpress.XtraLayout.LayoutControlItem
        Friend WithEvents LayoutControlItem4 As DevExpress.XtraLayout.LayoutControlItem
        Friend WithEvents LayoutControlItem2 As DevExpress.XtraLayout.LayoutControlItem
        Friend WithEvents LayoutControlItem5 As DevExpress.XtraLayout.LayoutControlItem
        Friend WithEvents MemoEdit_note As DevExpress.XtraEditors.MemoEdit
        Friend WithEvents LookUpEdit_biller_type As DevExpress.XtraEditors.LookUpEdit
        Friend WithEvents LayoutControlItem6 As DevExpress.XtraLayout.LayoutControlItem
        Friend WithEvents LayoutControlItem9 As DevExpress.XtraLayout.LayoutControlItem
        Friend WithEvents XtraTabControl1 As DevExpress.XtraTab.XtraTabControl
        Friend WithEvents XtraTabPage1 As DevExpress.XtraTab.XtraTabPage
        Friend WithEvents XtraTabPage2 As DevExpress.XtraTab.XtraTabPage
        Friend WithEvents XtraTabPage3 As DevExpress.XtraTab.XtraTabPage
        Friend WithEvents XtraTabPage4 As DevExpress.XtraTab.XtraTabPage
        Friend WithEvents LayoutControlItem12 As DevExpress.XtraLayout.LayoutControlItem
        Friend WithEvents CheckEdit_payment_prenote As DevExpress.XtraEditors.CheckEdit
        Friend WithEvents CheckEdit_send_cdv As DevExpress.XtraEditors.CheckEdit
        Friend WithEvents CheckEdit_dmppayonly As DevExpress.XtraEditors.CheckEdit
        Friend WithEvents CheckEdit_proposal_prenote As DevExpress.XtraEditors.CheckEdit
        Friend WithEvents CheckEdit_send_fbd As DevExpress.XtraEditors.CheckEdit
        Friend WithEvents CheckEdit_send_cdp As DevExpress.XtraEditors.CheckEdit
        Friend WithEvents CheckEdit_send_cdd As DevExpress.XtraEditors.CheckEdit
        Friend WithEvents CheckEdit_send_fbc As DevExpress.XtraEditors.CheckEdit
        Friend WithEvents LayoutControlItem7 As DevExpress.XtraLayout.LayoutControlItem
        Friend WithEvents LayoutControlItem8 As DevExpress.XtraLayout.LayoutControlItem
        Friend WithEvents LayoutControlItem10 As DevExpress.XtraLayout.LayoutControlItem
        Friend WithEvents LayoutControlItem11 As DevExpress.XtraLayout.LayoutControlItem
        Friend WithEvents LayoutControlItem13 As DevExpress.XtraLayout.LayoutControlItem
        Friend WithEvents LayoutControlItem14 As DevExpress.XtraLayout.LayoutControlItem
        Friend WithEvents LayoutControlItem15 As DevExpress.XtraLayout.LayoutControlItem
        Friend WithEvents LayoutControlItem16 As DevExpress.XtraLayout.LayoutControlItem
        Friend WithEvents DateEdit_eff_date As DevExpress.XtraEditors.DateEdit
        Friend WithEvents LayoutControlItem17 As DevExpress.XtraLayout.LayoutControlItem
        Friend WithEvents CheckEdit_send_cdn As DevExpress.XtraEditors.CheckEdit
        Friend WithEvents CheckEdit_checkdigit As DevExpress.XtraEditors.CheckEdit
        Friend WithEvents LayoutControlItem18 As DevExpress.XtraLayout.LayoutControlItem
        Friend WithEvents LayoutControlItem19 As DevExpress.XtraLayout.LayoutControlItem
        Friend WithEvents CheckEdit_send_cdf As DevExpress.XtraEditors.CheckEdit
        Friend WithEvents LayoutControlItem20 As DevExpress.XtraLayout.LayoutControlItem
        Friend WithEvents CheckEdit_send_exception_pay As DevExpress.XtraEditors.CheckEdit
        Friend WithEvents LayoutControlItem21 As DevExpress.XtraLayout.LayoutControlItem
        Friend WithEvents CheckEdit_returns_cdt As DevExpress.XtraEditors.CheckEdit
        Friend WithEvents CheckEdit_returns_cda As DevExpress.XtraEditors.CheckEdit
        Friend WithEvents CheckEdit_returns_cdv As DevExpress.XtraEditors.CheckEdit
        Friend WithEvents CheckEdit_returns_cdm As DevExpress.XtraEditors.CheckEdit
        Friend WithEvents CheckEdit_reqAdndaRev As DevExpress.XtraEditors.CheckEdit
        Friend WithEvents CheckEdit_returns_CDR As DevExpress.XtraEditors.CheckEdit
        Friend WithEvents LayoutControlItem22 As DevExpress.XtraLayout.LayoutControlItem
        Friend WithEvents LayoutControlItem23 As DevExpress.XtraLayout.LayoutControlItem
        Friend WithEvents LayoutControlItem24 As DevExpress.XtraLayout.LayoutControlItem
        Friend WithEvents LayoutControlItem25 As DevExpress.XtraLayout.LayoutControlItem
        Friend WithEvents LayoutControlItem26 As DevExpress.XtraLayout.LayoutControlItem
        Friend WithEvents LayoutControlItem27 As DevExpress.XtraLayout.LayoutControlItem
        Friend WithEvents EmptySpaceItem1 As DevExpress.XtraLayout.EmptySpaceItem
        Friend WithEvents TextEdit_blroldname As DevExpress.XtraEditors.TextEdit
        Friend WithEvents LayoutControlItem28 As DevExpress.XtraLayout.LayoutControlItem
        Friend WithEvents TextEdit_currency As DevExpress.XtraEditors.TextEdit
        Friend WithEvents CheckEdit_guaranteed_funds As DevExpress.XtraEditors.CheckEdit
        Friend WithEvents LayoutControlItem29 As DevExpress.XtraLayout.LayoutControlItem
        Friend WithEvents LayoutControlItem30 As DevExpress.XtraLayout.LayoutControlItem
        Friend WithEvents DateEdit_live_date As DevExpress.XtraEditors.DateEdit
        Friend WithEvents LayoutControlItem31 As DevExpress.XtraLayout.LayoutControlItem
    End Class
End Namespace
