﻿Namespace RPPS.Biller.Edit.BILLER
    <Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
    Partial Class MainForm
        Inherits DebtPlus.UI.Common.Templates.ADO.Net.MainTemplateForm

        'Form overrides dispose to clean up the component list.
        <System.Diagnostics.DebuggerNonUserCode()> _
        Protected Overrides Sub Dispose(ByVal disposing As Boolean)
            Try
                If disposing AndAlso components IsNot Nothing Then
                    components.Dispose()
                End If
            Finally
                MyBase.Dispose(disposing)
            End Try
        End Sub

        'Required by the Windows Form Designer
        Private components As System.ComponentModel.IContainer

        'NOTE: The following procedure is required by the Windows Form Designer
        'It can be modified using the Windows Form Designer.  
        'Do not modify it using the code editor.
        <System.Diagnostics.DebuggerStepThrough()> _
        Private Sub InitializeComponent()
            Dim StyleFormatCondition1 As DevExpress.XtraGrid.StyleFormatCondition = New DevExpress.XtraGrid.StyleFormatCondition
            Dim StyleFormatCondition2 As DevExpress.XtraGrid.StyleFormatCondition = New DevExpress.XtraGrid.StyleFormatCondition
            Me.GridColumn6 = New DevExpress.XtraGrid.Columns.GridColumn
            Me.GridColumn6.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.True
            Me.GridColumn_id = New DevExpress.XtraGrid.Columns.GridColumn
            Me.GridColumn_id.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.True
            Me.GridColumn1 = New DevExpress.XtraGrid.Columns.GridColumn
            Me.GridColumn1.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.True
            Me.GridColumn2 = New DevExpress.XtraGrid.Columns.GridColumn
            Me.GridColumn2.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.True
            Me.GridColumn3 = New DevExpress.XtraGrid.Columns.GridColumn
            Me.GridColumn3.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.True
            Me.GridColumn4 = New DevExpress.XtraGrid.Columns.GridColumn
            Me.GridColumn4.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.True
            Me.GridColumn5 = New DevExpress.XtraGrid.Columns.GridColumn
            Me.GridColumn5.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.True
            Me.GridColumn7 = New DevExpress.XtraGrid.Columns.GridColumn
            Me.GridColumn7.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.True
            Me.GridColumn8 = New DevExpress.XtraGrid.Columns.GridColumn
            Me.GridColumn8.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.True
            Me.GridColumn9 = New DevExpress.XtraGrid.Columns.GridColumn
            Me.GridColumn9.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.True
            Me.GridColumn10 = New DevExpress.XtraGrid.Columns.GridColumn
            Me.GridColumn10.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.True
            Me.GridColumn11 = New DevExpress.XtraGrid.Columns.GridColumn
            Me.GridColumn11.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.True
            Me.GridColumn12 = New DevExpress.XtraGrid.Columns.GridColumn
            Me.GridColumn12.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.True
            Me.GridColumn13 = New DevExpress.XtraGrid.Columns.GridColumn
            Me.GridColumn13.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.True
            Me.GridColumn14 = New DevExpress.XtraGrid.Columns.GridColumn
            Me.GridColumn14.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.True
            Me.GridColumn15 = New DevExpress.XtraGrid.Columns.GridColumn
            Me.GridColumn15.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.True
            Me.GridColumn16 = New DevExpress.XtraGrid.Columns.GridColumn
            Me.GridColumn16.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.True
            Me.GridColumn17 = New DevExpress.XtraGrid.Columns.GridColumn
            Me.GridColumn17.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.True
            Me.GridColumn18 = New DevExpress.XtraGrid.Columns.GridColumn
            Me.GridColumn18.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.True
            CType(Me.GridControl1, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.GridView1, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.DxErrorProvider1, System.ComponentModel.ISupportInitialize).BeginInit()
            Me.SuspendLayout()
            '
            'GridControl1
            '
            Me.GridControl1.EmbeddedNavigator.Name = ""
            '
            'GridView1
            '
            Me.GridView1.Appearance.ColumnFilterButton.BackColor = System.Drawing.Color.FromArgb(CType(CType(104, Byte), Int32), CType(CType(184, Byte), Int32), CType(CType(251, Byte), Int32))
            Me.GridView1.Appearance.ColumnFilterButton.BorderColor = System.Drawing.Color.FromArgb(CType(CType(104, Byte), Int32), CType(CType(184, Byte), Int32), CType(CType(251, Byte), Int32))
            Me.GridView1.Appearance.ColumnFilterButton.ForeColor = System.Drawing.Color.White
            Me.GridView1.Appearance.ColumnFilterButton.Options.UseBackColor = True
            Me.GridView1.Appearance.ColumnFilterButton.Options.UseBorderColor = True
            Me.GridView1.Appearance.ColumnFilterButton.Options.UseForeColor = True
            Me.GridView1.Appearance.ColumnFilterButtonActive.BackColor = System.Drawing.Color.FromArgb(CType(CType(170, Byte), Int32), CType(CType(216, Byte), Int32), CType(CType(254, Byte), Int32))
            Me.GridView1.Appearance.ColumnFilterButtonActive.BorderColor = System.Drawing.Color.FromArgb(CType(CType(170, Byte), Int32), CType(CType(216, Byte), Int32), CType(CType(254, Byte), Int32))
            Me.GridView1.Appearance.ColumnFilterButtonActive.ForeColor = System.Drawing.Color.Black
            Me.GridView1.Appearance.ColumnFilterButtonActive.Options.UseBackColor = True
            Me.GridView1.Appearance.ColumnFilterButtonActive.Options.UseBorderColor = True
            Me.GridView1.Appearance.ColumnFilterButtonActive.Options.UseForeColor = True
            Me.GridView1.Appearance.Empty.BackColor = System.Drawing.Color.FromArgb(CType(CType(236, Byte), Int32), CType(CType(246, Byte), Int32), CType(CType(255, Byte), Int32))
            Me.GridView1.Appearance.Empty.BackColor2 = System.Drawing.Color.White
            Me.GridView1.Appearance.Empty.Options.UseBackColor = True
            Me.GridView1.Appearance.EvenRow.BackColor = System.Drawing.Color.FromArgb(CType(CType(247, Byte), Int32), CType(CType(251, Byte), Int32), CType(CType(255, Byte), Int32))
            Me.GridView1.Appearance.EvenRow.BorderColor = System.Drawing.Color.FromArgb(CType(CType(247, Byte), Int32), CType(CType(251, Byte), Int32), CType(CType(255, Byte), Int32))
            Me.GridView1.Appearance.EvenRow.ForeColor = System.Drawing.Color.Black
            Me.GridView1.Appearance.EvenRow.Options.UseBackColor = True
            Me.GridView1.Appearance.EvenRow.Options.UseBorderColor = True
            Me.GridView1.Appearance.EvenRow.Options.UseForeColor = True
            Me.GridView1.Appearance.FilterCloseButton.BackColor = System.Drawing.Color.FromArgb(CType(CType(104, Byte), Int32), CType(CType(184, Byte), Int32), CType(CType(251, Byte), Int32))
            Me.GridView1.Appearance.FilterCloseButton.BorderColor = System.Drawing.Color.FromArgb(CType(CType(104, Byte), Int32), CType(CType(184, Byte), Int32), CType(CType(251, Byte), Int32))
            Me.GridView1.Appearance.FilterCloseButton.ForeColor = System.Drawing.Color.White
            Me.GridView1.Appearance.FilterCloseButton.Options.UseBackColor = True
            Me.GridView1.Appearance.FilterCloseButton.Options.UseBorderColor = True
            Me.GridView1.Appearance.FilterCloseButton.Options.UseForeColor = True
            Me.GridView1.Appearance.FilterPanel.BackColor = System.Drawing.Color.FromArgb(CType(CType(236, Byte), Int32), CType(CType(246, Byte), Int32), CType(CType(255, Byte), Int32))
            Me.GridView1.Appearance.FilterPanel.BackColor2 = System.Drawing.Color.White
            Me.GridView1.Appearance.FilterPanel.ForeColor = System.Drawing.Color.Black
            Me.GridView1.Appearance.FilterPanel.Options.UseBackColor = True
            Me.GridView1.Appearance.FilterPanel.Options.UseForeColor = True
            Me.GridView1.Appearance.FixedLine.BackColor = System.Drawing.Color.FromArgb(CType(CType(59, Byte), Int32), CType(CType(133, Byte), Int32), CType(CType(195, Byte), Int32))
            Me.GridView1.Appearance.FixedLine.Options.UseBackColor = True
            Me.GridView1.Appearance.FocusedCell.BackColor = System.Drawing.Color.White
            Me.GridView1.Appearance.FocusedCell.ForeColor = System.Drawing.Color.Black
            Me.GridView1.Appearance.FocusedCell.Options.UseBackColor = True
            Me.GridView1.Appearance.FocusedCell.Options.UseForeColor = True
            Me.GridView1.Appearance.FocusedRow.BackColor = System.Drawing.Color.FromArgb(CType(CType(38, Byte), Int32), CType(CType(109, Byte), Int32), CType(CType(189, Byte), Int32))
            Me.GridView1.Appearance.FocusedRow.BorderColor = System.Drawing.Color.FromArgb(CType(CType(59, Byte), Int32), CType(CType(139, Byte), Int32), CType(CType(206, Byte), Int32))
            Me.GridView1.Appearance.FocusedRow.ForeColor = System.Drawing.Color.White
            Me.GridView1.Appearance.FocusedRow.Options.UseBackColor = True
            Me.GridView1.Appearance.FocusedRow.Options.UseBorderColor = True
            Me.GridView1.Appearance.FocusedRow.Options.UseForeColor = True
            Me.GridView1.Appearance.FooterPanel.BackColor = System.Drawing.Color.FromArgb(CType(CType(104, Byte), Int32), CType(CType(184, Byte), Int32), CType(CType(251, Byte), Int32))
            Me.GridView1.Appearance.FooterPanel.BorderColor = System.Drawing.Color.FromArgb(CType(CType(104, Byte), Int32), CType(CType(184, Byte), Int32), CType(CType(251, Byte), Int32))
            Me.GridView1.Appearance.FooterPanel.ForeColor = System.Drawing.Color.Black
            Me.GridView1.Appearance.FooterPanel.Options.UseBackColor = True
            Me.GridView1.Appearance.FooterPanel.Options.UseBorderColor = True
            Me.GridView1.Appearance.FooterPanel.Options.UseForeColor = True
            Me.GridView1.Appearance.GroupButton.BackColor = System.Drawing.Color.FromArgb(CType(CType(104, Byte), Int32), CType(CType(184, Byte), Int32), CType(CType(251, Byte), Int32))
            Me.GridView1.Appearance.GroupButton.BorderColor = System.Drawing.Color.FromArgb(CType(CType(104, Byte), Int32), CType(CType(184, Byte), Int32), CType(CType(251, Byte), Int32))
            Me.GridView1.Appearance.GroupButton.Options.UseBackColor = True
            Me.GridView1.Appearance.GroupButton.Options.UseBorderColor = True
            Me.GridView1.Appearance.GroupFooter.BackColor = System.Drawing.Color.FromArgb(CType(CType(170, Byte), Int32), CType(CType(216, Byte), Int32), CType(CType(254, Byte), Int32))
            Me.GridView1.Appearance.GroupFooter.BorderColor = System.Drawing.Color.FromArgb(CType(CType(170, Byte), Int32), CType(CType(216, Byte), Int32), CType(CType(254, Byte), Int32))
            Me.GridView1.Appearance.GroupFooter.ForeColor = System.Drawing.Color.Black
            Me.GridView1.Appearance.GroupFooter.Options.UseBackColor = True
            Me.GridView1.Appearance.GroupFooter.Options.UseBorderColor = True
            Me.GridView1.Appearance.GroupFooter.Options.UseForeColor = True
            Me.GridView1.Appearance.GroupPanel.BackColor = System.Drawing.Color.FromArgb(CType(CType(236, Byte), Int32), CType(CType(246, Byte), Int32), CType(CType(255, Byte), Int32))
            Me.GridView1.Appearance.GroupPanel.BackColor2 = System.Drawing.Color.White
            Me.GridView1.Appearance.GroupPanel.ForeColor = System.Drawing.Color.Black
            Me.GridView1.Appearance.GroupPanel.Options.UseBackColor = True
            Me.GridView1.Appearance.GroupPanel.Options.UseForeColor = True
            Me.GridView1.Appearance.GroupRow.BackColor = System.Drawing.Color.FromArgb(CType(CType(170, Byte), Int32), CType(CType(216, Byte), Int32), CType(CType(254, Byte), Int32))
            Me.GridView1.Appearance.GroupRow.BorderColor = System.Drawing.Color.FromArgb(CType(CType(170, Byte), Int32), CType(CType(216, Byte), Int32), CType(CType(254, Byte), Int32))
            Me.GridView1.Appearance.GroupRow.ForeColor = System.Drawing.Color.Black
            Me.GridView1.Appearance.GroupRow.Options.UseBackColor = True
            Me.GridView1.Appearance.GroupRow.Options.UseBorderColor = True
            Me.GridView1.Appearance.GroupRow.Options.UseForeColor = True
            Me.GridView1.Appearance.HeaderPanel.BackColor = System.Drawing.Color.FromArgb(CType(CType(139, Byte), Int32), CType(CType(201, Byte), Int32), CType(CType(254, Byte), Int32))
            Me.GridView1.Appearance.HeaderPanel.BorderColor = System.Drawing.Color.FromArgb(CType(CType(139, Byte), Int32), CType(CType(201, Byte), Int32), CType(CType(254, Byte), Int32))
            Me.GridView1.Appearance.HeaderPanel.ForeColor = System.Drawing.Color.Black
            Me.GridView1.Appearance.HeaderPanel.Options.UseBackColor = True
            Me.GridView1.Appearance.HeaderPanel.Options.UseBorderColor = True
            Me.GridView1.Appearance.HeaderPanel.Options.UseForeColor = True
            Me.GridView1.Appearance.HideSelectionRow.BackColor = System.Drawing.Color.FromArgb(CType(CType(105, Byte), Int32), CType(CType(170, Byte), Int32), CType(CType(225, Byte), Int32))
            Me.GridView1.Appearance.HideSelectionRow.BorderColor = System.Drawing.Color.FromArgb(CType(CType(83, Byte), Int32), CType(CType(155, Byte), Int32), CType(CType(215, Byte), Int32))
            Me.GridView1.Appearance.HideSelectionRow.ForeColor = System.Drawing.Color.FromArgb(CType(CType(236, Byte), Int32), CType(CType(246, Byte), Int32), CType(CType(255, Byte), Int32))
            Me.GridView1.Appearance.HideSelectionRow.Options.UseBackColor = True
            Me.GridView1.Appearance.HideSelectionRow.Options.UseBorderColor = True
            Me.GridView1.Appearance.HideSelectionRow.Options.UseForeColor = True
            Me.GridView1.Appearance.HorzLine.BackColor = System.Drawing.Color.FromArgb(CType(CType(104, Byte), Int32), CType(CType(184, Byte), Int32), CType(CType(251, Byte), Int32))
            Me.GridView1.Appearance.HorzLine.Options.UseBackColor = True
            Me.GridView1.Appearance.OddRow.BackColor = System.Drawing.Color.FromArgb(CType(CType(236, Byte), Int32), CType(CType(246, Byte), Int32), CType(CType(255, Byte), Int32))
            Me.GridView1.Appearance.OddRow.BorderColor = System.Drawing.Color.FromArgb(CType(CType(236, Byte), Int32), CType(CType(246, Byte), Int32), CType(CType(255, Byte), Int32))
            Me.GridView1.Appearance.OddRow.ForeColor = System.Drawing.Color.Black
            Me.GridView1.Appearance.OddRow.Options.UseBackColor = True
            Me.GridView1.Appearance.OddRow.Options.UseBorderColor = True
            Me.GridView1.Appearance.OddRow.Options.UseForeColor = True
            Me.GridView1.Appearance.Preview.Font = New System.Drawing.Font("Verdana", 7.5!)
            Me.GridView1.Appearance.Preview.ForeColor = System.Drawing.Color.FromArgb(CType(CType(83, Byte), Int32), CType(CType(155, Byte), Int32), CType(CType(215, Byte), Int32))
            Me.GridView1.Appearance.Preview.Options.UseFont = True
            Me.GridView1.Appearance.Preview.Options.UseForeColor = True
            Me.GridView1.Appearance.Row.BackColor = System.Drawing.Color.FromArgb(CType(CType(247, Byte), Int32), CType(CType(251, Byte), Int32), CType(CType(255, Byte), Int32))
            Me.GridView1.Appearance.Row.ForeColor = System.Drawing.Color.Black
            Me.GridView1.Appearance.Row.Options.UseBackColor = True
            Me.GridView1.Appearance.Row.Options.UseForeColor = True
            Me.GridView1.Appearance.RowSeparator.BackColor = System.Drawing.Color.FromArgb(CType(CType(236, Byte), Int32), CType(CType(246, Byte), Int32), CType(CType(255, Byte), Int32))
            Me.GridView1.Appearance.RowSeparator.BackColor2 = System.Drawing.Color.White
            Me.GridView1.Appearance.RowSeparator.Options.UseBackColor = True
            Me.GridView1.Appearance.SelectedRow.BackColor = System.Drawing.Color.FromArgb(CType(CType(83, Byte), Int32), CType(CType(155, Byte), Int32), CType(CType(215, Byte), Int32))
            Me.GridView1.Appearance.SelectedRow.ForeColor = System.Drawing.Color.White
            Me.GridView1.Appearance.SelectedRow.Options.UseBackColor = True
            Me.GridView1.Appearance.SelectedRow.Options.UseForeColor = True
            Me.GridView1.Appearance.TopNewRow.BackColor = System.Drawing.Color.White
            Me.GridView1.Appearance.TopNewRow.Options.UseBackColor = True
            Me.GridView1.Appearance.VertLine.BackColor = System.Drawing.Color.FromArgb(CType(CType(104, Byte), Int32), CType(CType(184, Byte), Int32), CType(CType(251, Byte), Int32))
            Me.GridView1.Appearance.VertLine.Options.UseBackColor = True
            Me.GridView1.Columns.AddRange(New DevExpress.XtraGrid.Columns.GridColumn() {Me.GridColumn_id, Me.GridColumn5, Me.GridColumn6, Me.GridColumn4, Me.GridColumn1, Me.GridColumn2, Me.GridColumn3, Me.GridColumn7, Me.GridColumn8, Me.GridColumn9, Me.GridColumn10, Me.GridColumn11, Me.GridColumn12, Me.GridColumn13, Me.GridColumn14, Me.GridColumn15, Me.GridColumn16, Me.GridColumn17, Me.GridColumn18})
            StyleFormatCondition1.Appearance.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Int32), CType(CType(255, Byte), Int32), CType(CType(128, Byte), Int32))
            StyleFormatCondition1.Appearance.BackColor2 = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Int32), CType(CType(255, Byte), Int32), CType(CType(128, Byte), Int32))
            StyleFormatCondition1.Appearance.BorderColor = System.Drawing.Color.Yellow
            StyleFormatCondition1.Appearance.Options.UseBackColor = True
            StyleFormatCondition1.Appearance.Options.UseBorderColor = True
            StyleFormatCondition1.ApplyToRow = True
            StyleFormatCondition1.Column = Me.GridColumn6
            StyleFormatCondition1.Condition = DevExpress.XtraGrid.FormatConditionEnum.Equal
            StyleFormatCondition1.Value1 = 4
            StyleFormatCondition2.Appearance.BackColor = System.Drawing.Color.FromArgb(CType(CType(192, Byte), Int32), CType(CType(255, Byte), Int32), CType(CType(192, Byte), Int32))
            StyleFormatCondition2.Appearance.BackColor2 = System.Drawing.Color.FromArgb(CType(CType(192, Byte), Int32), CType(CType(255, Byte), Int32), CType(CType(192, Byte), Int32))
            StyleFormatCondition2.Appearance.BorderColor = System.Drawing.Color.FromArgb(CType(CType(192, Byte), Int32), CType(CType(255, Byte), Int32), CType(CType(192, Byte), Int32))
            StyleFormatCondition2.Appearance.Options.UseBackColor = True
            StyleFormatCondition2.Appearance.Options.UseBorderColor = True
            StyleFormatCondition2.ApplyToRow = True
            StyleFormatCondition2.Column = Me.GridColumn6
            StyleFormatCondition2.Condition = DevExpress.XtraGrid.FormatConditionEnum.Between
            StyleFormatCondition2.Value1 = 10
            StyleFormatCondition2.Value2 = 13
            Me.GridView1.FormatConditions.AddRange(New DevExpress.XtraGrid.StyleFormatCondition() {StyleFormatCondition1, StyleFormatCondition2})
            Me.GridView1.OptionsBehavior.Editable = False
            Me.GridView1.OptionsNavigation.AutoFocusNewRow = True
            Me.GridView1.OptionsSelection.InvertSelection = True
            Me.GridView1.OptionsView.RowAutoHeight = True
            Me.GridView1.OptionsView.ShowGroupPanel = False
            Me.GridView1.OptionsView.ShowIndicator = False
            '
            'GridColumn6
            '
            Me.GridColumn6.AppearanceCell.Options.UseTextOptions = True
            Me.GridColumn6.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
            Me.GridColumn6.AppearanceHeader.Options.UseTextOptions = True
            Me.GridColumn6.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
            Me.GridColumn6.Caption = "Biller Type"
            Me.GridColumn6.DisplayFormat.FormatString = "f0"
            Me.GridColumn6.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Custom
            Me.GridColumn6.FieldName = "biller_type"
            Me.GridColumn6.GroupFormat.FormatString = "f0"
            Me.GridColumn6.GroupFormat.FormatType = DevExpress.Utils.FormatType.Custom
            Me.GridColumn6.GroupInterval = DevExpress.XtraGrid.ColumnGroupInterval.DisplayText
            Me.GridColumn6.Name = "GridColumn6"
            Me.GridColumn6.SortMode = DevExpress.XtraGrid.ColumnSortMode.DisplayText
            Me.GridColumn6.Visible = True
            Me.GridColumn6.VisibleIndex = 3
            '
            'GridColumn_id
            '
            Me.GridColumn_id.AppearanceCell.Options.UseTextOptions = True
            Me.GridColumn_id.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
            Me.GridColumn_id.AppearanceHeader.Options.UseTextOptions = True
            Me.GridColumn_id.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
            Me.GridColumn_id.Caption = "ID"
            Me.GridColumn_id.FieldName = "rpps_biller_id"
            Me.GridColumn_id.Name = "GridColumn_id"
            Me.GridColumn_id.Visible = True
            Me.GridColumn_id.VisibleIndex = 0
            '
            'GridColumn1
            '
            Me.GridColumn1.Caption = "Rev Biller ID"
            Me.GridColumn1.FieldName = "reversal_biller_id"
            Me.GridColumn1.Name = "GridColumn1"
            '
            'GridColumn2
            '
            Me.GridColumn2.Caption = "EFF Date"
            Me.GridColumn2.FieldName = "effdate"
            Me.GridColumn2.Name = "GridColumn2"
            '
            'GridColumn3
            '
            Me.GridColumn3.Caption = "ABA"
            Me.GridColumn3.FieldName = "biller_aba"
            Me.GridColumn3.Name = "GridColumn3"
            Me.GridColumn3.Visible = True
            Me.GridColumn3.VisibleIndex = 1
            '
            'GridColumn4
            '
            Me.GridColumn4.Caption = "Name"
            Me.GridColumn4.FieldName = "biller_name"
            Me.GridColumn4.Name = "GridColumn4"
            Me.GridColumn4.Visible = True
            Me.GridColumn4.VisibleIndex = 2
            '
            'GridColumn5
            '
            Me.GridColumn5.Caption = "Biller Class"
            Me.GridColumn5.FieldName = "biller_class"
            Me.GridColumn5.Name = "GridColumn5"
            Me.GridColumn5.Visible = True
            Me.GridColumn5.VisibleIndex = 3
            '
            'GridColumn7
            '
            Me.GridColumn7.Caption = "Payment Pnote"
            Me.GridColumn7.FieldName = "payment_prenote"
            Me.GridColumn7.Name = "GridColumn7"
            '
            'GridColumn8
            '
            Me.GridColumn8.Caption = "Proposal Pnote"
            Me.GridColumn8.FieldName = "proposal_prenote"
            Me.GridColumn8.Name = "GridColumn8"
            '
            'GridColumn9
            '
            Me.GridColumn9.Caption = "DMP Pay Only"
            Me.GridColumn9.FieldName = "dmppayonly"
            Me.GridColumn9.Name = "GridColumn9"
            '
            'GridColumn10
            '
            Me.GridColumn10.Caption = "Gntd Funds"
            Me.GridColumn10.FieldName = "guaranteed_funds"
            Me.GridColumn10.Name = "GridColumn10"
            '
            'GridColumn11
            '
            Me.GridColumn11.Caption = "PVT Biller"
            Me.GridColumn11.FieldName = "pvt_biller_id"
            Me.GridColumn11.Name = "GridColumn11"
            '
            'GridColumn12
            '
            Me.GridColumn12.Caption = "Note"
            Me.GridColumn12.FieldName = "note"
            Me.GridColumn12.Name = "GridColumn12"
            '
            'GridColumn13
            '
            Me.GridColumn13.Caption = "CDP"
            Me.GridColumn13.FieldName = "send_cdp"
            Me.GridColumn13.Name = "GridColumn13"
            '
            'GridColumn14
            '
            Me.GridColumn14.Caption = "FBC"
            Me.GridColumn14.FieldName = "send_fbc"
            Me.GridColumn14.Name = "GridColumn14"
            '
            'GridColumn15
            '
            Me.GridColumn15.Caption = "FBD"
            Me.GridColumn15.FieldName = "send_fbd"
            Me.GridColumn15.Name = "GridColumn15"
            '
            'GridColumn16
            '
            Me.GridColumn16.Caption = "CDV"
            Me.GridColumn16.FieldName = "send_cdv"
            Me.GridColumn16.Name = "GridColumn16"
            '
            'GridColumn17
            '
            Me.GridColumn17.Caption = "CDD"
            Me.GridColumn17.FieldName = "send_cdd"
            Me.GridColumn17.Name = "GridColumn17"
            '
            'GridColumn18
            '
            Me.GridColumn18.Caption = "blroldname"
            Me.GridColumn18.FieldName = "blroldname"
            Me.GridColumn18.Name = "GridColumn18"
            '
            'MainForm
            '
            Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
            Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
            Me.ClientSize = New System.Drawing.Size(528, 294)
            Me.Name = "MainForm"
            Me.ToolTipController1.SetSuperTip(Me, Nothing)
            Me.Text = "RPPS Biller IDs"
            CType(Me.GridControl1, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.GridView1, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.DxErrorProvider1, System.ComponentModel.ISupportInitialize).EndInit()
            Me.ResumeLayout(False)

        End Sub
        Friend WithEvents GridColumn_id As DevExpress.XtraGrid.Columns.GridColumn
        Friend WithEvents GridColumn1 As DevExpress.XtraGrid.Columns.GridColumn
        Friend WithEvents GridColumn2 As DevExpress.XtraGrid.Columns.GridColumn
        Friend WithEvents GridColumn3 As DevExpress.XtraGrid.Columns.GridColumn
        Friend WithEvents GridColumn4 As DevExpress.XtraGrid.Columns.GridColumn
        Friend WithEvents GridColumn5 As DevExpress.XtraGrid.Columns.GridColumn
        Friend WithEvents GridColumn6 As DevExpress.XtraGrid.Columns.GridColumn
        Friend WithEvents GridColumn7 As DevExpress.XtraGrid.Columns.GridColumn
        Friend WithEvents GridColumn8 As DevExpress.XtraGrid.Columns.GridColumn
        Friend WithEvents GridColumn9 As DevExpress.XtraGrid.Columns.GridColumn
        Friend WithEvents GridColumn10 As DevExpress.XtraGrid.Columns.GridColumn
        Friend WithEvents GridColumn11 As DevExpress.XtraGrid.Columns.GridColumn
        Friend WithEvents GridColumn12 As DevExpress.XtraGrid.Columns.GridColumn
        Friend WithEvents GridColumn13 As DevExpress.XtraGrid.Columns.GridColumn
        Friend WithEvents GridColumn14 As DevExpress.XtraGrid.Columns.GridColumn
        Friend WithEvents GridColumn15 As DevExpress.XtraGrid.Columns.GridColumn
        Friend WithEvents GridColumn16 As DevExpress.XtraGrid.Columns.GridColumn
        Friend WithEvents GridColumn17 As DevExpress.XtraGrid.Columns.GridColumn
        Friend WithEvents GridColumn18 As DevExpress.XtraGrid.Columns.GridColumn

    End Class
End Namespace
