#Region "Copyright 2000-2012 DebtPlus, L.L.C."
'{*******************************************************************}
'{                                                                   }
'{       DebtPlus Debt Management System                             }
'{                                                                   }
'{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
'{       ALL RIGHTS RESERVED                                         }
'{                                                                   }
'{   The entire contents of this file is protected by U.S. and       }
'{   International Copyright Laws. Unauthorized reproduction,        }
'{   reverse-engineering, and distribution of all or any portion of  }
'{   the code contained in this file is strictly prohibited and may  }
'{   result in severe civil and criminal penalties and will be       }
'{   prosecuted to the maximum extent possible under the law.        }
'{                                                                   }
'{   RESTRICTIONS                                                    }
'{                                                                   }
'{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
'{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
'{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
'{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
'{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
'{                                                                   }
'{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
'{   ADDITIONAL RESTRICTIONS.                                        }
'{                                                                   }
'{*******************************************************************}
#End Region

Namespace RPPS.Biller.Edit.MASK
    Public Class MaskEdit

        Public Sub New()
            MyBase.New()
            InitializeComponent()
        End Sub

        Private drv As System.Data.DataRowView = Nothing
        Public Sub New(ByVal drv As System.Data.DataRowView)
            MyClass.New()
            Me.drv = drv

            AddHandler Me.Load, AddressOf Form_Load
            AddHandler SpinEdit1.EditValueChanging, AddressOf SpinEdit1_EditValueChanging
        End Sub

        Private Sub Form_Load(ByVal sender As Object, ByVal e As System.EventArgs)

            With TextEdit1
                .DataBindings.Clear()
                .DataBindings.Add("EditValue", drv, "mask")
                AddHandler .EditValueChanged, AddressOf Form_Changed
            End With

            With TextEdit2
                .DataBindings.Clear()
                .DataBindings.Add("EditValue", drv, "description")
                AddHandler .EditValueChanged, AddressOf Form_Changed
            End With

            With CheckEdit_ExceptionMask
                .DataBindings.Clear()
                .DataBindings.Add("EditValue", drv, "ExceptionMask")
                AddHandler .EditValueChanged, AddressOf Form_Changed
            End With

            With CheckEdit_checkdigit
                .DataBindings.Clear()
                .DataBindings.Add("EditValue", drv, "CheckDigit")
                AddHandler .EditValueChanged, AddressOf Form_Changed
            End With

            With SpinEdit1
                .DataBindings.Clear()
                .DataBindings.Add("EditValue", drv, "length")
                AddHandler .EditValueChanged, AddressOf Form_Changed
            End With

            SimpleButton_OK.Enabled = Not HasErrors()
        End Sub

        Private Sub Form_Changed(ByVal Sender As Object, ByVal e As System.EventArgs)
            SimpleButton_OK.Enabled = Not HasErrors()
        End Sub

        Private Function HasErrors() As Boolean
            Return False
        End Function

        Private Sub SpinEdit1_EditValueChanging(ByVal sender As Object, ByVal e As DevExpress.XtraEditors.Controls.ChangingEventArgs)
            Dim value As Object = SpinEdit1.EditValue
            If value Is Nothing OrElse value Is System.DBNull.Value Then value = 0
            e.Cancel = Convert.ToInt32(value) < 0
        End Sub
    End Class
End Namespace
