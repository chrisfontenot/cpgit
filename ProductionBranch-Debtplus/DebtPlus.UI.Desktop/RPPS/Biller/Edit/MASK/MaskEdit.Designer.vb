﻿Namespace RPPS.Biller.Edit.MASK
    <Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
    Partial Class MaskEdit
        Inherits DebtPlus.Data.Forms.DebtPlusForm

        'Form overrides dispose to clean up the component list.
        <System.Diagnostics.DebuggerNonUserCode()> _
        Protected Overrides Sub Dispose(ByVal disposing As Boolean)
            Try
                If disposing AndAlso components IsNot Nothing Then
                    components.Dispose()
                End If
            Finally
                MyBase.Dispose(disposing)
            End Try
        End Sub

        'Required by the Windows Form Designer
        Private components As System.ComponentModel.IContainer

        'NOTE: The following procedure is required by the Windows Form Designer
        'It can be modified using the Windows Form Designer.  
        'Do not modify it using the code editor.
        <System.Diagnostics.DebuggerStepThrough()> _
        Private Sub InitializeComponent()
            Me.SimpleButton_OK = New DevExpress.XtraEditors.SimpleButton
            Me.SimpleButton_Cance = New DevExpress.XtraEditors.SimpleButton
            Me.TextEdit1 = New DevExpress.XtraEditors.TextEdit
            Me.TextEdit2 = New DevExpress.XtraEditors.TextEdit
            Me.SpinEdit1 = New DevExpress.XtraEditors.SpinEdit
            Me.LabelControl1 = New DevExpress.XtraEditors.LabelControl
            Me.LabelControl2 = New DevExpress.XtraEditors.LabelControl
            Me.LabelControl3 = New DevExpress.XtraEditors.LabelControl
            Me.CheckEdit_ExceptionMask = New DevExpress.XtraEditors.CheckEdit
            Me.CheckEdit_checkdigit = New DevExpress.XtraEditors.CheckEdit
            CType(Me.DxErrorProvider1, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.TextEdit1.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.TextEdit2.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.SpinEdit1.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.CheckEdit_ExceptionMask.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.CheckEdit_checkdigit.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
            Me.SuspendLayout()
            '
            'SimpleButton_OK
            '
            Me.SimpleButton_OK.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
            Me.SimpleButton_OK.DialogResult = System.Windows.Forms.DialogResult.OK
            Me.SimpleButton_OK.Enabled = False
            Me.SimpleButton_OK.Location = New System.Drawing.Point(242, 86)
            Me.SimpleButton_OK.Name = "SimpleButton_OK"
            Me.SimpleButton_OK.Size = New System.Drawing.Size(75, 23)
            Me.SimpleButton_OK.TabIndex = 8
            Me.SimpleButton_OK.Text = "&OK"
            '
            'SimpleButton_Cance
            '
            Me.SimpleButton_Cance.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
            Me.SimpleButton_Cance.DialogResult = System.Windows.Forms.DialogResult.Cancel
            Me.SimpleButton_Cance.Location = New System.Drawing.Point(242, 126)
            Me.SimpleButton_Cance.Name = "SimpleButton_Cance"
            Me.SimpleButton_Cance.Size = New System.Drawing.Size(75, 23)
            Me.SimpleButton_Cance.TabIndex = 9
            Me.SimpleButton_Cance.Text = "&Cancel"
            '
            'TextEdit1
            '
            Me.TextEdit1.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                        Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
            Me.TextEdit1.Location = New System.Drawing.Point(81, 12)
            Me.TextEdit1.Name = "TextEdit1"
            Me.TextEdit1.Properties.MaxLength = 50
            Me.TextEdit1.Size = New System.Drawing.Size(241, 20)
            Me.TextEdit1.TabIndex = 1
            '
            'TextEdit2
            '
            Me.TextEdit2.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                        Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
            Me.TextEdit2.Location = New System.Drawing.Point(81, 38)
            Me.TextEdit2.Name = "TextEdit2"
            Me.TextEdit2.Properties.MaxLength = 22
            Me.TextEdit2.Size = New System.Drawing.Size(241, 20)
            Me.TextEdit2.TabIndex = 3
            '
            'SpinEdit1
            '
            Me.SpinEdit1.EditValue = New Decimal(New Int32() {0, 0, 0, 0})
            Me.SpinEdit1.Location = New System.Drawing.Point(81, 64)
            Me.SpinEdit1.Name = "SpinEdit1"
            Me.SpinEdit1.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton})
            Me.SpinEdit1.Properties.IsFloatValue = False
            Me.SpinEdit1.Properties.Mask.EditMask = "N00"
            Me.SpinEdit1.Properties.MaxValue = New Decimal(New Int32() {22, 0, 0, 0})
            Me.SpinEdit1.Size = New System.Drawing.Size(75, 20)
            Me.SpinEdit1.TabIndex = 5
            '
            'LabelControl1
            '
            Me.LabelControl1.Location = New System.Drawing.Point(13, 15)
            Me.LabelControl1.Name = "LabelControl1"
            Me.LabelControl1.Size = New System.Drawing.Size(24, 13)
            Me.LabelControl1.TabIndex = 0
            Me.LabelControl1.Text = "Mask"
            '
            'LabelControl2
            '
            Me.LabelControl2.Location = New System.Drawing.Point(13, 41)
            Me.LabelControl2.Name = "LabelControl2"
            Me.LabelControl2.Size = New System.Drawing.Size(23, 13)
            Me.LabelControl2.TabIndex = 2
            Me.LabelControl2.Text = "Note"
            '
            'LabelControl3
            '
            Me.LabelControl3.Location = New System.Drawing.Point(12, 67)
            Me.LabelControl3.Name = "LabelControl3"
            Me.LabelControl3.Size = New System.Drawing.Size(33, 13)
            Me.LabelControl3.TabIndex = 4
            Me.LabelControl3.Text = "Length"
            '
            'CheckEdit_ExceptionMask
            '
            Me.CheckEdit_ExceptionMask.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
            Me.CheckEdit_ExceptionMask.Location = New System.Drawing.Point(10, 100)
            Me.CheckEdit_ExceptionMask.Name = "CheckEdit_ExceptionMask"
            Me.CheckEdit_ExceptionMask.Properties.Caption = "Exception Mask"
            Me.CheckEdit_ExceptionMask.Size = New System.Drawing.Size(105, 19)
            Me.CheckEdit_ExceptionMask.TabIndex = 6
            '
            'CheckEdit_checkdigit
            '
            Me.CheckEdit_checkdigit.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
            Me.CheckEdit_checkdigit.Location = New System.Drawing.Point(11, 125)
            Me.CheckEdit_checkdigit.Name = "CheckEdit_checkdigit"
            Me.CheckEdit_checkdigit.Properties.Caption = "Has Check Digit"
            Me.CheckEdit_checkdigit.Size = New System.Drawing.Size(105, 19)
            Me.CheckEdit_checkdigit.TabIndex = 7
            '
            'MaskEdit
            '
            Me.AcceptButton = Me.SimpleButton_OK
            Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
            Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
            Me.CancelButton = Me.SimpleButton_Cance
            Me.ClientSize = New System.Drawing.Size(329, 175)
            Me.Controls.Add(Me.CheckEdit_checkdigit)
            Me.Controls.Add(Me.CheckEdit_ExceptionMask)
            Me.Controls.Add(Me.LabelControl3)
            Me.Controls.Add(Me.LabelControl2)
            Me.Controls.Add(Me.LabelControl1)
            Me.Controls.Add(Me.SpinEdit1)
            Me.Controls.Add(Me.TextEdit2)
            Me.Controls.Add(Me.TextEdit1)
            Me.Controls.Add(Me.SimpleButton_Cance)
            Me.Controls.Add(Me.SimpleButton_OK)
            Me.MaximizeBox = False
            Me.Name = "MaskEdit"
            Me.Text = "RPPS Mask"
            CType(Me.DxErrorProvider1, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.TextEdit1.Properties, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.TextEdit2.Properties, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.SpinEdit1.Properties, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.CheckEdit_ExceptionMask.Properties, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.CheckEdit_checkdigit.Properties, System.ComponentModel.ISupportInitialize).EndInit()
            Me.ResumeLayout(False)
            Me.PerformLayout()

        End Sub
        Friend WithEvents SimpleButton_OK As DevExpress.XtraEditors.SimpleButton
        Friend WithEvents SimpleButton_Cance As DevExpress.XtraEditors.SimpleButton
        Friend WithEvents TextEdit1 As DevExpress.XtraEditors.TextEdit
        Friend WithEvents TextEdit2 As DevExpress.XtraEditors.TextEdit
        Friend WithEvents SpinEdit1 As DevExpress.XtraEditors.SpinEdit
        Friend WithEvents LabelControl1 As DevExpress.XtraEditors.LabelControl
        Friend WithEvents LabelControl2 As DevExpress.XtraEditors.LabelControl
        Friend WithEvents LabelControl3 As DevExpress.XtraEditors.LabelControl
        Friend WithEvents CheckEdit_ExceptionMask As DevExpress.XtraEditors.CheckEdit
        Friend WithEvents CheckEdit_checkdigit As DevExpress.XtraEditors.CheckEdit
    End Class
End Namespace
