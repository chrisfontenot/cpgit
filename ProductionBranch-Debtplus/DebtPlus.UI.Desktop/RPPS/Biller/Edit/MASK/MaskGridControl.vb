#Region "Copyright 2000-2012 DebtPlus, L.L.C."
'{*******************************************************************}
'{                                                                   }
'{       DebtPlus Debt Management System                             }
'{                                                                   }
'{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
'{       ALL RIGHTS RESERVED                                         }
'{                                                                   }
'{   The entire contents of this file is protected by U.S. and       }
'{   International Copyright Laws. Unauthorized reproduction,        }
'{   reverse-engineering, and distribution of all or any portion of  }
'{   the code contained in this file is strictly prohibited and may  }
'{   result in severe civil and criminal penalties and will be       }
'{   prosecuted to the maximum extent possible under the law.        }
'{                                                                   }
'{   RESTRICTIONS                                                    }
'{                                                                   }
'{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
'{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
'{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
'{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
'{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
'{                                                                   }
'{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
'{   ADDITIONAL RESTRICTIONS.                                        }
'{                                                                   }
'{*******************************************************************}
#End Region
Imports System.Data.SqlClient
Imports System.Windows.Forms

Namespace RPPS.Biller.Edit.MASK

    Friend Class MaskGridControl

        Private OriginalBillerID As String

        Private RecordTable As System.Data.DataTable = Nothing
        Public Sub ReadForm(ByVal RppsBillerID As String)
            RecordTable = ReadDataset(RppsBillerID)
            GridControl1.DataSource = New System.Data.DataView(RecordTable, String.Format("[rpps_biller_id]='{0}'", RppsBillerID), String.Empty, DataViewRowState.CurrentRows)
            GridControl1.RefreshDataSource()
            GridView1.BestFitColumns()
        End Sub

        Private Function ReadDataset(ByVal RppsBillerID As String) As System.Data.DataTable
            OriginalBillerID = RppsBillerID

            Dim tbl As System.Data.DataTable = ctx.ds.Tables("rpps_Mask")
            If tbl IsNot Nothing Then
                Dim rows() As System.Data.DataRow = tbl.Select(String.Format("[rpps_biller_id]='{0}'", RppsBillerID))
                If rows.Length <= 0 Then tbl = Nothing
            End If

            ' Load the dataset
            If tbl Is Nothing Then
                Using cmd As SqlClient.SqlCommand = New SqlCommand
                    With cmd
                        .Connection = New SqlConnection(DebtPlus.LINQ.SQLInfoClass.getDefault().ConnectionString)
                        .CommandText = "SELECT [rpps_mask],[Mastercard_Key],[EffDate],[rpps_biller_id],[Length],[Mask],[Description],[ExceptionMask],[CheckDigit],[date_created],[created_by] FROM rpps_masks WHERE rpps_biller_id=@rpps_biller_id"
                        .CommandType = CommandType.Text
                        .Parameters.Add("@rpps_biller_id", SqlDbType.VarChar, 20).Value = OriginalBillerID
                    End With

                    Using da As New SqlClient.SqlDataAdapter(cmd)
                        da.FillLoadOption = LoadOption.PreserveChanges
                        da.AcceptChangesDuringFill = True
                        da.Fill(ctx.ds, "rpps_masks")
                        tbl = ctx.ds.Tables("rpps_masks")
                    End Using
                End Using

                ' Supplement the schema with the known values that are not read from the database
                With tbl
                    If .PrimaryKey.GetUpperBound(0) < 0 Then
                        .PrimaryKey = New System.Data.DataColumn() {.Columns("rpps_mask")}
                        With .Columns("rpps_mask")
                            .AutoIncrement = True
                            .AutoIncrementSeed = -1
                            .AutoIncrementStep = -1
                        End With
                    End If
                    tbl.Columns("ExceptionMask").DefaultValue = False
                    tbl.Columns("CheckDigit").DefaultValue = False
                End With
            End If

            Return tbl
        End Function

        Public Sub SaveForm()
        End Sub

        ''' <summary>
        ''' Edit the values for the current row
        ''' </summary>
        Protected Overrides Sub EditRow(ByVal EditDrv As System.Data.DataRowView)
            EditDrv.BeginEdit()
            With New MaskEdit(EditDrv)
                If .ShowDialog() = DialogResult.OK Then
                    EditDrv.EndEdit()
                Else
                    EditDrv.CancelEdit()
                End If
                .Dispose()
            End With
        End Sub

        ''' <summary>
        ''' Create a new blank row in the database
        ''' </summary>
        Protected Overrides Sub CreateRow(ByVal vue As System.Data.DataView)
            Dim EditDrv As System.Data.DataRowView = vue.AddNew
            EditDrv("rpps_biller_id") = OriginalBillerID
            EditDrv("date_created") = Now
            EditDrv("created_by") = "Me"
            EditRow(EditDrv)
        End Sub

        ''' <summary>
        ''' Delete the current row from the database
        ''' </summary>
        Protected Overrides Sub DeleteRow(ByVal EditDrv As System.Data.DataRowView)
            EditDrv.Delete()
        End Sub
    End Class
End Namespace
