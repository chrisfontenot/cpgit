﻿Imports DebtPlus.UI.Desktop.RPPS.Biller.Edit.Templates

Namespace RPPS.Biller.Edit.MASK
    <Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
    Partial Class MaskGridControl
        Inherits EditRecordControl

        'UserControl overrides dispose to clean up the component list.
        <System.Diagnostics.DebuggerNonUserCode()> _
        Protected Overrides Sub Dispose(ByVal disposing As Boolean)
            Try
                If disposing AndAlso components IsNot Nothing Then
                    components.Dispose()
                End If
            Finally
                MyBase.Dispose(disposing)
            End Try
        End Sub

        'Required by the Windows Form Designer
        Private components As System.ComponentModel.IContainer

        'NOTE: The following procedure is required by the Windows Form Designer
        'It can be modified using the Windows Form Designer.  
        'Do not modify it using the code editor.
        <System.Diagnostics.DebuggerStepThrough()> _
        Private Sub InitializeComponent()
            Me.components = New System.ComponentModel.Container
            Me.GridColumn_address = New DevExpress.XtraGrid.Columns.GridColumn
            Me.GridColumn_address.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.True
            Me.GridColumn2 = New DevExpress.XtraGrid.Columns.GridColumn
            Me.GridColumn2.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.True
            Me.GridColumn1 = New DevExpress.XtraGrid.Columns.GridColumn
            Me.GridColumn1.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.True
            Me.GridColumn3 = New DevExpress.XtraGrid.Columns.GridColumn
            Me.GridColumn3.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.True
            Me.GridColumn4 = New DevExpress.XtraGrid.Columns.GridColumn
            Me.GridColumn4.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.True
            Me.SuspendLayout()
            '
            'GridView1
            '
            Me.GridView1.Columns.AddRange(New DevExpress.XtraGrid.Columns.GridColumn() {Me.GridColumn_address, Me.GridColumn2, Me.GridColumn1, Me.GridColumn3, Me.GridColumn4})
            '
            'GridColumn_address
            '
            Me.GridColumn_address.AppearanceHeader.Options.UseTextOptions = True
            Me.GridColumn_address.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
            Me.GridColumn_address.AppearanceCell.Options.UseTextOptions = True
            Me.GridColumn_address.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
            Me.GridColumn_address.Caption = "ID"
            Me.GridColumn_address.CustomizationCaption = "Record ID"
            Me.GridColumn_address.DisplayFormat.FormatString = "{0:f0}"
            Me.GridColumn_address.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
            Me.GridColumn_address.FieldName = "rpps_mask"
            Me.GridColumn_address.GroupFormat.FormatString = "{0:f0}"
            Me.GridColumn_address.GroupFormat.FormatType = DevExpress.Utils.FormatType.Numeric
            Me.GridColumn_address.Name = "GridColumn_address"
            '
            'GridColumn2
            '
            Me.GridColumn2.Caption = "Mask"
            Me.GridColumn2.FieldName = "Mask"
            Me.GridColumn2.Name = "GridColumn2"
            Me.GridColumn2.Visible = True
            Me.GridColumn2.VisibleIndex = 0
            '
            'GridColumn1
            '
            Me.GridColumn1.Caption = "Length"
            Me.GridColumn1.AppearanceHeader.Options.UseTextOptions = True
            Me.GridColumn1.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
            Me.GridColumn1.AppearanceCell.Options.UseTextOptions = True
            Me.GridColumn1.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
            Me.GridColumn1.DisplayFormat.FormatString = "{0:f0}"
            Me.GridColumn1.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
            Me.GridColumn1.FieldName = "Length"
            Me.GridColumn1.GroupFormat.FormatString = "{0:f0}"
            Me.GridColumn1.GroupFormat.FormatType = DevExpress.Utils.FormatType.Numeric
            Me.GridColumn1.Name = "GridColumn1"
            Me.GridColumn1.Visible = True
            Me.GridColumn1.VisibleIndex = 1
            '
            'GridColumn3
            '
            Me.GridColumn3.Caption = "Description"
            Me.GridColumn3.FieldName = "Description"
            Me.GridColumn3.Name = "GridColumn3"
            '
            'GridColumn4
            '
            Me.GridColumn4.Caption = "Checksum"
            Me.GridColumn4.DisplayFormat.FormatString = "{0:f0}"
            Me.GridColumn4.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
            Me.GridColumn4.FieldName = "CheckDigit"
            Me.GridColumn4.GroupFormat.FormatString = "{0:f0}"
            Me.GridColumn4.GroupFormat.FormatType = DevExpress.Utils.FormatType.Numeric
            Me.GridColumn4.Name = "GridColumn4"
            '
            'MaskGridControl
            '
            Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
            Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
            Me.Name = "MaskGridControl"
            Me.Size = New System.Drawing.Size(426, 355)
            Me.ResumeLayout(False)

        End Sub
        Friend WithEvents GridColumn_address As DevExpress.XtraGrid.Columns.GridColumn
        Friend WithEvents GridColumn1 As DevExpress.XtraGrid.Columns.GridColumn
        Friend WithEvents GridColumn2 As DevExpress.XtraGrid.Columns.GridColumn
        Friend WithEvents GridColumn3 As DevExpress.XtraGrid.Columns.GridColumn
        Friend WithEvents GridColumn4 As DevExpress.XtraGrid.Columns.GridColumn
    End Class
End Namespace
