#Region "Copyright 2000-2012 DebtPlus, L.L.C."
'{*******************************************************************}
'{                                                                   }
'{       DebtPlus Debt Management System                             }
'{                                                                   }
'{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
'{       ALL RIGHTS RESERVED                                         }
'{                                                                   }
'{   The entire contents of this file is protected by U.S. and       }
'{   International Copyright Laws. Unauthorized reproduction,        }
'{   reverse-engineering, and distribution of all or any portion of  }
'{   the code contained in this file is strictly prohibited and may  }
'{   result in severe civil and criminal penalties and will be       }
'{   prosecuted to the maximum extent possible under the law.        }
'{                                                                   }
'{   RESTRICTIONS                                                    }
'{                                                                   }
'{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
'{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
'{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
'{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
'{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
'{                                                                   }
'{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
'{   ADDITIONAL RESTRICTIONS.                                        }
'{                                                                   }
'{*******************************************************************}
#End Region

Namespace RPPS.Biller.Edit.ADDRESS
    Public Class AddressEdit

        Public Sub New()
            MyBase.New()
            InitializeComponent()
        End Sub

        Private drv As System.Data.DataRowView = Nothing
        Public Sub New(ByVal drv As System.Data.DataRowView)
            MyClass.New()
            Me.drv = drv
            AddHandler SimpleButton_OK.Click, AddressOf SimpleButton_OK_Click
            AddHandler Load, AddressOf Form_Load
        End Sub

        Private Sub Form_Load(ByVal sender As Object, ByVal e As System.EventArgs)
            AddressParts1.BeginInit()
            Try
                AddressParts1.Line1.DataBindings.Clear()
                AddressParts1.Line1.DataBindings.Add("EditValue", drv, "biller_addr1")
                AddHandler AddressParts1.Line1.EditValueChanged, AddressOf Form_Changed

                AddressParts1.Line2.DataBindings.Clear()
                AddressParts1.Line2.DataBindings.Add("EditValue", drv, "biller_addr2")
                AddHandler AddressParts1.Line2.EditValueChanged, AddressOf Form_Changed

                AddressParts1.PostalCode.DataBindings.Clear()
                AddressParts1.PostalCode.DataBindings.Add("EditValue", drv, "biller_zipcode")
                AddHandler AddressParts1.PostalCode.EditValueChanged, AddressOf Form_Changed

                AddressParts1.City.DataBindings.Clear()
                AddressParts1.City.DataBindings.Add("EditValue", drv, "biller_city")
                AddHandler AddressParts1.City.EditValueChanged, AddressOf Form_Changed

                AddressParts1.States.DataBindings.Clear()
                AddressParts1.States.DataBindings.Add("Text", drv, "biller_state")
                AddHandler AddressParts1.States.EditValueChanged, AddressOf Form_Changed

            Finally
                AddressParts1.EndInit()
            End Try

            TextEdit1.DataBindings.Clear()
            TextEdit1.DataBindings.Add("EditValue", drv, "biller_country")
            AddHandler TextEdit1.EditValueChanged, AddressOf Form_Changed

            SimpleButton_OK.Enabled = Not HasErrors()
        End Sub

        Private Sub Form_Changed(ByVal Sender As Object, ByVal e As System.EventArgs)
            SimpleButton_OK.Enabled = Not HasErrors()
        End Sub

        Private Function HasErrors() As Boolean
            Return False
        End Function

        Private Sub SimpleButton_OK_Click(ByVal sender As Object, ByVal e As System.EventArgs)
            drv("biller_state") = AddressParts1.States.Text
        End Sub
    End Class
End Namespace
