﻿Namespace RPPS.Biller.Edit.LOB
    <Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
    Partial Class LOBEdit
        Inherits DebtPlus.Data.Forms.DebtPlusForm

        'Form overrides dispose to clean up the component list.
        <System.Diagnostics.DebuggerNonUserCode()> _
        Protected Overrides Sub Dispose(ByVal disposing As Boolean)
            Try
                If disposing AndAlso components IsNot Nothing Then
                    components.Dispose()
                End If
            Finally
                MyBase.Dispose(disposing)
            End Try
        End Sub

        'Required by the Windows Form Designer
        Private components As System.ComponentModel.IContainer

        'NOTE: The following procedure is required by the Windows Form Designer
        'It can be modified using the Windows Form Designer.  
        'Do not modify it using the code editor.
        <System.Diagnostics.DebuggerStepThrough()> _
        Private Sub InitializeComponent()
            Me.SimpleButton_OK = New DevExpress.XtraEditors.SimpleButton
            Me.SimpleButton_Cance = New DevExpress.XtraEditors.SimpleButton
            Me.LabelControl1 = New DevExpress.XtraEditors.LabelControl
            Me.TextEdit1 = New DevExpress.XtraEditors.TextEdit
            CType(Me.DxErrorProvider1, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.TextEdit1.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
            Me.SuspendLayout()
            '
            'SimpleButton_OK
            '
            Me.SimpleButton_OK.DialogResult = System.Windows.Forms.DialogResult.OK
            Me.SimpleButton_OK.Enabled = False
            Me.SimpleButton_OK.Location = New System.Drawing.Point(63, 57)
            Me.SimpleButton_OK.Name = "SimpleButton_OK"
            Me.SimpleButton_OK.Size = New System.Drawing.Size(75, 23)
            Me.SimpleButton_OK.TabIndex = 2
            Me.SimpleButton_OK.Text = "&OK"
            '
            'SimpleButton_Cance
            '
            Me.SimpleButton_Cance.DialogResult = System.Windows.Forms.DialogResult.Cancel
            Me.SimpleButton_Cance.Location = New System.Drawing.Point(155, 57)
            Me.SimpleButton_Cance.Name = "SimpleButton_Cance"
            Me.SimpleButton_Cance.Size = New System.Drawing.Size(75, 23)
            Me.SimpleButton_Cance.TabIndex = 3
            Me.SimpleButton_Cance.Text = "&Cancel"
            '
            'LabelControl1
            '
            Me.LabelControl1.Location = New System.Drawing.Point(12, 16)
            Me.LabelControl1.Name = "LabelControl1"
            Me.LabelControl1.Size = New System.Drawing.Size(76, 13)
            Me.LabelControl1.TabIndex = 0
            Me.LabelControl1.Text = "Line of Business"
            '
            'TextEdit1
            '
            Me.TextEdit1.Location = New System.Drawing.Point(94, 13)
            Me.TextEdit1.Name = "TextEdit1"
            Me.TextEdit1.Properties.MaxLength = 3
            Me.TextEdit1.Size = New System.Drawing.Size(65, 20)
            Me.TextEdit1.TabIndex = 1
            '
            'LOBEdit
            '
            Me.AcceptButton = Me.SimpleButton_OK
            Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
            Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
            Me.CancelButton = Me.SimpleButton_Cance
            Me.ClientSize = New System.Drawing.Size(292, 98)
            Me.Controls.Add(Me.TextEdit1)
            Me.Controls.Add(Me.LabelControl1)
            Me.Controls.Add(Me.SimpleButton_Cance)
            Me.Controls.Add(Me.SimpleButton_OK)
            Me.MaximizeBox = False
            Me.Name = "LOBEdit"
            Me.ToolTipController1.SetSuperTip(Me, Nothing)
            Me.Text = "RPPS Line Of Buisness"
            CType(Me.DxErrorProvider1, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.TextEdit1.Properties, System.ComponentModel.ISupportInitialize).EndInit()
            Me.ResumeLayout(False)
            Me.PerformLayout()

        End Sub
        Friend WithEvents SimpleButton_OK As DevExpress.XtraEditors.SimpleButton
        Friend WithEvents SimpleButton_Cance As DevExpress.XtraEditors.SimpleButton
        Friend WithEvents LabelControl1 As DevExpress.XtraEditors.LabelControl
        Friend WithEvents TextEdit1 As DevExpress.XtraEditors.TextEdit
    End Class
End Namespace
