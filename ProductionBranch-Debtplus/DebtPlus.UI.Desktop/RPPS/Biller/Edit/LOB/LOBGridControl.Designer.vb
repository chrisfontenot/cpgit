﻿Namespace RPPS.Biller.Edit.LOB

    <Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
    Partial Class LOBGridControl

        'UserControl overrides dispose to clean up the component list.
        <System.Diagnostics.DebuggerNonUserCode()> _
        Protected Overrides Sub Dispose(ByVal disposing As Boolean)
            Try
                If disposing AndAlso components IsNot Nothing Then
                    components.Dispose()
                End If
            Finally
                MyBase.Dispose(disposing)
            End Try
        End Sub

        'Required by the Windows Form Designer
        Private components As System.ComponentModel.IContainer

        'NOTE: The following procedure is required by the Windows Form Designer
        'It can be modified using the Windows Form Designer.  
        'Do not modify it using the code editor.
        <System.Diagnostics.DebuggerStepThrough()> _
        Private Sub InitializeComponent()
            Me.components = New System.ComponentModel.Container
            Me.GridColumn_lob = New DevExpress.XtraGrid.Columns.GridColumn
            Me.GridColumn_lob.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.True
            Me.GridColumn_description = New DevExpress.XtraGrid.Columns.GridColumn
            Me.GridColumn_description.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.True
            Me.SuspendLayout()
            '
            'GridView1
            '
            Me.GridView1.Columns.AddRange(New DevExpress.XtraGrid.Columns.GridColumn() {Me.GridColumn_lob, Me.GridColumn_description})
            '
            'GridColumn_lob
            '
            Me.GridColumn_lob.AppearanceHeader.Options.UseTextOptions = True
            Me.GridColumn_lob.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
            Me.GridColumn_lob.Caption = "ID"
            Me.GridColumn_lob.CustomizationCaption = "Record ID"
            Me.GridColumn_lob.DisplayFormat.FormatString = "{0:f0}"
            Me.GridColumn_lob.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
            Me.GridColumn_lob.FieldName = "rpps_biller_lob"
            Me.GridColumn_lob.GroupFormat.FormatString = "{0:f0}"
            Me.GridColumn_lob.GroupFormat.FormatType = DevExpress.Utils.FormatType.Numeric
            Me.GridColumn_lob.Name = "GridColumn_lob"
            '
            'GridColumn_description
            '
            Me.GridColumn_description.Caption = "Line Of Business"
            Me.GridColumn_description.FieldName = "biller_lob"
            Me.GridColumn_description.Name = "GridColumn_description"
            Me.GridColumn_description.Visible = True
            Me.GridColumn_description.VisibleIndex = 0
            '
            'LOBGridControl
            '
            Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
            Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
            Me.Name = "LOBGridControl"
            Me.Size = New System.Drawing.Size(426, 355)
            Me.ResumeLayout(False)

        End Sub
        Friend WithEvents GridColumn_lob As DevExpress.XtraGrid.Columns.GridColumn
        Friend WithEvents GridColumn_description As DevExpress.XtraGrid.Columns.GridColumn
    End Class
End Namespace
