﻿Namespace RPPS.Biller.Edit.AKA
    <Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
    Partial Class AKAGridControl

        'UserControl overrides dispose to clean up the component list.
        <System.Diagnostics.DebuggerNonUserCode()> _
        Protected Overrides Sub Dispose(ByVal disposing As Boolean)
            Try
                If disposing AndAlso components IsNot Nothing Then
                    components.Dispose()
                End If
            Finally
                MyBase.Dispose(disposing)
            End Try
        End Sub

        'Required by the Windows Form Designer
        Private components As System.ComponentModel.IContainer

        'NOTE: The following procedure is required by the Windows Form Designer
        'It can be modified using the Windows Form Designer.  
        'Do not modify it using the code editor.
        <System.Diagnostics.DebuggerStepThrough()> _
        Private Sub InitializeComponent()
            Me.components = New System.ComponentModel.Container
            Me.GridColumn_aka = New DevExpress.XtraGrid.Columns.GridColumn
            Me.GridColumn_aka.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.True
            Me.GridColumn_description = New DevExpress.XtraGrid.Columns.GridColumn
            Me.GridColumn_description.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.True
            Me.SuspendLayout()
            '
            'GridView1
            '
            Me.GridView1.Columns.AddRange(New DevExpress.XtraGrid.Columns.GridColumn() {Me.GridColumn_aka, Me.GridColumn_description})
            '
            'GridColumn_aka
            '
            Me.GridColumn_aka.AppearanceHeader.Options.UseTextOptions = True
            Me.GridColumn_aka.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
            Me.GridColumn_aka.Caption = "ID"
            Me.GridColumn_aka.CustomizationCaption = "Record ID"
            Me.GridColumn_aka.DisplayFormat.FormatString = "{0:f0}"
            Me.GridColumn_aka.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
            Me.GridColumn_aka.FieldName = "rpps_aka"
            Me.GridColumn_aka.GroupFormat.FormatString = "{0:f0}"
            Me.GridColumn_aka.GroupFormat.FormatType = DevExpress.Utils.FormatType.Numeric
            Me.GridColumn_aka.Name = "GridColumn_aka"
            '
            'GridColumn_description
            '
            Me.GridColumn_description.Caption = "Aliases"
            Me.GridColumn_description.FieldName = "name"
            Me.GridColumn_description.Name = "GridColumn_description"
            Me.GridColumn_description.Visible = True
            Me.GridColumn_description.VisibleIndex = 0
            '
            'AKAGridControl
            '
            Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
            Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
            Me.Name = "AKAGridControl"
            Me.Size = New System.Drawing.Size(426, 355)
            Me.ResumeLayout(False)

        End Sub
        Friend WithEvents GridColumn_aka As DevExpress.XtraGrid.Columns.GridColumn
        Friend WithEvents GridColumn_description As DevExpress.XtraGrid.Columns.GridColumn
    End Class
End Namespace
