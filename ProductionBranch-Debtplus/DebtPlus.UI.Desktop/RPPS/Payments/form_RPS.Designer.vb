﻿Namespace RPPS.Payments
    <Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
    Partial Class form_RPS
        Inherits DebtPlus.Data.Forms.DebtPlusForm

        'Form overrides dispose to clean up the component list.
        <System.Diagnostics.DebuggerNonUserCode()> _
        Protected Overrides Sub Dispose(ByVal disposing As Boolean)
            Try
                If disposing AndAlso components IsNot Nothing Then
                    components.Dispose()
                End If
            Finally
                MyBase.Dispose(disposing)
            End Try
        End Sub

        'Required by the Windows Form Designer
        Private components As System.ComponentModel.IContainer

        'NOTE: The following procedure is required by the Windows Form Designer
        'It can be modified using the Windows Form Designer.  
        'Do not modify it using the code editor.
        <System.Diagnostics.DebuggerStepThrough()> _
        Private Sub InitializeComponent()
            Me.LabelControl_total_debit_amount = New DevExpress.XtraEditors.LabelControl
            Me.LabelControl_total_credit_amount = New DevExpress.XtraEditors.LabelControl
            Me.LabelControl_total_items = New DevExpress.XtraEditors.LabelControl
            Me.LabelControl_total_batches = New DevExpress.XtraEditors.LabelControl
            Me.LabelControl_total_billed_amounts = New DevExpress.XtraEditors.LabelControl
            Me.LabelControl_total_gross_amounts = New DevExpress.XtraEditors.LabelControl
            Me.LabelControl_total_net_amounts = New DevExpress.XtraEditors.LabelControl
            Me.LabelControl_prenote_count = New DevExpress.XtraEditors.LabelControl
            Me.lbl_prenote_count = New DevExpress.XtraEditors.LabelControl
            Me.lbl_net_amounts = New DevExpress.XtraEditors.LabelControl
            Me.lbl_gross_amounts = New DevExpress.XtraEditors.LabelControl
            Me.lbl_billed_amounts = New DevExpress.XtraEditors.LabelControl
            Me.lbl_total_batches = New DevExpress.XtraEditors.LabelControl
            Me.lbl_total_items = New DevExpress.XtraEditors.LabelControl
            Me.lbl_total_credit_amount = New DevExpress.XtraEditors.LabelControl
            Me.lbl_total_debit_amount = New DevExpress.XtraEditors.LabelControl
            Me.LabelControl9 = New DevExpress.XtraEditors.LabelControl
            Me.lbl_batch_id = New DevExpress.XtraEditors.LabelControl
            Me.Button_OK = New DevExpress.XtraEditors.SimpleButton
            Me.Button_Print = New DevExpress.XtraEditors.SimpleButton
            Me.BackgroundWorker1 = New System.ComponentModel.BackgroundWorker
            CType(Me.DxErrorProvider1, System.ComponentModel.ISupportInitialize).BeginInit()
            Me.SuspendLayout()
            '
            'LabelControl_total_debit_amount
            '
            Me.LabelControl_total_debit_amount.Appearance.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold)
            Me.LabelControl_total_debit_amount.Appearance.Options.UseFont = True
            Me.LabelControl_total_debit_amount.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None
            Me.LabelControl_total_debit_amount.Location = New System.Drawing.Point(12, 46)
            Me.LabelControl_total_debit_amount.Name = "LabelControl_total_debit_amount"
            Me.LabelControl_total_debit_amount.Size = New System.Drawing.Size(166, 13)
            Me.LabelControl_total_debit_amount.TabIndex = 0
            Me.LabelControl_total_debit_amount.Text = "Total Debit Amount:"
            Me.LabelControl_total_debit_amount.UseMnemonic = False
            '
            'LabelControl_total_credit_amount
            '
            Me.LabelControl_total_credit_amount.Appearance.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold)
            Me.LabelControl_total_credit_amount.Appearance.Options.UseFont = True
            Me.LabelControl_total_credit_amount.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None
            Me.LabelControl_total_credit_amount.Location = New System.Drawing.Point(12, 65)
            Me.LabelControl_total_credit_amount.Name = "LabelControl_total_credit_amount"
            Me.LabelControl_total_credit_amount.Size = New System.Drawing.Size(166, 13)
            Me.LabelControl_total_credit_amount.TabIndex = 1
            Me.LabelControl_total_credit_amount.Text = "Total Credit Amount:"
            Me.LabelControl_total_credit_amount.UseMnemonic = False
            '
            'LabelControl_total_items
            '
            Me.LabelControl_total_items.Appearance.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold)
            Me.LabelControl_total_items.Appearance.Options.UseFont = True
            Me.LabelControl_total_items.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None
            Me.LabelControl_total_items.Location = New System.Drawing.Point(12, 84)
            Me.LabelControl_total_items.Name = "LabelControl_total_items"
            Me.LabelControl_total_items.Size = New System.Drawing.Size(166, 13)
            Me.LabelControl_total_items.TabIndex = 2
            Me.LabelControl_total_items.Text = "Total Items:"
            Me.LabelControl_total_items.UseMnemonic = False
            '
            'LabelControl_total_batches
            '
            Me.LabelControl_total_batches.Appearance.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold)
            Me.LabelControl_total_batches.Appearance.Options.UseFont = True
            Me.LabelControl_total_batches.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None
            Me.LabelControl_total_batches.Location = New System.Drawing.Point(12, 103)
            Me.LabelControl_total_batches.Name = "LabelControl_total_batches"
            Me.LabelControl_total_batches.Size = New System.Drawing.Size(166, 13)
            Me.LabelControl_total_batches.TabIndex = 3
            Me.LabelControl_total_batches.Text = "Total Batches:"
            Me.LabelControl_total_batches.UseMnemonic = False
            '
            'LabelControl_total_billed_amounts
            '
            Me.LabelControl_total_billed_amounts.Appearance.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold)
            Me.LabelControl_total_billed_amounts.Appearance.Options.UseFont = True
            Me.LabelControl_total_billed_amounts.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None
            Me.LabelControl_total_billed_amounts.Location = New System.Drawing.Point(12, 143)
            Me.LabelControl_total_billed_amounts.Name = "LabelControl_total_billed_amounts"
            Me.LabelControl_total_billed_amounts.Size = New System.Drawing.Size(166, 13)
            Me.LabelControl_total_billed_amounts.TabIndex = 4
            Me.LabelControl_total_billed_amounts.Text = "Billed Amounts:"
            Me.LabelControl_total_billed_amounts.UseMnemonic = False
            '
            'LabelControl_total_gross_amounts
            '
            Me.LabelControl_total_gross_amounts.Appearance.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold)
            Me.LabelControl_total_gross_amounts.Appearance.Options.UseFont = True
            Me.LabelControl_total_gross_amounts.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None
            Me.LabelControl_total_gross_amounts.Location = New System.Drawing.Point(12, 162)
            Me.LabelControl_total_gross_amounts.Name = "LabelControl_total_gross_amounts"
            Me.LabelControl_total_gross_amounts.Size = New System.Drawing.Size(166, 13)
            Me.LabelControl_total_gross_amounts.TabIndex = 5
            Me.LabelControl_total_gross_amounts.Text = "Gross Amounts:"
            Me.LabelControl_total_gross_amounts.UseMnemonic = False
            '
            'LabelControl_total_net_amounts
            '
            Me.LabelControl_total_net_amounts.Appearance.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold)
            Me.LabelControl_total_net_amounts.Appearance.Options.UseFont = True
            Me.LabelControl_total_net_amounts.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None
            Me.LabelControl_total_net_amounts.Location = New System.Drawing.Point(12, 181)
            Me.LabelControl_total_net_amounts.Name = "LabelControl_total_net_amounts"
            Me.LabelControl_total_net_amounts.Size = New System.Drawing.Size(166, 13)
            Me.LabelControl_total_net_amounts.TabIndex = 6
            Me.LabelControl_total_net_amounts.Text = "Net Amounts:"
            Me.LabelControl_total_net_amounts.UseMnemonic = False
            '
            'LabelControl_prenote_count
            '
            Me.LabelControl_prenote_count.Appearance.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold)
            Me.LabelControl_prenote_count.Appearance.Options.UseFont = True
            Me.LabelControl_prenote_count.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None
            Me.LabelControl_prenote_count.Location = New System.Drawing.Point(12, 200)
            Me.LabelControl_prenote_count.Name = "LabelControl_prenote_count"
            Me.LabelControl_prenote_count.Size = New System.Drawing.Size(166, 13)
            Me.LabelControl_prenote_count.TabIndex = 7
            Me.LabelControl_prenote_count.Text = "Prenote Count:"
            Me.LabelControl_prenote_count.UseMnemonic = False
            '
            'lbl_prenote_count
            '
            Me.lbl_prenote_count.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
            Me.lbl_prenote_count.Appearance.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold)
            Me.lbl_prenote_count.Appearance.Options.UseFont = True
            Me.lbl_prenote_count.Appearance.Options.UseTextOptions = True
            Me.lbl_prenote_count.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
            Me.lbl_prenote_count.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None
            Me.lbl_prenote_count.Location = New System.Drawing.Point(264, 200)
            Me.lbl_prenote_count.Name = "lbl_prenote_count"
            Me.lbl_prenote_count.Size = New System.Drawing.Size(120, 13)
            Me.lbl_prenote_count.TabIndex = 15
            Me.lbl_prenote_count.Text = "0"
            Me.lbl_prenote_count.UseMnemonic = False
            '
            'lbl_net_amounts
            '
            Me.lbl_net_amounts.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
            Me.lbl_net_amounts.Appearance.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold)
            Me.lbl_net_amounts.Appearance.Options.UseFont = True
            Me.lbl_net_amounts.Appearance.Options.UseTextOptions = True
            Me.lbl_net_amounts.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
            Me.lbl_net_amounts.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None
            Me.lbl_net_amounts.Location = New System.Drawing.Point(264, 181)
            Me.lbl_net_amounts.Name = "lbl_net_amounts"
            Me.lbl_net_amounts.Size = New System.Drawing.Size(120, 13)
            Me.lbl_net_amounts.TabIndex = 14
            Me.lbl_net_amounts.Text = "$0.00"
            Me.lbl_net_amounts.UseMnemonic = False
            '
            'lbl_gross_amounts
            '
            Me.lbl_gross_amounts.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
            Me.lbl_gross_amounts.Appearance.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold)
            Me.lbl_gross_amounts.Appearance.Options.UseFont = True
            Me.lbl_gross_amounts.Appearance.Options.UseTextOptions = True
            Me.lbl_gross_amounts.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
            Me.lbl_gross_amounts.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None
            Me.lbl_gross_amounts.Location = New System.Drawing.Point(264, 162)
            Me.lbl_gross_amounts.Name = "lbl_gross_amounts"
            Me.lbl_gross_amounts.Size = New System.Drawing.Size(120, 13)
            Me.lbl_gross_amounts.TabIndex = 13
            Me.lbl_gross_amounts.Text = "$0.00"
            Me.lbl_gross_amounts.UseMnemonic = False
            '
            'lbl_billed_amounts
            '
            Me.lbl_billed_amounts.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
            Me.lbl_billed_amounts.Appearance.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold)
            Me.lbl_billed_amounts.Appearance.Options.UseFont = True
            Me.lbl_billed_amounts.Appearance.Options.UseTextOptions = True
            Me.lbl_billed_amounts.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
            Me.lbl_billed_amounts.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None
            Me.lbl_billed_amounts.Location = New System.Drawing.Point(264, 143)
            Me.lbl_billed_amounts.Name = "lbl_billed_amounts"
            Me.lbl_billed_amounts.Size = New System.Drawing.Size(120, 13)
            Me.lbl_billed_amounts.TabIndex = 12
            Me.lbl_billed_amounts.Text = "$0.00"
            Me.lbl_billed_amounts.UseMnemonic = False
            '
            'lbl_total_batches
            '
            Me.lbl_total_batches.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
            Me.lbl_total_batches.Appearance.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold)
            Me.lbl_total_batches.Appearance.Options.UseFont = True
            Me.lbl_total_batches.Appearance.Options.UseTextOptions = True
            Me.lbl_total_batches.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
            Me.lbl_total_batches.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None
            Me.lbl_total_batches.Location = New System.Drawing.Point(264, 103)
            Me.lbl_total_batches.Name = "lbl_total_batches"
            Me.lbl_total_batches.Size = New System.Drawing.Size(120, 13)
            Me.lbl_total_batches.TabIndex = 11
            Me.lbl_total_batches.Text = "0"
            Me.lbl_total_batches.UseMnemonic = False
            '
            'lbl_total_items
            '
            Me.lbl_total_items.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
            Me.lbl_total_items.Appearance.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold)
            Me.lbl_total_items.Appearance.Options.UseFont = True
            Me.lbl_total_items.Appearance.Options.UseTextOptions = True
            Me.lbl_total_items.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
            Me.lbl_total_items.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None
            Me.lbl_total_items.Location = New System.Drawing.Point(264, 84)
            Me.lbl_total_items.Name = "lbl_total_items"
            Me.lbl_total_items.Size = New System.Drawing.Size(120, 13)
            Me.lbl_total_items.TabIndex = 10
            Me.lbl_total_items.Text = "0"
            Me.lbl_total_items.UseMnemonic = False
            '
            'lbl_total_credit_amount
            '
            Me.lbl_total_credit_amount.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
            Me.lbl_total_credit_amount.Appearance.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold)
            Me.lbl_total_credit_amount.Appearance.Options.UseFont = True
            Me.lbl_total_credit_amount.Appearance.Options.UseTextOptions = True
            Me.lbl_total_credit_amount.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
            Me.lbl_total_credit_amount.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None
            Me.lbl_total_credit_amount.Location = New System.Drawing.Point(264, 65)
            Me.lbl_total_credit_amount.Name = "lbl_total_credit_amount"
            Me.lbl_total_credit_amount.Size = New System.Drawing.Size(120, 13)
            Me.lbl_total_credit_amount.TabIndex = 9
            Me.lbl_total_credit_amount.Text = "$0.00"
            Me.lbl_total_credit_amount.UseMnemonic = False
            '
            'lbl_total_debit_amount
            '
            Me.lbl_total_debit_amount.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
            Me.lbl_total_debit_amount.Appearance.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold)
            Me.lbl_total_debit_amount.Appearance.Options.UseFont = True
            Me.lbl_total_debit_amount.Appearance.Options.UseTextOptions = True
            Me.lbl_total_debit_amount.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
            Me.lbl_total_debit_amount.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None
            Me.lbl_total_debit_amount.Location = New System.Drawing.Point(264, 46)
            Me.lbl_total_debit_amount.Name = "lbl_total_debit_amount"
            Me.lbl_total_debit_amount.Size = New System.Drawing.Size(120, 13)
            Me.lbl_total_debit_amount.TabIndex = 8
            Me.lbl_total_debit_amount.Text = "$0.00"
            Me.lbl_total_debit_amount.UseMnemonic = False
            '
            'LabelControl9
            '
            Me.LabelControl9.Appearance.Font = New System.Drawing.Font("Comic Sans MS", 12.0!, System.Drawing.FontStyle.Bold)
            Me.LabelControl9.Appearance.Options.UseFont = True
            Me.LabelControl9.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None
            Me.LabelControl9.Location = New System.Drawing.Point(12, 12)
            Me.LabelControl9.Name = "LabelControl9"
            Me.LabelControl9.Size = New System.Drawing.Size(225, 28)
            Me.LabelControl9.TabIndex = 16
            Me.LabelControl9.Text = "Processing Batch Identifier:"
            Me.LabelControl9.UseMnemonic = False
            '
            'lbl_batch_id
            '
            Me.lbl_batch_id.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
            Me.lbl_batch_id.Appearance.Font = New System.Drawing.Font("Comic Sans MS", 12.0!, System.Drawing.FontStyle.Bold)
            Me.lbl_batch_id.Appearance.Options.UseFont = True
            Me.lbl_batch_id.Appearance.Options.UseTextOptions = True
            Me.lbl_batch_id.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
            Me.lbl_batch_id.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None
            Me.lbl_batch_id.Location = New System.Drawing.Point(243, 12)
            Me.lbl_batch_id.Name = "lbl_batch_id"
            Me.lbl_batch_id.Size = New System.Drawing.Size(141, 28)
            Me.lbl_batch_id.TabIndex = 17
            Me.lbl_batch_id.Text = "000000000000000"
            Me.lbl_batch_id.UseMnemonic = False
            '
            'Button_OK
            '
            Me.Button_OK.Anchor = System.Windows.Forms.AnchorStyles.Bottom
            Me.Button_OK.Location = New System.Drawing.Point(114, 231)
            Me.Button_OK.Name = "Button_OK"
            Me.Button_OK.Size = New System.Drawing.Size(75, 23)
            Me.Button_OK.TabIndex = 18
            Me.Button_OK.Tag = "cancel"
            Me.Button_OK.Text = "&Cancel"
            '
            'Button_Print
            '
            Me.Button_Print.Anchor = System.Windows.Forms.AnchorStyles.Bottom
            Me.Button_Print.Location = New System.Drawing.Point(208, 231)
            Me.Button_Print.Name = "Button_Print"
            Me.Button_Print.Size = New System.Drawing.Size(75, 23)
            Me.Button_Print.TabIndex = 19
            Me.Button_Print.Tag = "print"
            Me.Button_Print.Text = "&Print..."
            Me.Button_Print.Visible = False
            '
            'BackgroundWorker1
            '
            Me.BackgroundWorker1.WorkerSupportsCancellation = True
            '
            'form_RPS
            '
            Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
            Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
            Me.ClientSize = New System.Drawing.Size(396, 266)
            Me.Controls.Add(Me.Button_Print)
            Me.Controls.Add(Me.Button_OK)
            Me.Controls.Add(Me.lbl_batch_id)
            Me.Controls.Add(Me.LabelControl9)
            Me.Controls.Add(Me.lbl_prenote_count)
            Me.Controls.Add(Me.lbl_net_amounts)
            Me.Controls.Add(Me.lbl_gross_amounts)
            Me.Controls.Add(Me.lbl_billed_amounts)
            Me.Controls.Add(Me.lbl_total_batches)
            Me.Controls.Add(Me.lbl_total_items)
            Me.Controls.Add(Me.lbl_total_credit_amount)
            Me.Controls.Add(Me.lbl_total_debit_amount)
            Me.Controls.Add(Me.LabelControl_prenote_count)
            Me.Controls.Add(Me.LabelControl_total_net_amounts)
            Me.Controls.Add(Me.LabelControl_total_gross_amounts)
            Me.Controls.Add(Me.LabelControl_total_billed_amounts)
            Me.Controls.Add(Me.LabelControl_total_batches)
            Me.Controls.Add(Me.LabelControl_total_items)
            Me.Controls.Add(Me.LabelControl_total_credit_amount)
            Me.Controls.Add(Me.LabelControl_total_debit_amount)
            Me.Name = "form_RPS"
            Me.ToolTipController1.SetSuperTip(Me, Nothing)
            Me.Text = "Generate RPS File for MasterCard"
            CType(Me.DxErrorProvider1, System.ComponentModel.ISupportInitialize).EndInit()
            Me.ResumeLayout(False)

        End Sub
        Friend WithEvents LabelControl_total_debit_amount As DevExpress.XtraEditors.LabelControl
        Friend WithEvents LabelControl_total_credit_amount As DevExpress.XtraEditors.LabelControl
        Friend WithEvents LabelControl_total_items As DevExpress.XtraEditors.LabelControl
        Friend WithEvents LabelControl_total_batches As DevExpress.XtraEditors.LabelControl
        Friend WithEvents LabelControl_total_billed_amounts As DevExpress.XtraEditors.LabelControl
        Friend WithEvents LabelControl_total_gross_amounts As DevExpress.XtraEditors.LabelControl
        Friend WithEvents LabelControl_total_net_amounts As DevExpress.XtraEditors.LabelControl
        Friend WithEvents LabelControl_prenote_count As DevExpress.XtraEditors.LabelControl
        Friend WithEvents lbl_prenote_count As DevExpress.XtraEditors.LabelControl
        Friend WithEvents lbl_net_amounts As DevExpress.XtraEditors.LabelControl
        Friend WithEvents lbl_gross_amounts As DevExpress.XtraEditors.LabelControl
        Friend WithEvents lbl_billed_amounts As DevExpress.XtraEditors.LabelControl
        Friend WithEvents lbl_total_batches As DevExpress.XtraEditors.LabelControl
        Friend WithEvents lbl_total_items As DevExpress.XtraEditors.LabelControl
        Friend WithEvents lbl_total_credit_amount As DevExpress.XtraEditors.LabelControl
        Friend WithEvents lbl_total_debit_amount As DevExpress.XtraEditors.LabelControl
        Friend WithEvents LabelControl9 As DevExpress.XtraEditors.LabelControl
        Friend WithEvents lbl_batch_id As DevExpress.XtraEditors.LabelControl
        Friend WithEvents Button_OK As DevExpress.XtraEditors.SimpleButton
        Friend WithEvents Button_Print As DevExpress.XtraEditors.SimpleButton
        Friend WithEvents BackgroundWorker1 As System.ComponentModel.BackgroundWorker
    End Class
End Namespace
