Option Compare Binary
Option Explicit On 
Option Strict On

Imports System.Windows.Forms

Namespace RPPS.Payments
    Friend Class ArgParser
        Inherits DebtPlus.Utils.ArgParserBase

        ' The current time reported in the output files
        Public CurrentTime As Date = Now

        ' Output file name
        Public OutputFileName As String = String.Empty

        ''' <summary>
        ''' Bank to generate proposals
        ''' </summary>
        Private _bank As System.Int32 = -1
        Public ReadOnly Property Bank() As System.Int32
            Get
                Return _bank
            End Get
        End Property

        ''' <summary>
        ''' Current output RPPS file identifier in the rpps_files table
        ''' </summary>
        Private _rpps_file As System.Int32 = -1
        Public Property rpps_file() As System.Int32
            Get
                Return _rpps_file
            End Get
            Set(ByVal value As System.Int32)
                _rpps_file = value
            End Set
        End Property

        ''' <summary>
        ''' Create an instance of our class
        ''' </summary>
        Public Sub New()
            MyBase.New(New String() {"b", "f"})
        End Sub

        ''' <summary>
        ''' Generate the command usage information
        ''' </summary>
        Protected Overrides Sub OnUsage(ByVal errorInfo As String)
            ' deprecated
        End Sub

        Protected Overrides Function OnSwitch(ByVal switchSymbol As String, ByVal switchValue As String) As SwitchStatus
            Dim ss As SwitchStatus = DebtPlus.Utils.ArgParserBase.SwitchStatus.NoError

            Select Case switchSymbol.ToLower()
                Case "b"
                    Dim TempInteger As System.Int32
                    If Int32.TryParse(switchValue, Globalization.NumberStyles.Integer, System.Globalization.CultureInfo.InvariantCulture, TempInteger) Then
                        If TempInteger > 0 Then
                            _bank = TempInteger
                            Exit Select
                        End If
                    End If
                    ss = DebtPlus.Utils.ArgParserBase.SwitchStatus.YesError

                Case "f"
                    Dim TempInteger As System.Int32
                    If Int32.TryParse(switchValue, Globalization.NumberStyles.Integer, System.Globalization.CultureInfo.InvariantCulture, TempInteger) Then
                        If TempInteger > 0 Then
                            _rpps_file = TempInteger
                            Exit Select
                        End If
                    End If
                    ss = DebtPlus.Utils.ArgParserBase.SwitchStatus.YesError

                Case "?"
                    ss = DebtPlus.Utils.ArgParserBase.SwitchStatus.ShowUsage

                Case Else
                    ss = DebtPlus.Utils.ArgParserBase.SwitchStatus.YesError
            End Select
            Return ss
        End Function

        ''' <summary>
        ''' Process the completion of the parse operation
        ''' </summary>
        Protected Overrides Function OnDoneParse() As SwitchStatus
            Dim ss As SwitchStatus = DebtPlus.Utils.ArgParserBase.SwitchStatus.NoError

            If Bank < 0 Then
                Dim colBanks As System.Collections.Generic.List(Of DebtPlus.LINQ.bank) = DebtPlus.LINQ.Cache.bank.getList().FindAll(Function(s) s.type = "R" AndAlso s.ActiveFlag = True).ToList()
                If colBanks.Count = 1 Then
                    _bank = colBanks(0).Id
                    Return ss
                End If

                Using frm As New frmBank(colBanks)
                    If frm.ShowDialog() = DialogResult.OK Then
                        _bank = frm.bank
                    End If
                End Using

                If Bank <= 0 Then ss = DebtPlus.Utils.ArgParserBase.SwitchStatus.YesError
            End If

            ' If there is no name specified then ask for one.
            If ss = DebtPlus.Utils.ArgParserBase.SwitchStatus.NoError AndAlso OutputFileName = String.Empty Then
                With New System.Windows.Forms.OpenFileDialog
                    .AddExtension = True
                    .CheckFileExists = False
                    .CheckPathExists = True
                    .DefaultExt = ".txt"
                    .FileName = "*.txt"
                    .Filter = "Text Files (*.txt)|*.txt|Data Files (*.dat)|*.dat|All Files (*.*)|*.*"
                    .FilterIndex = 0
                    .InitialDirectory = System.Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments)
                    .RestoreDirectory = True
                    .ShowReadOnly = False

                    If .ShowDialog() = Windows.Forms.DialogResult.OK Then
                        OutputFileName = .FileName
                    Else
                        ss = DebtPlus.Utils.ArgParserBase.SwitchStatus.YesError
                    End If
                    .Dispose()
                End With
            End If

            Return ss
        End Function
    End Class
End Namespace
