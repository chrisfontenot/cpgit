Namespace RPPS.Payments
    <Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
    Partial Class frmBank
        Inherits DebtPlus.Data.Forms.DebtPlusForm

        'Form overrides dispose to clean up the component list.
        <System.Diagnostics.DebuggerNonUserCode()> _
        Protected Overrides Sub Dispose(ByVal disposing As Boolean)
            Try
                If disposing AndAlso components IsNot Nothing Then
                    components.Dispose()
                End If
            Finally
                MyBase.Dispose(disposing)
            End Try
        End Sub

        'Required by the Windows Form Designer
        Private components As System.ComponentModel.IContainer
        Public ToolTip1 As System.Windows.Forms.ToolTip
        Public WithEvents Button_Cancel As DevExpress.XtraEditors.SimpleButton
        Public WithEvents Button_OK As DevExpress.XtraEditors.SimpleButton
        Friend WithEvents LookUpEdit1 As DevExpress.XtraEditors.LookUpEdit
        Public WithEvents Label1 As DevExpress.XtraEditors.LabelControl
        'NOTE: The following procedure is required by the Windows Form Designer
        'It can be modified using the Windows Form Designer.
        'Do not modify it using the code editor.
        <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
            Me.components = New System.ComponentModel.Container()
            Me.ToolTip1 = New System.Windows.Forms.ToolTip(Me.components)
            Me.Button_Cancel = New DevExpress.XtraEditors.SimpleButton()
            Me.Button_OK = New DevExpress.XtraEditors.SimpleButton()
            Me.Label1 = New DevExpress.XtraEditors.LabelControl()
            Me.LookUpEdit1 = New DevExpress.XtraEditors.LookUpEdit()
            CType(Me.DxErrorProvider1, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.LookUpEdit1.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
            Me.SuspendLayout()
            '
            'DefaultLookAndFeel1
            '
            Me.DefaultLookAndFeel1.LookAndFeel.SkinName = "McSkin"
            '
            'Button_Cancel
            '
            Me.Button_Cancel.Appearance.Font = New System.Drawing.Font("Arial", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
            Me.Button_Cancel.Appearance.ForeColor = System.Drawing.SystemColors.ControlText
            Me.Button_Cancel.Appearance.Options.UseFont = True
            Me.Button_Cancel.Appearance.Options.UseForeColor = True
            Me.Button_Cancel.Cursor = System.Windows.Forms.Cursors.Default
            Me.Button_Cancel.DialogResult = System.Windows.Forms.DialogResult.Cancel
            Me.Button_Cancel.Location = New System.Drawing.Point(199, 112)
            Me.Button_Cancel.Name = "Button_Cancel"
            Me.Button_Cancel.RightToLeft = System.Windows.Forms.RightToLeft.No
            Me.Button_Cancel.Size = New System.Drawing.Size(73, 25)
            Me.Button_Cancel.TabIndex = 3
            Me.Button_Cancel.Text = "&Cancel"
            '
            'Button_OK
            '
            Me.Button_OK.Appearance.Font = New System.Drawing.Font("Arial", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
            Me.Button_OK.Appearance.ForeColor = System.Drawing.SystemColors.ControlText
            Me.Button_OK.Appearance.Options.UseFont = True
            Me.Button_OK.Appearance.Options.UseForeColor = True
            Me.Button_OK.Cursor = System.Windows.Forms.Cursors.Default
            Me.Button_OK.DialogResult = System.Windows.Forms.DialogResult.OK
            Me.Button_OK.Location = New System.Drawing.Point(104, 112)
            Me.Button_OK.Name = "Button_OK"
            Me.Button_OK.RightToLeft = System.Windows.Forms.RightToLeft.No
            Me.Button_OK.Size = New System.Drawing.Size(73, 25)
            Me.Button_OK.TabIndex = 2
            Me.Button_OK.Text = "&OK"
            '
            'Label1
            '
            Me.Label1.Appearance.BackColor = System.Drawing.Color.Transparent
            Me.Label1.Appearance.Font = New System.Drawing.Font("Arial", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
            Me.Label1.Appearance.ForeColor = System.Drawing.SystemColors.ControlText
            Me.Label1.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
            Me.Label1.Appearance.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Top
            Me.Label1.Appearance.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap
            Me.Label1.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None
            Me.Label1.Cursor = System.Windows.Forms.Cursors.Default
            Me.Label1.Location = New System.Drawing.Point(16, 12)
            Me.Label1.Name = "Label1"
            Me.Label1.Size = New System.Drawing.Size(345, 54)
            Me.Label1.TabIndex = 0
            Me.Label1.Text = "Please choose the appropriate RPPS bank account for the transactions. Each RPPS b" & _
        "ank account holds the originator ID and various other parameters that are needed" & _
        " to properly build the RPPS file."
            Me.Label1.UseMnemonic = False
            '
            'LookUpEdit1
            '
            Me.LookUpEdit1.Location = New System.Drawing.Point(18, 72)
            Me.LookUpEdit1.Name = "LookUpEdit1"
            Me.LookUpEdit1.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
            Me.LookUpEdit1.Properties.Columns.AddRange(New DevExpress.XtraEditors.Controls.LookUpColumnInfo() {New DevExpress.XtraEditors.Controls.LookUpColumnInfo("Id", "ID", 5, DevExpress.Utils.FormatType.Numeric, "f0", False, DevExpress.Utils.HorzAlignment.Far, DevExpress.Data.ColumnSortOrder.None, DevExpress.Utils.DefaultBoolean.[True]), New DevExpress.XtraEditors.Controls.LookUpColumnInfo("description", "Description", 20, DevExpress.Utils.FormatType.None, "", True, DevExpress.Utils.HorzAlignment.[Default], DevExpress.Data.ColumnSortOrder.Ascending, DevExpress.Utils.DefaultBoolean.[True])})
            Me.LookUpEdit1.Properties.DisplayMember = "description"
            Me.LookUpEdit1.Properties.NullText = ""
            Me.LookUpEdit1.Properties.ShowFooter = False
            Me.LookUpEdit1.Properties.ShowHeader = False
            Me.LookUpEdit1.Properties.ValueMember = "Id"
            Me.LookUpEdit1.Size = New System.Drawing.Size(345, 20)
            Me.LookUpEdit1.TabIndex = 4
            '
            'frmBank
            '
            Me.AcceptButton = Me.Button_OK
            Me.Appearance.Font = New System.Drawing.Font("Arial", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
            Me.Appearance.Options.UseFont = True
            Me.AutoScaleBaseSize = New System.Drawing.Size(5, 13)
            Me.CancelButton = Me.Button_Cancel
            Me.ClientSize = New System.Drawing.Size(375, 151)
            Me.Controls.Add(Me.LookUpEdit1)
            Me.Controls.Add(Me.Button_Cancel)
            Me.Controls.Add(Me.Button_OK)
            Me.Controls.Add(Me.Label1)
            Me.Cursor = System.Windows.Forms.Cursors.Default
            Me.Location = New System.Drawing.Point(4, 30)
            Me.MaximizeBox = False
            Me.MinimizeBox = False
            Me.Name = "frmBank"
            Me.Text = "Bank Account Selection"
            CType(Me.DxErrorProvider1, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.LookUpEdit1.Properties, System.ComponentModel.ISupportInitialize).EndInit()
            Me.ResumeLayout(False)

        End Sub
    End Class
End Namespace
