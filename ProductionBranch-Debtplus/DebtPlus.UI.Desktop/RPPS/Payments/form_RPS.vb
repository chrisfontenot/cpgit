#Region "Copyright 2000-2012 DebtPlus, L.L.C."
'{*******************************************************************}
'{                                                                   }
'{       DebtPlus Debt Management System                             }
'{                                                                   }
'{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
'{       ALL RIGHTS RESERVED                                         }
'{                                                                   }
'{   The entire contents of this file is protected by U.S. and       }
'{   International Copyright Laws. Unauthorized reproduction,        }
'{   reverse-engineering, and distribution of all or any portion of  }
'{   the code contained in this file is strictly prohibited and may  }
'{   result in severe civil and criminal penalties and will be       }
'{   prosecuted to the maximum extent possible under the law.        }
'{                                                                   }
'{   RESTRICTIONS                                                    }
'{                                                                   }
'{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
'{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
'{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
'{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
'{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
'{                                                                   }
'{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
'{   ADDITIONAL RESTRICTIONS.                                        }
'{                                                                   }
'{*******************************************************************}
#End Region
Imports DebtPlus.UI.Desktop.Disbursement.Common
Imports System.Drawing
Imports System.Windows.Forms

Namespace RPPS.Payments
    Friend Class form_RPS
        Implements Iform_RPS

        ''' <summary>
        ''' Create instances of this form
        ''' </summary>
        Public Sub New()
            MyBase.New()
            InitializeComponent()
        End Sub

        Private ap As ArgParser = Nothing
        Public Sub New(ByVal ap As ArgParser)
            MyClass.New()
            Me.ap = ap
            AddHandler Me.Load, AddressOf form_RPS_Load
            AddHandler BackgroundWorker1.DoWork, AddressOf BackgroundWorker1_DoWork
            AddHandler BackgroundWorker1.RunWorkerCompleted, AddressOf BackgroundWorker1_RunWorkerCompleted
            AddHandler Button_OK.Click, AddressOf Button_OK_Click
            AddHandler Button_Print.Click, AddressOf Button_OK_Click
        End Sub

        ''' <summary>
        ''' Routine to hold the processing class info
        ''' </summary>
        Public WriteOnly Property cls_item() As item_info
            Set(ByVal Value As item_info)
                ' do nothing. We don't need anything done here.
            End Set
        End Property

        ''' <summary>
        ''' Determine the output file name
        ''' </summary>
        Public Function Request_FileName(ByVal disbursement_register As System.Int32, ByVal Bank As System.Int32, ByVal FileType As String) As System.IO.StreamWriter Implements Iform_RPS.Request_FileName
            Return Request_FileName(disbursement_register, Bank, FileType, False)
        End Function

        Public Function Request_FileName(ByVal disbursement_register As System.Int32, ByVal Bank As System.Int32, ByVal FileType As String, ByVal UseLocalFileOnly As Boolean) As System.IO.StreamWriter Implements Iform_RPS.Request_FileName
            GenerateRPSPayments.FileName = ap.OutputFileName
            Return New System.IO.StreamWriter(New System.IO.FileStream(GenerateRPSPayments.FileName, System.IO.FileMode.Create, System.IO.FileAccess.Write, System.IO.FileShare.Read, 4096), System.Text.Encoding.ASCII, 4096)
        End Function

        Private Delegate Sub UpdateDecimalDelegate(ByVal value As Decimal)
        Private Delegate Sub UpdateIntegerDelegate(ByVal value As System.Int32)
        Private Delegate Sub UpdateStringDelegate(ByVal value As String)

#Region " total_debit "
        Private Sub update_total_debt(ByVal value As Decimal)
            If InvokeRequired Then
                BeginInvoke(New UpdateDecimalDelegate(AddressOf update_total_debt), New Object() {value})
            Else
                lbl_total_debit_amount.Text = String.Format("{0:c}", value)
            End If
        End Sub

        Public WriteOnly Property total_debt() As Decimal Implements Iform_RPS.total_debt
            Set(ByVal Value As Decimal)
                update_total_debt(Value)
            End Set
        End Property
#End Region

#Region " total_credit "
        Private Sub update_total_credit(ByVal value As Decimal)
            If InvokeRequired Then
                BeginInvoke(New UpdateDecimalDelegate(AddressOf update_total_credit), New Object() {value})
            Else
                lbl_total_credit_amount.Text = String.Format("{0:c}", value)
            End If
        End Sub

        Public WriteOnly Property total_credit() As Decimal Implements Iform_RPS.total_credit
            Set(ByVal Value As Decimal)
                update_total_credit(Value)
            End Set
        End Property
#End Region

#Region " total_items "
        Private Sub update_total_items(ByVal value As System.Int32)
            If InvokeRequired Then
                BeginInvoke(New UpdateIntegerDelegate(AddressOf update_total_items), New Object() {value})
            Else
                lbl_total_items.Text = String.Format("{0:n0}", value)
            End If
        End Sub

        Public WriteOnly Property total_items() As System.Int32 Implements Iform_RPS.total_items
            Set(ByVal Value As System.Int32)
                update_total_items(Value)
            End Set
        End Property
#End Region

#Region " total_batches "
        Private Sub update_total_batches(ByVal value As System.Int32)
            If InvokeRequired Then
                BeginInvoke(New UpdateIntegerDelegate(AddressOf update_total_batches), New Object() {value})
            Else
                lbl_total_batches.Text = String.Format("{0:n0}", value)
            End If
        End Sub

        Public WriteOnly Property total_batches() As System.Int32 Implements Iform_RPS.total_batches
            Set(ByVal Value As System.Int32)
                update_total_batches(Value)
            End Set
        End Property
#End Region

#Region " total_billed "
        Private Sub update_total_billed(ByVal value As Decimal)
            If InvokeRequired Then
                BeginInvoke(New UpdateDecimalDelegate(AddressOf update_total_billed), New Object() {value})
            Else
                lbl_billed_amounts.Text = String.Format("{0:c}", value)
            End If
        End Sub

        Public WriteOnly Property total_billed() As Decimal Implements Iform_RPS.total_billed
            Set(ByVal Value As Decimal)
                update_total_billed(Value)
            End Set
        End Property
#End Region

#Region " total_gross "
        Private Sub update_total_gross(ByVal value As Decimal)
            If InvokeRequired Then
                BeginInvoke(New UpdateDecimalDelegate(AddressOf update_total_gross), New Object() {value})
            Else
                lbl_gross_amounts.Text = String.Format("{0:c}", value)
            End If
        End Sub

        Public WriteOnly Property total_gross() As Decimal Implements Iform_RPS.total_gross
            Set(ByVal Value As Decimal)
                update_total_gross(Value)
            End Set
        End Property
#End Region

#Region " total_net "
        Private Sub update_total_net(ByVal value As Decimal)
            If InvokeRequired Then
                BeginInvoke(New UpdateDecimalDelegate(AddressOf update_total_net), New Object() {value})
            Else
                lbl_net_amounts.Text = String.Format("{0:c}", value)
            End If
        End Sub

        Public WriteOnly Property total_net() As Decimal Implements Iform_RPS.total_net
            Set(ByVal Value As Decimal)
                update_total_net(Value)
            End Set
        End Property
#End Region

#Region " total_prenotes "
        Private Sub update_total_prenotes(ByVal value As System.Int32)
            If InvokeRequired Then
                BeginInvoke(New UpdateIntegerDelegate(AddressOf update_total_prenotes), New Object() {value})
            Else
                lbl_prenote_count.Text = String.Format("{0:n0}", value)
            End If
        End Sub

        Public WriteOnly Property total_prenotes() As System.Int32 Implements Iform_RPS.total_prenotes
            Set(ByVal Value As System.Int32)
                update_total_prenotes(Value)
            End Set
        End Property
#End Region

#Region " batch "
        Private Sub update_batch(ByVal value As String)
            If InvokeRequired Then
                BeginInvoke(New UpdateStringDelegate(AddressOf update_batch), New Object() {value})
            Else
                lbl_batch_id.Text = value
            End If
        End Sub

        Public WriteOnly Property batch() As String Implements Iform_RPS.batch
            Set(ByVal Value As String)
                update_batch(Value)
            End Set
        End Property
#End Region

        Private Sub form_RPS_Load(ByVal sender As Object, ByVal e As System.EventArgs)
            BackgroundWorker1.RunWorkerAsync()
        End Sub

        Private Sub BackgroundWorker1_DoWork(ByVal sender As Object, ByVal e As System.ComponentModel.DoWorkEventArgs)
            With New GenerateRPSPayments
                .bank = ap.Bank
                If Not .Process_Payments(Me) Then
                    DebtPlus.Data.Forms.MessageBox.Show("An error prevented the generation of the payment file. Please run the utility again.", "Sorry, an error occurred", MessageBoxButtons.OK, MessageBoxIcon.Error)
                End If
            End With
        End Sub

        Private Sub BackgroundWorker1_RunWorkerCompleted(ByVal sender As Object, ByVal e As System.ComponentModel.RunWorkerCompletedEventArgs)

            ' Enable the buttons on the form
            With Button_OK
                .Text = "&OK"
                .Tag = "ok"
                .Visible = True
            End With

            Button_Print.Visible = True
        End Sub

        Private Sub Button_OK_Click(ByVal sender As Object, ByVal e As System.EventArgs)
            Select Case Convert.ToString(CType(sender, DevExpress.XtraEditors.SimpleButton).Tag)
                Case "ok"
                    Close()

                Case "cancel"
                    BackgroundWorker1.CancelAsync()
                    Button_OK.Enabled = False

                Case "print"
                    PrintForm()
            End Select
        End Sub

        Private printFont As System.Drawing.Font
        Private Sub PrintForm()
            Try
                printFont = New Font("Arial", 10)
                Dim pd As New System.Drawing.Printing.PrintDocument
                AddHandler pd.PrintPage, AddressOf pd_PrintPage

#If 0 Then ' Direct to printer
                pd.Print()

#Else      ' Use preview form first
                Using PreviewDialog As New PrintPreviewDialog()
                    With PreviewDialog
                        .ShowIcon = False
                        .Document = pd
                        .ShowDialog(Me)
                    End With
                End Using
#End If

            Catch ex As Exception
                DebtPlus.Data.Forms.MessageBox.Show(ex.Message, "Error printing document")
            End Try
        End Sub

        ''' <summary>
        ''' Generate the printer document
        ''' </summary>
        Private Sub pd_PrintPage(ByVal sender As Object, ByVal ev As System.Drawing.Printing.PrintPageEventArgs)
            Dim leftMargin As Single = ev.MarginBounds.Left
            Dim topMargin As Single = ev.MarginBounds.Top
            Dim yPos As Single = topMargin
            Dim fmt As New System.Drawing.StringFormat
            Dim LineNo As System.Int32 = 0

            ' Generate the font for the header
            Dim HdrFont As New System.Drawing.Font(printFont.FontFamily, 20, FontStyle.Bold, GraphicsUnit.Point, CByte(0))
            fmt.Alignment = StringAlignment.Near
            ev.Graphics.DrawString(Text, HdrFont, Brushes.Black, leftMargin + 0, yPos, fmt)
            yPos += (HdrFont.GetHeight(ev.Graphics) * 2)
            HdrFont.Dispose()

            ' Print each line of the file.
            ev.HasMorePages = True
            While ev.HasMorePages

                Dim TitleString As String = String.Empty
                Dim ValueString As String = String.Empty

                Select Case LineNo
                    Case 0
                        TitleString = "Output Text File"
                        ValueString = ap.OutputFileName

                        ' Try to measure the font width
                        fmt.Alignment = StringAlignment.Far
                        Dim NormalHeight As Single = printFont.GetHeight(ev.Graphics)
                        Do
                            Dim WidthSize As System.Drawing.SizeF = ev.Graphics.MeasureString("..." + ValueString, printFont, New System.Drawing.SizeF(350, NormalHeight * 2), fmt)
                            If WidthSize.Height <> NormalHeight * 2 Then Exit Do
                            If ValueString.Length <= 0 Then Exit Do
                            ValueString = ValueString.Substring(1)
                        Loop
                        If ValueString <> ap.OutputFileName Then ValueString = "..." + ValueString

                    Case 1
                        TitleString = "Date" : ValueString = String.Format("{0:MM/dd/yyyy HH:mm}", ap.CurrentTime)

                    Case 2, 3, 8
                        TitleString = String.Empty : ValueString = String.Empty

                    Case 4
                        TitleString = LabelControl_total_debit_amount.Text : ValueString = lbl_total_debit_amount.Text
                    Case 5
                        TitleString = LabelControl_total_credit_amount.Text : ValueString = lbl_total_credit_amount.Text
                    Case 6
                        TitleString = LabelControl_total_items.Text : ValueString = lbl_total_items.Text
                    Case 7
                        TitleString = LabelControl_total_batches.Text : ValueString = lbl_total_batches.Text

                    Case 9
                        TitleString = LabelControl_total_billed_amounts.Text : ValueString = lbl_billed_amounts.Text
                    Case 10
                        TitleString = LabelControl_total_gross_amounts.Text : ValueString = lbl_gross_amounts.Text
                    Case 11
                        TitleString = LabelControl_total_net_amounts.Text : ValueString = lbl_net_amounts.Text
                    Case 12
                        TitleString = LabelControl_prenote_count.Text : ValueString = lbl_prenote_count.Text
                        ev.HasMorePages = False
                End Select

                ' Generate the position on the form and print the line
                fmt.Alignment = StringAlignment.Near
                ev.Graphics.DrawString(TitleString, printFont, Brushes.Black, leftMargin + 0, yPos, fmt)
                fmt.Alignment = StringAlignment.Far
                ev.Graphics.DrawString(ValueString, printFont, Brushes.Black, leftMargin + 450, yPos, fmt)
                yPos += printFont.GetHeight(ev.Graphics)

                LineNo += 1
            End While
        End Sub
    End Class
End Namespace
