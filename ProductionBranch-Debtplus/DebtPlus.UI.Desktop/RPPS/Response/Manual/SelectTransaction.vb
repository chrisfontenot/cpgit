#Region "Copyright 2000-2012 DebtPlus, L.L.C."
'{*******************************************************************}
'{                                                                   }
'{       DebtPlus Debt Management System                             }
'{                                                                   }
'{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
'{       ALL RIGHTS RESERVED                                         }
'{                                                                   }
'{   The entire contents of this file is protected by U.S. and       }
'{   International Copyright Laws. Unauthorized reproduction,        }
'{   reverse-engineering, and distribution of all or any portion of  }
'{   the code contained in this file is strictly prohibited and may  }
'{   result in severe civil and criminal penalties and will be       }
'{   prosecuted to the maximum extent possible under the law.        }
'{                                                                   }
'{   RESTRICTIONS                                                    }
'{                                                                   }
'{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
'{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
'{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
'{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
'{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
'{                                                                   }
'{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
'{   ADDITIONAL RESTRICTIONS.                                        }
'{                                                                   }
'{*******************************************************************}
#End Region

Option Compare Binary
Option Explicit On
Option Strict On

Namespace RPPS.Response.Manual
    Public Class SelectTransaction

        Private ReadOnly vue As System.Data.DataView
        Public drv As System.Data.DataRowView
        Public Sub New(ByVal vue As System.Data.DataView)
            MyBase.New()
            InitializeComponent()
            AddHandler Me.Load, AddressOf SelectTransaction_Load

            Me.vue = vue
        End Sub

        Private Sub SelectTransaction_Load(ByVal sender As Object, ByVal e As System.EventArgs)

            ' Load the grid with the possible responses
            With GridControl1
                .DataSource = vue
                .Refresh()
            End With

            ' Add the handlers to the processing logic
            AddHandler GridView1.FocusedRowChanged, AddressOf GridView1_FocusedRowChanged
            AddHandler GridView1.DoubleClick, AddressOf GridView1_DoubleClick
            AddHandler GridControl1.DoubleClick, AddressOf GridControl_DoubleClick

            ' Enable processing to find the appropriate item
            GridView1.FocusedRowHandle = GridView1.GetRowHandle(0)
            Perform_FocusedRowHandleChanged(GridView1.FocusedRowHandle)
        End Sub

        Private Sub GridView1_FocusedRowChanged(ByVal sender As Object, ByVal e As DevExpress.XtraGrid.Views.Base.FocusedRowChangedEventArgs)
            Perform_FocusedRowHandleChanged(e.FocusedRowHandle)
        End Sub

        Private Sub Perform_FocusedRowHandleChanged(ByVal NewRowHandle As System.Int32)
            Try

                ' If there is a row then process the double-click event. Ignore it if there is no row.
                If NewRowHandle >= 0 Then
                    drv = CType(GridView1.GetRow(NewRowHandle), System.Data.DataRowView)
                End If

            Catch ex As Exception
                drv = Nothing

            End Try

            ' Enable the OK button if the creditor is specified
            Button_OK.Enabled = drv IsNot Nothing
        End Sub

        Private Sub GridView1_DoubleClick(ByVal sender As Object, ByVal e As System.EventArgs)

            If Button_OK.Enabled Then

                ' Find the row targetted as the double-click item
                Dim gv As DevExpress.XtraGrid.Views.Grid.GridView = CType(sender, DevExpress.XtraGrid.Views.Grid.GridView)
                Dim ctl As DevExpress.XtraGrid.GridControl = CType(gv.GridControl, DevExpress.XtraGrid.GridControl)
                Dim hi As DevExpress.XtraGrid.Views.Grid.ViewInfo.GridHitInfo = gv.CalcHitInfo((ctl.PointToClient(Windows.Forms.Control.MousePosition)))

                ' If there is a row then process the double-click event. Ignore it if there is no row.
                Dim ControlRow As System.Int32 = hi.RowHandle
                If ControlRow >= 0 Then
                    drv = CType(gv.GetRow(ControlRow), System.Data.DataRowView)
                End If

                ' From the datasource, we can find the client in the table.
                If drv IsNot Nothing Then
                    DialogResult = Windows.Forms.DialogResult.OK
                End If
            End If
        End Sub

        Private Sub GridControl_DoubleClick(ByVal sender As Object, ByVal e As System.EventArgs)

            ' Find the row targetted as the double-click item
            Dim ctl As DevExpress.XtraGrid.GridControl = CType(sender, DevExpress.XtraGrid.GridControl)
            Dim gv As DevExpress.XtraGrid.Views.Grid.GridView = CType(ctl.DefaultView, DevExpress.XtraGrid.Views.Grid.GridView)
            Dim hi As DevExpress.XtraGrid.Views.Grid.ViewInfo.GridHitInfo = gv.CalcHitInfo((ctl.PointToClient(Windows.Forms.Control.MousePosition)))

            ' If there is a row then process the double-click event. Ignore it if there is no row.
            Dim ControlRow As System.Int32 = hi.RowHandle
            If ControlRow >= 0 Then
                drv = CType(gv.GetRow(ControlRow), System.Data.DataRowView)
            End If

            ' From the datasource, we can find the client in the table.
            If drv IsNot Nothing Then
                DialogResult = Windows.Forms.DialogResult.OK
            End If
        End Sub
    End Class
End Namespace
