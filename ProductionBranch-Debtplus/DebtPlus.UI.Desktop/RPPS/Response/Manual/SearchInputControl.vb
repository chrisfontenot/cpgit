#Region "Copyright 2000-2012 DebtPlus, L.L.C."
'{*******************************************************************}
'{                                                                   }
'{       DebtPlus Debt Management System                             }
'{                                                                   }
'{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
'{       ALL RIGHTS RESERVED                                         }
'{                                                                   }
'{   The entire contents of this file is protected by U.S. and       }
'{   International Copyright Laws. Unauthorized reproduction,        }
'{   reverse-engineering, and distribution of all or any portion of  }
'{   the code contained in this file is strictly prohibited and may  }
'{   result in severe civil and criminal penalties and will be       }
'{   prosecuted to the maximum extent possible under the law.        }
'{                                                                   }
'{   RESTRICTIONS                                                    }
'{                                                                   }
'{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
'{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
'{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
'{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
'{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
'{                                                                   }
'{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
'{   ADDITIONAL RESTRICTIONS.                                        }
'{                                                                   }
'{*******************************************************************}
#End Region
Imports DebtPlus.Utils
Imports System.Data.SqlClient
Imports System.Windows.Forms

Namespace RPPS.Response.Manual
    Public Class SearchInputControl

        Public Sub New()
            MyBase.New()
            InitializeComponent()
            AddHandler LookupEdit3.Resize, AddressOf LookupEdit3_Resize
            AddHandler LookupEdit3.ProcessNewValue, AddressOf LookupEdit3_ProcessNewValue
            AddHandler LookupEdit3.EditValueChanged, AddressOf TextEdit1_EditValueChanged
            AddHandler LookupEdit2.EditValueChanged, AddressOf LookupEdit2_EditValueChanged
            AddHandler LookUpEdit1.EditValueChanged, AddressOf LookUpEdit1_EditValueChanged
            AddHandler SimpleButton2.Click, AddressOf SimpleButton2_Click
            AddHandler SimpleButton1.Click, AddressOf SimpleButton1_Click
            AddHandler Me.Load, AddressOf SearchInputControl_Load
        End Sub

        Public Class SelectedArgs
            Inherits System.EventArgs

            Public rpps_transaction As Int32
            Public client As Int32
            Public account_number As String
            Public creditor As String
            Public gross As Decimal
            Public deduct As Decimal
            Public creditor_type As String

            Public ReadOnly Property Net() As Decimal
                Get
                    If creditor_type = "D" Then
                        Return gross - deduct
                    End If
                    Return gross
                End Get
            End Property
        End Class

        Public Delegate Sub SelectedEventHandler(ByVal Sender As Object, ByVal e As SelectedArgs)

        Public Event Cancelled As EventHandler
        Public Event Selected As SelectedEventHandler

        Public TraceNumber As String = String.Empty

        ' If we are entering an account number then we need to have it defined in the table. So, here it is.
        Private AccountNumberRow As System.Data.DataRowView

        Private Sub SimpleButton1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
            With LookupEdit3
                Dim drv As System.Data.DataRowView = CType(.GetSelectedDataRow, System.Data.DataRowView)
                Dim args As New SelectedArgs()
                With args
                    .rpps_transaction = Convert.ToInt32(drv("rpps_transaction"))
                    .account_number = Convert.ToString(drv("account_number"))
                    .client = Convert.ToInt32(drv("client"))
                    .creditor = Convert.ToString(drv("creditor"))
                    .creditor_type = Convert.ToString(drv("creditor_type"))
                    .deduct = Convert.ToDecimal(drv("fairshare_amt"))
                    .gross = Convert.ToDecimal(drv("debit_amt"))
                End With
                RaiseEvent Selected(Me, args)
            End With
        End Sub

        Private Sub SimpleButton2_Click(ByVal sender As Object, ByVal e As System.EventArgs)
            RaiseEvent Cancelled(Me, EventArgs.Empty)
        End Sub

        Public Sub RefreshData()

            ' Restore the previously selected file
            If LookUpEdit1.EditValue Is Nothing OrElse LookUpEdit1.EditValue Is System.DBNull.Value Then
                LookUpEdit1.EditValue = LastFile

                LookUpEdit2_LoadItems()
                LookupEdit2.EditValue = LastBatch
            End If

            SimpleButton1.Enabled = Not HasErrors()
        End Sub

        Private Function HasErrors() As Boolean
            Dim answer As Boolean = True
            Do
                If LookUpEdit1.EditValue Is Nothing OrElse LookUpEdit1.EditValue Is System.DBNull.Value Then Exit Do
                If LookupEdit2.EditValue Is Nothing OrElse LookupEdit2.EditValue Is System.DBNull.Value Then Exit Do
                If LookupEdit3.Text.Trim().Length = 0 Then Exit Do
                answer = False
                Exit Do
            Loop
            Return answer
        End Function

        Private Sub SearchInputControl_Load(ByVal sender As Object, ByVal e As System.EventArgs)

            ' Load the waiting cursor before reading the database
            Dim current_cursor As System.Windows.Forms.Cursor = System.Windows.Forms.Cursor.Current
            Try
                System.Windows.Forms.Cursor.Current = Cursors.WaitCursor

                Dim FilesTable As System.Data.DataTable = rpps_files()
                Dim BatchesTable As System.Data.DataTable = rpps_batches()

                ' Load the data relation between the batches/files if it is missing
                Dim rel As System.Data.DataRelation = ds.Relations("rpps_files_rpps_batches")
                If rel Is Nothing Then
                    rel = New System.Data.DataRelation("rpps_files_rpps_batches", FilesTable.Columns("rpps_file"), BatchesTable.Columns("rpps_file"))
                    ds.Relations.Add(rel)
                End If

                ' Load the control with the list of rpps files
                LookUpEdit1.Properties.DataSource = New System.Data.DataView(FilesTable, String.Empty, "[date_created] desc, [rpps_file] desc", DataViewRowState.CurrentRows)
                LookUpEdit2_LoadItems()

            Finally
                System.Windows.Forms.Cursor.Current = current_cursor
            End Try
        End Sub

        Private Function rpps_files() As System.Data.DataTable
            Dim tbl As System.Data.DataTable = ds.Tables("rpps_files")
            If tbl Is Nothing Then
                Try
                    Using cmd As SqlClient.SqlCommand = New SqlCommand
                        With cmd
                            .Connection = New SqlConnection(DebtPlus.LINQ.SQLInfoClass.getDefault().ConnectionString)
                            .CommandText = "SELECT [rpps_file],[bank],[date_created],[created_by] FROM rpps_files WHERE [file_type]='EFT'"
                            .CommandType = CommandType.Text
                        End With

                        Using da As New SqlClient.SqlDataAdapter(cmd)
                            da.AcceptChangesDuringFill = True
                            da.Fill(ds, "rpps_files")
                        End Using
                    End Using

                    tbl = ds.Tables("rpps_files")
                    With tbl
                        .PrimaryKey = New System.Data.DataColumn() {.Columns("rpps_file")}
                    End With

                Catch ex As Exception
                    DebtPlus.UI.Common.ErrorHandling.HandleErrors(ex, "Error reading rpps_files table")
                End Try
            End If

            Return tbl
        End Function

        Private Function rpps_batches() As System.Data.DataTable
            Dim tbl As System.Data.DataTable = ds.Tables("rpps_batches")
            If tbl Is Nothing Then
                Try
                    Using cmd As SqlClient.SqlCommand = New SqlCommand
                        With cmd
                            .Connection = New SqlConnection(DebtPlus.LINQ.SQLInfoClass.getDefault().ConnectionString)
                            .CommandText = "SELECT b.[rpps_batch],b.[rpps_file],b.[rpps_biller_id],b.[biller_name] FROM rpps_batches b INNER JOIN rpps_files f on f.rpps_file = b.rpps_file WHERE f.[file_type]='EFT'"
                            .CommandType = CommandType.Text
                        End With

                        Using da As New SqlClient.SqlDataAdapter(cmd)
                            da.AcceptChangesDuringFill = True
                            da.Fill(ds, "rpps_batches")
                        End Using

                        tbl = ds.Tables("rpps_batches")
                        With tbl
                            .PrimaryKey = New System.Data.DataColumn() {.Columns("rpps_batch")}
                        End With
                    End Using

                Catch ex As Exception
                    DebtPlus.UI.Common.ErrorHandling.HandleErrors(ex, "Error reading rpps_batches table")
                End Try
            End If

            Return tbl
        End Function

        Private Function rpps_transactions(ByVal rpps_batch As Int32) As System.Data.DataView
            Dim tbl As System.Data.DataTable = ds.Tables("rpps_transactions")

            ' If there is a table then determine if there are transactions for this batch. If so, we have been here before....
            If tbl IsNot Nothing Then
                Dim vue As New System.Data.DataView(tbl, String.Format("[rpps_batch]={0:f0}", rpps_batch), "account_number", DataViewRowState.CurrentRows)
                If vue.Count > 0 Then Return vue
                vue.Dispose()
            End If

            ' Read the transactions for that batch
            Dim current_cursor As System.Windows.Forms.Cursor = System.Windows.Forms.Cursor.Current
            Try
                System.Windows.Forms.Cursor.Current = Cursors.WaitCursor
                Using cmd As SqlClient.SqlCommand = New SqlCommand
                    With cmd
                        .Connection = New SqlConnection(DebtPlus.LINQ.SQLInfoClass.getDefault().ConnectionString)
                        .CommandText = "SELECT t.[rpps_transaction], t.[rpps_batch], t.[rpps_file], rrc.[account_number], t.[trace_number_first], rrc.client, rrc.creditor, rrc.debit_amt, rrc.creditor_type, rrc.fairshare_amt FROM rpps_transactions t INNER JOIN registers_client_creditor rrc on t.client_creditor_register = rrc.client_creditor_register WHERE t.[rpps_batch]=@rpps_batch"
                        .CommandType = CommandType.Text
                        .Parameters.Add("@rpps_batch", SqlDbType.Int).Value = rpps_batch
                    End With

                    Using da As New SqlClient.SqlDataAdapter(cmd)
                        da.AcceptChangesDuringFill = True
                        da.Fill(ds, "rpps_transactions")
                    End Using
                End Using

                tbl = ds.Tables("rpps_transactions")
                With tbl
                    .PrimaryKey = New System.Data.DataColumn() {.Columns("rpps_transaction")}
                    If Not .Columns.Contains("deduct") Then
                        .Columns.Add("deduct", GetType(Decimal), "iif([creditor_type]='D',fairshare_amt,0)")
                    End If

                    If Not .Columns.Contains("net") Then
                        .Columns.Add("net", GetType(Decimal), "[debit_amt]-[deduct]")
                    End If
                End With

            Catch ex As Exception
                DebtPlus.UI.Common.ErrorHandling.HandleErrors(ex, "Error reading rpps_transactions table")

            Finally
                System.Windows.Forms.Cursor.Current = current_cursor
            End Try

            ' Return the view to the data
            Return New System.Data.DataView(tbl, String.Format("[rpps_batch]={0:f0}", rpps_batch), "account_number", DataViewRowState.CurrentRows)
        End Function

        Private Sub LookUpEdit1_EditValueChanged(ByVal sender As Object, ByVal e As System.EventArgs)
            LastFile = LookUpEdit1.EditValue
            LookUpEdit2_LoadItems()
        End Sub

        Private Sub LookUpEdit2_LoadItems()
            Dim vue As System.Data.DataView

            If LookUpEdit1.EditValue Is Nothing OrElse LookUpEdit1.EditValue Is System.DBNull.Value Then
                vue = Nothing
            Else
                vue = New System.Data.DataView(rpps_batches, String.Format("[rpps_file]='{0}'", LookUpEdit1.EditValue), "rpps_biller_id", DataViewRowState.CurrentRows)
            End If

            LookupEdit2.Properties.DataSource = vue
            SimpleButton1.Enabled = Not HasErrors()
        End Sub

        Private Sub TextEdit1_EditValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs)
            CalcEdit1.EditValue = LookupEdit3.GetColumnValue("debit_amt")
            CalcEdit2.EditValue = LookupEdit3.GetColumnValue("net")
            TraceNumber = DebtPlus.Utils.Nulls.DStr(LookupEdit3.GetColumnValue("trace_number_first"))

            SimpleButton1.Enabled = Not HasErrors()
        End Sub

        Private Sub LookupEdit2_EditValueChanged(ByVal sender As Object, ByVal e As System.EventArgs)
            LastBatch = LookupEdit2.EditValue
            LookupEdit3_LoadItems()
            SimpleButton1.Enabled = Not HasErrors()
        End Sub

        Private Sub LookupEdit3_LoadItems()
            Dim BatchID As System.Int32
            If LookupEdit2.EditValue IsNot Nothing AndAlso LookupEdit2.EditValue IsNot System.DBNull.Value Then
                BatchID = Convert.ToInt32(LookupEdit2.GetColumnValue("rpps_batch"))
            Else
                BatchID = -1
            End If

            If BatchID <= 0 Then
                LookupEdit3.EditValue = Nothing
                LookupEdit3.Properties.DataSource = Nothing
            Else
                Dim vue As System.Data.DataView = rpps_transactions(BatchID)
                LookupEdit3.Properties.DataSource = vue
            End If
        End Sub

        Private Sub LookupEdit3_ProcessNewValue(ByVal sender As Object, ByVal e As DevExpress.XtraEditors.Controls.ProcessNewValueEventArgs)

            ' If there is a batch then you must choose one from the list of items. You can't just type a value in.
            If LookupEdit2.EditValue Is Nothing OrElse LookupEdit2.EditValue Is System.DBNull.Value Then
                Dim vue As System.Data.DataView = CType(LookupEdit3.Properties.DataSource, System.Data.DataView)
                If vue Is Nothing Then

                    ' There is no pending view. Is there a table that we can use?
                    Dim tbl As System.Data.DataTable = ds.Tables("rpps_transactions")
                    If tbl Is Nothing Then
                        Try
                            ' No table either. Define the schema for the table but leave it empty.
                            Using cmd As SqlClient.SqlCommand = New SqlCommand
                                With cmd
                                    .Connection = New SqlConnection(DebtPlus.LINQ.SQLInfoClass.getDefault().ConnectionString)
                                    .CommandText = "SELECT t.[rpps_transaction], t.[rpps_batch], t.[rpps_file], rrc.[account_number], t.[trace_number_first], rrc.client, rrc.debit_amt, rrc.creditor_type, rrc.fairshare_amt FROM rpps_transactions t INNER JOIN registers_client_creditor rrc on t.client_creditor_register = rrc.client_creditor_register"
                                    .CommandType = CommandType.Text
                                End With

                                Using da As New SqlClient.SqlDataAdapter(cmd)
                                    da.AcceptChangesDuringFill = True
                                    da.FillSchema(ds, SchemaType.Source, "rpps_transactions")
                                End Using
                            End Using

                            tbl = ds.Tables("rpps_transactions")
                            With tbl
                                .PrimaryKey = New System.Data.DataColumn() {.Columns("rpps_transaction")}
                                If Not .Columns.Contains("deduct") Then
                                    .Columns.Add("deduct", GetType(Decimal), "iif([creditor_type]='D',fairshare_amt,0)")
                                End If

                                If Not .Columns.Contains("net") Then
                                    .Columns.Add("net", GetType(Decimal), "[debit_amt]-[deduct]")
                                End If
                            End With

                        Catch ex As Exception
                            DebtPlus.UI.Common.ErrorHandling.HandleErrors(ex, "Error reading rpps_transactions table")
                        End Try
                    End If

                    vue = New System.Data.DataView(tbl, "[rpps_batch] IS NULL", "account_number", DataViewRowState.CurrentRows)
                    LookupEdit3.Properties.DataSource = vue
                End If

                ' If there is no added row then add a new row to the list.
                If AccountNumberRow Is Nothing Then
                    AccountNumberRow = vue.AddNew
                    AccountNumberRow.BeginEdit()

                    ' Specify default values for the dummy row
                    AccountNumberRow("rpps_transaction") = -1
                    AccountNumberRow("trace_number_first") = String.Empty
                    AccountNumberRow("client") = -1
                    AccountNumberRow("debit_amt") = 0D
                    AccountNumberRow("creditor_type") = "N"
                    AccountNumberRow("fairshare_amt") = 0D
                    AccountNumberRow("rpps_batch") = System.DBNull.Value
                End If

                ' Set the account number in the new row and return success to the caller.
                AccountNumberRow("account_number") = e.DisplayValue
                e.Handled = True
            End If
        End Sub

        Private Sub LookupEdit3_Resize(ByVal sender As Object, ByVal e As System.EventArgs)
            LookupEdit3.Properties.PopupWidth = LookupEdit3.Width + 280
        End Sub
    End Class
End Namespace
