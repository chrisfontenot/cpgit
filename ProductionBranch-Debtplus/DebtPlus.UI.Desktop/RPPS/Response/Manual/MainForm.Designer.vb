Namespace RPPS.Response.Manual
    <Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
    Partial Class MainForm
        Inherits DebtPlus.Data.Forms.DebtPlusForm

        'Form overrides dispose to clean up the component list.
        <System.Diagnostics.DebuggerNonUserCode()> _
        Protected Overrides Sub Dispose(ByVal disposing As Boolean)
            Try
                If disposing AndAlso components IsNot Nothing Then
                    components.Dispose()
                End If
            Finally
                MyBase.Dispose(disposing)
            End Try
        End Sub

        'Required by the Windows Form Designer
        Private components As System.ComponentModel.IContainer

        'NOTE: The following procedure is required by the Windows Form Designer
        'It can be modified using the Windows Form Designer.  
        'Do not modify it using the code editor.
        <System.Diagnostics.DebuggerStepThrough()> _
        Private Sub InitializeComponent()
            Me.LabelControl1 = New DevExpress.XtraEditors.LabelControl
            Me.LabelControl2 = New DevExpress.XtraEditors.LabelControl
            Me.LookUpEdit1 = New DevExpress.XtraEditors.LookUpEdit
            Me.LabelControl3 = New DevExpress.XtraEditors.LabelControl
            Me.LabelControl4 = New DevExpress.XtraEditors.LabelControl
            Me.SimpleButton1 = New DevExpress.XtraEditors.SimpleButton
            Me.SimpleButton2 = New DevExpress.XtraEditors.SimpleButton
            Me.LookUpEdit2 = New DevExpress.XtraEditors.LookUpEdit
            Me.LabelControl5 = New DevExpress.XtraEditors.LabelControl
            Me.gross = New DevExpress.XtraEditors.LabelControl
            Me.net = New DevExpress.XtraEditors.LabelControl
            Me.creditor = New DevExpress.XtraEditors.LabelControl
            Me.client = New DevExpress.XtraEditors.LabelControl
            Me.LabelControl8 = New DevExpress.XtraEditors.LabelControl
            Me.LabelControl9 = New DevExpress.XtraEditors.LabelControl
            Me.account_number = New DevExpress.XtraEditors.LabelControl
            Me.LabelControl11 = New DevExpress.XtraEditors.LabelControl
            Me.TextEdit1 = New DevExpress.XtraEditors.ButtonEdit
            CType(Me.DxErrorProvider1, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.LookUpEdit1.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.LookUpEdit2.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.TextEdit1.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
            Me.SuspendLayout()
            '
            'LabelControl1
            '
            Me.LabelControl1.Location = New System.Drawing.Point(12, 40)
            Me.LabelControl1.Name = "LabelControl1"
            Me.LabelControl1.Size = New System.Drawing.Size(67, 13)
            Me.LabelControl1.TabIndex = 2
            Me.LabelControl1.Text = "Trace Number"
            '
            'LabelControl2
            '
            Me.LabelControl2.Location = New System.Drawing.Point(13, 66)
            Me.LabelControl2.Name = "LabelControl2"
            Me.LabelControl2.Size = New System.Drawing.Size(52, 13)
            Me.LabelControl2.TabIndex = 4
            Me.LabelControl2.Text = "Error Code"
            '
            'LookUpEdit1
            '
            Me.LookUpEdit1.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                        Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
            Me.LookUpEdit1.Location = New System.Drawing.Point(122, 63)
            Me.LookUpEdit1.Name = "LookUpEdit1"
            Me.LookUpEdit1.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
            Me.LookUpEdit1.Properties.NullText = "Please Choose One..."
            Me.LookUpEdit1.Size = New System.Drawing.Size(254, 20)
            Me.LookUpEdit1.TabIndex = 5
            '
            'LabelControl3
            '
            Me.LabelControl3.Location = New System.Drawing.Point(12, 146)
            Me.LabelControl3.Name = "LabelControl3"
            Me.LabelControl3.Size = New System.Drawing.Size(27, 13)
            Me.LabelControl3.TabIndex = 12
            Me.LabelControl3.Text = "Gross"
            '
            'LabelControl4
            '
            Me.LabelControl4.Location = New System.Drawing.Point(12, 165)
            Me.LabelControl4.Name = "LabelControl4"
            Me.LabelControl4.Size = New System.Drawing.Size(17, 13)
            Me.LabelControl4.TabIndex = 14
            Me.LabelControl4.Text = "Net"
            '
            'SimpleButton1
            '
            Me.SimpleButton1.Anchor = System.Windows.Forms.AnchorStyles.Bottom
            Me.SimpleButton1.Enabled = False
            Me.SimpleButton1.Location = New System.Drawing.Point(122, 194)
            Me.SimpleButton1.Name = "SimpleButton1"
            Me.SimpleButton1.Size = New System.Drawing.Size(75, 23)
            Me.SimpleButton1.TabIndex = 16
            Me.SimpleButton1.Text = "Refund"
            '
            'SimpleButton2
            '
            Me.SimpleButton2.Anchor = System.Windows.Forms.AnchorStyles.Bottom
            Me.SimpleButton2.CausesValidation = False
            Me.SimpleButton2.Location = New System.Drawing.Point(222, 194)
            Me.SimpleButton2.Name = "SimpleButton2"
            Me.SimpleButton2.Size = New System.Drawing.Size(75, 23)
            Me.SimpleButton2.TabIndex = 17
            Me.SimpleButton2.Text = "&Quit"
            '
            'LookUpEdit2
            '
            Me.LookUpEdit2.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                        Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
            Me.LookUpEdit2.Location = New System.Drawing.Point(121, 11)
            Me.LookUpEdit2.Name = "LookUpEdit2"
            Me.LookUpEdit2.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
            Me.LookUpEdit2.Properties.NullText = "Please Choose One..."
            Me.LookUpEdit2.Size = New System.Drawing.Size(254, 20)
            Me.LookUpEdit2.TabIndex = 1
            '
            'LabelControl5
            '
            Me.LabelControl5.Location = New System.Drawing.Point(12, 12)
            Me.LabelControl5.Name = "LabelControl5"
            Me.LabelControl5.Size = New System.Drawing.Size(23, 13)
            Me.LabelControl5.TabIndex = 0
            Me.LabelControl5.Text = "Bank"
            '
            'gross
            '
            Me.gross.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None
            Me.gross.Location = New System.Drawing.Point(121, 146)
            Me.gross.Name = "gross"
            Me.gross.Size = New System.Drawing.Size(176, 13)
            Me.gross.TabIndex = 13
            Me.gross.Text = "$0.00"
            Me.gross.UseMnemonic = False
            '
            'net
            '
            Me.net.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None
            Me.net.Location = New System.Drawing.Point(121, 165)
            Me.net.Name = "net"
            Me.net.Size = New System.Drawing.Size(176, 13)
            Me.net.TabIndex = 15
            Me.net.Text = "$0.00"
            Me.net.UseMnemonic = False
            '
            'creditor
            '
            Me.creditor.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None
            Me.creditor.Location = New System.Drawing.Point(122, 108)
            Me.creditor.Name = "creditor"
            Me.creditor.Size = New System.Drawing.Size(253, 13)
            Me.creditor.TabIndex = 9
            Me.creditor.UseMnemonic = False
            '
            'Client
            '
            Me.client.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None
            Me.client.Location = New System.Drawing.Point(122, 89)
            Me.client.Name = "client"
            Me.client.Size = New System.Drawing.Size(253, 13)
            Me.client.TabIndex = 7
            Me.client.UseMnemonic = False
            '
            'LabelControl8
            '
            Me.LabelControl8.Location = New System.Drawing.Point(13, 108)
            Me.LabelControl8.Name = "LabelControl8"
            Me.LabelControl8.Size = New System.Drawing.Size(39, 13)
            Me.LabelControl8.TabIndex = 8
            Me.LabelControl8.Text = "Creditor"
            '
            'LabelControl9
            '
            Me.LabelControl9.Location = New System.Drawing.Point(13, 89)
            Me.LabelControl9.Name = "LabelControl9"
            Me.LabelControl9.Size = New System.Drawing.Size(27, 13)
            Me.LabelControl9.TabIndex = 6
            Me.LabelControl9.Text = "Client"
            '
            'account_number
            '
            Me.account_number.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None
            Me.account_number.Location = New System.Drawing.Point(121, 127)
            Me.account_number.Name = "account_number"
            Me.account_number.Size = New System.Drawing.Size(254, 13)
            Me.account_number.TabIndex = 11
            Me.account_number.UseMnemonic = False
            '
            'LabelControl11
            '
            Me.LabelControl11.Location = New System.Drawing.Point(12, 127)
            Me.LabelControl11.Name = "LabelControl11"
            Me.LabelControl11.Size = New System.Drawing.Size(79, 13)
            Me.LabelControl11.TabIndex = 10
            Me.LabelControl11.Text = "Account Number"
            '
            'TextEdit1
            '
            Me.TextEdit1.EditValue = ""
            Me.TextEdit1.Location = New System.Drawing.Point(122, 37)
            Me.TextEdit1.Name = "TextEdit1"
            Me.TextEdit1.Properties.Appearance.Options.UseTextOptions = True
            Me.TextEdit1.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
            Me.TextEdit1.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton})
            Me.TextEdit1.Properties.DisplayFormat.FormatString = "0000000"
            Me.TextEdit1.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
            Me.TextEdit1.Properties.Mask.EditMask = "\d{0,7}"
            Me.TextEdit1.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.RegEx
            Me.TextEdit1.Properties.Mask.ShowPlaceHolders = False
            Me.TextEdit1.Properties.MaxLength = 7
            Me.TextEdit1.Properties.NullValuePrompt = "Value Required"
            Me.TextEdit1.Properties.NullValuePromptShowForEmptyValue = True
            Me.TextEdit1.Size = New System.Drawing.Size(121, 20)
            Me.TextEdit1.TabIndex = 3
            Me.TextEdit1.TabStop = False
            '
            'MainForm
            '
            Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
            Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
            Me.ClientSize = New System.Drawing.Size(387, 229)
            Me.Controls.Add(Me.account_number)
            Me.Controls.Add(Me.LabelControl11)
            Me.Controls.Add(Me.creditor)
            Me.Controls.Add(Me.client)
            Me.Controls.Add(Me.LabelControl8)
            Me.Controls.Add(Me.LabelControl9)
            Me.Controls.Add(Me.net)
            Me.Controls.Add(Me.gross)
            Me.Controls.Add(Me.LookUpEdit2)
            Me.Controls.Add(Me.LabelControl5)
            Me.Controls.Add(Me.SimpleButton2)
            Me.Controls.Add(Me.SimpleButton1)
            Me.Controls.Add(Me.LabelControl4)
            Me.Controls.Add(Me.LabelControl3)
            Me.Controls.Add(Me.LookUpEdit1)
            Me.Controls.Add(Me.LabelControl2)
            Me.Controls.Add(Me.LabelControl1)
            Me.Controls.Add(Me.TextEdit1)
            Me.LookAndFeel.UseDefaultLookAndFeel = True
            Me.Name = "MainForm"
            Me.Text = "RPPS Refunds"
            CType(Me.DxErrorProvider1, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.LookUpEdit1.Properties, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.LookUpEdit2.Properties, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.TextEdit1.Properties, System.ComponentModel.ISupportInitialize).EndInit()
            Me.ResumeLayout(False)
            Me.PerformLayout()

        End Sub
        Friend WithEvents LabelControl1 As DevExpress.XtraEditors.LabelControl
        Friend WithEvents LabelControl2 As DevExpress.XtraEditors.LabelControl
        Friend WithEvents LookUpEdit1 As DevExpress.XtraEditors.LookUpEdit
        Friend WithEvents LabelControl3 As DevExpress.XtraEditors.LabelControl
        Friend WithEvents LabelControl4 As DevExpress.XtraEditors.LabelControl
        Friend WithEvents SimpleButton1 As DevExpress.XtraEditors.SimpleButton
        Friend WithEvents SimpleButton2 As DevExpress.XtraEditors.SimpleButton
        Friend WithEvents LookUpEdit2 As DevExpress.XtraEditors.LookUpEdit
        Friend WithEvents LabelControl5 As DevExpress.XtraEditors.LabelControl
        Friend WithEvents gross As DevExpress.XtraEditors.LabelControl
        Friend WithEvents net As DevExpress.XtraEditors.LabelControl
        Friend WithEvents creditor As DevExpress.XtraEditors.LabelControl
        Friend WithEvents client As DevExpress.XtraEditors.LabelControl
        Friend WithEvents LabelControl8 As DevExpress.XtraEditors.LabelControl
        Friend WithEvents LabelControl9 As DevExpress.XtraEditors.LabelControl
        Friend WithEvents account_number As DevExpress.XtraEditors.LabelControl
        Friend WithEvents LabelControl11 As DevExpress.XtraEditors.LabelControl
        Friend WithEvents TextEdit1 As DevExpress.XtraEditors.ButtonEdit
    End Class
End Namespace
