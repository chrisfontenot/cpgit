﻿Namespace RPPS.Response.Manual
    <Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
    Partial Class SearchInputControl
        Inherits DevExpress.XtraEditors.XtraUserControl

        'UserControl overrides dispose to clean up the component list.
        <System.Diagnostics.DebuggerNonUserCode()> _
        Protected Overrides Sub Dispose(ByVal disposing As Boolean)
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
            MyBase.Dispose(disposing)
        End Sub

        'Required by the Windows Form Designer
        Private components As System.ComponentModel.IContainer

        'NOTE: The following procedure is required by the Windows Form Designer
        'It can be modified using the Windows Form Designer.  
        'Do not modify it using the code editor.
        <System.Diagnostics.DebuggerStepThrough()> _
        Private Sub InitializeComponent()
            Me.SimpleButton1 = New DevExpress.XtraEditors.SimpleButton
            Me.SimpleButton2 = New DevExpress.XtraEditors.SimpleButton
            Me.LayoutControl1 = New DevExpress.XtraLayout.LayoutControl
            Me.CalcEdit2 = New DevExpress.XtraEditors.CalcEdit
            Me.CalcEdit1 = New DevExpress.XtraEditors.CalcEdit
            Me.LookUpEdit1 = New DevExpress.XtraEditors.LookUpEdit
            Me.LookupEdit2 = New DevExpress.XtraEditors.LookUpEdit
            Me.LookupEdit3 = New DevExpress.XtraEditors.LookUpEdit
            Me.LayoutControlGroup1 = New DevExpress.XtraLayout.LayoutControlGroup
            Me.LayoutControlItem1 = New DevExpress.XtraLayout.LayoutControlItem
            Me.LayoutControlItem2 = New DevExpress.XtraLayout.LayoutControlItem
            Me.LayoutControlItem3 = New DevExpress.XtraLayout.LayoutControlItem
            Me.LayoutControlItem4 = New DevExpress.XtraLayout.LayoutControlItem
            Me.LayoutControlItem5 = New DevExpress.XtraLayout.LayoutControlItem
            CType(Me.LayoutControl1, System.ComponentModel.ISupportInitialize).BeginInit()
            Me.LayoutControl1.SuspendLayout()
            CType(Me.CalcEdit2.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.CalcEdit1.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.LookUpEdit1.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.LookupEdit2.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.LookupEdit3.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.LayoutControlGroup1, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.LayoutControlItem1, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.LayoutControlItem2, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.LayoutControlItem3, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.LayoutControlItem4, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.LayoutControlItem5, System.ComponentModel.ISupportInitialize).BeginInit()
            Me.SuspendLayout()
            '
            'SimpleButton1
            '
            Me.SimpleButton1.Anchor = System.Windows.Forms.AnchorStyles.Bottom
            Me.SimpleButton1.Enabled = False
            Me.SimpleButton1.Location = New System.Drawing.Point(96, 246)
            Me.SimpleButton1.Name = "SimpleButton1"
            Me.SimpleButton1.Size = New System.Drawing.Size(75, 23)
            Me.SimpleButton1.TabIndex = 2
            Me.SimpleButton1.Text = "&OK"
            '
            'SimpleButton2
            '
            Me.SimpleButton2.Anchor = System.Windows.Forms.AnchorStyles.Bottom
            Me.SimpleButton2.Location = New System.Drawing.Point(192, 246)
            Me.SimpleButton2.Name = "SimpleButton2"
            Me.SimpleButton2.Size = New System.Drawing.Size(75, 23)
            Me.SimpleButton2.TabIndex = 3
            Me.SimpleButton2.Text = "&Cancel"
            '
            'LayoutControl1
            '
            Me.LayoutControl1.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                        Or System.Windows.Forms.AnchorStyles.Left) _
                        Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
            Me.LayoutControl1.Controls.Add(Me.CalcEdit2)
            Me.LayoutControl1.Controls.Add(Me.CalcEdit1)
            Me.LayoutControl1.Controls.Add(Me.LookUpEdit1)
            Me.LayoutControl1.Controls.Add(Me.LookupEdit2)
            Me.LayoutControl1.Controls.Add(Me.LookupEdit3)
            Me.LayoutControl1.Location = New System.Drawing.Point(4, 4)
            Me.LayoutControl1.Name = "LayoutControl1"
            Me.LayoutControl1.Root = Me.LayoutControlGroup1
            Me.LayoutControl1.Size = New System.Drawing.Size(353, 224)
            Me.LayoutControl1.TabIndex = 4
            Me.LayoutControl1.Text = "LayoutControl1"
            '
            'CalcEdit2
            '
            Me.CalcEdit2.Location = New System.Drawing.Point(97, 108)
            Me.CalcEdit2.Name = "CalcEdit2"
            Me.CalcEdit2.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
            Me.CalcEdit2.Properties.Mask.BeepOnError = True
            Me.CalcEdit2.Properties.Mask.EditMask = "c"
            Me.CalcEdit2.Properties.Precision = 2
            Me.CalcEdit2.Size = New System.Drawing.Size(244, 20)
            Me.CalcEdit2.StyleController = Me.LayoutControl1
            Me.CalcEdit2.TabIndex = 7
            '
            'CalcEdit1
            '
            Me.CalcEdit1.Location = New System.Drawing.Point(97, 84)
            Me.CalcEdit1.Name = "CalcEdit1"
            Me.CalcEdit1.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
            Me.CalcEdit1.Properties.Mask.BeepOnError = True
            Me.CalcEdit1.Properties.Mask.EditMask = "c"
            Me.CalcEdit1.Properties.Precision = 2
            Me.CalcEdit1.Size = New System.Drawing.Size(244, 20)
            Me.CalcEdit1.StyleController = Me.LayoutControl1
            Me.CalcEdit1.TabIndex = 6
            '
            'LookUpEdit1
            '
            Me.LookUpEdit1.Location = New System.Drawing.Point(97, 12)
            Me.LookUpEdit1.Name = "LookUpEdit1"
            Me.LookUpEdit1.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
            Me.LookUpEdit1.Properties.Columns.AddRange(New DevExpress.XtraEditors.Controls.LookUpColumnInfo() {New DevExpress.XtraEditors.Controls.LookUpColumnInfo("rpps_file", "ID", 20, DevExpress.Utils.FormatType.Numeric, "f0", True, DevExpress.Utils.HorzAlignment.[Default]), New DevExpress.XtraEditors.Controls.LookUpColumnInfo("date_created", "Date", 20, DevExpress.Utils.FormatType.DateTime, "d", True, DevExpress.Utils.HorzAlignment.[Default], DevExpress.Data.ColumnSortOrder.Descending), New DevExpress.XtraEditors.Controls.LookUpColumnInfo("created_by", "Creator", 20, DevExpress.Utils.FormatType.None, "", True, DevExpress.Utils.HorzAlignment.[Default], DevExpress.Data.ColumnSortOrder.Ascending)})
            Me.LookUpEdit1.Properties.DisplayFormat.FormatString = "d"
            Me.LookUpEdit1.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime
            Me.LookUpEdit1.Properties.DisplayMember = "date_created"
            Me.LookUpEdit1.Properties.NullText = ""
            Me.LookUpEdit1.Properties.ShowFooter = False
            Me.LookUpEdit1.Properties.ValueMember = "rpps_file"
            Me.LookUpEdit1.Size = New System.Drawing.Size(244, 20)
            Me.LookUpEdit1.StyleController = Me.LayoutControl1
            Me.LookUpEdit1.TabIndex = 4
            Me.LookUpEdit1.Properties.SortColumnIndex = 1
            '
            'LookupEdit2
            '
            Me.LookupEdit2.Location = New System.Drawing.Point(97, 36)
            Me.LookupEdit2.Name = "LookupEdit2"
            Me.LookupEdit2.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
            Me.LookupEdit2.Properties.Columns.AddRange(New DevExpress.XtraEditors.Controls.LookUpColumnInfo() {New DevExpress.XtraEditors.Controls.LookUpColumnInfo("rpps_biller_id", "ID", 20, DevExpress.Utils.FormatType.None, "", True, DevExpress.Utils.HorzAlignment.[Default], DevExpress.Data.ColumnSortOrder.Ascending), New DevExpress.XtraEditors.Controls.LookUpColumnInfo("biller_name", "Name", 20, DevExpress.Utils.FormatType.None, "", True, DevExpress.Utils.HorzAlignment.[Default], DevExpress.Data.ColumnSortOrder.Ascending)})
            Me.LookupEdit2.Properties.DisplayMember = "rpps_biller_id"
            Me.LookupEdit2.Properties.MaxLength = 15
            Me.LookupEdit2.Properties.NullText = ""
            Me.LookupEdit2.Properties.ShowFooter = False
            Me.LookupEdit2.Properties.ValueMember = "rpps_biller_id"
            Me.LookupEdit2.Size = New System.Drawing.Size(244, 20)
            Me.LookupEdit2.StyleController = Me.LayoutControl1
            Me.LookupEdit2.TabIndex = 8
            Me.LookupEdit2.TabStop = False
            Me.LookupEdit2.Properties.SortColumnIndex = 1
            '
            'LookupEdit3
            '
            Me.LookupEdit3.Location = New System.Drawing.Point(97, 60)
            Me.LookupEdit3.Name = "LookupEdit3"
            Me.LookupEdit3.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
            Me.LookupEdit3.Properties.Columns.AddRange(New DevExpress.XtraEditors.Controls.LookUpColumnInfo() {New DevExpress.XtraEditors.Controls.LookUpColumnInfo("trace_number_first", "Trace #", 20, DevExpress.Utils.FormatType.None, "", True, DevExpress.Utils.HorzAlignment.[Default], DevExpress.Data.ColumnSortOrder.Ascending), New DevExpress.XtraEditors.Controls.LookUpColumnInfo("client", "Client", 20, DevExpress.Utils.FormatType.Numeric, "f0", True, DevExpress.Utils.HorzAlignment.[Default]), New DevExpress.XtraEditors.Controls.LookUpColumnInfo("account_number", "Account #", 20, DevExpress.Utils.FormatType.None, "", True, DevExpress.Utils.HorzAlignment.[Default], DevExpress.Data.ColumnSortOrder.Ascending), New DevExpress.XtraEditors.Controls.LookUpColumnInfo("debit_amt", "Gross", 20, DevExpress.Utils.FormatType.Numeric, "c", True, DevExpress.Utils.HorzAlignment.[Default]), New DevExpress.XtraEditors.Controls.LookUpColumnInfo("net", "Net", 20, DevExpress.Utils.FormatType.Numeric, "c", True, DevExpress.Utils.HorzAlignment.[Default]), New DevExpress.XtraEditors.Controls.LookUpColumnInfo("rpps_transaction", "ID", 20, DevExpress.Utils.FormatType.Numeric, "f0", False, DevExpress.Utils.HorzAlignment.[Default])})
            Me.LookupEdit3.Properties.DisplayMember = "account_number"
            Me.LookupEdit3.Properties.MaxLength = 22
            Me.LookupEdit3.Properties.NullText = ""
            Me.LookupEdit3.Properties.ShowFooter = False
            Me.LookupEdit3.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.Standard
            Me.LookupEdit3.Properties.ValueMember = "rpps_transaction"
            Me.LookupEdit3.Size = New System.Drawing.Size(244, 20)
            Me.LookupEdit3.StyleController = Me.LayoutControl1
            Me.LookupEdit3.TabIndex = 5
            Me.LookupEdit3.TabStop = False
            Me.LookupEdit3.Properties.SortColumnIndex = 1
            '
            'LayoutControlGroup1
            '
            Me.LayoutControlGroup1.CustomizationFormText = "LayoutControlGroup1"
            Me.LayoutControlGroup1.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.[True]
            Me.LayoutControlGroup1.GroupBordersVisible = False
            Me.LayoutControlGroup1.Items.AddRange(New DevExpress.XtraLayout.BaseLayoutItem() {Me.LayoutControlItem1, Me.LayoutControlItem2, Me.LayoutControlItem3, Me.LayoutControlItem4, Me.LayoutControlItem5})
            Me.LayoutControlGroup1.Location = New System.Drawing.Point(0, 0)
            Me.LayoutControlGroup1.Name = "LayoutControlGroup1"
            Me.LayoutControlGroup1.Size = New System.Drawing.Size(353, 224)
            Me.LayoutControlGroup1.Spacing = New DevExpress.XtraLayout.Utils.Padding(0, 0, 0, 0)
            Me.LayoutControlGroup1.Text = "LayoutControlGroup1"
            Me.LayoutControlGroup1.TextVisible = False
            '
            'LayoutControlItem1
            '
            Me.LayoutControlItem1.Control = Me.LookUpEdit1
            Me.LayoutControlItem1.CustomizationFormText = "Output RPPS File"
            Me.LayoutControlItem1.Location = New System.Drawing.Point(0, 0)
            Me.LayoutControlItem1.Name = "LayoutControlItem1"
            Me.LayoutControlItem1.Size = New System.Drawing.Size(333, 24)
            Me.LayoutControlItem1.Text = "Output RPPS File"
            Me.LayoutControlItem1.TextSize = New System.Drawing.Size(81, 13)
            '
            'LayoutControlItem2
            '
            Me.LayoutControlItem2.Control = Me.LookupEdit3
            Me.LayoutControlItem2.CustomizationFormText = "Account Number"
            Me.LayoutControlItem2.Location = New System.Drawing.Point(0, 48)
            Me.LayoutControlItem2.Name = "LayoutControlItem2"
            Me.LayoutControlItem2.Size = New System.Drawing.Size(333, 24)
            Me.LayoutControlItem2.Text = "Account Number"
            Me.LayoutControlItem2.TextSize = New System.Drawing.Size(81, 13)
            '
            'LayoutControlItem3
            '
            Me.LayoutControlItem3.Control = Me.CalcEdit1
            Me.LayoutControlItem3.CustomizationFormText = "Gross Amount"
            Me.LayoutControlItem3.Location = New System.Drawing.Point(0, 72)
            Me.LayoutControlItem3.Name = "LayoutControlItem3"
            Me.LayoutControlItem3.Size = New System.Drawing.Size(333, 24)
            Me.LayoutControlItem3.Text = "Gross Amount"
            Me.LayoutControlItem3.TextSize = New System.Drawing.Size(81, 13)
            '
            'LayoutControlItem4
            '
            Me.LayoutControlItem4.Control = Me.CalcEdit2
            Me.LayoutControlItem4.CustomizationFormText = "Net Amount"
            Me.LayoutControlItem4.Location = New System.Drawing.Point(0, 96)
            Me.LayoutControlItem4.Name = "LayoutControlItem4"
            Me.LayoutControlItem4.Size = New System.Drawing.Size(333, 108)
            Me.LayoutControlItem4.Text = "Net Amount"
            Me.LayoutControlItem4.TextSize = New System.Drawing.Size(81, 13)
            '
            'LayoutControlItem5
            '
            Me.LayoutControlItem5.Control = Me.LookupEdit2
            Me.LayoutControlItem5.CustomizationFormText = "RPPS Biller ID"
            Me.LayoutControlItem5.Location = New System.Drawing.Point(0, 24)
            Me.LayoutControlItem5.Name = "LayoutControlItem5"
            Me.LayoutControlItem5.Size = New System.Drawing.Size(333, 24)
            Me.LayoutControlItem5.Text = "RPPS Biller ID"
            Me.LayoutControlItem5.TextSize = New System.Drawing.Size(81, 13)
            '
            'SearchInputControl
            '
            Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
            Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
            Me.Controls.Add(Me.LayoutControl1)
            Me.Controls.Add(Me.SimpleButton2)
            Me.Controls.Add(Me.SimpleButton1)
            Me.Name = "SearchInputControl"
            Me.Size = New System.Drawing.Size(362, 291)
            CType(Me.LayoutControl1, System.ComponentModel.ISupportInitialize).EndInit()
            Me.LayoutControl1.ResumeLayout(False)
            CType(Me.CalcEdit2.Properties, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.CalcEdit1.Properties, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.LookUpEdit1.Properties, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.LookupEdit2.Properties, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.LookupEdit3.Properties, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.LayoutControlGroup1, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.LayoutControlItem1, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.LayoutControlItem2, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.LayoutControlItem3, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.LayoutControlItem4, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.LayoutControlItem5, System.ComponentModel.ISupportInitialize).EndInit()
            Me.ResumeLayout(False)

        End Sub
        Friend WithEvents SimpleButton1 As DevExpress.XtraEditors.SimpleButton
        Friend WithEvents SimpleButton2 As DevExpress.XtraEditors.SimpleButton
        Friend WithEvents LayoutControl1 As DevExpress.XtraLayout.LayoutControl
        Friend WithEvents LayoutControlGroup1 As DevExpress.XtraLayout.LayoutControlGroup
        Friend WithEvents LayoutControlItem1 As DevExpress.XtraLayout.LayoutControlItem
        Friend WithEvents LayoutControlItem2 As DevExpress.XtraLayout.LayoutControlItem
        Friend WithEvents LayoutControlItem3 As DevExpress.XtraLayout.LayoutControlItem
        Friend WithEvents LayoutControlItem4 As DevExpress.XtraLayout.LayoutControlItem
        Friend WithEvents LayoutControlItem5 As DevExpress.XtraLayout.LayoutControlItem
        Public WithEvents CalcEdit2 As DevExpress.XtraEditors.CalcEdit
        Public WithEvents CalcEdit1 As DevExpress.XtraEditors.CalcEdit
        Public WithEvents LookUpEdit1 As DevExpress.XtraEditors.LookUpEdit
        Friend WithEvents LookupEdit2 As DevExpress.XtraEditors.LookUpEdit
        Friend WithEvents LookupEdit3 As DevExpress.XtraEditors.LookUpEdit

    End Class
End Namespace
