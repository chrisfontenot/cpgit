﻿Namespace RPPS.Response.Manual
    <Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
    Partial Class TraceNumberSearchForm
        Inherits DebtPlus.Data.Forms.DebtPlusForm

        'Form overrides dispose to clean up the component list.
        <System.Diagnostics.DebuggerNonUserCode()> _
        Protected Overrides Sub Dispose(ByVal disposing As Boolean)
            Try
                If disposing AndAlso components IsNot Nothing Then
                    components.Dispose()
                End If
            Finally
                MyBase.Dispose(disposing)
            End Try
        End Sub

        'Required by the Windows Form Designer
        Private components As System.ComponentModel.IContainer

        'NOTE: The following procedure is required by the Windows Form Designer
        'It can be modified using the Windows Form Designer.  
        'Do not modify it using the code editor.
        <System.Diagnostics.DebuggerStepThrough()> _
        Private Sub InitializeComponent()
            Me.SearchInputControl1 = New SearchInputControl
            Me.SearchResultControl1 = New SearchResultControl
            CType(Me.DxErrorProvider1, System.ComponentModel.ISupportInitialize).BeginInit()
            Me.SuspendLayout()
            '
            'SearchInputControl1
            '
            Me.SearchInputControl1.Dock = System.Windows.Forms.DockStyle.Fill
            Me.SearchInputControl1.Location = New System.Drawing.Point(0, 0)
            Me.SearchInputControl1.Name = "SearchInputControl1"
            Me.SearchInputControl1.Size = New System.Drawing.Size(342, 266)
            Me.SearchInputControl1.TabIndex = 2
            '
            'SearchResultControl1
            '
            Me.SearchResultControl1.Dock = System.Windows.Forms.DockStyle.Fill
            Me.SearchResultControl1.Location = New System.Drawing.Point(0, 0)
            Me.SearchResultControl1.Name = "SearchResultControl1"
            Me.SearchResultControl1.Size = New System.Drawing.Size(342, 266)
            Me.SearchResultControl1.TabIndex = 3
            Me.SearchResultControl1.Visible = False
            '
            'TraceNumberSearchForm
            '
            Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
            Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
            Me.ClientSize = New System.Drawing.Size(342, 266)
            Me.Controls.Add(Me.SearchInputControl1)
            Me.Controls.Add(Me.SearchResultControl1)
            Me.Name = "TraceNumberSearchForm"
            Me.Text = "Trace Number Search"
            CType(Me.DxErrorProvider1, System.ComponentModel.ISupportInitialize).EndInit()
            Me.ResumeLayout(False)

        End Sub
        Friend WithEvents SearchInputControl1 As SearchInputControl
        Friend WithEvents SearchResultControl1 As SearchResultControl
    End Class
End Namespace
