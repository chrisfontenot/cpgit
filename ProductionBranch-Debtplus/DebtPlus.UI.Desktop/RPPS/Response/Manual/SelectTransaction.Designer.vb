Namespace RPPS.Response.Manual
    <Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
    Partial Class SelectTransaction
        Inherits DebtPlus.Data.Forms.DebtPlusForm

        'Form overrides dispose to clean up the component list.
        <System.Diagnostics.DebuggerNonUserCode()> _
        Protected Overrides Sub Dispose(ByVal disposing As Boolean)
            Try
                If disposing AndAlso components IsNot Nothing Then
                    components.Dispose()
                End If
            Finally
                MyBase.Dispose(disposing)
            End Try
        End Sub

        'Required by the Windows Form Designer
        Private components As System.ComponentModel.IContainer

        'NOTE: The following procedure is required by the Windows Form Designer
        'It can be modified using the Windows Form Designer.  
        'Do not modify it using the code editor.
        <System.Diagnostics.DebuggerStepThrough()> _
        Private Sub InitializeComponent()
            Me.GridControl1 = New DevExpress.XtraGrid.GridControl
            Me.GridView1 = New DevExpress.XtraGrid.Views.Grid.GridView
            Me.col_client = New DevExpress.XtraGrid.Columns.GridColumn
            Me.col_client.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.True
            Me.col_creditor = New DevExpress.XtraGrid.Columns.GridColumn
            Me.col_creditor.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.True
            Me.col_account_number = New DevExpress.XtraGrid.Columns.GridColumn
            Me.col_account_number.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.True
            Me.col_gross = New DevExpress.XtraGrid.Columns.GridColumn
            Me.col_gross.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.True
            Me.col_net = New DevExpress.XtraGrid.Columns.GridColumn
            Me.col_net.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.True
            Me.Button_OK = New DevExpress.XtraEditors.SimpleButton
            Me.Button_Cancel = New DevExpress.XtraEditors.SimpleButton
            Me.LabelControl1 = New DevExpress.XtraEditors.LabelControl
            CType(Me.DxErrorProvider1, System.ComponentModel.ISupportInitialize).BeginInit()

            CType(Me.GridControl1, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.GridView1, System.ComponentModel.ISupportInitialize).BeginInit()
            Me.SuspendLayout()

            '
            'GridControl1
            '
            Me.GridControl1.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                        Or System.Windows.Forms.AnchorStyles.Left) _
                        Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
            Me.GridControl1.EmbeddedNavigator.Name = ""
            Me.GridControl1.Location = New System.Drawing.Point(2, 63)
            Me.GridControl1.MainView = Me.GridView1
            Me.GridControl1.Name = "GridControl1"
            Me.GridControl1.Size = New System.Drawing.Size(291, 162)
            Me.GridControl1.TabIndex = 0
            Me.GridControl1.ViewCollection.AddRange(New DevExpress.XtraGrid.Views.Base.BaseView() {Me.GridView1})
            '
            'GridView1
            '
            Me.GridView1.Columns.AddRange(New DevExpress.XtraGrid.Columns.GridColumn() {Me.col_client, Me.col_creditor, Me.col_account_number, Me.col_gross, Me.col_net})
            Me.GridView1.GridControl = Me.GridControl1
            Me.GridView1.Name = "GridView1"
            Me.GridView1.OptionsBehavior.Editable = False
            Me.GridView1.OptionsSelection.InvertSelection = True
            Me.GridView1.OptionsSelection.UseIndicatorForSelection = False
            Me.GridView1.OptionsView.ShowDetailButtons = False
            Me.GridView1.OptionsView.ShowGroupPanel = False
            Me.GridView1.OptionsView.ShowIndicator = False
            '
            'col_client
            '
            Me.col_client.Caption = "Client"
            Me.col_client.CustomizationCaption = "Client ID"
            Me.col_client.FieldName = "client"
            Me.col_client.Name = "col_client"
            Me.col_client.Visible = True
            Me.col_client.VisibleIndex = 0
            '
            'col_creditor
            '
            Me.col_creditor.Caption = "Creditor"
            Me.col_creditor.CustomizationCaption = "Creditor ID"
            Me.col_creditor.FieldName = "creditor"
            Me.col_creditor.Name = "col_creditor"
            Me.col_creditor.Visible = True
            Me.col_creditor.VisibleIndex = 1
            '
            'col_account_number
            '
            Me.col_account_number.Caption = "Account #"
            Me.col_account_number.CustomizationCaption = "Account Number"
            Me.col_account_number.FieldName = "account_number"
            Me.col_account_number.Name = "col_account_number"
            Me.col_account_number.Visible = True
            Me.col_account_number.VisibleIndex = 2
            '
            'col_gross
            '
            Me.col_gross.Caption = "Gross"
            Me.col_gross.CustomizationCaption = "Gross Amount"
            Me.col_gross.DisplayFormat.FormatString = "c"
            Me.col_gross.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
            Me.col_gross.FieldName = "gross"
            Me.col_gross.GroupFormat.FormatString = "c"
            Me.col_gross.GroupFormat.FormatType = DevExpress.Utils.FormatType.Numeric
            Me.col_gross.Name = "col_gross"
            Me.col_gross.Visible = True
            Me.col_gross.VisibleIndex = 3
            '
            'col_net
            '
            Me.col_net.Caption = "Net"
            Me.col_net.CustomizationCaption = "Net Amount"
            Me.col_net.DisplayFormat.FormatString = "c"
            Me.col_net.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
            Me.col_net.FieldName = "net"
            Me.col_net.GroupFormat.FormatString = "c"
            Me.col_net.GroupFormat.FormatType = DevExpress.Utils.FormatType.Numeric
            Me.col_net.Name = "col_net"
            Me.col_net.Visible = True
            Me.col_net.VisibleIndex = 4
            '
            'Button_OK
            '
            Me.Button_OK.Anchor = System.Windows.Forms.AnchorStyles.Bottom
            Me.Button_OK.DialogResult = System.Windows.Forms.DialogResult.OK
            Me.Button_OK.Location = New System.Drawing.Point(60, 231)
            Me.Button_OK.Name = "Button_OK"
            Me.Button_OK.Size = New System.Drawing.Size(75, 23)
            Me.Button_OK.TabIndex = 1
            Me.Button_OK.Text = "&OK"
            '
            'Button_Cancel
            '
            Me.Button_Cancel.Anchor = System.Windows.Forms.AnchorStyles.Bottom
            Me.Button_Cancel.DialogResult = System.Windows.Forms.DialogResult.Cancel
            Me.Button_Cancel.Location = New System.Drawing.Point(158, 231)
            Me.Button_Cancel.Name = "Button_Cancel"
            Me.Button_Cancel.Size = New System.Drawing.Size(75, 23)
            Me.Button_Cancel.TabIndex = 2
            Me.Button_Cancel.Text = "&Cancel"
            '
            'LabelControl1
            '
            Me.LabelControl1.Appearance.Options.UseTextOptions = True
            Me.LabelControl1.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
            Me.LabelControl1.Appearance.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Top
            Me.LabelControl1.Appearance.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap
            Me.LabelControl1.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None
            Me.LabelControl1.Location = New System.Drawing.Point(11, 12)
            Me.LabelControl1.Name = "LabelControl1"
            Me.LabelControl1.Size = New System.Drawing.Size(269, 39)
            Me.LabelControl1.TabIndex = 3
            Me.LabelControl1.Text = "There are multiple items which match the trace number that you provided. Please s" & _
                "elect the item to be refunded from the following list."
            Me.LabelControl1.UseMnemonic = False
            '
            'SelectTransaction
            '
            Me.AcceptButton = Me.Button_OK
            Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
            Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
            Me.CancelButton = Me.Button_Cancel
            Me.ClientSize = New System.Drawing.Size(292, 266)
            Me.Controls.Add(Me.LabelControl1)
            Me.Controls.Add(Me.Button_Cancel)
            Me.Controls.Add(Me.Button_OK)
            Me.Controls.Add(Me.GridControl1)
            Me.Name = "SelectTransaction"
            Me.ToolTipController1.SetSuperTip(Me, Nothing)
            Me.Text = "Select Transaction"
            CType(Me.DxErrorProvider1, System.ComponentModel.ISupportInitialize).EndInit()

            CType(Me.GridControl1, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.GridView1, System.ComponentModel.ISupportInitialize).EndInit()
            Me.ResumeLayout(False)

        End Sub
        Friend WithEvents GridControl1 As DevExpress.XtraGrid.GridControl
        Friend WithEvents GridView1 As DevExpress.XtraGrid.Views.Grid.GridView
        Friend WithEvents Button_OK As DevExpress.XtraEditors.SimpleButton
        Friend WithEvents Button_Cancel As DevExpress.XtraEditors.SimpleButton
        Friend WithEvents col_client As DevExpress.XtraGrid.Columns.GridColumn
        Friend WithEvents col_creditor As DevExpress.XtraGrid.Columns.GridColumn
        Friend WithEvents col_account_number As DevExpress.XtraGrid.Columns.GridColumn
        Friend WithEvents col_gross As DevExpress.XtraGrid.Columns.GridColumn
        Friend WithEvents col_net As DevExpress.XtraGrid.Columns.GridColumn
        Friend WithEvents LabelControl1 As DevExpress.XtraEditors.LabelControl
    End Class
End Namespace
