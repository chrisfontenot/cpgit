#Region "Copyright 2000-2012 DebtPlus, L.L.C."
'{*******************************************************************}
'{                                                                   }
'{       DebtPlus Debt Management System                             }
'{                                                                   }
'{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
'{       ALL RIGHTS RESERVED                                         }
'{                                                                   }
'{   The entire contents of this file is protected by U.S. and       }
'{   International Copyright Laws. Unauthorized reproduction,        }
'{   reverse-engineering, and distribution of all or any portion of  }
'{   the code contained in this file is strictly prohibited and may  }
'{   result in severe civil and criminal penalties and will be       }
'{   prosecuted to the maximum extent possible under the law.        }
'{                                                                   }
'{   RESTRICTIONS                                                    }
'{                                                                   }
'{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
'{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
'{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
'{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
'{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
'{                                                                   }
'{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
'{   ADDITIONAL RESTRICTIONS.                                        }
'{                                                                   }
'{*******************************************************************}
#End Region
Imports DebtPlus.Utils

Namespace RPPS.Response.Manual
    Public Class TraceNumberSearchForm

        Public Sub New()
            MyBase.New()
            InitializeComponent()
            AddHandler Me.Load, AddressOf TraceNumberSearchForm_Load
            AddHandler SearchInputControl1.Cancelled, AddressOf SearchInputControl1_Cancelled
            AddHandler SearchResultControl1.Cancelled, AddressOf SearchResultControl1_Cancelled
            AddHandler SearchInputControl1.Selected, AddressOf SearchInputControl1_Selected
        End Sub

        Private privateTraceNumber As String = String.Empty
        Public Property TraceNumber() As String
            Get
                Return privateTraceNumber
            End Get
            Set(ByVal value As String)
                privateTraceNumber = value
            End Set
        End Property

        Private Sub TraceNumberSearchForm_Load(ByVal sender As Object, ByVal e As System.EventArgs)
            SwitchToInput()
        End Sub

        Private Sub SearchInputControl1_Cancelled(ByVal sender As Object, ByVal e As System.EventArgs)
            DialogResult = Windows.Forms.DialogResult.Cancel
        End Sub

        Private Sub SearchResultControl1_Cancelled(ByVal sender As Object, ByVal e As System.EventArgs)
            SwitchToInput()
        End Sub

        Private Sub SwitchToInput()

            ' Hide the result
            SearchResultControl1.Visible = False
            SearchResultControl1.TabStop = False
            SearchResultControl1.SendToBack()

            ' Enable the input control
            SearchInputControl1.Visible = True
            SearchInputControl1.TabStop = True
            SearchInputControl1.BringToFront()
            SearchInputControl1.TraceNumber = String.Empty
            SearchInputControl1.RefreshData()
        End Sub

        Private Sub SearchInputControl1_Selected(ByVal sender As Object, ByVal e As System.EventArgs)
            If SearchInputControl1.TraceNumber <> String.Empty Then
                TraceNumber = SearchInputControl1.TraceNumber
                DialogResult = Windows.Forms.DialogResult.OK
            End If
        End Sub
    End Class
End Namespace
