#Region "Copyright 2000-2012 DebtPlus, L.L.C."
'{*******************************************************************}
'{                                                                   }
'{       DebtPlus Debt Management System                             }
'{                                                                   }
'{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
'{       ALL RIGHTS RESERVED                                         }
'{                                                                   }
'{   The entire contents of this file is protected by U.S. and       }
'{   International Copyright Laws. Unauthorized reproduction,        }
'{   reverse-engineering, and distribution of all or any portion of  }
'{   the code contained in this file is strictly prohibited and may  }
'{   result in severe civil and criminal penalties and will be       }
'{   prosecuted to the maximum extent possible under the law.        }
'{                                                                   }
'{   RESTRICTIONS                                                    }
'{                                                                   }
'{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
'{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
'{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
'{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
'{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
'{                                                                   }
'{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
'{   ADDITIONAL RESTRICTIONS.                                        }
'{                                                                   }
'{*******************************************************************}
#End Region

Option Compare Binary
Option Explicit On
Option Strict On

Imports DebtPlus.Utils
Imports System.Data.SqlClient
Imports System.Windows.Forms

Namespace RPPS.Response.Manual
    Friend Class MainForm

        Protected ap As ArgParser
        Private rpps_transaction As System.Int32 = -1
        Private PendingTransactions As Boolean

        Public Sub New()
            MyClass.New(CType(Nothing, ArgParser))
        End Sub

        Public Sub New(ByVal ap As ArgParser)
            MyBase.New()
            InitializeComponent()

            Me.ap = ap

            AddHandler FormClosing, AddressOf Form_FormClosing
            AddHandler Load, AddressOf Form_Load
            AddHandler LookUpEdit1.EditValueChanged, AddressOf LookUpEdit1_EditValueChanged
            AddHandler LookUpEdit2.EditValueChanged, AddressOf TextEdit1_Validated
            AddHandler TextEdit1.Validated, AddressOf TextEdit1_Validated
            AddHandler SimpleButton2.Click, AddressOf SimpleButton2_Click
            AddHandler SimpleButton1.Click, AddressOf SimpleButton1_Click
            AddHandler TextEdit1.ButtonPressed, AddressOf TextEdit1_ButtonPressed
            AddHandler TextEdit1.EditValueChanged, AddressOf FieldChanged
            AddHandler LookUpEdit1.EditValueChanged, AddressOf FieldChanged
            AddHandler LookUpEdit2.EditValueChanged, AddressOf FieldChanged
        End Sub

        Private Sub SimpleButton2_Click(ByVal sender As Object, ByVal e As System.EventArgs)
            Close()
        End Sub

        ReadOnly dsErrors As New System.Data.DataSet("dsErrors")

        Private Sub Form_FormClosing(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosingEventArgs)
            If PendingTransactions Then
                Dim answer As Boolean = False

                Dim cn As SqlClient.SqlConnection = New SqlConnection(DebtPlus.LINQ.SQLInfoClass.getDefault().ConnectionString)
                Dim current_cursor As Cursor = System.Windows.Forms.Cursor.Current
                Try
                    System.Windows.Forms.Cursor.Current = Cursors.WaitCursor
                    cn.Open()
                    Using cmd As SqlClient.SqlCommand = New SqlCommand
                        With cmd
                            .Connection = cn
                            .CommandText = "xpr_rpps_response_process_post"
                            .CommandType = CommandType.StoredProcedure
                            .CommandTimeout = System.Math.Max(600, DebtPlus.LINQ.SQLInfoClass.getDefault().CommandTimeout)
                            .Parameters.Add("@response_file", SqlDbType.Int).Value = ap.rpps_response_file
                            .ExecuteNonQuery()
                            answer = True
                        End With
                    End Using

                Catch ex As SqlClient.SqlException
                    DebtPlus.UI.Common.ErrorHandling.HandleErrors(ex, "Error posting the response file")

                Finally
                    If cn IsNot Nothing Then cn.Dispose()
                    System.Windows.Forms.Cursor.Current = current_cursor
                End Try
            End If
        End Sub

        Private Sub Form_Load(ByVal sender As Object, ByVal e As System.EventArgs)

            ' Invalidate the fields
            client.Text = String.Empty
            creditor.Text = String.Empty
            account_number.Text = String.Empty
            gross.Text = String.Empty
            net.Text = String.Empty

            ' Invalidate the transaction at the start
            rpps_transaction = -1

            ' Load the list of error codes
            Dim current_cursor As System.Windows.Forms.Cursor = System.Windows.Forms.Cursor.Current
            Try
                Using cmd As SqlClient.SqlCommand = New SqlCommand
                    With cmd
                        .Connection = New SqlConnection(DebtPlus.LINQ.SQLInfoClass.getDefault().ConnectionString)
                        .CommandText = "SELECT [rpps_reject_code],[description],[resend_funds] FROM rpps_reject_codes WITH (NOLOCK)"
                        .CommandType = CommandType.Text
                    End With

                    Using da As New SqlClient.SqlDataAdapter(cmd)
                        da.Fill(dsErrors, "rpps_reject_codes")
                        With dsErrors.Tables("rpps_reject_codes")
                            .PrimaryKey = New System.Data.DataColumn() {.Columns("rpps_reject_code")}
                            If Not .Columns.Contains("formatted_rpps_reject_code") Then
                                .Columns.Add("formatted_rpps_reject_code", GetType(String), "[rpps_reject_code] + isnull(' - ' + [description],'')")
                            End If
                        End With
                    End Using
                End Using

                With LookUpEdit1
                    With .Properties
                        .ShowFooter = False
                        .DataSource = dsErrors.Tables("rpps_reject_codes").DefaultView
                        .DisplayMember = "formatted_rpps_reject_code"
                        .ValueMember = "rpps_reject_code"
                        .PopupWidth = LookUpEdit1.Width + 130
                        .Columns.Clear()
                        .Columns.Add(New DevExpress.XtraEditors.Controls.LookUpColumnInfo("rpps_reject_code", 20, "Error"))
                        .Columns.Add(New DevExpress.XtraEditors.Controls.LookUpColumnInfo("description", 60, "Description"))
                    End With
                End With

                Using cmd As SqlClient.SqlCommand = New SqlCommand
                    With cmd
                        .Connection = New SqlConnection(DebtPlus.LINQ.SQLInfoClass.getDefault().ConnectionString)
                        .CommandText = "SELECT [bank],[description],[immediate_origin],[Default],[ActiveFlag] FROM banks WITH (NOLOCK) WHERE type = 'R'"
                        .CommandType = CommandType.Text
                    End With
                    Using da As New SqlClient.SqlDataAdapter(cmd)
                        da.Fill(dsErrors, "banks")
                    End Using

                    With dsErrors.Tables("banks")
                        .PrimaryKey = New System.Data.DataColumn() {.Columns("bank")}
                        If Not .Columns.Contains("formatted_description") Then
                            .Columns.Add("formatted_description", GetType(String), "isnull([immediate_origin],'') + isnull(' - ' + [description],'')")
                        End If
                    End With
                End Using

                With LookUpEdit2
                    With .Properties
                        .ShowFooter = False
                        .DataSource = dsErrors.Tables("banks").DefaultView
                        .DisplayMember = "formatted_description"
                        .ValueMember = "bank"
                        .PopupWidth = LookUpEdit1.Width + 130
                        .Columns.Clear()
                        .Columns.Add(New DevExpress.XtraEditors.Controls.LookUpColumnInfo("bank", 20, "ID"))
                        .Columns.Add(New DevExpress.XtraEditors.Controls.LookUpColumnInfo("description", 60, "Description"))
                        .Columns.Add(New DevExpress.XtraEditors.Controls.LookUpColumnInfo("immediate_origin", 20, "Prefix Code"))
                    End With
                End With

            Catch ex As SqlClient.SqlException
                DebtPlus.UI.Common.ErrorHandling.HandleErrors(ex, "Error reading RPPS reject codes")

            Finally
                System.Windows.Forms.Cursor.Current = current_cursor
            End Try

        End Sub

        Private Sub LookUpEdit1_EditValueChanged(ByVal sender As Object, ByVal e As System.EventArgs)
            SimpleButton1.Enabled = IsValid()
        End Sub

        Private Sub TextEdit1_ButtonPressed(ByVal sender As Object, ByVal e As DevExpress.XtraEditors.Controls.ButtonPressedEventArgs)
            Using frm As New TraceNumberSearchForm()
                AddHandler frm.SearchInputControl1.Selected, AddressOf SelectedHandler
                If frm.ShowDialog() = Windows.Forms.DialogResult.OK Then
                    TextEdit1.Text = frm.TraceNumber
                End If
                RemoveHandler frm.SearchInputControl1.Selected, AddressOf SelectedHandler
            End Using
        End Sub

        Private Sub SelectedHandler(ByVal Sender As Object, ByVal e As SearchInputControl.SelectedArgs)
            rpps_transaction = e.rpps_transaction
            client.Text = String.Format("{0:0000000}", e.client)
            account_number.Text = e.account_number
            creditor.Text = e.creditor
            gross.Text = String.Format("{0:c}", e.gross)
            net.Text = String.Format("{0:c}", e.Net)
            SimpleButton1.Enabled = IsValid()
        End Sub

        Private Sub TextEdit1_Validated(ByVal sender As Object, ByVal e As System.EventArgs)

            ' Do validation only when we have not found the transaction
            If rpps_transaction <= 0 Then

                ' Invalidate the fields
                client.Text = String.Empty
                creditor.Text = String.Empty
                account_number.Text = String.Empty
                gross.Text = String.Empty
                net.Text = String.Empty

                ' Find the prefix to the trace number
                Dim bank As System.Int32 = -1
                If LookUpEdit2.EditValue IsNot Nothing AndAlso LookUpEdit2.EditValue IsNot System.DBNull.Value Then bank = Convert.ToInt32(LookUpEdit2.EditValue)
                If bank <= 0 Then Return

                ' If there is no trace number then terminate
                Dim trace_number As String = DebtPlus.Utils.Nulls.DStr(TextEdit1.Text.Trim())
                If trace_number = String.Empty Then Return

                ' Take only the last seven digits of the trace number.
                trace_number = trace_number.PadLeft(7, "0"c)
                If trace_number.Length > 7 Then
                    trace_number = trace_number.Substring(trace_number.Length - 7)
                End If

                ' Lookup the trace number in the system.
                Using ds As New System.Data.DataSet("ds")
                    Dim tbl As System.Data.DataTable = Nothing
                    Dim drv As System.Data.DataRowView = Nothing

                    Dim current_cursor As System.Windows.Forms.Cursor = System.Windows.Forms.Cursor.Current
                    Try
                        Using cmd As SqlClient.SqlCommand = New SqlCommand
                            With cmd
                                .Connection = New SqlConnection(DebtPlus.LINQ.SQLInfoClass.getDefault().ConnectionString)
                                .CommandText = "SELECT t.rpps_transaction, t.bank, t.client, t.creditor, t.client_creditor, t.biller_id, t.trace_number_first, t.trace_number_last, t.rpps_file, t.rpps_batch, t.transaction_code, t.service_class_or_purpose, t.return_code, t.client_creditor_register, t.client_creditor_proposal, t.death_date, t.old_account_number, t.new_account_number, t.old_balance, t.response_batch_id, t.prenote_status, t.debt_note, t.date_created, t.created_by, rcc.debit_amt as gross, case rcc.creditor_type when 'D' then rcc.debit_amt - rcc.fairshare_amt else rcc.debit_amt end as net, rcc.creditor, rcc.client, rcc.account_number, rcc.date_created as disbursement_date, v.name, cr.creditor_name FROM rpps_transactions t WITH (NOLOCK) LEFT OUTER JOIN registers_client_creditor rcc WITH (NOLOCK) on t.client_creditor_register = rcc.client_creditor_register LEFT OUTER JOIN view_client_address v WITH (NOLOCK) ON rcc.client = v.client LEFT OUTER JOIN creditors cr WITH (NOLOCK) ON rcc.creditor = cr.creditor WHERE t.trace_number_first like '[0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9]' AND t.bank = @bank AND @trace_number BETWEEN right(t.trace_number_first,7) AND right(t.trace_number_last, 7) AND t.service_class_or_purpose = 'CIE'"
                                .Parameters.Add("@bank", SqlDbType.Int).Value = bank
                                .Parameters.Add("@trace_number", SqlDbType.VarChar, 7).Value = trace_number
                                .CommandType = CommandType.Text
                            End With

                            Using da As New SqlClient.SqlDataAdapter(cmd)
                                da.Fill(ds, "transactions")
                                tbl = ds.Tables(0)
                            End Using
                        End Using

                        ' Look for obvious error conditions
                        Dim ErrorMessage As String = String.Empty
                        Using vue As New System.Data.DataView(tbl, "[return_code] IS NULL", String.Empty, DataViewRowState.CurrentRows)

                            ' If there are no rows then there is no trace number
                            If tbl.Rows.Count = 0 Then
                                ErrorMessage = "The trace number does not exist in the system"

                                ' If the view is empty then the trace number has been rejected.
                            ElseIf vue.Count = 0 Then
                                ErrorMessage = "The item has already been rejected."

                                ' If only one row then we have the row
                            ElseIf vue.Count = 1 Then
                                drv = vue(0)
                            Else

                                ' Since there are more than one, ask the user for the specific item.
                                Using frm As New SelectTransaction(vue)
                                    If frm.ShowDialog() = Windows.Forms.DialogResult.OK Then
                                        drv = frm.drv
                                    End If
                                End Using
                            End If

                            ' Supply the current values for the gross and net figures
                            If drv IsNot Nothing Then
                                rpps_transaction = Convert.ToInt32(drv("rpps_transaction"))

                                If drv("gross") IsNot System.DBNull.Value Then gross.Text = String.Format("{0:c}", Convert.ToDecimal(drv("gross")))
                                If drv("net") IsNot System.DBNull.Value Then net.Text = String.Format("{0:c}", Convert.ToDecimal(drv("net")))
                                If drv("account_number") IsNot System.DBNull.Value Then account_number.Text = Convert.ToString(drv("account_number"))

                                ' Add the creditor to the form
                                Dim string_creditor As String = String.Empty
                                Dim string_creditor_name As String = String.Empty
                                If drv("creditor") IsNot System.DBNull.Value Then string_creditor = Convert.ToString(drv("creditor"))
                                If drv("creditor_name") IsNot System.DBNull.Value Then string_creditor_name = Convert.ToString(drv("creditor_name"))
                                If string_creditor <> String.Empty AndAlso string_creditor_name <> String.Empty Then
                                    creditor.Text = String.Format("{0} {1}", string_creditor, string_creditor_name)
                                Else
                                    creditor.Text = string_creditor + string_creditor_name
                                End If

                                ' Add the client name and id to the form
                                Dim string_client As String = String.Empty
                                Dim string_name As String = String.Empty
                                If drv("client") IsNot System.DBNull.Value Then string_client = String.Format("{0:0000000}", Convert.ToInt32(drv("ClientId")))
                                If drv("name") IsNot System.DBNull.Value Then string_name = Convert.ToString(drv("name"))
                                If string_client <> String.Empty AndAlso string_name <> String.Empty Then
                                    client.Text = String.Format("{0} {1}", string_client, string_name)
                                Else
                                    client.Text = string_client + string_name
                                End If
                            End If
                        End Using

                        TextEdit1.ErrorText = ErrorMessage

                    Catch ex As SqlClient.SqlException
                        DebtPlus.UI.Common.ErrorHandling.HandleErrors(ex, "Error reading RPPS transactions")
                        System.Windows.Forms.Cursor.Current = current_cursor
                    End Try
                End Using
            End If
        End Sub

        Private Sub FieldChanged(ByVal Sender As Object, ByVal e As System.EventArgs)
            SimpleButton1.Enabled = IsValid()
        End Sub

        Private Function IsValid() As Boolean
            Dim answer As Boolean = False

            Do
                ' Ensure that there is an error code
                With LookUpEdit1
                    Dim ErrorCode As String = String.Empty
                    If .ErrorText <> String.Empty Then Exit Do
                    If .EditValue IsNot Nothing Then ErrorCode = Convert.ToString(.EditValue)
                    If ErrorCode = String.Empty Then Exit Do
                End With

                ' Ensure that there is a bank
                With LookUpEdit2
                    Dim bank As System.Int32 = -1
                    If .ErrorText <> String.Empty Then Exit Do
                    If .EditValue IsNot Nothing Then bank = Convert.ToInt32(.EditValue)
                    If bank <= 0 Then Exit Do
                End With

                ' There should be a transaction
                If rpps_transaction <= 0 Then Exit Do

                ' Finally, things are valid.
                answer = True
                Exit Do
            Loop

            Return answer
        End Function

        Private Sub SimpleButton1_Click(ByVal sender As Object, ByVal e As System.EventArgs)
            Dim cn As SqlClient.SqlConnection = New SqlConnection(DebtPlus.LINQ.SQLInfoClass.getDefault().ConnectionString)
            Dim current_cursor As System.Windows.Forms.Cursor = System.Windows.Forms.Cursor.Current

            Try
                System.Windows.Forms.Cursor.Current = Cursors.WaitCursor
                cn.Open()

                ' Generate the transaction to void the item
                Using cmd As SqlClient.SqlCommand = New SqlCommand
                    With cmd
                        .Connection = cn
                        .CommandText = "xpr_rpps_response_Manual"
                        .CommandType = CommandType.StoredProcedure
                        .Parameters.Add("@response_file", SqlDbType.Int).Value = ap.rpps_response_file
                        .Parameters.Add("@rpps_transaction", SqlDbType.Int).Value = rpps_transaction
                        .Parameters.Add("@return_code", SqlDbType.VarChar, 4).Value = Convert.ToString(LookUpEdit1.EditValue)
                        .Parameters.Add("@disbursement_message", SqlDbType.Int).Value = 0
                        .ExecuteNonQuery()

                        ' Enable the "Do you Wish to post?" function.
                        PendingTransactions = True
                    End With
                End Using

                ' Invalidate the fields
                client.Text = String.Empty
                creditor.Text = String.Empty
                account_number.Text = String.Empty
                gross.Text = String.Empty
                net.Text = String.Empty

                TextEdit1.EditValue = Nothing
                rpps_transaction = -1
                SimpleButton1.Enabled = False

                ' Set the focus to the trace number
                TextEdit1.Focus()

            Catch ex As SqlClient.SqlException
                DebtPlus.UI.Common.ErrorHandling.HandleErrors(ex, "Error processing transaction")

            Finally
                If cn IsNot Nothing Then cn.Dispose()
                System.Windows.Forms.Cursor.Current = current_cursor
            End Try
        End Sub
    End Class
End Namespace
