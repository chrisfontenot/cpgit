Option Compare Binary
Option Explicit On 
Option Strict On

Imports DebtPlus.UI.FormLib.RPPS.New
Imports System.Windows.Forms

Namespace RPPS.Response.Manual
    Friend Class ArgParser
        Inherits DebtPlus.Utils.ArgParserBase
        Private pathnames As New ArrayList()

        ''' <summary>
        ''' RPPS Response file
        ''' </summary>
        Private _rpps_response_file As System.Int32 = -1
        Public Property rpps_response_file() As System.Int32
            Get
                Return _rpps_response_file
            End Get
            Set(ByVal value As System.Int32)
                _rpps_response_file = value
            End Set
        End Property

        ''' <summary>
        ''' Process an option
        ''' </summary>
        Protected Overrides Function OnSwitch(ByVal switchName As String, ByVal switchValue As String) As DebtPlus.Utils.ArgParserBase.SwitchStatus
            Dim ss As SwitchStatus = DebtPlus.Utils.ArgParserBase.SwitchStatus.YesError
            Select Case switchName
                Case "b"
                    Dim tempInteger As System.Int32
                    If Int32.TryParse(switchValue, tempInteger) Then
                        If tempInteger > 0 Then
                            rpps_response_file = tempInteger
                            ss = DebtPlus.Utils.ArgParserBase.SwitchStatus.NoError
                        End If
                    End If

                Case "?"
                    ss = DebtPlus.Utils.ArgParserBase.SwitchStatus.ShowUsage
            End Select

            Return ss
        End Function

        ''' <summary>
        ''' Create an instance of our class
        ''' </summary>
        Public Sub New()
            MyBase.New(New String() {"b"})
        End Sub

        ''' <summary>
        ''' Generate the command usage information
        ''' </summary>
        Protected Overrides Sub OnUsage(ByVal errorInfo As String)
            ' deprecated
        End Sub

        ''' <summary>
        ''' Called after all of the arguments have been parsed
        ''' </summary>
        Protected Overrides Function OnDoneParse() As DebtPlus.Utils.ArgParserBase.SwitchStatus
            Dim ss As SwitchStatus = DebtPlus.Utils.ArgParserBase.SwitchStatus.NoError

            ' Ask for the response batch to use
            If rpps_response_file <= 0 Then
                Using frm As New BatchForm()
                    With frm
                        If .ShowDialog() = DialogResult.OK Then rpps_response_file = .rpps_response_file
                    End With
                End Using

                If rpps_response_file <= 0 Then ss = DebtPlus.Utils.ArgParserBase.SwitchStatus.YesError
            End If

            Return ss
        End Function 'OnDoneParse
    End Class
End Namespace
