Option Compare Binary
Option Explicit On 
Option Strict On

Imports System.IO

Namespace RPPS.Response.File
    Friend Class ArgParser
        Inherits DebtPlus.Utils.ArgParserBase

        Public pathnames As New ArrayList()

        ''' <summary>
        ''' RPPS Response file
        ''' </summary>
        Private _rpps_response_file As System.Int32 = -1
        Public Property rpps_response_file() As System.Int32
            Get
                Return _rpps_response_file
            End Get
            Set(ByVal value As System.Int32)
                _rpps_response_file = value
            End Set
        End Property

        ''' <summary>
        ''' Desire disbursement notes
        ''' </summary>
        Private _notes As System.Int32 = 0
        Public ReadOnly Property notes() As Boolean
            Get
                Return _notes = 1
            End Get
        End Property

        ''' <summary>
        ''' Process an option
        ''' </summary>
        Protected Overrides Function OnSwitch(ByVal switchName As String, ByVal switchValue As String) As DebtPlus.Utils.ArgParserBase.SwitchStatus
            Dim ss As SwitchStatus = DebtPlus.Utils.ArgParserBase.SwitchStatus.NoError
            Select Case switchName
                Case "n1"
                    _notes = 1

                Case "n0"
                    _notes = 2

                Case "b"
                    Dim TempInteger As System.Int32
                    If Int32.TryParse(switchValue, Globalization.NumberStyles.Integer, System.Globalization.CultureInfo.InvariantCulture, TempInteger) AndAlso TempInteger > 0 Then
                        rpps_response_file = TempInteger
                        Exit Select
                    End If
                    ss = DebtPlus.Utils.ArgParserBase.SwitchStatus.YesError

                Case "?"
                    ss = DebtPlus.Utils.ArgParserBase.SwitchStatus.ShowUsage

                Case Else
                    ss = DebtPlus.Utils.ArgParserBase.SwitchStatus.YesError
            End Select

            Return ss
        End Function

        ''' <summary>
        ''' Input file to be read
        ''' </summary>
        Protected Overrides Function OnNonSwitch(ByVal switchValue As String) As DebtPlus.Utils.ArgParserBase.SwitchStatus
            Dim ss As SwitchStatus = DebtPlus.Utils.ArgParserBase.SwitchStatus.NoError

            ' Add command-line argument to array of pathnames.
            ' Convert switchValue to set of pathnames, add each pathname to the pathnames ArrayList.
            Try
                Dim d As String = Path.GetDirectoryName(switchValue)
                Dim dir As DirectoryInfo
                If (d.Length = 0) Then
                    dir = New DirectoryInfo(".")
                Else
                    dir = New DirectoryInfo(d)
                End If

                Dim f As FileInfo
                For Each f In dir.GetFiles(Path.GetFileName(switchValue))
                    pathnames.Add(f.FullName)
                Next f

            Catch SecEx As System.Security.SecurityException
                Throw SecEx

            Catch
                ss = DebtPlus.Utils.ArgParserBase.SwitchStatus.YesError
            End Try

            If pathnames.Count = 0 Then
                AppendErrorLine("None of the specified files exists.")
                ss = DebtPlus.Utils.ArgParserBase.SwitchStatus.YesError
            End If

            Return ss
        End Function 'OnNonSwitch

        ''' <summary>
        ''' Return the list of input file name
        ''' </summary>
        Public Function GetPathnameEnumerator() As IEnumerator
            Return pathnames.GetEnumerator(0, pathnames.Count)
        End Function 'GetPathnameEnumerator

        ''' <summary>
        ''' Create an instance of our class
        ''' </summary>
        Public Sub New()
            MyBase.New(New String() {"b", "n0", "n1"})
        End Sub

        ''' <summary>
        ''' Generate the command usage information
        ''' </summary>
        Protected Overrides Sub OnUsage(ByVal errorInfo As String)
            ' deprecated
        End Sub
    End Class
End NameSpace
