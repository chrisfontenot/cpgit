#Region "Copyright 2000-2012 DebtPlus, L.L.C."
'{*******************************************************************}
'{                                                                   }
'{       DebtPlus Debt Management System                             }
'{                                                                   }
'{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
'{       ALL RIGHTS RESERVED                                         }
'{                                                                   }
'{   The entire contents of this file is protected by U.S. and       }
'{   International Copyright Laws. Unauthorized reproduction,        }
'{   reverse-engineering, and distribution of all or any portion of  }
'{   the code contained in this file is strictly prohibited and may  }
'{   result in severe civil and criminal penalties and will be       }
'{   prosecuted to the maximum extent possible under the law.        }
'{                                                                   }
'{   RESTRICTIONS                                                    }
'{                                                                   }
'{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
'{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
'{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
'{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
'{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
'{                                                                   }
'{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
'{   ADDITIONAL RESTRICTIONS.                                        }
'{                                                                   }
'{*******************************************************************}
#End Region

Option Compare Binary
Option Explicit On
Option Strict On

Imports DebtPlus.Utils
Imports System.Data.SqlClient
Imports DebtPlus.UI.FormLib.RPPS.New
Imports System.Windows.Forms
Imports System.Drawing

Namespace RPPS.Response.File
    Friend Class Status_Form
        Inherits DebtPlus.Data.Forms.DebtPlusForm

        Private WithEvents bt As New System.ComponentModel.BackgroundWorker
        Friend ap As ArgParser

        Public Sub New()
            MyBase.New()
            privateInstance = Me
            InitializeComponent()
        End Sub

        Public Sub New(ByVal ap As ArgParser)
            MyClass.New()
            Me.ap = ap
            AddHandler Me.Load, AddressOf Status_Form_Load
            AddHandler bt.RunWorkerCompleted, AddressOf bt_RunWorkerCompleted
            AddHandler bt.DoWork, AddressOf bt_DoWork
            AddHandler Button_OK.Click, AddressOf cmdOK_Click
            AddHandler Button_Print.Click, AddressOf printButton_Click
        End Sub

        ''' <summary>
        ''' Class to read the input file
        ''' </summary>
        Private Class Reader
            Implements IDisposable

            Private fs As System.IO.FileStream
            Private InputStream As System.IO.StreamReader

            ''' <summary>
            ''' Create an instance of our class
            ''' </summary>
            Public Sub New()
            End Sub

            Public Sub New(ByVal FileName As String)
                MyClass.New()
                Me.FileName = FileName
            End Sub

            ''' <summary>
            ''' Input file name
            ''' </summary>
            Public Property FileName() As String = String.Empty

            ''' <summary>
            ''' Flag to indicate that the file is at the end
            ''' </summary>
            Private privateAtEnd As Boolean
            Public ReadOnly Property AtEnd() As Boolean
                Get
                    Return privateAtEnd
                End Get
            End Property

            ''' <summary>
            ''' Line buffer for the input file
            ''' </summary>
            Private privateNextLine As String = String.Empty
            Public ReadOnly Property NextLine() As String
                Get
                    Return privateNextLine
                End Get
            End Property

            ''' <summary>
            ''' Access the next line in the input file
            ''' </summary>
            Public Function ReadNextLine() As Boolean
                If Not AtEnd AndAlso InputStream.EndOfStream Then
                    privateAtEnd = True
                End If
                If AtEnd Then Return False

                privateNextLine = InputStream.ReadLine().PadRight(94)
                RecordType = privateNextLine.Substring(0, 1)
                Return True
            End Function

            ' The first character of the record is the type
            Private RecordType As String = String.Empty

            ''' <summary>
            ''' Look for a type 1 record
            ''' </summary>
            Public ReadOnly Property IsType1() As Boolean
                Get
                    Return RecordType = "1"
                End Get
            End Property

            ''' <summary>
            ''' Look for a type 5 record
            ''' </summary>
            Public ReadOnly Property IsType5() As Boolean
                Get
                    Return RecordType = "5"
                End Get
            End Property

            ''' <summary>
            ''' Look for a type 6 record
            ''' </summary>
            Public ReadOnly Property IsType6() As Boolean
                Get
                    Return RecordType = "6"
                End Get
            End Property

            ''' <summary>
            ''' Look for a type 7 record
            ''' </summary>
            Public ReadOnly Property IsType7() As Boolean
                Get
                    Return RecordType = "7"
                End Get
            End Property

            ''' <summary>
            ''' Look for a type 8 record
            ''' </summary>
            Public ReadOnly Property IsType8() As Boolean
                Get
                    Return RecordType = "8"
                End Get
            End Property

            ''' <summary>
            ''' Look for a type 9 record
            ''' </summary>
            Public ReadOnly Property IsType9() As Boolean
                Get
                    Return RecordType = "9"
                End Get
            End Property

            ''' <summary>
            ''' Open the input file
            ''' </summary>
            Public Function OpenFile() As Boolean
                Dim answer As Boolean = False

                Try
                    fs = New System.IO.FileStream(FileName, System.IO.FileMode.Open, System.IO.FileAccess.Read, System.IO.FileShare.Read, 4096)
                    InputStream = New System.IO.StreamReader(fs)
                    answer = True

                Catch ex As System.IO.FileNotFoundException
                Catch ex As System.IO.DirectoryNotFoundException
                Catch ex As System.IO.PathTooLongException
                End Try

                Return answer
            End Function

            ''' <summary>
            ''' Close the input file
            ''' </summary>
            Public Sub CloseFile()
                If InputStream IsNot Nothing Then InputStream.Close()
                If fs IsNot Nothing Then fs.Close()
            End Sub

            ''' <summary>
            ''' Dispose of the class
            ''' </summary>
            Private disposedValue As Boolean        ' To detect redundant calls
            Protected Overridable Sub Dispose(ByVal disposing As Boolean)
                If Not disposedValue Then
                    If disposing Then
                        CloseFile()
                    End If

                    ' set large fields to null.
                    InputStream = Nothing
                    fs = Nothing
                End If

                disposedValue = True
            End Sub

#Region " IDisposable Support "
            ' This code added by Visual Basic to correctly implement the disposable pattern.
            Public Sub Dispose() Implements IDisposable.Dispose
                ' Do not change this code.  Put cleanup code in Dispose(ByVal disposing As Boolean) above.
                Dispose(True)
                GC.SuppressFinalize(Me)
            End Sub
#End Region
        End Class

        ''' <summary>
        ''' Generic class for files, batches, and transactions
        ''' </summary>
        Private Class Responses

            ''' <summary>
            ''' Create an instance of our class
            ''' </summary>
            Public Sub New()
                MyBase.New()
            End Sub

            Public Sub New(ByVal rdr As Reader)
                MyClass.New()
                Me.rdr = rdr
            End Sub

            ''' <summary>
            ''' Linkage to the file reader class
            ''' </summary>
            Public Property rdr() As Reader

            ''' <summary>
            ''' Set the error code value
            ''' </summary>
            Public Property ErrorCode() As String = String.Empty

            ''' <summary>
            ''' Return the base error code
            ''' </summary>
            Public Overridable Function GetError() As String
                Return ErrorCode
            End Function

            ''' <summary>
            ''' Record the transactions to the database
            ''' </summary>
            Public Overridable Sub UpdateDatabase()
            End Sub
        End Class

        ''' <summary>
        ''' A transaction is a representation of the following class
        ''' </summary>
        Private Class StandardTransactionProcessing
            Inherits Responses

            Protected Batch As BatchProcessing

            ' List of addendum records
            Protected AddendumCollection As New System.Collections.Specialized.StringCollection
            Protected TransactionLine As String = String.Empty

            ''' <summary>
            ''' Create an instance of our class
            ''' </summary>
            Public Sub New()
                MyBase.New()
            End Sub

            Public Sub New(ByVal rdr As Reader, ByVal Batch As BatchProcessing)
                MyBase.New(rdr)
                Me.Batch = Batch
            End Sub

            Public Sub New(ByVal rdr As Reader)
                MyBase.New(rdr)
            End Sub

            ''' <summary>
            ''' Parse the type 6 (Transaction Header) record
            ''' </summary>
            Public Overridable Function ParseType6() As Boolean
                Dim answer As Boolean = False

                ' Save the transaction line for later examination
                TransactionLine = rdr.NextLine

                ' Gather all of the addendum records for this line. We don't look at the count
                ' since the note buffers don't have a valid count.
                Do While rdr.ReadNextLine
                    If rdr.IsType7 Then
                        AddendumCollection.Add(rdr.NextLine)
                    Else
                        answer = True
                        Exit Do
                    End If
                Loop

                Return answer
            End Function

            ''' <summary>
            ''' Obtain the error status for the file
            ''' </summary>
            Public Overrides Function GetError() As String
                Dim answer As String = Batch.GetError
                If answer = String.Empty Then answer = ErrorCode
                Return answer
            End Function

            ''' <summary>
            ''' Process a CDP record
            ''' </summary>
            Protected Sub process_transaction_CDP(ByVal ClientName As String, ByVal account_number As String, ByVal trace_number As String, ByVal net As Decimal)

                ' Count an instance of our record type
                privateInstance.CDP += 1

                Dim cn As SqlClient.SqlConnection = New SqlConnection(DebtPlus.LINQ.SQLInfoClass.getDefault().ConnectionString)
                Dim current_cursor As Cursor = Cursor.Current
                Try
                    Cursor.Current = Cursors.WaitCursor
                    cn.Open()
                    With New SqlCommand
                        .Connection = cn
                        .CommandText = "xpr_rpps_response_CDP"
                        .CommandType = CommandType.StoredProcedure
                        .CommandTimeout = System.Math.Max(600, DebtPlus.LINQ.SQLInfoClass.getDefault().CommandTimeout)
                        .Parameters.Add("RETURN", SqlDbType.Int).Direction = ParameterDirection.ReturnValue
                        .Parameters.Add("@response_file", SqlDbType.Int).Value = Batch.File.ResponseFileID
                        .Parameters.Add("@trace_number", SqlDbType.VarChar, 80).Value = trace_number
                        .Parameters.Add("@biller_id", SqlDbType.VarChar, 80).Value = Batch.BillerID
                        .Parameters.Add("@return_code", SqlDbType.VarChar, 80).Value = GetError()
                        .Parameters.Add("@name", SqlDbType.VarChar, 80).Value = ClientName
                        .Parameters.Add("@account_number", SqlDbType.VarChar, 80).Value = account_number
                        .Parameters.Add("@net", SqlDbType.Decimal).Value = net
                        .ExecuteNonQuery()
                    End With

                Catch ex As SqlClient.SqlException
                    DebtPlus.UI.Common.ErrorHandling.HandleErrors(ex, "Error recording CDP response")

                Finally
                    If cn IsNot Nothing AndAlso cn.State <> ConnectionState.Closed Then cn.Close()
                    Cursor.Current = current_cursor
                End Try
            End Sub

            ''' <summary>
            ''' This is a proposal record
            ''' </summary>
            Protected Sub process_transaction_FBC(ByVal ClientName As String, ByVal account_number As String, ByVal trace_number As String, ByVal net As Decimal)
                process_transaction_CDP(ClientName, account_number, trace_number, net)
            End Sub

            ''' <summary>
            ''' This is a proposal record
            ''' </summary>
            Protected Sub process_transaction_FBD(ByVal ClientName As String, ByVal account_number As String, ByVal trace_number As String, ByVal net As Decimal)
                process_transaction_CDP(ClientName, account_number, trace_number, net)
            End Sub

            ''' <summary>
            ''' This is a drop record
            ''' </summary>
            Protected Sub process_transaction_CDD(ByVal ClientName As String, ByVal account_number As String, ByVal trace_number As String, ByVal net As Decimal)

                ' Count an instance of our record type
                privateInstance.CDD += 1

                ' Fetch the fields from the input record
                'Dim CompanyIdentifier As String = AddendumCollection.Item(0).Substring(6, 6)
                'Dim SSN As String = AddendumCollection.Item(0).Substring(12, 9)
                'Dim Client As Int32 = IntegerValue(AddendumCollection.Item(0), 21, 15)

                Dim cn As SqlClient.SqlConnection = New SqlConnection(DebtPlus.LINQ.SQLInfoClass.getDefault().ConnectionString)
                Dim current_cursor As Cursor = Cursor.Current
                Try
                    Cursor.Current = Cursors.WaitCursor
                    cn.Open()
                    With New SqlCommand
                        .Connection = cn
                        .CommandText = "xpr_rpps_response_CDD"
                        .CommandType = CommandType.StoredProcedure
                        .CommandTimeout = System.Math.Max(600, DebtPlus.LINQ.SQLInfoClass.getDefault().CommandTimeout)
                        .Parameters.Add("RETURN", SqlDbType.Int).Direction = ParameterDirection.ReturnValue
                        .Parameters.Add("@response_file", SqlDbType.Int).Value = Batch.File.ResponseFileID
                        .Parameters.Add("@trace_number", SqlDbType.VarChar, 80).Value = trace_number
                        .Parameters.Add("@biller_id", SqlDbType.VarChar, 80).Value = Batch.BillerID
                        .Parameters.Add("@return_code", SqlDbType.VarChar, 80).Value = GetError()
                        .Parameters.Add("@name", SqlDbType.VarChar, 80).Value = ClientName
                        .Parameters.Add("@account_number", SqlDbType.VarChar, 80).Value = account_number
                        .Parameters.Add("@net", SqlDbType.Decimal).Value = net
                        .ExecuteNonQuery()
                    End With

                Catch ex As SqlClient.SqlException
                    DebtPlus.UI.Common.ErrorHandling.HandleErrors(ex, "Error recording CDD response")

                Finally
                    If cn IsNot Nothing AndAlso cn.State <> ConnectionState.Closed Then cn.Close()
                    Cursor.Current = current_cursor
                End Try
            End Sub

            ''' <summary>
            ''' This is a change request
            ''' </summary>
            Protected Sub process_transaction_CDC(ByVal ClientName As String, ByVal account_number As String, ByVal trace_number As String, ByVal net As Decimal)

                ' Count an instance of our record type
                privateInstance.CDC += 1

                ' Find the information for the record
                'Dim client As Int32 = IntegerValue(AddendumCollection.Item(0), 21, 15)
                Dim new_account_number As String = AddendumCollection.Item(0).Substring(36, 22).Trim()
                Dim Payment As Decimal = MoneyValue(AddendumCollection.Item(0), 61, 7)
                Dim Balance As Decimal = MoneyValue(AddendumCollection.Item(0), 68, 10)

                ' Override the location for the error condition
                ErrorCode = AddendumCollection.Item(0).Substring(58, 3)

                Dim cn As SqlClient.SqlConnection = New SqlConnection(DebtPlus.LINQ.SQLInfoClass.getDefault().ConnectionString)
                Dim current_cursor As Cursor = Cursor.Current
                Try
                    Cursor.Current = Cursors.WaitCursor
                    cn.Open()
                    With New SqlCommand
                        .Connection = cn
                        .CommandText = "xpr_rpps_response_CDC"
                        .CommandType = CommandType.StoredProcedure
                        .CommandTimeout = System.Math.Max(600, DebtPlus.LINQ.SQLInfoClass.getDefault().CommandTimeout)
                        .Parameters.Add("RETURN", SqlDbType.Int).Direction = ParameterDirection.ReturnValue
                        .Parameters.Add("@response_file", SqlDbType.Int).Value = Batch.File.ResponseFileID
                        .Parameters.Add("@trace_number", SqlDbType.VarChar, 80).Value = trace_number
                        .Parameters.Add("@biller_id", SqlDbType.VarChar, 80).Value = Batch.BillerID
                        .Parameters.Add("@return_code", SqlDbType.VarChar, 80).Value = GetError()
                        .Parameters.Add("@name", SqlDbType.VarChar, 80).Value = ClientName
                        .Parameters.Add("@old_account_number", SqlDbType.VarChar, 80).Value = account_number
                        .Parameters.Add("@net", SqlDbType.Decimal).Value = net
                        .Parameters.Add("@new_account_number", SqlDbType.VarChar, 80).Value = new_account_number
                        .Parameters.Add("@monthly_payment_amount", SqlDbType.Decimal).Value = Payment
                        .Parameters.Add("@balance", SqlDbType.Decimal).Value = Balance
                        .ExecuteNonQuery()
                    End With

                Catch ex As SqlClient.SqlException
                    DebtPlus.UI.Common.ErrorHandling.HandleErrors(ex, "Error recording CDC response")

                Finally
                    If cn IsNot Nothing AndAlso cn.State <> ConnectionState.Closed Then cn.Close()
                    Cursor.Current = current_cursor
                End Try
            End Sub

            ''' <summary>
            ''' This is a change request
            ''' </summary>
            Protected Sub process_transaction_ADV(ByVal ClientName As String, ByVal account_number As String, ByVal trace_number As String, ByVal net As Decimal, ByVal Gross As Decimal)

                ' Count an instance of our record type
                privateInstance.ADV += 1

                ' Look for the addendum lines to determine the processing required
                Dim AddendumCode As String = AddendumCollection.Item(0).Substring(1, 2)
                Dim AddendumDate As String = AddendumCollection.Item(0).Substring(21, 6)
                Dim AddendumRoutingNumber As String = AddendumCollection.Item(0).Substring(27, 8)
                Dim AddendumInformation As String = AddendumCollection.Item(0).Substring(35, 44)

                ' Open the database connection
                Dim cn As SqlClient.SqlConnection = New SqlConnection(DebtPlus.LINQ.SQLInfoClass.getDefault().ConnectionString)
                Dim current_cursor As Cursor = Cursor.Current
                Try
                    Cursor.Current = Cursors.WaitCursor
                    cn.Open()
                    With New SqlCommand
                        .Connection = cn
                        .CommandText = "xpr_rpps_response_ADV"
                        .CommandType = CommandType.StoredProcedure
                        .CommandTimeout = System.Math.Max(600, DebtPlus.LINQ.SQLInfoClass.getDefault().CommandTimeout)
                        .Parameters.Add("RETURN", SqlDbType.Int).Direction = ParameterDirection.ReturnValue
                        .Parameters.Add("@response_file", SqlDbType.Int).Value = Batch.File.ResponseFileID
                        .Parameters.Add("@trace_number", SqlDbType.VarChar, 80).Value = trace_number
                        .Parameters.Add("@biller_id", SqlDbType.VarChar, 80).Value = Batch.BillerID
                        .Parameters.Add("@return_code", SqlDbType.VarChar, 80).Value = GetError()
                        .Parameters.Add("@name", SqlDbType.VarChar, 80).Value = ClientName
                        .Parameters.Add("@account_number", SqlDbType.VarChar, 80).Value = account_number
                        .Parameters.Add("@net", SqlDbType.Decimal).Value = net
                        .Parameters.Add("@gross", SqlDbType.Decimal).Value = Gross

                        .Parameters.Add("@addendum_code", SqlDbType.VarChar, 2).Value = AddendumCode
                        .Parameters.Add("@addendum_date", SqlDbType.VarChar, 8).Value = AddendumDate
                        .Parameters.Add("@addendum_routing", SqlDbType.VarChar, 8).Value = AddendumRoutingNumber
                        .Parameters.Add("@addendum_information", SqlDbType.VarChar, 50).Value = AddendumInformation
                        .ExecuteNonQuery()
                    End With

                Catch ex As SqlClient.SqlException
                    DebtPlus.UI.Common.ErrorHandling.HandleErrors(ex, "Error recording ADV response")

                Finally
                    If cn IsNot Nothing AndAlso cn.State <> ConnectionState.Closed Then cn.Close()
                    Cursor.Current = current_cursor
                End Try
            End Sub

            ''' <summary>
            ''' Parse the input string into a suitable date object. If the date is not valid, NULL is returned.
            ''' </summary>
            ''' <param name="InputString">The date string. It may be either 6 or 8 digits in length.</param>
            ''' <returns></returns>
            ''' <remarks></remarks>
            Protected Function ParseNullDate(ByVal InputString As String) As Object
                Dim answer As Object = DBNull.Value

                ' Split the fields so that we can use a language independant date format
                Dim Year As Int32 = -1
                Dim Month As Int32 = -1
                Dim Day As Int32 = -1
                Try

                    ' Convert the year and try to fold in the centurary. We use dates from 00 through 80 as being 2000 through 2080. Dates from 81 to 99 are 1981 through 1999.
                    If Int32.TryParse(InputString.Substring(0, 2), Year) Then
                        Year += 1900
                        If Year <= 1980 Then
                            Year += 100
                        End If
                    End If

                    Int32.TryParse(InputString.Substring(2, 2), Month)
                    Int32.TryParse(InputString.Substring(4, 2), Day)

                    ' Convert the date fields to a system date.
                    If Year > 1900 AndAlso Month > 0 AndAlso Day > 0 Then
                        answer = New DateTime(Year, Month, Day)
                    End If

                Catch ex As Exception
                    ' do nothing about an exception here. It will be good to return the null value.
                End Try

                Return answer
            End Function

            ''' <summary>
            ''' Parse the input string into a suitable date object. If the date is not valid, NULL is returned.
            ''' </summary>
            ''' <param name="InputString">The date string. It may be either 6 or 8 digits in length.</param>
            ''' <returns></returns>
            ''' <remarks></remarks>
            Protected Function ParseNullInt(ByVal InputString As String) As Object
                Dim answer As Object = DBNull.Value

                ' Convert the number to a suitable value
                Try
                    Dim Value As Int32 = 0
                    If Int32.TryParse(InputString, Value) Then
                        answer = Value
                    End If

                Catch ex As Exception
                    ' do nothing about an exception here. It will be good to return the null value.
                End Try

                Return answer
            End Function

            ''' <summary>
            ''' Parse the input string into a suitable date object. If the date is not valid, NULL is returned.
            ''' </summary>
            ''' <param name="InputString">The date string. It may be either 6 or 8 digits in length.</param>
            ''' <returns></returns>
            ''' <remarks></remarks>
            Protected Function ParseNullFloat(ByVal InputString As String) As Object
                Dim answer As Object = DBNull.Value

                ' Convert the number to a suitable value
                Try
                    Dim Value As Double = 0.0#

                    ' Make the floating point number "legal" to our system. We use from 0.0 to 1.0 (exclusive).
                    Dim TempString As String = "0." + InputString.Trim()
                    If Double.TryParse(TempString, Value) Then
                        answer = Value
                    End If

                Catch ex As Exception
                    ' do nothing about an exception here. It will be good to return the null value.
                End Try

                Return answer
            End Function

            ''' <summary>
            ''' Parse the input string into a suitable decimal object. If the money is not valid, NULL is returned.
            ''' </summary>
            ''' <param name="InputString">The money string.</param>
            ''' <returns></returns>
            ''' <remarks></remarks>
            Protected Function ParseNullMoney(ByVal InputString As String) As Object
                Dim answer As Object = DBNull.Value

                ' Convert the number to a suitable value
                Try
                    Dim Value As Decimal = 0D

                    ' Insert a specific radix point two places to the left
                    Dim TempString As String = InputString
                    If TempString.Length > 2 Then
                        TempString = TempString.Substring(0, TempString.Length - 2) + "." + TempString.Substring(TempString.Length - 2)
                    End If

                    If Decimal.TryParse(TempString, Value) Then
                        answer = Value
                    End If

                Catch ex As Exception
                    ' do nothing about an exception here. It will be good to return the null value.
                End Try

                Return answer
            End Function

            ''' <summary>
            ''' This is a proposal reject record
            ''' </summary>
            Protected Sub process_transaction_CDR(ByVal ClientName As String, ByVal account_number As String, ByVal trace_number As String, ByVal net As Decimal)

                ' Count an instance of our record type
                privateInstance.CDR += 1

                ' Fetch the fields from the input record
                Dim CompanyIdentifier As String = AddendumCollection.Item(0).Substring(21, 6)
                Dim SSN As String = AddendumCollection.Item(0).Substring(27, 9)
                Dim Client As Int32 = IntegerValue(AddendumCollection.Item(0), 36, 15)
                Dim ReasonString As String = AddendumCollection.Item(0).Substring(51, 3).Trim()
                Dim CounterAmount As Decimal = 0D
                Dim InfoRequired As String = String.Empty

                ' These fields vary depending upon the reject code.
                Dim resubmit_date As Object = DBNull.Value
                Dim cycle_months_remaining As Object = DBNull.Value
                Dim forgiven_pct As Object = DBNull.Value
                Dim first_payment_date As Object = DBNull.Value
                Dim good_thru_date As Object = DBNull.Value
                Dim ineligible_reason As Object = DBNull.Value
                Dim internal_program_ends_date As Object = DBNull.Value
                Dim interest_rate As Object = DBNull.Value
                Dim third_party_detail As Object = DBNull.Value
                Dim third_party_contact As Object = DBNull.Value

                ' The counter amount does not seem to conflict with other fields. So, just include it on all items
                CounterAmount = MoneyValue(AddendumCollection.Item(0), 54, 7)

                ' Determine the fields based upon the error code. Only some have formatted fields, so just include those in the parsing logic.
                Select Case ReasonString
                    Case "AO"
                        resubmit_date = ParseNullDate(AddendumCollection.Item(0).Substring(61, 6))

                    Case "FA"
                        cycle_months_remaining = ParseNullInt(AddendumCollection.Item(0).Substring(61, 2))
                        forgiven_pct = ParseNullFloat(AddendumCollection(0).Substring(63, 5))

                    Case "IP"
                        first_payment_date = ParseNullDate(AddendumCollection(0).Substring(61, 6))
                        good_thru_date = ParseNullDate(AddendumCollection(0).Substring(67, 6))

                    Case "IE"
                        Dim TempString As String = AddendumCollection(0).Substring(61, 2).Trim()
                        If TempString <> String.Empty Then
                            ineligible_reason = TempString
                        End If

                    Case "PI"
                        internal_program_ends_date = ParseNullDate(AddendumCollection(0).Substring(61, 6))
                        interest_rate = ParseNullFloat(AddendumCollection(0).Substring(67, 2))
                        good_thru_date = ParseNullDate(AddendumCollection(0).Substring(69, 6))

                    Case "PO"
                        resubmit_date = ParseNullDate(AddendumCollection.Item(0).Substring(61, 6))

                    Case "PT"
                        first_payment_date = ParseNullDate(AddendumCollection(0).Substring(61, 6))
                        good_thru_date = ParseNullDate(AddendumCollection(0).Substring(67, 6))

                    Case "RA"
                        resubmit_date = ParseNullDate(AddendumCollection.Item(0).Substring(61, 6))

                    Case "TP"
                        third_party_detail = AddendumCollection(0).Substring(61, 2).Trim()
                        third_party_contact = AddendumCollection(0).Substring(63, 16).Trim()

                    Case Else
                        ' This is the general case. It is just "information required"
                        InfoRequired = AddendumCollection.Item(0).Substring(61, 18).Trim()
                End Select

                Dim cn As SqlClient.SqlConnection = New SqlConnection(DebtPlus.LINQ.SQLInfoClass.getDefault().ConnectionString)
                Dim current_cursor As Cursor = Cursor.Current
                Try
                    Cursor.Current = Cursors.WaitCursor
                    cn.Open()
                    With New SqlCommand
                        .Connection = cn
                        .CommandText = "xpr_rpps_response_CDR"
                        .CommandType = CommandType.StoredProcedure
                        .CommandTimeout = System.Math.Max(600, DebtPlus.LINQ.SQLInfoClass.getDefault().CommandTimeout)
                        .Parameters.Add("RETURN", SqlDbType.Int).Direction = ParameterDirection.ReturnValue
                        .Parameters.Add("@response_file", SqlDbType.Int).Value = Batch.File.ResponseFileID
                        .Parameters.Add("@trace_number", SqlDbType.VarChar, 80).Value = trace_number
                        .Parameters.Add("@biller_id", SqlDbType.VarChar, 80).Value = Batch.BillerID
                        .Parameters.Add("@return_code", SqlDbType.VarChar, 80).Value = GetError()
                        .Parameters.Add("@name", SqlDbType.VarChar, 80).Value = ClientName
                        .Parameters.Add("@account_number", SqlDbType.VarChar, 80).Value = account_number
                        .Parameters.Add("@net", SqlDbType.Decimal).Value = net
                        .Parameters.Add("@client", SqlDbType.Int).Value = Client
                        .Parameters.Add("@ssn", SqlDbType.VarChar, 80).Value = SSN
                        .Parameters.Add("@company_identification", SqlDbType.VarChar, 80).Value = CompanyIdentifier
                        .Parameters.Add("@reason", SqlDbType.VarChar, 80).Value = ReasonString
                        .Parameters.Add("@counter_offer", SqlDbType.Decimal).Value = CounterAmount
                        .Parameters.Add("@information_required", SqlDbType.VarChar, 80).Value = InfoRequired
                        .Parameters.Add("@resubmit_date", System.Data.SqlDbType.DateTime, 8).Value = resubmit_date
                        .Parameters.Add("@cycle_months_remaining", System.Data.SqlDbType.Int, 4).Value = cycle_months_remaining
                        .Parameters.Add("@forgiven_pct", System.Data.SqlDbType.Float, 8).Value = forgiven_pct
                        .Parameters.Add("@first_payment_date", System.Data.SqlDbType.DateTime, 8).Value = first_payment_date
                        .Parameters.Add("@good_thru_date", System.Data.SqlDbType.DateTime, 8).Value = good_thru_date
                        .Parameters.Add("@ineligible_reason", System.Data.SqlDbType.VarChar, 80).Value = ineligible_reason
                        .Parameters.Add("@internal_program_ends_date", System.Data.SqlDbType.DateTime, 8).Value = internal_program_ends_date
                        .Parameters.Add("@interest_rate", System.Data.SqlDbType.Float, 8).Value = interest_rate
                        .Parameters.Add("@third_party_detail", System.Data.SqlDbType.VarChar, 80).Value = third_party_detail
                        .Parameters.Add("@third_party_contact", System.Data.SqlDbType.VarChar, 80).Value = third_party_contact

                        .ExecuteNonQuery()
                    End With

                Catch ex As SqlClient.SqlException
                    DebtPlus.UI.Common.ErrorHandling.HandleErrors(ex, "Error recording CDR response")

                Finally
                    If cn IsNot Nothing AndAlso cn.State <> ConnectionState.Closed Then cn.Close()
                    Cursor.Current = current_cursor
                End Try
            End Sub

            ''' <summary>
            ''' This is a termination record
            ''' </summary>
            Protected Sub process_transaction_CDT(ByVal ClientName As String, ByVal account_number As String, ByVal trace_number As String, ByVal net As Decimal)

                ' Count an instance of our record type
                privateInstance.CDT += 1

                ' Fetch the fields from the input record
                Dim CompanyIdentifier As String = AddendumCollection.Item(0).Substring(21, 6)
                Dim SSN As String = AddendumCollection.Item(0).Substring(27, 9)
                Dim Client As Int32 = IntegerValue(AddendumCollection.Item(0), 36, 15)

                Dim cn As SqlClient.SqlConnection = New SqlConnection(DebtPlus.LINQ.SQLInfoClass.getDefault().ConnectionString)
                Dim current_cursor As Cursor = Cursor.Current
                Try
                    Cursor.Current = Cursors.WaitCursor
                    cn.Open()
                    With New SqlCommand
                        .Connection = cn
                        .CommandText = "xpr_rpps_response_CDT"
                        .CommandType = CommandType.StoredProcedure
                        .CommandTimeout = System.Math.Max(600, DebtPlus.LINQ.SQLInfoClass.getDefault().CommandTimeout)
                        .Parameters.Add("RETURN", SqlDbType.Int).Direction = ParameterDirection.ReturnValue
                        .Parameters.Add("@response_file", SqlDbType.Int).Value = Batch.File.ResponseFileID
                        .Parameters.Add("@trace_number", SqlDbType.VarChar, 80).Value = trace_number
                        .Parameters.Add("@biller_id", SqlDbType.VarChar, 80).Value = Batch.BillerID
                        .Parameters.Add("@return_code", SqlDbType.VarChar, 80).Value = GetError()
                        .Parameters.Add("@name", SqlDbType.VarChar, 80).Value = ClientName
                        .Parameters.Add("@account_number", SqlDbType.VarChar, 80).Value = account_number
                        .Parameters.Add("@net", SqlDbType.Decimal).Value = net
                        .Parameters.Add("@client", SqlDbType.Int).Value = Client
                        .Parameters.Add("@ssn", SqlDbType.VarChar, 80).Value = SSN
                        .Parameters.Add("@company_identification", SqlDbType.VarChar, 80).Value = CompanyIdentifier
                        .ExecuteNonQuery()
                    End With

                Catch ex As SqlClient.SqlException
                    DebtPlus.UI.Common.ErrorHandling.HandleErrors(ex, "Error recording CDT response")

                Finally
                    If cn IsNot Nothing AndAlso cn.State <> ConnectionState.Closed Then cn.Close()
                    Cursor.Current = current_cursor
                End Try
            End Sub

            ''' <summary>
            ''' This is a proposal acceptance record
            ''' </summary>
            Protected Sub process_transaction_CDA(ByVal ClientName As String, ByVal account_number As String, ByVal trace_number As String, ByVal net As Decimal)

                ' Count an instance of our record type
                privateInstance.CDA += 1

                ' Fetch the fields from the input record
                Dim CompanyIdentifier As String = AddendumCollection.Item(0).Substring(21, 6)
                Dim SSN As String = AddendumCollection.Item(0).Substring(27, 9)
                Dim Client As Int32 = IntegerValue(AddendumCollection.Item(0), 36, 15)
                Dim LastCommunication As String = AddendumCollection.Item(0).Substring(6, 10)
                Dim Balance As Decimal = IntegerValue(AddendumCollection.Item(0), 51, 10)
                Dim APR As Int32 = IntegerValue(AddendumCollection.Item(0), 61, 5)
                Dim payment_amount As Object = ParseNullMoney(AddendumCollection(0).Substring(66, 7))
                Dim start_date As Object = ParseNullDate(AddendumCollection(0).Substring(73, 6))

                Dim cn As SqlClient.SqlConnection = New SqlConnection(DebtPlus.LINQ.SQLInfoClass.getDefault().ConnectionString)
                Dim current_cursor As Cursor = Cursor.Current
                Try
                    Cursor.Current = Cursors.WaitCursor
                    cn.Open()
                    With New SqlCommand
                        .Connection = cn
                        .CommandText = "xpr_rpps_response_CDA"
                        .CommandType = CommandType.StoredProcedure
                        .CommandTimeout = System.Math.Max(600, DebtPlus.LINQ.SQLInfoClass.getDefault().CommandTimeout)
                        .Parameters.Add("RETURN", SqlDbType.Int).Direction = ParameterDirection.ReturnValue
                        .Parameters.Add("@response_file", SqlDbType.Int).Value = Batch.File.ResponseFileID
                        .Parameters.Add("@trace_number", SqlDbType.VarChar, 80).Value = trace_number
                        .Parameters.Add("@biller_id", SqlDbType.VarChar, 80).Value = Batch.BillerID
                        .Parameters.Add("@return_code", SqlDbType.VarChar, 80).Value = GetError()
                        .Parameters.Add("@name", SqlDbType.VarChar, 80).Value = ClientName
                        .Parameters.Add("@account_number", SqlDbType.VarChar, 80).Value = account_number
                        .Parameters.Add("@net", SqlDbType.Decimal).Value = net
                        .Parameters.Add("@client", SqlDbType.Int).Value = Client
                        .Parameters.Add("@ssn", SqlDbType.VarChar, 80).Value = SSN
                        .Parameters.Add("@company_identification", SqlDbType.VarChar, 80).Value = CompanyIdentifier
                        .Parameters.Add("@last_communication", SqlDbType.VarChar, 80).Value = LastCommunication
                        .Parameters.Add("@balance", SqlDbType.Decimal).Value = Balance
                        .Parameters.Add("@apr", SqlDbType.Int).Value = APR
                        .Parameters.Add("@payment_amount", System.Data.SqlDbType.Decimal).Value = payment_amount
                        .Parameters.Add("@start_date", System.Data.SqlDbType.DateTime, 8).Value = start_date

                        .ExecuteNonQuery()
                    End With

                Catch ex As SqlClient.SqlException
                    DebtPlus.UI.Common.ErrorHandling.HandleErrors(ex, "Error recording CDA response")

                Finally
                    If cn IsNot Nothing AndAlso cn.State <> ConnectionState.Closed Then cn.Close()
                    Cursor.Current = current_cursor
                End Try
            End Sub

            ''' <summary>
            ''' This is a balance verification record
            ''' </summary>
            Protected Sub process_transaction_CDV(ByVal ClientName As String, ByVal account_number As String, ByVal trace_number As String, ByVal net As Decimal)

                ' Count an instance of our record type
                privateInstance.CDV += 1

                ' Fetch the fields from the input record
                Dim Balance As Decimal = MoneyValue(AddendumCollection.Item(0), 21, 10)
                Dim LastCommunication As String = AddendumCollection.Item(0).Substring(31, 15)
                Dim SSN As String = AddendumCollection.Item(0).Substring(46, 9)
                Dim Client As Int32 = IntegerValue(AddendumCollection.Item(0), 55, 15)

                ' Reverse the sign of the balance if the amount should be negative. These are "C"redit balances.
                Dim Status As String = AddendumCollection(0).Substring(70, 1)
                If Status = "C" AndAlso Balance > 0 Then
                    Balance = 0 - Balance
                End If

                Dim cn As SqlClient.SqlConnection = New SqlConnection(DebtPlus.LINQ.SQLInfoClass.getDefault().ConnectionString)
                Dim current_cursor As Cursor = Cursor.Current
                Try
                    Cursor.Current = Cursors.WaitCursor
                    cn.Open()
                    With New SqlCommand
                        .Connection = cn
                        .CommandText = "xpr_rpps_response_CDV"
                        .CommandType = CommandType.StoredProcedure
                        .CommandTimeout = System.Math.Max(600, DebtPlus.LINQ.SQLInfoClass.getDefault().CommandTimeout)
                        .Parameters.Add("RETURN", SqlDbType.Int).Direction = ParameterDirection.ReturnValue
                        .Parameters.Add("@response_file", SqlDbType.Int).Value = Batch.File.ResponseFileID
                        .Parameters.Add("@trace_number", SqlDbType.VarChar, 80).Value = trace_number
                        .Parameters.Add("@biller_id", SqlDbType.VarChar, 80).Value = Batch.BillerID
                        .Parameters.Add("@return_code", SqlDbType.VarChar, 80).Value = GetError()
                        .Parameters.Add("@name", SqlDbType.VarChar, 80).Value = ClientName
                        .Parameters.Add("@account_number", SqlDbType.VarChar, 80).Value = account_number
                        .Parameters.Add("@net", SqlDbType.Decimal).Value = net
                        .Parameters.Add("@client", SqlDbType.Int).Value = Client
                        .Parameters.Add("@ssn", SqlDbType.VarChar, 80).Value = SSN
                        .Parameters.Add("@balance", SqlDbType.Decimal).Value = Balance
                        .Parameters.Add("@last_communication", SqlDbType.VarChar, 80).Value = LastCommunication
                        .ExecuteNonQuery()
                    End With

                Catch ex As SqlClient.SqlException
                    DebtPlus.UI.Common.ErrorHandling.HandleErrors(ex, "Error recording CDV response")

                Finally
                    If cn IsNot Nothing AndAlso cn.State <> ConnectionState.Closed Then cn.Close()
                    Cursor.Current = current_cursor
                End Try
            End Sub

            ''' <summary>
            ''' There are no detail records on this response
            ''' </summary>
            Protected Sub process_transaction_CIE(ByVal ClientName As String, ByVal account_number As String, ByVal trace_number As String, ByVal net As Decimal, ByVal gross As Decimal)

                ' Count an instance of our record type
                privateInstance.CIE += 1

                Dim cn As SqlClient.SqlConnection = New SqlConnection(DebtPlus.LINQ.SQLInfoClass.getDefault().ConnectionString)
                Dim current_cursor As Cursor = Cursor.Current
                Try
                    Cursor.Current = Cursors.WaitCursor
                    cn.Open()
                    With New SqlCommand
                        .Connection = cn
                        .CommandText = "xpr_rpps_response_CIE"
                        .CommandType = CommandType.StoredProcedure
                        .CommandTimeout = System.Math.Max(600, DebtPlus.LINQ.SQLInfoClass.getDefault().CommandTimeout)
                        .Parameters.Add("RETURN", SqlDbType.Int).Direction = ParameterDirection.ReturnValue
                        .Parameters.Add("@response_file", SqlDbType.Int).Value = Batch.File.ResponseFileID
                        .Parameters.Add("@trace_number", SqlDbType.VarChar, 80).Value = trace_number
                        .Parameters.Add("@biller_id", SqlDbType.VarChar, 80).Value = Batch.BillerID
                        .Parameters.Add("@return_code", SqlDbType.VarChar, 80).Value = GetError()
                        .Parameters.Add("@name", SqlDbType.VarChar, 80).Value = ClientName
                        .Parameters.Add("@account_number", SqlDbType.VarChar, 80).Value = account_number
                        .Parameters.Add("@net", SqlDbType.Decimal).Value = net
                        .Parameters.Add("@gross", SqlDbType.Decimal).Value = gross
                        .ExecuteNonQuery()
                    End With

                Catch ex As SqlClient.SqlException
                    DebtPlus.UI.Common.ErrorHandling.HandleErrors(ex, "Error recording CIE response")

                Finally
                    If cn IsNot Nothing AndAlso cn.State <> ConnectionState.Closed Then cn.Close()
                    Cursor.Current = current_cursor
                End Try
            End Sub

            ''' <summary>
            ''' Record the transactions to the database
            ''' </summary>
            Public Overrides Sub UpdateDatabase()
                Dim trace_number As String

                ' Parse the first record. These are common for all record types.
                Dim ClientName As String = TransactionLine.Substring(39, 15).Trim()
                Dim account_number As String = TransactionLine.Substring(54, 22).Trim()
                Dim net As Decimal = MoneyValue(TransactionLine, 29, 10)
                Dim Gross As Decimal = 0D

                ' The gross amount is part of the client name field if this is a payment request
                If ClientName Like "##########?*" Then ' 10 digits followed by a non-blank and then anything.
                    Gross = MoneyValue(ClientName.Substring(0, 10))
                    ClientName = ClientName.Substring(11).Trim()
                End If

                ' If there are no addendum records then this must be a payment reject
                If AddendumCollection.Count = 0 Then
                    ErrorCode = TransactionLine.Substring(12, 3)
                    trace_number = TransactionLine.Substring(79, 15)
                    process_transaction_CIE(ClientName, account_number, trace_number, net, Gross)

                Else
                    ' Otherwise, look at the batch type to find the error and trace numbers
                    Select Case Batch.CompanyEntryDescription
                        Case "RETURN    ", "CONVERSION"
                            ErrorCode = AddendumCollection.Item(0).Substring(3, 3)
                            trace_number = AddendumCollection.Item(0).Substring(6, 15)

                        Case Else ' "REJECT ENT", "REJECT BAT", "REJECT FIL", "ACCEPT FIL"
                            ErrorCode = TransactionLine.Substring(12, 3)
                            trace_number = TransactionLine.Substring(79, 15)
                    End Select

                    ' Look at the record to determine the subtype processing desired
                    Select Case AddendumCollection.Item(0).Substring(3, 3)
                        Case "C09", "C11", "C50"
                            process_transaction_ADV(ClientName, account_number, trace_number, net, Gross)

                        Case "CDA"
                            process_transaction_CDA(ClientName, account_number, trace_number, net)

                        Case "CDR"
                            process_transaction_CDR(ClientName, account_number, trace_number, net)

                        Case "CDC"
                            process_transaction_CDC(ClientName, account_number, trace_number, net)

                        Case "CDD"
                            process_transaction_CDD(ClientName, account_number, trace_number, net)

                        Case "CDT"
                            process_transaction_CDT(ClientName, account_number, trace_number, net)

                        Case "CDV"
                            process_transaction_CDV(ClientName, account_number, trace_number, net)

                        Case "CDP"
                            process_transaction_CDP(ClientName, account_number, trace_number, net)

                        Case "FBC"
                            process_transaction_FBC(ClientName, account_number, trace_number, net)

                        Case "FBD"
                            process_transaction_FBD(ClientName, account_number, trace_number, net)

                        Case Else
                            process_transaction_CIE(ClientName, account_number, trace_number, net, Gross)
                    End Select
                End If
            End Sub
        End Class

        ''' <summary>
        ''' Process a message return transaction for a MESSAGE batch type
        ''' </summary>
        Private Class MessageTransactionProcessing
            Inherits StandardTransactionProcessing

            ''' <summary>
            ''' Create an instance of our class
            ''' </summary>
            Public Sub New(ByVal rdr As Reader, ByVal Batch As BatchProcessing)
                MyBase.New(rdr, Batch)
            End Sub

            Public Sub New()
                MyBase.New()
            End Sub

            Public Sub New(ByVal rdr As Reader)
                MyBase.New(rdr)
            End Sub

            ''' <summary>
            ''' Record the transactions to the database
            ''' </summary>
            Public Overrides Sub UpdateDatabase()

                Dim LastSequence As Int32 = 0
                Dim Net As Decimal = MoneyValue(TransactionLine, 29, 10) / 100D
                Dim account_number As String = TransactionLine.Substring(54, 22).Trim()
                Dim ClientName As String = TransactionLine.Substring(39, 15).Trim()
                Dim trace_number As String = String.Empty
                Dim TextMessage As String = String.Empty

                ' Count an instance of our record type
                privateInstance.CDM += 1

                ' The error code is always CDM for now.
                ErrorCode = "CDM"

                ' Process the addendum lines with the line text
                Dim cn As SqlClient.SqlConnection = New SqlConnection(DebtPlus.LINQ.SQLInfoClass.getDefault().ConnectionString)
                Dim current_cursor As Cursor = Cursor.Current
                Try
                    Cursor.Current = Cursors.WaitCursor
                    cn.Open()

                    For Each CurrentInputLine As String In AddendumCollection
                        Dim current_sequence As Int32 = 0
                        If Not Int32.TryParse(CurrentInputLine.Substring(83, 4), Globalization.NumberStyles.AllowLeadingWhite Or Globalization.NumberStyles.AllowTrailingWhite, System.Globalization.CultureInfo.InvariantCulture, current_sequence) OrElse current_sequence < 0 Then current_sequence = 0

                        If current_sequence = 1 Then
                            trace_number = CurrentInputLine.Substring(6, 15)
                            TextMessage = CurrentInputLine.Substring(21, 62).Trim()
                        Else
                            TextMessage = CurrentInputLine.Substring(6, 77).Trim()
                        End If

                        ' Count the sequence number
                        LastSequence += 1

                        With New SqlCommand
                            .Connection = cn
                            .CommandText = "xpr_rpps_response_CDM"
                            .CommandType = CommandType.StoredProcedure
                            .CommandTimeout = System.Math.Max(600, DebtPlus.LINQ.SQLInfoClass.getDefault().CommandTimeout)

                            .Parameters.Add("@response_file", SqlDbType.Int).Value = Batch.File.ResponseFileID
                            .Parameters.Add("@trace_number", SqlDbType.VarChar, 80).Value = trace_number
                            .Parameters.Add("@biller_id", SqlDbType.VarChar, 80).Value = Batch.BillerID
                            .Parameters.Add("@error_code", SqlDbType.VarChar, 80).Value = GetError()
                            .Parameters.Add("@name", SqlDbType.VarChar, 80).Value = ClientName
                            .Parameters.Add("@account_number", SqlDbType.VarChar, 80).Value = account_number
                            .Parameters.Add("@net", SqlDbType.Decimal).Value = Net
                            .Parameters.Add("@line_number", SqlDbType.Int).Value = current_sequence
                            .Parameters.Add("@message", SqlDbType.VarChar, 256).Value = TextMessage

                            .ExecuteNonQuery()
                        End With
                    Next

                Catch ex As SqlClient.SqlException
                    DebtPlus.UI.Common.ErrorHandling.HandleErrors(ex, "Error recording CDM response")

                Finally
                    If cn IsNot Nothing AndAlso cn.State <> ConnectionState.Closed Then cn.Close()
                    Cursor.Current = current_cursor
                End Try
            End Sub
        End Class

        ''' <summary>
        ''' Process a message return transaction for a MESSAGE batch type
        ''' </summary>
        Private Class NotificationOfChangeTransactionProcessing
            Inherits StandardTransactionProcessing

            ''' <summary>
            ''' Create an instance of our class
            ''' </summary>
            ''' <param name="rdr">Pointer to the data reader</param>
            ''' <param name="Batch">Batch ID associated with this RPPS batch</param>
            ''' <remarks></remarks>
            Public Sub New(ByVal rdr As Reader, ByVal Batch As BatchProcessing)
                MyBase.New(rdr, Batch)
            End Sub

            ''' <summary>
            ''' Create an instance of our class
            ''' </summary>
            ''' <remarks></remarks>
            Public Sub New()
                MyBase.New()
            End Sub

            ''' <summary>
            ''' Create an instance of our class
            ''' </summary>
            ''' <param name="rdr">Pointer to the data reader</param>
            ''' <remarks></remarks>
            Public Sub New(ByVal rdr As Reader)
                MyBase.New(rdr)
            End Sub

            ''' <summary>
            ''' Process an advisory change in the account numbers
            ''' </summary>
            ''' <param name="ChangeAccountNumber">TRUE if there is a change in the account number</param>
            ''' <param name="ChangeBillerID">TRUE if there is a change in the biller ID number</param>
            ''' <param name="ErrorCode">Error code asssociated with the transaction</param>
            ''' <param name="OriginalEntryTraceNumber">Original trace number to find the transaction</param>
            ''' <param name="OldAccountNumber">Old account number if there is a change</param>
            ''' <param name="NewAccountNumber">New account number if there is a change</param>
            ''' <param name="NewBillerID">New biller id for the creditor</param>
            ''' <param name="ClientName">Client name</param>
            ''' <remarks></remarks>
            Protected Sub process_transaction_COA(ByVal ChangeAccountNumber As Boolean, ByVal ChangeBillerID As Boolean, ByVal ErrorCode As String, ByVal OriginalEntryTraceNumber As String, ByVal OldAccountNumber As String, ByVal NewAccountNumber As String, ByVal NewBillerID As String, ByVal ClientName As String)

                ' Count an instance of our record type
                privateInstance.CIE += 1

                Dim cn As SqlClient.SqlConnection = New SqlConnection(DebtPlus.LINQ.SQLInfoClass.getDefault().ConnectionString)
                Dim current_cursor As Cursor = Cursor.Current
                Try
                    Cursor.Current = Cursors.WaitCursor
                    cn.Open()
                    With New SqlCommand
                        .Connection = cn
                        .CommandText = "xpr_rpps_response_COA"
                        .CommandType = CommandType.StoredProcedure
                        .CommandTimeout = System.Math.Max(600, DebtPlus.LINQ.SQLInfoClass.getDefault().CommandTimeout)

                        .Parameters.Add("RETURN", SqlDbType.Int).Direction = ParameterDirection.ReturnValue
                        .Parameters.Add("@response_file", SqlDbType.Int).Value = Batch.File.ResponseFileID
                        .Parameters.Add("@trace_number", SqlDbType.VarChar, 80).Value = OriginalEntryTraceNumber
                        .Parameters.Add("@biller_id", SqlDbType.VarChar, 80).Value = Batch.BillerID
                        .Parameters.Add("@return_code", SqlDbType.VarChar, 80).Value = ErrorCode
                        .Parameters.Add("@name", SqlDbType.VarChar, 80).Value = ClientName
                        .Parameters.Add("@OldAccountNumber", SqlDbType.VarChar, 80).Value = OldAccountNumber
                        .Parameters.Add("@ChangeAccountNumber", SqlDbType.Bit, 0).Value = ChangeAccountNumber
                        .Parameters.Add("@NewAccountNumber", SqlDbType.VarChar, 80).Value = NewAccountNumber
                        .Parameters.Add("@ChangeBillerID", SqlDbType.Bit, 0).Value = ChangeBillerID
                        .Parameters.Add("@NewBillerID", SqlDbType.VarChar, 80).Value = NewBillerID
                        .ExecuteNonQuery()
                    End With

                Catch ex As SqlClient.SqlException
                    DebtPlus.UI.Common.ErrorHandling.HandleErrors(ex, "Error recording COA response")

                Finally
                    If cn IsNot Nothing Then cn.Dispose()

                    Cursor.Current = current_cursor
                End Try
            End Sub

            ''' <summary>
            ''' Change in the account number
            ''' </summary>
            ''' <param name="OriginalEntryTraceNumber">Original trace number</param>
            ''' <param name="OldAccountNumber">Old account number value</param>
            ''' <param name="NewAccountNumber">New account number value</param>
            ''' <param name="NewBillerID">New biller id value</param>
            ''' <param name="ClientName">Client name</param>
            ''' <remarks></remarks>
            Protected Sub process_transaction_C50(ByVal OriginalEntryTraceNumber As String, ByVal OldAccountNumber As String, ByVal NewAccountNumber As String, ByVal NewBillerID As String, ByVal ClientName As String)
                process_transaction_COA(True, False, "C50", OriginalEntryTraceNumber, OldAccountNumber, NewAccountNumber, NewBillerID, ClientName)
            End Sub

            ''' <summary>
            ''' Change in the biller ID number
            ''' </summary>
            ''' <param name="OriginalEntryTraceNumber">Original trace number</param>
            ''' <param name="OldAccountNumber">Old account number value</param>
            ''' <param name="NewAccountNumber">New account number value</param>
            ''' <param name="NewBillerID">New biller id value</param>
            ''' <param name="ClientName">Client name</param>
            ''' <remarks></remarks>
            Protected Sub process_transaction_C51(ByVal OriginalEntryTraceNumber As String, ByVal OldAccountNumber As String, ByVal NewAccountNumber As String, ByVal NewBillerID As String, ByVal ClientName As String)
                process_transaction_COA(False, True, "C51", OriginalEntryTraceNumber, OldAccountNumber, NewAccountNumber, NewBillerID, ClientName)
            End Sub

            ''' <summary>
            ''' Change in the account number and biller ID number
            ''' </summary>
            ''' <param name="OriginalEntryTraceNumber">Original trace number</param>
            ''' <param name="OldAccountNumber">Old account number value</param>
            ''' <param name="NewAccountNumber">New account number value</param>
            ''' <param name="NewBillerID">New biller id value</param>
            ''' <param name="ClientName">Client name</param>
            ''' <remarks></remarks>
            Protected Sub process_transaction_C52(ByVal OriginalEntryTraceNumber As String, ByVal OldAccountNumber As String, ByVal NewAccountNumber As String, ByVal NewBillerID As String, ByVal ClientName As String)
                process_transaction_COA(True, True, "C52", OriginalEntryTraceNumber, OldAccountNumber, NewAccountNumber, NewBillerID, ClientName)
            End Sub

            ''' <summary>
            ''' Record the transactions to the database
            ''' </summary>
            Public Overrides Sub UpdateDatabase()

                ' Parse the first record. These are common for all record types.
                Dim ClientName As String = TransactionLine.Substring(39, 15).Trim()
                Dim OldAccountNumber As String = TransactionLine.Substring(54, 22).Trim()

                ' These are in the first addendum record
                Dim ErrorCode As String = String.Empty
                Dim OriginalEntryTraceNumber As String = String.Empty
                Dim OriginalDFI As String = String.Empty
                Dim NewAccountNumber As String = String.Empty
                Dim NewBillerID As String = String.Empty

                ' Find the information from the addendum records
                If AddendumCollection.Count > 0 Then
                    Dim Addendum As String = AddendumCollection(0)
                    If Addendum.Substring(0, 3) = "798" Then
                        ErrorCode = Addendum.Substring(3, 3).Trim()
                        OriginalEntryTraceNumber = Addendum.Substring(6, 15)
                        OriginalDFI = Addendum.Substring(27, 8)
                        NewAccountNumber = Addendum.Substring(35, 22).Trim()
                        NewBillerID = Addendum.Substring(57, 10)
                    End If
                End If

                ' Look at the reason for the transaction and handle accordingly.
                Select Case ErrorCode
                    Case "C50"
                        process_transaction_C50(OriginalEntryTraceNumber, OldAccountNumber, NewAccountNumber, NewBillerID, ClientName)
                    Case "C51"
                        process_transaction_C51(OriginalEntryTraceNumber, OldAccountNumber, NewAccountNumber, NewBillerID, ClientName)
                    Case "C52"
                        process_transaction_C52(OriginalEntryTraceNumber, OldAccountNumber, NewAccountNumber, NewBillerID, ClientName)
                    Case Else
                End Select
            End Sub
        End Class

        ''' <summary>
        ''' A batch is a representation of the following class
        ''' </summary>
        Private Class BatchProcessing
            Inherits Responses

            ' List of transactions for this batch
            Private ReadOnly TranCollection As New LinkedList(Of Responses)()
            Public File As FileProcessing

            ' Information from the batch header
            Public BatchHeader As String = String.Empty
            Public BillerID As String = String.Empty
            Public CompanyEntryDescription As String = String.Empty
            Public ServiceClassCode As String = String.Empty
            Public StandardEntryClass As String = String.Empty

            ''' <summary>
            ''' Create an instance of our class
            ''' </summary>
            Public Sub New()
                MyBase.New()
            End Sub

            Public Sub New(ByVal rdr As Reader, ByVal File As FileProcessing)
                MyBase.New(rdr)
                Me.File = File
            End Sub

            Public Sub New(ByVal rdr As Reader)
                MyBase.New(rdr)
            End Sub

            ''' <summary>
            ''' Obtain the error status for the file
            ''' </summary>
            Public Overrides Function GetError() As String
                Dim answer As String = File.GetError
                If answer = String.Empty Then answer = ErrorCode
                Return answer
            End Function

            ''' <summary>
            ''' Process a batch from the creditor
            ''' </summary>
            Public Function ParseType5() As Boolean
                Dim answer As Boolean = False

                ' Determine the biller id for the response
                BatchHeader = rdr.NextLine
                BillerID = BatchHeader.Substring(40, 10).Trim()

                ' Save the fields from the batch header
                ServiceClassCode = BatchHeader.Substring(1, 3).Trim()
                StandardEntryClass = BatchHeader.Substring(50, 3).Trim()
                CompanyEntryDescription = BatchHeader.Substring(53, 10)

                ' Read the first transaction line which should follow the batch header
                If Not rdr.ReadNextLine Then
                    Return False
                End If

                ' If this is the end of the batch then stop processing
                Do
                    If rdr.IsType8 Then
                        ParseType8()
                        answer = True
                        Exit Do
                    End If

                    ' If this is the end of file record then we missed the batch EOF
                    If rdr.IsType9 OrElse rdr.IsType1 Then
                        answer = False
                        Exit Do
                    End If

                    ' If this is a type 6 record then process the batch
                    If rdr.IsType6 Then

                        ' If this is a correction batch then process it seperately.
                        If StandardEntryClass = "CIE" Then
                            Select Case CompanyEntryDescription
                                Case "CONVERSION"
                                    Dim Tran As New NotificationOfChangeTransactionProcessing(rdr, Me)
                                    If Tran.ParseType6 Then
                                        TranCollection.AddLast(Tran)
                                    End If

                                    ' Look for message lines
                                Case "MESSAGE   ", "MSG RETURN"
                                    Dim Tran As New MessageTransactionProcessing(rdr, Me)
                                    If Tran.ParseType6 Then
                                        TranCollection.AddLast(Tran)
                                    End If

                                    ' Take all others as payment and proposal rejects
                                Case Else
                                    Dim Tran As New StandardTransactionProcessing(rdr, Me)
                                    If Tran.ParseType6 Then
                                        TranCollection.AddLast(Tran)
                                    End If
                            End Select
                        End If

                        ' If this is not the expected type, flush the record until we find one.
                    ElseIf Not rdr.ReadNextLine Then
                        answer = False
                        Exit Do
                    End If
                Loop

                Return answer
            End Function

            ''' <summary>
            ''' Parse the type 8 (EOB) record for batch trailer
            ''' </summary>
            Public Function ParseType8() As Boolean

                ' Save the error code associated with the batch
                ErrorCode = rdr.NextLine.Substring(54, 3).Trim()

                Return False
            End Function

            ''' <summary>
            ''' Record the transactions to the database
            ''' </summary>
            Public Overrides Sub UpdateDatabase()

                ' Count an instance of our record type
                privateInstance.batch_count += 1

                For Each tran As Responses In TranCollection
                    tran.UpdateDatabase()
                Next
            End Sub
        End Class

        ''' <summary>
        ''' A file is a representation of the following class
        ''' </summary>
        Private Class FileProcessing
            Inherits Responses

            Public ResponseFileID As Int32 = -1

            ''' <summary>
            ''' Create an instance of our class
            ''' </summary>
            Public Sub New()
                MyBase.New()
            End Sub

            Public Sub New(ByVal rdr As Reader, ByVal ResponseFileID As Int32)
                MyBase.New(rdr)
                Me.ResponseFileID = ResponseFileID
            End Sub

            Public Sub New(ByVal rdr As Reader)
                MyBase.New(rdr)
            End Sub

            ' List of batches in this file
            Private ReadOnly BatchCollection As New LinkedList(Of BatchProcessing)()
            Public FileHeader As String = String.Empty
            Public FileTrailer As String = String.Empty

            ''' <summary>
            ''' Examine the type 1 (HEADER) record which starts the file
            ''' </summary>
            Public Function ParseType1() As Boolean
                Dim answer As Boolean = False

                ' Examine the type 1 start of file record
                FileHeader = rdr.NextLine
                If Not FileHeader.StartsWith("101") OrElse String.Compare(FileHeader.Substring(34, 3), "094", False) <> 0 Then
                    DebtPlus.Data.Forms.MessageBox.Show("The file does not have a valid RPPS reject header near line 1")
                    Return False
                End If

                ' Ensure that the file is not a response file
                If String.Compare(FileHeader.Substring(63, 23), "MC REMIT PROC CENT SITE", True) <> 0 Then
                    DebtPlus.Data.Forms.MessageBox.Show("The file is not an RPPS response file. It is missing the proper source marker in columns 64 to 87.")
                    Return False
                End If

                ' Read the first line for the file contents
                If Not rdr.ReadNextLine Then
                    Return False
                End If

                Do
                    ' If this is the end of the file then stop processing
                    If rdr.IsType9 Then
                        ParseType9()
                        answer = True
                        Exit Do
                    End If

                    ' If this is a new type 1 record then we missed the type 9
                    If rdr.IsType1 Then
                        answer = False
                        Exit Do
                    End If

                    ' If this is a type 5 record then process the batch
                    If rdr.IsType5 Then
                        Dim batch As New BatchProcessing(rdr, Me)
                        If batch.ParseType5 Then
                            BatchCollection.AddLast(batch)
                        End If

                    ElseIf Not rdr.ReadNextLine Then
                        answer = False
                        Exit Do
                    End If
                Loop

                Return answer
            End Function

            ''' <summary>
            ''' Examine the type 9 (EOF) record which terminates the file
            ''' </summary>
            Public Function ParseType9() As Boolean

                ' Save the file trailer line
                FileTrailer = rdr.NextLine

                ' Isolate the error code from the file trailer
                ErrorCode = FileTrailer.Substring(56, 3).Trim()

                Return False
            End Function

            ''' <summary>
            ''' Record the transactions to the database
            ''' </summary>
            Public Overrides Sub UpdateDatabase()

                ' Count an instance of our record type
                privateInstance.file_count += 1

                For Each batch As BatchProcessing In BatchCollection
                    batch.UpdateDatabase()
                Next
            End Sub
        End Class

        ''' <summary>
        ''' Class to manage the processing of multiple files in a dataset
        ''' </summary>
        Private Class InputProcessing

            ''' <summary>
            ''' Create an instance of our class
            ''' </summary>
            Private ReadOnly ResponseFileID As Int32 = -1
            Public Sub New(ByVal ResponseFileID As Int32)
                Me.ResponseFileID = ResponseFileID
            End Sub

            ''' <summary>
            ''' Parse the input file
            ''' </summary>
            Public Sub ParseFile(ByVal FileName As String)
                Dim rdr As New Reader(FileName)

                ' Open and read the first line in the file
                If rdr.OpenFile() Then
                    If rdr.ReadNextLine Then
                        Do
                            ' If the record is a type 1 then process the new file
                            If rdr.IsType1 Then
                                With New FileProcessing(rdr, ResponseFileID)
                                    .ParseType1()
                                    .UpdateDatabase()
                                End With

                                ' Flush records until we reach the end or we find a type 1 record again.
                            ElseIf Not rdr.ReadNextLine Then
                                Exit Do
                            End If
                        Loop
                    End If

                    ' Close the opened file
                    rdr.CloseFile()
                End If
            End Sub
        End Class

        ''' <summary>
        ''' Post the response file
        ''' </summary>
        Public Sub PostChanges()
            Dim answer As Boolean = False

            Dim cn As SqlClient.SqlConnection = New SqlConnection(DebtPlus.LINQ.SQLInfoClass.getDefault().ConnectionString)
            Dim current_cursor As Cursor = System.Windows.Forms.Cursor.Current
            Try
                System.Windows.Forms.Cursor.Current = Cursors.WaitCursor
                cn.Open()
                Using cmd As SqlClient.SqlCommand = New SqlCommand
                    With cmd
                        .Connection = cn
                        .CommandText = "xpr_rpps_response_process_post"
                        .CommandType = CommandType.StoredProcedure
                        .CommandTimeout = System.Math.Max(600, DebtPlus.LINQ.SQLInfoClass.getDefault().CommandTimeout)
                        .Parameters.Add("@response_file", SqlDbType.Int).Value = ap.rpps_response_file
                        .ExecuteNonQuery()
                        answer = True
                    End With
                End Using

            Catch ex As SqlClient.SqlException
                DebtPlus.UI.Common.ErrorHandling.HandleErrors(ex, "Error posting the response file")

            Finally
                If cn IsNot Nothing Then cn.Dispose()
                System.Windows.Forms.Cursor.Current = current_cursor
            End Try
        End Sub

#Region "Windows Form Designer generated code "

        'Form overrides dispose to clean up the component list.
        Protected Overloads Overrides Sub Dispose(ByVal Disposing As Boolean)
            If Disposing Then
                If components IsNot Nothing Then
                    components.Dispose()
                End If
            End If
            MyBase.Dispose(Disposing)
        End Sub

        Friend WithEvents lbl_ADV_count As DevExpress.XtraEditors.LabelControl
        Friend WithEvents lbl_ADV_text As DevExpress.XtraEditors.LabelControl
        Friend WithEvents Button_OK As DevExpress.XtraEditors.SimpleButton
        Friend WithEvents Button_Print As DevExpress.XtraEditors.SimpleButton
        Friend WithEvents lbl_cdm_text As DevExpress.XtraEditors.LabelControl
        Friend WithEvents lbl_cie_text As DevExpress.XtraEditors.LabelControl
        Friend WithEvents lbl_cda_text As DevExpress.XtraEditors.LabelControl
        Friend WithEvents lbl_cdr_text As DevExpress.XtraEditors.LabelControl
        Friend WithEvents lbl_cdv_text As DevExpress.XtraEditors.LabelControl
        Friend WithEvents lbl_batch_count_text As DevExpress.XtraEditors.LabelControl
        Friend WithEvents lbl_file_count_text As DevExpress.XtraEditors.LabelControl
        Friend WithEvents lbl_cdc_text As DevExpress.XtraEditors.LabelControl
        Friend WithEvents lbl_cdm As DevExpress.XtraEditors.LabelControl
        Friend WithEvents lbl_cie As DevExpress.XtraEditors.LabelControl
        Friend WithEvents lbl_cda As DevExpress.XtraEditors.LabelControl
        Friend WithEvents lbl_cdr As DevExpress.XtraEditors.LabelControl
        Friend WithEvents lbl_cdv As DevExpress.XtraEditors.LabelControl
        Friend WithEvents lbl_batch_count As DevExpress.XtraEditors.LabelControl
        Friend WithEvents lbl_file_count As DevExpress.XtraEditors.LabelControl
        Friend WithEvents lbl_cdc As DevExpress.XtraEditors.LabelControl
        Friend WithEvents MarqueeProgressBarControl1 As DevExpress.XtraEditors.MarqueeProgressBarControl

        'Required by the Windows Form Designer
        Private components As System.ComponentModel.IContainer
        'NOTE: The following procedure is required by the Windows Form Designer
        'It can be modified using the Windows Form Designer.
        'Do not modify it using the code editor.
        <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
            Me.Button_OK = New DevExpress.XtraEditors.SimpleButton
            Me.Button_Print = New DevExpress.XtraEditors.SimpleButton
            Me.lbl_cdm_text = New DevExpress.XtraEditors.LabelControl
            Me.lbl_cie_text = New DevExpress.XtraEditors.LabelControl
            Me.lbl_cda_text = New DevExpress.XtraEditors.LabelControl
            Me.lbl_cdr_text = New DevExpress.XtraEditors.LabelControl
            Me.lbl_cdv_text = New DevExpress.XtraEditors.LabelControl
            Me.lbl_batch_count_text = New DevExpress.XtraEditors.LabelControl
            Me.lbl_file_count_text = New DevExpress.XtraEditors.LabelControl
            Me.lbl_cdc_text = New DevExpress.XtraEditors.LabelControl
            Me.lbl_cdm = New DevExpress.XtraEditors.LabelControl
            Me.lbl_cie = New DevExpress.XtraEditors.LabelControl
            Me.lbl_cda = New DevExpress.XtraEditors.LabelControl
            Me.lbl_cdr = New DevExpress.XtraEditors.LabelControl
            Me.lbl_cdv = New DevExpress.XtraEditors.LabelControl
            Me.lbl_batch_count = New DevExpress.XtraEditors.LabelControl
            Me.lbl_file_count = New DevExpress.XtraEditors.LabelControl
            Me.lbl_cdc = New DevExpress.XtraEditors.LabelControl
            Me.MarqueeProgressBarControl1 = New DevExpress.XtraEditors.MarqueeProgressBarControl
            Me.lbl_ADV_count = New DevExpress.XtraEditors.LabelControl
            Me.lbl_ADV_text = New DevExpress.XtraEditors.LabelControl
            CType(Me.DxErrorProvider1, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.MarqueeProgressBarControl1.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
            Me.SuspendLayout()
            '
            'Button_OK
            '
            Me.Button_OK.Location = New System.Drawing.Point(63, 230)
            Me.Button_OK.Name = "Button_OK"
            Me.Button_OK.Size = New System.Drawing.Size(75, 23)
            Me.Button_OK.TabIndex = 0
            Me.Button_OK.Text = "&Cancel"
            '
            'Button_Print
            '
            Me.Button_Print.Enabled = False
            Me.Button_Print.Location = New System.Drawing.Point(153, 230)
            Me.Button_Print.Name = "Button_Print"
            Me.Button_Print.Size = New System.Drawing.Size(75, 23)
            Me.Button_Print.TabIndex = 1
            Me.Button_Print.Text = "&Print"
            '
            'lbl_cdm_text
            '
            Me.lbl_cdm_text.Location = New System.Drawing.Point(12, 12)
            Me.lbl_cdm_text.Name = "lbl_cdm_text"
            Me.lbl_cdm_text.Size = New System.Drawing.Size(84, 13)
            Me.lbl_cdm_text.TabIndex = 2
            Me.lbl_cdm_text.Text = "Message Records"
            '
            'lbl_cie_text
            '
            Me.lbl_cie_text.Location = New System.Drawing.Point(12, 31)
            Me.lbl_cie_text.Name = "lbl_cie_text"
            Me.lbl_cie_text.Size = New System.Drawing.Size(93, 13)
            Me.lbl_cie_text.TabIndex = 3
            Me.lbl_cie_text.Text = "All Others (Payments?)"
            '
            'lbl_cda_text
            '
            Me.lbl_cda_text.Location = New System.Drawing.Point(12, 50)
            Me.lbl_cda_text.Name = "lbl_cda_text"
            Me.lbl_cda_text.Size = New System.Drawing.Size(94, 13)
            Me.lbl_cda_text.TabIndex = 4
            Me.lbl_cda_text.Text = "Proposals Accepted"
            '
            'lbl_cdr_text
            '
            Me.lbl_cdr_text.Location = New System.Drawing.Point(12, 69)
            Me.lbl_cdr_text.Name = "lbl_cdr_text"
            Me.lbl_cdr_text.Size = New System.Drawing.Size(92, 13)
            Me.lbl_cdr_text.TabIndex = 5
            Me.lbl_cdr_text.Text = "Proposals Rejected"
            '
            'lbl_cdv_text
            '
            Me.lbl_cdv_text.Location = New System.Drawing.Point(12, 88)
            Me.lbl_cdv_text.Name = "lbl_cdv_text"
            Me.lbl_cdv_text.Size = New System.Drawing.Size(98, 13)
            Me.lbl_cdv_text.TabIndex = 6
            Me.lbl_cdv_text.Text = "Balance Verifications"
            '
            'lbl_batch_count_text
            '
            Me.lbl_batch_count_text.Location = New System.Drawing.Point(12, 107)
            Me.lbl_batch_count_text.Name = "lbl_batch_count_text"
            Me.lbl_batch_count_text.Size = New System.Drawing.Size(90, 13)
            Me.lbl_batch_count_text.TabIndex = 7
            Me.lbl_batch_count_text.Text = "Batches Processed"
            '
            'lbl_file_count_text
            '
            Me.lbl_file_count_text.Location = New System.Drawing.Point(12, 126)
            Me.lbl_file_count_text.Name = "lbl_file_count_text"
            Me.lbl_file_count_text.Size = New System.Drawing.Size(73, 13)
            Me.lbl_file_count_text.TabIndex = 8
            Me.lbl_file_count_text.Text = "Files Processed"
            '
            'lbl_cdc_text
            '
            Me.lbl_cdc_text.Location = New System.Drawing.Point(12, 145)
            Me.lbl_cdc_text.Name = "lbl_cdc_text"
            Me.lbl_cdc_text.Size = New System.Drawing.Size(124, 13)
            Me.lbl_cdc_text.TabIndex = 9
            Me.lbl_cdc_text.Text = "Account Number Changes"
            '
            'lbl_cdm
            '
            Me.lbl_cdm.Appearance.Options.UseTextOptions = True
            Me.lbl_cdm.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
            Me.lbl_cdm.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None
            Me.lbl_cdm.Location = New System.Drawing.Point(230, 12)
            Me.lbl_cdm.Name = "lbl_cdm"
            Me.lbl_cdm.Size = New System.Drawing.Size(50, 13)
            Me.lbl_cdm.TabIndex = 10
            Me.lbl_cdm.Text = "0"
            '
            'lbl_cie
            '
            Me.lbl_cie.Appearance.Options.UseTextOptions = True
            Me.lbl_cie.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
            Me.lbl_cie.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None
            Me.lbl_cie.Location = New System.Drawing.Point(230, 31)
            Me.lbl_cie.Name = "lbl_cie"
            Me.lbl_cie.Size = New System.Drawing.Size(50, 13)
            Me.lbl_cie.TabIndex = 11
            Me.lbl_cie.Text = "0"
            '
            'lbl_cda
            '
            Me.lbl_cda.Appearance.Options.UseTextOptions = True
            Me.lbl_cda.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
            Me.lbl_cda.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None
            Me.lbl_cda.Location = New System.Drawing.Point(230, 50)
            Me.lbl_cda.Name = "lbl_cda"
            Me.lbl_cda.Size = New System.Drawing.Size(50, 13)
            Me.lbl_cda.TabIndex = 12
            Me.lbl_cda.Text = "0"
            '
            'lbl_cdr
            '
            Me.lbl_cdr.Appearance.Options.UseTextOptions = True
            Me.lbl_cdr.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
            Me.lbl_cdr.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None
            Me.lbl_cdr.Location = New System.Drawing.Point(230, 69)
            Me.lbl_cdr.Name = "lbl_cdr"
            Me.lbl_cdr.Size = New System.Drawing.Size(50, 13)
            Me.lbl_cdr.TabIndex = 13
            Me.lbl_cdr.Text = "0"
            '
            'lbl_cdv
            '
            Me.lbl_cdv.Appearance.Options.UseTextOptions = True
            Me.lbl_cdv.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
            Me.lbl_cdv.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None
            Me.lbl_cdv.Location = New System.Drawing.Point(230, 88)
            Me.lbl_cdv.Name = "lbl_cdv"
            Me.lbl_cdv.Size = New System.Drawing.Size(50, 13)
            Me.lbl_cdv.TabIndex = 14
            Me.lbl_cdv.Text = "0"
            '
            'lbl_batch_count
            '
            Me.lbl_batch_count.Appearance.Options.UseTextOptions = True
            Me.lbl_batch_count.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
            Me.lbl_batch_count.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None
            Me.lbl_batch_count.Location = New System.Drawing.Point(230, 107)
            Me.lbl_batch_count.Name = "lbl_batch_count"
            Me.lbl_batch_count.Size = New System.Drawing.Size(50, 13)
            Me.lbl_batch_count.TabIndex = 15
            Me.lbl_batch_count.Text = "0"
            '
            'lbl_file_count
            '
            Me.lbl_file_count.Appearance.Options.UseTextOptions = True
            Me.lbl_file_count.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
            Me.lbl_file_count.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None
            Me.lbl_file_count.Location = New System.Drawing.Point(230, 126)
            Me.lbl_file_count.Name = "lbl_file_count"
            Me.lbl_file_count.Size = New System.Drawing.Size(50, 13)
            Me.lbl_file_count.TabIndex = 16
            Me.lbl_file_count.Text = "0"
            '
            'lbl_cdc
            '
            Me.lbl_cdc.Appearance.Options.UseTextOptions = True
            Me.lbl_cdc.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
            Me.lbl_cdc.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None
            Me.lbl_cdc.Location = New System.Drawing.Point(230, 145)
            Me.lbl_cdc.Name = "lbl_cdc"
            Me.lbl_cdc.Size = New System.Drawing.Size(50, 13)
            Me.lbl_cdc.TabIndex = 17
            Me.lbl_cdc.Text = "0"
            '
            'MarqueeProgressBarControl1
            '
            Me.MarqueeProgressBarControl1.EditValue = "Initializing for Processing"
            Me.MarqueeProgressBarControl1.Location = New System.Drawing.Point(12, 190)
            Me.MarqueeProgressBarControl1.Name = "MarqueeProgressBarControl1"
            Me.MarqueeProgressBarControl1.Size = New System.Drawing.Size(268, 23)
            Me.MarqueeProgressBarControl1.TabIndex = 18
            '
            'lbl_ADV_count
            '
            Me.lbl_ADV_count.Appearance.Options.UseTextOptions = True
            Me.lbl_ADV_count.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
            Me.lbl_ADV_count.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None
            Me.lbl_ADV_count.Location = New System.Drawing.Point(230, 164)
            Me.lbl_ADV_count.Name = "lbl_ADV_count"
            Me.lbl_ADV_count.Size = New System.Drawing.Size(50, 13)
            Me.lbl_ADV_count.TabIndex = 20
            Me.lbl_ADV_count.Text = "0"
            '
            'lbl_ADV_text
            '
            Me.lbl_ADV_text.Location = New System.Drawing.Point(12, 164)
            Me.lbl_ADV_text.Name = "lbl_ADV_text"
            Me.lbl_ADV_text.Size = New System.Drawing.Size(84, 13)
            Me.lbl_ADV_text.TabIndex = 19
            Me.lbl_ADV_text.Text = "Advisory Records"
            '
            'Status_Form
            '
            Me.ToolTipController1.SetAllowHtmlText(Me, DevExpress.Utils.DefaultBoolean.[False])
            Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
            Me.ClientSize = New System.Drawing.Size(292, 273)
            Me.Controls.Add(Me.lbl_ADV_count)
            Me.Controls.Add(Me.lbl_ADV_text)
            Me.Controls.Add(Me.MarqueeProgressBarControl1)
            Me.Controls.Add(Me.lbl_cdc)
            Me.Controls.Add(Me.lbl_file_count)
            Me.Controls.Add(Me.lbl_batch_count)
            Me.Controls.Add(Me.lbl_cdv)
            Me.Controls.Add(Me.lbl_cdr)
            Me.Controls.Add(Me.lbl_cda)
            Me.Controls.Add(Me.lbl_cie)
            Me.Controls.Add(Me.lbl_cdm)
            Me.Controls.Add(Me.lbl_cdc_text)
            Me.Controls.Add(Me.lbl_file_count_text)
            Me.Controls.Add(Me.lbl_batch_count_text)
            Me.Controls.Add(Me.lbl_cdv_text)
            Me.Controls.Add(Me.lbl_cdr_text)
            Me.Controls.Add(Me.lbl_cda_text)
            Me.Controls.Add(Me.lbl_cie_text)
            Me.Controls.Add(Me.lbl_cdm_text)
            Me.Controls.Add(Me.Button_Print)
            Me.Controls.Add(Me.Button_OK)
            Me.MaximizeBox = False
            Me.MaximumSize = New System.Drawing.Size(300, 300)
            Me.MinimumSize = New System.Drawing.Size(300, 300)
            Me.Name = "Status_Form"
            Me.ToolTipController1.SetSuperTip(Me, Nothing)
            Me.Text = "RPPS Response File Processing"
            CType(Me.DxErrorProvider1, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.MarqueeProgressBarControl1.Properties, System.ComponentModel.ISupportInitialize).EndInit()
            Me.ResumeLayout(False)
            Me.PerformLayout()

        End Sub
#End Region

#Region " Counters "

        ''' <summary>
        ''' Count of CDD records
        ''' </summary>
        Dim _CIE As Int32
        Private Sub delegated_CIE()
            If InvokeRequired Then
                Invoke(New MethodInvoker(AddressOf delegated_CIE))
            Else
                lbl_cie.Text = String.Format("{0:n0}", CIE)
            End If
        End Sub

        Public Property CIE() As Int32
            Get
                Return _CIE
            End Get
            Set(ByVal value As Int32)
                _CIE = value
                delegated_CIE()
            End Set
        End Property

        ''' <summary>
        ''' Count of CDD records
        ''' </summary>
        Dim _CDD As Int32
        Private Sub delegated_CDD()
        End Sub

        Public Property CDD() As Int32
            Get
                Return _CDD
            End Get
            Set(ByVal value As Int32)
                _CDD = value
                delegated_CDD()
            End Set
        End Property

        ''' <summary>
        ''' Count of CDT records
        ''' </summary>
        Dim _CDT As Int32 = 0
        Private Sub delegated_CDT()
        End Sub

        Public Property CDT() As Int32
            Get
                Return _CDT
            End Get
            Set(ByVal value As Int32)
                _CDT = value
                delegated_CDT()
            End Set
        End Property

        ''' <summary>
        ''' Count of CDR records
        ''' </summary>
        Dim _CDR As Int32
        Private Sub delegated_CDR()
            If InvokeRequired Then
                Invoke(New MethodInvoker(AddressOf delegated_CDR))
            Else
                lbl_cdr.Text = String.Format("{0:n0}", CDR)
            End If
        End Sub

        Public Property CDR() As Int32
            Get
                Return _CDR
            End Get
            Set(ByVal value As Int32)
                _CDR = value
                delegated_CDR()
            End Set
        End Property

        ''' <summary>
        ''' Count of CDA records
        ''' </summary>
        Dim _CDA As Int32
        Private Sub delegated_CDA()
            If InvokeRequired Then
                Invoke(New MethodInvoker(AddressOf delegated_CDA))
            Else
                lbl_cda.Text = String.Format("{0:n0}", CDA)
            End If
        End Sub

        Public Property CDA() As Int32
            Get
                Return _CDA
            End Get
            Set(ByVal value As Int32)
                _CDA = value
                delegated_CDA()
            End Set
        End Property

        ''' <summary>
        ''' Count of CDV records
        ''' </summary>
        Dim _CDV As Int32
        Private Sub delegated_CDV()
            If InvokeRequired Then
                Invoke(New MethodInvoker(AddressOf delegated_CDV))
            Else
                lbl_cdv.Text = String.Format("{0:n0}", CDV)
            End If
        End Sub

        Public Property CDV() As Int32
            Get
                Return _CDV
            End Get
            Set(ByVal value As Int32)
                _CDV = value
                delegated_CDV()
            End Set
        End Property

        ''' <summary>
        ''' Count of CDM records
        ''' </summary>
        Dim _CDM As Int32
        Private Sub delegated_CDM()
            If InvokeRequired Then
                Invoke(New MethodInvoker(AddressOf delegated_CDM))
            Else
                lbl_cdm.Text = String.Format("{0:n0}", CDM)
            End If
        End Sub

        Public Property CDM() As Int32
            Get
                Return _CDM
            End Get
            Set(ByVal value As Int32)
                _CDM = value
                delegated_CDM()
            End Set
        End Property

        ''' <summary>
        ''' Count of CDC records
        ''' </summary>
        Dim _CDC As Int32
        Private Sub delegated_CDC()
            If InvokeRequired Then
                Invoke(New MethodInvoker(AddressOf delegated_CDC))
            Else
                lbl_cdc.Text = String.Format("{0:n0}", _CDC)
            End If
        End Sub

        Public Property CDC() As Int32
            Get
                Return _CDC
            End Get
            Set(ByVal value As Int32)
                _CDC = value
                delegated_CDC()
            End Set
        End Property

        ''' <summary>
        ''' Count of proposals
        ''' </summary>
        Dim _CDP As Int32 = 0
        Private Sub delegated_CDP()
        End Sub

        Public Property CDP() As Int32
            Get
                Return _CDP
            End Get
            Set(ByVal value As Int32)
                _CDP = value
                delegated_CDP()
            End Set
        End Property

        ''' <summary>
        ''' Batch Count
        ''' </summary>
        Dim _batch_count As Int32 = 0
        Private Sub delegated_batch_count()
            If InvokeRequired Then
                Invoke(New MethodInvoker(AddressOf delegated_batch_count))
            Else
                lbl_batch_count.Text = String.Format("{0:n0}", _batch_count)
            End If
        End Sub

        Public Property batch_count() As Int32
            Get
                Return _batch_count
            End Get
            Set(ByVal value As Int32)
                _batch_count = value
                delegated_batch_count()
            End Set
        End Property

        ''' <summary>
        ''' File Count
        ''' </summary>
        Dim _file_count As Int32
        Private Sub delegated_file_count()
            If InvokeRequired Then
                Invoke(New MethodInvoker(AddressOf delegated_file_count))
            Else
                lbl_file_count.Text = String.Format("{0:n0}", _file_count)
            End If
        End Sub

        Public Property file_count() As Int32
            Get
                Return _file_count
            End Get
            Set(ByVal value As Int32)
                _file_count = value
                delegated_file_count()
            End Set
        End Property

        ''' <summary>
        ''' Advisorary Count
        ''' </summary>
        Dim _ADV As Int32
        Private Sub delegated_ADV()
            If InvokeRequired Then
                Invoke(New MethodInvoker(AddressOf delegated_ADV))
            Else
                lbl_ADV_count.Text = String.Format("{0:n0}", _ADV)
            End If
        End Sub

        Public Property ADV() As Int32
            Get
                Return _ADV
            End Get
            Set(ByVal value As Int32)
                _ADV = value
                delegated_ADV()
            End Set
        End Property
#End Region

#Region " Form Processing "

        ''' <summary>
        ''' Process the form update logic once the form is displayed
        ''' </summary>
        Private Sub Status_Form_Load(ByVal sender As Object, ByVal e As EventArgs)
            AddHandler Activated, AddressOf Form1_Activated

            With MarqueeProgressBarControl1
                .Properties.Stopped = True
                .Text = "INITIALIZING FOR PROCESSING"
                .Properties.ShowTitle = True
            End With
        End Sub

        ''' <summary>
        ''' Process the form update logic once the form is displayed
        ''' </summary>
        Private Sub Form1_Activated(ByVal sender As Object, ByVal e As EventArgs)
            Dim Enabled As Boolean = True

            ' Do this logic only on the first time that we are activated
            RemoveHandler Activated, AddressOf Form1_Activated

            ' Ask for the response batch to use
            If ap.rpps_response_file <= 0 Then
                Using frm As New BatchForm()
                    With frm
                        .Owner = Me
                        If .ShowDialog() = DialogResult.OK Then ap.rpps_response_file = .rpps_response_file
                    End With
                End Using

                If ap.rpps_response_file <= 0 Then Enabled = False
            End If

            ' Sort all the pathnames in the list
            If Enabled AndAlso ap.pathnames.Count = 0 Then
                Dim answer As DialogResult
                Using dlg As New OpenFileDialog
                    With dlg
                        AddHandler .FileOk, AddressOf dlg_FileOk

                        .Title = "Open RPPS Input response file"
                        .ShowReadOnly = False
                        .ShowHelp = False
                        .RestoreDirectory = True
                        .Multiselect = True
                        .InitialDirectory = Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments)
                        .DereferenceLinks = True
                        .CheckPathExists = True
                        .CheckFileExists = True
                        .Filter = "All Files (*.*)|*.*"
                        answer = .ShowDialog()

                        ' Add the filenames to the list
                        If answer = Windows.Forms.DialogResult.OK Then
                            For Each f As String In .FileNames()
                                ap.pathnames.Add(f)
                            Next
                        End If
                    End With
                End Using
            End If

            ' If there are no names then return an error. Otherwise, sort them.
            If Enabled AndAlso ap.pathnames.Count = 0 Then Enabled = False

            ' If the form is not valid then close it. This will kill the application.
            If Not Enabled Then
                Close()
            Else
                ap.pathnames.Sort(0, ap.pathnames.Count, Nothing)

                ' Change the message text to indicate "CANCEL"
                Button_OK.Text = "&Cancel"
                Button_Print.Enabled = False

                ' Show the marquee
                With MarqueeProgressBarControl1
                    .Properties.Stopped = False
                    .Properties.ShowTitle = False
                End With

                ' Start the background thread to process the file(s).
                With bt
                    .WorkerSupportsCancellation = True
                    .WorkerReportsProgress = False
                    .RunWorkerAsync()
                End With
            End If
        End Sub

        ''' <summary>
        ''' Validation routine for the file open dialog
        ''' </summary>
        Private Sub dlg_FileOk(ByVal sender As Object, ByVal e As System.ComponentModel.CancelEventArgs)
            Dim Canceled As Boolean = True

            ' Process the selected file names
            For Each fname As String In CType(sender, OpenFileDialog).FileNames
                Canceled = True
                If fname Is Nothing Then Exit For

                ' Read the first part of the file. It should start with the string "101"
                Try
                    Dim FirstPart As String = String.Empty
                    Dim fs As New System.IO.StreamReader(New System.IO.FileStream(fname, System.IO.FileMode.Open, System.IO.FileAccess.Read, System.IO.FileShare.Read, 4096))
                    FirstPart = fs.ReadLine()
                    fs.Close()

                    ' Look for a valid response file
                    If FirstPart.Length > 80 AndAlso FirstPart.StartsWith("101") Then Canceled = False

                Catch ex As System.IO.FileNotFoundException
                Catch ex As System.IO.DirectoryNotFoundException
                Catch ex As System.IO.EndOfStreamException
                    ' These are ignored at this point

                End Try

                ' If the entry is not valid, set the cancel status
                If Canceled Then Exit For
            Next

            e.Cancel = Canceled
        End Sub

        ''' <summary>
        ''' Complete processing for the thread
        ''' </summary>
        Private Sub bt_RunWorkerCompleted(ByVal sender As Object, ByVal e As System.ComponentModel.RunWorkerCompletedEventArgs)

            ' Change the message text to indicate "OK"
            Button_OK.Text = "&OK"
            Button_Print.Enabled = True

            ' Stop the marque from being displayed
            With MarqueeProgressBarControl1
                .Properties.Stopped = True
                .Text = "OPERATION COMPLETED"
                .Properties.ShowTitle = True
            End With
        End Sub

        ''' <summary>
        ''' Process the cancel/ok button
        ''' </summary>
        Private Sub cmdOK_Click(ByVal eventSender As Object, ByVal eventArgs As EventArgs)

            ' Closing the main form will terminate the background thread and the program.
            ' There is no need to "CANCEL" the thread since it is stopped automatically when we stop.
            Close()
        End Sub
#End Region

#Region " Thread Processing "

        ''' <summary>
        ''' Handle the thread procedure
        ''' </summary>
        Private Sub bt_DoWork(ByVal sender As Object, ByVal e As System.ComponentModel.DoWorkEventArgs)
            Try
                ' Do the application here
                Dim ie As IEnumerator = ap.GetPathnameEnumerator
                Do While ie.MoveNext
                    With New InputProcessing(ap.rpps_response_file)
                        .ParseFile(Convert.ToString(ie.Current))
                    End With
                Loop

                ' If we have something to process then post the changes to the database
                PostChanges()

            Catch ex As System.Threading.ThreadAbortException
                DebtPlus.Svc.Common.ErrorHandling.HandleErrors(ex)
            End Try
        End Sub
#End Region

#Region " Parsing routines "

        ''' <summary>
        ''' Decode the numerical value of a fixed point number string
        ''' </summary>
        Friend Shared Function IntegerValue(ByVal input_string As String) As Int32
            Dim answer As Int32 = 0

            If Not Int32.TryParse(input_string, Globalization.NumberStyles.Integer, System.Globalization.CultureInfo.InvariantCulture, answer) OrElse answer < 0 Then answer = 0

            Return answer
        End Function

        Friend Shared Function IntegerValue(ByVal input_string As String, ByVal StartingIndex As Int32, ByVal Length As Int32) As Int32
            Return IntegerValue(input_string.Substring(StartingIndex, Length))
        End Function

        ''' <summary>
        ''' Retrieve the monitary value for the column
        ''' </summary>
        Friend Shared Function MoneyValue(ByVal input_string As String) As Decimal
            Dim answer As Decimal = 0D

            If Not Decimal.TryParse(input_string, Globalization.NumberStyles.Currency, System.Globalization.CultureInfo.InvariantCulture, answer) OrElse answer < 0D Then answer = 0D
            answer = Math.Truncate(answer / 100D, 2)

            Return answer
        End Function

        Friend Shared Function MoneyValue(ByVal input_string As String, ByVal StartingIndex As Int32, ByVal Length As Int32) As Decimal
            Return MoneyValue(input_string.Substring(StartingIndex, Length))
        End Function
#End Region

#Region " Print Form "

        ''' <summary>
        ''' Print the form on the default printer
        ''' </summary>
        Private printFont As Font
        Private Sub printButton_Click(ByVal sender As Object, ByVal e As EventArgs)
            Try
                printFont = New Font("Arial", 10)
                Dim pd As New System.Drawing.Printing.PrintDocument()
                AddHandler pd.PrintPage, AddressOf pd_PrintPage

#If 0 Then ' Direct to printer
            pd.Print()
#Else      ' Use preview form first
                Dim PreviewDialog As New PrintPreviewDialog()
                With PreviewDialog
                    .ShowIcon = False
                    .Document = pd
                    .ShowDialog(Me)
                End With
#End If

            Catch ex As Exception
                DebtPlus.Data.Forms.MessageBox.Show(ex.Message, "Error printing document")
            End Try
        End Sub

        ''' <summary>
        ''' Event handler to put data on the page for the printer
        ''' </summary>
        Private Sub pd_PrintPage(ByVal sender As Object, ByVal ev As System.Drawing.Printing.PrintPageEventArgs)
            Dim leftMargin As Single = ev.MarginBounds.Left
            Dim topMargin As Single = ev.MarginBounds.Top
            Dim yPos As Single = topMargin
            Dim fmt As New StringFormat
            Dim LineNo As Int32 = 0

            ' Generate the font for the header
            Dim HdrFont As New Font(printFont.FontFamily, 20, FontStyle.Bold, GraphicsUnit.Point, CByte(0))
            fmt.Alignment = StringAlignment.Near
            ev.Graphics.DrawString(Text, HdrFont, Brushes.Black, leftMargin + 0, yPos, fmt)
            yPos += (HdrFont.GetHeight(ev.Graphics) * 2)
            HdrFont.Dispose()

            ' Print each line of the file.
            ev.HasMorePages = True
            While ev.HasMorePages

                Dim TitleString As String = String.Empty
                Dim ValueString As String = String.Empty
                Select Case LineNo
                    Case 0
                        TitleString = lbl_cdm_text.Text : ValueString = lbl_cdm.Text
                    Case 1
                        TitleString = lbl_cie_text.Text : ValueString = lbl_cie.Text
                    Case 2
                        TitleString = lbl_cda_text.Text : ValueString = lbl_cda.Text
                    Case 3
                        TitleString = lbl_cdr_text.Text : ValueString = lbl_cdr.Text
                    Case 4
                        TitleString = lbl_cdv_text.Text : ValueString = lbl_cdv.Text
                    Case 5
                        TitleString = lbl_batch_count_text.Text : ValueString = lbl_batch_count.Text
                    Case 6
                        TitleString = lbl_file_count_text.Text : ValueString = lbl_file_count.Text
                    Case 7
                        TitleString = lbl_cdc_text.Text : ValueString = lbl_cdc.Text
                    Case 8
                        TitleString = lbl_ADV_text.Text : ValueString = lbl_ADV_count.Text
                        ev.HasMorePages = False
                    Case Else
                End Select

                ' Generate the position on the form and print the line
                fmt.Alignment = StringAlignment.Near
                ev.Graphics.DrawString(TitleString, printFont, Brushes.Black, leftMargin + 0, yPos, fmt)
                fmt.Alignment = StringAlignment.Far
                ev.Graphics.DrawString(ValueString, printFont, Brushes.Black, leftMargin + 300, yPos, fmt)
                yPos += printFont.GetHeight(ev.Graphics)

                LineNo += 1
            End While
        End Sub
#End Region

        Protected Friend Shared privateInstance As Status_Form
    End Class
End Namespace
