#Region "Copyright 2000-2012 DebtPlus, L.L.C."
'{*******************************************************************}
'{                                                                   }
'{       DebtPlus Debt Management System                             }
'{                                                                   }
'{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
'{       ALL RIGHTS RESERVED                                         }
'{                                                                   }
'{   The entire contents of this file is protected by U.S. and       }
'{   International Copyright Laws. Unauthorized reproduction,        }
'{   reverse-engineering, and distribution of all or any portion of  }
'{   the code contained in this file is strictly prohibited and may  }
'{   result in severe civil and criminal penalties and will be       }
'{   prosecuted to the maximum extent possible under the law.        }
'{                                                                   }
'{   RESTRICTIONS                                                    }
'{                                                                   }
'{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
'{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
'{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
'{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
'{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
'{                                                                   }
'{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
'{   ADDITIONAL RESTRICTIONS.                                        }
'{                                                                   }
'{*******************************************************************}
#End Region

Option Compare Binary
Option Explicit On 
Option Strict On

Imports System.Data.SqlClient
Imports DebtPlus.Reports
Imports DebtPlus.Interfaces.Reports
Imports System.Windows.Forms
Imports System.Drawing

Namespace Intake.Import
    Friend Class MainForm
        Inherits DebtPlus.Data.Forms.DebtPlusForm

        Public Sub New()
            MyBase.New()
            InitializeComponent()

            AddHandler Load, AddressOf MainForm_Load
            AddHandler Closing, AddressOf MainForm_Closing
            AddHandler Button_Edit.Click, AddressOf Button_Edit_Click
            AddHandler Button_Cancel.Click, AddressOf Button_Cancel_Click
            AddHandler GridView1.FocusedRowChanged, AddressOf GridView1_FocusedRowChanged
            AddHandler GridView1.DoubleClick, AddressOf GridView1_DoubleClick
            AddHandler GridView1.MouseDown, AddressOf GridView1_MouseDown
            AddHandler Button_Delete.Click, AddressOf Button_Delete_Click
            AddHandler BarButtonItem2.ItemClick, AddressOf BarButtonItem2_ItemClick
            AddHandler BarButtonItem1.ItemClick, AddressOf BarButtonItem1_ItemClick
            AddHandler BarButtonItem3.ItemClick, AddressOf BarButtonItem3_ItemClick
        End Sub

        Private intake_client As Int32 = -1
        Dim ControlRow As Int32 = -1

#Region " Windows Form Designer generated code "

        'Form overrides dispose to clean up the component list.
        Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
            If disposing Then
                If components IsNot Nothing Then
                    components.Dispose()
                End If
            End If
            MyBase.Dispose(disposing)
        End Sub

        'Required by the Windows Form Designer
        Private components As System.ComponentModel.IContainer

        'NOTE: The following procedure is required by the Windows Form Designer
        'It can be modified using the Windows Form Designer.  
        'Do not modify it using the code editor.
        Friend WithEvents GridControl1 As DevExpress.XtraGrid.GridControl
        Friend WithEvents GridView1 As DevExpress.XtraGrid.Views.Grid.GridView
        Friend WithEvents GridColumn_ID As DevExpress.XtraGrid.Columns.GridColumn
        Friend WithEvents GridColumn_DateCreated As DevExpress.XtraGrid.Columns.GridColumn
        Friend WithEvents GridColumn_HomePh As DevExpress.XtraGrid.Columns.GridColumn
        Friend WithEvents GridColumn_Status As DevExpress.XtraGrid.Columns.GridColumn
        Friend WithEvents GridColumn_Bankruptcy As DevExpress.XtraGrid.Columns.GridColumn
        Friend WithEvents Button_Edit As DevExpress.XtraEditors.SimpleButton
        Friend WithEvents Button_Delete As DevExpress.XtraEditors.SimpleButton
        Friend WithEvents Button_Cancel As DevExpress.XtraEditors.SimpleButton
        <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
            Me.components = New System.ComponentModel.Container()
            Dim StyleFormatCondition1 As DevExpress.XtraGrid.StyleFormatCondition = New DevExpress.XtraGrid.StyleFormatCondition()
            Me.GridColumn_Status = New DevExpress.XtraGrid.Columns.GridColumn()
            Me.GridControl1 = New DevExpress.XtraGrid.GridControl()
            Me.GridView1 = New DevExpress.XtraGrid.Views.Grid.GridView()
            Me.GridColumn_ID = New DevExpress.XtraGrid.Columns.GridColumn()
            Me.GridColumn_DateCreated = New DevExpress.XtraGrid.Columns.GridColumn()
            Me.GridColumn_HomePh = New DevExpress.XtraGrid.Columns.GridColumn()
            Me.GridColumn_Bankruptcy = New DevExpress.XtraGrid.Columns.GridColumn()
            Me.Button_Edit = New DevExpress.XtraEditors.SimpleButton()
            Me.LayoutControl1 = New DevExpress.XtraLayout.LayoutControl()
            Me.Button_Cancel = New DevExpress.XtraEditors.SimpleButton()
            Me.Button_Delete = New DevExpress.XtraEditors.SimpleButton()
            Me.LayoutControlGroup1 = New DevExpress.XtraLayout.LayoutControlGroup()
            Me.LayoutControlItem1 = New DevExpress.XtraLayout.LayoutControlItem()
            Me.LayoutControlItem3 = New DevExpress.XtraLayout.LayoutControlItem()
            Me.LayoutControlItem4 = New DevExpress.XtraLayout.LayoutControlItem()
            Me.LayoutControlItem2 = New DevExpress.XtraLayout.LayoutControlItem()
            Me.EmptySpaceItem1 = New DevExpress.XtraLayout.EmptySpaceItem()
            Me.EmptySpaceItem2 = New DevExpress.XtraLayout.EmptySpaceItem()
            Me.barManager1 = New DevExpress.XtraBars.BarManager(Me.components)
            Me.Bar2 = New DevExpress.XtraBars.Bar()
            Me.BarSubItem1 = New DevExpress.XtraBars.BarSubItem()
            Me.BarButtonItem1 = New DevExpress.XtraBars.BarButtonItem()
            Me.BarSubItem2 = New DevExpress.XtraBars.BarSubItem()
            Me.BarButtonItem2 = New DevExpress.XtraBars.BarButtonItem()
            Me.BarButtonItem3 = New DevExpress.XtraBars.BarButtonItem()
            Me.barDockControlTop = New DevExpress.XtraBars.BarDockControl()
            Me.barDockControlBottom = New DevExpress.XtraBars.BarDockControl()
            Me.barDockControlLeft = New DevExpress.XtraBars.BarDockControl()
            Me.barDockControlRight = New DevExpress.XtraBars.BarDockControl()
            CType(Me.DxErrorProvider1, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.GridControl1, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.GridView1, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.LayoutControl1, System.ComponentModel.ISupportInitialize).BeginInit()
            Me.LayoutControl1.SuspendLayout()
            CType(Me.LayoutControlGroup1, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.LayoutControlItem1, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.LayoutControlItem3, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.LayoutControlItem4, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.LayoutControlItem2, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.EmptySpaceItem1, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.EmptySpaceItem2, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.barManager1, System.ComponentModel.ISupportInitialize).BeginInit()
            Me.SuspendLayout()
            '
            'GridColumn_Status
            '
            Me.GridColumn_Status.AppearanceCell.Options.UseTextOptions = True
            Me.GridColumn_Status.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
            Me.GridColumn_Status.AppearanceHeader.Options.UseTextOptions = True
            Me.GridColumn_Status.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
            Me.GridColumn_Status.Caption = "Status"
            Me.GridColumn_Status.CustomizationCaption = "Completion Status"
            Me.GridColumn_Status.FieldName = "status"
            Me.GridColumn_Status.Name = "GridColumn_Status"
            Me.GridColumn_Status.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.[True]
            Me.GridColumn_Status.Visible = True
            Me.GridColumn_Status.VisibleIndex = 3
            Me.GridColumn_Status.Width = 81
            '
            'GridControl1
            '
            Me.GridControl1.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                Or System.Windows.Forms.AnchorStyles.Left) _
                Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
            Me.GridControl1.Location = New System.Drawing.Point(12, 12)
            Me.GridControl1.MainView = Me.GridView1
            Me.GridControl1.Name = "GridControl1"
            Me.GridControl1.Size = New System.Drawing.Size(385, 247)
            Me.GridControl1.TabIndex = 0
            Me.GridControl1.ViewCollection.AddRange(New DevExpress.XtraGrid.Views.Base.BaseView() {Me.GridView1})
            '
            'GridView1
            '
            Me.GridView1.Appearance.ColumnFilterButton.BackColor = System.Drawing.Color.FromArgb(CType(CType(104, Byte), Int32), CType(CType(184, Byte), Int32), CType(CType(251, Byte), Int32))
            Me.GridView1.Appearance.ColumnFilterButton.BorderColor = System.Drawing.Color.FromArgb(CType(CType(104, Byte), Int32), CType(CType(184, Byte), Int32), CType(CType(251, Byte), Int32))
            Me.GridView1.Appearance.ColumnFilterButton.ForeColor = System.Drawing.Color.White
            Me.GridView1.Appearance.ColumnFilterButton.Options.UseBackColor = True
            Me.GridView1.Appearance.ColumnFilterButton.Options.UseBorderColor = True
            Me.GridView1.Appearance.ColumnFilterButton.Options.UseForeColor = True
            Me.GridView1.Appearance.ColumnFilterButtonActive.BackColor = System.Drawing.Color.FromArgb(CType(CType(170, Byte), Int32), CType(CType(216, Byte), Int32), CType(CType(254, Byte), Int32))
            Me.GridView1.Appearance.ColumnFilterButtonActive.BorderColor = System.Drawing.Color.FromArgb(CType(CType(170, Byte), Int32), CType(CType(216, Byte), Int32), CType(CType(254, Byte), Int32))
            Me.GridView1.Appearance.ColumnFilterButtonActive.ForeColor = System.Drawing.Color.Black
            Me.GridView1.Appearance.ColumnFilterButtonActive.Options.UseBackColor = True
            Me.GridView1.Appearance.ColumnFilterButtonActive.Options.UseBorderColor = True
            Me.GridView1.Appearance.ColumnFilterButtonActive.Options.UseForeColor = True
            Me.GridView1.Appearance.Empty.BackColor = System.Drawing.Color.FromArgb(CType(CType(236, Byte), Int32), CType(CType(246, Byte), Int32), CType(CType(255, Byte), Int32))
            Me.GridView1.Appearance.Empty.BackColor2 = System.Drawing.Color.White
            Me.GridView1.Appearance.Empty.Options.UseBackColor = True
            Me.GridView1.Appearance.EvenRow.BackColor = System.Drawing.Color.FromArgb(CType(CType(247, Byte), Int32), CType(CType(251, Byte), Int32), CType(CType(255, Byte), Int32))
            Me.GridView1.Appearance.EvenRow.BorderColor = System.Drawing.Color.FromArgb(CType(CType(247, Byte), Int32), CType(CType(251, Byte), Int32), CType(CType(255, Byte), Int32))
            Me.GridView1.Appearance.EvenRow.ForeColor = System.Drawing.Color.Black
            Me.GridView1.Appearance.EvenRow.Options.UseBackColor = True
            Me.GridView1.Appearance.EvenRow.Options.UseBorderColor = True
            Me.GridView1.Appearance.EvenRow.Options.UseForeColor = True
            Me.GridView1.Appearance.FilterCloseButton.BackColor = System.Drawing.Color.FromArgb(CType(CType(104, Byte), Int32), CType(CType(184, Byte), Int32), CType(CType(251, Byte), Int32))
            Me.GridView1.Appearance.FilterCloseButton.BorderColor = System.Drawing.Color.FromArgb(CType(CType(104, Byte), Int32), CType(CType(184, Byte), Int32), CType(CType(251, Byte), Int32))
            Me.GridView1.Appearance.FilterCloseButton.ForeColor = System.Drawing.Color.White
            Me.GridView1.Appearance.FilterCloseButton.Options.UseBackColor = True
            Me.GridView1.Appearance.FilterCloseButton.Options.UseBorderColor = True
            Me.GridView1.Appearance.FilterCloseButton.Options.UseForeColor = True
            Me.GridView1.Appearance.FilterPanel.BackColor = System.Drawing.Color.FromArgb(CType(CType(236, Byte), Int32), CType(CType(246, Byte), Int32), CType(CType(255, Byte), Int32))
            Me.GridView1.Appearance.FilterPanel.BackColor2 = System.Drawing.Color.White
            Me.GridView1.Appearance.FilterPanel.ForeColor = System.Drawing.Color.Black
            Me.GridView1.Appearance.FilterPanel.Options.UseBackColor = True
            Me.GridView1.Appearance.FilterPanel.Options.UseForeColor = True
            Me.GridView1.Appearance.FixedLine.BackColor = System.Drawing.Color.FromArgb(CType(CType(59, Byte), Int32), CType(CType(133, Byte), Int32), CType(CType(195, Byte), Int32))
            Me.GridView1.Appearance.FixedLine.Options.UseBackColor = True
            Me.GridView1.Appearance.FocusedCell.BackColor = System.Drawing.Color.White
            Me.GridView1.Appearance.FocusedCell.ForeColor = System.Drawing.Color.Black
            Me.GridView1.Appearance.FocusedCell.Options.UseBackColor = True
            Me.GridView1.Appearance.FocusedCell.Options.UseForeColor = True
            Me.GridView1.Appearance.FocusedRow.BackColor = System.Drawing.Color.FromArgb(CType(CType(38, Byte), Int32), CType(CType(109, Byte), Int32), CType(CType(189, Byte), Int32))
            Me.GridView1.Appearance.FocusedRow.BorderColor = System.Drawing.Color.FromArgb(CType(CType(59, Byte), Int32), CType(CType(139, Byte), Int32), CType(CType(206, Byte), Int32))
            Me.GridView1.Appearance.FocusedRow.ForeColor = System.Drawing.Color.White
            Me.GridView1.Appearance.FocusedRow.Options.UseBackColor = True
            Me.GridView1.Appearance.FocusedRow.Options.UseBorderColor = True
            Me.GridView1.Appearance.FocusedRow.Options.UseForeColor = True
            Me.GridView1.Appearance.FooterPanel.BackColor = System.Drawing.Color.FromArgb(CType(CType(104, Byte), Int32), CType(CType(184, Byte), Int32), CType(CType(251, Byte), Int32))
            Me.GridView1.Appearance.FooterPanel.BorderColor = System.Drawing.Color.FromArgb(CType(CType(104, Byte), Int32), CType(CType(184, Byte), Int32), CType(CType(251, Byte), Int32))
            Me.GridView1.Appearance.FooterPanel.ForeColor = System.Drawing.Color.Black
            Me.GridView1.Appearance.FooterPanel.Options.UseBackColor = True
            Me.GridView1.Appearance.FooterPanel.Options.UseBorderColor = True
            Me.GridView1.Appearance.FooterPanel.Options.UseForeColor = True
            Me.GridView1.Appearance.GroupButton.BackColor = System.Drawing.Color.FromArgb(CType(CType(104, Byte), Int32), CType(CType(184, Byte), Int32), CType(CType(251, Byte), Int32))
            Me.GridView1.Appearance.GroupButton.BorderColor = System.Drawing.Color.FromArgb(CType(CType(104, Byte), Int32), CType(CType(184, Byte), Int32), CType(CType(251, Byte), Int32))
            Me.GridView1.Appearance.GroupButton.Options.UseBackColor = True
            Me.GridView1.Appearance.GroupButton.Options.UseBorderColor = True
            Me.GridView1.Appearance.GroupFooter.BackColor = System.Drawing.Color.FromArgb(CType(CType(170, Byte), Int32), CType(CType(216, Byte), Int32), CType(CType(254, Byte), Int32))
            Me.GridView1.Appearance.GroupFooter.BorderColor = System.Drawing.Color.FromArgb(CType(CType(170, Byte), Int32), CType(CType(216, Byte), Int32), CType(CType(254, Byte), Int32))
            Me.GridView1.Appearance.GroupFooter.ForeColor = System.Drawing.Color.Black
            Me.GridView1.Appearance.GroupFooter.Options.UseBackColor = True
            Me.GridView1.Appearance.GroupFooter.Options.UseBorderColor = True
            Me.GridView1.Appearance.GroupFooter.Options.UseForeColor = True
            Me.GridView1.Appearance.GroupPanel.BackColor = System.Drawing.Color.FromArgb(CType(CType(236, Byte), Int32), CType(CType(246, Byte), Int32), CType(CType(255, Byte), Int32))
            Me.GridView1.Appearance.GroupPanel.BackColor2 = System.Drawing.Color.White
            Me.GridView1.Appearance.GroupPanel.ForeColor = System.Drawing.Color.Black
            Me.GridView1.Appearance.GroupPanel.Options.UseBackColor = True
            Me.GridView1.Appearance.GroupPanel.Options.UseForeColor = True
            Me.GridView1.Appearance.GroupRow.BackColor = System.Drawing.Color.FromArgb(CType(CType(170, Byte), Int32), CType(CType(216, Byte), Int32), CType(CType(254, Byte), Int32))
            Me.GridView1.Appearance.GroupRow.BorderColor = System.Drawing.Color.FromArgb(CType(CType(170, Byte), Int32), CType(CType(216, Byte), Int32), CType(CType(254, Byte), Int32))
            Me.GridView1.Appearance.GroupRow.ForeColor = System.Drawing.Color.Black
            Me.GridView1.Appearance.GroupRow.Options.UseBackColor = True
            Me.GridView1.Appearance.GroupRow.Options.UseBorderColor = True
            Me.GridView1.Appearance.GroupRow.Options.UseForeColor = True
            Me.GridView1.Appearance.HeaderPanel.BackColor = System.Drawing.Color.FromArgb(CType(CType(139, Byte), Int32), CType(CType(201, Byte), Int32), CType(CType(254, Byte), Int32))
            Me.GridView1.Appearance.HeaderPanel.BorderColor = System.Drawing.Color.FromArgb(CType(CType(139, Byte), Int32), CType(CType(201, Byte), Int32), CType(CType(254, Byte), Int32))
            Me.GridView1.Appearance.HeaderPanel.ForeColor = System.Drawing.Color.Black
            Me.GridView1.Appearance.HeaderPanel.Options.UseBackColor = True
            Me.GridView1.Appearance.HeaderPanel.Options.UseBorderColor = True
            Me.GridView1.Appearance.HeaderPanel.Options.UseForeColor = True
            Me.GridView1.Appearance.HideSelectionRow.BackColor = System.Drawing.Color.FromArgb(CType(CType(105, Byte), Int32), CType(CType(170, Byte), Int32), CType(CType(225, Byte), Int32))
            Me.GridView1.Appearance.HideSelectionRow.BorderColor = System.Drawing.Color.FromArgb(CType(CType(83, Byte), Int32), CType(CType(155, Byte), Int32), CType(CType(215, Byte), Int32))
            Me.GridView1.Appearance.HideSelectionRow.ForeColor = System.Drawing.Color.FromArgb(CType(CType(236, Byte), Int32), CType(CType(246, Byte), Int32), CType(CType(255, Byte), Int32))
            Me.GridView1.Appearance.HideSelectionRow.Options.UseBackColor = True
            Me.GridView1.Appearance.HideSelectionRow.Options.UseBorderColor = True
            Me.GridView1.Appearance.HideSelectionRow.Options.UseForeColor = True
            Me.GridView1.Appearance.HorzLine.BackColor = System.Drawing.Color.FromArgb(CType(CType(104, Byte), Int32), CType(CType(184, Byte), Int32), CType(CType(251, Byte), Int32))
            Me.GridView1.Appearance.HorzLine.Options.UseBackColor = True
            Me.GridView1.Appearance.OddRow.BackColor = System.Drawing.Color.FromArgb(CType(CType(236, Byte), Int32), CType(CType(246, Byte), Int32), CType(CType(255, Byte), Int32))
            Me.GridView1.Appearance.OddRow.BorderColor = System.Drawing.Color.FromArgb(CType(CType(236, Byte), Int32), CType(CType(246, Byte), Int32), CType(CType(255, Byte), Int32))
            Me.GridView1.Appearance.OddRow.ForeColor = System.Drawing.Color.Black
            Me.GridView1.Appearance.OddRow.Options.UseBackColor = True
            Me.GridView1.Appearance.OddRow.Options.UseBorderColor = True
            Me.GridView1.Appearance.OddRow.Options.UseForeColor = True
            Me.GridView1.Appearance.Preview.Font = New System.Drawing.Font("Verdana", 7.5!)
            Me.GridView1.Appearance.Preview.ForeColor = System.Drawing.Color.FromArgb(CType(CType(83, Byte), Int32), CType(CType(155, Byte), Int32), CType(CType(215, Byte), Int32))
            Me.GridView1.Appearance.Preview.Options.UseFont = True
            Me.GridView1.Appearance.Preview.Options.UseForeColor = True
            Me.GridView1.Appearance.Row.BackColor = System.Drawing.Color.FromArgb(CType(CType(247, Byte), Int32), CType(CType(251, Byte), Int32), CType(CType(255, Byte), Int32))
            Me.GridView1.Appearance.Row.ForeColor = System.Drawing.Color.Black
            Me.GridView1.Appearance.Row.Options.UseBackColor = True
            Me.GridView1.Appearance.Row.Options.UseForeColor = True
            Me.GridView1.Appearance.RowSeparator.BackColor = System.Drawing.Color.FromArgb(CType(CType(236, Byte), Int32), CType(CType(246, Byte), Int32), CType(CType(255, Byte), Int32))
            Me.GridView1.Appearance.RowSeparator.BackColor2 = System.Drawing.Color.White
            Me.GridView1.Appearance.RowSeparator.Options.UseBackColor = True
            Me.GridView1.Appearance.SelectedRow.BackColor = System.Drawing.Color.FromArgb(CType(CType(83, Byte), Int32), CType(CType(155, Byte), Int32), CType(CType(215, Byte), Int32))
            Me.GridView1.Appearance.SelectedRow.ForeColor = System.Drawing.Color.White
            Me.GridView1.Appearance.SelectedRow.Options.UseBackColor = True
            Me.GridView1.Appearance.SelectedRow.Options.UseForeColor = True
            Me.GridView1.Appearance.TopNewRow.BackColor = System.Drawing.Color.White
            Me.GridView1.Appearance.TopNewRow.Options.UseBackColor = True
            Me.GridView1.Appearance.VertLine.BackColor = System.Drawing.Color.FromArgb(CType(CType(104, Byte), Int32), CType(CType(184, Byte), Int32), CType(CType(251, Byte), Int32))
            Me.GridView1.Appearance.VertLine.Options.UseBackColor = True
            Me.GridView1.AppearancePrint.EvenRow.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Int32), CType(CType(255, Byte), Int32), CType(CType(192, Byte), Int32))
            Me.GridView1.AppearancePrint.EvenRow.BackColor2 = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Int32), CType(CType(255, Byte), Int32), CType(CType(192, Byte), Int32))
            Me.GridView1.AppearancePrint.EvenRow.Options.UseBackColor = True
            Me.GridView1.AppearancePrint.HeaderPanel.BackColor = System.Drawing.Color.Teal
            Me.GridView1.AppearancePrint.HeaderPanel.BackColor2 = System.Drawing.Color.Teal
            Me.GridView1.AppearancePrint.HeaderPanel.BorderColor = System.Drawing.Color.Teal
            Me.GridView1.AppearancePrint.HeaderPanel.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold)
            Me.GridView1.AppearancePrint.HeaderPanel.ForeColor = System.Drawing.Color.White
            Me.GridView1.AppearancePrint.HeaderPanel.Options.UseBackColor = True
            Me.GridView1.AppearancePrint.HeaderPanel.Options.UseBorderColor = True
            Me.GridView1.AppearancePrint.HeaderPanel.Options.UseFont = True
            Me.GridView1.AppearancePrint.HeaderPanel.Options.UseForeColor = True
            Me.GridView1.Columns.AddRange(New DevExpress.XtraGrid.Columns.GridColumn() {Me.GridColumn_ID, Me.GridColumn_DateCreated, Me.GridColumn_HomePh, Me.GridColumn_Status, Me.GridColumn_Bankruptcy})
            StyleFormatCondition1.Appearance.BackColor = System.Drawing.Color.FromArgb(CType(CType(192, Byte), Int32), CType(CType(255, Byte), Int32), CType(CType(192, Byte), Int32))
            StyleFormatCondition1.Appearance.BackColor2 = System.Drawing.Color.FromArgb(CType(CType(192, Byte), Int32), CType(CType(255, Byte), Int32), CType(CType(192, Byte), Int32))
            StyleFormatCondition1.Appearance.BorderColor = System.Drawing.Color.FromArgb(CType(CType(192, Byte), Int32), CType(CType(255, Byte), Int32), CType(CType(192, Byte), Int32))
            StyleFormatCondition1.Appearance.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold)
            StyleFormatCondition1.Appearance.Options.UseBackColor = True
            StyleFormatCondition1.Appearance.Options.UseBorderColor = True
            StyleFormatCondition1.Appearance.Options.UseFont = True
            StyleFormatCondition1.ApplyToRow = True
            StyleFormatCondition1.Column = Me.GridColumn_Status
            StyleFormatCondition1.Condition = DevExpress.XtraGrid.FormatConditionEnum.GreaterOrEqual
            StyleFormatCondition1.Value1 = "Comp"
            Me.GridView1.FormatConditions.AddRange(New DevExpress.XtraGrid.StyleFormatCondition() {StyleFormatCondition1})
            Me.GridView1.GridControl = Me.GridControl1
            Me.GridView1.Name = "GridView1"
            Me.GridView1.OptionsBehavior.Editable = False
            Me.GridView1.OptionsFilter.UseNewCustomFilterDialog = True
            Me.GridView1.OptionsPrint.EnableAppearanceEvenRow = True
            Me.GridView1.OptionsPrint.ExpandAllDetails = True
            Me.GridView1.OptionsPrint.PrintFooter = False
            Me.GridView1.OptionsPrint.PrintGroupFooter = False
            Me.GridView1.OptionsPrint.PrintHorzLines = False
            Me.GridView1.OptionsPrint.PrintVertLines = False
            Me.GridView1.OptionsPrint.UsePrintStyles = True
            Me.GridView1.OptionsView.ShowGroupPanel = False
            Me.GridView1.OptionsView.ShowIndicator = False
            Me.GridView1.SortInfo.AddRange(New DevExpress.XtraGrid.Columns.GridColumnSortInfo() {New DevExpress.XtraGrid.Columns.GridColumnSortInfo(Me.GridColumn_DateCreated, DevExpress.Data.ColumnSortOrder.Descending)})
            '
            'GridColumn_ID
            '
            Me.GridColumn_ID.AppearanceCell.Options.UseTextOptions = True
            Me.GridColumn_ID.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
            Me.GridColumn_ID.AppearanceHeader.Options.UseTextOptions = True
            Me.GridColumn_ID.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
            Me.GridColumn_ID.Caption = "ID"
            Me.GridColumn_ID.CustomizationCaption = "Intake ID"
            Me.GridColumn_ID.FieldName = "intake_client"
            Me.GridColumn_ID.Name = "GridColumn_ID"
            Me.GridColumn_ID.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.[True]
            Me.GridColumn_ID.Visible = True
            Me.GridColumn_ID.VisibleIndex = 0
            Me.GridColumn_ID.Width = 69
            '
            'GridColumn_DateCreated
            '
            Me.GridColumn_DateCreated.AppearanceCell.Options.UseTextOptions = True
            Me.GridColumn_DateCreated.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
            Me.GridColumn_DateCreated.AppearanceHeader.Options.UseTextOptions = True
            Me.GridColumn_DateCreated.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
            Me.GridColumn_DateCreated.Caption = "Created"
            Me.GridColumn_DateCreated.CustomizationCaption = "Date Created"
            Me.GridColumn_DateCreated.DisplayFormat.FormatString = "d"
            Me.GridColumn_DateCreated.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime
            Me.GridColumn_DateCreated.FieldName = "date_created"
            Me.GridColumn_DateCreated.GroupFormat.FormatString = "d"
            Me.GridColumn_DateCreated.GroupFormat.FormatType = DevExpress.Utils.FormatType.DateTime
            Me.GridColumn_DateCreated.GroupInterval = DevExpress.XtraGrid.ColumnGroupInterval.[Date]
            Me.GridColumn_DateCreated.Name = "GridColumn_DateCreated"
            Me.GridColumn_DateCreated.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.[True]
            Me.GridColumn_DateCreated.Visible = True
            Me.GridColumn_DateCreated.VisibleIndex = 1
            Me.GridColumn_DateCreated.Width = 81
            '
            'GridColumn_HomePh
            '
            Me.GridColumn_HomePh.AppearanceCell.Options.UseTextOptions = True
            Me.GridColumn_HomePh.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
            Me.GridColumn_HomePh.AppearanceHeader.Options.UseTextOptions = True
            Me.GridColumn_HomePh.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
            Me.GridColumn_HomePh.Caption = "Home PH#"
            Me.GridColumn_HomePh.CustomizationCaption = "Home Phone #"
            Me.GridColumn_HomePh.FieldName = "home_ph"
            Me.GridColumn_HomePh.Name = "GridColumn_HomePh"
            Me.GridColumn_HomePh.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.[True]
            Me.GridColumn_HomePh.Visible = True
            Me.GridColumn_HomePh.VisibleIndex = 2
            Me.GridColumn_HomePh.Width = 81
            '
            'GridColumn_Bankruptcy
            '
            Me.GridColumn_Bankruptcy.AppearanceCell.Options.UseTextOptions = True
            Me.GridColumn_Bankruptcy.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
            Me.GridColumn_Bankruptcy.AppearanceHeader.Options.UseTextOptions = True
            Me.GridColumn_Bankruptcy.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
            Me.GridColumn_Bankruptcy.Caption = "Bankruptcy"
            Me.GridColumn_Bankruptcy.CustomizationCaption = "Bankruptcy Status"
            Me.GridColumn_Bankruptcy.FieldName = "bankruptcy"
            Me.GridColumn_Bankruptcy.Name = "GridColumn_Bankruptcy"
            Me.GridColumn_Bankruptcy.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.[True]
            Me.GridColumn_Bankruptcy.Visible = True
            Me.GridColumn_Bankruptcy.VisibleIndex = 4
            Me.GridColumn_Bankruptcy.Width = 84
            '
            'Button_Edit
            '
            Me.Button_Edit.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
            Me.Button_Edit.Location = New System.Drawing.Point(401, 12)
            Me.Button_Edit.MaximumSize = New System.Drawing.Size(75, 0)
            Me.Button_Edit.Name = "Button_Edit"
            Me.Button_Edit.Size = New System.Drawing.Size(75, 22)
            Me.Button_Edit.StyleController = Me.LayoutControl1
            Me.Button_Edit.TabIndex = 1
            Me.Button_Edit.Text = "&Edit"
            '
            'LayoutControl1
            '
            Me.LayoutControl1.Controls.Add(Me.GridControl1)
            Me.LayoutControl1.Controls.Add(Me.Button_Cancel)
            Me.LayoutControl1.Controls.Add(Me.Button_Edit)
            Me.LayoutControl1.Controls.Add(Me.Button_Delete)
            Me.LayoutControl1.Dock = System.Windows.Forms.DockStyle.Fill
            Me.LayoutControl1.Location = New System.Drawing.Point(0, 24)
            Me.LayoutControl1.Name = "LayoutControl1"
            Me.LayoutControl1.Root = Me.LayoutControlGroup1
            Me.LayoutControl1.Size = New System.Drawing.Size(488, 271)
            Me.LayoutControl1.TabIndex = 4
            Me.LayoutControl1.Text = "LayoutControl1"
            '
            'Button_Cancel
            '
            Me.Button_Cancel.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
            Me.Button_Cancel.Location = New System.Drawing.Point(401, 38)
            Me.Button_Cancel.MaximumSize = New System.Drawing.Size(75, 0)
            Me.Button_Cancel.Name = "Button_Cancel"
            Me.Button_Cancel.Size = New System.Drawing.Size(75, 22)
            Me.Button_Cancel.StyleController = Me.LayoutControl1
            Me.Button_Cancel.TabIndex = 3
            Me.Button_Cancel.Text = "&Cancel"
            '
            'Button_Delete
            '
            Me.Button_Delete.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
            Me.Button_Delete.Appearance.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold)
            Me.Button_Delete.Appearance.ForeColor = System.Drawing.Color.Firebrick
            Me.Button_Delete.Appearance.Options.UseFont = True
            Me.Button_Delete.Appearance.Options.UseForeColor = True
            Me.Button_Delete.Location = New System.Drawing.Point(401, 93)
            Me.Button_Delete.MaximumSize = New System.Drawing.Size(75, 0)
            Me.Button_Delete.Name = "Button_Delete"
            Me.Button_Delete.Size = New System.Drawing.Size(75, 22)
            Me.Button_Delete.StyleController = Me.LayoutControl1
            Me.Button_Delete.TabIndex = 2
            Me.Button_Delete.Text = "&Delete"
            '
            'LayoutControlGroup1
            '
            Me.LayoutControlGroup1.CustomizationFormText = "LayoutControlGroup1"
            Me.LayoutControlGroup1.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.[True]
            Me.LayoutControlGroup1.GroupBordersVisible = False
            Me.LayoutControlGroup1.Items.AddRange(New DevExpress.XtraLayout.BaseLayoutItem() {Me.LayoutControlItem1, Me.LayoutControlItem3, Me.LayoutControlItem4, Me.LayoutControlItem2, Me.EmptySpaceItem1, Me.EmptySpaceItem2})
            Me.LayoutControlGroup1.Location = New System.Drawing.Point(0, 0)
            Me.LayoutControlGroup1.Name = "LayoutControlGroup1"
            Me.LayoutControlGroup1.Size = New System.Drawing.Size(488, 271)
            Me.LayoutControlGroup1.Text = "LayoutControlGroup1"
            Me.LayoutControlGroup1.TextVisible = False
            '
            'LayoutControlItem1
            '
            Me.LayoutControlItem1.Control = Me.GridControl1
            Me.LayoutControlItem1.CustomizationFormText = "Grid"
            Me.LayoutControlItem1.Location = New System.Drawing.Point(0, 0)
            Me.LayoutControlItem1.Name = "LayoutControlItem1"
            Me.LayoutControlItem1.Size = New System.Drawing.Size(389, 251)
            Me.LayoutControlItem1.Text = "Grid"
            Me.LayoutControlItem1.TextSize = New System.Drawing.Size(0, 0)
            Me.LayoutControlItem1.TextToControlDistance = 0
            Me.LayoutControlItem1.TextVisible = False
            '
            'LayoutControlItem3
            '
            Me.LayoutControlItem3.Control = Me.Button_Delete
            Me.LayoutControlItem3.CustomizationFormText = "Delete Button"
            Me.LayoutControlItem3.Location = New System.Drawing.Point(389, 81)
            Me.LayoutControlItem3.Name = "LayoutControlItem3"
            Me.LayoutControlItem3.Size = New System.Drawing.Size(79, 26)
            Me.LayoutControlItem3.Text = "Delete Button"
            Me.LayoutControlItem3.TextSize = New System.Drawing.Size(0, 0)
            Me.LayoutControlItem3.TextToControlDistance = 0
            Me.LayoutControlItem3.TextVisible = False
            '
            'LayoutControlItem4
            '
            Me.LayoutControlItem4.Control = Me.Button_Cancel
            Me.LayoutControlItem4.CustomizationFormText = "Cancel Button"
            Me.LayoutControlItem4.Location = New System.Drawing.Point(389, 26)
            Me.LayoutControlItem4.Name = "LayoutControlItem4"
            Me.LayoutControlItem4.Size = New System.Drawing.Size(79, 26)
            Me.LayoutControlItem4.Text = "Cancel Button"
            Me.LayoutControlItem4.TextSize = New System.Drawing.Size(0, 0)
            Me.LayoutControlItem4.TextToControlDistance = 0
            Me.LayoutControlItem4.TextVisible = False
            '
            'LayoutControlItem2
            '
            Me.LayoutControlItem2.Control = Me.Button_Edit
            Me.LayoutControlItem2.CustomizationFormText = "Edit Button"
            Me.LayoutControlItem2.Location = New System.Drawing.Point(389, 0)
            Me.LayoutControlItem2.Name = "LayoutControlItem2"
            Me.LayoutControlItem2.Size = New System.Drawing.Size(79, 26)
            Me.LayoutControlItem2.Text = "Edit Button"
            Me.LayoutControlItem2.TextSize = New System.Drawing.Size(0, 0)
            Me.LayoutControlItem2.TextToControlDistance = 0
            Me.LayoutControlItem2.TextVisible = False
            '
            'EmptySpaceItem1
            '
            Me.EmptySpaceItem1.AllowHotTrack = False
            Me.EmptySpaceItem1.CustomizationFormText = "EmptySpaceItem1"
            Me.EmptySpaceItem1.Location = New System.Drawing.Point(389, 52)
            Me.EmptySpaceItem1.MaxSize = New System.Drawing.Size(0, 29)
            Me.EmptySpaceItem1.MinSize = New System.Drawing.Size(10, 29)
            Me.EmptySpaceItem1.Name = "EmptySpaceItem1"
            Me.EmptySpaceItem1.Size = New System.Drawing.Size(79, 29)
            Me.EmptySpaceItem1.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom
            Me.EmptySpaceItem1.Text = "EmptySpaceItem1"
            Me.EmptySpaceItem1.TextSize = New System.Drawing.Size(0, 0)
            '
            'EmptySpaceItem2
            '
            Me.EmptySpaceItem2.AllowHotTrack = False
            Me.EmptySpaceItem2.CustomizationFormText = "EmptySpaceItem2"
            Me.EmptySpaceItem2.Location = New System.Drawing.Point(389, 107)
            Me.EmptySpaceItem2.Name = "EmptySpaceItem2"
            Me.EmptySpaceItem2.Size = New System.Drawing.Size(79, 144)
            Me.EmptySpaceItem2.Text = "EmptySpaceItem2"
            Me.EmptySpaceItem2.TextSize = New System.Drawing.Size(0, 0)
            '
            'barManager1
            '
            Me.barManager1.Bars.AddRange(New DevExpress.XtraBars.Bar() {Me.Bar2})
            Me.barManager1.DockControls.Add(Me.barDockControlTop)
            Me.barManager1.DockControls.Add(Me.barDockControlBottom)
            Me.barManager1.DockControls.Add(Me.barDockControlLeft)
            Me.barManager1.DockControls.Add(Me.barDockControlRight)
            Me.barManager1.Form = Me
            Me.barManager1.Items.AddRange(New DevExpress.XtraBars.BarItem() {Me.BarSubItem1, Me.BarSubItem2, Me.BarButtonItem1, Me.BarButtonItem2, Me.BarButtonItem3})
            Me.barManager1.MainMenu = Me.Bar2
            Me.barManager1.MaxItemId = 5
            '
            'Bar2
            '
            Me.Bar2.BarName = "Main menu"
            Me.Bar2.DockCol = 0
            Me.Bar2.DockRow = 0
            Me.Bar2.DockStyle = DevExpress.XtraBars.BarDockStyle.Top
            Me.Bar2.LinksPersistInfo.AddRange(New DevExpress.XtraBars.LinkPersistInfo() {New DevExpress.XtraBars.LinkPersistInfo(Me.BarSubItem1), New DevExpress.XtraBars.LinkPersistInfo(Me.BarSubItem2)})
            Me.Bar2.OptionsBar.MultiLine = True
            Me.Bar2.OptionsBar.UseWholeRow = True
            Me.Bar2.Text = "Main menu"
            '
            'BarSubItem1
            '
            Me.BarSubItem1.Caption = "&File"
            Me.BarSubItem1.Id = 0
            Me.BarSubItem1.LinksPersistInfo.AddRange(New DevExpress.XtraBars.LinkPersistInfo() {New DevExpress.XtraBars.LinkPersistInfo(Me.BarButtonItem1)})
            Me.BarSubItem1.Name = "BarSubItem1"
            '
            'BarButtonItem1
            '
            Me.BarButtonItem1.Caption = "&Exit"
            Me.BarButtonItem1.Id = 2
            Me.BarButtonItem1.Name = "BarButtonItem1"
            '
            'BarSubItem2
            '
            Me.BarSubItem2.Caption = "&Reports"
            Me.BarSubItem2.Id = 1
            Me.BarSubItem2.LinksPersistInfo.AddRange(New DevExpress.XtraBars.LinkPersistInfo() {New DevExpress.XtraBars.LinkPersistInfo(Me.BarButtonItem2), New DevExpress.XtraBars.LinkPersistInfo(Me.BarButtonItem3)})
            Me.BarSubItem2.Name = "BarSubItem2"
            '
            'BarButtonItem2
            '
            Me.BarButtonItem2.Caption = "&List..."
            Me.BarButtonItem2.Id = 3
            Me.BarButtonItem2.Name = "BarButtonItem2"
            '
            'BarButtonItem3
            '
            Me.BarButtonItem3.Caption = "&View Item..."
            Me.BarButtonItem3.Id = 4
            Me.BarButtonItem3.Name = "BarButtonItem3"
            '
            'barDockControlTop
            '
            Me.barDockControlTop.CausesValidation = False
            Me.barDockControlTop.Dock = System.Windows.Forms.DockStyle.Top
            Me.barDockControlTop.Location = New System.Drawing.Point(0, 0)
            Me.barDockControlTop.Size = New System.Drawing.Size(488, 24)
            '
            'barDockControlBottom
            '
            Me.barDockControlBottom.CausesValidation = False
            Me.barDockControlBottom.Dock = System.Windows.Forms.DockStyle.Bottom
            Me.barDockControlBottom.Location = New System.Drawing.Point(0, 295)
            Me.barDockControlBottom.Size = New System.Drawing.Size(488, 0)
            '
            'barDockControlLeft
            '
            Me.barDockControlLeft.CausesValidation = False
            Me.barDockControlLeft.Dock = System.Windows.Forms.DockStyle.Left
            Me.barDockControlLeft.Location = New System.Drawing.Point(0, 24)
            Me.barDockControlLeft.Size = New System.Drawing.Size(0, 271)
            '
            'barDockControlRight
            '
            Me.barDockControlRight.CausesValidation = False
            Me.barDockControlRight.Dock = System.Windows.Forms.DockStyle.Right
            Me.barDockControlRight.Location = New System.Drawing.Point(488, 24)
            Me.barDockControlRight.Size = New System.Drawing.Size(0, 271)
            '
            'MainForm
            '
            Me.AutoScaleBaseSize = New System.Drawing.Size(5, 14)
            Me.ClientSize = New System.Drawing.Size(488, 295)
            Me.Controls.Add(Me.LayoutControl1)
            Me.Controls.Add(Me.barDockControlLeft)
            Me.Controls.Add(Me.barDockControlRight)
            Me.Controls.Add(Me.barDockControlBottom)
            Me.Controls.Add(Me.barDockControlTop)
            Me.Name = "MainForm"
            Me.Text = "Intake Clients"
            CType(Me.DxErrorProvider1, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.GridControl1, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.GridView1, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.LayoutControl1, System.ComponentModel.ISupportInitialize).EndInit()
            Me.LayoutControl1.ResumeLayout(False)
            CType(Me.LayoutControlGroup1, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.LayoutControlItem1, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.LayoutControlItem3, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.LayoutControlItem4, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.LayoutControlItem2, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.EmptySpaceItem1, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.EmptySpaceItem2, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.barManager1, System.ComponentModel.ISupportInitialize).EndInit()
            Me.ResumeLayout(False)
        End Sub
        Friend WithEvents LayoutControl1 As DevExpress.XtraLayout.LayoutControl
        Friend WithEvents LayoutControlGroup1 As DevExpress.XtraLayout.LayoutControlGroup
        Friend WithEvents LayoutControlItem1 As DevExpress.XtraLayout.LayoutControlItem
        Friend WithEvents LayoutControlItem3 As DevExpress.XtraLayout.LayoutControlItem
        Friend WithEvents LayoutControlItem4 As DevExpress.XtraLayout.LayoutControlItem
        Friend WithEvents LayoutControlItem2 As DevExpress.XtraLayout.LayoutControlItem
        Friend WithEvents EmptySpaceItem1 As DevExpress.XtraLayout.EmptySpaceItem
        Friend WithEvents EmptySpaceItem2 As DevExpress.XtraLayout.EmptySpaceItem
        Friend WithEvents barManager1 As DevExpress.XtraBars.BarManager
        Friend WithEvents Bar2 As DevExpress.XtraBars.Bar
        Friend WithEvents BarSubItem1 As DevExpress.XtraBars.BarSubItem
        Friend WithEvents BarButtonItem1 As DevExpress.XtraBars.BarButtonItem
        Friend WithEvents BarSubItem2 As DevExpress.XtraBars.BarSubItem
        Friend WithEvents BarButtonItem2 As DevExpress.XtraBars.BarButtonItem
        Friend WithEvents BarButtonItem3 As DevExpress.XtraBars.BarButtonItem
        Friend WithEvents barDockControlTop As DevExpress.XtraBars.BarDockControl
        Friend WithEvents barDockControlBottom As DevExpress.XtraBars.BarDockControl
        Friend WithEvents barDockControlLeft As DevExpress.XtraBars.BarDockControl
        Friend WithEvents barDockControlRight As DevExpress.XtraBars.BarDockControl
#End Region

        ''' <summary>
        ''' The CANCEL button closes the form
        ''' </summary>
        Private Sub Button_Cancel_Click(ByVal sender As Object, ByVal e As EventArgs)
            Close()
        End Sub

        ''' <summary>
        ''' Load the list of pending INTAKE clients
        ''' </summary>
        Private Sub MainForm_Load(ByVal sender As Object, ByVal e As EventArgs)

            Dim current_cursor As Cursor = System.Windows.Forms.Cursor.Current
            Try
                System.Windows.Forms.Cursor.Current = Cursors.WaitCursor
                Using cmd As SqlClient.SqlCommand = New SqlCommand
                    cmd.Connection = New SqlConnection(DebtPlus.LINQ.SQLInfoClass.getDefault().ConnectionString)
                    cmd.CommandText = "SELECT intake_client, intake_id, date_created, home_ph, status, bankruptcy, date_imported FROM view_intake_clients WITH (NOLOCK) WHERE date_imported IS NULL"

                    Using da As New SqlClient.SqlDataAdapter(cmd)
                        da.Fill(ds, "intake_clients")
                    End Using
                End Using

                GridControl1.DataSource = New DataView(ds.Tables(0), "date_imported IS NULL", String.Empty, DataViewRowState.CurrentRows)
                GridControl1.RefreshDataSource()

            Catch ex As SqlClient.SqlException
                DebtPlus.UI.Common.ErrorHandling.HandleErrors(ex, "Error reading inake client list")

            Finally
                System.Windows.Forms.Cursor.Current = current_cursor
            End Try
        End Sub

        ''' <summary>
        ''' Process a change in the current row for the control
        ''' </summary>
        Private Sub GridView1_FocusedRowChanged(ByVal sender As Object, ByVal e As DevExpress.XtraGrid.Views.Base.FocusedRowChangedEventArgs)
            Dim gv As DevExpress.XtraGrid.Views.Grid.GridView = CType(sender, DevExpress.XtraGrid.Views.Grid.GridView)
            ControlRow = e.FocusedRowHandle

            ' Find the datarowview from the input tables.
            Dim drv As DataRowView
            intake_client = -1

            If ControlRow >= 0 Then
                drv = CType(gv.GetRow(ControlRow), DataRowView)
                If drv IsNot Nothing AndAlso drv("intake_client") IsNot Nothing AndAlso drv("intake_client") IsNot DBNull.Value Then
                    intake_client = Convert.ToInt32(drv("intake_client"))
                End If
            End If

            Button_Edit.Enabled = (intake_client > 0)
        End Sub

        ''' <summary>
        ''' Double Click on an item -- edit it
        ''' </summary>
        Private Sub GridView1_DoubleClick(ByVal sender As Object, ByVal e As EventArgs)

            ' Find the row targetted as the double-click item
            Dim gv As DevExpress.XtraGrid.Views.Grid.GridView = CType(sender, DevExpress.XtraGrid.Views.Grid.GridView)
            Dim ctl As DevExpress.XtraGrid.GridControl = CType(gv.GridControl, DevExpress.XtraGrid.GridControl)
            Dim hi As DevExpress.XtraGrid.Views.Grid.ViewInfo.GridHitInfo = gv.CalcHitInfo((ctl.PointToClient(Windows.Forms.Control.MousePosition)))

            ControlRow = hi.RowHandle

            ' Find the datarowview from the input tables.
            Dim drv As DataRowView
            If ControlRow >= 0 Then
                drv = CType(gv.GetRow(ControlRow), DataRowView)
            Else
                drv = Nothing
            End If

            ' If there is a row then select it
            intake_client = -1
            If drv IsNot Nothing Then
                If drv("intake_client") IsNot Nothing AndAlso drv("intake_client") IsNot DBNull.Value Then intake_client = Convert.ToInt32(drv("intake_client"))
            End If

            ' If there is a workshop then select it
            If intake_client > 0 Then Button_Edit.PerformClick()
        End Sub

        ''' <summary>
        ''' Process a click event on the row in the grid
        ''' </summary>
        Private Sub GridView1_MouseDown(ByVal sender As Object, ByVal e As MouseEventArgs)
            Dim gv As DevExpress.XtraGrid.Views.Grid.GridView = GridView1
            Dim hi As DevExpress.XtraGrid.Views.Grid.ViewInfo.GridHitInfo = gv.CalcHitInfo(New Point(e.X, e.Y))

            ' Remember the position for the popup menu handler.
            intake_client = -1
            If hi.InRow Then
                ControlRow = hi.RowHandle

                If ControlRow >= 0 Then
                    Dim drv As DataRowView = CType(GridView1.GetRow(ControlRow), DataRowView)
                    If drv IsNot Nothing Then
                        If drv("intake_client") IsNot Nothing AndAlso drv("intake_client") IsNot DBNull.Value Then intake_client = Convert.ToInt32(drv("intake_client"))
                    End If
                End If
            End If

            Button_Edit.Enabled = (intake_client > 0)
        End Sub

        ''' <summary>
        ''' Delete the client in the list
        ''' </summary>
        Private Sub Button_Delete_Click(ByVal sender As Object, ByVal e As EventArgs)

            ' Delete the row from the dataset. It will be actually deleted when the form is closed.
            If ControlRow >= 0 Then
                Dim drv As DataRowView = CType(GridView1.GetRow(ControlRow), DataRowView)
                If DebtPlus.Data.Prompts.RequestConfirmation_Delete = System.Windows.Forms.DialogResult.Yes Then drv.Delete()
            End If

        End Sub

        ''' <summary>
        ''' Post the updates to the database
        ''' </summary>
        Private Sub MainForm_Closing(ByVal sender As Object, ByVal e As System.ComponentModel.CancelEventArgs)

            Dim current_cursor As Cursor = System.Windows.Forms.Cursor.Current
            Try
                System.Windows.Forms.Cursor.Current = Cursors.WaitCursor
                Using delete_cmd As SqlClient.SqlCommand = New SqlCommand
                    delete_cmd.Connection = New SqlConnection(DebtPlus.LINQ.SQLInfoClass.getDefault().ConnectionString)
                    delete_cmd.CommandText = "xpr_intake_delete"
                    delete_cmd.CommandType = CommandType.StoredProcedure
                    delete_cmd.Parameters.Add("@intake_client", SqlDbType.Int, 0, "intake_client")

                    Using da As New SqlClient.SqlDataAdapter
                        da.DeleteCommand = delete_cmd
                        da.Update(ds.Tables(0))
                    End Using
                End Using

            Catch ex As SqlClient.SqlException
                DebtPlus.UI.Common.ErrorHandling.HandleErrors(ex, "Error updating intake clients")

            Finally
                System.Windows.Forms.Cursor.Current = current_cursor
            End Try
        End Sub

        ''' <summary>
        ''' Perform the edit of the current row when needed
        ''' </summary>
        Private Sub Button_Edit_Click(ByVal sender As Object, ByVal e As EventArgs)

            Dim drv As DataRowView = CType(GridView1.GetRow(ControlRow), DataRowView)

            ' Edit the current row.
            drv.BeginEdit()
            Using frm As New IntakeClientForm(drv)
                If frm.ShowDialog() = System.Windows.Forms.DialogResult.OK Then
                    drv.EndEdit()
                Else
                    drv.CancelEdit()
                End If
            End Using
        End Sub

        Private Sub BarButtonItem2_ItemClick(sender As System.Object, e As DevExpress.XtraBars.ItemClickEventArgs)
            Dim rpt As New DebtPlus.Reports.Intake.ClientList.IntakeClientListReport()
            If rpt.RequestReportParameters = DialogResult.OK Then
                rpt.RunReportInSeparateThread()
            End If
        End Sub

        Private Sub BarButtonItem1_ItemClick(sender As System.Object, e As DevExpress.XtraBars.ItemClickEventArgs)
            Close()
        End Sub

        Private Sub BarButtonItem3_ItemClick(sender As System.Object, e As DevExpress.XtraBars.ItemClickEventArgs)
            Dim RowHandle As Int32 = GridView1.FocusedRowHandle
            Dim InterNetClient As Int32 = -1

            If RowHandle >= 0 Then
                Dim DataRow As DataRowView = CType(GridView1.GetRow(RowHandle), DataRowView)
                If DataRow IsNot Nothing Then
                    InterNetClient = DebtPlus.Utils.Nulls.DInt(DataRow("intake_client"))
                End If
            End If

            If InterNetClient >= 0 Then
                Dim rpt As New DebtPlus.Reports.Intake.ClientInfo.IntakeClientInfoReport()
                rpt.Parameter_IntakeClient = InterNetClient
                If CType(rpt, DebtPlus.Interfaces.Reports.IReports).RequestReportParameters = Windows.Forms.DialogResult.OK Then
                    rpt.RunReportInSeparateThread()
                End If
            End If
        End Sub
    End Class
End Namespace
