#Region "Copyright 2000-2012 DebtPlus, L.L.C."
'{*******************************************************************}
'{                                                                   }
'{       DebtPlus Debt Management System                             }
'{                                                                   }
'{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
'{       ALL RIGHTS RESERVED                                         }
'{                                                                   }
'{   The entire contents of this file is protected by U.S. and       }
'{   International Copyright Laws. Unauthorized reproduction,        }
'{   reverse-engineering, and distribution of all or any portion of  }
'{   the code contained in this file is strictly prohibited and may  }
'{   result in severe civil and criminal penalties and will be       }
'{   prosecuted to the maximum extent possible under the law.        }
'{                                                                   }
'{   RESTRICTIONS                                                    }
'{                                                                   }
'{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
'{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
'{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
'{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
'{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
'{                                                                   }
'{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
'{   ADDITIONAL RESTRICTIONS.                                        }
'{                                                                   }
'{*******************************************************************}
#End Region

Option Compare Binary
Option Explicit On 
Option Strict On

Imports System.Data.SqlClient
Imports DebtPlus.Interfaces
Imports System.Windows.Forms

Namespace Intake.Import
    Public Class IntakeNotesList
        Inherits DevExpress.XtraEditors.XtraUserControl

#Region " Windows Form Designer generated code "

        Public Sub New()
            MyBase.New()
            InitializeComponent()
        End Sub

        'UserControl overrides dispose to clean up the component list.
        Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
            If disposing Then
                If components IsNot Nothing Then
                    components.Dispose()
                End If
            End If
            MyBase.Dispose(disposing)
        End Sub

        'Required by the Windows Form Designer
        Private components As System.ComponentModel.IContainer

        'NOTE: The following procedure is required by the Windows Form Designer
        'It can be modified using the Windows Form Designer.  
        'Do not modify it using the code editor.
        Friend WithEvents GridControl1 As DevExpress.XtraGrid.GridControl
        Friend WithEvents GridView1 As DevExpress.XtraGrid.Views.Grid.GridView
        Friend WithEvents GridColumn_Date As DevExpress.XtraGrid.Columns.GridColumn
        Friend WithEvents GridColumn_Creator As DevExpress.XtraGrid.Columns.GridColumn
        Friend WithEvents GridColumn_Note As DevExpress.XtraGrid.Columns.GridColumn
        Friend WithEvents NoteTextEdit As DevExpress.XtraEditors.Repository.RepositoryItemComboBox
        <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
            Me.GridControl1 = New DevExpress.XtraGrid.GridControl
            Me.GridView1 = New DevExpress.XtraGrid.Views.Grid.GridView
            Me.GridColumn_Date = New DevExpress.XtraGrid.Columns.GridColumn
            Me.GridColumn_Date.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.True
            Me.GridColumn_Creator = New DevExpress.XtraGrid.Columns.GridColumn
            Me.GridColumn_Creator.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.True
            Me.GridColumn_Note = New DevExpress.XtraGrid.Columns.GridColumn
            Me.GridColumn_Note.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.True
            Me.NoteTextEdit = New DevExpress.XtraEditors.Repository.RepositoryItemComboBox
            CType(Me.GridControl1, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.GridView1, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.NoteTextEdit, System.ComponentModel.ISupportInitialize).BeginInit()
            Me.SuspendLayout()
            '
            'GridControl1
            '
            Me.GridControl1.Dock = System.Windows.Forms.DockStyle.Fill
            '
            'GridControl1.EmbeddedNavigator
            '
            Me.GridControl1.EmbeddedNavigator.Name = ""
            Me.GridControl1.Location = New System.Drawing.Point(0, 0)
            Me.GridControl1.MainView = Me.GridView1
            Me.GridControl1.Name = "GridControl1"
            Me.GridControl1.RepositoryItems.AddRange(New DevExpress.XtraEditors.Repository.RepositoryItem() {Me.NoteTextEdit})
            Me.GridControl1.Size = New System.Drawing.Size(424, 280)
            Me.GridControl1.TabIndex = 0
            Me.GridControl1.UseEmbeddedNavigator = True
            Me.GridControl1.ViewCollection.AddRange(New DevExpress.XtraGrid.Views.Base.BaseView() {Me.GridView1})
            '
            'GridView1
            '
            Me.GridView1.Appearance.ColumnFilterButton.BackColor = System.Drawing.Color.FromArgb(CType(104, Byte), CType(184, Byte), CType(251, Byte))
            Me.GridView1.Appearance.ColumnFilterButton.BorderColor = System.Drawing.Color.FromArgb(CType(104, Byte), CType(184, Byte), CType(251, Byte))
            Me.GridView1.Appearance.ColumnFilterButton.ForeColor = System.Drawing.Color.White
            Me.GridView1.Appearance.ColumnFilterButton.Options.UseBackColor = True
            Me.GridView1.Appearance.ColumnFilterButton.Options.UseBorderColor = True
            Me.GridView1.Appearance.ColumnFilterButton.Options.UseForeColor = True
            Me.GridView1.Appearance.ColumnFilterButtonActive.BackColor = System.Drawing.Color.FromArgb(CType(170, Byte), CType(216, Byte), CType(254, Byte))
            Me.GridView1.Appearance.ColumnFilterButtonActive.BorderColor = System.Drawing.Color.FromArgb(CType(170, Byte), CType(216, Byte), CType(254, Byte))
            Me.GridView1.Appearance.ColumnFilterButtonActive.ForeColor = System.Drawing.Color.Black
            Me.GridView1.Appearance.ColumnFilterButtonActive.Options.UseBackColor = True
            Me.GridView1.Appearance.ColumnFilterButtonActive.Options.UseBorderColor = True
            Me.GridView1.Appearance.ColumnFilterButtonActive.Options.UseForeColor = True
            Me.GridView1.Appearance.Empty.BackColor = System.Drawing.Color.FromArgb(CType(236, Byte), CType(246, Byte), CType(255, Byte))
            Me.GridView1.Appearance.Empty.BackColor2 = System.Drawing.Color.White
            Me.GridView1.Appearance.Empty.Options.UseBackColor = True
            Me.GridView1.Appearance.EvenRow.BackColor = System.Drawing.Color.FromArgb(CType(247, Byte), CType(251, Byte), CType(255, Byte))
            Me.GridView1.Appearance.EvenRow.BorderColor = System.Drawing.Color.FromArgb(CType(247, Byte), CType(251, Byte), CType(255, Byte))
            Me.GridView1.Appearance.EvenRow.ForeColor = System.Drawing.Color.Black
            Me.GridView1.Appearance.EvenRow.Options.UseBackColor = True
            Me.GridView1.Appearance.EvenRow.Options.UseBorderColor = True
            Me.GridView1.Appearance.EvenRow.Options.UseForeColor = True
            Me.GridView1.Appearance.FilterCloseButton.BackColor = System.Drawing.Color.FromArgb(CType(104, Byte), CType(184, Byte), CType(251, Byte))
            Me.GridView1.Appearance.FilterCloseButton.BorderColor = System.Drawing.Color.FromArgb(CType(104, Byte), CType(184, Byte), CType(251, Byte))
            Me.GridView1.Appearance.FilterCloseButton.ForeColor = System.Drawing.Color.White
            Me.GridView1.Appearance.FilterCloseButton.Options.UseBackColor = True
            Me.GridView1.Appearance.FilterCloseButton.Options.UseBorderColor = True
            Me.GridView1.Appearance.FilterCloseButton.Options.UseForeColor = True
            Me.GridView1.Appearance.FilterPanel.BackColor = System.Drawing.Color.FromArgb(CType(236, Byte), CType(246, Byte), CType(255, Byte))
            Me.GridView1.Appearance.FilterPanel.BackColor2 = System.Drawing.Color.White
            Me.GridView1.Appearance.FilterPanel.ForeColor = System.Drawing.Color.Black
            Me.GridView1.Appearance.FilterPanel.Options.UseBackColor = True
            Me.GridView1.Appearance.FilterPanel.Options.UseForeColor = True
            Me.GridView1.Appearance.FixedLine.BackColor = System.Drawing.Color.FromArgb(CType(59, Byte), CType(133, Byte), CType(195, Byte))
            Me.GridView1.Appearance.FixedLine.Options.UseBackColor = True
            Me.GridView1.Appearance.FocusedCell.BackColor = System.Drawing.Color.White
            Me.GridView1.Appearance.FocusedCell.ForeColor = System.Drawing.Color.Black
            Me.GridView1.Appearance.FocusedCell.Options.UseBackColor = True
            Me.GridView1.Appearance.FocusedCell.Options.UseForeColor = True
            Me.GridView1.Appearance.FocusedRow.BackColor = System.Drawing.Color.FromArgb(CType(38, Byte), CType(109, Byte), CType(189, Byte))
            Me.GridView1.Appearance.FocusedRow.BorderColor = System.Drawing.Color.FromArgb(CType(59, Byte), CType(139, Byte), CType(206, Byte))
            Me.GridView1.Appearance.FocusedRow.ForeColor = System.Drawing.Color.White
            Me.GridView1.Appearance.FocusedRow.Options.UseBackColor = True
            Me.GridView1.Appearance.FocusedRow.Options.UseBorderColor = True
            Me.GridView1.Appearance.FocusedRow.Options.UseForeColor = True
            Me.GridView1.Appearance.FooterPanel.BackColor = System.Drawing.Color.FromArgb(CType(104, Byte), CType(184, Byte), CType(251, Byte))
            Me.GridView1.Appearance.FooterPanel.BorderColor = System.Drawing.Color.FromArgb(CType(104, Byte), CType(184, Byte), CType(251, Byte))
            Me.GridView1.Appearance.FooterPanel.ForeColor = System.Drawing.Color.Black
            Me.GridView1.Appearance.FooterPanel.Options.UseBackColor = True
            Me.GridView1.Appearance.FooterPanel.Options.UseBorderColor = True
            Me.GridView1.Appearance.FooterPanel.Options.UseForeColor = True
            Me.GridView1.Appearance.GroupButton.BackColor = System.Drawing.Color.FromArgb(CType(104, Byte), CType(184, Byte), CType(251, Byte))
            Me.GridView1.Appearance.GroupButton.BorderColor = System.Drawing.Color.FromArgb(CType(104, Byte), CType(184, Byte), CType(251, Byte))
            Me.GridView1.Appearance.GroupButton.Options.UseBackColor = True
            Me.GridView1.Appearance.GroupButton.Options.UseBorderColor = True
            Me.GridView1.Appearance.GroupFooter.BackColor = System.Drawing.Color.FromArgb(CType(170, Byte), CType(216, Byte), CType(254, Byte))
            Me.GridView1.Appearance.GroupFooter.BorderColor = System.Drawing.Color.FromArgb(CType(170, Byte), CType(216, Byte), CType(254, Byte))
            Me.GridView1.Appearance.GroupFooter.ForeColor = System.Drawing.Color.Black
            Me.GridView1.Appearance.GroupFooter.Options.UseBackColor = True
            Me.GridView1.Appearance.GroupFooter.Options.UseBorderColor = True
            Me.GridView1.Appearance.GroupFooter.Options.UseForeColor = True
            Me.GridView1.Appearance.GroupPanel.BackColor = System.Drawing.Color.FromArgb(CType(236, Byte), CType(246, Byte), CType(255, Byte))
            Me.GridView1.Appearance.GroupPanel.BackColor2 = System.Drawing.Color.White
            Me.GridView1.Appearance.GroupPanel.ForeColor = System.Drawing.Color.Black
            Me.GridView1.Appearance.GroupPanel.Options.UseBackColor = True
            Me.GridView1.Appearance.GroupPanel.Options.UseForeColor = True
            Me.GridView1.Appearance.GroupRow.BackColor = System.Drawing.Color.FromArgb(CType(170, Byte), CType(216, Byte), CType(254, Byte))
            Me.GridView1.Appearance.GroupRow.BorderColor = System.Drawing.Color.FromArgb(CType(170, Byte), CType(216, Byte), CType(254, Byte))
            Me.GridView1.Appearance.GroupRow.ForeColor = System.Drawing.Color.Black
            Me.GridView1.Appearance.GroupRow.Options.UseBackColor = True
            Me.GridView1.Appearance.GroupRow.Options.UseBorderColor = True
            Me.GridView1.Appearance.GroupRow.Options.UseForeColor = True
            Me.GridView1.Appearance.HeaderPanel.BackColor = System.Drawing.Color.FromArgb(CType(139, Byte), CType(201, Byte), CType(254, Byte))
            Me.GridView1.Appearance.HeaderPanel.BorderColor = System.Drawing.Color.FromArgb(CType(139, Byte), CType(201, Byte), CType(254, Byte))
            Me.GridView1.Appearance.HeaderPanel.ForeColor = System.Drawing.Color.Black
            Me.GridView1.Appearance.HeaderPanel.Options.UseBackColor = True
            Me.GridView1.Appearance.HeaderPanel.Options.UseBorderColor = True
            Me.GridView1.Appearance.HeaderPanel.Options.UseForeColor = True
            Me.GridView1.Appearance.HideSelectionRow.BackColor = System.Drawing.Color.FromArgb(CType(105, Byte), CType(170, Byte), CType(225, Byte))
            Me.GridView1.Appearance.HideSelectionRow.BorderColor = System.Drawing.Color.FromArgb(CType(83, Byte), CType(155, Byte), CType(215, Byte))
            Me.GridView1.Appearance.HideSelectionRow.ForeColor = System.Drawing.Color.FromArgb(CType(236, Byte), CType(246, Byte), CType(255, Byte))
            Me.GridView1.Appearance.HideSelectionRow.Options.UseBackColor = True
            Me.GridView1.Appearance.HideSelectionRow.Options.UseBorderColor = True
            Me.GridView1.Appearance.HideSelectionRow.Options.UseForeColor = True
            Me.GridView1.Appearance.HorzLine.BackColor = System.Drawing.Color.FromArgb(CType(104, Byte), CType(184, Byte), CType(251, Byte))
            Me.GridView1.Appearance.HorzLine.Options.UseBackColor = True
            Me.GridView1.Appearance.OddRow.BackColor = System.Drawing.Color.FromArgb(CType(236, Byte), CType(246, Byte), CType(255, Byte))
            Me.GridView1.Appearance.OddRow.BorderColor = System.Drawing.Color.FromArgb(CType(236, Byte), CType(246, Byte), CType(255, Byte))
            Me.GridView1.Appearance.OddRow.ForeColor = System.Drawing.Color.Black
            Me.GridView1.Appearance.OddRow.Options.UseBackColor = True
            Me.GridView1.Appearance.OddRow.Options.UseBorderColor = True
            Me.GridView1.Appearance.OddRow.Options.UseForeColor = True
            Me.GridView1.Appearance.Preview.Font = New System.Drawing.Font("Verdana", 7.5!)
            Me.GridView1.Appearance.Preview.ForeColor = System.Drawing.Color.FromArgb(CType(83, Byte), CType(155, Byte), CType(215, Byte))
            Me.GridView1.Appearance.Preview.Options.UseFont = True
            Me.GridView1.Appearance.Preview.Options.UseForeColor = True
            Me.GridView1.Appearance.Row.BackColor = System.Drawing.Color.FromArgb(CType(247, Byte), CType(251, Byte), CType(255, Byte))
            Me.GridView1.Appearance.Row.ForeColor = System.Drawing.Color.Black
            Me.GridView1.Appearance.Row.Options.UseBackColor = True
            Me.GridView1.Appearance.Row.Options.UseForeColor = True
            Me.GridView1.Appearance.RowSeparator.BackColor = System.Drawing.Color.FromArgb(CType(236, Byte), CType(246, Byte), CType(255, Byte))
            Me.GridView1.Appearance.RowSeparator.BackColor2 = System.Drawing.Color.White
            Me.GridView1.Appearance.RowSeparator.Options.UseBackColor = True
            Me.GridView1.Appearance.SelectedRow.BackColor = System.Drawing.Color.FromArgb(CType(83, Byte), CType(155, Byte), CType(215, Byte))
            Me.GridView1.Appearance.SelectedRow.ForeColor = System.Drawing.Color.White
            Me.GridView1.Appearance.SelectedRow.Options.UseBackColor = True
            Me.GridView1.Appearance.SelectedRow.Options.UseForeColor = True
            Me.GridView1.Appearance.TopNewRow.BackColor = System.Drawing.Color.White
            Me.GridView1.Appearance.TopNewRow.Options.UseBackColor = True
            Me.GridView1.Appearance.VertLine.BackColor = System.Drawing.Color.FromArgb(CType(104, Byte), CType(184, Byte), CType(251, Byte))
            Me.GridView1.Appearance.VertLine.Options.UseBackColor = True
            Me.GridView1.Columns.AddRange(New DevExpress.XtraGrid.Columns.GridColumn() {Me.GridColumn_Date, Me.GridColumn_Creator, Me.GridColumn_Note})
            Me.GridView1.GridControl = Me.GridControl1
            Me.GridView1.Name = "GridView1"
            Me.GridView1.NewItemRowText = "Click here to add a new note"
            Me.GridView1.OptionsBehavior.AllowIncrementalSearch = True
            Me.GridView1.OptionsNavigation.AutoFocusNewRow = True
            Me.GridView1.OptionsView.EnableAppearanceEvenRow = True
            Me.GridView1.OptionsView.EnableAppearanceOddRow = True
            Me.GridView1.OptionsView.NewItemRowPosition = DevExpress.XtraGrid.Views.Grid.NewItemRowPosition.Top
            Me.GridView1.OptionsView.ShowGroupPanel = False
            Me.GridView1.SortInfo.AddRange(New DevExpress.XtraGrid.Columns.GridColumnSortInfo() {New DevExpress.XtraGrid.Columns.GridColumnSortInfo(Me.GridColumn_Date, DevExpress.Data.ColumnSortOrder.Ascending)})
            '
            'GridColumn_Date
            '
            Me.GridColumn_Date.Caption = "Date"
            Me.GridColumn_Date.CustomizationCaption = "Date Created"
            Me.GridColumn_Date.DisplayFormat.FormatString = "d"
            Me.GridColumn_Date.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime
            Me.GridColumn_Date.FieldName = "date_created"
            Me.GridColumn_Date.GroupFormat.FormatString = "d"
            Me.GridColumn_Date.GroupFormat.FormatType = DevExpress.Utils.FormatType.DateTime
            Me.GridColumn_Date.GroupInterval = DevExpress.XtraGrid.ColumnGroupInterval.Date
            Me.GridColumn_Date.Name = "GridColumn_Date"
            Me.GridColumn_Date.OptionsColumn.AllowEdit = False
            Me.GridColumn_Date.Visible = True
            Me.GridColumn_Date.VisibleIndex = 0
            Me.GridColumn_Date.Width = 79
            '
            'GridColumn_Creator
            '
            Me.GridColumn_Creator.Caption = "Creator"
            Me.GridColumn_Creator.CustomizationCaption = "Created By"
            Me.GridColumn_Creator.FieldName = "created_by"
            Me.GridColumn_Creator.Name = "GridColumn_Creator"
            Me.GridColumn_Creator.OptionsColumn.AllowEdit = False
            Me.GridColumn_Creator.Visible = True
            Me.GridColumn_Creator.VisibleIndex = 1
            Me.GridColumn_Creator.Width = 90
            '
            'GridColumn_Note
            '
            Me.GridColumn_Note.Caption = "Note"
            Me.GridColumn_Note.ColumnEdit = Me.NoteTextEdit
            Me.GridColumn_Note.CustomizationCaption = "Note Text"
            Me.GridColumn_Note.FieldName = "note"
            Me.GridColumn_Note.Name = "GridColumn_Note"
            Me.GridColumn_Note.Visible = True
            Me.GridColumn_Note.VisibleIndex = 2
            Me.GridColumn_Note.Width = 234
            '
            'NoteTextEdit
            '
            Me.NoteTextEdit.AutoHeight = False
            Me.NoteTextEdit.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
            Me.NoteTextEdit.MaxLength = 50
            Me.NoteTextEdit.Name = "NoteTextEdit"
            Me.NoteTextEdit.Sorted = True
            '
            'NotesList
            '
            Me.Controls.Add(Me.GridControl1)
            Me.Name = "NotesList"
            Me.Size = New System.Drawing.Size(424, 280)
            CType(Me.GridControl1, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.GridView1, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.NoteTextEdit, System.ComponentModel.ISupportInitialize).EndInit()
            Me.ResumeLayout(False)

        End Sub

#End Region

        Private MyForm As Form
        Private intake_client As Int32 = -1

        Dim WithEvents _tbl As DataTable = Nothing
        Private Property tbl As DataTable
            Get
                Return _tbl
            End Get
            Set(value As DataTable)
                If _tbl IsNot Nothing Then
                    RemoveHandler tbl.RowChanged, AddressOf tbl_RowChanged
                    RemoveHandler tbl.RowDeleting, AddressOf tbl_RowDeleting
                End If
                _tbl = value
                If _tbl IsNot Nothing Then
                    AddHandler tbl.RowChanged, AddressOf tbl_RowChanged
                    AddHandler tbl.RowDeleting, AddressOf tbl_RowDeleting
                End If
            End Set
        End Property

        ''' <summary>
        ''' Process the loading for the control
        ''' </summary>
        Protected Overrides Sub OnLoad(ByVal e As EventArgs)
            MyBase.OnLoad(e)

            ' Find the form
            MyForm = FindForm()

            ' In case we are called more than once!!!!
            RemoveHandler MyForm.Closing, AddressOf Form_Closing

            ' Propigate the tooltip controller to our control
            If TypeOf MyForm Is IForm Then
                With GridControl1
                    .ToolTipController = CType(MyForm, IForm).GetToolTipController
                End With
            End If

            ' Hook into the form closing event to save our changes
            AddHandler MyForm.Closing, AddressOf Form_Closing
        End Sub

        ''' <summary>
        ''' Read the information from the database
        ''' </summary>
        Public Sub ReadForm(ByVal IntakeClient As Int32)

            ' Save the current set of notes before loading a new batch
            tbl = ds.Tables("intake_notes")
            If tbl IsNot Nothing Then
                SaveChanges(tbl)
                tbl.Clear()
            End If

            intake_client = IntakeClient

            ' Read the list of notes for this client
            Dim cmd As SqlClient.SqlCommand = New SqlCommand
            With cmd
                .Connection = New SqlConnection(DebtPlus.LINQ.SQLInfoClass.getDefault().ConnectionString)
                .CommandText = "SELECT intake_note, note, date_created, created_by FROM intake_notes WITH (NOLOCK) WHERE intake_client=@intake_client"
                .Parameters.Add("@intake_client", SqlDbType.Int).Value = intake_client
            End With

            ' Read the notes from the database
            Dim current_cursor As Cursor = System.Windows.Forms.Cursor.Current
            Try
                System.Windows.Forms.Cursor.Current = Cursors.WaitCursor

                ' Populate the dataset and the editor with the standard set of notes
                ReadStandardNotes()

                Using da As New SqlClient.SqlDataAdapter(cmd)
                    da.Fill(ds, "intake_notes")
                End Using
                tbl = ds.Tables("intake_notes")

                With GridControl1
                    .DataSource = tbl.DefaultView
                    .RefreshDataSource()
                End With

            Catch ex As SqlClient.SqlException
                DebtPlus.UI.Common.ErrorHandling.HandleErrors(ex, "Error reading intake_notes table")

            Finally
                System.Windows.Forms.Cursor.Current = current_cursor
            End Try
        End Sub

        ''' <summary>
        ''' Read the text for the standard messages
        ''' </summary>
        Private Sub ReadStandardNotes()

            If ds.Tables("intake_message_types") IsNot Nothing Then

                ' Read the list of notes for this client
                Using cmd As SqlClient.SqlCommand = New SqlCommand
                    cmd.Connection = New SqlConnection(DebtPlus.LINQ.SQLInfoClass.getDefault().ConnectionString)
                    cmd.CommandText = "SELECT intake_message_type, message FROM intake_message_types WITH (NOLOCK)"
                    cmd.CommandType = CommandType.Text

                    Using da As New SqlClient.SqlDataAdapter(cmd)
                        da.Fill(ds, "intake_message_types")
                    End Using
                End Using
            End If

            ' Set the control to use these items
            With NoteTextEdit
                .Items.Clear()
                For Each drv As DataRowView In ds.Tables("intake_message_types").DefaultView
                    .Items.Add(New DebtPlus.Data.Controls.ComboboxItem(Convert.ToString(drv("message")), Convert.ToInt32(drv("intake_message_type"))))
                Next
            End With
        End Sub

        ''' <summary>
        ''' Post the updates to the database
        ''' </summary>
        Private Sub Form_Closing(ByVal sender As Object, ByVal e As System.ComponentModel.CancelEventArgs)

            ' Do the update if there is a change to the tables
            tbl = ds.Tables("intake_notes")
            If tbl IsNot Nothing Then SaveChanges(tbl)
        End Sub

        ''' <summary>
        ''' Save the database changes
        ''' </summary>
        Private Sub SaveChanges(ByVal tbl As DataTable)
            Using da As New SqlClient.SqlDataAdapter()

                ' Build the update command
                Dim cmd As SqlClient.SqlCommand = New SqlCommand
                With cmd
                    .Connection = New SqlConnection(DebtPlus.LINQ.SQLInfoClass.getDefault().ConnectionString)
                    .CommandText = "UPDATE intake_notes SET note=@note WHERE intake_note=@intake_note"
                    .Parameters.Add("@note", SqlDbType.VarChar, 50, "note")
                    .Parameters.Add("@intake_note", SqlDbType.Int, 0, "intake_note")
                End With
                da.UpdateCommand = cmd

                ' Build the delete command
                cmd = New SqlCommand
                With cmd
                    .Connection = New SqlConnection(DebtPlus.LINQ.SQLInfoClass.getDefault().ConnectionString)
                    .CommandText = "DELETE FROM intake_notes WHERE intake_note=@intake_note"
                    .Parameters.Add("@intake_note", SqlDbType.Int, 0, "intake_note")
                End With
                da.DeleteCommand = cmd

                ' Build the insert command
                cmd = New SqlCommand
                With cmd
                    .Connection = New SqlConnection(DebtPlus.LINQ.SQLInfoClass.getDefault().ConnectionString)
                    .CommandText = "INSERT INTO intake_notes(intake_client,note) VALUES (@intake_client,@note)"
                    .Parameters.Add("@intake_client", SqlDbType.Int).Value = intake_client
                    .Parameters.Add("@note", SqlDbType.VarChar, 50, "note")
                End With
                da.InsertCommand = cmd

                Dim current_cursor As Cursor = System.Windows.Forms.Cursor.Current
                Try
                    System.Windows.Forms.Cursor.Current = Cursors.WaitCursor
                    da.Update(tbl)

                Catch ex As SqlClient.SqlException
                    DebtPlus.UI.Common.ErrorHandling.HandleErrors(ex)

                Finally
                    System.Windows.Forms.Cursor.Current = current_cursor
                End Try
            End Using
        End Sub

        ''' <summary>
        ''' Handle the creation of a new row to insert the data
        ''' </summary>
        Private Sub tbl_RowChanged(ByVal sender As Object, ByVal e As DataRowChangeEventArgs)
            Dim row As DataRow = e.Row

            ' Supply needed defaults for the display when a new row is added to the table
            If e.Action = DataRowAction.Add Then
                If row("date_created") Is DBNull.Value Then row("date_created") = Now
                If row("created_by") Is DBNull.Value Then row("created_by") = "Me"
            End If
        End Sub

        Private Sub tbl_RowDeleting(ByVal sender As Object, ByVal e As DataRowChangeEventArgs)
            If e.Action = DataRowAction.Delete AndAlso DebtPlus.Data.Prompts.RequestConfirmation_Delete = DialogResult.No Then Throw New ApplicationException("The delete was cancelled")
        End Sub
    End Class
End Namespace
