#Region "Copyright 2000-2012 DebtPlus, L.L.C."
'{*******************************************************************}
'{                                                                   }
'{       DebtPlus Debt Management System                             }
'{                                                                   }
'{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
'{       ALL RIGHTS RESERVED                                         }
'{                                                                   }
'{   The entire contents of this file is protected by U.S. and       }
'{   International Copyright Laws. Unauthorized reproduction,        }
'{   reverse-engineering, and distribution of all or any portion of  }
'{   the code contained in this file is strictly prohibited and may  }
'{   result in severe civil and criminal penalties and will be       }
'{   prosecuted to the maximum extent possible under the law.        }
'{                                                                   }
'{   RESTRICTIONS                                                    }
'{                                                                   }
'{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
'{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
'{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
'{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
'{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
'{                                                                   }
'{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
'{   ADDITIONAL RESTRICTIONS.                                        }
'{                                                                   }
'{*******************************************************************}
#End Region

Option Compare Binary
Option Explicit On 
Option Strict On

Imports System.Data.SqlClient
Imports System.Windows.Forms

Namespace Intake.Import
    Public Class IntakePeople
        Inherits DevExpress.XtraEditors.XtraUserControl

        Public Event DataChanged(ByVal sender As Object, ByVal e As EventArgs)
        Private IntakeClient As Int32 = -1
        Public Property person() As Int32 = -1

        Public Sub New()
            MyBase.New()
            InitializeComponent()
            AddHandler Button_Update.Click, AddressOf Button_Update_Click
            AddHandler NoEmployer.EditValueChanged, AddressOf NoEmployer_EditValueChanged
        End Sub

#Region " Windows Form Designer generated code "

        'UserControl overrides dispose to clean up the component list.
        Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
            If disposing Then
                If components IsNot Nothing Then
                    components.Dispose()
                End If
            End If
            MyBase.Dispose(disposing)
        End Sub

        'Required by the Windows Form Designer
        Private components As System.ComponentModel.IContainer

        'NOTE: The following procedure is required by the Windows Form Designer
        'It can be modified using the Windows Form Designer.  
        'Do not modify it using the code editor.
        Friend WithEvents LabelControl1 As DevExpress.XtraEditors.LabelControl
        Friend WithEvents client_name As DevExpress.XtraEditors.LabelControl
        Friend WithEvents GroupControl1 As DevExpress.XtraEditors.GroupControl
        Friend WithEvents LabelControl2 As DevExpress.XtraEditors.LabelControl
        Friend WithEvents LabelControl3 As DevExpress.XtraEditors.LabelControl
        Friend WithEvents employer As DevExpress.XtraEditors.LabelControl
        Friend WithEvents Button_Update As DevExpress.XtraEditors.SimpleButton
        Friend WithEvents NoEmployer As DevExpress.XtraEditors.CheckEdit
        <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
            Me.LabelControl1 = New DevExpress.XtraEditors.LabelControl
            Me.client_name = New DevExpress.XtraEditors.LabelControl
            Me.GroupControl1 = New DevExpress.XtraEditors.GroupControl
            Me.NoEmployer = New DevExpress.XtraEditors.CheckEdit
            Me.Button_Update = New DevExpress.XtraEditors.SimpleButton
            Me.employer = New DevExpress.XtraEditors.LabelControl
            Me.LabelControl2 = New DevExpress.XtraEditors.LabelControl
            Me.LabelControl3 = New DevExpress.XtraEditors.LabelControl
            CType(Me.GroupControl1, System.ComponentModel.ISupportInitialize).BeginInit()
            Me.GroupControl1.SuspendLayout()
            CType(Me.NoEmployer.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
            Me.SuspendLayout()
            '
            'LabelControl1
            '
            Me.LabelControl1.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.Horizontal
            Me.LabelControl1.Location = New System.Drawing.Point(8, 0)
            Me.LabelControl1.Name = "LabelControl1"
            Me.LabelControl1.Size = New System.Drawing.Size(31, 13)
            Me.LabelControl1.TabIndex = 0
            Me.LabelControl1.Text = "Name:"
            Me.LabelControl1.UseMnemonic = False
            '
            'client_name
            '
            Me.client_name.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                        Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
            Me.client_name.Location = New System.Drawing.Point(64, 0)
            Me.client_name.Name = "client_name"
            Me.client_name.Size = New System.Drawing.Size(208, 14)
            Me.client_name.TabIndex = 1
            Me.client_name.UseMnemonic = False
            '
            'GroupControl1
            '
            Me.GroupControl1.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                        Or System.Windows.Forms.AnchorStyles.Left) _
                        Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
            Me.GroupControl1.Controls.Add(Me.NoEmployer)
            Me.GroupControl1.Controls.Add(Me.Button_Update)
            Me.GroupControl1.Controls.Add(Me.employer)
            Me.GroupControl1.Location = New System.Drawing.Point(64, 16)
            Me.GroupControl1.Name = "GroupControl1"
            Me.GroupControl1.ShowCaption = False
            Me.GroupControl1.Size = New System.Drawing.Size(216, 168)
            Me.GroupControl1.TabIndex = 2
            Me.GroupControl1.Text = "GroupControl1"
            '
            'NoEmployer
            '
            Me.NoEmployer.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
            Me.NoEmployer.Location = New System.Drawing.Point(8, 143)
            Me.NoEmployer.Name = "NoEmployer"
            '
            'NoEmployer.Properties
            '
            Me.NoEmployer.Properties.Caption = "Import without an employer"
            Me.NoEmployer.Size = New System.Drawing.Size(208, 19)
            Me.NoEmployer.TabIndex = 2
            '
            'Button_Update
            '
            Me.Button_Update.Anchor = System.Windows.Forms.AnchorStyles.Bottom
            Me.Button_Update.Location = New System.Drawing.Point(71, 112)
            Me.Button_Update.Name = "Button_Update"
            Me.Button_Update.TabIndex = 1
            Me.Button_Update.Text = "Update..."
            '
            'employer
            '
            Me.employer.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                        Or System.Windows.Forms.AnchorStyles.Left) _
                        Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
            Me.employer.Location = New System.Drawing.Point(8, 8)
            Me.employer.Name = "employer"
            Me.employer.Size = New System.Drawing.Size(200, 96)
            Me.employer.TabIndex = 0
            Me.employer.UseMnemonic = False
            '
            'LabelControl2
            '
            Me.LabelControl2.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.Horizontal
            Me.LabelControl2.Location = New System.Drawing.Point(8, 24)
            Me.LabelControl2.Name = "LabelControl2"
            Me.LabelControl2.Size = New System.Drawing.Size(48, 13)
            Me.LabelControl2.TabIndex = 4
            Me.LabelControl2.Text = "Employer:"
            Me.LabelControl2.UseMnemonic = False
            '
            'LabelControl3
            '
            Me.LabelControl3.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.Horizontal
            Me.LabelControl3.Location = New System.Drawing.Point(8, 40)
            Me.LabelControl3.Name = "LabelControl3"
            Me.LabelControl3.Size = New System.Drawing.Size(43, 13)
            Me.LabelControl3.TabIndex = 5
            Me.LabelControl3.Text = "Address:"
            Me.LabelControl3.UseMnemonic = False
            '
            'IntakePeople
            '
            Me.Controls.Add(Me.LabelControl3)
            Me.Controls.Add(Me.LabelControl2)
            Me.Controls.Add(Me.GroupControl1)
            Me.Controls.Add(Me.client_name)
            Me.Controls.Add(Me.LabelControl1)
            Me.Name = "IntakePeople"
            Me.Size = New System.Drawing.Size(280, 184)
            CType(Me.GroupControl1, System.ComponentModel.ISupportInitialize).EndInit()
            Me.GroupControl1.ResumeLayout(False)
            CType(Me.NoEmployer.Properties, System.ComponentModel.ISupportInitialize).EndInit()
            Me.ResumeLayout(False)

        End Sub

#End Region

        Public Sub ReadForm(ByVal intake_client As Int32)
            IntakeClient = intake_client
            RefreshInformation()
        End Sub

        Public ReadOnly Property isValid() As Boolean
            Get
                Return Not person_read OrElse (person_employer > 0 OrElse NoEmployer.Checked)
            End Get
        End Property

        Private Sub Button_Update_Click(ByVal sender As Object, ByVal e As EventArgs)

            ' Ignore the button if the person is not valid
            If Not person_read Then Return

            ' If there is an employer then generate a warning.
            If person_employer > 0 Then
                If DebtPlus.Data.Forms.MessageBox.Show(String.Format("The employer is already specified for this person.{0}{0}If you continue, you will erase the previous employer.{0}Are you sure that you want to do this?", Environment.NewLine), "Warning: Employer about to be overwritten", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2) <> DialogResult.Yes Then
                    Return
                End If
            End If

            ' Do the update for the employer
            Dim NewEmployer As Int32 = -1
            Using frm As New Employers.EmployerListForm()
                If frm.ShowDialog() = DialogResult.OK Then
                    NewEmployer = frm.Employer
                End If
            End Using

            ' If there is a new employer then update the people table accordingly
            If NewEmployer > 0 Then
                Dim cn As SqlClient.SqlConnection = New SqlConnection(DebtPlus.LINQ.SQLInfoClass.getDefault().ConnectionString)
                Dim current_cursor As Cursor = System.Windows.Forms.Cursor.Current
                Try
                    System.Windows.Forms.Cursor.Current = Cursors.WaitCursor
                    cn.Open()
                    Using cmd As SqlClient.SqlCommand = New SqlCommand
                        With cmd
                            .Connection = cn
                            .CommandText = "UPDATE intake_people SET employer=e.employer, employer_name=e.name, employer_address1 = dbo.format_Address_Line_1(ea.house,ea.direction,ea.street,ea.suffix,ea.modifier,ea.modifier_value), employer_address2 = ea.address_line_2, employer_city=ea.city, employer_state=ea.state, employer_postalcode=ea.postalcode FROM intake_people p LEFT OUTER JOIN employers e WITH (NOLOCK) ON @employer = e.employer LEFT OUTER JOIN addresses ea WITH (NOLOCK) ON e.AddressID = ea.Address WHERE p.intake_client=@intake_client AND p.person = @person"
                            .Parameters.Add("@intake_client", SqlDbType.Int).Value = IntakeClient
                            .Parameters.Add("@person", SqlDbType.Int).Value = person
                            .Parameters.Add("@employer", SqlDbType.Int).Value = NewEmployer
                            .ExecuteNonQuery()
                        End With
                    End Using

                Catch ex As SqlClient.SqlException
                    DebtPlus.UI.Common.ErrorHandling.HandleErrors(ex, "Error setting employer in intake_people")

                Finally
                    System.Windows.Forms.Cursor.Current = current_cursor
                    If cn IsNot Nothing Then cn.Dispose()
                End Try

                ' Reload the client information with the new employer data
                RefreshInformation()
                RaiseEvent DataChanged(Me, e)
            End If
        End Sub

        Dim person_read As Boolean
        Dim person_employer As Int32 = -1
        Dim person_name As String = String.Empty
        Dim person_employer_name As String = String.Empty
        Dim person_employer_address1 As String = String.Empty
        Dim person_employer_address2 As String = String.Empty
        Dim person_employer_address3 As String = String.Empty

        Private Sub RefreshInformation()

            ' Clear the current settings
            person_read = False
            person_employer = -1
            person_name = String.Empty
            person_employer_name = String.Empty
            person_employer_address1 = String.Empty
            person_employer_address2 = String.Empty
            person_employer_address3 = String.Empty

            ' Read the personal information for the client
            Dim cn As SqlClient.SqlConnection = New SqlConnection(DebtPlus.LINQ.SQLInfoClass.getDefault().ConnectionString)
            Dim rd As SqlClient.SqlDataReader = Nothing

            Dim current_cursor As Cursor = System.Windows.Forms.Cursor.Current
            Try
                System.Windows.Forms.Cursor.Current = Cursors.WaitCursor
                cn.Open()
                Using cmd As SqlClient.SqlCommand = New SqlCommand
                    With cmd
                        .Connection = cn
                        .CommandText = "SELECT name, employer, employer_name, employer_address1, employer_address2, employer_address3 FROM view_intake_people WITH (NOLOCK) WHERE intake_client=@intake_client AND person=@person"
                        .Parameters.Add("@intake_client", SqlDbType.Int).Value = IntakeClient
                        .Parameters.Add("@person", SqlDbType.Int).Value = person
                        rd = .ExecuteReader(CommandBehavior.CloseConnection Or CommandBehavior.SingleRow)
                    End With
                End Using

                If rd.Read Then
                    person_read = True
                    If Not rd.IsDBNull(0) Then person_name = rd.GetString(0).Trim()
                    If Not rd.IsDBNull(1) Then person_employer = Convert.ToInt32(rd.GetValue(1))
                    If Not rd.IsDBNull(2) Then person_employer_name = rd.GetString(2).Trim()
                    If Not rd.IsDBNull(3) Then person_employer_address1 = rd.GetString(3).Trim()
                    If Not rd.IsDBNull(4) Then person_employer_address2 = rd.GetString(4).Trim()
                    If Not rd.IsDBNull(5) Then person_employer_address3 = rd.GetString(5).Trim()
                End If

            Catch ex As SqlClient.SqlException
                DebtPlus.UI.Common.ErrorHandling.HandleErrors(ex, "Error reading intake_people")

            Finally
                System.Windows.Forms.Cursor.Current = current_cursor
                If rd IsNot Nothing Then rd.Dispose()
                If cn IsNot Nothing Then cn.Dispose()
            End Try

            ' Fill in the required values
            client_name.Text = person_name
            Dim sb As New System.Text.StringBuilder
            If person_employer_name <> String.Empty Then
                sb.Append(Environment.NewLine)
                sb.Append(person_employer_name)
            End If

            If person_employer_address1 <> String.Empty Then
                sb.Append(Environment.NewLine)
                sb.Append(person_employer_address1)
            End If

            If person_employer_address2 <> String.Empty Then
                sb.Append(Environment.NewLine)
                sb.Append(person_employer_address2)
            End If

            If person_employer_address3 <> String.Empty Then
                sb.Append(Environment.NewLine)
                sb.Append(person_employer_address3)
            End If

            If sb.Length > 0 Then sb.Remove(0, 2)
            employer.Text = sb.ToString()

            ' If there is an employer then make everything "grey".
            If person_employer > 0 Then
                NoEmployer.Checked = False
                NoEmployer.Enabled = False
                ' Button_Update.Enabled = False
            Else
                ' The employer is not specified.
                NoEmployer.Checked = False
                NoEmployer.Enabled = True
                ' Button_Update.Enabled = False

                ' If there is no name then don't bother requiring that one be given.
                If person_employer_name.Length = 0 Then
                    NoEmployer.Checked = True
                    NoEmployer.Enabled = False

                    ' Enable the button to update the employer if one is not present
                    'ElseIf person_employer <= 0 Then
                    'Button_Update.Enabled = True
                End If
            End If
        End Sub

        Private Sub NoEmployer_EditValueChanged(ByVal sender As Object, ByVal e As EventArgs)
            RaiseEvent DataChanged(Me, e)
        End Sub
    End Class
End Namespace
