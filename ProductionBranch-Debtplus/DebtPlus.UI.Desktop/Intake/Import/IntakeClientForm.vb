#Region "Copyright 2000-2012 DebtPlus, L.L.C."
'{*******************************************************************}
'{                                                                   }
'{       DebtPlus Debt Management System                             }
'{                                                                   }
'{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
'{       ALL RIGHTS RESERVED                                         }
'{                                                                   }
'{   The entire contents of this file is protected by U.S. and       }
'{   International Copyright Laws. Unauthorized reproduction,        }
'{   reverse-engineering, and distribution of all or any portion of  }
'{   the code contained in this file is strictly prohibited and may  }
'{   result in severe civil and criminal penalties and will be       }
'{   prosecuted to the maximum extent possible under the law.        }
'{                                                                   }
'{   RESTRICTIONS                                                    }
'{                                                                   }
'{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
'{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
'{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
'{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
'{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
'{                                                                   }
'{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
'{   ADDITIONAL RESTRICTIONS.                                        }
'{                                                                   }
'{*******************************************************************}

#End Region

Option Compare Binary
Option Explicit On
Option Strict On

Imports DebtPlus.Data.Forms
Imports System.Data.SqlClient
Imports DebtPlus.Reports
Imports DebtPlus.Interfaces.Reports
Imports System.Windows.Forms

Namespace Intake.Import
    Friend Class IntakeClientForm
        Inherits DebtPlusForm

        Private ReadOnly drv As DataRowView

        Public Sub New()
            MyBase.New()
            InitializeComponent()
        End Sub

        Public Sub New(ByVal DataRow As DataRowView)
            MyClass.New()
            drv = DataRow
            AddHandler Me.Load, AddressOf IntakeClientForm_Load
            AddHandler IntakePeople1.DataChanged, AddressOf IntakePeople1_DataChanged
            AddHandler Button_View.Click, AddressOf Button_View_Click
            AddHandler Button_Import.Click, AddressOf Button_Import_Click
            AddHandler IntakePeople2.DataChanged, AddressOf IntakePeople1_DataChanged
        End Sub

#Region " Windows Form Designer generated code "

        'Form overrides dispose to clean up the component list.
        Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
            If disposing Then
                If components IsNot Nothing Then
                    components.Dispose()
                End If
            End If
            MyBase.Dispose(disposing)
        End Sub

        'Required by the Windows Form Designer
        Private components As System.ComponentModel.IContainer

        'NOTE: The following procedure is required by the Windows Form Designer
        'It can be modified using the Windows Form Designer.  
        'Do not modify it using the code editor.
        Friend WithEvents NotesList1 As IntakeNotesList
        Friend WithEvents IntakePeople1 As DebtPlus.UI.Desktop.Intake.Import.IntakePeople
        Friend WithEvents IntakePeople2 As DebtPlus.UI.Desktop.Intake.Import.IntakePeople
        Friend WithEvents Button_Import As DevExpress.XtraEditors.SimpleButton
        Friend WithEvents LabelControl1 As DevExpress.XtraEditors.LabelControl
        Friend WithEvents LabelControl2 As DevExpress.XtraEditors.LabelControl
        Friend WithEvents Button_View As DevExpress.XtraEditors.SimpleButton
        Friend WithEvents Button_Cancel As DevExpress.XtraEditors.SimpleButton
        Friend WithEvents LabelControl3 As DevExpress.XtraEditors.LabelControl
        Friend WithEvents LabelControl4 As DevExpress.XtraEditors.LabelControl
        Friend WithEvents intake_client_id As DevExpress.XtraEditors.LabelControl
        Friend WithEvents intake_id As DevExpress.XtraEditors.LabelControl
        Friend WithEvents submitted_services As DevExpress.XtraEditors.LabelControl

        <System.Diagnostics.DebuggerStepThrough()>
        Private Sub InitializeComponent()
            Me.NotesList1 = New DebtPlus.UI.Desktop.Intake.Import.IntakeNotesList
            Me.IntakePeople1 = New DebtPlus.UI.Desktop.Intake.Import.IntakePeople
            Me.IntakePeople2 = New DebtPlus.UI.Desktop.Intake.Import.IntakePeople
            Me.Button_Import = New DevExpress.XtraEditors.SimpleButton
            Me.LabelControl1 = New DevExpress.XtraEditors.LabelControl
            Me.LabelControl2 = New DevExpress.XtraEditors.LabelControl
            Me.Button_View = New DevExpress.XtraEditors.SimpleButton
            Me.Button_Cancel = New DevExpress.XtraEditors.SimpleButton
            Me.LabelControl3 = New DevExpress.XtraEditors.LabelControl
            Me.LabelControl4 = New DevExpress.XtraEditors.LabelControl
            Me.intake_client_id = New DevExpress.XtraEditors.LabelControl
            Me.intake_id = New DevExpress.XtraEditors.LabelControl
            Me.submitted_services = New DevExpress.XtraEditors.LabelControl
            CType(Me.DxErrorProvider1, System.ComponentModel.ISupportInitialize).BeginInit()
            Me.SuspendLayout()
            '
            'NotesList1
            '
            Me.NotesList1.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                                           Or System.Windows.Forms.AnchorStyles.Left) _
                                          Or System.Windows.Forms.AnchorStyles.Right), 
                                         System.Windows.Forms.AnchorStyles)
            Me.NotesList1.Location = New System.Drawing.Point(0, 256)
            Me.NotesList1.Name = "NotesList1"
            Me.NotesList1.Size = New System.Drawing.Size(688, 152)
            Me.NotesList1.TabIndex = 0
            '
            'IntakePeople1
            '
            Me.IntakePeople1.Location = New System.Drawing.Point(8, 80)
            Me.IntakePeople1.Name = "IntakePeople1"
            Me.IntakePeople1.person = 1
            Me.IntakePeople1.Size = New System.Drawing.Size(296, 176)
            Me.IntakePeople1.TabIndex = 1
            '
            'IntakePeople2
            '
            Me.IntakePeople2.Location = New System.Drawing.Point(304, 80)
            Me.IntakePeople2.Name = "IntakePeople2"
            Me.IntakePeople2.person = 2
            Me.IntakePeople2.Size = New System.Drawing.Size(296, 176)
            Me.IntakePeople2.TabIndex = 2
            '
            'Button_Import
            '
            Me.Button_Import.Location = New System.Drawing.Point(608, 64)
            Me.Button_Import.Name = "Button_Import"
            Me.Button_Import.Size = New System.Drawing.Size(75, 23)
            Me.Button_Import.TabIndex = 3
            Me.Button_Import.Text = "Import"
            '
            'LabelControl1
            '
            Me.LabelControl1.Appearance.Font = New System.Drawing.Font("Tahoma", 14.0!, System.Drawing.FontStyle.Bold)
            Me.LabelControl1.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center
            Me.LabelControl1.Location = New System.Drawing.Point(8, 56)
            Me.LabelControl1.Name = "LabelControl1"
            Me.LabelControl1.Size = New System.Drawing.Size(90, 23)
            Me.LabelControl1.TabIndex = 4
            Me.LabelControl1.Text = "Applicant"
            '
            'LabelControl2
            '
            Me.LabelControl2.Appearance.Font = New System.Drawing.Font("Tahoma", 14.0!, System.Drawing.FontStyle.Bold)
            Me.LabelControl2.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center
            Me.LabelControl2.Location = New System.Drawing.Point(304, 56)
            Me.LabelControl2.Name = "LabelControl2"
            Me.LabelControl2.Size = New System.Drawing.Size(123, 23)
            Me.LabelControl2.TabIndex = 5
            Me.LabelControl2.Text = "Co-Applicant"
            '
            'Button_View
            '
            Me.Button_View.Location = New System.Drawing.Point(608, 32)
            Me.Button_View.Name = "Button_View"
            Me.Button_View.Size = New System.Drawing.Size(75, 23)
            Me.Button_View.TabIndex = 6
            Me.Button_View.Text = "View"
            '
            'Button_Cancel
            '
            Me.Button_Cancel.DialogResult = System.Windows.Forms.DialogResult.Cancel
            Me.Button_Cancel.Location = New System.Drawing.Point(608, 96)
            Me.Button_Cancel.Name = "Button_Cancel"
            Me.Button_Cancel.Size = New System.Drawing.Size(75, 23)
            Me.Button_Cancel.TabIndex = 7
            Me.Button_Cancel.Text = "Cancel"
            '
            'LabelControl3
            '
            Me.LabelControl3.Location = New System.Drawing.Point(8, 8)
            Me.LabelControl3.Name = "LabelControl3"
            Me.LabelControl3.Size = New System.Drawing.Size(49, 13)
            Me.LabelControl3.TabIndex = 8
            Me.LabelControl3.Text = "Intake ID:"
            '
            'LabelControl4
            '
            Me.LabelControl4.Location = New System.Drawing.Point(8, 22)
            Me.LabelControl4.Name = "LabelControl4"
            Me.LabelControl4.Size = New System.Drawing.Size(65, 13)
            Me.LabelControl4.TabIndex = 9
            Me.LabelControl4.Text = "Intake Client:"
            '
            'intake_client_id
            '
            Me.intake_client_id.Appearance.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold)
            Me.intake_client_id.Location = New System.Drawing.Point(88, 24)
            Me.intake_client_id.Name = "intake_client_id"
            Me.intake_client_id.Size = New System.Drawing.Size(0, 13)
            Me.intake_client_id.TabIndex = 11
            '
            'intake_id
            '
            Me.intake_id.Appearance.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold)
            Me.intake_id.Location = New System.Drawing.Point(88, 8)
            Me.intake_id.Name = "intake_id"
            Me.intake_id.Size = New System.Drawing.Size(0, 13)
            Me.intake_id.TabIndex = 10
            '
            'submitted_services
            '
            Me.submitted_services.Appearance.ForeColor = System.Drawing.Color.Red
            Me.submitted_services.Location = New System.Drawing.Point(232, 8)
            Me.submitted_services.Name = "submitted_services"
            Me.submitted_services.Size = New System.Drawing.Size(258, 13)
            Me.submitted_services.TabIndex = 12
            Me.submitted_services.Text = "Client submitted Agreement for Services electronically"
            Me.submitted_services.Visible = False
            '
            'IntakeClientForm
            '
            Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
            Me.CancelButton = Me.Button_Cancel
            Me.ClientSize = New System.Drawing.Size(688, 406)
            Me.Controls.Add(Me.submitted_services)
            Me.Controls.Add(Me.intake_client_id)
            Me.Controls.Add(Me.intake_id)
            Me.Controls.Add(Me.LabelControl4)
            Me.Controls.Add(Me.LabelControl3)
            Me.Controls.Add(Me.Button_Cancel)
            Me.Controls.Add(Me.Button_View)
            Me.Controls.Add(Me.LabelControl2)
            Me.Controls.Add(Me.LabelControl1)
            Me.Controls.Add(Me.Button_Import)
            Me.Controls.Add(Me.IntakePeople2)
            Me.Controls.Add(Me.IntakePeople1)
            Me.Controls.Add(Me.NotesList1)
            Me.LookAndFeel.UseDefaultLookAndFeel = True
            Me.Name = "IntakeClientForm"
            Me.Text = "IntakeClient"
            CType(Me.DxErrorProvider1, System.ComponentModel.ISupportInitialize).EndInit()
            Me.ResumeLayout(False)
            Me.PerformLayout()
        End Sub

#End Region

        Private intake_client As Int32 = -1

        Private Sub IntakeClientForm_Load(ByVal sender As Object, ByVal e As EventArgs)

            ' Load the controls for the other record components
            intake_client = Convert.ToInt32(drv("intake_client"))
            NotesList1.ReadForm(intake_client)
            IntakePeople1.ReadForm(intake_client)
            IntakePeople2.ReadForm(intake_client)
            Button_Import.Enabled = IntakePeople1.isValid AndAlso IntakePeople2.isValid

            ' Read the client information
            Dim cn As SqlConnection = New SqlConnection(DebtPlus.LINQ.SQLInfoClass.getDefault().ConnectionString)
            Dim rd As SqlDataReader = Nothing
            Dim intake_agreement As Boolean = False
            Dim current_cursor As Cursor = Cursor.Current
            Try
                Cursor.Current = Cursors.WaitCursor
                cn.Open()
                Using cmd As SqlCommand = New SqlCommand
                    With cmd
                        .Connection = cn
                        .CommandText = "SELECT intake_id, intake_agreement FROM view_intake_clients WITH (NOLOCK) WHERE intake_client=@intake_client"
                        .Parameters.Add("@intake_client", SqlDbType.Int).Value = intake_client
                        rd = .ExecuteReader(CommandBehavior.CloseConnection Or CommandBehavior.SingleRow)
                    End With
                End Using

                If rd.Read Then
                    If Not rd.IsDBNull(0) Then intake_id.Text = rd.GetString(0).Trim()
                    If Not rd.IsDBNull(1) Then intake_agreement = Convert.ToInt32(rd.GetValue(1)) <> 0
                End If

            Catch ex As SqlException
                DebtPlus.UI.Common.ErrorHandling.HandleErrors(ex, "Error reading intake_client table")

            Finally
                If rd IsNot Nothing Then rd.Dispose()
                If cn IsNot Nothing Then cn.Dispose()
                Cursor.Current = current_cursor
            End Try

            submitted_services.Visible = intake_agreement
            intake_client_id.Text = intake_client.ToString()
        End Sub

        Private Sub IntakePeople1_DataChanged(ByVal sender As Object, ByVal e As EventArgs)
            Button_Import.Enabled = IntakePeople1.isValid AndAlso IntakePeople2.isValid
        End Sub

        Private Sub Button_View_Click(ByVal sender As Object, ByVal e As EventArgs)
            Dim rpt As New DebtPlus.Reports.Intake.ClientInfo.IntakeClientInfoReport()

            rpt.Parameter_IntakeClient = intake_client
            If CType(rpt, DebtPlus.Interfaces.Reports.IReports).RequestReportParameters = Windows.Forms.DialogResult.OK Then
                rpt.RunReportInSeparateThread()
            End If

        End Sub

        Private Sub Button_Import_Click(ByVal sender As Object, ByVal e As EventArgs)
            Dim cn As SqlConnection = New SqlConnection(DebtPlus.LINQ.SQLInfoClass.getDefault().ConnectionString)
            Dim DebtPlusClient As Int32 = -1
            Try
                cn.Open()
                Using cmd As SqlCommand = New SqlCommand
                    With cmd
                        .Connection = cn
                        .CommandText = "xpr_intake_import"
                        .CommandType = CommandType.StoredProcedure
                        .Parameters.Add("@RETURN_VALUE", SqlDbType.Int).Direction = ParameterDirection.ReturnValue
                        .Parameters.Add("@intake_client", SqlDbType.Int).Value = intake_client
                        .ExecuteNonQuery()
                        DebtPlusClient = Convert.ToInt32(.Parameters(0).Value)
                    End With
                End Using

                cn.Close()

                If DebtPlusClient > 0 Then
                    DebtPlus.Data.Forms.MessageBox.Show(String.Format("The client was imported into DebtPlus ClientId # {0}", String.Format("{0:0000000}", DebtPlusClient)), "Operation Completed", MessageBoxButtons.OK)
                    DialogResult = DialogResult.OK
                End If

            Catch ex As SqlException
                DebtPlus.UI.Common.ErrorHandling.HandleErrors(ex, "Error importing client")

            Finally
                If cn IsNot Nothing Then cn.Dispose()
            End Try
        End Sub
    End Class
End Namespace
