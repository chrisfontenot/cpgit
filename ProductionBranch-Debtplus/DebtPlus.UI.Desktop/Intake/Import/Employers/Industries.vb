#Region "Copyright 2000-2012 DebtPlus, L.L.C."
'{*******************************************************************}
'{                                                                   }
'{       DebtPlus Debt Management System                             }
'{                                                                   }
'{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
'{       ALL RIGHTS RESERVED                                         }
'{                                                                   }
'{   The entire contents of this file is protected by U.S. and       }
'{   International Copyright Laws. Unauthorized reproduction,        }
'{   reverse-engineering, and distribution of all or any portion of  }
'{   the code contained in this file is strictly prohibited and may  }
'{   result in severe civil and criminal penalties and will be       }
'{   prosecuted to the maximum extent possible under the law.        }
'{                                                                   }
'{   RESTRICTIONS                                                    }
'{                                                                   }
'{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
'{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
'{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
'{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
'{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
'{                                                                   }
'{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
'{   ADDITIONAL RESTRICTIONS.                                        }
'{                                                                   }
'{*******************************************************************}
#End Region

Option Compare Binary
Option Explicit On 
Option Strict On

Imports System.Data.SqlClient
Imports System.Windows.Forms

Namespace Intake.Import.Employers
    Partial Class NewForm

        ''' <summary>
        ''' Industry information table
        ''' </summary>
        Public Function IndustriesTable() As DataTable
            Dim tbl As DataTable = ds.Tables(Names.industries)

            ' If the table is not found then read the information from the database
            If tbl Is Nothing Then
                Dim current_cursor As Cursor = Cursor.Current
                Try
                    Cursor.Current = Cursors.WaitCursor
                    SyncLock ds
                        Using cmd As SqlClient.SqlCommand = New SqlCommand
                            With cmd
                                .Connection = New SqlConnection(DebtPlus.LINQ.SQLInfoClass.getDefault().ConnectionString)
                                .CommandText = "SELECT [industry],[description],[ActiveFlag],[Default],[note] FROM industries WITH (NOLOCK)"
                            End With

                            ' Ask the database for the information
                            Using da As New SqlClient.SqlDataAdapter(cmd)
                                da.Fill(ds, Names.industries)
                                tbl = ds.Tables(Names.industries)
                            End Using
                        End Using
                    End SyncLock

                    ' When we do an update, the update results in zero rows being updated. IGNORE IT.
                Catch ex As DBConcurrencyException
                    DebtPlus.Svc.Common.ErrorHandling.HandleErrors(ex)

                    ' Something happend in trying to talk to the database.
                Catch ex As SqlClient.SqlException
                    DebtPlus.UI.Common.ErrorHandling.HandleErrors(ex)

                Finally
                    Cursor.Current = current_cursor
                End Try
            End If

            Return tbl
        End Function
    End Class
End Namespace
