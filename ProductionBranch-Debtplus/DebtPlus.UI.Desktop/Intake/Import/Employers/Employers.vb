#Region "Copyright 2000-2012 DebtPlus, L.L.C."
'{*******************************************************************}
'{                                                                   }
'{       DebtPlus Debt Management System                             }
'{                                                                   }
'{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
'{       ALL RIGHTS RESERVED                                         }
'{                                                                   }
'{   The entire contents of this file is protected by U.S. and       }
'{   International Copyright Laws. Unauthorized reproduction,        }
'{   reverse-engineering, and distribution of all or any portion of  }
'{   the code contained in this file is strictly prohibited and may  }
'{   result in severe civil and criminal penalties and will be       }
'{   prosecuted to the maximum extent possible under the law.        }
'{                                                                   }
'{   RESTRICTIONS                                                    }
'{                                                                   }
'{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
'{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
'{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
'{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
'{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
'{                                                                   }
'{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
'{   ADDITIONAL RESTRICTIONS.                                        }
'{                                                                   }
'{*******************************************************************}
#End Region

Option Compare Binary
Option Explicit On 
Option Strict On

Imports System.Data.SqlClient
Imports System.Windows.Forms

Namespace Intake.Import.Employers
    Partial Class EmployerList

        ''' <summary>
        ''' Update the employer table
        ''' </summary>
        Public Function UpdateEmployerTable() As Int32
            Dim tbl As DataTable = ds.Tables(Names.Employers)
            Dim RowCount As Int32 = 0

            If tbl IsNot Nothing Then
                Dim current_cursor As Cursor = Cursor.Current
                Try
                    Cursor.Current = Cursors.WaitCursor

                    ' Update the employer row with the data
                    Using InsertCmd As SqlClient.SqlCommand = New SqlCommand
                        With InsertCmd
                            .Connection = New SqlConnection(DebtPlus.LINQ.SQLInfoClass.getDefault().ConnectionString)
                            .CommandText = "xpr_insert_employers"
                            .CommandType = CommandType.StoredProcedure
                            .Parameters.Add("@RETURN_VALUE", SqlDbType.Int, 0, "Employer").Direction = ParameterDirection.ReturnValue

                            .Parameters.Add("@Name", SqlDbType.VarChar, 50, "name")
                            .Parameters.Add("@AddressID", SqlDbType.Int, 0, "AddressID")
                            .Parameters.Add("@TelephoneID", SqlDbType.Int, 0, "TelephoneID")
                            .Parameters.Add("@FAXID", SqlDbType.Int, 0, "FAXID")
                            .Parameters.Add("@Industry", SqlDbType.Int, 0, "Industry")
                        End With

                        Using da As New SqlClient.SqlDataAdapter
                            With da
                                .InsertCommand = InsertCmd
                                .AcceptChangesDuringUpdate = True

                                SyncLock ds
                                    RowCount = .Update(tbl)
                                End SyncLock
                            End With
                        End Using
                    End Using

                Catch ex As SqlClient.SqlException
                    DebtPlus.UI.Common.ErrorHandling.HandleErrors(ex, "Error adding employer to employers table")

                Finally
                    Cursor.Current = current_cursor
                End Try
            End If

            Return RowCount
        End Function

        ''' <summary>
        ''' Known list of employers
        ''' </summary>
        Public Function EmployersTable() As DataTable
            Dim tbl As DataTable = ds.Tables(Names.Employers)

            If tbl Is Nothing Then
                Dim current_cursor As Cursor = Cursor.Current
                Try
                    Cursor.Current = Cursors.WaitCursor
                    SyncLock ds
                        Using cmd As SqlClient.SqlCommand = New SqlCommand
                            With cmd
                                .Connection = New SqlConnection(DebtPlus.LINQ.SQLInfoClass.getDefault().ConnectionString)
                                .CommandText = "SELECT [employer],[name],[AddressID],[TelephoneID],[FAXID],[Industry], dbo.format_address_block([AddressID]) AS address FROM employers WHERE [employer]=@employer"
                                .CommandType = CommandType.Text
                            End With

                            ' Ask the database for the information
                            Using da As New SqlClient.SqlDataAdapter(cmd)
                                da.FillSchema(ds, SchemaType.Source, Names.Employers)
                            End Using
                        End Using

                        tbl = ds.Tables(Names.Employers)
                        With tbl
                            If .PrimaryKey.GetUpperBound(0) < 0 Then .PrimaryKey = New DataColumn() {.Columns("employer")}

                            ' Add the two columns to specifiy the grouping for the name
                            If Not .Columns.Contains("firstchar") Then
                                .Columns.Add("firstchar", GetType(String), "substring([name],1,1)")
                            End If
                            If Not .Columns.Contains("grouping") Then
                                .Columns.Add("grouping", GetType(String), "iif([firstchar] IN ('A','B','C','D','E','F','G','H','I','J','K','L','M','N','O','P','Q','R','S','T','U','V','W','X','Y','Z'),firstchar,'?')")
                            End If

                            With .Columns("employer")
                                If Not .AutoIncrement Then
                                    .AutoIncrement = True
                                    .AutoIncrementSeed = -1
                                    .AutoIncrementStep = -1
                                End If
                            End With
                        End With
                    End SyncLock

                    ' When we do an update, the update results in zero rows being updated. IGNORE IT.
                Catch ex As DBConcurrencyException
                    DebtPlus.Svc.Common.ErrorHandling.HandleErrors(ex)

                    ' Something happend in trying to talk to the database.
                Catch ex As SqlClient.SqlException
                    DebtPlus.UI.Common.ErrorHandling.HandleErrors(ex)

                Finally
                    Cursor.Current = current_cursor
                End Try

            End If
            Return tbl
        End Function

        ''' <summary>
        ''' Refresh the employer information
        ''' </summary>
        Public Function ReadEmployersByKey(ByVal KeyID As Char) As DataTable
            Dim tbl As DataTable = ds.Tables(Names.Employers)

            ' If the table is not found then read the information from the database
            Dim current_cursor As Cursor = Cursor.Current
            Try
                Cursor.Current = Cursors.WaitCursor
                SyncLock ds
                    Using cmd As SqlClient.SqlCommand = New SqlCommand
                        With cmd
                            .Connection = New SqlConnection(DebtPlus.LINQ.SQLInfoClass.getDefault().ConnectionString)
                            .CommandText = "SELECT [employer],[name],[AddressID],[TelephoneID],[FAXID],[Industry], dbo.format_address_block([AddressID]) AS address FROM employers WHERE [name]"
                            .CommandText += If(KeyID = "?"c, " NOT LIKE '[A-Z]%'", String.Format(" LIKE '{0}%'", KeyID))
                        End With

                        ' Ask the database for the information
                        Using da As New SqlClient.SqlDataAdapter(cmd)
                            da.Fill(ds, Names.Employers)
                            tbl = ds.Tables(Names.Employers)
                        End Using
                    End Using

                    With tbl
                        If .PrimaryKey.GetUpperBound(0) < 0 Then .PrimaryKey = New DataColumn() {.Columns("employer")}

                        ' Add the two columns to specifiy the grouping for the name
                        If Not .Columns.Contains("firstchar") Then
                            .Columns.Add("firstchar", GetType(String), "substring([name],1,1)")
                        End If
                        If Not .Columns.Contains("grouping") Then
                            .Columns.Add("grouping", GetType(String), "iif([firstchar] IN ('A','B','C','D','E','F','G','H','I','J','K','L','M','N','O','P','Q','R','S','T','U','V','W','X','Y','Z'),firstchar,'?')")
                        End If

                        With .Columns("employer")
                            If Not .AutoIncrement Then
                                .AutoIncrement = True
                                .AutoIncrementSeed = -1
                                .AutoIncrementStep = -1
                            End If
                        End With
                    End With
                End SyncLock

                ' When we do an update, the update results in zero rows being updated. IGNORE IT.
            Catch ex As DBConcurrencyException
                DebtPlus.Svc.Common.ErrorHandling.HandleErrors(ex)

                ' Something happend in trying to talk to the database.
            Catch ex As SqlClient.SqlException
                DebtPlus.UI.Common.ErrorHandling.HandleErrors(ex)

            Finally
                Cursor.Current = current_cursor
            End Try

            Return tbl
        End Function

        ''' <summary>
        ''' Employer information
        ''' </summary>
        Public Function EmployersDataRow(ByVal Employer As Object) As DataRow
            Dim tbl As DataTable = ds.Tables(Names.Employers)

            ' If we found the table then look for the item in the table.
            If tbl IsNot Nothing Then
                Dim row As DataRow = tbl.Rows.Find(Employer)
                If row IsNot Nothing Then Return row
            End If

            ' If the table is not found then read the information from the database
            Dim current_cursor As Cursor = Cursor.Current
            Try
                Cursor.Current = Cursors.WaitCursor
                SyncLock ds
                    Using cmd As SqlClient.SqlCommand = New SqlCommand
                        With cmd
                            .Connection = New SqlConnection(DebtPlus.LINQ.SQLInfoClass.getDefault().ConnectionString)
                            .CommandText = "SELECT [employer],[name],[AddressID],[TelephoneID],[FAXID],[Industry], dbo.format_address_block([AddressID]) AS address FROM employers WHERE [employer]=@employer"
                            .Parameters.Add("@employer", SqlDbType.Int).Value = Employer
                        End With

                        ' Ask the database for the information
                        Using da As New SqlClient.SqlDataAdapter(cmd)
                            da.Fill(ds, Names.Employers)
                        End Using
                    End Using

                    tbl = ds.Tables(Names.Employers)
                    With tbl
                        If .PrimaryKey.GetUpperBound(0) < 0 Then .PrimaryKey = New DataColumn() {.Columns("employer")}

                        ' Add the two columns to specifiy the grouping for the name
                        If Not .Columns.Contains("firstchar") Then
                            .Columns.Add("firstchar", GetType(String), "substring([name],1,1)")
                        End If
                        If Not .Columns.Contains("grouping") Then
                            .Columns.Add("grouping", GetType(String), "iif([firstchar] IN ('A','B','C','D','E','F','G','H','I','J','K','L','M','N','O','P','Q','R','S','T','U','V','W','X','Y','Z'),firstchar,'?')")
                        End If

                        With .Columns("employer")
                            If Not .AutoIncrement Then
                                .AutoIncrement = True
                                .AutoIncrementSeed = -1
                                .AutoIncrementStep = -1
                            End If
                        End With
                    End With
                End SyncLock

                ' When we do an update, the update results in zero rows being updated. IGNORE IT.
            Catch ex As DBConcurrencyException
                DebtPlus.Svc.Common.ErrorHandling.HandleErrors(ex)

                ' Something happend in trying to talk to the database.
            Catch ex As SqlClient.SqlException
                DebtPlus.UI.Common.ErrorHandling.HandleErrors(ex)

            Finally
                Cursor.Current = current_cursor
            End Try

            ' Find the single record in the table that we want
            If tbl IsNot Nothing Then
                Return tbl.Rows.Find(Employer)
            End If
            Return Nothing
        End Function

        ''' <summary>
        ''' Obtain a description for the item
        ''' </summary>
        Public Function GetEmployerDescription(ByVal key As Object) As String
            Dim answer As String
            Dim row As DataRow = EmployersDataRow(key)
            If row IsNot Nothing Then
                answer = DebtPlus.Utils.Nulls.DStr(row("name")).Trim()
            Else
                answer = String.Empty
            End If

            Return answer
        End Function
    End Class
End Namespace
