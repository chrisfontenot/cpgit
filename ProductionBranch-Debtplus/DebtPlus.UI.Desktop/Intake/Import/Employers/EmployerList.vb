#Region "Copyright 2000-2012 DebtPlus, L.L.C."
'{*******************************************************************}
'{                                                                   }
'{       DebtPlus Debt Management System                             }
'{                                                                   }
'{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
'{       ALL RIGHTS RESERVED                                         }
'{                                                                   }
'{   The entire contents of this file is protected by U.S. and       }
'{   International Copyright Laws. Unauthorized reproduction,        }
'{   reverse-engineering, and distribution of all or any portion of  }
'{   the code contained in this file is strictly prohibited and may  }
'{   result in severe civil and criminal penalties and will be       }
'{   prosecuted to the maximum extent possible under the law.        }
'{                                                                   }
'{   RESTRICTIONS                                                    }
'{                                                                   }
'{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
'{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
'{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
'{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
'{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
'{                                                                   }
'{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
'{   ADDITIONAL RESTRICTIONS.                                        }
'{                                                                   }
'{*******************************************************************}
#End Region

Option Compare Binary
Option Explicit On
Option Strict On

Imports DebtPlus.Utils
Imports System.Data.SqlClient
Imports System.Drawing
Imports System.Windows.Forms

Namespace Intake.Import.Employers
    Friend Class EmployerList
        Inherits DevExpress.XtraEditors.XtraUserControl

        Public Event ItemSelected As EventHandler
        Public Event Cancelled As EventHandler
        Private ControlRow As Int32 = -1

        Public Sub New()
            MyBase.New()
            InitializeComponent()
            AddHandler Button_Select.Click, AddressOf Button_Select_Click
            AddHandler Button_Cancel.Click, AddressOf Button_Cancel_Click
            AddHandler Button_New.Click, AddressOf Button_New_Click
            AddHandler GridView1.FocusedRowChanged, AddressOf GridView1_FocusedRowChanged
            AddHandler GridView1.DoubleClick, AddressOf GridView1_DoubleClick
            AddHandler GridView1.MouseDown, AddressOf GridView1_MouseDown
            AddHandler TextEditFilter.EditValueChanging, AddressOf TextEditFilter_EditValueChanging
        End Sub

#Region " Windows Form Designer generated code "

        'UserControl overrides dispose to clean up the component list.
        Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
            If disposing Then
                If components IsNot Nothing Then
                    components.Dispose()
                End If
            End If
            MyBase.Dispose(disposing)
        End Sub

        'Required by the Windows Form Designer
        Private components As System.ComponentModel.IContainer

        'NOTE: The following procedure is required by the Windows Form Designer
        'It can be modified using the Windows Form Designer.  
        'Do not modify it using the code editor.
        Friend WithEvents GridControl1 As DevExpress.XtraGrid.GridControl
        Friend WithEvents GridView1 As DevExpress.XtraGrid.Views.Grid.GridView
        Friend WithEvents GridColumn_Employer As DevExpress.XtraGrid.Columns.GridColumn
        Friend WithEvents GridColumn_Name As DevExpress.XtraGrid.Columns.GridColumn
        Friend WithEvents GridColumn_Address1 As DevExpress.XtraGrid.Columns.GridColumn
        Friend WithEvents GridColumn_Address2 As DevExpress.XtraGrid.Columns.GridColumn
        Friend WithEvents GridColumn_Address3 As DevExpress.XtraGrid.Columns.GridColumn
        Friend WithEvents GridColumn_City As DevExpress.XtraGrid.Columns.GridColumn
        Friend WithEvents GridColumn_State As DevExpress.XtraGrid.Columns.GridColumn
        Friend WithEvents GridColumn_PostalCode As DevExpress.XtraGrid.Columns.GridColumn
        Friend WithEvents GridColumn_Address As DevExpress.XtraGrid.Columns.GridColumn
        Friend WithEvents LabelControl1 As DevExpress.XtraEditors.LabelControl
        Friend WithEvents Button_Select As DevExpress.XtraEditors.SimpleButton
        Friend WithEvents Button_Cancel As DevExpress.XtraEditors.SimpleButton
        Friend WithEvents TextEditFilter As DevExpress.XtraEditors.TextEdit
        Friend WithEvents Button_New As DevExpress.XtraEditors.SimpleButton
        <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
            Me.GridControl1 = New DevExpress.XtraGrid.GridControl()
            Me.GridView1 = New DevExpress.XtraGrid.Views.Grid.GridView()
            Me.GridColumn_Employer = New DevExpress.XtraGrid.Columns.GridColumn()
            Me.GridColumn_Name = New DevExpress.XtraGrid.Columns.GridColumn()
            Me.GridColumn_Address1 = New DevExpress.XtraGrid.Columns.GridColumn()
            Me.GridColumn_Address2 = New DevExpress.XtraGrid.Columns.GridColumn()
            Me.GridColumn_Address3 = New DevExpress.XtraGrid.Columns.GridColumn()
            Me.GridColumn_City = New DevExpress.XtraGrid.Columns.GridColumn()
            Me.GridColumn_State = New DevExpress.XtraGrid.Columns.GridColumn()
            Me.GridColumn_PostalCode = New DevExpress.XtraGrid.Columns.GridColumn()
            Me.GridColumn_Address = New DevExpress.XtraGrid.Columns.GridColumn()
            Me.LabelControl1 = New DevExpress.XtraEditors.LabelControl()
            Me.Button_Select = New DevExpress.XtraEditors.SimpleButton()
            Me.Button_Cancel = New DevExpress.XtraEditors.SimpleButton()
            Me.Button_New = New DevExpress.XtraEditors.SimpleButton()
            Me.TextEditFilter = New DevExpress.XtraEditors.TextEdit()
            CType(Me.GridControl1, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.GridView1, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.TextEditFilter.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
            Me.SuspendLayout()
            '
            'GridControl1
            '
            Me.GridControl1.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                Or System.Windows.Forms.AnchorStyles.Left) _
                Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
            Me.GridControl1.Location = New System.Drawing.Point(0, 0)
            Me.GridControl1.MainView = Me.GridView1
            Me.GridControl1.Name = "GridControl1"
            Me.GridControl1.Size = New System.Drawing.Size(424, 224)
            Me.GridControl1.TabIndex = 0
            Me.GridControl1.ViewCollection.AddRange(New DevExpress.XtraGrid.Views.Base.BaseView() {Me.GridView1})
            '
            'GridView1
            '
            Me.GridView1.Columns.AddRange(New DevExpress.XtraGrid.Columns.GridColumn() {Me.GridColumn_Employer, Me.GridColumn_Name, Me.GridColumn_Address1, Me.GridColumn_Address2, Me.GridColumn_Address3, Me.GridColumn_City, Me.GridColumn_State, Me.GridColumn_PostalCode, Me.GridColumn_Address})
            Me.GridView1.GridControl = Me.GridControl1
            Me.GridView1.Name = "GridView1"
            Me.GridView1.OptionsBehavior.Editable = False
            Me.GridView1.OptionsView.AutoCalcPreviewLineCount = True
            Me.GridView1.OptionsView.EnableAppearanceEvenRow = True
            Me.GridView1.OptionsView.EnableAppearanceOddRow = True
            Me.GridView1.OptionsView.ShowGroupPanel = False
            Me.GridView1.OptionsView.ShowIndicator = False
            Me.GridView1.OptionsView.ShowPreview = True
            Me.GridView1.PreviewFieldName = "address"
            Me.GridView1.SortInfo.AddRange(New DevExpress.XtraGrid.Columns.GridColumnSortInfo() {New DevExpress.XtraGrid.Columns.GridColumnSortInfo(Me.GridColumn_Name, DevExpress.Data.ColumnSortOrder.Ascending)})
            '
            'GridColumn_Employer
            '
            Me.GridColumn_Employer.Caption = "Employer"
            Me.GridColumn_Employer.CustomizationCaption = "Employer ID"
            Me.GridColumn_Employer.Name = "GridColumn_Employer"
            Me.GridColumn_Employer.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.[True]
            '
            'GridColumn_Name
            '
            Me.GridColumn_Name.Caption = "Name"
            Me.GridColumn_Name.CustomizationCaption = "Employer Name"
            Me.GridColumn_Name.FieldName = "name"
            Me.GridColumn_Name.GroupInterval = DevExpress.XtraGrid.ColumnGroupInterval.Alphabetical
            Me.GridColumn_Name.Name = "GridColumn_Name"
            Me.GridColumn_Name.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.[True]
            Me.GridColumn_Name.SortMode = DevExpress.XtraGrid.ColumnSortMode.Value
            Me.GridColumn_Name.Visible = True
            Me.GridColumn_Name.VisibleIndex = 0
            '
            'GridColumn_Address1
            '
            Me.GridColumn_Address1.Caption = "Address Line 1"
            Me.GridColumn_Address1.CustomizationCaption = "Address: Line 1"
            Me.GridColumn_Address1.FieldName = "employer_address1"
            Me.GridColumn_Address1.Name = "GridColumn_Address1"
            Me.GridColumn_Address1.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.[True]
            '
            'GridColumn_Address2
            '
            Me.GridColumn_Address2.Caption = "Address Line 2"
            Me.GridColumn_Address2.CustomizationCaption = "Address: Line 2"
            Me.GridColumn_Address2.FieldName = "address2"
            Me.GridColumn_Address2.Name = "GridColumn_Address2"
            Me.GridColumn_Address2.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.[True]
            '
            'GridColumn_Address3
            '
            Me.GridColumn_Address3.Caption = "Address Line 3"
            Me.GridColumn_Address3.CustomizationCaption = "Address: Line 3"
            Me.GridColumn_Address3.FieldName = "address3"
            Me.GridColumn_Address3.Name = "GridColumn_Address3"
            Me.GridColumn_Address3.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.[True]
            '
            'GridColumn_City
            '
            Me.GridColumn_City.Caption = "City"
            Me.GridColumn_City.CustomizationCaption = "Address: City"
            Me.GridColumn_City.FieldName = "city"
            Me.GridColumn_City.Name = "GridColumn_City"
            Me.GridColumn_City.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.[True]
            '
            'GridColumn_State
            '
            Me.GridColumn_State.Caption = "State"
            Me.GridColumn_State.CustomizationCaption = "Address: State"
            Me.GridColumn_State.FieldName = "state"
            Me.GridColumn_State.Name = "GridColumn_State"
            Me.GridColumn_State.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.[True]
            '
            'GridColumn_PostalCode
            '
            Me.GridColumn_PostalCode.Caption = "Postal Code"
            Me.GridColumn_PostalCode.CustomizationCaption = "Address: Postal Code"
            Me.GridColumn_PostalCode.FieldName = "postalcode"
            Me.GridColumn_PostalCode.Name = "GridColumn_PostalCode"
            Me.GridColumn_PostalCode.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.[True]
            '
            'GridColumn_Address
            '
            Me.GridColumn_Address.Caption = "Address"
            Me.GridColumn_Address.CustomizationCaption = "All Address Lines"
            Me.GridColumn_Address.FieldName = "address"
            Me.GridColumn_Address.Name = "GridColumn_Address"
            Me.GridColumn_Address.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.[True]
            '
            'LabelControl1
            '
            Me.LabelControl1.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
            Me.LabelControl1.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.Horizontal
            Me.LabelControl1.Location = New System.Drawing.Point(16, 244)
            Me.LabelControl1.Name = "LabelControl1"
            Me.LabelControl1.Size = New System.Drawing.Size(28, 13)
            Me.LabelControl1.TabIndex = 2
            Me.LabelControl1.Text = "&Filter:"
            '
            'Button_Select
            '
            Me.Button_Select.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
            Me.Button_Select.Location = New System.Drawing.Point(144, 240)
            Me.Button_Select.Name = "Button_Select"
            Me.Button_Select.Size = New System.Drawing.Size(75, 23)
            Me.Button_Select.TabIndex = 3
            Me.Button_Select.Text = "&Select"
            '
            'Button_Cancel
            '
            Me.Button_Cancel.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
            Me.Button_Cancel.Location = New System.Drawing.Point(232, 240)
            Me.Button_Cancel.Name = "Button_Cancel"
            Me.Button_Cancel.Size = New System.Drawing.Size(75, 23)
            Me.Button_Cancel.TabIndex = 4
            Me.Button_Cancel.Text = "&Cancel"
            '
            'Button_New
            '
            Me.Button_New.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
            Me.Button_New.Location = New System.Drawing.Point(320, 240)
            Me.Button_New.Name = "Button_New"
            Me.Button_New.Size = New System.Drawing.Size(75, 23)
            Me.Button_New.TabIndex = 5
            Me.Button_New.Text = "&New..."
            '
            'TextEditFilter
            '
            Me.TextEditFilter.Anchor = CType(((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left) _
                Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
            Me.TextEditFilter.Location = New System.Drawing.Point(50, 240)
            Me.TextEditFilter.Name = "TextEditFilter"
            Me.TextEditFilter.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[False]
            Me.TextEditFilter.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
            Me.TextEditFilter.Properties.Appearance.Options.UseFont = True
            Me.TextEditFilter.Properties.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
            Me.TextEditFilter.Size = New System.Drawing.Size(78, 20)
            Me.TextEditFilter.TabIndex = 1
            '
            'EmployerList
            '
            Me.Controls.Add(Me.Button_New)
            Me.Controls.Add(Me.Button_Cancel)
            Me.Controls.Add(Me.Button_Select)
            Me.Controls.Add(Me.LabelControl1)
            Me.Controls.Add(Me.GridControl1)
            Me.Controls.Add(Me.TextEditFilter)
            Me.Name = "EmployerList"
            Me.Size = New System.Drawing.Size(424, 280)
            CType(Me.GridControl1, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.GridView1, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.TextEditFilter.Properties, System.ComponentModel.ISupportInitialize).EndInit()
            Me.ResumeLayout(False)
            Me.PerformLayout()

        End Sub

#End Region

        ''' <summary>
        ''' Process the load event on the control
        ''' </summary>
        Public Sub LoadForm(ByVal Employer As Int32)
            Me.Employer = Employer

            With Button_Cancel
                .Tag = "cancel"
            End With

            With Button_New
                .Tag = "new"
            End With

            With Button_Select
                .Tag = "ok"
            End With

            ' Mark the current employer in the list
            Dim StyleFormatCondition1 As DevExpress.XtraGrid.StyleFormatCondition = New DevExpress.XtraGrid.StyleFormatCondition
            With StyleFormatCondition1
                With .Appearance
                    .BackColor = Color.Navy
                    .Font = New Font("Tahoma", 8.25!, FontStyle.Bold)
                    .ForeColor = Color.White
                    .Options.UseBackColor = True
                    .Options.UseFont = True
                    .Options.UseForeColor = True
                End With
                .ApplyToRow = True
                .Column = GridColumn_Employer
                .Expression = String.Format("[employer]={0:f0}", Employer)
                .Condition = DevExpress.XtraGrid.FormatConditionEnum.Expression
            End With
            GridView1.FormatConditions.Add(StyleFormatCondition1)

            With GridView1
                .OptionsSelection.EnableAppearanceFocusedRow = False
                .OptionsSelection.EnableAppearanceFocusedCell = True
            End With

            ' Disable the SELECT button
            Button_Select.Enabled = False

            ' Preload the grid with the name
            Dim drv As DataRow = EmployersDataRow(Employer)
            If drv IsNot Nothing Then
                TextEditFilter.Text = DebtPlus.Utils.Nulls.DStr(drv("name"))
                ReadGridView1(TextEditFilter.Text)
                LoadGridView1(TextEditFilter.Text)
            End If

            ' Kill any selected employer from the list if the text filter changes.
            Employer = -1
            Button_Select.Enabled = False
        End Sub

        ReadOnly employerDataSet As New DataSet("employerDataSet")

        Private _Employer As Int32 = -1
        Public Event EmployerChanged As EventHandler

        ''' <summary>
        ''' The employer changed. Trip the event.
        ''' </summary>
        Protected Overridable Sub OnEmployerChanged(ByVal e As EventArgs)
            RaiseEvent EmployerChanged(Me, e)
        End Sub

        ''' <summary>
        ''' Protected function to trip the employer change event.
        ''' </summary>
        Protected Sub PerformEmployerChanged()
            OnEmployerChanged(EventArgs.Empty)
        End Sub

        ''' <summary>
        ''' The current employer ID
        ''' </summary>
        <System.ComponentModel.Browsable(False)> _
        Public Property Employer() As Int32
            Get
                Return _Employer
            End Get
            Set(ByVal Value As Int32)
                If _Employer <> Value Then
                    _Employer = Value
                    PerformEmployerChanged()
                End If
            End Set
        End Property

        ''' <summary>
        ''' Process the SELECT button click
        ''' </summary>
        Private Sub Button_Select_Click(ByVal sender As Object, ByVal e As EventArgs)
            RaiseEvent ItemSelected(Me, EventArgs.Empty)
        End Sub

        ''' <summary>
        ''' Process the CANCEL button click
        ''' </summary>
        Private Sub Button_Cancel_Click(ByVal sender As Object, ByVal e As EventArgs)
            RaiseEvent Cancelled(Me, EventArgs.Empty)
        End Sub

        ''' <summary>
        ''' Process the NEW button click
        ''' </summary>
        Private Sub Button_New_Click(ByVal sender As Object, ByVal e As EventArgs)
            Dim answer As DialogResult

            ' Create a new row for the employer
            Dim tbl As DataTable = EmployersTable()
            Dim vue As DataView = tbl.DefaultView

            Dim drv As DataRowView = vue.AddNew
            drv.BeginEdit()
            Using frm As New NewForm(drv)
                answer = frm.ShowDialog()
            End Using

            ' If the user cancelled the edit then abort the creation event
            If answer <> DialogResult.OK Then
                drv.CancelEdit()
            Else
                drv.EndEdit()
                UpdateEmployerTable()

                ' Find the employer value and complete the dialog
                Employer = Convert.ToInt32(drv("employer"))
                RaiseEvent ItemSelected(Me, EventArgs.Empty)
            End If
        End Sub

        ''' <summary>
        ''' Update the employer when the grid control moves the focus
        ''' </summary>
        Private Sub GridView1_FocusedRowChanged(ByVal sender As Object, ByVal e As DevExpress.XtraGrid.Views.Base.FocusedRowChangedEventArgs)
            Dim gv As DevExpress.XtraGrid.Views.Grid.GridView = CType(sender, DevExpress.XtraGrid.Views.Grid.GridView)
            ControlRow = e.FocusedRowHandle

            ' Find the datarowview from the input tables.
            Dim drv As DataRowView
            If ControlRow >= 0 Then
                drv = CType(gv.GetRow(ControlRow), DataRowView)
            Else
                drv = Nothing
            End If

            ' Ask the user to do something with the selected row
            Employer = -1
            If drv IsNot Nothing Then
                If Not IsNothing(drv("employer")) AndAlso drv("employer") IsNot DBNull.Value Then
                    Employer = Convert.ToInt32(drv("employer"))
                End If
            End If
            Button_Select.Enabled = (Employer > 0)
        End Sub

        ''' <summary>
        ''' Double Click on an item -- select it
        ''' </summary>
        Private Sub GridView1_DoubleClick(ByVal sender As Object, ByVal e As EventArgs)

            ' Find the row targetted as the double-click item
            Dim gv As DevExpress.XtraGrid.Views.Grid.GridView = CType(sender, DevExpress.XtraGrid.Views.Grid.GridView)
            Dim ctl As DevExpress.XtraGrid.GridControl = CType(gv.GridControl, DevExpress.XtraGrid.GridControl)
            Dim hi As DevExpress.XtraGrid.Views.Grid.ViewInfo.GridHitInfo = gv.CalcHitInfo((ctl.PointToClient(Windows.Forms.Control.MousePosition)))

            ControlRow = hi.RowHandle

            ' Find the datarowview from the input tables.
            Dim drv As DataRowView = Nothing
            If ControlRow >= 0 Then
                drv = CType(gv.GetRow(ControlRow), DataRowView)
            End If

            ' If there is a row then select it
            Employer = -1
            If drv IsNot Nothing Then
                If Not IsNothing(drv("employer")) AndAlso drv("employer") IsNot DBNull.Value Then Employer = Convert.ToInt32(drv("employer"))
            End If

            ' If there is an employer then select it
            If Employer > 0 Then
                RaiseEvent ItemSelected(Me, EventArgs.Empty)
            End If
        End Sub

        ''' <summary>
        ''' Process a click event on the row in the grid
        ''' </summary>
        Private Sub GridView1_MouseDown(ByVal sender As Object, ByVal e As MouseEventArgs)
            Dim gv As DevExpress.XtraGrid.Views.Grid.GridView = GridView1
            Dim hi As DevExpress.XtraGrid.Views.Grid.ViewInfo.GridHitInfo = gv.CalcHitInfo(New Point(e.X, e.Y))

            ' Remember the position for the popup menu handler.
            If hi.InRow Then
                ControlRow = hi.RowHandle
            Else
                ControlRow = -1
            End If

            Employer = -1
            If ControlRow >= 0 Then
                Dim drv As DataRowView = CType(GridView1.GetRow(ControlRow), DataRowView)
                If drv IsNot Nothing Then
                    If Not IsNothing(drv("employer")) AndAlso drv("employer") IsNot DBNull.Value Then Employer = Convert.ToInt32(drv("employer"))
                End If
            End If
            Button_Select.Enabled = (Employer > 0)
        End Sub

        Private LastFilterCriteria As String
        Private Sub TextEditFilter_EditValueChanging(sender As Object, e As DevExpress.XtraEditors.Controls.ChangingEventArgs)
            Dim OldValue As String = DebtPlus.Utils.Nulls.DStr(e.OldValue)

            ' If the new value is empty then empty the list
            Dim NewValue As String = DebtPlus.Utils.Nulls.DStr(e.NewValue)
            If NewValue = String.Empty Then
                GridControl1.DataSource = Nothing
            Else

                ' If the old value was empty then ensure that the new one is just not a space
                If OldValue = String.Empty Then
                    If NewValue = " " Then
                        e.Cancel = True
                        Return
                    End If

                    ReadGridView1(NewValue)
                    LastFilterCriteria = NewValue

                    ' Force a reload if the first character differs
                ElseIf LastFilterCriteria <> NewValue.Substring(0, 1) Then
                    ReadGridView1(NewValue)
                    LastFilterCriteria = NewValue
                End If

                LoadGridView1(NewValue)
            End If

            ' Kill any selected employer from the list if the text filter changes.
            Employer = -1
            Button_Select.Enabled = False
        End Sub

        Private Sub ReadGridView1(ByVal NewValue As String)
            Dim SelectionString As String

            ' Retrieve the entries from the database that start with the first letter
            NewValue = NewValue.Substring(0, 1)
            If Char.IsLetter(Convert.ToChar(NewValue)) Then
                SelectionString = String.Format("e.[NAME] LIKE '{0}%'", NewValue)
            Else
                SelectionString = "e.[NAME] NOT LIKE '[A-Z]%'"
            End If

            SelectionString = String.Format("SELECT e.[employer],e.[name],dbo.address_block_5 (dbo.format_Address_Line_1(a.house,a.direction,a.street,a.suffix,a.modifier,a.modifier_value),a.address_line_2,a.address_line_3,dbo.format_city_state_zip(a.city, a.state, a.postalcode),default) as address FROM employers e with (nolock) LEFT OUTER JOIN addresses a ON e.AddressID = a.Address WHERE {0}", SelectionString)
            Using cmd As SqlClient.SqlCommand = New SqlCommand
                With cmd
                    .Connection = New SqlConnection(DebtPlus.LINQ.SQLInfoClass.getDefault().ConnectionString)
                    .CommandText = SelectionString
                    .CommandType = CommandType.Text
                End With

                Dim current_cursor As Cursor = System.Windows.Forms.Cursor.Current
                Try
                    System.Windows.Forms.Cursor.Current = Cursors.WaitCursor
                    Using da As New SqlClient.SqlDataAdapter(cmd)
                        da.Fill(employerDataSet, "employers")
                        With employerDataSet.Tables("employers")
                            .PrimaryKey = New DataColumn() {.Columns("employer")}
                        End With
                    End Using

                Finally
                    System.Windows.Forms.Cursor.Current = current_cursor
                End Try
            End Using
        End Sub

        Private Sub LoadGridView1(ByVal NewValue As String)

            ' Update the filter with the list new employers
            Dim SelectionString As String = String.Format("[name] like '{0}%'", NewValue.Replace("'", "''"))
            Dim vue As DataView = New DataView(employerDataSet.Tables("employers"), SelectionString, "[name]", DataViewRowState.CurrentRows)
            GridControl1.DataSource = vue
        End Sub
    End Class
End Namespace
