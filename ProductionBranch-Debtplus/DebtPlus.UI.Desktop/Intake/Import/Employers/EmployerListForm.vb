#Region "Copyright 2000-2012 DebtPlus, L.L.C."
'{*******************************************************************}
'{                                                                   }
'{       DebtPlus Debt Management System                             }
'{                                                                   }
'{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
'{       ALL RIGHTS RESERVED                                         }
'{                                                                   }
'{   The entire contents of this file is protected by U.S. and       }
'{   International Copyright Laws. Unauthorized reproduction,        }
'{   reverse-engineering, and distribution of all or any portion of  }
'{   the code contained in this file is strictly prohibited and may  }
'{   result in severe civil and criminal penalties and will be       }
'{   prosecuted to the maximum extent possible under the law.        }
'{                                                                   }
'{   RESTRICTIONS                                                    }
'{                                                                   }
'{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
'{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
'{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
'{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
'{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
'{                                                                   }
'{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
'{   ADDITIONAL RESTRICTIONS.                                        }
'{                                                                   }
'{*******************************************************************}
#End Region

Option Compare Binary
Option Explicit On
Option Strict On

Namespace Intake.Import.Employers
    Public Class EmployerListForm
        Inherits DebtPlus.Data.Forms.DebtPlusForm

        Public Employer As Int32 = -1

        ''' <summary>
        ''' Initialize the form
        ''' </summary>
        Public Sub New()
            MyBase.New()
            InitializeComponent()
            AddHandler Me.Load, AddressOf EmployerListForm_Load
            AddHandler EmployerList1.Cancelled, AddressOf EmployerList1_Cancelled
            AddHandler EmployerList1.ItemSelected, AddressOf EmployerList1_ItemSelected
        End Sub

#Region " Windows Form Designer generated code "

        'Form overrides dispose to clean up the component list.
        Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
            If disposing Then
                If components IsNot Nothing Then
                    components.Dispose()
                End If
            End If
            MyBase.Dispose(disposing)
        End Sub

        'Required by the Windows Form Designer
        Private components As System.ComponentModel.IContainer

        'NOTE: The following procedure is required by the Windows Form Designer
        'It can be modified using the Windows Form Designer.  
        'Do not modify it using the code editor.
        Friend WithEvents EmployerList1 As Employers.EmployerList
        <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
            Me.EmployerList1 = New EmployerList()
            CType(Me.DxErrorProvider1, System.ComponentModel.ISupportInitialize).BeginInit()
            Me.SuspendLayout()
            '
            'EmployerList1
            '
            Me.EmployerList1.Dock = System.Windows.Forms.DockStyle.Fill
            Me.EmployerList1.Employer = -1
            Me.EmployerList1.Location = New System.Drawing.Point(0, 0)
            Me.EmployerList1.Name = "EmployerList1"
            Me.EmployerList1.Size = New System.Drawing.Size(416, 278)
            Me.EmployerList1.TabIndex = 0
            '
            'ListForm
            '
            Me.ClientSize = New System.Drawing.Size(416, 278)
            Me.Controls.Add(Me.EmployerList1)
            Me.Name = "ListForm"
            Me.Text = "List of Employers"
            CType(Me.DxErrorProvider1, System.ComponentModel.ISupportInitialize).EndInit()
            Me.ResumeLayout(False)

        End Sub

#End Region

        Private Sub EmployerListForm_Load(ByVal sender As Object, ByVal e As EventArgs)
            EmployerList1.LoadForm(Employer)
        End Sub

        Private Sub EmployerList1_Cancelled(ByVal sender As Object, ByVal e As EventArgs)
            DialogResult = System.Windows.Forms.DialogResult.Cancel
        End Sub

        Private Sub EmployerList1_ItemSelected(ByVal sender As Object, ByVal e As EventArgs)
            Employer = EmployerList1.Employer
            DialogResult = System.Windows.Forms.DialogResult.OK
        End Sub
    End Class
End Namespace
