Option Compare Binary
Option Explicit On 
Option Strict On

Imports System.Windows.Forms

Namespace Creditor.Move
    Friend Class ArgParser
        Inherits DebtPlus.Utils.ArgParserBase

        ''' <summary>
        ''' Delete the source creditor when complete
        ''' </summary>
        Private _DeleteCreditor As Boolean = False
        Public Property DeleteCreditor() As Boolean
            Get
                Return _DeleteCreditor
            End Get
            Set(ByVal Value As Boolean)
                _DeleteCreditor = Value
            End Set
        End Property

        ''' <summary>
        ''' Source (old) creditor
        ''' </summary>
        Private _SourceCreditor As String = String.Empty
        Public Property SourceCreditor() As String
            Get
                Return _SourceCreditor
            End Get
            Set(ByVal Value As String)
                _SourceCreditor = Value
            End Set
        End Property

        ''' <summary>
        ''' New (Target) creditor
        ''' </summary>
        Private _DestinationCreditor As String = String.Empty
        Public Property DestinationCreditor() As String
            Get
                Return _DestinationCreditor
            End Get
            Set(ByVal Value As String)
                _DestinationCreditor = Value
            End Set
        End Property

        ''' <summary>
        ''' Create an instance of our class
        ''' </summary>
        Public Sub New()
            MyBase.New(New String() {"f", "t", "d"})
        End Sub

        ''' <summary>
        ''' Process a switch condition
        ''' </summary>
        Protected Overrides Function OnSwitch(ByVal switchSymbol As String, ByVal switchValue As String) As SwitchStatus
            Dim ss As DebtPlus.Utils.ArgParserBase.SwitchStatus = DebtPlus.Utils.ArgParserBase.SwitchStatus.NoError

            Select Case switchSymbol
                Case "f"
                    SourceCreditor = switchValue
                Case "t"
                    DestinationCreditor = switchValue
                Case "d"
                    DeleteCreditor = True
                Case "?"
                    ss = DebtPlus.Utils.ArgParserBase.SwitchStatus.ShowUsage
                Case Else
                    ss = DebtPlus.Utils.ArgParserBase.SwitchStatus.YesError
            End Select

            Return ss
        End Function

        ''' <summary>
        ''' Generate the command usage information
        ''' </summary>
        Protected Overrides Function OnDoneParse() As DebtPlus.Utils.ArgParserBase.SwitchStatus
            Dim ss As DebtPlus.Utils.ArgParserBase.SwitchStatus = DebtPlus.Utils.ArgParserBase.SwitchStatus.NoError

            If SourceCreditor = String.Empty OrElse DestinationCreditor = String.Empty Then
                With New MoveCreditorForm(Me)
                    If .ShowDialog() <> DialogResult.OK Then ss = DebtPlus.Utils.ArgParserBase.SwitchStatus.Quit
                    .Dispose()
                End With
            End If

            Return ss
        End Function

        ''' <summary>
        ''' Generate the command usage information
        ''' </summary>
        Protected Overrides Sub OnUsage(ByVal errorInfo As String)
            ' deprecated
        End Sub
    End Class
End Namespace
