#Region "Copyright 2000-2012 DebtPlus, L.L.C."
'{*******************************************************************}
'{                                                                   }
'{       DebtPlus Debt Management System                             }
'{                                                                   }
'{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
'{       ALL RIGHTS RESERVED                                         }
'{                                                                   }
'{   The entire contents of this file is protected by U.S. and       }
'{   International Copyright Laws. Unauthorized reproduction,        }
'{   reverse-engineering, and distribution of all or any portion of  }
'{   the code contained in this file is strictly prohibited and may  }
'{   result in severe civil and criminal penalties and will be       }
'{   prosecuted to the maximum extent possible under the law.        }
'{                                                                   }
'{   RESTRICTIONS                                                    }
'{                                                                   }
'{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
'{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
'{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
'{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
'{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
'{                                                                   }
'{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
'{   ADDITIONAL RESTRICTIONS.                                        }
'{                                                                   }
'{*******************************************************************}
#End Region

Option Compare Binary
Option Explicit On 
Option Strict On

Imports DebtPlus.UI.Creditor.Widgets.Controls
Imports DebtPlus.UI.Creditor.Widgets

Namespace Creditor.Move

    Friend Class MoveCreditorForm
        Inherits DebtPlus.Data.Forms.DebtPlusForm

        Public Sub New()
            MyBase.New()

            'This call is required by the Windows Form Designer.
            InitializeComponent()
            AddHandler Load, AddressOf MoveCreditorForm_Load
            AddHandler Closing, AddressOf MoveCreditorForm_Closing
            AddHandler CreditorControl1.CreditorChanged, AddressOf CreditorControl1_CreditorChanged
            AddHandler CreditorControl2.CreditorChanged, AddressOf CreditorControl1_CreditorChanged

            'Add any initialization after the InitializeComponent() call
            CreditorControl1.Text = "Source (old) Creditor ID"
            CreditorControl2.Text = "Target (new) Creditor ID"
        End Sub

        Private ap As ArgParser = Nothing
        Public Sub New(ByVal ap As ArgParser)
            MyClass.New()
            Me.ap = ap
        End Sub

#Region " Windows Form Designer generated code "

        'Form overrides dispose to clean up the component list.
        Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
            If disposing Then
                If Not (components Is Nothing) Then
                    components.Dispose()
                End If
            End If
            MyBase.Dispose(disposing)
        End Sub

        'Required by the Windows Form Designer
        Private components As System.ComponentModel.IContainer

        'NOTE: The following procedure is required by the Windows Form Designer
        'It can be modified using the Windows Form Designer.  
        'Do not modify it using the code editor.
        Friend WithEvents LabelControl1 As DevExpress.XtraEditors.LabelControl
        Friend WithEvents CreditorControl1 As DebtPlus.UI.Desktop.Creditor.Move.CreditorControl
        Friend WithEvents CreditorControl2 As DebtPlus.UI.Desktop.Creditor.Move.CreditorControl
        Friend WithEvents Button_OK As DevExpress.XtraEditors.SimpleButton
        Friend WithEvents Button_Cancel As DevExpress.XtraEditors.SimpleButton
        Friend WithEvents chkDelete As DevExpress.XtraEditors.CheckEdit
        <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
            Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(MoveCreditorForm))
            Me.LabelControl1 = New DevExpress.XtraEditors.LabelControl
            Me.Button_OK = New DevExpress.XtraEditors.SimpleButton
            Me.Button_Cancel = New DevExpress.XtraEditors.SimpleButton
            Me.chkDelete = New DevExpress.XtraEditors.CheckEdit
            Me.CreditorControl2 = New DebtPlus.UI.Desktop.Creditor.Move.CreditorControl
            Me.CreditorControl1 = New DebtPlus.UI.Desktop.Creditor.Move.CreditorControl
            CType(Me.DxErrorProvider1, System.ComponentModel.ISupportInitialize).BeginInit()

            CType(Me.chkDelete.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
            Me.SuspendLayout()

            '
            'LabelControl1
            '
            Me.LabelControl1.Appearance.Options.UseTextOptions = True
            Me.LabelControl1.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
            Me.LabelControl1.Appearance.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Top
            Me.LabelControl1.Appearance.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap
            Me.LabelControl1.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None
            Me.LabelControl1.Dock = System.Windows.Forms.DockStyle.Top
            Me.LabelControl1.Location = New System.Drawing.Point(0, 0)
            Me.LabelControl1.Name = "LabelControl1"
            Me.LabelControl1.Padding = New System.Windows.Forms.Padding(5)
            Me.LabelControl1.Size = New System.Drawing.Size(512, 50)
            Me.LabelControl1.TabIndex = 0
            Me.LabelControl1.Text = resources.GetString("LabelControl1.Text")
            Me.LabelControl1.UseMnemonic = False
            '
            'Button_OK
            '
            Me.Button_OK.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
            Me.Button_OK.DialogResult = System.Windows.Forms.DialogResult.OK
            Me.Button_OK.Enabled = False
            Me.Button_OK.Location = New System.Drawing.Point(424, 72)
            Me.Button_OK.Name = "Button_OK"
            Me.Button_OK.Size = New System.Drawing.Size(75, 23)
            Me.Button_OK.TabIndex = 4
            Me.Button_OK.Text = "&OK"
            '
            'Button_Cancel
            '
            Me.Button_Cancel.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
            Me.Button_Cancel.DialogResult = System.Windows.Forms.DialogResult.Cancel
            Me.Button_Cancel.Location = New System.Drawing.Point(424, 104)
            Me.Button_Cancel.Name = "Button_Cancel"
            Me.Button_Cancel.Size = New System.Drawing.Size(75, 23)
            Me.Button_Cancel.TabIndex = 5
            Me.Button_Cancel.Text = "&Cancel"
            '
            'chkDelete
            '
            Me.chkDelete.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
            Me.chkDelete.Location = New System.Drawing.Point(8, 336)
            Me.chkDelete.Name = "chkDelete"
            Me.chkDelete.Properties.Caption = "Also permanently delete the source creditor from the system."
            Me.chkDelete.Size = New System.Drawing.Size(392, 19)
            Me.chkDelete.TabIndex = 3
            '
            'CreditorControl2
            '
            Me.CreditorControl2.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                        Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
            Me.CreditorControl2.Location = New System.Drawing.Point(8, 200)
            Me.CreditorControl2.Name = "CreditorControl2"
            Me.CreditorControl2.Size = New System.Drawing.Size(408, 128)
            Me.ToolTipController1.SetSuperTip(Me.CreditorControl2, Nothing)
            Me.CreditorControl2.TabIndex = 2
            Me.CreditorControl2.ValidationLevel = CreditorID.ValidationLevelEnum.NotProhibitUse
            '
            'CreditorControl1
            '
            Me.CreditorControl1.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                        Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
            Me.CreditorControl1.Location = New System.Drawing.Point(8, 64)
            Me.CreditorControl1.Name = "CreditorControl1"
            Me.CreditorControl1.Size = New System.Drawing.Size(408, 128)
            Me.ToolTipController1.SetSuperTip(Me.CreditorControl1, Nothing)
            Me.CreditorControl1.TabIndex = 1
            Me.CreditorControl1.ValidationLevel = CreditorID.ValidationLevelEnum.Exists
            '
            'MoveCreditorForm
            '
            Me.AutoScaleBaseSize = New System.Drawing.Size(5, 14)
            Me.ClientSize = New System.Drawing.Size(512, 366)
            Me.Controls.Add(Me.CreditorControl2)
            Me.Controls.Add(Me.CreditorControl1)
            Me.Controls.Add(Me.chkDelete)
            Me.Controls.Add(Me.Button_Cancel)
            Me.Controls.Add(Me.Button_OK)
            Me.Controls.Add(Me.LabelControl1)
            Me.Name = "MoveCreditorForm"
            Me.ToolTipController1.SetSuperTip(Me, Nothing)
            Me.Text = "Move Creditor Information to an NEW Creditor"
            CType(Me.DxErrorProvider1, System.ComponentModel.ISupportInitialize).EndInit()

            CType(Me.chkDelete.Properties, System.ComponentModel.ISupportInitialize).EndInit()
            Me.ResumeLayout(False)

        End Sub

#End Region

        Private Sub MoveCreditorForm_Load(ByVal sender As System.Object, ByVal e As System.EventArgs)

            ' Define the current values from the argument parser
            chkDelete.Checked = ap.DeleteCreditor
            CreditorControl1.CreditorID1.EditValue = ap.SourceCreditor
            CreditorControl2.CreditorID1.EditValue = ap.DestinationCreditor

            ' Enable or disable the OK button based upon the creditor information
            EnableOK()
        End Sub

        Private Sub CreditorControl1_CreditorChanged(ByVal sender As Object, ByVal e As System.EventArgs)
            EnableOK()
        End Sub

        Private Sub EnableOK()
            Dim Enabled As Boolean = False

            Do
                With CreditorControl1
                    If .CreditorID1.EditValue = String.Empty Then Exit Do
                    If .CreditorID1.ErrorText <> String.Empty Then Exit Do
                End With

                With CreditorControl2
                    If .CreditorID1.EditValue = String.Empty Then Exit Do
                    If .CreditorID1.ErrorText = "The creditor IDs may not be the same" Then CreditorControl2.CreditorID1.ErrorText = String.Empty

                    If String.Compare(CreditorControl1.CreditorID1.EditValue, .CreditorID1.EditValue, True) = 0 Then
                        .CreditorID1.ErrorText = "The creditor IDs may not be the same"
                    End If
                    If .CreditorID1.ErrorText <> String.Empty Then Exit Do
                End With

                Enabled = True
                Exit Do
            Loop

            Button_OK.Enabled = Enabled
        End Sub

        Private Sub MoveCreditorForm_Closing(ByVal sender As Object, ByVal e As System.ComponentModel.CancelEventArgs)
            ap.DeleteCreditor = chkDelete.Checked
            ap.SourceCreditor = CreditorControl1.CreditorID1.EditValue
            ap.DestinationCreditor = CreditorControl2.CreditorID1.EditValue
        End Sub
    End Class
End Namespace
