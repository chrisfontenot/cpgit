#Region "Copyright 2000-2012 DebtPlus, L.L.C."
'{*******************************************************************}
'{                                                                   }
'{       DebtPlus Debt Management System                             }
'{                                                                   }
'{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
'{       ALL RIGHTS RESERVED                                         }
'{                                                                   }
'{   The entire contents of this file is protected by U.S. and       }
'{   International Copyright Laws. Unauthorized reproduction,        }
'{   reverse-engineering, and distribution of all or any portion of  }
'{   the code contained in this file is strictly prohibited and may  }
'{   result in severe civil and criminal penalties and will be       }
'{   prosecuted to the maximum extent possible under the law.        }
'{                                                                   }
'{   RESTRICTIONS                                                    }
'{                                                                   }
'{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
'{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
'{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
'{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
'{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
'{                                                                   }
'{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
'{   ADDITIONAL RESTRICTIONS.                                        }
'{                                                                   }
'{*******************************************************************}

#End Region

Option Compare Binary
Option Explicit On
Option Strict On

Imports System.Data.SqlClient
Imports DebtPlus.Interfaces.Desktop

Namespace Creditor.Move

    Public Class Mainline
        Implements IDesktopMainline

        Public Sub OldAppMain(ByVal args() As String) Implements IDesktopMainline.OldAppMain
            Dim ap As New ArgParser
            If ap.Parse(args) Then
                MoveCreditor(ap.SourceCreditor, ap.DestinationCreditor, ap.DeleteCreditor)
            End If
        End Sub

        Public Sub MoveCreditor(ByVal sourceCreditor As String, ByVal destinationCreditor As String, ByVal deleteCreditor As Boolean)
            Dim cn As SqlConnection = New SqlConnection(DebtPlus.LINQ.SQLInfoClass.getDefault().ConnectionString)
            Try
                cn.Open()

                With New SqlCommand
                    .Connection = cn
                    .CommandText = "cmd_Move_Creditor"
                    .CommandType = CommandType.StoredProcedure
                    .Parameters.Add("@src_creditor", SqlDbType.VarChar, 20).Value = sourceCreditor
                    .Parameters.Add("@dest_creditor", SqlDbType.VarChar, 20).Value = destinationCreditor
                    .Parameters.Add("@delete", SqlDbType.Bit).Value = deleteCreditor
                    .ExecuteNonQuery()
                End With

            Catch ex As SqlException
                DebtPlus.UI.Common.ErrorHandling.HandleErrors(ex, "Error moving creditor")

            Finally
                If cn IsNot Nothing AndAlso cn.State <> ConnectionState.Closed Then cn.Close()
            End Try
        End Sub
    End Class

End Namespace