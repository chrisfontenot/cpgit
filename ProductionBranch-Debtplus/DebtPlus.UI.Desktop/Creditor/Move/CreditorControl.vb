#Region "Copyright 2000-2012 DebtPlus, L.L.C."
'{*******************************************************************}
'{                                                                   }
'{       DebtPlus Debt Management System                             }
'{                                                                   }
'{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
'{       ALL RIGHTS RESERVED                                         }
'{                                                                   }
'{   The entire contents of this file is protected by U.S. and       }
'{   International Copyright Laws. Unauthorized reproduction,        }
'{   reverse-engineering, and distribution of all or any portion of  }
'{   the code contained in this file is strictly prohibited and may  }
'{   result in severe civil and criminal penalties and will be       }
'{   prosecuted to the maximum extent possible under the law.        }
'{                                                                   }
'{   RESTRICTIONS                                                    }
'{                                                                   }
'{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
'{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
'{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
'{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
'{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
'{                                                                   }
'{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
'{   ADDITIONAL RESTRICTIONS.                                        }
'{                                                                   }
'{*******************************************************************}
#End Region

Option Compare Binary
Option Explicit On 
Option Strict On

Imports DebtPlus.UI.Creditor.Widgets.Controls
Imports System.Data.SqlClient
Imports DebtPlus.UI.Creditor.Widgets
Imports DebtPlus.Interfaces

Namespace Creditor.Move

    Friend Class CreditorControl
        Inherits DevExpress.XtraEditors.XtraUserControl

        Public Sub New()
            MyBase.New()
            InitializeComponent()
            AddHandler CreditorID1.Validated, AddressOf CreditorID1_Validated
            AddHandler Me.Load, AddressOf CreditorControl_Load
        End Sub

        Public Event CreditorChanged As System.EventHandler

        Public Property ValidationLevel() As CreditorID.ValidationLevelEnum
            Get
                Return CreditorID1.ValidationLevel
            End Get
            Set(ByVal Value As CreditorID.ValidationLevelEnum)
                CreditorID1.ValidationLevel = Value
            End Set
        End Property

        Public Overrides Property Text() As String
            Get
                Return GroupControl1.Text
            End Get
            Set(ByVal Value As String)
                GroupControl1.Text = Value
            End Set
        End Property

#Region " Windows Form Designer generated code "

        'UserControl overrides dispose to clean up the component list.
        Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
            If disposing Then
                If Not (components Is Nothing) Then
                    components.Dispose()
                End If
            End If
            MyBase.Dispose(disposing)
        End Sub

        'Required by the Windows Form Designer
        Private components As System.ComponentModel.IContainer

        'NOTE: The following procedure is required by the Windows Form Designer
        'It can be modified using the Windows Form Designer.  
        'Do not modify it using the code editor.
        Public WithEvents GroupControl1 As DevExpress.XtraEditors.GroupControl
        Public WithEvents CreditorID1 As CreditorID
        Private WithEvents LabelControl2 As DevExpress.XtraEditors.LabelControl
        Private WithEvents LabelControl1 As DevExpress.XtraEditors.LabelControl
        <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
            Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(CreditorControl))
            Me.LabelControl2 = New DevExpress.XtraEditors.LabelControl
            Me.CreditorID1 = New CreditorID
            Me.GroupControl1 = New DevExpress.XtraEditors.GroupControl
            Me.LabelControl1 = New DevExpress.XtraEditors.LabelControl
            CType(Me.CreditorID1.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.GroupControl1, System.ComponentModel.ISupportInitialize).BeginInit()
            Me.GroupControl1.SuspendLayout()
            Me.SuspendLayout()
            '
            'LabelControl2
            '
            Me.LabelControl2.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.Horizontal
            Me.LabelControl2.Location = New System.Drawing.Point(8, 32)
            Me.LabelControl2.Name = "LabelControl2"
            Me.LabelControl2.Size = New System.Drawing.Size(53, 13)
            Me.LabelControl2.TabIndex = 0
            Me.LabelControl2.Text = "Creditor ID"
            '
            'CreditorID1
            '
            Me.CreditorID1.AllowDrop = True
            Me.CreditorID1.EditValue = Nothing
            Me.CreditorID1.Location = New System.Drawing.Point(72, 28)
            Me.CreditorID1.Name = "CreditorID1"
            Me.CreditorID1.Properties.Appearance.Options.UseTextOptions = True
            Me.CreditorID1.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
            Me.CreditorID1.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph, "", -1, True, True, False, DevExpress.Utils.HorzAlignment.Center, CType(resources.GetObject("CreditorID1.Properties.Buttons"), System.Drawing.Image), New DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), "Click here to search for a creditor ID", "search")})
            Me.CreditorID1.Properties.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
            Me.CreditorID1.Properties.Mask.AutoComplete = DevExpress.XtraEditors.Mask.AutoCompleteType.Strong
            Me.CreditorID1.Properties.Mask.BeepOnError = True
            Me.CreditorID1.Properties.Mask.EditMask = "[A-Z]{1,2}[0-9]{4,}"
            Me.CreditorID1.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.RegEx
            Me.CreditorID1.Properties.Mask.UseMaskAsDisplayFormat = True
            Me.CreditorID1.Properties.MaxLength = 10
            Me.CreditorID1.Size = New System.Drawing.Size(100, 20)
            Me.CreditorID1.TabIndex = 1
            Me.CreditorID1.ValidationLevel = CreditorID.ValidationLevelEnum.Exists
            '
            'GroupControl1
            '
            Me.GroupControl1.Controls.Add(Me.LabelControl1)
            Me.GroupControl1.Controls.Add(Me.LabelControl2)
            Me.GroupControl1.Controls.Add(Me.CreditorID1)
            Me.GroupControl1.Dock = System.Windows.Forms.DockStyle.Fill
            Me.GroupControl1.Location = New System.Drawing.Point(0, 0)
            Me.GroupControl1.Name = "GroupControl1"
            Me.GroupControl1.Size = New System.Drawing.Size(480, 150)
            Me.GroupControl1.TabIndex = 0
            Me.GroupControl1.Text = "GroupControl1"
            '
            'LabelControl1
            '
            Me.LabelControl1.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                        Or System.Windows.Forms.AnchorStyles.Left) _
                        Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
            Me.LabelControl1.Appearance.Options.UseTextOptions = True
            Me.LabelControl1.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
            Me.LabelControl1.Appearance.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Top
            Me.LabelControl1.Appearance.TextOptions.WordWrap = DevExpress.Utils.WordWrap.NoWrap
            Me.LabelControl1.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None
            Me.LabelControl1.Location = New System.Drawing.Point(72, 56)
            Me.LabelControl1.Name = "LabelControl1"
            Me.LabelControl1.Size = New System.Drawing.Size(403, 89)
            Me.LabelControl1.TabIndex = 2
            Me.LabelControl1.UseMnemonic = False
            '
            'CreditorControl
            '
            Me.Controls.Add(Me.GroupControl1)
            Me.Name = "CreditorControl"
            Me.Size = New System.Drawing.Size(480, 150)
            CType(Me.CreditorID1.Properties, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.GroupControl1, System.ComponentModel.ISupportInitialize).EndInit()
            Me.GroupControl1.ResumeLayout(False)
            Me.GroupControl1.PerformLayout()
            Me.ResumeLayout(False)

        End Sub

#End Region

        Private Sub CreditorID1_Validated(ByVal sender As Object, ByVal e As System.EventArgs)
            Dim cn As SqlClient.SqlConnection = New SqlConnection(DebtPlus.LINQ.SQLInfoClass.getDefault().ConnectionString)
            Dim rd As SqlClient.SqlDataReader = Nothing
            Dim current_cursor As System.Windows.Forms.Cursor = System.Windows.Forms.Cursor.Current
            Dim sb As New System.Text.StringBuilder

            ' Do not attempt to lookup empty creditor names
            If String.IsNullOrEmpty(CreditorID1.EditValue) Then Return

            Try
                System.Windows.Forms.Cursor.Current = System.Windows.Forms.Cursors.WaitCursor
                cn.Open()
                With New SqlCommand
                    .Connection = cn
                    .CommandText = "xpr_creditor_address_merged"
                    .CommandType = CommandType.StoredProcedure
                    .Parameters.Add("@creditor", SqlDbType.VarChar, 10).Value = CreditorID1.EditValue
                    .Parameters.Add("@type", SqlDbType.VarChar, 1).Value = "P"
                    rd = .ExecuteReader(CommandBehavior.CloseConnection Or CommandBehavior.SingleRow)
                End With

                If rd IsNot Nothing AndAlso rd.Read Then
                    For Each strName As String In New String() {"addr1", "addr2", "addr3", "addr4", "addr5", "addr6", "addr7"}
                        Dim strValue As String = String.Empty
                        If Not rd.IsDBNull(rd.GetOrdinal(strName)) Then strValue = rd.GetString(rd.GetOrdinal(strName))
                        If strValue <> String.Empty Then
                            sb.Append(Environment.NewLine)
                            sb.Append(strValue)
                        End If
                    Next strName

                    If sb.Length > 0 Then sb.Remove(0, 2)
                End If

                ' Save the address information
                LabelControl1.Text = sb.ToString()
                RaiseEvent CreditorChanged(Me, EventArgs.Empty)

            Catch ex As SqlClient.SqlException
                DebtPlus.UI.Common.ErrorHandling.HandleErrors(ex, "Error reading creditor address")

            Finally
                If rd IsNot Nothing AndAlso Not rd.IsClosed Then rd.Close()
                If cn IsNot Nothing AndAlso cn.State <> ConnectionState.Closed Then cn.Close()
            End Try
        End Sub

        Private Sub CreditorControl_Load(ByVal sender As Object, ByVal e As System.EventArgs)
            Dim ToolTipController As DevExpress.Utils.ToolTipController = CType(Me.ParentForm, IForm).GetToolTipController

            CreditorID1.ToolTipController = ToolTipController
            LabelControl2.ToolTipController = ToolTipController
            LabelControl1.ToolTipController = ToolTipController
        End Sub
    End Class
End Namespace
