﻿' We need to use late-binding objects so turn off strict processing.
Option Strict Off

Imports DebtPlus.Utils
Imports System.Reflection
Imports System.Data.SqlClient
Imports DebtPlus.UI.Desktop.Creditor.Balance.Verification.CapitalOne.Report
Imports DebtPlus.Reports
Imports DebtPlus.Interfaces.Reports
Imports System.Windows.Forms

Namespace Creditor.Balance.Verification.CapitalOne

    Partial Class Form1
        Private ThisApplication As Microsoft.Office.Interop.Excel.ApplicationClass = Nothing
        Private ThisWorkbook As Microsoft.Office.Interop.Excel.WorkbookClass = Nothing
        Private ThisSheet As Microsoft.Office.Interop.Excel._Worksheet = Nothing
        Private ThisRange As Microsoft.Office.Interop.Excel.Range = Nothing

        Private Function OpenApplication() As Boolean
            ThisApplication = New Microsoft.Office.Interop.Excel.ApplicationClass
            Return True
        End Function

        Private Sub CloseApplication()
            If ThisApplication IsNot Nothing Then
                ThisApplication.UserControl = True
                ThisApplication = Nothing
            End If
        End Sub

        Private Function QuitApplication() As Boolean
            ThisApplication.Quit()
            ThisApplication = Nothing
            Return True
        End Function

        Private Function SheetCount() As System.Int32
            Return ThisWorkbook.Sheets.Count
        End Function

        Private Function CreateSheet() As Boolean
            ThisWorkbook.Sheets.Add()
            Return True
        End Function

        Private Function ActivateSheet() As Boolean
            ThisSheet.Activate()
            Return True
        End Function

        Private Function GetSheet(ByVal Name As Object) As Boolean

            Try
                ThisSheet = ThisWorkbook.Sheets.Item(Name)

            Catch ex As Exception
                DebtPlus.Svc.Common.ErrorHandling.HandleErrors(ex)
                Return False
            End Try

            Return True
        End Function

        Private Sub SetVisible(ByVal Value As Boolean)
            ThisApplication.Visible = Value
        End Sub

        Private Function GetRange(ByVal CellName As String) As Object
            Return ThisApplication.Range(CellName)
        End Function

        Private Function SetValue(ByRef Rng As Object, ByVal Value As Object) As Boolean
            If IsNumeric(Value) Then
                Rng.Value = Convert.ToDouble(Value)
            Else
                Rng.Value = Value
            End If
            Return True
        End Function

        Private Function OpenWorkbook(ByVal Fname As String) As Boolean
            Try
                ThisWorkbook = ThisApplication.Workbooks.Open(Fname)

            Catch ex As Exception
                DebtPlus.Svc.Common.ErrorHandling.HandleErrors(ex)
                DevExpress.XtraEditors.XtraMessageBox.Show(ex.ToString(), "Error opening input file", MessageBoxButtons.OK, MessageBoxIcon.Error)
                Return False
            End Try

            Return True
        End Function

        Private Function CreateWorkbook() As Boolean
            ThisWorkbook = ThisApplication.Workbooks.Add
            Return True
        End Function

        Private Function SaveWorkbook(ByRef WkBk As Object, ByVal Fname As String) As Boolean
            Try
                WkBk.SaveAs(Fname)

            Catch ex As Exception
                DebtPlus.Svc.Common.ErrorHandling.HandleErrors(ex)
                DevExpress.XtraEditors.XtraMessageBox.Show(ex.ToString(), "Error saving workbook", MessageBoxButtons.OK, MessageBoxIcon.Error)
                Return False
            End Try

            Return True
        End Function

        Private Function CellValue(ByVal RowID As Int32, ByVal ColId As Int32) As String

            ' If there is no location then there is no value.
            If RowID < 0 OrElse ColId < 0 Then Return String.Empty

            ' Create a range to hold the desired cell. We just want one cell in the range.
            Try
                Dim rangeValue As Object = ThisSheet.Cells(RowID + 1, ColId + 1)
                If rangeValue Is Nothing Then
                    Return String.Empty
                End If

                ' Return the string value of the cell.
                Return Convert.ToString(rangeValue.value)
            Catch
                Return String.Empty
            End Try
        End Function
    End Class
End Namespace
