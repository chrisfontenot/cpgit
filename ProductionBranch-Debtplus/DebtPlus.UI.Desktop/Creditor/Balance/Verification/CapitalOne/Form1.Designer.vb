﻿Namespace Creditor.Balance.Verification.CapitalOne

    <Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
    Partial Class Form1
        Inherits DebtPlus.Data.Forms.DebtPlusForm

        'Form overrides dispose to clean up the component list.
        <System.Diagnostics.DebuggerNonUserCode()> _
        Protected Overrides Sub Dispose(ByVal disposing As Boolean)
            Try
                If disposing AndAlso components IsNot Nothing Then
                    components.Dispose()
                End If
            Finally
                MyBase.Dispose(disposing)
            End Try
        End Sub

        'Required by the Windows Form Designer
        Private components As System.ComponentModel.IContainer

        'NOTE: The following procedure is required by the Windows Form Designer
        'It can be modified using the Windows Form Designer.  
        'Do not modify it using the code editor.
        <System.Diagnostics.DebuggerStepThrough()> _
        Private Sub InitializeComponent()
            Me.LabelControl1 = New DevExpress.XtraEditors.LabelControl()
            Me.lbl_client = New DevExpress.XtraEditors.LabelControl()
            Me.lbl_account_number = New DevExpress.XtraEditors.LabelControl()
            Me.LabelControl4 = New DevExpress.XtraEditors.LabelControl()
            Me.lbl_disbursement_amt = New DevExpress.XtraEditors.LabelControl()
            Me.LabelControl6 = New DevExpress.XtraEditors.LabelControl()
            Me.lbl_balance = New DevExpress.XtraEditors.LabelControl()
            Me.LabelControl8 = New DevExpress.XtraEditors.LabelControl()
            Me.lbl_last_disb_date = New DevExpress.XtraEditors.LabelControl()
            Me.LabelControl10 = New DevExpress.XtraEditors.LabelControl()
            Me.lbl_last_disb_amt = New DevExpress.XtraEditors.LabelControl()
            Me.LabelControl12 = New DevExpress.XtraEditors.LabelControl()
            Me.MarqueeProgressBarControl1 = New DevExpress.XtraEditors.MarqueeProgressBarControl()
            CType(Me.DxErrorProvider1, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.MarqueeProgressBarControl1.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
            Me.SuspendLayout()
            '
            'LabelControl1
            '
            Me.LabelControl1.Location = New System.Drawing.Point(12, 12)
            Me.LabelControl1.Name = "LabelControl1"
            Me.LabelControl1.Size = New System.Drawing.Size(27, 13)
            Me.LabelControl1.TabIndex = 0
            Me.LabelControl1.Text = "Client"
            '
            'lbl_client
            '
            Me.lbl_client.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None
            Me.lbl_client.Location = New System.Drawing.Point(117, 12)
            Me.lbl_client.Name = "lbl_client"
            Me.lbl_client.Size = New System.Drawing.Size(163, 13)
            Me.lbl_client.TabIndex = 1
            Me.lbl_client.UseMnemonic = False
            '
            'lbl_account_number
            '
            Me.lbl_account_number.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None
            Me.lbl_account_number.Location = New System.Drawing.Point(117, 31)
            Me.lbl_account_number.Name = "lbl_account_number"
            Me.lbl_account_number.Size = New System.Drawing.Size(163, 13)
            Me.lbl_account_number.TabIndex = 3
            Me.lbl_account_number.UseMnemonic = False
            '
            'LabelControl4
            '
            Me.LabelControl4.Location = New System.Drawing.Point(12, 31)
            Me.LabelControl4.Name = "LabelControl4"
            Me.LabelControl4.Size = New System.Drawing.Size(79, 13)
            Me.LabelControl4.TabIndex = 2
            Me.LabelControl4.Text = "Account Number"
            '
            'lbl_disbursement_amt
            '
            Me.lbl_disbursement_amt.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None
            Me.lbl_disbursement_amt.Location = New System.Drawing.Point(117, 50)
            Me.lbl_disbursement_amt.Name = "lbl_disbursement_amt"
            Me.lbl_disbursement_amt.Size = New System.Drawing.Size(163, 13)
            Me.lbl_disbursement_amt.TabIndex = 5
            Me.lbl_disbursement_amt.UseMnemonic = False
            '
            'LabelControl6
            '
            Me.LabelControl6.Location = New System.Drawing.Point(12, 50)
            Me.LabelControl6.Name = "LabelControl6"
            Me.LabelControl6.Size = New System.Drawing.Size(87, 13)
            Me.LabelControl6.TabIndex = 4
            Me.LabelControl6.Text = "Disbursement Amt"
            '
            'lbl_balance
            '
            Me.lbl_balance.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None
            Me.lbl_balance.Location = New System.Drawing.Point(117, 69)
            Me.lbl_balance.Name = "lbl_balance"
            Me.lbl_balance.Size = New System.Drawing.Size(163, 13)
            Me.lbl_balance.TabIndex = 7
            Me.lbl_balance.UseMnemonic = False
            '
            'LabelControl8
            '
            Me.LabelControl8.Location = New System.Drawing.Point(12, 69)
            Me.LabelControl8.Name = "LabelControl8"
            Me.LabelControl8.Size = New System.Drawing.Size(37, 13)
            Me.LabelControl8.TabIndex = 6
            Me.LabelControl8.Text = "Balance"
            '
            'lbl_last_disb_date
            '
            Me.lbl_last_disb_date.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None
            Me.lbl_last_disb_date.Location = New System.Drawing.Point(117, 88)
            Me.lbl_last_disb_date.Name = "lbl_last_disb_date"
            Me.lbl_last_disb_date.Size = New System.Drawing.Size(163, 13)
            Me.lbl_last_disb_date.TabIndex = 9
            Me.lbl_last_disb_date.UseMnemonic = False
            '
            'LabelControl10
            '
            Me.LabelControl10.Location = New System.Drawing.Point(12, 88)
            Me.LabelControl10.Name = "LabelControl10"
            Me.LabelControl10.Size = New System.Drawing.Size(69, 13)
            Me.LabelControl10.TabIndex = 8
            Me.LabelControl10.Text = "Last Disb Date"
            '
            'lbl_last_disb_amt
            '
            Me.lbl_last_disb_amt.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None
            Me.lbl_last_disb_amt.Location = New System.Drawing.Point(117, 107)
            Me.lbl_last_disb_amt.Name = "lbl_last_disb_amt"
            Me.lbl_last_disb_amt.Size = New System.Drawing.Size(163, 13)
            Me.lbl_last_disb_amt.TabIndex = 11
            Me.lbl_last_disb_amt.UseMnemonic = False
            '
            'LabelControl12
            '
            Me.LabelControl12.Location = New System.Drawing.Point(12, 107)
            Me.LabelControl12.Name = "LabelControl12"
            Me.LabelControl12.Size = New System.Drawing.Size(65, 13)
            Me.LabelControl12.TabIndex = 10
            Me.LabelControl12.Text = "Last Disb Amt"
            '
            'MarqueeProgressBarControl1
            '
            Me.MarqueeProgressBarControl1.Anchor = CType(((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left) _
                Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
            Me.MarqueeProgressBarControl1.EditValue = 0
            Me.MarqueeProgressBarControl1.Location = New System.Drawing.Point(12, 131)
            Me.MarqueeProgressBarControl1.Name = "MarqueeProgressBarControl1"
            Me.MarqueeProgressBarControl1.Size = New System.Drawing.Size(268, 18)
            Me.MarqueeProgressBarControl1.TabIndex = 16
            '
            'Form1
            '
            Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
            Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
            Me.ClientSize = New System.Drawing.Size(292, 159)
            Me.Controls.Add(Me.MarqueeProgressBarControl1)
            Me.Controls.Add(Me.lbl_last_disb_amt)
            Me.Controls.Add(Me.LabelControl12)
            Me.Controls.Add(Me.lbl_last_disb_date)
            Me.Controls.Add(Me.LabelControl10)
            Me.Controls.Add(Me.lbl_balance)
            Me.Controls.Add(Me.LabelControl8)
            Me.Controls.Add(Me.lbl_disbursement_amt)
            Me.Controls.Add(Me.LabelControl6)
            Me.Controls.Add(Me.lbl_account_number)
            Me.Controls.Add(Me.LabelControl4)
            Me.Controls.Add(Me.lbl_client)
            Me.Controls.Add(Me.LabelControl1)
            Me.Name = "Form1"
            Me.Text = "Setting Peregrin Balances"
            CType(Me.DxErrorProvider1, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.MarqueeProgressBarControl1.Properties, System.ComponentModel.ISupportInitialize).EndInit()
            Me.ResumeLayout(False)
            Me.PerformLayout()

        End Sub
        Friend WithEvents LabelControl1 As DevExpress.XtraEditors.LabelControl
        Friend WithEvents lbl_client As DevExpress.XtraEditors.LabelControl
        Friend WithEvents lbl_account_number As DevExpress.XtraEditors.LabelControl
        Friend WithEvents LabelControl4 As DevExpress.XtraEditors.LabelControl
        Friend WithEvents lbl_disbursement_amt As DevExpress.XtraEditors.LabelControl
        Friend WithEvents LabelControl6 As DevExpress.XtraEditors.LabelControl
        Friend WithEvents lbl_balance As DevExpress.XtraEditors.LabelControl
        Friend WithEvents LabelControl8 As DevExpress.XtraEditors.LabelControl
        Friend WithEvents lbl_last_disb_date As DevExpress.XtraEditors.LabelControl
        Friend WithEvents LabelControl10 As DevExpress.XtraEditors.LabelControl
        Friend WithEvents lbl_last_disb_amt As DevExpress.XtraEditors.LabelControl
        Friend WithEvents LabelControl12 As DevExpress.XtraEditors.LabelControl
        Friend WithEvents MarqueeProgressBarControl1 As DevExpress.XtraEditors.MarqueeProgressBarControl
    End Class
End Namespace