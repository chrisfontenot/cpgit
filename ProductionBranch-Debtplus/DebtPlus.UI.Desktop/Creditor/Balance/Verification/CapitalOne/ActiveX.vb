﻿' We need to use late-binding objects so turn off strict processing.
Option Strict Off

Imports System.Reflection

Namespace Creditor.Balance.Verification.CapitalOne

    Partial Class Form1
        Private ThisApplication As Object
        Private ThisWorkbook As Object
        Private ThisSheet As Object

        <System.Security.SecuritySafeCritical()> _
        Private Function OpenApplication() As Boolean

            Dim objClassType As Type = Type.GetTypeFromProgID("Excel.Application")
            If objClassType Is Nothing Then
                Throw New ApplicationException("Unable to load Excel.Application from system")
            End If

            ThisApplication = Activator.CreateInstance(objClassType)
            If ThisApplication Is Nothing Then
                Throw New ApplicationException("Unable to create new Application instance from Excel.Application type")
            End If

            Return True
        End Function

        Private Function QuitApplication() As Boolean
            If ThisApplication IsNot Nothing Then
                Try
                    InvokeMethod(ThisApplication, "Quit")
                    ThisApplication = Nothing
                    Return True

                Catch ex As Exception
                    DebtPlus.UI.Common.ErrorHandling.HandleErrors(ex, "Error quitting application")
                End Try
            End If

            Return False
        End Function

        Private Function ActivateSheet() As Boolean
            Try
                InvokeMethod(ThisSheet, "Activate")
                Return True

            Catch ex As Exception
                DebtPlus.UI.Common.ErrorHandling.HandleErrors(ex, "Error activating sheet")
            End Try

            Return False
        End Function

        Private Function GetSheet(ByVal Name As Object) As Boolean

            Try
                Dim Sheets As Object = GetProperty(ThisWorkbook, "Sheets")
                ThisSheet = GetProperty(Sheets, "Item", Name)
                Return True

            Catch ex As Exception
                DebtPlus.UI.Common.ErrorHandling.HandleErrors(ex, "Error finding sheet")
            End Try

            Return False
        End Function

        Private Sub SetVisible(ByVal Value As Boolean)
            Try
                SetProperty(ThisApplication, "Visible", Value)

            Catch ex As Exception
                DebtPlus.UI.Common.ErrorHandling.HandleErrors(ex, "Error setting visibility")
            End Try
        End Sub

        Private Function OpenWorkbook(ByVal Fname As String) As Boolean
            Try
                Dim Workbooks As Object = GetProperty(ThisApplication, "Workbooks")
                If Workbooks Is Nothing Then
                    Throw New ApplicationException("Error opening workbook list")
                End If

                ThisWorkbook = InvokeMethod(Workbooks, "Open", Fname)
                If ThisWorkbook Is Nothing Then
                    Throw New ApplicationException(String.Format("Error opening workbook ('{0}') by name", Fname))
                End If

                Return True

            Catch ex As Exception
                DebtPlus.UI.Common.ErrorHandling.HandleErrors(ex, "Error opening workbook")
            End Try

            Return False
        End Function

        Private Function CellValue(ByVal RowID As Int32, ByVal ColId As Int32) As String

            ' If there is no location then there is no value.
            If RowID < 0 OrElse ColId < 0 Then Return String.Empty

            ' Create a range to hold the desired cell. We just want one cell in the range.
            Try
                Dim rangeValue As Object = ThisSheet.Cells(RowID + 1, ColId + 1)
                If rangeValue Is Nothing Then
                    Return String.Empty
                End If

                ' Return the string value of the cell.
                Return Convert.ToString(rangeValue.value)
            Catch
                Return String.Empty
            End Try
        End Function

        <System.Security.SecuritySafeCritical()> _
        Private Shared Sub SetProperty(ByVal obj As Object, ByVal sProperty As String, ByVal oValue As Object)
            Dim oParam(0) As Object
            oParam(0) = oValue
            obj.GetType().InvokeMember(sProperty, BindingFlags.SetProperty, Nothing, obj, oParam)
        End Sub

        <System.Security.SecuritySafeCritical()> _
        Private Shared Function GetProperty(ByVal obj As Object, ByVal sProperty As String, ByVal oValue As Object) As Object
            Dim oParam(0) As Object
            oParam(0) = oValue
            Return obj.GetType().InvokeMember(sProperty, BindingFlags.GetProperty, Nothing, obj, oParam)
        End Function

        <System.Security.SecuritySafeCritical()> _
        Private Shared Function GetProperty(ByVal obj As Object, ByVal sProperty As String) As Object
            Return obj.GetType().InvokeMember(sProperty, BindingFlags.GetProperty, Nothing, obj, Nothing)
        End Function

        <System.Security.SecuritySafeCritical()> _
        Private Shared Function InvokeMethod(ByVal obj As Object, ByVal sProperty As String, ByVal oValue As Object) As Object
            Dim oParam(0) As Object
            oParam(0) = oValue
            Return obj.GetType().InvokeMember(sProperty, BindingFlags.InvokeMethod, Nothing, obj, oParam)
        End Function

        <System.Security.SecuritySafeCritical()> _
        Private Shared Function InvokeMethod(ByVal obj As Object, ByVal sProperty As String) As Object
            Return obj.GetType().InvokeMember(sProperty, BindingFlags.InvokeMethod, Nothing, obj, Nothing)
        End Function
    End Class
End Namespace
