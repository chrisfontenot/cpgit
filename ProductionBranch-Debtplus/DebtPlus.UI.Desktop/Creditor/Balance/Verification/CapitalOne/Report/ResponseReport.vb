#Region "Copyright 2000-2012 DebtPlus, L.L.C."
'{*******************************************************************}
'{                                                                   }
'{       DebtPlus Debt Management System                             }
'{                                                                   }
'{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
'{       ALL RIGHTS RESERVED                                         }
'{                                                                   }
'{   The entire contents of this file is protected by U.S. and       }
'{   International Copyright Laws. Unauthorized reproduction,        }
'{   reverse-engineering, and distribution of all or any portion of  }
'{   the code contained in this file is strictly prohibited and may  }
'{   result in severe civil and criminal penalties and will be       }
'{   prosecuted to the maximum extent possible under the law.        }
'{                                                                   }
'{   RESTRICTIONS                                                    }
'{                                                                   }
'{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
'{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
'{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
'{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
'{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
'{                                                                   }
'{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
'{   ADDITIONAL RESTRICTIONS.                                        }
'{                                                                   }
'{*******************************************************************}
#End Region
Imports DebtPlus.Reports.Template
Imports System.IO
Imports DebtPlus.Utils

Namespace Creditor.Balance.Verification.CapitalOne.Report

    Friend Class ResponseReport
        Inherits TemplateXtraReportClass

        Public Sub New()
            MyBase.New()

            ' Name of the report file
            Const ReportName As String = "DebtPlus.Reports.Creditor.Balance.Verification.CapitalOne.Responses.repx"

            ' See if there is a report reference that we can use
            Dim UseDefault As Boolean = True
            Dim Fname As String = System.IO.Path.Combine(DebtPlus.Configuration.Config.ReportsDirectory, ReportName)
            Try
                If System.IO.File.Exists(Fname) Then
                    LoadLayout(Fname)
                    UseDefault = False
                End If

            Catch ex As Exception
                DebtPlus.Svc.Common.ErrorHandling.HandleErrors(ex)
            End Try

            ' Load the standard report definition if we need a new item
            If UseDefault Then
                Dim asm As Reflection.Assembly = Reflection.Assembly.GetExecutingAssembly()                                           ' changed
                Using ios As Stream = asm.GetManifestResourceStream(String.Format("DebtPlus.UI.Desktop.{0}", ReportName))              ' changed
                    If ios IsNot Nothing Then LoadLayout(ios)
                End Using
            End If

            ' Set the script references as needed
            ScriptReferences = DebtPlus.Utils.AssemblyTools.GetAllSiblingAssemblies()

            ' Update the subreports as well
            For Each BandPtr As DevExpress.XtraReports.UI.Band In Bands
                For Each CtlPtr As DevExpress.XtraReports.UI.XRControl In BandPtr.Controls
                    Dim Rpt As DevExpress.XtraReports.UI.XRSubreport = TryCast(CtlPtr, DevExpress.XtraReports.UI.XRSubreport)
                    If Rpt IsNot Nothing Then
                        Dim RptSrc As DevExpress.XtraReports.UI.XtraReport = TryCast(Rpt.ReportSource, DevExpress.XtraReports.UI.XtraReport)
                        If RptSrc IsNot Nothing Then
                            RptSrc.ScriptReferences = DebtPlus.Utils.AssemblyTools.GetAllSiblingAssemblies()
                        End If
                    End If
                Next
            Next

            ' Indicate that we support the report filter
            ReportFilter.IsEnabled = True
        End Sub

        ''' <summary>
        ''' Report title
        ''' </summary>
        Public Overrides ReadOnly Property ReportTitle As String
            Get
                Return "Balance Verifications"
            End Get
        End Property
    End Class
End Namespace
