#Region "Copyright 2000-2012 DebtPlus, L.L.C."
'{*******************************************************************}
'{                                                                   }
'{       DebtPlus Debt Management System                             }
'{                                                                   }
'{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
'{       ALL RIGHTS RESERVED                                         }
'{                                                                   }
'{   The entire contents of this file is protected by U.S. and       }
'{   International Copyright Laws. Unauthorized reproduction,        }
'{   reverse-engineering, and distribution of all or any portion of  }
'{   the code contained in this file is strictly prohibited and may  }
'{   result in severe civil and criminal penalties and will be       }
'{   prosecuted to the maximum extent possible under the law.        }
'{                                                                   }
'{   RESTRICTIONS                                                    }
'{                                                                   }
'{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
'{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
'{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
'{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
'{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
'{                                                                   }
'{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
'{   ADDITIONAL RESTRICTIONS.                                        }
'{                                                                   }
'{*******************************************************************}
#End Region

Friend Class ResponseReport

    ''' <summary>
    ''' Create a new instance of our class
    ''' </summary>
    ''' <remarks></remarks>
    Public Sub New()
        MyClass.New(CType(Nothing, DebtPlus.LINQ.SQLInfoClass))
    End Sub

    ''' <summary>
    ''' Create a new instance of our class
    ''' </summary>
    ''' <param name="DatabaseInfo"></param>
    ''' <remarks></remarks>
    Public Sub New(ByVal DatabaseInfo As DebtPlus.LINQ.SQLInfoClass)
        MyBase.New()
        InitializeComponent()
        ReportFilter.IsEnabled = True
    End Sub

    ''' <summary>
    ''' Report title
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Overrides ReadOnly Property ReportTitle As String
        Get
            Return "Balance Verifications"
        End Get
    End Property

    ''' <summary>
    ''' Report sub-title
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Overrides ReadOnly Property ReportSubTitle As String
        Get
            Return If(DebtPlus.Utils.Nulls.v_String(Parameters("ParameterSubtitle").Value), String.Empty)
        End Get
    End Property
End Class
