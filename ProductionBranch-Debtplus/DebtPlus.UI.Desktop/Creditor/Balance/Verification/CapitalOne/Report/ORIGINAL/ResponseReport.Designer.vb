﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class ResponseReport
    Inherits DebtPlus.Reports.Template.TemplateXtraReportClass

    'XtraReport overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Designer
    'It can be modified using the Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.XrPanel1 = New DevExpress.XtraReports.UI.XRPanel()
        Me.XrLabel9 = New DevExpress.XtraReports.UI.XRLabel()
        Me.XrLabel5 = New DevExpress.XtraReports.UI.XRLabel()
        Me.XrLabel1 = New DevExpress.XtraReports.UI.XRLabel()
        Me.XrLabel2 = New DevExpress.XtraReports.UI.XRLabel()
        Me.XrLabel8 = New DevExpress.XtraReports.UI.XRLabel()
        Me.XrLabel4 = New DevExpress.XtraReports.UI.XRLabel()
        Me.XrLabel3 = New DevExpress.XtraReports.UI.XRLabel()
        Me.XrLabel_result = New DevExpress.XtraReports.UI.XRLabel()
        Me.XrLabel_account_number = New DevExpress.XtraReports.UI.XRLabel()
        Me.XrLabel_balance = New DevExpress.XtraReports.UI.XRLabel()
        Me.XrLabel_disbursement_factor = New DevExpress.XtraReports.UI.XRLabel()
        Me.XrLabel_last_disb_date = New DevExpress.XtraReports.UI.XRLabel()
        Me.XrLabel_client = New DevExpress.XtraReports.UI.XRLabel()
        Me.XrLabel_last_disb_amt = New DevExpress.XtraReports.UI.XRLabel()
        Me.XrPanel3 = New DevExpress.XtraReports.UI.XRPanel()
        Me.XrLabel12 = New DevExpress.XtraReports.UI.XRLabel()
        Me.ParameterSubtitle = New DevExpress.XtraReports.Parameters.Parameter()
        CType(Me, System.ComponentModel.ISupportInitialize).BeginInit()
        '
        'PageHeader
        '
        Me.PageHeader.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrPanel3, Me.XrPanel1})
        Me.PageHeader.HeightF = 160.25!
        Me.PageHeader.Controls.SetChildIndex(Me.XrPanel_AgencyAddress, 0)
        Me.PageHeader.Controls.SetChildIndex(Me.XrLabel_Title, 0)
        Me.PageHeader.Controls.SetChildIndex(Me.XrLine_Title, 0)
        Me.PageHeader.Controls.SetChildIndex(Me.XrLabel_Subtitle, 0)
        Me.PageHeader.Controls.SetChildIndex(Me.XrPageInfo1, 0)
        Me.PageHeader.Controls.SetChildIndex(Me.XrPanel1, 0)
        Me.PageHeader.Controls.SetChildIndex(Me.XrPanel3, 0)
        '
        'XrLabel_Title
        '
        Me.XrLabel_Title.StylePriority.UseFont = False
        '
        'XrLabel_Subtitle
        '
        Me.XrLabel_Subtitle.LocationFloat = New DevExpress.Utils.PointFloat(7.999992!, 85.00001!)
        Me.XrLabel_Subtitle.SizeF = New System.Drawing.SizeF(1034.0!, 17.0!)
        '
        'XrPageInfo_PageNumber
        '
        Me.XrPageInfo_PageNumber.LocationFloat = New DevExpress.Utils.PointFloat(917.0!, 8.0!)
        Me.XrPageInfo_PageNumber.StylePriority.UseTextAlignment = False
        '
        'XrPanel_AgencyAddress
        '
        Me.XrPanel_AgencyAddress.LocationFloat = New DevExpress.Utils.PointFloat(742.0!, 0.0!)
        '
        'XRLabel_Agency_Name
        '
        Me.XRLabel_Agency_Name.StylePriority.UseTextAlignment = False
        '
        'XrLabel_Agency_Address3
        '
        Me.XrLabel_Agency_Address3.StylePriority.UseFont = False
        Me.XrLabel_Agency_Address3.StylePriority.UseTextAlignment = False
        '
        'XrLabel_Agency_Address1
        '
        Me.XrLabel_Agency_Address1.StylePriority.UseFont = False
        Me.XrLabel_Agency_Address1.StylePriority.UseTextAlignment = False
        '
        'XrLabel_Agency_Phone
        '
        Me.XrLabel_Agency_Phone.StylePriority.UseFont = False
        Me.XrLabel_Agency_Phone.StylePriority.UseTextAlignment = False
        '
        'XrLabel_Agency_Address2
        '
        Me.XrLabel_Agency_Address2.StylePriority.UseFont = False
        Me.XrLabel_Agency_Address2.StylePriority.UseTextAlignment = False
        '
        'Detail
        '
        Me.Detail.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrLabel_last_disb_amt, Me.XrLabel_last_disb_date, Me.XrLabel_client, Me.XrLabel_account_number, Me.XrLabel_result, Me.XrLabel_disbursement_factor, Me.XrLabel_balance})
        Me.Detail.HeightF = 15.0!
        '
        'XrPanel1
        '
        Me.XrPanel1.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrLabel9, Me.XrLabel5, Me.XrLabel1, Me.XrLabel2, Me.XrLabel8, Me.XrLabel4, Me.XrLabel3})
        Me.XrPanel1.LocationFloat = New DevExpress.Utils.PointFloat(0.0!, 137.0!)
        Me.XrPanel1.Name = "XrPanel1"
        Me.XrPanel1.SizeF = New System.Drawing.SizeF(1040.0!, 17.00001!)
        Me.XrPanel1.StyleName = "XrControlStyle_HeaderPannel"
        '
        'XrLabel9
        '
        Me.XrLabel9.LocationFloat = New DevExpress.Utils.PointFloat(393.75!, 0.9999084!)
        Me.XrLabel9.Name = "XrLabel9"
        Me.XrLabel9.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel9.SizeF = New System.Drawing.SizeF(100.0!, 15.00002!)
        Me.XrLabel9.Text = "AMT"
        '
        'XrLabel5
        '
        Me.XrLabel5.LocationFloat = New DevExpress.Utils.PointFloat(493.7501!, 0.9999847!)
        Me.XrLabel5.Name = "XrLabel5"
        Me.XrLabel5.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel5.SizeF = New System.Drawing.SizeF(76.04163!, 15.0!)
        Me.XrLabel5.Text = "DATE"
        '
        'XrLabel1
        '
        Me.XrLabel1.LocationFloat = New DevExpress.Utils.PointFloat(0.0!, 1.0!)
        Me.XrLabel1.Name = "XrLabel1"
        Me.XrLabel1.Padding = New DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 100.0!)
        Me.XrLabel1.SizeF = New System.Drawing.SizeF(100.0!, 15.0!)
        Me.XrLabel1.StylePriority.UsePadding = False
        Me.XrLabel1.Text = "CLIENT"
        '
        'XrLabel2
        '
        Me.XrLabel2.LocationFloat = New DevExpress.Utils.PointFloat(100.0!, 0.9999847!)
        Me.XrLabel2.Name = "XrLabel2"
        Me.XrLabel2.Padding = New DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 100.0!)
        Me.XrLabel2.SizeF = New System.Drawing.SizeF(193.75!, 15.0!)
        Me.XrLabel2.StylePriority.UsePadding = False
        Me.XrLabel2.StylePriority.UseTextAlignment = False
        Me.XrLabel2.Text = "ACCOUNT"
        Me.XrLabel2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
        '
        'XrLabel8
        '
        Me.XrLabel8.LocationFloat = New DevExpress.Utils.PointFloat(690.6251!, 0.9999847!)
        Me.XrLabel8.Name = "XrLabel8"
        Me.XrLabel8.Padding = New DevExpress.XtraPrinting.PaddingInfo(10, 2, 0, 0, 100.0!)
        Me.XrLabel8.SizeF = New System.Drawing.SizeF(178.5417!, 15.0!)
        Me.XrLabel8.StylePriority.UsePadding = False
        Me.XrLabel8.StylePriority.UseTextAlignment = False
        Me.XrLabel8.Text = "STATUS"
        Me.XrLabel8.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
        '
        'XrLabel4
        '
        Me.XrLabel4.LocationFloat = New DevExpress.Utils.PointFloat(569.7916!, 1.0!)
        Me.XrLabel4.Name = "XrLabel4"
        Me.XrLabel4.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel4.SizeF = New System.Drawing.SizeF(100.0!, 15.0!)
        Me.XrLabel4.Text = "DISB FACTOR"
        '
        'XrLabel3
        '
        Me.XrLabel3.LocationFloat = New DevExpress.Utils.PointFloat(293.75!, 1.0!)
        Me.XrLabel3.Name = "XrLabel3"
        Me.XrLabel3.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel3.SizeF = New System.Drawing.SizeF(100.0!, 15.0!)
        Me.XrLabel3.Text = "BALANCE"
        '
        'XrLabel_result
        '
        Me.XrLabel_result.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "result")})
        Me.XrLabel_result.LocationFloat = New DevExpress.Utils.PointFloat(690.6251!, 0.0!)
        Me.XrLabel_result.Name = "XrLabel_result"
        Me.XrLabel_result.Padding = New DevExpress.XtraPrinting.PaddingInfo(10, 2, 0, 0, 100.0!)
        Me.XrLabel_result.SizeF = New System.Drawing.SizeF(335.8333!, 15.0!)
        Me.XrLabel_result.StylePriority.UsePadding = False
        Me.XrLabel_result.StylePriority.UseTextAlignment = False
        Me.XrLabel_result.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
        '
        'XrLabel_account_number
        '
        Me.XrLabel_account_number.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "account_number")})
        Me.XrLabel_account_number.LocationFloat = New DevExpress.Utils.PointFloat(100.0!, 0.0!)
        Me.XrLabel_account_number.Name = "XrLabel_account_number"
        Me.XrLabel_account_number.Padding = New DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 100.0!)
        Me.XrLabel_account_number.SizeF = New System.Drawing.SizeF(193.75!, 15.0!)
        Me.XrLabel_account_number.StylePriority.UsePadding = False
        Me.XrLabel_account_number.StylePriority.UseTextAlignment = False
        Me.XrLabel_account_number.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
        '
        'XrLabel_balance
        '
        Me.XrLabel_balance.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "balance", "{0:c}")})
        Me.XrLabel_balance.LocationFloat = New DevExpress.Utils.PointFloat(293.75!, 0.0!)
        Me.XrLabel_balance.Name = "XrLabel_balance"
        Me.XrLabel_balance.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel_balance.SizeF = New System.Drawing.SizeF(100.0!, 15.0!)
        Me.XrLabel_balance.StylePriority.UseTextAlignment = False
        Me.XrLabel_balance.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
        '
        'XrLabel_disbursement_factor
        '
        Me.XrLabel_disbursement_factor.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "disbursement_factor", "{0:c}")})
        Me.XrLabel_disbursement_factor.LocationFloat = New DevExpress.Utils.PointFloat(569.7916!, 0.0!)
        Me.XrLabel_disbursement_factor.Name = "XrLabel_disbursement_factor"
        Me.XrLabel_disbursement_factor.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel_disbursement_factor.SizeF = New System.Drawing.SizeF(100.0!, 15.0!)
        Me.XrLabel_disbursement_factor.StylePriority.UseTextAlignment = False
        Me.XrLabel_disbursement_factor.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
        '
        'XrLabel_last_disb_date
        '
        Me.XrLabel_last_disb_date.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "last_disbursement_date", "{0:d}")})
        Me.XrLabel_last_disb_date.LocationFloat = New DevExpress.Utils.PointFloat(493.75!, 0.0!)
        Me.XrLabel_last_disb_date.Name = "XrLabel_last_disb_date"
        Me.XrLabel_last_disb_date.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel_last_disb_date.SizeF = New System.Drawing.SizeF(76.04163!, 15.0!)
        Me.XrLabel_last_disb_date.StylePriority.UseTextAlignment = False
        Me.XrLabel_last_disb_date.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
        '
        'XrLabel_client
        '
        Me.XrLabel_client.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "client", "{0:0000000}")})
        Me.XrLabel_client.LocationFloat = New DevExpress.Utils.PointFloat(0.0!, 0.0!)
        Me.XrLabel_client.Name = "XrLabel_client"
        Me.XrLabel_client.Padding = New DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 100.0!)
        Me.XrLabel_client.SizeF = New System.Drawing.SizeF(100.0!, 15.0!)
        Me.XrLabel_client.StylePriority.UsePadding = False
        Me.XrLabel_client.StylePriority.UseTextAlignment = False
        Me.XrLabel_client.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
        '
        'XrLabel_last_disb_amt
        '
        Me.XrLabel_last_disb_amt.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "last_disbursement_amount", "{0:c}")})
        Me.XrLabel_last_disb_amt.LocationFloat = New DevExpress.Utils.PointFloat(393.75!, 0.0!)
        Me.XrLabel_last_disb_amt.Name = "XrLabel_last_disb_amt"
        Me.XrLabel_last_disb_amt.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel_last_disb_amt.SizeF = New System.Drawing.SizeF(100.0!, 15.0!)
        Me.XrLabel_last_disb_amt.StylePriority.UseTextAlignment = False
        Me.XrLabel_last_disb_amt.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
        '
        'XrPanel3
        '
        Me.XrPanel3.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrLabel12})
        Me.XrPanel3.LocationFloat = New DevExpress.Utils.PointFloat(393.75!, 120.0!)
        Me.XrPanel3.Name = "XrPanel3"
        Me.XrPanel3.SizeF = New System.Drawing.SizeF(176.0417!, 17.0!)
        Me.XrPanel3.StyleName = "XrControlStyle_HeaderPannel"
        '
        'XrLabel12
        '
        Me.XrLabel12.LocationFloat = New DevExpress.Utils.PointFloat(0.0!, 0.9999924!)
        Me.XrLabel12.Name = "XrLabel12"
        Me.XrLabel12.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel12.SizeF = New System.Drawing.SizeF(176.0416!, 15.00002!)
        Me.XrLabel12.StylePriority.UseTextAlignment = False
        Me.XrLabel12.Text = "LAST DISBURSEMENT"
        Me.XrLabel12.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter
        '
        'ParameterSubtitle
        '
        Me.ParameterSubtitle.Description = "Subtitle String"
        Me.ParameterSubtitle.Name = "ParameterSubtitle"
        Me.ParameterSubtitle.Visible = False
        '
        'ResponseReport
        '
        Me.Bands.AddRange(New DevExpress.XtraReports.UI.Band() {Me.TopMarginBand1, Me.Detail, Me.BottomMarginBand1, Me.PageHeader, Me.PageFooter})
        Me.Landscape = True
        Me.PageHeight = 850
        Me.PageWidth = 1100
        Me.Parameters.AddRange(New DevExpress.XtraReports.Parameters.Parameter() {Me.ParameterSubtitle})
        Me.RequestParameters = False
        Me.ScriptLanguage = DevExpress.XtraReports.ScriptLanguage.VisualBasic
        Me.StyleSheet.AddRange(New DevExpress.XtraReports.UI.XRControlStyle() {Me.XrControlStyle_Header, Me.XrControlStyle_Totals, Me.XrControlStyle_HeaderPannel, Me.XrControlStyle_GroupHeader})
        Me.Version = "11.2"
        CType(Me, System.ComponentModel.ISupportInitialize).EndInit()

    End Sub
    Friend WithEvents XrPanel1 As DevExpress.XtraReports.UI.XRPanel
    Friend WithEvents XrLabel5 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrLabel1 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrLabel2 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrLabel8 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrLabel4 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrLabel3 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrLabel_last_disb_date As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrLabel_client As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrLabel_account_number As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrLabel_result As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrLabel_disbursement_factor As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrLabel_balance As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrLabel_last_disb_amt As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrPanel3 As DevExpress.XtraReports.UI.XRPanel
    Friend WithEvents XrLabel12 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrLabel9 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents ParameterSubtitle As DevExpress.XtraReports.Parameters.Parameter
End Class
