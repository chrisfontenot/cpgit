#Region "Copyright 2000-2012 DebtPlus, L.L.C."
'{*******************************************************************}
'{                                                                   }
'{       DebtPlus Debt Management System                             }
'{                                                                   }
'{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
'{       ALL RIGHTS RESERVED                                         }
'{                                                                   }
'{   The entire contents of this file is protected by U.S. and       }
'{   International Copyright Laws. Unauthorized reproduction,        }
'{   reverse-engineering, and distribution of all or any portion of  }
'{   the code contained in this file is strictly prohibited and may  }
'{   result in severe civil and criminal penalties and will be       }
'{   prosecuted to the maximum extent possible under the law.        }
'{                                                                   }
'{   RESTRICTIONS                                                    }
'{                                                                   }
'{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
'{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
'{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
'{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
'{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
'{                                                                   }
'{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
'{   ADDITIONAL RESTRICTIONS.                                        }
'{                                                                   }
'{*******************************************************************}
#End Region

Imports DebtPlus.UI.Desktop.Creditor.Balance.Verification.CapitalOne.Report
Imports System.Windows.Forms

Namespace Creditor.Balance.Verification.CapitalOne
    Friend Class Form1

        Public Sub New()
            MyBase.New()
            InitializeComponent()
            AddHandler Me.Load, AddressOf Form1_Load
        End Sub

        Private ReadOnly ap As ArgParser
        Private ReadOnly ds As New DataSet("ds")
        Private tbl As DataTable

        ' Regular expression for the account number
        Private ReadOnly rx_account_number As New System.Text.RegularExpressions.Regex("\d{11,}", System.Text.RegularExpressions.RegexOptions.Singleline)

        ' Information shared from the parser to the display logic
        Private Balance As Decimal
        Private DisbursementFactor As Decimal
        Private DisbursementAmount As Decimal
        Private Client As Int32
        Private AccountNumber As String
        Private DisbursementDate As DateTime

        Public Sub New(ByVal ap As ArgParser)
            MyClass.New()
            Me.ap = ap
        End Sub

        Private Sub Form1_Load(ByVal sender As Object, ByVal e As EventArgs)
            Dim thrd As New System.Threading.Thread(AddressOf ThreadProcedure)
            thrd.SetApartmentState(Threading.ApartmentState.STA)
            thrd.IsBackground = True
            thrd.Name = "ProcessingThread"
            thrd.Start()
        End Sub

        Private Delegate Sub FormCloseDelegate()
        Private Sub FormClose()
            If InvokeRequired Then
                BeginInvoke(New FormCloseDelegate(AddressOf FormClose))
            Else
                Close()
            End If
        End Sub

        Private Sub ThreadProcedure()

            ' Find the defaults for the items from the app.config file
            Dim configKeys As System.Collections.Specialized.NameValueCollection = DebtPlus.Configuration.Config.GetConfigSettings(DebtPlus.Configuration.Config.ConfigSet.CreditorBalanceVerificationCapitalOne)

            ' Create the update table to hold the update information
            tbl = New DataTable("updates")
            tbl.Columns.Add("client", GetType(Int32))
            tbl.Columns.Add("account_number", GetType(String))
            tbl.Columns.Add("balance", GetType(Decimal))
            tbl.Columns.Add("disbursement_factor", GetType(Decimal))
            tbl.Columns.Add("last_disbursement_date", GetType(DateTime))
            tbl.Columns.Add("last_disbursement_amount", GetType(Decimal))
            tbl.Columns.Add("result", GetType(String))
            tbl.Columns.Add("id", GetType(Int32))

            tbl.Columns("result").AllowDBNull = True

            Dim col As DataColumn = tbl.Columns("id")
            col.AutoIncrement = True
            col.AutoIncrementSeed = 1
            col.AutoIncrementStep = 1
            tbl.PrimaryKey = New DataColumn() {col}

            ds.Tables.Add(tbl)

            Try
                ' Open the excel application
                If OpenApplication() Then
                    Try
                        SetVisible(ap.Visible)

                        ' Process the files and the clients
                        Dim ie As IEnumerator = ap.GetPathnameEnumerator
                        Do While ie.MoveNext
                            Dim Filename As String = Convert.ToString(ie.Current)
                            ProcessFile(Filename)
                        Loop

                        ' Update the database
                        UpdateDatabase()
                        PrintReport()

                    Catch ex As Exception
                        DebtPlus.UI.Common.ErrorHandling.HandleErrors(ex)

                    Finally
                        QuitApplication()   ' Terminate the EXCEL application
                    End Try
                End If

            Catch ex As Exception
                DebtPlus.UI.Common.ErrorHandling.HandleErrors(ex)
            End Try

            ' Close the form when we are completed
            FormClose()
        End Sub

        Private Function MapColumn(ByVal strName As String) As Int32

            ' If there is no string then return the "it is missing" special value
            If String.IsNullOrWhiteSpace(strName) OrElse strName.Length < 1 Then
                Return -1
            End If

            Dim key As String = strName.ToUpper()
            Dim value As Int32 = 0

            ' Handle the case where the column is "aa" ect. The columns need to handle values above 26.
            For charIndex = 0 To key.Length - 1
                Dim chr As Char = key(charIndex)
                Dim id As Int32 = "ABCDEFGHIJKLMNOPQRSTUVWXYZ".IndexOf(chr) + 1
                If id < 1 Then
                    Return -2
                End If
                value = (value * 26) + id
            Next

            ' Return the column "a" as 0, "b" as 1, etc.
            Return value - 1
        End Function

        Private Sub ProcessFile(ByVal Filename As String)

            ' Find the defaults for the items from the app.config file
            Dim configKeys As System.Collections.Specialized.NameValueCollection = DebtPlus.Configuration.Config.GetConfigSettings(DebtPlus.Configuration.Config.ConfigSet.CreditorBalanceVerificationCapitalOne)
            If configKeys Is Nothing Then
                Throw New ApplicationException("Error accessing configuration file")
            End If

            ' Find the columns for the fields
            Dim colClientID As Int32 = MapColumn(configKeys("ClientID"))
            Dim colAccountNumber As Int32 = MapColumn(configKeys("AccoutNumber"))
            Dim colProposalAmt As Int32 = MapColumn(configKeys("ProposalAmt"))
            Dim colBalance As Int32 = MapColumn(configKeys("Balance"))
            Dim colLastPaymentDate As Int32 = MapColumn(configKeys("LastPaymentDate"))
            Dim colLastPaymentAmt As Int32 = MapColumn(configKeys("LastPaymentAmt"))

            If colClientID < 0 OrElse colAccountNumber < 0 OrElse colProposalAmt < -1 OrElse colBalance < 0 OrElse colLastPaymentAmt < -1 OrElse colLastPaymentDate < -1 Then
                DebtPlus.Data.Forms.MessageBox.Show("Invalid configuration file Config/DebtPlus.Creditor.Balance.Verification.CapitalOne.Default.config", "Configuration Error")
                Return
            End If

            ' Open the excel document
            If OpenWorkbook(Filename) AndAlso GetSheet(1) AndAlso ActivateSheet() Then

                ' Process the rows in the sheet
                Dim Previous As Boolean = False
                Dim RowCount As Int32 = Convert.ToInt32(ThisSheet.Rows.Count)
                Client = 0

                For RowNumber As Int32 = 0 To RowCount - 1

                    ' If this row is not processed but the last one was then we have handled all of the items.
                    Dim Current As Boolean = UpdateBalances(CellValue(RowNumber, colClientID), CellValue(RowNumber, colAccountNumber), CellValue(RowNumber, colProposalAmt), CellValue(RowNumber, colBalance), CellValue(RowNumber, colLastPaymentDate), CellValue(RowNumber, colLastPaymentAmt))
                    If Previous AndAlso Not Current Then Exit For
                    Previous = Current
                Next
            End If
        End Sub

        Private Sub DisplayInformation()

            If InvokeRequired Then
                Dim ia As IAsyncResult = BeginInvoke(New MethodInvoker(AddressOf DisplayInformation))
                EndInvoke(ia)

            Else

                ' Display the information on the form
                lbl_client.Text = String.Format("{0:0000000}", Client)
                lbl_account_number.Text = AccountNumber
                lbl_balance.Text = String.Format("{0:c}", Balance)
                lbl_disbursement_amt.Text = String.Format("{0:c}", DisbursementFactor)
                lbl_last_disb_date.Text = DisbursementDate.ToShortDateString
                lbl_last_disb_amt.Text = String.Format("{0:c}", DisbursementAmount)

                Invalidate()
            End If
        End Sub

        Private Function UpdateBalances(ByVal colClientID As String, ByVal colAccountNumber As String, ByVal colDisbursementFactor As String, ByVal colBalance As String, ByVal colDisbursementDate As String, ByVal colDisbursementAmount As String) As Boolean

            ' The account number is just a string. It must be present for all valid rows.
            AccountNumber = colAccountNumber
            If String.IsNullOrEmpty(AccountNumber) OrElse Not rx_account_number.IsMatch(AccountNumber) Then Return False

            ' Save the last parsed client
            If Not String.IsNullOrEmpty(colClientID) Then
                Dim CurrentClient As Int32
                If Int32.TryParse(colClientID, CurrentClient) AndAlso CurrentClient > 0 Then Client = CurrentClient
            End If
            If Client <= 0 Then Return False

            ' Parse the other fields to suitable numeric values
            If Not Decimal.TryParse(colDisbursementFactor, DisbursementFactor) Then Return False
            If Not Decimal.TryParse(colBalance, Balance) Then Return False
            If Not Decimal.TryParse(colDisbursementAmount, DisbursementAmount) Then Return False

            ' These are defaulted to reasonable figures
            If Not Date.TryParse(colDisbursementDate, DisbursementDate) Then DisbursementDate = New DateTime(1900, 1, 1)

            ' Update the information with the parameters
            DisplayInformation()

            ' Add the row to the table
            tbl.Rows.Add(New Object() {Client, AccountNumber, Balance, DisbursementFactor, DisbursementDate, DisbursementAmount})
            Return True
        End Function

        Private Delegate Sub UpdateCompletedDelegate(ByVal ItemCount As Int32)
        Private Sub UpdateCompleted(ByVal ItemCount As Int32)
            If InvokeRequired Then
                Invoke(New UpdateCompletedDelegate(AddressOf UpdateCompleted), New Object() {ItemCount})
            Else
                MarqueeProgressBarControl1.Properties.Stopped = True
                'DebtPlus.Data.Forms.MessageBox.Show(String.Format("{0:n0} debts have been updated.", ItemCount), "Operation Successful", MessageBoxButtons.OK, MessageBoxIcon.Information)
            End If
        End Sub

        Private Sub UpdateDatabase()

            ' Generate the update event
            Try
                Using cm As New DebtPlus.UI.Common.CursorManager()
                    Using cmd As New System.Data.SqlClient.SqlCommand()
                        cmd.Connection = New System.Data.SqlClient.SqlConnection(DebtPlus.LINQ.SQLInfoClass.getDefault().ConnectionString)
                        cmd.CommandText = "xpr_Peregrin_Balance_Update"
                        cmd.CommandType = System.Data.CommandType.StoredProcedure

                        cmd.Parameters.Add("@client_text", System.Data.SqlDbType.Int, 0, "client")
                        cmd.Parameters.Add("@account_number", System.Data.SqlDbType.VarChar, 80, "account_number")
                        cmd.Parameters.Add("@disbursement_factor_text", System.Data.SqlDbType.Decimal, 0, "disbursement_factor")
                        cmd.Parameters.Add("@balance_text", System.Data.SqlDbType.Decimal, 0, "balance")
                        cmd.Parameters.Add("@last_disbursement_date_text", System.Data.SqlDbType.DateTime, 0, "last_disbursement_date")
                        cmd.Parameters.Add("@last_disbursement_amount_text", System.Data.SqlDbType.Decimal, 0, "last_disbursement_amount")
                        cmd.Parameters.Add("@result", System.Data.SqlDbType.VarChar, 80, "result").Direction = ParameterDirection.Output
                        cmd.Parameters.Add("@creditor", System.Data.SqlDbType.VarChar, 10).Value = ap.Creditor

                        Using da As New System.Data.SqlClient.SqlDataAdapter() With {.InsertCommand = cmd}
                            UpdateCompleted(da.Update(tbl))
                        End Using
                    End Using
                End Using

            Catch ex As SqlClient.SqlException
                DebtPlus.UI.Common.ErrorHandling.HandleErrors(ex, "Error inserting balance updates")
            End Try
        End Sub

        ''' <summary>
        ''' Generate and display the resulting report
        ''' </summary>
        ''' <remarks></remarks>
        Private Sub PrintReport()
            Using rpt As New ResponseReport()
                AddHandler rpt.BeforePrint, AddressOf SetReportDataSource
                AddHandler rpt.GetSubTitleString, AddressOf GetSubTitleString
                Using frm As New DebtPlus.Reports.Template.PrintPreviewForm(rpt)
                    frm.ShowDialog()
                End Using
                RemoveHandler rpt.BeforePrint, AddressOf SetReportDataSource
                RemoveHandler rpt.GetSubTitleString, AddressOf GetSubTitleString
            End Using
        End Sub

        Private Sub GetSubTitleString(ByVal sender As Object, ByVal e As DebtPlus.Reports.Template.TitleStringEventArgs)
            e.Title = String.Format("Items for creditor ID {0}", ap.Creditor)
        End Sub

        Private Sub SetReportDataSource(ByVal Sender As Object, ByVal e As System.EventArgs)
            Dim rpt As DevExpress.XtraReports.UI.XtraReport = CType(Sender, DevExpress.XtraReports.UI.XtraReport)
            rpt.DataSource = New System.Data.DataView(tbl, CType(rpt, DebtPlus.Interfaces.Reports.IReportFilter).ReportFilter.ViewFilter, String.Empty, DataViewRowState.CurrentRows)
            For Each calc As DevExpress.XtraReports.UI.CalculatedField In rpt.CalculatedFields
                calc.Assign(rpt.DataSource, rpt.DataMember)
            Next
        End Sub
    End Class
End Namespace
