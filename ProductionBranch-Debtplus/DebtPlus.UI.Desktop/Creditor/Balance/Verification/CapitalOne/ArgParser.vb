Option Compare Binary
Option Explicit On 
Option Strict On

Imports System.IO
Imports DebtPlus.Reports.Template.Forms
Imports System.Windows.Forms

Namespace Creditor.Balance.Verification.CapitalOne
    Friend Class ArgParser
        Inherits DebtPlus.Utils.ArgParserBase

        Private pathnames As New ArrayList()
        Private privateCreditor As String = String.Empty

        ''' <summary>
        ''' Creditor ID for the update event
        ''' </summary>
        Public ReadOnly Property Creditor() As String
            Get
                Return privateCreditor
            End Get
        End Property

        ''' <summary>
        ''' Input file to be read
        ''' </summary>
        Protected Overrides Function OnNonSwitch(ByVal switchValue As String) As DebtPlus.Utils.ArgParserBase.SwitchStatus
            Dim ss As SwitchStatus = DebtPlus.Utils.ArgParserBase.SwitchStatus.NoError

            ' Add command-line argument to array of pathnames.
            ' Convert switchValue to set of pathnames, add each pathname to the pathnames ArrayList.
            Try
                Dim d As String = Path.GetDirectoryName(switchValue)
                Dim dir As DirectoryInfo
                If (d.Length = 0) Then
                    dir = New DirectoryInfo(".")
                Else
                    dir = New DirectoryInfo(d)
                End If
                Dim f As FileInfo
                For Each f In dir.GetFiles(Path.GetFileName(switchValue))
                    pathnames.Add(f.FullName)
                Next f

            Catch SecEx As System.Security.SecurityException
                Throw SecEx

            Catch
                ss = DebtPlus.Utils.ArgParserBase.SwitchStatus.YesError
            End Try

            If pathnames.Count = 0 Then
                AppendErrorLine("None of the specified files exists.")
                ss = DebtPlus.Utils.ArgParserBase.SwitchStatus.YesError
            End If

            Return ss
        End Function 'OnNonSwitch

        Private privateVisible As Boolean
        Public Property Visible() As Boolean
            Get
                Return privateVisible
            End Get
            Set(ByVal value As Boolean)
                privateVisible = value
            End Set
        End Property

        ''' <summary>
        ''' Return the list of input file name
        ''' </summary>
        Public Function GetPathnameEnumerator() As IEnumerator
            Return pathnames.GetEnumerator(0, pathnames.Count)
        End Function 'GetPathnameEnumerator

        Protected Overrides Function OnSwitch(ByVal switchSymbol As String, ByVal switchValue As String) As SwitchStatus
            Dim ss As SwitchStatus = DebtPlus.Utils.ArgParserBase.SwitchStatus.NoError
            Select Case switchSymbol
                Case "v"
                    Visible = True
                Case "c"
                    privateCreditor = switchValue.Trim()
                Case "?"
                    ss = DebtPlus.Utils.ArgParserBase.SwitchStatus.ShowUsage
                Case Else
                    ss = DebtPlus.Utils.ArgParserBase.SwitchStatus.YesError
            End Select
            Return ss
        End Function

        ''' <summary>
        ''' Create an instance of our class
        ''' </summary>
        Public Sub New()
            MyBase.New(New String() {"v", "c"})
        End Sub

        ''' <summary>
        ''' Generate the command usage information
        ''' </summary>
        Protected Overrides Sub OnUsage(ByVal errorInfo As String)
            ' deprecated
        End Sub

        ''' <summary>
        ''' Called after all of the arguments have been parsed
        ''' </summary>
        Protected Overrides Function OnDoneParse() As DebtPlus.Utils.ArgParserBase.SwitchStatus
            Dim ss As SwitchStatus

            ' Ask for the creditor ID from the list of valid ids.
            If Creditor = String.Empty Then
                Dim answer As DialogResult
                Using frm As New CreditorParametersForm()
                    With frm
                        .Text = "Creditor ID for the update"
                        answer = .ShowDialog()
                        privateCreditor = .Parameter_Creditor
                    End With
                End Using

                If answer <> DialogResult.OK Then
                    Return DebtPlus.Utils.ArgParserBase.SwitchStatus.Quit
                End If
            End If

            ' Ask for the input file(s) to use
            If pathnames.Count <= 0 Then
                With New OpenFileDialog
                    .AddExtension = True
                    .CheckFileExists = True
                    .CheckPathExists = True
                    .DereferenceLinks = True
                    .FileName = "*.xls"
                    .DefaultExt = ".xls"
                    .InitialDirectory = Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments)
                    .Filter = "Excel Files (*.xls)|*.xls"
                    .FilterIndex = 0
                    .Multiselect = True
                    .ReadOnlyChecked = True
                    .RestoreDirectory = True
                    .Title = "Open Excel Sheet Input"
                    If .ShowDialog() = DialogResult.OK Then
                        For Each FileName As String In .FileNames
                            pathnames.Add(FileName)
                        Next
                    End If
                    .Dispose()
                End With
            End If

            ' Cancel if there are no file names now.
            If pathnames.Count <= 0 Then
                ss = DebtPlus.Utils.ArgParserBase.SwitchStatus.YesError
            Else
                ss = DebtPlus.Utils.ArgParserBase.SwitchStatus.NoError
            End If

            ' If there are no names then return an error. Otherwise, sort them.
            If ss = DebtPlus.Utils.ArgParserBase.SwitchStatus.NoError Then
                pathnames.Sort(0, pathnames.Count, Nothing)
            End If

            Return ss
        End Function 'OnDoneParse
    End Class
End NameSpace
