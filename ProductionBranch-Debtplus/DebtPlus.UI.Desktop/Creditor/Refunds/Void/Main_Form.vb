#Region "Copyright 2000-2012 DebtPlus, L.L.C."
'{*******************************************************************}
'{                                                                   }
'{       DebtPlus Debt Management System                             }
'{                                                                   }
'{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
'{       ALL RIGHTS RESERVED                                         }
'{                                                                   }
'{   The entire contents of this file is protected by U.S. and       }
'{   International Copyright Laws. Unauthorized reproduction,        }
'{   reverse-engineering, and distribution of all or any portion of  }
'{   the code contained in this file is strictly prohibited and may  }
'{   result in severe civil and criminal penalties and will be       }
'{   prosecuted to the maximum extent possible under the law.        }
'{                                                                   }
'{   RESTRICTIONS                                                    }
'{                                                                   }
'{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
'{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
'{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
'{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
'{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
'{                                                                   }
'{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
'{   ADDITIONAL RESTRICTIONS.                                        }
'{                                                                   }
'{*******************************************************************}
#End Region

Option Compare Binary
Option Explicit On 
Option Strict On

Namespace Creditor.Refunds.Void
    Friend Class Main_Form
        Inherits DebtPlus.Data.Forms.DebtPlusForm

        Friend ap As CreditorRefundsVoidArgParser = Nothing
        Public Sub New(ByVal ap As CreditorRefundsVoidArgParser)
            MyClass.New()
            Me.ap = ap

            AddHandler VoidCreditorList1.Cancelled, AddressOf VoidCreditorList1_Cancelled
            AddHandler VoidCreditorList1.Selected, AddressOf VoidCreditorList1_Selected
            AddHandler RefundCheckDetails1.Cancelled, AddressOf RefundCheckDetails1_Cancelled
            AddHandler Me.Load, AddressOf Main_Form_Load
        End Sub

#Region " Windows Form Designer generated code "

        Public Sub New()
            MyBase.New()

            'This call is required by the Windows Form Designer.
            InitializeComponent()

            'Add any initialization after the InitializeComponent() call

        End Sub

        'Form overrides dispose to clean up the component list.
        Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
            If disposing Then
                If components IsNot Nothing Then
                    components.Dispose()
                End If
            End If
            MyBase.Dispose(disposing)
        End Sub

        'Required by the Windows Form Designer
        Private components As System.ComponentModel.IContainer

        'NOTE: The following procedure is required by the Windows Form Designer
        'It can be modified using the Windows Form Designer.  
        'Do not modify it using the code editor.
        Protected WithEvents VoidCreditorList1 As DebtPlus.UI.Desktop.Creditor.Refunds.Void.VoidCreditorList
        Friend WithEvents RefundCheckDetails1 As DebtPlus.UI.Desktop.Creditor.Refunds.Void.RefundCheckDetails
        <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
            Me.VoidCreditorList1 = New DebtPlus.UI.Desktop.Creditor.Refunds.Void.VoidCreditorList
            Me.RefundCheckDetails1 = New DebtPlus.UI.Desktop.Creditor.Refunds.Void.RefundCheckDetails

            Me.SuspendLayout()

            '
            'VoidCreditorList1
            '
            Me.VoidCreditorList1.Dock = System.Windows.Forms.DockStyle.Fill
            Me.VoidCreditorList1.Location = New System.Drawing.Point(0, 0)
            Me.VoidCreditorList1.Name = "VoidCreditorList1"
            Me.VoidCreditorList1.Size = New System.Drawing.Size(536, 382)
            Me.ToolTipController1.SetSuperTip(Me.VoidCreditorList1, Nothing)
            Me.VoidCreditorList1.TabIndex = 0
            '
            'RefundCheckDetails1
            '
            Me.RefundCheckDetails1.Dock = System.Windows.Forms.DockStyle.Fill
            Me.RefundCheckDetails1.Location = New System.Drawing.Point(0, 0)
            Me.RefundCheckDetails1.Name = "RefundCheckDetails1"
            Me.RefundCheckDetails1.Size = New System.Drawing.Size(536, 382)
            Me.ToolTipController1.SetSuperTip(Me.RefundCheckDetails1, Nothing)
            Me.RefundCheckDetails1.TabIndex = 1
            '
            'Main_Form
            '
            Me.AutoScaleBaseSize = New System.Drawing.Size(5, 14)
            Me.ClientSize = New System.Drawing.Size(536, 382)
            Me.Controls.Add(Me.VoidCreditorList1)
            Me.Controls.Add(Me.RefundCheckDetails1)
            Me.Name = "Main_Form"
            Me.ToolTipController1.SetSuperTip(Me, Nothing)
            Me.Text = "Void Creditor Refund Checks"

            Me.ResumeLayout(False)

        End Sub

#End Region

        Private Sub Main_Form_Load(ByVal sender As Object, ByVal e As System.EventArgs)

            ' Hide the refund information
            RefundCheckDetails1.Visible = False
            RefundCheckDetails1.SendToBack()

            ' Initialize the information for the creditor display
            VoidCreditorList1.Reload()
            VoidCreditorList1.Visible = True
        End Sub

        Private Sub VoidCreditorList1_Cancelled(ByVal sender As Object, ByVal e As System.EventArgs)
            Me.Close()
        End Sub

        Private Sub VoidCreditorList1_Selected(ByVal Sender As Object, ByVal CreditorRegister As System.Int32)
            VoidCreditorList1.Visible = False
            VoidCreditorList1.SendToBack()

            RefundCheckDetails1.Visible = True
            RefundCheckDetails1.Reload(CreditorRegister)
        End Sub

        Private Sub RefundCheckDetails1_Cancelled(ByVal sender As Object, ByVal e As System.EventArgs)
            ' Hide the refund information
            RefundCheckDetails1.Visible = False
            RefundCheckDetails1.SendToBack()

            ' Initialize the information for the creditor display
            VoidCreditorList1.Reload()
            VoidCreditorList1.Visible = True
        End Sub
    End Class
End Namespace
