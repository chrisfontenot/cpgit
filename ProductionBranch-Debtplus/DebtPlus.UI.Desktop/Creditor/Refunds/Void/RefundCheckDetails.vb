#Region "Copyright 2000-2012 DebtPlus, L.L.C."
'{*******************************************************************}
'{                                                                   }
'{       DebtPlus Debt Management System                             }
'{                                                                   }
'{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
'{       ALL RIGHTS RESERVED                                         }
'{                                                                   }
'{   The entire contents of this file is protected by U.S. and       }
'{   International Copyright Laws. Unauthorized reproduction,        }
'{   reverse-engineering, and distribution of all or any portion of  }
'{   the code contained in this file is strictly prohibited and may  }
'{   result in severe civil and criminal penalties and will be       }
'{   prosecuted to the maximum extent possible under the law.        }
'{                                                                   }
'{   RESTRICTIONS                                                    }
'{                                                                   }
'{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
'{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
'{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
'{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
'{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
'{                                                                   }
'{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
'{   ADDITIONAL RESTRICTIONS.                                        }
'{                                                                   }
'{*******************************************************************}
#End Region

Option Compare Binary
Option Explicit On 
Option Strict On

Imports DebtPlus.Utils
Imports System.Data.SqlClient
Imports System.Windows.Forms

Namespace Creditor.Refunds.Void

    Friend Class RefundCheckDetails
        Inherits DevExpress.XtraEditors.XtraUserControl

        Protected ds As New System.Data.DataSet("registers_client_creditor")

        Public Sub New()
            MyBase.New()
            InitializeComponent()
            AddHandler Button_Cancel.Click, AddressOf Button_Cancel_Click
            AddHandler Button_OK.Click, AddressOf Button_OK_Click
        End Sub

#Region " Windows Form Designer generated code "

        'UserControl overrides dispose to clean up the component list.
        Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
            If disposing Then
                If components IsNot Nothing Then
                    components.Dispose()
                End If
            End If
            MyBase.Dispose(disposing)
        End Sub

        'Required by the Windows Form Designer
        Private components As System.ComponentModel.IContainer

        'NOTE: The following procedure is required by the Windows Form Designer
        'It can be modified using the Windows Form Designer.  
        'Do not modify it using the code editor.
        Protected WithEvents GridControl1 As DevExpress.XtraGrid.GridControl
        Protected WithEvents GridView1 As DevExpress.XtraGrid.Views.Grid.GridView
        Protected WithEvents LabelControl1 As DevExpress.XtraEditors.LabelControl
        Protected WithEvents GridColumn_client As DevExpress.XtraGrid.Columns.GridColumn
        Protected WithEvents GridColumn_name As DevExpress.XtraGrid.Columns.GridColumn
        Protected WithEvents GridColumn_gross As DevExpress.XtraGrid.Columns.GridColumn
        Protected WithEvents GridColumn_deducted As DevExpress.XtraGrid.Columns.GridColumn
        Protected WithEvents GridColumn_net As DevExpress.XtraGrid.Columns.GridColumn
        Protected WithEvents GridColumn_creditor_type As DevExpress.XtraGrid.Columns.GridColumn
        Protected WithEvents GridColumn_fairshare_amt As DevExpress.XtraGrid.Columns.GridColumn
        Protected WithEvents GridColumn_fairshare_pct As DevExpress.XtraGrid.Columns.GridColumn
        Protected WithEvents GridColumn_void As DevExpress.XtraGrid.Columns.GridColumn
        Protected WithEvents Button_OK As DevExpress.XtraEditors.SimpleButton
        Protected WithEvents Button_Cancel As DevExpress.XtraEditors.SimpleButton
        Protected WithEvents reason As DevExpress.XtraEditors.LookUpEdit
        <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
            Me.GridControl1 = New DevExpress.XtraGrid.GridControl
            Me.GridView1 = New DevExpress.XtraGrid.Views.Grid.GridView
            Me.GridColumn_client = New DevExpress.XtraGrid.Columns.GridColumn
            Me.GridColumn_client.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.True
            Me.GridColumn_name = New DevExpress.XtraGrid.Columns.GridColumn
            Me.GridColumn_name.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.True
            Me.GridColumn_net = New DevExpress.XtraGrid.Columns.GridColumn
            Me.GridColumn_net.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.True
            Me.GridColumn_deducted = New DevExpress.XtraGrid.Columns.GridColumn
            Me.GridColumn_deducted.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.True
            Me.GridColumn_gross = New DevExpress.XtraGrid.Columns.GridColumn
            Me.GridColumn_gross.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.True
            Me.GridColumn_creditor_type = New DevExpress.XtraGrid.Columns.GridColumn
            Me.GridColumn_creditor_type.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.True
            Me.GridColumn_fairshare_amt = New DevExpress.XtraGrid.Columns.GridColumn
            Me.GridColumn_fairshare_amt.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.True
            Me.GridColumn_fairshare_pct = New DevExpress.XtraGrid.Columns.GridColumn
            Me.GridColumn_fairshare_pct.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.True
            Me.GridColumn_void = New DevExpress.XtraGrid.Columns.GridColumn
            Me.GridColumn_void.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.True
            Me.reason = New DevExpress.XtraEditors.LookUpEdit
            Me.LabelControl1 = New DevExpress.XtraEditors.LabelControl
            Me.Button_OK = New DevExpress.XtraEditors.SimpleButton
            Me.Button_Cancel = New DevExpress.XtraEditors.SimpleButton
            CType(Me.GridControl1, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.GridView1, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.reason.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
            Me.SuspendLayout()
            '
            'GridControl1
            '
            Me.GridControl1.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                        Or System.Windows.Forms.AnchorStyles.Left) _
                        Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
            Me.GridControl1.EmbeddedNavigator.Name = ""
            Me.GridControl1.Location = New System.Drawing.Point(0, 40)
            Me.GridControl1.MainView = Me.GridView1
            Me.GridControl1.Name = "GridControl1"
            Me.GridControl1.Size = New System.Drawing.Size(440, 312)
            Me.GridControl1.TabIndex = 2
            Me.GridControl1.ViewCollection.AddRange(New DevExpress.XtraGrid.Views.Base.BaseView() {Me.GridView1})
            '
            'GridView1
            '
            Me.GridView1.Appearance.ColumnFilterButton.BackColor = System.Drawing.Color.FromArgb(CType(CType(104, Byte), Int32), CType(CType(184, Byte), Int32), CType(CType(251, Byte), Int32))
            Me.GridView1.Appearance.ColumnFilterButton.BorderColor = System.Drawing.Color.FromArgb(CType(CType(104, Byte), Int32), CType(CType(184, Byte), Int32), CType(CType(251, Byte), Int32))
            Me.GridView1.Appearance.ColumnFilterButton.ForeColor = System.Drawing.Color.White
            Me.GridView1.Appearance.ColumnFilterButton.Options.UseBackColor = True
            Me.GridView1.Appearance.ColumnFilterButton.Options.UseBorderColor = True
            Me.GridView1.Appearance.ColumnFilterButton.Options.UseForeColor = True
            Me.GridView1.Appearance.ColumnFilterButtonActive.BackColor = System.Drawing.Color.FromArgb(CType(CType(170, Byte), Int32), CType(CType(216, Byte), Int32), CType(CType(254, Byte), Int32))
            Me.GridView1.Appearance.ColumnFilterButtonActive.BorderColor = System.Drawing.Color.FromArgb(CType(CType(170, Byte), Int32), CType(CType(216, Byte), Int32), CType(CType(254, Byte), Int32))
            Me.GridView1.Appearance.ColumnFilterButtonActive.ForeColor = System.Drawing.Color.Black
            Me.GridView1.Appearance.ColumnFilterButtonActive.Options.UseBackColor = True
            Me.GridView1.Appearance.ColumnFilterButtonActive.Options.UseBorderColor = True
            Me.GridView1.Appearance.ColumnFilterButtonActive.Options.UseForeColor = True
            Me.GridView1.Appearance.Empty.BackColor = System.Drawing.Color.FromArgb(CType(CType(236, Byte), Int32), CType(CType(246, Byte), Int32), CType(CType(255, Byte), Int32))
            Me.GridView1.Appearance.Empty.BackColor2 = System.Drawing.Color.White
            Me.GridView1.Appearance.Empty.Options.UseBackColor = True
            Me.GridView1.Appearance.EvenRow.BackColor = System.Drawing.Color.FromArgb(CType(CType(247, Byte), Int32), CType(CType(251, Byte), Int32), CType(CType(255, Byte), Int32))
            Me.GridView1.Appearance.EvenRow.BorderColor = System.Drawing.Color.FromArgb(CType(CType(247, Byte), Int32), CType(CType(251, Byte), Int32), CType(CType(255, Byte), Int32))
            Me.GridView1.Appearance.EvenRow.ForeColor = System.Drawing.Color.Black
            Me.GridView1.Appearance.EvenRow.Options.UseBackColor = True
            Me.GridView1.Appearance.EvenRow.Options.UseBorderColor = True
            Me.GridView1.Appearance.EvenRow.Options.UseForeColor = True
            Me.GridView1.Appearance.FilterCloseButton.BackColor = System.Drawing.Color.FromArgb(CType(CType(104, Byte), Int32), CType(CType(184, Byte), Int32), CType(CType(251, Byte), Int32))
            Me.GridView1.Appearance.FilterCloseButton.BorderColor = System.Drawing.Color.FromArgb(CType(CType(104, Byte), Int32), CType(CType(184, Byte), Int32), CType(CType(251, Byte), Int32))
            Me.GridView1.Appearance.FilterCloseButton.ForeColor = System.Drawing.Color.White
            Me.GridView1.Appearance.FilterCloseButton.Options.UseBackColor = True
            Me.GridView1.Appearance.FilterCloseButton.Options.UseBorderColor = True
            Me.GridView1.Appearance.FilterCloseButton.Options.UseForeColor = True
            Me.GridView1.Appearance.FilterPanel.BackColor = System.Drawing.Color.FromArgb(CType(CType(236, Byte), Int32), CType(CType(246, Byte), Int32), CType(CType(255, Byte), Int32))
            Me.GridView1.Appearance.FilterPanel.BackColor2 = System.Drawing.Color.White
            Me.GridView1.Appearance.FilterPanel.ForeColor = System.Drawing.Color.Black
            Me.GridView1.Appearance.FilterPanel.Options.UseBackColor = True
            Me.GridView1.Appearance.FilterPanel.Options.UseForeColor = True
            Me.GridView1.Appearance.FixedLine.BackColor = System.Drawing.Color.FromArgb(CType(CType(59, Byte), Int32), CType(CType(133, Byte), Int32), CType(CType(195, Byte), Int32))
            Me.GridView1.Appearance.FixedLine.Options.UseBackColor = True
            Me.GridView1.Appearance.FocusedCell.BackColor = System.Drawing.Color.White
            Me.GridView1.Appearance.FocusedCell.ForeColor = System.Drawing.Color.Black
            Me.GridView1.Appearance.FocusedCell.Options.UseBackColor = True
            Me.GridView1.Appearance.FocusedCell.Options.UseForeColor = True
            Me.GridView1.Appearance.FocusedRow.BackColor = System.Drawing.Color.FromArgb(CType(CType(38, Byte), Int32), CType(CType(109, Byte), Int32), CType(CType(189, Byte), Int32))
            Me.GridView1.Appearance.FocusedRow.BorderColor = System.Drawing.Color.FromArgb(CType(CType(59, Byte), Int32), CType(CType(139, Byte), Int32), CType(CType(206, Byte), Int32))
            Me.GridView1.Appearance.FocusedRow.ForeColor = System.Drawing.Color.White
            Me.GridView1.Appearance.FocusedRow.Options.UseBackColor = True
            Me.GridView1.Appearance.FocusedRow.Options.UseBorderColor = True
            Me.GridView1.Appearance.FocusedRow.Options.UseForeColor = True
            Me.GridView1.Appearance.FooterPanel.BackColor = System.Drawing.Color.FromArgb(CType(CType(104, Byte), Int32), CType(CType(184, Byte), Int32), CType(CType(251, Byte), Int32))
            Me.GridView1.Appearance.FooterPanel.BorderColor = System.Drawing.Color.FromArgb(CType(CType(104, Byte), Int32), CType(CType(184, Byte), Int32), CType(CType(251, Byte), Int32))
            Me.GridView1.Appearance.FooterPanel.ForeColor = System.Drawing.Color.Black
            Me.GridView1.Appearance.FooterPanel.Options.UseBackColor = True
            Me.GridView1.Appearance.FooterPanel.Options.UseBorderColor = True
            Me.GridView1.Appearance.FooterPanel.Options.UseForeColor = True
            Me.GridView1.Appearance.GroupButton.BackColor = System.Drawing.Color.FromArgb(CType(CType(104, Byte), Int32), CType(CType(184, Byte), Int32), CType(CType(251, Byte), Int32))
            Me.GridView1.Appearance.GroupButton.BorderColor = System.Drawing.Color.FromArgb(CType(CType(104, Byte), Int32), CType(CType(184, Byte), Int32), CType(CType(251, Byte), Int32))
            Me.GridView1.Appearance.GroupButton.Options.UseBackColor = True
            Me.GridView1.Appearance.GroupButton.Options.UseBorderColor = True
            Me.GridView1.Appearance.GroupFooter.BackColor = System.Drawing.Color.FromArgb(CType(CType(170, Byte), Int32), CType(CType(216, Byte), Int32), CType(CType(254, Byte), Int32))
            Me.GridView1.Appearance.GroupFooter.BorderColor = System.Drawing.Color.FromArgb(CType(CType(170, Byte), Int32), CType(CType(216, Byte), Int32), CType(CType(254, Byte), Int32))
            Me.GridView1.Appearance.GroupFooter.ForeColor = System.Drawing.Color.Black
            Me.GridView1.Appearance.GroupFooter.Options.UseBackColor = True
            Me.GridView1.Appearance.GroupFooter.Options.UseBorderColor = True
            Me.GridView1.Appearance.GroupFooter.Options.UseForeColor = True
            Me.GridView1.Appearance.GroupPanel.BackColor = System.Drawing.Color.FromArgb(CType(CType(236, Byte), Int32), CType(CType(246, Byte), Int32), CType(CType(255, Byte), Int32))
            Me.GridView1.Appearance.GroupPanel.BackColor2 = System.Drawing.Color.White
            Me.GridView1.Appearance.GroupPanel.ForeColor = System.Drawing.Color.Black
            Me.GridView1.Appearance.GroupPanel.Options.UseBackColor = True
            Me.GridView1.Appearance.GroupPanel.Options.UseForeColor = True
            Me.GridView1.Appearance.GroupRow.BackColor = System.Drawing.Color.FromArgb(CType(CType(170, Byte), Int32), CType(CType(216, Byte), Int32), CType(CType(254, Byte), Int32))
            Me.GridView1.Appearance.GroupRow.BorderColor = System.Drawing.Color.FromArgb(CType(CType(170, Byte), Int32), CType(CType(216, Byte), Int32), CType(CType(254, Byte), Int32))
            Me.GridView1.Appearance.GroupRow.ForeColor = System.Drawing.Color.Black
            Me.GridView1.Appearance.GroupRow.Options.UseBackColor = True
            Me.GridView1.Appearance.GroupRow.Options.UseBorderColor = True
            Me.GridView1.Appearance.GroupRow.Options.UseForeColor = True
            Me.GridView1.Appearance.HeaderPanel.BackColor = System.Drawing.Color.FromArgb(CType(CType(139, Byte), Int32), CType(CType(201, Byte), Int32), CType(CType(254, Byte), Int32))
            Me.GridView1.Appearance.HeaderPanel.BorderColor = System.Drawing.Color.FromArgb(CType(CType(139, Byte), Int32), CType(CType(201, Byte), Int32), CType(CType(254, Byte), Int32))
            Me.GridView1.Appearance.HeaderPanel.ForeColor = System.Drawing.Color.Black
            Me.GridView1.Appearance.HeaderPanel.Options.UseBackColor = True
            Me.GridView1.Appearance.HeaderPanel.Options.UseBorderColor = True
            Me.GridView1.Appearance.HeaderPanel.Options.UseForeColor = True
            Me.GridView1.Appearance.HideSelectionRow.BackColor = System.Drawing.Color.FromArgb(CType(CType(105, Byte), Int32), CType(CType(170, Byte), Int32), CType(CType(225, Byte), Int32))
            Me.GridView1.Appearance.HideSelectionRow.BorderColor = System.Drawing.Color.FromArgb(CType(CType(83, Byte), Int32), CType(CType(155, Byte), Int32), CType(CType(215, Byte), Int32))
            Me.GridView1.Appearance.HideSelectionRow.ForeColor = System.Drawing.Color.FromArgb(CType(CType(236, Byte), Int32), CType(CType(246, Byte), Int32), CType(CType(255, Byte), Int32))
            Me.GridView1.Appearance.HideSelectionRow.Options.UseBackColor = True
            Me.GridView1.Appearance.HideSelectionRow.Options.UseBorderColor = True
            Me.GridView1.Appearance.HideSelectionRow.Options.UseForeColor = True
            Me.GridView1.Appearance.HorzLine.BackColor = System.Drawing.Color.FromArgb(CType(CType(104, Byte), Int32), CType(CType(184, Byte), Int32), CType(CType(251, Byte), Int32))
            Me.GridView1.Appearance.HorzLine.Options.UseBackColor = True
            Me.GridView1.Appearance.OddRow.BackColor = System.Drawing.Color.FromArgb(CType(CType(236, Byte), Int32), CType(CType(246, Byte), Int32), CType(CType(255, Byte), Int32))
            Me.GridView1.Appearance.OddRow.BorderColor = System.Drawing.Color.FromArgb(CType(CType(236, Byte), Int32), CType(CType(246, Byte), Int32), CType(CType(255, Byte), Int32))
            Me.GridView1.Appearance.OddRow.ForeColor = System.Drawing.Color.Black
            Me.GridView1.Appearance.OddRow.Options.UseBackColor = True
            Me.GridView1.Appearance.OddRow.Options.UseBorderColor = True
            Me.GridView1.Appearance.OddRow.Options.UseForeColor = True
            Me.GridView1.Appearance.Preview.Font = New System.Drawing.Font("Verdana", 7.5!)
            Me.GridView1.Appearance.Preview.ForeColor = System.Drawing.Color.FromArgb(CType(CType(83, Byte), Int32), CType(CType(155, Byte), Int32), CType(CType(215, Byte), Int32))
            Me.GridView1.Appearance.Preview.Options.UseFont = True
            Me.GridView1.Appearance.Preview.Options.UseForeColor = True
            Me.GridView1.Appearance.Row.BackColor = System.Drawing.Color.FromArgb(CType(CType(247, Byte), Int32), CType(CType(251, Byte), Int32), CType(CType(255, Byte), Int32))
            Me.GridView1.Appearance.Row.ForeColor = System.Drawing.Color.Black
            Me.GridView1.Appearance.Row.Options.UseBackColor = True
            Me.GridView1.Appearance.Row.Options.UseForeColor = True
            Me.GridView1.Appearance.RowSeparator.BackColor = System.Drawing.Color.FromArgb(CType(CType(236, Byte), Int32), CType(CType(246, Byte), Int32), CType(CType(255, Byte), Int32))
            Me.GridView1.Appearance.RowSeparator.BackColor2 = System.Drawing.Color.White
            Me.GridView1.Appearance.RowSeparator.Options.UseBackColor = True
            Me.GridView1.Appearance.SelectedRow.BackColor = System.Drawing.Color.FromArgb(CType(CType(83, Byte), Int32), CType(CType(155, Byte), Int32), CType(CType(215, Byte), Int32))
            Me.GridView1.Appearance.SelectedRow.ForeColor = System.Drawing.Color.White
            Me.GridView1.Appearance.SelectedRow.Options.UseBackColor = True
            Me.GridView1.Appearance.SelectedRow.Options.UseForeColor = True
            Me.GridView1.Appearance.TopNewRow.BackColor = System.Drawing.Color.White
            Me.GridView1.Appearance.TopNewRow.Options.UseBackColor = True
            Me.GridView1.Appearance.VertLine.BackColor = System.Drawing.Color.FromArgb(CType(CType(104, Byte), Int32), CType(CType(184, Byte), Int32), CType(CType(251, Byte), Int32))
            Me.GridView1.Appearance.VertLine.Options.UseBackColor = True
            Me.GridView1.Columns.AddRange(New DevExpress.XtraGrid.Columns.GridColumn() {Me.GridColumn_client, Me.GridColumn_name, Me.GridColumn_net, Me.GridColumn_deducted, Me.GridColumn_gross, Me.GridColumn_creditor_type, Me.GridColumn_fairshare_amt, Me.GridColumn_fairshare_pct, Me.GridColumn_void})
            Me.GridView1.CustomizationFormBounds = New System.Drawing.Rectangle(806, 518, 208, 170)
            Me.GridView1.GridControl = Me.GridControl1
            Me.GridView1.Name = "GridView1"
            Me.GridView1.OptionsBehavior.Editable = False
            Me.GridView1.OptionsView.ShowFooter = True
            Me.GridView1.OptionsView.ShowGroupPanel = False
            Me.GridView1.OptionsView.ShowIndicator = False
            '
            'GridColumn_client
            '
            Me.GridColumn_client.AppearanceCell.Options.UseTextOptions = True
            Me.GridColumn_client.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
            Me.GridColumn_client.AppearanceHeader.Options.UseTextOptions = True
            Me.GridColumn_client.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
            Me.GridColumn_client.Caption = "Client ID"
            Me.GridColumn_client.DisplayFormat.FormatString = "0000000"
            Me.GridColumn_client.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Custom
            Me.GridColumn_client.FieldName = "client"
            Me.GridColumn_client.GroupFormat.FormatString = "0000000"
            Me.GridColumn_client.GroupFormat.FormatType = DevExpress.Utils.FormatType.Custom
            Me.GridColumn_client.Name = "GridColumn_client"
            Me.GridColumn_client.SummaryItem.DisplayFormat = "{0:d} item(s)"
            Me.GridColumn_client.SummaryItem.SummaryType = DevExpress.Data.SummaryItemType.Count
            Me.GridColumn_client.Visible = True
            Me.GridColumn_client.VisibleIndex = 0
            Me.GridColumn_client.Width = 45
            '
            'GridColumn_name
            '
            Me.GridColumn_name.Caption = "Name"
            Me.GridColumn_name.FieldName = "name"
            Me.GridColumn_name.Name = "GridColumn_name"
            Me.GridColumn_name.Visible = True
            Me.GridColumn_name.VisibleIndex = 1
            Me.GridColumn_name.Width = 210
            '
            'GridColumn_net
            '
            Me.GridColumn_net.AppearanceCell.Options.UseTextOptions = True
            Me.GridColumn_net.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
            Me.GridColumn_net.AppearanceHeader.Options.UseTextOptions = True
            Me.GridColumn_net.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
            Me.GridColumn_net.Caption = "Net"
            Me.GridColumn_net.DisplayFormat.FormatString = "c"
            Me.GridColumn_net.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
            Me.GridColumn_net.FieldName = "net"
            Me.GridColumn_net.GroupFormat.FormatString = "c"
            Me.GridColumn_net.GroupFormat.FormatType = DevExpress.Utils.FormatType.Numeric
            Me.GridColumn_net.Name = "GridColumn_net"
            Me.GridColumn_net.SummaryItem.DisplayFormat = "{0:c}"
            Me.GridColumn_net.SummaryItem.SummaryType = DevExpress.Data.SummaryItemType.Sum
            Me.GridColumn_net.Visible = True
            Me.GridColumn_net.VisibleIndex = 2
            Me.GridColumn_net.Width = 70
            '
            'GridColumn_deducted
            '
            Me.GridColumn_deducted.AppearanceCell.Options.UseTextOptions = True
            Me.GridColumn_deducted.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
            Me.GridColumn_deducted.AppearanceHeader.Options.UseTextOptions = True
            Me.GridColumn_deducted.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
            Me.GridColumn_deducted.Caption = "Deducted"
            Me.GridColumn_deducted.DisplayFormat.FormatString = "c"
            Me.GridColumn_deducted.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
            Me.GridColumn_deducted.FieldName = "deducted"
            Me.GridColumn_deducted.GroupFormat.FormatString = "c"
            Me.GridColumn_deducted.GroupFormat.FormatType = DevExpress.Utils.FormatType.Numeric
            Me.GridColumn_deducted.Name = "GridColumn_deducted"
            Me.GridColumn_deducted.SummaryItem.DisplayFormat = "{0:c}"
            Me.GridColumn_deducted.SummaryItem.SummaryType = DevExpress.Data.SummaryItemType.Sum
            Me.GridColumn_deducted.Visible = True
            Me.GridColumn_deducted.VisibleIndex = 3
            Me.GridColumn_deducted.Width = 60
            '
            'GridColumn_gross
            '
            Me.GridColumn_gross.AppearanceCell.Options.UseTextOptions = True
            Me.GridColumn_gross.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
            Me.GridColumn_gross.AppearanceHeader.Options.UseTextOptions = True
            Me.GridColumn_gross.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
            Me.GridColumn_gross.Caption = "Gross"
            Me.GridColumn_gross.DisplayFormat.FormatString = "c"
            Me.GridColumn_gross.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
            Me.GridColumn_gross.FieldName = "credit_amt"
            Me.GridColumn_gross.GroupFormat.FormatString = "c"
            Me.GridColumn_gross.GroupFormat.FormatType = DevExpress.Utils.FormatType.Numeric
            Me.GridColumn_gross.Name = "GridColumn_gross"
            Me.GridColumn_gross.SummaryItem.DisplayFormat = "{0:c}"
            Me.GridColumn_gross.SummaryItem.SummaryType = DevExpress.Data.SummaryItemType.Sum
            Me.GridColumn_gross.Visible = True
            Me.GridColumn_gross.VisibleIndex = 4
            Me.GridColumn_gross.Width = 70
            '
            'GridColumn_creditor_type
            '
            Me.GridColumn_creditor_type.AppearanceCell.Options.UseTextOptions = True
            Me.GridColumn_creditor_type.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center
            Me.GridColumn_creditor_type.AppearanceHeader.Options.UseTextOptions = True
            Me.GridColumn_creditor_type.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center
            Me.GridColumn_creditor_type.Caption = "Creditor Type"
            Me.GridColumn_creditor_type.FieldName = "creditor_type"
            Me.GridColumn_creditor_type.Name = "GridColumn_creditor_type"
            '
            'GridColumn_fairshare_amt
            '
            Me.GridColumn_fairshare_amt.AppearanceCell.Options.UseTextOptions = True
            Me.GridColumn_fairshare_amt.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
            Me.GridColumn_fairshare_amt.AppearanceHeader.Options.UseTextOptions = True
            Me.GridColumn_fairshare_amt.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
            Me.GridColumn_fairshare_amt.Caption = "Fairshare"
            Me.GridColumn_fairshare_amt.DisplayFormat.FormatString = "c"
            Me.GridColumn_fairshare_amt.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
            Me.GridColumn_fairshare_amt.FieldName = "fairshare_amt"
            Me.GridColumn_fairshare_amt.GroupFormat.FormatString = "c"
            Me.GridColumn_fairshare_amt.GroupFormat.FormatType = DevExpress.Utils.FormatType.Numeric
            Me.GridColumn_fairshare_amt.Name = "GridColumn_fairshare_amt"
            Me.GridColumn_fairshare_amt.SummaryItem.DisplayFormat = "{0:c}"
            Me.GridColumn_fairshare_amt.SummaryItem.SummaryType = DevExpress.Data.SummaryItemType.Sum
            '
            'GridColumn_fairshare_pct
            '
            Me.GridColumn_fairshare_pct.AppearanceCell.Options.UseTextOptions = True
            Me.GridColumn_fairshare_pct.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
            Me.GridColumn_fairshare_pct.AppearanceHeader.Options.UseTextOptions = True
            Me.GridColumn_fairshare_pct.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
            Me.GridColumn_fairshare_pct.Caption = "Fairshare Pct"
            Me.GridColumn_fairshare_pct.DisplayFormat.FormatString = "p"
            Me.GridColumn_fairshare_pct.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
            Me.GridColumn_fairshare_pct.FieldName = "fairshare_pct"
            Me.GridColumn_fairshare_pct.GroupFormat.FormatString = "p"
            Me.GridColumn_fairshare_pct.GroupFormat.FormatType = DevExpress.Utils.FormatType.Numeric
            Me.GridColumn_fairshare_pct.Name = "GridColumn_fairshare_pct"
            '
            'GridColumn_void
            '
            Me.GridColumn_void.Caption = "Voided"
            Me.GridColumn_void.FieldName = "void"
            Me.GridColumn_void.Name = "GridColumn_void"
            '
            'reason
            '
            Me.reason.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                        Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
            Me.reason.Location = New System.Drawing.Point(104, 8)
            Me.reason.Name = "reason"
            Me.reason.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
            Me.reason.Properties.Columns.AddRange(New DevExpress.XtraEditors.Controls.LookUpColumnInfo() {New DevExpress.XtraEditors.Controls.LookUpColumnInfo("item_key", "ID", 20, DevExpress.Utils.FormatType.Numeric, "f0", False, DevExpress.Utils.HorzAlignment.Far, DevExpress.Data.ColumnSortOrder.None), New DevExpress.XtraEditors.Controls.LookUpColumnInfo("description", "Description", 20, DevExpress.Utils.FormatType.None, "", True, DevExpress.Utils.HorzAlignment.Near, DevExpress.Data.ColumnSortOrder.Ascending)})
            Me.reason.Properties.NullText = ""
            Me.reason.Properties.ShowFooter = False
            Me.reason.Properties.ShowHeader = False
            Me.reason.Properties.ShowLines = False
            Me.reason.Size = New System.Drawing.Size(336, 20)
            Me.reason.TabIndex = 1
            Me.reason.Properties.SortColumnIndex = 1
            '
            'LabelControl1
            '
            Me.LabelControl1.Location = New System.Drawing.Point(16, 11)
            Me.LabelControl1.Name = "LabelControl1"
            Me.LabelControl1.Size = New System.Drawing.Size(36, 13)
            Me.LabelControl1.TabIndex = 0
            Me.LabelControl1.Text = "Reason"
            '
            'Button_OK
            '
            Me.Button_OK.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
            Me.Button_OK.Enabled = False
            Me.Button_OK.Location = New System.Drawing.Point(456, 8)
            Me.Button_OK.Name = "Button_OK"
            Me.Button_OK.Size = New System.Drawing.Size(75, 23)
            Me.Button_OK.TabIndex = 3
            Me.Button_OK.Text = "&OK"
            '
            'Button_Cancel
            '
            Me.Button_Cancel.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
            Me.Button_Cancel.CausesValidation = False
            Me.Button_Cancel.Location = New System.Drawing.Point(456, 56)
            Me.Button_Cancel.Name = "Button_Cancel"
            Me.Button_Cancel.Size = New System.Drawing.Size(75, 23)
            Me.Button_Cancel.TabIndex = 4
            Me.Button_Cancel.Text = "&Cancel"
            '
            'RefundCheckDetails
            '
            Me.Controls.Add(Me.Button_Cancel)
            Me.Controls.Add(Me.Button_OK)
            Me.Controls.Add(Me.LabelControl1)
            Me.Controls.Add(Me.reason)
            Me.Controls.Add(Me.GridControl1)
            Me.Name = "RefundCheckDetails"
            Me.Size = New System.Drawing.Size(544, 352)
            CType(Me.GridControl1, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.GridView1, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.reason.Properties, System.ComponentModel.ISupportInitialize).EndInit()
            Me.ResumeLayout(False)
            Me.PerformLayout()

        End Sub

#End Region

        ''' <summary>
        ''' Intialize the control with the values
        ''' </summary>
        Private creditor_register As System.Int32 = -1
        Public Sub Reload(ByVal CreditorRegister As System.Int32)
            creditor_register = CreditorRegister

            Dim cmd As SqlClient.SqlCommand = New SqlCommand
            With cmd
                .Connection = New SqlConnection(DebtPlus.LINQ.SQLInfoClass.getDefault().ConnectionString)
                .CommandText = "SELECT rrc.client_creditor_register, rrc.client_creditor, rrc.tran_type, rrc.client, rrc.creditor, rrc.creditor_type, rrc.disbursement_register, rrc.trust_register, rrc.invoice_register, rrc.creditor_register, rrc.credit_amt, rrc.debit_amt, rrc.fairshare_amt, rrc.fairshare_pct, rrc.account_number, rrc.void, rrc.date_created, rrc.created_by, dbo.format_normal_name(p.prefix, p.first, p.middle, p.last, p.suffix) AS name, isnull(rrc.fairshare_amt,0) AS deducted, isnull(rrc.credit_amt,0) - isnull(rrc.fairshare_amt,0) AS net FROM registers_client_creditor rrc LEFT OUTER JOIN people p ON rrc.client = p.client AND 1 = p.relation WHERE tran_type = 'RF' AND creditor_register=@creditor_register"
                .Parameters.Add("@creditor_register", SqlDbType.Int).Value = CreditorRegister
            End With

            Dim current_cursor As Cursor = System.Windows.Forms.Cursor.Current
            Try
                System.Windows.Forms.Cursor.Current = Cursors.WaitCursor
                Dim da As New SqlClient.SqlDataAdapter(cmd)
                ds.Clear()
                da.Fill(ds, "registers_client_creditor")

                With GridControl1
                    .DataSource = ds.Tables("registers_client_creditor")
                    .RefreshDataSource()
                End With

                ' Try to fit the columns as best that we can
                GridView1.BestFitColumns()

                ' Load the reason list as well
                With reason
                    Dim tbl As System.Data.DataTable = MessagesTable()
                    With .Properties
                        .DataSource = tbl.DefaultView
                        .DisplayMember = "description"
                        .ValueMember = "item_key"
                    End With
                    If tbl.Columns.IndexOf("Default") >= 0 Then
                        Dim vue As New System.Data.DataView(tbl, "[default]=True", String.Empty, DataViewRowState.CurrentRows)
                        If vue.Count > 0 Then
                            .EditValue = vue(0)("item_key")
                        End If
                    End If

                    If .EditValue Is Nothing AndAlso tbl.Rows.Count > 0 Then
                        .EditValue = tbl.Rows(0)("item_key")
                    End If
                End With
                System.Windows.Forms.Cursor.Current = current_cursor

                ' Enable the OK button as needed
                Button_OK.Enabled = (GridView1.RowCount > 0) AndAlso reason.EditValue IsNot Nothing

                ' If the check contains voided items then complain.
                Dim VoidCount As Object = ds.Tables(0).Compute("count(void)", "void<>0")
                If VoidCount IsNot Nothing AndAlso Convert.ToInt32(VoidCount) > 0 Then
                    DebtPlus.Data.Forms.MessageBox.Show(String.Format("{0:d} item(s) have been previously voided on this refund check." + Environment.NewLine + Environment.NewLine + "This check may not be voided again.", Convert.ToInt32(VoidCount)), "Sorry, but the check is voided", MessageBoxButtons.OK, MessageBoxIcon.Hand)
                    Button_OK.Enabled = False
                End If

            Catch ex As SqlClient.SqlException
                DebtPlus.UI.Common.ErrorHandling.HandleErrors(ex, "Error reading registers_client_creditor table")

            Finally
                System.Windows.Forms.Cursor.Current = current_cursor
            End Try
        End Sub

        ''' <summary>
        ''' Retrieve the list of void reasons for a creditor check
        ''' </summary>
        Friend Function MessagesTable() As System.Data.DataTable
            Dim tbl As System.Data.DataTable = Nothing
            Dim ds As New System.Data.DataSet("messages")
            ds.Clear()

            Dim cmd As SqlClient.SqlCommand = New SqlCommand
            With cmd
                .CommandText = "lst_descriptions_vf"
                .CommandType = CommandType.StoredProcedure
                .Connection = New SqlConnection(DebtPlus.LINQ.SQLInfoClass.getDefault().ConnectionString)
            End With
            Dim da As New SqlClient.SqlDataAdapter(cmd)

            Dim current_cursor As Cursor = System.Windows.Forms.Cursor.Current
            Try
                System.Windows.Forms.Cursor.Current = Cursors.WaitCursor
                da.Fill(ds, "reasons")
                tbl = ds.Tables(0)

            Catch ex As SqlClient.SqlException
                DebtPlus.UI.Common.ErrorHandling.HandleErrors(ex, "Error reading messages table")

            Finally
                System.Windows.Forms.Cursor.Current = current_cursor
            End Try

            Return tbl
        End Function

        ''' <summary>
        ''' Process a click on the CANCEL button
        ''' </summary>
        Public Event Cancelled As System.EventHandler
        Private Sub Button_Cancel_Click(ByVal sender As Object, ByVal e As System.EventArgs)
            RaiseEvent Cancelled(Me, EventArgs.Empty)
        End Sub

        ''' <summary>
        ''' Process a click on the OK button
        ''' </summary>
        Private Sub Button_OK_Click(ByVal sender As Object, ByVal e As System.EventArgs)
            Dim cn As SqlClient.SqlConnection = New SqlConnection(DebtPlus.LINQ.SQLInfoClass.getDefault().ConnectionString)
            Dim current_cursor As Cursor = System.Windows.Forms.Cursor.Current

            Try
                System.Windows.Forms.Cursor.Current = Cursors.WaitCursor
                cn.Open()

                With New SqlCommand
                    .Connection = cn
                    .CommandText = "xpr_void_creditor_refund"
                    .CommandType = CommandType.StoredProcedure
                    .Parameters.Add("@creditor_register", SqlDbType.Int).Value = creditor_register
                    .Parameters.Add("@reason", SqlDbType.VarChar, 50).Value = reason.Text
                    .ExecuteNonQuery()
                End With

                ' All is well with the world
                System.Windows.Forms.Cursor.Current = current_cursor
                DebtPlus.Data.Forms.MessageBox.Show("The creditor refund check was successfully voided.", "Operation Completed", MessageBoxButtons.OK, MessageBoxIcon.Information)

            Catch ex As SqlClient.SqlException
                DebtPlus.UI.Common.ErrorHandling.HandleErrors(ex, "Error voiding creditor refund")

            Finally
                System.Windows.Forms.Cursor.Current = current_cursor
            End Try

            ' Trip the cancelled function to return to the previous screen.
            RaiseEvent Cancelled(Me, EventArgs.Empty)
        End Sub
    End Class
End Namespace
