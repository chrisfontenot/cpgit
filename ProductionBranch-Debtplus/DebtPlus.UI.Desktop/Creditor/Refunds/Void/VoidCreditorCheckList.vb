#Region "Copyright 2000-2012 DebtPlus, L.L.C."
'{*******************************************************************}
'{                                                                   }
'{       DebtPlus Debt Management System                             }
'{                                                                   }
'{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
'{       ALL RIGHTS RESERVED                                         }
'{                                                                   }
'{   The entire contents of this file is protected by U.S. and       }
'{   International Copyright Laws. Unauthorized reproduction,        }
'{   reverse-engineering, and distribution of all or any portion of  }
'{   the code contained in this file is strictly prohibited and may  }
'{   result in severe civil and criminal penalties and will be       }
'{   prosecuted to the maximum extent possible under the law.        }
'{                                                                   }
'{   RESTRICTIONS                                                    }
'{                                                                   }
'{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
'{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
'{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
'{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
'{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
'{                                                                   }
'{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
'{   ADDITIONAL RESTRICTIONS.                                        }
'{                                                                   }
'{*******************************************************************}
#End Region

Option Compare Binary
Option Explicit On 
Option Strict On

Imports DebtPlus.Utils
Imports System.Data.SqlClient
Imports System.Windows.Forms

Namespace Creditor.Refunds.Void
    Friend Class VoidCreditorCheckList
        Inherits DevExpress.XtraEditors.XtraUserControl

        ' Dataset for the creditor register
        Public ds As New System.Data.DataSet("registers_creditor")

        ' Event when a record is selected
        Public Event Selected(ByVal Sender As Object, ByVal CreditorRegister As System.Int32)

        Public Sub New()
            MyBase.New()
            InitializeComponent()
            AddHandler Button_OK.Click, AddressOf Button_OK_Click
            AddHandler GridControl1.Click, AddressOf GridControl1_Click
            AddHandler GridView1.DoubleClick, AddressOf GridView1_DoubleClick
            AddHandler Button_Cancel.Click, AddressOf Button_Cancel_Click
        End Sub

#Region " Windows Form Designer generated code "

        'UserControl overrides dispose to clean up the component list.
        Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
            If disposing Then
                If components IsNot Nothing Then
                    components.Dispose()
                End If
            End If
            MyBase.Dispose(disposing)
        End Sub

        'Required by the Windows Form Designer
        Private components As System.ComponentModel.IContainer

        'NOTE: The following procedure is required by the Windows Form Designer
        'It can be modified using the Windows Form Designer.  
        'Do not modify it using the code editor.
        Protected Friend WithEvents GridControl1 As DevExpress.XtraGrid.GridControl
        Protected Friend WithEvents GridView1 As DevExpress.XtraGrid.Views.Grid.GridView
        Protected Friend WithEvents GridColumn_creditor_register As DevExpress.XtraGrid.Columns.GridColumn
        Protected Friend WithEvents GridColumn_message As DevExpress.XtraGrid.Columns.GridColumn
        Protected Friend WithEvents GridColumn_date_created As DevExpress.XtraGrid.Columns.GridColumn
        Protected Friend WithEvents GridColumn_created_by As DevExpress.XtraGrid.Columns.GridColumn
        Protected Friend WithEvents GridColumn_credit_amt As DevExpress.XtraGrid.Columns.GridColumn
        Protected Friend WithEvents Button_OK As DevExpress.XtraEditors.SimpleButton
        Protected Friend WithEvents Button_Cancel As DevExpress.XtraEditors.SimpleButton
        <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
            Me.GridControl1 = New DevExpress.XtraGrid.GridControl
            Me.GridView1 = New DevExpress.XtraGrid.Views.Grid.GridView
            Me.GridColumn_creditor_register = New DevExpress.XtraGrid.Columns.GridColumn
            Me.GridColumn_creditor_register.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.True
            Me.GridColumn_message = New DevExpress.XtraGrid.Columns.GridColumn
            Me.GridColumn_message.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.True
            Me.GridColumn_date_created = New DevExpress.XtraGrid.Columns.GridColumn
            Me.GridColumn_date_created.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.True
            Me.GridColumn_created_by = New DevExpress.XtraGrid.Columns.GridColumn
            Me.GridColumn_created_by.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.True
            Me.GridColumn_credit_amt = New DevExpress.XtraGrid.Columns.GridColumn
            Me.GridColumn_credit_amt.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.True
            Me.Button_OK = New DevExpress.XtraEditors.SimpleButton
            Me.Button_Cancel = New DevExpress.XtraEditors.SimpleButton
            CType(Me.GridControl1, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.GridView1, System.ComponentModel.ISupportInitialize).BeginInit()
            Me.SuspendLayout()
            '
            'GridControl1
            '
            Me.GridControl1.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                        Or System.Windows.Forms.AnchorStyles.Left) _
                        Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
            '
            'GridControl1.EmbeddedNavigator
            '
            Me.GridControl1.EmbeddedNavigator.Name = ""
            Me.GridControl1.Location = New System.Drawing.Point(0, 0)
            Me.GridControl1.MainView = Me.GridView1
            Me.GridControl1.Name = "GridControl1"
            Me.GridControl1.Size = New System.Drawing.Size(440, 352)
            Me.GridControl1.TabIndex = 0
            Me.GridControl1.ViewCollection.AddRange(New DevExpress.XtraGrid.Views.Base.BaseView() {Me.GridView1})
            '
            'GridView1
            '
            Me.GridView1.Appearance.ColumnFilterButton.BackColor = System.Drawing.Color.FromArgb(CType(104, Byte), CType(184, Byte), CType(251, Byte))
            Me.GridView1.Appearance.ColumnFilterButton.BorderColor = System.Drawing.Color.FromArgb(CType(104, Byte), CType(184, Byte), CType(251, Byte))
            Me.GridView1.Appearance.ColumnFilterButton.ForeColor = System.Drawing.Color.White
            Me.GridView1.Appearance.ColumnFilterButton.Options.UseBackColor = True
            Me.GridView1.Appearance.ColumnFilterButton.Options.UseBorderColor = True
            Me.GridView1.Appearance.ColumnFilterButton.Options.UseForeColor = True
            Me.GridView1.Appearance.ColumnFilterButtonActive.BackColor = System.Drawing.Color.FromArgb(CType(170, Byte), CType(216, Byte), CType(254, Byte))
            Me.GridView1.Appearance.ColumnFilterButtonActive.BorderColor = System.Drawing.Color.FromArgb(CType(170, Byte), CType(216, Byte), CType(254, Byte))
            Me.GridView1.Appearance.ColumnFilterButtonActive.ForeColor = System.Drawing.Color.Black
            Me.GridView1.Appearance.ColumnFilterButtonActive.Options.UseBackColor = True
            Me.GridView1.Appearance.ColumnFilterButtonActive.Options.UseBorderColor = True
            Me.GridView1.Appearance.ColumnFilterButtonActive.Options.UseForeColor = True
            Me.GridView1.Appearance.Empty.BackColor = System.Drawing.Color.FromArgb(CType(236, Byte), CType(246, Byte), CType(255, Byte))
            Me.GridView1.Appearance.Empty.BackColor2 = System.Drawing.Color.White
            Me.GridView1.Appearance.Empty.Options.UseBackColor = True
            Me.GridView1.Appearance.EvenRow.BackColor = System.Drawing.Color.FromArgb(CType(247, Byte), CType(251, Byte), CType(255, Byte))
            Me.GridView1.Appearance.EvenRow.BorderColor = System.Drawing.Color.FromArgb(CType(247, Byte), CType(251, Byte), CType(255, Byte))
            Me.GridView1.Appearance.EvenRow.ForeColor = System.Drawing.Color.Black
            Me.GridView1.Appearance.EvenRow.Options.UseBackColor = True
            Me.GridView1.Appearance.EvenRow.Options.UseBorderColor = True
            Me.GridView1.Appearance.EvenRow.Options.UseForeColor = True
            Me.GridView1.Appearance.FilterCloseButton.BackColor = System.Drawing.Color.FromArgb(CType(104, Byte), CType(184, Byte), CType(251, Byte))
            Me.GridView1.Appearance.FilterCloseButton.BorderColor = System.Drawing.Color.FromArgb(CType(104, Byte), CType(184, Byte), CType(251, Byte))
            Me.GridView1.Appearance.FilterCloseButton.ForeColor = System.Drawing.Color.White
            Me.GridView1.Appearance.FilterCloseButton.Options.UseBackColor = True
            Me.GridView1.Appearance.FilterCloseButton.Options.UseBorderColor = True
            Me.GridView1.Appearance.FilterCloseButton.Options.UseForeColor = True
            Me.GridView1.Appearance.FilterPanel.BackColor = System.Drawing.Color.FromArgb(CType(236, Byte), CType(246, Byte), CType(255, Byte))
            Me.GridView1.Appearance.FilterPanel.BackColor2 = System.Drawing.Color.White
            Me.GridView1.Appearance.FilterPanel.ForeColor = System.Drawing.Color.Black
            Me.GridView1.Appearance.FilterPanel.Options.UseBackColor = True
            Me.GridView1.Appearance.FilterPanel.Options.UseForeColor = True
            Me.GridView1.Appearance.FixedLine.BackColor = System.Drawing.Color.FromArgb(CType(59, Byte), CType(133, Byte), CType(195, Byte))
            Me.GridView1.Appearance.FixedLine.Options.UseBackColor = True
            Me.GridView1.Appearance.FocusedCell.BackColor = System.Drawing.Color.White
            Me.GridView1.Appearance.FocusedCell.ForeColor = System.Drawing.Color.Black
            Me.GridView1.Appearance.FocusedCell.Options.UseBackColor = True
            Me.GridView1.Appearance.FocusedCell.Options.UseForeColor = True
            Me.GridView1.Appearance.FocusedRow.BackColor = System.Drawing.Color.FromArgb(CType(38, Byte), CType(109, Byte), CType(189, Byte))
            Me.GridView1.Appearance.FocusedRow.BorderColor = System.Drawing.Color.FromArgb(CType(59, Byte), CType(139, Byte), CType(206, Byte))
            Me.GridView1.Appearance.FocusedRow.ForeColor = System.Drawing.Color.White
            Me.GridView1.Appearance.FocusedRow.Options.UseBackColor = True
            Me.GridView1.Appearance.FocusedRow.Options.UseBorderColor = True
            Me.GridView1.Appearance.FocusedRow.Options.UseForeColor = True
            Me.GridView1.Appearance.FooterPanel.BackColor = System.Drawing.Color.FromArgb(CType(104, Byte), CType(184, Byte), CType(251, Byte))
            Me.GridView1.Appearance.FooterPanel.BorderColor = System.Drawing.Color.FromArgb(CType(104, Byte), CType(184, Byte), CType(251, Byte))
            Me.GridView1.Appearance.FooterPanel.ForeColor = System.Drawing.Color.Black
            Me.GridView1.Appearance.FooterPanel.Options.UseBackColor = True
            Me.GridView1.Appearance.FooterPanel.Options.UseBorderColor = True
            Me.GridView1.Appearance.FooterPanel.Options.UseForeColor = True
            Me.GridView1.Appearance.GroupButton.BackColor = System.Drawing.Color.FromArgb(CType(104, Byte), CType(184, Byte), CType(251, Byte))
            Me.GridView1.Appearance.GroupButton.BorderColor = System.Drawing.Color.FromArgb(CType(104, Byte), CType(184, Byte), CType(251, Byte))
            Me.GridView1.Appearance.GroupButton.Options.UseBackColor = True
            Me.GridView1.Appearance.GroupButton.Options.UseBorderColor = True
            Me.GridView1.Appearance.GroupFooter.BackColor = System.Drawing.Color.FromArgb(CType(170, Byte), CType(216, Byte), CType(254, Byte))
            Me.GridView1.Appearance.GroupFooter.BorderColor = System.Drawing.Color.FromArgb(CType(170, Byte), CType(216, Byte), CType(254, Byte))
            Me.GridView1.Appearance.GroupFooter.ForeColor = System.Drawing.Color.Black
            Me.GridView1.Appearance.GroupFooter.Options.UseBackColor = True
            Me.GridView1.Appearance.GroupFooter.Options.UseBorderColor = True
            Me.GridView1.Appearance.GroupFooter.Options.UseForeColor = True
            Me.GridView1.Appearance.GroupPanel.BackColor = System.Drawing.Color.FromArgb(CType(236, Byte), CType(246, Byte), CType(255, Byte))
            Me.GridView1.Appearance.GroupPanel.BackColor2 = System.Drawing.Color.White
            Me.GridView1.Appearance.GroupPanel.ForeColor = System.Drawing.Color.Black
            Me.GridView1.Appearance.GroupPanel.Options.UseBackColor = True
            Me.GridView1.Appearance.GroupPanel.Options.UseForeColor = True
            Me.GridView1.Appearance.GroupRow.BackColor = System.Drawing.Color.FromArgb(CType(170, Byte), CType(216, Byte), CType(254, Byte))
            Me.GridView1.Appearance.GroupRow.BorderColor = System.Drawing.Color.FromArgb(CType(170, Byte), CType(216, Byte), CType(254, Byte))
            Me.GridView1.Appearance.GroupRow.ForeColor = System.Drawing.Color.Black
            Me.GridView1.Appearance.GroupRow.Options.UseBackColor = True
            Me.GridView1.Appearance.GroupRow.Options.UseBorderColor = True
            Me.GridView1.Appearance.GroupRow.Options.UseForeColor = True
            Me.GridView1.Appearance.HeaderPanel.BackColor = System.Drawing.Color.FromArgb(CType(139, Byte), CType(201, Byte), CType(254, Byte))
            Me.GridView1.Appearance.HeaderPanel.BorderColor = System.Drawing.Color.FromArgb(CType(139, Byte), CType(201, Byte), CType(254, Byte))
            Me.GridView1.Appearance.HeaderPanel.ForeColor = System.Drawing.Color.Black
            Me.GridView1.Appearance.HeaderPanel.Options.UseBackColor = True
            Me.GridView1.Appearance.HeaderPanel.Options.UseBorderColor = True
            Me.GridView1.Appearance.HeaderPanel.Options.UseForeColor = True
            Me.GridView1.Appearance.HideSelectionRow.BackColor = System.Drawing.Color.FromArgb(CType(105, Byte), CType(170, Byte), CType(225, Byte))
            Me.GridView1.Appearance.HideSelectionRow.BorderColor = System.Drawing.Color.FromArgb(CType(83, Byte), CType(155, Byte), CType(215, Byte))
            Me.GridView1.Appearance.HideSelectionRow.ForeColor = System.Drawing.Color.FromArgb(CType(236, Byte), CType(246, Byte), CType(255, Byte))
            Me.GridView1.Appearance.HideSelectionRow.Options.UseBackColor = True
            Me.GridView1.Appearance.HideSelectionRow.Options.UseBorderColor = True
            Me.GridView1.Appearance.HideSelectionRow.Options.UseForeColor = True
            Me.GridView1.Appearance.HorzLine.BackColor = System.Drawing.Color.FromArgb(CType(104, Byte), CType(184, Byte), CType(251, Byte))
            Me.GridView1.Appearance.HorzLine.Options.UseBackColor = True
            Me.GridView1.Appearance.OddRow.BackColor = System.Drawing.Color.FromArgb(CType(236, Byte), CType(246, Byte), CType(255, Byte))
            Me.GridView1.Appearance.OddRow.BorderColor = System.Drawing.Color.FromArgb(CType(236, Byte), CType(246, Byte), CType(255, Byte))
            Me.GridView1.Appearance.OddRow.ForeColor = System.Drawing.Color.Black
            Me.GridView1.Appearance.OddRow.Options.UseBackColor = True
            Me.GridView1.Appearance.OddRow.Options.UseBorderColor = True
            Me.GridView1.Appearance.OddRow.Options.UseForeColor = True
            Me.GridView1.Appearance.Preview.Font = New System.Drawing.Font("Verdana", 7.5!)
            Me.GridView1.Appearance.Preview.ForeColor = System.Drawing.Color.FromArgb(CType(83, Byte), CType(155, Byte), CType(215, Byte))
            Me.GridView1.Appearance.Preview.Options.UseFont = True
            Me.GridView1.Appearance.Preview.Options.UseForeColor = True
            Me.GridView1.Appearance.Row.BackColor = System.Drawing.Color.FromArgb(CType(247, Byte), CType(251, Byte), CType(255, Byte))
            Me.GridView1.Appearance.Row.ForeColor = System.Drawing.Color.Black
            Me.GridView1.Appearance.Row.Options.UseBackColor = True
            Me.GridView1.Appearance.Row.Options.UseForeColor = True
            Me.GridView1.Appearance.RowSeparator.BackColor = System.Drawing.Color.FromArgb(CType(236, Byte), CType(246, Byte), CType(255, Byte))
            Me.GridView1.Appearance.RowSeparator.BackColor2 = System.Drawing.Color.White
            Me.GridView1.Appearance.RowSeparator.Options.UseBackColor = True
            Me.GridView1.Appearance.SelectedRow.BackColor = System.Drawing.Color.FromArgb(CType(83, Byte), CType(155, Byte), CType(215, Byte))
            Me.GridView1.Appearance.SelectedRow.ForeColor = System.Drawing.Color.White
            Me.GridView1.Appearance.SelectedRow.Options.UseBackColor = True
            Me.GridView1.Appearance.SelectedRow.Options.UseForeColor = True
            Me.GridView1.Appearance.TopNewRow.BackColor = System.Drawing.Color.White
            Me.GridView1.Appearance.TopNewRow.Options.UseBackColor = True
            Me.GridView1.Appearance.VertLine.BackColor = System.Drawing.Color.FromArgb(CType(104, Byte), CType(184, Byte), CType(251, Byte))
            Me.GridView1.Appearance.VertLine.Options.UseBackColor = True
            Me.GridView1.Columns.AddRange(New DevExpress.XtraGrid.Columns.GridColumn() {Me.GridColumn_creditor_register, Me.GridColumn_message, Me.GridColumn_date_created, Me.GridColumn_created_by, Me.GridColumn_credit_amt})
            Me.GridView1.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus
            Me.GridView1.GridControl = Me.GridControl1
            Me.GridView1.Name = "GridView1"
            Me.GridView1.OptionsBehavior.Editable = False
            Me.GridView1.OptionsView.ShowGroupPanel = False
            Me.GridView1.OptionsView.ShowIndicator = False
            Me.GridView1.ShowButtonMode = DevExpress.XtraGrid.Views.Base.ShowButtonModeEnum.ShowForFocusedRow
            '
            'GridColumn_creditor_register
            '
            Me.GridColumn_creditor_register.AppearanceCell.Options.UseTextOptions = True
            Me.GridColumn_creditor_register.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
            Me.GridColumn_creditor_register.AppearanceHeader.Options.UseTextOptions = True
            Me.GridColumn_creditor_register.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
            Me.GridColumn_creditor_register.Caption = "ID"
            Me.GridColumn_creditor_register.CustomizationCaption = "ID for the check"
            Me.GridColumn_creditor_register.DisplayFormat.FormatString = "f0"
            Me.GridColumn_creditor_register.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
            Me.GridColumn_creditor_register.FieldName = "creditor_register"
            Me.GridColumn_creditor_register.GroupFormat.FormatString = "f0"
            Me.GridColumn_creditor_register.GroupFormat.FormatType = DevExpress.Utils.FormatType.Numeric
            Me.GridColumn_creditor_register.Name = "GridColumn_creditor_register"
            Me.GridColumn_creditor_register.Visible = True
            Me.GridColumn_creditor_register.VisibleIndex = 0
            Me.GridColumn_creditor_register.Width = 63
            '
            'GridColumn_message
            '
            Me.GridColumn_message.Caption = "Reference"
            Me.GridColumn_message.CustomizationCaption = "Check Number Reference"
            Me.GridColumn_message.FieldName = "message"
            Me.GridColumn_message.Name = "GridColumn_message"
            Me.GridColumn_message.Visible = True
            Me.GridColumn_message.VisibleIndex = 1
            Me.GridColumn_message.Width = 119
            '
            'GridColumn_date_created
            '
            Me.GridColumn_date_created.AppearanceCell.Options.UseTextOptions = True
            Me.GridColumn_date_created.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
            Me.GridColumn_date_created.AppearanceHeader.Options.UseTextOptions = True
            Me.GridColumn_date_created.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
            Me.GridColumn_date_created.Caption = "Date Received"
            Me.GridColumn_date_created.CustomizationCaption = "Date check received"
            Me.GridColumn_date_created.DisplayFormat.FormatString = "d"
            Me.GridColumn_date_created.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime
            Me.GridColumn_date_created.FieldName = "date_created"
            Me.GridColumn_date_created.GroupFormat.FormatString = "d"
            Me.GridColumn_date_created.GroupFormat.FormatType = DevExpress.Utils.FormatType.DateTime
            Me.GridColumn_date_created.GroupInterval = DevExpress.XtraGrid.ColumnGroupInterval.Date
            Me.GridColumn_date_created.Name = "GridColumn_date_created"
            Me.GridColumn_date_created.Visible = True
            Me.GridColumn_date_created.VisibleIndex = 2
            Me.GridColumn_date_created.Width = 119
            '
            'GridColumn_created_by
            '
            Me.GridColumn_created_by.Caption = "Creator"
            Me.GridColumn_created_by.CustomizationCaption = "Person who created the refund"
            Me.GridColumn_created_by.FieldName = "created_by"
            Me.GridColumn_created_by.Name = "GridColumn_created_by"
            Me.GridColumn_created_by.Visible = True
            Me.GridColumn_created_by.VisibleIndex = 3
            Me.GridColumn_created_by.Width = 119
            '
            'GridColumn_credit_amt
            '
            Me.GridColumn_credit_amt.AppearanceCell.Options.UseTextOptions = True
            Me.GridColumn_credit_amt.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
            Me.GridColumn_credit_amt.AppearanceHeader.Options.UseTextOptions = True
            Me.GridColumn_credit_amt.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
            Me.GridColumn_credit_amt.Caption = "Amount"
            Me.GridColumn_credit_amt.CustomizationCaption = "Amount of the refund"
            Me.GridColumn_credit_amt.DisplayFormat.FormatString = "c"
            Me.GridColumn_credit_amt.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
            Me.GridColumn_credit_amt.FieldName = "credit_amt"
            Me.GridColumn_credit_amt.GroupFormat.FormatString = "c"
            Me.GridColumn_credit_amt.GroupFormat.FormatType = DevExpress.Utils.FormatType.Numeric
            Me.GridColumn_credit_amt.Name = "GridColumn_credit_amt"
            Me.GridColumn_credit_amt.Visible = True
            Me.GridColumn_credit_amt.VisibleIndex = 4
            Me.GridColumn_credit_amt.Width = 120
            '
            'Button_OK
            '
            Me.Button_OK.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
            Me.Button_OK.Enabled = False
            Me.Button_OK.Location = New System.Drawing.Point(456, 8)
            Me.Button_OK.Name = "Button_OK"
            Me.Button_OK.TabIndex = 1
            Me.Button_OK.Text = "&OK"
            Me.Button_OK.ToolTip = "Click here to select the indicated row from the grid"
            '
            'Button_Cancel
            '
            Me.Button_Cancel.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
            Me.Button_Cancel.CausesValidation = False
            Me.Button_Cancel.Location = New System.Drawing.Point(456, 56)
            Me.Button_Cancel.Name = "Button_Cancel"
            Me.Button_Cancel.TabIndex = 2
            Me.Button_Cancel.Text = "&Cancel"
            Me.Button_Cancel.ToolTip = "Click here to cancel the form and return to the previous screen"
            '
            'VoidCreditorCheckList
            '
            Me.Controls.Add(Me.Button_Cancel)
            Me.Controls.Add(Me.Button_OK)
            Me.Controls.Add(Me.GridControl1)
            Me.Name = "VoidCreditorCheckList"
            Me.Size = New System.Drawing.Size(544, 352)
            CType(Me.GridControl1, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.GridView1, System.ComponentModel.ISupportInitialize).EndInit()
            Me.ResumeLayout(False)

        End Sub

#End Region

        ''' <summary>
        ''' Reload the creditor grid with the creditor information
        ''' </summary>
        Public Sub Reload(ByVal Creditor As String)
            Dim cmd As SqlClient.SqlCommand = New SqlCommand
            With cmd
                .Connection = New SqlConnection(DebtPlus.LINQ.SQLInfoClass.getDefault().ConnectionString)
                .CommandText = "SELECT [creditor_register],[tran_type],[creditor],[item_date],[fairshare_pct],[disbursement_register],[trust_register],[invoice_register],[debit_amt],[credit_amt],[message],[date_created],[created_by] FROM registers_creditor WHERE creditor=@creditor AND tran_type='RF'"
                .Parameters.Add("@creditor", SqlDbType.VarChar, 10).Value = Creditor
            End With

            Dim current_cursor As Cursor = System.Windows.Forms.Cursor.Current
            Try
                System.Windows.Forms.Cursor.Current = Cursors.WaitCursor
                ds.Clear()
                Dim da As New SqlClient.SqlDataAdapter(cmd)
                da.Fill(ds, "registers_creditor")

                With GridControl1
                    .DataSource = ds.Tables(0).DefaultView
                    .RefreshDataSource()
                End With

            Catch ex As SqlClient.SqlException
                DebtPlus.UI.Common.ErrorHandling.HandleErrors(ex, "Error reading registers_creditor")

            Finally
                System.Windows.Forms.Cursor.Current = current_cursor
            End Try

            ' If there are no transactions then say so
            If GridView1.RowCount <= 0 Then
                DebtPlus.Data.Forms.MessageBox.Show("There are no creditor refund transactions to void for this creditor", "Sorry, but there is no data", MessageBoxButtons.OK, MessageBoxIcon.Information)
            End If
        End Sub

        ''' <summary>
        ''' Process a click event on the grid
        ''' </summary>
        Private Sub GridControl1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)

            Dim ctl As DevExpress.XtraGrid.GridControl = CType(sender, DevExpress.XtraGrid.GridControl)
            Dim gv As DevExpress.XtraGrid.Views.Grid.GridView = CType(ctl.DefaultView, DevExpress.XtraGrid.Views.Grid.GridView)
            Dim hi As DevExpress.XtraGrid.Views.Grid.ViewInfo.GridHitInfo = gv.CalcHitInfo((ctl.PointToClient(Windows.Forms.Control.MousePosition)))

            If hi.IsValid AndAlso hi.InRow Then
                Dim RowHandle As System.Int32 = hi.RowHandle
                GridView1.FocusedRowHandle = RowHandle
                Button_OK.Enabled = RowHandle >= 0
            Else
                Button_OK.Enabled = False
            End If
        End Sub

        ''' <summary>
        ''' Process a double-click event on the grid
        ''' </summary>
        Private Sub GridView1_DoubleClick(ByVal sender As Object, ByVal e As System.EventArgs)

            ' Find the row targetted as the double-click item
            Dim gv As DevExpress.XtraGrid.Views.Grid.GridView = CType(sender, DevExpress.XtraGrid.Views.Grid.GridView)
            Dim ctl As DevExpress.XtraGrid.GridControl = gv.GridControl
            Dim hi As DevExpress.XtraGrid.Views.Grid.ViewInfo.GridHitInfo = gv.CalcHitInfo((ctl.PointToClient(Windows.Forms.Control.MousePosition)))

            ' If there is a row then process the double-click event. Ignore it if there is no row.
            If hi.IsValid AndAlso hi.InRow Then
                Dim RowHandle As System.Int32 = hi.RowHandle
                If RowHandle >= 0 Then

                    ' If there is a row then process the double-click event. Ignore it if there is no row.
                    Dim row As DataRow = Nothing
                    If RowHandle >= 0 Then row = CType(gv.GetDataRow(RowHandle), System.Data.DataRow)

                    ' From the datasource, we can find the client in the table.
                    If row IsNot Nothing Then RaiseEvent Selected(Me, Convert.ToInt32(row("creditor_register")))
                End If
            End If
        End Sub

        ''' <summary>
        ''' Process a click on the CANCEL button
        ''' </summary>
        Public Event Cancelled As System.EventHandler

        Protected Sub RaiseCancelled()
            RaiseEvent Cancelled(Me, EventArgs.Empty)
        End Sub

        Private Sub Button_Cancel_Click(ByVal sender As Object, ByVal e As System.EventArgs)
            RaiseCancelled()
        End Sub

        ''' <summary>
        ''' Process a click on the OK button
        ''' </summary>
        Protected Sub RaiseSelected(ByVal creditor_register As System.Int32)
            RaiseEvent Selected(Me, creditor_register)
        End Sub

        Private Sub Button_OK_Click(ByVal sender As Object, ByVal e As System.EventArgs)
            Dim RowHandle As System.Int32 = GridView1.FocusedRowHandle
            Dim row As System.Data.DataRow = Nothing
            If RowHandle >= 0 Then row = GridView1.GetDataRow(RowHandle)
            If row IsNot Nothing Then RaiseSelected(Convert.ToInt32(row("creditor_register")))
        End Sub
    End Class
End Namespace

