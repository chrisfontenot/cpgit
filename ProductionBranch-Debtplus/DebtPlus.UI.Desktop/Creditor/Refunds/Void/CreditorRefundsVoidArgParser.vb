Option Compare Binary
Option Explicit On 
Option Strict On

Namespace Creditor.Refunds.Void
    Friend Class CreditorRefundsVoidArgParser
        Inherits DebtPlus.Utils.ArgParserBase

        ''' <summary>
        ''' Create an instance of our class
        ''' </summary>
        Public Sub New()
            MyBase.New(New String() {})
        End Sub

        ''' <summary>
        ''' Generate the command usage information
        ''' </summary>
        Protected Overrides Sub OnUsage(ByVal errorInfo As String)
            If errorInfo IsNot Nothing Then
                AppendErrorLine(String.Format("Invalid parameter: {0}" + Environment.NewLine, errorInfo))
            End If

            AppendErrorLine("Usage: DebtPlus.Creditor.Refunds.Void.exe")
        End Sub
    End Class
End Namespace
