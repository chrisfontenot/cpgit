Option Compare Binary
Option Explicit On
Option Strict On

Imports System.Configuration
Imports System.Collections.Specialized
Imports System.Globalization

Namespace Creditor.Refunds
    Public Class DepositArgParser
        Inherits DebtPlus.Utils.ArgParserBase

        Private _batch As Int32 = -1

        Public Property Batch() As Int32
            Get
                Return _batch
            End Get
            Set(ByVal Value As Int32)
                _batch = Value
            End Set
        End Property

        Public Sub New()
            MyBase.New(New String() {"b"})
            Batch = -1
        End Sub

        Protected Overrides Function onSwitch(ByVal switchName As String, ByVal switchValue As String) As SwitchStatus
            Dim ss As SwitchStatus = DebtPlus.Utils.ArgParserBase.SwitchStatus.NoError
            Select Case switchName.ToLower()
                Case "b"
                    If Int32.TryParse(switchValue, NumberStyles.Integer, CultureInfo.CurrentCulture, Batch) AndAlso Batch <= 0 Then
                        ss = DebtPlus.Utils.ArgParserBase.SwitchStatus.YesError
                    End If

                Case "?"
                    ss = DebtPlus.Utils.ArgParserBase.SwitchStatus.ShowUsage

                Case Else
                    ss = DebtPlus.Utils.ArgParserBase.SwitchStatus.YesError
            End Select
            Return ss
        End Function
    End Class
End Namespace