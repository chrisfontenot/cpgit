Option Compare Binary
Option Explicit On
Option Strict On

Namespace Creditor.Refunds
    Public Class CreditorRefundsContext
        Public ap As New DepositArgParser
        Public ds As New DataSet("ds")

        Private privateEnterMoveNextControl As Boolean = False
        Public Property EnterMoveNextControl As Boolean
            Get
                Return privateEnterMoveNextControl
            End Get
            Private Set(value As Boolean)
                privateEnterMoveNextControl = value
            End Set
        End Property

        Public Sub New()

            ' Find the flag for the mapping of the kepad tab to the enter button sequence.
            Dim TempString As String = DebtPlus.Configuration.Config.GetConfigValueForKey(DebtPlus.Configuration.Config.GetConfigSettings(DebtPlus.Configuration.Config.ConfigSet.DepositManual), "EnterMoveNextControl")
            If Not Boolean.TryParse(TempString, EnterMoveNextControl) Then
                EnterMoveNextControl = False
            End If
        End Sub
    End Class
End Namespace