#Region "Copyright 2000-2012 DebtPlus, L.L.C."
'{*******************************************************************}
'{                                                                   }
'{       DebtPlus Debt Management System                             }
'{                                                                   }
'{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
'{       ALL RIGHTS RESERVED                                         }
'{                                                                   }
'{   The entire contents of this file is protected by U.S. and       }
'{   International Copyright Laws. Unauthorized reproduction,        }
'{   reverse-engineering, and distribution of all or any portion of  }
'{   the code contained in this file is strictly prohibited and may  }
'{   result in severe civil and criminal penalties and will be       }
'{   prosecuted to the maximum extent possible under the law.        }
'{                                                                   }
'{   RESTRICTIONS                                                    }
'{                                                                   }
'{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
'{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
'{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
'{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
'{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
'{                                                                   }
'{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
'{   ADDITIONAL RESTRICTIONS.                                        }
'{                                                                   }
'{*******************************************************************}

#End Region
Imports DevExpress.XtraBars
Imports System.Data.SqlClient
Imports System.Windows.Forms

Namespace Creditor.Refunds
    ' Singleton
    Friend Class Form_CreditorRefund

        Private ReadOnly ctx As CreditorRefundsContext = Nothing

        Private Sub New()
            MyBase.New()
            InitializeComponent()
        End Sub

        Public Sub New(ByVal ctx As CreditorRefundsContext)
            MyBase.New()
            Me.ctx = ctx
            InitializeComponent()
            AddHandler Me.Load, AddressOf Form_CreditorRefund_Load
            AddHandler BarButtonItem1.ItemClick, AddressOf BarButtonItem1_ItemClick
            AddHandler Me.FormClosing, AddressOf Form_FormClosing
        End Sub

        ''' <summary>
        ''' Process the one-time load event for the form
        ''' </summary>
        Private Sub Form_CreditorRefund_Load(ByVal sender As Object, ByVal e As EventArgs)

            ' If there is no batch then use the form realestate to ask for a batch.
            If ctx.ap.Batch <= 0 Then

                With CreditorRefundControl1
                    .SendToBack()
                    .Visible = False
                End With

                With NewCreditorRefundBatch1
                    .Visible = True
                    .BringToFront()
                    .ReadForm()
                    .Focus()
                    AddHandler .Cancelled, AddressOf NewCreditorRefundBatch1_Cancelled
                    AddHandler .Selected, AddressOf NewCreditorRefundBatch1_Selected
                End With
            Else
                ' Otherwise, do the deposits for this batch
                PerformDeposits()
            End If
        End Sub

        ''' <summary>
        ''' If the entry is to cancel the form then close it
        ''' </summary>
        Private Sub NewCreditorRefundBatch1_Cancelled(ByVal sender As Object, ByVal e As EventArgs)
            Close()
        End Sub

        ''' <summary>
        ''' When a new batch is selected then process the batch updates
        ''' </summary>
        Private Sub NewCreditorRefundBatch1_Selected(ByVal sender As Object, ByVal e As EventArgs)
            ctx.ap.Batch = NewCreditorRefundBatch1.DepositBatchID
            PerformDeposits()
        End Sub

        ''' <summary>
        ''' Come here once we have a valid batch ID to start the updates
        ''' </summary>
        Private Sub PerformDeposits()

            ' Make the batch selection control to the back
            With NewCreditorRefundBatch1
                .SendToBack()
                .Visible = False
            End With

            ' Bring forward the batch update control. It will take it from here.
            With CreditorRefundControl1
                .BringToFront()
                .Visible = True
                .ReadForm()
                .Focus()
            End With
        End Sub

        ''' <summary>
        ''' Process the MENU FILE -> CLOSE operation
        ''' </summary>
        Private Sub BarButtonItem1_ItemClick(ByVal sender As Object, ByVal e As ItemClickEventArgs)
            Close()
        End Sub

        ''' <summary>
        ''' If the form is closing then check for lost updates
        ''' </summary>
        Private Sub Form_FormClosing(ByVal sender As Object, ByVal e As FormClosingEventArgs)

            ' If there is no batch then simply return to close the form
            If ctx.ap.Batch <= 0 Then Return

            ' Look to see if some transactions were made. If so, ask what to do.
            If PendingTransactions() Then
                Select Case DebtPlus.Data.Forms.MessageBox.Show("Warning: You have made changes to this batch." + Environment.NewLine + "Do you wish to save these transactions now?", "Are you sure", MessageBoxButtons.YesNoCancel, MessageBoxIcon.Exclamation, MessageBoxDefaultButton.Button1)
                    Case DialogResult.Cancel
                        e.Cancel = True
                        Return

                    Case DialogResult.Yes
                        If SaveTransactions() < 0 Then
                            e.Cancel = True
                            Return
                        End If

                        If DebtPlus.Data.Forms.MessageBox.Show("Do you also wish to close/post the batch?" + Environment.NewLine + Environment.NewLine + "(Once the batch is closed you may not make any changes to the batch. It must be closed to be posted.)", "Close the batch?", MessageBoxButtons.YesNo) = DialogResult.Yes Then
                            CloseBatch()
                        End If

                    Case Else
                        ' Assume "no" as the response.
                End Select

            Else

                If DebtPlus.Data.Forms.MessageBox.Show("You did not make any changes to this batch" + Environment.NewLine + "Do you wish to close/post the batch anyway?" + Environment.NewLine + Environment.NewLine + "(Once the batch is closed you may not make any changes to the batch. It must be closed to be posted.)", "Close the batch?", MessageBoxButtons.YesNo) = DialogResult.Yes Then
                    CloseBatch()
                End If
            End If

            ' Ensure that re-entry calls do nothing.
            ctx.ap.Batch = 0
        End Sub

        ''' <summary>
        ''' Determine if there are transactions still pending to be recorded
        ''' </summary>
        Friend Function PendingTransactions() As Boolean
            If ctx.ds.Tables("deposit_batch_details") IsNot Nothing AndAlso ctx.ds.Tables("deposit_batch_details").GetChanges IsNot Nothing Then Return True
            Return False
        End Function

        ''' <summary>
        ''' Save the transactions to the database
        ''' </summary>
        Friend Function SaveTransactions() As Int32
            Dim rowsUpdated As Int32 = -1
            Dim tbl As DataTable = ctx.ds.Tables("deposit_batch_details")

            ' Update the database with the changed items
            Dim cn As SqlConnection = New SqlConnection(DebtPlus.LINQ.SQLInfoClass.getDefault().ConnectionString)
            Dim txn As SqlTransaction = Nothing
            Try
                UseWaitCursor = True
                cn.Open()
                txn = cn.BeginTransaction

                Dim da As New SqlDataAdapter
                da.UpdateCommand = UpdateCommand(cn, txn)
                da.InsertCommand = InsertCommand(cn, txn)
                da.DeleteCommand = DeleteCommand(cn, txn)

                ' Perform the pending updates on the database
                rowsUpdated = da.Update(tbl)

                ' Commit the changes
                txn.Commit()
                txn = Nothing

            Catch ex As DBConcurrencyException
                DebtPlus.Data.Forms.MessageBox.Show("The transaction(s) are no longer valid for this batch. The most common cause is that the batch has been posted and edits are no longer permitted.", "Database Consistency Check Violation", MessageBoxButtons.OK, MessageBoxIcon.Asterisk)

            Catch ex As Exception
                DebtPlus.UI.Common.ErrorHandling.HandleErrors(ex, "Error updating the deposit batch details")

            Finally
                If txn IsNot Nothing Then
                    Try
                        txn.Rollback()
                    Catch exRollback As Exception
                    End Try
                    txn.Dispose()
                    txn = Nothing
                End If

                If cn IsNot Nothing Then cn.Dispose()
                UseWaitCursor = False
            End Try

            Return rowsUpdated
        End Function

        ''' <summary>
        ''' Generate the update statement
        ''' </summary>
        Private Function UpdateCommand(ByRef cn As SqlConnection, ByRef txn As SqlTransaction) As SqlCommand
            Dim cmd As SqlCommand = New SqlCommand
            With cmd
                .Connection = cn
                .Transaction = txn
                .CommandText = "UPDATE deposit_batch_details SET client=cc.client, creditor=cc.creditor, client_creditor=cc.client_creditor, amount=@amount, fairshare_pct=@fairshare_pct, fairshare_amt=@fairshare_amt, creditor_type=@creditor_type, reference=@reference FROM deposit_batch_details d INNER JOIN client_creditor cc ON cc.client_creditor=@client_creditor WHERE deposit_batch_detail=@deposit_batch_detail"
                .CommandType = CommandType.Text
                .Parameters.Add("@client_creditor", SqlDbType.Int, 0, "client_creditor")
                .Parameters.Add("@amount", SqlDbType.Decimal, 0, "amount")
                .Parameters.Add("@fairshare_pct", SqlDbType.Float, 0, "fairshare_pct")
                .Parameters.Add("@fairshare_amt", SqlDbType.Decimal, 0, "fairshare_amt")
                .Parameters.Add("@creditor_type", SqlDbType.VarChar, 10, "creditor_type")
                .Parameters.Add("@reference", SqlDbType.VarChar, 50, "reference")
                .Parameters.Add("@deposit_batch_detail", SqlDbType.Int, 0, "deposit_batch_detail")
            End With
            Return cmd
        End Function

        ''' <summary>
        ''' Generate the insert statement
        ''' </summary>
        Private Function InsertCommand(ByRef cn As SqlConnection, ByRef txn As SqlTransaction) As SqlCommand
            Dim cmd As SqlCommand = New SqlCommand
            With cmd
                .Connection = cn
                .Transaction = txn
                .CommandText = "INSERT INTO deposit_batch_details (deposit_batch_id, client, creditor, client_creditor, amount, fairshare_pct, fairshare_amt, creditor_type, reference) SELECT @deposit_batch_id, cc.client, cc.creditor, cc.client_creditor, @amount, @fairshare_pct, @fairshare_amt, @creditor_type, @reference FROM client_creditor cc WHERE cc.client_creditor = @client_creditor"
                .CommandType = CommandType.Text
                .Parameters.Add("@client_creditor", SqlDbType.Int, 0, "client_creditor")
                .Parameters.Add("@amount", SqlDbType.Decimal, 0, "amount")
                .Parameters.Add("@fairshare_amt", SqlDbType.Decimal, 0, "fairshare_amt")
                .Parameters.Add("@fairshare_pct", SqlDbType.Float, 0, "fairshare_pct")
                .Parameters.Add("@creditor_type", SqlDbType.VarChar, 10, "creditor_type")
                .Parameters.Add("@reference", SqlDbType.VarChar, 50, "reference")
                .Parameters.Add("@deposit_batch_id", SqlDbType.Int).Value = ctx.ap.Batch
            End With
            Return cmd
        End Function

        ''' <summary>
        ''' Generate the delete statement
        ''' </summary>
        Private Function DeleteCommand(ByRef cn As SqlConnection, ByRef txn As SqlTransaction) As SqlCommand
            Dim cmd As SqlCommand = New SqlCommand
            With cmd
                .Connection = cn
                .Transaction = txn
                .CommandText = "DELETE FROM deposit_batch_details WHERE deposit_batch_detail = @deposit_batch_detail AND deposit_batch_id=@deposit_batch_id"
                .CommandType = CommandType.Text
                .Parameters.Add("@deposit_batch_detail", SqlDbType.Int, 0, "deposit_batch_detail")
                .Parameters.Add("@deposit_batch_id", SqlDbType.Int).Value = ctx.ap.Batch
            End With
            Return cmd
        End Function

        ''' <summary>
        ''' Close the deposit batch
        ''' </summary>
        Private Sub CloseBatch()
            Dim cn As SqlConnection = New SqlConnection(DebtPlus.LINQ.SQLInfoClass.getDefault().ConnectionString)
            Dim txn As SqlTransaction = Nothing
            Try
                cn.Open()
                txn = cn.BeginTransaction

                ' First, close the batch
                With New SqlCommand
                    .Connection = cn
                    .Transaction = txn
                    .CommandText = "xpr_creditor_refund_batch_close"
                    .CommandType = CommandType.StoredProcedure
                    .Parameters.Add("@deposit_batch_id", SqlDbType.Int).Value = ctx.ap.Batch
                    .ExecuteNonQuery()
                End With

                ' Commit the transactions
                txn.Commit()
                txn = Nothing

            Catch ex As SqlException
                DebtPlus.UI.Common.ErrorHandling.HandleErrors(ex, "Error closing/posting the deposit batch")

            Finally
                If txn IsNot Nothing Then
                    Try
                        txn.Rollback()
                    Catch exRollback As Exception
                    End Try
                    txn.Dispose()
                    txn = Nothing
                End If

                If cn IsNot Nothing Then cn.Dispose()
            End Try
        End Sub
    End Class
End Namespace
