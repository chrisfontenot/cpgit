﻿Imports DebtPlus.UI.FormLib.Deposit

Namespace Creditor.Refunds

    <Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
    Partial Class Form_CreditorRefund
        Inherits DebtPlus.Data.Forms.DebtPlusForm

        'Form overrides dispose to clean up the component list.
        <System.Diagnostics.DebuggerNonUserCode()> _
        Protected Overrides Sub Dispose(ByVal disposing As Boolean)
            Try
                If disposing AndAlso components IsNot Nothing Then
                    components.Dispose()
                End If
            Finally
                MyBase.Dispose(disposing)
            End Try
        End Sub

        'Required by the Windows Form Designer
        Private components As System.ComponentModel.IContainer

        'NOTE: The following procedure is required by the Windows Form Designer
        'It can be modified using the Windows Form Designer.  
        'Do not modify it using the code editor.
        <System.Diagnostics.DebuggerStepThrough()> _
        Private Sub InitializeComponent()
            Me.components = New System.ComponentModel.Container
            Me.NewCreditorRefundBatch1 = New NewClientDepositBatch
            Me.CreditorRefundControl1 = New DebtPlus.UI.Desktop.Creditor.Refunds.Controls.CreditorRefundControl(Me, ctx)
            Me.barManager1 = New DevExpress.XtraBars.BarManager(Me.components)
            Me.Bar2 = New DevExpress.XtraBars.Bar
            Me.BarSubItem_File = New DevExpress.XtraBars.BarSubItem
            Me.BarButtonItem1 = New DevExpress.XtraBars.BarButtonItem
            Me.BarAndDockingController1 = New DevExpress.XtraBars.BarAndDockingController(Me.components)
            Me.barDockControlTop = New DevExpress.XtraBars.BarDockControl
            Me.barDockControlBottom = New DevExpress.XtraBars.BarDockControl
            Me.barDockControlLeft = New DevExpress.XtraBars.BarDockControl
            Me.barDockControlRight = New DevExpress.XtraBars.BarDockControl
            CType(Me.DxErrorProvider1, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.barManager1, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.BarAndDockingController1, System.ComponentModel.ISupportInitialize).BeginInit()
            Me.SuspendLayout()
            '
            'NewCreditorRefundBatch1
            '
            Me.NewCreditorRefundBatch1.BatchType = "CR"
            Me.NewCreditorRefundBatch1.Dock = System.Windows.Forms.DockStyle.Fill
            Me.NewCreditorRefundBatch1.Location = New System.Drawing.Point(0, 24)
            Me.NewCreditorRefundBatch1.Name = "NewCreditorRefundBatch1"
            Me.NewCreditorRefundBatch1.Size = New System.Drawing.Size(557, 409)
            Me.NewCreditorRefundBatch1.TabIndex = 0
            '
            'CreditorRefundControl1
            '
            Me.CreditorRefundControl1.Dock = System.Windows.Forms.DockStyle.Fill
            Me.CreditorRefundControl1.Location = New System.Drawing.Point(0, 24)
            Me.CreditorRefundControl1.Name = "CreditorRefundControl1"
            Me.CreditorRefundControl1.Size = New System.Drawing.Size(557, 409)
            Me.CreditorRefundControl1.TabIndex = 1
            '
            'barManager1
            '
            Me.barManager1.Bars.AddRange(New DevExpress.XtraBars.Bar() {Me.Bar2})
            Me.barManager1.Controller = Me.BarAndDockingController1
            Me.barManager1.DockControls.Add(Me.barDockControlTop)
            Me.barManager1.DockControls.Add(Me.barDockControlBottom)
            Me.barManager1.DockControls.Add(Me.barDockControlLeft)
            Me.barManager1.DockControls.Add(Me.barDockControlRight)
            Me.barManager1.Form = Me
            Me.barManager1.Items.AddRange(New DevExpress.XtraBars.BarItem() {Me.BarSubItem_File, Me.BarButtonItem1})
            Me.barManager1.MainMenu = Me.Bar2
            Me.barManager1.MaxItemId = 2
            '
            'Bar2
            '
            Me.Bar2.BarName = "Main menu"
            Me.Bar2.DockCol = 0
            Me.Bar2.DockRow = 0
            Me.Bar2.DockStyle = DevExpress.XtraBars.BarDockStyle.Top
            Me.Bar2.LinksPersistInfo.AddRange(New DevExpress.XtraBars.LinkPersistInfo() {New DevExpress.XtraBars.LinkPersistInfo(Me.BarSubItem_File)})
            Me.Bar2.OptionsBar.MultiLine = True
            Me.Bar2.OptionsBar.UseWholeRow = True
            Me.Bar2.Text = "Main menu"
            '
            'BarSubItem_File
            '
            Me.BarSubItem_File.Caption = "&File"
            Me.BarSubItem_File.Id = 0
            Me.BarSubItem_File.LinksPersistInfo.AddRange(New DevExpress.XtraBars.LinkPersistInfo() {New DevExpress.XtraBars.LinkPersistInfo(Me.BarButtonItem1)})
            Me.BarSubItem_File.Name = "BarSubItem_File"
            '
            'BarButtonItem1
            '
            Me.BarButtonItem1.Caption = "&Exit"
            Me.BarButtonItem1.Id = 1
            Me.BarButtonItem1.Name = "BarButtonItem1"
            '
            'Form_CreditorRefund
            '
            Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
            Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
            Me.ClientSize = New System.Drawing.Size(557, 433)
            Me.Controls.Add(Me.CreditorRefundControl1)
            Me.Controls.Add(Me.NewCreditorRefundBatch1)
            Me.Controls.Add(Me.barDockControlLeft)
            Me.Controls.Add(Me.barDockControlRight)
            Me.Controls.Add(Me.barDockControlBottom)
            Me.Controls.Add(Me.barDockControlTop)
            Me.Name = "Form_CreditorRefund"
            Me.Text = "Creditor Refunds"
            CType(Me.DxErrorProvider1, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.barManager1, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.BarAndDockingController1, System.ComponentModel.ISupportInitialize).EndInit()
            Me.ResumeLayout(False)

        End Sub
        Friend WithEvents NewCreditorRefundBatch1 As NewClientDepositBatch
        Friend WithEvents CreditorRefundControl1 As DebtPlus.UI.Desktop.Creditor.Refunds.Controls.CreditorRefundControl
        Friend WithEvents barManager1 As DevExpress.XtraBars.BarManager
        Friend WithEvents Bar2 As DevExpress.XtraBars.Bar
        Friend WithEvents BarAndDockingController1 As DevExpress.XtraBars.BarAndDockingController
        Friend WithEvents barDockControlTop As DevExpress.XtraBars.BarDockControl
        Friend WithEvents barDockControlBottom As DevExpress.XtraBars.BarDockControl
        Friend WithEvents barDockControlLeft As DevExpress.XtraBars.BarDockControl
        Friend WithEvents barDockControlRight As DevExpress.XtraBars.BarDockControl
        Friend WithEvents BarButtonItem1 As DevExpress.XtraBars.BarButtonItem
        Protected Friend WithEvents BarSubItem_File As DevExpress.XtraBars.BarSubItem

    End Class
End Namespace