#Region "Copyright 2000-2012 DebtPlus, L.L.C."
'{*******************************************************************}
'{                                                                   }
'{       DebtPlus Debt Management System                             }
'{                                                                   }
'{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
'{       ALL RIGHTS RESERVED                                         }
'{                                                                   }
'{   The entire contents of this file is protected by U.S. and       }
'{   International Copyright Laws. Unauthorized reproduction,        }
'{   reverse-engineering, and distribution of all or any portion of  }
'{   the code contained in this file is strictly prohibited and may  }
'{   result in severe civil and criminal penalties and will be       }
'{   prosecuted to the maximum extent possible under the law.        }
'{                                                                   }
'{   RESTRICTIONS                                                    }
'{                                                                   }
'{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
'{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
'{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
'{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
'{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
'{                                                                   }
'{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
'{   ADDITIONAL RESTRICTIONS.                                        }
'{                                                                   }
'{*******************************************************************}

#End Region
Imports DevExpress.XtraGrid.Views.Grid
Imports DevExpress.XtraGrid
Imports DevExpress.XtraGrid.Views.Grid.ViewInfo
Imports System.IO

Namespace Creditor.Refunds.Controls
    Friend Class BatchListControl

        Public Sub New()
            MyBase.New()
            InitializeComponent()

            AddHandler Me.Load, AddressOf MyGridControl_Load
            AddHandler GridView1.DoubleClick, AddressOf GridView1_DoubleClick
            AddHandler GridView1.Layout, AddressOf LayoutChanged
        End Sub

        ''' <summary>
        ''' Load the batch list on the first call
        ''' </summary>
        Public Sub LoadList(ByVal tbl As DataTable)

            ' Update the list with the display information
            With GridControl1
                .DataSource = tbl.DefaultView
            End With
        End Sub

        ''' <summary>
        ''' Find the item to edit
        ''' </summary>
        Private Sub GridView1_DoubleClick(ByVal sender As Object, ByVal e As EventArgs)

            ' Find the row targeted as the double-click item
            Dim gv As GridView = CType(sender, GridView)
            Dim ctl As GridControl = CType(gv.GridControl, GridControl)
            Dim hi As GridHitInfo = gv.CalcHitInfo((ctl.PointToClient(MousePosition)))
            Dim ControlRow As Int32 = hi.RowHandle

            ' Find the datarowview from the input tables.
            Dim drv As DataRowView = Nothing
            If hi.InRow AndAlso ControlRow >= 0 Then
                Dim vue As DataView = CType(ctl.DataSource, DataView)
                drv = CType(gv.GetRow(ControlRow), DataRowView)

                ' If this is not the new row then edit it with the edit control.
                If Not drv.IsNew Then
                    With CType(Parent, CreditorRefundControl)
                        .NewRefundItemControl1.GiveFocus()
                        .RefundItemControl1.TakeFocus()
                        .RefundItemControl1.ReadRecord(drv)
                    End With
                End If
            End If
        End Sub

        Private InInit As Boolean = True

        ''' <summary>
        ''' Return the directory to the saved layout information
        ''' </summary>
        Protected Overridable Function XMLBasePath() As String
            If DesignMode OrElse ParentForm Is Nothing Then Return String.Empty
            Dim BasePath As String = Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData) + Path.DirectorySeparatorChar + "DebtPlus"
            Return Path.Combine(BasePath, "Creditor.Refunds")
        End Function

        ''' <summary>
        ''' When loading the control, restore the layout from the file
        ''' </summary>
        Private Sub MyGridControl_Load(ByVal sender As Object, ByVal e As EventArgs)
            ReloadGridControlLayout()
            InInit = False
        End Sub

        ''' <summary>
        ''' Reload the layout of the grid control if needed
        ''' </summary>
        Protected Sub ReloadGridControlLayout()

            ' Find the base path to the saved file location
            Dim PathName As String = XMLBasePath()
            If Not String.IsNullOrEmpty(PathName) Then
                InInit = True
                Try
                    Dim FileName As String = Path.Combine(PathName, String.Format("{0}.Grid.xml", Name))
                    If File.Exists(FileName) Then
                        GridView1.RestoreLayoutFromXml(FileName)
                    End If
                Catch ex As DirectoryNotFoundException
                Catch ex As FileNotFoundException

                Finally
                    InInit = False
                End Try
            End If
        End Sub

        ''' <summary>
        ''' If the layout is changed then update the file
        ''' </summary>
        Private Sub LayoutChanged(ByVal Sender As Object, ByVal e As EventArgs)
            Dim PathName As String = XMLBasePath()
            If Not InInit AndAlso Not String.IsNullOrEmpty(PathName) Then
                If Not Directory.Exists(PathName) Then
                    Directory.CreateDirectory(PathName)
                End If

                Dim FileName As String = Path.Combine(PathName, String.Format("{0}.Grid.xml", Name))
#If 0 Then ' Save all options so that it may be imported into the designer
            GridView1.OptionsLayout.StoreAllOptions = True
            GridView1.OptionsLayout.StoreAppearance = True
            GridView1.OptionsLayout.StoreDataSettings = True
            GridView1.OptionsLayout.StoreVisualOptions = True
            GridView1.OptionsLayout.Columns.StoreAllOptions = True
            GridView1.OptionsLayout.Columns.StoreAppearance = True
            GridView1.OptionsLayout.Columns.StoreLayout = True
#End If
                GridView1.SaveLayoutToXml(FileName)
            End If
        End Sub
    End Class
End Namespace
