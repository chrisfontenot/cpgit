#Region "Copyright 2000-2012 DebtPlus, L.L.C."
'{*******************************************************************}
'{                                                                   }
'{       DebtPlus Debt Management System                             }
'{                                                                   }
'{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
'{       ALL RIGHTS RESERVED                                         }
'{                                                                   }
'{   The entire contents of this file is protected by U.S. and       }
'{   International Copyright Laws. Unauthorized reproduction,        }
'{   reverse-engineering, and distribution of all or any portion of  }
'{   the code contained in this file is strictly prohibited and may  }
'{   result in severe civil and criminal penalties and will be       }
'{   prosecuted to the maximum extent possible under the law.        }
'{                                                                   }
'{   RESTRICTIONS                                                    }
'{                                                                   }
'{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
'{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
'{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
'{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
'{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
'{                                                                   }
'{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
'{   ADDITIONAL RESTRICTIONS.                                        }
'{                                                                   }
'{*******************************************************************}

#End Region
Imports DevExpress.XtraBars
Imports System.Data.SqlClient
Imports DebtPlus.Reports
Imports DevExpress.XtraReports.UI
Imports DevExpress.XtraPrinting.Drawing
Imports DebtPlus.Interfaces.Reports
Imports System.Drawing

Namespace Creditor.Refunds.Controls
    Friend Class CreditorRefundControl
        Private ReadOnly ctx As CreditorRefundsContext = Nothing
        Private _tbl As DataTable = Nothing

        Private parentFrm As Form_CreditorRefund

        Public Sub New(ByVal p As Form_CreditorRefund, ByVal ctx As CreditorRefundsContext)
            MyBase.new()
            Me.ctx = ctx
            parentFrm = p
            InitializeComponent()
            RefundItemControl1.GiveFocus()
            NewRefundItemControl1.TakeFocus()
            _tbl = ctx.ds.Tables("deposit_batch_details")
        End Sub

        Public Sub ReadForm()

            ' Add a handler to the menu for printing the report
            With parentFrm
                Dim mgr As BarManager = .barManager1
                Dim newItem As New BarButtonItem(mgr, "&Print...")
                With newItem
                    .Name = "PrintReport"
                    AddHandler .ItemClick, AddressOf PrintReport
                End With
                .BarSubItem_File.InsertItem(.BarSubItem_File.ItemLinks(0), newItem)
            End With

            ' Read the list of pending items for the deposit details
            If _tbl Is Nothing Then
                Dim cmd As SqlCommand = New SqlCommand
                With cmd
                    .Connection = New SqlConnection(DebtPlus.LINQ.SQLInfoClass.getDefault().ConnectionString)
                    .CommandType = CommandType.Text
                    .CommandText = "SELECT [deposit_batch_detail],[deposit_batch_id],[tran_subtype],[ok_to_post],[scanned_client],[client],[creditor],[client_creditor],[amount],[fairshare_amt],[fairshare_pct],[creditor_type],[item_date],[reference],[message],[ach_transaction_code],[ach_routing_number],[ach_account_number],[ach_authentication_code],[ach_response_batch_id],[date_posted],[created_by],[date_created] FROM deposit_batch_details WHERE deposit_batch_id = @deposit_batch ORDER BY deposit_batch_detail"
                    .Parameters.Add("@deposit_batch", SqlDbType.Int).Value = ctx.ap.Batch
                End With
                Dim da As New SqlDataAdapter(cmd)
                da.Fill(ctx.ds, "deposit_batch_details")
                _tbl = ctx.ds.Tables("deposit_batch_details")
            End If

            ' Supply the necessary default values for the columns
            With _tbl
                .PrimaryKey = New DataColumn() {.Columns("deposit_batch_detail")}

                With .Columns("deposit_batch_detail")
                    Dim objAnswer As Object = _tbl.Compute("max(deposit_batch_detail)", String.Empty)
                    If objAnswer Is Nothing OrElse objAnswer Is DBNull.Value Then objAnswer = 0

                    .AutoIncrement = True
                    .AutoIncrementSeed = Convert.ToInt32(objAnswer) + 1
                    .AutoIncrementStep = 1
                End With

                With .Columns("creditor_type")
                    .DefaultValue = "N"
                End With

                With .Columns("fairshare_pct")
                    .DefaultValue = 0.0#
                End With

                With .Columns("fairshare_amt") ' fairshare
                    .DefaultValue = 0D
                End With

                With .Columns("amount") ' gross
                    .DefaultValue = 0D
                End With

                With .Columns.Add("deduct_amt", GetType(Decimal), "iif([creditor_type]='D',[fairshare_amt],0)")
                    .DefaultValue = 0D
                End With

                With .Columns.Add("net_amt", GetType(Decimal), "[amount]-[deduct_amt]") ' net
                    .DefaultValue = 0D
                End With
            End With

            ' Set the controls in the proper order
            RefundItemControl1.GiveFocus()
            NewRefundItemControl1.TakeFocus()

            ' Bind the table to the various controls
            BatchListControl1.LoadList(_tbl)

            ' Add a new row to the view
            NewRefundItemControl1.ReadRecord()
        End Sub

        Private Sub PrintReport(ByVal sender As Object, ByVal e As ItemClickEventArgs)

            Dim rpt As New DebtPlus.Reports.Deposits.Batch.DepositBatchReport()
            rpt.Parameter_DepositBatchID = ctx.ap.Batch
            rpt.Parameter_SortString = "client"

            If rpt.RequestReportParameters = Windows.Forms.DialogResult.OK Then
                Dim updatedTable As DataTable = _tbl.GetChanges
                If updatedTable IsNot Nothing AndAlso updatedTable.Rows.Count > 0 Then
                    With CType(rpt, XtraReport).Watermark
                        .Font = New Font("Arial", 60, FontStyle.Bold, GraphicsUnit.Point, 0)
                        .ImageViewMode = ImageViewMode.Stretch
                        .ShowBehind = True
                        .Text = String.Format("DATABASE IS{0}NOT UPDATED", Environment.NewLine)
                        .TextDirection = DirectionMode.BackwardDiagonal
                        .TextTransparency = 180
                        .ForeColor = Color.DarkGreen
                    End With
                End If
                rpt.RunReportInSeparateThread()
            End If
        End Sub
    End Class
End Namespace
