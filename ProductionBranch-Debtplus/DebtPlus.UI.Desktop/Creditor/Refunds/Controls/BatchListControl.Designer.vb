﻿Namespace Creditor.Refunds.Controls
    <Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
    Partial Class BatchListControl
        Inherits BaseControl

        'UserControl overrides dispose to clean up the component list.
        <System.Diagnostics.DebuggerNonUserCode()> _
        Protected Overrides Sub Dispose(ByVal disposing As Boolean)
            Try
                If disposing AndAlso components IsNot Nothing Then
                    components.Dispose()
                End If
            Finally
                MyBase.Dispose(disposing)
            End Try
        End Sub

        'Required by the Windows Form Designer
        Private components As System.ComponentModel.IContainer

        'NOTE: The following procedure is required by the Windows Form Designer
        'It can be modified using the Windows Form Designer.  
        'Do not modify it using the code editor.
        <System.Diagnostics.DebuggerStepThrough()> _
        Private Sub InitializeComponent()
            Me.GroupControl1 = New DevExpress.XtraEditors.GroupControl()
            Me.GridControl1 = New DevExpress.XtraGrid.GridControl()
            Me.GridView1 = New DevExpress.XtraGrid.Views.Grid.GridView()
            Me.GridColumn_client = New DevExpress.XtraGrid.Columns.GridColumn()
            Me.GridColumn_creditor = New DevExpress.XtraGrid.Columns.GridColumn()
            Me.GridColumn_net = New DevExpress.XtraGrid.Columns.GridColumn()
            Me.GridColumn_fairshare = New DevExpress.XtraGrid.Columns.GridColumn()
            Me.GridColumn_gross = New DevExpress.XtraGrid.Columns.GridColumn()
            Me.GridColumn_creditor_type = New DevExpress.XtraGrid.Columns.GridColumn()
            Me.GridColumn_deduct_amt = New DevExpress.XtraGrid.Columns.GridColumn()
            Me.GridColumn_reference = New DevExpress.XtraGrid.Columns.GridColumn()
            Me.GridColumn_ID = New DevExpress.XtraGrid.Columns.GridColumn()
            CType(Me.GroupControl1, System.ComponentModel.ISupportInitialize).BeginInit()
            Me.GroupControl1.SuspendLayout()
            CType(Me.GridControl1, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.GridView1, System.ComponentModel.ISupportInitialize).BeginInit()
            Me.SuspendLayout()
            '
            'GroupControl1
            '
            Me.GroupControl1.Controls.Add(Me.GridControl1)
            Me.GroupControl1.Dock = System.Windows.Forms.DockStyle.Fill
            Me.GroupControl1.Location = New System.Drawing.Point(0, 0)
            Me.GroupControl1.Name = "GroupControl1"
            Me.GroupControl1.Size = New System.Drawing.Size(439, 261)
            Me.GroupControl1.TabIndex = 0
            Me.GroupControl1.Text = "Batch Items"
            '
            'GridControl1
            '
            Me.GridControl1.Dock = System.Windows.Forms.DockStyle.Fill
            Me.GridControl1.Location = New System.Drawing.Point(2, 21)
            Me.GridControl1.MainView = Me.GridView1
            Me.GridControl1.Name = "GridControl1"
            Me.GridControl1.Size = New System.Drawing.Size(435, 238)
            Me.GridControl1.TabIndex = 0
            Me.GridControl1.ViewCollection.AddRange(New DevExpress.XtraGrid.Views.Base.BaseView() {Me.GridView1})
            '
            'GridView1
            '
            Me.GridView1.Appearance.ColumnFilterButton.BackColor = System.Drawing.Color.FromArgb(CType(CType(104, Byte), Int32), CType(CType(184, Byte), Int32), CType(CType(251, Byte), Int32))
            Me.GridView1.Appearance.ColumnFilterButton.BorderColor = System.Drawing.Color.FromArgb(CType(CType(104, Byte), Int32), CType(CType(184, Byte), Int32), CType(CType(251, Byte), Int32))
            Me.GridView1.Appearance.ColumnFilterButton.ForeColor = System.Drawing.Color.White
            Me.GridView1.Appearance.ColumnFilterButton.Options.UseBackColor = True
            Me.GridView1.Appearance.ColumnFilterButton.Options.UseBorderColor = True
            Me.GridView1.Appearance.ColumnFilterButton.Options.UseForeColor = True
            Me.GridView1.Appearance.ColumnFilterButtonActive.BackColor = System.Drawing.Color.FromArgb(CType(CType(170, Byte), Int32), CType(CType(216, Byte), Int32), CType(CType(254, Byte), Int32))
            Me.GridView1.Appearance.ColumnFilterButtonActive.BorderColor = System.Drawing.Color.FromArgb(CType(CType(170, Byte), Int32), CType(CType(216, Byte), Int32), CType(CType(254, Byte), Int32))
            Me.GridView1.Appearance.ColumnFilterButtonActive.ForeColor = System.Drawing.Color.Black
            Me.GridView1.Appearance.ColumnFilterButtonActive.Options.UseBackColor = True
            Me.GridView1.Appearance.ColumnFilterButtonActive.Options.UseBorderColor = True
            Me.GridView1.Appearance.ColumnFilterButtonActive.Options.UseForeColor = True
            Me.GridView1.Appearance.Empty.BackColor = System.Drawing.Color.FromArgb(CType(CType(236, Byte), Int32), CType(CType(246, Byte), Int32), CType(CType(255, Byte), Int32))
            Me.GridView1.Appearance.Empty.BackColor2 = System.Drawing.Color.White
            Me.GridView1.Appearance.Empty.Options.UseBackColor = True
            Me.GridView1.Appearance.EvenRow.BackColor = System.Drawing.Color.FromArgb(CType(CType(247, Byte), Int32), CType(CType(251, Byte), Int32), CType(CType(255, Byte), Int32))
            Me.GridView1.Appearance.EvenRow.BorderColor = System.Drawing.Color.FromArgb(CType(CType(247, Byte), Int32), CType(CType(251, Byte), Int32), CType(CType(255, Byte), Int32))
            Me.GridView1.Appearance.EvenRow.ForeColor = System.Drawing.Color.Black
            Me.GridView1.Appearance.EvenRow.Options.UseBackColor = True
            Me.GridView1.Appearance.EvenRow.Options.UseBorderColor = True
            Me.GridView1.Appearance.EvenRow.Options.UseForeColor = True
            Me.GridView1.Appearance.FilterCloseButton.BackColor = System.Drawing.Color.FromArgb(CType(CType(104, Byte), Int32), CType(CType(184, Byte), Int32), CType(CType(251, Byte), Int32))
            Me.GridView1.Appearance.FilterCloseButton.BorderColor = System.Drawing.Color.FromArgb(CType(CType(104, Byte), Int32), CType(CType(184, Byte), Int32), CType(CType(251, Byte), Int32))
            Me.GridView1.Appearance.FilterCloseButton.ForeColor = System.Drawing.Color.White
            Me.GridView1.Appearance.FilterCloseButton.Options.UseBackColor = True
            Me.GridView1.Appearance.FilterCloseButton.Options.UseBorderColor = True
            Me.GridView1.Appearance.FilterCloseButton.Options.UseForeColor = True
            Me.GridView1.Appearance.FilterPanel.BackColor = System.Drawing.Color.FromArgb(CType(CType(236, Byte), Int32), CType(CType(246, Byte), Int32), CType(CType(255, Byte), Int32))
            Me.GridView1.Appearance.FilterPanel.BackColor2 = System.Drawing.Color.White
            Me.GridView1.Appearance.FilterPanel.ForeColor = System.Drawing.Color.Black
            Me.GridView1.Appearance.FilterPanel.Options.UseBackColor = True
            Me.GridView1.Appearance.FilterPanel.Options.UseForeColor = True
            Me.GridView1.Appearance.FixedLine.BackColor = System.Drawing.Color.FromArgb(CType(CType(59, Byte), Int32), CType(CType(133, Byte), Int32), CType(CType(195, Byte), Int32))
            Me.GridView1.Appearance.FixedLine.Options.UseBackColor = True
            Me.GridView1.Appearance.FocusedCell.BackColor = System.Drawing.Color.White
            Me.GridView1.Appearance.FocusedCell.ForeColor = System.Drawing.Color.Black
            Me.GridView1.Appearance.FocusedCell.Options.UseBackColor = True
            Me.GridView1.Appearance.FocusedCell.Options.UseForeColor = True
            Me.GridView1.Appearance.FocusedRow.BackColor = System.Drawing.Color.FromArgb(CType(CType(38, Byte), Int32), CType(CType(109, Byte), Int32), CType(CType(189, Byte), Int32))
            Me.GridView1.Appearance.FocusedRow.BorderColor = System.Drawing.Color.FromArgb(CType(CType(59, Byte), Int32), CType(CType(139, Byte), Int32), CType(CType(206, Byte), Int32))
            Me.GridView1.Appearance.FocusedRow.ForeColor = System.Drawing.Color.White
            Me.GridView1.Appearance.FocusedRow.Options.UseBackColor = True
            Me.GridView1.Appearance.FocusedRow.Options.UseBorderColor = True
            Me.GridView1.Appearance.FocusedRow.Options.UseForeColor = True
            Me.GridView1.Appearance.FooterPanel.BackColor = System.Drawing.Color.FromArgb(CType(CType(104, Byte), Int32), CType(CType(184, Byte), Int32), CType(CType(251, Byte), Int32))
            Me.GridView1.Appearance.FooterPanel.BorderColor = System.Drawing.Color.FromArgb(CType(CType(104, Byte), Int32), CType(CType(184, Byte), Int32), CType(CType(251, Byte), Int32))
            Me.GridView1.Appearance.FooterPanel.ForeColor = System.Drawing.Color.Black
            Me.GridView1.Appearance.FooterPanel.Options.UseBackColor = True
            Me.GridView1.Appearance.FooterPanel.Options.UseBorderColor = True
            Me.GridView1.Appearance.FooterPanel.Options.UseForeColor = True
            Me.GridView1.Appearance.GroupButton.BackColor = System.Drawing.Color.FromArgb(CType(CType(104, Byte), Int32), CType(CType(184, Byte), Int32), CType(CType(251, Byte), Int32))
            Me.GridView1.Appearance.GroupButton.BorderColor = System.Drawing.Color.FromArgb(CType(CType(104, Byte), Int32), CType(CType(184, Byte), Int32), CType(CType(251, Byte), Int32))
            Me.GridView1.Appearance.GroupButton.Options.UseBackColor = True
            Me.GridView1.Appearance.GroupButton.Options.UseBorderColor = True
            Me.GridView1.Appearance.GroupFooter.BackColor = System.Drawing.Color.FromArgb(CType(CType(170, Byte), Int32), CType(CType(216, Byte), Int32), CType(CType(254, Byte), Int32))
            Me.GridView1.Appearance.GroupFooter.BorderColor = System.Drawing.Color.FromArgb(CType(CType(170, Byte), Int32), CType(CType(216, Byte), Int32), CType(CType(254, Byte), Int32))
            Me.GridView1.Appearance.GroupFooter.ForeColor = System.Drawing.Color.Black
            Me.GridView1.Appearance.GroupFooter.Options.UseBackColor = True
            Me.GridView1.Appearance.GroupFooter.Options.UseBorderColor = True
            Me.GridView1.Appearance.GroupFooter.Options.UseForeColor = True
            Me.GridView1.Appearance.GroupPanel.BackColor = System.Drawing.Color.FromArgb(CType(CType(236, Byte), Int32), CType(CType(246, Byte), Int32), CType(CType(255, Byte), Int32))
            Me.GridView1.Appearance.GroupPanel.BackColor2 = System.Drawing.Color.White
            Me.GridView1.Appearance.GroupPanel.ForeColor = System.Drawing.Color.Black
            Me.GridView1.Appearance.GroupPanel.Options.UseBackColor = True
            Me.GridView1.Appearance.GroupPanel.Options.UseForeColor = True
            Me.GridView1.Appearance.GroupRow.BackColor = System.Drawing.Color.FromArgb(CType(CType(170, Byte), Int32), CType(CType(216, Byte), Int32), CType(CType(254, Byte), Int32))
            Me.GridView1.Appearance.GroupRow.BorderColor = System.Drawing.Color.FromArgb(CType(CType(170, Byte), Int32), CType(CType(216, Byte), Int32), CType(CType(254, Byte), Int32))
            Me.GridView1.Appearance.GroupRow.ForeColor = System.Drawing.Color.Black
            Me.GridView1.Appearance.GroupRow.Options.UseBackColor = True
            Me.GridView1.Appearance.GroupRow.Options.UseBorderColor = True
            Me.GridView1.Appearance.GroupRow.Options.UseForeColor = True
            Me.GridView1.Appearance.HeaderPanel.BackColor = System.Drawing.Color.FromArgb(CType(CType(139, Byte), Int32), CType(CType(201, Byte), Int32), CType(CType(254, Byte), Int32))
            Me.GridView1.Appearance.HeaderPanel.BorderColor = System.Drawing.Color.FromArgb(CType(CType(139, Byte), Int32), CType(CType(201, Byte), Int32), CType(CType(254, Byte), Int32))
            Me.GridView1.Appearance.HeaderPanel.ForeColor = System.Drawing.Color.Black
            Me.GridView1.Appearance.HeaderPanel.Options.UseBackColor = True
            Me.GridView1.Appearance.HeaderPanel.Options.UseBorderColor = True
            Me.GridView1.Appearance.HeaderPanel.Options.UseForeColor = True
            Me.GridView1.Appearance.HideSelectionRow.BackColor = System.Drawing.Color.FromArgb(CType(CType(105, Byte), Int32), CType(CType(170, Byte), Int32), CType(CType(225, Byte), Int32))
            Me.GridView1.Appearance.HideSelectionRow.BorderColor = System.Drawing.Color.FromArgb(CType(CType(83, Byte), Int32), CType(CType(155, Byte), Int32), CType(CType(215, Byte), Int32))
            Me.GridView1.Appearance.HideSelectionRow.ForeColor = System.Drawing.Color.FromArgb(CType(CType(236, Byte), Int32), CType(CType(246, Byte), Int32), CType(CType(255, Byte), Int32))
            Me.GridView1.Appearance.HideSelectionRow.Options.UseBackColor = True
            Me.GridView1.Appearance.HideSelectionRow.Options.UseBorderColor = True
            Me.GridView1.Appearance.HideSelectionRow.Options.UseForeColor = True
            Me.GridView1.Appearance.HorzLine.BackColor = System.Drawing.Color.FromArgb(CType(CType(104, Byte), Int32), CType(CType(184, Byte), Int32), CType(CType(251, Byte), Int32))
            Me.GridView1.Appearance.HorzLine.Options.UseBackColor = True
            Me.GridView1.Appearance.OddRow.BackColor = System.Drawing.Color.FromArgb(CType(CType(236, Byte), Int32), CType(CType(246, Byte), Int32), CType(CType(255, Byte), Int32))
            Me.GridView1.Appearance.OddRow.BorderColor = System.Drawing.Color.FromArgb(CType(CType(236, Byte), Int32), CType(CType(246, Byte), Int32), CType(CType(255, Byte), Int32))
            Me.GridView1.Appearance.OddRow.ForeColor = System.Drawing.Color.Black
            Me.GridView1.Appearance.OddRow.Options.UseBackColor = True
            Me.GridView1.Appearance.OddRow.Options.UseBorderColor = True
            Me.GridView1.Appearance.OddRow.Options.UseForeColor = True
            Me.GridView1.Appearance.Preview.Font = New System.Drawing.Font("Verdana", 7.5!)
            Me.GridView1.Appearance.Preview.ForeColor = System.Drawing.Color.FromArgb(CType(CType(83, Byte), Int32), CType(CType(155, Byte), Int32), CType(CType(215, Byte), Int32))
            Me.GridView1.Appearance.Preview.Options.UseFont = True
            Me.GridView1.Appearance.Preview.Options.UseForeColor = True
            Me.GridView1.Appearance.Row.BackColor = System.Drawing.Color.FromArgb(CType(CType(247, Byte), Int32), CType(CType(251, Byte), Int32), CType(CType(255, Byte), Int32))
            Me.GridView1.Appearance.Row.ForeColor = System.Drawing.Color.Black
            Me.GridView1.Appearance.Row.Options.UseBackColor = True
            Me.GridView1.Appearance.Row.Options.UseForeColor = True
            Me.GridView1.Appearance.RowSeparator.BackColor = System.Drawing.Color.FromArgb(CType(CType(236, Byte), Int32), CType(CType(246, Byte), Int32), CType(CType(255, Byte), Int32))
            Me.GridView1.Appearance.RowSeparator.BackColor2 = System.Drawing.Color.White
            Me.GridView1.Appearance.RowSeparator.Options.UseBackColor = True
            Me.GridView1.Appearance.SelectedRow.BackColor = System.Drawing.Color.FromArgb(CType(CType(83, Byte), Int32), CType(CType(155, Byte), Int32), CType(CType(215, Byte), Int32))
            Me.GridView1.Appearance.SelectedRow.ForeColor = System.Drawing.Color.White
            Me.GridView1.Appearance.SelectedRow.Options.UseBackColor = True
            Me.GridView1.Appearance.SelectedRow.Options.UseForeColor = True
            Me.GridView1.Appearance.TopNewRow.BackColor = System.Drawing.Color.White
            Me.GridView1.Appearance.TopNewRow.Options.UseBackColor = True
            Me.GridView1.Appearance.VertLine.BackColor = System.Drawing.Color.FromArgb(CType(CType(104, Byte), Int32), CType(CType(184, Byte), Int32), CType(CType(251, Byte), Int32))
            Me.GridView1.Appearance.VertLine.Options.UseBackColor = True
            Me.GridView1.Columns.AddRange(New DevExpress.XtraGrid.Columns.GridColumn() {Me.GridColumn_client, Me.GridColumn_creditor, Me.GridColumn_net, Me.GridColumn_fairshare, Me.GridColumn_gross, Me.GridColumn_creditor_type, Me.GridColumn_deduct_amt, Me.GridColumn_reference, Me.GridColumn_ID})
            Me.GridView1.CustomizationFormBounds = New System.Drawing.Rectangle(579, 424, 208, 170)
            Me.GridView1.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus
            Me.GridView1.GridControl = Me.GridControl1
            Me.GridView1.Name = "GridView1"
            Me.GridView1.OptionsBehavior.Editable = False
            Me.GridView1.OptionsLayout.Columns.StoreAllOptions = True
            Me.GridView1.OptionsLayout.Columns.StoreAppearance = True
            Me.GridView1.OptionsLayout.StoreAllOptions = True
            Me.GridView1.OptionsLayout.StoreAppearance = True
            Me.GridView1.OptionsView.ShowFooter = True
            Me.GridView1.OptionsView.ShowGroupPanel = False
            Me.GridView1.OptionsView.ShowIndicator = False
            Me.GridView1.ShowButtonMode = DevExpress.XtraGrid.Views.Base.ShowButtonModeEnum.ShowOnlyInEditor
            '
            'GridColumn_client
            '
            Me.GridColumn_client.AppearanceCell.Options.UseTextOptions = True
            Me.GridColumn_client.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
            Me.GridColumn_client.AppearanceHeader.Options.UseTextOptions = True
            Me.GridColumn_client.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
            Me.GridColumn_client.Caption = "Client"
            Me.GridColumn_client.CustomizationCaption = "Client"
            Me.GridColumn_client.DisplayFormat.FormatString = "{0:f0}"
            Me.GridColumn_client.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
            Me.GridColumn_client.FieldName = "client"
            Me.GridColumn_client.GroupFormat.FormatString = "{0:f0}"
            Me.GridColumn_client.GroupFormat.FormatType = DevExpress.Utils.FormatType.Numeric
            Me.GridColumn_client.Name = "GridColumn_client"
            Me.GridColumn_client.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.[True]
            Me.GridColumn_client.Summary.AddRange(New DevExpress.XtraGrid.GridSummaryItem() {New DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Count, "client", "{0:n0} items")})
            Me.GridColumn_client.Visible = True
            Me.GridColumn_client.VisibleIndex = 0
            Me.GridColumn_client.Width = 94
            '
            'GridColumn_creditor
            '
            Me.GridColumn_creditor.Caption = "Creditor"
            Me.GridColumn_creditor.CustomizationCaption = "Creditor ID"
            Me.GridColumn_creditor.FieldName = "creditor"
            Me.GridColumn_creditor.Name = "GridColumn_creditor"
            Me.GridColumn_creditor.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.[True]
            Me.GridColumn_creditor.Visible = True
            Me.GridColumn_creditor.VisibleIndex = 1
            Me.GridColumn_creditor.Width = 238
            '
            'GridColumn_net
            '
            Me.GridColumn_net.AppearanceCell.Options.UseTextOptions = True
            Me.GridColumn_net.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
            Me.GridColumn_net.AppearanceHeader.Options.UseTextOptions = True
            Me.GridColumn_net.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
            Me.GridColumn_net.Caption = "Net"
            Me.GridColumn_net.CustomizationCaption = "Net Amount"
            Me.GridColumn_net.DisplayFormat.FormatString = "{0:c}"
            Me.GridColumn_net.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
            Me.GridColumn_net.FieldName = "net_amt"
            Me.GridColumn_net.GroupFormat.FormatString = "{0:c}"
            Me.GridColumn_net.GroupFormat.FormatType = DevExpress.Utils.FormatType.Numeric
            Me.GridColumn_net.Name = "GridColumn_net"
            Me.GridColumn_net.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.[True]
            Me.GridColumn_net.Summary.AddRange(New DevExpress.XtraGrid.GridSummaryItem() {New DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "net_amt", "{0:c}")})
            Me.GridColumn_net.Visible = True
            Me.GridColumn_net.VisibleIndex = 2
            Me.GridColumn_net.Width = 74
            '
            'GridColumn_fairshare
            '
            Me.GridColumn_fairshare.AppearanceCell.Options.UseTextOptions = True
            Me.GridColumn_fairshare.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
            Me.GridColumn_fairshare.AppearanceHeader.Options.UseTextOptions = True
            Me.GridColumn_fairshare.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
            Me.GridColumn_fairshare.Caption = "Fairshare"
            Me.GridColumn_fairshare.CustomizationCaption = "Fairshare Amount"
            Me.GridColumn_fairshare.DisplayFormat.FormatString = "{0:c}"
            Me.GridColumn_fairshare.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
            Me.GridColumn_fairshare.FieldName = "fairshare_amt"
            Me.GridColumn_fairshare.GroupFormat.FormatString = "{0:c}"
            Me.GridColumn_fairshare.GroupFormat.FormatType = DevExpress.Utils.FormatType.Numeric
            Me.GridColumn_fairshare.Name = "GridColumn_fairshare"
            Me.GridColumn_fairshare.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.[True]
            Me.GridColumn_fairshare.Summary.AddRange(New DevExpress.XtraGrid.GridSummaryItem() {New DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "fairshare_amt", "{0:c}")})
            Me.GridColumn_fairshare.Visible = True
            Me.GridColumn_fairshare.VisibleIndex = 3
            Me.GridColumn_fairshare.Width = 65
            '
            'GridColumn_gross
            '
            Me.GridColumn_gross.AppearanceCell.Options.UseTextOptions = True
            Me.GridColumn_gross.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
            Me.GridColumn_gross.AppearanceHeader.Options.UseTextOptions = True
            Me.GridColumn_gross.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
            Me.GridColumn_gross.Caption = "Gross"
            Me.GridColumn_gross.CustomizationCaption = "Gross Amount"
            Me.GridColumn_gross.DisplayFormat.FormatString = "{0:c}"
            Me.GridColumn_gross.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
            Me.GridColumn_gross.FieldName = "amount"
            Me.GridColumn_gross.GroupFormat.FormatString = "{0:c}"
            Me.GridColumn_gross.GroupFormat.FormatType = DevExpress.Utils.FormatType.Numeric
            Me.GridColumn_gross.Name = "GridColumn_gross"
            Me.GridColumn_gross.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.[True]
            Me.GridColumn_gross.Summary.AddRange(New DevExpress.XtraGrid.GridSummaryItem() {New DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "amount", "{0:c}")})
            Me.GridColumn_gross.Visible = True
            Me.GridColumn_gross.VisibleIndex = 4
            Me.GridColumn_gross.Width = 70
            '
            'GridColumn_creditor_type
            '
            Me.GridColumn_creditor_type.Caption = "Type"
            Me.GridColumn_creditor_type.CustomizationCaption = "Bill/Deduct/None"
            Me.GridColumn_creditor_type.FieldName = "creditor_type"
            Me.GridColumn_creditor_type.Name = "GridColumn_creditor_type"
            Me.GridColumn_creditor_type.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.[True]
            '
            'GridColumn_deduct_amt
            '
            Me.GridColumn_deduct_amt.AppearanceCell.Options.UseTextOptions = True
            Me.GridColumn_deduct_amt.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
            Me.GridColumn_deduct_amt.AppearanceHeader.Options.UseTextOptions = True
            Me.GridColumn_deduct_amt.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
            Me.GridColumn_deduct_amt.Caption = "Deducted"
            Me.GridColumn_deduct_amt.CustomizationCaption = "Deduct"
            Me.GridColumn_deduct_amt.DisplayFormat.FormatString = "{0:c}"
            Me.GridColumn_deduct_amt.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
            Me.GridColumn_deduct_amt.FieldName = "deduct_amt"
            Me.GridColumn_deduct_amt.GroupFormat.FormatString = "{0:c}"
            Me.GridColumn_deduct_amt.GroupFormat.FormatType = DevExpress.Utils.FormatType.Numeric
            Me.GridColumn_deduct_amt.Name = "GridColumn_deduct_amt"
            Me.GridColumn_deduct_amt.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.[True]
            Me.GridColumn_deduct_amt.Summary.AddRange(New DevExpress.XtraGrid.GridSummaryItem() {New DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "deduct_amt", "{0:c}")})
            '
            'GridColumn_reference
            '
            Me.GridColumn_reference.Caption = "Reference"
            Me.GridColumn_reference.CustomizationCaption = "Message Field"
            Me.GridColumn_reference.FieldName = "reference"
            Me.GridColumn_reference.Name = "GridColumn_reference"
            Me.GridColumn_reference.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.[True]
            '
            'GridColumn_ID
            '
            Me.GridColumn_ID.AppearanceCell.Options.UseTextOptions = True
            Me.GridColumn_ID.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
            Me.GridColumn_ID.AppearanceHeader.Options.UseTextOptions = True
            Me.GridColumn_ID.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
            Me.GridColumn_ID.Caption = "ID"
            Me.GridColumn_ID.CustomizationCaption = "Deposit Record ID"
            Me.GridColumn_ID.DisplayFormat.FormatString = "{0:f0}"
            Me.GridColumn_ID.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
            Me.GridColumn_ID.FieldName = "deposit_batch_id"
            Me.GridColumn_ID.GroupFormat.FormatString = "{0:f0}"
            Me.GridColumn_ID.GroupFormat.FormatType = DevExpress.Utils.FormatType.Numeric
            Me.GridColumn_ID.Name = "GridColumn_ID"
            Me.GridColumn_ID.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.[True]
            '
            'BatchListControl
            '
            Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
            Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
            Me.Controls.Add(Me.GroupControl1)
            Me.Name = "BatchListControl"
            Me.Size = New System.Drawing.Size(439, 261)
            CType(Me.GroupControl1, System.ComponentModel.ISupportInitialize).EndInit()
            Me.GroupControl1.ResumeLayout(False)
            CType(Me.GridControl1, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.GridView1, System.ComponentModel.ISupportInitialize).EndInit()
            Me.ResumeLayout(False)

        End Sub
        Friend WithEvents GroupControl1 As DevExpress.XtraEditors.GroupControl
        Friend WithEvents GridControl1 As DevExpress.XtraGrid.GridControl
        Friend WithEvents GridView1 As DevExpress.XtraGrid.Views.Grid.GridView
        Friend WithEvents GridColumn_client As DevExpress.XtraGrid.Columns.GridColumn
        Friend WithEvents GridColumn_creditor As DevExpress.XtraGrid.Columns.GridColumn
        Friend WithEvents GridColumn_net As DevExpress.XtraGrid.Columns.GridColumn
        Friend WithEvents GridColumn_fairshare As DevExpress.XtraGrid.Columns.GridColumn
        Friend WithEvents GridColumn_gross As DevExpress.XtraGrid.Columns.GridColumn
        Friend WithEvents GridColumn_creditor_type As DevExpress.XtraGrid.Columns.GridColumn
        Friend WithEvents GridColumn_deduct_amt As DevExpress.XtraGrid.Columns.GridColumn
        Friend WithEvents GridColumn_reference As DevExpress.XtraGrid.Columns.GridColumn
        Friend WithEvents GridColumn_ID As DevExpress.XtraGrid.Columns.GridColumn

    End Class
End Namespace