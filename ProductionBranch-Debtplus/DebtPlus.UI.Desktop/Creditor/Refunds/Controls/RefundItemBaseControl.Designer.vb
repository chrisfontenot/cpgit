﻿
Imports DebtPlus.UI.Creditor.Widgets.Controls
Imports DebtPlus.UI.Client.Widgets.Controls
Imports DebtPlus.UI.Creditor.Widgets

Namespace Creditor.Refunds.Controls
    <Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
    Partial Class RefundItemBaseControl
        Inherits BaseControl

        'UserControl overrides dispose to clean up the component list.
        <System.Diagnostics.DebuggerNonUserCode()> _
        Protected Overrides Sub Dispose(ByVal disposing As Boolean)
            Try
                If disposing AndAlso components IsNot Nothing Then
                    components.Dispose()
                End If
            Finally
                MyBase.Dispose(disposing)
            End Try
        End Sub

        'Required by the Windows Form Designer
        Private components As System.ComponentModel.IContainer

        'NOTE: The following procedure is required by the Windows Form Designer
        'It can be modified using the Windows Form Designer.  
        'Do not modify it using the code editor.
        <System.Diagnostics.DebuggerStepThrough()> _
        Private Sub InitializeComponent()
            Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(RefundItemBaseControl))
            Dim SerializableAppearanceObject1 As DevExpress.Utils.SerializableAppearanceObject = New DevExpress.Utils.SerializableAppearanceObject()
            Dim SerializableAppearanceObject2 As DevExpress.Utils.SerializableAppearanceObject = New DevExpress.Utils.SerializableAppearanceObject()
            Me.LabelControl_client_name = New DevExpress.XtraEditors.LabelControl()
            Me.LayoutControl1 = New DevExpress.XtraLayout.LayoutControl()
            Me.CreditorID1 = New DebtPlus.UI.Creditor.Widgets.Controls.CreditorID()
            Me.ClientID1 = New DebtPlus.UI.Client.Widgets.Controls.ClientID()
            Me.LookUpEdit_creditor_type = New DevExpress.XtraEditors.LookUpEdit()
            Me.CalcEdit_fairshare_rate = New DevExpress.XtraEditors.CalcEdit()
            Me.CalcEdit_gross = New DevExpress.XtraEditors.CalcEdit()
            Me.SimpleButton_ok = New DevExpress.XtraEditors.SimpleButton()
            Me.CalcEdit_fairshare_amt = New DevExpress.XtraEditors.CalcEdit()
            Me.ComboBoxEdit_message = New DevExpress.XtraEditors.ComboBoxEdit()
            Me.CalcEdit_net = New DevExpress.XtraEditors.CalcEdit()
            Me.LookUpEdit_client_creditor = New DevExpress.XtraEditors.LookUpEdit()
            Me.LabelControl_creditor_name = New DevExpress.XtraEditors.LabelControl()
            Me.SimpleButton_cancel = New DevExpress.XtraEditors.SimpleButton()
            Me.LayoutControlGroup1 = New DevExpress.XtraLayout.LayoutControlGroup()
            Me.LayoutControlGroup3 = New DevExpress.XtraLayout.LayoutControlGroup()
            Me.LayoutControlItem8 = New DevExpress.XtraLayout.LayoutControlItem()
            Me.LayoutControlItem7 = New DevExpress.XtraLayout.LayoutControlItem()
            Me.LayoutControlItem6 = New DevExpress.XtraLayout.LayoutControlItem()
            Me.LayoutControlItem10 = New DevExpress.XtraLayout.LayoutControlItem()
            Me.LayoutControlItem11 = New DevExpress.XtraLayout.LayoutControlItem()
            Me.LayoutControlItem12 = New DevExpress.XtraLayout.LayoutControlItem()
            Me.LayoutControlItem9 = New DevExpress.XtraLayout.LayoutControlItem()
            Me.EmptySpaceItem1 = New DevExpress.XtraLayout.EmptySpaceItem()
            Me.LayoutControlItem13 = New DevExpress.XtraLayout.LayoutControlItem()
            Me.LayoutControlGroup2 = New DevExpress.XtraLayout.LayoutControlGroup()
            Me.LayoutControlItem3 = New DevExpress.XtraLayout.LayoutControlItem()
            Me.LayoutControlItem4 = New DevExpress.XtraLayout.LayoutControlItem()
            Me.LayoutControlItem5 = New DevExpress.XtraLayout.LayoutControlItem()
            Me.EmptySpaceItem2 = New DevExpress.XtraLayout.EmptySpaceItem()
            Me.LayoutControlItem1 = New DevExpress.XtraLayout.LayoutControlItem()
            Me.LayoutControlItem2 = New DevExpress.XtraLayout.LayoutControlItem()
            CType(Me.LayoutControl1, System.ComponentModel.ISupportInitialize).BeginInit()
            Me.LayoutControl1.SuspendLayout()
            CType(Me.CreditorID1.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.ClientID1.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.LookUpEdit_creditor_type.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.CalcEdit_fairshare_rate.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.CalcEdit_gross.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.CalcEdit_fairshare_amt.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.ComboBoxEdit_message.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.CalcEdit_net.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.LookUpEdit_client_creditor.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.LayoutControlGroup1, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.LayoutControlGroup3, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.LayoutControlItem8, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.LayoutControlItem7, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.LayoutControlItem6, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.LayoutControlItem10, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.LayoutControlItem11, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.LayoutControlItem12, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.LayoutControlItem9, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.EmptySpaceItem1, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.LayoutControlItem13, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.LayoutControlGroup2, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.LayoutControlItem3, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.LayoutControlItem4, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.LayoutControlItem5, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.EmptySpaceItem2, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.LayoutControlItem1, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.LayoutControlItem2, System.ComponentModel.ISupportInitialize).BeginInit()
            Me.SuspendLayout()
            '
            'LabelControl_client_name
            '
            Me.LabelControl_client_name.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
            Me.LabelControl_client_name.Appearance.TextOptions.Trimming = DevExpress.Utils.Trimming.Character
            Me.LabelControl_client_name.Appearance.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center
            Me.LabelControl_client_name.Appearance.TextOptions.WordWrap = DevExpress.Utils.WordWrap.NoWrap
            Me.LabelControl_client_name.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None
            Me.LabelControl_client_name.Location = New System.Drawing.Point(204, 67)
            Me.LabelControl_client_name.Name = "LabelControl_client_name"
            Me.LabelControl_client_name.Padding = New System.Windows.Forms.Padding(3)
            Me.LabelControl_client_name.Size = New System.Drawing.Size(209, 19)
            Me.LabelControl_client_name.StyleController = Me.LayoutControl1
            Me.LabelControl_client_name.TabIndex = 3
            Me.LabelControl_client_name.UseMnemonic = False
            '
            'LayoutControl1
            '
            Me.LayoutControl1.Controls.Add(Me.CreditorID1)
            Me.LayoutControl1.Controls.Add(Me.ClientID1)
            Me.LayoutControl1.Controls.Add(Me.LookUpEdit_creditor_type)
            Me.LayoutControl1.Controls.Add(Me.LabelControl_client_name)
            Me.LayoutControl1.Controls.Add(Me.CalcEdit_fairshare_rate)
            Me.LayoutControl1.Controls.Add(Me.CalcEdit_gross)
            Me.LayoutControl1.Controls.Add(Me.SimpleButton_ok)
            Me.LayoutControl1.Controls.Add(Me.CalcEdit_fairshare_amt)
            Me.LayoutControl1.Controls.Add(Me.ComboBoxEdit_message)
            Me.LayoutControl1.Controls.Add(Me.CalcEdit_net)
            Me.LayoutControl1.Controls.Add(Me.LookUpEdit_client_creditor)
            Me.LayoutControl1.Controls.Add(Me.LabelControl_creditor_name)
            Me.LayoutControl1.Controls.Add(Me.SimpleButton_cancel)
            Me.LayoutControl1.Dock = System.Windows.Forms.DockStyle.Fill
            Me.LayoutControl1.Location = New System.Drawing.Point(0, 0)
            Me.LayoutControl1.Name = "LayoutControl1"
            Me.LayoutControl1.Root = Me.LayoutControlGroup1
            Me.LayoutControl1.Size = New System.Drawing.Size(437, 294)
            Me.LayoutControl1.TabIndex = 2
            Me.LayoutControl1.OptionsFocus.EnableAutoTabOrder = False
            Me.LayoutControl1.Text = "LayoutControl1"
            '
            'CreditorID1
            '
            Me.CreditorID1.AllowDrop = True
            Me.CreditorID1.EditValue = Nothing
            Me.CreditorID1.Location = New System.Drawing.Point(114, 43)
            Me.CreditorID1.Name = "CreditorID1"
            Me.CreditorID1.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph, "", -1, True, True, False, DevExpress.XtraEditors.ImageLocation.MiddleCenter, CType(resources.GetObject("CreditorID1.Properties.Buttons"), System.Drawing.Image), New DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), SerializableAppearanceObject1, "Click here to search for a creditor ID", "search", Nothing, True)})
            Me.CreditorID1.Properties.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
            Me.CreditorID1.Properties.Mask.AutoComplete = DevExpress.XtraEditors.Mask.AutoCompleteType.Strong
            Me.CreditorID1.Properties.Mask.BeepOnError = True
            Me.CreditorID1.Properties.Mask.EditMask = "[A-Z]{1,2}[0-9]{4,}"
            Me.CreditorID1.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.RegEx
            Me.CreditorID1.Properties.Mask.UseMaskAsDisplayFormat = True
            Me.CreditorID1.Properties.MaxLength = 10
            Me.CreditorID1.Size = New System.Drawing.Size(86, 20)
            Me.CreditorID1.StyleController = Me.LayoutControl1
            Me.CreditorID1.TabIndex = 0
            '
            'ClientID1
            '
            Me.ClientID1.AllowDrop = True
            Me.ClientID1.Location = New System.Drawing.Point(114, 67)
            Me.ClientID1.Name = "ClientID1"
            Me.ClientID1.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[False]
            Me.ClientID1.Properties.Appearance.Options.UseTextOptions = True
            Me.ClientID1.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
            Me.ClientID1.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph, "", -1, True, True, False, DevExpress.XtraEditors.ImageLocation.MiddleCenter, CType(resources.GetObject("ClientID1.Properties.Buttons"), System.Drawing.Image), New DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), SerializableAppearanceObject2, "Click here to search for a client ID", "search", Nothing, True)})
            Me.ClientID1.Properties.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
            Me.ClientID1.Properties.DisplayFormat.FormatString = "0000000"
            Me.ClientID1.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Custom
            Me.ClientID1.Properties.EditFormat.FormatString = "f0"
            Me.ClientID1.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric
            Me.ClientID1.Properties.Mask.BeepOnError = True
            Me.ClientID1.Properties.Mask.EditMask = "\d*"
            Me.ClientID1.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.RegEx
            Me.ClientID1.Properties.ValidateOnEnterKey = True
            Me.ClientID1.Size = New System.Drawing.Size(86, 20)
            Me.ClientID1.StyleController = Me.LayoutControl1
            Me.ClientID1.TabIndex = 2
            '
            'LookUpEdit_creditor_type
            '
            Me.LookUpEdit_creditor_type.Location = New System.Drawing.Point(275, 175)
            Me.LookUpEdit_creditor_type.Name = "LookUpEdit_creditor_type"
            Me.LookUpEdit_creditor_type.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[False]
            Me.LookUpEdit_creditor_type.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
            Me.LookUpEdit_creditor_type.Properties.Columns.AddRange(New DevExpress.XtraEditors.Controls.LookUpColumnInfo() {New DevExpress.XtraEditors.Controls.LookUpColumnInfo("Id", "Type", 20, DevExpress.Utils.FormatType.None, "", False, DevExpress.Utils.HorzAlignment.[Default]), New DevExpress.XtraEditors.Controls.LookUpColumnInfo("description", "Description", 20, DevExpress.Utils.FormatType.None, "", True, DevExpress.Utils.HorzAlignment.[Default], DevExpress.Data.ColumnSortOrder.Ascending)})
            Me.LookUpEdit_creditor_type.Properties.DisplayMember = "description"
            Me.LookUpEdit_creditor_type.Properties.NullText = ""
            Me.LookUpEdit_creditor_type.Properties.ShowFooter = False
            Me.LookUpEdit_creditor_type.Properties.ShowHeader = False
            Me.LookUpEdit_creditor_type.Properties.SortColumnIndex = 1
            Me.LookUpEdit_creditor_type.Properties.ValueMember = "Id"
            Me.LookUpEdit_creditor_type.Size = New System.Drawing.Size(59, 20)
            Me.LookUpEdit_creditor_type.StyleController = Me.LayoutControl1
            Me.LookUpEdit_creditor_type.TabIndex = 6
            '
            'CalcEdit_fairshare_rate
            '
            Me.CalcEdit_fairshare_rate.Location = New System.Drawing.Point(275, 199)
            Me.CalcEdit_fairshare_rate.Name = "CalcEdit_fairshare_rate"
            Me.CalcEdit_fairshare_rate.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[False]
            Me.CalcEdit_fairshare_rate.Properties.Appearance.Options.UseTextOptions = True
            Me.CalcEdit_fairshare_rate.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
            Me.CalcEdit_fairshare_rate.Properties.AppearanceDisabled.Options.UseTextOptions = True
            Me.CalcEdit_fairshare_rate.Properties.AppearanceDisabled.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
            Me.CalcEdit_fairshare_rate.Properties.AppearanceDropDown.Options.UseTextOptions = True
            Me.CalcEdit_fairshare_rate.Properties.AppearanceDropDown.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
            Me.CalcEdit_fairshare_rate.Properties.AppearanceFocused.Options.UseTextOptions = True
            Me.CalcEdit_fairshare_rate.Properties.AppearanceFocused.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
            Me.CalcEdit_fairshare_rate.Properties.AppearanceReadOnly.Options.UseTextOptions = True
            Me.CalcEdit_fairshare_rate.Properties.AppearanceReadOnly.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
            Me.CalcEdit_fairshare_rate.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
            Me.CalcEdit_fairshare_rate.Properties.DisplayFormat.FormatString = "p"
            Me.CalcEdit_fairshare_rate.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
            Me.CalcEdit_fairshare_rate.Properties.EditFormat.FormatString = "f6"
            Me.CalcEdit_fairshare_rate.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric
            Me.CalcEdit_fairshare_rate.Properties.Mask.BeepOnError = True
            Me.CalcEdit_fairshare_rate.Properties.Mask.EditMask = "f6"
            Me.CalcEdit_fairshare_rate.Size = New System.Drawing.Size(59, 20)
            Me.CalcEdit_fairshare_rate.StyleController = Me.LayoutControl1
            Me.CalcEdit_fairshare_rate.TabIndex = 8
            '
            'CalcEdit_gross
            '
            Me.CalcEdit_gross.Location = New System.Drawing.Point(114, 223)
            Me.CalcEdit_gross.Name = "CalcEdit_gross"
            Me.CalcEdit_gross.Properties.Appearance.Options.UseTextOptions = True
            Me.CalcEdit_gross.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
            Me.CalcEdit_gross.Properties.AppearanceDisabled.Options.UseTextOptions = True
            Me.CalcEdit_gross.Properties.AppearanceDisabled.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
            Me.CalcEdit_gross.Properties.AppearanceDropDown.Options.UseTextOptions = True
            Me.CalcEdit_gross.Properties.AppearanceDropDown.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
            Me.CalcEdit_gross.Properties.AppearanceFocused.Options.UseTextOptions = True
            Me.CalcEdit_gross.Properties.AppearanceFocused.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
            Me.CalcEdit_gross.Properties.AppearanceReadOnly.Options.UseTextOptions = True
            Me.CalcEdit_gross.Properties.AppearanceReadOnly.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
            Me.CalcEdit_gross.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
            Me.CalcEdit_gross.Properties.DisplayFormat.FormatString = "{0:c}"
            Me.CalcEdit_gross.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
            Me.CalcEdit_gross.Properties.EditFormat.FormatString = "{0:f2}"
            Me.CalcEdit_gross.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric
            Me.CalcEdit_gross.Properties.Mask.EditMask = "c"
            Me.CalcEdit_gross.Properties.Precision = 2
            Me.CalcEdit_gross.Properties.ReadOnly = True
            Me.CalcEdit_gross.Size = New System.Drawing.Size(67, 20)
            Me.CalcEdit_gross.StyleController = Me.LayoutControl1
            Me.CalcEdit_gross.TabIndex = 9
            Me.CalcEdit_gross.TabStop = False
            '
            'SimpleButton_ok
            '
            Me.SimpleButton_ok.Location = New System.Drawing.Point(338, 175)
            Me.SimpleButton_ok.MaximumSize = New System.Drawing.Size(75, 23)
            Me.SimpleButton_ok.MinimumSize = New System.Drawing.Size(75, 23)
            Me.SimpleButton_ok.Name = "SimpleButton_ok"
            Me.SimpleButton_ok.Size = New System.Drawing.Size(75, 23)
            Me.SimpleButton_ok.StyleController = Me.LayoutControl1
            Me.SimpleButton_ok.TabIndex = 11
            Me.SimpleButton_ok.Text = "&OK"
            '
            'CalcEdit_fairshare_amt
            '
            Me.CalcEdit_fairshare_amt.Location = New System.Drawing.Point(114, 199)
            Me.CalcEdit_fairshare_amt.Name = "CalcEdit_fairshare_amt"
            Me.CalcEdit_fairshare_amt.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[False]
            Me.CalcEdit_fairshare_amt.Properties.Appearance.Options.UseTextOptions = True
            Me.CalcEdit_fairshare_amt.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
            Me.CalcEdit_fairshare_amt.Properties.AppearanceDisabled.Options.UseTextOptions = True
            Me.CalcEdit_fairshare_amt.Properties.AppearanceDisabled.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
            Me.CalcEdit_fairshare_amt.Properties.AppearanceDropDown.Options.UseTextOptions = True
            Me.CalcEdit_fairshare_amt.Properties.AppearanceDropDown.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
            Me.CalcEdit_fairshare_amt.Properties.AppearanceFocused.Options.UseTextOptions = True
            Me.CalcEdit_fairshare_amt.Properties.AppearanceFocused.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
            Me.CalcEdit_fairshare_amt.Properties.AppearanceReadOnly.Options.UseTextOptions = True
            Me.CalcEdit_fairshare_amt.Properties.AppearanceReadOnly.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
            Me.CalcEdit_fairshare_amt.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
            Me.CalcEdit_fairshare_amt.Properties.DisplayFormat.FormatString = "{0:c}"
            Me.CalcEdit_fairshare_amt.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Custom
            Me.CalcEdit_fairshare_amt.Properties.EditFormat.FormatString = "{0:f2}"
            Me.CalcEdit_fairshare_amt.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric
            Me.CalcEdit_fairshare_amt.Properties.Mask.EditMask = "c"
            Me.CalcEdit_fairshare_amt.Properties.Precision = 2
            Me.CalcEdit_fairshare_amt.Size = New System.Drawing.Size(67, 20)
            Me.CalcEdit_fairshare_amt.StyleController = Me.LayoutControl1
            Me.CalcEdit_fairshare_amt.TabIndex = 7
            '
            'ComboBoxEdit_message
            '
            Me.ComboBoxEdit_message.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
            Me.ComboBoxEdit_message.Location = New System.Drawing.Point(114, 250)
            Me.ComboBoxEdit_message.Name = "ComboBoxEdit_message"
            Me.ComboBoxEdit_message.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
            Me.ComboBoxEdit_message.Size = New System.Drawing.Size(299, 20)
            Me.ComboBoxEdit_message.StyleController = Me.LayoutControl1
            Me.ComboBoxEdit_message.TabIndex = 10
            '
            'CalcEdit_net
            '
            Me.CalcEdit_net.Location = New System.Drawing.Point(114, 175)
            Me.CalcEdit_net.Name = "CalcEdit_net"
            Me.CalcEdit_net.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[False]
            Me.CalcEdit_net.Properties.Appearance.Options.UseTextOptions = True
            Me.CalcEdit_net.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
            Me.CalcEdit_net.Properties.AppearanceDisabled.Options.UseTextOptions = True
            Me.CalcEdit_net.Properties.AppearanceDisabled.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
            Me.CalcEdit_net.Properties.AppearanceDropDown.Options.UseTextOptions = True
            Me.CalcEdit_net.Properties.AppearanceDropDown.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
            Me.CalcEdit_net.Properties.AppearanceFocused.Options.UseTextOptions = True
            Me.CalcEdit_net.Properties.AppearanceFocused.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
            Me.CalcEdit_net.Properties.AppearanceReadOnly.Options.UseTextOptions = True
            Me.CalcEdit_net.Properties.AppearanceReadOnly.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
            Me.CalcEdit_net.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
            Me.CalcEdit_net.Properties.DisplayFormat.FormatString = "{0:c}"
            Me.CalcEdit_net.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
            Me.CalcEdit_net.Properties.EditFormat.FormatString = "{0:f2}"
            Me.CalcEdit_net.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric
            Me.CalcEdit_net.Properties.Mask.EditMask = "c"
            Me.CalcEdit_net.Properties.Precision = 2
            Me.CalcEdit_net.Size = New System.Drawing.Size(67, 20)
            Me.CalcEdit_net.StyleController = Me.LayoutControl1
            Me.CalcEdit_net.TabIndex = 5
            '
            'LookUpEdit_client_creditor
            '
            Me.LookUpEdit_client_creditor.Location = New System.Drawing.Point(114, 91)
            Me.LookUpEdit_client_creditor.Name = "LookUpEdit_client_creditor"
            Me.LookUpEdit_client_creditor.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
            Me.LookUpEdit_client_creditor.Properties.Columns.AddRange(New DevExpress.XtraEditors.Controls.LookUpColumnInfo() {New DevExpress.XtraEditors.Controls.LookUpColumnInfo("client_creditor", "Debt", 20, DevExpress.Utils.FormatType.Numeric, "f0", True, DevExpress.Utils.HorzAlignment.[Default]), New DevExpress.XtraEditors.Controls.LookUpColumnInfo("account_number", "Account Number", 20, DevExpress.Utils.FormatType.None, "", True, DevExpress.Utils.HorzAlignment.[Default], DevExpress.Data.ColumnSortOrder.Ascending)})
            Me.LookUpEdit_client_creditor.Properties.DisplayMember = "account_number"
            Me.LookUpEdit_client_creditor.Properties.NullText = "REQUIRED"
            Me.LookUpEdit_client_creditor.Properties.ValueMember = "client_creditor"
            Me.LookUpEdit_client_creditor.Size = New System.Drawing.Size(299, 20)
            Me.LookUpEdit_client_creditor.StyleController = Me.LayoutControl1
            Me.LookUpEdit_client_creditor.TabIndex = 4
            '
            'LabelControl_creditor_name
            '
            Me.LabelControl_creditor_name.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
            Me.LabelControl_creditor_name.Appearance.TextOptions.Trimming = DevExpress.Utils.Trimming.Character
            Me.LabelControl_creditor_name.Appearance.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center
            Me.LabelControl_creditor_name.Appearance.TextOptions.WordWrap = DevExpress.Utils.WordWrap.NoWrap
            Me.LabelControl_creditor_name.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None
            Me.LabelControl_creditor_name.Location = New System.Drawing.Point(204, 43)
            Me.LabelControl_creditor_name.Name = "LabelControl_creditor_name"
            Me.LabelControl_creditor_name.Padding = New System.Windows.Forms.Padding(3)
            Me.LabelControl_creditor_name.Size = New System.Drawing.Size(209, 19)
            Me.LabelControl_creditor_name.StyleController = Me.LayoutControl1
            Me.LabelControl_creditor_name.TabIndex = 1
            Me.LabelControl_creditor_name.UseMnemonic = False
            '
            'SimpleButton_cancel
            '
            Me.SimpleButton_cancel.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
            Me.SimpleButton_cancel.Location = New System.Drawing.Point(338, 223)
            Me.SimpleButton_cancel.MaximumSize = New System.Drawing.Size(75, 23)
            Me.SimpleButton_cancel.MinimumSize = New System.Drawing.Size(75, 23)
            Me.SimpleButton_cancel.Name = "SimpleButton_cancel"
            Me.SimpleButton_cancel.Size = New System.Drawing.Size(75, 23)
            Me.SimpleButton_cancel.StyleController = Me.LayoutControl1
            Me.SimpleButton_cancel.TabIndex = 12
            Me.SimpleButton_cancel.Text = "&Cancel"
            '
            'LayoutControlGroup1
            '
            Me.LayoutControlGroup1.CustomizationFormText = "LayoutControlGroup1"
            Me.LayoutControlGroup1.Items.AddRange(New DevExpress.XtraLayout.BaseLayoutItem() {Me.LayoutControlGroup3, Me.LayoutControlGroup2})
            Me.LayoutControlGroup1.Location = New System.Drawing.Point(0, 0)
            Me.LayoutControlGroup1.Name = "LayoutControlGroup1"
            Me.LayoutControlGroup1.Size = New System.Drawing.Size(437, 294)
            Me.LayoutControlGroup1.Text = "LayoutControlGroup1"
            Me.LayoutControlGroup1.TextVisible = False
            '
            'LayoutControlGroup3
            '
            Me.LayoutControlGroup3.CustomizationFormText = "Refund Information"
            Me.LayoutControlGroup3.Items.AddRange(New DevExpress.XtraLayout.BaseLayoutItem() {Me.LayoutControlItem8, Me.LayoutControlItem7, Me.LayoutControlItem6, Me.LayoutControlItem10, Me.LayoutControlItem11, Me.LayoutControlItem12, Me.LayoutControlItem9, Me.EmptySpaceItem1, Me.LayoutControlItem13})
            Me.LayoutControlGroup3.Location = New System.Drawing.Point(0, 132)
            Me.LayoutControlGroup3.Name = "LayoutControlGroup3"
            Me.LayoutControlGroup3.Size = New System.Drawing.Size(417, 142)
            Me.LayoutControlGroup3.Text = "Refund Information"
            '
            'LayoutControlItem8
            '
            Me.LayoutControlItem8.Control = Me.CalcEdit_gross
            Me.LayoutControlItem8.CustomizationFormText = "Gross"
            Me.LayoutControlItem8.Location = New System.Drawing.Point(0, 48)
            Me.LayoutControlItem8.Name = "LayoutControlItem8"
            Me.LayoutControlItem8.Size = New System.Drawing.Size(161, 27)
            Me.LayoutControlItem8.Text = "Gross"
            Me.LayoutControlItem8.TextSize = New System.Drawing.Size(86, 13)
            '
            'LayoutControlItem7
            '
            Me.LayoutControlItem7.Control = Me.CalcEdit_fairshare_amt
            Me.LayoutControlItem7.CustomizationFormText = "Fairshare"
            Me.LayoutControlItem7.Location = New System.Drawing.Point(0, 24)
            Me.LayoutControlItem7.Name = "LayoutControlItem7"
            Me.LayoutControlItem7.Size = New System.Drawing.Size(161, 24)
            Me.LayoutControlItem7.Text = "Fairshare"
            Me.LayoutControlItem7.TextSize = New System.Drawing.Size(86, 13)
            '
            'LayoutControlItem6
            '
            Me.LayoutControlItem6.Control = Me.CalcEdit_net
            Me.LayoutControlItem6.CustomizationFormText = "Net"
            Me.LayoutControlItem6.Location = New System.Drawing.Point(0, 0)
            Me.LayoutControlItem6.Name = "LayoutControlItem6"
            Me.LayoutControlItem6.Size = New System.Drawing.Size(161, 24)
            Me.LayoutControlItem6.Text = "Net"
            Me.LayoutControlItem6.TextSize = New System.Drawing.Size(86, 13)
            '
            'LayoutControlItem10
            '
            Me.LayoutControlItem10.Control = Me.LookUpEdit_creditor_type
            Me.LayoutControlItem10.CustomizationFormText = "Contribution Type"
            Me.LayoutControlItem10.Location = New System.Drawing.Point(161, 0)
            Me.LayoutControlItem10.Name = "LayoutControlItem10"
            Me.LayoutControlItem10.Size = New System.Drawing.Size(153, 24)
            Me.LayoutControlItem10.Text = "Contribution Type"
            Me.LayoutControlItem10.TextSize = New System.Drawing.Size(86, 13)
            '
            'LayoutControlItem11
            '
            Me.LayoutControlItem11.Control = Me.CalcEdit_fairshare_rate
            Me.LayoutControlItem11.CustomizationFormText = "Fairshare Rate"
            Me.LayoutControlItem11.Location = New System.Drawing.Point(161, 24)
            Me.LayoutControlItem11.Name = "LayoutControlItem11"
            Me.LayoutControlItem11.Size = New System.Drawing.Size(153, 24)
            Me.LayoutControlItem11.Text = "Fairshare Rate"
            Me.LayoutControlItem11.TextSize = New System.Drawing.Size(86, 13)
            '
            'LayoutControlItem12
            '
            Me.LayoutControlItem12.Control = Me.SimpleButton_ok
            Me.LayoutControlItem12.CustomizationFormText = "LayoutControlItem12"
            Me.LayoutControlItem12.Location = New System.Drawing.Point(314, 0)
            Me.LayoutControlItem12.Name = "LayoutControlItem12"
            Me.LayoutControlItem12.Size = New System.Drawing.Size(79, 48)
            Me.LayoutControlItem12.Text = "LayoutControlItem12"
            Me.LayoutControlItem12.TextSize = New System.Drawing.Size(0, 0)
            Me.LayoutControlItem12.TextToControlDistance = 0
            Me.LayoutControlItem12.TextVisible = False
            '
            'LayoutControlItem9
            '
            Me.LayoutControlItem9.Control = Me.ComboBoxEdit_message
            Me.LayoutControlItem9.CustomizationFormText = "Message"
            Me.LayoutControlItem9.Location = New System.Drawing.Point(0, 75)
            Me.LayoutControlItem9.Name = "LayoutControlItem9"
            Me.LayoutControlItem9.Size = New System.Drawing.Size(393, 24)
            Me.LayoutControlItem9.Text = "Message"
            Me.LayoutControlItem9.TextSize = New System.Drawing.Size(86, 13)
            '
            'EmptySpaceItem1
            '
            Me.EmptySpaceItem1.AllowHotTrack = False
            Me.EmptySpaceItem1.CustomizationFormText = "EmptySpaceItem1"
            Me.EmptySpaceItem1.Location = New System.Drawing.Point(161, 48)
            Me.EmptySpaceItem1.Name = "EmptySpaceItem1"
            Me.EmptySpaceItem1.Size = New System.Drawing.Size(153, 27)
            Me.EmptySpaceItem1.Text = "EmptySpaceItem1"
            Me.EmptySpaceItem1.TextSize = New System.Drawing.Size(0, 0)
            '
            'LayoutControlItem13
            '
            Me.LayoutControlItem13.Control = Me.SimpleButton_cancel
            Me.LayoutControlItem13.CustomizationFormText = "LayoutControlItem13"
            Me.LayoutControlItem13.Location = New System.Drawing.Point(314, 48)
            Me.LayoutControlItem13.Name = "LayoutControlItem13"
            Me.LayoutControlItem13.Size = New System.Drawing.Size(79, 27)
            Me.LayoutControlItem13.Text = "LayoutControlItem13"
            Me.LayoutControlItem13.TextSize = New System.Drawing.Size(0, 0)
            Me.LayoutControlItem13.TextToControlDistance = 0
            Me.LayoutControlItem13.TextVisible = False
            '
            'LayoutControlGroup2
            '
            Me.LayoutControlGroup2.CustomizationFormText = "Client Information"
            Me.LayoutControlGroup2.Items.AddRange(New DevExpress.XtraLayout.BaseLayoutItem() {Me.LayoutControlItem3, Me.LayoutControlItem4, Me.LayoutControlItem5, Me.EmptySpaceItem2, Me.LayoutControlItem1, Me.LayoutControlItem2})
            Me.LayoutControlGroup2.Location = New System.Drawing.Point(0, 0)
            Me.LayoutControlGroup2.Name = "LayoutControlGroup2"
            Me.LayoutControlGroup2.Size = New System.Drawing.Size(417, 132)
            Me.LayoutControlGroup2.Text = "Client Information"
            '
            'LayoutControlItem3
            '
            Me.LayoutControlItem3.Control = Me.LookUpEdit_client_creditor
            Me.LayoutControlItem3.CustomizationFormText = "Account Number"
            Me.LayoutControlItem3.Location = New System.Drawing.Point(0, 48)
            Me.LayoutControlItem3.Name = "LayoutControlItem3"
            Me.LayoutControlItem3.Size = New System.Drawing.Size(393, 24)
            Me.LayoutControlItem3.Text = "Account Number"
            Me.LayoutControlItem3.TextSize = New System.Drawing.Size(86, 13)
            '
            'LayoutControlItem4
            '
            Me.LayoutControlItem4.Control = Me.LabelControl_creditor_name
            Me.LayoutControlItem4.CustomizationFormText = "LayoutControlItem4"
            Me.LayoutControlItem4.Location = New System.Drawing.Point(180, 0)
            Me.LayoutControlItem4.Name = "LayoutControlItem4"
            Me.LayoutControlItem4.Size = New System.Drawing.Size(213, 24)
            Me.LayoutControlItem4.Text = "LayoutControlItem4"
            Me.LayoutControlItem4.TextSize = New System.Drawing.Size(0, 0)
            Me.LayoutControlItem4.TextToControlDistance = 0
            Me.LayoutControlItem4.TextVisible = False
            '
            'LayoutControlItem5
            '
            Me.LayoutControlItem5.Control = Me.LabelControl_client_name
            Me.LayoutControlItem5.CustomizationFormText = "LayoutControlItem5"
            Me.LayoutControlItem5.Location = New System.Drawing.Point(180, 24)
            Me.LayoutControlItem5.Name = "LayoutControlItem5"
            Me.LayoutControlItem5.Size = New System.Drawing.Size(213, 24)
            Me.LayoutControlItem5.Text = "LayoutControlItem5"
            Me.LayoutControlItem5.TextSize = New System.Drawing.Size(0, 0)
            Me.LayoutControlItem5.TextToControlDistance = 0
            Me.LayoutControlItem5.TextVisible = False
            '
            'EmptySpaceItem2
            '
            Me.EmptySpaceItem2.AllowHotTrack = False
            Me.EmptySpaceItem2.CustomizationFormText = "EmptySpaceItem2"
            Me.EmptySpaceItem2.Location = New System.Drawing.Point(0, 72)
            Me.EmptySpaceItem2.Name = "EmptySpaceItem2"
            Me.EmptySpaceItem2.Size = New System.Drawing.Size(393, 17)
            Me.EmptySpaceItem2.Text = "EmptySpaceItem2"
            Me.EmptySpaceItem2.TextSize = New System.Drawing.Size(0, 0)
            '
            'LayoutControlItem1
            '
            Me.LayoutControlItem1.Control = Me.ClientID1
            Me.LayoutControlItem1.CustomizationFormText = "Client"
            Me.LayoutControlItem1.Location = New System.Drawing.Point(0, 24)
            Me.LayoutControlItem1.Name = "LayoutControlItem1"
            Me.LayoutControlItem1.Size = New System.Drawing.Size(180, 24)
            Me.LayoutControlItem1.Text = "Client"
            Me.LayoutControlItem1.TextSize = New System.Drawing.Size(86, 13)
            '
            'LayoutControlItem2
            '
            Me.LayoutControlItem2.Control = Me.CreditorID1
            Me.LayoutControlItem2.CustomizationFormText = "Creditor"
            Me.LayoutControlItem2.Location = New System.Drawing.Point(0, 0)
            Me.LayoutControlItem2.Name = "LayoutControlItem2"
            Me.LayoutControlItem2.Size = New System.Drawing.Size(180, 24)
            Me.LayoutControlItem2.Text = "Creditor"
            Me.LayoutControlItem2.TextSize = New System.Drawing.Size(86, 13)
            '
            'RefundItemBaseControl
            '
            Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
            Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
            Me.Controls.Add(Me.LayoutControl1)
            Me.Name = "RefundItemBaseControl"
            Me.Size = New System.Drawing.Size(437, 294)
            CType(Me.LayoutControl1, System.ComponentModel.ISupportInitialize).EndInit()
            Me.LayoutControl1.ResumeLayout(False)
            CType(Me.CreditorID1.Properties, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.ClientID1.Properties, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.LookUpEdit_creditor_type.Properties, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.CalcEdit_fairshare_rate.Properties, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.CalcEdit_gross.Properties, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.CalcEdit_fairshare_amt.Properties, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.ComboBoxEdit_message.Properties, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.CalcEdit_net.Properties, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.LookUpEdit_client_creditor.Properties, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.LayoutControlGroup1, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.LayoutControlGroup3, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.LayoutControlItem8, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.LayoutControlItem7, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.LayoutControlItem6, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.LayoutControlItem10, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.LayoutControlItem11, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.LayoutControlItem12, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.LayoutControlItem9, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.EmptySpaceItem1, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.LayoutControlItem13, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.LayoutControlGroup2, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.LayoutControlItem3, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.LayoutControlItem4, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.LayoutControlItem5, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.EmptySpaceItem2, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.LayoutControlItem1, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.LayoutControlItem2, System.ComponentModel.ISupportInitialize).EndInit()
            Me.ResumeLayout(False)

        End Sub
        Protected Friend WithEvents LabelControl_client_name As DevExpress.XtraEditors.LabelControl
        Protected Friend WithEvents LabelControl_creditor_name As DevExpress.XtraEditors.LabelControl
        Protected Friend WithEvents LookUpEdit_client_creditor As DevExpress.XtraEditors.LookUpEdit
        Protected Friend WithEvents CalcEdit_fairshare_rate As DevExpress.XtraEditors.CalcEdit
        Protected Friend WithEvents ComboBoxEdit_message As DevExpress.XtraEditors.ComboBoxEdit
        Protected Friend WithEvents SimpleButton_cancel As DevExpress.XtraEditors.SimpleButton
        Protected Friend WithEvents SimpleButton_ok As DevExpress.XtraEditors.SimpleButton
        Protected Friend WithEvents CalcEdit_fairshare_amt As DevExpress.XtraEditors.CalcEdit
        Protected Friend WithEvents CalcEdit_net As DevExpress.XtraEditors.CalcEdit
        Protected Friend WithEvents CalcEdit_gross As DevExpress.XtraEditors.CalcEdit
        Protected Friend WithEvents LookUpEdit_creditor_type As DevExpress.XtraEditors.LookUpEdit
        Friend WithEvents LayoutControl1 As DevExpress.XtraLayout.LayoutControl
        Friend WithEvents LayoutControlGroup1 As DevExpress.XtraLayout.LayoutControlGroup
        Friend WithEvents LayoutControlGroup2 As DevExpress.XtraLayout.LayoutControlGroup
        Friend WithEvents LayoutControlItem3 As DevExpress.XtraLayout.LayoutControlItem
        Friend WithEvents LayoutControlItem5 As DevExpress.XtraLayout.LayoutControlItem
        Friend WithEvents LayoutControlItem4 As DevExpress.XtraLayout.LayoutControlItem
        Friend WithEvents LayoutControlItem6 As DevExpress.XtraLayout.LayoutControlItem
        Friend WithEvents LayoutControlItem7 As DevExpress.XtraLayout.LayoutControlItem
        Friend WithEvents LayoutControlItem8 As DevExpress.XtraLayout.LayoutControlItem
        Friend WithEvents LayoutControlItem9 As DevExpress.XtraLayout.LayoutControlItem
        Friend WithEvents LayoutControlItem10 As DevExpress.XtraLayout.LayoutControlItem
        Friend WithEvents LayoutControlItem11 As DevExpress.XtraLayout.LayoutControlItem
        Friend WithEvents LayoutControlGroup3 As DevExpress.XtraLayout.LayoutControlGroup
        Friend WithEvents LayoutControlItem12 As DevExpress.XtraLayout.LayoutControlItem
        Friend WithEvents LayoutControlItem13 As DevExpress.XtraLayout.LayoutControlItem
        Friend WithEvents EmptySpaceItem1 As DevExpress.XtraLayout.EmptySpaceItem
        Friend WithEvents EmptySpaceItem2 As DevExpress.XtraLayout.EmptySpaceItem
        Friend WithEvents ClientID1 As DebtPlus.UI.Client.Widgets.Controls.ClientID
        Friend WithEvents LayoutControlItem1 As DevExpress.XtraLayout.LayoutControlItem
        Friend WithEvents CreditorID1 As CreditorID
        Friend WithEvents LayoutControlItem2 As DevExpress.XtraLayout.LayoutControlItem
    End Class
End Namespace