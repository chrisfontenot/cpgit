﻿Namespace Creditor.Refunds.Controls
    <Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
    Partial Class CreditorRefundControl
        Inherits BaseControl

        'UserControl overrides dispose to clean up the component list.
        <System.Diagnostics.DebuggerNonUserCode()> _
        Protected Overrides Sub Dispose(ByVal disposing As Boolean)
            Try
                If disposing AndAlso components IsNot Nothing Then
                    components.Dispose()
                End If
            Finally
                MyBase.Dispose(disposing)
            End Try
        End Sub

        'Required by the Windows Form Designer
        Private components As System.ComponentModel.IContainer

        'NOTE: The following procedure is required by the Windows Form Designer
        'It can be modified using the Windows Form Designer.  
        'Do not modify it using the code editor.
        <System.Diagnostics.DebuggerStepThrough()> _
        Private Sub InitializeComponent()
            Me.NewRefundItemControl1 = New DebtPlus.UI.Desktop.Creditor.Refunds.Controls.RefundItemNewControl(ctx)
            Me.BatchListControl1 = New DebtPlus.UI.Desktop.Creditor.Refunds.Controls.BatchListControl
            Me.RefundItemControl1 = New DebtPlus.UI.Desktop.Creditor.Refunds.Controls.RefundItemOldControl(ctx)
            Me.SuspendLayout()
            '
            'NewRefundItemControl1
            '
            Me.NewRefundItemControl1.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                        Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
            Me.NewRefundItemControl1.Location = New System.Drawing.Point(0, 0)
            Me.NewRefundItemControl1.Name = "NewRefundItemControl1"
            Me.NewRefundItemControl1.Size = New System.Drawing.Size(458, 292)
            Me.NewRefundItemControl1.TabIndex = 1
            '
            'BatchListControl1
            '
            Me.BatchListControl1.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                        Or System.Windows.Forms.AnchorStyles.Left) _
                        Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
            Me.BatchListControl1.Location = New System.Drawing.Point(0, 289)
            Me.BatchListControl1.Name = "BatchListControl1"
            Me.BatchListControl1.Padding = New System.Windows.Forms.Padding(4)
            Me.BatchListControl1.Size = New System.Drawing.Size(458, 154)
            Me.BatchListControl1.TabIndex = 3
            Me.BatchListControl1.TabStop = False
            '
            'RefundItemControl1
            '
            Me.RefundItemControl1.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                        Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
            Me.RefundItemControl1.Location = New System.Drawing.Point(0, 0)
            Me.RefundItemControl1.Name = "RefundItemControl1"
            Me.RefundItemControl1.Size = New System.Drawing.Size(458, 256)
            Me.RefundItemControl1.TabIndex = 2
            '
            'CreditorRefundControl
            '
            Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
            Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
            Me.Controls.Add(Me.BatchListControl1)
            Me.Controls.Add(Me.NewRefundItemControl1)
            Me.Controls.Add(Me.RefundItemControl1)
            Me.Name = "CreditorRefundControl"
            Me.Size = New System.Drawing.Size(458, 438)
            Me.ResumeLayout(False)

        End Sub
        Friend WithEvents BatchListControl1 As DebtPlus.UI.Desktop.Creditor.Refunds.Controls.BatchListControl
        Friend WithEvents NewRefundItemControl1 As DebtPlus.UI.Desktop.Creditor.Refunds.Controls.RefundItemNewControl
        Friend WithEvents RefundItemControl1 As DebtPlus.UI.Desktop.Creditor.Refunds.Controls.RefundItemOldControl

    End Class
End Namespace