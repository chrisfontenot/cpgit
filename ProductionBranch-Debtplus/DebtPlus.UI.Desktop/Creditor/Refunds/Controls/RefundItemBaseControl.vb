#Region "Copyright 2000-2012 DebtPlus, L.L.C."
'{*******************************************************************}
'{                                                                   }
'{       DebtPlus Debt Management System                             }
'{                                                                   }
'{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
'{       ALL RIGHTS RESERVED                                         }
'{                                                                   }
'{   The entire contents of this file is protected by U.S. and       }
'{   International Copyright Laws. Unauthorized reproduction,        }
'{   reverse-engineering, and distribution of all or any portion of  }
'{   the code contained in this file is strictly prohibited and may  }
'{   result in severe civil and criminal penalties and will be       }
'{   prosecuted to the maximum extent possible under the law.        }
'{                                                                   }
'{   RESTRICTIONS                                                    }
'{                                                                   }
'{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
'{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
'{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
'{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
'{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
'{                                                                   }
'{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
'{   ADDITIONAL RESTRICTIONS.                                        }
'{                                                                   }
'{*******************************************************************}

#End Region
Imports System.ComponentModel
Imports DevExpress.XtraEditors.Controls
Imports System.Data.SqlClient
Imports DebtPlus.Utils
Imports DevExpress.XtraEditors
Imports ComboboxItem = DebtPlus.Data.Controls.ComboboxItem

Namespace Creditor.Refunds.Controls
    Friend Class RefundItemBaseControl
        Protected ReadOnly ctx As CreditorRefundsContext = Nothing
        Public Event Cancelled As EventHandler
        Public Event Completed As EventHandler

        Public Sub New(ByVal ctx As CreditorRefundsContext)
            MyBase.new()
            Me.ctx = ctx
            InitializeComponent()
            AddHandler Load, AddressOf RefundItemBaseControl_Load
            AddHandler LookUpEdit_client_creditor.EditValueChanged, AddressOf LookUpEdit_client_creditor_EditValueChanged
            AddHandler CalcEdit_net.EditValueChanged, AddressOf EnableOK
            AddHandler CreditorID1.Enter, AddressOf CreditorID1_Enter
            AddHandler ClientID1.EditValueChanged, AddressOf EnableOK
            AddHandler CreditorID1.EditValueChanged, AddressOf EnableOK
            AddHandler LookUpEdit_client_creditor.EditValueChanged, AddressOf EnableOK
            AddHandler ClientID1.Enter, AddressOf CreditorID1_Enter
            AddHandler CalcEdit_fairshare_amt.Enter, AddressOf CreditorID1_Enter
            AddHandler CalcEdit_fairshare_rate.Enter, AddressOf CreditorID1_Enter
            AddHandler CalcEdit_gross.Enter, AddressOf CreditorID1_Enter
            AddHandler CalcEdit_net.Enter, AddressOf CreditorID1_Enter
            CalcEdit_fairshare_amt.Properties.DisplayFormat.Format = New DeductFormatter(Me)
        End Sub

        Protected Sub RegisterHandlers()
            AddHandler CreditorID1.Validating, AddressOf CreditorID1_Validating
            AddHandler CreditorID1.EditValueChanged, AddressOf EnableOK
            AddHandler ClientID1.Validating, AddressOf ClientID1_Validating
            AddHandler ClientID1.EditValueChanged, AddressOf EnableOK
            AddHandler CalcEdit_net.EditValueChanged, AddressOf CalcEdit_net_EditValueChanged
            AddHandler CalcEdit_fairshare_amt.EditValueChanged, AddressOf CalcEdit_net_EditValueChanged
            AddHandler CalcEdit_fairshare_rate.EditValueChanged, AddressOf CalcEdit_fairshare_rate_EditValueChanged
            AddHandler CalcEdit_fairshare_rate.EditValueChanging, AddressOf CalcEdit_net_EditValueChanging
            AddHandler CalcEdit_net.EditValueChanging, AddressOf CalcEdit_net_EditValueChanging
            AddHandler CalcEdit_net.EditValueChanged, AddressOf CalcEdit_fairshare_rate_EditValueChanged
            AddHandler CalcEdit_net.EditValueChanged, AddressOf EnableOK
            AddHandler CalcEdit_gross.EditValueChanging, AddressOf CalcEdit_net_EditValueChanging
            AddHandler LookUpEdit_creditor_type.EditValueChanged, AddressOf CalcEdit_fairshare_rate_EditValueChanged
            AddHandler LookUpEdit_creditor_type.EditValueChanged, AddressOf CalcEdit_net_EditValueChanged
            AddHandler LookUpEdit_client_creditor.EditValueChanged, AddressOf LookUpEdit_client_creditor_EditValueChanged
            AddHandler LookUpEdit_client_creditor.EditValueChanged, AddressOf EnableOK
        End Sub

        Protected Sub UnRegisterHandlers()
            RemoveHandler CreditorID1.Validating, AddressOf CreditorID1_Validating
            RemoveHandler CreditorID1.EditValueChanged, AddressOf EnableOK
            RemoveHandler ClientID1.Validating, AddressOf ClientID1_Validating
            RemoveHandler ClientID1.EditValueChanged, AddressOf EnableOK
            RemoveHandler CalcEdit_net.EditValueChanged, AddressOf CalcEdit_net_EditValueChanged
            RemoveHandler CalcEdit_fairshare_amt.EditValueChanged, AddressOf CalcEdit_net_EditValueChanged
            RemoveHandler CalcEdit_fairshare_rate.EditValueChanged, AddressOf CalcEdit_fairshare_rate_EditValueChanged
            RemoveHandler CalcEdit_fairshare_rate.EditValueChanging, AddressOf CalcEdit_net_EditValueChanging
            RemoveHandler CalcEdit_net.EditValueChanging, AddressOf CalcEdit_net_EditValueChanging
            RemoveHandler CalcEdit_net.EditValueChanged, AddressOf CalcEdit_fairshare_rate_EditValueChanged
            RemoveHandler CalcEdit_net.EditValueChanged, AddressOf EnableOK
            RemoveHandler CalcEdit_gross.EditValueChanging, AddressOf CalcEdit_net_EditValueChanging
            RemoveHandler LookUpEdit_creditor_type.EditValueChanged, AddressOf CalcEdit_fairshare_rate_EditValueChanged
            RemoveHandler LookUpEdit_creditor_type.EditValueChanged, AddressOf CalcEdit_net_EditValueChanged
            RemoveHandler LookUpEdit_client_creditor.EditValueChanged, AddressOf LookUpEdit_client_creditor_EditValueChanged
            RemoveHandler LookUpEdit_client_creditor.EditValueChanged, AddressOf EnableOK
        End Sub

        Protected Overridable Sub OnCancelled(ByVal e As EventArgs)
            RaiseEvent Cancelled(Me, e)
        End Sub

        Protected Sub RaiseCancelled()
            OnCancelled(EventArgs.Empty)
        End Sub

        Protected Overridable Sub OnCompleted(ByVal e As EventArgs)
            RaiseEvent Completed(Me, e)
        End Sub

        Protected Sub RaiseCompleted()
            OnCompleted(EventArgs.Empty)
        End Sub

        Private Class DeductFormatter
            Implements IFormatProvider
            Implements ICustomFormatter

            Public Parent As RefundItemBaseControl

            Public Sub New(ByVal Parent As RefundItemBaseControl)
                Me.Parent = Parent
            End Sub

            Public Function GetFormat(ByVal formatType As Type) As Object Implements IFormatProvider.GetFormat
                Return Me
            End Function

            Public Function Format(ByVal format1 As String, ByVal arg As Object, ByVal formatProvider As IFormatProvider) As String Implements ICustomFormatter.Format
                Dim DeductType As String = String.Empty
                If Parent.LookUpEdit_creditor_type.EditValue IsNot Nothing AndAlso Parent.LookUpEdit_creditor_type.EditValue IsNot DBNull.Value Then
                    DeductType = Convert.ToString(Parent.LookUpEdit_creditor_type.EditValue)
                End If
                If DeductType <> "D" Then
                    arg = 0D
                End If
                If arg Is Nothing OrElse arg Is DBNull.Value Then arg = 0D
                If format1 = String.Empty Then
                    Return Convert.ToDecimal(arg).ToString()
                End If
                If Not format1.StartsWith("{") Then format1 = "{0:" + format1 + "}"
                Return String.Format(format1, arg)
            End Function
        End Class

        Protected Overridable Function HasErrors() As Boolean
            Dim answer As Boolean = Not (ValidClient() AndAlso ValidCreditor() AndAlso ValidAmount())
            Return answer
        End Function

        Public Overridable Sub ReadRecord()

            ' Define the client and creditor references
            ReadClientName(ClientID1.EditValue)
            ReadCreditorName(CreditorID1.EditValue)

            ' Read the debt list if possible
            If ValidClient() AndAlso ValidCreditor() Then
                ReadDebtList(ClientID1.EditValue)
            End If

            ' Determine if there are pending errors
            SimpleButton_ok.Enabled = Not HasErrors()
            CreditorID1.Focus()
        End Sub

        Private Sub RefundItemBaseControl_Load(ByVal sender As Object, ByVal e As EventArgs)
            If Not DesignMode Then

                ' Set the controls to be the proper setting for the keypad enter key
                CalcEdit_fairshare_amt.EnterMoveNextControl = ctx.EnterMoveNextControl
                CalcEdit_fairshare_rate.EnterMoveNextControl = ctx.EnterMoveNextControl
                CalcEdit_gross.EnterMoveNextControl = ctx.EnterMoveNextControl
                CalcEdit_net.EnterMoveNextControl = ctx.EnterMoveNextControl
                ClientID1.EnterMoveNextControl = ctx.EnterMoveNextControl
                CreditorID1.EnterMoveNextControl = ctx.EnterMoveNextControl
                LookUpEdit_client_creditor.EnterMoveNextControl = ctx.EnterMoveNextControl
                LookUpEdit_creditor_type.EnterMoveNextControl = ctx.EnterMoveNextControl
                ComboBoxEdit_message.EnterMoveNextControl = ctx.EnterMoveNextControl

                ' Load the list of creditor types
                LookUpEdit_creditor_type.Properties.DataSource = DebtPlus.LINQ.InMemory.CreditorBillDeductTypes.getList()

                ' Load the list of message types
                If ComboBoxEdit_message.Properties.Items.Count = 0 Then
                    With ComboBoxEdit_message.Properties.Items
                        .Clear()
                        For Each item As DebtPlus.LINQ.message In DebtPlus.LINQ.Cache.message.getList().FindAll(Function(s) s.item_type = "RF").OrderBy(Function(s) s.description).ToList
                            .Add(New DebtPlus.Data.Controls.ComboboxItem(item.description, item.Id, item.ActiveFlag))
                        Next
                    End With
                End If
            End If
        End Sub

        Private Sub CreditorID1_Validating(ByVal sender As Object, ByVal e As CancelEventArgs)
            ReadCreditorName(CreditorID1.EditValue)
            If ValidClient() AndAlso ValidCreditor() Then LoadDebtList()
        End Sub

        Protected Sub ReadCreditorName(ByVal Creditor As Object)
            LabelControl_creditor_name.Text = String.Empty

            If Creditor IsNot Nothing AndAlso Creditor IsNot DBNull.Value Then
                Dim tbl As DataTable = ctx.ds.Tables("creditors")
                Dim row As DataRow

                If tbl IsNot Nothing Then
                    row = tbl.Rows.Find(Creditor)
                    If row IsNot Nothing Then
                        CreditorID1.ErrorText = String.Empty
                        If row("creditor_name") IsNot Nothing AndAlso row("creditor_name") IsNot DBNull.Value Then LabelControl_creditor_name.Text = Convert.ToString(row("creditor_name"))
                        CalcEdit_fairshare_rate.EditValue = row("fairshare_pct_eft")
                        LookUpEdit_creditor_type.EditValue = row("creditor_type")
                        Return
                    End If
                End If

                ' Attempt to read the creditor id from the record
                Dim cmd As SqlCommand = New SqlCommand
                With cmd
                    .Connection = New SqlConnection(DebtPlus.LINQ.SQLInfoClass.getDefault().ConnectionString)
                    .CommandText = "SELECT cr.creditor, cr.creditor_name, pct.creditor_type_eft as creditor_type, pct.fairshare_pct_eft, pct.fairshare_pct_check FROM creditors cr LEFT OUTER JOIN creditor_contribution_pcts pct ON cr.creditor_contribution_pct = pct.creditor_contribution_pct WHERE cr.creditor = @creditor"
                    .CommandType = CommandType.Text
                    .Parameters.Add("@creditor", SqlDbType.VarChar, 10).Value = Creditor
                End With

                ' Read the information from the creditor tables
                Dim da As New SqlDataAdapter(cmd)
                da.Fill(ctx.ds, "creditors")
                da.Dispose()
                tbl = ctx.ds.Tables("creditors")
                If tbl.PrimaryKey.Count = 0 Then tbl.PrimaryKey = New DataColumn() {tbl.Columns(0)}

                ' Locate the creditor information
                row = tbl.Rows.Find(Creditor)
                If row IsNot Nothing Then
                    CreditorID1.ErrorText = String.Empty
                    If row("creditor_name") IsNot Nothing AndAlso row("creditor_name") IsNot DBNull.Value Then LabelControl_creditor_name.Text = Convert.ToString(row("creditor_name"))
                    CalcEdit_fairshare_rate.EditValue = row("fairshare_pct_eft")
                    LookUpEdit_creditor_type.EditValue = row("creditor_type")
                    Return
                End If
            End If

            ' The creditor is not valid
            CreditorID1.ErrorText = "Invalid creditor ID"
        End Sub

        Protected Function ValidCreditor() As Boolean
            Return CreditorID1.EditValue IsNot Nothing AndAlso CreditorID1.EditValue IsNot DBNull.Value AndAlso CreditorID1.ErrorText = String.Empty
        End Function

        Private Sub ClientID1_Validating(ByVal sender As Object, ByVal e As CancelEventArgs)
            ReadClientName(ClientID1.EditValue)
            If ValidClient() AndAlso ValidCreditor() Then LoadDebtList()
        End Sub

        Protected Sub ReadClientName(ByVal client As Int32?)

            LabelControl_client_name.Text = String.Empty
            If client IsNot Nothing Then
                Dim tbl As DataTable = ctx.ds.Tables("clients")
                Dim row As DataRow

                If tbl IsNot Nothing Then
                    row = tbl.Rows.Find(client.Value)
                    If row IsNot Nothing Then
                        ClientID1.ErrorText = String.Empty
                        If row("client_name") IsNot Nothing AndAlso row("client_name") IsNot DBNull.Value Then LabelControl_client_name.Text = Convert.ToString(row("client_name"))
                        Return
                    End If
                End If

                ' Attempt to read the creditor id from the record
                Dim cmd As SqlCommand = New SqlCommand
                With cmd
                    .Connection = New SqlConnection(DebtPlus.LINQ.SQLInfoClass.getDefault().ConnectionString)
                    .CommandText = "SELECT c.client, dbo.format_normal_name(pn.prefix, pn.first, pn.middle, pn.last, pn.suffix) as client_name FROM clients c LEFT OUTER JOIN people p on c.client = p.client and 1 = p.relation LEFT OUTER JOIN names pn WITH (NOLOCK) ON p.NameID = pn.Name WHERE c.client = @client"
                    .CommandType = CommandType.Text
                    .Parameters.Add("@client", SqlDbType.Int).Value = client.Value
                End With

                ' Read the information from the creditor tables
                Dim da As New SqlDataAdapter(cmd)
                da.Fill(ctx.ds, "clients")
                da.Dispose()
                tbl = ctx.ds.Tables("clients")
                If tbl.PrimaryKey.Count = 0 Then tbl.PrimaryKey = New DataColumn() {tbl.Columns(0)}

                ' Locate the creditor information
                row = tbl.Rows.Find(client.Value)
                If row IsNot Nothing Then
                    CreditorID1.ErrorText = String.Empty
                    If row("client_name") IsNot Nothing AndAlso row("client_name") IsNot DBNull.Value Then LabelControl_client_name.Text = Convert.ToString(row("client_name"))
                    Return
                End If
            End If

            ' The client is not valid
            ClientID1.ErrorText = "Invalid Client ID"
        End Sub

        Protected Function ValidClient() As Boolean
            Return ClientID1.EditValue.GetValueOrDefault(-1) >= 0 AndAlso ClientID1.ErrorText = String.Empty
        End Function

        Protected Sub LoadDebtList()

            ' Find the table
            Dim tbl As DataTable = ctx.ds.Tables("client_creditor")
            If tbl Is Nothing Then
                ReadDebtList(ClientID1.EditValue)
                tbl = ctx.ds.Tables("client_creditor")
            End If

            ' Look to see if we have any debts for this client in the list.
            Dim vue As New DataView(tbl, String.Format("[client]={0:f0}", ClientID1.EditValue.Value), String.Empty, DataViewRowState.CurrentRows)
            If vue.Count = 0 Then
                ReadDebtList(ClientID1.EditValue)
                tbl = ctx.ds.Tables("client_creditor")
            End If

            ' Find the proper view for the client and creditor reference
            vue = New DataView(tbl, String.Format("[creditor]='{0}' AND [client]={1:f0}", Convert.ToString(CreditorID1.EditValue), ClientID1.EditValue.Value), "client_creditor", DataViewRowState.CurrentRows)

            ' Load the view into the list of debts
            LookUpEdit_client_creditor.Properties.DataSource = vue

            ' If there are debts then clear the error else set it.
            If vue.Count <= 0 Then
                LookUpEdit_client_creditor.ErrorText = "This client has no debt for this creditor. Wrong ClientId? Wrong creditor?"
            Else
                LookUpEdit_client_creditor.ErrorText = String.Empty

                ' If there is only one debt then select that debt
                If vue.Count = 1 Then LookUpEdit_client_creditor.EditValue = vue(0)("client_creditor")
            End If
        End Sub

        Protected Sub ReadDebtList(ByVal Client As Int32?)
            Using cmd As SqlCommand = New SqlCommand
                With cmd
                    .Connection = New SqlConnection(DebtPlus.LINQ.SQLInfoClass.getDefault().ConnectionString)
                    .CommandType = CommandType.Text
                    .CommandText = "SELECT cc.client, cc.creditor, cc.client_creditor, cc.account_number, cc.fairshare_pct_eft, cc.fairshare_pct_check FROM client_creditor cc WITH (NOLOCK) WHERE cc.client=@client"
                    .Parameters.Add("@client", SqlDbType.Int).Value = Client.GetValueOrDefault(-1)
                End With

                Using da As New SqlDataAdapter(cmd)
                    da.Fill(ctx.ds, "client_creditor")
                End Using
            End Using

            ' Create the primary key to the table if required
            Dim tbl As DataTable = ctx.ds.Tables("client_creditor")
            If tbl.PrimaryKey.GetUpperBound(0) < 0 Then tbl.PrimaryKey = New DataColumn() {tbl.Columns("client_creditor")}
        End Sub

        Private Sub CalcEdit_net_EditValueChanged(ByVal sender As Object, ByVal e As EventArgs)
            Dim DeductAmount As Decimal = 0D
            If Convert.ToString(LookUpEdit_creditor_type.EditValue) = "D" Then DeductAmount = Convert.ToDecimal(CalcEdit_fairshare_amt.EditValue)
            CalcEdit_gross.EditValue = Convert.ToDecimal(CalcEdit_net.EditValue) + DeductAmount
        End Sub

        Protected Function ValidAmount() As Boolean
            Return CalcEdit_net.EditValue IsNot Nothing AndAlso CalcEdit_net.EditValue IsNot DBNull.Value AndAlso Convert.ToDecimal(CalcEdit_net.EditValue) > 0D AndAlso CalcEdit_net.ErrorText = String.Empty
        End Function

        Private Sub CalcEdit_fairshare_rate_EditValueChanged(ByVal sender As Object, ByVal e As EventArgs)

            ' Find the net amount
            Dim Net As Decimal = 0D
            If CalcEdit_net.EditValue IsNot Nothing AndAlso CalcEdit_net.EditValue IsNot DBNull.Value Then Net = Convert.ToDecimal(CalcEdit_net.EditValue)

            ' Find the rate for the discount
            Dim Rate As Double = 0.0#
            If CalcEdit_fairshare_rate.EditValue IsNot Nothing AndAlso CalcEdit_fairshare_rate IsNot DBNull.Value Then Rate = Convert.ToDouble(CalcEdit_fairshare_rate.EditValue)

            ' Force the rate to be zero if it is NOT "deduct"
            Dim CreditorType As String = Convert.ToString(LookUpEdit_creditor_type.EditValue)
            If CreditorType <> "D" Then Rate = 0.0#
            Do While Rate >= 1.0
                Rate /= 100.0
            Loop

            ' If the rate is zero then the fairshare is zero.
            If Rate <= 0.0 Then
                CalcEdit_fairshare_amt.EditValue = 0D
            Else
                ' Otherwise, calculate it as a new gross figure to find the fairshare amount
                Dim NewGross As Decimal = Math.Truncate(Convert.ToDecimal(Convert.ToDouble(Net) / (1.0# - Rate)), 2)
                CalcEdit_fairshare_amt.EditValue = NewGross - Net
            End If
        End Sub

        Private Sub CalcEdit_net_EditValueChanging(ByVal sender As Object, ByVal e As ChangingEventArgs)

            ' If this is the fairshare rate then do additional logic
            If sender Is CalcEdit_fairshare_rate Then
                If e.NewValue IsNot Nothing AndAlso e.NewValue IsNot DBNull.Value Then
                    Dim Rate As Double = Convert.ToDouble(e.NewValue)
                    If Rate < 0.0 Then
                        e.Cancel = True
                        Return
                    End If

                    If Rate >= 1.0 Then
                        Do While Rate >= 1.0
                            Rate /= 100.0
                        Loop

                        ' Reduce the rate to the proper value
                        CalcEdit_fairshare_rate.EditValue = Rate
                        e.Cancel = True
                        Return
                    End If
                End If
            End If

            e.Cancel = e.NewValue Is Nothing OrElse e.NewValue Is DBNull.Value OrElse Convert.ToDecimal(e.NewValue) < 0D
        End Sub

        Private Sub LookUpEdit_client_creditor_EditValueChanged(ByVal sender As Object, ByVal e As EventArgs)
            Dim client_creditor As Int32 = -1
            If LookUpEdit_client_creditor.EditValue IsNot Nothing AndAlso LookUpEdit_client_creditor.EditValue IsNot DBNull.Value Then client_creditor = Convert.ToInt32(LookUpEdit_client_creditor.EditValue)
            If client_creditor > 0 Then
                Dim row As DataRow = ctx.ds.Tables("client_creditor").Rows.Find(client_creditor)
                If row IsNot Nothing Then
                    If row("fairshare_pct_eft") IsNot Nothing AndAlso row("fairshare_pct_eft") IsNot DBNull.Value Then CalcEdit_fairshare_rate.EditValue = Convert.ToDouble(row("fairshare_pct_eft"))
                End If
            End If
        End Sub

        Private Sub EnableOK(ByVal sender As Object, ByVal e As EventArgs)
            SimpleButton_ok.Enabled = Not HasErrors()
        End Sub

        Private Sub CreditorID1_Enter(ByVal sender As Object, ByVal e As EventArgs)
            CType(sender, TextEdit).SelectAll()
        End Sub
    End Class
End Namespace
