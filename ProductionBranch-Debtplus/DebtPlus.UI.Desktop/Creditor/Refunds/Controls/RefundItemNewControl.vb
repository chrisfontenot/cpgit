#Region "Copyright 2000-2012 DebtPlus, L.L.C."
'{*******************************************************************}
'{                                                                   }
'{       DebtPlus Debt Management System                             }
'{                                                                   }
'{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
'{       ALL RIGHTS RESERVED                                         }
'{                                                                   }
'{   The entire contents of this file is protected by U.S. and       }
'{   International Copyright Laws. Unauthorized reproduction,        }
'{   reverse-engineering, and distribution of all or any portion of  }
'{   the code contained in this file is strictly prohibited and may  }
'{   result in severe civil and criminal penalties and will be       }
'{   prosecuted to the maximum extent possible under the law.        }
'{                                                                   }
'{   RESTRICTIONS                                                    }
'{                                                                   }
'{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
'{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
'{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
'{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
'{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
'{                                                                   }
'{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
'{   ADDITIONAL RESTRICTIONS.                                        }
'{                                                                   }
'{*******************************************************************}
#End Region

Namespace Creditor.Refunds.Controls
    Friend Class RefundItemNewControl

        Public Sub New(ByVal ctx As CreditorRefundsContext)
            MyBase.new(ctx)
            InitializeComponent()

            ' Add the standard handlers when the control is created
            AddHandler SimpleButton_cancel.Click, AddressOf SimpleButton_cancel_Click
            AddHandler SimpleButton_ok.Click, AddressOf SimpleButton_ok_Click
        End Sub

        ' Save the previous values for the input
        Dim _lastCreditorType As Object = "N"
        Dim _lastFairsharePct As Object = 0.0#

        Public Overrides Sub ReadRecord()

            ' Do not do update events until the record is properly defined
            UnRegisterHandlers()

            ' Set the values into the controls
            ClientID1.EditValue = Nothing
            CreditorID1.EditValue = Nothing
            LookUpEdit_client_creditor.EditValue = Nothing
            CalcEdit_fairshare_amt.EditValue = 0D
            CalcEdit_net.EditValue = 0D
            CalcEdit_gross.EditValue = 0D
            CalcEdit_fairshare_rate.EditValue = _lastFairsharePct
            LookUpEdit_creditor_type.EditValue = _lastCreditorType

            ' Go to the standard processing logic
            MyBase.ReadRecord()

            ' Enable the update events to track the changes
            RegisterHandlers()
        End Sub

        Public Sub TakeFocus()
            With CType(Parent, CreditorRefundControl)
                .BatchListControl1.Enabled = True
            End With

            With Me
                .BringToFront()
                .Visible = True
                .TabStop = True
            End With

            ' Go to the creditor ID information
            CreditorID1.Focus()
        End Sub

        Public Sub GiveFocus()
            With Me
                .SendToBack()
                .Visible = False
                .TabStop = False
            End With
        End Sub

        Private Sub SimpleButton_ok_Click(ByVal sender As Object, ByVal e As EventArgs)

            ' Save the previous values for the input
            _lastCreditorType = LookUpEdit_creditor_type.EditValue
            _lastFairsharePct = CalcEdit_fairshare_rate.EditValue

            ' Add a new record to the list and populate the controls with the values
            Dim drv As DataRowView = ctx.ds.Tables("deposit_batch_details").DefaultView.AddNew
            drv.BeginEdit()

            ' Update the fields for the row
            drv("client") = DebtPlus.Utils.Nulls.ToDbNull(ClientID1.EditValue)
            drv("creditor") = CreditorID1.EditValue
            drv("amount") = CalcEdit_gross.EditValue
            drv("fairshare_amt") = CalcEdit_fairshare_amt.EditValue
            drv("fairshare_pct") = CalcEdit_fairshare_rate.EditValue
            drv("creditor_type") = LookUpEdit_creditor_type.EditValue
            drv("reference") = ComboBoxEdit_message.Text.Trim()
            drv("client_creditor") = LookUpEdit_client_creditor.EditValue

            ' End the edit operation
            drv.EndEdit()

            ' Load the control with the new record
            ReadRecord()
        End Sub

        Private Sub SimpleButton_cancel_Click(ByVal sender As Object, ByVal e As EventArgs)

            ' Complete the transactions
            With CType(ParentForm, Form_CreditorRefund)
                .Close()
            End With
        End Sub
    End Class
End Namespace