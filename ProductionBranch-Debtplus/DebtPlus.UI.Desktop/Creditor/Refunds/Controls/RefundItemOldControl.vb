#Region "Copyright 2000-2012 DebtPlus, L.L.C."
'{*******************************************************************}
'{                                                                   }
'{       DebtPlus Debt Management System                             }
'{                                                                   }
'{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
'{       ALL RIGHTS RESERVED                                         }
'{                                                                   }
'{   The entire contents of this file is protected by U.S. and       }
'{   International Copyright Laws. Unauthorized reproduction,        }
'{   reverse-engineering, and distribution of all or any portion of  }
'{   the code contained in this file is strictly prohibited and may  }
'{   result in severe civil and criminal penalties and will be       }
'{   prosecuted to the maximum extent possible under the law.        }
'{                                                                   }
'{   RESTRICTIONS                                                    }
'{                                                                   }
'{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
'{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
'{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
'{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
'{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
'{                                                                   }
'{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
'{   ADDITIONAL RESTRICTIONS.                                        }
'{                                                                   }
'{*******************************************************************}

#End Region

Namespace Creditor.Refunds.Controls
    Friend Class RefundItemOldControl

        Public Sub New(ByVal ctx As CreditorRefundsContext)
            MyBase.new(ctx)
            InitializeComponent()
            AddHandler SimpleButton_ok.Click, AddressOf SimpleButton_ok_Click
            AddHandler SimpleButton_cancel.Click, AddressOf SimpleButton_cancel_Click
        End Sub

        Private EditDrv As DataRowView = Nothing

        Public Overloads Sub ReadRecord(ByVal drv As DataRowView)
            EditDrv = drv

            ' Do not do update events until the record is properly defined
            UnRegisterHandlers()

            ' Set the values into the controls
            ClientID1.EditValue = DebtPlus.Utils.Nulls.v_Int32(drv("client"))
            ReadClientName(ClientID1.EditValue)

            CreditorID1.EditValue = Nothing
            If drv("creditor") IsNot DBNull.Value Then CreditorID1.EditValue = Convert.ToString(drv("creditor"))
            ReadCreditorName(CreditorID1.EditValue)

            LookUpEdit_client_creditor.EditValue = drv("client_creditor")
            CalcEdit_fairshare_amt.EditValue = drv("fairshare_amt")
            CalcEdit_gross.EditValue = drv("amount")
            CalcEdit_net.EditValue = drv("net_amt")
            ComboBoxEdit_message.Text = Convert.ToString(drv("reference"))

            ' Go to the standard processing logic
            ReadRecord()

            ' Since these are changed when we lookup the creditor, put them back.
            LookUpEdit_creditor_type.EditValue = drv("creditor_type")
            CalcEdit_fairshare_rate.EditValue = drv("fairshare_pct")

            ' Enable the update events to track the changes
            RegisterHandlers()
        End Sub

        Public Sub TakeFocus()
            With CType(Parent, CreditorRefundControl)
                .BatchListControl1.Enabled = False
            End With

            With Me
                .BringToFront()
                .Visible = True
                .TabStop = True
            End With

            ' Go to the creditor ID information
            CreditorID1.Focus()
        End Sub

        Public Sub GiveFocus()
            With Me
                .SendToBack()
                .Visible = False
                .TabStop = False
            End With
        End Sub

        Private Sub SimpleButton_ok_Click(ByVal sender As Object, ByVal e As EventArgs)

            ' Start the edit operation on the row
            EditDrv.BeginEdit()

            ' Update the fields for the row
            EditDrv("client") = DebtPlus.Utils.Nulls.ToDbNull(ClientID1.EditValue)
            EditDrv("creditor") = CreditorID1.EditValue
            EditDrv("amount") = CalcEdit_gross.EditValue
            EditDrv("fairshare_amt") = CalcEdit_fairshare_amt.EditValue
            EditDrv("fairshare_pct") = CalcEdit_fairshare_rate.EditValue
            EditDrv("creditor_type") = LookUpEdit_creditor_type.EditValue
            EditDrv("reference") = ComboBoxEdit_message.Text.Trim()
            EditDrv("client_creditor") = LookUpEdit_client_creditor.EditValue

            EditDrv.EndEdit()

            With CType(Me.Parent, CreditorRefundControl)
                .RefundItemControl1.GiveFocus()
                .NewRefundItemControl1.TakeFocus()
            End With
        End Sub

        Private Sub SimpleButton_cancel_Click(ByVal sender As Object, ByVal e As EventArgs)

            With CType(Me.Parent, CreditorRefundControl)
                .RefundItemControl1.GiveFocus()
                .NewRefundItemControl1.TakeFocus()
            End With
        End Sub
    End Class
End Namespace
