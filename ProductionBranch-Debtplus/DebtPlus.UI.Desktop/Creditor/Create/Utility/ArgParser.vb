Option Compare Binary
Option Explicit On 
Option Strict On

Namespace Creditor.Create.Utility

    Friend Class ArgParser
        Inherits DebtPlus.Utils.ArgParserBase

        ''' <summary>
        ''' Client for the update operation
        ''' </summary>
        Private _type As String = String.Empty
        Public ReadOnly Property Type() As String
            Get
                Return _type
            End Get
        End Property

        ''' <summary>
        ''' Create an instance of our class
        ''' </summary>
        Public Sub New()
            MyBase.New(New String() {"t"})
        End Sub

        ''' <summary>
        ''' Process the switch options
        ''' </summary>
        Protected Overrides Function OnSwitch(ByVal switchSymbol As String, ByVal switchValue As String) As SwitchStatus
            Dim ss As SwitchStatus = DebtPlus.Utils.ArgParserBase.SwitchStatus.NoError
            Select Case switchSymbol
                Case "t"
                    _type = switchValue
                Case "?"
                    ss = DebtPlus.Utils.ArgParserBase.SwitchStatus.ShowUsage
                Case Else
                    ss = DebtPlus.Utils.ArgParserBase.SwitchStatus.YesError
            End Select
            Return ss
        End Function

        ''' <summary>
        ''' Generate the command usage information
        ''' </summary>
        Protected Overrides Sub OnUsage(ByVal errorInfo As String)
            'AppendErrorLine("Usage: " + fname + " [-t#]")
            'AppendErrorLine("       -t : specify the type letter for the new creditor")
        End Sub
    End Class

End Namespace
