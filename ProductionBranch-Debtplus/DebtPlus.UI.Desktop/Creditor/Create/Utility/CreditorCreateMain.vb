Option Compare Binary
Option Explicit On 
Option Strict On

Imports DebtPlus.UI.Creditor.Create
Imports DebtPlus.UI.Creditor.Update
Imports System.Windows.Forms

Namespace Creditor.Create.Utility

    Friend Class CreditorCreateMain
        Implements System.IDisposable

        Private CreditorID As String = System.String.Empty

        Public Sub New(ByVal ap As ArgParser)
            MyBase.new()
        End Sub

        Public Sub ShowDialog()
            Dim creditorRecord As DebtPlus.LINQ.creditor = Nothing
            Dim CreditorID As String = Nothing

            ' Do the create creditor function. It will create the creditor on the database at this point.
            Using createClass As New DebtPlus.UI.Creditor.Create.CreateCreditorClass()
                If createClass.ShowDialog() <> DialogResult.OK Then
                    Return
                End If

                ' This is the ID of the new creditor.
                CreditorID = createClass.Creditor
            End Using

            ' Do the edit operation on the new creditor
            Dim bc As New DebtPlus.LINQ.BusinessContext()
            Try
                creditorRecord = bc.creditors.Where(Function(s) s.Id = CreditorID).FirstOrDefault()
                If creditorRecord Is Nothing Then
                    DebtPlus.Data.Forms.MessageBox.Show("The creditor is no longer in the system. The edit operation is cancelled.", "Sorry, can not find the creditor", MessageBoxButtons.OK, MessageBoxIcon.Error)
                    Return
                End If

                ' Update the MRU with the new creditor so that it will show in the list
                Using mru As New DebtPlus.Data.MRU("Creditors")
                    mru.InsertTopMost(CreditorID)
                    mru.SaveKey()
                End Using

                AddHandler bc.BeforeSubmitChanges, AddressOf bc.RecordSystemNoteHandler

                ' We have a creditor record. Do the edit operation on the new creditor
                Using frm As New DebtPlus.UI.Creditor.Update.Forms.Form_CreditorUpdate(bc, creditorRecord, True)
                    frm.ShowDialog()
                End Using

                ' Submit the changes before we leave.
                bc.SubmitChanges()

            Catch ex As System.Data.SqlClient.SqlException
                DebtPlus.UI.Common.ErrorHandling.HandleErrors(ex, "Error reading creditor information")

            Finally
                RemoveHandler bc.BeforeSubmitChanges, AddressOf bc.RecordSystemNoteHandler
                bc.Dispose()
            End Try
        End Sub

#Region "IDisposable Support"
            Private disposedValue As Boolean ' To detect redundant calls

            ' IDisposable
        Protected Overridable Sub Dispose(disposing As Boolean)
            If Not Me.disposedValue Then
                If disposing Then
                    ' TODO: dispose managed state (managed objects).
                End If

                ' TODO: free unmanaged resources (unmanaged objects) and override Finalize() below.
                ' TODO: set large fields to null.
            End If
            Me.disposedValue = True
        End Sub

        ' TODO: override Finalize() only if Dispose(ByVal disposing As Boolean) above has code to free unmanaged resources.
        'Protected Overrides Sub Finalize()
        '    ' Do not change this code.  Put cleanup code in Dispose(ByVal disposing As Boolean) above.
        '    Dispose(False)
        '    MyBase.Finalize()
        'End Sub

        ' This code added by Visual Basic to correctly implement the disposable pattern.
        Public Sub Dispose() Implements IDisposable.Dispose
            ' Do not change this code.  Put cleanup code in Dispose(ByVal disposing As Boolean) above.
            Dispose(True)
            GC.SuppressFinalize(Me)
        End Sub
#End Region
    End Class

End Namespace