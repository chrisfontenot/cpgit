﻿Namespace Creditor.Billing

    Partial Class FormStatus
        Inherits DebtPlus.Data.Forms.DebtPlusForm

        'Form overrides dispose to clean up the component list.
        Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
            If disposing Then
                If components IsNot Nothing Then
                    components.Dispose()
                End If
            End If
            MyBase.Dispose(disposing)
        End Sub

        'Required by the Windows Form Designer
        Private components As System.ComponentModel.IContainer

        'NOTE: The following procedure is required by the Windows Form Designer
        'It can be modified using the Windows Form Designer.  
        'Do not modify it using the code editor.
        Friend WithEvents Timer1 As System.Windows.Forms.Timer
        Friend WithEvents Button_Cancel As DevExpress.XtraEditors.SimpleButton
        Friend WithEvents Label_ElapsedTime As DevExpress.XtraEditors.LabelControl
        Friend WithEvents Label_Amount As DevExpress.XtraEditors.LabelControl
        Friend WithEvents Label_Invoice As DevExpress.XtraEditors.LabelControl
        Friend WithEvents Label_Creditor As DevExpress.XtraEditors.LabelControl
        Friend WithEvents label_Caption As DevExpress.XtraEditors.LabelControl
        Friend WithEvents Label4 As DevExpress.XtraEditors.LabelControl
        Friend WithEvents Label3 As DevExpress.XtraEditors.LabelControl
        Friend WithEvents Label2 As DevExpress.XtraEditors.LabelControl
        Friend WithEvents Label1 As DevExpress.XtraEditors.LabelControl
        <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
            Me.components = New System.ComponentModel.Container
            Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(FormStatus))
            Me.Timer1 = New System.Windows.Forms.Timer(Me.components)
            Me.Button_Cancel = New DevExpress.XtraEditors.SimpleButton
            Me.Label_ElapsedTime = New DevExpress.XtraEditors.LabelControl
            Me.Label_Amount = New DevExpress.XtraEditors.LabelControl
            Me.Label_Invoice = New DevExpress.XtraEditors.LabelControl
            Me.Label_Creditor = New DevExpress.XtraEditors.LabelControl
            Me.label_Caption = New DevExpress.XtraEditors.LabelControl
            Me.Label4 = New DevExpress.XtraEditors.LabelControl
            Me.Label3 = New DevExpress.XtraEditors.LabelControl
            Me.Label2 = New DevExpress.XtraEditors.LabelControl
            Me.Label1 = New DevExpress.XtraEditors.LabelControl
            CType(Me.DxErrorProvider1, System.ComponentModel.ISupportInitialize).BeginInit()
            Me.SuspendLayout()
            '
            'Timer1
            '
            '
            'Button_Cancel
            '
            Me.Button_Cancel.Location = New System.Drawing.Point(116, 144)
            Me.Button_Cancel.Name = "Button_Cancel"
            Me.Button_Cancel.Size = New System.Drawing.Size(80, 26)
            Me.Button_Cancel.TabIndex = 24
            Me.Button_Cancel.Text = "&Cancel"
            Me.Button_Cancel.ToolTip = "Cancel the invoice creation at the next most convienient point"
            Me.Button_Cancel.ToolTipController = Me.ToolTipController1
            '
            'Label_ElapsedTime
            '
            Me.Label_ElapsedTime.Appearance.Options.UseTextOptions = True
            Me.Label_ElapsedTime.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
            Me.Label_ElapsedTime.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None
            Me.Label_ElapsedTime.Location = New System.Drawing.Point(148, 103)
            Me.Label_ElapsedTime.Name = "Label_ElapsedTime"
            Me.Label_ElapsedTime.Size = New System.Drawing.Size(112, 13)
            Me.Label_ElapsedTime.TabIndex = 23
            Me.Label_ElapsedTime.ToolTipController = Me.ToolTipController1
            Me.Label_ElapsedTime.UseMnemonic = False
            '
            'Label_Amount
            '
            Me.Label_Amount.Appearance.Options.UseTextOptions = True
            Me.Label_Amount.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
            Me.Label_Amount.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None
            Me.Label_Amount.Location = New System.Drawing.Point(148, 77)
            Me.Label_Amount.Name = "Label_Amount"
            Me.Label_Amount.Size = New System.Drawing.Size(112, 13)
            Me.Label_Amount.TabIndex = 22
            Me.Label_Amount.Text = "$0.00"
            Me.Label_Amount.ToolTipController = Me.ToolTipController1
            Me.Label_Amount.UseMnemonic = False
            '
            'Label_Invoice
            '
            Me.Label_Invoice.Appearance.Options.UseTextOptions = True
            Me.Label_Invoice.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
            Me.Label_Invoice.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None
            Me.Label_Invoice.Location = New System.Drawing.Point(148, 90)
            Me.Label_Invoice.Name = "Label_Invoice"
            Me.Label_Invoice.Size = New System.Drawing.Size(112, 13)
            Me.Label_Invoice.TabIndex = 21
            Me.Label_Invoice.Text = "00000000"
            Me.Label_Invoice.ToolTipController = Me.ToolTipController1
            Me.Label_Invoice.UseMnemonic = False
            '
            'Label_Creditor
            '
            Me.Label_Creditor.Appearance.Options.UseTextOptions = True
            Me.Label_Creditor.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
            Me.Label_Creditor.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None
            Me.Label_Creditor.Location = New System.Drawing.Point(148, 64)
            Me.Label_Creditor.Name = "Label_Creditor"
            Me.Label_Creditor.Size = New System.Drawing.Size(112, 13)
            Me.Label_Creditor.TabIndex = 20
            Me.Label_Creditor.ToolTipController = Me.ToolTipController1
            Me.Label_Creditor.UseMnemonic = False
            '
            'label_Caption
            '
            Me.label_Caption.Appearance.Options.UseTextOptions = True
            Me.label_Caption.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
            Me.label_Caption.Appearance.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Top
            Me.label_Caption.Appearance.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap
            Me.label_Caption.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None
            Me.label_Caption.Location = New System.Drawing.Point(16, 12)
            Me.label_Caption.Name = "label_Caption"
            Me.label_Caption.Size = New System.Drawing.Size(284, 46)
            Me.label_Caption.TabIndex = 19
            Me.label_Caption.Text = "The database connections are being established. Please wait for the operation to " & _
                "be completed since it may not be cancelled until the process actually starts."
            Me.label_Caption.ToolTipController = Me.ToolTipController1
            Me.label_Caption.UseMnemonic = False
            '
            'Label4
            '
            Me.Label4.Location = New System.Drawing.Point(16, 103)
            Me.Label4.Name = "Label4"
            Me.Label4.Size = New System.Drawing.Size(66, 13)
            Me.Label4.TabIndex = 18
            Me.Label4.Text = "Elapsed Time:"
            Me.Label4.ToolTipController = Me.ToolTipController1
            Me.Label4.UseMnemonic = False
            '
            'Label3
            '
            Me.Label3.Location = New System.Drawing.Point(16, 77)
            Me.Label3.Name = "Label3"
            Me.Label3.Size = New System.Drawing.Size(79, 13)
            Me.Label3.TabIndex = 17
            Me.Label3.Text = "Invoice Amount:"
            Me.Label3.ToolTipController = Me.ToolTipController1
            Me.Label3.UseMnemonic = False
            '
            'Label2
            '
            Me.Label2.Location = New System.Drawing.Point(16, 90)
            Me.Label2.Name = "Label2"
            Me.Label2.Size = New System.Drawing.Size(93, 13)
            Me.Label2.TabIndex = 16
            Me.Label2.Text = "Processing Invoice:"
            Me.Label2.ToolTipController = Me.ToolTipController1
            Me.Label2.UseMnemonic = False
            '
            'Label1
            '
            Me.Label1.Location = New System.Drawing.Point(16, 64)
            Me.Label1.Name = "Label1"
            Me.Label1.Size = New System.Drawing.Size(97, 13)
            Me.Label1.TabIndex = 15
            Me.Label1.Text = "Processing Creditor:"
            Me.Label1.ToolTipController = Me.ToolTipController1
            Me.Label1.UseMnemonic = False
            '
            'FormStatus
            '
            Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
            Me.ClientSize = New System.Drawing.Size(312, 182)
            Me.Controls.Add(Me.Button_Cancel)
            Me.Controls.Add(Me.Label_ElapsedTime)
            Me.Controls.Add(Me.Label_Amount)
            Me.Controls.Add(Me.Label_Invoice)
            Me.Controls.Add(Me.Label_Creditor)
            Me.Controls.Add(Me.label_Caption)
            Me.Controls.Add(Me.Label4)
            Me.Controls.Add(Me.Label3)
            Me.Controls.Add(Me.Label2)
            Me.Controls.Add(Me.Label1)
            Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
            Me.MaximizeBox = False
            Me.MaximumSize = New System.Drawing.Size(320, 216)
            Me.MinimizeBox = False
            Me.MinimumSize = New System.Drawing.Size(320, 216)
            Me.Name = "FormStatus"
            Me.Text = "Status"
            Me.TopMost = True
            CType(Me.DxErrorProvider1, System.ComponentModel.ISupportInitialize).EndInit()
            Me.ResumeLayout(False)
            Me.PerformLayout()

        End Sub
    End Class
End Namespace