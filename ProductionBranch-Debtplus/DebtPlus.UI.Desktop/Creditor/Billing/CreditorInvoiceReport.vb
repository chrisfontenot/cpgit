#Region "Copyright 2000-2012 DebtPlus, L.L.C."
'{*******************************************************************}
'{                                                                   }
'{       DebtPlus Debt Management System                             }
'{                                                                   }
'{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
'{       ALL RIGHTS RESERVED                                         }
'{                                                                   }
'{   The entire contents of this file is protected by U.S. and       }
'{   International Copyright Laws. Unauthorized reproduction,        }
'{   reverse-engineering, and distribution of all or any portion of  }
'{   the code contained in this file is strictly prohibited and may  }
'{   result in severe civil and criminal penalties and will be       }
'{   prosecuted to the maximum extent possible under the law.        }
'{                                                                   }
'{   RESTRICTIONS                                                    }
'{                                                                   }
'{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
'{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
'{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
'{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
'{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
'{                                                                   }
'{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
'{   ADDITIONAL RESTRICTIONS.                                        }
'{                                                                   }
'{*******************************************************************}
#End Region

Imports DebtPlus.Interfaces.Creditor
Imports DebtPlus.Reports.Template
Imports DebtPlus.Interfaces.Reports
Imports DebtPlus.Reports
Imports System.Windows.Forms

Namespace Creditor.Billing

    Public Class CreditorInvoiceReport
        Inherits BaseXtraReportClass

        Public Sub New()
            MyBase.New()
            InitializeComponent()
            AddHandler XrSubreportInvoiceReport.BeforePrint, AddressOf XrSubreportInvoiceReport_BeforePrint

            ' Load the invoice report as the sub-report
            Try
                Dim InvoiceReport As New DebtPlus.Reports.Creditor.Invoice.CreditorInvoiceReport()
                InvoiceReport.RequestParameters = False
                For Each parm As DevExpress.XtraReports.Parameters.Parameter In InvoiceReport.Parameters
                    parm.Visible = False
                Next

                ' Suppress showing the page numbering. It is for the whole report and not a single Creditor Invoice
                Dim band As DevExpress.XtraReports.UI.PageFooterBand = TryCast(InvoiceReport.Bands("PageFooter"), DevExpress.XtraReports.UI.PageFooterBand)
                If band IsNot Nothing Then
                    Dim ctl As DevExpress.XtraReports.UI.XRControl = CType(InvoiceReport, DevExpress.XtraReports.UI.XtraReport).FindControl("XrPageInfo1", True)
                    If ctl IsNot Nothing Then
                        ctl.Visible = False
                    End If
                End If

                ' Finally, the report is the sub-report source
                XrSubreportInvoiceReport.ReportSource = CType(InvoiceReport, DevExpress.XtraReports.UI.XtraReport)

            Catch ex As System.Exception
                DebtPlus.Data.Forms.MessageBox.Show(String.Format("{0}: {3}{1}{1}{2}", "Error loading report", Environment.NewLine, ex.Message, "DebtPlus.Reports.Creditor.Invoice.dll"), "Failure to load invoice report", MessageBoxButtons.OK, MessageBoxIcon.Error)
            End Try
        End Sub

        Private Sub XrSubreportInvoiceReport_BeforePrint(ByVal sender As Object, ByVal e As System.Drawing.Printing.PrintEventArgs)
            With CType(sender, DevExpress.XtraReports.UI.XRSubreport)
                Dim creditor As String = Convert.ToString(GetCurrentColumnValue("creditor"))

                Dim rpt As DevExpress.XtraReports.UI.XtraReport = CType(.ReportSource, DevExpress.XtraReports.UI.XtraReport)
                Dim DebtPlusReport As DebtPlus.Interfaces.Reports.IReports = TryCast(rpt, DebtPlus.Interfaces.Reports.IReports)
                If DebtPlusReport IsNot Nothing Then
                    If TypeOf DebtPlusReport Is ICreditor Then
                        CType(DebtPlusReport, ICreditor).Creditor = creditor
                    Else
                        DebtPlusReport.SetReportParameter("Creditor", creditor)
                    End If
                Else
                    e.Cancel = True
                End If
            End With
        End Sub

        Public Overrides ReadOnly Property ReportTitle() As String
            Get
                Return "Creditor Invoices"
            End Get
        End Property

    End Class
End Namespace

