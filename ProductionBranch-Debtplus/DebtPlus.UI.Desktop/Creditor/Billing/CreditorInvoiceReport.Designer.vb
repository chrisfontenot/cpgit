Namespace Creditor.Billing

    <Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
    Partial Class CreditorInvoiceReport

        'XtraReport overrides dispose to clean up the component list.
        <System.Diagnostics.DebuggerNonUserCode()> _
        Protected Overrides Sub Dispose(ByVal disposing As Boolean)
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
            MyBase.Dispose(disposing)
        End Sub

        'Required by the Designer
        Private components As System.ComponentModel.IContainer

        'NOTE: The following procedure is required by the Designer
        'It can be modified using the Designer.  
        'Do not modify it using the code editor.
        <System.Diagnostics.DebuggerStepThrough()> _
        Private Sub InitializeComponent()
            Me.XrSubreportInvoiceReport = New DevExpress.XtraReports.UI.XRSubreport
            CType(Me, System.ComponentModel.ISupportInitialize).BeginInit()
            '
            'Detail
            '
            Me.Detail.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrSubreportInvoiceReport})
            Me.Detail.HeightF = 23.0!
            Me.Detail.Name = "Detail"
            Me.Detail.Padding = New DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100.0!)
            Me.Detail.PageBreak = DevExpress.XtraReports.UI.PageBreak.AfterBand
            Me.Detail.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft
            '
            'XrSubreportInvoiceReport
            '
            Me.XrSubreportInvoiceReport.LocationFloat = New DevExpress.Utils.PointFloat(0.0!, 0.0!)
            Me.XrSubreportInvoiceReport.Name = "XrSubreportInvoiceReport"
            Me.XrSubreportInvoiceReport.SizeF = New System.Drawing.SizeF(799.9999!, 23.0!)
            '
            'CreditorInvoiceReport
            '
            Me.Version = "10.2"
            CType(Me, System.ComponentModel.ISupportInitialize).EndInit()
        End Sub
        Friend WithEvents XrSubreportInvoiceReport As DevExpress.XtraReports.UI.XRSubreport
    End Class
End Namespace
