#Region "Copyright 2000-2012 DebtPlus, L.L.C."
'{*******************************************************************}
'{                                                                   }
'{       DebtPlus Debt Management System                             }
'{                                                                   }
'{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
'{       ALL RIGHTS RESERVED                                         }
'{                                                                   }
'{   The entire contents of this file is protected by U.S. and       }
'{   International Copyright Laws. Unauthorized reproduction,        }
'{   reverse-engineering, and distribution of all or any portion of  }
'{   the code contained in this file is strictly prohibited and may  }
'{   result in severe civil and criminal penalties and will be       }
'{   prosecuted to the maximum extent possible under the law.        }
'{                                                                   }
'{   RESTRICTIONS                                                    }
'{                                                                   }
'{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
'{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
'{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
'{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
'{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
'{                                                                   }
'{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
'{   ADDITIONAL RESTRICTIONS.                                        }
'{                                                                   }
'{*******************************************************************}
#End Region

Option Compare Binary
Option Explicit On 
Option Strict On

Imports DebtPlus.Utils
Imports System.Data.SqlClient
Imports System.Windows.Forms

Namespace Creditor.Billing.Payments

    Public Class Form_NonAR
        Inherits DebtPlus.Data.Forms.DebtPlusForm

        Private MaximumAvailable As Decimal = 0D

        Public Sub New()
            MyBase.New()
            InitializeComponent()
            AddHandler Me.Load, AddressOf Form_NonAR_Load
        End Sub

        Public Sub New(ByVal MaxFunds As Decimal)
            MyClass.New()
            MaximumAvailable = MaxFunds
        End Sub

#Region " Windows Form Designer generated code "

        'Form overrides dispose to clean up the component list.
        Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
            If disposing Then
                If components IsNot Nothing Then
                    components.Dispose()
                End If
            End If
            MyBase.Dispose(disposing)
        End Sub

        'Required by the Windows Form Designer
        Private components As System.ComponentModel.IContainer

        'NOTE: The following procedure is required by the Windows Form Designer
        'It can be modified using the Windows Form Designer.  
        'Do not modify it using the code editor.
        Friend WithEvents LabelControl1 As DevExpress.XtraEditors.LabelControl
        Friend WithEvents LabelControl2 As DevExpress.XtraEditors.LabelControl
        Friend WithEvents LabelControl3 As DevExpress.XtraEditors.LabelControl
        Friend WithEvents LabelControl4 As DevExpress.XtraEditors.LabelControl
        Friend WithEvents LabelControl5 As DevExpress.XtraEditors.LabelControl
        Friend WithEvents label_amt_available As DevExpress.XtraEditors.LabelControl
        Friend WithEvents txt_contribution As DevExpress.XtraEditors.CalcEdit
        Friend WithEvents MemoEdit1 As DevExpress.XtraEditors.MemoEdit
        Friend WithEvents Button_OK As DevExpress.XtraEditors.SimpleButton
        Friend WithEvents Button_Cancel As DevExpress.XtraEditors.SimpleButton
        Friend WithEvents lst_LedgerCode As DevExpress.XtraEditors.LookUpEdit
        <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
            Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(Form_NonAR))
            Me.LabelControl1 = New DevExpress.XtraEditors.LabelControl
            Me.LabelControl2 = New DevExpress.XtraEditors.LabelControl
            Me.LabelControl3 = New DevExpress.XtraEditors.LabelControl
            Me.LabelControl4 = New DevExpress.XtraEditors.LabelControl
            Me.LabelControl5 = New DevExpress.XtraEditors.LabelControl
            Me.label_amt_available = New DevExpress.XtraEditors.LabelControl
            Me.txt_contribution = New DevExpress.XtraEditors.CalcEdit
            Me.lst_LedgerCode = New DevExpress.XtraEditors.LookUpEdit
            Me.MemoEdit1 = New DevExpress.XtraEditors.MemoEdit
            Me.Button_OK = New DevExpress.XtraEditors.SimpleButton
            Me.Button_Cancel = New DevExpress.XtraEditors.SimpleButton
            CType(Me.DxErrorProvider1, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.txt_contribution.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.lst_LedgerCode.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.MemoEdit1.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
            Me.SuspendLayout()
            '
            'LabelControl1
            '
            Me.LabelControl1.Appearance.Options.UseTextOptions = True
            Me.LabelControl1.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center
            Me.LabelControl1.Appearance.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center
            Me.LabelControl1.Dock = System.Windows.Forms.DockStyle.Top
            Me.LabelControl1.Location = New System.Drawing.Point(0, 0)
            Me.LabelControl1.Name = "LabelControl1"
            Me.LabelControl1.Padding = New System.Windows.Forms.Padding(8)
            Me.LabelControl1.Size = New System.Drawing.Size(340, 29)
            Me.LabelControl1.TabIndex = 0
            Me.LabelControl1.Text = "Enter the information to make a contribution to the non-AR system."
            Me.LabelControl1.ToolTipController = Me.ToolTipController1
            '
            'LabelControl2
            '
            Me.LabelControl2.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.Horizontal
            Me.LabelControl2.Location = New System.Drawing.Point(8, 32)
            Me.LabelControl2.Name = "LabelControl2"
            Me.LabelControl2.Size = New System.Drawing.Size(83, 13)
            Me.LabelControl2.TabIndex = 1
            Me.LabelControl2.Text = "Amount Available"
            Me.LabelControl2.ToolTipController = Me.ToolTipController1
            '
            'LabelControl3
            '
            Me.LabelControl3.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.Horizontal
            Me.LabelControl3.Location = New System.Drawing.Point(8, 60)
            Me.LabelControl3.Name = "LabelControl3"
            Me.LabelControl3.Size = New System.Drawing.Size(106, 13)
            Me.LabelControl3.TabIndex = 3
            Me.LabelControl3.Text = "Amount To Contribute"
            Me.LabelControl3.ToolTipController = Me.ToolTipController1
            '
            'LabelControl4
            '
            Me.LabelControl4.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.Horizontal
            Me.LabelControl4.Location = New System.Drawing.Point(8, 92)
            Me.LabelControl4.Name = "LabelControl4"
            Me.LabelControl4.Size = New System.Drawing.Size(75, 13)
            Me.LabelControl4.TabIndex = 5
            Me.LabelControl4.Text = "Ledger Account"
            Me.LabelControl4.ToolTipController = Me.ToolTipController1
            '
            'LabelControl5
            '
            Me.LabelControl5.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.Horizontal
            Me.LabelControl5.Location = New System.Drawing.Point(8, 120)
            Me.LabelControl5.Name = "LabelControl5"
            Me.LabelControl5.Size = New System.Drawing.Size(36, 13)
            Me.LabelControl5.TabIndex = 7
            Me.LabelControl5.Text = "Reason"
            Me.LabelControl5.ToolTipController = Me.ToolTipController1
            '
            'label_amt_available
            '
            Me.label_amt_available.Appearance.Options.UseTextOptions = True
            Me.label_amt_available.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
            Me.label_amt_available.Location = New System.Drawing.Point(128, 32)
            Me.label_amt_available.Name = "label_amt_available"
            Me.label_amt_available.Size = New System.Drawing.Size(28, 13)
            Me.label_amt_available.TabIndex = 2
            Me.label_amt_available.Text = "$0.00"
            Me.label_amt_available.ToolTip = "This is the available (upper limit) that you may enter for the contribution amoun" & _
                "t."
            Me.label_amt_available.ToolTipController = Me.ToolTipController1
            '
            'txt_contribution
            '
            Me.txt_contribution.Location = New System.Drawing.Point(128, 56)
            Me.txt_contribution.Name = "txt_contribution"
            Me.txt_contribution.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
            Me.txt_contribution.Size = New System.Drawing.Size(93, 20)
            Me.txt_contribution.TabIndex = 4
            Me.txt_contribution.ToolTip = "Enter the dollar amount that you wish to accept. Valid entries are from $0.01 to " & _
                "the amount available (as shown above)"
            Me.txt_contribution.ToolTipController = Me.ToolTipController1
            '
            'lst_LedgerCode
            '
            Me.lst_LedgerCode.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                        Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
            Me.lst_LedgerCode.Location = New System.Drawing.Point(128, 88)
            Me.lst_LedgerCode.Name = "lst_LedgerCode"
            Me.lst_LedgerCode.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
            Me.lst_LedgerCode.Properties.NullText = "Please choose a ledger account..."
            Me.lst_LedgerCode.Size = New System.Drawing.Size(200, 20)
            Me.lst_LedgerCode.TabIndex = 6
            Me.lst_LedgerCode.ToolTip = "Choose the ledger account associated with the contribution from this list"
            Me.lst_LedgerCode.ToolTipController = Me.ToolTipController1
            '
            'MemoEdit1
            '
            Me.MemoEdit1.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                        Or System.Windows.Forms.AnchorStyles.Left) _
                        Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
            Me.MemoEdit1.Location = New System.Drawing.Point(128, 120)
            Me.MemoEdit1.Name = "MemoEdit1"
            Me.MemoEdit1.Size = New System.Drawing.Size(200, 96)
            Me.MemoEdit1.TabIndex = 8
            Me.MemoEdit1.ToolTip = "Enter a textual description here. You have 512 characters to make an entry"
            Me.MemoEdit1.ToolTipController = Me.ToolTipController1
            '
            'Button_OK
            '
            Me.Button_OK.Anchor = System.Windows.Forms.AnchorStyles.Bottom
            Me.Button_OK.DialogResult = System.Windows.Forms.DialogResult.OK
            Me.Button_OK.Location = New System.Drawing.Point(87, 232)
            Me.Button_OK.Name = "Button_OK"
            Me.Button_OK.Size = New System.Drawing.Size(75, 23)
            Me.Button_OK.TabIndex = 9
            Me.Button_OK.Text = "&OK"
            Me.Button_OK.ToolTip = "Click here to submit the changes to the database"
            Me.Button_OK.ToolTipController = Me.ToolTipController1
            '
            'Button_Cancel
            '
            Me.Button_Cancel.Anchor = System.Windows.Forms.AnchorStyles.Bottom
            Me.Button_Cancel.DialogResult = System.Windows.Forms.DialogResult.Cancel
            Me.Button_Cancel.Location = New System.Drawing.Point(191, 232)
            Me.Button_Cancel.Name = "Button_Cancel"
            Me.Button_Cancel.Size = New System.Drawing.Size(75, 23)
            Me.Button_Cancel.TabIndex = 10
            Me.Button_Cancel.Text = "&Cancel"
            Me.Button_Cancel.ToolTip = "Click here to cancel this form and return to the previous display"
            Me.Button_Cancel.ToolTipController = Me.ToolTipController1
            '
            'Form_NonAR
            '
            Me.AcceptButton = Me.Button_OK
            Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
            Me.CancelButton = Me.Button_Cancel
            Me.ClientSize = New System.Drawing.Size(354, 268)
            Me.Controls.Add(Me.Button_Cancel)
            Me.Controls.Add(Me.Button_OK)
            Me.Controls.Add(Me.MemoEdit1)
            Me.Controls.Add(Me.lst_LedgerCode)
            Me.Controls.Add(Me.txt_contribution)
            Me.Controls.Add(Me.label_amt_available)
            Me.Controls.Add(Me.LabelControl5)
            Me.Controls.Add(Me.LabelControl4)
            Me.Controls.Add(Me.LabelControl3)
            Me.Controls.Add(Me.LabelControl2)
            Me.Controls.Add(Me.LabelControl1)
            Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog
            Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
            Me.MaximizeBox = False
            Me.MaximumSize = New System.Drawing.Size(360, 300)
            Me.MinimumSize = New System.Drawing.Size(360, 300)
            Me.Name = "Form_NonAR"
            Me.ToolTipController1.SetSuperTip(Me, Nothing)
            Me.Text = "Non-A/R Creditor Contributions"
            CType(Me.DxErrorProvider1, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.txt_contribution.Properties, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.lst_LedgerCode.Properties, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.MemoEdit1.Properties, System.ComponentModel.ISupportInitialize).EndInit()
            Me.ResumeLayout(False)
            Me.PerformLayout()

        End Sub

#End Region

        Private Sub Form_NonAR_Load(ByVal sender As Object, ByVal e As System.EventArgs)

            ' Format the amount that we have to use
            label_amt_available.Text = System.String.Format("{0:c2}", MaximumAvailable)

            ' Load the list of ledger codes
            Load_LedgerCode()

            ' Define the contribution information
            txt_contribution.EditValue = 0D
            AddHandler txt_contribution.EditValueChanged, AddressOf Form_Changed

            ' Add the handler when the text value is changed
            AddHandler label_amt_available.TextChanged, AddressOf Form_Changed

            ' Complete the processing by enabling the OK button
            Enable_OK()
        End Sub

        Private Sub Form_Changed(ByVal sender As Object, ByVal e As System.EventArgs)
            Enable_OK()
        End Sub

        Private Sub Enable_OK()
            Button_OK.Enabled = True

            ' Check the range for the decimal input
            If Convert.ToDecimal(txt_contribution.EditValue) <= 0D OrElse Convert.ToDecimal(txt_contribution.EditValue) > MaximumAvailable Then
                DxErrorProvider1.SetError(txt_contribution, System.String.Format("The value is out of the allowed range. It must be between $0.01 and {0:c2}", MaximumAvailable))
                Button_OK.Enabled = False
            Else
                DxErrorProvider1.SetError(txt_contribution, "")
            End If

            ' There should be a ledger account
            If lst_LedgerCode.EditValue Is Nothing Then
                DxErrorProvider1.SetError(lst_LedgerCode, "A ledger account is required")
                Button_OK.Enabled = False
            Else
                DxErrorProvider1.SetError(lst_LedgerCode, "")
            End If

        End Sub

#Region " lst_ledger_code "
        Private Sub Load_LedgerCode()
            ' Load the source ledger code table
            Dim ledger_vue As System.Data.DataView = LedgerCodesTableView()
            With lst_LedgerCode
                With .Properties
                    .DataSource = ledger_vue
                    .PopulateColumns()
                    .Columns("Default").Visible = False         ' Do not display the default column
                    .Columns("ActiveFlag").Visible = False      ' Do not display the active flag
                    .DisplayMember = "Description"              ' Show the description
                    .ValueMember = "Ledger Code"                ' but the ledger code is what we want
                    .NullText = ""                              ' do not show anything if there is no item
                End With

                ' Try to select the default item
                Using vue As System.Data.DataView = New System.Data.DataView(ledger_vue.Table, "Default<>0", "", DataViewRowState.CurrentRows)
                    If vue.Count > 0 Then .EditValue = vue(0).Item("Ledger Code")
                End Using

                ' Bind the controls
                AddHandler .EditValueChanging, AddressOf lst_LedgerCode_EditValueChanging
                AddHandler .EditValueChanged, AddressOf Form_Changed
            End With
        End Sub

        Private Sub lst_LedgerCode_EditValueChanging(ByVal sender As Object, ByVal e As DevExpress.XtraEditors.Controls.ChangingEventArgs)

            ' If the new value is defined then make sure that the office is still active.
            With CType(sender, DevExpress.XtraEditors.LookUpEdit)
                If e.NewValue IsNot Nothing Then

                    ' Find the new office being selected
                    Dim IDKey As String = Convert.ToString(e.NewValue)
                    Dim drv As DataRowView = CType(.Properties.GetDataSourceRowByKeyValue(IDKey), System.Data.DataRowView)

                    ' From the office, ensure that the active flag is set
                    If Not Convert.ToBoolean(drv.Item("ActiveFlag")) Then
                        e.Cancel = True
                        DebtPlus.Data.Forms.MessageBox.Show("The ledger account is no longer active. Please do not use this account.", "Ledger code selection is not valid", MessageBoxButtons.OK, MessageBoxIcon.Hand, MessageBoxDefaultButton.Button1)
                    End If
                End If
            End With
        End Sub

        Private Function LedgerCodesTableView() As System.Data.DataView
            Dim answer As System.Data.DataView = Nothing
            Dim ds As New System.Data.DataSet("ledger_codes")

            Dim cn As SqlClient.SqlConnection = New SqlConnection(DebtPlus.LINQ.SQLInfoClass.getDefault().ConnectionString)
            Dim cmd As SqlClient.SqlCommand = New SqlCommand
            With cmd
                .Connection = cn
                .CommandText = "SELECT [ledger_code] as [Ledger Code],[description] as Description, 1 as [ActiveFlag], 0 as [Default] FROM ledger_codes WITH (NOLOCK) ORDER BY description"
            End With

            ' Fill the ledger codes
            Try
                Dim da As New SqlClient.SqlDataAdapter(cmd)
                da.Fill(ds, "ledger_codes")
                answer = ds.Tables(0).DefaultView

            Catch ex As SqlClient.SqlException
                DebtPlus.Data.Forms.MessageBox.Show(ex.Message, "Error loading ledger codes", MessageBoxButtons.OK, MessageBoxIcon.Error)

            Finally
                If cn IsNot Nothing Then cn.Dispose()
            End Try

            Return answer
        End Function
#End Region

    End Class

End Namespace
