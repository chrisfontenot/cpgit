#Region "Copyright 2000-2012 DebtPlus, L.L.C."
'{*******************************************************************}
'{                                                                   }
'{       DebtPlus Debt Management System                             }
'{                                                                   }
'{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
'{       ALL RIGHTS RESERVED                                         }
'{                                                                   }
'{   The entire contents of this file is protected by U.S. and       }
'{   International Copyright Laws. Unauthorized reproduction,        }
'{   reverse-engineering, and distribution of all or any portion of  }
'{   the code contained in this file is strictly prohibited and may  }
'{   result in severe civil and criminal penalties and will be       }
'{   prosecuted to the maximum extent possible under the law.        }
'{                                                                   }
'{   RESTRICTIONS                                                    }
'{                                                                   }
'{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
'{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
'{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
'{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
'{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
'{                                                                   }
'{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
'{   ADDITIONAL RESTRICTIONS.                                        }
'{                                                                   }
'{*******************************************************************}
#End Region

Option Compare Binary
Option Explicit On 
Option Strict On

Namespace Creditor.Billing.Payments

    Friend Class From_ItemEdit
        Inherits DebtPlus.Data.Forms.DebtPlusForm

        Private row As System.Data.DataRow = Nothing
        Private AvailableFunds As Decimal = 0D
        Private inv_amount As Decimal = 0D
        Private adj_amount As Decimal = 0D
        Private pmt_amount As Decimal = 0D

        Public Sub New(ByVal Available As Decimal, ByVal EditRow As System.Data.DataRow)
            MyClass.New()
            row = EditRow
            AvailableFunds = Available
        End Sub

        Public Sub New()
            MyBase.New()
            InitializeComponent()
            AddHandler Me.Load, AddressOf From_ItemEdit_Load
            AddHandler CalcEdit1.Validating, AddressOf DebtPlus.Data.Validation.NonNegative
            AddHandler CalcEdit2.Validating, AddressOf DebtPlus.Data.Validation.NonNegative
            AddHandler CalcEdit3.Validating, AddressOf DebtPlus.Data.Validation.NonNegative
            AddHandler CalcEdit4.Validating, AddressOf DebtPlus.Data.Validation.NonNegative
            AddHandler CalcEdit5.Validating, AddressOf DebtPlus.Data.Validation.NonNegative
            AddHandler CalcEdit6.Validating, AddressOf DebtPlus.Data.Validation.NonNegative
            AddHandler CalcEdit7.Validating, AddressOf DebtPlus.Data.Validation.NonNegative
        End Sub

#Region " Windows Form Designer generated code "

        'Form overrides dispose to clean up the component list.
        Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
            If disposing Then
                If components IsNot Nothing Then
                    components.Dispose()
                End If
            End If
            MyBase.Dispose(disposing)
        End Sub

        'Required by the Windows Form Designer
        Private components As System.ComponentModel.IContainer

        'NOTE: The following procedure is required by the Windows Form Designer
        'It can be modified using the Windows Form Designer.  
        'Do not modify it using the code editor.
        Friend WithEvents LabelControl1 As DevExpress.XtraEditors.LabelControl
        Friend WithEvents LabelControl2 As DevExpress.XtraEditors.LabelControl
        Friend WithEvents LabelControl3 As DevExpress.XtraEditors.LabelControl
        Friend WithEvents LabelControl4 As DevExpress.XtraEditors.LabelControl
        Friend WithEvents LabelControl5 As DevExpress.XtraEditors.LabelControl
        Friend WithEvents LabelControl6 As DevExpress.XtraEditors.LabelControl
        Friend WithEvents LabelControl7 As DevExpress.XtraEditors.LabelControl
        Friend WithEvents LabelControl8 As DevExpress.XtraEditors.LabelControl
        Friend WithEvents CalcEdit1 As DevExpress.XtraEditors.CalcEdit
        Friend WithEvents CalcEdit2 As DevExpress.XtraEditors.CalcEdit
        Friend WithEvents CalcEdit3 As DevExpress.XtraEditors.CalcEdit
        Friend WithEvents CalcEdit4 As DevExpress.XtraEditors.CalcEdit
        Friend WithEvents CalcEdit5 As DevExpress.XtraEditors.CalcEdit
        Friend WithEvents CalcEdit6 As DevExpress.XtraEditors.CalcEdit
        Friend WithEvents CalcEdit7 As DevExpress.XtraEditors.CalcEdit
        Friend WithEvents Button_OK As DevExpress.XtraEditors.SimpleButton
        Friend WithEvents Button_Cancel As DevExpress.XtraEditors.SimpleButton
        <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
            Me.LabelControl1 = New DevExpress.XtraEditors.LabelControl
            Me.LabelControl2 = New DevExpress.XtraEditors.LabelControl
            Me.LabelControl3 = New DevExpress.XtraEditors.LabelControl
            Me.LabelControl4 = New DevExpress.XtraEditors.LabelControl
            Me.LabelControl5 = New DevExpress.XtraEditors.LabelControl
            Me.LabelControl6 = New DevExpress.XtraEditors.LabelControl
            Me.LabelControl7 = New DevExpress.XtraEditors.LabelControl
            Me.LabelControl8 = New DevExpress.XtraEditors.LabelControl
            Me.Button_OK = New DevExpress.XtraEditors.SimpleButton
            Me.Button_Cancel = New DevExpress.XtraEditors.SimpleButton
            Me.CalcEdit1 = New DevExpress.XtraEditors.CalcEdit
            Me.CalcEdit2 = New DevExpress.XtraEditors.CalcEdit
            Me.CalcEdit3 = New DevExpress.XtraEditors.CalcEdit
            Me.CalcEdit4 = New DevExpress.XtraEditors.CalcEdit
            Me.CalcEdit5 = New DevExpress.XtraEditors.CalcEdit
            Me.CalcEdit6 = New DevExpress.XtraEditors.CalcEdit
            Me.CalcEdit7 = New DevExpress.XtraEditors.CalcEdit
            CType(Me.DxErrorProvider1, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.CalcEdit1.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.CalcEdit2.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.CalcEdit3.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.CalcEdit4.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.CalcEdit5.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.CalcEdit6.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.CalcEdit7.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
            Me.SuspendLayout()
            '
            'LabelControl1
            '
            Me.LabelControl1.Appearance.Options.UseTextOptions = True
            Me.LabelControl1.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
            Me.LabelControl1.Appearance.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Top
            Me.LabelControl1.Appearance.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap
            Me.LabelControl1.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None
            Me.LabelControl1.Location = New System.Drawing.Point(16, 12)
            Me.LabelControl1.Name = "LabelControl1"
            Me.LabelControl1.Size = New System.Drawing.Size(355, 38)
            Me.LabelControl1.TabIndex = 6
            Me.LabelControl1.ToolTipController = Me.ToolTipController1
            Me.LabelControl1.UseMnemonic = False
            '
            'LabelControl2
            '
            Me.LabelControl2.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.Horizontal
            Me.LabelControl2.Location = New System.Drawing.Point(8, 60)
            Me.LabelControl2.Name = "LabelControl2"
            Me.LabelControl2.Size = New System.Drawing.Size(114, 13)
            Me.LabelControl2.TabIndex = 7
            Me.LabelControl2.Text = "Original Invoice Amount"
            Me.LabelControl2.ToolTipController = Me.ToolTipController1
            '
            'LabelControl3
            '
            Me.LabelControl3.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.Horizontal
            Me.LabelControl3.Location = New System.Drawing.Point(8, 84)
            Me.LabelControl3.Name = "LabelControl3"
            Me.LabelControl3.Size = New System.Drawing.Size(107, 13)
            Me.LabelControl3.TabIndex = 9
            Me.LabelControl3.Text = "Less Amount Adjusted"
            Me.LabelControl3.ToolTipController = Me.ToolTipController1
            '
            'LabelControl4
            '
            Me.LabelControl4.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.Horizontal
            Me.LabelControl4.Location = New System.Drawing.Point(8, 108)
            Me.LabelControl4.Name = "LabelControl4"
            Me.LabelControl4.Size = New System.Drawing.Size(136, 13)
            Me.LabelControl4.TabIndex = 11
            Me.LabelControl4.Text = "Less Amount Previously Paid"
            Me.LabelControl4.ToolTipController = Me.ToolTipController1
            '
            'LabelControl5
            '
            Me.LabelControl5.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.Horizontal
            Me.LabelControl5.Location = New System.Drawing.Point(8, 140)
            Me.LabelControl5.Name = "LabelControl5"
            Me.LabelControl5.Size = New System.Drawing.Size(99, 13)
            Me.LabelControl5.TabIndex = 13
            Me.LabelControl5.Text = "Outstanding Balance"
            Me.LabelControl5.ToolTipController = Me.ToolTipController1
            '
            'LabelControl6
            '
            Me.LabelControl6.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.Horizontal
            Me.LabelControl6.Location = New System.Drawing.Point(8, 164)
            Me.LabelControl6.Name = "LabelControl6"
            Me.LabelControl6.Size = New System.Drawing.Size(138, 13)
            Me.LabelControl6.TabIndex = 0
            Me.LabelControl6.Text = "Less Amount to pay this time"
            Me.LabelControl6.ToolTipController = Me.ToolTipController1
            '
            'LabelControl7
            '
            Me.LabelControl7.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.Horizontal
            Me.LabelControl7.Location = New System.Drawing.Point(8, 188)
            Me.LabelControl7.Name = "LabelControl7"
            Me.LabelControl7.Size = New System.Drawing.Size(150, 13)
            Me.LabelControl7.TabIndex = 2
            Me.LabelControl7.Text = "Less Amount to adjust this time"
            Me.LabelControl7.ToolTipController = Me.ToolTipController1
            '
            'LabelControl8
            '
            Me.LabelControl8.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.Horizontal
            Me.LabelControl8.Location = New System.Drawing.Point(8, 228)
            Me.LabelControl8.Name = "LabelControl8"
            Me.LabelControl8.Size = New System.Drawing.Size(129, 13)
            Me.LabelControl8.TabIndex = 15
            Me.LabelControl8.Text = "Yields New Invoice Balance"
            Me.LabelControl8.ToolTipController = Me.ToolTipController1
            '
            'Button_OK
            '
            Me.Button_OK.DialogResult = System.Windows.Forms.DialogResult.OK
            Me.Button_OK.Location = New System.Drawing.Point(296, 56)
            Me.Button_OK.Name = "Button_OK"
            Me.Button_OK.Size = New System.Drawing.Size(75, 23)
            Me.Button_OK.TabIndex = 4
            Me.Button_OK.Text = "&OK"
            Me.Button_OK.ToolTipController = Me.ToolTipController1
            '
            'Button_Cancel
            '
            Me.Button_Cancel.DialogResult = System.Windows.Forms.DialogResult.Cancel
            Me.Button_Cancel.Location = New System.Drawing.Point(296, 88)
            Me.Button_Cancel.Name = "Button_Cancel"
            Me.Button_Cancel.Size = New System.Drawing.Size(75, 23)
            Me.Button_Cancel.TabIndex = 5
            Me.Button_Cancel.Text = "&Cancel"
            Me.Button_Cancel.ToolTipController = Me.ToolTipController1
            '
            'CalcEdit1
            '
            Me.CalcEdit1.Enabled = False
            Me.CalcEdit1.Location = New System.Drawing.Point(184, 56)
            Me.CalcEdit1.Name = "CalcEdit1"
            Me.CalcEdit1.Properties.Appearance.BackColor = System.Drawing.SystemColors.InactiveBorder
            Me.CalcEdit1.Properties.Appearance.BackColor2 = System.Drawing.SystemColors.InactiveBorder
            Me.CalcEdit1.Properties.Appearance.BorderColor = System.Drawing.SystemColors.InactiveBorder
            Me.CalcEdit1.Properties.Appearance.ForeColor = System.Drawing.Color.Black
            Me.CalcEdit1.Properties.Appearance.Options.UseBackColor = True
            Me.CalcEdit1.Properties.Appearance.Options.UseBorderColor = True
            Me.CalcEdit1.Properties.Appearance.Options.UseForeColor = True
            Me.CalcEdit1.Properties.Appearance.Options.UseTextOptions = True
            Me.CalcEdit1.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
            Me.CalcEdit1.Properties.AppearanceDisabled.BackColor = System.Drawing.SystemColors.InactiveBorder
            Me.CalcEdit1.Properties.AppearanceDisabled.BackColor2 = System.Drawing.SystemColors.InactiveBorder
            Me.CalcEdit1.Properties.AppearanceDisabled.BorderColor = System.Drawing.SystemColors.InactiveBorder
            Me.CalcEdit1.Properties.AppearanceDisabled.ForeColor = System.Drawing.Color.Black
            Me.CalcEdit1.Properties.AppearanceDisabled.Options.UseBackColor = True
            Me.CalcEdit1.Properties.AppearanceDisabled.Options.UseBorderColor = True
            Me.CalcEdit1.Properties.AppearanceDisabled.Options.UseForeColor = True
            Me.CalcEdit1.Properties.AppearanceDisabled.Options.UseTextOptions = True
            Me.CalcEdit1.Properties.AppearanceDisabled.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
            Me.CalcEdit1.Properties.AppearanceDropDown.Options.UseTextOptions = True
            Me.CalcEdit1.Properties.AppearanceDropDown.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
            Me.CalcEdit1.Properties.AppearanceFocused.Options.UseTextOptions = True
            Me.CalcEdit1.Properties.AppearanceFocused.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
            Me.CalcEdit1.Properties.AppearanceReadOnly.Options.UseTextOptions = True
            Me.CalcEdit1.Properties.AppearanceReadOnly.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
            Me.CalcEdit1.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
            Me.CalcEdit1.Properties.DisplayFormat.FormatString = "c2"
            Me.CalcEdit1.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
            Me.CalcEdit1.Properties.EditFormat.FormatString = "f2"
            Me.CalcEdit1.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric
            Me.CalcEdit1.Properties.Precision = 2
            Me.CalcEdit1.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor
            Me.CalcEdit1.Size = New System.Drawing.Size(100, 20)
            Me.CalcEdit1.TabIndex = 8
            Me.CalcEdit1.ToolTipController = Me.ToolTipController1
            '
            'CalcEdit2
            '
            Me.CalcEdit2.Enabled = False
            Me.CalcEdit2.Location = New System.Drawing.Point(184, 80)
            Me.CalcEdit2.Name = "CalcEdit2"
            Me.CalcEdit2.Properties.Appearance.BackColor = System.Drawing.SystemColors.InactiveBorder
            Me.CalcEdit2.Properties.Appearance.BackColor2 = System.Drawing.SystemColors.InactiveBorder
            Me.CalcEdit2.Properties.Appearance.BorderColor = System.Drawing.SystemColors.InactiveBorder
            Me.CalcEdit2.Properties.Appearance.ForeColor = System.Drawing.Color.Black
            Me.CalcEdit2.Properties.Appearance.Options.UseBackColor = True
            Me.CalcEdit2.Properties.Appearance.Options.UseBorderColor = True
            Me.CalcEdit2.Properties.Appearance.Options.UseForeColor = True
            Me.CalcEdit2.Properties.Appearance.Options.UseTextOptions = True
            Me.CalcEdit2.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
            Me.CalcEdit2.Properties.AppearanceDisabled.BackColor = System.Drawing.SystemColors.InactiveBorder
            Me.CalcEdit2.Properties.AppearanceDisabled.BackColor2 = System.Drawing.SystemColors.InactiveBorder
            Me.CalcEdit2.Properties.AppearanceDisabled.BorderColor = System.Drawing.SystemColors.InactiveBorder
            Me.CalcEdit2.Properties.AppearanceDisabled.ForeColor = System.Drawing.Color.Black
            Me.CalcEdit2.Properties.AppearanceDisabled.Options.UseBackColor = True
            Me.CalcEdit2.Properties.AppearanceDisabled.Options.UseBorderColor = True
            Me.CalcEdit2.Properties.AppearanceDisabled.Options.UseForeColor = True
            Me.CalcEdit2.Properties.AppearanceDisabled.Options.UseTextOptions = True
            Me.CalcEdit2.Properties.AppearanceDisabled.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
            Me.CalcEdit2.Properties.AppearanceDropDown.Options.UseTextOptions = True
            Me.CalcEdit2.Properties.AppearanceDropDown.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
            Me.CalcEdit2.Properties.AppearanceFocused.Options.UseTextOptions = True
            Me.CalcEdit2.Properties.AppearanceFocused.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
            Me.CalcEdit2.Properties.AppearanceReadOnly.Options.UseTextOptions = True
            Me.CalcEdit2.Properties.AppearanceReadOnly.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
            Me.CalcEdit2.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
            Me.CalcEdit2.Properties.DisplayFormat.FormatString = "c2"
            Me.CalcEdit2.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
            Me.CalcEdit2.Properties.EditFormat.FormatString = "f2"
            Me.CalcEdit2.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric
            Me.CalcEdit2.Properties.Precision = 2
            Me.CalcEdit2.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor
            Me.CalcEdit2.Size = New System.Drawing.Size(100, 20)
            Me.CalcEdit2.TabIndex = 10
            Me.CalcEdit2.ToolTipController = Me.ToolTipController1
            '
            'CalcEdit3
            '
            Me.CalcEdit3.Enabled = False
            Me.CalcEdit3.Location = New System.Drawing.Point(184, 104)
            Me.CalcEdit3.Name = "CalcEdit3"
            Me.CalcEdit3.Properties.Appearance.BackColor = System.Drawing.SystemColors.InactiveBorder
            Me.CalcEdit3.Properties.Appearance.BackColor2 = System.Drawing.SystemColors.InactiveBorder
            Me.CalcEdit3.Properties.Appearance.BorderColor = System.Drawing.SystemColors.InactiveBorder
            Me.CalcEdit3.Properties.Appearance.ForeColor = System.Drawing.Color.Black
            Me.CalcEdit3.Properties.Appearance.Options.UseBackColor = True
            Me.CalcEdit3.Properties.Appearance.Options.UseBorderColor = True
            Me.CalcEdit3.Properties.Appearance.Options.UseForeColor = True
            Me.CalcEdit3.Properties.Appearance.Options.UseTextOptions = True
            Me.CalcEdit3.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
            Me.CalcEdit3.Properties.AppearanceDisabled.BackColor = System.Drawing.SystemColors.InactiveBorder
            Me.CalcEdit3.Properties.AppearanceDisabled.BackColor2 = System.Drawing.SystemColors.InactiveBorder
            Me.CalcEdit3.Properties.AppearanceDisabled.BorderColor = System.Drawing.SystemColors.InactiveBorder
            Me.CalcEdit3.Properties.AppearanceDisabled.ForeColor = System.Drawing.Color.Black
            Me.CalcEdit3.Properties.AppearanceDisabled.Options.UseBackColor = True
            Me.CalcEdit3.Properties.AppearanceDisabled.Options.UseBorderColor = True
            Me.CalcEdit3.Properties.AppearanceDisabled.Options.UseForeColor = True
            Me.CalcEdit3.Properties.AppearanceDisabled.Options.UseTextOptions = True
            Me.CalcEdit3.Properties.AppearanceDisabled.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
            Me.CalcEdit3.Properties.AppearanceDropDown.Options.UseTextOptions = True
            Me.CalcEdit3.Properties.AppearanceDropDown.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
            Me.CalcEdit3.Properties.AppearanceFocused.Options.UseTextOptions = True
            Me.CalcEdit3.Properties.AppearanceFocused.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
            Me.CalcEdit3.Properties.AppearanceReadOnly.Options.UseTextOptions = True
            Me.CalcEdit3.Properties.AppearanceReadOnly.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
            Me.CalcEdit3.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
            Me.CalcEdit3.Properties.DisplayFormat.FormatString = "c2"
            Me.CalcEdit3.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
            Me.CalcEdit3.Properties.EditFormat.FormatString = "f2"
            Me.CalcEdit3.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric
            Me.CalcEdit3.Properties.Precision = 2
            Me.CalcEdit3.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor
            Me.CalcEdit3.Size = New System.Drawing.Size(100, 20)
            Me.CalcEdit3.TabIndex = 12
            Me.CalcEdit3.ToolTipController = Me.ToolTipController1
            '
            'CalcEdit4
            '
            Me.CalcEdit4.Enabled = False
            Me.CalcEdit4.Location = New System.Drawing.Point(184, 136)
            Me.CalcEdit4.Name = "CalcEdit4"
            Me.CalcEdit4.Properties.Appearance.BackColor = System.Drawing.SystemColors.InactiveBorder
            Me.CalcEdit4.Properties.Appearance.BackColor2 = System.Drawing.SystemColors.InactiveBorder
            Me.CalcEdit4.Properties.Appearance.BorderColor = System.Drawing.SystemColors.InactiveBorder
            Me.CalcEdit4.Properties.Appearance.ForeColor = System.Drawing.Color.Black
            Me.CalcEdit4.Properties.Appearance.Options.UseBackColor = True
            Me.CalcEdit4.Properties.Appearance.Options.UseBorderColor = True
            Me.CalcEdit4.Properties.Appearance.Options.UseForeColor = True
            Me.CalcEdit4.Properties.Appearance.Options.UseTextOptions = True
            Me.CalcEdit4.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
            Me.CalcEdit4.Properties.AppearanceDisabled.BackColor = System.Drawing.SystemColors.InactiveBorder
            Me.CalcEdit4.Properties.AppearanceDisabled.BackColor2 = System.Drawing.SystemColors.InactiveBorder
            Me.CalcEdit4.Properties.AppearanceDisabled.BorderColor = System.Drawing.SystemColors.InactiveBorder
            Me.CalcEdit4.Properties.AppearanceDisabled.ForeColor = System.Drawing.Color.Black
            Me.CalcEdit4.Properties.AppearanceDisabled.Options.UseBackColor = True
            Me.CalcEdit4.Properties.AppearanceDisabled.Options.UseBorderColor = True
            Me.CalcEdit4.Properties.AppearanceDisabled.Options.UseForeColor = True
            Me.CalcEdit4.Properties.AppearanceDisabled.Options.UseTextOptions = True
            Me.CalcEdit4.Properties.AppearanceDisabled.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
            Me.CalcEdit4.Properties.AppearanceDropDown.Options.UseTextOptions = True
            Me.CalcEdit4.Properties.AppearanceDropDown.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
            Me.CalcEdit4.Properties.AppearanceFocused.Options.UseTextOptions = True
            Me.CalcEdit4.Properties.AppearanceFocused.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
            Me.CalcEdit4.Properties.AppearanceReadOnly.Options.UseTextOptions = True
            Me.CalcEdit4.Properties.AppearanceReadOnly.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
            Me.CalcEdit4.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
            Me.CalcEdit4.Properties.DisplayFormat.FormatString = "c2"
            Me.CalcEdit4.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
            Me.CalcEdit4.Properties.EditFormat.FormatString = "f2"
            Me.CalcEdit4.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric
            Me.CalcEdit4.Properties.Precision = 2
            Me.CalcEdit4.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor
            Me.CalcEdit4.Size = New System.Drawing.Size(100, 20)
            Me.CalcEdit4.TabIndex = 14
            Me.CalcEdit4.ToolTipController = Me.ToolTipController1
            '
            'CalcEdit5
            '
            Me.CalcEdit5.Location = New System.Drawing.Point(184, 160)
            Me.CalcEdit5.Name = "CalcEdit5"
            Me.CalcEdit5.Properties.Appearance.Options.UseTextOptions = True
            Me.CalcEdit5.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
            Me.CalcEdit5.Properties.AppearanceDisabled.Options.UseTextOptions = True
            Me.CalcEdit5.Properties.AppearanceDisabled.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
            Me.CalcEdit5.Properties.AppearanceDropDown.Options.UseTextOptions = True
            Me.CalcEdit5.Properties.AppearanceDropDown.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
            Me.CalcEdit5.Properties.AppearanceFocused.Options.UseTextOptions = True
            Me.CalcEdit5.Properties.AppearanceFocused.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
            Me.CalcEdit5.Properties.AppearanceReadOnly.Options.UseTextOptions = True
            Me.CalcEdit5.Properties.AppearanceReadOnly.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
            Me.CalcEdit5.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
            Me.CalcEdit5.Properties.DisplayFormat.FormatString = "c2"
            Me.CalcEdit5.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
            Me.CalcEdit5.Properties.EditFormat.FormatString = "f2"
            Me.CalcEdit5.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric
            Me.CalcEdit5.Properties.Precision = 2
            Me.CalcEdit5.Size = New System.Drawing.Size(100, 20)
            Me.CalcEdit5.TabIndex = 1
            Me.CalcEdit5.ToolTipController = Me.ToolTipController1
            '
            'CalcEdit6
            '
            Me.CalcEdit6.Location = New System.Drawing.Point(184, 184)
            Me.CalcEdit6.Name = "CalcEdit6"
            Me.CalcEdit6.Properties.Appearance.Options.UseTextOptions = True
            Me.CalcEdit6.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
            Me.CalcEdit6.Properties.AppearanceDisabled.Options.UseTextOptions = True
            Me.CalcEdit6.Properties.AppearanceDisabled.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
            Me.CalcEdit6.Properties.AppearanceDropDown.Options.UseTextOptions = True
            Me.CalcEdit6.Properties.AppearanceDropDown.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
            Me.CalcEdit6.Properties.AppearanceFocused.Options.UseTextOptions = True
            Me.CalcEdit6.Properties.AppearanceFocused.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
            Me.CalcEdit6.Properties.AppearanceReadOnly.Options.UseTextOptions = True
            Me.CalcEdit6.Properties.AppearanceReadOnly.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
            Me.CalcEdit6.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
            Me.CalcEdit6.Properties.DisplayFormat.FormatString = "c2"
            Me.CalcEdit6.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
            Me.CalcEdit6.Properties.EditFormat.FormatString = "f2"
            Me.CalcEdit6.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric
            Me.CalcEdit6.Properties.Precision = 2
            Me.CalcEdit6.Size = New System.Drawing.Size(100, 20)
            Me.CalcEdit6.TabIndex = 3
            Me.CalcEdit6.ToolTipController = Me.ToolTipController1
            '
            'CalcEdit7
            '
            Me.CalcEdit7.Enabled = False
            Me.CalcEdit7.Location = New System.Drawing.Point(184, 224)
            Me.CalcEdit7.Name = "CalcEdit7"
            Me.CalcEdit7.Properties.Appearance.BackColor = System.Drawing.SystemColors.InactiveBorder
            Me.CalcEdit7.Properties.Appearance.BackColor2 = System.Drawing.SystemColors.InactiveBorder
            Me.CalcEdit7.Properties.Appearance.BorderColor = System.Drawing.SystemColors.InactiveBorder
            Me.CalcEdit7.Properties.Appearance.ForeColor = System.Drawing.Color.Black
            Me.CalcEdit7.Properties.Appearance.Options.UseBackColor = True
            Me.CalcEdit7.Properties.Appearance.Options.UseBorderColor = True
            Me.CalcEdit7.Properties.Appearance.Options.UseForeColor = True
            Me.CalcEdit7.Properties.Appearance.Options.UseTextOptions = True
            Me.CalcEdit7.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
            Me.CalcEdit7.Properties.AppearanceDisabled.BackColor = System.Drawing.SystemColors.InactiveBorder
            Me.CalcEdit7.Properties.AppearanceDisabled.BackColor2 = System.Drawing.SystemColors.InactiveBorder
            Me.CalcEdit7.Properties.AppearanceDisabled.BorderColor = System.Drawing.SystemColors.InactiveBorder
            Me.CalcEdit7.Properties.AppearanceDisabled.ForeColor = System.Drawing.Color.Black
            Me.CalcEdit7.Properties.AppearanceDisabled.Options.UseBackColor = True
            Me.CalcEdit7.Properties.AppearanceDisabled.Options.UseBorderColor = True
            Me.CalcEdit7.Properties.AppearanceDisabled.Options.UseForeColor = True
            Me.CalcEdit7.Properties.AppearanceDisabled.Options.UseTextOptions = True
            Me.CalcEdit7.Properties.AppearanceDisabled.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
            Me.CalcEdit7.Properties.AppearanceDropDown.Options.UseTextOptions = True
            Me.CalcEdit7.Properties.AppearanceDropDown.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
            Me.CalcEdit7.Properties.AppearanceFocused.Options.UseTextOptions = True
            Me.CalcEdit7.Properties.AppearanceFocused.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
            Me.CalcEdit7.Properties.AppearanceReadOnly.Options.UseTextOptions = True
            Me.CalcEdit7.Properties.AppearanceReadOnly.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
            Me.CalcEdit7.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
            Me.CalcEdit7.Properties.DisplayFormat.FormatString = "c2"
            Me.CalcEdit7.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
            Me.CalcEdit7.Properties.EditFormat.FormatString = "f2"
            Me.CalcEdit7.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric
            Me.CalcEdit7.Properties.Precision = 2
            Me.CalcEdit7.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor
            Me.CalcEdit7.Size = New System.Drawing.Size(100, 20)
            Me.CalcEdit7.TabIndex = 16
            Me.CalcEdit7.ToolTipController = Me.ToolTipController1
            '
            'From_ItemEdit
            '
            Me.AcceptButton = Me.Button_OK
            Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
            Me.CancelButton = Me.Button_Cancel
            Me.ClientSize = New System.Drawing.Size(384, 262)
            Me.Controls.Add(Me.CalcEdit7)
            Me.Controls.Add(Me.CalcEdit6)
            Me.Controls.Add(Me.CalcEdit5)
            Me.Controls.Add(Me.CalcEdit4)
            Me.Controls.Add(Me.CalcEdit3)
            Me.Controls.Add(Me.CalcEdit2)
            Me.Controls.Add(Me.CalcEdit1)
            Me.Controls.Add(Me.Button_Cancel)
            Me.Controls.Add(Me.Button_OK)
            Me.Controls.Add(Me.LabelControl8)
            Me.Controls.Add(Me.LabelControl7)
            Me.Controls.Add(Me.LabelControl6)
            Me.Controls.Add(Me.LabelControl5)
            Me.Controls.Add(Me.LabelControl4)
            Me.Controls.Add(Me.LabelControl3)
            Me.Controls.Add(Me.LabelControl2)
            Me.Controls.Add(Me.LabelControl1)
            Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog
            Me.MaximizeBox = False
            Me.Name = "From_ItemEdit"
            Me.ToolTipController1.SetSuperTip(Me, Nothing)
            Me.Text = "Invoice Information"
            CType(Me.DxErrorProvider1, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.CalcEdit1.Properties, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.CalcEdit2.Properties, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.CalcEdit3.Properties, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.CalcEdit4.Properties, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.CalcEdit5.Properties, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.CalcEdit6.Properties, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.CalcEdit7.Properties, System.ComponentModel.ISupportInitialize).EndInit()
            Me.ResumeLayout(False)
            Me.PerformLayout()

        End Sub

#End Region

        Private Sub From_ItemEdit_Load(ByVal sender As Object, ByVal e As System.EventArgs)

            ' IF there is an input row then obtain the values from the row
            If row IsNot Nothing Then
                If row("inv_amount") IsNot System.DBNull.Value Then inv_amount = Convert.ToDecimal(row("inv_amount"))
                If row("pmt_amount") IsNot System.DBNull.Value Then pmt_amount = Convert.ToDecimal(row("pmt_amount"))
                If row("adj_amount") IsNot System.DBNull.Value Then adj_amount = Convert.ToDecimal(row("adj_amount"))
            End If

            ' Generate the information for the display.
            CalcEdit1.EditValue = inv_amount
            CalcEdit2.EditValue = adj_amount
            CalcEdit3.EditValue = pmt_amount
            CalcEdit4.EditValue = inv_amount - adj_amount - pmt_amount
            CalcEdit5.EditValue = System.Math.Min(AvailableFunds, inv_amount - adj_amount - pmt_amount)
            CalcEdit6.EditValue = 0D
            CalcEdit7.EditValue = inv_amount - adj_amount - pmt_amount - Convert.ToDecimal(CalcEdit5.EditValue)

            ' Register the two event handlers
            AddHandler CalcEdit5.Validated, AddressOf Form_Changed
            AddHandler CalcEdit6.Validated, AddressOf Form_Changed

            ' Change the display text according the money available
            If AvailableFunds <= 0D Then
                LabelControl1.Text = "There is no money to pay this invoice. However, you are permitted to adjust the invoice. Or you may click on the cancel button to return to the previous form."
                CalcEdit5.Enabled = False
            Else
                LabelControl1.Text = System.String.Format("There is {0:c} to pay this invoice. Either pay some (or all) of this and/or adjust the invoice.", AvailableFunds)
            End If

            ' Set the OK button status
            EnableOK()
        End Sub

        Private Sub Form_Changed(ByVal Sender As Object, ByVal e As System.EventArgs)
            EnableOK()
        End Sub

        Private Sub EnableOK()
            ' Update the information on the display and enable the OK button if possible.
            CalcEdit7.EditValue = inv_amount - adj_amount - pmt_amount - Convert.ToDecimal(CalcEdit5.EditValue) - Convert.ToDecimal(CalcEdit6.EditValue)
            Button_OK.Enabled = (Convert.ToDecimal(CalcEdit5.EditValue) > 0D) OrElse (Convert.ToDecimal(CalcEdit6.EditValue) > 0D)

            ' DO not allow the invoice to be paid with funds more than available in the check.
            If Convert.ToDecimal(CalcEdit5.EditValue) > AvailableFunds AndAlso AvailableFunds > 0 Then
                DxErrorProvider1.SetError(CalcEdit5, System.String.Format("You have only {0:c2} to use on this invoice. Please limit it to that amount.", AvailableFunds))
                Button_OK.Enabled = False
            Else
                DxErrorProvider1.SetError(CalcEdit5, System.String.Empty)
            End If

            ' Do not allow the invoice to go negative.
            If Convert.ToDecimal(CalcEdit7.EditValue) < 0D Then Button_OK.Enabled = False
        End Sub
    End Class
End Namespace
