#Region "Copyright 2000-2012 DebtPlus, L.L.C."
'{*******************************************************************}
'{                                                                   }
'{       DebtPlus Debt Management System                             }
'{                                                                   }
'{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
'{       ALL RIGHTS RESERVED                                         }
'{                                                                   }
'{   The entire contents of this file is protected by U.S. and       }
'{   International Copyright Laws. Unauthorized reproduction,        }
'{   reverse-engineering, and distribution of all or any portion of  }
'{   the code contained in this file is strictly prohibited and may  }
'{   result in severe civil and criminal penalties and will be       }
'{   prosecuted to the maximum extent possible under the law.        }
'{                                                                   }
'{   RESTRICTIONS                                                    }
'{                                                                   }
'{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
'{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
'{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
'{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
'{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
'{                                                                   }
'{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
'{   ADDITIONAL RESTRICTIONS.                                        }
'{                                                                   }
'{*******************************************************************}
#End Region

Option Compare Binary
Option Explicit On 
Option Strict On

Namespace Creditor.Billing.Payments

    Public Class Form_PaidInFull
        Inherits DebtPlus.Data.Forms.DebtPlusForm
        Private MaximumAvailable As Decimal = 0D

        Public Sub New()
            MyBase.New()
            InitializeComponent()
            AddHandler CalcEdit1.EditValueChanging, AddressOf CalcEdit1_EditValueChanging
            AddHandler Load, AddressOf Form_PaidInFull_Load
        End Sub

        Public Sub New(ByVal MaxFunds As Decimal)
            MyClass.New()
            MaximumAvailable = MaxFunds
        End Sub

#Region " Windows Form Designer generated code "

        'Form overrides dispose to clean up the component list.
        Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
            If disposing Then
                If components IsNot Nothing Then
                    components.Dispose()
                End If
            End If
            MyBase.Dispose(disposing)
        End Sub

        'Required by the Windows Form Designer
        Private components As System.ComponentModel.IContainer

        'NOTE: The following procedure is required by the Windows Form Designer
        'It can be modified using the Windows Form Designer.  
        'Do not modify it using the code editor.
        Friend WithEvents label_header As DevExpress.XtraEditors.LabelControl
        Friend WithEvents LabelControl2 As DevExpress.XtraEditors.LabelControl
        Friend WithEvents CalcEdit1 As DevExpress.XtraEditors.CalcEdit
        Friend WithEvents Button_OK As DevExpress.XtraEditors.SimpleButton
        Friend WithEvents Button_Cancel As DevExpress.XtraEditors.SimpleButton
        <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
            Me.label_header = New DevExpress.XtraEditors.LabelControl
            Me.LabelControl2 = New DevExpress.XtraEditors.LabelControl
            Me.CalcEdit1 = New DevExpress.XtraEditors.CalcEdit
            Me.Button_OK = New DevExpress.XtraEditors.SimpleButton
            Me.Button_Cancel = New DevExpress.XtraEditors.SimpleButton
            CType(Me.DxErrorProvider1, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.CalcEdit1.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
            Me.SuspendLayout()
            '
            'label_header
            '
            Me.label_header.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
            Me.label_header.Appearance.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Top
            Me.label_header.Appearance.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap
            Me.label_header.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None
            Me.label_header.Location = New System.Drawing.Point(12, 12)
            Me.label_header.Name = "label_header"
            Me.label_header.Size = New System.Drawing.Size(274, 40)
            Me.label_header.TabIndex = 0
            Me.label_header.ToolTipController = Me.ToolTipController1
            '
            'LabelControl2
            '
            Me.LabelControl2.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
            Me.LabelControl2.Appearance.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Top
            Me.LabelControl2.Appearance.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap
            Me.LabelControl2.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None
            Me.LabelControl2.Location = New System.Drawing.Point(8, 61)
            Me.LabelControl2.Name = "LabelControl2"
            Me.LabelControl2.Size = New System.Drawing.Size(149, 31)
            Me.LabelControl2.TabIndex = 1
            Me.LabelControl2.Text = "What is the amount that you wish to pay?"
            Me.LabelControl2.ToolTipController = Me.ToolTipController1
            '
            'CalcEdit1
            '
            Me.CalcEdit1.Location = New System.Drawing.Point(163, 58)
            Me.CalcEdit1.Name = "CalcEdit1"
            Me.CalcEdit1.Properties.Appearance.Options.UseTextOptions = True
            Me.CalcEdit1.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
            Me.CalcEdit1.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
            Me.CalcEdit1.Properties.DisplayFormat.FormatString = "c2"
            Me.CalcEdit1.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
            Me.CalcEdit1.Properties.EditFormat.FormatString = "d2"
            Me.CalcEdit1.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric
            Me.CalcEdit1.Properties.Precision = 2
            Me.CalcEdit1.Size = New System.Drawing.Size(100, 20)
            Me.CalcEdit1.TabIndex = 2
            Me.CalcEdit1.ToolTipController = Me.ToolTipController1
            '
            'Button_OK
            '
            Me.Button_OK.DialogResult = System.Windows.Forms.DialogResult.OK
            Me.Button_OK.Location = New System.Drawing.Point(60, 106)
            Me.Button_OK.Name = "Button_OK"
            Me.Button_OK.Size = New System.Drawing.Size(75, 23)
            Me.Button_OK.TabIndex = 3
            Me.Button_OK.Text = "&OK"
            Me.Button_OK.ToolTipController = Me.ToolTipController1
            '
            'Button_Cancel
            '
            Me.Button_Cancel.DialogResult = System.Windows.Forms.DialogResult.Cancel
            Me.Button_Cancel.Location = New System.Drawing.Point(163, 106)
            Me.Button_Cancel.Name = "Button_Cancel"
            Me.Button_Cancel.Size = New System.Drawing.Size(75, 23)
            Me.Button_Cancel.TabIndex = 4
            Me.Button_Cancel.Text = "&Cancel"
            Me.Button_Cancel.ToolTipController = Me.ToolTipController1
            '
            'Form_PaidInFull
            '
            Me.AcceptButton = Me.Button_OK
            Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
            Me.CancelButton = Me.Button_Cancel
            Me.ClientSize = New System.Drawing.Size(294, 141)
            Me.Controls.Add(Me.Button_Cancel)
            Me.Controls.Add(Me.Button_OK)
            Me.Controls.Add(Me.CalcEdit1)
            Me.Controls.Add(Me.LabelControl2)
            Me.Controls.Add(Me.label_header)
            Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog
            Me.LookAndFeel.UseDefaultLookAndFeel = True
            Me.MaximizeBox = False
            Me.Name = "Form_PaidInFull"
            Me.Text = "Pay Oldest Invoices First"
            CType(Me.DxErrorProvider1, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.CalcEdit1.Properties, System.ComponentModel.ISupportInitialize).EndInit()
            Me.ResumeLayout(False)

        End Sub

#End Region

        Private Sub CalcEdit1_EditValueChanging(ByVal sender As Object, ByVal e As DevExpress.XtraEditors.Controls.ChangingEventArgs)
            With CalcEdit1
                Button_OK.Enabled = Convert.ToDecimal(.EditValue) >= 0D And Convert.ToDecimal(.EditValue) <= MaximumAvailable
            End With
        End Sub

        Private Sub Form_PaidInFull_Load(ByVal sender As Object, ByVal e As System.EventArgs)
            label_header.Text = System.String.Format("There is {0:c2} available to pay the invoices for this creditor. Please indicate the amount that is to be used to pay the invoices in the space below.", MaximumAvailable)
        End Sub
    End Class

End Namespace