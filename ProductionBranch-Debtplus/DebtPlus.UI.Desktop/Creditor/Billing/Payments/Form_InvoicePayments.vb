#Region "Copyright 2000-2012 DebtPlus, L.L.C."
'{*******************************************************************}
'{                                                                   }
'{       DebtPlus Debt Management System                             }
'{                                                                   }
'{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
'{       ALL RIGHTS RESERVED                                         }
'{                                                                   }
'{   The entire contents of this file is protected by U.S. and       }
'{   International Copyright Laws. Unauthorized reproduction,        }
'{   reverse-engineering, and distribution of all or any portion of  }
'{   the code contained in this file is strictly prohibited and may  }
'{   result in severe civil and criminal penalties and will be       }
'{   prosecuted to the maximum extent possible under the law.        }
'{                                                                   }
'{   RESTRICTIONS                                                    }
'{                                                                   }
'{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
'{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
'{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
'{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
'{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
'{                                                                   }
'{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
'{   ADDITIONAL RESTRICTIONS.                                        }
'{                                                                   }
'{*******************************************************************}
#End Region

Option Compare Binary
Option Explicit On 
Option Strict On

Imports DebtPlus.UI.Creditor.Widgets.Controls
Imports DebtPlus.Utils
Imports System.Data.SqlClient
Imports DebtPlus.UI.Creditor.Widgets
Imports System.Windows.Forms

Namespace Creditor.Billing.Payments
    Friend Class Form_InvoicePayments
        Inherits DebtPlus.Data.Forms.DebtPlusForm

        Friend ds As New System.Data.DataSet("ds")
        Friend WithEvents Text_CreditorID As CreditorID
        Private ap As ArgParser = Nothing

        Public Sub New(ByVal ap As ArgParser)
            MyClass.New()
            Me.ap = ap
        End Sub

        Public Sub New()
            MyBase.New()
            InitializeComponent()
            RegisterHandlers()
        End Sub

        Private Sub RegisterHandlers()
            AddHandler Text_Amount.EditValueChanging, AddressOf Text_Amount_EditValueChanging
            AddHandler Load, AddressOf Form_InvoicePayments_Load
            AddHandler Text_Amount.EditValueChanged, AddressOf txt_check_amount_TextChanged
            AddHandler GridView1.DoubleClick, AddressOf GridView1_DoubleClick
            AddHandler GridView1.FocusedRowChanged, AddressOf GridView1_FocusedRowChanged
            AddHandler Text_CreditorID.Validated, AddressOf Text_CreditorID_Validated
            AddHandler Button_Select.Click, AddressOf Button_Select_Click
            AddHandler Button_NonAR.Click, AddressOf Button_NonAR_Click
            AddHandler Button_PayOldest.Click, AddressOf Button_PayOldest_Click
            AddHandler GridView1.Layout, AddressOf LayoutChanged
        End Sub

        Private Sub RemoveHandlers()
            RemoveHandler Text_Amount.EditValueChanging, AddressOf Text_Amount_EditValueChanging
            RemoveHandler Load, AddressOf Form_InvoicePayments_Load
            RemoveHandler Text_Amount.EditValueChanged, AddressOf txt_check_amount_TextChanged
            RemoveHandler GridView1.DoubleClick, AddressOf GridView1_DoubleClick
            RemoveHandler GridView1.FocusedRowChanged, AddressOf GridView1_FocusedRowChanged
            RemoveHandler Text_CreditorID.Validated, AddressOf Text_CreditorID_Validated
            RemoveHandler Button_Select.Click, AddressOf Button_Select_Click
            RemoveHandler Button_NonAR.Click, AddressOf Button_NonAR_Click
            RemoveHandler Button_PayOldest.Click, AddressOf Button_PayOldest_Click
            RemoveHandler GridView1.Layout, AddressOf LayoutChanged
        End Sub

        ''' <summary>
        ''' A/R payments
        ''' </summary>
        Private Property ar_payments() As Decimal
            Get
                If label_ar.Text.Length = 0 Then Return 0D
                Return System.Decimal.Parse(label_ar.Text, Globalization.NumberStyles.Currency)
            End Get
            Set(ByVal Value As Decimal)
                label_ar.Text = String.Format("{0:c}", Value)
                unspent_funds = check_amount - ar_payments - non_ar_payments
            End Set
        End Property

        ''' <summary>
        ''' Non-A/R payments
        ''' </summary>
        Private Property non_ar_payments() As Decimal
            Get
                If label_non_ar.Text.Length = 0 Then Return 0D
                Return System.Decimal.Parse(label_non_ar.Text, Globalization.NumberStyles.Currency)
            End Get
            Set(ByVal Value As Decimal)
                label_non_ar.Text = String.Format("{0:c}", Value)
                unspent_funds = check_amount - ar_payments - non_ar_payments
            End Set
        End Property

        ''' <summary>
        ''' Check amount
        ''' </summary>
        Private Property check_amount() As Decimal
            Get
                Return Convert.ToDecimal(Text_Amount.Value)
            End Get
            Set(ByVal Value As Decimal)
                Text_Amount.Value = Value
            End Set
        End Property

        ''' <summary>
        ''' Unspent funds from the check
        ''' </summary>
        Private Property unspent_funds() As Decimal
            Get
                If label_unspent_funds.Text.Length = 0 Then Return 0D
                Return System.Decimal.Parse(label_unspent_funds.Text, Globalization.NumberStyles.Currency)
                Return Convert.ToDecimal(label_unspent_funds.Text)
            End Get
            Set(ByVal Value As Decimal)
                label_unspent_funds.Text = String.Format("{0:c}", Value)
                EnableOK()
            End Set
        End Property

        ''' <summary>
        ''' Enable or Disable the buttons
        ''' </summary>
        Private Sub EnableOK()
            Button_NonAR.Enabled = (unspent_funds > 0D) AndAlso Label_CreditorAddress.Text <> String.Empty
            Button_PayOldest.Enabled = Button_NonAR.Enabled AndAlso ds.Tables.Contains("invoices") AndAlso Not IsNothing(ds.Tables("invoices").Rows)

            ' Look for a row on the grid to enable the select button
            Dim EditRow As System.Data.DataRow = CType(GridView1.GetDataRow(GridView1.FocusedRowHandle), System.Data.DataRow)
            Button_Select.Enabled = EditRow IsNot Nothing AndAlso Convert.ToDecimal(EditRow("balance")) > 0D
        End Sub

        ''' <summary>
        ''' Current creditor ID
        ''' </summary>
        Private Property creditor() As String
            Get
                Return Text_CreditorID.Text.Trim()
            End Get
            Set(ByVal Value As String)
                Text_CreditorID.Text = Value
            End Set
        End Property

#Region " Windows Form Designer generated code "

        'Form overrides dispose to clean up the component list.
        Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
            If disposing Then
                If components IsNot Nothing Then
                    components.Dispose()
                End If
            End If
            MyBase.Dispose(disposing)
        End Sub

        'Required by the Windows Form Designer
        Private components As System.ComponentModel.IContainer

        'NOTE: The following procedure is required by the Windows Form Designer
        'It can be modified using the Windows Form Designer.  
        'Do not modify it using the code editor.
        Friend WithEvents LabelControl1 As DevExpress.XtraEditors.LabelControl
        Friend WithEvents LabelControl2 As DevExpress.XtraEditors.LabelControl
        Friend WithEvents LabelControl3 As DevExpress.XtraEditors.LabelControl
        Friend WithEvents LabelControl4 As DevExpress.XtraEditors.LabelControl
        Friend WithEvents LabelControl5 As DevExpress.XtraEditors.LabelControl
        Friend WithEvents LabelControl6 As DevExpress.XtraEditors.LabelControl
        Friend WithEvents LabelControl7 As DevExpress.XtraEditors.LabelControl
        Friend WithEvents Text_Amount As DevExpress.XtraEditors.CalcEdit
        Friend WithEvents Text_ReferenceInfo As DevExpress.XtraEditors.TextEdit
        Friend WithEvents Button_Select As DevExpress.XtraEditors.SimpleButton
        Friend WithEvents Button_PayOldest As DevExpress.XtraEditors.SimpleButton
        Friend WithEvents Button_NonAR As DevExpress.XtraEditors.SimpleButton
        Friend WithEvents GridControl1 As DevExpress.XtraGrid.GridControl
        Friend WithEvents GridView1 As DevExpress.XtraGrid.Views.Grid.GridView
        Friend WithEvents Label_CreditorAddress As DevExpress.XtraEditors.LabelControl
        Friend WithEvents label_ar As DevExpress.XtraEditors.LabelControl
        Friend WithEvents label_non_ar As DevExpress.XtraEditors.LabelControl
        Friend WithEvents label_unspent_funds As DevExpress.XtraEditors.LabelControl
        Friend WithEvents label_net_invoice_amt As DevExpress.XtraEditors.LabelControl
        Friend WithEvents GridColumn_Invoice As DevExpress.XtraGrid.Columns.GridColumn
        Friend WithEvents GridColumn_CheckNumber As DevExpress.XtraGrid.Columns.GridColumn
        Friend WithEvents GridColumn_InvDate As DevExpress.XtraGrid.Columns.GridColumn
        Friend WithEvents GridColumn_InvAmount As DevExpress.XtraGrid.Columns.GridColumn
        Friend WithEvents GridColumn_PmtAmt As DevExpress.XtraGrid.Columns.GridColumn
        Friend WithEvents GridColumn_PmtDate As DevExpress.XtraGrid.Columns.GridColumn
        Friend WithEvents GridColumn_AdjAmt As DevExpress.XtraGrid.Columns.GridColumn
        Friend WithEvents GridColumn_AdjDate As DevExpress.XtraGrid.Columns.GridColumn
        Friend WithEvents GridColumn_Balance As DevExpress.XtraGrid.Columns.GridColumn
        <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
            Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(Form_InvoicePayments))
            Dim SerializableAppearanceObject1 As DevExpress.Utils.SerializableAppearanceObject = New DevExpress.Utils.SerializableAppearanceObject
            Me.LabelControl1 = New DevExpress.XtraEditors.LabelControl
            Me.LabelControl2 = New DevExpress.XtraEditors.LabelControl
            Me.LabelControl3 = New DevExpress.XtraEditors.LabelControl
            Me.LabelControl4 = New DevExpress.XtraEditors.LabelControl
            Me.LabelControl5 = New DevExpress.XtraEditors.LabelControl
            Me.LabelControl6 = New DevExpress.XtraEditors.LabelControl
            Me.LabelControl7 = New DevExpress.XtraEditors.LabelControl
            Me.Text_Amount = New DevExpress.XtraEditors.CalcEdit
            Me.label_ar = New DevExpress.XtraEditors.LabelControl
            Me.label_non_ar = New DevExpress.XtraEditors.LabelControl
            Me.label_unspent_funds = New DevExpress.XtraEditors.LabelControl
            Me.label_net_invoice_amt = New DevExpress.XtraEditors.LabelControl
            Me.Text_ReferenceInfo = New DevExpress.XtraEditors.TextEdit
            Me.Button_Select = New DevExpress.XtraEditors.SimpleButton
            Me.Button_PayOldest = New DevExpress.XtraEditors.SimpleButton
            Me.Button_NonAR = New DevExpress.XtraEditors.SimpleButton
            Me.GridControl1 = New DevExpress.XtraGrid.GridControl
            Me.GridView1 = New DevExpress.XtraGrid.Views.Grid.GridView
            Me.GridColumn_Invoice = New DevExpress.XtraGrid.Columns.GridColumn
            Me.GridColumn_Invoice.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.True
            Me.GridColumn_CheckNumber = New DevExpress.XtraGrid.Columns.GridColumn
            Me.GridColumn_CheckNumber.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.True
            Me.GridColumn_InvDate = New DevExpress.XtraGrid.Columns.GridColumn
            Me.GridColumn_InvDate.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.True
            Me.GridColumn_InvAmount = New DevExpress.XtraGrid.Columns.GridColumn
            Me.GridColumn_InvAmount.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.True
            Me.GridColumn_PmtAmt = New DevExpress.XtraGrid.Columns.GridColumn
            Me.GridColumn_PmtAmt.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.True
            Me.GridColumn_PmtDate = New DevExpress.XtraGrid.Columns.GridColumn
            Me.GridColumn_PmtDate.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.True
            Me.GridColumn_AdjAmt = New DevExpress.XtraGrid.Columns.GridColumn
            Me.GridColumn_AdjAmt.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.True
            Me.GridColumn_AdjDate = New DevExpress.XtraGrid.Columns.GridColumn
            Me.GridColumn_AdjDate.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.True
            Me.GridColumn_Balance = New DevExpress.XtraGrid.Columns.GridColumn
            Me.GridColumn_Balance.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.True
            Me.Label_CreditorAddress = New DevExpress.XtraEditors.LabelControl
            Me.Text_CreditorID = New CreditorID
            CType(Me.DxErrorProvider1, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.Text_Amount.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.Text_ReferenceInfo.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.GridControl1, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.GridView1, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.Text_CreditorID.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
            Me.SuspendLayout()
            '
            'LabelControl1
            '
            Me.LabelControl1.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.Horizontal
            Me.LabelControl1.Location = New System.Drawing.Point(59, 9)
            Me.LabelControl1.Name = "LabelControl1"
            Me.LabelControl1.Size = New System.Drawing.Size(57, 13)
            Me.LabelControl1.TabIndex = 0
            Me.LabelControl1.Text = "Creditor &ID:"
            Me.LabelControl1.ToolTipController = Me.ToolTipController1
            '
            'LabelControl2
            '
            Me.LabelControl2.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.Horizontal
            Me.LabelControl2.Location = New System.Drawing.Point(43, 34)
            Me.LabelControl2.Name = "LabelControl2"
            Me.LabelControl2.Size = New System.Drawing.Size(73, 13)
            Me.LabelControl2.TabIndex = 3
            Me.LabelControl2.Text = "Check &Amount:"
            Me.LabelControl2.ToolTipController = Me.ToolTipController1
            '
            'LabelControl3
            '
            Me.LabelControl3.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.Horizontal
            Me.LabelControl3.Location = New System.Drawing.Point(44, 60)
            Me.LabelControl3.Name = "LabelControl3"
            Me.LabelControl3.Size = New System.Drawing.Size(72, 13)
            Me.LabelControl3.TabIndex = 5
            Me.LabelControl3.Text = "A/R Payments:"
            Me.LabelControl3.ToolTipController = Me.ToolTipController1
            Me.LabelControl3.UseMnemonic = False
            '
            'LabelControl4
            '
            Me.LabelControl4.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.Horizontal
            Me.LabelControl4.Location = New System.Drawing.Point(21, 73)
            Me.LabelControl4.Name = "LabelControl4"
            Me.LabelControl4.Size = New System.Drawing.Size(95, 13)
            Me.LabelControl4.TabIndex = 7
            Me.LabelControl4.Text = "Non-A/R Payments:"
            Me.LabelControl4.ToolTipController = Me.ToolTipController1
            Me.LabelControl4.UseMnemonic = False
            '
            'LabelControl5
            '
            Me.LabelControl5.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.Horizontal
            Me.LabelControl5.Location = New System.Drawing.Point(36, 86)
            Me.LabelControl5.Name = "LabelControl5"
            Me.LabelControl5.Size = New System.Drawing.Size(80, 13)
            Me.LabelControl5.TabIndex = 9
            Me.LabelControl5.Text = "Un-spent Funds:"
            Me.LabelControl5.ToolTipController = Me.ToolTipController1
            Me.LabelControl5.UseMnemonic = False
            '
            'LabelControl6
            '
            Me.LabelControl6.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.Horizontal
            Me.LabelControl6.Location = New System.Drawing.Point(8, 99)
            Me.LabelControl6.Name = "LabelControl6"
            Me.LabelControl6.Size = New System.Drawing.Size(108, 13)
            Me.LabelControl6.TabIndex = 11
            Me.LabelControl6.Text = "Total Net Invoice Amt:"
            Me.LabelControl6.ToolTipController = Me.ToolTipController1
            Me.LabelControl6.UseMnemonic = False
            '
            'LabelControl7
            '
            Me.LabelControl7.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.Horizontal
            Me.LabelControl7.Location = New System.Drawing.Point(264, 99)
            Me.LabelControl7.Name = "LabelControl7"
            Me.LabelControl7.Size = New System.Drawing.Size(77, 13)
            Me.LabelControl7.TabIndex = 13
            Me.LabelControl7.Text = "&Reference Info:"
            Me.LabelControl7.ToolTipController = Me.ToolTipController1
            '
            'Text_Amount
            '
            Me.Text_Amount.Location = New System.Drawing.Point(136, 30)
            Me.Text_Amount.Name = "Text_Amount"
            Me.Text_Amount.Properties.Appearance.Options.UseTextOptions = True
            Me.Text_Amount.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
            Me.Text_Amount.Properties.AppearanceDisabled.Options.UseTextOptions = True
            Me.Text_Amount.Properties.AppearanceDisabled.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
            Me.Text_Amount.Properties.AppearanceDropDown.Options.UseTextOptions = True
            Me.Text_Amount.Properties.AppearanceDropDown.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
            Me.Text_Amount.Properties.AppearanceFocused.Options.UseTextOptions = True
            Me.Text_Amount.Properties.AppearanceFocused.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
            Me.Text_Amount.Properties.AppearanceReadOnly.Options.UseTextOptions = True
            Me.Text_Amount.Properties.AppearanceReadOnly.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
            Me.Text_Amount.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
            Me.Text_Amount.Properties.DisplayFormat.FormatString = "{0:c}"
            Me.Text_Amount.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
            Me.Text_Amount.Properties.EditFormat.FormatString = "{0:f2}"
            Me.Text_Amount.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric
            Me.Text_Amount.Properties.Mask.BeepOnError = True
            Me.Text_Amount.Properties.Mask.EditMask = "c"
            Me.Text_Amount.Properties.Precision = 2
            Me.Text_Amount.Size = New System.Drawing.Size(100, 20)
            Me.Text_Amount.TabIndex = 4
            Me.Text_Amount.ToolTip = "This is the total amount of the check"
            Me.Text_Amount.ToolTipController = Me.ToolTipController1
            '
            'label_ar
            '
            Me.label_ar.Appearance.Options.UseTextOptions = True
            Me.label_ar.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
            Me.label_ar.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.Vertical
            Me.label_ar.Location = New System.Drawing.Point(136, 60)
            Me.label_ar.Name = "label_ar"
            Me.label_ar.Size = New System.Drawing.Size(100, 13)
            Me.label_ar.TabIndex = 6
            Me.label_ar.Text = "$0.00"
            Me.label_ar.ToolTipController = Me.ToolTipController1
            '
            'label_non_ar
            '
            Me.label_non_ar.Appearance.Options.UseTextOptions = True
            Me.label_non_ar.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
            Me.label_non_ar.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.Vertical
            Me.label_non_ar.Location = New System.Drawing.Point(136, 73)
            Me.label_non_ar.Name = "label_non_ar"
            Me.label_non_ar.Size = New System.Drawing.Size(100, 13)
            Me.label_non_ar.TabIndex = 8
            Me.label_non_ar.Text = "$0.00"
            Me.label_non_ar.ToolTipController = Me.ToolTipController1
            '
            'label_unspent_funds
            '
            Me.label_unspent_funds.Appearance.Options.UseTextOptions = True
            Me.label_unspent_funds.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
            Me.label_unspent_funds.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.Vertical
            Me.label_unspent_funds.Location = New System.Drawing.Point(136, 86)
            Me.label_unspent_funds.Name = "label_unspent_funds"
            Me.label_unspent_funds.Size = New System.Drawing.Size(100, 13)
            Me.label_unspent_funds.TabIndex = 10
            Me.label_unspent_funds.Text = "$0.00"
            Me.label_unspent_funds.ToolTipController = Me.ToolTipController1
            '
            'label_net_invoice_amt
            '
            Me.label_net_invoice_amt.Appearance.Options.UseTextOptions = True
            Me.label_net_invoice_amt.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
            Me.label_net_invoice_amt.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.Vertical
            Me.label_net_invoice_amt.Location = New System.Drawing.Point(136, 99)
            Me.label_net_invoice_amt.Name = "label_net_invoice_amt"
            Me.label_net_invoice_amt.Size = New System.Drawing.Size(100, 13)
            Me.label_net_invoice_amt.TabIndex = 12
            Me.label_net_invoice_amt.Text = "$0.00"
            Me.label_net_invoice_amt.ToolTipController = Me.ToolTipController1
            '
            'Text_ReferenceInfo
            '
            Me.Text_ReferenceInfo.Location = New System.Drawing.Point(352, 95)
            Me.Text_ReferenceInfo.Name = "Text_ReferenceInfo"
            Me.Text_ReferenceInfo.Size = New System.Drawing.Size(264, 20)
            Me.Text_ReferenceInfo.TabIndex = 14
            Me.Text_ReferenceInfo.ToolTip = "Include any reference information associated with the payment. For example, the c" & _
                "heck number."
            Me.Text_ReferenceInfo.ToolTipController = Me.ToolTipController1
            '
            'Button_Select
            '
            Me.Button_Select.Location = New System.Drawing.Point(544, 8)
            Me.Button_Select.Name = "Button_Select"
            Me.Button_Select.Size = New System.Drawing.Size(75, 23)
            Me.Button_Select.TabIndex = 15
            Me.Button_Select.Text = "&Select"
            Me.Button_Select.ToolTip = "Pay the indicated invoice"
            Me.Button_Select.ToolTipController = Me.ToolTipController1
            '
            'Button_PayOldest
            '
            Me.Button_PayOldest.Location = New System.Drawing.Point(544, 39)
            Me.Button_PayOldest.Name = "Button_PayOldest"
            Me.Button_PayOldest.Size = New System.Drawing.Size(75, 23)
            Me.Button_PayOldest.TabIndex = 16
            Me.Button_PayOldest.Text = "&Pay Oldest"
            Me.Button_PayOldest.ToolTip = "Automatically pay the oldest invoice or invoices until the balance is paid"
            Me.Button_PayOldest.ToolTipController = Me.ToolTipController1
            '
            'Button_NonAR
            '
            Me.Button_NonAR.Location = New System.Drawing.Point(544, 70)
            Me.Button_NonAR.Name = "Button_NonAR"
            Me.Button_NonAR.Size = New System.Drawing.Size(75, 23)
            Me.Button_NonAR.TabIndex = 17
            Me.Button_NonAR.Text = "&Non-A/R"
            Me.Button_NonAR.ToolTip = "If you wish to make a non-AR payment for this creditor, click here."
            Me.Button_NonAR.ToolTipController = Me.ToolTipController1
            '
            'GridControl1
            '
            Me.GridControl1.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                        Or System.Windows.Forms.AnchorStyles.Left) _
                        Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
            Me.GridControl1.Location = New System.Drawing.Point(8, 128)
            Me.GridControl1.MainView = Me.GridView1
            Me.GridControl1.Name = "GridControl1"
            Me.GridControl1.Size = New System.Drawing.Size(624, 200)
            Me.GridControl1.TabIndex = 18
            Me.GridControl1.ViewCollection.AddRange(New DevExpress.XtraGrid.Views.Base.BaseView() {Me.GridView1})
            '
            'GridView1
            '
            Me.GridView1.Appearance.ColumnFilterButton.BackColor = System.Drawing.Color.FromArgb(CType(CType(104, Byte), Int32), CType(CType(184, Byte), Int32), CType(CType(251, Byte), Int32))
            Me.GridView1.Appearance.ColumnFilterButton.BorderColor = System.Drawing.Color.FromArgb(CType(CType(104, Byte), Int32), CType(CType(184, Byte), Int32), CType(CType(251, Byte), Int32))
            Me.GridView1.Appearance.ColumnFilterButton.ForeColor = System.Drawing.Color.White
            Me.GridView1.Appearance.ColumnFilterButton.Options.UseBackColor = True
            Me.GridView1.Appearance.ColumnFilterButton.Options.UseBorderColor = True
            Me.GridView1.Appearance.ColumnFilterButton.Options.UseForeColor = True
            Me.GridView1.Appearance.ColumnFilterButtonActive.BackColor = System.Drawing.Color.FromArgb(CType(CType(170, Byte), Int32), CType(CType(216, Byte), Int32), CType(CType(254, Byte), Int32))
            Me.GridView1.Appearance.ColumnFilterButtonActive.BorderColor = System.Drawing.Color.FromArgb(CType(CType(170, Byte), Int32), CType(CType(216, Byte), Int32), CType(CType(254, Byte), Int32))
            Me.GridView1.Appearance.ColumnFilterButtonActive.ForeColor = System.Drawing.Color.Black
            Me.GridView1.Appearance.ColumnFilterButtonActive.Options.UseBackColor = True
            Me.GridView1.Appearance.ColumnFilterButtonActive.Options.UseBorderColor = True
            Me.GridView1.Appearance.ColumnFilterButtonActive.Options.UseForeColor = True
            Me.GridView1.Appearance.Empty.BackColor = System.Drawing.Color.FromArgb(CType(CType(236, Byte), Int32), CType(CType(246, Byte), Int32), CType(CType(255, Byte), Int32))
            Me.GridView1.Appearance.Empty.BackColor2 = System.Drawing.Color.White
            Me.GridView1.Appearance.Empty.Options.UseBackColor = True
            Me.GridView1.Appearance.EvenRow.BackColor = System.Drawing.Color.FromArgb(CType(CType(247, Byte), Int32), CType(CType(251, Byte), Int32), CType(CType(255, Byte), Int32))
            Me.GridView1.Appearance.EvenRow.BorderColor = System.Drawing.Color.FromArgb(CType(CType(247, Byte), Int32), CType(CType(251, Byte), Int32), CType(CType(255, Byte), Int32))
            Me.GridView1.Appearance.EvenRow.ForeColor = System.Drawing.Color.Black
            Me.GridView1.Appearance.EvenRow.Options.UseBackColor = True
            Me.GridView1.Appearance.EvenRow.Options.UseBorderColor = True
            Me.GridView1.Appearance.EvenRow.Options.UseForeColor = True
            Me.GridView1.Appearance.FilterCloseButton.BackColor = System.Drawing.Color.FromArgb(CType(CType(104, Byte), Int32), CType(CType(184, Byte), Int32), CType(CType(251, Byte), Int32))
            Me.GridView1.Appearance.FilterCloseButton.BorderColor = System.Drawing.Color.FromArgb(CType(CType(104, Byte), Int32), CType(CType(184, Byte), Int32), CType(CType(251, Byte), Int32))
            Me.GridView1.Appearance.FilterCloseButton.ForeColor = System.Drawing.Color.White
            Me.GridView1.Appearance.FilterCloseButton.Options.UseBackColor = True
            Me.GridView1.Appearance.FilterCloseButton.Options.UseBorderColor = True
            Me.GridView1.Appearance.FilterCloseButton.Options.UseForeColor = True
            Me.GridView1.Appearance.FilterPanel.BackColor = System.Drawing.Color.FromArgb(CType(CType(236, Byte), Int32), CType(CType(246, Byte), Int32), CType(CType(255, Byte), Int32))
            Me.GridView1.Appearance.FilterPanel.BackColor2 = System.Drawing.Color.White
            Me.GridView1.Appearance.FilterPanel.ForeColor = System.Drawing.Color.Black
            Me.GridView1.Appearance.FilterPanel.Options.UseBackColor = True
            Me.GridView1.Appearance.FilterPanel.Options.UseForeColor = True
            Me.GridView1.Appearance.FixedLine.BackColor = System.Drawing.Color.FromArgb(CType(CType(59, Byte), Int32), CType(CType(133, Byte), Int32), CType(CType(195, Byte), Int32))
            Me.GridView1.Appearance.FixedLine.Options.UseBackColor = True
            Me.GridView1.Appearance.FocusedCell.BackColor = System.Drawing.Color.White
            Me.GridView1.Appearance.FocusedCell.ForeColor = System.Drawing.Color.Black
            Me.GridView1.Appearance.FocusedCell.Options.UseBackColor = True
            Me.GridView1.Appearance.FocusedCell.Options.UseForeColor = True
            Me.GridView1.Appearance.FocusedRow.BackColor = System.Drawing.Color.FromArgb(CType(CType(38, Byte), Int32), CType(CType(109, Byte), Int32), CType(CType(189, Byte), Int32))
            Me.GridView1.Appearance.FocusedRow.BorderColor = System.Drawing.Color.FromArgb(CType(CType(59, Byte), Int32), CType(CType(139, Byte), Int32), CType(CType(206, Byte), Int32))
            Me.GridView1.Appearance.FocusedRow.ForeColor = System.Drawing.Color.White
            Me.GridView1.Appearance.FocusedRow.Options.UseBackColor = True
            Me.GridView1.Appearance.FocusedRow.Options.UseBorderColor = True
            Me.GridView1.Appearance.FocusedRow.Options.UseForeColor = True
            Me.GridView1.Appearance.FooterPanel.BackColor = System.Drawing.Color.FromArgb(CType(CType(104, Byte), Int32), CType(CType(184, Byte), Int32), CType(CType(251, Byte), Int32))
            Me.GridView1.Appearance.FooterPanel.BorderColor = System.Drawing.Color.FromArgb(CType(CType(104, Byte), Int32), CType(CType(184, Byte), Int32), CType(CType(251, Byte), Int32))
            Me.GridView1.Appearance.FooterPanel.ForeColor = System.Drawing.Color.Black
            Me.GridView1.Appearance.FooterPanel.Options.UseBackColor = True
            Me.GridView1.Appearance.FooterPanel.Options.UseBorderColor = True
            Me.GridView1.Appearance.FooterPanel.Options.UseForeColor = True
            Me.GridView1.Appearance.GroupButton.BackColor = System.Drawing.Color.FromArgb(CType(CType(104, Byte), Int32), CType(CType(184, Byte), Int32), CType(CType(251, Byte), Int32))
            Me.GridView1.Appearance.GroupButton.BorderColor = System.Drawing.Color.FromArgb(CType(CType(104, Byte), Int32), CType(CType(184, Byte), Int32), CType(CType(251, Byte), Int32))
            Me.GridView1.Appearance.GroupButton.Options.UseBackColor = True
            Me.GridView1.Appearance.GroupButton.Options.UseBorderColor = True
            Me.GridView1.Appearance.GroupFooter.BackColor = System.Drawing.Color.FromArgb(CType(CType(170, Byte), Int32), CType(CType(216, Byte), Int32), CType(CType(254, Byte), Int32))
            Me.GridView1.Appearance.GroupFooter.BorderColor = System.Drawing.Color.FromArgb(CType(CType(170, Byte), Int32), CType(CType(216, Byte), Int32), CType(CType(254, Byte), Int32))
            Me.GridView1.Appearance.GroupFooter.ForeColor = System.Drawing.Color.Black
            Me.GridView1.Appearance.GroupFooter.Options.UseBackColor = True
            Me.GridView1.Appearance.GroupFooter.Options.UseBorderColor = True
            Me.GridView1.Appearance.GroupFooter.Options.UseForeColor = True
            Me.GridView1.Appearance.GroupPanel.BackColor = System.Drawing.Color.FromArgb(CType(CType(236, Byte), Int32), CType(CType(246, Byte), Int32), CType(CType(255, Byte), Int32))
            Me.GridView1.Appearance.GroupPanel.BackColor2 = System.Drawing.Color.White
            Me.GridView1.Appearance.GroupPanel.ForeColor = System.Drawing.Color.Black
            Me.GridView1.Appearance.GroupPanel.Options.UseBackColor = True
            Me.GridView1.Appearance.GroupPanel.Options.UseForeColor = True
            Me.GridView1.Appearance.GroupRow.BackColor = System.Drawing.Color.FromArgb(CType(CType(170, Byte), Int32), CType(CType(216, Byte), Int32), CType(CType(254, Byte), Int32))
            Me.GridView1.Appearance.GroupRow.BorderColor = System.Drawing.Color.FromArgb(CType(CType(170, Byte), Int32), CType(CType(216, Byte), Int32), CType(CType(254, Byte), Int32))
            Me.GridView1.Appearance.GroupRow.ForeColor = System.Drawing.Color.Black
            Me.GridView1.Appearance.GroupRow.Options.UseBackColor = True
            Me.GridView1.Appearance.GroupRow.Options.UseBorderColor = True
            Me.GridView1.Appearance.GroupRow.Options.UseForeColor = True
            Me.GridView1.Appearance.HeaderPanel.BackColor = System.Drawing.Color.FromArgb(CType(CType(139, Byte), Int32), CType(CType(201, Byte), Int32), CType(CType(254, Byte), Int32))
            Me.GridView1.Appearance.HeaderPanel.BorderColor = System.Drawing.Color.FromArgb(CType(CType(139, Byte), Int32), CType(CType(201, Byte), Int32), CType(CType(254, Byte), Int32))
            Me.GridView1.Appearance.HeaderPanel.ForeColor = System.Drawing.Color.Black
            Me.GridView1.Appearance.HeaderPanel.Options.UseBackColor = True
            Me.GridView1.Appearance.HeaderPanel.Options.UseBorderColor = True
            Me.GridView1.Appearance.HeaderPanel.Options.UseForeColor = True
            Me.GridView1.Appearance.HideSelectionRow.BackColor = System.Drawing.Color.FromArgb(CType(CType(105, Byte), Int32), CType(CType(170, Byte), Int32), CType(CType(225, Byte), Int32))
            Me.GridView1.Appearance.HideSelectionRow.BorderColor = System.Drawing.Color.FromArgb(CType(CType(83, Byte), Int32), CType(CType(155, Byte), Int32), CType(CType(215, Byte), Int32))
            Me.GridView1.Appearance.HideSelectionRow.ForeColor = System.Drawing.Color.FromArgb(CType(CType(236, Byte), Int32), CType(CType(246, Byte), Int32), CType(CType(255, Byte), Int32))
            Me.GridView1.Appearance.HideSelectionRow.Options.UseBackColor = True
            Me.GridView1.Appearance.HideSelectionRow.Options.UseBorderColor = True
            Me.GridView1.Appearance.HideSelectionRow.Options.UseForeColor = True
            Me.GridView1.Appearance.HorzLine.BackColor = System.Drawing.Color.FromArgb(CType(CType(104, Byte), Int32), CType(CType(184, Byte), Int32), CType(CType(251, Byte), Int32))
            Me.GridView1.Appearance.HorzLine.Options.UseBackColor = True
            Me.GridView1.Appearance.OddRow.BackColor = System.Drawing.Color.FromArgb(CType(CType(236, Byte), Int32), CType(CType(246, Byte), Int32), CType(CType(255, Byte), Int32))
            Me.GridView1.Appearance.OddRow.BorderColor = System.Drawing.Color.FromArgb(CType(CType(236, Byte), Int32), CType(CType(246, Byte), Int32), CType(CType(255, Byte), Int32))
            Me.GridView1.Appearance.OddRow.ForeColor = System.Drawing.Color.Black
            Me.GridView1.Appearance.OddRow.Options.UseBackColor = True
            Me.GridView1.Appearance.OddRow.Options.UseBorderColor = True
            Me.GridView1.Appearance.OddRow.Options.UseForeColor = True
            Me.GridView1.Appearance.Preview.Font = New System.Drawing.Font("Verdana", 7.5!)
            Me.GridView1.Appearance.Preview.ForeColor = System.Drawing.Color.FromArgb(CType(CType(83, Byte), Int32), CType(CType(155, Byte), Int32), CType(CType(215, Byte), Int32))
            Me.GridView1.Appearance.Preview.Options.UseFont = True
            Me.GridView1.Appearance.Preview.Options.UseForeColor = True
            Me.GridView1.Appearance.Row.BackColor = System.Drawing.Color.FromArgb(CType(CType(247, Byte), Int32), CType(CType(251, Byte), Int32), CType(CType(255, Byte), Int32))
            Me.GridView1.Appearance.Row.ForeColor = System.Drawing.Color.Black
            Me.GridView1.Appearance.Row.Options.UseBackColor = True
            Me.GridView1.Appearance.Row.Options.UseForeColor = True
            Me.GridView1.Appearance.RowSeparator.BackColor = System.Drawing.Color.FromArgb(CType(CType(236, Byte), Int32), CType(CType(246, Byte), Int32), CType(CType(255, Byte), Int32))
            Me.GridView1.Appearance.RowSeparator.BackColor2 = System.Drawing.Color.White
            Me.GridView1.Appearance.RowSeparator.Options.UseBackColor = True
            Me.GridView1.Appearance.SelectedRow.BackColor = System.Drawing.Color.FromArgb(CType(CType(83, Byte), Int32), CType(CType(155, Byte), Int32), CType(CType(215, Byte), Int32))
            Me.GridView1.Appearance.SelectedRow.ForeColor = System.Drawing.Color.White
            Me.GridView1.Appearance.SelectedRow.Options.UseBackColor = True
            Me.GridView1.Appearance.SelectedRow.Options.UseForeColor = True
            Me.GridView1.Appearance.TopNewRow.BackColor = System.Drawing.Color.White
            Me.GridView1.Appearance.TopNewRow.Options.UseBackColor = True
            Me.GridView1.Appearance.VertLine.BackColor = System.Drawing.Color.FromArgb(CType(CType(104, Byte), Int32), CType(CType(184, Byte), Int32), CType(CType(251, Byte), Int32))
            Me.GridView1.Appearance.VertLine.Options.UseBackColor = True
            Me.GridView1.Columns.AddRange(New DevExpress.XtraGrid.Columns.GridColumn() {Me.GridColumn_Invoice, Me.GridColumn_CheckNumber, Me.GridColumn_InvDate, Me.GridColumn_InvAmount, Me.GridColumn_PmtAmt, Me.GridColumn_PmtDate, Me.GridColumn_AdjAmt, Me.GridColumn_AdjDate, Me.GridColumn_Balance})
            Me.GridView1.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus
            Me.GridView1.GridControl = Me.GridControl1
            Me.GridView1.GroupSummary.AddRange(New DevExpress.XtraGrid.GridSummaryItem() {New DevExpress.XtraGrid.GridGroupSummaryItem(DevExpress.Data.SummaryItemType.Sum, "balance", Nothing, "")})
            Me.GridView1.Name = "GridView1"
            Me.GridView1.OptionsBehavior.AutoPopulateColumns = False
            Me.GridView1.OptionsBehavior.Editable = False
            Me.GridView1.OptionsView.AutoCalcPreviewLineCount = True
            Me.GridView1.OptionsView.EnableAppearanceEvenRow = True
            Me.GridView1.OptionsView.EnableAppearanceOddRow = True
            Me.GridView1.OptionsView.RowAutoHeight = True
            Me.GridView1.OptionsView.ShowFooter = True
            Me.GridView1.OptionsView.ShowGroupPanel = False
            Me.GridView1.OptionsView.ShowIndicator = False
            Me.GridView1.ShowButtonMode = DevExpress.XtraGrid.Views.Base.ShowButtonModeEnum.ShowForFocusedRow
            '
            'GridColumn_Invoice
            '
            Me.GridColumn_Invoice.AppearanceCell.Options.UseTextOptions = True
            Me.GridColumn_Invoice.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
            Me.GridColumn_Invoice.AppearanceHeader.Options.UseTextOptions = True
            Me.GridColumn_Invoice.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
            Me.GridColumn_Invoice.Caption = "Invoice"
            Me.GridColumn_Invoice.CustomizationCaption = "InvoiceNumber"
            Me.GridColumn_Invoice.DisplayFormat.FormatString = "d10"
            Me.GridColumn_Invoice.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
            Me.GridColumn_Invoice.FieldName = "invoice_register"
            Me.GridColumn_Invoice.Name = "GridColumn_Invoice"
            Me.GridColumn_Invoice.Visible = True
            Me.GridColumn_Invoice.VisibleIndex = 0
            '
            'GridColumn_CheckNumber
            '
            Me.GridColumn_CheckNumber.AppearanceCell.Options.UseTextOptions = True
            Me.GridColumn_CheckNumber.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
            Me.GridColumn_CheckNumber.AppearanceHeader.Options.UseTextOptions = True
            Me.GridColumn_CheckNumber.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
            Me.GridColumn_CheckNumber.Caption = "Check #"
            Me.GridColumn_CheckNumber.CustomizationCaption = "CheckNumber"
            Me.GridColumn_CheckNumber.FieldName = "checknum"
            Me.GridColumn_CheckNumber.Name = "GridColumn_CheckNumber"
            Me.GridColumn_CheckNumber.Visible = True
            Me.GridColumn_CheckNumber.VisibleIndex = 1
            '
            'GridColumn_InvDate
            '
            Me.GridColumn_InvDate.AppearanceCell.Options.UseTextOptions = True
            Me.GridColumn_InvDate.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
            Me.GridColumn_InvDate.AppearanceHeader.Options.UseTextOptions = True
            Me.GridColumn_InvDate.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
            Me.GridColumn_InvDate.Caption = "Inv Date"
            Me.GridColumn_InvDate.CustomizationCaption = "InvoiceDate"
            Me.GridColumn_InvDate.DisplayFormat.FormatString = "d"
            Me.GridColumn_InvDate.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime
            Me.GridColumn_InvDate.FieldName = "inv_date"
            Me.GridColumn_InvDate.Name = "GridColumn_InvDate"
            Me.GridColumn_InvDate.Visible = True
            Me.GridColumn_InvDate.VisibleIndex = 2
            '
            'GridColumn_InvAmount
            '
            Me.GridColumn_InvAmount.AppearanceCell.Options.UseTextOptions = True
            Me.GridColumn_InvAmount.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
            Me.GridColumn_InvAmount.AppearanceHeader.Options.UseTextOptions = True
            Me.GridColumn_InvAmount.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
            Me.GridColumn_InvAmount.Caption = "Inv Amt"
            Me.GridColumn_InvAmount.CustomizationCaption = "InvoiceAmount"
            Me.GridColumn_InvAmount.DisplayFormat.FormatString = "c2"
            Me.GridColumn_InvAmount.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
            Me.GridColumn_InvAmount.FieldName = "inv_amount"
            Me.GridColumn_InvAmount.GroupFormat.FormatString = "c2"
            Me.GridColumn_InvAmount.GroupFormat.FormatType = DevExpress.Utils.FormatType.Numeric
            Me.GridColumn_InvAmount.Name = "GridColumn_InvAmount"
            Me.GridColumn_InvAmount.SummaryItem.DisplayFormat = "{0:c}"
            Me.GridColumn_InvAmount.SummaryItem.SummaryType = DevExpress.Data.SummaryItemType.Sum
            Me.GridColumn_InvAmount.Visible = True
            Me.GridColumn_InvAmount.VisibleIndex = 3
            '
            'GridColumn_PmtAmt
            '
            Me.GridColumn_PmtAmt.AppearanceCell.Options.UseTextOptions = True
            Me.GridColumn_PmtAmt.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
            Me.GridColumn_PmtAmt.AppearanceHeader.Options.UseTextOptions = True
            Me.GridColumn_PmtAmt.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
            Me.GridColumn_PmtAmt.Caption = "Pmt Amt"
            Me.GridColumn_PmtAmt.CustomizationCaption = "PaymentAmount"
            Me.GridColumn_PmtAmt.DisplayFormat.FormatString = "c2"
            Me.GridColumn_PmtAmt.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
            Me.GridColumn_PmtAmt.FieldName = "pmt_amount"
            Me.GridColumn_PmtAmt.GroupFormat.FormatString = "c2"
            Me.GridColumn_PmtAmt.GroupFormat.FormatType = DevExpress.Utils.FormatType.Numeric
            Me.GridColumn_PmtAmt.Name = "GridColumn_PmtAmt"
            Me.GridColumn_PmtAmt.SummaryItem.DisplayFormat = "{0:c}"
            Me.GridColumn_PmtAmt.SummaryItem.SummaryType = DevExpress.Data.SummaryItemType.Sum
            Me.GridColumn_PmtAmt.Visible = True
            Me.GridColumn_PmtAmt.VisibleIndex = 4
            '
            'GridColumn_PmtDate
            '
            Me.GridColumn_PmtDate.AppearanceCell.Options.UseTextOptions = True
            Me.GridColumn_PmtDate.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
            Me.GridColumn_PmtDate.AppearanceHeader.Options.UseTextOptions = True
            Me.GridColumn_PmtDate.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
            Me.GridColumn_PmtDate.Caption = "Pmt Date"
            Me.GridColumn_PmtDate.CustomizationCaption = "PaymentDate"
            Me.GridColumn_PmtDate.DisplayFormat.FormatString = "d"
            Me.GridColumn_PmtDate.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime
            Me.GridColumn_PmtDate.FieldName = "pmt_date"
            Me.GridColumn_PmtDate.Name = "GridColumn_PmtDate"
            '
            'GridColumn_AdjAmt
            '
            Me.GridColumn_AdjAmt.AppearanceCell.Options.UseTextOptions = True
            Me.GridColumn_AdjAmt.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
            Me.GridColumn_AdjAmt.AppearanceHeader.Options.UseTextOptions = True
            Me.GridColumn_AdjAmt.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
            Me.GridColumn_AdjAmt.Caption = "Adj Amt"
            Me.GridColumn_AdjAmt.CustomizationCaption = "AdjustmentAmount"
            Me.GridColumn_AdjAmt.DisplayFormat.FormatString = "c2"
            Me.GridColumn_AdjAmt.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
            Me.GridColumn_AdjAmt.FieldName = "adj_amount"
            Me.GridColumn_AdjAmt.GroupFormat.FormatString = "c2"
            Me.GridColumn_AdjAmt.GroupFormat.FormatType = DevExpress.Utils.FormatType.Numeric
            Me.GridColumn_AdjAmt.Name = "GridColumn_AdjAmt"
            Me.GridColumn_AdjAmt.SummaryItem.DisplayFormat = "{0:c}"
            Me.GridColumn_AdjAmt.SummaryItem.SummaryType = DevExpress.Data.SummaryItemType.Sum
            Me.GridColumn_AdjAmt.Visible = True
            Me.GridColumn_AdjAmt.VisibleIndex = 5
            '
            'GridColumn_AdjDate
            '
            Me.GridColumn_AdjDate.AppearanceCell.Options.UseTextOptions = True
            Me.GridColumn_AdjDate.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
            Me.GridColumn_AdjDate.AppearanceHeader.Options.UseTextOptions = True
            Me.GridColumn_AdjDate.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
            Me.GridColumn_AdjDate.Caption = "Adj Date"
            Me.GridColumn_AdjDate.CustomizationCaption = "AdjustmentDate"
            Me.GridColumn_AdjDate.DisplayFormat.FormatString = "d"
            Me.GridColumn_AdjDate.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime
            Me.GridColumn_AdjDate.FieldName = "adj_date"
            Me.GridColumn_AdjDate.Name = "GridColumn_AdjDate"
            '
            'GridColumn_Balance
            '
            Me.GridColumn_Balance.AppearanceCell.Options.UseTextOptions = True
            Me.GridColumn_Balance.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
            Me.GridColumn_Balance.AppearanceHeader.Options.UseTextOptions = True
            Me.GridColumn_Balance.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
            Me.GridColumn_Balance.Caption = "Balance"
            Me.GridColumn_Balance.CustomizationCaption = "Balance"
            Me.GridColumn_Balance.DisplayFormat.FormatString = "c2"
            Me.GridColumn_Balance.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
            Me.GridColumn_Balance.FieldName = "balance"
            Me.GridColumn_Balance.GroupFormat.FormatString = "c2"
            Me.GridColumn_Balance.GroupFormat.FormatType = DevExpress.Utils.FormatType.Numeric
            Me.GridColumn_Balance.Name = "GridColumn_Balance"
            Me.GridColumn_Balance.SummaryItem.DisplayFormat = "{0:c}"
            Me.GridColumn_Balance.SummaryItem.SummaryType = DevExpress.Data.SummaryItemType.Sum
            Me.GridColumn_Balance.Visible = True
            Me.GridColumn_Balance.VisibleIndex = 6
            '
            'Label_CreditorAddress
            '
            Me.Label_CreditorAddress.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                        Or System.Windows.Forms.AnchorStyles.Left) _
                        Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
            Me.Label_CreditorAddress.Location = New System.Drawing.Point(256, 8)
            Me.Label_CreditorAddress.Name = "Label_CreditorAddress"
            Me.Label_CreditorAddress.Size = New System.Drawing.Size(0, 13)
            Me.Label_CreditorAddress.TabIndex = 2
            Me.Label_CreditorAddress.ToolTipController = Me.ToolTipController1
            Me.Label_CreditorAddress.UseMnemonic = False
            '
            'Text_CreditorID
            '
            Me.Text_CreditorID.EditValue = Nothing
            Me.Text_CreditorID.Location = New System.Drawing.Point(136, 5)
            Me.Text_CreditorID.Name = "Text_CreditorID"
            Me.Text_CreditorID.Properties.Buttons.AddRange(
                New DevExpress.XtraEditors.Controls.EditorButton() {
                    New DevExpress.XtraEditors.Controls.EditorButton(
                        DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph,
                        "",
                        -1,
                        True,
                        True,
                        False,
                        DevExpress.XtraEditors.ImageLocation.MiddleCenter, CType(resources.GetObject("CreditorID1.Properties.Buttons"), System.Drawing.Image),
                        New DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None),
                        SerializableAppearanceObject1,
                        "Click here to search for a creditor ID",
                        "search",
                        Nothing,
                        True
                    )
                }
            )
            Me.Text_CreditorID.Properties.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
            Me.Text_CreditorID.Properties.Mask.AutoComplete = DevExpress.XtraEditors.Mask.AutoCompleteType.Strong
            Me.Text_CreditorID.Properties.Mask.BeepOnError = True
            Me.Text_CreditorID.Properties.Mask.EditMask = "[A-Z]{1,2}[0-9]{4,}"
            Me.Text_CreditorID.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.RegEx
            Me.Text_CreditorID.Properties.Mask.UseMaskAsDisplayFormat = True
            Me.Text_CreditorID.Properties.MaxLength = 10
            Me.Text_CreditorID.Size = New System.Drawing.Size(100, 20)
            Me.Text_CreditorID.TabIndex = 1
            '
            'Form_InvoicePayments
            '
            Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
            Me.ClientSize = New System.Drawing.Size(640, 334)
            Me.Controls.Add(Me.Text_CreditorID)
            Me.Controls.Add(Me.Label_CreditorAddress)
            Me.Controls.Add(Me.GridControl1)
            Me.Controls.Add(Me.Button_NonAR)
            Me.Controls.Add(Me.Button_PayOldest)
            Me.Controls.Add(Me.Button_Select)
            Me.Controls.Add(Me.Text_ReferenceInfo)
            Me.Controls.Add(Me.label_net_invoice_amt)
            Me.Controls.Add(Me.label_unspent_funds)
            Me.Controls.Add(Me.label_non_ar)
            Me.Controls.Add(Me.label_ar)
            Me.Controls.Add(Me.Text_Amount)
            Me.Controls.Add(Me.LabelControl7)
            Me.Controls.Add(Me.LabelControl6)
            Me.Controls.Add(Me.LabelControl5)
            Me.Controls.Add(Me.LabelControl4)
            Me.Controls.Add(Me.LabelControl3)
            Me.Controls.Add(Me.LabelControl2)
            Me.Controls.Add(Me.LabelControl1)
            Me.Name = "Form_InvoicePayments"
            Me.Text = "Invoice Payments"
            CType(Me.DxErrorProvider1, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.Text_Amount.Properties, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.Text_ReferenceInfo.Properties, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.GridControl1, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.GridView1, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.Text_CreditorID.Properties, System.ComponentModel.ISupportInitialize).EndInit()
            Me.ResumeLayout(False)
            Me.PerformLayout()

        End Sub

#End Region

        ''' <summary>
        ''' Perform a Non-A/R transaction for this creditor
        ''' </summary>
        Private Sub Button_NonAR_Click(ByVal Sender As System.Object, ByVal e As System.EventArgs)
            Dim Amount As Decimal = 0D
            Dim LedgerCode As String = String.Empty
            Dim Message As String = String.Empty

            With New Form_NonAR(unspent_funds)
                Dim answer As System.Windows.Forms.DialogResult = .ShowDialog()
                Amount = Convert.ToDecimal(.txt_contribution.EditValue)
                LedgerCode = Convert.ToString(.lst_LedgerCode.EditValue)
                Message = .MemoEdit1.Text.Trim()
                .Dispose()
                If answer <> Windows.Forms.DialogResult.OK Then Return
            End With

            ' Process the non-ar information
            Dim cn As SqlClient.SqlConnection = New SqlConnection(DebtPlus.LINQ.SQLInfoClass.getDefault().ConnectionString)
            Try
                cn.Open()

                With New SqlCommand
                    .Connection = cn
                    .CommandText = "xpr_creditor_contribution_non_ar"
                    .CommandType = CommandType.StoredProcedure
                    .Parameters.Add("@creditor", SqlDbType.VarChar, 10).Value = creditor
                    .Parameters.Add("@amount", SqlDbType.Decimal).Value = Amount
                    .Parameters.Add("@Account", SqlDbType.VarChar, 50).Value = LedgerCode
                    .Parameters.Add("@Message", SqlDbType.VarChar, 1024).Value = Message
                    .ExecuteNonQuery()
                End With

                non_ar_payments += Amount
                EnableOK()

            Catch ex As SqlClient.SqlException
                DebtPlus.UI.Common.ErrorHandling.HandleErrors(ex, "Error setting Non-AR Amount")

            Finally
                If cn IsNot Nothing Then cn.Dispose()
            End Try
        End Sub

        ''' <summary>
        ''' Pay invoices until there is no more money.
        ''' </summary>
        Private Sub Button_PayOldest_Click(ByVal Sender As System.Object, ByVal e As System.EventArgs)
            Dim FundsChosen As Decimal = 0D

            ' If there are unspent funds then we may apply it to the invoices.
            ' Ask how much is desired.
            If unspent_funds > 0D Then
                With New Form_PaidInFull(unspent_funds)
                    Dim answer As System.Windows.Forms.DialogResult = .ShowDialog()
                    FundsChosen = Convert.ToDecimal(.CalcEdit1.EditValue)
                    .Dispose()
                    If answer <> Windows.Forms.DialogResult.OK Then Return
                End With

                ' If there is an amount then call the stored procedure to process the invoices.
                If FundsChosen > 0D Then
                    Dim cn As SqlClient.SqlConnection = New SqlConnection(DebtPlus.LINQ.SQLInfoClass.getDefault().ConnectionString)
                    Try
                        cn.Open()
                        With New SqlCommand
                            .Connection = cn
                            .CommandText = "xpr_creditor_contribution_pif"
                            .CommandType = CommandType.StoredProcedure
                            .Parameters.Add("@creditor", SqlDbType.VarChar, 80).Value = creditor
                            .Parameters.Add("@amount", SqlDbType.Decimal).Value = FundsChosen
                            .Parameters.Add("@message", SqlDbType.VarChar, 256).Value = Text_ReferenceInfo.Text.Trim()
                            Dim objPaid As Object = .ExecuteScalar
                            If objPaid IsNot Nothing AndAlso objPaid IsNot System.DBNull.Value Then ar_payments += FundsChosen - Convert.ToDecimal(objPaid)
                        End With

                        ' Since the stored procedure may have changed multiple items, refresh the whole list.
                        RefreshInvoiceList()
                        EnableOK()

                    Catch ex As SqlClient.SqlException
                        DebtPlus.UI.Common.ErrorHandling.HandleErrors(ex, "Error setting Non-AR Amount")

                    Finally
                        If cn IsNot Nothing Then cn.Dispose()
                    End Try
                End If
            End If
        End Sub

#Region " Grid Control "
        Private EditRow As System.Data.DataRow

        ''' <summary>
        ''' Return the directory to the saved layout information
        ''' </summary>
        Protected Overridable Function XMLBasePath() As String
            If DesignMode Then Return String.Empty
            Dim BasePath As String = System.Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData) + System.IO.Path.DirectorySeparatorChar + "DebtPlus"
            Return System.IO.Path.Combine(BasePath, "Creditor.Billing.Payments")
        End Function

        ''' <summary>
        ''' Reload the layout of the grid control if needed
        ''' </summary>
        Protected Sub ReloadGridControlLayout()

            ' Find the base path to the saved file location
            Dim PathName As String = XMLBasePath()
            If Not String.IsNullOrEmpty(PathName) Then
                RemoveHandlers()
                Try
                    Dim FileName As String = System.IO.Path.Combine(PathName, String.Format("{0}.Grid.xml", Name))
                    If System.IO.File.Exists(FileName) Then
                        GridView1.RestoreLayoutFromXml(FileName)
                    End If

                Catch ex As System.IO.DirectoryNotFoundException
                Catch ex As System.IO.FileNotFoundException

                Finally
                    RegisterHandlers()
                End Try
            End If
        End Sub

        ''' <summary>
        ''' If the layout is changed then update the file
        ''' </summary>
        Private Sub LayoutChanged(ByVal Sender As Object, ByVal e As System.EventArgs)
            RemoveHandlers()
            Try
                Dim PathName As String = XMLBasePath()
                If Not String.IsNullOrEmpty(PathName) Then
                    If Not System.IO.Directory.Exists(PathName) Then
                        System.IO.Directory.CreateDirectory(PathName)
                    End If
                    Dim FileName As String = System.IO.Path.Combine(PathName, String.Format("{0}.Grid.xml", Name))
                    GridView1.SaveLayoutToXml(FileName)
                End If
            Finally
                RegisterHandlers()
            End Try
        End Sub

        ''' <summary>
        ''' Find the outstanding balance for the invoices
        ''' </summary>
        Private Function BalanceTotal() As Decimal
            Dim tbl As System.Data.DataTable = CType(GridControl1.DataSource, System.Data.DataTable)
            Dim answer As Object = tbl.Compute("sum(balance)", String.Empty)
            If answer Is System.DBNull.Value Then answer = 0D
            Return Convert.ToDecimal(answer)
        End Function

        ''' <summary>
        ''' Rebuild the list of pending invoices
        ''' </summary>
        Private Sub RefreshInvoiceList()
            Const TableName As String = "invoices"

            ' Clear the invoice table
            Dim tbl As System.Data.DataTable = ds.Tables(TableName)
            If tbl IsNot Nothing Then ds.Tables.Remove(tbl)

            ' Set the wait cursor to indicate that we are busy
            Dim prior_cursor As System.Windows.Forms.Cursor = System.Windows.Forms.Cursor.Current
            System.Windows.Forms.Cursor.Current = System.Windows.Forms.Cursors.WaitCursor
            Try
                Using cmd As SqlClient.SqlCommand = SelectCommand()
                    Using da As New SqlClient.SqlDataAdapter(cmd)
                        da.Fill(ds, TableName)
                    End Using
                End Using

                tbl = ds.Tables(TableName)

                If tbl IsNot Nothing Then
                    With tbl
                        'Under some rare conditions, the check number may be null. This would fail as a primary key. So, since we don't do a "find" on the rows
                        'and the table is deleted before it is reloaded, we don't need a primary key. For now, leave it off.
                        'If .PrimaryKey.Length <= 0 Then .PrimaryKey = New System.Data.DataColumn() {.Columns("invoice_register"), .Columns("checknum")}

                        If Not .Columns.Contains("balance") Then
                            With .Columns.Add("balance", GetType(Decimal), "[inv_amount]-[pmt_amount]-[adj_amount]")
                                .DefaultValue = 0D
                                .ReadOnly = True
                                .Caption = "Balance"
                            End With
                        End If
                    End With

                    With GridControl1
                        .DataSource = tbl
                        .RefreshDataSource()
                    End With

                    ' Correct the outstanding balance information
                    label_net_invoice_amt.Text = System.String.Format("{0:c}", BalanceTotal)
                End If

            Catch ex As SqlClient.SqlException
                DebtPlus.UI.Common.ErrorHandling.HandleErrors(ex, "Error reading invoice table")

            Finally
                System.Windows.Forms.Cursor.Current = prior_cursor
            End Try
        End Sub

        ''' <summary>
        ''' Generate the select command to retrieve the invoice data
        ''' </summary>
        Private Function SelectCommand() As SqlClient.SqlCommand
            Dim cmd As SqlClient.SqlCommand = New SqlCommand

            With cmd
                .Connection = New SqlConnection(DebtPlus.LINQ.SQLInfoClass.getDefault().ConnectionString)

                .CommandText = "SELECT " + _
                                "	ri.invoice_register, " + _
                                "	tr.checknum, " + _
                                "	isnull(ri.inv_amount,0) as inv_amount, ri.inv_date, " + _
                                "	isnull(ri.pmt_amount,0) as pmt_amount, ri.pmt_date, " + _
                                "	isnull(ri.adj_amount,0) as adj_amount, ri.adj_date  " + _
                                "FROM	registers_invoices ri WITH (NOLOCK) " + _
                                "RIGHT OUTER JOIN registers_creditor rc WITH (NOLOCK) ON rc.invoice_register = ri.invoice_register AND rc.tran_type IN ('AD','BW','MD','CM') " + _
                                "LEFT OUTER JOIN registers_trust tr WITH (NOLOCK) ON tr.trust_register = rc.trust_register " + _
                                "WHERE	ri.creditor=@creditor " + _
                                "AND	round(isnull(ri.inv_amount,0)-isnull(ri.adj_amount,0)-isnull(ri.pmt_amount,0), 2) > 0 " + _
                                "ORDER BY ri.inv_date, ri.invoice_register, tr.checknum"

                .CommandType = CommandType.Text
                .Parameters.Add("@creditor", SqlDbType.VarChar, 10).Value = creditor
            End With

            Return cmd
        End Function

        ''' <summary>
        ''' A row was clicked to change the focus
        ''' </summary>
        Private Sub GridView1_FocusedRowChanged(ByVal sender As Object, ByVal e As DevExpress.XtraGrid.Views.Base.FocusedRowChangedEventArgs)
            EnableOK()
        End Sub

        ''' <summary>
        ''' Double Click on an invoice -- pay it
        ''' </summary>
        Private Sub GridView1_DoubleClick(ByVal sender As Object, ByVal e As System.EventArgs)

            ' Find the row targetted as the double-click item
            Dim gv As DevExpress.XtraGrid.Views.Grid.GridView = CType(sender, DevExpress.XtraGrid.Views.Grid.GridView)
            Dim ctl As DevExpress.XtraGrid.GridControl = CType(gv.GridControl, DevExpress.XtraGrid.GridControl)
            Dim hi As DevExpress.XtraGrid.Views.Grid.ViewInfo.GridHitInfo = gv.CalcHitInfo((ctl.PointToClient(Windows.Forms.Control.MousePosition)))
            Dim ControlRow As System.Int32 = hi.RowHandle

            ' If there is a row then process the double-click event. Ignore it if there is no row.
            Dim EditRow As System.Data.DataRow = Nothing
            If ControlRow >= 0 Then
                Dim tbl As DataTable = CType(ctl.DataSource, DataTable)
                EditRow = CType(gv.GetDataRow(ControlRow), System.Data.DataRow)
            End If

            ' From the datasource, we can find the client in the table.
            If EditRow IsNot Nothing Then
                If Convert.ToDecimal(EditRow("balance")) > 0D Then
                    PayInvoice(EditRow)
                    EnableOK()
                End If
            End If
        End Sub

        ''' <summary>
        ''' Process the edit of the invoice data
        ''' </summary>
        Private Sub Button_Select_Click(ByVal sender As Object, ByVal e As System.EventArgs)
            PayInvoice(CType(GridView1.GetDataRow(GridView1.FocusedRowHandle), System.Data.DataRow))
            EnableOK()
        End Sub
#End Region

        ''' <summary>
        ''' Initialize the form
        ''' </summary>
        Private Sub Form_InvoicePayments_Load(ByVal sender As Object, ByVal e As System.EventArgs)

            ' Try to restore the layout from the registry settings.
            ReloadGridControlLayout()

            ' Disable the buttons until a creditor is entered
            EnableOK()
        End Sub

        ''' <summary>
        ''' Process a change in the check amount
        ''' </summary>
        Private Sub txt_check_amount_TextChanged(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs)
            RemoveHandlers()
            Try
                Text_ReferenceInfo.Text = System.String.Empty
                non_ar_payments = 0D
                ar_payments = 0D

            Finally
                RegisterHandlers()
            End Try
            EnableOK()
        End Sub

        ''' <summary>
        ''' Find the balance owed on an invoice
        ''' </summary>
        Private Function invoice_balance(ByVal invoice_register As System.Int32) As Decimal
            Dim answer As Decimal = 0D

            Dim cn As SqlClient.SqlConnection = New SqlConnection(DebtPlus.LINQ.SQLInfoClass.getDefault().ConnectionString)
            Try
                cn.Open()
                With New SqlCommand
                    .Connection = cn
                    .CommandText = "SELECT round(isnull(inv_amount,0)-isnull(adj_amount,0)-isnull(pmt_amount,0),2) as 'balance' FROM registers_invoices WITH (NOLOCK) WHERE invoice_register=@invoice_register"
                    .Parameters.Add("@invoice_register", SqlDbType.Int).Value = invoice_register
                    Dim objAnswer As Object = .ExecuteScalar
                    If objAnswer IsNot Nothing AndAlso objAnswer IsNot System.DBNull.Value Then answer = Convert.ToDecimal(objAnswer)
                End With

            Catch ex As SqlClient.SqlException
                DebtPlus.UI.Common.ErrorHandling.HandleErrors(ex, "Error reading invoice balance")

            Finally
                If cn IsNot Nothing Then cn.Dispose()
            End Try

            Return answer
        End Function

        ''' <summary>
        ''' Update the net_invoice amount to reflect the current total
        ''' </summary>
        Private Sub PayInvoice(ByVal row As System.Data.DataRow)
            Dim invoice_register As System.Int32 = Convert.ToInt32(row("invoice_register"))

            ' Find the balance to pay on the invoice
            Dim Balance As Decimal = invoice_balance(invoice_register)
            If Balance <= 0D Then
                DebtPlus.Data.Forms.MessageBox.Show("That invoice is paid. Please choose a different one.", "Data Entry Error", MessageBoxButtons.OK)
            Else
                ' Determine the information for the invoice
                Dim amt_paid As Decimal = 0D
                Dim amt_adj As Decimal = 0D
                With New From_ItemEdit(unspent_funds, row)
                    Dim answer As System.Windows.Forms.DialogResult = .ShowDialog()
                    amt_paid = Convert.ToDecimal(.CalcEdit5.EditValue)
                    amt_adj = Convert.ToDecimal(.CalcEdit6.EditValue)
                    .Dispose()
                    If answer <> Windows.Forms.DialogResult.OK Then Return
                End With

                ' Update the invoice with the changed information
                Dim cn As SqlClient.SqlConnection = New SqlConnection(DebtPlus.LINQ.SQLInfoClass.getDefault().ConnectionString)
                Dim txn As SqlClient.SqlTransaction = Nothing
                Dim rd As SqlClient.SqlDataReader = Nothing

                Try
                    cn.Open()
                    txn = cn.BeginTransaction

                    row.BeginEdit()

                    ' Pay on this invoice
                    If amt_paid > 0D Then
                        With New SqlCommand
                            .Connection = cn
                            .Transaction = txn
                            .CommandText = "xpr_creditor_contribution_invoice"
                            .CommandType = CommandType.StoredProcedure
                            .Parameters.Add("@invoice_register", SqlDbType.Int).Value = invoice_register
                            .Parameters.Add("@amount", SqlDbType.Decimal).Value = amt_paid
                            .Parameters.Add("@message", SqlDbType.VarChar, 80).Value = Text_ReferenceInfo.Text.Trim()
                            .ExecuteNonQuery()
                        End With

                        ' Correct the invoice parameters
                        row("pmt_amount") = Convert.ToDecimal(row("pmt_amount")) + amt_paid
                        row("pmt_date") = Now
                        ar_payments += amt_paid
                    End If

                    ' Adjust the invoice as needed
                    If amt_adj > 0D Then
                        With New SqlCommand
                            .Connection = cn
                            .Transaction = txn
                            .CommandText = "xpr_creditor_contribution_adj"
                            .CommandType = CommandType.StoredProcedure
                            .Parameters.Add("@invoice_register", SqlDbType.Int).Value = invoice_register
                            .Parameters.Add("@amount", SqlDbType.Decimal).Value = amt_adj
                            .Parameters.Add("@reference", SqlDbType.VarChar, 80).Value = Text_ReferenceInfo.Text.Trim()
                            .ExecuteNonQuery()
                        End With

                        ' Correct the invoice parameters
                        row("adj_amount") = Convert.ToDecimal(row("adj_amount")) + amt_adj
                        row("adj_date") = Now
                    End If

                    ' Commit the update of the row
                    row.EndEdit()
                    row.AcceptChanges()

                    label_net_invoice_amt.Text = System.String.Format("{0:c}", BalanceTotal())
                    GridControl1.RefreshDataSource()

                    ' Commit the changes to the database
                    txn.Commit()
                    txn.Dispose()
                    txn = Nothing

                Catch ex As SqlClient.SqlException
                    DebtPlus.UI.Common.ErrorHandling.HandleErrors(ex, "Error updating invoice information")

                Finally
                    If row IsNot Nothing Then
                        row.CancelEdit()
                    End If

                    If txn IsNot Nothing Then
                        Try
                            txn.Rollback()
                        Catch exRollback As System.Exception
                        End Try
                        txn.Dispose()
                        txn = Nothing
                    End If

                    If cn IsNot Nothing Then cn.Dispose()
                End Try
            End If
        End Sub

        ''' <summary>
        ''' Handle the condition where the creditor ID was changed
        ''' </summary>
        Private Sub Text_CreditorID_Validated(ByVal sender As Object, ByVal e As System.EventArgs)
            RemoveHandlers()
            Try
                Text_Amount.EditValue = 0D
                Text_ReferenceInfo.EditValue = Nothing
            Finally
                RegisterHandlers()
            End Try

            ReadCreditor()
            EnableOK()
        End Sub

        ''' <summary>
        ''' Fetch the creditor information from the creditor_id field
        ''' </summary>
        Private Sub ReadCreditor()

            Dim CreditorAddress As String = ReadCreditor_Address()
            If CreditorAddress = String.Empty Then
                CreditorAddress = ReadCreditor_Name()
            End If

            Label_CreditorAddress.Text = CreditorAddress
            If CreditorAddress <> String.Empty Then
                RefreshInvoiceList()

            Else

                GridControl1.DataSource = Nothing
                GridControl1.RefreshDataSource()
                GridView1.FocusedRowHandle = -1
            End If
        End Sub

        Private Function ReadCreditor_Name() As String
            Dim answer As String = String.Empty
            Dim tbl As System.Data.DataTable = ds.Tables("creditors")
            Dim row As System.Data.DataRow

            If tbl IsNot Nothing Then
                row = tbl.Rows.Find(creditor)
                If row IsNot Nothing Then
                    If Not row("creditor_name") Is System.DBNull.Value Then answer = Convert.ToString(row("creditor_name")).Trim()
                    Return answer
                End If
            End If

            Try
                ' Attempt to read the creditor from the list.
                Dim cmd As SqlClient.SqlCommand = New SqlCommand
                With cmd
                    .Connection = New SqlConnection(DebtPlus.LINQ.SQLInfoClass.getDefault().ConnectionString)
                    .CommandText = "SELECT creditor, creditor_name FROM creditors WITH (NOLOCK) WHERE creditor = @creditor"
                    .CommandType = CommandType.Text
                    .Parameters.Add("@creditor", SqlDbType.VarChar, 10).Value = creditor
                End With

                Dim da As New SqlClient.SqlDataAdapter(cmd)
                da.Fill(ds, "creditors")
                tbl = ds.Tables("creditors")
                If tbl.PrimaryKey.Length <= 0 Then tbl.PrimaryKey = New System.Data.DataColumn() {tbl.Columns("creditor")}

            Catch ex As SqlClient.SqlException
                DebtPlus.UI.Common.ErrorHandling.HandleErrors(ex, "Error reading creditors table")
            End Try

            row = tbl.Rows.Find(creditor)
            If row IsNot Nothing Then
                If Not row("creditor_name") Is System.DBNull.Value Then answer = Convert.ToString(row("creditor_name")).Trim()
            End If

            Return answer
        End Function

        Private Function ReadCreditor_Address() As String
            Dim answer As String = String.Empty
            Dim tbl As System.Data.DataTable = ds.Tables("creditor_addresses")
            Dim row As System.Data.DataRow

            ' If the entry is loaded from previous use, then reuse the information
            If tbl IsNot Nothing Then
                row = tbl.Rows.Find(creditor)
                If row IsNot Nothing Then
                    If Not row("creditor_address") Is System.DBNull.Value Then answer = Convert.ToString(row("creditor_address")).Trim()
                End If
            End If

            If answer = String.Empty Then
                Try
                    ' Attempt to read the creditor from the list.
                    Dim cmd As SqlClient.SqlCommand = New SqlCommand
                    With cmd
                        .Connection = New SqlConnection(DebtPlus.LINQ.SQLInfoClass.getDefault().ConnectionString)
                        .CommandText = "SELECT creditor, dbo.address_block(attn, addr1, addr2, addr3, addr6) as creditor_address FROM view_creditor_addresses WITH (NOLOCK) WHERE creditor = @creditor AND type = 'P'"
                        .CommandType = CommandType.Text
                        .Parameters.Add("@creditor", SqlDbType.VarChar, 10).Value = creditor
                    End With

                    Dim da As New SqlClient.SqlDataAdapter(cmd)
                    da.Fill(ds, "creditor_addresses")
                    tbl = ds.Tables("creditor_addresses")
                    If tbl.PrimaryKey.Length <= 0 Then tbl.PrimaryKey = New System.Data.DataColumn() {tbl.Columns("creditor")}

                Catch ex As SqlClient.SqlException
                    DebtPlus.UI.Common.ErrorHandling.HandleErrors(ex, "Error reading creditor addresss table")
                End Try

                row = tbl.Rows.Find(creditor)
                If row IsNot Nothing Then
                    If Not row("creditor_address") Is System.DBNull.Value Then answer = Convert.ToString(row("creditor_address")).Trim()
                End If
            End If

            Return answer
        End Function

        Private Sub Text_Amount_EditValueChanging(ByVal sender As Object, ByVal e As DevExpress.XtraEditors.Controls.ChangingEventArgs)
            e.Cancel = e.NewValue Is Nothing OrElse e.NewValue Is System.DBNull.Value OrElse Convert.ToDecimal(e.NewValue) <= 0D
        End Sub
    End Class
End Namespace
