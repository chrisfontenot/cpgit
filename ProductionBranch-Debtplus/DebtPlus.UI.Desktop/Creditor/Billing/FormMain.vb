#Region "Copyright 2000-2012 DebtPlus, L.L.C."
'{*******************************************************************}
'{                                                                   }
'{       DebtPlus Debt Management System                             }
'{                                                                   }
'{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
'{       ALL RIGHTS RESERVED                                         }
'{                                                                   }
'{   The entire contents of this file is protected by U.S. and       }
'{   International Copyright Laws. Unauthorized reproduction,        }
'{   reverse-engineering, and distribution of all or any portion of  }
'{   the code contained in this file is strictly prohibited and may  }
'{   result in severe civil and criminal penalties and will be       }
'{   prosecuted to the maximum extent possible under the law.        }
'{                                                                   }
'{   RESTRICTIONS                                                    }
'{                                                                   }
'{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
'{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
'{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
'{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
'{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
'{                                                                   }
'{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
'{   ADDITIONAL RESTRICTIONS.                                        }
'{                                                                   }
'{*******************************************************************}

#End Region

Option Explicit On
Option Strict On
Option Compare Binary

Imports System.ComponentModel
Imports System.Data.SqlClient
Imports System.Drawing.Printing
Imports System.Threading
Imports System.Text
Imports DevExpress.Utils
Imports System.Windows.Forms
Imports DevExpress.XtraReports.UI

Namespace Creditor.Billing
    Friend Class FormMain
        Private WithEvents bt As New BackgroundWorker
        Private ReadOnly bill As New Billing
        Private _checksProcessed As Int32 = 0
        Private ReadOnly ctx As CreditorBillingContext = Nothing

        Public Sub New(ByVal ctx As CreditorBillingContext)
            MyBase.New()
            Me.ctx = ctx
            InitializeComponent()

            AddHandler bt.DoWork, AddressOf bt_DoWork
            AddHandler bt.RunWorkerCompleted, AddressOf bt_RunWorkerCompleted
            AddHandler Button_Print.Click, AddressOf Button_Print_Click
            AddHandler Button_Cancel.Click, AddressOf Button_Cancel_Click
        End Sub

        ''' <summary>
        ''' Information used to talk to the billing status form
        ''' </summary>
        Friend Class Billing
            Public Cancelled As Boolean = False

            ' Status for the various groups of creditors
            Public Check_Annually As Boolean
            Public Check_SemiAnnually As Boolean
            Public Check_Quarterly As Boolean
            Public Check_Monthly As Boolean
            Public Check_Weekly As Boolean
            Public Check_Ignore As Boolean

            Public Period_Annually As String
            Public Period_SemiAnnually As String
            Public Period_Quarterly As String
            Public Period_Monthly As String
            Public Period_Weekly As String

            Private privateStatusForm As FormStatus = Nothing
            Private privateInvoiceRegister As Int32 = 0
            Private privateBilled As Decimal = 0D
            Private privateLastCreditor As String = String.Empty

            Public ReadOnly Property StatusForm() As FormStatus
                Get
                    If privateStatusForm Is Nothing OrElse privateStatusForm.IsDisposed Then
                        privateStatusForm = New FormStatus(Me)
                        privateStatusForm.Show()
                    End If
                    Return privateStatusForm
                End Get
            End Property

            Friend Property InvoiceRegister() As Int32
                Get
                    Return privateInvoiceRegister
                End Get
                Set(ByVal Value As Int32)
                    privateInvoiceRegister = Value
                    SetLabelInvoice()
                End Set
            End Property

            Private Sub SetLabelInvoice()
                If StatusForm.InvokeRequired Then
                    StatusForm.Invoke(New MethodInvoker(AddressOf SetLabelInvoice))
                Else
                    StatusForm.Label_Invoice.Text = String.Format("{0:d8}", InvoiceRegister)
                End If
            End Sub

            Friend Property BilledAmount() As Decimal
                Get
                    Return privateBilled
                End Get
                Set(ByVal Value As Decimal)
                    privateBilled = Value
                    SetLabelAmount()
                End Set
            End Property

            Private Sub SetLabelAmount()
                If StatusForm.InvokeRequired Then
                    StatusForm.Invoke(New MethodInvoker(AddressOf SetLabelAmount))
                Else
                    StatusForm.Label_Amount.Text = String.Format("{0:c}", BilledAmount)
                End If
            End Sub

            Friend Property LastCreditor() As String
                Get
                    Return privateLastCreditor
                End Get
                Set(ByVal Value As String)
                    privateLastCreditor = Value
                    SetLastCreditor()
                End Set
            End Property

            Private Sub SetLastCreditor()
                If StatusForm.InvokeRequired Then
                    StatusForm.Invoke(New MethodInvoker(AddressOf SetLastCreditor))
                Else
                    StatusForm.Label_Creditor.Text = LastCreditor
                End If
            End Sub
        End Class

        ''' <summary>
        ''' Handle the GO button to start the operation
        ''' </summary>
        Private Sub Button_Print_Click(ByVal sender As Object, ByVal e As EventArgs)
            Button_Print.Enabled = False
            UseWaitCursor = True

            ' Create the billing class and supply the members
            With bill
                .Check_Annually = (Check_Annual.CheckState <> CheckState.Unchecked)
                .Check_SemiAnnually = (Check_SemiAnnual.CheckState <> CheckState.Unchecked)
                .Check_Quarterly = (Check_Quarterly.CheckState <> CheckState.Unchecked)
                .Check_Monthly = (Check_Monthly.CheckState <> CheckState.Unchecked)
                .Check_Weekly = (Check_Weekly.CheckState <> CheckState.Unchecked)
                .Check_Ignore = (Check_Ignore.CheckState <> CheckState.Unchecked)

                .Period_Annually = Period_Annually.Text.Trim()
                .Period_SemiAnnually = Period_SemiAnnually.Text.Trim()
                .Period_Quarterly = Period_Quarterly.Text.Trim()
                .Period_Monthly = Period_Monthly.Text.Trim()
                .Period_Weekly = Period_Weekly.Text.Trim()

                ' Load the status form in the main thread
                .StatusForm.Show()
            End With

            ' Start the backgroun thread to process the generation logic
            bt.RunWorkerAsync(bill)
        End Sub

        ''' <summary>
        ''' The invoices have been created.
        ''' </summary>
        Private Sub bt_RunWorkerCompleted(ByVal sender As Object, ByVal e As RunWorkerCompletedEventArgs)
            bill.StatusForm.Close()
            UseWaitCursor = False
            Close()
        End Sub

        ''' <summary>
        ''' Background thread to do the work of creating invoices
        ''' </summary>
        Private Sub bt_DoWork(ByVal sender As Object, ByVal e As DoWorkEventArgs)

            ' Set the pointer to the billing class
            Dim bill As Billing = CType(e.Argument, Billing)

            ' Indicate that the operation has not been cancelled
            e.Result = False

            Do
                ' Look for some form of invoice generation desired.
                If bill.Check_Annually Then Exit Do
                If bill.Check_SemiAnnually Then Exit Do
                If bill.Check_Quarterly Then Exit Do
                If bill.Check_Monthly Then Exit Do
                If bill.Check_Weekly Then Exit Do

                ' Generate the report only
                With New Thread(AddressOf PrintInvoices_Thread)
                    .SetApartmentState(ApartmentState.STA)
                    .Name = "PrintInvoices_Thread"
                    .Start()
                End With
                Return
            Loop

            ' Generate a database connection to process the outer objects
            Dim cn As SqlConnection = New SqlConnection(DebtPlus.LINQ.SQLInfoClass.getDefault().ConnectionString)
            Try
                cn.Open()
                If Create_Invoice_list(cn, Nothing) Then Process_Creditor_List(cn, Nothing)

                cn.Dispose()
                cn = Nothing

                ' Print the report operation if it is not cancelled
                If Not bill.Cancelled Then
                    With New Thread(AddressOf PrintInvoices_Thread)
                        .SetApartmentState(ApartmentState.STA)
                        .Name = "PrintInvoices_Thread"
                        .Start()
                    End With
                End If

            Finally
                If cn IsNot Nothing Then cn.Dispose()
            End Try
        End Sub

        ''' <summary>
        ''' Stop the program if the CANCEL button is pressed
        ''' </summary>
        Private Sub Button_Cancel_Click(ByVal sender As Object, ByVal e As EventArgs)
            Close()
        End Sub

        ''' <summary>
        ''' Allocate a new invoice number from the system
        ''' </summary>
        Private Function new_invoice(ByRef cn As SqlConnection, ByRef txn As SqlTransaction, ByVal creditor As String) As Int32
            Dim result As Int32 = 0

            ' Build the command block to create the invoice
            Using cmd As SqlCommand = New SqlCommand
                With cmd
                    .Connection = cn
                    .Transaction = txn
                    .CommandText = "xpr_insert_registers_invoice"
                    .CommandType = CommandType.StoredProcedure

                    ' Include the parameters for the request
                    .Parameters.Add("RETURN", SqlDbType.Int).Direction = ParameterDirection.ReturnValue
                    .Parameters.Add("@creditor", SqlDbType.VarChar, 10).Value = creditor
                    .Parameters.Add("@inv_amount", SqlDbType.Decimal).Value = 0D
                    .Parameters.Add("@adj_amount", SqlDbType.Decimal).Value = 0D
                    .Parameters.Add("@pmt_amount", SqlDbType.Decimal).Value = 0D

                    ' Execute the request
                    .ExecuteNonQuery()
                    Dim obj As Object = .Parameters(0).Value
                    If obj IsNot Nothing Then result = Convert.ToInt32(obj)
                End With
            End Using

            Return result
        End Function

        ''' <summary>
        ''' Update the invoice information for later printing
        ''' </summary>
        Private Sub CloseInvoice(ByRef cn As SqlConnection, ByRef txn As SqlTransaction, ByVal InvoiceRegister As Int32)

            ' Build the command block to complete the invoice
            Dim strSql As String = "UPDATE registers_invoices SET inv_amount=@inv_amount,inv_date=getdate() WHERE invoice_register=@invoice_register"
            Using cmd As SqlCommand = New SqlCommand
                With cmd
                    .Connection = cn
                    .Transaction = txn
                    .CommandText = strSql

                    ' Include the parameters for the request
                    .Parameters.Add("@inv_amount", SqlDbType.Decimal).Value = bill.BilledAmount
                    .Parameters.Add("@invoice_register", SqlDbType.Int).Value = bill.InvoiceRegister

                    ' Update the invoice
                    .ExecuteNonQuery()
                End With
            End Using

            ' Update the mtd statistics for the creditor
            strSql = "UPDATE creditors SET contrib_mtd_billed=contrib_mtd_billed + @billed_amount WHERE creditor=@creditor"

            ' Build the command block
            Using cmd As SqlCommand = New SqlCommand
                With cmd
                    .Connection = cn
                    .Transaction = txn
                    .CommandText = strSql

                    ' Include the parameters for the request
                    .Parameters.Add("@billed_amount", SqlDbType.Decimal).Value = bill.BilledAmount
                    .Parameters.Add("@creditor", SqlDbType.VarChar, 10).Value = bill.LastCreditor

                    ' Update the invoice
                    .ExecuteNonQuery()
                End With
            End Using
        End Sub

        ''' <summary>
        ''' Create a table showing the creditors with pending invoices
        ''' </summary>
        Private rd As SqlDataReader = Nothing

        Public Sub Process_Creditor_List(ByVal main_cn As SqlConnection, ByVal main_txn As SqlTransaction)

            ' Open a new connection to process the items for the invoices
            Dim cn As SqlConnection = New SqlConnection(DebtPlus.LINQ.SQLInfoClass.getDefault().ConnectionString)
            Try
                cn.Open()

                ' Allocate a cursor to process the transaction
                Dim commandString As New StringBuilder

                commandString.Append("SELECT isnull(d.creditor_register,0) as CreditorRegister, isnull(d.client_creditor_register,0) as DebtRegister, isnull(d.creditor,'') as Creditor, isnull(d.fairshare_amt,0) as FairshareAmt, isnull(d.trust_register,0) as TrustRegister, isnull(t.ChecksPerInvoice,0) as ChecksPerInvoice")
                commandString.Append(" FROM registers_client_creditor d")
                commandString.Append(" INNER JOIN #invoice_list i ON d.creditor = i.creditor AND d.creditor_type in ('B', i.creditor_type)")
                commandString.Append(" INNER JOIN #creditor_list t ON d.creditor = t.creditor")
                commandString.Append(" WHERE d.tran_type in ('AD', 'BW', 'MD', 'CM')")
                commandString.Append(" AND d.invoice_register IS NULL")
                commandString.Append(" AND d.fairshare_amt > 0")
                commandString.Append(" AND d.void = 0")
                commandString.Append(" ORDER BY d.creditor, d.trust_register;")

                Using cmd As SqlCommand = New SqlCommand
                    With cmd
                        .CommandTimeout = 0
                        .Connection = main_cn
                        .Transaction = main_txn

                        .CommandText = commandString.ToString()

                        rd = .ExecuteReader(CommandBehavior.SingleResult Or CommandBehavior.CloseConnection)
                    End With
                End Using

                ' Process all invoices, one at a time until we have no more to generate
                If rd.Read Then
                    Do
                        If Not ProcessCurrentInvoice(cn) Then Exit Do
                    Loop
                End If

            Finally
                If cn IsNot Nothing Then cn.Dispose()
            End Try
        End Sub

        ''' <summary>
        ''' Process a single creditor from the list of creditors
        ''' </summary>
        Private Function ProcessCurrentInvoice(ByVal cn As SqlConnection) As Boolean
            Dim answer As Boolean = True

            ' Input parameters
            Dim LastCreditorRegister As Int32

            ' Parameters for the current invoice
            Dim LastTrustRegister As Int32
            Dim CommandString As String

            ' Pending transaction
            Dim txn As SqlTransaction = Nothing

            ' Process the records until we need to start a new invoice
            Dim Creditor As String = rd.GetString(rd.GetOrdinal("Creditor"))
            Dim TrustRegister As Int32 = Convert.ToInt32(rd.GetValue(rd.GetOrdinal("TrustRegister")))
            Dim ChecksPerInvoice As Int32 = Convert.ToInt32(rd.GetValue(rd.GetOrdinal("ChecksPerInvoice")))

            ' Start a transaction for the operations
            Try
                txn = cn.BeginTransaction

                ' Allocate a new invoice for the operation
                bill.InvoiceRegister = new_invoice(cn, txn, Creditor)
                If bill.InvoiceRegister <= 0 Then bill.Cancelled = True

                ' Save the transaction information
                bill.LastCreditor = Creditor
                bill.BilledAmount = 0D
                LastTrustRegister = 0
                LastCreditorRegister = 0
                _checksProcessed = 0

                Do
                    ' Retrieve the current parameters from the recordset. Stop when the creditor changes.
                    Creditor = rd.GetString(rd.GetOrdinal("Creditor"))
                    If String.Compare(Creditor, bill.LastCreditor, True) <> 0 Then Exit Do

                    ' Process the details for this invoice
                    TrustRegister = Convert.ToInt32(rd.GetValue(rd.GetOrdinal("TrustRegister")))
                    Dim DebtRegister As Int32 = Convert.ToInt32(rd.GetValue(rd.GetOrdinal("DebtRegister")))
                    Dim CreditorRegister As Int32 = Convert.ToInt32(rd.GetValue(rd.GetOrdinal("CreditorRegister")))
                    Dim FairshareAmt As Decimal = rd.GetDecimal(rd.GetOrdinal("FairshareAmt"))

                    Application.DoEvents()
                    If bill.Cancelled Then
                        Try
                            txn.Rollback()
                        Catch exRollback As Exception
                            DebtPlus.Svc.Common.ErrorHandling.HandleErrors(exRollback)
                        End Try

                        txn.Dispose()
                        txn = Nothing
                        Return False
                    End If

                    ' If the trust register is different then count the checks.
                    If LastTrustRegister <> TrustRegister Then
                        _checksProcessed += 1
                        If ChecksPerInvoice > 0 AndAlso ChecksPerInvoice < _checksProcessed Then Exit Do
                        LastTrustRegister = TrustRegister

                        ' Set the invoice pointer in the trust register.
                        CommandString = "UPDATE registers_trust SET invoice_register=@invoice_register WHERE trust_register=@trust_register"
                        Using cmd As SqlCommand = New SqlCommand
                            With cmd
                                .Connection = cn
                                .Transaction = txn
                                .CommandText = CommandString
                                .CommandType = CommandType.Text
                                .Parameters.Add("@invoice_register", SqlDbType.Int).Value = bill.InvoiceRegister
                                .Parameters.Add("@trust_register", SqlDbType.Int).Value = LastTrustRegister
                                .ExecuteNonQuery()
                            End With
                        End Using
                    End If

                    ' Update the invoice register in the creditor register for this invoice
                    If CreditorRegister > 0 AndAlso CreditorRegister <> LastCreditorRegister Then
                        LastCreditorRegister = CreditorRegister
                        CommandString = "UPDATE registers_creditor SET invoice_register=@invoice_register WHERE creditor_register=@creditor_register"
                        Using cmd As SqlCommand = New SqlCommand
                            With cmd
                                .Connection = cn
                                .Transaction = txn
                                .CommandText = CommandString
                                .CommandType = CommandType.Text
                                .Parameters.Add("@invoice_register", SqlDbType.Int).Value = bill.InvoiceRegister
                                .Parameters.Add("@creditor_register", SqlDbType.Int).Value = LastCreditorRegister
                                .ExecuteNonQuery()
                            End With
                        End Using
                    End If

                    ' Set the invoice register into the transaction. This will mark the transaction as being processed
                    CommandString = "UPDATE registers_client_creditor SET invoice_register=@invoice_register WHERE client_creditor_register=@client_creditor_register"
                    Using cmd As SqlCommand = New SqlCommand
                        With cmd
                            .Connection = cn
                            .Transaction = txn
                            .CommandText = CommandString
                            .CommandType = CommandType.Text
                            .Parameters.Add("@invoice_register", SqlDbType.Int).Value = bill.InvoiceRegister
                            .Parameters.Add("@client_creditor_register", SqlDbType.Int).Value = DebtRegister
                            .ExecuteNonQuery()
                        End With
                    End Using

                    ' Add to the total invoice amount the figure for the current transaction.
                    bill.BilledAmount += FairshareAmt

                    ' Read the next detail record. At the end, stop the loop
                    If Not rd.Read Then
                        answer = False
                        Exit Do
                    End If
                Loop

                ' Close out the current invoice. If we stopped in middle processing, then
                ' the next input record is left as the starting values for the new invoice.
                CloseInvoice(cn, txn, bill.InvoiceRegister)
                txn.Commit()
                txn.Dispose()
                txn = Nothing

            Finally
                If txn IsNot Nothing Then
                    txn.Rollback()
                    txn.Dispose()
                End If
            End Try

            Return answer
        End Function

        ''' <summary>
        ''' Do any followup cleanup operations
        ''' </summary>
        Public Sub Cleanup_Processing(ByVal cn As SqlConnection, ByVal txn As SqlTransaction)
            Try
                Using cmd As SqlCommand = New SqlCommand
                    With cmd
                        .Connection = cn
                        .Transaction = txn

                        .CommandText = "DROP TABLE #creditor_list"
                        .ExecuteNonQuery()
                    End With
                End Using

            Catch ex As SqlException
                DebtPlus.Svc.Common.ErrorHandling.HandleErrors(ex)  ' we don't care about these erorrs
            End Try

            Try
                Using cmd As SqlCommand = New SqlCommand
                    With cmd
                        .Connection = cn
                        .Transaction = txn
                        .CommandText = "DROP TABLE #invoice_list"
                        .ExecuteNonQuery()
                    End With
                End Using

            Catch ex As SqlException
                DebtPlus.Svc.Common.ErrorHandling.HandleErrors(ex)  ' we don't care about these erorrs
            End Try
        End Sub

        ''' <summary>
        ''' Create the list of creditors for billing
        ''' </summary>
        Public Sub CreateBillingCreditors(ByVal cn As SqlConnection, ByVal txn As SqlTransaction)

            Using cmd As SqlCommand = New SqlCommand
                With cmd
                    .Connection = cn
                    .Transaction = txn
                    .CommandText = "CREATE TABLE #creditor_list (creditor varchar(10), contrib_bill_month int, ChecksPerInvoice int, min_accept_per_bill Money, special_processing int);"
                    .ExecuteNonQuery()
                End With
            End Using

            Using cmd As SqlCommand = New SqlCommand
                With cmd
                    .Connection = cn
                    .Transaction = txn
                    .CommandText = "CREATE UNIQUE CLUSTERED INDEX ix2_t_billing_creditors on #creditor_list ( creditor );"
                    .ExecuteNonQuery()
                End With
            End Using
        End Sub

        ''' <summary>
        ''' Create the list of creditor invoices that are outstanding
        ''' </summary>
        Public Sub CreateBillingCounts(ByVal cn As SqlConnection, ByVal txn As SqlTransaction)

            Using cmd As SqlCommand = New SqlCommand
                With cmd
                    .Connection = cn
                    .Transaction = txn
                    .CommandText = "create table #invoice_list ( creditor varchar(10), creditor_type varchar(1), outstanding_bills money );"
                    .ExecuteNonQuery()
                End With
            End Using

            ' The first index is the creditor and type
            Using cmd As SqlCommand = New SqlCommand
                With cmd
                    .Connection = cn
                    .Transaction = txn
                    .CommandText = "create unique clustered index ix2_t_billing_counts on #invoice_list ( creditor, creditor_type );"
                    .ExecuteNonQuery()
                End With
            End Using

            ' Augment this with an index for the creditor only
            Using cmd As SqlCommand = New SqlCommand
                With cmd
                    .Connection = cn
                    .Transaction = txn
                    .CommandText = "create index ix3_t_billing_counts on #invoice_list ( creditor );"
                    .ExecuteNonQuery()
                End With
            End Using
        End Sub

        ''' <summary>
        ''' Toss creditors from the list when they are not on the proper cycle
        ''' </summary>
        Private Sub RemoveCreditors(ByVal cn As SqlConnection, ByVal txn As SqlTransaction, ByVal CreditorType As String, ByVal chk As Boolean, ByVal txt As String)
            Dim CommandString As String

            If chk Then

                ' If a period is given then delete all creditors which are not in the desired period
                If txt <> String.Empty Then
                    Dim ContribBillMonth As Int32 = Convert.ToInt32(txt)

                    If ContribBillMonth > 0 Then
                        CommandString = "DELETE #invoice_list FROM #invoice_list i INNER JOIN #creditor_list cr ON cr.creditor = i.creditor WHERE cr.contrib_bill_month <> @contrib_bill_month AND i.creditor_type = @creditor_type;"
                        Using cmd As SqlCommand = New SqlCommand
                            With cmd
                                .Connection = cn
                                .Transaction = txn
                                .CommandText = CommandString
                                .Parameters.Add("@contrib_bill_month", SqlDbType.Int).Value = ContribBillMonth
                                .Parameters.Add("@creditor_type", SqlDbType.VarChar, 1).Value = CreditorType
                                .ExecuteNonQuery()
                            End With
                        End Using
                    End If
                End If

            Else

                ' The creditor type is not to be processed. Ignore the invoices for this creditor
                CommandString = "DELETE FROM #invoice_list WHERE creditor_type=@creditor_type;"
                Using cmd As SqlCommand = New SqlCommand
                    With cmd
                        .Connection = cn
                        .Transaction = txn
                        .CommandText = CommandString
                        .Parameters.Add("@creditor_type", SqlDbType.VarChar, 1).Value = CreditorType
                        .ExecuteNonQuery()
                    End With
                End Using
            End If
        End Sub

        ''' <summary>
        ''' Insert the creditors for later processing
        ''' </summary>
        Private Sub InsertCreditors(ByVal cn As SqlConnection, ByVal txn As SqlTransaction)

            ' Insert the creditor into the list of possible creditors
            Dim CommandString As New StringBuilder

            CommandString.Append("INSERT INTO #creditor_list (creditor, contrib_bill_month, ChecksPerInvoice, min_accept_per_bill, special_processing)")
            CommandString.Append(" SELECT cr.creditor as 'creditor', cr.contrib_bill_month,")
            CommandString.Append(" coalesce(cr.chks_per_invoice,cnf.chks_per_invoice,1) as 'ChecksPerInvoice',")
            CommandString.Append(" isnull(cr.min_accept_per_bill,0) as 'min_accept_per_bill',")
            CommandString.Append(" 0 as 'special_processing'")
            CommandString.Append(" FROM creditors cr WITH (NOLOCK)")
            CommandString.Append(" CROSS JOIN config cnf WITH (NOLOCK)")

            Using cmd As SqlCommand = New SqlCommand
                With cmd
                    .Connection = cn
                    .Transaction = txn
                    .CommandTimeout = 0
                    .CommandText = CommandString.ToString()
                    .ExecuteNonQuery()
                End With
            End Using
        End Sub

        ''' <summary>
        ''' Insert the invoices for later processing
        ''' </summary>
        Private Sub InsertInvoices(ByVal cn As SqlConnection, ByVal txn As SqlTransaction)

            Using cmd As SqlCommand = New SqlCommand
                Dim CommandString As New StringBuilder

                ' Find the list of creditors that have outstanding invoices for the various billing periods
                CommandString.Append("INSERT INTO #invoice_list (creditor, creditor_type, outstanding_bills)")
                CommandString.Append(" SELECT d.creditor, creditor_type, SUM(d.fairshare_amt) AS 'outstanding_bills'")
                CommandString.Append(" FROM registers_client_creditor d")
                CommandString.Append(" WHERE d.tran_type in ('AD', 'BW', 'MD', 'CM')")
                CommandString.Append(" AND d.invoice_register IS NULL")
                CommandString.Append(" AND d.creditor_type NOT IN ('D','N')")
                CommandString.Append(" AND d.fairshare_amt > 0")
                CommandString.Append(" AND d.void = 0")
                CommandString.Append(" GROUP BY creditor, creditor_type;")

                With cmd
                    .Connection = cn
                    .Transaction = txn
                    .CommandTimeout = 0
                    .CommandText = CommandString.ToString()
                    .ExecuteNonQuery()
                End With
            End Using

            ' Correct the old "B" creditor types to be the billing period.
            Using cmd As SqlCommand = New SqlCommand
                Dim CommandString As New StringBuilder

                ' Find the list of creditors that have outstanding invoices for the various billing periods
                CommandString.Append("UPDATE #invoice_list ")
                CommandString.Append("SET [creditor_type] = cr.[contrib_cycle] ")
                CommandString.Append("FROM #invoice_list i ")
                CommandString.Append("INNER JOIN [creditors] cr ON i.[creditor] = cr.[creditor] ")
                CommandString.Append("WHERE i.[creditor_type] = 'B'")

                With cmd
                    .Connection = cn
                    .Transaction = txn
                    .CommandTimeout = 0
                    .CommandText = CommandString.ToString()
                    .ExecuteNonQuery()
                End With
            End Using
        End Sub

        ''' <summary>
        ''' Create a list of invoices
        ''' </summary>
        Public Function Create_Invoice_list(ByVal cn As SqlConnection, ByVal txn As SqlTransaction) As Boolean
            Dim answer As Boolean = False
            Dim lRecords As Int32 = 0
            Dim strSQL As String

            ' Indicate that we are busy
            Dim CurrentCursor As Cursor = Cursor.Current
            Try
                Cursor.Current = Cursors.WaitCursor

                ' Create the two working tables
                CreateBillingCreditors(cn, txn)
                CreateBillingCounts(cn, txn)

                ' Load the list of creditors and the invoices
                InsertCreditors(cn, txn)
                InsertInvoices(cn, txn)

                ' Remove the creditors that we don't want to invoice at this time.
                RemoveCreditors(cn, txn, "A", bill.Check_Annually, bill.Period_Annually.Trim())
                RemoveCreditors(cn, txn, "S", bill.Check_SemiAnnually, bill.Period_SemiAnnually.Trim())
                RemoveCreditors(cn, txn, "Q", bill.Check_Quarterly, bill.Period_Quarterly.Trim())
                RemoveCreditors(cn, txn, "M", bill.Check_Monthly, bill.Period_Monthly.Trim())
                RemoveCreditors(cn, txn, "W", bill.Check_Weekly, bill.Period_Weekly.Trim())

                ' Discard the creditors that are too small for an invoice
                If Not bill.Check_Ignore Then
                    strSQL = "DELETE #invoice_list FROM #invoice_list i INNER JOIN #creditor_list cr ON i.creditor = cr.creditor WHERE IsNull(cr.min_accept_per_bill, 0) > 0 AND IsNull(cr.min_accept_per_bill,0) > IsNull(i.outstanding_bills,0)"
                    Using cmd As SqlCommand = New SqlCommand
                        With cmd
                            .Connection = cn
                            .Transaction = txn
                            .CommandText = strSQL
                            .ExecuteNonQuery()
                        End With
                    End Using
                End If

                ' Complete the processing with the cleanup exception handling
                Try
                    Using cmd As SqlCommand = New SqlCommand
                        With cmd
                            .Connection = cn
                            .Transaction = txn
                            .CommandText = "xpr_creditor_invoice_special"
                            .CommandType = CommandType.StoredProcedure
                            .ExecuteNonQuery()
                        End With
                    End Using

                Catch ex As SqlException
                    DebtPlus.Svc.Common.ErrorHandling.HandleErrors(ex) ' Do nothing with the exception. I want to ignore any error here.
                End Try

                ' Ensure that there are some records to be processed
                strSQL = "SELECT count(*) FROM #invoice_list"
                Using cmd As SqlCommand = New SqlCommand
                    With cmd
                        .Connection = cn
                        .Transaction = txn
                        .CommandText = strSQL
                        Dim objRecords As Object = .ExecuteScalar()

                        lRecords = 0
                        If objRecords IsNot Nothing AndAlso objRecords IsNot DBNull.Value Then lRecords = Convert.ToInt32(objRecords)
                    End With
                End Using

            Finally
                Cursor.Current = CurrentCursor

            End Try

            If lRecords < 1 Then
                DebtPlus.Data.Forms.MessageBox.Show("There are no creditors which match the criteria for billing. The existing bills will still be printed.", "Database Information", MessageBoxButtons.OK, MessageBoxIcon.Information, MessageBoxDefaultButton.Button1)
            Else
                answer = True
            End If

            Return answer
        End Function

        ''' <summary>
        ''' Print the invoice reports
        ''' </summary>
        Private ds As DataSet

        Private Sub PrintInvoices_Thread()
            Using rpt As New CreditorInvoiceReport()
                ds = New DataSet("ds")
                Try
                    AddHandler rpt.BeforePrint, AddressOf ReportBeforePrint
                    rpt.DisplayPreviewDialog()
                    RemoveHandler rpt.BeforePrint, AddressOf ReportBeforePrint
                Finally
                    ds.Dispose()
                End Try
            End Using
        End Sub

        Private Sub ReportBeforePrint(ByVal sender As Object, ByVal e As PrintEventArgs)
            Dim rpt As CreditorInvoiceReport = CType(sender, CreditorInvoiceReport)

            ' Process the list of creditors that have pending invoices
            Using dlg As New WaitDialogForm("Reading creditor list", "Printing Invoices")
                dlg.ShowInTaskbar = True
                dlg.Show()

                Using cmd As SqlCommand = New SqlCommand
                    With cmd
                        .Connection = New SqlConnection(DebtPlus.LINQ.SQLInfoClass.getDefault().ConnectionString)

                        ' Generate a list of the creditors, in zipcode order, that have invoices.
                        Dim cmdText As New StringBuilder
                        cmdText.Append("SELECT distinct ri.creditor, coalesce(left(a1.PostalCode,5), left(a2.PostalCode,5), '99999') as PostalCode2 ")
                        cmdText.Append("FROM registers_invoices ri ")
                        cmdText.Append("INNER JOIN creditors cr WITH (NOLOCK) ON ri.creditor = cr.creditor ")
                        cmdText.Append("LEFT OUTER JOIN creditor_addresses ca1 WITH (NOLOCK) ON cr.creditor = ca1.creditor AND 'I' = ca1.type ")
                        cmdText.Append("LEFT OUTER JOIN addresses a1 WITH (NOLOCK) ON ca1.AddressID = a1.Address ")
                        cmdText.Append("LEFT OUTER JOIN creditor_addresses ca2 WITH (NOLOCK) ON cr.creditor = ca2.creditor AND 'P' = ca2.type ")
                        cmdText.Append("LEFT OUTER JOIN addresses a2 WITH (NOLOCK) ON ca2.AddressID = a2.Address ")
                        cmdText.Append("WHERE isnull(ri.inv_amount,0) > isnull(ri.pmt_amount,0) + isnull(ri.adj_amount,0) AND isnull(cr.suppress_invoice,0) = 0 ")
                        cmdText.Append("ORDER BY 2, 1")

                        .CommandText = cmdText.ToString()
                        .CommandType = CommandType.Text
                    End With

                    Using da As New SqlDataAdapter(cmd)
                        da.Fill(ds, "creditors")
                    End Using
                End Using

                rpt.DataSource = ds.Tables("creditors")
                dlg.Close()
            End Using
        End Sub
    End Class
End Namespace
