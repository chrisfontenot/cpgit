﻿Namespace Creditor.Billing

    Partial Class FormMain
        Inherits DebtPlus.Data.Forms.DebtPlusForm

        'Form overrides dispose to clean up the component list.
        Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
            If disposing Then
                If components IsNot Nothing Then
                    components.Dispose()
                End If
            End If
            MyBase.Dispose(disposing)
        End Sub

        'Required by the Windows Form Designer
        Private components As System.ComponentModel.IContainer

        'NOTE: The following procedure is required by the Windows Form Designer
        'It can be modified using the Windows Form Designer.  
        'Do not modify it using the code editor.
        <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
            Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(FormMain))
            Me.Check_Annual = New DevExpress.XtraEditors.CheckEdit
            Me.Check_SemiAnnual = New DevExpress.XtraEditors.CheckEdit
            Me.Check_Quarterly = New DevExpress.XtraEditors.CheckEdit
            Me.Check_Monthly = New DevExpress.XtraEditors.CheckEdit
            Me.Check_Weekly = New DevExpress.XtraEditors.CheckEdit
            Me.Check_Ignore = New DevExpress.XtraEditors.CheckEdit
            Me.Period_Annually = New DevExpress.XtraEditors.TextEdit
            Me.Period_SemiAnnually = New DevExpress.XtraEditors.TextEdit
            Me.Period_Quarterly = New DevExpress.XtraEditors.TextEdit
            Me.Period_Monthly = New DevExpress.XtraEditors.TextEdit
            Me.Period_Weekly = New DevExpress.XtraEditors.TextEdit
            Me.Label1 = New DevExpress.XtraEditors.LabelControl
            Me.GroupBox1 = New System.Windows.Forms.GroupBox
            Me.GroupBox2 = New System.Windows.Forms.GroupBox
            Me.Label3 = New DevExpress.XtraEditors.LabelControl
            Me.Button_Print = New DevExpress.XtraEditors.SimpleButton
            Me.Button_Cancel = New DevExpress.XtraEditors.SimpleButton

            CType(Me.Check_Annual.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.Check_SemiAnnual.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.Check_Quarterly.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.Check_Monthly.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.Check_Weekly.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.Check_Ignore.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.Period_Annually.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.Period_SemiAnnually.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.Period_Quarterly.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.Period_Monthly.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.Period_Weekly.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
            Me.SuspendLayout()

            '
            'Check_Annual
            '
            Me.Check_Annual.Location = New System.Drawing.Point(16, 52)
            Me.Check_Annual.Name = "Check_Annual"
            Me.Check_Annual.Properties.Appearance.BackColor = System.Drawing.Color.Transparent
            Me.Check_Annual.Properties.Appearance.ForeColor = System.Drawing.SystemColors.WindowText
            Me.Check_Annual.Properties.Appearance.Options.UseBackColor = True
            Me.Check_Annual.Properties.Appearance.Options.UseForeColor = True
            Me.Check_Annual.Properties.Caption = "Generate invoices for creditors billed annually"
            Me.Check_Annual.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Style5
            Me.Check_Annual.Size = New System.Drawing.Size(328, 22)

            Me.Check_Annual.TabIndex = 0
            Me.Check_Annual.ToolTip = "Generate creditor invoices for transactions that are billed ANNUALLY"
            Me.Check_Annual.ToolTipController = Me.ToolTipController1
            '
            'Check_SemiAnnual
            '
            Me.Check_SemiAnnual.Location = New System.Drawing.Point(16, 78)
            Me.Check_SemiAnnual.Name = "Check_SemiAnnual"
            Me.Check_SemiAnnual.Properties.Appearance.BackColor = System.Drawing.Color.Transparent
            Me.Check_SemiAnnual.Properties.Appearance.ForeColor = System.Drawing.SystemColors.WindowText
            Me.Check_SemiAnnual.Properties.Appearance.Options.UseBackColor = True
            Me.Check_SemiAnnual.Properties.Appearance.Options.UseForeColor = True
            Me.Check_SemiAnnual.Properties.Caption = "Generate invoices for creditors billed semi-annually"
            Me.Check_SemiAnnual.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Style5
            Me.Check_SemiAnnual.Size = New System.Drawing.Size(328, 22)

            Me.Check_SemiAnnual.TabIndex = 1
            Me.Check_SemiAnnual.ToolTip = "Generate creditor invoices for transactions that are billed SEMI-ANNUALLY"
            Me.Check_SemiAnnual.ToolTipController = Me.ToolTipController1
            '
            'Check_Quarterly
            '
            Me.Check_Quarterly.Location = New System.Drawing.Point(16, 103)
            Me.Check_Quarterly.Name = "Check_Quarterly"
            Me.Check_Quarterly.Properties.Appearance.BackColor = System.Drawing.Color.Transparent
            Me.Check_Quarterly.Properties.Appearance.ForeColor = System.Drawing.SystemColors.WindowText
            Me.Check_Quarterly.Properties.Appearance.Options.UseBackColor = True
            Me.Check_Quarterly.Properties.Appearance.Options.UseForeColor = True
            Me.Check_Quarterly.Properties.Caption = "Generate invoices for creditors billed quarterly"
            Me.Check_Quarterly.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Style5
            Me.Check_Quarterly.Size = New System.Drawing.Size(328, 22)

            Me.Check_Quarterly.TabIndex = 2
            Me.Check_Quarterly.ToolTip = "Generate creditor invoices for transactions that are billed QUARTERLY"
            Me.Check_Quarterly.ToolTipController = Me.ToolTipController1
            '
            'Check_Monthly
            '
            Me.Check_Monthly.Location = New System.Drawing.Point(16, 129)
            Me.Check_Monthly.Name = "Check_Monthly"
            Me.Check_Monthly.Properties.Appearance.BackColor = System.Drawing.Color.Transparent
            Me.Check_Monthly.Properties.Appearance.ForeColor = System.Drawing.SystemColors.WindowText
            Me.Check_Monthly.Properties.Appearance.Options.UseBackColor = True
            Me.Check_Monthly.Properties.Appearance.Options.UseForeColor = True
            Me.Check_Monthly.Properties.Caption = "Generate invoices for creditors billed monthly"
            Me.Check_Monthly.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Style5
            Me.Check_Monthly.Size = New System.Drawing.Size(328, 22)

            Me.Check_Monthly.TabIndex = 3
            Me.Check_Monthly.ToolTip = "Generate creditor invoices for transactions that are billed MONTHLY"
            Me.Check_Monthly.ToolTipController = Me.ToolTipController1
            '
            'Check_Weekly
            '
            Me.Check_Weekly.Location = New System.Drawing.Point(16, 155)
            Me.Check_Weekly.Name = "Check_Weekly"
            Me.Check_Weekly.Properties.Appearance.BackColor = System.Drawing.Color.Transparent
            Me.Check_Weekly.Properties.Appearance.ForeColor = System.Drawing.SystemColors.WindowText
            Me.Check_Weekly.Properties.Appearance.Options.UseBackColor = True
            Me.Check_Weekly.Properties.Appearance.Options.UseForeColor = True
            Me.Check_Weekly.Properties.Caption = "Generate invoices for creditors billed weekly"
            Me.Check_Weekly.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Style5
            Me.Check_Weekly.Size = New System.Drawing.Size(328, 22)

            Me.Check_Weekly.TabIndex = 4
            Me.Check_Weekly.ToolTip = "Generate creditor invoices for transactions that are billed WEEKLY"
            Me.Check_Weekly.ToolTipController = Me.ToolTipController1
            '
            'Check_Ignore
            '
            Me.Check_Ignore.Location = New System.Drawing.Point(16, 198)
            Me.Check_Ignore.Name = "Check_Ignore"
            Me.Check_Ignore.Properties.Appearance.BackColor = System.Drawing.Color.Transparent
            Me.Check_Ignore.Properties.Appearance.ForeColor = System.Drawing.SystemColors.WindowText
            Me.Check_Ignore.Properties.Appearance.Options.UseBackColor = True
            Me.Check_Ignore.Properties.Appearance.Options.UseForeColor = True
            Me.Check_Ignore.Properties.Caption = "Ignore creditor minimum amount to be billed and generate bill"
            Me.Check_Ignore.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Style5
            Me.Check_Ignore.Size = New System.Drawing.Size(384, 22)

            Me.Check_Ignore.TabIndex = 5
            Me.Check_Ignore.ToolTip = "At the end of the year, do you want to override the minimum bill amount and gener" & _
                "ate the invoices for the current year?"
            Me.Check_Ignore.ToolTipController = Me.ToolTipController1
            '
            'Period_Annually
            '
            Me.Period_Annually.Location = New System.Drawing.Point(352, 52)
            Me.Period_Annually.Name = "Period_Annually"
            Me.Period_Annually.Properties.Appearance.Options.UseTextOptions = True
            Me.Period_Annually.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
            Me.Period_Annually.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Style3D
            Me.Period_Annually.Size = New System.Drawing.Size(48, 22)

            Me.Period_Annually.TabIndex = 6
            Me.Period_Annually.ToolTip = "Restrict the generation of invoices to only creditors with this billing month. CA" & _
                "UTION: Do not enter a value here unless you wish to generate only specific credi" & _
                "tor's invoices."
            Me.Period_Annually.ToolTipController = Me.ToolTipController1
            '
            'Period_SemiAnnually
            '
            Me.Period_SemiAnnually.Location = New System.Drawing.Point(352, 78)
            Me.Period_SemiAnnually.Name = "Period_SemiAnnually"
            Me.Period_SemiAnnually.Properties.Appearance.Options.UseTextOptions = True
            Me.Period_SemiAnnually.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
            Me.Period_SemiAnnually.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Style3D
            Me.Period_SemiAnnually.Size = New System.Drawing.Size(48, 22)

            Me.Period_SemiAnnually.TabIndex = 7
            Me.Period_SemiAnnually.ToolTip = "Restrict the generation of invoices to only creditors with this billing month. CA" & _
                "UTION: Do not enter a value here unless you wish to generate only specific credi" & _
                "tor's invoices."
            Me.Period_SemiAnnually.ToolTipController = Me.ToolTipController1
            '
            'Period_Quarterly
            '
            Me.Period_Quarterly.Location = New System.Drawing.Point(352, 103)
            Me.Period_Quarterly.Name = "Period_Quarterly"
            Me.Period_Quarterly.Properties.Appearance.Options.UseTextOptions = True
            Me.Period_Quarterly.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
            Me.Period_Quarterly.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Style3D
            Me.Period_Quarterly.Size = New System.Drawing.Size(48, 22)

            Me.Period_Quarterly.TabIndex = 8
            Me.Period_Quarterly.ToolTip = "Restrict the generation of invoices to only creditors with this billing month. CA" & _
                "UTION: Do not enter a value here unless you wish to generate only specific credi" & _
                "tor's invoices."
            Me.Period_Quarterly.ToolTipController = Me.ToolTipController1
            '
            'Period_Monthly
            '
            Me.Period_Monthly.Location = New System.Drawing.Point(352, 129)
            Me.Period_Monthly.Name = "Period_Monthly"
            Me.Period_Monthly.Properties.Appearance.Options.UseTextOptions = True
            Me.Period_Monthly.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
            Me.Period_Monthly.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Style3D
            Me.Period_Monthly.Size = New System.Drawing.Size(48, 22)

            Me.Period_Monthly.TabIndex = 9
            Me.Period_Monthly.ToolTip = "Restrict the generation of invoices to only creditors with this billing month. CA" & _
                "UTION: Do not enter a value here unless you wish to generate only specific credi" & _
                "tor's invoices."
            Me.Period_Monthly.ToolTipController = Me.ToolTipController1
            '
            'Period_Weekly
            '
            Me.Period_Weekly.Location = New System.Drawing.Point(352, 155)
            Me.Period_Weekly.Name = "Period_Weekly"
            Me.Period_Weekly.Properties.Appearance.Options.UseTextOptions = True
            Me.Period_Weekly.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
            Me.Period_Weekly.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Style3D
            Me.Period_Weekly.Size = New System.Drawing.Size(48, 22)

            Me.Period_Weekly.TabIndex = 10
            Me.Period_Weekly.ToolTip = "Restrict the generation of invoices to only creditors with this billing month. CA" & _
                "UTION: Do not enter a value here unless you wish to generate only specific credi" & _
                "tor's invoices."
            Me.Period_Weekly.ToolTipController = Me.ToolTipController1
            '
            'Label1
            '
            Me.Label1.Appearance.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
            Me.Label1.Appearance.Options.UseFont = True
            Me.Label1.Appearance.Options.UseTextOptions = True
            Me.Label1.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
            Me.Label1.Appearance.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Bottom
            Me.Label1.Location = New System.Drawing.Point(16, 9)
            Me.Label1.Name = "Label1"
            Me.Label1.Size = New System.Drawing.Size(437, 13)

            Me.Label1.TabIndex = 11
            Me.Label1.Text = "Select the billing periods to generate invoices for creditors in the billing tabl" & _
                "e"
            Me.Label1.ToolTipController = Me.ToolTipController1
            Me.Label1.UseMnemonic = False
            '
            'GroupBox1
            '
            Me.GroupBox1.Location = New System.Drawing.Point(16, 181)
            Me.GroupBox1.Name = "GroupBox1"
            Me.GroupBox1.Size = New System.Drawing.Size(384, 9)
            Me.ToolTipController1.SetSuperTip(Me.GroupBox1, Nothing)
            Me.GroupBox1.TabIndex = 12
            Me.GroupBox1.TabStop = False
            '
            'GroupBox2
            '
            Me.GroupBox2.Location = New System.Drawing.Point(16, 224)
            Me.GroupBox2.Name = "GroupBox2"
            Me.GroupBox2.Size = New System.Drawing.Size(384, 9)
            Me.ToolTipController1.SetSuperTip(Me.GroupBox2, Nothing)
            Me.GroupBox2.TabIndex = 13
            Me.GroupBox2.TabStop = False
            '
            'Label3
            '
            Me.Label3.Appearance.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
            Me.Label3.Appearance.Options.UseFont = True
            Me.Label3.Appearance.Options.UseTextOptions = True
            Me.Label3.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
            Me.Label3.Appearance.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Bottom
            Me.Label3.Location = New System.Drawing.Point(352, 28)
            Me.Label3.Name = "Label3"
            Me.Label3.Size = New System.Drawing.Size(48, 13)

            Me.Label3.TabIndex = 15
            Me.Label3.Text = "Period #"
            Me.Label3.ToolTipController = Me.ToolTipController1
            Me.Label3.UseMnemonic = False
            '
            'Button_Print
            '
            Me.Button_Print.Location = New System.Drawing.Point(123, 250)
            Me.Button_Print.Name = "Button_Print"
            Me.Button_Print.Size = New System.Drawing.Size(75, 25)

            Me.Button_Print.TabIndex = 16
            Me.Button_Print.Text = "&Print..."
            Me.Button_Print.ToolTip = "Generate and print the invoices"
            Me.Button_Print.ToolTipController = Me.ToolTipController1
            '
            'Button_Cancel
            '
            Me.Button_Cancel.Location = New System.Drawing.Point(219, 250)
            Me.Button_Cancel.Name = "Button_Cancel"
            Me.Button_Cancel.Size = New System.Drawing.Size(75, 25)

            Me.Button_Cancel.TabIndex = 17
            Me.Button_Cancel.Text = "&Cancel"
            Me.Button_Cancel.ToolTip = "Generate and print the invoices"
            Me.Button_Cancel.ToolTipController = Me.ToolTipController1
            '
            'FormMain
            '
            Me.AutoScaleBaseSize = New System.Drawing.Size(5, 14)
            Me.ClientSize = New System.Drawing.Size(464, 292)
            Me.Controls.Add(Me.Button_Cancel)
            Me.Controls.Add(Me.Button_Print)
            Me.Controls.Add(Me.Label3)
            Me.Controls.Add(Me.GroupBox2)
            Me.Controls.Add(Me.GroupBox1)
            Me.Controls.Add(Me.Label1)
            Me.Controls.Add(Me.Period_Weekly)
            Me.Controls.Add(Me.Period_Monthly)
            Me.Controls.Add(Me.Period_Quarterly)
            Me.Controls.Add(Me.Period_SemiAnnually)
            Me.Controls.Add(Me.Period_Annually)
            Me.Controls.Add(Me.Check_Ignore)
            Me.Controls.Add(Me.Check_Weekly)
            Me.Controls.Add(Me.Check_Monthly)
            Me.Controls.Add(Me.Check_Quarterly)
            Me.Controls.Add(Me.Check_SemiAnnual)
            Me.Controls.Add(Me.Check_Annual)
            Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog
            Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
            Me.MaximizeBox = False
            Me.Name = "FormMain"
            Me.ToolTipController1.SetSuperTip(Me, Nothing)
            Me.Text = "Select Creditor Billing Periods to Generate Invoices"

            CType(Me.Check_Annual.Properties, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.Check_SemiAnnual.Properties, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.Check_Quarterly.Properties, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.Check_Monthly.Properties, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.Check_Weekly.Properties, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.Check_Ignore.Properties, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.Period_Annually.Properties, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.Period_SemiAnnually.Properties, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.Period_Quarterly.Properties, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.Period_Monthly.Properties, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.Period_Weekly.Properties, System.ComponentModel.ISupportInitialize).EndInit()
            Me.ResumeLayout(False)
            Me.PerformLayout()
        End Sub

        Friend WithEvents Label1 As DevExpress.XtraEditors.LabelControl
        Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
        Friend WithEvents GroupBox2 As System.Windows.Forms.GroupBox
        Friend WithEvents Label3 As DevExpress.XtraEditors.LabelControl
        Friend WithEvents Check_Annual As DevExpress.XtraEditors.CheckEdit
        Friend WithEvents Check_SemiAnnual As DevExpress.XtraEditors.CheckEdit
        Friend WithEvents Check_Quarterly As DevExpress.XtraEditors.CheckEdit
        Friend WithEvents Check_Monthly As DevExpress.XtraEditors.CheckEdit
        Friend WithEvents Check_Weekly As DevExpress.XtraEditors.CheckEdit
        Friend WithEvents Check_Ignore As DevExpress.XtraEditors.CheckEdit
        Friend WithEvents Period_Annually As DevExpress.XtraEditors.TextEdit
        Friend WithEvents Period_SemiAnnually As DevExpress.XtraEditors.TextEdit
        Friend WithEvents Period_Quarterly As DevExpress.XtraEditors.TextEdit
        Friend WithEvents Period_Monthly As DevExpress.XtraEditors.TextEdit
        Friend WithEvents Period_Weekly As DevExpress.XtraEditors.TextEdit
        Friend WithEvents Button_Print As DevExpress.XtraEditors.SimpleButton
        Friend WithEvents Button_Cancel As DevExpress.XtraEditors.SimpleButton
    End Class
End Namespace