#Region "Copyright 2000-2012 DebtPlus, L.L.C."
'{*******************************************************************}
'{                                                                   }
'{       DebtPlus Debt Management System                             }
'{                                                                   }
'{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
'{       ALL RIGHTS RESERVED                                         }
'{                                                                   }
'{   The entire contents of this file is protected by U.S. and       }
'{   International Copyright Laws. Unauthorized reproduction,        }
'{   reverse-engineering, and distribution of all or any portion of  }
'{   the code contained in this file is strictly prohibited and may  }
'{   result in severe civil and criminal penalties and will be       }
'{   prosecuted to the maximum extent possible under the law.        }
'{                                                                   }
'{   RESTRICTIONS                                                    }
'{                                                                   }
'{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
'{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
'{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
'{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
'{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
'{                                                                   }
'{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
'{   ADDITIONAL RESTRICTIONS.                                        }
'{                                                                   }
'{*******************************************************************}
#End Region

Option Strict On
Option Explicit On 
Option Compare Binary

Namespace Creditor.Billing

    Friend Class FormStatus

        Private cls_billing As FormMain.Billing
        Dim BaseTime As Date

        ''' <summary>
        ''' Create a new instance of our form
        ''' </summary>
        Public Sub New()
            MyBase.New()
            InitializeComponent()
            AddHandler Timer1.Tick, AddressOf Timer1_Tick
            AddHandler Me.GotFocus, AddressOf FormStatus_GotFocus
            AddHandler Me.Load, AddressOf FormStatus_Load
            AddHandler MyBase.Closing, AddressOf FormStatus_Closing
        End Sub

        Public Sub New(ByVal bill As FormMain.Billing)
            MyClass.New()
            cls_billing = bill
        End Sub

        ''' <summary>
        ''' Set the CANCEL status if the cancel button is pressed
        ''' </summary>
        Private Sub Button_Cancel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
            cls_billing.Cancelled = True
            Button_Cancel.Enabled = False
            label_Caption.Text = "The operation is being cancelled. Please wait."
            cls_billing.Cancelled = True
        End Sub

        ''' <summary>
        ''' Handle the tick of the background timer
        ''' </summary>
        Private Sub Timer1_Tick(ByVal sender As Object, ByVal e As System.EventArgs)

            ' Convert the time to a formatted string
            Try

                ' Find the elapsed time
                Dim ElapsedTime As System.TimeSpan = Date.UtcNow.Subtract(BaseTime)
                Dim hours As System.Int32 = ElapsedTime.Hours
                Dim Minutes As System.Int32 = ElapsedTime.Minutes
                Dim Seconds As System.Int32 = ElapsedTime.Seconds
                Label_ElapsedTime.Text = System.String.Format("{0:0}:{1:00}:{2:00}", hours, Minutes, Seconds)

            Catch ex As Exception
                DebtPlus.Svc.Common.ErrorHandling.HandleErrors(ex)
            End Try

            ' Restart the timer at one second
            With Timer1
                .Interval = 1000
                .Start()
            End With
        End Sub

        ''' <summary>
        ''' Process the condition when the window receives the input focus
        ''' </summary>
        Private Sub FormStatus_GotFocus(ByVal sender As Object, ByVal e As System.EventArgs)
            TopMost = False
        End Sub

        ''' <summary>
        ''' One-Time intialization of our form
        ''' </summary>
        Private Sub FormStatus_Load(ByVal sender As Object, ByVal e As System.EventArgs)

            ' Start the timer at one second
            BaseTime = Date.UtcNow
            With Timer1
                .Enabled = True
                .Interval = 1000
                .Start()
            End With

        End Sub

        ''' <summary>
        ''' The form is being closed. Set the CANCEL status
        ''' </summary>
        Private Sub FormStatus_Closing(ByVal sender As Object, ByVal e As System.ComponentModel.CancelEventArgs)
            If cls_billing IsNot Nothing Then cls_billing.Cancelled = True
            Timer1.Stop()
            Timer1.Enabled = False
        End Sub
    End Class
End Namespace
