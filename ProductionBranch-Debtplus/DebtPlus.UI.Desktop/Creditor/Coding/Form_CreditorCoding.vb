#Region "Copyright 2000-2012 DebtPlus, L.L.C."
'{*******************************************************************}
'{                                                                   }
'{       DebtPlus Debt Management System                             }
'{                                                                   }
'{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
'{       ALL RIGHTS RESERVED                                         }
'{                                                                   }
'{   The entire contents of this file is protected by U.S. and       }
'{   International Copyright Laws. Unauthorized reproduction,        }
'{   reverse-engineering, and distribution of all or any portion of  }
'{   the code contained in this file is strictly prohibited and may  }
'{   result in severe civil and criminal penalties and will be       }
'{   prosecuted to the maximum extent possible under the law.        }
'{                                                                   }
'{   RESTRICTIONS                                                    }
'{                                                                   }
'{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
'{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
'{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
'{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
'{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
'{                                                                   }
'{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
'{   ADDITIONAL RESTRICTIONS.                                        }
'{                                                                   }
'{*******************************************************************}
#End Region

Option Compare Binary
Option Explicit On 
Option Strict On

Imports DebtPlus.UI.Creditor.Widgets.Controls
Imports DebtPlus.Svc.Common
Imports System.Data.SqlClient
Imports System.Windows.Forms

Namespace Creditor.Coding

    Friend Class Form_CreditorCoding
        Inherits DebtPlus.Data.Forms.DebtPlusForm

        Private Const STR_CreditorCoding As String = "Creditor.Coding"

        ReadOnly ds As New DataSet("ds")

        Private WithEvents _tbl As DataTable
        Private Property tbl As DataTable
            Get
                Return _tbl
            End Get
            Set(value As DataTable)
                If _tbl IsNot Nothing Then
                    RemoveHandler tbl.ColumnChanged, AddressOf tbl_ColumnChanged
                End If
                _tbl = value
                If _tbl IsNot Nothing Then
                    AddHandler tbl.ColumnChanged, AddressOf tbl_ColumnChanged
                End If
            End Set
        End Property

        Private WithEvents bt As New System.ComponentModel.BackgroundWorker()
        Private dlg As DevExpress.Utils.WaitDialogForm

        Private ReadOnly ap As ArgParser
        Public Sub New(ByVal ap As ArgParser)
            MyClass.New()
            Me.ap = ap
        End Sub

        Public Sub New()
            MyBase.New()
            InitializeComponent()
            AddHandler GridView1.KeyPress, AddressOf GridView1_KeyPress
            AddHandler Button_Update.Click, AddressOf Button_Update_Click
            AddHandler CheckedItem.EditValueChanging, AddressOf CheckedItem_EditValueChanging
            AddHandler Load, AddressOf Form_CreditorCoding_Load
            AddHandler bt.DoWork, AddressOf bt_DoWork
            AddHandler bt.RunWorkerCompleted, AddressOf bt_RunWorkerCompleted
            AddHandler CreditorID1.Validating, AddressOf CreditorID1_Validating
        End Sub

#Region " Windows Form Designer generated code "

        'Form overrides dispose to clean up the component list.
        Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
            If disposing Then
                If components IsNot Nothing Then
                    components.Dispose()
                End If
            End If
            MyBase.Dispose(disposing)
        End Sub

        'Required by the Windows Form Designer
        Private components As System.ComponentModel.IContainer

        'NOTE: The following procedure is required by the Windows Form Designer
        'It can be modified using the Windows Form Designer.  
        'Do not modify it using the code editor.
        Friend WithEvents AccountNumberEdit As DevExpress.XtraEditors.Repository.RepositoryItemTextEdit
        Friend WithEvents GridColumn_client_status As DevExpress.XtraGrid.Columns.GridColumn
        Friend WithEvents CreditorID1 As CreditorID
        Friend WithEvents GridControl1 As DevExpress.XtraGrid.GridControl
        Friend WithEvents GridView1 As DevExpress.XtraGrid.Views.Grid.GridView
        Friend WithEvents LabelControl1 As DevExpress.XtraEditors.LabelControl
        Friend WithEvents GridColumn_checked As DevExpress.XtraGrid.Columns.GridColumn
        Friend WithEvents GridColumn_Client As DevExpress.XtraGrid.Columns.GridColumn
        Friend WithEvents GridColumn_held_in_trust As DevExpress.XtraGrid.Columns.GridColumn
        Friend WithEvents GridColumn_active_status As DevExpress.XtraGrid.Columns.GridColumn
        Friend WithEvents GridColumn_creditor As DevExpress.XtraGrid.Columns.GridColumn
        Friend WithEvents GridColumn_account As DevExpress.XtraGrid.Columns.GridColumn
        Friend WithEvents GridColumn_message As DevExpress.XtraGrid.Columns.GridColumn
        Friend WithEvents GridColumn_start_date As DevExpress.XtraGrid.Columns.GridColumn
        Friend WithEvents CheckedItem As DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit
        Friend WithEvents MessageEdit As DevExpress.XtraEditors.Repository.RepositoryItemTextEdit
        Friend WithEvents EraseMessageField As DevExpress.XtraEditors.CheckEdit
        Friend WithEvents Button_Update As DevExpress.XtraEditors.SimpleButton
        <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
            Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(Form_CreditorCoding))
            Dim SerializableAppearanceObject1 As DevExpress.Utils.SerializableAppearanceObject = New DevExpress.Utils.SerializableAppearanceObject
            Me.GridControl1 = New DevExpress.XtraGrid.GridControl
            Me.GridView1 = New DevExpress.XtraGrid.Views.Grid.GridView
            Me.GridColumn_checked = New DevExpress.XtraGrid.Columns.GridColumn
            Me.CheckedItem = New DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit
            Me.GridColumn_Client = New DevExpress.XtraGrid.Columns.GridColumn
            Me.GridColumn_held_in_trust = New DevExpress.XtraGrid.Columns.GridColumn
            Me.GridColumn_active_status = New DevExpress.XtraGrid.Columns.GridColumn
            Me.GridColumn_creditor = New DevExpress.XtraGrid.Columns.GridColumn
            Me.GridColumn_account = New DevExpress.XtraGrid.Columns.GridColumn
            Me.AccountNumberEdit = New DevExpress.XtraEditors.Repository.RepositoryItemTextEdit
            Me.GridColumn_message = New DevExpress.XtraGrid.Columns.GridColumn
            Me.MessageEdit = New DevExpress.XtraEditors.Repository.RepositoryItemTextEdit
            Me.GridColumn_start_date = New DevExpress.XtraGrid.Columns.GridColumn
            Me.GridColumn_client_status = New DevExpress.XtraGrid.Columns.GridColumn
            Me.EraseMessageField = New DevExpress.XtraEditors.CheckEdit
            Me.LabelControl1 = New DevExpress.XtraEditors.LabelControl
            Me.Button_Update = New DevExpress.XtraEditors.SimpleButton
            Me.CreditorID1 = New CreditorID
            CType(Me.DxErrorProvider1, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.GridControl1, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.GridView1, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.CheckedItem, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.AccountNumberEdit, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.MessageEdit, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.EraseMessageField.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.CreditorID1.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
            Me.SuspendLayout()
            '
            'GridControl1
            '
            Me.GridControl1.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                        Or System.Windows.Forms.AnchorStyles.Left) _
                        Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
            Me.GridControl1.Location = New System.Drawing.Point(8, 40)
            Me.GridControl1.MainView = Me.GridView1
            Me.GridControl1.Name = "GridControl1"
            Me.GridControl1.RepositoryItems.AddRange(New DevExpress.XtraEditors.Repository.RepositoryItem() {Me.CheckedItem, Me.MessageEdit, Me.AccountNumberEdit})
            Me.GridControl1.Size = New System.Drawing.Size(705, 200)
            Me.GridControl1.TabIndex = 3
            Me.GridControl1.ToolTipController = Me.ToolTipController1
            Me.GridControl1.ViewCollection.AddRange(New DevExpress.XtraGrid.Views.Base.BaseView() {Me.GridView1})
            '
            'GridView1
            '
            Me.GridView1.Appearance.ColumnFilterButton.BackColor = System.Drawing.Color.FromArgb(CType(CType(104, Byte), Int32), CType(CType(184, Byte), Int32), CType(CType(251, Byte), Int32))
            Me.GridView1.Appearance.ColumnFilterButton.BorderColor = System.Drawing.Color.FromArgb(CType(CType(104, Byte), Int32), CType(CType(184, Byte), Int32), CType(CType(251, Byte), Int32))
            Me.GridView1.Appearance.ColumnFilterButton.ForeColor = System.Drawing.Color.White
            Me.GridView1.Appearance.ColumnFilterButton.Options.UseBackColor = True
            Me.GridView1.Appearance.ColumnFilterButton.Options.UseBorderColor = True
            Me.GridView1.Appearance.ColumnFilterButton.Options.UseForeColor = True
            Me.GridView1.Appearance.ColumnFilterButtonActive.BackColor = System.Drawing.Color.FromArgb(CType(CType(170, Byte), Int32), CType(CType(216, Byte), Int32), CType(CType(254, Byte), Int32))
            Me.GridView1.Appearance.ColumnFilterButtonActive.BorderColor = System.Drawing.Color.FromArgb(CType(CType(170, Byte), Int32), CType(CType(216, Byte), Int32), CType(CType(254, Byte), Int32))
            Me.GridView1.Appearance.ColumnFilterButtonActive.ForeColor = System.Drawing.Color.Black
            Me.GridView1.Appearance.ColumnFilterButtonActive.Options.UseBackColor = True
            Me.GridView1.Appearance.ColumnFilterButtonActive.Options.UseBorderColor = True
            Me.GridView1.Appearance.ColumnFilterButtonActive.Options.UseForeColor = True
            Me.GridView1.Appearance.Empty.BackColor = System.Drawing.Color.FromArgb(CType(CType(236, Byte), Int32), CType(CType(246, Byte), Int32), CType(CType(255, Byte), Int32))
            Me.GridView1.Appearance.Empty.BackColor2 = System.Drawing.Color.White
            Me.GridView1.Appearance.Empty.Options.UseBackColor = True
            Me.GridView1.Appearance.EvenRow.BackColor = System.Drawing.Color.FromArgb(CType(CType(247, Byte), Int32), CType(CType(251, Byte), Int32), CType(CType(255, Byte), Int32))
            Me.GridView1.Appearance.EvenRow.BorderColor = System.Drawing.Color.FromArgb(CType(CType(247, Byte), Int32), CType(CType(251, Byte), Int32), CType(CType(255, Byte), Int32))
            Me.GridView1.Appearance.EvenRow.ForeColor = System.Drawing.Color.Black
            Me.GridView1.Appearance.EvenRow.Options.UseBackColor = True
            Me.GridView1.Appearance.EvenRow.Options.UseBorderColor = True
            Me.GridView1.Appearance.EvenRow.Options.UseForeColor = True
            Me.GridView1.Appearance.FilterCloseButton.BackColor = System.Drawing.Color.FromArgb(CType(CType(104, Byte), Int32), CType(CType(184, Byte), Int32), CType(CType(251, Byte), Int32))
            Me.GridView1.Appearance.FilterCloseButton.BorderColor = System.Drawing.Color.FromArgb(CType(CType(104, Byte), Int32), CType(CType(184, Byte), Int32), CType(CType(251, Byte), Int32))
            Me.GridView1.Appearance.FilterCloseButton.ForeColor = System.Drawing.Color.White
            Me.GridView1.Appearance.FilterCloseButton.Options.UseBackColor = True
            Me.GridView1.Appearance.FilterCloseButton.Options.UseBorderColor = True
            Me.GridView1.Appearance.FilterCloseButton.Options.UseForeColor = True
            Me.GridView1.Appearance.FilterPanel.BackColor = System.Drawing.Color.FromArgb(CType(CType(236, Byte), Int32), CType(CType(246, Byte), Int32), CType(CType(255, Byte), Int32))
            Me.GridView1.Appearance.FilterPanel.BackColor2 = System.Drawing.Color.White
            Me.GridView1.Appearance.FilterPanel.ForeColor = System.Drawing.Color.Black
            Me.GridView1.Appearance.FilterPanel.Options.UseBackColor = True
            Me.GridView1.Appearance.FilterPanel.Options.UseForeColor = True
            Me.GridView1.Appearance.FixedLine.BackColor = System.Drawing.Color.FromArgb(CType(CType(59, Byte), Int32), CType(CType(133, Byte), Int32), CType(CType(195, Byte), Int32))
            Me.GridView1.Appearance.FixedLine.Options.UseBackColor = True
            Me.GridView1.Appearance.FocusedCell.BackColor = System.Drawing.Color.White
            Me.GridView1.Appearance.FocusedCell.ForeColor = System.Drawing.Color.Black
            Me.GridView1.Appearance.FocusedCell.Options.UseBackColor = True
            Me.GridView1.Appearance.FocusedCell.Options.UseForeColor = True
            Me.GridView1.Appearance.FocusedRow.BackColor = System.Drawing.Color.FromArgb(CType(CType(38, Byte), Int32), CType(CType(109, Byte), Int32), CType(CType(189, Byte), Int32))
            Me.GridView1.Appearance.FocusedRow.BorderColor = System.Drawing.Color.FromArgb(CType(CType(59, Byte), Int32), CType(CType(139, Byte), Int32), CType(CType(206, Byte), Int32))
            Me.GridView1.Appearance.FocusedRow.ForeColor = System.Drawing.Color.White
            Me.GridView1.Appearance.FocusedRow.Options.UseBackColor = True
            Me.GridView1.Appearance.FocusedRow.Options.UseBorderColor = True
            Me.GridView1.Appearance.FocusedRow.Options.UseForeColor = True
            Me.GridView1.Appearance.FooterPanel.BackColor = System.Drawing.Color.FromArgb(CType(CType(104, Byte), Int32), CType(CType(184, Byte), Int32), CType(CType(251, Byte), Int32))
            Me.GridView1.Appearance.FooterPanel.BorderColor = System.Drawing.Color.FromArgb(CType(CType(104, Byte), Int32), CType(CType(184, Byte), Int32), CType(CType(251, Byte), Int32))
            Me.GridView1.Appearance.FooterPanel.ForeColor = System.Drawing.Color.Black
            Me.GridView1.Appearance.FooterPanel.Options.UseBackColor = True
            Me.GridView1.Appearance.FooterPanel.Options.UseBorderColor = True
            Me.GridView1.Appearance.FooterPanel.Options.UseForeColor = True
            Me.GridView1.Appearance.GroupButton.BackColor = System.Drawing.Color.FromArgb(CType(CType(104, Byte), Int32), CType(CType(184, Byte), Int32), CType(CType(251, Byte), Int32))
            Me.GridView1.Appearance.GroupButton.BorderColor = System.Drawing.Color.FromArgb(CType(CType(104, Byte), Int32), CType(CType(184, Byte), Int32), CType(CType(251, Byte), Int32))
            Me.GridView1.Appearance.GroupButton.Options.UseBackColor = True
            Me.GridView1.Appearance.GroupButton.Options.UseBorderColor = True
            Me.GridView1.Appearance.GroupFooter.BackColor = System.Drawing.Color.FromArgb(CType(CType(170, Byte), Int32), CType(CType(216, Byte), Int32), CType(CType(254, Byte), Int32))
            Me.GridView1.Appearance.GroupFooter.BorderColor = System.Drawing.Color.FromArgb(CType(CType(170, Byte), Int32), CType(CType(216, Byte), Int32), CType(CType(254, Byte), Int32))
            Me.GridView1.Appearance.GroupFooter.ForeColor = System.Drawing.Color.Black
            Me.GridView1.Appearance.GroupFooter.Options.UseBackColor = True
            Me.GridView1.Appearance.GroupFooter.Options.UseBorderColor = True
            Me.GridView1.Appearance.GroupFooter.Options.UseForeColor = True
            Me.GridView1.Appearance.GroupPanel.BackColor = System.Drawing.Color.FromArgb(CType(CType(236, Byte), Int32), CType(CType(246, Byte), Int32), CType(CType(255, Byte), Int32))
            Me.GridView1.Appearance.GroupPanel.BackColor2 = System.Drawing.Color.White
            Me.GridView1.Appearance.GroupPanel.ForeColor = System.Drawing.Color.Black
            Me.GridView1.Appearance.GroupPanel.Options.UseBackColor = True
            Me.GridView1.Appearance.GroupPanel.Options.UseForeColor = True
            Me.GridView1.Appearance.GroupRow.BackColor = System.Drawing.Color.FromArgb(CType(CType(170, Byte), Int32), CType(CType(216, Byte), Int32), CType(CType(254, Byte), Int32))
            Me.GridView1.Appearance.GroupRow.BorderColor = System.Drawing.Color.FromArgb(CType(CType(170, Byte), Int32), CType(CType(216, Byte), Int32), CType(CType(254, Byte), Int32))
            Me.GridView1.Appearance.GroupRow.ForeColor = System.Drawing.Color.Black
            Me.GridView1.Appearance.GroupRow.Options.UseBackColor = True
            Me.GridView1.Appearance.GroupRow.Options.UseBorderColor = True
            Me.GridView1.Appearance.GroupRow.Options.UseForeColor = True
            Me.GridView1.Appearance.HeaderPanel.BackColor = System.Drawing.Color.FromArgb(CType(CType(139, Byte), Int32), CType(CType(201, Byte), Int32), CType(CType(254, Byte), Int32))
            Me.GridView1.Appearance.HeaderPanel.BorderColor = System.Drawing.Color.FromArgb(CType(CType(139, Byte), Int32), CType(CType(201, Byte), Int32), CType(CType(254, Byte), Int32))
            Me.GridView1.Appearance.HeaderPanel.ForeColor = System.Drawing.Color.Black
            Me.GridView1.Appearance.HeaderPanel.Options.UseBackColor = True
            Me.GridView1.Appearance.HeaderPanel.Options.UseBorderColor = True
            Me.GridView1.Appearance.HeaderPanel.Options.UseForeColor = True
            Me.GridView1.Appearance.HideSelectionRow.BackColor = System.Drawing.Color.FromArgb(CType(CType(105, Byte), Int32), CType(CType(170, Byte), Int32), CType(CType(225, Byte), Int32))
            Me.GridView1.Appearance.HideSelectionRow.BorderColor = System.Drawing.Color.FromArgb(CType(CType(83, Byte), Int32), CType(CType(155, Byte), Int32), CType(CType(215, Byte), Int32))
            Me.GridView1.Appearance.HideSelectionRow.ForeColor = System.Drawing.Color.FromArgb(CType(CType(236, Byte), Int32), CType(CType(246, Byte), Int32), CType(CType(255, Byte), Int32))
            Me.GridView1.Appearance.HideSelectionRow.Options.UseBackColor = True
            Me.GridView1.Appearance.HideSelectionRow.Options.UseBorderColor = True
            Me.GridView1.Appearance.HideSelectionRow.Options.UseForeColor = True
            Me.GridView1.Appearance.HorzLine.BackColor = System.Drawing.Color.FromArgb(CType(CType(104, Byte), Int32), CType(CType(184, Byte), Int32), CType(CType(251, Byte), Int32))
            Me.GridView1.Appearance.HorzLine.Options.UseBackColor = True
            Me.GridView1.Appearance.OddRow.BackColor = System.Drawing.Color.FromArgb(CType(CType(236, Byte), Int32), CType(CType(246, Byte), Int32), CType(CType(255, Byte), Int32))
            Me.GridView1.Appearance.OddRow.BorderColor = System.Drawing.Color.FromArgb(CType(CType(236, Byte), Int32), CType(CType(246, Byte), Int32), CType(CType(255, Byte), Int32))
            Me.GridView1.Appearance.OddRow.ForeColor = System.Drawing.Color.Black
            Me.GridView1.Appearance.OddRow.Options.UseBackColor = True
            Me.GridView1.Appearance.OddRow.Options.UseBorderColor = True
            Me.GridView1.Appearance.OddRow.Options.UseForeColor = True
            Me.GridView1.Appearance.Preview.Font = New System.Drawing.Font("Verdana", 7.5!)
            Me.GridView1.Appearance.Preview.ForeColor = System.Drawing.Color.FromArgb(CType(CType(83, Byte), Int32), CType(CType(155, Byte), Int32), CType(CType(215, Byte), Int32))
            Me.GridView1.Appearance.Preview.Options.UseFont = True
            Me.GridView1.Appearance.Preview.Options.UseForeColor = True
            Me.GridView1.Appearance.Row.BackColor = System.Drawing.Color.FromArgb(CType(CType(247, Byte), Int32), CType(CType(251, Byte), Int32), CType(CType(255, Byte), Int32))
            Me.GridView1.Appearance.Row.ForeColor = System.Drawing.Color.Black
            Me.GridView1.Appearance.Row.Options.UseBackColor = True
            Me.GridView1.Appearance.Row.Options.UseForeColor = True
            Me.GridView1.Appearance.RowSeparator.BackColor = System.Drawing.Color.FromArgb(CType(CType(236, Byte), Int32), CType(CType(246, Byte), Int32), CType(CType(255, Byte), Int32))
            Me.GridView1.Appearance.RowSeparator.BackColor2 = System.Drawing.Color.White
            Me.GridView1.Appearance.RowSeparator.Options.UseBackColor = True
            Me.GridView1.Appearance.SelectedRow.BackColor = System.Drawing.Color.FromArgb(CType(CType(83, Byte), Int32), CType(CType(155, Byte), Int32), CType(CType(215, Byte), Int32))
            Me.GridView1.Appearance.SelectedRow.ForeColor = System.Drawing.Color.White
            Me.GridView1.Appearance.SelectedRow.Options.UseBackColor = True
            Me.GridView1.Appearance.SelectedRow.Options.UseForeColor = True
            Me.GridView1.Appearance.TopNewRow.BackColor = System.Drawing.Color.White
            Me.GridView1.Appearance.TopNewRow.Options.UseBackColor = True
            Me.GridView1.Appearance.VertLine.BackColor = System.Drawing.Color.FromArgb(CType(CType(104, Byte), Int32), CType(CType(184, Byte), Int32), CType(CType(251, Byte), Int32))
            Me.GridView1.Appearance.VertLine.Options.UseBackColor = True
            Me.GridView1.Columns.AddRange(New DevExpress.XtraGrid.Columns.GridColumn() {Me.GridColumn_checked, Me.GridColumn_Client, Me.GridColumn_held_in_trust, Me.GridColumn_active_status, Me.GridColumn_creditor, Me.GridColumn_account, Me.GridColumn_message, Me.GridColumn_start_date, Me.GridColumn_client_status})
            Me.GridView1.GridControl = Me.GridControl1
            Me.GridView1.Name = "GridView1"
            Me.GridView1.OptionsSelection.InvertSelection = True
            Me.GridView1.OptionsSelection.MultiSelect = True
            Me.GridView1.OptionsSelection.UseIndicatorForSelection = False
            Me.GridView1.OptionsView.ShowGroupPanel = False
            Me.GridView1.OptionsView.ShowIndicator = False
            Me.GridView1.SortInfo.AddRange(New DevExpress.XtraGrid.Columns.GridColumnSortInfo() {New DevExpress.XtraGrid.Columns.GridColumnSortInfo(Me.GridColumn_Client, DevExpress.Data.ColumnSortOrder.Ascending)})
            '
            'GridColumn_checked
            '
            Me.GridColumn_checked.AppearanceCell.Options.UseTextOptions = True
            Me.GridColumn_checked.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center
            Me.GridColumn_checked.ColumnEdit = Me.CheckedItem
            Me.GridColumn_checked.CustomizationCaption = "Selected Item"
            Me.GridColumn_checked.FieldName = "checked"
            Me.GridColumn_checked.Name = "GridColumn_checked"
            Me.GridColumn_checked.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.[True]
            Me.GridColumn_checked.ShowButtonMode = DevExpress.XtraGrid.Views.Base.ShowButtonModeEnum.ShowAlways
            Me.GridColumn_checked.ToolTip = "Check this box if you wish to assign the creditor to this debt."
            Me.GridColumn_checked.Visible = True
            Me.GridColumn_checked.VisibleIndex = 0
            Me.GridColumn_checked.Width = 20
            '
            'CheckedItem
            '
            Me.CheckedItem.AutoHeight = False
            Me.CheckedItem.Name = "CheckedItem"
            '
            'GridColumn_Client
            '
            Me.GridColumn_Client.AppearanceCell.Options.UseTextOptions = True
            Me.GridColumn_Client.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
            Me.GridColumn_Client.AppearanceHeader.Options.UseTextOptions = True
            Me.GridColumn_Client.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
            Me.GridColumn_Client.Caption = "Client"
            Me.GridColumn_Client.CustomizationCaption = "Client ID"
            Me.GridColumn_Client.DisplayFormat.FormatString = "f0"
            Me.GridColumn_Client.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
            Me.GridColumn_Client.FieldName = "client"
            Me.GridColumn_Client.GroupFormat.FormatString = "f0"
            Me.GridColumn_Client.GroupFormat.FormatType = DevExpress.Utils.FormatType.Numeric
            Me.GridColumn_Client.Name = "GridColumn_Client"
            Me.GridColumn_Client.OptionsColumn.AllowEdit = False
            Me.GridColumn_Client.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.[True]
            Me.GridColumn_Client.SortMode = DevExpress.XtraGrid.ColumnSortMode.Value
            Me.GridColumn_Client.ToolTip = "Client ID associated with the debt"
            Me.GridColumn_Client.Visible = True
            Me.GridColumn_Client.VisibleIndex = 1
            Me.GridColumn_Client.Width = 63
            '
            'GridColumn_held_in_trust
            '
            Me.GridColumn_held_in_trust.AppearanceCell.Options.UseTextOptions = True
            Me.GridColumn_held_in_trust.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
            Me.GridColumn_held_in_trust.AppearanceHeader.Options.UseTextOptions = True
            Me.GridColumn_held_in_trust.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
            Me.GridColumn_held_in_trust.Caption = "Trust"
            Me.GridColumn_held_in_trust.CustomizationCaption = "Amount in the trust"
            Me.GridColumn_held_in_trust.DisplayFormat.FormatString = "c2"
            Me.GridColumn_held_in_trust.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
            Me.GridColumn_held_in_trust.FieldName = "held_in_trust"
            Me.GridColumn_held_in_trust.GroupFormat.FormatString = "c2"
            Me.GridColumn_held_in_trust.GroupFormat.FormatType = DevExpress.Utils.FormatType.Numeric
            Me.GridColumn_held_in_trust.Name = "GridColumn_held_in_trust"
            Me.GridColumn_held_in_trust.OptionsColumn.AllowEdit = False
            Me.GridColumn_held_in_trust.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.[True]
            Me.GridColumn_held_in_trust.ToolTip = "Client's current trust balance"
            Me.GridColumn_held_in_trust.Visible = True
            Me.GridColumn_held_in_trust.VisibleIndex = 2
            Me.GridColumn_held_in_trust.Width = 59
            '
            'GridColumn_active_status
            '
            Me.GridColumn_active_status.AppearanceCell.Options.UseTextOptions = True
            Me.GridColumn_active_status.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center
            Me.GridColumn_active_status.AppearanceHeader.Options.UseTextOptions = True
            Me.GridColumn_active_status.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center
            Me.GridColumn_active_status.Caption = "Status"
            Me.GridColumn_active_status.CustomizationCaption = "Client Active Status"
            Me.GridColumn_active_status.FieldName = "active_status"
            Me.GridColumn_active_status.Name = "GridColumn_active_status"
            Me.GridColumn_active_status.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.[True]
            Me.GridColumn_active_status.ToolTip = "Client's active status"
            Me.GridColumn_active_status.Visible = True
            Me.GridColumn_active_status.VisibleIndex = 3
            Me.GridColumn_active_status.Width = 54
            '
            'GridColumn_creditor
            '
            Me.GridColumn_creditor.Caption = "Creditor"
            Me.GridColumn_creditor.CustomizationCaption = "Creditor"
            Me.GridColumn_creditor.FieldName = "creditor_name"
            Me.GridColumn_creditor.Name = "GridColumn_creditor"
            Me.GridColumn_creditor.OptionsColumn.AllowEdit = False
            Me.GridColumn_creditor.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.[True]
            Me.GridColumn_creditor.ToolTip = "The name assigned by the counselor when the debt was created."
            Me.GridColumn_creditor.Visible = True
            Me.GridColumn_creditor.VisibleIndex = 4
            Me.GridColumn_creditor.Width = 58
            '
            'GridColumn_account
            '
            Me.GridColumn_account.Caption = "Account#"
            Me.GridColumn_account.ColumnEdit = Me.AccountNumberEdit
            Me.GridColumn_account.CustomizationCaption = "Account Number"
            Me.GridColumn_account.FieldName = "account_number"
            Me.GridColumn_account.Name = "GridColumn_account"
            Me.GridColumn_account.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.[True]
            Me.GridColumn_account.ToolTip = "Account number for the client's debt"
            Me.GridColumn_account.Visible = True
            Me.GridColumn_account.VisibleIndex = 5
            Me.GridColumn_account.Width = 83
            '
            'AccountNumberEdit
            '
            Me.AccountNumberEdit.AllowNullInput = DevExpress.Utils.DefaultBoolean.[False]
            Me.AccountNumberEdit.AutoHeight = False
            Me.AccountNumberEdit.MaxLength = 22
            Me.AccountNumberEdit.Name = "AccountNumberEdit"
            '
            'GridColumn_message
            '
            Me.GridColumn_message.Caption = "Message"
            Me.GridColumn_message.ColumnEdit = Me.MessageEdit
            Me.GridColumn_message.CustomizationCaption = "Message on Account"
            Me.GridColumn_message.FieldName = "message"
            Me.GridColumn_message.Name = "GridColumn_message"
            Me.GridColumn_message.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.[True]
            Me.GridColumn_message.ToolTip = "Message field. Type a new value here if you wish to change this item."
            Me.GridColumn_message.Visible = True
            Me.GridColumn_message.VisibleIndex = 6
            Me.GridColumn_message.Width = 123
            '
            'MessageEdit
            '
            Me.MessageEdit.AllowNullInput = DevExpress.Utils.DefaultBoolean.[True]
            Me.MessageEdit.AutoHeight = False
            Me.MessageEdit.MaxLength = 50
            Me.MessageEdit.Name = "MessageEdit"
            '
            'GridColumn_start_date
            '
            Me.GridColumn_start_date.AppearanceCell.Options.UseTextOptions = True
            Me.GridColumn_start_date.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
            Me.GridColumn_start_date.AppearanceHeader.Options.UseTextOptions = True
            Me.GridColumn_start_date.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
            Me.GridColumn_start_date.Caption = "Start Date"
            Me.GridColumn_start_date.CustomizationCaption = "Start Date"
            Me.GridColumn_start_date.DisplayFormat.FormatString = "d"
            Me.GridColumn_start_date.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime
            Me.GridColumn_start_date.FieldName = "start_date"
            Me.GridColumn_start_date.GroupFormat.FormatString = "d"
            Me.GridColumn_start_date.GroupFormat.FormatType = DevExpress.Utils.FormatType.DateTime
            Me.GridColumn_start_date.GroupInterval = DevExpress.XtraGrid.ColumnGroupInterval.DateMonth
            Me.GridColumn_start_date.Name = "GridColumn_start_date"
            Me.GridColumn_start_date.OptionsColumn.AllowEdit = False
            Me.GridColumn_start_date.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.[True]
            Me.GridColumn_start_date.ToolTip = "Start date for the client."
            Me.GridColumn_start_date.Visible = True
            Me.GridColumn_start_date.VisibleIndex = 7
            Me.GridColumn_start_date.Width = 64
            '
            'GridColumn_client_status
            '
            Me.GridColumn_client_status.Caption = "Client Status"
            Me.GridColumn_client_status.CustomizationCaption = "Client status"
            Me.GridColumn_client_status.FieldName = "client_status"
            Me.GridColumn_client_status.Name = "GridColumn_client_status"
            Me.GridColumn_client_status.OptionsColumn.AllowEdit = False
            Me.GridColumn_client_status.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.[True]
            Me.GridColumn_client_status.ToolTip = "Client status from the Client form"
            '
            'EraseMessageField
            '
            Me.EraseMessageField.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
            Me.EraseMessageField.Location = New System.Drawing.Point(8, 248)
            Me.EraseMessageField.Name = "EraseMessageField"
            Me.EraseMessageField.Properties.Caption = "Clear the message once the debt has been updated."
            Me.EraseMessageField.Size = New System.Drawing.Size(280, 19)
            Me.EraseMessageField.TabIndex = 4
            Me.EraseMessageField.ToolTip = "SHould the message field be cleared when the debts are assigned?"
            Me.EraseMessageField.ToolTipController = Me.ToolTipController1
            '
            'LabelControl1
            '
            Me.LabelControl1.Location = New System.Drawing.Point(16, 11)
            Me.LabelControl1.Name = "LabelControl1"
            Me.LabelControl1.Size = New System.Drawing.Size(43, 13)
            Me.LabelControl1.TabIndex = 0
            Me.LabelControl1.Text = "Creditor:"
            Me.LabelControl1.ToolTipController = Me.ToolTipController1
            '
            'Button_Update
            '
            Me.Button_Update.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
            Me.Button_Update.Enabled = False
            Me.Button_Update.Location = New System.Drawing.Point(641, 8)
            Me.Button_Update.Name = "Button_Update"
            Me.Button_Update.Size = New System.Drawing.Size(75, 23)
            Me.Button_Update.TabIndex = 2
            Me.Button_Update.Text = "Update"
            Me.Button_Update.ToolTip = "Click here to assign the creditor to the checked debts."
            Me.Button_Update.ToolTipController = Me.ToolTipController1
            '
            'CreditorID1
            '
            Me.CreditorID1.EditValue = Nothing
            Me.CreditorID1.Location = New System.Drawing.Point(65, 8)
            Me.CreditorID1.Name = "CreditorID1"
            Me.CreditorID1.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph, "", -1, True, True, False, DevExpress.XtraEditors.ImageLocation.MiddleCenter, CType(resources.GetObject("CreditorID1.Properties.Buttons"), System.Drawing.Image), New DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), SerializableAppearanceObject1, "Click here to search for a creditor ID", "search", Nothing, True)})
            Me.CreditorID1.Properties.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
            Me.CreditorID1.Properties.Mask.AutoComplete = DevExpress.XtraEditors.Mask.AutoCompleteType.Strong
            Me.CreditorID1.Properties.Mask.BeepOnError = True
            Me.CreditorID1.Properties.Mask.EditMask = "[A-Z]{1,2}[0-9]{4,}"
            Me.CreditorID1.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.RegEx
            Me.CreditorID1.Properties.Mask.UseMaskAsDisplayFormat = True
            Me.CreditorID1.Properties.MaxLength = 10
            Me.CreditorID1.Size = New System.Drawing.Size(100, 20)
            Me.CreditorID1.TabIndex = 1
            '
            'Form_CreditorCoding
            '
            Me.AcceptButton = Me.Button_Update
            Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
            Me.ClientSize = New System.Drawing.Size(721, 278)
            Me.Controls.Add(Me.CreditorID1)
            Me.Controls.Add(Me.Button_Update)
            Me.Controls.Add(Me.LabelControl1)
            Me.Controls.Add(Me.EraseMessageField)
            Me.Controls.Add(Me.GridControl1)
            Me.LookAndFeel.UseDefaultLookAndFeel = True
            Me.Name = "Form_CreditorCoding"
            Me.Text = "Code the debt with the creditor ID"
            CType(Me.DxErrorProvider1, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.GridControl1, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.GridView1, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.CheckedItem, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.AccountNumberEdit, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.MessageEdit, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.EraseMessageField.Properties, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.CreditorID1.Properties, System.ComponentModel.ISupportInitialize).EndInit()
            Me.ResumeLayout(False)
            Me.PerformLayout()

        End Sub

#End Region

        ''' <summary>
        ''' General mainline to start our process
        ''' </summary>
        Private Sub Form_CreditorCoding_Load(ByVal sender As Object, ByVal e As EventArgs)

            ' Create a validation routine for this creditor
            ValidateRoutine = New AccountNumberValidation()

            LoadPlacement(STR_CreditorCoding)

            ' Tell the user that this will take some time and don't worry
            dlg = New DevExpress.Utils.WaitDialogForm(My.Resources.LoadingListOfUnCodedDebts)
            dlg.Show()

            With bt
                .WorkerSupportsCancellation = True
                .WorkerReportsProgress = False
                .RunWorkerAsync()
            End With

            ReloadGridFromXML(GridView1)
        End Sub

        ''' <summary>
        ''' Filename to the disk storage for this program
        ''' </summary>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Private Shared Function XMLDirectoryName() As String
            Dim answer As String = System.IO.Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData), String.Format("DebtPlus{0}{1}", System.IO.Path.DirectorySeparatorChar, STR_CreditorCoding)) + System.IO.Path.DirectorySeparatorChar
            Return answer
        End Function

        ''' <summary>
        ''' Restore the layout from the disk system
        ''' </summary>
        ''' <param name="view"></param>
        ''' <remarks></remarks>
        Private Sub ReloadGridFromXML(ByVal view As DevExpress.XtraGrid.Views.Base.BaseView)
            Dim Filename As String = System.IO.Path.Combine(XMLDirectoryName, view.Name + ".xml")
            Try
                GridView1.RestoreLayoutFromXml(Filename)

            Catch ex As System.IO.DirectoryNotFoundException
            Catch ex As System.IO.FileNotFoundException
            End Try

            AddHandler view.Layout, AddressOf LayoutChanged
        End Sub

        ''' <summary>
        ''' Save the layout when it is changed to the disk
        ''' </summary>
        ''' <param name="Sender"></param>
        ''' <param name="e"></param>
        ''' <remarks></remarks>
        Private Sub LayoutChanged(ByVal Sender As Object, ByVal e As EventArgs)
            Dim PathName As String = XMLDirectoryName()
            Dim Filename As String = System.IO.Path.Combine(PathName, CType(Sender, DevExpress.XtraGrid.Views.Base.BaseView).Name + ".xml")
            Dim Success As Boolean = False

            ' Try to save the file directly. This is the normal case once there is a file to be saved.
            Try
                GridView1.SaveLayoutToXml(Filename)
                Success = True

            Catch ex As System.IO.FileLoadException
            Catch ex As System.IO.DirectoryNotFoundException
            End Try

            ' Try to create the base directory if the item does not exist
            If Not Success Then
                Try
                    System.IO.Directory.CreateDirectory(PathName)
                    GridView1.SaveLayoutToXml(Filename)
                    Success = True

                Catch ex As System.IO.FileLoadException
                Catch ex As System.IO.DirectoryNotFoundException
                End Try
            End If
        End Sub

        ''' <summary>
        ''' Do the work to read the debt list in a separate thread
        ''' </summary>
        ''' <param name="sender"></param>
        ''' <param name="e"></param>
        ''' <remarks></remarks>
        Private Sub bt_DoWork(ByVal sender As Object, ByVal e As System.ComponentModel.DoWorkEventArgs)

            ' Clear the tables in the dataset
            ds.Clear()

            ' Ask the system for the list of un-coded debts
            Dim cmd As SqlClient.SqlCommand = New SqlCommand
            With cmd
                .Connection = New SqlConnection(DebtPlus.LINQ.SQLInfoClass.getDefault().ConnectionString)
                .CommandText = "xpr_debt_coding"
                .CommandType = CommandType.StoredProcedure
                .CommandTimeout = 0
            End With

            Dim da As New SqlClient.SqlDataAdapter(cmd)
            Try
                da.Fill(ds, "uncoded_debts")

            Catch ex As SqlClient.SqlException
                DebtPlus.UI.Common.ErrorHandling.HandleErrors(ex)
            End Try

        End Sub

        ''' <summary>
        ''' Back at the main thread -- process the completion logic
        ''' </summary>
        ''' <param name="sender"></param>
        ''' <param name="e"></param>
        ''' <remarks></remarks>
        Private Sub bt_RunWorkerCompleted(ByVal sender As Object, ByVal e As System.ComponentModel.RunWorkerCompletedEventArgs)

            tbl = ds.Tables("uncoded_debts")
            If tbl IsNot Nothing Then

                ' Remove any column called "checked" from the table
                With tbl
                    If .Columns.Contains("checked") Then
                        .Columns.Remove(.Columns("checked"))
                    End If

                    ' Add the checked column to the table so that the table has a column called "checked"
                    ' It is normally not there. It is the status of our checkbox. For some reason, it seems to
                    ' come and go.
                    Dim col As New DataColumn("checked", GetType(Boolean))
                    With col
                        .AllowDBNull = False
                        .Caption = "Checked"
                        .DefaultValue = False
                        .ReadOnly = False
                    End With
                    tbl.Columns.Add(col)
                End With

                ' Bind the data to the grid
                With GridControl1
                    .DataSource = tbl.DefaultView
                    .RefreshDataSource()
                End With
            End If

            ' Dispose of the waiting dialog
            If dlg IsNot Nothing Then
                dlg.Close()
                dlg.Dispose()
                dlg = Nothing
            End If
        End Sub

        ''' <summary>
        ''' Return the current creditor ID value
        ''' </summary>
        ''' <value></value>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Private ReadOnly Property creditor() As String
            Get
                If CreditorID1.EditValue Is Nothing Then Return String.Empty
                Return Convert.ToString(CreditorID1.EditValue)
            End Get
        End Property

        ''' <summary>
        ''' Validate the Creditor ID
        ''' </summary>
        ''' <param name="sender"></param>
        ''' <param name="e"></param>
        ''' <remarks></remarks>
        Private Sub CreditorID1_Validating(ByVal sender As Object, ByVal e As System.ComponentModel.CancelEventArgs)

            Dim ErrorMessage As String = String.Empty
            Dim read_creditor As String = String.Empty
            Dim read_prohibit_use As Boolean = True

            If creditor = String.Empty Then
                ErrorMessage = My.Resources.ThisFieldIsRequired

            Else

                Dim cn As SqlClient.SqlConnection = New SqlConnection(DebtPlus.LINQ.SQLInfoClass.getDefault().ConnectionString)
                Dim rd As SqlClient.SqlDataReader = Nothing

                Try
                    cn.Open()
                    With New SqlCommand
                        .Connection = cn
                        .CommandText = "SELECT creditor, prohibit_use FROM creditors WHERE creditor = @creditor"
                        .Parameters.Add("@creditor", SqlDbType.VarChar, 10).Value = creditor
                        rd = .ExecuteReader(CommandBehavior.CloseConnection Or CommandBehavior.SingleRow)
                    End With

                    If rd.Read Then
                        If Not rd.IsDBNull(0) Then read_creditor = rd.GetString(0)
                        If Not rd.IsDBNull(1) Then read_prohibit_use = Convert.ToInt32(rd.GetValue(1)) <> 0
                    End If

                Catch ex As SqlClient.SqlException
                    DebtPlus.UI.Common.ErrorHandling.HandleErrors(ex)

                Finally
                    If rd IsNot Nothing Then rd.Dispose()
                    If cn IsNot Nothing Then cn.Dispose()
                End Try

                ' Do not allow the user to leave the field if the valid is invalid
                If read_creditor = String.Empty Then
                    ErrorMessage = My.Resources.InvalidCreditorID
                ElseIf read_prohibit_use Then
                    ErrorMessage = My.Resources.ProhibitUseCreditor
                End If
            End If

            ' Set the error message text but don't cancel the update or you can't leave the field to close the form.
            CreditorID1.ErrorText = ErrorMessage

            ' Disable the ok button if there is an error
            Button_Update.Enabled = (CreditorID1.ErrorText = String.Empty) AndAlso (creditor <> String.Empty) AndAlso (CheckedItems > 0)
        End Sub

        ''' <summary>
        ''' Process a checked status change in the grid
        ''' </summary>
        ''' <param name="sender"></param>
        ''' <param name="e"></param>
        ''' <remarks></remarks>
        Private Sub CheckedItem_EditValueChanging(ByVal sender As Object, ByVal e As DevExpress.XtraEditors.Controls.ChangingEventArgs)
            Button_Update.Enabled = (CreditorID1.ErrorText = String.Empty) AndAlso (creditor <> String.Empty) AndAlso (Convert.ToBoolean(e.NewValue) OrElse (CheckedItems > 1))
        End Sub

        ''' <summary>
        ''' Return the number of checked debts in the list
        ''' </summary>
        ''' <value></value>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Private ReadOnly Property CheckedItems() As Int32
            Get
                Dim obj As Object = tbl.Compute("count(checked)", "[checked]<>0")
                If obj Is Nothing OrElse obj Is DBNull.Value Then obj = 0
                Return Convert.ToInt32(obj)
            End Get
        End Property

        ''' <summary>
        ''' Handle a change in the debt message column
        ''' </summary>
        ''' <param name="sender"></param>
        ''' <param name="e"></param>
        ''' <remarks></remarks>
        Private Sub tbl_ColumnChanged(ByVal sender As Object, ByVal e As DataColumnChangeEventArgs)

            Dim client_creditor As Int32
            If e.Row("client_creditor") IsNot Nothing AndAlso e.Row("client_creditor") IsNot DBNull.Value Then
                client_creditor = Convert.ToInt32(e.Row("client_creditor"))
            Else
                client_creditor = -1
            End If

            ' Look for a change to the message field
            If client_creditor >= 0 Then
                Select Case e.Column.ColumnName.ToLower()
                    Case "message"

                        ' Obtain the new message value from the change. Change an empty message to be a NULL value.
                        Dim message As Object = DBNull.Value
                        If e.ProposedValue IsNot Nothing AndAlso e.ProposedValue IsNot DBNull.Value Then
                            message = Convert.ToString(e.ProposedValue).Trim()
                            If Convert.ToString(message) = String.Empty Then
                                message = DBNull.Value
                            End If
                        End If

                        ' There must be a debt record to continue
                        Dim cn As SqlClient.SqlConnection = New SqlConnection(DebtPlus.LINQ.SQLInfoClass.getDefault().ConnectionString)
                        Dim current_cursor As Cursor = System.Windows.Forms.Cursor.Current
                        Try
                            System.Windows.Forms.Cursor.Current = Cursors.WaitCursor
                            cn.Open()

                            ' Update the field with either NULL (for a blank message) or the message text
                            With New SqlCommand
                                .Connection = cn
                                .CommandText = "UPDATE update_client_creditor SET message=@message WHERE client_creditor=@client_creditor"
                                .Parameters.Add("@message", SqlDbType.VarChar, 50).Value = message
                                .Parameters.Add("@client_creditor", SqlDbType.Int).Value = client_creditor
                                .ExecuteNonQuery()
                            End With

                        Catch ex As SqlClient.SqlException
                            ' Something bad happened
                            DebtPlus.UI.Common.ErrorHandling.HandleErrors(ex)

                        Finally
                            System.Windows.Forms.Cursor.Current = current_cursor
                            If cn IsNot Nothing Then cn.Dispose()
                        End Try

                    Case "account_number"

                        ' Obtain the new account_number value from the change. NULL values are not allowed here.
                        Dim account_number As String = DebtPlus.Utils.Nulls.DStr(e.ProposedValue)

                        Dim cn As SqlClient.SqlConnection = New SqlConnection(DebtPlus.LINQ.SQLInfoClass.getDefault().ConnectionString)
                        Dim current_cursor As Cursor = System.Windows.Forms.Cursor.Current
                        Try
                            System.Windows.Forms.Cursor.Current = Cursors.WaitCursor
                            cn.Open()

                            With New SqlCommand
                                .Connection = cn
                                .CommandText = "UPDATE update_client_creditor SET account_number=@account_number WHERE client_creditor=@client_creditor"
                                .Parameters.Add("@account_number", SqlDbType.VarChar, 22).Value = account_number
                                .Parameters.Add("@client_creditor", SqlDbType.Int).Value = client_creditor
                                .ExecuteNonQuery()
                            End With

                        Catch ex As SqlClient.SqlException
                            ' Something bad happened
                            DebtPlus.UI.Common.ErrorHandling.HandleErrors(ex)

                        Finally
                            System.Windows.Forms.Cursor.Current = current_cursor
                            If cn IsNot Nothing Then cn.Dispose()
                        End Try

                    Case Else
                End Select
            End If
        End Sub

        ''' <summary>
        ''' When the update button is clicked, update the debts
        ''' </summary>
        ''' <param name="sender"></param>
        ''' <param name="e"></param>
        ''' <remarks></remarks>
        Private Sub Button_Update_Click(ByVal sender As Object, ByVal e As EventArgs)

            ' Set the waiting cursor
            Dim current_cursor As Cursor = System.Windows.Forms.Cursor.Current
            Dim LastFocusedRow As Int32 = GridView1.FocusedRowHandle
            Try
                System.Windows.Forms.Cursor.Current = Cursors.WaitCursor
                Using vue As New DataView(tbl, "[checked]<>0", String.Empty, DataViewRowState.CurrentRows)
                    For Each drv As DataRowView In vue
                        UpdateDebt(drv, EraseMessageField.Checked)
                    Next
                End Using

                ' If all is good then clear the creditor ID
                CreditorID1.EditValue = Nothing

                ' Try to position the input the creditor field
                CreditorID1.Focus()

            Finally
                tbl.AcceptChanges()
                GridView1.FocusedRowHandle = LastFocusedRow
                System.Windows.Forms.Cursor.Current = current_cursor
            End Try
        End Sub

        Private ValidateRoutine As AccountNumberValidation

        ''' <summary>
        ''' Process the debt update
        ''' </summary>
        ''' <param name="drv"></param>
        ''' <param name="EraseMessage"></param>
        ''' <remarks></remarks>
        Private Sub UpdateDebt(ByVal drv As DataRowView, ByVal EraseMessage As Boolean)
            Dim AccountNumber As String = DebtPlus.Utils.Nulls.DStr(drv("account_number"))
            Dim clientCreditor As Int32 = DebtPlus.Utils.Nulls.DInt(drv("client_creditor"), -1)

            If clientCreditor > 0 Then

                ' Validate the account number against the list of valid creditor masks
                Do While Not ValidateRoutine.IsValidAccount(creditor, AccountNumber)
                    Dim NewAccountNumber As String = String.Empty
                    If DebtPlus.Data.Forms.InputBox.Show(String.Format(My.Resources.InvalidAccountNumber, Environment.NewLine, AccountNumber), NewAccountNumber, My.Resources.InvalidAccountNumberTitle, AccountNumber) <> Windows.Forms.DialogResult.OK Then
                        Return
                    End If
                    AccountNumber = NewAccountNumber
                Loop
                AccountNumber = ValidateRoutine.ValidatedAccountNumber

                ' Try to update the debts for the first pass.
                Select Case DoUpdate(clientCreditor, EraseMessage, AccountNumber)
                    Case 0
                        drv.Delete()    ' The account was changed. Remove it from the list.

                    Case 1
                        Dim NewAccountNumber As String = String.Empty
                        If DebtPlus.Data.Forms.InputBox.Show(String.Format(My.Resources.InvalidAccountNumber, Environment.NewLine, AccountNumber), NewAccountNumber, My.Resources.InvalidAccountNumberTitle, AccountNumber) = Windows.Forms.DialogResult.OK Then
                            If DoUpdate(clientCreditor, EraseMessage, NewAccountNumber) = 0 Then
                                drv.Delete()
                            End If
                        End If

                    Case 2
                        ' Some other error occurred. Leave the row alone.

                    Case Else
                        ' Some other error occurred. Leave the row alone.
                End Select
            End If
        End Sub

        ''' <summary>
        ''' Perform an update on the debt with the account number
        ''' </summary>
        ''' <param name="ClientCreditor"></param>
        ''' <param name="EraseMessage"></param>
        ''' <param name="AccountNumber"></param>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Private Function DoUpdate(ByVal ClientCreditor As Int32, ByVal EraseMessage As Boolean, ByVal AccountNumber As String) As Int32
            Dim answer As Int32 = 2

            ' Missing account numbers are always invalid.
            If AccountNumber.Length = 0 Then
                answer = 1
            Else
                Dim cn As SqlClient.SqlConnection = New SqlConnection(DebtPlus.LINQ.SQLInfoClass.getDefault().ConnectionString)
                Try
                    cn.Open()

                    'Correct the debt on the database and allow for a bad account number
                    Using cmd As SqlClient.SqlCommand = New SqlCommand
                        With cmd
                            .Connection = cn
                            .CommandText = "xpr_debt_creditor"
                            .CommandType = CommandType.StoredProcedure
                            .Parameters.Add("@client_creditor", SqlDbType.Int).Value = ClientCreditor
                            .Parameters.Add("@creditor", SqlDbType.VarChar, 10).Value = creditor
                            .Parameters.Add("@clear_msg", SqlDbType.Bit).Value = EraseMessage
                            .Parameters.Add("@account_number", SqlDbType.VarChar, 50).Value = AccountNumber

                            .ExecuteNonQuery()
                            answer = 0
                        End With
                    End Using

                Catch ex As SqlClient.SqlException
                    If ex.Message.Contains(My.Resources.AccountNumberIsNotValid) Then
                        answer = 1
                    Else
                        DebtPlus.UI.Common.ErrorHandling.HandleErrors(ex)
                    End If

                Catch ex As Exception
                    DebtPlus.UI.Common.ErrorHandling.HandleErrors(ex)

                Finally
                    If cn IsNot Nothing Then cn.Dispose()
                End Try
            End If

            Return answer
        End Function

        ''' <summary>
        ''' Process a keypress event on the grid to select the rows
        ''' </summary>
        ''' <param name="sender"></param>
        ''' <param name="e"></param>
        ''' <remarks></remarks>
        Private Sub GridView1_KeyPress(ByVal sender As Object, ByVal e As KeyPressEventArgs)

            If e.KeyChar = " "c Then
                Dim RowHandles() As Int32 = GridView1.GetSelectedRows

                ' Determine if a selection is desired for the rows.
                ' If none are selected then we want to select the items.
                ' Otherwise, deselect them.
                Dim ShouldSelectRow As Boolean = True
                For Each RowHandle As Int32 In RowHandles
                    Dim row As DataRow = GridView1.GetDataRow(RowHandle)
                    If Convert.ToBoolean(row("checked")) Then
                        ShouldSelectRow = False
                        Exit For
                    End If
                Next

                ' Select or deselect the rows.
                For Each rowhandle As Int32 In RowHandles
                    Dim row As DataRow = GridView1.GetDataRow(rowhandle)
                    row("checked") = ShouldSelectRow
                Next
                GridView1.ClearSelection()
            End If

            ' If the new status is to select the row then enable the update button
            Button_Update.Enabled = CheckedItems > 1
        End Sub
    End Class
End Namespace
