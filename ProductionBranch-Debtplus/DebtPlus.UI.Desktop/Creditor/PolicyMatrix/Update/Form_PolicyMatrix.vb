#Region "Copyright 2000-2012 DebtPlus, L.L.C."
'{*******************************************************************}
'{                                                                   }
'{       DebtPlus Debt Management System                             }
'{                                                                   }
'{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
'{       ALL RIGHTS RESERVED                                         }
'{                                                                   }
'{   The entire contents of this file is protected by U.S. and       }
'{   International Copyright Laws. Unauthorized reproduction,        }
'{   reverse-engineering, and distribution of all or any portion of  }
'{   the code contained in this file is strictly prohibited and may  }
'{   result in severe civil and criminal penalties and will be       }
'{   prosecuted to the maximum extent possible under the law.        }
'{                                                                   }
'{   RESTRICTIONS                                                    }
'{                                                                   }
'{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
'{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
'{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
'{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
'{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
'{                                                                   }
'{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
'{   ADDITIONAL RESTRICTIONS.                                        }
'{                                                                   }
'{*******************************************************************}

#End Region

Option Compare Binary
Option Explicit On
Option Strict On

Imports System.ComponentModel
Imports DevExpress.XtraGrid.Views.Base
Imports DebtPlus.Data.Forms
Imports DebtPlus.Utils
Imports System.Data.SqlClient
Imports DevExpress.XtraGrid.Views.Grid
Imports DevExpress.XtraGrid
Imports DevExpress.XtraGrid.Views.Grid.ViewInfo
Imports System.Windows.Forms

Namespace Creditor.PolicyMatrix.Update
    Friend Class Form_PolicyMatrix
        Inherits DebtPlusForm

        Private ReadOnly _ds As DataSet
        Private ReadOnly _drv As DataRowView = Nothing
        Private Const TableName As String = "policy_matrix_policies"

        ''' <summary>
        ''' Find the creditor that we are editing
        ''' </summary>
        Private ReadOnly Property creditor() As String
            Get
                If _drv Is Nothing OrElse _drv("creditor") Is Nothing OrElse _drv("creditor") Is DBNull.Value Then Return String.Empty
                Return Convert.ToString(_drv("creditor")).Trim()
            End Get
        End Property

        ''' <summary>
        ''' Find the creditor name that we are editing
        ''' </summary>
        Private ReadOnly Property creditor_name() As String
            Get
                Dim result As String = String.Empty
                If _drv IsNot Nothing AndAlso _drv("creditor_name") IsNot Nothing AndAlso _drv("creditor_name") IsNot DBNull.Value Then result = Convert.ToString(_drv("creditor_name")).Trim()
                Return result
            End Get
        End Property

        ''' <summary>
        ''' Value to be used for the editing form as the creditor
        ''' </summary>
        Private ReadOnly Property creditor_label() As String
            Get
                Dim lbl_creditor As String = creditor
                Dim lbl_name As String = creditor_name
                If lbl_creditor <> String.Empty AndAlso lbl_name <> String.Empty Then Return "[" + lbl_creditor + "] " + lbl_name
                Return lbl_creditor + lbl_name
            End Get
        End Property

        Public Sub New(ByVal ds As DataSet, ByVal drv As DataRowView)
            MyBase.New()
            InitializeComponent()
            AddHandler Me.Load, AddressOf Form_PolicyMatrix_Load
            AddHandler MyBase.Closing, AddressOf Form_PolicyMatrix_Closing
            AddHandler SimpleButton_Edit.Click, AddressOf SimpleButton_Edit_Click
            _drv = drv
            _ds = ds
        End Sub

#Region " Windows Form Designer generated code "

        'Form overrides dispose to clean up the component list.
        Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
            If disposing Then
                If components IsNot Nothing Then
                    components.Dispose()
                End If
            End If
            MyBase.Dispose(disposing)
        End Sub

        'Required by the Windows Form Designer
        Private components As System.ComponentModel.IContainer

        'NOTE: The following procedure is required by the Windows Form Designer
        'It can be modified using the Windows Form Designer.  
        'Do not modify it using the code editor.
        Friend WithEvents GridControl1 As DevExpress.XtraGrid.GridControl
        Friend WithEvents GridView1 As DevExpress.XtraGrid.Views.Grid.GridView
        Friend WithEvents SimpleButton_Cancel As DevExpress.XtraEditors.SimpleButton
        Friend WithEvents SimpleButton_Edit As DevExpress.XtraEditors.SimpleButton
        Friend WithEvents GridColumn_policy_matrix_policy As DevExpress.XtraGrid.Columns.GridColumn
        Friend WithEvents GridColumn_Creditor As DevExpress.XtraGrid.Columns.GridColumn
        Friend WithEvents GridColumn_policy_matrix_policy_type As DevExpress.XtraGrid.Columns.GridColumn
        Friend WithEvents GridColumn_changed_date As DevExpress.XtraGrid.Columns.GridColumn
        Friend WithEvents GridColumn_item_value As DevExpress.XtraGrid.Columns.GridColumn
        Friend WithEvents GridColumn_message As DevExpress.XtraGrid.Columns.GridColumn
        Friend WithEvents GridColumn_date_created As DevExpress.XtraGrid.Columns.GridColumn
        Friend WithEvents GridColumn_created_by As DevExpress.XtraGrid.Columns.GridColumn
        Friend WithEvents GridColumn_formatted_policy_matrix_policy_type As DevExpress.XtraGrid.Columns.GridColumn
        Friend WithEvents GridColumn_description As DevExpress.XtraGrid.Columns.GridColumn

        <System.Diagnostics.DebuggerStepThrough()>
        Private Sub InitializeComponent()
            Me.GridControl1 = New DevExpress.XtraGrid.GridControl
            Me.GridView1 = New DevExpress.XtraGrid.Views.Grid.GridView
            Me.GridColumn_description = New DevExpress.XtraGrid.Columns.GridColumn
            Me.GridColumn_description.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.True
            Me.GridColumn_item_value = New DevExpress.XtraGrid.Columns.GridColumn
            Me.GridColumn_item_value.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.True
            Me.GridColumn_message = New DevExpress.XtraGrid.Columns.GridColumn
            Me.GridColumn_message.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.True
            Me.GridColumn_policy_matrix_policy = New DevExpress.XtraGrid.Columns.GridColumn
            Me.GridColumn_policy_matrix_policy.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.True
            Me.GridColumn_Creditor = New DevExpress.XtraGrid.Columns.GridColumn
            Me.GridColumn_Creditor.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.True
            Me.GridColumn_policy_matrix_policy_type = New DevExpress.XtraGrid.Columns.GridColumn
            Me.GridColumn_policy_matrix_policy_type.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.True
            Me.GridColumn_changed_date = New DevExpress.XtraGrid.Columns.GridColumn
            Me.GridColumn_changed_date.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.True
            Me.GridColumn_date_created = New DevExpress.XtraGrid.Columns.GridColumn
            Me.GridColumn_date_created.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.True
            Me.GridColumn_created_by = New DevExpress.XtraGrid.Columns.GridColumn
            Me.GridColumn_created_by.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.True
            Me.GridColumn_formatted_policy_matrix_policy_type = New DevExpress.XtraGrid.Columns.GridColumn
            Me.GridColumn_formatted_policy_matrix_policy_type.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.True
            Me.SimpleButton_Cancel = New DevExpress.XtraEditors.SimpleButton
            Me.SimpleButton_Edit = New DevExpress.XtraEditors.SimpleButton

            CType(Me.GridControl1, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.GridView1, System.ComponentModel.ISupportInitialize).BeginInit()
            Me.SuspendLayout()

            '
            'GridControl1
            '
            Me.GridControl1.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                                             Or System.Windows.Forms.AnchorStyles.Left) _
                                            Or System.Windows.Forms.AnchorStyles.Right), 
                                           System.Windows.Forms.AnchorStyles)
            '
            'GridControl1.EmbeddedNavigator
            '
            Me.GridControl1.EmbeddedNavigator.Name = ""
            Me.GridControl1.Location = New System.Drawing.Point(8, 8)
            Me.GridControl1.MainView = Me.GridView1
            Me.GridControl1.Name = "GridControl1"
            Me.GridControl1.Size = New System.Drawing.Size(400, 256)
            Me.GridControl1.TabIndex = 5
            Me.GridControl1.ViewCollection.AddRange(New DevExpress.XtraGrid.Views.Base.BaseView() {Me.GridView1})
            '
            'GridView1
            '
            Me.GridView1.Appearance.ColumnFilterButton.BackColor = System.Drawing.Color.FromArgb(CType(104, Byte), CType(184, Byte), CType(251, Byte))
            Me.GridView1.Appearance.ColumnFilterButton.BorderColor = System.Drawing.Color.FromArgb(CType(104, Byte), CType(184, Byte), CType(251, Byte))
            Me.GridView1.Appearance.ColumnFilterButton.ForeColor = System.Drawing.Color.White
            Me.GridView1.Appearance.ColumnFilterButton.Options.UseBackColor = True
            Me.GridView1.Appearance.ColumnFilterButton.Options.UseBorderColor = True
            Me.GridView1.Appearance.ColumnFilterButton.Options.UseForeColor = True
            Me.GridView1.Appearance.ColumnFilterButtonActive.BackColor = System.Drawing.Color.FromArgb(CType(170, Byte), CType(216, Byte), CType(254, Byte))
            Me.GridView1.Appearance.ColumnFilterButtonActive.BorderColor = System.Drawing.Color.FromArgb(CType(170, Byte), CType(216, Byte), CType(254, Byte))
            Me.GridView1.Appearance.ColumnFilterButtonActive.ForeColor = System.Drawing.Color.Black
            Me.GridView1.Appearance.ColumnFilterButtonActive.Options.UseBackColor = True
            Me.GridView1.Appearance.ColumnFilterButtonActive.Options.UseBorderColor = True
            Me.GridView1.Appearance.ColumnFilterButtonActive.Options.UseForeColor = True
            Me.GridView1.Appearance.Empty.BackColor = System.Drawing.Color.FromArgb(CType(236, Byte), CType(246, Byte), CType(255, Byte))
            Me.GridView1.Appearance.Empty.BackColor2 = System.Drawing.Color.White
            Me.GridView1.Appearance.Empty.Options.UseBackColor = True
            Me.GridView1.Appearance.EvenRow.BackColor = System.Drawing.Color.FromArgb(CType(247, Byte), CType(251, Byte), CType(255, Byte))
            Me.GridView1.Appearance.EvenRow.BorderColor = System.Drawing.Color.FromArgb(CType(247, Byte), CType(251, Byte), CType(255, Byte))
            Me.GridView1.Appearance.EvenRow.ForeColor = System.Drawing.Color.Black
            Me.GridView1.Appearance.EvenRow.Options.UseBackColor = True
            Me.GridView1.Appearance.EvenRow.Options.UseBorderColor = True
            Me.GridView1.Appearance.EvenRow.Options.UseForeColor = True
            Me.GridView1.Appearance.FilterCloseButton.BackColor = System.Drawing.Color.FromArgb(CType(104, Byte), CType(184, Byte), CType(251, Byte))
            Me.GridView1.Appearance.FilterCloseButton.BorderColor = System.Drawing.Color.FromArgb(CType(104, Byte), CType(184, Byte), CType(251, Byte))
            Me.GridView1.Appearance.FilterCloseButton.ForeColor = System.Drawing.Color.White
            Me.GridView1.Appearance.FilterCloseButton.Options.UseBackColor = True
            Me.GridView1.Appearance.FilterCloseButton.Options.UseBorderColor = True
            Me.GridView1.Appearance.FilterCloseButton.Options.UseForeColor = True
            Me.GridView1.Appearance.FilterPanel.BackColor = System.Drawing.Color.FromArgb(CType(236, Byte), CType(246, Byte), CType(255, Byte))
            Me.GridView1.Appearance.FilterPanel.BackColor2 = System.Drawing.Color.White
            Me.GridView1.Appearance.FilterPanel.ForeColor = System.Drawing.Color.Black
            Me.GridView1.Appearance.FilterPanel.Options.UseBackColor = True
            Me.GridView1.Appearance.FilterPanel.Options.UseForeColor = True
            Me.GridView1.Appearance.FixedLine.BackColor = System.Drawing.Color.FromArgb(CType(59, Byte), CType(133, Byte), CType(195, Byte))
            Me.GridView1.Appearance.FixedLine.Options.UseBackColor = True
            Me.GridView1.Appearance.FocusedCell.BackColor = System.Drawing.Color.White
            Me.GridView1.Appearance.FocusedCell.ForeColor = System.Drawing.Color.Black
            Me.GridView1.Appearance.FocusedCell.Options.UseBackColor = True
            Me.GridView1.Appearance.FocusedCell.Options.UseForeColor = True
            Me.GridView1.Appearance.FocusedRow.BackColor = System.Drawing.Color.FromArgb(CType(38, Byte), CType(109, Byte), CType(189, Byte))
            Me.GridView1.Appearance.FocusedRow.BorderColor = System.Drawing.Color.FromArgb(CType(59, Byte), CType(139, Byte), CType(206, Byte))
            Me.GridView1.Appearance.FocusedRow.ForeColor = System.Drawing.Color.White
            Me.GridView1.Appearance.FocusedRow.Options.UseBackColor = True
            Me.GridView1.Appearance.FocusedRow.Options.UseBorderColor = True
            Me.GridView1.Appearance.FocusedRow.Options.UseForeColor = True
            Me.GridView1.Appearance.FooterPanel.BackColor = System.Drawing.Color.FromArgb(CType(104, Byte), CType(184, Byte), CType(251, Byte))
            Me.GridView1.Appearance.FooterPanel.BorderColor = System.Drawing.Color.FromArgb(CType(104, Byte), CType(184, Byte), CType(251, Byte))
            Me.GridView1.Appearance.FooterPanel.ForeColor = System.Drawing.Color.Black
            Me.GridView1.Appearance.FooterPanel.Options.UseBackColor = True
            Me.GridView1.Appearance.FooterPanel.Options.UseBorderColor = True
            Me.GridView1.Appearance.FooterPanel.Options.UseForeColor = True
            Me.GridView1.Appearance.GroupButton.BackColor = System.Drawing.Color.FromArgb(CType(104, Byte), CType(184, Byte), CType(251, Byte))
            Me.GridView1.Appearance.GroupButton.BorderColor = System.Drawing.Color.FromArgb(CType(104, Byte), CType(184, Byte), CType(251, Byte))
            Me.GridView1.Appearance.GroupButton.Options.UseBackColor = True
            Me.GridView1.Appearance.GroupButton.Options.UseBorderColor = True
            Me.GridView1.Appearance.GroupFooter.BackColor = System.Drawing.Color.FromArgb(CType(170, Byte), CType(216, Byte), CType(254, Byte))
            Me.GridView1.Appearance.GroupFooter.BorderColor = System.Drawing.Color.FromArgb(CType(170, Byte), CType(216, Byte), CType(254, Byte))
            Me.GridView1.Appearance.GroupFooter.ForeColor = System.Drawing.Color.Black
            Me.GridView1.Appearance.GroupFooter.Options.UseBackColor = True
            Me.GridView1.Appearance.GroupFooter.Options.UseBorderColor = True
            Me.GridView1.Appearance.GroupFooter.Options.UseForeColor = True
            Me.GridView1.Appearance.GroupPanel.BackColor = System.Drawing.Color.FromArgb(CType(236, Byte), CType(246, Byte), CType(255, Byte))
            Me.GridView1.Appearance.GroupPanel.BackColor2 = System.Drawing.Color.White
            Me.GridView1.Appearance.GroupPanel.ForeColor = System.Drawing.Color.Black
            Me.GridView1.Appearance.GroupPanel.Options.UseBackColor = True
            Me.GridView1.Appearance.GroupPanel.Options.UseForeColor = True
            Me.GridView1.Appearance.GroupRow.BackColor = System.Drawing.Color.FromArgb(CType(170, Byte), CType(216, Byte), CType(254, Byte))
            Me.GridView1.Appearance.GroupRow.BorderColor = System.Drawing.Color.FromArgb(CType(170, Byte), CType(216, Byte), CType(254, Byte))
            Me.GridView1.Appearance.GroupRow.ForeColor = System.Drawing.Color.Black
            Me.GridView1.Appearance.GroupRow.Options.UseBackColor = True
            Me.GridView1.Appearance.GroupRow.Options.UseBorderColor = True
            Me.GridView1.Appearance.GroupRow.Options.UseForeColor = True
            Me.GridView1.Appearance.HeaderPanel.BackColor = System.Drawing.Color.FromArgb(CType(139, Byte), CType(201, Byte), CType(254, Byte))
            Me.GridView1.Appearance.HeaderPanel.BorderColor = System.Drawing.Color.FromArgb(CType(139, Byte), CType(201, Byte), CType(254, Byte))
            Me.GridView1.Appearance.HeaderPanel.ForeColor = System.Drawing.Color.Black
            Me.GridView1.Appearance.HeaderPanel.Options.UseBackColor = True
            Me.GridView1.Appearance.HeaderPanel.Options.UseBorderColor = True
            Me.GridView1.Appearance.HeaderPanel.Options.UseForeColor = True
            Me.GridView1.Appearance.HideSelectionRow.BackColor = System.Drawing.Color.FromArgb(CType(105, Byte), CType(170, Byte), CType(225, Byte))
            Me.GridView1.Appearance.HideSelectionRow.BorderColor = System.Drawing.Color.FromArgb(CType(83, Byte), CType(155, Byte), CType(215, Byte))
            Me.GridView1.Appearance.HideSelectionRow.ForeColor = System.Drawing.Color.FromArgb(CType(236, Byte), CType(246, Byte), CType(255, Byte))
            Me.GridView1.Appearance.HideSelectionRow.Options.UseBackColor = True
            Me.GridView1.Appearance.HideSelectionRow.Options.UseBorderColor = True
            Me.GridView1.Appearance.HideSelectionRow.Options.UseForeColor = True
            Me.GridView1.Appearance.HorzLine.BackColor = System.Drawing.Color.FromArgb(CType(104, Byte), CType(184, Byte), CType(251, Byte))
            Me.GridView1.Appearance.HorzLine.Options.UseBackColor = True
            Me.GridView1.Appearance.OddRow.BackColor = System.Drawing.Color.FromArgb(CType(236, Byte), CType(246, Byte), CType(255, Byte))
            Me.GridView1.Appearance.OddRow.BorderColor = System.Drawing.Color.FromArgb(CType(236, Byte), CType(246, Byte), CType(255, Byte))
            Me.GridView1.Appearance.OddRow.ForeColor = System.Drawing.Color.Black
            Me.GridView1.Appearance.OddRow.Options.UseBackColor = True
            Me.GridView1.Appearance.OddRow.Options.UseBorderColor = True
            Me.GridView1.Appearance.OddRow.Options.UseForeColor = True
            Me.GridView1.Appearance.Preview.Font = New System.Drawing.Font("Verdana", 7.5!)
            Me.GridView1.Appearance.Preview.ForeColor = System.Drawing.Color.FromArgb(CType(83, Byte), CType(155, Byte), CType(215, Byte))
            Me.GridView1.Appearance.Preview.Options.UseFont = True
            Me.GridView1.Appearance.Preview.Options.UseForeColor = True
            Me.GridView1.Appearance.Row.BackColor = System.Drawing.Color.FromArgb(CType(247, Byte), CType(251, Byte), CType(255, Byte))
            Me.GridView1.Appearance.Row.ForeColor = System.Drawing.Color.Black
            Me.GridView1.Appearance.Row.Options.UseBackColor = True
            Me.GridView1.Appearance.Row.Options.UseForeColor = True
            Me.GridView1.Appearance.RowSeparator.BackColor = System.Drawing.Color.FromArgb(CType(236, Byte), CType(246, Byte), CType(255, Byte))
            Me.GridView1.Appearance.RowSeparator.BackColor2 = System.Drawing.Color.White
            Me.GridView1.Appearance.RowSeparator.Options.UseBackColor = True
            Me.GridView1.Appearance.SelectedRow.BackColor = System.Drawing.Color.FromArgb(CType(83, Byte), CType(155, Byte), CType(215, Byte))
            Me.GridView1.Appearance.SelectedRow.ForeColor = System.Drawing.Color.White
            Me.GridView1.Appearance.SelectedRow.Options.UseBackColor = True
            Me.GridView1.Appearance.SelectedRow.Options.UseForeColor = True
            Me.GridView1.Appearance.TopNewRow.BackColor = System.Drawing.Color.White
            Me.GridView1.Appearance.TopNewRow.Options.UseBackColor = True
            Me.GridView1.Appearance.VertLine.BackColor = System.Drawing.Color.FromArgb(CType(104, Byte), CType(184, Byte), CType(251, Byte))
            Me.GridView1.Appearance.VertLine.Options.UseBackColor = True
            Me.GridView1.Columns.AddRange(New DevExpress.XtraGrid.Columns.GridColumn() {Me.GridColumn_description, Me.GridColumn_item_value, Me.GridColumn_message, Me.GridColumn_policy_matrix_policy, Me.GridColumn_Creditor, Me.GridColumn_policy_matrix_policy_type, Me.GridColumn_changed_date, Me.GridColumn_date_created, Me.GridColumn_created_by, Me.GridColumn_formatted_policy_matrix_policy_type})
            Me.GridView1.GridControl = Me.GridControl1
            Me.GridView1.Name = "GridView1"
            Me.GridView1.OptionsBehavior.Editable = False
            Me.GridView1.OptionsView.ShowGroupPanel = False
            Me.GridView1.OptionsView.ShowIndicator = False
            '
            'GridColumn_description
            '
            Me.GridColumn_description.Caption = "Policy"
            Me.GridColumn_description.CustomizationCaption = "Policy"
            Me.GridColumn_description.FieldName = "description"
            Me.GridColumn_description.Name = "GridColumn_description"
            Me.GridColumn_description.Visible = True
            Me.GridColumn_description.VisibleIndex = 0
            Me.GridColumn_description.Width = 148
            '
            'GridColumn_item_value
            '
            Me.GridColumn_item_value.Caption = "Value"
            Me.GridColumn_item_value.CustomizationCaption = "Value"
            Me.GridColumn_item_value.FieldName = "item_value"
            Me.GridColumn_item_value.Name = "GridColumn_item_value"
            Me.GridColumn_item_value.Visible = True
            Me.GridColumn_item_value.VisibleIndex = 1
            Me.GridColumn_item_value.Width = 67
            '
            'GridColumn_message
            '
            Me.GridColumn_message.Caption = "Note"
            Me.GridColumn_message.CustomizationCaption = "Note"
            Me.GridColumn_message.FieldName = "message"
            Me.GridColumn_message.Name = "GridColumn_message"
            Me.GridColumn_message.Visible = True
            Me.GridColumn_message.VisibleIndex = 2
            Me.GridColumn_message.Width = 181
            '
            'GridColumn_policy_matrix_policy
            '
            Me.GridColumn_policy_matrix_policy.Caption = "policy_matrix_policy"
            Me.GridColumn_policy_matrix_policy.CustomizationCaption = "policy_matrix_policy"
            Me.GridColumn_policy_matrix_policy.FieldName = "policy_matrix_policy"
            Me.GridColumn_policy_matrix_policy.Name = "GridColumn_policy_matrix_policy"
            '
            'GridColumn_Creditor
            '
            Me.GridColumn_Creditor.Caption = "Creditor"
            Me.GridColumn_Creditor.CustomizationCaption = "Creditor"
            Me.GridColumn_Creditor.FieldName = "creditor"
            Me.GridColumn_Creditor.Name = "GridColumn_Creditor"
            '
            'GridColumn_policy_matrix_policy_type
            '
            Me.GridColumn_policy_matrix_policy_type.Caption = "policy_matrix_policy_type"
            Me.GridColumn_policy_matrix_policy_type.CustomizationCaption = "policy_matrix_policy_type"
            Me.GridColumn_policy_matrix_policy_type.FieldName = "policy_matrix_policy_type"
            Me.GridColumn_policy_matrix_policy_type.Name = "GridColumn_policy_matrix_policy_type"
            '
            'GridColumn_changed_date
            '
            Me.GridColumn_changed_date.Caption = "Date Changed"
            Me.GridColumn_changed_date.CustomizationCaption = "Date Changed"
            Me.GridColumn_changed_date.FieldName = "changed_date"
            Me.GridColumn_changed_date.Name = "GridColumn_changed_date"
            '
            'GridColumn_date_created
            '
            Me.GridColumn_date_created.Caption = "Date Created"
            Me.GridColumn_date_created.CustomizationCaption = "Date Created"
            Me.GridColumn_date_created.FieldName = "date_created"
            Me.GridColumn_date_created.Name = "GridColumn_date_created"
            '
            'GridColumn_created_by
            '
            Me.GridColumn_created_by.Caption = "Created By"
            Me.GridColumn_created_by.CustomizationCaption = "Created By"
            Me.GridColumn_created_by.FieldName = "created_by"
            Me.GridColumn_created_by.Name = "GridColumn_created_by"
            '
            'GridColumn_formatted_policy_matrix_policy_type
            '
            Me.GridColumn_formatted_policy_matrix_policy_type.Caption = "formatted_policy_matrix_policy_type"
            Me.GridColumn_formatted_policy_matrix_policy_type.CustomizationCaption = "formatted_policy_matrix_policy_type"
            Me.GridColumn_formatted_policy_matrix_policy_type.FieldName = "formatted_policy_matrix_policy_type"
            Me.GridColumn_formatted_policy_matrix_policy_type.Name = "GridColumn_formatted_policy_matrix_policy_type"
            '
            'SimpleButton_Cancel
            '
            Me.SimpleButton_Cancel.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
            Me.SimpleButton_Cancel.DialogResult = System.Windows.Forms.DialogResult.Cancel
            Me.SimpleButton_Cancel.Location = New System.Drawing.Point(416, 48)
            Me.SimpleButton_Cancel.Name = "SimpleButton_Cancel"
            Me.SimpleButton_Cancel.TabIndex = 4
            Me.SimpleButton_Cancel.Text = "&Cancel"
            '
            'SimpleButton_Edit
            '
            Me.SimpleButton_Edit.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
            Me.SimpleButton_Edit.Location = New System.Drawing.Point(416, 8)
            Me.SimpleButton_Edit.Name = "SimpleButton_Edit"
            Me.SimpleButton_Edit.TabIndex = 3
            Me.SimpleButton_Edit.Text = "&Edit..."
            '
            'Form_PolicyMatrix
            '
            Me.AutoScaleBaseSize = New System.Drawing.Size(5, 14)
            Me.CancelButton = Me.SimpleButton_Cancel
            Me.ClientSize = New System.Drawing.Size(504, 270)
            Me.Controls.Add(Me.GridControl1)
            Me.Controls.Add(Me.SimpleButton_Cancel)
            Me.Controls.Add(Me.SimpleButton_Edit)
            Me.Name = "Form_PolicyMatrix"
            Me.ToolTipController1.SetSuperTip(Me, Nothing)
            Me.Text = "Policy Matrix - Policy Values"

            CType(Me.GridControl1, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.GridView1, System.ComponentModel.ISupportInitialize).EndInit()
            Me.ResumeLayout(False)
        End Sub

#End Region

        Private Sub Form_PolicyMatrix_Load(ByVal sender As Object, ByVal e As EventArgs)

            ' Replace our title with the creditor name if given
            If creditor_name <> String.Empty Then Me.Text = creditor_name + " Policy Matrix"

            Dim current_cursor As Cursor = Cursor.Current
            Try
                Cursor.Current = Cursors.WaitCursor
                Dim tbl As DataTable = _ds.Tables(TableName)
                If tbl IsNot Nothing Then tbl.Clear()

                ' Load the list of policies for this creditor
                Using cmd As SqlCommand = New SqlCommand
                    With cmd
                        .Connection = New SqlConnection(DebtPlus.LINQ.SQLInfoClass.getDefault().ConnectionString)
                        .CommandText = "SELECT p.policy_matrix_policy, p.creditor, p.policy_matrix_policy_type, p.changed_date, p.item_value, p.message, p.date_created, p.created_by, t.policy_matrix_policy_type as formatted_policy_matrix_policy_type, t.description, convert(bit,0) AS CopyToSIC FROM policy_matrix_policy_types t WITH (NOLOCK) LEFT OUTER JOIN policy_matrix_policies p WITH (NOLOCK) ON t.policy_matrix_policy_type = p.policy_matrix_policy_type AND p.creditor = @creditor"
                        .Parameters.Add("@creditor", SqlDbType.VarChar, 10).Value = creditor
                    End With
                    Using da As New SqlDataAdapter(cmd)
                        da.Fill(_ds, TableName)
                    End Using
                End Using

                tbl = _ds.Tables(TableName)

                With GridControl1
                    .DataSource = tbl.DefaultView
                    .RefreshDataSource()
                End With

                With GridView1
                    .BestFitColumns()
                End With

                ' Disable the edit operation if there are no policies to edit???
                If tbl.Rows.Count <= 0 Then Me.SimpleButton_Edit.Enabled = False

            Catch ex As Exception
                DebtPlus.UI.Common.ErrorHandling.HandleErrors(ex, "Error reading policy_matrix tables")

            Finally
                Cursor.Current = current_cursor
            End Try

            ' Add the handlers for the events
            AddHandler GridView1.DoubleClick, AddressOf GridView1_DoubleClick
            AddHandler GridView1.FocusedRowChanged, AddressOf GridView1_FocusedRowChanged
        End Sub

        ''' <summary>
        ''' Process a change in the current row for the control
        ''' </summary>
        Private Sub GridView1_FocusedRowChanged(ByVal sender As Object, ByVal e As FocusedRowChangedEventArgs)
            Dim gv As GridView = CType(sender, GridView)
            Dim ctl As GridControl = gv.GridControl
            Dim controlRow As Int32 = e.FocusedRowHandle

            ' Find the datarowview from the input tables.
            Dim drv As DataRowView = Nothing
            If controlRow >= 0 Then
                Dim vue As DataView = CType(ctl.DataSource, DataView)
                drv = CType(gv.GetRow(controlRow), DataRowView)
            End If
        End Sub

        Private Sub GridView1_DoubleClick(ByVal sender As Object, ByVal e As EventArgs)

            ' Find the row targetted as the double-click item
            Dim gv As GridView = CType(sender, GridView)
            Dim ctl As GridControl = CType(gv.GridControl, GridControl)
            Dim hi As GridHitInfo = gv.CalcHitInfo((ctl.PointToClient(MousePosition)))
            Dim controlRow As Int32 = hi.RowHandle

            ' Find the datarowview from the input tables.
            Dim drv As DataRowView = Nothing
            If controlRow >= 0 Then
                Dim vue As DataView = CType(ctl.DataSource, DataView)
                drv = CType(gv.GetRow(controlRow), DataRowView)
            End If

            ' If there is a row then perform the edit operation. If successful, complete the changes else roll them back
            If drv IsNot Nothing Then EditItem(drv)
        End Sub

        Private Sub EditItem(ByVal drv As DataRowView)
            drv.BeginEdit()
            Using frm As New Form_EditItem(drv, creditor_label)
                If frm.ShowDialog() = DialogResult.OK Then
                    drv.EndEdit()
                Else
                    drv.CancelEdit()
                End If
            End Using
        End Sub

        ''' <summary>
        ''' Update the table of policy matrix values when the form is closed
        ''' </summary>
        Private Sub Form_PolicyMatrix_Closing(ByVal sender As Object, ByVal e As CancelEventArgs)
            Dim tbl As DataTable = _ds.Tables(TableName)

            ' If there is a table to update then build the update command.
            ' We don't have an insert nor delete since we don't do these operations.
            If tbl IsNot Nothing Then

                ' Do the update operation for any rows that are changed
                Try
                    Using cur As New DebtPlus.UI.Common.CursorManager()
                        Using cmd As SqlCommand = New SqlCommand()
                            With cmd
                                .Connection = New SqlConnection(DebtPlus.LINQ.SQLInfoClass.getDefault().ConnectionString)
                                .CommandText = "xpr_insert_policy_matrix_policies"
                                .CommandType = CommandType.StoredProcedure
                                .Parameters.Add("@creditor", SqlDbType.VarChar, 10).Value = creditor
                                .Parameters.Add("@policy_matrix_policy_type", SqlDbType.VarChar, 50, "formatted_policy_matrix_policy_type")
                                .Parameters.Add("@item_value", SqlDbType.VarChar, 1024, "item_value")
                                .Parameters.Add("@message", SqlDbType.VarChar, 1024, "message")
                                .Parameters.Add("@use_sic", SqlDbType.Bit, 0, "CopyToSIC")
                            End With

                            Using da As New SqlDataAdapter
                                da.UpdateCommand = cmd
                                da.Update(tbl)
                            End Using
                        End Using
                    End Using

                Catch ex As SqlClient.SqlException
                    DebtPlus.UI.Common.ErrorHandling.HandleErrors(ex, "Error updating creditor policy matrix")
                End Try
            End If
        End Sub

        Private Sub SimpleButton_Edit_Click(ByVal sender As Object, ByVal e As EventArgs)
            Dim drv As DataRowView = Nothing

            If GridView1.FocusedRowHandle >= 0 Then
                drv = CType(GridView1.GetRow(GridView1.FocusedRowHandle), DataRowView)
            End If

            ' If there is a row then perform the edit operation. If successful, complete the changes else roll them back
            If drv IsNot Nothing Then EditItem(drv)
        End Sub
    End Class
End Namespace
