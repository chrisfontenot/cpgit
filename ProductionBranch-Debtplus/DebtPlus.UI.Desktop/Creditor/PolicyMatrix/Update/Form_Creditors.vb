#Region "Copyright 2000-2012 DebtPlus, L.L.C."
'{*******************************************************************}
'{                                                                   }
'{       DebtPlus Debt Management System                             }
'{                                                                   }
'{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
'{       ALL RIGHTS RESERVED                                         }
'{                                                                   }
'{   The entire contents of this file is protected by U.S. and       }
'{   International Copyright Laws. Unauthorized reproduction,        }
'{   reverse-engineering, and distribution of all or any portion of  }
'{   the code contained in this file is strictly prohibited and may  }
'{   result in severe civil and criminal penalties and will be       }
'{   prosecuted to the maximum extent possible under the law.        }
'{                                                                   }
'{   RESTRICTIONS                                                    }
'{                                                                   }
'{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
'{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
'{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
'{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
'{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
'{                                                                   }
'{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
'{   ADDITIONAL RESTRICTIONS.                                        }
'{                                                                   }
'{*******************************************************************}

#End Region

Option Compare Binary
Option Explicit On
Option Strict On

Imports DevExpress.XtraGrid.Views.Base
Imports DebtPlus.Data.Forms
Imports DevExpress.XtraGrid.Views.Grid
Imports DevExpress.XtraGrid
Imports DevExpress.XtraGrid.Views.Grid.ViewInfo
Imports System.Data.SqlClient
Imports System.Windows.Forms

Namespace Creditor.PolicyMatrix.Update
    Friend Class Form_Creditors
        Inherits DebtPlusForm

        Private ReadOnly _ds As DataSet

        Public Sub New(ByVal ds As DataSet)
            MyBase.New()
            InitializeComponent()
            AddHandler SimpleButton_Cancel.Click, AddressOf SimpleButton_Cancel_Click
            AddHandler SimpleButton_Edit.Click, AddressOf SimpleButton_Edit_Click
            AddHandler GridView1.MouseDown, AddressOf GridView1_MouseDown
            AddHandler Me.Load, AddressOf MyGridControl_Load

            _ds = ds

            'Add any initialization after the InitializeComponent() call
            GridColumn_sic.GroupFormat.Format = New CreditorPolicyMatrixFormatter
            GridColumn_sic.DisplayFormat.Format = New CreditorPolicyMatrixFormatter
        End Sub

#Region " Windows Form Designer generated code "

        'Required by the Windows Form Designer
        Private components As System.ComponentModel.IContainer

        'NOTE: The following procedure is required by the Windows Form Designer
        'It can be modified using the Windows Form Designer.  
        'Do not modify it using the code editor.
        Friend WithEvents SimpleButton_Edit As DevExpress.XtraEditors.SimpleButton
        Friend WithEvents SimpleButton_Cancel As DevExpress.XtraEditors.SimpleButton
        Friend WithEvents GridControl1 As DevExpress.XtraGrid.GridControl
        Friend WithEvents GridView1 As DevExpress.XtraGrid.Views.Grid.GridView
        Friend WithEvents GridColumn_sic As DevExpress.XtraGrid.Columns.GridColumn
        Friend WithEvents GridColumn_creditor As DevExpress.XtraGrid.Columns.GridColumn
        Friend WithEvents GridColumn_CreditorName As DevExpress.XtraGrid.Columns.GridColumn

        <System.Diagnostics.DebuggerStepThrough()>
        Private Sub InitializeComponent()
            Me.SimpleButton_Edit = New DevExpress.XtraEditors.SimpleButton
            Me.SimpleButton_Cancel = New DevExpress.XtraEditors.SimpleButton
            Me.GridControl1 = New DevExpress.XtraGrid.GridControl
            Me.GridView1 = New DevExpress.XtraGrid.Views.Grid.GridView
            Me.GridColumn_sic = New DevExpress.XtraGrid.Columns.GridColumn
            Me.GridColumn_sic.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.True
            Me.GridColumn_creditor = New DevExpress.XtraGrid.Columns.GridColumn
            Me.GridColumn_creditor.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.True
            Me.GridColumn_CreditorName = New DevExpress.XtraGrid.Columns.GridColumn
            Me.GridColumn_CreditorName.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.True

            CType(Me.GridControl1, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.GridView1, System.ComponentModel.ISupportInitialize).BeginInit()
            Me.SuspendLayout()

            '
            'SimpleButton_Edit
            '
            Me.SimpleButton_Edit.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
            Me.SimpleButton_Edit.Location = New System.Drawing.Point(416, 8)
            Me.SimpleButton_Edit.Name = "SimpleButton_Edit"
            Me.SimpleButton_Edit.TabIndex = 0
            Me.SimpleButton_Edit.Text = "&Edit..."
            '
            'SimpleButton_Cancel
            '
            Me.SimpleButton_Cancel.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
            Me.SimpleButton_Cancel.Location = New System.Drawing.Point(416, 48)
            Me.SimpleButton_Cancel.Name = "SimpleButton_Cancel"
            Me.SimpleButton_Cancel.TabIndex = 1
            Me.SimpleButton_Cancel.Text = "&Cancel"
            '
            'GridControl1
            '
            Me.GridControl1.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                                             Or System.Windows.Forms.AnchorStyles.Left) _
                                            Or System.Windows.Forms.AnchorStyles.Right), 
                                           System.Windows.Forms.AnchorStyles)
            '
            'GridControl1.EmbeddedNavigator
            '
            Me.GridControl1.EmbeddedNavigator.Name = ""
            Me.GridControl1.Location = New System.Drawing.Point(8, 8)
            Me.GridControl1.MainView = Me.GridView1
            Me.GridControl1.Name = "GridControl1"
            Me.GridControl1.Size = New System.Drawing.Size(400, 256)
            Me.GridControl1.TabIndex = 2
            Me.GridControl1.ViewCollection.AddRange(New DevExpress.XtraGrid.Views.Base.BaseView() {Me.GridView1})
            '
            'GridView1
            '
            Me.GridView1.Appearance.ColumnFilterButton.BackColor = System.Drawing.Color.FromArgb(CType(104, Byte), CType(184, Byte), CType(251, Byte))
            Me.GridView1.Appearance.ColumnFilterButton.BorderColor = System.Drawing.Color.FromArgb(CType(104, Byte), CType(184, Byte), CType(251, Byte))
            Me.GridView1.Appearance.ColumnFilterButton.ForeColor = System.Drawing.Color.White
            Me.GridView1.Appearance.ColumnFilterButton.Options.UseBackColor = True
            Me.GridView1.Appearance.ColumnFilterButton.Options.UseBorderColor = True
            Me.GridView1.Appearance.ColumnFilterButton.Options.UseForeColor = True
            Me.GridView1.Appearance.ColumnFilterButtonActive.BackColor = System.Drawing.Color.FromArgb(CType(170, Byte), CType(216, Byte), CType(254, Byte))
            Me.GridView1.Appearance.ColumnFilterButtonActive.BorderColor = System.Drawing.Color.FromArgb(CType(170, Byte), CType(216, Byte), CType(254, Byte))
            Me.GridView1.Appearance.ColumnFilterButtonActive.ForeColor = System.Drawing.Color.Black
            Me.GridView1.Appearance.ColumnFilterButtonActive.Options.UseBackColor = True
            Me.GridView1.Appearance.ColumnFilterButtonActive.Options.UseBorderColor = True
            Me.GridView1.Appearance.ColumnFilterButtonActive.Options.UseForeColor = True
            Me.GridView1.Appearance.Empty.BackColor = System.Drawing.Color.FromArgb(CType(236, Byte), CType(246, Byte), CType(255, Byte))
            Me.GridView1.Appearance.Empty.BackColor2 = System.Drawing.Color.White
            Me.GridView1.Appearance.Empty.Options.UseBackColor = True
            Me.GridView1.Appearance.EvenRow.BackColor = System.Drawing.Color.FromArgb(CType(247, Byte), CType(251, Byte), CType(255, Byte))
            Me.GridView1.Appearance.EvenRow.BorderColor = System.Drawing.Color.FromArgb(CType(247, Byte), CType(251, Byte), CType(255, Byte))
            Me.GridView1.Appearance.EvenRow.ForeColor = System.Drawing.Color.Black
            Me.GridView1.Appearance.EvenRow.Options.UseBackColor = True
            Me.GridView1.Appearance.EvenRow.Options.UseBorderColor = True
            Me.GridView1.Appearance.EvenRow.Options.UseForeColor = True
            Me.GridView1.Appearance.FilterCloseButton.BackColor = System.Drawing.Color.FromArgb(CType(104, Byte), CType(184, Byte), CType(251, Byte))
            Me.GridView1.Appearance.FilterCloseButton.BorderColor = System.Drawing.Color.FromArgb(CType(104, Byte), CType(184, Byte), CType(251, Byte))
            Me.GridView1.Appearance.FilterCloseButton.ForeColor = System.Drawing.Color.White
            Me.GridView1.Appearance.FilterCloseButton.Options.UseBackColor = True
            Me.GridView1.Appearance.FilterCloseButton.Options.UseBorderColor = True
            Me.GridView1.Appearance.FilterCloseButton.Options.UseForeColor = True
            Me.GridView1.Appearance.FilterPanel.BackColor = System.Drawing.Color.FromArgb(CType(236, Byte), CType(246, Byte), CType(255, Byte))
            Me.GridView1.Appearance.FilterPanel.BackColor2 = System.Drawing.Color.White
            Me.GridView1.Appearance.FilterPanel.ForeColor = System.Drawing.Color.Black
            Me.GridView1.Appearance.FilterPanel.Options.UseBackColor = True
            Me.GridView1.Appearance.FilterPanel.Options.UseForeColor = True
            Me.GridView1.Appearance.FixedLine.BackColor = System.Drawing.Color.FromArgb(CType(59, Byte), CType(133, Byte), CType(195, Byte))
            Me.GridView1.Appearance.FixedLine.Options.UseBackColor = True
            Me.GridView1.Appearance.FocusedCell.BackColor = System.Drawing.Color.White
            Me.GridView1.Appearance.FocusedCell.ForeColor = System.Drawing.Color.Black
            Me.GridView1.Appearance.FocusedCell.Options.UseBackColor = True
            Me.GridView1.Appearance.FocusedCell.Options.UseForeColor = True
            Me.GridView1.Appearance.FocusedRow.BackColor = System.Drawing.Color.FromArgb(CType(38, Byte), CType(109, Byte), CType(189, Byte))
            Me.GridView1.Appearance.FocusedRow.BorderColor = System.Drawing.Color.FromArgb(CType(59, Byte), CType(139, Byte), CType(206, Byte))
            Me.GridView1.Appearance.FocusedRow.ForeColor = System.Drawing.Color.White
            Me.GridView1.Appearance.FocusedRow.Options.UseBackColor = True
            Me.GridView1.Appearance.FocusedRow.Options.UseBorderColor = True
            Me.GridView1.Appearance.FocusedRow.Options.UseForeColor = True
            Me.GridView1.Appearance.FooterPanel.BackColor = System.Drawing.Color.FromArgb(CType(104, Byte), CType(184, Byte), CType(251, Byte))
            Me.GridView1.Appearance.FooterPanel.BorderColor = System.Drawing.Color.FromArgb(CType(104, Byte), CType(184, Byte), CType(251, Byte))
            Me.GridView1.Appearance.FooterPanel.ForeColor = System.Drawing.Color.Black
            Me.GridView1.Appearance.FooterPanel.Options.UseBackColor = True
            Me.GridView1.Appearance.FooterPanel.Options.UseBorderColor = True
            Me.GridView1.Appearance.FooterPanel.Options.UseForeColor = True
            Me.GridView1.Appearance.GroupButton.BackColor = System.Drawing.Color.FromArgb(CType(104, Byte), CType(184, Byte), CType(251, Byte))
            Me.GridView1.Appearance.GroupButton.BorderColor = System.Drawing.Color.FromArgb(CType(104, Byte), CType(184, Byte), CType(251, Byte))
            Me.GridView1.Appearance.GroupButton.Options.UseBackColor = True
            Me.GridView1.Appearance.GroupButton.Options.UseBorderColor = True
            Me.GridView1.Appearance.GroupFooter.BackColor = System.Drawing.Color.FromArgb(CType(170, Byte), CType(216, Byte), CType(254, Byte))
            Me.GridView1.Appearance.GroupFooter.BorderColor = System.Drawing.Color.FromArgb(CType(170, Byte), CType(216, Byte), CType(254, Byte))
            Me.GridView1.Appearance.GroupFooter.ForeColor = System.Drawing.Color.Black
            Me.GridView1.Appearance.GroupFooter.Options.UseBackColor = True
            Me.GridView1.Appearance.GroupFooter.Options.UseBorderColor = True
            Me.GridView1.Appearance.GroupFooter.Options.UseForeColor = True
            Me.GridView1.Appearance.GroupPanel.BackColor = System.Drawing.Color.FromArgb(CType(236, Byte), CType(246, Byte), CType(255, Byte))
            Me.GridView1.Appearance.GroupPanel.BackColor2 = System.Drawing.Color.White
            Me.GridView1.Appearance.GroupPanel.ForeColor = System.Drawing.Color.Black
            Me.GridView1.Appearance.GroupPanel.Options.UseBackColor = True
            Me.GridView1.Appearance.GroupPanel.Options.UseForeColor = True
            Me.GridView1.Appearance.GroupRow.BackColor = System.Drawing.Color.FromArgb(CType(170, Byte), CType(216, Byte), CType(254, Byte))
            Me.GridView1.Appearance.GroupRow.BorderColor = System.Drawing.Color.FromArgb(CType(170, Byte), CType(216, Byte), CType(254, Byte))
            Me.GridView1.Appearance.GroupRow.ForeColor = System.Drawing.Color.Black
            Me.GridView1.Appearance.GroupRow.Options.UseBackColor = True
            Me.GridView1.Appearance.GroupRow.Options.UseBorderColor = True
            Me.GridView1.Appearance.GroupRow.Options.UseForeColor = True
            Me.GridView1.Appearance.HeaderPanel.BackColor = System.Drawing.Color.FromArgb(CType(139, Byte), CType(201, Byte), CType(254, Byte))
            Me.GridView1.Appearance.HeaderPanel.BorderColor = System.Drawing.Color.FromArgb(CType(139, Byte), CType(201, Byte), CType(254, Byte))
            Me.GridView1.Appearance.HeaderPanel.ForeColor = System.Drawing.Color.Black
            Me.GridView1.Appearance.HeaderPanel.Options.UseBackColor = True
            Me.GridView1.Appearance.HeaderPanel.Options.UseBorderColor = True
            Me.GridView1.Appearance.HeaderPanel.Options.UseForeColor = True
            Me.GridView1.Appearance.HideSelectionRow.BackColor = System.Drawing.Color.FromArgb(CType(105, Byte), CType(170, Byte), CType(225, Byte))
            Me.GridView1.Appearance.HideSelectionRow.BorderColor = System.Drawing.Color.FromArgb(CType(83, Byte), CType(155, Byte), CType(215, Byte))
            Me.GridView1.Appearance.HideSelectionRow.ForeColor = System.Drawing.Color.FromArgb(CType(236, Byte), CType(246, Byte), CType(255, Byte))
            Me.GridView1.Appearance.HideSelectionRow.Options.UseBackColor = True
            Me.GridView1.Appearance.HideSelectionRow.Options.UseBorderColor = True
            Me.GridView1.Appearance.HideSelectionRow.Options.UseForeColor = True
            Me.GridView1.Appearance.HorzLine.BackColor = System.Drawing.Color.FromArgb(CType(104, Byte), CType(184, Byte), CType(251, Byte))
            Me.GridView1.Appearance.HorzLine.Options.UseBackColor = True
            Me.GridView1.Appearance.OddRow.BackColor = System.Drawing.Color.FromArgb(CType(236, Byte), CType(246, Byte), CType(255, Byte))
            Me.GridView1.Appearance.OddRow.BorderColor = System.Drawing.Color.FromArgb(CType(236, Byte), CType(246, Byte), CType(255, Byte))
            Me.GridView1.Appearance.OddRow.ForeColor = System.Drawing.Color.Black
            Me.GridView1.Appearance.OddRow.Options.UseBackColor = True
            Me.GridView1.Appearance.OddRow.Options.UseBorderColor = True
            Me.GridView1.Appearance.OddRow.Options.UseForeColor = True
            Me.GridView1.Appearance.Preview.Font = New System.Drawing.Font("Verdana", 7.5!)
            Me.GridView1.Appearance.Preview.ForeColor = System.Drawing.Color.FromArgb(CType(83, Byte), CType(155, Byte), CType(215, Byte))
            Me.GridView1.Appearance.Preview.Options.UseFont = True
            Me.GridView1.Appearance.Preview.Options.UseForeColor = True
            Me.GridView1.Appearance.Row.BackColor = System.Drawing.Color.FromArgb(CType(247, Byte), CType(251, Byte), CType(255, Byte))
            Me.GridView1.Appearance.Row.ForeColor = System.Drawing.Color.Black
            Me.GridView1.Appearance.Row.Options.UseBackColor = True
            Me.GridView1.Appearance.Row.Options.UseForeColor = True
            Me.GridView1.Appearance.RowSeparator.BackColor = System.Drawing.Color.FromArgb(CType(236, Byte), CType(246, Byte), CType(255, Byte))
            Me.GridView1.Appearance.RowSeparator.BackColor2 = System.Drawing.Color.White
            Me.GridView1.Appearance.RowSeparator.Options.UseBackColor = True
            Me.GridView1.Appearance.SelectedRow.BackColor = System.Drawing.Color.FromArgb(CType(83, Byte), CType(155, Byte), CType(215, Byte))
            Me.GridView1.Appearance.SelectedRow.ForeColor = System.Drawing.Color.White
            Me.GridView1.Appearance.SelectedRow.Options.UseBackColor = True
            Me.GridView1.Appearance.SelectedRow.Options.UseForeColor = True
            Me.GridView1.Appearance.TopNewRow.BackColor = System.Drawing.Color.White
            Me.GridView1.Appearance.TopNewRow.Options.UseBackColor = True
            Me.GridView1.Appearance.VertLine.BackColor = System.Drawing.Color.FromArgb(CType(104, Byte), CType(184, Byte), CType(251, Byte))
            Me.GridView1.Appearance.VertLine.Options.UseBackColor = True
            Me.GridView1.Columns.AddRange(New DevExpress.XtraGrid.Columns.GridColumn() {Me.GridColumn_sic, Me.GridColumn_creditor, Me.GridColumn_CreditorName})
            Me.GridView1.GridControl = Me.GridControl1
            Me.GridView1.Name = "GridView1"
            Me.GridView1.OptionsBehavior.Editable = False
            Me.GridView1.OptionsView.ShowGroupPanel = False
            Me.GridView1.OptionsView.ShowIndicator = False
            Me.GridView1.SortInfo.AddRange(New DevExpress.XtraGrid.Columns.GridColumnSortInfo() {New DevExpress.XtraGrid.Columns.GridColumnSortInfo(Me.GridColumn_creditor, DevExpress.Data.ColumnSortOrder.Ascending)})
            '
            'GridColumn_sic
            '
            Me.GridColumn_sic.Caption = "SIC"
            Me.GridColumn_sic.CustomizationCaption = "S.I.C."
            Me.GridColumn_sic.DisplayFormat.FormatString = "A0000-00-0000"
            Me.GridColumn_sic.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Custom
            Me.GridColumn_sic.FieldName = "sic"
            Me.GridColumn_sic.GroupFormat.FormatString = "A0000-00-0000"
            Me.GridColumn_sic.GroupFormat.FormatType = DevExpress.Utils.FormatType.Custom
            Me.GridColumn_sic.GroupInterval = DevExpress.XtraGrid.ColumnGroupInterval.Alphabetical
            Me.GridColumn_sic.Name = "GridColumn_sic"
            Me.GridColumn_sic.Visible = True
            Me.GridColumn_sic.VisibleIndex = 0
            Me.GridColumn_sic.Width = 101
            '
            'GridColumn_creditor
            '
            Me.GridColumn_creditor.Caption = "Creditor"
            Me.GridColumn_creditor.CustomizationCaption = "Creditor ID"
            Me.GridColumn_creditor.FieldName = "creditor"
            Me.GridColumn_creditor.GroupInterval = DevExpress.XtraGrid.ColumnGroupInterval.Alphabetical
            Me.GridColumn_creditor.Name = "GridColumn_creditor"
            Me.GridColumn_creditor.SortMode = DevExpress.XtraGrid.ColumnSortMode.Value
            Me.GridColumn_creditor.Visible = True
            Me.GridColumn_creditor.VisibleIndex = 1
            Me.GridColumn_creditor.Width = 92
            '
            'GridColumn_CreditorName
            '
            Me.GridColumn_CreditorName.Caption = "Name"
            Me.GridColumn_CreditorName.CustomizationCaption = "Creditor Name"
            Me.GridColumn_CreditorName.FieldName = "creditor_name"
            Me.GridColumn_CreditorName.GroupInterval = DevExpress.XtraGrid.ColumnGroupInterval.Value
            Me.GridColumn_CreditorName.Name = "GridColumn_CreditorName"
            Me.GridColumn_CreditorName.Visible = True
            Me.GridColumn_CreditorName.VisibleIndex = 2
            Me.GridColumn_CreditorName.Width = 203
            '
            'Form_Creditors
            '
            Me.AutoScaleBaseSize = New System.Drawing.Size(5, 14)
            Me.ClientSize = New System.Drawing.Size(504, 266)
            Me.Controls.Add(Me.GridControl1)
            Me.Controls.Add(Me.SimpleButton_Cancel)
            Me.Controls.Add(Me.SimpleButton_Edit)
            Me.Name = "Form_Creditors"
            Me.ToolTipController1.SetSuperTip(Me, Nothing)
            Me.Text = "Policy Matrix - Creditor Listing"

            CType(Me.GridControl1, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.GridView1, System.ComponentModel.ISupportInitialize).EndInit()
            Me.ResumeLayout(False)
        End Sub

#End Region

        Private Sub SimpleButton_Cancel_Click(ByVal sender As Object, ByVal e As EventArgs)
            Me.Close()
        End Sub

        Private Sub SimpleButton_Edit_Click(ByVal sender As Object, ByVal e As EventArgs)
            Dim drv As DataRowView = Nothing

            If GridView1.FocusedRowHandle >= 0 Then
                Dim vue As DataView = CType(GridControl1.DataSource, DataView)
                drv = CType(GridView1.GetRow(GridView1.FocusedRowHandle), DataRowView)
            End If

            ' If there is a row then perform the edit operation. If successful, complete the changes else roll them back
            If drv IsNot Nothing Then EditItem(drv)
        End Sub

        ''' <summary>
        ''' Process a change in the current row for the control
        ''' </summary>
        Private Sub GridView1_FocusedRowChanged(ByVal sender As Object, ByVal e As FocusedRowChangedEventArgs)
            Dim gv As GridView = CType(sender, GridView)
            Dim ctl As GridControl = gv.GridControl
            Dim ControlRow As Int32 = e.FocusedRowHandle

            ' Find the datarowview from the input tables.
            Dim drv As DataRowView = Nothing
            If ControlRow >= 0 Then
                Dim vue As DataView = CType(ctl.DataSource, DataView)
                drv = CType(gv.GetRow(ControlRow), DataRowView)
            End If
        End Sub

        Private Sub GridView1_DoubleClick(ByVal sender As Object, ByVal e As EventArgs)

            ' Find the row targetted as the double-click item
            Dim gv As GridView = CType(sender, GridView)
            Dim ctl As GridControl = CType(gv.GridControl, GridControl)
            Dim hi As GridHitInfo = gv.CalcHitInfo((ctl.PointToClient(MousePosition)))
            Dim ControlRow As Int32 = hi.RowHandle

            ' Find the datarowview from the input tables.
            Dim drv As DataRowView = Nothing
            If ControlRow >= 0 Then
                Dim vue As DataView = CType(ctl.DataSource, DataView)
                drv = CType(gv.GetRow(ControlRow), DataRowView)
            End If

            ' If there is a row then perform the edit operation. If successful, complete the changes else roll them back
            If drv IsNot Nothing Then EditItem(drv)
        End Sub

        Private Sub GridView1_MouseDown(ByVal sender As Object, ByVal e As MouseEventArgs)
            'Dim gv As DevExpress.XtraGrid.Views.Grid.GridView = GridView1
            'Dim ctl As DevExpress.XtraGrid.GridControl = CType(gv.GridControl, DevExpress.XtraGrid.GridControl)
            'Dim hi As DevExpress.XtraGrid.Views.Grid.ViewInfo.GridHitInfo = gv.CalcHitInfo(New Point(e.X, e.Y))
        End Sub

        Private Sub MyGridControl_Load(ByVal sender As Object, ByVal e As EventArgs)

            ' Define the form information
            Dim cmd As SqlCommand = New SqlCommand
            With cmd
                .Connection = New SqlConnection(DebtPlus.LINQ.SQLInfoClass.getDefault().ConnectionString)
                .CommandText = "SELECT creditor_id, creditor, sic, creditor_name FROM creditors WITH (NOLOCK)"
            End With

            Dim current_cursor As Cursor = Cursor.Current
            Try
                Cursor.Current = Cursors.WaitCursor
                Dim da As New SqlDataAdapter(cmd)
                da.Fill(_ds, "creditors")

                With GridControl1
                    .DataSource = _ds.Tables("creditors").DefaultView
                    .RefreshDataSource()
                End With

                ' Disable the edit operation if there are no creditors to edit???
                If _ds.Tables("creditors").Rows.Count <= 0 Then Me.SimpleButton_Edit.Enabled = False

                With GridView1
                    .BestFitColumns()
                    .FocusedRowHandle = -1
                End With

            Catch ex As SqlClient.SqlException
                DebtPlus.UI.Common.ErrorHandling.HandleErrors(ex, "Error reading creditor list")

            Finally
                Cursor.Current = current_cursor
            End Try

            ' Add the handlers for the events
            AddHandler GridView1.DoubleClick, AddressOf GridView1_DoubleClick
            AddHandler GridView1.FocusedRowChanged, AddressOf GridView1_FocusedRowChanged
        End Sub

        Private Sub EditItem(ByVal drv As DataRowView)
            With New Form_PolicyMatrix(_ds, drv)
                .ShowDialog()
                .Dispose()
            End With
        End Sub

        'Form overrides dispose to clean up the component list.
        Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
            If disposing Then
                If components IsNot Nothing Then
                    components.Dispose()
                End If
            End If
            MyBase.Dispose(disposing)
        End Sub
    End Class
End Namespace
