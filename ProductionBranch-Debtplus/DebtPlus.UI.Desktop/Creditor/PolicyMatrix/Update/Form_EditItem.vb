#Region "Copyright 2000-2012 DebtPlus, L.L.C."
'{*******************************************************************}
'{                                                                   }
'{       DebtPlus Debt Management System                             }
'{                                                                   }
'{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
'{       ALL RIGHTS RESERVED                                         }
'{                                                                   }
'{   The entire contents of this file is protected by U.S. and       }
'{   International Copyright Laws. Unauthorized reproduction,        }
'{   reverse-engineering, and distribution of all or any portion of  }
'{   the code contained in this file is strictly prohibited and may  }
'{   result in severe civil and criminal penalties and will be       }
'{   prosecuted to the maximum extent possible under the law.        }
'{                                                                   }
'{   RESTRICTIONS                                                    }
'{                                                                   }
'{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
'{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
'{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
'{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
'{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
'{                                                                   }
'{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
'{   ADDITIONAL RESTRICTIONS.                                        }
'{                                                                   }
'{*******************************************************************}

#End Region

Option Compare Binary
Option Explicit On
Option Strict On

Imports DebtPlus.Data.Forms
Imports System.Windows.Forms

Namespace Creditor.PolicyMatrix.Update
    Friend Class Form_EditItem
        Inherits DebtPlusForm

        Private drv As DataRowView = Nothing
        Private creditor_label As String = String.Empty

        Public Sub New()
            MyBase.New()
            InitializeComponent()
            AddHandler Me.Load, AddressOf Form_EditItem_Load
        End Sub

        Public Sub New(ByVal drv As DataRowView, ByVal creditor_label As String)
            MyClass.New()
            Me.drv = drv
            Me.creditor_label = creditor_label
        End Sub

#Region " Windows Form Designer generated code "

        'Form overrides dispose to clean up the component list.
        Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
            If disposing Then
                If components IsNot Nothing Then
                    components.Dispose()
                End If
            End If
            MyBase.Dispose(disposing)
        End Sub

        'Required by the Windows Form Designer
        Private components As System.ComponentModel.IContainer

        'NOTE: The following procedure is required by the Windows Form Designer
        'It can be modified using the Windows Form Designer.  
        'Do not modify it using the code editor.
        Friend WithEvents LabelControl1 As DevExpress.XtraEditors.LabelControl
        Friend WithEvents LabelControl2 As DevExpress.XtraEditors.LabelControl
        Friend WithEvents LabelControl3 As DevExpress.XtraEditors.LabelControl
        Friend WithEvents LabelControl4 As DevExpress.XtraEditors.LabelControl
        Friend WithEvents Button_OK As DevExpress.XtraEditors.SimpleButton
        Friend WithEvents Button_Cancel As DevExpress.XtraEditors.SimpleButton
        Friend WithEvents message As DevExpress.XtraEditors.MemoEdit
        Friend WithEvents CopyToSIC As DevExpress.XtraEditors.CheckEdit
        Friend WithEvents value As DevExpress.XtraEditors.TextEdit
        Friend WithEvents label_description As DevExpress.XtraEditors.LabelControl
        Friend WithEvents label_creditor As DevExpress.XtraEditors.LabelControl

        <System.Diagnostics.DebuggerStepThrough()>
        Private Sub InitializeComponent()
            Me.LabelControl1 = New DevExpress.XtraEditors.LabelControl()
            Me.LabelControl2 = New DevExpress.XtraEditors.LabelControl()
            Me.LabelControl3 = New DevExpress.XtraEditors.LabelControl()
            Me.LabelControl4 = New DevExpress.XtraEditors.LabelControl()
            Me.message = New DevExpress.XtraEditors.MemoEdit()
            Me.CopyToSIC = New DevExpress.XtraEditors.CheckEdit()
            Me.Button_OK = New DevExpress.XtraEditors.SimpleButton()
            Me.Button_Cancel = New DevExpress.XtraEditors.SimpleButton()
            Me.value = New DevExpress.XtraEditors.TextEdit()
            Me.label_description = New DevExpress.XtraEditors.LabelControl()
            Me.label_creditor = New DevExpress.XtraEditors.LabelControl()
            CType(Me.DxErrorProvider1, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.message.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.CopyToSIC.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.value.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
            Me.SuspendLayout()
            '
            'LabelControl1
            '
            Me.LabelControl1.Location = New System.Drawing.Point(8, 8)
            Me.LabelControl1.Name = "LabelControl1"
            Me.LabelControl1.Size = New System.Drawing.Size(39, 13)
            Me.LabelControl1.TabIndex = 0
            Me.LabelControl1.Text = "Creditor"
            '
            'LabelControl2
            '
            Me.LabelControl2.Location = New System.Drawing.Point(8, 24)
            Me.LabelControl2.Name = "LabelControl2"
            Me.LabelControl2.Size = New System.Drawing.Size(53, 13)
            Me.LabelControl2.TabIndex = 2
            Me.LabelControl2.Text = "Description"
            '
            'LabelControl3
            '
            Me.LabelControl3.Location = New System.Drawing.Point(8, 48)
            Me.LabelControl3.Name = "LabelControl3"
            Me.LabelControl3.Size = New System.Drawing.Size(26, 13)
            Me.LabelControl3.TabIndex = 4
            Me.LabelControl3.Text = "Value"
            '
            'LabelControl4
            '
            Me.LabelControl4.Location = New System.Drawing.Point(8, 72)
            Me.LabelControl4.Name = "LabelControl4"
            Me.LabelControl4.Size = New System.Drawing.Size(23, 13)
            Me.LabelControl4.TabIndex = 6
            Me.LabelControl4.Text = "Note"
            '
            'message
            '
            Me.message.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                Or System.Windows.Forms.AnchorStyles.Left) _
                Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
            Me.message.Location = New System.Drawing.Point(88, 72)
            Me.message.Name = "message"
            Me.message.Properties.MaxLength = 1024
            Me.message.Size = New System.Drawing.Size(288, 96)
            Me.message.TabIndex = 7
            '
            'CopyToSIC
            '
            Me.CopyToSIC.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
            Me.CopyToSIC.Location = New System.Drawing.Point(8, 176)
            Me.CopyToSIC.Name = "CopyToSIC"
            Me.CopyToSIC.Properties.Caption = "Copy to all creditors of the same SIC code?"
            Me.CopyToSIC.Size = New System.Drawing.Size(232, 19)
            Me.CopyToSIC.TabIndex = 8
            '
            'Button_OK
            '
            Me.Button_OK.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
            Me.Button_OK.DialogResult = System.Windows.Forms.DialogResult.OK
            Me.Button_OK.Location = New System.Drawing.Point(304, 8)
            Me.Button_OK.Name = "Button_OK"
            Me.Button_OK.Size = New System.Drawing.Size(75, 23)
            Me.Button_OK.TabIndex = 9
            Me.Button_OK.Text = "&OK"
            '
            'Button_Cancel
            '
            Me.Button_Cancel.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
            Me.Button_Cancel.DialogResult = System.Windows.Forms.DialogResult.Cancel
            Me.Button_Cancel.Location = New System.Drawing.Point(304, 40)
            Me.Button_Cancel.Name = "Button_Cancel"
            Me.Button_Cancel.Size = New System.Drawing.Size(75, 23)
            Me.Button_Cancel.TabIndex = 10
            Me.Button_Cancel.Text = "&Cancel"
            '
            'value
            '
            Me.value.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
            Me.value.Location = New System.Drawing.Point(88, 48)
            Me.value.Name = "value"
            Me.value.Properties.MaxLength = 1024
            Me.value.Size = New System.Drawing.Size(200, 20)
            Me.value.TabIndex = 5
            '
            'label_description
            '
            Me.label_description.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
            Me.label_description.Location = New System.Drawing.Point(88, 24)
            Me.label_description.Name = "label_description"
            Me.label_description.Size = New System.Drawing.Size(0, 13)
            Me.label_description.TabIndex = 3
            '
            'label_creditor
            '
            Me.label_creditor.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
            Me.label_creditor.Location = New System.Drawing.Point(88, 8)
            Me.label_creditor.Name = "label_creditor"
            Me.label_creditor.Size = New System.Drawing.Size(0, 13)
            Me.label_creditor.TabIndex = 1
            '
            'Form_EditItem
            '
            Me.AcceptButton = Me.Button_OK
            Me.AutoScaleBaseSize = New System.Drawing.Size(5, 14)
            Me.CancelButton = Me.Button_Cancel
            Me.ClientSize = New System.Drawing.Size(392, 206)
            Me.Controls.Add(Me.label_description)
            Me.Controls.Add(Me.label_creditor)
            Me.Controls.Add(Me.value)
            Me.Controls.Add(Me.Button_Cancel)
            Me.Controls.Add(Me.Button_OK)
            Me.Controls.Add(Me.CopyToSIC)
            Me.Controls.Add(Me.message)
            Me.Controls.Add(Me.LabelControl4)
            Me.Controls.Add(Me.LabelControl3)
            Me.Controls.Add(Me.LabelControl2)
            Me.Controls.Add(Me.LabelControl1)
            Me.Name = "Form_EditItem"
            Me.Text = "Policy Matrix - Policy Information"
            CType(Me.DxErrorProvider1, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.message.Properties, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.CopyToSIC.Properties, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.value.Properties, System.ComponentModel.ISupportInitialize).EndInit()
            Me.ResumeLayout(False)
            Me.PerformLayout()

        End Sub

#End Region

        Private Sub Form_EditItem_Load(ByVal sender As Object, ByVal e As EventArgs)

            ' Copy the creditor name from the parent
            label_creditor.Text = creditor_label

            ' Bind the controls and let it run
            label_description.DataBindings.Add(New Binding("Text", drv, "description"))
            value.DataBindings.Add(New Binding("EditValue", drv, "item_value"))
            message.DataBindings.Add(New Binding("EditValue", drv, "message"))
            CopyToSIC.DataBindings.Add(New Binding("EditValue", drv, "CopyToSIC"))
        End Sub
    End Class
End Namespace
