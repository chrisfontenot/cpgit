Option Compare Binary
Option Explicit On
Option Strict On

Imports System.Text.RegularExpressions

Namespace Creditor.PolicyMatrix.Update
    Public Class CreditorPolicyMatrixFormatter
        Inherits Object
        Implements ICustomFormatter, IFormatProvider

        Public Function Format(ByVal FormatCode As String, ByVal arg As Object, ByVal formatProvider As IFormatProvider) As String Implements ICustomFormatter.Format
            If arg Is Nothing OrElse arg Is DBNull.Value Then Return String.Empty
            Dim argString As String = Convert.ToString(arg)

            ' If the value is un-formatted then format it.
            Dim rx As New Regex("^[A-Z]\d{10}$", RegexOptions.Singleline Or RegexOptions.IgnoreCase)
            If rx.IsMatch(argString) Then
                argString = argString.Substring(0, 5) + "-" + argString.Substring(5, 2) + "-" + argString.Substring(7, 4)
            End If

            Return argString
        End Function

        Public Function GetFormat(ByVal formatType As Type) As Object Implements IFormatProvider.GetFormat
            Return Me
        End Function
    End Class
End Namespace