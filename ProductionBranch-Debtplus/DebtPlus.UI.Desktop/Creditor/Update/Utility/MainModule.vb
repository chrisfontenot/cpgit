#Region "Copyright 2000-2012 DebtPlus, L.L.C."
'{*******************************************************************}
'{                                                                   }
'{       DebtPlus Debt Management System                             }
'{                                                                   }
'{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
'{       ALL RIGHTS RESERVED                                         }
'{                                                                   }
'{   The entire contents of this file is protected by U.S. and       }
'{   International Copyright Laws. Unauthorized reproduction,        }
'{   reverse-engineering, and distribution of all or any portion of  }
'{   the code contained in this file is strictly prohibited and may  }
'{   result in severe civil and criminal penalties and will be       }
'{   prosecuted to the maximum extent possible under the law.        }
'{                                                                   }
'{   RESTRICTIONS                                                    }
'{                                                                   }
'{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
'{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
'{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
'{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
'{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
'{                                                                   }
'{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
'{   ADDITIONAL RESTRICTIONS.                                        }
'{                                                                   }
'{*******************************************************************}

#End Region

Option Compare Binary
Option Explicit On
Option Strict On

Imports System.Threading
Imports DebtPlus.Interfaces.Creditor
Imports DebtPlus.UI.Creditor.Widgets.Search
Imports DebtPlus.Interfaces.Desktop
Imports System.Windows.Forms

Namespace Creditor.Update.Utility
    Public Class Mainline
        Implements IDesktopMainline

        Private creditorId As String = String.Empty
        Private _ap As CreditorUpdateArgParser = Nothing

        Public Sub OldAppMain(ByVal args() As String) Implements IDesktopMainline.OldAppMain
            _ap = New CreditorUpdateArgParser
            If Not _ap.Parse(args) Then Return

            creditorId = _ap.Creditor

            ' Start a thread to return to the menu utility as soon as possible
            Dim thrd As New Thread(New ParameterizedThreadStart(AddressOf UpdateProcedure))
            thrd.SetApartmentState(ApartmentState.STA)
            thrd.IsBackground = False
            thrd.Name = "CreditorUpdate"
            thrd.Start()
        End Sub

        Private Sub UpdateProcedure(ByVal obj As Object)
            If _ap Is Nothing Then
                Return
            End If

            ' If a search operation is desired then do it now.
            If String.IsNullOrEmpty(creditorId) Then
                Using search As New CreditorSearchClass()
                    If search.PerformCreditorSearch() <> DialogResult.OK Then
                        Return
                    End If

                    creditorId = search.Creditor
                    If String.IsNullOrEmpty(creditorId) Then
                        Return
                    End If
                End Using
            End If

            ' If there is no creditor then we did not choose a creditor

            ' If alerts are to be shown then display them
            If _ap.ShowAlerts Then
                Dim thrd As New Thread(AddressOf ShowAlerts)
                With thrd
                    .SetApartmentState(ApartmentState.STA)
                    .Name = "ShowAlerts"
                    .Start()
                End With
            End If

            Dim bc As New DebtPlus.LINQ.BusinessContext()
            Try
                Dim creditorRecord As DebtPlus.LINQ.creditor = bc.creditors.Where(Function(s) s.Id = creditorId).FirstOrDefault()
                If creditorRecord Is Nothing Then
                    DebtPlus.Data.Forms.MessageBox.Show("The creditor is no longer in the system. The edit operation is cancelled.", "Sorry, can not find the creditor", MessageBoxButtons.OK, MessageBoxIcon.Error)
                    Return
                End If

                ' Update the MRU with the new creditor so that it will show in the list
                Using mru As New DebtPlus.Data.MRU("Creditors")
                    mru.InsertTopMost(creditorRecord.Id)
                    mru.SaveKey()
                End Using

                ' Add the handler to generate the system note
                AddHandler bc.BeforeSubmitChanges, AddressOf bc.RecordSystemNoteHandler

                ' We have a creditor record. Do the edit operation on the new creditor
                Using frm As New DebtPlus.UI.Creditor.Update.Forms.Form_CreditorUpdate(bc, creditorRecord, False)
                    frm.ShowDialog()
                End Using

                ' Submit the changes before we leave.
                bc.SubmitChanges()

            Finally
                RemoveHandler bc.BeforeSubmitChanges, AddressOf bc.RecordSystemNoteHandler
                bc.Dispose()
            End Try
        End Sub

        Private Sub ShowAlerts(ByVal arg As Object)
            Using search As IAlertNotes = New DebtPlus.Notes.AlertNotes.Creditor()
                search.ShowAlerts(creditorId)
            End Using
        End Sub
    End Class
End Namespace
