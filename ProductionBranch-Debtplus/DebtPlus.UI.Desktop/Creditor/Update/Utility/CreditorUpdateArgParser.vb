Option Compare Binary
Option Explicit On 
Option Strict On

Namespace Creditor.Update.Utility
    Friend Class CreditorUpdateArgParser
        Inherits DebtPlus.Utils.ArgParserBase

        Private _ShowAlerts As Boolean = True
        Private _Search As Boolean = False
        Private _Creditor As String = String.Empty

        ''' <summary>
        ''' Return the flag for alert notes
        ''' </summary>
        Public ReadOnly Property ShowAlerts() As Boolean
            Get
                Return _ShowAlerts
            End Get
        End Property

        ''' <summary>
        ''' Return the search status
        ''' </summary>
        Public ReadOnly Property Search() As Boolean
            Get
                Return _Search
            End Get
        End Property

        ''' <summary>
        ''' Return the creidtor ID
        ''' </summary>
        Public ReadOnly Property Creditor() As String
            Get
                Return _Creditor
            End Get
        End Property

        ''' <summary>
        ''' Create an instance of our class
        ''' </summary>
        Public Sub New()
            MyBase.New(New String() {"a", "s", "c"})
        End Sub

        ''' <summary>
        ''' Display error information
        ''' </summary>
        Protected Overrides Sub OnUsage(ByVal errorInfo As String)
            'AppendErrorLine("Usage: " + Fname + " [-a] [-s] [-c creditor]")
            'AppendErrorLine("       -a   Supress alert notes")
            'AppendErrorLine("       -s   Search for the creditor. Result written to console.")
            'AppendErrorLine("       -c   Creditor ID to be updated")
        End Sub

        ''' <summary>
        ''' Handle an option switch
        ''' </summary>
        Protected Overrides Function onSwitch(ByVal switchSymbol As String, ByVal switchValue As String) As SwitchStatus
            Dim ss As SwitchStatus = DebtPlus.Utils.ArgParserBase.SwitchStatus.NoError
            Select Case switchSymbol
                Case "?" ' User wants to see Usage
                    ss = DebtPlus.Utils.ArgParserBase.SwitchStatus.ShowUsage

                Case "a" ' supress alert notes
                    _ShowAlerts = False

                Case "s" ' search
                    _Search = True

                Case "c" ' creditor
                    ss = SetCreditor(switchValue)

                Case Else
                    ss = DebtPlus.Utils.ArgParserBase.SwitchStatus.YesError
            End Select
            Return ss
        End Function

        ''' <summary>
        ''' Handle a non-switched option
        ''' </summary>
        Protected Overrides Function OnNonSwitch(ByVal Value As String) As SwitchStatus
            Return SetCreditor(Value)
        End Function

        ''' <summary>
        ''' Set the creditor for processing
        ''' </summary>
        Private Function SetCreditor(ByVal Value As String) As SwitchStatus
            Dim ss As SwitchStatus = DebtPlus.Utils.ArgParserBase.SwitchStatus.NoError
            Dim rx As New System.Text.RegularExpressions.Regex("[A-Za-z][0-9]{4,}")
            If rx.IsMatch(Value) Then
                _Creditor = Value
            Else
                AppendErrorLine("Invalid creditor ID: " + Value)
                ss = DebtPlus.Utils.ArgParserBase.SwitchStatus.ShowUsage
            End If
            Return ss
        End Function
    End Class
End Namespace
