﻿Imports DebtPlus.UI.Client.Widgets.Controls

Namespace Transfer.Oper.Client
    <Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
    Partial Class Form_Transfer_OperClient
        Inherits DebtPlus.Data.Forms.DebtPlusForm

        'Form overrides dispose to clean up the component list.
        <System.Diagnostics.DebuggerNonUserCode()> _
        Protected Overrides Sub Dispose(ByVal disposing As Boolean)
            Try
                If disposing AndAlso components IsNot Nothing Then
                    components.Dispose()
                End If
            Finally
                MyBase.Dispose(disposing)
            End Try
        End Sub

        'Required by the Windows Form Designer
        Private components As System.ComponentModel.IContainer

        'NOTE: The following procedure is required by the Windows Form Designer
        'It can be modified using the Windows Form Designer.  
        'Do not modify it using the code editor.
        <System.Diagnostics.DebuggerStepThrough()> _
        Private Sub InitializeComponent()
            Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(Form_Transfer_OperClient))
            Dim SerializableAppearanceObject1 As DevExpress.Utils.SerializableAppearanceObject = New DevExpress.Utils.SerializableAppearanceObject()
            Me.lbl_new_dest_trust = New DevExpress.XtraEditors.LabelControl()
            Me.Label8 = New DevExpress.XtraEditors.LabelControl()
            Me.Label9 = New DevExpress.XtraEditors.LabelControl()
            Me.lbl_old_dest_trust = New DevExpress.XtraEditors.LabelControl()
            Me.Label11 = New DevExpress.XtraEditors.LabelControl()
            Me.med_TargetName = New DevExpress.XtraEditors.MemoEdit()
            Me.Label12 = New DevExpress.XtraEditors.LabelControl()
            Me.GroupBox2 = New DevExpress.XtraEditors.GroupControl()
            Me.txc_amount = New DevExpress.XtraEditors.CalcEdit()
            Me.txb_dest_client = New ClientID()
            Me.lbl_dest_inactive = New DebtPlus.Data.Controls.BlinkLabel()
            Me.Label14 = New DevExpress.XtraEditors.LabelControl()
            Me.cbo_Reason = New DevExpress.XtraEditors.ComboBoxEdit()
            Me.Button_Transfer = New DevExpress.XtraEditors.SimpleButton()
            Me.Button_Cancel = New DevExpress.XtraEditors.SimpleButton()
            Me.GroupBox1 = New DevExpress.XtraEditors.GroupControl()
            Me.LookUpEdit_SourceAccount = New DevExpress.XtraEditors.LookUpEdit()
            Me.Label1 = New DevExpress.XtraEditors.LabelControl()
            CType(Me.DxErrorProvider1, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.med_TargetName.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.GroupBox2, System.ComponentModel.ISupportInitialize).BeginInit()
            Me.GroupBox2.SuspendLayout()
            CType(Me.txc_amount.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.txb_dest_client.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.cbo_Reason.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.GroupBox1, System.ComponentModel.ISupportInitialize).BeginInit()
            Me.GroupBox1.SuspendLayout()
            CType(Me.LookUpEdit_SourceAccount.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
            Me.SuspendLayout()
            '
            'lbl_new_dest_trust
            '
            Me.lbl_new_dest_trust.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
            Me.lbl_new_dest_trust.Appearance.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center
            Me.lbl_new_dest_trust.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None
            Me.lbl_new_dest_trust.Location = New System.Drawing.Point(360, 78)
            Me.lbl_new_dest_trust.Name = "lbl_new_dest_trust"
            Me.lbl_new_dest_trust.Size = New System.Drawing.Size(88, 13)
            Me.lbl_new_dest_trust.TabIndex = 8
            Me.lbl_new_dest_trust.Text = "$0.00"
            Me.lbl_new_dest_trust.ToolTipController = Me.ToolTipController1
            Me.lbl_new_dest_trust.UseMnemonic = False
            '
            'Label8
            '
            Me.Label8.Location = New System.Drawing.Point(256, 78)
            Me.Label8.Name = "Label8"
            Me.Label8.Size = New System.Drawing.Size(81, 13)
            Me.Label8.TabIndex = 7
            Me.Label8.Text = "New trust Balace"
            Me.Label8.ToolTipController = Me.ToolTipController1
            Me.Label8.UseMnemonic = False
            '
            'Label9
            '
            Me.Label9.Location = New System.Drawing.Point(256, 52)
            Me.Label9.Name = "Label9"
            Me.Label9.Size = New System.Drawing.Size(41, 13)
            Me.Label9.TabIndex = 5
            Me.Label9.Text = "&Amount:"
            Me.Label9.ToolTipController = Me.ToolTipController1
            '
            'lbl_old_dest_trust
            '
            Me.lbl_old_dest_trust.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
            Me.lbl_old_dest_trust.Appearance.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center
            Me.lbl_old_dest_trust.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None
            Me.lbl_old_dest_trust.Location = New System.Drawing.Point(360, 26)
            Me.lbl_old_dest_trust.Name = "lbl_old_dest_trust"
            Me.lbl_old_dest_trust.Size = New System.Drawing.Size(88, 13)
            Me.lbl_old_dest_trust.TabIndex = 3
            Me.lbl_old_dest_trust.Text = "$0.00"
            Me.lbl_old_dest_trust.ToolTipController = Me.ToolTipController1
            Me.lbl_old_dest_trust.UseMnemonic = False
            '
            'Label11
            '
            Me.Label11.Location = New System.Drawing.Point(256, 26)
            Me.Label11.Name = "Label11"
            Me.Label11.Size = New System.Drawing.Size(104, 13)
            Me.Label11.TabIndex = 2
            Me.Label11.Text = "Original Trust Balance"
            Me.Label11.ToolTipController = Me.ToolTipController1
            Me.Label11.UseMnemonic = False
            '
            'med_TargetName
            '
            Me.med_TargetName.EditValue = ""
            Me.med_TargetName.Location = New System.Drawing.Point(16, 52)
            Me.med_TargetName.Name = "med_TargetName"
            Me.med_TargetName.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Flat
            Me.med_TargetName.Properties.ReadOnly = True
            Me.med_TargetName.Properties.ScrollBars = System.Windows.Forms.ScrollBars.None
            Me.med_TargetName.Size = New System.Drawing.Size(232, 51)
            Me.med_TargetName.TabIndex = 4
            Me.med_TargetName.TabStop = False
            Me.med_TargetName.ToolTipController = Me.ToolTipController1
            '
            'Label12
            '
            Me.Label12.Location = New System.Drawing.Point(16, 26)
            Me.Label12.Name = "Label12"
            Me.Label12.Size = New System.Drawing.Size(66, 13)
            Me.Label12.TabIndex = 0
            Me.Label12.Text = "Target &Client:"
            Me.Label12.ToolTipController = Me.ToolTipController1
            '
            'GroupBox2
            '
            Me.GroupBox2.Controls.Add(Me.txc_amount)
            Me.GroupBox2.Controls.Add(Me.txb_dest_client)
            Me.GroupBox2.Controls.Add(Me.Label8)
            Me.GroupBox2.Controls.Add(Me.Label9)
            Me.GroupBox2.Controls.Add(Me.Label11)
            Me.GroupBox2.Controls.Add(Me.lbl_dest_inactive)
            Me.GroupBox2.Controls.Add(Me.lbl_new_dest_trust)
            Me.GroupBox2.Controls.Add(Me.lbl_old_dest_trust)
            Me.GroupBox2.Controls.Add(Me.med_TargetName)
            Me.GroupBox2.Controls.Add(Me.Label12)
            Me.GroupBox2.Location = New System.Drawing.Point(8, 82)
            Me.GroupBox2.Name = "GroupBox2"
            Me.GroupBox2.Size = New System.Drawing.Size(472, 112)
            Me.GroupBox2.TabIndex = 1
            Me.GroupBox2.Text = " Transfer to the client "
            '
            'txc_amount
            '
            Me.txc_amount.AllowDrop = True
            Me.txc_amount.EnterMoveNextControl = True
            Me.txc_amount.Location = New System.Drawing.Point(387, 49)
            Me.txc_amount.Name = "txc_amount"
            Me.txc_amount.Properties.Appearance.Options.UseTextOptions = True
            Me.txc_amount.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
            Me.txc_amount.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
            Me.txc_amount.Properties.DisplayFormat.FormatString = "c2"
            Me.txc_amount.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
            Me.txc_amount.Properties.EditFormat.FormatString = "d2"
            Me.txc_amount.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric
            Me.txc_amount.Properties.Mask.BeepOnError = True
            Me.txc_amount.Properties.Mask.EditMask = "c"
            Me.txc_amount.Properties.Precision = 2
            Me.txc_amount.Size = New System.Drawing.Size(80, 20)
            Me.txc_amount.TabIndex = 6
            Me.txc_amount.ToolTipController = Me.ToolTipController1
            '
            'txb_dest_client
            '
            Me.txb_dest_client.AllowDrop = True
            Me.txb_dest_client.EnterMoveNextControl = True
            Me.txb_dest_client.Location = New System.Drawing.Point(128, 26)
            Me.txb_dest_client.Name = "txb_dest_client"
            Me.txb_dest_client.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[False]
            Me.txb_dest_client.Properties.Appearance.Options.UseTextOptions = True
            Me.txb_dest_client.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
            Me.txb_dest_client.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph, "", -1, True, True, False, DevExpress.XtraEditors.ImageLocation.MiddleCenter, CType(resources.GetObject("txb_dest_client.Properties.Buttons"), System.Drawing.Image), New DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), SerializableAppearanceObject1, "Click here to search for a client ID", "search", Nothing, True)})
            Me.txb_dest_client.Properties.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
            Me.txb_dest_client.Properties.DisplayFormat.FormatString = "0000000"
            Me.txb_dest_client.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Custom
            Me.txb_dest_client.Properties.EditFormat.FormatString = "f0"
            Me.txb_dest_client.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric
            Me.txb_dest_client.Properties.Mask.BeepOnError = True
            Me.txb_dest_client.Properties.Mask.EditMask = "\d*"
            Me.txb_dest_client.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.RegEx
            Me.txb_dest_client.Properties.ValidateOnEnterKey = True
            Me.txb_dest_client.Size = New System.Drawing.Size(80, 20)
            Me.txb_dest_client.TabIndex = 1
            '
            'lbl_dest_inactive
            '
            Me.lbl_dest_inactive.Appearance.BackColor = System.Drawing.Color.Red
            Me.lbl_dest_inactive.Appearance.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
            Me.lbl_dest_inactive.Appearance.ForeColor = System.Drawing.Color.White
            Me.lbl_dest_inactive.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center
            Me.lbl_dest_inactive.Appearance.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center
            Me.lbl_dest_inactive.Location = New System.Drawing.Point(152, 0)
            Me.lbl_dest_inactive.Name = "lbl_dest_inactive"
            Me.lbl_dest_inactive.Size = New System.Drawing.Size(0, 13)
            Me.lbl_dest_inactive.TabIndex = 0
            Me.lbl_dest_inactive.UseMnemonic = False
            Me.lbl_dest_inactive.Visible = False
            '
            'Label14
            '
            Me.Label14.Location = New System.Drawing.Point(8, 202)
            Me.Label14.Name = "Label14"
            Me.Label14.Size = New System.Drawing.Size(114, 13)
            Me.Label14.TabIndex = 2
            Me.Label14.Text = "&Reason for the transfer"
            Me.Label14.ToolTipController = Me.ToolTipController1
            '
            'cbo_Reason
            '
            Me.cbo_Reason.AllowDrop = True
            Me.cbo_Reason.EditValue = ""
            Me.cbo_Reason.EnterMoveNextControl = True
            Me.cbo_Reason.Location = New System.Drawing.Point(136, 200)
            Me.cbo_Reason.Name = "cbo_Reason"
            Me.cbo_Reason.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
            Me.cbo_Reason.Properties.MaxLength = 50
            Me.cbo_Reason.Size = New System.Drawing.Size(339, 20)
            Me.cbo_Reason.TabIndex = 3
            Me.cbo_Reason.ToolTipController = Me.ToolTipController1
            '
            'Button_Transfer
            '
            Me.Button_Transfer.Enabled = False
            Me.Button_Transfer.Location = New System.Drawing.Point(152, 236)
            Me.Button_Transfer.Name = "Button_Transfer"
            Me.Button_Transfer.Size = New System.Drawing.Size(75, 24)
            Me.Button_Transfer.TabIndex = 4
            Me.Button_Transfer.Text = "Transfer"
            Me.Button_Transfer.ToolTipController = Me.ToolTipController1
            '
            'Button_Cancel
            '
            Me.Button_Cancel.DialogResult = System.Windows.Forms.DialogResult.Cancel
            Me.Button_Cancel.Location = New System.Drawing.Point(264, 236)
            Me.Button_Cancel.Name = "Button_Cancel"
            Me.Button_Cancel.Size = New System.Drawing.Size(75, 24)
            Me.Button_Cancel.TabIndex = 5
            Me.Button_Cancel.Text = "&Quit"
            Me.Button_Cancel.ToolTipController = Me.ToolTipController1
            '
            'GroupBox1
            '
            Me.GroupBox1.Controls.Add(Me.LookUpEdit_SourceAccount)
            Me.GroupBox1.Controls.Add(Me.Label1)
            Me.GroupBox1.Location = New System.Drawing.Point(8, 17)
            Me.GroupBox1.Name = "GroupBox1"
            Me.GroupBox1.Size = New System.Drawing.Size(472, 59)
            Me.GroupBox1.TabIndex = 0
            Me.GroupBox1.Text = " Source Account "
            '
            'LookUpEdit_SourceAccount
            '
            Me.LookUpEdit_SourceAccount.EnterMoveNextControl = True
            Me.LookUpEdit_SourceAccount.Location = New System.Drawing.Point(128, 25)
            Me.LookUpEdit_SourceAccount.Name = "LookUpEdit_SourceAccount"
            Me.LookUpEdit_SourceAccount.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
            Me.LookUpEdit_SourceAccount.Properties.NullText = "Please choose an account from the list..."
            Me.LookUpEdit_SourceAccount.Size = New System.Drawing.Size(339, 20)
            Me.LookUpEdit_SourceAccount.TabIndex = 1
            Me.LookUpEdit_SourceAccount.ToolTipController = Me.ToolTipController1
            '
            'Label1
            '
            Me.Label1.Location = New System.Drawing.Point(16, 28)
            Me.Label1.Name = "Label1"
            Me.Label1.Size = New System.Drawing.Size(43, 13)
            Me.Label1.TabIndex = 0
            Me.Label1.Text = "Accoun&t:"
            Me.Label1.ToolTipController = Me.ToolTipController1
            '
            'Form_Transfer_OperClient
            '
            Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
            Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
            Me.CancelButton = Me.Button_Cancel
            Me.ClientSize = New System.Drawing.Size(488, 272)
            Me.Controls.Add(Me.GroupBox1)
            Me.Controls.Add(Me.Button_Cancel)
            Me.Controls.Add(Me.Button_Transfer)
            Me.Controls.Add(Me.cbo_Reason)
            Me.Controls.Add(Me.Label14)
            Me.Controls.Add(Me.GroupBox2)
            Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog
            Me.MaximizeBox = False
            Me.Name = "Form_Transfer_OperClient"
            Me.Text = "Operating Account to Client Transfer of funds"
            CType(Me.DxErrorProvider1, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.med_TargetName.Properties, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.GroupBox2, System.ComponentModel.ISupportInitialize).EndInit()
            Me.GroupBox2.ResumeLayout(False)
            Me.GroupBox2.PerformLayout()
            CType(Me.txc_amount.Properties, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.txb_dest_client.Properties, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.cbo_Reason.Properties, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.GroupBox1, System.ComponentModel.ISupportInitialize).EndInit()
            Me.GroupBox1.ResumeLayout(False)
            Me.GroupBox1.PerformLayout()
            CType(Me.LookUpEdit_SourceAccount.Properties, System.ComponentModel.ISupportInitialize).EndInit()
            Me.ResumeLayout(False)
            Me.PerformLayout()

        End Sub
        Friend WithEvents Label8 As DevExpress.XtraEditors.LabelControl
        Friend WithEvents Label9 As DevExpress.XtraEditors.LabelControl
        Friend WithEvents Label11 As DevExpress.XtraEditors.LabelControl
        Friend WithEvents med_TargetName As DevExpress.XtraEditors.MemoEdit
        Friend WithEvents Label12 As DevExpress.XtraEditors.LabelControl
        Friend WithEvents GroupBox2 As DevExpress.XtraEditors.GroupControl
        Friend WithEvents Label14 As DevExpress.XtraEditors.LabelControl
        Friend WithEvents cbo_Reason As DevExpress.XtraEditors.ComboBoxEdit
        Friend WithEvents Button_Transfer As DevExpress.XtraEditors.SimpleButton
        Friend WithEvents Button_Cancel As DevExpress.XtraEditors.SimpleButton
        Friend WithEvents lbl_new_dest_trust As DevExpress.XtraEditors.LabelControl
        Friend WithEvents lbl_old_dest_trust As DevExpress.XtraEditors.LabelControl
        Friend WithEvents lbl_dest_inactive As DebtPlus.Data.Controls.BlinkLabel
        Friend WithEvents GroupBox1 As DevExpress.XtraEditors.GroupControl
        Friend WithEvents Label1 As DevExpress.XtraEditors.LabelControl
        Friend WithEvents LookUpEdit_SourceAccount As DevExpress.XtraEditors.LookUpEdit
        Friend WithEvents txb_dest_client As ClientID
        Friend WithEvents txc_amount As DevExpress.XtraEditors.CalcEdit
    End Class
End Namespace
