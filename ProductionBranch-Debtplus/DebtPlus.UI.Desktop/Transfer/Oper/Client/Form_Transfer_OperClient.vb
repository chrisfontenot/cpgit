#Region "Copyright 2000-2012 DebtPlus, L.L.C."
'{*******************************************************************}
'{                                                                   }
'{       DebtPlus Debt Management System                             }
'{                                                                   }
'{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
'{       ALL RIGHTS RESERVED                                         }
'{                                                                   }
'{   The entire contents of this file is protected by U.S. and       }
'{   International Copyright Laws. Unauthorized reproduction,        }
'{   reverse-engineering, and distribution of all or any portion of  }
'{   the code contained in this file is strictly prohibited and may  }
'{   result in severe civil and criminal penalties and will be       }
'{   prosecuted to the maximum extent possible under the law.        }
'{                                                                   }
'{   RESTRICTIONS                                                    }
'{                                                                   }
'{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
'{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
'{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
'{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
'{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
'{                                                                   }
'{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
'{   ADDITIONAL RESTRICTIONS.                                        }
'{                                                                   }
'{*******************************************************************}
#End Region
Imports DebtPlus.Utils
Imports System.Data.SqlClient
Imports System.Drawing
Imports System.Windows.Forms

Namespace Transfer.Oper.Client
    Friend Class Form_Transfer_OperClient

        Public Sub New()
            MyBase.New()
            InitializeComponent()
        End Sub

        Friend ap As ArgParser = Nothing
        Public Sub New(ByVal ap As ArgParser)
            MyClass.New()
            MyClass.ap = ap
        End Sub

        ' Linkage to our resource manager tables
        Protected RM As New System.Resources.ResourceManager("DebtPlus.UI.Desktop.Form_Transfer_OperClient_Strings", System.Reflection.Assembly.GetExecutingAssembly())

        Private Sub Form_Transfer_OperClient_Load(ByVal sender As Object, ByVal e As EventArgs) Handles Me.Load

            ' Load the ledger code table list
            With LookUpEdit_SourceAccount
                With .Properties
                    .DataSource = LedgerCodesTableView()
                    .PopulateColumns()
                    .Columns("Default").Visible = False         ' Do not display the default column
                    .Columns("ActiveFlag").Visible = False      ' Do not display the active flag
                    .DisplayMember = "Description"              ' Show the description
                    .ValueMember = "Ledger Code"                ' but the ledger code is what we want
                    .NullText = ""                              ' do not show anything if there is no item
                End With

                ' Try to select the default item
                Dim vue As DataView = New DataView(CType(.Properties.DataSource, DataView).Table, "Default<>0", "", DataViewRowState.CurrentRows)
                If vue.Count > 0 Then .EditValue = vue(0).Item("Ledger Code")

                ' Bind the controls
                AddHandler .EditValueChanging, AddressOf DebtPlus.Data.Validation.LookUpEdit_ActiveTest
                AddHandler .EditValueChanging, AddressOf LookUpEdit_SourceAccount_EditValueChanging
                AddHandler .EditValueChanged, AddressOf LookUpEdit_SourceAccount_EditValueChanged
            End With

            ' Load the list of reasons
            Load_Transfer_Reasons("OC")

            ' Register the handlers for the routines
            AddHandler txb_dest_client.DragEnter, AddressOf drag_drop_DragEnter
            AddHandler txb_dest_client.Validated, AddressOf txb_dest_client_Validated

            AddHandler txc_amount.Validated, AddressOf txc_amount_Validated

            AddHandler cbo_Reason.DragDrop, AddressOf drag_drop_DragDrop
            AddHandler cbo_Reason.DragEnter, AddressOf drag_drop_DragEnter
            AddHandler cbo_Reason.QueryContinueDrag, AddressOf drag_drop_QueryContinueDrag
            AddHandler cbo_Reason.MouseMove, AddressOf drag_drop_MouseMove

            AddHandler lbl_new_dest_trust.MouseMove, AddressOf drag_drop_MouseMove
            AddHandler lbl_new_dest_trust.MouseMove, AddressOf drag_drop_MouseMove
            AddHandler lbl_new_dest_trust.QueryContinueDrag, AddressOf drag_drop_QueryContinueDrag

            AddHandler lbl_old_dest_trust.MouseMove, AddressOf drag_drop_MouseMove
            AddHandler lbl_old_dest_trust.QueryContinueDrag, AddressOf drag_drop_QueryContinueDrag

            AddHandler med_TargetName.MouseMove, AddressOf drag_drop_MouseMove
            AddHandler med_TargetName.QueryContinueDrag, AddressOf drag_drop_QueryContinueDrag
        End Sub

        Private Function LedgerCodesTableView() As DataView
            Dim answer As DataView = Nothing
            Dim ds As New DataSet("ledger_codes")

            Dim cn As SqlClient.SqlConnection = New SqlConnection(LibraryGlobals.SqlInfo.ConnectionString)
            Try
                Using cmd As SqlClient.SqlCommand = New SqlCommand
                    With cmd
                        .Connection = cn
                        .CommandText = "SELECT [ledger_code] as [Ledger Code],[description] as Description, 1 as [ActiveFlag], 0 as [Default] FROM ledger_codes WITH (NOLOCK) ORDER BY description"
                        .CommandType = CommandType.Text
                    End With

                    ' Fill the ledger codes
                    Using da As New SqlClient.SqlDataAdapter(cmd)
                        da.Fill(ds, "ledger_codes")
                        answer = ds.Tables(0).DefaultView
                    End Using
                End Using

            Catch ex As SqlClient.SqlException
                Using gdr As New Repository.GetDataResult() : gdr.HandleException(ex) : DebtPlus.UI.Common.ErrorHandling.HandleErrors(gdr, "Error loading ledger codes") : End Using

            Finally
                If cn IsNot Nothing Then cn.Dispose()
            End Try

            Return answer
        End Function

        Private Sub Load_Transfer_Reasons(ByVal ItemType As String)
            Dim cn As SqlClient.SqlConnection = New SqlConnection(LibraryGlobals.SqlInfo.ConnectionString)
            Dim rd As SqlClient.SqlDataReader = Nothing

            Try
                ' Attempt to read the information from the database
                cn.Open()
                Using cmd As SqlClient.SqlCommand = New SqlCommand
                    With cmd
                        .Connection = cn
                        .CommandText = "lst_descriptions_" + ItemType
                        .CommandType = CommandType.StoredProcedure
                        rd = .ExecuteReader(CommandBehavior.CloseConnection Or CommandBehavior.SingleResult)
                    End With
                End Using

                ' Try to find if a default item is given as well as an active status.
                Dim default_column As Int32 = -1
                Dim active_column As Int32 = -1
                For col As Int32 = 0 To rd.FieldCount - 1
                    Select Case rd.GetName(col).ToLower
                        Case "default"
                            default_column = col
                        Case "activeflag"
                            active_column = col
                        Case Else
                    End Select
                Next

                ' Load the list of items into the control
                With cbo_Reason
                    With .Properties
                        With .Items
                            .Clear()
                            Do While rd.Read

                                ' Add the item. If there is an active status then load it otherwise use "TRUE"
                                Dim NewItem As Int32
                                If active_column >= 0 Then
                                    NewItem = .Add(New DebtPlus.Data.Controls.ComboboxItem(rd.GetString(rd.GetOrdinal("description")), rd.GetInt32(rd.GetOrdinal("item_key")), rd.GetBoolean(active_column)))
                                Else
                                    NewItem = .Add(New DebtPlus.Data.Controls.ComboboxItem(rd.GetString(rd.GetOrdinal("description")), rd.GetInt32(rd.GetOrdinal("item_key")), True))
                                End If

                                ' Set the default value if one is present
                                If default_column >= 0 Then
                                    If rd.GetBoolean(default_column) Then cbo_Reason.SelectedIndex = NewItem
                                End If
                            Loop
                        End With
                    End With

                    ' Select the top item if there is one and none are defaulted
                    If .SelectedIndex < 0 AndAlso .Properties.Items.Count > 0 Then .SelectedIndex = 0
                End With

            Catch ex As Exception
                DevExpress.XtraEditors.XtraMessageBox.Show(ex.ToString(), "Error loading form", MessageBoxButtons.OK, MessageBoxIcon.Error, MessageBoxDefaultButton.Button1)

            Finally
                If rd IsNot Nothing Then rd.Dispose()
                If cn IsNot Nothing Then cn.Dispose()
            End Try
        End Sub

        Private Sub clear_form()

            ' Clear the transfer information
            txb_dest_client.EditValue = Nothing
            txc_amount.EditValue = Nothing

            ' Clear the inactive warning labels
            lbl_dest_inactive.Text = String.Empty
            med_TargetName.Text = String.Empty

            ' Clear the dollar amounts
            _old_source_trust = 0D
            _new_source_trust = 0D
            old_dest_trust = 0D
            dest_amount = 0D
            new_dest_trust = 0D

            ' Set the error conditions for the values that are missing
            validate_source()
            validate_destination()
            validate_reason()
            validate_amount()

            ' Disable the transfer button if there are errors
            Button_Transfer.Enabled = Not HasErrors()
        End Sub

        Private Function HasErrors() As Boolean
            Dim answer As Boolean = True

            Do
                If DxErrorProvider1.GetError(LookUpEdit_SourceAccount) <> String.Empty Then Exit Do
                If DxErrorProvider1.GetError(txb_dest_client) <> String.Empty Then Exit Do
                If DxErrorProvider1.GetError(cbo_Reason) <> String.Empty Then Exit Do
                If DxErrorProvider1.GetError(txc_amount) <> String.Empty Then Exit Do

                ' All is well
                answer = False
                Exit Do
            Loop

            Return answer
        End Function

        Private _new_source_trust As Decimal = 0D
        Private ReadOnly Property new_source_trust() As Decimal
            Get
                Return _new_source_trust
            End Get
        End Property

        Private _old_source_trust As Decimal = 0D
        Private ReadOnly Property old_source_trust() As Decimal
            Get
                Return _old_source_trust
            End Get
        End Property

        Private _new_dest_trust As Decimal = 0D
        Private Property new_dest_trust() As Decimal
            Get
                Return _new_dest_trust
            End Get
            Set(ByVal Value As Decimal)
                _new_dest_trust = Value
                With lbl_new_dest_trust
                    .Text = String.Format("{0:c}", Value)
                    If Value < 0D Then
                        .ForeColor = Color.Red
                    Else
                        .ForeColor = Color.Black
                    End If
                End With
            End Set
        End Property

        Private _old_dest_trust As Decimal = 0D
        Private Property old_dest_trust() As Decimal
            Get
                Return _old_dest_trust
            End Get
            Set(ByVal Value As Decimal)
                _old_dest_trust = Value
                With lbl_old_dest_trust
                    .Text = String.Format("{0:c}", Value)
                    If Value < 0D Then
                        .ForeColor = Color.Red
                    Else
                        .ForeColor = Color.Black
                    End If
                End With
            End Set
        End Property

        Private Property dest_amount() As Decimal = 0D

        Private Property src_amount() As Decimal
            Get
                With txc_amount
                    If .Text = String.Empty Then Return 0D
                    If .EditValue Is Nothing Then Return 0D
                    Return Convert.ToDecimal(.EditValue)
                End With
            End Get
            Set(ByVal Value As Decimal)
                txc_amount.EditValue = Value
            End Set
        End Property

        Private Function reason() As String
            With cbo_Reason
                Return .Text.Trim()
            End With
        End Function

        Private Sub validate_reason()
            Dim error_message As String

            ' Ensure that there is a value for the information
            If reason() = String.Empty Then
                error_message = RM.GetString("required_value")
            Else
                error_message = String.Empty
            End If

            ' Set the error text and enable the transfer button if there is no error
            DxErrorProvider1.SetError(cbo_Reason, error_message)
            Button_Transfer.Enabled = Not HasErrors()
        End Sub

        Private Sub txc_amount_Validated(ByVal sender As Object, ByVal e As EventArgs)
            validate_amount()
        End Sub

        Private Sub validate_amount()
            Dim error_message As String = String.Empty

            ' If the amount is negative then reject the transfer
            If txc_amount.Text.Trim() = String.Empty Then
                error_message = RM.GetString("required_value")
            ElseIf src_amount <= 0D Then
                error_message = RM.GetString("must_be_posative")
            End If

            ' Update the totals accordingly
            dest_amount = src_amount
            _new_source_trust = old_source_trust - dest_amount
            new_dest_trust = old_dest_trust + dest_amount

            ' Set the error text
            DxErrorProvider1.SetError(txc_amount, error_message)
            Button_Transfer.Enabled = Not HasErrors()
        End Sub

        Private Sub drag_drop_MouseMove(ByVal sender As Object, ByVal e As MouseEventArgs)

            With CType(sender, Control)
                ' Do nothing if the operation is not valid
                If e.Button = 0 Then Exit Sub
                If e.X >= 0 AndAlso e.X < .Width AndAlso e.Y >= 0 AndAlso e.Y < .Height Then Exit Sub

                Dim txt As String = String.Empty

                ' If this is a text box then look for the selected text component of the text field
                If TypeOf sender Is DevExpress.XtraEditors.TextEdit Then
                    With CType(sender, DevExpress.XtraEditors.TextEdit)
                        If .SelectionLength > 0 Then txt = .SelectedText
                    End With
                End If

                ' If the text is missing then try the entire text field
                If txt = String.Empty Then txt = .Text.Trim()

                ' If there is no text then do nothing
                If txt = String.Empty Then Exit Sub

                ' Start a drag-drop operation when the mouse moves outside the control
                Dim dobj As New DataObject
                dobj.SetData(DataFormats.Text, True, txt)

                ' Do the operation. It will return when the operation is complete.
                Dim effect As DragDropEffects = DragDropEffects.Copy
                effect = .DoDragDrop(dobj, effect)

                ' If the operation result was move then erase the text.
                ' However, since we don't do the move then nothing more is needed.
            End With
        End Sub

        Private Sub drag_drop_DragEnter(ByVal sender As Object, ByVal e As DragEventArgs)

            ' If there is a text field then look to determine the accepable processing
            If e.Data.GetDataPresent(DataFormats.Text, True) Then
                e.Effect = e.AllowedEffect And DragDropEffects.Copy
            Else
                e.Effect = DragDropEffects.None
            End If
        End Sub

        Private Sub drag_drop_DragDrop(ByVal sender As Object, ByVal e As DragEventArgs)

            ' If there is a text field then look to determine the accepable processing
            If Not e.Data.GetDataPresent(DataFormats.Text, True) Then Return

            ' We only support copy. No modifier needs to be tested
            e.Effect = e.AllowedEffect And DragDropEffects.Copy

            ' Paste the text into the control
            With CType(sender, Control)
                .Text = Convert.ToString(e.Data.GetData(DataFormats.Text, True))

                If sender Is txb_dest_client Then
                    validate_destination()
                ElseIf sender Is txc_amount Then
                    txc_amount_Validated(txc_amount, New EventArgs)
                End If
            End With
        End Sub

        Private Sub drag_drop_QueryContinueDrag(ByVal sender As Object, ByVal e As QueryContinueDragEventArgs)

            ' If the escape key is pressed then cancel the operation
            If e.EscapePressed Then e.Action = DragAction.Cancel
        End Sub

        Private Sub LookUpEdit_SourceAccount_EditValueChanging(ByVal sender As Object, ByVal e As DevExpress.XtraEditors.Controls.ChangingEventArgs)

            ' If the new value is defined then make sure that the office is still active.
            With CType(sender, DevExpress.XtraEditors.LookUpEdit)
                If e.NewValue IsNot Nothing Then

                    ' Find the new office being selected
                    Dim IDKey As String = Convert.ToString(e.NewValue)
                    Dim drv As DataRowView = CType(.Properties.GetDataSourceRowByKeyValue(IDKey), DataRowView)

                    ' From the office, ensure that the active flag is set
                    If Not Convert.ToBoolean(drv.Item("ActiveFlag")) Then
                        e.Cancel = True
                        DebtPlus.Data.Forms.MessageBox.Show("The ledger account is no longer active. Please do not use this account.", "Bank selection is not valid", MessageBoxButtons.OK, MessageBoxIcon.Hand, MessageBoxDefaultButton.Button1)
                    End If
                End If
            End With
        End Sub

        Private Sub LookUpEdit_SourceAccount_EditValueChanged(ByVal sender As Object, ByVal e As EventArgs)
            validate_source()
        End Sub

        Private Sub validate_source()
            Dim error_message As String

            ' The only requirement is that there be a value.
            If source_ledger_code() = String.Empty Then
                error_message = RM.GetString("required_value")
            Else
                error_message = String.Empty
            End If

            ' Set the error message text
            DxErrorProvider1.SetError(LookUpEdit_SourceAccount, error_message)
            Button_Transfer.Enabled = Not HasErrors()
        End Sub

        Private Function source_ledger_code() As String
            Dim answer As String
            If LookUpEdit_SourceAccount.EditValue IsNot Nothing Then
                answer = Convert.ToString(LookUpEdit_SourceAccount.EditValue)
            Else
                answer = String.Empty
            End If
            Return answer
        End Function

        Private Function dest_client() As Int32
            With txb_dest_client
                If .Text = String.Empty Then Return -1
                If .EditValue Is Nothing Then Return -1
                Return Convert.ToInt32(.EditValue)
            End With
        End Function

        Private Sub txb_dest_client_Validated(ByVal sender As Object, ByVal e As EventArgs)
            validate_destination()
        End Sub

        Private Sub validate_destination()
            Dim error_message As String = String.Empty
            With txb_dest_client
                lbl_dest_inactive.Text = String.Empty

                ' There must be a client
                If .Text = String.Empty Then
                    error_message = RM.GetString("required_value")
                Else

                    ' Find the client. If the ClientId is invalid then reject the operation
                    If dest_client() <= 0 Then
                        error_message = RM.GetString("invalid_client")
                    Else
                        Dim active_status As String = String.Empty

                        ' Read the client information from the database
                        If Not read_client(dest_client, active_status, med_TargetName.Text, old_dest_trust) Then
                            error_message = RM.GetString("invalid_client")
                        Else

                            ' If the client is not active then indicate as such
                            Select Case active_status
                                Case "A", "AR"
                                    lbl_dest_inactive.Visible = False
                                Case Else
                                    lbl_dest_inactive.Text = "INACTIVE Client"
                            End Select
                        End If
                    End If
                End If
            End With

            ' Update the amount information for the trust balances
            validate_amount()

            ' Set the error message text
            DxErrorProvider1.SetError(txb_dest_client, error_message)
            Button_Transfer.Enabled = Not HasErrors()
        End Sub

        Private Function read_client(ByVal client As Int32, ByRef active_status As String, ByRef client_name As String, ByRef trust_balance As Decimal) As Boolean
            Dim cn As SqlClient.SqlConnection = New SqlConnection(LibraryGlobals.SqlInfo.ConnectionString)
            Dim rd As SqlClient.SqlDataReader = Nothing
            Dim answer As Boolean = False

            Dim result_client As Int32 = -1
            Dim result_trust As Decimal = 0D
            Dim result_active_status As String = String.Empty
            Dim result_address1 As String = String.Empty
            Dim result_address2 As String = String.Empty
            Dim result_address3 As String = String.Empty
            Dim result_name As String = String.Empty

            Try
                cn.Open()
                Using cmd As SqlClient.SqlCommand = New SqlCommand
                    With cmd
                        .CommandText = "SELECT v.client, c.held_in_trust, v.active_status, v.addr1, v.addr2, v.addr3, v.name FROM view_client_address v INNER JOIN clients c on v.client = c.client WHERE v.client = @client"
                        .Connection = cn
                        .Parameters.Add("@client", SqlDbType.Int).Value = client
                        rd = .ExecuteReader(CommandBehavior.CloseConnection Or CommandBehavior.SingleResult Or CommandBehavior.SingleRow)
                    End With
                End Using

                answer = rd.Read
                If answer Then
                    If Not rd.IsDBNull(0) Then result_client = rd.GetInt32(0)
                    If Not rd.IsDBNull(1) Then result_trust = rd.GetDecimal(1)
                    If Not rd.IsDBNull(2) Then result_active_status = rd.GetString(2).ToUpper
                    If Not rd.IsDBNull(3) Then result_address1 = rd.GetString(3)
                    If Not rd.IsDBNull(4) Then result_address2 = rd.GetString(4)
                    If Not rd.IsDBNull(5) Then result_address3 = rd.GetString(5)
                    If Not rd.IsDBNull(6) Then result_name = rd.GetString(6)
                End If
                rd.Close()

                ' Combine the results
                If result_client < 0 Then answer = False

                ' If successful then return the input fields
                If answer Then
                    trust_balance = result_trust
                    active_status = result_active_status

                    ' Append the fields to form the name
                    Dim result_string As New System.Text.StringBuilder
                    For Each field As String In New String() {result_name, result_address1, result_address2, result_address3}
                        If field <> String.Empty Then
                            result_string.Append(Environment.NewLine)
                            result_string.Append(field)
                        End If
                    Next

                    ' Remove the leading cr/lf and return the name string
                    If result_string.Length > 0 Then result_string.Remove(0, 2)
                    client_name = result_string.ToString()
                End If

            Catch ex As Exception
                DebtPlus.Data.Forms.MessageBox.Show(ex.ToString(), "Error reading client information", MessageBoxButtons.OK, MessageBoxIcon.Error)
                answer = False

            Finally
                If cn IsNot Nothing Then cn.Dispose()
            End Try

            ' Return the resulting status
            Return answer
        End Function

        Private Sub Button_Transfer_Click(ByVal sender As Object, ByVal e As EventArgs) Handles Button_Transfer.Click

            ' Do the transfer
            Dim cn As SqlClient.SqlConnection = New SqlConnection(LibraryGlobals.SqlInfo.ConnectionString)
            Dim new_balance As Object

            ' Issue the request to the database to make the entry.
            Try
                cn.Open()
                Using cmd As SqlClient.SqlCommand = New SqlCommand
                    With cmd
                        .CommandText = "xpr_transfer_op_cl"
                        .CommandType = CommandType.StoredProcedure
                        .Connection = cn

                        With .Parameters
                            .Add("@SrcAccount", SqlDbType.VarChar, source_ledger_code.Length).Value = source_ledger_code()
                            .Add("@DestClient", SqlDbType.Int).Value = dest_client()
                            .Add("@Amount", SqlDbType.Decimal).Value = src_amount
                            .Add("@Description", SqlDbType.VarChar, 256).Value = reason()
                        End With

                        new_balance = .ExecuteScalar()
                    End With
                End Using

                ' Success. Reset the form
                clear_form()

                ' Go to the source client for the next field
                LookUpEdit_SourceAccount.Focus()

            Catch ex As Exception
                Using gdr As New DebtPlus.Repository.GetDataResult()
                    gdr.HandleException(ex)
                    DebtPlus.UI.Common.ErrorHandling.HandleErrors(gdr)
                End Using

            Finally
                If cn IsNot Nothing Then cn.Dispose()
            End Try
        End Sub

        Private Sub Button_Cancel_Click(ByVal sender As Object, ByVal e As EventArgs) Handles Button_Cancel.Click
            Close()
        End Sub
    End Class
End Namespace
