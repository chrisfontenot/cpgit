﻿Namespace Transfer.Oper.Oper
    <Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
    Partial Class Form_Transfer_OperOper
        Inherits DebtPlus.Data.Forms.DebtPlusForm

        'Form overrides dispose to clean up the component list.
        <System.Diagnostics.DebuggerNonUserCode()> _
        Protected Overrides Sub Dispose(ByVal disposing As Boolean)
            Try
                If disposing AndAlso components IsNot Nothing Then
                    components.Dispose()
                End If
            Finally
                MyBase.Dispose(disposing)
            End Try
        End Sub

        'Required by the Windows Form Designer
        Private components As System.ComponentModel.IContainer

        'NOTE: The following procedure is required by the Windows Form Designer
        'It can be modified using the Windows Form Designer.  
        'Do not modify it using the code editor.
        <System.Diagnostics.DebuggerStepThrough()> _
        Private Sub InitializeComponent()
            Me.Label14 = New DevExpress.XtraEditors.LabelControl()
            Me.cbo_Reason = New DevExpress.XtraEditors.ComboBoxEdit()
            Me.Button_Transfer = New DevExpress.XtraEditors.SimpleButton()
            Me.Button_Cancel = New DevExpress.XtraEditors.SimpleButton()
            Me.GroupBox2 = New System.Windows.Forms.GroupBox()
            Me.LookUpEdit_DestAccount = New DevExpress.XtraEditors.LookUpEdit()
            Me.Label2 = New DevExpress.XtraEditors.LabelControl()
            Me.GroupBox1 = New System.Windows.Forms.GroupBox()
            Me.LookUpEdit_SourceAccount = New DevExpress.XtraEditors.LookUpEdit()
            Me.txc_amount = New DevExpress.XtraEditors.CalcEdit()
            Me.Label4 = New DevExpress.XtraEditors.LabelControl()
            Me.Label1 = New DevExpress.XtraEditors.LabelControl()
            CType(Me.DxErrorProvider1, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.cbo_Reason.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
            Me.GroupBox2.SuspendLayout()
            CType(Me.LookUpEdit_DestAccount.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
            Me.GroupBox1.SuspendLayout()
            CType(Me.LookUpEdit_SourceAccount.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.txc_amount.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
            Me.SuspendLayout()
            '
            'Label14
            '
            Me.Label14.Location = New System.Drawing.Point(16, 181)
            Me.Label14.Name = "Label14"
            Me.Label14.Size = New System.Drawing.Size(114, 13)
            Me.Label14.TabIndex = 2
            Me.Label14.Text = "Reason for the transfer"
            '
            'cbo_Reason
            '
            Me.cbo_Reason.AllowDrop = True
            Me.cbo_Reason.EditValue = ""
            Me.cbo_Reason.EnterMoveNextControl = True
            Me.cbo_Reason.Location = New System.Drawing.Point(136, 181)
            Me.cbo_Reason.Name = "cbo_Reason"
            Me.cbo_Reason.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
            Me.cbo_Reason.Properties.MaxLength = 50
            Me.cbo_Reason.Size = New System.Drawing.Size(320, 20)
            Me.cbo_Reason.TabIndex = 3
            Me.cbo_Reason.ToolTipController = Me.ToolTipController1
            '
            'Button_Transfer
            '
            Me.Button_Transfer.Enabled = False
            Me.Button_Transfer.Location = New System.Drawing.Point(152, 215)
            Me.Button_Transfer.Name = "Button_Transfer"
            Me.Button_Transfer.Size = New System.Drawing.Size(75, 25)
            Me.Button_Transfer.TabIndex = 4
            Me.Button_Transfer.Text = "Transfer"
            Me.Button_Transfer.ToolTipController = Me.ToolTipController1
            '
            'Button_Cancel
            '
            Me.Button_Cancel.DialogResult = System.Windows.Forms.DialogResult.Cancel
            Me.Button_Cancel.Location = New System.Drawing.Point(264, 215)
            Me.Button_Cancel.Name = "Button_Cancel"
            Me.Button_Cancel.Size = New System.Drawing.Size(75, 25)
            Me.Button_Cancel.TabIndex = 5
            Me.Button_Cancel.Text = "&Cancel"
            Me.Button_Cancel.ToolTipController = Me.ToolTipController1
            '
            'GroupBox2
            '
            Me.GroupBox2.Controls.Add(Me.LookUpEdit_DestAccount)
            Me.GroupBox2.Controls.Add(Me.Label2)
            Me.GroupBox2.Location = New System.Drawing.Point(8, 98)
            Me.GroupBox2.Name = "GroupBox2"
            Me.GroupBox2.Size = New System.Drawing.Size(472, 60)
            Me.GroupBox2.TabIndex = 1
            Me.GroupBox2.TabStop = False
            Me.GroupBox2.Text = " To the Operating Account  "
            '
            'LookUpEdit_DestAccount
            '
            Me.LookUpEdit_DestAccount.EnterMoveNextControl = True
            Me.LookUpEdit_DestAccount.Location = New System.Drawing.Point(128, 25)
            Me.LookUpEdit_DestAccount.Name = "LookUpEdit_DestAccount"
            Me.LookUpEdit_DestAccount.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
            Me.LookUpEdit_DestAccount.Properties.NullText = ""
            Me.LookUpEdit_DestAccount.Size = New System.Drawing.Size(320, 20)
            Me.LookUpEdit_DestAccount.TabIndex = 1
            Me.LookUpEdit_DestAccount.ToolTip = "Destination Non-AR ledger account for the transfer"
            Me.LookUpEdit_DestAccount.ToolTipController = Me.ToolTipController1
            '
            'Label2
            '
            Me.Label2.Location = New System.Drawing.Point(16, 25)
            Me.Label2.Name = "Label2"
            Me.Label2.Size = New System.Drawing.Size(43, 13)
            Me.Label2.TabIndex = 0
            Me.Label2.Text = "Account:"
            '
            'GroupBox1
            '
            Me.GroupBox1.Controls.Add(Me.LookUpEdit_SourceAccount)
            Me.GroupBox1.Controls.Add(Me.txc_amount)
            Me.GroupBox1.Controls.Add(Me.Label4)
            Me.GroupBox1.Controls.Add(Me.Label1)
            Me.GroupBox1.Location = New System.Drawing.Point(8, 9)
            Me.GroupBox1.Name = "GroupBox1"
            Me.GroupBox1.Size = New System.Drawing.Size(472, 86)
            Me.GroupBox1.TabIndex = 0
            Me.GroupBox1.TabStop = False
            Me.GroupBox1.Text = " Source Account "
            '
            'LookUpEdit_SourceAccount
            '
            Me.LookUpEdit_SourceAccount.EnterMoveNextControl = True
            Me.LookUpEdit_SourceAccount.Location = New System.Drawing.Point(128, 25)
            Me.LookUpEdit_SourceAccount.Name = "LookUpEdit_SourceAccount"
            Me.LookUpEdit_SourceAccount.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
            Me.LookUpEdit_SourceAccount.Properties.NullText = ""
            Me.LookUpEdit_SourceAccount.Size = New System.Drawing.Size(320, 20)
            Me.LookUpEdit_SourceAccount.TabIndex = 1
            '
            'txc_amount
            '
            Me.txc_amount.AllowDrop = True
            Me.txc_amount.EnterMoveNextControl = True
            Me.txc_amount.Location = New System.Drawing.Point(128, 52)
            Me.txc_amount.Name = "txc_amount"
            Me.txc_amount.Properties.Appearance.Options.UseTextOptions = True
            Me.txc_amount.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
            Me.txc_amount.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
            Me.txc_amount.Properties.DisplayFormat.FormatString = "{0:c}"
            Me.txc_amount.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
            Me.txc_amount.Properties.EditFormat.FormatString = "d2"
            Me.txc_amount.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric
            Me.txc_amount.Size = New System.Drawing.Size(80, 20)
            Me.txc_amount.TabIndex = 3
            '
            'Label4
            '
            Me.Label4.Location = New System.Drawing.Point(16, 53)
            Me.Label4.Name = "Label4"
            Me.Label4.Size = New System.Drawing.Size(41, 13)
            Me.Label4.TabIndex = 2
            Me.Label4.Text = "Amount:"
            Me.Label4.UseMnemonic = False
            '
            'Label1
            '
            Me.Label1.Location = New System.Drawing.Point(16, 25)
            Me.Label1.Name = "Label1"
            Me.Label1.Size = New System.Drawing.Size(43, 13)
            Me.Label1.TabIndex = 0
            Me.Label1.Text = "Account:"
            '
            'Form_Transfer_OperOper
            '
            Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
            Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
            Me.CancelButton = Me.Button_Cancel
            Me.ClientSize = New System.Drawing.Size(496, 250)
            Me.Controls.Add(Me.GroupBox1)
            Me.Controls.Add(Me.GroupBox2)
            Me.Controls.Add(Me.Button_Cancel)
            Me.Controls.Add(Me.Button_Transfer)
            Me.Controls.Add(Me.cbo_Reason)
            Me.Controls.Add(Me.Label14)
            Me.Name = "Form_Transfer_OperOper"
            Me.Text = "Transfer of funds Between Operating Accounts"
            CType(Me.DxErrorProvider1, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.cbo_Reason.Properties, System.ComponentModel.ISupportInitialize).EndInit()
            Me.GroupBox2.ResumeLayout(False)
            Me.GroupBox2.PerformLayout()
            CType(Me.LookUpEdit_DestAccount.Properties, System.ComponentModel.ISupportInitialize).EndInit()
            Me.GroupBox1.ResumeLayout(False)
            Me.GroupBox1.PerformLayout()
            CType(Me.LookUpEdit_SourceAccount.Properties, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.txc_amount.Properties, System.ComponentModel.ISupportInitialize).EndInit()
            Me.ResumeLayout(False)
            Me.PerformLayout()

        End Sub
        Friend WithEvents Label14 As DevExpress.XtraEditors.LabelControl
        Friend WithEvents cbo_Reason As DevExpress.XtraEditors.ComboBoxEdit
        Friend WithEvents Button_Transfer As DevExpress.XtraEditors.SimpleButton
        Friend WithEvents Button_Cancel As DevExpress.XtraEditors.SimpleButton
        Friend WithEvents GroupBox2 As System.Windows.Forms.GroupBox
        Friend WithEvents LookUpEdit_DestAccount As DevExpress.XtraEditors.LookUpEdit
        Friend WithEvents Label2 As DevExpress.XtraEditors.LabelControl
        Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
        Friend WithEvents LookUpEdit_SourceAccount As DevExpress.XtraEditors.LookUpEdit
        Friend WithEvents txc_amount As DevExpress.XtraEditors.CalcEdit
        Friend WithEvents Label4 As DevExpress.XtraEditors.LabelControl
        Friend WithEvents Label1 As DevExpress.XtraEditors.LabelControl
    End Class
End Namespace
