#Region "Copyright 2000-2012 DebtPlus, L.L.C."
'{*******************************************************************}
'{                                                                   }
'{       DebtPlus Debt Management System                             }
'{                                                                   }
'{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
'{       ALL RIGHTS RESERVED                                         }
'{                                                                   }
'{   The entire contents of this file is protected by U.S. and       }
'{   International Copyright Laws. Unauthorized reproduction,        }
'{   reverse-engineering, and distribution of all or any portion of  }
'{   the code contained in this file is strictly prohibited and may  }
'{   result in severe civil and criminal penalties and will be       }
'{   prosecuted to the maximum extent possible under the law.        }
'{                                                                   }
'{   RESTRICTIONS                                                    }
'{                                                                   }
'{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
'{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
'{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
'{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
'{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
'{                                                                   }
'{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
'{   ADDITIONAL RESTRICTIONS.                                        }
'{                                                                   }
'{*******************************************************************}
#End Region
Imports DebtPlus.Utils
Imports System.Data.SqlClient
Imports System.Windows.Forms

Namespace Transfer.Oper.Oper
    Friend Class Form_Transfer_OperOper

        Public Sub New()
            MyBase.New()
            InitializeComponent()

        End Sub

        Private ap As ArgParser = Nothing
        Public Sub New(ByVal ap As ArgParser)
            MyClass.New()
            MyClass.ap = ap
        End Sub

        ' Linkage to our resource manager tables
        Protected RM As New System.Resources.ResourceManager("DebtPlus.UI.Desktop.Form_Transfer_OperOper_Strings", System.Reflection.Assembly.GetExecutingAssembly())

        Private Sub Form_Transfer_OperOper_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

            ' Load the source ledger code table
            Dim ledger_vue As System.Data.DataView = LedgerCodesTableView()
            With LookUpEdit_SourceAccount
                With .Properties
                    .DataSource = ledger_vue
                    .PopulateColumns()
                    .Columns("Default").Visible = False         ' Do not display the default column
                    .Columns("ActiveFlag").Visible = False      ' Do not display the active flag
                    .DisplayMember = "Description"              ' Show the description
                    .ValueMember = "Ledger Code"                ' but the ledger code is what we want
                    .NullText = ""                              ' do not show anything if there is no item
                End With

                ' Try to select the default item
                Dim vue As System.Data.DataView = New System.Data.DataView(ledger_vue.Table, "Default<>0", "", DataViewRowState.CurrentRows)
                If vue.Count > 0 Then .EditValue = vue(0).Item("Ledger Code")

                ' Bind the controls
                AddHandler .EditValueChanging, AddressOf LookUpEdit_SourceAccount_EditValueChanging
                AddHandler .EditValueChanged, AddressOf LookUpEdit_SourceAccount_EditValueChanged
            End With

            ' Load the ledger code table list
            With LookUpEdit_DestAccount
                With .Properties
                    .DataSource = ledger_vue
                    .PopulateColumns()
                    .Columns("Default").Visible = False         ' Do not display the default column
                    .Columns("ActiveFlag").Visible = False      ' Do not display the active flag
                    .DisplayMember = "Description"              ' Show the description
                    .ValueMember = "Ledger Code"                ' but the ledger code is what we want
                    .NullText = ""                              ' do not show anything if there is no item
                End With

                ' Try to select the default item
                Dim vue As System.Data.DataView = New System.Data.DataView(CType(.Properties.DataSource, System.Data.DataView).Table, "Default<>0", "", DataViewRowState.CurrentRows)
                If vue.Count > 0 Then .EditValue = vue(0).Item("Ledger Code")

                ' Bind the controls
                AddHandler .EditValueChanging, AddressOf DebtPlus.Data.Validation.LookUpEdit_ActiveTest
                AddHandler .EditValueChanging, AddressOf LookUpEdit_SourceAccount_EditValueChanging
                AddHandler .EditValueChanged, AddressOf LookUpEdit_SourceAccount_EditValueChanged
            End With

            ' Load the list of reasons
            Load_Transfer_Reasons("OO")

            ' Clear the form
            clear_form()

            ' Register the handlers for the routines
            AddHandler txc_amount.Validated, AddressOf txc_amount_Validated
            AddHandler cbo_Reason.DragDrop, AddressOf drag_drop_DragDrop
            AddHandler cbo_Reason.DragEnter, AddressOf drag_drop_DragEnter
            AddHandler cbo_Reason.MouseMove, AddressOf drag_drop_MouseMove
            AddHandler cbo_Reason.QueryContinueDrag, AddressOf drag_drop_QueryContinueDrag
            AddHandler Button_Cancel.Click, AddressOf Button_Cancel_Click
            AddHandler Button_Transfer.Click, AddressOf Button_Transfer_Click
        End Sub

        Private Function LedgerCodesTableView() As System.Data.DataView
            Dim answer As System.Data.DataView = Nothing
            Dim ds As New System.Data.DataSet("ledger_codes")

            Dim cn As SqlClient.SqlConnection = New SqlConnection(LibraryGlobals.SqlInfo.ConnectionString)
            Dim cmd As SqlClient.SqlCommand = New SqlCommand
            With cmd
                .Connection = cn
                .CommandText = "SELECT [ledger_code] as [Ledger Code],[description] as Description, 1 as [ActiveFlag], 0 as [Default] FROM ledger_codes WITH (NOLOCK) ORDER BY description"
            End With

            ' Fill the ledger codes
            Try
                Dim da As New SqlClient.SqlDataAdapter(cmd)
                da.Fill(ds, "ledger_codes")
                answer = ds.Tables(0).DefaultView

            Catch ex As SqlClient.SqlException
                DevExpress.XtraEditors.XtraMessageBox.Show(ex.Message, "Error loading ledger codes", MessageBoxButtons.OK, MessageBoxIcon.Error)

            Finally
                If cn IsNot Nothing AndAlso cn.State <> ConnectionState.Closed Then cn.Close()
            End Try

            Return answer
        End Function

        Private Sub Load_Transfer_Reasons(ByVal ItemType As String)
            Dim cn As SqlClient.SqlConnection = New SqlConnection(LibraryGlobals.SqlInfo.ConnectionString)

            Try
                ' Attempt to read the information from the database
                cn.Open()
                With New SqlCommand
                    .Connection = cn
                    .CommandText = "Execute lst_descriptions_" + ItemType
                    Dim rd As SqlClient.SqlDataReader = .ExecuteReader(CommandBehavior.CloseConnection Or CommandBehavior.SingleResult)

                    ' Try to find if a default item is given as well as an active status.
                    Dim default_column As System.Int32 = -1
                    Dim active_column As System.Int32 = -1
                    For col As System.Int32 = 0 To rd.FieldCount - 1
                        Select Case rd.GetName(col).ToLower
                            Case "default"
                                default_column = col
                            Case "activeflag"
                                active_column = col
                        End Select
                    Next

                    ' Load the list of items into the control
                    With cbo_Reason
                        With .Properties
                            With .Items
                                .Clear()
                                Do While rd.Read

                                    ' Add the item. If there is an active status then load it otherwise use "TRUE"
                                    Dim NewItem As System.Int32
                                    If active_column >= 0 Then
                                        NewItem = .Add(New DebtPlus.Data.Controls.ComboboxItem(rd.GetString(rd.GetOrdinal("description")), rd.GetInt32(rd.GetOrdinal("item_key")), rd.GetBoolean(active_column)))
                                    Else
                                        NewItem = .Add(New DebtPlus.Data.Controls.ComboboxItem(rd.GetString(rd.GetOrdinal("description")), rd.GetInt32(rd.GetOrdinal("item_key")), True))
                                    End If

                                    ' Set the default value if one is present
                                    If default_column >= 0 Then
                                        If rd.GetBoolean(default_column) Then cbo_Reason.SelectedIndex = NewItem
                                    End If
                                Loop
                            End With
                        End With

                        ' Select the top item if there is one and none are defaulted
                        If .SelectedIndex < 0 AndAlso .Properties.Items.Count > 0 Then .SelectedIndex = 0
                    End With

                    rd.Close()
                End With

            Catch ex As Exception
                DevExpress.XtraEditors.XtraMessageBox.Show(ex.ToString(), "Error loading form", MessageBoxButtons.OK, MessageBoxIcon.Error, MessageBoxDefaultButton.Button1)

            Finally
                If cn IsNot Nothing AndAlso cn.State <> ConnectionState.Closed Then cn.Close()
            End Try
        End Sub

        Private Sub clear_form()

            ' Clear the transfer information
            LookUpEdit_SourceAccount.EditValue = Nothing
            LookUpEdit_DestAccount.EditValue = Nothing
            txc_amount.EditValue = Nothing

            ' Set the error conditions for the values that are missing
            validate_source()
            validate_destination()
            validate_reason()
            validate_amount()

            ' Disable the transfer button if there are errors
            Button_Transfer.Enabled = Not HasErrors()
        End Sub

        Private Sub LookUpEdit_SourceAccount_EditValueChanging(ByVal sender As Object, ByVal e As DevExpress.XtraEditors.Controls.ChangingEventArgs)

            ' If the new value is defined then make sure that the office is still active.
            With CType(sender, DevExpress.XtraEditors.LookUpEdit)
                If e.NewValue IsNot Nothing Then

                    ' Find the new office being selected
                    Dim IDKey As String = Convert.ToString(e.NewValue)
                    Dim drv As DataRowView = CType(.Properties.GetDataSourceRowByKeyValue(IDKey), System.Data.DataRowView)

                    ' From the office, ensure that the active flag is set
                    If Not Convert.ToBoolean(drv.Item("ActiveFlag")) Then
                        e.Cancel = True
                        DevExpress.XtraEditors.XtraMessageBox.Show("The ledger account is no longer active. Please do not use this account.", "Bank selection is not valid", MessageBoxButtons.OK, MessageBoxIcon.Hand, MessageBoxDefaultButton.Button1)
                    End If
                End If
            End With
        End Sub

        Private Sub LookUpEdit_SourceAccount_EditValueChanged(ByVal sender As Object, ByVal e As System.EventArgs)
            validate_source()
            validate_destination()
        End Sub

        Private Function HasErrors() As Boolean
            Dim answer As Boolean = True

            Do
                If DxErrorProvider1.GetError(LookUpEdit_SourceAccount) <> System.String.Empty Then Exit Do
                If DxErrorProvider1.GetError(LookUpEdit_DestAccount) <> System.String.Empty Then Exit Do

                ' The account numbers must be different
                If source_ledger_code() = dest_ledger_code() Then
                    DxErrorProvider1.SetError(LookUpEdit_DestAccount, RM.GetString("same_account"))
                    Exit Do
                End If

                ' All is well
                answer = False
                Exit Do
            Loop

            Return answer
        End Function

        Private Sub txc_amount_Validated(ByVal sender As System.Object, ByVal e As System.EventArgs)
            validate_amount()
        End Sub

        Private Property src_amount() As Decimal
            Get
                With txc_amount
                    If .Text = System.String.Empty Then Return 0D
                    If .EditValue Is Nothing Then Return 0D
                    Return Convert.ToDecimal(.EditValue)
                End With
            End Get
            Set(ByVal Value As Decimal)
                txc_amount.EditValue = Value
            End Set
        End Property

        Private Sub validate_amount()
            Dim error_message As String = System.String.Empty

            ' If the amount is negative then reject the transfer
            If txc_amount.Text.Trim() = System.String.Empty Then
                error_message = RM.GetString("required_value")
            ElseIf src_amount <= 0D Then
                error_message = RM.GetString("must_be_posative")
            End If

            ' Set the error text
            DxErrorProvider1.SetError(txc_amount, error_message)
            Button_Transfer.Enabled = Not HasErrors()
        End Sub

        Private Sub drag_drop_MouseMove(ByVal sender As System.Object, ByVal e As System.Windows.Forms.MouseEventArgs)

            With CType(sender, System.Windows.Forms.Control)
                ' Do nothing if the operation is not valid
                If e.Button = 0 Then Exit Sub
                If e.X >= 0 AndAlso e.X < .Width AndAlso e.Y >= 0 AndAlso e.Y < .Height Then Exit Sub

                Dim txt As String = System.String.Empty

                ' If this is a text box then look for the selected text component of the text field
                If TypeOf sender Is DevExpress.XtraEditors.TextEdit Then
                    With CType(sender, DevExpress.XtraEditors.TextEdit)
                        If .SelectionLength > 0 Then txt = .SelectedText
                    End With
                End If

                ' If the text is missing then try the entire text field
                If txt = System.String.Empty Then txt = .Text.Trim()

                ' If there is no text then do nothing
                If txt = System.String.Empty Then Exit Sub

                ' Start a drag-drop operation when the mouse moves outside the control
                Dim dobj As New System.Windows.Forms.DataObject
                dobj.SetData(DataFormats.Text, True, txt)

                ' Do the operation. It will return when the operation is complete.
                Dim effect As DragDropEffects = DragDropEffects.Copy
                effect = .DoDragDrop(dobj, effect)

                ' If the operation result was move then erase the text.
                ' However, since we don't do the move then nothing more is needed.
            End With
        End Sub

        Private Sub drag_drop_DragEnter(ByVal sender As System.Object, ByVal e As System.Windows.Forms.DragEventArgs)

            ' If there is a text field then look to determine the accepable processing
            If e.Data.GetDataPresent(DataFormats.Text, True) Then
                e.Effect = e.AllowedEffect And DragDropEffects.Copy
            Else
                e.Effect = DragDropEffects.None
            End If
        End Sub

        Private Sub drag_drop_DragDrop(ByVal sender As System.Object, ByVal e As System.Windows.Forms.DragEventArgs)

            ' If there is a text field then look to determine the accepable processing
            If Not e.Data.GetDataPresent(DataFormats.Text, True) Then Return

            ' We only support copy. No modifier needs to be tested
            e.Effect = e.AllowedEffect And DragDropEffects.Copy

            ' Paste the text into the control
            With CType(sender, System.Windows.Forms.Control)
                .Text = Convert.ToString(e.Data.GetData(DataFormats.Text, True))

                If sender Is cbo_Reason Then
                    cbo_Reason_EditValueChanged(sender, New EventArgs)
                ElseIf sender Is txc_amount Then
                    txc_amount_Validated(sender, New EventArgs)
                End If
            End With
        End Sub

        Private Sub drag_drop_QueryContinueDrag(ByVal sender As System.Object, ByVal e As System.Windows.Forms.QueryContinueDragEventArgs)
            If e.EscapePressed Then e.Action = DragAction.Cancel
        End Sub

        Private Sub Button_Cancel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button_Cancel.Click
            Close()
        End Sub

        Private Sub cbo_source_account_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs)
            validate_source()
        End Sub

        Private Sub validate_source()
            Dim error_message As String = System.String.Empty

            ' The only requirement is that there be a value.
            If source_ledger_code() = System.String.Empty Then
                error_message = RM.GetString("required_value")
            End If

            ' Set the error message text
            DxErrorProvider1.SetError(LookUpEdit_SourceAccount, error_message)
            Button_Transfer.Enabled = Not HasErrors()
        End Sub

        Private Function source_ledger_code() As String
            Dim answer As String = System.String.Empty
            With LookUpEdit_SourceAccount
                If .EditValue IsNot Nothing Then answer = CType(.EditValue, String)
            End With
            Return answer
        End Function

        Private Sub cbo_dest_account_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs)
            validate_destination()
        End Sub

        Private Sub validate_destination()
            Dim error_message As String = System.String.Empty

            ' The only requirement is that there be a value.
            If dest_ledger_code() = System.String.Empty Then
                error_message = RM.GetString("required_value")
            End If

            ' Set the error message text
            DxErrorProvider1.SetError(LookUpEdit_DestAccount, error_message)
            Button_Transfer.Enabled = Not HasErrors()
        End Sub

        Private Function dest_ledger_code() As String
            Dim answer As String = System.String.Empty
            With LookUpEdit_DestAccount
                If .EditValue IsNot Nothing Then answer = CType(.EditValue, String)
            End With
            Return answer
        End Function

        Private Function reason() As String
            With cbo_Reason
                Return .Text.Trim()
            End With
        End Function

        Private Sub cbo_Reason_EditValueChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cbo_Reason.EditValueChanged
            validate_reason()
        End Sub

        Private Sub validate_reason()
            Dim error_message As String = System.String.Empty

            ' Ensure that there is a value for the information
            If reason() = System.String.Empty Then error_message = RM.GetString("required_value")

            ' Set the error text and enable the transfer button if there is no error
            DxErrorProvider1.SetError(cbo_Reason, error_message)
            Button_Transfer.Enabled = Not HasErrors()
        End Sub

        Private Sub Button_Transfer_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button_Transfer.Click
            Dim new_balance As Object
            Dim cn As SqlClient.SqlConnection = New SqlConnection(LibraryGlobals.SqlInfo.ConnectionString)

            ' Issue the request to the database to make the entry.
            Try
                cn.Open()
                With New SqlCommand
                    .CommandText = "xpr_transfer_op_op"
                    .CommandType = CommandType.StoredProcedure
                    .Connection = cn
                    With .Parameters
                        .Add("@SrcAccount", SqlDbType.VarChar, 80).Value = source_ledger_code()
                        .Add("@DestAccount", SqlDbType.VarChar, 80).Value = dest_ledger_code()
                        .Add("@Amount", SqlDbType.Decimal).Value = src_amount
                        .Add("@Description", SqlDbType.VarChar, 256).Value = reason()
                    End With

                    new_balance = .ExecuteScalar()
                End With

                ' Success. Reset the form
                clear_form()

                ' Go to the source account for the next field
                LookUpEdit_SourceAccount.Focus()

            Catch ex As SqlClient.SqlException
                Using gdr As New DebtPlus.Repository.GetDataResult()
                    gdr.HandleException(ex)
                    DebtPlus.UI.Common.ErrorHandling.HandleErrors(gdr)
                End Using

            Finally
                If cn IsNot Nothing AndAlso cn.State <> ConnectionState.Closed Then cn.Close()
            End Try
        End Sub
    End Class
End Namespace
