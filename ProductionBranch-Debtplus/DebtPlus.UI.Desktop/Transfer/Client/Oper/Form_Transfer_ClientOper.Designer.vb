﻿Imports DebtPlus.UI.Client.Widgets.Controls

Namespace Transfer.Client.Oper
    <Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
    Partial Class Form_Transfer_ClientOper
        Inherits DebtPlus.Data.Forms.DebtPlusForm

        'Form overrides dispose to clean up the component list.
        <System.Diagnostics.DebuggerNonUserCode()> _
        Protected Overrides Sub Dispose(ByVal disposing As Boolean)
            Try
                If disposing AndAlso components IsNot Nothing Then
                    components.Dispose()
                End If
            Finally
                MyBase.Dispose(disposing)
            End Try
        End Sub

        'Required by the Windows Form Designer
        Private components As System.ComponentModel.IContainer

        'NOTE: The following procedure is required by the Windows Form Designer
        'It can be modified using the Windows Form Designer.  
        'Do not modify it using the code editor.
        <System.Diagnostics.DebuggerStepThrough()> _
        Private Sub InitializeComponent()
            Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(Form_Transfer_ClientOper))
            Dim SerializableAppearanceObject1 As DevExpress.Utils.SerializableAppearanceObject = New DevExpress.Utils.SerializableAppearanceObject()
            Me.lbl_new_source_trust = New DevExpress.XtraEditors.LabelControl()
            Me.Label8 = New DevExpress.XtraEditors.LabelControl()
            Me.Label9 = New DevExpress.XtraEditors.LabelControl()
            Me.lbl_old_source_trust = New DevExpress.XtraEditors.LabelControl()
            Me.Label11 = New DevExpress.XtraEditors.LabelControl()
            Me.med_SourceName = New DevExpress.XtraEditors.MemoEdit()
            Me.Label12 = New DevExpress.XtraEditors.LabelControl()
            Me.GroupBox2 = New DevExpress.XtraEditors.GroupControl()
            Me.ClientID1 = New ClientID()
            Me.lbl_source_inactive = New DebtPlus.Data.Controls.BlinkLabel()
            Me.txc_amount = New DevExpress.XtraEditors.CalcEdit()
            Me.Label14 = New DevExpress.XtraEditors.LabelControl()
            Me.cbo_Reason = New DevExpress.XtraEditors.ComboBoxEdit()
            Me.Button_Transfer = New DevExpress.XtraEditors.SimpleButton()
            Me.Button_Cancel = New DevExpress.XtraEditors.SimpleButton()
            Me.GroupBox1 = New DevExpress.XtraEditors.GroupControl()
            Me.LookUpEdit_DestAccount = New DevExpress.XtraEditors.LookUpEdit()
            Me.Label1 = New DevExpress.XtraEditors.LabelControl()
            CType(Me.DxErrorProvider1, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.med_SourceName.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.GroupBox2, System.ComponentModel.ISupportInitialize).BeginInit()
            Me.GroupBox2.SuspendLayout()
            CType(Me.ClientID1.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.txc_amount.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.cbo_Reason.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.GroupBox1, System.ComponentModel.ISupportInitialize).BeginInit()
            Me.GroupBox1.SuspendLayout()
            CType(Me.LookUpEdit_DestAccount.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
            Me.SuspendLayout()
            '
            'lbl_new_source_trust
            '
            Me.lbl_new_source_trust.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
            Me.lbl_new_source_trust.Appearance.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center
            Me.lbl_new_source_trust.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None
            Me.lbl_new_source_trust.Location = New System.Drawing.Point(368, 78)
            Me.lbl_new_source_trust.Name = "lbl_new_source_trust"
            Me.lbl_new_source_trust.Size = New System.Drawing.Size(62, 13)
            Me.lbl_new_source_trust.TabIndex = 9
            Me.lbl_new_source_trust.Text = "$0.00"
            Me.lbl_new_source_trust.ToolTipController = Me.ToolTipController1
            Me.lbl_new_source_trust.UseMnemonic = False
            '
            'Label8
            '
            Me.Label8.Location = New System.Drawing.Point(256, 78)
            Me.Label8.Name = "Label8"
            Me.Label8.Size = New System.Drawing.Size(81, 13)
            Me.Label8.TabIndex = 8
            Me.Label8.Text = "New trust Balace"
            Me.Label8.ToolTipController = Me.ToolTipController1
            Me.Label8.UseMnemonic = False
            '
            'Label9
            '
            Me.Label9.Location = New System.Drawing.Point(256, 52)
            Me.Label9.Name = "Label9"
            Me.Label9.Size = New System.Drawing.Size(67, 13)
            Me.Label9.TabIndex = 6
            Me.Label9.Text = "Less transfer:"
            Me.Label9.ToolTipController = Me.ToolTipController1
            Me.Label9.UseMnemonic = False
            '
            'lbl_old_source_trust
            '
            Me.lbl_old_source_trust.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
            Me.lbl_old_source_trust.Appearance.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center
            Me.lbl_old_source_trust.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None
            Me.lbl_old_source_trust.Location = New System.Drawing.Point(368, 26)
            Me.lbl_old_source_trust.Name = "lbl_old_source_trust"
            Me.lbl_old_source_trust.Size = New System.Drawing.Size(62, 13)
            Me.lbl_old_source_trust.TabIndex = 4
            Me.lbl_old_source_trust.Text = "$0.00"
            Me.lbl_old_source_trust.ToolTipController = Me.ToolTipController1
            Me.lbl_old_source_trust.UseMnemonic = False
            '
            'Label11
            '
            Me.Label11.Location = New System.Drawing.Point(256, 26)
            Me.Label11.Name = "Label11"
            Me.Label11.Size = New System.Drawing.Size(104, 13)
            Me.Label11.TabIndex = 3
            Me.Label11.Text = "Original Trust Balance"
            Me.Label11.ToolTipController = Me.ToolTipController1
            Me.Label11.UseMnemonic = False
            '
            'med_SourceName
            '
            Me.med_SourceName.EditValue = ""
            Me.med_SourceName.Location = New System.Drawing.Point(16, 52)
            Me.med_SourceName.Name = "med_SourceName"
            Me.med_SourceName.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Flat
            Me.med_SourceName.Properties.ReadOnly = True
            Me.med_SourceName.Properties.ScrollBars = System.Windows.Forms.ScrollBars.None
            Me.med_SourceName.Size = New System.Drawing.Size(232, 51)
            Me.med_SourceName.TabIndex = 5
            Me.med_SourceName.TabStop = False
            Me.med_SourceName.ToolTip = "Client name and address (display only)"
            Me.med_SourceName.ToolTipController = Me.ToolTipController1
            '
            'Label12
            '
            Me.Label12.Location = New System.Drawing.Point(16, 26)
            Me.Label12.Name = "Label12"
            Me.Label12.Size = New System.Drawing.Size(66, 13)
            Me.Label12.TabIndex = 1
            Me.Label12.Text = "Target Client:"
            Me.Label12.ToolTipController = Me.ToolTipController1
            '
            'GroupBox2
            '
            Me.GroupBox2.Controls.Add(Me.ClientID1)
            Me.GroupBox2.Controls.Add(Me.Label8)
            Me.GroupBox2.Controls.Add(Me.Label11)
            Me.GroupBox2.Controls.Add(Me.lbl_source_inactive)
            Me.GroupBox2.Controls.Add(Me.lbl_new_source_trust)
            Me.GroupBox2.Controls.Add(Me.lbl_old_source_trust)
            Me.GroupBox2.Controls.Add(Me.txc_amount)
            Me.GroupBox2.Controls.Add(Me.Label9)
            Me.GroupBox2.Controls.Add(Me.med_SourceName)
            Me.GroupBox2.Controls.Add(Me.Label12)
            Me.GroupBox2.Location = New System.Drawing.Point(8, 17)
            Me.GroupBox2.Name = "GroupBox2"
            Me.GroupBox2.Size = New System.Drawing.Size(472, 112)
            Me.GroupBox2.TabIndex = 0
            Me.GroupBox2.Text = " Transfer from the client "
            '
            'ClientID1
            '
            Me.ClientID1.AllowDrop = True
            Me.ClientID1.EnterMoveNextControl = True
            Me.ClientID1.Location = New System.Drawing.Point(128, 26)
            Me.ClientID1.Name = "ClientID1"
            Me.ClientID1.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[False]
            Me.ClientID1.Properties.Appearance.Options.UseTextOptions = True
            Me.ClientID1.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
            Me.ClientID1.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph, "", -1, True, True, False, DevExpress.XtraEditors.ImageLocation.MiddleCenter, CType(resources.GetObject("ClientID1.Properties.Buttons"), System.Drawing.Image), New DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), SerializableAppearanceObject1, "Click here to search for a client ID", "search", Nothing, True)})
            Me.ClientID1.Properties.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
            Me.ClientID1.Properties.DisplayFormat.FormatString = "0000000"
            Me.ClientID1.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Custom
            Me.ClientID1.Properties.EditFormat.FormatString = "f0"
            Me.ClientID1.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric
            Me.ClientID1.Properties.Mask.BeepOnError = True
            Me.ClientID1.Properties.Mask.EditMask = "\d*"
            Me.ClientID1.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.RegEx
            Me.ClientID1.Properties.ValidateOnEnterKey = True
            Me.ClientID1.Size = New System.Drawing.Size(88, 20)
            Me.ClientID1.TabIndex = 2
            '
            'lbl_source_inactive
            '
            Me.lbl_source_inactive.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
            Me.lbl_source_inactive.Appearance.BackColor = System.Drawing.Color.Red
            Me.lbl_source_inactive.Appearance.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
            Me.lbl_source_inactive.Appearance.ForeColor = System.Drawing.Color.White
            Me.lbl_source_inactive.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center
            Me.lbl_source_inactive.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None
            Me.lbl_source_inactive.BlinkTime = 500
            Me.lbl_source_inactive.CausesValidation = False
            Me.lbl_source_inactive.Location = New System.Drawing.Point(314, 0)
            Me.lbl_source_inactive.Name = "lbl_source_inactive"
            Me.lbl_source_inactive.Size = New System.Drawing.Size(158, 20)
            Me.lbl_source_inactive.TabIndex = 0
            Me.lbl_source_inactive.ToolTipController = Me.ToolTipController1
            Me.lbl_source_inactive.UseMnemonic = False
            Me.lbl_source_inactive.Visible = False
            '
            'txc_amount
            '
            Me.txc_amount.AllowDrop = True
            Me.txc_amount.EnterMoveNextControl = True
            Me.txc_amount.Location = New System.Drawing.Point(368, 52)
            Me.txc_amount.Name = "txc_amount"
            Me.txc_amount.Properties.Appearance.Options.UseTextOptions = True
            Me.txc_amount.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
            Me.txc_amount.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
            Me.txc_amount.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
            Me.txc_amount.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric
            Me.txc_amount.Size = New System.Drawing.Size(80, 20)
            Me.txc_amount.TabIndex = 7
            Me.txc_amount.ToolTip = "Amount to be transferred"
            Me.txc_amount.ToolTipController = Me.ToolTipController1
            '
            'Label14
            '
            Me.Label14.Location = New System.Drawing.Point(8, 260)
            Me.Label14.Name = "Label14"
            Me.Label14.Size = New System.Drawing.Size(114, 13)
            Me.Label14.TabIndex = 2
            Me.Label14.Text = "Reason for the transfer"
            Me.Label14.ToolTipController = Me.ToolTipController1
            '
            'cbo_Reason
            '
            Me.cbo_Reason.AllowDrop = True
            Me.cbo_Reason.EnterMoveNextControl = True
            Me.cbo_Reason.Location = New System.Drawing.Point(136, 257)
            Me.cbo_Reason.Name = "cbo_Reason"
            Me.cbo_Reason.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
            Me.cbo_Reason.Properties.MaxLength = 50
            Me.cbo_Reason.Properties.NullText = "Please choose a reason from this list..."
            Me.cbo_Reason.Size = New System.Drawing.Size(320, 20)
            Me.cbo_Reason.TabIndex = 3
            Me.cbo_Reason.ToolTip = "Enter or choose the reason for the transfer."
            Me.cbo_Reason.ToolTipController = Me.ToolTipController1
            '
            'Button_Transfer
            '
            Me.Button_Transfer.Enabled = False
            Me.Button_Transfer.Location = New System.Drawing.Point(151, 302)
            Me.Button_Transfer.Name = "Button_Transfer"
            Me.Button_Transfer.Size = New System.Drawing.Size(75, 24)
            Me.Button_Transfer.TabIndex = 4
            Me.Button_Transfer.Text = "Transfer"
            Me.Button_Transfer.ToolTipController = Me.ToolTipController1
            '
            'Button_Cancel
            '
            Me.Button_Cancel.CausesValidation = False
            Me.Button_Cancel.DialogResult = System.Windows.Forms.DialogResult.Cancel
            Me.Button_Cancel.Location = New System.Drawing.Point(263, 302)
            Me.Button_Cancel.Name = "Button_Cancel"
            Me.Button_Cancel.Size = New System.Drawing.Size(75, 24)
            Me.Button_Cancel.TabIndex = 5
            Me.Button_Cancel.Text = "&Quit"
            Me.Button_Cancel.ToolTipController = Me.ToolTipController1
            '
            'GroupBox1
            '
            Me.GroupBox1.AppearanceCaption.BackColor = System.Drawing.Color.DodgerBlue
            Me.GroupBox1.AppearanceCaption.Options.UseBackColor = True
            Me.GroupBox1.Controls.Add(Me.LookUpEdit_DestAccount)
            Me.GroupBox1.Controls.Add(Me.Label1)
            Me.GroupBox1.Location = New System.Drawing.Point(8, 138)
            Me.GroupBox1.Name = "GroupBox1"
            Me.GroupBox1.Size = New System.Drawing.Size(472, 60)
            Me.GroupBox1.TabIndex = 1
            Me.GroupBox1.Text = " To the Operating Account  "
            '
            'LookUpEdit_DestAccount
            '
            Me.LookUpEdit_DestAccount.EnterMoveNextControl = True
            Me.LookUpEdit_DestAccount.Location = New System.Drawing.Point(128, 25)
            Me.LookUpEdit_DestAccount.Name = "LookUpEdit_DestAccount"
            Me.LookUpEdit_DestAccount.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
            Me.LookUpEdit_DestAccount.Properties.NullText = "Please choose an account from this list..."
            Me.LookUpEdit_DestAccount.Size = New System.Drawing.Size(320, 20)
            Me.LookUpEdit_DestAccount.TabIndex = 1
            Me.LookUpEdit_DestAccount.ToolTip = "Destination Non-AR ledger account for the transfer"
            Me.LookUpEdit_DestAccount.ToolTipController = Me.ToolTipController1
            '
            'Label1
            '
            Me.Label1.Location = New System.Drawing.Point(16, 25)
            Me.Label1.Name = "Label1"
            Me.Label1.Size = New System.Drawing.Size(43, 13)
            Me.Label1.TabIndex = 0
            Me.Label1.Text = "Account:"
            Me.Label1.ToolTipController = Me.ToolTipController1
            '
            'Form_Transfer_ClientOper
            '
            Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
            Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
            Me.CancelButton = Me.Button_Cancel
            Me.ClientSize = New System.Drawing.Size(488, 342)
            Me.Controls.Add(Me.GroupBox1)
            Me.Controls.Add(Me.Button_Cancel)
            Me.Controls.Add(Me.Button_Transfer)
            Me.Controls.Add(Me.cbo_Reason)
            Me.Controls.Add(Me.Label14)
            Me.Controls.Add(Me.GroupBox2)
            Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog
            Me.MaximizeBox = False
            Me.Name = "Form_Transfer_ClientOper"
            Me.Text = "Client To Operating Account Transfer of funds"
            CType(Me.DxErrorProvider1, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.med_SourceName.Properties, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.GroupBox2, System.ComponentModel.ISupportInitialize).EndInit()
            Me.GroupBox2.ResumeLayout(False)
            Me.GroupBox2.PerformLayout()
            CType(Me.ClientID1.Properties, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.txc_amount.Properties, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.cbo_Reason.Properties, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.GroupBox1, System.ComponentModel.ISupportInitialize).EndInit()
            Me.GroupBox1.ResumeLayout(False)
            Me.GroupBox1.PerformLayout()
            CType(Me.LookUpEdit_DestAccount.Properties, System.ComponentModel.ISupportInitialize).EndInit()
            Me.ResumeLayout(False)
            Me.PerformLayout()

        End Sub
        Friend WithEvents Label8 As DevExpress.XtraEditors.LabelControl
        Friend WithEvents Label9 As DevExpress.XtraEditors.LabelControl
        Friend WithEvents Label11 As DevExpress.XtraEditors.LabelControl
        Friend WithEvents Label12 As DevExpress.XtraEditors.LabelControl
        Friend WithEvents GroupBox2 As DevExpress.XtraEditors.GroupControl
        Friend WithEvents Label14 As DevExpress.XtraEditors.LabelControl
        Friend WithEvents cbo_Reason As DevExpress.XtraEditors.ComboBoxEdit
        Friend WithEvents Button_Transfer As DevExpress.XtraEditors.SimpleButton
        Friend WithEvents Button_Cancel As DevExpress.XtraEditors.SimpleButton
        Friend WithEvents lbl_old_source_trust As DevExpress.XtraEditors.LabelControl
        Friend WithEvents lbl_source_inactive As DebtPlus.Data.Controls.BlinkLabel
        Friend WithEvents GroupBox1 As DevExpress.XtraEditors.GroupControl
        Friend WithEvents Label1 As DevExpress.XtraEditors.LabelControl
        Friend WithEvents LookUpEdit_DestAccount As DevExpress.XtraEditors.LookUpEdit
        Friend WithEvents lbl_new_source_trust As DevExpress.XtraEditors.LabelControl
        Friend WithEvents med_SourceName As DevExpress.XtraEditors.MemoEdit
        Friend WithEvents txc_amount As DevExpress.XtraEditors.CalcEdit
        Friend WithEvents ClientID1 As ClientID
    End Class
End Namespace
