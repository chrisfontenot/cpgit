#Region "Copyright 2000-2012 DebtPlus, L.L.C."
'{*******************************************************************}
'{                                                                   }
'{       DebtPlus Debt Management System                             }
'{                                                                   }
'{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
'{       ALL RIGHTS RESERVED                                         }
'{                                                                   }
'{   The entire contents of this file is protected by U.S. and       }
'{   International Copyright Laws. Unauthorized reproduction,        }
'{   reverse-engineering, and distribution of all or any portion of  }
'{   the code contained in this file is strictly prohibited and may  }
'{   result in severe civil and criminal penalties and will be       }
'{   prosecuted to the maximum extent possible under the law.        }
'{                                                                   }
'{   RESTRICTIONS                                                    }
'{                                                                   }
'{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
'{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
'{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
'{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
'{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
'{                                                                   }
'{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
'{   ADDITIONAL RESTRICTIONS.                                        }
'{                                                                   }
'{*******************************************************************}
#End Region
Imports DebtPlus.Utils
Imports System.Data.SqlClient
Imports System.Windows.Forms

Namespace Transfer.Client.Client

    Friend Class Form_Transfer_ClientClient

        Friend ap As ArgParser = Nothing

        Public Sub New()
            MyBase.New()
            InitializeComponent()

        End Sub

        Public Sub New(ByVal ap As ArgParser)
            MyClass.New()
            MyClass.ap = ap
        End Sub

        ' Linkage to our resource manager tables
        Protected RM As New System.Resources.ResourceManager("DebtPlus.UI.Desktop.Form_Transfer_ClientClient_Strings", System.Reflection.Assembly.GetExecutingAssembly)

        Private Sub Form_Transfer_ClientClient_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

            ' Load the list of reasons
            Load_Transfer_Reasons("CC")

            ' Clear the form
            clear_form()
        End Sub

        Private Sub Load_Transfer_Reasons(ByVal ItemType As String)
            Dim cn As SqlClient.SqlConnection = New SqlConnection(LibraryGlobals.SqlInfo.ConnectionString)
            Dim rd As SqlClient.SqlDataReader = Nothing

            Try
                ' Attempt to read the information from the database
                cn.Open()
                Using cmd As SqlClient.SqlCommand = New SqlCommand
                    With cmd
                        .Connection = cn
                        .CommandText = "lst_descriptions_" + ItemType
                        .CommandType = CommandType.StoredProcedure

                        rd = .ExecuteReader(CommandBehavior.CloseConnection Or CommandBehavior.SingleResult)
                    End With
                End Using

                ' Try to find if a default item is given as well as an active status.
                Dim default_column As System.Int32 = -1
                Dim active_column As System.Int32 = -1
                For col As System.Int32 = 0 To rd.FieldCount - 1
                    Select Case rd.GetName(col).ToLower
                        Case "default"
                            default_column = col
                        Case "activeflag"
                            active_column = col
                    End Select
                Next

                ' Load the list of items into the control
                With cbo_Reason
                    With .Properties
                        With .Items
                            .Clear()
                            Do While rd.Read

                                ' Add the item. If there is an active status then load it otherwise use "TRUE"
                                Dim NewItem As System.Int32
                                If active_column >= 0 Then
                                    NewItem = .Add(New DebtPlus.Data.Controls.ComboboxItem(rd.GetString(rd.GetOrdinal("description")), rd.GetInt32(rd.GetOrdinal("item_key")), rd.GetBoolean(active_column)))
                                Else
                                    NewItem = .Add(New DebtPlus.Data.Controls.ComboboxItem(rd.GetString(rd.GetOrdinal("description")), rd.GetInt32(rd.GetOrdinal("item_key")), True))
                                End If

                                ' Set the default value if one is present
                                If default_column >= 0 Then
                                    If rd.GetBoolean(default_column) Then cbo_Reason.SelectedIndex = NewItem
                                End If
                            Loop
                        End With
                    End With

                    ' Select the top item if there is one and none are defaulted
                    If .SelectedIndex < 0 AndAlso .Properties.Items.Count > 0 Then .SelectedIndex = 0
                End With

            Catch ex As Exception
                DebtPlus.Data.Forms.MessageBox.Show(ex.ToString(), "Error loading form", MessageBoxButtons.OK, MessageBoxIcon.Error, MessageBoxDefaultButton.Button1)

            Finally
                If cn IsNot Nothing Then cn.Dispose()
            End Try
        End Sub

        Private Sub clear_form()

            ' Clear the transfer information
            txb_source_client.EditValue = Nothing
            txb_dest_client.EditValue = Nothing
            txc_amount.EditValue = Nothing

            ' Clear the inactive warning labels
            lbl_dest_inactive.Text = String.Empty
            lbl_source_inactive.Text = String.Empty

            ' Clear the dollar amounts
            old_source_trust = 0D
            new_source_trust = 0D

            old_dest_trust = 0D
            dest_amount = 0D
            new_dest_trust = 0D

            ' Set the error conditions for the values that are missing
            validate_source()
            validate_destination()
            validate_amount()
            validate_reason()

            ' Disable the transfer button if there are errors
            Button_Transfer.Enabled = Not HasErrors()
        End Sub

        Private Function HasErrors() As Boolean
            Dim answer As Boolean = True

            Do
                If DxErrorProvider1.GetError(txb_source_client) <> System.String.Empty Then Exit Do
                If DxErrorProvider1.GetError(txb_dest_client) <> System.String.Empty Then Exit Do

                ' Ensure that the client numbers are provided
                If src_client() = dest_client() Then
                    DxErrorProvider1.SetError(txb_dest_client, RM.GetString("same_client"))
                    Exit Do
                End If

                ' After checking for duplicate client numbers, look at the other fields.
                If DxErrorProvider1.GetError(txc_amount) <> System.String.Empty Then Exit Do
                If DxErrorProvider1.GetError(cbo_Reason) <> System.String.Empty Then Exit Do

                ' If the client has enough in the trust then accept the form
                If new_source_trust < 0D Then Exit Do

                ' All is well
                answer = False
                Exit Do
            Loop

            Return answer
        End Function

        Private Property src_amount() As Decimal
            Get
                With txc_amount
                    If .Text = System.String.Empty Then Return 0D
                    If .EditValue Is Nothing Then Return 0D
                    Return Convert.ToDecimal(.EditValue)
                End With
            End Get
            Set(ByVal Value As Decimal)
                txc_amount.EditValue = Value
            End Set
        End Property

        Private Function src_client() As System.Int32
            With txb_source_client
                If .EditValue Is Nothing Then Return -1
                Return Convert.ToInt32(.EditValue)
            End With
        End Function

        Private _new_source_trust As Decimal = 0D
        Private Property new_source_trust() As Decimal
            Get
                Return _new_source_trust
            End Get
            Set(ByVal Value As Decimal)
                _new_source_trust = Value
                With lbl_new_source_trust
                    .Text = System.String.Format("{0:c}", Value)
                    If Value < 0D Then
                        .ForeColor = System.Drawing.Color.Red
                    Else
                        .ForeColor = System.Drawing.Color.Black
                    End If
                End With
            End Set
        End Property

        Private _old_source_trust As Decimal = 0D
        Private Property old_source_trust() As Decimal
            Get
                Return _old_source_trust
            End Get
            Set(ByVal Value As Decimal)
                _old_source_trust = Value
                With lbl_old_source_trust
                    .Text = System.String.Format("{0:c}", Value)
                    If Value < 0D Then
                        .ForeColor = System.Drawing.Color.Red
                    Else
                        .ForeColor = System.Drawing.Color.Black
                    End If
                End With
            End Set
        End Property

        Private _dest_amount As Decimal = 0D
        Private Property dest_amount() As Decimal
            Get
                Return _dest_amount
            End Get
            Set(ByVal Value As Decimal)
                _dest_amount = Value
                With lbl_dest_amount
                    .Text = System.String.Format("{0:c}", Value)
                    If Value < 0D Then
                        .ForeColor = System.Drawing.Color.Red
                    Else
                        .ForeColor = System.Drawing.Color.Black
                    End If
                End With
            End Set
        End Property

        Private _new_dest_trust As Decimal = 0D
        Private Property new_dest_trust() As Decimal
            Get
                Return _new_dest_trust
            End Get
            Set(ByVal Value As Decimal)
                _new_dest_trust = Value
                With lbl_new_dest_trust
                    .Text = System.String.Format("{0:c}", Value)
                    If Value < 0D Then
                        .ForeColor = System.Drawing.Color.Red
                    Else
                        .ForeColor = System.Drawing.Color.Black
                    End If
                End With
            End Set
        End Property

        Private _old_dest_trust As Decimal = 0D
        Private Property old_dest_trust() As Decimal
            Get
                If src_client() = dest_client() Then
                    Return new_source_trust
                Else
                    Return _old_dest_trust
                End If
            End Get

            Set(ByVal Value As Decimal)
                _old_dest_trust = Value

                With lbl_old_dest_trust
                    .Text = System.String.Format("{0:c}", old_dest_trust)
                    If old_dest_trust < 0D Then
                        .ForeColor = System.Drawing.Color.Red
                    Else
                        .ForeColor = System.Drawing.Color.Black
                    End If
                End With
            End Set
        End Property

        Private Sub txb_source_client_EditValueChanging(ByVal sender As System.Object, ByVal e As DevExpress.XtraEditors.Controls.ChangingEventArgs) Handles txb_source_client.EditValueChanging
            med_SourceName.Text = System.String.Empty
        End Sub

        Private Sub cbo_Reason_EditValueChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cbo_Reason.EditValueChanged
            validate_reason()
        End Sub

        Private Sub validate_reason()
            Dim error_message As String = System.String.Empty

            ' Ensure that there is a value for the information
            With cbo_Reason
                If .Text.Trim().Length = 0 Then error_message = RM.GetString("required_value")
            End With

            ' Set the error text and enable the transfer button if there is no error
            DxErrorProvider1.SetError(cbo_Reason, error_message)
            Button_Transfer.Enabled = Not HasErrors()
        End Sub

        Private Sub txc_amount_Validated(ByVal sender As Object, ByVal e As System.EventArgs) Handles txc_amount.Validated
            validate_amount()
        End Sub

        Private Sub validate_amount()
            Dim error_message As String = System.String.Empty

            ' If the amount is negative then reject the transfer
            If txc_amount.Text.Trim() = System.String.Empty Then
                error_message = RM.GetString("required_value")
            ElseIf src_amount <= 0D Then
                error_message = RM.GetString("must_be_posative")
            End If

            ' Update the totals accordingly
            dest_amount = src_amount
            new_source_trust = old_source_trust - dest_amount
            new_dest_trust = old_dest_trust + dest_amount

            ' If the new trust is negative then reject the update
            If DxErrorProvider1.GetError(txc_amount) = System.String.Empty AndAlso new_source_trust < 0D Then error_message = RM.GetString("insufficient_funds")

            ' Set the error text
            DxErrorProvider1.SetError(txc_amount, error_message)
            Button_Transfer.Enabled = Not HasErrors()
        End Sub

        Private Sub txb_source_client_Validated(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txb_source_client.Validated
            validate_source()
        End Sub

        Private Sub validate_source()
            Dim error_message As String = System.String.Empty
            With txb_source_client
                lbl_source_inactive.Text = System.String.Empty

                ' There must be a client
                If .Text = System.String.Empty Then
                    error_message = RM.GetString("required_value")
                Else

                    ' Find the client. If the ClientId is invalid then reject the operation
                    If src_client() <= 0 Then
                        error_message = RM.GetString("invalid_client")
                    Else
                        Dim active_status As String = String.Empty

                        ' Read the client information from the database
                        If Not read_client(src_client, active_status, med_SourceName.Text, old_source_trust) Then
                            error_message = RM.GetString("invalid_client")
                        Else

                            ' If the client is not active then indicate as such
                            Select Case active_status
                                Case "A", "AR"
                                Case Else
                                    lbl_source_inactive.Text = "INACTIVE Client"
                            End Select
                        End If
                    End If
                End If
            End With

            ' Update the amount fields
            validate_amount()

            ' Set the error message text
            DxErrorProvider1.SetError(txb_source_client, error_message)
            Button_Transfer.Enabled = Not HasErrors()
        End Sub

        Private Function dest_client() As System.Int32
            With txb_dest_client
                If .Text = System.String.Empty Then Return -1
                If .EditValue Is Nothing Then Return -1
                Return Convert.ToInt32(.EditValue)
            End With
        End Function

        Private Sub txb_dest_client_Validated(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txb_dest_client.Validated
            validate_destination()
        End Sub

        Private Sub validate_destination()
            Dim error_message As String = System.String.Empty
            With txb_dest_client
                lbl_dest_inactive.Text = System.String.Empty

                ' There must be a client
                If .Text = System.String.Empty Then
                    error_message = RM.GetString("required_value")
                Else

                    ' Find the client. If the ClientId is invalid then reject the operation
                    If dest_client() <= 0 Then
                        error_message = RM.GetString("invalid_client")
                    Else
                        Dim active_status As String = String.Empty

                        ' Read the client information from the database
                        If Not read_client(dest_client, active_status, med_TargetName.Text, old_dest_trust) Then
                            error_message = RM.GetString("invalid_client")
                        Else

                            ' If the client is not active then indicate as such
                            Select Case active_status
                                Case "A", "AR"
                                Case Else
                                    lbl_dest_inactive.Text = "INACTIVE Client"
                            End Select
                        End If
                    End If
                End If
            End With

            ' Update the amount fields
            validate_amount()

            ' Set the error message text
            DxErrorProvider1.SetError(txb_dest_client, error_message)
            Button_Transfer.Enabled = Not HasErrors()
        End Sub

        Private Sub txb_dest_client_EditValueChanging(ByVal sender As System.Object, ByVal e As DevExpress.XtraEditors.Controls.ChangingEventArgs) Handles txb_dest_client.EditValueChanging
            med_TargetName.Text = System.String.Empty
        End Sub

        Private Sub drag_drop_MouseMove(ByVal sender As System.Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles lbl_new_dest_trust.MouseMove, cbo_Reason.MouseMove, lbl_new_dest_trust.MouseMove, lbl_new_source_trust.MouseMove, lbl_old_dest_trust.MouseMove, lbl_old_source_trust.MouseMove, med_SourceName.MouseMove, med_TargetName.MouseMove, txc_amount.MouseMove, txb_dest_client.MouseMove, txb_source_client.MouseMove
            With CType(sender, System.Windows.Forms.Control)
                ' Do nothing if the operation is not valid
                If e.Button = 0 Then Exit Sub
                If e.X >= 0 AndAlso e.X < .Width AndAlso e.Y >= 0 AndAlso e.Y < .Height Then Exit Sub

                Dim txt As String = System.String.Empty

                ' If this is a text box then look for the selected text component of the text field
                If TypeOf sender Is DevExpress.XtraEditors.TextEdit Then
                    With CType(sender, DevExpress.XtraEditors.TextEdit)
                        If .SelectionLength > 0 Then txt = .SelectedText
                    End With
                End If

                ' If the text is missing then try the entire text field
                If txt = System.String.Empty Then txt = .Text.Trim()

                ' If there is no text then do nothing
                If txt = System.String.Empty Then Exit Sub

                ' Start a drag-drop operation when the mouse moves outside the control
                Dim dobj As New System.Windows.Forms.DataObject
                dobj.SetData(DataFormats.Text, True, txt)

                ' Do the operation. It will return when the operation is complete.
                Dim effect As DragDropEffects = DragDropEffects.Copy
                effect = .DoDragDrop(dobj, effect)

                ' If the operation result was move then erase the text.
                ' However, since we don't do the move then nothing more is needed.
            End With
        End Sub

        Private Sub drag_drop_DragEnter(ByVal sender As System.Object, ByVal e As System.Windows.Forms.DragEventArgs) Handles txc_amount.DragEnter, cbo_Reason.DragEnter, txb_dest_client.DragEnter, txb_source_client.DragEnter

            ' If there is a text field then look to determine the accepable processing
            If e.Data.GetDataPresent(DataFormats.Text, True) Then
                e.Effect = e.AllowedEffect And DragDropEffects.Copy
            Else
                e.Effect = DragDropEffects.None
            End If
        End Sub

        Private Sub drag_drop_DragDrop(ByVal sender As System.Object, ByVal e As System.Windows.Forms.DragEventArgs) Handles txc_amount.DragDrop, cbo_Reason.DragDrop, txb_dest_client.DragDrop, txb_source_client.DragDrop

            ' If there is a text field then look to determine the accepable processing
            If Not e.Data.GetDataPresent(DataFormats.Text, True) Then Return

            ' We only support copy. No modifier needs to be tested
            e.Effect = e.AllowedEffect And DragDropEffects.Copy

            ' Paste the text into the control
            With CType(sender, System.Windows.Forms.Control)
                .Text = Convert.ToString(e.Data.GetData(DataFormats.Text, True))

                If sender Is txb_source_client Then
                    validate_source()
                ElseIf sender Is txb_dest_client Then
                    validate_destination()
                ElseIf sender Is txc_amount Then
                    txc_amount_Validated(txc_amount, New EventArgs)
                End If
            End With
        End Sub

        Private Sub drag_drop_QueryContinueDrag(ByVal sender As System.Object, ByVal e As System.Windows.Forms.QueryContinueDragEventArgs) Handles lbl_dest_amount.QueryContinueDrag, cbo_Reason.QueryContinueDrag, lbl_new_dest_trust.QueryContinueDrag, lbl_new_dest_trust.QueryContinueDrag, lbl_new_source_trust.QueryContinueDrag, lbl_old_dest_trust.QueryContinueDrag, lbl_old_source_trust.QueryContinueDrag, med_SourceName.QueryContinueDrag, med_TargetName.QueryContinueDrag, txc_amount.QueryContinueDrag, txb_dest_client.QueryContinueDrag, txb_source_client.QueryContinueDrag

            ' If the escape key is pressed then cancel the operation
            If e.EscapePressed Then e.Action = DragAction.Cancel
        End Sub

        Private Sub Button_Cancel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button_Cancel.Click
            Close()
        End Sub

        Private Function read_client(ByVal client As System.Int32, ByRef active_status As String, ByRef client_name As String, ByRef trust_balance As Decimal) As Boolean
            Dim cn As SqlClient.SqlConnection = New SqlConnection(LibraryGlobals.SqlInfo.ConnectionString)
            Dim rd As SqlClient.SqlDataReader = Nothing
            Dim answer As Boolean = False

            Dim result_client As System.Int32 = -1
            Dim result_trust As Decimal = 0D
            Dim result_active_status As String = System.String.Empty
            Dim result_address1 As String = System.String.Empty
            Dim result_address2 As String = System.String.Empty
            Dim result_address3 As String = System.String.Empty
            Dim result_name As String = System.String.Empty

            Try
                cn.Open()
                Using cmd As SqlClient.SqlCommand = New SqlCommand
                    With cmd
                        .CommandText = "SELECT v.client, c.held_in_trust, v.active_status, v.addr1 as address1, v.addr2 as address2, v.addr3 as address3, v.name as name FROM clients c INNER JOIN view_client_address v on c.client = v.client WHERE c.client = @client"
                        .Connection = cn
                        .Parameters.Add("@client", SqlDbType.Int).Value = client
                        rd = .ExecuteReader(CommandBehavior.CloseConnection Or CommandBehavior.SingleResult Or CommandBehavior.SingleRow)
                    End With
                End Using

                If rd IsNot Nothing AndAlso rd.Read Then
                    If Not rd.IsDBNull(1) Then result_trust = rd.GetDecimal(1)
                    If Not rd.IsDBNull(2) Then result_active_status = rd.GetString(2).ToUpper
                    If Not rd.IsDBNull(3) Then result_address1 = rd.GetString(3)
                    If Not rd.IsDBNull(4) Then result_address2 = rd.GetString(4)
                    If Not rd.IsDBNull(5) Then result_address3 = rd.GetString(5)
                    If Not rd.IsDBNull(6) Then result_name = rd.GetString(6)

                    If Not rd.IsDBNull(0) Then result_client = rd.GetInt32(0)
                    If result_client >= 0 Then

                        ' If successful then return the input fields
                        trust_balance = result_trust
                        active_status = result_active_status

                        ' Append the fields to form the name
                        Dim result_string As New System.Text.StringBuilder
                        For Each field As String In New String() {result_name, result_address1, result_address2, result_address3}
                            If field <> System.String.Empty Then
                                result_string.AppendFormat("{0}{1}", Environment.NewLine, field)
                            End If
                        Next

                        ' Remove the leading cr/lf and return the name string
                        If result_string.Length > 0 Then result_string.Remove(0, 2)
                        client_name = result_string.ToString()
                        answer = True
                    End If
                End If

            Catch ex As Exception
                Using gdr As New Repository.GetDataResult() : gdr.HandleException(ex) : DebtPlus.UI.Common.ErrorHandling.HandleErrors(gdr, "Error reading client information") : End Using

            Finally
                If rd IsNot Nothing Then rd.Dispose()
                If cn IsNot Nothing Then cn.Dispose()
            End Try

            ' Return the resulting status
            Return answer
        End Function

        Private Sub Button_Transfer_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button_Transfer.Click

            ' Do the transfer
            Dim cn As SqlClient.SqlConnection = New SqlConnection(LibraryGlobals.SqlInfo.ConnectionString)

            ' Issue the request to the database to make the entry.
            Try
                cn.Open()
                Using cmd As SqlClient.SqlCommand = New SqlCommand
                    With cmd
                        .CommandText = "xpr_transfer_cl_cl"
                        .CommandType = CommandType.StoredProcedure
                        .Connection = cn

                        SqlClient.SqlCommandBuilder.DeriveParameters(cmd)

                        .Parameters(1).Value = txb_source_client.EditValue
                        .Parameters(2).Value = txb_dest_client.EditValue
                        .Parameters(3).Value = txc_amount.EditValue
                        .Parameters(4).Value = cbo_Reason.Text.Trim()
                        .ExecuteNonQuery()
                    End With
                End Using

                ' Success. Reset the form
                clear_form()

                ' Go to the source client for the next field
                txb_source_client.Focus()

            Catch ex As Exception
                Using gdr As New DebtPlus.Repository.GetDataResult()
                    gdr.HandleException(ex)
                    DebtPlus.UI.Common.ErrorHandling.HandleErrors(gdr)
                End Using

            Finally
                If cn IsNot Nothing Then cn.Dispose()
            End Try
        End Sub

        Private Sub txc_amount_EditValueChanging(ByVal sender As Object, ByVal e As DevExpress.XtraEditors.Controls.ChangingEventArgs) Handles txc_amount.EditValueChanging
            dest_amount = src_amount
            new_source_trust = old_source_trust - dest_amount
            new_dest_trust = old_dest_trust + dest_amount
        End Sub
    End Class
End Namespace
