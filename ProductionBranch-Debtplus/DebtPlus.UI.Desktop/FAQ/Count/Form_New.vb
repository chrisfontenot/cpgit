#Region "Copyright 2000-2012 DebtPlus, L.L.C."
'{*******************************************************************}
'{                                                                   }
'{       DebtPlus Debt Management System                             }
'{                                                                   }
'{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
'{       ALL RIGHTS RESERVED                                         }
'{                                                                   }
'{   The entire contents of this file is protected by U.S. and       }
'{   International Copyright Laws. Unauthorized reproduction,        }
'{   reverse-engineering, and distribution of all or any portion of  }
'{   the code contained in this file is strictly prohibited and may  }
'{   result in severe civil and criminal penalties and will be       }
'{   prosecuted to the maximum extent possible under the law.        }
'{                                                                   }
'{   RESTRICTIONS                                                    }
'{                                                                   }
'{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
'{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
'{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
'{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
'{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
'{                                                                   }
'{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
'{   ADDITIONAL RESTRICTIONS.                                        }
'{                                                                   }
'{*******************************************************************}
#End Region

Option Compare Binary
Option Explicit On 
Option Strict On

Imports DebtPlus.Utils
Imports System.Data.SqlClient

Namespace FAQ.Count
    Public Class Form_New
        Inherits DebtPlus.Data.Forms.DebtPlusForm

        Public Sub New()
            MyBase.New()
            InitializeComponent()
            AddHandler Me.Load, AddressOf Form_New_Load
        End Sub

#Region " Windows Form Designer generated code "

        'Form overrides dispose to clean up the component list.
        Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
            If disposing Then
                If components IsNot Nothing Then
                    components.Dispose()
                End If
            End If
            MyBase.Dispose(disposing)
        End Sub

        'Required by the Windows Form Designer
        Private components As System.ComponentModel.IContainer

        'NOTE: The following procedure is required by the Windows Form Designer
        'It can be modified using the Windows Form Designer.  
        'Do not modify it using the code editor.
        Friend WithEvents LabelControl1 As DevExpress.XtraEditors.LabelControl
        Friend WithEvents LabelControl2 As DevExpress.XtraEditors.LabelControl
        Friend WithEvents LabelControl3 As DevExpress.XtraEditors.LabelControl
        Friend WithEvents ComboBoxEdit1 As DevExpress.XtraEditors.ComboBoxEdit
        Friend WithEvents TextEdit1 As DevExpress.XtraEditors.TextEdit
        Friend WithEvents Button_OK As DevExpress.XtraEditors.SimpleButton
        Friend WithEvents Button_Cancel As DevExpress.XtraEditors.SimpleButton
        <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
            Dim resources As System.Resources.ResourceManager = New System.Resources.ResourceManager(GetType(Form_New))
            Me.LabelControl1 = New DevExpress.XtraEditors.LabelControl
            Me.LabelControl2 = New DevExpress.XtraEditors.LabelControl
            Me.LabelControl3 = New DevExpress.XtraEditors.LabelControl
            Me.Button_OK = New DevExpress.XtraEditors.SimpleButton
            Me.Button_Cancel = New DevExpress.XtraEditors.SimpleButton
            Me.ComboBoxEdit1 = New DevExpress.XtraEditors.ComboBoxEdit
            Me.TextEdit1 = New DevExpress.XtraEditors.TextEdit

            CType(Me.ComboBoxEdit1.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.TextEdit1.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
            Me.SuspendLayout()

            '
            'LabelControl1
            '
            Me.LabelControl1.Location = New System.Drawing.Point(8, 8)
            Me.LabelControl1.Name = "LabelControl1"
            Me.LabelControl1.Size = New System.Drawing.Size(416, 24)

            Me.LabelControl1.TabIndex = 0
            Me.LabelControl1.Text = "Enter the text of the question that you wish to add. You must choose an appropria" & _
            "te category for the question."
            Me.LabelControl1.ToolTipController = Me.ToolTipController1
            '
            'LabelControl2
            '
            Me.LabelControl2.Location = New System.Drawing.Point(8, 51)
            Me.LabelControl2.Name = "LabelControl2"

            Me.LabelControl2.TabIndex = 1
            Me.LabelControl2.Text = "Question:"
            Me.LabelControl2.ToolTipController = Me.ToolTipController1
            '
            'LabelControl3
            '
            Me.LabelControl3.Location = New System.Drawing.Point(8, 75)
            Me.LabelControl3.Name = "LabelControl3"

            Me.LabelControl3.TabIndex = 3
            Me.LabelControl3.Text = "Category:"
            Me.LabelControl3.ToolTipController = Me.ToolTipController1
            '
            'Button_OK
            '
            Me.Button_OK.DialogResult = System.Windows.Forms.DialogResult.OK
            Me.Button_OK.Location = New System.Drawing.Point(128, 104)
            Me.Button_OK.Name = "Button_OK"

            Me.Button_OK.TabIndex = 5
            Me.Button_OK.Text = "&OK"
            Me.Button_OK.ToolTipController = Me.ToolTipController1
            '
            'Button_Cancel
            '
            Me.Button_Cancel.DialogResult = System.Windows.Forms.DialogResult.Cancel
            Me.Button_Cancel.Location = New System.Drawing.Point(232, 104)
            Me.Button_Cancel.Name = "Button_Cancel"

            Me.Button_Cancel.TabIndex = 6
            Me.Button_Cancel.Text = "&Cancel"
            Me.Button_Cancel.ToolTipController = Me.ToolTipController1
            '
            'ComboBoxEdit1
            '
            Me.ComboBoxEdit1.Location = New System.Drawing.Point(88, 72)
            Me.ComboBoxEdit1.Name = "ComboBoxEdit1"
            '
            'ComboBoxEdit1.Properties
            '
            Me.ComboBoxEdit1.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
            Me.ComboBoxEdit1.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor
            Me.ComboBoxEdit1.Size = New System.Drawing.Size(328, 20)

            Me.ComboBoxEdit1.TabIndex = 4
            Me.ComboBoxEdit1.ToolTipController = Me.ToolTipController1
            '
            'TextEdit1
            '
            Me.TextEdit1.Location = New System.Drawing.Point(88, 48)
            Me.TextEdit1.Name = "TextEdit1"
            Me.TextEdit1.Size = New System.Drawing.Size(328, 20)

            Me.TextEdit1.TabIndex = 2
            Me.TextEdit1.ToolTipController = Me.ToolTipController1
            Me.TextEdit1.Properties.MaxLength = 50
            '
            'Form_New
            '
            Me.AcceptButton = Me.Button_OK
            Me.AutoScaleBaseSize = New System.Drawing.Size(5, 14)
            Me.CancelButton = Me.Button_Cancel
            Me.ClientSize = New System.Drawing.Size(432, 142)
            Me.Controls.Add(Me.TextEdit1)
            Me.Controls.Add(Me.ComboBoxEdit1)
            Me.Controls.Add(Me.Button_Cancel)
            Me.Controls.Add(Me.Button_OK)
            Me.Controls.Add(Me.LabelControl3)
            Me.Controls.Add(Me.LabelControl2)
            Me.Controls.Add(Me.LabelControl1)
            Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
            Me.Name = "Form_New"
            Me.ToolTipController1.SetSuperTip(Me, Nothing)
            Me.Text = "Add New Frequently Asked Question"

            CType(Me.ComboBoxEdit1.Properties, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.TextEdit1.Properties, System.ComponentModel.ISupportInitialize).EndInit()
            Me.ResumeLayout(False)

        End Sub

#End Region

        Private Sub Form_New_Load(ByVal sender As System.Object, ByVal e As System.EventArgs)

            ' Load the list of types
            Dim cn As SqlClient.SqlConnection = New SqlConnection(DebtPlus.LINQ.SQLInfoClass.getDefault().ConnectionString)
            Dim rd As SqlClient.SqlDataReader = Nothing
            Try
                cn.Open()
                Using cmd As SqlClient.SqlCommand = New SqlCommand
                    With cmd
                        .Connection = cn
                        .CommandText = "lst_faq_types"
                        .CommandType = CommandType.StoredProcedure
                        rd = .ExecuteReader(CommandBehavior.CloseConnection Or CommandBehavior.SingleResult)
                    End With
                End Using

                With ComboBoxEdit1.Properties
                    .Items.Clear()
                    Do While rd.Read
                        Dim description As String = System.String.Empty
                        Dim item_value As System.Int32 = 0
                        Dim default_item As Boolean = False
                        For idx As System.Int32 = 0 To rd.FieldCount - 1
                            If Not rd.IsDBNull(idx) Then
                                Select Case rd.GetName(idx).ToLower()
                                    Case "description"
                                        description = rd.GetString(idx)
                                    Case "item_key"
                                        item_value = Convert.ToInt32(rd.GetValue(idx))
                                    Case "default"
                                        default_item = Convert.ToInt32(rd.GetValue(idx)) <> 0
                                End Select
                            End If
                        Next

                        Dim NewItem As System.Int32 = .Items.Add(New DebtPlus.Data.Controls.ComboboxItem(description, item_value))
                        If default_item Then ComboBoxEdit1.SelectedIndex = NewItem
                    Loop
                End With

            Catch ex As SqlClient.SqlException
                DebtPlus.UI.Common.ErrorHandling.HandleErrors(ex, "Error Reading FAQ types")

            Finally
                If rd IsNot Nothing AndAlso Not rd.IsClosed Then rd.Close()
                If cn IsNot Nothing Then
                    If cn.State <> ConnectionState.Closed Then cn.Close()
                    cn.Close()
                End If
            End Try

            ' Clear the question field
            TextEdit1.Text = System.String.Empty

            ' Add the handlers for the controls
            AddHandler ComboBoxEdit1.SelectedIndexChanged, AddressOf Form_Changed
            AddHandler TextEdit1.TextChanged, AddressOf Form_Changed

            ' Enable the OK button
            Form_Changed(Me, New System.EventArgs)
        End Sub

        Private Sub Form_Changed(ByVal sender As Object, ByVal e As System.EventArgs)
            Button_OK.Enabled = TextEdit1.Text.Trim() <> System.String.Empty AndAlso ComboBoxEdit1.SelectedIndex >= 0
        End Sub
    End Class
End Namespace
