#Region "Copyright 2000-2012 DebtPlus, L.L.C."
'{*******************************************************************}
'{                                                                   }
'{       DebtPlus Debt Management System                             }
'{                                                                   }
'{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
'{       ALL RIGHTS RESERVED                                         }
'{                                                                   }
'{   The entire contents of this file is protected by U.S. and       }
'{   International Copyright Laws. Unauthorized reproduction,        }
'{   reverse-engineering, and distribution of all or any portion of  }
'{   the code contained in this file is strictly prohibited and may  }
'{   result in severe civil and criminal penalties and will be       }
'{   prosecuted to the maximum extent possible under the law.        }
'{                                                                   }
'{   RESTRICTIONS                                                    }
'{                                                                   }
'{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
'{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
'{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
'{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
'{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
'{                                                                   }
'{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
'{   ADDITIONAL RESTRICTIONS.                                        }
'{                                                                   }
'{*******************************************************************}
#End Region

Option Explicit On
Option Strict On
Option Compare Binary

Imports DebtPlus.Utils
Imports System.Data.SqlClient
Imports System.Windows.Forms

Namespace FAQ.Count
    Friend Class Form_Count
        Inherits DebtPlus.Data.Forms.DebtPlusForm

        Private ap As ArgParser = Nothing
        Public Sub New(ByVal ap As ArgParser)
            MyClass.New()
            Me.ap = ap
            AddHandler Me.Load, AddressOf Form_Count_Load
        End Sub

#Region " Windows Form Designer generated code "

        Public Sub New()
            MyBase.New()

            'This call is required by the Windows Form Designer.
            InitializeComponent()

            'Add any initialization after the InitializeComponent() call
            LabelControl1.Text = "This function will help track statistics for frequently asked questions. Not all " & _
            "questions that you may encounter are ""frequent"", however, for our annual report," & _
            " we need to capture a realistic list of questions that are commonly asked when y" & _
            "ou talk to the client." + Environment.NewLine + Environment.NewLine + "To indicate a question was asked, you need only check th" & _
            "e box next to the question and then press the submit button. This form will not " & _
            "go away until you close it, but it will record the question in the database."
        End Sub

        'Form overrides dispose to clean up the component list.
        Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
            If disposing Then
                If components IsNot Nothing Then
                    components.Dispose()
                End If
            End If
            MyBase.Dispose(disposing)
        End Sub

        'Required by the Windows Form Designer
        Private components As System.ComponentModel.IContainer

        'NOTE: The following procedure is required by the Windows Form Designer
        'It can be modified using the Windows Form Designer.  
        'Do not modify it using the code editor.
        Friend WithEvents LabelControl1 As DevExpress.XtraEditors.LabelControl
        Friend WithEvents CheckedListBoxControl1 As DevExpress.XtraEditors.CheckedListBoxControl
        Friend WithEvents Button_Submit As DevExpress.XtraEditors.SimpleButton
        Friend WithEvents Button_New As DevExpress.XtraEditors.SimpleButton
        <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
            Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(Form_Count))
            Me.LabelControl1 = New DevExpress.XtraEditors.LabelControl
            Me.CheckedListBoxControl1 = New DevExpress.XtraEditors.CheckedListBoxControl
            Me.Button_Submit = New DevExpress.XtraEditors.SimpleButton
            Me.Button_New = New DevExpress.XtraEditors.SimpleButton
            CType(Me.DxErrorProvider1, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.CheckedListBoxControl1, System.ComponentModel.ISupportInitialize).BeginInit()
            Me.SuspendLayout()
            '
            'LabelControl1
            '
            Me.LabelControl1.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                        Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
            Me.LabelControl1.Appearance.Options.UseTextOptions = True
            Me.LabelControl1.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
            Me.LabelControl1.Appearance.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Top
            Me.LabelControl1.Appearance.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap
            Me.LabelControl1.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None
            Me.LabelControl1.Location = New System.Drawing.Point(8, 8)
            Me.LabelControl1.Name = "LabelControl1"
            Me.LabelControl1.Size = New System.Drawing.Size(388, 107)
            Me.LabelControl1.TabIndex = 0
            Me.LabelControl1.Text = resources.GetString("LabelControl1.Text")
            Me.LabelControl1.ToolTipController = Me.ToolTipController1
            Me.LabelControl1.UseMnemonic = False
            '
            'CheckedListBoxControl1
            '
            Me.CheckedListBoxControl1.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                        Or System.Windows.Forms.AnchorStyles.Left) _
                        Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
            Me.CheckedListBoxControl1.CheckOnClick = True
            Me.CheckedListBoxControl1.Location = New System.Drawing.Point(8, 121)
            Me.CheckedListBoxControl1.Name = "CheckedListBoxControl1"
            Me.CheckedListBoxControl1.Size = New System.Drawing.Size(312, 215)
            Me.CheckedListBoxControl1.SortOrder = System.Windows.Forms.SortOrder.Ascending
            Me.CheckedListBoxControl1.TabIndex = 1
            Me.CheckedListBoxControl1.ToolTipController = Me.ToolTipController1
            '
            'Button_Submit
            '
            Me.Button_Submit.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
            Me.Button_Submit.Location = New System.Drawing.Point(326, 121)
            Me.Button_Submit.Name = "Button_Submit"
            Me.Button_Submit.Size = New System.Drawing.Size(75, 23)
            Me.Button_Submit.TabIndex = 2
            Me.Button_Submit.Text = "&Submit"
            Me.Button_Submit.ToolTip = "Click here to submit these question counts to the database"
            Me.Button_Submit.ToolTipController = Me.ToolTipController1
            '
            'Button_New
            '
            Me.Button_New.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
            Me.Button_New.Location = New System.Drawing.Point(326, 313)
            Me.Button_New.Name = "Button_New"
            Me.Button_New.Size = New System.Drawing.Size(75, 23)
            Me.Button_New.TabIndex = 3
            Me.Button_New.Text = "&New..."
            Me.Button_New.ToolTip = "Click here to create a new question for the FAQ system"
            Me.Button_New.ToolTipController = Me.ToolTipController1
            '
            'Form_Count
            '
            Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
            Me.ClientSize = New System.Drawing.Size(408, 350)
            Me.Controls.Add(Me.Button_New)
            Me.Controls.Add(Me.Button_Submit)
            Me.Controls.Add(Me.CheckedListBoxControl1)
            Me.Controls.Add(Me.LabelControl1)
            Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
            Me.Name = "Form_Count"
            Me.Text = "Frequently Asked Questions"
            CType(Me.DxErrorProvider1, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.CheckedListBoxControl1, System.ComponentModel.ISupportInitialize).EndInit()
            Me.ResumeLayout(False)

        End Sub

#End Region

        Private Sub ReloadQuestionList()
            ' Load the list of questions into the control
            Dim cn As SqlClient.SqlConnection = New SqlConnection(DebtPlus.LINQ.SQLInfoClass.getDefault().ConnectionString)
            Dim rd As SqlClient.SqlDataReader = Nothing

            Try

                ' Issue the request to read the database table
                cn.Open()
                Using cmd As SqlClient.SqlCommand = New SqlCommand
                    With cmd
                        .Connection = cn
                        .CommandType = CommandType.StoredProcedure
                        .CommandText = "lst_faq_questions"
                        rd = .ExecuteReader(CommandBehavior.CloseConnection Or CommandBehavior.SingleResult)
                    End With
                End Using

                ' Define the list of questions into the system
                With CheckedListBoxControl1
                    .Items.Clear()
                    Do While rd.Read
                        Dim description As String = String.Empty
                        Dim value As System.Int32
                        For itm As System.Int32 = 0 To rd.FieldCount - 1
                            If Not rd.IsDBNull(itm) Then
                                Select Case rd.GetName(itm).ToLower()
                                    Case "description"
                                        description = Convert.ToString(rd.GetValue(itm)).Trim()
                                    Case "item_key"
                                        value = Convert.ToInt32(rd.GetValue(itm))
                                End Select
                            End If
                        Next

                        ' Add the item. We need to wrap it in our own description/value class.
                        Dim ui_item As New DebtPlus.Data.Controls.ComboboxItem(description, value)
                        .Items.Add(New DevExpress.XtraEditors.Controls.CheckedListBoxItem(ui_item, CheckState.Unchecked, True))
                    Loop

                End With

            Catch ex As SqlClient.SqlException
                ' This is the expected error if it occurred
                DebtPlus.UI.Common.ErrorHandling.HandleErrors(ex, "Error Loading List of questions")

            Finally
                ' Close the database connection when completed
                If rd IsNot Nothing Then rd.Dispose()
                If cn IsNot Nothing Then cn.Dispose()
            End Try
        End Sub

        Private Sub Form_Count_Load(ByVal sender As Object, ByVal e As System.EventArgs)

            ' Load and save the form placement
            LoadPlacement("FAQ.Count.Main")

            ' Reload the list of questions
            ReloadQuestionList()

            ' Add the handler to process the checked events
            EnableOK()
            AddHandler CheckedListBoxControl1.ItemCheck, AddressOf CheckedListBoxControl1_ItemCheck
            AddHandler Button_Submit.Click, AddressOf Button_Submit_Click
            AddHandler Button_New.Click, AddressOf Button_New_Click
        End Sub

        Private Sub EnableOK()

            ' Find if an item is checked. If so, enable the update to be performed.
            With CheckedListBoxControl1
                For idx As System.Int32 = 0 To .Items.Count - 1
                    If .Items(idx).Enabled AndAlso .Items(idx).CheckState = CheckState.Checked Then
                        Button_Submit.Enabled = True
                        Return
                    End If
                Next
            End With

            ' The entire list is unchecked. Disable the button
            Button_Submit.Enabled = False
        End Sub

        Private Sub CheckedListBoxControl1_ItemCheck(ByVal sender As Object, ByVal e As DevExpress.XtraEditors.Controls.ItemCheckEventArgs)
            EnableOK()
        End Sub

        Private Sub Button_Submit_Click(ByVal sender As Object, ByVal e As System.EventArgs)
            Dim sb As New System.Text.StringBuilder

            With CheckedListBoxControl1
                For idx As System.Int32 = 0 To .Items.Count - 1
                    If .Items(idx).Enabled AndAlso .Items(idx).CheckState = CheckState.Checked Then
                        Dim ID As System.Int32 = Convert.ToInt32(CType(.Items(idx).Value, DebtPlus.Data.Controls.ComboboxItem).value)
                        sb.AppendFormat(",{0}", ID)
                    End If
                Next idx
            End With

            ' If there is an checked item then open the database for update
            If sb.Length > 0 Then
                sb.Remove(0, 1)
                Dim cn As SqlClient.SqlConnection = New SqlConnection(DebtPlus.LINQ.SQLInfoClass.getDefault().ConnectionString)
                Try

                    ' Perform the update to the database
                    cn.Open()
                    Using cmd As SqlClient.SqlCommand = New SqlCommand
                        With cmd
                            .Connection = cn
                            .CommandText = String.Format("UPDATE faq_questions SET count_mtd = count_mtd + 1 WHERE faq_question in ({0})", sb.ToString())
                            .ExecuteNonQuery()
                        End With
                    End Using

                    ' Remove the checked status from the event
                    With CheckedListBoxControl1
                        For idx As System.Int32 = 0 To .Items.Count - 1
                            .Items(idx).CheckState = CheckState.Unchecked
                        Next idx
                    End With

                Catch ex As SqlClient.SqlException
                    ' This is the expected error if it occurred
                    DebtPlus.UI.Common.ErrorHandling.HandleErrors(ex, "Error Updating Count")

                Finally
                    ' Close the database connection when completed
                    If cn IsNot Nothing Then cn.Dispose()
                End Try
            End If

            ' Enable/Disable the OK button
            EnableOK()
        End Sub

        Private Sub Button_New_Click(ByVal sender As Object, ByVal e As System.EventArgs)
            Dim cn As SqlClient.SqlConnection = New SqlConnection(DebtPlus.LINQ.SQLInfoClass.getDefault().ConnectionString)
            Dim objKey As Object = Nothing

            Dim result As System.Windows.Forms.DialogResult
            Dim description As String
            Dim category As System.Int32

            Using frm As New Form_New
                With frm
                    result = .ShowDialog()
                    If result = DialogResult.OK Then
                        description = .TextEdit1.Text.Trim()
                        category = Convert.ToInt32(CType(.ComboBoxEdit1.SelectedItem, DebtPlus.Data.Controls.ComboboxItem).value)

                        Try
                            ' Open the database and add the row to the table
                            cn.Open()
                            Using cmd As SqlClient.SqlCommand = New SqlCommand
                                With cmd
                                    .Connection = cn
                                    .CommandText = "SET NOCOUNT ON;INSERT INTO faq_questions(faq_type,description) values (@faq_type,@description);SELECT scope_identity() as faq_question;SET NOCOUNT OFF;"
                                    .Parameters.Add("@faq_type", SqlDbType.Int).Value = category
                                    .Parameters.Add("@description", SqlDbType.VarChar, 80).Value = description
                                    objKey = .ExecuteScalar
                                End With
                            End Using

                        Catch ex As SqlClient.SqlException
                            ' Generate the error message if needed
                            DebtPlus.UI.Common.ErrorHandling.HandleErrors(ex, "Error Adding FAQ question")

                        Finally
                            ' Close the conneciton
                            If cn IsNot Nothing Then cn.Dispose()
                        End Try
                    End If
                End With
            End Using

            ' Reload the list of questions once the list has been updated
            If objKey IsNot Nothing Then ReloadQuestionList()
        End Sub
    End Class
End Namespace
