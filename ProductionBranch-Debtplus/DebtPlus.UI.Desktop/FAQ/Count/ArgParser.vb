Option Compare Binary
Option Explicit On 
Option Strict On

Namespace FAQ.Count
    Friend Class ArgParser
        Inherits DebtPlus.Utils.ArgParserBase

        Public Sub New()
            MyBase.New(New String() {})
        End Sub

        Protected Overrides Sub OnUsage(ByVal errorInfo As String)
            ' deprecated
        End Sub
    End Class
End Namespace