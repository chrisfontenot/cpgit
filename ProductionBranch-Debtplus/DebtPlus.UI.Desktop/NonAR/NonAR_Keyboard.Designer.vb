﻿Namespace NonAR
    <Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
    Partial Class NonAR_Keyboard
        Inherits DevExpress.XtraEditors.XtraUserControl

        'UserControl overrides dispose to clean up the component list.
        <System.Diagnostics.DebuggerNonUserCode()> _
        Protected Overrides Sub Dispose(ByVal disposing As Boolean)
            Try
                If disposing AndAlso components IsNot Nothing Then
                    components.Dispose()
                End If
            Finally
                MyBase.Dispose(disposing)
            End Try
        End Sub

        'Required by the Windows Form Designer
        Private components As System.ComponentModel.IContainer

        'NOTE: The following procedure is required by the Windows Form Designer
        'It can be modified using the Windows Form Designer.  
        'Do not modify it using the code editor.
        <System.Diagnostics.DebuggerStepThrough()> _
        Private Sub InitializeComponent()
            Me.LayoutControl1 = New DevExpress.XtraLayout.LayoutControl
            Me.DateEdit1 = New DevExpress.XtraEditors.DateEdit
            Me.TextEdit1 = New DebtPlus.UI.Desktop.NonAR.Keypad_TextEdit
            Me.LookUpEdit1 = New DebtPlus.UI.Desktop.NonAR.Keypad_LookupEdit
            Me.CalcEdit1 = New DebtPlus.UI.Desktop.NonAR.Keypad_CalcEdit
            Me.LayoutControlGroup1 = New DevExpress.XtraLayout.LayoutControlGroup
            Me.LayoutControlItem2 = New DevExpress.XtraLayout.LayoutControlItem
            Me.LayoutControlItem1 = New DevExpress.XtraLayout.LayoutControlItem
            Me.LayoutControlItem3 = New DevExpress.XtraLayout.LayoutControlItem
            Me.LayoutControlItem4 = New DevExpress.XtraLayout.LayoutControlItem
            CType(Me.LayoutControl1, System.ComponentModel.ISupportInitialize).BeginInit()
            Me.LayoutControl1.SuspendLayout()
            CType(Me.DateEdit1.Properties.VistaTimeProperties, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.DateEdit1.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.TextEdit1.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.LookUpEdit1.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.CalcEdit1.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.LayoutControlGroup1, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.LayoutControlItem2, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.LayoutControlItem1, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.LayoutControlItem3, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.LayoutControlItem4, System.ComponentModel.ISupportInitialize).BeginInit()
            Me.SuspendLayout()
            '
            'LayoutControl1
            '
            Me.LayoutControl1.Controls.Add(Me.DateEdit1)
            Me.LayoutControl1.Controls.Add(Me.TextEdit1)
            Me.LayoutControl1.Controls.Add(Me.LookUpEdit1)
            Me.LayoutControl1.Controls.Add(Me.CalcEdit1)
            Me.LayoutControl1.Dock = System.Windows.Forms.DockStyle.Fill
            Me.LayoutControl1.Location = New System.Drawing.Point(0, 0)
            Me.LayoutControl1.Name = "LayoutControl1"
            Me.LayoutControl1.Root = Me.LayoutControlGroup1
            Me.LayoutControl1.Size = New System.Drawing.Size(339, 127)
            Me.LayoutControl1.TabIndex = 0
            Me.LayoutControl1.Text = "LayoutControl1"
            '
            'DateEdit1
            '
            Me.DateEdit1.EditValue = Nothing
            Me.DateEdit1.Location = New System.Drawing.Point(87, 100)
            Me.DateEdit1.Name = "DateEdit1"
            Me.DateEdit1.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[False]
            Me.DateEdit1.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
            Me.DateEdit1.Properties.NullText = "[Please enter a date here]"
            Me.DateEdit1.Properties.VistaTimeProperties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton})
            Me.DateEdit1.Size = New System.Drawing.Size(246, 20)
            Me.DateEdit1.StyleController = Me.LayoutControl1
            Me.DateEdit1.TabIndex = 7
            Me.DateEdit1.ToolTip = "Enter or select the date for the draft. This is the date of the check, not today'" & _
                "s date."
            '
            'TextEdit1
            '
            Me.TextEdit1.Location = New System.Drawing.Point(87, 69)
            Me.TextEdit1.Name = "TextEdit1"
            Me.TextEdit1.Properties.MaxLength = 50
            Me.TextEdit1.Size = New System.Drawing.Size(246, 20)
            Me.TextEdit1.StyleController = Me.LayoutControl1
            Me.TextEdit1.TabIndex = 6
            Me.TextEdit1.ToolTip = "Enter the id corresponding to the bank draft"
            '
            'LookUpEdit1
            '
            Me.LookUpEdit1.Location = New System.Drawing.Point(87, 7)
            Me.LookUpEdit1.Name = "LookUpEdit1"
            Me.LookUpEdit1.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
            Me.LookUpEdit1.Properties.NullText = "[Please choose an item here]"
            Me.LookUpEdit1.Size = New System.Drawing.Size(246, 20)
            Me.LookUpEdit1.StyleController = Me.LayoutControl1
            Me.LookUpEdit1.TabIndex = 4
            Me.LookUpEdit1.ToolTip = "Choose the apporpriate ledger account for the deposit"
            '
            'CalcEdit1
            '
            Me.CalcEdit1.Location = New System.Drawing.Point(87, 38)
            Me.CalcEdit1.Name = "CalcEdit1"
            Me.CalcEdit1.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[False]
            Me.CalcEdit1.Properties.Appearance.Options.UseTextOptions = True
            Me.CalcEdit1.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
            Me.CalcEdit1.Properties.Appearance.TextOptions.WordWrap = DevExpress.Utils.WordWrap.NoWrap
            Me.CalcEdit1.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
            Me.CalcEdit1.Properties.DisplayFormat.FormatString = "c2"
            Me.CalcEdit1.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
            Me.CalcEdit1.Properties.EditFormat.FormatString = "f2"
            Me.CalcEdit1.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric
            Me.CalcEdit1.Properties.Mask.BeepOnError = True
            Me.CalcEdit1.Properties.Mask.EditMask = "c2"
            Me.CalcEdit1.Size = New System.Drawing.Size(246, 20)
            Me.CalcEdit1.StyleController = Me.LayoutControl1
            Me.CalcEdit1.TabIndex = 5
            Me.CalcEdit1.ToolTip = "Enter the dollar amount of the item"
            '
            'LayoutControlGroup1
            '
            Me.LayoutControlGroup1.CustomizationFormText = "LayoutControlGroup1"
            Me.LayoutControlGroup1.Items.AddRange(New DevExpress.XtraLayout.BaseLayoutItem() {Me.LayoutControlItem2, Me.LayoutControlItem1, Me.LayoutControlItem3, Me.LayoutControlItem4})
            Me.LayoutControlGroup1.Location = New System.Drawing.Point(0, 0)
            Me.LayoutControlGroup1.Name = "LayoutControlGroup1"
            Me.LayoutControlGroup1.Size = New System.Drawing.Size(339, 127)
            Me.LayoutControlGroup1.Spacing = New DevExpress.XtraLayout.Utils.Padding(0, 0, 0, 0)
            Me.LayoutControlGroup1.Text = "LayoutControlGroup1"
            Me.LayoutControlGroup1.TextVisible = False
            '
            'LayoutControlItem2
            '
            Me.LayoutControlItem2.Control = Me.CalcEdit1
            Me.LayoutControlItem2.CustomizationFormText = "Dollar &Amount"
            Me.LayoutControlItem2.Location = New System.Drawing.Point(0, 31)
            Me.LayoutControlItem2.Name = "LayoutControlItem2"
            Me.LayoutControlItem2.Size = New System.Drawing.Size(337, 31)
            Me.LayoutControlItem2.Text = "Dollar &Amount"
            Me.LayoutControlItem2.TextLocation = DevExpress.Utils.Locations.Left
            Me.LayoutControlItem2.TextSize = New System.Drawing.Size(75, 20)
            '
            'LayoutControlItem1
            '
            Me.LayoutControlItem1.Control = Me.LookUpEdit1
            Me.LayoutControlItem1.CustomizationFormText = "&Ledger Account"
            Me.LayoutControlItem1.Location = New System.Drawing.Point(0, 0)
            Me.LayoutControlItem1.Name = "LayoutControlItem1"
            Me.LayoutControlItem1.Size = New System.Drawing.Size(337, 31)
            Me.LayoutControlItem1.Text = "&Ledger Account"
            Me.LayoutControlItem1.TextLocation = DevExpress.Utils.Locations.Left
            Me.LayoutControlItem1.TextSize = New System.Drawing.Size(75, 20)
            '
            'LayoutControlItem3
            '
            Me.LayoutControlItem3.Control = Me.TextEdit1
            Me.LayoutControlItem3.CustomizationFormText = "Check &ID"
            Me.LayoutControlItem3.Location = New System.Drawing.Point(0, 62)
            Me.LayoutControlItem3.Name = "LayoutControlItem3"
            Me.LayoutControlItem3.Size = New System.Drawing.Size(337, 31)
            Me.LayoutControlItem3.Text = "Check &ID"
            Me.LayoutControlItem3.TextLocation = DevExpress.Utils.Locations.Left
            Me.LayoutControlItem3.TextSize = New System.Drawing.Size(75, 20)
            '
            'LayoutControlItem4
            '
            Me.LayoutControlItem4.Control = Me.DateEdit1
            Me.LayoutControlItem4.CustomizationFormText = "Date of draft"
            Me.LayoutControlItem4.Location = New System.Drawing.Point(0, 93)
            Me.LayoutControlItem4.Name = "LayoutControlItem4"
            Me.LayoutControlItem4.Size = New System.Drawing.Size(337, 32)
            Me.LayoutControlItem4.Text = "&Date of draft"
            Me.LayoutControlItem4.TextLocation = DevExpress.Utils.Locations.Left
            Me.LayoutControlItem4.TextSize = New System.Drawing.Size(75, 20)
            '
            'NonAR_Keyboard
            '
            Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
            Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
            Me.Controls.Add(Me.LayoutControl1)
            Me.Name = "NonAR_Keyboard"
            Me.Size = New System.Drawing.Size(339, 127)
            CType(Me.LayoutControl1, System.ComponentModel.ISupportInitialize).EndInit()
            Me.LayoutControl1.ResumeLayout(False)
            CType(Me.DateEdit1.Properties.VistaTimeProperties, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.DateEdit1.Properties, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.TextEdit1.Properties, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.LookUpEdit1.Properties, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.CalcEdit1.Properties, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.LayoutControlGroup1, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.LayoutControlItem2, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.LayoutControlItem1, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.LayoutControlItem3, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.LayoutControlItem4, System.ComponentModel.ISupportInitialize).EndInit()
            Me.ResumeLayout(False)

        End Sub
        Friend WithEvents LayoutControl1 As DevExpress.XtraLayout.LayoutControl
        Friend WithEvents LayoutControlGroup1 As DevExpress.XtraLayout.LayoutControlGroup
        Friend WithEvents TextEdit1 As Keypad_TextEdit
        Friend WithEvents LookUpEdit1 As Keypad_LookupEdit
        Friend WithEvents CalcEdit1 As Keypad_CalcEdit
        Friend WithEvents LayoutControlItem2 As DevExpress.XtraLayout.LayoutControlItem
        Friend WithEvents LayoutControlItem1 As DevExpress.XtraLayout.LayoutControlItem
        Friend WithEvents LayoutControlItem3 As DevExpress.XtraLayout.LayoutControlItem
        Friend WithEvents DateEdit1 As DevExpress.XtraEditors.DateEdit
        Friend WithEvents LayoutControlItem4 As DevExpress.XtraLayout.LayoutControlItem

    End Class
End Namespace
