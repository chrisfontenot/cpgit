#Region "Copyright 2000-2012 DebtPlus, L.L.C."
'{*******************************************************************}
'{                                                                   }
'{       DebtPlus Debt Management System                             }
'{                                                                   }
'{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
'{       ALL RIGHTS RESERVED                                         }
'{                                                                   }
'{   The entire contents of this file is protected by U.S. and       }
'{   International Copyright Laws. Unauthorized reproduction,        }
'{   reverse-engineering, and distribution of all or any portion of  }
'{   the code contained in this file is strictly prohibited and may  }
'{   result in severe civil and criminal penalties and will be       }
'{   prosecuted to the maximum extent possible under the law.        }
'{                                                                   }
'{   RESTRICTIONS                                                    }
'{                                                                   }
'{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
'{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
'{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
'{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
'{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
'{                                                                   }
'{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
'{   ADDITIONAL RESTRICTIONS.                                        }
'{                                                                   }
'{*******************************************************************}

#End Region

Imports System.Windows.Forms
Imports DevExpress.XtraBars
Imports DevExpress.XtraEditors

Namespace NonAR
    Friend Class Form_Modal_NonAR
        Inherits DebtPlus.Data.Forms.DebtPlusForm

        ' This is the item being used for the edit format. It is either a keypad or keyboard interface.
        Protected WithEvents ItemEdit As IItemSubForm
        Protected Friend WithEvents registers_non_ar As DataTable = Nothing
        Protected Friend drv As DataRowView = Nothing

        Public Sub New()
            MyBase.New()
            InitializeComponent()
            AddHandler Me.Load, AddressOf Form_NonAR_Load
        End Sub

        Friend ap As ArgParser = Nothing

        Public Sub New(ByVal ap As ArgParser)
            MyClass.New()
            Me.ap = ap
        End Sub

        Public Sub New(ByVal ap As ArgParser, ByVal drv As DataRowView)
            MyClass.New(ap)
            Me.drv = drv
        End Sub

        ''' <summary>
        ''' Change the keyboard input control as needed
        ''' </summary>
        Protected Sub SetItemEditControl(ByVal Editor As IItemSubForm)

            If ItemEdit IsNot Nothing Then
                If drv IsNot Nothing Then ItemEdit.SaveForm()
                With CType(ItemEdit, XtraUserControl)
                    .Enabled = False
                    .Visible = False
                    .TabStop = False
                    .SendToBack()
                End With
            End If

            ItemEdit = Editor
            With CType(ItemEdit, XtraUserControl)
                .Enabled = True
                .Visible = True
                .TabStop = True
                .BringToFront()
            End With
            If drv IsNot Nothing Then ItemEdit.ReadForm()
        End Sub

        Private Sub Form_NonAR_Load(ByVal sender As Object, ByVal e As EventArgs)

            ' Read the list of transactions for this batch
            registers_non_ar = ds.Tables("registers_non_ar")

            ' Use the keypad if indicated
            If Deposit.Manual.Controls.EnterMoveNextControl() Then
                SetItemEditControl(NonAR_keypad1)
                BarButtonItem2.Checked = True
            Else
                SetItemEditControl(NonAR_keyboard1)
            End If

            ' Bind the new items to a new record
            If drv IsNot Nothing Then
                ItemEdit.ReadForm()
                NonAR_Source1.ReadForm()
                NonAR_BatchTotals1.ReadForm()
            End If

            ' Add the handlers once the initialization is complete
            AddHandler SimpleButton1.Click, AddressOf SimpleButton1_Click
            AddHandler SimpleButton2.Click, AddressOf SimpleButton2_Click
            AddHandler BarButtonItem1.ItemClick, AddressOf BarButtonItem1_ItemClick

            ' These are the change events in the sub-forms
            AddHandler ItemEdit.Changed, AddressOf Form_Changed
            AddHandler NonAR_Source1.Changed, AddressOf Form_Changed
        End Sub

        Protected Overridable Sub SimpleButton1_Click(ByVal sender As Object, ByVal e As EventArgs)

            If SimpleButton1.Enabled Then
                ItemEdit.SaveForm()
                NonAR_Source1.SaveForm()
                DialogResult = DialogResult.OK
            End If
        End Sub

        Protected Overridable Sub SimpleButton2_Click(ByVal sender As Object, ByVal e As EventArgs)
            DialogResult = DialogResult.Cancel
        End Sub

        Protected Overridable Sub BarButtonItem1_ItemClick(ByVal sender As Object, ByVal e As ItemClickEventArgs)
            DialogResult = DialogResult.Cancel
        End Sub

        Protected Overridable Sub Form_Changed(ByVal sender As Object, ByVal e As EventArgs)
            SimpleButton1.Enabled = ItemEdit.SaveValid AndAlso NonAR_Source1.SaveValid
        End Sub
    End Class

    Friend Module Globals
        Friend ds As New DataSet("ds")
        Friend MisingText As String = "A value is required"
    End Module

    Friend Interface ISubForms
        Sub ReadForm()
        Sub SaveForm()
        Event Changed(ByVal sender As Object, ByVal e As EventArgs)
        ReadOnly Property SaveValid() As Boolean
    End Interface

    Friend Interface IItemSubForm
        Inherits ISubForms
        ReadOnly Property LedgerAccount() As String
        ReadOnly Property DollarAmount() As Decimal
        ReadOnly Property CheckId() As String
        ReadOnly Property ItemDate() As Object
    End Interface
End Namespace
