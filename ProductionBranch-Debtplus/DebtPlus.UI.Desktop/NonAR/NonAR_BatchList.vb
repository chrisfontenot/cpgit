#Region "Copyright 2000-2012 DebtPlus, L.L.C."
'{*******************************************************************}
'{                                                                   }
'{       DebtPlus Debt Management System                             }
'{                                                                   }
'{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
'{       ALL RIGHTS RESERVED                                         }
'{                                                                   }
'{   The entire contents of this file is protected by U.S. and       }
'{   International Copyright Laws. Unauthorized reproduction,        }
'{   reverse-engineering, and distribution of all or any portion of  }
'{   the code contained in this file is strictly prohibited and may  }
'{   result in severe civil and criminal penalties and will be       }
'{   prosecuted to the maximum extent possible under the law.        }
'{                                                                   }
'{   RESTRICTIONS                                                    }
'{                                                                   }
'{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
'{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
'{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
'{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
'{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
'{                                                                   }
'{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
'{   ADDITIONAL RESTRICTIONS.                                        }
'{                                                                   }
'{*******************************************************************}

#End Region
Imports DevExpress.XtraEditors
Imports System.Windows.Forms

Namespace NonAR
    Friend Class NonAR_BatchList
        Implements ISubForms

        Public Sub New()
            MyBase.New()
            InitializeComponent()
            AddHandler ListBoxControl1.DrawItem, AddressOf ListBoxControl1_DrawItem
            AddHandler ListBoxControl1.DoubleClick, AddressOf ListBoxControl1_DoubleClick
        End Sub

        Public Event Changed(ByVal Sender As Object, ByVal e As EventArgs) Implements ISubForms.Changed

        ''' <summary>
        ''' Do any pre-processing and load any unbound fields
        ''' </summary>
        Public Sub ReadForm() Implements ISubForms.ReadForm
            With ListBoxControl1
                .DataSource = CType(Me.ParentForm, Form_Modal_NonAR).registers_non_ar.DefaultView
                .DisplayMember = "credit_amt"
                .ValueMember = "non_ar_register"
            End With
        End Sub

        ''' <summary>
        ''' Save any fields to the record before it is closed
        ''' </summary>
        Public Sub SaveForm() Implements ISubForms.SaveForm
        End Sub

        ''' <summary>
        ''' Is the SAVE mode valid now?
        ''' </summary>
        Public ReadOnly Property SaveValid() As Boolean Implements ISubForms.SaveValid
            Get
                Return True
            End Get
        End Property

        ''' <summary>
        ''' Draw the items with a dollar sign
        ''' </summary>
        Private Sub ListBoxControl1_DrawItem(ByVal sender As Object, ByVal e As ListBoxDrawItemEventArgs)
            With CType(sender, ListBoxControl)

                ' Put the background in the object
                e.Appearance.DrawBackground(e.Cache, e.Bounds)

                ' Find the item for the display
                If e.Index >= 0 Then

                    ' From the index, find the row.
                    Dim row As DataRow = CType(.DataSource, DataView).Item(e.Index).Row

                    ' From the row, find the amount to be displayed
                    Dim CreditAmt As Decimal = 0D
                    If row(.DisplayMember) IsNot Nothing AndAlso row(.DisplayMember) IsNot DBNull.Value Then
                        CreditAmt = Convert.ToDecimal(row(.DisplayMember))
                    End If

                    ' Format the display string and draw it
                    Dim txt As String = String.Format("{0:c}", CreditAmt)
                    e.Appearance.DrawString(e.Cache, txt, e.Bounds)
                End If

                ' Indicate that we displayed the row and do not do normal processing
                e.Handled = True
            End With
        End Sub

        ''' <summary>
        ''' DOUBLE-CLICK is "EDIT"
        ''' </summary>
        Private Sub ListBoxControl1_DoubleClick(ByVal sender As Object, ByVal e As EventArgs)
            With CType(sender, ListBoxControl)

                ' Find where the use clicked
                Dim HitIndex As Int32 = .IndexFromPoint(.PointToClient(MousePosition))

                ' If the user clicked on an item then it should be zero or positive. Blank area = negative
                If HitIndex >= 0 Then

                    ' From the index, find the row being clicked.
                    Dim vue As DataView = CType(.DataSource, DataView)
                    Dim new_drv As DataRowView = vue(HitIndex)

                    ' The row can not be the one that we are just creating
                    If Not new_drv.IsNew Then

                        ' Run the edit form as a new modal dialog to edit the indicated row.
                        Dim frm As Form_NonAR = CType(ParentForm, Form_NonAR)
                        With New Form_Modal_NonAR(frm.ap, new_drv)
                            new_drv.BeginEdit()

                            ' If successful, complete the edit. Otherwise cancel it.
                            If .ShowDialog() = DialogResult.OK Then
                                new_drv.EndEdit()
                            Else
                                new_drv.CancelEdit()
                            End If

                            ' Dispose of the modal dialog. The list will be updated by now.
                            .Dispose()
                        End With
                    End If
                End If
            End With
        End Sub
    End Class
End Namespace
