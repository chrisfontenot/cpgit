﻿Namespace NonAR
    <Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
    Partial Class NonAR_BatchList
        Inherits DevExpress.XtraEditors.XtraUserControl

        'UserControl overrides dispose to clean up the component list.
        <System.Diagnostics.DebuggerNonUserCode()> _
        Protected Overrides Sub Dispose(ByVal disposing As Boolean)
            Try
                If disposing AndAlso components IsNot Nothing Then
                    components.Dispose()
                End If
            Finally
                MyBase.Dispose(disposing)
            End Try
        End Sub

        'Required by the Windows Form Designer
        Private components As System.ComponentModel.IContainer

        'NOTE: The following procedure is required by the Windows Form Designer
        'It can be modified using the Windows Form Designer.  
        'Do not modify it using the code editor.
        <System.Diagnostics.DebuggerStepThrough()> _
        Private Sub InitializeComponent()
            Me.ListBoxControl1 = New DevExpress.XtraEditors.ListBoxControl
            CType(Me.ListBoxControl1, System.ComponentModel.ISupportInitialize).BeginInit()
            Me.SuspendLayout()
            '
            'ListBoxControl1
            '
            Me.ListBoxControl1.Appearance.Options.UseTextOptions = True
            Me.ListBoxControl1.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
            Me.ListBoxControl1.Dock = System.Windows.Forms.DockStyle.Fill
            Me.ListBoxControl1.HotTrackSelectMode = DevExpress.XtraEditors.HotTrackSelectMode.SelectItemOnClick
            Me.ListBoxControl1.Location = New System.Drawing.Point(0, 0)
            Me.ListBoxControl1.Name = "ListBoxControl1"
            Me.ListBoxControl1.Size = New System.Drawing.Size(150, 150)
            Me.ListBoxControl1.TabIndex = 0
            Me.ListBoxControl1.TabStop = False
            '
            'NonAR_BatchList
            '
            Me.Appearance.BackColor = System.Drawing.Color.Transparent
            Me.Appearance.Options.UseBackColor = True
            Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
            Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
            Me.Controls.Add(Me.ListBoxControl1)
            Me.Name = "NonAR_BatchList"
            CType(Me.ListBoxControl1, System.ComponentModel.ISupportInitialize).EndInit()
            Me.ResumeLayout(False)

        End Sub
        Friend WithEvents ListBoxControl1 As DevExpress.XtraEditors.ListBoxControl

    End Class
End Namespace
