#Region "Copyright 2000-2012 DebtPlus, L.L.C."
'{*******************************************************************}
'{                                                                   }
'{       DebtPlus Debt Management System                             }
'{                                                                   }
'{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
'{       ALL RIGHTS RESERVED                                         }
'{                                                                   }
'{   The entire contents of this file is protected by U.S. and       }
'{   International Copyright Laws. Unauthorized reproduction,        }
'{   reverse-engineering, and distribution of all or any portion of  }
'{   the code contained in this file is strictly prohibited and may  }
'{   result in severe civil and criminal penalties and will be       }
'{   prosecuted to the maximum extent possible under the law.        }
'{                                                                   }
'{   RESTRICTIONS                                                    }
'{                                                                   }
'{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
'{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
'{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
'{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
'{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
'{                                                                   }
'{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
'{   ADDITIONAL RESTRICTIONS.                                        }
'{                                                                   }
'{*******************************************************************}

#End Region

Imports DevExpress.XtraEditors.Controls
Imports System.Data.SqlClient
Imports DevExpress.Utils
Imports System.Windows.Forms

Namespace NonAR
    Friend Class NonAR_Keyboard
        Implements IItemSubForm

        Public Sub New()
            MyBase.New()
            InitializeComponent()
            AddHandler CalcEdit1.EditValueChanged, AddressOf DateEdit1_EditValueChanged
            AddHandler Me.Load, AddressOf NonAR_Keyboard_Load
            AddHandler Me.TabStopChanged, AddressOf NonAR_Keyboard_TabStopChanged
            AddHandler LookUpEdit1.EditValueChanged, AddressOf DateEdit1_EditValueChanged
            AddHandler TextEdit1.EditValueChanged, AddressOf DateEdit1_EditValueChanged
            AddHandler DateEdit1.EditValueChanged, AddressOf DateEdit1_EditValueChanged
        End Sub

        Public Event Changed(ByVal sender As Object, ByVal e As EventArgs) Implements ISubForms.Changed

        ''' <summary>
        ''' Is the SAVE mode valid now?
        ''' </summary>
        Friend ReadOnly Property SaveValid() As Boolean Implements ISubForms.SaveValid
            Get
                Return TextEdit1.ErrorText = String.Empty AndAlso CalcEdit1.ErrorText = String.Empty AndAlso DateEdit1.ErrorText = String.Empty AndAlso LookUpEdit1.ErrorText = String.Empty
            End Get
        End Property

        ''' <summary>
        ''' Interfaces to the various fields of this record
        ''' </summary>
        Public ReadOnly Property CheckId() As String Implements IItemSubForm.CheckId
            Get
                Return Convert.ToString(TextEdit1.EditValue).Trim()
            End Get
        End Property

        Public ReadOnly Property DollarAmount() As Decimal Implements IItemSubForm.DollarAmount
            Get
                Return Convert.ToDecimal(CalcEdit1.EditValue)
            End Get
        End Property

        Public ReadOnly Property ItemDate() As Object Implements IItemSubForm.ItemDate
            Get
                Return DateEdit1.EditValue
            End Get
        End Property

        Public ReadOnly Property LedgerAccount() As String Implements IItemSubForm.LedgerAccount
            Get
                Return Convert.ToString(LookUpEdit1.EditValue)
            End Get
        End Property

        ''' <summary>
        ''' Do any pre-processing and load any unbound fields
        ''' </summary>
        Public Sub ReadForm() Implements ISubForms.ReadForm
            ' Find the source items from the list of ar sources
            Dim source_view As DataView = CType(LookUpEdit1.Properties.DataSource, DataView)
            Dim drv As DataRowView = CType(Me.ParentForm, Form_Modal_NonAR).drv

            If source_view Is Nothing Then
                Dim tbl As DataTable = ds.Tables("ledger_codes")
                If tbl Is Nothing Then
                    Dim cmd As SqlCommand = New SqlCommand
                    With cmd
                        .Connection = New SqlConnection(DebtPlus.LINQ.SQLInfoClass.getDefault().ConnectionString)
                        .CommandText = "select ledger_code,description FROM ledger_codes ORDER BY description"
                        .CommandType = CommandType.Text
                    End With

                    Dim current_cursor As Cursor = Cursor.Current
                    Try
                        Cursor.Current = Cursors.WaitCursor
                        Dim da As New SqlDataAdapter(cmd)
                        da.Fill(ds, "ledger_codes")
                        tbl = ds.Tables("ledger_codes")

                    Catch ex As SqlException
                        DebtPlus.UI.Common.ErrorHandling.HandleErrors(ex, "Error reading ledger_codes from database")

                    Finally
                        Cursor.Current = current_cursor
                    End Try
                End If

                source_view = New DataView(tbl, String.Empty, "description", DataViewRowState.CurrentRows)
                With LookUpEdit1.Properties
                    .DataSource = source_view
                    .DisplayMember = "description"
                    .ValueMember = "ledger_code"

                    .ShowFooter = False
                    .ShowHeader = False
                    .ShowLines = True
                    .Columns.AddRange(New LookUpColumnInfo() {New LookUpColumnInfo("ledger_code", "ledger_code", 25, FormatType.None, String.Empty, False, HorzAlignment.Far), New LookUpColumnInfo("description", "description", 75, FormatType.None, String.Empty, True, HorzAlignment.Near)})
                End With
            End If

            ' Bind the data members to the items
            With LookUpEdit1
                .DataBindings.Clear()
                .DataBindings.Add("EditValue", drv, "dst_ledger_account")
                AddHandler .EditValueChanging, AddressOf DebtPlus.Data.Validation.LookUpEdit_ActiveTest
                AddHandler .EditValueChanging, AddressOf LookupEdit1_EditValueChanging
            End With

            With CalcEdit1
                .DataBindings.Clear()
                .DataBindings.Add("EditValue", drv, "credit_amt")
                AddHandler .EditValueChanging, AddressOf CalcEdit1_EditValueChanging
            End With

            With TextEdit1
                .DataBindings.Clear()
                .DataBindings.Add("EditValue", drv, "reference")
            End With

            With DateEdit1
                .DataBindings.Clear()
                .DataBindings.Add("EditValue", drv, "item_date")
                AddHandler .EditValueChanging, AddressOf DateEdit1_EditValueChanging
            End With
        End Sub

        ''' <summary>
        ''' Save any fields to the record before it is closed
        ''' </summary>
        Public Sub SaveForm() Implements ISubForms.SaveForm
        End Sub

        Private Sub DateEdit1_EditValueChanged(ByVal sender As Object, ByVal e As EventArgs)
            RaiseEvent Changed(Me, EventArgs.Empty)
        End Sub

        ''' <summary>
        ''' Handle the change in the amount field
        ''' </summary>
        Private Sub CalcEdit1_EditValueChanging(ByVal sender As Object, ByVal e As ChangingEventArgs)
            Dim ErrorText As String = String.Empty
            If e.NewValue Is Nothing OrElse e.NewValue Is DBNull.Value OrElse Convert.ToDecimal(e.NewValue) <= 0D Then
                ErrorText = MisingText
            End If
            CalcEdit1.ErrorText = ErrorText
        End Sub

        ''' <summary>
        ''' Handle the change in the ledger account field
        ''' </summary>
        Private Sub LookupEdit1_EditValueChanging(ByVal sender As Object, ByVal e As ChangingEventArgs)
            Dim ErrorText As String = String.Empty
            If e.NewValue Is Nothing OrElse e.NewValue Is DBNull.Value Then
                ErrorText = MisingText
            End If
            LookUpEdit1.ErrorText = ErrorText
        End Sub

        ''' <summary>
        ''' Handle the change in the date field
        ''' </summary>
        Private Sub DateEdit1_EditValueChanging(ByVal sender As Object, ByVal e As ChangingEventArgs)
            Dim ErrorText As String = String.Empty
            If e.NewValue Is Nothing OrElse e.NewValue Is DBNull.Value Then
                ErrorText = MisingText
            End If
            DateEdit1.ErrorText = ErrorText
        End Sub

        ''' <summary>
        ''' Process the load event for our control
        ''' </summary>
        Private Sub NonAR_Keyboard_Load(ByVal sender As Object, ByVal e As EventArgs)
            LookUpEdit1.TabStop = False
            CalcEdit1.TabStop = False
            TextEdit1.TabStop = False
            DateEdit1.TabStop = False
        End Sub

        ''' <summary>
        ''' Process the change in tab stop for our control.
        ''' </summary>
        Private Sub NonAR_Keyboard_TabStopChanged(ByVal sender As Object, ByVal e As EventArgs)

            ' Enable or disable the tab stop processing as the control is switched
            LookUpEdit1.TabStop = TabStop
            CalcEdit1.TabStop = TabStop
            TextEdit1.TabStop = TabStop
            DateEdit1.TabStop = TabStop
        End Sub
    End Class
End Namespace
