#Region "Copyright 2000-2012 DebtPlus, L.L.C."
'{*******************************************************************}
'{                                                                   }
'{       DebtPlus Debt Management System                             }
'{                                                                   }
'{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
'{       ALL RIGHTS RESERVED                                         }
'{                                                                   }
'{   The entire contents of this file is protected by U.S. and       }
'{   International Copyright Laws. Unauthorized reproduction,        }
'{   reverse-engineering, and distribution of all or any portion of  }
'{   the code contained in this file is strictly prohibited and may  }
'{   result in severe civil and criminal penalties and will be       }
'{   prosecuted to the maximum extent possible under the law.        }
'{                                                                   }
'{   RESTRICTIONS                                                    }
'{                                                                   }
'{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
'{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
'{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
'{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
'{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
'{                                                                   }
'{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
'{   ADDITIONAL RESTRICTIONS.                                        }
'{                                                                   }
'{*******************************************************************}

#End Region
Imports System.ComponentModel
Imports DevExpress.XtraEditors.Controls
Imports DevExpress.XtraEditors
Imports System.Data.SqlClient
Imports DevExpress.Utils
Imports System.Windows.Forms

Namespace NonAR
    Friend Class NonAR_Keypad
        Implements IItemSubForm

        Public Event Changed(ByVal sender As Object, ByVal e As EventArgs) Implements ISubForms.Changed

        Public Sub New()
            MyBase.New()
            InitializeComponent()
            AddHandler TextEdit2.Validating, AddressOf TextEdit2_Validating
            AddHandler TextEdit2.EditValueChanged, AddressOf TextEdit2_EditValueChanged
            AddHandler CalcEdit1.EditValueChanging, AddressOf CalcEdit1_EditValueChanging
            AddHandler LookUpEdit1.EditValueChanging, AddressOf LookupEdit1_EditValueChanging
            AddHandler Me.Load, AddressOf NonAR_Keypad_Load
            AddHandler Me.TabStopChanged, AddressOf NonAR_Keypad_TabStopChanged
            AddHandler CalcEdit1.EditValueChanged, AddressOf TextEdit2_EditValueChanged
            AddHandler LookUpEdit1.EditValueChanged, AddressOf TextEdit2_EditValueChanged
            AddHandler TextEdit1.EditValueChanged, AddressOf TextEdit2_EditValueChanged
        End Sub

        ''' <summary>
        ''' Is the SAVE mode valid now?
        ''' </summary>
        Friend ReadOnly Property SaveValid() As Boolean Implements ISubForms.SaveValid
            Get
                Return TextEdit1.ErrorText = String.Empty AndAlso CalcEdit1.ErrorText = String.Empty AndAlso TextEdit2.ErrorText = String.Empty AndAlso LookUpEdit1.ErrorText = String.Empty
            End Get
        End Property

        Public ReadOnly Property CheckId() As String Implements IItemSubForm.CheckId
            Get
                Return Convert.ToString(TextEdit1.EditValue).Trim()
            End Get
        End Property

        Public ReadOnly Property DollarAmount() As Decimal Implements IItemSubForm.DollarAmount
            Get
                Return Convert.ToDecimal(CalcEdit1.EditValue)
            End Get
        End Property

        Public ReadOnly Property ItemDate() As Object Implements IItemSubForm.ItemDate
            Get
                Dim value As Date
                If Not Date.TryParse(TextEdit2.Text.Trim(), value) Then value = Date.Now
                Return value
            End Get
        End Property

        Public ReadOnly Property LedgerAccount() As String Implements IItemSubForm.LedgerAccount
            Get
                Return Convert.ToString(LookUpEdit1.EditValue)
            End Get
        End Property

        ''' <summary>
        ''' Do any pre-processing and load any unbound fields
        ''' </summary>
        Public Sub ReadForm() Implements ISubForms.ReadForm
            ' Find the source items from the list of ar sources
            Dim source_view As DataView = CType(LookUpEdit1.Properties.DataSource, DataView)
            Dim drv As DataRowView = CType(Me.ParentForm, Form_Modal_NonAR).drv

            If source_view Is Nothing Then
                Dim tbl As DataTable = ds.Tables("ledger_codes")
                If tbl Is Nothing Then
                    Dim cmd As SqlCommand = New SqlCommand
                    With cmd
                        .Connection = New SqlConnection(DebtPlus.LINQ.SQLInfoClass.getDefault().ConnectionString)
                        .CommandText = "select ledger_code,description FROM ledger_codes ORDER BY description"
                        .CommandType = CommandType.Text
                    End With

                    Dim current_cursor As Cursor = Cursor.Current
                    Try
                        Cursor.Current = Cursors.WaitCursor
                        Dim da As New SqlDataAdapter(cmd)
                        da.Fill(ds, "ledger_codes")
                        tbl = ds.Tables("ledger_codes")

                    Catch ex As SqlException
                        DebtPlus.UI.Common.ErrorHandling.HandleErrors(ex, "Error reading ledger_codes from database")

                    Finally
                        Cursor.Current = current_cursor
                    End Try
                End If

                source_view = New DataView(tbl, String.Empty, "description", DataViewRowState.CurrentRows)
                With LookUpEdit1.Properties
                    .DataSource = source_view
                    .DisplayMember = "description"
                    .ValueMember = "ledger_code"

                    .ShowFooter = False
                    .ShowHeader = False
                    .ShowLines = True
                    .Columns.AddRange(New LookUpColumnInfo() {New LookUpColumnInfo("ledger_code", "ledger_code", 25, FormatType.None, String.Empty, False, HorzAlignment.Far), New LookUpColumnInfo("description", "description", 75, FormatType.None, String.Empty, True, HorzAlignment.Near)})
                End With
            End If

            ' Bind the data members to the items
            With LookUpEdit1
                .DataBindings.Clear()
                .DataBindings.Add("EditValue", drv, "dst_ledger_account")
                AddHandler .EditValueChanging, AddressOf DebtPlus.Data.Validation.LookUpEdit_ActiveTest
            End With

            With CalcEdit1
                .DataBindings.Clear()
                .DataBindings.Add("EditValue", drv, "credit_amt")
            End With

            With TextEdit1
                .DataBindings.Clear()
                .DataBindings.Add("EditValue", drv, "reference")
            End With

            With TextEdit2
                .Text = String.Empty
                If drv("item_date") IsNot Nothing AndAlso drv("item_date") IsNot DBNull.Value Then
                    .Text = String.Format("{0:d}", Convert.ToDateTime(drv("item_date")))
                End If
            End With
        End Sub

        ''' <summary>
        ''' Save any fields to the record before it is closed
        ''' </summary>
        Public Sub SaveForm() Implements ISubForms.SaveForm
            Dim drv As DataRowView = CType(Me.ParentForm, Form_Modal_NonAR).drv
            drv("item_date") = ItemDate
        End Sub

        ''' <summary>
        ''' Validate and update the date field when it is changed
        ''' </summary>
        Private Sub TextEdit2_Validating(ByVal sender As Object, ByVal e As CancelEventArgs)
            Dim Value As Date = Now.Date
            Dim DateText As String = Convert.ToString(TextEdit2.Text).Trim()

            ' Try to parse the date as it is.
            If Not Date.TryParse(DateText, Value) Then
                Select Case DateText.Length
                    Case 3
                        DateText = DateText.Substring(0, 1) + "/" + DateText.Substring(1, 2)

                    Case 4
                        DateText = DateText.Substring(0, 2) + "/" + DateText.Substring(2, 2)

                    Case 5
                        DateText = DateText.Substring(0, 1) + "/" + DateText.Substring(1, 2) + "/" + DateText.Substring(3, 2)

                    Case 6
                        DateText = DateText.Substring(0, 2) + "/" + DateText.Substring(2, 2) + "/" + DateText.Substring(4, 2)

                    Case 7
                        DateText = DateText.Substring(0, 1) + "/" + DateText.Substring(1, 2) + "/" + DateText.Substring(3, 4)

                    Case 8
                        DateText = DateText.Substring(0, 2) + "/" + DateText.Substring(2, 2) + "/" + DateText.Substring(4, 4)

                    Case Else
                        TextEdit2.Text = String.Empty
                        Return
                End Select
                Date.TryParse(DateText, Value)
            End If

            ' Update the date format and return its value
            TextEdit2.Text = String.Format("{0:d}", Value)
        End Sub

        ''' <summary>
        ''' Process the change in the date field
        ''' </summary>
        Private Sub TextEdit2_EditValueChanged(ByVal sender As Object, ByVal e As EventArgs)
            RaiseEvent Changed(Me, EventArgs.Empty)
        End Sub

        ''' <summary>
        ''' Process the change in the amount field
        ''' </summary>
        Private Sub CalcEdit1_EditValueChanging(ByVal sender As Object, ByVal e As ChangingEventArgs)
            Dim ErrorText As String = String.Empty
            If e.NewValue Is Nothing OrElse e.NewValue Is DBNull.Value OrElse Convert.ToDecimal(e.NewValue) <= 0D Then
                ErrorText = MisingText
            End If
            CalcEdit1.ErrorText = ErrorText
        End Sub

        ''' <summary>
        ''' Process the change in the ledger account field
        ''' </summary>
        Private Sub LookupEdit1_EditValueChanging(ByVal sender As Object, ByVal e As ChangingEventArgs)
            Dim ErrorText As String = String.Empty
            If e.NewValue Is Nothing OrElse e.NewValue Is DBNull.Value Then
                ErrorText = MisingText
            End If
            LookUpEdit1.ErrorText = ErrorText
        End Sub

        ''' <summary>
        ''' Handle the loading of our control
        ''' </summary>
        Private Sub NonAR_Keypad_Load(ByVal sender As Object, ByVal e As EventArgs)
            LookUpEdit1.TabStop = False
            CalcEdit1.TabStop = False
            TextEdit1.TabStop = False
            TextEdit2.TabStop = False
        End Sub

        ''' <summary>
        ''' Handle the change in the tab stop setting for our control
        ''' </summary>
        Private Sub NonAR_Keypad_TabStopChanged(ByVal sender As Object, ByVal e As EventArgs)

            ' Enable or disable the tab stop processing as the control is switched
            LookUpEdit1.TabStop = TabStop
            CalcEdit1.TabStop = TabStop
            TextEdit1.TabStop = TabStop
            TextEdit2.TabStop = TabStop
        End Sub
    End Class

    ''' <summary>
    ''' Override class to allow for the translation of CR to TAB
    ''' </summary>
    Friend Class Keypad_TextEdit
        Inherits TextEdit

        Private FocusedControl As Control = Nothing

        Private Function FindFocusedControl(ByVal ctl As Control) As Boolean
            Dim answer As Boolean = False
            If ctl.Focused Then
                FocusedControl = ctl
                answer = True
            Else
                For Each SubCtl As Control In ctl.Controls
                    answer = FindFocusedControl(SubCtl)
                    If answer Then Exit For
                Next
            End If
            Return answer
        End Function

        Private Function GenerateTabEvent(ByVal Forward As Boolean) As Boolean
            Dim answer As Boolean = False

            ' Find the top level form. It is the one without a parent.
            Dim ParentForm As Control = Me.Parent
            Do While ParentForm.Parent IsNot Nothing
                ParentForm = ParentForm.Parent
            Loop

            ' Find the control with the focus. It should be us.
            FocusedControl = Nothing
            If FindFocusedControl(ParentForm) Then
                ParentForm.SelectNextControl(FocusedControl, Forward, True, True, True)
                answer = True
            End If
            Return answer
        End Function

        Protected Overrides Sub OnKeyDown(ByVal e As KeyEventArgs)
            If e.KeyData = Keys.Return AndAlso GenerateTabEvent(Not e.Shift) Then
                e.Handled = True
                e.SuppressKeyPress = True
            End If
            MyBase.OnKeyDown(e)
        End Sub
    End Class

    ''' <summary>
    ''' Override class to allow for the translation of CR to TAB
    ''' </summary>
    Friend Class Keypad_CalcEdit
        Inherits CalcEdit

        Private FocusedControl As Control = Nothing

        Private Function FindFocusedControl(ByVal ctl As Control) As Boolean
            Dim answer As Boolean = False
            If ctl.Focused Then
                FocusedControl = ctl
                answer = True
            Else
                For Each SubCtl As Control In ctl.Controls
                    answer = FindFocusedControl(SubCtl)
                    If answer Then Exit For
                Next
            End If
            Return answer
        End Function

        Private Function GenerateTabEvent(ByVal Forward As Boolean) As Boolean
            Dim answer As Boolean = False

            ' Find the top level form. It is the one without a parent.
            Dim ParentForm As Control = Me.Parent
            Do While ParentForm.Parent IsNot Nothing
                ParentForm = ParentForm.Parent
            Loop

            ' Find the control with the focus. It should be us.
            FocusedControl = Nothing
            If FindFocusedControl(ParentForm) Then
                ParentForm.SelectNextControl(FocusedControl, Forward, True, True, True)
                answer = True
            End If
            Return answer
        End Function

        Protected Overrides Sub OnKeyDown(ByVal e As KeyEventArgs)
            If e.KeyData = Keys.Return AndAlso GenerateTabEvent(Not e.Shift) Then
                e.Handled = True
                e.SuppressKeyPress = True
            End If
            MyBase.OnKeyDown(e)
        End Sub
    End Class

    ''' <summary>
    ''' Override class to allow for the translation of CR to TAB
    ''' </summary>
    Friend Class Keypad_LookupEdit
        Inherits LookUpEdit

        Private FocusedControl As Control = Nothing

        Private Function FindFocusedControl(ByVal ctl As Control) As Boolean
            Dim answer As Boolean = False
            If ctl.Focused Then
                FocusedControl = ctl
                answer = True
            Else
                For Each SubCtl As Control In ctl.Controls
                    answer = FindFocusedControl(SubCtl)
                    If answer Then Exit For
                Next
            End If
            Return answer
        End Function

        Private Function GenerateTabEvent(ByVal Forward As Boolean) As Boolean
            Dim answer As Boolean = False

            ' Find the top level form. It is the one without a parent.
            Dim ParentForm As Control = Me.Parent
            Do While ParentForm.Parent IsNot Nothing
                ParentForm = ParentForm.Parent
            Loop

            ' Find the control with the focus. It should be us.
            FocusedControl = Nothing
            If FindFocusedControl(ParentForm) Then
                ParentForm.SelectNextControl(FocusedControl, Forward, True, True, True)
                answer = True
            End If
            Return answer
        End Function

        Protected Overrides Sub OnKeyDown(ByVal e As KeyEventArgs)
            If e.KeyData = Keys.Return AndAlso GenerateTabEvent(Not e.Shift) Then
                e.Handled = True
                e.SuppressKeyPress = True
            End If
            MyBase.OnKeyDown(e)
        End Sub
    End Class
End Namespace
