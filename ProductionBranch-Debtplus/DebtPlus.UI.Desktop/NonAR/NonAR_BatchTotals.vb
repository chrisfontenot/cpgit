#Region "Copyright 2000-2012 DebtPlus, L.L.C."
'{*******************************************************************}
'{                                                                   }
'{       DebtPlus Debt Management System                             }
'{                                                                   }
'{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
'{       ALL RIGHTS RESERVED                                         }
'{                                                                   }
'{   The entire contents of this file is protected by U.S. and       }
'{   International Copyright Laws. Unauthorized reproduction,        }
'{   reverse-engineering, and distribution of all or any portion of  }
'{   the code contained in this file is strictly prohibited and may  }
'{   result in severe civil and criminal penalties and will be       }
'{   prosecuted to the maximum extent possible under the law.        }
'{                                                                   }
'{   RESTRICTIONS                                                    }
'{                                                                   }
'{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
'{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
'{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
'{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
'{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
'{                                                                   }
'{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
'{   ADDITIONAL RESTRICTIONS.                                        }
'{                                                                   }
'{*******************************************************************}
#End Region

Namespace NonAR
    Friend Class NonAR_BatchTotals
        Implements ISubForms

        Public Event Changed(ByVal sender As Object, ByVal e As System.EventArgs) Implements ISubForms.Changed

        ''' <summary>
        ''' Is the SAVE mode valid now?
        ''' </summary>
        Friend ReadOnly Property SaveValid() As Boolean Implements ISubForms.SaveValid
            Get
                Return True
            End Get
        End Property

        ''' <summary>
        ''' Do any pre-processing and load any unbound fields
        ''' </summary>
        Public Sub ReadForm() Implements ISubForms.ReadForm
            Dim Items As System.Int32 = CType(Me.ParentForm, Form_Modal_NonAR).registers_non_ar.Rows.Count
            Dim Total As Object = CType(Me.ParentForm, Form_Modal_NonAR).registers_non_ar.Compute("sum(credit_amt)", String.Empty)
            If Total Is Nothing OrElse Total Is System.DBNull.Value Then Total = 0D

            ' Update the display counts correctly
            edit_item_count.Text = String.Format("{0:n0}", Items)
            edit_item_total.Text = String.Format("{0:c}", Convert.ToDecimal(Total))
        End Sub

        ''' <summary>
        ''' Save any fields to the record before it is closed
        ''' </summary>
        Public Sub SaveForm() Implements ISubForms.SaveForm
        End Sub
    End Class
End Namespace
