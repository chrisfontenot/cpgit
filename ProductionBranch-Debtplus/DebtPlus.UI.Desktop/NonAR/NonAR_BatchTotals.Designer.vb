﻿Namespace NonAR
    <Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
    Partial Class NonAR_BatchTotals
        Inherits DevExpress.XtraEditors.XtraUserControl

        'UserControl overrides dispose to clean up the component list.
        <System.Diagnostics.DebuggerNonUserCode()> _
        Protected Overrides Sub Dispose(ByVal disposing As Boolean)
            Try
                If disposing AndAlso components IsNot Nothing Then
                    components.Dispose()
                End If
            Finally
                MyBase.Dispose(disposing)
            End Try
        End Sub

        'Required by the Windows Form Designer
        Private components As System.ComponentModel.IContainer

        'NOTE: The following procedure is required by the Windows Form Designer
        'It can be modified using the Windows Form Designer.  
        'Do not modify it using the code editor.
        <System.Diagnostics.DebuggerStepThrough()> _
        Private Sub InitializeComponent()
            Me.LayoutControl1 = New DevExpress.XtraLayout.LayoutControl
            Me.edit_item_total = New DevExpress.XtraEditors.TextEdit
            Me.edit_item_count = New DevExpress.XtraEditors.TextEdit
            Me.LayoutControlGroup1 = New DevExpress.XtraLayout.LayoutControlGroup
            Me.LayoutControlItem1 = New DevExpress.XtraLayout.LayoutControlItem
            Me.LayoutControlItem2 = New DevExpress.XtraLayout.LayoutControlItem
            CType(Me.LayoutControl1, System.ComponentModel.ISupportInitialize).BeginInit()
            Me.LayoutControl1.SuspendLayout()
            CType(Me.edit_item_total.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.edit_item_count.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.LayoutControlGroup1, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.LayoutControlItem1, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.LayoutControlItem2, System.ComponentModel.ISupportInitialize).BeginInit()
            Me.SuspendLayout()
            '
            'LayoutControl1
            '
            Me.LayoutControl1.Controls.Add(Me.edit_item_total)
            Me.LayoutControl1.Controls.Add(Me.edit_item_count)
            Me.LayoutControl1.Dock = System.Windows.Forms.DockStyle.Fill
            Me.LayoutControl1.Location = New System.Drawing.Point(0, 0)
            Me.LayoutControl1.Name = "LayoutControl1"
            Me.LayoutControl1.Root = Me.LayoutControlGroup1
            Me.LayoutControl1.Size = New System.Drawing.Size(184, 67)
            Me.LayoutControl1.TabIndex = 0
            Me.LayoutControl1.Text = "LayoutControl1"
            '
            'edit_item_total
            '
            Me.edit_item_total.EditValue = "$0.00"
            Me.edit_item_total.Location = New System.Drawing.Point(66, 38)
            Me.edit_item_total.Name = "edit_item_total"
            Me.edit_item_total.Properties.AllowFocused = False
            Me.edit_item_total.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[False]
            Me.edit_item_total.Properties.Appearance.Options.UseTextOptions = True
            Me.edit_item_total.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
            Me.edit_item_total.Properties.ReadOnly = True
            Me.edit_item_total.Size = New System.Drawing.Size(112, 20)
            Me.edit_item_total.StyleController = Me.LayoutControl1
            Me.edit_item_total.TabIndex = 5
            '
            'edit_item_count
            '
            Me.edit_item_count.EditValue = "0"
            Me.edit_item_count.Location = New System.Drawing.Point(66, 7)
            Me.edit_item_count.Name = "edit_item_count"
            Me.edit_item_count.Properties.AllowFocused = False
            Me.edit_item_count.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[False]
            Me.edit_item_count.Properties.Appearance.Options.UseTextOptions = True
            Me.edit_item_count.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
            Me.edit_item_count.Properties.ReadOnly = True
            Me.edit_item_count.Size = New System.Drawing.Size(112, 20)
            Me.edit_item_count.StyleController = Me.LayoutControl1
            Me.edit_item_count.TabIndex = 4
            '
            'LayoutControlGroup1
            '
            Me.LayoutControlGroup1.CustomizationFormText = "LayoutControlGroup1"
            Me.LayoutControlGroup1.Items.AddRange(New DevExpress.XtraLayout.BaseLayoutItem() {Me.LayoutControlItem1, Me.LayoutControlItem2})
            Me.LayoutControlGroup1.Location = New System.Drawing.Point(0, 0)
            Me.LayoutControlGroup1.Name = "LayoutControlGroup1"
            Me.LayoutControlGroup1.Size = New System.Drawing.Size(184, 67)
            Me.LayoutControlGroup1.Spacing = New DevExpress.XtraLayout.Utils.Padding(0, 0, 0, 0)
            Me.LayoutControlGroup1.Text = "LayoutControlGroup1"
            Me.LayoutControlGroup1.TextVisible = False
            '
            'LayoutControlItem1
            '
            Me.LayoutControlItem1.Control = Me.edit_item_count
            Me.LayoutControlItem1.CustomizationFormText = "Item Count"
            Me.LayoutControlItem1.Location = New System.Drawing.Point(0, 0)
            Me.LayoutControlItem1.Name = "LayoutControlItem1"
            Me.LayoutControlItem1.Size = New System.Drawing.Size(182, 31)
            Me.LayoutControlItem1.Text = "Item Count"
            Me.LayoutControlItem1.TextLocation = DevExpress.Utils.Locations.Left
            Me.LayoutControlItem1.TextSize = New System.Drawing.Size(54, 20)
            '
            'LayoutControlItem2
            '
            Me.LayoutControlItem2.Control = Me.edit_item_total
            Me.LayoutControlItem2.CustomizationFormText = "Item Total"
            Me.LayoutControlItem2.Location = New System.Drawing.Point(0, 31)
            Me.LayoutControlItem2.Name = "LayoutControlItem2"
            Me.LayoutControlItem2.Size = New System.Drawing.Size(182, 34)
            Me.LayoutControlItem2.Text = "Item Total"
            Me.LayoutControlItem2.TextLocation = DevExpress.Utils.Locations.Left
            Me.LayoutControlItem2.TextSize = New System.Drawing.Size(54, 20)
            '
            'NonAR_BatchTotals
            '
            Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
            Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
            Me.Controls.Add(Me.LayoutControl1)
            Me.Name = "NonAR_BatchTotals"
            Me.Size = New System.Drawing.Size(184, 67)
            CType(Me.LayoutControl1, System.ComponentModel.ISupportInitialize).EndInit()
            Me.LayoutControl1.ResumeLayout(False)
            CType(Me.edit_item_total.Properties, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.edit_item_count.Properties, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.LayoutControlGroup1, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.LayoutControlItem1, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.LayoutControlItem2, System.ComponentModel.ISupportInitialize).EndInit()
            Me.ResumeLayout(False)

        End Sub
        Friend WithEvents LayoutControl1 As DevExpress.XtraLayout.LayoutControl
        Friend WithEvents LayoutControlGroup1 As DevExpress.XtraLayout.LayoutControlGroup
        Friend WithEvents edit_item_total As DevExpress.XtraEditors.TextEdit
        Friend WithEvents edit_item_count As DevExpress.XtraEditors.TextEdit
        Friend WithEvents LayoutControlItem1 As DevExpress.XtraLayout.LayoutControlItem
        Friend WithEvents LayoutControlItem2 As DevExpress.XtraLayout.LayoutControlItem

    End Class
End Namespace
