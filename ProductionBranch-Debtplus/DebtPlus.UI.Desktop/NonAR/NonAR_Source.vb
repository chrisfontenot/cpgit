#Region "Copyright 2000-2012 DebtPlus, L.L.C."
'{*******************************************************************}
'{                                                                   }
'{       DebtPlus Debt Management System                             }
'{                                                                   }
'{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
'{       ALL RIGHTS RESERVED                                         }
'{                                                                   }
'{   The entire contents of this file is protected by U.S. and       }
'{   International Copyright Laws. Unauthorized reproduction,        }
'{   reverse-engineering, and distribution of all or any portion of  }
'{   the code contained in this file is strictly prohibited and may  }
'{   result in severe civil and criminal penalties and will be       }
'{   prosecuted to the maximum extent possible under the law.        }
'{                                                                   }
'{   RESTRICTIONS                                                    }
'{                                                                   }
'{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
'{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
'{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
'{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
'{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
'{                                                                   }
'{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
'{   ADDITIONAL RESTRICTIONS.                                        }
'{                                                                   }
'{*******************************************************************}

#End Region

Imports DevExpress.XtraEditors.Controls
Imports System.Data.SqlClient
Imports DevExpress.Utils
Imports DebtPlus.Data.Forms
Imports System.Windows.Forms

Namespace NonAR
    Friend Class NonAR_Source
        Implements ISubForms

        Public Event Changed(ByVal sender As Object, ByVal e As EventArgs) Implements ISubForms.Changed

        Public Sub New()
            MyBase.New()
            InitializeComponent()
            AddHandler LookUpEdit_source.ButtonPressed, AddressOf LookUpEdit_source_ButtonPressed
            AddHandler LookUpEdit_source.EditValueChanged, AddressOf LookUpEdit_source_EditValueChanged
            AddHandler LookUpEdit_source.EditValueChanging, AddressOf LookUpEdit_source_EditValueChanging
            AddHandler MemoEdit_note.EditValueChanged, AddressOf MemoEdit_note_EditValueChanged
        End Sub

        ''' <summary>
        ''' Is the SAVE mode valid now?
        ''' </summary>
        Friend ReadOnly Property SaveValid() As Boolean Implements ISubForms.SaveValid
            Get
                Return LookUpEdit_source.ErrorText = String.Empty AndAlso MemoEdit_note.ErrorText = String.Empty
            End Get
        End Property

        ''' <summary>
        ''' Do any pre-processing and load any unbound fields
        ''' </summary>
        Public Sub ReadForm() Implements ISubForms.ReadForm
            Dim drv As DataRowView = CType(Me.ParentForm, Form_Modal_NonAR).drv

            ' Find the source items from the list of ar sources
            Dim source_view As DataView = CType(LookUpEdit_source.Properties.DataSource, DataView)

            If source_view Is Nothing Then
                Dim tbl As DataTable = ds.Tables("ar_sources")
                If tbl Is Nothing Then
                    Dim cmd As SqlCommand = New SqlCommand
                    With cmd
                        .Connection = New SqlConnection(DebtPlus.LINQ.SQLInfoClass.getDefault().ConnectionString)
                        .CommandText = "select non_ar_source,description FROM non_ar_sources ORDER BY description"
                        .CommandType = CommandType.Text
                    End With

                    Dim current_cursor As Cursor = Cursor.Current
                    Try
                        Cursor.Current = Cursors.WaitCursor
                        Dim da As New SqlDataAdapter(cmd)
                        da.Fill(ds, "ar_sources")
                        tbl = ds.Tables("ar_sources")

                    Catch ex As SqlException
                        DebtPlus.UI.Common.ErrorHandling.HandleErrors(ex, "Error reading AR sources from database")

                    Finally
                        Cursor.Current = current_cursor
                    End Try
                End If

                source_view = New DataView(tbl, String.Empty, "description", DataViewRowState.CurrentRows)
                With LookUpEdit_source.Properties
                    .DataSource = source_view
                    .DisplayMember = "description"
                    .ValueMember = "non_ar_source"

                    .ShowFooter = False
                    .ShowHeader = False
                    .ShowLines = True
                    .Columns.AddRange(New LookUpColumnInfo() {New LookUpColumnInfo("non_ar_source", "non_ar_source", 25, FormatType.None, String.Empty, False, HorzAlignment.Far), New LookUpColumnInfo("description", "description", 75, FormatType.None, String.Empty, True, HorzAlignment.Near)})
                End With
            End If

            ' Bind the data members to the items
            With LookUpEdit_source
                .DataBindings.Clear()
                .DataBindings.Add("EditValue", drv, "non_ar_source")
                AddHandler .EditValueChanging, AddressOf DebtPlus.Data.Validation.LookUpEdit_ActiveTest
            End With

            With MemoEdit_note
                .DataBindings.Clear()
                .DataBindings.Add("EditValue", drv, "message")
            End With
        End Sub

        ''' <summary>
        ''' Save any fields to the record before it is closed
        ''' </summary>
        Public Sub SaveForm() Implements ISubForms.SaveForm
        End Sub

        ''' <summary>
        ''' Process the ADD button for the source
        ''' </summary>
        Private Sub LookUpEdit_source_ButtonPressed(ByVal sender As Object, ByVal e As ButtonPressedEventArgs)
            Dim ButtonID As String = Convert.ToString(e.Button.Tag)

            If ButtonID = "add" Then
                Dim SourceName As String = String.Empty
                If InputBox.Show("What name do you wish to add to the list of Non-AR sources", SourceName, "Non-AR Source", "", 50) = DialogResult.OK AndAlso SourceName.Length > 0 Then
                    Dim tbl As DataTable = ds.Tables("ar_sources")
                    Dim rows As DataRow() = tbl.Select(String.Format("[description]='{0}'", SourceName.Replace("'", "''")))
                    Dim NewItem As Int32 = -1
                    If rows.Length > 0 Then NewItem = Convert.ToInt32(rows(0)("item_key"))

                    ' If the current dataset does not have the new item, add it.
                    If NewItem < 0 Then

                        ' Update the table with the new row
                        Dim cn As SqlConnection = New SqlConnection(DebtPlus.LINQ.SQLInfoClass.getDefault().ConnectionString)
                        Dim current_cursor As Cursor = Cursor.Current
                        Try
                            Cursor.Current = Cursors.WaitCursor
                            cn.Open()
                            With New SqlCommand
                                .Connection = cn
                                .CommandText = "INSERT INTO non_ar_sources(description) VALUES (@description); SELECT scope_identity() as non_ar_source;"
                                .CommandType = CommandType.Text
                                .Parameters.Add("@description", SqlDbType.VarChar, 80).Value = SourceName
                                Dim objAnswer As Object = .ExecuteScalar()
                                If objAnswer IsNot Nothing AndAlso objAnswer IsNot DBNull.Value Then NewItem = Convert.ToInt32(objAnswer)
                            End With

                        Catch ex As SqlException
                            DebtPlus.UI.Common.ErrorHandling.HandleErrors(ex, "Error adding Non-AR source")
                        Finally
                            If cn IsNot Nothing AndAlso cn.State <> ConnectionState.Closed Then cn.Close()
                            Cursor.Current = current_cursor
                        End Try

                        Dim row As DataRow = tbl.NewRow()
                        row.BeginEdit()
                        row("description") = SourceName
                        row("non_ar_source") = NewItem
                        row.EndEdit()
                        tbl.Rows.Add(row)
                        tbl.AcceptChanges()
                    End If

                    ' Set the view to the new list of items
                    Dim source_view As New DataView(tbl, String.Empty, "description", DataViewRowState.CurrentRows)
                    With LookUpEdit_source
                        With .Properties
                            .DataSource = source_view
                        End With

                        .EditValue = NewItem
                        .Refresh()
                    End With
                End If
            End If
        End Sub

        ''' <summary>
        ''' When the source changes, tell our parent
        ''' </summary>
        Private Sub LookUpEdit_source_EditValueChanged(ByVal sender As Object, ByVal e As EventArgs)
            RaiseEvent Changed(Me, EventArgs.Empty)
        End Sub

        ''' <summary>
        ''' Process a change in the source field
        ''' </summary>
        Private Sub LookUpEdit_source_EditValueChanging(ByVal sender As Object, ByVal e As ChangingEventArgs)
            Dim ErrorText As String = String.Empty
            If e.NewValue Is Nothing OrElse e.NewValue Is DBNull.Value Then
                ErrorText = MisingText
            End If
            LookUpEdit_source.ErrorText = ErrorText
        End Sub

        ''' <summary>
        ''' Process a change in the MEMO field
        ''' </summary>
        Private Sub MemoEdit_note_EditValueChanged(ByVal sender As Object, ByVal e As EventArgs)
            RaiseEvent Changed(Me, EventArgs.Empty)
        End Sub
    End Class
End Namespace
