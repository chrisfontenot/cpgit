﻿Namespace NonAR
    <Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
    Partial Class NonAR_Source
        Inherits DevExpress.XtraEditors.XtraUserControl

        'UserControl overrides dispose to clean up the component list.
        <System.Diagnostics.DebuggerNonUserCode()> _
        Protected Overrides Sub Dispose(ByVal disposing As Boolean)
            Try
                If disposing AndAlso components IsNot Nothing Then
                    components.Dispose()
                End If
            Finally
                MyBase.Dispose(disposing)
            End Try
        End Sub

        'Required by the Windows Form Designer
        Private components As System.ComponentModel.IContainer

        'NOTE: The following procedure is required by the Windows Form Designer
        'It can be modified using the Windows Form Designer.  
        'Do not modify it using the code editor.
        <System.Diagnostics.DebuggerStepThrough()> _
        Private Sub InitializeComponent()
            Me.LayoutControl1 = New DevExpress.XtraLayout.LayoutControl
            Me.MemoEdit_note = New DevExpress.XtraEditors.MemoEdit
            Me.LookUpEdit_source = New DevExpress.XtraEditors.LookUpEdit
            Me.LayoutControlGroup1 = New DevExpress.XtraLayout.LayoutControlGroup
            Me.LayoutControlItem1 = New DevExpress.XtraLayout.LayoutControlItem
            Me.LayoutControlItem2 = New DevExpress.XtraLayout.LayoutControlItem
            CType(Me.LayoutControl1, System.ComponentModel.ISupportInitialize).BeginInit()
            Me.LayoutControl1.SuspendLayout()
            CType(Me.MemoEdit_note.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.LookUpEdit_source.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.LayoutControlGroup1, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.LayoutControlItem1, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.LayoutControlItem2, System.ComponentModel.ISupportInitialize).BeginInit()
            Me.SuspendLayout()
            '
            'LayoutControl1
            '
            Me.LayoutControl1.Controls.Add(Me.MemoEdit_note)
            Me.LayoutControl1.Controls.Add(Me.LookUpEdit_source)
            Me.LayoutControl1.Dock = System.Windows.Forms.DockStyle.Fill
            Me.LayoutControl1.Location = New System.Drawing.Point(0, 0)
            Me.LayoutControl1.Name = "LayoutControl1"
            Me.LayoutControl1.Root = Me.LayoutControlGroup1
            Me.LayoutControl1.Size = New System.Drawing.Size(418, 150)
            Me.LayoutControl1.TabIndex = 0
            Me.LayoutControl1.Text = "LayoutControl1"
            '
            'MemoEdit_note
            '
            Me.MemoEdit_note.Location = New System.Drawing.Point(39, 38)
            Me.MemoEdit_note.Name = "MemoEdit_note"
            Me.MemoEdit_note.Properties.MaxLength = 256
            Me.MemoEdit_note.Size = New System.Drawing.Size(373, 106)
            Me.MemoEdit_note.StyleController = Me.LayoutControl1
            Me.MemoEdit_note.TabIndex = 5
            '
            'LookUpEdit_source
            '
            Me.LookUpEdit_source.Location = New System.Drawing.Point(39, 7)
            Me.LookUpEdit_source.Name = "LookUpEdit_source"
            Me.LookUpEdit_source.Properties.ActionButtonIndex = 1
            Me.LookUpEdit_source.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Plus, "", -1, True, True, False, DevExpress.Utils.HorzAlignment.Center, Nothing, New DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), "Add a new item to the list", "add"), New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
            Me.LookUpEdit_source.Properties.NullText = "[Please choose an item here]"
            Me.LookUpEdit_source.Size = New System.Drawing.Size(373, 20)
            Me.LookUpEdit_source.StyleController = Me.LayoutControl1
            Me.LookUpEdit_source.TabIndex = 4
            '
            'LayoutControlGroup1
            '
            Me.LayoutControlGroup1.CustomizationFormText = "LayoutControlGroup1"
            Me.LayoutControlGroup1.Items.AddRange(New DevExpress.XtraLayout.BaseLayoutItem() {Me.LayoutControlItem1, Me.LayoutControlItem2})
            Me.LayoutControlGroup1.Location = New System.Drawing.Point(0, 0)
            Me.LayoutControlGroup1.Name = "LayoutControlGroup1"
            Me.LayoutControlGroup1.Size = New System.Drawing.Size(418, 150)
            Me.LayoutControlGroup1.Spacing = New DevExpress.XtraLayout.Utils.Padding(0, 0, 0, 0)
            Me.LayoutControlGroup1.Text = "LayoutControlGroup1"
            Me.LayoutControlGroup1.TextVisible = False
            '
            'LayoutControlItem1
            '
            Me.LayoutControlItem1.Control = Me.LookUpEdit_source
            Me.LayoutControlItem1.CustomizationFormText = "Name"
            Me.LayoutControlItem1.Location = New System.Drawing.Point(0, 0)
            Me.LayoutControlItem1.Name = "LayoutControlItem1"
            Me.LayoutControlItem1.Size = New System.Drawing.Size(416, 31)
            Me.LayoutControlItem1.Text = "Name"
            Me.LayoutControlItem1.TextLocation = DevExpress.Utils.Locations.Left
            Me.LayoutControlItem1.TextSize = New System.Drawing.Size(27, 20)
            '
            'LayoutControlItem2
            '
            Me.LayoutControlItem2.AppearanceItemCaption.Options.UseTextOptions = True
            Me.LayoutControlItem2.AppearanceItemCaption.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Top
            Me.LayoutControlItem2.Control = Me.MemoEdit_note
            Me.LayoutControlItem2.CustomizationFormText = "Note"
            Me.LayoutControlItem2.Location = New System.Drawing.Point(0, 31)
            Me.LayoutControlItem2.Name = "LayoutControlItem2"
            Me.LayoutControlItem2.Size = New System.Drawing.Size(416, 117)
            Me.LayoutControlItem2.Text = "Note"
            Me.LayoutControlItem2.TextLocation = DevExpress.Utils.Locations.Left
            Me.LayoutControlItem2.TextSize = New System.Drawing.Size(27, 20)
            '
            'NonAR_Source
            '
            Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
            Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
            Me.Controls.Add(Me.LayoutControl1)
            Me.Name = "NonAR_Source"
            Me.Size = New System.Drawing.Size(418, 150)
            CType(Me.LayoutControl1, System.ComponentModel.ISupportInitialize).EndInit()
            Me.LayoutControl1.ResumeLayout(False)
            CType(Me.MemoEdit_note.Properties, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.LookUpEdit_source.Properties, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.LayoutControlGroup1, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.LayoutControlItem1, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.LayoutControlItem2, System.ComponentModel.ISupportInitialize).EndInit()
            Me.ResumeLayout(False)

        End Sub
        Friend WithEvents LayoutControl1 As DevExpress.XtraLayout.LayoutControl
        Friend WithEvents LookUpEdit_source As DevExpress.XtraEditors.LookUpEdit
        Friend WithEvents LayoutControlGroup1 As DevExpress.XtraLayout.LayoutControlGroup
        Friend WithEvents LayoutControlItem1 As DevExpress.XtraLayout.LayoutControlItem
        Friend WithEvents MemoEdit_note As DevExpress.XtraEditors.MemoEdit
        Friend WithEvents LayoutControlItem2 As DevExpress.XtraLayout.LayoutControlItem

    End Class
End Namespace
