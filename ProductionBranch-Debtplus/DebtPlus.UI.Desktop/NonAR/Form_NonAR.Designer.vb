﻿Namespace NonAR
    <Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
    Partial Class Form_NonAR
        Inherits Form_Modal_NonAR

        'Form overrides dispose to clean up the component list.
        <System.Diagnostics.DebuggerNonUserCode()> _
        Protected Overrides Sub Dispose(ByVal disposing As Boolean)
            Try
                If disposing AndAlso components IsNot Nothing Then
                    components.Dispose()
                End If
            Finally
                MyBase.Dispose(disposing)
            End Try
        End Sub

        'Required by the Windows Form Designer
        Private components As System.ComponentModel.IContainer

        'NOTE: The following procedure is required by the Windows Form Designer
        'It can be modified using the Windows Form Designer.  
        'Do not modify it using the code editor.
        <System.Diagnostics.DebuggerStepThrough()> _
        Private Sub InitializeComponent()
            Me.NonAR_BatchList1 = New DebtPlus.UI.Desktop.NonAR.NonAR_BatchList
            Me.SimpleButton3 = New DevExpress.XtraEditors.SimpleButton
            CType(Me.PanelControl1, System.ComponentModel.ISupportInitialize).BeginInit()
            Me.PanelControl1.SuspendLayout()
            CType(Me.DxErrorProvider1, System.ComponentModel.ISupportInitialize).BeginInit()
            Me.SuspendLayout()
            '
            'NonAR_BatchTotals1
            '
            Me.ToolTipController1.SetSuperTip(Me.NonAR_BatchTotals1, Nothing)
            '
            'NonAR_Source1
            '
            Me.ToolTipController1.SetSuperTip(Me.NonAR_Source1, Nothing)
            '
            'SimpleButton1
            '
            Me.SimpleButton1.Location = New System.Drawing.Point(386, 30)
            '
            'SimpleButton2
            '
            Me.SimpleButton2.Location = New System.Drawing.Point(386, 100)
            Me.SimpleButton2.Text = "&Close"
            '
            'PanelControl1
            '
            Me.ToolTipController1.SetSuperTip(Me.PanelControl1, Nothing)
            '
            'NonAR_keypad1
            '
            Me.ToolTipController1.SetSuperTip(Me.NonAR_keypad1, Nothing)
            '
            'NonAR_keyboard1
            '
            Me.ToolTipController1.SetSuperTip(Me.NonAR_keyboard1, Nothing)
            Me.NonAR_keyboard1.TabStop = True
            '
            'NonAR_BatchList1
            '
            Me.NonAR_BatchList1.Location = New System.Drawing.Point(386, 141)
            Me.NonAR_BatchList1.Name = "NonAR_BatchList1"
            Me.NonAR_BatchList1.Size = New System.Drawing.Size(75, 249)
            Me.ToolTipController1.SetSuperTip(Me.NonAR_BatchList1, Nothing)
            Me.NonAR_BatchList1.TabIndex = 9
            '
            'SimpleButton3
            '
            Me.SimpleButton3.Location = New System.Drawing.Point(386, 65)
            Me.SimpleButton3.Name = "SimpleButton3"
            Me.SimpleButton3.Size = New System.Drawing.Size(75, 23)
            Me.SimpleButton3.TabIndex = 10
            Me.SimpleButton3.TabStop = False
            Me.SimpleButton3.Text = "Apply"
            '
            'Form_NonAR
            '
            Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
            Me.ClientSize = New System.Drawing.Size(473, 402)
            Me.Controls.Add(Me.NonAR_BatchList1)
            Me.Controls.Add(Me.SimpleButton3)
            Me.Name = "Form_NonAR"
            Me.ToolTipController1.SetSuperTip(Me, Nothing)
            Me.Controls.SetChildIndex(Me.SimpleButton3, 0)
            Me.Controls.SetChildIndex(Me.SimpleButton1, 0)
            Me.Controls.SetChildIndex(Me.SimpleButton2, 0)
            Me.Controls.SetChildIndex(Me.NonAR_BatchList1, 0)
            Me.Controls.SetChildIndex(Me.NonAR_BatchTotals1, 0)
            Me.Controls.SetChildIndex(Me.NonAR_Source1, 0)
            Me.Controls.SetChildIndex(Me.PanelControl1, 0)
            CType(Me.PanelControl1, System.ComponentModel.ISupportInitialize).EndInit()
            Me.PanelControl1.ResumeLayout(False)
            CType(Me.DxErrorProvider1, System.ComponentModel.ISupportInitialize).EndInit()
            Me.ResumeLayout(False)

        End Sub
        Friend WithEvents NonAR_BatchList1 As DebtPlus.UI.Desktop.NonAR.NonAR_BatchList
        Friend WithEvents SimpleButton3 As DevExpress.XtraEditors.SimpleButton
    End Class
End Namespace
