#Region "Copyright 2000-2012 DebtPlus, L.L.C."
'{*******************************************************************}
'{                                                                   }
'{       DebtPlus Debt Management System                             }
'{                                                                   }
'{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
'{       ALL RIGHTS RESERVED                                         }
'{                                                                   }
'{   The entire contents of this file is protected by U.S. and       }
'{   International Copyright Laws. Unauthorized reproduction,        }
'{   reverse-engineering, and distribution of all or any portion of  }
'{   the code contained in this file is strictly prohibited and may  }
'{   result in severe civil and criminal penalties and will be       }
'{   prosecuted to the maximum extent possible under the law.        }
'{                                                                   }
'{   RESTRICTIONS                                                    }
'{                                                                   }
'{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
'{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
'{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
'{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
'{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
'{                                                                   }
'{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
'{   ADDITIONAL RESTRICTIONS.                                        }
'{                                                                   }
'{*******************************************************************}
#End Region
Imports DebtPlus.Utils
Imports System.Data.SqlClient
Imports System.Windows.Forms

Namespace NonAR
    Friend Class Form_NonAR

        ''' <summary>
        ''' Create a new instance of our class
        ''' </summary>
        Public Sub New()
            MyBase.New()
            InitializeComponent()

        End Sub

        Public Sub New(ByVal ap As ArgParser)
            MyBase.New(ap)
            InitializeComponent()
            AddHandler Me.Load, AddressOf Form_NonAR_Load
        End Sub

        ''' <summary>
        ''' Process the loading of the form
        ''' </summary>
        Private Sub Form_NonAR_Load(ByVal sender As Object, ByVal e As System.EventArgs)

            ' Read the list of transactions for this batch
            ReadRegistersNonArTable(-1)
            registers_non_ar = ds.Tables("registers_non_ar")
            With registers_non_ar
                .PrimaryKey = New DataColumn() {.Columns("non_ar_register")}
                With .Columns("non_ar_register")
                    .AllowDBNull = False
                    .AutoIncrement = True
                    .AutoIncrementSeed = -1
                    .AutoIncrementStep = -1
                End With

                With .Columns("tran_type")
                    .AllowDBNull = False
                    .DefaultValue = "NA"
                    .MaxLength = 2
                End With

                With .Columns("non_ar_source")
                    .AllowDBNull = True
                End With

                With .Columns("client")
                    .AllowDBNull = True
                End With

                With .Columns("creditor")
                    .AllowDBNull = True
                    .MaxLength = 10
                End With

                With .Columns("non_ar_source")
                    .AllowDBNull = True
                    .DefaultValue = DBNull.Value
                End With

                With .Columns("credit_amt")
                    .DefaultValue = 0D
                    .AllowDBNull = False
                End With

                With .Columns("debit_amt")
                    .DefaultValue = 0D
                    .AllowDBNull = False
                End With

                With .Columns("date_created")
                    .DefaultValue = Now.Date
                    .AllowDBNull = False
                End With

                With .Columns("item_date")
                    .DefaultValue = Now.Date
                    .AllowDBNull = False
                End With

                With .Columns("reference")
                    .AllowDBNull = True
                    .MaxLength = 50
                End With

                With .Columns("created_by")
                    .AllowDBNull = False
                    .DefaultValue = "ME"
                End With

                With .Columns("src_ledger_account")
                    .AllowDBNull = True
                    .MaxLength = 10
                End With

                With .Columns("dst_ledger_account")
                    .AllowDBNull = True
                    .MaxLength = 10
                End With

                With .Columns("message")
                    .AllowDBNull = True
                    .MaxLength = 256
                End With
            End With

            ' Create the first record
            Dim vue As System.Data.DataView = registers_non_ar.DefaultView
            drv = vue.AddNew
            drv.BeginEdit()

            ' Bind the new items to a new record
            ItemEdit.ReadForm()
            NonAR_Source1.ReadForm()
            NonAR_BatchList1.ReadForm()
            NonAR_BatchTotals1.ReadForm()

            ' Add the handlers once the initialization is complete
            AddHandler FormClosing, AddressOf Form_NonAR_FormClosing
            AddHandler SimpleButton3.Click, AddressOf SimpleButton3_Click

            ' Hook into the changes in the table to recalculate the totals
            AddHandler registers_non_ar.RowChanged, AddressOf TableChanged
            AddHandler registers_non_ar.RowDeleted, AddressOf TableChanged
            AddHandler registers_non_ar.TableNewRow, AddressOf TableNewRow
        End Sub

        ''' <summary>
        ''' Update the totals when the form is changed
        ''' </summary>
        Protected Sub TableChanged(ByVal sender As Object, ByVal e As System.Data.DataRowChangeEventArgs)
            NonAR_BatchTotals1.ReadForm()
        End Sub

        Protected Sub TableNewRow(ByVal sender As Object, ByVal e As System.Data.DataTableNewRowEventArgs)
            NonAR_BatchTotals1.ReadForm()
        End Sub

        ''' <summary>
        ''' Save the changes when the SAVE button is pressed
        ''' This also creates a new row for the edit and starts the process again
        ''' </summary>
        Protected Overrides Sub SimpleButton1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)

            If SimpleButton1.Enabled Then

                ' Save the current row
                ItemEdit.SaveForm()
                NonAR_Source1.SaveForm()
                drv.EndEdit()

                ' Create a new row for the new item
                drv = drv.DataView.AddNew
                ItemEdit.ReadForm()
                NonAR_Source1.ReadForm()
                NonAR_BatchTotals1.ReadForm()
            End If
        End Sub

        ''' <summary>
        ''' When a field is changed, update the button status
        ''' </summary>
        Protected Overrides Sub Form_Changed(ByVal sender As Object, ByVal e As System.EventArgs)
            MyBase.Form_Changed(sender, e)
            SimpleButton3.Enabled = SimpleButton1.Enabled
        End Sub

        ''' <summary>
        ''' Close the form when the menu item is choosen
        ''' </summary>
        Protected Overrides Sub BarButtonItem1_ItemClick(ByVal sender As System.Object, ByVal e As DevExpress.XtraBars.ItemClickEventArgs)
            Close()
        End Sub

        ''' <summary>
        ''' Close the form when the CANCEL button is choosen
        ''' </summary>
        Protected Overrides Sub SimpleButton2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
            Close()
        End Sub

        ''' <summary>
        ''' Read the table definitions for the updates
        ''' </summary>
        Protected Sub ReadRegistersNonArTable(ByVal non_ar_register As System.Int32)
            Dim current_cursor As System.Windows.Forms.Cursor = System.Windows.Forms.Cursor.Current
            Try
                Dim da As New SqlClient.SqlDataAdapter(SelectCommand)
                da.Fill(ds, "registers_non_ar")

            Catch ex As SqlClient.SqlException
                DebtPlus.UI.Common.ErrorHandling.HandleErrors(ex, "Error reading registers_non_ar table")
            Finally
                System.Windows.Forms.Cursor.Current = current_cursor
            End Try
        End Sub

        Protected Friend Function ReadRegistersNonAr(ByVal non_ar_register As System.Int32) As System.Data.DataRow
            Dim answer As System.Data.DataRow = Nothing
            Dim tbl As System.Data.DataTable = ds.Tables("registers_non_ar")
            If tbl IsNot Nothing Then
                answer = tbl.Rows.Find(non_ar_register)
            End If

            ' If the row is not present then read it.
            If answer Is Nothing Then
                ReadRegistersNonArTable(non_ar_register)
                tbl = ds.Tables("registers_non_ar")
                If tbl IsNot Nothing Then
                    answer = tbl.Rows.Find(non_ar_register)
                End If
            End If

            Return answer
        End Function

        ''' <summary>
        ''' SELECT command to read the table
        ''' </summary>
        Protected Function SelectCommand() As SqlClient.SqlCommand
            Dim cmd As SqlClient.SqlCommand = New SqlCommand
            With cmd
                .Connection = New SqlConnection(DebtPlus.LINQ.SQLInfoClass.getDefault().ConnectionString)
                .CommandText = "SELECT non_ar_register,tran_type,non_ar_source,client,creditor,credit_amt,debit_amt,date_created,item_date,reference,created_by,src_ledger_account,dst_ledger_account,message FROM registers_non_ar WHERE non_ar_register=@non_ar_register"
                .CommandType = CommandType.Text
                .Parameters.Add("@non_ar_register", SqlDbType.Int).Value = -1
            End With
            Return cmd
        End Function

        ''' <summary>
        ''' INSERT command to add to the table
        ''' </summary>
        Protected Function InsertCommand() As SqlClient.SqlCommand
            Dim cmd As SqlClient.SqlCommand = New SqlCommand

            ' Generate the insert command to insert new rows
            With cmd
                .Connection = New SqlConnection(DebtPlus.LINQ.SQLInfoClass.getDefault().ConnectionString)
                .CommandText = "INSERT INTO registers_non_ar(tran_type,non_ar_source,client,creditor,credit_amt,debit_amt,item_date,reference,src_ledger_account,dst_ledger_account,message) values (@tran_type,@non_ar_source,@client,@creditor,@credit_amt,@debit_amt,@item_date,@reference,@src_ledger_account,@dst_ledger_account,@message)"
                .CommandType = CommandType.Text
                .Parameters.Add("@tran_type", SqlDbType.VarChar, 10, "tran_type")
                .Parameters.Add("@non_ar_source", SqlDbType.Int, 0, "non_ar_source")
                .Parameters.Add("@client", SqlDbType.Int, 0, "client")
                .Parameters.Add("@creditor", SqlDbType.VarChar, 10, "creditor")
                .Parameters.Add("@credit_amt", SqlDbType.Money, 0, "credit_amt")
                .Parameters.Add("@debit_amt", SqlDbType.Money, 0, "debit_amt")
                .Parameters.Add("@item_date", SqlDbType.DateTime, 8, "item_date")
                .Parameters.Add("@reference", SqlDbType.VarChar, 50, "reference")
                .Parameters.Add("@src_ledger_account", SqlDbType.VarChar, 10, "src_ledger_account")
                .Parameters.Add("@dst_ledger_account", SqlDbType.VarChar, 10, "dst_ledger_account")
                .Parameters.Add("@message", SqlDbType.VarChar, 256, "message")
            End With

            Return cmd
        End Function

        ''' <summary>
        ''' UPDATE command to change the table
        ''' </summary>
        Protected Function UpdateCommand() As SqlClient.SqlCommand
            Dim cmd As SqlClient.SqlCommand = New SqlCommand

            ' Generate the insert command to insert new rows
            With cmd
                .Connection = New SqlConnection(DebtPlus.LINQ.SQLInfoClass.getDefault().ConnectionString)
                .CommandText = "update registers_non_ar set tran_type=@tran_type,non_ar_source=@non_ar_source,client=@client,creditor=@creditor,credit_amt=@credit_amt,debit_amt=@debit_amt,item_date=@item_date,reference=@reference,src_ledger_account=@src_ledger_account,dst_ledger_account=@dst_ledger_account,dst_ledger_account,message=@message WHERE non_ar_register=@non_ar_register"
                .CommandType = CommandType.Text
                .Parameters.Add("@tran_type", SqlDbType.VarChar, 10, "tran_type")
                .Parameters.Add("@non_ar_source", SqlDbType.Int, 0, "non_ar_source")
                .Parameters.Add("@client", SqlDbType.Int, 0, "client")
                .Parameters.Add("@creditor", SqlDbType.VarChar, 10, "creditor")
                .Parameters.Add("@credit_amt", SqlDbType.Money, 0, "credit_amt")
                .Parameters.Add("@debit_amt", SqlDbType.Money, 0, "debit_amt")
                .Parameters.Add("@item_date", SqlDbType.DateTime, 8, "item_date")
                .Parameters.Add("@reference", SqlDbType.VarChar, 50, "reference")
                .Parameters.Add("@src_ledger_account", SqlDbType.VarChar, 10, "src_ledger_account")
                .Parameters.Add("@dst_ledger_account", SqlDbType.VarChar, 10, "dst_ledger_account")
                .Parameters.Add("@message", SqlDbType.VarChar, 256, "message")
                .Parameters.Add("@non_ar_register", SqlDbType.Int, 0, "non_ar_register")
            End With

            Return cmd
        End Function

        ''' <summary>
        ''' DELETE command to delete rows from the table
        ''' </summary>
        Protected Function DeleteCommand() As SqlClient.SqlCommand
            Dim cmd As SqlClient.SqlCommand = New SqlCommand

            ' Generate the insert command to insert new rows
            With cmd
                .Connection = New SqlConnection(DebtPlus.LINQ.SQLInfoClass.getDefault().ConnectionString)
                .CommandText = "DELETE FROM registers_non_ar WHERE non_ar_register=@non_ar_register"
                .CommandType = CommandType.Text
                .Parameters.Add("@non_ar_register", SqlDbType.Int, 0, "non_ar_register")
            End With

            Return cmd
        End Function

        ''' <summary>
        ''' Determine if there are pending items for the update
        ''' </summary>
        Protected Function PendingItems() As Boolean
            Dim answer As Boolean = False
            Dim tbl As System.Data.DataTable = ds.Tables("registers_non_ar")
            Dim vue As System.Data.DataView

            If tbl IsNot Nothing Then
                vue = New System.Data.DataView(tbl, String.Empty, String.Empty, DataViewRowState.Added)
                If vue.Count > 0 Then answer = True

                If Not answer Then
                    vue = New System.Data.DataView(tbl, String.Empty, String.Empty, DataViewRowState.ModifiedCurrent)
                    If vue.Count > 0 Then answer = True
                End If

                If Not answer Then
                    vue = New System.Data.DataView(tbl, String.Empty, String.Empty, DataViewRowState.Deleted)
                    If vue.Count > 0 Then answer = True
                End If
            End If

            Return answer
        End Function

        ''' <summary>
        ''' Ask if the update is desired when the form is closed
        ''' </summary>
        Protected Sub Form_NonAR_FormClosing(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosingEventArgs)

            If PendingItems() Then
                Select Case DebtPlus.Data.Forms.MessageBox.Show("You have items that have not been saved to the database." + Environment.NewLine + Environment.NewLine + "Do you wish to save them now?", "Pending Items", MessageBoxButtons.YesNoCancel, MessageBoxIcon.Question, MessageBoxDefaultButton.Button1)
                    Case Windows.Forms.DialogResult.No
                    Case Windows.Forms.DialogResult.Yes
                        If Not ApplyChanges() Then e.Cancel = True

                    Case Else
                        e.Cancel = True
                        Return
                End Select
            End If
        End Sub

        ''' <summary>
        ''' Do the apply function to save and update the table
        ''' </summary>
        Private Sub SimpleButton3_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
            If drv IsNot Nothing Then

                ' Save the current changes
                ItemEdit.SaveForm()
                NonAR_Source1.SaveForm()
                drv.EndEdit()

                ' Update the database with the new changes and clear the pending table
                If ApplyChanges() Then
                    registers_non_ar.Clear()
                    drv = registers_non_ar.DefaultView.AddNew
                End If

                ' Create a new row for the new item
                ItemEdit.ReadForm()
                NonAR_Source1.ReadForm()
                NonAR_BatchTotals1.ReadForm()
                drv.BeginEdit()
            End If
        End Sub

        ''' <summary>
        ''' update the table
        ''' </summary>
        Private Function ApplyChanges() As Boolean
            Dim Result As Boolean = False
            Dim current_cursor As System.Windows.Forms.Cursor = System.Windows.Forms.Cursor.Current
            Try
                System.Windows.Forms.Cursor.Current = Cursors.WaitCursor

                Dim da As New SqlClient.SqlDataAdapter()
                da.InsertCommand = InsertCommand()
                da.UpdateCommand = UpdateCommand()
                da.DeleteCommand = DeleteCommand()

                Dim tbl As System.Data.DataTable = ds.Tables("registers_non_ar")
                da.Update(tbl)
                Result = True

            Catch ex As SqlClient.SqlException
                DebtPlus.UI.Common.ErrorHandling.HandleErrors(ex, "Error updating the database")

            Finally
                System.Windows.Forms.Cursor.Current = current_cursor
            End Try

            Return Result
        End Function
    End Class
End Namespace
