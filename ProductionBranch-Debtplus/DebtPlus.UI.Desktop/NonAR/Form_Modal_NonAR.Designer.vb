﻿Namespace NonAR
    <Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
    Partial Class Form_Modal_NonAR

        'Form overrides dispose to clean up the component list.
        <System.Diagnostics.DebuggerNonUserCode()> _
        Protected Overrides Sub Dispose(ByVal disposing As Boolean)
            Try
                If disposing AndAlso components IsNot Nothing Then
                    components.Dispose()
                End If
            Finally
                MyBase.Dispose(disposing)
            End Try
        End Sub

        'Required by the Windows Form Designer
        Private components As System.ComponentModel.IContainer

        'NOTE: The following procedure is required by the Windows Form Designer
        'It can be modified using the Windows Form Designer.  
        'Do not modify it using the code editor.
        <System.Diagnostics.DebuggerStepThrough()> _
        Private Sub InitializeComponent()
            Me.components = New System.ComponentModel.Container()
            Me.SimpleButton1 = New DevExpress.XtraEditors.SimpleButton()
            Me.SimpleButton2 = New DevExpress.XtraEditors.SimpleButton()
            Me.NonAR_Source1 = New DebtPlus.UI.Desktop.NonAR.NonAR_Source()
            Me.NonAR_BatchTotals1 = New DebtPlus.UI.Desktop.NonAR.NonAR_BatchTotals()
            Me.barManager1 = New DevExpress.XtraBars.BarManager(Me.components)
            Me.Bar2 = New DevExpress.XtraBars.Bar()
            Me.BarSubItem1 = New DevExpress.XtraBars.BarSubItem()
            Me.BarButtonItem1 = New DevExpress.XtraBars.BarButtonItem()
            Me.BarSubItem2 = New DevExpress.XtraBars.BarSubItem()
            Me.BarButtonItem2 = New DevExpress.XtraBars.BarCheckItem()
            Me.barDockControlTop = New DevExpress.XtraBars.BarDockControl()
            Me.barDockControlBottom = New DevExpress.XtraBars.BarDockControl()
            Me.barDockControlLeft = New DevExpress.XtraBars.BarDockControl()
            Me.barDockControlRight = New DevExpress.XtraBars.BarDockControl()
            Me.PanelControl1 = New DevExpress.XtraEditors.PanelControl()
            Me.NonAR_keyboard1 = New DebtPlus.UI.Desktop.NonAR.NonAR_Keyboard()
            Me.NonAR_keypad1 = New DebtPlus.UI.Desktop.NonAR.NonAR_Keypad()
            CType(Me.DxErrorProvider1, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.barManager1, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.PanelControl1, System.ComponentModel.ISupportInitialize).BeginInit()
            Me.PanelControl1.SuspendLayout()
            Me.SuspendLayout()
            '
            'SimpleButton1
            '
            Me.SimpleButton1.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
            Me.SimpleButton1.Location = New System.Drawing.Point(386, 42)
            Me.SimpleButton1.Name = "SimpleButton1"
            Me.SimpleButton1.Size = New System.Drawing.Size(75, 23)
            Me.SimpleButton1.TabIndex = 3
            Me.SimpleButton1.Text = "&Save"
            '
            'SimpleButton2
            '
            Me.SimpleButton2.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
            Me.SimpleButton2.Location = New System.Drawing.Point(386, 71)
            Me.SimpleButton2.Name = "SimpleButton2"
            Me.SimpleButton2.Size = New System.Drawing.Size(75, 23)
            Me.SimpleButton2.TabIndex = 4
            Me.SimpleButton2.TabStop = False
            Me.SimpleButton2.Text = "&Cancel"
            '
            'NonAR_Source1
            '
            Me.NonAR_Source1.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
            Me.NonAR_Source1.Location = New System.Drawing.Point(13, 100)
            Me.NonAR_Source1.Name = "NonAR_Source1"
            Me.NonAR_Source1.Size = New System.Drawing.Size(357, 150)
            Me.NonAR_Source1.TabIndex = 1
            '
            'NonAR_BatchTotals1
            '
            Me.NonAR_BatchTotals1.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
            Me.NonAR_BatchTotals1.Location = New System.Drawing.Point(13, 27)
            Me.NonAR_BatchTotals1.Name = "NonAR_BatchTotals1"
            Me.NonAR_BatchTotals1.Size = New System.Drawing.Size(357, 67)
            Me.NonAR_BatchTotals1.TabIndex = 0
            Me.NonAR_BatchTotals1.TabStop = False
            '
            'barManager1
            '
            Me.barManager1.AllowShowToolbarsPopup = False
            Me.barManager1.Bars.AddRange(New DevExpress.XtraBars.Bar() {Me.Bar2})
            Me.barManager1.DockControls.Add(Me.barDockControlTop)
            Me.barManager1.DockControls.Add(Me.barDockControlBottom)
            Me.barManager1.DockControls.Add(Me.barDockControlLeft)
            Me.barManager1.DockControls.Add(Me.barDockControlRight)
            Me.barManager1.Form = Me
            Me.barManager1.Items.AddRange(New DevExpress.XtraBars.BarItem() {Me.BarSubItem1, Me.BarButtonItem1, Me.BarSubItem2, Me.BarButtonItem2})
            Me.barManager1.MainMenu = Me.Bar2
            Me.barManager1.MaxItemId = 4
            '
            'Bar2
            '
            Me.Bar2.BarName = "Main menu"
            Me.Bar2.DockCol = 0
            Me.Bar2.DockRow = 0
            Me.Bar2.DockStyle = DevExpress.XtraBars.BarDockStyle.Top
            Me.Bar2.FloatLocation = New System.Drawing.Point(227, 144)
            Me.Bar2.FloatSize = New System.Drawing.Size(105, 24)
            Me.Bar2.LinksPersistInfo.AddRange(New DevExpress.XtraBars.LinkPersistInfo() {New DevExpress.XtraBars.LinkPersistInfo(Me.BarSubItem1)})
            Me.Bar2.OptionsBar.MultiLine = True
            Me.Bar2.OptionsBar.UseWholeRow = True
            Me.Bar2.Text = "Main menu"
            '
            'BarSubItem1
            '
            Me.BarSubItem1.Caption = "&File"
            Me.BarSubItem1.Id = 0
            Me.BarSubItem1.LinksPersistInfo.AddRange(New DevExpress.XtraBars.LinkPersistInfo() {New DevExpress.XtraBars.LinkPersistInfo(Me.BarButtonItem1)})
            Me.BarSubItem1.Name = "BarSubItem1"
            '
            'BarButtonItem1
            '
            Me.BarButtonItem1.Caption = "&Exit"
            Me.BarButtonItem1.Id = 1
            Me.BarButtonItem1.Name = "BarButtonItem1"
            '
            'BarSubItem2
            '
            Me.BarSubItem2.Caption = "&Options"
            Me.BarSubItem2.Id = 2
            Me.BarSubItem2.LinksPersistInfo.AddRange(New DevExpress.XtraBars.LinkPersistInfo() {New DevExpress.XtraBars.LinkPersistInfo(Me.BarButtonItem2)})
            Me.BarSubItem2.Name = "BarSubItem2"
            '
            'BarButtonItem2
            '
            Me.BarButtonItem2.Caption = "&Use Keypad Enter"
            Me.BarButtonItem2.Id = 3
            Me.BarButtonItem2.Name = "BarButtonItem2"
            '
            'barDockControlTop
            '
            Me.barDockControlTop.CausesValidation = False
            Me.barDockControlTop.Dock = System.Windows.Forms.DockStyle.Top
            Me.barDockControlTop.Location = New System.Drawing.Point(0, 0)
            Me.barDockControlTop.Size = New System.Drawing.Size(473, 22)
            '
            'barDockControlBottom
            '
            Me.barDockControlBottom.CausesValidation = False
            Me.barDockControlBottom.Dock = System.Windows.Forms.DockStyle.Bottom
            Me.barDockControlBottom.Location = New System.Drawing.Point(0, 402)
            Me.barDockControlBottom.Size = New System.Drawing.Size(473, 0)
            '
            'barDockControlLeft
            '
            Me.barDockControlLeft.CausesValidation = False
            Me.barDockControlLeft.Dock = System.Windows.Forms.DockStyle.Left
            Me.barDockControlLeft.Location = New System.Drawing.Point(0, 22)
            Me.barDockControlLeft.Size = New System.Drawing.Size(0, 380)
            '
            'barDockControlRight
            '
            Me.barDockControlRight.CausesValidation = False
            Me.barDockControlRight.Dock = System.Windows.Forms.DockStyle.Right
            Me.barDockControlRight.Location = New System.Drawing.Point(473, 22)
            Me.barDockControlRight.Size = New System.Drawing.Size(0, 380)
            '
            'PanelControl1
            '
            Me.PanelControl1.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
            Me.PanelControl1.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder
            Me.PanelControl1.Controls.Add(Me.NonAR_keyboard1)
            Me.PanelControl1.Controls.Add(Me.NonAR_keypad1)
            Me.PanelControl1.Location = New System.Drawing.Point(13, 256)
            Me.PanelControl1.Name = "PanelControl1"
            Me.PanelControl1.Size = New System.Drawing.Size(358, 134)
            Me.PanelControl1.TabIndex = 2
            '
            'NonAR_keyboard1
            '
            Me.NonAR_keyboard1.Dock = System.Windows.Forms.DockStyle.Fill
            Me.NonAR_keyboard1.Location = New System.Drawing.Point(0, 0)
            Me.NonAR_keyboard1.Name = "NonAR_keyboard1"
            Me.NonAR_keyboard1.Size = New System.Drawing.Size(358, 134)
            Me.NonAR_keyboard1.TabIndex = 1
            Me.NonAR_keyboard1.TabStop = False
            '
            'NonAR_keypad1
            '
            Me.NonAR_keypad1.Dock = System.Windows.Forms.DockStyle.Fill
            Me.NonAR_keypad1.Location = New System.Drawing.Point(0, 0)
            Me.NonAR_keypad1.Name = "NonAR_keypad1"
            Me.NonAR_keypad1.Size = New System.Drawing.Size(358, 134)
            Me.NonAR_keypad1.TabIndex = 0
            Me.NonAR_keypad1.TabStop = False
            '
            'Form_Modal_NonAR
            '
            Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
            Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
            Me.ClientSize = New System.Drawing.Size(473, 402)
            Me.Controls.Add(Me.PanelControl1)
            Me.Controls.Add(Me.SimpleButton2)
            Me.Controls.Add(Me.SimpleButton1)
            Me.Controls.Add(Me.NonAR_Source1)
            Me.Controls.Add(Me.NonAR_BatchTotals1)
            Me.Controls.Add(Me.barDockControlLeft)
            Me.Controls.Add(Me.barDockControlRight)
            Me.Controls.Add(Me.barDockControlBottom)
            Me.Controls.Add(Me.barDockControlTop)
            Me.Name = "Form_Modal_NonAR"
            Me.Text = "Add Non-AR Deposits to Ledger Accounts"
            CType(Me.DxErrorProvider1, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.barManager1, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.PanelControl1, System.ComponentModel.ISupportInitialize).EndInit()
            Me.PanelControl1.ResumeLayout(False)
            Me.ResumeLayout(False)

        End Sub
        Protected Friend WithEvents NonAR_BatchTotals1 As DebtPlus.UI.Desktop.NonAR.NonAR_BatchTotals
        Protected Friend WithEvents NonAR_Source1 As DebtPlus.UI.Desktop.NonAR.NonAR_Source
        Protected Friend WithEvents SimpleButton1 As DevExpress.XtraEditors.SimpleButton
        Protected Friend WithEvents SimpleButton2 As DevExpress.XtraEditors.SimpleButton
        Protected Friend WithEvents barManager1 As DevExpress.XtraBars.BarManager
        Protected Friend WithEvents Bar2 As DevExpress.XtraBars.Bar
        Protected Friend WithEvents barDockControlTop As DevExpress.XtraBars.BarDockControl
        Protected Friend WithEvents barDockControlBottom As DevExpress.XtraBars.BarDockControl
        Protected Friend WithEvents barDockControlLeft As DevExpress.XtraBars.BarDockControl
        Protected Friend WithEvents barDockControlRight As DevExpress.XtraBars.BarDockControl
        Protected Friend WithEvents PanelControl1 As DevExpress.XtraEditors.PanelControl
        Protected Friend WithEvents NonAR_keypad1 As DebtPlus.UI.Desktop.NonAR.NonAR_Keypad
        Protected Friend WithEvents NonAR_keyboard1 As DebtPlus.UI.Desktop.NonAR.NonAR_Keyboard
        Protected Friend WithEvents BarSubItem1 As DevExpress.XtraBars.BarSubItem
        Protected Friend WithEvents BarButtonItem1 As DevExpress.XtraBars.BarButtonItem
        Protected Friend WithEvents BarSubItem2 As DevExpress.XtraBars.BarSubItem
        Protected Friend WithEvents BarButtonItem2 As DevExpress.XtraBars.BarCheckItem
    End Class
End Namespace
