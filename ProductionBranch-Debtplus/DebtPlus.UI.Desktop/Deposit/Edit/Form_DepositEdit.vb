#Region "Copyright 2000-2012 DebtPlus, L.L.C."
'{*******************************************************************}
'{                                                                   }
'{       DebtPlus Debt Management System                             }
'{                                                                   }
'{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
'{       ALL RIGHTS RESERVED                                         }
'{                                                                   }
'{   The entire contents of this file is protected by U.S. and       }
'{   International Copyright Laws. Unauthorized reproduction,        }
'{   reverse-engineering, and distribution of all or any portion of  }
'{   the code contained in this file is strictly prohibited and may  }
'{   result in severe civil and criminal penalties and will be       }
'{   prosecuted to the maximum extent possible under the law.        }
'{                                                                   }
'{   RESTRICTIONS                                                    }
'{                                                                   }
'{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
'{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
'{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
'{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
'{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
'{                                                                   }
'{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
'{   ADDITIONAL RESTRICTIONS.                                        }
'{                                                                   }
'{*******************************************************************}

#End Region

Option Compare Binary
Option Explicit On
Option Strict On

Imports System.ComponentModel
Imports DebtPlus.Data.Forms
Imports DevExpress.XtraBars
Imports System.Windows.Forms

Namespace Deposit.Edit
    Public Class Form_DepositEdit
        Inherits DebtPlusForm

        ''' <summary>
        ''' Create an instance of our class
        ''' </summary>
        Friend ap As DepositEditArgParser

        Public Sub New(ByVal ap As DepositEditArgParser)
            MyClass.New()
            Me.ap = ap
        End Sub

        Public Sub New()
            MyBase.New()
            InitializeComponent()
            AddHandler Me.FormClosing, AddressOf EditForm_FormClosing
            AddHandler Me.Load, AddressOf Form_DepositEdit_Load
            AddHandler SelectControl1.Cancelled, AddressOf SelectControl1_Cancelled
            AddHandler SelectControl1.Selected, AddressOf SelectControl1_Selected
            AddHandler EditControl1.Cancelled, AddressOf EditControl1_Cancelled
            AddHandler BarButtonItem_File_Exit.ItemClick, AddressOf BarButtonItem_File_Exit_ItemClick
            AddHandler BarButtonItem_File_Print.ItemClick, AddressOf BarButtonItem_File_Print_ItemClick
        End Sub

#Region " Windows Form Designer generated code "

        'Form overrides dispose to clean up the component list.
        Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
            If disposing Then
                If components IsNot Nothing Then
                    components.Dispose()
                End If
            End If
            MyBase.Dispose(disposing)
        End Sub

        'Required by the Windows Form Designer
        Private components As System.ComponentModel.IContainer
        Friend WithEvents EditControl1 As DebtPlus.UI.Desktop.Deposit.Edit.EditControl
        Protected Friend WithEvents barManager1 As DevExpress.XtraBars.BarManager
        Friend WithEvents BarMainMenu As DevExpress.XtraBars.Bar
        Protected Friend WithEvents BarSubItem_File As DevExpress.XtraBars.BarSubItem
        Protected Friend WithEvents BarButtonItem_File_Print As DevExpress.XtraBars.BarButtonItem
        Protected Friend WithEvents BarButtonItem_File_Exit As DevExpress.XtraBars.BarButtonItem
        Friend WithEvents barDockControlTop As DevExpress.XtraBars.BarDockControl
        Friend WithEvents barDockControlBottom As DevExpress.XtraBars.BarDockControl
        Friend WithEvents barDockControlLeft As DevExpress.XtraBars.BarDockControl
        Friend WithEvents barDockControlRight As DevExpress.XtraBars.BarDockControl

        'NOTE: The following procedure is required by the Windows Form Designer
        'It can be modified using the Windows Form Designer.  
        'Do not modify it using the code editor.
        Friend WithEvents SelectControl1 As DebtPlus.UI.Desktop.Deposit.Edit.SelectControl

        <System.Diagnostics.DebuggerStepThrough()>
        Private Sub InitializeComponent()
            Me.components = New System.ComponentModel.Container()
            Me.SelectControl1 = New DebtPlus.UI.Desktop.Deposit.Edit.SelectControl()
            Me.EditControl1 = New DebtPlus.UI.Desktop.Deposit.Edit.EditControl()
            Me.barManager1 = New DevExpress.XtraBars.BarManager(Me.components)
            Me.BarMainMenu = New DevExpress.XtraBars.Bar()
            Me.BarSubItem_File = New DevExpress.XtraBars.BarSubItem()
            Me.BarButtonItem_File_Print = New DevExpress.XtraBars.BarButtonItem()
            Me.BarButtonItem_File_Exit = New DevExpress.XtraBars.BarButtonItem()
            Me.barDockControlTop = New DevExpress.XtraBars.BarDockControl()
            Me.barDockControlBottom = New DevExpress.XtraBars.BarDockControl()
            Me.barDockControlLeft = New DevExpress.XtraBars.BarDockControl()
            Me.barDockControlRight = New DevExpress.XtraBars.BarDockControl()
            CType(Me.DxErrorProvider1, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.barManager1, System.ComponentModel.ISupportInitialize).BeginInit()
            Me.SuspendLayout()
            '
            'SelectControl1
            '
            Me.SelectControl1.Dock = System.Windows.Forms.DockStyle.Fill
            Me.SelectControl1.Location = New System.Drawing.Point(0, 25)
            Me.SelectControl1.Name = "SelectControl1"
            Me.SelectControl1.Padding = New System.Windows.Forms.Padding(4)
            Me.SelectControl1.Size = New System.Drawing.Size(560, 269)
            Me.SelectControl1.TabIndex = 0
            '
            'EditControl1
            '
            Me.EditControl1.Dock = System.Windows.Forms.DockStyle.Fill
            Me.EditControl1.Location = New System.Drawing.Point(0, 25)
            Me.EditControl1.Name = "EditControl1"
            Me.EditControl1.Size = New System.Drawing.Size(560, 269)
            Me.EditControl1.TabIndex = 1
            '
            'barManager1
            '
            Me.barManager1.Bars.AddRange(New DevExpress.XtraBars.Bar() {Me.BarMainMenu})
            Me.barManager1.DockControls.Add(Me.barDockControlTop)
            Me.barManager1.DockControls.Add(Me.barDockControlBottom)
            Me.barManager1.DockControls.Add(Me.barDockControlLeft)
            Me.barManager1.DockControls.Add(Me.barDockControlRight)
            Me.barManager1.Form = Me
            Me.barManager1.Items.AddRange(New DevExpress.XtraBars.BarItem() {Me.BarSubItem_File, Me.BarButtonItem_File_Print, Me.BarButtonItem_File_Exit})
            Me.barManager1.MainMenu = Me.BarMainMenu
            Me.barManager1.MaxItemId = 3
            '
            'BarMainMenu
            '
            Me.BarMainMenu.BarName = "Main menu"
            Me.BarMainMenu.DockCol = 0
            Me.BarMainMenu.DockRow = 0
            Me.BarMainMenu.DockStyle = DevExpress.XtraBars.BarDockStyle.Top
            Me.BarMainMenu.LinksPersistInfo.AddRange(New DevExpress.XtraBars.LinkPersistInfo() {New DevExpress.XtraBars.LinkPersistInfo(Me.BarSubItem_File)})
            Me.BarMainMenu.OptionsBar.MultiLine = True
            Me.BarMainMenu.OptionsBar.UseWholeRow = True
            Me.BarMainMenu.Text = "Main menu"
            '
            'BarSubItem_File
            '
            Me.BarSubItem_File.Caption = "&File"
            Me.BarSubItem_File.Id = 0
            Me.BarSubItem_File.LinksPersistInfo.AddRange(New DevExpress.XtraBars.LinkPersistInfo() {New DevExpress.XtraBars.LinkPersistInfo(Me.BarButtonItem_File_Print), New DevExpress.XtraBars.LinkPersistInfo(Me.BarButtonItem_File_Exit)})
            Me.BarSubItem_File.Name = "BarSubItem_File"
            '
            'BarButtonItem_File_Print
            '
            Me.BarButtonItem_File_Print.Caption = "&Print..."
            Me.BarButtonItem_File_Print.Id = 1
            Me.BarButtonItem_File_Print.Name = "BarButtonItem_File_Print"
            '
            'BarButtonItem_File_Exit
            '
            Me.BarButtonItem_File_Exit.Caption = "&Exit"
            Me.BarButtonItem_File_Exit.Id = 2
            Me.BarButtonItem_File_Exit.Name = "BarButtonItem_File_Exit"
            '
            'barDockControlTop
            '
            Me.barDockControlTop.CausesValidation = False
            Me.barDockControlTop.Dock = System.Windows.Forms.DockStyle.Top
            Me.barDockControlTop.Location = New System.Drawing.Point(0, 0)
            Me.barDockControlTop.Size = New System.Drawing.Size(560, 25)
            '
            'barDockControlBottom
            '
            Me.barDockControlBottom.CausesValidation = False
            Me.barDockControlBottom.Dock = System.Windows.Forms.DockStyle.Bottom
            Me.barDockControlBottom.Location = New System.Drawing.Point(0, 294)
            Me.barDockControlBottom.Size = New System.Drawing.Size(560, 0)
            '
            'barDockControlLeft
            '
            Me.barDockControlLeft.CausesValidation = False
            Me.barDockControlLeft.Dock = System.Windows.Forms.DockStyle.Left
            Me.barDockControlLeft.Location = New System.Drawing.Point(0, 25)
            Me.barDockControlLeft.Size = New System.Drawing.Size(0, 269)
            '
            'barDockControlRight
            '
            Me.barDockControlRight.CausesValidation = False
            Me.barDockControlRight.Dock = System.Windows.Forms.DockStyle.Right
            Me.barDockControlRight.Location = New System.Drawing.Point(560, 25)
            Me.barDockControlRight.Size = New System.Drawing.Size(0, 269)
            '
            'Form_DepositEdit
            '
            Me.ClientSize = New System.Drawing.Size(560, 294)
            Me.Controls.Add(Me.SelectControl1)
            Me.Controls.Add(Me.EditControl1)
            Me.Controls.Add(Me.barDockControlLeft)
            Me.Controls.Add(Me.barDockControlRight)
            Me.Controls.Add(Me.barDockControlBottom)
            Me.Controls.Add(Me.barDockControlTop)
            Me.Name = "Form_DepositEdit"
            Me.Text = "Edit Deposit Batch"
            CType(Me.DxErrorProvider1, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.barManager1, System.ComponentModel.ISupportInitialize).EndInit()
            Me.ResumeLayout(False)
        End Sub

#End Region

        Dim ActivePage As MyControls

        Private Sub EditForm_FormClosing(ByVal sender As Object, ByVal e As FormClosingEventArgs)
            If ActivePage IsNot Nothing Then
                If Not ActivePage.SaveForm Then e.Cancel = True
            End If
        End Sub

        Private Sub Form_DepositEdit_Load(ByVal sender As Object, ByVal e As EventArgs)

            If ap.Batch <= 0 Then
                With EditControl1
                    .Visible = False
                    .TabStop = False
                    .SendToBack()
                End With

                With SelectControl1
                    .Visible = True
                    .TabStop = True
                    .BringToFront()
                    ActivePage = SelectControl1
                    ActivePage.ReadForm()
                End With
            Else
                ProcessBatch()
            End If
        End Sub

        Private Sub SelectControl1_Cancelled(ByVal sender As Object, ByVal e As EventArgs)
            Close()
        End Sub

        Private Sub ProcessBatch()
            With SelectControl1
                .Visible = False
                .TabStop = False
                .SendToBack()
            End With

            With EditControl1
                ActivePage = EditControl1
                ActivePage.ReadForm()
                .Visible = True
                .TabStop = True
                .BringToFront()
                .ReadForm()
            End With
        End Sub

        Private Sub SelectControl1_Selected(ByVal sender As Object, ByVal e As EventArgs)
            ap.Batch = SelectControl1.DepositBatchID
            ProcessBatch()
        End Sub

        Private Sub EditControl1_Cancelled(ByVal sender As Object, ByVal e As EventArgs)
            With EditControl1
                .Visible = False
                .TabStop = False
                .SendToBack()
            End With

            With SelectControl1
                .Visible = True
                .TabStop = True
                .BringToFront()
                ActivePage = SelectControl1
                ActivePage.ReadForm()
            End With
        End Sub

        Private Sub BarButtonItem_File_Exit_ItemClick(ByVal sender As Object, ByVal e As ItemClickEventArgs)
            Close()
        End Sub

        Private WithEvents btReportThread As New BackgroundWorker

        Private Sub BarButtonItem_File_Print_ItemClick(ByVal sender As Object, ByVal e As ItemClickEventArgs)
            If Not btReportThread.IsBusy Then
                btReportThread.RunWorkerAsync(ap.Batch)
            End If
        End Sub

        'AddHandler btReportThread.DoWork, AddressOf btReportThread_DoWork
        'Private Sub btReportThread_DoWork (ByVal sender As Object, ByVal e As System.ComponentModel.DoWorkEventArgs)
        'Dim Batch As Int32 = Convert.ToInt32(e.Argument)
        'Dim rpt As New DebtPlus.Reports.Deposits.Batch.DepositBatchReport()
        '    rpt.Parameter_DepositBatchID = Batch
        '    If rpt.RequestReportParameters = System.Windows.Forms.DialogResult.OK Then
        '        rpt.DisplayPreviewDialog()
        '    End If
        'End Sub
    End Class

    Friend Interface MyControls
        Sub ReadForm()
        Function SaveForm() As Boolean
    End Interface
End Namespace
