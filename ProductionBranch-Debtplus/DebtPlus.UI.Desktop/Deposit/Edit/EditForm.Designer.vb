﻿Imports DebtPlus.UI.Client.Widgets.Controls

Namespace Deposit.Edit

    <Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
    Partial Class EditForm
        Inherits DebtPlus.Data.Forms.DebtPlusForm

        'Form overrides dispose to clean up the component list.
        <System.Diagnostics.DebuggerNonUserCode()> _
        Protected Overrides Sub Dispose(ByVal disposing As Boolean)
            Try
                If disposing AndAlso components IsNot Nothing Then
                    components.Dispose()
                End If
            Finally
                MyBase.Dispose(disposing)
            End Try
        End Sub

        'Required by the Windows Form Designer
        Private components As System.ComponentModel.IContainer

        'NOTE: The following procedure is required by the Windows Form Designer
        'It can be modified using the Windows Form Designer.  
        'Do not modify it using the code editor.
        <System.Diagnostics.DebuggerStepThrough()> _
        Private Sub InitializeComponent()
            Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(EditForm))
            Dim SerializableAppearanceObject1 As DevExpress.Utils.SerializableAppearanceObject = New DevExpress.Utils.SerializableAppearanceObject
            Me.LabelControl1 = New DevExpress.XtraEditors.LabelControl
            Me.SimpleButton_OK = New DevExpress.XtraEditors.SimpleButton
            Me.SimpleButton_Cancel = New DevExpress.XtraEditors.SimpleButton
            Me.LabelControl_scanned_client = New DevExpress.XtraEditors.LabelControl
            Me.LabelControl2 = New DevExpress.XtraEditors.LabelControl
            Me.LabelControl3 = New DevExpress.XtraEditors.LabelControl
            Me.LabelControl4 = New DevExpress.XtraEditors.LabelControl
            Me.LabelControl5 = New DevExpress.XtraEditors.LabelControl
            Me.LabelControl6 = New DevExpress.XtraEditors.LabelControl
            Me.CheckEdit_ok_to_post = New DevExpress.XtraEditors.CheckEdit
            Me.LookUpEdit_subtype = New DevExpress.XtraEditors.LookUpEdit
            Me.DateEdit_item_date = New DevExpress.XtraEditors.DateEdit
            Me.CalcEdit_amount = New DevExpress.XtraEditors.CalcEdit
            Me.TextEdit_reference = New DevExpress.XtraEditors.TextEdit
            Me.ClientID_client = New DebtPlus.UI.Client.Widgets.Controls.ClientID
            CType(Me.DxErrorProvider1, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.CheckEdit_ok_to_post.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.LookUpEdit_subtype.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.DateEdit_item_date.Properties.VistaTimeProperties, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.DateEdit_item_date.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.CalcEdit_amount.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.TextEdit_reference.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.ClientID_client.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
            Me.SuspendLayout()
            '
            'LabelControl1
            '
            Me.LabelControl1.Location = New System.Drawing.Point(13, 13)
            Me.LabelControl1.Name = "LabelControl1"
            Me.LabelControl1.Size = New System.Drawing.Size(89, 13)
            Me.LabelControl1.TabIndex = 0
            Me.LabelControl1.Text = "Scanned Client ID:"
            '
            'SimpleButton_OK
            '
            Me.SimpleButton_OK.Anchor = System.Windows.Forms.AnchorStyles.Bottom
            Me.SimpleButton_OK.DialogResult = System.Windows.Forms.DialogResult.OK
            Me.SimpleButton_OK.Enabled = False
            Me.SimpleButton_OK.Location = New System.Drawing.Point(57, 184)
            Me.SimpleButton_OK.Name = "SimpleButton_OK"
            Me.SimpleButton_OK.Size = New System.Drawing.Size(75, 24)
            Me.SimpleButton_OK.TabIndex = 13
            Me.SimpleButton_OK.Text = "&OK"
            '
            'SimpleButton_Cancel
            '
            Me.SimpleButton_Cancel.Anchor = System.Windows.Forms.AnchorStyles.Bottom
            Me.SimpleButton_Cancel.DialogResult = System.Windows.Forms.DialogResult.Cancel
            Me.SimpleButton_Cancel.Location = New System.Drawing.Point(157, 184)
            Me.SimpleButton_Cancel.Name = "SimpleButton_Cancel"
            Me.SimpleButton_Cancel.Size = New System.Drawing.Size(75, 24)
            Me.SimpleButton_Cancel.TabIndex = 14
            Me.SimpleButton_Cancel.Text = "&Cancel"
            '
            'LabelControl_scanned_client
            '
            Me.LabelControl_scanned_client.Location = New System.Drawing.Point(108, 13)
            Me.LabelControl_scanned_client.Name = "LabelControl_scanned_client"
            Me.LabelControl_scanned_client.Size = New System.Drawing.Size(0, 13)
            Me.LabelControl_scanned_client.TabIndex = 1
            Me.LabelControl_scanned_client.UseMnemonic = False
            '
            'LabelControl2
            '
            Me.LabelControl2.Location = New System.Drawing.Point(12, 44)
            Me.LabelControl2.Name = "LabelControl2"
            Me.LabelControl2.Size = New System.Drawing.Size(63, 13)
            Me.LabelControl2.TabIndex = 2
            Me.LabelControl2.Text = "Deposit Type"
            '
            'LabelControl3
            '
            Me.LabelControl3.Location = New System.Drawing.Point(12, 71)
            Me.LabelControl3.Name = "LabelControl3"
            Me.LabelControl3.Size = New System.Drawing.Size(27, 13)
            Me.LabelControl3.TabIndex = 4
            Me.LabelControl3.Text = "Client"
            '
            'LabelControl4
            '
            Me.LabelControl4.Location = New System.Drawing.Point(12, 96)
            Me.LabelControl4.Name = "LabelControl4"
            Me.LabelControl4.Size = New System.Drawing.Size(23, 13)
            Me.LabelControl4.TabIndex = 6
            Me.LabelControl4.Text = "Date"
            '
            'LabelControl5
            '
            Me.LabelControl5.Location = New System.Drawing.Point(12, 123)
            Me.LabelControl5.Name = "LabelControl5"
            Me.LabelControl5.Size = New System.Drawing.Size(37, 13)
            Me.LabelControl5.TabIndex = 8
            Me.LabelControl5.Text = "Amount"
            '
            'LabelControl6
            '
            Me.LabelControl6.Location = New System.Drawing.Point(12, 150)
            Me.LabelControl6.Name = "LabelControl6"
            Me.LabelControl6.Size = New System.Drawing.Size(50, 13)
            Me.LabelControl6.TabIndex = 10
            Me.LabelControl6.Text = "Reference"
            '
            'CheckEdit_ok_to_post
            '
            Me.CheckEdit_ok_to_post.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
            Me.CheckEdit_ok_to_post.Location = New System.Drawing.Point(12, 221)
            Me.CheckEdit_ok_to_post.Name = "CheckEdit_ok_to_post"
            Me.CheckEdit_ok_to_post.Properties.Caption = "Transaction Valid"
            Me.CheckEdit_ok_to_post.Size = New System.Drawing.Size(268, 19)
            Me.CheckEdit_ok_to_post.TabIndex = 12
            '
            'LookUpEdit_subtype
            '
            Me.LookUpEdit_subtype.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                        Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
            Me.LookUpEdit_subtype.Location = New System.Drawing.Point(108, 41)
            Me.LookUpEdit_subtype.Name = "LookUpEdit_subtype"
            Me.LookUpEdit_subtype.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
            Me.LookUpEdit_subtype.Properties.Columns.AddRange(New DevExpress.XtraEditors.Controls.LookUpColumnInfo() {New DevExpress.XtraEditors.Controls.LookUpColumnInfo("tran_type", "ID", 20, DevExpress.Utils.FormatType.None, "", False, DevExpress.Utils.HorzAlignment.[Default]), New DevExpress.XtraEditors.Controls.LookUpColumnInfo("description", "Description", 20, DevExpress.Utils.FormatType.None, "", True, DevExpress.Utils.HorzAlignment.[Default], DevExpress.Data.ColumnSortOrder.Ascending)})
            Me.LookUpEdit_subtype.Properties.DisplayMember = "description"
            Me.LookUpEdit_subtype.Properties.NullText = ""
            Me.LookUpEdit_subtype.Properties.ShowFooter = False
            Me.LookUpEdit_subtype.Properties.ShowHeader = False
            Me.LookUpEdit_subtype.Properties.ValueMember = "tran_type"
            Me.LookUpEdit_subtype.Size = New System.Drawing.Size(172, 20)
            Me.LookUpEdit_subtype.TabIndex = 3
            Me.LookUpEdit_subtype.Properties.SortColumnIndex = 1
            '
            'DateEdit_item_date
            '
            Me.DateEdit_item_date.EditValue = Nothing
            Me.DateEdit_item_date.Location = New System.Drawing.Point(108, 93)
            Me.DateEdit_item_date.Name = "DateEdit_item_date"
            Me.DateEdit_item_date.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
            Me.DateEdit_item_date.Properties.VistaTimeProperties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton})
            Me.DateEdit_item_date.Size = New System.Drawing.Size(100, 20)
            Me.DateEdit_item_date.TabIndex = 7
            '
            'CalcEdit_amount
            '
            Me.CalcEdit_amount.Location = New System.Drawing.Point(108, 120)
            Me.CalcEdit_amount.Name = "CalcEdit_amount"
            Me.CalcEdit_amount.Properties.Appearance.Options.UseTextOptions = True
            Me.CalcEdit_amount.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
            Me.CalcEdit_amount.Properties.AppearanceDisabled.Options.UseTextOptions = True
            Me.CalcEdit_amount.Properties.AppearanceDisabled.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
            Me.CalcEdit_amount.Properties.AppearanceDropDown.Options.UseTextOptions = True
            Me.CalcEdit_amount.Properties.AppearanceDropDown.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
            Me.CalcEdit_amount.Properties.AppearanceFocused.Options.UseTextOptions = True
            Me.CalcEdit_amount.Properties.AppearanceFocused.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
            Me.CalcEdit_amount.Properties.AppearanceReadOnly.Options.UseTextOptions = True
            Me.CalcEdit_amount.Properties.AppearanceReadOnly.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
            Me.CalcEdit_amount.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
            Me.CalcEdit_amount.Properties.DisplayFormat.FormatString = "{0:c}"
            Me.CalcEdit_amount.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
            Me.CalcEdit_amount.Properties.EditFormat.FormatString = "{0:f2}"
            Me.CalcEdit_amount.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric
            Me.CalcEdit_amount.Properties.Mask.BeepOnError = True
            Me.CalcEdit_amount.Properties.Mask.EditMask = "c"
            Me.CalcEdit_amount.Properties.Precision = 2
            Me.CalcEdit_amount.Size = New System.Drawing.Size(100, 20)
            Me.CalcEdit_amount.TabIndex = 9
            '
            'TextEdit_reference
            '
            Me.TextEdit_reference.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                        Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
            Me.TextEdit_reference.Location = New System.Drawing.Point(108, 147)
            Me.TextEdit_reference.Name = "TextEdit_reference"
            Me.TextEdit_reference.Properties.MaxLength = 50
            Me.TextEdit_reference.Size = New System.Drawing.Size(172, 20)
            Me.TextEdit_reference.TabIndex = 11
            '
            'ClientID_client
            '
            Me.ClientID_client.Location = New System.Drawing.Point(108, 68)
            Me.ClientID_client.Name = "ClientID_client"
            Me.ClientID_client.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[False]
            Me.ClientID_client.Properties.Appearance.Options.UseTextOptions = True
            Me.ClientID_client.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
            Me.ClientID_client.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph, "", -1, True, True, False, DevExpress.XtraEditors.ImageLocation.MiddleCenter, CType(resources.GetObject("ClientID1.Properties.Buttons"), System.Drawing.Image), New DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), SerializableAppearanceObject1, "Click here to search for a client ID", "search", Nothing, True)})
            Me.ClientID_client.Properties.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
            Me.ClientID_client.Properties.DisplayFormat.FormatString = "0000000"
            Me.ClientID_client.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Custom
            Me.ClientID_client.Properties.EditFormat.FormatString = "f0"
            Me.ClientID_client.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric
            Me.ClientID_client.Properties.Mask.BeepOnError = True
            Me.ClientID_client.Properties.Mask.EditMask = "\d*"
            Me.ClientID_client.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.RegEx
            Me.ClientID_client.Properties.ValidateOnEnterKey = True
            Me.ClientID_client.Size = New System.Drawing.Size(100, 20)
            Me.ClientID_client.TabIndex = 5
            '
            'EditForm
            '
            Me.AcceptButton = Me.SimpleButton_OK
            Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
            Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
            Me.CancelButton = Me.SimpleButton_Cancel
            Me.ClientSize = New System.Drawing.Size(292, 252)
            Me.Controls.Add(Me.ClientID_client)
            Me.Controls.Add(Me.TextEdit_reference)
            Me.Controls.Add(Me.CalcEdit_amount)
            Me.Controls.Add(Me.DateEdit_item_date)
            Me.Controls.Add(Me.LookUpEdit_subtype)
            Me.Controls.Add(Me.CheckEdit_ok_to_post)
            Me.Controls.Add(Me.LabelControl6)
            Me.Controls.Add(Me.LabelControl5)
            Me.Controls.Add(Me.LabelControl4)
            Me.Controls.Add(Me.LabelControl3)
            Me.Controls.Add(Me.LabelControl2)
            Me.Controls.Add(Me.LabelControl_scanned_client)
            Me.Controls.Add(Me.SimpleButton_Cancel)
            Me.Controls.Add(Me.SimpleButton_OK)
            Me.Controls.Add(Me.LabelControl1)
            Me.Name = "EditForm"
            Me.Text = "EditForm"
            CType(Me.DxErrorProvider1, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.CheckEdit_ok_to_post.Properties, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.LookUpEdit_subtype.Properties, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.DateEdit_item_date.Properties.VistaTimeProperties, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.DateEdit_item_date.Properties, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.CalcEdit_amount.Properties, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.TextEdit_reference.Properties, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.ClientID_client.Properties, System.ComponentModel.ISupportInitialize).EndInit()
            Me.ResumeLayout(False)
            Me.PerformLayout()

        End Sub
        Friend WithEvents LabelControl1 As DevExpress.XtraEditors.LabelControl
        Friend WithEvents SimpleButton_OK As DevExpress.XtraEditors.SimpleButton
        Friend WithEvents SimpleButton_Cancel As DevExpress.XtraEditors.SimpleButton
        Friend WithEvents LabelControl_scanned_client As DevExpress.XtraEditors.LabelControl
        Friend WithEvents LabelControl2 As DevExpress.XtraEditors.LabelControl
        Friend WithEvents LabelControl3 As DevExpress.XtraEditors.LabelControl
        Friend WithEvents LabelControl4 As DevExpress.XtraEditors.LabelControl
        Friend WithEvents LabelControl5 As DevExpress.XtraEditors.LabelControl
        Friend WithEvents LabelControl6 As DevExpress.XtraEditors.LabelControl
        Friend WithEvents CheckEdit_ok_to_post As DevExpress.XtraEditors.CheckEdit
        Friend WithEvents LookUpEdit_subtype As DevExpress.XtraEditors.LookUpEdit
        Friend WithEvents DateEdit_item_date As DevExpress.XtraEditors.DateEdit
        Friend WithEvents CalcEdit_amount As DevExpress.XtraEditors.CalcEdit
        Friend WithEvents TextEdit_reference As DevExpress.XtraEditors.TextEdit
        Friend WithEvents ClientID_client As DebtPlus.UI.Client.Widgets.Controls.ClientID
    End Class
End Namespace