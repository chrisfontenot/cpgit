﻿Namespace Deposit.Edit

    <Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
    Partial Class EditControl
        Inherits DevExpress.XtraEditors.XtraUserControl

        'UserControl overrides dispose to clean up the component list.
        <System.Diagnostics.DebuggerNonUserCode()> _
        Protected Overrides Sub Dispose(ByVal disposing As Boolean)
            Try
                If disposing AndAlso components IsNot Nothing Then
                    components.Dispose()
                End If
            Finally
                MyBase.Dispose(disposing)
            End Try
        End Sub

        'Required by the Windows Form Designer
        Private components As System.ComponentModel.IContainer

        'NOTE: The following procedure is required by the Windows Form Designer
        'It can be modified using the Windows Form Designer.  
        'Do not modify it using the code editor.
        <System.Diagnostics.DebuggerStepThrough()> _
        Private Sub InitializeComponent()
            Me.components = New System.ComponentModel.Container()
            Me.SimpleButton_OK = New DevExpress.XtraEditors.SimpleButton()
            Me.SimpleButton_Cancel = New DevExpress.XtraEditors.SimpleButton()
            Me.GridControl1 = New DevExpress.XtraGrid.GridControl()
            Me.GridView1 = New DevExpress.XtraGrid.Views.Grid.GridView()
            Me.GridColumn_type = New DevExpress.XtraGrid.Columns.GridColumn()
            Me.GridColumn2 = New DevExpress.XtraGrid.Columns.GridColumn()
            Me.GridColumn_scanned_client = New DevExpress.XtraGrid.Columns.GridColumn()
            Me.GridColumn_deposit_client = New DevExpress.XtraGrid.Columns.GridColumn()
            Me.GridColumn_deposit_amount = New DevExpress.XtraGrid.Columns.GridColumn()
            Me.GridColumn_item_date = New DevExpress.XtraGrid.Columns.GridColumn()
            Me.GridColumn_reference = New DevExpress.XtraGrid.Columns.GridColumn()
            Me.SimpleButton_Apply = New DevExpress.XtraEditors.SimpleButton()
            Me.barManager1 = New DevExpress.XtraBars.BarManager(Me.components)
            Me.barDockControlTop = New DevExpress.XtraBars.BarDockControl()
            Me.barDockControlBottom = New DevExpress.XtraBars.BarDockControl()
            Me.barDockControlLeft = New DevExpress.XtraBars.BarDockControl()
            Me.barDockControlRight = New DevExpress.XtraBars.BarDockControl()
            Me.BarButtonItem_PopupEdit = New DevExpress.XtraBars.BarButtonItem()
            Me.BarButtonItem_PopupDelete = New DevExpress.XtraBars.BarButtonItem()
            Me.PopupMenu_Items = New DevExpress.XtraBars.PopupMenu(Me.components)
            CType(Me.GridControl1, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.GridView1, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.barManager1, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.PopupMenu_Items, System.ComponentModel.ISupportInitialize).BeginInit()
            Me.SuspendLayout()
            '
            'SimpleButton_OK
            '
            Me.SimpleButton_OK.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
            Me.SimpleButton_OK.Location = New System.Drawing.Point(352, 13)
            Me.SimpleButton_OK.Name = "SimpleButton_OK"
            Me.SimpleButton_OK.Size = New System.Drawing.Size(75, 23)
            Me.SimpleButton_OK.TabIndex = 0
            Me.SimpleButton_OK.Text = "&Select"
            '
            'SimpleButton_Cancel
            '
            Me.SimpleButton_Cancel.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
            Me.SimpleButton_Cancel.Location = New System.Drawing.Point(352, 91)
            Me.SimpleButton_Cancel.Name = "SimpleButton_Cancel"
            Me.SimpleButton_Cancel.Size = New System.Drawing.Size(75, 23)
            Me.SimpleButton_Cancel.TabIndex = 1
            Me.SimpleButton_Cancel.Text = "&Cancel"
            '
            'GridControl1
            '
            Me.GridControl1.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                Or System.Windows.Forms.AnchorStyles.Left) _
                Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
            Me.GridControl1.Location = New System.Drawing.Point(4, 4)
            Me.GridControl1.MainView = Me.GridView1
            Me.GridControl1.Name = "GridControl1"
            Me.GridControl1.Size = New System.Drawing.Size(342, 143)
            Me.GridControl1.TabIndex = 2
            Me.GridControl1.ViewCollection.AddRange(New DevExpress.XtraGrid.Views.Base.BaseView() {Me.GridView1})
            '
            'GridView1
            '
            Me.GridView1.Columns.AddRange(New DevExpress.XtraGrid.Columns.GridColumn() {Me.GridColumn_type, Me.GridColumn2, Me.GridColumn_scanned_client, Me.GridColumn_deposit_client, Me.GridColumn_deposit_amount, Me.GridColumn_item_date, Me.GridColumn_reference})
            Me.GridView1.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus
            Me.GridView1.GridControl = Me.GridControl1
            Me.GridView1.Name = "GridView1"
            Me.GridView1.OptionsBehavior.Editable = False
            Me.GridView1.OptionsView.ShowGroupPanel = False
            Me.GridView1.OptionsView.ShowIndicator = False
            Me.GridView1.ShowButtonMode = DevExpress.XtraGrid.Views.Base.ShowButtonModeEnum.ShowForFocusedRow
            '
            'GridColumn_type
            '
            Me.GridColumn_type.AppearanceCell.Options.UseTextOptions = True
            Me.GridColumn_type.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center
            Me.GridColumn_type.AppearanceHeader.Options.UseTextOptions = True
            Me.GridColumn_type.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center
            Me.GridColumn_type.Caption = "TYP"
            Me.GridColumn_type.FieldName = "subtype"
            Me.GridColumn_type.Name = "GridColumn_type"
            Me.GridColumn_type.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.[True]
            Me.GridColumn_type.Visible = True
            Me.GridColumn_type.VisibleIndex = 0
            '
            'GridColumn2
            '
            Me.GridColumn2.AppearanceCell.Options.UseTextOptions = True
            Me.GridColumn2.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center
            Me.GridColumn2.AppearanceHeader.Options.UseTextOptions = True
            Me.GridColumn2.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center
            Me.GridColumn2.Caption = "V"
            Me.GridColumn2.FieldName = "formatted_ok_to_post"
            Me.GridColumn2.Name = "GridColumn2"
            Me.GridColumn2.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.[True]
            Me.GridColumn2.Visible = True
            Me.GridColumn2.VisibleIndex = 1
            '
            'GridColumn_scanned_client
            '
            Me.GridColumn_scanned_client.Caption = "Scanned"
            Me.GridColumn_scanned_client.FieldName = "scanned_client"
            Me.GridColumn_scanned_client.Name = "GridColumn_scanned_client"
            Me.GridColumn_scanned_client.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.[True]
            Me.GridColumn_scanned_client.Visible = True
            Me.GridColumn_scanned_client.VisibleIndex = 2
            '
            'GridColumn_deposit_client
            '
            Me.GridColumn_deposit_client.AppearanceCell.Options.UseTextOptions = True
            Me.GridColumn_deposit_client.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
            Me.GridColumn_deposit_client.AppearanceHeader.Options.UseTextOptions = True
            Me.GridColumn_deposit_client.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
            Me.GridColumn_deposit_client.Caption = "Client"
            Me.GridColumn_deposit_client.FieldName = "deposit_client"
            Me.GridColumn_deposit_client.Name = "GridColumn_deposit_client"
            Me.GridColumn_deposit_client.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.[True]
            Me.GridColumn_deposit_client.Visible = True
            Me.GridColumn_deposit_client.VisibleIndex = 3
            '
            'GridColumn_deposit_amount
            '
            Me.GridColumn_deposit_amount.AppearanceCell.Options.UseTextOptions = True
            Me.GridColumn_deposit_amount.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
            Me.GridColumn_deposit_amount.AppearanceHeader.Options.UseTextOptions = True
            Me.GridColumn_deposit_amount.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
            Me.GridColumn_deposit_amount.Caption = "Amount"
            Me.GridColumn_deposit_amount.DisplayFormat.FormatString = "{0:c}"
            Me.GridColumn_deposit_amount.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
            Me.GridColumn_deposit_amount.FieldName = "amount"
            Me.GridColumn_deposit_amount.GroupFormat.FormatString = "{0:c}"
            Me.GridColumn_deposit_amount.GroupFormat.FormatType = DevExpress.Utils.FormatType.Numeric
            Me.GridColumn_deposit_amount.Name = "GridColumn_deposit_amount"
            Me.GridColumn_deposit_amount.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.[True]
            Me.GridColumn_deposit_amount.Visible = True
            Me.GridColumn_deposit_amount.VisibleIndex = 4
            '
            'GridColumn_item_date
            '
            Me.GridColumn_item_date.AppearanceCell.Options.UseTextOptions = True
            Me.GridColumn_item_date.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
            Me.GridColumn_item_date.AppearanceHeader.Options.UseTextOptions = True
            Me.GridColumn_item_date.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
            Me.GridColumn_item_date.Caption = "Date"
            Me.GridColumn_item_date.DisplayFormat.FormatString = "d"
            Me.GridColumn_item_date.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime
            Me.GridColumn_item_date.FieldName = "item_date"
            Me.GridColumn_item_date.GroupFormat.FormatString = "d"
            Me.GridColumn_item_date.GroupFormat.FormatType = DevExpress.Utils.FormatType.DateTime
            Me.GridColumn_item_date.Name = "GridColumn_item_date"
            Me.GridColumn_item_date.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.[True]
            Me.GridColumn_item_date.Visible = True
            Me.GridColumn_item_date.VisibleIndex = 5
            '
            'GridColumn_reference
            '
            Me.GridColumn_reference.Caption = "Reference"
            Me.GridColumn_reference.FieldName = "reference"
            Me.GridColumn_reference.Name = "GridColumn_reference"
            Me.GridColumn_reference.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.[True]
            Me.GridColumn_reference.Visible = True
            Me.GridColumn_reference.VisibleIndex = 6
            '
            'SimpleButton_Apply
            '
            Me.SimpleButton_Apply.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
            Me.SimpleButton_Apply.Location = New System.Drawing.Point(352, 42)
            Me.SimpleButton_Apply.Name = "SimpleButton_Apply"
            Me.SimpleButton_Apply.Size = New System.Drawing.Size(75, 23)
            Me.SimpleButton_Apply.TabIndex = 3
            Me.SimpleButton_Apply.Text = "&Apply"
            '
            'barManager1
            '
            Me.barManager1.DockControls.Add(Me.barDockControlTop)
            Me.barManager1.DockControls.Add(Me.barDockControlBottom)
            Me.barManager1.DockControls.Add(Me.barDockControlLeft)
            Me.barManager1.DockControls.Add(Me.barDockControlRight)
            Me.barManager1.Form = Me
            Me.barManager1.Items.AddRange(New DevExpress.XtraBars.BarItem() {Me.BarButtonItem_PopupEdit, Me.BarButtonItem_PopupDelete})
            Me.barManager1.MaxItemId = 13
            '
            'barDockControlTop
            '
            Me.barDockControlTop.CausesValidation = False
            Me.barDockControlTop.Dock = System.Windows.Forms.DockStyle.Top
            Me.barDockControlTop.Location = New System.Drawing.Point(0, 0)
            Me.barDockControlTop.Size = New System.Drawing.Size(439, 0)
            '
            'barDockControlBottom
            '
            Me.barDockControlBottom.CausesValidation = False
            Me.barDockControlBottom.Dock = System.Windows.Forms.DockStyle.Bottom
            Me.barDockControlBottom.Location = New System.Drawing.Point(0, 150)
            Me.barDockControlBottom.Size = New System.Drawing.Size(439, 0)
            '
            'barDockControlLeft
            '
            Me.barDockControlLeft.CausesValidation = False
            Me.barDockControlLeft.Dock = System.Windows.Forms.DockStyle.Left
            Me.barDockControlLeft.Location = New System.Drawing.Point(0, 0)
            Me.barDockControlLeft.Size = New System.Drawing.Size(0, 150)
            '
            'barDockControlRight
            '
            Me.barDockControlRight.CausesValidation = False
            Me.barDockControlRight.Dock = System.Windows.Forms.DockStyle.Right
            Me.barDockControlRight.Location = New System.Drawing.Point(439, 0)
            Me.barDockControlRight.Size = New System.Drawing.Size(0, 150)
            '
            'BarButtonItem_PopupEdit
            '
            Me.BarButtonItem_PopupEdit.Caption = "&Edit..."
            Me.BarButtonItem_PopupEdit.Id = 11
            Me.BarButtonItem_PopupEdit.Name = "BarButtonItem_PopupEdit"
            Me.BarButtonItem_PopupEdit.Description = "Change the item in the list"
            '
            'BarButtonItem_PopupDelete
            '
            Me.BarButtonItem_PopupDelete.Description = "Remove the item from the list"
            Me.BarButtonItem_PopupDelete.Caption = "&Delete..."
            Me.BarButtonItem_PopupDelete.Id = 12
            Me.BarButtonItem_PopupDelete.Name = "BarButtonItem_PopupDelete"
            '
            'PopupMenu_Items
            '
            Me.PopupMenu_Items.LinksPersistInfo.AddRange(New DevExpress.XtraBars.LinkPersistInfo() {New DevExpress.XtraBars.LinkPersistInfo(Me.BarButtonItem_PopupEdit), New DevExpress.XtraBars.LinkPersistInfo(Me.BarButtonItem_PopupDelete)})
            Me.PopupMenu_Items.Manager = Me.barManager1
            Me.PopupMenu_Items.MenuCaption = "Right Click Menus"
            Me.PopupMenu_Items.Name = "PopupMenu_Items"
            '
            'EditControl
            '
            Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
            Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
            Me.Controls.Add(Me.SimpleButton_Apply)
            Me.Controls.Add(Me.GridControl1)
            Me.Controls.Add(Me.SimpleButton_Cancel)
            Me.Controls.Add(Me.SimpleButton_OK)
            Me.Controls.Add(Me.barDockControlLeft)
            Me.Controls.Add(Me.barDockControlRight)
            Me.Controls.Add(Me.barDockControlBottom)
            Me.Controls.Add(Me.barDockControlTop)
            Me.Name = "EditControl"
            Me.Size = New System.Drawing.Size(439, 150)
            CType(Me.GridControl1, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.GridView1, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.barManager1, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.PopupMenu_Items, System.ComponentModel.ISupportInitialize).EndInit()
            Me.ResumeLayout(False)

        End Sub
        Friend WithEvents SimpleButton_OK As DevExpress.XtraEditors.SimpleButton
        Friend WithEvents SimpleButton_Cancel As DevExpress.XtraEditors.SimpleButton
        Friend WithEvents GridControl1 As DevExpress.XtraGrid.GridControl
        Friend WithEvents GridView1 As DevExpress.XtraGrid.Views.Grid.GridView
        Friend WithEvents GridColumn_type As DevExpress.XtraGrid.Columns.GridColumn
        Friend WithEvents GridColumn2 As DevExpress.XtraGrid.Columns.GridColumn
        Friend WithEvents GridColumn_deposit_amount As DevExpress.XtraGrid.Columns.GridColumn
        Friend WithEvents GridColumn_scanned_client As DevExpress.XtraGrid.Columns.GridColumn
        Friend WithEvents GridColumn_deposit_client As DevExpress.XtraGrid.Columns.GridColumn
        Friend WithEvents GridColumn_item_date As DevExpress.XtraGrid.Columns.GridColumn
        Friend WithEvents GridColumn_reference As DevExpress.XtraGrid.Columns.GridColumn
        Friend WithEvents SimpleButton_Apply As DevExpress.XtraEditors.SimpleButton
        Protected Friend WithEvents barManager1 As DevExpress.XtraBars.BarManager
        Protected Friend WithEvents barDockControlTop As DevExpress.XtraBars.BarDockControl
        Protected Friend WithEvents barDockControlBottom As DevExpress.XtraBars.BarDockControl
        Protected Friend WithEvents barDockControlLeft As DevExpress.XtraBars.BarDockControl
        Protected Friend WithEvents barDockControlRight As DevExpress.XtraBars.BarDockControl
        Friend WithEvents BarButtonItem_PopupEdit As DevExpress.XtraBars.BarButtonItem
        Friend WithEvents BarButtonItem_PopupDelete As DevExpress.XtraBars.BarButtonItem
        Protected WithEvents PopupMenu_Items As DevExpress.XtraBars.PopupMenu
    End Class
End Namespace