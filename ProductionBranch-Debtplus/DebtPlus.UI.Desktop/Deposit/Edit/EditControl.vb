#Region "Copyright 2000-2012 DebtPlus, L.L.C."
'{*******************************************************************}
'{                                                                   }
'{       DebtPlus Debt Management System                             }
'{                                                                   }
'{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
'{       ALL RIGHTS RESERVED                                         }
'{                                                                   }
'{   The entire contents of this file is protected by U.S. and       }
'{   International Copyright Laws. Unauthorized reproduction,        }
'{   reverse-engineering, and distribution of all or any portion of  }
'{   the code contained in this file is strictly prohibited and may  }
'{   result in severe civil and criminal penalties and will be       }
'{   prosecuted to the maximum extent possible under the law.        }
'{                                                                   }
'{   RESTRICTIONS                                                    }
'{                                                                   }
'{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
'{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
'{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
'{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
'{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
'{                                                                   }
'{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
'{   ADDITIONAL RESTRICTIONS.                                        }
'{                                                                   }
'{*******************************************************************}
#End Region
Imports System.Data.SqlClient
Imports System.Windows.Forms

Namespace Deposit.Edit
    Friend Class EditControl
        Implements MyControls

        Public Sub New()
            MyBase.New()
            InitializeComponent()
            AddHandler BarButtonItem_PopupEdit.ItemClick, AddressOf BarButtonItem1_ItemClick
            AddHandler SimpleButton_OK.Click, AddressOf SimpleButton_OK_Click
            AddHandler SimpleButton_Cancel.Click, AddressOf SimpleButton_Cancel_Click
            AddHandler GridView1.MouseUp, AddressOf GridView1_MouseUp
            AddHandler PopupMenu_Items.Popup, AddressOf PopupMenu_Items_Popup
            AddHandler GridView1.Click, AddressOf GridView1_Click
            AddHandler GridView1.DoubleClick, AddressOf GridView1_DoubleClick
            AddHandler GridView1.FocusedRowChanged, AddressOf GridView1_FocusedRowChanged
            AddHandler SimpleButton_Apply.Click, AddressOf SimpleButton_Apply_Click
            AddHandler BarButtonItem_PopupDelete.ItemClick, AddressOf BarButtonItem_PopupDelete_ItemClick
        End Sub

        ''' <summary>
        ''' Event tripped when the CANCEL button is pressed
        ''' </summary>
        ''' <remarks></remarks>
        Public Event Cancelled As EventHandler

        ''' <summary>
        ''' Generate the cancel event
        ''' </summary>
        ''' <remarks></remarks>
        Protected Sub RaiseCancelled()
            RaiseEvent Cancelled(Me, EventArgs.Empty)
        End Sub

        ''' <summary>
        ''' Retrieve the current batch number from the parent
        ''' </summary>
        ''' <value></value>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Protected ReadOnly Property Batch() As Int32
            Get
                Return CType(ParentForm, Form_DepositEdit).ap.Batch
            End Get
        End Property

        Private ds As New DataSet("ds")

        ''' <summary>
        ''' Read the form when we are first loaded
        ''' </summary>
        ''' <remarks></remarks>
        Public Sub ReadForm() Implements MyControls.ReadForm
            ds.Clear()

            Using cmd As SqlClient.SqlCommand = New SqlCommand
                With cmd
                    .Connection = New SqlConnection(DebtPlus.LINQ.SQLInfoClass.getDefault().ConnectionString)
                    .CommandText = "rpt_deposit_detail_ByBatch"
                    .CommandType = CommandType.StoredProcedure
                    .Parameters.Add("@deposit_batch_id", SqlDbType.Int).Value = Batch
                End With

                Using da As New SqlClient.SqlDataAdapter(cmd)
                    da.Fill(ds, "deposit_details")
                End Using
            End Using

            Dim tbl As DataTable = ds.Tables("deposit_details")
            With tbl
                .PrimaryKey = New DataColumn() {.Columns("deposit_batch_detail")}
                With .Columns("deposit_batch_detail")
                    .AutoIncrement = True
                    .AutoIncrementSeed = -1
                    .AutoIncrementStep = -1
                End With

                If Not .Columns.Contains("formatted_ok_to_post") Then
                    .Columns.Add("formatted_ok_to_post", GetType(String), "iif([ok_to_post]<>0,'Y','N')")
                End If
            End With

            GridControl1.DataSource = tbl.DefaultView
            GridControl1.RefreshDataSource()
            GridView1.BestFitColumns()

            With CType(ParentForm, Form_DepositEdit)
                .BarButtonItem_File_Print.Enabled = True
            End With
        End Sub

        ''' <summary>
        ''' When the cancel buttin is clicked, tell the owner.
        ''' </summary>
        ''' <param name="sender"></param>
        ''' <param name="e"></param>
        ''' <remarks></remarks>
        Private Sub SimpleButton_Cancel_Click(ByVal sender As Object, ByVal e As EventArgs)
            If SaveForm() Then RaiseCancelled()
        End Sub

        ''' <summary>
        ''' Handle the MOUSE-UP event on the grid to show the popup menus
        ''' </summary>
        ''' <param name="sender"></param>
        ''' <param name="e"></param>
        ''' <remarks></remarks>
        Private Sub GridView1_MouseUp(ByVal sender As Object, ByVal e As MouseEventArgs)
            Dim hi As DevExpress.XtraGrid.Views.Grid.ViewInfo.GridHitInfo = GridView1.CalcHitInfo((GridControl1.PointToClient(Windows.Forms.Control.MousePosition)))

            If hi.InRow Then
                Dim RowHandle As Int32 = hi.RowHandle
                GridView1.FocusedRowHandle = RowHandle
                SimpleButton_OK.Enabled = GridView1.FocusedRowHandle >= 0
            Else
                GridView1.FocusedRowHandle = -1
                SimpleButton_OK.Enabled = False
            End If

            If e.Button = MouseButtons.Right Then
                PopupMenu_Items.ShowPopup(Control.MousePosition)
            End If
        End Sub

        ''' <summary>
        ''' When the popup menu is being shown, enable or dsiable the items in the list.
        ''' </summary>
        ''' <param name="sender"></param>
        ''' <param name="e"></param>
        ''' <remarks></remarks>
        Protected Overridable Sub PopupMenu_Items_Popup(ByVal sender As Object, ByVal e As EventArgs)
            BarButtonItem_PopupDelete.Enabled = SimpleButton_OK.Enabled
            BarButtonItem_PopupEdit.Enabled = SimpleButton_OK.Enabled
        End Sub

        ''' <summary>
        ''' Handle the click event on the grid to enable the OK button as needed
        ''' </summary>
        ''' <param name="sender"></param>
        ''' <param name="e"></param>
        ''' <remarks></remarks>
        Private Sub GridView1_Click(ByVal sender As Object, ByVal e As EventArgs)
            Dim hi As DevExpress.XtraGrid.Views.Grid.ViewInfo.GridHitInfo = GridView1.CalcHitInfo((GridControl1.PointToClient(Windows.Forms.Control.MousePosition)))

            If hi.InRow Then
                Dim RowHandle As Int32 = hi.RowHandle
                GridView1.FocusedRowHandle = RowHandle
                SimpleButton_OK.Enabled = GridView1.FocusedRowHandle >= 0
            Else
                GridView1.FocusedRowHandle = -1
                SimpleButton_OK.Enabled = False
            End If
        End Sub

        ''' <summary>
        ''' Process the edit operation when the row is double-clicked
        ''' </summary>
        ''' <param name="sender"></param>
        ''' <param name="e"></param>
        ''' <remarks></remarks>
        Private Sub GridView1_DoubleClick(ByVal sender As Object, ByVal e As EventArgs)
            GridView1_Click(sender, e)
            If SimpleButton_OK.Enabled Then SimpleButton_OK.PerformClick()
        End Sub

        ''' <summary>
        ''' Process the edit operaiton when the OK button is clicked
        ''' </summary>
        ''' <param name="sender"></param>
        ''' <param name="e"></param>
        ''' <remarks></remarks>
        Private Sub SimpleButton_OK_Click(ByVal sender As Object, ByVal e As EventArgs)
            Dim SelectedRow As Int32 = GridView1.FocusedRowHandle
            If SelectedRow >= 0 Then EditRowHandle(SelectedRow)
        End Sub

        ''' <summary>
        ''' Edit the row given the handle to the row in the list
        ''' </summary>
        ''' <param name="RowHandle">Row handle to the list (0 relative)</param>
        ''' <remarks></remarks>
        Private Sub EditRowHandle(ByVal RowHandle As Int32)
            If RowHandle >= 0 Then
                Dim drv As DataRowView = CType(GridView1.GetRow(RowHandle), DataRowView)
                If drv IsNot Nothing Then EditRow(drv)
            End If
        End Sub

        ''' <summary>
        ''' Do an edit operation on the database row
        ''' </summary>
        ''' <param name="drv">Pointer to the row to be edited</param>
        ''' <remarks></remarks>
        Private Sub EditRow(ByVal drv As DataRowView)
            drv.BeginEdit()
            With New EditForm(drv)
                If .ShowDialog() = DialogResult.OK Then
                    Try
                        drv.EndEdit()
                        SimpleButton_Apply.Enabled = True

                    Catch ex As DataException
                        DebtPlus.Data.Forms.MessageBox.Show(ex.Message, "Transaction Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
                        drv.CancelEdit()
                    End Try
                Else
                    drv.CancelEdit()
                End If
                .Dispose()
            End With

        End Sub

        ''' <summary>
        ''' When the focused row changes, enable or disable the OK button.
        ''' </summary>
        ''' <param name="sender"></param>
        ''' <param name="e"></param>
        ''' <remarks></remarks>
        Private Sub GridView1_FocusedRowChanged(ByVal sender As Object, ByVal e As DevExpress.XtraGrid.Views.Base.FocusedRowChangedEventArgs)
            SimpleButton_OK.Enabled = (GridView1.FocusedRowHandle >= 0)
        End Sub

        ''' <summary>
        ''' Do the APPLY function to apply the changes to the database
        ''' </summary>
        ''' <param name="sender"></param>
        ''' <param name="e"></param>
        ''' <remarks></remarks>
        Private Sub SimpleButton_Apply_Click(ByVal sender As Object, ByVal e As EventArgs)
            UpdateDatabase()
            SimpleButton_Apply.Enabled = False
        End Sub

        ''' <summary>
        ''' Perform the physical update on the database pending items
        ''' </summary>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Private Function UpdateDatabase() As Boolean
            Dim answer As Boolean = False
            Dim tbl As DataTable = CType(GridControl1.DataSource, DataView).Table
            Dim UpdateCmd As SqlClient.SqlCommand = New SqlCommand
            Dim DeleteCmd As SqlClient.SqlCommand = New SqlCommand

            Try
                Using da As New SqlClient.SqlDataAdapter
                    With UpdateCmd
                        .Connection = New SqlConnection(DebtPlus.LINQ.SQLInfoClass.getDefault().ConnectionString)
                        .CommandText = "UPDATE deposit_batch_details SET client=@deposit_client, amount=@amount, reference=@reference, item_date=@item_date, ok_to_post=@ok_to_post, tran_subtype=@subtype WHERE deposit_batch_detail=@deposit_batch_detail"
                        .CommandType = CommandType.Text
                        .Parameters.Add("@deposit_client", SqlDbType.Int, 0, "deposit_client")
                        .Parameters.Add("@amount", SqlDbType.Decimal, 0, "amount")
                        .Parameters.Add("@reference", SqlDbType.VarChar, 50, "reference")
                        .Parameters.Add("@item_date", SqlDbType.DateTime, 0, "item_date")
                        .Parameters.Add("@ok_to_post", SqlDbType.Bit, 0, "ok_to_post")
                        .Parameters.Add("@subtype", SqlDbType.VarChar, 2, "subtype")
                        .Parameters.Add("@deposit_batch_detail", SqlDbType.Int, 0, "deposit_batch_detail")
                    End With
                    da.UpdateCommand = UpdateCmd

                    With DeleteCmd
                        .Connection = New SqlConnection(DebtPlus.LINQ.SQLInfoClass.getDefault().ConnectionString)
                        .CommandText = "DELETE FROM deposit_batch_details WHERE [deposit_batch_detail]=@deposit_batch_detail"
                        .CommandType = CommandType.Text
                        .Parameters.Add("@deposit_batch_detail", SqlDbType.Int, 0, "deposit_batch_detail")
                    End With
                    da.DeleteCommand = DeleteCmd

                    ' Do the update operation now
                    da.Update(tbl)
                End Using

                answer = True

            Catch ex As SqlClient.SqlException
                DebtPlus.UI.Common.ErrorHandling.HandleErrors(ex, "Error updating the database")

            Finally
                If UpdateCmd IsNot Nothing Then UpdateCmd.Dispose()
                If DeleteCmd IsNot Nothing Then DeleteCmd.Dispose()
            End Try

            Return answer
        End Function

        ''' <summary>
        ''' Save the form when the program terminates.
        ''' </summary>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public Function SaveForm() As Boolean Implements MyControls.SaveForm
            Dim answer As Boolean = True

            If HasChanges() Then
                Select Case DebtPlus.Data.Forms.MessageBox.Show(String.Format("You have pending changes to the database.{0}{0}Do you wish to post these updates before you close the form?", Environment.NewLine), "Pending Changes", MessageBoxButtons.YesNoCancel, MessageBoxIcon.Information, MessageBoxDefaultButton.Button1)
                    Case DialogResult.Yes
                        If Not UpdateDatabase() Then answer = False

                    Case DialogResult.No
                        ' Just ignore the updates

                    Case DialogResult.Cancel
                        answer = False
                End Select
            End If

            Return answer
        End Function

        ''' <summary>
        ''' Determine if there are pending changes to the database. They may be updates or deletes.
        ''' </summary>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Private Function HasChanges() As Boolean
            Using vue As New DataView(CType(GridControl1.DataSource, DataView).Table, String.Empty, String.Empty, DataViewRowState.Deleted)
                If vue.Count > 0 Then Return True
            End Using

            Using vue As New DataView(CType(GridControl1.DataSource, DataView).Table, String.Empty, String.Empty, DataViewRowState.ModifiedCurrent)
                If vue.Count > 0 Then Return True
            End Using

            Return False
        End Function

        ''' <summary>
        ''' Do the edit operation on the row if you rightclick and choose edit
        ''' </summary>
        ''' <param name="sender"></param>
        ''' <param name="e"></param>
        ''' <remarks></remarks>
        Private Sub BarButtonItem1_ItemClick(sender As System.Object, e As DevExpress.XtraBars.ItemClickEventArgs)
            Dim SelectedRow As Int32 = GridView1.FocusedRowHandle
            If SelectedRow >= 0 Then EditRowHandle(SelectedRow)
        End Sub

        ''' <summary>
        ''' Do the delete operation on the row if you rightclick on the row and choose delete.
        ''' </summary>
        ''' <param name="sender"></param>
        ''' <param name="e"></param>
        ''' <remarks></remarks>
        Private Sub BarButtonItem_PopupDelete_ItemClick(sender As Object, e As DevExpress.XtraBars.ItemClickEventArgs)
            Dim SelectedRow As Int32 = GridView1.FocusedRowHandle
            Dim drv As DataRowView = CType(GridView1.GetRow(SelectedRow), DataRowView)
            If drv IsNot Nothing Then
                If DebtPlus.Data.Forms.MessageBox.Show("Are you sure that you wish to delete this item?", "Are you sure?", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2) = DialogResult.Yes Then
                    drv.Delete()
                End If
            End If
        End Sub
    End Class
End Namespace