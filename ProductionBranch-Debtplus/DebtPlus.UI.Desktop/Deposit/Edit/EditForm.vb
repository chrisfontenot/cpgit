#Region "Copyright 2000-2012 DebtPlus, L.L.C."
'{*******************************************************************}
'{                                                                   }
'{       DebtPlus Debt Management System                             }
'{                                                                   }
'{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
'{       ALL RIGHTS RESERVED                                         }
'{                                                                   }
'{   The entire contents of this file is protected by U.S. and       }
'{   International Copyright Laws. Unauthorized reproduction,        }
'{   reverse-engineering, and distribution of all or any portion of  }
'{   the code contained in this file is strictly prohibited and may  }
'{   result in severe civil and criminal penalties and will be       }
'{   prosecuted to the maximum extent possible under the law.        }
'{                                                                   }
'{   RESTRICTIONS                                                    }
'{                                                                   }
'{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
'{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
'{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
'{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
'{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
'{                                                                   }
'{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
'{   ADDITIONAL RESTRICTIONS.                                        }
'{                                                                   }
'{*******************************************************************}
#End Region
Imports System.Data.SqlClient

Namespace Deposit.Edit
    Friend Class EditForm
        Private ReadOnly drv As DataRowView

        Public Sub New()
            MyClass.New(Nothing)
        End Sub

        Public Sub New(ByVal drv As DataRowView)
            MyBase.New()
            InitializeComponent()
            AddHandler Me.Load, AddressOf EditForm_Load
            AddHandler SimpleButton_OK.Click, AddressOf SimpleButton_OK_Click
            Me.drv = drv
        End Sub

        Private Sub EditForm_Load(ByVal sender As Object, ByVal e As EventArgs)
            Dim tbl As DataTable = dsTranTypes.Tables("tran_types")

            If tbl Is Nothing Then
                Using cmd As SqlClient.SqlCommand = New SqlCommand
                    With cmd
                        .Connection = New SqlConnection(DebtPlus.LINQ.SQLInfoClass.getDefault().ConnectionString)
                        .CommandText = "SELECT tran_type, description FROM tran_types WHERE deposit_subtype = 1 ORDER BY sort_order, description"
                        .CommandType = CommandType.Text
                    End With

                    Using da As New SqlClient.SqlDataAdapter(cmd)
                        da.Fill(dsTranTypes, "tran_types")
                        tbl = dsTranTypes.Tables("tran_types")
                    End Using
                End Using
            End If

            LookUpEdit_subtype.Properties.DataSource = tbl.DefaultView

            ' Bind the items to the display fields
            Dim ScannedClient As String
            If drv("scanned_client") IsNot Nothing AndAlso drv("scanned_client") IsNot DBNull.Value Then
                ScannedClient = Convert.ToString(drv("scanned_client"))
            Else
                ScannedClient = String.Empty
            End If
            LabelControl_scanned_client.Text = ScannedClient

            LookUpEdit_subtype.EditValue = drv("subtype")
            TextEdit_reference.EditValue = drv("reference")
            CalcEdit_amount.EditValue = DebtPlus.Utils.Nulls.v_Decimal(drv("amount"))
            DateEdit_item_date.EditValue = DebtPlus.Utils.Nulls.v_DateTime(drv("item_date"))
            ClientID_client.EditValue = DebtPlus.Utils.Nulls.v_Int32(drv("deposit_client"))
            CheckEdit_ok_to_post.Checked = Convert.ToBoolean(drv("ok_to_post"))

            AddHandler LookUpEdit_subtype.EditValueChanged, AddressOf FormChanged
            AddHandler CalcEdit_amount.EditValueChanged, AddressOf FormChanged
            AddHandler DateEdit_item_date.EditValueChanged, AddressOf FormChanged
            AddHandler TextEdit_reference.EditValueChanged, AddressOf FormChanged
            AddHandler ClientID_client.EditValueChanged, AddressOf FormChanged
            AddHandler CheckEdit_ok_to_post.EditValueChanged, AddressOf FormChanged
        End Sub

        Private Sub FormChanged(ByVal Sender As Object, ByVal e As EventArgs)
            SimpleButton_OK.Enabled = Not HasErrors()
        End Sub

        Private Function HasErrors() As Boolean
            Return False
        End Function

        Private Sub SimpleButton_OK_Click(sender As Object, e As System.EventArgs)

            drv("subtype") = LookUpEdit_subtype.EditValue
            drv("reference") = TextEdit_reference.EditValue
            drv("ok_to_post") = CheckEdit_ok_to_post.Checked

            drv("amount") = DebtPlus.Utils.Nulls.ToDbNull(CalcEdit_amount.EditValue)
            drv("item_date") = DebtPlus.Utils.Nulls.ToDbNull(DateEdit_item_date.EditValue)
            drv("deposit_client") = DebtPlus.Utils.Nulls.ToDbNull(ClientID_client.EditValue)

        End Sub
    End Class

    Friend Module Globals
        Friend dsTranTypes As New DataSet("dsTranTypes")
    End Module
End Namespace