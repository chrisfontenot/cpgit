#Region "Copyright 2000-2012 DebtPlus, L.L.C."
'{*******************************************************************}
'{                                                                   }
'{       DebtPlus Debt Management System                             }
'{                                                                   }
'{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
'{       ALL RIGHTS RESERVED                                         }
'{                                                                   }
'{   The entire contents of this file is protected by U.S. and       }
'{   International Copyright Laws. Unauthorized reproduction,        }
'{   reverse-engineering, and distribution of all or any portion of  }
'{   the code contained in this file is strictly prohibited and may  }
'{   result in severe civil and criminal penalties and will be       }
'{   prosecuted to the maximum extent possible under the law.        }
'{                                                                   }
'{   RESTRICTIONS                                                    }
'{                                                                   }
'{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
'{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
'{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
'{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
'{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
'{                                                                   }
'{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
'{   ADDITIONAL RESTRICTIONS.                                        }
'{                                                                   }
'{*******************************************************************}
#End Region

Option Compare Binary
Option Explicit On
Option Strict On

Imports DebtPlus.UI.FormLib.Deposit
Imports DebtPlus.Utils
Imports System.Data.SqlClient
Imports System.Windows.Forms

Namespace Deposit.Post

    Friend Class PostControl
        Inherits ClientDepositBatch

        Public Sub New()
            MyBase.New()
            InitializeComponent()
        End Sub

#Region " Windows Form Designer generated code "

        'Form overrides dispose to clean up the component list.
        Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
            If disposing Then
                If components IsNot Nothing Then
                    components.Dispose()
                End If
            End If
            MyBase.Dispose(disposing)
        End Sub

        'Required by the Windows Form Designer
        Private components As System.ComponentModel.IContainer

        'NOTE: The following procedure is required by the Windows Form Designer
        'It can be modified using the Windows Form Designer.  
        'Do not modify it using the code editor.
        <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
            CType(Me.GridControl1, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.GridView1, System.ComponentModel.ISupportInitialize).BeginInit()
            Me.SuspendLayout()
            '
            'GridColumn_deposit_batch_id
            '
            Me.GridColumn_deposit_batch_id.AppearanceCell.Options.UseTextOptions = True
            Me.GridColumn_deposit_batch_id.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
            Me.GridColumn_deposit_batch_id.AppearanceHeader.Options.UseTextOptions = True
            Me.GridColumn_deposit_batch_id.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
            Me.GridColumn_deposit_batch_id.OptionsColumn.AllowEdit = False
            '
            'GridColumn_amount
            '
            Me.GridColumn_amount.AppearanceCell.Options.UseTextOptions = True
            Me.GridColumn_amount.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
            Me.GridColumn_amount.AppearanceHeader.Options.UseTextOptions = True
            Me.GridColumn_amount.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
            Me.GridColumn_amount.DisplayFormat.FormatString = "c"
            Me.GridColumn_amount.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
            Me.GridColumn_amount.GroupFormat.FormatString = "c"
            Me.GridColumn_amount.GroupFormat.FormatType = DevExpress.Utils.FormatType.Numeric
            Me.GridColumn_amount.OptionsColumn.AllowEdit = False
            '
            'GridColumn_created_by
            '
            Me.GridColumn_created_by.OptionsColumn.AllowEdit = False
            '
            'GridColumn_date_created
            '
            Me.GridColumn_date_created.AppearanceCell.Options.UseTextOptions = True
            Me.GridColumn_date_created.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
            Me.GridColumn_date_created.AppearanceHeader.Options.UseTextOptions = True
            Me.GridColumn_date_created.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
            Me.GridColumn_date_created.DisplayFormat.FormatString = "d"
            Me.GridColumn_date_created.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime
            Me.GridColumn_date_created.GroupFormat.FormatString = "d"
            Me.GridColumn_date_created.GroupFormat.FormatType = DevExpress.Utils.FormatType.DateTime
            Me.GridColumn_date_created.OptionsColumn.AllowEdit = False
            '
            'LayoutControlItem2
            '
            Me.LayoutControlItem2.Text = "Please select the deposit batch or batches to be posted to the clients' account"
            '
            'GridControl1
            '
            '
            'GridControl1.EmbeddedNavigator
            '
            Me.GridControl1.EmbeddedNavigator.Name = ""
            Me.GridControl1.Name = "GridControl1"
            '
            'GridView1
            '
            Me.GridView1.Appearance.ColumnFilterButton.BackColor = System.Drawing.Color.FromArgb(CType(104, Byte), CType(184, Byte), CType(251, Byte))
            Me.GridView1.Appearance.ColumnFilterButton.BorderColor = System.Drawing.Color.FromArgb(CType(104, Byte), CType(184, Byte), CType(251, Byte))
            Me.GridView1.Appearance.ColumnFilterButton.ForeColor = System.Drawing.Color.White
            Me.GridView1.Appearance.ColumnFilterButton.Options.UseBackColor = True
            Me.GridView1.Appearance.ColumnFilterButton.Options.UseBorderColor = True
            Me.GridView1.Appearance.ColumnFilterButton.Options.UseForeColor = True
            Me.GridView1.Appearance.ColumnFilterButtonActive.BackColor = System.Drawing.Color.FromArgb(CType(170, Byte), CType(216, Byte), CType(254, Byte))
            Me.GridView1.Appearance.ColumnFilterButtonActive.BorderColor = System.Drawing.Color.FromArgb(CType(170, Byte), CType(216, Byte), CType(254, Byte))
            Me.GridView1.Appearance.ColumnFilterButtonActive.ForeColor = System.Drawing.Color.Black
            Me.GridView1.Appearance.ColumnFilterButtonActive.Options.UseBackColor = True
            Me.GridView1.Appearance.ColumnFilterButtonActive.Options.UseBorderColor = True
            Me.GridView1.Appearance.ColumnFilterButtonActive.Options.UseForeColor = True
            Me.GridView1.Appearance.Empty.BackColor = System.Drawing.Color.FromArgb(CType(236, Byte), CType(246, Byte), CType(255, Byte))
            Me.GridView1.Appearance.Empty.BackColor2 = System.Drawing.Color.White
            Me.GridView1.Appearance.Empty.Options.UseBackColor = True
            Me.GridView1.Appearance.EvenRow.BackColor = System.Drawing.Color.FromArgb(CType(247, Byte), CType(251, Byte), CType(255, Byte))
            Me.GridView1.Appearance.EvenRow.BorderColor = System.Drawing.Color.FromArgb(CType(247, Byte), CType(251, Byte), CType(255, Byte))
            Me.GridView1.Appearance.EvenRow.ForeColor = System.Drawing.Color.Black
            Me.GridView1.Appearance.EvenRow.Options.UseBackColor = True
            Me.GridView1.Appearance.EvenRow.Options.UseBorderColor = True
            Me.GridView1.Appearance.EvenRow.Options.UseForeColor = True
            Me.GridView1.Appearance.FilterCloseButton.BackColor = System.Drawing.Color.FromArgb(CType(104, Byte), CType(184, Byte), CType(251, Byte))
            Me.GridView1.Appearance.FilterCloseButton.BorderColor = System.Drawing.Color.FromArgb(CType(104, Byte), CType(184, Byte), CType(251, Byte))
            Me.GridView1.Appearance.FilterCloseButton.ForeColor = System.Drawing.Color.White
            Me.GridView1.Appearance.FilterCloseButton.Options.UseBackColor = True
            Me.GridView1.Appearance.FilterCloseButton.Options.UseBorderColor = True
            Me.GridView1.Appearance.FilterCloseButton.Options.UseForeColor = True
            Me.GridView1.Appearance.FilterPanel.BackColor = System.Drawing.Color.FromArgb(CType(236, Byte), CType(246, Byte), CType(255, Byte))
            Me.GridView1.Appearance.FilterPanel.BackColor2 = System.Drawing.Color.White
            Me.GridView1.Appearance.FilterPanel.ForeColor = System.Drawing.Color.Black
            Me.GridView1.Appearance.FilterPanel.Options.UseBackColor = True
            Me.GridView1.Appearance.FilterPanel.Options.UseForeColor = True
            Me.GridView1.Appearance.FixedLine.BackColor = System.Drawing.Color.FromArgb(CType(59, Byte), CType(133, Byte), CType(195, Byte))
            Me.GridView1.Appearance.FixedLine.Options.UseBackColor = True
            Me.GridView1.Appearance.FocusedCell.BackColor = System.Drawing.Color.White
            Me.GridView1.Appearance.FocusedCell.ForeColor = System.Drawing.Color.Black
            Me.GridView1.Appearance.FocusedCell.Options.UseBackColor = True
            Me.GridView1.Appearance.FocusedCell.Options.UseForeColor = True
            Me.GridView1.Appearance.FocusedRow.BackColor = System.Drawing.Color.FromArgb(CType(38, Byte), CType(109, Byte), CType(189, Byte))
            Me.GridView1.Appearance.FocusedRow.BorderColor = System.Drawing.Color.FromArgb(CType(59, Byte), CType(139, Byte), CType(206, Byte))
            Me.GridView1.Appearance.FocusedRow.ForeColor = System.Drawing.Color.White
            Me.GridView1.Appearance.FocusedRow.Options.UseBackColor = True
            Me.GridView1.Appearance.FocusedRow.Options.UseBorderColor = True
            Me.GridView1.Appearance.FocusedRow.Options.UseForeColor = True
            Me.GridView1.Appearance.FooterPanel.BackColor = System.Drawing.Color.FromArgb(CType(104, Byte), CType(184, Byte), CType(251, Byte))
            Me.GridView1.Appearance.FooterPanel.BorderColor = System.Drawing.Color.FromArgb(CType(104, Byte), CType(184, Byte), CType(251, Byte))
            Me.GridView1.Appearance.FooterPanel.ForeColor = System.Drawing.Color.Black
            Me.GridView1.Appearance.FooterPanel.Options.UseBackColor = True
            Me.GridView1.Appearance.FooterPanel.Options.UseBorderColor = True
            Me.GridView1.Appearance.FooterPanel.Options.UseForeColor = True
            Me.GridView1.Appearance.GroupButton.BackColor = System.Drawing.Color.FromArgb(CType(104, Byte), CType(184, Byte), CType(251, Byte))
            Me.GridView1.Appearance.GroupButton.BorderColor = System.Drawing.Color.FromArgb(CType(104, Byte), CType(184, Byte), CType(251, Byte))
            Me.GridView1.Appearance.GroupButton.Options.UseBackColor = True
            Me.GridView1.Appearance.GroupButton.Options.UseBorderColor = True
            Me.GridView1.Appearance.GroupFooter.BackColor = System.Drawing.Color.FromArgb(CType(170, Byte), CType(216, Byte), CType(254, Byte))
            Me.GridView1.Appearance.GroupFooter.BorderColor = System.Drawing.Color.FromArgb(CType(170, Byte), CType(216, Byte), CType(254, Byte))
            Me.GridView1.Appearance.GroupFooter.ForeColor = System.Drawing.Color.Black
            Me.GridView1.Appearance.GroupFooter.Options.UseBackColor = True
            Me.GridView1.Appearance.GroupFooter.Options.UseBorderColor = True
            Me.GridView1.Appearance.GroupFooter.Options.UseForeColor = True
            Me.GridView1.Appearance.GroupPanel.BackColor = System.Drawing.Color.FromArgb(CType(236, Byte), CType(246, Byte), CType(255, Byte))
            Me.GridView1.Appearance.GroupPanel.BackColor2 = System.Drawing.Color.White
            Me.GridView1.Appearance.GroupPanel.ForeColor = System.Drawing.Color.Black
            Me.GridView1.Appearance.GroupPanel.Options.UseBackColor = True
            Me.GridView1.Appearance.GroupPanel.Options.UseForeColor = True
            Me.GridView1.Appearance.GroupRow.BackColor = System.Drawing.Color.FromArgb(CType(170, Byte), CType(216, Byte), CType(254, Byte))
            Me.GridView1.Appearance.GroupRow.BorderColor = System.Drawing.Color.FromArgb(CType(170, Byte), CType(216, Byte), CType(254, Byte))
            Me.GridView1.Appearance.GroupRow.ForeColor = System.Drawing.Color.Black
            Me.GridView1.Appearance.GroupRow.Options.UseBackColor = True
            Me.GridView1.Appearance.GroupRow.Options.UseBorderColor = True
            Me.GridView1.Appearance.GroupRow.Options.UseForeColor = True
            Me.GridView1.Appearance.HeaderPanel.BackColor = System.Drawing.Color.FromArgb(CType(139, Byte), CType(201, Byte), CType(254, Byte))
            Me.GridView1.Appearance.HeaderPanel.BorderColor = System.Drawing.Color.FromArgb(CType(139, Byte), CType(201, Byte), CType(254, Byte))
            Me.GridView1.Appearance.HeaderPanel.ForeColor = System.Drawing.Color.Black
            Me.GridView1.Appearance.HeaderPanel.Options.UseBackColor = True
            Me.GridView1.Appearance.HeaderPanel.Options.UseBorderColor = True
            Me.GridView1.Appearance.HeaderPanel.Options.UseForeColor = True
            Me.GridView1.Appearance.HideSelectionRow.BackColor = System.Drawing.Color.FromArgb(CType(105, Byte), CType(170, Byte), CType(225, Byte))
            Me.GridView1.Appearance.HideSelectionRow.BorderColor = System.Drawing.Color.FromArgb(CType(83, Byte), CType(155, Byte), CType(215, Byte))
            Me.GridView1.Appearance.HideSelectionRow.ForeColor = System.Drawing.Color.FromArgb(CType(236, Byte), CType(246, Byte), CType(255, Byte))
            Me.GridView1.Appearance.HideSelectionRow.Options.UseBackColor = True
            Me.GridView1.Appearance.HideSelectionRow.Options.UseBorderColor = True
            Me.GridView1.Appearance.HideSelectionRow.Options.UseForeColor = True
            Me.GridView1.Appearance.HorzLine.BackColor = System.Drawing.Color.FromArgb(CType(104, Byte), CType(184, Byte), CType(251, Byte))
            Me.GridView1.Appearance.HorzLine.Options.UseBackColor = True
            Me.GridView1.Appearance.OddRow.BackColor = System.Drawing.Color.FromArgb(CType(236, Byte), CType(246, Byte), CType(255, Byte))
            Me.GridView1.Appearance.OddRow.BorderColor = System.Drawing.Color.FromArgb(CType(236, Byte), CType(246, Byte), CType(255, Byte))
            Me.GridView1.Appearance.OddRow.ForeColor = System.Drawing.Color.Black
            Me.GridView1.Appearance.OddRow.Options.UseBackColor = True
            Me.GridView1.Appearance.OddRow.Options.UseBorderColor = True
            Me.GridView1.Appearance.OddRow.Options.UseForeColor = True
            Me.GridView1.Appearance.Preview.Font = New System.Drawing.Font("Verdana", 7.5!)
            Me.GridView1.Appearance.Preview.ForeColor = System.Drawing.Color.FromArgb(CType(83, Byte), CType(155, Byte), CType(215, Byte))
            Me.GridView1.Appearance.Preview.Options.UseFont = True
            Me.GridView1.Appearance.Preview.Options.UseForeColor = True
            Me.GridView1.Appearance.Row.BackColor = System.Drawing.Color.FromArgb(CType(247, Byte), CType(251, Byte), CType(255, Byte))
            Me.GridView1.Appearance.Row.ForeColor = System.Drawing.Color.Black
            Me.GridView1.Appearance.Row.Options.UseBackColor = True
            Me.GridView1.Appearance.Row.Options.UseForeColor = True
            Me.GridView1.Appearance.RowSeparator.BackColor = System.Drawing.Color.FromArgb(CType(236, Byte), CType(246, Byte), CType(255, Byte))
            Me.GridView1.Appearance.RowSeparator.BackColor2 = System.Drawing.Color.White
            Me.GridView1.Appearance.RowSeparator.Options.UseBackColor = True
            Me.GridView1.Appearance.SelectedRow.BackColor = System.Drawing.Color.FromArgb(CType(83, Byte), CType(155, Byte), CType(215, Byte))
            Me.GridView1.Appearance.SelectedRow.ForeColor = System.Drawing.Color.White
            Me.GridView1.Appearance.SelectedRow.Options.UseBackColor = True
            Me.GridView1.Appearance.SelectedRow.Options.UseForeColor = True
            Me.GridView1.Appearance.TopNewRow.BackColor = System.Drawing.Color.White
            Me.GridView1.Appearance.TopNewRow.Options.UseBackColor = True
            Me.GridView1.Appearance.VertLine.BackColor = System.Drawing.Color.FromArgb(CType(104, Byte), CType(184, Byte), CType(251, Byte))
            Me.GridView1.Appearance.VertLine.Options.UseBackColor = True
            Me.GridView1.OptionsSelection.MultiSelect = True
            Me.GridView1.OptionsView.ShowGroupPanel = False
            Me.GridView1.OptionsView.ShowIndicator = False
            '
            'PostControl
            '
            Me.BatchStatus = ClientDepositBatch.BatchStatusEnum.Closed
            Me.Name = "PostControl"
            CType(Me.GridControl1, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.GridView1, System.ComponentModel.ISupportInitialize).EndInit()
            Me.ResumeLayout(False)

        End Sub

#End Region

        ''' <summary>
        ''' OK Button Linkage
        ''' </summary>
        Protected Overrides Sub OnSelected(ByVal e As System.EventArgs)

            Using cm As New DebtPlus.UI.Common.CursorManager()
                Using cn As New SqlConnection(DebtPlus.LINQ.SQLInfoClass.getDefault().ConnectionString)
                    AddHandler cn.InfoMessage, AddressOf cn_InfoMessage
                    Try
                        cn.Open()

                        ' Process the batches in the list
                        Dim RowHandles() As System.Int32 = GridView1.GetSelectedRows
                        For Each RowHandle As System.Int32 In RowHandles
                            Dim row As System.Data.DataRow = GridView1.GetDataRow(RowHandle)
                            Dim deposit_batch_id As System.Int32 = -1
                            If row IsNot Nothing AndAlso row("deposit_batch_id") IsNot Nothing AndAlso row("deposit_batch_id") IsNot System.DBNull.Value Then deposit_batch_id = Convert.ToInt32(row("deposit_batch_id"))

                            If deposit_batch_id > 0 Then
                                Try
                                    Using cmd As SqlClient.SqlCommand = New SqlCommand
                                        cmd.Connection = cn
                                        cmd.CommandTimeout = System.Math.Max(600, DebtPlus.LINQ.SQLInfoClass.getDefault().CommandTimeout)
                                        cmd.CommandText = "xpr_deposit_batch_post"
                                        cmd.CommandType = CommandType.StoredProcedure
                                        cmd.Parameters.Add("@deposit_batch_id", SqlDbType.Int).Value = deposit_batch_id
                                        cmd.ExecuteNonQuery()
                                    End Using

                                    ' Remove the row from the dataset. We don't delete rows when the table is updated so it is safe to simply remove it here.
                                    row.Delete()

                                Catch ex As System.Data.SqlClient.SqlException
                                    DebtPlus.Data.Forms.MessageBox.Show(ex.Message, String.Format("Error posting batch ID # {0:f0}", deposit_batch_id))
                                End Try

                            End If
                        Next

                        ' Finally, refresh the dataset with the list of still open batches
                        GridControl1.RefreshDataSource()

                    Catch ex As SqlClient.SqlException
                        DebtPlus.UI.Common.ErrorHandling.HandleErrors(ex, "Error closing deposit batch")
                    End Try

                    RemoveHandler cn.InfoMessage, AddressOf cn_InfoMessage
                End Using
            End Using
        End Sub

        Private Sub cn_InfoMessage(ByVal sender As Object, ByVal e As System.Data.SqlClient.SqlInfoMessageEventArgs)
            DebtPlus.Data.Forms.MessageBox.Show(e.Message, "Posting Status", MessageBoxButtons.OK, MessageBoxIcon.Information)
        End Sub
    End Class

End Namespace
