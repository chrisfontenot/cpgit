Option Compare Binary
Option Explicit On 
Option Strict On

Namespace Deposit.Close
    Friend Class DepositCloseArgParser
        Inherits DebtPlus.Utils.ArgParserBase

        ''' <summary>
        ''' Configuration information
        ''' </summary>
        Dim my_settings As System.Collections.Specialized.NameValueCollection = System.Configuration.ConfigurationManager.AppSettings

        Public Shadows ReadOnly Property config() As System.Collections.Specialized.NameValueCollection
            Get
                Return my_settings
            End Get
        End Property

        ''' <summary>
        ''' Deposit batch
        ''' </summary>
        Private _batch As System.Int32 = -1
        Public Property Batch() As System.Int32
            Get
                Return _batch
            End Get
            Set(ByVal Value As System.Int32)
                _batch = Value
            End Set
        End Property

        ''' <summary>
        ''' Create an instance of our class
        ''' </summary>
        Public Sub New()
            MyBase.New(New String() {"b"})

            ' Find the batch from the configuration file if there is one
            Dim config As System.Collections.Specialized.NameValueCollection = System.Configuration.ConfigurationManager.AppSettings

            Dim TempString As String = config.Item("batch")
            If TempString IsNot Nothing AndAlso TempString <> String.Empty Then
                Dim TempDouble As Double
                If Double.TryParse(TempString, Globalization.NumberStyles.Integer, System.Globalization.CultureInfo.InvariantCulture, TempDouble) Then
                    If TempDouble >= 0 AndAlso TempDouble <= Convert.ToDouble(Int32.MaxValue) Then
                        Batch = Convert.ToInt32(TempDouble)
                    End If
                End If
            End If
        End Sub

        ''' <summary>
        ''' Generate the command usage information
        ''' </summary>
        Protected Overrides Sub OnUsage(ByVal errorInfo As String)
            'AppendErrorLine("Usage: " + fname + " [-b#]")
            'AppendErrorLine("       -b# : existing batch ID")
        End Sub

        ''' <summary>
        ''' Process a switch item
        ''' </summary>
        Protected Overrides Function onSwitch(ByVal switchName As String, ByVal switchValue As String) As SwitchStatus
            Dim ss As SwitchStatus = DebtPlus.Utils.ArgParserBase.SwitchStatus.NoError
            Select Case switchName.ToLower()
                Case "b"
                    Dim value As Double
                    If Double.TryParse(switchValue, Globalization.NumberStyles.Integer, System.Globalization.CultureInfo.CurrentCulture, value) Then
                        If value <= 0.0 OrElse value > Convert.ToDouble(Int32.MaxValue) Then
                            ss = DebtPlus.Utils.ArgParserBase.SwitchStatus.YesError
                        Else
                            Batch = Convert.ToInt32(value)
                        End If
                    Else
                        ss = DebtPlus.Utils.ArgParserBase.SwitchStatus.YesError
                    End If

                Case "?"
                    ss = DebtPlus.Utils.ArgParserBase.SwitchStatus.ShowUsage

                Case Else
                    ss = DebtPlus.Utils.ArgParserBase.SwitchStatus.YesError
            End Select
            Return ss
        End Function
    End Class
End Namespace
