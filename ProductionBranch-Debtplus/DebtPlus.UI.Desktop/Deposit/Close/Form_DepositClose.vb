#Region "Copyright 2000-2012 DebtPlus, L.L.C."
'{*******************************************************************}
'{                                                                   }
'{       DebtPlus Debt Management System                             }
'{                                                                   }
'{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
'{       ALL RIGHTS RESERVED                                         }
'{                                                                   }
'{   The entire contents of this file is protected by U.S. and       }
'{   International Copyright Laws. Unauthorized reproduction,        }
'{   reverse-engineering, and distribution of all or any portion of  }
'{   the code contained in this file is strictly prohibited and may  }
'{   result in severe civil and criminal penalties and will be       }
'{   prosecuted to the maximum extent possible under the law.        }
'{                                                                   }
'{   RESTRICTIONS                                                    }
'{                                                                   }
'{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
'{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
'{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
'{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
'{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
'{                                                                   }
'{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
'{   ADDITIONAL RESTRICTIONS.                                        }
'{                                                                   }
'{*******************************************************************}
#End Region

Option Compare Binary
Option Explicit On 
Option Strict On

Namespace Deposit.Close

    Friend Class Form_DepositClose
        Inherits DebtPlus.Data.Forms.DebtPlusForm

        ''' <summary>
        ''' Create an instance of our class
        ''' </summary>
        Friend ap As DepositCloseArgParser
        Public Sub New(ByVal ap As DepositCloseArgParser)
            MyClass.New()
            Me.ap = ap
            AddHandler Me.Load, AddressOf Form_DepositClose_Load
            AddHandler CloseControl1.Cancelled, AddressOf CloseControl1_Cancelled
        End Sub

#Region " Windows Form Designer generated code "

        Public Sub New()
            MyBase.New()
            InitializeComponent()

        End Sub

        'Form overrides dispose to clean up the component list.
        Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
            If disposing Then
                If components IsNot Nothing Then
                    components.Dispose()
                End If
            End If
            MyBase.Dispose(disposing)
        End Sub

        'Required by the Windows Form Designer
        Private components As System.ComponentModel.IContainer

        'NOTE: The following procedure is required by the Windows Form Designer
        'It can be modified using the Windows Form Designer.  
        'Do not modify it using the code editor.
        Friend WithEvents CloseControl1 As DebtPlus.UI.Desktop.Deposit.Close.CloseControl
        <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
            Me.CloseControl1 = New CloseControl
            Me.SuspendLayout()

            '
            'CloseControl1
            '
            Me.CloseControl1.Dock = System.Windows.Forms.DockStyle.Fill
            Me.CloseControl1.DockPadding.All = 4
            Me.CloseControl1.Location = New System.Drawing.Point(0, 0)
            Me.CloseControl1.Name = "CloseControl1"
            Me.CloseControl1.Size = New System.Drawing.Size(560, 294)
            Me.ToolTipController1.SetSuperTip(Me.CloseControl1, Nothing)
            Me.CloseControl1.TabIndex = 0
            '
            'Form_DepositClose
            '
            Me.AutoScaleBaseSize = New System.Drawing.Size(5, 14)
            Me.ClientSize = New System.Drawing.Size(560, 294)
            Me.Controls.Add(Me.CloseControl1)
            Me.Name = "Form_DepositClose"
            Me.ToolTipController1.SetSuperTip(Me, Nothing)
            Me.Text = "Close Deposit Batch"
            Me.ResumeLayout(False)
        End Sub

#End Region

        Private Sub Form_DepositClose_Load(ByVal sender As Object, ByVal e As System.EventArgs)
            CloseControl1.ReadForm()
        End Sub

        Private Sub CloseControl1_Cancelled(ByVal sender As Object, ByVal e As System.EventArgs)
            Close()
        End Sub
    End Class
End Namespace
