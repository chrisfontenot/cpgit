#Region "Copyright 2000-2012 DebtPlus, L.L.C."
'{*******************************************************************}
'{                                                                   }
'{       DebtPlus Debt Management System                             }
'{                                                                   }
'{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
'{       ALL RIGHTS RESERVED                                         }
'{                                                                   }
'{   The entire contents of this file is protected by U.S. and       }
'{   International Copyright Laws. Unauthorized reproduction,        }
'{   reverse-engineering, and distribution of all or any portion of  }
'{   the code contained in this file is strictly prohibited and may  }
'{   result in severe civil and criminal penalties and will be       }
'{   prosecuted to the maximum extent possible under the law.        }
'{                                                                   }
'{   RESTRICTIONS                                                    }
'{                                                                   }
'{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
'{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
'{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
'{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
'{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
'{                                                                   }
'{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
'{   ADDITIONAL RESTRICTIONS.                                        }
'{                                                                   }
'{*******************************************************************}
#End Region

Option Compare Binary
Option Explicit On 
Option Strict On

Imports DebtPlus.UI.FormLib.Deposit

Namespace Deposit.Delete
    Friend Class Form_DepositDelete
        Inherits DebtPlus.Data.Forms.DebtPlusForm

        Public Sub New()
            MyBase.New()
            InitializeComponent()
            AddHandler Load, AddressOf Form_DepositDelete_Load
            AddHandler DeleteControl1.Cancelled, AddressOf DeleteControl1_Cancelled
        End Sub

        ''' <summary>
        ''' Create an instance of our class
        ''' </summary>
        Friend ap As DepositDeleteArgParser
        Public Sub New(ByVal ap As DepositDeleteArgParser)
            MyClass.New()
            Me.ap = ap
        End Sub

#Region " Windows Form Designer generated code "

        'Form overrides dispose to clean up the component list.
        Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
            If disposing Then
                If components IsNot Nothing Then
                    components.Dispose()
                End If
            End If
            MyBase.Dispose(disposing)
        End Sub

        'Required by the Windows Form Designer
        Private components As System.ComponentModel.IContainer

        'NOTE: The following procedure is required by the Windows Form Designer
        'It can be modified using the Windows Form Designer.  
        'Do not modify it using the code editor.
        Friend WithEvents DeleteControl1 As DebtPlus.UI.Desktop.Deposit.Delete.DeleteControl
        <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
            Me.DeleteControl1 = New DebtPlus.UI.Desktop.Deposit.Delete.DeleteControl

            Me.SuspendLayout()
            '
            'DeleteControl1
            '
            Me.DeleteControl1.BatchStatus = ClientDepositBatch.BatchStatusEnum.UnPosted
            Me.DeleteControl1.Dock = System.Windows.Forms.DockStyle.Fill
            Me.DeleteControl1.DockPadding.All = 4
            Me.DeleteControl1.Location = New System.Drawing.Point(0, 0)
            Me.DeleteControl1.Name = "DeleteControl1"
            Me.DeleteControl1.Size = New System.Drawing.Size(560, 294)
            Me.ToolTipController1.SetSuperTip(Me.DeleteControl1, Nothing)
            Me.DeleteControl1.TabIndex = 0
            '
            'Form_DepositDelete
            '
            Me.AutoScaleBaseSize = New System.Drawing.Size(5, 14)
            Me.ClientSize = New System.Drawing.Size(560, 294)
            Me.Controls.Add(Me.DeleteControl1)
            Me.Name = "Form_DepositDelete"
            Me.ToolTipController1.SetSuperTip(Me, Nothing)
            Me.Text = "Delete Deposit Batch"

            Me.ResumeLayout(False)

        End Sub

#End Region

        Private Sub Form_DepositDelete_Load(ByVal sender As Object, ByVal e As System.EventArgs)
            DeleteControl1.ReadForm()
        End Sub

        Private Sub DeleteControl1_Cancelled(ByVal sender As Object, ByVal e As System.EventArgs)
            Close()
        End Sub
    End Class

End Namespace
