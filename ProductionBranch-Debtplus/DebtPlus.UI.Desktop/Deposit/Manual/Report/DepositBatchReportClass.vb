#Region "Copyright 2000-2012 DebtPlus, L.L.C."
'{*******************************************************************}
'{                                                                   }
'{       DebtPlus Debt Management System                             }
'{                                                                   }
'{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
'{       ALL RIGHTS RESERVED                                         }
'{                                                                   }
'{   The entire contents of this file is protected by U.S. and       }
'{   International Copyright Laws. Unauthorized reproduction,        }
'{   reverse-engineering, and distribution of all or any portion of  }
'{   the code contained in this file is strictly prohibited and may  }
'{   result in severe civil and criminal penalties and will be       }
'{   prosecuted to the maximum extent possible under the law.        }
'{                                                                   }
'{   RESTRICTIONS                                                    }
'{                                                                   }
'{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
'{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
'{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
'{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
'{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
'{                                                                   }
'{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
'{   ADDITIONAL RESTRICTIONS.                                        }
'{                                                                   }
'{*******************************************************************}

#End Region

Option Compare Binary
Option Explicit On
Option Strict On

Imports System.ComponentModel
Imports DebtPlus.Reports.Template
Imports System.Drawing.Printing
Imports System.Data.SqlClient
Imports System.Windows.Forms

Namespace Deposit.Manual.Report
    Friend Class DepositBatchReportClass
        Inherits TemplateXtraReportClass

        Private ReadOnly ctx As DepositManualContext = Nothing
        Private depositManualForm As Form_DepositManual

        Public Sub New(ByVal p As Form_DepositManual, ByVal ctx As DepositManualContext)
            MyClass.New(p, ctx, CType(Nothing, IContainer))
        End Sub

        Public Sub New(ByVal p As Form_DepositManual, ctx As DepositManualContext, ByVal Container As IContainer)
            If Container IsNot Nothing Then Container.Add(Me)
            Me.ctx = ctx
            depositManualForm = p
            InitializeComponent()
            AddHandler XrLabel_ClientName.BeforePrint, AddressOf XrLabel_ClientName_BeforePrint
            AddHandler XrLabel_TranType.BeforePrint, AddressOf XrLabel_TranType_BeforePrint
            AddHandler Me.BeforePrint, AddressOf DepositBatchReportClass_BeforePrint
        End Sub

        ''' <summary>
        ''' Title associated with the report
        ''' </summary>
        Public Overrides ReadOnly Property ReportTitle() As String
            Get
                Return "Client Deposit Batch"
            End Get
        End Property

        ''' <summary>
        ''' SubTitle associated with the report
        ''' </summary>
        Public Overrides ReadOnly Property ReportSubTitle() As String
            Get
                Return String.Empty
            End Get
        End Property

        ''' <summary>
        ''' Request the parameters for the report
        ''' </summary>
        Public Overrides Function RequestReportParameters() As DialogResult
            Dim answer As DialogResult

            ' Request the disbursement register from the user
            Using dialogForm As New RequestParametersForm
                With dialogForm
                    answer = .ShowDialog()
                    Parameters("ParameterSortString").Value = .sort_order
                End With
            End Using

            ' The answer should be OK if we want to show the report.
            Return answer
        End Function

        ''' <summary>
        ''' Bind the dataset to the report
        ''' </summary>
        Private Sub DepositBatchReportClass_BeforePrint(ByVal sender As Object, ByVal e As PrintEventArgs)

            ' Bind the data fields to the report form
            Dim NewTbl As DataTable = ctx.ds.Tables("deposit_batch_details")
            DataSource = New DataView(NewTbl, String.Empty, Convert.ToString(Parameters("ParameterSortString").Value), DataViewRowState.CurrentRows)

            ' Read the batch header information
            ReadBatchHeader()
        End Sub

        ''' <summary>
        ''' Read the batch header information
        ''' </summary>
        Private Sub ReadBatchHeader()
            Dim rd As SqlDataReader = Nothing
            Dim cn As SqlConnection = New SqlConnection(DebtPlus.LINQ.SQLInfoClass.getDefault().ConnectionString)

            Try
                cn.Open()

                Using cmd As SqlCommand = New SqlCommand
                    With cmd
                        .Connection = cn
                        .CommandText = "SELECT d.date_created, dbo.format_counselor_name(d.created_by) as created_by, coalesce(b.description,'Bank #' + convert(varchar,d.bank),'') as BankName, isnull(b.account_number,'') as BankAccountNumber FROM deposit_batch_ids d LEFT OUTER JOIN banks b ON d.bank=b.bank WHERE deposit_batch_id=@deposit_batch_id"
                        .Parameters.Add("@deposit_batch_id", SqlDbType.Int).Value = ctx.ap.Batch
                        rd = .ExecuteReader(CommandBehavior.CloseConnection Or CommandBehavior.SingleRow)
                    End With
                End Using

                ' Populate the display information with the first row
                If rd.Read Then
                    XrLabel_BatchID.Text = ctx.ap.Batch.ToString()
                    XrLabel_BatchCreatedOn.Text = String.Format("{0} by {1}", rd.GetDateTime(rd.GetOrdinal("date_created")).ToShortDateString, rd.GetString(rd.GetOrdinal("created_by")))
                    XrLabel_Bank.Text = rd.GetString(rd.GetOrdinal("BankName"))
                    XrLabel_BankAccount.Text = rd.GetString(rd.GetOrdinal("BankAccount"))
                End If

            Catch ex As Exception
                DebtPlus.Svc.Common.ErrorHandling.HandleErrors(ex)

            Finally
                If cn IsNot Nothing Then cn.Dispose()
            End Try
        End Sub

        ''' <summary>
        ''' Format the transaction subtype description
        ''' </summary>
        Private Sub XrLabel_TranType_BeforePrint(ByVal sender As Object, ByVal e As PrintEventArgs)

            With XrLabel_TranType
                Dim TranType As Object = GetCurrentColumnValue("tran_subtype")
                If TranType Is Nothing OrElse TranType Is DBNull.Value Then
                    .Text = String.Empty
                Else
                    .Text = Convert.ToString(depositManualForm.DepositSubtypeTable.Rows.Find(TranType)("description"))
                End If
            End With
        End Sub

        ''' <summary>
        ''' Format the client name
        ''' </summary>
        Private Sub XrLabel_ClientName_BeforePrint(ByVal sender As Object, ByVal e As PrintEventArgs)

            With XrLabel_ClientName

                Dim client As Object = GetCurrentColumnValue("client")
                Dim Name As String = String.Empty
                If client IsNot DBNull.Value AndAlso Convert.ToInt32(client) > 0 Then
                    Dim row As DataRow = depositManualForm.ClientRow(Convert.ToInt32(client))
                    If row IsNot Nothing Then
                        If row("applicant") IsNot DBNull.Value Then Name = Convert.ToString(row("applicant"))
                    End If
                End If

                .Text = Name
            End With
        End Sub

    End Class
End Namespace
