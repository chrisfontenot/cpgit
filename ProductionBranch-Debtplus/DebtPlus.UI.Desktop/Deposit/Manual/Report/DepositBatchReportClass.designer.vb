﻿Namespace Deposit.Manual.Report

    Partial Class DepositBatchReportClass
        Inherits DebtPlus.Reports.Template.TemplateXtraReportClass

        'Component overrides dispose to clean up the component list.
        Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
            If disposing Then
                If components IsNot Nothing Then
                    components.Dispose()
                End If
            End If
            MyBase.Dispose(disposing)
        End Sub

        'Required by the Component Designer
        Private components As System.ComponentModel.IContainer

        'NOTE: The following procedure is required by the Component Designer
        'It can be modified using the Component Designer.
        'Do not modify it using the code editor.
        <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
            Dim XrSummary1 As DevExpress.XtraReports.UI.XRSummary = New DevExpress.XtraReports.UI.XRSummary()
            Dim XrSummary2 As DevExpress.XtraReports.UI.XRSummary = New DevExpress.XtraReports.UI.XRSummary()
            Me.StyleColumnHeaderText = New DevExpress.XtraReports.UI.XRControlStyle()
            Me.XrControlStyle2 = New DevExpress.XtraReports.UI.XRControlStyle()
            Me.XrPanel_BatchHeader = New DevExpress.XtraReports.UI.XRPanel()
            Me.XrLabel_BankAccount = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel_Bank = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel_BatchCreatedOn = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel_BatchID = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel4 = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel3 = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel2 = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel1 = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrPanel1 = New DevExpress.XtraReports.UI.XRPanel()
            Me.XrLabel10 = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel9 = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel8 = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel6 = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel5 = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel_TranType = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel_ClientID = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel_ClientName = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel_Amount = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel_Date = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel_Reference = New DevExpress.XtraReports.UI.XRLabel()
            Me.ReportFooter = New DevExpress.XtraReports.UI.ReportFooterBand()
            Me.XrLine1 = New DevExpress.XtraReports.UI.XRLine()
            Me.XrLabel_TotalAmount = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel_TotalsLabel = New DevExpress.XtraReports.UI.XRLabel()
            Me.ParameterSortString = New DevExpress.XtraReports.Parameters.Parameter()
            CType(Me, System.ComponentModel.ISupportInitialize).BeginInit()
            '
            'Detail
            '
            Me.Detail.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrLabel_Reference, Me.XrLabel_Date, Me.XrLabel_Amount, Me.XrLabel_ClientName, Me.XrLabel_ClientID, Me.XrLabel_TranType})
            Me.Detail.HeightF = 18.0!
            '
            'PageHeader
            '
            Me.PageHeader.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrPanel_BatchHeader, Me.XrPanel1})
            Me.PageHeader.HeightF = 206.0!
            Me.PageHeader.Controls.SetChildIndex(Me.XrPanel1, 0)
            Me.PageHeader.Controls.SetChildIndex(Me.XrPanel_BatchHeader, 0)
            Me.PageHeader.Controls.SetChildIndex(Me.XrPanel_AgencyAddress, 0)
            Me.PageHeader.Controls.SetChildIndex(Me.XrLabel_Title, 0)
            Me.PageHeader.Controls.SetChildIndex(Me.XrLine_Title, 0)
            Me.PageHeader.Controls.SetChildIndex(Me.XrLabel_Subtitle, 0)
            Me.PageHeader.Controls.SetChildIndex(Me.XrPageInfo1, 0)
            '
            'XrLabel_Title
            '
            Me.XrLabel_Title.StylePriority.UseFont = False
            '
            'XrLabel_Subtitle
            '
            Me.XrLabel_Subtitle.LocationFloat = New DevExpress.Utils.PointFloat(275.0!, 67.0!)
            Me.XrLabel_Subtitle.SizeF = New System.Drawing.SizeF(151.0!, 17.0!)
            Me.XrLabel_Subtitle.Text = "Subtitle is not used"
            Me.XrLabel_Subtitle.Visible = False
            '
            'XrPageInfo_PageNumber
            '
            Me.XrPageInfo_PageNumber.StylePriority.UseTextAlignment = False
            '
            'XRLabel_Agency_Name
            '
            Me.XRLabel_Agency_Name.StylePriority.UseTextAlignment = False
            '
            'XrLabel_Agency_Address3
            '
            Me.XrLabel_Agency_Address3.StylePriority.UseFont = False
            Me.XrLabel_Agency_Address3.StylePriority.UseTextAlignment = False
            '
            'XrLabel_Agency_Address1
            '
            Me.XrLabel_Agency_Address1.StylePriority.UseFont = False
            Me.XrLabel_Agency_Address1.StylePriority.UseTextAlignment = False
            '
            'XrLabel_Agency_Phone
            '
            Me.XrLabel_Agency_Phone.StylePriority.UseFont = False
            Me.XrLabel_Agency_Phone.StylePriority.UseTextAlignment = False
            '
            'XrLabel_Agency_Address2
            '
            Me.XrLabel_Agency_Address2.StylePriority.UseFont = False
            Me.XrLabel_Agency_Address2.StylePriority.UseTextAlignment = False
            '
            'StyleColumnHeaderText
            '
            Me.StyleColumnHeaderText.BackColor = System.Drawing.Color.Transparent
            Me.StyleColumnHeaderText.BorderColor = System.Drawing.Color.Transparent
            Me.StyleColumnHeaderText.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Bold)
            Me.StyleColumnHeaderText.ForeColor = System.Drawing.Color.White
            Me.StyleColumnHeaderText.Name = "StyleColumnHeaderText"
            '
            'XrControlStyle2
            '
            Me.XrControlStyle2.BackColor = System.Drawing.Color.Transparent
            Me.XrControlStyle2.ForeColor = System.Drawing.Color.Black
            Me.XrControlStyle2.Name = "XrControlStyle2"
            '
            'XrPanel_BatchHeader
            '
            Me.XrPanel_BatchHeader.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrLabel_BankAccount, Me.XrLabel_Bank, Me.XrLabel_BatchCreatedOn, Me.XrLabel_BatchID, Me.XrLabel4, Me.XrLabel3, Me.XrLabel2, Me.XrLabel1})
            Me.XrPanel_BatchHeader.LocationFloat = New DevExpress.Utils.PointFloat(0.0!, 100.0!)
            Me.XrPanel_BatchHeader.Name = "XrPanel_BatchHeader"
            Me.XrPanel_BatchHeader.SizeF = New System.Drawing.SizeF(433.0!, 60.0!)
            '
            'XrLabel_BankAccount
            '
            Me.XrLabel_BankAccount.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Bold)
            Me.XrLabel_BankAccount.ForeColor = System.Drawing.Color.Teal
            Me.XrLabel_BankAccount.LocationFloat = New DevExpress.Utils.PointFloat(158.0!, 45.0!)
            Me.XrLabel_BankAccount.Name = "XrLabel_BankAccount"
            Me.XrLabel_BankAccount.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel_BankAccount.SizeF = New System.Drawing.SizeF(266.0!, 15.0!)
            '
            'XrLabel_Bank
            '
            Me.XrLabel_Bank.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Bold)
            Me.XrLabel_Bank.ForeColor = System.Drawing.Color.Teal
            Me.XrLabel_Bank.LocationFloat = New DevExpress.Utils.PointFloat(158.0!, 30.0!)
            Me.XrLabel_Bank.Name = "XrLabel_Bank"
            Me.XrLabel_Bank.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel_Bank.SizeF = New System.Drawing.SizeF(266.0!, 15.0!)
            '
            'XrLabel_BatchCreatedOn
            '
            Me.XrLabel_BatchCreatedOn.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Bold)
            Me.XrLabel_BatchCreatedOn.ForeColor = System.Drawing.Color.Teal
            Me.XrLabel_BatchCreatedOn.LocationFloat = New DevExpress.Utils.PointFloat(158.0!, 15.0!)
            Me.XrLabel_BatchCreatedOn.Name = "XrLabel_BatchCreatedOn"
            Me.XrLabel_BatchCreatedOn.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel_BatchCreatedOn.SizeF = New System.Drawing.SizeF(267.0!, 15.0!)
            '
            'XrLabel_BatchID
            '
            Me.XrLabel_BatchID.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Bold)
            Me.XrLabel_BatchID.ForeColor = System.Drawing.Color.Teal
            Me.XrLabel_BatchID.LocationFloat = New DevExpress.Utils.PointFloat(158.0!, 0.0!)
            Me.XrLabel_BatchID.Name = "XrLabel_BatchID"
            Me.XrLabel_BatchID.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel_BatchID.SizeF = New System.Drawing.SizeF(266.0!, 15.0!)
            '
            'XrLabel4
            '
            Me.XrLabel4.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Bold)
            Me.XrLabel4.ForeColor = System.Drawing.Color.Teal
            Me.XrLabel4.LocationFloat = New DevExpress.Utils.PointFloat(8.0!, 45.0!)
            Me.XrLabel4.Name = "XrLabel4"
            Me.XrLabel4.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel4.SizeF = New System.Drawing.SizeF(145.0!, 15.0!)
            Me.XrLabel4.Text = "Bank Account Number:"
            '
            'XrLabel3
            '
            Me.XrLabel3.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Bold)
            Me.XrLabel3.ForeColor = System.Drawing.Color.Teal
            Me.XrLabel3.LocationFloat = New DevExpress.Utils.PointFloat(8.0!, 30.0!)
            Me.XrLabel3.Name = "XrLabel3"
            Me.XrLabel3.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel3.SizeF = New System.Drawing.SizeF(145.0!, 15.0!)
            Me.XrLabel3.Text = "Bank Name:"
            '
            'XrLabel2
            '
            Me.XrLabel2.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Bold)
            Me.XrLabel2.ForeColor = System.Drawing.Color.Teal
            Me.XrLabel2.LocationFloat = New DevExpress.Utils.PointFloat(8.0!, 15.0!)
            Me.XrLabel2.Name = "XrLabel2"
            Me.XrLabel2.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel2.SizeF = New System.Drawing.SizeF(145.0!, 15.0!)
            Me.XrLabel2.Text = "Batch Created On:"
            '
            'XrLabel1
            '
            Me.XrLabel1.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Bold)
            Me.XrLabel1.ForeColor = System.Drawing.Color.Teal
            Me.XrLabel1.LocationFloat = New DevExpress.Utils.PointFloat(8.0!, 0.0!)
            Me.XrLabel1.Name = "XrLabel1"
            Me.XrLabel1.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel1.SizeF = New System.Drawing.SizeF(145.0!, 15.0!)
            Me.XrLabel1.Text = "Batch Number:"
            '
            'XrPanel1
            '
            Me.XrPanel1.BackColor = System.Drawing.Color.Teal
            Me.XrPanel1.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrLabel10, Me.XrLabel9, Me.XrLabel8, Me.XrLabel6, Me.XrLabel5})
            Me.XrPanel1.LocationFloat = New DevExpress.Utils.PointFloat(0.0!, 183.0!)
            Me.XrPanel1.Name = "XrPanel1"
            Me.XrPanel1.SizeF = New System.Drawing.SizeF(750.0!, 17.0!)
            '
            'XrLabel10
            '
            Me.XrLabel10.LocationFloat = New DevExpress.Utils.PointFloat(600.0!, 0.0!)
            Me.XrLabel10.Name = "XrLabel10"
            Me.XrLabel10.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel10.SizeF = New System.Drawing.SizeF(150.0!, 17.0!)
            Me.XrLabel10.StyleName = "StyleColumnHeaderText"
            Me.XrLabel10.Text = "REFERENCE"
            '
            'XrLabel9
            '
            Me.XrLabel9.LocationFloat = New DevExpress.Utils.PointFloat(534.0!, 0.0!)
            Me.XrLabel9.Name = "XrLabel9"
            Me.XrLabel9.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel9.SizeF = New System.Drawing.SizeF(59.0!, 17.0!)
            Me.XrLabel9.StyleName = "StyleColumnHeaderText"
            Me.XrLabel9.Text = "DATE"
            Me.XrLabel9.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight
            '
            'XrLabel8
            '
            Me.XrLabel8.LocationFloat = New DevExpress.Utils.PointFloat(450.0!, 0.0!)
            Me.XrLabel8.Name = "XrLabel8"
            Me.XrLabel8.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel8.SizeF = New System.Drawing.SizeF(66.0!, 17.0!)
            Me.XrLabel8.StyleName = "StyleColumnHeaderText"
            Me.XrLabel8.Text = "AMOUNT"
            Me.XrLabel8.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight
            '
            'XrLabel6
            '
            Me.XrLabel6.LocationFloat = New DevExpress.Utils.PointFloat(117.0!, 0.0!)
            Me.XrLabel6.Name = "XrLabel6"
            Me.XrLabel6.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel6.SizeF = New System.Drawing.SizeF(325.0!, 17.0!)
            Me.XrLabel6.StyleName = "StyleColumnHeaderText"
            Me.XrLabel6.StylePriority.UseTextAlignment = False
            Me.XrLabel6.Text = "Client ID AND NAME"
            Me.XrLabel6.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
            '
            'XrLabel5
            '
            Me.XrLabel5.LocationFloat = New DevExpress.Utils.PointFloat(8.0!, 0.0!)
            Me.XrLabel5.Name = "XrLabel5"
            Me.XrLabel5.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel5.SizeF = New System.Drawing.SizeF(92.0!, 17.0!)
            Me.XrLabel5.StyleName = "StyleColumnHeaderText"
            Me.XrLabel5.Text = "ITEM TYPE"
            '
            'XrLabel_TranType
            '
            Me.XrLabel_TranType.LocationFloat = New DevExpress.Utils.PointFloat(8.0!, 0.0!)
            Me.XrLabel_TranType.Name = "XrLabel_TranType"
            Me.XrLabel_TranType.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel_TranType.SizeF = New System.Drawing.SizeF(92.0!, 17.0!)
            Me.XrLabel_TranType.WordWrap = False
            '
            'XrLabel_ClientID
            '
            Me.XrLabel_ClientID.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "client", "{0:0000000}")})
            Me.XrLabel_ClientID.LocationFloat = New DevExpress.Utils.PointFloat(108.0!, 0.0!)
            Me.XrLabel_ClientID.Name = "XrLabel_ClientID"
            Me.XrLabel_ClientID.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel_ClientID.SizeF = New System.Drawing.SizeF(75.0!, 17.0!)
            Me.XrLabel_ClientID.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight
            Me.XrLabel_ClientID.WordWrap = False
            '
            'XrLabel_ClientName
            '
            Me.XrLabel_ClientName.LocationFloat = New DevExpress.Utils.PointFloat(192.0!, 0.0!)
            Me.XrLabel_ClientName.Multiline = True
            Me.XrLabel_ClientName.Name = "XrLabel_ClientName"
            Me.XrLabel_ClientName.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel_ClientName.SizeF = New System.Drawing.SizeF(250.0!, 17.0!)
            '
            'XrLabel_Amount
            '
            Me.XrLabel_Amount.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "amount", "{0:c}")})
            Me.XrLabel_Amount.LocationFloat = New DevExpress.Utils.PointFloat(450.0!, 0.0!)
            Me.XrLabel_Amount.Name = "XrLabel_Amount"
            Me.XrLabel_Amount.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel_Amount.SizeF = New System.Drawing.SizeF(66.0!, 17.0!)
            Me.XrLabel_Amount.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight
            Me.XrLabel_Amount.WordWrap = False
            '
            'XrLabel_Date
            '
            Me.XrLabel_Date.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "item_date", "{0:d}")})
            Me.XrLabel_Date.LocationFloat = New DevExpress.Utils.PointFloat(517.0!, 0.0!)
            Me.XrLabel_Date.Name = "XrLabel_Date"
            Me.XrLabel_Date.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel_Date.SizeF = New System.Drawing.SizeF(76.0!, 17.0!)
            Me.XrLabel_Date.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight
            Me.XrLabel_Date.WordWrap = False
            '
            'XrLabel_Reference
            '
            Me.XrLabel_Reference.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "reference")})
            Me.XrLabel_Reference.LocationFloat = New DevExpress.Utils.PointFloat(600.0!, 0.0!)
            Me.XrLabel_Reference.Name = "XrLabel_Reference"
            Me.XrLabel_Reference.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel_Reference.SizeF = New System.Drawing.SizeF(150.0!, 17.0!)
            Me.XrLabel_Reference.WordWrap = False
            '
            'ReportFooter
            '
            Me.ReportFooter.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrLine1, Me.XrLabel_TotalAmount, Me.XrLabel_TotalsLabel})
            Me.ReportFooter.HeightF = 42.0!
            Me.ReportFooter.Name = "ReportFooter"
            '
            'XrLine1
            '
            Me.XrLine1.LocationFloat = New DevExpress.Utils.PointFloat(399.0!, 8.0!)
            Me.XrLine1.Name = "XrLine1"
            Me.XrLine1.SizeF = New System.Drawing.SizeF(117.0!, 9.0!)
            '
            'XrLabel_TotalAmount
            '
            Me.XrLabel_TotalAmount.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "amount")})
            Me.XrLabel_TotalAmount.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
            Me.XrLabel_TotalAmount.ForeColor = System.Drawing.Color.Black
            Me.XrLabel_TotalAmount.LocationFloat = New DevExpress.Utils.PointFloat(400.0!, 25.0!)
            Me.XrLabel_TotalAmount.Name = "XrLabel_TotalAmount"
            Me.XrLabel_TotalAmount.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel_TotalAmount.SizeF = New System.Drawing.SizeF(116.0!, 17.0!)
            XrSummary1.FormatString = "{0:c}"
            XrSummary1.Running = DevExpress.XtraReports.UI.SummaryRunning.Report
            Me.XrLabel_TotalAmount.Summary = XrSummary1
            Me.XrLabel_TotalAmount.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight
            Me.XrLabel_TotalAmount.WordWrap = False
            '
            'XrLabel_TotalsLabel
            '
            Me.XrLabel_TotalsLabel.CanGrow = False
            Me.XrLabel_TotalsLabel.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "client")})
            Me.XrLabel_TotalsLabel.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Bold)
            Me.XrLabel_TotalsLabel.ForeColor = System.Drawing.Color.Blue
            Me.XrLabel_TotalsLabel.LocationFloat = New DevExpress.Utils.PointFloat(0.0!, 25.0!)
            Me.XrLabel_TotalsLabel.Name = "XrLabel_TotalsLabel"
            Me.XrLabel_TotalsLabel.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel_TotalsLabel.SizeF = New System.Drawing.SizeF(351.375!, 17.0!)
            XrSummary2.FormatString = "Grand Total of Batch ({0:n0} items)"
            XrSummary2.Func = DevExpress.XtraReports.UI.SummaryFunc.Count
            XrSummary2.Running = DevExpress.XtraReports.UI.SummaryRunning.Report
            Me.XrLabel_TotalsLabel.Summary = XrSummary2
            Me.XrLabel_TotalsLabel.Text = "Grand Total of Batch (0 items)"
            '
            'ParameterSortString
            '
            Me.ParameterSortString.Description = "Sort Order"
            Me.ParameterSortString.Name = "ParameterSortString"
            Me.ParameterSortString.Visible = False
            '
            'DepositBatchReportClass
            '
            Me.Bands.AddRange(New DevExpress.XtraReports.UI.Band() {Me.Detail, Me.PageHeader, Me.PageFooter, Me.TopMarginBand1, Me.BottomMarginBand1, Me.ReportFooter})
            Me.Parameters.AddRange(New DevExpress.XtraReports.Parameters.Parameter() {Me.ParameterSortString})
            Me.RequestParameters = False
            Me.StyleSheet.AddRange(New DevExpress.XtraReports.UI.XRControlStyle() {Me.XrControlStyle_Header, Me.XrControlStyle_Totals, Me.XrControlStyle_HeaderPannel, Me.XrControlStyle_GroupHeader, Me.StyleColumnHeaderText, Me.XrControlStyle2})
            Me.Version = "11.2"
            Me.Controls.SetChildIndex(Me.ReportFooter, 0)
            Me.Controls.SetChildIndex(Me.BottomMarginBand1, 0)
            Me.Controls.SetChildIndex(Me.TopMarginBand1, 0)
            Me.Controls.SetChildIndex(Me.PageFooter, 0)
            Me.Controls.SetChildIndex(Me.PageHeader, 0)
            Me.Controls.SetChildIndex(Me.Detail, 0)
            CType(Me, System.ComponentModel.ISupportInitialize).EndInit()

        End Sub

        Friend WithEvents XrPanel_BatchHeader As DevExpress.XtraReports.UI.XRPanel
        Friend WithEvents XrLabel1 As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel2 As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel3 As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel4 As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel_BatchID As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel_BatchCreatedOn As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel_Bank As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel_BankAccount As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrPanel1 As DevExpress.XtraReports.UI.XRPanel
        Friend WithEvents XrLabel5 As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel6 As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel8 As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel9 As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel10 As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel_ClientName As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents ReportFooter As DevExpress.XtraReports.UI.ReportFooterBand
        Friend WithEvents XrLabel_TotalsLabel As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLine1 As DevExpress.XtraReports.UI.XRLine
        Friend WithEvents XrLabel_TranType As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel_ClientID As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel_Amount As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel_Date As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel_Reference As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel_TotalAmount As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents StyleColumnHeaderText As DevExpress.XtraReports.UI.XRControlStyle
        Friend WithEvents XrControlStyle2 As DevExpress.XtraReports.UI.XRControlStyle
        Friend WithEvents ParameterSortString As DevExpress.XtraReports.Parameters.Parameter
    End Class
End Namespace