#Region "Copyright 2000-2012 DebtPlus, L.L.C."
'{*******************************************************************}
'{                                                                   }
'{       DebtPlus Debt Management System                             }
'{                                                                   }
'{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
'{       ALL RIGHTS RESERVED                                         }
'{                                                                   }
'{   The entire contents of this file is protected by U.S. and       }
'{   International Copyright Laws. Unauthorized reproduction,        }
'{   reverse-engineering, and distribution of all or any portion of  }
'{   the code contained in this file is strictly prohibited and may  }
'{   result in severe civil and criminal penalties and will be       }
'{   prosecuted to the maximum extent possible under the law.        }
'{                                                                   }
'{   RESTRICTIONS                                                    }
'{                                                                   }
'{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
'{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
'{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
'{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
'{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
'{                                                                   }
'{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
'{   ADDITIONAL RESTRICTIONS.                                        }
'{                                                                   }
'{*******************************************************************}
#End Region

Namespace Deposit.Manual.Report
    Friend Class RequestParametersForm
        Inherits DebtPlus.Reports.Template.Forms.ReportParametersForm

        Public Sub New()
            MyBase.New()
            InitializeComponent()
            AddHandler Me.Load, AddressOf RequestParametersForm_Load
        End Sub

        Friend ReadOnly Property sort_order() As String
            Get
                Dim item As DebtPlus.Data.Controls.ComboboxItem
                With ComboBoxEdit_Sorting
                    If .SelectedIndex < 0 Then Return System.String.Empty
                    item = CType(.SelectedItem, DebtPlus.Data.Controls.ComboboxItem)
                    Return Convert.ToString(item.value)
                End With
            End Get
        End Property

#Region " Windows Form Designer generated code "

        'Form overrides dispose to clean up the component list.
        Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
            If disposing Then
                If components IsNot Nothing Then
                    components.Dispose()
                End If
            End If
            MyBase.Dispose(disposing)
        End Sub

        'Required by the Windows Form Designer
        Private components As System.ComponentModel.IContainer

        'NOTE: The following procedure is required by the Windows Form Designer
        'It can be modified using the Windows Form Designer.  
        'Do not modify it using the code editor.
        Friend WithEvents Label2 As DevExpress.XtraEditors.LabelControl
        Friend WithEvents ComboBoxEdit_Sorting As DevExpress.XtraEditors.ComboBoxEdit
        <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
            Me.Label2 = New DevExpress.XtraEditors.LabelControl
            Me.ComboBoxEdit_Sorting = New DevExpress.XtraEditors.ComboBoxEdit
            CType(Me.DxErrorProvider1, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.ComboBoxEdit_Sorting.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
            Me.SuspendLayout()
            '
            'ButtonSelect
            '
            Me.ButtonOK.Enabled = True
            Me.ButtonOK.Location = New System.Drawing.Point(248, 18)
            Me.ButtonOK.Size = New System.Drawing.Size(80, 28)
            Me.ButtonOK.TabIndex = 3
            '
            'ButtonCancel
            '
            Me.ButtonCancel.Location = New System.Drawing.Point(248, 56)
            Me.ButtonCancel.Size = New System.Drawing.Size(80, 28)
            Me.ButtonCancel.TabIndex = 4
            '
            'Label2
            '
            Me.Label2.Location = New System.Drawing.Point(10, 26)
            Me.Label2.Name = "Label2"
            Me.Label2.Size = New System.Drawing.Size(38, 13)
            Me.Label2.TabIndex = 1
            Me.Label2.Text = "Sorting:"
            '
            'ComboBoxEdit_Sorting
            '
            Me.ComboBoxEdit_Sorting.EditValue = ""
            Me.ComboBoxEdit_Sorting.Location = New System.Drawing.Point(54, 23)
            Me.ComboBoxEdit_Sorting.Name = "ComboBoxEdit_Sorting"
            Me.ComboBoxEdit_Sorting.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
            Me.ComboBoxEdit_Sorting.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor
            Me.ComboBoxEdit_Sorting.Size = New System.Drawing.Size(176, 20)
            Me.ComboBoxEdit_Sorting.TabIndex = 2
            Me.ComboBoxEdit_Sorting.ToolTip = "How should the report be sorted?"
            Me.ComboBoxEdit_Sorting.ToolTipController = Me.ToolTipController1
            '
            'RequestParametersForm
            '
            Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
            Me.ClientSize = New System.Drawing.Size(336, 95)
            Me.Controls.Add(Me.ComboBoxEdit_Sorting)
            Me.Controls.Add(Me.Label2)
            Me.Name = "RequestParametersForm"
            Me.ToolTipController1.SetSuperTip(Me, Nothing)
            Me.Text = "Deposit Batch Report Parameters"
            Me.Controls.SetChildIndex(Me.ButtonOK, 0)
            Me.Controls.SetChildIndex(Me.ButtonCancel, 0)
            Me.Controls.SetChildIndex(Me.Label2, 0)
            Me.Controls.SetChildIndex(Me.ComboBoxEdit_Sorting, 0)
            CType(Me.DxErrorProvider1, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.ComboBoxEdit_Sorting.Properties, System.ComponentModel.ISupportInitialize).EndInit()
            Me.ResumeLayout(False)
            Me.PerformLayout()
        End Sub

#End Region

        Private Sub RequestParametersForm_Load(ByVal sender As System.Object, ByVal e As System.EventArgs)

            ' Populate the sorting order dropdown
            With ComboBoxEdit_Sorting
                With .Properties
                    With .Items
                        .Clear()
                        ComboBoxEdit_Sorting.SelectedIndex = .Add(New DebtPlus.Data.Controls.ComboboxItem("Entry Order", "deposit_batch_detail"))
                        .Add(New DebtPlus.Data.Controls.ComboboxItem("Client ID", "client"))
                        .Add(New DebtPlus.Data.Controls.ComboboxItem("Amount", "amount"))
                        .Add(New DebtPlus.Data.Controls.ComboboxItem("Deposit Type", "tran_subtype"))
                        .Add(New DebtPlus.Data.Controls.ComboboxItem("Date/Time Entered", "date_created"))
                        .Add(New DebtPlus.Data.Controls.ComboboxItem("Instrument Date", "item_date"))
                        .Add(New DebtPlus.Data.Controls.ComboboxItem("Reference Field", "reference"))
                    End With
                End With
            End With
        End Sub
    End Class
End Namespace