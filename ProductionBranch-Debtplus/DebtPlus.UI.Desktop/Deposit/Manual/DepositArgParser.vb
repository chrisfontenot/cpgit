Option Compare Binary
Option Explicit On
Option Strict On

Imports System.Configuration
Imports System.Collections.Specialized
Imports System.Globalization

Namespace Deposit.Manual
    Public Class DepositArgParser
        Inherits DebtPlus.Utils.ArgParserBase

        ''' <summary>
        ''' Deposit batch
        ''' </summary>
        Private _batch As Int32 = -1

        Public Property Batch() As Int32
            Get
                Return _batch
            End Get
            Set(ByVal Value As Int32)
                _batch = Value
            End Set
        End Property

        ''' <summary>
        ''' Create an instance of our class
        ''' </summary>
        Public Sub New()
            MyBase.New(New String() {"b"})

            ' Find the batch from the configuration file if there is one
            Dim TempString As String = DebtPlus.Configuration.Config.GetConfigValueForKey(DebtPlus.Configuration.Config.GetConfigSettings(DebtPlus.Configuration.Config.ConfigSet.DepositManual), "batch")

            If TempString IsNot Nothing AndAlso TempString <> String.Empty Then
                If Not Int32.TryParse(TempString, NumberStyles.Integer, CultureInfo.CurrentCulture, Batch) OrElse Batch <= 0 Then
                    Batch = -1
                End If
            End If
        End Sub

        ''' <summary>
        ''' Generate the command usage information
        ''' </summary>
        Protected Overrides Sub OnUsage(ByVal errorInfo As String)
            'AppendErrorLine("Usage: " + fname + " [-b#]")
            'AppendErrorLine("       -b# : existing batch ID")
        End Sub

        ''' <summary>
        ''' Process a switch item
        ''' </summary>
        Protected Overrides Function onSwitch(ByVal switchName As String, ByVal switchValue As String) As SwitchStatus
            Dim ss As SwitchStatus = DebtPlus.Utils.ArgParserBase.SwitchStatus.NoError
            Select Case switchName.ToLower()
                Case "b"
                    If Int32.TryParse(switchValue, NumberStyles.Integer, CultureInfo.CurrentCulture, Batch) AndAlso Batch <= 0 Then
                        ss = DebtPlus.Utils.ArgParserBase.SwitchStatus.YesError
                    End If

                Case "?"
                    ss = DebtPlus.Utils.ArgParserBase.SwitchStatus.ShowUsage

                Case Else
                    ss = DebtPlus.Utils.ArgParserBase.SwitchStatus.YesError
            End Select
            Return ss
        End Function
    End Class
End Namespace
