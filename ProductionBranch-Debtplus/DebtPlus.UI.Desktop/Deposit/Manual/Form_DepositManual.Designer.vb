﻿Imports DebtPlus.UI.FormLib.Deposit

Namespace Deposit.Manual
    <Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
    Partial Class Form_DepositManual
        Inherits DebtPlus.Data.Forms.DebtPlusForm

        'Form overrides dispose to clean up the component list.
        <System.Diagnostics.DebuggerNonUserCode()> _
        Protected Overrides Sub Dispose(ByVal disposing As Boolean)
            Try
                If disposing AndAlso components IsNot Nothing Then
                    components.Dispose()
                End If
            Finally
                MyBase.Dispose(disposing)
            End Try
        End Sub

        'Required by the Windows Form Designer
        Private components As System.ComponentModel.IContainer

        'NOTE: The following procedure is required by the Windows Form Designer
        'It can be modified using the Windows Form Designer.  
        'Do not modify it using the code editor.
        <System.Diagnostics.DebuggerStepThrough()> _
        Private Sub InitializeComponent()
            Me.components = New System.ComponentModel.Container
            Me.NewClientDepositBatch1 = New NewClientDepositBatch
            Me.ClientDepositControl1 = New Controls.ClientDepositControl(Me, ctx)
            Me.barManager1 = New DevExpress.XtraBars.BarManager(Me.components)
            Me.Bar2 = New DevExpress.XtraBars.Bar
            Me.BarSubItem_File = New DevExpress.XtraBars.BarSubItem
            Me.BarButtonItem1 = New DevExpress.XtraBars.BarButtonItem
            Me.BarAndDockingController1 = New DevExpress.XtraBars.BarAndDockingController(Me.components)
            Me.barDockControlTop = New DevExpress.XtraBars.BarDockControl
            Me.barDockControlBottom = New DevExpress.XtraBars.BarDockControl
            Me.barDockControlLeft = New DevExpress.XtraBars.BarDockControl
            Me.barDockControlRight = New DevExpress.XtraBars.BarDockControl
            CType(Me.DxErrorProvider1, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.barManager1, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.BarAndDockingController1, System.ComponentModel.ISupportInitialize).BeginInit()
            Me.SuspendLayout()
            '
            'NewClientDepositBatch1
            '
            Me.NewClientDepositBatch1.Dock = System.Windows.Forms.DockStyle.Fill
            Me.NewClientDepositBatch1.Location = New System.Drawing.Point(0, 24)
            Me.NewClientDepositBatch1.Name = "NewClientDepositBatch1"
            Me.NewClientDepositBatch1.Size = New System.Drawing.Size(557, 409)
            Me.NewClientDepositBatch1.TabIndex = 0
            '
            'ClientDepositControl1
            '
            Me.ClientDepositControl1.Dock = System.Windows.Forms.DockStyle.Fill
            Me.ClientDepositControl1.Location = New System.Drawing.Point(0, 24)
            Me.ClientDepositControl1.Name = "ClientDepositControl1"
            Me.ClientDepositControl1.Size = New System.Drawing.Size(557, 409)
            Me.ClientDepositControl1.TabIndex = 1
            '
            'barManager1
            '
            Me.barManager1.Bars.AddRange(New DevExpress.XtraBars.Bar() {Me.Bar2})
            Me.barManager1.Controller = Me.BarAndDockingController1
            Me.barManager1.DockControls.Add(Me.barDockControlTop)
            Me.barManager1.DockControls.Add(Me.barDockControlBottom)
            Me.barManager1.DockControls.Add(Me.barDockControlLeft)
            Me.barManager1.DockControls.Add(Me.barDockControlRight)
            Me.barManager1.Form = Me
            Me.barManager1.Items.AddRange(New DevExpress.XtraBars.BarItem() {Me.BarSubItem_File, Me.BarButtonItem1})
            Me.barManager1.MainMenu = Me.Bar2
            Me.barManager1.MaxItemId = 2
            '
            'Bar2
            '
            Me.Bar2.BarName = "Main menu"
            Me.Bar2.DockCol = 0
            Me.Bar2.DockRow = 0
            Me.Bar2.DockStyle = DevExpress.XtraBars.BarDockStyle.Top
            Me.Bar2.LinksPersistInfo.AddRange(New DevExpress.XtraBars.LinkPersistInfo() {New DevExpress.XtraBars.LinkPersistInfo(Me.BarSubItem_File)})
            Me.Bar2.OptionsBar.MultiLine = True
            Me.Bar2.OptionsBar.UseWholeRow = True
            Me.Bar2.Text = "Main menu"
            '
            'BarSubItem_File
            '
            Me.BarSubItem_File.Caption = "&File"
            Me.BarSubItem_File.Id = 0
            Me.BarSubItem_File.LinksPersistInfo.AddRange(New DevExpress.XtraBars.LinkPersistInfo() {New DevExpress.XtraBars.LinkPersistInfo(Me.BarButtonItem1)})
            Me.BarSubItem_File.Name = "BarSubItem_File"
            '
            'BarButtonItem1
            '
            Me.BarButtonItem1.Caption = "&Exit"
            Me.BarButtonItem1.Id = 1
            Me.BarButtonItem1.Name = "BarButtonItem1"
            '
            'Form_DepositManual
            '
            Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
            Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
            Me.ClientSize = New System.Drawing.Size(557, 433)
            Me.Controls.Add(Me.ClientDepositControl1)
            Me.Controls.Add(Me.NewClientDepositBatch1)
            Me.Controls.Add(Me.barDockControlLeft)
            Me.Controls.Add(Me.barDockControlRight)
            Me.Controls.Add(Me.barDockControlBottom)
            Me.Controls.Add(Me.barDockControlTop)
            Me.Name = "Form_DepositManual"
            Me.Text = "Add payments to a batch"
            CType(Me.DxErrorProvider1, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.barManager1, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.BarAndDockingController1, System.ComponentModel.ISupportInitialize).EndInit()
            Me.ResumeLayout(False)

        End Sub
        Friend WithEvents NewClientDepositBatch1 As NewClientDepositBatch
        Friend WithEvents ClientDepositControl1 As Controls.ClientDepositControl
        Friend WithEvents barManager1 As DevExpress.XtraBars.BarManager
        Friend WithEvents Bar2 As DevExpress.XtraBars.Bar
        Friend WithEvents BarAndDockingController1 As DevExpress.XtraBars.BarAndDockingController
        Friend WithEvents barDockControlTop As DevExpress.XtraBars.BarDockControl
        Friend WithEvents barDockControlBottom As DevExpress.XtraBars.BarDockControl
        Friend WithEvents barDockControlLeft As DevExpress.XtraBars.BarDockControl
        Friend WithEvents barDockControlRight As DevExpress.XtraBars.BarDockControl
        Friend WithEvents BarButtonItem1 As DevExpress.XtraBars.BarButtonItem
        Protected Friend WithEvents BarSubItem_File As DevExpress.XtraBars.BarSubItem

    End Class
End Namespace