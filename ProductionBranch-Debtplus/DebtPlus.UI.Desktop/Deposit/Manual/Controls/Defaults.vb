#Region "Copyright 2000-2012 DebtPlus, L.L.C."
'{*******************************************************************}
'{                                                                   }
'{       DebtPlus Debt Management System                             }
'{                                                                   }
'{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
'{       ALL RIGHTS RESERVED                                         }
'{                                                                   }
'{   The entire contents of this file is protected by U.S. and       }
'{   International Copyright Laws. Unauthorized reproduction,        }
'{   reverse-engineering, and distribution of all or any portion of  }
'{   the code contained in this file is strictly prohibited and may  }
'{   result in severe civil and criminal penalties and will be       }
'{   prosecuted to the maximum extent possible under the law.        }
'{                                                                   }
'{   RESTRICTIONS                                                    }
'{                                                                   }
'{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
'{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
'{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
'{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
'{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
'{                                                                   }
'{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
'{   ADDITIONAL RESTRICTIONS.                                        }
'{                                                                   }
'{*******************************************************************}
#End Region

Namespace Deposit.Manual.Controls

    Public Module Defaults
        Private privateEnterMoveNextControl As Int32 = 0
        Private privateShowAlertNotesOnEntry As Int32 = 0
        Private privateShowAlertNotesOnEdit As Int32 = 0
        Private privateUseSameReference As Int32 = 0
        Private privateDefaultSubType As String = "*"

        ''' <summary>
        ''' Show the keypad "Enter" key be used as a TAB key?
        ''' </summary>
        Public Function EnterMoveNextControl() As Boolean
            If privateEnterMoveNextControl = 0 Then
                Dim strItem As String = DebtPlus.Configuration.Config.GetConfigValueForKey(DebtPlus.Configuration.Config.GetConfigSettings(DebtPlus.Configuration.Config.ConfigSet.DepositManual), "EnterMoveNextControl")
                Dim ItemValue As Boolean
                If Not Boolean.TryParse(strItem, ItemValue) Then ItemValue = True
                privateEnterMoveNextControl = If(ItemValue, 1, 2)
            End If

            Return privateEnterMoveNextControl = 1
        End Function

        ''' <summary>
        ''' Should alert notes be shown when the client is entered on the keypad?
        ''' </summary>
        Public Function ShowAlertNotesOnEntry() As Boolean
            If privateShowAlertNotesOnEntry = 0 Then
                Dim strItem As String = DebtPlus.Configuration.Config.GetConfigValueForKey(DebtPlus.Configuration.Config.GetConfigSettings(DebtPlus.Configuration.Config.ConfigSet.DepositManual), "ShowAlertNotesOnEntry")
                Dim ItemValue As Boolean
                If Not Boolean.TryParse(strItem, ItemValue) Then ItemValue = True
                privateShowAlertNotesOnEntry = If(ItemValue, 1, 2)
            End If

            Return privateShowAlertNotesOnEntry = 1
        End Function

        ''' <summary>
        ''' Should the alert notes be shown when the client is edited?
        ''' </summary>
        Public Function ShowAlertNotesOnEdit() As Boolean
            If privateShowAlertNotesOnEdit = 0 Then
                Dim strItem As String = DebtPlus.Configuration.Config.GetConfigValueForKey(DebtPlus.Configuration.Config.GetConfigSettings(DebtPlus.Configuration.Config.ConfigSet.DepositManual), "ShowAlertNotesOnEdit")
                Dim ItemValue As Boolean
                If Not Boolean.TryParse(strItem, ItemValue) Then ItemValue = False
                privateShowAlertNotesOnEdit = If(ItemValue, 1, 2)
            End If

            Return privateShowAlertNotesOnEdit = 1
        End Function

        ''' <summary>
        ''' Should the reference information be preserved from one client to the ntext?
        ''' </summary>
        Public Function UseSameReference() As Boolean
            If privateUseSameReference = 0 Then
                Dim strItem As String = DebtPlus.Configuration.Config.GetConfigValueForKey(DebtPlus.Configuration.Config.GetConfigSettings(DebtPlus.Configuration.Config.ConfigSet.DepositManual), "UseSameReference")
                Dim ItemValue As Boolean
                If Not Boolean.TryParse(strItem, ItemValue) Then ItemValue = True
                privateUseSameReference = If(ItemValue, 1, 2)
            End If

            Return privateUseSameReference = 1
        End Function

        ''' <summary>
        ''' Should the reference information be preserved from one client to the ntext?
        ''' </summary>
        Public Function DefaultSubType() As String
            If privateDefaultSubType = "*" Then
                Dim strItem As String = DebtPlus.Configuration.Config.GetConfigValueForKey(DebtPlus.Configuration.Config.GetConfigSettings(DebtPlus.Configuration.Config.ConfigSet.DepositManual), "DefaultSubType")
                If String.IsNullOrEmpty(strItem) Then
                    strItem = "MO"
                End If
                privateDefaultSubType = strItem
            End If

            Return privateDefaultSubType
        End Function
    End Module
End Namespace
