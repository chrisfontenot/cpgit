﻿Namespace Deposit.Manual.Controls

    <Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
    Partial Class BatchStatusControl
        Inherits BaseControl

        'UserControl overrides dispose to clean up the component list.
        <System.Diagnostics.DebuggerNonUserCode()> _
        Protected Overrides Sub Dispose(ByVal disposing As Boolean)
            Try
                If disposing AndAlso components IsNot Nothing Then
                    components.Dispose()
                End If
            Finally
                MyBase.Dispose(disposing)
            End Try
        End Sub

        'Required by the Windows Form Designer
        Private components As System.ComponentModel.IContainer

        'NOTE: The following procedure is required by the Windows Form Designer
        'It can be modified using the Windows Form Designer.  
        'Do not modify it using the code editor.
        <System.Diagnostics.DebuggerStepThrough()> _
        Private Sub InitializeComponent()
            Me.GroupControl1 = New DevExpress.XtraEditors.GroupControl
            Me.LabelControl_batch_id = New DevExpress.XtraEditors.LabelControl
            Me.LabelControl_total = New DevExpress.XtraEditors.LabelControl
            Me.LabelControl_batch_count = New DevExpress.XtraEditors.LabelControl
            Me.LabelControl3 = New DevExpress.XtraEditors.LabelControl
            Me.LabelControl2 = New DevExpress.XtraEditors.LabelControl
            Me.LabelControl1 = New DevExpress.XtraEditors.LabelControl
            CType(Me.GroupControl1, System.ComponentModel.ISupportInitialize).BeginInit()
            Me.GroupControl1.SuspendLayout()
            Me.SuspendLayout()
            '
            'GroupControl1
            '
            Me.GroupControl1.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                        Or System.Windows.Forms.AnchorStyles.Left) _
                        Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
            Me.GroupControl1.Controls.Add(Me.LabelControl_batch_id)
            Me.GroupControl1.Controls.Add(Me.LabelControl_total)
            Me.GroupControl1.Controls.Add(Me.LabelControl_batch_count)
            Me.GroupControl1.Controls.Add(Me.LabelControl3)
            Me.GroupControl1.Controls.Add(Me.LabelControl2)
            Me.GroupControl1.Controls.Add(Me.LabelControl1)
            Me.GroupControl1.Location = New System.Drawing.Point(4, 4)
            Me.GroupControl1.Name = "GroupControl1"
            Me.GroupControl1.Size = New System.Drawing.Size(105, 85)
            Me.GroupControl1.TabIndex = 0
            Me.GroupControl1.Text = "Batch Information"
            '
            'LabelControl_batch_id
            '
            Me.LabelControl_batch_id.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                        Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
            Me.LabelControl_batch_id.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
            Me.LabelControl_batch_id.Appearance.TextOptions.Trimming = DevExpress.Utils.Trimming.None
            Me.LabelControl_batch_id.Appearance.TextOptions.WordWrap = DevExpress.Utils.WordWrap.NoWrap
            Me.LabelControl_batch_id.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None
            Me.LabelControl_batch_id.Location = New System.Drawing.Point(71, 24)
            Me.LabelControl_batch_id.Name = "LabelControl_batch_id"
            Me.LabelControl_batch_id.Size = New System.Drawing.Size(28, 13)
            Me.LabelControl_batch_id.TabIndex = 5
            Me.LabelControl_batch_id.Text = "0"
            Me.LabelControl_batch_id.ToolTip = "ID of the deposit batch"
            '
            'LabelControl_total
            '
            Me.LabelControl_total.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                        Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
            Me.LabelControl_total.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
            Me.LabelControl_total.Appearance.TextOptions.Trimming = DevExpress.Utils.Trimming.None
            Me.LabelControl_total.Appearance.TextOptions.WordWrap = DevExpress.Utils.WordWrap.NoWrap
            Me.LabelControl_total.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None
            Me.LabelControl_total.Location = New System.Drawing.Point(71, 62)
            Me.LabelControl_total.Name = "LabelControl_total"
            Me.LabelControl_total.Size = New System.Drawing.Size(28, 13)
            Me.LabelControl_total.TabIndex = 4
            Me.LabelControl_total.Text = "$0.00"
            Me.LabelControl_total.ToolTip = "Total amount of the deposit batch"
            '
            'LabelControl_batch_count
            '
            Me.LabelControl_batch_count.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                        Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
            Me.LabelControl_batch_count.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
            Me.LabelControl_batch_count.Appearance.TextOptions.Trimming = DevExpress.Utils.Trimming.None
            Me.LabelControl_batch_count.Appearance.TextOptions.WordWrap = DevExpress.Utils.WordWrap.NoWrap
            Me.LabelControl_batch_count.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None
            Me.LabelControl_batch_count.Location = New System.Drawing.Point(72, 43)
            Me.LabelControl_batch_count.Name = "LabelControl_batch_count"
            Me.LabelControl_batch_count.Size = New System.Drawing.Size(28, 13)
            Me.LabelControl_batch_count.TabIndex = 3
            Me.LabelControl_batch_count.Text = "0"
            Me.LabelControl_batch_count.ToolTip = "Number of deposit items in the batch"
            '
            'LabelControl3
            '
            Me.LabelControl3.Location = New System.Drawing.Point(6, 62)
            Me.LabelControl3.Name = "LabelControl3"
            Me.LabelControl3.Size = New System.Drawing.Size(54, 13)
            Me.LabelControl3.TabIndex = 2
            Me.LabelControl3.Text = "Batch Total"
            Me.LabelControl3.UseMnemonic = False
            '
            'LabelControl2
            '
            Me.LabelControl2.Location = New System.Drawing.Point(6, 43)
            Me.LabelControl2.Name = "LabelControl2"
            Me.LabelControl2.Size = New System.Drawing.Size(59, 13)
            Me.LabelControl2.TabIndex = 1
            Me.LabelControl2.Text = "Batch Count"
            Me.LabelControl2.UseMnemonic = False
            '
            'LabelControl1
            '
            Me.LabelControl1.Location = New System.Drawing.Point(6, 24)
            Me.LabelControl1.Name = "LabelControl1"
            Me.LabelControl1.Size = New System.Drawing.Size(41, 13)
            Me.LabelControl1.TabIndex = 0
            Me.LabelControl1.Text = "Batch ID"
            Me.LabelControl1.UseMnemonic = False
            '
            'BatchStatusControl
            '
            Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
            Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
            Me.Controls.Add(Me.GroupControl1)
            Me.Name = "BatchStatusControl"
            Me.Size = New System.Drawing.Size(113, 95)
            CType(Me.GroupControl1, System.ComponentModel.ISupportInitialize).EndInit()
            Me.GroupControl1.ResumeLayout(False)
            Me.GroupControl1.PerformLayout()
            Me.ResumeLayout(False)

        End Sub
        Protected Friend WithEvents GroupControl1 As DevExpress.XtraEditors.GroupControl
        Protected Friend WithEvents LabelControl_batch_id As DevExpress.XtraEditors.LabelControl
        Protected Friend WithEvents LabelControl_total As DevExpress.XtraEditors.LabelControl
        Protected Friend WithEvents LabelControl_batch_count As DevExpress.XtraEditors.LabelControl
        Protected Friend WithEvents LabelControl3 As DevExpress.XtraEditors.LabelControl
        Protected Friend WithEvents LabelControl2 As DevExpress.XtraEditors.LabelControl
        Protected Friend WithEvents LabelControl1 As DevExpress.XtraEditors.LabelControl
    End Class
End Namespace