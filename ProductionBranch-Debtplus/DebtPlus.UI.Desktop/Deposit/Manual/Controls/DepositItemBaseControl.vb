#Region "Copyright 2000-2012 DebtPlus, L.L.C."
'{*******************************************************************}
'{                                                                   }
'{       DebtPlus Debt Management System                             }
'{                                                                   }
'{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
'{       ALL RIGHTS RESERVED                                         }
'{                                                                   }
'{   The entire contents of this file is protected by U.S. and       }
'{   International Copyright Laws. Unauthorized reproduction,        }
'{   reverse-engineering, and distribution of all or any portion of  }
'{   the code contained in this file is strictly prohibited and may  }
'{   result in severe civil and criminal penalties and will be       }
'{   prosecuted to the maximum extent possible under the law.        }
'{                                                                   }
'{   RESTRICTIONS                                                    }
'{                                                                   }
'{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
'{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
'{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
'{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
'{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
'{                                                                   }
'{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
'{   ADDITIONAL RESTRICTIONS.                                        }
'{                                                                   }
'{*******************************************************************}

#End Region
Imports System.ComponentModel
Imports DevExpress.Data
Imports DevExpress.XtraEditors.Controls
Imports DebtPlus.UI.Client.Widgets.Controls
Imports DebtPlus.Data.Controls
Imports DevExpress.XtraEditors
Imports DevExpress.Utils
Imports DevExpress.XtraEditors.Mask
Imports System.Text.RegularExpressions
Imports DebtPlus.Notes
Imports System.Globalization
Imports System.Threading
Imports DebtPlus.UI.Client.Service
Imports System.Data.SqlClient
Imports System.Windows.Forms
Imports System.Drawing

Namespace Deposit.Manual.Controls
    Friend Class DepositItemBaseControl
        Inherits BaseControl

#Region " Initialize Component "
        'UserControl overrides dispose to clean up the component list.
        Protected Overrides Sub Dispose(ByVal disposing As Boolean)
            Try
                If disposing AndAlso components IsNot Nothing Then
                    components.Dispose()
                End If
            Finally
                MyBase.Dispose(disposing)
            End Try
        End Sub

        Friend WithEvents TextEditItemDate As TextEdit
        Protected Friend WithEvents BlinkLabel1 As BlinkLabel

        'Required by the Windows Form Designer
        Private components As IContainer

        'NOTE: The following procedure is required by the Windows Form Designer
        'It can be modified using the Windows Form Designer.  
        'Do not modify it using the code editor.
        Private Sub InitializeComponent()
            Dim SuperToolTip7 As SuperToolTip = New SuperToolTip
            Dim ToolTipTitleItem7 As ToolTipTitleItem = New ToolTipTitleItem
            Dim ToolTipItem7 As ToolTipItem = New ToolTipItem
            Dim resources As ComponentResourceManager = New ComponentResourceManager(GetType(DepositItemBaseControl))
            Dim SerializableAppearanceObject2 As SerializableAppearanceObject = New SerializableAppearanceObject
            Dim SuperToolTip1 As SuperToolTip = New SuperToolTip
            Dim ToolTipTitleItem1 As ToolTipTitleItem = New ToolTipTitleItem
            Dim ToolTipItem1 As ToolTipItem = New ToolTipItem
            Dim SuperToolTip2 As SuperToolTip = New SuperToolTip
            Dim ToolTipTitleItem2 As ToolTipTitleItem = New ToolTipTitleItem
            Dim ToolTipItem2 As ToolTipItem = New ToolTipItem
            Dim SuperToolTip8 As SuperToolTip = New SuperToolTip
            Dim ToolTipTitleItem8 As ToolTipTitleItem = New ToolTipTitleItem
            Dim ToolTipItem8 As ToolTipItem = New ToolTipItem
            Dim SuperToolTip4 As SuperToolTip = New SuperToolTip
            Dim ToolTipTitleItem4 As ToolTipTitleItem = New ToolTipTitleItem
            Dim ToolTipItem4 As ToolTipItem = New ToolTipItem
            Dim SuperToolTip5 As SuperToolTip = New SuperToolTip
            Dim ToolTipTitleItem5 As ToolTipTitleItem = New ToolTipTitleItem
            Dim ToolTipItem5 As ToolTipItem = New ToolTipItem
            Me.GroupControl1 = New GroupControl
            Me.BlinkLabel1 = New BlinkLabel
            Me.LabelControl_coapplicant = New LabelControl
            Me.LabelControl_applicant = New LabelControl
            Me.LabelControl_address = New LabelControl
            Me.LabelControl_deposit_amount = New LabelControl
            Me.LabelControl4 = New LabelControl
            Me.LabelControl3 = New LabelControl
            Me.LabelControl2 = New LabelControl
            Me.LabelControl1 = New LabelControl
            Me.GroupControl2 = New GroupControl
            Me.TextEditItemDate = New TextEdit
            Me.ClientID_client = New DebtPlus.UI.Client.Widgets.Controls.ClientID
            Me.SimpleButton_Notes = New SimpleButton
            Me.LabelControl9 = New LabelControl
            Me.TextEdit_reference = New TextEdit
            Me.LabelControl8 = New LabelControl
            Me.LabelControl7 = New LabelControl
            Me.LookUpEdit_tran_subtype = New LookUpEdit
            Me.CalcEdit_deposit_amount = New CalcEdit
            Me.LabelControl6 = New LabelControl
            Me.LabelControl5 = New LabelControl
            CType(Me.GroupControl1, ISupportInitialize).BeginInit()
            Me.GroupControl1.SuspendLayout()
            CType(Me.GroupControl2, ISupportInitialize).BeginInit()
            Me.GroupControl2.SuspendLayout()
            CType(Me.TextEditItemDate.Properties, ISupportInitialize).BeginInit()
            CType(Me.ClientID_client.Properties, ISupportInitialize).BeginInit()
            CType(Me.TextEdit_reference.Properties, ISupportInitialize).BeginInit()
            CType(Me.LookUpEdit_tran_subtype.Properties, ISupportInitialize).BeginInit()
            CType(Me.CalcEdit_deposit_amount.Properties, ISupportInitialize).BeginInit()
            Me.SuspendLayout()
            '
            'GroupControl1
            '
            Me.GroupControl1.Anchor = CType((((AnchorStyles.Top Or AnchorStyles.Bottom) _
                                              Or AnchorStyles.Left) _
                                             Or AnchorStyles.Right),
                                            AnchorStyles)
            Me.GroupControl1.Controls.Add(Me.BlinkLabel1)
            Me.GroupControl1.Controls.Add(Me.LabelControl_coapplicant)
            Me.GroupControl1.Controls.Add(Me.LabelControl_applicant)
            Me.GroupControl1.Controls.Add(Me.LabelControl_address)
            Me.GroupControl1.Controls.Add(Me.LabelControl_deposit_amount)
            Me.GroupControl1.Controls.Add(Me.LabelControl4)
            Me.GroupControl1.Controls.Add(Me.LabelControl3)
            Me.GroupControl1.Controls.Add(Me.LabelControl2)
            Me.GroupControl1.Controls.Add(Me.LabelControl1)
            Me.GroupControl1.Location = New Point(4, 4)
            Me.GroupControl1.Name = "GroupControl1"
            Me.GroupControl1.Size = New Size(302, 137)
            Me.GroupControl1.TabIndex = 0
            Me.GroupControl1.Text = "Client Information"
            '
            'BlinkLabel1
            '
            Me.BlinkLabel1.Anchor = CType(((AnchorStyles.Bottom Or AnchorStyles.Left) _
                                           Or AnchorStyles.Right),
                                          AnchorStyles)
            Me.BlinkLabel1.Appearance.BackColor = Color.Red
            Me.BlinkLabel1.Appearance.Font = New Font("Tahoma", 8.25!, FontStyle.Bold)
            Me.BlinkLabel1.Appearance.ForeColor = Color.White
            Me.BlinkLabel1.Appearance.TextOptions.HAlignment = HorzAlignment.Center
            Me.BlinkLabel1.AutoSizeMode = LabelAutoSizeMode.None
            Me.BlinkLabel1.BlinkTime = 500
            Me.BlinkLabel1.CausesValidation = False
            Me.BlinkLabel1.Location = New Point(178, 117)
            Me.BlinkLabel1.Margin = New Padding(5)
            Me.BlinkLabel1.Name = "BlinkLabel1"
            Me.BlinkLabel1.Padding = New Padding(0, 2, 0, 2)
            Me.BlinkLabel1.Size = New Size(119, 17)
            Me.BlinkLabel1.TabIndex = 9
            Me.BlinkLabel1.Text = "INACTIVE client"
            Me.BlinkLabel1.UseMnemonic = False
            Me.BlinkLabel1.Visible = False
            '
            'LabelControl_coapplicant
            '
            Me.LabelControl_coapplicant.Anchor = CType(((AnchorStyles.Top Or AnchorStyles.Left) _
                                                        Or AnchorStyles.Right),
                                                       AnchorStyles)
            Me.LabelControl_coapplicant.Appearance.BackColor = Color.FromArgb(CType(CType(255, Byte), Int32), CType(CType(255, Byte), Int32), CType(CType(192, Byte), Int32))
            Me.LabelControl_coapplicant.AutoSizeMode = LabelAutoSizeMode.None
            Me.LabelControl_coapplicant.Location = New Point(88, 43)
            Me.LabelControl_coapplicant.Name = "LabelControl_coapplicant"
            Me.LabelControl_coapplicant.Size = New Size(209, 13)
            Me.LabelControl_coapplicant.TabIndex = 3
            Me.LabelControl_coapplicant.ToolTip = "Name of the co-applicant for the client"
            Me.LabelControl_coapplicant.UseMnemonic = False
            '
            'LabelControl_applicant
            '
            Me.LabelControl_applicant.Anchor = CType(((AnchorStyles.Top Or AnchorStyles.Left) _
                                                      Or AnchorStyles.Right), 
                                                     AnchorStyles)
            Me.LabelControl_applicant.Appearance.BackColor = Color.FromArgb(CType(CType(255, Byte), Int32), CType(CType(255, Byte), Int32), CType(CType(192, Byte), Int32))
            Me.LabelControl_applicant.AutoSizeMode = LabelAutoSizeMode.None
            Me.LabelControl_applicant.Location = New Point(88, 24)
            Me.LabelControl_applicant.Name = "LabelControl_applicant"
            Me.LabelControl_applicant.Size = New Size(209, 13)
            Me.LabelControl_applicant.TabIndex = 1
            Me.LabelControl_applicant.ToolTip = "Name of the applicant for the client"
            Me.LabelControl_applicant.UseMnemonic = False
            '
            'LabelControl_address
            '
            Me.LabelControl_address.Anchor = CType((((AnchorStyles.Top Or AnchorStyles.Bottom) _
                                                     Or AnchorStyles.Left) _
                                                    Or AnchorStyles.Right), 
                                                   AnchorStyles)
            Me.LabelControl_address.Appearance.BackColor = Color.FromArgb(CType(CType(255, Byte), Int32), CType(CType(255, Byte), Int32), CType(CType(192, Byte), Int32))
            Me.LabelControl_address.Appearance.TextOptions.HAlignment = HorzAlignment.Near
            Me.LabelControl_address.Appearance.TextOptions.Trimming = Trimming.EllipsisCharacter
            Me.LabelControl_address.Appearance.TextOptions.VAlignment = VertAlignment.Top
            Me.LabelControl_address.Appearance.TextOptions.WordWrap = WordWrap.NoWrap
            Me.LabelControl_address.AutoSizeMode = LabelAutoSizeMode.None
            Me.LabelControl_address.Location = New Point(88, 62)
            Me.LabelControl_address.Name = "LabelControl_address"
            Me.LabelControl_address.Size = New Size(209, 51)
            Me.LabelControl_address.TabIndex = 5
            Me.LabelControl_address.ToolTip = "Client's home address"
            Me.LabelControl_address.UseMnemonic = False
            '
            'LabelControl_deposit_amount
            '
            Me.LabelControl_deposit_amount.Anchor = CType((AnchorStyles.Bottom Or AnchorStyles.Left), AnchorStyles)
            Me.LabelControl_deposit_amount.Appearance.BackColor = Color.FromArgb(CType(CType(255, Byte), Int32), CType(CType(255, Byte), Int32), CType(CType(192, Byte), Int32))
            Me.LabelControl_deposit_amount.Appearance.TextOptions.HAlignment = HorzAlignment.Far
            Me.LabelControl_deposit_amount.AutoSizeMode = LabelAutoSizeMode.None
            Me.LabelControl_deposit_amount.Location = New Point(88, 119)
            Me.LabelControl_deposit_amount.Name = "LabelControl_deposit_amount"
            Me.LabelControl_deposit_amount.Size = New Size(84, 13)
            Me.LabelControl_deposit_amount.TabIndex = 7
            Me.LabelControl_deposit_amount.Text = "$0.00"
            Me.LabelControl_deposit_amount.ToolTip = "The normal deposit amount for this client"
            Me.LabelControl_deposit_amount.UseMnemonic = False
            '
            'LabelControl4
            '
            Me.LabelControl4.Anchor = CType((AnchorStyles.Bottom Or AnchorStyles.Left), AnchorStyles)
            Me.LabelControl4.Appearance.BackColor = Color.Transparent
            Me.LabelControl4.Location = New Point(6, 119)
            Me.LabelControl4.Name = "LabelControl4"
            Me.LabelControl4.Size = New Size(76, 13)
            Me.LabelControl4.TabIndex = 6
            Me.LabelControl4.Text = "Deposit Amount"
            Me.LabelControl4.UseMnemonic = False
            '
            'LabelControl3
            '
            Me.LabelControl3.Appearance.BackColor = Color.Transparent
            Me.LabelControl3.Location = New Point(6, 62)
            Me.LabelControl3.Name = "LabelControl3"
            Me.LabelControl3.Size = New Size(39, 13)
            Me.LabelControl3.TabIndex = 4
            Me.LabelControl3.Text = "Address"
            Me.LabelControl3.UseMnemonic = False
            '
            'LabelControl2
            '
            Me.LabelControl2.Appearance.BackColor = Color.Transparent
            Me.LabelControl2.Location = New Point(6, 43)
            Me.LabelControl2.Name = "LabelControl2"
            Me.LabelControl2.Size = New Size(61, 13)
            Me.LabelControl2.TabIndex = 2
            Me.LabelControl2.Text = "Co-Applicant"
            Me.LabelControl2.UseMnemonic = False
            '
            'LabelControl1
            '
            Me.LabelControl1.Appearance.BackColor = Color.Transparent
            Me.LabelControl1.Location = New Point(6, 24)
            Me.LabelControl1.Name = "LabelControl1"
            Me.LabelControl1.Size = New Size(44, 13)
            Me.LabelControl1.TabIndex = 0
            Me.LabelControl1.Text = "Applicant"
            Me.LabelControl1.UseMnemonic = False
            '
            'GroupControl2
            '
            Me.GroupControl2.Anchor = CType(((AnchorStyles.Bottom Or AnchorStyles.Left) _
                                             Or AnchorStyles.Right), 
                                            AnchorStyles)
            Me.GroupControl2.Controls.Add(Me.TextEditItemDate)
            Me.GroupControl2.Controls.Add(Me.ClientID_client)
            Me.GroupControl2.Controls.Add(Me.SimpleButton_Notes)
            Me.GroupControl2.Controls.Add(Me.LabelControl9)
            Me.GroupControl2.Controls.Add(Me.TextEdit_reference)
            Me.GroupControl2.Controls.Add(Me.LabelControl8)
            Me.GroupControl2.Controls.Add(Me.LabelControl7)
            Me.GroupControl2.Controls.Add(Me.LookUpEdit_tran_subtype)
            Me.GroupControl2.Controls.Add(Me.CalcEdit_deposit_amount)
            Me.GroupControl2.Controls.Add(Me.LabelControl6)
            Me.GroupControl2.Controls.Add(Me.LabelControl5)
            Me.GroupControl2.Location = New Point(4, 147)
            Me.GroupControl2.Name = "GroupControl2"
            Me.GroupControl2.Size = New Size(302, 156)
            Me.GroupControl2.TabIndex = 1
            Me.GroupControl2.TabStop = True
            Me.GroupControl2.Text = "Deposit Information"
            '
            'TextEditItemDate
            '
            Me.TextEditItemDate.EnterMoveNextControl = True
            Me.TextEditItemDate.Location = New Point(88, 130)
            Me.TextEditItemDate.Name = "TextEditItemDate"
            Me.TextEditItemDate.Properties.DisplayFormat.FormatString = "d"
            Me.TextEditItemDate.Properties.DisplayFormat.FormatType = FormatType.DateTime
            Me.TextEditItemDate.Properties.EditFormat.FormatString = "d"
            Me.TextEditItemDate.Properties.EditFormat.FormatType = FormatType.DateTime
            Me.TextEditItemDate.Properties.Mask.BeepOnError = True
            Me.TextEditItemDate.Properties.Mask.EditMask = "[0-9/]*"
            Me.TextEditItemDate.Properties.Mask.MaskType = MaskType.RegEx
            Me.TextEditItemDate.Size = New Size(116, 20)
            SuperToolTip7.AllowHtmlText = DefaultBoolean.[True]
            ToolTipTitleItem7.Text = "Date on deposit"
            ToolTipItem7.LeftIndent = 6
            ToolTipItem7.Text = resources.GetString("ToolTipItem7.Text")
            SuperToolTip7.Items.Add(ToolTipTitleItem7)
            SuperToolTip7.Items.Add(ToolTipItem7)
            Me.TextEditItemDate.SuperTip = SuperToolTip7
            Me.TextEditItemDate.TabIndex = 9
            '
            'ClientID_client
            '
            Me.ClientID_client.EnterMoveNextControl = True
            Me.ClientID_client.Location = New Point(88, 30)
            Me.ClientID_client.Name = "ClientID_client"
            Me.ClientID_client.Properties.AllowNullInput = DefaultBoolean.[False]
            Me.ClientID_client.Properties.Appearance.Options.UseTextOptions = True
            Me.ClientID_client.Properties.Appearance.TextOptions.HAlignment = HorzAlignment.Far
            Me.ClientID_client.Properties.Buttons.AddRange(New EditorButton() {New EditorButton(ButtonPredefines.Glyph, "", -1, True, True, False, ImageLocation.MiddleCenter, CType(resources.GetObject("ClientID_client.Properties.Buttons"), Image), New KeyShortcut(Keys.None), SerializableAppearanceObject2, "Click here to search for a client ID", "search", Nothing, True)})
            Me.ClientID_client.Properties.CharacterCasing = CharacterCasing.Upper
            Me.ClientID_client.Properties.DisplayFormat.FormatString = "0000000"
            Me.ClientID_client.Properties.DisplayFormat.FormatType = FormatType.Custom
            Me.ClientID_client.Properties.EditFormat.FormatString = "f0"
            Me.ClientID_client.Properties.EditFormat.FormatType = FormatType.Numeric
            Me.ClientID_client.Properties.Mask.AutoComplete = AutoCompleteType.None
            Me.ClientID_client.Properties.Mask.BeepOnError = True
            Me.ClientID_client.Properties.Mask.EditMask = "\d*"
            Me.ClientID_client.Properties.Mask.MaskType = MaskType.RegEx
            Me.ClientID_client.Properties.ValidateOnEnterKey = True
            Me.ClientID_client.Size = New Size(116, 20)
            ToolTipTitleItem1.Text = "Client ID Number"
            ToolTipItem1.LeftIndent = 6
            ToolTipItem1.Text = "Enter the ID number of the client into this field. The ClientId must be acceptable " &
                                "for deposits based upon their active state."
            SuperToolTip1.Items.Add(ToolTipTitleItem1)
            SuperToolTip1.Items.Add(ToolTipItem1)
            Me.ClientID_client.SuperTip = SuperToolTip1
            Me.ClientID_client.TabIndex = 1
            '
            'SimpleButton_Notes
            '
            Me.SimpleButton_Notes.Anchor = CType((AnchorStyles.Top Or AnchorStyles.Right), AnchorStyles)
            Me.SimpleButton_Notes.Location = New Point(222, 27)
            Me.SimpleButton_Notes.Name = "SimpleButton_Notes"
            Me.SimpleButton_Notes.Size = New Size(75, 23)
            ToolTipTitleItem2.Text = "Client Deposit Note"
            ToolTipItem2.LeftIndent = 6
            ToolTipItem2.Text = "If the client included a note along with the deposit describing how the money sho" &
                                "uld be disbursed, pressing this button will allow you to create the disbursement" &
                                " note."
            SuperToolTip2.Items.Add(ToolTipTitleItem2)
            SuperToolTip2.Items.Add(ToolTipItem2)
            Me.SimpleButton_Notes.SuperTip = SuperToolTip2
            Me.SimpleButton_Notes.TabIndex = 10
            Me.SimpleButton_Notes.TabStop = False
            Me.SimpleButton_Notes.Text = "&Note..."
            '
            'LabelControl9
            '
            Me.LabelControl9.Location = New Point(6, 134)
            Me.LabelControl9.Name = "LabelControl9"
            Me.LabelControl9.Size = New Size(64, 13)
            Me.LabelControl9.TabIndex = 8
            Me.LabelControl9.Text = "&Date of Draft"
            '
            'TextEdit_reference
            '
            Me.TextEdit_reference.Anchor = CType(((AnchorStyles.Top Or AnchorStyles.Left) _
                                                  Or AnchorStyles.Right), 
                                                 AnchorStyles)
            Me.TextEdit_reference.EnterMoveNextControl = True
            Me.TextEdit_reference.Location = New Point(88, 105)
            Me.TextEdit_reference.Name = "TextEdit_reference"
            Me.TextEdit_reference.Size = New Size(209, 20)
            ToolTipTitleItem8.Text = "Reference Number"
            ToolTipItem8.LeftIndent = 6
            ToolTipItem8.Text = "Include the check or money order number here."
            SuperToolTip8.Items.Add(ToolTipTitleItem8)
            SuperToolTip8.Items.Add(ToolTipItem8)
            Me.TextEdit_reference.SuperTip = SuperToolTip8
            Me.TextEdit_reference.TabIndex = 7
            '
            'LabelControl8
            '
            Me.LabelControl8.Location = New Point(6, 109)
            Me.LabelControl8.Name = "LabelControl8"
            Me.LabelControl8.Size = New Size(64, 13)
            Me.LabelControl8.TabIndex = 6
            Me.LabelControl8.Text = "&Reference ID"
            '
            'LabelControl7
            '
            Me.LabelControl7.Location = New Point(6, 84)
            Me.LabelControl7.Name = "LabelControl7"
            Me.LabelControl7.Size = New Size(65, 13)
            Me.LabelControl7.TabIndex = 4
            Me.LabelControl7.Text = "&Type of Draft"
            '
            'LookUpEdit_tran_subtype
            '
            Me.LookUpEdit_tran_subtype.Anchor = CType(((AnchorStyles.Top Or AnchorStyles.Left) _
                                                       Or AnchorStyles.Right), 
                                                      AnchorStyles)
            Me.LookUpEdit_tran_subtype.EnterMoveNextControl = True
            Me.LookUpEdit_tran_subtype.Location = New Point(88, 80)
            Me.LookUpEdit_tran_subtype.Name = "LookUpEdit_tran_subtype"
            Me.LookUpEdit_tran_subtype.Properties.Buttons.AddRange(New EditorButton() {New EditorButton(ButtonPredefines.Combo)})
            Me.LookUpEdit_tran_subtype.Properties.Columns.AddRange(New LookUpColumnInfo() {New LookUpColumnInfo("tran_subtype", "ID", 20, FormatType.None, "", False, HorzAlignment.[Default]), New LookUpColumnInfo("description", "Description", 20, FormatType.None, "", True, HorzAlignment.[Default], ColumnSortOrder.Ascending), New LookUpColumnInfo("restricted", "Restricted", 20, FormatType.Numeric, "{0:d}", False, HorzAlignment.[Default]), New LookUpColumnInfo("sort_order", "Sequence", 20, FormatType.Numeric, "{0:f0}", False, HorzAlignment.[Default], ColumnSortOrder.Ascending)})
            Me.LookUpEdit_tran_subtype.Properties.DisplayMember = "description"
            Me.LookUpEdit_tran_subtype.Properties.NullText = ""
            Me.LookUpEdit_tran_subtype.Properties.ShowFooter = False
            Me.LookUpEdit_tran_subtype.Properties.ShowHeader = False
            Me.LookUpEdit_tran_subtype.Properties.ValueMember = "tran_subtype"
            Me.LookUpEdit_tran_subtype.Size = New Size(209, 20)
            ToolTipTitleItem4.Text = "Type of deposit item"
            ToolTipItem4.LeftIndent = 6
            ToolTipItem4.Text = "This is the type of the deposit. It may be a money order, a check, or cash. Perso" &
                                "nal checks are not normally accepted."
            SuperToolTip4.Items.Add(ToolTipTitleItem4)
            SuperToolTip4.Items.Add(ToolTipItem4)
            Me.LookUpEdit_tran_subtype.SuperTip = SuperToolTip4
            Me.LookUpEdit_tran_subtype.TabIndex = 5
            Me.LookUpEdit_tran_subtype.Properties.SortColumnIndex = 1
            '
            'CalcEdit_deposit_amount
            '
            Me.CalcEdit_deposit_amount.EnterMoveNextControl = True
            Me.CalcEdit_deposit_amount.Location = New Point(88, 55)
            Me.CalcEdit_deposit_amount.Name = "CalcEdit_deposit_amount"
            Me.CalcEdit_deposit_amount.Properties.Appearance.Options.UseTextOptions = True
            Me.CalcEdit_deposit_amount.Properties.Appearance.TextOptions.HAlignment = HorzAlignment.Far
            Me.CalcEdit_deposit_amount.Properties.AppearanceDisabled.Options.UseTextOptions = True
            Me.CalcEdit_deposit_amount.Properties.AppearanceDisabled.TextOptions.HAlignment = HorzAlignment.Far
            Me.CalcEdit_deposit_amount.Properties.AppearanceDropDown.Options.UseTextOptions = True
            Me.CalcEdit_deposit_amount.Properties.AppearanceDropDown.TextOptions.HAlignment = HorzAlignment.Far
            Me.CalcEdit_deposit_amount.Properties.AppearanceFocused.Options.UseTextOptions = True
            Me.CalcEdit_deposit_amount.Properties.AppearanceFocused.TextOptions.HAlignment = HorzAlignment.Far
            Me.CalcEdit_deposit_amount.Properties.AppearanceReadOnly.Options.UseTextOptions = True
            Me.CalcEdit_deposit_amount.Properties.AppearanceReadOnly.TextOptions.HAlignment = HorzAlignment.Far
            Me.CalcEdit_deposit_amount.Properties.Buttons.AddRange(New EditorButton() {New EditorButton(ButtonPredefines.Combo)})
            Me.CalcEdit_deposit_amount.Properties.DisplayFormat.FormatString = "{0:c}"
            Me.CalcEdit_deposit_amount.Properties.DisplayFormat.FormatType = FormatType.Numeric
            Me.CalcEdit_deposit_amount.Properties.EditFormat.FormatString = "{0:c}"
            Me.CalcEdit_deposit_amount.Properties.EditFormat.FormatType = FormatType.Numeric
            Me.CalcEdit_deposit_amount.Properties.Mask.BeepOnError = True
            Me.CalcEdit_deposit_amount.Properties.Mask.EditMask = "c"
            Me.CalcEdit_deposit_amount.Properties.Precision = 2
            Me.CalcEdit_deposit_amount.Size = New Size(116, 20)
            ToolTipTitleItem5.Text = "Dollar amount"
            ToolTipItem5.LeftIndent = 6
            ToolTipItem5.Text = "Enter the amount of the deposit. If you wish, there is a calculator for adding, o" &
                                "r subtracting items. We accept only a single dollar amount for each item."
            SuperToolTip5.Items.Add(ToolTipTitleItem5)
            SuperToolTip5.Items.Add(ToolTipItem5)
            Me.CalcEdit_deposit_amount.SuperTip = SuperToolTip5
            Me.CalcEdit_deposit_amount.TabIndex = 3
            '
            'LabelControl6
            '
            Me.LabelControl6.Location = New Point(6, 59)
            Me.LabelControl6.Name = "LabelControl6"
            Me.LabelControl6.Size = New Size(37, 13)
            Me.LabelControl6.TabIndex = 2
            Me.LabelControl6.Text = "&Amount"
            '
            'LabelControl5
            '
            Me.LabelControl5.Location = New Point(6, 34)
            Me.LabelControl5.Name = "LabelControl5"
            Me.LabelControl5.Size = New Size(27, 13)
            Me.LabelControl5.TabIndex = 0
            Me.LabelControl5.Text = "&Client"
            '
            'DepositItemBaseControl
            '
            Me.AutoScaleDimensions = New SizeF(6.0!, 13.0!)
            Me.AutoScaleMode = AutoScaleMode.Font
            Me.Controls.Add(Me.GroupControl2)
            Me.Controls.Add(Me.GroupControl1)
            Me.Name = "DepositItemBaseControl"
            Me.Size = New Size(309, 307)
            CType(Me.GroupControl1, ISupportInitialize).EndInit()
            Me.GroupControl1.ResumeLayout(False)
            Me.GroupControl1.PerformLayout()
            CType(Me.GroupControl2, ISupportInitialize).EndInit()
            Me.GroupControl2.ResumeLayout(False)
            Me.GroupControl2.PerformLayout()
            CType(Me.TextEditItemDate.Properties, ISupportInitialize).EndInit()
            CType(Me.ClientID_client.Properties, ISupportInitialize).EndInit()
            CType(Me.TextEdit_reference.Properties, ISupportInitialize).EndInit()
            CType(Me.LookUpEdit_tran_subtype.Properties, ISupportInitialize).EndInit()
            CType(Me.CalcEdit_deposit_amount.Properties, ISupportInitialize).EndInit()
            Me.ResumeLayout(False)
        End Sub

        Protected Friend WithEvents GroupControl1 As GroupControl
        Protected Friend WithEvents LabelControl4 As LabelControl
        Protected Friend WithEvents LabelControl3 As LabelControl
        Protected Friend WithEvents LabelControl2 As LabelControl
        Protected Friend WithEvents LabelControl1 As LabelControl
        Protected Friend WithEvents LabelControl_coapplicant As LabelControl
        Protected Friend WithEvents LabelControl_applicant As LabelControl
        Protected Friend WithEvents LabelControl_address As LabelControl
        Protected Friend WithEvents LabelControl_deposit_amount As LabelControl
        Protected Friend WithEvents GroupControl2 As GroupControl
        Protected Friend WithEvents TextEdit_reference As TextEdit
        Protected Friend WithEvents LabelControl8 As LabelControl
        Protected Friend WithEvents LabelControl7 As LabelControl
        Protected Friend WithEvents LookUpEdit_tran_subtype As LookUpEdit
        Protected Friend WithEvents CalcEdit_deposit_amount As CalcEdit
        Protected Friend WithEvents LabelControl6 As LabelControl
        Protected Friend WithEvents LabelControl5 As LabelControl
        Protected Friend WithEvents LabelControl9 As LabelControl
        Protected Friend WithEvents SimpleButton_Notes As SimpleButton
        Protected Friend WithEvents ClientID_client As DebtPlus.UI.Client.Widgets.Controls.ClientID

#End Region

        Public Event Cancelled As EventHandler
        Public Event Completed As EventHandler

        Public Sub New(ByVal p As Form_DepositManual, ByVal ctx As DepositManualContext)
            MyBase.new(p, ctx)
            parent = p
            InitializeComponent()

            AddHandler SimpleButton_Notes.Click, AddressOf SimpleButton_Notes_Click
            AddHandler CalcEdit_deposit_amount.EditValueChanging, AddressOf CalcEdit1_EditValueChanging
            AddHandler ClientID_client.DoubleClick, AddressOf ClientID_client_DoubleClick
            AddHandler ClientID_client.SearchCompleted, AddressOf ClientID_client_SearchCompleted
            AddHandler ClientID_client.Validated, AddressOf ClientID_client_Validated
            AddHandler ClientID_client.Validating, AddressOf ClientID_client_Validating
            AddHandler TextEditItemDate.Leave, AddressOf TextEditItemDate_Leave
            AddHandler Me.Load, AddressOf DepositItemBaseControl_Load
        End Sub

        Protected Overridable Sub OnCancelled(ByVal e As EventArgs)
            RaiseEvent Cancelled(Me, e)
        End Sub

        Protected Sub RaiseCancelled()
            OnCancelled(EventArgs.Empty)
        End Sub

        Protected Overridable Sub OnCompleted(ByVal e As EventArgs)
            RaiseEvent Completed(Me, e)
        End Sub

        Protected Sub RaiseCompleted()
            OnCompleted(EventArgs.Empty)
        End Sub

        Protected Property DepositItemDate() As Object
            Get
                Dim item As Object = TextEditItemDate.EditValue
                If item Is Nothing OrElse item Is DBNull.Value Then Return DBNull.Value
                Dim Result As DateTime = Date.MinValue
                Dim str As String = Convert.ToString(item).Trim()
                If str = String.Empty Then Return DBNull.Value

                ' Try the standard date parse to find the item date
                If Date.TryParse(str, Result) Then
                    Return Result
                End If

                Dim month As Int32
                Dim day As Int32
                Dim year As Int32

                ' Look for the month, day, and year
                Dim MatchItem As Match = Regex.Match(str, "(<month>\d{1,2})(<day>\d\d)(<year>\d\d\d\d)")
                If Not MatchItem.Success Then
                    MatchItem = Regex.Match(str, "(?<month>\d{1,2})(?<day>\d\d)(?<year>\d\d)")
                End If

                If MatchItem.Success Then
                    year = Convert.ToInt32(MatchItem.Groups("year").Value)
                    If year < 100 Then
                        year += 1900
                        If year < 1950 Then year += 100
                    End If
                Else
                    year = Now.Year

                    ' Look for the month and day entry
                    MatchItem = Regex.Match(str, "(?<month>\d{1,2})(?<day>\d\d)")
                    If Not MatchItem.Success Then
                        month = Now.Month

                        ' Look for the day of the month as a single number entry
                        MatchItem = Regex.Match(str, "(?<day>\d{1,2})")
                        If Not MatchItem.Success Then
                            Return DBNull.Value
                        End If

                        ' Try to convert the item to a date
                        Try
                            Result = New Date(year, month, day)
                        Catch ex As Exception
                        End Try

                        If Result = Date.MinValue Then
                            Try
                                Result = New Date(year, month, 1).AddMonths(-1)
                                Return New Date(Result.Year, Result.Month, day)
                            Catch ex As Exception
                            End Try

                            Return DBNull.Value
                        End If
                    End If
                End If

                If MatchItem.Success Then
                    month = Convert.ToInt32(MatchItem.Groups("month").Value)
                    day = Convert.ToInt32(MatchItem.Groups("day").Value)
                    Try
                        Result = New Date(year, month, day)
                        If Result > Now.Date Then
                            Result = Result.AddYears(-1)
                        End If
                        Return Result

                    Catch ex As Exception
                    End Try
                End If
                Return DBNull.Value
            End Get

            Set(ByVal value As Object)
                TextEditItemDate.EditValue = value
            End Set
        End Property

        Protected Property DepositClient() As Object
            Get
                Return DebtPlus.Utils.Nulls.ToDbNull(ClientID_client.EditValue)
            End Get
            Set(ByVal value As Object)
                ClientID_client.EditValue = DebtPlus.Utils.Nulls.v_Int32(value)
            End Set
        End Property

        Protected Property DepositAmount() As Object
            Get
                Return CalcEdit_deposit_amount.EditValue
            End Get
            Set(ByVal value As Object)
                CalcEdit_deposit_amount.EditValue = value
            End Set
        End Property

        Protected Property DepositTranSubtype() As Object
            Get
                Return LookUpEdit_tran_subtype.EditValue
            End Get
            Set(ByVal value As Object)
                LookUpEdit_tran_subtype.EditValue = value
            End Set
        End Property

        Protected Property DepositReference() As Object
            Get
                Return TextEdit_reference.EditValue
            End Get
            Set(ByVal value As Object)
                TextEdit_reference.EditValue = value
            End Set
        End Property

        Protected Overridable Function HasErrors() As Boolean
            If ReadClient <= 0 Then Return True

            ' Ensure that there is a deposit amount and type selected
            If DepositTranSubtype Is Nothing OrElse DepositTranSubtype Is DBNull.Value Then Return True
            If DepositAmount Is Nothing OrElse DepositAmount Is DBNull.Value Then Return True
            Return Convert.ToDecimal(DepositAmount) <= 0D
        End Function

        Protected ReadClient As Int32 = -1

        Protected Sub ReadClientInfo(ByVal client As Int32?)

            ' Empty the current address information
            LabelControl_address.Text = String.Empty
            LabelControl_applicant.Text = String.Empty
            LabelControl_coapplicant.Text = String.Empty
            LabelControl_deposit_amount.Text = String.Empty

            Dim ActiveStatus As String = "CRE"
            ReadClient = -1

            If client.GetValueOrDefault(0) > 0 Then
                Dim row As DataRow = depositManualForm.ClientRow(client.Value)
                If row IsNot Nothing Then
                    ReadClient = client.Value
                    If row("active_status") IsNot DBNull.Value Then ActiveStatus = Convert.ToString(row("active_status"))
                    If row("applicant") IsNot DBNull.Value Then LabelControl_applicant.Text = Convert.ToString(row("applicant"))
                    If row("coapplicant") IsNot DBNull.Value Then LabelControl_coapplicant.Text = Convert.ToString(row("coapplicant"))
                    If row("address") IsNot DBNull.Value Then LabelControl_address.Text = Convert.ToString(row("address"))
                    If row("deposit_amount") IsNot DBNull.Value Then LabelControl_deposit_amount.Text = String.Format("{0:c}", Convert.ToDecimal(row("deposit_amount")))
                End If
            End If

            ' If there is no client then indicate as such
            Dim ValidClient As Boolean = ReadClient > 0
            If ValidClient Then
                ClientID_client.ErrorText = String.Empty

                ' Show or hide the inactive client alert condition.
                If ActiveStatus.ToUpper() = "I" Then
                    ShowBlinkAlert()
                Else
                    HideBlinkAlert()
                End If
            Else
                HideBlinkAlert()
                ClientID_client.ErrorText = "Invalid Client ID"
            End If

            ' The notes are permitted once we have a valid client
            SimpleButton_Notes.Enabled = ValidClient

            ' Enable the record button based upon a valid record
            With CType(Parent, ClientDepositControl)
                .SimpleButton_ok.Enabled = Not HasErrors()
            End With
        End Sub

        Protected Sub SimpleButton_Notes_Click(ByVal sender As Object, ByVal e As EventArgs)
            Dim client As Int32 = ClientID_client.EditValue.GetValueOrDefault(-1)
            If ReadClient = client AndAlso client > 0 Then
                Using Notes As New NoteClass.DisbursementNote()
                    Notes.Create(client, ctx.ap.Batch)
                End Using
            End If
        End Sub

        Private Sub CalcEdit1_EditValueChanging(ByVal sender As Object, ByVal e As ChangingEventArgs)
            Const ErrorMessage As String = "Invalid amount"

            With CType(Parent, ClientDepositControl)
                Dim ErrorsFound As Boolean = True

                CalcEdit_deposit_amount.ErrorText = String.Empty
                Do

                    ' A client is required
                    If ReadClient <= 0 Then Exit Do

                    ' A deposit subtype is required. (It is unlikely that it is missing, but it is required none the less.)
                    If DepositTranSubtype Is Nothing OrElse DepositTranSubtype Is DBNull.Value Then Exit Do

                    ' Ensure that the new value is positive. If negative, complain. If zero then don't allow the OK button
                    If e.NewValue Is Nothing OrElse e.NewValue Is DBNull.Value Then
                        e.Cancel = True
                        CalcEdit_deposit_amount.ErrorText = ErrorMessage
                        Return
                    End If

                    Dim v As Decimal = Convert.ToDecimal(e.NewValue, CultureInfo.InvariantCulture)
                    If v < 0D Then
                        e.Cancel = True
                        CalcEdit_deposit_amount.ErrorText = ErrorMessage
                        Return
                    End If
                    If v = 0D Then Exit Do

                    ErrorsFound = False
                    Exit Do
                Loop

                .SimpleButton_ok.Enabled = Not ErrorsFound
            End With
        End Sub

        Public Overridable Sub ReadRecord()

            ' Read the client information if there is a ClientId to be read
            ReadClient = -1
            ReadClientInfo(ClientID_client.EditValue)

            ' Enable the accept button if there are no errors
            With CType(Parent, ClientDepositControl)
                .SimpleButton_ok.Enabled = Not HasErrors()
            End With
        End Sub

        Private Sub ClientID_client_DoubleClick(ByVal sender As Object, ByVal e As EventArgs)
            Dim id As Int32 = ClientID_client.EditValue.GetValueOrDefault(-1)
            If id > 0 Then
                Dim thrd As New Thread(New ParameterizedThreadStart(AddressOf ShowClientThread))
                thrd.IsBackground = True
                thrd.SetApartmentState(ApartmentState.STA)
                thrd.Start(id)
            End If
        End Sub

        Private Sub ClientID_client_SearchCompleted(ByVal sender As Object, ByVal e As CancelEventArgs)
            ClientID_client_Validating(sender, e)
            If Not e.Cancel Then
                e.Cancel = True
                If ClientID_client.ErrorText = String.Empty Then
                    CalcEdit_deposit_amount.Focus()
                End If
            End If
        End Sub

        Private Sub ClientID_client_Validated(sender As Object, e As EventArgs)
            Static LastClientShown As Int32 = -2

            ' Determine the client to be displayed. Ignore blank items.
            Dim NewClient As Int32 = ClientID_client.EditValue.GetValueOrDefault(-1)
            If NewClient > 0 Then

                ' Do not keep displaying the same client over and over again....
                If LastClientShown <> NewClient Then
                    LastClientShown = NewClient

                    ' Do the standard logic to display this client's alert notes.
                    If Defaults.ShowAlertNotesOnEntry() Then
                        Using Notes As New AlertNotes.Client()
                            Notes.ShowAlerts(NewClient)
                        End Using
                    End If
                End If
            End If
        End Sub

        Private Sub ClientID_client_Validating(ByVal sender As Object, ByVal e As CancelEventArgs)
            Dim NewClient As Int32 = ClientID_client.EditValue.GetValueOrDefault(-1)
            If NewClient <> ReadClient Then ReadClientInfo(NewClient)
        End Sub

        Private Sub TextEditItemDate_Leave(ByVal sender As Object, ByVal e As EventArgs)

            ' Redraw the information from the date entered
            DepositItemDate = DepositItemDate
        End Sub

        Private Sub DepositItemBaseControl_Load(ByVal sender As Object, ByVal e As EventArgs)

            ' Set the status for the various items
            ClientID_client.EnterMoveNextControl = EnterMoveNextControl()
            CalcEdit_deposit_amount.EnterMoveNextControl = EnterMoveNextControl()
            LookUpEdit_tran_subtype.EnterMoveNextControl = EnterMoveNextControl()
            TextEdit_reference.EnterMoveNextControl = EnterMoveNextControl()
            TextEditItemDate.EnterMoveNextControl = EnterMoveNextControl()
        End Sub

        Public Sub ShowBlinkAlert()
            BlinkLabel1.Enabled = True
        End Sub

        Public Sub HideBlinkAlert()
            BlinkLabel1.Enabled = False
        End Sub

        Private Sub ShowClientThread(obj As Object)
            Dim clientID As Int32 = Convert.ToInt32(obj)
            If ValidClient(clientID) Then

                ' If the alert notes were not shown upon entering then display them now.
                If Defaults.ShowAlertNotesOnEdit() Then
                    Using cls As New AlertNotes.Client()
                        cls.ShowAlerts(clientID)
                    End Using
                End If

                ' Do the client edit operation
                Using cuc As ClientUpdateClass = New ClientUpdateClass()
                    cuc.ShowEditDialog(clientID, False)
                End Using
            End If
        End Sub

        Private Function ValidClient(ByVal ClientID As Int32) As Boolean
            Dim answer As Boolean = False
            Dim cn As SqlConnection = New SqlConnection(DebtPlus.LINQ.SQLInfoClass.getDefault().ConnectionString)
            Try
                cn.Open()
                Using cmd As SqlCommand = New SqlCommand()
                    With cmd
                        .Connection = cn
                        .CommandText = "SELECT client from clients with (nolock) where client=@client"
                        .CommandType = CommandType.Text
                        .Parameters.Add("@client", SqlDbType.Int).Value = ClientID
                    End With

                    Dim objAnswer As Object = cmd.ExecuteScalar
                    If objAnswer IsNot Nothing AndAlso objAnswer IsNot DBNull.Value Then answer = True
                End Using

            Catch ex As Exception
                DebtPlus.Svc.Common.ErrorHandling.HandleErrors(ex)

            Finally
                If cn IsNot Nothing Then cn.Dispose()
            End Try

            Return answer
        End Function
    End Class
End Namespace
