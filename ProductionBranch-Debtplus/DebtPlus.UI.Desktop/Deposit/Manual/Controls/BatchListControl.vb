#Region "Copyright 2000-2012 DebtPlus, L.L.C."
'{*******************************************************************}
'{                                                                   }
'{       DebtPlus Debt Management System                             }
'{                                                                   }
'{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
'{       ALL RIGHTS RESERVED                                         }
'{                                                                   }
'{   The entire contents of this file is protected by U.S. and       }
'{   International Copyright Laws. Unauthorized reproduction,        }
'{   reverse-engineering, and distribution of all or any portion of  }
'{   the code contained in this file is strictly prohibited and may  }
'{   result in severe civil and criminal penalties and will be       }
'{   prosecuted to the maximum extent possible under the law.        }
'{                                                                   }
'{   RESTRICTIONS                                                    }
'{                                                                   }
'{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
'{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
'{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
'{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
'{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
'{                                                                   }
'{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
'{   ADDITIONAL RESTRICTIONS.                                        }
'{                                                                   }
'{*******************************************************************}
#End Region

Namespace Deposit.Manual.Controls
    Friend Class BatchListControl
        Private vue As DataView

        Public Sub New(ByVal p As Form_DepositManual, ByVal ctx As DepositManualContext)
            MyBase.New(p, ctx)
            InitializeComponent()
            AddHandler ListBoxControl1.DoubleClick, AddressOf ListBoxControl1_DoubleClick
            AddHandler ListBoxControl1.DrawItem, AddressOf ListBoxControl1_DrawItem
        End Sub

        ''' <summary>
        ''' DOUBLE-CLICK is "EDIT"
        ''' </summary>
        Private Sub ListBoxControl1_DoubleClick(ByVal sender As Object, ByVal e As EventArgs)
            With CType(sender, DevExpress.XtraEditors.ListBoxControl)

                ' Find where the use clicked
                Dim HitIndex As Int32 = .IndexFromPoint(.PointToClient(Windows.Forms.Control.MousePosition))

                ' If the user clicked on an item then it should be zero or positive. Blank area = negative
                If HitIndex >= 0 Then

                    ' From the index, find the row being clicked.
                    Dim new_drv As DataRowView = vue(HitIndex)

                    ' The row can not be the one that we are just creating
                    If Not new_drv.IsNew Then
                        new_drv.BeginEdit()

                        ' Switch back to the add control
                        With CType(Parent, ClientDepositControl)
                            .NewDepositItemControl1.GiveFocus()
                            .DepositItemControl1.TakeFocus()
                            .DepositItemControl1.ReadRecord(new_drv)
                        End With
                    End If
                End If
            End With
        End Sub

        ''' <summary>
        ''' Load the batch list on the first call
        ''' </summary>
        Public Sub LoadList(ByVal tbl As DataTable)

            ' Update the list with the display information
            vue = New DataView(tbl, String.Empty, "deposit_batch_detail desc", DataViewRowState.CurrentRows)

            With ListBoxControl1
                .BeginUpdate()
                .DataSource = vue
                .DisplayMember = "amount"
                .ValueMember = "deposit_batch_detail"
                .EndUpdate()
            End With

        End Sub

        ''' <summary>
        ''' Draw the items with a dollar sign
        ''' </summary>
        Private Sub ListBoxControl1_DrawItem(ByVal sender As Object, ByVal e As DevExpress.XtraEditors.ListBoxDrawItemEventArgs)
            With CType(sender, DevExpress.XtraEditors.ListBoxControl)

                ' Put the background in the object
                e.Appearance.DrawBackground(e.Cache, e.Bounds)

                ' Find the item for the display
                If e.Index >= 0 Then

                    ' From the index, find the row.
                    Dim row As DataRow = CType(.DataSource, DataView).Item(e.Index).Row

                    ' From the row, find the amount to be displayed
                    Dim CreditAmt As Decimal = 0D
                    If row(.DisplayMember) IsNot Nothing AndAlso row(.DisplayMember) IsNot DBNull.Value Then CreditAmt = Convert.ToDecimal(row(.DisplayMember))

                    ' Format the display string and draw it
                    Dim txt As String = String.Format("{0:c}", CreditAmt)
                    e.Appearance.DrawString(e.Cache, txt, e.Bounds)
                End If

                ' Indicate that we displayed the row and do not do normal processing
                e.Handled = True
            End With
        End Sub

        Public Sub ScrollToRecord(ByVal Record As System.Data.DataRow)

            ' Ensure that the top most item is visable at the insertion. This is where the new item is going to be inserted
            ' if it is not inserted yet.
            With ListBoxControl1
                If .ItemCount > 0 Then
                    ListBoxControl1.MakeItemVisible(0)
                    ListBoxControl1.SelectedIndex = 0
                End If
            End With
        End Sub
    End Class
End Namespace
