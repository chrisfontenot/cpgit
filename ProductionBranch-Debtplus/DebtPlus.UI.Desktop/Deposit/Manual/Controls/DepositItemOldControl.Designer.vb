﻿Namespace Deposit.Manual.Controls

    <Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
    Partial Class DepositItemOldControl
        Inherits DepositItemBaseControl

        'UserControl overrides dispose to clean up the component list.
        <System.Diagnostics.DebuggerNonUserCode()> _
        Protected Overrides Sub Dispose(ByVal disposing As Boolean)
            Try
                If disposing AndAlso components IsNot Nothing Then
                    components.Dispose()
                End If
            Finally
                MyBase.Dispose(disposing)
            End Try
        End Sub

        'Required by the Windows Form Designer
        Private components As System.ComponentModel.IContainer

        'NOTE: The following procedure is required by the Windows Form Designer
        'It can be modified using the Windows Form Designer.  
        'Do not modify it using the code editor.
        <System.Diagnostics.DebuggerStepThrough()> _
        Private Sub InitializeComponent()
            CType(Me.GroupControl1, System.ComponentModel.ISupportInitialize).BeginInit()
            Me.GroupControl1.SuspendLayout()
            CType(Me.GroupControl2, System.ComponentModel.ISupportInitialize).BeginInit()
            Me.GroupControl2.SuspendLayout()
            CType(Me.TextEdit_reference.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.LookUpEdit_tran_subtype.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.CalcEdit_deposit_amount.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.ClientID_client.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
            Me.SuspendLayout()
            '
            'LabelControl4
            '
            Me.LabelControl4.Appearance.BackColor = System.Drawing.Color.Transparent
            Me.LabelControl4.Appearance.Options.UseBackColor = True
            '
            'LabelControl3
            '
            Me.LabelControl3.Appearance.BackColor = System.Drawing.Color.Transparent
            Me.LabelControl3.Appearance.Options.UseBackColor = True
            '
            'LabelControl2
            '
            Me.LabelControl2.Appearance.BackColor = System.Drawing.Color.Transparent
            Me.LabelControl2.Appearance.Options.UseBackColor = True
            '
            'LabelControl1
            '
            Me.LabelControl1.Appearance.BackColor = System.Drawing.Color.Transparent
            Me.LabelControl1.Appearance.Options.UseBackColor = True
            '
            'LabelControl_coapplicant
            '
            Me.LabelControl_coapplicant.Appearance.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Int32), CType(CType(255, Byte), Int32), CType(CType(192, Byte), Int32))
            Me.LabelControl_coapplicant.Appearance.Options.UseBackColor = True
            '
            'LabelControl_applicant
            '
            Me.LabelControl_applicant.Appearance.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Int32), CType(CType(255, Byte), Int32), CType(CType(192, Byte), Int32))
            Me.LabelControl_applicant.Appearance.Options.UseBackColor = True
            '
            'LabelControl_address
            '
            Me.LabelControl_address.Appearance.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Int32), CType(CType(255, Byte), Int32), CType(CType(192, Byte), Int32))
            Me.LabelControl_address.Appearance.Options.UseBackColor = True
            Me.LabelControl_address.Appearance.Options.UseTextOptions = True
            Me.LabelControl_address.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
            Me.LabelControl_address.Appearance.TextOptions.Trimming = DevExpress.Utils.Trimming.EllipsisCharacter
            Me.LabelControl_address.Appearance.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Top
            Me.LabelControl_address.Appearance.TextOptions.WordWrap = DevExpress.Utils.WordWrap.NoWrap
            '
            'LabelControl_deposit_amount
            '
            Me.LabelControl_deposit_amount.Appearance.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Int32), CType(CType(255, Byte), Int32), CType(CType(192, Byte), Int32))
            Me.LabelControl_deposit_amount.Appearance.Options.UseBackColor = True
            Me.LabelControl_deposit_amount.Appearance.Options.UseTextOptions = True
            Me.LabelControl_deposit_amount.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
            '
            'TextEdit1
            '
            '
            'LookUpEdit1
            '
            '
            'CalcEdit1
            '
            Me.CalcEdit_deposit_amount.Properties.Appearance.Options.UseTextOptions = True
            Me.CalcEdit_deposit_amount.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
            Me.CalcEdit_deposit_amount.Properties.AppearanceDisabled.Options.UseTextOptions = True
            Me.CalcEdit_deposit_amount.Properties.AppearanceDisabled.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
            Me.CalcEdit_deposit_amount.Properties.AppearanceDropDown.Options.UseTextOptions = True
            Me.CalcEdit_deposit_amount.Properties.AppearanceDropDown.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
            Me.CalcEdit_deposit_amount.Properties.AppearanceFocused.Options.UseTextOptions = True
            Me.CalcEdit_deposit_amount.Properties.AppearanceFocused.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
            Me.CalcEdit_deposit_amount.Properties.AppearanceReadOnly.Options.UseTextOptions = True
            Me.CalcEdit_deposit_amount.Properties.AppearanceReadOnly.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
            Me.CalcEdit_deposit_amount.Properties.DisplayFormat.FormatString = "{0:c}"
            Me.CalcEdit_deposit_amount.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
            Me.CalcEdit_deposit_amount.Properties.EditFormat.FormatString = "{0:c}"
            Me.CalcEdit_deposit_amount.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric
            Me.CalcEdit_deposit_amount.Properties.Mask.BeepOnError = True
            Me.CalcEdit_deposit_amount.Properties.Mask.EditMask = "c"
            '
            'ClientID1
            '
            Me.ClientID_client.Properties.Appearance.Options.UseTextOptions = True
            Me.ClientID_client.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
            Me.ClientID_client.Properties.DisplayFormat.FormatString = "0000000"
            Me.ClientID_client.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Custom
            Me.ClientID_client.Properties.EditFormat.FormatString = "f0"
            Me.ClientID_client.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric
            Me.ClientID_client.Properties.Mask.BeepOnError = True
            Me.ClientID_client.Properties.Mask.EditMask = "\d*"
            Me.ClientID_client.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.RegEx
            '
            'DepositItemOldControl
            '
            Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
            Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
            Me.Name = "DepositItemOldControl"
            CType(Me.GroupControl1, System.ComponentModel.ISupportInitialize).EndInit()
            Me.GroupControl1.ResumeLayout(False)
            Me.GroupControl1.PerformLayout()
            CType(Me.GroupControl2, System.ComponentModel.ISupportInitialize).EndInit()
            Me.GroupControl2.ResumeLayout(False)
            Me.GroupControl2.PerformLayout()
            CType(Me.TextEdit_reference.Properties, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.LookUpEdit_tran_subtype.Properties, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.CalcEdit_deposit_amount.Properties, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.ClientID_client.Properties, System.ComponentModel.ISupportInitialize).EndInit()
            Me.ResumeLayout(False)

        End Sub
    End Class
End Namespace