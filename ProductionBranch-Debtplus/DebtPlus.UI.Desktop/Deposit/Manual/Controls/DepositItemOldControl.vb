#Region "Copyright 2000-2012 DebtPlus, L.L.C."
'{*******************************************************************}
'{                                                                   }
'{       DebtPlus Debt Management System                             }
'{                                                                   }
'{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
'{       ALL RIGHTS RESERVED                                         }
'{                                                                   }
'{   The entire contents of this file is protected by U.S. and       }
'{   International Copyright Laws. Unauthorized reproduction,        }
'{   reverse-engineering, and distribution of all or any portion of  }
'{   the code contained in this file is strictly prohibited and may  }
'{   result in severe civil and criminal penalties and will be       }
'{   prosecuted to the maximum extent possible under the law.        }
'{                                                                   }
'{   RESTRICTIONS                                                    }
'{                                                                   }
'{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
'{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
'{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
'{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
'{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
'{                                                                   }
'{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
'{   ADDITIONAL RESTRICTIONS.                                        }
'{                                                                   }
'{*******************************************************************}
#End Region

Namespace Deposit.Manual.Controls

    Friend Class DepositItemOldControl

        Public Sub New(ByVal p As Form_DepositManual, ByVal ctx As DepositManualContext)
            MyBase.New(p, ctx)
            InitializeComponent()
        End Sub

        Private EditDrv As DataRowView
        Public Overloads Sub ReadRecord(ByVal drv As DataRowView)
            EditDrv = drv

            ' Ensure that the list is loaded
            If LookUpEdit_tran_subtype.Properties.DataSource Is Nothing Then
                LookUpEdit_tran_subtype.Properties.DataSource = New DataView(depositManualForm.DepositSubtypeTable, String.Empty, "sort_order, description", DataViewRowState.CurrentRows)
                AddHandler LookUpEdit_tran_subtype.EditValueChanging, AddressOf DebtPlus.Data.Validation.LookUpEdit_ActiveTest
            End If

            ' Load the control with the values
            DepositClient = EditDrv("client")
            DepositAmount = EditDrv("amount")
            DepositTranSubtype = EditDrv("tran_subtype")
            DepositReference = EditDrv("reference")

            Dim obj As Object = EditDrv("item_date")
            If obj Is Nothing OrElse obj Is System.DBNull.Value Then obj = DateTime.Now.Date
            DepositItemDate = obj

            MyBase.ReadRecord()
        End Sub

        Public Sub CancelEvent()

            ' Stop the alert from blinking
            HideBlinkAlert()

            ' Cancel the changes to this row
            EditDrv.CancelEdit()

            ' Switch back to the add control
            With CType(Parent, ClientDepositControl)
                .DepositItemControl1.GiveFocus()
                .NewDepositItemControl1.TakeFocus()
            End With
        End Sub

        Public Sub AcceptEvent()

            ' Stop the alert from blinking
            HideBlinkAlert()

            ' Complete the edit operation on the current row
            Dim obj As Object = DepositItemDate
            If obj Is Nothing OrElse obj Is System.DBNull.Value Then obj = DateTime.Now.Date
            EditDrv("item_date") = obj

            EditDrv("client") = DepositClient
            EditDrv("amount") = DepositAmount
            EditDrv("tran_subtype") = DepositTranSubtype
            EditDrv("reference") = DepositReference

            EditDrv.EndEdit()

            ' Switch back to the add control
            With CType(Parent, ClientDepositControl)
                .DepositItemControl1.GiveFocus()
                .NewDepositItemControl1.TakeFocus()
            End With
        End Sub

        Public Sub TakeFocus()

            ' Bring our control to the front
            With Me
                .BringToFront()
                .TabStop = True
                .Visible = True

                ' Go to the client id again.
                .Focus()
                .ClientID_client.Focus()
            End With

            ' Change the parent form to suit our needs
            With CType(Parent, ClientDepositControl)

                ' Disable the batch list control
                .BatchListControl1.Enabled = False

                ' Change the form buttons to come here when they are pressed
                With .SimpleButton_cancel
                    .Text = "&Cancel"
                    .Tag = "cancel"
                End With

                With .SimpleButton_ok
                    .Text = "&OK"
                    .Tag = "ok"
                    .Enabled = Not HasErrors()
                End With
            End With
        End Sub

        Public Sub GiveFocus()

            ' Hide our own input form since we don't need it anymore.
            With Me
                .SendToBack()
                .Visible = False
                .TabStop = False
            End With
        End Sub
    End Class

End Namespace
