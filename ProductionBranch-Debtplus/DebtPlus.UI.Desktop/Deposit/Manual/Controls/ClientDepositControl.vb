#Region "Copyright 2000-2012 DebtPlus, L.L.C."
'{*******************************************************************}
'{                                                                   }
'{       DebtPlus Debt Management System                             }
'{                                                                   }
'{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
'{       ALL RIGHTS RESERVED                                         }
'{                                                                   }
'{   The entire contents of this file is protected by U.S. and       }
'{   International Copyright Laws. Unauthorized reproduction,        }
'{   reverse-engineering, and distribution of all or any portion of  }
'{   the code contained in this file is strictly prohibited and may  }
'{   result in severe civil and criminal penalties and will be       }
'{   prosecuted to the maximum extent possible under the law.        }
'{                                                                   }
'{   RESTRICTIONS                                                    }
'{                                                                   }
'{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
'{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
'{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
'{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
'{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
'{                                                                   }
'{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
'{   ADDITIONAL RESTRICTIONS.                                        }
'{                                                                   }
'{*******************************************************************}

#End Region

Imports System.Data.SqlClient
Imports DevExpress.XtraBars
Imports DebtPlus.Utils
Imports DevExpress.XtraEditors
Imports System.Threading
Imports DebtPlus.UI.Desktop.Deposit.Manual.Report
Imports DevExpress.XtraPrinting.Drawing
Imports System.Windows.Forms
Imports System.Drawing
Imports DebtPlus.Configuration
Imports DevExpress.XtraReports.UI

Namespace Deposit.Manual.Controls
    Friend Class ClientDepositControl
        'Private ReadOnly ctx As DepositManualContext = Nothing
        Dim _tbl As DataTable = Nothing

        Public Sub New(ByVal p As Form_DepositManual, ByVal ctx As DepositManualContext)
            MyBase.new(p, ctx)
            InitializeComponent()
            AddHandler NewDepositItemControl1.Cancelled, AddressOf NewDepositItemControl1_Cancelled
            AddHandler NewDepositItemControl1.Completed, AddressOf NewDepositItemControl1_Completed
            AddHandler SimpleButton_ok.Click, AddressOf SimpleButton_ok_Click
            AddHandler SimpleButton_ok.Enter, AddressOf SimpleButton_ok_Enter
            AddHandler Me.Load, AddressOf ClientDepositControl_Load
            AddHandler NewDepositItemControl1.RecordAdded, AddressOf NewDepositItemControl1_RecordAdded
            AddHandler SimpleButton_cancel.Click, AddressOf SimpleButton_ok_Click

            DepositItemControl1.GiveFocus()
            NewDepositItemControl1.TakeFocus()
            _tbl = ctx.ds.Tables("deposit_batch_details")
        End Sub

        Public Event Cancelled As EventHandler

        Protected Overridable Sub OnCancelled(ByVal e As EventArgs)
            RaiseEvent Cancelled(Me, e)
        End Sub

        Protected Sub RaiseCancelled()
            OnCancelled(EventArgs.Empty)
        End Sub

        Public Sub ReadForm()

            ' Add a handler to the menu for printing the report
            With depositManualForm
                Dim mgr As BarManager = .barManager1
                Dim newItem As New BarButtonItem(mgr, "&Print...")
                With newItem
                    .Name = "PrintReport"
                    AddHandler .ItemClick, AddressOf PrintReport
                End With
                .BarSubItem_File.InsertItem(.BarSubItem_File.ItemLinks(0), newItem)
            End With

            ' Read the list of pending items for the deposit details
            If _tbl Is Nothing Then
                Using cmd As SqlCommand = New SqlCommand
                    With cmd
                        .Connection = New SqlConnection(DebtPlus.LINQ.SQLInfoClass.getDefault().ConnectionString)
                        .CommandType = CommandType.Text
                        .CommandText = "SELECT [deposit_batch_detail],[deposit_batch_id],[tran_subtype],[ok_to_post],[scanned_client],[client],[creditor],[client_creditor],[amount],[fairshare_amt],[fairshare_pct],[creditor_type],[item_date],[reference],[message],[ach_transaction_code],[ach_routing_number],[ach_account_number],[ach_authentication_code],[ach_response_batch_id],[date_posted],[created_by],[date_created] FROM deposit_batch_details WHERE deposit_batch_id = @deposit_batch ORDER BY deposit_batch_detail"
                        .Parameters.Add("@deposit_batch", SqlDbType.Int).Value = ctx.ap.Batch
                    End With

                    Using da As New SqlDataAdapter(cmd)
                        da.Fill(ctx.ds, "deposit_batch_details")
                        _tbl = ctx.ds.Tables("deposit_batch_details")
                    End Using
                End Using
            End If

            ' Supply the necessary default values for the columns
            With _tbl
                .PrimaryKey = New DataColumn() {.Columns("deposit_batch_detail")}

                With .Columns("deposit_batch_detail")
                    Dim objAnswer As Object = _tbl.Compute("max(deposit_batch_detail)", String.Empty)
                    If objAnswer Is Nothing OrElse objAnswer Is DBNull.Value Then objAnswer = 0

                    .AutoIncrement = True
                    .AutoIncrementSeed = Convert.ToInt32(objAnswer) + 1
                    .AutoIncrementStep = 1
                End With

                With .Columns("amount")
                    .DefaultValue = 0D
                End With

                With .Columns("client")
                    .DefaultValue = DBNull.Value
                End With

                With .Columns("item_date")
                    .DefaultValue = Now.Date
                End With

                With .Columns("tran_subtype")
                    Dim DefaultItem As String = DebtPlus.Configuration.Config.GetConfigValueForKey(DebtPlus.Configuration.Config.GetConfigSettings(DebtPlus.Configuration.Config.ConfigSet.DepositManual), "DefaultSubtype")
                    If String.IsNullOrEmpty(DefaultItem) OrElse DefaultItem.Trim() = String.Empty Then DefaultItem = "MO"
                    .DefaultValue = DefaultItem.Trim()
                End With
            End With

            ' Bind the table to the various controls
            BatchListControl1.LoadList(_tbl)
            BatchStatusControl1.LoadList(_tbl)

            ' Add a new row to the view
            NewDepositItemControl1.ReadRecord()
        End Sub

        Private Sub NewDepositItemControl1_Cancelled(ByVal sender As Object, ByVal e As EventArgs)
            RaiseCancelled()
        End Sub

        Private Sub NewDepositItemControl1_Completed(ByVal sender As Object, ByVal e As EventArgs)

            ' Update the database with the changed items
            Dim cn As SqlConnection = New SqlConnection(DebtPlus.LINQ.SQLInfoClass.getDefault().ConnectionString)
            Dim txn As SqlTransaction = Nothing
            Try
                cn.Open()
                txn = cn.BeginTransaction

                Using da As New SqlDataAdapter
                    da.UpdateCommand = UpdateCommand(cn, txn)
                    da.InsertCommand = InsertCommand(cn, txn)
                    da.DeleteCommand = DeleteCommand(cn, txn)

                    ' Perform the pending updates on the database
                    UseWaitCursor = True
                    da.Update(_tbl)
                    _tbl.AcceptChanges()
                End Using

                ' Commit the changes
                txn.Commit()
                txn = Nothing

                ' Close the connection while the dialog is pending
                cn.Close()
                UseWaitCursor = False

                ' Close the batch if desired....
                If DebtPlus.Data.Forms.MessageBox.Show(String.Format("Do you wish to close the current batch as well?{0}{0}Closing the batch will prevent further updates to the deposits and permit them{0}to be posted to the clients account.", Environment.NewLine), "Update completed", MessageBoxButtons.YesNo, MessageBoxIcon.Question) = DialogResult.Yes Then
                    UseWaitCursor = True
                    cn.Open()
                    Using cmd As SqlCommand = New SqlCommand
                        With cmd
                            .Connection = cn
                            .CommandText = "xpr_deposit_batch_close"
                            .CommandType = CommandType.StoredProcedure
                            .Parameters.Add("@deposit_batch_id", SqlDbType.Int).Value = ctx.ap.Batch
                            .ExecuteNonQuery()
                        End With
                    End Using
                End If

                ' Tell the application to terminate. At this point, we just want cancel since that is all that is being monitored.
                RaiseCancelled()

            Catch ex As DBConcurrencyException
                DebtPlus.Data.Forms.MessageBox.Show("The transaction(s) are no longer valid for this batch. The most common cause is that the batch has been posted and edits are no longer permitted.", "Database Consistency Check Violation", MessageBoxButtons.OK, MessageBoxIcon.Asterisk)

            Catch ex As Exception
                DebtPlus.UI.Common.ErrorHandling.HandleErrors(ex, "Error updating the deposit batch details")

            Finally
                If txn IsNot Nothing Then
                    Try
                        txn.Rollback()

                    Catch exRollback As Exception
                        DebtPlus.Svc.Common.ErrorHandling.HandleErrors(exRollback)
                    End Try

                    txn.Dispose()
                    txn = Nothing
                End If

                If cn IsNot Nothing Then cn.Dispose()

                UseWaitCursor = False
            End Try
        End Sub

        Private Function UpdateCommand(ByRef cn As SqlConnection, ByRef txn As SqlTransaction) As SqlCommand
            Dim cmd As SqlCommand = New SqlCommand
            With cmd
                .Connection = cn
                .Transaction = txn
                .CommandText = "UPDATE deposit_batch_details SET client=@client, amount=@amount, tran_subtype=@tran_subtype, reference=@reference, item_date=@item_date WHERE deposit_batch_detail=@deposit_batch_detail"
                .CommandType = CommandType.Text
                .Parameters.Add("@client", SqlDbType.Int, 0, "client")
                .Parameters.Add("@amount", SqlDbType.Decimal, 0, "amount")
                .Parameters.Add("@tran_subtype", SqlDbType.VarChar, 2, "tran_subtype")
                .Parameters.Add("@reference", SqlDbType.VarChar, 50, "reference")
                .Parameters.Add("@item_date", SqlDbType.DateTime, 0, "item_date")
                .Parameters.Add("@deposit_batch_detail", SqlDbType.Int, 0, "deposit_batch_detail")
            End With
            Return cmd
        End Function

        Private Function InsertCommand(ByRef cn As SqlConnection, ByRef txn As SqlTransaction) As SqlCommand
            Dim cmd As SqlCommand = New SqlCommand
            With cmd
                .Connection = cn
                .Transaction = txn
                .CommandText = "INSERT INTO deposit_batch_details(client,amount,tran_subtype,reference,item_date,ok_to_post,deposit_batch_id) values (@client,@amount,@tran_subtype,@reference,@item_date,1,@deposit_batch_id)"
                .CommandType = CommandType.Text
                .Parameters.Add("@client", SqlDbType.Int, 0, "client")
                .Parameters.Add("@amount", SqlDbType.Decimal, 0, "amount")
                .Parameters.Add("@tran_subtype", SqlDbType.VarChar, 2, "tran_subtype")
                .Parameters.Add("@reference", SqlDbType.VarChar, 50, "reference")
                .Parameters.Add("@item_date", SqlDbType.DateTime, 0, "item_date")
                .Parameters.Add("@deposit_batch_id", SqlDbType.Int).Value = ctx.ap.Batch
            End With
            Return cmd
        End Function

        Private Function DeleteCommand(ByRef cn As SqlConnection, ByRef txn As SqlTransaction) As SqlCommand
            Dim cmd As SqlCommand = New SqlCommand
            With cmd
                .Connection = cn
                .Transaction = txn
                .CommandText = "DELETE FROM deposit_batch_details WHERE deposit_batch_detail = @deposit_batch_detail AND deposit_batch_id=@deposit_batch_id"
                .CommandType = CommandType.Text
                .Parameters.Add("@deposit_batch_detail", SqlDbType.Int, 0, "deposit_batch_detail")
                .Parameters.Add("@deposit_batch_id", SqlDbType.Int).Value = ctx.ap.Batch
            End With
            Return cmd
        End Function

        Private Sub SimpleButton_ok_Click(ByVal sender As Object, ByVal e As EventArgs)
            With CType(sender, SimpleButton)
                Select Case Convert.ToString(.Tag)
                    Case "new cancel"
                        NewDepositItemControl1.CancelEvent()
                    Case "new ok"
                        NewDepositItemControl1.AcceptEvent()
                    Case "cancel"
                        DepositItemControl1.CancelEvent()
                    Case "ok"
                        DepositItemControl1.AcceptEvent()
                    Case Else
                        Debug.Assert(False, "Tag is not valid")
                End Select
            End With
        End Sub

        Private Sub PrintReport(ByVal sender As Object, ByVal e As ItemClickEventArgs)
            With New Thread(AddressOf PrintReport_Thread)
                .SetApartmentState(ApartmentState.STA)
                .Name = "PrintReport_Thread"
                .IsBackground = True
                .Start()
            End With
        End Sub

        Private Sub PrintReport_Thread()
            With New DepositBatchReportClass(depositManualForm, ctx)
                If .RequestReportParameters = DialogResult.OK Then

                    If depositManualForm.PendingTransactions Then
                        With .Watermark
                            .Font = New Font("Arial", 60, FontStyle.Bold, GraphicsUnit.Point, 0)
                            .ImageViewMode = ImageViewMode.Stretch
                            .ShowBehind = True
                            .Text = String.Format("DATABASE IS{0}NOT UPDATED", Environment.NewLine)
                            .TextDirection = DirectionMode.BackwardDiagonal
                            .TextTransparency = 180
                            .ForeColor = Color.DarkGreen
                        End With
                    End If

                    .DisplayPreviewDialog()
                End If
                .Dispose()
            End With
        End Sub

        Private Sub SimpleButton_ok_Enter(ByVal sender As Object, ByVal e As EventArgs)

            With CType(sender, SimpleButton)
                If "new ok".CompareTo(.Tag) = 0 Then
                    If SimpleButton_ok.Enabled Then
                        SimpleButton_ok.PerformClick()
                    End If
                End If
            End With
        End Sub

        Private Sub ClientDepositControl_Load(ByVal sender As Object, ByVal e As EventArgs)
            SimpleButton_cancel.TabStop = Not EnterMoveNextControl()
        End Sub

        Private Sub NewDepositItemControl1_RecordAdded(ByVal Sender As Object, ByVal record As DataRow)
            BatchListControl1.ScrollToRecord(record)
        End Sub
    End Class
End Namespace
