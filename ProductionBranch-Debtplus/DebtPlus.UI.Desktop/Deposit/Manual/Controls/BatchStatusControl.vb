#Region "Copyright 2000-2012 DebtPlus, L.L.C."
'{*******************************************************************}
'{                                                                   }
'{       DebtPlus Debt Management System                             }
'{                                                                   }
'{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
'{       ALL RIGHTS RESERVED                                         }
'{                                                                   }
'{   The entire contents of this file is protected by U.S. and       }
'{   International Copyright Laws. Unauthorized reproduction,        }
'{   reverse-engineering, and distribution of all or any portion of  }
'{   the code contained in this file is strictly prohibited and may  }
'{   result in severe civil and criminal penalties and will be       }
'{   prosecuted to the maximum extent possible under the law.        }
'{                                                                   }
'{   RESTRICTIONS                                                    }
'{                                                                   }
'{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
'{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
'{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
'{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
'{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
'{                                                                   }
'{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
'{   ADDITIONAL RESTRICTIONS.                                        }
'{                                                                   }
'{*******************************************************************}

#End Region

Namespace Deposit.Manual.Controls
    Friend Class BatchStatusControl

        Dim WithEvents _tbl As DataTable = Nothing
        Protected Property tbl As DataTable
            Get
                Return _tbl
            End Get
            Set(value As DataTable)
                If _tbl IsNot Nothing Then
                    RemoveHandler tbl.ColumnChanged, AddressOf tbl_ColumnChanged
                    RemoveHandler tbl.RowDeleted, AddressOf tbl_RowDeleted
                    RemoveHandler tbl.TableCleared, AddressOf tbl_TableCleared
                    RemoveHandler tbl.TableNewRow, AddressOf tbl_TableNewRow
                End If
                _tbl = value
                If _tbl IsNot Nothing Then
                    AddHandler tbl.ColumnChanged, AddressOf tbl_ColumnChanged
                    AddHandler tbl.RowDeleted, AddressOf tbl_RowDeleted
                    AddHandler tbl.TableCleared, AddressOf tbl_TableCleared
                    AddHandler tbl.TableNewRow, AddressOf tbl_TableNewRow
                End If
            End Set
        End Property

        Public Sub New(ByVal p As Form_DepositManual, ByVal ctx As DepositManualContext)
            MyBase.New(p, ctx)
            InitializeComponent()
        End Sub

        Public Sub LoadList(ByVal tbl As DataTable)
            Me.tbl = tbl

            ' Calculate the information for the display
            LabelControl_batch_id.Text = String.Format("{0:0000000000}", ctx.ap.Batch)
            RecomputeTotals()
        End Sub

        Private Sub tbl_ColumnChanged(ByVal sender As Object, ByVal e As DataColumnChangeEventArgs)
            If e.Column.ColumnName = "amount" Then RecomputeTotals()
        End Sub

        Private Sub tbl_RowDeleted(ByVal sender As Object, ByVal e As DataRowChangeEventArgs)
            RecomputeTotals()
        End Sub

        Private Sub tbl_TableCleared(ByVal sender As Object, ByVal e As DataTableClearEventArgs)
            RecomputeTotals()
        End Sub

        Private Sub tbl_TableNewRow(ByVal sender As Object, ByVal e As DataTableNewRowEventArgs)
            RecomputeTotals()
        End Sub

        Private Sub RecomputeTotals()

            ' The total dollar amount of the batch
            Dim objTotal As Object = tbl.Compute("sum(amount)", String.Empty)
            If objTotal Is Nothing OrElse objTotal Is DBNull.Value Then objTotal = 0D
            LabelControl_total.Text = String.Format("{0:c}", Convert.ToDecimal(objTotal))

            ' The total number of items in the batch
            Dim objCount As Object = tbl.Compute("count(deposit_batch_detail)", String.Empty)
            If objCount Is Nothing OrElse objCount Is DBNull.Value Then objCount = 0
            LabelControl_batch_count.Text = String.Format("{0:n0}", objCount)
        End Sub
    End Class
End Namespace
