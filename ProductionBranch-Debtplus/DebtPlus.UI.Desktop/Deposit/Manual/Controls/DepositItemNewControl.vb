#Region "Copyright 2000-2012 DebtPlus, L.L.C."
'{*******************************************************************}
'{                                                                   }
'{       DebtPlus Debt Management System                             }
'{                                                                   }
'{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
'{       ALL RIGHTS RESERVED                                         }
'{                                                                   }
'{   The entire contents of this file is protected by U.S. and       }
'{   International Copyright Laws. Unauthorized reproduction,        }
'{   reverse-engineering, and distribution of all or any portion of  }
'{   the code contained in this file is strictly prohibited and may  }
'{   result in severe civil and criminal penalties and will be       }
'{   prosecuted to the maximum extent possible under the law.        }
'{                                                                   }
'{   RESTRICTIONS                                                    }
'{                                                                   }
'{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
'{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
'{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
'{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
'{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
'{                                                                   }
'{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
'{   ADDITIONAL RESTRICTIONS.                                        }
'{                                                                   }
'{*******************************************************************}

#End Region

Imports DebtPlus.Utils

Namespace Deposit.Manual.Controls
    Friend Class DepositItemNewControl
        Public Event RecordAdded(ByVal Sender As Object, ByVal record As DataRow)

        Public Sub New(ByVal p As Form_DepositManual, ByVal ctx As DepositManualContext)
            MyBase.New(p, ctx)
            InitializeComponent()
        End Sub

        ' The client for which the information is displayed
        Private NewDrv As DataRowView

        ' Information saved for the last item should this be a new record
        Private last_tran_subtype As Object = Defaults.DefaultSubType()
        Private last_item_date As Object = DateTime.Now
        Private last_reference As Object = DBNull.Value

        Public Overrides Sub ReadRecord()

            ' Ensure that there is a datasource for the lookup control
            If LookUpEdit_tran_subtype.Properties.DataSource Is Nothing Then
                LookUpEdit_tran_subtype.Properties.DataSource = New DataView(depositManualForm.DepositSubtypeTable, String.Empty, "sort_order, description", DataViewRowState.CurrentRows)
                AddHandler LookUpEdit_tran_subtype.EditValueChanging, AddressOf DebtPlus.Data.Validation.LookUpEdit_ActiveTest
            End If

            AddRecord()
            MyBase.ReadRecord()
        End Sub

        Public Sub AddRecord()
            Dim tbl As DataTable = ctx.ds.Tables("deposit_batch_details")

            ' Add a new row to the table
            NewDrv = tbl.DefaultView.AddNew
            NewDrv.BeginEdit()

            ' Ensure that the item date has a value.
            Dim obj As Object = last_item_date
            If obj Is Nothing OrElse obj Is DBNull.Value Then obj = DateTime.Now.Date
            DepositItemDate = obj
            NewDrv("item_date") = obj

            NewDrv("tran_subtype") = last_tran_subtype

            ' If we want the same reference then pre-load it
            If Defaults.UseSameReference() Then
                NewDrv("reference") = last_reference
            Else
                NewDrv("reference") = String.Empty
            End If

            ' Load the control with the values
            DepositClient = NewDrv("client")
            DepositAmount = NewDrv("amount")
            DepositTranSubtype = NewDrv("tran_subtype")
            DepositReference = NewDrv("reference")
        End Sub

        Public Sub AcceptEvent()

            ' Ensure that the item date has a value.
            Dim obj As Object = DepositItemDate
            If obj Is Nothing OrElse obj Is DBNull.Value Then obj = DateTime.Now.Date
            NewDrv("item_date") = obj

            ' The item is unbound. Update the row values.
            NewDrv("client") = DepositClient
            NewDrv("amount") = DepositAmount
            NewDrv("tran_subtype") = DepositTranSubtype
            NewDrv("reference") = DepositReference

            ' Remember the current settings for the new record
            last_tran_subtype = NewDrv("tran_subtype")
            last_item_date = NewDrv("item_date")
            last_reference = NewDrv("reference")

            ' Complete the edit operation and create a new record
            NewDrv.EndEdit()

            ' Tell the parent that we added the record
            RaiseEvent RecordAdded(Me, NewDrv.Row)

            ' Create a new record for the list
            AddRecord()

            ' Reset the keyboard focus to the client ID
            Focus()
            ClientID_client.Focus()
            ClientID_client.EditValue = Nothing
            ReadClientInfo(Nothing)
        End Sub

        Public Sub CancelEvent()

            ' Find the table and cancel any pending edit operation
            If NewDrv IsNot Nothing AndAlso NewDrv.IsNew Then NewDrv.CancelEdit()

            ' Tell the parent that we have finished
            RaiseCompleted()
        End Sub

        Public Sub TakeFocus()

            ' Bring our control to the front
            With Me
                .BringToFront()
                .TabStop = True
                .Visible = True

                ' Go to the client id again.
                .Focus()
                .ClientID_client.Focus()
            End With

            ' Change the parent form to suit our needs
            With CType(Parent, ClientDepositControl)

                ' Disable the batch list control
                .BatchListControl1.Enabled = True

                ' Change the form buttons to come here when they are pressed
                With .SimpleButton_cancel
                    .Text = "&Quit"
                    .Tag = "new cancel"
                End With

                With .SimpleButton_ok
                    .Text = "&Save"
                    .Tag = "new ok"
                    .Enabled = Not HasErrors()
                End With
            End With
        End Sub

        Public Sub GiveFocus()

            ' Hide our own input form since we don't need it anymore.
            With Me
                .SendToBack()
                .Visible = False
                .TabStop = False
            End With
        End Sub
    End Class
End Namespace
