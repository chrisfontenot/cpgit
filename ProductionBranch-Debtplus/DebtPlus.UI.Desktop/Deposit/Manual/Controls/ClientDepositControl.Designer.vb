﻿Namespace Deposit.Manual.Controls
    <Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
    Partial Class ClientDepositControl
        Inherits BaseControl

        'UserControl overrides dispose to clean up the component list.
        <System.Diagnostics.DebuggerNonUserCode()> _
        Protected Overrides Sub Dispose(ByVal disposing As Boolean)
            Try
                If disposing AndAlso components IsNot Nothing Then
                    components.Dispose()
                End If
            Finally
                MyBase.Dispose(disposing)
            End Try
        End Sub

        'Required by the Windows Form Designer
        Private components As System.ComponentModel.IContainer

        'NOTE: The following procedure is required by the Windows Form Designer
        'It can be modified using the Windows Form Designer.  
        'Do not modify it using the code editor.
        <System.Diagnostics.DebuggerStepThrough()> _
        Private Sub InitializeComponent()
            Me.SimpleButton_ok = New DevExpress.XtraEditors.SimpleButton
            Me.SimpleButton_cancel = New DevExpress.XtraEditors.SimpleButton
            Me.NewDepositItemControl1 = New DepositItemNewControl(depositManualForm, ctx)
            Me.BatchListControl1 = New BatchListControl(depositManualForm, ctx)
            Me.BatchStatusControl1 = New BatchStatusControl(depositManualForm, ctx)
            Me.DepositItemControl1 = New DepositItemOldControl(depositManualForm, ctx)
            Me.SuspendLayout()
            '
            'SimpleButton_ok
            '
            Me.SimpleButton_ok.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
            Me.SimpleButton_ok.Location = New System.Drawing.Point(358, 14)
            Me.SimpleButton_ok.MaximumSize = New System.Drawing.Size(75, 23)
            Me.SimpleButton_ok.MinimumSize = New System.Drawing.Size(75, 23)
            Me.SimpleButton_ok.Name = "SimpleButton_ok"
            Me.SimpleButton_ok.Size = New System.Drawing.Size(75, 23)
            Me.SimpleButton_ok.TabIndex = 3
            Me.SimpleButton_ok.Tag = "new ok"
            Me.SimpleButton_ok.Text = "&Save"
            '
            'SimpleButton_cancel
            '
            Me.SimpleButton_cancel.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
            Me.SimpleButton_cancel.Location = New System.Drawing.Point(358, 52)
            Me.SimpleButton_cancel.MaximumSize = New System.Drawing.Size(75, 23)
            Me.SimpleButton_cancel.MinimumSize = New System.Drawing.Size(75, 23)
            Me.SimpleButton_cancel.Name = "SimpleButton_cancel"
            Me.SimpleButton_cancel.Size = New System.Drawing.Size(75, 23)
            Me.SimpleButton_cancel.TabIndex = 4
            Me.SimpleButton_cancel.Tag = "new cancel"
            Me.SimpleButton_cancel.Text = "&Quit"
            '
            'NewDepositItemControl1
            '
            Me.NewDepositItemControl1.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                        Or System.Windows.Forms.AnchorStyles.Left) _
                        Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
            Me.NewDepositItemControl1.Location = New System.Drawing.Point(4, 98)
            Me.NewDepositItemControl1.Name = "NewDepositItemControl1"
            Me.NewDepositItemControl1.Size = New System.Drawing.Size(328, 311)
            Me.NewDepositItemControl1.TabIndex = 1
            '
            'BatchListControl1
            '
            Me.BatchListControl1.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                        Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
            Me.BatchListControl1.Location = New System.Drawing.Point(338, 98)
            Me.BatchListControl1.Name = "BatchListControl1"
            Me.BatchListControl1.Size = New System.Drawing.Size(115, 309)
            Me.BatchListControl1.TabIndex = 5
            Me.BatchListControl1.TabStop = False
            '
            'BatchStatusControl1
            '
            Me.BatchStatusControl1.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                        Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
            Me.BatchStatusControl1.Location = New System.Drawing.Point(4, 4)
            Me.BatchStatusControl1.Name = "BatchStatusControl1"
            Me.BatchStatusControl1.Size = New System.Drawing.Size(328, 95)
            Me.BatchStatusControl1.TabIndex = 0
            Me.BatchStatusControl1.TabStop = False
            '
            'DepositItemControl1
            '
            Me.DepositItemControl1.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                        Or System.Windows.Forms.AnchorStyles.Left) _
                        Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
            Me.DepositItemControl1.Location = New System.Drawing.Point(4, 98)
            Me.DepositItemControl1.Name = "DepositItemControl1"
            Me.DepositItemControl1.Size = New System.Drawing.Size(328, 311)
            Me.DepositItemControl1.TabIndex = 2
            '
            'ClientDepositControl
            '
            Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
            Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
            Me.Controls.Add(Me.SimpleButton_cancel)
            Me.Controls.Add(Me.SimpleButton_ok)
            Me.Controls.Add(Me.BatchListControl1)
            Me.Controls.Add(Me.BatchStatusControl1)
            Me.Controls.Add(Me.DepositItemControl1)
            Me.Controls.Add(Me.NewDepositItemControl1)
            Me.Name = "ClientDepositControl"
            Me.Size = New System.Drawing.Size(458, 412)
            Me.ResumeLayout(False)

        End Sub
        Friend WithEvents BatchStatusControl1 As BatchStatusControl
        Friend WithEvents BatchListControl1 As BatchListControl
        Friend WithEvents SimpleButton_ok As DevExpress.XtraEditors.SimpleButton
        Friend WithEvents SimpleButton_cancel As DevExpress.XtraEditors.SimpleButton
        Friend WithEvents NewDepositItemControl1 As DepositItemNewControl
        Friend WithEvents DepositItemControl1 As DepositItemOldControl

    End Class
End Namespace