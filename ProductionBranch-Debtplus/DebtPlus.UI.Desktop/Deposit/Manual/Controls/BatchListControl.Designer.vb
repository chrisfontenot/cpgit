﻿
Imports System.Windows.Forms

Namespace Deposit.Manual.Controls
    <Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
    Partial Class BatchListControl
        Inherits BaseControl

        'UserControl overrides dispose to clean up the component list.
        <System.Diagnostics.DebuggerNonUserCode()> _
        Protected Overrides Sub Dispose(ByVal disposing As Boolean)
            Try
                If disposing AndAlso components IsNot Nothing Then
                    components.Dispose()
                End If
            Finally
                MyBase.Dispose(disposing)
            End Try
        End Sub

        'Required by the Windows Form Designer
        Private components As System.ComponentModel.IContainer

        'NOTE: The following procedure is required by the Windows Form Designer
        'It can be modified using the Windows Form Designer.  
        'Do not modify it using the code editor.
        <System.Diagnostics.DebuggerStepThrough()> _
        Private Sub InitializeComponent()
            Dim SuperToolTip1 As DevExpress.Utils.SuperToolTip = New DevExpress.Utils.SuperToolTip
            Dim ToolTipTitleItem1 As DevExpress.Utils.ToolTipTitleItem = New DevExpress.Utils.ToolTipTitleItem
            Dim ToolTipItem1 As DevExpress.Utils.ToolTipItem = New DevExpress.Utils.ToolTipItem
            Me.GroupControl1 = New DevExpress.XtraEditors.GroupControl
            Me.ListBoxControl1 = New DevExpress.XtraEditors.ListBoxControl
            CType(Me.GroupControl1, System.ComponentModel.ISupportInitialize).BeginInit()
            Me.GroupControl1.SuspendLayout()
            CType(Me.ListBoxControl1, System.ComponentModel.ISupportInitialize).BeginInit()
            Me.SuspendLayout()
            '
            'GroupControl1
            '
            Me.GroupControl1.Controls.Add(Me.ListBoxControl1)
            Me.GroupControl1.Dock = System.Windows.Forms.DockStyle.Fill
            Me.GroupControl1.Location = New System.Drawing.Point(0, 0)
            Me.GroupControl1.Name = "GroupControl1"
            Me.GroupControl1.Size = New System.Drawing.Size(150, 150)
            Me.GroupControl1.TabIndex = 0
            Me.GroupControl1.Text = "Batch Items"
            '
            'ListBoxControl1
            '
            Me.ListBoxControl1.Appearance.Options.UseTextOptions = True
            Me.ListBoxControl1.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
            Me.ListBoxControl1.Appearance.TextOptions.Trimming = DevExpress.Utils.Trimming.None
            Me.ListBoxControl1.Appearance.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Top
            Me.ListBoxControl1.Appearance.TextOptions.WordWrap = DevExpress.Utils.WordWrap.NoWrap
            Me.ListBoxControl1.Dock = System.Windows.Forms.DockStyle.Fill
            Me.ListBoxControl1.Location = New System.Drawing.Point(2, 22)
            Me.ListBoxControl1.Name = "ListBoxControl1"
            Me.ListBoxControl1.Size = New System.Drawing.Size(146, 126)
            ToolTipTitleItem1.Text = "Current Deposit Items"
            ToolTipItem1.LeftIndent = 6
            ToolTipItem1.Text = "This is the list of the pending deposits items. It is made to resemble an adding " & _
                "machine tape. If you double-click on an item, you may edit that record."
            SuperToolTip1.Items.Add(ToolTipTitleItem1)
            SuperToolTip1.Items.Add(ToolTipItem1)
            Me.ListBoxControl1.SuperTip = SuperToolTip1
            Me.ListBoxControl1.TabIndex = 1
            Me.ListBoxControl1.SelectionMode = SelectionMode.One
            '
            'BatchListControl
            '
            Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
            Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
            Me.Controls.Add(Me.GroupControl1)
            Me.Name = "BatchListControl"
            CType(Me.GroupControl1, System.ComponentModel.ISupportInitialize).EndInit()
            Me.GroupControl1.ResumeLayout(False)
            CType(Me.ListBoxControl1, System.ComponentModel.ISupportInitialize).EndInit()
            Me.ResumeLayout(False)

        End Sub
        Friend WithEvents GroupControl1 As DevExpress.XtraEditors.GroupControl
        Protected Friend WithEvents ListBoxControl1 As DevExpress.XtraEditors.ListBoxControl

    End Class
End Namespace