#Region "Copyright 2000-2012 DebtPlus, L.L.C."
'{*******************************************************************}
'{                                                                   }
'{       DebtPlus Debt Management System                             }
'{                                                                   }
'{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
'{       ALL RIGHTS RESERVED                                         }
'{                                                                   }
'{   The entire contents of this file is protected by U.S. and       }
'{   International Copyright Laws. Unauthorized reproduction,        }
'{   reverse-engineering, and distribution of all or any portion of  }
'{   the code contained in this file is strictly prohibited and may  }
'{   result in severe civil and criminal penalties and will be       }
'{   prosecuted to the maximum extent possible under the law.        }
'{                                                                   }
'{   RESTRICTIONS                                                    }
'{                                                                   }
'{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
'{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
'{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
'{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
'{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
'{                                                                   }
'{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
'{   ADDITIONAL RESTRICTIONS.                                        }
'{                                                                   }
'{*******************************************************************}
#End Region
Imports System.Data.SqlClient
Imports System.Windows.Forms

Namespace Deposit.Manual
    ' singleton
    Friend Class Form_DepositManual

        Private ReadOnly ctx As DepositManualContext = Nothing

        Public Sub New(ByVal ctx As DepositManualContext)
            MyBase.New()
            Me.ctx = ctx
            InitializeComponent()
            AddHandler Me.FormClosing, AddressOf Form_DepositManual_FormClosing
            AddHandler Me.Load, AddressOf Form_DepositManual_Load
            AddHandler BarButtonItem1.ItemClick, AddressOf BarButtonItem1_ItemClick
        End Sub

        ''' <summary>
        ''' If the form is closing then check for lost updates
        ''' </summary>
        Private Sub Form_DepositManual_FormClosing(ByVal sender As Object, ByVal e As FormClosingEventArgs)
            If PendingTransactions() Then
                If DebtPlus.Data.Forms.MessageBox.Show(String.Format("Warning: You have made changes to this batch.{0}These changes will NOT be saved to the database if you simply close this form.{0}{0}Do you wish to abandon these updates?", Environment.NewLine), "Are you sure", MessageBoxButtons.YesNo, MessageBoxIcon.Exclamation, MessageBoxDefaultButton.Button2) <> Windows.Forms.DialogResult.Yes Then e.Cancel = True
            End If
        End Sub

        ''' <summary>
        ''' Determine if there are transactions still pending to be recorded
        ''' </summary>
        Friend Function PendingTransactions() As Boolean
            Dim answer As Boolean = False

            ' Find the table for the updates
            Dim tbl As DataTable = ctx.ds.Tables("deposit_batch_details")

            ' If there is a table then look for changes to the rows. A changed row will generate a warning dialog.
            If tbl IsNot Nothing Then
                For Each row As DataRow In tbl.Rows
                    If row.RowState <> DataRowState.Unchanged Then
                        answer = True
                        Exit For
                    End If
                Next
            End If

            Return answer
        End Function

        ''' <summary>
        ''' Process the one-time load event for the form
        ''' </summary>
        Private Sub Form_DepositManual_Load(ByVal sender As Object, ByVal e As EventArgs)

            ' If there is no batch then use the form realestate to ask for a batch.
            If ctx.ap.Batch <= 0 Then

                With ClientDepositControl1
                    .SendToBack()
                    .Visible = False
                End With

                With NewClientDepositBatch1
                    .Visible = True
                    .BringToFront()
                    .ReadForm()
                    .Focus()
                    AddHandler .Cancelled, AddressOf NewClientDepositBatch1_Cancelled
                    AddHandler .Selected, AddressOf NewClientDepositBatch1_Selected
                End With
            Else
                ' Otherwise, do the deposits for this batch
                PerformDeposits()
            End If
        End Sub

        ''' <summary>
        ''' If the entry is to cancel the form then close it
        ''' </summary>
        Private Sub NewClientDepositBatch1_Cancelled(ByVal sender As Object, ByVal e As EventArgs)
            Close()
        End Sub

        ''' <summary>
        ''' When a new batch is selected then process the batch updates
        ''' </summary>
        Private Sub NewClientDepositBatch1_Selected(ByVal sender As Object, ByVal e As EventArgs)
            ctx.ap.Batch = NewClientDepositBatch1.DepositBatchID
            PerformDeposits()
        End Sub

        ''' <summary>
        ''' Come here once we have a valid batch ID to start the updates
        ''' </summary>
        Private Sub PerformDeposits()

            ' Make the batch selection control to the back
            With NewClientDepositBatch1
                .SendToBack()
                .Visible = False
            End With

            ' Bring forward the batch update control. It will take it from here.
            With ClientDepositControl1
                .BringToFront()
                .Visible = True
                .ReadForm()
                .Focus()
                AddHandler .Cancelled, AddressOf NewClientDepositBatch1_Cancelled
            End With
        End Sub

        Private Sub BarButtonItem1_ItemClick(ByVal sender As Object, ByVal e As DevExpress.XtraBars.ItemClickEventArgs)
            Close()
        End Sub

        Friend Function ClientRow(ByVal client As Int32) As DataRow
            Dim answer As DataRow = Nothing
            Dim tbl As DataTable = ctx.ds.Tables("clients")

            ' See if the data is currently in the table. If so, return it.
            If tbl IsNot Nothing Then
                Dim row As DataRow = tbl.Rows.Find(client)
                If row IsNot Nothing Then answer = row
            End If

            ' The item is not in the table. Create a new row
            If answer Is Nothing Then
                Using cmd As SqlClient.SqlCommand = New SqlCommand
                    With cmd
                        .Connection = New SqlConnection(DebtPlus.LINQ.SQLInfoClass.getDefault().ConnectionString)
                        .CommandText = "SELECT TOP 1 c.client, c.active_status, dbo.format_normal_name(p1n.prefix,p1n.first,p1n.middle,p1n.last,p1n.suffix) as applicant, dbo.format_normal_name(p2n.prefix,p2n.first,p2n.middle,p2n.last,p2n.suffix) as coapplicant, dbo.address_block(dbo.format_address_line_1(a.house,a.direction,a.street,a.suffix,a.modifier,a.modifier_value), a.address_line_2, dbo.format_city_state_zip(a.city, a.state, a.postalcode),default,default) as address, dbo.client_deposit_amount(c.client) as deposit_amount FROM clients c WITH (NOLOCK) LEFT OUTER JOIN people p1 WITH (NOLOCK) ON c.client = p1.client AND 1 = p1.relation LEFT OUTER JOIN names p1n WITH (NOLOCK) ON p1.NameID = p1n.Name LEFT OUTER JOIN people p2 WITH (NOLOCK) ON c.client = p2.Client AND 1 <> p2.relation LEFT OUTER JOIN Names p2n WITH (NOLOCK) ON p2.NameID = p2n.Name LEFT OUTER JOIN Addresses a WITH (NOLOCK) ON c.AddressID = a.Address WHERE c.client = @client"
                        .CommandType = CommandType.Text
                        .Parameters.Add("@client", SqlDbType.Int).Value = client
                    End With

                    Using da As New SqlClient.SqlDataAdapter(cmd)
                        da.Fill(ctx.ds, "clients")
                    End Using
                End Using

                If tbl Is Nothing Then
                    tbl = ctx.ds.Tables("clients")
                    With tbl
                        .PrimaryKey = New DataColumn() {.Columns("client")}
                    End With
                End If

                answer = tbl.Rows.Find(client)
            End If

            Return answer
        End Function

        Friend Function DepositSubtypeTable() As DataTable
            Dim tbl As DataTable = ctx.ds.Tables("deposit_subtypes")
            If tbl Is Nothing Then
                Using cmd As SqlClient.SqlCommand = New SqlCommand
                    With cmd
                        .Connection = New SqlConnection(DebtPlus.LINQ.SQLInfoClass.getDefault().ConnectionString)
                        .CommandType = CommandType.Text
                        .CommandText = "SELECT tran_type as tran_subtype, description, sort_order, convert(bit, case when tran_type = 'PC' then 1 else 0 end) as restricted FROM tran_types WHERE deposit_subtype <> 0"
                    End With

                    Using da As New SqlClient.SqlDataAdapter(cmd)
                        da.Fill(ctx.ds, "deposit_subtypes")
                    End Using
                End Using

                tbl = ctx.ds.Tables("deposit_subtypes")
                With tbl
                    .PrimaryKey = New DataColumn() {.Columns("tran_subtype")}
                End With
            End If

            Return tbl
        End Function
    End Class
End Namespace
