﻿Imports System.IO
Imports System.Collections.Specialized
Imports DebtPlus.Interfaces.Desktop
Imports System.Windows.Forms

Namespace VB6
    Public Class Mainline
        Implements IDesktopMainline

        Public Sub OldAppMain(ByVal args() As String) Implements IDesktopMainline.OldAppMain
            Dim settings As NameValueCollection = DebtPlus.Configuration.Config.GetConfigSettings()

            Dim vb6ExeLocationSetting As String = settings.Item("vb6.exe.location")
            If String.IsNullOrWhiteSpace(vb6ExeLocationSetting) Then Return

            Dim vb6ExeFullPath As String = Path.GetFullPath(vb6ExeLocationSetting)
            Dim returnDirectory As String = Environment.CurrentDirectory
            Try
                Using pgm As New Process()
                    pgm.StartInfo.Arguments = args(0)
                    pgm.StartInfo.FileName = vb6ExeFullPath
                    pgm.StartInfo.WorkingDirectory = Path.GetDirectoryName(vb6ExeFullPath)
                    pgm.Start()
                End Using

            Catch ex As Exception
                Using gdr As New Repository.GetDataResult()
                    gdr.HandleException(ex)
                    DebtPlus.UI.Common.ErrorHandling.HandleErrors(gdr, "Error running VB6\dplusx.exe application")
                End Using

            Finally
                Environment.CurrentDirectory = returnDirectory
            End Try
        End Sub
    End Class
End Namespace
