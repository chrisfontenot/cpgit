
Imports DebtPlus.Reports.Template

Namespace Counselor.Reassign.Reports
    Public Class ReassignmentReport
        Inherits TemplateXtraReportClass

        Public Sub New()
            MyBase.New()
            InitializeComponent()
        End Sub

        Public Overrides ReadOnly Property ReportTitle As String
            Get
                Return "Counselor Reassignments"
            End Get
        End Property

        Private privateSubtitle As String
        Public WriteOnly Property Subtitle As String
            Set(value As String)
                privateSubtitle = value
            End Set
        End Property

        Public Overrides ReadOnly Property ReportSubTitle As String
            Get
                Return privateSubtitle
            End Get
        End Property

    End Class
End Namespace
