Namespace Counselor.Reassign.Reports
    <Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
    Partial Class ReassignmentReport

        'XtraReport overrides dispose to clean up the component list.
        <System.Diagnostics.DebuggerNonUserCode()> _
        Protected Overrides Sub Dispose(ByVal disposing As Boolean)
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
            MyBase.Dispose(disposing)
        End Sub

        'Required by the Designer
        Private components As System.ComponentModel.IContainer

        'NOTE: The following procedure is required by the Designer
        'It can be modified using the Designer.  
        'Do not modify it using the code editor.
        <System.Diagnostics.DebuggerStepThrough()> _
        Private Sub InitializeComponent()
            Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(ReassignmentReport))
            Me.XrPanel1 = New DevExpress.XtraReports.UI.XRPanel()
            Me.XrLabel4 = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel3 = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel2 = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel1 = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel_start_date = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel_client_name = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel_client = New DevExpress.XtraReports.UI.XRLabel()
            Me.GroupHeader1 = New DevExpress.XtraReports.UI.GroupHeaderBand()
            Me.XrLabel_new_counselor_name = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel_active_status = New DevExpress.XtraReports.UI.XRLabel()
            CType(Me, System.ComponentModel.ISupportInitialize).BeginInit()
            '
            'PageHeader
            '
            Me.PageHeader.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrPanel1})
            Me.PageHeader.HeightF = 145.8333!
            Me.PageHeader.Controls.SetChildIndex(Me.XrPanel_AgencyAddress, 0)
            Me.PageHeader.Controls.SetChildIndex(Me.XrLabel_Title, 0)
            Me.PageHeader.Controls.SetChildIndex(Me.XrLine_Title, 0)
            Me.PageHeader.Controls.SetChildIndex(Me.XrLabel_Subtitle, 0)
            Me.PageHeader.Controls.SetChildIndex(Me.XrPageInfo1, 0)
            Me.PageHeader.Controls.SetChildIndex(Me.XrPanel1, 0)
            '
            'XrLabel_Title
            '
            Me.XrLabel_Title.StylePriority.UseFont = False
            '
            'XrPageInfo_PageNumber
            '
            Me.XrPageInfo_PageNumber.StylePriority.UseTextAlignment = False
            '
            'XRLabel_Agency_Name
            '
            Me.XRLabel_Agency_Name.StylePriority.UseTextAlignment = False
            '
            'XrLabel_Agency_Address3
            '
            Me.XrLabel_Agency_Address3.StylePriority.UseFont = False
            Me.XrLabel_Agency_Address3.StylePriority.UseTextAlignment = False
            '
            'XrLabel_Agency_Address1
            '
            Me.XrLabel_Agency_Address1.StylePriority.UseFont = False
            Me.XrLabel_Agency_Address1.StylePriority.UseTextAlignment = False
            '
            'XrLabel_Agency_Phone
            '
            Me.XrLabel_Agency_Phone.StylePriority.UseFont = False
            Me.XrLabel_Agency_Phone.StylePriority.UseTextAlignment = False
            '
            'XrLabel_Agency_Address2
            '
            Me.XrLabel_Agency_Address2.StylePriority.UseFont = False
            Me.XrLabel_Agency_Address2.StylePriority.UseTextAlignment = False
            '
            'Detail
            '
            Me.Detail.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrLabel_active_status, Me.XrLabel_client, Me.XrLabel_client_name, Me.XrLabel_start_date})
            Me.Detail.HeightF = 16.04166!
            '
            'XrPanel1
            '
            Me.XrPanel1.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrLabel4, Me.XrLabel3, Me.XrLabel2, Me.XrLabel1})
            Me.XrPanel1.LocationFloat = New DevExpress.Utils.PointFloat(0.0!, 119.7917!)
            Me.XrPanel1.Name = "XrPanel1"
            Me.XrPanel1.SizeF = New System.Drawing.SizeF(750.0!, 17.0!)
            Me.XrPanel1.StyleName = "XrControlStyle_HeaderPannel"
            '
            'XrLabel4
            '
            Me.XrLabel4.LocationFloat = New DevExpress.Utils.PointFloat(100.0!, 0.9999847!)
            Me.XrLabel4.Name = "XrLabel4"
            Me.XrLabel4.Padding = New DevExpress.XtraPrinting.PaddingInfo(10, 10, 0, 0, 100.0!)
            Me.XrLabel4.SizeF = New System.Drawing.SizeF(73.25!, 15.0!)
            Me.XrLabel4.StylePriority.UsePadding = False
            Me.XrLabel4.StylePriority.UseTextAlignment = False
            Me.XrLabel4.Text = "STATUS"
            Me.XrLabel4.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
            '
            'XrLabel3
            '
            Me.XrLabel3.LocationFloat = New DevExpress.Utils.PointFloat(608.0!, 0.9999924!)
            Me.XrLabel3.Name = "XrLabel3"
            Me.XrLabel3.Padding = New DevExpress.XtraPrinting.PaddingInfo(10, 10, 0, 0, 100.0!)
            Me.XrLabel3.SizeF = New System.Drawing.SizeF(142.0!, 15.0!)
            Me.XrLabel3.StylePriority.UsePadding = False
            Me.XrLabel3.StylePriority.UseTextAlignment = False
            Me.XrLabel3.Text = "START DATE"
            Me.XrLabel3.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
            '
            'XrLabel2
            '
            Me.XrLabel2.LocationFloat = New DevExpress.Utils.PointFloat(173.25!, 0.9999847!)
            Me.XrLabel2.Name = "XrLabel2"
            Me.XrLabel2.Padding = New DevExpress.XtraPrinting.PaddingInfo(10, 10, 0, 0, 100.0!)
            Me.XrLabel2.SizeF = New System.Drawing.SizeF(434.7501!, 15.0!)
            Me.XrLabel2.StylePriority.UsePadding = False
            Me.XrLabel2.StylePriority.UseTextAlignment = False
            Me.XrLabel2.Text = "NAME"
            Me.XrLabel2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
            '
            'XrLabel1
            '
            Me.XrLabel1.LocationFloat = New DevExpress.Utils.PointFloat(0.0!, 1.0!)
            Me.XrLabel1.Name = "XrLabel1"
            Me.XrLabel1.Padding = New DevExpress.XtraPrinting.PaddingInfo(10, 10, 0, 0, 100.0!)
            Me.XrLabel1.SizeF = New System.Drawing.SizeF(100.0!, 15.0!)
            Me.XrLabel1.StylePriority.UsePadding = False
            Me.XrLabel1.Text = "Client"
            '
            'XrLabel_start_date
            '
            Me.XrLabel_start_date.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "start_date", "{0:d}")})
            Me.XrLabel_start_date.LocationFloat = New DevExpress.Utils.PointFloat(608.0!, 0.0!)
            Me.XrLabel_start_date.Name = "XrLabel_start_date"
            Me.XrLabel_start_date.Padding = New DevExpress.XtraPrinting.PaddingInfo(10, 10, 0, 0, 100.0!)
            Me.XrLabel_start_date.SizeF = New System.Drawing.SizeF(142.0!, 15.0!)
            Me.XrLabel_start_date.StylePriority.UsePadding = False
            Me.XrLabel_start_date.StylePriority.UseTextAlignment = False
            Me.XrLabel_start_date.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
            '
            'XrLabel_client_name
            '
            Me.XrLabel_client_name.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "client_name")})
            Me.XrLabel_client_name.LocationFloat = New DevExpress.Utils.PointFloat(173.25!, 0.0!)
            Me.XrLabel_client_name.Name = "XrLabel_client_name"
            Me.XrLabel_client_name.Padding = New DevExpress.XtraPrinting.PaddingInfo(10, 10, 0, 0, 100.0!)
            Me.XrLabel_client_name.SizeF = New System.Drawing.SizeF(434.75!, 15.0!)
            Me.XrLabel_client_name.StylePriority.UsePadding = False
            Me.XrLabel_client_name.StylePriority.UseTextAlignment = False
            Me.XrLabel_client_name.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
            '
            'XrLabel_client
            '
            Me.XrLabel_client.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "client", "{0:0000000}")})
            Me.XrLabel_client.LocationFloat = New DevExpress.Utils.PointFloat(0.0!, 0.0!)
            Me.XrLabel_client.Name = "XrLabel_client"
            Me.XrLabel_client.Padding = New DevExpress.XtraPrinting.PaddingInfo(10, 10, 0, 0, 100.0!)
            Me.XrLabel_client.SizeF = New System.Drawing.SizeF(100.0!, 15.0!)
            Me.XrLabel_client.StylePriority.UsePadding = False
            Me.XrLabel_client.StylePriority.UseTextAlignment = False
            Me.XrLabel_client.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            '
            'GroupHeader1
            '
            Me.GroupHeader1.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrLabel_new_counselor_name})
            Me.GroupHeader1.GroupFields.AddRange(New DevExpress.XtraReports.UI.GroupField() {New DevExpress.XtraReports.UI.GroupField("new_counselor_name", DevExpress.XtraReports.UI.XRColumnSortOrder.Ascending)})
            Me.GroupHeader1.HeightF = 44.79167!
            Me.GroupHeader1.Name = "GroupHeader1"
            Me.GroupHeader1.PageBreak = DevExpress.XtraReports.UI.PageBreak.BeforeBand
            Me.GroupHeader1.RepeatEveryPage = True
            '
            'XrLabel_new_counselor_name
            '
            Me.XrLabel_new_counselor_name.BorderColor = System.Drawing.Color.Maroon
            Me.XrLabel_new_counselor_name.Borders = CType((((DevExpress.XtraPrinting.BorderSide.Left Or DevExpress.XtraPrinting.BorderSide.Top) _
                                                            Or DevExpress.XtraPrinting.BorderSide.Right) _
                                                           Or DevExpress.XtraPrinting.BorderSide.Bottom), DevExpress.XtraPrinting.BorderSide)
            Me.XrLabel_new_counselor_name.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "new_counselor_name", "New Counselor: {0}")})
            Me.XrLabel_new_counselor_name.LocationFloat = New DevExpress.Utils.PointFloat(0.0!, 10.00001!)
            Me.XrLabel_new_counselor_name.Name = "XrLabel_new_counselor_name"
            Me.XrLabel_new_counselor_name.Padding = New DevExpress.XtraPrinting.PaddingInfo(10, 10, 3, 3, 100.0!)
            Me.XrLabel_new_counselor_name.SizeF = New System.Drawing.SizeF(750.0001!, 23.0!)
            Me.XrLabel_new_counselor_name.StyleName = "XrControlStyle_GroupHeader"
            Me.XrLabel_new_counselor_name.StylePriority.UseBorderColor = False
            Me.XrLabel_new_counselor_name.StylePriority.UseBorders = False
            Me.XrLabel_new_counselor_name.StylePriority.UsePadding = False
            '
            'XrLabel_active_status
            '
            Me.XrLabel_active_status.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "active_status")})
            Me.XrLabel_active_status.LocationFloat = New DevExpress.Utils.PointFloat(100.0!, 0.0!)
            Me.XrLabel_active_status.Name = "XrLabel_active_status"
            Me.XrLabel_active_status.Padding = New DevExpress.XtraPrinting.PaddingInfo(10, 10, 0, 0, 100.0!)
            Me.XrLabel_active_status.SizeF = New System.Drawing.SizeF(73.25!, 15.0!)
            Me.XrLabel_active_status.StylePriority.UsePadding = False
            Me.XrLabel_active_status.StylePriority.UseTextAlignment = False
            Me.XrLabel_active_status.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
            '
            'ReassignmentReport
            '
            Me.Bands.AddRange(New DevExpress.XtraReports.UI.Band() {Me.TopMarginBand1, Me.Detail, Me.BottomMarginBand1, Me.PageHeader, Me.PageFooter, Me.GroupHeader1})
            Me.ReportPrintOptions.DetailCountOnEmptyDataSource = 0
            Me.ReportPrintOptions.PrintOnEmptyDataSource = False
            Me.RequestParameters = False
            Me.ScriptLanguage = DevExpress.XtraReports.ScriptLanguage.VisualBasic
            Me.ScriptReferencesString = resources.GetString("$this.ScriptReferencesString")
            Me.ShowPrintMarginsWarning = False
            Me.ShowPrintStatusDialog = False
            Me.StyleSheet.AddRange(New DevExpress.XtraReports.UI.XRControlStyle() {Me.XrControlStyle_Header, Me.XrControlStyle_Totals, Me.XrControlStyle_HeaderPannel, Me.XrControlStyle_GroupHeader})
            Me.Version = "11.2"
            Me.Controls.SetChildIndex(Me.GroupHeader1, 0)
            Me.Controls.SetChildIndex(Me.PageFooter, 0)
            Me.Controls.SetChildIndex(Me.PageHeader, 0)
            Me.Controls.SetChildIndex(Me.BottomMarginBand1, 0)
            Me.Controls.SetChildIndex(Me.Detail, 0)
            Me.Controls.SetChildIndex(Me.TopMarginBand1, 0)
            CType(Me, System.ComponentModel.ISupportInitialize).EndInit()

        End Sub
        Friend WithEvents XrPanel1 As DevExpress.XtraReports.UI.XRPanel
        Friend WithEvents XrLabel3 As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel2 As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel1 As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel_client As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel_client_name As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel_start_date As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents GroupHeader1 As DevExpress.XtraReports.UI.GroupHeaderBand
        Friend WithEvents XrLabel_new_counselor_name As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel4 As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel_active_status As DevExpress.XtraReports.UI.XRLabel
    End Class
End Namespace
