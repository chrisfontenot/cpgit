Imports DebtPlus.Letters.Compose
Imports DebtPlus.Utils
Imports System.Data.SqlClient
Imports DebtPlus.UI.Desktop.Counselor.Reassign.Reports

Namespace Counselor.Reassign

    Friend Class Form1

        Private CurrentRow As DataRow
        Const ClientsTableName As String = "clients"
        Const TicklersTableName As String = "ticklers"
        Public ap As ArgParser
        Dim OldCounselor As String

        Public Sub New()
            MyBase.New()
            InitializeComponent()
        End Sub

        Public Sub New(ByVal ap As ArgParser)
            MyClass.New()
            Me.ap = ap

            AddHandler SimpleButton2.Click, AddressOf SimpleButton2_Click
            AddHandler Me.Load, AddressOf Form1_Load
            AddHandler LookUpEdit_OldCounselor.EditValueChanged, AddressOf LookUpEdit_OldCounselor_EditValueChanged
            AddHandler GridView1.CellValueChanged, AddressOf GridView1_CellValueChanged
            AddHandler SimpleButton1.Click, AddressOf SimpleButton1_Click
            AddHandler BarButtonItem1.ItemClick, AddressOf BarButtonItem1_ItemClick
            AddHandler SimpleButton1.EnabledChanged, AddressOf SimpleButton1_EnabledChanged
            AddHandler BarButtonItem2.ItemClick, AddressOf BarButtonItem2_ItemClick
        End Sub

        Private Sub SimpleButton2_Click(sender As Object, e As EventArgs)
            Close()
        End Sub

        Private Sub Form1_Load(sender As Object, e As EventArgs)

            ' Set the current list of counselors into the "old" counselor and clear the list of new counselors
            Dim lst As List(Of ArgParser.NewCounselorListClass) = ap.CounselorsList()
            LookUpEdit_OldCounselor.Properties.DataSource = lst
            GridControl1.DataSource = Nothing

            ' Set or clear the check-box for active items only
            CheckEdit_ReassignActiveOnly.Checked = ap.DefaultActive

            ' Enable or disable the letter generation
            If ap.LetterCode <> String.Empty Then
                CheckEdit_GenerateLetters.Checked = True
                CheckEdit_GenerateLetters.Enabled = True
            Else
                CheckEdit_GenerateLetters.Checked = False
                CheckEdit_GenerateLetters.Enabled = False
            End If
        End Sub

        Private Sub LookUpEdit_OldCounselor_EditValueChanged(sender As Object, e As EventArgs)

            If LookUpEdit_OldCounselor.EditValue IsNot Nothing AndAlso LookUpEdit_OldCounselor.EditValue IsNot DBNull.Value Then

                ' Generate the list of new names. But, we need to filter out the current item from the list.
                Dim lst As List(Of ArgParser.NewCounselorListClass) = ap.CounselorsList()
                GridControl1.DataSource = lst

                GridView1.ActiveFilter.Clear()
                GridView1.ActiveFilterString = String.Format("[Counselor]<>{0:f0}", LookUpEdit_OldCounselor.EditValue)

                ' Set the name of the previous counselor
                OldCounselor = LookUpEdit_OldCounselor.Text

                ' Reset the percentages for the various counselors
                For Each item As ArgParser.NewCounselorListClass In lst
                    item.Percent = 0
                    item.ClientsProcessed = 0
                    item.ClientsToProcess = 0
                Next
            Else
                GridControl1.DataSource = Nothing
                OldCounselor = String.Empty
            End If

            ' Always disable the OK button.
            SimpleButton1.Enabled = False
        End Sub

        Private Sub GridView1_CellValueChanged(sender As Object, e As DevExpress.XtraGrid.Views.Base.CellValueChangedEventArgs)
            Dim TotalPercent As Int32 = 0

            ' Calculate the total for the percentages
            Dim lst As List(Of ArgParser.NewCounselorListClass) = TryCast(GridControl1.DataSource, List(Of ArgParser.NewCounselorListClass))
            If lst IsNot Nothing Then
                For Each item As ArgParser.NewCounselorListClass In lst
                    TotalPercent += item.Percent
                Next
            End If

            ' The percentage must equal 100 to be valid
            SimpleButton1.Enabled = (TotalPercent = 100)
        End Sub

        Private Sub SimpleButton1_Click(sender As Object, e As EventArgs)
            Dim ActiveOnly As Boolean = CheckEdit_ReassignActiveOnly.Checked
            Dim WantLetters As Boolean = CheckEdit_GenerateLetters.Checked

            Dim lst As List(Of ArgParser.NewCounselorListClass) = TryCast(GridControl1.DataSource, List(Of ArgParser.NewCounselorListClass))
            If lst IsNot Nothing Then

                ' Retrieve a list of the clients corresponding to the old counselor and active status
                Dim cn As SqlClient.SqlConnection = New SqlConnection(DebtPlus.LINQ.SQLInfoClass.getDefault().ConnectionString)
                Dim txn As SqlClient.SqlTransaction = Nothing
                Try
                    cn.Open()
                    txn = cn.BeginTransaction(IsolationLevel.RepeatableRead, "Reassign Counselors")

                    ' Read the clients that we wish to process
                    ReadClients(lst, cn, txn, ActiveOnly)

                    ' Update the clients with the new counselors
                    UpdateClients(cn, txn)

                    ' Commit the data to the database
                    txn.Commit()
                    txn = Nothing

                    ' Close the database connection object as well since we don't need it.
                    cn.Close()
                    cn.Dispose()
                    cn = Nothing

                    ' Start to send letters to the clients.
                    Dim LettersThread As System.Threading.Thread
                    If WantLetters Then
                        LettersThread = GenerateLetters()
                    Else
                        LettersThread = Nothing
                    End If

                    ' Generate the report showing what we did
                    Dim ReportThread As System.Threading.Thread = PrintReport()
                    If ReportThread IsNot Nothing AndAlso ReportThread.IsAlive Then
                        ReportThread.Join()
                    End If

                    ' If the letters are still being processed, wait for them.
                    If LettersThread IsNot Nothing AndAlso LettersThread.IsAlive Then
                        LettersThread.Join()
                    End If

                    ' Reset the form for the next reassignment
                    LookUpEdit_OldCounselor.EditValue = Nothing
                    GridControl1.DataSource = Nothing

                Catch ex As Exception
                    Using gdr As New Repository.GetDataResult()
                        gdr.HandleException(ex)
                        DebtPlus.UI.Common.ErrorHandling.HandleErrors(gdr, "Error processing reassignment")
                    End Using

                Finally
                    If txn IsNot Nothing Then
                        txn.Rollback()
                        txn = Nothing
                    End If

                    If cn IsNot Nothing Then cn.Dispose()
                End Try
            End If
        End Sub

        Private Sub ReadClients(ByRef lst As List(Of ArgParser.NewCounselorListClass), ByVal cn As SqlClient.SqlConnection, ByVal txn As SqlClient.SqlTransaction, ByVal ActiveOnly As Boolean)

            ' Toss the current list of clients from the data space
            Dim tbl As DataTable = ap.ds.Tables(ClientsTableName)
            If tbl IsNot Nothing Then
                ap.ds.Tables.Remove(tbl)
            End If

            tbl = ap.ds.Tables(TicklersTableName)
            If tbl IsNot Nothing Then
                ap.ds.Tables.Remove(tbl)
            End If

            ' Retrieve the clients
            Using cmd As SqlClient.SqlCommand = New SqlCommand
                With cmd
                    .CommandTimeout = System.Math.Max(600, DebtPlus.LINQ.SQLInfoClass.getDefault().CommandTimeout)
                    .Connection = cn
                    .Transaction = txn
                    .CommandText = "SELECT c.[client], c.[counselor] as old_counselor, c.[start_date], convert(int,null) as new_counselor, convert(varchar(80),null) as new_counselor_name, dbo.format_normal_name(pn.[prefix],pn.[first],pn.[middle],pn.[last],pn.[suffix]) as client_name, c.[active_status] FROM [clients] c LEFT OUTER JOIN [people] p ON c.[client] = p.[client] AND 1 = p.[relation] LEFT OUTER JOIN [names] pn ON p.[NameID] = pn.[Name] WHERE c.[counselor] = @counselor"
                    If ActiveOnly Then
                        .CommandText += " AND c.[active_status] IN ('A','AR')"
                    End If
                    .Parameters.Add("@counselor", SqlDbType.Int).Value = LookUpEdit_OldCounselor.EditValue
                End With

                Using da As New SqlClient.SqlDataAdapter(cmd)
                    da.Fill(ap.ds, ClientsTableName)
                End Using
            End Using

            ' Retrieve the ticklers
            Using cmd As SqlClient.SqlCommand = New SqlCommand
                With cmd
                    .CommandTimeout = System.Math.Max(600, DebtPlus.LINQ.SQLInfoClass.getDefault().CommandTimeout)
                    .Connection = cn
                    .Transaction = txn
                    .CommandText = "SELECT t.[tickler], c.[client] as client, c.[active_status], t.[counselor] as old_counselor, convert(int,null) as new_counselor FROM [ticklers] t INNER JOIN [clients] c ON t.[client] = c.[client] WHERE t.[counselor] = @counselor"
                    If ActiveOnly Then
                        .CommandText += " AND c.[active_status] IN ('A','AR')"
                    End If
                    .Parameters.Add("@counselor", SqlDbType.Int).Value = LookUpEdit_OldCounselor.EditValue
                End With

                Using da As New SqlClient.SqlDataAdapter(cmd)
                    da.Fill(ap.ds, TicklersTableName)
                End Using
            End Using

            ' Determine the number of clients to be assigned to each counselor
            Dim TotalClientsToAssign As Int32 = ap.ds.Tables(ClientsTableName).Rows.Count

            For Each item As ArgParser.NewCounselorListClass In lst
                item.ClientsProcessed = 0
                item.ClientsToProcess = (TotalClientsToAssign * item.Percent) \ 100

                ' Do not allow conditions where the numbers are just too small to handle.
                ' If there are only 5 clients then anything less than 20% is simply 0 so make it 1.
                If item.Percent > 0 AndAlso item.ClientsToProcess < 1 Then
                    item.ClientsToProcess = 1
                End If
            Next

            ' Now, scan the counselors and assign the new items to the clients
            Dim CurrentCounselor As Int32 = 0
            Dim OverflowCounselor As Int32 = 0

            For Each row As DataRow In ap.ds.Tables(ClientsTableName).Rows
                Dim Client As Int32 = Convert.ToInt32(row("client"))

                ' Find the counselor in a round-robbin affair so that the clients are distributed
                ' according to the percentages desired.
                Dim FoundCounselor As ArgParser.NewCounselorListClass = FindNormalCounselor(lst, CurrentCounselor)
                If FoundCounselor Is Nothing Then
                    CurrentCounselor = 0
                    FoundCounselor = FindNormalCounselor(lst, CurrentCounselor)
                End If

                ' If we have not found the counselor normally, take the first one that has a spot.
                If FoundCounselor Is Nothing Then
                    FoundCounselor = FindOverflowCounselor(lst, OverflowCounselor)
                    If FoundCounselor Is Nothing Then
                        OverflowCounselor = 0
                        FoundCounselor = FindOverflowCounselor(lst, OverflowCounselor)
                    End If
                End If

                ' Update the client with the new information
                If FoundCounselor IsNot Nothing Then
                    FoundCounselor.ClientsProcessed += 1
                    row.BeginEdit()
                    row("new_counselor") = FoundCounselor.Counselor
                    row("new_counselor_name") = FoundCounselor.Name
                    row.EndEdit()

                    ' Update the ticklers for that client as well
                    Using vue As New System.Data.DataView(ap.ds.Tables(TicklersTableName), String.Format("[client]={0:f0}", Client), String.Empty, DataViewRowState.CurrentRows)
                        For Each drv As DataRowView In vue
                            drv.BeginEdit()
                            drv("new_counselor") = FoundCounselor.Counselor
                            drv.EndEdit()
                        Next
                    End Using
                End If
            Next
        End Sub

        Private Sub UpdateClients(ByVal cn As SqlClient.SqlConnection, ByVal txn As SqlClient.SqlTransaction)

            ' Update the counselor information accordingly
            Using cmd As SqlClient.SqlCommand = New SqlCommand
                With cmd
                    .Connection = cn
                    .Transaction = txn
                    .CommandText = "UPDATE clients SET [counselor]=@new_counselor"

                    If ap.ReassignOffice Then
                        .CommandText += ",[office]=co.office"
                    End If

                    .CommandText += " FROM clients c WITH (NOLOCK)"

                    If ap.ReassignOffice Then
                        .CommandText += " LEFT OUTER JOIN counselors co WITH (NOLOCK) ON co.counselor=@new_counselor"
                    End If

                    .CommandText += " WHERE c.[client]=@client"

                    .CommandType = CommandType.Text
                    .Parameters.Add("@new_counselor", SqlDbType.Int, 0, "new_counselor")
                    .Parameters.Add("@client", SqlDbType.Int, 0, "client")
                End With

                Using da As New SqlClient.SqlDataAdapter
                    da.UpdateCommand = cmd
                    da.Update(ap.ds.Tables(ClientsTableName))
                End Using
            End Using

            ' Update the counselor information accordingly
            Using cmd As SqlClient.SqlCommand = New SqlCommand
                With cmd
                    .Connection = cn
                    .Transaction = txn
                    .CommandText = "UPDATE [ticklers] SET [counselor]=@new_counselor WHERE [tickler]=@tickler"
                    .CommandType = CommandType.Text
                    .Parameters.Add("@new_counselor", SqlDbType.Int, 0, "new_counselor")
                    .Parameters.Add("@tickler", SqlDbType.Int, 0, "tickler")
                End With

                Using da As New SqlClient.SqlDataAdapter
                    da.UpdateCommand = cmd
                    da.Update(ap.ds.Tables(TicklersTableName))
                End Using
            End Using
        End Sub

        Private Shared Function FindNormalCounselor(ByRef lst As List(Of ArgParser.NewCounselorListClass), ByRef CounselorNumber As Int32) As ArgParser.NewCounselorListClass

            Do While CounselorNumber < lst.Count
                Dim item As ArgParser.NewCounselorListClass = lst.Item(CounselorNumber)
                CounselorNumber += 1
                If item.ClientsToProcess > item.ClientsProcessed Then
                    Return item
                End If
            Loop

            Return Nothing
        End Function

        Private Shared Function FindOverflowCounselor(ByRef lst As List(Of ArgParser.NewCounselorListClass), ByRef CounselorNumber As Int32) As ArgParser.NewCounselorListClass

            Do While CounselorNumber < lst.Count
                Dim item As ArgParser.NewCounselorListClass = lst.Item(CounselorNumber)
                CounselorNumber += 1
                If item.Percent > 0 Then
                    Return item
                End If
            Loop

            Return Nothing
        End Function

        Private Sub BarButtonItem1_ItemClick(sender As System.Object, e As DevExpress.XtraBars.ItemClickEventArgs)
            Dim ActiveOnly As Boolean = CheckEdit_ReassignActiveOnly.Checked

            Dim lst As List(Of ArgParser.NewCounselorListClass) = TryCast(GridControl1.DataSource, List(Of ArgParser.NewCounselorListClass))
            If lst IsNot Nothing Then

                ' Retrieve a list of the clients corresponding to the old counselor and active status
                Dim cn As SqlClient.SqlConnection = New SqlConnection(DebtPlus.LINQ.SQLInfoClass.getDefault().ConnectionString)
                Try
                    cn.Open()
                    ReadClients(lst, cn, Nothing, ActiveOnly)
                    PrintReport()

                Catch ex As Exception

                Finally
                    If cn IsNot Nothing Then cn.Dispose()

                End Try
            End If
        End Sub

        Private Function PrintReport() As System.Threading.Thread

            Dim thrd As New System.Threading.Thread(New System.Threading.ThreadStart(AddressOf PrintReportThread))
            With thrd
                .SetApartmentState(Threading.ApartmentState.STA)
                .Name = "ReportThread"
                .Start()
            End With

            Return thrd
        End Function

        Private Sub PrintReportThread()

            ' Generate the report and the dataset for the report.
            Using rpt As New ReassignmentReport()
                rpt.DataSource = New DataView(ap.ds.Tables(ClientsTableName), String.Empty, "new_counselor_name, client", DataViewRowState.CurrentRows)

                ' Print the report only if there is data to printReport
                If ap.ds.Tables(ClientsTableName).Rows.Count > 0 Then

                    ' Set the report subtitle and if there are any calculated fields, update them.
                    rpt.Subtitle = String.Format("Replacements for counselor '{0}'", OldCounselor)
                    For Each calc As DevExpress.XtraReports.UI.CalculatedField In rpt.CalculatedFields
                        calc.Assign(rpt.DataSource, rpt.DataMember)
                    Next

                    ' Generate the report preview window with the current report and show it.
                    Using frm As New DebtPlus.Reports.Template.PrintPreviewForm(rpt)
                        frm.ShowDialog()
                    End Using
                End If
            End Using
        End Sub

        Private Function GenerateLetters() As System.Threading.Thread

            Dim thrd As New System.Threading.Thread(New System.Threading.ThreadStart(AddressOf GenerateLettersThread))
            With thrd
                .SetApartmentState(Threading.ApartmentState.STA)
                .Name = "LettersThread"
                .Start()
            End With

            Return thrd
        End Function

        Private Sub GenerateLettersThread()

            If ap.LetterCode <> String.Empty Then
                For Each Me.CurrentRow In ap.ds.Tables(ClientsTableName).Rows

                    Using ltr As New Composition(ap.LetterCode)
                        AddHandler ltr.GetValue, AddressOf LetterCallback
                        ltr.PrintLetter(False, True)
                        RemoveHandler ltr.GetValue, AddressOf LetterCallback
                    End Using
                Next
            End If
        End Sub

        Private Sub LetterCallback(ByVal Sender As Object, ByVal e As DebtPlus.Events.ParameterValueEventArgs)

            Select Case e.Name.ToLower()
                Case "clients.previous_counselor"
                    e.Value = OldCounselor
                Case "counselor_name", "counselor", "clients.counselor"
                    e.Value = CurrentRow("new_counselor_name")
                Case "client"
                    e.Value = CurrentRow("client")
                Case "client_name", "name"
                    e.Value = CurrentRow("client_name")
                Case "start_date"
                    e.Value = CurrentRow("start_date")
                Case "active_status"
                    e.Value = CurrentRow("active_status")
                Case Else
            End Select

        End Sub

        Private Sub BarButtonItem2_ItemClick(sender As System.Object, e As DevExpress.XtraBars.ItemClickEventArgs)
            Close()
        End Sub

        Private Sub SimpleButton1_EnabledChanged(sender As Object, e As System.EventArgs)
            BarButtonItem1.Enabled = SimpleButton1.Enabled
        End Sub
    End Class
End Namespace
