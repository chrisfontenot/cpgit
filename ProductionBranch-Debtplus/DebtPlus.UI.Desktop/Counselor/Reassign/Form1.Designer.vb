﻿Namespace Counselor.Reassign
    <Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
    Partial Class Form1
        Inherits DebtPlus.Data.Forms.DebtPlusForm

        'Form overrides dispose to clean up the component list.
        <System.Diagnostics.DebuggerNonUserCode()> _
        Protected Overrides Sub Dispose(ByVal disposing As Boolean)
            Try
                If disposing AndAlso components IsNot Nothing Then
                    components.Dispose()
                End If
            Finally
                MyBase.Dispose(disposing)
            End Try
        End Sub

        'Required by the Windows Form Designer
        Private components As System.ComponentModel.IContainer

        'NOTE: The following procedure is required by the Windows Form Designer
        'It can be modified using the Windows Form Designer.  
        'Do not modify it using the code editor.
        <System.Diagnostics.DebuggerStepThrough()> _
        Private Sub InitializeComponent()
            Me.components = New System.ComponentModel.Container()
            Me.barManager1 = New DevExpress.XtraBars.BarManager(Me.components)
            Me.Bar2 = New DevExpress.XtraBars.Bar()
            Me.BarSubItem1 = New DevExpress.XtraBars.BarSubItem()
            Me.BarButtonItem1 = New DevExpress.XtraBars.BarButtonItem()
            Me.BarButtonItem2 = New DevExpress.XtraBars.BarButtonItem()
            Me.barDockControlTop = New DevExpress.XtraBars.BarDockControl()
            Me.barDockControlBottom = New DevExpress.XtraBars.BarDockControl()
            Me.barDockControlLeft = New DevExpress.XtraBars.BarDockControl()
            Me.barDockControlRight = New DevExpress.XtraBars.BarDockControl()
            Me.SimpleButton1 = New DevExpress.XtraEditors.SimpleButton()
            Me.LayoutControl1 = New DevExpress.XtraLayout.LayoutControl()
            Me.CheckEdit_ReassignActiveOnly = New DevExpress.XtraEditors.CheckEdit()
            Me.LookUpEdit_OldCounselor = New DevExpress.XtraEditors.LookUpEdit()
            Me.GridControl1 = New DevExpress.XtraGrid.GridControl()
            Me.GridView1 = New DevExpress.XtraGrid.Views.Grid.GridView()
            Me.GridColumn_Counselor = New DevExpress.XtraGrid.Columns.GridColumn()
            Me.GridColumn_Name = New DevExpress.XtraGrid.Columns.GridColumn()
            Me.GridColumn_Percent = New DevExpress.XtraGrid.Columns.GridColumn()
            Me.RepositoryItemTextEdit_Percentage = New DevExpress.XtraEditors.Repository.RepositoryItemTextEdit()
            Me.SimpleButton2 = New DevExpress.XtraEditors.SimpleButton()
            Me.LayoutControlGroup1 = New DevExpress.XtraLayout.LayoutControlGroup()
            Me.LayoutControlItem1 = New DevExpress.XtraLayout.LayoutControlItem()
            Me.LayoutControlItem2 = New DevExpress.XtraLayout.LayoutControlItem()
            Me.LayoutControlItem3 = New DevExpress.XtraLayout.LayoutControlItem()
            Me.LayoutControlItem4 = New DevExpress.XtraLayout.LayoutControlItem()
            Me.EmptySpaceItem1 = New DevExpress.XtraLayout.EmptySpaceItem()
            Me.LayoutControlItem5 = New DevExpress.XtraLayout.LayoutControlItem()
            Me.EmptySpaceItem2 = New DevExpress.XtraLayout.EmptySpaceItem()
            Me.CheckEdit_GenerateLetters = New DevExpress.XtraEditors.CheckEdit()
            Me.LayoutControlItem6 = New DevExpress.XtraLayout.LayoutControlItem()
            CType(Me.DxErrorProvider1, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.barManager1, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.LayoutControl1, System.ComponentModel.ISupportInitialize).BeginInit()
            Me.LayoutControl1.SuspendLayout()
            CType(Me.CheckEdit_ReassignActiveOnly.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.LookUpEdit_OldCounselor.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.GridControl1, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.GridView1, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.RepositoryItemTextEdit_Percentage, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.LayoutControlGroup1, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.LayoutControlItem1, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.LayoutControlItem2, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.LayoutControlItem3, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.LayoutControlItem4, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.EmptySpaceItem1, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.LayoutControlItem5, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.EmptySpaceItem2, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.CheckEdit_GenerateLetters.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.LayoutControlItem6, System.ComponentModel.ISupportInitialize).BeginInit()
            Me.SuspendLayout()
            '
            'barManager1
            '
            Me.barManager1.Bars.AddRange(New DevExpress.XtraBars.Bar() {Me.Bar2})
            Me.barManager1.DockControls.Add(Me.barDockControlTop)
            Me.barManager1.DockControls.Add(Me.barDockControlBottom)
            Me.barManager1.DockControls.Add(Me.barDockControlLeft)
            Me.barManager1.DockControls.Add(Me.barDockControlRight)
            Me.barManager1.Form = Me
            Me.barManager1.Items.AddRange(New DevExpress.XtraBars.BarItem() {Me.BarSubItem1, Me.BarButtonItem1, Me.BarButtonItem2})
            Me.barManager1.MainMenu = Me.Bar2
            Me.barManager1.MaxItemId = 3
            '
            'Bar2
            '
            Me.Bar2.BarName = "Main menu"
            Me.Bar2.DockCol = 0
            Me.Bar2.DockRow = 0
            Me.Bar2.DockStyle = DevExpress.XtraBars.BarDockStyle.Top
            Me.Bar2.LinksPersistInfo.AddRange(New DevExpress.XtraBars.LinkPersistInfo() {New DevExpress.XtraBars.LinkPersistInfo(Me.BarSubItem1)})
            Me.Bar2.OptionsBar.MultiLine = True
            Me.Bar2.OptionsBar.UseWholeRow = True
            Me.Bar2.Text = "Main menu"
            '
            'BarSubItem1
            '
            Me.BarSubItem1.Caption = "&File"
            Me.BarSubItem1.Id = 0
            Me.BarSubItem1.LinksPersistInfo.AddRange(New DevExpress.XtraBars.LinkPersistInfo() {New DevExpress.XtraBars.LinkPersistInfo(Me.BarButtonItem1), New DevExpress.XtraBars.LinkPersistInfo(Me.BarButtonItem2, True)})
            Me.BarSubItem1.Name = "BarSubItem1"
            '
            'BarButtonItem1
            '
            Me.BarButtonItem1.Caption = "&Preview..."
            Me.BarButtonItem1.Enabled = False
            Me.BarButtonItem1.Id = 1
            Me.BarButtonItem1.Name = "BarButtonItem1"
            '
            'BarButtonItem2
            '
            Me.BarButtonItem2.Caption = "&Exit"
            Me.BarButtonItem2.Id = 2
            Me.BarButtonItem2.Name = "BarButtonItem2"
            '
            'barDockControlTop
            '
            Me.barDockControlTop.CausesValidation = False
            Me.barDockControlTop.Dock = System.Windows.Forms.DockStyle.Top
            Me.barDockControlTop.Location = New System.Drawing.Point(0, 0)
            Me.barDockControlTop.Size = New System.Drawing.Size(445, 24)
            '
            'barDockControlBottom
            '
            Me.barDockControlBottom.CausesValidation = False
            Me.barDockControlBottom.Dock = System.Windows.Forms.DockStyle.Bottom
            Me.barDockControlBottom.Location = New System.Drawing.Point(0, 339)
            Me.barDockControlBottom.Size = New System.Drawing.Size(445, 0)
            '
            'barDockControlLeft
            '
            Me.barDockControlLeft.CausesValidation = False
            Me.barDockControlLeft.Dock = System.Windows.Forms.DockStyle.Left
            Me.barDockControlLeft.Location = New System.Drawing.Point(0, 24)
            Me.barDockControlLeft.Size = New System.Drawing.Size(0, 315)
            '
            'barDockControlRight
            '
            Me.barDockControlRight.CausesValidation = False
            Me.barDockControlRight.Dock = System.Windows.Forms.DockStyle.Right
            Me.barDockControlRight.Location = New System.Drawing.Point(445, 24)
            Me.barDockControlRight.Size = New System.Drawing.Size(0, 315)
            '
            'SimpleButton1
            '
            Me.SimpleButton1.Enabled = False
            Me.SimpleButton1.Location = New System.Drawing.Point(358, 51)
            Me.SimpleButton1.MaximumSize = New System.Drawing.Size(75, 0)
            Me.SimpleButton1.Name = "SimpleButton1"
            Me.SimpleButton1.Size = New System.Drawing.Size(75, 22)
            Me.SimpleButton1.StyleController = Me.LayoutControl1
            Me.SimpleButton1.TabIndex = 4
            Me.SimpleButton1.Text = "&OK"
            '
            'LayoutControl1
            '
            Me.LayoutControl1.Controls.Add(Me.CheckEdit_GenerateLetters)
            Me.LayoutControl1.Controls.Add(Me.CheckEdit_ReassignActiveOnly)
            Me.LayoutControl1.Controls.Add(Me.LookUpEdit_OldCounselor)
            Me.LayoutControl1.Controls.Add(Me.GridControl1)
            Me.LayoutControl1.Controls.Add(Me.SimpleButton1)
            Me.LayoutControl1.Controls.Add(Me.SimpleButton2)
            Me.LayoutControl1.Dock = System.Windows.Forms.DockStyle.Fill
            Me.LayoutControl1.Location = New System.Drawing.Point(0, 24)
            Me.LayoutControl1.Name = "LayoutControl1"
            Me.LayoutControl1.Root = Me.LayoutControlGroup1
            Me.LayoutControl1.Size = New System.Drawing.Size(445, 315)
            Me.LayoutControl1.TabIndex = 6
            Me.LayoutControl1.Text = "LayoutControl1"
            '
            'CheckEdit_ReassignActiveOnly
            '
            Me.CheckEdit_ReassignActiveOnly.Location = New System.Drawing.Point(12, 283)
            Me.CheckEdit_ReassignActiveOnly.MenuManager = Me.barManager1
            Me.CheckEdit_ReassignActiveOnly.Name = "CheckEdit_ReassignActiveOnly"
            Me.CheckEdit_ReassignActiveOnly.Properties.Caption = "Reassign Only Active Clients"
            Me.CheckEdit_ReassignActiveOnly.Size = New System.Drawing.Size(208, 19)
            Me.CheckEdit_ReassignActiveOnly.StyleController = Me.LayoutControl1
            Me.CheckEdit_ReassignActiveOnly.TabIndex = 8
            '
            'LookUpEdit_OldCounselor
            '
            Me.LookUpEdit_OldCounselor.Location = New System.Drawing.Point(101, 12)
            Me.LookUpEdit_OldCounselor.MenuManager = Me.barManager1
            Me.LookUpEdit_OldCounselor.Name = "LookUpEdit_OldCounselor"
            Me.LookUpEdit_OldCounselor.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
            Me.LookUpEdit_OldCounselor.Properties.Columns.AddRange(New DevExpress.XtraEditors.Controls.LookUpColumnInfo() {New DevExpress.XtraEditors.Controls.LookUpColumnInfo("Counselor", "ID", 5, DevExpress.Utils.FormatType.Numeric, "f0", True, DevExpress.Utils.HorzAlignment.Far, DevExpress.Data.ColumnSortOrder.None, DevExpress.Utils.DefaultBoolean.[True]), New DevExpress.XtraEditors.Controls.LookUpColumnInfo("Name", "Name", 40, DevExpress.Utils.FormatType.None, "", True, DevExpress.Utils.HorzAlignment.[Default], DevExpress.Data.ColumnSortOrder.Ascending, DevExpress.Utils.DefaultBoolean.[True]), New DevExpress.XtraEditors.Controls.LookUpColumnInfo("Note", "Note", 10, DevExpress.Utils.FormatType.None, "", True, DevExpress.Utils.HorzAlignment.[Default], DevExpress.Data.ColumnSortOrder.Ascending, DevExpress.Utils.DefaultBoolean.[True])})
            Me.LookUpEdit_OldCounselor.Properties.DisplayMember = "Name"
            Me.LookUpEdit_OldCounselor.Properties.NullText = ""
            Me.LookUpEdit_OldCounselor.Properties.ShowFooter = False
            Me.LookUpEdit_OldCounselor.Properties.SortColumnIndex = 1
            Me.LookUpEdit_OldCounselor.Properties.ValueMember = "Counselor"
            Me.LookUpEdit_OldCounselor.Size = New System.Drawing.Size(332, 20)
            Me.LookUpEdit_OldCounselor.StyleController = Me.LayoutControl1
            Me.LookUpEdit_OldCounselor.TabIndex = 7
            Me.LookUpEdit_OldCounselor.ToolTip = "Counselor that you wish to reassign"
            '
            'GridControl1
            '
            Me.GridControl1.Location = New System.Drawing.Point(12, 67)
            Me.GridControl1.MainView = Me.GridView1
            Me.GridControl1.MenuManager = Me.barManager1
            Me.GridControl1.MinimumSize = New System.Drawing.Size(0, 200)
            Me.GridControl1.Name = "GridControl1"
            Me.GridControl1.RepositoryItems.AddRange(New DevExpress.XtraEditors.Repository.RepositoryItem() {Me.RepositoryItemTextEdit_Percentage})
            Me.GridControl1.Size = New System.Drawing.Size(342, 200)
            Me.GridControl1.TabIndex = 6
            Me.GridControl1.ViewCollection.AddRange(New DevExpress.XtraGrid.Views.Base.BaseView() {Me.GridView1})
            '
            'GridView1
            '
            Me.GridView1.Columns.AddRange(New DevExpress.XtraGrid.Columns.GridColumn() {Me.GridColumn_Counselor, Me.GridColumn_Name, Me.GridColumn_Percent})
            Me.GridView1.GridControl = Me.GridControl1
            Me.GridView1.Name = "GridView1"
            Me.GridView1.OptionsBehavior.AllowAddRows = DevExpress.Utils.DefaultBoolean.[False]
            Me.GridView1.OptionsBehavior.AllowDeleteRows = DevExpress.Utils.DefaultBoolean.[False]
            Me.GridView1.OptionsBehavior.AutoPopulateColumns = False
            Me.GridView1.OptionsSelection.UseIndicatorForSelection = False
            Me.GridView1.OptionsView.ShowFilterPanelMode = DevExpress.XtraGrid.Views.Base.ShowFilterPanelMode.Never
            Me.GridView1.OptionsView.ShowFooter = True
            Me.GridView1.OptionsView.ShowGroupPanel = False
            Me.GridView1.OptionsView.ShowIndicator = False
            Me.GridView1.SortInfo.AddRange(New DevExpress.XtraGrid.Columns.GridColumnSortInfo() {New DevExpress.XtraGrid.Columns.GridColumnSortInfo(Me.GridColumn_Name, DevExpress.Data.ColumnSortOrder.Ascending)})
            '
            'GridColumn_Counselor
            '
            Me.GridColumn_Counselor.AppearanceCell.Options.UseTextOptions = True
            Me.GridColumn_Counselor.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
            Me.GridColumn_Counselor.AppearanceHeader.Options.UseTextOptions = True
            Me.GridColumn_Counselor.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
            Me.GridColumn_Counselor.Caption = "ID"
            Me.GridColumn_Counselor.CustomizationCaption = "Counselor ID"
            Me.GridColumn_Counselor.DisplayFormat.FormatString = "f0"
            Me.GridColumn_Counselor.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
            Me.GridColumn_Counselor.FieldName = "Counselor"
            Me.GridColumn_Counselor.GroupFormat.FormatString = "f0"
            Me.GridColumn_Counselor.GroupFormat.FormatType = DevExpress.Utils.FormatType.Numeric
            Me.GridColumn_Counselor.Name = "GridColumn_Counselor"
            Me.GridColumn_Counselor.OptionsColumn.AllowEdit = False
            Me.GridColumn_Counselor.ToolTip = "Counselor Record ID"
            Me.GridColumn_Counselor.Visible = True
            Me.GridColumn_Counselor.VisibleIndex = 0
            Me.GridColumn_Counselor.Width = 87
            '
            'GridColumn_Name
            '
            Me.GridColumn_Name.AppearanceCell.Options.UseTextOptions = True
            Me.GridColumn_Name.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
            Me.GridColumn_Name.AppearanceHeader.Options.UseTextOptions = True
            Me.GridColumn_Name.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
            Me.GridColumn_Name.Caption = "Name"
            Me.GridColumn_Name.CustomizationCaption = "Counselor Name"
            Me.GridColumn_Name.FieldName = "Name"
            Me.GridColumn_Name.Name = "GridColumn_Name"
            Me.GridColumn_Name.OptionsColumn.AllowEdit = False
            Me.GridColumn_Name.Visible = True
            Me.GridColumn_Name.VisibleIndex = 1
            Me.GridColumn_Name.Width = 401
            '
            'GridColumn_Percent
            '
            Me.GridColumn_Percent.AppearanceCell.Options.UseTextOptions = True
            Me.GridColumn_Percent.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
            Me.GridColumn_Percent.AppearanceHeader.Options.UseTextOptions = True
            Me.GridColumn_Percent.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
            Me.GridColumn_Percent.Caption = "Percent"
            Me.GridColumn_Percent.ColumnEdit = Me.RepositoryItemTextEdit_Percentage
            Me.GridColumn_Percent.CustomizationCaption = "Percentage of clients"
            Me.GridColumn_Percent.DisplayFormat.FormatString = "{0:#0;-#0; }"
            Me.GridColumn_Percent.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
            Me.GridColumn_Percent.FieldName = "Percent"
            Me.GridColumn_Percent.Name = "GridColumn_Percent"
            Me.GridColumn_Percent.SortMode = DevExpress.XtraGrid.ColumnSortMode.Value
            Me.GridColumn_Percent.Summary.AddRange(New DevExpress.XtraGrid.GridSummaryItem() {New DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "Percent", "{0:f0}")})
            Me.GridColumn_Percent.ToolTip = "Percentage of the clients"
            Me.GridColumn_Percent.Visible = True
            Me.GridColumn_Percent.VisibleIndex = 2
            Me.GridColumn_Percent.Width = 112
            '
            'RepositoryItemTextEdit_Percentage
            '
            Me.RepositoryItemTextEdit_Percentage.AllowNullInput = DevExpress.Utils.DefaultBoolean.[False]
            Me.RepositoryItemTextEdit_Percentage.AutoHeight = False
            Me.RepositoryItemTextEdit_Percentage.DisplayFormat.FormatString = "f0"
            Me.RepositoryItemTextEdit_Percentage.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
            Me.RepositoryItemTextEdit_Percentage.EditFormat.FormatString = "f0"
            Me.RepositoryItemTextEdit_Percentage.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric
            Me.RepositoryItemTextEdit_Percentage.Mask.BeepOnError = True
            Me.RepositoryItemTextEdit_Percentage.Mask.EditMask = "f0"
            Me.RepositoryItemTextEdit_Percentage.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric
            Me.RepositoryItemTextEdit_Percentage.Name = "RepositoryItemTextEdit_Percentage"
            '
            'SimpleButton2
            '
            Me.SimpleButton2.Location = New System.Drawing.Point(358, 77)
            Me.SimpleButton2.MaximumSize = New System.Drawing.Size(75, 0)
            Me.SimpleButton2.Name = "SimpleButton2"
            Me.SimpleButton2.Size = New System.Drawing.Size(75, 22)
            Me.SimpleButton2.StyleController = Me.LayoutControl1
            Me.SimpleButton2.TabIndex = 5
            Me.SimpleButton2.Text = "&Cancel"
            '
            'LayoutControlGroup1
            '
            Me.LayoutControlGroup1.CustomizationFormText = "LayoutControlGroup1"
            Me.LayoutControlGroup1.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.[True]
            Me.LayoutControlGroup1.GroupBordersVisible = False
            Me.LayoutControlGroup1.Items.AddRange(New DevExpress.XtraLayout.BaseLayoutItem() {Me.LayoutControlItem1, Me.LayoutControlItem2, Me.LayoutControlItem3, Me.LayoutControlItem4, Me.EmptySpaceItem1, Me.LayoutControlItem5, Me.EmptySpaceItem2, Me.LayoutControlItem6})
            Me.LayoutControlGroup1.Location = New System.Drawing.Point(0, 0)
            Me.LayoutControlGroup1.Name = "LayoutControlGroup1"
            Me.LayoutControlGroup1.Size = New System.Drawing.Size(445, 315)
            Me.LayoutControlGroup1.Text = "LayoutControlGroup1"
            Me.LayoutControlGroup1.TextVisible = False
            '
            'LayoutControlItem1
            '
            Me.LayoutControlItem1.Control = Me.SimpleButton1
            Me.LayoutControlItem1.CustomizationFormText = "OK Button"
            Me.LayoutControlItem1.Location = New System.Drawing.Point(346, 39)
            Me.LayoutControlItem1.Name = "LayoutControlItem1"
            Me.LayoutControlItem1.Size = New System.Drawing.Size(79, 26)
            Me.LayoutControlItem1.Text = "OK Button"
            Me.LayoutControlItem1.TextSize = New System.Drawing.Size(0, 0)
            Me.LayoutControlItem1.TextToControlDistance = 0
            Me.LayoutControlItem1.TextVisible = False
            '
            'LayoutControlItem2
            '
            Me.LayoutControlItem2.Control = Me.SimpleButton2
            Me.LayoutControlItem2.CustomizationFormText = "Cancel Button"
            Me.LayoutControlItem2.Location = New System.Drawing.Point(346, 65)
            Me.LayoutControlItem2.Name = "LayoutControlItem2"
            Me.LayoutControlItem2.Size = New System.Drawing.Size(79, 194)
            Me.LayoutControlItem2.Text = "Cancel Button"
            Me.LayoutControlItem2.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.AutoSize
            Me.LayoutControlItem2.TextSize = New System.Drawing.Size(0, 0)
            Me.LayoutControlItem2.TextToControlDistance = 0
            Me.LayoutControlItem2.TextVisible = False
            '
            'LayoutControlItem3
            '
            Me.LayoutControlItem3.Control = Me.GridControl1
            Me.LayoutControlItem3.CustomizationFormText = "New Counselor(s)"
            Me.LayoutControlItem3.Location = New System.Drawing.Point(0, 39)
            Me.LayoutControlItem3.Name = "LayoutControlItem3"
            Me.LayoutControlItem3.Size = New System.Drawing.Size(346, 220)
            Me.LayoutControlItem3.Text = "New Counselor(s)"
            Me.LayoutControlItem3.TextLocation = DevExpress.Utils.Locations.Top
            Me.LayoutControlItem3.TextSize = New System.Drawing.Size(85, 13)
            '
            'LayoutControlItem4
            '
            Me.LayoutControlItem4.Control = Me.LookUpEdit_OldCounselor
            Me.LayoutControlItem4.CustomizationFormText = "Old Counselor"
            Me.LayoutControlItem4.Location = New System.Drawing.Point(0, 0)
            Me.LayoutControlItem4.Name = "LayoutControlItem4"
            Me.LayoutControlItem4.Size = New System.Drawing.Size(425, 24)
            Me.LayoutControlItem4.Text = "Old Counselor"
            Me.LayoutControlItem4.TextSize = New System.Drawing.Size(85, 13)
            '
            'EmptySpaceItem1
            '
            Me.EmptySpaceItem1.AllowHotTrack = False
            Me.EmptySpaceItem1.CustomizationFormText = "EmptySpaceItem1"
            Me.EmptySpaceItem1.Location = New System.Drawing.Point(0, 24)
            Me.EmptySpaceItem1.MaxSize = New System.Drawing.Size(0, 15)
            Me.EmptySpaceItem1.MinSize = New System.Drawing.Size(10, 15)
            Me.EmptySpaceItem1.Name = "EmptySpaceItem1"
            Me.EmptySpaceItem1.Size = New System.Drawing.Size(425, 15)
            Me.EmptySpaceItem1.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom
            Me.EmptySpaceItem1.Text = "EmptySpaceItem1"
            Me.EmptySpaceItem1.TextSize = New System.Drawing.Size(0, 0)
            '
            'LayoutControlItem5
            '
            Me.LayoutControlItem5.Control = Me.CheckEdit_ReassignActiveOnly
            Me.LayoutControlItem5.CustomizationFormText = "LayoutControlItem5"
            Me.LayoutControlItem5.Location = New System.Drawing.Point(0, 271)
            Me.LayoutControlItem5.Name = "LayoutControlItem5"
            Me.LayoutControlItem5.Size = New System.Drawing.Size(212, 24)
            Me.LayoutControlItem5.Text = "LayoutControlItem5"
            Me.LayoutControlItem5.TextSize = New System.Drawing.Size(0, 0)
            Me.LayoutControlItem5.TextToControlDistance = 0
            Me.LayoutControlItem5.TextVisible = False
            '
            'EmptySpaceItem2
            '
            Me.EmptySpaceItem2.AllowHotTrack = False
            Me.EmptySpaceItem2.CustomizationFormText = "EmptySpaceItem2"
            Me.EmptySpaceItem2.Location = New System.Drawing.Point(0, 259)
            Me.EmptySpaceItem2.MaxSize = New System.Drawing.Size(0, 12)
            Me.EmptySpaceItem2.MinSize = New System.Drawing.Size(10, 12)
            Me.EmptySpaceItem2.Name = "EmptySpaceItem2"
            Me.EmptySpaceItem2.Size = New System.Drawing.Size(425, 12)
            Me.EmptySpaceItem2.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom
            Me.EmptySpaceItem2.Text = "EmptySpaceItem2"
            Me.EmptySpaceItem2.TextSize = New System.Drawing.Size(0, 0)
            '
            'CheckEdit_GenerateLetters
            '
            Me.CheckEdit_GenerateLetters.Location = New System.Drawing.Point(224, 283)
            Me.CheckEdit_GenerateLetters.MenuManager = Me.barManager1
            Me.CheckEdit_GenerateLetters.Name = "CheckEdit_GenerateLetters"
            Me.CheckEdit_GenerateLetters.Properties.Caption = "Generate Letters"
            Me.CheckEdit_GenerateLetters.Size = New System.Drawing.Size(209, 19)
            Me.CheckEdit_GenerateLetters.StyleController = Me.LayoutControl1
            Me.CheckEdit_GenerateLetters.TabIndex = 9
            Me.CheckEdit_GenerateLetters.ToolTip = "Check if you wish to send a client letter"
            '
            'LayoutControlItem6
            '
            Me.LayoutControlItem6.Control = Me.CheckEdit_GenerateLetters
            Me.LayoutControlItem6.CustomizationFormText = "LayoutControlItem6"
            Me.LayoutControlItem6.Location = New System.Drawing.Point(212, 271)
            Me.LayoutControlItem6.Name = "LayoutControlItem6"
            Me.LayoutControlItem6.Size = New System.Drawing.Size(213, 24)
            Me.LayoutControlItem6.Text = "LayoutControlItem6"
            Me.LayoutControlItem6.TextSize = New System.Drawing.Size(0, 0)
            Me.LayoutControlItem6.TextToControlDistance = 0
            Me.LayoutControlItem6.TextVisible = False
            '
            'Form1
            '
            Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
            Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
            Me.ClientSize = New System.Drawing.Size(445, 339)
            Me.Controls.Add(Me.LayoutControl1)
            Me.Controls.Add(Me.barDockControlLeft)
            Me.Controls.Add(Me.barDockControlRight)
            Me.Controls.Add(Me.barDockControlBottom)
            Me.Controls.Add(Me.barDockControlTop)
            Me.Name = "Form1"
            Me.Text = "Reassign Counselors"
            CType(Me.DxErrorProvider1, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.barManager1, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.LayoutControl1, System.ComponentModel.ISupportInitialize).EndInit()
            Me.LayoutControl1.ResumeLayout(False)
            CType(Me.CheckEdit_ReassignActiveOnly.Properties, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.LookUpEdit_OldCounselor.Properties, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.GridControl1, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.GridView1, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.RepositoryItemTextEdit_Percentage, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.LayoutControlGroup1, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.LayoutControlItem1, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.LayoutControlItem2, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.LayoutControlItem3, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.LayoutControlItem4, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.EmptySpaceItem1, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.LayoutControlItem5, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.EmptySpaceItem2, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.CheckEdit_GenerateLetters.Properties, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.LayoutControlItem6, System.ComponentModel.ISupportInitialize).EndInit()
            Me.ResumeLayout(False)

        End Sub
        Friend WithEvents barManager1 As DevExpress.XtraBars.BarManager
        Friend WithEvents Bar2 As DevExpress.XtraBars.Bar
        Friend WithEvents barDockControlTop As DevExpress.XtraBars.BarDockControl
        Friend WithEvents barDockControlBottom As DevExpress.XtraBars.BarDockControl
        Friend WithEvents barDockControlLeft As DevExpress.XtraBars.BarDockControl
        Friend WithEvents barDockControlRight As DevExpress.XtraBars.BarDockControl
        Friend WithEvents LayoutControl1 As DevExpress.XtraLayout.LayoutControl
        Friend WithEvents GridControl1 As DevExpress.XtraGrid.GridControl
        Friend WithEvents GridView1 As DevExpress.XtraGrid.Views.Grid.GridView
        Friend WithEvents GridColumn_Counselor As DevExpress.XtraGrid.Columns.GridColumn
        Friend WithEvents GridColumn_Name As DevExpress.XtraGrid.Columns.GridColumn
        Friend WithEvents GridColumn_Percent As DevExpress.XtraGrid.Columns.GridColumn
        Friend WithEvents RepositoryItemTextEdit_Percentage As DevExpress.XtraEditors.Repository.RepositoryItemTextEdit
        Friend WithEvents SimpleButton1 As DevExpress.XtraEditors.SimpleButton
        Friend WithEvents SimpleButton2 As DevExpress.XtraEditors.SimpleButton
        Friend WithEvents LayoutControlGroup1 As DevExpress.XtraLayout.LayoutControlGroup
        Friend WithEvents LayoutControlItem1 As DevExpress.XtraLayout.LayoutControlItem
        Friend WithEvents LayoutControlItem2 As DevExpress.XtraLayout.LayoutControlItem
        Friend WithEvents LayoutControlItem3 As DevExpress.XtraLayout.LayoutControlItem
        Friend WithEvents LookUpEdit_OldCounselor As DevExpress.XtraEditors.LookUpEdit
        Friend WithEvents LayoutControlItem4 As DevExpress.XtraLayout.LayoutControlItem
        Friend WithEvents EmptySpaceItem1 As DevExpress.XtraLayout.EmptySpaceItem
        Friend WithEvents CheckEdit_ReassignActiveOnly As DevExpress.XtraEditors.CheckEdit
        Friend WithEvents LayoutControlItem5 As DevExpress.XtraLayout.LayoutControlItem
        Friend WithEvents EmptySpaceItem2 As DevExpress.XtraLayout.EmptySpaceItem
        Friend WithEvents BarSubItem1 As DevExpress.XtraBars.BarSubItem
        Friend WithEvents BarButtonItem1 As DevExpress.XtraBars.BarButtonItem
        Friend WithEvents BarButtonItem2 As DevExpress.XtraBars.BarButtonItem
        Friend WithEvents CheckEdit_GenerateLetters As DevExpress.XtraEditors.CheckEdit
        Friend WithEvents LayoutControlItem6 As DevExpress.XtraLayout.LayoutControlItem

    End Class
End Namespace