Option Compare Binary
Option Explicit On 
Option Strict On

Namespace Counselor.Reassign
    Partial Friend Class ArgParser
        Inherits DebtPlus.Utils.ArgParserBase

        Private privateDefaultActive As Boolean
        Public Property DefaultActive As Boolean
            Get
                Return privateDefaultActive
            End Get
            Set(ByVal value As Boolean)
                privateDefaultActive = value
            End Set
        End Property

        Private privateReassignOffice As Boolean
        Public Property ReassignOffice As Boolean
            Get
                Return privateReassignOffice
            End Get
            Private Set(ByVal value As Boolean)
                privateReassignOffice = value
            End Set
        End Property

        Private privateLetterCode As String
        Public Property LetterCode As String
            Get
                Return privateLetterCode
            End Get
            Private Set(ByVal value As String)
                privateLetterCode = value
            End Set
        End Property

        Protected Overrides Function OnSwitch(switchSymbol As String, switchValue As String) As SwitchStatus
            Select Case switchSymbol.ToLower()
                Case "l"
                    LetterCode = switchValue
                Case "a"
                    DefaultActive = Not DefaultActive
                Case Else
                    Return MyBase.OnSwitch(switchSymbol, switchValue)
            End Select

            Return DebtPlus.Utils.ArgParserBase.SwitchStatus.NoError
        End Function

        ''' <summary>
        ''' Create an instance of our class
        ''' </summary>
        Public Sub New()
            MyBase.New(New String() {"l", "a"})

            ' Find the defaults for the items from the app.config file
            LetterCode = DebtPlus.Utils.Nulls.DStr(DebtPlus.Configuration.Config.GetConfigValueForKey(DebtPlus.Configuration.Config.GetConfigSettings(DebtPlus.Configuration.Config.ConfigSet.CounselorReassign), "LetterType"))

            Dim settingsDefaultActive As String = DebtPlus.Utils.Nulls.DStr(DebtPlus.Configuration.Config.GetConfigValueForKey(DebtPlus.Configuration.Config.GetConfigSettings(DebtPlus.Configuration.Config.ConfigSet.CounselorReassign), "DefaultActive"))
            If Not Boolean.TryParse(settingsDefaultActive, DefaultActive) Then
                DefaultActive = False
            End If

            Dim settingsReassignOffice As String = DebtPlus.Utils.Nulls.DStr(DebtPlus.Configuration.Config.GetConfigValueForKey(DebtPlus.Configuration.Config.GetConfigSettings(DebtPlus.Configuration.Config.ConfigSet.CounselorReassign), "ReassignOffice"))
            If Not Boolean.TryParse(settingsReassignOffice, ReassignOffice) Then
                ReassignOffice = False
            End If
        End Sub

        ''' <summary>
        ''' Generate the command usage information
        ''' </summary>
        Protected Overrides Sub OnUsage(ByVal errorInfo As String)
            ' deprecated
        End Sub
    End Class
End Namespace
