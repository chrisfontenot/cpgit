#Region "Copyright 2000-2012 DebtPlus, L.L.C."
'{*******************************************************************}
'{                                                                   }
'{       DebtPlus Debt Management System                             }
'{                                                                   }
'{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
'{       ALL RIGHTS RESERVED                                         }
'{                                                                   }
'{   The entire contents of this file is protected by U.S. and       }
'{   International Copyright Laws. Unauthorized reproduction,        }
'{   reverse-engineering, and distribution of all or any portion of  }
'{   the code contained in this file is strictly prohibited and may  }
'{   result in severe civil and criminal penalties and will be       }
'{   prosecuted to the maximum extent possible under the law.        }
'{                                                                   }
'{   RESTRICTIONS                                                    }
'{                                                                   }
'{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
'{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
'{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
'{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
'{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
'{                                                                   }
'{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
'{   ADDITIONAL RESTRICTIONS.                                        }
'{                                                                   }
'{*******************************************************************}
#End Region

Option Compare Binary
Option Explicit On
Option Strict On

Imports System.ComponentModel
Imports DebtPlus.Utils
Imports System.Data.SqlClient
Imports System.Windows.Forms
Imports System.Drawing

Namespace Counselor.Reassign
    Partial Class ArgParser

        ''' <summary>
        ''' Table names in the dataset
        ''' </summary>
        Public Class Names
            Public Const action_items_goals As String = "action_items_goals"
            Public Const action_plans As String = "action_plans"
            Public Const active_statuses As String = "active_statuses"
            Public Const addresses As String = "Addresses"
            Public Const asset_ids As String = "asset_ids"
            Public Const assets As String = "assets"
            Public Const appointment_attributes As String = "appointment_attributes"
            Public Const attributetypes As String = "AttributeTypes"
            Public Const balance_verification_status As String = "BalanceVerificationStatusTypes"
            Public Const budgets As String = "budgets"
            Public Const calendar As String = "Calendar"
            Public Const CounselorAttributes As String = "CounselorAttributes"
            Public Const client_addkeys As String = "client_addkeys"
            Public Const client_deposits As String = "client_deposits"
            Public Const client_other_debts As String = "client_other_debts"
            Public Const client_retention_events As String = "client_retention_events"
            Public Const client_retention_actions As String = "client_retention_actions"
            Public Const client_status As String = "client_status"
            Public Const client_ach As String = "client_ach"
            Public Const client_www As String = "client_www"
            Public Const client_www_notes As String = "client_www_notes"
            Public Const clients As String = "clients"
            Public Const config As String = "config"
            Public Const contribution_cycles As String = "contribution_cycles"
            Public Const Counselors As String = "Counselors"
            Public Const counselor_attributes As String = "counselor_attributes"
            Public Const counties As String = "counties"
            Public Const countries As String = "countries"
            Public Const creditagency As String = "CreditAgency"
            Public Const creditor_classes As String = "creditor_classes"
            Public Const disbursement_creditor_note_types As String = "disbursement_creditor_note_types"
            Public Const creditors As String = "creditors"
            Public Const drop_reasons As String = "Drop_Reasons"
            Public Const education As String = "education"
            Public Const employers As String = "employers"
            Public Const Ethnicity As String = "Ethnicity"
            Public Const financial_problems As String = "financial_problems"
            Public Const gender As String = "gender"
            Public Const client_housing As String = "client_housing"
            Public Const HousingDelinquency_types As String = "HousingDelinquency_types"
            Public Const Housing_FICONotIncluded As String = "Housing_FICONotIncluded"
            Public Const Housing_FinancingTypes As String = "Housing_FinancingTypes"
            Public Const housing_properties As String = "housing_properties"
            Public Const housing_borrowers As String = "housing_borrowers"
            Public Const housing_loans As String = "housing_loans"
            Public Const housing_LoanTypes As String = "housing_LoanTypes"
            Public Const housing_loan_details As String = "housing_loan_details"
            Public Const housing_lenders As String = "housing_lenders"
            Public Const housing_lender_servicers As String = "housing_lender_servicers"
            Public Const housing_MortgageTypes As String = "housing_MortgageTypes"
            Public Const housing_interviewAddress As String = "Housing_InterviewAddress"
            Public Const housing_grants As String = "housing_grants"
            Public Const housing_types As String = "HousingTypes"
            Public Const hud_interview_types As String = "hud_interview_types"
            Public Const hud_results As String = "hud_results"
            Public Const hud_termination_reasons As String = "hud_termination_reasons"
            Public Const hud_interviews As String = "hud_interviews"
            Public Const hud_transactions As String = "hud_transactions"
            Public Const indicators As String = "indicators"
            Public Const industries As String = "industries"
            Public Const job_descriptions As String = "job_descriptions"
            Public Const marital_types As String = "MaritalTypes"
            Public Const messages As String = "messages"
            Public Const militaryservice As String = "MilitaryService"
            Public Const names As String = "Names"
            Public Const offices As String = "offices"
            Public Const people As String = "people"
            Public Const proposal_batch_ids As String = "proposal_batch_ids"
            Public Const proposal_messages As String = "proposal_messages"
            Public Const proposal_status As String = "proposal_status"
            Public Const proposal_reject_dispositions As String = "proposal_reject_dispositions"
            Public Const proposal_reject_reasons As String = "proposal_reject_reasons"
            Public Const referred_By As String = "Referred_By"
            Public Const referred_to As String = "lst_referred_to"
            Public Const regions As String = "Regions"
            Public Const registers_client_creditor As String = "registers_client_creditor"
            Public Const retention_actions As String = "retention_actions"
            Public Const retention_events As String = "retention_events"
            Public Const sales_files As String = "sales_files"
            Public Const secured_properties As String = "secured_properties"
            Public Const secured_types As String = "secured_types"
            Public Const states As String = "states"
            Public Const telephonenumbers As String = "TelephoneNumbers"
            Public Const WorkshopLocations As String = "workshop_locations"
        End Class

        ''' <summary>
        ''' Known attributes in the counselor_attributes table
        ''' </summary>
        Public Function AttributeTypesTable() As System.Data.DataTable
            Dim tbl As System.Data.DataTable = ds.Tables(Names.attributetypes)

            ' If there is a table then look for a match. If found, return it.
            If tbl Is Nothing Then

                ' Read the information from the system for the indicated name
                Dim cn As SqlClient.SqlConnection = New SqlConnection(DebtPlus.LINQ.SQLInfoClass.getDefault().ConnectionString)
                Try
                    cn.Open()
                    SyncLock ds
                        Using cmd As SqlClient.SqlCommand = New SqlCommand
                            With cmd
                                .Connection = cn
                                .CommandText = "SELECT [oID], [Grouping], [Attribute], [Default], [ActiveFlag] FROM AttributeTypes WITH (NOLOCK)"
                                .CommandType = CommandType.Text
                            End With

                            Using da As New SqlClient.SqlDataAdapter(cmd)
                                da.FillLoadOption = LoadOption.OverwriteChanges
                                da.Fill(ds, Names.attributetypes)
                            End Using
                        End Using

                        tbl = ds.Tables(Names.attributetypes)
                        If tbl.PrimaryKey.GetUpperBound(0) < 0 Then tbl.PrimaryKey = New System.Data.DataColumn() {tbl.Columns("oID")}
                    End SyncLock

                Catch ex As SqlClient.SqlException
                    DebtPlus.UI.Common.ErrorHandling.HandleErrors(ex)

                Finally
                    If cn IsNot Nothing Then cn.Dispose()
                End Try
            End If

            Return tbl
        End Function

        ''' <summary>
        ''' Known attributes in the counselor_attributes table
        ''' </summary>
        Public Function GetAttributeTypeDescription(ByVal value As Object) As String
            Dim answer As String = String.Empty

            If value IsNot Nothing AndAlso value IsNot System.DBNull.Value Then
                Dim tbl As System.Data.DataTable = AttributeTypesTable()
                If tbl IsNot Nothing Then

                    Dim row As System.Data.DataRow = tbl.Rows.Find(value)
                    If row IsNot Nothing Then

                        If row("Attribute") IsNot Nothing AndAlso row("Attribute") IsNot System.DBNull.Value Then
                            answer = Convert.ToString(row("Attribute"))
                        End If
                    End If
                End If
            End If

            Return answer
        End Function

        ''' <summary>
        ''' Find the specific row for the NAMEID
        ''' </summary>
        Public Function GetNameRowByID(ByVal NameID As Int32) As DataRow
            Dim tbl As DataTable = NamesTable(NameID)

            Dim answer As DataRow
            If tbl IsNot Nothing Then
                answer = tbl.Rows.Find(NameID)
            Else
                answer = Nothing
            End If

            Return answer
        End Function

        ''' <summary>
        ''' Name table information
        ''' </summary>
        Public Function NamesTable(ByVal NameID As Int32) As DataTable

            ' If the name row is already loaded then don't read it again.
            Dim tbl As DataTable = ds.Tables(Names.names)
            If tbl IsNot Nothing Then
                If tbl.Rows.Find(NameID) IsNot Nothing Then
                    Return tbl
                End If
            End If

            GetNameRowsRow(String.Format(" WHERE [Name]={0:f0}", NameID))
            Return ds.Tables(Names.names)
        End Function

        Private Sub GetNameRowsRow(ByVal SelectClause As String)
            Dim tbl As DataTable = ds.Tables(Names.names)

            ' Read the name from the system
            Dim current_cursor As Cursor = Cursor.Current
            Try
                Cursor.Current = Cursors.WaitCursor
                Using cmd As SqlClient.SqlCommand = New SqlCommand
                    With cmd
                        .Connection = New SqlConnection(DebtPlus.LINQ.SQLInfoClass.getDefault().ConnectionString)
                        .CommandText = String.Format("SELECT [Name],[prefix],[first],[middle],[last],[suffix] FROM Names WITH(NOLOCK){0}", SelectClause)
                        .CommandType = CommandType.Text
                    End With

                    ' Ask the database for the information
                    Using da As New SqlClient.SqlDataAdapter(cmd)
                        da.FillLoadOption = LoadOption.PreserveChanges
                        da.Fill(ds, Names.names)
                    End Using
                End Using

                tbl = ds.Tables(Names.names)
                If tbl.PrimaryKey.GetUpperBound(0) < 0 Then
                    tbl.PrimaryKey = New DataColumn() {tbl.Columns("Name")}
                    With tbl.Columns("Name")
                        .AutoIncrement = True
                        .AutoIncrementSeed = -1
                        .AutoIncrementStep = -1
                    End With
                End If

                ' When we do an update, the update results in zero rows being updated. IGNORE IT.
            Catch ex As DBConcurrencyException
                DebtPlus.Svc.Common.ErrorHandling.HandleErrors(ex)

                ' Something happend in trying to talk to the database.
            Catch ex As SqlClient.SqlException
                DebtPlus.UI.Common.ErrorHandling.HandleErrors(ex)

            Finally
                Cursor.Current = current_cursor
            End Try
        End Sub

        Public Function NamesSelectSchema() As DataTable
            Dim tbl As DataTable = ds.Tables(Names.names)

            ' Read the schema name from the system
            Dim current_cursor As Cursor = Cursor.Current
            Try
                Cursor.Current = Cursors.WaitCursor
                Using cmd As SqlClient.SqlCommand = New SqlCommand
                    With cmd
                        .Connection = New SqlConnection(DebtPlus.LINQ.SQLInfoClass.getDefault().ConnectionString)
                        .CommandText = "SELECT [Name],[prefix],[first],[middle],[last],[suffix] FROM Names WITH(NOLOCK)"
                        .CommandType = CommandType.Text
                    End With

                    ' Ask the database for the information
                    Using da As New SqlClient.SqlDataAdapter(cmd)
                        da.FillSchema(ds, SchemaType.Source, Names.names)
                    End Using
                End Using

                tbl = ds.Tables(Names.names)
                If tbl.PrimaryKey.GetUpperBound(0) < 0 Then
                    tbl.PrimaryKey = New DataColumn() {tbl.Columns("Name")}
                    With tbl.Columns("Name")
                        .AutoIncrement = True
                        .AutoIncrementSeed = -1
                        .AutoIncrementStep = -1
                    End With
                End If

                ' When we do an update, the update results in zero rows being updated. IGNORE IT.
            Catch ex As DBConcurrencyException
                DebtPlus.UI.Common.ErrorHandling.HandleErrors(ex)

                ' Something happend in trying to talk to the database.
            Catch ex As SqlClient.SqlException
                DebtPlus.UI.Common.ErrorHandling.HandleErrors(ex)

            Finally
                Cursor.Current = current_cursor
            End Try

            Return tbl
        End Function

        Public Sub UpdateNamesTable()
            Dim tbl As DataTable = ds.Tables(Names.names)

            Dim UpdateCmd As SqlClient.SqlCommand = New SqlCommand
            Dim InsertCmd As SqlClient.SqlCommand = New SqlCommand
            Dim DeleteCmd As SqlClient.SqlCommand = New SqlCommand
            Dim cn As SqlClient.SqlConnection = New SqlConnection(DebtPlus.LINQ.SQLInfoClass.getDefault().ConnectionString)

            Try
                With UpdateCmd
                    .Connection = cn
                    .CommandText = "UPDATE Names SET [prefix]=@prefix, [first]=@first, [middle]=@middle, [last]=@last, [suffix]=@suffix WHERE [Name]=@Name"
                    .CommandType = CommandType.Text
                    .Parameters.Add("@prefix", SqlDbType.VarChar, 80, "prefix")
                    .Parameters.Add("@first", SqlDbType.VarChar, 80, "first")
                    .Parameters.Add("@middle", SqlDbType.VarChar, 80, "middle")
                    .Parameters.Add("@last", SqlDbType.VarChar, 80, "last")
                    .Parameters.Add("@suffix", SqlDbType.VarChar, 80, "suffix")
                    .Parameters.Add("@Name", SqlDbType.Int, 0, "Name")
                End With

                With InsertCmd
                    .Connection = cn
                    .CommandText = "xpr_insert_names"
                    .CommandType = CommandType.StoredProcedure
                    .Parameters.Add("@prefix", SqlDbType.VarChar, 80, "prefix")
                    .Parameters.Add("@first", SqlDbType.VarChar, 80, "first")
                    .Parameters.Add("@middle", SqlDbType.VarChar, 80, "middle")
                    .Parameters.Add("@last", SqlDbType.VarChar, 80, "last")
                    .Parameters.Add("@suffix", SqlDbType.VarChar, 80, "suffix")
                    .Parameters.Add("@RETURN_VALUE", SqlDbType.Int, 0, "Name").Direction = ParameterDirection.ReturnValue
                End With

                With DeleteCmd
                    .Connection = cn
                    .CommandText = "DELETE FROM Names WHERE [Name]=@Name"
                    .CommandType = CommandType.Text
                    .Parameters.Add("@Name", SqlDbType.Int, 0, "Name")
                End With

                Dim RowsUpdated As Int32
                Using da As New SqlClient.SqlDataAdapter
                    With da
                        .UpdateCommand = UpdateCmd
                        .InsertCommand = InsertCmd
                        .DeleteCommand = DeleteCmd
                        .AcceptChangesDuringUpdate = True
                        RowsUpdated = .Update(tbl)
                    End With
                End Using

            Catch ex As SqlClient.SqlException
                DebtPlus.UI.Common.ErrorHandling.HandleErrors(ex, "Error updating Names table")

            Finally
                UpdateCmd.Dispose()
                InsertCmd.Dispose()
                DeleteCmd.Dispose()
                cn.Dispose()
            End Try
        End Sub

        Public Function FormattedNameByID(ByVal NameID As System.Int32) As String
            Dim row As System.Data.DataRow = GetNameRowByID(NameID) ' GetNameRowByID(NameID)
            Dim answer As String = String.Empty
            If row IsNot Nothing Then
                answer = DebtPlus.LINQ.Name.FormatNormalName(row("prefix"), row("first"), row("middle"), row("last"), row("suffix"))
            End If
            Return answer
        End Function

        Public Function FormatReverseNameByID(ByVal NameID As System.Int32) As String
            Dim row As System.Data.DataRow = GetNameRowByID(NameID) ' GetNameRowByID(NameID)
            If row IsNot Nothing Then
                Return DebtPlus.LINQ.Name.FormatReverseName(row("prefix"), row("first"), row("middle"), row("last"), row("suffix"))
            End If
            Return String.Empty
        End Function

        Public Class CounselorListClass
            Implements INotifyPropertyChanged
            Implements IComparable
            Implements IDisposable

            Private privateNameID As Int32
            Private privateOffice As Int32
            Private privateTelephoneID As Int32
            Private privateEmailID As Int32
            Private privateImage As Image
            Private privateCounselor As Int32
            Private privateName As String
            Private privatePerson As String
            Private privateSSN As String
            Private privateEmpStartDate As Object
            Private privateEmpEndDate As Object
            Private privateNote As Object
            Private privateHUD_ID As Int32
            Private privateDefault As Boolean
            Private privateActiveFlag As Boolean

            Public Event PropertyChanged(sender As Object, e As PropertyChangedEventArgs) Implements INotifyPropertyChanged.PropertyChanged
            Protected Overridable Sub OnPropertyChanged(ByVal e As PropertyChangedEventArgs)
                RaiseEvent PropertyChanged(Me, e)
            End Sub
            Protected Sub RaisePropertyChanged(ByVal e As PropertyChangedEventArgs)
                OnPropertyChanged(e)
            End Sub
            Protected Sub RaisePropertyChanged(ByVal PropertyName As String)
                RaisePropertyChanged(New PropertyChangedEventArgs(PropertyName))
            End Sub

            Private privateColor As Int32
            Public Property Color() As Int32
                Get
                    If privateColor = 0 Then
                        Return System.Drawing.Color.FromKnownColor(KnownColor.Window).ToArgb
                    End If

                    Return privateColor
                End Get
                Set(ByVal value As Int32)
                    privateColor = value
                    RaisePropertyChanged("Color")
                End Set
            End Property

            Public Property NameID As Int32
                Get
                    Return privateNameID
                End Get
                Set(value As Int32)
                    privateNameID = value
                    RaisePropertyChanged("NameID")
                End Set
            End Property

            Public Property Office As Int32
                Get
                    Return privateOffice
                End Get
                Set(value As Int32)
                    privateOffice = value
                    RaisePropertyChanged("Office")
                End Set
            End Property

            Public Property TelephoneID As Int32
                Get
                    Return privateTelephoneID
                End Get
                Set(value As Int32)
                    privateTelephoneID = value
                    RaisePropertyChanged("TelephoneID")
                End Set
            End Property

            Public Property EmailID As Int32
                Get
                    Return privateEmailID
                End Get
                Set(value As Int32)
                    privateEmailID = value
                    RaisePropertyChanged("EmailID")
                End Set
            End Property

            Public Property Image As Image
                Get
                    Return privateImage
                End Get
                Set(value As Image)
                    If privateImage IsNot Nothing Then
                        privateImage.Dispose()
                    End If

                    privateImage = value
                    RaisePropertyChanged("Image")
                End Set
            End Property

            Public Property Counselor As Int32
                Get
                    Return privateCounselor
                End Get
                Set(value As Int32)
                    privateCounselor = value
                    RaisePropertyChanged("Counselor")
                End Set
            End Property

            Public Property Name As String
                Get
                    Return privateName
                End Get
                Set(value As String)
                    privateName = value
                    RaisePropertyChanged("Name")
                End Set
            End Property

            Public Property Person As String
                Get
                    Return privatePerson
                End Get
                Set(value As String)
                    privatePerson = value
                    RaisePropertyChanged("Person")
                End Set
            End Property

            Public Property SSN As String
                Get
                    Return privateSSN
                End Get
                Set(value As String)
                    privateSSN = value
                    RaisePropertyChanged("SSN")
                End Set
            End Property

            Public Property EmpStartDate As Object
                Get
                    Return privateEmpStartDate
                End Get
                Set(value As Object)
                    privateEmpStartDate = value
                    RaisePropertyChanged("EmpStartDate")
                End Set
            End Property

            Public Property EmpEndDate As Object
                Get
                    Return privateEmpEndDate
                End Get
                Set(value As Object)
                    privateEmpEndDate = value
                    RaisePropertyChanged("EmpEndDate")
                End Set
            End Property

            Public Property Note As Object
                Get
                    Return privateNote
                End Get
                Set(value As Object)
                    privateNote = value
                    RaisePropertyChanged("Note")
                End Set
            End Property

            Public Property HUD_ID As Int32
                Get
                    Return privateHUD_ID
                End Get
                Set(value As Int32)
                    privateHUD_ID = value
                    RaisePropertyChanged("HUD_ID")
                End Set
            End Property

            Public Property [Default] As Boolean
                Get
                    Return privateDefault
                End Get
                Set(value As Boolean)
                    privateDefault = value
                    RaisePropertyChanged("Default")
                End Set
            End Property

            Public Property ActiveFlag As Boolean
                Get
                    Return privateActiveFlag
                End Get
                Set(value As Boolean)
                    privateActiveFlag = value
                    RaisePropertyChanged("ActiveFlag")
                End Set
            End Property

            Public Shadows Function toString() As String
                Return Name
            End Function

            Public Overridable Function CompareTo(ByVal obj As Object) As Int32 Implements IComparable.CompareTo
                Return Name.CompareTo(CType(obj, CounselorListClass).Name)
            End Function

            Public Shared Function DefaultColor(ByVal ItemIndex As Int32) As Int32
                Static DefaultColors() As Color = New Color() {Drawing.Color.LightGray, Drawing.Color.Snow, Drawing.Color.MistyRose, Drawing.Color.LightSalmon, Drawing.Color.SeaShell, Drawing.Color.Bisque, Drawing.Color.PeachPuff, Drawing.Color.Tan, Drawing.Color.NavajoWhite, Drawing.Color.BlanchedAlmond, Drawing.Color.LightGreen, Drawing.Color.Aquamarine, Drawing.Color.SkyBlue, Drawing.Color.Plum, Drawing.Color.Orchid}
                Return DefaultColors(ItemIndex Mod (DefaultColors.GetUpperBound(0) + 1)).ToArgb
            End Function

#Region " IDisposable Support "
            Private disposedValue As Boolean        ' To detect redundant calls

            ' IDisposable
            Protected Overridable Sub Dispose(ByVal disposing As Boolean)
                If Not disposedValue Then
                    disposedValue = True
                    If disposing Then
                        If privateImage IsNot Nothing Then
                            privateImage.Dispose()
                        End If
                    End If

                    ' Release any large objects
                    privateImage = Nothing
                End If
            End Sub

            ' This code added by Visual Basic to correctly implement the disposable pattern.
            Public Sub Dispose() Implements IDisposable.Dispose
                ' Do not change this code.  Put cleanup code in Dispose(ByVal disposing As Boolean) above.
                Dispose(True)
                GC.SuppressFinalize(Me)
            End Sub
#End Region
        End Class

        Public Class NewCounselorListClass
            Inherits CounselorListClass

            Public Sub New()
                MyBase.New()
            End Sub

            Private privatePercent As Int32
            ''' <summary>
            ''' The percentage of the old counselor's clients to be assigned to this counselor
            ''' </summary>
            ''' <value></value>
            ''' <returns></returns>
            ''' <remarks></remarks>
            Public Property Percent As Int32
                Get
                    Return privatePercent
                End Get
                Set(value As Int32)
                    privatePercent = value
                    RaisePropertyChanged("Percent")
                End Set
            End Property

            Private privateClientsToProcess As Int32
            ''' <summary>
            ''' The number of clients that we are to process for this counselor based upon the percentage
            ''' </summary>
            ''' <value></value>
            ''' <returns></returns>
            ''' <remarks></remarks>
            Public Property ClientsToProcess As Int32
                Get
                    Return privateClientsToProcess
                End Get
                Set(value As Int32)
                    privateClientsToProcess = value
                    RaisePropertyChanged("ClientsToProcess")
                End Set
            End Property

            Private privateClientsProcessed As Int32
            ''' <summary>
            ''' The number of items that we have processed already for this counselor
            ''' </summary>
            ''' <value></value>
            ''' <returns></returns>
            ''' <remarks></remarks>
            Public Property ClientsProcessed As Int32
                Get
                    Return privateClientsProcessed
                End Get
                Set(value As Int32)
                    privateClientsProcessed = value
                    RaisePropertyChanged("ClientsProcessed")
                End Set
            End Property
        End Class

        Private ReadCounselorNames_FristTimeOnly As Boolean = True
        Private Sub ReadCounselorNames()
            Dim tbl As DataTable = CounselorsTable()

            ' Build a list of the NameID values from the counslors so that the names may be read all at once.
            Dim sb As New System.Text.StringBuilder
            For Each row As DataRow In tbl.Rows
                Dim NameID As Int32 = DebtPlus.Utils.Nulls.DInt(row("NameID"))
                If NameID > 0 Then
                    sb.AppendFormat(",{0:f0}", NameID)
                End If
            Next

            ' Retrieve the names into the database for the counselors
            If sb.Length > 0 Then sb.Remove(0, 1)
            If sb.Length > 0 Then
                sb.Insert(0, " WHERE [Name] IN (")
                sb.Append(")")
                GetNameRowsRow(sb.ToString())
            End If
        End Sub

        Public Function CounselorsList() As List(Of NewCounselorListClass)
            Return CounselorsList(DBNull.Value)
        End Function

        Public Function CounselorsList(ByVal CurrentCounselor As Object) As List(Of NewCounselorListClass)
            If ReadCounselorNames_FristTimeOnly Then
                ReadCounselorNames_FristTimeOnly = False
                ReadCounselorNames()
            End If

            Return BuildCounselorList("COUNSELOR", CurrentCounselor)
        End Function

        Public Function CSRList() As List(Of NewCounselorListClass)
            Return CSRList(DBNull.Value)
        End Function

        Public Function CSRList(ByVal CurrentCSR As Object) As List(Of NewCounselorListClass)
            If ReadCounselorNames_FristTimeOnly Then
                ReadCounselorNames_FristTimeOnly = False
                ReadCounselorNames()
            End If

            Return BuildCounselorList("CSR", CurrentCSR)
        End Function

        Public Function EducatorList() As List(Of NewCounselorListClass)
            Return EducatorList(DBNull.Value)
        End Function

        Public Function EducatorList(ByVal CurrentEducator As Object) As List(Of NewCounselorListClass)
            If ReadCounselorNames_FristTimeOnly Then
                ReadCounselorNames_FristTimeOnly = False
                ReadCounselorNames()
            End If

            Return BuildCounselorList("EDUCATOR", CurrentEducator)
        End Function

        Private Function BuildCounselorList(ByVal RoleName As String, ByVal CurrentItem As Object) As List(Of NewCounselorListClass)
            Dim lst As New List(Of NewCounselorListClass)

            ' Find the ROLE in the list of attributes for the system.
            Dim RoleTable As DataTable = AttributeTypesTable()
            Dim RoleType As Int32
            Dim rows() As DataRow = RoleTable.Select(String.Format("[Grouping]='ROLE' AND [Attribute]='{0}'", RoleName))
            If rows.GetUpperBound(0) >= 0 Then
                RoleType = DebtPlus.Utils.Nulls.DInt(rows(0)("oID"))
            Else
                RoleType = -1
            End If

            ' Read the list of counselor_attributes
            Dim AttributesTable As DataTable = CounselorAttributesTable()

            If (AttributesTable IsNot Nothing) AndAlso RoleType > 0 Then

                ' Buld the list of counselors
                For Each drv As DataRowView In CounselorsTable.DefaultView
                    Dim Counselor As Int32 = DebtPlus.Utils.Nulls.DInt(drv("counselor"))
                    If Counselor > 0 Then

                        ' If the counselor row is a member of the counselor role then include it here.
                        rows = AttributesTable.Select(String.Format("[counselor]={0:f0} AND [Attribute]={1:f0}", Counselor, RoleType))
                        If rows.GetUpperBound(0) >= 0 Then
                            Dim item As New NewCounselorListClass
                            With item
                                .Counselor = Counselor
                                .EmailID = DebtPlus.Utils.Nulls.DInt(drv("EmailID"))
                                .NameID = DebtPlus.Utils.Nulls.DInt(drv("NameID"))
                                .Office = DebtPlus.Utils.Nulls.DInt(drv("Office"))
                                .TelephoneID = DebtPlus.Utils.Nulls.DInt(drv("TelephoneID"))
                                .Default = Nulls.DBool(drv("Default"))
                                .ActiveFlag = Nulls.DBool(drv("Activeflag"), True)
                                .Person = DebtPlus.Utils.Nulls.DStr(drv("Person"))
                                .SSN = DebtPlus.Utils.Nulls.DStr(drv("ssn"))
                                .EmpStartDate = drv("emp_start_date")
                                .EmpEndDate = drv("emp_end_date")
                                .HUD_ID = DebtPlus.Utils.Nulls.DInt(drv("HUD_ID"))
                                .Note = drv("Note")

                                If drv("Image") IsNot DBNull.Value Then .Image = TryCast(drv("Image"), Bitmap)

                                ' Ensure that the color is defined. The default is 0 which is black and that is black on black so it can't be seen.
                                Dim CurrentColor As Int32 = DebtPlus.Utils.Nulls.DInt(drv("Color"))
                                If CurrentColor = 0 Then
                                    CurrentColor = CounselorListClass.DefaultColor(lst.Count)
                                End If
                                .Color = CurrentColor

                                ' Read the name from the system
                                Dim NameRow As DataRow = GetNameRowByID(.NameID)
                                If (NameRow IsNot Nothing) Then
                                    .Name = DebtPlus.LINQ.Name.FormatNormalName(Nothing, NameRow("first"), Nothing, NameRow("last"), Nothing)
                                    '.Name = Format.Names.FormatReverseName(Nothing, NameRow("first"), Nothing, NameRow("last"), Nothing)
                                End If
                            End With

                            ' Do not include items that are inactive unless they are a match to the current value.
                            Dim ValidCounselor As Boolean = item.ActiveFlag
                            If Not ValidCounselor Then
                                If CurrentItem IsNot Nothing AndAlso CurrentItem IsNot DBNull.Value Then
                                    If Convert.ToInt32(CurrentItem) = Counselor Then ValidCounselor = True
                                End If
                            End If

                            If ValidCounselor Then
                                lst.Add(item)
                            End If
                        End If
                    End If
                Next

                ' Put the items in order by name
                lst.Sort()
            End If

            Return lst
        End Function

        ''' <summary>
        ''' Determine if the counselor has a specified attribute value
        ''' </summary>
        Public Function CounselorHasAttribute(ByVal CounselorID As Int32, ByVal AttributeID As Int32) As Boolean
            Dim tbl As DataTable = CounselorAttributesTable()
            Dim rows() As DataRow = tbl.Select(String.Format("[Counselor]={0:f0} AND [Attribute]={1:f0}", CounselorID, AttributeID))
            Return rows.GetUpperBound(0) >= 0
        End Function

        ''' <summary>
        ''' Counselors (Employees) table
        ''' </summary>
        Public Function CounselorsTable() As DataTable
            Dim tbl As DataTable = ds.Tables(Names.Counselors)

            ' If the table is not found then read the information from the database
            If tbl Is Nothing Then
                Dim current_cursor As Cursor = Cursor.Current
                Try
                    Cursor.Current = Cursors.WaitCursor
                    SyncLock ds
                        Using cmd As SqlClient.SqlCommand = New SqlCommand
                            With cmd
                                .Connection = New SqlConnection(DebtPlus.LINQ.SQLInfoClass.getDefault().ConnectionString)
                                .CommandText = "SELECT [Counselor],[Person],[Office],[Default],[ActiveFlag],[NameID],[Menu_level],[TelephoneID],[EmailID],[Color],[Image],[SSN],[emp_start_date],[emp_end_date],[HUD_ID],[Note] FROM counselors WITH (NOLOCK)"
                                .CommandType = CommandType.Text
                            End With

                            ' Ask the database for the information
                            Using da As New SqlClient.SqlDataAdapter(cmd)
                                da.FillLoadOption = LoadOption.OverwriteChanges
                                da.Fill(ds, Names.Counselors)
                            End Using
                        End Using

                        tbl = ds.Tables(Names.Counselors)
                        With tbl
                            If .PrimaryKey.GetUpperBound(0) < 0 Then .PrimaryKey = New DataColumn() {.Columns("Counselor")}
                            With .Columns("Counselor")
                                .AutoIncrement = True
                                .AutoIncrementSeed = -1
                                .AutoIncrementStep = -1
                            End With
                        End With
                    End SyncLock

                    ' When we do an update, the update results in zero rows being updated. IGNORE IT.
                Catch ex As DBConcurrencyException
                    DebtPlus.Svc.Common.ErrorHandling.HandleErrors(ex)

                    ' Something happend in trying to talk to the database.
                Catch ex As SqlClient.SqlException
                    DebtPlus.UI.Common.ErrorHandling.HandleErrors(ex)

                Finally
                    Cursor.Current = current_cursor
                End Try
            End If

            Return tbl
        End Function

        ''' <summary>
        ''' Obtain a description for the item
        ''' </summary>
        Public Function GetCounselorDescription(ByVal key As Object) As String
            Dim answer As String = String.Empty

            If key IsNot Nothing AndAlso key IsNot DBNull.Value Then
                Dim tbl As DataTable = CounselorsTable()
                If tbl IsNot Nothing Then
                    Dim row As DataRow = tbl.Rows.Find(key)
                    If row IsNot Nothing Then
                        If row("NameID") IsNot DBNull.Value AndAlso row("NameID") IsNot Nothing Then
                            Dim NameID As Int32 = Convert.ToInt32(row("NameID"))
                            Dim NameRow As DataRow = GetNameRowByID(NameID)
                            If NameRow IsNot Nothing Then
                                answer = DebtPlus.LINQ.Name.FormatNormalName(Nothing, NameRow("first"), Nothing, NameRow("last"), Nothing)
                            End If
                        End If
                    End If
                End If
            End If

            Return answer
        End Function

        ''' <summary>
        ''' List of attribute_types for each counselor
        ''' </summary>
        Public Function CounselorAttributesTable() As System.Data.DataTable
            Dim tbl As System.Data.DataTable = ds.Tables(Names.CounselorAttributes)

            ' If there is a table then look for a match. If found, return it.
            If tbl Is Nothing Then

                ' Read the information from the system for the indicated name
                Dim cn As SqlClient.SqlConnection = New SqlConnection(DebtPlus.LINQ.SQLInfoClass.getDefault().ConnectionString)
                Try
                    cn.Open()
                    SyncLock ds
                        Using cmd As SqlClient.SqlCommand = New SqlCommand
                            With cmd
                                .Connection = cn
                                .CommandText = "SELECT [oID], [Counselor], [Attribute] FROM counselor_attributes WITH (NOLOCK)"
                                .CommandType = CommandType.Text
                            End With

                            Using da As New SqlClient.SqlDataAdapter(cmd)
                                da.FillLoadOption = LoadOption.OverwriteChanges
                                da.Fill(ds, Names.CounselorAttributes)
                            End Using
                        End Using
                        tbl = ds.Tables(Names.CounselorAttributes)
                        If tbl.PrimaryKey.GetUpperBound(0) < 0 Then tbl.PrimaryKey = New System.Data.DataColumn() {tbl.Columns("oID")}
                    End SyncLock

                Catch ex As SqlClient.SqlException
                    DebtPlus.UI.Common.ErrorHandling.HandleErrors(ex)

                Finally
                    If cn IsNot Nothing Then cn.Dispose()
                End Try
            End If

            Return tbl
        End Function

    End Class
End Namespace
