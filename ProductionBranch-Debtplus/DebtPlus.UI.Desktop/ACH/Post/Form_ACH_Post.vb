#Region "Copyright 2000-2012 DebtPlus, L.L.C."
'{*******************************************************************}
'{                                                                   }
'{       DebtPlus Debt Management System                             }
'{                                                                   }
'{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
'{       ALL RIGHTS RESERVED                                         }
'{                                                                   }
'{   The entire contents of this file is protected by U.S. and       }
'{   International Copyright Laws. Unauthorized reproduction,        }
'{   reverse-engineering, and distribution of all or any portion of  }
'{   the code contained in this file is strictly prohibited and may  }
'{   result in severe civil and criminal penalties and will be       }
'{   prosecuted to the maximum extent possible under the law.        }
'{                                                                   }
'{   RESTRICTIONS                                                    }
'{                                                                   }
'{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
'{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
'{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
'{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
'{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
'{                                                                   }
'{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
'{   ADDITIONAL RESTRICTIONS.                                        }
'{                                                                   }
'{*******************************************************************}
#End Region

Option Compare Binary
Option Explicit On
Option Strict On

Namespace ACH.Post
    Friend Class Form_ACH_Post
        Inherits DebtPlus.Data.Forms.DebtPlusForm

        Friend ap As ArgParser = Nothing
        Public Sub New(ByVal ap As ArgParser)
            MyClass.New()
            MyClass.ap = ap
        End Sub

#Region " Windows Form Designer generated code "

        Public Sub New()
            MyBase.New()

            'This call is required by the Windows Form Designer.
            InitializeComponent()

            'Add any initialization after the InitializeComponent() call

        End Sub

        'Form overrides dispose to clean up the component list.
        Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
            If disposing Then
                If components IsNot Nothing Then
                    components.Dispose()
                End If
            End If
            MyBase.Dispose(disposing)
        End Sub

        'Required by the Windows Form Designer
        Private components As System.ComponentModel.IContainer

        'NOTE: The following procedure is required by the Windows Form Designer
        'It can be modified using the Windows Form Designer.  
        'Do not modify it using the code editor.
        Friend WithEvents Results1 As Results
        Friend WithEvents SelectBatch1 As SelectBatch
        Friend WithEvents ProcessBatch1 As ProcessBatch
        <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
            Me.Results1 = New Results
            Me.SelectBatch1 = New SelectBatch
            Me.ProcessBatch1 = New ProcessBatch

            Me.SuspendLayout()

            '
            'Results1
            '
            Me.Results1.Dock = System.Windows.Forms.DockStyle.Fill
            Me.Results1.Location = New System.Drawing.Point(0, 0)
            Me.Results1.Name = "Results1"
            Me.Results1.Size = New System.Drawing.Size(400, 266)
            Me.ToolTipController1.SetSuperTip(Me.Results1, Nothing)
            Me.Results1.TabIndex = 0
            Me.Results1.Visible = False
            '
            'SelectBatch1
            '
            Me.SelectBatch1.Dock = System.Windows.Forms.DockStyle.Fill
            Me.SelectBatch1.Location = New System.Drawing.Point(0, 0)
            Me.SelectBatch1.Name = "SelectBatch1"
            Me.SelectBatch1.Size = New System.Drawing.Size(400, 266)
            Me.ToolTipController1.SetSuperTip(Me.SelectBatch1, Nothing)
            Me.SelectBatch1.TabIndex = 1
            '
            'ProcessBatch1
            '
            Me.ProcessBatch1.Dock = System.Windows.Forms.DockStyle.Fill
            Me.ProcessBatch1.Location = New System.Drawing.Point(0, 0)
            Me.ProcessBatch1.Name = "ProcessBatch1"
            Me.ProcessBatch1.Size = New System.Drawing.Size(400, 266)
            Me.ToolTipController1.SetSuperTip(Me.ProcessBatch1, Nothing)
            Me.ProcessBatch1.TabIndex = 2
            Me.ProcessBatch1.Visible = False
            '
            'Form_ACH_Post
            '
            Me.AutoScaleBaseSize = New System.Drawing.Size(5, 14)
            Me.ClientSize = New System.Drawing.Size(400, 266)
            Me.Controls.Add(Me.SelectBatch1)
            Me.Controls.Add(Me.Results1)
            Me.Controls.Add(Me.ProcessBatch1)
            Me.Name = "Form_ACH_Post"
            Me.ToolTipController1.SetSuperTip(Me, Nothing)
            Me.Text = "Post ACH Deposit Batch"

            Me.ResumeLayout(False)

        End Sub

#End Region

        Private Sub Form_ACH_Post_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles MyBase.Load
            With SelectBatch1
                .Visible = True
                .Enabled = True
                .Process()
            End With

            With ProcessBatch1
                .Visible = False
                .Enabled = False
            End With

            With Results1
                .Visible = False
                .Enabled = False
            End With
        End Sub

        Private Sub SelectBatch1_Completed(ByVal sender As Object, ByVal e As System.EventArgs) Handles SelectBatch1.Completed
            With SelectBatch1
                .Visible = False
                .Enabled = False
            End With

            With Results1
                .Visible = False
                .Enabled = False
            End With

            With ProcessBatch1
                .Visible = True
                .Enabled = True
                .Process()
            End With
        End Sub

        Private Sub SelectBatch1_Cancelled(ByVal sender As Object, ByVal e As System.EventArgs) Handles SelectBatch1.Cancelled
            Close()
        End Sub

        Private Sub ProcessBatch1_Completed(ByVal sender As Object, ByVal e As System.EventArgs) Handles ProcessBatch1.Completed
            With SelectBatch1
                .Visible = False
                .Enabled = False
            End With

            With ProcessBatch1
                .Visible = False
                .Enabled = False
            End With

            With Results1
                .Visible = True
                .Enabled = True
                .Process()
            End With
        End Sub
    End Class
End Namespace
