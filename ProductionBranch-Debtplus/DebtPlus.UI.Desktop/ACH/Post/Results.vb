#Region "Copyright 2000-2012 DebtPlus, L.L.C."
'{*******************************************************************}
'{                                                                   }
'{       DebtPlus Debt Management System                             }
'{                                                                   }
'{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
'{       ALL RIGHTS RESERVED                                         }
'{                                                                   }
'{   The entire contents of this file is protected by U.S. and       }
'{   International Copyright Laws. Unauthorized reproduction,        }
'{   reverse-engineering, and distribution of all or any portion of  }
'{   the code contained in this file is strictly prohibited and may  }
'{   result in severe civil and criminal penalties and will be       }
'{   prosecuted to the maximum extent possible under the law.        }
'{                                                                   }
'{   RESTRICTIONS                                                    }
'{                                                                   }
'{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
'{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
'{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
'{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
'{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
'{                                                                   }
'{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
'{   ADDITIONAL RESTRICTIONS.                                        }
'{                                                                   }
'{*******************************************************************}
#End Region

Option Compare Binary
Option Explicit On
Option Strict On

Imports System.Data.SqlClient
Imports DebtPlus.Reports
Imports System.Windows.Forms

Namespace ACH.Post
    Public Class Results
        Inherits DevExpress.XtraEditors.XtraUserControl

        Private ReadOnly Property ap As ArgParser
            Get
                Return CType(ParentForm, Form_ACH_Post).ap
            End Get
        End Property

#Region " Windows Form Designer generated code "

        Public Sub New()
            MyBase.New()
            InitializeComponent()
        End Sub

        'UserControl overrides dispose to clean up the component list.
        Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
            If disposing Then
                If components IsNot Nothing Then
                    components.Dispose()
                End If
            End If
            MyBase.Dispose(disposing)
        End Sub

        'Required by the Windows Form Designer
        Private components As System.ComponentModel.IContainer

        'NOTE: The following procedure is required by the Windows Form Designer
        'It can be modified using the Windows Form Designer.  
        'Do not modify it using the code editor.
        Friend WithEvents LabelControl1 As DevExpress.XtraEditors.LabelControl
        Friend WithEvents LabelControl2 As DevExpress.XtraEditors.LabelControl
        Friend WithEvents Formatted_BatchID As DevExpress.XtraEditors.LabelControl
        Friend WithEvents check_report As DevExpress.XtraEditors.CheckEdit
        Friend WithEvents check_letters As DevExpress.XtraEditors.CheckEdit
        Friend WithEvents Button_OK As DevExpress.XtraEditors.SimpleButton
        <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
            Me.LabelControl1 = New DevExpress.XtraEditors.LabelControl
            Me.LabelControl2 = New DevExpress.XtraEditors.LabelControl
            Me.Formatted_BatchID = New DevExpress.XtraEditors.LabelControl
            Me.check_report = New DevExpress.XtraEditors.CheckEdit
            Me.check_letters = New DevExpress.XtraEditors.CheckEdit
            Me.Button_OK = New DevExpress.XtraEditors.SimpleButton
            CType(Me.check_report.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.check_letters.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
            Me.SuspendLayout()
            '
            'LabelControl1
            '
            Me.LabelControl1.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                                             Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
            Me.LabelControl1.Location = New System.Drawing.Point(8, 8)
            Me.LabelControl1.Name = "LabelControl1"
            Me.LabelControl1.Size = New System.Drawing.Size(376, 14)
            Me.LabelControl1.TabIndex = 0
            Me.LabelControl1.Text = "The ACH information has been posted to the account."
            Me.LabelControl1.UseMnemonic = False
            '
            'LabelControl2
            '
            Me.LabelControl2.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.Horizontal
            Me.LabelControl2.Location = New System.Drawing.Point(8, 40)
            Me.LabelControl2.Name = "LabelControl2"
            Me.LabelControl2.Size = New System.Drawing.Size(84, 13)
            Me.LabelControl2.TabIndex = 1
            Me.LabelControl2.Text = "Deposit Batch ID:"
            Me.LabelControl2.UseMnemonic = False
            '
            'Formatted_BatchID
            '
            Me.Formatted_BatchID.Appearance.Font = New System.Drawing.Font("Tahoma", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
            Me.Formatted_BatchID.Appearance.Options.UseFont = True
            Me.Formatted_BatchID.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.Horizontal
            Me.Formatted_BatchID.Location = New System.Drawing.Point(104, 32)
            Me.Formatted_BatchID.Name = "Formatted_BatchID"
            Me.Formatted_BatchID.Size = New System.Drawing.Size(96, 23)
            Me.Formatted_BatchID.TabIndex = 2
            Me.Formatted_BatchID.Text = "00000000"
            Me.Formatted_BatchID.UseMnemonic = False
            '
            'check_report
            '
            Me.check_report.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                                            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
            Me.check_report.EditValue = True
            Me.check_report.Location = New System.Drawing.Point(8, 64)
            Me.check_report.Name = "check_report"
            '
            'check_report.Properties
            '
            Me.check_report.Properties.Caption = "&Print a deposit report for the items"
            Me.check_report.Size = New System.Drawing.Size(384, 19)
            Me.check_report.TabIndex = 3
            '
            'check_letters
            '
            Me.check_letters.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                                             Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
            Me.check_letters.EditValue = True
            Me.check_letters.Location = New System.Drawing.Point(8, 88)
            Me.check_letters.Name = "check_letters"
            '
            'check_letters.Properties
            '
            Me.check_letters.Properties.Caption = "Generate client &letters for failed transactions in this batch"
            Me.check_letters.Size = New System.Drawing.Size(384, 19)
            Me.check_letters.TabIndex = 4
            '
            'Button_OK
            '
            Me.Button_OK.Anchor = System.Windows.Forms.AnchorStyles.Bottom
            Me.Button_OK.Location = New System.Drawing.Point(163, 187)
            Me.Button_OK.Name = "Button_OK"
            Me.Button_OK.TabIndex = 5
            Me.Button_OK.Text = "OK"
            '
            'Results
            '
            Me.Controls.Add(Me.Button_OK)
            Me.Controls.Add(Me.check_letters)
            Me.Controls.Add(Me.check_report)
            Me.Controls.Add(Me.Formatted_BatchID)
            Me.Controls.Add(Me.LabelControl2)
            Me.Controls.Add(Me.LabelControl1)
            Me.Name = "Results"
            Me.Size = New System.Drawing.Size(400, 224)
            CType(Me.check_report.Properties, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.check_letters.Properties, System.ComponentModel.ISupportInitialize).EndInit()
            Me.ResumeLayout(False)

        End Sub

#End Region

        Public Sub Process()
            Formatted_BatchID.Text = String.Format("{0:d8}", ap.trust_register)
        End Sub

        Private Sub Button_OK_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Button_OK.Click

            ' Generate the letters
            If check_letters.Checked Then
                Dim thrd As New System.Threading.Thread(AddressOf LetterWriter)
                With thrd
                    .Name = "letter_writer"
                    .SetApartmentState(Threading.ApartmentState.STA)
                    .Start()
                End With
            End If

            ' Generate the deposit batch report
            If check_report.Checked Then
                Dim thrd As New System.Threading.Thread(AddressOf ReportWriter)
                With thrd
                    .Name = "report_writer"
                    .SetApartmentState(Threading.ApartmentState.STA)
                    .Start()
                End With
            End If

            ' Generate the close event for our form as if someone pressed "Cancel".
            FindForm.Close()
        End Sub

        Private Sub LetterWriter()
            Dim rd As SqlClient.SqlDataReader = Nothing
            Dim cn As SqlClient.SqlConnection = New SqlConnection(LibraryGlobals.SqlInfo.ConnectionString)
            Dim letters_printed As System.Int32 = 0
            Try
                cn.Open()
                Using cmd As SqlClient.SqlCommand = New SqlCommand
                    With cmd
                        .Connection = cn
                        .CommandText = "xpr_ACH_Reject_Letters"
                        .CommandType = CommandType.StoredProcedure
                        SqlClient.SqlCommandBuilder.DeriveParameters(cmd)
                        .Parameters(1).Value = ap.Batch
                        .CommandTimeout = System.Math.Max(600, LibraryGlobals.SqlInfo.CommandTimeout)
                        rd = .ExecuteReader(CommandBehavior.CloseConnection Or CommandBehavior.SingleResult)
                    End With

                    If rd IsNot Nothing Then
                        Do While rd.Read
                            Dim client As System.Int32 = 0
                            Dim strCode As String = String.Empty
                            Dim Ordinal As System.Int32

                            Ordinal = rd.GetOrdinal("client")
                            If Ordinal >= 0 Then
                                If Not rd.IsDBNull(Ordinal) Then client = Convert.ToInt32(rd.GetValue(Ordinal))
                            End If

                            Ordinal = rd.GetOrdinal("letter_code")
                            If Ordinal >= 0 Then
                                If Not rd.IsDBNull(Ordinal) Then strCode = rd.GetString(Ordinal)
                            End If

                            If client > 0 AndAlso strCode <> String.Empty Then
                                Using ltr As New DebtPlus.Letters.Compose.Composition(strCode)
                                    ltr.ClientId = client
                                    ltr.PrintLetter(False, True)
                                    letters_printed += 1
                                End Using
                            End If
                        Loop
                    End If
                End Using

            Catch ex As SqlClient.SqlException
                Using gdr As New Repository.GetDataResult() : gdr.HandleException(ex) : DebtPlus.UI.Common.ErrorHandling.HandleErrors(gdr, "Error generating letters") : End Using

            Finally
                If rd IsNot Nothing Then rd.Dispose()
                If cn IsNot Nothing Then cn.Dispose()
            End Try

            ' Include the one status message indicating that the operation is complete
            If letters_printed > 0 Then
                Dim Message As String
                If letters_printed <> 1 Then
                    Message = String.Format("There were {0:n0} letters printed/queued.", letters_printed)
                Else
                    Message = "There was 1 letter printed/queued."
                End If
                DebtPlus.Data.Forms.MessageBox.Show(Message, "Operation complete", MessageBoxButtons.OK)
            End If
        End Sub

        Private Sub ReportWriter()
            Dim rpt As DebtPlus.Reports.Deposits.Batch.ByTrustRegister.DepositBatchReport = New DebtPlus.Reports.Deposits.Batch.ByTrustRegister.DepositBatchReport()
            rpt.Parameter_DepositBatchID = ap.trust_register
            If rpt.RequestReportParameters() = DialogResult.OK Then
                rpt.RunReportInSeparateThread()
            End If
        End Sub
    End Class
End Namespace
