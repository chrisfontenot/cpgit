Option Compare Binary
Option Explicit On
Option Strict On

Namespace ACH.Post

    Friend Class ArgParser
        Inherits DebtPlus.Utils.ArgParserBase

        Friend Batch As System.Int32 = -1
        Friend trust_register As System.Int32 = -1

        ''' <summary>
        ''' Create an instance of our class
        ''' </summary>
        Public Sub New()
            MyBase.New(New String() {})
        End Sub

        ''' <summary>
        ''' Generate the command usage information
        ''' </summary>
        Protected Overrides Sub OnUsage(ByVal errorInfo As String)
            ' deprecated
        End Sub
    End Class
End Namespace
