#Region "Copyright 2000-2012 DebtPlus, L.L.C."
'{*******************************************************************}
'{                                                                   }
'{       DebtPlus Debt Management System                             }
'{                                                                   }
'{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
'{       ALL RIGHTS RESERVED                                         }
'{                                                                   }
'{   The entire contents of this file is protected by U.S. and       }
'{   International Copyright Laws. Unauthorized reproduction,        }
'{   reverse-engineering, and distribution of all or any portion of  }
'{   the code contained in this file is strictly prohibited and may  }
'{   result in severe civil and criminal penalties and will be       }
'{   prosecuted to the maximum extent possible under the law.        }
'{                                                                   }
'{   RESTRICTIONS                                                    }
'{                                                                   }
'{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
'{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
'{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
'{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
'{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
'{                                                                   }
'{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
'{   ADDITIONAL RESTRICTIONS.                                        }
'{                                                                   }
'{*******************************************************************}
#End Region

Option Compare Binary
Option Explicit On
Option Strict On

Imports DebtPlus.Utils
Imports System.Data.SqlClient

Namespace ACH.Post
    Friend Class ProcessBatch
        Inherits DevExpress.XtraEditors.XtraUserControl

        Public Event Completed As System.EventHandler

        Private WithEvents bw As New System.ComponentModel.BackgroundWorker

        Private ReadOnly Property ap As ArgParser
            Get
                Return CType(ParentForm, Form_ACH_Post).ap
            End Get
        End Property

#Region " Windows Form Designer generated code "

        Public Sub New()
            MyBase.New()
            InitializeComponent()
        End Sub

        'UserControl overrides dispose to clean up the component list.
        Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
            If disposing Then
                If components IsNot Nothing Then
                    components.Dispose()
                End If
            End If
            MyBase.Dispose(disposing)
        End Sub

        'Required by the Windows Form Designer
        Private components As System.ComponentModel.IContainer

        'NOTE: The following procedure is required by the Windows Form Designer
        'It can be modified using the Windows Form Designer.  
        'Do not modify it using the code editor.
        Friend WithEvents MarqueeProgressBarControl1 As DevExpress.XtraEditors.MarqueeProgressBarControl
        Friend WithEvents LabelControl1 As DevExpress.XtraEditors.LabelControl
        <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
            Me.MarqueeProgressBarControl1 = New DevExpress.XtraEditors.MarqueeProgressBarControl
            Me.LabelControl1 = New DevExpress.XtraEditors.LabelControl
            CType(Me.MarqueeProgressBarControl1.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
            Me.SuspendLayout()
            '
            'MarqueeProgressBarControl1
            '
            Me.MarqueeProgressBarControl1.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                                                          Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
            Me.MarqueeProgressBarControl1.EditValue = 0
            Me.MarqueeProgressBarControl1.Location = New System.Drawing.Point(8, 64)
            Me.MarqueeProgressBarControl1.Name = "MarqueeProgressBarControl1"
            Me.MarqueeProgressBarControl1.Properties.Stopped = True
            Me.MarqueeProgressBarControl1.Size = New System.Drawing.Size(384, 18)
            Me.MarqueeProgressBarControl1.TabIndex = 0
            '
            'LabelControl1
            '
            Me.LabelControl1.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                                             Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
            Me.LabelControl1.Appearance.Options.UseTextOptions = True
            Me.LabelControl1.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center
            Me.LabelControl1.Location = New System.Drawing.Point(8, 24)
            Me.LabelControl1.Name = "LabelControl1"
            Me.LabelControl1.Size = New System.Drawing.Size(233, 13)
            Me.LabelControl1.TabIndex = 1
            Me.LabelControl1.Text = "We are currently posting the batch. Please wait."
            '
            'ProcessBatch
            '
            Me.Controls.Add(Me.LabelControl1)
            Me.Controls.Add(Me.MarqueeProgressBarControl1)
            Me.Name = "ProcessBatch"
            Me.Size = New System.Drawing.Size(400, 224)
            CType(Me.MarqueeProgressBarControl1.Properties, System.ComponentModel.ISupportInitialize).EndInit()
            Me.ResumeLayout(False)
            Me.PerformLayout()

        End Sub

#End Region

        ''' <summary>
        ''' Start the posting operation
        ''' </summary>
        Public Sub Process()
            MarqueeProgressBarControl1.Properties.Stopped = False
            bw.RunWorkerAsync()
        End Sub

        ''' <summary>
        ''' Indicate that we have finished processing
        ''' </summary>
        Private Sub bw_RunWorkerCompleted(ByVal sender As Object, ByVal e As System.ComponentModel.RunWorkerCompletedEventArgs) Handles bw.RunWorkerCompleted
            MarqueeProgressBarControl1.Properties.Stopped = True
            RaiseEvent Completed(Me, EventArgs.Empty)
        End Sub

        ''' <summary>
        ''' Post the batch
        ''' </summary>
        Private Sub bw_DoWork(ByVal sender As Object, ByVal e As System.ComponentModel.DoWorkEventArgs) Handles bw.DoWork

            Dim cn As SqlClient.SqlConnection = New SqlConnection(LibraryGlobals.SqlInfo.ConnectionString)
            Try
                cn.Open()
                Using cmd As SqlClient.SqlCommand = New SqlCommand
                    With cmd
                        .Connection = cn
                        .CommandText = "xpr_ACH_Post_Batch"
                        .CommandType = CommandType.StoredProcedure
                        .Parameters.Add("RETURN", SqlDbType.Int).Direction = ParameterDirection.ReturnValue
                        .Parameters.Add("@ACHFile", SqlDbType.Int).Value = ap.Batch
                        .ExecuteNonQuery()

                        ap.trust_register = Convert.ToInt32(.Parameters(0).Value)
                    End With
                End Using

            Finally
                If cn IsNot Nothing Then cn.Dispose()
            End Try
        End Sub

        ''' <summary>
        ''' This is the load of the control -- initialize it.
        ''' </summary>
        Private Sub ProcessBatch_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles MyBase.Load
            MarqueeProgressBarControl1.Properties.Stopped = True
        End Sub
    End Class
End Namespace
