Namespace ACH.Response.File
    <Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
    Partial Class ACHStatus_Form
        Inherits DebtPlus.Data.Forms.DebtPlusForm

        'Form overrides dispose to clean up the component list.
        <System.Diagnostics.DebuggerNonUserCode()> _
        Protected Overrides Sub Dispose(ByVal disposing As Boolean)
            Try
                If disposing AndAlso components IsNot Nothing Then
                    components.Dispose()
                End If
            Finally
                MyBase.Dispose(disposing)
            End Try
        End Sub

        'Required by the Windows Form Designer
        Private components As System.ComponentModel.IContainer

        'NOTE: The following procedure is required by the Windows Form Designer
        'It can be modified using the Windows Form Designer.  
        'Do not modify it using the code editor.
        <System.Diagnostics.DebuggerStepThrough()> _
        Private Sub InitializeComponent()
            Me.LabelControl1 = New DevExpress.XtraEditors.LabelControl
            Me.LabelControl2 = New DevExpress.XtraEditors.LabelControl
            Me.LabelControl3 = New DevExpress.XtraEditors.LabelControl
            Me.Label_Status = New DevExpress.XtraEditors.LabelControl
            Me.Button_OK = New DevExpress.XtraEditors.SimpleButton
            Me.Label_BatchCount = New DevExpress.XtraEditors.LabelControl
            Me.Label_RecordCount = New DevExpress.XtraEditors.LabelControl
            Me.Label_Payments = New DevExpress.XtraEditors.LabelControl
            CType(Me.DxErrorProvider1, System.ComponentModel.ISupportInitialize).BeginInit()
            Me.SuspendLayout()
            '
            'LabelControl1
            '
            Me.LabelControl1.Location = New System.Drawing.Point(12, 52)
            Me.LabelControl1.Name = "LabelControl1"
            Me.LabelControl1.Size = New System.Drawing.Size(147, 13)
            Me.LabelControl1.TabIndex = 0
            Me.LabelControl1.Text = "Number of batches processed:"
            '
            'LabelControl2
            '
            Me.LabelControl2.Location = New System.Drawing.Point(12, 71)
            Me.LabelControl2.Name = "LabelControl2"
            Me.LabelControl2.Size = New System.Drawing.Size(145, 13)
            Me.LabelControl2.TabIndex = 1
            Me.LabelControl2.Text = "Number of records processed:"
            '
            'LabelControl3
            '
            Me.LabelControl3.Location = New System.Drawing.Point(12, 90)
            Me.LabelControl3.Name = "LabelControl3"
            Me.LabelControl3.Size = New System.Drawing.Size(169, 13)
            Me.LabelControl3.TabIndex = 2
            Me.LabelControl3.Text = "Number of payment reject records:"
            '
            'Label_Status
            '
            Me.Label_Status.Appearance.Font = New System.Drawing.Font("Tahoma", 14.0!, System.Drawing.FontStyle.Bold)
            Me.Label_Status.Appearance.Options.UseFont = True
            Me.Label_Status.Location = New System.Drawing.Point(12, 12)
            Me.Label_Status.Name = "Label_Status"
            Me.Label_Status.Size = New System.Drawing.Size(308, 23)
            Me.Label_Status.TabIndex = 6
            Me.Label_Status.Text = "Response File Processing Status"
            '
            'Button_OK
            '
            Me.Button_OK.Anchor = System.Windows.Forms.AnchorStyles.Bottom
            Me.Button_OK.Location = New System.Drawing.Point(146, 122)
            Me.Button_OK.Name = "Button_OK"
            Me.Button_OK.Size = New System.Drawing.Size(75, 23)
            Me.Button_OK.TabIndex = 7
            Me.Button_OK.Text = "&Cancel"
            '
            'Label_BatchCount
            '
            Me.Label_BatchCount.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
            Me.Label_BatchCount.Appearance.Options.UseTextOptions = True
            Me.Label_BatchCount.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
            Me.Label_BatchCount.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None
            Me.Label_BatchCount.Location = New System.Drawing.Point(274, 52)
            Me.Label_BatchCount.Name = "Label_BatchCount"
            Me.Label_BatchCount.Size = New System.Drawing.Size(80, 13)
            Me.Label_BatchCount.TabIndex = 8
            Me.Label_BatchCount.Text = "0"
            Me.Label_BatchCount.UseMnemonic = False
            '
            'Label_RecordCount
            '
            Me.Label_RecordCount.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
            Me.Label_RecordCount.Appearance.Options.UseTextOptions = True
            Me.Label_RecordCount.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
            Me.Label_RecordCount.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None
            Me.Label_RecordCount.Location = New System.Drawing.Point(274, 71)
            Me.Label_RecordCount.Name = "Label_RecordCount"
            Me.Label_RecordCount.Size = New System.Drawing.Size(80, 13)
            Me.Label_RecordCount.TabIndex = 9
            Me.Label_RecordCount.Text = "0"
            Me.Label_RecordCount.UseMnemonic = False
            '
            'Label_Payments
            '
            Me.Label_Payments.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
            Me.Label_Payments.Appearance.Options.UseTextOptions = True
            Me.Label_Payments.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
            Me.Label_Payments.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None
            Me.Label_Payments.Location = New System.Drawing.Point(274, 90)
            Me.Label_Payments.Name = "Label_Payments"
            Me.Label_Payments.Size = New System.Drawing.Size(80, 13)
            Me.Label_Payments.TabIndex = 10
            Me.Label_Payments.Text = "0"
            Me.Label_Payments.UseMnemonic = False
            '
            'ACHStatus_Form
            '
            Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
            Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
            Me.ClientSize = New System.Drawing.Size(366, 157)
            Me.Controls.Add(Me.Label_Payments)
            Me.Controls.Add(Me.Label_RecordCount)
            Me.Controls.Add(Me.Label_BatchCount)
            Me.Controls.Add(Me.Button_OK)
            Me.Controls.Add(Me.Label_Status)
            Me.Controls.Add(Me.LabelControl3)
            Me.Controls.Add(Me.LabelControl2)
            Me.Controls.Add(Me.LabelControl1)
            Me.Name = "ACHStatus_Form"
            Me.ToolTipController1.SetSuperTip(Me, Nothing)
            Me.Text = "ACH Reject File Processing Status"
            CType(Me.DxErrorProvider1, System.ComponentModel.ISupportInitialize).EndInit()
            Me.ResumeLayout(False)
            Me.PerformLayout()

        End Sub
        Friend WithEvents LabelControl1 As DevExpress.XtraEditors.LabelControl
        Friend WithEvents LabelControl2 As DevExpress.XtraEditors.LabelControl
        Friend WithEvents LabelControl3 As DevExpress.XtraEditors.LabelControl
        Friend WithEvents Label_Status As DevExpress.XtraEditors.LabelControl
        Friend WithEvents Button_OK As DevExpress.XtraEditors.SimpleButton
        Friend WithEvents Label_BatchCount As DevExpress.XtraEditors.LabelControl
        Friend WithEvents Label_RecordCount As DevExpress.XtraEditors.LabelControl
        Friend WithEvents Label_Payments As DevExpress.XtraEditors.LabelControl
    End Class
End Namespace