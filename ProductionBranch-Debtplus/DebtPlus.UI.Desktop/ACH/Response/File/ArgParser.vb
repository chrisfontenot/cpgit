Option Compare Binary
Option Explicit On
Option Strict On

Imports System.IO
Imports System.Windows.Forms

Namespace ACH.Response.File
    Friend Class ArgParser
        Inherits DebtPlus.Utils.ArgParserBase

        Public pathnames As New ArrayList
        Public response_batch_id As String = System.Guid.NewGuid().ToString().ToUpper()

        ''' <summary>
        ''' Create an instance of our class
        ''' </summary>
        Public Sub New()
            MyBase.New(New String() {})
        End Sub

        ''' <summary>
        ''' Process the switch argument. This is the input file.
        ''' </summary>
        Protected Overrides Function OnSwitch(ByVal switchSymbol As String, ByVal switchValue As String) As SwitchStatus
            Dim ss As SwitchStatus
            Select Case switchSymbol
                Case "?"
                    ss = DebtPlus.Utils.ArgParserBase.SwitchStatus.ShowUsage
                Case Else
                    ss = DebtPlus.Utils.ArgParserBase.SwitchStatus.YesError
            End Select
            Return ss
        End Function

        ''' <summary>
        ''' Process the non-switch argument. This is the input file.
        ''' </summary>
        Protected Overrides Function OnNonSwitch(ByVal switchValue As String) As SwitchStatus
            Dim ss As SwitchStatus = DebtPlus.Utils.ArgParserBase.SwitchStatus.NoError

            ' Add command-line argument to array of pathnames.
            ' Convert switchValue to set of pathnames, add each pathname to the pathnames ArrayList.
            Try
                Dim d As String = Path.GetDirectoryName(switchValue)
                Dim dir As DirectoryInfo
                If (d.Length = 0) Then
                    dir = New DirectoryInfo(".")
                Else
                    dir = New DirectoryInfo(d)
                End If

                For Each f As FileInfo In dir.GetFiles(Path.GetFileName(switchValue))
                    If ValidFile(f.FullName) Then
                        pathnames.Add(f.FullName)
                    End If
                Next f

            Catch ex As System.Security.SecurityException
                Throw ex

            Catch ex As Exception
                ss = DebtPlus.Utils.ArgParserBase.SwitchStatus.YesError
            End Try

            If pathnames.Count = 0 Then
                AppendErrorLine("None of the specified files exist.")
                ss = DebtPlus.Utils.ArgParserBase.SwitchStatus.YesError
            End If

            Return ss
        End Function

        ''' <summary>
        ''' Process the end of the parse operation.
        ''' </summary>
        Protected Overrides Function OnDoneParse() As SwitchStatus
            Dim ss As SwitchStatus = DebtPlus.Utils.ArgParserBase.SwitchStatus.NoError

            ' Read the source file(s) if they are not given on the command line.
            If pathnames.Count = 0 Then
                With New System.Windows.Forms.OpenFileDialog
                    .AddExtension = True
                    .CheckFileExists = True
                    .CheckPathExists = True
                    .DefaultExt = "txt"
                    .Filter = "Text Files (*.txt)|*.txt|All Files (*.*)|*.*"
                    .FilterIndex = 0
                    .Multiselect = True
                    .ReadOnlyChecked = True
                    .RestoreDirectory = True
                    .ShowReadOnly = False
                    .Title = "Open input response file"
                    .ValidateNames = True
                    AddHandler .FileOk, AddressOf OpenFileDialog_FileOk

                    If .ShowDialog() <> DialogResult.OK Then ss = DebtPlus.Utils.ArgParserBase.SwitchStatus.YesError
                    If ss = DebtPlus.Utils.ArgParserBase.SwitchStatus.NoError Then
                        For Each fname As String In .FileNames()
                            pathnames.Add(fname)
                        Next
                    End If
                End With
            End If

            ' Sort the filenames.
            If pathnames.Count > 0 Then
                pathnames.Sort(0, pathnames.Count, Nothing)
            End If

            Return ss
        End Function

        ''' <summary>
        ''' Generate the command usage information
        ''' </summary>
        Protected Overrides Sub OnUsage(ByVal errorInfo As String)
            ' deprecated
        End Sub

        ''' <summary>
        ''' Process the FileOK function for the dialog
        ''' </summary>
        Private Sub OpenFileDialog_FileOk(ByVal sender As Object, ByVal e As System.ComponentModel.CancelEventArgs)
            For Each FileName As String In CType(sender, OpenFileDialog).FileNames
                If Not ValidFile(FileName) Then
                    e.Cancel = True
                    Exit For
                End If
            Next
        End Sub

        ''' <summary>
        ''' Determine if the file is valid
        ''' </summary>
        Private Function ValidFile(ByVal FileName As String) As Boolean
            Dim answer As Boolean = False

            Try
                Dim FirstPart As String = String.Empty
                Dim fs As New System.IO.StreamReader(New System.IO.FileStream(FileName, FileMode.Open, FileAccess.Read, FileShare.Read, 4096))
                FirstPart = fs.ReadLine()
                fs.Close()

                ' Look for a valid response file
                If FirstPart.Length > 80 Then
                    If FirstPart.StartsWith("101") AndAlso FirstPart.Substring(33, 7) = "1094101" Then
                        answer = True
                    End If
                End If

            Catch ex As System.IO.FileNotFoundException
            Catch ex As System.IO.DirectoryNotFoundException
            Catch ex As System.IO.EndOfStreamException
                ' These are ignored at this point

            Catch ex As Exception
                Throw
            End Try

            Return answer
        End Function
    End Class
End NameSpace
