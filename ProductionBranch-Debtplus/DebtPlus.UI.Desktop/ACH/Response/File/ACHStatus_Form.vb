#Region "Copyright 2000-2012 DebtPlus, L.L.C."
'{*******************************************************************}
'{                                                                   }
'{       DebtPlus Debt Management System                             }
'{                                                                   }
'{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
'{       ALL RIGHTS RESERVED                                         }
'{                                                                   }
'{   The entire contents of this file is protected by U.S. and       }
'{   International Copyright Laws. Unauthorized reproduction,        }
'{   reverse-engineering, and distribution of all or any portion of  }
'{   the code contained in this file is strictly prohibited and may  }
'{   result in severe civil and criminal penalties and will be       }
'{   prosecuted to the maximum extent possible under the law.        }
'{                                                                   }
'{   RESTRICTIONS                                                    }
'{                                                                   }
'{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
'{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
'{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
'{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
'{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
'{                                                                   }
'{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
'{   ADDITIONAL RESTRICTIONS.                                        }
'{                                                                   }
'{*******************************************************************}

#End Region

Option Compare Binary
Option Explicit On
Option Strict On

Imports System.ComponentModel
Imports DebtPlus.Utils
Imports System.IO
Imports System.Collections.Specialized
Imports System.Data.SqlClient
Imports System.Windows.Forms
Imports MessageBox = DebtPlus.Data.Forms.MessageBox

Namespace ACH.Response.File
    Friend Class ACHStatus_Form
        ' Thread to handle the reading and processing of the input files
        Private WithEvents bw As New BackgroundWorker
        Private ap As ArgParser = Nothing

        Public Sub New(ByVal ap As ArgParser)
            MyBase.New()
            InitializeComponent()
            AddHandler bw.DoWork, AddressOf bw_DoWork
            AddHandler Button_OK.Click, AddressOf Button_OK_Click
            AddHandler Me.Load, AddressOf StatusForm_Load
            AddHandler bw.RunWorkerCompleted, AddressOf bw_RunWorkerCompleted
            TopMost = True
            WindowState = FormWindowState.Normal
            BringToFront()
            Me.ap = ap
        End Sub

        Private Sub Button_OK_Click(ByVal sender As Object, ByVal e As EventArgs)
            Close()
        End Sub

        Private Sub StatusForm_Load(ByVal sender As Object, ByVal e As EventArgs)

            ' Start the background thread to process the files.
            bw.RunWorkerAsync()
        End Sub

        Private Sub bw_RunWorkerCompleted(ByVal sender As Object, ByVal e As RunWorkerCompletedEventArgs)
            Label_Status.Text = "Processing Complete. Press OK"
            Button_OK.Text = "&OK"
        End Sub

        ''' <summary>
        ''' Update the counts for the various items
        ''' </summary>
        Private Delegate Sub StatusDelegate(ByVal Count As Int32)

        Friend record_count As Int32 = 0

        Private Sub RecordCount(ByVal Count As Int32)
            If InvokeRequired Then
                Invoke(New StatusDelegate(AddressOf RecordCount), New Object() {Count})
            Else
                Label_RecordCount.Text = String.Format("{0:n0}", Count)
            End If
        End Sub

        ''' <summary>
        ''' Update the number of batches rejected
        ''' </summary>
        Friend batch_count As Int32 = 0

        Private Sub BatchCount(ByVal Count As Int32)
            If InvokeRequired Then
                Invoke(New StatusDelegate(AddressOf BatchCount), New Object() {Count})
            Else
                Label_BatchCount.Text = String.Format("{0:n0}", Count)
            End If
        End Sub

        ''' <summary>
        ''' Update the number of payments rejected
        ''' </summary>
        Friend payment_count As Int32 = 0

        Private Sub PaymentCount(ByVal Count As Int32)
            If InvokeRequired Then
                Invoke(New StatusDelegate(AddressOf PaymentCount), New Object() {Count})
            Else
                Label_Payments.Text = String.Format("{0:n0}", Count)
            End If
        End Sub

        ''' <summary>
        ''' Handle the thread procedure
        ''' </summary>
        Private Sub bw_DoWork(ByVal sender As Object, ByVal e As DoWorkEventArgs)

            ' Process the fiels in this response file
            For Each fname As String In ap.pathnames
                With New InputProcessing(Me, ap.response_batch_id)
                    .ParseFile(fname)
                End With
            Next
        End Sub

        ''' <summary>
        ''' Class to read the input file
        ''' </summary>
        Private Class Reader
            Implements IDisposable

            Private fs As FileStream = Nothing
            Private InputStream As StreamReader = Nothing
            Private RecordType As String = String.Empty
            Private privateFileName As String = String.Empty
            Private privateAtEnd As Boolean = False
            Private privateNextLine As String = String.Empty

            Private parent As ACHStatus_Form = Nothing

            ''' <summary>
            ''' Create an instance of our class
            ''' </summary>
            Public Sub New(ByVal p As ACHStatus_Form)
                MyClass.New(p, CType(Nothing, String))
            End Sub

            Public Sub New(ByVal p As ACHStatus_Form, ByVal fname As String)
                parent = p
                FileName = fname
            End Sub

            ''' <summary>
            ''' Input file name
            ''' </summary>
            Public Property FileName() As String
                Get
                    Return privateFileName
                End Get
                Set(ByVal value As String)
                    privateFileName = value
                End Set
            End Property

            ''' <summary>
            ''' Flag to indicate that the file is at the end
            ''' </summary>
            Public ReadOnly Property AtEnd() As Boolean
                Get
                    Return privateAtEnd
                End Get
            End Property

            ''' <summary>
            ''' Line buffer for the input file
            ''' </summary>
            Public ReadOnly Property NextLine() As String
                Get
                    Return privateNextLine
                End Get
            End Property

            ''' <summary>
            ''' Access the next line in the input file
            ''' </summary>
            Public Function ReadNextLine() As Boolean
                If Not AtEnd AndAlso InputStream.EndOfStream Then
                    privateAtEnd = True
                End If
                If AtEnd Then Return False

                privateNextLine = InputStream.ReadLine().PadRight(94)
                RecordType = privateNextLine.Substring(0, 1)

                parent.record_count += 1
                parent.RecordCount(parent.record_count)

                Return True
            End Function

            ''' <summary>
            ''' Look for a type 1 record
            ''' </summary>
            Public ReadOnly Property IsType1() As Boolean
                Get
                    Return RecordType = "1"
                End Get
            End Property

            ''' <summary>
            ''' Look for a type 5 record
            ''' </summary>
            Public ReadOnly Property IsType5() As Boolean
                Get
                    Return RecordType = "5"
                End Get
            End Property

            ''' <summary>
            ''' Look for a type 6 record
            ''' </summary>
            Public ReadOnly Property IsType6() As Boolean
                Get
                    Return RecordType = "6"
                End Get
            End Property

            ''' <summary>
            ''' Look for a type 7 record
            ''' </summary>
            Public ReadOnly Property IsType7() As Boolean
                Get
                    Return RecordType = "7"
                End Get
            End Property

            ''' <summary>
            ''' Look for a type 8 record
            ''' </summary>
            Public ReadOnly Property IsType8() As Boolean
                Get
                    Return RecordType = "8"
                End Get
            End Property

            ''' <summary>
            ''' Look for a type 9 record
            ''' </summary>
            Public ReadOnly Property IsType9() As Boolean
                Get
                    Return RecordType = "9"
                End Get
            End Property

            ''' <summary>
            ''' Open the input file
            ''' </summary>
            Public Function OpenFile() As Boolean
                Dim answer As Boolean = False

                Try
                    fs = New FileStream(FileName, FileMode.Open, FileAccess.Read, FileShare.Read, 4096)
                    InputStream = New StreamReader(fs)
                    answer = True

                Catch ex As FileNotFoundException
                Catch ex As DirectoryNotFoundException
                Catch ex As PathTooLongException
                End Try

                Return answer
            End Function

            ''' <summary>
            ''' Close the input file
            ''' </summary>
            Public Sub CloseFile()
                If InputStream IsNot Nothing Then InputStream.Close()
                If fs IsNot Nothing Then fs.Close()
            End Sub

            ''' <summary>
            ''' Dispose of the class
            ''' </summary>
            Private disposedValue As Boolean = False        ' To detect redundant calls
            Protected Overridable Sub Dispose(ByVal disposing As Boolean)
                If Not disposedValue Then
                    If disposing Then
                        CloseFile()
                    End If

                    ' set large fields to null.
                    InputStream = Nothing
                    fs = Nothing
                End If

                disposedValue = True
            End Sub

#Region " IDisposable Support "
            ' This code added by Visual Basic to correctly implement the disposable pattern.
            Public Sub Dispose() Implements IDisposable.Dispose
                ' Do not change this code.  Put cleanup code in Dispose(ByVal disposing As Boolean) above.
                Dispose(True)
                GC.SuppressFinalize(Me)
            End Sub

#End Region
        End Class

        ''' <summary>
        ''' Generic class for files, batches, and transactions
        ''' </summary>
        Private Class Responses
            Protected Sub New(ByVal rdr As Reader)
                Me.rdr = rdr
            End Sub

            ''' <summary>
            ''' Linkage to the file reader class
            ''' </summary>
            Private privaterdr As Reader = Nothing

            Public Property rdr() As Reader
                Get
                    Return privaterdr
                End Get
                Set(ByVal value As Reader)
                    privaterdr = value
                End Set
            End Property

            Private privateErrorCode As String = String.Empty

            Public Property ErrorCode() As String
                Get
                    Return privateErrorCode
                End Get
                Set(ByVal value As String)
                    privateErrorCode = value
                End Set
            End Property

            Public Overridable Function GetError() As String
                Return ErrorCode
            End Function

            Public Overridable Sub UpdateDatabase()
            End Sub
        End Class

        ''' <summary>
        ''' A transaction is a representation of the following class
        ''' </summary>
        Private NotInheritable Class StandardTransactionProcessing
            Inherits Responses

            Private parent As ACHStatus_Form = Nothing

            Private Batch As BatchProcessing = Nothing

            ' List of addendum records
            Private AddendumCollection As New StringCollection
            Private TransactionLine As String = String.Empty

            Public Sub New(ByVal p As ACHStatus_Form, ByVal rdr As Reader, ByVal Batch As BatchProcessing)
                MyBase.New(rdr)
                Me.Batch = Batch
                parent = p
            End Sub

            ''' <summary>
            ''' Parse the type 6 (Transaction Header) record
            ''' </summary>
            Public Function ParseType6() As Boolean
                Dim answer As Boolean = False

                ' Save the transaction line for later examination
                TransactionLine = rdr.NextLine

                ' Gather all of the addendum records for this line. We don't look at the count
                ' since the note buffers don't have a valid count.
                Do While rdr.ReadNextLine
                    If rdr.IsType7 Then
                        AddendumCollection.Add(rdr.NextLine)
                    Else
                        answer = True
                        Exit Do
                    End If
                Loop

                Return answer
            End Function

            ''' <summary>
            ''' Obtain the error status for the file
            ''' </summary>
            Public Overrides Function GetError() As String
                Dim answer As String = Batch.GetError
                If answer = String.Empty Then answer = ErrorCode
                Return answer
            End Function

            Public Overrides Sub UpdateDatabase()
                Dim TraceNumber As String = TransactionLine.Substring(79, 15)
                Dim ErrorCode As String = TransactionLine.Substring(12, 3)

                ' Change the values to the addendum line if there is one
                If AddendumCollection.Count > 0 AndAlso Batch.ResponseType = "RETURN    " Then
                    ErrorCode = AddendumCollection(0).Substring(3, 3)
                    TraceNumber = AddendumCollection(0).Substring(6, 15)
                End If

                ' Count this payment reject
                parent.payment_count += 1
                parent.PaymentCount(parent.payment_count)

                Dim cn As SqlConnection = New SqlConnection(DebtPlus.LINQ.SQLInfoClass.getDefault().ConnectionString)
                Dim current_cursor As Cursor = Cursor.Current
                Try
                    Cursor.Current = Cursors.WaitCursor
                    cn.Open()
                    With New SqlCommand
                        .Connection = cn
                        .CommandText = "xpr_ach_response_PAYMENT"
                        .CommandType = CommandType.StoredProcedure
                        .Parameters.Add("@batch_id", SqlDbType.VarChar, 80).Value = Batch.File.ResponseFileID
                        .Parameters.Add("@trace_number", SqlDbType.VarChar, 80).Value = TraceNumber
                        .Parameters.Add("@reason", SqlDbType.VarChar, 80).Value = GetError()
                        .ExecuteNonQuery()
                    End With

                Catch ex As SqlException
                    DebtPlus.UI.Common.ErrorHandling.HandleErrors(ex, "Error writing ACH reject transaction")

                Finally
                    If cn IsNot Nothing AndAlso cn.State <> ConnectionState.Closed Then cn.Close()
                    Cursor.Current = current_cursor
                End Try
            End Sub
        End Class

        ''' <summary>
        ''' A batch is a representation of the following class
        ''' </summary>
        Private Class BatchProcessing
            Inherits Responses

            ' List of transactions for this batch
            Private TranCollection As New LinkedList(Of Responses)
            Public File As FileProcessing = Nothing

            ' Information from the batch header
            Public BatchHeader As String = String.Empty
            Public ResponseType As String = String.Empty

            Private parent As ACHStatus_Form = Nothing

            Public Sub New(ByVal p As ACHStatus_Form, ByVal rdr As Reader, ByVal File As FileProcessing)
                MyBase.New(rdr)
                Me.File = File
                parent = p
            End Sub

            ''' <summary>
            ''' Obtain the error status for the file
            ''' </summary>
            Public Overrides Function GetError() As String
                Dim answer As String = File.GetError
                If answer = String.Empty Then answer = ErrorCode
                Return answer
            End Function

            ''' <summary>
            ''' Process a batch from the creditor
            ''' </summary>
            Public Function ParseType5() As Boolean
                Dim answer As Boolean = False

                ' Determine the biller id for the response
                BatchHeader = rdr.NextLine
                ResponseType = BatchHeader.Substring(53, 10)

                ' Read the first transaction line which should follow the batch header
                If Not rdr.ReadNextLine Then
                    Return False
                End If

                ' If this is the end of the batch then stop processing
                Do
                    If rdr.IsType8 Then
                        ParseType8()
                        answer = True
                        Exit Do
                    End If

                    ' If this is the end of file record then we missed the batch EOF
                    If rdr.IsType9 OrElse rdr.IsType1 Then
                        answer = False
                        Exit Do
                    End If

                    ' If this is a type 6 record then process the batch
                    If rdr.IsType6 Then
                        Dim Tran As New StandardTransactionProcessing(parent, rdr, Me)
                        If Tran.ParseType6 Then
                            TranCollection.AddLast(Tran)
                        End If

                        answer = False

                        ' If this is not the expected type, flush the record until we find one.
                    ElseIf Not rdr.ReadNextLine Then
                        answer = False
                        Exit Do
                    End If
                Loop

                Return answer
            End Function

            ''' <summary>
            ''' Parse the type 8 (EOB) record for batch trailer
            ''' </summary>
            Public Function ParseType8() As Boolean
                Dim answer As Boolean = False

                ' Save the error code associated with the batch
                ErrorCode = rdr.NextLine.Substring(54, 3).Trim()

                Return answer
            End Function

            Public Overrides Sub UpdateDatabase()

                parent.batch_count += 1
                parent.BatchCount(parent.batch_count)

                For Each tran As Responses In TranCollection
                    tran.UpdateDatabase()
                Next
            End Sub
        End Class

        ''' <summary>
        ''' A file is a representation of the following class
        ''' </summary>
        Private Class FileProcessing
            Inherits Responses

            Private parent As ACHStatus_Form = Nothing

            Private BatchCollection As New LinkedList(Of BatchProcessing)
            Public ResponseFileID As String = String.Empty
            Public FileHeader As String = String.Empty
            Public FileTrailer As String = String.Empty

            Public Sub New(ByVal p As ACHStatus_Form, ByVal rdr As Reader, ByVal ResponseFileID As String)
                MyBase.New(rdr)
                Me.ResponseFileID = ResponseFileID
                parent = p
            End Sub

            ''' <summary>
            ''' Examine the type 1 (HEADER) record which starts the file
            ''' </summary>
            Public Function ParseType1() As Boolean
                Dim answer As Boolean = False

                ' Examine the type 1 start of file record
                FileHeader = rdr.NextLine
                If FileHeader.Substring(0, 3) <> "101" OrElse FileHeader.Substring(33, 7) <> "1094101" Then
                    MessageBox.Show("The file does not have a valid RPPS reject header near line 1")
                    Return False
                End If

                ' Read the first line for the file contents
                If Not rdr.ReadNextLine Then
                    Return False
                End If

                Do
                    ' If this is the end of the file then stop processing
                    If rdr.IsType9 Then
                        ParseType9()
                        answer = True
                        Exit Do
                    End If

                    ' If this is a new type 1 record then we missed the type 9
                    If rdr.IsType1 Then
                        answer = False
                        Exit Do
                    End If

                    ' If this is a type 5 record then process the batch
                    If rdr.IsType5 Then
                        Dim batch As New BatchProcessing(Parent, rdr, Me)
                        If batch.ParseType5 Then
                            BatchCollection.AddLast(batch)
                        End If

                    ElseIf Not rdr.ReadNextLine Then
                        answer = False
                        Exit Do
                    End If
                Loop

                Return answer
            End Function

            ''' <summary>
            ''' Examine the type 9 (EOF) record which terminates the file
            ''' </summary>
            Public Function ParseType9() As Boolean

                ' Save the file trailer line
                FileTrailer = rdr.NextLine

                Return True
            End Function

            Public Overrides Sub UpdateDatabase()

                For Each batch As BatchProcessing In BatchCollection
                    batch.UpdateDatabase()
                Next
            End Sub
        End Class

        ''' <summary>
        ''' Class to manage the processing of multiple files in a dataset
        ''' </summary>
        Private Class InputProcessing
            Private parent As ACHStatus_Form = Nothing

            Private ResponseFileID As String = String.Empty

            Public Sub New(ByVal p As ACHStatus_Form, ByVal ResponseFileID As String)
                Me.ResponseFileID = ResponseFileID
                parent = p
            End Sub

            Public Sub ParseFile(ByVal FileName As String)
                Dim rdr As New Reader(parent, FileName)

                ' Open and read the first line in the file
                If rdr.OpenFile() Then
                    If rdr.ReadNextLine Then
                        Do
                            ' If the record is a type 1 then process the new file
                            If rdr.IsType1 Then
                                With New FileProcessing(parent, rdr, ResponseFileID)
                                    .ParseType1()
                                    .UpdateDatabase()
                                End With

                                ' Flush records until we reach the end or we find a type 1 record again.
                            ElseIf Not rdr.ReadNextLine Then
                                Exit Do
                            End If
                        Loop
                    End If

                    ' Close the opened file
                    rdr.CloseFile()
                End If
            End Sub
        End Class
    End Class
End Namespace

