#Region "Copyright 2000-2012 DebtPlus, L.L.C."
'{*******************************************************************}
'{                                                                   }
'{       DebtPlus Debt Management System                             }
'{                                                                   }
'{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
'{       ALL RIGHTS RESERVED                                         }
'{                                                                   }
'{   The entire contents of this file is protected by U.S. and       }
'{   International Copyright Laws. Unauthorized reproduction,        }
'{   reverse-engineering, and distribution of all or any portion of  }
'{   the code contained in this file is strictly prohibited and may  }
'{   result in severe civil and criminal penalties and will be       }
'{   prosecuted to the maximum extent possible under the law.        }
'{                                                                   }
'{   RESTRICTIONS                                                    }
'{                                                                   }
'{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
'{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
'{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
'{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
'{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
'{                                                                   }
'{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
'{   ADDITIONAL RESTRICTIONS.                                        }
'{                                                                   }
'{*******************************************************************}
#End Region

Imports DebtPlus.Letters.Compose
Imports DebtPlus.Utils
Imports System.Data.SqlClient
Imports System.Threading
Imports System.IO
Imports System.Windows.Forms

Namespace ACH.Response.File.ClearPoint
    Friend Class Form1

        Private Class ErrorInformation

            Public string_leader As String
            Public string_error_date As String
            Public string_ssn As String
            Public string_client_name As String
            Public string_amount As String
            Public string_error_code As String
            Public string_error_description As String
            Public string_client As String

            Public ErrorRow As DataRow
            Public ErrorLetter As String = String.Empty

            Private privateSeriousError As Boolean
            Public Property SeriousError() As Boolean
                Get
                    Return False AndAlso privateSeriousError    ' this is always false because the customer does not want this logic and won't set the flags to FALSE in the database.
                End Get
                Set(ByVal value As Boolean)
                    privateSeriousError = value
                End Set
            End Property

            Public ReadOnly Property Client() As Int32
                Get
                    Dim answer As Int32
                    If Int32.TryParse(string_client, answer) AndAlso answer > 0 Then Return answer
                    Return -1
                End Get
            End Property

            Public ReadOnly Property Amount() As Decimal
                Get
                    Dim answer As Decimal
                    If Decimal.TryParse(string_amount, answer) AndAlso answer > 0D Then Return answer
                    Return 0D
                End Get
            End Property

            Public ReadOnly Property ErrorDate() As Date
                Get
                    Dim answer As Date
                    If Date.TryParse(string_error_date, answer) Then Return answer
                    Return Date.MinValue
                End Get
            End Property

            Private Function ValidClient() As Boolean
                Dim ActiveStatus As String = "CRE"
                Dim cn As SqlConnection = New SqlConnection(DebtPlus.LINQ.SQLInfoClass.getDefault().ConnectionString)
                Try
                    cn.Open()
                    Using cmd As SqlCommand = New SqlCommand
                        With cmd
                            .Connection = cn
                            .CommandText = "SELECT active_status FROM clients WHERE client = @client"
                            .CommandType = CommandType.Text
                            .Parameters.Add("@client", SqlDbType.Int).Value = Client
                            Dim objAnswer As Object = .ExecuteScalar
                            If objAnswer IsNot Nothing AndAlso objAnswer IsNot DBNull.Value Then ActiveStatus = Convert.ToString(objAnswer)
                        End With
                    End Using

                Catch ex As SqlException
                    Using gdr = New Repository.GetDataResult()
                        gdr.HandleException(ex)
                        DebtPlus.UI.Common.ErrorHandling.HandleErrors(gdr, My.Resources.ErrorReadingFromClientsTable)
                    End Using

                Finally
                    If cn IsNot Nothing Then cn.Dispose()
                End Try

                Return String.Compare(ActiveStatus, "A", False) = 0 OrElse String.Compare(ActiveStatus, "AR", False) = 0
            End Function

            Public Sub GenerateLetter()
                Using ltr As New Composition(ErrorLetter)
                    AddHandler ltr.GetValue, AddressOf LetterCallback
                    ltr.PrintLetter(False, True)
                End Using
            End Sub

            Private Sub LetterCallback(ByVal Sender As Object, ByVal e As DebtPlus.Events.ParameterValueEventArgs)

                Select Case e.Name
                    Case "ach_error_code"
                        e.Value = string_error_code
                    Case "ach_error_description"
                        e.Value = string_error_description
                    Case "ach_client_name"
                        e.Value = string_client_name
                    Case "ach_ssn"
                        e.Value = string_ssn
                    Case "ach_client", DebtPlus.Events.ParameterValueEventArgs.name_client
                        e.Value = Client
                    Case "ach_amount"
                        e.Value = Amount
                    Case "ach_error_date"
                        e.Value = ErrorDate
                    Case Else
                End Select
            End Sub

            Public Sub MarkClient()
                Dim cn As SqlConnection = New SqlConnection(DebtPlus.LINQ.SQLInfoClass.getDefault().ConnectionString)
                Try
                    cn.Open()
                    Using cmd As SqlCommand = New SqlCommand
                        With cmd
                            .Connection = cn
                            .CommandText = "UPDATE client_ach SET [ErrorDate]=getdate() WHERE [client]=@client AND [ErrorDate] IS NULL"
                            .Parameters.Add("@client", SqlDbType.Int).Value = Client
                            .ExecuteNonQuery()
                        End With
                    End Using

                Catch ex As SqlException
                    Using gdr = New Repository.GetDataResult()
                        gdr.HandleException(ex)
                        DebtPlus.UI.Common.ErrorHandling.HandleErrors(gdr, My.Resources.ErrorUpdatingClientsTable)
                    End Using

                Finally
                    If cn IsNot Nothing Then cn.Dispose()
                End Try
            End Sub

            Public ReadOnly Property IsValid() As Boolean
                Get
                    Return Client > 0 AndAlso ErrorDate <> Date.MinValue AndAlso Amount > 0D AndAlso ValidClient()
                End Get
            End Property
        End Class

        Private Class ThreadClass

            Public ItemList As New List(Of ErrorInformation)

            Public Sub ProcessItems()
                For Each e As ErrorInformation In ItemList
                    With New Thread(New ParameterizedThreadStart(AddressOf ProcessingThread))
                        .SetApartmentState(Threading.ApartmentState.STA)
                        .Start(e)       ' Start the thread
                        .Join()         ' Wait for it to terminate. We need the thread for safety.
                    End With
                Next
            End Sub

            Private Sub ProcessingThread(ByVal obj As Object)
                Dim e As ErrorInformation = CType(obj, ErrorInformation)

                ' Generate the letter
                If String.Compare(e.ErrorLetter, String.Empty, False) <> 0 Then e.GenerateLetter()

                ' Mark the client as disabled from ACH
                If e.SeriousError Then e.MarkClient()
            End Sub
        End Class

        Private Filename As String
        Private ap As ArgParser
        Public Sub New(ByVal ap As ArgParser, ByVal Filename As String)
            MyBase.New()
            InitializeComponent()
            AddHandler BarButtonItem_File_Exit.ItemClick, AddressOf BarButtonItem_File_Exit_ItemClick
            AddHandler Me.Load, AddressOf Form1_Load
            AddHandler BarButtonItem_File_Generate.ItemClick, AddressOf BarButtonItem_File_Generate_ItemClick

            Me.ap = ap
            Me.Filename = Filename
        End Sub

        Private Sub BarButtonItem_File_Exit_ItemClick(ByVal sender As System.Object, ByVal e As DevExpress.XtraBars.ItemClickEventArgs)
            Close()
        End Sub

        Private ds As New DataSet("ds")
        Private tbl As DataTable
        Private Sub Form1_Load(ByVal sender As Object, ByVal e As EventArgs)

            ' Read the list of ach error codes
            SyncLock ds
                tbl = ds.Tables("ach_reject_codes")
                If tbl Is Nothing Then
                    Using cmd As SqlCommand = New SqlCommand
                        With cmd
                            .Connection = New SqlConnection(DebtPlus.LINQ.SQLInfoClass.getDefault().ConnectionString)
                            .CommandText = "SELECT [ach_reject_code],[description],[serious_error],[letter_code],[date_created],[created_by] FROM ach_reject_codes"
                            .CommandType = CommandType.Text
                        End With

                        Using da As New SqlDataAdapter(cmd)
                            da.Fill(ds, "ach_reject_codes")
                        End Using
                    End Using

                    tbl = ds.Tables("ach_reject_codes")
                    With tbl
                        .PrimaryKey = New DataColumn() {.Columns("ach_reject_code")}
                    End With
                End If
            End SyncLock

            ' Load the text control with the default file
            With RichTextBox1

                ' Read the file string into memory
                Dim FileText As String = String.Empty
                Dim File As StreamReader = Nothing
                Dim FType As Boolean = FileType(Filename)
                Try
                    File = New StreamReader(Filename)
                    FileText = File.ReadToEnd

                    ' If RTF is selected then ensure that it is a valid file.
                    If FType AndAlso (FileText.Length < 6 OrElse Not FileText.StartsWith("{\rtf")) Then FType = False

                    ' Load the file into the control.
                    If FType Then
                        .Rtf = FileText
                    Else
                        .Text = FileText
                    End If

                Catch ex As FileNotFoundException
                    DebtPlus.Data.Forms.MessageBox.Show(String.Format(My.Resources.TheFile0CouldNotBeFoundOnTheDisk, Filename), My.Resources.ErrorReadingDiskFile, MessageBoxButtons.OK, MessageBoxIcon.Error)

                Catch ex As FormatException
                    DebtPlus.Data.Forms.MessageBox.Show(String.Format(My.Resources.TheFile0CouldNotBeFoundOnTheDisk, Filename), My.Resources.ErrorReadingDiskFile, MessageBoxButtons.OK, MessageBoxIcon.Error)

                Finally
                    If File IsNot Nothing Then
                        File.Close()
                        File.Dispose()
                    End If
                End Try
            End With
        End Sub

        Private Shared Function FileType(ByVal fname As String) As Boolean
            Dim answer As Boolean
            Select Case Path.GetExtension(fname).ToLower()
                Case ".rtf"
                    answer = True
                Case Else
                    answer = False
            End Select
            Return answer
        End Function

        Private ClientCount As Int32
        Private Sub BarButtonItem_File_Generate_ItemClick(ByVal sender As System.Object, ByVal e As DevExpress.XtraBars.ItemClickEventArgs)
            Dim thrd As New ThreadClass()
            Dim txt As String = RichTextBox1.Text
            Dim Lines() As String = txt.Split(ControlChars.Lf)

            ' Process the items in the text file.
            ClientCount = 0
            For Each TextLine As String In Lines
                ProcessTextLine(TextLine, thrd)

                ' If we now have 30 items then start a thread to handle the items.
                If thrd.ItemList.Count = 30 Then
                    With New Thread(AddressOf thrd.ProcessItems)
                        .SetApartmentState(Threading.ApartmentState.STA)
                        .Start()
                    End With
                    thrd = New ThreadClass()
                End If
            Next

            ' Run the last thread to handle the last block of items
            If thrd.ItemList.Count > 0 Then
                With New Thread(AddressOf thrd.ProcessItems)
                    .SetApartmentState(Threading.ApartmentState.STA)
                    .Start()
                End With
            End If

            DebtPlus.Data.Forms.MessageBox.Show(String.Format(My.Resources.N0ClientsProcessed, ClientCount), My.Resources.OperationComplete, MessageBoxButtons.OK)
            DialogResult = Windows.Forms.DialogResult.OK
        End Sub

        Private Sub ProcessTextLine(ByVal TextLine As String, ByRef Thrd As ThreadClass)
            Dim Fields() As String = TextLine.Split(ControlChars.Tab)
            If Fields.GetUpperBound(0) - Fields.GetLowerBound(0) + 1 = 8 Then

                ' Store the fields into the appropriate names
                Dim e As New ErrorInformation()
                With e
                    .string_leader = Fields(0)
                    .string_error_date = Fields(1)
                    .string_ssn = Fields(2)
                    .string_client_name = Fields(3)
                    .string_amount = Fields(4).Replace("$", "").Replace(",", "")
                    .string_error_code = Fields(5)
                    .string_error_description = Fields(6).Trim()
                    .string_client = Fields(7)

                    ' Find the corresponding row to determine what to do with the error
                    .ErrorRow = tbl.Rows.Find(.string_error_code)
                    If .ErrorRow IsNot Nothing Then

                        ' Process the field information
                        .ErrorLetter = String.Empty
                        .SeriousError = False
                        If .ErrorRow("letter_code") IsNot Nothing AndAlso .ErrorRow("letter_code") IsNot DBNull.Value Then .ErrorLetter = Convert.ToString(.ErrorRow("letter_code")).Trim()
                        If .ErrorRow("serious_error") IsNot Nothing AndAlso .ErrorRow("serious_error") IsNot DBNull.Value Then .SeriousError = Convert.ToInt32(.ErrorRow("serious_error")) <> 0
                    End If
                End With

                ' Fork a thread to handle this error condition
                If e.IsValid AndAlso (String.Compare(e.ErrorLetter, String.Empty, False) <> 0 OrElse e.SeriousError) Then
                    ClientCount += 1
                    Thrd.ItemList.Add(e)
                End If
            End If
        End Sub
    End Class
End Namespace

