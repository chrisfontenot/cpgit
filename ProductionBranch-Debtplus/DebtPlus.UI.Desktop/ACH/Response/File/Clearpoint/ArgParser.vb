Option Compare Binary
Option Explicit On
Option Strict On

Imports System.IO
Imports System.Windows.Forms

Namespace ACH.Response.File.ClearPoint

    Friend Class ArgParser
        Inherits DebtPlus.Utils.ArgParserBase

        Public pathnames As New System.Collections.Generic.List(Of String)

        ''' <summary>
        ''' Create an instance of our class
        ''' </summary>
        Public Sub New()
            MyBase.New(New String() {})
        End Sub

        ''' <summary>
        ''' Process the switch argument. This is the input file.
        ''' </summary>
        Protected Overrides Function OnSwitch(ByVal switchSymbol As String, ByVal switchValue As String) As SwitchStatus
            Dim ss As SwitchStatus
            Select Case switchSymbol
                Case "?"
                    ss = DebtPlus.Utils.ArgParserBase.SwitchStatus.ShowUsage
                Case Else
                    ss = DebtPlus.Utils.ArgParserBase.SwitchStatus.YesError
            End Select
            Return ss
        End Function

        ''' <summary>
        ''' Process the non-switch argument. This is the input file.
        ''' </summary>
        Protected Overrides Function OnNonSwitch(ByVal switchValue As String) As SwitchStatus
            Dim ss As SwitchStatus = DebtPlus.Utils.ArgParserBase.SwitchStatus.NoError

            ' Add command-line argument to array of pathnames.
            ' Convert switchValue to set of pathnames, add each pathname to the pathnames ArrayList.
            Try
                Dim d As String = Path.GetDirectoryName(switchValue)
                Dim dir As DirectoryInfo
                If (d.Length = 0) Then
                    dir = New DirectoryInfo(".")
                Else
                    dir = New DirectoryInfo(d)
                End If

                For Each f As FileInfo In dir.GetFiles(Path.GetFileName(switchValue))
                    pathnames.Add(f.FullName)
                Next f

            Catch ex As System.Security.SecurityException
                Throw ex

            Catch ex As Exception
                ss = DebtPlus.Utils.ArgParserBase.SwitchStatus.YesError
            End Try

            If pathnames.Count = 0 Then
                AppendErrorLine("None of the specified files exist.")
                ss = DebtPlus.Utils.ArgParserBase.SwitchStatus.YesError
            End If

            Return ss
        End Function

        ''' <summary>
        ''' Process the end of the parse operation.
        ''' </summary>
        Protected Overrides Function OnDoneParse() As SwitchStatus
            Dim ss As SwitchStatus = DebtPlus.Utils.ArgParserBase.SwitchStatus.NoError

            ' Read the source file(s) if they are not given on the command line.
            If pathnames.Count = 0 Then
                With New System.Windows.Forms.OpenFileDialog
                    .AddExtension = True
                    .AutoUpgradeEnabled = True
                    .CheckFileExists = True
                    .CheckPathExists = True
                    .DefaultExt = ".rtf"
                    .Filter = "RTF Files (*.rtf)|*.rtf|Text Files (*.txt)|*.txt"
                    .FilterIndex = 0
                    .InitialDirectory = System.Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments)
                    .Multiselect = True
                    .ReadOnlyChecked = True
                    .RestoreDirectory = True
                    .ShowHelp = False
                    .ShowReadOnly = False
                    .Title = "ACH Response input file"
                    .ValidateNames = True

                    If .ShowDialog() <> DialogResult.OK Then ss = DebtPlus.Utils.ArgParserBase.SwitchStatus.YesError
                    If ss = DebtPlus.Utils.ArgParserBase.SwitchStatus.NoError Then
                        For Each fname As String In .FileNames()
                            pathnames.Add(fname)
                        Next
                    End If
                End With
            End If

            ' Sort the filenames.
            If pathnames.Count > 0 Then
                pathnames.Sort(0, pathnames.Count, Nothing)
            End If

            Return ss
        End Function

        ''' <summary>
        ''' Generate the command usage information
        ''' </summary>
        Protected Overrides Sub OnUsage(ByVal errorInfo As String)
            ' deprecated
        End Sub
    End Class

End Namespace

