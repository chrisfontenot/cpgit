#Region "Copyright 2000-2012 DebtPlus, L.L.C."
'{*******************************************************************}
'{                                                                   }
'{       DebtPlus Debt Management System                             }
'{                                                                   }
'{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
'{       ALL RIGHTS RESERVED                                         }
'{                                                                   }
'{   The entire contents of this file is protected by U.S. and       }
'{   International Copyright Laws. Unauthorized reproduction,        }
'{   reverse-engineering, and distribution of all or any portion of  }
'{   the code contained in this file is strictly prohibited and may  }
'{   result in severe civil and criminal penalties and will be       }
'{   prosecuted to the maximum extent possible under the law.        }
'{                                                                   }
'{   RESTRICTIONS                                                    }
'{                                                                   }
'{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
'{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
'{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
'{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
'{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
'{                                                                   }
'{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
'{   ADDITIONAL RESTRICTIONS.                                        }
'{                                                                   }
'{*******************************************************************}

#End Region

Option Compare Binary
Option Explicit On
Option Strict On

Imports System.Threading
Imports DebtPlus.Interfaces.Desktop

Namespace ACH.Response.File.ClearPoint

    Public Class Mainline
        Implements IDesktopMainline

        Public Sub OldAppMain(ByVal args() As String) Implements IDesktopMainline.OldAppMain
            Dim ap As New ArgParser()
            If ap.Parse(args) Then
                For itemCount As Int32 = 0 To ap.pathnames.Count - 1
                    Dim tc As New ThreadClass()
                    tc.ap = ap
                    tc.itemCount = itemCount
                    With New Thread(AddressOf tc.SecondaryFileThread)
                        .Name = "SECONDARY_FILE_" + Guid.NewGuid().ToString()
                        .SetApartmentState(ApartmentState.STA)
                        .Start(Nothing)
                    End With
                Next
            End If
        End Sub

        ''' <summary>
        ''' Internal class to represent the arguments to the thread procedure
        ''' </summary>
        ''' <remarks></remarks>
        Friend Class ThreadClass
            Public ap As ArgParser
            Public itemCount As Int32

            Public Sub SecondaryFileThread(ByVal obj As Object)

                ' Find the filename to process and start the form processing
                Dim filename As String = ap.pathnames(itemCount)
                Using mainForm As Form1 = New Form1(ap, filename)
                    mainForm.ShowDialog()
                End Using
            End Sub
        End Class
    End Class
End Namespace

