﻿Namespace ACH.Response.File.ClearPoint
    <Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
    Partial Class Form1
        Inherits DebtPlus.Data.Forms.DebtPlusForm

        'Form overrides dispose to clean up the component list.
        <System.Diagnostics.DebuggerNonUserCode()> _
        Protected Overrides Sub Dispose(ByVal disposing As Boolean)
            Try
                If disposing AndAlso components IsNot Nothing Then
                    components.Dispose()
                End If
            Finally
                MyBase.Dispose(disposing)
            End Try
        End Sub

        'Required by the Windows Form Designer
        Private components As System.ComponentModel.IContainer

        'NOTE: The following procedure is required by the Windows Form Designer
        'It can be modified using the Windows Form Designer.  
        'Do not modify it using the code editor.
        <System.Diagnostics.DebuggerStepThrough()> _
        Private Sub InitializeComponent()
            Me.components = New System.ComponentModel.Container
            Me.RichTextBox1 = New System.Windows.Forms.RichTextBox
            Me.barManager1 = New DevExpress.XtraBars.BarManager(Me.components)
            Me.Bar2 = New DevExpress.XtraBars.Bar
            Me.BarButtonItem_File_Exit = New DevExpress.XtraBars.BarButtonItem
            Me.BarButtonItem_File_Generate = New DevExpress.XtraBars.BarButtonItem
            Me.barDockControlTop = New DevExpress.XtraBars.BarDockControl
            Me.barDockControlBottom = New DevExpress.XtraBars.BarDockControl
            Me.barDockControlLeft = New DevExpress.XtraBars.BarDockControl
            Me.barDockControlRight = New DevExpress.XtraBars.BarDockControl
            CType(Me.DxErrorProvider1, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.barManager1, System.ComponentModel.ISupportInitialize).BeginInit()
            Me.SuspendLayout()
            '
            'RichTextBox1
            '
            Me.RichTextBox1.AutoWordSelection = True
            Me.RichTextBox1.DetectUrls = False
            Me.RichTextBox1.Dock = System.Windows.Forms.DockStyle.Fill
            Me.RichTextBox1.Location = New System.Drawing.Point(0, 24)
            Me.RichTextBox1.Name = "RichTextBox1"
            Me.RichTextBox1.ScrollBars = System.Windows.Forms.RichTextBoxScrollBars.ForcedVertical
            Me.RichTextBox1.Size = New System.Drawing.Size(529, 242)
            Me.ToolTipController1.SetSuperTip(Me.RichTextBox1, Nothing)
            Me.RichTextBox1.TabIndex = 0
            Me.RichTextBox1.Text = ""
            Me.RichTextBox1.WordWrap = False
            '
            'barManager1
            '
            Me.barManager1.Bars.AddRange(New DevExpress.XtraBars.Bar() {Me.Bar2})
            Me.barManager1.DockControls.Add(Me.barDockControlTop)
            Me.barManager1.DockControls.Add(Me.barDockControlBottom)
            Me.barManager1.DockControls.Add(Me.barDockControlLeft)
            Me.barManager1.DockControls.Add(Me.barDockControlRight)
            Me.barManager1.Form = Me
            Me.barManager1.Items.AddRange(New DevExpress.XtraBars.BarItem() {Me.BarButtonItem_File_Exit, Me.BarButtonItem_File_Generate})
            Me.barManager1.MainMenu = Me.Bar2
            Me.barManager1.MaxItemId = 2
            '
            'Bar2
            '
            Me.Bar2.BarName = "Main menu"
            Me.Bar2.DockCol = 0
            Me.Bar2.DockRow = 0
            Me.Bar2.DockStyle = DevExpress.XtraBars.BarDockStyle.Top
            Me.Bar2.LinksPersistInfo.AddRange(New DevExpress.XtraBars.LinkPersistInfo() {New DevExpress.XtraBars.LinkPersistInfo(Me.BarButtonItem_File_Exit), New DevExpress.XtraBars.LinkPersistInfo(Me.BarButtonItem_File_Generate)})
            Me.Bar2.OptionsBar.MultiLine = True
            Me.Bar2.OptionsBar.UseWholeRow = True
            Me.Bar2.Text = "Main menu"
            '
            'BarButtonItem_File_Exit
            '
            Me.BarButtonItem_File_Exit.Caption = "&Exit"
            Me.BarButtonItem_File_Exit.Id = 0
            Me.BarButtonItem_File_Exit.Name = "BarButtonItem_File_Exit"
            '
            'BarButtonItem_File_Generate
            '
            Me.BarButtonItem_File_Generate.Caption = "&Generate"
            Me.BarButtonItem_File_Generate.Id = 1
            Me.BarButtonItem_File_Generate.Name = "BarButtonItem_File_Generate"
            '
            'barDockControlTop
            '
            Me.ToolTipController1.SetSuperTip(Me.barDockControlTop, Nothing)
            '
            'barDockControlBottom
            '
            Me.ToolTipController1.SetSuperTip(Me.barDockControlBottom, Nothing)
            '
            'barDockControlLeft
            '
            Me.ToolTipController1.SetSuperTip(Me.barDockControlLeft, Nothing)
            '
            'barDockControlRight
            '
            Me.ToolTipController1.SetSuperTip(Me.barDockControlRight, Nothing)
            '
            'Form1
            '
            Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
            Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
            Me.ClientSize = New System.Drawing.Size(529, 266)
            Me.Controls.Add(Me.RichTextBox1)
            Me.Controls.Add(Me.barDockControlLeft)
            Me.Controls.Add(Me.barDockControlRight)
            Me.Controls.Add(Me.barDockControlBottom)
            Me.Controls.Add(Me.barDockControlTop)
            Me.Name = "Form1"
            Me.ToolTipController1.SetSuperTip(Me, Nothing)
            Me.Text = "Process ACH Rejects"
            CType(Me.DxErrorProvider1, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.barManager1, System.ComponentModel.ISupportInitialize).EndInit()
            Me.ResumeLayout(False)

        End Sub
        Friend WithEvents RichTextBox1 As System.Windows.Forms.RichTextBox
        Friend WithEvents barManager1 As DevExpress.XtraBars.BarManager
        Friend WithEvents Bar2 As DevExpress.XtraBars.Bar
        Friend WithEvents barDockControlTop As DevExpress.XtraBars.BarDockControl
        Friend WithEvents barDockControlBottom As DevExpress.XtraBars.BarDockControl
        Friend WithEvents barDockControlLeft As DevExpress.XtraBars.BarDockControl
        Friend WithEvents barDockControlRight As DevExpress.XtraBars.BarDockControl
        Friend WithEvents BarButtonItem_File_Exit As DevExpress.XtraBars.BarButtonItem
        Friend WithEvents BarButtonItem_File_Generate As DevExpress.XtraBars.BarButtonItem
    End Class
End Namespace