Namespace ACH.Response.Manual
    <Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
    Partial Class MainForm
        Inherits DebtPlus.Data.Forms.DebtPlusForm

        'Form overrides dispose to clean up the component list.
        <System.Diagnostics.DebuggerNonUserCode()> _
        Protected Overrides Sub Dispose(ByVal disposing As Boolean)
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
            MyBase.Dispose(disposing)
        End Sub

        'Required by the Windows Form Designer
        Private components As System.ComponentModel.IContainer

        'NOTE: The following procedure is required by the Windows Form Designer
        'It can be modified using the Windows Form Designer.  
        'Do not modify it using the code editor.
        <System.Diagnostics.DebuggerStepThrough()> _
        Private Sub InitializeComponent()
            Me.components = New System.ComponentModel.Container
            Me.LayoutControl1 = New DevExpress.XtraLayout.LayoutControl
            Me.GridControl1 = New DevExpress.XtraGrid.GridControl
            Me.GridView1 = New DevExpress.XtraGrid.Views.Grid.GridView
            Me.GridColumn_item_key = New DevExpress.XtraGrid.Columns.GridColumn
            Me.GridColumn_trace_number = New DevExpress.XtraGrid.Columns.GridColumn
            Me.GridColumn_client = New DevExpress.XtraGrid.Columns.GridColumn
            Me.GridColumn_amount = New DevExpress.XtraGrid.Columns.GridColumn
            Me.GridColumn_transaction_code = New DevExpress.XtraGrid.Columns.GridColumn
            Me.GridColumn_authenticaiton_code = New DevExpress.XtraGrid.Columns.GridColumn
            Me.RepositoryItemLookUpEdit_authentication_code = New DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit
            Me.barManager1 = New DevExpress.XtraBars.BarManager(Me.components)
            Me.Bar1 = New DevExpress.XtraBars.Bar
            Me.BarSubItem1 = New DevExpress.XtraBars.BarSubItem
            Me.BarButtonItem_File_Open = New DevExpress.XtraBars.BarButtonItem
            Me.BarButtonItem_File_DataEntry = New DevExpress.XtraBars.BarButtonItem
            Me.BarButtonItem_File_Exit = New DevExpress.XtraBars.BarButtonItem
            Me.BarSubItem2 = New DevExpress.XtraBars.BarSubItem
            Me.BarButtonItem_Reports_Transactions = New DevExpress.XtraBars.BarButtonItem
            Me.BarButtonItem_Reports_ErrorListing = New DevExpress.XtraBars.BarButtonItem
            Me.BarSubItem3 = New DevExpress.XtraBars.BarSubItem
            Me.BarButtonItem_Help_About = New DevExpress.XtraBars.BarButtonItem
            Me.Bar3 = New DevExpress.XtraBars.Bar
            Me.barDockControlTop = New DevExpress.XtraBars.BarDockControl
            Me.barDockControlBottom = New DevExpress.XtraBars.BarDockControl
            Me.barDockControlLeft = New DevExpress.XtraBars.BarDockControl
            Me.barDockControlRight = New DevExpress.XtraBars.BarDockControl
            Me.GridView2 = New DevExpress.XtraGrid.Views.Grid.GridView
            Me.LayoutControlGroup1 = New DevExpress.XtraLayout.LayoutControlGroup
            Me.LayoutControlItem1 = New DevExpress.XtraLayout.LayoutControlItem
            CType(Me.DxErrorProvider1, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.LayoutControl1, System.ComponentModel.ISupportInitialize).BeginInit()
            Me.LayoutControl1.SuspendLayout()
            CType(Me.GridControl1, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.GridView1, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.RepositoryItemLookUpEdit_authentication_code, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.barManager1, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.GridView2, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.LayoutControlGroup1, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.LayoutControlItem1, System.ComponentModel.ISupportInitialize).BeginInit()
            Me.SuspendLayout()
            '
            'LayoutControl1
            '
            Me.LayoutControl1.Controls.Add(Me.GridControl1)
            Me.LayoutControl1.Dock = System.Windows.Forms.DockStyle.Fill
            Me.LayoutControl1.Location = New System.Drawing.Point(0, 29)
            Me.LayoutControl1.Name = "LayoutControl1"
            Me.LayoutControl1.OptionsCustomizationForm.DesignTimeCustomizationFormPositionAndSize = New System.Drawing.Rectangle(573, 86, 250, 350)
            Me.LayoutControl1.Root = Me.LayoutControlGroup1
            Me.LayoutControl1.Size = New System.Drawing.Size(605, 237)
            Me.LayoutControl1.TabIndex = 0
            Me.LayoutControl1.Text = "LayoutControl1"
            '
            'GridControl1
            '
            Me.GridControl1.Location = New System.Drawing.Point(12, 12)
            Me.GridControl1.MainView = Me.GridView1
            Me.GridControl1.MenuManager = Me.barManager1
            Me.GridControl1.Name = "GridControl1"
            Me.GridControl1.RepositoryItems.AddRange(New DevExpress.XtraEditors.Repository.RepositoryItem() {Me.RepositoryItemLookUpEdit_authentication_code})
            Me.GridControl1.Size = New System.Drawing.Size(581, 213)
            Me.GridControl1.TabIndex = 4
            Me.GridControl1.ViewCollection.AddRange(New DevExpress.XtraGrid.Views.Base.BaseView() {Me.GridView1, Me.GridView2})
            '
            'GridView1
            '
            Me.GridView1.Appearance.ColumnFilterButton.BackColor = System.Drawing.Color.FromArgb(CType(CType(221, Byte), Int32), CType(CType(236, Byte), Int32), CType(CType(254, Byte), Int32))
            Me.GridView1.Appearance.ColumnFilterButton.BackColor2 = System.Drawing.Color.FromArgb(CType(CType(132, Byte), Int32), CType(CType(171, Byte), Int32), CType(CType(228, Byte), Int32))
            Me.GridView1.Appearance.ColumnFilterButton.BorderColor = System.Drawing.Color.FromArgb(CType(CType(221, Byte), Int32), CType(CType(236, Byte), Int32), CType(CType(254, Byte), Int32))
            Me.GridView1.Appearance.ColumnFilterButton.ForeColor = System.Drawing.Color.Black
            Me.GridView1.Appearance.ColumnFilterButton.GradientMode = System.Drawing.Drawing2D.LinearGradientMode.Vertical
            Me.GridView1.Appearance.ColumnFilterButton.Options.UseBackColor = True
            Me.GridView1.Appearance.ColumnFilterButton.Options.UseBorderColor = True
            Me.GridView1.Appearance.ColumnFilterButton.Options.UseForeColor = True
            Me.GridView1.Appearance.ColumnFilterButtonActive.BackColor = System.Drawing.Color.FromArgb(CType(CType(247, Byte), Int32), CType(CType(251, Byte), Int32), CType(CType(255, Byte), Int32))
            Me.GridView1.Appearance.ColumnFilterButtonActive.BackColor2 = System.Drawing.Color.FromArgb(CType(CType(154, Byte), Int32), CType(CType(190, Byte), Int32), CType(CType(243, Byte), Int32))
            Me.GridView1.Appearance.ColumnFilterButtonActive.BorderColor = System.Drawing.Color.FromArgb(CType(CType(247, Byte), Int32), CType(CType(251, Byte), Int32), CType(CType(255, Byte), Int32))
            Me.GridView1.Appearance.ColumnFilterButtonActive.ForeColor = System.Drawing.Color.Black
            Me.GridView1.Appearance.ColumnFilterButtonActive.GradientMode = System.Drawing.Drawing2D.LinearGradientMode.Vertical
            Me.GridView1.Appearance.ColumnFilterButtonActive.Options.UseBackColor = True
            Me.GridView1.Appearance.ColumnFilterButtonActive.Options.UseBorderColor = True
            Me.GridView1.Appearance.ColumnFilterButtonActive.Options.UseForeColor = True
            Me.GridView1.Appearance.Empty.BackColor = System.Drawing.Color.White
            Me.GridView1.Appearance.Empty.Options.UseBackColor = True
            Me.GridView1.Appearance.EvenRow.BackColor = System.Drawing.Color.FromArgb(CType(CType(231, Byte), Int32), CType(CType(242, Byte), Int32), CType(CType(254, Byte), Int32))
            Me.GridView1.Appearance.EvenRow.ForeColor = System.Drawing.Color.Black
            Me.GridView1.Appearance.EvenRow.Options.UseBackColor = True
            Me.GridView1.Appearance.EvenRow.Options.UseForeColor = True
            Me.GridView1.Appearance.FilterCloseButton.BackColor = System.Drawing.Color.FromArgb(CType(CType(221, Byte), Int32), CType(CType(236, Byte), Int32), CType(CType(254, Byte), Int32))
            Me.GridView1.Appearance.FilterCloseButton.BackColor2 = System.Drawing.Color.FromArgb(CType(CType(132, Byte), Int32), CType(CType(171, Byte), Int32), CType(CType(228, Byte), Int32))
            Me.GridView1.Appearance.FilterCloseButton.BorderColor = System.Drawing.Color.FromArgb(CType(CType(221, Byte), Int32), CType(CType(236, Byte), Int32), CType(CType(254, Byte), Int32))
            Me.GridView1.Appearance.FilterCloseButton.ForeColor = System.Drawing.Color.Black
            Me.GridView1.Appearance.FilterCloseButton.GradientMode = System.Drawing.Drawing2D.LinearGradientMode.Vertical
            Me.GridView1.Appearance.FilterCloseButton.Options.UseBackColor = True
            Me.GridView1.Appearance.FilterCloseButton.Options.UseBorderColor = True
            Me.GridView1.Appearance.FilterCloseButton.Options.UseForeColor = True
            Me.GridView1.Appearance.FilterPanel.BackColor = System.Drawing.Color.FromArgb(CType(CType(62, Byte), Int32), CType(CType(109, Byte), Int32), CType(CType(185, Byte), Int32))
            Me.GridView1.Appearance.FilterPanel.ForeColor = System.Drawing.Color.White
            Me.GridView1.Appearance.FilterPanel.Options.UseBackColor = True
            Me.GridView1.Appearance.FilterPanel.Options.UseForeColor = True
            Me.GridView1.Appearance.FixedLine.BackColor = System.Drawing.Color.FromArgb(CType(CType(59, Byte), Int32), CType(CType(97, Byte), Int32), CType(CType(156, Byte), Int32))
            Me.GridView1.Appearance.FixedLine.Options.UseBackColor = True
            Me.GridView1.Appearance.FocusedCell.BackColor = System.Drawing.Color.White
            Me.GridView1.Appearance.FocusedCell.ForeColor = System.Drawing.Color.Black
            Me.GridView1.Appearance.FocusedCell.Options.UseBackColor = True
            Me.GridView1.Appearance.FocusedCell.Options.UseForeColor = True
            Me.GridView1.Appearance.FocusedRow.BackColor = System.Drawing.Color.FromArgb(CType(CType(49, Byte), Int32), CType(CType(106, Byte), Int32), CType(CType(197, Byte), Int32))
            Me.GridView1.Appearance.FocusedRow.ForeColor = System.Drawing.Color.White
            Me.GridView1.Appearance.FocusedRow.Options.UseBackColor = True
            Me.GridView1.Appearance.FocusedRow.Options.UseForeColor = True
            Me.GridView1.Appearance.FooterPanel.BackColor = System.Drawing.Color.FromArgb(CType(CType(221, Byte), Int32), CType(CType(236, Byte), Int32), CType(CType(254, Byte), Int32))
            Me.GridView1.Appearance.FooterPanel.BackColor2 = System.Drawing.Color.FromArgb(CType(CType(132, Byte), Int32), CType(CType(171, Byte), Int32), CType(CType(228, Byte), Int32))
            Me.GridView1.Appearance.FooterPanel.BorderColor = System.Drawing.Color.FromArgb(CType(CType(221, Byte), Int32), CType(CType(236, Byte), Int32), CType(CType(254, Byte), Int32))
            Me.GridView1.Appearance.FooterPanel.ForeColor = System.Drawing.Color.Black
            Me.GridView1.Appearance.FooterPanel.GradientMode = System.Drawing.Drawing2D.LinearGradientMode.Vertical
            Me.GridView1.Appearance.FooterPanel.Options.UseBackColor = True
            Me.GridView1.Appearance.FooterPanel.Options.UseBorderColor = True
            Me.GridView1.Appearance.FooterPanel.Options.UseForeColor = True
            Me.GridView1.Appearance.GroupButton.BackColor = System.Drawing.Color.FromArgb(CType(CType(193, Byte), Int32), CType(CType(216, Byte), Int32), CType(CType(247, Byte), Int32))
            Me.GridView1.Appearance.GroupButton.BorderColor = System.Drawing.Color.FromArgb(CType(CType(193, Byte), Int32), CType(CType(216, Byte), Int32), CType(CType(247, Byte), Int32))
            Me.GridView1.Appearance.GroupButton.ForeColor = System.Drawing.Color.Black
            Me.GridView1.Appearance.GroupButton.Options.UseBackColor = True
            Me.GridView1.Appearance.GroupButton.Options.UseBorderColor = True
            Me.GridView1.Appearance.GroupButton.Options.UseForeColor = True
            Me.GridView1.Appearance.GroupFooter.BackColor = System.Drawing.Color.FromArgb(CType(CType(193, Byte), Int32), CType(CType(216, Byte), Int32), CType(CType(247, Byte), Int32))
            Me.GridView1.Appearance.GroupFooter.BorderColor = System.Drawing.Color.FromArgb(CType(CType(193, Byte), Int32), CType(CType(216, Byte), Int32), CType(CType(247, Byte), Int32))
            Me.GridView1.Appearance.GroupFooter.ForeColor = System.Drawing.Color.Black
            Me.GridView1.Appearance.GroupFooter.Options.UseBackColor = True
            Me.GridView1.Appearance.GroupFooter.Options.UseBorderColor = True
            Me.GridView1.Appearance.GroupFooter.Options.UseForeColor = True
            Me.GridView1.Appearance.GroupPanel.BackColor = System.Drawing.Color.FromArgb(CType(CType(62, Byte), Int32), CType(CType(109, Byte), Int32), CType(CType(185, Byte), Int32))
            Me.GridView1.Appearance.GroupPanel.ForeColor = System.Drawing.Color.FromArgb(CType(CType(221, Byte), Int32), CType(CType(236, Byte), Int32), CType(CType(254, Byte), Int32))
            Me.GridView1.Appearance.GroupPanel.Options.UseBackColor = True
            Me.GridView1.Appearance.GroupPanel.Options.UseForeColor = True
            Me.GridView1.Appearance.GroupRow.BackColor = System.Drawing.Color.FromArgb(CType(CType(193, Byte), Int32), CType(CType(216, Byte), Int32), CType(CType(247, Byte), Int32))
            Me.GridView1.Appearance.GroupRow.BorderColor = System.Drawing.Color.FromArgb(CType(CType(193, Byte), Int32), CType(CType(216, Byte), Int32), CType(CType(247, Byte), Int32))
            Me.GridView1.Appearance.GroupRow.Font = New System.Drawing.Font("Tahoma", 8.0!, System.Drawing.FontStyle.Bold)
            Me.GridView1.Appearance.GroupRow.ForeColor = System.Drawing.Color.Black
            Me.GridView1.Appearance.GroupRow.Options.UseBackColor = True
            Me.GridView1.Appearance.GroupRow.Options.UseBorderColor = True
            Me.GridView1.Appearance.GroupRow.Options.UseFont = True
            Me.GridView1.Appearance.GroupRow.Options.UseForeColor = True
            Me.GridView1.Appearance.HeaderPanel.BackColor = System.Drawing.Color.FromArgb(CType(CType(221, Byte), Int32), CType(CType(236, Byte), Int32), CType(CType(254, Byte), Int32))
            Me.GridView1.Appearance.HeaderPanel.BackColor2 = System.Drawing.Color.FromArgb(CType(CType(132, Byte), Int32), CType(CType(171, Byte), Int32), CType(CType(228, Byte), Int32))
            Me.GridView1.Appearance.HeaderPanel.BorderColor = System.Drawing.Color.FromArgb(CType(CType(221, Byte), Int32), CType(CType(236, Byte), Int32), CType(CType(254, Byte), Int32))
            Me.GridView1.Appearance.HeaderPanel.ForeColor = System.Drawing.Color.Black
            Me.GridView1.Appearance.HeaderPanel.GradientMode = System.Drawing.Drawing2D.LinearGradientMode.Vertical
            Me.GridView1.Appearance.HeaderPanel.Options.UseBackColor = True
            Me.GridView1.Appearance.HeaderPanel.Options.UseBorderColor = True
            Me.GridView1.Appearance.HeaderPanel.Options.UseForeColor = True
            Me.GridView1.Appearance.HideSelectionRow.BackColor = System.Drawing.Color.FromArgb(CType(CType(106, Byte), Int32), CType(CType(153, Byte), Int32), CType(CType(228, Byte), Int32))
            Me.GridView1.Appearance.HideSelectionRow.ForeColor = System.Drawing.Color.FromArgb(CType(CType(208, Byte), Int32), CType(CType(224, Byte), Int32), CType(CType(251, Byte), Int32))
            Me.GridView1.Appearance.HideSelectionRow.Options.UseBackColor = True
            Me.GridView1.Appearance.HideSelectionRow.Options.UseForeColor = True
            Me.GridView1.Appearance.HorzLine.BackColor = System.Drawing.Color.FromArgb(CType(CType(99, Byte), Int32), CType(CType(127, Byte), Int32), CType(CType(196, Byte), Int32))
            Me.GridView1.Appearance.HorzLine.Options.UseBackColor = True
            Me.GridView1.Appearance.OddRow.BackColor = System.Drawing.Color.White
            Me.GridView1.Appearance.OddRow.ForeColor = System.Drawing.Color.Black
            Me.GridView1.Appearance.OddRow.Options.UseBackColor = True
            Me.GridView1.Appearance.OddRow.Options.UseForeColor = True
            Me.GridView1.Appearance.Preview.BackColor = System.Drawing.Color.FromArgb(CType(CType(249, Byte), Int32), CType(CType(252, Byte), Int32), CType(CType(255, Byte), Int32))
            Me.GridView1.Appearance.Preview.ForeColor = System.Drawing.Color.FromArgb(CType(CType(88, Byte), Int32), CType(CType(129, Byte), Int32), CType(CType(185, Byte), Int32))
            Me.GridView1.Appearance.Preview.Options.UseBackColor = True
            Me.GridView1.Appearance.Preview.Options.UseForeColor = True
            Me.GridView1.Appearance.Row.BackColor = System.Drawing.Color.White
            Me.GridView1.Appearance.Row.ForeColor = System.Drawing.Color.Black
            Me.GridView1.Appearance.Row.Options.UseBackColor = True
            Me.GridView1.Appearance.Row.Options.UseForeColor = True
            Me.GridView1.Appearance.RowSeparator.BackColor = System.Drawing.Color.White
            Me.GridView1.Appearance.RowSeparator.Options.UseBackColor = True
            Me.GridView1.Appearance.SelectedRow.BackColor = System.Drawing.Color.FromArgb(CType(CType(69, Byte), Int32), CType(CType(126, Byte), Int32), CType(CType(217, Byte), Int32))
            Me.GridView1.Appearance.SelectedRow.ForeColor = System.Drawing.Color.White
            Me.GridView1.Appearance.SelectedRow.Options.UseBackColor = True
            Me.GridView1.Appearance.SelectedRow.Options.UseForeColor = True
            Me.GridView1.Appearance.VertLine.BackColor = System.Drawing.Color.FromArgb(CType(CType(99, Byte), Int32), CType(CType(127, Byte), Int32), CType(CType(196, Byte), Int32))
            Me.GridView1.Appearance.VertLine.Options.UseBackColor = True
            Me.GridView1.Columns.AddRange(New DevExpress.XtraGrid.Columns.GridColumn() {Me.GridColumn_item_key, Me.GridColumn_trace_number, Me.GridColumn_client, Me.GridColumn_amount, Me.GridColumn_transaction_code, Me.GridColumn_authenticaiton_code})
            Me.GridView1.GridControl = Me.GridControl1
            Me.GridView1.Name = "GridView1"
            Me.GridView1.OptionsView.EnableAppearanceEvenRow = True
            Me.GridView1.OptionsView.EnableAppearanceOddRow = True
            Me.GridView1.OptionsView.ShowGroupPanel = False
            '
            'GridColumn_item_key
            '
            Me.GridColumn_item_key.AppearanceCell.Options.UseTextOptions = True
            Me.GridColumn_item_key.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
            Me.GridColumn_item_key.AppearanceHeader.Options.UseTextOptions = True
            Me.GridColumn_item_key.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
            Me.GridColumn_item_key.Caption = "ID"
            Me.GridColumn_item_key.CustomizationCaption = "Record ID"
            Me.GridColumn_item_key.DisplayFormat.FormatString = "f0"
            Me.GridColumn_item_key.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
            Me.GridColumn_item_key.FieldName = "item_key"
            Me.GridColumn_item_key.GroupFormat.FormatString = "f0"
            Me.GridColumn_item_key.GroupFormat.FormatType = DevExpress.Utils.FormatType.Numeric
            Me.GridColumn_item_key.Name = "GridColumn_item_key"
            Me.GridColumn_item_key.OptionsColumn.AllowEdit = False
            '
            'GridColumn_trace_number
            '
            Me.GridColumn_trace_number.Caption = "Trace Number"
            Me.GridColumn_trace_number.CustomizationCaption = "Trace Number"
            Me.GridColumn_trace_number.FieldName = "trace_number"
            Me.GridColumn_trace_number.Name = "GridColumn_trace_number"
            Me.GridColumn_trace_number.OptionsColumn.AllowEdit = False
            Me.GridColumn_trace_number.Visible = True
            Me.GridColumn_trace_number.VisibleIndex = 0
            Me.GridColumn_trace_number.Width = 126
            '
            'GridColumn_client
            '
            Me.GridColumn_client.AppearanceCell.Options.UseTextOptions = True
            Me.GridColumn_client.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
            Me.GridColumn_client.AppearanceHeader.Options.UseTextOptions = True
            Me.GridColumn_client.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
            Me.GridColumn_client.Caption = "Client"
            Me.GridColumn_client.CustomizationCaption = "Client"
            Me.GridColumn_client.DisplayFormat.FormatString = "f0"
            Me.GridColumn_client.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Custom
            Me.GridColumn_client.FieldName = "client"
            Me.GridColumn_client.GroupFormat.FormatString = "f0"
            Me.GridColumn_client.GroupFormat.FormatType = DevExpress.Utils.FormatType.Custom
            Me.GridColumn_client.Name = "GridColumn_client"
            Me.GridColumn_client.OptionsColumn.AllowEdit = False
            Me.GridColumn_client.Visible = True
            Me.GridColumn_client.VisibleIndex = 1
            Me.GridColumn_client.Width = 76
            '
            'GridColumn_amount
            '
            Me.GridColumn_amount.Caption = "Amount"
            Me.GridColumn_amount.CustomizationCaption = "Amount"
            Me.GridColumn_amount.DisplayFormat.FormatString = "{0:c}"
            Me.GridColumn_amount.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
            Me.GridColumn_amount.FieldName = "amount"
            Me.GridColumn_amount.GroupFormat.FormatString = "{0:c}"
            Me.GridColumn_amount.GroupFormat.FormatType = DevExpress.Utils.FormatType.Numeric
            Me.GridColumn_amount.Name = "GridColumn_amount"
            Me.GridColumn_amount.OptionsColumn.AllowEdit = False
            Me.GridColumn_amount.Visible = True
            Me.GridColumn_amount.VisibleIndex = 2
            Me.GridColumn_amount.Width = 79
            '
            'GridColumn_transaction_code
            '
            Me.GridColumn_transaction_code.Caption = "Transaction"
            Me.GridColumn_transaction_code.CustomizationCaption = "Transaction Code"
            Me.GridColumn_transaction_code.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Custom
            Me.GridColumn_transaction_code.FieldName = "transaction_code"
            Me.GridColumn_transaction_code.GroupFormat.FormatType = DevExpress.Utils.FormatType.Custom
            Me.GridColumn_transaction_code.Name = "GridColumn_transaction_code"
            Me.GridColumn_transaction_code.OptionsColumn.AllowEdit = False
            Me.GridColumn_transaction_code.SortMode = DevExpress.XtraGrid.ColumnSortMode.DisplayText
            Me.GridColumn_transaction_code.Visible = True
            Me.GridColumn_transaction_code.VisibleIndex = 3
            Me.GridColumn_transaction_code.Width = 108
            '
            'GridColumn_authenticaiton_code
            '
            Me.GridColumn_authenticaiton_code.Caption = "Authenication"
            Me.GridColumn_authenticaiton_code.ColumnEdit = Me.RepositoryItemLookUpEdit_authentication_code
            Me.GridColumn_authenticaiton_code.CustomizationCaption = "Authentication Code"
            Me.GridColumn_authenticaiton_code.FieldName = "authentication_code"
            Me.GridColumn_authenticaiton_code.Name = "GridColumn_authenticaiton_code"
            Me.GridColumn_authenticaiton_code.Visible = True
            Me.GridColumn_authenticaiton_code.VisibleIndex = 4
            Me.GridColumn_authenticaiton_code.Width = 171
            '
            'RepositoryItemLookUpEdit_authentication_code
            '
            Me.RepositoryItemLookUpEdit_authentication_code.AllowNullInput = DevExpress.Utils.DefaultBoolean.[True]
            Me.RepositoryItemLookUpEdit_authentication_code.AutoHeight = False
            Me.RepositoryItemLookUpEdit_authentication_code.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
            Me.RepositoryItemLookUpEdit_authentication_code.Columns.AddRange(New DevExpress.XtraEditors.Controls.LookUpColumnInfo() {New DevExpress.XtraEditors.Controls.LookUpColumnInfo("ach_reject_code", "ID", 5, DevExpress.Utils.FormatType.Numeric, "f0", True, DevExpress.Utils.HorzAlignment.[Default]), New DevExpress.XtraEditors.Controls.LookUpColumnInfo("description", 30, "Description")})
            Me.RepositoryItemLookUpEdit_authentication_code.DisplayMember = "description"
            Me.RepositoryItemLookUpEdit_authentication_code.Name = "RepositoryItemLookUpEdit_authentication_code"
            Me.RepositoryItemLookUpEdit_authentication_code.NullText = ""
            Me.RepositoryItemLookUpEdit_authentication_code.ShowFooter = False
            Me.RepositoryItemLookUpEdit_authentication_code.ValueMember = "ach_reject_code"
            '
            'barManager1
            '
            Me.barManager1.Bars.AddRange(New DevExpress.XtraBars.Bar() {Me.Bar1, Me.Bar3})
            Me.barManager1.DockControls.Add(Me.barDockControlTop)
            Me.barManager1.DockControls.Add(Me.barDockControlBottom)
            Me.barManager1.DockControls.Add(Me.barDockControlLeft)
            Me.barManager1.DockControls.Add(Me.barDockControlRight)
            Me.barManager1.Form = Me
            Me.barManager1.Items.AddRange(New DevExpress.XtraBars.BarItem() {Me.BarSubItem1, Me.BarSubItem2, Me.BarSubItem3, Me.BarButtonItem_File_Open, Me.BarButtonItem_File_DataEntry, Me.BarButtonItem_Reports_Transactions, Me.BarButtonItem_Reports_ErrorListing, Me.BarButtonItem_Help_About, Me.BarButtonItem_File_Exit})
            Me.barManager1.MaxItemId = 9
            Me.barManager1.StatusBar = Me.Bar3
            '
            'Bar1
            '
            Me.Bar1.BarName = "Tools"
            Me.Bar1.DockCol = 0
            Me.Bar1.DockRow = 0
            Me.Bar1.DockStyle = DevExpress.XtraBars.BarDockStyle.Top
            Me.Bar1.LinksPersistInfo.AddRange(New DevExpress.XtraBars.LinkPersistInfo() {New DevExpress.XtraBars.LinkPersistInfo(Me.BarSubItem1), New DevExpress.XtraBars.LinkPersistInfo(Me.BarSubItem2), New DevExpress.XtraBars.LinkPersistInfo(Me.BarSubItem3)})
            Me.Bar1.Text = "Tools"
            '
            'BarSubItem1
            '
            Me.BarSubItem1.Caption = "&File"
            Me.BarSubItem1.Id = 0
            Me.BarSubItem1.LinksPersistInfo.AddRange(New DevExpress.XtraBars.LinkPersistInfo() {New DevExpress.XtraBars.LinkPersistInfo(Me.BarButtonItem_File_Open), New DevExpress.XtraBars.LinkPersistInfo(Me.BarButtonItem_File_DataEntry), New DevExpress.XtraBars.LinkPersistInfo(Me.BarButtonItem_File_Exit, True)})
            Me.BarSubItem1.Name = "BarSubItem1"
            '
            'BarButtonItem_File_Open
            '
            Me.BarButtonItem_File_Open.Caption = "&Open"
            Me.BarButtonItem_File_Open.Id = 3
            Me.BarButtonItem_File_Open.Name = "BarButtonItem_File_Open"
            '
            'BarButtonItem_File_DataEntry
            '
            Me.BarButtonItem_File_DataEntry.Caption = "&Data Entry ..."
            Me.BarButtonItem_File_DataEntry.Id = 4
            Me.BarButtonItem_File_DataEntry.Name = "BarButtonItem_File_DataEntry"
            '
            'BarButtonItem_File_Exit
            '
            Me.BarButtonItem_File_Exit.Caption = "&Exit"
            Me.BarButtonItem_File_Exit.Id = 8
            Me.BarButtonItem_File_Exit.Name = "BarButtonItem_File_Exit"
            '
            'BarSubItem2
            '
            Me.BarSubItem2.Caption = "&Reports"
            Me.BarSubItem2.Id = 1
            Me.BarSubItem2.LinksPersistInfo.AddRange(New DevExpress.XtraBars.LinkPersistInfo() {New DevExpress.XtraBars.LinkPersistInfo(Me.BarButtonItem_Reports_Transactions), New DevExpress.XtraBars.LinkPersistInfo(Me.BarButtonItem_Reports_ErrorListing)})
            Me.BarSubItem2.Name = "BarSubItem2"
            '
            'BarButtonItem_Reports_Transactions
            '
            Me.BarButtonItem_Reports_Transactions.Caption = "&Transactions..."
            Me.BarButtonItem_Reports_Transactions.Id = 5
            Me.BarButtonItem_Reports_Transactions.Name = "BarButtonItem_Reports_Transactions"
            '
            'BarButtonItem_Reports_ErrorListing
            '
            Me.BarButtonItem_Reports_ErrorListing.Caption = "&Error Listing..."
            Me.BarButtonItem_Reports_ErrorListing.Id = 6
            Me.BarButtonItem_Reports_ErrorListing.Name = "BarButtonItem_Reports_ErrorListing"
            '
            'BarSubItem3
            '
            Me.BarSubItem3.Caption = "&Help"
            Me.BarSubItem3.Id = 2
            Me.BarSubItem3.LinksPersistInfo.AddRange(New DevExpress.XtraBars.LinkPersistInfo() {New DevExpress.XtraBars.LinkPersistInfo(Me.BarButtonItem_Help_About)})
            Me.BarSubItem3.Name = "BarSubItem3"
            '
            'BarButtonItem_Help_About
            '
            Me.BarButtonItem_Help_About.Caption = "&About..."
            Me.BarButtonItem_Help_About.Id = 7
            Me.BarButtonItem_Help_About.Name = "BarButtonItem_Help_About"
            '
            'Bar3
            '
            Me.Bar3.BarName = "Status bar"
            Me.Bar3.CanDockStyle = DevExpress.XtraBars.BarCanDockStyle.Bottom
            Me.Bar3.DockCol = 0
            Me.Bar3.DockRow = 0
            Me.Bar3.DockStyle = DevExpress.XtraBars.BarDockStyle.Bottom
            Me.Bar3.OptionsBar.AllowQuickCustomization = False
            Me.Bar3.OptionsBar.DrawDragBorder = False
            Me.Bar3.OptionsBar.UseWholeRow = True
            Me.Bar3.Text = "Status bar"
            '
            'barDockControlTop
            '
            Me.barDockControlTop.CausesValidation = False
            Me.barDockControlTop.Dock = System.Windows.Forms.DockStyle.Top
            Me.barDockControlTop.Location = New System.Drawing.Point(0, 0)
            Me.barDockControlTop.Size = New System.Drawing.Size(605, 29)
            '
            'barDockControlBottom
            '
            Me.barDockControlBottom.CausesValidation = False
            Me.barDockControlBottom.Dock = System.Windows.Forms.DockStyle.Bottom
            Me.barDockControlBottom.Location = New System.Drawing.Point(0, 266)
            Me.barDockControlBottom.Size = New System.Drawing.Size(605, 25)
            '
            'barDockControlLeft
            '
            Me.barDockControlLeft.CausesValidation = False
            Me.barDockControlLeft.Dock = System.Windows.Forms.DockStyle.Left
            Me.barDockControlLeft.Location = New System.Drawing.Point(0, 29)
            Me.barDockControlLeft.Size = New System.Drawing.Size(0, 237)
            '
            'barDockControlRight
            '
            Me.barDockControlRight.CausesValidation = False
            Me.barDockControlRight.Dock = System.Windows.Forms.DockStyle.Right
            Me.barDockControlRight.Location = New System.Drawing.Point(605, 29)
            Me.barDockControlRight.Size = New System.Drawing.Size(0, 237)
            '
            'GridView2
            '
            Me.GridView2.GridControl = Me.GridControl1
            Me.GridView2.Name = "GridView2"
            '
            'LayoutControlGroup1
            '
            Me.LayoutControlGroup1.CustomizationFormText = "LayoutControlGroup1"
            Me.LayoutControlGroup1.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.[True]
            Me.LayoutControlGroup1.GroupBordersVisible = False
            Me.LayoutControlGroup1.Items.AddRange(New DevExpress.XtraLayout.BaseLayoutItem() {Me.LayoutControlItem1})
            Me.LayoutControlGroup1.Location = New System.Drawing.Point(0, 0)
            Me.LayoutControlGroup1.Name = "LayoutControlGroup1"
            Me.LayoutControlGroup1.Size = New System.Drawing.Size(605, 237)
            Me.LayoutControlGroup1.Spacing = New DevExpress.XtraLayout.Utils.Padding(0, 0, 0, 0)
            Me.LayoutControlGroup1.Text = "LayoutControlGroup1"
            Me.LayoutControlGroup1.TextVisible = False
            '
            'LayoutControlItem1
            '
            Me.LayoutControlItem1.Control = Me.GridControl1
            Me.LayoutControlItem1.CustomizationFormText = "LayoutControlItem1"
            Me.LayoutControlItem1.Location = New System.Drawing.Point(0, 0)
            Me.LayoutControlItem1.Name = "LayoutControlItem1"
            Me.LayoutControlItem1.Size = New System.Drawing.Size(585, 217)
            Me.LayoutControlItem1.Text = "LayoutControlItem1"
            Me.LayoutControlItem1.TextSize = New System.Drawing.Size(0, 0)
            Me.LayoutControlItem1.TextToControlDistance = 0
            Me.LayoutControlItem1.TextVisible = False
            '
            'MainForm
            '
            Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
            Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
            Me.ClientSize = New System.Drawing.Size(605, 291)
            Me.Controls.Add(Me.LayoutControl1)
            Me.Controls.Add(Me.barDockControlLeft)
            Me.Controls.Add(Me.barDockControlRight)
            Me.Controls.Add(Me.barDockControlBottom)
            Me.Controls.Add(Me.barDockControlTop)
            Me.LookAndFeel.UseDefaultLookAndFeel = True
            Me.Name = "MainForm"
            Me.Text = "Edit ACH Response Information"
            CType(Me.DxErrorProvider1, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.LayoutControl1, System.ComponentModel.ISupportInitialize).EndInit()
            Me.LayoutControl1.ResumeLayout(False)
            CType(Me.GridControl1, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.GridView1, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.RepositoryItemLookUpEdit_authentication_code, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.barManager1, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.GridView2, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.LayoutControlGroup1, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.LayoutControlItem1, System.ComponentModel.ISupportInitialize).EndInit()
            Me.ResumeLayout(False)

        End Sub
        Friend WithEvents LayoutControl1 As DevExpress.XtraLayout.LayoutControl
        Friend WithEvents LayoutControlGroup1 As DevExpress.XtraLayout.LayoutControlGroup
        Friend WithEvents barManager1 As DevExpress.XtraBars.BarManager
        Friend WithEvents Bar1 As DevExpress.XtraBars.Bar
        Friend WithEvents BarSubItem1 As DevExpress.XtraBars.BarSubItem
        Friend WithEvents BarButtonItem_File_Open As DevExpress.XtraBars.BarButtonItem
        Friend WithEvents BarButtonItem_File_DataEntry As DevExpress.XtraBars.BarButtonItem
        Friend WithEvents BarSubItem2 As DevExpress.XtraBars.BarSubItem
        Friend WithEvents BarButtonItem_Reports_Transactions As DevExpress.XtraBars.BarButtonItem
        Friend WithEvents BarButtonItem_Reports_ErrorListing As DevExpress.XtraBars.BarButtonItem
        Friend WithEvents BarSubItem3 As DevExpress.XtraBars.BarSubItem
        Friend WithEvents BarButtonItem_Help_About As DevExpress.XtraBars.BarButtonItem
        Friend WithEvents Bar3 As DevExpress.XtraBars.Bar
        Friend WithEvents barDockControlTop As DevExpress.XtraBars.BarDockControl
        Friend WithEvents barDockControlBottom As DevExpress.XtraBars.BarDockControl
        Friend WithEvents barDockControlLeft As DevExpress.XtraBars.BarDockControl
        Friend WithEvents barDockControlRight As DevExpress.XtraBars.BarDockControl
        Friend WithEvents GridControl1 As DevExpress.XtraGrid.GridControl
        Friend WithEvents GridView1 As DevExpress.XtraGrid.Views.Grid.GridView
        Friend WithEvents GridView2 As DevExpress.XtraGrid.Views.Grid.GridView
        Friend WithEvents LayoutControlItem1 As DevExpress.XtraLayout.LayoutControlItem
        Friend WithEvents BarButtonItem_File_Exit As DevExpress.XtraBars.BarButtonItem
        Friend WithEvents GridColumn_item_key As DevExpress.XtraGrid.Columns.GridColumn
        Friend WithEvents GridColumn_trace_number As DevExpress.XtraGrid.Columns.GridColumn
        Friend WithEvents GridColumn_client As DevExpress.XtraGrid.Columns.GridColumn
        Friend WithEvents GridColumn_amount As DevExpress.XtraGrid.Columns.GridColumn
        Friend WithEvents GridColumn_transaction_code As DevExpress.XtraGrid.Columns.GridColumn
        Friend WithEvents GridColumn_authenticaiton_code As DevExpress.XtraGrid.Columns.GridColumn
        Friend WithEvents RepositoryItemLookUpEdit_authentication_code As DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit
    End Class
End Namespace