#Region "Copyright 2000-2012 DebtPlus, L.L.C."
'{*******************************************************************}
'{                                                                   }
'{       DebtPlus Debt Management System                             }
'{                                                                   }
'{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
'{       ALL RIGHTS RESERVED                                         }
'{                                                                   }
'{   The entire contents of this file is protected by U.S. and       }
'{   International Copyright Laws. Unauthorized reproduction,        }
'{   reverse-engineering, and distribution of all or any portion of  }
'{   the code contained in this file is strictly prohibited and may  }
'{   result in severe civil and criminal penalties and will be       }
'{   prosecuted to the maximum extent possible under the law.        }
'{                                                                   }
'{   RESTRICTIONS                                                    }
'{                                                                   }
'{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
'{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
'{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
'{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
'{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
'{                                                                   }
'{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
'{   ADDITIONAL RESTRICTIONS.                                        }
'{                                                                   }
'{*******************************************************************}
#End Region

Option Compare Binary
Option Explicit On
Option Strict On

Imports DebtPlus.Utils
Imports System.Data.SqlClient

Namespace ACH.Response.Manual

    Public Class SelectBatchForm

        Private ap As ArgParser

        Friend Sub New(ByVal ap As ArgParser)
            MyBase.New()
            InitializeComponent()
            AddHandler Me.Load, AddressOf SelectBatchForm_Load
            AddHandler LookUpEdit1.EditValueChanged, AddressOf LookUpEdit1_EditValueChanged
            Me.ap = ap
        End Sub

        Public ReadOnly Property BatchID() As Int32
            Get
                If Me.LookUpEdit1.EditValue Is Nothing OrElse Me.LookUpEdit1.EditValue Is System.DBNull.Value Then Return -1
                Return System.Convert.ToInt32(Me.LookUpEdit1.EditValue)
            End Get
        End Property

        Private Sub SelectBatchForm_Load(ByVal sender As Object, ByVal e As System.EventArgs)
            Const TableName As String = "lst_ACH_files_open"

            Dim tbl As System.Data.DataTable = ap.ds.Tables(TableName)
            If tbl Is Nothing Then
                Using cmd As SqlClient.SqlCommand = New SqlCommand
                    With cmd
                        .Connection = New SqlConnection(DebtPlus.LINQ.SQLInfoClass.getDefault().ConnectionString)
                        .CommandText = "lst_ACH_files_open"
                        .CommandType = CommandType.StoredProcedure
                    End With

                    Using da As New SqlClient.SqlDataAdapter(cmd)
                        da.Fill(ap.ds, TableName)
                        tbl = ap.ds.Tables(TableName)
                        With tbl
                            .PrimaryKey = New System.Data.DataColumn() {.Columns("item_key")}
                        End With
                    End Using
                End Using
            End If

            Dim vue As System.Data.DataView = New System.Data.DataView(tbl, String.Empty, "[date_created] desc, [item_key] desc", DataViewRowState.CurrentRows)
            LookUpEdit1.Properties.DataSource = vue
            If vue.Count > 0 Then
                LookUpEdit1.EditValue = vue(0)("item_key")
            End If

            SimpleButton1.Enabled = (BatchID > 0)
        End Sub

        Private Sub LookUpEdit1_EditValueChanged(ByVal sender As Object, ByVal e As System.EventArgs)
            SimpleButton1.Enabled = (BatchID > 0)
        End Sub
    End Class

End Namespace

