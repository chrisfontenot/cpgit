Option Compare Binary
Option Explicit On
Option Strict On

Namespace ACH.Response.Manual
    Friend Class ArgParser
        Inherits DebtPlus.Utils.ArgParserBase

        Private privateBatch As Int32 = -1

        ' Global dataset for use as pseudo-static item
        Public ds As New System.Data.DataSet("ds")

        ''' <summary>
        ''' Batch ID for processing
        ''' </summary>
        ''' <value></value>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public Property ProcessingBatch() As Int32
            Get
                Return privateBatch
            End Get
            Set(ByVal value As Int32)
                privateBatch = value
            End Set
        End Property

        ''' <summary>
        ''' Create an instance of our class
        ''' </summary>
        Public Sub New()
            MyBase.New(New String() {})
        End Sub

        Protected Overrides Function OnSwitch(ByVal switchSymbol As String, ByVal switchValue As String) As SwitchStatus
            Dim Result As SwitchStatus = DebtPlus.Utils.ArgParserBase.SwitchStatus.NoError
            Select Case switchSymbol
                Case "b"
                    Try
                        privateBatch = Int32.Parse(switchValue)
                    Catch ex As System.Exception
                        Result = DebtPlus.Utils.ArgParserBase.SwitchStatus.YesError
                    End Try
                Case Else
                    Result = MyBase.OnSwitch(switchSymbol, switchValue)
            End Select

            Return Result
        End Function

        ''' <summary>
        ''' Generate the command usage information
        ''' </summary>
        Protected Overrides Sub OnUsage(ByVal errorInfo As String)
            ' deprecated
        End Sub

        ''' <summary>
        ''' Return the table with the reject codes
        ''' </summary>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public Function ACHRejectCodes() As System.Data.DataTable
            Const TableName As String = "ach_reject_codes"
            Dim tbl As System.Data.DataTable = ds.Tables(TableName)

            If tbl Is Nothing Then
                Dim cn As System.Data.SqlClient.SqlConnection = New System.Data.SqlClient.SqlConnection(DebtPlus.LINQ.SQLInfoClass.getDefault().ConnectionString)
                Try
                    cn.Open()
                    Using cmd As SqlClient.SqlCommand = New System.Data.SqlClient.SqlCommand()
                        With cmd
                            .Connection = cn
                            .CommandText = "SELECT ach_reject_code, description, serious_error FROM ach_reject_codes"
                            .CommandType = System.Data.CommandType.Text
                        End With

                        Using da As New System.Data.SqlClient.SqlDataAdapter(cmd)
                            da.Fill(ds, TableName)
                            tbl = ds.Tables(TableName)

                            With tbl
                                If .PrimaryKey.GetUpperBound(0) < 0 Then
                                    .PrimaryKey = New System.Data.DataColumn() {.Columns("ach_reject_code")}
                                End If

                                ' Add the pseudo row for "no error" to the list.
                                If .Rows.Find(String.Empty) Is Nothing Then
                                    Dim row As System.Data.DataRow = .NewRow()
                                    row("ach_reject_code") = String.Empty
                                    row("description") = "NO ERROR"
                                    row("serious_error") = False
                                    .Rows.Add(row)

                                    row.AcceptChanges()
                                    .AcceptChanges()
                                End If
                            End With
                        End Using
                    End Using

                Catch ex As System.Data.SqlClient.SqlException
                    Using gdr = New DebtPlus.Repository.GetDataResult()
                        gdr.HandleException(ex)
                        DebtPlus.UI.Common.ErrorHandling.HandleErrors(gdr, "Error reading ACH reject codes")
                    End Using
                End Try
            End If

            Return tbl
        End Function

        Public ReadOnly Property ResponseBatchID() As String
            Get
                Static ResponseID As String = String.Empty
                If ResponseID = String.Empty Then
                    ResponseID = String.Format("{0:YYYYMMDD_HHmm}", DateTime.Now)
                End If
                Return ResponseID
            End Get
        End Property

        Public Sub SaveChanges()
            Const TableName As String = "xpr_ACH_file_contents"
            Dim tbl As System.Data.DataTable = ds.Tables(TableName)

            If tbl IsNot Nothing Then
                Dim UpdateCmd As System.Data.SqlClient.SqlCommand = New System.Data.SqlClient.SqlCommand()
                Dim da As New System.Data.SqlClient.SqlDataAdapter
                Dim RowsUpdated As Int32
                Try
                    With UpdateCmd
                        .Connection = New System.Data.SqlClient.SqlConnection(DebtPlus.LINQ.SQLInfoClass.getDefault().ConnectionString)
                        .CommandText = "xpr_ACH_response_PAYMENT"
                        .CommandType = System.Data.CommandType.StoredProcedure
                        .Parameters.Add("@deposit_batch_detail", System.Data.SqlDbType.Int, 0, "item_key")
                        .Parameters.Add("@reason", System.Data.SqlDbType.VarChar, 4, "authentication_code")
                        .Parameters.Add("@ach_response_batch_id", System.Data.SqlDbType.VarChar, 50).Value = ResponseBatchID
                    End With
                    da.UpdateCommand = UpdateCmd
                    RowsUpdated = da.Update(tbl)

                Catch ex As System.Data.SqlClient.SqlException
                    Using gdr = New DebtPlus.Repository.GetDataResult()
                        gdr.HandleException(ex)
                        DebtPlus.UI.Common.ErrorHandling.HandleErrors(gdr, "Error updating ACH transactions")
                    End Using

                Finally
                    da.Dispose()
                    UpdateCmd.Dispose()
                End Try
            End If
        End Sub

        Public Function RequestBatchID() As Int32
            Dim answer As System.Windows.Forms.DialogResult
            Using frm As New SelectBatchForm(Me)
                answer = frm.ShowDialog()
                If answer = System.Windows.Forms.DialogResult.OK Then
                    Return frm.BatchID
                End If
            End Using

            Return -1
        End Function
    End Class
End NameSpace
