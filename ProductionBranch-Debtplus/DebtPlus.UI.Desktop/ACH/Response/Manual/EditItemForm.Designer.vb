Namespace ACH.Response.Manual
    <Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
    Partial Class EditItemForm
        Inherits DebtPlus.Data.Forms.DebtPlusForm

        'Form overrides dispose to clean up the component list.
        <System.Diagnostics.DebuggerNonUserCode()> _
        Protected Overrides Sub Dispose(ByVal disposing As Boolean)
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
            MyBase.Dispose(disposing)
        End Sub

        'Required by the Windows Form Designer
        Private components As System.ComponentModel.IContainer

        'NOTE: The following procedure is required by the Windows Form Designer
        'It can be modified using the Windows Form Designer.  
        'Do not modify it using the code editor.
        <System.Diagnostics.DebuggerStepThrough()> _
        Private Sub InitializeComponent()
            Me.LayoutControl1 = New DevExpress.XtraLayout.LayoutControl
            Me.LookUpEdit_authentication_code = New DevExpress.XtraEditors.LookUpEdit
            Me.LabelControl_amount = New DevExpress.XtraEditors.LabelControl
            Me.LabelControl_trace_number = New DevExpress.XtraEditors.LabelControl
            Me.SimpleButton2 = New DevExpress.XtraEditors.SimpleButton
            Me.SimpleButton1 = New DevExpress.XtraEditors.SimpleButton
            Me.LabelControl1 = New DevExpress.XtraEditors.LabelControl
            Me.LabelControl_client = New DevExpress.XtraEditors.LabelControl
            Me.LayoutControlGroup1 = New DevExpress.XtraLayout.LayoutControlGroup
            Me.LayoutControlItem1 = New DevExpress.XtraLayout.LayoutControlItem
            Me.LayoutControlItem2 = New DevExpress.XtraLayout.LayoutControlItem
            Me.LayoutControlItem3 = New DevExpress.XtraLayout.LayoutControlItem
            Me.EmptySpaceItem1 = New DevExpress.XtraLayout.EmptySpaceItem
            Me.EmptySpaceItem2 = New DevExpress.XtraLayout.EmptySpaceItem
            Me.EmptySpaceItem3 = New DevExpress.XtraLayout.EmptySpaceItem
            Me.LayoutControlItem4 = New DevExpress.XtraLayout.LayoutControlItem
            Me.LayoutControlItem5 = New DevExpress.XtraLayout.LayoutControlItem
            Me.LayoutControlItem6 = New DevExpress.XtraLayout.LayoutControlItem
            Me.LayoutControlItem7 = New DevExpress.XtraLayout.LayoutControlItem
            CType(Me.DxErrorProvider1, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.LayoutControl1, System.ComponentModel.ISupportInitialize).BeginInit()
            Me.LayoutControl1.SuspendLayout()
            CType(Me.LookUpEdit_authentication_code.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.LayoutControlGroup1, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.LayoutControlItem1, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.LayoutControlItem2, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.LayoutControlItem3, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.EmptySpaceItem1, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.EmptySpaceItem2, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.EmptySpaceItem3, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.LayoutControlItem4, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.LayoutControlItem5, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.LayoutControlItem6, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.LayoutControlItem7, System.ComponentModel.ISupportInitialize).BeginInit()
            Me.SuspendLayout()
            '
            'LayoutControl1
            '
            Me.LayoutControl1.Controls.Add(Me.LookUpEdit_authentication_code)
            Me.LayoutControl1.Controls.Add(Me.LabelControl_amount)
            Me.LayoutControl1.Controls.Add(Me.LabelControl_trace_number)
            Me.LayoutControl1.Controls.Add(Me.SimpleButton2)
            Me.LayoutControl1.Controls.Add(Me.SimpleButton1)
            Me.LayoutControl1.Controls.Add(Me.LabelControl1)
            Me.LayoutControl1.Controls.Add(Me.LabelControl_client)
            Me.LayoutControl1.Dock = System.Windows.Forms.DockStyle.Fill
            Me.LayoutControl1.Location = New System.Drawing.Point(0, 0)
            Me.LayoutControl1.Name = "LayoutControl1"
            Me.LayoutControl1.Root = Me.LayoutControlGroup1
            Me.LayoutControl1.Size = New System.Drawing.Size(388, 184)
            Me.LayoutControl1.TabIndex = 0
            Me.LayoutControl1.Text = "LayoutControl1"
            '
            'LookUpEdit_authentication_code
            '
            Me.LookUpEdit_authentication_code.Location = New System.Drawing.Point(83, 107)
            Me.LookUpEdit_authentication_code.Name = "LookUpEdit_authentication_code"
            Me.LookUpEdit_authentication_code.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[True]
            Me.LookUpEdit_authentication_code.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
            Me.LookUpEdit_authentication_code.Properties.Columns.AddRange(New DevExpress.XtraEditors.Controls.LookUpColumnInfo() {New DevExpress.XtraEditors.Controls.LookUpColumnInfo("ach_reject_code", "ID", 5, DevExpress.Utils.FormatType.Numeric, "f0", True, DevExpress.Utils.HorzAlignment.[Default], DevExpress.Data.ColumnSortOrder.Ascending), New DevExpress.XtraEditors.Controls.LookUpColumnInfo("description", 30, "Description")})
            Me.LookUpEdit_authentication_code.Properties.DisplayMember = "description"
            Me.LookUpEdit_authentication_code.Properties.NullText = ""
            Me.LookUpEdit_authentication_code.Properties.ShowFooter = False
            Me.LookUpEdit_authentication_code.Properties.ValueMember = "ach_reject_code"
            Me.LookUpEdit_authentication_code.Size = New System.Drawing.Size(293, 20)
            Me.LookUpEdit_authentication_code.StyleController = Me.LayoutControl1
            Me.LookUpEdit_authentication_code.TabIndex = 10
            Me.LookUpEdit_authentication_code.Properties.SortColumnIndex = 1
            '
            'LabelControl_amount
            '
            Me.LabelControl_amount.Location = New System.Drawing.Point(83, 90)
            Me.LabelControl_amount.Name = "LabelControl_amount"
            Me.LabelControl_amount.Padding = New System.Windows.Forms.Padding(20, 0, 0, 0)
            Me.LabelControl_amount.Size = New System.Drawing.Size(293, 13)
            Me.LabelControl_amount.StyleController = Me.LayoutControl1
            Me.LabelControl_amount.TabIndex = 9
            '
            'LabelControl_trace_number
            '
            Me.LabelControl_trace_number.Location = New System.Drawing.Point(83, 56)
            Me.LabelControl_trace_number.Name = "LabelControl_trace_number"
            Me.LabelControl_trace_number.Padding = New System.Windows.Forms.Padding(20, 0, 0, 0)
            Me.LabelControl_trace_number.Size = New System.Drawing.Size(293, 13)
            Me.LabelControl_trace_number.StyleController = Me.LayoutControl1
            Me.LabelControl_trace_number.TabIndex = 7
            '
            'SimpleButton2
            '
            Me.SimpleButton2.DialogResult = System.Windows.Forms.DialogResult.Cancel
            Me.SimpleButton2.Location = New System.Drawing.Point(194, 149)
            Me.SimpleButton2.MaximumSize = New System.Drawing.Size(75, 23)
            Me.SimpleButton2.MinimumSize = New System.Drawing.Size(75, 23)
            Me.SimpleButton2.Name = "SimpleButton2"
            Me.SimpleButton2.Size = New System.Drawing.Size(75, 23)
            Me.SimpleButton2.StyleController = Me.LayoutControl1
            Me.SimpleButton2.TabIndex = 6
            Me.SimpleButton2.Text = "&Cancel"
            '
            'SimpleButton1
            '
            Me.SimpleButton1.DialogResult = System.Windows.Forms.DialogResult.OK
            Me.SimpleButton1.Location = New System.Drawing.Point(115, 149)
            Me.SimpleButton1.MaximumSize = New System.Drawing.Size(75, 23)
            Me.SimpleButton1.MinimumSize = New System.Drawing.Size(75, 23)
            Me.SimpleButton1.Name = "SimpleButton1"
            Me.SimpleButton1.Size = New System.Drawing.Size(75, 23)
            Me.SimpleButton1.StyleController = Me.LayoutControl1
            Me.SimpleButton1.TabIndex = 5
            Me.SimpleButton1.Text = "&OK"
            '
            'LabelControl1
            '
            Me.LabelControl1.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
            Me.LabelControl1.Appearance.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Top
            Me.LabelControl1.Appearance.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap
            Me.LabelControl1.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None
            Me.LabelControl1.Location = New System.Drawing.Point(12, 12)
            Me.LabelControl1.MinimumSize = New System.Drawing.Size(0, 40)
            Me.LabelControl1.Name = "LabelControl1"
            Me.LabelControl1.Padding = New System.Windows.Forms.Padding(5)
            Me.LabelControl1.Size = New System.Drawing.Size(364, 40)
            Me.LabelControl1.StyleController = Me.LayoutControl1
            Me.LabelControl1.TabIndex = 4
            Me.LabelControl1.Text = "Select the error code associated with the indicated transaction. To remove the er" & _
                                    "ror, select NO ERROR from the list."
            '
            'LabelControl_client
            '
            Me.LabelControl_client.Location = New System.Drawing.Point(83, 73)
            Me.LabelControl_client.Name = "LabelControl_client"
            Me.LabelControl_client.Padding = New System.Windows.Forms.Padding(20, 0, 0, 0)
            Me.LabelControl_client.Size = New System.Drawing.Size(293, 13)
            Me.LabelControl_client.StyleController = Me.LayoutControl1
            Me.LabelControl_client.TabIndex = 8
            '
            'LayoutControlGroup1
            '
            Me.LayoutControlGroup1.CustomizationFormText = "LayoutControlGroup1"
            Me.LayoutControlGroup1.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.[True]
            Me.LayoutControlGroup1.GroupBordersVisible = False
            Me.LayoutControlGroup1.Items.AddRange(New DevExpress.XtraLayout.BaseLayoutItem() {Me.LayoutControlItem1, Me.LayoutControlItem2, Me.LayoutControlItem3, Me.EmptySpaceItem1, Me.EmptySpaceItem2, Me.EmptySpaceItem3, Me.LayoutControlItem4, Me.LayoutControlItem5, Me.LayoutControlItem6, Me.LayoutControlItem7})
            Me.LayoutControlGroup1.Location = New System.Drawing.Point(0, 0)
            Me.LayoutControlGroup1.Name = "LayoutControlGroup1"
            Me.LayoutControlGroup1.Size = New System.Drawing.Size(388, 184)
            Me.LayoutControlGroup1.Spacing = New DevExpress.XtraLayout.Utils.Padding(0, 0, 0, 0)
            Me.LayoutControlGroup1.Text = "LayoutControlGroup1"
            Me.LayoutControlGroup1.TextVisible = False
            '
            'LayoutControlItem1
            '
            Me.LayoutControlItem1.Control = Me.LabelControl1
            Me.LayoutControlItem1.CustomizationFormText = "LayoutControlItem1"
            Me.LayoutControlItem1.Location = New System.Drawing.Point(0, 0)
            Me.LayoutControlItem1.Name = "LayoutControlItem1"
            Me.LayoutControlItem1.Size = New System.Drawing.Size(368, 44)
            Me.LayoutControlItem1.Text = "LayoutControlItem1"
            Me.LayoutControlItem1.TextSize = New System.Drawing.Size(0, 0)
            Me.LayoutControlItem1.TextToControlDistance = 0
            Me.LayoutControlItem1.TextVisible = False
            '
            'LayoutControlItem2
            '
            Me.LayoutControlItem2.Control = Me.SimpleButton1
            Me.LayoutControlItem2.CustomizationFormText = "OK Button"
            Me.LayoutControlItem2.Location = New System.Drawing.Point(103, 137)
            Me.LayoutControlItem2.Name = "LayoutControlItem2"
            Me.LayoutControlItem2.Size = New System.Drawing.Size(79, 27)
            Me.LayoutControlItem2.Text = "OK Button"
            Me.LayoutControlItem2.TextSize = New System.Drawing.Size(0, 0)
            Me.LayoutControlItem2.TextToControlDistance = 0
            Me.LayoutControlItem2.TextVisible = False
            '
            'LayoutControlItem3
            '
            Me.LayoutControlItem3.Control = Me.SimpleButton2
            Me.LayoutControlItem3.CustomizationFormText = "CANCEL button"
            Me.LayoutControlItem3.Location = New System.Drawing.Point(182, 137)
            Me.LayoutControlItem3.Name = "LayoutControlItem3"
            Me.LayoutControlItem3.Size = New System.Drawing.Size(79, 27)
            Me.LayoutControlItem3.Text = "CANCEL button"
            Me.LayoutControlItem3.TextSize = New System.Drawing.Size(0, 0)
            Me.LayoutControlItem3.TextToControlDistance = 0
            Me.LayoutControlItem3.TextVisible = False
            '
            'EmptySpaceItem1
            '
            Me.EmptySpaceItem1.CustomizationFormText = "EmptySpaceItem1"
            Me.EmptySpaceItem1.Location = New System.Drawing.Point(0, 119)
            Me.EmptySpaceItem1.Name = "EmptySpaceItem1"
            Me.EmptySpaceItem1.Size = New System.Drawing.Size(368, 18)
            Me.EmptySpaceItem1.Text = "EmptySpaceItem1"
            Me.EmptySpaceItem1.TextSize = New System.Drawing.Size(0, 0)
            '
            'EmptySpaceItem2
            '
            Me.EmptySpaceItem2.CustomizationFormText = "EmptySpaceItem2"
            Me.EmptySpaceItem2.Location = New System.Drawing.Point(261, 137)
            Me.EmptySpaceItem2.Name = "EmptySpaceItem2"
            Me.EmptySpaceItem2.Size = New System.Drawing.Size(107, 27)
            Me.EmptySpaceItem2.Text = "EmptySpaceItem2"
            Me.EmptySpaceItem2.TextSize = New System.Drawing.Size(0, 0)
            '
            'EmptySpaceItem3
            '
            Me.EmptySpaceItem3.CustomizationFormText = "EmptySpaceItem3"
            Me.EmptySpaceItem3.Location = New System.Drawing.Point(0, 137)
            Me.EmptySpaceItem3.Name = "EmptySpaceItem3"
            Me.EmptySpaceItem3.Size = New System.Drawing.Size(103, 27)
            Me.EmptySpaceItem3.Text = "EmptySpaceItem3"
            Me.EmptySpaceItem3.TextSize = New System.Drawing.Size(0, 0)
            '
            'LayoutControlItem4
            '
            Me.LayoutControlItem4.Control = Me.LabelControl_trace_number
            Me.LayoutControlItem4.CustomizationFormText = "Trace Number"
            Me.LayoutControlItem4.Location = New System.Drawing.Point(0, 44)
            Me.LayoutControlItem4.Name = "LayoutControlItem4"
            Me.LayoutControlItem4.Size = New System.Drawing.Size(368, 17)
            Me.LayoutControlItem4.Text = "Trace Number"
            Me.LayoutControlItem4.TextSize = New System.Drawing.Size(67, 13)
            '
            'LayoutControlItem5
            '
            Me.LayoutControlItem5.Control = Me.LabelControl_client
            Me.LayoutControlItem5.CustomizationFormText = "Client ID"
            Me.LayoutControlItem5.Location = New System.Drawing.Point(0, 61)
            Me.LayoutControlItem5.Name = "LayoutControlItem5"
            Me.LayoutControlItem5.Size = New System.Drawing.Size(368, 17)
            Me.LayoutControlItem5.Text = "Client ID"
            Me.LayoutControlItem5.TextSize = New System.Drawing.Size(67, 13)
            '
            'LayoutControlItem6
            '
            Me.LayoutControlItem6.Control = Me.LabelControl_amount
            Me.LayoutControlItem6.CustomizationFormText = "Dollar Amount"
            Me.LayoutControlItem6.Location = New System.Drawing.Point(0, 78)
            Me.LayoutControlItem6.Name = "LayoutControlItem6"
            Me.LayoutControlItem6.Size = New System.Drawing.Size(368, 17)
            Me.LayoutControlItem6.Text = "Dollar Amount"
            Me.LayoutControlItem6.TextSize = New System.Drawing.Size(67, 13)
            '
            'LayoutControlItem7
            '
            Me.LayoutControlItem7.Control = Me.LookUpEdit_authentication_code
            Me.LayoutControlItem7.CustomizationFormText = "Error Code"
            Me.LayoutControlItem7.Location = New System.Drawing.Point(0, 95)
            Me.LayoutControlItem7.Name = "LayoutControlItem7"
            Me.LayoutControlItem7.Size = New System.Drawing.Size(368, 24)
            Me.LayoutControlItem7.Text = "Error Code"
            Me.LayoutControlItem7.TextSize = New System.Drawing.Size(67, 13)
            '
            'EditItemForm
            '
            Me.AcceptButton = Me.SimpleButton1
            Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
            Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
            Me.CancelButton = Me.SimpleButton2
            Me.ClientSize = New System.Drawing.Size(388, 184)
            Me.Controls.Add(Me.LayoutControl1)
            Me.LookAndFeel.UseDefaultLookAndFeel = True
            Me.Name = "EditItemForm"
            Me.Text = "Edit ACH Transaction"
            CType(Me.DxErrorProvider1, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.LayoutControl1, System.ComponentModel.ISupportInitialize).EndInit()
            Me.LayoutControl1.ResumeLayout(False)
            CType(Me.LookUpEdit_authentication_code.Properties, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.LayoutControlGroup1, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.LayoutControlItem1, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.LayoutControlItem2, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.LayoutControlItem3, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.EmptySpaceItem1, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.EmptySpaceItem2, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.EmptySpaceItem3, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.LayoutControlItem4, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.LayoutControlItem5, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.LayoutControlItem6, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.LayoutControlItem7, System.ComponentModel.ISupportInitialize).EndInit()
            Me.ResumeLayout(False)

        End Sub
        Friend WithEvents LayoutControl1 As DevExpress.XtraLayout.LayoutControl
        Friend WithEvents LayoutControlGroup1 As DevExpress.XtraLayout.LayoutControlGroup
        Friend WithEvents LabelControl1 As DevExpress.XtraEditors.LabelControl
        Friend WithEvents LayoutControlItem1 As DevExpress.XtraLayout.LayoutControlItem
        Friend WithEvents SimpleButton2 As DevExpress.XtraEditors.SimpleButton
        Friend WithEvents SimpleButton1 As DevExpress.XtraEditors.SimpleButton
        Friend WithEvents LayoutControlItem2 As DevExpress.XtraLayout.LayoutControlItem
        Friend WithEvents LayoutControlItem3 As DevExpress.XtraLayout.LayoutControlItem
        Friend WithEvents LookUpEdit_authentication_code As DevExpress.XtraEditors.LookUpEdit
        Friend WithEvents LabelControl_amount As DevExpress.XtraEditors.LabelControl
        Friend WithEvents LabelControl_trace_number As DevExpress.XtraEditors.LabelControl
        Friend WithEvents LabelControl_client As DevExpress.XtraEditors.LabelControl
        Friend WithEvents EmptySpaceItem1 As DevExpress.XtraLayout.EmptySpaceItem
        Friend WithEvents EmptySpaceItem2 As DevExpress.XtraLayout.EmptySpaceItem
        Friend WithEvents EmptySpaceItem3 As DevExpress.XtraLayout.EmptySpaceItem
        Friend WithEvents LayoutControlItem4 As DevExpress.XtraLayout.LayoutControlItem
        Friend WithEvents LayoutControlItem5 As DevExpress.XtraLayout.LayoutControlItem
        Friend WithEvents LayoutControlItem6 As DevExpress.XtraLayout.LayoutControlItem
        Friend WithEvents LayoutControlItem7 As DevExpress.XtraLayout.LayoutControlItem
    End Class
End Namespace