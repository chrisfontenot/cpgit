#Region "Copyright 2000-2012 DebtPlus, L.L.C."
'{*******************************************************************}
'{                                                                   }
'{       DebtPlus Debt Management System                             }
'{                                                                   }
'{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
'{       ALL RIGHTS RESERVED                                         }
'{                                                                   }
'{   The entire contents of this file is protected by U.S. and       }
'{   International Copyright Laws. Unauthorized reproduction,        }
'{   reverse-engineering, and distribution of all or any portion of  }
'{   the code contained in this file is strictly prohibited and may  }
'{   result in severe civil and criminal penalties and will be       }
'{   prosecuted to the maximum extent possible under the law.        }
'{                                                                   }
'{   RESTRICTIONS                                                    }
'{                                                                   }
'{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
'{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
'{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
'{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
'{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
'{                                                                   }
'{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
'{   ADDITIONAL RESTRICTIONS.                                        }
'{                                                                   }
'{*******************************************************************}
#End Region

Option Compare Binary
Option Explicit On
Option Strict On
Namespace ACH.Response.Manual

    Friend Class DataEntryForm
        Private ap As ArgParser

        Public Sub New(ByVal ap As ArgParser)
            MyBase.New()
            InitializeComponent()
            Me.ap = ap

            AddHandler TextEdit_trace_number.GotFocus, AddressOf TextEdit_trace_number_GotFocus
            AddHandler Me.Load, AddressOf DataEntryForm_Load
            AddHandler TextEdit_trace_number.Validating, AddressOf TextEdit_trace_number_Validating
            AddHandler TextEdit_trace_number.Validated, AddressOf TextEdit_trace_number_Validated
            AddHandler LookUpEdit_authentication_code.Validated, AddressOf TextEdit_trace_number_Validated
            AddHandler SimpleButton1.Click, AddressOf SimpleButton1_Click
        End Sub

        Private PrefixKey As String
        Private EditTable As System.Data.DataTable = Nothing
        Private Sub DataEntryForm_Load(ByVal sender As Object, ByVal e As System.EventArgs)
            Const TableName As String = "xpr_ACH_file_contents"
            PrefixKey = String.Empty
            EditTable = ap.ds.Tables(TableName)

            If EditTable.Rows.Count > 0 Then
                Dim row As System.Data.DataRow = EditTable.Rows(0)
                PrefixKey = Convert.ToString(row("trace_number")).Substring(0, 8)
            End If

            LookUpEdit_authentication_code.Properties.DataSource = ap.ACHRejectCodes.DefaultView

            EmptyForm()
        End Sub

        Private Sub EmptyForm()
            TextEdit_trace_number.Text = PrefixKey
            LookUpEdit_authentication_code.EditValue = Nothing
            SimpleButton1.Enabled = False
        End Sub

        Private Sub TextEdit_trace_number_GotFocus(ByVal sender As Object, ByVal e As System.EventArgs)
            With TextEdit_trace_number

                ' Find the current text
                Dim InputText As String = String.Empty
                If .EditValue IsNot Nothing AndAlso .EditValue IsNot System.DBNull.Value Then
                    InputText = Convert.ToString(.EditValue).Trim()
                End If

                ' Position the cursor after the text
                .SelectionStart = InputText.Length
                .SelectionLength = 0
            End With
        End Sub

        Private Sub TextEdit_trace_number_Validated(ByVal sender As Object, ByVal e As System.EventArgs)
            Dim TraceNumber As String = String.Empty

            If TextEdit_trace_number.EditValue IsNot System.DBNull.Value AndAlso TextEdit_trace_number.EditValue IsNot Nothing Then
                TraceNumber = Convert.ToString(TextEdit_trace_number.EditValue).Trim()
            End If

            Dim rows() As System.Data.DataRow = EditTable.Select(String.Format("[trace_number]='{0}'", TraceNumber), "item_key")
            SimpleButton1.Enabled = (rows.GetUpperBound(0) >= 0)
        End Sub

        Private Sub TextEdit_trace_number_Validating(ByVal sender As Object, ByVal e As System.ComponentModel.CancelEventArgs)
            Dim TraceNumberField As String = String.Empty

            If TextEdit_trace_number.EditValue IsNot System.DBNull.Value AndAlso TextEdit_trace_number.EditValue IsNot Nothing Then
                TraceNumberField = Convert.ToString(TextEdit_trace_number.EditValue).Trim()
            End If

            If TraceNumberField <> String.Empty Then
                Dim rows() As System.Data.DataRow = EditTable.Select(String.Format("[trace_number]='{0}'", TraceNumberField), "item_key")
                If rows.GetUpperBound(0) >= 0 Then
                    TextEdit_trace_number.ErrorText = String.Empty
                    Return
                End If

                TextEdit_trace_number.ErrorText = "Trace Number must be 15 digits and in the items for this batch"
                e.Cancel = True
            End If
        End Sub

        Private Sub SimpleButton1_Click(ByVal sender As Object, ByVal e As System.EventArgs)
            Dim TraceNumber As String = String.Empty

            If TextEdit_trace_number.EditValue IsNot System.DBNull.Value AndAlso TextEdit_trace_number.EditValue IsNot Nothing Then
                TraceNumber = Convert.ToString(TextEdit_trace_number.EditValue).Trim()
            End If

            If TraceNumber <> String.Empty Then
                Dim rows() As System.Data.DataRow = EditTable.Select(String.Format("[trace_number]='{0}'", TraceNumber), "item_key")
                If rows.GetUpperBound(0) >= 0 Then
                    Dim row As System.Data.DataRow = rows(0)
                    row.BeginEdit()
                    row("authentication_code") = LookUpEdit_authentication_code.EditValue
                    row.EndEdit()

                    ' Clear the form data
                    EmptyForm()

                    ' Go back to the trace number field.
                    TextEdit_trace_number.Focus()
                End If
            End If
        End Sub
    End Class
End Namespace