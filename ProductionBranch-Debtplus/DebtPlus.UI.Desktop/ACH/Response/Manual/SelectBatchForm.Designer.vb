Namespace ACH.Response.Manual
    <Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
    Partial Class SelectBatchForm
        Inherits DebtPlus.Data.Forms.DebtPlusForm

        'Form overrides dispose to clean up the component list.
        <System.Diagnostics.DebuggerNonUserCode()> _
        Protected Overrides Sub Dispose(ByVal disposing As Boolean)
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
            MyBase.Dispose(disposing)
        End Sub

        'Required by the Windows Form Designer
        Private components As System.ComponentModel.IContainer

        'NOTE: The following procedure is required by the Windows Form Designer
        'It can be modified using the Windows Form Designer.  
        'Do not modify it using the code editor.
        <System.Diagnostics.DebuggerStepThrough()> _
        Private Sub InitializeComponent()
            Me.LayoutControl1 = New DevExpress.XtraLayout.LayoutControl
            Me.SimpleButton2 = New DevExpress.XtraEditors.SimpleButton
            Me.SimpleButton1 = New DevExpress.XtraEditors.SimpleButton
            Me.LookUpEdit1 = New DevExpress.XtraEditors.LookUpEdit
            Me.LabelControl1 = New DevExpress.XtraEditors.LabelControl
            Me.LayoutControlGroup1 = New DevExpress.XtraLayout.LayoutControlGroup
            Me.LayoutControlItem1 = New DevExpress.XtraLayout.LayoutControlItem
            Me.LayoutControlItem2 = New DevExpress.XtraLayout.LayoutControlItem
            Me.LayoutControlItem3 = New DevExpress.XtraLayout.LayoutControlItem
            Me.LayoutControlItem4 = New DevExpress.XtraLayout.LayoutControlItem
            Me.EmptySpaceItem1 = New DevExpress.XtraLayout.EmptySpaceItem
            Me.EmptySpaceItem2 = New DevExpress.XtraLayout.EmptySpaceItem
            Me.EmptySpaceItem3 = New DevExpress.XtraLayout.EmptySpaceItem
            CType(Me.DxErrorProvider1, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.LayoutControl1, System.ComponentModel.ISupportInitialize).BeginInit()
            Me.LayoutControl1.SuspendLayout()
            CType(Me.LookUpEdit1.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.LayoutControlGroup1, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.LayoutControlItem1, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.LayoutControlItem2, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.LayoutControlItem3, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.LayoutControlItem4, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.EmptySpaceItem1, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.EmptySpaceItem2, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.EmptySpaceItem3, System.ComponentModel.ISupportInitialize).BeginInit()
            Me.SuspendLayout()
            '
            'LayoutControl1
            '
            Me.LayoutControl1.Controls.Add(Me.SimpleButton2)
            Me.LayoutControl1.Controls.Add(Me.SimpleButton1)
            Me.LayoutControl1.Controls.Add(Me.LookUpEdit1)
            Me.LayoutControl1.Controls.Add(Me.LabelControl1)
            Me.LayoutControl1.Dock = System.Windows.Forms.DockStyle.Fill
            Me.LayoutControl1.Location = New System.Drawing.Point(0, 0)
            Me.LayoutControl1.Name = "LayoutControl1"
            Me.LayoutControl1.Root = Me.LayoutControlGroup1
            Me.LayoutControl1.Size = New System.Drawing.Size(326, 148)
            Me.LayoutControl1.TabIndex = 0
            Me.LayoutControl1.Text = "LayoutControl1"
            '
            'SimpleButton2
            '
            Me.SimpleButton2.DialogResult = System.Windows.Forms.DialogResult.Cancel
            Me.SimpleButton2.Location = New System.Drawing.Point(177, 113)
            Me.SimpleButton2.MaximumSize = New System.Drawing.Size(75, 23)
            Me.SimpleButton2.MinimumSize = New System.Drawing.Size(75, 23)
            Me.SimpleButton2.Name = "SimpleButton2"
            Me.SimpleButton2.Size = New System.Drawing.Size(75, 23)
            Me.SimpleButton2.StyleController = Me.LayoutControl1
            Me.SimpleButton2.TabIndex = 7
            Me.SimpleButton2.Text = "&Cancel"
            '
            'SimpleButton1
            '
            Me.SimpleButton1.DialogResult = System.Windows.Forms.DialogResult.OK
            Me.SimpleButton1.Location = New System.Drawing.Point(98, 113)
            Me.SimpleButton1.MaximumSize = New System.Drawing.Size(75, 23)
            Me.SimpleButton1.MinimumSize = New System.Drawing.Size(75, 23)
            Me.SimpleButton1.Name = "SimpleButton1"
            Me.SimpleButton1.Size = New System.Drawing.Size(75, 23)
            Me.SimpleButton1.StyleController = Me.LayoutControl1
            Me.SimpleButton1.TabIndex = 6
            Me.SimpleButton1.Text = "&OK"
            '
            'LookUpEdit1
            '
            Me.LookUpEdit1.Location = New System.Drawing.Point(15, 59)
            Me.LookUpEdit1.Name = "LookUpEdit1"
            Me.LookUpEdit1.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[False]
            Me.LookUpEdit1.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
            Me.LookUpEdit1.Properties.Columns.AddRange(New DevExpress.XtraEditors.Controls.LookUpColumnInfo() {New DevExpress.XtraEditors.Controls.LookUpColumnInfo("item_key", "ID", 20, DevExpress.Utils.FormatType.Numeric, "f0", False, DevExpress.Utils.HorzAlignment.[Default]), New DevExpress.XtraEditors.Controls.LookUpColumnInfo("date_created", "Date", 20, DevExpress.Utils.FormatType.DateTime, "dddd, MMMM d, yyyy h:mm tt", True, DevExpress.Utils.HorzAlignment.[Default])})
            Me.LookUpEdit1.Properties.DisplayFormat.FormatString = "{0:dddd, MMMM d, yyyy h:mm tt}"
            Me.LookUpEdit1.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime
            Me.LookUpEdit1.Properties.DisplayMember = "date_created"
            Me.LookUpEdit1.Properties.NullText = ""
            Me.LookUpEdit1.Properties.ShowFooter = False
            Me.LookUpEdit1.Properties.ShowHeader = False
            Me.LookUpEdit1.Properties.ValueMember = "item_key"
            Me.LookUpEdit1.Size = New System.Drawing.Size(296, 20)
            Me.LookUpEdit1.StyleController = Me.LayoutControl1
            Me.LookUpEdit1.TabIndex = 5
            Me.LookUpEdit1.Properties.SortColumnIndex = 1
            '
            'LabelControl1
            '
            Me.LabelControl1.Appearance.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Top
            Me.LabelControl1.Appearance.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap
            Me.LabelControl1.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None
            Me.LabelControl1.Location = New System.Drawing.Point(12, 12)
            Me.LabelControl1.MinimumSize = New System.Drawing.Size(0, 40)
            Me.LabelControl1.Name = "LabelControl1"
            Me.LabelControl1.Padding = New System.Windows.Forms.Padding(5)
            Me.LabelControl1.Size = New System.Drawing.Size(302, 40)
            Me.LabelControl1.StyleController = Me.LayoutControl1
            Me.LabelControl1.TabIndex = 4
            Me.LabelControl1.Text = "Select the desired ACH batch to be processed. The following list shows the date t" & _
                                    "hat the batch was generated."
            '
            'LayoutControlGroup1
            '
            Me.LayoutControlGroup1.CustomizationFormText = "LayoutControlGroup1"
            Me.LayoutControlGroup1.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.[True]
            Me.LayoutControlGroup1.GroupBordersVisible = False
            Me.LayoutControlGroup1.Items.AddRange(New DevExpress.XtraLayout.BaseLayoutItem() {Me.LayoutControlItem1, Me.LayoutControlItem2, Me.LayoutControlItem3, Me.LayoutControlItem4, Me.EmptySpaceItem1, Me.EmptySpaceItem2, Me.EmptySpaceItem3})
            Me.LayoutControlGroup1.Location = New System.Drawing.Point(0, 0)
            Me.LayoutControlGroup1.Name = "LayoutControlGroup1"
            Me.LayoutControlGroup1.Size = New System.Drawing.Size(326, 148)
            Me.LayoutControlGroup1.Spacing = New DevExpress.XtraLayout.Utils.Padding(0, 0, 0, 0)
            Me.LayoutControlGroup1.Text = "LayoutControlGroup1"
            Me.LayoutControlGroup1.TextVisible = False
            '
            'LayoutControlItem1
            '
            Me.LayoutControlItem1.Control = Me.LabelControl1
            Me.LayoutControlItem1.CustomizationFormText = "LayoutControlItem1"
            Me.LayoutControlItem1.Location = New System.Drawing.Point(0, 0)
            Me.LayoutControlItem1.Name = "LayoutControlItem1"
            Me.LayoutControlItem1.Size = New System.Drawing.Size(306, 44)
            Me.LayoutControlItem1.Text = "LayoutControlItem1"
            Me.LayoutControlItem1.TextSize = New System.Drawing.Size(0, 0)
            Me.LayoutControlItem1.TextToControlDistance = 0
            Me.LayoutControlItem1.TextVisible = False
            '
            'LayoutControlItem2
            '
            Me.LayoutControlItem2.AppearanceItemCaption.Options.UseTextOptions = True
            Me.LayoutControlItem2.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
            Me.LayoutControlItem2.AppearanceItemCaption.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Top
            Me.LayoutControlItem2.AppearanceItemCaption.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap
            Me.LayoutControlItem2.Control = Me.LookUpEdit1
            Me.LayoutControlItem2.CustomizationFormText = "Batch List"
            Me.LayoutControlItem2.Location = New System.Drawing.Point(0, 44)
            Me.LayoutControlItem2.Name = "LayoutControlItem2"
            Me.LayoutControlItem2.Padding = New DevExpress.XtraLayout.Utils.Padding(5, 5, 5, 5)
            Me.LayoutControlItem2.Size = New System.Drawing.Size(306, 30)
            Me.LayoutControlItem2.Text = "Batch List"
            Me.LayoutControlItem2.TextLocation = DevExpress.Utils.Locations.Top
            Me.LayoutControlItem2.TextSize = New System.Drawing.Size(0, 0)
            Me.LayoutControlItem2.TextToControlDistance = 0
            Me.LayoutControlItem2.TextVisible = False
            '
            'LayoutControlItem3
            '
            Me.LayoutControlItem3.Control = Me.SimpleButton1
            Me.LayoutControlItem3.CustomizationFormText = "LayoutControlItem3"
            Me.LayoutControlItem3.Location = New System.Drawing.Point(86, 101)
            Me.LayoutControlItem3.Name = "LayoutControlItem3"
            Me.LayoutControlItem3.Size = New System.Drawing.Size(79, 27)
            Me.LayoutControlItem3.Text = "LayoutControlItem3"
            Me.LayoutControlItem3.TextSize = New System.Drawing.Size(0, 0)
            Me.LayoutControlItem3.TextToControlDistance = 0
            Me.LayoutControlItem3.TextVisible = False
            '
            'LayoutControlItem4
            '
            Me.LayoutControlItem4.Control = Me.SimpleButton2
            Me.LayoutControlItem4.CustomizationFormText = "LayoutControlItem4"
            Me.LayoutControlItem4.Location = New System.Drawing.Point(165, 101)
            Me.LayoutControlItem4.Name = "LayoutControlItem4"
            Me.LayoutControlItem4.Size = New System.Drawing.Size(79, 27)
            Me.LayoutControlItem4.Text = "LayoutControlItem4"
            Me.LayoutControlItem4.TextSize = New System.Drawing.Size(0, 0)
            Me.LayoutControlItem4.TextToControlDistance = 0
            Me.LayoutControlItem4.TextVisible = False
            '
            'EmptySpaceItem1
            '
            Me.EmptySpaceItem1.CustomizationFormText = "EmptySpaceItem1"
            Me.EmptySpaceItem1.Location = New System.Drawing.Point(0, 74)
            Me.EmptySpaceItem1.Name = "EmptySpaceItem1"
            Me.EmptySpaceItem1.Size = New System.Drawing.Size(354, 27)
            Me.EmptySpaceItem1.Text = "EmptySpaceItem1"
            Me.EmptySpaceItem1.TextSize = New System.Drawing.Size(0, 0)
            '
            'EmptySpaceItem2
            '
            Me.EmptySpaceItem2.CustomizationFormText = "EmptySpaceItem2"
            Me.EmptySpaceItem2.Location = New System.Drawing.Point(257, 101)
            Me.EmptySpaceItem2.Name = "EmptySpaceItem2"
            Me.EmptySpaceItem2.Size = New System.Drawing.Size(97, 27)
            Me.EmptySpaceItem2.Text = "EmptySpaceItem2"
            Me.EmptySpaceItem2.TextSize = New System.Drawing.Size(0, 0)
            '
            'EmptySpaceItem3
            '
            Me.EmptySpaceItem3.CustomizationFormText = "EmptySpaceItem3"
            Me.EmptySpaceItem3.Location = New System.Drawing.Point(0, 101)
            Me.EmptySpaceItem3.Name = "EmptySpaceItem3"
            Me.EmptySpaceItem3.Size = New System.Drawing.Size(99, 27)
            Me.EmptySpaceItem3.Text = "EmptySpaceItem3"
            Me.EmptySpaceItem3.TextSize = New System.Drawing.Size(0, 0)
            '
            'SelectBatchForm
            '
            Me.AcceptButton = Me.SimpleButton1
            Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
            Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
            Me.CancelButton = Me.SimpleButton2
            Me.ClientSize = New System.Drawing.Size(326, 148)
            Me.Controls.Add(Me.LayoutControl1)
            Me.LookAndFeel.UseDefaultLookAndFeel = True
            Me.Name = "SelectBatchForm"
            Me.Text = "Select ACH Batch"
            CType(Me.DxErrorProvider1, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.LayoutControl1, System.ComponentModel.ISupportInitialize).EndInit()
            Me.LayoutControl1.ResumeLayout(False)
            CType(Me.LookUpEdit1.Properties, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.LayoutControlGroup1, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.LayoutControlItem1, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.LayoutControlItem2, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.LayoutControlItem3, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.LayoutControlItem4, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.EmptySpaceItem1, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.EmptySpaceItem2, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.EmptySpaceItem3, System.ComponentModel.ISupportInitialize).EndInit()
            Me.ResumeLayout(False)

        End Sub
        Friend WithEvents LayoutControl1 As DevExpress.XtraLayout.LayoutControl
        Friend WithEvents LayoutControlGroup1 As DevExpress.XtraLayout.LayoutControlGroup
        Friend WithEvents LookUpEdit1 As DevExpress.XtraEditors.LookUpEdit
        Friend WithEvents LabelControl1 As DevExpress.XtraEditors.LabelControl
        Friend WithEvents LayoutControlItem1 As DevExpress.XtraLayout.LayoutControlItem
        Friend WithEvents LayoutControlItem2 As DevExpress.XtraLayout.LayoutControlItem
        Friend WithEvents SimpleButton2 As DevExpress.XtraEditors.SimpleButton
        Friend WithEvents SimpleButton1 As DevExpress.XtraEditors.SimpleButton
        Friend WithEvents LayoutControlItem3 As DevExpress.XtraLayout.LayoutControlItem
        Friend WithEvents LayoutControlItem4 As DevExpress.XtraLayout.LayoutControlItem
        Friend WithEvents EmptySpaceItem1 As DevExpress.XtraLayout.EmptySpaceItem
        Friend WithEvents EmptySpaceItem2 As DevExpress.XtraLayout.EmptySpaceItem
        Friend WithEvents EmptySpaceItem3 As DevExpress.XtraLayout.EmptySpaceItem
    End Class
End Namespace