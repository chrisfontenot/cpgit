#Region "Copyright 2000-2012 DebtPlus, L.L.C."
'{*******************************************************************}
'{                                                                   }
'{       DebtPlus Debt Management System                             }
'{                                                                   }
'{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
'{       ALL RIGHTS RESERVED                                         }
'{                                                                   }
'{   The entire contents of this file is protected by U.S. and       }
'{   International Copyright Laws. Unauthorized reproduction,        }
'{   reverse-engineering, and distribution of all or any portion of  }
'{   the code contained in this file is strictly prohibited and may  }
'{   result in severe civil and criminal penalties and will be       }
'{   prosecuted to the maximum extent possible under the law.        }
'{                                                                   }
'{   RESTRICTIONS                                                    }
'{                                                                   }
'{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
'{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
'{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
'{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
'{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
'{                                                                   }
'{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
'{   ADDITIONAL RESTRICTIONS.                                        }
'{                                                                   }
'{*******************************************************************}

#End Region

Option Compare Binary
Option Explicit On
Option Strict On

Imports System.ComponentModel
Imports System.Windows.Forms
Imports DevExpress.XtraBars
Imports System.Data.SqlClient
Imports DebtPlus.Reports
Imports DebtPlus.Data.Forms
Imports DevExpress.XtraGrid.Views.Grid
Imports DevExpress.XtraGrid
Imports DevExpress.XtraGrid.Views.Grid.ViewInfo

Namespace ACH.Response.Manual
    Friend Class MainForm

        Private ap As ArgParser = Nothing

        Private WithEvents _EditTable As DataTable = Nothing
        Private Property EditTable As DataTable
            Get
                Return _EditTable
            End Get
            Set(value As DataTable)
                If _EditTable IsNot Nothing Then
                    RemoveHandler _EditTable.ColumnChanged, AddressOf EditTable_ColumnChanged
                End If
                _EditTable = value
                If _EditTable IsNot Nothing Then
                    AddHandler _EditTable.ColumnChanged, AddressOf EditTable_ColumnChanged
                End If
            End Set
        End Property

        Public Sub New(ByVal ap As ArgParser)
            MyClass.New()
            Me.ap = ap
        End Sub

        Public Sub New()
            MyBase.New()
            InitializeComponent()

            AddHandler BarButtonItem_Help_About.ItemClick, AddressOf BarButtonItem_Help_About_ItemClick
            AddHandler BarButtonItem_Reports_ErrorListing.ItemClick, AddressOf BarButtonItem_Reports_ErrorListing_ItemClick
            AddHandler BarButtonItem_File_Open.ItemClick, AddressOf BarButtonItem_File_Open_ItemClick
            AddHandler FormClosing, AddressOf MainForm_FormClosing
            AddHandler Load, AddressOf MainForm_Load
            AddHandler BarButtonItem_File_DataEntry.ItemClick, AddressOf BarButtonItem_File_DataEntry_ItemClick
            AddHandler BarButtonItem_File_Exit.ItemClick, AddressOf BarButtonItem_File_Exit_ItemClick
            AddHandler BarButtonItem_Reports_Transactions.ItemClick, AddressOf BarButtonItem_Reports_Transactions_ItemClick
            AddHandler GridView1.DoubleClick, AddressOf GridView1_DoubleClick

            ' Client formatter
            GridColumn_client.DisplayFormat.Format = New DebtPlus.Utils.Format.Client.CustomFormatter
            GridColumn_client.GroupFormat.Format = New DebtPlus.Utils.Format.Client.CustomFormatter

            ' Transaction code
            GridColumn_transaction_code.DisplayFormat.Format = New TransactionCode_Formatter
            GridColumn_transaction_code.GroupFormat.Format = New TransactionCode_Formatter
        End Sub

        Private Class TransactionCode_Formatter
            Implements IFormatProvider, ICustomFormatter

            Public Function GetFormat(ByVal formatType As Type) As Object Implements IFormatProvider.GetFormat
                Return Me
            End Function

            Public Function Format(ByVal format1 As String, ByVal arg As Object, ByVal formatProvider As IFormatProvider) As String Implements ICustomFormatter.Format
                Dim Value As Int32 = -1
                Dim answer As String = String.Empty
                If arg IsNot Nothing AndAlso arg IsNot DBNull.Value Then
                    Value = Convert.ToInt32(arg)
                    Select Case Value
                        Case 22, 23
                            answer = "CHECKING REFUND"
                        Case 23, 33
                            answer = "SAVINGS REFUND"
                        Case 27
                            answer = "CHECKING"
                        Case 37
                            answer = "SAVINGS"
                        Case 28
                            answer = "PRENOTE CHECKING"
                        Case 38
                            answer = "PRENOTE SAVINGS"
                        Case Else
                    End Select
                End If
                Return answer
            End Function
        End Class

        Private Sub MainForm_FormClosing(ByVal sender As Object, ByVal e As FormClosingEventArgs)
            ap.SaveChanges()
        End Sub

        Private Sub MainForm_Load(ByVal sender As Object, ByVal e As EventArgs)

            ' Load the list of reject codes into the editor control
            RepositoryItemLookUpEdit_authentication_code.DataSource = New DataView(ap.ACHRejectCodes, String.Empty, "ach_reject_code", DataViewRowState.CurrentRows)

            ' Read the data transactions
            ReadDatabase()
        End Sub

        Private Sub BarButtonItem_File_Open_ItemClick(ByVal sender As Object, ByVal e As ItemClickEventArgs)
            Dim NewBatch As Int32 = ap.RequestBatchID()
            If NewBatch > 0 Then
                ap.SaveChanges()

                ap.ProcessingBatch = NewBatch
                ReadDatabase()
            End If
        End Sub

        Private Sub BarButtonItem_File_DataEntry_ItemClick(ByVal sender As Object, ByVal e As ItemClickEventArgs)
            Using frm As New DataEntryForm(ap)
                frm.ShowDialog()
            End Using
            ap.SaveChanges()
        End Sub

        ''' <summary>
        ''' Process the FILE / EXIT operation
        ''' </summary>
        Private Sub BarButtonItem_File_Exit_ItemClick(ByVal sender As Object, ByVal e As ItemClickEventArgs)
            Close()
        End Sub

        Private Sub ReadDatabase()
            Const TableName As String = "xpr_ACH_file_contents"

            Dim cn As SqlConnection = New SqlConnection(DebtPlus.LINQ.SQLInfoClass.getDefault().ConnectionString)
            Try
                cn.Open()
                Using cmd As SqlCommand = New SqlCommand
                    With cmd
                        .Connection = cn
                        .CommandText = "xpr_ACH_file_contents"
                        .CommandType = CommandType.StoredProcedure
                        SqlCommandBuilder.DeriveParameters(cmd)
                        .Parameters(1).Value = ap.ProcessingBatch
                    End With

                    Using da As New SqlDataAdapter(cmd)
                        da.Fill(ap.ds, TableName)
                        EditTable = ap.ds.Tables(TableName)

                        With EditTable
                            If .PrimaryKey.GetUpperBound(0) < 0 Then
                                .PrimaryKey = New DataColumn() {.Columns("item_key")}
                            End If
                        End With
                    End Using
                End Using

                ' Update the datasoure for the grid
                With GridControl1
                    .DataSource = New DataView(EditTable, String.Format("[deposit_batch_id]={0:f0}", ap.ProcessingBatch), "trace_number", DataViewRowState.CurrentRows)
                    .RefreshDataSource()
                End With

            Catch ex As SqlException
                Using gdr = New Repository.GetDataResult()
                    gdr.HandleException(ex)
                    DebtPlus.UI.Common.ErrorHandling.HandleErrors(gdr, "Error reading transaction data")
                End Using
            End Try
        End Sub

        Private Sub BarButtonItem_Reports_Transactions_ItemClick(ByVal sender As Object, ByVal e As ItemClickEventArgs)
            ap.SaveChanges()
            PerformReport(New DebtPlus.Reports.ACH.Transactions.AchTransactionsReport())
        End Sub

        ''' <summary>
        ''' Process the error listing report
        ''' </summary>
        Private Sub BarButtonItem_Reports_ErrorListing_ItemClick(ByVal sender As Object, ByVal e As ItemClickEventArgs)
            PerformReport(New DebtPlus.Reports.ACH.Responses.AchResponsesReport())
        End Sub

        ''' <summary>
        ''' Generate a Report
        ''' </summary>
        ''' <param name="rpt">Pointer to the report to be run</param>
        ''' <remarks></remarks>
        Private Sub PerformReport(ByRef rpt As DebtPlus.Interfaces.Reports.IReports)
            rpt.SetReportParameter("DepositBatchID", ap.ProcessingBatch)
            rpt.RunReportInSeparateThread()
        End Sub

        ''' <summary>
        ''' Display the about form
        ''' </summary>
        Private Sub BarButtonItem_Help_About_ItemClick(ByVal sender As Object, ByVal e As ItemClickEventArgs)
            Using frm As New AboutBoxForm()
                frm.ShowDialog()
            End Using
        End Sub

        Private Sub GridView1_DoubleClick(ByVal sender As Object, ByVal e As EventArgs)
            ' Find the row targeted as the double-click item
            Dim gv As GridView = CType(sender, GridView)
            Dim ctl As GridControl = CType(gv.GridControl, GridControl)
            Dim hi As GridHitInfo = gv.CalcHitInfo((ctl.PointToClient(MousePosition)))
            Dim ControlRow As Int32 = hi.RowHandle

            ' Find the datarowview from the input tables.
            Dim drv As DataRowView = Nothing
            If ControlRow >= 0 Then
                Dim vue As DataView = CType(ctl.DataSource, DataView)
                drv = CType(gv.GetRow(ControlRow), DataRowView)
                If drv IsNot Nothing Then
                    EditItem(drv)
                End If
            End If
        End Sub

        Private Sub EditItem(ByVal drv As DataRowView)
            drv.BeginEdit()
            Using frm As New EditItemForm(ap, drv)
                With frm
                    Dim answer As DialogResult = .ShowDialog()
                    If answer = DialogResult.OK Then
                        drv.EndEdit()
                    Else
                        drv.CancelEdit()
                    End If
                End With
            End Using
        End Sub

        Private Sub EditTable_ColumnChanged(ByVal sender As Object, ByVal e As DataColumnChangeEventArgs)
            If e.Column.ColumnName = "authentication_code" Then
                Dim v As Object = e.Row(e.Column)
                If v IsNot Nothing AndAlso v IsNot DBNull.Value Then
                    If Convert.ToString(v) = String.Empty Then
                        e.Row(e.Column) = DBNull.Value
                    End If
                End If
            End If
        End Sub
    End Class
End Namespace
