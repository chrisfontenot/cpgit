#Region "Copyright 2000-2012 DebtPlus, L.L.C."
'{*******************************************************************}
'{                                                                   }
'{       DebtPlus Debt Management System                             }
'{                                                                   }
'{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
'{       ALL RIGHTS RESERVED                                         }
'{                                                                   }
'{   The entire contents of this file is protected by U.S. and       }
'{   International Copyright Laws. Unauthorized reproduction,        }
'{   reverse-engineering, and distribution of all or any portion of  }
'{   the code contained in this file is strictly prohibited and may  }
'{   result in severe civil and criminal penalties and will be       }
'{   prosecuted to the maximum extent possible under the law.        }
'{                                                                   }
'{   RESTRICTIONS                                                    }
'{                                                                   }
'{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
'{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
'{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
'{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
'{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
'{                                                                   }
'{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
'{   ADDITIONAL RESTRICTIONS.                                        }
'{                                                                   }
'{*******************************************************************}
#End Region

Option Compare Binary
Option Explicit On
Option Strict On

Imports System.Windows.Forms

Namespace ACH.Response.Manual

    Friend Class EditItemForm
        Private drv As System.Data.DataRowView
        Private ap As ArgParser

        Public Sub New(ByVal ap As ArgParser, ByVal drv As System.Data.DataRowView)
            MyBase.New()
            InitializeComponent()
            AddHandler Me.Load, AddressOf EditItemForm_Load
            Me.drv = drv
            Me.ap = ap
        End Sub

        Private Sub EditItemForm_Load(ByVal sender As Object, ByVal e As System.EventArgs)
            With LabelControl_amount
                .DataBindings.Clear()
                .DataBindings.Add("Text", drv, "amount", True, DataSourceUpdateMode.Never, Nothing, "c")
            End With

            With LabelControl_client
                .Text = String.Format("{0:0000000}", drv("client"))
            End With

            With LabelControl_trace_number
                .DataBindings.Clear()
                .DataBindings.Add("Text", drv, "trace_number")
            End With

            With LookUpEdit_authentication_code
                .Properties.DataSource = ap.ACHRejectCodes.DefaultView
                .DataBindings.Clear()
                .DataBindings.Add("EditValue", drv, "authentication_code")
            End With
        End Sub
    End Class
End Namespace

