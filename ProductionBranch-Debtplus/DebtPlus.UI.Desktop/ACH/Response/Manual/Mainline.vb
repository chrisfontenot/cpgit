#Region "Copyright 2000-2012 DebtPlus, L.L.C."
'{*******************************************************************}
'{                                                                   }
'{       DebtPlus Debt Management System                             }
'{                                                                   }
'{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
'{       ALL RIGHTS RESERVED                                         }
'{                                                                   }
'{   The entire contents of this file is protected by U.S. and       }
'{   International Copyright Laws. Unauthorized reproduction,        }
'{   reverse-engineering, and distribution of all or any portion of  }
'{   the code contained in this file is strictly prohibited and may  }
'{   result in severe civil and criminal penalties and will be       }
'{   prosecuted to the maximum extent possible under the law.        }
'{                                                                   }
'{   RESTRICTIONS                                                    }
'{                                                                   }
'{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
'{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
'{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
'{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
'{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
'{                                                                   }
'{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
'{   ADDITIONAL RESTRICTIONS.                                        }
'{                                                                   }
'{*******************************************************************}
#End Region

Option Compare Binary
Option Explicit On
Option Strict On

Imports DebtPlus.Interfaces.Desktop

Namespace ACH.Response.Manual
    Public Class Mainline
        Implements IDesktopMainline

        Public Sub OldAppMain(ByVal args() As String) Implements IDesktopMainline.OldAppMain
            Dim thrd As New System.Threading.Thread(New System.Threading.ParameterizedThreadStart(AddressOf ProcessThread))
            thrd.SetApartmentState(Threading.ApartmentState.STA)
            thrd.IsBackground = False
            thrd.Name = "ProcessMain"
            thrd.Start(args)
        End Sub

        Private Sub ProcessThread(ByVal obj As Object)
            Dim args() As String = CType(obj, String())

            Dim ap As New ArgParser()
            If ap.Parse(args) Then

                ' Request the batch ID if one is not given
                If ap.ProcessingBatch <= 0 Then
                    ap.ProcessingBatch = ap.RequestBatchID()
                End If

                ' If there is a batch then evoke the edit form
                If ap.ProcessingBatch > 0 Then
                    Dim mainForm = New MainForm(ap)
                    mainForm.ShowDialog()
                End If
            End If
        End Sub
    End Class
End Namespace