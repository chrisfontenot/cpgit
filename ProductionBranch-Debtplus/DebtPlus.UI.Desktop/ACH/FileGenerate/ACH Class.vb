#Region "Copyright 2000-2012 DebtPlus, L.L.C."
'{*******************************************************************}
'{                                                                   }
'{       DebtPlus Debt Management System                             }
'{                                                                   }
'{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
'{       ALL RIGHTS RESERVED                                         }
'{                                                                   }
'{   The entire contents of this file is protected by U.S. and       }
'{   International Copyright Laws. Unauthorized reproduction,        }
'{   reverse-engineering, and distribution of all or any portion of  }
'{   the code contained in this file is strictly prohibited and may  }
'{   result in severe civil and criminal penalties and will be       }
'{   prosecuted to the maximum extent possible under the law.        }
'{                                                                   }
'{   RESTRICTIONS                                                    }
'{                                                                   }
'{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
'{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
'{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
'{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
'{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
'{                                                                   }
'{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
'{   ADDITIONAL RESTRICTIONS.                                        }
'{                                                                   }
'{*******************************************************************}
#End Region
Imports DebtPlus.Utils
Imports System.Data.SqlClient
Imports System.Windows.Forms

Namespace ACH.FileGenerate

    Friend Class ACHClass

        ' Linkage to the database
        Private EffectiveDate As System.DateTime

        ' The deposit batch number for the system
        Private DepositFile As System.Int32 = -1

        ' Information from the banks table
        Private ach_priority As String = "01"
        Private immediate_destination As String = System.String.Empty
        Private immediate_destination_name As String = System.String.Empty
        Private immediate_origin As String = System.String.Empty
        Private immediate_origin_name As String = System.String.Empty
        Private reference_code As String = System.String.Empty
        Private originating_dfi_identification As String = System.String.Empty
        Private standard_entry_class As String = System.String.Empty
        Private company_entry_description As String = System.String.Empty
        Private prefix As String = System.String.Empty
        Private suffix As String = System.String.Empty
        Private BatchTraceNumber As System.Int32 = 0
        Private TransactionTraceNumber As System.Int32 = 0
        Private ach_message_authentication As String = System.String.Empty
        Private ach_company_identification As String = System.String.Empty

        ' Number of lines written to the output file
        Private LinesWritten As System.Int32 = 0

        ' Current date and time from the system
        Private CurrentDateTime As System.DateTime = Now

        ' Information for the total file
        Private FileBatches As System.Int32 = 0
        Private FileTransactions As System.Int32 = 0
        Private FileDebit As System.Int64 = 0
        Private FileCredit As System.Int64 = 0

        ' Information for the current batch
        Private BatchType As String = "PPD"
        Private BatchTransactions As System.Int32 = 0
        Private BatchDebit As System.Int64 = 0
        Private BatchCredit As System.Int64 = 0

        ' Dataset for the transactions. Used in generating the report if desired.
        Private ds As New System.Data.DataSet("ds")

        Dim erf As System.Text.EncoderReplacementFallback
        Dim drf As System.Text.DecoderReplacementFallback
        Dim ae As System.Text.Encoding

        ''' <summary>
        ''' File Hash total of the routing numbers
        ''' </summary>
        Private privateFileHash As System.Int64 = 0
        Private Property FileHash() As System.Int64
            Get
                Return privateFileHash
            End Get
            Set(ByVal value As System.Int64)
                Debug.Assert(value >= 0)
                If value >= 10000000000 Then
                    value -= 10000000000
                End If
                privateFileHash = value
            End Set
        End Property

        ''' <summary>
        ''' Batch Hash total of the routing numbers
        ''' </summary>
        Private privateBatchHash As System.Int64 = 0
        Private Property BatchHash() As System.Int64
            Get
                Return privateBatchHash
            End Get
            Set(ByVal value As System.Int64)
                Debug.Assert(value >= 0)
                If value >= 10000000000 Then
                    value -= 10000000000
                End If
                privateBatchHash = value
            End Set
        End Property

        ''' <summary>
        ''' Deposit batch number used for posting operations
        ''' </summary>
        Public ReadOnly Property DepositBatchID() As System.Int32
            Get
                Return DepositFile
            End Get
        End Property

        ''' <summary>
        ''' Return the number of type 6 records written to the file
        ''' </summary>
        Public ReadOnly Property ClientCount() As System.Int32
            Get
                Return FileTransactions
            End Get
        End Property

        ''' <summary>
        ''' Return the total amount of money pulled in the file
        ''' </summary>
        Public ReadOnly Property TotalDebit() As Decimal
            Get
                Return Convert.ToDecimal(FileDebit) / 100D
            End Get
        End Property

        Public Sub New()
            MyBase.New()

            ' Create the encoding classes
            erf = New System.Text.EncoderReplacementFallback(" ")
            drf = New System.Text.DecoderReplacementFallback(" ")
            ae = System.Text.Encoding.GetEncoding("us-ascii", erf, drf)
        End Sub

        ''' <summary>
        ''' Do the requested operation to generate the output text file
        ''' </summary>
        Public Function CreateACHFile(ByVal Schedules As String, ByVal EffectiveDateValue As System.DateTime, ByVal Bank As System.Int32, ByVal IncludePrenotes As Boolean, ByVal Filename As String, ByVal Note As String) As Boolean
            Dim successful As Boolean = False

            ' Save the effective dates for the batch headers
            EffectiveDate = EffectiveDateValue
            CreateACHFile = False

            ' Open the output file to hold the results
            Dim outputStream As New System.IO.FileStream(Filename, System.IO.FileMode.Create)
            Dim outputWriter As New System.IO.StreamWriter(outputStream)

            Try
                ' Create a transaction for the operation. We want everything to just go away on an error
                DepositFile = xpr_ach_start_file(Schedules, Filename, EffectiveDate, Bank, Note)

                ' Build the transactions into the system
                xpr_ach_generate_pull(Schedules, DepositFile, Bank)
                If IncludePrenotes Then xpr_ach_Generate_Prenote(DepositFile, Bank)

                ' Build the output file
                successful = TransactionWrapper(outputWriter, DepositFile, Bank)

            Catch ex As Exception
                DebtPlus.Data.Forms.MessageBox.Show(ex.ToString(), My.Resources.ErrorCreatingACHFile, MessageBoxButtons.OK, MessageBoxIcon.Hand, MessageBoxDefaultButton.Button1)

            Finally
                If outputWriter IsNot Nothing Then outputWriter.Close()
                If outputStream IsNot Nothing Then outputStream.Close()

                ' If we are not successful then discard the partial output file
                If Not successful Then System.IO.File.Delete(Filename)
            End Try

            Return successful
        End Function

        ''' <summary>
        ''' Process the main transaction data for the pull.
        ''' </summary>
        Private Function TransactionWrapper(ByVal OutputWriter As System.IO.TextWriter, ByVal DepositFile As System.Int32, ByVal Bank As System.Int32) As Boolean
            Dim successful As Boolean = True

            Dim txn As System.Data.SqlClient.SqlTransaction = Nothing
            Dim cn As SqlClient.SqlConnection = New SqlConnection(LibraryGlobals.SqlInfo.ConnectionString)
            Try
                cn.Open()
                txn = cn.BeginTransaction(IsolationLevel.RepeatableRead)

                ' Read the configuration information now
                xpr_ach_config(cn, txn, Bank)

                ' Read the list of items that are still pending for this file.
                Using cmd As SqlClient.SqlCommand = New SqlCommand
                    With cmd
                        .CommandText = "SELECT * FROM view_ACH_Pending_Transactions WHERE ach_file=@ach_file ORDER BY routing_number, account_number"
                        .Connection = cn
                        .Transaction = txn
                        .CommandTimeout = System.Math.Max(300, LibraryGlobals.SqlInfo.CommandTimeout)
                        .Parameters.Add("@ach_file", SqlDbType.Int).Value = DepositFile
                    End With

                    ' Dataset for the updated information
                    Using da As New SqlClient.SqlDataAdapter(cmd)
                        da.Fill(ds, "transactions")
                    End Using
                End Using

                ' Process the items in the dataset
                Dim vue As DataView = ds.Tables(0).DefaultView
                If vue.Count > 0 Then

                    ' Include the prefix line(s)
                    If prefix <> System.String.Empty Then OutputWriter.WriteLine(prefix)

                    ' Write the ACH header line
                    WriteFileHeaderLine(OutputWriter, DepositFile)

                    ' Process the transactions
                    Dim RecordIndex As System.Int32 = 0
                    Do While RecordIndex >= 0
                        WriteBatch(OutputWriter, vue, RecordIndex)
                    Loop

                    ' Write the ACH trailer line
                    WriteFileTrailerLine(OutputWriter)

                    ' Padd the file to a multiple of 10 lines
                    PaddOutputFile(OutputWriter)

                    ' Include the suffix line(s)
                    If suffix <> System.String.Empty Then OutputWriter.WriteLine(suffix)

                    ' Update the trace numbers in the banks table
                    If Not xpr_ach_trace_update(cn, txn, Bank) Then
                        DebtPlus.Data.Forms.MessageBox.Show(My.Resources.AnotherUserWasGeneratingAnACHFileAtTheSameTimeTheCurrentFileIsVoidedPleaseReExecuteTheExtractAgain, "Database changed", MessageBoxButtons.OK, MessageBoxIcon.Asterisk)
                        successful = False
                    Else
                        ' Perform the update to the database at this point
                        Using UpdateCmd As SqlClient.SqlCommand = New SqlCommand
                            With UpdateCmd
                                .CommandText = "UPDATE deposit_batch_details SET reference=@trace_number WHERE deposit_batch_detail=@deposit_batch_detail"
                                .Connection = cn
                                .Transaction = txn

                                .Parameters.Add("@trace_number", SqlDbType.VarChar, 16, "trace_number")
                                .Parameters.Add("@deposit_batch_detail", SqlDbType.Int, 4, "ach_transaction")
                            End With

                            ' Set the update command and perform the update to the database for the changed rows
                            Using da As New SqlClient.SqlDataAdapter
                                da.UpdateCommand = UpdateCmd
                                Dim UpdatedCount As System.Int32 = da.Update(ds.Tables(0))
                            End Using
                        End Using
                    End If

                    ' If we are successful then close out the transaction and commit the changes
                    txn.Commit()
                    txn.Dispose()
                    txn = Nothing
                End If

            Finally
                If txn IsNot Nothing Then
                    Try
                        txn.Rollback()
                    Catch exRollback As System.Exception
                    End Try
                    txn.Dispose()
                End If

                If cn IsNot Nothing Then cn.Dispose()
            End Try

            Return successful
        End Function

        ''' <summary>
        ''' Return a valid trace number for the next item
        ''' </summary>
        Private Function tr(ByVal InputValue As System.Int32) As System.Int32
            Return (InputValue Mod 10000000)
        End Function

        ''' <summary>
        ''' Create the new ACH file
        ''' </summary>
        Private Function xpr_ach_start_file(ByVal PullDates As String, ByVal Filename As String, ByVal EffectiveDate As System.DateTime, ByVal Bank As System.Int32, ByVal Note As String) As System.Int32
            Dim Result As System.Int32 = -1

            Dim NewFilename As String = Filename
            If NewFilename.Length > 80 Then NewFilename = NewFilename.Substring(NewFilename.Length - 80)

            ' Open the database connection to the store
            Dim cn As SqlClient.SqlConnection = New SqlConnection(LibraryGlobals.SqlInfo.ConnectionString)
            Dim current_cursor As System.Windows.Forms.Cursor = System.Windows.Forms.Cursor.Current
            Try
                System.Windows.Forms.Cursor.Current = Cursors.WaitCursor
                cn.Open()

                ' Try to find the directory from the data
                Using cmd As SqlClient.SqlCommand = New SqlCommand
                    With cmd
                        .CommandText = "xpr_ach_start_file"
                        .CommandType = CommandType.StoredProcedure
                        .Connection = cn

                        ' Find the parameters from the stored procedure
                        SqlClient.SqlCommandBuilder.DeriveParameters(cmd)

                        ' Set the parameter values accordingly
                        .Parameters("@PullDate").Value = Convert.ToDateTime(PullDates)
                        .Parameters("@Filename").Value = NewFilename
                        .Parameters("@EffectiveDate").Value = EffectiveDate
                        .Parameters("@bank").Value = Bank
                        If .Parameters.Contains("@Note") Then .Parameters("@Note").Value = Note

                        ' Create the batch
                        .ExecuteNonQuery()

                        ' The batch number is the returned parameter if we have no errors
                        Result = Convert.ToInt32(.Parameters("@RETURN_VALUE").Value)
                    End With
                End Using

            Finally
                If cn IsNot Nothing Then cn.Dispose()
                System.Windows.Forms.Cursor.Current = current_cursor
            End Try

            Return Result
        End Function

        ''' <summary>
        ''' Do the pull operation with the parameters previously set
        ''' </summary>
        Private Sub xpr_ach_generate_pull(ByVal PullDates As String, ByVal DepositBatchID As System.Int32, ByVal Bank As System.Int32)
            Dim cn As SqlClient.SqlConnection = New SqlConnection(LibraryGlobals.SqlInfo.ConnectionString)
            Dim current_cursor As System.Windows.Forms.Cursor = System.Windows.Forms.Cursor.Current
            Try
                System.Windows.Forms.Cursor.Current = Cursors.WaitCursor
                cn.Open()

                ' Try to find the directory from the data
                Using cmd As SqlClient.SqlCommand = New SqlCommand
                    With cmd
                        .CommandText = "xpr_ACH_Generate_Pull"
                        .CommandType = CommandType.StoredProcedure
                        .Connection = cn
                        .CommandTimeout = System.Math.Max(300, LibraryGlobals.SqlInfo.CommandTimeout)

                        ' The rowcount is the result
                        .Parameters.Add("@RETURN_VALUE", SqlDbType.Int, 4).Direction = ParameterDirection.ReturnValue

                        ' Add the parameters
                        .Parameters.Add("@ACH_Pull_Date", SqlDbType.DateTime).Value = Convert.ToDateTime(PullDates)
                        .Parameters.Add("@ach_file", SqlDbType.Int).Value = DepositBatchID
                        .Parameters.Add("@bank", SqlDbType.Int).Value = Bank

                        ' Create the batch
                        .ExecuteNonQuery()
                    End With
                End Using

            Finally
                If cn IsNot Nothing Then cn.Dispose()
                System.Windows.Forms.Cursor.Current = current_cursor
            End Try
        End Sub

        ''' <summary>
        ''' Include any pending prenotes into the output file
        ''' </summary>
        Private Sub xpr_ach_Generate_Prenote(ByVal DepositBatchID As System.Int32, ByVal Bank As System.Int32)
            Dim cn As SqlClient.SqlConnection = New SqlConnection(LibraryGlobals.SqlInfo.ConnectionString)
            Dim current_cursor As System.Windows.Forms.Cursor = System.Windows.Forms.Cursor.Current
            Try
                System.Windows.Forms.Cursor.Current = Cursors.WaitCursor
                cn.Open()

                ' Try to find the directory from the data
                Using cmd As SqlClient.SqlCommand = New SqlCommand
                    With cmd
                        .CommandText = "xpr_ACH_Generate_Prenote"
                        .CommandType = CommandType.StoredProcedure
                        .Connection = cn

                        ' The rowcount is the result
                        .Parameters.Add("RETURN", SqlDbType.Int, 4).Direction = ParameterDirection.ReturnValue

                        ' Add the parameters
                        .Parameters.Add("@ach_file", SqlDbType.Int).Value = DepositBatchID
                        .Parameters.Add("@bank", SqlDbType.Int).Value = Bank

                        ' Create the batch
                        .ExecuteNonQuery()
                    End With
                End Using

            Finally
                If cn IsNot Nothing Then cn.Dispose()
                System.Windows.Forms.Cursor.Current = current_cursor
            End Try
        End Sub

        ''' <summary>
        ''' Retrieve the configuration information
        ''' </summary>
        Private Sub xpr_ach_config(ByVal cn As SqlClient.SqlConnection, ByVal txn As SqlClient.SqlTransaction, ByVal bank As System.Int32)
            Dim rd As System.Data.IDataReader = Nothing
            Try
                ' Retrieve the parameters for the ACH file
                Using cmd As SqlClient.SqlCommand = New SqlCommand

                    With cmd
                        .CommandText = "xpr_ACH_config"
                        .CommandType = CommandType.StoredProcedure
                        .Connection = cn
                        .Transaction = txn
                        .Parameters.Add(New SqlClient.SqlParameter("@bank", SqlDbType.Int)).Value = bank
                        rd = .ExecuteReader(CommandBehavior.SingleRow)
                    End With
                End Using

                ' Process the fields in the result set to update the local storage
                If rd IsNot Nothing AndAlso rd.Read Then
                    For FieldNo As System.Int32 = rd.FieldCount - 1 To 0 Step -1
                        If Not rd.IsDBNull(FieldNo) Then
                            Select Case rd.GetName(FieldNo).ToLower
                                Case "priority_code"
                                    ach_priority = rd.GetString(FieldNo).Trim()
                                Case "immediate_destination"
                                    immediate_destination = rd.GetString(FieldNo).Trim()
                                Case "immediate_destination_name"
                                    immediate_destination_name = rd.GetString(FieldNo).Trim()
                                Case "immediate_origin"
                                    immediate_origin = rd.GetString(FieldNo).Trim()
                                Case "immediate_origin_name"
                                    immediate_origin_name = rd.GetString(FieldNo).Trim()
                                Case "reference_code"
                                    reference_code = rd.GetString(FieldNo).Trim()
                                Case "originating_dfi_identification"
                                    originating_dfi_identification = rd.GetString(FieldNo).Trim()
                                Case "standard_entry_class"
                                    standard_entry_class = rd.GetString(FieldNo).Trim()
                                Case "company_entry_description"
                                    company_entry_description = rd.GetString(FieldNo).Trim()
                                Case "prefix"
                                    prefix = rd.GetString(FieldNo).Trim()
                                Case "suffix"
                                    suffix = rd.GetString(FieldNo).Trim()
                                Case "batch_number"
                                    BatchTraceNumber = rd.GetInt32(FieldNo)
                                Case "transaction_number"
                                    TransactionTraceNumber = rd.GetInt32(FieldNo)
                                Case "ach_message_authentication"
                                    ach_message_authentication = rd.GetString(FieldNo).Trim()
                                Case "ach_company_identification"
                                    ach_company_identification = rd.GetString(FieldNo).Trim()
                                Case "current_date_time"
                                    CurrentDateTime = rd.GetDateTime(FieldNo)
                            End Select
                        End If
                    Next FieldNo
                End If

            Finally
                If rd IsNot Nothing AndAlso Not rd.IsClosed Then rd.Close()
            End Try
        End Sub

        ''' <summary>
        ''' Write the ACH file header line
        ''' </summary>
        Private Sub WriteFileHeaderLine(ByVal OutputWriter As System.IO.TextWriter, ByVal ach_file_number As System.Int32)
            Dim sb As New System.Text.StringBuilder(100)

            sb.Append("1")                                              ' record type
            sb.Append(ach_priority.PadLeft(2, "0"c))                    ' priority
            sb.Append(immediate_destination.PadLeft(10, " "c))          ' immediate destination
            sb.Append(immediate_origin.PadRight(10, "0"c))              ' immediate origin
            sb.AppendFormat("{0:yyMMddHHmm}", CurrentDateTime)          ' Current date and time
            sb.Append("1")                                              ' Record type = 1 (header)
            sb.Append("094")                                            ' record size
            sb.Append("10")                                             ' number of records in a block
            sb.Append("1")                                              '
            sb.Append(immediate_destination_name.PadRight(23).Substring(0, 23))          ' Immediate destination name
            sb.Append(immediate_origin_name.PadRight(23).Substring(0, 23))               ' Immediate origin name
            sb.AppendFormat("{0:00000000}", ach_file_number)                  ' Our output file number

            Debug.Assert(sb.Length = 94)                                ' We must have 94 bytes EXACTLY

            OutputWriter.WriteLine(sb.ToString())                         ' Write the output line
            LinesWritten += 1                                           ' and count it for the blocking factor

            FileHash = 0                                                ' Generate the new hash key and set it to zeros
            FileBatches = 0                                             ' Total number of batches
            FileTransactions = 0                                        ' Total number of transactions
            FileDebit = 0                                               ' Total debit amount (client's total deposit)
            FileCredit = 0                                              ' Total credit amount (used if offset deposit only)
        End Sub

        ''' <summary>
        ''' Write the ACH file trailer line
        ''' </summary>
        Private Sub WriteFileTrailerLine(ByVal OutputWriter As System.IO.TextWriter)
            Dim sb As New System.Text.StringBuilder(100)
            sb.Append("9")                                              ' record type

            ' Find the number of blocks written.
            Dim Quotient As System.Int32, Remainder As System.Int32
            Quotient = System.Math.DivRem(LinesWritten, 10, Remainder)

            sb.AppendFormat("{0:000000}", FileBatches)                  ' Number of batches written
            sb.AppendFormat("{0:000000}", Quotient + 1)                 ' Number of blocks written
            sb.AppendFormat("{0:00000000}", FileTransactions)           ' Number of detail lines written
            sb.AppendFormat("{0:0000000000}", FileHash)                 ' Hash sequence
            sb.AppendFormat("{0:000000000000}", FileDebit)              ' Total debit amount
            sb.AppendFormat("{0:000000000000}", FileCredit)             ' Total credit amount
            sb.Append(New String(" "c, 39))                             ' Blank fill remainder of the record

            Debug.Assert(sb.Length = 94)                                ' We must have 94 bytes EXACTLY

            OutputWriter.WriteLine(sb.ToString())                         ' Write the output line
            LinesWritten += 1                                           ' and count it for the blocking factor
        End Sub

        ''' <summary>
        ''' Record the values for the new transactoun counts
        ''' </summary>
        Private Function xpr_ach_trace_update(ByVal cn As SqlClient.SqlConnection, ByVal txn As SqlClient.SqlTransaction, ByVal Bank As System.Int32) As Boolean
            Dim RowsUpdated As System.Int32 = 0

            Using cmd As SqlClient.SqlCommand = New SqlCommand
                With cmd
                    .Connection = cn
                    .Transaction = txn
                    .CommandText = "UPDATE banks SET transaction_number = @new_transaction_number, batch_number = @new_batch_number WHERE bank = @bank AND transaction_number = @old_transaction_number AND batch_number = @old_batch_number"
                    .CommandType = CommandType.Text

                    .Parameters.Add("@bank", SqlDbType.Int).Value = Bank
                    .Parameters.Add("@old_transaction_number", SqlDbType.Int).Value = TransactionTraceNumber
                    .Parameters.Add("@old_batch_number", SqlDbType.Int).Value = BatchTraceNumber
                    .Parameters.Add("@new_transaction_number", SqlDbType.Int).Value = tr(TransactionTraceNumber + FileTransactions)
                    .Parameters.Add("@new_batch_number", SqlDbType.Int).Value = tr(BatchTraceNumber + FileBatches)
                    RowsUpdated = .ExecuteNonQuery()
                End With
            End Using

            Return RowsUpdated = 1
        End Function

        ''' <summary>
        ''' Ensure that the output file is a multiple of 10 lines
        ''' </summary>
        Private Sub PaddOutputFile(ByVal OutputWriter As System.IO.TextWriter)

            ' Write the padding lines (94 bytes of "9"s) until we have multiple of 10 lines
            Do While LinesWritten Mod 10 <> 0
                OutputWriter.WriteLine(New String("9"c, 94))
                LinesWritten += 1
            Loop
        End Sub

        ''' <summary>
        ''' Write the batch transactions to the output file
        ''' </summary>
        Private Sub WriteBatch(ByVal OutputWriter As System.IO.TextWriter, ByRef vue As DataView, ByRef RecordIndex As System.Int32)

            ' Find the first pointer to the row
            Dim drv As DataRowView = vue(RecordIndex)

            ' Generate the batch header
            BatchType = standard_entry_class ' Convert.ToString(drv.Item("record_type"))
            WriteBatchHeader(OutputWriter, BatchType)

            ' Process the records until we are at the end or the batch type changes
            Do
                WriteTransaction(OutputWriter, drv)

                ' If this is the eof then indicate that we have reached the end
                RecordIndex += 1
                If RecordIndex >= vue.Count Then
                    RecordIndex = -1
                    Exit Do
                End If

                ' Go on to the next record and look for a change in the type
                drv = vue(RecordIndex)
                'If Convert.ToString(drv.Item("record_type")) <> BatchType Then Exit Do
            Loop

            ' Write the trailer when the type changes or we reach EOF
            WriteBatchTrailer(OutputWriter)
        End Sub

        ''' <summary>
        ''' Convert the relative trace number to a proper string
        ''' </summary>
        Private Function TraceNumber(ByVal TraceNumberDesired As System.Int32) As String

            ' We need exactly 8 characters from the identification string. No more. No less.
            Dim ident As String = originating_dfi_identification
            If ident.Length > 8 Then ident = ident.Substring(ident.Length - 8)

            Return System.String.Format("{0:s8}{1:0000000}", ident, tr(TraceNumberDesired))
        End Function

        ''' <summary>
        ''' Write a batch header
        ''' </summary>
        Private Sub WriteBatchHeader(ByVal OutputWriter As System.IO.TextWriter, ByVal Type As String)
            Dim sb As New System.Text.StringBuilder(100)

            sb.Append("5")                                              ' record type
            sb.Append("200")                                            ' record subtype
            sb.Append(immediate_origin_name.PadRight(36).Substring(0, 36))  ' immediate origin name
            sb.Append(ach_company_identification.PadLeft(10, "0"c))     ' Company identification
            sb.Append(Type.PadRight(3, " "c))                           ' Batch type
            sb.Append(company_entry_description.PadRight(10))           ' description
            sb.AppendFormat("{0:yyMMdd}", EffectiveDate)                ' Company descriptive date
            sb.AppendFormat("{0:yyMMdd}", EffectiveDate)                ' Effective date
            sb.Append(New String(" "c, 3))                              ' Filler
            sb.Append("1")                                              ' Batch type
            sb.Append(TraceNumber(FileBatches + BatchTraceNumber))      ' Batch trace number

            Debug.Assert(sb.Length = 94)                                ' We must have 94 bytes EXACTLY

            OutputWriter.WriteLine(sb.ToString())                         ' Write the output line
            LinesWritten += 1                                           ' and count it for the blocking factor

            ' Clear the batch counters
            BatchDebit = 0
            BatchCredit = 0
            BatchTransactions = 0
            BatchHash = 0
        End Sub

        ''' <summary>
        ''' Write a batch trailer
        ''' </summary>
        Private Sub WriteBatchTrailer(ByVal OutputWriter As System.IO.TextWriter)
            Dim sb As New System.Text.StringBuilder(100)

            sb.Append("8")                                              ' record type
            sb.Append("200")
            sb.AppendFormat("{0:000000}", BatchTransactions)            ' Number of transactions
            sb.AppendFormat("{0:0000000000}", BatchHash)                ' Hash sequence
            sb.AppendFormat("{0:000000000000}", BatchDebit)             ' Total debit amount
            sb.AppendFormat("{0:000000000000}", BatchCredit)            ' Total credit amount
            sb.Append(ach_company_identification.PadRight(10))          ' Company identification
            sb.Append(ach_message_authentication.PadRight(19))          ' Message authentication
            sb.Append(New String(" "c, 6))
            sb.Append(TraceNumber(FileBatches + BatchTraceNumber))      ' Batch trace number

            Debug.Assert(sb.Length = 94)                                ' We must have 94 bytes EXACTLY

            OutputWriter.WriteLine(sb.ToString())                         ' Write the output line
            LinesWritten += 1                                           ' and count it for the blocking factor

            ' Count this batch
            FileBatches += 1

            ' Add the information to the file totals
            FileCredit += BatchCredit
            FileDebit += BatchDebit
            FileTransactions += BatchTransactions
            FileHash += BatchHash
        End Sub

        ''' <summary>
        ''' Write a transaction
        ''' </summary>
        Private Sub WriteTransaction(ByVal OutputWriter As System.IO.TextWriter, ByVal drv As DataRowView)

            ' These fields are used later. However, for errors, I want them decoded now.
            Dim MyAccountNumber As String = clean_string(drv("account_number")).PadRight(17).Substring(0, 17)
            Dim MyClientID As String = String.Format("{0:0000000}", Convert.ToInt32(drv("client"))).PadRight(15).Substring(0, 15)
            Dim MyClientName As String = clean_string(drv("client_name")).PadRight(22).Substring(0, 22)
            Dim RoutingString As String = Convert.ToString(drv("routing_number"))

            ' Make the amount a number of cents rather than cents/dollars
            Dim Amount As System.Int32 = Convert.ToInt32(System.Decimal.Floor(Convert.ToDecimal(drv("amount")) * 100D))

            ' Adjust the total information
            Select Case Convert.ToString(drv("transaction_code"))
                Case "22", "32"
                    BatchCredit += Amount
                Case "27", "37"
                    BatchDebit += Amount
            End Select

            ' The routing number must be exactly nine digits in length to be valid here.
            If RoutingString.Length <> 9 Then
                Throw New ApplicationException(String.Format("ABA number for client {0} is incorrect.", MyClientID))
            End If

            ' Remove the last digit from the routine number for the checksum algorithm. It is supposed to be only 8 digits
            ' in length. The 9th is the checksum of the first 8 and is not included in the checksum here.
            BatchHash += Int64.Parse(RoutingString.Substring(0, 8))

            Dim sb As New System.Text.StringBuilder(100)

            sb.Append("6")                                              ' Transaction code
            sb.Append(Convert.ToString(drv("transaction_code")).PadLeft(2))         ' Transaction type
            sb.Append(RoutingString)                                    ' Routing number (already known to be 9 digits)

            sb.Append(MyAccountNumber)                                  ' Account number

            sb.AppendFormat("{0:0000000000}", Amount)                   ' Amount (in cents)
            sb.Append(MyClientID)                                       ' client ID
            sb.Append(MyClientName)                                     ' client name

            sb.Append("  ")
            sb.Append("0")

            Dim NewTraceNumber As String = TraceNumber(BatchTransactions + TransactionTraceNumber + FileTransactions)
            sb.Append(NewTraceNumber)

            ' Update the row with the trace number
            drv.BeginEdit()
            drv("trace_number") = NewTraceNumber
            drv.EndEdit()

            ' Write the text record
            Debug.Assert(sb.Length = 94)                                ' We must have 94 bytes EXACTLY

            OutputWriter.WriteLine(sb.ToString())                         ' Write the output line
            LinesWritten += 1                                           ' and count it for the blocking factor

            ' Count this transaction
            BatchTransactions += 1
        End Sub

        ''' <summary>
        ''' Ensure message does not have funny characters
        ''' </summary>
        Private Function clean_string(ByVal message As Object) As String

            ' Extract the string from the input buffer
            Dim StrItem As String = String.Empty
            If message IsNot Nothing AndAlso message IsNot System.DBNull.Value Then
                StrItem = Convert.ToString(message, System.Globalization.CultureInfo.InvariantCulture).Trim()
            End If

            ' Find the length of the input stream. It should be the number of bytes for the input.
            Dim Length As System.Int32 = ae.GetByteCount(StrItem)
            If Length < 1 Then
                Return String.Empty
            End If

            ' Convert the string to a list of bytes. This removes the non-ASCII characters.
            Dim ab(Length - 1) As Byte
            ae.GetBytes(StrItem, 0, StrItem.Length, ab, 0)

            ' Convert the bytes back to a string as ASCII characters
            Dim Pass1 As String = ae.GetString(ab)

            ' Now, remove any objectionable ASCII characters from the string. This is now a simple string, not unicode.
            Dim Result As String = System.Text.RegularExpressions.Regex.Replace(Pass1, "[\x00-\x1F\x7F]", " ")
            Return Result
        End Function
    End Class
End Namespace
