#Region "Copyright 2000-2012 DebtPlus, L.L.C."
'{*******************************************************************}
'{                                                                   }
'{       DebtPlus Debt Management System                             }
'{                                                                   }
'{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
'{       ALL RIGHTS RESERVED                                         }
'{                                                                   }
'{   The entire contents of this file is protected by U.S. and       }
'{   International Copyright Laws. Unauthorized reproduction,        }
'{   reverse-engineering, and distribution of all or any portion of  }
'{   the code contained in this file is strictly prohibited and may  }
'{   result in severe civil and criminal penalties and will be       }
'{   prosecuted to the maximum extent possible under the law.        }
'{                                                                   }
'{   RESTRICTIONS                                                    }
'{                                                                   }
'{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
'{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
'{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
'{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
'{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
'{                                                                   }
'{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
'{   ADDITIONAL RESTRICTIONS.                                        }
'{                                                                   }
'{*******************************************************************}
#End Region

Option Compare Binary
Option Explicit On
Option Strict On

Imports DebtPlus.UI.Common
Imports System.Windows.Forms

Namespace ACH.FileGenerate
    Friend Class MainForm
        Inherits DebtPlus.Data.Forms.DebtPlusForm

        Private WithEvents bt As New System.ComponentModel.BackgroundWorker
        Private ReadOnly BankDS As New DataSet("BankDS")

        Private m_mySchedules As String
        Private m_myEffectiveDate As DateTime
        Private m_myBank As Int32
        Private m_myIncludePrenotes As Boolean
        Private m_myFilename As String
        Private m_achCreateClass As ACHClass
        Private m_wt As DevExpress.Utils.WaitDialogForm

        ''' <summary>
        ''' Initialize the new class
        ''' </summary>
        Public Sub New()
            MyBase.New()
            InitializeComponent()
            RegisterHandlers()
        End Sub

        ''' <summary>
        ''' Process the load event for the form
        ''' </summary>
        Private Sub BankLookup_Load()
            Const tableName As String = "banks"

            Dim gdr As Repository.GetDataResult = Repository.LookupTables.Banks.GetAll(BankDS, tableName)
            If Not gdr.Success Then
                DebtPlus.UI.Common.ErrorHandling.HandleErrors(gdr)
            Else
                Dim tbl As DataTable = BankDS.Tables(tableName)

                ' Bind the data to the list
                Dim vue As DataView = New System.Data.DataView(tbl, "[ActiveFlag]<>0 AND [type]='A'", "description", DataViewRowState.CurrentRows)

                ' Bind the office list to the data
                BankLookup.Properties.DataSource = vue
                BankLookup.Properties.Columns.AddRange(New DevExpress.XtraEditors.Controls.LookUpColumnInfo() {New DevExpress.XtraEditors.Controls.LookUpColumnInfo("bank", "ID", 25, DevExpress.Utils.FormatType.Numeric, "f0", True, DevExpress.Utils.HorzAlignment.Far), New DevExpress.XtraEditors.Controls.LookUpColumnInfo("description", "Description", 50, DevExpress.Utils.FormatType.None, String.Empty, True, DevExpress.Utils.HorzAlignment.Near), New DevExpress.XtraEditors.Controls.LookUpColumnInfo("formatted_check", "Type", 25, DevExpress.Utils.FormatType.None, String.Empty, True, DevExpress.Utils.HorzAlignment.Near)})
                BankLookup.Properties.DisplayMember = "description"
                BankLookup.Properties.ValueMember = "bank"
                BankLookup.Properties.NullText = String.Empty
                BankLookup.Properties.PopupWidth = BankLookup.Width + 100           ' Add some extra space for the dropdown width

                ' Set the default item if possible
                Dim rows() As System.Data.DataRow = tbl.Select(vue.RowFilter + " AND [Default]<>0", "bank")
                If rows.GetUpperBound(0) >= 0 Then
                    BankLookup.EditValue = rows(0)("bank")
                End If

                ' If there is no bank selected and there are banks then select the first bank in the list.
                If BankLookup.EditValue Is Nothing AndAlso vue.Count > 0 Then BankLookup.EditValue = vue(0)("bank")
            End If
        End Sub

        ''' <summary>
        ''' When the item has been changed, update filename
        ''' </summary>
        Private Sub BankLookup_EditValueChanged(ByVal sender As Object, ByVal e As System.EventArgs)
            UpdateFilenameFromBanks()
        End Sub

        ''' <summary>
        ''' Retrieve the list of schedules that are checked
        ''' </summary>
        Private ReadOnly Property Schedules() As String
            Get
                Return Convert.ToDateTime(PullDateEdit.EditValue).ToShortDateString
            End Get
        End Property

        ''' <summary>
        ''' Register the event handlers
        ''' </summary>
        Private Sub RegisterHandlers()
            AddHandler Me.Resize, AddressOf MainForm_Resize
            AddHandler Me.Load, AddressOf MainForm_Load
            AddHandler Me.FormClosing, AddressOf MainForm_FormClosing
            AddHandler bt.RunWorkerCompleted, AddressOf bt_RunWorkerCompleted
            AddHandler bt.DoWork, AddressOf bt_DoWork
            AddHandler OKButton.Click, AddressOf OKButton_Click
            AddHandler CancelBtn.Click, AddressOf CancelBtn_Click
            AddHandler EffectiveDateEdit.Validating, AddressOf EffectiveDateEdit_Validating
            AddHandler FileNameText.ButtonClick, AddressOf FileNameText_ButtonClick
            AddHandler FileNameText.TextChanged, AddressOf FileNameText_TextChanged
            AddHandler BankLookup.EditValueChanging, AddressOf DebtPlus.Data.Validation.LookUpEdit_ActiveTest
            AddHandler BankLookup.EditValueChanged, AddressOf BankLookup_EditValueChanged
        End Sub

        ''' <summary>
        ''' Remove the event handler registrations
        ''' </summary>
        Private Sub UnRegisterHandlers()
            RemoveHandler Me.Resize, AddressOf MainForm_Resize
            RemoveHandler Me.Load, AddressOf MainForm_Load
            RemoveHandler Me.FormClosing, AddressOf MainForm_FormClosing
            RemoveHandler bt.RunWorkerCompleted, AddressOf bt_RunWorkerCompleted
            RemoveHandler bt.DoWork, AddressOf bt_DoWork
            RemoveHandler OKButton.Click, AddressOf OKButton_Click
            RemoveHandler CancelBtn.Click, AddressOf CancelBtn_Click
            RemoveHandler EffectiveDateEdit.Validating, AddressOf EffectiveDateEdit_Validating
            RemoveHandler FileNameText.ButtonClick, AddressOf FileNameText_ButtonClick
            RemoveHandler FileNameText.TextChanged, AddressOf FileNameText_TextChanged
            RemoveHandler BankLookup.EditValueChanging, AddressOf DebtPlus.Data.Validation.LookUpEdit_ActiveTest
            RemoveHandler BankLookup.EditValueChanged, AddressOf BankLookup_EditValueChanged
        End Sub

        Private Sub MainForm_Load(ByVal sender As Object, ByVal e As System.EventArgs)
            UnRegisterHandlers()
            Try
                BankLookup_Load()
                PullDateControl_Load()
                EffectiveDateControl_Load()
                FileNameText_Load()
                EnableOK()
            Finally
                RegisterHandlers()
            End Try
        End Sub

        ''' <summary>
        ''' Process the CANCEL button CLICK event
        ''' </summary>
        Private Sub CancelBtn_Click(ByVal sender As Object, ByVal e As System.EventArgs)
            Close()
        End Sub

        ''' <summary>
        ''' Load the default information for the pull date
        ''' </summary>
        Private Sub PullDateControl_Load()
            PullDateEdit.Properties.NullText = String.Empty
            PullDateEdit.Properties.NullDate = Nothing
            PullDateEdit.Properties.ShowToday = True
            PullDateEdit.Properties.ShowClear = False
            PullDateEdit.Properties.MinValue = New System.DateTime(System.DateTime.Now.Year, System.DateTime.Now.Month, System.DateTime.Now.Day, 0, 0, 0)
            PullDateEdit.EditValue = PullDateEdit.Properties.MinValue
        End Sub

        ''' <summary>
        ''' Load the default information for the effective date
        ''' </summary>
        Private Sub EffectiveDateControl_Load()

            ' Set the normal properties for the control
            EffectiveDateEdit.Properties.NullText = String.Empty
            EffectiveDateEdit.Properties.NullDate = Nothing
            EffectiveDateEdit.Properties.ShowToday = False
            EffectiveDateEdit.Properties.ShowClear = False

            Using cm As New CursorManager()
                Using gsr As Repository.GetScalarResult = Repository.LookupTables.Calendar.GetAchEffectiveDate()
                    If gsr.Success Then
                        EffectiveDateEdit.EditValue = gsr.Result
                    Else
                        DebtPlus.UI.Common.ErrorHandling.HandleErrors(gsr, "Error reading calendar table")
                        Return
                    End If
                End Using
            End Using

            ' The minimum date is the starting date.
            EffectiveDateEdit.Properties.MinValue = Convert.ToDateTime(EffectiveDateEdit.EditValue)
        End Sub

        ''' <summary>
        ''' Validate the effective date for the ACH deposit
        ''' </summary>
        Private Sub EffectiveDateEdit_Validating(ByVal sender As Object, ByVal e As System.ComponentModel.CancelEventArgs)
            Dim errorMessage As String = String.Empty
            Dim IsWeekday As Boolean = False
            Dim IsBankHoliday As Boolean = False
            Dim HolidayDescription As String = String.Empty

            Using cm As New CursorManager()
                Using gdr As Repository.GetDataResult = Repository.LookupTables.Calendar.DidGetAchEffectiveDateInformation(Convert.ToDateTime(EffectiveDateEdit.EditValue), IsWeekday, IsBankHoliday, HolidayDescription)

                    ' If not successful then complain
                    If Not gdr.Success Then
                        errorMessage = "Error reading calendar table"
                        DebtPlus.UI.Common.ErrorHandling.HandleErrors(gdr, errorMessage)

                    Else

                        ' If there are no rows read because the date is not defined...
                        If gdr.RowsAffected = 0 Then
                            errorMessage = My.Resources.DateNotDefinedInSystem
                        Else
                            ' Otherwise, look at the flags to determine the status of the date
                            If Not IsWeekday Then
                                errorMessage = My.Resources.MustBeMONDAYThroughFRIDAY

                                ' If the date is tagged a holiday then there should be a status.
                            ElseIf IsBankHoliday Then
                                If HolidayDescription <> String.Empty Then
                                    errorMessage = String.Format(My.Resources.CanNotBeOn0, HolidayDescription)
                                Else
                                    errorMessage = My.Resources.MustNotBeOnABankingHoliday
                                End If
                            End If
                        End If
                    End If
                End Using
            End Using

            DxErrorProvider1.SetError(EffectiveDateEdit, errorMessage)
            EnableOK()
        End Sub

        ''' <summary>
        ''' Enable or disable the OK button
        ''' </summary>
        Private Sub EnableOK()
            If Schedules = System.String.Empty Then
                OKButton.Enabled = False
                Return
            End If

            If BankLookup.EditValue Is Nothing Then
                OKButton.Enabled = False
                Return
            End If

            If FileNameText.Text = System.String.Empty Then
                OKButton.Enabled = False
                Return
            End If

            If DxErrorProvider1.GetError(EffectiveDateEdit) <> System.String.Empty Then
                OKButton.Enabled = False
                Return
            End If

            ' There are no pending DebtPlus.Data.Errors. Enable the OK button to start processing
            OKButton.Enabled = True
        End Sub

        ''' <summary>
        ''' Process the OK button click event
        ''' </summary>
        Private Sub OKButton_Click(ByVal sender As Object, ByVal e As System.EventArgs)

            ' Allocate the class to process the requests and update the parameters
            m_achCreateClass = New ACHClass()
            m_mySchedules = Schedules
            m_myEffectiveDate = Convert.ToDateTime(EffectiveDateEdit.EditValue)
            m_myBank = Convert.ToInt32(BankLookup.EditValue)
            m_myIncludePrenotes = (IncludePrenotesCheckbox.CheckState = CheckState.Checked)
            m_myFilename = FileNameText.Text.Trim()

            ' Display the waiting dialog
            m_wt = New DevExpress.Utils.WaitDialogForm("Please wait.", "Creating the ACH batch.")
            m_wt.Show()

            ' Starthe background thread
            OKButton.Enabled = False
            bt.RunWorkerAsync()
        End Sub

        Private Sub FileNameText_Load()
            Dim fileName As String = System.String.Format(My.Resources.ACH0YyMMddHHmmTxt, Now())
            FileNameText.Text = fileName
            UpdateFilenameFromBanks()
        End Sub

        ''' <summary>
        ''' Merge the directory with the filename portion
        ''' </summary>
        Private Sub UpdateFilenameFromBanks()
            If BankLookup.EditValue IsNot Nothing Then

                ' Find the current bank and from that, the default directory
                Dim bank As Int32 = Convert.ToInt32(BankLookup.EditValue)
                Dim directory As String = DefaultBankDirectory(bank)

                ' Merge the new directory into the existing filename
                If directory <> System.String.Empty Then
                    FileNameText.Text = IO.Path.Combine(directory, IO.Path.GetFileName(FileNameText.Text.Trim()))
                End If
            End If
        End Sub

        ''' <summary>
        ''' Process the button on the filename field click event
        ''' </summary>
        Private Sub FileNameText_ButtonClick(ByVal sender As Object, ByVal e As DevExpress.XtraEditors.Controls.ButtonPressedEventArgs)
            Dim newFileName As String = System.String.Empty

            ' Determine the type of the button. If lookup, do the filename lookup function.
            With CType(sender, DevExpress.XtraEditors.ButtonEdit)
                Select Case CType(e.Button.Tag, String)

                    ' Do the filename lookup
                    Case "lookup"
                        Using frm As New SaveFileDialog()
                            frm.FileName = CType(sender, DevExpress.XtraEditors.ButtonEdit).Text
                            frm.AddExtension = True
                            frm.CheckFileExists = False
                            frm.CheckPathExists = True
                            frm.CreatePrompt = False
                            frm.DefaultExt = ".txt"
                            frm.Filter = My.Resources.TextFilesTxtTxtAllFiles
                            frm.FilterIndex = 0
                            frm.OverwritePrompt = True
                            frm.RestoreDirectory = True
                            frm.Title = My.Resources.ACHOutputFile
                            frm.ValidateNames = True

                            If frm.ShowDialog() = DialogResult.OK Then
                                newFileName = frm.FileName()
                            End If
                        End Using

                    Case Else
                        Debug.Assert(False, My.Resources.ButtonTypeIsNotDefinedForFileNameTextField)
                End Select

                ' Update the control with the new filename if one is used
                If newFileName <> System.String.Empty Then
                    .Text = newFileName.Trim()
                End If
            End With
        End Sub

        ''' <summary>
        ''' Handle the change in the filename
        ''' </summary>
        Private Sub FileNameText_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs)
            EnableOK()
        End Sub

        ''' <summary>
        ''' Find the default directory from the banks table
        ''' </summary>
        Private Function DefaultBankDirectory(ByVal bank As Int32) As String
            Dim result As String = System.String.Empty

            ' Find the bank from the banks table
            If bank > 0 Then
                Dim tbl As DataTable = BankDS.Tables(0)
                Dim row As DataRow = tbl.Rows.Find(bank)
                If row IsNot Nothing Then
                    If row("output_directory") IsNot Nothing AndAlso row("output_directory") IsNot DBNull.Value Then result = Convert.ToString(row("output_directory"))
                End If
            End If

            ' If the string is empty then use the documents work directory
            If result = System.String.Empty Then
                result = Environment.GetFolderPath(Environment.SpecialFolder.Personal)
            End If

            ' Remove the trailing slash or colon if there is one.
            ' It is not acceptable to have a directory seperator for
            ' the end as we will add it when we merge the strings
            If result <> System.String.Empty Then
                Select Case result.Substring(result.Length)
                    Case "\", "/"
                        result = result.Remove(result.Length, 1)
                End Select
            End If

            ' The resulting directory is returned
            Return result
        End Function

        ''' <summary>
        ''' Process the background thread to create the ACH file
        ''' </summary>
        Private Sub bt_DoWork(ByVal sender As Object, ByVal e As System.ComponentModel.DoWorkEventArgs)
            Dim answer As Boolean = m_achCreateClass.CreateACHFile(m_mySchedules, m_myEffectiveDate, m_myBank, m_myIncludePrenotes, m_myFilename, TextEdit_Note.Text.Trim())
            e.Result = answer
        End Sub

        ''' <summary>
        ''' When the background thread terminates, handle the completion status
        ''' </summary>
        Private Sub bt_RunWorkerCompleted(ByVal sender As Object, ByVal e As System.ComponentModel.RunWorkerCompletedEventArgs)

            ' Dispose of the wait dialog
            If m_wt IsNot Nothing Then
                If m_wt.Visible Then m_wt.Close()
                If Not m_wt.IsDisposed Then m_wt.Dispose()
                m_wt = Nothing
            End If

            Dim answer As Boolean = CType(e.Result, Boolean)
            OKButton.Enabled = True
            If answer Then
                DebtPlus.Data.Forms.MessageBox.Show(String.Format("The file generation for batch #{2:f0} is complete." + Environment.NewLine + Environment.NewLine + "There are {0:n0} clients totalling {1:c}", m_achCreateClass.ClientCount, m_achCreateClass.TotalDebit, m_achCreateClass.DepositBatchID), "File Generation Completed")
                Close()
            End If
        End Sub

        ''' <summary>
        ''' Process the RESIZE event for the main form
        ''' </summary>
        Private Sub MainForm_Resize(ByVal sender As Object, ByVal e As System.EventArgs)
            CloseBox = True
            CloseBox = False
        End Sub

        ''' <summary>
        ''' Don't leave the status form alive if we are going.
        ''' </summary>
        Private Sub MainForm_FormClosing(sender As Object, e As System.Windows.Forms.FormClosingEventArgs)

            ' Clean up the waiting dialog
            If m_wt IsNot Nothing Then
                If m_wt.Visible Then m_wt.Close()
                If Not m_wt.IsDisposed Then m_wt.Dispose()
                m_wt = Nothing
            End If

        End Sub
    End Class
End Namespace
