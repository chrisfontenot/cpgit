Namespace ACH.FileGenerate
    <Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
    Partial Class MainForm

        'Form overrides dispose to clean up the component list.
        <System.Diagnostics.DebuggerNonUserCode()> _
        Protected Overrides Sub Dispose(ByVal disposing As Boolean)
            Try
                If disposing Then
                    If components IsNot Nothing Then components.Dispose()
                    BankDS.Dispose()
                    bt.Dispose()
                End If
            Finally
                MyBase.Dispose(disposing)
            End Try
        End Sub

        'Required by the Windows Form Designer
        Private components As System.ComponentModel.IContainer

        'NOTE: The following procedure is required by the Windows Form Designer
        'It can be modified using the Windows Form Designer.  
        'Do not modify it using the code editor.
        <System.Diagnostics.DebuggerStepThrough()> _
        Private Sub InitializeComponent()
            Dim SerializableAppearanceObject1 As DevExpress.Utils.SerializableAppearanceObject = New DevExpress.Utils.SerializableAppearanceObject
            Me.FileNameText = New DevExpress.XtraEditors.ButtonEdit
            Me.Label4 = New DevExpress.XtraEditors.LabelControl
            Me.CancelBtn = New DevExpress.XtraEditors.SimpleButton
            Me.IncludePrenotesCheckbox = New DevExpress.XtraEditors.CheckEdit
            Me.EffectiveDateEdit = New DevExpress.XtraEditors.DateEdit
            Me.Label3 = New DevExpress.XtraEditors.LabelControl
            Me.Label2 = New DevExpress.XtraEditors.LabelControl
            Me.BankLookup = New DevExpress.XtraEditors.LookUpEdit
            Me.OKButton = New DevExpress.XtraEditors.SimpleButton
            Me.Label1 = New DevExpress.XtraEditors.LabelControl
            Me.PullDateEdit = New DevExpress.XtraEditors.DateEdit
            Me.LabelControl1 = New DevExpress.XtraEditors.LabelControl
            Me.LabelControl2 = New DevExpress.XtraEditors.LabelControl
            Me.TextEdit_Note = New DevExpress.XtraEditors.TextEdit
            CType(Me.DxErrorProvider1, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.FileNameText.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.IncludePrenotesCheckbox.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.EffectiveDateEdit.Properties.VistaTimeProperties, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.EffectiveDateEdit.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.BankLookup.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.PullDateEdit.Properties.VistaTimeProperties, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.PullDateEdit.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.TextEdit_Note.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
            Me.SuspendLayout()
            '
            'FileNameText
            '
            Me.FileNameText.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                                            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
            Me.FileNameText.EditValue = ""
            Me.FileNameText.Location = New System.Drawing.Point(108, 121)
            Me.FileNameText.Name = "FileNameText"
            Me.FileNameText.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Up, "", -1, True, True, False, DevExpress.XtraEditors.ImageLocation.MiddleRight, Nothing, New DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), SerializableAppearanceObject1, "Click here to choose a file from a dialog", "lookup", Nothing, False)})
            Me.FileNameText.Properties.ButtonsStyle = DevExpress.XtraEditors.Controls.BorderStyles.Office2003
            Me.FileNameText.Properties.MaxLength = 256
            Me.FileNameText.Properties.ValidateOnEnterKey = True
            Me.FileNameText.Size = New System.Drawing.Size(348, 20)
            Me.FileNameText.TabIndex = 8
            Me.FileNameText.ToolTip = "Name of the output file to store the ACH information"
            '
            'Label4
            '
            Me.Label4.Location = New System.Drawing.Point(12, 124)
            Me.Label4.Name = "Label4"
            Me.Label4.Size = New System.Drawing.Size(57, 13)
            Me.Label4.TabIndex = 7
            Me.Label4.Text = "&Output File:"
            '
            'CancelBtn
            '
            Me.CancelBtn.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
            Me.CancelBtn.Location = New System.Drawing.Point(376, 71)
            Me.CancelBtn.Name = "CancelBtn"
            Me.CancelBtn.Size = New System.Drawing.Size(80, 26)
            Me.CancelBtn.TabIndex = 13
            Me.CancelBtn.Text = "&Cancel"
            Me.CancelBtn.ToolTip = "Cancel the operation."
            '
            'IncludePrenotesCheckbox
            '
            Me.IncludePrenotesCheckbox.EditValue = True
            Me.IncludePrenotesCheckbox.Location = New System.Drawing.Point(10, 176)
            Me.IncludePrenotesCheckbox.Name = "IncludePrenotesCheckbox"
            Me.IncludePrenotesCheckbox.Properties.Caption = "Include Prenotes in this file"
            Me.IncludePrenotesCheckbox.Size = New System.Drawing.Size(168, 19)
            Me.IncludePrenotesCheckbox.TabIndex = 11
            Me.IncludePrenotesCheckbox.ToolTip = "Should prenotes be included with this file? Normally they are."
            '
            'EffectiveDateEdit
            '
            Me.EffectiveDateEdit.EditValue = New Date(2005, 8, 19, 0, 0, 0, 0)
            Me.EffectiveDateEdit.Location = New System.Drawing.Point(108, 69)
            Me.EffectiveDateEdit.Name = "EffectiveDateEdit"
            Me.EffectiveDateEdit.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
            Me.EffectiveDateEdit.Properties.VistaTimeProperties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton})
            Me.EffectiveDateEdit.Size = New System.Drawing.Size(210, 20)
            Me.EffectiveDateEdit.TabIndex = 4
            Me.EffectiveDateEdit.ToolTip = "Effective date for the transfer. It must be atleast tomorrow."
            '
            'Label3
            '
            Me.Label3.Location = New System.Drawing.Point(12, 72)
            Me.Label3.Name = "Label3"
            Me.Label3.Size = New System.Drawing.Size(69, 13)
            Me.Label3.TabIndex = 3
            Me.Label3.Text = "&Effective Date"
            '
            'Label2
            '
            Me.Label2.Location = New System.Drawing.Point(12, 98)
            Me.Label2.Name = "Label2"
            Me.Label2.Size = New System.Drawing.Size(23, 13)
            Me.Label2.TabIndex = 5
            Me.Label2.Text = "&Bank"
            '
            'BankLookup
            '
            Me.BankLookup.Location = New System.Drawing.Point(108, 95)
            Me.BankLookup.Name = "BankLookup"
            Me.BankLookup.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
            Me.BankLookup.Size = New System.Drawing.Size(210, 20)
            Me.BankLookup.TabIndex = 6
            Me.BankLookup.ToolTip = "Select the ACH bank account that you wish to use for the transfer"
            '
            'OKButton
            '
            Me.OKButton.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
            Me.OKButton.Location = New System.Drawing.Point(376, 37)
            Me.OKButton.Name = "OKButton"
            Me.OKButton.Size = New System.Drawing.Size(80, 26)
            Me.OKButton.TabIndex = 12
            Me.OKButton.Text = "&OK"
            Me.OKButton.ToolTip = "Start the ACH file generation using the parameters that you have selected."
            '
            'Label1
            '
            Me.Label1.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                                      Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
            Me.Label1.Location = New System.Drawing.Point(12, 12)
            Me.Label1.Name = "Label1"
            Me.Label1.Size = New System.Drawing.Size(450, 13)
            Me.Label1.TabIndex = 0
            Me.Label1.Text = "Please enter the information needed to generate the ACH request data in the follo" & _
                             "wing fields."
            '
            'PullDateEdit
            '
            Me.PullDateEdit.EditValue = New Date(2005, 8, 19, 0, 0, 0, 0)
            Me.PullDateEdit.Location = New System.Drawing.Point(108, 43)
            Me.PullDateEdit.Name = "PullDateEdit"
            Me.PullDateEdit.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
            Me.PullDateEdit.Properties.VistaTimeProperties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton})
            Me.PullDateEdit.Size = New System.Drawing.Size(210, 20)
            Me.PullDateEdit.TabIndex = 2
            Me.PullDateEdit.ToolTip = "Effective date for the transfer. It must be atleast tomorrow."
            '
            'LabelControl1
            '
            Me.LabelControl1.Location = New System.Drawing.Point(12, 46)
            Me.LabelControl1.Name = "LabelControl1"
            Me.LabelControl1.Size = New System.Drawing.Size(42, 13)
            Me.LabelControl1.TabIndex = 1
            Me.LabelControl1.Text = "&Pull Date"
            '
            'LabelControl2
            '
            Me.LabelControl2.Location = New System.Drawing.Point(12, 153)
            Me.LabelControl2.Name = "LabelControl2"
            Me.LabelControl2.Size = New System.Drawing.Size(23, 13)
            Me.LabelControl2.TabIndex = 9
            Me.LabelControl2.Text = "&Note"
            '
            'TextEdit_Note
            '
            Me.TextEdit_Note.Location = New System.Drawing.Point(108, 150)
            Me.TextEdit_Note.Name = "TextEdit_Note"
            Me.TextEdit_Note.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[True]
            Me.TextEdit_Note.Properties.MaxLength = 50
            Me.TextEdit_Note.Size = New System.Drawing.Size(348, 20)
            Me.TextEdit_Note.TabIndex = 10
            '
            'MainForm
            '
            Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
            Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
            Me.ClientSize = New System.Drawing.Size(470, 200)
            Me.Controls.Add(Me.TextEdit_Note)
            Me.Controls.Add(Me.LabelControl2)
            Me.Controls.Add(Me.PullDateEdit)
            Me.Controls.Add(Me.LabelControl1)
            Me.Controls.Add(Me.FileNameText)
            Me.Controls.Add(Me.Label4)
            Me.Controls.Add(Me.CancelBtn)
            Me.Controls.Add(Me.IncludePrenotesCheckbox)
            Me.Controls.Add(Me.EffectiveDateEdit)
            Me.Controls.Add(Me.Label3)
            Me.Controls.Add(Me.Label2)
            Me.Controls.Add(Me.BankLookup)
            Me.Controls.Add(Me.OKButton)
            Me.Controls.Add(Me.Label1)
            Me.Name = "MainForm"
            Me.Text = "ACH Deposit Batch Creation"
            CType(Me.DxErrorProvider1, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.FileNameText.Properties, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.IncludePrenotesCheckbox.Properties, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.EffectiveDateEdit.Properties.VistaTimeProperties, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.EffectiveDateEdit.Properties, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.BankLookup.Properties, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.PullDateEdit.Properties.VistaTimeProperties, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.PullDateEdit.Properties, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.TextEdit_Note.Properties, System.ComponentModel.ISupportInitialize).EndInit()
            Me.ResumeLayout(False)
            Me.PerformLayout()

        End Sub
        Friend WithEvents FileNameText As DevExpress.XtraEditors.ButtonEdit
        Friend WithEvents Label4 As DevExpress.XtraEditors.LabelControl
        Friend WithEvents CancelBtn As DevExpress.XtraEditors.SimpleButton
        Friend WithEvents IncludePrenotesCheckbox As DevExpress.XtraEditors.CheckEdit
        Friend WithEvents EffectiveDateEdit As DevExpress.XtraEditors.DateEdit
        Friend WithEvents Label3 As DevExpress.XtraEditors.LabelControl
        Friend WithEvents Label2 As DevExpress.XtraEditors.LabelControl
        Friend WithEvents BankLookup As DevExpress.XtraEditors.LookUpEdit
        Friend WithEvents OKButton As DevExpress.XtraEditors.SimpleButton
        Friend WithEvents Label1 As DevExpress.XtraEditors.LabelControl
        Friend WithEvents PullDateEdit As DevExpress.XtraEditors.DateEdit
        Friend WithEvents LabelControl1 As DevExpress.XtraEditors.LabelControl
        Friend WithEvents LabelControl2 As DevExpress.XtraEditors.LabelControl
        Friend WithEvents TextEdit_Note As DevExpress.XtraEditors.TextEdit
    End Class
End Namespace
