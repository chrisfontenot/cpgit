Imports System.Data.SqlClient
Imports System.Windows.Forms

Namespace ACH.Custom.Suspend
    Public Class Sucess

        Public Sub ProcessSuccess(ByVal Filename As String)
            Dim ds As DataSet = Nothing

            Try
                Using fs As System.IO.FileStream = New System.IO.FileStream(Filename, System.IO.FileMode.Open)
                    If System.IO.Path.GetExtension(Filename).ToLower() = ".xlsx" Then
                        Using rdr As Excel.IExcelDataReader = Excel.ExcelReaderFactory.CreateOpenXmlReader(fs)
                            rdr.IsFirstRowAsColumnNames = False
                            ds = rdr.AsDataSet(True)
                        End Using
                    Else
                        Using rdr As Excel.IExcelDataReader = Excel.ExcelReaderFactory.CreateBinaryReader(fs)
                            rdr.IsFirstRowAsColumnNames = False
                            ds = rdr.AsDataSet(True)
                            If rdr.ExceptionMessage <> String.Empty Then
                                DebtPlus.Data.Forms.MessageBox.Show(String.Format("{0}{1}{1}The file can not be read. It is probably too old of an Excel format for use.{1}Please convert it to Office 2007 format (the ones that have an extension{1}of XLSX rather than XLS).", rdr.ExceptionMessage, Environment.NewLine), "Error processing file", MessageBoxButtons.OK)
                                Return
                            End If
                        End Using
                    End If
                End Using

                ' Key to the execution
                Dim itemGuid As System.Guid = System.Guid.NewGuid()

                If ds IsNot Nothing AndAlso ds.Tables.Count > 0 Then
                    Dim tbl As DataTable = ds.Tables(0)

                    ' Generate the key id
                    Dim cmd As SqlClient.SqlCommand = New SqlCommand
                    Dim cn As SqlClient.SqlConnection = New SqlConnection(DebtPlus.LINQ.SQLInfoClass.getDefault().ConnectionString)

                    cn.Open()
                    With cmd
                        .Connection = cn
                        .CommandText = "xpr_FirstNetSuccess"
                        .CommandType = CommandType.StoredProcedure
                        .Parameters.Add("@BatchId", SqlDbType.UniqueIdentifier).Value = itemGuid
                        .Parameters.Add("@Status", SqlDbType.VarChar, 80, tbl.Columns(0).ColumnName)
                        .Parameters.Add("@Client", SqlDbType.VarChar, 80, tbl.Columns(1).ColumnName)
                    End With

                    Using da As New SqlClient.SqlDataAdapter
                        da.InsertCommand = cmd
                        da.Update(tbl)
                    End Using
                End If

            Catch ex As System.Data.SqlClient.SqlException
                DebtPlus.Data.Forms.MessageBox.Show(ex.Message, "Error processing file")

            Finally
                If ds IsNot Nothing Then
                    ds.Dispose()
                End If
            End Try

        End Sub
    End Class
End Namespace