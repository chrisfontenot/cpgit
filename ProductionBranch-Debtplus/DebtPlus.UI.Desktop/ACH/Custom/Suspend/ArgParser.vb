Option Compare Binary
Option Explicit On
Option Strict On

Imports System.IO
Imports System.Security
Imports System.Windows.Forms
Imports MessageBox = DebtPlus.Data.Forms.MessageBox

Namespace ACH.Custom.Suspend
    Public Class ArgParser
        Inherits DebtPlus.Utils.ArgParserBase

        Public pathnames As New ArrayList()

        ''' <summary>
        ''' TRUE if process the success transaction files
        ''' </summary>
        Public Property Success As Boolean = False

        ''' <summary>
        ''' Process an option
        ''' </summary>
        Protected Overrides Function OnSwitch(ByVal switchName As String, ByVal switchValue As String) As SwitchStatus
            Dim ss As SwitchStatus = DebtPlus.Utils.ArgParserBase.SwitchStatus.NoError
            Select Case switchName
                Case "s"
                    Success = True

                Case "?"
                    ss = DebtPlus.Utils.ArgParserBase.SwitchStatus.ShowUsage

                Case Else
                    ss = DebtPlus.Utils.ArgParserBase.SwitchStatus.YesError
            End Select

            Return ss
        End Function

        ''' <summary>
        ''' Input file to be read
        ''' </summary>
        Protected Overrides Function OnNonSwitch(ByVal switchValue As String) As SwitchStatus
            Dim ss As SwitchStatus = DebtPlus.Utils.ArgParserBase.SwitchStatus.NoError

            ' Add command-line argument to array of pathnames.
            ' Convert switchValue to set of pathnames, add each pathname to the pathnames ArrayList.
            Try
                Dim d As String = Path.GetDirectoryName(switchValue)
                Dim dir As DirectoryInfo
                If (d.Length = 0) Then
                    dir = New DirectoryInfo(".")
                Else
                    dir = New DirectoryInfo(d)
                End If

                Dim f As FileInfo
                For Each f In dir.GetFiles(Path.GetFileName(switchValue))
                    pathnames.Add(f.FullName)
                Next f

            Catch SecEx As SecurityException
                Throw SecEx

            Catch
                ss = DebtPlus.Utils.ArgParserBase.SwitchStatus.YesError
            End Try

            If pathnames.Count = 0 Then
                AppendErrorLine("None of the specified files exists.")
                ss = DebtPlus.Utils.ArgParserBase.SwitchStatus.YesError
            End If

            Return ss
        End Function 'OnNonSwitch

        ''' <summary>
        ''' Ensure that we have a file to process
        ''' </summary>
        Protected Overrides Function OnDoneParse() As SwitchStatus
            Dim ss As SwitchStatus = MyBase.OnDoneParse
            If ss = DebtPlus.Utils.ArgParserBase.SwitchStatus.NoError AndAlso pathnames.Count = 0 Then
                Using dlg As New OpenFileDialog()

                    ' Define the options for the dialog
                    With dlg
                        .AddExtension = True
                        .AutoUpgradeEnabled = True
                        .CheckFileExists = True
                        .CheckPathExists = True
                        .DefaultExt = ".xlsx"
                        .DereferenceLinks = True
                        .Filter = "Excel Files (*.xls;*.xlsx)|*.xls;*.xlsx"
                        .FilterIndex = 0
                        .InitialDirectory = Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments)
                        .Multiselect = True
                        .ReadOnlyChecked = True
                        .ShowReadOnly = False
                        .SupportMultiDottedExtensions = True
                        .Title = "Open Excel Document(s)"
                        .ValidateNames = True

                        ' Run the dialog. If not OK then cancel.
                        If .ShowDialog() <> DialogResult.OK Then
                            ss = DebtPlus.Utils.ArgParserBase.SwitchStatus.Quit
                        Else

                            ' Add the names selected in the input dialog box
                            For Each Name As String In .FileNames
                                pathnames.Add(Path.GetFullPath(Name))
                            Next

                            ' If there are still no names then reject the program.
                            If pathnames.Count <= 0 Then
                                MessageBox.Show("You did not select any files. Please try the operation again and choose the input file.", "Sorry, but no files selected", MessageBoxButtons.OK)
                                ss = DebtPlus.Utils.ArgParserBase.SwitchStatus.Quit
                            End If
                        End If
                    End With
                End Using
            End If

            Return ss
        End Function

        ''' <summary>
        ''' Return the list of input file name
        ''' </summary>
        Public Function GetPathnameEnumerator() As IEnumerator
            Return pathnames.GetEnumerator(0, pathnames.Count)
        End Function 'GetPathnameEnumerator

        ''' <summary>
        ''' Create an instance of our class
        ''' </summary>
        Public Sub New()
            MyBase.New(New String() {"s"})
        End Sub

        Public Sub New(ByVal switchSymbols As String())
            MyBase.New(switchSymbols)
        End Sub

        Public Sub New(ByVal switchSymbols As String(), ByVal caseSensitiveSwitches As Boolean)
            MyBase.New(switchSymbols, caseSensitiveSwitches)
        End Sub

        Public Sub New(ByVal switchSymbols As String(), ByVal caseSensitiveSwitches As Boolean, ByVal switchChars As String())
            MyBase.New(switchSymbols, caseSensitiveSwitches, switchChars)
        End Sub

        ''' <summary>
        ''' Generate the command usage information
        ''' </summary>
        Protected Overrides Sub OnUsage(ByVal errorInfo As String)
            ' deprecated
        End Sub
    End Class
End Namespace
