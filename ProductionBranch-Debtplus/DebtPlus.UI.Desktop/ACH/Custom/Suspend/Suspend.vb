Imports System.Data.SqlClient
Imports System.Windows.Forms

Namespace ACH.Custom.Suspend
    Public Class Suspend

        Public Sub ProcessSuspend(ByVal Filename As String)
            Dim ds As DataSet = Nothing
            Dim outputDs As New DataSet("outputDs")

            Try
                Using fs As System.IO.FileStream = New System.IO.FileStream(Filename, System.IO.FileMode.Open)
                    If System.IO.Path.GetExtension(Filename).ToLower() = ".xlsx" Then
                        Using rdr As Excel.IExcelDataReader = Excel.ExcelReaderFactory.CreateOpenXmlReader(fs)
                            rdr.IsFirstRowAsColumnNames = False
                            ds = rdr.AsDataSet(True)
                            If ds Is Nothing AndAlso rdr.ExceptionMessage IsNot Nothing Then
                                DebtPlus.Data.Forms.MessageBox.Show(rdr.ExceptionMessage, "Error reading Excel File", MessageBoxButtons.OK, MessageBoxIcon.Error)
                            End If
                        End Using
                    Else
                        Using rdr As Excel.IExcelDataReader = Excel.ExcelReaderFactory.CreateBinaryReader(fs)
                            rdr.IsFirstRowAsColumnNames = False
                            ds = rdr.AsDataSet(True)
                            If ds Is Nothing AndAlso rdr.ExceptionMessage IsNot Nothing Then
                                DebtPlus.Data.Forms.MessageBox.Show(rdr.ExceptionMessage, "Error reading Excel File", MessageBoxButtons.OK, MessageBoxIcon.Error)
                            End If
                        End Using
                    End If
                End Using

                ' Key to the execution
                Dim itemGuid As System.Guid = System.Guid.NewGuid()

                If ds IsNot Nothing AndAlso ds.Tables.Count > 0 Then
                    Dim tbl As DataTable = ds.Tables(0)
                    Dim RowCount As Int32 = tbl.Rows.Count

                    ' Populate the table column names with the headings from the spreadsheet
                    Dim outputTbl As New DataTable("outputTbl")
                    Dim row As System.Data.DataRow = tbl.Rows(1)
                    outputTbl.Columns.Add("Sequence", GetType(System.Guid))

                    For col As Int32 = 0 To tbl.Columns.Count - 1
                        outputTbl.Columns.Add(Convert.ToString(row(col)), GetType(String))
                    Next

                    ' Correct the non-string fields with the appropriate type
                    outputTbl.Columns("MaintDate").DataType = GetType(DateTime)
                    outputTbl.Columns("RestartDate").DataType = GetType(DateTime)
                    outputTbl.Columns("ClientId").DataType = GetType(Int32)
                    outputTbl.Columns("Amount").DataType = GetType(Decimal)

                    ' Add the table to the dataset
                    outputDs.Tables.Add(outputTbl)

                    ' Add the data to the table
                    For RowNumber As Int32 = 2 To RowCount - 1
                        Dim InputRow As DataRow = tbl.Rows(RowNumber)
                        Dim OutputRow As DataRow = outputTbl.NewRow

                        ' Insert the batch ID field so that we know which records we have processed when.
                        OutputRow("Sequence") = itemGuid

                        Dim ValidRow As Boolean = True

                        ' Process each column in the spreadsheet for its value
                        For Each col As DataColumn In tbl.Columns
                            Dim ColName As String = Convert.ToString(tbl.Rows(1).Item(col))
                            Dim Value As Object = InputRow.Item(col)

                            ' The value should be legal.
                            If Value IsNot Nothing AndAlso Value IsNot DBNull.Value Then

                                ' Ignore empty strings.
                                If TypeOf Value Is String Then
                                    If Convert.ToString(Value).Trim() = String.Empty Then
                                        Continue For
                                    End If
                                End If

                                ' These columns are special. Ensure that they are valid items
                                ' The client id
                                If ColName = "ClientId" Then
                                    Dim Result As Int32
                                    If Not Int32.TryParse(Convert.ToString(Value), Result) Then
                                        ValidRow = False
                                    End If
                                    Value = Result

                                    ' The two datetime dates
                                ElseIf ColName = "MaintDate" OrElse ColName = "RestartDate" Then
                                    Dim Result As DateTime = DateTime.MinValue
                                    If Not DateTime.TryParse(Convert.ToString(Value), Result) Then
                                        ValidRow = False
                                    End If
                                    Value = Result

                                    ' And the amount field
                                ElseIf ColName = "Amount" Then
                                    Dim Result As Decimal = 0D
                                    If Not Decimal.TryParse(Convert.ToString(Value), Result) Then
                                        ValidRow = False
                                    End If
                                    Value = Result
                                End If

                                ' Update the resulting row with the value
                                OutputRow(ColName) = Value
                            End If
                        Next

                        ' Add the new row to the table
                        If ValidRow Then
                            outputTbl.Rows.Add(OutputRow)
                        End If
                    Next

                    ' Generate the key id
                    Dim cmd As SqlClient.SqlCommand = New SqlCommand
                    Dim cn As SqlClient.SqlConnection = New SqlConnection(DebtPlus.LINQ.SQLInfoClass.getDefault().ConnectionString)

                    cn.Open()
                    With cmd
                        .Connection = cn
                        .CommandText = "xpr_FirstNetStopSuspend"
                        .CommandType = CommandType.StoredProcedure
                        .Parameters.Add("@BatchId", SqlDbType.UniqueIdentifier, 8, "Sequence")
                        .Parameters.Add("@CompanyN", SqlDbType.VarChar, 80, "CompanyN")
                        .Parameters.Add("@LastName", SqlDbType.VarChar, 80, "LastName")
                        .Parameters.Add("@FirstName", SqlDbType.VarChar, 80, "FirstName")
                        .Parameters.Add("@client", SqlDbType.Int, 4, "ClientId")
                        .Parameters.Add("@MaintDate", SqlDbType.DateTime, 8, "MaintDate")
                        .Parameters.Add("@PullDates", SqlDbType.VarChar, 80, "PullDates")
                        .Parameters.Add("@Status", SqlDbType.VarChar, 80, "Status")
                        .Parameters.Add("@Amount", SqlDbType.Decimal, 8, "Amount")
                        .Parameters.Add("@RestartDate", SqlDbType.DateTime, 8, "RestartDate")
                        .Parameters.Add("@RoutingNum", SqlDbType.VarChar, 80, "RoutingNum")
                        .Parameters.Add("@AccountNum", SqlDbType.VarChar, 80, "AccountNum")
                    End With

                    Using da As New SqlClient.SqlDataAdapter
                        da.InsertCommand = cmd
                        da.Update(outputTbl)
                    End Using
                End If

            Catch ex As System.Data.SqlClient.SqlException
                DebtPlus.UI.Common.ErrorHandling.HandleErrors(ex, "Error processing file")

            Finally
                If ds IsNot Nothing Then
                    ds.Dispose()
                End If
                If outputDs IsNot Nothing Then
                    outputDs.Dispose()
                End If
            End Try

        End Sub
    End Class
End Namespace