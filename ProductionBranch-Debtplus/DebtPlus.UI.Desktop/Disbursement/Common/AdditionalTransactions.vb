#Region "Copyright 2000-2012 DebtPlus, L.L.C."
'{*******************************************************************}
'{                                                                   }
'{       DebtPlus Debt Management System                             }
'{                                                                   }
'{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
'{       ALL RIGHTS RESERVED                                         }
'{                                                                   }
'{   The entire contents of this file is protected by U.S. and       }
'{   International Copyright Laws. Unauthorized reproduction,        }
'{   reverse-engineering, and distribution of all or any portion of  }
'{   the code contained in this file is strictly prohibited and may  }
'{   result in severe civil and criminal penalties and will be       }
'{   prosecuted to the maximum extent possible under the law.        }
'{                                                                   }
'{   RESTRICTIONS                                                    }
'{                                                                   }
'{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
'{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
'{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
'{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
'{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
'{                                                                   }
'{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
'{   ADDITIONAL RESTRICTIONS.                                        }
'{                                                                   }
'{*******************************************************************}
#End Region

Imports System.Data.SqlClient
Imports System.Windows.Forms

Namespace Disbursement.Common
    Friend Class AdditionalTransactions
        Implements Disbursement.Common.ISupportContext

        Public Property context As Disbursement.Common.IDisbursementContext Implements Disbursement.Common.ISupportContext.Context
        Private ReadOnly m_ClientId As Int32

        Public Sub New(ByVal Context As Disbursement.Common.IDisbursementContext)
            MyClass.New()
            Me.context = context
        End Sub

        Public Sub New(ByVal Context As IDisbursementContext, ByVal ClientId As Int32)
            MyClass.New()
            m_ClientId = ClientId
            Me.context = Context
        End Sub

        Private Sub New()
            MyBase.New()
            InitializeComponent()
            RegisterHandlers()
        End Sub

        Private ds As New DataSet("ds")

''' <summary>
        ''' Initialize the form with the client number
        ''' </summary>

        Private Sub RegisterHandlers()
            AddHandler Load, AddressOf AdditionalTransactions_Load
            AddHandler ComboBoxEdit1.SelectedIndexChanged, AddressOf ComboBoxEdit1_SelectedIndexChanged
            AddHandler GridView1.Layout, AddressOf LayoutChanged
        End Sub

        Private Sub UnRegisterHandlers()
            RemoveHandler Load, AddressOf AdditionalTransactions_Load
            RemoveHandler ComboBoxEdit1.SelectedIndexChanged, AddressOf ComboBoxEdit1_SelectedIndexChanged
            RemoveHandler GridView1.Layout, AddressOf LayoutChanged
        End Sub

        ''' <summary>
        ''' Process the FORM LOAD event
        ''' </summary>
        Private Sub AdditionalTransactions_Load(ByVal sender As Object, ByVal e As EventArgs)

            UnRegisterHandlers()
            Try
                ' Reload the placement and size
                MyBase.LoadPlacement("Disbursement.Auto.Process.AdditionalTransactions")

                RefreshList()
                GridView1.BestFitColumns()

                ' Load the layout for the grid control
                Dim pathName As String = XMLBasePath()
                Dim fileName As String = System.IO.Path.Combine(pathName, "AdditionalTransactions.Grid.xml")
                If System.IO.File.Exists(fileName) Then
                    GridView1.RestoreLayoutFromXml(fileName)
                End If

            Catch ex As System.IO.DirectoryNotFoundException
            Catch ex As System.IO.FileNotFoundException

            Finally
                RegisterHandlers()
            End Try
        End Sub

        ''' <summary>
        ''' Return the directory to the saved layout information
        ''' </summary>
        Protected Overridable Function XMLBasePath() As String
            Dim basePath As String = String.Format("{0}{1}DebtPlus", Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData), System.IO.Path.DirectorySeparatorChar)
            Return System.IO.Path.Combine(basePath, "Disbursement.Auto.Process")
        End Function

        ''' <summary>
        ''' If the layout is changed then update the file
        ''' </summary>
        Private Sub LayoutChanged(ByVal Sender As Object, ByVal e As EventArgs)
            Dim pathName As String = XMLBasePath()
            If Not System.IO.Directory.Exists(pathName) Then
                System.IO.Directory.CreateDirectory(pathName)
            End If

            Dim fileName As String = System.IO.Path.Combine(pathName, "AdditionalTransactions.Grid.xml")
            GridView1.SaveLayoutToXml(fileName)
        End Sub

        ''' <summary>
        ''' Handle the change in the period control
        ''' </summary>
        Private Sub ComboBoxEdit1_SelectedIndexChanged(ByVal sender As Object, ByVal e As EventArgs)
            RefreshList()
        End Sub

        ''' <summary>
        ''' Reload the list of transactions for the period
        ''' </summary>
        Private Sub RefreshList()

            ' The period is simply the index. We don't have values since it is too simple of a list.
            Dim datePeriod As Int32 = ComboBoxEdit1.SelectedIndex + 1

            Using cmd As SqlClient.SqlCommand = New SqlCommand
                With cmd
                    .Connection = New SqlConnection(DebtPlus.LINQ.SQLInfoClass.getDefault().ConnectionString)
                    .CommandText = "SELECT date_created, isnull(tran_type,'') + isnull('/' + tran_subtype,'') as tran_type_subtype, isnull(credit_amt,0) as credit_amt, isnull(debit_amt,0) as debit_amt, message FROM registers_client WITH (NOLOCK) WHERE client = @client AND datediff(m, date_created, getdate()) between 0 and @period"
                    .CommandType = CommandType.Text
                    With .Parameters
                        .Add("@client", SqlDbType.Int).Value = m_ClientId
                        .Add("@period", SqlDbType.Int).Value = datePeriod
                    End With
                End With

                Dim current_cursor As Cursor = Cursor.Current
                Dim da As New SqlClient.SqlDataAdapter(cmd)
                Try
                    Cursor.Current = Cursors.WaitCursor
                    ds.Clear()
                    da.Fill(ds, "transactions")

                    With GridControl1
                        .DataSource = ds.Tables(0).DefaultView
                        .RefreshDataSource()
                    End With

                Catch ex As SqlClient.SqlException
                    DebtPlus.UI.Common.ErrorHandling.HandleErrors(ex, "Error reading client transactions")

                Finally
                    da.Dispose()
                    Cursor.Current = current_cursor
                End Try
            End Using
        End Sub
    End Class
End Namespace
