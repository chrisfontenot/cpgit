#Region "Copyright 2000-2012 DebtPlus, L.L.C."
'{*******************************************************************}
'{                                                                   }
'{       DebtPlus Debt Management System                             }
'{                                                                   }
'{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
'{       ALL RIGHTS RESERVED                                         }
'{                                                                   }
'{   The entire contents of this file is protected by U.S. and       }
'{   International Copyright Laws. Unauthorized reproduction,        }
'{   reverse-engineering, and distribution of all or any portion of  }
'{   the code contained in this file is strictly prohibited and may  }
'{   result in severe civil and criminal penalties and will be       }
'{   prosecuted to the maximum extent possible under the law.        }
'{                                                                   }
'{   RESTRICTIONS                                                    }
'{                                                                   }
'{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
'{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
'{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
'{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
'{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
'{                                                                   }
'{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
'{   ADDITIONAL RESTRICTIONS.                                        }
'{                                                                   }
'{*******************************************************************}

#End Region

Option Compare Binary
Option Strict Off
Option Explicit On

Imports DebtPlus.Utils
Imports DebtPlus.UI.Desktop.Disbursement.Automated.Post
Imports System.Data.SqlClient
Imports System.Windows.Forms

Namespace Disbursement.Common
    Friend Class item_info

        ''' <summary>
        ''' Class of processing for payments. All payments are vectored through here.
        ''' </summary>
        Private cls_post As CDisbursement_Post = Nothing            ' Pointer to our parent class
        Private cleared_status As String = "P"                      ' Used when the item is created
        Public bank As Int32 = -1                            ' Bank number

        Public tran_type As String = "AD"                           ' "AD" or "BW"
        Public item_count As Int32 = 0                       ' Number of items currently on the draft
        Public max_items As Int32 = 0                        ' Maximum number of items per check
        Public max_amount As Decimal = 0D                           ' Maximum dollar amount per check
        Public output_directory As String = String.Empty     ' If EFT, this is the output directorty for the file

        Public trust_register As Int32 = 0                   ' current trust register
        Public gross As Decimal = 0D                                ' gross amount
        Public deducted As Decimal = 0D                             ' total deducted amount
        Public billed As Decimal = 0D                               ' total billed amount

        Public creditor_register As Int32 = 0                       ' Pointer to the creditor register entry

        ''' <summary>
        ''' Create a new instance of our class
        ''' </summary>
        Public Sub New()
            MyBase.new()
        End Sub

        Public Sub New(ByVal Parent As CDisbursement_Post)
            MyClass.New()
            cls_post = Parent
        End Sub

        ''' <summary>
        ''' Return the type of the bank account
        ''' </summary>
        Private _bank_type As String = "C"                          ' Type of account
        Public ReadOnly Property bank_type() As String
            Get
                Return _bank_type
            End Get
        End Property

        ''' <summary>
        ''' Initialize the item data
        ''' </summary>
        Public Sub New_Check()
            billed = 0D
            deducted = 0D
            gross = 0D
            item_count = 0
            creditor_register = 0
        End Sub

        ''' <summary>
        ''' Initialize some of the information about the bank
        ''' </summary>
        Public Sub read_bank(ByVal BankNumber As Int32)

            ' Set the default parameters
            bank = BankNumber
            max_amount = 0D
            tran_type = "AD"
            max_items = 0
            max_amount = 0D
            output_directory = String.Empty
            cleared_status = "P"
            _bank_type = "C"

            Dim cn As SqlConnection = New SqlConnection(DebtPlus.LINQ.SQLInfoClass.getDefault().ConnectionString)
            Dim rd As SqlDataReader = Nothing
            Try
                cn.Open()
                Using cmd As SqlCommand = New SqlCommand
                    With cmd
                        .Connection = cn
                        .CommandText = "SELECT output_directory, convert(int,max_clients_per_check) as max_items, convert(money,max_amt_per_check) as max_amount,type FROM banks WITH (NOLOCK) WHERE bank=@bank"
                        .Parameters.Add("@bank", SqlDbType.Int).Value = bank
                        rd = .ExecuteReader(CommandBehavior.CloseConnection Or CommandBehavior.SingleRow)
                    End With
                End Using

                If rd.Read Then
                    If Not rd.IsDBNull(0) Then output_directory = rd.GetString(0).Trim()
                    If Not rd.IsDBNull(1) Then max_items = Convert.ToInt32(rd.GetValue(1))
                    If Not rd.IsDBNull(2) Then max_amount = Convert.ToDecimal(rd.GetValue(2))
                    If Not rd.IsDBNull(3) Then _bank_type = rd.GetString(3)
                End If

            Finally
                If rd IsNot Nothing Then rd.Dispose()
                If cn IsNot Nothing Then cn.Dispose()
            End Try

            ' Do some post-processing of the items from the banks table
            Select Case _bank_type

                Case "R", "V", "E" ' EFT types have no limits and are created "pending to be reconciled"
                    tran_type = "BW"
                    cleared_status = " "
                    max_items = 0
                    max_amount = 0D

                Case Else ' Checks have a limit on the number of items and amount.
                    tran_type = "AD"
                    cleared_status = "P"
            End Select
        End Sub

        ''' <summary>
        ''' Complete the processing of all check or rps creditors
        ''' </summary>
        Public Sub Save_Trust_Register(ByVal cn As SqlConnection, ByVal txn As SqlTransaction)

            Using cmd As SqlCommand = New SqlCommand
                With cmd
                    .Connection = cn
                    .Transaction = txn
                    .CommandText = "xpr_disbursement_cr_trans"
                    .CommandType = CommandType.StoredProcedure

                    .Parameters.Add("@tran_type", SqlDbType.VarChar, 2).Value = tran_type
                    .Parameters.Add("@trust_register", SqlDbType.Int).Value = trust_register
                    .Parameters.Add("@disbursement_register", SqlDbType.Int).Value = cls_post.disbursement_register
                    .Parameters.Add("@creditor", SqlDbType.VarChar, 10).Value = cls_post.Creditor
                    .Parameters.Add("@gross", SqlDbType.Decimal).Value = gross
                    .Parameters.Add("@deducted", SqlDbType.Decimal).Value = deducted
                    .Parameters.Add("@billed", SqlDbType.Decimal).Value = billed
                    .Parameters.Add("@creditor_register", SqlDbType.Int).Value = creditor_register

                    .ExecuteNonQuery()
                End With
            End Using

            ' Start with new check information
            New_Check()

            ' We no longer have an open trust register if this is a check. We do for EFT, but not a check.
            If _bank_type = "C" Then trust_register = 0
        End Sub

        ''' <summary>
        ''' Complete the processing of all check or rps creditors
        ''' </summary>
        Public Sub New_Trust_Register(ByVal cn As SqlConnection, ByVal txn As SqlTransaction)
            Using cmd As SqlCommand = New SqlCommand
                With cmd
                    .Connection = cn
                    .Transaction = txn
                    .CommandText = "xpr_trust_register_create_" + tran_type
                    .CommandType = CommandType.StoredProcedure
                    .Parameters.Add("RETURN", SqlDbType.Int).Direction = ParameterDirection.ReturnValue
                    .Parameters.Add("@creditor", SqlDbType.VarChar, 80).Value = cls_post.Creditor
                    .Parameters.Add("@cleared", SqlDbType.VarChar, 1).Value = cleared_status
                    .Parameters.Add("@bank", SqlDbType.Int).Value = bank

                    .ExecuteNonQuery()
                    trust_register = Convert.ToInt32(.Parameters(0).Value)
                End With
            End Using
        End Sub

        ''' <summary>
        ''' Create the new creditor register entry
        ''' </summary>
        Public Sub New_Creditor_Register(ByVal cn As SqlConnection, ByVal txn As SqlTransaction)
            Using cmd As SqlCommand = New SqlCommand
                With cmd
                    .Connection = cn
                    .Transaction = txn
                    .CommandText = "xpr_insert_registers_creditor"
                    .CommandType = CommandType.StoredProcedure
                    .Parameters.Add("RETURN", SqlDbType.Int).Direction = ParameterDirection.ReturnValue
                    .Parameters.Add("@tran_type", SqlDbType.VarChar, 4).Value = tran_type
                    .Parameters.Add("@creditor", SqlDbType.VarChar, 80).Value = cls_post.Creditor
                    .Parameters.Add("@disbursement_register", SqlDbType.Int).Value = cls_post.disbursement_register
                    .Parameters.Add("@trust_register", SqlDbType.Int).Value = trust_register
                    .Parameters.Add("@debit_amt", SqlDbType.Decimal).Value = 0D

                    .ExecuteNonQuery()
                    creditor_register = Convert.ToInt32(.Parameters(0).Value)
                End With
            End Using
        End Sub

        ''' <summary>
        ''' Generate the RPPS output file if this is an RPPS bank
        ''' </summary>
        Public Sub Write_RPPS(Optional ByVal useLocalFile As Boolean = False)
            Dim frmRPS As RPPS_Processing = Nothing

            Select Case bank_type
                Case "R"
                    frmRPS = New RPPS_Processing()
                    Do
                        Dim clsRPPS As New GenerateRPSPayments
                        With clsRPPS
                            .disbursement_register = cls_post.disbursement_register
                            .bank = bank
                            If .Process_Prenotes(frmRPS, useLocalFile) Then
                                Exit Do
                            End If
                        End With

                        ' Redo the file
                        DebtPlus.Data.Forms.MessageBox.Show("Someone else was generating an RPPS file. They have changed the trace numbers so we need to regenerate the RPPS file.", "Sorry, but we need to redo the RPPS file", MessageBoxButtons.OK, MessageBoxIcon.Hand)
                    Loop

                    Do
                        Dim clsRPPS As New GenerateRPSPayments
                        With clsRPPS
                            .disbursement_register = cls_post.disbursement_register
                            .bank = bank
                            If .Process_Payments(frmRPS, useLocalFile) Then
                                Exit Do
                            End If
                        End With

                        ' Redo the file
                        DebtPlus.Data.Forms.MessageBox.Show("Someone else was generating an RPPS file. They have changed the trace numbers so we need to regenerate the RPPS file.", "Sorry, but we need to redo the RPPS file", MessageBoxButtons.OK, MessageBoxIcon.Hand)
                    Loop

                    frmRPS.Dispose()
                    frmRPS = Nothing
            End Select

            If frmRPS IsNot Nothing Then
                frmRPS.Dispose()
            End If
        End Sub
    End Class
End Namespace
