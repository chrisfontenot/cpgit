#Region "Copyright 2000-2012 DebtPlus, L.L.C."
'{*******************************************************************}
'{                                                                   }
'{       DebtPlus Debt Management System                             }
'{                                                                   }
'{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
'{       ALL RIGHTS RESERVED                                         }
'{                                                                   }
'{   The entire contents of this file is protected by U.S. and       }
'{   International Copyright Laws. Unauthorized reproduction,        }
'{   reverse-engineering, and distribution of all or any portion of  }
'{   the code contained in this file is strictly prohibited and may  }
'{   result in severe civil and criminal penalties and will be       }
'{   prosecuted to the maximum extent possible under the law.        }
'{                                                                   }
'{   RESTRICTIONS                                                    }
'{                                                                   }
'{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
'{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
'{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
'{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
'{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
'{                                                                   }
'{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
'{   ADDITIONAL RESTRICTIONS.                                        }
'{                                                                   }
'{*******************************************************************}

#End Region

Option Compare Binary
Option Explicit On
Option Strict On

Imports System.ComponentModel
Imports DebtPlus.Interfaces.Debt
Imports DevExpress.XtraEditors.Controls
Imports DevExpress.XtraGrid.Views.Base
Imports DebtPlus.Data.Forms
Imports System.Threading
Imports System.Data.SqlClient
Imports System.Text
Imports System.IO
Imports DebtPlus.UI.Client.Service
Imports System.Drawing
Imports System.Windows.Forms

Namespace Disbursement.Common
    Friend Class Examine_Form
        Implements Disbursement.Common.ISupportContext

        ' Fonts used for the difference amount field
        Dim RegularFont As Font
        Dim BoldFont As Font
        Dim NotesThread As Thread

        ' Pointer to the client information for the disbursement.
        Private ReadOnly m_clientInfo As ClientClass
        Public Property context As Disbursement.Common.IDisbursementContext Implements Disbursement.Common.ISupportContext.Context

        Public Sub New(ByVal context As Disbursement.Common.IDisbursementContext, ByRef clientInfo As ClientClass)
            MyClass.New()
            Me.context = context
            m_clientInfo = clientInfo
        End Sub

        Private Sub New()
            MyBase.New()
            InitializeComponent()
            RegisterHandlers()
        End Sub

        Private Sub RegisterHandlers()
            AddHandler FormClosing, AddressOf Examine_Form_FormClosing
            AddHandler Load, AddressOf Examine_Form_Load
            AddHandler ButtonDontPay.Click, AddressOf ButtonDontPay_Click
            AddHandler ButtonPay.Click, AddressOf ButtonPay_Click
            AddHandler ButtonProrate.Click, AddressOf ButtonProrate_Click
            AddHandler ButtonQuit.Click, AddressOf ButtonQuit_Click
            AddHandler ButtonPrevious.Click, AddressOf ButtonPrevious_Click
            AddHandler ButtonShowNotes.Click, AddressOf ButtonShowNotes_Click
            AddHandler ButtonShowClient.Click, AddressOf ButtonShowClient_Click
            AddHandler ButtonClear.Click, AddressOf ButtonClear_Click
            AddHandler CalcEdit_col_debit_amt.EditValueChanging, AddressOf RepositoryItemCalcEdit1_EditValueChanging
            AddHandler CalcEdit_col_debit_amt.EditValueChanged, AddressOf Amount_Validated
            AddHandler GridView1.FocusedRowChanged, AddressOf GridView1_FocusedRowChanged
            AddHandler GridView1.Layout, AddressOf LayoutChanged
            AddHandler GridView1.CellValueChanged, AddressOf GridView1_CellValueChanged
        End Sub

        Private Sub UnRegisterHandlers()
            RemoveHandler FormClosing, AddressOf Examine_Form_FormClosing
            RemoveHandler Load, AddressOf Examine_Form_Load
            RemoveHandler ButtonDontPay.Click, AddressOf ButtonDontPay_Click
            RemoveHandler ButtonPay.Click, AddressOf ButtonPay_Click
            RemoveHandler ButtonProrate.Click, AddressOf ButtonProrate_Click
            RemoveHandler ButtonQuit.Click, AddressOf ButtonQuit_Click
            RemoveHandler ButtonPrevious.Click, AddressOf ButtonPrevious_Click
            RemoveHandler ButtonShowNotes.Click, AddressOf ButtonShowNotes_Click
            RemoveHandler ButtonShowClient.Click, AddressOf ButtonShowClient_Click
            RemoveHandler ButtonClear.Click, AddressOf ButtonClear_Click
            RemoveHandler CalcEdit_col_debit_amt.EditValueChanging, AddressOf RepositoryItemCalcEdit1_EditValueChanging
            RemoveHandler CalcEdit_col_debit_amt.EditValueChanged, AddressOf Amount_Validated
            RemoveHandler GridView1.FocusedRowChanged, AddressOf GridView1_FocusedRowChanged
            RemoveHandler GridView1.Layout, AddressOf LayoutChanged
            RemoveHandler GridView1.CellValueChanged, AddressOf GridView1_CellValueChanged
        End Sub

        ''' <summary>
        ''' Cancel the current form
        ''' </summary>
        Private Sub Examine_Form_FormClosing(ByVal sender As Object, ByVal e As FormClosingEventArgs)

            ' Cancel the previous thread execution for the notes.
            If NotesThread IsNot Nothing Then

                ' Send an abort request to the thread
                If NotesThread.IsAlive Then
                    NotesThread.Abort()
                End If

                ' Join the thread to pick up the completion event.
                NotesThread.Join(0)
                NotesThread = Nothing
            End If
        End Sub

        ''' <summary>
        ''' Process the form load event
        ''' </summary>
        Private Sub Examine_Form_Load(ByVal sender As Object, ByVal e As EventArgs)

            UnRegisterHandlers()
            m_clientInfo.BeginUpdate()
            Try

                ' Find the fonts to use for the difference figures
                RegularFont = New Font(Difference.Font, FontStyle.Regular)
                BoldFont = New Font(RegularFont, FontStyle.Bold)

                ' Restore the form placement
                MyBase.LoadPlacement("Disbursement.Auto.Process.ExamineForm")

                ' Load the client information
                With m_clientInfo
                    Const TableName As String = "xpr_disbursement_client_info"

                    ' Enable the buttons as needed
                    ButtonPrevious.Enabled = .CanExaminePrevious
                    ButtonShowNotes.Enabled = .CanShowNotes

                    Dim ds As DataSet = .ClientDS
                    Dim tbl As DataTable = ds.Tables(TableName)
                    If tbl Is Nothing Then
                        Dim current_cursor As Cursor = Cursor.Current
                        Try
                            Cursor.Current = Cursors.WaitCursor
                            Using cmd As SqlCommand = New SqlCommand
                                With cmd
                                    .Connection = New SqlConnection(DebtPlus.LINQ.SQLInfoClass.getDefault().ConnectionString)
                                    .CommandText = "xpr_disbursement_client_info"
                                    .CommandType = CommandType.StoredProcedure
                                    .Parameters.Add("@disbursement_register", SqlDbType.Int).Value = m_clientInfo.disbursement_register
                                    .Parameters.Add("@client", SqlDbType.Int).Value = m_clientInfo.ClientId
                                End With

                                Using da As New SqlDataAdapter(cmd)
                                    da.FillLoadOption = LoadOption.OverwriteChanges
                                    da.Fill(ds, TableName)
                                    tbl = ds.Tables(TableName)
                                End Using
                            End Using

                        Catch ex As SqlException
                            DebtPlus.UI.Common.ErrorHandling.HandleErrors(ex, "Error reading client information")

                        Finally
                            Cursor.Current = current_cursor
                        End Try
                    End If

                    lbl_client.Text = String.Format("{0:0000000}", .ClientId)

                    If tbl IsNot Nothing AndAlso tbl.Rows.Count > 0 Then
                        Dim row As DataRow = tbl.Rows(0)

                        ' Retrieve the address information
                        Dim sb As New StringBuilder
                        If Not row.IsNull("address1") Then
                            sb.Append(Environment.NewLine)
                            sb.Append(Convert.ToString(row("address1")))
                        End If

                        If Not row.IsNull("address2") Then
                            sb.Append(Environment.NewLine)
                            sb.Append(Convert.ToString(row("address2")))
                        End If

                        If Not row.IsNull("address3") Then
                            sb.Append(Environment.NewLine)
                            sb.Append(Convert.ToString(row("address3")))
                        End If
                        If sb.Length > 0 Then sb.Remove(0, 2)
                        lbl_address.Text = sb.ToString()

                        ' Retrieve the client name
                        If Not row.IsNull("name") Then lbl_name.Text = Convert.ToString(row("name"))

                        ' Retrieve the home phone
                        If Not row.IsNull("home_phone") Then lbl_home_ph.Text = Convert.ToString(row("home_phone"))

                        ' Retrieve the work phone
                        Dim phone As String = String.Empty
                        If Not row.IsNull("work_phone") Then phone = Convert.ToString(row("work_phone"))
                        lbl_work_ph.Text = phone

                        ' Retrieve the language
                        If Not row.IsNull("language") Then lbl_language.Text = Convert.ToString(row("language"))
                    End If
                End With

                With GridControl1
                    .BeginUpdate()
                    .DataSource = m_clientInfo.DebtList
                    .RefreshDataSource()
                    .EndUpdate()
                End With

                ' Update the deposit information
                DepositsControl1.DisplayInformation(m_clientInfo.ClientId)

                ' Update the display information for the client status
                TrustFundsOnHold.Text = String.Format("{0:c}", m_clientInfo.trust_funds_on_hold)
                TrustFundsAvailable.Text = String.Format("{0:c}", m_clientInfo.held_in_trust)

                UpdateDisplayedAmounts()

                ' Load the layout for the grid control
                Dim PathName As String = XMLBasePath()
                Dim FileName As String = Path.Combine(PathName, "ExamineForm.Grid.xml")
                Try
                    If File.Exists(FileName) Then
                        GridView1.RestoreLayoutFromXml(FileName)
                    End If
                Catch ex As DirectoryNotFoundException
                Catch ex As FileNotFoundException
                End Try

                ' Enable the edit operation on the first row in the list
                col_debit_amt.OptionsColumn.AllowEdit = ShouldAllowEdit()

                ' Enable/Disable the prorate button
                ButtonProrate.Enabled = m_clientInfo.CanProrate

            Finally
                m_clientInfo.EndUpdate()
                RegisterHandlers()
            End Try

            ' Show any pending notes if there are any recorded
            If m_clientInfo.CanShowNotes Then
                ShowClientNotes(False)
            End If
        End Sub

        ''' <summary>
        ''' Return the directory to the saved layout information
        ''' </summary>
        Protected Overridable Function XMLBasePath() As String
            Dim BasePath As String = String.Format("{0}{1}DebtPlus", Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData), Path.DirectorySeparatorChar)
            Return Path.Combine(BasePath, "Disbursement.Auto.Process")
        End Function

        ''' <summary>
        ''' If the layout is changed then update the file
        ''' </summary>
        Private Sub LayoutChanged(ByVal Sender As Object, ByVal e As EventArgs)
            Dim PathName As String = XMLBasePath()
            If Not Directory.Exists(PathName) Then
                Directory.CreateDirectory(PathName)
            End If

            Dim FileName As String = Path.Combine(PathName, "ExamineForm.Grid.xml")
            GridView1.SaveLayoutToXml(FileName)
        End Sub

        ''' <summary>
        ''' Update the amounts on the form when the values change
        ''' </summary>
        Private Sub UpdateDisplayedAmounts()

            ' Find the total of the disbursement amounts
            Dim DisbursedTotal As Decimal = DebtPlus.Utils.Nulls.DDec(col_debit_amt.SummaryItem.SummaryValue)
            SumOfDisbursements.Text = String.Format("{0:c}", DisbursedTotal)

            ' Compute the difference between the two items
            Dim DifferenceAmount As Decimal = m_clientInfo.held_in_trust - DisbursedTotal
            Difference.Text = String.Format("{0:c}", DifferenceAmount)

            ' Zero is a normal display
            If DifferenceAmount = 0D Then
                Difference.BackColor = Color.Transparent
                Difference.ForeColor = Color.Black
                Difference.Font = RegularFont

                ' Negative is a bold red display
            ElseIf DifferenceAmount < 0D Then
                Difference.BackColor = Color.Red
                Difference.ForeColor = Color.White
                Difference.Font = BoldFont

            Else

                ' Positive is a normal green display
                Difference.BackColor = Color.Transparent
                Difference.ForeColor = Color.Green
                Difference.Font = RegularFont
            End If

            ' Enable the payment option if there is a pay amount for a debt and the difference is not negative
            ButtonPay.Enabled = DisbursedTotal >= 0D AndAlso DifferenceAmount >= 0D
        End Sub

#If 0 Then

    ''' <summary>
    ''' Total dollar figure disbursed
    ''' </summary>
    Private Function TotalDisbursed() As Decimal
        Dim answer As Decimal = 0D
        For Each Debt As DebtPlus.Debt.IProratable In ClientInfo.DebtList
            answer += Debt.debit_amt
        Next
        Return answer
    End Function
#End If

        ''' <summary>
        ''' Process a change in the cell value
        ''' </summary>
        Private Sub GridView1_CellValueChanged(ByVal sender As Object, ByVal e As CellValueChangedEventArgs)
            GridView1.UpdateTotalSummary()
            UpdateDisplayedAmounts()

            ' Enable the PRORATE button as appropriate
            ButtonProrate.Enabled = m_clientInfo.CanProrate
        End Sub

        ''' <summary>
        ''' Do not pay the debts
        ''' </summary>
        Private Sub ButtonDontPay_Click(ByVal sender As Object, ByVal e As EventArgs)
            DialogResult = DialogResult.OK
            m_clientInfo.Do_NoPay()
        End Sub

        ''' <summary>
        ''' Pay the debts based upon the entered figures
        ''' </summary>
        Private Sub ButtonPay_Click(ByVal sender As Object, ByVal e As EventArgs)
            DialogResult = DialogResult.OK
            m_clientInfo.Do_Pay()
        End Sub

        ''' <summary>
        ''' Prorate the amount available to the debts
        ''' </summary>
        Private Sub ButtonProrate_Click(ByVal sender As Object, ByVal e As EventArgs)
            m_clientInfo.Do_Prorate()
            GridView1.RefreshData()
            UpdateDisplayedAmounts()
        End Sub

        ''' <summary>
        ''' Quit the disbursement process
        ''' </summary>
        Private Sub ButtonQuit_Click(ByVal sender As Object, ByVal e As EventArgs)
            DialogResult = DialogResult.OK
            m_clientInfo.Do_Quit()
        End Sub

        ''' <summary>
        ''' Go to the previous client
        ''' </summary>
        Private Sub ButtonPrevious_Click(ByVal sender As Object, ByVal e As EventArgs)
            m_clientInfo.Do_ExaminePrevious()
        End Sub

        ''' <summary>
        ''' Display the disbursement notes for the client
        ''' </summary>
        Private Sub ButtonShowNotes_Click(ByVal sender As Object, ByVal e As EventArgs)
            ShowClientNotes(True)
        End Sub

        ''' <summary>
        ''' Display the alert and disbursement notes
        ''' </summary>
        Private Sub ShowClientNotes(ByVal ShowNoNotesWarningDialog As Boolean)

            ' Cancel the previous thread execution for the notes.
            If NotesThread IsNot Nothing Then

                ' If the thread is still alive then we don't do anything.
                If NotesThread.IsAlive Then
                    Return
                End If

                ' Try to clean up the thread when it is completed
                NotesThread.Join(0)
            End If

            ' Create a new thread to process the note display
            NotesThread = New Thread(New ParameterizedThreadStart(AddressOf ShowNotes))
            With NotesThread
                .IsBackground = True
                .Name = "Disbursement Notes"
                .SetApartmentState(ApartmentState.STA)
                .Start(ShowNoNotesWarningDialog)
            End With
        End Sub

        ''' <summary>
        ''' Thread to display the notes
        ''' </summary>
        Private Sub ShowNotes(ByVal ShowWarningMessage As Object)

            Using disbAlert As New DebtPlus.Notes.AlertNotes.Disbursement()
                With disbAlert

                    ' Display the notes
                    Dim NotesShown As Int32 = .ShowAlerts(m_clientInfo.ClientId, m_clientInfo.disbursement_register)

                    ' Tell the user that there are no notes to give them some sense of a dialog.
                    If NotesShown <= 0 AndAlso Convert.ToBoolean(ShowWarningMessage) Then
                        DebtPlus.Data.Forms.MessageBox.Show("There are no notes to be displayed for this client", "No Notes", MessageBoxButtons.OK, MessageBoxIcon.Information)
                    End If
                End With
            End Using
        End Sub

        ''' <summary>
        ''' Display the client information
        ''' </summary>
        Private Sub ButtonShowClient_Click(ByVal sender As Object, ByVal e As EventArgs)
            Using showClient As New ClientUpdateClass()
                showClient.ShowEditDialog(m_clientInfo.ClientId, False)
            End Using
        End Sub

        ''' <summary>
        ''' Clear the payment amounts
        ''' </summary>
        Private Sub ButtonClear_Click(ByVal sender As Object, ByVal e As EventArgs)

            ' Clear the payments
            For Each row As DisbursementDebtRecord In m_clientInfo.DebtList
                row.debit_amt = 0D
            Next

            ' Redraw the grid and calculate the new amounts
            GridView1.RefreshData()
            UpdateDisplayedAmounts()

            ' Enable the PRORATE button as appropriate
            ButtonProrate.Enabled = m_clientInfo.CanProrate
        End Sub

        ''' <summary>
        ''' Do not allow negative numbers in the payment field
        ''' </summary>
        Protected Sub RepositoryItemCalcEdit1_EditValueChanging(ByVal sender As Object, ByVal e As ChangingEventArgs)

            ' If the debt can not be changed then reject the request
            If Not ShouldAllowEdit() Then
                e.Cancel = True
                Return
            End If

            ' Ensure that the amount is valid
            Dim NewValue As Decimal = Convert.ToDecimal(e.NewValue)
            If NewValue < 0D Then
                e.Cancel = True
                Return
            End If

            ' Ensure that the row is being updated. We need it done immediately rather than buffered for later.
            Dim Debt As DisbursementDebtRecord = CType(GridView1.GetFocusedRow, DisbursementDebtRecord)
            If Debt IsNot Nothing Then
                Debt.debit_amt = Convert.ToDecimal(e.NewValue)
            End If
        End Sub

        Private RecursiveFlag As Boolean
        ''' <summary>
        ''' Handle the completion of an edit cell operation
        ''' </summary>
        Private Sub Amount_Validated(ByVal sender As Object, ByVal e As EventArgs)

            ' Do not do this again if we are updating the row for the monthly fee
            If Not RecursiveFlag AndAlso Not m_clientInfo.InUpdate Then
                RecursiveFlag = True
                Try
                    ' Determine if this is the fee debt
                    Dim CurrentDebt As DisbursementDebtRecord = TryCast(GridView1.GetFocusedRow, DisbursementDebtRecord)
                    If CurrentDebt IsNot Nothing AndAlso CurrentDebt.DebtType <> DebtPlus.Interfaces.Debt.DebtTypeEnum.MonthlyFee Then
                        RecalculateFee()
                    End If

                Finally
                    RecursiveFlag = False
                    GridView1.UpdateTotalSummary()
                    UpdateDisplayedAmounts()
                End Try
            End If
        End Sub

        ''' <summary>
        ''' Recalculate the monthly fee as debts are changed
        ''' </summary>
        Protected Overridable Sub RecalculateFee()

            ' Find the monthly debt figure and recalculate the disbursement factor for it.
            Dim FeeDebt As DisbursementDebtRecord = TryCast(m_clientInfo.DebtList.MonthlyFeeDebt, DisbursementDebtRecord)
            If FeeDebt IsNot Nothing Then
                Try
                    AddHandler m_clientInfo.FeeInfo.QueryValue, AddressOf m_clientInfo.FeeInfo_QueryValue
                    Dim FeeAmount As Decimal = m_clientInfo.FeeInfo.FeeAmount()
                    FeeDebt.debit_amt = FeeAmount

                Finally
                    RemoveHandler m_clientInfo.FeeInfo.QueryValue, AddressOf m_clientInfo.FeeInfo_QueryValue
                End Try

                ' Refresh the grid for this row
                Dim Ordinal As Int32 = m_clientInfo.DebtList.IndexOf(FeeDebt)
                If Ordinal >= 0 Then
                    Dim RowHandle As Int32 = GridView1.GetRowHandle(Ordinal)
                    If RowHandle >= 0 Then
                        GridView1.RefreshRow(RowHandle)         ' Refresh the row
                        GridView1.InvalidateRow(RowHandle)      ' Redraw the row on the display
                    End If
                End If
            End If
        End Sub

        ''' <summary>
        ''' Do not permit HELD debts to be paid
        ''' </summary>
        Protected Sub GridView1_FocusedRowChanged(ByVal sender As Object, ByVal e As FocusedRowChangedEventArgs)
            col_debit_amt.OptionsColumn.AllowEdit = ShouldAllowEdit()
        End Sub

        ''' <summary>
        ''' Should the debt on the current focus be allowed to be paid?
        ''' </summary>
        Protected Function ShouldAllowEdit() As Boolean
            Dim Debt As DisbursementDebtRecord = TryCast(GridView1.GetFocusedRow, DisbursementDebtRecord)
            Return (Debt IsNot Nothing) AndAlso (Not Debt.hold_disbursements)
        End Function
    End Class
End Namespace
