#Region "Copyright 2000-2012 DebtPlus, L.L.C."
'{*******************************************************************}
'{                                                                   }
'{       DebtPlus Debt Management System                             }
'{                                                                   }
'{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
'{       ALL RIGHTS RESERVED                                         }
'{                                                                   }
'{   The entire contents of this file is protected by U.S. and       }
'{   International Copyright Laws. Unauthorized reproduction,        }
'{   reverse-engineering, and distribution of all or any portion of  }
'{   the code contained in this file is strictly prohibited and may  }
'{   result in severe civil and criminal penalties and will be       }
'{   prosecuted to the maximum extent possible under the law.        }
'{                                                                   }
'{   RESTRICTIONS                                                    }
'{                                                                   }
'{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
'{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
'{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
'{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
'{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
'{                                                                   }
'{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
'{   ADDITIONAL RESTRICTIONS.                                        }
'{                                                                   }
'{*******************************************************************}
#End Region
Imports DebtPlus.Interfaces.Debt
Imports DebtPlus.Svc.Common
Imports DebtPlus.Svc.Debt

Namespace Disbursement.Common

    ''' <summary>
    ''' Override prorate to handle special cases for disbursement
    ''' </summary>
    Public Class DisbursementProrate
        Inherits ProrateDebts

        ''' <summary>
        ''' Create a new instance of our class
        ''' </summary>
        Public Sub New()
            MyBase.New()
        End Sub

        Public Sub New(ByVal Debts As IProratableList)
            MyBase.New(Debts)
        End Sub

        ''' <summary>
        ''' Do the prorate operation
        ''' </summary>
        Private LocalTrustBalance As Decimal = 0D
        Private FirstTimeAtProrate As Boolean
        Public Overrides Sub Prorate(ByVal TrustBalance As Decimal)

            ' Save the values for the calculations
            Dim FeeClass As MonthlyFeeCalculation = Nothing
            LocalTrustBalance = TrustBalance
            availableFunds = TrustBalance
            FirstTimeAtProrate = True

            Try
                FeeClass = New MonthlyFeeCalculation()
                AddHandler FeeClass.QueryValue, AddressOf fees_QueryValue

                ' Adjust the PAF creditor's scheduled payment with the correct amount
                Dim Record As IProratable = DebtList.MonthlyFeeDebt
                If Record IsNot Nothing Then
                    With Record
                        .Debit_Amt = FeeClass.FeeAmount()
                        .Sched_Payment = .Debit_Amt
                    End With
                End If

                ' Use the new fee class for the debts
                FirstTimeAtProrate = False

                ' Find the amount of money that needs to be "taken off the top" of the payments and not prorated.
                Dim Reserved As Decimal = DebtList.TotalFees

                ' Do an initial stab at the proration procedure.
                TryProrate(TrustBalance - Reserved)

                ' Limit the disbursements to the debt balances where they are needed.
                Dim LastPayments As Decimal
                Dim CurrentPayments As Decimal = DebtList.TotalPayments
                Do
                    LastPayments = CurrentPayments

                    ' If the payments match the expected or there are no payments then we have nothing left to do
                    If LastPayments + Reserved = TrustBalance Then Exit Do
                    If LastPayments = 0D Then Exit Do

                    ' Adjust the PAF creditor's scheduled payment with the correct amount
                    Record = DebtList.MonthlyFeeDebt
                    If Record IsNot Nothing Then
                        With Record
                            .Debit_Amt = FeeClass.FeeAmount()
                            .Sched_Payment = .Debit_Amt
                        End With
                    End If

                    ' Do the prorate function again once the values have changed
                    TryProrate(TrustBalance - Reserved)

                    ' If the value did not change then we have reached equiliberium and stop
                    CurrentPayments = DebtList.TotalPayments
                Loop Until CurrentPayments = LastPayments

            Finally
                If FeeClass IsNot Nothing Then FeeClass.Dispose()
            End Try
        End Sub

        ''' <summary>
        ''' Determine the value if we know for the requested item
        ''' </summary>
        Protected Overrides Sub fees_QueryValue(sender As Object, e As Events.ParameterValueEventArgs)

            Select Case e.Name
                Case DebtPlus.Events.ParameterValueEventArgs.name_DollarsDisbursed
                    If FirstTimeAtProrate Then
                        e.Value = LocalTrustBalance
                    Else
                        e.Value = DebtList.TotalPayments
                    End If

                Case Else
                    MyBase.fees_QueryValue(sender, e)
            End Select
        End Sub
    End Class
End Namespace
