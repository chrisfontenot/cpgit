#Region "Copyright 2000-2012 DebtPlus, L.L.C."
'{*******************************************************************}
'{                                                                   }
'{       DebtPlus Debt Management System                             }
'{                                                                   }
'{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
'{       ALL RIGHTS RESERVED                                         }
'{                                                                   }
'{   The entire contents of this file is protected by U.S. and       }
'{   International Copyright Laws. Unauthorized reproduction,        }
'{   reverse-engineering, and distribution of all or any portion of  }
'{   the code contained in this file is strictly prohibited and may  }
'{   result in severe civil and criminal penalties and will be       }
'{   prosecuted to the maximum extent possible under the law.        }
'{                                                                   }
'{   RESTRICTIONS                                                    }
'{                                                                   }
'{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
'{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
'{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
'{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
'{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
'{                                                                   }
'{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
'{   ADDITIONAL RESTRICTIONS.                                        }
'{                                                                   }
'{*******************************************************************}

#End Region

Option Compare Binary
Option Strict On
Option Explicit On

Imports DebtPlus.Utils
Imports System.IO
Imports System.Text
Imports System.Data.SqlClient
Imports System.Text.RegularExpressions
Imports System.Windows.Forms

Namespace Disbursement.Common
    Friend Class GenerateRPSPayments
        Inherits Object

        ''' <summary>
        ''' Number of batch records written to the file
        ''' </summary>
        Private m_TotalFile_BatchCount As Int32 = 0
        Private Property TotalFile_BatchCount() As Int32
            Get
                Return m_TotalFile_BatchCount
            End Get
            Set(ByVal Value As Int32)
                m_TotalFile_BatchCount = Value
                If status_form IsNot Nothing Then status_form.total_batches = Value
            End Set
        End Property

        ''' <summary>
        ''' Number of blocks written to the file
        ''' </summary>
        Private m_BlockCount As Int32 = 0
        Private Property BlockCount() As Int32
            Get
                Return m_BlockCount
            End Get
            Set(ByVal Value As Int32)
                m_BlockCount = Value
            End Set
        End Property

        ''' <summary>
        ''' Number of records written to the text file
        ''' </summary>
        Private m_TotalFile_DetailCount As Int32 = 0
        Private Property TotalFile_DetailCount() As Int32
            Get
                Return m_TotalFile_DetailCount
            End Get
            Set(ByVal Value As Int32)
                m_TotalFile_DetailCount = Value
                If status_form IsNot Nothing Then status_form.total_items = Value
            End Set
        End Property

        ''' <summary>
        ''' Debit (paid) dollars in the file
        ''' </summary>
        Private m_TotalFile_Debit As Int32 = 0
        Private Property TotalFile_Debit() As Int32
            Get
                Return m_TotalFile_Debit
            End Get
            Set(ByVal Value As Int32)
                m_TotalFile_Debit = Value
                If status_form IsNot Nothing Then
                    status_form.total_debt = Convert.ToDecimal(Value) / 100D
                End If
            End Set
        End Property

        ''' <summary>
        ''' Credit (returned) dollars in the file
        ''' </summary>
        Private m_TotalFile_Credit As Int32 = 0
        Private Property TotalFile_Credit() As Int32
            Get
                Return m_TotalFile_Credit
            End Get
            Set(ByVal Value As Int32)
                m_TotalFile_Credit = Value
                If status_form IsNot Nothing Then
                    status_form.total_credit = Convert.ToDecimal(Value) / 100D
                End If
            End Set
        End Property

        ''' <summary>
        ''' Debit amount for the batch
        ''' </summary>
        Private m_Batch_Debit As Int32 = 0
        Private Property Batch_Debit() As Int32
            Get
                Return m_Batch_Debit
            End Get
            Set(ByVal Value As Int32)
                m_Batch_Debit = Value
                If status_form IsNot Nothing Then
                    status_form.total_debt = Convert.ToDecimal(Value + TotalFile_Debit) / 100D
                End If
            End Set
        End Property

        ''' <summary>
        ''' Credit amount for the batch
        ''' </summary>
        Private m_Batch_Credit As Int32 = 0
        Private Property Batch_Credit() As Int32
            Get
                Return m_Batch_Credit
            End Get
            Set(ByVal Value As Int32)
                m_Batch_Credit = Value
                If status_form IsNot Nothing Then
                    status_form.total_credit = Convert.ToDecimal(Value + TotalFile_Credit) / 100D
                End If
            End Set
        End Property

        ''' <summary>
        ''' Number of detail records for this batch
        ''' </summary>
        Private m_Batch_DetailCount As Int32 = 0
        Private Property Batch_DetailCount() As Int32
            Get
                Return m_Batch_DetailCount
            End Get
            Set(ByVal Value As Int32)
                m_Batch_DetailCount = Value
                If status_form IsNot Nothing Then status_form.total_items = Value + TotalFile_DetailCount
            End Set
        End Property

        ''' <summary>
        ''' Total billed amount
        ''' </summary>
        Private m_TotalFile_Billed As Int32 = 0
        Private Property TotalFile_Billed() As Int32
            Get
                Return m_TotalFile_Billed
            End Get
            Set(ByVal Value As Int32)
                m_TotalFile_Billed = Value
                If status_form IsNot Nothing Then status_form.total_billed = Convert.ToDecimal(Value) / 100D
            End Set
        End Property

        ''' <summary>
        ''' Total gross dollars amount
        ''' </summary>
        Private m_TotalFile_Gross As Int32 = 0
        Private Property TotalFile_Gross() As Int32
            Get
                Return m_TotalFile_Gross
            End Get
            Set(ByVal Value As Int32)
                m_TotalFile_Gross = Value
                If status_form IsNot Nothing Then status_form.total_gross = Convert.ToDecimal(Value) / 100D
            End Set
        End Property

        ''' <summary>
        ''' Total net dollars amount
        ''' </summary>
        Private m_TotalFile_Net As Int32 = 0
        Private Property TotalFile_Net() As Int32
            Get
                Return m_TotalFile_Net
            End Get
            Set(ByVal Value As Int32)
                m_TotalFile_Net = Value
                If status_form IsNot Nothing Then status_form.total_net = Convert.ToDecimal(Value) / 100D
            End Set
        End Property

        ''' <summary>
        ''' Number of prenote records
        ''' </summary>
        Private m_Prenote_Count As Int32 = 0
        Private Property Prenote_Count() As Int32
            Get
                Return m_Prenote_Count
            End Get
            Set(ByVal Value As Int32)
                m_Prenote_Count = Value
                If status_form IsNot Nothing Then status_form.total_prenotes = Value
            End Set
        End Property

        ' Pointer to our status form
        Private status_form As Iform_RPS = Nothing

        ' Current date for the batch
        Dim CurrentDate As Date = Now.Date

        ' Public information
        Public bank As Int32 = 0
        Public disbursement_register As Int32 = 0
        Public Shared FileName As String = String.Empty
        Private OutputTextFile As StreamWriter = Nothing
        Private eft_file As Int32 = 0

        Private Fake As Boolean = False
        Private CIE_Format As Boolean = False
        Private BillerType As Int32 = 0

        ' Information from the banks table
        Private RPS_ID As String = String.Empty
        Private RPS_Name As String = String.Empty
        Private RPS_Prefix As String = String.Empty
        Private RPS_Suffix As String = String.Empty

        Private Batch_ID As Int32 = 0                ' Value for the current day's batches
        Private transaction_id As Int32 = 0          ' Value for the current day's transactions

        Private OrigTest As Boolean = False

        Private open_batch_id As String = String.Empty
        Private open_biller_id As String = String.Empty
        Private Open_Biller_Name As String = String.Empty
        Private Open_Biller_Class As String = String.Empty
        Private Open_Biller_Description As String = String.Empty

        Private ds As DataSet
        Private tbl As DataTable
        Private vue As DataView
        Private drv As DataRowView
        Private RowNumber As Int32

        ' Sequence Counters
        Private transaction_number As Int32 = 0      ' Number of transactions processed

        Dim erf As EncoderReplacementFallback
        Dim drf As DecoderReplacementFallback
        Dim ae As Encoding

        Public Sub New()
            MyBase.new()

            Dim doy As Int32

            ' Fetch the date to be used for all purposes meaning "now", "today", etc.
            OrigTest = False
            Prenote_Count = 0

            ' Seed values for the sequence numbers to ensure that the values increase for days
            doy = CurrentDate.DayOfYear
            transaction_id = ((doy Mod 98) + 1) * 100000
            Batch_ID = doy * 10000

            ' Create the encoding classes
            erf = New EncoderReplacementFallback(" ")
            drf = New DecoderReplacementFallback(" ")
            ae = Encoding.GetEncoding("us-ascii", erf, drf)
        End Sub

        ''' <summary>
        ''' Ensure message does not have funny characters
        ''' </summary>
        Private Function clean_string(ByVal message As String) As String

            ' Find the length of the input stream. It should be the number of bytes for the input.
            Dim Length As Int32 = ae.GetByteCount(message)
            If Length < 1 Then
                Return String.Empty
            End If

            ' Convert the string to a list of bytes. This removes the non-ASCII characters.
            Dim ab(Length - 1) As Byte
            ae.GetBytes(message, 0, message.Length, ab, 0)

            ' Convert the bytes back to a string as ASCII characters
            Return ae.GetString(ab)
        End Function

        ''' <summary>
        ''' Process the RPS payment prenotes
        ''' </summary>
        Public Sub Generate_Payment_Prenotes(ByVal eft_file As Int32)
            Dim cn As SqlConnection = New SqlConnection(DebtPlus.LINQ.SQLInfoClass.getDefault().ConnectionString)

            Dim current_cursor As Cursor = Cursor.Current
            Try
                Cursor.Current = Cursors.WaitCursor
                cn.Open()
                Using cmd As SqlCommand = New SqlCommand
                    With cmd
                        .Connection = cn
                        .CommandText = "xpr_rpps_generate_prenote"
                        .CommandTimeout = System.Math.Min(300, DebtPlus.LINQ.SQLInfoClass.getDefault().CommandTimeout)
                        .CommandType = CommandType.StoredProcedure
                        .Parameters.Add("@rpps_file", SqlDbType.Int).Value = eft_file
                        .ExecuteNonQuery()
                    End With
                End Using

            Finally
                If cn IsNot Nothing Then cn.Dispose()
                Cursor.Current = current_cursor
            End Try
        End Sub

        ''' <summary>
        ''' Write the text record
        ''' </summary>
        Private Sub WriteRecord(ByVal strRecord As String)

            ' Count this record in the system
            Dim _FileRecordNumber As Int32 = FileRecordNumber()

            If Not Fake Then OutputTextFile.WriteLine(strRecord)
        End Sub

        ''' <summary>
        ''' Record number generated in the file
        ''' </summary>
        Private Function FileRecordNumber() As Int32
            Static _FileRecordNumber As Int32
            _FileRecordNumber += 1
            Return _FileRecordNumber
        End Function

        ''' <summary>
        ''' Read the configuration from the system table
        ''' </summary>
        Public Function ReadPaymentConfiguration() As Boolean
            Dim answer As Boolean = True
            Dim rd As SqlDataReader = Nothing
            Dim cn As SqlConnection = New SqlConnection(DebtPlus.LINQ.SQLInfoClass.getDefault().ConnectionString)

            ' Use the CIE format for "customer initiated requests"
            CIE_Format = True

            RPS_Name = String.Empty
            RPS_ID = String.Empty
            RPS_Prefix = String.Empty
            RPS_Suffix = String.Empty

            ' Fetch the values from the config table
            Dim current_cursor As Cursor = Cursor.Current
            Try
                Cursor.Current = Cursors.WaitCursor
                cn.Open()
                Using cmd As SqlCommand = New SqlCommand
                    With cmd
                        .Connection = cn
                        .CommandText = "xpr_rpps_config_payment"
                        .CommandType = CommandType.StoredProcedure
                        .Parameters.Add("@bank", SqlDbType.Int).Value = bank
                        rd = .ExecuteReader(CommandBehavior.CloseConnection Or CommandBehavior.SingleRow)
                    End With
                End Using

                If rd.Read Then
                    For FieldNo As Int32 = 0 To rd.FieldCount - 1
                        If Not rd.IsDBNull(FieldNo) Then
                            Select Case rd.GetName(FieldNo).ToLower()
                                Case "rps_id", "rpps_id" : RPS_ID = Convert.ToString(rd.GetValue(FieldNo))
                                Case "rps_origin_name", "rpps_origin_name" : RPS_Name = Convert.ToString(rd.GetValue(FieldNo))
                                Case "rps_prefix", "rpps_prefix" : RPS_Prefix = Convert.ToString(rd.GetValue(FieldNo))
                                Case "rps_suffix", "rpps_suffix" : RPS_Suffix = Convert.ToString(rd.GetValue(FieldNo))
                                Case "batch_id" : Batch_ID = Convert.ToInt32(rd.GetValue(FieldNo))
                                Case "transaction_id" : transaction_id = Convert.ToInt32(rd.GetValue(FieldNo))
                                Case "cie_format" : CIE_Format = Convert.ToInt32(rd.GetValue(FieldNo)) <> 0
                                Case "bank"
                                    If bank <= 0 Then bank = Convert.ToInt32(rd.GetValue(FieldNo))
                            End Select
                        End If
                    Next
                End If

            Finally
                If rd IsNot Nothing Then rd.Dispose()
                If cn IsNot Nothing Then cn.Dispose()
                Cursor.Current = current_cursor
            End Try

            ' Update the paramters for the configuration
            If RPS_Name = String.Empty Then
                answer = False
            Else
                RPS_ID = RPS_ID.PadLeft(8, "0"c)
                If RPS_ID = "00000000" Then
                    answer = False
                End If
            End If

            ' If this is a fake file then ask the user if we should continue
            If Not answer Then
                If DebtPlus.Data.Forms.MessageBox.Show(
                    "The RPS Biller ID is not configured properly." + vbCrLf +
                    "This is configured in the configuration tool under the RPS tab." + vbCrLf +
                    vbCrLf +
                    "If you wish to continue then no output file will be generated but the RPS transactions " +
                    vbCrLf + "will be allocated to a fake disbursement." +
                    vbCrLf +
                    vbCrLf + "If you don't continue now then please set the proper ORIGINATOR (8 digit) ID into the system." +
                    vbCrLf +
                    vbCrLf + "Do you wish to continue?", "Configuration Error", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2) = DialogResult.Yes Then answer = True
            End If

            Return answer
        End Function

        ''' <summary>
        ''' Obtain the next trace number for a detail record
        ''' </summary>
        Private Function Next_Detail_Trace() As String
            Dim Remainder As Int32

            ' Bump the number of transactions generated
            transaction_number += 1

            ' Ensure that the trace number wraps at the proper location
            Remainder = (transaction_id + transaction_number) Mod 10000000

            ' Return the trace string
            Return RPS_ID.PadLeft(8, "0"c) + Remainder.ToString("0000000")
        End Function

        ''' <summary>
        ''' Obtain the next trace number for a detail record
        ''' </summary>
        Private Function Next_Batch_Trace() As String
            Dim Remainder As Int32

            ' Ensure that the trace number wraps at the proper location
            Remainder = (Batch_ID + TotalFile_BatchCount) Mod 10000000

            ' Return the trace string
            Return RPS_ID.PadLeft(8, "0"c) + Remainder.ToString("0000000")
        End Function

        ''' <summary>
        ''' Generate the account name value for the detail
        ''' </summary>
        Private Function FixedField(ByVal varName As Object, ByVal length As Int32) As String

            ' Retrieve the value as a suitable string.
            Dim answer As String = DebtPlus.Utils.Nulls.DStr(varName).Trim()

            ' Make the field the exact number of spaces needed to generate the fixed format record
            answer = answer.PadRight(length)
            answer = answer.Substring(0, length)
            Return answer
        End Function

        ''' <summary>
        ''' Write the file header record
        ''' </summary>
        Private Sub Write_File_Header()
            Dim txt As New StringBuilder(100)

            txt.Append("1")                              ' Record type
            txt.Append("01")                             ' Priority code
            txt.Append(" 999900004")                     ' Immediate destination
            txt.AppendFormat(" {0:00000000}0", Convert.ToInt32(RPS_ID))    ' Origin code

            txt.AppendFormat("{0:yyMMddHHmm}", CurrentDate)     ' File creation date/time
            txt.Append("1")                                     ' File ID modifier
            txt.Append("094")                                   ' File Record Size
            txt.Append("10")                                    ' Blocking factor
            txt.Append("1")                                     ' File format code
            txt.Append("MC REMIT PROC CENT SITE")               ' File destination
            txt.Append(FixedField(RPS_Name, 23))                ' Source name

            ' Include the proper "marker" as needed
            If OrigTest Then
                txt.Append("ORIGTEST")                   ' Testing string
            Else
                txt.Append("        ")                   ' Normal string
            End If

            ' Write the record
            If txt.Length <> 94 Then Throw New ApplicationException("File Header record length <> 94 characters")
            WriteRecord(txt.ToString())
        End Sub

        ''' <summary>
        ''' Write the file trailer record
        ''' </summary>
        Private Sub Write_File_Control()
            Dim _FileRecordNumber As Int32

            ' Compute the number of blocks. It is based upon the record size and the blocking factor
            _FileRecordNumber = FileRecordNumber() - 1
            BlockCount = (_FileRecordNumber + 9) \ 10
            If BlockCount > 0 Then
                Dim txt As New StringBuilder

                txt.Append("9")
                txt.Append(TotalFile_BatchCount.ToString("000000"))
                txt.Append(BlockCount.ToString("000000"))
                txt.Append(TotalFile_DetailCount.ToString("00000000"))
                txt.Append("0"c, 10)
                txt.Append(TotalFile_Debit.ToString("000000000000"))
                txt.Append(TotalFile_Credit.ToString("000000000000"))
                txt.Append(" "c, 39)

                ' Write the record to the disk file
                If txt.Length <> 94 Then Throw New ApplicationException("File Trailer record length <> 94 characters")
                WriteRecord(txt.ToString())
            End If
        End Sub

        ''' <summary>
        ''' Write the batch header record
        ''' </summary>
        Private Sub Write_Batch_Header()
            Dim txt As New StringBuilder

            txt.Append("5")
            txt.Append(Open_Biller_Class)
            txt.Append(FixedField(Open_Biller_Name, 16))
            txt.Append(" "c, 20)
            txt.Append(open_biller_id)
            txt.Append("CIE")
            txt.Append(Open_Biller_Description)
            txt.Append(" "c, 6)
            txt.Append(CurrentDate.ToString("yyMMdd"))
            txt.Append(" "c, 3)
            txt.Append("1")
            txt.Append(open_batch_id)

            ' Write the record to the disk file
            If txt.Length <> 94 Then Throw New ApplicationException("Batch Header record length <> 94 characters")
            WriteRecord(txt.ToString())
        End Sub

        ''' <summary>
        ''' Write the batch trailer record
        ''' </summary>
        Private Sub Write_Batch_Control()
            Dim txt As New StringBuilder

            ' Emit the file control record
            txt.Append("8")
            txt.Append(Open_Biller_Class)
            txt.Append(Batch_DetailCount.ToString("000000"))
            txt.Append("0"c, 10)
            txt.Append(Batch_Debit.ToString("000000000000"))
            txt.Append(Batch_Credit.ToString("000000000000"))
            txt.Append(open_biller_id)
            txt.Append(" "c, 25)
            txt.Append(open_batch_id)

            ' Write the record to the disk file
            If txt.Length <> 94 Then Throw New ApplicationException("Batch Trailer record length <> 94 characters")
            WriteRecord(txt.ToString())
        End Sub

        ''' <summary>
        ''' Write the detail record
        ''' </summary>
        Private Sub Write_Detail(ByVal Trace_Number As String,
                                 ByVal Transaction_Code As String,
                                 Optional ByVal Debit_Amount As Int32 = 0,
                                 Optional ByVal Credit_Amount As Int32 = 0,
                                 Optional ByVal Account_Number As String = "",
                                 Optional ByVal Account_Name As String = "")

            Dim txt As New StringBuilder

            Select Case Transaction_Code
                Case ENTRY_TRANSACTION_CODE_PRENOTE
                    Prenote_Count += 1

                    ' Prenotes have zeros for the values
                    Debit_Amount = 0
                    Credit_Amount = 0

                Case ENTRY_TRANSACTION_CODE_REVERSAL
                    Batch_Debit += Debit_Amount

                Case ENTRY_TRANSACTION_CODE_PAYMENT
                    Batch_Credit += Debit_Amount

                Case Else
                    MsgBox("The transaction type " + Transaction_Code + " is not defined. This will result in the record being skipped and will cause the RPS file to be out of balance.", vbOKOnly, "RPS Processing Error")
                    Exit Sub
            End Select

            ' Build the record
            txt.Append("6")
            txt.Append(Transaction_Code)
            txt.Append(" "c, 9)
            txt.Append(" "c, 17)
            txt.Append(Debit_Amount.ToString("0000000000"))
            txt.Append(Credit_Amount.ToString("0000000000"))
            txt.Append(Account_Name)
            txt.Append(Account_Number)
            txt.Append(" "c, 2)
            txt.Append("0")
            txt.Append(Trace_Number)

            ' Write the record
            If txt.Length <> 94 Then Throw New ApplicationException("Detail record length <> 94 characters")
            WriteRecord(txt.ToString())

            ' Update batch totals
            Batch_DetailCount += 1
        End Sub

        Public Function create_eft_file(ByVal file_type As String) As Int32
            Dim Result As Int32 = -1

            ' Find the right most 50 characters of the filename
            Dim local_filename As String = FileName
            If local_filename.Length > 50 Then local_filename = "..." + local_filename.Substring(local_filename.Length - 47)

            ' Create the file in the database
            Dim cn As SqlConnection = New SqlConnection(DebtPlus.LINQ.SQLInfoClass.getDefault().ConnectionString)
            Dim current_cursor As Cursor = Cursor.Current
            Try
                Cursor.Current = Cursors.WaitCursor
                cn.Open()
                Using cmd As SqlCommand = New SqlCommand
                    With cmd
                        .Connection = cn
                        .CommandText = "xpr_rpps_create_file"
                        .CommandType = CommandType.StoredProcedure
                        .Parameters.Add("@RETURN_VALUE", SqlDbType.Int).Direction = ParameterDirection.ReturnValue
                        .Parameters.Add("@bank", SqlDbType.Int).Value = bank
                        .Parameters.Add("@filename", SqlDbType.VarChar, 80).Value = local_filename
                        .Parameters.Add("@file_type", SqlDbType.VarChar, 80).Value = file_type

                        .ExecuteNonQuery()
                        Result = Convert.ToInt32(.Parameters(0).Value)
                    End With
                End Using

            Finally
                If cn IsNot Nothing Then cn.Dispose()
                Cursor.Current = current_cursor
            End Try

            Return Result
        End Function

        ''' <summary>
        ''' Update the count values for subsequent batches today
        ''' </summary>
        Private Function update_batch_counts() As Boolean
            Dim answer As Boolean = False

            ' Correct the transaciton trace number so that it is never 9000000 or larger.
            ' We do wrap at 10000000, but this causes an invalid trace number condition to occur
            ' since Matercard never allows the counters to be wrap in a single file. They can
            ' start at 0000001, but going from 9999999 to 0000001 is not allowed.
            Dim NewBatchTrace As Long = (Batch_ID + TotalFile_BatchCount) Mod 10000000
            If NewBatchTrace >= 9000000 Then
                NewBatchTrace -= 9000000
            End If

            ' They never allow a trace number of zero. So, if it is zero then make it 1. (This is an almost impossible condition to occur, but still.)
            If NewBatchTrace = 0 Then
                NewBatchTrace = 1
            End If

            ' Do the same thing to the transaction trace ID
            Dim NewTransactionTrace As Long = (transaction_id + transaction_number) Mod 10000000
            If NewTransactionTrace >= 9000000 Then
                NewTransactionTrace -= 9000000
            End If

            ' They never allow a trace number of zero. So, if it is zero then make it 1. (This is an almost impossible condition to occur, but still.)
            If NewTransactionTrace = 0 Then
                NewTransactionTrace = 1
            End If

            ' Update the banks table with the correct information for the next generated file
            Dim cn As SqlConnection = New SqlConnection(DebtPlus.LINQ.SQLInfoClass.getDefault().ConnectionString)
            Dim current_cursor As Cursor = Cursor.Current
            Try
                Cursor.Current = Cursors.WaitCursor
                cn.Open()
                Using cmd As SqlCommand = New SqlCommand
                    With cmd
                        .Connection = cn
                        .CommandTimeout = System.Math.Max(300, DebtPlus.LINQ.SQLInfoClass.getDefault().CommandTimeout)
                        .CommandText = "UPDATE [banks] SET [batch_number]=@new_batch_number, [transaction_number]=@new_transaction_number WHERE [bank]=@bank AND [batch_number] = @old_batch_number AND [transaction_number] = @old_transaction_number"
                        .CommandType = CommandType.Text
                        .Parameters.Add("@new_batch_number", SqlDbType.Int).Value = NewBatchTrace
                        .Parameters.Add("@new_transaction_number", SqlDbType.Int).Value = NewTransactionTrace
                        .Parameters.Add("@bank", SqlDbType.Int).Value = bank
                        .Parameters.Add("@old_batch_number", SqlDbType.Int).Value = Batch_ID
                        .Parameters.Add("@old_transaction_number", SqlDbType.Int).Value = transaction_id

                        answer = (.ExecuteNonQuery() = 1)
                    End With
                End Using

            Finally
                If cn IsNot Nothing Then cn.Dispose()
                Cursor.Current = current_cursor
            End Try

            Return answer
        End Function

        ''' <summary>
        ''' Generate a RPPS Batch ID
        ''' </summary>
        Private Function Next_Batch(ByVal rpps_biller_id As String, ByVal biller_name As String, Optional ByVal service_class As String = SERVICE_CLASS_CREDIT, Optional ByVal description As String = ENTRY_DESCRIPTION_PAYMENT) As Int32
            Dim Result As Int32 = -1

            ' Clear the batch counters
            Batch_DetailCount = 0
            Batch_Debit = 0
            Batch_Credit = 0

            ' Generate the batch trace number
            TotalFile_BatchCount += 1
            Dim strBatchID As String = Next_Batch_Trace()
            If status_form IsNot Nothing Then status_form.batch = strBatchID

            Dim cn As SqlConnection = New SqlConnection(DebtPlus.LINQ.SQLInfoClass.getDefault().ConnectionString)
            Dim current_cursor As Cursor = Cursor.Current
            Try
                Cursor.Current = Cursors.WaitCursor
                cn.Open()
                Using cmd As SqlCommand = New SqlCommand
                    With cmd
                        .Connection = cn
                        .CommandText = "xpr_rpps_create_batch"
                        .CommandType = CommandType.StoredProcedure
                        .CommandTimeout = DebtPlus.LINQ.SQLInfoClass.getDefault().CommandTimeout

                        .Parameters.Add("@RETURN_VALUE", SqlDbType.Int).Direction = ParameterDirection.ReturnValue
                        .Parameters.Add("@FileNumber", SqlDbType.Int).Value = eft_file
                        .Parameters.Add("@BillerID", SqlDbType.VarChar, 80).Value = rpps_biller_id
                        .Parameters.Add("@TraceNumber", SqlDbType.VarChar, 80).Value = strBatchID
                        .Parameters.Add("@ServiceClass", SqlDbType.VarChar, 80).Value = service_class
                        .Parameters.Add("@BillerName", SqlDbType.VarChar, 80).Value = biller_name
                        .ExecuteNonQuery()

                        Result = Convert.ToInt32(.Parameters(0).Value)
                    End With
                End Using

            Finally
                If cn IsNot Nothing Then cn.Dispose()
                Cursor.Current = current_cursor
            End Try

            ' Save the information for the batch control's later processing
            open_batch_id = strBatchID
            open_biller_id = rpps_biller_id
            Open_Biller_Name = biller_name
            Open_Biller_Class = service_class
            Open_Biller_Description = description

            ' Write the batch header
            If Result > 0 Then Write_Batch_Header()
            Return Result
        End Function

        ''' <summary>
        ''' Format the Decimal value as a long value in cents
        ''' </summary>
        Private Function FormattedMoney(ByVal curValue As Object) As Int32
            Dim answer As Int32 = 0

            If curValue IsNot Nothing AndAlso curValue IsNot DBNull.Value Then

                If TypeOf curValue Is Decimal Then
                    Dim ItemValue As Decimal = DirectCast(curValue, Decimal) * 100D
                    answer = Convert.ToInt32(Decimal.Floor(ItemValue))

                ElseIf TypeOf curValue Is Int32 Then
                    answer = DirectCast(curValue, Int32)

                Else
                    Throw New ArgumentException("Value must be decimal or Int32")
                End If
            End If

            Return answer
        End Function

        ''' <summary>
        ''' Generate the account name value for the detail
        ''' </summary>
        Private Function AccountName(ByVal varName As Object) As String
            Dim InputName As String = DebtPlus.Utils.Nulls.DStr(varName)
            InputName = clean_string(InputName)

            ' The criteria is more strict here. Toss all but printable characters/
            InputName = Regex.Replace(InputName, "[^0-9A-Z]", String.Empty, RegexOptions.Singleline Or RegexOptions.IgnoreCase)
            Return FixedField(InputName, 5).ToUpper()
        End Function

        ''' <summary>
        ''' Generate the account number value for the detail
        ''' </summary>
        Private Function AccountNumber(ByVal varName As Object) As String
            Dim InputNumber As String = DebtPlus.Utils.Nulls.DStr(varName)
            InputNumber = clean_string(InputNumber)

            InputNumber = Regex.Replace(InputNumber, "[^- 0-9A-Z]", String.Empty, RegexOptions.Singleline Or RegexOptions.IgnoreCase)
            Return FixedField(InputNumber, 22).ToUpper()
        End Function

        ''' <summary>
        ''' Write records for a "net" biller
        ''' </summary>
        Private Sub Write_Net_Biller()
            Dim strName As String
            Dim trace_number_last As String
            Dim trace_number_first As String
            Dim rpps_batch As Int32

            ' Open a new group for this biller
            rpps_batch = Next_Batch(Convert.ToString(drv("rpps_biller_id")), Convert.ToString(drv("biller_name")))

            ' Process the records for this biller
            Do While RowNumber < vue.Count
                drv = vue(RowNumber)

                ' Stop if the biller IDs differ
                If Convert.ToString(drv("rpps_biller_id")) <> open_biller_id Then Exit Do

                Dim TransactionBilled As Int32 = FormattedMoney(drv("billed"))
                Dim TransactionGross As Int32 = FormattedMoney(drv("amount"))
                Dim TransactionDeduct As Int32 = FormattedMoney(drv("deducted"))
                Dim TransactionNet As Int32 = TransactionGross - TransactionDeduct
                Dim TransactionCode As String = Convert.ToString(drv("transaction_code"))

                ' Generate a trace number for this transaction
                trace_number_last = Next_Detail_Trace()
                trace_number_first = trace_number_last

                ' Use the supplied name if one was given. Otherwise, generate one from our copy of the name
                strName = AccountName(drv("account_name"))

                If TransactionCode = "23" Then
                    Write_Detail(trace_number_last,
                                 TransactionCode,
                                 0,
                                 0,
                                 AccountNumber(drv("account_number")),
                                 strName)
                Else
                    ' Accumulate the total amount of money billed into the form for display purposes only
                    TotalFile_Billed += TransactionBilled
                    TotalFile_Net += TransactionNet
                    TotalFile_Gross += TransactionGross

                    ' DebitAmount = "net" transfer
                    ' CreditAmount = "gross" transfer
                    Write_Detail(trace_number_last,
                                 TransactionCode,
                                 TransactionNet,
                                 TransactionGross,
                                 AccountNumber(drv("account_number")),
                                 strName)
                End If

                ' Set the parameters for the transactions update
                drv.BeginEdit()
                drv("trace_number_first") = trace_number_first
                drv("trace_number_last") = trace_number_last
                drv("rpps_batch") = rpps_batch
                drv("rpps_file") = eft_file
                drv.EndEdit()

                ' Go to the next record in the list
                RowNumber += 1
            Loop

            ' Update the totals for the file processing
            TotalFile_DetailCount += Batch_DetailCount
            TotalFile_Debit += Batch_Debit
            TotalFile_Credit += Batch_Credit

            ' Close the current batch
            Write_Batch_Control()
        End Sub

        ''' <summary>
        ''' Write records for a "gross" biller
        ''' </summary>
        Private Sub Write_Gross_Biller()
            Dim strName As String
            Dim trace_number_last As String
            Dim trace_number_first As String
            Dim rpps_batch As Int32

            ' Open a new group for this biller
            rpps_batch = Next_Batch(Convert.ToString(drv("rpps_biller_id")), Convert.ToString(drv("biller_name")))

            ' Process the records for this biller
            Do While RowNumber < vue.Count
                drv = vue(RowNumber)

                ' Stop if the biller IDs differ
                If Convert.ToString(drv("rpps_biller_id")) <> open_biller_id Then Exit Do

                ' Generate a trace number for this transaction
                trace_number_last = Next_Detail_Trace()

                ' Use the supplied name if one was given. Otherwise, generate one from our copy of the name
                strName = AccountName(drv("account_name"))

                ' Update the database to reflect the assignment to the batch and trace number
                trace_number_first = trace_number_last

                Dim TransactionBilled As Int32 = FormattedMoney(drv("billed"))
                Dim TransactionGross As Int32 = FormattedMoney(drv("amount"))
                Dim TransactionDeduct As Int32 = FormattedMoney(drv("deducted"))
                Dim TransactionNet As Int32 = TransactionGross - TransactionDeduct
                Dim TransactionCode As String = Convert.ToString(drv("transaction_code"))

                If TransactionCode = "23" Then
                    Write_Detail(trace_number_last,
                                 TransactionCode,
                                 0,
                                 0,
                                 AccountNumber(drv("account_number")),
                                 strName)
                Else

                    ' Accumulate the total amount of money billed
                    TotalFile_Billed += TransactionBilled
                    TotalFile_Net += TransactionNet
                    TotalFile_Gross += TransactionGross

                    ' DebitAmount = "net" transfer
                    ' CreditAmount = "net" transfer
                    Write_Detail(trace_number_last,
                                 TransactionCode,
                                 TransactionNet,
                                 TransactionNet,
                                 AccountNumber(drv("account_number")),
                                 strName)
                End If

                ' Set the parameters for the transactions update
                drv.BeginEdit()
                drv("trace_number_first") = trace_number_first
                drv("trace_number_last") = trace_number_last
                drv("rpps_batch") = rpps_batch
                drv("rpps_file") = eft_file
                drv.EndEdit()

                ' Go to the next record in the list
                RowNumber += 1
            Loop

            ' Update the totals for the file processing
            TotalFile_DetailCount += Batch_DetailCount
            TotalFile_Debit += Batch_Debit
            TotalFile_Credit += Batch_Credit

            ' Close the current batch
            Write_Batch_Control()
        End Sub

        ''' <summary>
        ''' Retrieve the account number for the modified gross biller
        ''' </summary>
        Private Function ReversalAccountNumber(ByVal strReversalBillerID As String) As String
            Dim cn As SqlConnection = New SqlConnection(DebtPlus.LINQ.SQLInfoClass.getDefault().ConnectionString)
            Dim Result As String = "1999999999"

            Try
                cn.Open()
                Using cmd As SqlCommand = New SqlCommand
                    With cmd
                        .Connection = cn
                        .CommandTimeout = DebtPlus.LINQ.SQLInfoClass.getDefault().CommandTimeout
                        .CommandText = "SELECT TOP 1 m.mask FROM rpps_masks m WITH (NOLOCK) INNER JOIN rpps_biller_ids ids WITH (NOLOCK) ON ids.rpps_biller_id = m.rpps_biller_id WHERE ids.rpps_biller_id=@rpps_mask"
                        .Parameters.Add("@rpps_mask", SqlDbType.VarChar, 80).Value = strReversalBillerID

                        Dim objResult As Object = .ExecuteScalar
                        If objResult IsNot Nothing AndAlso objResult IsNot DBNull.Value Then Result = Convert.ToString(objResult)
                    End With
                End Using

            Finally
                If cn IsNot Nothing Then cn.Dispose()
            End Try

            Return Result
        End Function

        ''' <summary>
        ''' Write records for a "modified gross" biller
        ''' </summary>
        Private Sub Write_Modified_Gross_Biller()
            Dim strName As String
            Dim trace_number_last As String
            Dim trace_number_first As String
            Dim Modified_Gross_Deduct As Int32 = 0
            Dim strCreditor As String
            Dim rpps_batch As Int32
            Dim strReversalBillerID As String
            Dim strReversalAccountNumber As String

            ' Open a new group for this biller
            rpps_batch = Next_Batch(Convert.ToString(drv("rpps_biller_id")), Convert.ToString(drv("biller_name")))

            ' Save the creditor ID for the crediting function later
            strCreditor = String.Empty
            If drv("creditor") IsNot DBNull.Value Then strCreditor = Convert.ToString(drv("creditor"))

            ' Remember the ID for the reversal process later
            strReversalBillerID = Convert.ToString(drv("rpps_biller_id"))
            If drv("reversal_biller_id") IsNot DBNull.Value Then strReversalBillerID = Convert.ToString(drv("reversal_biller_id"))
            strReversalAccountNumber = ReversalAccountNumber(strReversalBillerID)

            ' Process the records for this biller
            Do While RowNumber < vue.Count
                drv = vue(RowNumber)

                ' Stop if the biller IDs differ
                If Convert.ToString(drv("rpps_biller_id")) <> open_biller_id Then Exit Do

                ' Generate a trace number for this transaction
                trace_number_last = Next_Detail_Trace()
                trace_number_first = trace_number_last

                ' Use the supplied name if one was given. Otherwise, generate one from our copy of the name
                strName = AccountName(drv("account_name"))

                Dim TransactionBilled As Int32 = FormattedMoney(drv("billed"))
                Dim TransactionGross As Int32 = FormattedMoney(drv("amount"))
                Dim TransactionDeduct As Int32 = FormattedMoney(drv("deducted"))
                Dim TransactionNet As Int32 = TransactionGross - TransactionDeduct
                Dim TransactionCode As String = Convert.ToString(drv("transaction_code"))

                If TransactionCode = "23" Then
                    Write_Detail(trace_number_last,
                                 TransactionCode,
                                 0,
                                 0,
                                 AccountNumber(drv("account_number")),
                                 strName)
                Else
                    ' Accumulate the total amount of money billed
                    TotalFile_Billed += TransactionBilled
                    TotalFile_Net += TransactionNet
                    TotalFile_Gross += TransactionGross

                    Modified_Gross_Deduct += TransactionDeduct

                    ' DebitAmount = "gross" transfer
                    ' CreditAmount = "net" transfer
                    Write_Detail(trace_number_last,
                                 TransactionCode,
                                 TransactionGross,
                                 TransactionNet,
                                 AccountNumber(drv("account_number")),
                                 strName)
                End If

                ' Set the parameters for the transactions update
                drv.BeginEdit()
                drv("trace_number_first") = trace_number_first
                drv("trace_number_last") = trace_number_last
                drv("rpps_batch") = rpps_batch
                drv("rpps_file") = eft_file
                drv.EndEdit()

                ' Go to the next record in the list
                RowNumber += 1
            Loop

            ' Update the totals for the file processing
            TotalFile_DetailCount += Batch_DetailCount
            TotalFile_Debit += Batch_Debit
            TotalFile_Credit += Batch_Credit

            ' Close the current batch
            Write_Batch_Control()

            ' If there is a deducted amount then generate a new batch for the deduction
            If Modified_Gross_Deduct > 0D Then

                ' Clear the batch counters
                Batch_DetailCount = 0
                Batch_Debit = 0
                Batch_Credit = 0

                ' Generate a transaction record to hold the reversal entry
                Dim cn As SqlConnection = New SqlConnection(DebtPlus.LINQ.SQLInfoClass.getDefault().ConnectionString)
                Dim current_cursor As Cursor = Cursor.Current
                Try
                    Cursor.Current = Cursors.WaitCursor
                    cn.Open()

                    rpps_batch = Next_Batch(strReversalBillerID, Open_Biller_Name, "225", "REVERSAL  ")
                    trace_number_last = Next_Detail_Trace()

                    Using cmd As SqlCommand = New SqlCommand
                        With cmd
                            .Connection = cn
                            .CommandTimeout = System.Math.Max(300, DebtPlus.LINQ.SQLInfoClass.getDefault().CommandTimeout)
                            .CommandText = "xpr_rpps_cie_modified_gross"
                            .CommandType = CommandType.StoredProcedure

                            .Parameters.Add("@creditor", SqlDbType.VarChar, 80).Value = strCreditor
                            .Parameters.Add("@trace_number", SqlDbType.VarChar, 80).Value = trace_number_last
                            .Parameters.Add("@rpps_batch", SqlDbType.Int).Value = rpps_batch
                            .Parameters.Add("@biller_id", SqlDbType.VarChar, 80).Value = open_biller_id
                            .Parameters.Add("@account_number", SqlDbType.VarChar, 80).Value = strReversalAccountNumber
                            .Parameters.Add("@rpps_file", SqlDbType.Int).Value = eft_file

                            .ExecuteNonQuery()
                        End With
                    End Using

                    ' Generate the items for the transaction in the file
                    Write_Detail(trace_number_last,
                                 "27",
                                 Modified_Gross_Deduct,
                                 0,
                                 AccountNumber(strReversalAccountNumber),
                                 "     ")

                    ' Update the totals for the file processing
                    TotalFile_DetailCount += Batch_DetailCount
                    TotalFile_Debit += Batch_Debit
                    TotalFile_Credit += Batch_Credit

                    ' Close the current batch
                    Write_Batch_Control()

                Finally
                    If cn IsNot Nothing Then cn.Dispose()

                    Cursor.Current = current_cursor
                End Try
            End If
        End Sub

        ''' <summary>
        ''' Generate prenotes for the current file
        ''' </summary>
        Private Function process_prenote_file(ByVal useLocalFile As Boolean) As Boolean
            Dim answer As Boolean = True

            ' From the transaction, determine the current bank and region information
            bank = Convert.ToInt32(drv("bank"))
            eft_file = 0

            ' Read the file configuration data for the new file
            Fake = Not ReadPaymentConfiguration()

            ' Process the records in the batch based upon the type of biller
            Do While RowNumber < vue.Count
                drv = vue(RowNumber)

                ' If the bank changed then we need a new file.
                If Convert.ToInt32(drv("bank")) <> bank Then Exit Do
                If Not Fake Then

                    ' Create an EFT text file
                    If OutputTextFile Is Nothing Then
                        OutputTextFile = status_form.Request_FileName(disbursement_register, bank, "PNOTE", useLocalFile)
                        If OutputTextFile Is Nothing Then
                            Fake = True
                        End If
                    End If

                    ' Write the file header to the output text file
                    If Not Fake Then
                        If RPS_Prefix <> String.Empty Then
                            OutputTextFile.WriteLine(RPS_Prefix)
                        End If
                        Write_File_Header()
                    End If
                End If

                ' Generate the prenote operation
                If Fake Then
                    RowNumber += 1
                Else
                    ' Do a payment operation
                    If eft_file <= 0 Then eft_file = create_eft_file("EFT")
                    BillerType = Convert.ToInt32(drv("biller_type"))

                    Select Case BillerType
                        Case 2, 12 ' GROSS BILLER
                            Write_Gross_Biller()

                        Case 3, 13 ' MODIFIED GROSS BILLER
                            Write_Modified_Gross_Biller()

                        Case Else ' BILLER_TYPE_NET (and other invalid items)
                            Write_Net_Biller()
                    End Select
                End If
            Loop

            If Not Fake Then

                ' Write the file trailer to the output
                Write_File_Control()

                ' Write the suffix line to the output file
                If RPS_Suffix <> String.Empty Then OutputTextFile.WriteLine(RPS_Suffix)

                ' Close the output file
                OutputTextFile.Close()
                OutputTextFile = Nothing

                ' Update the system with the batch data
                answer = update_batch_counts()
            End If

            Return answer
        End Function

        ''' <summary>
        ''' Generate payments for the current file
        ''' </summary>
        Private Function process_payment_file(ByVal useLocalFile As Boolean) As Boolean
            Dim answer As Boolean = True

            ' From the transaction, determine the current bank and region information
            bank = Convert.ToInt32(drv("bank"))
            eft_file = 0

            ' Read the file configuration data for the new file
            Fake = Not ReadPaymentConfiguration()

            ' Retrieve the initial region for processing
            If Not Fake Then
                OutputTextFile = status_form.Request_FileName(disbursement_register, bank, "PYMT", useLocalFile)
                If OutputTextFile Is Nothing Then
                    Fake = True
                End If
            End If

            ' Write the file header to the output text file
            If Not Fake Then
                If RPS_Prefix <> String.Empty Then
                    OutputTextFile.WriteLine(RPS_Prefix)
                End If
                Write_File_Header()
            End If

            ' Process the records in the batch based upon the type of biller
            Do While RowNumber < vue.Count
                drv = vue(RowNumber)

                ' If the bank changed then we need a new file.
                If Convert.ToInt32(drv("bank")) <> bank Then Exit Do

                ' Create an EFT file
                If eft_file <= 0 Then eft_file = create_eft_file("EFT")

                ' Generate the payment operation
                If Fake Then
                    RowNumber += 1
                Else
                    ' Do a payment operation
                    BillerType = Convert.ToInt32(drv("biller_type"))

                    Select Case BillerType
                        Case 2, 12 ' GROSS BILLER
                            Write_Gross_Biller()

                        Case 3, 13 ' MODIFIED GROSS BILLER
                            Write_Modified_Gross_Biller()

                        Case Else ' BILLER_TYPE_NET (and other invalid items)
                            Write_Net_Biller()
                    End Select
                End If
            Loop

            If Not Fake Then
                ' Write the file trailer to the output
                Write_File_Control()

                ' Write the suffix line to the output file
                If RPS_Suffix <> String.Empty Then OutputTextFile.WriteLine(RPS_Suffix)

                ' Close the output file and flush to the disk
                OutputTextFile.Close()
                OutputTextFile = Nothing

                ' Update the system with the batch data
                answer = update_batch_counts()
            End If

            Return answer
        End Function

        ''' <summary>
        ''' Process the prenote requests for the regions
        ''' </summary>
        Public Function Process_Prenotes(ByRef status_form As Iform_RPS, Optional ByVal useLocalFile As Boolean = False) As Boolean
            Dim answer As Boolean = True

            ' Set the global pointer to the form
            Me.status_form = status_form

            ' Generate the prenotes for all regions.
            Generate_Payment_Prenotes(-1)

            ' Read the list of prenotes
            ds = New DataSet("ds")
            Using select_cmd As SqlCommand = New SqlCommand
                With select_cmd
                    .Connection = New SqlConnection(DebtPlus.LINQ.SQLInfoClass.getDefault().ConnectionString)
                    .CommandTimeout = System.Math.Max(600, DebtPlus.LINQ.SQLInfoClass.getDefault().CommandTimeout)
                    .CommandText = "SELECT [rpps_transaction],[bank],[rpps_file],[creditor],[transaction_code],[client],[deducted],[billed],[amount],[account_number],[account_name],[rpps_biller_id],[reversal_biller_id],[biller_name],[biller_type],[trace_number_first],[trace_number_last],[rpps_batch] FROM view_rpps_cie_info WHERE [transaction_code] = @transaction_code"
                    .Parameters.Add("@transaction_code", SqlDbType.VarChar, 80).Value = ENTRY_TRANSACTION_CODE_PRENOTE.ToString()

                    If bank > 0 Then
                        .CommandText += " AND [bank] = @bank"
                        .Parameters.Add("@bank", SqlDbType.Int).Value = bank
                    End If
                End With

                Using da As New SqlDataAdapter(select_cmd)
                    da.Fill(ds, "prenotes")
                    tbl = ds.Tables(0)

                    With tbl

                        ' Ensure that the columns for update are present in the view
                        If Not .Columns.Contains("trace_number_first") Then
                            With .Columns.Add("trace_number_first", GetType(String))
                                .ReadOnly = False
                            End With
                        End If

                        If Not .Columns.Contains("trace_number_last") Then
                            With .Columns.Add("trace_number_last", GetType(String))
                                .ReadOnly = False
                            End With
                        End If

                        If Not .Columns.Contains("rpps_batch") Then
                            With .Columns.Add("rpps_batch", GetType(Int32))
                                .ReadOnly = False
                            End With
                        End If

                        If Not .Columns.Contains("rpps_file") Then
                            With .Columns.Add("rpps_file", GetType(Int32))
                                .ReadOnly = False
                            End With
                        End If
                    End With

                    vue = New DataView(tbl, String.Empty, "bank,rpps_biller_id", DataViewRowState.CurrentRows)
                    If vue.Count > 0 Then

                        ' Process the prenote file with the prenotes for all banks.
                        RowNumber = 0
                        drv = vue(0)
                        answer = process_prenote_file(useLocalFile)

                        ' Update the database once we have processed the rows
                        If answer Then
                            answer = UpdateTables(tbl)
                        End If
                    End If
                End Using
            End Using

            Return answer
        End Function

        ''' <summary>
        ''' Process the payment requests for the regions
        ''' </summary>
        Public Function Process_Payments(ByVal status_form As Iform_RPS, Optional ByVal useLocalFile As Boolean = False) As Boolean
            Dim answer As Boolean = True

            ' Set the global pointer to the form
            Me.status_form = status_form

            ' Read the list of payments
            ds = New DataSet("ds")
            Using select_cmd As SqlCommand = New SqlCommand
                With select_cmd
                    .Connection = New SqlConnection(DebtPlus.LINQ.SQLInfoClass.getDefault().ConnectionString)
                    .CommandTimeout = System.Math.Max(600, DebtPlus.LINQ.SQLInfoClass.getDefault().CommandTimeout)
                    .CommandText = "SELECT [rpps_transaction],[bank],[rpps_file],[creditor],[transaction_code],[client],[deducted],[billed],[amount],[account_number],[account_name],[rpps_biller_id],[reversal_biller_id],[biller_name],[biller_type],[trace_number_first],[trace_number_last],[rpps_batch] FROM view_rpps_cie_info WHERE [transaction_code] <> @transaction_code"
                    .Parameters.Add("@transaction_code", SqlDbType.VarChar, 80).Value = ENTRY_TRANSACTION_CODE_PRENOTE.ToString()

                    If bank > 0 Then
                        .CommandText += " AND [bank]=@bank"
                        .Parameters.Add("@bank", SqlDbType.Int).Value = bank
                    End If
                End With

                Using da As New SqlDataAdapter(select_cmd)
                    da.Fill(ds, "payments")
                End Using
            End Using

            tbl = ds.Tables(0)
            With tbl

                ' Ensure that the columns for update are present in the view
                If Not .Columns.Contains("trace_number_first") Then
                    With .Columns.Add("trace_number_first", GetType(String))
                        .ReadOnly = False
                    End With
                End If

                If Not .Columns.Contains("trace_number_last") Then
                    With .Columns.Add("trace_number_last", GetType(String))
                        .ReadOnly = False
                    End With
                End If

                If Not .Columns.Contains("rpps_batch") Then
                    With .Columns.Add("rpps_batch", GetType(Int32))
                        .ReadOnly = False
                    End With
                End If

                If Not .Columns.Contains("rpps_file") Then
                    With .Columns.Add("rpps_file", GetType(Int32))
                        .ReadOnly = False
                    End With
                End If
            End With

            vue = New DataView(tbl, String.Empty, "bank,rpps_biller_id", DataViewRowState.CurrentRows)

            If vue.Count > 0 Then
                RowNumber = 0
                drv = vue(0)
                answer = process_payment_file(useLocalFile)

                ' Update the rows
                If answer Then
                    answer = UpdateTables(tbl)
                End If
            End If

            Return answer
        End Function

        ''' <summary>
        ''' Update the trace numbers on the transactions when required
        ''' </summary>
        Private Function UpdateTables(ByVal tbl As DataTable) As Boolean
            Dim answer As Boolean = False

            Dim current_cursor As Cursor = Cursor.Current
            Dim cn As SqlConnection = New SqlConnection(DebtPlus.LINQ.SQLInfoClass.getDefault().ConnectionString)
            Dim txn As SqlTransaction = Nothing
            Try
                Cursor.Current = Cursors.WaitCursor
                cn.Open()
                txn = cn.BeginTransaction

                ' Generate the update statement to the transactions
                Using update_cmd As SqlCommand = New SqlCommand
                    With update_cmd
                        .Connection = cn
                        .Transaction = txn
                        .CommandText = "UPDATE rpps_transactions SET [trace_number_first]=@trace_number_first, [trace_number_last]=@trace_number_last, [rpps_batch]=@rpps_batch, [rpps_file]=@rpps_file, [death_date]=dateadd(d, 180, getdate()) WHERE [rpps_transaction]=@rpps_transaction AND [trace_number_first] IS NULL;"

                        .Parameters.Add("@trace_number_first", SqlDbType.VarChar, CByte(80), "trace_number_first")
                        .Parameters.Add("@trace_number_last", SqlDbType.VarChar, CByte(80), "trace_number_last")
                        .Parameters.Add("@rpps_batch", SqlDbType.Int, CByte(0), "rpps_batch")
                        .Parameters.Add("@rpps_file", SqlDbType.Int, CByte(0), "rpps_file")
                        .Parameters.Add("@rpps_transaction", SqlDbType.Int, CByte(0), "rpps_transaction")
                    End With

                    ' Build the update command to change the rows
                    Dim rows_updated As Int32
                    Using update_da As New SqlDataAdapter
                        With update_da
                            .UpdateCommand = update_cmd
                            rows_updated = .Update(tbl)
                        End With
                    End Using
                End Using

                txn.Commit()
                txn.Dispose()
                txn = Nothing
                answer = True

            Finally
                If txn IsNot Nothing Then
                    Try
                        txn.Rollback()
                    Catch exRollback As Exception
                    End Try
                    txn.Dispose()
                    txn = Nothing
                End If

                If cn IsNot Nothing Then cn.Dispose()

                Cursor.Current = current_cursor
            End Try

            Return answer
        End Function
    End Class
End Namespace
