#Region "Copyright 2000-2012 DebtPlus, L.L.C."
'{*******************************************************************}
'{                                                                   }
'{       DebtPlus Debt Management System                             }
'{                                                                   }
'{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
'{       ALL RIGHTS RESERVED                                         }
'{                                                                   }
'{   The entire contents of this file is protected by U.S. and       }
'{   International Copyright Laws. Unauthorized reproduction,        }
'{   reverse-engineering, and distribution of all or any portion of  }
'{   the code contained in this file is strictly prohibited and may  }
'{   result in severe civil and criminal penalties and will be       }
'{   prosecuted to the maximum extent possible under the law.        }
'{                                                                   }
'{   RESTRICTIONS                                                    }
'{                                                                   }
'{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
'{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
'{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
'{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
'{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
'{                                                                   }
'{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
'{   ADDITIONAL RESTRICTIONS.                                        }
'{                                                                   }
'{*******************************************************************}
#End Region

Imports DebtPlus.Interfaces.Debt
Imports DebtPlus.Interfaces.Disbursement
Imports DebtPlus.Svc.Common
Imports DebtPlus.Svc.Debt
Imports DebtPlus.Utils
Imports System.Data.SqlClient
Imports System.Collections.Generic
Imports System.ComponentModel
Imports System.Data
Imports System.Drawing
Imports System.Linq
Imports System.Text
Imports System.Windows.Forms
Imports System.Diagnostics
Imports DevExpress.XtraEditors.Controls

Namespace Disbursement.Common

    ''' <summary>
    ''' Debts for the disbursement operation come from the disbursement
    ''' tables and not the debts table. As such, they don't have as many
    ''' fields as the normal debts and some of the fields that are read-only
    ''' on the normal debts are read/write here. But, these debts need to be
    ''' proratable as well as able to calculate the monthly fees.
    ''' </summary>
    Partial Class DisbursementDebtRecord
        Implements DebtPlus.Interfaces.Client.IClient
        Implements DebtPlus.Interfaces.Creditor.ICreditor
        Implements DebtPlus.Interfaces.Debt.IDebtRecord
        Implements DebtPlus.Interfaces.Debt.INotifyDataChanged
        Implements DebtPlus.Interfaces.Debt.IProratable
        Implements System.IDisposable
        Implements System.IComparable
        Implements System.IComparable(Of DebtPlus.UI.Desktop.Disbursement.Common.DisbursementDebtRecord)
        Implements System.ComponentModel.INotifyPropertyChanged
        Implements System.ComponentModel.ISupportInitialize

        Protected InInit As Boolean = False     ' Track the BeginInit / EndInit sequences

        ''' <summary>
        ''' Client ID
        ''' </summary>
        ''' <value></value>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public Property ClientId() As Integer Implements DebtPlus.Interfaces.Client.IClient.ClientId
            Get
                Return 0
            End Get
            Set(value As Integer)
            End Set
        End Property

        Public m_Creditor As String
        ''' <summary>
        ''' Creditor ID
        ''' </summary>
        ''' <value></value>
        ''' <returns></returns>
        ''' <remarks></remarks>
        <DebtPlus.Svc.Debt.DatabaseField("Creditor", DebtPlus.Svc.Debt.DatabaseFieldAttribute.FieldType.TypeString)> _
        Public Property Creditor() As String Implements Interfaces.Creditor.ICreditor.Creditor
            Get
                Return m_Creditor
            End Get
            Set(value As String)
                m_Creditor = value
                RaiseDataChanged("Creditor")
            End Set
        End Property

        ''' <summary>
        ''' System.ComponentModel.INotifyPropertyChanged
        ''' </summary>
        ''' <remarks></remarks>
        Public Event PropertyChanged As System.ComponentModel.PropertyChangedEventHandler Implements System.ComponentModel.INotifyPropertyChanged.PropertyChanged

        ''' <summary>
        ''' Raise the property changed event
        ''' </summary>
        ''' <param name="e"></param>
        ''' <remarks></remarks>
        Protected Sub RaisePropertyChanged(e As System.ComponentModel.PropertyChangedEventArgs)
            If Not InInit Then
                RaiseEvent PropertyChanged(Me, e)
            End If
        End Sub

        ''' <summary>
        ''' Raise the property changed event
        ''' </summary>
        ''' <param name="PropertyName"></param>
        ''' <remarks></remarks>
        Public Sub RaisePropertyChanged(PropertyName As String) Implements DebtPlus.Interfaces.Debt.INotifyDataChanged.RaisePropertyChanged
            RaisePropertyChanged(New System.ComponentModel.PropertyChangedEventArgs(PropertyName))
        End Sub

        Private m_OID As Int32 = 0
        ''' <summary>
        ''' ID of the debt record
        ''' </summary>
        ''' <value></value>
        ''' <returns></returns>
        ''' <remarks></remarks>
        <DebtPlus.Svc.Debt.DatabaseField("oID", DebtPlus.Svc.Debt.DatabaseFieldAttribute.FieldType.TypeInt32)> _
        Public Property oid() As Int32
            Get
                Return m_OID
            End Get
            Set(value As Int32)
                m_OID = value
                RaiseDataChanged("oid")
            End Set
        End Property

        Private m_ClientCreditor As Int32 = 0
        ''' <summary>
        ''' ID of the record associated with the payment information. This may change over the life of the debt.
        ''' </summary>
        <DebtPlus.Svc.Debt.DatabaseField("client_creditor", DebtPlus.Svc.Debt.DatabaseFieldAttribute.FieldType.TypeInt32)> _
        Public Property client_creditor() As Int32 Implements Interfaces.Debt.IDebtRecord.client_creditor
            Get
                Return m_ClientCreditor
            End Get
            Set(value As Int32)
                m_ClientCreditor = value
                RaiseDataChanged("client_creditor")
            End Set
        End Property

        ''' <summary>
        ''' Primary key to the debt table. This is the ID for the debt.
        ''' </summary>
        Public Property DebtId() As Int32 Implements Interfaces.Debt.IDebtRecord.DebtId
            Get
                Return client_creditor
            End Get
            Set(value As Int32)
                client_creditor = value
            End Set
        End Property

        Private m_SchedPayment As Decimal
        ''' <summary>
        ''' Scheduled payment for THIS MONTH
        ''' </summary>
        <DebtPlus.Svc.Debt.DatabaseField("sched_payment", DebtPlus.Svc.Debt.DatabaseFieldAttribute.FieldType.TypeDecimal)> _
        Public Property sched_payment() As Decimal Implements Interfaces.Debt.IDebtRecord.sched_payment, DebtPlus.Interfaces.Debt.IProratable.sched_payment
            Get
                Return m_SchedPayment
            End Get
            Set(value As Decimal)
                m_SchedPayment = value
                RaiseDataChanged("sched_payment")
            End Set
        End Property

        Private m_DisbursementFactor As Decimal
        ''' <summary>
        ''' Disbursement factor
        ''' </summary>
        <DebtPlus.Svc.Debt.DatabaseField("disbursement_factor", DebtPlus.Svc.Debt.DatabaseFieldAttribute.FieldType.TypeDecimal)> _
        Public Property disbursement_factor() As Decimal Implements Interfaces.Debt.IDebtRecord.disbursement_factor, DebtPlus.Interfaces.Debt.IProratable.disbursement_factor
            Get
                Return m_DisbursementFactor
            End Get
            Set(value As Decimal)
                m_DisbursementFactor = value
                RaiseDataChanged("disbursement_factor")
            End Set
        End Property

        Public Overridable Property display_disbursement_factor() As Decimal Implements Interfaces.Debt.IDebtRecord.display_disbursement_factor
            Get
                Return disbursement_factor
            End Get
            Set(value As Decimal)
                disbursement_factor = value
            End Set
        End Property

        Private m_PaymentsMonth0 As Decimal = 0D
        ''' <summary>
        ''' Total amount paid THIS MONTH on the debt
        ''' </summary>
        <DebtPlus.Svc.Debt.DatabaseField("current_month_disbursement", DebtPlus.Svc.Debt.DatabaseFieldAttribute.FieldType.TypeDecimal)> _
        Public Property payments_month_0() As Decimal Implements Interfaces.Debt.IDebtRecord.payments_month_0, DebtPlus.Interfaces.Debt.IProratable.payments_month_0
            Get
                Return m_PaymentsMonth0
            End Get
            Set(value As Decimal)
                m_PaymentsMonth0 = value
                RaiseDataChanged("payments_month_0")
            End Set
        End Property

        Private m_OrigBalance As Decimal = 0D
        ''' <summary>
        ''' Original balance information
        ''' </summary>
        <DebtPlus.Svc.Debt.DatabaseField("orig_balance", DebtPlus.Svc.Debt.DatabaseFieldAttribute.FieldType.TypeDecimal)> _
        Public Property orig_balance() As Decimal Implements Interfaces.Debt.IDebtRecord.orig_balance
            Get
                Return m_OrigBalance
            End Get
            Set(value As Decimal)
                m_OrigBalance = value
                RaiseDataChanged("orig_balance")
            End Set
        End Property

        Private m_OrigDMPPayment As Decimal = 0D
        ''' <summary>
        ''' Amount on the proposal that was accepted by the creditor
        ''' </summary>
        <DebtPlus.Svc.Debt.DatabaseField("orig_dmp_payment", DebtPlus.Svc.Debt.DatabaseFieldAttribute.FieldType.TypeDecimal)> _
        Public Property orig_dmp_payment() As Decimal Implements Interfaces.Debt.IDebtRecord.orig_dmp_payment
            Get
                Return m_OrigDMPPayment
            End Get
            Set(value As Decimal)
                m_OrigDMPPayment = value
                RaiseDataChanged("orig_dmp_payment")
            End Set
        End Property

        Private m_DMPInterst As Object = DBNull.Value
        ''' <summary>
        ''' DMP Interest rate
        ''' </summary>
        Public Property dmp_interest() As Object Implements Interfaces.Debt.IDebtRecord.dmp_interest
            Get
                Return m_DMPInterst
            End Get
            Set(value As Object)
                m_DMPInterst = value
                RaiseDataChanged("dmp_interest")
            End Set
        End Property

        Private m_OrigBalanceAdjustment As Decimal = 0D
        ''' <summary>
        ''' Adjusted original balance
        ''' </summary>
        Public Property orig_balance_adjustment() As Decimal Implements Interfaces.Debt.IDebtRecord.orig_balance_adjustment
            Get
                Return m_OrigBalanceAdjustment
            End Get
            Set(value As Decimal)
                If m_OrigBalanceAdjustment <> value Then
                    m_OrigBalanceAdjustment = value
                    RaiseDataChanged("orig_balance_adjustment")
                End If
            End Set
        End Property

        Private m_TotalPayments As Decimal = 0D
        ''' <summary>
        ''' Total payments
        ''' </summary>
        Public Property total_payments() As Decimal Implements Interfaces.Debt.IDebtRecord.total_payments
            Get
                Return m_TotalPayments
            End Get
            Set(value As Decimal)
                If m_TotalPayments <> value Then
                    m_TotalPayments = value
                    RaiseDataChanged("total_payments")
                End If
            End Set
        End Property

        Private m_DisbursementCurrentBalance As Decimal = 0D
        ''' <summary>
        ''' Current balance. This is only for the load operation. For general use, see "current_balance".
        ''' </summary>
        <DebtPlus.Svc.Debt.DatabaseField("current_balance", DebtPlus.Svc.Debt.DatabaseFieldAttribute.FieldType.TypeDecimal)> _
        Public Property disbursement_current_balance() As Decimal
            Get
                Return m_DisbursementCurrentBalance
            End Get
            Set(value As Decimal)
                If m_DisbursementCurrentBalance <> value Then
                    m_DisbursementCurrentBalance = value
                    RaiseDataChanged("current_balance")
                End If
            End Set
        End Property

        ''' <summary>
        ''' Current balance
        ''' </summary>
        Public ReadOnly Property current_balance() As Decimal Implements DebtPlus.Interfaces.Debt.IProratable.current_balance
            Get
                Return disbursement_current_balance
            End Get
        End Property

        ''' <summary>
        ''' Adjusted original balance. Used in the payout report.
        ''' </summary>
        Public ReadOnly Property adjusted_original_balance() As Decimal Implements IProratable.adjusted_original_balance
            Get
                Return orig_balance + orig_balance_adjustment
            End Get
        End Property

        ''' <summary>
        ''' Balance for the display grid
        ''' </summary>
        Public Overridable ReadOnly Property display_current_balance() As Decimal
            Get
                If Not IsActive Then
                    Return 0D
                End If

                If ccl_zero_balance Then
                    Return 0D
                End If

                If current_balance < 0D Then
                    Return 0D
                End If
                Return current_balance
            End Get
        End Property

        Private m_DebitAmt As Decimal = 0D
        ''' <summary>
        ''' Amount to be disbursed in this disbursement operation
        ''' </summary>
        <DebtPlus.Svc.Debt.DatabaseField("debit_amt", DebtPlus.Svc.Debt.DatabaseFieldAttribute.FieldType.TypeDecimal)> _
        Public Property debit_amt() As Decimal Implements DebtPlus.Interfaces.Debt.IProratable.debit_amt
            Get
                Return m_DebitAmt
            End Get
            Set(value As Decimal)
                value = System.Math.Truncate(value * 100D) / 100D ' Remove any fractional cents

                ' If we are not loading the information then do some range checking on the data
                ' to prevent amounts in excess of what are allowed.
                If Not InInit Then
                    If value < 0D Then
                        value = 0D
                    ElseIf Not ccl_zero_balance AndAlso value > current_balance Then
                        value = current_balance
                    End If
                End If

                m_DebitAmt = value
                RaiseDataChanged("debit_amt")
            End Set
        End Property

        Private m_ZeroBalance As Boolean = False
        ''' <summary>
        ''' Does the debt have a condition where the balance is always zero?
        ''' </summary>
        <DebtPlus.Svc.Debt.DatabaseField("zero_balance", DebtPlus.Svc.Debt.DatabaseFieldAttribute.FieldType.TypeBoolean)> _
        Public Property ccl_zero_balance() As Boolean Implements DebtPlus.Interfaces.Debt.IDebtRecord.ccl_zero_balance
            Get
                Return m_ZeroBalance
            End Get
            Set(value As Boolean)
                m_ZeroBalance = value
                RaiseDataChanged("ccl_zero_balance")
            End Set
        End Property

        Private m_Priority As Int32 = 9
        ''' <summary>
        ''' Relative importance of paying this debt to others
        ''' </summary>
        <DebtPlus.Svc.Debt.DatabaseField("priority", DebtPlus.Svc.Debt.DatabaseFieldAttribute.FieldType.TypeInt32)> _
        Public Property priority() As Int32 Implements DebtPlus.Interfaces.Debt.IDebtRecord.priority, DebtPlus.Interfaces.Debt.IProratable.priority
            Get
                Return m_Priority
            End Get
            Set(value As Int32)
                m_Priority = value
                RaiseDataChanged("Priority")
            End Set
        End Property

        Private m_CreditorName As String = String.Empty
        ''' <summary>
        ''' Name of the creditor
        ''' </summary>
        <DebtPlus.Svc.Debt.DatabaseField("creditor_name", DebtPlus.Svc.Debt.DatabaseFieldAttribute.FieldType.TypeString)> _
        Public Property creditor_name() As String Implements DebtPlus.Interfaces.Debt.IDebtRecord.creditor_name
            Get
                Return m_CreditorName
            End Get
            Set(value As String)
                m_CreditorName = value
                RaiseDataChanged("creditor_name")
            End Set
        End Property

        Private m_AccountNumber As String
        ''' <summary>
        ''' Creditor's account number
        ''' </summary>
        <DebtPlus.Svc.Debt.DatabaseField("account_number", DebtPlus.Svc.Debt.DatabaseFieldAttribute.FieldType.TypeString)> _
        Public Property account_number() As String Implements DebtPlus.Interfaces.Debt.IDebtRecord.account_number
            Get
                Return m_AccountNumber
            End Get
            Set(value As String)
                m_AccountNumber = value
                RaiseDataChanged("account_number")
            End Set
        End Property

        Private m_LineNumber As Int32 = 0
        ''' <summary>
        ''' Relative stacking order for the debts
        ''' </summary>
        <DebtPlus.Svc.Debt.DatabaseField("line_number", DebtPlus.Svc.Debt.DatabaseFieldAttribute.FieldType.TypeInt32)> _
        Public Property line_number() As Int32 Implements DebtPlus.Interfaces.Debt.IDebtRecord.line_number
            Get
                Return m_LineNumber
            End Get
            Set(value As Int32)
                m_LineNumber = value
                RaiseDataChanged("line_number")
            End Set
        End Property

        Private m_Prorate As Boolean = False
        ''' <summary>
        ''' Should we Prorate the debt?
        ''' </summary>
        <DebtPlus.Svc.Debt.DatabaseField("prorate", DebtPlus.Svc.Debt.DatabaseFieldAttribute.FieldType.TypeBoolean)> _
        Public Property ccl_prorate() As Boolean Implements DebtPlus.Interfaces.Debt.IDebtRecord.ccl_prorate
            Get
                Return m_Prorate
            End Get
            Set(value As Boolean)
                m_Prorate = value
                RaiseDataChanged("ccl_prorate")
            End Set
        End Property

        Private m_CreditorType As String = "N"
        ''' <summary>
        ''' Type of the fairshare for this creditor
        ''' </summary>
        <DebtPlus.Svc.Debt.DatabaseField("creditor_type", DebtPlus.Svc.Debt.DatabaseFieldAttribute.FieldType.TypeString)> _
        Public Property creditor_type() As String
            Get
                Return m_CreditorType
            End Get
            Set(value As String)
                m_CreditorType = value
                RaiseDataChanged("creditor_type")
            End Set
        End Property

        Private m_DebtType As DebtPlus.Interfaces.Debt.DebtTypeEnum = DebtPlus.Interfaces.Debt.DebtTypeEnum.Normal
        ''' <summary>
        ''' Type of the debt
        ''' </summary>
        Public Property DebtType() As DebtPlus.Interfaces.Debt.DebtTypeEnum Implements DebtPlus.Interfaces.Debt.IDebtRecord.DebtType, DebtPlus.Interfaces.Debt.IProratable.DebtType
            Get
                Return m_DebtType
            End Get
            Set(value As DebtPlus.Interfaces.Debt.DebtTypeEnum)
                m_DebtType = value
                RaiseDataChanged("DebtType")
            End Set
        End Property

        ''' <summary>
        ''' Routine to set the debttype when loading
        ''' </summary>
        ''' <param name="SpecialCreditorList"></param>
        Public Sub SetDebtType(SpecialCreditorList As DebtPlus.Interfaces.Debt.SpecialCreditors) Implements DebtPlus.Interfaces.Debt.IDebtRecord.SetDebtType

            ' Process the normal record list (Disbursement debts don't have agency account)
            DebtType = DebtPlus.Interfaces.Debt.DebtTypeEnum.Normal

            ' Look at active items only from this point to find the other types
            If IsActive Then
                If String.Compare(Creditor, SpecialCreditorList.MonthlyFeeCreditor, True) = 0 Then
                    DebtType = DebtPlus.Interfaces.Debt.DebtTypeEnum.MonthlyFee
                ElseIf String.Compare(Creditor, SpecialCreditorList.DeductCreditor, True) = 0 Then
                    DebtType = DebtPlus.Interfaces.Debt.DebtTypeEnum.Fairshare
                ElseIf String.Compare(Creditor, SpecialCreditorList.SetupCreditor, True) = 0 Then
                    DebtType = DebtPlus.Interfaces.Debt.DebtTypeEnum.SetupFee
                End If
            End If
        End Sub

        ''' <summary>
        ''' Is this a fee creditor?
        ''' </summary>
        Public ReadOnly Property IsFee() As Boolean Implements DebtPlus.Interfaces.Debt.IProratable.IsFee
            Get
                Dim Answer As Boolean

                ' If the item is a payoff for the debt then it is a fee amount and not proratable.
                ' we want the money to pay off the debt taken off the top first since the disbursement
                ' factor is reduced to the payoff amount.
                If IsPayoff Then
                    Answer = True
                Else
                    Answer = False

                    ' Otherwise, look that the other flags and return false if the value is not a valid debt.
                    If Not hold_disbursements AndAlso IsActive Then
                        If DebtType = DebtPlus.Interfaces.Debt.DebtTypeEnum.FixedAgencyFee OrElse DebtType = DebtPlus.Interfaces.Debt.DebtTypeEnum.MonthlyFee OrElse DebtType = DebtPlus.Interfaces.Debt.DebtTypeEnum.SetupFee Then
                            Answer = True
                        End If
                    End If
                End If

                Return Answer
            End Get
        End Property

        Private m_IsPayoff As Boolean = False
        ''' <summary>
        ''' Is the debt being paid off such that balance is less than or equal to the debit_amt?
        ''' </summary>
        Public Property IsPayoff() As Boolean Implements DebtPlus.Interfaces.Debt.IProratable.IsPayoff
            Get
                Return m_IsPayoff
            End Get
            Set(value As Boolean)
                m_IsPayoff = value
                RaiseDataChanged("IsPayoff")
            End Set
        End Property

        ''' <summary>
        ''' Are we allowed to prorate this debt's amount?
        ''' </summary>
        Public ReadOnly Property IsProratable() As Boolean Implements DebtPlus.Interfaces.Debt.IProratable.IsProratable
            Get
                If Not IsPayoff AndAlso Not hold_disbursements AndAlso IsActive Then
                    Select Case DebtType
                        Case DebtTypeEnum.Normal
                            Return True
                        Case DebtTypeEnum.PaymentCushion
                            Return True
                        Case Else
                            Exit Select
                    End Select
                End If

                Return False
            End Get
        End Property

        Private m_TotalInterest As Decimal = 0D

        ''' <summary>
        ''' Total amount of interest paid on this debt
        ''' </summary>
        Public Property total_interest() As Decimal Implements DebtPlus.Interfaces.Debt.IDebtRecord.total_interest, IProratable.total_interest
            Get
                Return m_TotalInterest
            End Get
            Set(value As Decimal)
                m_TotalInterest = value
                RaiseDataChanged("total_interest")
            End Set
        End Property

        Private m_IsActive As Boolean = True

        ''' <summary>
        ''' Is this debt active or not?
        ''' </summary>
        Public Property IsActive() As Boolean Implements DebtPlus.Interfaces.Debt.IDebtRecord.IsActive, DebtPlus.Interfaces.Debt.IProratable.IsActive
            Get
                Return m_IsActive
            End Get
            Set(value As Boolean)
                m_IsActive = value
                RaiseDataChanged("IsActive")
            End Set
        End Property

        ''' <summary>
        ''' Creditor division name
        ''' </summary>
        Public Property cr_division_name() As String Implements DebtPlus.Interfaces.Debt.IDebtRecord.cr_division_name
            Get
                Return String.Empty
            End Get
            Set(value As String)
            End Set
        End Property

        Private m_apr As Object = System.DBNull.Value
        ''' <summary>
        ''' Creditor interest rate for debts
        ''' </summary>
        <DatabaseField("apr")> _
        Public Property display_intererst_rate() As Object
            Get
                Return m_apr
            End Get
            Set(value As Object)
                m_apr = value
                RaiseDataChanged("apr")
            End Set
        End Property

        Private m_Parent As DebtPlus.Interfaces.Debt.IDebtRecordList = Nothing
        ''' <summary>
        ''' Linkage to the list of debts in which this is an item
        ''' </summary>
        Public Property Parent() As DebtPlus.Interfaces.Debt.IDebtRecordList Implements DebtPlus.Interfaces.Debt.IDebtRecord.Parent
            Get
                Return m_Parent
            End Get
            Set(value As DebtPlus.Interfaces.Debt.IDebtRecordList)
                m_Parent = value
                RaiseDataChanged("Parent")
            End Set
        End Property

        Private m_HoldDisbursements As Boolean = False
        ''' <summary>
        ''' Is the debt marked "hold disbursements"?
        ''' </summary>
        <DebtPlus.Svc.Debt.DatabaseField("held", DebtPlus.Svc.Debt.DatabaseFieldAttribute.FieldType.TypeBoolean)> _
        Public Property hold_disbursements() As Boolean Implements DebtPlus.Interfaces.Debt.IDebtRecord.hold_disbursements, DebtPlus.Interfaces.Disbursement.IDisburseable.IsHeld
            Get
                Return m_HoldDisbursements
            End Get
            Set(value As Boolean)
                m_HoldDisbursements = value
                RaiseDataChanged("hold_disbursements")
            End Set
        End Property

        ''' <summary>
        ''' Compare the items for the sort function
        ''' </summary>
        ''' <param name="CmpObj"></param>
        ''' <returns></returns>
        Public Function CompareTo(CmpObj As Object) As Int32 Implements IComparable.CompareTo
            If Not (TypeOf CmpObj Is DisbursementDebtRecord) Then
                Throw New ArgumentException("Comparison type of DisbursementDebtRecord is expected", "obj")
            End If
            Return CompareTo(DirectCast(CmpObj, DisbursementDebtRecord))
        End Function

        ''' <summary>
        ''' Compare the items for the sort function
        ''' </summary>
        ''' <param name="CmpObj"></param>
        ''' <returns></returns>
        Public Function CompareTo(CmpObj As DisbursementDebtRecord) As Int32 Implements IComparable(Of DebtPlus.UI.Desktop.Disbursement.Common.DisbursementDebtRecord).CompareTo

            Dim Order As Int32 = DebtPlus.Utils.Nulls.DInt(CmpObj.line_number).CompareTo(DebtPlus.Utils.Nulls.DInt(line_number))
            If Order = 0 Then Order = DebtPlus.Utils.Nulls.DDbl(CmpObj.dmp_interest).CompareTo(DebtPlus.Utils.Nulls.DDbl(dmp_interest))
            If Order = 0 Then Order = CmpObj.display_current_balance.CompareTo(display_current_balance)
            If Order = 0 Then Order = DebtPlus.Utils.Nulls.DDec(CmpObj.total_interest).CompareTo(total_interest)

            Return Order
        End Function

#Region "ISupportInitialize"
        ''' <summary>
        ''' Class initialization
        ''' </summary>
        Public Sub BeginInit() Implements ISupportInitialize.BeginInit
            InInit = True
        End Sub

        ''' <summary>
        ''' Class initialization
        ''' </summary>
        Public Sub EndInit() Implements ISupportInitialize.EndInit
            If InInit Then
                ' Default the amount to be paid to the amount scheduled
                If debit_amt = 0D AndAlso sched_payment > 0D Then
                    debit_amt = sched_payment
                End If

                ' Limit the amount to the balance
                If debit_amt > current_balance Then
                    debit_amt = current_balance
                End If

                ' If the debt is on hold then the amount is, by definition, zero.
                If hold_disbursements Then
                    debit_amt = 0D
                End If

                InInit = False
            End If
        End Sub
#End Region

        Public Event DataChanged As DebtPlus.Events.DataChangedEventHandler Implements DebtPlus.Interfaces.Debt.INotifyDataChanged.DataChanged
        Protected Overridable Sub OnDataChanged(e As DebtPlus.Events.DataChangedEventArgs)
            If Not InInit Then
                RaiseEvent DataChanged(Me, e)
            End If
        End Sub

        Protected Sub RaiseDataChanged(DataName As String)
            If Not InInit Then
                OnDataChanged(New DebtPlus.Events.DataChangedEventArgs(DataName))
            End If
        End Sub

#Region "IDisposable Support"

        Private disposedValue As Boolean
        ' To detect redundant calls
        ''' <summary>
        ''' Dispose of local storage
        ''' </summary>
        ''' <param name="disposing"></param>
        Protected Overridable Sub Dispose(disposing As Boolean)
            If Not Me.disposedValue Then
                If disposing Then
                End If
                Me.disposedValue = True
            End If
        End Sub

        Public Sub Dispose() Implements IDisposable.Dispose
            Dispose(True)
            GC.SuppressFinalize(Me)
        End Sub

        Protected Overrides Sub Finalize()
            Try
                Dispose(False)
            Finally
                MyBase.Finalize()
            End Try
        End Sub
#End Region

#Region "Items not used"

        ' These items are not used for the Disbursement Debt record. They are, however, required
        ' to complete the definition of a "debt record". So, just define them but throw a not-
        ' implemented exception should they be refrenced.

        Public Property balance_client_creditor() As Integer Implements DebtPlus.Interfaces.Debt.IDebtRecord.balance_client_creditor
            Get
                Return 0
            End Get
            Set(value As Integer)
            End Set
        End Property

        Public Property balance_verification_release() As Object Implements DebtPlus.Interfaces.Debt.IDebtRecord.balance_verification_release
            Get
                Return System.DBNull.Value
            End Get
            Set(value As Object)
            End Set
        End Property

        Public Property balance_verify_by() As String Implements DebtPlus.Interfaces.Debt.IDebtRecord.balance_verify_by
            Get
                Return String.Empty
            End Get
            Set(value As String)
            End Set
        End Property

        Public Property balance_verify_date() As Object Implements DebtPlus.Interfaces.Debt.IDebtRecord.balance_verify_date
            Get
                Return System.DBNull.Value
            End Get
            Set(value As Object)
            End Set
        End Property

        Public Property ccl_agency_account() As Boolean Implements DebtPlus.Interfaces.Debt.IDebtRecord.ccl_agency_account
            Get
                Return False
            End Get
            Set(value As Boolean)
            End Set
        End Property

        Public Property ccl_always_disburse() As Boolean Implements DebtPlus.Interfaces.Debt.IDebtRecord.ccl_always_disburse
            Get
                Return False
            End Get
            Set(value As Boolean)
            End Set
        End Property

        Public Property check_payments() As Integer Implements DebtPlus.Interfaces.Debt.IDebtRecord.check_payments
            Get
                Return 0
            End Get
            Set(value As Integer)
            End Set
        End Property

        Public Property client_creditor_balance() As Integer Implements DebtPlus.Interfaces.Debt.IDebtRecord.client_creditor_balance
            Get
                Return 0
            End Get
            Set(value As Integer)
            End Set
        End Property

        Public Property client_creditor_proposal() As Integer Implements DebtPlus.Interfaces.Debt.IDebtRecord.client_creditor_proposal
            Get
                Return 0
            End Get
            Set(value As Integer)
            End Set
        End Property

        Public Property client_name() As String Implements DebtPlus.Interfaces.Debt.IDebtRecord.client_name
            Get
                Return String.Empty
            End Get
            Set(value As String)
            End Set
        End Property

        Public Property contact_name() As String Implements DebtPlus.Interfaces.Debt.IDebtRecord.contact_name
            Get
                Return String.Empty
            End Get
            Set(value As String)
            End Set
        End Property

        Public Property cr_creditor_name() As String Implements DebtPlus.Interfaces.Debt.IDebtRecord.cr_creditor_name
            Get
                Return String.Empty
            End Get
            Set(value As String)
            End Set
        End Property

        Public Property cr_min_accept_amt() As Decimal Implements DebtPlus.Interfaces.Debt.IDebtRecord.cr_min_accept_amt
            Get
                Return 0D
            End Get
            Set(value As Decimal)
            End Set
        End Property

        Public Property cr_min_accept_pct() As Double Implements DebtPlus.Interfaces.Debt.IDebtRecord.cr_min_accept_pct
            Get
                Return 0.0
            End Get
            Set(value As Double)
            End Set
        End Property

        Public Property cr_payment_balance() As String Implements DebtPlus.Interfaces.Debt.IDebtRecord.cr_payment_balance
            Get
                Return String.Empty
            End Get
            Set(value As String)
            End Set
        End Property

        Public Property created_by() As String Implements DebtPlus.Interfaces.Debt.IDebtRecord.created_by
            Get
                Return String.Empty
            End Get
            Set(value As String)
            End Set
        End Property

        Public Property current_sched_payment() As Decimal Implements DebtPlus.Interfaces.Debt.IDebtRecord.current_sched_payment
            Get
                Return 0D
            End Get
            Set(value As Decimal)
            End Set
        End Property

        Public Property date_created() As System.DateTime Implements DebtPlus.Interfaces.Debt.IDebtRecord.date_created
            Get
                Return DateTime.MinValue
            End Get
            Set(value As System.DateTime)
            End Set
        End Property

        Public Property date_disp_changed() As Object Implements DebtPlus.Interfaces.Debt.IDebtRecord.date_disp_changed
            Get
                Return System.DBNull.Value
            End Get
            Set(value As Object)
            End Set
        End Property

        Public Property date_proposal_prenoted() As Object Implements DebtPlus.Interfaces.Debt.IDebtRecord.date_proposal_prenoted
            Get
                Return System.DBNull.Value
            End Get
            Set(value As Object)
            End Set
        End Property

        Public Property dmp_payout_interest() As Object Implements DebtPlus.Interfaces.Debt.IDebtRecord.dmp_payout_interest
            Get
                Return System.DBNull.Value
            End Get
            Set(value As Object)
            End Set
        End Property

        Public Property drop_date() As Object Implements DebtPlus.Interfaces.Debt.IDebtRecord.drop_date
            Get
                Return System.DBNull.Value
            End Get
            Set(value As Object)
            End Set
        End Property

        Public Property drop_reason() As Object Implements DebtPlus.Interfaces.Debt.IDebtRecord.drop_reason
            Get
                Return System.DBNull.Value
            End Get
            Set(value As Object)
            End Set
        End Property

        Public Property drop_reason_date() As Object Implements DebtPlus.Interfaces.Debt.IDebtRecord.drop_reason_date
            Get
                Return System.DBNull.Value
            End Get
            Set(value As Object)
            End Set
        End Property

        Public Property drop_reason_sent() As Object Implements DebtPlus.Interfaces.Debt.IDebtRecord.drop_reason_sent
            Get
                Return System.DBNull.Value
            End Get
            Set(value As Object)
            End Set
        End Property

        Public Property expected_payout_date() As Object Implements DebtPlus.Interfaces.Debt.IDebtRecord.expected_payout_date
            Get
                Return System.DBNull.Value
            End Get
            Set(value As Object)
            End Set
        End Property

        Public Property fairshare_pct_check() As Object Implements DebtPlus.Interfaces.Debt.IDebtRecord.fairshare_pct_check
            Get
                Return 0.0
            End Get
            Set(value As Object)
            End Set
        End Property

        Public Property fairshare_pct_eft() As Object Implements DebtPlus.Interfaces.Debt.IDebtRecord.fairshare_pct_eft
            Get
                Return 0.0
            End Get
            Set(value As Object)
            End Set
        End Property

        Public Property first_payment() As Integer Implements DebtPlus.Interfaces.Debt.IDebtRecord.first_payment
            Get
                Return 0
            End Get
            Set(value As Integer)
            End Set
        End Property

        Public Property first_payment_amt() As Decimal Implements DebtPlus.Interfaces.Debt.IDebtRecord.first_payment_amt
            Get
                Return 0D
            End Get
            Set(value As Decimal)
            End Set
        End Property

        Public Property first_payment_date() As Object Implements DebtPlus.Interfaces.Debt.IDebtRecord.first_payment_date
            Get
                Return System.DBNull.Value
            End Get
            Set(value As Object)
            End Set
        End Property

        Public Property first_payment_type() As String Implements DebtPlus.Interfaces.Debt.IDebtRecord.first_payment_type
            Get
                Return String.Empty
            End Get
            Set(value As String)
            End Set
        End Property

        Public Property interest_this_creditor() As Decimal Implements DebtPlus.Interfaces.Debt.IDebtRecord.interest_this_creditor
            Get
                Return 0D
            End Get
            Set(value As Decimal)
            End Set
        End Property

        Public Property irs_form_on_file() As Boolean Implements DebtPlus.Interfaces.Debt.IDebtRecord.irs_form_on_file
            Get
                Return False
            End Get
            Set(value As Boolean)
            End Set
        End Property

        Public Property last_communication() As String Implements DebtPlus.Interfaces.Debt.IDebtRecord.last_communication
            Get
                Return String.Empty
            End Get
            Set(value As String)
            End Set
        End Property

        Public Property last_payment() As Integer Implements DebtPlus.Interfaces.Debt.IDebtRecord.last_payment
            Get
                Return 0
            End Get
            Set(value As Integer)
            End Set
        End Property

        Public Property last_payment_amt() As Decimal Implements DebtPlus.Interfaces.Debt.IDebtRecord.last_payment_amt
            Get
                Return 0D
            End Get
            Set(value As Decimal)
            End Set
        End Property

        Public Property last_payment_date() As Object Implements DebtPlus.Interfaces.Debt.IDebtRecord.last_payment_date
            Get
                Return System.DBNull.Value
            End Get
            Set(value As Object)
            End Set
        End Property

        Public Property last_payment_date_b4_dmp() As Object Implements DebtPlus.Interfaces.Debt.IDebtRecord.last_payment_date_b4_dmp
            Get
                Return System.DBNull.Value
            End Get
            Set(value As Object)
            End Set
        End Property

        Public Property last_payment_type() As String Implements DebtPlus.Interfaces.Debt.IDebtRecord.last_payment_type
            Get
                Return String.Empty
            End Get
            Set(value As String)
            End Set
        End Property

        Public Property last_stmt_balance() As Decimal Implements DebtPlus.Interfaces.Debt.IDebtRecord.last_stmt_balance
            Get
                Return 0D
            End Get
            Set(value As Decimal)
            End Set
        End Property

        Public Property last_stmt_date() As Object Implements DebtPlus.Interfaces.Debt.IDebtRecord.last_stmt_date
            Get
                Return System.DBNull.Value
            End Get
            Set(value As Object)
            End Set
        End Property

        Public Function MaximumPayment() As Decimal Implements DebtPlus.Interfaces.Debt.IDebtRecord.MaximumPayment
            Return MaximumPayment(current_balance)
        End Function

        Public Function MaximumPayment(CurrentBalance As Decimal) As Decimal Implements DebtPlus.Interfaces.Debt.IDebtRecord.MaximumPayment
            Return 0D
        End Function

        Public Function MinimumPayment() As Decimal Implements DebtPlus.Interfaces.Debt.IDebtRecord.MinimumPayment
            Return MinimumPayment(current_balance)
        End Function

        Public Function MinimumPayment(CurrentBalance As Decimal) As Decimal Implements DebtPlus.Interfaces.Debt.IDebtRecord.MinimumPayment
            Return 0D
        End Function

        Public Property Message() As Object Implements DebtPlus.Interfaces.Debt.IDebtRecord.Message
            Get
                Return System.DBNull.Value
            End Get
            Set(value As Object)
            End Set
        End Property

        Public Property months_delinquent() As Integer Implements DebtPlus.Interfaces.Debt.IDebtRecord.months_delinquent
            Get
                Return 0
            End Get
            Set(value As Integer)
            End Set
        End Property

        Public Property non_dmp_interest() As Double Implements DebtPlus.Interfaces.Debt.IDebtRecord.non_dmp_interest
            Get
                Return 0.0
            End Get
            Set(value As Double)
            End Set
        End Property

        Public Property non_dmp_payment() As Decimal Implements DebtPlus.Interfaces.Debt.IDebtRecord.non_dmp_payment
            Get
                Return 0D
            End Get
            Set(value As Decimal)
            End Set
        End Property

        Public Property payment_rpps_mask() As Object Implements DebtPlus.Interfaces.Debt.IDebtRecord.payment_rpps_mask
            Get
                Return System.DBNull.Value
            End Get
            Set(value As Object)
            End Set
        End Property

        Public Property payments_month_1() As Decimal Implements DebtPlus.Interfaces.Debt.IDebtRecord.payments_month_1
            Get
                Return 0D
            End Get
            Set(value As Decimal)
            End Set
        End Property

        Public Property payments_this_creditor() As Decimal Implements DebtPlus.Interfaces.Debt.IDebtRecord.payments_this_creditor
            Get
                Return 0D
            End Get
            Set(value As Decimal)
            End Set
        End Property

        Public Property percent_balance() As Double Implements DebtPlus.Interfaces.Debt.IDebtRecord.percent_balance
            Get
                Return 0.0
            End Get
            Set(value As Double)
            End Set
        End Property

        Public Property Person() As Integer Implements DebtPlus.Interfaces.Debt.IDebtRecord.person
            Get
                Return 0
            End Get
            Set(value As Integer)
            End Set
        End Property

        Public Property prenote_date() As Object Implements DebtPlus.Interfaces.Debt.IDebtRecord.prenote_date
            Get
                Return System.DBNull.Value
            End Get
            Set(value As Object)
            End Set
        End Property

        Public Property proposal_balance() As Decimal Implements DebtPlus.Interfaces.Debt.IDebtRecord.proposal_balance
            Get
                Return 0D
            End Get
            Set(value As Decimal)
            End Set
        End Property

        Public Property proposal_status() As Integer Implements DebtPlus.Interfaces.Debt.IDebtRecord.proposal_status
            Get
                Return 0
            End Get
            Set(value As Integer)
            End Set
        End Property

        Public Property returns_this_creditor() As Decimal Implements DebtPlus.Interfaces.Debt.IDebtRecord.returns_this_creditor
            Get
                Return 0D
            End Get
            Set(value As Decimal)
            End Set
        End Property

        Public Property rpps_client_type_indicator() As Object Implements DebtPlus.Interfaces.Debt.IDebtRecord.rpps_client_type_indicator
            Get
                Return System.DBNull.Value
            End Get
            Set(value As Object)
            End Set
        End Property

        Public Property rpps_mask() As Object Implements DebtPlus.Interfaces.Debt.IDebtRecord.rpps_mask
            Get
                Return System.DBNull.Value
            End Get
            Set(value As Object)
            End Set
        End Property

        Public Property send_bal_verify() As Integer Implements DebtPlus.Interfaces.Debt.IDebtRecord.send_bal_verify
            Get
                Return 0
            End Get
            Set(value As Integer)
            End Set
        End Property

        Public Property send_drop_notice() As Boolean Implements DebtPlus.Interfaces.Debt.IDebtRecord.send_drop_notice
            Get
                Return False
            End Get
            Set(value As Boolean)
            End Set
        End Property

        Public Property start_date() As Object Implements DebtPlus.Interfaces.Debt.IDebtRecord.start_date
            Get
                Return System.DBNull.Value
            End Get
            Set(value As Object)
            End Set
        End Property

        Public Property student_loan_release() As Boolean Implements DebtPlus.Interfaces.Debt.IDebtRecord.student_loan_release
            Get
                Return False
            End Get
            Set(value As Boolean)
            End Set
        End Property

        Public Property terms() As Integer Implements DebtPlus.Interfaces.Debt.IDebtRecord.terms
            Get
                Return 60
            End Get
            Set(value As Integer)
            End Set
        End Property

        Public Property total_sched_payment() As Decimal Implements DebtPlus.Interfaces.Debt.IDebtRecord.total_sched_payment
            Get
                Return 0D
            End Get
            Set(value As Decimal)
            End Set
        End Property

        Public Property verify_request_date() As Object Implements DebtPlus.Interfaces.Debt.IDebtRecord.verify_request_date
            Get
                Return System.DBNull.Value
            End Get
            Set(value As Object)
            End Set
        End Property

        Public Property creditor_interest As Object Implements Interfaces.Debt.IDebtRecord.creditor_interest
            Get
                Return System.DBNull.Value
            End Get
            Set(value As Object)
            End Set
        End Property
#End Region
    End Class

    Partial Class DisbursementDebtList
        Inherits DebtPlus.Svc.Debt.DebtRecordList

        ''' <summary>
        ''' Create a new instance of our class
        ''' </summary>
        Public Sub New()
            MyBase.New(GetType(DebtPlus.UI.Desktop.Disbursement.Common.DisbursementDebtRecord))
        End Sub

        Public Sub New(ItemTypes As Type)
            MyBase.New(ItemTypes)
        End Sub

        ''' <summary>
        ''' Answer questions from the routine to calculate the monthly fee
        ''' </summary>
        Public Overrides Function QueryFeeValue(Item As String) As Object
            Dim Answer As Object = Nothing

            Select Case Item
                Case "debit_amt"
                    Dim fee_creditor As DebtPlus.Interfaces.Debt.IProratable = MonthlyFeeDebt()
                    If fee_creditor IsNot Nothing Then
                        Answer = fee_creditor.debit_amt
                    End If
                    Exit Select

                    ' Enable all of the limit values
                Case DebtPlus.Events.ParameterValueEventArgs.name_LimitDebtBalance
                    Answer = True
                    Exit Select

                Case DebtPlus.Events.ParameterValueEventArgs.name_LimitMonthlyMax
                    Answer = True
                    Exit Select

                Case DebtPlus.Events.ParameterValueEventArgs.name_LimitDisbMax
                    Answer = True
                    Exit Select

                Case Else
                    Answer = MyBase.QueryFeeValue(Item)
                    Exit Select
            End Select

            Return Answer
        End Function

        ''' <summary>
        ''' Calculate the number of creditors for the disbursement
        ''' </summary>
        ''' <returns>The number of creditors that are not held, are not a fee account, and have a disbursement amount and balance</returns>
        Public Overrides Function TotalCreditors() As Int32
            Dim answer As Int32 = 0
            For Each record As IProratable In Me
                With record
                    If Not .IsHeld AndAlso Not .IsFee AndAlso .debit_amt > 0D AndAlso .current_balance > 0D Then
                        answer += 1
                    End If
                End With
            Next
            Return answer
        End Function

        ''' <summary>
        ''' Read the list of client debts for the disbursement operation
        ''' </summary>
        Protected disbursement_register As Int32
        Public Overloads Sub ReadDebtsTable(Client As Int32, disbursement_register As Int32)
            Me.disbursement_register = disbursement_register
            MyBase.ReadDebtsTable(Client)
        End Sub

        ''' <summary>
        ''' Select command to read the debt information
        ''' </summary>
        Protected Overrides Function view_debt_info_Select(ClientId As Int32) As SqlCommand
            Dim cmd As SqlCommand = New SqlClient.SqlCommand()
            cmd.CommandText = "xpr_disbursement_creditor_info"
            cmd.CommandType = CommandType.StoredProcedure
            cmd.Parameters.Add("@disbursement_register", SqlDbType.Int).Value = disbursement_register
            cmd.Parameters.Add("@client", SqlDbType.Int).Value = ClientId

            Return cmd
        End Function

        ''' <summary>
        ''' These functions are not implemented in the disbursement debt list
        ''' </summary>
        Public Overrides Sub RefreshData()
            Throw New NotImplementedException()
        End Sub
    End Class
End Namespace
