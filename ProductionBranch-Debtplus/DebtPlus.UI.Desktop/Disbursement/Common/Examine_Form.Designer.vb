﻿Namespace Disbursement.Common
    <Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
    Partial Class Examine_Form
        Inherits DebtPlus.Data.Forms.DebtPlusForm

        'Form overrides dispose to clean up the component list.
        <System.Diagnostics.DebuggerNonUserCode()> _
        Protected Overrides Sub Dispose(ByVal disposing As Boolean)
            Try
                If disposing AndAlso components IsNot Nothing Then
                    components.Dispose()
                    UnRegisterHandlers()
                End If
            Finally
                MyBase.Dispose(disposing)
            End Try
        End Sub

        'Required by the Windows Form Designer
        Private components As System.ComponentModel.IContainer

        'NOTE: The following procedure is required by the Windows Form Designer
        'It can be modified using the Windows Form Designer.  
        'Do not modify it using the code editor.
        'NOTE: The following procedure is required by the Windows Form Designer
        'It can be modified using the Windows Form Designer.  
        'Do not modify it using the code editor.
        <System.Diagnostics.DebuggerStepThrough()>
        Private Sub InitializeComponent()
            Dim StyleFormatCondition1 As DevExpress.XtraGrid.StyleFormatCondition = New DevExpress.XtraGrid.StyleFormatCondition
            Me.col_held = New DevExpress.XtraGrid.Columns.GridColumn
            Me.col_held.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.True
            Me.ButtonQuit = New DevExpress.XtraEditors.SimpleButton
            Me.ButtonClear = New DevExpress.XtraEditors.SimpleButton
            Me.ButtonProrate = New DevExpress.XtraEditors.SimpleButton
            Me.ButtonPay = New DevExpress.XtraEditors.SimpleButton
            Me.ButtonDontPay = New DevExpress.XtraEditors.SimpleButton
            Me.ButtonShowClient = New DevExpress.XtraEditors.SimpleButton
            Me.ButtonShowNotes = New DevExpress.XtraEditors.SimpleButton
            Me.ButtonPrevious = New DevExpress.XtraEditors.SimpleButton
            Me.GroupControl1 = New DevExpress.XtraEditors.GroupControl
            Me.LabelControl6 = New DevExpress.XtraEditors.LabelControl
            Me.lbl_language = New DevExpress.XtraEditors.LabelControl
            Me.lbl_work_ph = New DevExpress.XtraEditors.LabelControl
            Me.lbl_home_ph = New DevExpress.XtraEditors.LabelControl
            Me.lbl_name = New DevExpress.XtraEditors.LabelControl
            Me.lbl_client = New DevExpress.XtraEditors.LabelControl
            Me.lbl_address = New DevExpress.XtraEditors.LabelControl
            Me.LabelControl5 = New DevExpress.XtraEditors.LabelControl
            Me.LabelControl4 = New DevExpress.XtraEditors.LabelControl
            Me.LabelControl3 = New DevExpress.XtraEditors.LabelControl
            Me.LabelControl2 = New DevExpress.XtraEditors.LabelControl
            Me.LabelControl1 = New DevExpress.XtraEditors.LabelControl
            Me.GridControl1 = New DevExpress.XtraGrid.GridControl
            Me.GridView1 = New DevExpress.XtraGrid.Views.Grid.GridView
            Me.col_creditor = New DevExpress.XtraGrid.Columns.GridColumn
            Me.col_creditor.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.True
            Me.col_name = New DevExpress.XtraGrid.Columns.GridColumn
            Me.col_name.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.True
            Me.col_debit_amt = New DevExpress.XtraGrid.Columns.GridColumn
            Me.col_debit_amt.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.True
            Me.CalcEdit_col_debit_amt = New DevExpress.XtraEditors.Repository.RepositoryItemCalcEdit
            Me.col_disbursement_factor = New DevExpress.XtraGrid.Columns.GridColumn
            Me.col_disbursement_factor.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.True
            Me.col_priority = New DevExpress.XtraGrid.Columns.GridColumn
            Me.col_priority.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.True
            Me.col_balance = New DevExpress.XtraGrid.Columns.GridColumn
            Me.col_balance.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.True
            Me.col_acct_number = New DevExpress.XtraGrid.Columns.GridColumn
            Me.col_acct_number.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.True
            Me.col_interest = New DevExpress.XtraGrid.Columns.GridColumn
            Me.col_interest.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.True
            Me.col_IncludeInFees = New DevExpress.XtraGrid.Columns.GridColumn
            Me.col_IncludeInFees.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.True
            Me.col_IncludeInProrate = New DevExpress.XtraGrid.Columns.GridColumn
            Me.col_IncludeInProrate.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.True
            Me.col_Prorate = New DevExpress.XtraGrid.Columns.GridColumn
            Me.col_Prorate.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.True
            Me.col_ZeroBalance = New DevExpress.XtraGrid.Columns.GridColumn
            Me.col_ZeroBalance.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.True
            Me.col_orig_balance = New DevExpress.XtraGrid.Columns.GridColumn
            Me.col_orig_balance.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.True
            Me.col_total_interest = New DevExpress.XtraGrid.Columns.GridColumn
            Me.col_total_interest.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.True
            Me.col_debt_id = New DevExpress.XtraGrid.Columns.GridColumn
            Me.col_debt_id.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.True
            Me.col_sched_payment = New DevExpress.XtraGrid.Columns.GridColumn
            Me.col_sched_payment.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.True
            Me.col_payments_month_0 = New DevExpress.XtraGrid.Columns.GridColumn
            Me.col_payments_month_0.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.True
            Me.col_line_number = New DevExpress.XtraGrid.Columns.GridColumn
            Me.col_line_number.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.True
            Me.col_creditor_type = New DevExpress.XtraGrid.Columns.GridColumn
            Me.col_creditor_type.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.True
            Me.col_orig_dmp_payment = New DevExpress.XtraGrid.Columns.GridColumn
            Me.col_orig_dmp_payment.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.True
            Me.DepositsControl1 = New DebtPlus.UI.Desktop.Disbursement.Common.DepositsControl
            Me.GroupControl2 = New DevExpress.XtraEditors.GroupControl
            Me.Difference = New DevExpress.XtraEditors.LabelControl
            Me.SumOfDisbursements = New DevExpress.XtraEditors.LabelControl
            Me.TrustFundsAvailable = New DevExpress.XtraEditors.LabelControl
            Me.TrustFundsOnHold = New DevExpress.XtraEditors.LabelControl
            Me.LabelControl10 = New DevExpress.XtraEditors.LabelControl
            Me.LabelControl13 = New DevExpress.XtraEditors.LabelControl
            Me.LabelControl14 = New DevExpress.XtraEditors.LabelControl
            Me.LabelControl15 = New DevExpress.XtraEditors.LabelControl
            Me.LabelControl16 = New DevExpress.XtraEditors.LabelControl
            CType(Me.DxErrorProvider1, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.GroupControl1, System.ComponentModel.ISupportInitialize).BeginInit()
            Me.GroupControl1.SuspendLayout()
            CType(Me.GridControl1, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.GridView1, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.CalcEdit_col_debit_amt, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.GroupControl2, System.ComponentModel.ISupportInitialize).BeginInit()
            Me.GroupControl2.SuspendLayout()
            Me.SuspendLayout()
            '
            'col_held
            '
            Me.col_held.Caption = "Hold"
            Me.col_held.CustomizationCaption = "Debt Held"
            Me.col_held.FieldName = "hold_disbursements"
            Me.col_held.Name = "col_held"
            Me.col_held.OptionsColumn.AllowEdit = False
            Me.col_held.OptionsColumn.ShowInCustomizationForm = False
            Me.col_held.ToolTip = "Debt on HOLD status?"
            Me.col_held.UnboundType = DevExpress.Data.UnboundColumnType.[Boolean]
            Me.col_held.Width = 33
            '
            'ButtonQuit
            '
            Me.ButtonQuit.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
            Me.ButtonQuit.Location = New System.Drawing.Point(578, 8)
            Me.ButtonQuit.Name = "ButtonQuit"
            Me.ButtonQuit.Size = New System.Drawing.Size(75, 23)
            Me.ButtonQuit.TabIndex = 0
            Me.ButtonQuit.Text = "Quit"
            '
            'ButtonClear
            '
            Me.ButtonClear.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
            Me.ButtonClear.Location = New System.Drawing.Point(578, 40)
            Me.ButtonClear.Name = "ButtonClear"
            Me.ButtonClear.Size = New System.Drawing.Size(75, 23)
            Me.ButtonClear.TabIndex = 1
            Me.ButtonClear.Text = "Clear"
            '
            'ButtonProrate
            '
            Me.ButtonProrate.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
            Me.ButtonProrate.Location = New System.Drawing.Point(578, 72)
            Me.ButtonProrate.Name = "ButtonProrate"
            Me.ButtonProrate.Size = New System.Drawing.Size(75, 23)
            Me.ButtonProrate.TabIndex = 2
            Me.ButtonProrate.Text = "Prorate"
            '
            'ButtonPay
            '
            Me.ButtonPay.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
            Me.ButtonPay.Location = New System.Drawing.Point(578, 104)
            Me.ButtonPay.Name = "ButtonPay"
            Me.ButtonPay.Size = New System.Drawing.Size(75, 23)
            Me.ButtonPay.TabIndex = 3
            Me.ButtonPay.Text = "Pay"
            '
            'ButtonDontPay
            '
            Me.ButtonDontPay.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
            Me.ButtonDontPay.Location = New System.Drawing.Point(578, 136)
            Me.ButtonDontPay.Name = "ButtonDontPay"
            Me.ButtonDontPay.Size = New System.Drawing.Size(75, 23)
            Me.ButtonDontPay.TabIndex = 4
            Me.ButtonDontPay.Text = "Don't Pay"
            '
            'ButtonShowClient
            '
            Me.ButtonShowClient.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
            Me.ButtonShowClient.Location = New System.Drawing.Point(578, 168)
            Me.ButtonShowClient.Name = "ButtonShowClient"
            Me.ButtonShowClient.Size = New System.Drawing.Size(75, 23)
            Me.ButtonShowClient.TabIndex = 5
            Me.ButtonShowClient.Text = "Show Client"
            '
            'ButtonShowNotes
            '
            Me.ButtonShowNotes.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
            Me.ButtonShowNotes.Location = New System.Drawing.Point(578, 200)
            Me.ButtonShowNotes.Name = "ButtonShowNotes"
            Me.ButtonShowNotes.Size = New System.Drawing.Size(75, 23)
            Me.ButtonShowNotes.TabIndex = 6
            Me.ButtonShowNotes.Text = "Show Notes"
            '
            'ButtonPrevious
            '
            Me.ButtonPrevious.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
            Me.ButtonPrevious.Location = New System.Drawing.Point(578, 232)
            Me.ButtonPrevious.Name = "ButtonPrevious"
            Me.ButtonPrevious.Size = New System.Drawing.Size(75, 23)
            Me.ButtonPrevious.TabIndex = 7
            Me.ButtonPrevious.Text = "Previous"
            '
            'GroupControl1
            '
            Me.GroupControl1.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                                             Or System.Windows.Forms.AnchorStyles.Right), 
                                            System.Windows.Forms.AnchorStyles)
            Me.GroupControl1.Controls.Add(Me.LabelControl6)
            Me.GroupControl1.Controls.Add(Me.lbl_language)
            Me.GroupControl1.Controls.Add(Me.lbl_work_ph)
            Me.GroupControl1.Controls.Add(Me.lbl_home_ph)
            Me.GroupControl1.Controls.Add(Me.lbl_name)
            Me.GroupControl1.Controls.Add(Me.lbl_client)
            Me.GroupControl1.Controls.Add(Me.lbl_address)
            Me.GroupControl1.Controls.Add(Me.LabelControl5)
            Me.GroupControl1.Controls.Add(Me.LabelControl4)
            Me.GroupControl1.Controls.Add(Me.LabelControl3)
            Me.GroupControl1.Controls.Add(Me.LabelControl2)
            Me.GroupControl1.Controls.Add(Me.LabelControl1)
            Me.GroupControl1.Location = New System.Drawing.Point(8, 8)
            Me.GroupControl1.Name = "GroupControl1"
            Me.GroupControl1.Size = New System.Drawing.Size(562, 144)
            Me.GroupControl1.TabIndex = 8
            Me.GroupControl1.Text = "Client Information"
            '
            'LabelControl6
            '
            Me.LabelControl6.Appearance.Options.UseTextOptions = True
            Me.LabelControl6.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
            Me.LabelControl6.Appearance.TextOptions.WordWrap = DevExpress.Utils.WordWrap.NoWrap
            Me.LabelControl6.Location = New System.Drawing.Point(217, 24)
            Me.LabelControl6.Name = "LabelControl6"
            Me.LabelControl6.Size = New System.Drawing.Size(47, 13)
            Me.LabelControl6.TabIndex = 11
            Me.LabelControl6.Text = "Language"
            '
            'lbl_language
            '
            Me.lbl_language.Location = New System.Drawing.Point(270, 24)
            Me.lbl_language.Name = "lbl_language"
            Me.lbl_language.Size = New System.Drawing.Size(28, 13)
            Me.lbl_language.TabIndex = 10
            Me.lbl_language.Text = "NONE"
            '
            'lbl_work_ph
            '
            Me.lbl_work_ph.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                                           Or System.Windows.Forms.AnchorStyles.Right), 
                                          System.Windows.Forms.AnchorStyles)
            Me.lbl_work_ph.Location = New System.Drawing.Point(128, 72)
            Me.lbl_work_ph.Name = "lbl_work_ph"
            Me.lbl_work_ph.Size = New System.Drawing.Size(28, 13)
            Me.lbl_work_ph.TabIndex = 9
            Me.lbl_work_ph.Text = "NONE"
            '
            'lbl_home_ph
            '
            Me.lbl_home_ph.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                                           Or System.Windows.Forms.AnchorStyles.Right), 
                                          System.Windows.Forms.AnchorStyles)
            Me.lbl_home_ph.Location = New System.Drawing.Point(128, 56)
            Me.lbl_home_ph.Name = "lbl_home_ph"
            Me.lbl_home_ph.Size = New System.Drawing.Size(28, 13)
            Me.lbl_home_ph.TabIndex = 8
            Me.lbl_home_ph.Text = "NONE"
            '
            'lbl_name
            '
            Me.lbl_name.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                                        Or System.Windows.Forms.AnchorStyles.Right), 
                                       System.Windows.Forms.AnchorStyles)
            Me.lbl_name.Location = New System.Drawing.Point(128, 40)
            Me.lbl_name.Name = "lbl_name"
            Me.lbl_name.Size = New System.Drawing.Size(28, 13)
            Me.lbl_name.TabIndex = 7
            Me.lbl_name.Text = "NONE"
            '
            'lbl_client
            '
            Me.lbl_client.Location = New System.Drawing.Point(128, 24)
            Me.lbl_client.Name = "lbl_client"
            Me.lbl_client.Size = New System.Drawing.Size(28, 13)
            Me.lbl_client.TabIndex = 6
            Me.lbl_client.Text = "NONE"
            '
            'lbl_address
            '
            Me.lbl_address.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                                            Or System.Windows.Forms.AnchorStyles.Left) _
                                           Or System.Windows.Forms.AnchorStyles.Right), 
                                          System.Windows.Forms.AnchorStyles)
            Me.lbl_address.Location = New System.Drawing.Point(128, 88)
            Me.lbl_address.Name = "lbl_address"
            Me.lbl_address.Size = New System.Drawing.Size(28, 13)
            Me.lbl_address.TabIndex = 5
            Me.lbl_address.Text = "NONE"
            '
            'LabelControl5
            '
            Me.LabelControl5.Appearance.Options.UseTextOptions = True
            Me.LabelControl5.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
            Me.LabelControl5.Appearance.TextOptions.WordWrap = DevExpress.Utils.WordWrap.NoWrap
            Me.LabelControl5.Location = New System.Drawing.Point(16, 88)
            Me.LabelControl5.Name = "LabelControl5"
            Me.LabelControl5.Size = New System.Drawing.Size(39, 13)
            Me.LabelControl5.TabIndex = 4
            Me.LabelControl5.Text = "Address"
            '
            'LabelControl4
            '
            Me.LabelControl4.Appearance.Options.UseTextOptions = True
            Me.LabelControl4.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
            Me.LabelControl4.Appearance.TextOptions.WordWrap = DevExpress.Utils.WordWrap.NoWrap
            Me.LabelControl4.Location = New System.Drawing.Point(16, 72)
            Me.LabelControl4.Name = "LabelControl4"
            Me.LabelControl4.Size = New System.Drawing.Size(78, 13)
            Me.LabelControl4.TabIndex = 3
            Me.LabelControl4.Text = "Work Telephone"
            '
            'LabelControl3
            '
            Me.LabelControl3.Appearance.Options.UseTextOptions = True
            Me.LabelControl3.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
            Me.LabelControl3.Appearance.TextOptions.WordWrap = DevExpress.Utils.WordWrap.NoWrap
            Me.LabelControl3.Location = New System.Drawing.Point(16, 56)
            Me.LabelControl3.Name = "LabelControl3"
            Me.LabelControl3.Size = New System.Drawing.Size(80, 13)
            Me.LabelControl3.TabIndex = 2
            Me.LabelControl3.Text = "Home Telephone"
            '
            'LabelControl2
            '
            Me.LabelControl2.Appearance.Options.UseTextOptions = True
            Me.LabelControl2.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
            Me.LabelControl2.Appearance.TextOptions.WordWrap = DevExpress.Utils.WordWrap.NoWrap
            Me.LabelControl2.Location = New System.Drawing.Point(16, 40)
            Me.LabelControl2.Name = "LabelControl2"
            Me.LabelControl2.Size = New System.Drawing.Size(27, 13)
            Me.LabelControl2.TabIndex = 1
            Me.LabelControl2.Text = "Name"
            '
            'LabelControl1
            '
            Me.LabelControl1.Appearance.Options.UseTextOptions = True
            Me.LabelControl1.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
            Me.LabelControl1.Appearance.TextOptions.WordWrap = DevExpress.Utils.WordWrap.NoWrap
            Me.LabelControl1.Location = New System.Drawing.Point(16, 24)
            Me.LabelControl1.Name = "LabelControl1"
            Me.LabelControl1.Size = New System.Drawing.Size(41, 13)
            Me.LabelControl1.TabIndex = 0
            Me.LabelControl1.Text = "Client ID"
            '
            'GridControl1
            '
            Me.GridControl1.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                                             Or System.Windows.Forms.AnchorStyles.Left) _
                                            Or System.Windows.Forms.AnchorStyles.Right), 
                                           System.Windows.Forms.AnchorStyles)
            Me.GridControl1.Location = New System.Drawing.Point(8, 265)
            Me.GridControl1.MainView = Me.GridView1
            Me.GridControl1.Name = "GridControl1"
            Me.GridControl1.RepositoryItems.AddRange(New DevExpress.XtraEditors.Repository.RepositoryItem() {Me.CalcEdit_col_debit_amt})
            Me.GridControl1.Size = New System.Drawing.Size(645, 172)
            Me.GridControl1.TabIndex = 12
            Me.GridControl1.ViewCollection.AddRange(New DevExpress.XtraGrid.Views.Base.BaseView() {Me.GridView1})
            '
            'GridView1
            '
            Me.GridView1.Appearance.ColumnFilterButton.BackColor = System.Drawing.Color.FromArgb(CType(CType(104, Byte), Int32), CType(CType(184, Byte), Int32), CType(CType(251, Byte), Int32))
            Me.GridView1.Appearance.ColumnFilterButton.BorderColor = System.Drawing.Color.FromArgb(CType(CType(104, Byte), Int32), CType(CType(184, Byte), Int32), CType(CType(251, Byte), Int32))
            Me.GridView1.Appearance.ColumnFilterButton.ForeColor = System.Drawing.Color.White
            Me.GridView1.Appearance.ColumnFilterButton.Options.UseBackColor = True
            Me.GridView1.Appearance.ColumnFilterButton.Options.UseBorderColor = True
            Me.GridView1.Appearance.ColumnFilterButton.Options.UseForeColor = True
            Me.GridView1.Appearance.ColumnFilterButtonActive.BackColor = System.Drawing.Color.FromArgb(CType(CType(170, Byte), Int32), CType(CType(216, Byte), Int32), CType(CType(254, Byte), Int32))
            Me.GridView1.Appearance.ColumnFilterButtonActive.BorderColor = System.Drawing.Color.FromArgb(CType(CType(170, Byte), Int32), CType(CType(216, Byte), Int32), CType(CType(254, Byte), Int32))
            Me.GridView1.Appearance.ColumnFilterButtonActive.ForeColor = System.Drawing.Color.Black
            Me.GridView1.Appearance.ColumnFilterButtonActive.Options.UseBackColor = True
            Me.GridView1.Appearance.ColumnFilterButtonActive.Options.UseBorderColor = True
            Me.GridView1.Appearance.ColumnFilterButtonActive.Options.UseForeColor = True
            Me.GridView1.Appearance.Empty.BackColor = System.Drawing.Color.FromArgb(CType(CType(236, Byte), Int32), CType(CType(246, Byte), Int32), CType(CType(255, Byte), Int32))
            Me.GridView1.Appearance.Empty.BackColor2 = System.Drawing.Color.White
            Me.GridView1.Appearance.Empty.Options.UseBackColor = True
            Me.GridView1.Appearance.EvenRow.BackColor = System.Drawing.Color.FromArgb(CType(CType(247, Byte), Int32), CType(CType(251, Byte), Int32), CType(CType(255, Byte), Int32))
            Me.GridView1.Appearance.EvenRow.BorderColor = System.Drawing.Color.FromArgb(CType(CType(247, Byte), Int32), CType(CType(251, Byte), Int32), CType(CType(255, Byte), Int32))
            Me.GridView1.Appearance.EvenRow.ForeColor = System.Drawing.Color.Black
            Me.GridView1.Appearance.EvenRow.Options.UseBackColor = True
            Me.GridView1.Appearance.EvenRow.Options.UseBorderColor = True
            Me.GridView1.Appearance.EvenRow.Options.UseForeColor = True
            Me.GridView1.Appearance.FilterCloseButton.BackColor = System.Drawing.Color.FromArgb(CType(CType(104, Byte), Int32), CType(CType(184, Byte), Int32), CType(CType(251, Byte), Int32))
            Me.GridView1.Appearance.FilterCloseButton.BorderColor = System.Drawing.Color.FromArgb(CType(CType(104, Byte), Int32), CType(CType(184, Byte), Int32), CType(CType(251, Byte), Int32))
            Me.GridView1.Appearance.FilterCloseButton.ForeColor = System.Drawing.Color.White
            Me.GridView1.Appearance.FilterCloseButton.Options.UseBackColor = True
            Me.GridView1.Appearance.FilterCloseButton.Options.UseBorderColor = True
            Me.GridView1.Appearance.FilterCloseButton.Options.UseForeColor = True
            Me.GridView1.Appearance.FilterPanel.BackColor = System.Drawing.Color.FromArgb(CType(CType(236, Byte), Int32), CType(CType(246, Byte), Int32), CType(CType(255, Byte), Int32))
            Me.GridView1.Appearance.FilterPanel.BackColor2 = System.Drawing.Color.White
            Me.GridView1.Appearance.FilterPanel.ForeColor = System.Drawing.Color.Black
            Me.GridView1.Appearance.FilterPanel.Options.UseBackColor = True
            Me.GridView1.Appearance.FilterPanel.Options.UseForeColor = True
            Me.GridView1.Appearance.FixedLine.BackColor = System.Drawing.Color.FromArgb(CType(CType(59, Byte), Int32), CType(CType(133, Byte), Int32), CType(CType(195, Byte), Int32))
            Me.GridView1.Appearance.FixedLine.Options.UseBackColor = True
            Me.GridView1.Appearance.FocusedCell.BackColor = System.Drawing.Color.White
            Me.GridView1.Appearance.FocusedCell.ForeColor = System.Drawing.Color.Black
            Me.GridView1.Appearance.FocusedCell.Options.UseBackColor = True
            Me.GridView1.Appearance.FocusedCell.Options.UseForeColor = True
            Me.GridView1.Appearance.FocusedRow.BackColor = System.Drawing.Color.FromArgb(CType(CType(38, Byte), Int32), CType(CType(109, Byte), Int32), CType(CType(189, Byte), Int32))
            Me.GridView1.Appearance.FocusedRow.BorderColor = System.Drawing.Color.FromArgb(CType(CType(59, Byte), Int32), CType(CType(139, Byte), Int32), CType(CType(206, Byte), Int32))
            Me.GridView1.Appearance.FocusedRow.ForeColor = System.Drawing.Color.White
            Me.GridView1.Appearance.FocusedRow.Options.UseBackColor = True
            Me.GridView1.Appearance.FocusedRow.Options.UseBorderColor = True
            Me.GridView1.Appearance.FocusedRow.Options.UseForeColor = True
            Me.GridView1.Appearance.FooterPanel.BackColor = System.Drawing.Color.FromArgb(CType(CType(104, Byte), Int32), CType(CType(184, Byte), Int32), CType(CType(251, Byte), Int32))
            Me.GridView1.Appearance.FooterPanel.BorderColor = System.Drawing.Color.FromArgb(CType(CType(104, Byte), Int32), CType(CType(184, Byte), Int32), CType(CType(251, Byte), Int32))
            Me.GridView1.Appearance.FooterPanel.ForeColor = System.Drawing.Color.Black
            Me.GridView1.Appearance.FooterPanel.Options.UseBackColor = True
            Me.GridView1.Appearance.FooterPanel.Options.UseBorderColor = True
            Me.GridView1.Appearance.FooterPanel.Options.UseForeColor = True
            Me.GridView1.Appearance.GroupButton.BackColor = System.Drawing.Color.FromArgb(CType(CType(104, Byte), Int32), CType(CType(184, Byte), Int32), CType(CType(251, Byte), Int32))
            Me.GridView1.Appearance.GroupButton.BorderColor = System.Drawing.Color.FromArgb(CType(CType(104, Byte), Int32), CType(CType(184, Byte), Int32), CType(CType(251, Byte), Int32))
            Me.GridView1.Appearance.GroupButton.Options.UseBackColor = True
            Me.GridView1.Appearance.GroupButton.Options.UseBorderColor = True
            Me.GridView1.Appearance.GroupFooter.BackColor = System.Drawing.Color.FromArgb(CType(CType(170, Byte), Int32), CType(CType(216, Byte), Int32), CType(CType(254, Byte), Int32))
            Me.GridView1.Appearance.GroupFooter.BorderColor = System.Drawing.Color.FromArgb(CType(CType(170, Byte), Int32), CType(CType(216, Byte), Int32), CType(CType(254, Byte), Int32))
            Me.GridView1.Appearance.GroupFooter.ForeColor = System.Drawing.Color.Black
            Me.GridView1.Appearance.GroupFooter.Options.UseBackColor = True
            Me.GridView1.Appearance.GroupFooter.Options.UseBorderColor = True
            Me.GridView1.Appearance.GroupFooter.Options.UseForeColor = True
            Me.GridView1.Appearance.GroupPanel.BackColor = System.Drawing.Color.FromArgb(CType(CType(236, Byte), Int32), CType(CType(246, Byte), Int32), CType(CType(255, Byte), Int32))
            Me.GridView1.Appearance.GroupPanel.BackColor2 = System.Drawing.Color.White
            Me.GridView1.Appearance.GroupPanel.ForeColor = System.Drawing.Color.Black
            Me.GridView1.Appearance.GroupPanel.Options.UseBackColor = True
            Me.GridView1.Appearance.GroupPanel.Options.UseForeColor = True
            Me.GridView1.Appearance.GroupRow.BackColor = System.Drawing.Color.FromArgb(CType(CType(170, Byte), Int32), CType(CType(216, Byte), Int32), CType(CType(254, Byte), Int32))
            Me.GridView1.Appearance.GroupRow.BorderColor = System.Drawing.Color.FromArgb(CType(CType(170, Byte), Int32), CType(CType(216, Byte), Int32), CType(CType(254, Byte), Int32))
            Me.GridView1.Appearance.GroupRow.ForeColor = System.Drawing.Color.Black
            Me.GridView1.Appearance.GroupRow.Options.UseBackColor = True
            Me.GridView1.Appearance.GroupRow.Options.UseBorderColor = True
            Me.GridView1.Appearance.GroupRow.Options.UseForeColor = True
            Me.GridView1.Appearance.HeaderPanel.BackColor = System.Drawing.Color.FromArgb(CType(CType(139, Byte), Int32), CType(CType(201, Byte), Int32), CType(CType(254, Byte), Int32))
            Me.GridView1.Appearance.HeaderPanel.BorderColor = System.Drawing.Color.FromArgb(CType(CType(139, Byte), Int32), CType(CType(201, Byte), Int32), CType(CType(254, Byte), Int32))
            Me.GridView1.Appearance.HeaderPanel.ForeColor = System.Drawing.Color.Black
            Me.GridView1.Appearance.HeaderPanel.Options.UseBackColor = True
            Me.GridView1.Appearance.HeaderPanel.Options.UseBorderColor = True
            Me.GridView1.Appearance.HeaderPanel.Options.UseForeColor = True
            Me.GridView1.Appearance.HideSelectionRow.BackColor = System.Drawing.Color.FromArgb(CType(CType(105, Byte), Int32), CType(CType(170, Byte), Int32), CType(CType(225, Byte), Int32))
            Me.GridView1.Appearance.HideSelectionRow.BorderColor = System.Drawing.Color.FromArgb(CType(CType(83, Byte), Int32), CType(CType(155, Byte), Int32), CType(CType(215, Byte), Int32))
            Me.GridView1.Appearance.HideSelectionRow.ForeColor = System.Drawing.Color.FromArgb(CType(CType(236, Byte), Int32), CType(CType(246, Byte), Int32), CType(CType(255, Byte), Int32))
            Me.GridView1.Appearance.HideSelectionRow.Options.UseBackColor = True
            Me.GridView1.Appearance.HideSelectionRow.Options.UseBorderColor = True
            Me.GridView1.Appearance.HideSelectionRow.Options.UseForeColor = True
            Me.GridView1.Appearance.HorzLine.BackColor = System.Drawing.Color.FromArgb(CType(CType(104, Byte), Int32), CType(CType(184, Byte), Int32), CType(CType(251, Byte), Int32))
            Me.GridView1.Appearance.HorzLine.Options.UseBackColor = True
            Me.GridView1.Appearance.OddRow.BackColor = System.Drawing.Color.FromArgb(CType(CType(236, Byte), Int32), CType(CType(246, Byte), Int32), CType(CType(255, Byte), Int32))
            Me.GridView1.Appearance.OddRow.BorderColor = System.Drawing.Color.FromArgb(CType(CType(236, Byte), Int32), CType(CType(246, Byte), Int32), CType(CType(255, Byte), Int32))
            Me.GridView1.Appearance.OddRow.ForeColor = System.Drawing.Color.Black
            Me.GridView1.Appearance.OddRow.Options.UseBackColor = True
            Me.GridView1.Appearance.OddRow.Options.UseBorderColor = True
            Me.GridView1.Appearance.OddRow.Options.UseForeColor = True
            Me.GridView1.Appearance.Preview.Font = New System.Drawing.Font("Verdana", 7.5!)
            Me.GridView1.Appearance.Preview.ForeColor = System.Drawing.Color.FromArgb(CType(CType(83, Byte), Int32), CType(CType(155, Byte), Int32), CType(CType(215, Byte), Int32))
            Me.GridView1.Appearance.Preview.Options.UseFont = True
            Me.GridView1.Appearance.Preview.Options.UseForeColor = True
            Me.GridView1.Appearance.Row.BackColor = System.Drawing.Color.FromArgb(CType(CType(247, Byte), Int32), CType(CType(251, Byte), Int32), CType(CType(255, Byte), Int32))
            Me.GridView1.Appearance.Row.ForeColor = System.Drawing.Color.Black
            Me.GridView1.Appearance.Row.Options.UseBackColor = True
            Me.GridView1.Appearance.Row.Options.UseForeColor = True
            Me.GridView1.Appearance.RowSeparator.BackColor = System.Drawing.Color.FromArgb(CType(CType(236, Byte), Int32), CType(CType(246, Byte), Int32), CType(CType(255, Byte), Int32))
            Me.GridView1.Appearance.RowSeparator.BackColor2 = System.Drawing.Color.White
            Me.GridView1.Appearance.RowSeparator.Options.UseBackColor = True
            Me.GridView1.Appearance.SelectedRow.BackColor = System.Drawing.Color.FromArgb(CType(CType(83, Byte), Int32), CType(CType(155, Byte), Int32), CType(CType(215, Byte), Int32))
            Me.GridView1.Appearance.SelectedRow.ForeColor = System.Drawing.Color.White
            Me.GridView1.Appearance.SelectedRow.Options.UseBackColor = True
            Me.GridView1.Appearance.SelectedRow.Options.UseForeColor = True
            Me.GridView1.Appearance.TopNewRow.BackColor = System.Drawing.Color.White
            Me.GridView1.Appearance.TopNewRow.Options.UseBackColor = True
            Me.GridView1.Appearance.VertLine.BackColor = System.Drawing.Color.FromArgb(CType(CType(104, Byte), Int32), CType(CType(184, Byte), Int32), CType(CType(251, Byte), Int32))
            Me.GridView1.Appearance.VertLine.Options.UseBackColor = True
            Me.GridView1.Columns.AddRange(New DevExpress.XtraGrid.Columns.GridColumn() {Me.col_creditor, Me.col_name, Me.col_debit_amt, Me.col_disbursement_factor, Me.col_priority, Me.col_balance, Me.col_acct_number, Me.col_interest, Me.col_held, Me.col_IncludeInFees, Me.col_IncludeInProrate, Me.col_Prorate, Me.col_ZeroBalance, Me.col_orig_balance, Me.col_total_interest, Me.col_debt_id, Me.col_sched_payment, Me.col_payments_month_0, Me.col_line_number, Me.col_creditor_type, Me.col_orig_dmp_payment})
            Me.GridView1.CustomizationFormBounds = New System.Drawing.Rectangle(816, 570, 208, 170)
            StyleFormatCondition1.Appearance.BackColor = System.Drawing.Color.IndianRed
            StyleFormatCondition1.Appearance.BackColor2 = System.Drawing.Color.Tomato
            StyleFormatCondition1.Appearance.BorderColor = System.Drawing.Color.Coral
            StyleFormatCondition1.Appearance.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold)
            StyleFormatCondition1.Appearance.ForeColor = System.Drawing.Color.White
            StyleFormatCondition1.Appearance.Options.UseBackColor = True
            StyleFormatCondition1.Appearance.Options.UseBorderColor = True
            StyleFormatCondition1.Appearance.Options.UseFont = True
            StyleFormatCondition1.Appearance.Options.UseForeColor = True
            StyleFormatCondition1.ApplyToRow = True
            StyleFormatCondition1.Column = Me.col_held
            StyleFormatCondition1.Condition = DevExpress.XtraGrid.FormatConditionEnum.NotEqual
            StyleFormatCondition1.Value1 = False
            Me.GridView1.FormatConditions.AddRange(New DevExpress.XtraGrid.StyleFormatCondition() {StyleFormatCondition1})
            Me.GridView1.GridControl = Me.GridControl1
            Me.GridView1.Name = "GridView1"
            Me.GridView1.OptionsView.ColumnAutoWidth = False
            Me.GridView1.OptionsView.ShowFilterPanelMode = DevExpress.XtraGrid.Views.Base.ShowFilterPanelMode.Never
            Me.GridView1.OptionsView.ShowFooter = True
            Me.GridView1.OptionsView.ShowGroupPanel = False
            Me.GridView1.OptionsView.ShowIndicator = False
            Me.GridView1.SortInfo.AddRange(New DevExpress.XtraGrid.Columns.GridColumnSortInfo() {New DevExpress.XtraGrid.Columns.GridColumnSortInfo(Me.col_creditor, DevExpress.Data.ColumnSortOrder.Ascending)})
            '
            'col_creditor
            '
            Me.col_creditor.Caption = "Creditor"
            Me.col_creditor.CustomizationCaption = "Creditor ID"
            Me.col_creditor.FieldName = "Creditor"
            Me.col_creditor.Name = "col_creditor"
            Me.col_creditor.OptionsColumn.AllowEdit = False
            Me.col_creditor.SortMode = DevExpress.XtraGrid.ColumnSortMode.DisplayText
            Me.col_creditor.ToolTip = "ID for the creditor"
            Me.col_creditor.UnboundType = DevExpress.Data.UnboundColumnType.[String]
            Me.col_creditor.Visible = True
            Me.col_creditor.VisibleIndex = 0
            Me.col_creditor.Width = 58
            '
            'col_name
            '
            Me.col_name.AppearanceCell.Options.UseTextOptions = True
            Me.col_name.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
            Me.col_name.AppearanceHeader.Options.UseTextOptions = True
            Me.col_name.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
            Me.col_name.Caption = "Name"
            Me.col_name.CustomizationCaption = "Creditor Name"
            Me.col_name.FieldName = "creditor_name"
            Me.col_name.Name = "col_name"
            Me.col_name.OptionsColumn.AllowEdit = False
            Me.col_name.SummaryItem.DisplayFormat = "{0:n0} debt(s)"
            Me.col_name.SummaryItem.SummaryType = DevExpress.Data.SummaryItemType.Count
            Me.col_name.ToolTip = "Creditor Name"
            Me.col_name.UnboundType = DevExpress.Data.UnboundColumnType.[String]
            Me.col_name.Visible = True
            Me.col_name.VisibleIndex = 1
            Me.col_name.Width = 108
            '
            'col_debit_amt
            '
            Me.col_debit_amt.AppearanceCell.Options.UseTextOptions = True
            Me.col_debit_amt.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
            Me.col_debit_amt.AppearanceHeader.Options.UseTextOptions = True
            Me.col_debit_amt.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
            Me.col_debit_amt.Caption = "Pay"
            Me.col_debit_amt.ColumnEdit = Me.CalcEdit_col_debit_amt
            Me.col_debit_amt.CustomizationCaption = "Amount to be disbursed"
            Me.col_debit_amt.DisplayFormat.FormatString = "c2"
            Me.col_debit_amt.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
            Me.col_debit_amt.FieldName = "debit_amt"
            Me.col_debit_amt.GroupFormat.FormatString = "c2"
            Me.col_debit_amt.GroupFormat.FormatType = DevExpress.Utils.FormatType.Numeric
            Me.col_debit_amt.Name = "col_debit_amt"
            Me.col_debit_amt.SummaryItem.DisplayFormat = "{0:c}"
            Me.col_debit_amt.SummaryItem.SummaryType = DevExpress.Data.SummaryItemType.Sum
            Me.col_debit_amt.ToolTip = "Current amount to be paid"
            Me.col_debit_amt.UnboundType = DevExpress.Data.UnboundColumnType.[Decimal]
            Me.col_debit_amt.Visible = True
            Me.col_debit_amt.VisibleIndex = 4
            Me.col_debit_amt.Width = 68
            '
            'CalcEdit_col_debit_amt
            '
            Me.CalcEdit_col_debit_amt.AllowNullInput = DevExpress.Utils.DefaultBoolean.[False]
            Me.CalcEdit_col_debit_amt.Appearance.Options.UseTextOptions = True
            Me.CalcEdit_col_debit_amt.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
            Me.CalcEdit_col_debit_amt.AutoHeight = False
            Me.CalcEdit_col_debit_amt.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
            Me.CalcEdit_col_debit_amt.DisplayFormat.FormatString = "c2"
            Me.CalcEdit_col_debit_amt.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
            Me.CalcEdit_col_debit_amt.EditFormat.FormatString = "f2"
            Me.CalcEdit_col_debit_amt.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric
            Me.CalcEdit_col_debit_amt.Name = "CalcEdit_col_debit_amt"
            Me.CalcEdit_col_debit_amt.Precision = 2
            Me.CalcEdit_col_debit_amt.ValidateOnEnterKey = True
            '
            'col_disbursement_factor
            '
            Me.col_disbursement_factor.AppearanceCell.Options.UseTextOptions = True
            Me.col_disbursement_factor.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
            Me.col_disbursement_factor.AppearanceHeader.Options.UseTextOptions = True
            Me.col_disbursement_factor.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
            Me.col_disbursement_factor.Caption = "Disb Factor"
            Me.col_disbursement_factor.CustomizationCaption = "Normal Monthly Payment"
            Me.col_disbursement_factor.DisplayFormat.FormatString = "c2"
            Me.col_disbursement_factor.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
            Me.col_disbursement_factor.FieldName = "disbursement_factor"
            Me.col_disbursement_factor.GroupFormat.FormatString = "c2"
            Me.col_disbursement_factor.GroupFormat.FormatType = DevExpress.Utils.FormatType.Numeric
            Me.col_disbursement_factor.Name = "col_disbursement_factor"
            Me.col_disbursement_factor.OptionsColumn.AllowEdit = False
            Me.col_disbursement_factor.SummaryItem.DisplayFormat = "{0:c}"
            Me.col_disbursement_factor.SummaryItem.SummaryType = DevExpress.Data.SummaryItemType.Sum
            Me.col_disbursement_factor.ToolTip = "Normal monthly payment amount"
            Me.col_disbursement_factor.UnboundType = DevExpress.Data.UnboundColumnType.[Decimal]
            Me.col_disbursement_factor.Visible = True
            Me.col_disbursement_factor.VisibleIndex = 3
            Me.col_disbursement_factor.Width = 67
            '
            'col_priority
            '
            Me.col_priority.AppearanceCell.Options.UseTextOptions = True
            Me.col_priority.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center
            Me.col_priority.AppearanceHeader.Options.UseTextOptions = True
            Me.col_priority.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center
            Me.col_priority.Caption = "P"
            Me.col_priority.CustomizationCaption = "Priority for the debt payment"
            Me.col_priority.DisplayFormat.FormatString = "{0:f0}"
            Me.col_priority.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
            Me.col_priority.FieldName = "priority"
            Me.col_priority.Name = "col_priority"
            Me.col_priority.OptionsColumn.AllowEdit = False
            Me.col_priority.ToolTip = "Priority"
            Me.col_priority.UnboundType = DevExpress.Data.UnboundColumnType.[Integer]
            Me.col_priority.Visible = True
            Me.col_priority.VisibleIndex = 6
            Me.col_priority.Width = 20
            '
            'col_balance
            '
            Me.col_balance.AppearanceCell.Options.UseTextOptions = True
            Me.col_balance.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
            Me.col_balance.AppearanceHeader.Options.UseTextOptions = True
            Me.col_balance.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
            Me.col_balance.Caption = "Balance"
            Me.col_balance.CustomizationCaption = "Debt Balance"
            Me.col_balance.DisplayFormat.FormatString = "c2"
            Me.col_balance.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
            Me.col_balance.FieldName = "display_current_balance"
            Me.col_balance.GroupFormat.FormatString = "c2"
            Me.col_balance.GroupFormat.FormatType = DevExpress.Utils.FormatType.Numeric
            Me.col_balance.Name = "col_balance"
            Me.col_balance.OptionsColumn.AllowEdit = False
            Me.col_balance.SummaryItem.DisplayFormat = "{0:c}"
            Me.col_balance.SummaryItem.SummaryType = DevExpress.Data.SummaryItemType.Sum
            Me.col_balance.ToolTip = "Debt Balance"
            Me.col_balance.UnboundType = DevExpress.Data.UnboundColumnType.[Decimal]
            Me.col_balance.Visible = True
            Me.col_balance.VisibleIndex = 5
            Me.col_balance.Width = 72
            '
            'col_acct_number
            '
            Me.col_acct_number.Caption = "Acct#"
            Me.col_acct_number.CustomizationCaption = "Account Number"
            Me.col_acct_number.FieldName = "account_number"
            Me.col_acct_number.Name = "col_acct_number"
            Me.col_acct_number.OptionsColumn.AllowEdit = False
            Me.col_acct_number.ToolTip = "Account Number"
            Me.col_acct_number.UnboundType = DevExpress.Data.UnboundColumnType.[String]
            Me.col_acct_number.Visible = True
            Me.col_acct_number.VisibleIndex = 2
            Me.col_acct_number.Width = 125
            '
            'col_interest
            '
            Me.col_interest.AppearanceCell.Options.UseTextOptions = True
            Me.col_interest.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
            Me.col_interest.AppearanceHeader.Options.UseTextOptions = True
            Me.col_interest.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
            Me.col_interest.Caption = "Interest"
            Me.col_interest.CustomizationCaption = "Debt Interest Rate"
            Me.col_interest.DisplayFormat.FormatString = "{0:p}"
            Me.col_interest.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
            Me.col_interest.FieldName = "display_intererst_rate"
            Me.col_interest.GroupFormat.FormatString = "{0:p}"
            Me.col_interest.GroupFormat.FormatType = DevExpress.Utils.FormatType.Numeric
            Me.col_interest.Name = "col_interest"
            Me.col_interest.OptionsColumn.AllowEdit = False
            Me.col_interest.ToolTip = "Interest rate for debt"
            Me.col_interest.Visible = True
            Me.col_interest.VisibleIndex = 7
            Me.col_interest.Width = 51
            '
            'col_IncludeInFees
            '
            Me.col_IncludeInFees.Caption = "IncludeInFees"
            Me.col_IncludeInFees.CustomizationCaption = "IncludeInFees"
            Me.col_IncludeInFees.FieldName = "IncludeInFees"
            Me.col_IncludeInFees.Name = "col_IncludeInFees"
            Me.col_IncludeInFees.OptionsColumn.AllowEdit = False
            Me.col_IncludeInFees.Width = 80
            '
            'col_IncludeInProrate
            '
            Me.col_IncludeInProrate.Caption = "IncludeInProrate"
            Me.col_IncludeInProrate.CustomizationCaption = "IncludeInProrate"
            Me.col_IncludeInProrate.FieldName = "IncludeInProrate"
            Me.col_IncludeInProrate.Name = "col_IncludeInProrate"
            Me.col_IncludeInProrate.OptionsColumn.AllowEdit = False
            Me.col_IncludeInProrate.Width = 93
            '
            'col_Prorate
            '
            Me.col_Prorate.Caption = "Prorate"
            Me.col_Prorate.CustomizationCaption = "Prorate"
            Me.col_Prorate.FieldName = "ccl_prorate"
            Me.col_Prorate.Name = "col_Prorate"
            Me.col_Prorate.OptionsColumn.AllowEdit = False
            Me.col_Prorate.Width = 48
            '
            'col_ZeroBalance
            '
            Me.col_ZeroBalance.Caption = "ZeroBalance"
            Me.col_ZeroBalance.CustomizationCaption = "ZeroBalance"
            Me.col_ZeroBalance.FieldName = "ccl_zero_balance"
            Me.col_ZeroBalance.Name = "col_ZeroBalance"
            Me.col_ZeroBalance.OptionsColumn.AllowEdit = False
            Me.col_ZeroBalance.Width = 71
            '
            'col_orig_balance
            '
            Me.col_orig_balance.AppearanceCell.Options.UseTextOptions = True
            Me.col_orig_balance.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
            Me.col_orig_balance.AppearanceHeader.Options.UseTextOptions = True
            Me.col_orig_balance.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
            Me.col_orig_balance.Caption = "Orig Bal"
            Me.col_orig_balance.CustomizationCaption = "Original Balance"
            Me.col_orig_balance.DisplayFormat.FormatString = "c2"
            Me.col_orig_balance.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
            Me.col_orig_balance.FieldName = "display_orig_balance"
            Me.col_orig_balance.GroupFormat.FormatString = "c2"
            Me.col_orig_balance.GroupFormat.FormatType = DevExpress.Utils.FormatType.Numeric
            Me.col_orig_balance.Name = "col_orig_balance"
            Me.col_orig_balance.OptionsColumn.AllowEdit = False
            Me.col_orig_balance.SummaryItem.DisplayFormat = "{0:c}"
            Me.col_orig_balance.SummaryItem.SummaryType = DevExpress.Data.SummaryItemType.Sum
            Me.col_orig_balance.Width = 49
            '
            'col_total_interest
            '
            Me.col_total_interest.AppearanceCell.Options.UseTextOptions = True
            Me.col_total_interest.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
            Me.col_total_interest.AppearanceHeader.Options.UseTextOptions = True
            Me.col_total_interest.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
            Me.col_total_interest.Caption = "total_interest"
            Me.col_total_interest.CustomizationCaption = "total_interest"
            Me.col_total_interest.DisplayFormat.FormatString = "c2"
            Me.col_total_interest.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
            Me.col_total_interest.FieldName = "display_total_interest"
            Me.col_total_interest.GroupFormat.FormatString = "c2"
            Me.col_total_interest.GroupFormat.FormatType = DevExpress.Utils.FormatType.Numeric
            Me.col_total_interest.Name = "col_total_interest"
            Me.col_total_interest.OptionsColumn.AllowEdit = False
            Me.col_total_interest.SummaryItem.DisplayFormat = "{0:c}"
            Me.col_total_interest.SummaryItem.SummaryType = DevExpress.Data.SummaryItemType.Sum
            Me.col_total_interest.Width = 77
            '
            'col_debt_id
            '
            Me.col_debt_id.Caption = "ID"
            Me.col_debt_id.CustomizationCaption = "Debt ID"
            Me.col_debt_id.FieldName = "display_debt_id"
            Me.col_debt_id.Name = "col_debt_id"
            Me.col_debt_id.OptionsColumn.AllowEdit = False
            Me.col_debt_id.Width = 23
            '
            'col_sched_payment
            '
            Me.col_sched_payment.AppearanceCell.Options.UseTextOptions = True
            Me.col_sched_payment.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
            Me.col_sched_payment.AppearanceHeader.Options.UseTextOptions = True
            Me.col_sched_payment.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
            Me.col_sched_payment.Caption = "Scheduled"
            Me.col_sched_payment.CustomizationCaption = "Scheduled Payment"
            Me.col_sched_payment.DisplayFormat.FormatString = "c2"
            Me.col_sched_payment.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
            Me.col_sched_payment.FieldName = "sched_payment"
            Me.col_sched_payment.GroupFormat.FormatString = "c"
            Me.col_sched_payment.GroupFormat.FormatType = DevExpress.Utils.FormatType.Numeric
            Me.col_sched_payment.Name = "col_sched_payment"
            Me.col_sched_payment.OptionsColumn.AllowEdit = False
            Me.col_sched_payment.SummaryItem.DisplayFormat = "{0:c}"
            Me.col_sched_payment.SummaryItem.SummaryType = DevExpress.Data.SummaryItemType.Sum
            Me.col_sched_payment.Width = 68
            '
            'col_payments_month_0
            '
            Me.col_payments_month_0.AppearanceCell.Options.UseTextOptions = True
            Me.col_payments_month_0.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
            Me.col_payments_month_0.AppearanceHeader.Options.UseTextOptions = True
            Me.col_payments_month_0.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
            Me.col_payments_month_0.Caption = "Curr Mo Disb"
            Me.col_payments_month_0.CustomizationCaption = "Payments made this month"
            Me.col_payments_month_0.DisplayFormat.FormatString = "c2"
            Me.col_payments_month_0.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
            Me.col_payments_month_0.FieldName = "payments_month_0"
            Me.col_payments_month_0.GroupFormat.FormatString = "c2"
            Me.col_payments_month_0.GroupFormat.FormatType = DevExpress.Utils.FormatType.Numeric
            Me.col_payments_month_0.Name = "col_payments_month_0"
            Me.col_payments_month_0.OptionsColumn.AllowEdit = False
            Me.col_payments_month_0.SummaryItem.DisplayFormat = "{0:c}"
            Me.col_payments_month_0.SummaryItem.SummaryType = DevExpress.Data.SummaryItemType.Sum
            Me.col_payments_month_0.Width = 73
            '
            'col_line_number
            '
            Me.col_line_number.Caption = "Line"
            Me.col_line_number.CustomizationCaption = "Stacking Order"
            Me.col_line_number.FieldName = "line_number"
            Me.col_line_number.Name = "col_line_number"
            Me.col_line_number.OptionsColumn.AllowEdit = False
            Me.col_line_number.Width = 41
            '
            'col_creditor_type
            '
            Me.col_creditor_type.Caption = "Type"
            Me.col_creditor_type.CustomizationCaption = "Bill/Deduct/None Contrib status"
            Me.col_creditor_type.FieldName = "creditor_type"
            Me.col_creditor_type.Name = "col_creditor_type"
            Me.col_creditor_type.OptionsColumn.AllowEdit = False
            Me.col_creditor_type.Width = 50
            '
            'col_orig_dmp_payment
            '
            Me.col_orig_dmp_payment.Caption = "Orig Pmt"
            Me.col_orig_dmp_payment.CustomizationCaption = "Payment proposed to creditor"
            Me.col_orig_dmp_payment.FieldName = "orig_dmp_payment"
            Me.col_orig_dmp_payment.Name = "col_orig_dmp_payment"
            Me.col_orig_dmp_payment.OptionsColumn.AllowEdit = False
            Me.col_orig_dmp_payment.SummaryItem.DisplayFormat = "{0:c}"
            Me.col_orig_dmp_payment.SummaryItem.SummaryType = DevExpress.Data.SummaryItemType.Sum
            Me.col_orig_dmp_payment.Width = 53
            '
            'DepositsControl1
            '
            Me.DepositsControl1.Location = New System.Drawing.Point(238, 158)
            Me.DepositsControl1.MaximumSize = New System.Drawing.Size(148, 101)
            Me.DepositsControl1.Name = "DepositsControl1"
            Me.DepositsControl1.Size = New System.Drawing.Size(148, 101)
            Me.DepositsControl1.TabIndex = 10
            '
            'GroupControl2
            '
            Me.GroupControl2.Controls.Add(Me.Difference)
            Me.GroupControl2.Controls.Add(Me.SumOfDisbursements)
            Me.GroupControl2.Controls.Add(Me.TrustFundsAvailable)
            Me.GroupControl2.Controls.Add(Me.TrustFundsOnHold)
            Me.GroupControl2.Controls.Add(Me.LabelControl10)
            Me.GroupControl2.Controls.Add(Me.LabelControl13)
            Me.GroupControl2.Controls.Add(Me.LabelControl14)
            Me.GroupControl2.Controls.Add(Me.LabelControl15)
            Me.GroupControl2.Controls.Add(Me.LabelControl16)
            Me.GroupControl2.Location = New System.Drawing.Point(8, 158)
            Me.GroupControl2.Name = "GroupControl2"
            Me.GroupControl2.Size = New System.Drawing.Size(223, 101)
            Me.GroupControl2.TabIndex = 9
            Me.GroupControl2.Text = "Payment Information"
            '
            'Difference
            '
            Me.Difference.Appearance.Options.UseTextOptions = True
            Me.Difference.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
            Me.Difference.Appearance.TextOptions.WordWrap = DevExpress.Utils.WordWrap.NoWrap
            Me.Difference.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None
            Me.Difference.Location = New System.Drawing.Point(134, 72)
            Me.Difference.Name = "Difference"
            Me.Difference.Size = New System.Drawing.Size(84, 13)
            Me.Difference.TabIndex = 14
            Me.Difference.Text = "$0.00"
            Me.Difference.ToolTip = "Excess (both greater and less). This should be $0.00 if the disbursements match t" &
                                    "he trust. No disbursement allowed for funds greater than what is in the trust."
            Me.Difference.UseMnemonic = False
            '
            'SumOfDisbursements
            '
            Me.SumOfDisbursements.Appearance.Options.UseTextOptions = True
            Me.SumOfDisbursements.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
            Me.SumOfDisbursements.Appearance.TextOptions.WordWrap = DevExpress.Utils.WordWrap.NoWrap
            Me.SumOfDisbursements.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None
            Me.SumOfDisbursements.Location = New System.Drawing.Point(134, 56)
            Me.SumOfDisbursements.Name = "SumOfDisbursements"
            Me.SumOfDisbursements.Size = New System.Drawing.Size(84, 13)
            Me.SumOfDisbursements.TabIndex = 13
            Me.SumOfDisbursements.Text = "$0.00"
            Me.SumOfDisbursements.ToolTip = "Total of all of the monies to be paid to creditors"
            Me.SumOfDisbursements.UseMnemonic = False
            '
            'TrustFundsAvailable
            '
            Me.TrustFundsAvailable.Appearance.Options.UseTextOptions = True
            Me.TrustFundsAvailable.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
            Me.TrustFundsAvailable.Appearance.TextOptions.WordWrap = DevExpress.Utils.WordWrap.NoWrap
            Me.TrustFundsAvailable.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None
            Me.TrustFundsAvailable.Location = New System.Drawing.Point(134, 40)
            Me.TrustFundsAvailable.Name = "TrustFundsAvailable"
            Me.TrustFundsAvailable.Size = New System.Drawing.Size(84, 13)
            Me.TrustFundsAvailable.TabIndex = 12
            Me.TrustFundsAvailable.Text = "$0.00"
            Me.TrustFundsAvailable.ToolTip = "Dollar amount available in the trust to be disbursed"
            Me.TrustFundsAvailable.UseMnemonic = False
            '
            'TrustFundsOnHold
            '
            Me.TrustFundsOnHold.Appearance.Options.UseTextOptions = True
            Me.TrustFundsOnHold.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
            Me.TrustFundsOnHold.Appearance.TextOptions.WordWrap = DevExpress.Utils.WordWrap.NoWrap
            Me.TrustFundsOnHold.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None
            Me.TrustFundsOnHold.Location = New System.Drawing.Point(134, 24)
            Me.TrustFundsOnHold.Name = "TrustFundsOnHold"
            Me.TrustFundsOnHold.Size = New System.Drawing.Size(84, 13)
            Me.TrustFundsOnHold.TabIndex = 11
            Me.TrustFundsOnHold.Text = "$0.00"
            Me.TrustFundsOnHold.ToolTip = "Funds that are deposited but not available for disbursement"
            Me.TrustFundsOnHold.UseMnemonic = False
            '
            'LabelControl10
            '
            Me.LabelControl10.Location = New System.Drawing.Point(128, 24)
            Me.LabelControl10.Name = "LabelControl10"
            Me.LabelControl10.Size = New System.Drawing.Size(0, 13)
            Me.LabelControl10.TabIndex = 6
            '
            'LabelControl13
            '
            Me.LabelControl13.Appearance.Options.UseTextOptions = True
            Me.LabelControl13.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
            Me.LabelControl13.Appearance.TextOptions.WordWrap = DevExpress.Utils.WordWrap.NoWrap
            Me.LabelControl13.Location = New System.Drawing.Point(16, 72)
            Me.LabelControl13.Name = "LabelControl13"
            Me.LabelControl13.Size = New System.Drawing.Size(50, 13)
            Me.LabelControl13.TabIndex = 3
            Me.LabelControl13.Text = "Difference"
            Me.LabelControl13.UseMnemonic = False
            '
            'LabelControl14
            '
            Me.LabelControl14.Appearance.Options.UseTextOptions = True
            Me.LabelControl14.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
            Me.LabelControl14.Appearance.TextOptions.WordWrap = DevExpress.Utils.WordWrap.NoWrap
            Me.LabelControl14.Location = New System.Drawing.Point(16, 56)
            Me.LabelControl14.Name = "LabelControl14"
            Me.LabelControl14.Size = New System.Drawing.Size(108, 13)
            Me.LabelControl14.TabIndex = 2
            Me.LabelControl14.Text = "Sum Of Disbursements"
            Me.LabelControl14.UseMnemonic = False
            '
            'LabelControl15
            '
            Me.LabelControl15.Appearance.Options.UseTextOptions = True
            Me.LabelControl15.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
            Me.LabelControl15.Appearance.TextOptions.WordWrap = DevExpress.Utils.WordWrap.NoWrap
            Me.LabelControl15.Location = New System.Drawing.Point(16, 40)
            Me.LabelControl15.Name = "LabelControl15"
            Me.LabelControl15.Size = New System.Drawing.Size(103, 13)
            Me.LabelControl15.TabIndex = 1
            Me.LabelControl15.Text = "Trust Funds Available"
            Me.LabelControl15.UseMnemonic = False
            '
            'LabelControl16
            '
            Me.LabelControl16.Appearance.Options.UseTextOptions = True
            Me.LabelControl16.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
            Me.LabelControl16.Appearance.TextOptions.WordWrap = DevExpress.Utils.WordWrap.NoWrap
            Me.LabelControl16.Location = New System.Drawing.Point(16, 24)
            Me.LabelControl16.Name = "LabelControl16"
            Me.LabelControl16.Size = New System.Drawing.Size(98, 13)
            Me.LabelControl16.TabIndex = 0
            Me.LabelControl16.Text = "Turst Funds On Hold"
            Me.LabelControl16.UseMnemonic = False
            '
            'Fix bug in sorting option
            '
            Me.col_acct_number.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.True
            Me.col_balance.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.True
            Me.col_creditor.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.True
            Me.col_creditor_type.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.True
            Me.col_debit_amt.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.True
            Me.col_debt_id.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.True
            Me.col_disbursement_factor.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.True
            Me.col_held.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.True
            Me.col_IncludeInFees.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.True
            Me.col_IncludeInProrate.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.True
            Me.col_interest.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.True
            Me.col_line_number.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.True
            Me.col_name.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.True
            Me.col_orig_balance.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.True
            Me.col_orig_dmp_payment.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.True
            Me.col_payments_month_0.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.True
            Me.col_priority.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.True
            Me.col_Prorate.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.True
            Me.col_sched_payment.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.True
            Me.col_total_interest.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.True
            Me.col_ZeroBalance.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.True
            '
            'Examine_Form
            '
            Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
            Me.ClientSize = New System.Drawing.Size(658, 449)
            Me.Controls.Add(Me.GroupControl2)
            Me.Controls.Add(Me.DepositsControl1)
            Me.Controls.Add(Me.GridControl1)
            Me.Controls.Add(Me.GroupControl1)
            Me.Controls.Add(Me.ButtonPrevious)
            Me.Controls.Add(Me.ButtonShowNotes)
            Me.Controls.Add(Me.ButtonShowClient)
            Me.Controls.Add(Me.ButtonDontPay)
            Me.Controls.Add(Me.ButtonPay)
            Me.Controls.Add(Me.ButtonProrate)
            Me.Controls.Add(Me.ButtonClear)
            Me.Controls.Add(Me.ButtonQuit)
            Me.Name = "Examine_Form"
            Me.Text = "Disbursement Client Information"
            CType(Me.DxErrorProvider1, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.GroupControl1, System.ComponentModel.ISupportInitialize).EndInit()
            Me.GroupControl1.ResumeLayout(False)
            Me.GroupControl1.PerformLayout()
            CType(Me.GridControl1, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.GridView1, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.CalcEdit_col_debit_amt, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.GroupControl2, System.ComponentModel.ISupportInitialize).EndInit()
            Me.GroupControl2.ResumeLayout(False)
            Me.GroupControl2.PerformLayout()
            Me.ResumeLayout(False)
        End Sub

        Friend WithEvents LabelControl6 As DevExpress.XtraEditors.LabelControl
        Friend WithEvents ButtonQuit As DevExpress.XtraEditors.SimpleButton
        Friend WithEvents ButtonClear As DevExpress.XtraEditors.SimpleButton
        Friend WithEvents ButtonProrate As DevExpress.XtraEditors.SimpleButton
        Friend WithEvents ButtonPay As DevExpress.XtraEditors.SimpleButton
        Friend WithEvents ButtonDontPay As DevExpress.XtraEditors.SimpleButton
        Friend WithEvents ButtonShowClient As DevExpress.XtraEditors.SimpleButton
        Friend WithEvents ButtonShowNotes As DevExpress.XtraEditors.SimpleButton
        Friend WithEvents ButtonPrevious As DevExpress.XtraEditors.SimpleButton
        Friend WithEvents GroupControl1 As DevExpress.XtraEditors.GroupControl
        Friend WithEvents LabelControl1 As DevExpress.XtraEditors.LabelControl
        Friend WithEvents LabelControl2 As DevExpress.XtraEditors.LabelControl
        Friend WithEvents LabelControl3 As DevExpress.XtraEditors.LabelControl
        Friend WithEvents LabelControl4 As DevExpress.XtraEditors.LabelControl
        Friend WithEvents LabelControl5 As DevExpress.XtraEditors.LabelControl
        Friend WithEvents lbl_address As DevExpress.XtraEditors.LabelControl
        Friend WithEvents lbl_client As DevExpress.XtraEditors.LabelControl
        Friend WithEvents lbl_name As DevExpress.XtraEditors.LabelControl
        Friend WithEvents lbl_home_ph As DevExpress.XtraEditors.LabelControl
        Friend WithEvents lbl_work_ph As DevExpress.XtraEditors.LabelControl
        Friend WithEvents lbl_language As DevExpress.XtraEditors.LabelControl
        Friend WithEvents GridControl1 As DevExpress.XtraGrid.GridControl
        Friend WithEvents GridView1 As DevExpress.XtraGrid.Views.Grid.GridView
        Friend WithEvents CalcEdit_col_debit_amt As DevExpress.XtraEditors.Repository.RepositoryItemCalcEdit
        Friend WithEvents col_creditor As DevExpress.XtraGrid.Columns.GridColumn
        Friend WithEvents col_oid As DevExpress.XtraGrid.Columns.GridColumn
        Friend WithEvents col_debt_id As DevExpress.XtraGrid.Columns.GridColumn
        Friend WithEvents col_sched_payment As DevExpress.XtraGrid.Columns.GridColumn
        Friend WithEvents col_disbursement_factor As DevExpress.XtraGrid.Columns.GridColumn
        Friend WithEvents col_payments_month_0 As DevExpress.XtraGrid.Columns.GridColumn
        Friend WithEvents col_orig_balance As DevExpress.XtraGrid.Columns.GridColumn
        Friend WithEvents col_orig_dmp_payment As DevExpress.XtraGrid.Columns.GridColumn
        Friend WithEvents col_interest As DevExpress.XtraGrid.Columns.GridColumn
        Friend WithEvents col_balance As DevExpress.XtraGrid.Columns.GridColumn
        Friend WithEvents col_debit_amt As DevExpress.XtraGrid.Columns.GridColumn
        Friend WithEvents col_priority As DevExpress.XtraGrid.Columns.GridColumn
        Friend WithEvents col_name As DevExpress.XtraGrid.Columns.GridColumn
        Friend WithEvents col_acct_number As DevExpress.XtraGrid.Columns.GridColumn
        Friend WithEvents col_line_number As DevExpress.XtraGrid.Columns.GridColumn
        Friend WithEvents col_ZeroBalance As DevExpress.XtraGrid.Columns.GridColumn
        Friend WithEvents col_Prorate As DevExpress.XtraGrid.Columns.GridColumn
        Friend WithEvents col_creditor_type As DevExpress.XtraGrid.Columns.GridColumn
        Friend WithEvents col_held As DevExpress.XtraGrid.Columns.GridColumn
        Friend WithEvents col_IncludeInProrate As DevExpress.XtraGrid.Columns.GridColumn
        Friend WithEvents col_IncludeInFees As DevExpress.XtraGrid.Columns.GridColumn
        Friend WithEvents col_total_interest As DevExpress.XtraGrid.Columns.GridColumn
        Friend WithEvents DepositsControl1 As DebtPlus.UI.Desktop.Disbursement.Common.DepositsControl
        Friend WithEvents GroupControl2 As DevExpress.XtraEditors.GroupControl
        Friend WithEvents Difference As DevExpress.XtraEditors.LabelControl
        Friend WithEvents SumOfDisbursements As DevExpress.XtraEditors.LabelControl
        Friend WithEvents TrustFundsAvailable As DevExpress.XtraEditors.LabelControl
        Friend WithEvents TrustFundsOnHold As DevExpress.XtraEditors.LabelControl
        Friend WithEvents LabelControl10 As DevExpress.XtraEditors.LabelControl
        Friend WithEvents LabelControl13 As DevExpress.XtraEditors.LabelControl
        Friend WithEvents LabelControl14 As DevExpress.XtraEditors.LabelControl
        Friend WithEvents LabelControl15 As DevExpress.XtraEditors.LabelControl
        Friend WithEvents LabelControl16 As DevExpress.XtraEditors.LabelControl
    End Class
End Namespace
