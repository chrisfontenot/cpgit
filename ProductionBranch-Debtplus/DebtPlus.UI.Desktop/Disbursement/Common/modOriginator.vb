#Region "Copyright 2000-2012 DebtPlus, L.L.C."
'{*******************************************************************}
'{                                                                   }
'{       DebtPlus Debt Management System                             }
'{                                                                   }
'{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
'{       ALL RIGHTS RESERVED                                         }
'{                                                                   }
'{   The entire contents of this file is protected by U.S. and       }
'{   International Copyright Laws. Unauthorized reproduction,        }
'{   reverse-engineering, and distribution of all or any portion of  }
'{   the code contained in this file is strictly prohibited and may  }
'{   result in severe civil and criminal penalties and will be       }
'{   prosecuted to the maximum extent possible under the law.        }
'{                                                                   }
'{   RESTRICTIONS                                                    }
'{                                                                   }
'{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
'{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
'{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
'{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
'{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
'{                                                                   }
'{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
'{   ADDITIONAL RESTRICTIONS.                                        }
'{                                                                   }
'{*******************************************************************}
#End Region

Option Strict Off
Option Explicit On

Namespace Disbursement.Common
    Module modOriginator

        ' Length of each record in the database file
        Public Const FILE_RECORD_LENGTH As Short = 94

        ' File type codes for the various record formats
        Public Const RECORD_TYPE_FILE_HEADER As String = "1"
        Public Const RECORD_TYPE_BATCH_HEADER As String = "5"
        Public Const RECORD_TYPE_ENTRY_DETAIL As String = "6"
        Public Const RECORD_TYPE_ENTRY_DETAIL_ADDENDUM As String = "7"
        Public Const RECORD_TYPE_BATCH_CONTROL As String = "8"
        Public Const RECORD_TYPE_FILE_CONTROL As String = "9"

''' <summary>
        ''' File header record
        ''' </summary>

        Public Const FILE_PRIORITY_CODE As String = "01"
        Public Const FILE_IMMEDIATE_DESTINATION As String = " 999900004"
        Public Const FILE_ID_MODIFIER_VALUE As String = "1"
        Public Const FILE_RECORD_SIZE As String = "094"
        Public Const FILE_BLOCKING_FACTOR As String = "10"
        Public Const FILE_FORMAT_CODE As String = "1"
        Public Const FILE_DESTINATION As String = "MC REMIT PROC CENT SITE"
        Public Const FILE_REFERENCE_CODE_TESTING As String = "ORIGTEST"
        Public Const FILE_REFERENCE_CODE_NORMAL As String = "        "

        ''' <summary>
        ''' Batch header record
        ''' </summary>

        Public Const BATCH_ORIGINATOR_STATUS_CODE As String = "1"
        Public Const SERVICE_CLASS_CREDIT As String = "220"
        Public Const SERVICE_CLASS_DEBIT As String = "225"
        Public Const SERVICE_CLASS_CREDIT_DEBIT As String = "200"
        Public Const ENTRY_CLASS_CIE As String = "CIE" ' Payments
        Public Const ENTRY_CLASS_CDP As String = "CDP" ' Proposals
        Public Const ENTRY_CLASS_CDV As String = "CDV" ' Balance verifications
        Public Const ENTRY_CLASS_CDD As String = "CDD" ' Drops
        Public Const ENTRY_CLASS_COR As String = "COR" ' Returns (for concentrators only)
        Public Const ENTRY_CLASS_BLANK As String = "   "
        Public Const ENTRY_DESCRIPTION_PAYMENT As String = "RPS PAYMNT"
        Public Const ENTRY_DESCRIPTION_REVERSAL As String = "REVERSAL  "
        Public Const ENTRY_DESCRIPTION_RETURN As String = "RETURN    "
        Public Const ENTRY_DESCRIPTION_MESSAGE As String = "MESSAGE   "

        ''' <summary>
        ''' Entry detail record
        ''' </summary>

        Public Const ENTRY_TRANSACTION_CODE_CREDIT As String = "21"
        Public Const ENTRY_TRANSACTION_CODE_PAYMENT As String = "22"
        Public Const ENTRY_TRANSACTION_CODE_PRENOTE As String = "23"
        Public Const ENTRY_TRANSACTION_CODE_RETURN As String = "26"
        Public Const ENTRY_TRANSACTION_CODE_REVERSAL As String = "27"
        Public Const ENTRY_ADDENDUM_UNUSED As String = "0"
        Public Const ENTRY_ADDENDUM_USED As String = "1"
        Public Const ADDENDUM_TYPE_CODE_RETURN As String = "99"
        Public Const ADDENDUM_TYPE_CODE_NONFINANCIAL As String = "98"
        Public Const ADDENDUM_TYPE_CODE_REVERSAL As String = "97"

    End Module
End Namespace
