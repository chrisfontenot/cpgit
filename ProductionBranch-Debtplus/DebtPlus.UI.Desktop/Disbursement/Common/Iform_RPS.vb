Namespace Disbursement.Common
    Friend Interface Iform_RPS
        Function Request_FileName(ByVal disbursement_register As System.Int32, ByVal Bank As System.Int32, ByVal FileType As String) As System.IO.StreamWriter
        Function Request_FileName(ByVal disbursement_register As System.Int32, ByVal Bank As System.Int32, ByVal FileType As String, ByVal UseLocalFileOnly As Boolean) As System.IO.StreamWriter
        WriteOnly Property total_debt() As Decimal
        WriteOnly Property total_credit() As Decimal
        WriteOnly Property total_items() As System.Int32
        WriteOnly Property total_batches() As System.Int32
        WriteOnly Property total_billed() As Decimal
        WriteOnly Property total_gross() As Decimal
        WriteOnly Property total_net() As Decimal
        WriteOnly Property total_prenotes() As System.Int32
        WriteOnly Property batch() As String
    End Interface
End Namespace