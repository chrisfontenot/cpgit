﻿#Region "Copyright 2000-2012 DebtPlus, L.L.C."
'{*******************************************************************}
'{                                                                   }
'{       DebtPlus Debt Management System                             }
'{                                                                   }
'{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
'{       ALL RIGHTS RESERVED                                         }
'{                                                                   }
'{   The entire contents of this file is protected by U.S. and       }
'{   International Copyright Laws. Unauthorized reproduction,        }
'{   reverse-engineering, and distribution of all or any portion of  }
'{   the code contained in this file is strictly prohibited and may  }
'{   result in severe civil and criminal penalties and will be       }
'{   prosecuted to the maximum extent possible under the law.        }
'{                                                                   }
'{   RESTRICTIONS                                                    }
'{                                                                   }
'{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
'{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
'{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
'{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
'{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
'{                                                                   }
'{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
'{   ADDITIONAL RESTRICTIONS.                                        }
'{                                                                   }
'{*******************************************************************}

#End Region

Option Compare Binary
Option Explicit On
Option Strict On

Imports DebtPlus.Interfaces.Debt
Imports DebtPlus.Svc.Common
Imports DebtPlus.Events
Imports System.Data.SqlClient
Imports DebtPlus.Data.Forms

Namespace Disbursement.Common
    Friend Interface IDisbursementContext
        Inherits IDisposable
        Property MasterQuit As Boolean
        Property ap As DebtPlus.Utils.ArgParserBase
        Function CounselorList() As System.Collections.ArrayList
        Function CSRList() As System.Collections.ArrayList
        Function OfficeList() As System.Collections.ArrayList
    End Interface

    Friend Interface ISupportContext
        Property Context As IDisbursementContext
    End Interface

    Friend Class DisbursementContext
        Implements IDisposable
        Implements IDisbursementContext

        Public Property MasterQuit As Boolean = False Implements IDisbursementContext.MasterQuit
        Public Property ap As DebtPlus.Utils.ArgParserBase Implements IDisbursementContext.ap

        Private ReadOnly ds As New System.Data.DataSet("ds")

        Public Function CounselorList() As System.Collections.ArrayList Implements IDisbursementContext.CounselorList
            Const MyTableName As String = "counselors"
            Dim answer As New System.Collections.ArrayList
            Dim tbl As System.Data.DataTable = ds.Tables(MyTableName)

            If tbl Is Nothing Then
                Dim cmd As SqlClient.SqlCommand = New SqlCommand
                With cmd
                    .Connection = New SqlConnection(DebtPlus.LINQ.SQLInfoClass.getDefault().ConnectionString)
                    .CommandText = "SELECT counselor as item_key, name as description FROM view_counselors order by 2"
                    .CommandType = CommandType.Text
                End With

                Dim current_cursor As System.Windows.Forms.Cursor = System.Windows.Forms.Cursor.Current
                Try
                    Dim da As New SqlClient.SqlDataAdapter(cmd)
                    da.Fill(ds, MyTableName)
                    tbl = ds.Tables(MyTableName)

                Catch ex As SqlClient.SqlException
                    DebtPlus.UI.Common.ErrorHandling.HandleErrors(ex)

                Finally
                    System.Windows.Forms.Cursor.Current = current_cursor
                End Try
            End If

            ' Load the list of counselors
            answer.Clear()
            If tbl IsNot Nothing Then
                For Each drv As System.Data.DataRowView In New System.Data.DataView(tbl, String.Empty, "description", DataViewRowState.CurrentRows)
                    If drv("description") IsNot Nothing AndAlso drv("description") IsNot System.DBNull.Value Then
                        answer.Add(New DebtPlus.Data.Controls.ComboboxItem(Convert.ToString(drv("description")), Convert.ToInt32(drv("item_key"))))
                    End If
                Next
            End If

            Return answer
        End Function

        Public Function CSRList() As System.Collections.ArrayList Implements IDisbursementContext.CSRList
            Const MyTableName As String = "CSRs"
            Dim answer As New System.Collections.ArrayList
            Dim tbl As System.Data.DataTable = ds.Tables(MyTableName)

            If tbl Is Nothing Then
                Dim cmd As SqlClient.SqlCommand = New SqlCommand
                With cmd
                    .Connection = New SqlConnection(DebtPlus.LINQ.SQLInfoClass.getDefault().ConnectionString)
                    .CommandText = "SELECT counselor as item_key, name as description FROM view_csrs order by 2"
                    .CommandType = CommandType.Text
                End With

                Dim current_cursor As System.Windows.Forms.Cursor = System.Windows.Forms.Cursor.Current
                Try
                    Dim da As New SqlClient.SqlDataAdapter(cmd)
                    da.Fill(ds, MyTableName)
                    tbl = ds.Tables(MyTableName)

                Catch ex As SqlClient.SqlException
                    DebtPlus.UI.Common.ErrorHandling.HandleErrors(ex)

                Finally
                    System.Windows.Forms.Cursor.Current = current_cursor
                End Try
            End If

            ' Load the list of counselors
            answer.Clear()
            If tbl IsNot Nothing Then
                For Each drv As System.Data.DataRowView In New System.Data.DataView(tbl, String.Empty, "description", DataViewRowState.CurrentRows)
                    If drv("description") IsNot Nothing AndAlso drv("description") IsNot System.DBNull.Value Then
                        answer.Add(New DebtPlus.Data.Controls.ComboboxItem(Convert.ToString(drv("description")), Convert.ToInt32(drv("item_key"))))
                    End If
                Next
            End If

            Return answer
        End Function

        Public Function OfficeList() As System.Collections.ArrayList Implements IDisbursementContext.OfficeList
            Const MyTableName As String = "offices"
            Dim answer As New System.Collections.ArrayList
            Dim tbl As System.Data.DataTable = ds.Tables(MyTableName)

            If tbl Is Nothing Then
                Dim cmd As SqlClient.SqlCommand = New SqlCommand
                With cmd
                    .Connection = New SqlConnection(DebtPlus.LINQ.SQLInfoClass.getDefault().ConnectionString)
                    .CommandText = "lst_offices"
                    .CommandType = CommandType.StoredProcedure
                End With

                Dim current_cursor As System.Windows.Forms.Cursor = System.Windows.Forms.Cursor.Current
                Try
                    Dim da As New SqlClient.SqlDataAdapter(cmd)
                    da.Fill(ds, MyTableName)
                    tbl = ds.Tables(MyTableName)

                Catch ex As SqlClient.SqlException
                    DebtPlus.UI.Common.ErrorHandling.HandleErrors(ex)

                Finally
                    System.Windows.Forms.Cursor.Current = current_cursor
                End Try
            End If

            ' Load the list of counselors
            answer.Clear()
            If tbl IsNot Nothing Then
                For Each drv As System.Data.DataRowView In New System.Data.DataView(tbl, String.Empty, "description", DataViewRowState.CurrentRows)
                    If drv("description") IsNot Nothing AndAlso drv("description") IsNot System.DBNull.Value Then
                        answer.Add(New DebtPlus.Data.Controls.ComboboxItem(Convert.ToString(drv("description")), Convert.ToInt32(drv("item_key"))))
                    End If
                Next
            End If

            Return answer
        End Function

#Region "IDisposable Support"
        Private disposedValue As Boolean ' To detect redundant calls

        ' IDisposable
        Protected Overridable Sub Dispose(disposing As Boolean)
            If Not Me.disposedValue Then
                If disposing Then
                    ds.Dispose()
                End If
            End If
            Me.disposedValue = True
        End Sub

        ' This code added by Visual Basic to correctly implement the disposable pattern.
        Public Sub Dispose() Implements IDisposable.Dispose
            ' Do not change this code.  Put cleanup code in Dispose(ByVal disposing As Boolean) above.
            Dispose(True)
            GC.SuppressFinalize(Me)
        End Sub
#End Region
    End Class
End Namespace
