﻿Namespace Disbursement.Common
    <Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
    Partial Class DepositsControl
        Inherits DevExpress.XtraEditors.XtraUserControl

        'UserControl overrides dispose to clean up the component list.
        <System.Diagnostics.DebuggerNonUserCode()> _
        Protected Overrides Sub Dispose(ByVal disposing As Boolean)
            Try
                If disposing Then
                    If components IsNot Nothing Then components.Dispose()
                    ds.Dispose()
                    UnRegisterHandlers()
                End If
            Finally
                MyBase.Dispose(disposing)
            End Try
        End Sub

        'Required by the Windows Form Designer
        Private components As System.ComponentModel.IContainer

        'NOTE: The following procedure is required by the Windows Form Designer
        'It can be modified using the Windows Form Designer.  
        'Do not modify it using the code editor.
        <System.Diagnostics.DebuggerStepThrough()> _
        Private Sub InitializeComponent()
            Me.SimpleButton1 = New DevExpress.XtraEditors.SimpleButton
            Me.LabelControl1 = New DevExpress.XtraEditors.LabelControl
            Me.LastDeposit = New DevExpress.XtraEditors.LabelControl
            Me.GridControl1 = New DevExpress.XtraGrid.GridControl
            Me.GridView1 = New DevExpress.XtraGrid.Views.Grid.GridView
            Me.GridColumn_deposit_amount = New DevExpress.XtraGrid.Columns.GridColumn
            Me.GridColumn_deposit_amount.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.True
            Me.GridColumn_deposit_date = New DevExpress.XtraGrid.Columns.GridColumn
            Me.GridColumn_deposit_date.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.True
            CType(Me.GridControl1, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.GridView1, System.ComponentModel.ISupportInitialize).BeginInit()
            Me.SuspendLayout()
            '
            'SimpleButton1
            '
            Me.SimpleButton1.Location = New System.Drawing.Point(0, 0)
            Me.SimpleButton1.Name = "SimpleButton1"
            Me.SimpleButton1.Size = New System.Drawing.Size(75, 23)
            Me.SimpleButton1.TabIndex = 1
            Me.SimpleButton1.Text = "Deposits..."
            '
            'LabelControl1
            '
            Me.LabelControl1.Location = New System.Drawing.Point(3, 29)
            Me.LabelControl1.Name = "LabelControl1"
            Me.LabelControl1.Size = New System.Drawing.Size(20, 13)
            Me.LabelControl1.TabIndex = 2
            Me.LabelControl1.Text = "Last"
            '
            'LastDeposit
            '
            Me.LastDeposit.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                        Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
            Me.LastDeposit.Appearance.Options.UseTextOptions = True
            Me.LastDeposit.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
            Me.LastDeposit.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None
            Me.LastDeposit.Location = New System.Drawing.Point(29, 29)
            Me.LastDeposit.Name = "LastDeposit"
            Me.LastDeposit.Size = New System.Drawing.Size(116, 13)
            Me.LastDeposit.TabIndex = 3
            '
            'GridControl1
            '
            Me.GridControl1.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                        Or System.Windows.Forms.AnchorStyles.Left) _
                        Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
            Me.GridControl1.EmbeddedNavigator.Name = ""
            Me.GridControl1.Location = New System.Drawing.Point(0, 51)
            Me.GridControl1.MainView = Me.GridView1
            Me.GridControl1.Name = "GridControl1"
            Me.GridControl1.Size = New System.Drawing.Size(148, 50)
            Me.GridControl1.TabIndex = 4
            Me.GridControl1.ViewCollection.AddRange(New DevExpress.XtraGrid.Views.Base.BaseView() {Me.GridView1})
            '
            'GridView1
            '
            Me.GridView1.Columns.AddRange(New DevExpress.XtraGrid.Columns.GridColumn() {Me.GridColumn_deposit_amount, Me.GridColumn_deposit_date})
            Me.GridView1.GridControl = Me.GridControl1
            Me.GridView1.Name = "GridView1"
            Me.GridView1.OptionsBehavior.AutoPopulateColumns = False
            Me.GridView1.OptionsBehavior.AutoSelectAllInEditor = False
            Me.GridView1.OptionsBehavior.AutoUpdateTotalSummary = False
            Me.GridView1.OptionsBehavior.Editable = False
            Me.GridView1.OptionsCustomization.AllowColumnMoving = False
            Me.GridView1.OptionsCustomization.AllowFilter = False
            Me.GridView1.OptionsCustomization.AllowGroup = False
            Me.GridView1.OptionsCustomization.AllowSort = False
            Me.GridView1.OptionsDetail.AllowZoomDetail = False
            Me.GridView1.OptionsDetail.EnableMasterViewMode = False
            Me.GridView1.OptionsDetail.ShowDetailTabs = False
            Me.GridView1.OptionsDetail.SmartDetailExpand = False
            Me.GridView1.OptionsFilter.AllowColumnMRUFilterList = False
            Me.GridView1.OptionsFilter.AllowFilterEditor = False
            Me.GridView1.OptionsFilter.AllowMRUFilterList = False
            Me.GridView1.OptionsHint.ShowCellHints = False
            Me.GridView1.OptionsHint.ShowColumnHeaderHints = False
            Me.GridView1.OptionsLayout.StoreDataSettings = False
            Me.GridView1.OptionsLayout.StoreVisualOptions = False
            Me.GridView1.OptionsMenu.EnableColumnMenu = False
            Me.GridView1.OptionsMenu.EnableFooterMenu = False
            Me.GridView1.OptionsMenu.EnableGroupPanelMenu = False
            Me.GridView1.OptionsNavigation.AutoMoveRowFocus = False
            Me.GridView1.OptionsSelection.EnableAppearanceFocusedCell = False
            Me.GridView1.OptionsSelection.EnableAppearanceFocusedRow = False
            Me.GridView1.OptionsSelection.EnableAppearanceHideSelection = False
            Me.GridView1.OptionsSelection.UseIndicatorForSelection = False
            Me.GridView1.OptionsView.ShowColumnHeaders = False
            Me.GridView1.OptionsView.ShowDetailButtons = False
            Me.GridView1.OptionsView.ShowGroupPanel = False
            Me.GridView1.OptionsView.ShowHorizontalLines = DevExpress.Utils.DefaultBoolean.True
            Me.GridView1.OptionsView.ShowIndicator = False
            Me.GridView1.OptionsView.ShowPreviewRowLines = DevExpress.Utils.DefaultBoolean.True
            Me.GridView1.OptionsView.ShowVerticalLines = DevExpress.Utils.DefaultBoolean.False
            '
            'GridColumn_deposit_amount
            '
            Me.GridColumn_deposit_amount.Caption = "Amount"
            Me.GridColumn_deposit_amount.CustomizationCaption = "Deposit Amount"
            Me.GridColumn_deposit_amount.DisplayFormat.FormatString = "c2"
            Me.GridColumn_deposit_amount.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
            Me.GridColumn_deposit_amount.FieldName = "deposit_amount"
            Me.GridColumn_deposit_amount.Name = "GridColumn_deposit_amount"
            Me.GridColumn_deposit_amount.Visible = True
            Me.GridColumn_deposit_amount.VisibleIndex = 0
            '
            'GridColumn_deposit_date
            '
            Me.GridColumn_deposit_date.Caption = "Date"
            Me.GridColumn_deposit_date.CustomizationCaption = "Deposit Date"
            Me.GridColumn_deposit_date.DisplayFormat.FormatString = "d"
            Me.GridColumn_deposit_date.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime
            Me.GridColumn_deposit_date.FieldName = "deposit_date"
            Me.GridColumn_deposit_date.Name = "GridColumn_deposit_date"
            Me.GridColumn_deposit_date.Visible = True
            Me.GridColumn_deposit_date.VisibleIndex = 1
            '
            'DepositsControl
            '
            Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
            Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
            Me.Controls.Add(Me.GridControl1)
            Me.Controls.Add(Me.LastDeposit)
            Me.Controls.Add(Me.LabelControl1)
            Me.Controls.Add(Me.SimpleButton1)
            Me.MaximumSize = New System.Drawing.Size(148, 101)
            Me.Name = "DepositsControl"
            Me.Size = New System.Drawing.Size(148, 101)
            CType(Me.GridControl1, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.GridView1, System.ComponentModel.ISupportInitialize).EndInit()
            Me.ResumeLayout(False)
            Me.PerformLayout()
        End Sub

        Friend WithEvents SimpleButton1 As DevExpress.XtraEditors.SimpleButton
        Friend WithEvents LabelControl1 As DevExpress.XtraEditors.LabelControl
        Friend WithEvents LastDeposit As DevExpress.XtraEditors.LabelControl
        Friend WithEvents GridControl1 As DevExpress.XtraGrid.GridControl
        Friend WithEvents GridView1 As DevExpress.XtraGrid.Views.Grid.GridView
        Friend WithEvents GridColumn_deposit_amount As DevExpress.XtraGrid.Columns.GridColumn
        Friend WithEvents GridColumn_deposit_date As DevExpress.XtraGrid.Columns.GridColumn
    End Class
End Namespace
