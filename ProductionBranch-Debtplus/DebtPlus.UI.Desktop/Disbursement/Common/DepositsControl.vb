#Region "Copyright 2000-2012 DebtPlus, L.L.C."
'{*******************************************************************}
'{                                                                   }
'{       DebtPlus Debt Management System                             }
'{                                                                   }
'{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
'{       ALL RIGHTS RESERVED                                         }
'{                                                                   }
'{   The entire contents of this file is protected by U.S. and       }
'{   International Copyright Laws. Unauthorized reproduction,        }
'{   reverse-engineering, and distribution of all or any portion of  }
'{   the code contained in this file is strictly prohibited and may  }
'{   result in severe civil and criminal penalties and will be       }
'{   prosecuted to the maximum extent possible under the law.        }
'{                                                                   }
'{   RESTRICTIONS                                                    }
'{                                                                   }
'{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
'{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
'{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
'{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
'{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
'{                                                                   }
'{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
'{   ADDITIONAL RESTRICTIONS.                                        }
'{                                                                   }
'{*******************************************************************}

#End Region
Imports System.Data.SqlClient
Imports System.Windows.Forms

Namespace Disbursement.Common
    Friend Class DepositsControl
        Private ClientId As Int32 = -1
        Private ds As New DataSet("ds")

        Protected Property context As Disbursement.Common.IDisbursementContext
            Get
                Return DirectCast(ParentForm, Disbursement.Common.ISupportContext).Context
            End Get
            Set(value As Disbursement.Common.IDisbursementContext)
                Throw New NotImplementedException()
            End Set
        End Property

        Public Sub New()
            MyBase.New()
            InitializeComponent()
            RegisterHandlers()
        End Sub

        Private Sub RegisterHandlers()
            AddHandler SimpleButton1.Click, AddressOf SimpleButton1_Click
        End Sub

        Private Sub UnRegisterHandlers()
            RemoveHandler SimpleButton1.Click, AddressOf SimpleButton1_Click
        End Sub

        Friend Sub DisplayInformation(ByVal ClientId As Int32)

            ' Save the client should the user press the deposits button
            Me.ClientId = ClientId
            ds.Clear()

            ' Load the list control with the amount and date of the deposits
            Dim current_cursor As Cursor = Cursor.Current
            Dim cn As SqlConnection = New SqlConnection(DebtPlus.LINQ.SQLInfoClass.getDefault().ConnectionString)
            Dim rd As SqlDataReader = Nothing
            Try
                Cursor.Current = Cursors.WaitCursor
                cn.Open()
                Using cmd As SqlCommand = New SqlCommand
                    With cmd
                        .Connection = cn
                        .CommandText = "SELECT deposit_amount, deposit_date FROM client_deposits WITH (NOLOCK) WHERE client = @client"
                        .CommandType = CommandType.Text
                        .Parameters.Add("@client", SqlDbType.Int).Value = ClientId
                    End With
                    Using da As New SqlDataAdapter(cmd)
                        da.Fill(ds, "client_deposits")
                    End Using
                End Using

                With GridControl1
                    .DataSource = ds.Tables("client_deposits").DefaultView
                    .RefreshDataSource()
                End With

                ' Read the last deposit date and amount from the client
                Dim LastDepositAmount As Decimal = 0D
                Dim LastDepositDate As Date = Date.MinValue
                Using cmd As SqlCommand = New SqlCommand
                    With cmd
                        .Connection = cn
                        .CommandType = CommandType.Text
                        .CommandText = "SELECT last_deposit_date, last_deposit_amount FROM clients WITH (NOLOCK) WHERE client = @client"
                        .Parameters.Add("@client", SqlDbType.Int).Value = ClientId
                        rd = .ExecuteReader(CommandBehavior.CloseConnection Or CommandBehavior.SingleRow)
                    End With
                End Using

                If rd.Read Then
                    If Not rd.IsDBNull(0) Then LastDepositDate = rd.GetDateTime(0)
                    If Not rd.IsDBNull(1) Then LastDepositAmount = Convert.ToDecimal(rd.GetValue(1))
                End If

                ' Format the date and the amount fields
                If LastDepositDate <> Date.MinValue Then LastDeposit.Text = String.Format("{0:c2} {1:d}", LastDepositAmount, LastDepositDate)

            Catch ex As SqlException
                DebtPlus.UI.Common.ErrorHandling.HandleErrors(ex, "Error reading client deposits")

            Finally
                If rd IsNot Nothing Then rd.Dispose()
                If cn IsNot Nothing Then cn.Dispose()

                Cursor.Current = current_cursor
            End Try
        End Sub

        Private Sub SimpleButton1_Click(ByVal sender As Object, ByVal e As EventArgs)
            Using frm As New AdditionalTransactions(Context, ClientId)
                frm.ShowDialog()
            End Using
        End Sub
    End Class
End Namespace