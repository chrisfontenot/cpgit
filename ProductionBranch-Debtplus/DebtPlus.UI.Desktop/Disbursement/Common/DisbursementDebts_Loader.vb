Imports System
Imports System.Data
Imports System.Reflection

Namespace Disbursement.Common

    '     Debts for the disbursement operation come from the disbursement
    '     tables and not the debts table. As such, they don't have as many
    '     fields as the normal debts and some of the fields that are read-only
    '     on the normal debts are read/write here. But, these debts need to be
    '     proratable as well as able to calculate the monthly fees.

    Partial Class DisbursementDebtRecord
        Implements DebtPlus.Interfaces.Debt.IDebtRecordLoad

        ''' <summary>
        ''' Load the record
        ''' </summary>
        ''' <param name="rdr"></param>
        Public Sub LoadRecord(RecordType As System.Type, ByRef rdr As IDataReader) Implements Interfaces.Debt.IDebtRecordLoad.LoadRecord
            For indx As Int32 = rdr.FieldCount - 1 To 0 Step -1
                If Not rdr.IsDBNull(indx) Then
                    ProcessField(RecordType, rdr.GetName(indx), rdr.GetValue(indx))
                End If
            Next
        End Sub

        ''' <summary>
        ''' Load the record
        ''' </summary>
        ''' <param name="row"></param>
        Public Sub LoadRecord(RecordType As System.Type, ByRef row As DataRow) Implements Interfaces.Debt.IDebtRecordLoad.LoadRecord
            Dim tbl As DataTable = row.Table
            For Each col As DataColumn In tbl.Columns
                If Not row.IsNull(col) Then
                    ProcessField(RecordType, col.ColumnName, row(col))
                End If
            Next
        End Sub

        ''' <summary>
        ''' Set the field value in the debt record to the passed parameter.
        ''' </summary>
        ''' <param name="RecordType"></param>
        ''' <param name="FieldName"></param>
        ''' <param name="objValue"></param>
        Private Sub ProcessField(RecordType As System.Type, FieldName As String, objValue As Object)
            Dim properties As PropertyInfo() = RecordType.GetProperties()

            ' Process the properties to set the current field to its value
            For Each propertyField As PropertyInfo In properties

                Dim propertyAttributeList As Object() = propertyField.GetCustomAttributes(GetType(DebtPlus.Svc.Debt.DatabaseFieldAttribute), True)
                If propertyAttributeList IsNot Nothing AndAlso propertyAttributeList.Length = 1 Then

                    Dim customAttr As DebtPlus.Svc.Debt.DatabaseFieldAttribute = TryCast(propertyAttributeList(0), DebtPlus.Svc.Debt.DatabaseFieldAttribute)
                    If customAttr IsNot Nothing Then
                        Dim propertyFieldName As String = customAttr.ColumnName
                        Dim TypeOfField As DebtPlus.Svc.Debt.DatabaseFieldAttribute.FieldType = customAttr.TypeOfField

                        ' Per SQL, ignore the case of the column name. If the database is case sensitive, we need to change this.
                        If String.Compare(FieldName, propertyFieldName, True, System.Globalization.CultureInfo.InvariantCulture) = 0 Then

                            ' If the item is not an object then convert the data to the appropriate type
                            Select Case TypeOfField
                                Case DebtPlus.Svc.Debt.DatabaseFieldAttribute.FieldType.TypeObject
                                    Exit Select

                                Case DebtPlus.Svc.Debt.DatabaseFieldAttribute.FieldType.TypeInt32
                                    objValue = Convert.ToInt32(objValue)
                                    Exit Select

                                Case DebtPlus.Svc.Debt.DatabaseFieldAttribute.FieldType.TypeBoolean
                                    objValue = Convert.ToBoolean(objValue)
                                    Exit Select

                                Case DebtPlus.Svc.Debt.DatabaseFieldAttribute.FieldType.TypeDate
                                    objValue = Convert.ToDateTime(objValue)
                                    Exit Select

                                Case DebtPlus.Svc.Debt.DatabaseFieldAttribute.FieldType.TypeDecimal
                                    objValue = Convert.ToDecimal(objValue)
                                    Exit Select

                                Case DebtPlus.Svc.Debt.DatabaseFieldAttribute.FieldType.TypeDouble
                                    objValue = Convert.ToDouble(objValue)
                                    Exit Select

                                Case DebtPlus.Svc.Debt.DatabaseFieldAttribute.FieldType.TypeString
                                    objValue = Convert.ToString(objValue)
                                    Exit Select

                                Case Else
                                    Exit Select
                            End Select

                            ' Set the propertyField to the current column value
                            propertyField.SetValue(Me, objValue, Nothing)
                            Exit For
                        End If
                    End If
                End If
            Next
        End Sub
    End Class
End Namespace
