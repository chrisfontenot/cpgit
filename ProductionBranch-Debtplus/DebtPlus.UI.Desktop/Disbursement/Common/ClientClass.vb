#Region "Copyright 2000-2012 DebtPlus, L.L.C."
'{*******************************************************************}
'{                                                                   }
'{       DebtPlus Debt Management System                             }
'{                                                                   }
'{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
'{       ALL RIGHTS RESERVED                                         }
'{                                                                   }
'{   The entire contents of this file is protected by U.S. and       }
'{   International Copyright Laws. Unauthorized reproduction,        }
'{   reverse-engineering, and distribution of all or any portion of  }
'{   the code contained in this file is strictly prohibited and may  }
'{   result in severe civil and criminal penalties and will be       }
'{   prosecuted to the maximum extent possible under the law.        }
'{                                                                   }
'{   RESTRICTIONS                                                    }
'{                                                                   }
'{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
'{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
'{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
'{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
'{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
'{                                                                   }
'{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
'{   ADDITIONAL RESTRICTIONS.                                        }
'{                                                                   }
'{*******************************************************************}

#End Region

Option Compare Binary
Option Explicit On
Option Strict On

Imports DebtPlus.Interfaces.Debt
Imports DebtPlus.Svc.Common
Imports DebtPlus.Events
Imports System.Data.SqlClient
Imports System.Windows.Forms

Namespace Disbursement.Common
    Friend Class ClientClass
        Implements IDisposable
        Implements ISupportContext

        Public Property Context As IDisbursementContext Implements ISupportContext.Context

        ' Local storage for the client information
        Protected Friend LastExaminedClient As DataRow
        Protected Friend ClientROW As DataRow
        Protected Friend ClientDS As New DataSet("ClientDS")
        Protected Friend DebtList As DisbursementDebtList
        Protected CurrentForm As Examine_Form

        ''' <summary>
        ''' Override fee calculation so that when debts are initially loaded the fees are
        ''' calculated for percentage based fees only.
        ''' </summary>
        Public Class FeeCalculation
            Inherits MonthlyFeeCalculation

            Public Sub New()
                MyBase.New()
            End Sub

            ''' <summary>
            ''' Use the debit_amt for the scheduled payment when we are working a disbursement batch
            ''' </summary>
            Protected Overrides Sub OnQueryValue(ByVal e As DebtPlus.Events.ParameterValueEventArgs)

                ' Look for a debit_amt figure if we want the scheduled pay.
                If e.Name = DebtPlus.Events.ParameterValueEventArgs.name_SchedPayment Then

                    ' If so, use the debit_amt instead. This is the normal amount being paid.
                    Dim NewValue As Object = GetValue("debit_amt")
                    If NewValue IsNot Nothing AndAlso NewValue IsNot DBNull.Value Then
                        Dim DecimalValue As Decimal = Convert.ToDecimal(NewValue)
                        If DecimalValue > 0D Then
                            e.Value = DecimalValue
                            Return
                        End If
                    End If
                End If

                ' Everything else is simply passed along.
                MyBase.OnQueryValue(e)
            End Sub
        End Class

        ''' <summary>
        ''' Pointer to the routine to calculate the monthly client fee
        ''' </summary>
        Private WithEvents privateFeeInfo As FeeCalculation

        Public ReadOnly Property FeeInfo() As FeeCalculation
            Get
                If privateFeeInfo Is Nothing Then
                    privateFeeInfo = New FeeCalculation()
                End If
                Return privateFeeInfo
            End Get
        End Property

        ''' <summary>
        ''' Supress the update events for the process
        ''' </summary>
        Private UpdateCount As Int32

        Public Sub BeginUpdate()
            UpdateCount += 1
        End Sub

        ''' <summary>
        ''' Complete the update operation
        ''' </summary>
        Public Sub EndUpdate()
            If UpdateCount < 2 Then
                UpdateCount = 0
            Else
                UpdateCount -= 1
            End If
        End Sub

        ''' <summary>
        ''' Return TRUE if the update flag is set
        ''' </summary>
        Public ReadOnly Property InUpdate() As Boolean
            Get
                Return UpdateCount > 0
            End Get
        End Property

        ''' <summary>
        ''' Create an instance of our class
        ''' </summary>
        Private Sub New()
            MyBase.new()
        End Sub

        Public Sub New(ByVal Context As IDisbursementContext)
            MyClass.New()
            Me.Context = Context
        End Sub

        Public Sub New(ByVal Context As IDisbursementContext, ByVal row As DataRow, ByRef DebtList As DisbursementDebtList)
            MyClass.New(Context)

            ' Save the parameters for processing later
            ClientROW = row

            ' Save the list of debts that we are to process
            Me.DebtList = DebtList
        End Sub

        ''' <summary>
        ''' Pointer to the previous client
        ''' </summary>
        Public Property Previous() As ClientClass

        ''' <summary>
        ''' Determine the disbursement register
        ''' </summary>
        Protected Friend ReadOnly Property disbursement_register() As Int32
            Get
                Return Convert.ToInt32(ClientROW("disbursement_register"))
            End Get
        End Property

        ''' <summary>
        ''' Determine the client ID
        ''' </summary>
        Protected Friend ReadOnly Property ClientId() As Int32
            Get
                Return Convert.ToInt32(ClientROW("client"))
            End Get
        End Property

        ''' <summary>
        ''' Determine the number of disbursement notes for the client
        ''' </summary>
        Protected Friend ReadOnly Property note_count() As Int32
            Get
                Return Convert.ToInt32(ClientROW("note_count"))
            End Get
        End Property

        ''' <summary>
        ''' Determine the trust funds available for the client
        ''' </summary>
        Protected Friend Overridable ReadOnly Property held_in_trust() As Decimal
            Get
                Dim answer As Decimal = Convert.ToDecimal(ClientROW("held_in_trust"))
                Return answer
            End Get
        End Property

        ''' <summary>
        ''' Determine the trust funds on hold for the client
        ''' </summary>
        Protected Friend ReadOnly Property trust_funds_on_hold() As Decimal
            Get
                Dim answer As Decimal = Convert.ToDecimal(ClientROW("reserved_in_trust"))
                Return answer
            End Get
        End Property

        ''' <summary>
        ''' Process the SKIP button event
        ''' </summary>
        Public Overridable Sub Do_Skip()
            If CurrentForm IsNot Nothing Then
                CurrentForm.DialogResult = DialogResult.Cancel
            End If
        End Sub

        ''' <summary>
        ''' Process the NO PAY button event
        ''' </summary>
        Public Overridable Sub Do_NoPay()

            ' Reset any pending information on the prior disbursement of this client
            ' This will reset the amounts paid to each of the creditors in the database.
            Dim RetryStatus As DialogResult

            Do
                RetryStatus = DialogResult.Ignore

                Dim cn As SqlConnection = New SqlConnection(DebtPlus.LINQ.SQLInfoClass.getDefault().ConnectionString)
                Dim txn As SqlTransaction = Nothing
                Dim current_cursor As Cursor = Cursor.Current
                Try
                    Cursor.Current = Cursors.WaitCursor
                    cn.Open()
                    txn = cn.BeginTransaction(IsolationLevel.ReadUncommitted)

                    ' First, remove any payments made by this client
                    Using cmd As SqlCommand = New SqlCommand
                        With cmd
                            .Connection = cn
                            .Transaction = txn
                            .CommandText = "xpr_disbursement_transaction_start"
                            .CommandType = CommandType.StoredProcedure
                            With .Parameters
                                .Add("@disbursement_register", SqlDbType.Int).Value = disbursement_register
                                .Add("@client", SqlDbType.Int).Value = ClientId
                            End With
                            .ExecuteNonQuery()
                        End With
                    End Using

                    Using cmd As SqlCommand = New SqlCommand
                        With cmd
                            .Connection = cn
                            .Transaction = txn
                            .CommandText = "xpr_disbursement_transaction_end"
                            .CommandType = CommandType.StoredProcedure
                            With .Parameters
                                .Add("@disbursement_register", SqlDbType.Int).Value = disbursement_register
                                .Add("@client", SqlDbType.Int).Value = ClientId
                                .Add("@debit_amt", SqlDbType.Decimal).Value = 0D
                            End With
                            .ExecuteNonQuery()
                        End With
                    End Using

                    ' Commit the changes to the database now
                    txn.Commit()
                    txn.Dispose()
                    txn = Nothing

                    ' Terminate the form normally with OK status
                    If CurrentForm IsNot Nothing Then
                        CurrentForm.DialogResult = DialogResult.OK
                    End If

                Catch ex As SqlException
                    Const ErrorTitle As String = "Error doing the NO-PAY operation"
                    RetryStatus = DebtPlus.Data.Forms.MessageBox.Show(ex.Message, ErrorTitle, MessageBoxButtons.RetryCancel, MessageBoxIcon.Error, MessageBoxDefaultButton.Button1)

                Finally
                    If txn IsNot Nothing Then
                        Try
                            txn.Rollback()
                        Catch exRollback As Exception
                        End Try
                        txn.Dispose()
                        txn = Nothing
                    End If

                    If cn IsNot Nothing Then cn.Dispose()

                    Cursor.Current = current_cursor
                End Try
            Loop Until RetryStatus <> DialogResult.Retry
        End Sub

        ''' <summary>
        ''' Process the PAY button event
        ''' </summary>
        Public Overridable Function Do_Pay() As Decimal
            Dim debit_amt As Decimal

            ' Reset any pending information on the prior disbursement of this client
            ' This will reset the amounts paid to each of the creditors in the database.
            Dim RetryStatus As DialogResult

            Do
                debit_amt = 0D
                RetryStatus = DialogResult.Ignore

                Dim cn As SqlConnection = New SqlConnection(DebtPlus.LINQ.SQLInfoClass.getDefault().ConnectionString)
                Dim txn As SqlTransaction = Nothing
                Dim current_cursor As Cursor = Cursor.Current

                Try
                    Cursor.Current = Cursors.WaitCursor
                    cn.Open()
                    txn = cn.BeginTransaction(IsolationLevel.ReadUncommitted)

                    ' First, remove any payments made by this client
                    Using cmd As SqlCommand = New SqlCommand
                        With cmd
                            .Connection = cn
                            .Transaction = txn
                            .CommandText = "xpr_disbursement_transaction_start"
                            .CommandType = CommandType.StoredProcedure
                            With .Parameters
                                .Add("@disbursement_register", SqlDbType.Int).Value = disbursement_register
                                .Add("@client", SqlDbType.Int).Value = ClientId
                            End With
                            .ExecuteNonQuery()
                        End With
                    End Using

                    ' Process the payments made by the debt list
                    For Each DebtInfo As DisbursementDebtRecord In DebtList
                        With DebtInfo

                            ' If an amount is paid, then pay the debt.
                            If .debit_amt > 0D Then
                                debit_amt += .debit_amt

                                ' Pay the debt.
                                Using cmd As SqlCommand = New SqlCommand
                                    With cmd
                                        .Connection = cn
                                        .Transaction = txn
                                        .CommandText = "xpr_disbursement_transaction_trans"
                                        .CommandType = CommandType.StoredProcedure
                                        With .Parameters
                                            .Add("@disbursement_register", SqlDbType.Int).Value = disbursement_register
                                            .Add("@client_creditor", SqlDbType.Int).Value = DebtInfo.client_creditor
                                            .Add("@debit_amt", SqlDbType.Decimal).Value = DebtInfo.debit_amt
                                        End With
                                        .ExecuteNonQuery()
                                    End With
                                End Using
                            End If
                        End With
                    Next

                    ' Complete the payments on this client
                    Using cmd As SqlCommand = New SqlCommand
                        With cmd
                            .Connection = cn
                            .Transaction = txn
                            .CommandText = "xpr_disbursement_transaction_end"
                            .CommandType = CommandType.StoredProcedure
                            With .Parameters
                                .Add("@disbursement_register", SqlDbType.Int).Value = disbursement_register
                                .Add("@client", SqlDbType.Int).Value = ClientId
                                .Add("@debit_amt", SqlDbType.Decimal).Value = debit_amt
                            End With
                            .ExecuteNonQuery()
                        End With
                    End Using

                    ' Commit the changes to the database now
                    txn.Commit()
                    txn.Dispose()
                    txn = Nothing

                    ' Terminate the form normally with OK status
                    If CurrentForm IsNot Nothing Then
                        CurrentForm.DialogResult = DialogResult.OK
                    End If

                Catch ex As SqlException
                    Const ErrorTitle As String = "Error doing the PAY operation"
                    RetryStatus = DebtPlus.Data.Forms.MessageBox.Show(ex.Message, ErrorTitle, MessageBoxButtons.RetryCancel, MessageBoxIcon.Error, MessageBoxDefaultButton.Button1)

                Finally
                    If txn IsNot Nothing Then
                        Try
                            txn.Rollback()
                        Catch exRollback As Exception
                        End Try
                        txn.Dispose()
                        txn = Nothing
                    End If

                    If cn IsNot Nothing Then cn.Dispose()
                    Cursor.Current = current_cursor
                End Try
            Loop Until RetryStatus <> DialogResult.Retry

            ' Tell the caller the total dollar amount paid
            Return debit_amt
        End Function

        ''' <summary>
        ''' Process the QUIT button event
        ''' </summary>
        Public Overridable Sub Do_Quit()
            If DebtPlus.Data.Forms.MessageBox.Show(String.Format("WARNING: This will terminate the disbursement and stop all further processing for clients.{0}The current client will NOT be paid.{0}You must reexecute the disbursement processing to return to this disbursement.{0}{0}Are you sure that you wish to stop processing immediately?", Environment.NewLine), "Are you sure?", MessageBoxButtons.YesNo, MessageBoxIcon.Hand, MessageBoxDefaultButton.Button2) = DialogResult.Yes Then

                ' Set the top most "let's bug out" flag
                Context.MasterQuit = True

                ' Force the dialog to terminate with an abort status if indicated
                If CurrentForm IsNot Nothing Then
                    CurrentForm.DialogResult = DialogResult.Abort
                End If
            End If
        End Sub

        ''' <summary>
        ''' Process the PRORATE button event
        ''' </summary>
        Public Overridable Sub Do_Prorate()
            Prorate(held_in_trust)
        End Sub

        ''' <summary>
        ''' Process the EXAMINE button event
        ''' </summary>
        Public Overridable Function Do_Examine() As DialogResult
            Using CurrentForm As New Examine_Form(Context, Me)
                Return CurrentForm.ShowDialog()
            End Using
        End Function

        ''' <summary>
        ''' Process the EXAMINE PREVIOUS button event
        ''' </summary>
        Public Overridable Sub Do_ExaminePrevious()
            If (Not Context.MasterQuit) AndAlso (Previous IsNot Nothing) Then
                If Previous.Do_Examine() = DialogResult.Abort AndAlso (CurrentForm IsNot Nothing) Then
                    CurrentForm.DialogResult = DialogResult.Abort
                End If
            End If
        End Sub

        ''' <summary>
        ''' Determine if we can show the previous client information
        ''' </summary>
        Public ReadOnly Property CanExaminePrevious() As Boolean
            Get
                Return (Previous IsNot Nothing) AndAlso (Not Context.MasterQuit)
            End Get
        End Property

        ''' <summary>
        ''' Determine if we can show the client notes
        ''' </summary>
        Public ReadOnly Property CanShowNotes() As Boolean
            Get
                'Return note_count > 0  02/10/2010 to always show notes, we say "yes".
                Return True
            End Get
        End Property

        ''' <summary>
        ''' Determine if we can do the prorate function
        ''' </summary>
        Public ReadOnly Property CanProrate() As Boolean
            Get
                Return True                 ' to always allow prorate, say yes.
            End Get
        End Property

        ''' <summary>
        ''' Do the prorate operation on the debts list
        ''' </summary>
        Private Sub Prorate(ByVal TrustBalance As Decimal)
            Using cls As New DisbursementProrate(CType(DebtList, IProratableList))
                cls.Prorate(TrustBalance)
            End Using
        End Sub

        ''' <summary>
        ''' Recalculate the monthly fee amount from the disbursement information
        ''' </summary>
        Public Sub RecalulateMonthlyFee()

            ' Recalculate the fee amount for the debt based upon the payment information
            Dim FeeDebt As IProratable = TryCast(DebtList.MonthlyFeeDebt, IProratable)
            If FeeDebt IsNot Nothing Then

                ' Find the new fee figure for the debt and update it.
                Try
                    AddHandler FeeInfo.QueryValue, AddressOf FeeInfo_QueryValue
                    Dim FeeAmount As Decimal = FeeInfo.FeeAmount()
                    If FeeDebt.debit_amt <> FeeAmount Then
                        FeeDebt.debit_amt = FeeAmount
                    End If

                Finally
                    RemoveHandler FeeInfo.QueryValue, AddressOf FeeInfo_QueryValue
                End Try
            End If
        End Sub

        ''' <summary>
        ''' Hook into the fee query process
        ''' </summary>
        Public Overridable Sub FeeInfo_QueryValue(ByVal Sender As Object, ByVal e As DebtPlus.Events.ParameterValueEventArgs)

            ' We can handle the ClientId as a request here.
            If String.Compare(e.Name, DebtPlus.Events.ParameterValueEventArgs.name_client, True) = 0 Then
                e.Value = ClientId
                Return
            End If

            ' Others are simply passed up the chain
            e.Value = CType(DebtList, IFeeable).QueryFeeValue(e.Name)
        End Sub

        Private disposedValue As Boolean        ' To detect redundant calls

        ''' <summary>
        ''' Handle the dispose method
        ''' </summary>
        Protected Overridable Sub Dispose(ByVal disposing As Boolean)

            ' If we have not disposed of the storage then do so now.
            If Not disposedValue Then
                If disposing Then
                    ClientDS.Dispose()
                    DebtList.Clear()
                End If

                ' Release the storage pointers once the items have been removed
                Context = Nothing
                Previous = Nothing
                ClientROW = Nothing
                ClientDS = Nothing
                DebtList = Nothing
            End If
            disposedValue = True
        End Sub

        ''' <summary>
        ''' Dispose of the class
        ''' </summary>
        Public Sub Dispose() Implements IDisposable.Dispose
            Dispose(True)
            GC.SuppressFinalize(Me)
        End Sub

        Protected Overrides Sub Finalize()
            Try
                Dispose(False)
            Finally
                MyBase.Finalize()
            End Try
        End Sub
    End Class
End Namespace
