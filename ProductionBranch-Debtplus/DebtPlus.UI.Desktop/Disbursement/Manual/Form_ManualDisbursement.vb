#Region "Copyright 2000-2012 DebtPlus, L.L.C."
'{*******************************************************************}
'{                                                                   }
'{       DebtPlus Debt Management System                             }
'{                                                                   }
'{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
'{       ALL RIGHTS RESERVED                                         }
'{                                                                   }
'{   The entire contents of this file is protected by U.S. and       }
'{   International Copyright Laws. Unauthorized reproduction,        }
'{   reverse-engineering, and distribution of all or any portion of  }
'{   the code contained in this file is strictly prohibited and may  }
'{   result in severe civil and criminal penalties and will be       }
'{   prosecuted to the maximum extent possible under the law.        }
'{                                                                   }
'{   RESTRICTIONS                                                    }
'{                                                                   }
'{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
'{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
'{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
'{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
'{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
'{                                                                   }
'{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
'{   ADDITIONAL RESTRICTIONS.                                        }
'{                                                                   }
'{*******************************************************************}
#End Region
Imports DebtPlus.UI.Common

Namespace Disbursement.Manual
    Friend Class Form_ManualDisbursement

        ' Information set by the copy form and passed to the main entry form later.
        Friend Creditor As String = String.Empty
        Friend TrustRegister As System.Int32 = -1

        Private ctx As Context = Nothing

        Public Sub New()
            MyBase.New()
            InitializeComponent()
        End Sub

        ''' <summary>
        ''' Create an instance of our form
        ''' </summary>
        Public Sub New(ByVal ctx As Context)
            MyClass.New()
            Me.ctx = ctx

            AddHandler Main_Copy1.Cancelled, AddressOf Main_Copy1_Cancelled
            AddHandler Main_Copy1.Completed, AddressOf Main_Copy1_Completed
            AddHandler Load, AddressOf Form_ManualDisbursement_Load
            AddHandler Main_Entry1.Cancelled, AddressOf Main_Copy1_Cancelled
        End Sub

        ''' <summary>
        ''' Process the cancel event from the form
        ''' </summary>
        Private Sub Main_Copy1_Cancelled(ByVal sender As Object, ByVal e As System.EventArgs)
            Close()
        End Sub

        ''' <summary>
        ''' Process the OK event from the check copy screen
        ''' </summary>
        Private Sub Main_Copy1_Completed(ByVal sender As Object, ByVal e As System.EventArgs)

            ' Move the copy frame to the background
            Main_Copy1.SendToBack()
            Main_Copy1.Visible = False
            Main_Copy1.TabStop = False

            ' Make the main entry form go to the background
            Main_Entry1.TabStop = True
            Main_Entry1.Visible = True
            Main_Entry1.BringToFront()
            Main_Entry1.Focus()
            Main_Entry1.ReadForm()
        End Sub

        ''' <summary>
        ''' Display the copy information when we are started
        ''' </summary>
        Private Sub Form_ManualDisbursement_Load(ByVal sender As Object, ByVal e As System.EventArgs)

            ' Restore the location and size of the form
            MyBase.LoadPlacement("Disbursement.Manual")

            ' Make the main entry form go to the background
            Main_Entry1.SendToBack()
            Main_Entry1.Visible = False
            Main_Entry1.TabStop = False

            ' Move the copy frame to the foreground
            Main_Copy1.TabStop = True
            Main_Copy1.Visible = True
            Main_Copy1.BringToFront()
            Main_Copy1.Focus()
            Main_Copy1.ReadForm()
        End Sub
    End Class
End Namespace
