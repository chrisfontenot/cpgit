﻿Namespace Disbursement.Manual
    <Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
    Partial Class Form_ManualDisbursement
        Inherits DebtPlus.Data.Forms.DebtPlusForm

        'Form overrides dispose to clean up the component list.
        <System.Diagnostics.DebuggerNonUserCode()> _
        Protected Overrides Sub Dispose(ByVal disposing As Boolean)
            Try
                If disposing AndAlso components IsNot Nothing Then
                    components.Dispose()
                End If
            Finally
                MyBase.Dispose(disposing)
            End Try
        End Sub

        'Required by the Windows Form Designer
        Private components As System.ComponentModel.IContainer

        'NOTE: The following procedure is required by the Windows Form Designer
        'It can be modified using the Windows Form Designer.  
        'Do not modify it using the code editor.
        <System.Diagnostics.DebuggerStepThrough()> _
        Private Sub InitializeComponent()
            Me.Main_Entry1 = New DebtPlus.UI.Desktop.Disbursement.Manual.Controls.Main_Entry(ctx)
            Me.Main_Copy1 = New DebtPlus.UI.Desktop.Disbursement.Manual.Controls.Main_Copy(ctx)
            CType(Me.DxErrorProvider1, System.ComponentModel.ISupportInitialize).BeginInit()
            Me.SuspendLayout()
            '
            'Main_Entry1
            '
            Me.Main_Entry1.Dock = System.Windows.Forms.DockStyle.Fill
            Me.Main_Entry1.Location = New System.Drawing.Point(0, 0)
            Me.Main_Entry1.Name = "Main_Entry1"
            Me.Main_Entry1.Padding = New System.Windows.Forms.Padding(4)
            Me.Main_Entry1.Size = New System.Drawing.Size(598, 351)
            Me.Main_Entry1.TabIndex = 2
            '
            'Main_Copy1
            '
            Me.Main_Copy1.Dock = System.Windows.Forms.DockStyle.Fill
            Me.Main_Copy1.Location = New System.Drawing.Point(0, 0)
            Me.Main_Copy1.Name = "Main_Copy1"
            Me.Main_Copy1.Size = New System.Drawing.Size(598, 351)
            Me.Main_Copy1.TabIndex = 0
            '
            'Form_ManualDisbursement
            '
            Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
            Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
            Me.ClientSize = New System.Drawing.Size(598, 351)
            Me.Controls.Add(Me.Main_Entry1)
            Me.Controls.Add(Me.Main_Copy1)
            Me.LookAndFeel.UseDefaultLookAndFeel = True
            Me.Name = "Form_ManualDisbursement"
            Me.Text = "Manual Disbursement"
            CType(Me.DxErrorProvider1, System.ComponentModel.ISupportInitialize).EndInit()
            Me.ResumeLayout(False)

        End Sub
        Friend WithEvents Main_Copy1 As DebtPlus.UI.Desktop.Disbursement.Manual.Controls.Main_Copy
        Friend WithEvents Main_Entry1 As DebtPlus.UI.Desktop.Disbursement.Manual.Controls.Main_Entry
    End Class
End Namespace
