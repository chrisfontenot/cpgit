﻿Imports DebtPlus.UI.Client.Widgets.Controls

Namespace Disbursement.Manual.Controls
    <Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
    Partial Class Main_Entry
        Inherits BaseControl

        'UserControl overrides dispose to clean up the component list.
        <System.Diagnostics.DebuggerNonUserCode()> _
        Protected Overrides Sub Dispose(ByVal disposing As Boolean)
            Try
                If disposing AndAlso components IsNot Nothing Then
                    components.Dispose()
                End If
            Finally
                MyBase.Dispose(disposing)
            End Try
        End Sub

        'Required by the Windows Form Designer
        Private components As System.ComponentModel.IContainer

        'NOTE: The following procedure is required by the Windows Form Designer
        'It can be modified using the Windows Form Designer.  
        'Do not modify it using the code editor.
        <System.Diagnostics.DebuggerStepThrough()> _
        Private Sub InitializeComponent()
            Dim StyleFormatCondition1 As DevExpress.XtraGrid.StyleFormatCondition = New DevExpress.XtraGrid.StyleFormatCondition
            Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(Main_Entry))
            Dim SerializableAppearanceObject1 As DevExpress.Utils.SerializableAppearanceObject = New DevExpress.Utils.SerializableAppearanceObject
            Me.GridColumn_IsValid = New DevExpress.XtraGrid.Columns.GridColumn
            Me.GridColumn_void = New DevExpress.XtraGrid.Columns.GridColumn
            Me.GridControl1 = New DevExpress.XtraGrid.GridControl
            Me.GridView1 = New DevExpress.XtraGrid.Views.Grid.GridView
            Me.GridColumn_Client = New DevExpress.XtraGrid.Columns.GridColumn
            Me.RepositoryItemTextEdit_client = New DevExpress.XtraEditors.Repository.RepositoryItemTextEdit
            Me.GridColumn_Name = New DevExpress.XtraGrid.Columns.GridColumn
            Me.GridColumn_Debit_Amt = New DevExpress.XtraGrid.Columns.GridColumn
            Me.RepositoryItemCalcEdit_debit_amt = New DevExpress.XtraEditors.Repository.RepositoryItemCalcEdit
            Me.GridColumn_Contribution = New DevExpress.XtraGrid.Columns.GridColumn
            Me.GridColumn_account_number = New DevExpress.XtraGrid.Columns.GridColumn
            Me.GridColumn_Rate = New DevExpress.XtraGrid.Columns.GridColumn
            Me.GridColumn_ID = New DevExpress.XtraGrid.Columns.GridColumn
            Me.GridColumn_Deducted = New DevExpress.XtraGrid.Columns.GridColumn
            Me.GridColumn_Billed = New DevExpress.XtraGrid.Columns.GridColumn
            Me.GridColumn_Net_Amount = New DevExpress.XtraGrid.Columns.GridColumn
            Me.GridColumn_creditor_type = New DevExpress.XtraGrid.Columns.GridColumn
            Me.RepositoryItemLookUpEdit_creditor_type = New DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit
            Me.LookUpEdit_client_creditor = New DevExpress.XtraEditors.LookUpEdit
            Me.LayoutControl1 = New DevExpress.XtraLayout.LayoutControl
            Me.ClientID1 = New DebtPlus.UI.Client.Widgets.Controls.ClientID
            Me.LabelControl_client_name = New DevExpress.XtraEditors.LabelControl
            Me.SimpleButton_Post = New DevExpress.XtraEditors.SimpleButton
            Me.SimpleButton_add = New DevExpress.XtraEditors.SimpleButton
            Me.CalcEdit_amount = New DevExpress.XtraEditors.CalcEdit
            Me.SimpleButton_del = New DevExpress.XtraEditors.SimpleButton
            Me.ComboBoxEdit_reason = New DevExpress.XtraEditors.ComboBoxEdit
            Me.CalcEdit_rate = New DevExpress.XtraEditors.CalcEdit
            Me.ComboBoxEdit_disposition = New DevExpress.XtraEditors.ComboBoxEdit
            Me.LabelControl_creditor = New DevExpress.XtraEditors.LabelControl
            Me.LookupEdit_contribution = New DevExpress.XtraEditors.LookUpEdit
            Me.LayoutControlGroup1 = New DevExpress.XtraLayout.LayoutControlGroup
            Me.LayoutControlItem1 = New DevExpress.XtraLayout.LayoutControlItem
            Me.LayoutControlItem2 = New DevExpress.XtraLayout.LayoutControlItem
            Me.LayoutControlItem6 = New DevExpress.XtraLayout.LayoutControlItem
            Me.LayoutControlItem8 = New DevExpress.XtraLayout.LayoutControlItem
            Me.LayoutControlItem9 = New DevExpress.XtraLayout.LayoutControlItem
            Me.LayoutControlItem11 = New DevExpress.XtraLayout.LayoutControlItem
            Me.LayoutControlItem12 = New DevExpress.XtraLayout.LayoutControlItem
            Me.LayoutControlItem10 = New DevExpress.XtraLayout.LayoutControlItem
            Me.EmptySpaceItem1 = New DevExpress.XtraLayout.EmptySpaceItem
            Me.LayoutControlItem7 = New DevExpress.XtraLayout.LayoutControlItem
            Me.LayoutControlItem13 = New DevExpress.XtraLayout.LayoutControlItem
            Me.LayoutControlItem3 = New DevExpress.XtraLayout.LayoutControlItem
            Me.LayoutControlItem5 = New DevExpress.XtraLayout.LayoutControlItem
            Me.LayoutControlItem14 = New DevExpress.XtraLayout.LayoutControlItem
            CType(Me.GridControl1, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.GridView1, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.RepositoryItemTextEdit_client, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.RepositoryItemCalcEdit_debit_amt, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.RepositoryItemLookUpEdit_creditor_type, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.LookUpEdit_client_creditor.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.LayoutControl1, System.ComponentModel.ISupportInitialize).BeginInit()
            Me.LayoutControl1.SuspendLayout()
            CType(Me.ClientID1.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.CalcEdit_amount.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.ComboBoxEdit_reason.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.CalcEdit_rate.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.ComboBoxEdit_disposition.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.LookupEdit_contribution.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.LayoutControlGroup1, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.LayoutControlItem1, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.LayoutControlItem2, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.LayoutControlItem6, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.LayoutControlItem8, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.LayoutControlItem9, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.LayoutControlItem11, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.LayoutControlItem12, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.LayoutControlItem10, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.EmptySpaceItem1, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.LayoutControlItem7, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.LayoutControlItem13, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.LayoutControlItem3, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.LayoutControlItem5, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.LayoutControlItem14, System.ComponentModel.ISupportInitialize).BeginInit()
            Me.SuspendLayout()
            '
            'GridColumn_IsValid
            '
            Me.GridColumn_IsValid.AppearanceCell.Options.UseTextOptions = True
            Me.GridColumn_IsValid.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
            Me.GridColumn_IsValid.AppearanceHeader.Options.UseTextOptions = True
            Me.GridColumn_IsValid.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
            Me.GridColumn_IsValid.Caption = "Valid?"
            Me.GridColumn_IsValid.FieldName = "IsValid"
            Me.GridColumn_IsValid.Name = "GridColumn_IsValid"
            Me.GridColumn_IsValid.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.[True]
            '
            'GridColumn_void
            '
            Me.GridColumn_void.AppearanceCell.Options.UseTextOptions = True
            Me.GridColumn_void.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
            Me.GridColumn_void.AppearanceHeader.Options.UseTextOptions = True
            Me.GridColumn_void.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
            Me.GridColumn_void.Caption = "Deleted"
            Me.GridColumn_void.CustomizationCaption = "Item Deleted"
            Me.GridColumn_void.FieldName = "void"
            Me.GridColumn_void.Name = "GridColumn_void"
            Me.GridColumn_void.OptionsColumn.AllowEdit = False
            Me.GridColumn_void.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.[True]
            Me.GridColumn_void.OptionsFilter.AllowFilter = False
            '
            'GridControl1
            '
            Me.GridControl1.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                        Or System.Windows.Forms.AnchorStyles.Left) _
                        Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
            Me.GridControl1.Location = New System.Drawing.Point(12, 133)
            Me.GridControl1.MainView = Me.GridView1
            Me.GridControl1.Name = "GridControl1"
            Me.GridControl1.RepositoryItems.AddRange(New DevExpress.XtraEditors.Repository.RepositoryItem() {Me.RepositoryItemTextEdit_client, Me.RepositoryItemCalcEdit_debit_amt, Me.RepositoryItemLookUpEdit_creditor_type})
            Me.GridControl1.Size = New System.Drawing.Size(559, 248)
            Me.GridControl1.TabIndex = 19
            Me.GridControl1.ViewCollection.AddRange(New DevExpress.XtraGrid.Views.Base.BaseView() {Me.GridView1})
            '
            'GridView1
            '
            Me.GridView1.Appearance.ColumnFilterButton.BackColor = System.Drawing.Color.FromArgb(CType(CType(104, Byte), Int32), CType(CType(184, Byte), Int32), CType(CType(251, Byte), Int32))
            Me.GridView1.Appearance.ColumnFilterButton.BorderColor = System.Drawing.Color.FromArgb(CType(CType(104, Byte), Int32), CType(CType(184, Byte), Int32), CType(CType(251, Byte), Int32))
            Me.GridView1.Appearance.ColumnFilterButton.ForeColor = System.Drawing.Color.White
            Me.GridView1.Appearance.ColumnFilterButton.Options.UseBackColor = True
            Me.GridView1.Appearance.ColumnFilterButton.Options.UseBorderColor = True
            Me.GridView1.Appearance.ColumnFilterButton.Options.UseForeColor = True
            Me.GridView1.Appearance.ColumnFilterButtonActive.BackColor = System.Drawing.Color.FromArgb(CType(CType(170, Byte), Int32), CType(CType(216, Byte), Int32), CType(CType(254, Byte), Int32))
            Me.GridView1.Appearance.ColumnFilterButtonActive.BorderColor = System.Drawing.Color.FromArgb(CType(CType(170, Byte), Int32), CType(CType(216, Byte), Int32), CType(CType(254, Byte), Int32))
            Me.GridView1.Appearance.ColumnFilterButtonActive.ForeColor = System.Drawing.Color.Black
            Me.GridView1.Appearance.ColumnFilterButtonActive.Options.UseBackColor = True
            Me.GridView1.Appearance.ColumnFilterButtonActive.Options.UseBorderColor = True
            Me.GridView1.Appearance.ColumnFilterButtonActive.Options.UseForeColor = True
            Me.GridView1.Appearance.Empty.BackColor = System.Drawing.Color.FromArgb(CType(CType(236, Byte), Int32), CType(CType(246, Byte), Int32), CType(CType(255, Byte), Int32))
            Me.GridView1.Appearance.Empty.BackColor2 = System.Drawing.Color.White
            Me.GridView1.Appearance.Empty.Options.UseBackColor = True
            Me.GridView1.Appearance.EvenRow.BackColor = System.Drawing.Color.FromArgb(CType(CType(247, Byte), Int32), CType(CType(251, Byte), Int32), CType(CType(255, Byte), Int32))
            Me.GridView1.Appearance.EvenRow.BorderColor = System.Drawing.Color.FromArgb(CType(CType(247, Byte), Int32), CType(CType(251, Byte), Int32), CType(CType(255, Byte), Int32))
            Me.GridView1.Appearance.EvenRow.ForeColor = System.Drawing.Color.Black
            Me.GridView1.Appearance.EvenRow.Options.UseBackColor = True
            Me.GridView1.Appearance.EvenRow.Options.UseBorderColor = True
            Me.GridView1.Appearance.EvenRow.Options.UseForeColor = True
            Me.GridView1.Appearance.FilterCloseButton.BackColor = System.Drawing.Color.FromArgb(CType(CType(104, Byte), Int32), CType(CType(184, Byte), Int32), CType(CType(251, Byte), Int32))
            Me.GridView1.Appearance.FilterCloseButton.BorderColor = System.Drawing.Color.FromArgb(CType(CType(104, Byte), Int32), CType(CType(184, Byte), Int32), CType(CType(251, Byte), Int32))
            Me.GridView1.Appearance.FilterCloseButton.ForeColor = System.Drawing.Color.White
            Me.GridView1.Appearance.FilterCloseButton.Options.UseBackColor = True
            Me.GridView1.Appearance.FilterCloseButton.Options.UseBorderColor = True
            Me.GridView1.Appearance.FilterCloseButton.Options.UseForeColor = True
            Me.GridView1.Appearance.FilterPanel.BackColor = System.Drawing.Color.FromArgb(CType(CType(236, Byte), Int32), CType(CType(246, Byte), Int32), CType(CType(255, Byte), Int32))
            Me.GridView1.Appearance.FilterPanel.BackColor2 = System.Drawing.Color.White
            Me.GridView1.Appearance.FilterPanel.ForeColor = System.Drawing.Color.Black
            Me.GridView1.Appearance.FilterPanel.Options.UseBackColor = True
            Me.GridView1.Appearance.FilterPanel.Options.UseForeColor = True
            Me.GridView1.Appearance.FixedLine.BackColor = System.Drawing.Color.FromArgb(CType(CType(59, Byte), Int32), CType(CType(133, Byte), Int32), CType(CType(195, Byte), Int32))
            Me.GridView1.Appearance.FixedLine.Options.UseBackColor = True
            Me.GridView1.Appearance.FocusedCell.BackColor = System.Drawing.Color.White
            Me.GridView1.Appearance.FocusedCell.ForeColor = System.Drawing.Color.Black
            Me.GridView1.Appearance.FocusedCell.Options.UseBackColor = True
            Me.GridView1.Appearance.FocusedCell.Options.UseForeColor = True
            Me.GridView1.Appearance.FocusedRow.BackColor = System.Drawing.Color.FromArgb(CType(CType(38, Byte), Int32), CType(CType(109, Byte), Int32), CType(CType(189, Byte), Int32))
            Me.GridView1.Appearance.FocusedRow.BorderColor = System.Drawing.Color.FromArgb(CType(CType(59, Byte), Int32), CType(CType(139, Byte), Int32), CType(CType(206, Byte), Int32))
            Me.GridView1.Appearance.FocusedRow.ForeColor = System.Drawing.Color.White
            Me.GridView1.Appearance.FocusedRow.Options.UseBackColor = True
            Me.GridView1.Appearance.FocusedRow.Options.UseBorderColor = True
            Me.GridView1.Appearance.FocusedRow.Options.UseForeColor = True
            Me.GridView1.Appearance.FooterPanel.BackColor = System.Drawing.Color.FromArgb(CType(CType(104, Byte), Int32), CType(CType(184, Byte), Int32), CType(CType(251, Byte), Int32))
            Me.GridView1.Appearance.FooterPanel.BorderColor = System.Drawing.Color.FromArgb(CType(CType(104, Byte), Int32), CType(CType(184, Byte), Int32), CType(CType(251, Byte), Int32))
            Me.GridView1.Appearance.FooterPanel.ForeColor = System.Drawing.Color.Black
            Me.GridView1.Appearance.FooterPanel.Options.UseBackColor = True
            Me.GridView1.Appearance.FooterPanel.Options.UseBorderColor = True
            Me.GridView1.Appearance.FooterPanel.Options.UseForeColor = True
            Me.GridView1.Appearance.GroupButton.BackColor = System.Drawing.Color.FromArgb(CType(CType(104, Byte), Int32), CType(CType(184, Byte), Int32), CType(CType(251, Byte), Int32))
            Me.GridView1.Appearance.GroupButton.BorderColor = System.Drawing.Color.FromArgb(CType(CType(104, Byte), Int32), CType(CType(184, Byte), Int32), CType(CType(251, Byte), Int32))
            Me.GridView1.Appearance.GroupButton.Options.UseBackColor = True
            Me.GridView1.Appearance.GroupButton.Options.UseBorderColor = True
            Me.GridView1.Appearance.GroupFooter.BackColor = System.Drawing.Color.FromArgb(CType(CType(170, Byte), Int32), CType(CType(216, Byte), Int32), CType(CType(254, Byte), Int32))
            Me.GridView1.Appearance.GroupFooter.BorderColor = System.Drawing.Color.FromArgb(CType(CType(170, Byte), Int32), CType(CType(216, Byte), Int32), CType(CType(254, Byte), Int32))
            Me.GridView1.Appearance.GroupFooter.ForeColor = System.Drawing.Color.Black
            Me.GridView1.Appearance.GroupFooter.Options.UseBackColor = True
            Me.GridView1.Appearance.GroupFooter.Options.UseBorderColor = True
            Me.GridView1.Appearance.GroupFooter.Options.UseForeColor = True
            Me.GridView1.Appearance.GroupPanel.BackColor = System.Drawing.Color.FromArgb(CType(CType(236, Byte), Int32), CType(CType(246, Byte), Int32), CType(CType(255, Byte), Int32))
            Me.GridView1.Appearance.GroupPanel.BackColor2 = System.Drawing.Color.White
            Me.GridView1.Appearance.GroupPanel.ForeColor = System.Drawing.Color.Black
            Me.GridView1.Appearance.GroupPanel.Options.UseBackColor = True
            Me.GridView1.Appearance.GroupPanel.Options.UseForeColor = True
            Me.GridView1.Appearance.GroupRow.BackColor = System.Drawing.Color.FromArgb(CType(CType(170, Byte), Int32), CType(CType(216, Byte), Int32), CType(CType(254, Byte), Int32))
            Me.GridView1.Appearance.GroupRow.BorderColor = System.Drawing.Color.FromArgb(CType(CType(170, Byte), Int32), CType(CType(216, Byte), Int32), CType(CType(254, Byte), Int32))
            Me.GridView1.Appearance.GroupRow.ForeColor = System.Drawing.Color.Black
            Me.GridView1.Appearance.GroupRow.Options.UseBackColor = True
            Me.GridView1.Appearance.GroupRow.Options.UseBorderColor = True
            Me.GridView1.Appearance.GroupRow.Options.UseForeColor = True
            Me.GridView1.Appearance.HeaderPanel.BackColor = System.Drawing.Color.FromArgb(CType(CType(139, Byte), Int32), CType(CType(201, Byte), Int32), CType(CType(254, Byte), Int32))
            Me.GridView1.Appearance.HeaderPanel.BorderColor = System.Drawing.Color.FromArgb(CType(CType(139, Byte), Int32), CType(CType(201, Byte), Int32), CType(CType(254, Byte), Int32))
            Me.GridView1.Appearance.HeaderPanel.ForeColor = System.Drawing.Color.Black
            Me.GridView1.Appearance.HeaderPanel.Options.UseBackColor = True
            Me.GridView1.Appearance.HeaderPanel.Options.UseBorderColor = True
            Me.GridView1.Appearance.HeaderPanel.Options.UseForeColor = True
            Me.GridView1.Appearance.HideSelectionRow.BackColor = System.Drawing.Color.FromArgb(CType(CType(105, Byte), Int32), CType(CType(170, Byte), Int32), CType(CType(225, Byte), Int32))
            Me.GridView1.Appearance.HideSelectionRow.BorderColor = System.Drawing.Color.FromArgb(CType(CType(83, Byte), Int32), CType(CType(155, Byte), Int32), CType(CType(215, Byte), Int32))
            Me.GridView1.Appearance.HideSelectionRow.ForeColor = System.Drawing.Color.FromArgb(CType(CType(236, Byte), Int32), CType(CType(246, Byte), Int32), CType(CType(255, Byte), Int32))
            Me.GridView1.Appearance.HideSelectionRow.Options.UseBackColor = True
            Me.GridView1.Appearance.HideSelectionRow.Options.UseBorderColor = True
            Me.GridView1.Appearance.HideSelectionRow.Options.UseForeColor = True
            Me.GridView1.Appearance.HorzLine.BackColor = System.Drawing.Color.FromArgb(CType(CType(104, Byte), Int32), CType(CType(184, Byte), Int32), CType(CType(251, Byte), Int32))
            Me.GridView1.Appearance.HorzLine.Options.UseBackColor = True
            Me.GridView1.Appearance.OddRow.BackColor = System.Drawing.Color.FromArgb(CType(CType(236, Byte), Int32), CType(CType(246, Byte), Int32), CType(CType(255, Byte), Int32))
            Me.GridView1.Appearance.OddRow.BorderColor = System.Drawing.Color.FromArgb(CType(CType(236, Byte), Int32), CType(CType(246, Byte), Int32), CType(CType(255, Byte), Int32))
            Me.GridView1.Appearance.OddRow.ForeColor = System.Drawing.Color.Black
            Me.GridView1.Appearance.OddRow.Options.UseBackColor = True
            Me.GridView1.Appearance.OddRow.Options.UseBorderColor = True
            Me.GridView1.Appearance.OddRow.Options.UseForeColor = True
            Me.GridView1.Appearance.Preview.Font = New System.Drawing.Font("Verdana", 7.5!)
            Me.GridView1.Appearance.Preview.ForeColor = System.Drawing.Color.FromArgb(CType(CType(83, Byte), Int32), CType(CType(155, Byte), Int32), CType(CType(215, Byte), Int32))
            Me.GridView1.Appearance.Preview.Options.UseFont = True
            Me.GridView1.Appearance.Preview.Options.UseForeColor = True
            Me.GridView1.Appearance.Row.BackColor = System.Drawing.Color.FromArgb(CType(CType(247, Byte), Int32), CType(CType(251, Byte), Int32), CType(CType(255, Byte), Int32))
            Me.GridView1.Appearance.Row.ForeColor = System.Drawing.Color.Black
            Me.GridView1.Appearance.Row.Options.UseBackColor = True
            Me.GridView1.Appearance.Row.Options.UseForeColor = True
            Me.GridView1.Appearance.RowSeparator.BackColor = System.Drawing.Color.FromArgb(CType(CType(236, Byte), Int32), CType(CType(246, Byte), Int32), CType(CType(255, Byte), Int32))
            Me.GridView1.Appearance.RowSeparator.BackColor2 = System.Drawing.Color.White
            Me.GridView1.Appearance.RowSeparator.Options.UseBackColor = True
            Me.GridView1.Appearance.SelectedRow.BackColor = System.Drawing.Color.FromArgb(CType(CType(83, Byte), Int32), CType(CType(155, Byte), Int32), CType(CType(215, Byte), Int32))
            Me.GridView1.Appearance.SelectedRow.ForeColor = System.Drawing.Color.White
            Me.GridView1.Appearance.SelectedRow.Options.UseBackColor = True
            Me.GridView1.Appearance.SelectedRow.Options.UseForeColor = True
            Me.GridView1.Appearance.TopNewRow.BackColor = System.Drawing.Color.White
            Me.GridView1.Appearance.TopNewRow.Options.UseBackColor = True
            Me.GridView1.Appearance.VertLine.BackColor = System.Drawing.Color.FromArgb(CType(CType(104, Byte), Int32), CType(CType(184, Byte), Int32), CType(CType(251, Byte), Int32))
            Me.GridView1.Appearance.VertLine.Options.UseBackColor = True
            Me.GridView1.Columns.AddRange(New DevExpress.XtraGrid.Columns.GridColumn() {Me.GridColumn_Client, Me.GridColumn_Name, Me.GridColumn_Debit_Amt, Me.GridColumn_Contribution, Me.GridColumn_account_number, Me.GridColumn_Rate, Me.GridColumn_ID, Me.GridColumn_Deducted, Me.GridColumn_Billed, Me.GridColumn_Net_Amount, Me.GridColumn_creditor_type, Me.GridColumn_void, Me.GridColumn_IsValid})
            StyleFormatCondition1.Appearance.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Int32), CType(CType(128, Byte), Int32), CType(CType(128, Byte), Int32))
            StyleFormatCondition1.Appearance.BackColor2 = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Int32), CType(CType(128, Byte), Int32), CType(CType(128, Byte), Int32))
            StyleFormatCondition1.Appearance.BorderColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Int32), CType(CType(128, Byte), Int32), CType(CType(128, Byte), Int32))
            StyleFormatCondition1.Appearance.Options.UseBackColor = True
            StyleFormatCondition1.Appearance.Options.UseBorderColor = True
            StyleFormatCondition1.ApplyToRow = True
            StyleFormatCondition1.Column = Me.GridColumn_IsValid
            StyleFormatCondition1.Condition = DevExpress.XtraGrid.FormatConditionEnum.Equal
            StyleFormatCondition1.Value1 = False
            Me.GridView1.FormatConditions.AddRange(New DevExpress.XtraGrid.StyleFormatCondition() {StyleFormatCondition1})
            Me.GridView1.GridControl = Me.GridControl1
            Me.GridView1.Name = "GridView1"
            Me.GridView1.OptionsView.ShowFooter = True
            Me.GridView1.OptionsView.ShowGroupPanel = False
            Me.GridView1.OptionsView.ShowIndicator = False
            '
            'GridColumn_Client
            '
            Me.GridColumn_Client.AppearanceCell.Options.UseTextOptions = True
            Me.GridColumn_Client.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
            Me.GridColumn_Client.AppearanceHeader.Options.UseTextOptions = True
            Me.GridColumn_Client.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
            Me.GridColumn_Client.Caption = "Client"
            Me.GridColumn_Client.ColumnEdit = Me.RepositoryItemTextEdit_client
            Me.GridColumn_Client.FieldName = "client"
            Me.GridColumn_Client.Name = "GridColumn_Client"
            Me.GridColumn_Client.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.[True]
            Me.GridColumn_Client.SummaryItem.DisplayFormat = "{0:n0} items"
            Me.GridColumn_Client.SummaryItem.SummaryType = DevExpress.Data.SummaryItemType.Count
            Me.GridColumn_Client.Visible = True
            Me.GridColumn_Client.VisibleIndex = 0
            Me.GridColumn_Client.Width = 51
            '
            'RepositoryItemTextEdit_client
            '
            Me.RepositoryItemTextEdit_client.AllowNullInput = DevExpress.Utils.DefaultBoolean.[False]
            Me.RepositoryItemTextEdit_client.AutoHeight = False
            Me.RepositoryItemTextEdit_client.DisplayFormat.FormatString = "f0"
            Me.RepositoryItemTextEdit_client.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Custom
            Me.RepositoryItemTextEdit_client.EditFormat.FormatString = "f0"
            Me.RepositoryItemTextEdit_client.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric
            Me.RepositoryItemTextEdit_client.Mask.BeepOnError = True
            Me.RepositoryItemTextEdit_client.Mask.EditMask = "\d+"
            Me.RepositoryItemTextEdit_client.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.RegEx
            Me.RepositoryItemTextEdit_client.Name = "RepositoryItemTextEdit_client"
            '
            'GridColumn_Name
            '
            Me.GridColumn_Name.Caption = "Name"
            Me.GridColumn_Name.FieldName = "name"
            Me.GridColumn_Name.Name = "GridColumn_Name"
            Me.GridColumn_Name.OptionsColumn.AllowEdit = False
            Me.GridColumn_Name.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.[True]
            Me.GridColumn_Name.Visible = True
            Me.GridColumn_Name.VisibleIndex = 1
            Me.GridColumn_Name.Width = 151
            '
            'GridColumn_Debit_Amt
            '
            Me.GridColumn_Debit_Amt.AppearanceCell.Options.UseTextOptions = True
            Me.GridColumn_Debit_Amt.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
            Me.GridColumn_Debit_Amt.AppearanceHeader.Options.UseTextOptions = True
            Me.GridColumn_Debit_Amt.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
            Me.GridColumn_Debit_Amt.Caption = "Gross"
            Me.GridColumn_Debit_Amt.ColumnEdit = Me.RepositoryItemCalcEdit_debit_amt
            Me.GridColumn_Debit_Amt.DisplayFormat.FormatString = "c"
            Me.GridColumn_Debit_Amt.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
            Me.GridColumn_Debit_Amt.FieldName = "debit_amt"
            Me.GridColumn_Debit_Amt.GroupFormat.FormatString = "c"
            Me.GridColumn_Debit_Amt.GroupFormat.FormatType = DevExpress.Utils.FormatType.Numeric
            Me.GridColumn_Debit_Amt.Name = "GridColumn_Debit_Amt"
            Me.GridColumn_Debit_Amt.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.[True]
            Me.GridColumn_Debit_Amt.SummaryItem.DisplayFormat = "{0:c}"
            Me.GridColumn_Debit_Amt.SummaryItem.SummaryType = DevExpress.Data.SummaryItemType.Sum
            Me.GridColumn_Debit_Amt.Visible = True
            Me.GridColumn_Debit_Amt.VisibleIndex = 3
            Me.GridColumn_Debit_Amt.Width = 72
            '
            'RepositoryItemCalcEdit_debit_amt
            '
            Me.RepositoryItemCalcEdit_debit_amt.AllowNullInput = DevExpress.Utils.DefaultBoolean.[False]
            Me.RepositoryItemCalcEdit_debit_amt.AutoHeight = False
            Me.RepositoryItemCalcEdit_debit_amt.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
            Me.RepositoryItemCalcEdit_debit_amt.DisplayFormat.FormatString = "c2"
            Me.RepositoryItemCalcEdit_debit_amt.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
            Me.RepositoryItemCalcEdit_debit_amt.EditFormat.FormatString = "c2"
            Me.RepositoryItemCalcEdit_debit_amt.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric
            Me.RepositoryItemCalcEdit_debit_amt.Mask.BeepOnError = True
            Me.RepositoryItemCalcEdit_debit_amt.Mask.EditMask = "c2"
            Me.RepositoryItemCalcEdit_debit_amt.Name = "RepositoryItemCalcEdit_debit_amt"
            Me.RepositoryItemCalcEdit_debit_amt.Precision = 2
            '
            'GridColumn_Contribution
            '
            Me.GridColumn_Contribution.AppearanceCell.Options.UseTextOptions = True
            Me.GridColumn_Contribution.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
            Me.GridColumn_Contribution.AppearanceHeader.Options.UseTextOptions = True
            Me.GridColumn_Contribution.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
            Me.GridColumn_Contribution.Caption = "Fairshare"
            Me.GridColumn_Contribution.DisplayFormat.FormatString = "c"
            Me.GridColumn_Contribution.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
            Me.GridColumn_Contribution.FieldName = "fairshare_amt"
            Me.GridColumn_Contribution.GroupFormat.FormatString = "c"
            Me.GridColumn_Contribution.GroupFormat.FormatType = DevExpress.Utils.FormatType.Numeric
            Me.GridColumn_Contribution.Name = "GridColumn_Contribution"
            Me.GridColumn_Contribution.OptionsColumn.AllowEdit = False
            Me.GridColumn_Contribution.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.[True]
            Me.GridColumn_Contribution.SummaryItem.DisplayFormat = "{0:c}"
            Me.GridColumn_Contribution.SummaryItem.SummaryType = DevExpress.Data.SummaryItemType.Sum
            Me.GridColumn_Contribution.Visible = True
            Me.GridColumn_Contribution.VisibleIndex = 4
            Me.GridColumn_Contribution.Width = 84
            '
            'GridColumn_account_number
            '
            Me.GridColumn_account_number.AppearanceCell.Options.UseTextOptions = True
            Me.GridColumn_account_number.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
            Me.GridColumn_account_number.AppearanceHeader.Options.UseTextOptions = True
            Me.GridColumn_account_number.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
            Me.GridColumn_account_number.Caption = "Acct Num"
            Me.GridColumn_account_number.CustomizationCaption = "Account Number"
            Me.GridColumn_account_number.FieldName = "account_number"
            Me.GridColumn_account_number.Name = "GridColumn_account_number"
            Me.GridColumn_account_number.OptionsColumn.AllowEdit = False
            Me.GridColumn_account_number.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.[True]
            Me.GridColumn_account_number.SortMode = DevExpress.XtraGrid.ColumnSortMode.DisplayText
            Me.GridColumn_account_number.Visible = True
            Me.GridColumn_account_number.VisibleIndex = 2
            Me.GridColumn_account_number.Width = 114
            '
            'GridColumn_Rate
            '
            Me.GridColumn_Rate.AppearanceCell.Options.UseTextOptions = True
            Me.GridColumn_Rate.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
            Me.GridColumn_Rate.AppearanceHeader.Options.UseTextOptions = True
            Me.GridColumn_Rate.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
            Me.GridColumn_Rate.Caption = "Rate"
            Me.GridColumn_Rate.DisplayFormat.FormatString = "{0:p}"
            Me.GridColumn_Rate.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
            Me.GridColumn_Rate.FieldName = "fairshare_pct"
            Me.GridColumn_Rate.GroupFormat.FormatString = "{0:p}"
            Me.GridColumn_Rate.GroupFormat.FormatType = DevExpress.Utils.FormatType.Numeric
            Me.GridColumn_Rate.Name = "GridColumn_Rate"
            Me.GridColumn_Rate.OptionsColumn.AllowEdit = False
            Me.GridColumn_Rate.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.[True]
            '
            'GridColumn_ID
            '
            Me.GridColumn_ID.AppearanceCell.Options.UseTextOptions = True
            Me.GridColumn_ID.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
            Me.GridColumn_ID.AppearanceHeader.Options.UseTextOptions = True
            Me.GridColumn_ID.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
            Me.GridColumn_ID.Caption = "ID"
            Me.GridColumn_ID.DisplayFormat.FormatString = "{0:f0}"
            Me.GridColumn_ID.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
            Me.GridColumn_ID.FieldName = "client_creditor_register"
            Me.GridColumn_ID.GroupFormat.FormatString = "{0:f0}"
            Me.GridColumn_ID.GroupFormat.FormatType = DevExpress.Utils.FormatType.Numeric
            Me.GridColumn_ID.Name = "GridColumn_ID"
            Me.GridColumn_ID.OptionsColumn.AllowEdit = False
            Me.GridColumn_ID.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.[True]
            '
            'GridColumn_Deducted
            '
            Me.GridColumn_Deducted.AppearanceCell.Options.UseTextOptions = True
            Me.GridColumn_Deducted.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
            Me.GridColumn_Deducted.AppearanceHeader.Options.UseTextOptions = True
            Me.GridColumn_Deducted.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
            Me.GridColumn_Deducted.Caption = "Deducted"
            Me.GridColumn_Deducted.CustomizationCaption = "Deducted Contributions only"
            Me.GridColumn_Deducted.DisplayFormat.FormatString = "c"
            Me.GridColumn_Deducted.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
            Me.GridColumn_Deducted.FieldName = "deducted"
            Me.GridColumn_Deducted.GroupFormat.FormatString = "c"
            Me.GridColumn_Deducted.GroupFormat.FormatType = DevExpress.Utils.FormatType.Numeric
            Me.GridColumn_Deducted.Name = "GridColumn_Deducted"
            Me.GridColumn_Deducted.OptionsColumn.AllowEdit = False
            Me.GridColumn_Deducted.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.[True]
            Me.GridColumn_Deducted.SummaryItem.DisplayFormat = "{0:c}"
            Me.GridColumn_Deducted.SummaryItem.SummaryType = DevExpress.Data.SummaryItemType.Sum
            '
            'GridColumn_Billed
            '
            Me.GridColumn_Billed.AppearanceCell.Options.UseTextOptions = True
            Me.GridColumn_Billed.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
            Me.GridColumn_Billed.AppearanceHeader.Options.UseTextOptions = True
            Me.GridColumn_Billed.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
            Me.GridColumn_Billed.Caption = "Billed"
            Me.GridColumn_Billed.CustomizationCaption = "Billed Amount"
            Me.GridColumn_Billed.DisplayFormat.FormatString = "c"
            Me.GridColumn_Billed.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
            Me.GridColumn_Billed.FieldName = "billed"
            Me.GridColumn_Billed.GroupFormat.FormatString = "c"
            Me.GridColumn_Billed.GroupFormat.FormatType = DevExpress.Utils.FormatType.Numeric
            Me.GridColumn_Billed.Name = "GridColumn_Billed"
            Me.GridColumn_Billed.OptionsColumn.AllowEdit = False
            Me.GridColumn_Billed.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.[True]
            Me.GridColumn_Billed.SummaryItem.DisplayFormat = "{0:c}"
            Me.GridColumn_Billed.SummaryItem.SummaryType = DevExpress.Data.SummaryItemType.Sum
            '
            'GridColumn_Net_Amount
            '
            Me.GridColumn_Net_Amount.AppearanceCell.Options.UseTextOptions = True
            Me.GridColumn_Net_Amount.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
            Me.GridColumn_Net_Amount.AppearanceHeader.Options.UseTextOptions = True
            Me.GridColumn_Net_Amount.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
            Me.GridColumn_Net_Amount.Caption = "Net"
            Me.GridColumn_Net_Amount.CustomizationCaption = "Net amount for this debt"
            Me.GridColumn_Net_Amount.DisplayFormat.FormatString = "{0:c}"
            Me.GridColumn_Net_Amount.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
            Me.GridColumn_Net_Amount.FieldName = "net_amount"
            Me.GridColumn_Net_Amount.GroupFormat.FormatString = "{0:c}"
            Me.GridColumn_Net_Amount.GroupFormat.FormatType = DevExpress.Utils.FormatType.Numeric
            Me.GridColumn_Net_Amount.Name = "GridColumn_Net_Amount"
            Me.GridColumn_Net_Amount.OptionsColumn.AllowEdit = False
            Me.GridColumn_Net_Amount.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.[True]
            Me.GridColumn_Net_Amount.SummaryItem.DisplayFormat = "{0:c}"
            Me.GridColumn_Net_Amount.SummaryItem.SummaryType = DevExpress.Data.SummaryItemType.Sum
            Me.GridColumn_Net_Amount.Visible = True
            Me.GridColumn_Net_Amount.VisibleIndex = 5
            Me.GridColumn_Net_Amount.Width = 90
            '
            'GridColumn_creditor_type
            '
            Me.GridColumn_creditor_type.AppearanceCell.Options.UseTextOptions = True
            Me.GridColumn_creditor_type.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center
            Me.GridColumn_creditor_type.AppearanceHeader.Options.UseTextOptions = True
            Me.GridColumn_creditor_type.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center
            Me.GridColumn_creditor_type.Caption = "Type"
            Me.GridColumn_creditor_type.ColumnEdit = Me.RepositoryItemLookUpEdit_creditor_type
            Me.GridColumn_creditor_type.CustomizationCaption = "Bill/Deduct/None"
            Me.GridColumn_creditor_type.FieldName = "creditor_type"
            Me.GridColumn_creditor_type.Name = "GridColumn_creditor_type"
            Me.GridColumn_creditor_type.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.[True]
            '
            'RepositoryItemLookUpEdit_creditor_type
            '
            Me.RepositoryItemLookUpEdit_creditor_type.AllowNullInput = DevExpress.Utils.DefaultBoolean.[False]
            Me.RepositoryItemLookUpEdit_creditor_type.AutoHeight = False
            Me.RepositoryItemLookUpEdit_creditor_type.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
            Me.RepositoryItemLookUpEdit_creditor_type.Columns.AddRange(New DevExpress.XtraEditors.Controls.LookUpColumnInfo() {New DevExpress.XtraEditors.Controls.LookUpColumnInfo("creditor_type", "ID", 20, DevExpress.Utils.FormatType.None, "", False, DevExpress.Utils.HorzAlignment.[Default]), New DevExpress.XtraEditors.Controls.LookUpColumnInfo("description", "Descripiton", 20, DevExpress.Utils.FormatType.None, "", True, DevExpress.Utils.HorzAlignment.[Default], DevExpress.Data.ColumnSortOrder.Ascending)})
            Me.RepositoryItemLookUpEdit_creditor_type.DisplayMember = "description"
            Me.RepositoryItemLookUpEdit_creditor_type.Name = "RepositoryItemLookUpEdit_creditor_type"
            Me.RepositoryItemLookUpEdit_creditor_type.NullText = ""
            Me.RepositoryItemLookUpEdit_creditor_type.ShowFooter = False
            Me.RepositoryItemLookUpEdit_creditor_type.ShowHeader = False
            Me.RepositoryItemLookUpEdit_creditor_type.ValueMember = "creditor_type"
            '
            'LookUpEdit_client_creditor
            '
            Me.LookUpEdit_client_creditor.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                        Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
            Me.LookUpEdit_client_creditor.Location = New System.Drawing.Point(99, 85)
            Me.LookUpEdit_client_creditor.Name = "LookUpEdit_client_creditor"
            Me.LookUpEdit_client_creditor.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
            Me.LookUpEdit_client_creditor.Properties.Columns.AddRange(New DevExpress.XtraEditors.Controls.LookUpColumnInfo() {New DevExpress.XtraEditors.Controls.LookUpColumnInfo("client_creditor", "Debt", 20, DevExpress.Utils.FormatType.Numeric, "f0", False, DevExpress.Utils.HorzAlignment.Far, DevExpress.Data.ColumnSortOrder.Ascending), New DevExpress.XtraEditors.Controls.LookUpColumnInfo("account_number", "Account Number", 20, DevExpress.Utils.FormatType.None, "", True, DevExpress.Utils.HorzAlignment.[Default], DevExpress.Data.ColumnSortOrder.Ascending)})
            Me.LookUpEdit_client_creditor.Properties.DisplayMember = "account_number"
            Me.LookUpEdit_client_creditor.Properties.NullText = ""
            Me.LookUpEdit_client_creditor.Properties.PopupWidth = 140
            Me.LookUpEdit_client_creditor.Properties.ShowFooter = False
            Me.LookUpEdit_client_creditor.Properties.ShowHeader = False
            Me.LookUpEdit_client_creditor.Properties.ValueMember = "client_creditor"
            Me.LookUpEdit_client_creditor.Size = New System.Drawing.Size(266, 20)
            Me.LookUpEdit_client_creditor.StyleController = Me.LayoutControl1
            Me.LookUpEdit_client_creditor.TabIndex = 39
            Me.LookUpEdit_client_creditor.Properties.SortColumnIndex = 1
            '
            'LayoutControl1
            '
            Me.LayoutControl1.Controls.Add(Me.ClientID1)
            Me.LayoutControl1.Controls.Add(Me.LabelControl_client_name)
            Me.LayoutControl1.Controls.Add(Me.SimpleButton_Post)
            Me.LayoutControl1.Controls.Add(Me.SimpleButton_add)
            Me.LayoutControl1.Controls.Add(Me.LookUpEdit_client_creditor)
            Me.LayoutControl1.Controls.Add(Me.CalcEdit_amount)
            Me.LayoutControl1.Controls.Add(Me.SimpleButton_del)
            Me.LayoutControl1.Controls.Add(Me.ComboBoxEdit_reason)
            Me.LayoutControl1.Controls.Add(Me.CalcEdit_rate)
            Me.LayoutControl1.Controls.Add(Me.ComboBoxEdit_disposition)
            Me.LayoutControl1.Controls.Add(Me.GridControl1)
            Me.LayoutControl1.Controls.Add(Me.LabelControl_creditor)
            Me.LayoutControl1.Controls.Add(Me.LookupEdit_contribution)
            Me.LayoutControl1.Dock = System.Windows.Forms.DockStyle.Fill
            Me.LayoutControl1.Location = New System.Drawing.Point(0, 0)
            Me.LayoutControl1.Name = "LayoutControl1"
            Me.LayoutControl1.Root = Me.LayoutControlGroup1
            Me.LayoutControl1.Size = New System.Drawing.Size(583, 393)
            Me.LayoutControl1.TabIndex = 21
            Me.LayoutControl1.Text = "LayoutControl1"
            '
            'ClientID1
            '
            Me.ClientID1.Location = New System.Drawing.Point(99, 60)
            Me.ClientID1.Name = "ClientID1"
            Me.ClientID1.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[False]
            Me.ClientID1.Properties.Appearance.Options.UseTextOptions = True
            Me.ClientID1.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
            Me.ClientID1.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph, "", -1, True, True, False, DevExpress.XtraEditors.ImageLocation.MiddleCenter, CType(resources.GetObject("ClientID1.Properties.Buttons"), System.Drawing.Image), New DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), SerializableAppearanceObject1, "Click here to search for a client ID", "search", Nothing, True)})
            Me.ClientID1.Properties.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
            Me.ClientID1.Properties.DisplayFormat.FormatString = "0000000"
            Me.ClientID1.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Custom
            Me.ClientID1.Properties.EditFormat.FormatString = "f0"
            Me.ClientID1.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric
            Me.ClientID1.Properties.Mask.BeepOnError = True
            Me.ClientID1.Properties.Mask.EditMask = "\d*"
            Me.ClientID1.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.RegEx
            Me.ClientID1.Properties.ValidateOnEnterKey = True
            Me.ClientID1.Size = New System.Drawing.Size(109, 20)
            Me.ClientID1.StyleController = Me.LayoutControl1
            Me.ClientID1.TabIndex = 41
            '
            'LabelControl_client_name
            '
            Me.LabelControl_client_name.Appearance.TextOptions.WordWrap = DevExpress.Utils.WordWrap.NoWrap
            Me.LabelControl_client_name.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None
            Me.LabelControl_client_name.Location = New System.Drawing.Point(216, 64)
            Me.LabelControl_client_name.Name = "LabelControl_client_name"
            Me.LabelControl_client_name.Size = New System.Drawing.Size(272, 13)
            Me.LabelControl_client_name.StyleController = Me.LayoutControl1
            Me.LabelControl_client_name.TabIndex = 40
            '
            'SimpleButton_Post
            '
            Me.SimpleButton_Post.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
            Me.SimpleButton_Post.Location = New System.Drawing.Point(496, 106)
            Me.SimpleButton_Post.MaximumSize = New System.Drawing.Size(75, 23)
            Me.SimpleButton_Post.MinimumSize = New System.Drawing.Size(75, 23)
            Me.SimpleButton_Post.Name = "SimpleButton_Post"
            Me.SimpleButton_Post.Size = New System.Drawing.Size(75, 23)
            Me.SimpleButton_Post.StyleController = Me.LayoutControl1
            Me.SimpleButton_Post.TabIndex = 37
            Me.SimpleButton_Post.Text = "&Cut Check"
            '
            'SimpleButton_add
            '
            Me.SimpleButton_add.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
            Me.SimpleButton_add.Location = New System.Drawing.Point(496, 12)
            Me.SimpleButton_add.MaximumSize = New System.Drawing.Size(75, 23)
            Me.SimpleButton_add.MinimumSize = New System.Drawing.Size(75, 23)
            Me.SimpleButton_add.Name = "SimpleButton_add"
            Me.SimpleButton_add.Size = New System.Drawing.Size(75, 23)
            Me.SimpleButton_add.StyleController = Me.LayoutControl1
            Me.SimpleButton_add.TabIndex = 35
            Me.SimpleButton_add.Text = "&New Item"
            '
            'CalcEdit_amount
            '
            Me.CalcEdit_amount.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
            Me.CalcEdit_amount.Location = New System.Drawing.Point(99, 36)
            Me.CalcEdit_amount.Name = "CalcEdit_amount"
            Me.CalcEdit_amount.Properties.Appearance.Options.UseTextOptions = True
            Me.CalcEdit_amount.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
            Me.CalcEdit_amount.Properties.AppearanceDisabled.Options.UseTextOptions = True
            Me.CalcEdit_amount.Properties.AppearanceDisabled.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
            Me.CalcEdit_amount.Properties.AppearanceDropDown.Options.UseTextOptions = True
            Me.CalcEdit_amount.Properties.AppearanceDropDown.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
            Me.CalcEdit_amount.Properties.AppearanceFocused.Options.UseTextOptions = True
            Me.CalcEdit_amount.Properties.AppearanceFocused.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
            Me.CalcEdit_amount.Properties.AppearanceReadOnly.Options.UseTextOptions = True
            Me.CalcEdit_amount.Properties.AppearanceReadOnly.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
            Me.CalcEdit_amount.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
            Me.CalcEdit_amount.Properties.DisplayFormat.FormatString = "{0:c}"
            Me.CalcEdit_amount.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
            Me.CalcEdit_amount.Properties.EditFormat.FormatString = "{0:f0}"
            Me.CalcEdit_amount.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric
            Me.CalcEdit_amount.Properties.Mask.BeepOnError = True
            Me.CalcEdit_amount.Properties.Mask.EditMask = "c"
            Me.CalcEdit_amount.Properties.Precision = 2
            Me.CalcEdit_amount.Size = New System.Drawing.Size(109, 20)
            Me.CalcEdit_amount.StyleController = Me.LayoutControl1
            Me.CalcEdit_amount.TabIndex = 33
            '
            'SimpleButton_del
            '
            Me.SimpleButton_del.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
            Me.SimpleButton_del.Location = New System.Drawing.Point(496, 39)
            Me.SimpleButton_del.MaximumSize = New System.Drawing.Size(75, 23)
            Me.SimpleButton_del.MinimumSize = New System.Drawing.Size(75, 23)
            Me.SimpleButton_del.Name = "SimpleButton_del"
            Me.SimpleButton_del.Size = New System.Drawing.Size(75, 23)
            Me.SimpleButton_del.StyleController = Me.LayoutControl1
            Me.SimpleButton_del.TabIndex = 36
            Me.SimpleButton_del.Text = "&Remove Item"
            '
            'ComboBoxEdit_reason
            '
            Me.ComboBoxEdit_reason.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                        Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
            Me.ComboBoxEdit_reason.Location = New System.Drawing.Point(99, 109)
            Me.ComboBoxEdit_reason.Name = "ComboBoxEdit_reason"
            Me.ComboBoxEdit_reason.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
            Me.ComboBoxEdit_reason.Size = New System.Drawing.Size(393, 20)
            Me.ComboBoxEdit_reason.StyleController = Me.LayoutControl1
            Me.ComboBoxEdit_reason.TabIndex = 26
            '
            'CalcEdit_rate
            '
            Me.CalcEdit_rate.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
            Me.CalcEdit_rate.Location = New System.Drawing.Point(99, 12)
            Me.CalcEdit_rate.Name = "CalcEdit_rate"
            Me.CalcEdit_rate.Properties.Appearance.Options.UseTextOptions = True
            Me.CalcEdit_rate.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
            Me.CalcEdit_rate.Properties.AppearanceDisabled.Options.UseTextOptions = True
            Me.CalcEdit_rate.Properties.AppearanceDisabled.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
            Me.CalcEdit_rate.Properties.AppearanceDropDown.Options.UseTextOptions = True
            Me.CalcEdit_rate.Properties.AppearanceDropDown.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
            Me.CalcEdit_rate.Properties.AppearanceFocused.Options.UseTextOptions = True
            Me.CalcEdit_rate.Properties.AppearanceFocused.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
            Me.CalcEdit_rate.Properties.AppearanceReadOnly.Options.UseTextOptions = True
            Me.CalcEdit_rate.Properties.AppearanceReadOnly.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
            Me.CalcEdit_rate.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
            Me.CalcEdit_rate.Properties.DisplayFormat.FormatString = "{0:p}"
            Me.CalcEdit_rate.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
            Me.CalcEdit_rate.Properties.EditFormat.FormatString = "{0:f4}"
            Me.CalcEdit_rate.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric
            Me.CalcEdit_rate.Properties.Mask.BeepOnError = True
            Me.CalcEdit_rate.Properties.Mask.EditMask = "p"
            Me.CalcEdit_rate.Properties.Precision = 4
            Me.CalcEdit_rate.Size = New System.Drawing.Size(109, 20)
            Me.CalcEdit_rate.StyleController = Me.LayoutControl1
            Me.CalcEdit_rate.TabIndex = 22
            '
            'ComboBoxEdit_disposition
            '
            Me.ComboBoxEdit_disposition.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                        Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
            Me.ComboBoxEdit_disposition.Location = New System.Drawing.Point(299, 36)
            Me.ComboBoxEdit_disposition.Name = "ComboBoxEdit_disposition"
            Me.ComboBoxEdit_disposition.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[False]
            Me.ComboBoxEdit_disposition.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
            Me.ComboBoxEdit_disposition.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor
            Me.ComboBoxEdit_disposition.Size = New System.Drawing.Size(193, 20)
            Me.ComboBoxEdit_disposition.StyleController = Me.LayoutControl1
            Me.ComboBoxEdit_disposition.TabIndex = 24
            '
            'LabelControl_creditor
            '
            Me.LabelControl_creditor.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
            Me.LabelControl_creditor.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
            Me.LabelControl_creditor.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None
            Me.LabelControl_creditor.Location = New System.Drawing.Point(369, 85)
            Me.LabelControl_creditor.Name = "LabelControl_creditor"
            Me.LabelControl_creditor.Size = New System.Drawing.Size(123, 13)
            Me.LabelControl_creditor.StyleController = Me.LayoutControl1
            Me.LabelControl_creditor.TabIndex = 34
            '
            'LookupEdit_contribution
            '
            Me.LookupEdit_contribution.Location = New System.Drawing.Point(299, 12)
            Me.LookupEdit_contribution.Name = "LookupEdit_contribution"
            Me.LookupEdit_contribution.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
            Me.LookupEdit_contribution.Properties.Columns.AddRange(New DevExpress.XtraEditors.Controls.LookUpColumnInfo() {New DevExpress.XtraEditors.Controls.LookUpColumnInfo("creditor_type", "ID", 20, DevExpress.Utils.FormatType.None, "", False, DevExpress.Utils.HorzAlignment.[Default]), New DevExpress.XtraEditors.Controls.LookUpColumnInfo("description", "Description", 20, DevExpress.Utils.FormatType.None, "", True, DevExpress.Utils.HorzAlignment.[Default], DevExpress.Data.ColumnSortOrder.Ascending)})
            Me.LookupEdit_contribution.Properties.DisplayMember = "description"
            Me.LookupEdit_contribution.Properties.NullText = ""
            Me.LookupEdit_contribution.Properties.PopupSizeable = False
            Me.LookupEdit_contribution.Properties.ShowFooter = False
            Me.LookupEdit_contribution.Properties.ShowHeader = False
            Me.LookupEdit_contribution.Properties.ValueMember = "creditor_type"
            Me.LookupEdit_contribution.Size = New System.Drawing.Size(193, 20)
            Me.LookupEdit_contribution.StyleController = Me.LayoutControl1
            Me.LookupEdit_contribution.TabIndex = 20
            Me.LookupEdit_contribution.Properties.SortColumnIndex = 1
            '
            'LayoutControlGroup1
            '
            Me.LayoutControlGroup1.CustomizationFormText = "LayoutControlGroup1"
            Me.LayoutControlGroup1.Items.AddRange(New DevExpress.XtraLayout.BaseLayoutItem() {Me.LayoutControlItem1, Me.LayoutControlItem2, Me.LayoutControlItem6, Me.LayoutControlItem8, Me.LayoutControlItem9, Me.LayoutControlItem11, Me.LayoutControlItem12, Me.LayoutControlItem10, Me.EmptySpaceItem1, Me.LayoutControlItem7, Me.LayoutControlItem13, Me.LayoutControlItem3, Me.LayoutControlItem5, Me.LayoutControlItem14})
            Me.LayoutControlGroup1.Location = New System.Drawing.Point(0, 0)
            Me.LayoutControlGroup1.Name = "LayoutControlGroup1"
            Me.LayoutControlGroup1.Size = New System.Drawing.Size(583, 393)
            Me.LayoutControlGroup1.Text = "LayoutControlGroup1"
            Me.LayoutControlGroup1.TextVisible = False
            '
            'LayoutControlItem1
            '
            Me.LayoutControlItem1.Control = Me.GridControl1
            Me.LayoutControlItem1.CustomizationFormText = "LayoutControlItem1"
            Me.LayoutControlItem1.Location = New System.Drawing.Point(0, 121)
            Me.LayoutControlItem1.Name = "LayoutControlItem1"
            Me.LayoutControlItem1.Size = New System.Drawing.Size(563, 252)
            Me.LayoutControlItem1.Text = "LayoutControlItem1"
            Me.LayoutControlItem1.TextSize = New System.Drawing.Size(0, 0)
            Me.LayoutControlItem1.TextToControlDistance = 0
            Me.LayoutControlItem1.TextVisible = False
            '
            'LayoutControlItem2
            '
            Me.LayoutControlItem2.Control = Me.LookUpEdit_client_creditor
            Me.LayoutControlItem2.CustomizationFormText = "Account Number"
            Me.LayoutControlItem2.Location = New System.Drawing.Point(0, 73)
            Me.LayoutControlItem2.Name = "LayoutControlItem2"
            Me.LayoutControlItem2.Size = New System.Drawing.Size(357, 24)
            Me.LayoutControlItem2.Text = "Account Number"
            Me.LayoutControlItem2.TextSize = New System.Drawing.Size(83, 13)
            '
            'LayoutControlItem6
            '
            Me.LayoutControlItem6.Control = Me.ComboBoxEdit_disposition
            Me.LayoutControlItem6.CustomizationFormText = "Check Disposition"
            Me.LayoutControlItem6.Location = New System.Drawing.Point(200, 24)
            Me.LayoutControlItem6.Name = "LayoutControlItem6"
            Me.LayoutControlItem6.Size = New System.Drawing.Size(284, 24)
            Me.LayoutControlItem6.Text = "Check Disposition"
            Me.LayoutControlItem6.TextSize = New System.Drawing.Size(83, 13)
            '
            'LayoutControlItem8
            '
            Me.LayoutControlItem8.Control = Me.CalcEdit_rate
            Me.LayoutControlItem8.CustomizationFormText = "Rate"
            Me.LayoutControlItem8.Location = New System.Drawing.Point(0, 0)
            Me.LayoutControlItem8.Name = "LayoutControlItem8"
            Me.LayoutControlItem8.Size = New System.Drawing.Size(200, 24)
            Me.LayoutControlItem8.Text = "Rate"
            Me.LayoutControlItem8.TextSize = New System.Drawing.Size(83, 13)
            '
            'LayoutControlItem9
            '
            Me.LayoutControlItem9.Control = Me.LabelControl_creditor
            Me.LayoutControlItem9.CustomizationFormText = "LayoutControlItem9"
            Me.LayoutControlItem9.Location = New System.Drawing.Point(357, 73)
            Me.LayoutControlItem9.Name = "LayoutControlItem9"
            Me.LayoutControlItem9.Size = New System.Drawing.Size(127, 24)
            Me.LayoutControlItem9.Text = "LayoutControlItem9"
            Me.LayoutControlItem9.TextSize = New System.Drawing.Size(0, 0)
            Me.LayoutControlItem9.TextToControlDistance = 0
            Me.LayoutControlItem9.TextVisible = False
            '
            'LayoutControlItem11
            '
            Me.LayoutControlItem11.Control = Me.SimpleButton_add
            Me.LayoutControlItem11.CustomizationFormText = "LayoutControlItem11"
            Me.LayoutControlItem11.Location = New System.Drawing.Point(484, 0)
            Me.LayoutControlItem11.Name = "LayoutControlItem11"
            Me.LayoutControlItem11.Size = New System.Drawing.Size(79, 27)
            Me.LayoutControlItem11.Text = "LayoutControlItem11"
            Me.LayoutControlItem11.TextSize = New System.Drawing.Size(0, 0)
            Me.LayoutControlItem11.TextToControlDistance = 0
            Me.LayoutControlItem11.TextVisible = False
            '
            'LayoutControlItem12
            '
            Me.LayoutControlItem12.Control = Me.SimpleButton_del
            Me.LayoutControlItem12.CustomizationFormText = "LayoutControlItem12"
            Me.LayoutControlItem12.Location = New System.Drawing.Point(484, 27)
            Me.LayoutControlItem12.Name = "LayoutControlItem12"
            Me.LayoutControlItem12.Size = New System.Drawing.Size(79, 27)
            Me.LayoutControlItem12.Text = "LayoutControlItem12"
            Me.LayoutControlItem12.TextSize = New System.Drawing.Size(0, 0)
            Me.LayoutControlItem12.TextToControlDistance = 0
            Me.LayoutControlItem12.TextVisible = False
            '
            'LayoutControlItem10
            '
            Me.LayoutControlItem10.Control = Me.SimpleButton_Post
            Me.LayoutControlItem10.CustomizationFormText = "LayoutControlItem10"
            Me.LayoutControlItem10.Location = New System.Drawing.Point(484, 94)
            Me.LayoutControlItem10.Name = "LayoutControlItem10"
            Me.LayoutControlItem10.Size = New System.Drawing.Size(79, 27)
            Me.LayoutControlItem10.Text = "LayoutControlItem10"
            Me.LayoutControlItem10.TextSize = New System.Drawing.Size(0, 0)
            Me.LayoutControlItem10.TextToControlDistance = 0
            Me.LayoutControlItem10.TextVisible = False
            '
            'EmptySpaceItem1
            '
            Me.EmptySpaceItem1.CustomizationFormText = "EmptySpaceItem1"
            Me.EmptySpaceItem1.Location = New System.Drawing.Point(484, 54)
            Me.EmptySpaceItem1.Name = "EmptySpaceItem1"
            Me.EmptySpaceItem1.Size = New System.Drawing.Size(79, 40)
            Me.EmptySpaceItem1.Text = "EmptySpaceItem1"
            Me.EmptySpaceItem1.TextSize = New System.Drawing.Size(0, 0)
            '
            'LayoutControlItem7
            '
            Me.LayoutControlItem7.Control = Me.LookupEdit_contribution
            Me.LayoutControlItem7.CustomizationFormText = "Contribution"
            Me.LayoutControlItem7.Location = New System.Drawing.Point(200, 0)
            Me.LayoutControlItem7.Name = "LayoutControlItem7"
            Me.LayoutControlItem7.Size = New System.Drawing.Size(284, 24)
            Me.LayoutControlItem7.Text = "Contribution"
            Me.LayoutControlItem7.TextSize = New System.Drawing.Size(83, 13)
            '
            'LayoutControlItem13
            '
            Me.LayoutControlItem13.Control = Me.LabelControl_client_name
            Me.LayoutControlItem13.CustomizationFormText = "LayoutControlItem13"
            Me.LayoutControlItem13.Location = New System.Drawing.Point(200, 48)
            Me.LayoutControlItem13.Name = "LayoutControlItem13"
            Me.LayoutControlItem13.Size = New System.Drawing.Size(284, 25)
            Me.LayoutControlItem13.Spacing = New DevExpress.XtraLayout.Utils.Padding(4, 4, 4, 4)
            Me.LayoutControlItem13.Text = "LayoutControlItem13"
            Me.LayoutControlItem13.TextSize = New System.Drawing.Size(0, 0)
            Me.LayoutControlItem13.TextToControlDistance = 0
            Me.LayoutControlItem13.TextVisible = False
            '
            'LayoutControlItem3
            '
            Me.LayoutControlItem3.Control = Me.CalcEdit_amount
            Me.LayoutControlItem3.CustomizationFormText = "Amount"
            Me.LayoutControlItem3.Location = New System.Drawing.Point(0, 24)
            Me.LayoutControlItem3.Name = "LayoutControlItem3"
            Me.LayoutControlItem3.Size = New System.Drawing.Size(200, 24)
            Me.LayoutControlItem3.Text = "Amount"
            Me.LayoutControlItem3.TextSize = New System.Drawing.Size(83, 13)
            '
            'LayoutControlItem5
            '
            Me.LayoutControlItem5.Control = Me.ComboBoxEdit_reason
            Me.LayoutControlItem5.CustomizationFormText = "Reason"
            Me.LayoutControlItem5.Location = New System.Drawing.Point(0, 97)
            Me.LayoutControlItem5.Name = "LayoutControlItem5"
            Me.LayoutControlItem5.Size = New System.Drawing.Size(484, 24)
            Me.LayoutControlItem5.Text = "Reason"
            Me.LayoutControlItem5.TextSize = New System.Drawing.Size(83, 13)
            '
            'LayoutControlItem14
            '
            Me.LayoutControlItem14.Control = Me.ClientID1
            Me.LayoutControlItem14.CustomizationFormText = "Client"
            Me.LayoutControlItem14.Location = New System.Drawing.Point(0, 48)
            Me.LayoutControlItem14.Name = "LayoutControlItem14"
            Me.LayoutControlItem14.Size = New System.Drawing.Size(200, 25)
            Me.LayoutControlItem14.Text = "Client"
            Me.LayoutControlItem14.TextSize = New System.Drawing.Size(83, 13)
            '
            'Main_Entry
            '
            Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
            Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
            Me.Controls.Add(Me.LayoutControl1)
            Me.Name = "Main_Entry"
            Me.Size = New System.Drawing.Size(583, 393)
            CType(Me.GridControl1, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.GridView1, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.RepositoryItemTextEdit_client, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.RepositoryItemCalcEdit_debit_amt, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.RepositoryItemLookUpEdit_creditor_type, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.LookUpEdit_client_creditor.Properties, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.LayoutControl1, System.ComponentModel.ISupportInitialize).EndInit()
            Me.LayoutControl1.ResumeLayout(False)
            CType(Me.ClientID1.Properties, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.CalcEdit_amount.Properties, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.ComboBoxEdit_reason.Properties, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.CalcEdit_rate.Properties, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.ComboBoxEdit_disposition.Properties, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.LookupEdit_contribution.Properties, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.LayoutControlGroup1, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.LayoutControlItem1, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.LayoutControlItem2, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.LayoutControlItem6, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.LayoutControlItem8, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.LayoutControlItem9, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.LayoutControlItem11, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.LayoutControlItem12, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.LayoutControlItem10, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.EmptySpaceItem1, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.LayoutControlItem7, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.LayoutControlItem13, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.LayoutControlItem3, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.LayoutControlItem5, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.LayoutControlItem14, System.ComponentModel.ISupportInitialize).EndInit()
            Me.ResumeLayout(False)

        End Sub
        Friend WithEvents GridControl1 As DevExpress.XtraGrid.GridControl
        Friend WithEvents GridView1 As DevExpress.XtraGrid.Views.Grid.GridView
        Friend WithEvents GridColumn_Client As DevExpress.XtraGrid.Columns.GridColumn
        Friend WithEvents GridColumn_Name As DevExpress.XtraGrid.Columns.GridColumn
        Friend WithEvents GridColumn_Debit_Amt As DevExpress.XtraGrid.Columns.GridColumn
        Friend WithEvents GridColumn_Contribution As DevExpress.XtraGrid.Columns.GridColumn
        Friend WithEvents GridColumn_account_number As DevExpress.XtraGrid.Columns.GridColumn
        Friend WithEvents GridColumn_Rate As DevExpress.XtraGrid.Columns.GridColumn
        Friend WithEvents GridColumn_ID As DevExpress.XtraGrid.Columns.GridColumn
        Friend WithEvents GridColumn_Deducted As DevExpress.XtraGrid.Columns.GridColumn
        Friend WithEvents GridColumn_Net_Amount As DevExpress.XtraGrid.Columns.GridColumn
        Friend WithEvents GridColumn_creditor_type As DevExpress.XtraGrid.Columns.GridColumn
        Protected Friend WithEvents SimpleButton_Post As DevExpress.XtraEditors.SimpleButton
        Friend WithEvents CalcEdit_amount As DevExpress.XtraEditors.CalcEdit
        Protected Friend WithEvents ComboBoxEdit_reason As DevExpress.XtraEditors.ComboBoxEdit
        Protected Friend WithEvents ComboBoxEdit_disposition As DevExpress.XtraEditors.ComboBoxEdit
        Protected Friend WithEvents CalcEdit_rate As DevExpress.XtraEditors.CalcEdit
        Protected Friend WithEvents SimpleButton_del As DevExpress.XtraEditors.SimpleButton
        Protected Friend WithEvents SimpleButton_add As DevExpress.XtraEditors.SimpleButton
        Protected Friend WithEvents LabelControl_creditor As DevExpress.XtraEditors.LabelControl
        Friend WithEvents LookUpEdit_client_creditor As DevExpress.XtraEditors.LookUpEdit
        Friend WithEvents LayoutControl1 As DevExpress.XtraLayout.LayoutControl
        Friend WithEvents LayoutControlGroup1 As DevExpress.XtraLayout.LayoutControlGroup
        Friend WithEvents GridColumn_Billed As DevExpress.XtraGrid.Columns.GridColumn
        Friend WithEvents LayoutControlItem1 As DevExpress.XtraLayout.LayoutControlItem
        Friend WithEvents LayoutControlItem2 As DevExpress.XtraLayout.LayoutControlItem
        Friend WithEvents LayoutControlItem3 As DevExpress.XtraLayout.LayoutControlItem
        Friend WithEvents LayoutControlItem5 As DevExpress.XtraLayout.LayoutControlItem
        Friend WithEvents LayoutControlItem6 As DevExpress.XtraLayout.LayoutControlItem
        Friend WithEvents LayoutControlItem7 As DevExpress.XtraLayout.LayoutControlItem
        Friend WithEvents LayoutControlItem8 As DevExpress.XtraLayout.LayoutControlItem
        Friend WithEvents LayoutControlItem9 As DevExpress.XtraLayout.LayoutControlItem
        Friend WithEvents LayoutControlItem10 As DevExpress.XtraLayout.LayoutControlItem
        Friend WithEvents LayoutControlItem11 As DevExpress.XtraLayout.LayoutControlItem
        Friend WithEvents LayoutControlItem12 As DevExpress.XtraLayout.LayoutControlItem
        Friend WithEvents EmptySpaceItem1 As DevExpress.XtraLayout.EmptySpaceItem
        Friend WithEvents LabelControl_client_name As DevExpress.XtraEditors.LabelControl
        Friend WithEvents LayoutControlItem13 As DevExpress.XtraLayout.LayoutControlItem
        Friend WithEvents ClientID1 As DebtPlus.UI.Client.Widgets.Controls.ClientID
        Friend WithEvents LayoutControlItem14 As DevExpress.XtraLayout.LayoutControlItem
        Friend WithEvents GridColumn_void As DevExpress.XtraGrid.Columns.GridColumn
        Friend WithEvents GridColumn_IsValid As DevExpress.XtraGrid.Columns.GridColumn
        Friend WithEvents LookupEdit_contribution As DevExpress.XtraEditors.LookUpEdit
        Friend WithEvents RepositoryItemTextEdit_client As DevExpress.XtraEditors.Repository.RepositoryItemTextEdit
        Friend WithEvents RepositoryItemCalcEdit_debit_amt As DevExpress.XtraEditors.Repository.RepositoryItemCalcEdit
        Friend WithEvents RepositoryItemLookUpEdit_creditor_type As DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit
    End Class
End Namespace
