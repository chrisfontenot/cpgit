#Region "Copyright 2000-2012 DebtPlus, L.L.C."
'{*******************************************************************}
'{                                                                   }
'{       DebtPlus Debt Management System                             }
'{                                                                   }
'{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
'{       ALL RIGHTS RESERVED                                         }
'{                                                                   }
'{   The entire contents of this file is protected by U.S. and       }
'{   International Copyright Laws. Unauthorized reproduction,        }
'{   reverse-engineering, and distribution of all or any portion of  }
'{   the code contained in this file is strictly prohibited and may  }
'{   result in severe civil and criminal penalties and will be       }
'{   prosecuted to the maximum extent possible under the law.        }
'{                                                                   }
'{   RESTRICTIONS                                                    }
'{                                                                   }
'{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
'{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
'{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
'{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
'{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
'{                                                                   }
'{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
'{   ADDITIONAL RESTRICTIONS.                                        }
'{                                                                   }
'{*******************************************************************}
#End Region

Imports DebtPlus.UI.Common
Imports System.Data.SqlClient
Imports System.Windows.Forms

Namespace Disbursement.Manual.Controls
    Friend Class Main_Entry

        ' List of the transactions for the current disbursement batch
        Public TransactionsTable As New List(Of Transaction)

        ''' <summary>
        ''' Handle the new event creation
        ''' </summary>
        Public Sub New(ByVal ctx As Context)
            MyBase.new(ctx)
            InitializeComponent()
            RegisterHandlers()
        End Sub

        Private Sub RegisterHandlers()
            AddHandler Load, AddressOf MyGridControl_Load
            AddHandler ClientID1.EditValueChanged, AddressOf ClientID1_EditValueChanged
            AddHandler SimpleButton_add.Click, AddressOf SimpleButton_Add_Click
            AddHandler SimpleButton_del.Click, AddressOf SimpleButton_Delete_Click
            AddHandler SimpleButton_Post.Click, AddressOf SimpleButton_Post_Click
            AddHandler GridView1.Layout, AddressOf LayoutChanged
            AddHandler GridView1.FocusedRowChanged, AddressOf BindEntryControls
        End Sub

        Private Sub DeRegisterHandlers()
            RemoveHandler Load, AddressOf MyGridControl_Load
            RemoveHandler ClientID1.EditValueChanged, AddressOf ClientID1_EditValueChanged
            RemoveHandler SimpleButton_add.Click, AddressOf SimpleButton_Add_Click
            RemoveHandler SimpleButton_del.Click, AddressOf SimpleButton_Delete_Click
            RemoveHandler SimpleButton_Post.Click, AddressOf SimpleButton_Post_Click
            RemoveHandler GridView1.Layout, AddressOf LayoutChanged
            RemoveHandler GridView1.FocusedRowChanged, AddressOf BindEntryControls
        End Sub

        ''' <summary>
        ''' Return the directory to the saved layout information
        ''' </summary>
        Protected Overridable Function XMLBasePath() As String
            Dim basePath As String = String.Format("{0}{1}DebtPlus", Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData), System.IO.Path.DirectorySeparatorChar)
            Return System.IO.Path.Combine(basePath, "Disbursement.Manual")
        End Function

        ''' <summary>
        ''' When loading the control, restore the layout from the file
        ''' </summary>
        Private Sub MyGridControl_Load(ByVal sender As Object, ByVal e As EventArgs)
            Dim pathName As String = XMLBasePath()
            Dim fileName As String = System.IO.Path.Combine(pathName, Name + ".Grid.xml")

            DeRegisterHandlers()
            Try
                If System.IO.File.Exists(fileName) Then
                    GridView1.RestoreLayoutFromXml(fileName)
                End If
            Catch ex As System.IO.DirectoryNotFoundException
            Catch ex As System.IO.FileNotFoundException

            Finally
                GridView1.ActiveFilterString = "[void] = 0"
                GridView1.OptionsView.ShowFilterPanelMode = DevExpress.XtraGrid.Views.Base.ShowFilterPanelMode.Never
                RegisterHandlers()
            End Try
        End Sub

        ''' <summary>
        ''' If the layout is changed then update the file
        ''' </summary>
        Private Sub LayoutChanged(ByVal Sender As Object, ByVal e As EventArgs)
            Dim pathName As String = XMLBasePath()
            If Not System.IO.Directory.Exists(pathName) Then
                System.IO.Directory.CreateDirectory(pathName)
            End If

            Dim fileName As String = System.IO.Path.Combine(pathName, Name + ".Grid.xml")
            GridView1.SaveLayoutToXml(fileName)
        End Sub

        ''' <summary>
        ''' Generate the CANCEL event to the caller
        ''' </summary>
        Public Event Cancelled As EventHandler
        Protected Sub OnCancelled(ByVal e As EventArgs)
            RaiseEvent Cancelled(Me, e)
        End Sub
        Protected Sub RaiseCancelled()
            OnCancelled(EventArgs.Empty)
        End Sub

        ''' <summary>
        ''' Generate the OK event to the caller
        ''' </summary>
        Public Event Completed As EventHandler
        Protected Sub OnCompleted(ByVal e As EventArgs)
            RaiseEvent Completed(Me, e)
        End Sub
        Protected Sub RaiseCompleted()
            OnCompleted(EventArgs.Empty)
        End Sub

        ''' <summary>
        ''' Creditor ID used for the form
        ''' </summary>
        Private ReadOnly Property Creditor() As String
            Get
                Return CType(FindForm(), Form_ManualDisbursement).Creditor
            End Get
        End Property

        ''' <summary>
        ''' Trust Register used for the form
        ''' </summary>
        Private ReadOnly Property TrustRegister() As Int32
            Get
                Return CType(FindForm(), Form_ManualDisbursement).TrustRegister
            End Get
        End Property

        Private Sub Load_ComboboxEdit_contribution()
            With LookupEdit_contribution
                .Properties.DataSource = CreditorTypes.DefaultView
            End With
        End Sub

        Private Sub Load_ComboBoxEdit_disposition()
            With ComboBoxEdit_disposition
                With .Properties
                    With .Items
                        .Clear()
                        .Add(New DebtPlus.Data.Controls.ComboboxItem("Save the check for the next print run", "QUEUE"))
                        .Add(New DebtPlus.Data.Controls.ComboboxItem("Print the check immediately", "PRINT"))
                        .Add(New DebtPlus.Data.Controls.ComboboxItem("Enter the information for a previously printed check", "MANUAL"))
                    End With
                End With
                .SelectedIndex = 0
            End With
        End Sub

        Private Sub Load_ComboBoxEdit_reason()
            With ComboBoxEdit_reason
                With .Properties
                    With .Items
                        .Clear()
                        For Each row As DataRow In ReasonsTable.Rows
                            .Add(New DebtPlus.Data.Controls.ComboboxItem(Convert.ToString(row("description")), row("item_key")))
                        Next
                    End With
                End With
                .SelectedIndex = 0
            End With
        End Sub

        ''' <summary>
        ''' Do the CONTROL LOAD event processing once things are stable
        ''' </summary>
        Public Overrides Sub ReadForm()

            ' Set the creditor for display
            LabelControl_creditor.Text = "Creditor: " + Creditor

            Load_ComboboxEdit_contribution()
            Load_ComboBoxEdit_disposition()
            Load_ComboBoxEdit_reason()

            ' Determine if the warning dialog is to be used
            CheckChangeStatus = 0
            If TrustRegister > 0 Then
                CheckChangeStatus = 1
                ReadTransactions()
            End If

            ' Bind the data list to the grid and select the first item in the list
            With GridControl1
                .DataSource = TransactionsTable
                .RefreshDataSource()
                If TransactionsTable.Count > 0 Then
                    GridView1.FocusedRowHandle = 0
                End If
            End With

            With RepositoryItemLookUpEdit_creditor_type
                .DataSource = CreditorTypes()
            End With

            ' Trip the first change in the FocusedRow
            BindEntryControls(Nothing, Nothing)
        End Sub

        Private Sub BindEntryControls(ByVal sender As Object, ByVal e As DevExpress.XtraGrid.Views.Base.FocusedRowChangedEventArgs)
            Dim CurrentTransaction As Transaction = CType(GridView1.GetFocusedRow, Transaction)
            With ClientID1
                .DataBindings.Clear()
                If CurrentTransaction IsNot Nothing Then
                    .DataBindings.Add("EditValue", CurrentTransaction, "client", False, DataSourceUpdateMode.OnValidation)
                    .Enabled = True
                Else
                    .Enabled = False
                End If
            End With

            With CalcEdit_amount
                .DataBindings.Clear()
                If CurrentTransaction IsNot Nothing Then
                    .DataBindings.Add("EditValue", CurrentTransaction, "debit_amt", False, DataSourceUpdateMode.OnPropertyChanged)
                    .Enabled = True
                Else
                    .Enabled = False
                End If
            End With

            With CalcEdit_rate
                .DataBindings.Clear()
                If CurrentTransaction IsNot Nothing Then
                    .DataBindings.Add("EditValue", CurrentTransaction, "fairshare_pct", False, DataSourceUpdateMode.OnPropertyChanged)
                    .Enabled = True
                Else
                    .Enabled = False
                End If
            End With

            With LabelControl_client_name
                .DataBindings.Clear()
                If CurrentTransaction IsNot Nothing Then
                    .DataBindings.Add("Text", CurrentTransaction, "name", False, DataSourceUpdateMode.OnPropertyChanged)
                Else
                    .Text = String.Empty
                End If
            End With

            With LookUpEdit_client_creditor
                .DataBindings.Clear()
                If CurrentTransaction IsNot Nothing Then
                    .Properties.DataSource = ClientCreditorView(CurrentTransaction)
                    .DataBindings.Add("EditValue", CurrentTransaction, "client_creditor", False, DataSourceUpdateMode.OnPropertyChanged)
                    .Enabled = True
                Else
                    .Enabled = False
                End If
            End With

            With LookupEdit_contribution
                .DataBindings.Clear()
                If CurrentTransaction IsNot Nothing Then
                    .DataBindings.Add("EditValue", CurrentTransaction, "creditor_type", False, DataSourceUpdateMode.OnPropertyChanged)
                    .Enabled = True
                Else
                    .Enabled = False
                End If
            End With

            ' If there is a transaction then enable the delete button
            SimpleButton_del.Enabled = (CurrentTransaction IsNot Nothing)

            ' Enable the post button if possible
            EnablePostButton()
        End Sub

        Private Function ClientCreditorView(ByVal CurrentTransaction As Transaction) As DataView
            Const tableName As String = "client_creditor"
            Dim tbl As DataTable = ctx.ds.Tables(tableName)

            If tbl IsNot Nothing Then
                Dim vue As New DataView(tbl, String.Format("[client]={0:f0} AND [creditor]='{1}'", CurrentTransaction.client, Creditor), "account_number", DataViewRowState.CurrentRows)
                If vue.Count > 0 Then
                    Return vue
                End If
                vue.Dispose()
            End If

            Dim cn As SqlClient.SqlConnection = New SqlConnection(DebtPlus.LINQ.SQLInfoClass.getDefault().ConnectionString)
            Try
                cn.Open()
                Using cmd As SqlClient.SqlCommand = New SqlCommand
                    With cmd
                        .Connection = cn
                        .CommandText = "SELECT client_creditor, client, creditor, account_number, reassigned_debt from client_creditor WHERE client=@client and creditor=@creditor"
                        .CommandType = CommandType.Text
                        .Parameters.Add("@client", SqlDbType.Int).Value = CurrentTransaction.client
                        .Parameters.Add("@creditor", SqlDbType.VarChar).Value = Creditor
                    End With

                    Using da As New SqlClient.SqlDataAdapter(cmd)
                        da.Fill(ctx.ds, tableName)
                    End Using

                    tbl = ctx.ds.Tables(tableName)
                    With tbl
                        If .PrimaryKey.GetUpperBound(0) < 0 Then
                            .PrimaryKey = New DataColumn() {.Columns("client_creditor")}
                        End If
                    End With
                End Using

            Catch ex As Exception
                DebtPlus.UI.Common.ErrorHandling.HandleErrors(ex)

            Finally
                If cn IsNot Nothing Then cn.Dispose()
            End Try

            If tbl IsNot Nothing Then
                Return New DataView(tbl, String.Format("[client]={0:f0} AND [creditor]='{1}'", CurrentTransaction.client, Creditor), "account_number", DataViewRowState.CurrentRows)
            End If

            Return Nothing
        End Function

        Private Function ReasonsTable() As DataTable
            Const TableName As String = "lst_descriptions_MD"
            Dim tbl As DataTable = ctx.ds.Tables(TableName)

            If tbl Is Nothing Then
                Try
                    Using cmd As SqlClient.SqlCommand = New SqlCommand
                        With cmd
                            .Connection = New SqlConnection(DebtPlus.LINQ.SQLInfoClass.getDefault().ConnectionString)
                            .CommandText = "lst_descriptions_MD"
                            .CommandType = CommandType.StoredProcedure
                        End With

                        Using da As New SqlClient.SqlDataAdapter(cmd)
                            da.Fill(ctx.ds, TableName)
                        End Using

                        tbl = ctx.ds.Tables(TableName)
                        With tbl
                            If .PrimaryKey.GetUpperBound(0) < 0 Then
                                .PrimaryKey = New DataColumn() {.Columns("client")}
                            End If
                        End With
                    End Using

                Catch ex As SqlClient.SqlException
                    DebtPlus.UI.Common.ErrorHandling.HandleErrors(ex, "Error reading lst_descripitons_MD")
                End Try
            End If

            Return tbl
        End Function

        Private Function DefaultCreditorType() As String
            Dim row As DataRow = ReadCreditor(Creditor)
            Return DebtPlus.Utils.Nulls.DStr(row("creditor_type_check"))
        End Function

        Private Function DefaultFairsharePct() As Double
            Dim row As DataRow = ReadCreditor(Creditor)
            Return DebtPlus.Utils.Nulls.DDbl(row("fairshare_pct_check"))
        End Function

        ''' <summary>
        ''' Process the ADD event
        ''' </summary>
        Private Sub SimpleButton_Add_Click(ByVal Sender As Object, ByVal e As EventArgs)
            If AllowUpdate() Then
                Dim CurrentTransaction As New Transaction(ctx)
                AddHandler CurrentTransaction.PropertyChanged, AddressOf TransactionPropertyChanged

                With CurrentTransaction
                    .BeginInit()
                    .creditor = Creditor
                    .creditor_type = DefaultCreditorType()
                    .fairshare_pct = DefaultFairsharePct()
                    .EndInit()
                End With
                TransactionsTable.Add(CurrentTransaction)
                GridControl1.RefreshDataSource()

                ' Find the new row in the grid
                GridView1.FocusedRowHandle = GridView1.GetDataSourceRowIndex(TransactionsTable.Count - 1)
            End If
        End Sub

        ''' <summary>
        ''' Process the DELETE event
        ''' </summary>
        Private Sub SimpleButton_Delete_Click(ByVal Sender As Object, ByVal e As EventArgs)

            Dim CurrentTransaction As Transaction = CType(GridView1.GetFocusedRow, Transaction)
            If CurrentTransaction IsNot Nothing Then
                If AllowUpdate() Then
                    CurrentTransaction.void = 1
                    GridControl1.RefreshDataSource()
                End If
            End If

            ' If there are no more transactions then clear the focused row
            Dim HasTransactions As Boolean = False
            For Each tran As Transaction In TransactionsTable
                If tran.void = 0 Then
                    HasTransactions = True
                End If
            Next

            ' If there are no transactions then there is no focused row and we can not delete it.
            If Not HasTransactions Then
                GridView1.FocusedRowHandle = -1
                SimpleButton_del.Enabled = False
            End If
        End Sub

        ''' <summary>
        ''' Define the transactions table
        ''' </summary>
        Private Sub ReadTransactions()
            Dim cn As SqlClient.SqlConnection = New SqlConnection(DebtPlus.LINQ.SQLInfoClass.getDefault().ConnectionString)
            Dim rd As SqlClient.SqlDataReader = Nothing
            Try
                cn.Open()
                Using cmd As SqlClient.SqlCommand = New SqlCommand
                    With cmd
                        .Connection = cn
                        .CommandText = "SELECT dbo.format_normal_name(pn.prefix,pn.first,pn.middle,pn.last,pn.suffix) as name, rcc.client, rcc.creditor, rcc.client_creditor, rcc.debit_amt, rcc.fairshare_amt, rcc.fairshare_pct, rcc.creditor_type, rcc.account_number FROM registers_client_creditor rcc with (nolock) left outer join people p with (nolock) on rcc.client = p.client and 1 = p.relation LEFT OUTER JOIN names pn WITH (NOLOCK) ON p.NameID = pn.Name WHERE trust_register = @trust_register AND rcc.tran_type in ('AD','MD','CM') AND rcc.creditor = @creditor"
                        .CommandType = CommandType.Text
                        .Parameters.Add("@trust_register", SqlDbType.Int).Value = TrustRegister
                        .Parameters.Add("@creditor", SqlDbType.VarChar).Value = Creditor
                        rd = .ExecuteReader(CommandBehavior.CloseConnection Or CommandBehavior.SingleResult)
                    End With
                End Using

                If rd IsNot Nothing Then
                    Do While rd.Read

                        ' Copy the results from the database into a list of transaction records.
                        Dim NewTransaction As New Transaction(ctx)
                        AddHandler NewTransaction.PropertyChanged, AddressOf TransactionPropertyChanged
                        With NewTransaction
                            .BeginInit()
                            For col As Int32 = 0 To rd.FieldCount - 1
                                If Not rd.IsDBNull(col) Then
                                    Dim FieldName As String = rd.GetName(col).ToLower()
                                    Dim pi As System.Reflection.PropertyInfo = GetType(Transaction).GetProperty(FieldName)
                                    If pi Is Nothing Then
                                        Debug.WriteLine(String.Format("Missing field property '{0}'", FieldName))
                                    Else
                                        pi.SetValue(NewTransaction, rd(col), Nothing)
                                    End If
                                End If
                            Next
                            .EndInit()
                        End With
                        TransactionsTable.Add(NewTransaction)
                    Loop
                End If

            Catch ex As SqlClient.SqlException
                DebtPlus.UI.Common.ErrorHandling.HandleErrors(ex)

            Finally
                If rd IsNot Nothing Then rd.Dispose()
                If cn IsNot Nothing Then cn.Dispose()
            End Try
        End Sub

        Public CheckChangeStatus As Int32
        Private Function AllowUpdate() As Boolean

            ' Generate an error if the operation is not acceptable
            If CheckChangeStatus = 1 Then
                If DebtPlus.Data.Forms.MessageBox.Show("Warning, changing a copy of a check is not advisable. Are you really sure that you want to do this?", "Are you sure", MessageBoxButtons.YesNo, MessageBoxIcon.Exclamation, MessageBoxDefaultButton.Button2) = DialogResult.Yes Then
                    CheckChangeStatus = 3
                Else
                    CheckChangeStatus = 2
                End If
            End If

            ' Generate the error if this is the case
            Return CheckChangeStatus <> 2
        End Function

        Private Function DebtView(ByVal ClientObject As Int32?) As DataView
            Dim answer As DataView = Nothing
            Dim Client As Int32 = ClientObject.GetValueOrDefault(-1)
            If Client >= 0 Then
                Dim tbl As DataTable = ctx.ds.Tables("client_creditor")
                If tbl IsNot Nothing Then
                    answer = New DataView(tbl, String.Format("[ClientId]={0:f0} AND creditor='{1}'", Client, Creditor), "client_creditor", DataViewRowState.CurrentRows)
                End If

                If answer Is Nothing OrElse answer.Count = 0 Then
                    Using cmd As SqlClient.SqlCommand = New SqlCommand
                        With cmd
                            .Connection = New SqlConnection(DebtPlus.LINQ.SQLInfoClass.getDefault().ConnectionString)
                            .CommandText = "SELECT cc.client_creditor, cc.client_creditor_balance, cc.client, cc.creditor, cc.creditor_name, cc.line_number, cc.priority, cc.drop_date, cc.drop_reason, cc.drop_reason_sent, cc.person, cc.client_name, cc.non_dmp_payment, cc.non_dmp_interest, cc.disbursement_factor, cc.date_disp_changed, cc.orig_dmp_payment, cc.months_delinquent, cc.start_date, cc.dmp_interest, cc.dmp_payout_interest, cc.last_stmt_date, cc.irs_form_on_file, cc.student_loan_release, cc.balance_verification_release, cc.last_payment_date_b4_dmp, cc.expected_payout_date, cc.rpps_client_type_indicator, cc.terms, cc.percent_balance, cc.client_creditor_proposal, cc.contact_name, cc.payments_this_creditor, cc.returns_this_creditor, cc.interest_this_creditor, cc.sched_payment, cc.proposal_prenote_date, cc.send_bal_verify, cc.verify_request_date, cc.balance_verify_date, cc.balance_verify_by, cc.first_payment, cc.last_payment, cc.account_number, cc.message, cc.hold_disbursements, cc.send_drop_notice, cc.last_communication, cc.reassigned_debt, cc.fairshare_pct_check, cc.fairshare_pct_eft, cc.prenote_date, cc.check_payments, cc.payment_rpps_mask, cc.payment_rpps_mask_timestamp, cc.rpps_mask, cc.rpps_mask_timestamp, cc.created_by, cc.date_created, b.orig_balance, b.orig_balance+b.orig_balance_adjustment+b.total_interest-b.total_payments as current_balance FROM client_creditor cc WITH (NOLOCK) LEFT OUTER JOIN client_creditor_balances b ON cc.client_creditor_balance = b.client_creditor_balance WHERE cc.client=@client AND cc.creditor = @creditor"
                            .CommandType = CommandType.Text
                            .Parameters.Add("@client", SqlDbType.Int).Value = Client
                            .Parameters.Add("@creditor", SqlDbType.VarChar, 10).Value = Creditor
                        End With

                        Dim da As New SqlClient.SqlDataAdapter(cmd)
                        Try
                            da.Fill(ctx.ds, "client_creditor")
                            tbl = ctx.ds.Tables("client_creditor")
                            If tbl.PrimaryKey.Count = 0 Then
                                With tbl
                                    .PrimaryKey = New DataColumn() {.Columns("client_creditor")}
                                End With
                            End If

                        Catch ex As SqlClient.SqlException
                            DebtPlus.UI.Common.ErrorHandling.HandleErrors(ex, "Error reading debts")
                        Finally
                            da.Dispose()
                        End Try
                    End Using
                End If

                If tbl IsNot Nothing Then
                    answer = New DataView(tbl, String.Format("[client]={0:f0} AND creditor='{1}'", Client, Creditor), "client_creditor", DataViewRowState.CurrentRows)
                End If
            End If

            Return answer
        End Function

        Private Sub ClientID1_EditValueChanged(ByVal sender As Object, ByVal e As EventArgs)
            ' Read the list of debts that match the client/creditor combination
            Dim vue As DataView = DebtView(ClientID1.EditValue)
            LookUpEdit_client_creditor.Properties.DataSource = vue
        End Sub

        Private Sub SimpleButton_Post_Click(ByVal sender As Object, ByVal e As EventArgs)

            ' Find the trust register for the new check.
            Dim trust_register As Int32 = PostManualDisbursement()
            If trust_register > 0 Then

                ' Determine the post processing for the check
                Select Case Convert.ToString(CType(ComboBoxEdit_disposition.SelectedItem, DebtPlus.Data.Controls.ComboboxItem).value)
                    Case "MANUAL"
                        Dim libry As New DebtPlus.Check.Print.Library.PrintChecksClass()
                        With libry
                            .RecordSingleCheck(trust_register)
                        End With

                    Case "QUEUE"
                        DebtPlus.Data.Forms.MessageBox.Show(String.Format("The manual disbursement is complete{0}{0}The check is left pending to be printed later", Environment.NewLine), "Operation completed", MessageBoxButtons.OK)

                    Case "PRINT"
                        Dim libry As New DebtPlus.Check.Print.Library.PrintChecksClass()
                        With libry
                            .PrintSingleCheck(trust_register)
                        End With
                    Case Else
                End Select

                ' Complete the processing for the form.
                RaiseCancelled()
            End If
        End Sub

        Private Function PostManualDisbursement() As Int32
            Dim cn As SqlClient.SqlConnection = New SqlConnection(DebtPlus.LINQ.SQLInfoClass.getDefault().ConnectionString)
            Dim txn As SqlClient.SqlTransaction = Nothing
            Dim current_cursor As Cursor = Cursor.Current
            Dim trust_register As Int32 = -1
            Dim creditor_register As Int32 = -1
            Dim Tran As Transaction = Nothing

            Try
                Cursor.Current = Cursors.WaitCursor
                cn.Open()
                txn = cn.BeginTransaction

                ' Create a disbursement batch and trust_register
                Dim disbursement_register As Int32 = NewManualDisbursement(cn, txn)
                trust_register = NewTrustRegister(cn, txn, Creditor)

                For Each Tran In TransactionsTable
                    If Tran.void = 0 AndAlso Tran.IsValid Then
                        Using cmd As SqlClient.SqlCommand = New SqlCommand
                            With cmd
                                .Connection = cn
                                .Transaction = txn
                                .CommandText = "xpr_disbursement_manual_detail"
                                .CommandType = CommandType.StoredProcedure

                                .Parameters.Add("@RETURN", SqlDbType.Int).Direction = ParameterDirection.ReturnValue
                                .Parameters.Add("@trust_register", SqlDbType.Int).Value = trust_register
                                .Parameters.Add("@disbursement_register", SqlDbType.Int).Value = disbursement_register
                                .Parameters.Add("@client_creditor", SqlDbType.Int).Value = Tran.client_creditor
                                .Parameters.Add("@amount", SqlDbType.Decimal).Value = Tran.debit_amt
                                .Parameters.Add("@fairshare_amt", SqlDbType.Decimal).Value = Tran.fairshare_amt
                                .Parameters.Add("@creditor_type", SqlDbType.VarChar, 10).Value = Tran.Recorded_creditor_type

                                .ExecuteNonQuery()
                                If Convert.ToInt32(.Parameters(0).Value) <= 0 Then
                                    Throw New DataException("Error creating disbursement detail")
                                End If
                            End With
                        End Using
                    End If
                Next

                ' Close out the check
                Using cmd As SqlClient.SqlCommand = New SqlCommand
                    With cmd
                        .Connection = cn
                        .Transaction = txn
                        .CommandText = "xpr_disbursement_manual_close"
                        .CommandType = CommandType.StoredProcedure
                        .Parameters.Add("@RETURN", SqlDbType.Int).Direction = ParameterDirection.ReturnValue
                        .Parameters.Add("@disbursement_register", SqlDbType.Int).Value = disbursement_register
                        .Parameters.Add("@trust_register", SqlDbType.Int).Value = trust_register
                        .Parameters.Add("@message", SqlDbType.VarChar, 80).Value = ComboBoxEdit_reason.Text.Trim()
                        .ExecuteNonQuery()

                        creditor_register = Convert.ToInt32(.Parameters(0).Value)
                        If creditor_register <= 0 Then
                            Throw New DataException("Error creating disbursement detail")
                        End If
                    End With
                End Using

                txn.Commit()
                txn.Dispose()
                txn = Nothing

            Catch ex As DataException
                trust_register = -1

            Catch ex As SqlClient.SqlException
                Dim Msg As String = ex.Message
                Dim Msgs() As String = Msg.Replace(ControlChars.Cr.ToString(), "").Split(ControlChars.Lf)
                Dim FirstMsg As String = Msgs(0)

                DebtPlus.Data.Forms.MessageBox.Show(FirstMsg, "Error disbursing money", MessageBoxButtons.OK, MessageBoxIcon.Error)
                trust_register = -1

            Finally
                If txn IsNot Nothing Then
                    Try
                        txn.Rollback()
                    Catch exRollback As Exception
                    End Try
                    txn.Dispose()
                    txn = Nothing
                End If

                If cn IsNot Nothing Then cn.Dispose()
                Cursor.Current = current_cursor
            End Try

            ' Return the pointer to the new check.
            Return trust_register
        End Function

        Private Function NewManualDisbursement(ByVal cn As SqlClient.SqlConnection, ByVal txn As SqlClient.SqlTransaction) As Int32
            Dim answer As Int32 = -1

            Using cmd As SqlClient.SqlCommand = New SqlCommand
                With cmd
                    .Connection = cn
                    .Transaction = txn
                    .CommandText = "xpr_disbursement_manual_create"
                    .CommandType = CommandType.StoredProcedure
                    .Parameters.Add("RETURN", SqlDbType.Int).Direction = ParameterDirection.ReturnValue
                    .Parameters.Add("@type", SqlDbType.VarChar, 10).Value = "MD"
                    .ExecuteNonQuery()
                    answer = Convert.ToInt32(.Parameters(0).Value)
                End With
            End Using
            Return answer
        End Function

        Private Function NewTrustRegister(ByVal cn As SqlClient.SqlConnection, ByVal txn As SqlClient.SqlTransaction, ByVal Creditor As String) As Int32
            Dim answer As Int32 = -1
            Dim bank As Int32

            ' Find the bank number used for the creditor checks
            Dim row As DataRow = ctx.ds.Tables("creditors").Rows.Find(Creditor)
            If row IsNot Nothing AndAlso row("check_bank") IsNot DBNull.Value Then
                bank = Convert.ToInt32(row("check_bank"))
            Else
                bank = -1
            End If

            ' Create the item in the trust register
            Using cmd As SqlClient.SqlCommand = New SqlCommand
                With cmd
                    .Connection = cn
                    .Transaction = txn
                    .CommandText = "INSERT INTO registers_trust(tran_type,creditor,bank,amount,cleared) VALUES (@tran_type,@creditor,@bank,0,'P'); SELECT scope_identity() AS trust_register"
                    .CommandType = CommandType.Text
                    .Parameters.Add("@tran_type", SqlDbType.VarChar, 10).Value = "MD"
                    .Parameters.Add("@creditor", SqlDbType.VarChar, 10).Value = Creditor
                    .Parameters.Add("@bank", SqlDbType.Int).Value = bank

                    Dim objAnswer As Object = .ExecuteScalar
                    If objAnswer IsNot Nothing AndAlso objAnswer IsNot DBNull.Value Then answer = Convert.ToInt32(objAnswer)
                End With
            End Using
            Return answer
        End Function

        Private Sub TransactionPropertyChanged(ByVal Sender As Object, ByVal e As System.ComponentModel.PropertyChangedEventArgs)

            ' Refresh the row contining the field that was last changed.
            Dim CurrentTransaction As Transaction = CType(Sender, Transaction)
            Dim RowInTable As Int32 = TransactionsTable.IndexOf(CurrentTransaction)
            If RowInTable >= 0 Then
                Dim RowHandle As Int32 = GridView1.GetRowHandle(RowInTable)
                If RowHandle >= 0 Then
                    GridView1.RefreshRow(RowHandle)
                    GridView1.UpdateTotalSummary()
                End If
            End If

            ' Enable the post button if possible
            EnablePostButton()
        End Sub

        Private Sub EnablePostButton()

            ' Determine if there are any errors and there is at least one Transaction
            Dim HasError As Boolean = False
            Dim HasTransactions As Boolean = False
            For Each Tran As Transaction In TransactionsTable
                If Tran.void = 0 Then
                    HasTransactions = True
                End If

                If Not Tran.IsValid Then
                    HasError = True
                End If
            Next

            ' We can post if there is a transaction and there are no errors
            SimpleButton_Post.Enabled = HasTransactions AndAlso (Not HasError)
        End Sub
    End Class
End Namespace
