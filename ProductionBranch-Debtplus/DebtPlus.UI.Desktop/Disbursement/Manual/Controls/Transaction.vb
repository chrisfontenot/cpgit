Imports DebtPlus.UI.Common
Imports DebtPlus.Utils
Imports System.Data.SqlClient
Imports DebtPlus.Utils.Format

Namespace Disbursement.Manual.Controls

    Public Class Transaction
        Inherits Object
        Implements System.ComponentModel.INotifyPropertyChanged
        Implements System.ComponentModel.ISupportInitialize
        Implements System.ComponentModel.IEditableObject

        Private Structure StorageTemplate
            Dim client_creditor_register As Int32
            Dim client As Int32
            Dim client_creditor As Int32
            Dim name As String
            Dim debit_amt As Decimal
            Dim creditor As String
            Dim account_number As String
            Dim fairshare_amt As Decimal
            Dim fairshare_pct As Double
            Dim creditor_type As String
            Dim void As Int32
        End Structure

        Private privateStorage As New StorageTemplate
        Private privateUndoStorage As New StorageTemplate
        Private InEdit As Boolean
        Private InInit As Boolean
        Private ctx As Context = Nothing

        Public Sub New(ByVal ctx As Context)
            MyBase.New()
            Me.ctx = ctx
        End Sub

        Public Event PropertyChanged(ByVal sender As Object, ByVal e As System.ComponentModel.PropertyChangedEventArgs) Implements System.ComponentModel.INotifyPropertyChanged.PropertyChanged
        Protected Sub DoSetProperty(ByVal PropertyName As String)
            If Not InInit Then
                RaiseEvent PropertyChanged(Me, New System.ComponentModel.PropertyChangedEventArgs(PropertyName))
            End If
        End Sub

        Public Sub BeginInit() Implements System.ComponentModel.ISupportInitialize.BeginInit
            InInit = True
        End Sub

        Public Sub EndInit() Implements System.ComponentModel.ISupportInitialize.EndInit
            InInit = False
        End Sub

        Public Property void() As Int32
            Get
                Return privateStorage.void
            End Get
            Set(ByVal value As Int32)
                If value <> privateStorage.void Then
                    privateStorage.void = value
                    DoSetProperty("void")
                End If
            End Set
        End Property

        Public ReadOnly Property IsValid() As Boolean
            Get
                Dim answer As Boolean = False

                Do
                    If String.IsNullOrEmpty(creditor) Then Exit Do
                    If client <= 0 Then Exit Do
                    If client_creditor <= 0 Then Exit Do
                    If debit_amt <= 0D Then Exit Do
                    If fairshare_pct >= 1.0 Then Exit Do
                    answer = True
                    Exit Do
                Loop

                Return answer
            End Get
        End Property

        Public Property client_creditor_register() As Int32
            Get
                Return privateStorage.client_creditor_register
            End Get
            Set(ByVal value As Int32)
                If value <> privateStorage.client_creditor_register Then
                    privateStorage.client_creditor_register = value
                    DoSetProperty("client_creditor_register")
                End If
            End Set
        End Property

        Public Property client() As Int32
            Get
                Return privateStorage.client
            End Get
            Set(ByVal value As Int32)
                If value <> privateStorage.client Then
                    privateStorage.client = value
                    DoSetProperty("client")

                    ' Set the client name when we have a ClientId
                    If Not InInit Then
                        ReadClientName()
                    End If
                End If
            End Set
        End Property

        Public Property client_creditor() As Int32
            Get
                Return privateStorage.client_creditor
            End Get
            Set(ByVal value As Int32)
                If value <> privateStorage.client_creditor Then
                    privateStorage.client_creditor = value
                    DoSetProperty("client_creditor")

                    ' Change the account number if needed
                    If Not InInit Then
                        Const TableName As String = "client_creditor"
                        Dim tbl As DataTable = ctx.ds.Tables(TableName)
                        If tbl IsNot Nothing Then
                            Dim row As DataRow = tbl.Rows.Find(client_creditor)
                            If row IsNot Nothing Then
                                account_number = DebtPlus.Utils.Nulls.DStr(row("account_number"))
                            End If
                        End If
                    End If
                End If
            End Set
        End Property

        Public Property name() As String
            Get
                Return privateStorage.name
            End Get
            Set(ByVal value As String)
                If name <> value Then
                    privateStorage.name = value
                    DoSetProperty("name")
                End If
            End Set
        End Property

        Public Property debit_amt() As Decimal
            Get
                Return privateStorage.debit_amt
            End Get
            Set(ByVal value As Decimal)
                If value <> privateStorage.debit_amt Then

                    If Not InInit AndAlso value <= 0D Then
                        Throw New DataException("The amount must be greater than $0.00")
                    End If

                    privateStorage.debit_amt = value
                    DoSetProperty("debit_amt")
                    RecalculateFairshareAmt()
                End If
            End Set
        End Property

        Public Property creditor() As String
            Get
                Return privateStorage.creditor
            End Get
            Set(ByVal value As String)
                If value <> privateStorage.creditor Then
                    privateStorage.creditor = value
                    DoSetProperty("creditor")
                End If
            End Set
        End Property

        Public Property account_number() As String
            Get
                Return privateStorage.account_number
            End Get
            Set(ByVal value As String)
                If value <> privateStorage.account_number Then
                    privateStorage.account_number = value
                    DoSetProperty("account_number")
                End If
            End Set
        End Property

        Public Property fairshare_amt() As Decimal
            Get
                Return privateStorage.fairshare_amt
            End Get
            Set(ByVal value As Decimal)
                If value <> privateStorage.fairshare_amt Then
                    privateStorage.fairshare_amt = value
                    DoSetProperty("fairshare_amt")
                End If
            End Set
        End Property

        Public Property fairshare_pct() As Double
            Get
                Return privateStorage.fairshare_pct
            End Get
            Set(ByVal value As Double)
                If value <> privateStorage.fairshare_pct Then
                    privateStorage.fairshare_pct = value
                    DoSetProperty("fairshare_pct")
                    RecalculateFairshareAmt()
                End If
            End Set
        End Property

        Public ReadOnly Property Recorded_farishare_pct() As Double
            Get
                If creditor_type = "N" Then Return 0.0
                Return fairshare_pct
            End Get
        End Property

        Public ReadOnly Property deducted() As Decimal
            Get
                If creditor_type = "D" Then
                    Return fairshare_amt
                End If
                Return 0D
            End Get
        End Property

        Public ReadOnly Property billed() As Decimal
            Get
                If creditor_type = "D" OrElse creditor_type = "N" Then
                    Return 0D
                End If
                Return fairshare_amt
            End Get
        End Property

        Public ReadOnly Property net_amount() As Decimal
            Get
                Return debit_amt - deducted
            End Get
        End Property

        Public Property creditor_type() As String
            Get
                Return privateStorage.creditor_type
            End Get
            Set(ByVal value As String)
                If value <> privateStorage.creditor_type Then
                    privateStorage.creditor_type = value
                    DoSetProperty("creditor_type")
                    RecalculateFairshareAmt()
                End If
            End Set
        End Property

        Public ReadOnly Property Recorded_creditor_type() As String
            Get
                If creditor_type <> "N" AndAlso fairshare_pct <= 0.0 Then Return "N"
                Return creditor_type
            End Get
        End Property

        Public Sub BeginEdit() Implements System.ComponentModel.IEditableObject.BeginEdit
            privateUndoStorage = privateStorage
            InEdit = True
        End Sub

        Public Sub CancelEdit() Implements System.ComponentModel.IEditableObject.CancelEdit
            If InEdit Then
                privateStorage = privateUndoStorage
                InEdit = False
            End If
        End Sub

        Public Sub EndEdit() Implements System.ComponentModel.IEditableObject.EndEdit
            InEdit = False
        End Sub

        Public Sub RecalculateFairshareAmt()
            If Not InInit Then
                If creditor_type = "N" Then
                    fairshare_amt = 0D
                Else
                    fairshare_amt = Math.Truncate(Convert.ToDecimal(Convert.ToDouble(debit_amt) * fairshare_pct), 2)
                End If
            End If
        End Sub

        Public Sub ReadClientName()
            Dim newName As String = String.Empty
            If client > 0 Then
                Dim nameID As Int32 = -1
                Dim row As DataRow = ReadApplicant()
                If row IsNot Nothing Then
                    If row("NameID") IsNot Nothing AndAlso row("NameID") IsNot DBNull.Value Then
                        nameID = Convert.ToInt32(row("NameID"), System.Globalization.CultureInfo.InvariantCulture)
                    End If
                End If

                If nameID > 0 Then
                    row = ReadName(nameID)
                    If row IsNot Nothing Then
                        newName = DebtPlus.LINQ.Name.FormatNormalName(row("prefix"), row("first"), row("middle"), row("last"), row("suffix"))
                    End If
                End If
            End If
            name = newName
        End Sub

        Public Function ReadName(ByVal NameID As Int32) As DataRow
            Const tableName As String = "names"
            Dim row As DataRow

            ' If there is a table then look for the row. If found, return it.
            Dim tbl As DataTable = ctx.ds.Tables(tableName)
            If tbl IsNot Nothing Then
                row = tbl.Rows.Find(NameID)
                If row IsNot Nothing Then
                    Return row
                End If
            End If

            ' Read the row from the database
            Dim cn As SqlClient.SqlConnection = New SqlConnection(DebtPlus.LINQ.SQLInfoClass.getDefault().ConnectionString)
            Try
                cn.Open()
                Using cmd As SqlClient.SqlCommand = New SqlCommand
                    With cmd
                        .Connection = cn
                        .CommandText = "SELECT [name],[prefix],[first],[middle],[last],[suffix] FROM [names] WHERE [name]=@name"
                        .CommandType = CommandType.Text
                        .Parameters.Add("@name", SqlDbType.Int).Value = NameID
                    End With

                    Using da As New SqlClient.SqlDataAdapter(cmd)
                        da.Fill(ctx.ds, tableName)
                    End Using

                    tbl = ctx.ds.Tables(tableName)
                    With tbl
                        If .PrimaryKey.GetUpperBound(0) < 0 Then
                            .PrimaryKey = New DataColumn() {.Columns("name")}
                        End If
                    End With
                End Using

            Catch ex As SqlClient.SqlException
                DebtPlus.UI.Common.ErrorHandling.HandleErrors(ex)

            Finally
                If cn IsNot Nothing Then cn.Dispose()
            End Try

            ' Now, find the row. If the row is still not there then there is no name.
            ' If there is no name then record the fact that the name is blank so that we don't do this again.
            row = tbl.Rows.Find(NameID)
            If row Is Nothing Then
                row = tbl.NewRow()
                row("name") = NameID
                tbl.Rows.Add(row)
                row.AcceptChanges() ' Prevent the row from being written should we mistakenly update the table.
            End If

            Return row
        End Function

        Public Function ReadApplicant() As DataRow
            Const TableName As String = "people"
            Dim rows() As DataRow

            ' If there is a table then look for the row. If found, return it.
            Dim tbl As DataTable = ctx.ds.Tables(TableName)
            If tbl IsNot Nothing Then
                rows = tbl.Select(String.Format("[client]={0:f0} AND [relation]=1", client), String.Empty)
                If rows.GetUpperBound(0) >= 0 Then
                    Return rows(0)
                End If
            End If

            ' Read the row from the database
            Dim cn As SqlClient.SqlConnection = New SqlConnection(DebtPlus.LINQ.SQLInfoClass.getDefault().ConnectionString)
            Try
                cn.Open()
                Using cmd As SqlClient.SqlCommand = New SqlCommand
                    With cmd
                        .Connection = cn
                        .CommandText = "SELECT [person],[client],[relation],[NameID] FROM [people] WHERE [client]=@client AND [relation]=1"
                        .CommandType = CommandType.Text
                        .Parameters.Add("@client", SqlDbType.Int).Value = client
                    End With

                    Using da As New SqlClient.SqlDataAdapter(cmd)
                        da.Fill(ctx.ds, TableName)
                    End Using

                    tbl = ctx.ds.Tables(TableName)
                    With tbl
                        If .PrimaryKey.GetUpperBound(0) < 0 Then
                            .PrimaryKey = New DataColumn() {.Columns("person")}
                        End If

                        With .Columns("person")
                            If Not .AutoIncrement Then
                                .AutoIncrement = True
                                .AutoIncrementSeed = -1
                                .AutoIncrementStep = -1
                            End If
                        End With
                    End With
                End Using

            Catch ex As SqlClient.SqlException
                DebtPlus.UI.Common.ErrorHandling.HandleErrors(ex)

            Finally
                If cn IsNot Nothing Then cn.Dispose()
            End Try

            ' Now, find the row. If the row is still not there then there is no name.
            ' If there is no name then record the fact that the name is blank so that we don't do this again.
            rows = tbl.Select(String.Format("[client]={0:f0} AND [relation]=1", client), String.Empty)
            If rows.GetUpperBound(0) >= 0 Then
                Return rows(0)
            End If

            ' Add a blank row to the table so that we don't look again for this person.
            Dim row As DataRow = tbl.NewRow
            row("client") = client
            row("relation") = 1
            row("NameID") = DBNull.Value
            tbl.Rows.Add(row)
            row.AcceptChanges()

            Return row
        End Function
    End Class

End Namespace
