#Region "Copyright 2000-2012 DebtPlus, L.L.C."
'{*******************************************************************}
'{                                                                   }
'{       DebtPlus Debt Management System                             }
'{                                                                   }
'{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
'{       ALL RIGHTS RESERVED                                         }
'{                                                                   }
'{   The entire contents of this file is protected by U.S. and       }
'{   International Copyright Laws. Unauthorized reproduction,        }
'{   reverse-engineering, and distribution of all or any portion of  }
'{   the code contained in this file is strictly prohibited and may  }
'{   result in severe civil and criminal penalties and will be       }
'{   prosecuted to the maximum extent possible under the law.        }
'{                                                                   }
'{   RESTRICTIONS                                                    }
'{                                                                   }
'{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
'{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
'{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
'{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
'{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
'{                                                                   }
'{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
'{   ADDITIONAL RESTRICTIONS.                                        }
'{                                                                   }
'{*******************************************************************}
#End Region
Imports DebtPlus.UI.Common
Imports System.Data.SqlClient

Namespace Disbursement.Manual.Controls
    Friend Class BaseControl

        Protected ctx As Context = Nothing

        Public Sub New(ByVal ctx As Context)
            Me.ctx = ctx
            InitializeComponent()
        End Sub

        ''' <summary>
        ''' Called to do the equivalent of "one-time" initialization
        ''' when the program is stablized. Equivalent to the LOAD event
        ''' </summary>
        Public Overridable Sub ReadForm()
        End Sub

        ''' <summary>
        ''' Define the creditors table
        ''' </summary>
        Protected Function ReadCreditor(ByVal creditor As String) As System.Data.DataRow
            Dim answer As System.Data.DataRow = Nothing

            ' If the table is not defined then read it from the database.
            Dim tbl As System.Data.DataTable = ctx.ds.Tables("creditors")
            If tbl IsNot Nothing Then answer = tbl.Rows.Find(creditor)

            If answer Is Nothing Then
                Using cmd As SqlClient.SqlCommand = New SqlCommand
                    With cmd
                        .Connection = New SqlConnection(DebtPlus.LINQ.SQLInfoClass.getDefault().ConnectionString)
                        .CommandText = "SELECT cr.creditor, cr.creditor_id, cr.type, cr.sic, cr.creditor_name, cr.comment, cr.division, cr.creditor_class, cr.voucher_spacing, cr.mail_priority, cr.payment_balance, cr.prohibit_use, cr.full_disclosure, cr.suppress_invoice, cr.proposal_budget_info, cr.proposal_income_info, cr.contrib_cycle, cr.contrib_bill_month, cr.pledge_amt, cr.pledge_cycle, cr.pledge_bill_month, cr.min_accept_amt, cr.min_accept_pct, cr.min_accept_per_bill, cr.lowest_apr_pct, cr.medium_apr_pct, cr.highest_apr_pct, cr.medium_apr_amt, cr.highest_apr_amt, cr.max_clients_per_check, cr.max_amt_per_check, cr.max_fairshare_per_debt, cr.chks_per_invoice, cr.returned_mail, cr.po_number, cr.usual_priority, cr.percent_balance, cr.distrib_mtd, cr.distrib_ytd, cr.contrib_mtd_billed, cr.contrib_ytd_billed, cr.contrib_mtd_received, cr.contrib_ytd_received, cr.first_payment, cr.last_payment, cr.acceptance_days, cr.check_payments, cr.check_bank, cr.creditor_contribution_pct, cr.date_created, cr.created_by, pct.creditor_type_check, pct.fairshare_pct_check FROM creditors cr WITH (NOLOCK) LEFT OUTER JOIN creditor_contribution_pcts pct ON cr.creditor_contribution_pct = pct.creditor_contribution_pct WHERE cr.creditor = @creditor"
                        .CommandType = CommandType.Text
                        .Parameters.Add("@creditor", SqlDbType.VarChar, 10).Value = creditor
                    End With

                    Dim da As New SqlClient.SqlDataAdapter(cmd)
                    Try
                        da.Fill(ctx.ds, "creditors")
                        tbl = ctx.ds.Tables("creditors")
                        If tbl.PrimaryKey.Count = 0 Then
                            With tbl
                                .PrimaryKey = New System.Data.DataColumn() {.Columns("creditor")}
                            End With
                        End If

                        answer = tbl.Rows.Find(creditor)

                    Catch ex As SqlClient.SqlException
                        DebtPlus.UI.Common.ErrorHandling.HandleErrors(ex, "Error reading creditors")
                    Finally
                        da.Dispose()
                    End Try
                End Using
            End If

            Return answer
        End Function

        ''' <summary>
        ''' Define the clients table
        ''' </summary>
        Protected Function ReadClient(ByVal client As System.Int32) As System.Data.DataRow
            Dim answer As System.Data.DataRow = Nothing

            ' If the table is not defined then read it from the database.
            Dim tbl As System.Data.DataTable = ctx.ds.Tables("clients")
            If tbl IsNot Nothing Then answer = tbl.Rows.Find(client)

            If answer Is Nothing Then
                Using cmd As SqlClient.SqlCommand = New SqlCommand
                    With cmd
                        .Connection = New SqlConnection(DebtPlus.LINQ.SQLInfoClass.getDefault().ConnectionString)
                        .CommandText = "SELECT c.client, c.salutation, c.AddressID, c.HomeTelephoneID, c.MsgTelephoneID, c.county, c.region, c.active_status, c.active_status_date, c.dmp_status_date, c.client_status, c.client_status_date, c.disbursement_date, c.mail_error_date, c.start_date, c.restart_date, c.drop_date, c.drop_reason, c.drop_reason_other, c.referred_by, c.cause_fin_problem1, c.cause_fin_problem2, c.cause_fin_problem3, c.cause_fin_problem4, c.office, c.counselor, c.csr, c.hold_disbursements, c.personal_checks, c.ach_active, c.stack_proration, c.mortgage_problems, c.intake_agreement, c.ElectronicCorrespondence, c.ElectronicStatements, c.bankruptcy_class, c.satisfaction_score, c.fed_tax_owed, c.state_tax_owed, c.local_tax_owed, c.fed_tax_months, c.state_tax_months, c.local_tax_months, c.held_in_trust, c.deposit_in_trust, c.reserved_in_trust, c.reserved_in_trust_cutoff, c.marital_status, c.language, c.dependents, c.household, c.method_first_contact, c.preferred_contact, c.config_fee, c.first_appt, c.first_kept_appt, c.first_resched_appt, c.program_months, c.first_deposit_date, c.last_deposit_date, c.last_deposit_amount, c.payout_total_debt, c.payout_months_to_payout, c.payout_total_fees, c.payout_total_interest, c.payout_total_payments, c.payout_deposit_amount, c.payout_monthly_fee, c.payout_cushion_amount, c.payout_termination_date, c.created_by, c.date_created, c.client_guid, c.initial_program, c.current_program, dbo.format_normal_name(pn.prefix,pn.first,pn.middle,pn.last,pn.suffix) as name FROM clients c WITH (NOLOCK) LEFT OUTER JOIN people p ON c.client = p.client AND 1 = p.relation LEFT OUTER JOIN names pn WITH (NOLOCK) ON p.NameID = pn.Name WHERE c.client = @client"
                        .CommandType = CommandType.Text
                        .Parameters.Add("@client", SqlDbType.Int).Value = client
                    End With

                    Dim da As New SqlClient.SqlDataAdapter(cmd)
                    Try
                        da.Fill(ctx.ds, "clients")
                        tbl = ctx.ds.Tables("clients")
                        If tbl.PrimaryKey.Count = 0 Then
                            With tbl
                                .PrimaryKey = New System.Data.DataColumn() {.Columns("client")}
                            End With
                        End If

                        answer = tbl.Rows.Find(client)

                    Catch ex As SqlClient.SqlException
                        DebtPlus.UI.Common.ErrorHandling.HandleErrors(ex, "Error reading clients")
                    Finally
                        da.Dispose()
                    End Try
                End Using
            End If

            Return answer
        End Function

        Protected Function CreditorTypes() As System.Data.DataTable
            Const TableName As String = "creditor_types"
            Dim tbl As System.Data.DataTable = ctx.ds.Tables(TableName)

            If tbl Is Nothing Then
                tbl = New System.Data.DataTable("creditor_types")
                With tbl
                    .Columns.Add("creditor_type", GetType(String))
                    .Columns.Add("description", GetType(String))
                    .PrimaryKey = New System.Data.DataColumn() {.Columns(0)}
                    .Rows.Add(New Object() {"N", "None"})
                    .Rows.Add(New Object() {"D", "Deduct"})
                    .Rows.Add(New Object() {"M", "Bill Monthly"})
                    .Rows.Add(New Object() {"Q", "Bill Quarterly"})
                    .Rows.Add(New Object() {"S", "Bill Semi-Annually"})
                    .Rows.Add(New Object() {"A", "Bill Annually"})
                End With
                ctx.ds.Tables.Add(tbl)
            End If

            Return tbl
        End Function
    End Class
End Namespace
