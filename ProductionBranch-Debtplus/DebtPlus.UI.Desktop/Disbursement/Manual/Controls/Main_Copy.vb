#Region "Copyright 2000-2012 DebtPlus, L.L.C."
'{*******************************************************************}
'{                                                                   }
'{       DebtPlus Debt Management System                             }
'{                                                                   }
'{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
'{       ALL RIGHTS RESERVED                                         }
'{                                                                   }
'{   The entire contents of this file is protected by U.S. and       }
'{   International Copyright Laws. Unauthorized reproduction,        }
'{   reverse-engineering, and distribution of all or any portion of  }
'{   the code contained in this file is strictly prohibited and may  }
'{   result in severe civil and criminal penalties and will be       }
'{   prosecuted to the maximum extent possible under the law.        }
'{                                                                   }
'{   RESTRICTIONS                                                    }
'{                                                                   }
'{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
'{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
'{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
'{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
'{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
'{                                                                   }
'{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
'{   ADDITIONAL RESTRICTIONS.                                        }
'{                                                                   }
'{*******************************************************************}
#End Region
Imports DebtPlus.UI.Common
Imports System.Data.SqlClient
Imports System.Windows.Forms

Namespace Disbursement.Manual.Controls
    Friend Class Main_Copy

        ''' <summary>
        ''' Creditor ID used for the form
        ''' </summary>
        Private Property Creditor() As String
            Get
                Return CType(Me.FindForm, Form_ManualDisbursement).Creditor
            End Get
            Set(ByVal value As String)
                CType(Me.FindForm, Form_ManualDisbursement).Creditor = value
            End Set
        End Property

        ''' <summary>
        ''' Trust Register used for the form
        ''' </summary>
        Private Property TrustRegister() As System.Int32
            Get
                Return CType(Me.FindForm, Form_ManualDisbursement).TrustRegister
            End Get
            Set(ByVal value As System.Int32)
                CType(Me.FindForm, Form_ManualDisbursement).TrustRegister = value
            End Set
        End Property

        ''' <summary>
        ''' Generate the CANCEL event to the caller
        ''' </summary>
        Public Event Cancelled As EventHandler
        Protected Sub OnCancelled(ByVal e As System.EventArgs)
            RaiseEvent Cancelled(Me, e)
        End Sub
        Protected Sub RaiseCancelled()
            OnCancelled(EventArgs.Empty)
        End Sub

        ''' <summary>
        ''' Generate the OK event to the caller
        ''' </summary>
        Public Event Completed As EventHandler
        Protected Sub OnCompleted(ByVal e As System.EventArgs)
            RaiseEvent Completed(Me, e)
        End Sub
        Protected Sub RaiseCompleted()
            OnCompleted(EventArgs.Empty)
        End Sub

        ''' <summary>
        ''' Create the instance of our control
        ''' </summary>
        Public Sub New(ByVal ctx As Context)
            MyBase.new(ctx)
            InitializeComponent()
            AddHandler TextEdit1.Validating, AddressOf TextEdit1_Validating
            AddHandler CreditorID1.Validating, AddressOf CreditorID1_Validating
            AddHandler SimpleButton_ok.Click, AddressOf SimpleButton_OK_Click
            AddHandler SimpleButton_cancel.Click, AddressOf SimpleButton_Cancel_Click
        End Sub

        ''' <summary>
        ''' Handle the OK button
        ''' </summary>
        Private Sub SimpleButton_OK_Click(ByVal Sender As Object, ByVal e As System.EventArgs)
            Creditor = CreditorID1.EditValue
            If TextEdit1.Text = String.Empty Then TrustRegister = -1
            RaiseCompleted()
        End Sub

        ''' <summary>
        ''' Handle the CANCEL button
        ''' </summary>
        Private Sub SimpleButton_Cancel_Click(ByVal Sender As Object, ByVal e As System.EventArgs)
            RaiseCancelled()
        End Sub

        ''' <summary>
        ''' Look for errors on the check number entry
        ''' </summary>
        Private Sub TextEdit1_Validating(ByVal sender As Object, ByVal e As System.ComponentModel.CancelEventArgs)
            Dim ErrorText As String = String.Empty

            ' Find the new check number
            Dim NewCheckNumber As String = String.Empty
            If TextEdit1.EditValue IsNot Nothing AndAlso TextEdit1.EditValue IsNot System.DBNull.Value Then NewCheckNumber = Convert.ToString(TextEdit1.EditValue)
            If NewCheckNumber <> String.Empty Then

                ' Supress the creditor if a check is given. It will be defined later when it is valid.
                CreditorID1.EditValue = Nothing

                ' The check number was specified. Look it up in the database.
                Dim cn As SqlClient.SqlConnection = New SqlConnection(DebtPlus.LINQ.SQLInfoClass.getDefault().ConnectionString)
                Dim rd As SqlClient.SqlDataReader = Nothing
                Dim current_cursor As System.Windows.Forms.Cursor = System.Windows.Forms.Cursor.Current
                Try
                    System.Windows.Forms.Cursor.Current = Cursors.WaitCursor
                    cn.Open()
                    Using cmd As SqlClient.SqlCommand = New SqlCommand
                        With cmd
                            .Connection = cn
                            .CommandText = "SELECT TOP 1 tr.tran_type, tr.trust_register, tr.creditor FROM registers_trust tr WITH (NOLOCK) INNER JOIN banks b ON tr.bank = b.bank WHERE tr.checknum = @checknum AND tr.cleared = 'V' AND tr.tran_type in ('AD','MD','CR','CM') AND b.type = 'C' ORDER BY tr.date_created desc"
                            .CommandType = CommandType.Text
                            .Parameters.Add("@checknum", SqlDbType.VarChar, 50).Value = NewCheckNumber
                            rd = .ExecuteReader(CommandBehavior.CloseConnection Or CommandBehavior.SingleRow)
                        End With
                    End Using

                    If rd Is Nothing OrElse Not rd.Read Then
                        ErrorText = "check is not VOIDED"
                    Else
                        Dim tran_type As String = String.Empty
                        Dim trust_register As System.Int32 = -1
                        Dim creditor As String = String.Empty

                        If Not rd.IsDBNull(0) Then tran_type = Convert.ToString(rd.GetValue(0))
                        If Not rd.IsDBNull(1) Then trust_register = Convert.ToInt32(rd.GetValue(1))
                        If Not rd.IsDBNull(2) Then creditor = Convert.ToString(rd.GetValue(2))

                        If tran_type = "CR" Then
                            ErrorText = "do not use a client REFUND check"
                        Else
                            TrustRegister = trust_register
                            Me.Creditor = creditor
                            CreditorID1.EditValue = creditor
                        End If
                    End If

                Catch ex As SqlClient.SqlException
                    DebtPlus.UI.Common.ErrorHandling.HandleErrors(ex, "Error reading trust register")

                Finally
                    If rd IsNot Nothing Then rd.Dispose()
                    If cn IsNot Nothing Then cn.Dispose()
                    System.Windows.Forms.Cursor.Current = current_cursor
                End Try
            End If

            ' Set the error condition
            TextEdit1.ErrorText = ErrorText
            SimpleButton_ok.Enabled = Me.Creditor <> String.Empty AndAlso ErrorText = String.Empty
        End Sub

        Private Sub CreditorID1_Validating(ByVal sender As Object, ByVal e As System.ComponentModel.CancelEventArgs)
            Dim ErrorText As String = String.Empty

            ' Clear the creditor information
            Me.Creditor = String.Empty

            ' Find the new check number
            Dim NewCreditor As String = String.Empty
            If CreditorID1.EditValue IsNot Nothing AndAlso CreditorID1.EditValue IsNot System.DBNull.Value Then NewCreditor = Convert.ToString(CreditorID1.EditValue)
            If NewCreditor <> String.Empty Then
                Dim row As System.Data.DataRow = ReadCreditor(NewCreditor)
                If row IsNot Nothing Then
                    Me.Creditor = NewCreditor
                End If
            End If

            ' If there is no creditor then reject the entry
            If Me.Creditor = String.Empty Then ErrorText = "Invalid Creditor"

            ' Set the error condition
            CreditorID1.ErrorText = ErrorText
            SimpleButton_ok.Enabled = Me.Creditor <> String.Empty AndAlso ErrorText = String.Empty
        End Sub
    End Class
End Namespace
