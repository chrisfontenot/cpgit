﻿
Imports DebtPlus.UI.Creditor.Widgets.Controls
Imports DebtPlus.UI.Creditor.Widgets

Namespace Disbursement.Manual.Controls
    <Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
    Partial Class Main_Copy
        Inherits BaseControl

        'UserControl overrides dispose to clean up the component list.
        <System.Diagnostics.DebuggerNonUserCode()> _
        Protected Overrides Sub Dispose(ByVal disposing As Boolean)
            Try
                If disposing AndAlso components IsNot Nothing Then
                    components.Dispose()
                End If
            Finally
                MyBase.Dispose(disposing)
            End Try
        End Sub

        'Required by the Windows Form Designer
        Private components As System.ComponentModel.IContainer

        'NOTE: The following procedure is required by the Windows Form Designer
        'It can be modified using the Windows Form Designer.  
        'Do not modify it using the code editor.
        <System.Diagnostics.DebuggerStepThrough()> _
        Private Sub InitializeComponent()
            Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(Main_Copy))
            Dim SerializableAppearanceObject1 As DevExpress.Utils.SerializableAppearanceObject = New DevExpress.Utils.SerializableAppearanceObject
            Me.LabelControl1 = New DevExpress.XtraEditors.LabelControl
            Me.PanelControl1 = New DevExpress.XtraEditors.PanelControl
            Me.CreditorID1 = New CreditorID
            Me.SimpleButton_cancel = New DevExpress.XtraEditors.SimpleButton
            Me.SimpleButton_ok = New DevExpress.XtraEditors.SimpleButton
            Me.TextEdit1 = New DevExpress.XtraEditors.TextEdit
            Me.LabelControl3 = New DevExpress.XtraEditors.LabelControl
            Me.LabelControl_creditor = New DevExpress.XtraEditors.LabelControl
            CType(Me.PanelControl1, System.ComponentModel.ISupportInitialize).BeginInit()
            Me.PanelControl1.SuspendLayout()
            CType(Me.CreditorID1.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.TextEdit1.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
            Me.SuspendLayout()
            '
            'LabelControl1
            '
            Me.LabelControl1.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                        Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
            Me.LabelControl1.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
            Me.LabelControl1.Appearance.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Top
            Me.LabelControl1.Appearance.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap
            Me.LabelControl1.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None
            Me.LabelControl1.Location = New System.Drawing.Point(4, 13)
            Me.LabelControl1.Name = "LabelControl1"
            Me.LabelControl1.Padding = New System.Windows.Forms.Padding(4)
            Me.LabelControl1.Size = New System.Drawing.Size(468, 60)
            Me.LabelControl1.TabIndex = 0
            Me.LabelControl1.Text = resources.GetString("LabelControl1.Text")
            Me.LabelControl1.UseMnemonic = False
            '
            'PanelControl1
            '
            Me.PanelControl1.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom), System.Windows.Forms.AnchorStyles)
            Me.PanelControl1.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder
            Me.PanelControl1.Controls.Add(Me.CreditorID1)
            Me.PanelControl1.Controls.Add(Me.SimpleButton_cancel)
            Me.PanelControl1.Controls.Add(Me.SimpleButton_ok)
            Me.PanelControl1.Controls.Add(Me.TextEdit1)
            Me.PanelControl1.Controls.Add(Me.LabelControl3)
            Me.PanelControl1.Controls.Add(Me.LabelControl_creditor)
            Me.PanelControl1.Location = New System.Drawing.Point(48, 79)
            Me.PanelControl1.Name = "PanelControl1"
            Me.PanelControl1.Size = New System.Drawing.Size(378, 109)
            Me.PanelControl1.TabIndex = 1
            '
            'CreditorID1
            '
            Me.CreditorID1.Anchor = System.Windows.Forms.AnchorStyles.None
            Me.CreditorID1.EditValue = Nothing
            Me.CreditorID1.Location = New System.Drawing.Point(139, 31)
            Me.CreditorID1.Name = "CreditorID1"
            Me.CreditorID1.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph, "", -1, True, True, False, DevExpress.XtraEditors.ImageLocation.MiddleCenter, CType(resources.GetObject("CreditorID1.Properties.Buttons"), System.Drawing.Image), New DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), SerializableAppearanceObject1, "Click here to search for a creditor ID", "search", Nothing, True)})
            Me.CreditorID1.Properties.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
            Me.CreditorID1.Properties.Mask.AutoComplete = DevExpress.XtraEditors.Mask.AutoCompleteType.Strong
            Me.CreditorID1.Properties.Mask.BeepOnError = True
            Me.CreditorID1.Properties.Mask.EditMask = "[A-Z]{1,2}[0-9]{4,}"
            Me.CreditorID1.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.RegEx
            Me.CreditorID1.Properties.Mask.UseMaskAsDisplayFormat = True
            Me.CreditorID1.Properties.MaxLength = 10
            Me.CreditorID1.Size = New System.Drawing.Size(100, 20)
            Me.CreditorID1.TabIndex = 1
            '
            'SimpleButton_cancel
            '
            Me.SimpleButton_cancel.Anchor = System.Windows.Forms.AnchorStyles.None
            Me.SimpleButton_cancel.Location = New System.Drawing.Point(270, 61)
            Me.SimpleButton_cancel.Name = "SimpleButton_cancel"
            Me.SimpleButton_cancel.Size = New System.Drawing.Size(75, 23)
            Me.SimpleButton_cancel.TabIndex = 5
            Me.SimpleButton_cancel.Text = "&Cancel"
            '
            'SimpleButton_ok
            '
            Me.SimpleButton_ok.Anchor = System.Windows.Forms.AnchorStyles.None
            Me.SimpleButton_ok.Enabled = False
            Me.SimpleButton_ok.Location = New System.Drawing.Point(270, 24)
            Me.SimpleButton_ok.Name = "SimpleButton_ok"
            Me.SimpleButton_ok.Size = New System.Drawing.Size(75, 23)
            Me.SimpleButton_ok.TabIndex = 4
            Me.SimpleButton_ok.Text = "&Next"
            '
            'TextEdit1
            '
            Me.TextEdit1.Anchor = System.Windows.Forms.AnchorStyles.None
            Me.TextEdit1.Location = New System.Drawing.Point(139, 58)
            Me.TextEdit1.Name = "TextEdit1"
            Me.TextEdit1.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[False]
            Me.TextEdit1.Properties.Appearance.Options.UseTextOptions = True
            Me.TextEdit1.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
            Me.TextEdit1.Properties.AppearanceDisabled.Options.UseTextOptions = True
            Me.TextEdit1.Properties.AppearanceDisabled.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
            Me.TextEdit1.Properties.AppearanceFocused.Options.UseTextOptions = True
            Me.TextEdit1.Properties.AppearanceFocused.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
            Me.TextEdit1.Properties.AppearanceReadOnly.Options.UseTextOptions = True
            Me.TextEdit1.Properties.AppearanceReadOnly.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
            Me.TextEdit1.Properties.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
            Me.TextEdit1.Properties.Mask.BeepOnError = True
            Me.TextEdit1.Properties.Mask.EditMask = "[A-Z]?[0-9]+"
            Me.TextEdit1.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.RegEx
            Me.TextEdit1.Size = New System.Drawing.Size(100, 20)
            Me.TextEdit1.TabIndex = 3
            '
            'LabelControl3
            '
            Me.LabelControl3.Anchor = System.Windows.Forms.AnchorStyles.None
            Me.LabelControl3.Location = New System.Drawing.Point(23, 61)
            Me.LabelControl3.Name = "LabelControl3"
            Me.LabelControl3.Size = New System.Drawing.Size(110, 13)
            Me.LabelControl3.TabIndex = 2
            Me.LabelControl3.Text = "Check Number to Copy"
            '
            'LabelControl_creditor
            '
            Me.LabelControl_creditor.Anchor = System.Windows.Forms.AnchorStyles.None
            Me.LabelControl_creditor.Location = New System.Drawing.Point(23, 34)
            Me.LabelControl_creditor.Name = "LabelControl_creditor"
            Me.LabelControl_creditor.Size = New System.Drawing.Size(39, 13)
            Me.LabelControl_creditor.TabIndex = 0
            Me.LabelControl_creditor.Text = "Creditor"
            '
            'Main_Copy
            '
            Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
            Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
            Me.Controls.Add(Me.PanelControl1)
            Me.Controls.Add(Me.LabelControl1)
            Me.Name = "Main_Copy"
            Me.Size = New System.Drawing.Size(475, 217)
            CType(Me.PanelControl1, System.ComponentModel.ISupportInitialize).EndInit()
            Me.PanelControl1.ResumeLayout(False)
            Me.PanelControl1.PerformLayout()
            CType(Me.CreditorID1.Properties, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.TextEdit1.Properties, System.ComponentModel.ISupportInitialize).EndInit()
            Me.ResumeLayout(False)

        End Sub
        Friend WithEvents LabelControl1 As DevExpress.XtraEditors.LabelControl
        Friend WithEvents PanelControl1 As DevExpress.XtraEditors.PanelControl
        Friend WithEvents SimpleButton_cancel As DevExpress.XtraEditors.SimpleButton
        Friend WithEvents SimpleButton_ok As DevExpress.XtraEditors.SimpleButton
        Friend WithEvents TextEdit1 As DevExpress.XtraEditors.TextEdit
        Friend WithEvents LabelControl3 As DevExpress.XtraEditors.LabelControl
        Friend WithEvents LabelControl_creditor As DevExpress.XtraEditors.LabelControl
        Friend WithEvents CreditorID1 As CreditorID

    End Class
End Namespace
