Option Compare Binary
Option Explicit On 
Option Strict On

Namespace Disbursement.Manual
    Friend Class DisbursementArgParser
        Inherits DebtPlus.Utils.ArgParserBase

        Public Sub New()
            MyBase.New(New String() {})
        End Sub

        Protected Overrides Sub OnUsage(ByVal errorInfo As String)
            ' deprecated
        End Sub
    End Class
End Namespace