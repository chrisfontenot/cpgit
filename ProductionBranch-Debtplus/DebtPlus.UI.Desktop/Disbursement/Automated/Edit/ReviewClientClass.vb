#Region "Copyright 2000-2012 DebtPlus, L.L.C."
'{*******************************************************************}
'{                                                                   }
'{       DebtPlus Debt Management System                             }
'{                                                                   }
'{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
'{       ALL RIGHTS RESERVED                                         }
'{                                                                   }
'{   The entire contents of this file is protected by U.S. and       }
'{   International Copyright Laws. Unauthorized reproduction,        }
'{   reverse-engineering, and distribution of all or any portion of  }
'{   the code contained in this file is strictly prohibited and may  }
'{   result in severe civil and criminal penalties and will be       }
'{   prosecuted to the maximum extent possible under the law.        }
'{                                                                   }
'{   RESTRICTIONS                                                    }
'{                                                                   }
'{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
'{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
'{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
'{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
'{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
'{                                                                   }
'{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
'{   ADDITIONAL RESTRICTIONS.                                        }
'{                                                                   }
'{*******************************************************************}

#End Region
Imports DebtPlus.UI.Desktop.Disbursement.Common
Imports System.Data.SqlClient
Imports System.Windows.Forms

Namespace Disbursement.Automated.Edit
    Friend Class ReviewClientClass
        Inherits ClientClass

        ' Tripped at the end of the update to refresh the row in the main display
        Public Event RefreshRow As EventHandler
        Protected Friend drv As DataRowView = Nothing

        ''' <summary>
        ''' Initialize the class instance
        ''' </summary>
        ''' <param name="Context">Pointer to the global context information</param>
        ''' <remarks></remarks>
        Public Sub New(ByVal Context As IDisbursementContext)
            MyBase.New(Context)
        End Sub

        ''' <summary>
        ''' Initialize the class instance
        ''' </summary>
        ''' <param name="Context">Pointer to the global context information</param>
        ''' <param name="drv">Row that represents the client in the disbursement</param>
        ''' <remarks></remarks>
        Public Sub New(ByVal Context As IDisbursementContext, ByVal drv As DataRowView)
            MyClass.New(Context)
            Me.drv = drv
            ClientROW = drv.Row
        End Sub

        ''' <summary>
        ''' Thread procedure to do the review of the client
        ''' </summary>
        ''' <remarks></remarks>
        Public Sub ReviewDisbursement()

            ' Build the list of debts in the separate thread for appearance performance enhancment
            DebtList = New DisbursementDebtList()
            DebtList.ReadDebtsTable(Convert.ToInt32(drv("client")), DirectCast(Context.ap, ArgParser).BatchID)
            ' $$$ EDIT $$$ MyBase.RecalulateMonthlyFee()

            ' Conduct the dialog and tell the parent that we have completed the form.
            If Do_Examine() = DialogResult.OK Then

                ' Empty the information before we do the update
                drv("client_register") = DBNull.Value
                drv("disbursed") = 0D
                drv("created_by") = DBNull.Value
                drv("date_created") = DBNull.Value

                ' Update the current row with the disbursement information once we have done the operation
                Dim cn As SqlConnection = New SqlConnection(DebtPlus.LINQ.SQLInfoClass.getDefault().ConnectionString)
                Dim ClientRegister As Object
                Dim rd As SqlDataReader
                Try
                    cn.Open()
                    Using cmd As SqlCommand = New SqlCommand()
                        With cmd
                            .Connection = cn
                            .CommandText = "SELECT client_register FROM disbursement_clients WHERE client = @client AND disbursement_register = @disbursement_register"
                            .CommandType = CommandType.Text
                            .Parameters.Add("@client", SqlDbType.Int).Value = ClientId
                            .Parameters.Add("@disbursement_register", SqlDbType.Int).Value = disbursement_register
                            ClientRegister = .ExecuteScalar
                        End With
                    End Using
                    drv("client_register") = ClientRegister

                    ' From the client register, get the disbursed dollar amount and the various other information
                    If ClientRegister IsNot Nothing AndAlso ClientRegister IsNot DBNull.Value Then
                        Using cmd As SqlCommand = New SqlCommand()
                            With cmd
                                .Connection = cn
                                .CommandText = "SELECT debit_amt, created_by, date_created FROM registers_client WHERE client_register = @client_register"
                                .CommandType = CommandType.Text
                                .Parameters.Add("@client_register", SqlDbType.Int).Value = ClientRegister
                                rd = .ExecuteReader(CommandBehavior.CloseConnection Or CommandBehavior.SingleRow)
                            End With
                        End Using

                        ' Read the field information
                        If rd IsNot Nothing AndAlso rd.Read Then
                            drv("disbursed") = rd.GetValue(0)
                            drv("created_by") = rd.GetValue(1)
                            drv("date_created") = rd.GetValue(2)
                        End If
                    End If

                Catch ex As SqlException
                    DebtPlus.UI.Common.ErrorHandling.HandleErrors(ex, "Error reading disbursement information")

                Finally
                    If cn IsNot Nothing Then cn.Dispose()
                End Try

                ' Refresh the form with the new information
                RaiseEvent RefreshRow(Me, EventArgs.Empty)
            End If
        End Sub

        ''' <summary>
        ''' Handle the EXAMINE PREVIOUS button event differently in the review
        ''' </summary>
        ''' <remarks></remarks>
        Public Overrides Sub Do_ExaminePrevious()
            Debug.Assert(False, "ExaminePrevious called when it has no previous pointer")
        End Sub

        ''' <summary>
        ''' Handle the QUIT button event differently in the review
        ''' </summary>
        ''' <remarks></remarks>
        Public Overrides Sub Do_Quit()
            CurrentForm.DialogResult = DialogResult.Cancel
        End Sub

        ''' <summary>
        ''' Process the EXAMINE button event
        ''' </summary>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public Overrides Function Do_Examine() As DialogResult
            Dim answer As DialogResult
            CurrentForm = New Examine_Form(Context, Me)
            Try

                ' For the review operation we want the QUIT button to be CANCEL and there is no
                ' need for a PREVIOUS button since there is no concept of a PREVIOUS client.
                CurrentForm.ButtonQuit.Text = "&Cancel"
                CurrentForm.ButtonPrevious.Visible = False

                ' Do the standard logic from here.
                answer = CurrentForm.ShowDialog()
            Finally
                CurrentForm.Dispose()
                CurrentForm = Nothing
            End Try

            Return answer
        End Function
    End Class
End Namespace