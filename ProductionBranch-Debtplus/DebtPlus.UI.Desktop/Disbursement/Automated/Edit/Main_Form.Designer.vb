Imports DebtPlus.UI.FormLib.Disbursement

Namespace Disbursement.Automated.Edit
    <Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
    Partial Class Main_Form
        Inherits DebtPlus.Data.Forms.DebtPlusForm

        'Form overrides dispose to clean up the component list.
        <System.Diagnostics.DebuggerNonUserCode()> _
        Protected Overrides Sub Dispose(ByVal disposing As Boolean)
            Try
                If disposing AndAlso components IsNot Nothing Then
                    components.Dispose()
                End If
            Finally
                MyBase.Dispose(disposing)
            End Try
        End Sub

        'Required by the Windows Form Designer
        Private components As System.ComponentModel.IContainer

        'NOTE: The following procedure is required by the Windows Form Designer
        'It can be modified using the Windows Form Designer.  
        'Do not modify it using the code editor.
        <System.Diagnostics.DebuggerStepThrough()> _
        Private Sub InitializeComponent()
            Me.PanelControl1 = New DevExpress.XtraEditors.PanelControl
            Me.LabelControl1 = New DevExpress.XtraEditors.LabelControl
            Me.DisbursementRegisterControl1 = New DisbursementRegisterControl
            Me.Panel_Process1 = New Panel_Process()
            CType(Me.DxErrorProvider1, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.PanelControl1, System.ComponentModel.ISupportInitialize).BeginInit()
            Me.PanelControl1.SuspendLayout()
            Me.SuspendLayout()
            '
            'PanelControl1
            '
            Me.PanelControl1.Controls.Add(Me.LabelControl1)
            Me.PanelControl1.Controls.Add(Me.DisbursementRegisterControl1)
            Me.PanelControl1.Dock = System.Windows.Forms.DockStyle.Fill
            Me.PanelControl1.Location = New System.Drawing.Point(0, 0)
            Me.PanelControl1.Name = "PanelControl1"
            Me.PanelControl1.Size = New System.Drawing.Size(497, 331)
            Me.ToolTipController1.SetSuperTip(Me.PanelControl1, Nothing)
            Me.PanelControl1.TabIndex = 0
            '
            'LabelControl1
            '
            Me.LabelControl1.Appearance.Options.UseTextOptions = True
            Me.LabelControl1.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
            Me.LabelControl1.Appearance.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Top
            Me.LabelControl1.Appearance.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap
            Me.LabelControl1.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None
            Me.LabelControl1.Location = New System.Drawing.Point(12, 0)
            Me.LabelControl1.Name = "LabelControl1"
            Me.LabelControl1.Padding = New System.Windows.Forms.Padding(4)
            Me.LabelControl1.Size = New System.Drawing.Size(472, 29)
            Me.LabelControl1.TabIndex = 1
            Me.LabelControl1.Text = "Please choose the disbursement batch that you wish to update or press CANCEL to t" & "erminate"
            '
            'DisbursementRegisterControl1
            '
            Me.DisbursementRegisterControl1.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                        Or System.Windows.Forms.AnchorStyles.Left) _
                        Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
            Me.DisbursementRegisterControl1.Location = New System.Drawing.Point(6, 33)
            Me.DisbursementRegisterControl1.Name = "DisbursementRegisterControl1"
            Me.DisbursementRegisterControl1.Size = New System.Drawing.Size(486, 286)
            Me.ToolTipController1.SetSuperTip(Me.DisbursementRegisterControl1, Nothing)
            Me.DisbursementRegisterControl1.TabIndex = 0
            '
            'Panel_Process1
            '
            Me.Panel_Process1.Dock = System.Windows.Forms.DockStyle.Fill
            Me.Panel_Process1.Location = New System.Drawing.Point(0, 0)
            Me.Panel_Process1.Name = "Panel_Process1"
            Me.Panel_Process1.Size = New System.Drawing.Size(497, 331)
            Me.ToolTipController1.SetSuperTip(Me.Panel_Process1, Nothing)
            Me.Panel_Process1.TabIndex = 1
            '
            'Form1
            '
            Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
            Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
            Me.ClientSize = New System.Drawing.Size(497, 331)
            Me.Controls.Add(Me.Panel_Process1)
            Me.Controls.Add(Me.PanelControl1)
            Me.Name = "Form1"
            Me.ToolTipController1.SetSuperTip(Me, Nothing)
            Me.Text = "Disbursement Review"
            CType(Me.DxErrorProvider1, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.PanelControl1, System.ComponentModel.ISupportInitialize).EndInit()
            Me.PanelControl1.ResumeLayout(False)
            Me.ResumeLayout(False)

        End Sub
        Friend WithEvents PanelControl1 As DevExpress.XtraEditors.PanelControl
        Friend WithEvents DisbursementRegisterControl1 As DisbursementRegisterControl
        Friend WithEvents Panel_Process1 As Panel_Process
        Friend WithEvents LabelControl1 As DevExpress.XtraEditors.LabelControl

    End Class
End Namespace