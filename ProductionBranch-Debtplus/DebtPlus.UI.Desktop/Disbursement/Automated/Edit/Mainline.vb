#Region "Copyright 2000-2012 DebtPlus, L.L.C."
'{*******************************************************************}
'{                                                                   }
'{       DebtPlus Debt Management System                             }
'{                                                                   }
'{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
'{       ALL RIGHTS RESERVED                                         }
'{                                                                   }
'{   The entire contents of this file is protected by U.S. and       }
'{   International Copyright Laws. Unauthorized reproduction,        }
'{   reverse-engineering, and distribution of all or any portion of  }
'{   the code contained in this file is strictly prohibited and may  }
'{   result in severe civil and criminal penalties and will be       }
'{   prosecuted to the maximum extent possible under the law.        }
'{                                                                   }
'{   RESTRICTIONS                                                    }
'{                                                                   }
'{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
'{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
'{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
'{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
'{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
'{                                                                   }
'{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
'{   ADDITIONAL RESTRICTIONS.                                        }
'{                                                                   }
'{*******************************************************************}

#End Region

Option Compare Binary
Option Explicit On
Option Strict On

Imports System.Data.SqlClient
Imports DebtPlus.Interfaces.Desktop
Imports System.Windows.Forms

Namespace Disbursement.Automated.Edit
    Public Class Mainline
        Implements IDesktopMainline

        Public Sub OldAppMain(ByVal args() As String) Implements IDesktopMainline.OldAppMain
            Dim ap As New ArgParser()

            ' Parse the arguments
            If ap.Parse(args) Then
                Dim thrd As New System.Threading.Thread(New System.Threading.ParameterizedThreadStart(AddressOf ThreadProcedure))
                thrd.SetApartmentState(Threading.ApartmentState.STA)
                thrd.Name = "EditDisbursementProcessing"
                thrd.Start(ap)
            End If
        End Sub

        Private Sub ThreadProcedure(ByVal obj As Object)
            Dim ap As ArgParser = DirectCast(obj, ArgParser)
            Using context As New DisbursementAutoEditContext()
                context.ap = ap

                Dim txn As SqlTransaction = Nothing
                Dim rd As SqlDataReader = Nothing

                Dim cn As SqlConnection = New SqlConnection(DebtPlus.LINQ.SQLInfoClass.getDefault().ConnectionString)
                Try
                    cn.Open()
                    txn = cn.BeginTransaction(IsolationLevel.ReadCommitted, "AutoDisbursement")

                    Using cmd As SqlCommand = New SqlCommand
                        With cmd
                            .Connection = cn
                            .Transaction = txn
                            .CommandText = "SELECT TOP 1 current_status FROM DISBURSEMENT_LOCK WITH (HOLDLOCK, ROWLOCK)"
                            .CommandType = CommandType.Text
                            rd = .ExecuteReader(CommandBehavior.SingleRow) ' DO NOT CLOSE THE CONNECTION HERE !!
                        End With
                    End Using

                    ' Just read the first row from the result. It will leave the transaction pending.
                    If rd IsNot Nothing Then
                        rd.Read()
                        rd.Close()
                        rd = Nothing
                    End If

                    ' Now, with the transaction still open, run the program. It will process the data
                    ' and allow full operation. When the form terminates, we will return to complete
                    ' the transaction and release the shared lock.
                    Using mainForm As New Main_Form(context)
                        mainForm.ShowDialog()
                    End Using

                Catch ex As Exception
                    DebtPlus.Data.Forms.MessageBox.Show(ex.ToString(), "Thread Exception occurred", MessageBoxButtons.OK, MessageBoxIcon.Stop)

                Finally

                    ' Ensure that the read is still not pending
                    If rd IsNot Nothing Then rd.Dispose()

                    ' Commit the transaction before closing the connection
                    If txn IsNot Nothing Then
                        txn.Commit()
                        txn = Nothing
                    End If

                    ' Finally, close the connection
                    If cn IsNot Nothing Then cn.Dispose()
                End Try
            End Using
        End Sub
    End Class
End Namespace