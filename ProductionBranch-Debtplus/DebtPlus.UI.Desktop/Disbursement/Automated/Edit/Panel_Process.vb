#Region "Copyright 2000-2012 DebtPlus, L.L.C."
'{*******************************************************************}
'{                                                                   }
'{       DebtPlus Debt Management System                             }
'{                                                                   }
'{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
'{       ALL RIGHTS RESERVED                                         }
'{                                                                   }
'{   The entire contents of this file is protected by U.S. and       }
'{   International Copyright Laws. Unauthorized reproduction,        }
'{   reverse-engineering, and distribution of all or any portion of  }
'{   the code contained in this file is strictly prohibited and may  }
'{   result in severe civil and criminal penalties and will be       }
'{   prosecuted to the maximum extent possible under the law.        }
'{                                                                   }
'{   RESTRICTIONS                                                    }
'{                                                                   }
'{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
'{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
'{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
'{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
'{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
'{                                                                   }
'{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
'{   ADDITIONAL RESTRICTIONS.                                        }
'{                                                                   }
'{*******************************************************************}

#End Region
Imports DebtPlus.Data.Controls
Imports DevExpress.XtraGrid.Views.Base
Imports DevExpress.XtraEditors
Imports DevExpress.Utils
Imports System.Data.SqlClient
Imports DevExpress.XtraGrid.Views.Grid.ViewInfo
Imports System.Threading
Imports System.IO
Imports System.Windows.Forms
Imports System.Drawing

Namespace Disbursement.Automated.Edit
    Friend Class Panel_Process

        ''' <summary>
        ''' Event raised when the CANCEL button is pressed
        ''' </summary>
        ''' <remarks></remarks>
        Public Event Cancelled As EventHandler

        Private FocusedDRV As DataRowView = Nothing

        ''' <summary>
        ''' Dataset for the local use
        ''' </summary>
        ''' <remarks></remarks>
        Private ds As New DataSet("ds")

        ''' <summary>
        ''' Context (static) information
        ''' </summary>
        ''' <value></value>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Protected Property context As Disbursement.Common.IDisbursementContext
            Get
                Return DirectCast(ParentForm, Disbursement.Common.ISupportContext).Context
            End Get
            Set(value As Disbursement.Common.IDisbursementContext)
                Throw New NotImplementedException()
            End Set
        End Property

        ''' <summary>
        ''' Initialize the class
        ''' </summary>
        ''' <remarks></remarks>
        Public Sub New()
            MyBase.New()
            InitializeComponent()
            RegisterHandlers()
        End Sub

        ''' <summary>
        ''' Register the events
        ''' </summary>
        ''' <remarks></remarks>
        Private Sub RegisterHandlers()
            AddHandler Load, AddressOf Control_Load
            AddHandler SimpleButton_Select.Click, AddressOf SimpleButton_Select_Click
            AddHandler SimpleButton_Cancel.Click, AddressOf SimpleButton_Cancel_Click
            AddHandler GridView1.FilterEditorCreated, AddressOf GridView1_FilterEditorCreated
            AddHandler GridView1.DoubleClick, AddressOf GridView1_DoubleClick
            AddHandler GridView1.FocusedRowChanged, AddressOf GridView1_FocusedRowChanged
            AddHandler GridView1.Layout, AddressOf LayoutChanged
            AddHandler ComboBoxEdit_filter.SelectedIndexChanged, AddressOf ComboBoxEdit_filter_SelectedIndexChanged
        End Sub

        ''' <summary>
        ''' Remove the event registration
        ''' </summary>
        ''' <remarks></remarks>
        Private Sub UnRegisterHandlers()
            RemoveHandler Load, AddressOf Control_Load
            RemoveHandler SimpleButton_Cancel.Click, AddressOf SimpleButton_Cancel_Click
            RemoveHandler SimpleButton_Select.Click, AddressOf SimpleButton_Select_Click
            RemoveHandler GridView1.FilterEditorCreated, AddressOf GridView1_FilterEditorCreated
            RemoveHandler GridView1.DoubleClick, AddressOf GridView1_DoubleClick
            RemoveHandler GridView1.FocusedRowChanged, AddressOf GridView1_FocusedRowChanged
            RemoveHandler GridView1.Layout, AddressOf LayoutChanged
            RemoveHandler ComboBoxEdit_filter.SelectedIndexChanged, AddressOf ComboBoxEdit_filter_SelectedIndexChanged
        End Sub

        ''' <summary>
        ''' Process the load of the control. Here is where the initialization is performed.
        ''' </summary>
        ''' <param name="sender"></param>
        ''' <param name="e"></param>
        ''' <remarks></remarks>
        Private Sub Control_Load(ByVal sender As Object, ByVal e As EventArgs)
            UnRegisterHandlers()

            Try
                ' Load the layout for the grid control
                Dim pathName As String = XMLBasePath()
                Dim fileName As String = Path.Combine(pathName, "Process.Grid.xml")

                Try
                    If File.Exists(fileName) Then
                        GridView1.RestoreLayoutFromXml(fileName)
                    End If
                Catch ex As DirectoryNotFoundException
                Catch ex As FileNotFoundException
                End Try

                With GridColumn_client
                    With .DisplayFormat
                        .Format = New DebtPlus.Utils.Format.Client.CustomFormatter
                    End With

                    With .GroupFormat
                        .Format = New DebtPlus.Utils.Format.Client.CustomFormatter
                    End With
                End With

                With ComboBoxEdit_filter
                    With .Properties
                        With .Items
                            .Clear()
                            .Add(New DebtPlus.Data.Controls.ComboboxItem("All clients in the batch", ""))
                            .Add(New DebtPlus.Data.Controls.ComboboxItem("Only disbursed clients in the batch", "[disbursed]>0.0"))
                            .Add(New DebtPlus.Data.Controls.ComboboxItem("Only clients who were NOT disbursed in the batch", "[disbursed]=0.0"))
                            .Add(New DebtPlus.Data.Controls.ComboboxItem("Has trust but not disbursed", "[held_in_trust]>0.0 AND [disbursed]=0.0"))
                            .Add(New DebtPlus.Data.Controls.ComboboxItem("Not Disbused but with disbursement notes", "[disbursed]=0.0 AND [note_count]>0"))
                            .Add(New DebtPlus.Data.Controls.ComboboxItem("Disbursement Factor differs from Amount Disbursed", "[disbursed]<>[disbursement_factor]"))
                            .Add(New DebtPlus.Data.Controls.ComboboxItem("Did not disburse whole trust amount", "[disbursed]<>[held_in_trust]"))
                        End With
                    End With
                    .SelectedIndex = 0
                End With
            Finally
                RegisterHandlers()
            End Try
        End Sub

        ''' <summary>
        ''' Process the CANCEL button
        ''' </summary>
        ''' <param name="sender"></param>
        ''' <param name="e"></param>
        ''' <remarks></remarks>
        Private Sub SimpleButton_Cancel_Click(ByVal sender As Object, ByVal e As EventArgs)
            RaiseEvent Cancelled(Me, EventArgs.Empty)
        End Sub

        ''' <summary>
        ''' Read the list of clients disbursed in the batch
        ''' </summary>
        ''' <remarks></remarks>
        Public Sub ReadForm()
            UnRegisterHandlers()
            Dim current_cursor As Cursor = Cursor.Current

            ' Put a big notice over the form that we are reading the data for the time being
            Try
                Using lbl As New LabelControl()
                    With lbl
                        .Location = GridControl1.Location
                        .Size = GridControl1.Size
                        .Anchor = AnchorStyles.Bottom Or AnchorStyles.Left Or AnchorStyles.Right Or AnchorStyles.Top
                        .AutoSizeMode = LabelAutoSizeMode.None
                        .AutoEllipsis = False
                        .UseMnemonic = False
                        .Cursor = Cursors.WaitCursor

                        ' Set the color
                        .BackColor = Color.FromKnownColor(KnownColor.Window)
                        .ForeColor = Color.FromKnownColor(KnownColor.WindowText)
                        .Appearance.BackColor = .BackColor
                        .Appearance.ForeColor = .ForeColor

                        With .Appearance

                            ' Force the text to the middle of the area
                            With .TextOptions
                                .HAlignment = HorzAlignment.Near
                                .VAlignment = VertAlignment.Center
                                .WordWrap = WordWrap.Wrap
                            End With

                            With .Options
                                .UseTextOptions = True
                                .UseBackColor = True
                                .UseForeColor = True
                            End With
                        End With

                        .Font = New Font(.Font, FontStyle.Bold)
                        .Padding = New Padding(20)
                        .Text = "The system is currently reading in the list of clients that where placed into this disbursement batch. This may take a few minutes. It depends upon the number of clients in the batch. When the reading operation is completed this notice will go away and you will have a grid showing the clients. Until then please be patient. It is working as fast as possible. Thank you."
                    End With

                    ' Add the item to the control and make it overlay the grid control
                    Controls.Add(lbl)
                    lbl.BringToFront()

                    ' Do the painting operation for the controls
                    Application.DoEvents()
                    Cursor.Current = Cursors.WaitCursor

                    Using cmd As SqlCommand = New SqlCommand
                        With cmd
                            .Connection = New SqlConnection(DebtPlus.LINQ.SQLInfoClass.getDefault().ConnectionString)
                            .CommandText = "SELECT v.disbursement_register, v.client, v.held_in_trust, v.reserved_in_trust, v.disbursement_factor, v.note_count, v.client_register, v.active_status, v.start_date, v.region, v.client_name, v.disbursed, v.created_by, v.date_created, dbo.format_normal_name(default,cox.first,cox.middle,cox.last,cox.suffix) as counselor_name, o.name as office_name FROM [view_disbursement_review] v WITH (NOLOCK) INNER JOIN clients c ON v.client = c.client LEFT OUTER JOIN offices o ON c.office = o.office LEFT OUTER JOIN counselors co ON c.counselor = co.counselor LEFT OUTER JOIN names cox WITH (NOLOCK) ON co.NameID = cox.Name WHERE disbursement_register=@disbursement_register"
                            .CommandType = CommandType.Text
                            .Parameters.Add("@disbursement_register", SqlDbType.Int).Value = DirectCast(context.ap, ArgParser).BatchID
                        End With

                        Using da As New SqlDataAdapter(cmd)
                            ds.Clear()
                            da.Fill(ds, "view_disbursement_review")
                        End Using
                    End Using

                    Dim tbl As DataTable = ds.Tables("view_disbursement_review")
                    With tbl
                        If .PrimaryKey.GetUpperBound(0) < 0 Then .PrimaryKey = New DataColumn() {.Columns("client_creditor")}
                        If Not .Columns.Contains("starting_trust") Then .Columns.Add("starting_trust", GetType(Decimal), "[held_in_trust]")
                        If Not .Columns.Contains("ending_trust") Then .Columns.Add("ending_trust", GetType(Decimal), "[held_in_trust]-[disbursed]")
                    End With

                    ' Update the grid information
                    With GridControl1
                        .DataSource = tbl.DefaultView
                        .RefreshDataSource()
                    End With

                    ' Put in the selected filter
                    AdjustFilter()

                    ' Remove the label control once we have the data
                    lbl.SendToBack()
                    Controls.Remove(lbl)
                End Using

            Finally
                Cursor.Current = current_cursor
                RegisterHandlers()
            End Try
        End Sub

        ''' <summary>
        ''' Set the filter into the grid
        ''' </summary>
        ''' <remarks></remarks>
        Private Sub AdjustFilter()

            With GridView1
                .BeginUpdate()                  ' Suspend the update of the grid for a moment
                .ActiveFilter.Clear()           ' Remove the existing filter from the list

                ' Set the filter to the expression
                If CurrentFilterMode <> String.Empty Then
                    .ActiveFilterString = CurrentFilterMode
                    .OptionsView.ShowFilterPanelMode = ShowFilterPanelMode.Never
                End If

                .EndUpdate()                    ' Complete the update event
            End With
        End Sub

        ''' <summary>
        ''' Find the current filter string
        ''' </summary>
        ''' <value></value>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Private ReadOnly Property CurrentFilterMode() As String
            Get
                If ComboBoxEdit_filter.SelectedIndex < 0 Then Return String.Empty
                Return Convert.ToString(CType(ComboBoxEdit_filter.SelectedItem, DebtPlus.Data.Controls.ComboboxItem).value)
            End Get
        End Property

        ''' <summary>
        ''' When the combobox changes, update the filter
        ''' </summary>
        ''' <param name="sender"></param>
        ''' <param name="e"></param>
        ''' <remarks></remarks>
        Private Sub ComboBoxEdit_filter_SelectedIndexChanged(ByVal sender As Object, ByVal e As EventArgs)
            AdjustFilter()
        End Sub

        ''' <summary>
        ''' Process an update on the filter for the grid
        ''' </summary>
        ''' <param name="sender"></param>
        ''' <param name="e"></param>
        ''' <remarks></remarks>
        Private Sub GridView1_FilterEditorCreated(ByVal sender As Object, ByVal e As FilterControlEventArgs)
            GridView1.OptionsView.ShowFilterPanelMode = ShowFilterPanelMode.Default
        End Sub

        ''' <summary>
        ''' Handle a double-click event on the grid view
        ''' </summary>
        ''' <param name="sender"></param>
        ''' <param name="e"></param>
        ''' <remarks></remarks>
        Private Sub GridView1_DoubleClick(ByVal sender As Object, ByVal e As EventArgs)

            Dim hi As GridHitInfo = GridView1.CalcHitInfo((GridControl1.PointToClient(MousePosition)))
            If hi.IsValid AndAlso (hi.InRow OrElse hi.InRowCell) Then

                ' Find the row from the hit test routine
                Dim controlRow As Int32 = hi.RowHandle

                ' If there is a row then edit that row
                If controlRow >= 0 Then
                    Dim vue As DataView = CType(GridControl1.DataSource, DataView)
                    Dim drv As DataRowView = CType(GridView1.GetRow(controlRow), DataRowView)
                    EditClient(drv)
                End If
            End If
        End Sub

        ''' <summary>
        ''' When the grid control focus row changes, enable the SELECT button
        ''' </summary>
        ''' <param name="sender"></param>
        ''' <param name="e"></param>
        ''' <remarks></remarks>
        Private Sub GridView1_FocusedRowChanged(ByVal sender As Object, ByVal e As FocusedRowChangedEventArgs)
            FocusedDRV = Nothing
            If Not DesignMode Then
                Dim controlRow As Int32 = e.FocusedRowHandle
                If controlRow >= 0 Then
                    Dim vue As DataView = CType(GridControl1.DataSource, DataView)
                    FocusedDRV = CType(GridView1.GetRow(controlRow), DataRowView)
                End If
            End If

            SimpleButton_Select.Enabled = FocusedDRV IsNot Nothing
        End Sub

        ''' <summary>
        ''' Handle the SELECT button click event
        ''' </summary>
        ''' <param name="sender"></param>
        ''' <param name="e"></param>
        ''' <remarks></remarks>
        Private Sub SimpleButton_Select_Click(ByVal sender As Object, ByVal e As EventArgs)
            EditClient(FocusedDRV)
            FocusedDRV = Nothing
            SimpleButton_Select.Enabled = False
        End Sub

        ''' <summary>
        ''' Edit the client row
        ''' </summary>
        ''' <param name="drv"></param>
        ''' <remarks></remarks>
        Private Sub EditClient(ByVal drv As DataRowView)

            ' Launch a thread to handle the dialog
            Dim reviewClass As New ReviewClientClass(context, drv)
            AddHandler reviewClass.RefreshRow, AddressOf RefreshRow

            With New Thread(AddressOf reviewClass.ReviewDisbursement)
                .SetApartmentState(ApartmentState.STA)
                .Name = "Edit_Thread"
                .IsBackground = True
                .Start()
            End With
        End Sub

        ''' <summary>
        ''' Perform a refresh on the grid row (thread safe)
        ''' </summary>
        ''' <param name="Sender"></param>
        ''' <param name="e"></param>
        ''' <remarks></remarks>
        Private Sub RefreshRow(ByVal Sender As Object, ByVal e As EventArgs)
            If InvokeRequired Then
                BeginInvoke(New EventHandler(AddressOf RefreshRow), New Object() {Sender, e})
            Else
                GridView1.RefreshData()
            End If
        End Sub

        ''' <summary>
        ''' Return the directory to the saved layout information
        ''' </summary>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Protected Overridable Function XMLBasePath() As String
            Dim basePath As String = Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData) + Path.DirectorySeparatorChar + "DebtPlus"
            Return Path.Combine(basePath, "Disbursement.Auto.Edit")
        End Function

        ''' <summary>
        ''' If the layout is changed then update the file
        ''' </summary>
        ''' <param name="Sender"></param>
        ''' <param name="e"></param>
        ''' <remarks></remarks>
        Private Sub LayoutChanged(ByVal Sender As Object, ByVal e As EventArgs)
            Dim pathName As String = XMLBasePath()
            If Not Directory.Exists(pathName) Then
                Directory.CreateDirectory(pathName)
            End If

            Dim fileName As String = Path.Combine(pathName, "Process.Grid.xml")
            GridView1.SaveLayoutToXml(fileName)
        End Sub
    End Class
End Namespace