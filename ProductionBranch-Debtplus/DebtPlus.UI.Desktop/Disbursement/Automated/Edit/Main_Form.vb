#Region "Copyright 2000-2012 DebtPlus, L.L.C."
'{*******************************************************************}
'{                                                                   }
'{       DebtPlus Debt Management System                             }
'{                                                                   }
'{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
'{       ALL RIGHTS RESERVED                                         }
'{                                                                   }
'{   The entire contents of this file is protected by U.S. and       }
'{   International Copyright Laws. Unauthorized reproduction,        }
'{   reverse-engineering, and distribution of all or any portion of  }
'{   the code contained in this file is strictly prohibited and may  }
'{   result in severe civil and criminal penalties and will be       }
'{   prosecuted to the maximum extent possible under the law.        }
'{                                                                   }
'{   RESTRICTIONS                                                    }
'{                                                                   }
'{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
'{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
'{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
'{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
'{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
'{                                                                   }
'{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
'{   ADDITIONAL RESTRICTIONS.                                        }
'{                                                                   }
'{*******************************************************************}
#End Region

Namespace Disbursement.Automated.Edit
    Friend Class Main_Form
        Implements DebtPlus.UI.Desktop.Disbursement.Common.ISupportContext

        Public Property Context As DebtPlus.UI.Desktop.Disbursement.Common.IDisbursementContext = Nothing Implements Disbursement.Common.ISupportContext.Context

        Public Sub New(ByVal Context As DisbursementAutoEditContext)
            MyBase.New()
            Me.Context = Context
            InitializeComponent()

            AddHandler DisbursementRegisterControl1.Selected, AddressOf DisbursementRegisterControl1_Selected
            AddHandler Load, AddressOf Form1_Load
            AddHandler Panel_Process1.Cancelled, AddressOf Panel_process1_Cancel
            AddHandler DisbursementRegisterControl1.Cancelled, AddressOf DisbursementRegisterControl1_Cancelled
        End Sub

        Private Sub Form1_Load(ByVal sender As Object, ByVal e As EventArgs)

            ' Reload the placement and size
            LoadPlacement("Disbursement.Auto.Edit")

            If DirectCast(Context.ap, ArgParser).BatchID <= 0 Then
                PanelControl1.Visible = True
                PanelControl1.TabStop = True
                PanelControl1.BringToFront()

                DisbursementRegisterControl1.ReadForm()
            Else
                ProcessBatch()
            End If
        End Sub

        Private Sub Panel_process1_Cancel(ByVal sender As Object, ByVal e As EventArgs)
            Panel_Process1.Visible = False
            Panel_Process1.TabStop = False
            Panel_Process1.SendToBack()

            PanelControl1.Visible = True
            PanelControl1.TabStop = True
            PanelControl1.BringToFront()

            DisbursementRegisterControl1.ReadForm()
        End Sub

        Private Sub DisbursementRegisterControl1_Cancelled(ByVal sender As Object, ByVal e As EventArgs)
            DialogResult = Windows.Forms.DialogResult.Cancel
        End Sub

        Private Sub DisbursementRegisterControl1_Selected(ByVal sender As Object, ByVal e As EventArgs)
            DirectCast(Context.ap, ArgParser).BatchID = DisbursementRegisterControl1.BatchID
            ProcessBatch()
        End Sub

        Private Sub ProcessBatch()
            PanelControl1.Visible = False
            PanelControl1.TabStop = False
            PanelControl1.SendToBack()

            Panel_Process1.Visible = True
            Panel_Process1.TabStop = True
            Panel_Process1.BringToFront()
            Panel_Process1.ReadForm()
        End Sub
    End Class
End Namespace