Namespace Disbursement.Automated.Edit
    <Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
    Partial Class Panel_Process
        Inherits DevExpress.XtraEditors.XtraUserControl

        'UserControl overrides dispose to clean up the component list.
        <System.Diagnostics.DebuggerNonUserCode()> _
        Protected Overrides Sub Dispose(ByVal disposing As Boolean)
            Try
                If disposing Then
                    If components IsNot Nothing Then components.Dispose()
                    ds.Dispose()
                End If
            Finally
                MyBase.Dispose(disposing)
            End Try
        End Sub

        'Required by the Windows Form Designer
        Private components As System.ComponentModel.IContainer

        'NOTE: The following procedure is required by the Windows Form Designer
        'It can be modified using the Windows Form Designer.  
        'Do not modify it using the code editor.
        <System.Diagnostics.DebuggerStepThrough()> _
        Private Sub InitializeComponent()
            Dim StyleFormatCondition1 As DevExpress.XtraGrid.StyleFormatCondition = New DevExpress.XtraGrid.StyleFormatCondition
            Me.GridColumn_ending_trust = New DevExpress.XtraGrid.Columns.GridColumn
            Me.GridColumn_ending_trust.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.True
            Me.GridColumn_disbursed = New DevExpress.XtraGrid.Columns.GridColumn
            Me.GridColumn_disbursed.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.True
            Me.LabelControl1 = New DevExpress.XtraEditors.LabelControl
            Me.GridControl1 = New DevExpress.XtraGrid.GridControl
            Me.GridView1 = New DevExpress.XtraGrid.Views.Grid.GridView
            Me.GridColumn_client = New DevExpress.XtraGrid.Columns.GridColumn
            Me.GridColumn_client.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.True
            Me.GridColumn_client_name = New DevExpress.XtraGrid.Columns.GridColumn
            Me.GridColumn_client_name.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.True
            Me.GridColumn_held_in_trust = New DevExpress.XtraGrid.Columns.GridColumn
            Me.GridColumn_held_in_trust.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.True
            Me.GridColumn_reserved_in_trust = New DevExpress.XtraGrid.Columns.GridColumn
            Me.GridColumn_reserved_in_trust.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.True
            Me.GridColumn_disbursement_factor = New DevExpress.XtraGrid.Columns.GridColumn
            Me.GridColumn_disbursement_factor.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.True
            Me.GridColumn_note_count = New DevExpress.XtraGrid.Columns.GridColumn
            Me.GridColumn_note_count.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.True
            Me.GridColumn_client_register = New DevExpress.XtraGrid.Columns.GridColumn
            Me.GridColumn_client_register.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.True
            Me.GridColumn_active_status = New DevExpress.XtraGrid.Columns.GridColumn
            Me.GridColumn_active_status.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.True
            Me.GridColumn_start_date = New DevExpress.XtraGrid.Columns.GridColumn
            Me.GridColumn_start_date.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.True
            Me.GridColumn_region = New DevExpress.XtraGrid.Columns.GridColumn
            Me.GridColumn_region.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.True
            Me.GridColumn_created_by = New DevExpress.XtraGrid.Columns.GridColumn
            Me.GridColumn_created_by.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.True
            Me.GridColumn_date_created = New DevExpress.XtraGrid.Columns.GridColumn
            Me.GridColumn_date_created.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.True
            Me.GridColumn_office_name = New DevExpress.XtraGrid.Columns.GridColumn
            Me.GridColumn_office_name.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.True
            Me.GridColumn_counselor_name = New DevExpress.XtraGrid.Columns.GridColumn
            Me.GridColumn_counselor_name.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.True
            Me.SimpleButton_Select = New DevExpress.XtraEditors.SimpleButton
            Me.SimpleButton_Cancel = New DevExpress.XtraEditors.SimpleButton
            Me.ComboBoxEdit_filter = New DevExpress.XtraEditors.ComboBoxEdit
            Me.LabelControl2 = New DevExpress.XtraEditors.LabelControl
            CType(Me.GridControl1, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.GridView1, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.ComboBoxEdit_filter.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
            Me.SuspendLayout()
            '
            'GridColumn_ending_trust
            '
            Me.GridColumn_ending_trust.Caption = "Ending Trust"
            Me.GridColumn_ending_trust.DisplayFormat.FormatString = "{0:c}"
            Me.GridColumn_ending_trust.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
            Me.GridColumn_ending_trust.FieldName = "ending_trust"
            Me.GridColumn_ending_trust.GroupFormat.FormatString = "{0:c}"
            Me.GridColumn_ending_trust.GroupFormat.FormatType = DevExpress.Utils.FormatType.Numeric
            Me.GridColumn_ending_trust.Name = "GridColumn_ending_trust"
            '
            'GridColumn_disbursed
            '
            Me.GridColumn_disbursed.AppearanceCell.Options.UseTextOptions = True
            Me.GridColumn_disbursed.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
            Me.GridColumn_disbursed.AppearanceHeader.Options.UseTextOptions = True
            Me.GridColumn_disbursed.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
            Me.GridColumn_disbursed.Caption = "Disbursed"
            Me.GridColumn_disbursed.DisplayFormat.FormatString = "{0:c}"
            Me.GridColumn_disbursed.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
            Me.GridColumn_disbursed.FieldName = "disbursed"
            Me.GridColumn_disbursed.GroupFormat.FormatString = "{0:c}"
            Me.GridColumn_disbursed.GroupFormat.FormatType = DevExpress.Utils.FormatType.Numeric
            Me.GridColumn_disbursed.Name = "GridColumn_disbursed"
            Me.GridColumn_disbursed.Visible = True
            Me.GridColumn_disbursed.VisibleIndex = 4
            '
            'LabelControl1
            '
            Me.LabelControl1.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                        Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
            Me.LabelControl1.Appearance.Options.UseTextOptions = True
            Me.LabelControl1.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
            Me.LabelControl1.Appearance.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center
            Me.LabelControl1.Appearance.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap
            Me.LabelControl1.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None
            Me.LabelControl1.Location = New System.Drawing.Point(4, 3)
            Me.LabelControl1.Name = "LabelControl1"
            Me.LabelControl1.Size = New System.Drawing.Size(482, 39)
            Me.LabelControl1.TabIndex = 0
            Me.LabelControl1.Text = "This is a list of the clients involved in the disbursement for this batch. Double" & _
                "-click on a client or click on the SELECT button to the right to edit the client" & _
                " information."
            Me.LabelControl1.UseMnemonic = False
            '
            'GridControl1
            '
            Me.GridControl1.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                        Or System.Windows.Forms.AnchorStyles.Left) _
                        Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
            Me.GridControl1.Location = New System.Drawing.Point(4, 49)
            Me.GridControl1.MainView = Me.GridView1
            Me.GridControl1.Name = "GridControl1"
            Me.GridControl1.Size = New System.Drawing.Size(400, 166)
            Me.GridControl1.TabIndex = 1
            Me.GridControl1.ViewCollection.AddRange(New DevExpress.XtraGrid.Views.Base.BaseView() {Me.GridView1})
            '
            'GridView1
            '
            Me.GridView1.Appearance.ColumnFilterButton.BackColor = System.Drawing.Color.FromArgb(CType(CType(104, Byte), Int32), CType(CType(184, Byte), Int32), CType(CType(251, Byte), Int32))
            Me.GridView1.Appearance.ColumnFilterButton.BorderColor = System.Drawing.Color.FromArgb(CType(CType(104, Byte), Int32), CType(CType(184, Byte), Int32), CType(CType(251, Byte), Int32))
            Me.GridView1.Appearance.ColumnFilterButton.ForeColor = System.Drawing.Color.White
            Me.GridView1.Appearance.ColumnFilterButton.Options.UseBackColor = True
            Me.GridView1.Appearance.ColumnFilterButton.Options.UseBorderColor = True
            Me.GridView1.Appearance.ColumnFilterButton.Options.UseForeColor = True
            Me.GridView1.Appearance.ColumnFilterButtonActive.BackColor = System.Drawing.Color.FromArgb(CType(CType(170, Byte), Int32), CType(CType(216, Byte), Int32), CType(CType(254, Byte), Int32))
            Me.GridView1.Appearance.ColumnFilterButtonActive.BorderColor = System.Drawing.Color.FromArgb(CType(CType(170, Byte), Int32), CType(CType(216, Byte), Int32), CType(CType(254, Byte), Int32))
            Me.GridView1.Appearance.ColumnFilterButtonActive.ForeColor = System.Drawing.Color.Black
            Me.GridView1.Appearance.ColumnFilterButtonActive.Options.UseBackColor = True
            Me.GridView1.Appearance.ColumnFilterButtonActive.Options.UseBorderColor = True
            Me.GridView1.Appearance.ColumnFilterButtonActive.Options.UseForeColor = True
            Me.GridView1.Appearance.Empty.BackColor = System.Drawing.Color.FromArgb(CType(CType(236, Byte), Int32), CType(CType(246, Byte), Int32), CType(CType(255, Byte), Int32))
            Me.GridView1.Appearance.Empty.BackColor2 = System.Drawing.Color.White
            Me.GridView1.Appearance.Empty.Options.UseBackColor = True
            Me.GridView1.Appearance.EvenRow.BackColor = System.Drawing.Color.FromArgb(CType(CType(247, Byte), Int32), CType(CType(251, Byte), Int32), CType(CType(255, Byte), Int32))
            Me.GridView1.Appearance.EvenRow.BorderColor = System.Drawing.Color.FromArgb(CType(CType(247, Byte), Int32), CType(CType(251, Byte), Int32), CType(CType(255, Byte), Int32))
            Me.GridView1.Appearance.EvenRow.ForeColor = System.Drawing.Color.Black
            Me.GridView1.Appearance.EvenRow.Options.UseBackColor = True
            Me.GridView1.Appearance.EvenRow.Options.UseBorderColor = True
            Me.GridView1.Appearance.EvenRow.Options.UseForeColor = True
            Me.GridView1.Appearance.FilterCloseButton.BackColor = System.Drawing.Color.FromArgb(CType(CType(104, Byte), Int32), CType(CType(184, Byte), Int32), CType(CType(251, Byte), Int32))
            Me.GridView1.Appearance.FilterCloseButton.BorderColor = System.Drawing.Color.FromArgb(CType(CType(104, Byte), Int32), CType(CType(184, Byte), Int32), CType(CType(251, Byte), Int32))
            Me.GridView1.Appearance.FilterCloseButton.ForeColor = System.Drawing.Color.White
            Me.GridView1.Appearance.FilterCloseButton.Options.UseBackColor = True
            Me.GridView1.Appearance.FilterCloseButton.Options.UseBorderColor = True
            Me.GridView1.Appearance.FilterCloseButton.Options.UseForeColor = True
            Me.GridView1.Appearance.FilterPanel.BackColor = System.Drawing.Color.FromArgb(CType(CType(236, Byte), Int32), CType(CType(246, Byte), Int32), CType(CType(255, Byte), Int32))
            Me.GridView1.Appearance.FilterPanel.BackColor2 = System.Drawing.Color.White
            Me.GridView1.Appearance.FilterPanel.ForeColor = System.Drawing.Color.Black
            Me.GridView1.Appearance.FilterPanel.Options.UseBackColor = True
            Me.GridView1.Appearance.FilterPanel.Options.UseForeColor = True
            Me.GridView1.Appearance.FixedLine.BackColor = System.Drawing.Color.FromArgb(CType(CType(59, Byte), Int32), CType(CType(133, Byte), Int32), CType(CType(195, Byte), Int32))
            Me.GridView1.Appearance.FixedLine.Options.UseBackColor = True
            Me.GridView1.Appearance.FocusedCell.BackColor = System.Drawing.Color.White
            Me.GridView1.Appearance.FocusedCell.ForeColor = System.Drawing.Color.Black
            Me.GridView1.Appearance.FocusedCell.Options.UseBackColor = True
            Me.GridView1.Appearance.FocusedCell.Options.UseForeColor = True
            Me.GridView1.Appearance.FocusedRow.BackColor = System.Drawing.Color.FromArgb(CType(CType(38, Byte), Int32), CType(CType(109, Byte), Int32), CType(CType(189, Byte), Int32))
            Me.GridView1.Appearance.FocusedRow.BorderColor = System.Drawing.Color.FromArgb(CType(CType(59, Byte), Int32), CType(CType(139, Byte), Int32), CType(CType(206, Byte), Int32))
            Me.GridView1.Appearance.FocusedRow.ForeColor = System.Drawing.Color.White
            Me.GridView1.Appearance.FocusedRow.Options.UseBackColor = True
            Me.GridView1.Appearance.FocusedRow.Options.UseBorderColor = True
            Me.GridView1.Appearance.FocusedRow.Options.UseForeColor = True
            Me.GridView1.Appearance.FooterPanel.BackColor = System.Drawing.Color.FromArgb(CType(CType(104, Byte), Int32), CType(CType(184, Byte), Int32), CType(CType(251, Byte), Int32))
            Me.GridView1.Appearance.FooterPanel.BorderColor = System.Drawing.Color.FromArgb(CType(CType(104, Byte), Int32), CType(CType(184, Byte), Int32), CType(CType(251, Byte), Int32))
            Me.GridView1.Appearance.FooterPanel.ForeColor = System.Drawing.Color.Black
            Me.GridView1.Appearance.FooterPanel.Options.UseBackColor = True
            Me.GridView1.Appearance.FooterPanel.Options.UseBorderColor = True
            Me.GridView1.Appearance.FooterPanel.Options.UseForeColor = True
            Me.GridView1.Appearance.GroupButton.BackColor = System.Drawing.Color.FromArgb(CType(CType(104, Byte), Int32), CType(CType(184, Byte), Int32), CType(CType(251, Byte), Int32))
            Me.GridView1.Appearance.GroupButton.BorderColor = System.Drawing.Color.FromArgb(CType(CType(104, Byte), Int32), CType(CType(184, Byte), Int32), CType(CType(251, Byte), Int32))
            Me.GridView1.Appearance.GroupButton.Options.UseBackColor = True
            Me.GridView1.Appearance.GroupButton.Options.UseBorderColor = True
            Me.GridView1.Appearance.GroupFooter.BackColor = System.Drawing.Color.FromArgb(CType(CType(170, Byte), Int32), CType(CType(216, Byte), Int32), CType(CType(254, Byte), Int32))
            Me.GridView1.Appearance.GroupFooter.BorderColor = System.Drawing.Color.FromArgb(CType(CType(170, Byte), Int32), CType(CType(216, Byte), Int32), CType(CType(254, Byte), Int32))
            Me.GridView1.Appearance.GroupFooter.ForeColor = System.Drawing.Color.Black
            Me.GridView1.Appearance.GroupFooter.Options.UseBackColor = True
            Me.GridView1.Appearance.GroupFooter.Options.UseBorderColor = True
            Me.GridView1.Appearance.GroupFooter.Options.UseForeColor = True
            Me.GridView1.Appearance.GroupPanel.BackColor = System.Drawing.Color.FromArgb(CType(CType(236, Byte), Int32), CType(CType(246, Byte), Int32), CType(CType(255, Byte), Int32))
            Me.GridView1.Appearance.GroupPanel.BackColor2 = System.Drawing.Color.White
            Me.GridView1.Appearance.GroupPanel.ForeColor = System.Drawing.Color.Black
            Me.GridView1.Appearance.GroupPanel.Options.UseBackColor = True
            Me.GridView1.Appearance.GroupPanel.Options.UseForeColor = True
            Me.GridView1.Appearance.GroupRow.BackColor = System.Drawing.Color.FromArgb(CType(CType(170, Byte), Int32), CType(CType(216, Byte), Int32), CType(CType(254, Byte), Int32))
            Me.GridView1.Appearance.GroupRow.BorderColor = System.Drawing.Color.FromArgb(CType(CType(170, Byte), Int32), CType(CType(216, Byte), Int32), CType(CType(254, Byte), Int32))
            Me.GridView1.Appearance.GroupRow.ForeColor = System.Drawing.Color.Black
            Me.GridView1.Appearance.GroupRow.Options.UseBackColor = True
            Me.GridView1.Appearance.GroupRow.Options.UseBorderColor = True
            Me.GridView1.Appearance.GroupRow.Options.UseForeColor = True
            Me.GridView1.Appearance.HeaderPanel.BackColor = System.Drawing.Color.FromArgb(CType(CType(139, Byte), Int32), CType(CType(201, Byte), Int32), CType(CType(254, Byte), Int32))
            Me.GridView1.Appearance.HeaderPanel.BorderColor = System.Drawing.Color.FromArgb(CType(CType(139, Byte), Int32), CType(CType(201, Byte), Int32), CType(CType(254, Byte), Int32))
            Me.GridView1.Appearance.HeaderPanel.ForeColor = System.Drawing.Color.Black
            Me.GridView1.Appearance.HeaderPanel.Options.UseBackColor = True
            Me.GridView1.Appearance.HeaderPanel.Options.UseBorderColor = True
            Me.GridView1.Appearance.HeaderPanel.Options.UseForeColor = True
            Me.GridView1.Appearance.HideSelectionRow.BackColor = System.Drawing.Color.FromArgb(CType(CType(105, Byte), Int32), CType(CType(170, Byte), Int32), CType(CType(225, Byte), Int32))
            Me.GridView1.Appearance.HideSelectionRow.BorderColor = System.Drawing.Color.FromArgb(CType(CType(83, Byte), Int32), CType(CType(155, Byte), Int32), CType(CType(215, Byte), Int32))
            Me.GridView1.Appearance.HideSelectionRow.ForeColor = System.Drawing.Color.FromArgb(CType(CType(236, Byte), Int32), CType(CType(246, Byte), Int32), CType(CType(255, Byte), Int32))
            Me.GridView1.Appearance.HideSelectionRow.Options.UseBackColor = True
            Me.GridView1.Appearance.HideSelectionRow.Options.UseBorderColor = True
            Me.GridView1.Appearance.HideSelectionRow.Options.UseForeColor = True
            Me.GridView1.Appearance.HorzLine.BackColor = System.Drawing.Color.FromArgb(CType(CType(104, Byte), Int32), CType(CType(184, Byte), Int32), CType(CType(251, Byte), Int32))
            Me.GridView1.Appearance.HorzLine.Options.UseBackColor = True
            Me.GridView1.Appearance.OddRow.BackColor = System.Drawing.Color.FromArgb(CType(CType(236, Byte), Int32), CType(CType(246, Byte), Int32), CType(CType(255, Byte), Int32))
            Me.GridView1.Appearance.OddRow.BorderColor = System.Drawing.Color.FromArgb(CType(CType(236, Byte), Int32), CType(CType(246, Byte), Int32), CType(CType(255, Byte), Int32))
            Me.GridView1.Appearance.OddRow.ForeColor = System.Drawing.Color.Black
            Me.GridView1.Appearance.OddRow.Options.UseBackColor = True
            Me.GridView1.Appearance.OddRow.Options.UseBorderColor = True
            Me.GridView1.Appearance.OddRow.Options.UseForeColor = True
            Me.GridView1.Appearance.Preview.Font = New System.Drawing.Font("Verdana", 7.5!)
            Me.GridView1.Appearance.Preview.ForeColor = System.Drawing.Color.FromArgb(CType(CType(83, Byte), Int32), CType(CType(155, Byte), Int32), CType(CType(215, Byte), Int32))
            Me.GridView1.Appearance.Preview.Options.UseFont = True
            Me.GridView1.Appearance.Preview.Options.UseForeColor = True
            Me.GridView1.Appearance.Row.BackColor = System.Drawing.Color.FromArgb(CType(CType(247, Byte), Int32), CType(CType(251, Byte), Int32), CType(CType(255, Byte), Int32))
            Me.GridView1.Appearance.Row.ForeColor = System.Drawing.Color.Black
            Me.GridView1.Appearance.Row.Options.UseBackColor = True
            Me.GridView1.Appearance.Row.Options.UseForeColor = True
            Me.GridView1.Appearance.RowSeparator.BackColor = System.Drawing.Color.FromArgb(CType(CType(236, Byte), Int32), CType(CType(246, Byte), Int32), CType(CType(255, Byte), Int32))
            Me.GridView1.Appearance.RowSeparator.BackColor2 = System.Drawing.Color.White
            Me.GridView1.Appearance.RowSeparator.Options.UseBackColor = True
            Me.GridView1.Appearance.SelectedRow.BackColor = System.Drawing.Color.FromArgb(CType(CType(83, Byte), Int32), CType(CType(155, Byte), Int32), CType(CType(215, Byte), Int32))
            Me.GridView1.Appearance.SelectedRow.ForeColor = System.Drawing.Color.White
            Me.GridView1.Appearance.SelectedRow.Options.UseBackColor = True
            Me.GridView1.Appearance.SelectedRow.Options.UseForeColor = True
            Me.GridView1.Appearance.TopNewRow.BackColor = System.Drawing.Color.White
            Me.GridView1.Appearance.TopNewRow.Options.UseBackColor = True
            Me.GridView1.Appearance.VertLine.BackColor = System.Drawing.Color.FromArgb(CType(CType(104, Byte), Int32), CType(CType(184, Byte), Int32), CType(CType(251, Byte), Int32))
            Me.GridView1.Appearance.VertLine.Options.UseBackColor = True
            Me.GridView1.Columns.AddRange(New DevExpress.XtraGrid.Columns.GridColumn() {Me.GridColumn_client, Me.GridColumn_client_name, Me.GridColumn_held_in_trust, Me.GridColumn_ending_trust, Me.GridColumn_reserved_in_trust, Me.GridColumn_disbursement_factor, Me.GridColumn_note_count, Me.GridColumn_client_register, Me.GridColumn_active_status, Me.GridColumn_start_date, Me.GridColumn_region, Me.GridColumn_disbursed, Me.GridColumn_created_by, Me.GridColumn_date_created, Me.GridColumn_office_name, Me.GridColumn_counselor_name})
            StyleFormatCondition1.Appearance.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Int32), CType(CType(255, Byte), Int32), CType(CType(192, Byte), Int32))
            StyleFormatCondition1.Appearance.BackColor2 = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Int32), CType(CType(255, Byte), Int32), CType(CType(192, Byte), Int32))
            StyleFormatCondition1.Appearance.Options.UseBackColor = True
            StyleFormatCondition1.ApplyToRow = True
            StyleFormatCondition1.Column = Me.GridColumn_ending_trust
            StyleFormatCondition1.Condition = DevExpress.XtraGrid.FormatConditionEnum.Greater
            StyleFormatCondition1.Value1 = New Decimal(New Int32() {0, 0, 0, 0})
            Me.GridView1.FormatConditions.AddRange(New DevExpress.XtraGrid.StyleFormatCondition() {StyleFormatCondition1})
            Me.GridView1.GridControl = Me.GridControl1
            Me.GridView1.Name = "GridView1"
            Me.GridView1.OptionsBehavior.Editable = False
            Me.GridView1.OptionsView.ShowGroupPanel = False
            Me.GridView1.OptionsView.ShowIndicator = False
            Me.GridView1.SortInfo.Add(GridColumn_client, DevExpress.Data.ColumnSortOrder.Ascending)
            '
            'GridColumn_client
            '
            Me.GridColumn_client.AppearanceCell.Options.UseTextOptions = True
            Me.GridColumn_client.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
            Me.GridColumn_client.AppearanceHeader.Options.UseTextOptions = True
            Me.GridColumn_client.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
            Me.GridColumn_client.Caption = "Client"
            Me.GridColumn_client.DisplayFormat.FormatString = "f0"
            Me.GridColumn_client.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Custom
            Me.GridColumn_client.FieldName = "client"
            Me.GridColumn_client.GroupFormat.FormatString = "f0"
            Me.GridColumn_client.GroupFormat.FormatType = DevExpress.Utils.FormatType.Custom
            Me.GridColumn_client.Name = "GridColumn_client"
            Me.GridColumn_client.Visible = True
            Me.GridColumn_client.VisibleIndex = 0
            Me.GridColumn_client.SortOrder = DevExpress.Data.ColumnSortOrder.Ascending
            '
            'GridColumn_client_name
            '
            Me.GridColumn_client_name.AppearanceCell.Options.UseTextOptions = True
            Me.GridColumn_client_name.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
            Me.GridColumn_client_name.AppearanceHeader.Options.UseTextOptions = True
            Me.GridColumn_client_name.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
            Me.GridColumn_client_name.Caption = "Name"
            Me.GridColumn_client_name.FieldName = "client_name"
            Me.GridColumn_client_name.Name = "GridColumn_client_name"
            Me.GridColumn_client_name.Visible = True
            Me.GridColumn_client_name.VisibleIndex = 1
            '
            'GridColumn_held_in_trust
            '
            Me.GridColumn_held_in_trust.AppearanceCell.Options.UseTextOptions = True
            Me.GridColumn_held_in_trust.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
            Me.GridColumn_held_in_trust.AppearanceHeader.Options.UseTextOptions = True
            Me.GridColumn_held_in_trust.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
            Me.GridColumn_held_in_trust.Caption = "Starting Trust"
            Me.GridColumn_held_in_trust.DisplayFormat.FormatString = "{0:c}"
            Me.GridColumn_held_in_trust.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
            Me.GridColumn_held_in_trust.FieldName = "starting_trust"
            Me.GridColumn_held_in_trust.GroupFormat.FormatString = "{0:c}"
            Me.GridColumn_held_in_trust.GroupFormat.FormatType = DevExpress.Utils.FormatType.Numeric
            Me.GridColumn_held_in_trust.Name = "GridColumn_held_in_trust"
            Me.GridColumn_held_in_trust.Visible = True
            Me.GridColumn_held_in_trust.VisibleIndex = 2
            '
            'GridColumn_reserved_in_trust
            '
            Me.GridColumn_reserved_in_trust.AppearanceCell.Options.UseTextOptions = True
            Me.GridColumn_reserved_in_trust.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
            Me.GridColumn_reserved_in_trust.AppearanceHeader.Options.UseTextOptions = True
            Me.GridColumn_reserved_in_trust.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
            Me.GridColumn_reserved_in_trust.Caption = "Reserved $"
            Me.GridColumn_reserved_in_trust.DisplayFormat.FormatString = "{0:c}"
            Me.GridColumn_reserved_in_trust.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
            Me.GridColumn_reserved_in_trust.FieldName = "reserved_in_trust"
            Me.GridColumn_reserved_in_trust.GroupFormat.FormatString = "{0:c}"
            Me.GridColumn_reserved_in_trust.GroupFormat.FormatType = DevExpress.Utils.FormatType.Numeric
            Me.GridColumn_reserved_in_trust.Name = "GridColumn_reserved_in_trust"
            '
            'GridColumn_disbursement_factor
            '
            Me.GridColumn_disbursement_factor.AppearanceCell.Options.UseTextOptions = True
            Me.GridColumn_disbursement_factor.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
            Me.GridColumn_disbursement_factor.AppearanceHeader.Options.UseTextOptions = True
            Me.GridColumn_disbursement_factor.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
            Me.GridColumn_disbursement_factor.Caption = "Disbursement"
            Me.GridColumn_disbursement_factor.DisplayFormat.FormatString = "{0:c}"
            Me.GridColumn_disbursement_factor.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
            Me.GridColumn_disbursement_factor.FieldName = "disbursement_factor"
            Me.GridColumn_disbursement_factor.GroupFormat.FormatString = "{0:c}"
            Me.GridColumn_disbursement_factor.GroupFormat.FormatType = DevExpress.Utils.FormatType.Numeric
            Me.GridColumn_disbursement_factor.Name = "GridColumn_disbursement_factor"
            Me.GridColumn_disbursement_factor.Visible = True
            Me.GridColumn_disbursement_factor.VisibleIndex = 3
            '
            'GridColumn_note_count
            '
            Me.GridColumn_note_count.AppearanceCell.Options.UseTextOptions = True
            Me.GridColumn_note_count.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
            Me.GridColumn_note_count.AppearanceHeader.Options.UseTextOptions = True
            Me.GridColumn_note_count.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
            Me.GridColumn_note_count.Caption = "Notes"
            Me.GridColumn_note_count.DisplayFormat.FormatString = "{0:f0}"
            Me.GridColumn_note_count.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
            Me.GridColumn_note_count.FieldName = "note_count"
            Me.GridColumn_note_count.GroupFormat.FormatString = "{0:f0}"
            Me.GridColumn_note_count.GroupFormat.FormatType = DevExpress.Utils.FormatType.Numeric
            Me.GridColumn_note_count.Name = "GridColumn_note_count"
            '
            'GridColumn_client_register
            '
            Me.GridColumn_client_register.AppearanceCell.Options.UseTextOptions = True
            Me.GridColumn_client_register.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
            Me.GridColumn_client_register.AppearanceHeader.Options.UseTextOptions = True
            Me.GridColumn_client_register.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
            Me.GridColumn_client_register.Caption = "ClientRegister"
            Me.GridColumn_client_register.DisplayFormat.FormatString = "{0:f0}"
            Me.GridColumn_client_register.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
            Me.GridColumn_client_register.FieldName = "client_register"
            Me.GridColumn_client_register.GroupFormat.FormatString = "{0:f0}"
            Me.GridColumn_client_register.GroupFormat.FormatType = DevExpress.Utils.FormatType.Numeric
            Me.GridColumn_client_register.Name = "GridColumn_client_register"
            '
            'GridColumn_active_status
            '
            Me.GridColumn_active_status.AppearanceCell.Options.UseTextOptions = True
            Me.GridColumn_active_status.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center
            Me.GridColumn_active_status.AppearanceHeader.Options.UseTextOptions = True
            Me.GridColumn_active_status.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center
            Me.GridColumn_active_status.Caption = "Active Status"
            Me.GridColumn_active_status.FieldName = "active_status"
            Me.GridColumn_active_status.Name = "GridColumn_active_status"
            '
            'GridColumn_start_date
            '
            Me.GridColumn_start_date.AppearanceCell.Options.UseTextOptions = True
            Me.GridColumn_start_date.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
            Me.GridColumn_start_date.AppearanceHeader.Options.UseTextOptions = True
            Me.GridColumn_start_date.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
            Me.GridColumn_start_date.Caption = "Start Date"
            Me.GridColumn_start_date.DisplayFormat.FormatString = "d"
            Me.GridColumn_start_date.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime
            Me.GridColumn_start_date.FieldName = "start_date"
            Me.GridColumn_start_date.GroupFormat.FormatString = "d"
            Me.GridColumn_start_date.GroupFormat.FormatType = DevExpress.Utils.FormatType.DateTime
            Me.GridColumn_start_date.Name = "GridColumn_start_date"
            '
            'GridColumn_region
            '
            Me.GridColumn_region.AppearanceCell.Options.UseTextOptions = True
            Me.GridColumn_region.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
            Me.GridColumn_region.AppearanceHeader.Options.UseTextOptions = True
            Me.GridColumn_region.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
            Me.GridColumn_region.Caption = "Region"
            Me.GridColumn_region.DisplayFormat.FormatString = "{0:f0}"
            Me.GridColumn_region.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
            Me.GridColumn_region.FieldName = "region"
            Me.GridColumn_region.GroupFormat.FormatString = "{0:f0}"
            Me.GridColumn_region.GroupFormat.FormatType = DevExpress.Utils.FormatType.Numeric
            Me.GridColumn_region.Name = "GridColumn_region"
            '
            'GridColumn_created_by
            '
            Me.GridColumn_created_by.AppearanceCell.Options.UseTextOptions = True
            Me.GridColumn_created_by.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
            Me.GridColumn_created_by.AppearanceHeader.Options.UseTextOptions = True
            Me.GridColumn_created_by.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
            Me.GridColumn_created_by.Caption = "Disbursed By"
            Me.GridColumn_created_by.FieldName = "created_by"
            Me.GridColumn_created_by.Name = "GridColumn_created_by"
            '
            'GridColumn_date_created
            '
            Me.GridColumn_date_created.AppearanceCell.Options.UseTextOptions = True
            Me.GridColumn_date_created.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
            Me.GridColumn_date_created.AppearanceHeader.Options.UseTextOptions = True
            Me.GridColumn_date_created.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
            Me.GridColumn_date_created.Caption = "Disbused Time"
            Me.GridColumn_date_created.DisplayFormat.FormatString = "d"
            Me.GridColumn_date_created.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime
            Me.GridColumn_date_created.FieldName = "date_created"
            Me.GridColumn_date_created.GroupFormat.FormatString = "d"
            Me.GridColumn_date_created.GroupFormat.FormatType = DevExpress.Utils.FormatType.DateTime
            Me.GridColumn_date_created.Name = "GridColumn_date_created"
            '
            'GridColumn_office_name
            '
            Me.GridColumn_office_name.Caption = "Office"
            Me.GridColumn_office_name.FieldName = "office_name"
            Me.GridColumn_office_name.Name = "GridColumn_office_name"
            '
            'GridColumn_counselor_name
            '
            Me.GridColumn_counselor_name.Caption = "Counselor"
            Me.GridColumn_counselor_name.FieldName = "counselor_name"
            Me.GridColumn_counselor_name.Name = "GridColumn_counselor_name"
            '
            'SimpleButton_Select
            '
            Me.SimpleButton_Select.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
            Me.SimpleButton_Select.Location = New System.Drawing.Point(411, 49)
            Me.SimpleButton_Select.Name = "SimpleButton_Select"
            Me.SimpleButton_Select.Size = New System.Drawing.Size(75, 23)
            Me.SimpleButton_Select.TabIndex = 2
            Me.SimpleButton_Select.Text = "&Select..."
            '
            'SimpleButton_Cancel
            '
            Me.SimpleButton_Cancel.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
            Me.SimpleButton_Cancel.Location = New System.Drawing.Point(410, 78)
            Me.SimpleButton_Cancel.Name = "SimpleButton_Cancel"
            Me.SimpleButton_Cancel.Size = New System.Drawing.Size(75, 23)
            Me.SimpleButton_Cancel.TabIndex = 3
            Me.SimpleButton_Cancel.Text = "&Cancel"
            '
            'ComboBoxEdit_filter
            '
            Me.ComboBoxEdit_filter.Anchor = CType(((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left) _
                        Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
            Me.ComboBoxEdit_filter.Location = New System.Drawing.Point(34, 221)
            Me.ComboBoxEdit_filter.Name = "ComboBoxEdit_filter"
            Me.ComboBoxEdit_filter.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
            Me.ComboBoxEdit_filter.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor
            Me.ComboBoxEdit_filter.Size = New System.Drawing.Size(370, 20)
            Me.ComboBoxEdit_filter.TabIndex = 4
            '
            'LabelControl2
            '
            Me.LabelControl2.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
            Me.LabelControl2.Location = New System.Drawing.Point(4, 224)
            Me.LabelControl2.Name = "LabelControl2"
            Me.LabelControl2.Size = New System.Drawing.Size(24, 13)
            Me.LabelControl2.TabIndex = 5
            Me.LabelControl2.Text = "Filter"
            '
            'Panel_Process
            '
            Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
            Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
            Me.Controls.Add(Me.LabelControl2)
            Me.Controls.Add(Me.ComboBoxEdit_filter)
            Me.Controls.Add(Me.SimpleButton_Cancel)
            Me.Controls.Add(Me.SimpleButton_Select)
            Me.Controls.Add(Me.GridControl1)
            Me.Controls.Add(Me.LabelControl1)
            Me.Name = "Panel_Process"
            Me.Size = New System.Drawing.Size(497, 244)
            CType(Me.GridControl1, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.GridView1, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.ComboBoxEdit_filter.Properties, System.ComponentModel.ISupportInitialize).EndInit()
            Me.ResumeLayout(False)
            Me.PerformLayout()

        End Sub
        Friend WithEvents LabelControl1 As DevExpress.XtraEditors.LabelControl
        Friend WithEvents GridControl1 As DevExpress.XtraGrid.GridControl
        Friend WithEvents GridView1 As DevExpress.XtraGrid.Views.Grid.GridView
        Friend WithEvents SimpleButton_Select As DevExpress.XtraEditors.SimpleButton
        Friend WithEvents SimpleButton_Cancel As DevExpress.XtraEditors.SimpleButton
        Friend WithEvents ComboBoxEdit_filter As DevExpress.XtraEditors.ComboBoxEdit
        Friend WithEvents GridColumn_client As DevExpress.XtraGrid.Columns.GridColumn
        Friend WithEvents GridColumn_held_in_trust As DevExpress.XtraGrid.Columns.GridColumn
        Friend WithEvents GridColumn_reserved_in_trust As DevExpress.XtraGrid.Columns.GridColumn
        Friend WithEvents GridColumn_disbursement_factor As DevExpress.XtraGrid.Columns.GridColumn
        Friend WithEvents GridColumn_note_count As DevExpress.XtraGrid.Columns.GridColumn
        Friend WithEvents GridColumn_client_register As DevExpress.XtraGrid.Columns.GridColumn
        Friend WithEvents GridColumn_active_status As DevExpress.XtraGrid.Columns.GridColumn
        Friend WithEvents GridColumn_start_date As DevExpress.XtraGrid.Columns.GridColumn
        Friend WithEvents GridColumn_region As DevExpress.XtraGrid.Columns.GridColumn
        Friend WithEvents GridColumn_client_name As DevExpress.XtraGrid.Columns.GridColumn
        Friend WithEvents GridColumn_disbursed As DevExpress.XtraGrid.Columns.GridColumn
        Friend WithEvents GridColumn_created_by As DevExpress.XtraGrid.Columns.GridColumn
        Friend WithEvents GridColumn_date_created As DevExpress.XtraGrid.Columns.GridColumn
        Friend WithEvents LabelControl2 As DevExpress.XtraEditors.LabelControl
        Friend WithEvents GridColumn_office_name As DevExpress.XtraGrid.Columns.GridColumn
        Friend WithEvents GridColumn_counselor_name As DevExpress.XtraGrid.Columns.GridColumn
        Friend WithEvents GridColumn_ending_trust As DevExpress.XtraGrid.Columns.GridColumn

    End Class
End Namespace