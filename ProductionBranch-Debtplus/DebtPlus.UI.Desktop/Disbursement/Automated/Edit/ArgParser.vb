Option Compare Binary
Option Explicit On
Option Strict On

Imports System.Globalization

Namespace Disbursement.Automated.Edit
    Public Class ArgParser
        Inherits DebtPlus.Utils.ArgParserBase
        Public Property BatchID() As Int32 = -1
        Public Property SelectionClause() As String

        Public Sub New()
            MyBase.New(New String() {"b"})
        End Sub

        Protected Overrides Function OnSwitch(ByVal switchName As String, ByVal switchValue As String) As SwitchStatus
            Dim ss As SwitchStatus = DebtPlus.Utils.ArgParserBase.SwitchStatus.NoError
            Select Case switchName
                Case "b"
                    Dim IntTemp As Int32
                    If Int32.TryParse(switchValue, NumberStyles.Integer, CultureInfo.InvariantCulture, IntTemp) Then
                        If IntTemp >= 0 Then
                            BatchID = IntTemp
                            Exit Select
                        End If
                    End If
                    ss = DebtPlus.Utils.ArgParserBase.SwitchStatus.YesError

                Case "?"
                    ss = DebtPlus.Utils.ArgParserBase.SwitchStatus.ShowUsage

                Case Else
                    ss = DebtPlus.Utils.ArgParserBase.SwitchStatus.YesError
            End Select

            Return ss
        End Function

        Protected Overrides Sub OnUsage(ByVal errorInfo As String)
            ' deprecated
        End Sub
    End Class
End Namespace