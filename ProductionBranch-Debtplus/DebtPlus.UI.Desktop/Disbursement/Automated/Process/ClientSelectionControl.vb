#Region "Copyright 2000-2012 DebtPlus, L.L.C."
'{*******************************************************************}
'{                                                                   }
'{       DebtPlus Debt Management System                             }
'{                                                                   }
'{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
'{       ALL RIGHTS RESERVED                                         }
'{                                                                   }
'{   The entire contents of this file is protected by U.S. and       }
'{   International Copyright Laws. Unauthorized reproduction,        }
'{   reverse-engineering, and distribution of all or any portion of  }
'{   the code contained in this file is strictly prohibited and may  }
'{   result in severe civil and criminal penalties and will be       }
'{   prosecuted to the maximum extent possible under the law.        }
'{                                                                   }
'{   RESTRICTIONS                                                    }
'{                                                                   }
'{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
'{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
'{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
'{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
'{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
'{                                                                   }
'{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
'{   ADDITIONAL RESTRICTIONS.                                        }
'{                                                                   }
'{*******************************************************************}
#End Region

Option Compare Binary
Option Explicit On 
Option Strict On

Imports System.Data.SqlClient

Namespace Disbursement.Automated.Process
    Friend Class ClientSelection

        Protected Property context As Disbursement.Common.IDisbursementContext
            Get
                Return DirectCast(ParentForm, Disbursement.Common.ISupportContext).Context
            End Get
            Set(value As Disbursement.Common.IDisbursementContext)
                Throw New NotImplementedException()
            End Set
        End Property

        Private _SelectionClause As String = String.Empty
        Public ReadOnly Property SelectionClause() As String
            Get
                Return _SelectionClause
            End Get
        End Property

        Public Enum SelectionCriteria As System.Int32
            All_Clients
            Client_ID
            Client_LastName
            TrustBalance
            Counselor
            Office
            CSR
        End Enum

        Public Event Cancelled As EventHandler
        Public Event Selected As EventHandler

        Public Sub ReadForm()
            AddHandler Button_OK.Click, AddressOf Button_OK_Click
            AddHandler Button_Cancel.Click, AddressOf Button_Cancel_Click

            ' Populate the list of items
            With SelectionList
                With .Properties.Items()
                    .Add(New DebtPlus.Data.Controls.ComboboxItem("All Clients", SelectionCriteria.All_Clients))
                    .Add(New DebtPlus.Data.Controls.ComboboxItem("Client IDs in the range between", SelectionCriteria.Client_ID))
                    .Add(New DebtPlus.Data.Controls.ComboboxItem("Client last names between", SelectionCriteria.Client_LastName))
                    .Add(New DebtPlus.Data.Controls.ComboboxItem("Client trust balances between", SelectionCriteria.TrustBalance))
                    .Add(New DebtPlus.Data.Controls.ComboboxItem("Counselor is equal to", SelectionCriteria.Counselor))
                    .Add(New DebtPlus.Data.Controls.ComboboxItem("Office is equal to", SelectionCriteria.Office))
                    .Add(New DebtPlus.Data.Controls.ComboboxItem("CSR is equal to", SelectionCriteria.CSR))
                End With
                .SelectedIndex = 0
            End With

            AddHandler SelectionList.SelectedIndexChanged, AddressOf SelectionList_SelectedIndexChanged
            LoadParameters()
        End Sub

        Private Sub Button_Cancel_Click(ByVal sender As Object, ByVal e As System.EventArgs)
            RaiseCancelled()
        End Sub

        Protected Sub RaiseCancelled()
            RaiseEvent Cancelled(Me, EventArgs.Empty)
        End Sub

        Private Sub Button_OK_Click(ByVal sender As Object, ByVal e As System.EventArgs)
            RaiseSelected()
        End Sub

        Protected Sub RaiseSelected()
            Dim sb As New System.Text.StringBuilder

            ' Determine the selection criteria
            Select Case CType(CType(SelectionList.SelectedItem, DebtPlus.Data.Controls.ComboboxItem).value, SelectionCriteria)
                Case SelectionCriteria.All_Clients
                    ' do nothing

                Case SelectionCriteria.Client_ID
                    Dim ClientID As System.Int32
                    If Int32.TryParse(Parameter1.Text.Trim(), ClientID) AndAlso ClientID >= 0 Then sb.AppendFormat(" AND client>={0:f0}", ClientID)
                    If Int32.TryParse(Parameter2.Text.Trim(), ClientID) AndAlso ClientID >= 0 Then sb.AppendFormat(" AND client<={0:f0}", ClientID)

                Case SelectionCriteria.Client_LastName
                    With Parameter1
                        If .SelectedIndex >= 0 Then sb.AppendFormat(" AND last_name>='{0}'", .Text)
                    End With

                    With Parameter2
                        If .SelectedIndex >= 0 Then sb.AppendFormat(" AND last_name<='{0}'", .Text)
                    End With

                Case SelectionCriteria.TrustBalance
                    With Parameter1
                        If .SelectedIndex >= 0 Then sb.AppendFormat(" AND held_in_trust>={0:f2}", Convert.ToDecimal(.EditValue))
                    End With

                    With Parameter2
                        If .SelectedIndex >= 0 Then sb.AppendFormat(" AND held_in_trust<={0:f2}", Convert.ToDecimal(.EditValue))
                    End With

                Case SelectionCriteria.Counselor
                    With Parameter1
                        sb.AppendFormat(" AND counselor={0:f0}", Convert.ToInt32(CType(.SelectedItem, DebtPlus.Data.Controls.ComboboxItem).value))
                    End With

                Case SelectionCriteria.Office
                    With Parameter1
                        sb.AppendFormat(" AND office={0:f0}", Convert.ToInt32(CType(.SelectedItem, DebtPlus.Data.Controls.ComboboxItem).value))
                    End With

                Case SelectionCriteria.CSR
                    With Parameter1
                        sb.AppendFormat(" AND csr={0:f0}", Convert.ToInt32(CType(.SelectedItem, DebtPlus.Data.Controls.ComboboxItem).value))
                    End With
            End Select

            ' If the checkbox is set then include only items which have no disbursement register
            If OnlyNonDisbursedClients.Checked Then
                sb.Append(" AND client_register is null")
            End If

            _SelectionClause = sb.ToString()
            RaiseEvent Selected(Me, EventArgs.Empty)
        End Sub

        Private Sub SelectionList_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
            LoadParameters()
        End Sub

        Private Sub LoadParameters()
            With Parameter1
                .SelectedIndex = -1
                .Text = String.Empty

                With .Properties.DisplayFormat ' Remove the client formatting
                    .FormatType = DevExpress.Utils.FormatType.None
                End With
            End With

            With Parameter2
                .SelectedIndex = -1
                .Text = String.Empty

                With .Properties.DisplayFormat ' Remove the client formatting
                    .FormatType = DevExpress.Utils.FormatType.None
                End With
            End With

            Select Case CType(CType(SelectionList.SelectedItem, DebtPlus.Data.Controls.ComboboxItem).value, SelectionCriteria)
                Case SelectionCriteria.All_Clients
                    Parameter1.Visible = False
                    Parameter2.Visible = False
                    LabelParameter2.Visible = False

                Case SelectionCriteria.Client_ID
                    Parameter1.Visible = True
                    Parameter2.Visible = True
                    LabelParameter2.Visible = True

                    With Parameter1.Properties
                        .Items.Clear()
                        .TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.Standard
                        .Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
                        With .Mask
                            .EditMask = "f0"
                            .MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric
                            .BeepOnError = True
                        End With

                        With .DisplayFormat
                            .FormatType = DevExpress.Utils.FormatType.Custom
                            .Format = New DebtPlus.Utils.Format.Client.CustomFormatter
                            .FormatString = "0000000"
                        End With
                    End With

                    With Parameter2.Properties
                        .Items.Clear()
                        .TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.Standard
                        .Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
                        With .Mask
                            .EditMask = "f0"
                            .MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric
                            .BeepOnError = True
                        End With

                        With .DisplayFormat
                            .FormatType = DevExpress.Utils.FormatType.Custom
                            .Format = New DebtPlus.Utils.Format.Client.CustomFormatter
                            .FormatString = "0000000"
                        End With
                    End With

                Case SelectionCriteria.Client_LastName
                    Parameter1.Visible = True
                    Parameter2.Visible = True
                    LabelParameter2.Visible = True

                    With Parameter1
                        With .Properties
                            .Items.Clear()
                            .Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
                            .TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor
                            .Items.AddRange("ABCDEFGHIJKLMNOPQRSTUVWXYZ".ToCharArray)
                        End With
                        .SelectedIndex = 0
                    End With

                    With Parameter2
                        With .Properties
                            .Items.Clear()
                            .Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
                            .TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor
                            .Items.AddRange("ABCDEFGHIJKLMNOPQRSTUVWXYZ".ToCharArray)
                        End With
                        .SelectedIndex = .Properties.Items.Count - 1
                    End With

                Case SelectionCriteria.Counselor
                    Parameter1.Visible = True
                    Parameter2.Visible = False
                    LabelParameter2.Visible = False

                    With Parameter1
                        With .Properties
                            .Items.Clear()
                            .Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
                            .TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor
                            .Items.AddRange(context.CounselorList)
                        End With
                        .SelectedIndex = 0
                    End With

                Case SelectionCriteria.CSR
                    Parameter1.Visible = True
                    Parameter2.Visible = False
                    LabelParameter2.Visible = False

                    With Parameter1
                        With .Properties
                            .Items.Clear()
                            .Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
                            .TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor
                            .Items.AddRange(context.CSRList)
                        End With
                        .SelectedIndex = 0
                    End With

                Case SelectionCriteria.Office
                    Parameter1.Visible = True
                    Parameter2.Visible = False
                    LabelParameter2.Visible = False

                    With Parameter1
                        With .Properties
                            .Items.Clear()
                            .Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
                            .TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor
                            .Items.AddRange(context.OfficeList)
                        End With
                        .SelectedIndex = 0
                    End With

                Case SelectionCriteria.TrustBalance
                    Parameter1.Visible = True
                    Parameter2.Visible = True
                    LabelParameter2.Visible = True

                    With Parameter1.Properties
                        .Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
                        .Items.Clear()
                        .TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.Standard
                        .Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
                        With .Mask
                            .EditMask = "n2"
                            .MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric
                            .BeepOnError = True
                        End With
                        With .DisplayFormat
                            .FormatType = DevExpress.Utils.FormatType.Numeric
                            .FormatString = "c"
                        End With
                    End With

                    With Parameter2.Properties
                        .Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
                        .Items.Clear()
                        .TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.Standard
                        .Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
                        With .Mask
                            .EditMask = "n2"
                            .MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric
                            .BeepOnError = True
                        End With
                        With .DisplayFormat
                            .FormatType = DevExpress.Utils.FormatType.Numeric
                            .FormatString = "c"
                        End With
                    End With
            End Select
        End Sub
    End Class
End Namespace