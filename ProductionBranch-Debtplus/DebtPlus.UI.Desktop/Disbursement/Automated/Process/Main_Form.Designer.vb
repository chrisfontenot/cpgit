Namespace Disbursement.Automated.Process
    <Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
    Partial Class Main_Form
        Inherits DebtPlus.Data.Forms.DebtPlusForm

        'Form overrides dispose to clean up the component list.
        <System.Diagnostics.DebuggerNonUserCode()> _
        Protected Overrides Sub Dispose(ByVal disposing As Boolean)
            Try
                If disposing AndAlso components IsNot Nothing Then
                    components.Dispose()
                End If
            Finally
                MyBase.Dispose(disposing)
            End Try
        End Sub

        'Required by the Windows Form Designer
        Private components As System.ComponentModel.IContainer

        'NOTE: The following procedure is required by the Windows Form Designer
        'It can be modified using the Windows Form Designer.  
        'Do not modify it using the code editor.
        <System.Diagnostics.DebuggerStepThrough()> _
        Private Sub InitializeComponent()
            Me.ProcessControl1 = New DebtPlus.UI.Desktop.Disbursement.Automated.Process.ProcessControl()
            Me.DisbursementBatchControl1 = New DebtPlus.UI.Desktop.Disbursement.Automated.Process.DisbursementBatchControl()
            Me.ClientSelection1 = New DebtPlus.UI.Desktop.Disbursement.Automated.Process.ClientSelection()
            CType(Me.DxErrorProvider1, System.ComponentModel.ISupportInitialize).BeginInit()
            Me.SuspendLayout()
            '
            'DefaultLookAndFeel1
            '
            Me.DefaultLookAndFeel1.LookAndFeel.SkinName = "McSkin"
            '
            'ProcessControl1
            '
            Me.ProcessControl1.Dock = System.Windows.Forms.DockStyle.Fill
            Me.ProcessControl1.Location = New System.Drawing.Point(0, 0)
            Me.ProcessControl1.Name = "ProcessControl1"
            Me.ProcessControl1.Size = New System.Drawing.Size(472, 430)
            Me.ProcessControl1.TabIndex = 3
            '
            'DisbursementBatchControl1
            '
            Me.DisbursementBatchControl1.Dock = System.Windows.Forms.DockStyle.Fill
            Me.DisbursementBatchControl1.Location = New System.Drawing.Point(0, 0)
            Me.DisbursementBatchControl1.Name = "DisbursementBatchControl1"
            Me.DisbursementBatchControl1.Size = New System.Drawing.Size(472, 430)
            Me.DisbursementBatchControl1.TabIndex = 4
            '
            'ClientSelection1
            '
            Me.ClientSelection1.Dock = System.Windows.Forms.DockStyle.Fill
            Me.ClientSelection1.Location = New System.Drawing.Point(0, 0)
            Me.ClientSelection1.Name = "ClientSelection1"
            Me.ClientSelection1.Size = New System.Drawing.Size(472, 430)
            Me.ClientSelection1.TabIndex = 5
            '
            'Main_Form
            '
            Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
            Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
            Me.ClientSize = New System.Drawing.Size(472, 430)
            Me.Controls.Add(Me.ProcessControl1)
            Me.Controls.Add(Me.ClientSelection1)
            Me.Controls.Add(Me.DisbursementBatchControl1)
            Me.Name = "Main_Form"
            Me.Text = "Auto Disbursement"
            CType(Me.DxErrorProvider1, System.ComponentModel.ISupportInitialize).EndInit()
            Me.ResumeLayout(False)

        End Sub
        Friend WithEvents ProcessControl1 As ProcessControl
        Friend WithEvents DisbursementBatchControl1 As DisbursementBatchControl
        Friend WithEvents ClientSelection1 As ClientSelection
    End Class
End Namespace