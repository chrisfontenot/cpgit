#Region "Copyright 2000-2012 DebtPlus, L.L.C."
'{*******************************************************************}
'{                                                                   }
'{       DebtPlus Debt Management System                             }
'{                                                                   }
'{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
'{       ALL RIGHTS RESERVED                                         }
'{                                                                   }
'{   The entire contents of this file is protected by U.S. and       }
'{   International Copyright Laws. Unauthorized reproduction,        }
'{   reverse-engineering, and distribution of all or any portion of  }
'{   the code contained in this file is strictly prohibited and may  }
'{   result in severe civil and criminal penalties and will be       }
'{   prosecuted to the maximum extent possible under the law.        }
'{                                                                   }
'{   RESTRICTIONS                                                    }
'{                                                                   }
'{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
'{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
'{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
'{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
'{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
'{                                                                   }
'{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
'{   ADDITIONAL RESTRICTIONS.                                        }
'{                                                                   }
'{*******************************************************************}
#End Region

Namespace Disbursement.Automated.Process
    Friend Class Main_Form
        Implements Disbursement.Common.ISupportContext

        Public Property context As Disbursement.Common.IDisbursementContext Implements Disbursement.Common.ISupportContext.Context

        Private Sub New()
            MyBase.New()
            InitializeComponent()
        End Sub

        Public Sub New(ByVal context As Disbursement.Common.IDisbursementContext)
            MyClass.New()
            Me.context = context
            AddHandler Load, AddressOf Form_Load
            AddHandler DisbursementBatchControl1.Cancelled, AddressOf DisbursementRegisterControl1_Cancelled
            AddHandler DisbursementBatchControl1.Selected, AddressOf DisbursementRegisterControl1_Selected
            AddHandler ClientSelection1.Selected, AddressOf ClientSelection1_Selected
            AddHandler ProcessControl1.Completed, AddressOf ProcessControl1_Completed
            AddHandler ClientSelection1.Cancelled, AddressOf DisbursementRegisterControl1_Cancelled
            AddHandler ProcessControl1.Cancelled, AddressOf DisbursementRegisterControl1_Cancelled
        End Sub

        Private Sub Form_Load(ByVal sender As Object, ByVal e As System.EventArgs)
            ClientSelection1.Visible = False
            ProcessControl1.Visible = False

            If DirectCast(context.ap, ArgParser).BatchID <= 0 Then
                DisbursementBatchControl1.Visible = True
                DisbursementBatchControl1.ReadForm()
            Else
                ClientSelection1.Visible = True
                ClientSelection1.ReadForm()
            End If
        End Sub

        Private Sub DisbursementRegisterControl1_Cancelled(ByVal sender As Object, ByVal e As System.EventArgs)
            context.MasterQuit = True
            DialogResult = Windows.Forms.DialogResult.Cancel
        End Sub

        Private Sub DisbursementRegisterControl1_Selected(ByVal sender As Object, ByVal e As System.EventArgs)
            DisbursementBatchControl1.Visible = False
            DirectCast(context.ap, ArgParser).BatchID = DisbursementBatchControl1.BatchID

            ClientSelection1.Visible = True
            ClientSelection1.ReadForm()
        End Sub

        Private Sub ClientSelection1_Selected(ByVal sender As Object, ByVal e As System.EventArgs)

            DirectCast(context.ap, ArgParser).SelectionClause = ClientSelection1.SelectionClause
            If ProcessControl1.ReadForm() Then
                ClientSelection1.Visible = False
                ProcessControl1.Visible = True
            End If
        End Sub

        Private Sub ProcessControl1_Completed(sender As Object, e As System.EventArgs)
            context.MasterQuit = True
            DialogResult = Windows.Forms.DialogResult.OK
        End Sub
    End Class
End Namespace