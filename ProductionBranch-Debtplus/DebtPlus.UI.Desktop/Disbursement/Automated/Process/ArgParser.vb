Option Compare Binary
Option Explicit On 
Option Strict On

Namespace Disbursement.Automated.Process
    Friend Class ArgParser
        Inherits DebtPlus.Utils.ArgParserBase
        Public Property BatchID() As Int32
        Public Property SelectionClause() As String

        Public Sub New()
            MyBase.New(New String() {"b"})
        End Sub

        Public Sub New(ByVal switchSymbols As String())
            MyBase.New(switchSymbols)
        End Sub

        Public Sub New(ByVal switchSymbols As String(), ByVal caseSensitiveSwitches As Boolean)
            MyBase.New(switchSymbols, caseSensitiveSwitches)
        End Sub

        Public Sub New(ByVal switchSymbols As String(), ByVal caseSensitiveSwitches As Boolean, ByVal switchChars As String())
            MyBase.New(switchSymbols, caseSensitiveSwitches, switchChars)
        End Sub

        Protected Overrides Function OnSwitch(ByVal switchName As String, ByVal switchValue As String) As SwitchStatus
            Dim ss As SwitchStatus = DebtPlus.Utils.ArgParserBase.SwitchStatus.NoError
            Select Case switchName
                Case "b"
                    Dim dblTemp As Double
                    If Double.TryParse(switchValue, Globalization.NumberStyles.Integer, System.Globalization.CultureInfo.InvariantCulture, dblTemp) Then
                        If dblTemp >= 0 AndAlso dblTemp < Convert.ToDouble(Int32.MaxValue) Then
                            BatchID = Convert.ToInt32(dblTemp)
                            Exit Select
                        End If
                    End If
                    ss = DebtPlus.Utils.ArgParserBase.SwitchStatus.YesError

                Case "?"
                    ss = DebtPlus.Utils.ArgParserBase.SwitchStatus.ShowUsage

                Case Else
                    ss = DebtPlus.Utils.ArgParserBase.SwitchStatus.YesError
            End Select

            Return ss
        End Function

        Protected Overrides Sub OnUsage(ByVal errorInfo As String)
            ' deprecated
        End Sub
    End Class

End Namespace