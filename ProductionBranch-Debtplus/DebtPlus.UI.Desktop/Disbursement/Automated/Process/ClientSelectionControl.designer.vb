Namespace Disbursement.Automated.Process
    <Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
    Partial Class ClientSelection
        Inherits DevExpress.XtraEditors.XtraUserControl

        'UserControl overrides dispose to clean up the component list.
        <System.Diagnostics.DebuggerNonUserCode()> _
        Protected Overrides Sub Dispose(ByVal disposing As Boolean)
            Try
                If disposing AndAlso components IsNot Nothing Then
                    components.Dispose()
                End If
            Finally
                MyBase.Dispose(disposing)
            End Try
        End Sub

        'Required by the Windows Form Designer
        Private components As System.ComponentModel.IContainer

        'NOTE: The following procedure is required by the Windows Form Designer
        'It can be modified using the Windows Form Designer.  
        'Do not modify it using the code editor.
        <System.Diagnostics.DebuggerStepThrough()> _
        Private Sub InitializeComponent()
            Me.LabelControl1 = New DevExpress.XtraEditors.LabelControl
            Me.LabelParameter2 = New DevExpress.XtraEditors.LabelControl
            Me.SelectionList = New DevExpress.XtraEditors.ComboBoxEdit
            Me.Parameter1 = New DevExpress.XtraEditors.ComboBoxEdit
            Me.Parameter2 = New DevExpress.XtraEditors.ComboBoxEdit
            Me.OnlyNonDisbursedClients = New DevExpress.XtraEditors.CheckEdit
            Me.Button_OK = New DevExpress.XtraEditors.SimpleButton
            Me.Button_Cancel = New DevExpress.XtraEditors.SimpleButton
            CType(Me.SelectionList.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.Parameter1.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.Parameter2.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.OnlyNonDisbursedClients.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
            Me.SuspendLayout()
            '
            'LabelControl1
            '
            Me.LabelControl1.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                        Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
            Me.LabelControl1.Appearance.Options.UseTextOptions = True
            Me.LabelControl1.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
            Me.LabelControl1.Appearance.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Top
            Me.LabelControl1.Appearance.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap
            Me.LabelControl1.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None
            Me.LabelControl1.Location = New System.Drawing.Point(10, 3)
            Me.LabelControl1.Name = "LabelControl1"
            Me.LabelControl1.Size = New System.Drawing.Size(382, 55)
            Me.LabelControl1.TabIndex = 0
            Me.LabelControl1.Text = "The next step is to decide which group of clients that you wish to include in you" & _
                "r selection now. The choice of clients should not overlap between workstations p" & _
                "rocessing this disbursement."
            '
            'LabelParameter2
            '
            Me.LabelParameter2.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
            Me.LabelParameter2.Appearance.Options.UseTextOptions = True
            Me.LabelParameter2.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center
            Me.LabelParameter2.Appearance.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center
            Me.LabelParameter2.Location = New System.Drawing.Point(270, 67)
            Me.LabelParameter2.Name = "LabelParameter2"
            Me.LabelParameter2.Size = New System.Drawing.Size(18, 13)
            Me.LabelParameter2.TabIndex = 1
            Me.LabelParameter2.Text = "and"
            Me.LabelParameter2.Visible = False
            '
            'SelectionList
            '
            Me.SelectionList.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                        Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
            Me.SelectionList.Location = New System.Drawing.Point(8, 64)
            Me.SelectionList.Name = "SelectionList"
            Me.SelectionList.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
            Me.SelectionList.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor
            Me.SelectionList.Size = New System.Drawing.Size(160, 20)
            Me.SelectionList.TabIndex = 2
            '
            'Parameter1
            '
            Me.Parameter1.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
            Me.Parameter1.Location = New System.Drawing.Point(176, 64)
            Me.Parameter1.Name = "Parameter1"
            Me.Parameter1.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
            Me.Parameter1.Size = New System.Drawing.Size(88, 20)
            Me.Parameter1.TabIndex = 3
            Me.Parameter1.Visible = False
            '
            'Parameter2
            '
            Me.Parameter2.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
            Me.Parameter2.Location = New System.Drawing.Point(296, 64)
            Me.Parameter2.Name = "Parameter2"
            Me.Parameter2.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
            Me.Parameter2.Size = New System.Drawing.Size(96, 20)
            Me.Parameter2.TabIndex = 4
            Me.Parameter2.Visible = False
            '
            'OnlyNonDisbursedClients
            '
            Me.OnlyNonDisbursedClients.Anchor = CType(((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left) _
                        Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
            Me.OnlyNonDisbursedClients.EditValue = True
            Me.OnlyNonDisbursedClients.Location = New System.Drawing.Point(8, 88)
            Me.OnlyNonDisbursedClients.Name = "OnlyNonDisbursedClients"
            Me.OnlyNonDisbursedClients.Properties.Caption = "Only include clients who have not been disbursed in this batch"
            Me.OnlyNonDisbursedClients.Size = New System.Drawing.Size(376, 19)
            Me.OnlyNonDisbursedClients.TabIndex = 5
            '
            'Button_OK
            '
            Me.Button_OK.Anchor = System.Windows.Forms.AnchorStyles.Bottom
            Me.Button_OK.Location = New System.Drawing.Point(123, 120)
            Me.Button_OK.Name = "Button_OK"
            Me.Button_OK.Size = New System.Drawing.Size(75, 23)
            Me.Button_OK.TabIndex = 6
            Me.Button_OK.Text = "&OK"
            '
            'Button_Cancel
            '
            Me.Button_Cancel.Anchor = System.Windows.Forms.AnchorStyles.Bottom
            Me.Button_Cancel.Location = New System.Drawing.Point(219, 120)
            Me.Button_Cancel.Name = "Button_Cancel"
            Me.Button_Cancel.Size = New System.Drawing.Size(75, 23)
            Me.Button_Cancel.TabIndex = 7
            Me.Button_Cancel.Text = "&Cancel"
            '
            'ClientSelection
            '
            Me.Controls.Add(Me.Button_Cancel)
            Me.Controls.Add(Me.Button_OK)
            Me.Controls.Add(Me.OnlyNonDisbursedClients)
            Me.Controls.Add(Me.Parameter2)
            Me.Controls.Add(Me.Parameter1)
            Me.Controls.Add(Me.SelectionList)
            Me.Controls.Add(Me.LabelParameter2)
            Me.Controls.Add(Me.LabelControl1)
            Me.Name = "ClientSelection"
            Me.Size = New System.Drawing.Size(416, 150)
            CType(Me.SelectionList.Properties, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.Parameter1.Properties, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.Parameter2.Properties, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.OnlyNonDisbursedClients.Properties, System.ComponentModel.ISupportInitialize).EndInit()
            Me.ResumeLayout(False)
            Me.PerformLayout()

        End Sub
        Friend WithEvents LabelControl1 As DevExpress.XtraEditors.LabelControl
        Friend WithEvents LabelParameter2 As DevExpress.XtraEditors.LabelControl
        Friend WithEvents SelectionList As DevExpress.XtraEditors.ComboBoxEdit
        Friend WithEvents Parameter1 As DevExpress.XtraEditors.ComboBoxEdit
        Friend WithEvents Parameter2 As DevExpress.XtraEditors.ComboBoxEdit
        Friend WithEvents OnlyNonDisbursedClients As DevExpress.XtraEditors.CheckEdit
        Friend WithEvents Button_OK As DevExpress.XtraEditors.SimpleButton
        Friend WithEvents Button_Cancel As DevExpress.XtraEditors.SimpleButton
    End Class
End Namespace
