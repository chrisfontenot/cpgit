Namespace Disbursement.Automated.Process
    <Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
    Partial Class ProcessControl
        Inherits DevExpress.XtraEditors.XtraUserControl

        'UserControl overrides dispose to clean up the component list.
        <System.Diagnostics.DebuggerNonUserCode()> _
        Protected Overrides Sub Dispose(ByVal disposing As Boolean)
            Try
                If disposing Then
                    If components IsNot Nothing Then components.Dispose()
                    ds.Dispose()
                End If
            Finally
                MyBase.Dispose(disposing)
            End Try
        End Sub

        'Required by the Windows Form Designer
        Private components As System.ComponentModel.IContainer

        'NOTE: The following procedure is required by the Windows Form Designer
        'It can be modified using the Windows Form Designer.  
        'Do not modify it using the code editor.
        <System.Diagnostics.DebuggerStepThrough()> _
        Private Sub InitializeComponent()
            Me.Button_Cancel = New DevExpress.XtraEditors.SimpleButton
            Me.Button_Start = New DevExpress.XtraEditors.SimpleButton
            Me.GroupControl4 = New DevExpress.XtraEditors.GroupControl
            Me.less_yes = New DevExpress.XtraEditors.ComboBoxEdit
            Me.LabelControl12 = New DevExpress.XtraEditors.LabelControl
            Me.less_no = New DevExpress.XtraEditors.ComboBoxEdit
            Me.LabelControl13 = New DevExpress.XtraEditors.LabelControl
            Me.GroupControl3 = New DevExpress.XtraEditors.GroupControl
            Me.more_yes = New DevExpress.XtraEditors.ComboBoxEdit
            Me.LabelControl10 = New DevExpress.XtraEditors.LabelControl
            Me.more_no = New DevExpress.XtraEditors.ComboBoxEdit
            Me.LabelControl11 = New DevExpress.XtraEditors.LabelControl
            Me.GroupControl2 = New DevExpress.XtraEditors.GroupControl
            Me.normal_yes = New DevExpress.XtraEditors.ComboBoxEdit
            Me.LabelControl9 = New DevExpress.XtraEditors.LabelControl
            Me.normal_no = New DevExpress.XtraEditors.ComboBoxEdit
            Me.LabelControl8 = New DevExpress.XtraEditors.LabelControl
            Me.GroupControl1 = New DevExpress.XtraEditors.GroupControl
            Me.lbl_total_count = New DevExpress.XtraEditors.LabelControl
            Me.ProgressBarControl1 = New DevExpress.XtraEditors.ProgressBarControl
            Me.lbl_amount = New DevExpress.XtraEditors.LabelControl
            Me.lbl_count = New DevExpress.XtraEditors.LabelControl
            Me.lbl_client = New DevExpress.XtraEditors.LabelControl
            Me.LabelControl4 = New DevExpress.XtraEditors.LabelControl
            Me.LabelControl3 = New DevExpress.XtraEditors.LabelControl
            Me.LabelControl2 = New DevExpress.XtraEditors.LabelControl
            Me.LabelControl1 = New DevExpress.XtraEditors.LabelControl
            CType(Me.GroupControl4, System.ComponentModel.ISupportInitialize).BeginInit()
            Me.GroupControl4.SuspendLayout()
            CType(Me.less_yes.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.less_no.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.GroupControl3, System.ComponentModel.ISupportInitialize).BeginInit()
            Me.GroupControl3.SuspendLayout()
            CType(Me.more_yes.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.more_no.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.GroupControl2, System.ComponentModel.ISupportInitialize).BeginInit()
            Me.GroupControl2.SuspendLayout()
            CType(Me.normal_yes.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.normal_no.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.GroupControl1, System.ComponentModel.ISupportInitialize).BeginInit()
            Me.GroupControl1.SuspendLayout()
            CType(Me.ProgressBarControl1.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
            Me.SuspendLayout()
            '
            'Button_Cancel
            '
            Me.Button_Cancel.Anchor = System.Windows.Forms.AnchorStyles.Bottom
            Me.Button_Cancel.Location = New System.Drawing.Point(264, 393)
            Me.Button_Cancel.Name = "Button_Cancel"
            Me.Button_Cancel.Size = New System.Drawing.Size(75, 23)
            Me.Button_Cancel.TabIndex = 13
            Me.Button_Cancel.Text = "&Cancel"
            '
            'Button_Start
            '
            Me.Button_Start.Anchor = System.Windows.Forms.AnchorStyles.Bottom
            Me.Button_Start.Enabled = False
            Me.Button_Start.Location = New System.Drawing.Point(139, 393)
            Me.Button_Start.Name = "Button_Start"
            Me.Button_Start.Size = New System.Drawing.Size(75, 23)
            Me.Button_Start.TabIndex = 12
            Me.Button_Start.Text = "&Start"
            '
            'GroupControl4
            '
            Me.GroupControl4.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                        Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
            Me.GroupControl4.Appearance.ForeColor = System.Drawing.SystemColors.ControlText
            Me.GroupControl4.Appearance.Options.UseForeColor = True
            Me.GroupControl4.Controls.Add(Me.less_yes)
            Me.GroupControl4.Controls.Add(Me.LabelControl12)
            Me.GroupControl4.Controls.Add(Me.less_no)
            Me.GroupControl4.Controls.Add(Me.LabelControl13)
            Me.GroupControl4.Location = New System.Drawing.Point(12, 305)
            Me.GroupControl4.Name = "GroupControl4"
            Me.GroupControl4.Size = New System.Drawing.Size(440, 72)
            Me.GroupControl4.TabIndex = 11
            Me.GroupControl4.Text = "If there is less money in the trust than what is normally needed"
            '
            'less_yes
            '
            Me.less_yes.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                        Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
            Me.less_yes.Location = New System.Drawing.Point(208, 48)
            Me.less_yes.Name = "less_yes"
            Me.less_yes.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
            Me.less_yes.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor
            Me.less_yes.Size = New System.Drawing.Size(224, 20)
            Me.less_yes.TabIndex = 3
            '
            'LabelControl12
            '
            Me.LabelControl12.Appearance.ForeColor = System.Drawing.Color.Red
            Me.LabelControl12.Appearance.Options.UseForeColor = True
            Me.LabelControl12.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.Horizontal
            Me.LabelControl12.Location = New System.Drawing.Point(24, 52)
            Me.LabelControl12.Name = "LabelControl12"
            Me.LabelControl12.Size = New System.Drawing.Size(164, 13)
            Me.LabelControl12.TabIndex = 2
            Me.LabelControl12.Text = "And there are disbursement notes"
            '
            'less_no
            '
            Me.less_no.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                        Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
            Me.less_no.Location = New System.Drawing.Point(208, 24)
            Me.less_no.Name = "less_no"
            Me.less_no.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
            Me.less_no.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor
            Me.less_no.Size = New System.Drawing.Size(224, 20)
            Me.less_no.TabIndex = 1
            '
            'LabelControl13
            '
            Me.LabelControl13.Appearance.ForeColor = System.Drawing.Color.Green
            Me.LabelControl13.Appearance.Options.UseForeColor = True
            Me.LabelControl13.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.Horizontal
            Me.LabelControl13.Location = New System.Drawing.Point(24, 28)
            Me.LabelControl13.Name = "LabelControl13"
            Me.LabelControl13.Size = New System.Drawing.Size(179, 13)
            Me.LabelControl13.TabIndex = 0
            Me.LabelControl13.Text = "And there are no disbursement notes"
            '
            'GroupControl3
            '
            Me.GroupControl3.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                        Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
            Me.GroupControl3.Appearance.ForeColor = System.Drawing.SystemColors.ControlText
            Me.GroupControl3.Appearance.Options.UseForeColor = True
            Me.GroupControl3.Controls.Add(Me.more_yes)
            Me.GroupControl3.Controls.Add(Me.LabelControl10)
            Me.GroupControl3.Controls.Add(Me.more_no)
            Me.GroupControl3.Controls.Add(Me.LabelControl11)
            Me.GroupControl3.Location = New System.Drawing.Point(12, 225)
            Me.GroupControl3.Name = "GroupControl3"
            Me.GroupControl3.Size = New System.Drawing.Size(440, 72)
            Me.GroupControl3.TabIndex = 10
            Me.GroupControl3.Text = "If there is more money in the trust than what is normal"
            '
            'more_yes
            '
            Me.more_yes.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                        Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
            Me.more_yes.Location = New System.Drawing.Point(208, 48)
            Me.more_yes.Name = "more_yes"
            Me.more_yes.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
            Me.more_yes.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor
            Me.more_yes.Size = New System.Drawing.Size(224, 20)
            Me.more_yes.TabIndex = 3
            '
            'LabelControl10
            '
            Me.LabelControl10.Appearance.ForeColor = System.Drawing.Color.Red
            Me.LabelControl10.Appearance.Options.UseForeColor = True
            Me.LabelControl10.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.Horizontal
            Me.LabelControl10.Location = New System.Drawing.Point(24, 52)
            Me.LabelControl10.Name = "LabelControl10"
            Me.LabelControl10.Size = New System.Drawing.Size(164, 13)
            Me.LabelControl10.TabIndex = 2
            Me.LabelControl10.Text = "And there are disbursement notes"
            '
            'more_no
            '
            Me.more_no.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                        Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
            Me.more_no.Location = New System.Drawing.Point(208, 24)
            Me.more_no.Name = "more_no"
            Me.more_no.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
            Me.more_no.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor
            Me.more_no.Size = New System.Drawing.Size(224, 20)
            Me.more_no.TabIndex = 1
            '
            'LabelControl11
            '
            Me.LabelControl11.Appearance.ForeColor = System.Drawing.Color.Green
            Me.LabelControl11.Appearance.Options.UseForeColor = True
            Me.LabelControl11.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.Horizontal
            Me.LabelControl11.Location = New System.Drawing.Point(24, 28)
            Me.LabelControl11.Name = "LabelControl11"
            Me.LabelControl11.Size = New System.Drawing.Size(179, 13)
            Me.LabelControl11.TabIndex = 0
            Me.LabelControl11.Text = "And there are no disbursement notes"
            '
            'GroupControl2
            '
            Me.GroupControl2.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                        Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
            Me.GroupControl2.Appearance.BackColor = System.Drawing.Color.Transparent
            Me.GroupControl2.Appearance.BackColor2 = System.Drawing.Color.PaleGreen
            Me.GroupControl2.Appearance.ForeColor = System.Drawing.SystemColors.ControlText
            Me.GroupControl2.Appearance.Options.UseBackColor = True
            Me.GroupControl2.Appearance.Options.UseForeColor = True
            Me.GroupControl2.AppearanceCaption.BackColor = System.Drawing.Color.Transparent
            Me.GroupControl2.AppearanceCaption.BackColor2 = System.Drawing.Color.Transparent
            Me.GroupControl2.AppearanceCaption.Options.UseBackColor = True
            Me.GroupControl2.Controls.Add(Me.normal_yes)
            Me.GroupControl2.Controls.Add(Me.LabelControl9)
            Me.GroupControl2.Controls.Add(Me.normal_no)
            Me.GroupControl2.Controls.Add(Me.LabelControl8)
            Me.GroupControl2.Location = New System.Drawing.Point(12, 145)
            Me.GroupControl2.Name = "GroupControl2"
            Me.GroupControl2.Size = New System.Drawing.Size(440, 72)
            Me.GroupControl2.TabIndex = 9
            Me.GroupControl2.Text = "If the client paid the normal amount"
            '
            'normal_yes
            '
            Me.normal_yes.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                        Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
            Me.normal_yes.Location = New System.Drawing.Point(208, 48)
            Me.normal_yes.Name = "normal_yes"
            Me.normal_yes.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
            Me.normal_yes.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor
            Me.normal_yes.Size = New System.Drawing.Size(224, 20)
            Me.normal_yes.TabIndex = 3
            '
            'LabelControl9
            '
            Me.LabelControl9.Appearance.ForeColor = System.Drawing.Color.Red
            Me.LabelControl9.Appearance.Options.UseForeColor = True
            Me.LabelControl9.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.Horizontal
            Me.LabelControl9.Location = New System.Drawing.Point(24, 52)
            Me.LabelControl9.Name = "LabelControl9"
            Me.LabelControl9.Size = New System.Drawing.Size(164, 13)
            Me.LabelControl9.TabIndex = 2
            Me.LabelControl9.Text = "And there are disbursement notes"
            '
            'normal_no
            '
            Me.normal_no.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                        Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
            Me.normal_no.Location = New System.Drawing.Point(208, 24)
            Me.normal_no.Name = "normal_no"
            Me.normal_no.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
            Me.normal_no.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor
            Me.normal_no.Size = New System.Drawing.Size(224, 20)
            Me.normal_no.TabIndex = 1
            '
            'LabelControl8
            '
            Me.LabelControl8.Appearance.ForeColor = System.Drawing.Color.Green
            Me.LabelControl8.Appearance.Options.UseForeColor = True
            Me.LabelControl8.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.Horizontal
            Me.LabelControl8.Location = New System.Drawing.Point(24, 28)
            Me.LabelControl8.Name = "LabelControl8"
            Me.LabelControl8.Size = New System.Drawing.Size(179, 13)
            Me.LabelControl8.TabIndex = 0
            Me.LabelControl8.Text = "And there are no disbursement notes"
            '
            'GroupControl1
            '
            Me.GroupControl1.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                        Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
            Me.GroupControl1.Appearance.ForeColor = System.Drawing.SystemColors.ControlText
            Me.GroupControl1.Appearance.Options.UseForeColor = True
            Me.GroupControl1.Controls.Add(Me.lbl_total_count)
            Me.GroupControl1.Controls.Add(Me.ProgressBarControl1)
            Me.GroupControl1.Controls.Add(Me.lbl_amount)
            Me.GroupControl1.Controls.Add(Me.lbl_count)
            Me.GroupControl1.Controls.Add(Me.lbl_client)
            Me.GroupControl1.Controls.Add(Me.LabelControl4)
            Me.GroupControl1.Controls.Add(Me.LabelControl3)
            Me.GroupControl1.Controls.Add(Me.LabelControl2)
            Me.GroupControl1.Location = New System.Drawing.Point(12, 33)
            Me.GroupControl1.Name = "GroupControl1"
            Me.GroupControl1.Size = New System.Drawing.Size(440, 104)
            Me.GroupControl1.TabIndex = 8
            Me.GroupControl1.Text = "Disbursement Identification"
            '
            'lbl_total_count
            '
            Me.lbl_total_count.Appearance.Options.UseTextOptions = True
            Me.lbl_total_count.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
            Me.lbl_total_count.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None
            Me.lbl_total_count.Location = New System.Drawing.Point(278, 40)
            Me.lbl_total_count.Name = "lbl_total_count"
            Me.lbl_total_count.Size = New System.Drawing.Size(154, 13)
            Me.lbl_total_count.TabIndex = 7
            Me.lbl_total_count.Text = "out of 0,000,000,000"
            Me.lbl_total_count.UseMnemonic = False
            '
            'ProgressBarControl1
            '
            Me.ProgressBarControl1.Dock = System.Windows.Forms.DockStyle.Bottom
            Me.ProgressBarControl1.Location = New System.Drawing.Point(2, 78)
            Me.ProgressBarControl1.Name = "ProgressBarControl1"
            Me.ProgressBarControl1.Properties.DisplayFormat.FormatString = "P0"
            Me.ProgressBarControl1.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Custom
            Me.ProgressBarControl1.Properties.EditFormat.FormatString = "P0"
            Me.ProgressBarControl1.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Custom
            Me.ProgressBarControl1.Size = New System.Drawing.Size(436, 24)
            Me.ProgressBarControl1.TabIndex = 6
            '
            'lbl_amount
            '
            Me.lbl_amount.Appearance.Options.UseTextOptions = True
            Me.lbl_amount.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
            Me.lbl_amount.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None
            Me.lbl_amount.Location = New System.Drawing.Point(200, 56)
            Me.lbl_amount.Name = "lbl_amount"
            Me.lbl_amount.Size = New System.Drawing.Size(72, 13)
            Me.lbl_amount.TabIndex = 5
            Me.lbl_amount.Text = "$0,000,000.00"
            Me.lbl_amount.UseMnemonic = False
            '
            'lbl_count
            '
            Me.lbl_count.Appearance.Options.UseTextOptions = True
            Me.lbl_count.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
            Me.lbl_count.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None
            Me.lbl_count.Location = New System.Drawing.Point(200, 40)
            Me.lbl_count.Name = "lbl_count"
            Me.lbl_count.Size = New System.Drawing.Size(72, 13)
            Me.lbl_count.TabIndex = 4
            Me.lbl_count.Text = "0,000,000,000"
            Me.lbl_count.UseMnemonic = False
            '
            'lbl_client
            '
            Me.lbl_client.Appearance.Options.UseTextOptions = True
            Me.lbl_client.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
            Me.lbl_client.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None
            Me.lbl_client.Location = New System.Drawing.Point(200, 24)
            Me.lbl_client.Name = "lbl_client"
            Me.lbl_client.Size = New System.Drawing.Size(72, 13)
            Me.lbl_client.TabIndex = 3
            Me.lbl_client.Text = "0000000"
            Me.lbl_client.UseMnemonic = False
            '
            'LabelControl4
            '
            Me.LabelControl4.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.Horizontal
            Me.LabelControl4.Location = New System.Drawing.Point(48, 56)
            Me.LabelControl4.Name = "LabelControl4"
            Me.LabelControl4.Size = New System.Drawing.Size(121, 13)
            Me.LabelControl4.TabIndex = 2
            Me.LabelControl4.Text = "Dollar Amount Disbursed:"
            Me.LabelControl4.UseMnemonic = False
            '
            'LabelControl3
            '
            Me.LabelControl3.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.Horizontal
            Me.LabelControl3.Location = New System.Drawing.Point(48, 40)
            Me.LabelControl3.Name = "LabelControl3"
            Me.LabelControl3.Size = New System.Drawing.Size(139, 13)
            Me.LabelControl3.TabIndex = 1
            Me.LabelControl3.Text = "Number of clients processed:"
            Me.LabelControl3.UseMnemonic = False
            '
            'LabelControl2
            '
            Me.LabelControl2.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.Horizontal
            Me.LabelControl2.Location = New System.Drawing.Point(48, 24)
            Me.LabelControl2.Name = "LabelControl2"
            Me.LabelControl2.Size = New System.Drawing.Size(131, 13)
            Me.LabelControl2.TabIndex = 0
            Me.LabelControl2.Text = "Currently processing client:"
            Me.LabelControl2.UseMnemonic = False
            '
            'LabelControl1
            '
            Me.LabelControl1.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                        Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
            Me.LabelControl1.Appearance.Font = New System.Drawing.Font("Comic Sans MS", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
            Me.LabelControl1.Appearance.Options.UseFont = True
            Me.LabelControl1.Appearance.Options.UseTextOptions = True
            Me.LabelControl1.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center
            Me.LabelControl1.Appearance.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Top
            Me.LabelControl1.Appearance.TextOptions.WordWrap = DevExpress.Utils.WordWrap.NoWrap
            Me.LabelControl1.Location = New System.Drawing.Point(14, 8)
            Me.LabelControl1.Name = "LabelControl1"
            Me.LabelControl1.Size = New System.Drawing.Size(135, 19)
            Me.LabelControl1.TabIndex = 7
            Me.LabelControl1.Text = "Disbursement Process"
            '
            'ProcessControl
            '
            Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
            Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
            Me.Controls.Add(Me.Button_Cancel)
            Me.Controls.Add(Me.Button_Start)
            Me.Controls.Add(Me.GroupControl4)
            Me.Controls.Add(Me.GroupControl3)
            Me.Controls.Add(Me.GroupControl2)
            Me.Controls.Add(Me.GroupControl1)
            Me.Controls.Add(Me.LabelControl1)
            Me.Name = "ProcessControl"
            Me.Size = New System.Drawing.Size(464, 424)
            CType(Me.GroupControl4, System.ComponentModel.ISupportInitialize).EndInit()
            Me.GroupControl4.ResumeLayout(False)
            Me.GroupControl4.PerformLayout()
            CType(Me.less_yes.Properties, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.less_no.Properties, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.GroupControl3, System.ComponentModel.ISupportInitialize).EndInit()
            Me.GroupControl3.ResumeLayout(False)
            Me.GroupControl3.PerformLayout()
            CType(Me.more_yes.Properties, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.more_no.Properties, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.GroupControl2, System.ComponentModel.ISupportInitialize).EndInit()
            Me.GroupControl2.ResumeLayout(False)
            Me.GroupControl2.PerformLayout()
            CType(Me.normal_yes.Properties, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.normal_no.Properties, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.GroupControl1, System.ComponentModel.ISupportInitialize).EndInit()
            Me.GroupControl1.ResumeLayout(False)
            Me.GroupControl1.PerformLayout()
            CType(Me.ProgressBarControl1.Properties, System.ComponentModel.ISupportInitialize).EndInit()
            Me.ResumeLayout(False)
            Me.PerformLayout()

        End Sub
        Friend WithEvents Button_Cancel As DevExpress.XtraEditors.SimpleButton
        Friend WithEvents Button_Start As DevExpress.XtraEditors.SimpleButton
        Friend WithEvents GroupControl4 As DevExpress.XtraEditors.GroupControl
        Friend WithEvents less_yes As DevExpress.XtraEditors.ComboBoxEdit
        Friend WithEvents LabelControl12 As DevExpress.XtraEditors.LabelControl
        Friend WithEvents less_no As DevExpress.XtraEditors.ComboBoxEdit
        Friend WithEvents LabelControl13 As DevExpress.XtraEditors.LabelControl
        Friend WithEvents GroupControl3 As DevExpress.XtraEditors.GroupControl
        Friend WithEvents more_yes As DevExpress.XtraEditors.ComboBoxEdit
        Friend WithEvents LabelControl10 As DevExpress.XtraEditors.LabelControl
        Friend WithEvents more_no As DevExpress.XtraEditors.ComboBoxEdit
        Friend WithEvents LabelControl11 As DevExpress.XtraEditors.LabelControl
        Friend WithEvents GroupControl2 As DevExpress.XtraEditors.GroupControl
        Friend WithEvents normal_yes As DevExpress.XtraEditors.ComboBoxEdit
        Friend WithEvents LabelControl9 As DevExpress.XtraEditors.LabelControl
        Friend WithEvents normal_no As DevExpress.XtraEditors.ComboBoxEdit
        Friend WithEvents LabelControl8 As DevExpress.XtraEditors.LabelControl
        Friend WithEvents GroupControl1 As DevExpress.XtraEditors.GroupControl
        Friend WithEvents ProgressBarControl1 As DevExpress.XtraEditors.ProgressBarControl
        Friend WithEvents lbl_amount As DevExpress.XtraEditors.LabelControl
        Friend WithEvents lbl_count As DevExpress.XtraEditors.LabelControl
        Friend WithEvents lbl_client As DevExpress.XtraEditors.LabelControl
        Friend WithEvents LabelControl4 As DevExpress.XtraEditors.LabelControl
        Friend WithEvents LabelControl3 As DevExpress.XtraEditors.LabelControl
        Friend WithEvents LabelControl2 As DevExpress.XtraEditors.LabelControl
        Friend WithEvents LabelControl1 As DevExpress.XtraEditors.LabelControl
        Friend WithEvents lbl_total_count As DevExpress.XtraEditors.LabelControl
    End Class
End Namespace