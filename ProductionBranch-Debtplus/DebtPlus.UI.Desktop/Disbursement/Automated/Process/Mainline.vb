#Region "Copyright 2000-2012 DebtPlus, L.L.C."
'{*******************************************************************}
'{                                                                   }
'{       DebtPlus Debt Management System                             }
'{                                                                   }
'{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
'{       ALL RIGHTS RESERVED                                         }
'{                                                                   }
'{   The entire contents of this file is protected by U.S. and       }
'{   International Copyright Laws. Unauthorized reproduction,        }
'{   reverse-engineering, and distribution of all or any portion of  }
'{   the code contained in this file is strictly prohibited and may  }
'{   result in severe civil and criminal penalties and will be       }
'{   prosecuted to the maximum extent possible under the law.        }
'{                                                                   }
'{   RESTRICTIONS                                                    }
'{                                                                   }
'{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
'{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
'{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
'{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
'{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
'{                                                                   }
'{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
'{   ADDITIONAL RESTRICTIONS.                                        }
'{                                                                   }
'{*******************************************************************}

#End Region

Option Compare Binary
Option Explicit On
Option Strict On

Imports System.Data.SqlClient
Imports DebtPlus.UI.Desktop.Disbursement.Common
Imports DebtPlus.Interfaces.Desktop
Imports System.Windows.Forms

Namespace Disbursement.Automated.Process

    Public Class Mainline
        Implements IDesktopMainline

        Public Sub OldAppMain(ByVal args() As String) Implements IDesktopMainline.OldAppMain
            Dim ap As New ArgParser()
            If ap.Parse(args) Then
                Dim thrd As New System.Threading.Thread(New System.Threading.ParameterizedThreadStart(AddressOf ProcessingThread))
                thrd.SetApartmentState(Threading.ApartmentState.STA)
                thrd.Name = "AutodisbursementProcessing"
                thrd.IsBackground = False
                thrd.Start(ap)
            End If
        End Sub

        Private Sub ProcessingThread(ByVal obj As Object)
            Dim ap As ArgParser = DirectCast(obj, ArgParser)

            ' Allocate a "global" context for the processing and clear the MasterQuit flag.
            Using Context As New DisbursementContext()
                Context.MasterQuit = False
                Context.ap = ap

                ' Parse the arguments
                Using cn As SqlConnection = New SqlConnection(DebtPlus.LINQ.SQLInfoClass.getDefault().ConnectionString)
                    Try
                        cn.Open()
                        Using txn As SqlTransaction = cn.BeginTransaction(IsolationLevel.ReadCommitted, "AutoDisbursement")
                            Using cmd As SqlCommand = New SqlCommand()
                                With cmd
                                    .Connection = cn
                                    .Transaction = txn
                                    .CommandText = "SELECT TOP 1 current_status FROM DISBURSEMENT_LOCK WITH (HOLDLOCK, ROWLOCK)"
                                    .CommandType = CommandType.Text

                                    ' Put an operation on this transaction. This will lock the tables with a shared lock.
                                    Using rd As SqlDataReader = .ExecuteReader(CommandBehavior.SingleRow) ' Do not attempt to close this transaction just yet!
                                        rd.Read() ' we want the lock to remain pending during the entire processing state.
                                        ' It is only to be released should we terminate processing.

                                        ' Now, with the transaction still open, run the program. It will process the data
                                        ' and allow full operation. When the form terminates, we will return to complete
                                        ' the transaction and release the shared lock.

                                        ' DO NOT attempt to use "show()". The timing is critical and it can not run until
                                        ' the actual processing is completed
                                        Using mainForm As New Main_Form(Context)
                                            mainForm.ShowDialog()
                                        End Using
                                    End Using
                                End With
                            End Using

                            txn.Commit()
                        End Using

                    Catch ex As Exception
                        DebtPlus.Data.Forms.MessageBox.Show(ex.ToString(), "Thread Exception Occurred", MessageBoxButtons.OK, MessageBoxIcon.Stop)

                    End Try
                End Using
            End Using
        End Sub
    End Class

End Namespace