#Region "Copyright 2000-2012 DebtPlus, L.L.C."
'{*******************************************************************}
'{                                                                   }
'{       DebtPlus Debt Management System                             }
'{                                                                   }
'{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
'{       ALL RIGHTS RESERVED                                         }
'{                                                                   }
'{   The entire contents of this file is protected by U.S. and       }
'{   International Copyright Laws. Unauthorized reproduction,        }
'{   reverse-engineering, and distribution of all or any portion of  }
'{   the code contained in this file is strictly prohibited and may  }
'{   result in severe civil and criminal penalties and will be       }
'{   prosecuted to the maximum extent possible under the law.        }
'{                                                                   }
'{   RESTRICTIONS                                                    }
'{                                                                   }
'{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
'{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
'{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
'{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
'{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
'{                                                                   }
'{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
'{   ADDITIONAL RESTRICTIONS.                                        }
'{                                                                   }
'{*******************************************************************}

#End Region

Imports System.Threading
Imports DebtPlus.UI.Desktop.Disbursement.Common
Imports DebtPlus.Data.Controls
Imports DevExpress.XtraEditors
Imports System.Data.SqlClient
Imports System.Windows.Forms

Namespace Disbursement.Automated.Process
    Friend Class ProcessControl

        Friend ds As New DataSet("ds")
        Public Event Cancelled As EventHandler

        Protected Property context As Disbursement.Common.IDisbursementContext
            Get
                Dim ictx As ISupportContext = TryCast(ParentForm, Disbursement.Common.ISupportContext)
                If ictx IsNot Nothing Then
                    Return ictx.Context
                End If
                Return Nothing
            End Get
            Set(value As Disbursement.Common.IDisbursementContext)
                'Throw New NotImplementedException()
            End Set
        End Property

        Public Sub New()
            MyBase.New()
            InitializeComponent()

            normal_no.Tag = "1"
            normal_yes.Tag = "2"
            more_no.Tag = "3"
            more_yes.Tag = "4"
            less_no.Tag = "5"
            less_yes.Tag = "6"

            normal_no.Properties.Items.Add(New DebtPlus.Data.Controls.ComboboxItem(Const_Manual, Modes.show))
            normal_no.Properties.Items.Add(New DebtPlus.Data.Controls.ComboboxItem(Const_NoPay, Modes.nopay))
            normal_no.Properties.Items.Add(New DebtPlus.Data.Controls.ComboboxItem(Const_Normal, Modes.pay))
            normal_no.Properties.Items.Add(New DebtPlus.Data.Controls.ComboboxItem(Const_Skip, Modes.skip))

            normal_yes.Properties.Items.Add(New DebtPlus.Data.Controls.ComboboxItem(Const_Manual, Modes.show))
            normal_yes.Properties.Items.Add(New DebtPlus.Data.Controls.ComboboxItem(Const_NoPay, Modes.nopay))
            normal_yes.Properties.Items.Add(New DebtPlus.Data.Controls.ComboboxItem(Const_Normal, Modes.pay))
            normal_yes.Properties.Items.Add(New DebtPlus.Data.Controls.ComboboxItem(Const_Skip, Modes.skip))

            more_no.Properties.Items.Add(New DebtPlus.Data.Controls.ComboboxItem(Const_Manual, Modes.show))
            more_no.Properties.Items.Add(New DebtPlus.Data.Controls.ComboboxItem(Const_NoPay, Modes.nopay))
            more_no.Properties.Items.Add(New DebtPlus.Data.Controls.ComboboxItem(Const_Normal, Modes.pay))
            more_no.Properties.Items.Add(New DebtPlus.Data.Controls.ComboboxItem(Const_ProrateExcess, Modes.prorate))
            more_no.Properties.Items.Add(New DebtPlus.Data.Controls.ComboboxItem(Const_Skip, Modes.skip))

            more_yes.Properties.Items.Add(New DebtPlus.Data.Controls.ComboboxItem(Const_Manual, Modes.show))
            more_yes.Properties.Items.Add(New DebtPlus.Data.Controls.ComboboxItem(Const_NoPay, Modes.nopay))
            more_yes.Properties.Items.Add(New DebtPlus.Data.Controls.ComboboxItem(Const_Normal, Modes.pay))
            more_yes.Properties.Items.Add(New DebtPlus.Data.Controls.ComboboxItem(Const_ProrateExcess, Modes.prorate))
            more_yes.Properties.Items.Add(New DebtPlus.Data.Controls.ComboboxItem(Const_Skip, Modes.skip))

            less_no.Properties.Items.Add(New DebtPlus.Data.Controls.ComboboxItem(Const_Manual, Modes.show))
            less_no.Properties.Items.Add(New DebtPlus.Data.Controls.ComboboxItem(Const_NoPay, Modes.nopay))
            less_no.Properties.Items.Add(New DebtPlus.Data.Controls.ComboboxItem(Const_ProrateLess, Modes.prorate))
            less_no.Properties.Items.Add(New DebtPlus.Data.Controls.ComboboxItem(Const_Skip, Modes.skip))

            less_yes.Properties.Items.Add(New DebtPlus.Data.Controls.ComboboxItem(Const_Manual, Modes.show))
            less_yes.Properties.Items.Add(New DebtPlus.Data.Controls.ComboboxItem(Const_NoPay, Modes.nopay))
            less_yes.Properties.Items.Add(New DebtPlus.Data.Controls.ComboboxItem(Const_ProrateLess, Modes.prorate))
            less_yes.Properties.Items.Add(New DebtPlus.Data.Controls.ComboboxItem(Const_Skip, Modes.skip))

            RegisterHandlers()
            ProgressBarControl1.Properties.DisplayFormat.Format = New ProgressBarCustomFormatter
            ProgressBarControl1.Properties.EditFormat.Format = New ProgressBarCustomFormatter
        End Sub

#Region "OnCompleted"

        ''' <summary>
        ''' Generated when the disbursement operation is complete and we should quit.
        ''' </summary>
        Public Event Completed As EventHandler

        ''' <summary>
        ''' Triggers the Completed event.
        ''' </summary>
        Protected Overridable Sub OnCompleted(ByVal ea As EventArgs)
            RaiseEvent Completed(Me, ea)
        End Sub

#End Region

        ''' <summary>
        ''' Format the progress bar to include the percentage in the display
        ''' </summary>
        Private Class ProgressBarCustomFormatter
            Implements IFormatProvider
            Implements ICustomFormatter

            Public Function GetFormat(ByVal formatType As Type) As Object Implements IFormatProvider.GetFormat
                Return Me
            End Function

            Public Function Format(ByVal FormatString As String, ByVal arg As Object, ByVal formatProvider As IFormatProvider) As String Implements ICustomFormatter.Format
                Dim answer As String = String.Empty
                If arg IsNot Nothing AndAlso arg IsNot DBNull.Value Then
                    Try
                        Dim pct As Double = Convert.ToDouble(arg) / 100.0#
                        answer = String.Format("{0:p0}", pct)
                    Catch ex As Exception
                    End Try
                End If
                Return answer
            End Function
        End Class

        ''' <summary>
        ''' Modes used to determine what to do based upon the disbursement information
        ''' </summary>
        Friend Enum Modes
            [quit] = 0 ' used only by the examine form
            [skip] = 1
            [pay] = 2
            [nopay] = 3
            [prorate] = 4
            [show] = 5

            [slop] = 20
            [slop_skip]
            [slop_pay]
            [slop_nopay]
            [slop_prorate]
            [slop_show]
        End Enum

        ' Strings for the combo-box literals
        Private Const_Manual As String = "Manually examine the client"
        Private Const_NoPay As String = "Do not pay anything."
        Private Const_Normal As String = "Pay the creditors the normal amount."
        Private Const_Skip As String = "Skip this client. Go to the next."
        Private Const_ProrateExcess As String = "Prorate the excess amount among the creditors."
        Private Const_ProrateLess As String = "Prorate the available funds among the creditors."

        Private Shared ReadOnly Property IsLocked() As Boolean
            Get
                Return DebtPlus.Configuration.Config.DisbursementLocked
            End Get
        End Property

        Private Sub RegisterHandlers()
            AddHandler Load, AddressOf ProcessControl_Load
            AddHandler Button_Cancel.Click, AddressOf Button_Cancel_Click
            AddHandler Button_Start.Click, AddressOf Button_Start_Click

            AddHandler normal_no.SelectedIndexChanged, AddressOf SelectedIndexChanged
            AddHandler normal_yes.SelectedIndexChanged, AddressOf SelectedIndexChanged
            AddHandler less_no.SelectedIndexChanged, AddressOf SelectedIndexChanged
            AddHandler less_yes.SelectedIndexChanged, AddressOf SelectedIndexChanged
            AddHandler more_no.SelectedIndexChanged, AddressOf SelectedIndexChanged
            AddHandler more_yes.SelectedIndexChanged, AddressOf SelectedIndexChanged
        End Sub

        Private Sub UnRegisterHandlers()
            RemoveHandler Load, AddressOf ProcessControl_Load
            RemoveHandler Button_Cancel.Click, AddressOf Button_Cancel_Click
            RemoveHandler Button_Start.Click, AddressOf Button_Start_Click

            RemoveHandler normal_no.SelectedIndexChanged, AddressOf SelectedIndexChanged
            RemoveHandler normal_yes.SelectedIndexChanged, AddressOf SelectedIndexChanged
            RemoveHandler less_no.SelectedIndexChanged, AddressOf SelectedIndexChanged
            RemoveHandler less_yes.SelectedIndexChanged, AddressOf SelectedIndexChanged
            RemoveHandler more_no.SelectedIndexChanged, AddressOf SelectedIndexChanged
            RemoveHandler more_yes.SelectedIndexChanged, AddressOf SelectedIndexChanged
        End Sub

        ''' <summary>
        ''' Handle the LOAD event for our control
        ''' </summary>
        Private Sub ProcessControl_Load(ByVal sender As Object, ByVal e As EventArgs)

            UnRegisterHandlers()
            Try

                SetSelectedItem(normal_no, DebtPlus.Configuration.Config.DisbursementDefault1)
                SetSelectedItem(normal_yes, DebtPlus.Configuration.Config.DisbursementDefault2)
                SetSelectedItem(more_no, DebtPlus.Configuration.Config.DisbursementDefault3)
                SetSelectedItem(more_yes, DebtPlus.Configuration.Config.DisbursementDefault4)
                SetSelectedItem(less_no, DebtPlus.Configuration.Config.DisbursementDefault5)
                SetSelectedItem(less_yes, DebtPlus.Configuration.Config.DisbursementDefault6)

                ' Clear the status information
                lbl_client.Text = String.Empty
                lbl_amount.Text = String.Format("{0:c}", 0D)    ' localized $0.00 string.
                lbl_count.Text = String.Empty
                ProgressBarControl1.Visible = False

                If IsLocked Then
                    normal_no.Properties.ReadOnly = GetSelectedItem(normal_no) >= 0
                    normal_yes.Properties.ReadOnly = GetSelectedItem(normal_yes) >= 0
                    more_no.Properties.ReadOnly = GetSelectedItem(more_no) >= 0
                    more_yes.Properties.ReadOnly = GetSelectedItem(more_yes) >= 0
                    less_no.Properties.ReadOnly = GetSelectedItem(less_no) >= 0
                    less_yes.Properties.ReadOnly = GetSelectedItem(less_yes) >= 0
                Else
                    normal_no.Properties.ReadOnly = False
                    normal_yes.Properties.ReadOnly = False
                    more_no.Properties.ReadOnly = False
                    more_yes.Properties.ReadOnly = False
                    less_no.Properties.ReadOnly = False
                    less_yes.Properties.ReadOnly = False
                End If

            Finally
                RegisterHandlers()
            End Try

            ' Enable the OK button if the values are properly set
            Button_Start.Enabled = Not HasErrors()
        End Sub

        Private Sub Button_Cancel_Click(ByVal sender As Object, ByVal e As EventArgs)
            RaiseEvent Cancelled(Me, EventArgs.Empty)
        End Sub

        ''' <summary>
        ''' We are now selected. Read the information and "do our thing."
        ''' </summary>
        Public Function ReadForm() As Boolean
            Dim answer As Boolean = False

            Try
                Using cm As New DebtPlus.UI.Common.WaitCursor()

                    ' Read the information for the common processing
                    Using cmd As SqlCommand = New SqlCommand()
                        With cmd
                            .Connection = New SqlConnection(DebtPlus.LINQ.SQLInfoClass.getDefault().ConnectionString)
                            .CommandText = "xpr_disbursement_general_info"
                            .CommandType = CommandType.StoredProcedure
                        End With

                        Using da As New SqlDataAdapter(cmd)
                            Cursor.Current = Cursors.WaitCursor
                            da.Fill(ds, "xpr_disbursement_general_info")
                        End Using
                    End Using

                    ' Retrieve the disbursement information for the client fields
                    Using cmd As SqlCommand = New SqlCommand()
                        With cmd
                            .Connection = New SqlConnection(DebtPlus.LINQ.SQLInfoClass.getDefault().ConnectionString)
                            .CommandText = String.Format("SELECT [oID],[disbursement_register],[client],[held_in_trust],[reserved_in_trust],[disbursement_factor],[note_count],[client_register],[active_status],[start_date],[region],[counselor],[csr],[office],[last_name],[date_created],[created_by] FROM disbursement_clients WHERE disbursement_register=@disbursement_register {0}", DirectCast(context.ap, ArgParser).SelectionClause)
                            .Parameters.Add("@disbursement_register", SqlDbType.Int).Value = DirectCast(context.ap, ArgParser).BatchID
                        End With

                        Using da As New SqlDataAdapter(cmd)
                            Cursor.Current = Cursors.WaitCursor
                            da.Fill(ds, "disbursement_clients")
                        End Using
                    End Using
                End Using

            Catch ex As SqlException
                Using gdr As New Repository.GetDataResult()
                    gdr.HandleException(ex)
                    DebtPlus.UI.Common.ErrorHandling.HandleErrors(gdr, "Error reading disbursement configuration information")
                End Using
            End Try

            ' Complain if there are no records to process
            Dim tbl As DataTable = ds.Tables("disbursement_clients")
            If tbl Is Nothing OrElse tbl.Rows.Count = 0 Then
                DebtPlus.Data.Forms.MessageBox.Show("There are no clients that match your selection criteria", "Sorry, there is nothing to disburse")
            Else

                ' Format the count of items
                Dim TotalClientCount As Int32 = tbl.Rows.Count
                lbl_total_count.Text = String.Format("out of {0:n0}", TotalClientCount)
                lbl_count.Text = "0"

                ' Set the maximum value for the progress bar to the client count
                With ProgressBarControl1
                    With .Properties
                        .Maximum = TotalClientCount
                        .Minimum = 0
                        .PercentView = True
                        .ShowTitle = True
                    End With
                    .EditValue = 0
                    .Visible = True
                End With
                answer = True

                ' Enable/Disable the OK button
                Button_Start.Enabled = Not HasErrors()
            End If

            Return answer
        End Function

        ''' <summary>
        ''' Update the statstics client count
        ''' </summary>
        Private Delegate Sub ProcessClientCountDelegate(ByVal Count As Int32)

        Private Sub ProcessClientCount(ByVal Count As Int32)
            If InvokeRequired() Then
                Dim ia As IAsyncResult = BeginInvoke(New ProcessClientCountDelegate(AddressOf ProcessClientCount), New Object() {Count})
                EndInvoke(ia)
            Else
                With ProgressBarControl1
                    .EditValue = Convert.ToInt32(.EditValue) + Count
                    lbl_count.Text = Convert.ToInt32(.EditValue).ToString()
                End With
            End If
        End Sub

        ''' <summary>
        ''' Update the statistics total dollars disbursed
        ''' </summary>
        Private TotalDollarsDisbursed As Decimal = 0D

        Private Delegate Sub ProcessClientAmountDelegate(ByVal Amount As Decimal)

        Private Sub ProcessClientAmount(Optional ByVal Amount As Decimal = 0D)
            If InvokeRequired() Then
                Dim ia As IAsyncResult = BeginInvoke(New ProcessClientAmountDelegate(AddressOf ProcessClientAmount), New Object() {Amount})
                EndInvoke(ia)
            Else
                TotalDollarsDisbursed += Amount
                lbl_amount.Text = String.Format("{0:c}", TotalDollarsDisbursed)
            End If
        End Sub

        ''' <summary>
        ''' Update the statistics current client ID
        ''' </summary>
        Private Delegate Sub ProcessClientDelegate(ByVal Client As Int32)

        Private Sub ProcessClient(ByVal Client As Int32)
            If InvokeRequired() Then
                Dim ia As IAsyncResult = BeginInvoke(New ProcessClientDelegate(AddressOf ProcessClient), New Object() {Client})
                EndInvoke(ia)
            Else
                lbl_client.Text = String.Format("{0:0000000}", Client)
            End If
        End Sub

        Dim ProcessingThread As Thread = Nothing
        Dim CurrentClientIndex As Int32 = 0

        ''' <summary>
        ''' Process the OK button to start the disbursement
        ''' </summary>
        Private Sub Button_Start_Click(ByVal sender As Object, ByVal e As EventArgs)

            ' Do not allow the start button again.
            Button_Start.Enabled = False

            ' Start a thread to process the disbursements. It will take it from there
            ProcessingThread = New Thread(AddressOf ProcessDisbursement)
            With ProcessingThread
                .Name = "ProcessDisbursement"
                .SetApartmentState(ApartmentState.STA)
                .Start()
            End With
        End Sub

        ''' <summary>
        ''' The disbursement operation is complete
        ''' </summary>
        Private Sub CompletedDisbursement()

            ' Do the thread switch if needed
            If InvokeRequired() Then
                Dim ia As IAsyncResult = BeginInvoke(New MethodInvoker(AddressOf CompletedDisbursement))
                EndInvoke(ia)

            Else

                Try
                    lbl_client.Text = String.Empty
                    ProgressBarControl1.EditValue = ProgressBarControl1.Properties.Maximum
                Catch ex As Exception
                    DebtPlus.Svc.Common.ErrorHandling.HandleErrors(ex)
                End Try

                ' If there is no parent form then just quit the thread. We are complete anyway.
                If ParentForm IsNot Nothing AndAlso Not context.MasterQuit Then
                    DebtPlus.Data.Forms.MessageBox.Show("The disbursement operation is complete.", "Completed")
                End If

                ' We should be able to close our parent form at this point and complete the application.
                OnCompleted(EventArgs.Empty)
            End If
        End Sub

        ' Thread to handle the display of the client information
        Private bt As Thread = Nothing

        ''' <summary>
        ''' Process the disbursement operation
        ''' </summary>
        Private Sub ProcessDisbursement()
            Dim tbl As DataTable = ds.Tables("disbursement_clients")
            Using vue As New DataView(tbl, String.Empty, "client", DataViewRowState.CurrentRows)
                Dim TotalClients As Int32 = vue.Count

                ' Last client examined when "previous" button is pressed
                Dim LastExaminedClient As ClientClass = Nothing

                ' Process the disbursement for this thread                
                Do While (Not context.MasterQuit) AndAlso (CurrentClientIndex < TotalClients)
                    Dim drv As DataRowView = vue(CurrentClientIndex)
                    CurrentClientIndex += 1

                    ' Find the current client ID
                    Dim Client As Int32 = Convert.ToInt32(drv("client"))
                    ProcessClient(Client)

                    ' Read the list of debts
                    Dim DebtList As New DisbursementDebtList()
                    DebtList.ReadDebtsTable(Client, DirectCast(CType(ParentForm, ISupportContext).Context.ap, ArgParser).BatchID)

                    ' Recalculate the fee amount before we determine the variance
                    Dim ProcessingClass As New ClientClass(context, drv.Row, DebtList)
                    ProcessingClass.RecalulateMonthlyFee()

                    ' Compute the amount that is needed for the disbursement
                    Dim DisbursementAmount As Decimal = DebtList.TotalPayments
                    If DisbursementAmount > 0D Then DisbursementAmount += DebtList.TotalFees
                    DisbursementAmount += DebtList.TotalFixedFeePayments()

                    ' Find the relative position for this client in the list of classifications
                    Dim Difference As Decimal = Convert.ToDecimal(drv("held_in_trust")) - DisbursementAmount
                    Dim HasNotes As Boolean = Convert.ToInt32(drv("note_count")) <> 0

                    ' Determine which control is to process this item
                    Dim ProcessingMode As Modes
                    If Difference = 0D Then ' The expected normal deposit amount
                        If HasNotes Then
                            ProcessingMode = CType(GetSelectedItem(normal_yes), Modes)
                        Else
                            ProcessingMode = CType(GetSelectedItem(normal_no), Modes)
                        End If

                    ElseIf Difference > 0D Then ' More funds than needed
                        If HasNotes Then
                            ProcessingMode = CType(GetSelectedItem(more_yes), Modes)
                        Else
                            ProcessingMode = CType(GetSelectedItem(more_no), Modes)
                        End If

                        Dim PaddGreater As Decimal = Convert.ToDecimal(ds.Tables("xpr_disbursement_general_info").Rows(0).Item("payments_pad_greater"))
                        If PaddGreater > 0D AndAlso Difference <= PaddGreater Then
                            ProcessingMode = CType(Convert.ToInt32(ProcessingMode) + Convert.ToInt32(Modes.slop), Modes)
                        End If

                    Else
                        If HasNotes Then
                            ProcessingMode = CType(GetSelectedItem(less_yes), Modes)
                        Else
                            ProcessingMode = CType(GetSelectedItem(less_no), Modes)
                        End If
                        Dim PaddLess As Decimal = Convert.ToDecimal(ds.Tables("xpr_disbursement_general_info").Rows(0).Item("payments_pad_less"))

                        Difference = 0D - Difference
                        If PaddLess > 0D AndAlso Difference < PaddLess Then
                            ProcessingMode = CType(Convert.ToInt32(ProcessingMode) + Convert.ToInt32(Modes.slop), Modes)
                        End If
                    End If

                    ' Do the major processing cycle for the type. The "examine" has its own modified copy.
                    Select Case ProcessingMode
                        Case Modes.quit
                            Exit Do

                        Case Modes.nopay, Modes.slop_nopay
                            With ProcessingClass
                                .Do_NoPay()
                            End With

                        Case Modes.pay
                            With ProcessingClass
                                Dim debit_amt As Decimal = .Do_Pay()
                                ProcessClientAmount(debit_amt)
                            End With

                        Case Modes.skip, Modes.slop_skip
                            ' Do nothing

                        Case Modes.show, Modes.slop_show
                            With ProcessingClass

                                ' Join the previous thread if the thread is still busy.
                                ' This will cause us to hold until the thread terminates.
                                If bt IsNot Nothing AndAlso bt.IsAlive Then
                                    bt.Join()
                                End If

                                ' Set the handler for the previous button and save the value for next time.
                                If context Is Nothing Then
                                    Exit Do
                                End If

                                If Not context.MasterQuit Then
                                    .Previous = LastExaminedClient
                                    LastExaminedClient = ProcessingClass

                                    ' Create a new thread to handle the client update
                                    bt = New Thread(AddressOf .Do_Examine)
                                    With bt
                                        .SetApartmentState(ApartmentState.STA)
                                        .Name = "SHOW_CLIENT_" + Guid.NewGuid().ToString()
                                        .Start()
                                    End With
                                End If
                            End With

                        Case Modes.prorate
                            With ProcessingClass
                                .Do_Prorate()
                                Dim debit_amt As Decimal = .Do_Pay()
                                ProcessClientAmount(debit_amt)
                            End With

                        Case Modes.slop_prorate, Modes.slop_pay
                            With ProcessingClass
                                .Do_Prorate()
                                Dim debit_amt As Decimal = .Do_Pay()
                                ProcessClientAmount(debit_amt)
                            End With

                        Case Else
                    End Select

                    ' Count this client
                    ProcessClientCount(1)
                Loop

                ' At the very end, wait for the last examined client
                If bt IsNot Nothing AndAlso bt.IsAlive Then
                    bt.Join()
                End If

                ' At the end, tell the parent thread that we have completed
                If context IsNot Nothing Then
                    CompletedDisbursement()
                End If
            End Using
        End Sub

        ''' <summary>
        ''' A drop-down index was changed. Enable the START button as needed
        ''' </summary>
        Private Sub SelectedIndexChanged(ByVal sender As Object, ByVal e As EventArgs)
            Button_Start.Enabled = Not HasErrors()

            ' Save the values accordingly
            DebtPlus.Configuration.Config.DisbursementDefault1 = GetSelectedItem(normal_no)
            DebtPlus.Configuration.Config.DisbursementDefault2 = GetSelectedItem(normal_yes)
            DebtPlus.Configuration.Config.DisbursementDefault3 = GetSelectedItem(more_no)
            DebtPlus.Configuration.Config.DisbursementDefault4 = GetSelectedItem(more_yes)
            DebtPlus.Configuration.Config.DisbursementDefault5 = GetSelectedItem(less_no)
            DebtPlus.Configuration.Config.DisbursementDefault6 = GetSelectedItem(less_yes)
        End Sub

        ''' <summary>
        ''' Determine if the form has error conditions
        ''' </summary>
        Private Function HasErrors() As Boolean

            If GetSelectedItem(normal_yes) < 0 Then Return True
            If GetSelectedItem(normal_yes) < 0 Then Return True
            If GetSelectedItem(normal_no) < 0 Then Return True
            If GetSelectedItem(less_yes) < 0 Then Return True
            If GetSelectedItem(less_no) < 0 Then Return True
            If GetSelectedItem(more_yes) < 0 Then Return True
            If GetSelectedItem(more_no) < 0 Then Return True

            Return False
        End Function

        Private Function GetSelectedItem(ByRef ctl As DevExpress.XtraEditors.ComboBoxEdit) As Int32
            Dim item As DebtPlus.Data.Controls.ComboboxItem = TryCast(ctl.SelectedItem, DebtPlus.Data.Controls.ComboboxItem)
            If item IsNot Nothing Then
                Return item.value
            End If
            Return -1
        End Function

        Private Sub SetSelectedItem(ByRef ctl As DevExpress.XtraEditors.ComboBoxEdit, ByVal value As Int32)

            For idx = 0 To ctl.Properties.Items.Count - 1
                Dim item As DebtPlus.Data.Controls.ComboboxItem = TryCast(ctl.Properties.Items(idx), DebtPlus.Data.Controls.ComboboxItem)
                If item IsNot Nothing Then
                    If item.value = value Then
                        ctl.SelectedIndex = idx
                        Return
                    End If
                End If
            Next

            ctl.SelectedIndex = -1
        End Sub
    End Class
End Namespace
