#Region "Copyright 2000-2012 DebtPlus, L.L.C."
'{*******************************************************************}
'{                                                                   }
'{       DebtPlus Debt Management System                             }
'{                                                                   }
'{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
'{       ALL RIGHTS RESERVED                                         }
'{                                                                   }
'{   The entire contents of this file is protected by U.S. and       }
'{   International Copyright Laws. Unauthorized reproduction,        }
'{   reverse-engineering, and distribution of all or any portion of  }
'{   the code contained in this file is strictly prohibited and may  }
'{   result in severe civil and criminal penalties and will be       }
'{   prosecuted to the maximum extent possible under the law.        }
'{                                                                   }
'{   RESTRICTIONS                                                    }
'{                                                                   }
'{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
'{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
'{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
'{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
'{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
'{                                                                   }
'{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
'{   ADDITIONAL RESTRICTIONS.                                        }
'{                                                                   }
'{*******************************************************************}
#End Region

Option Compare Binary
Option Explicit On 
Option Strict On

Imports DebtPlus.UI.FormLib.Disbursement

Namespace Disbursement.Automated.Process
    Friend Class DisbursementBatchControl
        Inherits NewDisbursementRegisterControl

        Protected Property context As Disbursement.Common.IDisbursementContext
            Get
                Return DirectCast(ParentForm, Disbursement.Common.ISupportContext).Context
            End Get
            Set(value As Disbursement.Common.IDisbursementContext)
                Throw New NotImplementedException()
            End Set
        End Property

        Public Sub New()
            MyBase.New()
            InitializeComponent()
        End Sub

#Region " Windows Form Designer generated code "

        'UserControl overrides dispose to clean up the component list.
        Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
            If disposing Then
                If components IsNot Nothing Then
                    components.Dispose()
                End If
            End If
            MyBase.Dispose(disposing)
        End Sub

        'Required by the Windows Form Designer
        Private components As System.ComponentModel.IContainer

        'NOTE: The following procedure is required by the Windows Form Designer
        'It can be modified using the Windows Form Designer.  
        'Do not modify it using the code editor.
        Friend WithEvents LabelControl1 As DevExpress.XtraEditors.LabelControl
        <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
            Me.LabelControl1 = New DevExpress.XtraEditors.LabelControl
            CType(Me.GridControl1, System.ComponentModel.ISupportInitialize).BeginInit()
            Me.SuspendLayout()
            '
            'Button_New
            '
            Me.Button_New.Location = New System.Drawing.Point(432, 64)
            Me.Button_New.Name = "Button_New"
            '
            'GridControl1
            '
            '
            'GridControl1.EmbeddedNavigator
            '
            Me.GridControl1.EmbeddedNavigator.Name = ""
            Me.GridControl1.Location = New System.Drawing.Point(8, 32)
            Me.GridControl1.Name = "GridControl1"
            Me.GridControl1.Size = New System.Drawing.Size(416, 168)
            '
            'Button_Select
            '
            Me.Button_Select.Location = New System.Drawing.Point(432, 32)
            Me.Button_Select.Name = "Button_Select"
            '
            'Button_Cancel
            '
            Me.Button_Cancel.Location = New System.Drawing.Point(432, 96)
            Me.Button_Cancel.Name = "Button_Cancel"
            '
            'LabelControl1
            '
            Me.LabelControl1.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                        Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
            Me.LabelControl1.Appearance.Options.UseTextOptions = True
            Me.LabelControl1.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
            Me.LabelControl1.Appearance.TextOptions.Trimming = DevExpress.Utils.Trimming.Word
            Me.LabelControl1.Appearance.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Top
            Me.LabelControl1.Appearance.TextOptions.WordWrap = DevExpress.Utils.WordWrap.NoWrap
            Me.LabelControl1.Location = New System.Drawing.Point(8, 8)
            Me.LabelControl1.Name = "LabelControl1"
            Me.LabelControl1.Size = New System.Drawing.Size(496, 14)
            Me.LabelControl1.TabIndex = 0
            Me.LabelControl1.Text = "Please select the batch that you wish to update or choose New to create a new bat" & _
            "ch."
            '
            'DisbursementBatchControl
            '
            Me.Controls.Add(Me.LabelControl1)
            Me.Name = "DisbursementBatchControl"
            Me.Size = New System.Drawing.Size(512, 208)
            Me.Controls.SetChildIndex(Me.Button_New, 0)
            Me.Controls.SetChildIndex(Me.LabelControl1, 0)
            Me.Controls.SetChildIndex(Me.GridControl1, 0)
            Me.Controls.SetChildIndex(Me.Button_Select, 0)
            Me.Controls.SetChildIndex(Me.Button_Cancel, 0)
            CType(Me.GridControl1, System.ComponentModel.ISupportInitialize).EndInit()
            Me.ResumeLayout(False)

        End Sub
#End Region

    End Class
End Namespace