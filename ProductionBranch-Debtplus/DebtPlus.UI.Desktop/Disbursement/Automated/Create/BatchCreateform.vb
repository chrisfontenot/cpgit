#Region "Copyright 2000-2012 DebtPlus, L.L.C."
'{*******************************************************************}
'{                                                                   }
'{       DebtPlus Debt Management System                             }
'{                                                                   }
'{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
'{       ALL RIGHTS RESERVED                                         }
'{                                                                   }
'{   The entire contents of this file is protected by U.S. and       }
'{   International Copyright Laws. Unauthorized reproduction,        }
'{   reverse-engineering, and distribution of all or any portion of  }
'{   the code contained in this file is strictly prohibited and may  }
'{   result in severe civil and criminal penalties and will be       }
'{   prosecuted to the maximum extent possible under the law.        }
'{                                                                   }
'{   RESTRICTIONS                                                    }
'{                                                                   }
'{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
'{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
'{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
'{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
'{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
'{                                                                   }
'{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
'{   ADDITIONAL RESTRICTIONS.                                        }
'{                                                                   }
'{*******************************************************************}
#End Region

Option Compare Binary
Option Strict On
Option Explicit On

Imports System.Data.SqlClient
Imports System.Windows.Forms

Namespace Disbursement.Automated.Create

    Friend Class BatchCreateForm
        Inherits DebtPlus.Data.Forms.DebtPlusForm

        Private ap As ArgParser

        Public Sub New()
            MyBase.New()
            InitializeComponent()
            AddHandler Button_cycle.Click, AddressOf Button_cycle_Click
            AddHandler Load, AddressOf BatchCreateForm_Load
            AddHandler Text_Label.KeyPress, AddressOf Text_Label_KeyPress
            AddHandler Text_Label.Enter, AddressOf Text_Label_Enter
            AddHandler Button_Create.Click, AddressOf btn_ok_Click
            AddHandler Button_Cancel.Click, AddressOf btn_cancel_Click
            AddHandler Button_Region.Click, AddressOf Button_cycle_Click
        End Sub

        Public Sub New(ByVal ap As ArgParser)
            MyClass.New()
            Me.ap = ap
        End Sub

#Region " Windows Form Designer generated code "

        'Form overrides dispose to clean up the component list.
        Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
            If disposing Then
                If components IsNot Nothing Then
                    components.Dispose()
                End If
            End If
            MyBase.Dispose(disposing)
        End Sub

        'Required by the Windows Form Designer
        Private components As System.ComponentModel.IContainer

        'NOTE: The following procedure is required by the Windows Form Designer
        'It can be modified using the Windows Form Designer.  
        'Do not modify it using the code editor.
        Friend WithEvents GroupControl1 As DevExpress.XtraEditors.GroupControl
        Friend WithEvents GroupControl2 As DevExpress.XtraEditors.GroupControl
        Friend WithEvents Label1 As DevExpress.XtraEditors.LabelControl
        Friend WithEvents CheckedListBox_Cycle As DevExpress.XtraEditors.CheckedListBoxControl
        Friend WithEvents Button_cycle As DevExpress.XtraEditors.SimpleButton
        Friend WithEvents CheckedListBox_Region As DevExpress.XtraEditors.CheckedListBoxControl
        Friend WithEvents Button_Region As DevExpress.XtraEditors.SimpleButton
        Friend WithEvents Button_Create As DevExpress.XtraEditors.SimpleButton
        Friend WithEvents Button_Cancel As DevExpress.XtraEditors.SimpleButton
        Friend WithEvents Text_Label As DevExpress.XtraEditors.TextEdit
        <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
            Me.GroupControl1 = New DevExpress.XtraEditors.GroupControl
            Me.CheckedListBox_Cycle = New DevExpress.XtraEditors.CheckedListBoxControl
            Me.Button_cycle = New DevExpress.XtraEditors.SimpleButton
            Me.GroupControl2 = New DevExpress.XtraEditors.GroupControl
            Me.CheckedListBox_Region = New DevExpress.XtraEditors.CheckedListBoxControl
            Me.Button_Region = New DevExpress.XtraEditors.SimpleButton
            Me.Button_Create = New DevExpress.XtraEditors.SimpleButton
            Me.Button_Cancel = New DevExpress.XtraEditors.SimpleButton
            Me.Label1 = New DevExpress.XtraEditors.LabelControl
            Me.Text_Label = New DevExpress.XtraEditors.TextEdit
            CType(Me.DxErrorProvider1, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.GroupControl1, System.ComponentModel.ISupportInitialize).BeginInit()
            Me.GroupControl1.SuspendLayout()
            CType(Me.CheckedListBox_Cycle, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.GroupControl2, System.ComponentModel.ISupportInitialize).BeginInit()
            Me.GroupControl2.SuspendLayout()
            CType(Me.CheckedListBox_Region, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.Text_Label.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
            Me.SuspendLayout()
            '
            'GroupControl1
            '
            Me.GroupControl1.Controls.Add(Me.CheckedListBox_Cycle)
            Me.GroupControl1.Controls.Add(Me.Button_cycle)
            Me.GroupControl1.Location = New System.Drawing.Point(8, 40)
            Me.GroupControl1.Name = "GroupControl1"
            Me.GroupControl1.Size = New System.Drawing.Size(160, 200)
            Me.GroupControl1.TabIndex = 2
            Me.GroupControl1.Text = " Disbursement Cycle(s) "
            '
            'CheckedListBox_Cycle
            '
            Me.CheckedListBox_Cycle.CheckOnClick = True
            Me.CheckedListBox_Cycle.ItemHeight = 16
            Me.CheckedListBox_Cycle.Items.AddRange(New DevExpress.XtraEditors.Controls.CheckedListBoxItem() {New DevExpress.XtraEditors.Controls.CheckedListBoxItem("1"), New DevExpress.XtraEditors.Controls.CheckedListBoxItem("2"), New DevExpress.XtraEditors.Controls.CheckedListBoxItem("3"), New DevExpress.XtraEditors.Controls.CheckedListBoxItem("4"), New DevExpress.XtraEditors.Controls.CheckedListBoxItem("5"), New DevExpress.XtraEditors.Controls.CheckedListBoxItem("6"), New DevExpress.XtraEditors.Controls.CheckedListBoxItem("7"), New DevExpress.XtraEditors.Controls.CheckedListBoxItem("8"), New DevExpress.XtraEditors.Controls.CheckedListBoxItem("9"), New DevExpress.XtraEditors.Controls.CheckedListBoxItem("10"), New DevExpress.XtraEditors.Controls.CheckedListBoxItem("11"), New DevExpress.XtraEditors.Controls.CheckedListBoxItem("12"), New DevExpress.XtraEditors.Controls.CheckedListBoxItem("13"), New DevExpress.XtraEditors.Controls.CheckedListBoxItem("14"), New DevExpress.XtraEditors.Controls.CheckedListBoxItem("15"), New DevExpress.XtraEditors.Controls.CheckedListBoxItem("16"), New DevExpress.XtraEditors.Controls.CheckedListBoxItem("17"), New DevExpress.XtraEditors.Controls.CheckedListBoxItem("18"), New DevExpress.XtraEditors.Controls.CheckedListBoxItem("19"), New DevExpress.XtraEditors.Controls.CheckedListBoxItem("20"), New DevExpress.XtraEditors.Controls.CheckedListBoxItem("21"), New DevExpress.XtraEditors.Controls.CheckedListBoxItem("22"), New DevExpress.XtraEditors.Controls.CheckedListBoxItem("23"), New DevExpress.XtraEditors.Controls.CheckedListBoxItem("24"), New DevExpress.XtraEditors.Controls.CheckedListBoxItem("25"), New DevExpress.XtraEditors.Controls.CheckedListBoxItem("26"), New DevExpress.XtraEditors.Controls.CheckedListBoxItem("27"), New DevExpress.XtraEditors.Controls.CheckedListBoxItem("28"), New DevExpress.XtraEditors.Controls.CheckedListBoxItem("29"), New DevExpress.XtraEditors.Controls.CheckedListBoxItem("30"), New DevExpress.XtraEditors.Controls.CheckedListBoxItem("31")})
            Me.CheckedListBox_Cycle.Location = New System.Drawing.Point(8, 16)
            Me.CheckedListBox_Cycle.Name = "CheckedListBox_Cycle"
            Me.CheckedListBox_Cycle.Size = New System.Drawing.Size(144, 144)
            Me.CheckedListBox_Cycle.TabIndex = 0
            Me.CheckedListBox_Cycle.ToolTip = "Choose the client's disbursement cycle for those clients that you wish to disburs" & _
                "e in this batch"
            Me.CheckedListBox_Cycle.ToolTipController = Me.ToolTipController1
            '
            'Button_cycle
            '
            Me.Button_cycle.Location = New System.Drawing.Point(48, 168)
            Me.Button_cycle.Name = "Button_cycle"
            Me.Button_cycle.Size = New System.Drawing.Size(75, 23)
            Me.Button_cycle.TabIndex = 1
            Me.Button_cycle.Tag = "1"
            Me.Button_cycle.Text = "Select &All"
            Me.Button_cycle.ToolTip = "Click here to select all of the items in this list"
            Me.Button_cycle.ToolTipController = Me.ToolTipController1
            Me.Button_cycle.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Hand
            '
            'GroupControl2
            '
            Me.GroupControl2.Controls.Add(Me.CheckedListBox_Region)
            Me.GroupControl2.Controls.Add(Me.Button_Region)
            Me.GroupControl2.Location = New System.Drawing.Point(192, 40)
            Me.GroupControl2.Name = "GroupControl2"
            Me.GroupControl2.Size = New System.Drawing.Size(160, 200)
            Me.GroupControl2.TabIndex = 3
            Me.GroupControl2.Text = " Region(s) "
            '
            'CheckedListBox_Region
            '
            Me.CheckedListBox_Region.CheckOnClick = True
            Me.CheckedListBox_Region.ItemHeight = 16
            Me.CheckedListBox_Region.Location = New System.Drawing.Point(8, 16)
            Me.CheckedListBox_Region.Name = "CheckedListBox_Region"
            Me.CheckedListBox_Region.Size = New System.Drawing.Size(144, 144)
            Me.CheckedListBox_Region.TabIndex = 0
            Me.CheckedListBox_Region.ToolTip = "Choose the region or regions that you wish to disburse in this batch"
            Me.CheckedListBox_Region.ToolTipController = Me.ToolTipController1
            '
            'Button_Region
            '
            Me.Button_Region.Location = New System.Drawing.Point(48, 168)
            Me.Button_Region.Name = "Button_Region"
            Me.Button_Region.Size = New System.Drawing.Size(75, 23)
            Me.Button_Region.TabIndex = 1
            Me.Button_Region.Tag = "1"
            Me.Button_Region.Text = "Select &All"
            Me.Button_Region.ToolTip = "Click here to select all of the items in this list"
            Me.Button_Region.ToolTipController = Me.ToolTipController1
            Me.Button_Region.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Hand
            '
            'Button_Create
            '
            Me.Button_Create.DialogResult = System.Windows.Forms.DialogResult.Cancel
            Me.Button_Create.Location = New System.Drawing.Point(56, 248)
            Me.Button_Create.Name = "Button_Create"
            Me.Button_Create.Size = New System.Drawing.Size(75, 23)
            Me.Button_Create.TabIndex = 4
            Me.Button_Create.Text = "C&reate"
            Me.Button_Create.ToolTip = "Click here to create the batch"
            Me.Button_Create.ToolTipController = Me.ToolTipController1
            Me.Button_Create.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Hand
            '
            'Button_Cancel
            '
            Me.Button_Cancel.Location = New System.Drawing.Point(240, 248)
            Me.Button_Cancel.Name = "Button_Cancel"
            Me.Button_Cancel.Size = New System.Drawing.Size(75, 23)
            Me.Button_Cancel.TabIndex = 5
            Me.Button_Cancel.Text = "&Cancel"
            Me.Button_Cancel.ToolTip = "Cancel this form and return to the previous condition"
            Me.Button_Cancel.ToolTipController = Me.ToolTipController1
            Me.Button_Cancel.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Hand
            '
            'Label1
            '
            Me.Label1.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
            Me.Label1.Appearance.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center
            Me.Label1.Location = New System.Drawing.Point(16, 8)
            Me.Label1.Name = "Label1"
            Me.Label1.Size = New System.Drawing.Size(29, 13)
            Me.Label1.TabIndex = 0
            Me.Label1.Text = "Label:"
            Me.Label1.ToolTipController = Me.ToolTipController1
            '
            'Text_Label
            '
            Me.Text_Label.Location = New System.Drawing.Point(80, 8)
            Me.Text_Label.Name = "Text_Label"
            Me.Text_Label.Properties.NullValuePrompt = "... The optional label for the batch goes here ..."
            Me.Text_Label.Size = New System.Drawing.Size(272, 20)
            Me.Text_Label.TabIndex = 1
            Me.Text_Label.ToolTipController = Me.ToolTipController1
            '
            'BatchCreateForm
            '
            Me.AcceptButton = Me.Button_Create
            Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
            Me.CancelButton = Me.Button_Create
            Me.ClientSize = New System.Drawing.Size(368, 286)
            Me.Controls.Add(Me.Text_Label)
            Me.Controls.Add(Me.Label1)
            Me.Controls.Add(Me.Button_Cancel)
            Me.Controls.Add(Me.Button_Create)
            Me.Controls.Add(Me.GroupControl2)
            Me.Controls.Add(Me.GroupControl1)
            Me.LookAndFeel.UseDefaultLookAndFeel = True
            Me.MaximizeBox = False
            Me.MinimizeBox = False
            Me.Name = "BatchCreateForm"
            Me.Text = "Create Auto Disbursement Batch"
            CType(Me.DxErrorProvider1, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.GroupControl1, System.ComponentModel.ISupportInitialize).EndInit()
            Me.GroupControl1.ResumeLayout(False)
            CType(Me.CheckedListBox_Cycle, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.GroupControl2, System.ComponentModel.ISupportInitialize).EndInit()
            Me.GroupControl2.ResumeLayout(False)
            CType(Me.CheckedListBox_Region, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.Text_Label.Properties, System.ComponentModel.ISupportInitialize).EndInit()
            Me.ResumeLayout(False)
            Me.PerformLayout()

        End Sub

#End Region

        Private Function CycleString() As String
            Dim sb As New System.Text.StringBuilder
            Dim SelectedAll As Boolean = True

            ' Find the list of checked cycle dates. If all are selected then treat it as "none"
            ' and this will process all of the dates
            With CheckedListBox_Cycle
                For Index As System.Int32 = 0 To .ItemCount - 1
                    If .Items(Index).CheckState = CheckState.Checked Then
                        sb.AppendFormat(",{0}", .Items(Index).Value)
                    Else
                        SelectedAll = False ' Since we found one that was not selected then not all are selected
                    End If
                Next

                ' Return the empty string if all are selected.
                If SelectedAll Then
                    Return System.String.Empty
                Else
                    If sb.Length > 0 Then sb.Remove(0, 1)
                    Return sb.ToString()
                End If
            End With
        End Function

        Private Function RegionString() As String
            Dim sb As New System.Text.StringBuilder
            Dim SelectedAll As Boolean = True

            ' Find the list of checked cycle dates. If all are selected then treat it as "none"
            ' and this will process all of the dates
            With CheckedListBox_Region
                For Index As System.Int32 = 0 To .ItemCount - 1
                    If .Items(Index).CheckState = CheckState.Checked Then
                        sb.AppendFormat(",{0:f0}", Convert.ToInt32(CType(.Items(Index).Value, DebtPlus.Data.Controls.ComboboxItem).value))
                    Else
                        SelectedAll = False ' Since we found one that was not selected then not all are selected
                    End If
                Next

                ' Return the empty string if all are selected.
                If SelectedAll Then
                    Return System.String.Empty
                Else
                    If sb.Length > 0 Then sb.Remove(0, 1)
                    Return sb.ToString()
                End If
            End With
        End Function

        Private Sub Button_cycle_Click(ByVal sender As Object, ByVal e As System.EventArgs)

            ' Translate the button to the associated control
            Dim ctl As DevExpress.XtraEditors.CheckedListBoxControl
            If sender Is Button_cycle Then
                ctl = CheckedListBox_Cycle
            Else
                ctl = CheckedListBox_Region
            End If

            ' Process the check status for the button
            With CType(sender, DevExpress.XtraEditors.SimpleButton)
                Dim CheckedState As System.Windows.Forms.CheckState = CheckState.Checked
                If Convert.ToInt32(.Tag) = 0 Then CheckedState = CheckState.Unchecked

                ' Select all of the items in the list
                With ctl
                    For ItemCount As System.Int32 = 0 To .ItemCount - 1
                        .Items(ItemCount).CheckState = CheckedState
                    Next
                End With

                ' Change the legend to match the tag
                If CheckedState = CheckState.Checked Then
                    .Tag = 0
                    .Text = "&Clear All"
                Else
                    .Tag = 1
                    .Text = "&Set All"
                End If
            End With
        End Sub

        Private Sub load_lst_regions()

            With CheckedListBox_Region
                .Items.Clear()

                Dim cn As SqlClient.SqlConnection = New SqlConnection(DebtPlus.LINQ.SQLInfoClass.getDefault().ConnectionString)
                Dim rd As SqlClient.SqlDataReader
                Try
                    cn.Open()
                    With New SqlCommand
                        .Connection = cn
                        .CommandText = "SELECT isnull(region,0) as region,isnull(description,'') as description FROM regions WITH (NOLOCK) ORDER BY description"
                        rd = .ExecuteReader(CommandBehavior.CloseConnection Or CommandBehavior.SingleResult)
                    End With

                    Do While rd.Read
                        .Items.Add(New DebtPlus.Data.Controls.ComboboxItem(Convert.ToString(rd("description")), Convert.ToInt32(rd("region"))), CheckState.Unchecked, True)
                    Loop

                Catch ex As SqlClient.SqlException
                    DebtPlus.Data.Forms.MessageBox.Show(ex.ToString(), "Error Loading the region list", MessageBoxButtons.OK, MessageBoxIcon.Error)

                Finally
                    If cn IsNot Nothing Then cn.Dispose()
                End Try
            End With
        End Sub

        Private Sub BatchCreateForm_Load(ByVal sender As System.Object, ByVal e As System.EventArgs)
            load_lst_regions()
        End Sub

        Private Sub Text_Label_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs)
            If Text_Label.Text = System.String.Empty AndAlso e.KeyChar = " "c Then e.Handled = True
        End Sub

        Private Sub Text_Label_Enter(ByVal sender As Object, ByVal e As System.EventArgs)
            Text_Label.SelectAll()
        End Sub

        Private dlg As DevExpress.Utils.WaitDialogForm = Nothing
        Private Sub btn_ok_Click(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs)
            Button_Create.Enabled = False

            Dim cls As New ThreadClass(AddressOf Completed, CycleString, RegionString, Text_Label.Text.Trim())
            With cls
                Dim thrd As New System.Threading.Thread(AddressOf cls.DoThread)
                thrd.IsBackground = True
                thrd.SetApartmentState(Threading.ApartmentState.STA)
                thrd.Name = "CreateBatch"
                thrd.Start()
            End With

            ' Put put the waiting dialog
            dlg = New DevExpress.Utils.WaitDialogForm("Creating the disbursement batch")
            dlg.Show()
        End Sub

        Friend Delegate Sub CallbackDelegate(ByVal BatchID As System.Int32)
        Friend Sub Completed(ByVal BatchID As System.Int32)
            If InvokeRequired Then
                BeginInvoke(New CallbackDelegate(AddressOf Completed), New Object() {BatchID})
            Else
                If dlg IsNot Nothing Then
                    dlg.Close()
                    dlg.Dispose()
                    dlg = Nothing
                End If

                DebtPlus.Data.Forms.MessageBox.Show(System.String.Format("The disbursement batch {0:f0} was created successfully.", BatchID), "Operation Completed", MessageBoxButtons.OK, MessageBoxIcon.Information)
                Close()
            End If
        End Sub

        Private Sub btn_cancel_Click(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs)
            Close()
        End Sub

        Private Class ThreadClass
            Private CompletedDelegate As CallbackDelegate
            Private CycleString As String
            Private RegionString As String
            Private LabelString As String

            Public Sub New(ByVal CompletedDelegate As CallbackDelegate, ByVal CycleString As String, ByVal RegionString As String, ByVal LabelString As String)
                Me.CompletedDelegate = CompletedDelegate
                Me.CycleString = CycleString
                Me.RegionString = RegionString
                Me.LabelString = LabelString
            End Sub

            Public Sub DoThread()
                Dim cn As SqlClient.SqlConnection = New SqlConnection(DebtPlus.LINQ.SQLInfoClass.getDefault().ConnectionString)
                Dim DisbursementRegister As System.Int32 = -1
                Try
                    cn.Open()
                    With New SqlCommand
                        .Connection = cn
                        .CommandTimeout = System.Math.Max(1200, DebtPlus.LINQ.SQLInfoClass.getDefault().CommandTimeout)
                        .CommandText = "xpr_disbursement_create"
                        .CommandType = CommandType.StoredProcedure
                        .Parameters.Add("@RETURN_VALUE", SqlDbType.Int).Direction = ParameterDirection.ReturnValue
                        .Parameters.Add("@disbursement_date", SqlDbType.VarChar, 512).Value = CycleString
                        .Parameters.Add("@region", SqlDbType.VarChar, 512).Value = RegionString
                        .Parameters.Add("@note", SqlDbType.VarChar, 50).Value = LabelString

                        .ExecuteNonQuery()
                        CompletedDelegate.Invoke(Convert.ToInt32(.Parameters(0).Value))
                    End With

                Catch ex As SqlClient.SqlException
                    DebtPlus.Data.Forms.MessageBox.Show(ex.ToString(), "Error Loading the region list", MessageBoxButtons.OK, MessageBoxIcon.Error)

                Finally
                    If cn IsNot Nothing Then cn.Dispose()
                End Try
            End Sub
        End Class
    End Class

End Namespace
