#Region "Copyright 2000-2012 DebtPlus, L.L.C."
'{*******************************************************************}
'{                                                                   }
'{       DebtPlus Debt Management System                             }
'{                                                                   }
'{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
'{       ALL RIGHTS RESERVED                                         }
'{                                                                   }
'{   The entire contents of this file is protected by U.S. and       }
'{   International Copyright Laws. Unauthorized reproduction,        }
'{   reverse-engineering, and distribution of all or any portion of  }
'{   the code contained in this file is strictly prohibited and may  }
'{   result in severe civil and criminal penalties and will be       }
'{   prosecuted to the maximum extent possible under the law.        }
'{                                                                   }
'{   RESTRICTIONS                                                    }
'{                                                                   }
'{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
'{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
'{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
'{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
'{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
'{                                                                   }
'{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
'{   ADDITIONAL RESTRICTIONS.                                        }
'{                                                                   }
'{*******************************************************************}

#End Region

Option Compare Binary
Option Strict Off
Option Explicit On

Imports DebtPlus.UI.Desktop.Disbursement.Common
Imports DebtPlus.Utils
Imports System.IO
Imports System.Text
Imports System.Data.SqlClient
Imports System.Windows.Forms

Namespace Disbursement.Automated.Post
    Friend Class RPPS_Processing
        Implements IDisposable
        Implements Iform_RPS

        Public Sub New()
            MyBase.New()
        End Sub

        ''' <summary>
        ''' This "form" is a fake. It is present so that the
        ''' payment class may have something to send events.
        ''' </summary>
        Public OutputDirectory As String = String.Empty

        ''' <summary>
        ''' Fake routine to simulate the form display
        ''' </summary>
        Public Sub Show()
        End Sub

#Region " IDisposable Support "

        ''' <summary>
        ''' Support the dispose item for the "form"
        ''' </summary>
        Private disposedValue As Boolean = False        ' To detect redundant calls

        ' IDisposable
        Protected Overridable Sub Dispose(ByVal disposing As Boolean)
            If Not Me.disposedValue Then
                If disposing Then
                    ' TODO: free other state (managed objects).
                End If
            End If
            Me.disposedValue = True
        End Sub

        ' This code added by Visual Basic to correctly implement the disposable pattern.
        Public Sub Dispose() Implements IDisposable.Dispose
            ' Do not change this code.  Put cleanup code in Dispose(ByVal disposing As Boolean) above.
            Dispose(True)
            GC.SuppressFinalize(Me)
        End Sub

#End Region

        ''' <summary>
        ''' Determine the output file name
        ''' (In the "real" form, this housed the CommonDialog control.
        ''' so it had to be moved here to ask for the output file.)
        ''' </summary>
        Public Function Request_FileName(ByVal disbursement_register As Int32, ByVal Bank As Int32, ByVal FileType As String) As StreamWriter Implements Iform_RPS.Request_FileName
            Return Request_FileName(disbursement_register, Bank, FileType, False)
        End Function

        Public Function Request_FileName(ByVal disbursement_register As Int32, ByVal Bank As Int32, ByVal FileType As String, ByVal UseLocalFileOnly As Boolean) As StreamWriter Implements Iform_RPS.Request_FileName
            Dim ios As FileStream = Nothing
            Dim Result As StreamWriter = Nothing
            Dim ErrorMessage As String = String.Empty

            ' Create the base file name that we are going to use
            Dim CreatedName As String = String.Format("{0:00000000}_{1:0000}_{2}.TXT", disbursement_register, Bank, FileType)
            Dim PathName As String

            ' If we can create a remote file then do so now.
            If Not UseLocalFileOnly Then
                PathName = DefaultFolder(Bank)
                If PathName <> String.Empty Then
                    GenerateRPSPayments.FileName = Path.Combine(PathName, CreatedName)
                    Try
                        ios = New FileStream(GenerateRPSPayments.FileName, FileMode.Create, FileAccess.Write, FileShare.Read, 4096)

                    Catch ex As Exception
                        ErrorMessage = String.Format("{0}{1}{1}An error prevented the file from being written to the default directory.{1}The system will try to use your local documents directory instead.", ex.Message, Environment.NewLine)
                    End Try
                End If
            End If

            ' Use the current documents directory for the file.
            If ios Is Nothing Then
                PathName = DocumentsFolder()
                If PathName = String.Empty Then
                    ErrorMessage = "The system did not return a path to the My Documents folder so the file can not be written."
                Else
                    GenerateRPSPayments.FileName = Path.Combine(PathName, CreatedName)
                    Try
                        ios = New FileStream(GenerateRPSPayments.FileName, FileMode.Create, FileAccess.Write, FileShare.Read, 4096)

                    Catch ex As Exception
                        ErrorMessage = String.Format("{0}{1}{1}An error prevented the file from being written to documents directory.{1}The file has been skipped.", ex.Message, Environment.NewLine)
                    End Try
                End If
            End If

            ' If there is a pending error then display it to the user
            If ErrorMessage <> String.Empty Then
                DebtPlus.Data.Forms.MessageBox.Show(ErrorMessage, "Error creating RPPS output file", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
            End If

            ' Return the file pointer or null if none available
            If ios IsNot Nothing Then
                Result = New StreamWriter(ios, Encoding.ASCII, 4096)
            End If

            Return Result
        End Function

        Private Function DocumentsFolder() As String
            Return Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments)
        End Function

        Private Function DefaultFolder(ByVal Bank As Int32) As String
            Dim answer As String = String.Empty

            Dim cn As SqlConnection = New SqlConnection(DebtPlus.LINQ.SQLInfoClass.getDefault().ConnectionString)
            Try
                cn.Open()
                Using cmd As SqlCommand = New SqlCommand
                    With cmd
                        .Connection = cn
                        .CommandText = "SELECT output_directory FROM banks WHERE bank = @bank"
                        .CommandType = CommandType.Text
                        .Parameters.Add("@bank", SqlDbType.Int).Value = Bank
                        Dim obj As Object = .ExecuteScalar
                        If obj IsNot Nothing AndAlso obj IsNot DBNull.Value Then answer = Convert.ToString(obj).Trim()
                    End With
                End Using

            Catch ex As SqlException
                DebtPlus.UI.Common.ErrorHandling.HandleErrors(ex, "Error reading banks table")

            Finally
                If cn IsNot Nothing Then cn.Dispose()
            End Try

            Return answer
        End Function

        Public WriteOnly Property total_debt() As Decimal Implements Iform_RPS.total_debt
            Set(ByVal Value As Decimal)
            End Set
        End Property

        Public WriteOnly Property total_credit() As Decimal Implements Iform_RPS.total_credit
            Set(ByVal Value As Decimal)
            End Set
        End Property

        Public WriteOnly Property total_billed() As Decimal Implements Iform_RPS.total_billed
            Set(ByVal Value As Decimal)
            End Set
        End Property

        Public WriteOnly Property total_gross() As Decimal Implements Iform_RPS.total_gross
            Set(ByVal Value As Decimal)
            End Set
        End Property

        Public WriteOnly Property total_net() As Decimal Implements Iform_RPS.total_net
            Set(ByVal Value As Decimal)
            End Set
        End Property

        Public WriteOnly Property total_items() As Int32 Implements Iform_RPS.total_items
            Set(ByVal Value As Int32)
            End Set
        End Property

        Public WriteOnly Property total_prenotes() As Int32 Implements Iform_RPS.total_prenotes
            Set(ByVal Value As Int32)
            End Set
        End Property

        Public WriteOnly Property total_batches() As Int32 Implements Iform_RPS.total_batches
            Set(ByVal Value As Int32)
            End Set
        End Property

        Public WriteOnly Property Batch() As String Implements Iform_RPS.batch
            Set(ByVal Value As String)
            End Set
        End Property
    End Class
End Namespace
