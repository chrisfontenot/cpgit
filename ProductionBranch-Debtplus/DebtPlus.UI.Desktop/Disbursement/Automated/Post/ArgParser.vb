Option Compare Binary
Option Explicit On
Option Strict On

Imports System.Globalization

Namespace Disbursement.Automated.Post
    Friend Class ArgParser
        Inherits DebtPlus.Utils.ArgParserBase

        Private _batch As Int32 = - 1

        Public Property Batch() As Int32
            Get
                Return _batch
            End Get
            Set(value As Int32)
                _batch = value
            End Set
        End Property

        Private _useLocalFile As Boolean

        Public ReadOnly Property UseLocalFile() As Boolean
            Get
                Return _useLocalFile
            End Get
        End Property

        Public Sub New()
            MyBase.New(New String() {"b", "l"})
        End Sub

        Protected Overrides Function OnSwitch(ByVal switchSymbol As String, ByVal switchValue As String) As SwitchStatus
            Dim ss As SwitchStatus = DebtPlus.Utils.ArgParserBase.SwitchStatus.NoError
            Select Case switchSymbol
                Case "b"
                    Dim TempBatch As Double
                    If (Double.TryParse(switchValue, NumberStyles.Integer, CultureInfo.InvariantCulture, TempBatch)) Then
                        If TempBatch > 0 AndAlso TempBatch <= Convert.ToDouble(Int32.MaxValue) Then
                            _batch = Convert.ToInt32(TempBatch)
                            Exit Select
                        End If
                    End If
                    ss = DebtPlus.Utils.ArgParserBase.SwitchStatus.YesError

                Case "l"
                    _useLocalFile = True

                Case "?"
                    ss = DebtPlus.Utils.ArgParserBase.SwitchStatus.ShowUsage

                Case Else
                    ss = DebtPlus.Utils.ArgParserBase.SwitchStatus.YesError
            End Select
            Return ss
        End Function

        Protected Overrides Sub OnUsage(ByVal errorInfo As String)
            'AppendErrorLine(String.Format("Usage: {0} [-b#]", fname))
            'AppendErrorLine("       -b : Disbursement batch to be posted")
            'AppendErrorLine("       -l : Generate EFT files locally and not according to the banks table")
        End Sub
    End Class
End Namespace
