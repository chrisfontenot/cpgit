﻿#Region "Copyright 2000-2012 DebtPlus, L.L.C."
'{*******************************************************************}
'{                                                                   }
'{       DebtPlus Debt Management System                             }
'{                                                                   }
'{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
'{       ALL RIGHTS RESERVED                                         }
'{                                                                   }
'{   The entire contents of this file is protected by U.S. and       }
'{   International Copyright Laws. Unauthorized reproduction,        }
'{   reverse-engineering, and distribution of all or any portion of  }
'{   the code contained in this file is strictly prohibited and may  }
'{   result in severe civil and criminal penalties and will be       }
'{   prosecuted to the maximum extent possible under the law.        }
'{                                                                   }
'{   RESTRICTIONS                                                    }
'{                                                                   }
'{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
'{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
'{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
'{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
'{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
'{                                                                   }
'{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
'{   ADDITIONAL RESTRICTIONS.                                        }
'{                                                                   }
'{*******************************************************************}
#End Region

Option Strict Off
Option Explicit On

Imports DebtPlus.UI.FormLib.RPPS.Open

Namespace Disbursement.Automated.Post
    Friend Class ProcessControl

        ''' <summary>
        ''' Raised when the CANCEL button is pressed
        ''' </summary>
        ''' <remarks></remarks>
        Public Event Completed As EventHandler

        ''' <summary>
        ''' Current arguments to the procedure
        ''' </summary>
        ''' <value></value>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Private ReadOnly Property ap As ArgParser
            Get
                Return CType(ParentForm, Main_Form).ap
            End Get
        End Property

        ''' <summary>
        ''' Time when the process was started to find the elapsed time
        ''' </summary>
        ''' <remarks></remarks>
        Dim startTime As DateTime

        ''' <summary>
        ''' Initialize the new context for the class
        ''' </summary>
        ''' <remarks></remarks>
        Public Sub New()
            MyBase.New()
            InitializeComponent()
        End Sub

        ''' <summary>
        ''' Process the background thread to do the posting operation
        ''' </summary>
        ''' <param name="sender"></param>
        ''' <param name="e"></param>
        ''' <remarks></remarks>
        Private Sub bt_DoWork(sender As Object, e As System.ComponentModel.DoWorkEventArgs)

            ' Allocate a new class to hold the work
            Using cls As New CDisbursement_Post()
                AddHandler cls.PropertyChanged, AddressOf cls_PropertyChanged
                With cls
                    cls.UseLocalFile = ap.UseLocalFile
                    cls.disbursement_register = ap.Batch
                    cls.ProcessBatch()
                End With
                RemoveHandler cls.PropertyChanged, AddressOf cls_PropertyChanged
            End Using
        End Sub

        ''' <summary>
        ''' Look for changes in the properties of the control. These effect the status fields.
        ''' </summary>
        ''' <param name="sender"></param>
        ''' <param name="e"></param>
        ''' <remarks></remarks>
        Private Sub cls_PropertyChanged(ByVal sender As Object, ByVal e As System.ComponentModel.PropertyChangedEventArgs)

            ' Pointer to the class with the data
            Dim cls As CDisbursement_Post = CType(sender, CDisbursement_Post)

            ' Marshal the thread to the main window thread to avoid cross-thread problems
            If InvokeRequired Then
                EndInvoke(BeginInvoke(New System.ComponentModel.PropertyChangedEventHandler(AddressOf cls_PropertyChanged), New Object() {sender, e}))
            Else
                Select Case e.PropertyName
                    Case "Label"
                        lbl_Status.Text = cls.Label
                        Exit Select

                    Case "Client"
                        lbl_client.Text = String.Format("{0:0000000}", cls.Client)
                        Exit Select

                    Case "Creditor"
                        lbl_creditor.Text = cls.Creditor
                        Exit Select

                    Case "CreditorsProcessed"
                        lbl_creditor_count.Text = String.Format("{0:n0}", cls.CreditorsProcessed)
                        Exit Select

                    Case "gross"
                        lbl_gross.Text = String.Format("{0:c}", cls.gross)
                        lbl_net.Text = String.Format("{0:c}", cls.gross - cls.deducted)
                        Exit Select

                    Case "deducted"
                        lbl_deducted.Text = String.Format("{0:c}", cls.deducted)
                        lbl_net.Text = String.Format("{0:c}", cls.gross - cls.deducted)
                        Exit Select

                    Case "billed"
                        lbl_billed.Text = String.Format("{0:c}", cls.billed)
                        Exit Select

                    Case "RowNumber", "RowCount"
                        Dim Count As Int32 = cls.RowCount
                        Dim Number As Int32 = cls.RowNumber
                        If Count > 0 AndAlso Number > 0 Then
                            bt.ReportProgress(Count * 100 \ Number)
                        End If
                        Exit Select

                    Case Else
                        Exit Select
                End Select
            End If
        End Sub

        Private Sub bt_RunWorkerCompleted(ByVal sender As Object, ByVal e As System.ComponentModel.RunWorkerCompletedEventArgs)
            Timer1.Enabled = False

            lbl_creditor.Text = "Completed"
            lbl_client.Text = "Completed"

            lbl_Status.Visible = False
            lbl_Complete.Visible = True
            Button_Close.Visible = True

            ProgressBarControl1.EditValue = 100
        End Sub

        Private Sub cmd_Close_Click(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs)
            Timer1.Enabled = False
            RaiseEvent Completed(Me, eventArgs.Empty)
        End Sub

        Public Sub Start()

            AddHandler bt.DoWork, AddressOf bt_DoWork
            AddHandler bt.RunWorkerCompleted, AddressOf bt_RunWorkerCompleted
            AddHandler bt.ProgressChanged, AddressOf bt_ProgressChanged
            AddHandler Button_Close.Click, AddressOf cmd_Close_Click
            AddHandler Timer1.Tick, AddressOf Timer1_Tick

            lbl_creditor.Text = "Reading Payment Data"
            bt.WorkerReportsProgress = True
            bt.RunWorkerAsync()

            startTime = DateTime.UtcNow()
            With Timer1
                .Interval = 1000
                .Enabled = True
            End With
        End Sub

        Private Sub Timer1_Tick(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs)

            ' Find the number of seconds since we started
            Dim ts As System.TimeSpan = DateTime.UtcNow().Subtract(startTime)

            ' Format the time value
            lbl_elapsed.Text = System.String.Format("{0:0}:{1:00}:{2:00}", ts.Hours, ts.Minutes, ts.Seconds)
        End Sub

        Private Sub bt_ProgressChanged(ByVal sender As Object, ByVal e As System.ComponentModel.ProgressChangedEventArgs)
            ProgressBarControl1.EditValue = e.ProgressPercentage
        End Sub
    End Class
End Namespace