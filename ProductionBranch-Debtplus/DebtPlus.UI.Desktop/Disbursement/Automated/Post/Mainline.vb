#Region "Copyright 2000-2012 DebtPlus, L.L.C."
'{*******************************************************************}
'{                                                                   }
'{       DebtPlus Debt Management System                             }
'{                                                                   }
'{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
'{       ALL RIGHTS RESERVED                                         }
'{                                                                   }
'{   The entire contents of this file is protected by U.S. and       }
'{   International Copyright Laws. Unauthorized reproduction,        }
'{   reverse-engineering, and distribution of all or any portion of  }
'{   the code contained in this file is strictly prohibited and may  }
'{   result in severe civil and criminal penalties and will be       }
'{   prosecuted to the maximum extent possible under the law.        }
'{                                                                   }
'{   RESTRICTIONS                                                    }
'{                                                                   }
'{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
'{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
'{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
'{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
'{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
'{                                                                   }
'{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
'{   ADDITIONAL RESTRICTIONS.                                        }
'{                                                                   }
'{*******************************************************************}

#End Region

Option Compare Binary
Option Explicit On
Option Strict On

Imports DebtPlus.UI.FormLib.Disbursement
Imports DebtPlus.Interfaces.Desktop
Imports System.Windows.Forms

Namespace Disbursement.Automated.Post
    Public Class Mainline
        Implements IDesktopMainline

        Public Sub OldAppMain(ByVal args() As String) Implements IDesktopMainline.OldAppMain

            ' Parse the arguments
            Dim ap As New ArgParser()
            If ap.Parse(args) Then

                ' Find the number of open disbursement batches
                Dim openClients As Int32
                Try
                    Using bc As New DebtPlus.LINQ.BusinessContext()
                        openClients = bc.xpr_DisbursementLockCount()
                    End Using

                Catch ex As System.Data.SqlClient.SqlException
                    openClients = 0
                End Try

                ' If there are open clients then ask if the user still wants to continue.
                If openClients > 0 Then
                    Dim errorMessage As String
                    If openClients > 1 Then
                        errorMessage = String.Format("There are {0:n0} clients still open and processing this batch.", openClients)
                    Else
                        errorMessage = "There is one client still open and processing this batch."
                    End If

                    If DebtPlus.Data.Forms.MessageBox.Show(String.Format("{0}{1}{1}Do you still wish to continue?", errorMessage, Environment.NewLine), "Open Clients", MessageBoxButtons.YesNo, MessageBoxIcon.Exclamation, MessageBoxDefaultButton.Button2) = DialogResult.No Then
                        Return
                    End If
                End If

                ' Conduct the mode-less form
                Dim frm As New Main_Form(ap)
                frm.Show()
            End If
        End Sub
    End Class
End Namespace
