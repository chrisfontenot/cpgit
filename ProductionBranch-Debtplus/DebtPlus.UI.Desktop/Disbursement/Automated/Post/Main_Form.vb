﻿#Region "Copyright 2000-2012 DebtPlus, L.L.C."
'{*******************************************************************}
'{                                                                   }
'{       DebtPlus Debt Management System                             }
'{                                                                   }
'{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
'{       ALL RIGHTS RESERVED                                         }
'{                                                                   }
'{   The entire contents of this file is protected by U.S. and       }
'{   International Copyright Laws. Unauthorized reproduction,        }
'{   reverse-engineering, and distribution of all or any portion of  }
'{   the code contained in this file is strictly prohibited and may  }
'{   result in severe civil and criminal penalties and will be       }
'{   prosecuted to the maximum extent possible under the law.        }
'{                                                                   }
'{   RESTRICTIONS                                                    }
'{                                                                   }
'{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
'{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
'{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
'{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
'{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
'{                                                                   }
'{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
'{   ADDITIONAL RESTRICTIONS.                                        }
'{                                                                   }
'{*******************************************************************}
#End Region

Option Strict Off
Option Explicit On

Imports DebtPlus.UI.FormLib.RPPS.Open

Namespace Disbursement.Automated.Post
    Friend Class Main_Form

        Friend Property ap As ArgParser = Nothing

        Public Sub New()
            MyBase.New()
            InitializeComponent()
        End Sub

        Public Sub New(ByVal ap As ArgParser)
            MyClass.New()
            Me.ap = ap
            AddHandler Me.Load, AddressOf Main_Form_Load
            AddHandler BatchControl1.Cancelled, AddressOf BatchControl1_Cancelled
            AddHandler BatchControl1.Selected, AddressOf BatchControl1_Selected
            AddHandler ProcessControl1.Completed, AddressOf ProcessControl1_Completed
        End Sub

        Private Sub Main_Form_Load(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs)
            If ap.Batch <= 0 Then
                ProcessControl1.Visible = False
                ProcessControl1.SendToBack()

                BatchControl1.BringToFront()
                BatchControl1.Visible = True
                BatchControl1.ReadForm()
            Else
                StartProcessing()
            End If
        End Sub

        Private Sub StartProcessing()
            BatchControl1.SendToBack()
            BatchControl1.Visible = False

            ProcessControl1.BringToFront()
            ProcessControl1.Visible = True
            ProcessControl1.Start()
        End Sub

        Private Sub BatchControl1_Cancelled(sender As Object, e As System.EventArgs)
            DialogResult = Windows.Forms.DialogResult.Cancel
            If Not Modal Then Close()
        End Sub

        Private Sub BatchControl1_Selected(sender As Object, e As System.EventArgs)
            ' Save the batch ID for processing later
            ap.Batch = BatchControl1.BatchID

            ' Do the start processing function once the batch is known.
            StartProcessing()
        End Sub

        Private Sub ProcessControl1_Completed(sender As Object, e As System.EventArgs)
            DialogResult = Windows.Forms.DialogResult.OK
            If Not Modal Then Close()
        End Sub
    End Class
End Namespace