#Region "Copyright 2000-2012 DebtPlus, L.L.C."
'{*******************************************************************}
'{                                                                   }
'{       DebtPlus Debt Management System                             }
'{                                                                   }
'{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
'{       ALL RIGHTS RESERVED                                         }
'{                                                                   }
'{   The entire contents of this file is protected by U.S. and       }
'{   International Copyright Laws. Unauthorized reproduction,        }
'{   reverse-engineering, and distribution of all or any portion of  }
'{   the code contained in this file is strictly prohibited and may  }
'{   result in severe civil and criminal penalties and will be       }
'{   prosecuted to the maximum extent possible under the law.        }
'{                                                                   }
'{   RESTRICTIONS                                                    }
'{                                                                   }
'{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
'{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
'{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
'{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
'{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
'{                                                                   }
'{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
'{   ADDITIONAL RESTRICTIONS.                                        }
'{                                                                   }
'{*******************************************************************}
#End Region

Option Compare Binary
Option Strict On
Option Explicit On

Imports DebtPlus.UI.Desktop.Disbursement.Common
Imports DebtPlus.Utils
Imports System.Data.SqlClient

Namespace Disbursement.Automated.Post
    Friend Class CDisbursement_Post
        Implements IDisposable
        Implements System.ComponentModel.INotifyPropertyChanged

        ''' <summary>
        ''' Event to notify the caller that a field was changed
        ''' </summary>
        ''' <param name="sender"></param>
        ''' <param name="e"></param>
        ''' <remarks></remarks>
        Public Event PropertyChanged(sender As Object, e As System.ComponentModel.PropertyChangedEventArgs) Implements System.ComponentModel.INotifyPropertyChanged.PropertyChanged

        ' Items that need to be disposed
        Private ReadOnly ds As New DataSet("ds")

        ' Collection of items for payments.
        Private col_item_info As New ArrayList()

        Private tbl As DataTable

        Private _Client As Int32 = 0
        Private _Creditor As String = String.Empty
        Private _CreditorsProcessed As Int32 = 0
        Private _gross As Decimal = 0D
        Private _deducted As Decimal = 0D
        Private _billed As Decimal = 0D

        ''' <summary>
        ''' Batch Number to process
        ''' </summary>
        ''' <value></value>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public Property disbursement_register() As Int32 = -1

        ''' <summary>
        ''' Override to use the local documents directory rather than the config entry for RPPS files
        ''' </summary>
        ''' <value></value>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public Property UseLocalFile() As Boolean = False

        ''' <summary>
        ''' Change the display label on the status form
        ''' </summary>
        Private privateLabel As String = String.Empty
        Public Property Label As String
            Get
                Return privateLabel
            End Get
            Set(value As String)
                If value <> privateLabel Then
                    privateLabel = value
                    RaiseEvent PropertyChanged(Me, New System.ComponentModel.PropertyChangedEventArgs("Label"))
                End If
            End Set
        End Property

        Public Property Client() As Int32
            Get
                Return _Client
            End Get
            Set(ByVal Value As Int32)
                If _Client <> Value Then
                    _Client = Value
                    RaiseEvent PropertyChanged(Me, New System.ComponentModel.PropertyChangedEventArgs("Client"))
                End If
            End Set
        End Property

        ''' <summary>
        ''' Current creditor ID
        ''' </summary>
        Public Property Creditor() As String
            Get
                Return _Creditor
            End Get
            Set(ByVal Value As String)
                If _Creditor <> Value Then
                    _Creditor = Value
                    RaiseEvent PropertyChanged(Me, New System.ComponentModel.PropertyChangedEventArgs("Creditor"))
                End If
            End Set
        End Property

        ''' <summary>
        ''' Number of processed creditors
        ''' </summary>
        Public Property CreditorsProcessed() As Int32
            Get
                Return _CreditorsProcessed
            End Get
            Set(ByVal Value As Int32)
                If _CreditorsProcessed <> Value Then
                    _CreditorsProcessed = Value
                    RaiseEvent PropertyChanged(Me, New System.ComponentModel.PropertyChangedEventArgs("CreditorsProcessed"))
                End If
            End Set
        End Property

        ''' <summary>
        ''' Dollar amount deducted from gross
        ''' </summary>
        Public Property gross() As Decimal
            Get
                Return _gross
            End Get
            Set(ByVal Value As Decimal)
                If _gross <> Value Then
                    _gross = Value
                    RaiseEvent PropertyChanged(Me, New System.ComponentModel.PropertyChangedEventArgs("gross"))
                End If
            End Set
        End Property

        ''' <summary>
        ''' Dollar amount deducted from gross
        ''' </summary>
        Public Property deducted() As Decimal
            Get
                Return _deducted
            End Get
            Set(ByVal Value As Decimal)
                If _deducted <> Value Then
                    _deducted = Value
                    RaiseEvent PropertyChanged(Me, New System.ComponentModel.PropertyChangedEventArgs("deducted"))
                End If
            End Set
        End Property

        ''' <summary>
        ''' Dollar amount billed to creditors
        ''' </summary>
        Public Property billed() As Decimal
            Get
                Return _billed
            End Get
            Set(ByVal Value As Decimal)
                If _billed <> Value Then
                    _billed = Value
                    RaiseEvent PropertyChanged(Me, New System.ComponentModel.PropertyChangedEventArgs("billed"))
                End If
            End Set
        End Property

        Private _RowNumber As Int32 = 0
        Public Property RowNumber As Int32
            Get
                Return _RowNumber
            End Get
            Set(value As Int32)
                If _RowNumber <> value Then
                    _RowNumber = value
                    RaiseEvent PropertyChanged(Me, New System.ComponentModel.PropertyChangedEventArgs("RowNumber"))
                End If
            End Set
        End Property

        Private _RowCount As Int32 = 0
        Public Property RowCount As Int32
            Get
                Return _RowCount
            End Get
            Set(value As Int32)
                If _RowCount <> value Then
                    _RowCount = value
                    RaiseEvent PropertyChanged(Me, New System.ComponentModel.PropertyChangedEventArgs("RowCount"))
                End If
            End Set
        End Property

        ''' <summary>
        ''' Find the payment item the is corresponding to the bank
        ''' </summary>
        Public Function get_item_info(ByVal bank As Int32) As item_info
            Dim result As item_info = Nothing
            Dim item As item_info = Nothing

            ' Try to find the bank account in the collection of items
            For Each item In col_item_info
                If item.bank = bank Then
                    result = item
                    Exit For
                End If
            Next item

            ' If there is no match then create a new item.
            If result Is Nothing Then
                result = New item_info(Me)
                With result
                    .bank = bank
                    .read_bank(bank)
                End With
                col_item_info.Add(result)
            End If

            ' Return the resulting structure pointer
            Return result
        End Function

        ''' <summary>
        ''' Process the posting operation
        ''' </summary>
        Public Sub ProcessBatch()

            ' Find the list of payments to process
            tbl = InputTable()
            If tbl IsNot Nothing Then
                PayBatches()
                CalculateDeductCheck()
                GenerateEFTFiles()
            End If
        End Sub

        ''' <summary>
        ''' Find a list of the transactions to be processed
        ''' </summary>
        Private Function InputTable() As DataTable
            Dim tbl As DataTable = Nothing
            Dim cn As SqlClient.SqlConnection = New SqlConnection(DebtPlus.LINQ.SQLInfoClass.getDefault().ConnectionString)
            Try
                cn.Open()
                Using cmd As SqlClient.SqlCommand = New SqlCommand
                    With cmd
                        .Connection = cn
                        .CommandTimeout = 0
                        .CommandText = "xpr_disbursement_list_post"
                        .CommandType = CommandType.StoredProcedure
                        .Parameters.Add("@disbursement_register", SqlDbType.Int).Value = disbursement_register
                    End With

                    Using da As New SqlClient.SqlDataAdapter(cmd)
                        da.Fill(ds, "xpr_disbursement_list_post")
                        tbl = ds.Tables("xpr_disbursement_list_post")
                    End Using
                End Using

            Catch ex As SqlClient.SqlException
                DebtPlus.UI.Common.ErrorHandling.HandleErrors(ex, "Error reading disbursement information")

            Finally
                If cn IsNot Nothing Then cn.Dispose()
            End Try

            Return tbl
        End Function

        ''' <summary>
        ''' Pay all of the creditors in the batch
        ''' </summary>
        Private Sub PayBatches()

            ' Obtain the transactions for the batch
            Label = "Processing creditor payments"
            RowCount = tbl.Rows.Count
            RowNumber = 0
            CreditorsProcessed = 0

            ' Process the items until there are no more to process.
            ' The ProcessCredior returns at the end of each creditor.
            Do While RowNumber < RowCount
                CreditorsProcessed += 1
                ProcessCreditor()
            Loop
        End Sub

        ''' <summary>
        ''' Calculate the Z9999 deduction check
        ''' </summary>
        Private Sub CalculateDeductCheck()
            Dim cn As SqlClient.SqlConnection = New SqlConnection(DebtPlus.LINQ.SQLInfoClass.getDefault().ConnectionString)

            ' Generate the deduction check(s) and close the batch
            Label = "Creating deduct check information"
            Try
                cn.Open()

                Using cmd As SqlClient.SqlCommand = New SqlCommand
                    With cmd
                        .Connection = cn
                        .CommandTimeout = System.Math.Max(600, DebtPlus.LINQ.SQLInfoClass.getDefault().CommandTimeout)
                        .CommandText = "xpr_disbursement_deduct"
                        .CommandType = CommandType.StoredProcedure
                        .Parameters.Add("@disbursement_register", SqlDbType.Int).Value = disbursement_register
                        .ExecuteNonQuery()
                    End With
                End Using

            Finally
                If cn IsNot Nothing Then cn.Dispose()
            End Try
        End Sub

        ''' <summary>
        ''' Generate the EFT files for the disbursement
        ''' </summary>
        Private Sub GenerateEFTFiles()

            ' Process the RPPS files once the creditors have been generated
            For Each item As item_info In col_item_info
                Select Case item.bank_type
                    Case "R"
                        Label = String.Format("Generating Payment File for bank # {0:f0}.", item.bank)
                        item.Write_RPPS(UseLocalFile)
                End Select
            Next item
        End Sub

        ''' <summary>
        ''' A creditor was found. Process all records for this creditor
        ''' </summary>
        Private Sub ProcessCreditor()

            ' Information about our current creditor trust register
            Dim pmt_info As item_info = Nothing

            ' Information for the current debt
            Dim debt_fairshare_pct_check As Double = 0.0
            Dim debt_fairshare_pct_eft As Double = 0.0

            Dim creditor_type As String = String.Empty
            Dim fairshare_pct As Double = 0.0
            Dim fairshare_amt As Decimal = 0D
            Dim debt_gross As Decimal = 0D

            ' Information from the creditor table
            Dim creditor_voucher_spacing As Int32 = 0
            Dim creditor_max_amt_per_check As Decimal = 0D
            Dim creditor_max_clients_per_check As Int32 = -1
            Dim creditor_max_fairshare_per_debt As Decimal = 0
            Dim bank As Int32 = 0
            Dim rpps_biller_id As String = String.Empty

            ' Information for the current transaction
            Dim fairshare_deducted As Decimal = 0D
            Dim fairshare_billed As Decimal = 0D
            Dim client_creditor_register As Int32 = 0

            ' Number of items permitted for this creditor
            Dim creditor_item_limit As Int32 = 0
            Dim creditor_dollar_limit As Decimal = 0D

            ' Find the information for this creditor
            Dim cn As SqlClient.SqlConnection = New SqlConnection(DebtPlus.LINQ.SQLInfoClass.getDefault().ConnectionString)
            Dim txn As SqlClient.SqlTransaction = Nothing
            Dim rd As SqlClient.SqlDataReader = Nothing

            Dim row As DataRow = tbl.Rows(RowNumber)
            Creditor = Convert.ToString(row("creditor"))

            ' Find the bank for this specific creditor.
            ' The bank does not change for each transaction since the debt is not tied to a bank. The creditor has a bank, not the debt.
            bank = Convert.ToInt32(row("bank"))
            pmt_info = get_item_info(bank)

            ' Ensure that we start with a new creditor register even if this is a bank wire transaction.
            pmt_info.creditor_register = 0

            Try
                cn.Open()
                txn = cn.BeginTransaction()

                Using cmd As SqlClient.SqlCommand = New SqlCommand
                    With cmd
                        .Transaction = txn
                        .Connection = cn
                        .CommandText = "SELECT [creditor],[voucher_spacing],[max_amt_per_check],[max_fairshare_per_debt],[max_clients_per_check] FROM view_disbursement_post_creditor_info WHERE creditor=@creditor"
                        .Parameters.Add("@creditor", SqlDbType.VarChar, 80).Value = Creditor
                        rd = .ExecuteReader(CommandBehavior.SingleRow)

                        If rd IsNot Nothing Then
                            If rd.Read Then
                                For FieldNo As Int32 = 0 To rd.FieldCount - 1
                                    If Not rd.IsDBNull(FieldNo) Then
                                        Select Case rd.GetName(FieldNo).ToLower()
                                            Case "voucher_spacing"
                                                creditor_voucher_spacing = Convert.ToInt32(rd.GetValue(FieldNo))
                                            Case "max_amt_per_check"
                                                creditor_max_amt_per_check = rd.GetDecimal(FieldNo)
                                            Case "max_clients_per_check"
                                                creditor_max_clients_per_check = Convert.ToInt32(rd.GetValue(FieldNo))
                                            Case "max_fairshare_per_debt"
                                                creditor_max_fairshare_per_debt = rd.GetDecimal(FieldNo)
                                            Case Else
                                        End Select
                                    End If
                                Next
                                rd.Close()
                            End If
                            rd = Nothing
                        End If
                    End With
                End Using

                ' Stop processing when the creditor changes
                Do While RowNumber < RowCount

                    ' Stop processing when the creditor ID changes.
                    row = tbl.Rows(RowNumber)
                    If Creditor <> Convert.ToString(row("creditor")) Then Exit Do

                    ' Find the item limit for the creditor checks
                    creditor_item_limit = creditor_max_clients_per_check
                    If creditor_item_limit <= 0 Then creditor_item_limit = pmt_info.max_items
                    If creditor_voucher_spacing = 2 Then creditor_item_limit = 0
                    If creditor_item_limit > 0 Then
                        If creditor_voucher_spacing = 1 Then creditor_item_limit = (creditor_item_limit + 1) \ 2
                    End If

                    ' Apply a maximum limit of 40 items.
                    If creditor_item_limit > 40 Then creditor_item_limit = 40

                    ' Find the item limit for the creditor checks
                    creditor_dollar_limit = creditor_max_amt_per_check
                    If creditor_dollar_limit <= 0D Then creditor_dollar_limit = pmt_info.max_amount

                    ' Save the client for later.
                    Client = Convert.ToInt32(row("client"))

                    ' Save the ids
                    rpps_biller_id = DebtPlus.Utils.Nulls.DStr(row("rpps_biller_id"))

                    creditor_type = DebtPlus.Utils.Nulls.DStr(row("creditor_type"), "N")

                    ' Save the gross amount to be paid for later
                    debt_gross = DebtPlus.Utils.Nulls.DDec(row("debit_amt"))

                    ' Calculate the parameters for the client/creditor information
                    debt_fairshare_pct_eft = DebtPlus.Utils.Nulls.DDbl(row("fairshare_pct_eft"))
                    debt_fairshare_pct_check = DebtPlus.Utils.Nulls.DDbl(row("fairshare_pct_check"))

                    ' Find the creditor type and the corresponding fairshare percentage
                    If pmt_info.bank_type = "C" Then
                        fairshare_pct = debt_fairshare_pct_check
                    Else
                        fairshare_pct = debt_fairshare_pct_eft
                    End If

                    ' Look at the type of the creditor for the proper fairshare information
                    If creditor_type = "N" Then
                        fairshare_pct = 0.0#
                        fairshare_amt = 0D
                    Else
                        fairshare_amt = Math.Truncate(Convert.ToDecimal(Convert.ToDouble(debt_gross) * fairshare_pct), 2)

                        ' If the fairshare is too much then limit it to the maximum
                        If creditor_max_fairshare_per_debt > 0D Then
                            If creditor_max_fairshare_per_debt < fairshare_amt Then fairshare_amt = creditor_max_fairshare_per_debt
                        End If
                    End If

                    If (pmt_info.bank_type = "C") AndAlso (pmt_info.trust_register > 0) Then

                        ' Limit the check to the maximum number of clients
                        If creditor_item_limit > 0 AndAlso pmt_info.item_count >= creditor_item_limit Then
                            pmt_info.Save_Trust_Register(cn, txn)

                            ' Limit the check to the maximum dollar amount
                        ElseIf (creditor_dollar_limit > 0D) AndAlso (((pmt_info.gross - pmt_info.deducted) + (debt_gross - fairshare_deducted)) >= creditor_dollar_limit) Then
                            pmt_info.Save_Trust_Register(cn, txn)
                        End If
                    End If

                    fairshare_deducted = 0D
                    fairshare_billed = 0D

                    ' Determine if the amount is billed or deducted
                    If creditor_type = "D" Then
                        fairshare_deducted = fairshare_amt
                    ElseIf creditor_type <> "N" Then
                        fairshare_billed = fairshare_amt
                    End If

                    ' Create a new trust register for the check if there is not one presently open
                    If pmt_info.trust_register <= 0 Then
                        pmt_info.New_Check()
                        pmt_info.New_Trust_Register(cn, txn)
                    End If

                    ' We need a creditor register entry to complete the processing. Get one if we
                    ' don't currently have one.
                    If pmt_info.creditor_register <= 0 Then
                        pmt_info.New_Creditor_Register(cn, txn)
                    End If

                    ' Update the statistics for the current item
                    With pmt_info
                        .gross += debt_gross
                        .deducted += fairshare_deducted
                        .billed += fairshare_billed
                        .item_count += 1
                    End With

                    gross += debt_gross
                    deducted += fairshare_deducted
                    billed += fairshare_billed

                    ' Insert the transaction into the registers_client_creditor table
                    ' and deduct the amount paid from the balance of the debt.
                    Using cmd As SqlClient.SqlCommand = New SqlCommand
                        With cmd
                            .Connection = cn
                            .Transaction = txn
                            .CommandText = "xpr_disbursement_cc_trans"
                            .CommandType = CommandType.StoredProcedure

                            .Parameters.Add("RETURN", SqlDbType.Int).Direction = ParameterDirection.ReturnValue

                            .Parameters.Add("@tran_type", SqlDbType.VarChar, 2).Value = pmt_info.tran_type
                            .Parameters.Add("@client_creditor", SqlDbType.Int).Value = Convert.ToInt32(row("client_creditor"))
                            .Parameters.Add("@creditor_type", SqlDbType.VarChar, 2).Value = creditor_type
                            .Parameters.Add("@debit_amt", SqlDbType.Decimal).Value = debt_gross
                            .Parameters.Add("@fairshare_pct", SqlDbType.Float).Value = fairshare_pct
                            .Parameters.Add("@fairshare_amt", SqlDbType.Decimal).Value = fairshare_amt
                            .Parameters.Add("@disbursement_register", SqlDbType.Int).Value = disbursement_register
                            .Parameters.Add("@trust_register", SqlDbType.Int).Value = pmt_info.trust_register
                            .Parameters.Add("@account_number", SqlDbType.VarChar, 256).Value = Convert.ToString(row("account_number"))
                            .Parameters.Add("@creditor_register", SqlDbType.Int).Value = pmt_info.creditor_register

                            .ExecuteNonQuery()
                            client_creditor_register = Convert.ToInt32(.Parameters("RETURN").Value)
                        End With
                    End Using

                    ' If this is an RPPS transaction then find the file number for the transactions.
                    If pmt_info.bank_type = "R" Then

                        ' Attach the transaction to the file.
                        Using cmd As SqlClient.SqlCommand = New SqlCommand
                            With cmd
                                .Connection = cn
                                .Transaction = txn
                                .CommandText = "xpr_rpps_cie_payment"
                                .CommandType = CommandType.StoredProcedure

                                .Parameters.Add("@client_creditor", SqlDbType.Int).Value = Convert.ToInt32(row("client_creditor"))
                                .Parameters.Add("@client_creditor_register", SqlDbType.Int).Value = client_creditor_register
                                .Parameters.Add("@BillerID", SqlDbType.VarChar, 80).Value = rpps_biller_id
                                .Parameters.Add("@bank", SqlDbType.Int).Value = pmt_info.bank
                                .ExecuteNonQuery()
                            End With
                        End Using
                    End If
                    RowNumber += 1
                Loop

                ' Close out the creditor for this is no longer the proper creditor.
                pmt_info.Save_Trust_Register(cn, txn)

                ' Commit the changes to the database
                txn.Commit()
                txn.Dispose()
                txn = Nothing

            Catch ex As Exception
                DebtPlus.UI.Common.ErrorHandling.HandleErrors(ex)

            Finally
                If txn IsNot Nothing Then
                    Try
                        txn.Rollback()
                    Catch exRollback As Exception
                        DebtPlus.Svc.Common.ErrorHandling.HandleErrors(exRollback)
                    End Try
                    txn.Dispose()
                    txn = Nothing
                End If

                If cn IsNot Nothing Then cn.Dispose()
            End Try
        End Sub

#Region "IDisposable Support"
        Private disposedValue As Boolean ' To detect redundant calls

        ' IDisposable
        Protected Overridable Sub Dispose(disposing As Boolean)
            If Not Me.disposedValue Then
                If disposing Then
                    ds.Dispose()
                    If col_item_info IsNot Nothing Then col_item_info.Clear()
                End If

                ' TODO: free unmanaged resources (unmanaged objects) and override Finalize() below.

                ' set large fields to null.
                col_item_info = Nothing
                tbl = Nothing
            End If

            Me.disposedValue = True
        End Sub

        ' TODO: override Finalize() only if Dispose(ByVal disposing As Boolean) above has code to free unmanaged resources.
        'Protected Overrides Sub Finalize()
        '    ' Do not change this code.  Put cleanup code in Dispose(ByVal disposing As Boolean) above.
        '    Dispose(False)
        '    MyBase.Finalize()
        'End Sub

        ' This code added by Visual Basic to correctly implement the disposable pattern.
        Public Sub Dispose() Implements IDisposable.Dispose
            ' Do not change this code.  Put cleanup code in Dispose(ByVal disposing As Boolean) above.
            Dispose(True)
            GC.SuppressFinalize(Me)
        End Sub
#End Region
    End Class
End Namespace
