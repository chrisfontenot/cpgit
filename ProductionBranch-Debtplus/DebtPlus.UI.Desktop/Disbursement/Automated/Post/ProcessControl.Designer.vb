﻿Namespace Disbursement.Automated.Post
    <Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
    Partial Class ProcessControl
        Inherits DebtPlus.Data.Controls.UserControl

        'Form overrides dispose to clean up the component list.
        Protected Overloads Overrides Sub Dispose(ByVal Disposing As Boolean)
            If Disposing Then
                If components IsNot Nothing Then
                    components.Dispose()
                End If
            End If
            MyBase.Dispose(Disposing)
        End Sub

        'Required by the Windows Form Designer
        Private components As System.ComponentModel.IContainer

        'NOTE: The following procedure is required by the Windows Form Designer
        'It can be modified using the Windows Form Designer.
        'Do not modify it using the code editor.
        Private Sub InitializeComponent()
            Me.components = New System.ComponentModel.Container()
            Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(ProcessControl))
            Me.Timer1 = New System.Windows.Forms.Timer(Me.components)
            Me.lbl_Status = New DevExpress.XtraEditors.LabelControl()
            Me.lbl_Complete = New DevExpress.XtraEditors.LabelControl()
            Me.lbl_creditor = New DevExpress.XtraEditors.LabelControl()
            Me._Label1_7 = New DevExpress.XtraEditors.LabelControl()
            Me.lbl_creditor_count = New DevExpress.XtraEditors.LabelControl()
            Me._Label1_6 = New DevExpress.XtraEditors.LabelControl()
            Me.lbl_billed = New DevExpress.XtraEditors.LabelControl()
            Me._Label1_5 = New DevExpress.XtraEditors.LabelControl()
            Me.lbl_net = New DevExpress.XtraEditors.LabelControl()
            Me._Label1_4 = New DevExpress.XtraEditors.LabelControl()
            Me.lbl_deducted = New DevExpress.XtraEditors.LabelControl()
            Me._Label1_3 = New DevExpress.XtraEditors.LabelControl()
            Me.lbl_gross = New DevExpress.XtraEditors.LabelControl()
            Me._Label1_2 = New DevExpress.XtraEditors.LabelControl()
            Me.lbl_client = New DevExpress.XtraEditors.LabelControl()
            Me._Label1_1 = New DevExpress.XtraEditors.LabelControl()
            Me.lbl_elapsed = New DevExpress.XtraEditors.LabelControl()
            Me._Label1_0 = New DevExpress.XtraEditors.LabelControl()
            Me.Button_Close = New DevExpress.XtraEditors.SimpleButton()
            Me.ProgressBarControl1 = New DevExpress.XtraEditors.ProgressBarControl()
            CType(Me.ProgressBarControl1.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
            Me.SuspendLayout()
            '
            'Timer1
            '
            Me.Timer1.Interval = 1000
            '
            'lbl_Status
            '
            Me.lbl_Status.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
            Me.lbl_Status.Appearance.BackColor = System.Drawing.Color.Transparent
            Me.lbl_Status.Appearance.Font = New System.Drawing.Font("Arial", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
            Me.lbl_Status.Appearance.ForeColor = System.Drawing.SystemColors.ControlText
            Me.lbl_Status.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
            Me.lbl_Status.Appearance.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Top
            Me.lbl_Status.Appearance.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap
            Me.lbl_Status.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None
            Me.lbl_Status.Cursor = System.Windows.Forms.Cursors.Default
            Me.lbl_Status.Location = New System.Drawing.Point(8, 8)
            Me.lbl_Status.Name = "lbl_Status"
            Me.lbl_Status.Size = New System.Drawing.Size(299, 66)
            Me.lbl_Status.TabIndex = 0
            Me.lbl_Status.Text = resources.GetString("lbl_Status.Text")
            Me.lbl_Status.UseMnemonic = False
            '
            'lbl_Complete
            '
            Me.lbl_Complete.Appearance.BackColor = System.Drawing.SystemColors.Control
            Me.lbl_Complete.Appearance.Font = New System.Drawing.Font("Arial", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
            Me.lbl_Complete.Appearance.ForeColor = System.Drawing.SystemColors.ControlText
            Me.lbl_Complete.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
            Me.lbl_Complete.Appearance.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Top
            Me.lbl_Complete.Appearance.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap
            Me.lbl_Complete.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None
            Me.lbl_Complete.Cursor = System.Windows.Forms.Cursors.Default
            Me.lbl_Complete.Location = New System.Drawing.Point(8, 8)
            Me.lbl_Complete.Name = "lbl_Complete"
            Me.lbl_Complete.Size = New System.Drawing.Size(218, 66)
            Me.lbl_Complete.TabIndex = 1
            Me.lbl_Complete.Text = "The processing is complete. Please close this form by pressing the button to the " & _
        "right."
            Me.lbl_Complete.Visible = False
            '
            'lbl_creditor
            '
            Me.lbl_creditor.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
            Me.lbl_creditor.Appearance.BackColor = System.Drawing.Color.Transparent
            Me.lbl_creditor.Appearance.Font = New System.Drawing.Font("Arial", 10.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
            Me.lbl_creditor.Appearance.ForeColor = System.Drawing.SystemColors.ControlText
            Me.lbl_creditor.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
            Me.lbl_creditor.Appearance.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Top
            Me.lbl_creditor.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None
            Me.lbl_creditor.Cursor = System.Windows.Forms.Cursors.Default
            Me.lbl_creditor.Location = New System.Drawing.Point(147, 141)
            Me.lbl_creditor.Name = "lbl_creditor"
            Me.lbl_creditor.Size = New System.Drawing.Size(153, 16)
            Me.lbl_creditor.TabIndex = 7
            Me.lbl_creditor.UseMnemonic = False
            '
            '_Label1_7
            '
            Me._Label1_7.Appearance.BackColor = System.Drawing.Color.Transparent
            Me._Label1_7.Appearance.Font = New System.Drawing.Font("Arial", 10.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
            Me._Label1_7.Appearance.ForeColor = System.Drawing.SystemColors.ControlText
            Me._Label1_7.Cursor = System.Windows.Forms.Cursors.Default
            Me._Label1_7.Location = New System.Drawing.Point(13, 141)
            Me._Label1_7.Name = "_Label1_7"
            Me._Label1_7.Size = New System.Drawing.Size(128, 16)
            Me._Label1_7.TabIndex = 6
            Me._Label1_7.Text = "Processing Creditor:"
            Me._Label1_7.UseMnemonic = False
            '
            'lbl_creditor_count
            '
            Me.lbl_creditor_count.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
            Me.lbl_creditor_count.Appearance.BackColor = System.Drawing.Color.Transparent
            Me.lbl_creditor_count.Appearance.Font = New System.Drawing.Font("Arial", 10.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
            Me.lbl_creditor_count.Appearance.ForeColor = System.Drawing.SystemColors.ControlText
            Me.lbl_creditor_count.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
            Me.lbl_creditor_count.Appearance.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Top
            Me.lbl_creditor_count.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None
            Me.lbl_creditor_count.Cursor = System.Windows.Forms.Cursors.Default
            Me.lbl_creditor_count.Location = New System.Drawing.Point(184, 118)
            Me.lbl_creditor_count.Name = "lbl_creditor_count"
            Me.lbl_creditor_count.Size = New System.Drawing.Size(116, 16)
            Me.lbl_creditor_count.TabIndex = 5
            Me.lbl_creditor_count.Text = "0"
            Me.lbl_creditor_count.UseMnemonic = False
            '
            '_Label1_6
            '
            Me._Label1_6.Appearance.BackColor = System.Drawing.Color.Transparent
            Me._Label1_6.Appearance.Font = New System.Drawing.Font("Arial", 10.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
            Me._Label1_6.Appearance.ForeColor = System.Drawing.SystemColors.ControlText
            Me._Label1_6.Cursor = System.Windows.Forms.Cursors.Default
            Me._Label1_6.Location = New System.Drawing.Point(13, 118)
            Me._Label1_6.Name = "_Label1_6"
            Me._Label1_6.Size = New System.Drawing.Size(130, 16)
            Me._Label1_6.TabIndex = 4
            Me._Label1_6.Text = "Creditors Processed:"
            Me._Label1_6.UseMnemonic = False
            '
            'lbl_billed
            '
            Me.lbl_billed.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
            Me.lbl_billed.Appearance.BackColor = System.Drawing.Color.Transparent
            Me.lbl_billed.Appearance.Font = New System.Drawing.Font("Arial", 10.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
            Me.lbl_billed.Appearance.ForeColor = System.Drawing.SystemColors.ControlText
            Me.lbl_billed.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
            Me.lbl_billed.Appearance.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Top
            Me.lbl_billed.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None
            Me.lbl_billed.Cursor = System.Windows.Forms.Cursors.Default
            Me.lbl_billed.Location = New System.Drawing.Point(184, 256)
            Me.lbl_billed.Name = "lbl_billed"
            Me.lbl_billed.Size = New System.Drawing.Size(116, 16)
            Me.lbl_billed.TabIndex = 17
            Me.lbl_billed.Text = "$0.00"
            Me.lbl_billed.UseMnemonic = False
            '
            '_Label1_5
            '
            Me._Label1_5.Appearance.BackColor = System.Drawing.Color.Transparent
            Me._Label1_5.Appearance.Font = New System.Drawing.Font("Arial", 10.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
            Me._Label1_5.Appearance.ForeColor = System.Drawing.SystemColors.ControlText
            Me._Label1_5.Cursor = System.Windows.Forms.Cursors.Default
            Me._Label1_5.Location = New System.Drawing.Point(13, 256)
            Me._Label1_5.Name = "_Label1_5"
            Me._Label1_5.Size = New System.Drawing.Size(77, 16)
            Me._Label1_5.TabIndex = 16
            Me._Label1_5.Text = "Total Billed:"
            Me._Label1_5.UseMnemonic = False
            '
            'lbl_net
            '
            Me.lbl_net.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
            Me.lbl_net.Appearance.BackColor = System.Drawing.Color.Transparent
            Me.lbl_net.Appearance.Font = New System.Drawing.Font("Arial", 10.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
            Me.lbl_net.Appearance.ForeColor = System.Drawing.SystemColors.ControlText
            Me.lbl_net.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
            Me.lbl_net.Appearance.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Top
            Me.lbl_net.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None
            Me.lbl_net.Cursor = System.Windows.Forms.Cursors.Default
            Me.lbl_net.Location = New System.Drawing.Point(184, 233)
            Me.lbl_net.Name = "lbl_net"
            Me.lbl_net.Size = New System.Drawing.Size(116, 16)
            Me.lbl_net.TabIndex = 15
            Me.lbl_net.Text = "$0.00"
            Me.lbl_net.UseMnemonic = False
            '
            '_Label1_4
            '
            Me._Label1_4.Appearance.BackColor = System.Drawing.Color.Transparent
            Me._Label1_4.Appearance.Font = New System.Drawing.Font("Arial", 10.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
            Me._Label1_4.Appearance.ForeColor = System.Drawing.SystemColors.ControlText
            Me._Label1_4.Cursor = System.Windows.Forms.Cursors.Default
            Me._Label1_4.Location = New System.Drawing.Point(13, 233)
            Me._Label1_4.Name = "_Label1_4"
            Me._Label1_4.Size = New System.Drawing.Size(115, 16)
            Me._Label1_4.TabIndex = 14
            Me._Label1_4.Text = "Net Disbursement:"
            Me._Label1_4.UseMnemonic = False
            '
            'lbl_deducted
            '
            Me.lbl_deducted.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
            Me.lbl_deducted.Appearance.BackColor = System.Drawing.Color.Transparent
            Me.lbl_deducted.Appearance.Font = New System.Drawing.Font("Arial", 10.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
            Me.lbl_deducted.Appearance.ForeColor = System.Drawing.SystemColors.ControlText
            Me.lbl_deducted.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
            Me.lbl_deducted.Appearance.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Top
            Me.lbl_deducted.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None
            Me.lbl_deducted.Cursor = System.Windows.Forms.Cursors.Default
            Me.lbl_deducted.Location = New System.Drawing.Point(184, 210)
            Me.lbl_deducted.Name = "lbl_deducted"
            Me.lbl_deducted.Size = New System.Drawing.Size(116, 16)
            Me.lbl_deducted.TabIndex = 13
            Me.lbl_deducted.Text = "$0.00"
            Me.lbl_deducted.UseMnemonic = False
            '
            '_Label1_3
            '
            Me._Label1_3.Appearance.BackColor = System.Drawing.Color.Transparent
            Me._Label1_3.Appearance.Font = New System.Drawing.Font("Arial", 10.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
            Me._Label1_3.Appearance.ForeColor = System.Drawing.SystemColors.ControlText
            Me._Label1_3.Cursor = System.Windows.Forms.Cursors.Default
            Me._Label1_3.Location = New System.Drawing.Point(13, 210)
            Me._Label1_3.Name = "_Label1_3"
            Me._Label1_3.Size = New System.Drawing.Size(100, 16)
            Me._Label1_3.TabIndex = 12
            Me._Label1_3.Text = "Total Deducted:"
            Me._Label1_3.UseMnemonic = False
            '
            'lbl_gross
            '
            Me.lbl_gross.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
            Me.lbl_gross.Appearance.BackColor = System.Drawing.Color.Transparent
            Me.lbl_gross.Appearance.Font = New System.Drawing.Font("Arial", 10.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
            Me.lbl_gross.Appearance.ForeColor = System.Drawing.SystemColors.ControlText
            Me.lbl_gross.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
            Me.lbl_gross.Appearance.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Top
            Me.lbl_gross.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None
            Me.lbl_gross.Cursor = System.Windows.Forms.Cursors.Default
            Me.lbl_gross.Location = New System.Drawing.Point(184, 187)
            Me.lbl_gross.Name = "lbl_gross"
            Me.lbl_gross.Size = New System.Drawing.Size(116, 16)
            Me.lbl_gross.TabIndex = 11
            Me.lbl_gross.Text = "$0.00"
            Me.lbl_gross.UseMnemonic = False
            '
            '_Label1_2
            '
            Me._Label1_2.Appearance.BackColor = System.Drawing.Color.Transparent
            Me._Label1_2.Appearance.Font = New System.Drawing.Font("Arial", 10.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
            Me._Label1_2.Appearance.ForeColor = System.Drawing.SystemColors.ControlText
            Me._Label1_2.Cursor = System.Windows.Forms.Cursors.Default
            Me._Label1_2.Location = New System.Drawing.Point(12, 187)
            Me._Label1_2.Name = "_Label1_2"
            Me._Label1_2.Size = New System.Drawing.Size(129, 16)
            Me._Label1_2.TabIndex = 10
            Me._Label1_2.Text = "Gross Disbursement:"
            Me._Label1_2.UseMnemonic = False
            '
            'lbl_client
            '
            Me.lbl_client.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
            Me.lbl_client.Appearance.BackColor = System.Drawing.Color.Transparent
            Me.lbl_client.Appearance.Font = New System.Drawing.Font("Arial", 10.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
            Me.lbl_client.Appearance.ForeColor = System.Drawing.SystemColors.ControlText
            Me.lbl_client.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
            Me.lbl_client.Appearance.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Top
            Me.lbl_client.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None
            Me.lbl_client.Cursor = System.Windows.Forms.Cursors.Default
            Me.lbl_client.Location = New System.Drawing.Point(184, 164)
            Me.lbl_client.Name = "lbl_client"
            Me.lbl_client.Size = New System.Drawing.Size(116, 16)
            Me.lbl_client.TabIndex = 9
            Me.lbl_client.Text = "0000000"
            Me.lbl_client.UseMnemonic = False
            '
            '_Label1_1
            '
            Me._Label1_1.Appearance.BackColor = System.Drawing.Color.Transparent
            Me._Label1_1.Appearance.Font = New System.Drawing.Font("Arial", 10.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
            Me._Label1_1.Appearance.ForeColor = System.Drawing.SystemColors.ControlText
            Me._Label1_1.Cursor = System.Windows.Forms.Cursors.Default
            Me._Label1_1.Location = New System.Drawing.Point(13, 164)
            Me._Label1_1.Name = "_Label1_1"
            Me._Label1_1.Size = New System.Drawing.Size(114, 16)
            Me._Label1_1.TabIndex = 8
            Me._Label1_1.Text = "Processing Client:"
            Me._Label1_1.UseMnemonic = False
            '
            'lbl_elapsed
            '
            Me.lbl_elapsed.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
            Me.lbl_elapsed.Appearance.BackColor = System.Drawing.Color.Transparent
            Me.lbl_elapsed.Appearance.Font = New System.Drawing.Font("Arial", 10.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
            Me.lbl_elapsed.Appearance.ForeColor = System.Drawing.SystemColors.ControlText
            Me.lbl_elapsed.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
            Me.lbl_elapsed.Appearance.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Top
            Me.lbl_elapsed.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None
            Me.lbl_elapsed.Cursor = System.Windows.Forms.Cursors.Default
            Me.lbl_elapsed.Location = New System.Drawing.Point(184, 95)
            Me.lbl_elapsed.Name = "lbl_elapsed"
            Me.lbl_elapsed.Size = New System.Drawing.Size(116, 16)
            Me.lbl_elapsed.TabIndex = 3
            Me.lbl_elapsed.Text = "00:00:00"
            Me.lbl_elapsed.UseMnemonic = False
            '
            '_Label1_0
            '
            Me._Label1_0.Appearance.BackColor = System.Drawing.Color.Transparent
            Me._Label1_0.Appearance.Font = New System.Drawing.Font("Arial", 10.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
            Me._Label1_0.Appearance.ForeColor = System.Drawing.SystemColors.ControlText
            Me._Label1_0.Cursor = System.Windows.Forms.Cursors.Default
            Me._Label1_0.Location = New System.Drawing.Point(13, 95)
            Me._Label1_0.Name = "_Label1_0"
            Me._Label1_0.Size = New System.Drawing.Size(90, 16)
            Me._Label1_0.TabIndex = 2
            Me._Label1_0.Text = "Elapsed Time:"
            Me._Label1_0.UseMnemonic = False
            '
            'Button_Close
            '
            Me.Button_Close.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
            Me.Button_Close.Location = New System.Drawing.Point(232, 8)
            Me.Button_Close.Name = "Button_Close"
            Me.Button_Close.Size = New System.Drawing.Size(75, 23)
            Me.Button_Close.TabIndex = 18
            Me.Button_Close.Text = "&Close"
            Me.Button_Close.Visible = False
            '
            'ProgressBarControl1
            '
            Me.ProgressBarControl1.Anchor = CType(((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left) _
                Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
            Me.ProgressBarControl1.Location = New System.Drawing.Point(13, 288)
            Me.ProgressBarControl1.Name = "ProgressBarControl1"
            Me.ProgressBarControl1.Properties.Appearance.ForeColor = System.Drawing.Color.DodgerBlue
            Me.ProgressBarControl1.Properties.ProgressViewStyle = DevExpress.XtraEditors.Controls.ProgressViewStyle.Solid
            Me.ProgressBarControl1.Properties.Step = 5
            Me.ProgressBarControl1.Size = New System.Drawing.Size(287, 18)
            Me.ProgressBarControl1.TabIndex = 19
            '
            'ProcessControl
            '
            Me.Appearance.Font = New System.Drawing.Font("Arial", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
            Me.Appearance.Options.UseFont = True
            Me.Controls.Add(Me.lbl_Status)
            Me.Controls.Add(Me.ProgressBarControl1)
            Me.Controls.Add(Me.lbl_creditor)
            Me.Controls.Add(Me._Label1_7)
            Me.Controls.Add(Me.lbl_creditor_count)
            Me.Controls.Add(Me._Label1_6)
            Me.Controls.Add(Me.lbl_billed)
            Me.Controls.Add(Me._Label1_5)
            Me.Controls.Add(Me.lbl_net)
            Me.Controls.Add(Me._Label1_4)
            Me.Controls.Add(Me.lbl_deducted)
            Me.Controls.Add(Me._Label1_3)
            Me.Controls.Add(Me.lbl_gross)
            Me.Controls.Add(Me._Label1_2)
            Me.Controls.Add(Me.lbl_client)
            Me.Controls.Add(Me._Label1_1)
            Me.Controls.Add(Me.lbl_elapsed)
            Me.Controls.Add(Me._Label1_0)
            Me.Controls.Add(Me.lbl_Complete)
            Me.Controls.Add(Me.Button_Close)
            Me.Cursor = System.Windows.Forms.Cursors.Default
            Me.Name = "ProcessControl"
            Me.Size = New System.Drawing.Size(312, 318)
            CType(Me.ProgressBarControl1.Properties, System.ComponentModel.ISupportInitialize).EndInit()
            Me.ResumeLayout(False)
            Me.PerformLayout()

        End Sub

        Public WithEvents Timer1 As System.Windows.Forms.Timer
        Public WithEvents lbl_Status As DevExpress.XtraEditors.LabelControl
        Public WithEvents lbl_Complete As DevExpress.XtraEditors.LabelControl
        Public WithEvents lbl_creditor As DevExpress.XtraEditors.LabelControl
        Public WithEvents _Label1_7 As DevExpress.XtraEditors.LabelControl
        Public WithEvents lbl_creditor_count As DevExpress.XtraEditors.LabelControl
        Public WithEvents _Label1_6 As DevExpress.XtraEditors.LabelControl
        Public WithEvents lbl_billed As DevExpress.XtraEditors.LabelControl
        Public WithEvents _Label1_5 As DevExpress.XtraEditors.LabelControl
        Public WithEvents lbl_net As DevExpress.XtraEditors.LabelControl
        Public WithEvents _Label1_4 As DevExpress.XtraEditors.LabelControl
        Public WithEvents lbl_deducted As DevExpress.XtraEditors.LabelControl
        Public WithEvents _Label1_3 As DevExpress.XtraEditors.LabelControl
        Public WithEvents lbl_gross As DevExpress.XtraEditors.LabelControl
        Public WithEvents _Label1_2 As DevExpress.XtraEditors.LabelControl
        Public WithEvents lbl_client As DevExpress.XtraEditors.LabelControl
        Public WithEvents _Label1_1 As DevExpress.XtraEditors.LabelControl
        Public WithEvents lbl_elapsed As DevExpress.XtraEditors.LabelControl
        Public WithEvents _Label1_0 As DevExpress.XtraEditors.LabelControl
        Friend WithEvents Button_Close As DevExpress.XtraEditors.SimpleButton
        Friend WithEvents bt As New System.ComponentModel.BackgroundWorker
        Friend WithEvents ProgressBarControl1 As DevExpress.XtraEditors.ProgressBarControl
    End Class
End Namespace