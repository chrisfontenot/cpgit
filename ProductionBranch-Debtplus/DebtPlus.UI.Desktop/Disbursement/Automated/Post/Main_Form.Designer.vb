﻿Namespace Disbursement.Automated.Post
    <Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
    Partial Class Main_Form
        Inherits DebtPlus.Data.Forms.DebtPlusForm

        'Form overrides dispose to clean up the component list.
        Protected Overloads Overrides Sub Dispose(ByVal Disposing As Boolean)
            If Disposing Then
                If components IsNot Nothing Then
                    components.Dispose()
                End If
            End If
            MyBase.Dispose(Disposing)
        End Sub

        'Required by the Windows Form Designer
        Private components As System.ComponentModel.IContainer

        'NOTE: The following procedure is required by the Windows Form Designer
        'It can be modified using the Windows Form Designer.
        'Do not modify it using the code editor.
        Private Sub InitializeComponent()
            Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(Main_Form))
            Me.BatchControl1 = New DebtPlus.UI.FormLib.Disbursement.DisbursementRegisterControl()
            Me.ProcessControl1 = New DebtPlus.UI.Desktop.Disbursement.Automated.Post.ProcessControl()
            CType(Me.DxErrorProvider1, System.ComponentModel.ISupportInitialize).BeginInit()
            Me.SuspendLayout()
            '
            'BatchControl1
            '
            Me.BatchControl1.Dock = System.Windows.Forms.DockStyle.Fill
            Me.BatchControl1.Location = New System.Drawing.Point(0, 0)
            Me.BatchControl1.Name = "BatchControl1"
            Me.BatchControl1.Size = New System.Drawing.Size(424, 318)
            Me.BatchControl1.TabIndex = 0
            '
            'ProcessControl1
            '
            Me.ProcessControl1.Appearance.Font = New System.Drawing.Font("Arial", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
            Me.ProcessControl1.Appearance.Options.UseFont = True
            Me.ProcessControl1.Cursor = System.Windows.Forms.Cursors.Default
            Me.ProcessControl1.Dock = System.Windows.Forms.DockStyle.Fill
            Me.ProcessControl1.Location = New System.Drawing.Point(0, 0)
            Me.ProcessControl1.Name = "ProcessControl1"
            Me.ProcessControl1.Size = New System.Drawing.Size(424, 318)
            Me.ProcessControl1.TabIndex = 1
            '
            'Main_Form
            '
            Me.Appearance.Font = New System.Drawing.Font("Arial", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
            Me.Appearance.Options.UseFont = True
            Me.ClientSize = New System.Drawing.Size(424, 318)
            Me.ControlBox = False
            Me.Controls.Add(Me.ProcessControl1)
            Me.Controls.Add(Me.BatchControl1)
            Me.Cursor = System.Windows.Forms.Cursors.Default
            Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
            Me.Location = New System.Drawing.Point(3, 22)
            Me.MaximizeBox = False
            Me.MinimizeBox = False
            Me.Name = "Main_Form"
            Me.Text = "Posting Disbursement Status"
            CType(Me.DxErrorProvider1, System.ComponentModel.ISupportInitialize).EndInit()
            Me.ResumeLayout(False)

        End Sub
        Friend WithEvents BatchControl1 As DebtPlus.UI.FormLib.Disbursement.DisbursementRegisterControl
        Friend WithEvents ProcessControl1 As ProcessControl
    End Class
End Namespace