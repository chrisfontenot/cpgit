#Region "Copyright 2000-2012 DebtPlus, L.L.C."
'{*******************************************************************}
'{                                                                   }
'{       DebtPlus Debt Management System                             }
'{                                                                   }
'{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
'{       ALL RIGHTS RESERVED                                         }
'{                                                                   }
'{   The entire contents of this file is protected by U.S. and       }
'{   International Copyright Laws. Unauthorized reproduction,        }
'{   reverse-engineering, and distribution of all or any portion of  }
'{   the code contained in this file is strictly prohibited and may  }
'{   result in severe civil and criminal penalties and will be       }
'{   prosecuted to the maximum extent possible under the law.        }
'{                                                                   }
'{   RESTRICTIONS                                                    }
'{                                                                   }
'{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
'{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
'{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
'{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
'{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
'{                                                                   }
'{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
'{   ADDITIONAL RESTRICTIONS.                                        }
'{                                                                   }
'{*******************************************************************}

#End Region

Option Compare Binary
Option Explicit On
Option Strict On

Imports DevExpress.XtraGrid.Views.Base
Imports DevExpress.XtraGrid.Columns
Imports DebtPlus.UI.Client.Widgets.Controls
Imports DebtPlus.Data.Forms
Imports DevExpress.XtraEditors.Repository
Imports System.Data.SqlClient
Imports System.Text
Imports DevExpress.XtraGrid.Views.Grid
Imports DevExpress.XtraGrid.Views.Grid.ViewInfo
Imports System.IO
Imports System.Windows.Forms

Namespace HUD.Interview.Update
    Friend Class Form1
        Inherits DebtPlusForm

        Dim ds As New DataSet("ds")

        ''' <summary>
        ''' Normal way to create my class
        ''' </summary>
        Public Sub New()
            MyBase.New()
            InitializeComponent()

            AddHandler Me.Load, AddressOf MyGridControl_Load
            AddHandler ContextMenu1.Popup, AddressOf ContextMenu1_Popup
            AddHandler GridView1.Click, AddressOf GridView1_Click
            AddHandler Me.FormClosing, AddressOf Form1_FormClosing
            AddHandler Me.Load, AddressOf Form1_Load
            AddHandler GridView1.FocusedRowChanged, AddressOf GridView1_FocusedRowChanged
            AddHandler GridView1.CellValueChanged, AddressOf GridView1_CellValueChanged
            AddHandler MenuItem_delete.Click, AddressOf MenuItem_delete_Click
            AddHandler txt_client.Validated, AddressOf txt_client_Validated
        End Sub

        Friend WithEvents txt_client As DebtPlus.UI.Client.Widgets.Controls.ClientID
        Friend WithEvents col_HUD_Grant As GridColumn
        Friend WithEvents LookUpEdit_HUD_Grant As RepositoryItemLookUpEdit
        Friend WithEvents col_HousingFeeAmount As GridColumn
        Friend WithEvents CalcEdit_HousingFeeAmount As RepositoryItemCalcEdit

        Private ap As ArgParser

        Public Sub New(ByVal ap As ArgParser)
            MyClass.New()
            Me.ap = ap
        End Sub

#Region " Windows Form Designer generated code "

        'Form overrides dispose to clean up the component list.
        Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
            If disposing Then
                If components IsNot Nothing Then
                    components.Dispose()
                End If
            End If
            MyBase.Dispose(disposing)
        End Sub

        'Required by the Windows Form Designer
        Private components As System.ComponentModel.IContainer

        'NOTE: The following procedure is required by the Windows Form Designer
        'It can be modified using the Windows Form Designer.  
        'Do not modify it using the code editor.
        Friend WithEvents LabelControl1 As DevExpress.XtraEditors.LabelControl
        Friend WithEvents LabelControl2 As DevExpress.XtraEditors.LabelControl
        Friend WithEvents lbl_client_name As DevExpress.XtraEditors.LabelControl
        Friend WithEvents GridControl1 As DevExpress.XtraGrid.GridControl
        Friend WithEvents GridView1 As DevExpress.XtraGrid.Views.Grid.GridView
        Friend WithEvents col_hud_interview As DevExpress.XtraGrid.Columns.GridColumn
        Friend WithEvents col_interview_date As DevExpress.XtraGrid.Columns.GridColumn
        Friend WithEvents col_result_date As DevExpress.XtraGrid.Columns.GridColumn
        Friend WithEvents LookupEdit_Interview As DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit
        Friend WithEvents LookupEdit_Result As DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit
        Friend WithEvents col_interview_counselor As DevExpress.XtraGrid.Columns.GridColumn
        Friend WithEvents col_result_counselor As DevExpress.XtraGrid.Columns.GridColumn
        Friend WithEvents col_hud_result As DevExpress.XtraGrid.Columns.GridColumn
        Friend WithEvents col_interview_type As DevExpress.XtraGrid.Columns.GridColumn
        Friend WithEvents col_termination_reason As DevExpress.XtraGrid.Columns.GridColumn
        Friend WithEvents LookupEdit_Termination As DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit
        Friend WithEvents col_termination_date As DevExpress.XtraGrid.Columns.GridColumn
        Friend WithEvents ContextMenu1 As ContextMenu
        Friend WithEvents MenuItem_delete As MenuItem

        <System.Diagnostics.DebuggerStepThrough()>
        Private Sub InitializeComponent()
            Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(Form1))
            Dim SerializableAppearanceObject1 As DevExpress.Utils.SerializableAppearanceObject = New DevExpress.Utils.SerializableAppearanceObject()
            Me.LabelControl1 = New DevExpress.XtraEditors.LabelControl()
            Me.LabelControl2 = New DevExpress.XtraEditors.LabelControl()
            Me.lbl_client_name = New DevExpress.XtraEditors.LabelControl()
            Me.GridControl1 = New DevExpress.XtraGrid.GridControl()
            Me.ContextMenu1 = New System.Windows.Forms.ContextMenu()
            Me.MenuItem_delete = New System.Windows.Forms.MenuItem()
            Me.GridView1 = New DevExpress.XtraGrid.Views.Grid.GridView()
            Me.col_hud_interview = New DevExpress.XtraGrid.Columns.GridColumn()
            Me.col_interview_type = New DevExpress.XtraGrid.Columns.GridColumn()
            Me.LookupEdit_Interview = New DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit()
            Me.col_interview_date = New DevExpress.XtraGrid.Columns.GridColumn()
            Me.col_interview_counselor = New DevExpress.XtraGrid.Columns.GridColumn()
            Me.col_hud_result = New DevExpress.XtraGrid.Columns.GridColumn()
            Me.LookupEdit_Result = New DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit()
            Me.col_result_date = New DevExpress.XtraGrid.Columns.GridColumn()
            Me.col_result_counselor = New DevExpress.XtraGrid.Columns.GridColumn()
            Me.col_termination_reason = New DevExpress.XtraGrid.Columns.GridColumn()
            Me.LookupEdit_Termination = New DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit()
            Me.col_termination_date = New DevExpress.XtraGrid.Columns.GridColumn()
            Me.col_HUD_Grant = New DevExpress.XtraGrid.Columns.GridColumn()
            Me.LookUpEdit_HUD_Grant = New DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit()
            Me.txt_client = New DebtPlus.UI.Client.Widgets.Controls.ClientID()
            Me.col_HousingFeeAmount = New DevExpress.XtraGrid.Columns.GridColumn()
            Me.CalcEdit_HousingFeeAmount = New DevExpress.XtraEditors.Repository.RepositoryItemCalcEdit()
            CType(Me.DxErrorProvider1, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.GridControl1, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.GridView1, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.LookupEdit_Interview, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.LookupEdit_Result, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.LookupEdit_Termination, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.LookUpEdit_HUD_Grant, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.txt_client.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.CalcEdit_HousingFeeAmount, System.ComponentModel.ISupportInitialize).BeginInit()
            Me.SuspendLayout()
            '
            'LabelControl1
            '
            Me.LabelControl1.Location = New System.Drawing.Point(8, 11)
            Me.LabelControl1.Name = "LabelControl1"
            Me.LabelControl1.Size = New System.Drawing.Size(27, 13)
            Me.LabelControl1.TabIndex = 0
            Me.LabelControl1.Text = "Client"
            '
            'LabelControl2
            '
            Me.LabelControl2.Location = New System.Drawing.Point(8, 40)
            Me.LabelControl2.Name = "LabelControl2"
            Me.LabelControl2.Size = New System.Drawing.Size(27, 13)
            Me.LabelControl2.TabIndex = 2
            Me.LabelControl2.Text = "Name"
            '
            'lbl_client_name
            '
            Me.lbl_client_name.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                                               Or System.Windows.Forms.AnchorStyles.Right), 
                                              System.Windows.Forms.AnchorStyles)
            Me.lbl_client_name.Location = New System.Drawing.Point(96, 40)
            Me.lbl_client_name.Name = "lbl_client_name"
            Me.lbl_client_name.Size = New System.Drawing.Size(0, 13)
            Me.lbl_client_name.TabIndex = 3
            '
            'GridControl1
            '
            Me.GridControl1.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                                             Or System.Windows.Forms.AnchorStyles.Left) _
                                            Or System.Windows.Forms.AnchorStyles.Right), 
                                           System.Windows.Forms.AnchorStyles)
            Me.GridControl1.ContextMenu = Me.ContextMenu1
            Me.GridControl1.Location = New System.Drawing.Point(0, 66)
            Me.GridControl1.MainView = Me.GridView1
            Me.GridControl1.Name = "GridControl1"
            Me.GridControl1.RepositoryItems.AddRange(New DevExpress.XtraEditors.Repository.RepositoryItem() {Me.LookupEdit_Interview, Me.LookupEdit_Result, Me.LookupEdit_Termination, Me.LookUpEdit_HUD_Grant, Me.CalcEdit_HousingFeeAmount})
            Me.GridControl1.Size = New System.Drawing.Size(580, 200)
            Me.GridControl1.TabIndex = 4
            Me.GridControl1.ViewCollection.AddRange(New DevExpress.XtraGrid.Views.Base.BaseView() {Me.GridView1})
            '
            'ContextMenu1
            '
            Me.ContextMenu1.MenuItems.AddRange(New System.Windows.Forms.MenuItem() {Me.MenuItem_delete})
            '
            'MenuItem_delete
            '
            Me.MenuItem_delete.Index = 0
            Me.MenuItem_delete.Text = "&Delete..."
            '
            'GridView1
            '
            Me.GridView1.Appearance.ColumnFilterButton.BackColor = System.Drawing.Color.FromArgb(CType(CType(104, Byte), Int32), CType(CType(184, Byte), Int32), CType(CType(251, Byte), Int32))
            Me.GridView1.Appearance.ColumnFilterButton.BorderColor = System.Drawing.Color.FromArgb(CType(CType(104, Byte), Int32), CType(CType(184, Byte), Int32), CType(CType(251, Byte), Int32))
            Me.GridView1.Appearance.ColumnFilterButton.ForeColor = System.Drawing.Color.White
            Me.GridView1.Appearance.ColumnFilterButton.Options.UseBackColor = True
            Me.GridView1.Appearance.ColumnFilterButton.Options.UseBorderColor = True
            Me.GridView1.Appearance.ColumnFilterButton.Options.UseForeColor = True
            Me.GridView1.Appearance.ColumnFilterButtonActive.BackColor = System.Drawing.Color.FromArgb(CType(CType(170, Byte), Int32), CType(CType(216, Byte), Int32), CType(CType(254, Byte), Int32))
            Me.GridView1.Appearance.ColumnFilterButtonActive.BorderColor = System.Drawing.Color.FromArgb(CType(CType(170, Byte), Int32), CType(CType(216, Byte), Int32), CType(CType(254, Byte), Int32))
            Me.GridView1.Appearance.ColumnFilterButtonActive.ForeColor = System.Drawing.Color.Black
            Me.GridView1.Appearance.ColumnFilterButtonActive.Options.UseBackColor = True
            Me.GridView1.Appearance.ColumnFilterButtonActive.Options.UseBorderColor = True
            Me.GridView1.Appearance.ColumnFilterButtonActive.Options.UseForeColor = True
            Me.GridView1.Appearance.Empty.BackColor = System.Drawing.Color.FromArgb(CType(CType(236, Byte), Int32), CType(CType(246, Byte), Int32), CType(CType(255, Byte), Int32))
            Me.GridView1.Appearance.Empty.BackColor2 = System.Drawing.Color.White
            Me.GridView1.Appearance.Empty.Options.UseBackColor = True
            Me.GridView1.Appearance.EvenRow.BackColor = System.Drawing.Color.FromArgb(CType(CType(247, Byte), Int32), CType(CType(251, Byte), Int32), CType(CType(255, Byte), Int32))
            Me.GridView1.Appearance.EvenRow.BorderColor = System.Drawing.Color.FromArgb(CType(CType(247, Byte), Int32), CType(CType(251, Byte), Int32), CType(CType(255, Byte), Int32))
            Me.GridView1.Appearance.EvenRow.ForeColor = System.Drawing.Color.Black
            Me.GridView1.Appearance.EvenRow.Options.UseBackColor = True
            Me.GridView1.Appearance.EvenRow.Options.UseBorderColor = True
            Me.GridView1.Appearance.EvenRow.Options.UseForeColor = True
            Me.GridView1.Appearance.FilterCloseButton.BackColor = System.Drawing.Color.FromArgb(CType(CType(104, Byte), Int32), CType(CType(184, Byte), Int32), CType(CType(251, Byte), Int32))
            Me.GridView1.Appearance.FilterCloseButton.BorderColor = System.Drawing.Color.FromArgb(CType(CType(104, Byte), Int32), CType(CType(184, Byte), Int32), CType(CType(251, Byte), Int32))
            Me.GridView1.Appearance.FilterCloseButton.ForeColor = System.Drawing.Color.White
            Me.GridView1.Appearance.FilterCloseButton.Options.UseBackColor = True
            Me.GridView1.Appearance.FilterCloseButton.Options.UseBorderColor = True
            Me.GridView1.Appearance.FilterCloseButton.Options.UseForeColor = True
            Me.GridView1.Appearance.FilterPanel.BackColor = System.Drawing.Color.FromArgb(CType(CType(236, Byte), Int32), CType(CType(246, Byte), Int32), CType(CType(255, Byte), Int32))
            Me.GridView1.Appearance.FilterPanel.BackColor2 = System.Drawing.Color.White
            Me.GridView1.Appearance.FilterPanel.ForeColor = System.Drawing.Color.Black
            Me.GridView1.Appearance.FilterPanel.Options.UseBackColor = True
            Me.GridView1.Appearance.FilterPanel.Options.UseForeColor = True
            Me.GridView1.Appearance.FixedLine.BackColor = System.Drawing.Color.FromArgb(CType(CType(59, Byte), Int32), CType(CType(133, Byte), Int32), CType(CType(195, Byte), Int32))
            Me.GridView1.Appearance.FixedLine.Options.UseBackColor = True
            Me.GridView1.Appearance.FocusedCell.BackColor = System.Drawing.Color.White
            Me.GridView1.Appearance.FocusedCell.ForeColor = System.Drawing.Color.Black
            Me.GridView1.Appearance.FocusedCell.Options.UseBackColor = True
            Me.GridView1.Appearance.FocusedCell.Options.UseForeColor = True
            Me.GridView1.Appearance.FocusedRow.BackColor = System.Drawing.Color.FromArgb(CType(CType(38, Byte), Int32), CType(CType(109, Byte), Int32), CType(CType(189, Byte), Int32))
            Me.GridView1.Appearance.FocusedRow.BorderColor = System.Drawing.Color.FromArgb(CType(CType(59, Byte), Int32), CType(CType(139, Byte), Int32), CType(CType(206, Byte), Int32))
            Me.GridView1.Appearance.FocusedRow.ForeColor = System.Drawing.Color.White
            Me.GridView1.Appearance.FocusedRow.Options.UseBackColor = True
            Me.GridView1.Appearance.FocusedRow.Options.UseBorderColor = True
            Me.GridView1.Appearance.FocusedRow.Options.UseForeColor = True
            Me.GridView1.Appearance.FooterPanel.BackColor = System.Drawing.Color.FromArgb(CType(CType(104, Byte), Int32), CType(CType(184, Byte), Int32), CType(CType(251, Byte), Int32))
            Me.GridView1.Appearance.FooterPanel.BorderColor = System.Drawing.Color.FromArgb(CType(CType(104, Byte), Int32), CType(CType(184, Byte), Int32), CType(CType(251, Byte), Int32))
            Me.GridView1.Appearance.FooterPanel.ForeColor = System.Drawing.Color.Black
            Me.GridView1.Appearance.FooterPanel.Options.UseBackColor = True
            Me.GridView1.Appearance.FooterPanel.Options.UseBorderColor = True
            Me.GridView1.Appearance.FooterPanel.Options.UseForeColor = True
            Me.GridView1.Appearance.GroupButton.BackColor = System.Drawing.Color.FromArgb(CType(CType(104, Byte), Int32), CType(CType(184, Byte), Int32), CType(CType(251, Byte), Int32))
            Me.GridView1.Appearance.GroupButton.BorderColor = System.Drawing.Color.FromArgb(CType(CType(104, Byte), Int32), CType(CType(184, Byte), Int32), CType(CType(251, Byte), Int32))
            Me.GridView1.Appearance.GroupButton.Options.UseBackColor = True
            Me.GridView1.Appearance.GroupButton.Options.UseBorderColor = True
            Me.GridView1.Appearance.GroupFooter.BackColor = System.Drawing.Color.FromArgb(CType(CType(170, Byte), Int32), CType(CType(216, Byte), Int32), CType(CType(254, Byte), Int32))
            Me.GridView1.Appearance.GroupFooter.BorderColor = System.Drawing.Color.FromArgb(CType(CType(170, Byte), Int32), CType(CType(216, Byte), Int32), CType(CType(254, Byte), Int32))
            Me.GridView1.Appearance.GroupFooter.ForeColor = System.Drawing.Color.Black
            Me.GridView1.Appearance.GroupFooter.Options.UseBackColor = True
            Me.GridView1.Appearance.GroupFooter.Options.UseBorderColor = True
            Me.GridView1.Appearance.GroupFooter.Options.UseForeColor = True
            Me.GridView1.Appearance.GroupPanel.BackColor = System.Drawing.Color.FromArgb(CType(CType(236, Byte), Int32), CType(CType(246, Byte), Int32), CType(CType(255, Byte), Int32))
            Me.GridView1.Appearance.GroupPanel.BackColor2 = System.Drawing.Color.White
            Me.GridView1.Appearance.GroupPanel.ForeColor = System.Drawing.Color.Black
            Me.GridView1.Appearance.GroupPanel.Options.UseBackColor = True
            Me.GridView1.Appearance.GroupPanel.Options.UseForeColor = True
            Me.GridView1.Appearance.GroupRow.BackColor = System.Drawing.Color.FromArgb(CType(CType(170, Byte), Int32), CType(CType(216, Byte), Int32), CType(CType(254, Byte), Int32))
            Me.GridView1.Appearance.GroupRow.BorderColor = System.Drawing.Color.FromArgb(CType(CType(170, Byte), Int32), CType(CType(216, Byte), Int32), CType(CType(254, Byte), Int32))
            Me.GridView1.Appearance.GroupRow.ForeColor = System.Drawing.Color.Black
            Me.GridView1.Appearance.GroupRow.Options.UseBackColor = True
            Me.GridView1.Appearance.GroupRow.Options.UseBorderColor = True
            Me.GridView1.Appearance.GroupRow.Options.UseForeColor = True
            Me.GridView1.Appearance.HeaderPanel.BackColor = System.Drawing.Color.FromArgb(CType(CType(139, Byte), Int32), CType(CType(201, Byte), Int32), CType(CType(254, Byte), Int32))
            Me.GridView1.Appearance.HeaderPanel.BorderColor = System.Drawing.Color.FromArgb(CType(CType(139, Byte), Int32), CType(CType(201, Byte), Int32), CType(CType(254, Byte), Int32))
            Me.GridView1.Appearance.HeaderPanel.ForeColor = System.Drawing.Color.Black
            Me.GridView1.Appearance.HeaderPanel.Options.UseBackColor = True
            Me.GridView1.Appearance.HeaderPanel.Options.UseBorderColor = True
            Me.GridView1.Appearance.HeaderPanel.Options.UseForeColor = True
            Me.GridView1.Appearance.HideSelectionRow.BackColor = System.Drawing.Color.FromArgb(CType(CType(105, Byte), Int32), CType(CType(170, Byte), Int32), CType(CType(225, Byte), Int32))
            Me.GridView1.Appearance.HideSelectionRow.BorderColor = System.Drawing.Color.FromArgb(CType(CType(83, Byte), Int32), CType(CType(155, Byte), Int32), CType(CType(215, Byte), Int32))
            Me.GridView1.Appearance.HideSelectionRow.ForeColor = System.Drawing.Color.FromArgb(CType(CType(236, Byte), Int32), CType(CType(246, Byte), Int32), CType(CType(255, Byte), Int32))
            Me.GridView1.Appearance.HideSelectionRow.Options.UseBackColor = True
            Me.GridView1.Appearance.HideSelectionRow.Options.UseBorderColor = True
            Me.GridView1.Appearance.HideSelectionRow.Options.UseForeColor = True
            Me.GridView1.Appearance.HorzLine.BackColor = System.Drawing.Color.FromArgb(CType(CType(104, Byte), Int32), CType(CType(184, Byte), Int32), CType(CType(251, Byte), Int32))
            Me.GridView1.Appearance.HorzLine.Options.UseBackColor = True
            Me.GridView1.Appearance.OddRow.BackColor = System.Drawing.Color.FromArgb(CType(CType(236, Byte), Int32), CType(CType(246, Byte), Int32), CType(CType(255, Byte), Int32))
            Me.GridView1.Appearance.OddRow.BorderColor = System.Drawing.Color.FromArgb(CType(CType(236, Byte), Int32), CType(CType(246, Byte), Int32), CType(CType(255, Byte), Int32))
            Me.GridView1.Appearance.OddRow.ForeColor = System.Drawing.Color.Black
            Me.GridView1.Appearance.OddRow.Options.UseBackColor = True
            Me.GridView1.Appearance.OddRow.Options.UseBorderColor = True
            Me.GridView1.Appearance.OddRow.Options.UseForeColor = True
            Me.GridView1.Appearance.Preview.Font = New System.Drawing.Font("Verdana", 7.5!)
            Me.GridView1.Appearance.Preview.ForeColor = System.Drawing.Color.FromArgb(CType(CType(83, Byte), Int32), CType(CType(155, Byte), Int32), CType(CType(215, Byte), Int32))
            Me.GridView1.Appearance.Preview.Options.UseFont = True
            Me.GridView1.Appearance.Preview.Options.UseForeColor = True
            Me.GridView1.Appearance.Row.BackColor = System.Drawing.Color.FromArgb(CType(CType(247, Byte), Int32), CType(CType(251, Byte), Int32), CType(CType(255, Byte), Int32))
            Me.GridView1.Appearance.Row.ForeColor = System.Drawing.Color.Black
            Me.GridView1.Appearance.Row.Options.UseBackColor = True
            Me.GridView1.Appearance.Row.Options.UseForeColor = True
            Me.GridView1.Appearance.RowSeparator.BackColor = System.Drawing.Color.FromArgb(CType(CType(236, Byte), Int32), CType(CType(246, Byte), Int32), CType(CType(255, Byte), Int32))
            Me.GridView1.Appearance.RowSeparator.BackColor2 = System.Drawing.Color.White
            Me.GridView1.Appearance.RowSeparator.Options.UseBackColor = True
            Me.GridView1.Appearance.SelectedRow.BackColor = System.Drawing.Color.FromArgb(CType(CType(83, Byte), Int32), CType(CType(155, Byte), Int32), CType(CType(215, Byte), Int32))
            Me.GridView1.Appearance.SelectedRow.ForeColor = System.Drawing.Color.White
            Me.GridView1.Appearance.SelectedRow.Options.UseBackColor = True
            Me.GridView1.Appearance.SelectedRow.Options.UseForeColor = True
            Me.GridView1.Appearance.TopNewRow.BackColor = System.Drawing.Color.White
            Me.GridView1.Appearance.TopNewRow.Options.UseBackColor = True
            Me.GridView1.Appearance.VertLine.BackColor = System.Drawing.Color.FromArgb(CType(CType(104, Byte), Int32), CType(CType(184, Byte), Int32), CType(CType(251, Byte), Int32))
            Me.GridView1.Appearance.VertLine.Options.UseBackColor = True
            Me.GridView1.Columns.AddRange(New DevExpress.XtraGrid.Columns.GridColumn() {Me.col_hud_interview, Me.col_interview_type, Me.col_interview_date, Me.col_interview_counselor, Me.col_hud_result, Me.col_result_date, Me.col_result_counselor, Me.col_termination_reason, Me.col_termination_date, Me.col_HUD_Grant, Me.col_HousingFeeAmount})
            Me.GridView1.GridControl = Me.GridControl1
            Me.GridView1.Name = "GridView1"
            Me.GridView1.OptionsView.ShowGroupPanel = False
            Me.GridView1.OptionsView.ShowIndicator = False
            '
            'col_hud_interview
            '
            Me.col_hud_interview.AppearanceCell.Options.UseTextOptions = True
            Me.col_hud_interview.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
            Me.col_hud_interview.AppearanceHeader.Options.UseTextOptions = True
            Me.col_hud_interview.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
            Me.col_hud_interview.Caption = "Record ID"
            Me.col_hud_interview.CustomizationCaption = "Record Number"
            Me.col_hud_interview.FieldName = "hud_interview"
            Me.col_hud_interview.Name = "col_hud_interview"
            Me.col_hud_interview.OptionsColumn.AllowEdit = False
            Me.col_hud_interview.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.[True]
            Me.col_hud_interview.ToolTip = "Interview Record Number"
            '
            'col_interview_type
            '
            Me.col_interview_type.Caption = "Interview"
            Me.col_interview_type.ColumnEdit = Me.LookupEdit_Interview
            Me.col_interview_type.CustomizationCaption = "Interview Description"
            Me.col_interview_type.FieldName = "interview_type"
            Me.col_interview_type.Name = "col_interview_type"
            Me.col_interview_type.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.[True]
            Me.col_interview_type.Visible = True
            Me.col_interview_type.VisibleIndex = 1
            Me.col_interview_type.Width = 140
            '
            'LookupEdit_Interview
            '
            Me.LookupEdit_Interview.AllowNullInput = DevExpress.Utils.DefaultBoolean.[False]
            Me.LookupEdit_Interview.AutoHeight = False
            Me.LookupEdit_Interview.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
            Me.LookupEdit_Interview.Columns.AddRange(New DevExpress.XtraEditors.Controls.LookUpColumnInfo() {New DevExpress.XtraEditors.Controls.LookUpColumnInfo("oID", "ID", 20, DevExpress.Utils.FormatType.Numeric, "f0", False, DevExpress.Utils.HorzAlignment.[Default]), New DevExpress.XtraEditors.Controls.LookUpColumnInfo("description", 60, "Description")})
            Me.LookupEdit_Interview.Name = "LookupEdit_Interview"
            Me.LookupEdit_Interview.NullText = ""
            Me.LookupEdit_Interview.ShowFooter = False
            Me.LookupEdit_Interview.ShowHeader = False
            Me.LookupEdit_Interview.ShowLines = False
            '
            'col_interview_date
            '
            Me.col_interview_date.AppearanceCell.Options.UseTextOptions = True
            Me.col_interview_date.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
            Me.col_interview_date.AppearanceHeader.Options.UseTextOptions = True
            Me.col_interview_date.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
            Me.col_interview_date.Caption = "Interview Date"
            Me.col_interview_date.CustomizationCaption = "Interview Date"
            Me.col_interview_date.DisplayFormat.FormatString = "d"
            Me.col_interview_date.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime
            Me.col_interview_date.FieldName = "interview_date"
            Me.col_interview_date.GroupFormat.FormatString = "d"
            Me.col_interview_date.GroupFormat.FormatType = DevExpress.Utils.FormatType.DateTime
            Me.col_interview_date.GroupInterval = DevExpress.XtraGrid.ColumnGroupInterval.DateMonth
            Me.col_interview_date.Name = "col_interview_date"
            Me.col_interview_date.OptionsColumn.AllowEdit = False
            Me.col_interview_date.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.[True]
            Me.col_interview_date.Visible = True
            Me.col_interview_date.VisibleIndex = 0
            Me.col_interview_date.Width = 80
            '
            'col_interview_counselor
            '
            Me.col_interview_counselor.Caption = "Interview Counselor"
            Me.col_interview_counselor.CustomizationCaption = "Interview Counselor"
            Me.col_interview_counselor.FieldName = "interview_counselor"
            Me.col_interview_counselor.Name = "col_interview_counselor"
            Me.col_interview_counselor.OptionsColumn.AllowEdit = False
            Me.col_interview_counselor.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.[True]
            '
            'col_hud_result
            '
            Me.col_hud_result.Caption = "Result"
            Me.col_hud_result.ColumnEdit = Me.LookupEdit_Result
            Me.col_hud_result.CustomizationCaption = "Result"
            Me.col_hud_result.FieldName = "hud_result"
            Me.col_hud_result.Name = "col_hud_result"
            Me.col_hud_result.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.[True]
            Me.col_hud_result.Visible = True
            Me.col_hud_result.VisibleIndex = 3
            Me.col_hud_result.Width = 173
            '
            'LookupEdit_Result
            '
            Me.LookupEdit_Result.AllowNullInput = DevExpress.Utils.DefaultBoolean.[True]
            Me.LookupEdit_Result.AutoHeight = False
            Me.LookupEdit_Result.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
            Me.LookupEdit_Result.Columns.AddRange(New DevExpress.XtraEditors.Controls.LookUpColumnInfo() {New DevExpress.XtraEditors.Controls.LookUpColumnInfo("result", "ID", 20, DevExpress.Utils.FormatType.Numeric, "f0", False, DevExpress.Utils.HorzAlignment.[Default]), New DevExpress.XtraEditors.Controls.LookUpColumnInfo("description", "Description", 20, DevExpress.Utils.FormatType.None, "", True, DevExpress.Utils.HorzAlignment.[Default], DevExpress.Data.ColumnSortOrder.Ascending)})
            Me.LookupEdit_Result.Name = "LookupEdit_Result"
            Me.LookupEdit_Result.NullText = ""
            Me.LookupEdit_Result.ShowFooter = False
            Me.LookupEdit_Result.ShowHeader = False
            Me.LookupEdit_Result.ShowLines = False
            '
            'col_result_date
            '
            Me.col_result_date.AppearanceCell.Options.UseTextOptions = True
            Me.col_result_date.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
            Me.col_result_date.AppearanceHeader.Options.UseTextOptions = True
            Me.col_result_date.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
            Me.col_result_date.Caption = "Result Date"
            Me.col_result_date.CustomizationCaption = "Result Date"
            Me.col_result_date.DisplayFormat.FormatString = "d"
            Me.col_result_date.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime
            Me.col_result_date.FieldName = "result_date"
            Me.col_result_date.GroupFormat.FormatString = "d"
            Me.col_result_date.GroupFormat.FormatType = DevExpress.Utils.FormatType.DateTime
            Me.col_result_date.GroupInterval = DevExpress.XtraGrid.ColumnGroupInterval.DateMonth
            Me.col_result_date.Name = "col_result_date"
            Me.col_result_date.OptionsColumn.AllowEdit = False
            Me.col_result_date.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.[True]
            Me.col_result_date.Visible = True
            Me.col_result_date.VisibleIndex = 2
            Me.col_result_date.Width = 148
            '
            'col_result_counselor
            '
            Me.col_result_counselor.Caption = "Result Counselor"
            Me.col_result_counselor.CustomizationCaption = "Result Counselor"
            Me.col_result_counselor.FieldName = "result_counselor"
            Me.col_result_counselor.Name = "col_result_counselor"
            Me.col_result_counselor.OptionsColumn.AllowEdit = False
            Me.col_result_counselor.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.[True]
            '
            'col_termination_reason
            '
            Me.col_termination_reason.Caption = "Termination"
            Me.col_termination_reason.ColumnEdit = Me.LookupEdit_Termination
            Me.col_termination_reason.CustomizationCaption = "Termination Reason"
            Me.col_termination_reason.FieldName = "termination_reason"
            Me.col_termination_reason.Name = "col_termination_reason"
            Me.col_termination_reason.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.[True]
            Me.col_termination_reason.Visible = True
            Me.col_termination_reason.VisibleIndex = 5
            Me.col_termination_reason.Width = 187
            '
            'LookupEdit_Termination
            '
            Me.LookupEdit_Termination.AllowNullInput = DevExpress.Utils.DefaultBoolean.[True]
            Me.LookupEdit_Termination.AutoHeight = False
            Me.LookupEdit_Termination.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo), New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
            Me.LookupEdit_Termination.Columns.AddRange(New DevExpress.XtraEditors.Controls.LookUpColumnInfo() {New DevExpress.XtraEditors.Controls.LookUpColumnInfo("oID", "ID", 20, DevExpress.Utils.FormatType.Numeric, "", False, DevExpress.Utils.HorzAlignment.[Default]), New DevExpress.XtraEditors.Controls.LookUpColumnInfo("description", "Description", 20, DevExpress.Utils.FormatType.None, "", True, DevExpress.Utils.HorzAlignment.[Default], DevExpress.Data.ColumnSortOrder.Ascending)})
            Me.LookupEdit_Termination.DisplayMember = "(None)"
            Me.LookupEdit_Termination.Name = "LookupEdit_Termination"
            Me.LookupEdit_Termination.NullText = "[Not Terminated - Still Pending]"
            Me.LookupEdit_Termination.ShowFooter = False
            Me.LookupEdit_Termination.ShowHeader = False
            '
            'col_termination_date
            '
            Me.col_termination_date.Caption = "Termination Date"
            Me.col_termination_date.CustomizationCaption = "Termination Date"
            Me.col_termination_date.FieldName = "termination_date"
            Me.col_termination_date.Name = "col_termination_date"
            Me.col_termination_date.OptionsColumn.AllowEdit = False
            Me.col_termination_date.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.[True]
            Me.col_termination_date.Visible = True
            Me.col_termination_date.VisibleIndex = 4
            '
            'col_HUD_Grant
            '
            Me.col_HUD_Grant.AppearanceCell.Options.UseTextOptions = True
            Me.col_HUD_Grant.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
            Me.col_HUD_Grant.AppearanceHeader.Options.UseTextOptions = True
            Me.col_HUD_Grant.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
            Me.col_HUD_Grant.Caption = "Grant"
            Me.col_HUD_Grant.ColumnEdit = Me.LookUpEdit_HUD_Grant
            Me.col_HUD_Grant.CustomizationCaption = "HUD Grant ID"
            Me.col_HUD_Grant.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Custom
            Me.col_HUD_Grant.FieldName = "HUD_Grant"
            Me.col_HUD_Grant.GroupFormat.FormatType = DevExpress.Utils.FormatType.Custom
            Me.col_HUD_Grant.Name = "col_HUD_Grant"
            Me.col_HUD_Grant.SortMode = DevExpress.XtraGrid.ColumnSortMode.DisplayText
            Me.col_HUD_Grant.ToolTip = "HUD Grant"
            Me.col_HUD_Grant.Visible = True
            Me.col_HUD_Grant.VisibleIndex = 6
            '
            'LookUpEdit_HUD_Grant
            '
            Me.LookUpEdit_HUD_Grant.AllowNullInput = DevExpress.Utils.DefaultBoolean.[False]
            Me.LookUpEdit_HUD_Grant.AutoHeight = False
            Me.LookUpEdit_HUD_Grant.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
            Me.LookUpEdit_HUD_Grant.Columns.AddRange(New DevExpress.XtraEditors.Controls.LookUpColumnInfo() {New DevExpress.XtraEditors.Controls.LookUpColumnInfo("oID", "ID", 20, DevExpress.Utils.FormatType.Numeric, "f0", False, DevExpress.Utils.HorzAlignment.Far), New DevExpress.XtraEditors.Controls.LookUpColumnInfo("description", "Description", 20, DevExpress.Utils.FormatType.None, "", True, DevExpress.Utils.HorzAlignment.Near, DevExpress.Data.ColumnSortOrder.Ascending, DevExpress.Utils.DefaultBoolean.[True])})
            Me.LookUpEdit_HUD_Grant.DisplayMember = "description"
            Me.LookUpEdit_HUD_Grant.Name = "LookUpEdit_HUD_Grant"
            Me.LookUpEdit_HUD_Grant.NullText = ""
            Me.LookUpEdit_HUD_Grant.ShowFooter = False
            Me.LookUpEdit_HUD_Grant.ShowHeader = False
            Me.LookUpEdit_HUD_Grant.ValueMember = "oID"
            '
            'txt_client
            '
            Me.txt_client.AllowDrop = True
            Me.txt_client.Location = New System.Drawing.Point(96, 8)
            Me.txt_client.Name = "txt_client"
            Me.txt_client.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[False]
            Me.txt_client.Properties.Appearance.Options.UseTextOptions = True
            Me.txt_client.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
            Me.txt_client.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph, "", -1, True, True, False, DevExpress.XtraEditors.ImageLocation.MiddleCenter, CType(resources.GetObject("txt_client.Properties.Buttons"), System.Drawing.Image), New DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), SerializableAppearanceObject1, "Click here to search for a client ID", "search", Nothing, True)})
            Me.txt_client.Properties.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
            Me.txt_client.Properties.DisplayFormat.FormatString = "0000000"
            Me.txt_client.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Custom
            Me.txt_client.Properties.EditFormat.FormatString = "f0"
            Me.txt_client.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric
            Me.txt_client.Properties.Mask.BeepOnError = True
            Me.txt_client.Properties.Mask.EditMask = "\d*"
            Me.txt_client.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.RegEx
            Me.txt_client.Properties.ValidateOnEnterKey = True
            Me.txt_client.Size = New System.Drawing.Size(100, 20)
            Me.txt_client.TabIndex = 1
            '
            'col_HousingFeeAmount
            '
            Me.col_HousingFeeAmount.AppearanceCell.Options.UseTextOptions = True
            Me.col_HousingFeeAmount.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
            Me.col_HousingFeeAmount.AppearanceHeader.Options.UseTextOptions = True
            Me.col_HousingFeeAmount.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
            Me.col_HousingFeeAmount.Caption = "Fee"
            Me.col_HousingFeeAmount.ColumnEdit = Me.CalcEdit_HousingFeeAmount
            Me.col_HousingFeeAmount.CustomizationCaption = "Housing Fee Amount"
            Me.col_HousingFeeAmount.DisplayFormat.FormatString = "c"
            Me.col_HousingFeeAmount.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
            Me.col_HousingFeeAmount.FieldName = "HousingFeeAmount"
            Me.col_HousingFeeAmount.GroupFormat.FormatString = "c"
            Me.col_HousingFeeAmount.GroupFormat.FormatType = DevExpress.Utils.FormatType.Numeric
            Me.col_HousingFeeAmount.Name = "col_HousingFeeAmount"
            Me.col_HousingFeeAmount.SortMode = DevExpress.XtraGrid.ColumnSortMode.Value
            Me.col_HousingFeeAmount.ToolTip = "Amount changed for this interview"
            Me.col_HousingFeeAmount.Visible = True
            Me.col_HousingFeeAmount.VisibleIndex = 7
            '
            'CalcEdit_HousingFeeAmount
            '
            Me.CalcEdit_HousingFeeAmount.AllowNullInput = DevExpress.Utils.DefaultBoolean.[False]
            Me.CalcEdit_HousingFeeAmount.AutoHeight = False
            Me.CalcEdit_HousingFeeAmount.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
            Me.CalcEdit_HousingFeeAmount.DisplayFormat.FormatString = "c"
            Me.CalcEdit_HousingFeeAmount.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
            Me.CalcEdit_HousingFeeAmount.EditFormat.FormatString = "c"
            Me.CalcEdit_HousingFeeAmount.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric
            Me.CalcEdit_HousingFeeAmount.Mask.BeepOnError = True
            Me.CalcEdit_HousingFeeAmount.Mask.EditMask = "c"
            Me.CalcEdit_HousingFeeAmount.Name = "CalcEdit_HousingFeeAmount"
            Me.CalcEdit_HousingFeeAmount.Precision = 2
            '
            'Form1
            '
            Me.ClientSize = New System.Drawing.Size(580, 266)
            Me.Controls.Add(Me.GridControl1)
            Me.Controls.Add(Me.lbl_client_name)
            Me.Controls.Add(Me.LabelControl2)
            Me.Controls.Add(Me.LabelControl1)
            Me.Controls.Add(Me.txt_client)
            Me.Name = "Form1"
            Me.Text = "Alter HUD Interview / Result Transactions"
            CType(Me.DxErrorProvider1, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.GridControl1, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.GridView1, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.LookupEdit_Interview, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.LookupEdit_Result, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.LookupEdit_Termination, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.LookUpEdit_HUD_Grant, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.txt_client.Properties, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.CalcEdit_HousingFeeAmount, System.ComponentModel.ISupportInitialize).EndInit()
            Me.ResumeLayout(False)
            Me.PerformLayout()
        End Sub

#End Region

        ''' <summary>
        ''' Record the changed table to the database
        ''' </summary>
        Private Function SaveChanges() As Boolean

            Dim tbl As DataTable = ds.Tables("hud_interviews")
            If tbl Is Nothing Then
                Return True
            End If

            Using cm As New DebtPlus.UI.Common.CursorManager()
                Using da As New SqlDataAdapter()
                    Using UpdateCmd As SqlCommand = New SqlCommand()
                        UpdateCmd.Connection = New SqlConnection(DebtPlus.LINQ.SQLInfoClass.getDefault().ConnectionString)
                        UpdateCmd.CommandText = "UPDATE hud_interviews SET [interview_type]=@interview_type, [hud_result]=@hud_result, [termination_reason]=@termination_reason,[HUD_grant]=@HUD_Grant,[HousingFeeAmount]=@HousingFeeAmount WHERE hud_interview=@hud_interview"
                        UpdateCmd.Parameters.Add("@interview_type", SqlDbType.Int, 4, "interview_type")
                        UpdateCmd.Parameters.Add("@hud_result", SqlDbType.Int, 4, "hud_result")
                        UpdateCmd.Parameters.Add("@termination_reason", SqlDbType.Int, 4, "termination_reason")
                        UpdateCmd.Parameters.Add("@HUD_Grant", SqlDbType.Int, 4, "HUD_Grant")
                        UpdateCmd.Parameters.Add("@HousingFeeAmount", SqlDbType.Decimal, 0, "HousingFeeAmount")
                        UpdateCmd.Parameters.Add("@hud_interview", SqlDbType.Int, 4, "hud_interview")
                        da.UpdateCommand = UpdateCmd

                        Using DeleteCmd As SqlCommand = New SqlCommand()
                            DeleteCmd.Connection = New SqlConnection(DebtPlus.LINQ.SQLInfoClass.getDefault().ConnectionString)
                            DeleteCmd.CommandText = "DELETE update_hud_interviews WHERE hud_interview=@hud_interview"
                            DeleteCmd.Parameters.Add("@hud_interview", SqlDbType.Int, 4, "hud_interview")
                            da.DeleteCommand = DeleteCmd

                            Try
                                da.Update(tbl)
                                Return True

                            Catch ex As SqlException
                                DebtPlus.UI.Common.ErrorHandling.HandleErrors(ex, "Error updating hud interviews")
                            End Try
                        End Using
                    End Using
                End Using
            End Using

            Return False
        End Function

        ''' <summary>
        ''' Process a change in the client ID
        ''' </summary>
        Private Sub txt_client_Validated(ByVal sender As Object, ByVal e As EventArgs)

            ' Save the changes to the client.
            If SaveChanges() Then

                ' Read the client name
                Dim client_name As String = String.Empty

                ' Attempt to read the new client's transactions into the list
                Dim clientID As Int32 = txt_client.EditValue.GetValueOrDefault(-1)
                If clientID >= 0 Then
                    client_name = ClientName(clientID)
                    lbl_client_name.Text = client_name

                    Using cm As New DebtPlus.UI.Common.CursorManager()
                        Try
                            Using cmd As SqlCommand = New SqlCommand
                                cmd.Connection = New SqlConnection(DebtPlus.LINQ.SQLInfoClass.getDefault().ConnectionString)
                                cmd.CommandText = "SELECT [hud_interview],[client],[HUD_Grant],[interview_type],[interview_date],[interview_counselor],[hud_result],[result_date],[result_counselor],[termination_reason],[termination_date],[termination_counselor],[HousingFeeAmount] FROM hud_interviews WHERE client = @client"
                                cmd.Parameters.Add("@client", SqlDbType.Int).Value = clientID

                                Using da As New SqlDataAdapter(cmd)
                                    da.Fill(ds, "hud_interviews")
                                End Using
                            End Using

                            Dim tbl As DataTable = ds.Tables("hud_interviews")
                            With tbl
                                If .PrimaryKey.GetUpperBound(0) < 0 Then
                                    .PrimaryKey = New DataColumn() {.Columns("hud_interview")}
                                End If
                            End With

                            ' If there are no items then say so
                            Dim vue As New DataView(tbl, String.Format("[client]={0:f0}", clientID), "interview_date", DataViewRowState.CurrentRows)
                            If vue.Count <= 0 Then txt_client.ErrorText = "There are no items for this client"

                            GridControl1.DataSource = vue
                            GridControl1.RefreshDataSource()

                        Catch ex As SqlException
                            DebtPlus.UI.Common.ErrorHandling.HandleErrors(ex, "Error reading hud transactions")
                        End Try
                    End Using
                End If
            End If
        End Sub

        ''' <summary>
        ''' Read the client name from the database
        ''' </summary>
        Private Function ClientName(ByVal ClientID As Int32) As String

            If ClientID <= 0 Then
                Return String.Empty
            End If

            Using cn As SqlConnection = New SqlConnection(DebtPlus.LINQ.SQLInfoClass.getDefault().ConnectionString)
                Using cm As New DebtPlus.UI.Common.CursorManager()
                    Try
                        cn.Open()
                        Using cmd As SqlCommand = New SqlCommand
                            cmd.Connection = cn
                            cmd.CommandText = "SELECT [name] FROM [view_client_address] WITH (NOLOCK) WHERE [client] = @client"
                            cmd.Parameters.Add("@client", SqlDbType.Int).Value = ClientID
                            Return If(DebtPlus.Utils.Nulls.v_String(cmd.ExecuteScalar()), String.Empty).Trim()
                        End Using

                    Catch ex As SqlException
                        DebtPlus.UI.Common.ErrorHandling.HandleErrors(ex, "Error reading client name")
                    End Try
                End Using
            End Using

            Return String.Empty
        End Function

        ''' <summary>
        ''' Save the changes when the form is closed
        ''' </summary>
        Private Sub Form1_FormClosing(ByVal sender As Object, ByVal e As FormClosingEventArgs)

            ' Ask if the changes are to be made to the database
            If GridControl1.DataSource IsNot Nothing AndAlso CType(GridControl1.DataSource, DataView).Count > 0 Then
                Select Case DebtPlus.Data.Forms.MessageBox.Show("Do you wish to update the database with the changes before you quit?", "Are you sure?", MessageBoxButtons.YesNoCancel, MessageBoxIcon.Question)
                    Case DialogResult.No
                    Case DialogResult.Yes
                        If Not SaveChanges() Then e.Cancel = True

                    Case DialogResult.Cancel
                        e.Cancel = True
                End Select
            End If
        End Sub

        ''' <summary>
        ''' Process a load event on the form
        ''' </summary>
        Private Sub Form1_Load(ByVal sender As Object, ByVal e As EventArgs)

            ' Restore the previous size
            MyBase.LoadPlacement("HUD.Interview.update")

            LookupEdit_Interview.DataSource = Housing_PurposeOfVisitTypes.DefaultView
            LookupEdit_Interview.DisplayMember = "description"
            LookupEdit_Interview.ValueMember = "oID"
            LookupEdit_Interview.PopupWidth = col_hud_interview.Width + 200

            LookupEdit_Result.DataSource = Nothing
            LookupEdit_Result.DisplayMember = "description"
            LookupEdit_Result.ValueMember = "oID"

            LookupEdit_Termination.DataSource = Housing_TerminationReasonTypes.DefaultView
            LookupEdit_Termination.DisplayMember = "description"
            LookupEdit_Termination.ValueMember = "oID"
            LookupEdit_Termination.PopupWidth = col_hud_interview.Width + 200

            LookUpEdit_HUD_Grant.DataSource = Housing_GrantTypes.DefaultView
            LookUpEdit_HUD_Grant.DisplayMember = "description"
            LookUpEdit_HUD_Grant.ValueMember = "oID"
            LookUpEdit_HUD_Grant.PopupWidth = col_hud_interview.Width + 200
        End Sub

        ''' <summary>
        ''' Return a pointer to the interview types table
        ''' </summary>
        Private Function Housing_PurposeOfVisitTypes() As DataTable

            ' Attempt to read the new client's transactions into the list
            Dim tbl As DataTable = ds.Tables("Housing_PurposeOfVisitTypes")

            If tbl IsNot Nothing Then
                Return tbl
            End If

            Using cm As New DebtPlus.UI.Common.CursorManager()
                Try
                    Using cmd As SqlCommand = New SqlCommand
                        cmd.Connection = New SqlConnection(DebtPlus.LINQ.SQLInfoClass.getDefault().ConnectionString)
                        cmd.CommandText = "SELECT item_value AS oID, description as description FROM messages WITH (NOLOCK) WHERE 'HUD INTERVIEW TYPE' = item_type ORDER BY 1"
                        cmd.CommandType = CommandType.Text

                        Using da As New SqlDataAdapter(cmd)
                            da.Fill(ds, "Housing_PurposeOfVisitTypes")
                        End Using
                    End Using

                    Return ds.Tables("Housing_PurposeOfVisitTypes")

                Catch ex As SqlException
                    DebtPlus.UI.Common.ErrorHandling.HandleErrors(ex, "Error reading hud interview types")
                End Try
            End Using

            Return Nothing
        End Function

        ''' <summary>
        ''' Return a pointer to the Grant types table
        ''' </summary>
        Private Function Housing_GrantTypes() As DataTable

            ' Attempt to read the new client's transactions into the list
            Dim tbl As DataTable = ds.Tables("Housing_GrantTypes")

            If tbl Is Nothing Then
                Dim current_cursor As Cursor = Cursor.Current
                Try
                    Cursor.Current = Cursors.WaitCursor
                    Using cmd As SqlCommand = New SqlCommand
                        With cmd
                            .Connection = New SqlConnection(DebtPlus.LINQ.SQLInfoClass.getDefault().ConnectionString)
                            .CommandText = "SELECT item_value AS oID, description as description FROM messages WITH (NOLOCK) WHERE 'HUD GRANT' = item_type ORDER BY 1"
                        End With

                        Using da As New SqlDataAdapter(cmd)
                            da.Fill(ds, "Housing_GrantTypes")
                        End Using
                    End Using

                    tbl = ds.Tables("Housing_GrantTypes")

                Catch ex As SqlException
                    DebtPlus.UI.Common.ErrorHandling.HandleErrors(ex, "Error reading Housing_GrantTypes")

                Finally
                    Cursor.Current = current_cursor
                End Try
            End If

            Return tbl
        End Function

        ''' <summary>
        ''' Return a pointer to the result types table
        ''' </summary>
        Private Function Housing_VisitOutcomeTypes() As DataTable

            ' Attempt to read the new client's transactions into the list
            Dim tbl As DataTable = ds.Tables("Housing_VisitOutcomeTypes")

            If tbl Is Nothing Then
                Dim current_cursor As Cursor = Cursor.Current
                Try
                    Cursor.Current = Cursors.WaitCursor
                    Using cmd As SqlCommand = New SqlCommand
                        With cmd
                            .Connection = New SqlConnection(DebtPlus.LINQ.SQLInfoClass.getDefault().ConnectionString)
                            .CommandText = "SELECT item_value AS oID, description as description FROM messages WITH (NOLOCK) WHERE 'HUD RESULT' = item_type ORDER BY 1"
                        End With

                        Using da As New SqlDataAdapter(cmd)
                            da.Fill(ds, "Housing_VisitOutcomeTypes")
                        End Using
                    End Using

                    tbl = ds.Tables("Housing_VisitOutcomeTypes")

                Catch ex As SqlException
                    DebtPlus.UI.Common.ErrorHandling.HandleErrors(ex, "Error reading hud result types")

                Finally
                    Cursor.Current = current_cursor
                End Try
            End If

            Return tbl
        End Function

        ''' <summary>
        ''' Return a pointer to the interview types table
        ''' </summary>
        Private Function Housing_TerminationReasonTypes() As DataTable

            ' Attempt to read the new client's transactions into the list
            Dim tbl As DataTable = ds.Tables("Housing_TerminationReasonTypes")

            If tbl Is Nothing Then
                Dim current_cursor As Cursor = Cursor.Current
                Try
                    Cursor.Current = Cursors.WaitCursor
                    Using cmd As SqlCommand = New SqlCommand
                        With cmd
                            .Connection = New SqlConnection(DebtPlus.LINQ.SQLInfoClass.getDefault().ConnectionString)
                            .CommandText = "SELECT item_value as oID, description FROM messages where item_type = 'HUD TERM REASON'"
                            .CommandType = CommandType.Text
                        End With

                        Using da As New SqlDataAdapter(cmd)
                            da.Fill(ds, "Housing_TerminationReasonTypes")
                        End Using
                    End Using

                    tbl = ds.Tables("Housing_TerminationReasonTypes")

                Catch ex As SqlException
                    DebtPlus.UI.Common.ErrorHandling.HandleErrors(ex, "Error reading hud termination reasons")

                Finally
                    Cursor.Current = current_cursor
                End Try
            End If

            Return tbl
        End Function

        ''' <summary>
        ''' Return a pointer to the table listing valid results for types
        ''' </summary>
        Private Function Housing_AllowedVisitOutcomeTypesTable() As DataTable

            ' Attempt to read the new client's transactions into the list
            Dim tbl As DataTable = ds.Tables("Housing_AllowedVisitOutcomeTypes")

            If tbl Is Nothing Then
                Dim current_cursor As Cursor = Cursor.Current
                Try
                    Cursor.Current = Cursors.WaitCursor
                    Using cmd As SqlCommand = New SqlCommand
                        With cmd
                            .Connection = New SqlConnection(DebtPlus.LINQ.SQLInfoClass.getDefault().ConnectionString)
                            .CommandText = "SELECT PurposeOfVisit AS hud_interview, Outcome AS hud_result FROM Housing_AllowedVisitOutcomeTypes WITH (NOLOCK) WHERE PurposeOfVisit IS NOT NULL AND Outcome IS NOT NULL ORDER BY 1, 2"
                        End With

                        Using da As New SqlDataAdapter(cmd)
                            da.Fill(ds, "Housing_AllowedVisitOutcomeTypes")
                        End Using
                    End Using

                    tbl = ds.Tables("Housing_AllowedVisitOutcomeTypes")

                Catch ex As SqlException
                    DebtPlus.UI.Common.ErrorHandling.HandleErrors(ex, "Error reading Housing_AllowedVisitOutcomeTypes table")

                Finally
                    Cursor.Current = current_cursor
                End Try
            End If

            Return tbl
        End Function

        ''' <summary>
        ''' When the row changes, reload the list of valid results
        ''' </summary>
        Private Sub GridView1_FocusedRowChanged(ByVal sender As Object, ByVal e As FocusedRowChangedEventArgs)
            ReloadResultList(e.FocusedRowHandle)
        End Sub

        ''' <summary>
        ''' When the interview is changed, reload the list of results
        ''' </summary>
        Private Sub GridView1_CellValueChanged(ByVal sender As Object, ByVal e As CellValueChangedEventArgs)
            If e.Column.FieldName = "interview_type" Then
                ReloadResultList(e.RowHandle)
            End If
        End Sub

        ''' <summary>
        ''' reload the list of valid results
        ''' </summary>
        Private Sub ReloadResultList(ByVal Handle As Int32)
            Dim row As DataRow = GridView1.GetDataRow(Handle)
            Dim DataSource As DataView = Nothing

            ' From the row, find the interview id
            If row IsNot Nothing AndAlso row("interview_type") IsNot Nothing AndAlso row("interview_type") IsNot DBNull.Value Then
                Dim interview As Int32 = Convert.ToInt32(row("interview_type"))

                ' Build a list of the valid results for this interview type
                Dim sb As New StringBuilder
                Using vue As New DataView(Housing_AllowedVisitOutcomeTypesTable, String.Format("[hud_interview]={0:f0}", interview), "hud_result", DataViewRowState.CurrentRows)
                    For Each drv As DataRowView In vue
                        sb.AppendFormat(",{0:f0}", drv("hud_result"))
                    Next
                End Using

                ' If there are results then build the proper selection clause
                If sb.Length > 0 Then
                    sb.Remove(0, 1)
                    sb.Insert(0, "oID in (")
                    sb.Append(")")

                    ' Create a view to select only those items that match the interview type
                    DataSource = New DataView(Housing_VisitOutcomeTypes, sb.ToString(), "description", DataViewRowState.CurrentRows)
                End If
            End If

            With LookupEdit_Result
                .DataSource = DataSource
                .PopupWidth = col_hud_result.Width + 200
                .BestFit()
            End With
        End Sub

        ''' <summary>
        ''' Process the DELETE menu item click event
        ''' </summary>
        Private Sub MenuItem_delete_Click(ByVal sender As Object, ByVal e As EventArgs)
            Dim row As DataRow = GridView1.GetDataRow(GridView1.FocusedRowHandle)

            If row IsNot Nothing Then
                If DebtPlus.Data.Prompts.RequestConfirmation_Delete("Are you sure that you want to delete this item?") = DialogResult.Yes Then
                    row.Delete()
                    GridControl1.RefreshDataSource()
                End If
            End If
        End Sub

        ''' <summary>
        ''' Find the row selected when the click event occurs
        ''' </summary>
        Private Sub GridView1_Click(ByVal sender As Object, ByVal e As EventArgs)

            ' Find the row targeted as the double-click item
            Dim gv As GridView = CType(sender, GridView)
            Dim hi As GridHitInfo = gv.CalcHitInfo((gv.GridControl.PointToClient(MousePosition)))

            If hi.InRow AndAlso hi.IsValid Then
                GridView1.FocusedRowHandle = hi.RowHandle
            Else
                GridView1.FocusedRowHandle = -1
            End If
        End Sub

        ''' <summary>
        ''' Do the POPUP event for the right-click menu
        ''' </summary>
        Private Sub ContextMenu1_Popup(ByVal sender As Object, ByVal e As EventArgs)
            Dim row As DataRow = GridView1.GetDataRow(GridView1.FocusedRowHandle)
            MenuItem_delete.Enabled = row IsNot Nothing
        End Sub

        ''' <summary>
        ''' Return the directory to the saved layout information
        ''' </summary>
        Protected Overridable Function XMLBasePath() As String
            Dim BasePath As String = Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData) + Path.DirectorySeparatorChar + "DebtPlus"
            Return Path.Combine(BasePath, "HUD.Interview.Update")
        End Function

        ''' <summary>
        ''' When loading the control, restore the layout from the file
        ''' </summary>
        Private Sub MyGridControl_Load(ByVal sender As Object, ByVal e As EventArgs)
            Dim PathName As String = XMLBasePath()
            Dim FileName As String = Path.Combine(PathName, Name + ".Grid.xml")

            Try
                If File.Exists(FileName) Then
                    GridView1.RestoreLayoutFromXml(FileName)
                End If
            Catch ex As DirectoryNotFoundException
            Catch ex As FileNotFoundException
            End Try

            ' Hook into the changes of the layout from this point
            AddHandler GridView1.Layout, AddressOf LayoutChanged
        End Sub

        ''' <summary>
        ''' If the layout is changed then update the file
        ''' </summary>
        Private Sub LayoutChanged(ByVal Sender As Object, ByVal e As EventArgs)
            Dim PathName As String = XMLBasePath()
            If Not Directory.Exists(PathName) Then
                Directory.CreateDirectory(PathName)
            End If

            Dim FileName As String = Path.Combine(PathName, Name + ".Grid.xml")
            GridView1.SaveLayoutToXml(FileName)
        End Sub
    End Class
End Namespace
